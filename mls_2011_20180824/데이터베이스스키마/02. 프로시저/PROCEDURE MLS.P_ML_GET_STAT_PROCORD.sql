CREATE OR REPLACE PROCEDURE MLS.P_ML_GET_STAT_PROCORD (
  P_GUBUN NUMBER
)
IS

    V_CNT_YN NUMBER;
    V_TARGCNT_YN NUMBER;
    V_CNT NUMBER;
    V_CNT_SUB NUMBER;
    V_ORD NUMBER;
    

----------------------------------------------------------------------------------
--서브 펑션 및 프로시져 
----------------------------------------------------------------------------------
 PROCEDURE SP_INSERT_PROCORD(
   P_WORKS_DIVS_CD NUMBER,
   P_STAT_CHNG_YN VARCHAR2
 )
 IS
 
 g_ERROR VARCHAR2(2000);   -- 에러메시지
 g_OID VARCHAR2(50);-- 에러 오브젝트 아이디 (에러대상) 
 
-------------------------------------------------------------------------------
-- 프로그램명 : 상당한노력 로그 입력                                     
-------------------------------------------------------------------------------
  BEGIN
         BEGIN
            
            g_OID := 'ML_STAT_PROC_SUB_LOG || INSERT';
         
            /* 진행중(STAT_CD = 3)인 건이 있는경우 수집불가*/
            SELECT COUNT(1)
              INTO V_CNT_YN
              FROM ML_STAT_PROC_ALL_LOG
             WHERE WORKS_DIVS_CD = P_WORKS_DIVS_CD
               AND STAT_CD = 3
            ;
            
           
            /*대상저작물이 없는 경우 수집불가*/
            SELECT COUNT(1)
              INTO V_TARGCNT_YN
              FROM ML_STAT_PROC_TARG_WORKS
             WHERE STAT_PROC_YN = 'Y'
               AND WORKS_MAPP_DIVS_CD = P_WORKS_DIVS_CD
               AND NVL(STAT_CHNG_YN,'N') = P_STAT_CHNG_YN
             ;
             
                   
             IF ( V_CNT_YN = 0 AND V_TARGCNT_YN>0 ) THEN
        
                /* 기존 미진행(수집완료)건 유무에 따른 수집프로세스 분기 */
                SELECT COUNT(1)
                  INTO V_CNT
                  FROM ML_STAT_PROC_ALL_LOG
                 WHERE WORKS_DIVS_CD = P_WORKS_DIVS_CD
                   AND STAT_CD = 2;
    
            
                /* 기존 수집건수에 추가*/
                IF V_CNT > 0 THEN
                
                
                    /* GET 진행차수 */
                    SELECT NVL(MAX(ORD),0) /*차수*/
                      INTO V_ORD
                      FROM ML_STAT_PROC_ALL_LOG
                     WHERE WORKS_DIVS_CD = P_WORKS_DIVS_CD
                    ;
            
                /* 신규수집 */    
                ELSE
            
                    /* GET 진행차수 */
                    SELECT NVL(MAX(ORD),0)+1 /*차수*/
                      INTO V_ORD
                      FROM ML_STAT_PROC_ALL_LOG
                     WHERE WORKS_DIVS_CD = P_WORKS_DIVS_CD
                    ;
            
                END IF;
    
    
        
                /* 대상물 진행차수 UPDATE */
                UPDATE ML_STAT_PROC_TARG_WORKS
                   SET ORD =  V_ORD /*차수*/
                 WHERE STAT_PROC_YN = 'Y'
                   AND WORKS_MAPP_DIVS_CD = P_WORKS_DIVS_CD
                   AND NVL(STAT_CHNG_YN,'N') = P_STAT_CHNG_YN
               ;
           
        
                /* 전체로그 생성 위한 분기*/
                SELECT COUNT(1)
                  INTO V_CNT_SUB
                  FROM ML_STAT_PROC_ALL_LOG
                 WHERE STAT_CD = 2
                   AND WORKS_DIVS_CD = P_WORKS_DIVS_CD
                ;
           
           
                IF V_CNT_SUB >0 THEN
        
                    /* 처리 로그 UPDATE */
                    UPDATE ML_STAT_PROC_ALL_LOG
                       SET TARG_WORKS_CONT = ( SELECT COUNT(1)
                                                 FROM ML_STAT_PROC_TARG_WORKS
                                                WHERE ORD = V_ORD
                                                  AND WORKS_MAPP_DIVS_CD = P_WORKS_DIVS_CD
                                                  AND  NVL(STAT_CHNG_YN,'N') = P_STAT_CHNG_YN)
                     WHERE ORD = V_ORD
                       AND STAT_CD = 2
                       AND WORKS_DIVS_CD = P_WORKS_DIVS_CD
                     ;
                     
             
                ELSE
        
                    /* 처리 로그 INSERT */
                    INSERT
                      INTO ML_STAT_PROC_ALL_LOG (ORD, YYYYMM, WORKS_DIVS_CD, TARG_WORKS_CONT, STAT_CD)
                    VALUES (V_ORD, TO_CHAR(SYSDATE, 'YYYYMM'), P_WORKS_DIVS_CD
                            ,(SELECT COUNT(1)
                                FROM ML_STAT_PROC_TARG_WORKS
                               WHERE ORD = V_ORD /* 차수 */
                                 AND WORKS_MAPP_DIVS_CD = P_WORKS_DIVS_CD
                                 AND NVL(STAT_CHNG_YN,'N') = P_STAT_CHNG_YN)
                            , 2);         
        
                END IF;
                
                
                
                /* 상당한노력 상태값 update : ML_STAT_WORKS.SYS_EFFORT_STAT_CD = (3) 진행중 */
                UPDATE ML_STAT_WORKS
                   SET SYST_EFFORT_STAT_CD = 3
                      ,SYST_EFFORT_STAT_DTTM = SYSDATE
                 WHERE WORKS_ID IN ( SELECT WORKS_ID
                                       FROM ML_STAT_PROC_TARG_WORKS
                                                WHERE ORD = V_ORD
                                                  AND WORKS_MAPP_DIVS_CD = P_WORKS_DIVS_CD
                                                  AND  NVL(STAT_CHNG_YN,'N') = P_STAT_CHNG_YN)
                ;
                
                
                
            
             END IF;
             
             COMMIT;
         
          
             EXCEPTION  
                  WHEN OTHERS THEN
                   
                   -- RAISE ERR_INSERT('123') ; 
                    ROLLBACK;
                    
                    g_ERROR  := SUBSTR(SQLERRM,1,2000);
                    PKG_ON_ERROR.P_ML_ON_ERROR(SYSDATE, SYS_CONTEXT('USERENV','CURRENT_USER'), SYS_CONTEXT('USERENV','SESSION_USER'), g_OID, g_ERROR); 
                  --   DBMS_OUTPUT.PUT_LINE('ERR CODE1 : ' || SUBSTR(SQLERRM,1,200));
                    -- DBMS_OUTPUT.PUT_LINE('ERR MESSAGE1 : ' || SQLCODE );
                   
          END;                         
                    
  END;


/*===========================================================================*/
/*  메인 프로시져 시작                                                       */
/*===========================================================================*/


    BEGIN
 
        
        ---------------------------------------------------------------------------
         -- 대상저작물 구분 :  거소불명(2), 기승인(3), 법정허락(거소불명) (5) 
        ---------------------------------------------------------------------------
        IF P_GUBUN = 199 THEN
        
            -- 
            
            SP_INSERT_PROCORD(2, 'N'); -- 거소불명 
            SP_INSERT_PROCORD(3, 'N'); -- 기승인 
            SP_INSERT_PROCORD(5, 'Y'); -- 법정허락(거소불명)   
        
        
         ---------------------------------------------------------------------------
         -- 대상저작물 구분 :  미분배(1), 법정허락(미분배) (4) 
        ---------------------------------------------------------------------------
        ELSE
            
            SP_INSERT_PROCORD(1, 'N'); -- 미분배 
            SP_INSERT_PROCORD(4, 'Y'); -- 법정허락(미분배)   
            
        
        END IF;
         
        
    END;
/