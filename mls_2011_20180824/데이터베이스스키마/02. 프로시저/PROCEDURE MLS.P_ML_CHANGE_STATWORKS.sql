CREATE OR REPLACE PROCEDURE MLS.P_ML_CHANGE_STATWORKS (
    P_STATORD NUMBER,
    P_YYYYMM NUMBER,
    P_TOTCNT NUMBER,
    P_FROM_ROW_NO NUMBER,
    P_TO_ROW_NO NUMBER
    )
IS

-------------------------------------------------------------------------------
-- 프로그램명 : 법정허락 전환 프로시저        
--                   법정허락 전환 후 상당한노력 공고 삭제
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 9월 15일                                              
-- 수정  일자 :                                           
-- 스키마정보 : 수집저작물에 법정허락 전환을 진행한다.
                      
-- 수 행 시 기: 관리자 실행                                                       
-------------------------------------------------------------------------------

CURSOR C_TARG_WORKS IS

SELECT *
  FROM (
    SELECT /*+ index(W ML_STAT_PROC_TARG_WORKS_PK) */ 
           ROWNUM RNUM, W.*
      FROM MLS.ML_STAT_PROC_TARG_WORKS W
     WHERE STAT_ORD = P_STATORD
     --  AND WORKS_MAPP_DIVS_CD = P_WORKS_DIV_CD
  ) 
  WHERE 1=1
    AND RNUM  BETWEEN P_FROM_ROW_NO AND P_TO_ROW_NO
 ;

V_CNT_CHNG NUMBER;


BEGIN

    /* 처리 로그 UPDATE : 시작시간, STAT_CD(3)-진행중*/
    IF P_FROM_ROW_NO = 1 THEN
        
        UPDATE ML_STAT_WORKS_ALL_LOG
           SET START_DTTM = SYSDATE
              ,STAT_CD = 3
        WHERE STAT_ORD = P_STATORD
          AND YYYYMM = P_YYYYMM
        --  AND WORKS_DIVS_CD = P_WORKS_DIV_CD
        ;
        
        COMMIT;
                                  
    END IF;
    
    
    V_CNT_CHNG := 0;
    

    FOR I IN C_TARG_WORKS LOOP
        
        
        /* 1.C_TARG_WORKS 법정허락 전환여부 update */
        UPDATE ML_STAT_PROC_TARG_WORKS
           SET WORKS_MAPP_DIVS_CD = DECODE(I.WORKS_DIVS_CD, 4, 5, 4) /* 4:법정허락대상물(미분배). 5:법정허락대상물(거소불명)*/ 
             , STAT_CHNG_YN = 'Y' 
             , STAT_CHNG_DTTM = SYSDATE      
         WHERE STAT_ORD = I.STAT_ORD
           AND WORKS_ID = I.WORKS_ID
         ;
          
        
        /* 2. 원 저작물정보 : 법정허락 여부 update */
        UPDATE ML_STAT_WORKS
           SET STAT_WORKS_YN = 'Y'  
              ,SYST_EFFORT_STAT_CD = 4
              ,SYST_EFFORT_STAT_DTTM = SYSDATE
              ,OPEN_YN = 'Y'
              ,OPEN_DTTM = SYSDATE       
         WHERE WORKS_ID = I.WORKS_ID
        ;
        
        
        /* 3. 상당한 노력공고 : 삭제 여부 update 처리*/
        UPDATE ML_STAT_ANUC_BORD
           SET DELT_YSNO = 'Y'
              ,MODI_IDNT = 'SUPER_ADMIN'
              ,MODI_DTTM = SYSDATE
         WHERE BORD_CD = 1
           AND WORKS_ID = I.WORKS_ID
       ;
        
        
        /* 4. 결과 insert */
                 
        INSERT INTO MLS.ML_STAT_WORKS_SUB_LOG
                 (  STAT_ORD
                  , YYYYMM
                  , WORKS_DIVS_CD
                  , WORKS_ID
                  , PROC_DTTM
                  , STAT_CHNG_YN
                 ) 
            VALUES (  I.STAT_ORD
                     ,P_YYYYMM
                     ,I.WORKS_MAPP_DIVS_CD
                     ,I.WORKS_ID
                     ,SYSDATE
                     ,'Y'
                   )
         ;
                                  
        
          V_CNT_CHNG := V_CNT_CHNG+1;
             
          COMMIT;
          
           
    END LOOP;
    
    
    /* 처리 로그 UPDATE : 처리결과 - 프로시저 실행단위 */
    UPDATE ML_STAT_WORKS_ALL_LOG AL
       SET ( RSLT_CONT )  = (SELECT NVL(RSLT_CONT,0)+V_CNT_CHNG
                               FROM ML_STAT_WORKS_ALL_LOG
                              WHERE STAT_ORD = P_STATORD
                                AND YYYYMM = P_YYYYMM)
      WHERE STAT_ORD = P_STATORD
        AND YYYYMM = P_YYYYMM
     ;
        
     COMMIT;
    
    
    /* 처리 로그 UPDATE : 종료시간, STAT_CD(4)-완료*/
    IF P_TOTCNT = P_TO_ROW_NO THEN
        
        UPDATE ML_STAT_WORKS_ALL_LOG AL
           SET END_DTTM = SYSDATE
              ,STAT_CD = 4
        WHERE STAT_ORD = P_STATORD
          AND YYYYMM = P_YYYYMM
        ;
        
        COMMIT;
                                  
    END IF;
    
    
      
 
   EXCEPTION
       WHEN OTHERS THEN
          --SP_STAT_MATCH_INSERT_LOG(V_MAPP_TOT_SEQN, '프로시저 에러', 0);
          ROLLBACK; 

END;
/