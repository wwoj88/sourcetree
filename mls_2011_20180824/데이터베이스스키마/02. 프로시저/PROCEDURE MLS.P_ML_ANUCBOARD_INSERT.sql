CREATE OR REPLACE PROCEDURE MLS.P_ML_ANUCBOARD_INSERT ( 
    V_ORD NUMBER,
    V_WORKS_ID NUMBER,
    V_WORKS_DIVS_CD NUMBER
    )
IS

  g_ERROR VARCHAR2(2000);   -- 에러메시지
  g_OID VARCHAR2(50);-- 에러 오브젝트 아이디 (에러대상)
-------------------------------------------------------------------------------
-- 프로그램명 : 상당한노력 공고 등록 프로시저        
--                   각 미분배보상금정보, 거소불명저작물에 대한 상당한노력공고
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 9월 15일                                              
-- 수정  일자 :                                           
-- 스키마정보 : 저작물에 대한 상당한노력 공고 등록
                      
-- 수 행 시 기: P_ML_STATPROC 상당한노력 프로시저에서 호출                                    
-------------------------------------------------------------------------------
     
BEGIN
    
    g_OID := 'ML_STAT_ANUC_BORD|| INSERT';
    
    /* 방송음악 보상금 */
    IF V_WORKS_DIVS_CD = 1 THEN
            
            INSERT
              INTO ML_STAT_ANUC_BORD (BORD_CD, BORD_SEQN, DIVS_CD, WORKS_ID, GENRE_CD, OPEN_YN, OPEN_IDNT, OPEN_DTTM
                                     ,RGST_IDNT, RGST_DTTM, DELT_YSNO, OBJC_YN
                                     ,TITE /* 제목*/
                                     ,BORD_DESC  /* 1. 저작권자를 찾는다는 취지 */
                                     ,ANUC_ITEM_1 /* 저작재산권자의 성명*/
                                     ,ANUC_ITEM_2 /* 저작재산권자의 주소*/
                                     ,ANUC_ITEM_3 /* 저작재산권자의 연락처*/
                                     ,ANUC_ITEM_4 /* 저작물의 제호*/
                                     ,ANUC_ITEM_5 /* 공표시 표시된 저작재산권자의 성명 */
                                     ,ANUC_ITEM_6 /* 저작물 발행자 */ 
                                     ,ANUC_ITEM_7 /* 공표 연월일 */
                                     ,ANUC_ITEM_8 /* 저작물의 이용 목적 */
                                     ,ANUC_ITEM_9 /* 공고자 */
                                     ,ANUC_ITEM_10 /* 공고자 주소 */
                                     ,ANUC_ITEM_11 /* 공고자 연락처 */
                                     ,ANUC_ITEM_12 /* 공고자 담당자 */
                                    )
            SELECT  1
                   ,(SELECT NVL(MAX(BORD_SEQN),0)+1 FROM ML_STAT_ANUC_BORD)
                   ,V_WORKS_DIVS_CD
                   ,V_WORKS_ID
                   ,W.GENRE_CD
                   ,'Y'
                   ,'SUPER_ADMIN'
                   ,SYSDATE
                   ,'SUPER_ADMIN'
                   ,SYSDATE
                   ,'N'
                   ,'N'    
                   ,'<'||W.WORKS_TITLE||'> 저작권자 조회 공고'
                   ,'미분배 보상금 대상 저작물 및 저작재산권자 불명인 저작물 이용에 대한 저작권법 제18조 2항에 따른 공고'
                   --
                   ,BI.LYRI_WRTR ||' '|| BI.COMS_WRTR
                   ,'알수없음'
                   ,'알수없음'
                   ,W.WORKS_TITLE
                   ,'알수없음'
                   --
                   ,'알수없음'
                   ,'알수없음'
                   ,'알수없음'
                   ,'한국저작권위원회'
                   ,'서울특별시 강남구 개포동길 619 강남우체국 6,7층'
                   --
                   ,'02-2660-0126'
                   ,'유통진흥팀'
              FROM ML_STAT_WORKS W   
                  ,ML_STAT_BRCT_INMT BI
             WHERE W.WORKS_ID = BI.WORKS_ID
               AND W.WORKS_ID = V_WORKS_ID;
            
               
        
    END IF;
    
    
    /* 교과용 보상금 */
    IF V_WORKS_DIVS_CD = 2 THEN
    
      
            INSERT
              INTO ML_STAT_ANUC_BORD (BORD_CD, BORD_SEQN, DIVS_CD, WORKS_ID, GENRE_CD, OPEN_YN, OPEN_IDNT, OPEN_DTTM
                                     ,RGST_IDNT, RGST_DTTM, DELT_YSNO, OBJC_YN
                                     ,TITE /* 제목*/
                                     ,BORD_DESC  /* 1. 저작권자를 찾는다는 취지 */
                                     ,ANUC_ITEM_1 /* 저작재산권자의 성명*/
                                     ,ANUC_ITEM_2 /* 저작재산권자의 주소*/
                                     ,ANUC_ITEM_3 /* 저작재산권자의 연락처*/
                                     ,ANUC_ITEM_4 /* 저작물의 제호*/
                                     ,ANUC_ITEM_5 /* 공표시 표시된 저작재산권자의 성명 */
                                     ,ANUC_ITEM_6 /* 저작물 발행자 */ 
                                     ,ANUC_ITEM_7 /* 공표 연월일 */
                                     ,ANUC_ITEM_8 /* 저작물의 이용 목적 */
                                     ,ANUC_ITEM_9 /* 공고자 */
                                     ,ANUC_ITEM_10 /* 공고자 주소 */
                                     ,ANUC_ITEM_11 /* 공고자 연락처 */
                                     ,ANUC_ITEM_12 /* 공고자 담당자 */
                                    )
            SELECT  1
                   ,(SELECT NVL(MAX(BORD_SEQN),0)+1 FROM ML_STAT_ANUC_BORD)
                   ,V_WORKS_DIVS_CD
                   ,V_WORKS_ID
                   ,W.GENRE_CD
                   ,'Y'
                   ,'SUPER_ADMIN'
                   ,SYSDATE
                   ,'SUPER_ADMIN'
                   ,SYSDATE
                   ,'N'
                   ,'N'
                   ,'<'||W.WORKS_TITLE||'> 저작권자 조회 공고'
                   ,'미분배 보상금 대상 저작물 및 저작재산권자 불명인 저작물 이용에 대한 저작권법 제18조 2항에 따른 공고'
                   --
                   ,NVL(SI.COPT_HODR, '알수없음')
                   ,'알수없음'
                   ,'알수없음'
                   ,W.WORKS_TITLE
                   ,'알수없음'
                   --
                   ,'알수없음'
                   ,W.OPEN_DTTM
                   ,'알수없음'
                   ,'한국저작권위원회'
                   ,'서울특별시 강남구 개포동길 619 강남우체국 6,7층'
                   --
                   ,'02-2660-0126'
                   ,'유통진흥팀'
              FROM ML_STAT_WORKS W   
                  ,ML_STAT_SUBJ_INMT SI
             WHERE W.WORKS_ID = SI.WORKS_ID
               AND W.WORKS_ID = V_WORKS_ID;
        
    END IF;
    
    /* 도서관 보상금 */
    IF V_WORKS_DIVS_CD = 3 THEN
    
      
            INSERT
              INTO ML_STAT_ANUC_BORD (BORD_CD, BORD_SEQN, DIVS_CD, WORKS_ID, GENRE_CD, OPEN_YN, OPEN_IDNT, OPEN_DTTM
                                     ,RGST_IDNT, RGST_DTTM, DELT_YSNO, OBJC_YN
                                     ,TITE /* 제목*/
                                     ,BORD_DESC  /* 1. 저작권자를 찾는다는 취지 */
                                     ,ANUC_ITEM_1 /* 저작재산권자의 성명*/
                                     ,ANUC_ITEM_2 /* 저작재산권자의 주소*/
                                     ,ANUC_ITEM_3 /* 저작재산권자의 연락처*/
                                     ,ANUC_ITEM_4 /* 저작물의 제호*/
                                     ,ANUC_ITEM_5 /* 공표시 표시된 저작재산권자의 성명 */
                                     ,ANUC_ITEM_6 /* 저작물 발행자 */ 
                                     ,ANUC_ITEM_7 /* 공표 연월일 */
                                     ,ANUC_ITEM_8 /* 저작물의 이용 목적 */
                                     ,ANUC_ITEM_9 /* 공고자 */
                                     ,ANUC_ITEM_10 /* 공고자 주소 */
                                     ,ANUC_ITEM_11 /* 공고자 연락처 */
                                     ,ANUC_ITEM_12 /* 공고자 담당자 */
                                    )
            SELECT  1
                   ,(SELECT NVL(MAX(BORD_SEQN),0)+1 FROM ML_STAT_ANUC_BORD)
                   ,V_WORKS_DIVS_CD
                   ,V_WORKS_ID
                   ,W.GENRE_CD
                   ,'Y'
                   ,'SUPER_ADMIN'
                   ,SYSDATE
                   ,'SUPER_ADMIN'
                   ,SYSDATE
                   ,'N'
                   ,'N'
                   ,'<'||W.WORKS_TITLE||'> 저작권자 조회 공고'
                   ,'미분배 보상금 대상 저작물 및 저작재산권자 불명인 저작물 이용에 대한 저작권법 제18조 2항에 따른 공고'
                   --
                   ,NVL(LI.COPT_HODR, '알수없음')
                   ,'알수없음'
                   ,'알수없음'
                   ,W.WORKS_TITLE
                   ,'알수없음'
                   --
                   ,'알수없음'
                   ,W.OPEN_DTTM
                   ,'알수없음'
                   ,'한국저작권위원회'
                   ,'서울특별시 강남구 개포동길 619 강남우체국 6,7층'
                   --
                   ,'02-2660-0126'
                   ,'유통진흥팀'
              FROM ML_STAT_WORKS W   
                  ,ML_STAT_LIBR_INMT LI
             WHERE W.WORKS_ID = LI.WORKS_ID
               AND W.WORKS_ID = V_WORKS_ID;
        
    END IF;
    
    /* 거소불명 */
    IF V_WORKS_DIVS_CD = 4 THEN
    
      
            INSERT
              INTO ML_STAT_ANUC_BORD (BORD_CD, BORD_SEQN, DIVS_CD, WORKS_ID, GENRE_CD, OPEN_YN, OPEN_IDNT, OPEN_DTTM
                                     ,RGST_IDNT, RGST_DTTM, DELT_YSNO, OBJC_YN
                                     ,TITE /* 제목*/
                                     ,BORD_DESC  /* 1. 저작권자를 찾는다는 취지 */
                                     ,ANUC_ITEM_1 /* 저작재산권자의 성명*/
                                     ,ANUC_ITEM_2 /* 저작재산권자의 주소*/
                                     ,ANUC_ITEM_3 /* 저작재산권자의 연락처*/
                                     ,ANUC_ITEM_4 /* 저작물의 제호*/
                                     ,ANUC_ITEM_5 /* 공표시 표시된 저작재산권자의 성명 */
                                     ,ANUC_ITEM_6 /* 저작물 발행자 */ 
                                     ,ANUC_ITEM_7 /* 공표 연월일 */
                                     ,ANUC_ITEM_8 /* 저작물의 이용 목적 */
                                     ,ANUC_ITEM_9 /* 공고자 */
                                     ,ANUC_ITEM_10 /* 공고자 주소 */
                                     ,ANUC_ITEM_11 /* 공고자 연락처 */
                                     ,ANUC_ITEM_12 /* 공고자 담당자 */
                                    )
            SELECT  1
                   ,(SELECT NVL(MAX(BORD_SEQN),0)+1 FROM ML_STAT_ANUC_BORD)
                   ,V_WORKS_DIVS_CD
                   ,V_WORKS_ID
                   ,W.GENRE_CD
                   ,'Y'
                   ,'SUPER_ADMIN'
                   ,SYSDATE
                   ,'SUPER_ADMIN'
                   ,SYSDATE
                   ,'N'
                   ,'N'
                    ,'<'||W.WORKS_TITLE||'> 저작권자 조회 공고'
                   ,ANUC_ITEM_1  /* 1. 저작권자를 찾는다는 취지 */
                   ,ANUC_ITEM_2 /* 저작재산권자의 성명*/
                   ,ANUC_ITEM_3 /* 저작재산권자의 주소*/
                   ,ANUC_ITEM_4 /* 저작재산권자의 연락처*/
                   ,W.WORKS_TITLE /* 저작물의 제호*/
                   ,ANUC_ITEM_5 /* 공표시 표시된 저작재산권자의 성명 */
                   ,ANUC_ITEM_6 /* 저작물 발행자 */ 
                   ,ANUC_ITEM_7 /* 공표 연월일 */
                   ,ANUC_ITEM_8 /* 저작물의 이용 목적 */
                   ,ANUC_ITEM_9 /* 공고자 */
                   ,ANUC_ITEM_10 /* 공고자 주소 */
                   ,ANUC_ITEM_11 /* 공고자 연락처 */
                   ,ANUC_ITEM_12 /* 공고자 담당자 */
              FROM ML_STAT_WORKS W   
                  ,ML_NON_IDNT_WORKS IW
             WHERE W.WORKS_ID = IW.WORKS_ID
               AND W.WORKS_ID = V_WORKS_ID;
                
    END IF;
    
    COMMIT;
    
    EXCEPTION
       WHEN OTHERS THEN
          -- 에러처리 
          --SELECT  F_ML_INSERT_ERROR(V_ORD, V_WORKS_ID, 1) FROM DUAL;
          ROLLBACK;
          
          P_ML_INSERT_ERROR_LOG(V_ORD, V_WORKS_ID, 1);
                             
          g_ERROR  := SUBSTR(SQLERRM,1,2000);
          PKG_ON_ERROR.P_ML_ON_ERROR(SYSDATE, SYS_CONTEXT('USERENV','CURRENT_USER'), SYS_CONTEXT('USERENV','SESSION_USER'), g_OID, g_ERROR); 
END;
/