CREATE OR REPLACE PROCEDURE MLS.P_ML_STATPROC_02(
    P_ORD NUMBER,
    P_YYYYMM NUMBER,
    P_WORKS_DIVS_CD NUMBER,
    P_TOTCNT NUMBER,
    P_FROM_ROW_NO NUMBER,
    P_TO_ROW_NO NUMBER
    )
IS

  ERR_OCCUR1  EXCEPTION;   -- 에러처리1 
  ERR_INSERT1  EXCEPTION;  -- 인서트 오류  
  ERR_UPDATE1  EXCEPTION;  -- 업데이트 오류
  ERR_UPDATE2  EXCEPTION;  -- 업데이트 오류
  
  g_ERROR VARCHAR2(2000);   -- 에러메시지
  g_OID VARCHAR2(50);-- 에러 오브젝트 아이디 (에러대상)
  
-------------------------------------------------------------------------------
-- 프로그램명 : 상당한노력 진행 프로시저        
--              - 1차매칭 (위탁관리저작물) 
--              - 2차매칭(기승인저작물) - 법정허락 대상물은 매칭 제외
--              - 3차매칭 (저작권등록) 
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 11월 08일                                              
-- 수정  일자 :                                           
-- 스키마정보 : 수집저작물에 상당한 노력(02.상당한노력) 을 진행한다. 
                      
-- 수 행 시 기: 관리자 실행                                                       
-------------------------------------------------------------------------------


V_CNT_1ST NUMBER;
V_CNT_2ND NUMBER;
V_CNT_3RD NUMBER;
V_CNT_NON_1ST NUMBER;
V_CNT_NON_2ND NUMBER;
V_CNT_NON_3RD NUMBER;
V_CNT_ANUC NUMBER;

V_RSLT_1ST_YN CHAR(1);  
V_RSLT_2ND_YN CHAR(1);  
V_RSLT_3RD_YN CHAR(1); 
V_ANUC_REGI_YN CHAR(1); 
V_ANUC_REGI_DTTM DATE;

V_RSLT_1ST_ID VARCHAR2(4000);
V_RSLT_2ND_ID VARCHAR2(4000);
V_RSLT_3RD_ID VARCHAR2(4000);


CURSOR C_TARG_WORKS IS

SELECT *
  FROM (
    SELECT /*+ index(W ML_STAT_PROC_TARG_WORKS_PK) */
           ROWNUM RNUM, W.*
      FROM MLS.ML_STAT_PROC_TARG_WORKS W
     WHERE ORD = P_ORD
       AND WORKS_MAPP_DIVS_CD = P_WORKS_DIVS_CD
  ) 
  WHERE 1=1
    AND RNUM  BETWEEN P_FROM_ROW_NO AND P_TO_ROW_NO
 ;


----------------------------------------------------------------------------------
--서브 펑션 및 프로시져 
----------------------------------------------------------------------------------
 PROCEDURE SP_INSERT_SUBLOG(
   P_ORD NUMBER,
   P_YYYYMM NUMBER,
   P_WORKS_DIVS_CD NUMBER,    
   P_WORKS_ID NUMBER,
   P_RSLT_1ST_YN CHAR,
   P_RSLT_2ND_YN CHAR,
   P_RSLT_3RD_YN CHAR,
   P_ANUC_REGI_YN CHAR,
   P_RSLT_1ST_ID VARCHAR2,
   P_RSLT_2ND_ID VARCHAR2,
   P_RSLT_3RD_ID VARCHAR2,
   P_ANUC_REGI_DTTM DATE
 )
 IS
 
 g_ERROR VARCHAR2(2000);   -- 에러메시지
 g_OID VARCHAR2(50);-- 에러 오브젝트 아이디 (에러대상) 
 
-------------------------------------------------------------------------------
-- 프로그램명 : 상당한노력 로그 입력                                     
-------------------------------------------------------------------------------
  BEGIN
         BEGIN
            
            g_OID := 'ML_STAT_PROC_SUB_LOG || INSERT';
         
            INSERT INTO MLS.ML_STAT_PROC_SUB_LOG
                       (  ORD
                        , YYYYMM
                        , WORKS_DIVS_CD
                        , WORKS_ID
                        , PROC_DTTM
                        , RSLT_1ST_YN
                        , RSLT_2ND_YN
                        , RSLT_3RD_YN
                        , ANUC_REGI_YN
                        , RSLT_1ST_WORKS_ID 
                        , RSLT_2ND_WORKS_ID 
                        , RSLT_3RD_WORKS_ID
                        , ANUC_REGI_DTTM 
                       ) 
                  VALUES (  P_ORD
                           ,P_YYYYMM
                           ,P_WORKS_DIVS_CD    
                           ,P_WORKS_ID
                           ,SYSDATE
                           ,P_RSLT_1ST_YN 
                           ,P_RSLT_2ND_YN 
                           ,P_RSLT_3RD_YN 
                           ,P_ANUC_REGI_YN 
                           ,P_RSLT_1ST_ID 
                           ,P_RSLT_2ND_ID 
                           ,P_RSLT_3RD_ID 
                           ,P_ANUC_REGI_DTTM
                          )
                     ;
                                
            COMMIT;
          
             EXCEPTION  
                  WHEN OTHERS THEN
                   
                    ROLLBACK;
                    
                    g_ERROR  := SUBSTR(SQLERRM,1,2000);
                    PKG_ON_ERROR.P_ML_ON_ERROR(SYSDATE, SYS_CONTEXT('USERENV','CURRENT_USER'), SYS_CONTEXT('USERENV','SESSION_USER'), g_OID, g_ERROR); 
                   
          END;                         
                    
  END;




 
 PROCEDURE SP_UPDATE_WORKSINFO(
   P_ORD NUMBER, 
   P_WORKS_ID NUMBER
 )
 IS
 
 g_ERROR VARCHAR2(2000);   -- 에러메시지
 g_OID VARCHAR2(50);-- 에러 오브젝트 아이디 (에러대상) 
 -------------------------------------------------------------------------------
-- 프로그램명 : 매칭저작물 상당한노력관련 제외처리      
--              PARAM : VP_ORD  차수
--                      P_WORKS_ID  대상저작물ID (ML_STAT_WORKS)

-- 스키마정보 : 상당한노력 진행 중 매칭건이 있는경우, 상당한노력 관련 정보 UPDATE
--              1. 대상저작물 : 상당한노력 대상제외 UPDATE   
--              2. 상당한 노력공고 : 삭제 여부 update
--              3. 거소불명 저작물 : 상당한노력값SYS_EFFORT_STAT_CD UPDATE                    
-- 수 행 시 기: 상당한노력 진행 중 매칭건이 있는경우                                                      
-------------------------------------------------------------------------------
  BEGIN
         BEGIN
            
            g_OID := 'ML_STAT_PROC_TARG_WORKS || UPDATE';
            
            /* 대상저작물 : 상당한노력 대상제외 UPDATE */
             UPDATE ML_STAT_PROC_TARG_WORKS
                SET STAT_PROC_YN = 'N'            
              WHERE ORD = P_ORD
                AND WORKS_ID = P_WORKS_ID
             ;
             
             g_OID := 'ML_STAT_ANUC_BORD || UPDATE';
             
             /* 상당한 노력공고 : 삭제 여부 update 처리*/
             UPDATE ML_STAT_ANUC_BORD
                SET DELT_YSNO = 'Y'
                   ,MODI_IDNT = 'SUPER_ADMIN'
                   ,MODI_DTTM = SYSDATE
              WHERE BORD_CD = 1
                AND WORKS_ID = P_WORKS_ID
             ;  
             
             g_OID := 'ML_STAT_WORKS || UPDATE';
              
             /* 거소불명 저작물 : 상당한노력값SYS_EFFORT_STAT_CD UPDATE  :: (4)완료*/         
             UPDATE ML_STAT_WORKS
                SET STAT_WORKS_YN = 'N'
                   ,SYST_EFFORT_STAT_CD = 4
                   ,SYST_EFFORT_STAT_DTTM = SYSDATE
              WHERE WORKS_ID = P_WORKS_ID
             ;
         
            
            COMMIT;
          
             EXCEPTION  
                  WHEN OTHERS THEN
                       
                    ROLLBACK;
                    
                    g_ERROR  := SUBSTR(SQLERRM,1,2000);
                    PKG_ON_ERROR.P_ML_ON_ERROR(SYSDATE, SYS_CONTEXT('USERENV','CURRENT_USER'), SYS_CONTEXT('USERENV','SESSION_USER'), g_OID, g_ERROR); 
              
          END;                         
                    
  END;


 PROCEDURE SP_INSERT_MAPP_INFO(
    P_MAPP_WORKS_DIVS_CD NUMBER,
    P_WORKS_ID NUMBER,
    P_MAPP_WORKS_ID_LIST VARCHAR2
 )
 IS
 
 g_ERROR VARCHAR2(2000);   -- 에러메시지
 g_OID VARCHAR2(50);-- 에러 오브젝트 아이디 (에러대상)
  
 V_CNT NUMBER := 1;
 V_IDX NUMBER := 0;
 V_LIST VARCHAR2(4000) := P_MAPP_WORKS_ID_LIST;
 V_DEL CHAR(1) := ',';  
 V_WORKSVALUE VARCHAR2(50);
 
-------------------------------------------------------------------------------
-- 프로그램명 : 매칭저작물 ID INSERT 프로시저        
--              PARAM : V_MAPP_WORKS_DIVS_CD 구분(1:저작권등록, 2:위탁저작물)
--                      V_WORKS_ID  대상저작물ID (ML_STAT_WORKS)
--                      V_MAPP_WORKS_ID_LIST 매칭저작물 목록 (구분자 ,) 

-- 스키마정보 : 상당한노력 진행 중 매칭건이 있는경우 법정허락저작물에 정보 INSERT                    
-- 수 행 시 기: 상당한노력 진행 중 매칭건이 있는경우                                                      
-------------------------------------------------------------------------------

 BEGIN
 
        
        BEGIN
           
            LOOP 
            
                g_OID := 'ML_STAT_WORKS_MAPP_INFO || INSERT';
                V_IDX := INSTR(V_LIST, V_DEL);
        
                IF  V_IDX>0 THEN
        
                    V_WORKSVALUE := SUBSTR(V_LIST,1,V_IDX -1);
            
                    V_LIST := SUBSTR(V_LIST, V_IDX+LENGTH(V_DEL));
            
            
                     INSERT
                      INTO  ML_STAT_WORKS_MAPP_INFO( WORKS_ID
                            ,WORKS_SEQN
                            ,MAPP_WORKS_DIVS_CD
                            ,MAPP_WORKS_ID
                            ,MAPP_DTTM)
                     VALUES(
                             P_WORKS_ID 
                            ,V_CNT
                            ,P_MAPP_WORKS_DIVS_CD
                            ,V_WORKSVALUE
                            ,SYSDATE 
                            )
                      ;
              
                     V_CNT := V_CNT+1;
        
                ELSE
        
                    V_WORKSVALUE := V_LIST;
            
                     INSERT
                      INTO  ML_STAT_WORKS_MAPP_INFO( WORKS_ID
                            ,WORKS_SEQN
                            ,MAPP_WORKS_DIVS_CD
                            ,MAPP_WORKS_ID
                            ,MAPP_DTTM)
                     VALUES(
                             P_WORKS_ID 
                            ,V_CNT
                            ,P_MAPP_WORKS_DIVS_CD
                            ,V_WORKSVALUE
                            ,SYSDATE 
                            )
                  ;
              
                      EXIT;
        
        
                END IF;
    
            END LOOP;
            
            
 
          EXCEPTION  
                  WHEN OTHERS THEN
                       
                    ROLLBACK;
                    
                    g_ERROR  := SUBSTR(SQLERRM,1,2000);
                    PKG_ON_ERROR.P_ML_ON_ERROR(SYSDATE, SYS_CONTEXT('USERENV','CURRENT_USER'), SYS_CONTEXT('USERENV','SESSION_USER'), g_OID, g_ERROR);          

        END;

 END;




/*===========================================================================*/
/*  메인 프로시져 시작                                                       */
/*===========================================================================*/


BEGIN

--Declare
V_CNT_1ST := 0;
V_CNT_2ND := 0;
V_CNT_3RD := 0;
V_CNT_NON_1ST := 0;
V_CNT_NON_2ND := 0; 
V_CNT_NON_3RD := 0; 
V_CNT_ANUC := 0;

    BEGIN    

        FOR I IN C_TARG_WORKS LOOP
        
                     
        
             V_RSLT_1ST_YN := 'N';
             V_RSLT_2ND_YN := 'N';
             V_RSLT_3RD_YN := 'N';
             
             V_ANUC_REGI_YN := 'N';
             V_ANUC_REGI_DTTM := '';
             
             V_RSLT_1ST_ID := '';
             V_RSLT_2ND_ID := '';
             V_RSLT_3RD_ID := '';
         
        
             
          /*==================================================*/
          /* 1차 매핑 정보 : 위탁관리 저작물 */
          /*==================================================*/
            SELECT F_ML_GET_MAPP_WORKS_ID(I.GENRE_CD, I.WORKS_TITLE)
              INTO V_RSLT_2ND_ID
              FROM DUAL;
         
         
            /*==================================================*/
            /* 1차매칭 성공*/
            /*==================================================*/
            IF V_RSLT_2ND_ID != '' OR V_RSLT_2ND_ID IS NOT NULL THEN
            
                 V_RSLT_2ND_YN := 'Y';
                 V_CNT_2ND := V_CNT_2ND+1;
                 V_ANUC_REGI_YN := 'N';
             
                    
                 SP_UPDATE_WORKSINFO( I.ORD, I.WORKS_ID); -- 관련 정보 UPDATE (대상저작물, 상당한노력 공고, 저작물정보:상당한노력값)  
                 
                 SP_INSERT_MAPP_INFO ( 2,I.WORKS_ID, V_RSLT_2ND_ID);  -- 법정허락 저작물 매핑정보 입력 : ML_STAT_WORKS_MAPP_INFO 
            
        
            ELSE 
            
            /*==================================================*/
            /* 1차매칭 실패*/
            /*==================================================*/ 
        
                V_CNT_NON_2ND := V_CNT_NON_2ND+1;  
            
                          
                --------------------------------------------------------------------------------
                -- 대상저작물이 [기승인 저작물]이 아닌경우 만 2차 매칭 진행 
                IF I.WORKS_MAPP_DIVS_CD<>3 THEN
                    
                   /*==================================================*/
                   /* 2차 매핑 정보 : 기승인저작물 */
                   /*==================================================*/
                    SELECT F_ML_GET_MAPP_CONFWORKS_ID(I.GENRE_CD, I.WORKS_TITLE)
                      INTO V_RSLT_3RD_ID
                      FROM DUAL;
                
                END IF;
                     
              
            
                /*==================================================*/
                 /* 2차매칭 성공*/
                 /*==================================================*/
                 IF V_RSLT_3RD_ID != '' OR V_RSLT_3RD_ID IS NOT NULL THEN
            
                     V_RSLT_3RD_YN := 'Y';
                     V_CNT_3RD := V_CNT_3RD+1;
                     
                     
                     SP_UPDATE_WORKSINFO( I.ORD, I.WORKS_ID);  -- 관련 정보 UPDATE (대상저작물, 상당한노력 공고, 저작물정보:상당한노력값)  
                 
                     SP_INSERT_MAPP_INFO ( 3,I.WORKS_ID, V_RSLT_1ST_ID);  -- 법정허락 저작물 매핑정보 입력 : ML_STAT_WORKS_MAPP_INFO 
            
            
         
                 /*==================================================*/
                 /* 2차매칭 실패*/
                 /*==================================================*/
                 ELSE 
                    
                    -- 대상저작물이 [기승인 저작물]이 아닌경우 만 2차 매칭 진행 
                    IF I.WORKS_MAPP_DIVS_CD<>3 THEN
                    
                        V_CNT_NON_3RD := V_CNT_NON_3RD+1;   
                
                    END IF;
                 
                 
                    /* 3차 매핑 정보 : 저작권 등록공보 */
                    SELECT F_ML_GET_MAPP_CROSWORKS_ID(I.GENRE_CD, I.WORKS_TITLE)
                      INTO V_RSLT_1ST_ID
                      FROM DUAL;
              
            
                    /*==================================================*/
                     /* 3차매칭 성공*/
                     /*==================================================*/
                     IF V_RSLT_1ST_ID != '' OR V_RSLT_1ST_ID IS NOT NULL THEN
            
                         V_RSLT_1ST_YN := 'Y';
                         V_CNT_1ST := V_CNT_1ST+1;
                     
                     
                         SP_UPDATE_WORKSINFO( I.ORD, I.WORKS_ID);  -- 관련 정보 UPDATE (대상저작물, 상당한노력 공고, 저작물정보:상당한노력값)  
                 
                         SP_INSERT_MAPP_INFO ( 1,I.WORKS_ID, V_RSLT_1ST_ID);  -- 법정허락 저작물 매핑정보 입력 : ML_STAT_WORKS_MAPP_INFO 
            
         
                     /*==================================================*/
                     /* 3차매칭 실패*/
                     /*==================================================*/
                     ELSE 
         
                        V_CNT_NON_1ST := V_CNT_NON_1ST+1;
                        
                        IF ( I.WORKS_MAPP_DIVS_CD<>3 AND  I.STAT_PROC_CONT =0) THEN
             
                             V_ANUC_REGI_YN := 'Y';
                             V_CNT_ANUC := V_CNT_ANUC+1;
             
                          END IF;
                
             
                     END IF;
                    
                    
                 
                                 
             
                 END IF;
  
            --------------------------------------------------------------------------------
        
            END IF;
            
         
             /* 상당한노력 결과 INSERT */
             -- START -----------------------------------------------------------------     
              IF ( I.WORKS_MAPP_DIVS_CD<>3 AND  I.STAT_PROC_CONT =0) THEN            
                    
                     /* 최초 상당한노력 공고 있는경우*/
                      V_ANUC_REGI_DTTM := I.ANUC_REGI_DTTM;
                      V_ANUC_REGI_YN := 'Y';
                               
              ELSE 
                     V_ANUC_REGI_DTTM := I.ANUC_REGI_DTTM;
                     V_ANUC_REGI_YN := 'N';
             
              END IF; 
              
              
              SP_INSERT_SUBLOG(  I.ORD
                                ,P_YYYYMM
                                ,I.WORKS_MAPP_DIVS_CD
                                ,I.WORKS_ID
                                ,V_RSLT_1ST_YN
                                ,V_RSLT_2ND_YN
                                ,V_RSLT_3RD_YN
                                ,V_ANUC_REGI_YN 
                                ,V_RSLT_1ST_ID 
                                ,V_RSLT_2ND_ID 
                                ,V_RSLT_3RD_ID 
                                ,V_ANUC_REGI_DTTM
                               ) ;
              -- END -----------------------------------------------------------------
              
              g_OID := 'ML_STAT_PROC_TARG_WORKS || UPDATE';
                 
              /* 대상저작물 STAT 값 UPDATE */
              UPDATE ML_STAT_PROC_TARG_WORKS
                 SET STAT_PROC_CONT = (I.STAT_PROC_CONT)+1        
               WHERE ORD = I.ORD
                 AND WORKS_ID = I.WORKS_ID
             ;
             
             COMMIT;
         
         
        END LOOP;
    
        
        g_OID := 'ML_STAT_PROC_ALL_LOG || UPDATE';
        
        /* 처리 로그 UPDATE : 처리결과 - 프로시저 실행단위 */
        UPDATE ML_STAT_PROC_ALL_LOG AL
           SET ( RSLT_CONT_1ST_YS 
                ,RSLT_CONT_1ST_NO 
                ,RSLT_CONT_2ND_YS 
                ,RSLT_CONT_2ND_NO
                ,RSLT_CONT_3RD_YS
                ,RSLT_CONT_3RD_NO
                ,ANUC_CONT 
                           )  = (SELECT NVL(RSLT_CONT_1ST_YS,0)+V_CNT_1ST
                                       ,NVL(RSLT_CONT_1ST_NO,0)+V_CNT_NON_1ST
                                       ,NVL(RSLT_CONT_2ND_YS,0)+V_CNT_2ND
                                       ,NVL(RSLT_CONT_2ND_NO,0)+V_CNT_NON_2ND
                                       ,NVL(RSLT_CONT_3RD_YS,0)+V_CNT_3RD
                                       ,NVL(RSLT_CONT_3RD_NO,0)+V_CNT_NON_3RD
                                       ,NVL(ANUC_CONT,0)+V_CNT_ANUC 
                                  FROM ML_STAT_PROC_ALL_LOG
                                 WHERE ORD = P_ORD
                                   AND YYYYMM = P_YYYYMM
                                   AND WORKS_DIVS_CD = P_WORKS_DIVS_CD)
          WHERE ORD = P_ORD
            AND YYYYMM = P_YYYYMM
            AND WORKS_DIVS_CD = P_WORKS_DIVS_CD
            ;
        
        COMMIT;
     
         /* 처리 로그 UPDATE : 종료시간, STAT_CD(4)-완료*/
        IF P_TOTCNT = P_TO_ROW_NO THEN
            
            g_OID := 'ML_STAT_PROC_ALL_LOG || UPDATE';
        
            UPDATE ML_STAT_PROC_ALL_LOG
               SET END_DTTM = SYSDATE
                  ,STAT_CD = 4
            WHERE ORD = P_ORD
              AND YYYYMM = P_YYYYMM
              AND WORKS_DIVS_CD = P_WORKS_DIVS_CD
            ;
        
            COMMIT;
                                  
        END IF;
    
      
   END;
   
   EXCEPTION
    /*
       WHEN ERR_INSERT() THEN
            
            ROLLBACK;
            g_ERROR  := SUBSTR(SQLERRM,1,2000);
            PKG_ON_ERROR.P_ML_ON_ERROR(SYSDATE, SYS_CONTEXT('USERENV','CURRENT_USER'), SYS_CONTEXT('USERENV','SESSION_USER'), g_OID, g_ERROR);
    */
       WHEN OTHERS THEN
          ROLLBACK;
                    
          g_ERROR  := SUBSTR(SQLERRM,1,2000);
          PKG_ON_ERROR.P_ML_ON_ERROR(SYSDATE, SYS_CONTEXT('USERENV','CURRENT_USER'), SYS_CONTEXT('USERENV','SESSION_USER'), g_OID, g_ERROR); 
        

END;
/