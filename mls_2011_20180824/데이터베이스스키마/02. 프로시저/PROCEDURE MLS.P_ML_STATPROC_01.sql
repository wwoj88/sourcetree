CREATE OR REPLACE PROCEDURE MLS.P_ML_STATPROC_01(
    P_ORD NUMBER,
    P_YYYYMM NUMBER,
    P_WORKS_DIVS_CD NUMBER,
    P_TOTCNT NUMBER,
    P_FROM_ROW_NO NUMBER,
    P_TO_ROW_NO NUMBER
    )
IS
  
  g_ERROR VARCHAR2(2000);   -- 에러메시지
  g_OID VARCHAR2(50);-- 에러 오브젝트 아이디 (에러대상)
-------------------------------------------------------------------------------
-- 프로그램명 : 상당한노력 진행 프로시저        
--              (상당한노력공고) 
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 11월 08일                                               
-- 수정  일자 :                                           
-- 스키마정보 : 수집저작물에 상당한 노력(01.상당한노력) 을 진행한다. 
                      
-- 수 행 시 기: 관리자 실행                                                       
-------------------------------------------------------------------------------

CURSOR C_TARG_WORKS IS

SELECT *
  FROM (
        SELECT *
          FROM (
            SELECT /*+ index(W ML_STAT_PROC_TARG_WORKS_PK) */ 
                   ROWNUM RNUM, W.*
              FROM MLS.ML_STAT_PROC_TARG_WORKS W
             WHERE ORD = P_ORD
               AND WORKS_MAPP_DIVS_CD = P_WORKS_DIVS_CD
          ) 
          WHERE 1=1
            AND RNUM  BETWEEN P_FROM_ROW_NO AND P_TO_ROW_NO
        )
WHERE WORKS_MAPP_DIVS_CD<>3
  AND STAT_PROC_CONT =0
 ;

V_CNT_ANUC NUMBER; 
V_ANUC_REGI_YN CHAR(1); 



BEGIN

    /* 01. 처리 로그 UPDATE : 시작시간, STAT_CD(3)-진행중*/
    IF P_FROM_ROW_NO = 1 THEN
        
        UPDATE ML_STAT_PROC_ALL_LOG
           SET START_DTTM = SYSDATE
              ,STAT_CD = 3
        WHERE ORD = P_ORD
          AND YYYYMM = P_YYYYMM
          AND WORKS_DIVS_CD = P_WORKS_DIVS_CD
        ;
        
        COMMIT;
                                  
    END IF;
    

    FOR I IN C_TARG_WORKS LOOP
        
         V_ANUC_REGI_YN := 'N';
         
         
          /* 02. 상당한노력 공고 INSERT :: IF( !법정허락 && 상당한노력 최초 PROC ) */
          IF ( I.WORKS_MAPP_DIVS_CD<>3 AND  I.STAT_PROC_CONT =0) THEN
             
             -- 상당한 노력 공고 프로시저 호출 (EXCEPTION 처리 필요) 
             P_ML_ANUCBOARD_INSERT(I.ORD, I.WORKS_ID, I.WORKS_DIVS_CD);
             
             V_ANUC_REGI_YN := 'Y';
             V_CNT_ANUC := V_CNT_ANUC+1;
             
             /* 대상저작물 STAT 값 UPDATE */
             UPDATE ML_STAT_PROC_TARG_WORKS
                SET ANUC_REGI_DTTM = SYSDATE
                 --  ,STAT_PROC_YN = DECODE (I.STAT_PROC_CONT, 2,'N', 'Y')               
              WHERE ORD = I.ORD
                AND WORKS_ID = I.WORKS_ID;
             
             
             /* 상당한노력 결과 INSERT  -- P_ML_GET_STAT_PROCORD 에서 선처리 하도록 함*/ 
             /*
              INSERT INTO MLS.ML_STAT_PROC_SUB_LOG
                       (  ORD
                        , YYYYMM
                        , WORKS_DIVS_CD
                        , WORKS_ID
                        , PROC_DTTM
                        , RSLT_1ST_YN
                        , RSLT_2ND_YN
                        , ANUC_REGI_YN
                        , RSLT_1ST_WORKS_ID 
                        , RSLT_2ND_WORKS_ID 
                        , ANUC_REGI_DTTM 
                       ) 
                  VALUES (  I.ORD
                           ,P_YYYYMM
                           ,I.WORKS_MAPP_DIVS_CD
                           ,I.WORKS_ID
                           ,SYSDATE
                           ,V_RSLT_1ST_YN
                           ,V_RSLT_2ND_YN
                           ,V_ANUC_REGI_YN
                           ,V_RSLT_1ST_ID 
                           ,V_RSLT_2ND_ID
                           ,SYSDATE
                          );
          
          
             */     
             
          END IF;
    END LOOP;
    
    
    g_OID := 'ML_STAT_PROC_ALL_LOG || UPDATE';
        
    /* 처리 로그 UPDATE : 처리결과 - 프로시저 실행단위 */
    
    UPDATE ML_STAT_PROC_ALL_LOG AL
       SET ( ANUC_CONT)  = (SELECT NVL(ANUC_CONT,0)+V_CNT_ANUC
                              FROM ML_STAT_PROC_ALL_LOG
                             WHERE ORD = P_ORD
                               AND YYYYMM = P_YYYYMM
                               AND WORKS_DIVS_CD = P_WORKS_DIVS_CD)
      WHERE ORD = P_ORD
        AND YYYYMM = P_YYYYMM
        AND WORKS_DIVS_CD = P_WORKS_DIVS_CD
        ;
      
       
 
   EXCEPTION
       WHEN OTHERS THEN
          ROLLBACK;
                    
          g_ERROR  := SUBSTR(SQLERRM,1,2000);
          PKG_ON_ERROR.P_ML_ON_ERROR(SYSDATE, SYS_CONTEXT('USERENV','CURRENT_USER'), SYS_CONTEXT('USERENV','SESSION_USER'), g_OID, g_ERROR); 

END;
/