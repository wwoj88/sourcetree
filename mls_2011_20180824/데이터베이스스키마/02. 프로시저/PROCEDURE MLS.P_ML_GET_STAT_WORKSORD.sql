CREATE OR REPLACE PROCEDURE MLS.P_ML_GET_STAT_WORKSORD
IS

-------------------------------------------------------------------------------
-- 프로그램명 : 법정허락 전환 대상물 수집 프로시저 
--                   대상 : 상당한노력 3회이상 + 상당한노력 공고 90일 이상
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 9월 15일                                              
-- 수정  일자 :                                           
-- 스키마정보 : 법정허락 전환 대상물 수집
                      
-- 수 행 시 기: 관리자 실행                                                       
-------------------------------------------------------------------------------

    V_CNT_YN NUMBER;
    V_CNT NUMBER;
    V_CNT_SUB NUMBER;
    V_STAT_ORD NUMBER;
    

BEGIN

    /* 수집완료, 진행중(STAT_CD = 2,3)인 건이 있는경우 수집불가*/
    SELECT COUNT(1)
      INTO V_CNT_YN
      FROM ML_STAT_WORKS_ALL_LOG
     WHERE STAT_CD IN (2,3)
    ;
     
         
    IF V_CNT_YN = 0 THEN
        
    
    
    
        /* 기존 미진행(수집완료)건 유무에 따른 수집프로세스 분기 */
        SELECT COUNT(1)
         INTO V_CNT
          FROM ML_STAT_WORKS_ALL_LOG
         WHERE STAT_CD = 2
         ;
    
        IF V_CNT > 0  THEN
    
        /* 기존 수집건수에 추가*/
        
            /* GET 진행차수 */
            SELECT NVL(MAX(STAT_ORD),0) /*차수*/
              INTO V_STAT_ORD
              FROM ML_STAT_WORKS_ALL_LOG
              ;
              
            /* 대상물 진행차수 UPDATE */
            UPDATE ML_STAT_PROC_TARG_WORKS
               SET STAT_ORD =  V_STAT_ORD /*차수*/
             WHERE STAT_PROC_YN = 'Y'
               AND STAT_PROC_CONT >3
               AND NVL(STAT_CHNG_YN,'N') = 'N'
               AND SYSDATE - ANUC_REGI_DTTM >90
           ;
           
            SELECT COUNT(1)
              INTO V_CNT_SUB
              FROM ML_STAT_WORKS_ALL_LOG
             WHERE STAT_CD = 2
            ;
           
            IF V_CNT_SUB >0 THEN
        
                /* 처리 로그 UPDATE */
                UPDATE ML_STAT_WORKS_ALL_LOG
                   SET TARG_WORKS_CONT = ( SELECT COUNT(1)
                                             FROM ML_STAT_PROC_TARG_WORKS
                                            WHERE STAT_ORD = V_STAT_ORD )
                     ,WORKS_DIVS_CONT_01 = (SELECT COUNT(1)
                                              FROM ML_STAT_PROC_TARG_WORKS
                                             WHERE STAT_ORD = V_STAT_ORD 
                                               AND WORKS_MAPP_DIVS_CD = 1 /* 미분배 */ )
                    ,WORKS_DIVS_CONT_02 = (SELECT COUNT(1)
                                             FROM ML_STAT_PROC_TARG_WORKS
                                            WHERE STAT_ORD = V_STAT_ORD 
                                              AND WORKS_MAPP_DIVS_CD = 2/* 거소불명 */ )
                 WHERE STAT_ORD = V_STAT_ORD
                   AND STAT_CD = 2
                 ;
             
            ELSE
        
                /* 처리 로그 INSERT */
               INSERT
                INTO ML_STAT_WORKS_ALL_LOG (STAT_ORD, YYYYMM, TARG_WORKS_CONT, WORKS_DIVS_CONT_01, WORKS_DIVS_CONT_02, STAT_CD)
              VALUES (V_STAT_ORD, TO_CHAR(SYSDATE, 'YYYYMM')
                    ,(SELECT COUNT(1)
                        FROM ML_STAT_PROC_TARG_WORKS
                       WHERE STAT_ORD = V_STAT_ORD /* 차수 */ )
                    ,(SELECT COUNT(1)
                        FROM ML_STAT_PROC_TARG_WORKS
                       WHERE STAT_ORD = V_STAT_ORD 
                         AND WORKS_MAPP_DIVS_CD = 1 /* 미분배 */ )
                    ,(SELECT COUNT(1)
                        FROM ML_STAT_PROC_TARG_WORKS
                       WHERE STAT_ORD = V_STAT_ORD 
                         AND WORKS_DIVS_CD = 2/* 거소불명 */ )
                    , 2)
                ; 
        
        
            END IF;
        
        
         
         
    
        ELSE
    
        /* 신규 수집*/
        
            /* GET 진행차수 */
            SELECT NVL(MAX(STAT_ORD),0)+1 /*차수*/
              INTO V_STAT_ORD
              FROM ML_STAT_WORKS_ALL_LOG
            ;
          
            /* 대상물 진행차수 UPDATE */
            UPDATE ML_STAT_PROC_TARG_WORKS
               SET STAT_ORD =  V_STAT_ORD /*차수*/
             WHERE STAT_PROC_YN = 'Y'
               AND STAT_PROC_CONT >3
               AND NVL(STAT_CHNG_YN,'N') = 'N'
               AND SYSDATE - ANUC_REGI_DTTM >90
           ;
           
           
           SELECT COUNT(1) /*차수*/
              INTO V_CNT_SUB
              FROM ML_STAT_PROC_TARG_WORKS
             WHERE STAT_ORD =  V_STAT_ORD /*차수*/
           ;
           
           IF V_CNT_SUB >0 THEN 
           
                /* 처리 로그 INSERT */
                INSERT
                  INTO ML_STAT_WORKS_ALL_LOG (STAT_ORD, YYYYMM, TARG_WORKS_CONT, WORKS_DIVS_CONT_01, WORKS_DIVS_CONT_02, STAT_CD)
                VALUES (V_STAT_ORD, TO_CHAR(SYSDATE, 'YYYYMM')
                        ,(SELECT COUNT(1)
                            FROM ML_STAT_PROC_TARG_WORKS
                           WHERE STAT_ORD = V_STAT_ORD /* 차수 */ )
                        ,(SELECT COUNT(1)
                            FROM ML_STAT_PROC_TARG_WORKS
                           WHERE STAT_ORD = V_STAT_ORD 
                             AND WORKS_MAPP_DIVS_CD = 1 /* 미분배 */ )
                        ,(SELECT COUNT(1)
                            FROM ML_STAT_PROC_TARG_WORKS
                           WHERE STAT_ORD = V_STAT_ORD 
                             AND WORKS_MAPP_DIVS_CD = 2/* 거소불명 */ )
                        , 2)
                ; 
            
          END IF;
            
            
    
        END IF;
    
       
    
    
         COMMIT;
         
     END IF;
      

   EXCEPTION
       WHEN OTHERS THEN
          ROLLBACK;

END;
/