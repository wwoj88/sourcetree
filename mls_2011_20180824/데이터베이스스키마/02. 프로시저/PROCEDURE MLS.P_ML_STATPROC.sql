CREATE OR REPLACE PROCEDURE MLS.P_ML_STATPROC(
    P_ORD NUMBER,
    P_YYYYMM NUMBER,
    P_WORKS_DIVS_CD NUMBER,
    P_TOTCNT NUMBER,
    P_FROM_ROW_NO NUMBER,
    P_TO_ROW_NO NUMBER
    )
IS

-------------------------------------------------------------------------------
-- 프로그램명 : 상당한노력 진행 프로시저        
--                   1차매칭 (저작권등록), 2차매칭(위탁관리저작물), 상당한노력공고
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 9월 15일                                              
-- 수정  일자 :                                           
-- 스키마정보 : 수집저작물에 상당한 노력을 진행한다. 
                      
-- 수 행 시 기: 관리자 실행                                                       
-------------------------------------------------------------------------------

CURSOR C_TARG_WORKS IS

SELECT *
  FROM (
    SELECT ROWNUM RNUM, W.*
      FROM MLS.ML_STAT_PROC_TARG_WORKS W
     WHERE ORD = P_ORD
       AND WORKS_MAPP_DIVS_CD = P_WORKS_DIVS_CD
  ) 
  WHERE 1=1
    AND RNUM  BETWEEN P_FROM_ROW_NO AND P_TO_ROW_NO
 ;

V_CNT_1ST NUMBER;
V_CNT_2ND NUMBER;
V_CNT_NON_1ST NUMBER;
V_CNT_NON_2ND NUMBER;
V_CNT_ANUC NUMBER;

V_RSLT_1ST_YN CHAR(1);  
V_RSLT_2ND_YN CHAR(1);  
V_ANUC_REGI_YN CHAR(1); 

V_RSLT_1ST_ID VARCHAR2(4000);
V_RSLT_2ND_ID VARCHAR2(4000);




BEGIN

    /* 처리 로그 UPDATE : 시작시간, STAT_CD(3)-진행중*/
    IF P_FROM_ROW_NO = 1 THEN
        
        UPDATE ML_STAT_PROC_ALL_LOG
           SET START_DTTM = SYSDATE
              ,STAT_CD = 3
        WHERE ORD = P_ORD
          AND YYYYMM = P_YYYYMM
          AND WORKS_DIVS_CD = P_WORKS_DIVS_CD
        ;
        
        COMMIT;
                                  
    END IF;
    
    
    V_CNT_1ST := 0;
    V_CNT_2ND := 0;
    V_CNT_NON_1ST := 0;
    V_CNT_NON_2ND := 0; 
    V_CNT_ANUC := 0;
    

    FOR I IN C_TARG_WORKS LOOP
        
        /* 1차 매핑 정보 : 저작권 등록공보 */
        SELECT F_ML_GET_MAPP_CROSWORKS_ID(I.GENRE_CD, I.WORKS_TITLE)
          INTO V_RSLT_1ST_ID
          FROM DUAL;
      
        
         V_RSLT_1ST_YN := 'N';
         V_RSLT_2ND_YN := 'N';
         V_ANUC_REGI_YN := 'N';
         
         
         
         /*==================================================*/
         /* 1차매칭 성공*/
         /*==================================================*/
         IF V_RSLT_1ST_ID != '' OR V_RSLT_1ST_ID IS NOT NULL THEN
            
             V_RSLT_1ST_YN := 'Y';
             V_CNT_1ST := V_CNT_1ST+1;
             
             /* 대상저작물 : 상당한노력 대상제외 UPDATE */
             UPDATE ML_STAT_PROC_TARG_WORKS
                SET STAT_PROC_YN = 'N'            
              WHERE ORD = I.ORD
                AND WORKS_ID = I.WORKS_ID
             ;
             
             
             /* 거소불명 저작물 : 상당한노력값SYS_EFFORT_STAT_CD UPDATE */
             --IF I.WORKS_DIVS_CD = 4 THEN
                 
                 UPDATE ML_STAT_WORKS
                    SET STAT_WORKS_YN = 'N'
                       ,SYST_EFFORT_STAT_CD = 4
                       ,SYST_EFFORT_STAT_DTTM = SYSDATE
                  WHERE WORKS_ID = I.WORKS_ID
                 ;
                 
            -- END IF;
            
            /* 법정허락 저작물 매핑정보 입력 : ML_STAT_WORKS_MAPP_INFO */
            /*     P_ML_STAT_MAPP_INFO_INSERT ( 저작권등록 ,법정허락저작물ID, 매칭아이디; */
            P_ML_STAT_MAPP_INFO_INSERT ( 1,I.WORKS_ID, V_RSLT_1ST_ID);
         
         /*==================================================*/
         /* 1차매칭 실패*/
         /*==================================================*/
         ELSE 
         
            V_CNT_NON_1ST := V_CNT_NON_1ST+1;
            
            /* 2차 매핑 정보 : 위탁관리 저작물 */
            SELECT F_ML_GET_MAPP_WORKS_ID(I.GENRE_CD, I.WORKS_TITLE)
              INTO V_RSLT_2ND_ID
              FROM DUAL;
              
            
            /*==================================================*/
            /* 2차매칭 성공*/
            /*==================================================*/
            IF V_RSLT_2ND_ID != '' OR V_RSLT_2ND_ID IS NOT NULL THEN
            
             V_RSLT_2ND_YN := 'Y';
             V_CNT_2ND := V_CNT_2ND+1;
             
             /* 대상저작물 : 상당한노력 대상제외 UPDATE */
             UPDATE ML_STAT_PROC_TARG_WORKS
                SET STAT_PROC_YN = 'N'            
              WHERE ORD = I.ORD
                AND WORKS_ID = I.WORKS_ID;
                
                
                /* 거소불명 저작물 : 상당한노력값SYS_EFFORT_STAT_CD UPDATE */
                -- IF I.WORKS_DIVS_CD = 4 THEN
                 
                     UPDATE ML_STAT_WORKS
                        SET STAT_WORKS_YN = 'N'
                           ,SYST_EFFORT_STAT_CD = 4
                           ,SYST_EFFORT_STAT_DTTM = SYSDATE
                      WHERE WORKS_ID = I.WORKS_ID
                     ;
                 
               --  END IF;
               
               /* 법정허락 저작물 매핑정보 입력 : ML_STAT_WORKS_MAPP_INFO */
               /*     P_ML_STAT_MAPP_INFO_INSERT ( 위탁관리저작물 ,법정허락저작물ID, 매칭아이디; */
                P_ML_STAT_MAPP_INFO_INSERT ( 2,I.WORKS_ID, V_RSLT_2ND_ID);
            
            /*==================================================*/
            /* 2차매칭 실패*/
            /*==================================================*/ 
            ELSE 
                
               V_CNT_NON_2ND := V_CNT_NON_2ND+1; 
               
               
                  -- 상당한노력 공고 INSERT 
                  IF ( I.WORKS_MAPP_DIVS_CD<>3 AND  I.STAT_PROC_CONT =0) THEN
             
                     V_ANUC_REGI_YN := 'Y';
                     V_CNT_ANUC := V_CNT_ANUC+1;
             
                     -- 상당한 노력 공고 프로시저 호출 (EXCEPTION 처리 필요) 
                     P_ML_ANUCBOARD_INSERT(I.WORKS_ID, I.WORKS_DIVS_CD);
             
                     /* 상당한노력 결과 INSERT */
                    
                      INSERT INTO MLS.ML_STAT_PROC_SUB_LOG
                               (  ORD
                                , YYYYMM
                                , WORKS_DIVS_CD
                                , WORKS_ID
                                , PROC_DTTM
                                , RSLT_1ST_YN
                                , RSLT_2ND_YN
                                , ANUC_REGI_YN
                                , RSLT_1ST_WORKS_ID 
                                , RSLT_2ND_WORKS_ID 
                                , ANUC_REGI_DTTM 
                               ) 
                          VALUES (  I.ORD
                                   ,P_YYYYMM
                                   ,I.WORKS_MAPP_DIVS_CD
                                   ,I.WORKS_ID
                                   ,SYSDATE
                                   ,V_RSLT_1ST_YN
                                   ,V_RSLT_2ND_YN
                                   ,V_ANUC_REGI_YN
                                   ,V_RSLT_1ST_ID 
                                   ,V_RSLT_2ND_ID
                                   ,SYSDATE
                                  );
          
          
                  
             
                  END IF;
             
            END IF;
         
         
             
         END IF;
         
         
         --IF I.STAT_PROC_CONT >0 THEN
         -- 상당한노력 공고 없는경우 LOG 처리
         IF V_ANUC_REGI_YN = 'N' THEN
               
         /* 상당한노력 결과 INSERT */
          
          INSERT INTO MLS.ML_STAT_PROC_SUB_LOG
                   (  ORD
                    , YYYYMM
                    , WORKS_DIVS_CD
                    , WORKS_ID
                    , PROC_DTTM
                    , RSLT_1ST_YN
                    , RSLT_2ND_YN
                    , ANUC_REGI_YN
                    , RSLT_1ST_WORKS_ID 
                    , RSLT_2ND_WORKS_ID 
                   -- , ANUC_REGI_DTTM 
                   ) 
              VALUES (  I.ORD
                       ,P_YYYYMM
                       ,I.WORKS_MAPP_DIVS_CD
                       ,I.WORKS_ID
                       ,SYSDATE
                       ,V_RSLT_1ST_YN
                       ,V_RSLT_2ND_YN
                       ,V_ANUC_REGI_YN
                       ,V_RSLT_1ST_ID 
                       ,V_RSLT_2ND_ID
                    --
                      );
                             
         END IF;
         
            
         /* 대상저작물 STAT 값 UPDATE */
         UPDATE ML_STAT_PROC_TARG_WORKS
            SET STAT_PROC_CONT = (I.STAT_PROC_CONT)+1
               ,ANUC_REGI_DTTM = DECODE(V_ANUC_REGI_YN,'Y',SYSDATE, ANUC_REGI_DTTM)
             --  ,STAT_PROC_YN = DECODE (I.STAT_PROC_CONT, 2,'N', 'Y')               
          WHERE ORD = I.ORD
            AND WORKS_ID = I.WORKS_ID;
             
             
          COMMIT;
          
           
    END LOOP;
    
    
    /* 처리 로그 UPDATE : 처리결과 - 프로시저 실행단위 */
    UPDATE ML_STAT_PROC_ALL_LOG AL
       SET ( RSLT_CONT_1ST_YS 
            ,RSLT_CONT_1ST_NO 
            ,RSLT_CONT_2ND_YS 
            ,RSLT_CONT_2ND_NO
            ,ANUC_CONT )  = (SELECT NVL(RSLT_CONT_1ST_YS,0)+V_CNT_1ST
                                   ,NVL(RSLT_CONT_1ST_NO,0)+V_CNT_NON_1ST
                                   ,NVL(RSLT_CONT_2ND_YS,0)+V_CNT_2ND
                                   ,NVL(RSLT_CONT_2ND_NO,0)+V_CNT_NON_1ST
                                   ,NVL(ANUC_CONT,0)+V_CNT_ANUC
                              FROM ML_STAT_PROC_ALL_LOG
                             WHERE ORD = P_ORD
                               AND YYYYMM = P_YYYYMM
                               AND WORKS_DIVS_CD = P_WORKS_DIVS_CD)
      WHERE ORD = P_ORD
        AND YYYYMM = P_YYYYMM
        AND WORKS_DIVS_CD = P_WORKS_DIVS_CD
        ;
        
     COMMIT;
    
    
    /* 처리 로그 UPDATE : 종료시간, STAT_CD(4)-완료*/
    IF P_TOTCNT = P_TO_ROW_NO THEN
        
        UPDATE ML_STAT_PROC_ALL_LOG
           SET END_DTTM = SYSDATE
              ,STAT_CD = 4
        WHERE ORD = P_ORD
          AND YYYYMM = P_YYYYMM
          AND WORKS_DIVS_CD = P_WORKS_DIVS_CD
        ;
        
        COMMIT;
                                  
    END IF;
    
    
      
 
   EXCEPTION
       WHEN OTHERS THEN
          --SP_STAT_MATCH_INSERT_LOG(V_MAPP_TOT_SEQN, '프로시저 에러', 0);
          ROLLBACK; 

END;
/