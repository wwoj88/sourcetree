CREATE OR REPLACE TRIGGER MLS.TR_ML_STAT_WORKS
AFTER INSERT OR UPDATE
ON MLS.ML_STAT_WORKS 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN

-------------------------------------------------------------------------------
-- 프로그램명 : 상당한노력 진행 대상저작물 수집        
--              (미분배보상금 대상저작물, 상당한노력신청 저작물) 
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 09월 10일                                              
-- 수정  일자 :                                           
                      
-- 수 행 시 기: 트리거                                                      
-------------------------------------------------------------------------------

    IF INSERTING THEN
        
        IF ( :NEW.SYST_EFFORT_STAT_CD =1) THEN
            
            INSERT INTO ML_STAT_PROC_TARG_WORKS
                       ( ORD
                        ,WORKS_ID
                        ,GENRE_CD
                        ,WORKS_TITLE
                        ,WORKS_DIVS_CD
                        ,WORKS_MAPP_DIVS_CD 
                        ,STAT_PROC_YN 
                        ,STAT_PROC_CONT      
                       )
                VALUES( 0
                       ,:NEW.WORKS_ID
                       ,:NEW.GENRE_CD
                       ,UPPER(REPLACE(:NEW.WORKS_TITLE,' ',''))
                       ,:NEW.WORKS_DIVS_CD
                       ,DECODE(:NEW.WORKS_DIVS_CD, 4, 2, 6,3, 1)        
                       ,'Y'  
                       ,0 
                        )
            ;
            
        END IF;


    ELSIF UPDATING THEN
    
        IF ( :OLD.SYST_EFFORT_STAT_CD =0 AND :NEW.SYST_EFFORT_STAT_CD =1 ) THEN
             
            
           
            
            INSERT INTO ML_STAT_PROC_TARG_WORKS
                   ( ORD
                    ,WORKS_ID
                    ,GENRE_CD
                    ,WORKS_TITLE
                    ,WORKS_DIVS_CD
                    ,WORKS_MAPP_DIVS_CD   
                    ,STAT_PROC_YN      
                    ,STAT_PROC_CONT     
                   )
            VALUES( 0
                   ,:NEW.WORKS_ID
                   ,:NEW.GENRE_CD
                   ,UPPER(REPLACE(:NEW.WORKS_TITLE,' ',''))
                   ,:NEW.WORKS_DIVS_CD
                   ,DECODE(:NEW.WORKS_DIVS_CD, 4, 2,  6,3, 1)    
                   ,'Y'   
                   ,0    
                    )
            ;
                          
            
        END IF ;


    END IF;
END;
/
