CREATE OR REPLACE TRIGGER MLS.TR_ML_COMM_WORKS
AFTER INSERT OR UPDATE
ON MLS.ML_COMM_WORKS
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW


-------------------------------------------------------------------------------
-- 프로그램명 : 01.(ML_STAT_MAPP_WORKS) 매핑대상저작물 수집 (기타장르 제외_위탁관리 저작물) 
--              02.(ML_STAT_MAPP_WORKS.USE_YN) 매핑대상저작물 _사용여부 수정 
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 :                                            
-- 수정  일자 :                                           
                      
-- 수 행 시 기: 트리거                                                      
-------------------------------------------------------------------------------


BEGIN

    IF INSERTING THEN
       
        IF ( :NEW.GENRE_CD <> 99   AND NVL(:NEW.NATION_CD,1)=1 )THEN
            
            INSERT INTO ML_STAT_MAPP_WORKS
                       ( WORKS_ID
                        ,GENRE_CD
                        ,WORKS_TITLE
                        ,USE_YN           
                       )
                VALUES( :NEW.WORKS_ID
                       ,FC_GET_GENRE_CD(:NEW.GENRE_CD)
                       ,UPPER(REPLACE(:NEW.WORKS_TITLE, ' ',''))
                       ,DECODE(:NEW.COMM_MGNT_CD, 0, 'N', 3, 'N', 'Y')           
                        )
            ;
         
        END IF;   

    ELSIF UPDATING THEN
    
        IF ( :OLD.COMM_MGNT_CD <> :NEW.COMM_MGNT_CD ) THEN
             
            UPDATE ML_STAT_MAPP_WORKS
               SET USE_YN = DECODE(:NEW.COMM_MGNT_CD, 0, 'N', 3, 'N', 'Y')
              WHERE WORKS_ID = :NEW.WORKS_ID
               ;         
                        
        END IF ;

    END IF;
    
END;
/
