CREATE OR REPLACE TRIGGER MLS.TR_ML_STAT_APPLY_WORKS
AFTER UPDATE
ON MLS.ML_STAT_APPLY_WORKS
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE 
    
    V_APPLY_DIV_CD NUMBER;   
    V_CONT NUMBER;
    V_GENRE_CD NUMBER;
    V_WORKS_ID NUMBER;

-------------------------------------------------------------------------------
-- 프로그램명 : 01.(ML_STAT_CONF_WORKS) 법정허락 승인저작물 -> 기승인저작물 수집 (DIVS_CD : 6 ) 
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 :                                            
-- 수정  일자 :                                           
                      
-- 수 행 시 기: 트리거                                                      
-------------------------------------------------------------------------------
    
BEGIN
    
  IF UPDATING THEN
    
   
        /* 현재 승인인 경우*/ 
            IF ( (:OLD.STAT_RSLT_CD IS NULL OR :OLD.STAT_RSLT_CD <> 2) AND :NEW.STAT_RSLT_CD = 2) THEN
        
            SELECT APPLY_DIVS_CD
              INTO V_APPLY_DIV_CD
              FROM ML_STAT_APPLICATION
             WHERE APPLY_WRITE_YMD = :NEW.APPLY_WRITE_YMD
               AND APPLY_WRITE_SEQ = :NEW.APPLY_WRITE_SEQ
            ;
            
            
            /* 법정허락 대상 저작물이 아닌경우*/
            IF (V_APPLY_DIV_CD <>2 )THEN
            
                SELECT MIN(WORKS_ID)
                  INTO V_CONT
                  FROM ML_STAT_CONF_WORKS
                 WHERE APPLY_WRITE_YMD = :NEW.APPLY_WRITE_YMD
                   AND APPLY_WRITE_SEQ = :NEW.APPLY_WRITE_SEQ
                   AND WORKS_SEQN = :NEW.WORKS_SEQN
                ;
        
                    IF (V_CONT IS NULL OR V_CONT ='') THEN
        
                       
         
                        SELECT MIN(MID_CODE)
                          INTO V_GENRE_CD
                          FROM ML_CODE
                         WHERE BIG_CODE = 35
                           AND CODE_NAME = :NEW.WORKS_KIND
                        ;
                        
                        
                        IF ( V_GENRE_CD IS NULL OR V_GENRE_CD ='') THEN
                        
                             V_GENRE_CD := 99;
                        
                        END IF;
                        
                        
                        SELECT NVL(MAX(works_id),0)+1 
                          INTO V_WORKS_ID
                          FROM ML_STAT_WORKS
                        ;
        
            
                        INSERT INTO ML_STAT_WORKS
                                   ( WORKS_ID
                                   , GENRE_CD
                                   , WORKS_TITLE
                                   , WORKS_DIVS_CD
                                   , STAT_WORKS_YN
                                   , SYST_EFFORT_STAT_CD
                                   , RGST_IDNT
                                   , RGST_DTTM
                                   , OBJC_YN
                                   , OPEN_YN
                                   , OPEN_DTTM
                                   , DELT_YSNO      
                                   )
                            VALUES(  V_WORKS_ID
                                    ,V_GENRE_CD
                                    ,:NEW.WORKS_TITL
                                    ,6
                                    ,'Y'
                                    ,1
                                    ,'SUPER_ADMIN'
                                    ,SYSDATE
                                    ,'N'
                                    ,'Y'
                                    ,SYSDATE
                                    ,'N'         
                                    )
                        ;
        
        
                        INSERT INTO ML_STAT_CONF_WORKS
                                   ( WORKS_ID
                                   , APPLY_WRITE_YMD
                                   , APPLY_WRITE_SEQ
                                   , WORKS_SEQN
                                   , WORKS_TITL
                                   , WORKS_KIND
                                   , WORKS_FORM
                                   , WORKS_DESC
                                   , PUBL_YMD
                                   , PUBL_NATN
                                   , PUBL_MEDI_CD
                                   , PUBL_MEDI
                                   , PUBL_MEDI_ETC
                                   , COPT_HODR_NAME
                                   , COPT_HODR_TELX_NUMB
                                   , COPT_HODR_ADDR
                                   , CONF_DATE
                                   ) 
                            VALUES(  V_WORKS_ID
                                   , :NEW.APPLY_WRITE_YMD
                                   , :NEW.APPLY_WRITE_SEQ
                                   , :NEW.WORKS_SEQN
                                   , :NEW.WORKS_TITL
                                   , :NEW.WORKS_KIND
                                   , :NEW.WORKS_FORM
                                   , :NEW.WORKS_DESC
                                   , :NEW.PUBL_YMD
                                   , :NEW.PUBL_NATN
                                   , :NEW.PUBL_MEDI_CD
                                   , :NEW.PUBL_MEDI
                                   , :NEW.PUBL_MEDI_ETC
                                   , :NEW.COPT_HODR_NAME
                                   , :NEW.COPT_HODR_TELX_NUMB
                                   , :NEW.COPT_HODR_ADDR
                                   , TO_CHAR(SYSDATE, 'YYYYMMDD')
                                    )
                        ;
                    
                        
        
                    END IF;

              END IF;
              
        END IF;
    
  END IF;
    
END;
/
