CREATE OR REPLACE TRIGGER MLS.TR_ML_STAT_WORKS_02
AFTER INSERT OR UPDATE
ON MLS.ML_STAT_WORKS 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN

-------------------------------------------------------------------------------
-- 프로그램명 : 상당한노력 진행 매칭대상저작물 수집        
--              (기승인 저작물 : 6) 
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 12월 07일                                              
-- 수정  일자 :                                           
                      
-- 수 행 시 기: 트리거                                                      
-------------------------------------------------------------------------------
    
    IF INSERTING THEN
       
        IF ( :NEW.WORKS_DIVS_CD = 6 AND :NEW.STAT_WORKS_YN = 'Y' )THEN
            
            INSERT INTO ML_STAT_MAPP_CONF_WORKS
                       ( WORKS_ID
                        ,GENRE_CD
                        ,WORKS_TITLE
                        ,USE_YN           
                       )
                VALUES( :NEW.WORKS_ID
                       ,:NEW.GENRE_CD
                       ,UPPER(REPLACE(:NEW.WORKS_TITLE, ' ',''))
                       ,'Y'          
                        )
            ;
         
        END IF;   
        
        

    ELSIF UPDATING THEN
        
    
        IF ( :NEW.WORKS_DIVS_CD = 6 AND :OLD.STAT_WORKS_YN = 'Y' AND :NEW.STAT_WORKS_YN = 'N') THEN
             
            UPDATE ML_STAT_MAPP_CONF_WORKS
               SET USE_YN = 'N'
              WHERE WORKS_ID = :NEW.WORKS_ID
               ;         
                        
        END IF ;
 
    END IF;
   
    
  
END;
/
