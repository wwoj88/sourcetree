CREATE OR REPLACE TRIGGER MLS.TR_ML_STAT_SUBJ_INMT
AFTER UPDATE
ON MLS.ML_STAT_SUBJ_INMT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN

-------------------------------------------------------------------------------
-- 프로그램명 : 미분배 보상금 분배시 ,매칭대상저작물 수집제외(교과용)    
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 12월 21일                                              
-- 수정  일자 :                                           
                      
-- 수 행 시 기: 트리거                                                      
-------------------------------------------------------------------------------
    
    IF UPDATING THEN
       
        IF ( :NEW.ALLT_YSNO = 'Y' AND :OLD.ALLT_YSNO = 'N'  )THEN
            
           
         UPDATE ML_STAT_PROC_TARG_WORKS
               SET STAT_PROC_YN = 'N'
                  ,STAT_CHNG_YN = 'N'
                  ,STAT_PROC_EXCP_DTTM = SYSDATE
             WHERE WORKS_ID = :OLD.WORKS_ID
            ;
            
            
            UPDATE ML_STAT_WORKS
               SET STAT_WORKS_YN = 'N'
                  ,SYST_EFFORT_STAT_CD = 5 -- 상당한노력진행상태CD (5): 진행중지(분배완료) 
                  ,SYST_EFFORT_STAT_DTTM = SYSDATE
             WHERE WORKS_ID = :OLD.WORKS_ID
            ;
            
         
        END IF;   
       
 
    END IF;
   
    
  
END;
/
