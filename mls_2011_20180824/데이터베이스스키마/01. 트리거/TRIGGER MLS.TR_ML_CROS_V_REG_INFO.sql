CREATE OR REPLACE TRIGGER MLS.TR_ML_CROS_V_REG_INFO
AFTER INSERT
ON MLS.ML_CROS_V_REG_INFO
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE 

-------------------------------------------------------------------------------
-- 프로그램명 : 01.(ML_STAT_MAPP_CROS_WORKS) 매핑대상저작물 수집 (저작권등록) 
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 :                                            
-- 수정  일자 :                                           
                      
-- 수 행 시 기: 트리거                                                      
-------------------------------------------------------------------------------
BEGIN

    IF INSERTING THEN
    
                
            INSERT INTO ML_STAT_MAPP_CROS_WORKS
                       ( WORKS_ID
                        ,GENRE_CD
                        ,WORKS_TITLE
                        ,USE_YN           
                       )
                VALUES( :NEW.WORKS_ID
                       ,:NEW.CONT_CLASS_NAME_CD
                       ,UPPER(REPLACE(:NEW.CONT_TITLE, ' ',''))
                       ,'Y'          
                        )
            ;
         
        END IF;  
    
    
END;
/
