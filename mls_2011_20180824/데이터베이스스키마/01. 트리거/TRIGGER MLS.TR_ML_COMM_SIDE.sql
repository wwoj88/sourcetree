CREATE OR REPLACE TRIGGER MLS.TR_ML_COMM_SIDE
AFTER INSERT
ON MLS.ML_COMM_SIDE
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE 
    
    V_CW    ML_COMM_WORKS%ROWTYPE ;
    
BEGIN

-------------------------------------------------------------------------------
-- 프로그램명 : (ML_STAT_MAPP_WORKS) 매핑대상저작물 수집 (기타장르_위탁관리 저작물) 
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 :                                            
-- 수정  일자 :                                           
                      
-- 수 행 시 기: 트리거                                                      
-------------------------------------------------------------------------------

    IF INSERTING THEN
    
            SELECT *
              INTO V_CW
              FROM ML_COMM_WORKS
             WHERE GENRE_CD = 99
               AND WORKS_ID = :NEW.WORKS_ID
            ;
            
           
                
            INSERT INTO ML_STAT_MAPP_WORKS
                       ( WORKS_ID
                        ,GENRE_CD
                        ,WORKS_TITLE
                        ,USE_YN           
                       )
                VALUES( :NEW.WORKS_ID
                       ,FC_GET_GENRE_CD(99||:NEW.SIDE_GENRE_CD)
                       ,UPPER(REPLACE(V_CW.WORKS_TITLE, ' ',''))
                       ,DECODE(V_CW.COMM_MGNT_CD, 0, 'N',  3, 'N', 'Y')           
                        )
            ;
         
        END IF;   

    
    
END;
/
