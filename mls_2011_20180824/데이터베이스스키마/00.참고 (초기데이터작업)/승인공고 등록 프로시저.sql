CREATE OR REPLACE PROCEDURE MLS.P_ML_ANUC_INSERT_ROWDATE_01
IS

-------------------------------------------------------------------------------
-- 프로그램명 : 승인공고 데이터 등록 프로시저        
--              from : TMP_ML_ANUC_ROWDATA_121114 
--                to : ML_STAT_ANUC_BORD (bord_seqn :4)
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 11월 15일                                              
-- 수정  일자 :                                           
-- 스키마정보 : 승인공고 공고 등록
                      
-- 수 행 시 기: P_ML_STATPROC 상당한노력 프로시저에서 호출                                    
-------------------------------------------------------------------------------

CURSOR C_TARG_WORKS IS


SELECT *
  FROM MLS.TMP_ML_ANUC_ROWDATA_121114
 order by to_number(seqn)
;

V_BORD_SEQN NUMBER;
    
BEGIN

    
    FOR I IN C_TARG_WORKS LOOP
    
        SELECT NVL(MAX(BORD_SEQN),0)+1
          INTO V_BORD_SEQN
          FROM ML_STAT_ANUC_BORD
        ;
        
        INSERT
          INTO ML_STAT_ANUC_BORD (BORD_CD, BORD_SEQN, DIVS_CD, OPEN_YN, OPEN_IDNT, OPEN_DTTM
                                 ,RGST_IDNT, RGST_DTTM, DELT_YSNO, OBJC_YN
                                 -----
                                 ,TITE /* 제목*/
                                 ,ANUC_ITEM_1 /* 제호 */
                                 ,ANUC_ITEM_2 /* 공표 당시의 저작권자*/
                                 ,ANUC_ITEM_3 /* 신청인*/
                                 ,ANUC_ITEM_4 /* 이용허락기간*/
                                 ,ANUC_ITEM_5 /* 보상금 */
                                 ,ANUC_ITEM_6 /* 저작물의 이용방법 및 형태 */ 
                                 ,ANUC_ITEM_7 /* 승인일자 */ 
                                )
            VALUES(  4
                   ,V_BORD_SEQN
                   ,5
                   ,'Y'
                   ,'CPMADMIN'
                   ,SYSDATE
                   ,'CPMADMIN'
                   ,SYSDATE
                   ,'N'
                   ,'N'    
                   --
                   ,'<'||I.TITLE||'> 승인 공고'
                   ,I.TITLE || DECODE(NVL(I.OPEN_DATE,'-'), '-', '',' (공표연월일 : '||I.OPEN_DATE||')')
                   ,I.COPT_NAME
                   ,I.APPLY_NAME || ' (주소 : '||I.APPLY_ADDR||')'
                   ,I.LGMT_PERI
                   ,I.LGMT_AMNT
                   ,I.WORKS_KIND || ' ('||I.WORKS_FORM||') / '
                    ||chr(13)||chr(10)||I.USEX_DESC
                   ,I.CONF_DATE
                   )
           ;
         
        
        UPDATE TMP_ML_ANUC_ROWDATA_121114
           SET BORD_SEQN =  V_BORD_SEQN
         WHERE SEQN = I.SEQN
        ;
    
    END LOOP;
    
    
    COMMIT;
    
    EXCEPTION
       WHEN OTHERS THEN
          ROLLBACK; 
END;
/