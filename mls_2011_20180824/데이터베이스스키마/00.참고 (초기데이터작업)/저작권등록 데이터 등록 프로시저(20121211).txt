CREATE OR REPLACE PROCEDURE MLS.P_ML_CROS_INFO_ROWDATA_01
IS

-------------------------------------------------------------------------------
-- 프로그램명 : 저작권등록 데이터 등록 프로시저        
--              from : TMP_CROS_V_REG_INFO_1211 (12.12.04 데이터) 대상 : 2,394건
--                to : ML_CROS_V_REG_INFO 
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 12월 15일                                              
-- 수정  일자 :                                           
-- 스키마정보 : 승인공고 공고 등록
                      
-- 수 행 시 기: 직접호출 exec P_ML_CROS_INFO_ROWDATA_01     

-- 선행조건
/*

UPDATE TMP_CROS_V_REG_INFO_1211
    SET SEQN = ROWNUM
*/                               
-------------------------------------------------------------------------------

CURSOR C_TARG_WORKS IS


SELECT *
  FROM MLS.TMP_CROS_V_REG_INFO_1211
 order by to_number(reg_date_str) ASC
;

V_WORKS_ID NUMBER;
    
BEGIN
    

    /* 등록일, 장르코드 업데이트*/
    update  TMP_CROS_V_REG_INFO_1211 
       set reg_date = to_date(reg_date_str, 'YYYY-MM-DD') -- ok 
        , cont_class_name_cd = (SELECT mid_code
                                   FROM ML_CODE
                                  WHERE BIG_CODE = 79
                                    AND SUBSTR(CODE_NAME,1,2) = SUBSTR(cont_class_name,1,2))
                                ;
    commit;
    
                                   
    FOR I IN C_TARG_WORKS LOOP
        
    
        SELECT NVL(MAX(WORKS_ID),0)+1
          INTO V_WORKS_ID
          FROM ML_CROS_V_REG_INFO
        ;
        
        
                
        INSERT
          INTO ML_CROS_V_REG_INFO (WORKS_ID 
                                   ,REG_ID 
                                   ,REG_REASON 
                                   ,REG_COPT_HODR_NAME  
                                   ,CONT_TITLE 
                                   ,REG_PART1_NAME 
                                   ,REG_PART2_NAME 
                                   ,CONT_CLASS_NAME 
                                   ,CONT_CLASS_NAME_CD 
                                   ,AUTHOR_NAME 
                                   ,REG_DATE_STR 
                                   ,REG_DATE 
                                   ,DISPOSAL_ADDR1 
                                   ,RGST_DTTM
                                 )
            VALUES( V_WORKS_ID            
                    ,I.REG_ID 
                    ,I.REG_REASON 
                    ,I.REG_COPT_HODR_NAME 
                    ,I.CONT_TITLE
                    ,I.REG_PART1_NAME 
                    ,I.REG_PART2_NAME 
                    ,I.CONT_CLASS_NAME 
                    ,I.CONT_CLASS_NAME_CD 
                    ,I.AUTHOR_NAME 
                    ,I.REG_DATE_STR 
                    ,I.REG_DATE
                    ,I.DISPOSAL_ADDR1
                    ,SYSDATE          
                   )
           ;
         
        
        
        UPDATE TMP_CROS_V_REG_INFO_1211 
           SET WORKS_ID =  V_WORKS_ID
         WHERE SEQN = I.SEQN
        ;
        
    
    END LOOP;
    
    
    COMMIT;
    
    EXCEPTION
       WHEN OTHERS THEN
         
          ROLLBACK; 
          DBMS_OUTPUT.PUT_LINE('ERR CODE1 : ' || SUBSTR(SQLERRM,1,200));
          DBMS_OUTPUT.PUT_LINE('ERR MESSAGE1 : ' || SQLCODE );
END;
/