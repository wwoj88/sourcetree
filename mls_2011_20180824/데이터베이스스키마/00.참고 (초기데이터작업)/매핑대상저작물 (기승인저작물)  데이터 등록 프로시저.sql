CREATE OR REPLACE PROCEDURE MLS.P_ML_MAPP_CONF_ROWDATA_01  
IS

-------------------------------------------------------------------------------
-- 프로그램명 : 매핑대상저작물 (기승인저작물)  데이터 등록 프로시저        
--              from : ML_STAT_WORKS 
--                to : ML_STAT_MAPP_CONF_WORKS
--
-- 만  든  이 : 김소라                                             
-- 생성  일자 : 2012년 12월 13일                                              
-- 수정  일자 :                                           
-- 스키마정보 : 매핑대상저작물 (기승인저작물) 초기데이터 등록
                      
-- 수 행 시 기: 직접호출 exec P_ML_MAPP_CONF_ROWDATA_01
                              
-------------------------------------------------------------------------------

CURSOR C_TARG_WORKS IS


SELECT *
  FROM MLS.ML_STAT_WORKS
 WHERE WORKS_DIVS_CD = 6
   AND STAT_WORKS_YN = 'Y'
 order by WORKS_ID ASC
;

V_WORKS_ID NUMBER;
    
BEGIN
    

                                   
    FOR I IN C_TARG_WORKS LOOP
        
    
        INSERT INTO ML_STAT_MAPP_CONF_WORKS
                       ( WORKS_ID
                        ,GENRE_CD
                        ,WORKS_TITLE
                        ,USE_YN           
                       )
                VALUES( I.WORKS_ID
                       ,I.GENRE_CD
                       ,UPPER(REPLACE(I.WORKS_TITLE, ' ',''))
                       ,'Y'          
                        )
            ;
        
     
    END LOOP;
    
    
    COMMIT;
    
    EXCEPTION
       WHEN OTHERS THEN
         
          ROLLBACK; 
          DBMS_OUTPUT.PUT_LINE('ERR CODE1 : ' || SUBSTR(SQLERRM,1,200));
          DBMS_OUTPUT.PUT_LINE('ERR MESSAGE1 : ' || SQLCODE );
END;
/