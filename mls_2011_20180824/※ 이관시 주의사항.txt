=====================================================================================================
* 개발서버 및 운영서버 이관시 제외대상
=====================================================================================================

1) property 파일
   - class/*.properties
   - class/log4j.xml
   - 해당파일은 추가 및 수정내역을 직접 작성하도록 함
   
 
2) 설정파일
   - web.xml
   - class/spring/applicationContext-datasource.xml
   - 해당파일은 추가 및 수정내역을 직접 작성하도록 함

  
3) 제품관련 파일
   - web/admin/main/index.jsp (관리자시스템 설치화면)
   -	/cab
   - 	/cdoc
   - 	/editor
   -	/install
   -	/iPin
   - 	/MagicLine4
   - 	/NameCheck
   -	/pay
   -	/CheckPlusSafe
   - web/WEB-INF/KICA_SecuKit
   -			/magicline
   - 관련파일의 추가 및 수정내역을 직접 작성하도록 함