package kr.or.copyright.mls.user.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring/*.xml"})
public class UserDAOTest {
private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserDao dao;
	
	@Test
	public void allTest() throws Exception{
		selectTest();
	}
	
	public void selectTest(){
		Map params = new HashMap();
		params.put("selectNo", "3");
		
		params.put("sidoNm", "서울특별시");
		params.put("gugunNm", "종로구");
		
		//params.put("lawDong", "구기");
		//params.put("jibunOrigNo", "260");
		//params.put("jibunSubNo", "14");
		
		//params.put("roadNm", "세검");
		//params.put("buldOrigNo", "35");
		//params.put("buldSubNo", "10");
		
		params.put("buldNm", "청구");
		
		List<HashMap<String, String>> list = dao.findPostRoadNumb(params);
		System.out.println("############################################");
		System.out.println("############# resultCnt: "+list.size());
		System.out.println("############################################");
		for(int i = 0; i<list.size(); i++){
			HashMap map = (HashMap) list.get(i);
			System.out.println("############################################");
			System.out.println("############# result: "+map.values());
			System.out.println("############################################");
		}
	}

}
