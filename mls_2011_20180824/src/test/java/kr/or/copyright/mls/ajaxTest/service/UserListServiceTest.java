package kr.or.copyright.mls.ajaxTest.service;

import java.util.List;

import kr.or.copyright.mls.ajaxTest.service.UserListService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring/*.xml"})
public class UserListServiceTest {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserListService userListBO;
	
	@Test
	public void allTest() throws Exception{
		selectTest();
	}
	
	public void selectTest(){
		String preFix = "정";
		List<String> result = userListBO.getNameListForSuggest(preFix);
		for(int i=0; i<result.size(); i++){
			String resultStr = result.get(i);
			logger.info("############################################");
			logger.info("############# 검색결과"+i+" : "+resultStr);
			logger.info("############################################");
		}
	}
}
