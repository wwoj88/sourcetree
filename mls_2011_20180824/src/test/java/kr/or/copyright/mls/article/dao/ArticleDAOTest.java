package kr.or.copyright.mls.article.dao;

import kr.or.copyright.mls.article.dao.ArticleDAO;
import kr.or.copyright.mls.article.model.Article;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring/*.xml"})
public class ArticleDAOTest {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ArticleDAO dao;
	
	@Test
	public void allTest() throws Exception{
		insertTest();
	}
	
	public void insertTest() throws Exception{
		Article vo = setVO();
		dao.insertArticle(vo);
	}
	
	public Article setVO(){
		Article vo = new Article();
		vo.setArticleId(3);
		vo.setCommunityId(1);
		vo.setEmpno(0);
		vo.setGroupId(3);
		vo.setReLevel(0);
		vo.setReDepth(0);
		vo.setTitle("test");
		vo.setName("test");
		vo.setContent("tessdfdsffasdfdsafsdafdsafdsafdsafdsf");
		vo.setUseYn("Y");
		
		return vo;
	}
	
}
