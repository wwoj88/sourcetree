package kr.or.copyright.mls.ajaxTest.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.ajaxTest.model.Code;
import kr.or.copyright.mls.ajaxTest.service.CodeListService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring/*.xml"})
public class CodeListServiceTest {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CodeListService codeListBO;
	
	@Test
	public void allTest(){
		//selectTest();
		selectTest1();
	}
	
	public void selectTest(){
		Map<String, String> result = codeListBO.getInitCodeList();
		
		Integer sizeStr = result.size();
		
		Iterator iter = result.values().iterator();
		
		while(iter.hasNext()){
			logger.info("######### start #########");
			logger.info(sizeStr.toString());
			logger.info(result.values().toString());
			logger.info(iter.next().toString());
			logger.info("######### end #########");
		}
	}
	
	public void selectTest1(){
		Map<String, String> params = new HashMap<String, String>();
		
		params.put("superdeptid", "2");
		
		List<Code> result = codeListBO.getCodeList(params);
		
		for(int i=0; i<result.size(); i++){
			Code vo = result.get(i);
			logger.info("######### start #########");
			logger.info("######### resultVO:"+vo.toString()+"#########");
			logger.info("######### end #########");
		}
	}
}
