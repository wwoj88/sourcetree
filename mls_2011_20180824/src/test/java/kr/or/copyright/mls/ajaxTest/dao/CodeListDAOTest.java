package kr.or.copyright.mls.ajaxTest.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.ajaxTest.dao.CodeListDAO;
import kr.or.copyright.mls.ajaxTest.model.Code;
import kr.or.copyright.mls.user.model.ZipCodeRoad;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring/*.xml"})
public class CodeListDAOTest {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	CodeListDAO codeListDAO;
	
	@Test
	public void allTest(){
		//selectTest();
		//selectTest1();
		//selectTest2();
		selectTest3();
	}
	
	public void selectTest(){
		List<Code> result = codeListDAO.getInitCodeList();
		for(int i=0; i<result.size(); i++){
			Code codeVO = result.get(i);
			logger.info("############################################");
			logger.info("############# result"+i+" : "+codeVO.toString());
			logger.info("############################################");
		}
	}
	
	public void selectTest1(){
		Map<String, String> parms = new HashMap<String, String>();
		parms.put("superdeptid", "1");
		List<Code> result = codeListDAO.getCodeList(parms);
		
		for(int i = 0; i<result.size(); i++){
			Code codeVO = result.get(i);
			logger.info("############################################");
			logger.info("############# result"+i+" : "+codeVO.toString());
			logger.info("############################################");
		}
	}
	
	public void selectTest2(){
	    Map<String, String> params = new HashMap<String, String>();
	    params.put("sidoNm", "서울특별시");
	    List<ZipCodeRoad> result = codeListDAO.getRoadCodeList(params);
	    for(int i = 0; i<result.size(); i++){
		ZipCodeRoad codeVO = result.get(i);
		logger.info("############################################");
		logger.info("############# result"+i+" : "+codeVO.toString());
		logger.info("############################################");
	    }
	}
	
	public void selectTest3(){
	    List<ZipCodeRoad> result = codeListDAO.getInitRoadCodeList();
	    for(int i = 0; i<result.size(); i++){
		ZipCodeRoad codeVO = result.get(i);
		logger.info("############################################");
		logger.info("############# result"+i+" : "+codeVO.toString());
		logger.info("############################################");
	    }
	}
	
	

}
