package kr.or.copyright.mls.board.dao;

import kr.or.copyright.mls.board.model.Board;

import org.aspectj.apache.bcel.generic.ReturnaddressType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring/*.xml"})
public class BoardDAOTest {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private BoardDao dao;
	
	@Test
	public void allTest() throws Exception{
		selectTest();
	}
	
	public Board setVO(){
		Board vo = new Board();
		vo.setBordSeqn(157000);
		vo.setMenuSeqn(2);
		vo.setThreaded(157000);
		return vo;
	}
	
	public void selectTest(){
		Board result = (Board)dao.boardView(setVO());
		logger.info("############################################");
		logger.info("############# result title: "+result.getTite());
		logger.info("############# result rgstName: "+result.getRgstName());
		logger.info("############# result mail: "+result.getMail());
		logger.info("############# result pswd: "+result.getPswd());
		logger.info("############# result bordDesc: "+result.getBordDesc());
		logger.info("############################################");
	}
}
