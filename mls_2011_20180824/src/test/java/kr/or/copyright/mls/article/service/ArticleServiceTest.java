package kr.or.copyright.mls.article.service;

import kr.or.copyright.mls.article.model.Article;
import kr.or.copyright.mls.article.service.ArticleService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring/*.xml"})
public class ArticleServiceTest {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ArticleService articleBO;
	
	@Test
	public void allTest(){
		insertTest();
	}
	
	public void insertTest(){
		Article vo = setVO();
		int articleID = articleBO.writeArticle(vo);
		logger.info("############################################");
		logger.info("############# ARTICLE_ID"+articleID);
		logger.info("############################################");
	}
	
	public Article setVO(){
		Article vo = new Article();
		vo.setCommunityId(1);
		vo.setEmpno(0);
		vo.setGroupId(3);
		vo.setReLevel(0);
		vo.setReDepth(0);
		vo.setTitle("test");
		vo.setName("test");
		vo.setContent("test");
		vo.setUseYn("Y");
		
		return vo;
	}
}
