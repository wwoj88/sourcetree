package kr.or.copyright.mls.test.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import kr.or.copyright.mls.console.civilservices.CivilServiceDaoImpl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring/*.xml"})
public class TestDaoTest{
private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CivilServiceDaoImpl dao;
	
	@Test
	public void allTest() throws Exception{
		insertTest();
	}
	
	public void insertTest() throws Exception{
		List list = (List)dao.selectCivilStats();
		System.out.println( "List : " +list );
	}
	

}
