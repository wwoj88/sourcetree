package kr.or.copyright.mls.ajaxTest.dao;

import java.util.List;

import kr.or.copyright.mls.ajaxTest.dao.UserListDAO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring/*.xml"})
public class UserListDAOTest {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserListDAO dao;
	
	@Test
	public void allTest() throws Exception{
		selectTest();
	}
	
	public void selectTest(){
		String preFix = "��";
		List<String> result = dao.getNameListForSuggest(preFix);
		for(int i=0; i<result.size(); i++){
			String resultStr = result.get(i);
			logger.info("############################################");
			logger.info("############# �达�λ����"+i+" : "+resultStr);
			logger.info("############################################");
		}
	}

}
