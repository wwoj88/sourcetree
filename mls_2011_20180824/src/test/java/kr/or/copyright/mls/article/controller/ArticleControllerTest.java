package kr.or.copyright.mls.article.controller;

import kr.or.copyright.mls.article.model.Article;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/*.xml"})
public class ArticleControllerTest {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ArticleController cmd;
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Test
	public void allTest() throws Exception{
		insertTest();
	}
	
	public void insertTest() throws Exception{
		request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        
        request.setMethod("POST");
        
        request.setParameter("communityId", "1");
        request.setParameter("name", "정병호");
        request.setParameter("title", "컨트롤러 테스트");
        request.setParameter("content", "성공해라");
        request.setParameter("useYn", "Y");
        
        request.setRequestURI("/article/write.do");
        
        ModelAndView mav = new AnnotationMethodHandlerAdapter().handle(request, response, cmd);
        
        Article result = (Article)mav.getModelMap().get("article");
        System.out.println("############################################");
        System.out.println("############# result : "+result);
        System.out.println("############################################");
	}
	
	
}
