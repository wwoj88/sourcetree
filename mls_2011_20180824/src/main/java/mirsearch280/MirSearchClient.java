package mirsearch280;
import java.net.*;
import java.io.*;
import java.util.*;
import java.lang.*;

public class MirSearchClient {

	private Socket client;
	private String request;
	private String field[];
	private String result[][];
	private String logfile;
	private String charset="CP949";
	private PrintStream printout;
	
	private String queryfilter;
	private String sifter;
	private String sftval;
	private String srchsession;
	private String errormsg;

	private int fieldcount;
	private int totalnum;
	private int foundnum;
	private int validnum;
	private int rownum;
	private int columenum;
	private int eachCollsize;
	private int eachCollcount[];
	private int eachCollTotalcount[];
	private boolean debug=false;
	private int error;
	
	public String []SplitString(String str, String del) {
		int pos=0;
		int newpos=0;
		int i=0;
		String token[];
		Vector v=new Vector();
		
		while((newpos=str.indexOf(del, pos))!=-1){
			v.add(new String(str.substring(pos, newpos)));
			pos=newpos+1;
		}
		if(pos!=str.length())
			v.add(new String(str.substring(pos)));
		
		token=new String[v.size()];
		for(i=0; i<v.size(); i++)
			token[i]=(String)v.get(i);
		return token;
	}

	public int skip(BufferedReader br, int n) {
		int i, loop=0, ch;
		String out=new String("");
		
		if(debug==true && client==null)  {
			PutLog("Connection is closed[A]");
			return 1;
		}
		for(i=0; i<n; i++){
			try{
				ch=br.read();
			}catch(IOException e){
				if(loop<100) {
					loop++;
					try{
						Thread.sleep(100);
					}catch(Exception f){
					}
					i--;
					continue;
				}
				error=-2;
				errormsg="Network is Busy"; 
				if(debug==true) PutLog("Network is too Busy. 1:"+out);
				return 1;
			}
			out+= (char)ch;
		}
		return 0;
	}
	
	public String readLine(BufferedReader br) {
		int ch, loop=0;
		String out=new String("");
		
		if(debug==true && client==null)  {
			PutLog("Connection is closed[B]");
			return null;
		}
		while(true){
			try{
				ch=br.read();
			}catch(IOException e){
				if(loop<100) {
					loop++;
					try{
						Thread.sleep(100);
					}catch(Exception f){
					}
					continue;
				}				
				error=-2;
				errormsg="Network is Busy";
				if(debug==true) PutLog("Network is too Busy. 2:"+out);
				return null;
			}
			if(ch==-1 || ch=='\n') break;
			if(ch!='\r') out+= (char)ch;
		}
		
		//if(debug==true) PutLog("["+client+"]OUT: "+out);
		return out;
	}
	

	
	public void setDebug(String filename){
		FileOutputStream fp;
		logfile=filename;
		debug=true;
		try{
			fp = new FileOutputStream(filename, true);
			printout = new PrintStream(fp);
		}catch(IOException e){
			return;
		}
	}

	public void setCharset(String cs){
		charset=cs;
		return;
	}
	
	public void PutLog(String str) {
		printout.println(str);
		printout.flush();
	}
	
	public void Init(){
		fieldcount=0;
		totalnum=0;
		foundnum=0;
		validnum=0;
		rownum=0;
		columenum=0;
		eachCollsize=0;
		error=0;
	
		queryfilter="";
		sifter="";
		sftval="";
		srchsession="";
		errormsg="";
	}
	
	public int Connect(String StringServer, int nPort){
		try{	// 서버에 연결
			String Server="localhost";
			int port = 4002;
			
			if(StringServer!=null && StringServer.length()>0)  Server=StringServer;
			if(nPort>0) port=nPort;
			
			client=new Socket(Server, port);
			Init();
			if(debug==true) PutLog("["+client+"]Connect and Init()");
		}catch(Exception e){
			if(debug==true) PutLog("Connect Error");
			errormsg="Search Server Connection Error.";
			error=-1;
			client=null;
			return -1;
		}
		return 0;
	}
	
	public void Close() {
		try{
			if(client!=null) {
				if(debug==true) PutLog("["+client+"]Close");
				client.close();
				Init();
				client=null;
			}
		}catch(Exception e){
			if(debug==true) PutLog("["+client+"]Close Error");
		}
	}

	public void SetQueryFilter(String qFilter) {
		if(qFilter!=null) queryfilter=qFilter;
	}
	
	
	public void SetSifter(String Sifter, String SftVal) {
		if(Sifter==null || SftVal==null)
			return;
		sifter=Sifter;
		sftval=SftVal;
	}
	
	
	public int SearchNew(String Collection, String QueryParser, String QueryTxt, int nMaxDoc, 
		String SortSpec, int nFromDoc, int nToDoc, String Fields, String StartTag, String EndTag, String SrcQuery) {
		
		char delim=(char)30;
		
		if(client!=null){
		    	OutputStream os;
		    	OutputStreamWriter osw;
		    	BufferedWriter bw;
		    	InputStream is;
		    	InputStreamReader isr;
		    	BufferedReader br;
			String Line, eachColl[];
			int i=0, j=0;

			field=SplitString(Fields, ",");
			if(SrcQuery==null) request="version=280"+delim+"querytext="+QueryTxt+delim+"resultstart="+nFromDoc+delim+"resultcount="+nToDoc+delim+"collname="+Collection+delim+"sortspec="+SortSpec+delim+"maxdoc="+nMaxDoc+delim+"qparser="+QueryParser+delim+"starttag="+StartTag+delim+"endtag="+EndTag+delim+"sifter="+sifter+delim+"sftval="+sftval+delim+"qfilter="+queryfilter+delim+"resrch=";
			else request="version=280"+delim+"querytext="+QueryTxt+delim+"resultstart="+nFromDoc+delim+"resultcount="+nToDoc+delim+"collname="+Collection+delim+"sortspec="+SortSpec+delim+"maxdoc="+nMaxDoc+delim+"qparser="+QueryParser+delim+"starttag="+StartTag+delim+"endtag="+EndTag+delim+"sifter="+sifter+delim+"sftval="+sftval+delim+"qfilter="+queryfilter+delim+"resrch="+SrcQuery;
			
			for(i=0; i<field.length; i++){
				request=request+delim+"field="+field[i];			
			}
			fieldcount=i;

		    try{
				os=client.getOutputStream();
				if(charset==null) osw=new OutputStreamWriter(os);
				else osw=new OutputStreamWriter(os, charset);
				bw=new BufferedWriter(osw);
				is=client.getInputStream();
				if(charset==null) isr=new InputStreamReader(is);
				else isr=new InputStreamReader(is, charset);
				br=new BufferedReader(isr);

				if(debug==true) PutLog("["+client+"]Output Encoder: "+osw.getEncoding());
				if(debug==true) PutLog("["+client+"]Input Encoder: "+isr.getEncoding());

				try{
					// Sending Message
					bw.write(request);
					bw.flush();
					if(debug==true) PutLog("["+client+"]Send Msg: "+request);
					// Reading Message
					try{
						do{
							// Skip 10 Bytes
							if(skip(br, 10)!=0) break;
							
							// Message Parsing
							Line=readLine(br);
							if(Line == null) break;
							error=Integer.parseInt(Line);
							if(debug==true) PutLog("["+client+"]error: "+error);
							
							errormsg=br.readLine();
							if(errormsg == null) break;
							if(debug==true) PutLog("["+client+"]errormsg: "+errormsg);
							
							Line=readLine(br);
							if(Line == null) break;
							totalnum=Integer.parseInt(Line);
							if(debug==true) PutLog("["+client+"]totalnum: "+totalnum);
							
							Line=br.readLine();
							if(Line == null) break;
							eachColl=SplitString(Line, "\t");
							foundnum=Integer.parseInt(eachColl[0]);
							if(debug==true) PutLog("["+client+"]foundnum: "+foundnum);
							
							
							Line=br.readLine();
							if(Line == null) break;
							validnum=Integer.parseInt(Line);
							if(debug==true) PutLog("["+client+"]validnum: "+validnum);
							
							Line=br.readLine();
							if(Line == null) break;
							rownum=Integer.parseInt(Line);
							if(debug==true) PutLog("["+client+"]rownum: "+rownum);
							
							Line=br.readLine();
							if(Line == null) break;
							columenum=Integer.parseInt(Line);
							if(debug==true) PutLog("["+client+"]columenum: "+columenum);
							
							srchsession=readLine(br);
							if(srchsession == null) break;
							if(debug==true) PutLog("["+client+"]srchsession: "+srchsession);
											
							eachCollsize=(eachColl.length)-1;
							eachCollcount= new int[eachCollsize];
							eachCollTotalcount= new int[eachCollsize];
							
							String EachVal[];
							for(i=1; i<=eachCollsize; i++){
								EachVal=SplitString(eachColl[i], "/");
								eachCollcount[i-1]=Integer.parseInt(EachVal[0]);
								eachCollTotalcount[i-1]=Integer.parseInt(EachVal[1]);
								if(debug==true) {
									PutLog("["+client+"]eachCollcount["+i+"]: "+eachCollcount[i-1]);
									PutLog("["+client+"]eachCollTotalcount["+i+"]: "+eachCollTotalcount[i-1]);
								}
							}
							
							result=new String[rownum][];
							for(i=0; i<rownum; i++){
								result[i]=new String[columenum];
								for(j=0; j<columenum; j++){
										result[i][j]=readLine(br);
										if(result[i][j]==null||result[i][j]=="") {
											i=rownum;
											break;
										}
										if(debug==true) PutLog("["+client+"]result["+i+"]["+j+"]: "+result[i][j]);
								}
							}
						}while(false);

	
						try{
							bw.close();
							br.close();
						}catch(IOException e){
							if(debug==true) PutLog("["+client+"]Stream Close Error");
						}
					}catch(IOException e){
						if(debug==true) PutLog("["+client+"]Recieve Error(1)");
						errormsg="Message Reciving Error.";
						error=-3;
						Init();
					}
				}catch(IOException e){
					if(debug==true) PutLog("["+client+"]Send Error");
					errormsg="Message Sending Error.";
					error=-2;
					Init();
				}
			}catch(IOException e){
				if(debug==true) PutLog("["+client+"]Input/Output Create Stream Error");
			}
		}else{
		    	Init();
		}


		return error;
	}
	
	public String GetErrorMsg(){
		return "["+error+"]"+errormsg;
	}
	
	public int GetTotalCount(){
		return totalnum;
	}

	public int GetFoundCount(){
		return foundnum;
	}

	public int GetValidCount(){
		return validnum;
	}

	public int GetResultCount(){
		return rownum;
	}

	public int GetFieldCount(){
		return columenum;
	}

	public int GetEachCollSize(){
		return eachCollsize;
	}

	public String GetSrchSession(){
		return srchsession;
	}
	
	public int GetEachCount(int i){
		if(i<1 && i>eachCollsize) return 0;
		return eachCollcount[i-1];
	}
	
	public int GetEachTotalCount(int i){
		if(i<1 && i>eachCollsize) return 0;
		return eachCollTotalcount[i-1];
	}
	
	public String GetFieldVal(int Row, String FieldName){
		int Col=0;

		for(Col=0; Col<fieldcount; Col++){
			if(FieldName.compareToIgnoreCase(field[Col])==0) 
				break;
		}
		if(fieldcount==Col){
			return "Field Name is Invalid!";
		}else{
			return result[Row-1][Col];
		}
	}
}



