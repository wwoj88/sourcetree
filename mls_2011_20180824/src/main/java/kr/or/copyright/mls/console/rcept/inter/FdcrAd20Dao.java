package kr.or.copyright.mls.console.rcept.inter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tobesoft.platform.data.Dataset;

public interface FdcrAd20Dao{

	public List brctInmtInsert( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	public List subjInmtInsert( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	public List librInmtInsert( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	public List lssnInmtInsert( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	public List subjInmtUpdate( Dataset ds );

	public List inmtMgntMuscList( Map map );

	public List totalRowinmtMgntMuscList( Map map );

	public List inmtMgntLibrList( Map map );

	public List totalRowinmtMgntLibrList( Map map );

	public List inmtMgntSubjList( Map map );

	public List totalRowinmtMgntSubjList( Map map );

	public List inmtMgntLssnList( Map map );

	public List totalRowinmtMgntLssnList( Map map );

	public List imgeInsert( ArrayList<Map<String, Object>> excelList );

	public List movieInsert( ArrayList<Map<String, Object>> excelList );

	public List inmtMgntMovieList( Map map );

	public List totalRowinmtMgntMovieList( Map map );

	public List inmtMgntImageList( Map map );

	public List totalRowinmtMgntImageList( Map map );

	public List subjInmtUpdateUpload( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	public List librInmtUpdateUpload( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	public List brctInmtUpdateUpload( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	public List lssnInmtUpdateUpload( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	public List inmtMgntReptView( Map map );

	public void updateMuscStatYn( Map map );

	public void updateLibrStatYn( Map map );

	public void updateSubjStatYn( Map map );

	public void updateLssnStatYn( Map map );

}
