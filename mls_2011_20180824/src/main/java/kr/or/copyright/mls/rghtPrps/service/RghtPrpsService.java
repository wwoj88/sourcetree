package kr.or.copyright.mls.rghtPrps.service;

import java.util.List;

import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.rghtPrps.model.PrpsAttc;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

public interface RghtPrpsService {
	
	public String getMuscRecFile(RghtPrps rghtPrps);
	
	public ListResult muscRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);

	public RghtPrps muscRghtDetail(RghtPrps rghtPrps);
		
	public ListResult bookRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	
	public RghtPrps bookRghtDetail(RghtPrps rghtPrps);
	
	public ListResult scriptRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	
	public RghtPrps scriptRghtDetail(RghtPrps rghtPrps);
	
	public ListResult imageRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	
	public ListResult mvieRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	
	public RghtPrps mvieRghtDetail(RghtPrps rghtPrps);
	
	public ListResult broadcastRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	
	public RghtPrps broadcastRghtDetail(RghtPrps rghtPrps);
	
	public ListResult newsRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);

	public RghtPrps newsRghtDetail(RghtPrps rghtPrps);
	
	public List prpsList(RghtPrps rghtPrps);
	
	public RghtPrps rghtPrpsSave(RghtPrps rghtPrps) throws Exception;
	
	public int rghtPrpsModi(RghtPrps rghtPrps) throws Exception;
	
	public List rghtPrpsHistList(RghtPrps rghtPrps);
	
	public void insertPrpsAttc(PrpsAttc params);
}
