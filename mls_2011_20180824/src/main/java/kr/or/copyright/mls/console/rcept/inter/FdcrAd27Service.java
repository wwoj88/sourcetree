package kr.or.copyright.mls.console.rcept.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd27Service{

	/**
	 * 미분배보상금 수업목적 저작물 보고일 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd27List1( Map<String, Object> commandMap ) throws Exception;

}
