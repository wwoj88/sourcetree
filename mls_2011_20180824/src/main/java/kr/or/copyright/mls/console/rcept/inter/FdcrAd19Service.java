package kr.or.copyright.mls.console.rcept.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd19Service{

	/**
	 * 미분배보상금저작물 보고현황 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd19List1( Map<String, Object> commandMap ) throws Exception;

}
