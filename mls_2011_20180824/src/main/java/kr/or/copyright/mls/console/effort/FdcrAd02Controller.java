package kr.or.copyright.mls.console.effort;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.mls.adminCommon.dao.AdminCommonDao;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Service;
import kr.or.copyright.mls.console.effort.inter.FdcrAd02Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 상당한노력(신청 및 조회공고) > 상당한노력 신청접수
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd02Controller extends DefaultController{

	@Resource( name = "fdcrAd02Service" )
	private FdcrAd02Service fdcrAd02Service;
	
	@Resource( name = "fdcrAd01Service" )
	private FdcrAd01Service fdcrAd01Service;
	
  @Resource(name = "adminCommonDao")
  private AdminCommonDao adminCommonDao;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd02Controller.class );

	/**
	 * 저작권자 조회 공고 게시판 목록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd02List1.page" )
	public String fdcrAd02List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String tabIndex = EgovWebUtil.getString( commandMap, "TAB_INDEX" );
		if( "".equals( tabIndex ) ){
			tabIndex = "0";
		}
		commandMap.put( "TAB_INDEX", tabIndex );
		model.addAttribute( "commandMap", commandMap );
		return "effort/fdcrAd02List1.tiles";
	}
	
	/**
	 * 저작권자 조회 공고 게시판 목록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd02List1Sub1.page" )
	public String fdcrAd02List1Sub1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		EgovWebUtil.ajaxCommonEncoding( request, commandMap );
		// 파라미터 셋팅
		fdcrAd02Service.fdcrAd02List1( commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		model.addAttribute( "ds_count", commandMap.get( "ds_count" ) );
		model.addAttribute( "ds_count0", ( (List) commandMap.get( "ds_count" ) ).get( 0 ) );
		model.addAttribute( "ds_count1", ( (List) commandMap.get( "ds_count" ) ).get( 1 ) );
		model.addAttribute( "ds_count2", ( (List) commandMap.get( "ds_count" ) ).get( 2 ) );
		model.addAttribute( "commandMap", commandMap );
		return "effort/fdcrAd02List1Sub1";
	}

	/**
	 * 상당한 노력 신청 접수 - 상세조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd02UpdateForm1.page" )
	public String fdcrAd02UpdateForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		

		fdcrAd02Service.fdcrAd02UpdateForm1( commandMap );
		
		
	   HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
     String ip = req.getHeader("X-FORWARDED-FOR");
     if (ip == null) {
          ip = req.getRemoteAddr();
     }
     String USER_IDNT = request.getParameter("USER_IDNT");
     Map logMap = new HashMap();
     logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
     logMap.put("PROC_STATUS", "상당한 노력 신청접수" );
     logMap.put("PROC_ID",  "상당한 노력 신청접수 상세조회");
     logMap.put("MENU_URL", request.getRequestURI());
     logMap.put("IP_ADDRESS", ip);

     adminCommonDao.insertAdminLogDo(logMap);
     
     
     
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_file", commandMap.get( "ds_file" ) );
		model.addAttribute( "ds_copt_hodr", commandMap.get( "ds_copt_hodr" ) );
		model.addAttribute( "ds_shist", commandMap.get( "ds_shist" ) );
		model.addAttribute( "ds_supl_list", commandMap.get( "ds_supl_list" ) );
		model.addAttribute( "commandMap", commandMap );
		return "effort/fdcrAd02UpdateForm1.tiles";
	}

	/**
	 * 상당한 노력 신청 접수 - 저작권자 조회 공고내용 보완
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd02UpdateForm2.page" )
	public String fdcrAd02UpdateForm2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		fdcrAd02Service.fdcrAd02UpdateForm2( commandMap );
		
		
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    String ip = req.getHeader("X-FORWARDED-FOR");
    if (ip == null) {
         ip = req.getRemoteAddr();
    }
    String USER_IDNT = request.getParameter("USER_IDNT");
    Map logMap = new HashMap();
    logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
    logMap.put("PROC_STATUS", "상당한 노력 신청접수" );
    logMap.put("PROC_ID",  "상당한 노력 신청접수 / 내용보완");
    logMap.put("MENU_URL", request.getRequestURI());
    logMap.put("IP_ADDRESS", ip);

    adminCommonDao.insertAdminLogDo(logMap);
    
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_file", commandMap.get( "ds_file" ) );
		model.addAttribute( "ds_code", commandMap.get( "ds_code" ) );
		model.addAttribute( "commandMap", commandMap );
		return "effort/fdcrAd02Pop2.popup";
	}

	/**
	 * 상당한 노력 신청 접수 - 저작권자 조회 공고내용 보완
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd02Update2.page" )
	public String fdcrAd02Update2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 기존 컬럼
				commandMap.put( "BIG_CODE", "88" );
				ArrayList<Map<String,Object>> codeList = fdcrAd01Service.selectCdList( commandMap );
				Map<String, Object> info = (Map<String, Object>)((List)fdcrAd02Service.fdcrAd02List4( commandMap )).get( 0 );

				String WORKS_TITLE = EgovWebUtil.getString( info, "WORKS_TITLE" );
				String ANUC_ITEM_1 = EgovWebUtil.getString( info, "ANUC_ITEM_1" );
				String ANUC_ITEM_2 = EgovWebUtil.getString( info, "ANUC_ITEM_2" );
				String ANUC_ITEM_3 = EgovWebUtil.getString( info, "ANUC_ITEM_3" );
				String ANUC_ITEM_4 = EgovWebUtil.getString( info, "ANUC_ITEM_4" );
				String ANUC_ITEM_5 = EgovWebUtil.getString( info, "ANUC_ITEM_5" );
				String ANUC_ITEM_6 = EgovWebUtil.getString( info, "ANUC_ITEM_6" );
				String ANUC_ITEM_7 = EgovWebUtil.getString( info, "ANUC_ITEM_7" );
				String ANUC_ITEM_8 = EgovWebUtil.getString( info, "ANUC_ITEM_8" );
				String ANUC_ITEM_9 = EgovWebUtil.getString( info, "ANUC_ITEM_9" );
				String ANUC_ITEM_10 = EgovWebUtil.getString( info, "ANUC_ITEM_10" );
				String ANUC_ITEM_11 = EgovWebUtil.getString( info, "ANUC_ITEM_11" );
				String ANUC_ITEM_12 = EgovWebUtil.getString( info, "ANUC_ITEM_12" );

				// 넘긴 파라미터
				String PARAM_WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
				String PARAM_ANUC_ITEM_1 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_1" );
				String PARAM_ANUC_ITEM_2 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_2" );
				String PARAM_ANUC_ITEM_3 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_3" );
				String PARAM_ANUC_ITEM_4 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_4" );
				String PARAM_ANUC_ITEM_5 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_5" );
				String PARAM_ANUC_ITEM_6 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_6" );
				String PARAM_ANUC_ITEM_7 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_7" );
				String PARAM_ANUC_ITEM_8 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_8" );
				String PARAM_ANUC_ITEM_9 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_9" );
				String PARAM_ANUC_ITEM_10 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_10" );
				String PARAM_ANUC_ITEM_11 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_11" );
				String PARAM_ANUC_ITEM_12 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_12" );

				ArrayList<Map<String, Object>> suplList = new ArrayList<Map<String, Object>>();
				if( !PARAM_ANUC_ITEM_1.equals( ANUC_ITEM_1 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_1 );
					param.put( "post_item", PARAM_ANUC_ITEM_1 );
					param.put( "supl_item_cd", "10" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 10 ) );
					suplList.add( param );
				}
				if( !PARAM_ANUC_ITEM_2.equals( ANUC_ITEM_2 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_2 );
					param.put( "post_item", PARAM_ANUC_ITEM_2 );
					param.put( "supl_item_cd", "21" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 21 ) );
					suplList.add( param );
				}
				if( !PARAM_ANUC_ITEM_3.equals( ANUC_ITEM_3 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_3 );
					param.put( "post_item", PARAM_ANUC_ITEM_3 );
					param.put( "supl_item_cd", "22" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 22 ) );
					suplList.add( param );
				}
				if( !PARAM_ANUC_ITEM_4.equals( ANUC_ITEM_4 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_4 );
					param.put( "post_item", PARAM_ANUC_ITEM_4 );
					param.put( "supl_item_cd", "23" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 23 ) );
					suplList.add( param );
				}
				
				if( !PARAM_WORKS_TITLE.equals( WORKS_TITLE ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", WORKS_TITLE );
					param.put( "post_item", PARAM_WORKS_TITLE );
					param.put( "supl_item_cd", "32" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 31 ) );
					suplList.add( param );
				}
				
				if( !PARAM_ANUC_ITEM_5.equals( ANUC_ITEM_5 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_5 );
					param.put( "post_item", PARAM_ANUC_ITEM_5 );
					param.put( "supl_item_cd", "40" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 40 ) );
					suplList.add( param );
				}
				
				if( !PARAM_ANUC_ITEM_6.equals( ANUC_ITEM_6 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_6 );
					param.put( "post_item", PARAM_ANUC_ITEM_6 );
					param.put( "supl_item_cd", "51" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 51 ) );
					suplList.add( param );
				}
				
				if( !PARAM_ANUC_ITEM_7.equals( ANUC_ITEM_7 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_7 );
					param.put( "post_item", PARAM_ANUC_ITEM_7 );
					param.put( "supl_item_cd", "52" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 52 ) );
					suplList.add( param );
				}
				
				if( !PARAM_ANUC_ITEM_8.equals( ANUC_ITEM_8 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_8 );
					param.put( "post_item", PARAM_ANUC_ITEM_8 );
					param.put( "supl_item_cd", "60" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 60 ) );
					suplList.add( param );
				}
				
				if( !PARAM_ANUC_ITEM_9.equals( ANUC_ITEM_9 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_9 );
					param.put( "post_item", PARAM_ANUC_ITEM_9 );
					param.put( "supl_item_cd", "81" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 81 ) );
					suplList.add( param );
				}
				
				if( !PARAM_ANUC_ITEM_10.equals( ANUC_ITEM_10 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_10 );
					param.put( "post_item", PARAM_ANUC_ITEM_10 );
					param.put( "supl_item_cd", "82" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 82 ) );
					suplList.add( param );
				}
				
				if( !PARAM_ANUC_ITEM_11.equals( ANUC_ITEM_11 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_11 );
					param.put( "post_item", PARAM_ANUC_ITEM_11 );
					param.put( "supl_item_cd", "83" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 83 ) );
					suplList.add( param );
				}
				
				if( !PARAM_ANUC_ITEM_12.equals( ANUC_ITEM_12 ) ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "pre_item", ANUC_ITEM_12 );
					param.put( "post_item", PARAM_ANUC_ITEM_12 );
					param.put( "supl_item_cd", "84" );
					param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 84 ) );
					suplList.add( param );
				}
				
				commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
				commandMap.put( "RGST_NAME", ConsoleLoginUser.getUserName() );
				commandMap.put( "SEND_MAIL_ADDR", ConsoleLoginUser.getUserEmail() );
				commandMap.put( "RECV_MAIL_ADDR", EgovWebUtil.getString( info, "RECV_MAIL_ADDR" ) );
				commandMap.put( "RGST_DTTM", EgovWebUtil.getString( info, "RGST_DTTM" ) );
				
				
				if( suplList.size() != 0 ){
					commandMap.put( "suplList", suplList );

					boolean isSuccess = fdcrAd02Service.fdcrAd02Update2( commandMap );
					if( isSuccess ){
					     
					     HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
					     String ip = req.getHeader("X-FORWARDED-FOR");
					     if (ip == null) {
					          ip = req.getRemoteAddr();
					     }
					     String USER_IDNT = request.getParameter("USER_IDNT");
					     Map logMap = new HashMap();
					     logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
					     logMap.put("PROC_STATUS", "상당한 노력 신청접수" );
					     logMap.put("PROC_ID",  "상당한 노력 신청접수 / 보완");
					     logMap.put("MENU_URL", request.getRequestURI());
					     logMap.put("IP_ADDRESS", ip);

					     adminCommonDao.insertAdminLogDo(logMap);
					     
						return returnUrl(
							model,
							suplList.size()+"건의 데이터가 보완처리되었습니다.","");
					}else{
						return returnUrl(
							model,
							"실패했습니다.",
							"/console/effort/fdcrAd02UpdateForm2.page?WORKS_ID=" + EgovWebUtil.getString( commandMap, "WORKS_ID" ) );
					}
				}else{
					return returnUrl(
						model,
						"보완처리 할 내용이 없습니다.",
						"/console/effort/fdcrAd02UpdateForm2.page?WORKS_ID=" + EgovWebUtil.getString( commandMap, "WORKS_ID" ) );
				}
				
	}

	/**
	 * 진행상태 내역 목록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd02List2.page" )
	public String fdcrAd02List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		fdcrAd02Service.fdcrAd02List2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "commandMap", commandMap );
		return "effort/fdcrAd02Pop1.popup";
	}

	/**
	 * 진행상태 내역 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings( "rawtypes" )
	@RequestMapping( value = "/console/effort/fdcrAd02Download1.page" )
	public void fdcrAd02Download1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		int ATTC_SEQN = EgovWebUtil.getToInt(commandMap, "ATTC_SEQN" );
		// 파라미터 셋팅
		List list = fdcrAd02Service.fdcrAd02List3( commandMap );
		for( int i = 0; i < list.size(); i++ ){
			Map map = (Map) list.get( i );
			int attcSeqn = ((BigDecimal)map.get("ATTC_SEQN")).intValue();
			if( attcSeqn == ATTC_SEQN){
				String filePath = (String) map.get( "FILE_PATH" );
				String fileName = (String) map.get( "FILE_NAME" );
				String realFileName = (String) map.get( "REAL_FILE_NAME" );
				logger.debug( "filePath + realFileName" + filePath + realFileName );
				System.out.println( "filePath + realFileName" + filePath + realFileName);
				System.out.println( "fileName : " + fileName );
				download( request, response, filePath + fileName, realFileName );
			}
		}
	}

	/**
	 * 신청상태 수정
	 * 
	 * @param model
	 * @param commandMap
	 * @param mreq
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd02Update1.page" )
	public String fdcrAd02Update1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		String WORKS_ID = EgovWebUtil.getString( commandMap, "WORKS_ID" );
		String STAT_CD = EgovWebUtil.getString( commandMap, "STAT_CD" );
		String RSLT_CD = EgovWebUtil.getString( commandMap, "RSLT_CD" );
		String STAT_MEMO = EgovWebUtil.getString( commandMap, "STAT_MEMO" );
		String RGST_IDNT = ConsoleLoginUser.getUserId();
		
		String[] worksIds = new String[]{WORKS_ID};
		String[] statCds = new String[]{STAT_CD};
		String[] rsltCds = new String[]{RSLT_CD};

		commandMap.put( "worksIds", worksIds );
		commandMap.put( "statCds", statCds );
		commandMap.put( "rsltCds", rsltCds );
		commandMap.put( "STAT_MEMO", STAT_MEMO );
		commandMap.put( "RGST_IDNT", RGST_IDNT );

		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAd02Service.fdcrAd02Update1( commandMap, fileinfo );
		if( isSuccess ){
		     
		     HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		     String ip = req.getHeader("X-FORWARDED-FOR");
		     if (ip == null) {
		          ip = req.getRemoteAddr();
		     }
		     String USER_IDNT = request.getParameter("USER_IDNT");
		     Map logMap = new HashMap();
		     logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
		     logMap.put("PROC_STATUS", "상당한 노력 신청접수" );
		     logMap.put("PROC_ID",  "상당한 노력 신청접수 / 신청상태 수정");
		     logMap.put("MENU_URL", request.getRequestURI());
		     logMap.put("IP_ADDRESS", ip);

		     adminCommonDao.insertAdminLogDo(logMap);
		     
			return returnUrl(
				model,
				"수정되었습니다.",
				"/console/effort/fdcrAd02UpdateForm1.page?WORKS_ID=" + WORKS_ID );
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/effort/fdcrAd02UpdateForm1.page?WORKS_ID=" + WORKS_ID );
		}
	}

	/**
	 * 상당한노력신청 - 저작권자 조회공고 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd02Delete1.page" )
	public String fdcrAd02Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		boolean isSuccess = fdcrAd02Service.fdcrAd02Delete1( commandMap );
		if( isSuccess ){
			return returnUrl(
				model,
				"수정되었습니다.",
				"/console/effort/fdcrAd02List1.page" );
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/effort/fdcrAd02List1.page" );
		}
	}

}
