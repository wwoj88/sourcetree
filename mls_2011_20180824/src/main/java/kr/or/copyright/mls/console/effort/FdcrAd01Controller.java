package kr.or.copyright.mls.console.effort;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.mls.adminCommon.dao.AdminCommonDao;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Service;
import kr.or.copyright.mls.console.legal.inter.FdcrAd10Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 상당한노력(신청 및 조회공고) > 저작권자 조회공고(직접수행)
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd01Controller extends DefaultController{

	@Resource( name = "fdcrAd01Service" )
	private FdcrAd01Service fdcrAd01Service;
	
	@Resource( name = "fdcrAd10Service" )
	private FdcrAd10Service fdcrAd10Service;
	
	
  @Resource(name = "adminCommonDao")
  private AdminCommonDao adminCommonDao;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd01Controller.class );

	/**
	 * 저작권자 조회 공고 게시판 목록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd01List1.page" )
	public String fdcrAd01List1( ModelMap model,		Map<String, Object> commandMap,		HttpServletRequest request,		HttpServletResponse response ) throws Exception{
		String tabIndex = EgovWebUtil.getString( commandMap, "TAB_INDEX" );
		if( "".equals( tabIndex ) ){
			tabIndex = "2";
		}
		commandMap.put( "TAB_INDEX", tabIndex );
		model.addAttribute( "page", request.getParameter("PAGE") );
		
		model.addAttribute( "commandMap", commandMap );
		return "effort/fdcrAd01List1.tiles";
	}

	/**
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd01List2.page" )
	public String fdcrAd01List2( ModelMap model,		Map<String, Object> commandMap,		HttpServletRequest request,		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd01List2 Start" );
		EgovWebUtil.ajaxCommonEncoding( request, commandMap );
		// 파라미터 셋팅
		commandMap.put( "BORD_CD", "1" ); // 게시판 CD
		String tabIndex = EgovWebUtil.getString( commandMap, "TAB_INDEX" );
		if( "".equals( tabIndex ) ){
			tabIndex = "2";
		}
		commandMap.put( "TAB_INDEX", tabIndex );

		fdcrAd01Service.fdcrAd01List1( commandMap );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		model.addAttribute( "ds_count0", ( (List) commandMap.get( "ds_count" ) ).get( 2 ) );
		model.addAttribute( "ds_count1", ( (List) commandMap.get( "ds_count" ) ).get( 0 ) );
		model.addAttribute( "ds_count2", ( (List) commandMap.get( "ds_count" ) ).get( 1 ) );
		model.addAttribute( "ds_count3", ( (List) commandMap.get( "ds_count" ) ).get( 3 ) );
		
		logger.debug( "fdcrAd01List2 End" );
		return "effort/fdcrAd01List2";
	}

	/**
	 * 저작권자 조회 공고 게시판 상세 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd01View1.page" )
	public String fdcrAd01View1( ModelMap model,

		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{		logger.debug( "fdcrAd01View1 Start" );

		// 파라미터 셋팅
		commandMap.put( "BORD_CD", "1" );
		commandMap.put( "BIG_CODE", "88" );

		fdcrAd01Service.fdcrAd01View1( commandMap );
		
		
		
		String USER_IDNT = request.getParameter("USER_IDNT");
    HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    String ip = req.getHeader("X-FORWARDED-FOR");
    if (ip == null) {
         ip = req.getRemoteAddr();
    }
    Map logMap = new HashMap();
    

    logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
    logMap.put("PROC_STATUS", "저작권자 조회공고 (직접수행)" );
    logMap.put("PROC_ID",   "저작권자 조회공고 상세조회");
    logMap.put("MENU_URL", request.getRequestURI());
    logMap.put("IP_ADDRESS", ip);

    adminCommonDao.insertAdminLogDo(logMap);
		
		
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "detailList", commandMap.get( "detailList" ) );
		model.addAttribute( "fileList", commandMap.get( "fileList" ) );
		model.addAttribute( "objectList", commandMap.get( "objectList" ) );
		model.addAttribute( "objectFileList", commandMap.get( "objectFileList" ) );
		model.addAttribute( "suplList", commandMap.get( "suplList" ) );
		model.addAttribute( "suplListCount", commandMap.get( "suplListCount" ) );
    model.addAttribute( "page", request.getParameter("PAGE") );
		logger.debug( "fdcrAd01View1 End" );
		return "effort/fdcrAd01View1.tiles";
	}

	/**
	 * 보상금 공탁 공고 게시판 - 공고승인
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd01Update1.page" )
	public String fdcrAd01Update1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd01Update1 Start" );
		// 파라미터 셋팅
		commandMap.put( "OPEN_IDNT", ConsoleLoginUser.getUserId());
		boolean isSuccess = fdcrAd01Service.fdcrAd01Update1( commandMap );
		logger.debug( "fdcrAd01Update1 End" );
		if( isSuccess ){
		     
		     String USER_IDNT = request.getParameter("USER_IDNT");
		     HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		     String ip = req.getHeader("X-FORWARDED-FOR");
		     if (ip == null) {
		          ip = req.getRemoteAddr();
		     }
		     Map logMap = new HashMap();
		     System.out.println(request.getRequestURI());

		     logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
		     logMap.put("PROC_STATUS", "저작권자 조회공고 (직접수행)" );
		     logMap.put("PROC_ID",  "저작권자 조회공고 승인");
		     logMap.put("MENU_URL", request.getRequestURI());
		     logMap.put("IP_ADDRESS", ip);

		     adminCommonDao.insertAdminLogDo(logMap);
		     
		     
			return returnUrl(	model,"공고승인 완료되었습니다.",	"/console/effort/fdcrAd01View1.page?BORD_CD=1&BORD_SEQN=" + commandMap.get( "BORD_SEQN" ) );
		}else{
			return returnUrl(	model,		"실패했습니다.",	"/console/effort/fdcrAd01View1.page?BORD_CD=1&BORD_SEQN=" + commandMap.get( "BORD_SEQN" ) );
		}
	}

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 상세보기(보완처리용)
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd01Pop1.page" )
	public String fdcrAd01Pop1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd01Pop1 Start" );
		// 파라미터 셋팅
		fdcrAd01Service.fdcrAd01View2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_file", commandMap.get( "ds_file" ) );
		model.addAttribute( "ds_code", commandMap.get( "ds_code" ) );
		model.addAttribute( "ds_genre", commandMap.get( "ds_genre" ) );
		model.addAttribute( "commandMap", commandMap );
		logger.debug( "fdcrAd01Pop1 End" );
		return "effort/fdcrAd01Pop1.popup";
	}

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 삭제
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd01Delete1.page" )
	public String fdcrAd01Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd01Delete1 Start" );
		// 파라미터 셋팅
		boolean isSuccess = fdcrAd01Service.fdcrAd01Delete1( commandMap );
		logger.debug( "fdcrAd01Delete1 End" );
		if( isSuccess ){
		     String USER_IDNT = request.getParameter("USER_IDNT");
         HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
         String ip = req.getHeader("X-FORWARDED-FOR");
         if (ip == null) {
              ip = req.getRemoteAddr();
         }
         Map logMap = new HashMap();
         System.out.println(request.getRequestURI());

         logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
         logMap.put("PROC_STATUS", "저작권자 조회공고 (직접수행)" );
         logMap.put("PROC_ID",  "저작권자 조회공고 삭제");
         logMap.put("MENU_URL", request.getRequestURI());
         logMap.put("IP_ADDRESS", ip);

         adminCommonDao.insertAdminLogDo(logMap);
			return returnUrl(
				model,
				"삭제했습니다.",
				"/console/effort/fdcrAd01List1.page?BORD_CD=1");
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/effort/fdcrAd01View1.page?BORD_CD=1&BORD_SEQN=" + commandMap.get( "BORD_SEQN" ) );
		}
	}

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 처리상태 수정(신청,보완,신청승인,반려)
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd01Update2.page" )
	public String fdcrAd01Update2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd01Update2 Start" );
		boolean isSuccess = fdcrAd01Service.fdcrAd01Update2( commandMap );
		logger.debug( "fdcrAd01Update2 End" );
		if( isSuccess ){
		     String USER_IDNT = request.getParameter("USER_IDNT");
         HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
         String ip = req.getHeader("X-FORWARDED-FOR");
         if (ip == null) {
              ip = req.getRemoteAddr();
         }
         Map logMap = new HashMap();
         System.out.println(request.getRequestURI());

         logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
         logMap.put("PROC_STATUS", "저작권자 조회공고 (직접수행)" );
         logMap.put("PROC_ID",  "저작권자 조회공고 수정");
         logMap.put("MENU_URL", request.getRequestURI());
         logMap.put("IP_ADDRESS", ip);

         adminCommonDao.insertAdminLogDo(logMap);
			return returnUrl(
				model,
				"저장했습니다.",
				"/console/effort/fdcrAd01View1.page?BORD_CD=1&BORD_SEQN=" + commandMap.get( "BORD_SEQN" ) );
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/effort/fdcrAd01View1.page?BORD_CD=1&BORD_SEQN=" + commandMap.get( "BORD_SEQN" ) );
		}
	}
	
	/**
	 * 보상금 공탁공고 게시판 공고내용 수정 및 보완이력 등록
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd01Update3.page" )
	public String fdcrAd01Update3( Map<String, Object> commandMap,
		ModelMap model,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 기존 컬럼
		commandMap.put( "BIG_CODE", "88" );
		ArrayList<Map<String,Object>> codeList = fdcrAd01Service.selectCdList( commandMap );
		Map<String, Object> info = fdcrAd10Service.fdcrAd10Detail( commandMap );

		String BORD_DESC = EgovWebUtil.getString( info, "BORD_DESC" );
		String ANUC_ITEM_1 = EgovWebUtil.getString( info, "ANUC_ITEM_1" );
		String ANUC_ITEM_2 = EgovWebUtil.getString( info, "ANUC_ITEM_2" );
		String ANUC_ITEM_3 = EgovWebUtil.getString( info, "ANUC_ITEM_3" );
		String GENRE_CD = EgovWebUtil.getString( info, "GENRE_CD" );
		String ANUC_ITEM_4 = EgovWebUtil.getString( info, "ANUC_ITEM_4" );
		String ANUC_ITEM_5 = EgovWebUtil.getString( info, "ANUC_ITEM_5" );
		String ANUC_ITEM_6 = EgovWebUtil.getString( info, "ANUC_ITEM_6" );
		String ANUC_ITEM_7 = EgovWebUtil.getString( info, "ANUC_ITEM_7" );
		String ANUC_ITEM_8 = EgovWebUtil.getString( info, "ANUC_ITEM_8" );
		String ANUC_ITEM_9 = EgovWebUtil.getString( info, "ANUC_ITEM_9" );
		String ANUC_ITEM_10 = EgovWebUtil.getString( info, "ANUC_ITEM_10" );
		String ANUC_ITEM_11 = EgovWebUtil.getString( info, "ANUC_ITEM_11" );
		String ANUC_ITEM_12 = EgovWebUtil.getString( info, "ANUC_ITEM_12" );

		// 넘긴 파라미터
		String PARAM_BORD_DESC = EgovWebUtil.getString( commandMap, "BORD_DESC" );
		String PARAM_ANUC_ITEM_1 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_1" );
		String PARAM_ANUC_ITEM_2 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_2" );
		String PARAM_ANUC_ITEM_3 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_3" );
		String PARAM_GENRE_CD = EgovWebUtil.getString( commandMap, "GENRE_CD" );
		String PARAM_ANUC_ITEM_4 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_4" );
		String PARAM_ANUC_ITEM_5 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_5" );
		String PARAM_ANUC_ITEM_6 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_6" );
		String PARAM_ANUC_ITEM_7 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_7" );
		String PARAM_ANUC_ITEM_8 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_8" );
		String PARAM_ANUC_ITEM_9 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_9" );
		String PARAM_ANUC_ITEM_10 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_10" );
		String PARAM_ANUC_ITEM_11 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_11" );
		String PARAM_ANUC_ITEM_12 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_12" );

		ArrayList<Map<String, Object>> suplList = new ArrayList<Map<String, Object>>();
		if( !PARAM_BORD_DESC.equals( BORD_DESC ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", BORD_DESC );
			param.put( "post_item", PARAM_BORD_DESC );
			param.put( "supl_item_cd", "10" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 10 ) );
			suplList.add( param );
		}
		if( !PARAM_ANUC_ITEM_1.equals( ANUC_ITEM_1 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_1 );
			param.put( "post_item", PARAM_ANUC_ITEM_1 );
			param.put( "supl_item_cd", "21" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 21 ) );
			suplList.add( param );
		}
		if( !PARAM_ANUC_ITEM_2.equals( ANUC_ITEM_2 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_2 );
			param.put( "post_item", PARAM_ANUC_ITEM_2 );
			param.put( "supl_item_cd", "22" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 22 ) );
			suplList.add( param );
		}
		if( !PARAM_ANUC_ITEM_3.equals( ANUC_ITEM_3 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_3 );
			param.put( "post_item", PARAM_ANUC_ITEM_3 );
			param.put( "supl_item_cd", "23" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 23 ) );
			suplList.add( param );
		}
		
		if( !PARAM_GENRE_CD.equals( GENRE_CD ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", GENRE_CD );
			param.put( "post_item", PARAM_GENRE_CD );
			param.put( "supl_item_cd", "31" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 31 ) );
			suplList.add( param );
		}
		
		if( !PARAM_ANUC_ITEM_4.equals( ANUC_ITEM_4 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_4 );
			param.put( "post_item", PARAM_ANUC_ITEM_4 );
			param.put( "supl_item_cd", "32" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 32 ) );
			suplList.add( param );
		}
		
		if( !PARAM_ANUC_ITEM_5.equals( ANUC_ITEM_5 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_5 );
			param.put( "post_item", PARAM_ANUC_ITEM_5 );
			param.put( "supl_item_cd", "40" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 40 ) );
			suplList.add( param );
		}
		
		if( !PARAM_ANUC_ITEM_6.equals( ANUC_ITEM_6 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_6 );
			param.put( "post_item", PARAM_ANUC_ITEM_6 );
			param.put( "supl_item_cd", "51" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 51 ) );
			suplList.add( param );
		}
		
		if( !PARAM_ANUC_ITEM_7.equals( ANUC_ITEM_7 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_7 );
			param.put( "post_item", PARAM_ANUC_ITEM_7 );
			param.put( "supl_item_cd", "52" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 52 ) );
			suplList.add( param );
		}
		
		if( !PARAM_ANUC_ITEM_8.equals( ANUC_ITEM_8 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_8 );
			param.put( "post_item", PARAM_ANUC_ITEM_8 );
			param.put( "supl_item_cd", "60" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 60 ) );
			suplList.add( param );
		}
		
		if( !PARAM_ANUC_ITEM_9.equals( ANUC_ITEM_9 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_9 );
			param.put( "post_item", PARAM_ANUC_ITEM_9 );
			param.put( "supl_item_cd", "81" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 81 ) );
			suplList.add( param );
		}
		
		if( !PARAM_ANUC_ITEM_10.equals( ANUC_ITEM_10 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_10 );
			param.put( "post_item", PARAM_ANUC_ITEM_10 );
			param.put( "supl_item_cd", "82" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 82 ) );
			suplList.add( param );
		}
		
		if( !PARAM_ANUC_ITEM_11.equals( ANUC_ITEM_11 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_11 );
			param.put( "post_item", PARAM_ANUC_ITEM_11 );
			param.put( "supl_item_cd", "83" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 83 ) );
			suplList.add( param );
		}
		
		if( !PARAM_ANUC_ITEM_12.equals( ANUC_ITEM_12 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_12 );
			param.put( "post_item", PARAM_ANUC_ITEM_12 );
			param.put( "supl_item_cd", "84" );
			param.put( "supl_item", EgovWebUtil.getCodeName( codeList, 84 ) );
			suplList.add( param );
		}
		
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		commandMap.put( "RGST_NAME", ConsoleLoginUser.getUserName() );
		commandMap.put( "SEND_MAIL_ADDR", ConsoleLoginUser.getUserEmail() );
		commandMap.put( "RECV_MAIL_ADDR", EgovWebUtil.getString( info, "RECV_MAIL_ADDR" ) );
		
		if( suplList.size() != 0 ){
			commandMap.put( "suplList", suplList );

			boolean isSuccess = fdcrAd10Service.fdcrAd10SuplRegi1( commandMap );
			if( isSuccess ){
				return returnUrl(
					model,
					suplList.size()+"건의 데이터가 보완처리되었습니다.","");
			}else{
				return returnUrl(
					model,
					"실패했습니다.",
					"/console/effort/fdcrAd01Pop1.page?BORD_CD=" + EgovWebUtil.getString( commandMap, "BORD_CD" )
						+ "&BORD_SEQN=" + EgovWebUtil.getString( commandMap, "BORD_SEQN" ) );
			}
		}else{
			return returnUrl(
				model,
				"보완처리 할 내용이 없습니다.",
				"/console/effort/fdcrAd01Pop1.page?BORD_CD=" + EgovWebUtil.getString( commandMap, "BORD_CD" )
					+ "&BORD_SEQN=" + EgovWebUtil.getString( commandMap, "BORD_SEQN" ) );
		}
	}
}
