package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;


public interface FdcrAd93Service{
	/**
	 * 저작권찾기신청 및 처리통계(임시)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd93List1( Map<String, Object> commandMap ) throws Exception;
}
