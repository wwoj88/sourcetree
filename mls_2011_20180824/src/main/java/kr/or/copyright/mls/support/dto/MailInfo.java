package kr.or.copyright.mls.support.dto;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
	
public class MailInfo {
	
	private boolean isSuccess;
	private String email;
	private String emailName;
	private String host;
	private String subject;
	private String message;
	private String from;
	private String fromName;
	
	private String arrEmail[];
	private String arrEmailName[];
	
	private String prpsDivsName = "";
	private String prpsTitle = "";
	private String prpsName = "";
	private String prpsEmail = "";
	private String prpsSms = "";
	
	private Date sendDttm;
	private String mailReadHtml="";//메일 수신확인 태그
	private String mailRejcCd="";//메일수신거부코드
	private String detlYn="";//사이트 상세보기 YN
	
	
	

	public String getDetlYn() {
		return detlYn;
	}


	public void setDetlYn(String detlYn) {
		this.detlYn = detlYn;
	}


	public String toString() {
        StringBuffer tempBuffer = new StringBuffer();
        tempBuffer.append("\n [" + this.getClass().getName() + "]");
        Field[] myField = this.getClass().getDeclaredFields();

        for (int i = 0; i < myField.length; i++) {
            try {
                if (myField[i].getType().isArray()) {
                    String className = myField[i].getType().getName();
                    className = className.substring(2, className.length() - 1);
                    
                    if(myField[i].get(this) != null) {
	                    tempBuffer.append("\n " + className + "[] ")
	                        .append(myField[i].getName())
	                        .append(" = " + Arrays.asList( (Object[]) myField[i].get(this)));
                    }else{
                    	tempBuffer.append("\n " + className + "[] ")
                        .append(myField[i].getName())
                        .append(" = null" );
                    }
                    
                } else if (myField[i].getType().isPrimitive() ||
                           myField[i].getType() == String.class) {

                    String typeName = myField[i].getType().getName();
                    typeName = ( (myField[i].getType() == String.class) ? "String" : typeName);
                    tempBuffer.append("\n " + typeName + " ")
                        .append(myField[i].getName())
                        .append(" = [" + myField[i].get(this) + "]");
                } else if (myField[i].getType() == Class.class) {
                    // ignore
                } else {

                    String className = myField[i].getType().getName();
                    tempBuffer.append("\n " + className + " ")
                        .append(myField[i].getName())
                        .append(" = [" + myField[i].get(this) + "]");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return tempBuffer.toString();
    }
    
	
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getEmailName() {
		return emailName;
	}


	public void setEmailName(String emailName) {
		this.emailName = emailName;
	}


	public String getFromName() {
		return fromName;
	}


	public void setFromName(String fromName) {
		this.fromName = fromName;
	}
	

	public String getPrpsDivsName() {
		return prpsDivsName;
	}


	public void setPrpsDivsName(String prpsDivsName) {
		this.prpsDivsName = prpsDivsName;
	}


	public String getPrpsEmail() {
		return prpsEmail;
	}


	public void setPrpsEmail(String prpsEmail) {
		this.prpsEmail = prpsEmail;
	}


	public String getPrpsName() {
		return prpsName;
	}


	public void setPrpsName(String prpsName) {
		this.prpsName = prpsName;
	}


	public String getPrpsSms() {
		return prpsSms;
	}


	public void setPrpsSms(String prpsSms) {
		this.prpsSms = prpsSms;
	}


	public String getPrpsTitle() {
		return prpsTitle;
	}


	public void setPrpsTitle(String prpsTitle) {
		this.prpsTitle = prpsTitle;
	}


	public String[] getArrEmail() {
		return arrEmail;
	}


	public void setArrEmail(String[] arrEmail) {
		this.arrEmail = arrEmail;
	}


	public String[] getArrEmailName() {
		return arrEmailName;
	}


	public void setArrEmailName(String[] arrEmailName) {
		this.arrEmailName = arrEmailName;
	}


	
	public Date getSendDttm() {
		return sendDttm;
	}


	public void setSendDttm(Date sendDttm) {
		this.sendDttm = sendDttm;
	}
	
	public String getMailRejcCd() {
		return mailRejcCd;
	}
	public void setMailRejcCd(String mailRejcCd) {
		this.mailRejcCd = mailRejcCd;
	}
	public String getMailReadHtml() {
		return mailReadHtml;
	}
	public void setMailReadHtml(String mailReadHtml) {
		this.mailReadHtml = mailReadHtml;
	}

}
