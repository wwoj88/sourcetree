package kr.or.copyright.mls.console.legal;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd14Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd14Service" )
public class FdcrAd14ServiceImpl extends CommandService implements FdcrAd14Service{

	// AdminStatMgntSqlMapDao
	@Resource( name = "fdcrAd06Dao" )
	private FdcrAd06Dao fdcrAd06Dao;

	// AdminStatBoardSqlMapDao
	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	// AdminCommonSqlMapDao
	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	/**
	 * 결제집결표 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd14List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd06Dao.cPayList( commandMap );
		commandMap.put( "ds_list", list );

	}

}
