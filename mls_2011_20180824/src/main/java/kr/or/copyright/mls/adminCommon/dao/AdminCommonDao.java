package kr.or.copyright.mls.adminCommon.dao;

import java.util.List;
import java.util.Map;

public interface AdminCommonDao {

	public List login(Map map);

	// 로그인 한 관리자 메뉴 조회
	public List selectLoginMenu(Map map);

	// 회원 정보 조회
	public List selectMemberInfo(Map map);
	
	// 관리자 로그 저장
	public void insertAdminLogDo (Map map);
	
}
