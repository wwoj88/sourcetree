package kr.or.copyright.mls.console.legal.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface FdcrAd06Dao{

	// 법정허락 신청목록 목록
	public ArrayList<Map<String, Object>> adminStatPrpsList( Map<String, Object> commandMap );

	// 법정허락 신청목록 COUNT
	public List adminStatPrpsListCount( Map map );

	// 법정허락 이용승인 신청서 조회
	public Map<String, Object> adminStatApplicationSelect( Map<String, Object> commandMap );

	/**
	 * 법정허락신청 첨부서류 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> adminStatAttcFileSelect( Map<String, Object> commandMap ) throws SQLException;
	
	/**
	 * 법정허락신청 첨부서류 정보
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> adminStatAttcFileInfo( Map<String, Object> commandMap ) throws SQLException;

	// 법정허락 신청첨부서류 등록
	public void adminStatAttcFileInsert( Map map );

	// 법정허락 신청첨부서류 삭제
	public void adminStatAttcFileDel( Map map );

	// 법정허락 이용승인신청 명세서 목록
	public ArrayList<Map<String, Object>> adminStatApplyWorksSelect( Map<String, Object> commandMap )throws SQLException;
	
	// 법정허락 이용승인신청 명세서 상세
	public ArrayList<Map<String, Object>> adminStatApplyWorksSelectInfo( Map<String, Object> commandMap )throws SQLException;

	// 법정허락 이용승인신청 진행상태 내역
	public List adminStatApplicationShisSelect( Map map );

	// 법정허락 이용승인신청 심의결과상태 내역
	public List adminStatApplyWorksShisSelect( Map map );

	// 접수번호 시퀀스 조회
	public String adminStatApplicationReceiptSeqSelect();

	// 이용승인 신청서 수정
	public void adminStatApplicationUpdate( Map map );

	// 이용승인 신청서 상태변경내역 등록
	public void adminStatApplicationShisInsert( Map map );

	// 첨부파일 등록
	public void adminFileInsert( Map map );

	// 첨부파일 삭제
	public void adminFileDel( Map map );

	// 이용승인신청 명세서 수정
	public void adminStatApplyWorksUpdate( Map map );

	// 이용승인신청 명세서 상태변경내역 등록
	public void adminStatApplyWorksShisInsert( Map map );

	// adminStatPrpsRegi 법정허락 관리자 등록 20120829 정병호
	public void adminStatPrpsRegi( Map map );

	// adminStatPrpsUpte 이용승인신청서 수정 2014 11 06 이병원
	public void adminStatPrpsUpte( Map map );

	// adminStatPrpsShisRegi2 신청서 상태변경내역 수정 2014 11 21 이병원
	public void adminStatPrpsShisRegi2( Map map );

	// adminStatWorksDel 명세서 수정 2014 11 06 이병
	public void adminStatWorksDel( Map map );

	// 명세서 WORKS_SEQN 조회
	public int adminStatWorksSeqn( Map map );

	// adminStatPrpsRegiSelect 법정허락 데이터 중복확인 20141020 이병원
	public int adminStatPrpsRegiSelect( Map map );

	// adminStatPrpsRegiAll
	public void adminStatPrpsRegiAll( Map map );

	// adminStatWorksInsertAll
	public void adminStatWorksInsertAll( Map map );

	// 법정허락신청 이용승인신청명세서 등록 20120831 정병호
	public void adminStatWorksInsert( Map map );

	// adminStatPrpsRegi 법정허락 관리자 이용승인신청서 상태변경내역 등록 20120829 정병호
	public void adminStatPrpsShisRegi( Map map );

	// adminStatPrpsShisRegiAll 법정허락 승인신청서 일괄등록 20141020 이병원
	public void adminStatPrpsShisRegiAll( Map map );

	// 법정허락 결제집결표 목록
	public List cPayList( Map map );

	// 이용승인 신청서 수정진행상태
	public void updateStatApplicationStat( Map map );

	// 이용승인신청서 SHIS 등록
	public void insertStatApplicationShis( Map map );

	
	//2017년 신규 adminStatPrpsRegiAll
	public void adminStatPrpsRegiAllNew( Map<String, Object> param );

	//2017년 신규 법정허락 관리자 이용승인신청서 상태변경내역 등록
	public void adminStatPrpsShisRegiAllNew( Map<String, Object> param );

	//2017년 신규 법정허락 이용승인신청서 명세서 일괄등록
	public void adminStatWorksInsertAllNew( Map<String, Object> param );
	
	//2017년 신규 법정허락 이용승인신청서 등록
	public void adminStatPrpsRegiNew(Map map);
	
	//2017년 신규 법정허락 상태변경내역 등록
	public void adminStatPrpsShisRegiNew(Map map);

	public void adminStatWorksInsertNew( Map map );

}
