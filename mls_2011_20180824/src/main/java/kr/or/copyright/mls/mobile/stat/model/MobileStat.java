package kr.or.copyright.mls.mobile.stat.model;


public class MobileStat {
	private int statObjcId;		//이의제기 구분
	private int bordCd;		//게시판 구분CD
	private int bordSeqn;		//게시판 순번
	private String openYn;		//공고 여부
	private String openDttm;	//공고 날짜
	private String rgstIdnt;	//등록자 ID
	private String rgstDttm;	//등록 날짜
	private String modiIdnt;	//수정자 ID
	private String modiDttm;	//수정 날짜
	private String tite;		//제목
	private String bordDesc;	//내용
	private String deltYsno;	//삭제유무
	private String objcYn;		//이의제기 신청 여부
	private int worksId;		//저작물 ID
	private String receiptNo;	//이용승인신청접수번호
	private int divsCd;		//저작물 구분CD
	private int inqrCont;		//조회수
	private String anucItem1;	//공고항목1
	private String anucItem2;
	private String anucItem3;
	private String anucItem4;
	private String anucItem5;
	private String anucItem6;
	private String anucItem7;
	private String anucItem8;
	private String anucItem9;
	private String anucItem10;
	private String anucItem11;
	private String anucItem12;
	private String anucItem13;
	private String anucItem14;
	private String anucItem15;	//공고항목 15
	private int genreCd;		//장르
	private String genreCdName;	//장르 Name
	private String divsCdName; 	//구분 Name

	//	페이징
	private int nowPage;  
	private int startRow; 
	private int endRow; 
	private int totalRow;
	private int rowNo;
	
	@Override
	public String toString() {
		return "MobileStat [statObjcId=" + statObjcId + ", bordCd=" + bordCd
				+ ", bordSeqn=" + bordSeqn + ", openYn=" + openYn
				+ ", openDttm=" + openDttm + ", rgstIdnt=" + rgstIdnt
				+ ", rgstDttm=" + rgstDttm + ", modiIdnt=" + modiIdnt
				+ ", modiDttm=" + modiDttm + ", tite=" + tite + ", bordDesc="
				+ bordDesc + ", deltYsno=" + deltYsno + ", objcYn=" + objcYn
				+ ", worksId=" + worksId + ", receiptNo=" + receiptNo
				+ ", divsCd=" + divsCd + ", inqrCont=" + inqrCont
				+ ", anucItem1=" + anucItem1 + ", anucItem2=" + anucItem2
				+ ", anucItem3=" + anucItem3 + ", anucItem4=" + anucItem4
				+ ", anucItem5=" + anucItem5 + ", anucItem6=" + anucItem6
				+ ", anucItem7=" + anucItem7 + ", anucItem8=" + anucItem8
				+ ", anucItem9=" + anucItem9 + ", anucItem10=" + anucItem10
				+ ", anucItem11=" + anucItem11 + ", anucItem12=" + anucItem12
				+ ", anucItem13=" + anucItem13 + ", anucItem14=" + anucItem14
				+ ", anucItem15=" + anucItem15 + ", genreCd=" + genreCd
				+ ", genreCdName=" + genreCdName + ", divsCdName=" + divsCdName
				+ ", nowPage=" + nowPage + ", startRow=" + startRow
				+ ", endRow=" + endRow + ", totalRow=" + totalRow + ", rowNo="
				+ rowNo + ", getStatObjcId()=" + getStatObjcId()
				+ ", getBordCd()=" + getBordCd() + ", getBordSeqn()="
				+ getBordSeqn() + ", getOpenYn()=" + getOpenYn()
				+ ", getOpenDttm()=" + getOpenDttm() + ", getRgstIdnt()="
				+ getRgstIdnt() + ", getRgstDttm()=" + getRgstDttm()
				+ ", getModiIdnt()=" + getModiIdnt() + ", getModiDttm()="
				+ getModiDttm() + ", getTite()=" + getTite()
				+ ", getBordDesc()=" + getBordDesc() + ", getDeltYsno()="
				+ getDeltYsno() + ", getObjcYn()=" + getObjcYn()
				+ ", getWorksId()=" + getWorksId() + ", getReceiptNo()="
				+ getReceiptNo() + ", getDivsCd()=" + getDivsCd()
				+ ", getInqrCont()=" + getInqrCont() + ", getAnucItem1()="
				+ getAnucItem1() + ", getAnucItem2()=" + getAnucItem2()
				+ ", getAnucItem3()=" + getAnucItem3() + ", getAnucItem4()="
				+ getAnucItem4() + ", getAnucItem5()=" + getAnucItem5()
				+ ", getAnucItem6()=" + getAnucItem6() + ", getAnucItem7()="
				+ getAnucItem7() + ", getAnucItem8()=" + getAnucItem8()
				+ ", getAnucItem9()=" + getAnucItem9() + ", getAnucItem10()="
				+ getAnucItem10() + ", getAnucItem11()=" + getAnucItem11()
				+ ", getAnucItem12()=" + getAnucItem12() + ", getAnucItem13()="
				+ getAnucItem13() + ", getAnucItem14()=" + getAnucItem14()
				+ ", getAnucItem15()=" + getAnucItem15() + ", getGenreCd()="
				+ getGenreCd() + ", getGenreCdName()=" + getGenreCdName()
				+ ", getDivsCdName()=" + getDivsCdName() + ", getNowPage()="
				+ getNowPage() + ", getStartRow()=" + getStartRow()
				+ ", getEndRow()=" + getEndRow() + ", getTotalRow()="
				+ getTotalRow() + ", getRowNo()=" + getRowNo()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	public int getStatObjcId() {
		return statObjcId;
	}
	public void setStatObjcId(int statObjcId) {
		this.statObjcId = statObjcId;
	}
	public int getBordCd() {
		return bordCd;
	}
	public void setBordCd(int bordCd) {
		this.bordCd = bordCd;
	}
	public int getBordSeqn() {
		return bordSeqn;
	}
	public void setBordSeqn(int bordSeqn) {
		this.bordSeqn = bordSeqn;
	}
	public String getOpenYn() {
		return openYn;
	}
	public void setOpenYn(String openYn) {
		this.openYn = openYn;
	}
	public String getOpenDttm() {
		return openDttm;
	}
	public void setOpenDttm(String openDttm) {
		this.openDttm = openDttm;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	public String getModiIdnt() {
		return modiIdnt;
	}
	public void setModiIdnt(String modiIdnt) {
		this.modiIdnt = modiIdnt;
	}
	public String getModiDttm() {
		return modiDttm;
	}
	public void setModiDttm(String modiDttm) {
		this.modiDttm = modiDttm;
	}
	public String getTite() {
		return tite;
	}
	public void setTite(String tite) {
		this.tite = tite;
	}
	public String getBordDesc() {
		return bordDesc;
	}
	public void setBordDesc(String bordDesc) {
		this.bordDesc = bordDesc;
	}
	public String getDeltYsno() {
		return deltYsno;
	}
	public void setDeltYsno(String deltYsno) {
		this.deltYsno = deltYsno;
	}
	public String getObjcYn() {
		return objcYn;
	}
	public void setObjcYn(String objcYn) {
		this.objcYn = objcYn;
	}
	public int getWorksId() {
		return worksId;
	}
	public void setWorksId(int worksId) {
		this.worksId = worksId;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public int getDivsCd() {
		return divsCd;
	}
	public void setDivsCd(int divsCd) {
		this.divsCd = divsCd;
	}
	public int getInqrCont() {
		return inqrCont;
	}
	public void setInqrCont(int inqrCont) {
		this.inqrCont = inqrCont;
	}
	public String getAnucItem1() {
		return anucItem1;
	}
	public void setAnucItem1(String anucItem1) {
		this.anucItem1 = anucItem1;
	}
	public String getAnucItem2() {
		return anucItem2;
	}
	public void setAnucItem2(String anucItem2) {
		this.anucItem2 = anucItem2;
	}
	public String getAnucItem3() {
		return anucItem3;
	}
	public void setAnucItem3(String anucItem3) {
		this.anucItem3 = anucItem3;
	}
	public String getAnucItem4() {
		return anucItem4;
	}
	public void setAnucItem4(String anucItem4) {
		this.anucItem4 = anucItem4;
	}
	public String getAnucItem5() {
		return anucItem5;
	}
	public void setAnucItem5(String anucItem5) {
		this.anucItem5 = anucItem5;
	}
	public String getAnucItem6() {
		return anucItem6;
	}
	public void setAnucItem6(String anucItem6) {
		this.anucItem6 = anucItem6;
	}
	public String getAnucItem7() {
		return anucItem7;
	}
	public void setAnucItem7(String anucItem7) {
		this.anucItem7 = anucItem7;
	}
	public String getAnucItem8() {
		return anucItem8;
	}
	public void setAnucItem8(String anucItem8) {
		this.anucItem8 = anucItem8;
	}
	public String getAnucItem9() {
		return anucItem9;
	}
	public void setAnucItem9(String anucItem9) {
		this.anucItem9 = anucItem9;
	}
	public String getAnucItem10() {
		return anucItem10;
	}
	public void setAnucItem10(String anucItem10) {
		this.anucItem10 = anucItem10;
	}
	public String getAnucItem11() {
		return anucItem11;
	}
	public void setAnucItem11(String anucItem11) {
		this.anucItem11 = anucItem11;
	}
	public String getAnucItem12() {
		return anucItem12;
	}
	public void setAnucItem12(String anucItem12) {
		this.anucItem12 = anucItem12;
	}
	public String getAnucItem13() {
		return anucItem13;
	}
	public void setAnucItem13(String anucItem13) {
		this.anucItem13 = anucItem13;
	}
	public String getAnucItem14() {
		return anucItem14;
	}
	public void setAnucItem14(String anucItem14) {
		this.anucItem14 = anucItem14;
	}
	public String getAnucItem15() {
		return anucItem15;
	}
	public void setAnucItem15(String anucItem15) {
		this.anucItem15 = anucItem15;
	}
	public int getGenreCd() {
		return genreCd;
	}
	public void setGenreCd(int genreCd) {
		this.genreCd = genreCd;
	}
	public String getGenreCdName() {
		return genreCdName;
	}
	public void setGenreCdName(String genreCdName) {
		this.genreCdName = genreCdName;
	}
	public String getDivsCdName() {
		return divsCdName;
	}
	public void setDivsCdName(String divsCdName) {
		this.divsCdName = divsCdName;
	}
	public int getNowPage() {
		return nowPage;
	}
	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
	public int getTotalRow() {
		return totalRow;
	}
	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}
	public int getRowNo() {
		return rowNo;
	}
	public void setRowNo(int rowNo) {
		this.rowNo = rowNo;
	}
	
	
}
