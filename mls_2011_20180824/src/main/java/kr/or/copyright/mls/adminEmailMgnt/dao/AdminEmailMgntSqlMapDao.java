package kr.or.copyright.mls.adminEmailMgnt.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.ibatis.sqlmap.client.SqlMapClient;

public class AdminEmailMgntSqlMapDao extends SqlMapClientDaoSupport implements AdminEmailMgntDao {

	//이메일 주소록 그룹 조회
	public List selectEmailAddrGroupList(Map map){
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectEmailAddrGroupList",map);		
	}
	
	
	//이메일 주소록 조회
	public List selectEmailAddrList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectEmailAddrList",map);
	}	
	//이메일 메세지함 SEQ 번호 max+1 조회
	public int selectMaxMsgIdSeq() {
		return (Integer) getSqlMapClientTemplate().queryForObject("AdminEmailMgnt.selectMaxMsgIdSeq");
	}
	
	// 이메일 작성내용 저장
	public void insertEmailMsg(Map map) {
		getSqlMapClientTemplate().insert("AdminEmailMgnt.insertEmailMsg",map);
		
	}
	// 이메일 수신자 정보 저장
	public void insertEmailRecvInfo(Map map){
		getSqlMapClientTemplate().insert("AdminEmailMgnt.insertEmailRecvInfo",map);
	}	
	
	// 이메일 메시지 목록 조회
	public List selectEmailMsgList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectEmailMsgList",map);
	}
	// 이메일 메세지 상세보기
	public List selectEmailMsg(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectEmailMsg",map);
	}
	//이메일 메시지 상세보기 주소록 리스트 조회
	public List selectEmailAddrListByMsgId(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectEmailAddrListByMsgId",map);

	}//이메일 메시지 수정
	public void updateEmailMsg(Map map) {
		getSqlMapClientTemplate().update("AdminEmailMgnt.updateEmailMsg",map);

	}
	//이메일 메시지 주소록 삭제
	public boolean deleteEmailRecvInfo(Map map) throws SQLException {
		boolean bFlag = true;
		try{
			getSqlMapClientTemplate().delete("AdminEmailMgnt.deleteEmailRecvInfo",map);
			
			bFlag = true;
			
		}catch(Exception e) {
			e.printStackTrace();
			bFlag = false;
		}finally{
			return bFlag;
		}	
	}
	//이메일 메세지 삭제
	public void deleteEmailMsg(Map map) {
		getSqlMapClientTemplate().update("AdminEmailMgnt.deleteEmailMsg",map);

	}
	//이메일 전송목록 저장
	public void insertSendList(Map map) {
		getSqlMapClientTemplate().insert("AdminEmailMgnt.insertSendList",map);
		
	}
	
	// 이메일 전송목록 조회
	public List selectSendEmailList() {
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectSendEmailList");
		
	}
	// 이메일 전송목록 상세보기 조회
	public List selectSendEmailView(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectSendEmailView",map);
		
	}

	// 신청정보보완내역 메일발신내역 저장
	public void insertSuplList(Map map){
		
		getSqlMapClientTemplate().insert("AdminEmailMgnt.insertSuplList",map);
	}
	
	public List selectScMailingList(Map map){
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectScMailingList",map);		
	}
	
	public List selectScMailingMsgId(Map map) {
		// TODO Auto-generated method stub
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectScMailingMsgId",map);
	}
	public void insertScMailing(Map map) {
		getSqlMapClientTemplate().insert("AdminEmailMgnt.insertScMailing",map);
		
	}
	public int selectScMailingCount(Map map) {
		
		return (Integer)getSqlMapClientTemplate().queryForObject("AdminEmailMgnt.selectScMailingCount",map);
	}
	public void updateScMailng(Map map) {
		getSqlMapClientTemplate().update("AdminEmailMgnt.updateScMailng",map);
		
	}
	public List selectScMailing(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectScMailing",map);
	}
	
	public void updateSendScMailng(Map map) {
		getSqlMapClientTemplate().update("AdminEmailMgnt.updateSendScMailng",map);
		
	}
	
	public void insertMailAttc(Map map) {
		getSqlMapClientTemplate().insert("AdminEmailMgnt.insertMailAttc",map);
		
	}
	public List selectMailAttcList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminEmailMgnt.selectMailAttcList",map);
	}
	public void deleteMailAttc(Map map) {
		getSqlMapClientTemplate().delete("AdminEmailMgnt.deleteMailAttc",map);
	}
	
	public void updateEndDttmMsg(Map map) {
		getSqlMapClientTemplate().update("AdminEmailMgnt.updateEndDttmMsg",map);
	}
	
	
	
	
}
