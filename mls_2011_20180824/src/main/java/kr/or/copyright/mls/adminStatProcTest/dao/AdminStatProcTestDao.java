package kr.or.copyright.mls.adminStatProcTest.dao;

import java.util.List;
import java.util.Map;


public interface AdminStatProcTestDao {
	
	public List selectStatProcAllLogList(Map map);
	
	
	public void callProcGetStatProcOrd();
	
	public void callStatProc(Map map);
	
	public void callStatProc01(Map map);
	
	public void callStatProc02(Map map);
	
	public List statProcPopup(Map map);
}
