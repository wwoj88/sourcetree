package kr.or.copyright.mls.console.effort;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd01Service" )
public class FdcrAd01ServiceImpl extends CommandService implements FdcrAd01Service{

	// old AdminStatBoardDao
	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	/**
	 * 저작권자 조회 공고 게시판 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd01List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List pageList = (List) fdcrAd01Dao.statBord04RowList( commandMap ); // 페이징카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		List list = (List) fdcrAd01Dao.statBord04List( commandMap );
		List pageCount = (List) fdcrAd01Dao.statBord04RowCount( commandMap ); // 카운트
		
		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageList );
		commandMap.put( "ds_count", pageCount );
	}

	/**
	 * 상당한노력 저작권자 조회 공고 게시판 상세조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd01View1( Map<String, Object> commandMap ) throws Exception{

		List detailList = (List) fdcrAd01Dao.statBord01Detail( commandMap );
		List fileList = (List) fdcrAd01Dao.statBord01File( commandMap );
		List objectList = (List) fdcrAd01Dao.statBord01Object( commandMap );
		List objectFileList = (List) fdcrAd01Dao.statBord01ObjectFile( commandMap );
		// 보완요청 LIST
		List suplList = (List) fdcrAd01Dao.selectBordSuplItemList( commandMap );
		int suplListCount = suplList.size(); 
		// hmap를 dataset로 변환한다.
		commandMap.put( "detailList", detailList );
		commandMap.put( "fileList", fileList );
		commandMap.put( "objectList", objectList );
		commandMap.put( "objectFileList", objectFileList );
		commandMap.put( "suplList", suplList );
		commandMap.put( "suplListCount", suplListCount );
	}

	/**
	 * 보상금 공탁 공고 게시판 - 공고승인
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd01Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBord03Mgnt( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 상세보기(보완처리용)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd01View2( Map<String, Object> commandMap ) throws Exception{

		List detailList = (List) fdcrAd01Dao.statBord01Detail( commandMap );
		List fileList = (List) fdcrAd01Dao.statBord01File( commandMap );
		commandMap.put( "BIG_CODE", "88" );
		List suplCdList = (List) fdcrAd01Dao.selectCdList( commandMap );
		commandMap.put( "BIG_CODE", "35" );
		List genreCdList = (List) fdcrAd01Dao.selectCdList( commandMap );

		commandMap.put( "ds_list", detailList );
		commandMap.put( "ds_file", fileList );
		commandMap.put( "ds_code", suplCdList );
		commandMap.put( "ds_genre", genreCdList );
	}

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd01Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBordStatDeltUpdate( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 처리상태 수정(신청,보완,신청승인,반려)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd01Update2( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBordStatUpdate( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 공고 항목 코드
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Map<String,Object>> selectCdList(Map<String,Object> commandMap) throws Exception{
		return (ArrayList<Map<String,Object>>)fdcrAd01Dao.selectCdList( commandMap );
	}
}
