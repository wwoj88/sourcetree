package kr.or.copyright.mls.console.rcept;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd20Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd21Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd21Service" )
public class FdcrAd21ServiceImpl extends CommandService implements FdcrAd21Service{

	@Resource( name = "fdcrAd03Dao" )
	private FdcrAd03Dao fdcrAd03Dao;

	@Resource( name = "fdcrAd20Dao" )
	private FdcrAd20Dao fdcrAd20Dao;

	/**
	 * 미분배보상금 방송음악 저작물 보고일 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd21List1( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List list = (List) fdcrAd03Dao.reptMgntList( commandMap );

		commandMap.put( "ds_list_0", list );

	}

	/**
	 * 일괄등록
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd21Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception{
		boolean result = false;
		try{

			List returnList = new ArrayList();

			if( commandMap.get( "GENRE" ).equals( "M" ) ){ // 방송음악
				returnList = fdcrAd20Dao.brctInmtInsert( commandMap, excelList );
				// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 20151027
				// 2017-11-02 스케줄러로 변경
				//fdcrAd20Dao.updateMuscStatYn( commandMap );
			}else if( commandMap.get( "GENRE" ).equals( "S" ) ){ // 교과용
				returnList = fdcrAd20Dao.subjInmtInsert( commandMap, excelList );
				// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 20151027
				fdcrAd20Dao.updateSubjStatYn( commandMap );
			}else if( commandMap.get( "GENRE" ).equals( "L" ) ){ // 도서관
				returnList = fdcrAd20Dao.librInmtInsert( commandMap, excelList );
				// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 20151027
				// 2017-11-02 스케줄러로 변경
				//fdcrAd20Dao.updateLibrStatYn( commandMap );
			}else if( commandMap.get( "GENRE" ).equals( "I" ) ){ // 이미지
				returnList = fdcrAd20Dao.imgeInsert( excelList );
			}else if( commandMap.get( "GENRE" ).equals( "V" ) ){ // 영화
				returnList = fdcrAd20Dao.movieInsert( excelList );
			}else if( commandMap.get( "GENRE" ).equals( "A" ) ){ // 수업목적
				returnList = fdcrAd20Dao.lssnInmtInsert( commandMap, excelList );
				// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 20151027
				// 2017-11-02 스케줄러로 변경
				//fdcrAd20Dao.updateLssnStatYn( commandMap );
			}
			commandMap.put( "ds_excel", returnList );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}
}
