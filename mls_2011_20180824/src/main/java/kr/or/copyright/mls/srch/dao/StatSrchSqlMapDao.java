package kr.or.copyright.mls.srch.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class StatSrchSqlMapDao extends SqlMapClientDaoSupport implements StatSrchDao {
	
	public List statSrchList(Map map) {
		return getSqlMapClientTemplate().queryForList("Srch.statSrchList",map);
	}	
	
	public List statSrchListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Srch.statSrchListCount",map);
	}

}
