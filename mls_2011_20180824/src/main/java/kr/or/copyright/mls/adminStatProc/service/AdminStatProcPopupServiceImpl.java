package kr.or.copyright.mls.adminStatProc.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminStatProc.dao.AdminStatProcPopupDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminStatProcPopupServiceImpl implements AdminStatProcPopupService{

	@Autowired
	private AdminStatProcPopupDao adminStatProcPopupDao;
	
	public byte[] statProcTargPopupExelDown(Map<String, Object> map) throws Exception{
		List<String> list = adminStatProcPopupDao.statProcTargPopupExelDown(map);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
		}
		return sb.toString().getBytes();
	}

	public byte[] statProcPopupExelDown(HashMap<String, Object> map) throws Exception {
		List<String> list = adminStatProcPopupDao.statProcPopupExelDown(map);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
		}
		return sb.toString().getBytes();
	}
	
	public byte[] statProcPopupExelDown2(Map<String, Object> map) throws Exception {
		List<String> list = adminStatProcPopupDao.statProcPopupExelDown2(map);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
		}
		return sb.toString().getBytes();
	}
	
	public byte[] statProcWorksPopupExelDown(Map<String, Object> map) throws Exception {
		List<String> list = adminStatProcPopupDao.statProcWorksPopupExelDown(map);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
		}
		return sb.toString().getBytes();
	}
	
	public byte[] statProcInfoExelDown(Map<String, Object> map) throws Exception{
		List<String> list = adminStatProcPopupDao.statProcInfoExelDown(map);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
		}
		return sb.toString().getBytes();
		
	}
}