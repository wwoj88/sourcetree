package kr.or.copyright.mls.console.mber;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.mls.adminCommon.dao.AdminCommonDao;
import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Service;
import kr.or.copyright.mls.console.mber.inter.FdcrAd84Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 회원관리 > 신탁단체 담당자 목록
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd84Controller extends DefaultController {

     @Resource(name = "fdcrAd84Service")
     private FdcrAd84Service fdcrAd84Service;

     @Resource(name = "fdcrAd83Service")
     private FdcrAd83Service fdcrAd83Service;

     @Resource(name = "consoleCommonService")
     private ConsoleCommonService consoleCommonService;
     @Resource(name = "adminCommonDao")
     private AdminCommonDao adminCommonDao;

     private static final Logger logger = LoggerFactory.getLogger(FdcrAd84Controller.class);

     /**
      * 신탁단체 담당자목록 조회
      * 
      * @param commandMap
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84List1.page")
     public String fdcrAd84List1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd83List1 Start");

          String USER_IDNT = EgovWebUtil.getString(commandMap, "USER_IDNT");
          if (!"".equals(USER_IDNT)) {
               commandMap.put("USER_IDNT", USER_IDNT.toUpperCase());
          }
          // 파라미터 셋팅
          String USER_NAME = EgovWebUtil.getString(commandMap, "USER_NAME");
          if (!"".equals(USER_NAME)) {
               USER_NAME = URLDecoder.decode(USER_NAME, "UTF-8");
               commandMap.put("USER_NAME", USER_NAME);
          }

          String COMM_NAME = EgovWebUtil.getString(commandMap, "COMM_NAME");
          if (!"".equals(COMM_NAME)) {
               COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
               commandMap.put("COMM_NAME", COMM_NAME);
          }
          
          
          HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
          String ip = req.getHeader("X-FORWARDED-FOR");
          if (ip == null) {
               ip = req.getRemoteAddr();
          }
          Map logMap = new HashMap();

          logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
          logMap.put("PROC_STATUS", "담당자목록" );
          logMap.put("PROC_ID", "담당자목록/전체");
          logMap.put("MENU_URL", request.getRequestURI());
          logMap.put("IP_ADDRESS", ip);

          adminCommonDao.insertAdminLogDo(logMap);
          
          fdcrAd84Service.fdcrAd84List1(commandMap);
          model.addAttribute("commandMap", commandMap);
          model.addAttribute("paginationInfo", commandMap.get("paginationInfo"));
          model.addAttribute("ds_list", commandMap.get("ds_list"));
          logger.debug("fdcrAd84List1 End");
          return "/mber/fdcrAd84List1.tiles";
     }

     /**
      * 신탁단체 담당자 상세 조회, 내정보조회 상세
      * 
      * @param commandMap
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84UpdateForm1.page")
     public String fdcrAd84UpdateForm1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd84UpdateForm1 Start");
          // 파라미터 셋팅
          String USER_IDNT = request.getParameter("USER_IDNT");
          String COMM_NAME = request.getParameter("COMM_NAME");
          // System.out.println("COMM_NAME:: " + COMM_NAME);
          // System.out.println("USER_IDNT:: " + USER_IDNT);
          commandMap.put("USER_IDNT", USER_IDNT);
          String gubun = EgovWebUtil.getString(commandMap, "GUBUN");
          if (gubun.equals("edit")) {
               fdcrAd84Service.fdcrAd84UpdateForm1(commandMap);
          }
          model.addAttribute("info", commandMap.get("ds_list"));
          logger.debug("fdcrAd84UpdateForm1 End");


          HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
          String ip = req.getHeader("X-FORWARDED-FOR");
          if (ip == null) {
               ip = req.getRemoteAddr();
          }
          System.out.println("CONNECT IP : " + ip);
          System.out.println(ConsoleLoginUser.getUserId());


          Map logMap = new HashMap();

          logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
          logMap.put("PROC_STATUS", "담당자목록" );
          logMap.put("PROC_ID",  USER_IDNT + "/ 검색");
          logMap.put("MENU_URL", request.getRequestURI());
          logMap.put("IP_ADDRESS", ip);

          adminCommonDao.insertAdminLogDo(logMap);

          return "/mber/fdcrAd84UpdateForm1.tiles";
     }

     /**
      * 신탁담당자 저장
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84Insert1.page")
     public String fdcrAd84Insert1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd84Insert1 Start");

          // 파라미터 셋팅
          String TRST_ORGN_DIVS_CODE = EgovWebUtil.getString(commandMap, "TRST_ORGN_DIVS_CODE");
          System.out.println("그룹코드:::::::" + TRST_ORGN_DIVS_CODE);
          boolean isSuccess = fdcrAd84Service.fdcrAd84Insert1(commandMap);
          logger.debug("fdcrAd84Insert1 End");
          if (isSuccess) {



               return returnUrl(model, "저장했습니다.", "/console/mber/fdcrAd84List1.page");
               // "/console/mber/fdcrAd84UpdateForm1.page?GUBUN=edit&USER_IDNT=" + EgovWebUtil.getString(
               // commandMap, "USER_IDNT" ).toUpperCase());
          } else {
               return returnUrl(model, "실패했습니다.", "/console/mber/fdcrAd84UpdateForm1.page?GUBUN=write");
          }
     }

     /**
      * 신탁담당자 수정
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84Update1.page")
     public String fdcrAd84Update1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd84Update1 Start");
          // 파라미터 셋팅
          System.out.println("1414");
          String USER_IDNT = EgovWebUtil.getString(commandMap, "USER_IDNT");
          boolean isSuccess = fdcrAd84Service.fdcrAd84Update1(commandMap);
          returnAjaxString(response, isSuccess);
          logger.debug("fdcrAd84Update1 End");
          if (isSuccess) {

               HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
               String ip = req.getHeader("X-FORWARDED-FOR");
               if (ip == null) {
                    ip = req.getRemoteAddr();
               }
               Map logMap = new HashMap();


               logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
               logMap.put("PROC_STATUS", "담당자 목록");
               logMap.put("PROC_ID", USER_IDNT + "/ 정보수정");
               logMap.put("MENU_URL",request.getRequestURI());
               logMap.put("IP_ADDRESS", ip);

               adminCommonDao.insertAdminLogDo(logMap);

               return returnUrl(model, "저장했습니다.", "/console/mber/fdcrAd84List1.page");
          } else {
               return returnUrl(model, "실패했습니다.", "/console/mber/fdcrAd84List1.page");
          }
     }

     /**
      * 관리자 아이디 중복체크
      * 
      * @param commandMap
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84Update2.page")
     public void fdcrAd84Update2(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd84Update2 Start");
          // 파라미터 셋팅
          int iUserCnt = fdcrAd84Service.fdcrAd84Update2(commandMap);
          String msg = "N";
          if (iUserCnt > 0) {
               msg = "Y";
          }
          returnAjaxString(response, msg);
          logger.debug("fdcrAd84Update2 End");
     }

     /**
      * 대표 담당자 존재여부 체크
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84Update3.page")
     public void fdcrAd84Update3(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd84Update3 Start");

          // 파라미터 셋팅
          int iUserCnt = fdcrAd84Service.fdcrAd84Update3(commandMap);
          String msg = "N";
          if (iUserCnt > 0) {
               msg = "Y";
          }
          returnAjaxString(response, msg);
          logger.debug("fdcrAd84Update3 End");
     }

     /**
      * 비밀번호 변경
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84Update4.page")
     public String fdcrAd84Update4(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd84Update4 Start");
          String USER_IDNT = request.getParameter("USER_IDNT");
          // 파라미터 셋팅
          boolean isSuccess = fdcrAd83Service.fdcrAd83Update1(commandMap);
          logger.debug("fdcrAd84Update4 End");
          if (isSuccess) {
               HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
               String ip = req.getHeader("X-FORWARDED-FOR");
               if (ip == null) {
                    ip = req.getRemoteAddr();
               }
               Map logMap = new HashMap();


               logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
               logMap.put("PROC_STATUS", "담당자 목록 ");
               logMap.put("PROC_ID",  USER_IDNT+"/ 암호변경");
               logMap.put("MENU_URL", request.getRequestURI());
               logMap.put("IP_ADDRESS", ip);

               adminCommonDao.insertAdminLogDo(logMap);
               return returnUrl(model, "변경했습니다.", "/console/mber/fdcrAd84List1.page");
          } else {
               return returnUrl(model, "실패했습니다.", "/console/mber/fdcrAd84List1.page");
               
          }
     }

     /**
      * 업체명 변경
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84Update5.page")
     public String fdcrAd84Update5(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd84Update5 Start");

          System.out.println("COMM : " + request.getParameter("COMM_NAME"));
          System.out.println("COMM_OLD :" + request.getParameter("COMM_NAME_OLD"));
          commandMap.put("CONSOLE_USER", request.getSession().getAttribute("CONSOLE_USER"));
          commandMap.put("COMM_NAME", request.getParameter("COMM_NAME"));
          commandMap.put("COMM_NAME_OLD", request.getParameter("COMM_NAME_OLD"));
          // 파라미터 셋팅
          boolean isSuccess = fdcrAd83Service.fdcrAd83CommNameUpdate(commandMap);

          logger.debug("fdcrAd84Update5 End");
          if (isSuccess) {
               HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
               String ip = req.getHeader("X-FORWARDED-FOR");
               if (ip == null) {
                    ip = req.getRemoteAddr();
               }
               Map logMap = new HashMap();


               logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
               logMap.put("PROC_STATUS", "담당자 목록");
               logMap.put("PROC_ID", EgovWebUtil.getString(commandMap, "USER_IDNT")+"/ 삭제");
               logMap.put("MENU_URL", request.getRequestURI());
               logMap.put("IP_ADDRESS", ip);

               adminCommonDao.insertAdminLogDo(logMap);
               
               return returnUrl(model, "변경했습니다.", "/console/mber/fdcrAd84List1.page");
          } else {
               return returnUrl(model, "실패했습니다.", "/console/mber/fdcrAd84List1.page");
          }
     }

     /**
      * 업체 삭제
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84CompanyDelete.page")
     public String fdcrAd84CompanyDelete(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd84CompanyDelete Start");
          System.out.println("fdcrAd84CompanyDelete Start");
          // session.getAttribute( "sessUserIdnt" )
          /*
           * System.out.println( "CONSOLE_USER :::::" + request.getSession().getAttribute("CONSOLE_USER") );
           * System.out.println( "commandMap:" ); System.out.println( commandMap );
           */
          System.out.println(request.getParameter("userId"));
          commandMap.put("CONSOLE_USER", request.getSession().getAttribute("CONSOLE_USER"));
          // 파라미터 셋팅
          boolean isSuccess = fdcrAd83Service.fdcrAd83CompanyDelete(commandMap);
          // boolean isSuccess = true;
          logger.debug("fdcrAd84CompanyDelete End");
          if (isSuccess) {
               return returnUrl(model, "변경했습니다.", "/console/mber/fdcrAd84UpdateForm1.page?GUBUN=edit&USER_IDNT=" + EgovWebUtil.getString(commandMap, "USER_IDNT"));
          } else {
               return returnUrl(model, "실패했습니다.", "/console/mber/fdcrAd84UpdateForm1.page?GUBUN=edit&USER_IDNT=" + EgovWebUtil.getString(commandMap, "USER_IDNT"));
          }
     }

     /**
      * 메뉴 트리
      * 
      * @param model
      * @param request
      * @param commandMap
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84UpdateForm1Sub1.page")
     public void fdcrAd84UpdateForm1Sub1(ModelMap model, HttpServletRequest request, Map<String, Object> commandMap, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAdB0UpdateForm1Sub1 Start");
          ArrayList<Map<String, Object>> treeList = new ArrayList<Map<String, Object>>();
          Map<String, Object> rootTree = new HashMap<String, Object>();
          rootTree.put("id", "menuId_0");
          rootTree.put("text", "메뉴정보");
          rootTree.put("icon", "folder");
          Map<String, Object> rootTreeState = new HashMap<String, Object>();
          rootTreeState.put("opened", true);
          rootTree.put("state", rootTreeState);
          treeList.add(rootTree);

          try {

               ArrayList<Map<String, Object>> groupMenuList = fdcrAd84Service.adminGroupMenuInfo(commandMap);
               ArrayList<Map<String, Object>> menuList = (ArrayList<Map<String, Object>>) consoleCommonService.selectMenuGroupList(new HashMap<String, Object>());
               ArrayList<Map<String, Object>> curMenuList = new ArrayList<Map<String, Object>>();
               for (int i = 0; i < menuList.size(); i++) {
                    Map<String, Object> map = menuList.get(i);
                    String menuId = ((BigDecimal) map.get("MENU_ID")).intValue() + "";
                    int menuId1 = ((BigDecimal) map.get("MENU_ID")).intValue();
                    for (Map<String, Object> selMenu : groupMenuList) {
                         int selMenuId = ((BigDecimal) selMenu.get("MENU_ID")).intValue();
                         if (selMenuId == menuId1) {
                              curMenuList.add(map);
                         }
                    }
               }
               for (int i = 0; i < curMenuList.size(); i++) {
                    Map<String, Object> map = menuList.get(i);
                    int menuId1 = ((BigDecimal) map.get("MENU_ID")).intValue();
                    String menuId = ((BigDecimal) map.get("MENU_ID")).intValue() + "";
                    String upperMenuId = ((BigDecimal) map.get("PARENT_MENU_ID")).intValue() + "";
                    String menuNm = EgovWebUtil.getString(map, "MENU_TITLE");
                    String menuDesc = EgovWebUtil.getString(map, "MENU_DESC");
                    int childCnt = EgovWebUtil.getToInt(map, "CHILD_CNT");
                    String html = "";
                    html += "<li style=\"width:300px;float:right\" title=\"" + menuDesc + "\">" + menuNm + "</li>";
                    Map<String, Object> tree = new HashMap<String, Object>();
                    tree.put("id", "menuId_" + menuId);
                    tree.put("text", html);
                    Map<String, Object> treeState = new HashMap<String, Object>();
                    treeState.put("opened", true);
                    if (childCnt > 0) {
                         tree.put("icon", "folder");
                    } else {
                         tree.put("icon", "file");
                    }
                    // if("11001".equals( menuId )){
                    // treeState.put( "selected", true );
                    // }
                    tree.put("state", treeState);
                    tree.put("parent", "menuId_" + upperMenuId);
                    setChildNode(treeList, tree);
               }
          } catch (RuntimeException e) {
               // e.printStackTrace();
               System.out.println("실행중 에러발생 ");
          }
          returnAjaxJsonArray(response, treeList);
     }

     public void setChildNode(ArrayList<Map<String, Object>> list, Map<String, Object> node) {

          String parent = (String) node.get("parent");

          for (int i = 0; i < list.size(); i++) {
               Map<String, Object> compareMap = list.get(i);
               String compMenuId = (String) compareMap.get("id");
               ArrayList<Map<String, Object>> compChildList = (ArrayList<Map<String, Object>>) compareMap.get("children");

               if (compMenuId.equals(parent)) {
                    ArrayList<Map<String, Object>> childList = (ArrayList<Map<String, Object>>) compareMap.get("children");
                    if (null != childList && childList.size() > 0) {
                         childList.add(node);
                    } else {
                         childList = new ArrayList<Map<String, Object>>();
                         childList.add(node);
                    }
                    compareMap.put("children", childList);
               } else {
                    if (null != compChildList) {
                         setChildNode(compChildList, node);
                    }
               }
          }
     }

     /**
      * 기관 단체 검색 팝업
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd84Pop1.page")
     public String fdcrAd84Pop1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          String SCH_CONAME = EgovWebUtil.getString(commandMap, "SCH_CONAME");
          if (!"".equals(SCH_CONAME)) {
               SCH_CONAME = URLDecoder.decode(SCH_CONAME, "UTF-8");
               commandMap.put("SCH_CONAME", SCH_CONAME);
               // 파라미터 셋팅
               fdcrAd84Service.trstOrgnCoNameList(commandMap);
               // model.addAttribute( "list", list);
               model.addAttribute("commandMap", commandMap);
               model.addAttribute("paginationInfo", commandMap.get("paginationInfo"));
               model.addAttribute("list", commandMap.get("ds_list"));
          }
          return "/mber/fdcrAd84Pop1.popup";
     }
}
