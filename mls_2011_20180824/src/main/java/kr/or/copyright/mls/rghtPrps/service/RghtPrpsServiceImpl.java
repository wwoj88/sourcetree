package kr.or.copyright.mls.rghtPrps.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.inmtPrps.dao.InmtPrpsDao;
import kr.or.copyright.mls.rghtPrps.dao.RghtPrpsDao;
import kr.or.copyright.mls.rghtPrps.model.PrpsAttc;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;
import kr.or.copyright.mls.rsltInqr.dao.RsltInqrDao;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;

public class RghtPrpsServiceImpl extends BaseService implements RghtPrpsService{

	 
	private RghtPrpsDao rghtPrpsDao;
	private RsltInqrDao rsltInqrDao;
	private InmtPrpsDao inmtPrpsDao;

	public void setRghtPrpsDao( RghtPrpsDao rghtPrpsDao) {
		this.rghtPrpsDao = rghtPrpsDao;
	}
	
	public void setRsltInqrDao( RsltInqrDao rsltInqrDao) {
		this.rsltInqrDao = rsltInqrDao;
	}
	
	public void setInmtPrpsDao( InmtPrpsDao inmtPrpsDao) {
		this.inmtPrpsDao = inmtPrpsDao;
	}
	
	// 음악저작물 미리듣기 url
	public String getMuscRecFile(RghtPrps rghtPrps) {
		return rghtPrpsDao.getMuscRecFile(rghtPrps);
	}
	
	// 음악목록
	public ListResult muscRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps) {
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = rghtPrpsDao.muscRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
		
		if(totalRow>0)
			list = rghtPrpsDao.muscRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		
		return listResult;
		
	}
	
	// 음악상세
	public RghtPrps muscRghtDetail(RghtPrps rghtPrps) {
		
		rghtPrps = rghtPrpsDao.muscRghtDetail(rghtPrps);
		
		return rghtPrps;
	}
	
	// 도서목록
	public ListResult bookRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps) {
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = rghtPrpsDao.bookRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
		
		if(totalRow>0)
			list = rghtPrpsDao.bookRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		
		return listResult;
	}
	
	// 도서상세
	public RghtPrps bookRghtDetail(RghtPrps rghtPrps) {
		
		rghtPrps = rghtPrpsDao.bookRghtDetail(rghtPrps);
		
		return rghtPrps;
	}
	
	// 방송대본 
	public ListResult scriptRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps){
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = rghtPrpsDao.scriptRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
		
		if(totalRow>0)
			list = rghtPrpsDao.scriptRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		
		return listResult;
		
	}
	
	// 방송대본 상세 
	public RghtPrps scriptRghtDetail(RghtPrps rghtPrps){
		rghtPrps = rghtPrpsDao.scriptRghtDetail(rghtPrps);
		
		return rghtPrps;
	}
	
	// 이미지목록
	public ListResult imageRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps) {
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = rghtPrpsDao.imageRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
		
		if(totalRow>0)
			list = rghtPrpsDao.imageRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		
		return listResult;
		
	}
	
	// 영화목록
	public ListResult mvieRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps) {
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = rghtPrpsDao.mvieRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
		
		if(totalRow>0)
			list = rghtPrpsDao.mvieRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		
		return listResult;
	}
	
	// 영화상세
	public RghtPrps mvieRghtDetail(RghtPrps rghtPrps) {
		
		rghtPrps = rghtPrpsDao.mvieRghtDetail(rghtPrps);
		
		return rghtPrps;
	}
	
	// 방송목록
	public ListResult broadcastRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps){
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = rghtPrpsDao.broadcastRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
		
		if(totalRow>0)
			list = rghtPrpsDao.broadcastRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		
		return listResult;
		
	}
	
	// 방송상세
	public RghtPrps broadcastRghtDetail(RghtPrps rghtPrps){
		rghtPrps = rghtPrpsDao.broadcastRghtDetail(rghtPrps);
		
		return rghtPrps;
	}
	
	//뉴스목록
	public ListResult newsRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps){
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = rghtPrpsDao.newsRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
		
		if(totalRow>0)
			list = rghtPrpsDao.newsRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		
		return listResult;
	}
	
	// 뉴스상세
	public RghtPrps newsRghtDetail(RghtPrps rghtPrps){
		rghtPrps = rghtPrpsDao.newsRghtDetail(rghtPrps);
		
		return rghtPrps;
	}
	
	// 권리찾기신청 저작물목록
	public List prpsList(RghtPrps rghtPrps) {
		
		List prpsList = null;
		
		// 음악인 경우
		if(rghtPrps.getDivs().equals("M"))
			prpsList= rghtPrpsDao.muscPrpsList(rghtPrps);
		// 도서인 경우
		else if(rghtPrps.getDivs().equals("B"))
			prpsList= rghtPrpsDao.bookPrpsList(rghtPrps);
		// 방송대본인 경우
		else if(rghtPrps.getDivs().equals("C"))
			prpsList= rghtPrpsDao.scriptPrpsList(rghtPrps);
		// 이미지인 경우
		else if(rghtPrps.getDivs().equals("I"))
			prpsList= rghtPrpsDao.imagePrpsList(rghtPrps);
		// 영화인 경우
		else if(rghtPrps.getDivs().equals("V"))
			prpsList= rghtPrpsDao.mviePrpsList(rghtPrps);
		// 방송인 경우
		else if(rghtPrps.getDivs().equals("R"))
			prpsList= rghtPrpsDao.broadcastPrpsList(rghtPrps);
		// 뉴스인 경우
		else if(rghtPrps.getDivs().equals("N"))
			prpsList= rghtPrpsDao.newsPrpsList(rghtPrps);
		
		return prpsList;
	}
	
	
	// 저작물별 권리찾기신청내역 List
	public List rghtPrpsHistList(RghtPrps rghtPrps){
		return rghtPrpsDao.rghtPrpsHistList(rghtPrps);
	}	

	public void insertPrpsAttc(PrpsAttc params) {
		rghtPrpsDao.insertPrpsAttc(params);
	}
	
	// get MastKey
	public int getPrpsMastKey(){
		
		return rghtPrpsDao.prpsMastKey();
	}

	// 권리찾기 신청처리
	@Transactional(readOnly = false)
	public RghtPrps rghtPrpsSave(RghtPrps rghtPrps) throws Exception{
		
		RghtPrps reRghtPrps = new RghtPrps();
		int iResult = 1;
		String inmtSeqn = "";
		String inmtSeqnStr = "";
		
		try{
			
			
			if(rghtPrps.getDIVS().equals("B")) 
				rghtPrps.setDIVS("O");
			
			// 1. get prps_mast_key
			int prpsMastKey = rghtPrpsDao.prpsMastKey();
			
			rghtPrps.setPRPS_MAST_KEY(""+prpsMastKey);
			rghtPrps.setPRPS_DIVS(rghtPrps.getDIVS());
			
			// 2. 권리찾기 신청 master 입력
			rghtPrpsDao.prpsMasterInsert(rghtPrps);
			
	//========================================================
			// 첨부파일관련 추가작성 필요함
			int i1 = 0; int i201 = 0; int i202 = 0; int i203 = 0; int i204 = 0; int i205 = 0; int i206 = 0; int i211 = 0;
			int iAll = 0; int i212 = 0; int i213 = 0; int i214 = 0; int i215 = 0; int i216 = 0;
	//========================================================
		
			// 3. 권리찾기 신청처리 기관별 입력
			int workCnt = rghtPrps.getKeyId().length;
			int trstCnt = rghtPrps.getTRST_ORGN_CODE_ARR().length;
			
			String trstOrgnCode  = "";
			String prpsDivs  = rghtPrps.getPRPS_DIVS();
			String userIdnt = rghtPrps.getUSER_IDNT();
			
			for( int i=0; i<workCnt; i++ ) {
				
				rghtPrps.setSEQN((i+1)+"");	// 저작물 순번 setting
System.out.println("@@@@@@@@@@@@222  키값 확인  :  " + rghtPrps.getKeyId()[i]);				
				// 4. 추가저작물 등록 : keyId 가 없는 경우 
				if(rghtPrps.getKeyId()[i].length()==0) {
				    	System.out.println("###############GETKEYID###############:"+rghtPrps.getKeyId()[i].length());
					
					rghtPrps.setPRPS_IDNT_CODE("2");	// 추가저작물
					
					if(rghtPrps.getDIVS().equals("M")) {
						rghtPrps.setGENRE("1");	// 음악코드
						
						// 저작물 정보
						rghtPrps.setTITLE(rghtPrps.getMUSIC_TITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setALBUM_TITLE(rghtPrps.getALBUM_TITLE_ARR()[i]);
						rghtPrps.setPERF_TIME(rghtPrps.getPERF_TIME_ARR()[i]);
						rghtPrps.setISSUED_DATE(rghtPrps.getISSUED_DATE_ARR()[i]);
						//rghtPrps.setLYRICS(rghtPrps.getLYRICS_ARR()[i]);
						
						
						// (1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						// (2) 앨범정보 Insert
						String album_id = ""+rghtPrpsDao.getAlbumId();
						rghtPrps.setPRPS_IDNT_ME(album_id);
						rghtPrpsDao.albumInsert(rghtPrps);
						
						// (3) 앨범음원정보 Insert
						String nr_id = ""+rghtPrpsDao.getNrId(rghtPrps);
						rghtPrps.setPRPS_IDNT_NR(nr_id);
						rghtPrpsDao.musicInsert(rghtPrps);
						
						// 보상금 동시신청의 경우.(방송음악)
						if(rghtPrps.getPRPS_DOBL_CODE().equals("1")) {
							
							rghtPrps.setSDSR_NAME(rghtPrps.getTITLE());		// 곡명
							
							rghtPrps.setALBM_NAME(rghtPrps.getALBUM_TITLE()); // 앨범명
						
							if(rghtPrps.getSINGER_ARR()!=null && rghtPrps.getSINGER_ARR()[i]!=null)
								rghtPrps.setMUCI_NAME(rghtPrps.getSINGER_ARR()[i].replaceAll("``", "&quot;"));	// 가수
							
							if(rghtPrps.getLYRICIST_ARR()!=null && rghtPrps.getLYRICIST_ARR()[i]!=null)
								rghtPrps.setLYRI_WRTR(rghtPrps.getLYRICIST_ARR()[i].replaceAll("``", "&quot;"));	// 작사
							
							if(rghtPrps.getCOMPOSER_ARR()!=null && rghtPrps.getCOMPOSER_ARR()[i]!=null)
								rghtPrps.setCOMS_WRTR(rghtPrps.getCOMPOSER_ARR()[i].replaceAll("``", "&quot;"));	// 작곡
							
							if(rghtPrps.getARRANGER_ARR()!=null && rghtPrps.getARRANGER_ARR()[i]!=null)
								rghtPrps.setARRG_WRTR(rghtPrps.getARRANGER_ARR()[i].replaceAll("``", "&quot;"));	// 편곡
							
							inmtSeqn = inmtPrpsDao.getBrctInmtSeqn()+"";
							
							inmtSeqnStr += "&iChk="+inmtSeqn;	// 키값생성
							
							rghtPrps.setINMT_SEQN(inmtSeqn);
							inmtPrpsDao.brctImtInsert(rghtPrps);
						}
						
					}
					if(rghtPrps.getDIVS().equals("O")) {
						rghtPrps.setGENRE("2");	// 도서코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getTITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setBOOK_TITLE( rghtPrps.getBOOK_TITLE_ARR()[i]); 
						
						rghtPrps.setPUBLISHER( rghtPrps.getPUBLISHER_ARR()[i]); 
						rghtPrps.setFIRST_EDITION_YEAR( rghtPrps.getFIRST_EDITION_YEAR_ARR()[i]); 
						rghtPrps.setPUBLISH_TYPE( rghtPrps.getPUBLISH_TYPE_ARR()[i]); 
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						// (2) 도서정보 Insert
						String book_nr_id = ""+rghtPrpsDao.getBookNrId(rghtPrps);
						rghtPrps.setPRPS_IDNT_NR(book_nr_id);
						rghtPrpsDao.bookInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						
						// 보상금 동시신청의 경우.(도서관)
						if(rghtPrps.getPRPS_DOBL_CODE().equals("1")) {
							
							rghtPrps.setWORK_NAME(rghtPrps.getTITLE());		// 저작물명
							rghtPrps.setLISH_COMP(rghtPrps.getPUBLISHER()); // 출판사
							rghtPrps.setPUBC_YEAR(rghtPrps.getFIRST_EDITION_YEAR()); // 발행년도
						
							if(rghtPrps.getLICENSOR_NAME_KOR_ARR()!=null && rghtPrps.getLICENSOR_NAME_KOR_ARR()[i]!=null)
								rghtPrps.setCOPT_HODR(rghtPrps.getLICENSOR_NAME_KOR_ARR()[i].replaceAll("``", "&quot;"));	// 저자
							
							inmtSeqn = inmtPrpsDao.getLibrInmtSeqn()+"";
							
							inmtSeqnStr += "&iChk="+inmtSeqn;	// 키값생성
							
							rghtPrps.setINMT_SEQN(inmtSeqn);
							inmtPrpsDao.librImtInsert(rghtPrps);
						}
					}
					if(rghtPrps.getDIVS().equals("N")) {	// 뉴스
						
						rghtPrps.setGENRE("21");	// 뉴스코드
						
						rghtPrps.setTITLE(rghtPrps.getTITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setOPEN_DATE(rghtPrps.getOPEN_DATE_ARR()[i]);
						
//						(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						//(2) 뉴스정보 Insert
						rghtPrpsDao.newsInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						rghtPrps.setPRPS_IDNT_NR("0");
					}
					if(rghtPrps.getDIVS().equals("C")) {  // 방송대본 
						rghtPrps.setGENRE("3");	// 방송대본코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getTITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setGENRE_KIND(rghtPrps.getGENRE_KIND_ARR()[i]);
						rghtPrps.setBROAD_MEDI(rghtPrps.getBROAD_MEDI_ARR()[i]);
						rghtPrps.setBROAD_STAT(rghtPrps.getBROAD_STAT_ARR()[i]);
						rghtPrps.setBROAD_ORD(rghtPrps.getBROAD_ORD_ARR()[i]);
						rghtPrps.setBROAD_DATE(rghtPrps.getBROAD_DATE_ARR()[i]);
						rghtPrps.setDIRECT(rghtPrps.getDIRECT_ARR()[i]);
						rghtPrps.setMAKER(rghtPrps.getMAKER_ARR()[i]);
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						//(2) 방송대본정보 Insert
						rghtPrpsDao.scriptInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						rghtPrps.setPRPS_IDNT_NR("0");
					}
					if(rghtPrps.getDIVS().equals("I")) {  // 이미지 
						rghtPrps.setGENRE("6");	// 이미지코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getWORK_NAME_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setIMAGE_DIVS(rghtPrps.getIMAGE_DIVS_ARR()[i]);
						rghtPrps.setOPEN_MEDI_CODE(rghtPrps.getOPEN_MEDI_CODE_ARR()[i]);
						rghtPrps.setOPEN_MEDI_TEXT(rghtPrps.getOPEN_MEDI_TEXT_ARR()[i]);
						rghtPrps.setOPEN_DATE(rghtPrps.getOPEN_DATE_ARR()[i]);
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						//(2) 이미지정보 Insert
						rghtPrpsDao.imageInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						rghtPrps.setPRPS_IDNT_NR("0");
						
						// 보상금 동시신청의 경우.(교과용)
						if(rghtPrps.getPRPS_DOBL_CODE().equals("1")) {
						
							//rghtPrps.getCOPT_HODR_ARR()[i]
							                            
							if(rghtPrps.getCOPT_HODR_ARR()!=null && rghtPrps.getCOPT_HODR_ARR()[i]!=null)
								rghtPrps.setCOPT_HODR(rghtPrps.getCOPT_HODR_ARR()[i].replaceAll("``", "&quot;"));	// 저자
							
							inmtSeqn = inmtPrpsDao.getSubjInmtSeqn()+"";
							
							inmtSeqnStr += "&iChk=2|"+inmtSeqn;	// 키값생성
							
							rghtPrps.setINMT_SEQN(inmtSeqn);
							inmtPrpsDao.subjImtInsert(rghtPrps);
						}
						
					}
					if(rghtPrps.getDIVS().equals("V")) {
						rghtPrps.setGENRE("5");	// 영화코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getTITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setDIRECTOR(rghtPrps.getDIRECTOR_ARR()[i]);
						rghtPrps.setLEADING_ACTOR(rghtPrps.getLEADING_ACTOR_ARR()[i]);
						rghtPrps.setPRODUCE_DATE(rghtPrps.getPRODUCE_DATE_ARR()[i]);
						rghtPrps.setMEDIA_CODE_VALUE(rghtPrps.getMEDIA_CODE_VALUE_ARR()[i]);
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						//(2) 영화정보 Insert
						rghtPrpsDao.movieInsert(rghtPrps);
						
						//(3) 매체정보 Insert
						String media_id = ""+rghtPrpsDao.getMovieMediaId(rghtPrps);
						rghtPrps.setPRPS_IDNT_ME(media_id);
						rghtPrpsDao.movieMediaInsert(rghtPrps);
						
						// null
						 rghtPrps.setPRPS_IDNT_NR("N");
					}
					if(rghtPrps.getDIVS().equals("R")) {  // 방송
						rghtPrps.setGENRE("4");	// 방송대본코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getTITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setPROG_NAME(rghtPrps.getPROG_NAME_ARR()[i]);
						rghtPrps.setMEDI_CODE(rghtPrps.getMEDI_CODE_ARR()[i]);
						rghtPrps.setCHNL_CODE(rghtPrps.getCHNL_CODE_ARR()[i]);
						rghtPrps.setPROG_ORDSEQ(rghtPrps.getPROG_ORDSEQ_ARR()[i]);
						rghtPrps.setPROG_GRAD(rghtPrps.getPROG_GRAD_ARR()[i]);
						rghtPrps.setBROAD_DATE(rghtPrps.getBROAD_DATE_ARR()[i]);
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						//(2) 방송대본정보 Insert
						rghtPrpsDao.broadcastInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						rghtPrps.setPRPS_IDNT_NR("0");
					}
					if(rghtPrps.getDIVS().equals("X")) {  // 기타
						rghtPrps.setGENRE("99");	// 기타 코드
						
						String genre = rghtPrps.getPRPS_SIDE_GENRE();
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getWORK_NAME_ARR()[i]);		// 저작물명 셋팅
						//rghtPrps.setIMAGE_DIVS(rghtPrps.getIMAGE_DIVS_ARR()[i]);
						rghtPrps.setOPEN_MEDI_CODE(rghtPrps.getOPEN_MEDI_CODE_ARR()[i]);
						rghtPrps.setOPEN_MEDI_TEXT(rghtPrps.getOPEN_MEDI_TEXT_ARR()[i]);
						rghtPrps.setOPEN_DATE(rghtPrps.getOPEN_DATE_ARR()[i]);
						rghtPrps.setDIVS_CODE(rghtPrps.getPRPS_SIDE_R_GENRE_ARR()[i]);
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						if(genre.equals("1")){
							rghtPrps.setBIG_CODE("20"); // 연극
						}else if(genre.equals("2")){
							rghtPrps.setBIG_CODE("21"); // 건축
						}else if(genre.equals("3")){
							rghtPrps.setBIG_CODE("22"); // 도형
						}else if(genre.equals("4")){
							rghtPrps.setBIG_CODE("23"); // 컴퓨터
						}else if(genre.equals("5")){
							rghtPrps.setBIG_CODE("24"); // 편집
						}else if(genre.equals("6")){
							rghtPrps.setBIG_CODE("25"); // 공공콘텐츠
						}else if(genre.equals("7")){
							rghtPrps.setBIG_CODE("26"); // 뉴스
						}
						
						//(2) 기타정보 Insert
						rghtPrpsDao.sideInsert(rghtPrps);
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						rghtPrps.setPRPS_IDNT_NR("0");
					}
					
				}
				else {
					
					rghtPrps.setPRPS_IDNT_CODE("1");	// 기존저작물
					
					// 저작물 key 값 셋팅 : prps_idnt/ prps_idnt_nr/ prps_idnt_me
					String keyIdArr[] = rghtPrps.getKeyId()[i].split("\\|");
					
					rghtPrps.setPRPS_IDNT(keyIdArr[0]);
					if(keyIdArr.length>1) rghtPrps.setPRPS_IDNT_NR(keyIdArr[1]); else rghtPrps.setPRPS_IDNT_NR("0");
					if(keyIdArr.length>2) rghtPrps.setPRPS_IDNT_ME(keyIdArr[2]); else rghtPrps.setPRPS_IDNT_ME("0");
					
					// 보상금 동시신청의 경우.(방송음악)
					if(rghtPrps.getPRPS_DOBL_CODE().equals("1")) {
							
						if(rghtPrps.getDIVS().equals("M")) {

							rghtPrps.setTITLE(rghtPrps.getMUSIC_TITLE_ARR()[i]);		// 저작물명 셋팅
							rghtPrps.setALBUM_TITLE(rghtPrps.getALBUM_TITLE_ARR()[i]);
							
							rghtPrps.setSDSR_NAME(rghtPrps.getTITLE());		// 곡명
							
							rghtPrps.setALBM_NAME(rghtPrps.getALBUM_TITLE()); // 앨범명
						
							if(rghtPrps.getSINGER_ORGN_ARR()!=null && rghtPrps.getSINGER_ORGN_ARR()[i]!=null)
								rghtPrps.setMUCI_NAME(rghtPrps.getSINGER_ORGN_ARR()[i].replaceAll("``", "&quot;"));	// 가수
							
							if(rghtPrps.getLYRICIST_ORGN_ARR()!=null && rghtPrps.getLYRICIST_ORGN_ARR()[i]!=null)
								rghtPrps.setLYRI_WRTR(rghtPrps.getLYRICIST_ORGN_ARR()[i].replaceAll("``", "&quot;"));	// 작사
							
							if(rghtPrps.getCOMPOSER_ORGN_ARR()!=null && rghtPrps.getCOMPOSER_ORGN_ARR()[i]!=null)
								rghtPrps.setCOMS_WRTR(rghtPrps.getCOMPOSER_ORGN_ARR()[i].replaceAll("``", "&quot;"));	// 작곡
							
							if(rghtPrps.getARRANGER_ORGN_ARR()!=null && rghtPrps.getARRANGER_ORGN_ARR()[i]!=null)
								rghtPrps.setARRG_WRTR(rghtPrps.getARRANGER_ORGN_ARR()[i].replaceAll("``", "&quot;"));	// 편곡
							
							inmtSeqn = inmtPrpsDao.getBrctInmtSeqn()+"";
							inmtSeqnStr += "&iChk="+inmtSeqn;	// 키값생성
							
							rghtPrps.setINMT_SEQN(inmtSeqn);
							inmtPrpsDao.brctImtInsert(rghtPrps);
						}
						 // 보상금 동시신청의 경우.(도서관)
						else if(rghtPrps.getDIVS().equals("O")) {
							
							rghtPrps.setWORK_NAME(rghtPrps.getTITLE_ARR()[i]);		// 저작물명
							rghtPrps.setLISH_COMP(rghtPrps.getPUBLISHER_ARR()[i]); // 출판사
							rghtPrps.setPUBC_YEAR(rghtPrps.getFIRST_EDITION_YEAR_ARR()[i]); // 발행년도
						
							if(rghtPrps.getLICENSOR_NAME_ORGN_KOR_ARR()!=null && rghtPrps.getLICENSOR_NAME_ORGN_KOR_ARR()[i]!=null)
								rghtPrps.setCOPT_HODR(rghtPrps.getLICENSOR_NAME_ORGN_KOR_ARR()[i].replaceAll("``", "&quot;"));	// 저자
							
							inmtSeqn = inmtPrpsDao.getLibrInmtSeqn()+"";
							
							inmtSeqnStr += "&iChk="+inmtSeqn;	// 키값생성
							
							rghtPrps.setINMT_SEQN(inmtSeqn);
							inmtPrpsDao.librImtInsert(rghtPrps);
						}
						
						// 보상금 동시신청의 경우.(교과용)
						else if(rghtPrps.getDIVS().equals("I")) {
							
							// 매핑된 교과용 보상금 정보가 있는경우.
							if (rghtPrps.getSUBJ_INMT_SEQN_ARR()[i] !=null && rghtPrps.getSUBJ_INMT_SEQN_ARR()[i].length()>0) {
								
								//System.out.println("#################################################");
								//System.out.println("SUBJ_INMT_SEQN_ARR = "+rghtPrps.getSUBJ_INMT_SEQN_ARR()[i]);
								//System.out.println("SUBJ_INMT_SEQN_ARR = "+rghtPrps.getSUBJ_INMT_SEQN_ARR()[i].replaceAll("\\|", "&iChk=1|"));
								
								inmtSeqnStr += "&iChk=1|"+(rghtPrps.getSUBJ_INMT_SEQN_ARR()[i]).replaceAll("\\|", "&iChk=1|");
								
								System.out.println("inmtSeqnStr = "+inmtSeqnStr);
								
							} else {
							                            
								if(rghtPrps.getCOPT_HODR_ORGN_ARR()!=null && rghtPrps.getCOPT_HODR_ORGN_ARR()[i]!=null)
									rghtPrps.setCOPT_HODR(rghtPrps.getCOPT_HODR_ORGN_ARR()[i].replaceAll("``", "&quot;"));	// 저자
								
								inmtSeqn = inmtPrpsDao.getSubjInmtSeqn()+"";
								
								inmtSeqnStr += "&iChk=2|"+inmtSeqn;	// 키값생성
								
								rghtPrps.setINMT_SEQN(inmtSeqn);
								inmtPrpsDao.subjImtInsert(rghtPrps);
							
							}
							
						} // .. end 교과용 보상금
						
						
					}
					
				}
					
				for( int k=0; k<trstCnt; k++) {
					
					rghtPrps.setTRST_ORGN_CODE(rghtPrps.getTRST_ORGN_CODE_ARR()[k]);
					
					rghtPrpsDao.prpsRsltInsert(rghtPrps);
					
					// 5. 기관별 관리권리정보 입력
					// 음악, 도서, 방송대본, 이미지, 영화, 방송
					if(!rghtPrps.getDIVS().equals("X")){
						// (1) 음저협
						if( rghtPrps.getTRST_ORGN_CODE().equals("201")) {	
							
							i201++;
							trstOrgnCode = "201";
							// 신청목적이 이용자 권리조회가 아닌경우.
							//if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ) {
								
								// 작사(101)
								rghtPrps.setRGHT_ROLE_CODE("101"); 
								rghtPrps.setHOLD_NAME(rghtPrps.getLYRICIST_ARR()[i].replaceAll("``", "&quot;"));
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getLYRICIST_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 작곡(102)
								rghtPrps.setRGHT_ROLE_CODE("102");
								rghtPrps.setHOLD_NAME(rghtPrps.getCOMPOSER_ARR()[i].replaceAll("``", "&quot;"));
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOMPOSER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 편곡(103)
								rghtPrps.setRGHT_ROLE_CODE("103");
								rghtPrps.setHOLD_NAME(rghtPrps.getARRANGER_ARR()[i].replaceAll("``", "&quot;"));
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getARRANGER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							//}
						}
						
						// (2) 음실연
						if(rghtPrps.getTRST_ORGN_CODE().equals("202")) {	
							
							i202++;
							trstOrgnCode = "202";
							// 신청목적이 이용자 권리조회가 아닌경우.
							//if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ) {
								
								// 가창(112)
								rghtPrps.setRGHT_ROLE_CODE("112");
								rghtPrps.setHOLD_NAME(rghtPrps.getSINGER_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getSINGER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 연주(113)
								rghtPrps.setRGHT_ROLE_CODE("113");
								rghtPrps.setHOLD_NAME(rghtPrps.getPLAYER_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getPLAYER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 지휘(113)
								rghtPrps.setRGHT_ROLE_CODE("114");
								rghtPrps.setHOLD_NAME(rghtPrps.getCONDUCTOR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCONDUCTOR_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							//}
						}
		
						// (3) 음제협
						if(rghtPrps.getTRST_ORGN_CODE().equals("203")) {
						
							i203++;
							trstOrgnCode = "203";
							//	신청목적이 이용자 권리조회가 아닌경우.
							//if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ) {
								
							
							// 앨범제작(111)
							rghtPrps.setRGHT_ROLE_CODE("111");
							rghtPrps.setHOLD_NAME(rghtPrps.getPRODUCER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getPRODUCER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							//}
						}
						
						// (4)문예협
						if(rghtPrps.getTRST_ORGN_CODE().equals("204")) {
							
							i204++;
							trstOrgnCode = "204";
							
							if(rghtPrps.getDIVS().equals("O")) {
								
								// 저자(201)
								rghtPrps.setRGHT_ROLE_CODE("201");
								rghtPrps.setHOLD_NAME(rghtPrps.getLICENSOR_NAME_KOR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getLICENSOR_NAME_ORGN_KOR_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 역자(202)
								rghtPrps.setRGHT_ROLE_CODE("202");
								rghtPrps.setHOLD_NAME(rghtPrps.getTRANSLATOR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getTRANSLATOR_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							
							}else if(rghtPrps.getDIVS().equals("I")) {
								// 저작권자(301)
								rghtPrps.setRGHT_ROLE_CODE("301");
								rghtPrps.setHOLD_NAME(rghtPrps.getCOPT_HODR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ){ // 권리자 권리찾기
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOPT_HODR_ORGN_ARR()[i]);
								}	
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							}
							
						}
							
						// (5)복전협
						if(rghtPrps.getTRST_ORGN_CODE().equals("205")) {
							
							i205++;
							trstOrgnCode = "205";
							
							if(rghtPrps.getDIVS().equals("M")) {
							
								// 작사(101)
								rghtPrps.setRGHT_ROLE_CODE("101"); 
								rghtPrps.setHOLD_NAME(rghtPrps.getLYRICIST_ARR()[i].replaceAll("``", "&quot;"));
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getLYRICIST_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 작곡(102)
								rghtPrps.setRGHT_ROLE_CODE("102");
								rghtPrps.setHOLD_NAME(rghtPrps.getCOMPOSER_ARR()[i].replaceAll("``", "&quot;"));
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOMPOSER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 편곡(103)
								rghtPrps.setRGHT_ROLE_CODE("103");
								rghtPrps.setHOLD_NAME(rghtPrps.getARRANGER_ARR()[i].replaceAll("``", "&quot;"));
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getARRANGER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
							}else if(rghtPrps.getDIVS().equals("O")) {
								// 저자(201)
								rghtPrps.setRGHT_ROLE_CODE("201");
								rghtPrps.setHOLD_NAME(rghtPrps.getLICENSOR_NAME_KOR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getLICENSOR_NAME_ORGN_KOR_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 역자(202)
								rghtPrps.setRGHT_ROLE_CODE("202");
								rghtPrps.setHOLD_NAME(rghtPrps.getTRANSLATOR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getTRANSLATOR_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							}else if(rghtPrps.getDIVS().equals("I")){
								// 저작권자(301)
								rghtPrps.setRGHT_ROLE_CODE("301");
								rghtPrps.setHOLD_NAME(rghtPrps.getCOPT_HODR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ){ // 권리자 권리찾기
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOPT_HODR_ORGN_ARR()[i]);
								}	
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							}
						}
						
						// (6)방작협
						if(rghtPrps.getTRST_ORGN_CODE().equals("206")){
							
							i206++;
							trstOrgnCode = "206";
							//작가(501)
							rghtPrps.setRGHT_ROLE_CODE("501");
							rghtPrps.setHOLD_NAME(rghtPrps.getWRITER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3"))
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getWRITER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
						
						// (11)영산협
						if(rghtPrps.getTRST_ORGN_CODE().equals("211")) {
							
							i211++;
							trstOrgnCode = "211";
							
							// 투자사(402)
							rghtPrps.setRGHT_ROLE_CODE("402");
							rghtPrps.setHOLD_NAME(rghtPrps.getINVESTOR_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getINVESTOR_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							
							// 배급사(403)
							rghtPrps.setRGHT_ROLE_CODE("403");
							rghtPrps.setHOLD_NAME(rghtPrps.getDISTRIBUTOR_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getDISTRIBUTOR_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							
							
						}
						
						// (12)시나리오작가협
						if(rghtPrps.getTRST_ORGN_CODE().equals("212")) {
							
							i212++;
							trstOrgnCode = "212";
							// 제작자(404)
							rghtPrps.setRGHT_ROLE_CODE("404");
							rghtPrps.setHOLD_NAME(rghtPrps.getWRITER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getWRITER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}

						// (13)영제협
						if(rghtPrps.getTRST_ORGN_CODE().equals("213")) {
							
							i213++;
							trstOrgnCode = "213";
							// 제작자(401)
							rghtPrps.setRGHT_ROLE_CODE("401");
							rghtPrps.setHOLD_NAME(rghtPrps.getPRODUCER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getPRODUCER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							
							// 투자사(402)
							rghtPrps.setRGHT_ROLE_CODE("402");
							rghtPrps.setHOLD_NAME(rghtPrps.getINVESTOR_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getINVESTOR_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							
							// 배급사(403)
							rghtPrps.setRGHT_ROLE_CODE("403");
							rghtPrps.setHOLD_NAME(rghtPrps.getDISTRIBUTOR_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getDISTRIBUTOR_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
						
						// (8)한국방송콘텐츠협회
						if(rghtPrps.getTRST_ORGN_CODE().equals("1")){
							
							i1++;
							trstOrgnCode = "1";
							//제작자(601)
							rghtPrps.setRGHT_ROLE_CODE("601");
							rghtPrps.setHOLD_NAME(rghtPrps.getMAKER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3"))
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getMAKER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
						
						// (14)한국방송실연자연합회
						if(rghtPrps.getTRST_ORGN_CODE().equals("214")){
							
							i214++;
							trstOrgnCode = "214";
							
							//제작자(601)
							rghtPrps.setRGHT_ROLE_CODE("601");
							rghtPrps.setHOLD_NAME(rghtPrps.getMAKER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3"))
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getMAKER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
						
						// (15)한국언론진흥재단
						if(rghtPrps.getTRST_ORGN_CODE().equals("215")) {
							
							i215++;
							trstOrgnCode = "215";
							
							// 언론사(701)
							rghtPrps.setRGHT_ROLE_CODE("701");
							rghtPrps.setHOLD_NAME(rghtPrps.getCOPT_HODR_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ){ // 권리자 권리찾기
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOPT_HODR_ORGN_ARR()[i]);
							}	
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
					
						// (16)한국문화콘텐츠진흥원
						if(rghtPrps.getTRST_ORGN_CODE().equals("216")) {
							
							i216++;
							
							if(rghtPrps.getDIVS().equals("I")) {
								// 저작권자(301)
								rghtPrps.setRGHT_ROLE_CODE("301");
								rghtPrps.setHOLD_NAME(rghtPrps.getCOPT_HODR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ){ // 권리자 권리찾기
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOPT_HODR_ORGN_ARR()[i]);
								}	
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							}
						}
						
					}else{
						// 기타저작물 권리찾기 신청
						if(rghtPrps.getTRST_ORGN_CODE().equals("201")) {
							i201++;
							trstOrgnCode = "201";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("202")) {
							i202++;
							trstOrgnCode = "202";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("203")) {
							i203++;
							trstOrgnCode = "203";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("204")) {
							i204++;
							trstOrgnCode = "204";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("205")) {
							i205++;
							trstOrgnCode = "205";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("206")) {
							i206++;
							trstOrgnCode = "206";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("211")) {
							i211++;
							trstOrgnCode = "211";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("212")) {
							i212++;
							trstOrgnCode = "212";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("213")) {
							i213++;
							trstOrgnCode = "213";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("214")) {
							i214++;
							trstOrgnCode = "214";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("215")) {
							i215++;
							trstOrgnCode = "215";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("216")) {
							i216++;
							trstOrgnCode = "216";
						}
						// 기타 저작권자(001)
						rghtPrps.setRGHT_ROLE_CODE("001");
						if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ){ // 권리자 권리찾기
							
							rghtPrps.setHOLD_NAME(rghtPrps.getCOPT_HODR_ARR()[i]);
							rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOPT_HODR_ORGN_ARR()[i]);
						}	
						rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
					}
					// 첨부파일
					
					if( (i201 == 1 && trstOrgnCode.equals("201")) ||	
							(i202 == 1 && trstOrgnCode.equals("202")) ||	
							(i203 == 1 && trstOrgnCode.equals("203")) ||	
							(i204 == 1 && trstOrgnCode.equals("204")) ||	
							(i205 == 1 && trstOrgnCode.equals("205")) ||
							(i206 == 1 && trstOrgnCode.equals("206")) ||	
							(i211 == 1 && trstOrgnCode.equals("211")) ||
							(i212 == 1 && trstOrgnCode.equals("212")) ||
							(i213 == 1 && trstOrgnCode.equals("213")) ||
							(i214 == 1 && trstOrgnCode.equals("214")) ||
							(i215 == 1 && trstOrgnCode.equals("215")) ||
							(i216 == 1 && trstOrgnCode.equals("216")) ||
							(i1 == 1 && trstOrgnCode.equals("1")))
					{
						PrpsAttc prpsAttc = new PrpsAttc();
						PrpsAttc prpsAttcFile = new PrpsAttc();	
						
						System.out.println("rghtPrps.getFileList(). >> "+rghtPrps.getFileList().size());
						
						iAll++;      
						for(int fi=0; fi < rghtPrps.getFileList().size(); fi++) {
							
							System.out.print(" "+fi);
							
							prpsAttc = (PrpsAttc) rghtPrps.getFileList().get(fi);
							
							// 공통파일
							if( prpsAttc.getTRST_ORGN_CODE().equals("0") && iAll == 1){
								
								prpsAttcFile.setREAL_FILE_NAME(prpsAttc.getREAL_FILE_NAME());
								prpsAttcFile.setFILE_PATH(prpsAttc.getFILE_PATH());
								prpsAttcFile.setTRST_ORGN_CODE(prpsAttc.getTRST_ORGN_CODE());
								prpsAttcFile.setPRPS_DIVS(prpsDivs);
								prpsAttcFile.setUSER_IDNT(userIdnt);
								prpsAttcFile.setPRPS_MAST_KEY(prpsMastKey+"");
								prpsAttcFile.setFILE_SIZE(prpsAttc.getFILE_SIZE());
								prpsAttcFile.setFILE_NAME(prpsAttc.getFILE_NAME());
							
								rghtPrpsDao.insertPrpsAttc(prpsAttcFile);
							}
							// 첨부파일의 신탁단체와 신청기관이 같은경우만 등록
							else if( prpsAttc.getTRST_ORGN_CODE().equals(trstOrgnCode)){
								
								prpsAttcFile.setREAL_FILE_NAME(prpsAttc.getREAL_FILE_NAME());
								prpsAttcFile.setFILE_PATH(prpsAttc.getFILE_PATH());
								prpsAttcFile.setTRST_ORGN_CODE(prpsAttc.getTRST_ORGN_CODE());
								prpsAttcFile.setPRPS_DIVS(prpsDivs);
								prpsAttcFile.setUSER_IDNT(userIdnt);
								prpsAttcFile.setPRPS_MAST_KEY(prpsMastKey+"");
								prpsAttcFile.setFILE_SIZE(prpsAttc.getFILE_SIZE());
								prpsAttcFile.setFILE_NAME(prpsAttc.getFILE_NAME());
							
								rghtPrpsDao.insertPrpsAttc(prpsAttcFile);
							}
						}	
					} // .. end if 첨부파일
					
					
				} // .. end for 기관별 입력
				
			} // .. end for 저작물 입력
			
			// 메일, SMS
			//----------------------------- mail send------------------------------//
			
		    String host = Constants.MAIL_SERVER_IP;							// smtp서버
		    String to     = rghtPrps.getSESS_U_MAIL();					// 수신EMAIL
		    String toName = rghtPrps.getUSER_NAME();			// 수신인
		    String subject = "[저작권찾기] 저작권찾기신청 완료";
		    String idntName = "";	// 신청대상명
		    if(rghtPrps.getDIVS().equals("M")) { // 음악 
		    	idntName = rghtPrps.getMUSIC_TITLE_ARR()[0];
		    }else if(rghtPrps.getDIVS().equals("I")){ // 이미지 
		    	idntName = rghtPrps.getWORK_NAME_ARR()[0];
		    }else if(rghtPrps.getDIVS().equals("X")){ // 기타 
		    	idntName = rghtPrps.getWORK_NAME_ARR()[0];
		    }else{ // 도서, 방송대본, 영화, 방송 , 뉴스
		    	idntName = rghtPrps.getTITLE_ARR()[0];
		    }

		    if( (rghtPrps.getMUSIC_TITLE_ARR().length + rghtPrps.getTITLE_ARR().length + rghtPrps.getWORK_NAME_ARR().length) > 0)
		    	idntName  += " 외 "+ ( (rghtPrps.getMUSIC_TITLE_ARR().length + rghtPrps.getTITLE_ARR().length + rghtPrps.getWORK_NAME_ARR().length)-1 )+ "건";
		      
		    String from = Constants.SYSTEM_MAIL_ADDRESS;  //발신인 주소 - 시스템 대표 메일
		    String fromName = Constants.SYSTEM_NAME;
		    
		    
		    // 신청자에게 이메일
		    if( rghtPrps.getSESS_U_MAIL_RECE_YSNO().equals("Y") && to != null && to != "")  {
			
		    	//------------------------- mail 정보 ----------------------------------//
			    MailInfo info = new MailInfo();
			    info.setFrom(from);
			    info.setFromName(fromName);
			    info.setHost(host);
			    info.setEmail(to);
			    info.setEmailName(toName);
			    info.setSubject(subject);
			      
			    StringBuffer sb = new StringBuffer();   
			    
			    sb.append("<div class=\"mail_title\">"+toName+"님, 저작권찾기신청이  완료되었습니다. </div>");
			    sb.append("<div class=\"mail_contents\"> ");
			    sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
			    sb.append("		<tr> ");
			    sb.append("			<td> ");
			    sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
	// 변경부분
			    sb.append("					<tr> ");
			    sb.append("						<td class=\"tdLabel\">신청 저작물명</td> ");
			    sb.append("						<td class=\"tdData\">"+ idntName+"</td> ");
			    sb.append("					</tr> ");
			    sb.append("				</table> ");
	// 변경부분
			    
			    if(rghtPrps.getDIVS().equals("M")){
				    sb.append("			<br/> ");
		    		sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "); 	
		    		sb.append("				<tr> 	 ");
		    		sb.append("					<td>  ");
		    		sb.append("						<font color=\"blue\" > * 음악저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</font> ");
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("				<tr> ");
		    		sb.append("					<td class=\"tdData\">  ");
		    		sb.append("						<li><b>한국음악저작권협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_201").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_201").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_201").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국음악실연자연합회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_202").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_202").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_202").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국음원제작자협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_203").getBytes("ISO-8859-1"), "EUC-KR")
		    											+"  "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_203").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("			</table>");
			    }else if(rghtPrps.getDIVS().equals("B")){
				    sb.append("			<br/> ");
		    		sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "); 	
		    		sb.append("				<tr> 	 ");
		    		sb.append("					<td>  ");
		    		sb.append("						<font color=\"blue\" > * 도서저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</font> ");
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("				<tr> ");
		    		sb.append("					<td class=\"tdData\">  ");
		    		sb.append("						<li><b>한국문예학술저작권협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_204").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_204").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_204").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국복사전송권협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_205").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_205").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_205").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("			</table>");
			    }else if(rghtPrps.getDIVS().equals("N")){
				    sb.append("			<br/> ");
		    		sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "); 	
		    		sb.append("				<tr> 	 ");
		    		sb.append("					<td>  ");
		    		sb.append("						<font color=\"blue\" > * 뉴스저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</font> ");
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("				<tr> ");
		    		sb.append("					<td class=\"tdData\">  ");
		    		sb.append("						<li><b>한국언론진흥재단</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_215").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_215").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_215").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("			</table>");
			    }else if(rghtPrps.getDIVS().equals("C")){
				    sb.append("			<br/> ");
		    		sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "); 	
		    		sb.append("				<tr> 	 ");
		    		sb.append("					<td>  ");
		    		sb.append("						<font color=\"blue\" > * 방송대본저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</font> ");
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("				<tr> ");
		    		sb.append("					<td class=\"tdData\">  ");
		    		sb.append("						<li><b>한국방송작가협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_206").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_206").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_206").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("			</table>");
			    }else if(rghtPrps.getDIVS().equals("I")){
				    sb.append("			<br/> ");
		    		sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "); 	
		    		sb.append("				<tr> 	 ");
		    		sb.append("					<td>  ");
		    		sb.append("						<font color=\"blue\" > * 이미지저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</font> ");
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("				<tr> ");
		    		sb.append("					<td class=\"tdData\">  ");
		    		sb.append("						<li><b>한국문예학술저작권협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_204").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_204").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_204").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국복사전송권협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_205").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_205").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_205").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("			</table>");
			    }else if(rghtPrps.getDIVS().equals("V")){
				    sb.append("			<br/> ");
		    		sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "); 	
		    		sb.append("				<tr> 	 ");
		    		sb.append("					<td>  ");
		    		sb.append("						<font color=\"blue\" > * 영화저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</font> ");
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("				<tr> ");
		    		sb.append("					<td class=\"tdData\">  ");
		    		sb.append("						<li><b>한국영화제작가협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_213").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_213").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_213").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국시나리오작가협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_212").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_212").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_212").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국영상산업협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_211").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_211").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_211").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("			</table>");
			    }else if(rghtPrps.getDIVS().equals("R")){
				    sb.append("			<br/> ");
		    		sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "); 	
		    		sb.append("				<tr> 	 ");
		    		sb.append("					<td>  ");
		    		sb.append("						<font color=\"blue\" > * 영화저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</font> ");
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("				<tr> ");
		    		sb.append("					<td class=\"tdData\">  ");
		    		sb.append("						<li><b>한국방송실연자협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_214").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_214").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_214").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("			</table>");
			    }else if(rghtPrps.getDIVS().equals("X")){
				    sb.append("			<br/> ");
		    		sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "); 	
		    		sb.append("				<tr> 	 ");
		    		sb.append("					<td>  ");
		    		sb.append("						<font color=\"blue\" > * 영화저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</font> ");
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("				<tr> ");
		    		sb.append("					<td class=\"tdData\">  ");
		    		sb.append("						<li><b>한국음악저작권협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_201").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_201").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_201").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국음악실연자연합회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_202").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_202").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_202").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국음원제작자협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_203").getBytes("ISO-8859-1"), "EUC-KR")
		    											+"  "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_203").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국문예학술저작권협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_204").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_204").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_204").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국복사전송권협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_205").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_205").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_205").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국방송작가협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_206").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_206").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_206").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국영상산업협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_211").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_211").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_211").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국시나리오작가협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_212").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_212").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_212").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국영화제작가협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_213").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_213").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_213").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국방송실연자협회</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_214").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_214").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_214").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("						<li><b>한국언론진흥재단</b></li> ");
		    		sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_215").getBytes("ISO-8859-1"), "EUC-KR")+"</u></b>"
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_tel_215").getBytes("ISO-8859-1"), "EUC-KR")
														+"<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_215").getBytes("ISO-8859-1"), "EUC-KR"));
		    		sb.append("					</td> ");
		    		sb.append("				</tr> ");
		    		sb.append("			</table>");
			    }

			    sb.append("				</table> ");
			    sb.append("			</td> ");
			    sb.append("		</tr> ");
			    sb.append("	</table> ");
			    sb.append("</div> ");

			    info.setMessage(new String(MailMessage.getChangeInfoMailText(sb.toString())));
	    	    MailManager manager = MailManager.getInstance();
		    	
	    	    info = manager.sendMail(info);
		    	if(info.isSuccess()){
		    	  System.out.println(">>>>>>>>>>>> 신청자 message success :"+info.getEmail() );
		    	}else{
		    	  System.out.println(">>>>>>>>>>>> 신청자 message false   :"+info.getEmail() );
		    	}
		    }
		      
		    String smsTo = rghtPrps.getSESS_U_MOBL_PHON();
		    String smsFrom = Constants.SYSTEM_TELEPHONE;
		    
		    String smsMessage = "[저작권찾기] "+ idntName+" - 저작권찾기신청이  완료되었습니다.";
		    
		    if( rghtPrps.getSESS_U_SMS_RECE_YSNO().equals("Y") && smsTo != null && smsTo != "") {
			
		    	SMSManager smsManager = new SMSManager();
			  	
			  	System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
		    }	
		    
		    // end email, sms
			
			//	접속정보 저장---------------------
			HashMap connMap = new HashMap();
			connMap.put("userIdnt", rghtPrps.getUSER_IDNT());
			connMap.put("connUrl", "저작권찾기신청 등록");
			inmtPrpsDao.insertConnInfo(connMap);
			//-------------------------------------
			
			
			
			/* 담당자에게 email 발송 시작 ============================================== */
			MailInfo orgnInfo = new MailInfo();
			orgnInfo.setFrom(from);
			orgnInfo.setFromName(fromName);
			orgnInfo.setHost(host);
			
			
			String prpsDivsName = "";
			
			if(rghtPrps.getDIVS().equals("M")) 				prpsDivsName = "음악";
			else if(rghtPrps.getDIVS().equals("O")) 		prpsDivsName = "도서";
			else if(rghtPrps.getDIVS().equals("C")) 		prpsDivsName = "방송대본";
		    else if(rghtPrps.getDIVS().equals("I")) 		prpsDivsName = "이미지";
		    else if(rghtPrps.getDIVS().equals("V")) 		prpsDivsName = "영화";
		    else if(rghtPrps.getDIVS().equals("R")) 		prpsDivsName = "방송";
		    else if(rghtPrps.getDIVS().equals("X"))			prpsDivsName = "기타"; 
		    else if(rghtPrps.getDIVS().equals("N"))			prpsDivsName = "뉴스"; 
		    
			orgnInfo.setSubject("[저작권찾기] "+prpsDivsName+"저작물 저작권찾기가 신청되었습니다. ");
			
			orgnInfo.setPrpsDivsName(prpsDivsName);				// 장르
			orgnInfo.setPrpsTitle(idntName);								// 저작물 명
			orgnInfo.setPrpsName(rghtPrps.getUSER_NAME());	// 신청자 이름
			orgnInfo.setPrpsEmail(rghtPrps.getSESS_U_MAIL());			// 신청자 이메일
			orgnInfo.setPrpsSms(rghtPrps.getSESS_U_MOBL_PHON());	// 신청자 모바일
			
			String orgnTo = "";
			String orgnToName = "";
			
			// 음저협
			if( i201> 0) {
				
				orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_201").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
				orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_201").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
				  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 1. message false   :"+orgnInfo.getEmail() );
				}
			}
			// 음실련
			if( i202> 0) {
			   orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_202").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
			   orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_202").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
					  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 2. message false   :"+orgnInfo.getEmail() );
				}
			}
			
			// 음제협
			if( i203> 0) {
			   orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_203").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
			   orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_203").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
					  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 2. message false   :"+orgnInfo.getEmail() );
				}
			}
			
			// 문예
			if( i204> 0) {
			   orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_204").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
			   orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_204").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
					  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 2. message false   :"+orgnInfo.getEmail() );
				}
			}
			
			// 복전
			if( i205> 0) {
			   orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_205").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
			   orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_205").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
					  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 2. message false   :"+orgnInfo.getEmail() );
				}
			}
			
			// 방작협
			if( i206> 0) {
			   orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_206").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
			   orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_206").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
					  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 2. message false   :"+orgnInfo.getEmail() );
				}
			}
			
			// # 영상협
			if( i211> 0) {
			   orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_211").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
			   orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_211").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
					  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 2. message false   :"+orgnInfo.getEmail() );
				}
			}
			
			// # 한국시나리오작가협회
			if( i212> 0) {
			   orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_212").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
			   orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_212").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
					  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 2. message false   :"+orgnInfo.getEmail() );
				}
			}
			
			// # 한국영화제작가협회
			if( i213> 0) {
			   orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_213").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
			   orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_213").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
					  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 2. message false   :"+orgnInfo.getEmail() );
				}
			}
			
			//# 한국방송실연자협회
			if( i214> 0) {
			   orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_214").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
			   orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_214").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
					  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 2. message false   :"+orgnInfo.getEmail() );
				}
			}
			
			//# 한국언론진흥재단
			if( i215> 0) {
			   orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_215").getBytes("ISO-8859-1"), "EUC-KR");				// 수신EMAIL
			   orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_215").getBytes("ISO-8859-1"), "EUC-KR");			// 수신인
			   
			   orgnInfo.setEmail(orgnTo);
			   orgnInfo.setEmailName(orgnToName);
			   
			   orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
			   MailManager manager = MailManager.getInstance();
				
			   orgnInfo = manager.sendMail(orgnInfo);
			    
				if(orgnInfo.isSuccess()){
					  System.out.println(">>>>>>>>>>>> 1. message success :"+orgnInfo.getEmail() );
				}else{
				  System.out.println(">>>>>>>>>>>> 2. message false   :"+orgnInfo.getEmail() );
				}
			}
			/* 담당자에게 email 발송 끝 ============================================== */
			
			
			
			
			//System.out.println("inmtSeqnStr = "+inmtSeqnStr);
			//System.out.println("#################################################");
			
			//return set
			reRghtPrps.setPRPS_MAST_KEY(prpsMastKey+"");
			//reRghtPrps.setIResult(iResult);
			reRghtPrps.setINMT_SEQN(inmtSeqnStr);		// 보상금 신청 key값
			
		} catch(Exception e){
			e.printStackTrace();
			
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			
			iResult = -1;
			//reRghtPrps.setIResult(iResult);
		}
		
		reRghtPrps.setIResult(iResult);
		return reRghtPrps;
	}
	
	// 권리찾기 수정처리
	@Transactional(readOnly = false)
	public int rghtPrpsModi(RghtPrps rghtPrps) throws Exception {
		
		int iResult = 1;
		try {
			if(rghtPrps.getDIVS().equals("B")) 
				rghtPrps.setDIVS("O");
			
			// * 기존 prps_mast_key로 유지한다.
			
			rghtPrps.setPRPS_DIVS(rghtPrps.getDIVS());
			
			// 1. 권리찾기 신청 master 수정
			rghtPrpsDao.prpsMasterUpdate(rghtPrps); 
			
			// 2. 아래 하위 테이블 삭제
			rsltInqrDao.prpsRsltDelete(rghtPrps);			// 신청처리결과
			rsltInqrDao.prpsRsltRghtDelete(rghtPrps);	// 신청권리정보
			rsltInqrDao.prpsFileDelete(rghtPrps);			// 첨부파일
		
			// 등록 저작물 관련 삭제도 필요함
			
	//===============================================
			// 첨부파일관련 추가작성 필요함
			int i1 = 0; int i201 = 0; int i202 = 0; int i203 = 0; int i204 = 0; int i205 = 0; int i206 = 0; int i211 = 0;
			int iAll = 0; int i212 = 0; int i213 = 0; int i214 = 0; int i215 = 0; int i216 = 0;
	//===============================================
		
			// 3. 권리찾기 신청처리 기관별 입력
			int workCnt = rghtPrps.getKeyId().length;
			int trstCnt = rghtPrps.getTRST_ORGN_CODE_ARR().length;
			String trstOrgnCode  = "";
			String prpsDivs  = rghtPrps.getPRPS_DIVS();
			String userIdnt = rghtPrps.getUSER_IDNT();
			
			for( int i=0; i<workCnt; i++ ) {
				
				rghtPrps.setSEQN((i+1)+"");	// 저작물 순번 setting
				
				// 4. 추가저작물 등록 : keyId 가 없는 경우 
				if(rghtPrps.getKeyId()[i].length()==0) {
					
					rghtPrps.setPRPS_IDNT_CODE("2");	// 추가저작물
					
					if(rghtPrps.getDIVS().equals("M")) {
						rghtPrps.setGENRE("1");	// 음악코드
						
						// 저작물 정보
						rghtPrps.setTITLE(rghtPrps.getMUSIC_TITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setALBUM_TITLE(rghtPrps.getALBUM_TITLE_ARR()[i]);
						rghtPrps.setPERF_TIME(rghtPrps.getPERF_TIME_ARR()[i]);
						rghtPrps.setISSUED_DATE(rghtPrps.getISSUED_DATE_ARR()[i]);
						//rghtPrps.setLYRICS(rghtPrps.getLYRICS_ARR()[i]);
						
						
						// (1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						// (2) 앨범정보 Insert
						String album_id = ""+rghtPrpsDao.getAlbumId();
						rghtPrps.setPRPS_IDNT_ME(album_id);
						rghtPrpsDao.albumInsert(rghtPrps);
						
						// (3) 앨범음원정보 Insert
						String nr_id = ""+rghtPrpsDao.getNrId(rghtPrps);
						rghtPrps.setPRPS_IDNT_NR(nr_id);
						rghtPrpsDao.musicInsert(rghtPrps);
						
					}
					if(rghtPrps.getDIVS().equals("O")) {
						rghtPrps.setGENRE("2");	// 도서코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getTITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setBOOK_TITLE( rghtPrps.getBOOK_TITLE_ARR()[i]); 
						
						rghtPrps.setPUBLISHER( rghtPrps.getPUBLISHER_ARR()[i]); 
						rghtPrps.setFIRST_EDITION_YEAR( rghtPrps.getFIRST_EDITION_YEAR_ARR()[i]); 
						rghtPrps.setPUBLISH_TYPE( rghtPrps.getPUBLISH_TYPE_ARR()[i]); 
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						// (2) 도서정보 Insert
						String book_nr_id = ""+rghtPrpsDao.getBookNrId(rghtPrps);
						rghtPrps.setPRPS_IDNT_NR(book_nr_id);
						rghtPrpsDao.bookInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
					}
					
					if(rghtPrps.getDIVS().equals("N")) {  
						rghtPrps.setGENRE("21");	// 뉴스코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getTITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setOPEN_DATE(rghtPrps.getOPEN_DATE_ARR()[i]);
						
						// (1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						//(2) 뉴스정보 Insert
						rghtPrpsDao.newsInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						rghtPrps.setPRPS_IDNT_NR("0");
					}
					
					if(rghtPrps.getDIVS().equals("C")) { 
						rghtPrps.setGENRE("3");	// 방송대본코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getTITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setGENRE_KIND(rghtPrps.getGENRE_KIND_ARR()[i]);
						rghtPrps.setBROAD_MEDI(rghtPrps.getBROAD_MEDI_ARR()[i]);
						rghtPrps.setBROAD_STAT(rghtPrps.getBROAD_STAT_ARR()[i]);
						rghtPrps.setBROAD_ORD(rghtPrps.getBROAD_ORD_ARR()[i]);
						rghtPrps.setBROAD_DATE(rghtPrps.getBROAD_DATE_ARR()[i]);
						rghtPrps.setDIRECT(rghtPrps.getDIRECT_ARR()[i]);
						rghtPrps.setMAKER(rghtPrps.getMAKER_ARR()[i]);
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						//(2) 방송대본정보 Insert
						rghtPrpsDao.scriptInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						rghtPrps.setPRPS_IDNT_NR("0");
					}
					if(rghtPrps.getDIVS().equals("I")) {  
						rghtPrps.setGENRE("6");	// 이미지코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getWORK_NAME_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setIMAGE_DIVS(rghtPrps.getIMAGE_DIVS_ARR()[i]);
						rghtPrps.setOPEN_MEDI_CODE(rghtPrps.getOPEN_MEDI_CODE_ARR()[i]);
						rghtPrps.setOPEN_MEDI_TEXT(rghtPrps.getOPEN_MEDI_TEXT_ARR()[i]);
						rghtPrps.setOPEN_DATE(rghtPrps.getOPEN_DATE_ARR()[i]);
						
						// (1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						// (2) 이미지정보 Insert
						rghtPrpsDao.imageInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						rghtPrps.setPRPS_IDNT_NR("0");
					}
					if(rghtPrps.getDIVS().equals("V")) {
						rghtPrps.setGENRE("5");	// 영화코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getTITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setDIRECTOR(rghtPrps.getDIRECTOR_ARR()[i]);
						rghtPrps.setLEADING_ACTOR(rghtPrps.getLEADING_ACTOR_ARR()[i]);
						rghtPrps.setPRODUCE_DATE(rghtPrps.getPRODUCE_DATE_ARR()[i]);
						rghtPrps.setMEDIA_CODE_VALUE(rghtPrps.getMEDIA_CODE_VALUE_ARR()[i]);
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						//(2) 영화정보 Insert
						rghtPrpsDao.movieInsert(rghtPrps);
						
						//(3) 매체정보 Insert
						String media_id = ""+rghtPrpsDao.getMovieMediaId(rghtPrps);
						rghtPrps.setPRPS_IDNT_ME(media_id);
						rghtPrpsDao.movieMediaInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_NR("N");
					}
					if(rghtPrps.getDIVS().equals("R")) {  // 방송
						rghtPrps.setGENRE("4");	// 방송대본코드
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getTITLE_ARR()[i]);		// 저작물명 셋팅
						rghtPrps.setPROG_NAME(rghtPrps.getPROG_NAME_ARR()[i]);
						rghtPrps.setMEDI_CODE(rghtPrps.getMEDI_CODE_ARR()[i]);
						rghtPrps.setCHNL_CODE(rghtPrps.getCHNL_CODE_ARR()[i]);
						rghtPrps.setPROG_ORDSEQ(rghtPrps.getPROG_ORDSEQ_ARR()[i]);
						rghtPrps.setPROG_GRAD(rghtPrps.getPROG_GRAD_ARR()[i]);
						rghtPrps.setBROAD_DATE(rghtPrps.getBROAD_DATE_ARR()[i]);
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						//(2) 방송대본정보 Insert
						rghtPrpsDao.broadcastInsert(rghtPrps);
						
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						rghtPrps.setPRPS_IDNT_NR("0");
					}
					if(rghtPrps.getDIVS().equals("X")) {  // 기타
						rghtPrps.setGENRE("99");	// 기타 코드
						
						String genre = rghtPrps.getPRPS_SIDE_GENRE();
						
						// 저작물정보
						rghtPrps.setTITLE(rghtPrps.getWORK_NAME_ARR()[i]);		// 저작물명 셋팅
						//rghtPrps.setIMAGE_DIVS(rghtPrps.getIMAGE_DIVS_ARR()[i]);
						rghtPrps.setOPEN_MEDI_CODE(rghtPrps.getOPEN_MEDI_CODE_ARR()[i]);
						rghtPrps.setOPEN_MEDI_TEXT(rghtPrps.getOPEN_MEDI_TEXT_ARR()[i]);
						rghtPrps.setOPEN_DATE(rghtPrps.getOPEN_DATE_ARR()[i]);
						rghtPrps.setDIVS_CODE(rghtPrps.getPRPS_SIDE_R_GENRE_ARR()[i]);
						
						//(1) 저작물 Insert
						String cr_id = ""+rghtPrpsDao.getCrId();
						rghtPrps.setPRPS_IDNT(cr_id);
						rghtPrpsDao.worksInsert(rghtPrps);
						
						if(genre.equals("1")){
							rghtPrps.setBIG_CODE("20"); // 연극
						}else if(genre.equals("2")){
							rghtPrps.setBIG_CODE("21"); // 건축
						}else if(genre.equals("3")){
							rghtPrps.setBIG_CODE("22"); // 도형
						}else if(genre.equals("4")){
							rghtPrps.setBIG_CODE("23"); // 컴퓨터
						}else if(genre.equals("5")){
							rghtPrps.setBIG_CODE("24"); // 편집
						}else if(genre.equals("6")){
							rghtPrps.setBIG_CODE("25"); // 공공콘텐츠
						}else if(genre.equals("7")){
							rghtPrps.setBIG_CODE("26"); // 뉴스
						}
						
						//(2) 기타정보 Insert
						rghtPrpsDao.sideInsert(rghtPrps);
						// null
						rghtPrps.setPRPS_IDNT_ME("0");
						rghtPrps.setPRPS_IDNT_NR("0");
					}
					
				}
				else {
					
					rghtPrps.setPRPS_IDNT_CODE("1");	// 기존저작물
					
					// 저작물 key 값 셋팅 : prps_idnt/ prps_idnt_nr/ prps_idnt_me
					String keyIdArr[] = rghtPrps.getKeyId()[i].split("\\|");
					
					rghtPrps.setPRPS_IDNT(keyIdArr[0]);
					if(keyIdArr.length>1) rghtPrps.setPRPS_IDNT_NR(keyIdArr[1]); else rghtPrps.setPRPS_IDNT_NR("0");
					if(keyIdArr.length>2) rghtPrps.setPRPS_IDNT_ME(keyIdArr[2]); else rghtPrps.setPRPS_IDNT_ME("0");
					//rghtPrps.setPRPS_IDNT_NR(keyIdArr[1]);
					//rghtPrps.setPRPS_IDNT_ME(keyIdArr[2]);
					
				}
					
				for( int k=0; k<trstCnt; k++) {
					rghtPrps.setTRST_ORGN_CODE(rghtPrps.getTRST_ORGN_CODE_ARR()[k]);
					rghtPrpsDao.prpsRsltInsert(rghtPrps);
					
					// 5. 기관별 관리권리정보 입력
					// 음악, 도서, 방송대본, 이미지, 영화, 방송
					if(!rghtPrps.getDIVS().equals("X")){
						// (1) 음저협
						if( rghtPrps.getTRST_ORGN_CODE().equals("201")) {	
							
							i201++;
							trstOrgnCode = "201";
							// 신청목적이 이용자 권리조회가 아닌경우.
							//if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ) {
								
								// 작사(101)
								rghtPrps.setRGHT_ROLE_CODE("101"); 
								rghtPrps.setHOLD_NAME(rghtPrps.getLYRICIST_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getLYRICIST_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 작곡(102)
								rghtPrps.setRGHT_ROLE_CODE("102");
								rghtPrps.setHOLD_NAME(rghtPrps.getCOMPOSER_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOMPOSER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 편곡(103)
								rghtPrps.setRGHT_ROLE_CODE("103");
								rghtPrps.setHOLD_NAME(rghtPrps.getARRANGER_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getARRANGER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							//}
						}
						
						// (2) 음실연
						if(rghtPrps.getTRST_ORGN_CODE().equals("202")) {	
							
							i202++;
							trstOrgnCode = "202";
							// 신청목적이 이용자 권리조회가 아닌경우.
							//if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ) {
								
								// 가창(112)
								rghtPrps.setRGHT_ROLE_CODE("112");
								rghtPrps.setHOLD_NAME(rghtPrps.getSINGER_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getSINGER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 연주(113)
								rghtPrps.setRGHT_ROLE_CODE("113");
								rghtPrps.setHOLD_NAME(rghtPrps.getPLAYER_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getPLAYER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 지휘(113)
								rghtPrps.setRGHT_ROLE_CODE("114");
								rghtPrps.setHOLD_NAME(rghtPrps.getCONDUCTOR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCONDUCTOR_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							//}
						}
		
						// (3) 음제협
						if(rghtPrps.getTRST_ORGN_CODE().equals("203")) {
						
							i203++;
							trstOrgnCode = "203";
							//	신청목적이 이용자 권리조회가 아닌경우.
							//if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ) {
								
							
							// 앨범제작(111)
							rghtPrps.setRGHT_ROLE_CODE("111");
							rghtPrps.setHOLD_NAME(rghtPrps.getPRODUCER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getPRODUCER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							//}
						}
						
						// (4)문예협
						if(rghtPrps.getTRST_ORGN_CODE().equals("204")) {
							
							i204++;
							trstOrgnCode = "204";
							
							if(!rghtPrps.getDIVS().equals("I")) {
								// 저자(201)
								rghtPrps.setRGHT_ROLE_CODE("201");
								rghtPrps.setHOLD_NAME(rghtPrps.getLICENSOR_NAME_KOR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getLICENSOR_NAME_ORGN_KOR_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 역자(202)
								rghtPrps.setRGHT_ROLE_CODE("202");
								rghtPrps.setHOLD_NAME(rghtPrps.getTRANSLATOR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getTRANSLATOR_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							}else if(rghtPrps.getDIVS().equals("I")) {
								// 저작권자(301)
								rghtPrps.setRGHT_ROLE_CODE("301");
								rghtPrps.setHOLD_NAME(rghtPrps.getCOPT_HODR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ){ // 권리자 권리찾기
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOPT_HODR_ORGN_ARR()[i]);
								}	
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							}
						}
							
						// (5)복전협
						if(rghtPrps.getTRST_ORGN_CODE().equals("205")) {
							
							i205++;
							trstOrgnCode = "205";
							
							if(rghtPrps.getDIVS().equals("M")) {
								
								// 작사(101)
								rghtPrps.setRGHT_ROLE_CODE("101"); 
								rghtPrps.setHOLD_NAME(rghtPrps.getLYRICIST_ARR()[i].replaceAll("``", "&quot;"));
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getLYRICIST_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 작곡(102)
								rghtPrps.setRGHT_ROLE_CODE("102");
								rghtPrps.setHOLD_NAME(rghtPrps.getCOMPOSER_ARR()[i].replaceAll("``", "&quot;"));
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOMPOSER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 편곡(103)
								rghtPrps.setRGHT_ROLE_CODE("103");
								rghtPrps.setHOLD_NAME(rghtPrps.getARRANGER_ARR()[i].replaceAll("``", "&quot;"));
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getARRANGER_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
							}else if(rghtPrps.getDIVS().equals("O")) {
								// 저자(201)
								rghtPrps.setRGHT_ROLE_CODE("201");
								rghtPrps.setHOLD_NAME(rghtPrps.getLICENSOR_NAME_KOR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getLICENSOR_NAME_ORGN_KOR_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
								
								// 역자(202)
								rghtPrps.setRGHT_ROLE_CODE("202");
								rghtPrps.setHOLD_NAME(rghtPrps.getTRANSLATOR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getTRANSLATOR_ORGN_ARR()[i]);
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							}else if(rghtPrps.getDIVS().equals("I")) {
								// 저작권자(301)
								rghtPrps.setRGHT_ROLE_CODE("301");
								rghtPrps.setHOLD_NAME(rghtPrps.getCOPT_HODR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ){ // 권리자 권리찾기
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOPT_HODR_ORGN_ARR()[i]);
								}	
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							}	
						}
						
						// (6)방작협
						if(rghtPrps.getTRST_ORGN_CODE().equals("206")){
							
							i206++;
							trstOrgnCode = "206";
							//작가(501)
							rghtPrps.setRGHT_ROLE_CODE("501");
							rghtPrps.setHOLD_NAME(rghtPrps.getWRITER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3"))
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getWRITER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
						
						// (11)영산협
						if(rghtPrps.getTRST_ORGN_CODE().equals("211")) {
							
							i211++;
							trstOrgnCode = "211";
							
							// 투자사(402)
							rghtPrps.setRGHT_ROLE_CODE("402");
							rghtPrps.setHOLD_NAME(rghtPrps.getINVESTOR_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getINVESTOR_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							
							// 배급사(403)
							rghtPrps.setRGHT_ROLE_CODE("403");
							rghtPrps.setHOLD_NAME(rghtPrps.getDISTRIBUTOR_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getDISTRIBUTOR_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
						
						// (12)시나리오작가협
						if(rghtPrps.getTRST_ORGN_CODE().equals("212")) {
							
							i212++;
							trstOrgnCode = "212";
							// 제작자(404)
							rghtPrps.setRGHT_ROLE_CODE("404");
							rghtPrps.setHOLD_NAME(rghtPrps.getWRITER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getWRITER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}

						// (13)영제협
						if(rghtPrps.getTRST_ORGN_CODE().equals("213")) {
							
							i213++;
							trstOrgnCode = "213";
							// 제작자(401)
							rghtPrps.setRGHT_ROLE_CODE("401");
							rghtPrps.setHOLD_NAME(rghtPrps.getPRODUCER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getPRODUCER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							
							// 투자사(402)
							rghtPrps.setRGHT_ROLE_CODE("402");
							rghtPrps.setHOLD_NAME(rghtPrps.getINVESTOR_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getINVESTOR_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							
							// 배급사(403)
							rghtPrps.setRGHT_ROLE_CODE("403");
							rghtPrps.setHOLD_NAME(rghtPrps.getDISTRIBUTOR_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") )
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getDISTRIBUTOR_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
						
						// (8)한국방송콘텐츠협회
						if(rghtPrps.getTRST_ORGN_CODE().equals("1")){
							
							i1++;
							trstOrgnCode = "1";
							//제작자(601)
							rghtPrps.setRGHT_ROLE_CODE("601");
							rghtPrps.setHOLD_NAME(rghtPrps.getMAKER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3"))
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getMAKER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
						
						// (14)한국방송실연자연합회
						if(rghtPrps.getTRST_ORGN_CODE().equals("214")){
							
							i1++;
							trstOrgnCode = "214";
							
							//제작자(601)
							rghtPrps.setRGHT_ROLE_CODE("601");
							rghtPrps.setHOLD_NAME(rghtPrps.getMAKER_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3"))
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getMAKER_ORGN_ARR()[i]);
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
						
//						 (15)한국언론진흥재단
						if(rghtPrps.getTRST_ORGN_CODE().equals("215")) {
							
							i215++;
							trstOrgnCode = "215";
							
							// 언론사(701)
							rghtPrps.setRGHT_ROLE_CODE("701");
							rghtPrps.setHOLD_NAME(rghtPrps.getCOPT_HODR_ARR()[i]);
							if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ){ // 권리자 권리찾기
								rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOPT_HODR_ORGN_ARR()[i]);
							}	
							rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
						}
						
						//(16)한국문화콘텐츠진흥원
						if(rghtPrps.getTRST_ORGN_CODE().equals("216")) {
							
							i216++;
							
							if(rghtPrps.getDIVS().equals("I")) {
								// 저작권자(301)
								rghtPrps.setRGHT_ROLE_CODE("301");
								rghtPrps.setHOLD_NAME(rghtPrps.getCOPT_HODR_ARR()[i]);
								if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ){ // 권리자 권리찾기
									rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOPT_HODR_ORGN_ARR()[i]);
								}	
								rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
							}
						}
						
					}else{
						// 기타저작물 권리찾기 신청
						if(rghtPrps.getTRST_ORGN_CODE().equals("201")) {
							i201++;
							trstOrgnCode = "201";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("202")) {
							i202++;
							trstOrgnCode = "202";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("203")) {
							i203++;
							trstOrgnCode = "203";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("204")) {
							i204++;
							trstOrgnCode = "204";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("205")) {
							i205++;
							trstOrgnCode = "205";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("206")) {
							i206++;
							trstOrgnCode = "206";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("211")) {
							i211++;
							trstOrgnCode = "211";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("212")) {
							i212++;
							trstOrgnCode = "212";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("213")) {
							i213++;
							trstOrgnCode = "213";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("214")) {
							i214++;
							trstOrgnCode = "214";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("215")) {
							i215++;
							trstOrgnCode = "215";
						}
						if(rghtPrps.getTRST_ORGN_CODE().equals("216")) {
							i216++;
							trstOrgnCode = "216";
						}
						// 기타 저작권자(001)
						rghtPrps.setRGHT_ROLE_CODE("001");
						if(!rghtPrps.getPRPS_RGHT_CODE().equals("3") ){ // 권리자 권리찾기
							
							rghtPrps.setHOLD_NAME(rghtPrps.getCOPT_HODR_ARR()[i]);
							rghtPrps.setHOLD_NAME_ORGN(rghtPrps.getCOPT_HODR_ORGN_ARR()[i]);
						}	
						rghtPrpsDao.prpsRsltRghtInsert(rghtPrps);
					}
					// 첨부파일
					if( (i201 == 1 && trstOrgnCode.equals("201")) ||	
							(i202 == 1 && trstOrgnCode.equals("202")) ||	
							(i203 == 1 && trstOrgnCode.equals("203")) ||	
							(i204 == 1 && trstOrgnCode.equals("204")) ||	
							(i205 == 1 && trstOrgnCode.equals("205")) ||	
							(i206 == 1 && trstOrgnCode.equals("206")) ||	
							(i211 == 1 && trstOrgnCode.equals("211")) ||
							(i212 == 1 && trstOrgnCode.equals("212")) ||
							(i213 == 1 && trstOrgnCode.equals("213")) ||
							(i214 == 1 && trstOrgnCode.equals("214")) ||
							(i215 == 1 && trstOrgnCode.equals("215")) ||
							(i216 == 1 && trstOrgnCode.equals("216")) ||
							(i1 == 1 && trstOrgnCode.equals("1")))
					{
						PrpsAttc prpsAttc = new PrpsAttc();
						PrpsAttc prpsAttcFile = new PrpsAttc();	
						
						iAll++;      
						for(int fi=0; fi < rghtPrps.getFileList().size(); fi++) {
							
							prpsAttc = (PrpsAttc) rghtPrps.getFileList().get(fi);
							
							// 공통파일
							if( prpsAttc.getTRST_ORGN_CODE().equals("0") && iAll == 1){
								
								prpsAttcFile.setREAL_FILE_NAME(prpsAttc.getREAL_FILE_NAME());
								prpsAttcFile.setFILE_PATH(prpsAttc.getFILE_PATH());
								prpsAttcFile.setTRST_ORGN_CODE(prpsAttc.getTRST_ORGN_CODE());
								prpsAttcFile.setPRPS_DIVS(prpsDivs);
								prpsAttcFile.setUSER_IDNT(userIdnt);
								prpsAttcFile.setPRPS_MAST_KEY(rghtPrps.getPRPS_MAST_KEY());
								prpsAttcFile.setFILE_SIZE(prpsAttc.getFILE_SIZE());
								prpsAttcFile.setFILE_NAME(prpsAttc.getFILE_NAME());
							
								rghtPrpsDao.insertPrpsAttc(prpsAttcFile);
							}
							// 첨부파일의 신탁단체와 신청기관이 같은경우만 등록
							else if( prpsAttc.getTRST_ORGN_CODE().equals(trstOrgnCode)){
								
								prpsAttcFile.setREAL_FILE_NAME(prpsAttc.getREAL_FILE_NAME());
								prpsAttcFile.setFILE_PATH(prpsAttc.getFILE_PATH());
								prpsAttcFile.setTRST_ORGN_CODE(prpsAttc.getTRST_ORGN_CODE());
								prpsAttcFile.setPRPS_DIVS(prpsDivs);
								prpsAttcFile.setUSER_IDNT(userIdnt);
								prpsAttcFile.setPRPS_MAST_KEY(rghtPrps.getPRPS_MAST_KEY());
								prpsAttcFile.setFILE_SIZE(prpsAttc.getFILE_SIZE());
								prpsAttcFile.setFILE_NAME(prpsAttc.getFILE_NAME());
							
								rghtPrpsDao.insertPrpsAttc(prpsAttcFile);
							}
						}	
					} // .. end if 첨부파일
					
				} // .. end for 기관별 입력
				
			} // .. end for 저작물 입력
			
			
			// 접속정보 저장---------------------
			HashMap connMap = new HashMap();
			connMap.put("userIdnt", rghtPrps.getUSER_IDNT());
			connMap.put("connUrl", "권리찾기신청 수정");
			inmtPrpsDao.insertConnInfo(connMap);
			//-------------------------------------
		
		} catch(Exception e){
			
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			iResult = -1;
		}
		
		// 삭제된 첨부파일 물리삭제
		for( int fi =0; fi<rghtPrps.getDELT_FILE_INFO().length; fi++) {
			
			File file = new File( rghtPrps.getDELT_FILE_INFO()[fi] );
			
			try{
				if (file.exists()) 
			    	 file.delete(); 
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		
		// mail/ sms 발송
		return iResult;
	}
}
