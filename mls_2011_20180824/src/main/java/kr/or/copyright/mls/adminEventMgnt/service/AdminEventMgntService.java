package kr.or.copyright.mls.adminEventMgnt.service;

public interface AdminEventMgntService {

	//�׽�Ʈ
	public void saveEvent() throws Exception;
	public void updateEvent() throws Exception;
	public void getEventList() throws Exception;
	public void delEventList() throws Exception;
	public void getEventDetl() throws Exception;
	public void uptWinY() throws Exception;
	public void uptWinCancel() throws Exception;
	public void getEventStatList() throws Exception;
	public void getEventQnaList() throws Exception;
	public void getEventQnaDetail() throws Exception;
	public void delEventQnaDetail() throws Exception;
	public void uptEventQnaAnswRegi() throws Exception;
	public void uptEventQnaMenuOpen() throws Exception;
	public void getEventQnaMenuOpen() throws Exception;
}
