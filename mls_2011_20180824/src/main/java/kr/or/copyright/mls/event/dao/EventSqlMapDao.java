package kr.or.copyright.mls.event.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.event.model.Event;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class EventSqlMapDao extends SqlMapClientDaoSupport implements EventDao{
	
	// 중복확인
	public int checkCampPart(Event event) {
		Integer cnt = (Integer)getSqlMapClientTemplate().queryForObject("Event.checkCampPart" ,event );
		return cnt;
	}
	
	// 참가자정보 insert
	public void insertCampPartInfo (Event event) {
		getSqlMapClientTemplate().insert("Event.insertCampPartInfo", event);
	}
	
	// 설문참가 정보 insert
	public void insertCampPartRslt (Event event) {
		getSqlMapClientTemplate().insert("Event.insertCampPartRslt", event);
	}
	
	// 설문참가 정보 List
	public List campPartList(Map map) {
		return getSqlMapClientTemplate().queryForList("Event.campPartList", map);
	}
}
