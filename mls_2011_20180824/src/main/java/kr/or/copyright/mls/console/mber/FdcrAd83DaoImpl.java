package kr.or.copyright.mls.console.mber;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAd83Dao" )
public class FdcrAd83DaoImpl extends EgovComAbstractDAO implements FdcrAd83Dao {

	// 회원정보 목록조회
	public List userList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.userList",map);
	}
	
	/**
	 * 회원정보 카운트
	 * @return
	 * @throws SQLException
	 */
	public int userListCount( Map<String, Object> commandMap ) throws SQLException{
		return (Integer) selectByPk( "AdminUser.userListCount", commandMap );
	}

	/**
	 * 회원정보 카운트
	 * @return
	 * @throws SQLException
	 */
	public int trstOrgnMgnbListCount( Map<String, Object> commandMap ) throws SQLException{
		return (Integer) selectByPk( "AdminUser.trstOrgnMgnbListCount", commandMap );
	}
	
	//회원정보 상세조회
	public List userDetlList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.userDetlList",map);
	}	
	
	//신탁단체 담당자목록 조회
	public List trstOrgnMgnbList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.trstOrgnMgnbList",map);
	}	

	//신탁단체 담당자 상세 조회
	public List trstOrgnMgnbDetlList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.trstOrgnMgnbDetlList",map);
	}
	
	
	//신탁단체 담당자 입력
	public void trstOrgnMgnbDetlInsert(Map map) {
		getSqlMapClientTemplate().insert("AdminUser.trstOrgnMgnbDetlInsert",map);
	}	
	
	//신탁단체 담당자 수정
	public void trstOrgnMgnbDetlUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.trstOrgnMgnbDetlUpdate",map);
	}	
	
	//담당자 ,회원 비밀번호 수정
	public void userMgnbPswdUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.userMgnbPswdUpdate",map);
	}
	
	//담당자 ,ML_CODE 검색
	public List fdcrAd83MlCodeSelectList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.mlcodeSelectList",map);
	}
	
	//담당자 ,업체명으로 ML_USER_INFO 검색
	public List fdcrAd83CommNameSelectList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.CommNameSelectList",map);
	}
	
	//담당자 ,업체명으로 ML_COMM_WORKS 검색
	public List fdcrAd83commNameSelectListByCommWorks(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.commNameSelectListByCommWorks",map);
	}
	
	//담당자 ,업체명 변경 히스토리
	public void insertCommNameUpdateHistory(Map map) {
		getSqlMapClientTemplate().insert("AdminUser.insertCommNameUpdateHistory",map);
	}
	
	//담당자 ,ML_USER_INFO user_name 수정
	public void fdcrAd83UserNameUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.userNameUpdate",map);
	}
	
	//담당자 ,업체명 ML_USER_INFO COMM_NAME 수정
	public void fdcrAd83UserInfoCommNameUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.userMgnbCommNameUpdate",map);
	}
	
	//담당자 ,업체명 ML_COMM_WORKS_REPORT COMM_NAME 수정
	public void fdcrAd83CommWorksReportCommNameUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.MlCommWorksReportCommNameUpdate",map);
	}
	
	//담당자 ,업체명 ML_COMM_WORKS COMM_NAME 수정
	public void fdcrAd83MlCommWorksCommNameUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.MlCommWorksCommNameUpdate",map);
	}
	
	//CLMS 회원정보 update
	public void updateClmsUserUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.updateClmsUserCommName",map);
	}
	
	//fdcrAd83CompanyDelete
	public void fdcrAd83CompanyDelete(Map map) {
		getSqlMapClientTemplate().update("AdminUser.CompanyUserDelete",map);
	}
	//담당자,ML_COMM_WORK_REPORT 업체 삭제 데이터 백업
	public void fdcrAd83CommWorksReportCommNameBackup( Map map ) {
		getSqlMapClientTemplate().update("AdminUser.CommWorksReportCommNameBackup",map);
	}
	//담당자,ML_COMM_WORK_REPORT 업체 데이터 삭제 
	public void fdcrAd83CommWorksReportCommNameDelete( Map map ) {
		getSqlMapClientTemplate().update("AdminUser.CommWorksReportCommNameDelete",map);
	}
	//fdcrAd83UserBackup
	public void fdcrAd83UserBackup(Map map) {
		getSqlMapClientTemplate().update("AdminUser.fdcrAd83UserBackup",map);
	}
	
	//담당자,ML_COMM_WORKS 업체 삭제 데이터 백업 
	public void fdcrAd83MlCommWorksCommNameBackup( Map map ) {
		getSqlMapClientTemplate().update("AdminUser.MlCommWorksCommNameBackup",map);
	}
		
	//담당자,ML_COMM_WORKS 업체 데이터 삭제
	public void fdcrAd83MlCommWorksCommNameDelete( Map map ) {
		getSqlMapClientTemplate().update("AdminUser.MlCommWorksCommNameDelete",map);
	}
	
	//담당자,ML_COMM_TABLES 업체 데이터 검색
	public List fdcrAd83CommWorksTablesCommNameSelect(  Map map  ) {
		return getSqlMapClientTemplate().queryForList("AdminUser.CommWorksTablesCommNameSelect",map);
	}
	
	//담당자,ML_COMM_TABLES 업체 삭제 데이터 백업
	public void fdcrAd83CommWorksTablesCommNameBackup(  Map map  ) {
		getSqlMapClientTemplate().update("AdminUser.CommWorksTablesCommNameBackup",map);
	}
	
	//담당자,ML_COMM_TABLES 업체 데이터 삭제
	public void fdcrAd83CommWorksTablesCommNameDelete(  Map map  ) {
		getSqlMapClientTemplate().update("AdminUser.CommWorksTablesCommNameDelete",map);
	}
	
	// 그룹 메뉴 조회 하기
	public List adminGroupMenuInfo(Map map){
		return getSqlMapClientTemplate().queryForList("AdminUser.adminGroupMenuInfo",map);
	}
	
	// 관리자 아이디 중복체크
	public int adminIdCheck(Map map){
		int iUserCnt = (Integer)getSqlMapClientTemplate().queryForObject("AdminUser.adminIdCheck", map);
		return iUserCnt;
	}

	public List trstOrgnCoNameList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.trstOrgnCoNameList",map);
	}

	public void updateClmsUserInfo(Map map) {
		getSqlMapClientTemplate().update("AdminUser.updateClmsUserInfo",map);
		
	}
	
	// 대표담당자정보 수정
	public void orgnMgnbUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.orgnMgnbUpdate",map);
	}	
	
	// 대표담당자 존재여부 체크
	public int orgnMgnbCheck(Map map){
		int iUserCnt = (Integer)getSqlMapClientTemplate().queryForObject("AdminUser.orgnMgnbCheck", map);
		return iUserCnt;
	}

	//문예협단체 CA_ID 체크
	public int getNewTrstOrgnCd(){
		return Integer.parseInt( "" + getSqlMapClientTemplate().queryForObject( "AdminUser.getNewTrstOrgnCd" ) );
	}
	
	public int trstOrgnCoNameListCount( Map<String, Object> commandMap ) throws SQLException{
		return (Integer) selectByPk( "AdminUser.trstOrgnCoNameListCount", commandMap );
	}

	public void fncUpdateFailCnt( Map<String, Object> commandMap ){
		getSqlMapClientTemplate().update("AdminUser.fncUpdateFailCnt",commandMap);
	}
}
