package kr.or.copyright.mls.mobile.board.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.mobile.board.dao.MBoardDao;
import kr.or.copyright.mls.mobile.board.model.MBoard;

public class MBoardServiceImpl  extends BaseService implements MBoardService {

	
	private MBoardDao mBoardDao;
	
	public void setMBoardDao (MBoardDao mBoardDao) {
		this.mBoardDao = mBoardDao;
	}
	
	public ListResult findBoardList(int pageNo, int rowPerPage, Map params) {
		
		int from = rowPerPage * (pageNo-1) + 1;
        int to = rowPerPage * pageNo;
        
        params.put("FROM", ""+from);
        params.put("TO", ""+to);
        int totalRow = 0;
        
        totalRow = mBoardDao.findBoardCount(params);
        List list = mBoardDao.findBoardList(params);
	
        ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
        
        return listResult;
	}
	
	public MBoard boardView(MBoard board) {
		
		board = mBoardDao.boardView(board);
		
		return board;
	}
}
