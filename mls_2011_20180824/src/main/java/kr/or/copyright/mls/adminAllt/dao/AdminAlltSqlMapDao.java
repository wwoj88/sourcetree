package kr.or.copyright.mls.adminAllt.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class AdminAlltSqlMapDao extends SqlMapClientDaoSupport implements AdminAlltDao {

	public List alltInmtDetl(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminAllt.alltInmtDetl", map);
	}
	
	public void alltInmtUpdate(Map map){
		getSqlMapClientTemplate().update("AdminAllt.alltInmtUpdate",map);
	}
	
	public List alltInmtCount(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminAllt.alltInmtCount", map);
	}
	
	public void alltInmtSave(Map map) {
		getSqlMapClientTemplate().insert("AdminAllt.alltInmtSave",map);
	}	
	
	public List alltInmtList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminAllt.alltInmtList", map);
	}
	
	public void alltInmtYsNoUpdate(Map map){
		getSqlMapClientTemplate().update("AdminAllt.alltInmtYsNoUpdate",map);
	}
	
	public void alltInmtAllUpdate(Map map){
		getSqlMapClientTemplate().update("AdminAllt.alltInmtAllUpdate",map);
	}

	/* (non-Javadoc)
	 * @Author ����ȣ
	 * @Date 2012.11.20
	 */
	public List alltInmtMgntList(Map map) {
	    return getSqlMapClientTemplate().queryForList("AdminAllt.alltInmtMgntList",map);
	}

	public void alltInmtMgntUpdate(Map map) {
	    getSqlMapClientTemplate().update("AdminAllt.alltInmtMgntUpdate",map);
	    
	}

	public String alltInmtMgntMaxYear() {
	    // TODO Auto-generated method stub
	    return (String) getSqlMapClientTemplate().queryForObject("AdminAllt.alltInmtMgntMaxYear", null);
	}

	public void alltInmtMgntInsert(Map map) {
	    getSqlMapClientTemplate().insert("AdminAllt.alltInmtMgntInsert",map);
	}
}
