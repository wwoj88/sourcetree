package kr.or.copyright.mls.console.stats;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.stats.inter.FdcrAd98Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAd98Dao" )
public class FdcrAd98DaoImpl extends EgovComAbstractDAO implements FdcrAd98Dao {
	
	public List statSrchList(Map map) {
		return getSqlMapClientTemplate().queryForList("Srch.statSrchList",map);
	}	
	
	public List statSrchListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Srch.statSrchListCount",map);
	}

}
