package kr.or.copyright.mls.console.legal;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.legal.inter.FdcrAd10Service;
import kr.or.copyright.mls.myStat.service.MyStatService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 법정허락 > 보상금 공탁공고
 * 
 * @author ljh
 */
@Controller
public class FdcrAd10Controller extends DefaultController{

	@Resource( name = "fdcrAd10Service" )
	private FdcrAd10Service fdcrAd10Service;

	@Resource( name = "myStatService" )
	private MyStatService myStatService;

	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd10Controller.class );

	/**
	 * 보상금 공탁공고 목록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10List1.page" )
	public String fdcrAd10List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		EgovWebUtil.ajaxCommonEncoding( request, commandMap );

		commandMap.put( "BORD_CD", "5" );
		commandMap.put( "TAB_INDEX", "0" );
		/*
		String SCH_RECEIPT_NO = EgovWebUtil.getString( commandMap, "SCH_RECEIPT_NO" );
		String SCH_TITL = EgovWebUtil.getString( commandMap, "SCH_TITL" );
		
		if( !"".equals( SCH_RECEIPT_NO ) ){
			SCH_RECEIPT_NO = URLDecoder.decode( SCH_RECEIPT_NO, "UTF-8" );
			commandMap.put( "SCH_RECEIPT_NO", SCH_RECEIPT_NO );
		}
		if( !"".equals( SCH_TITL ) ){
			SCH_RECEIPT_NO = URLDecoder.decode( SCH_TITL, "UTF-8" );
			commandMap.put( "SCH_TITL", SCH_TITL );
		}
		*/
		
		fdcrAd10Service.fdcrAd10List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_count", commandMap.get( "ds_count" ) );
		model.addAttribute( "ds_count0", ( (List) commandMap.get( "ds_count" ) ).get( 0 ) );
		model.addAttribute( "ds_count1", ( (List) commandMap.get( "ds_count" ) ).get( 1 ) );
		model.addAttribute( "ds_count2", ( (List) commandMap.get( "ds_count" ) ).get( 2 ) );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		System.out.println( "보상금 공탁공고 게시판 목록" );
		return "legal/fdcrAd10List1.tiles";
	}

	/**
	 * 미공고 목록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10List2.page" )
	public String fdcrAd10List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "BORD_CD", "5" );
		commandMap.put( "TAB_INDEX", "1" );
		commandMap.put( "SCH_RECEIPT_NO", "" );
		commandMap.put( "SCH_TITL", "" );

		fdcrAd10Service.fdcrAd10List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_count", commandMap.get( "ds_count" ) );
		model.addAttribute( "ds_count0", ( (List) commandMap.get( "ds_count" ) ).get( 0 ) );
		model.addAttribute( "ds_count1", ( (List) commandMap.get( "ds_count" ) ).get( 1 ) );
		model.addAttribute( "ds_count2", ( (List) commandMap.get( "ds_count" ) ).get( 2 ) );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		System.out.println( "미공고 목록" );
		return "legal/fdcrAd10List2.tiles";
	}

	/**
	 * 반려 목록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10List3.page" )
	public String fdcrAd10List3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "BORD_CD", "5" );
		commandMap.put( "TAB_INDEX", "2" );
		commandMap.put( "SCH_RECEIPT_NO", "" );
		commandMap.put( "SCH_TITL", "" );

		fdcrAd10Service.fdcrAd10List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_count", commandMap.get( "ds_count" ) );
		model.addAttribute( "ds_count0", ( (List) commandMap.get( "ds_count" ) ).get( 0 ) );
		model.addAttribute( "ds_count1", ( (List) commandMap.get( "ds_count" ) ).get( 1 ) );
		model.addAttribute( "ds_count2", ( (List) commandMap.get( "ds_count" ) ).get( 2 ) );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		System.out.println( "반려 목록" );
		return "legal/fdcrAd10List3.tiles";
	}

	/**
	 * 보상금 공탁공고 게시판 상세
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10View1.page" )
	public String fdcrAd10View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		//commandMap.put( "OPEN_YN", "" );
		commandMap.put( "BIG_CODE", "88" );

		fdcrAd10Service.fdcrAd10View1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_file", commandMap.get( "ds_file" ) );
		model.addAttribute( "ds_supl_list", commandMap.get( "ds_supl_list" ) );
		System.out.println( "이용승인 신청 공고 상세" );
		return "legal/fdcrAd10View1.tiles";
	}

	/**
	 * 보상금 공탁공고 게시판 등록자 정보 조회
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10Pop1.page" )
	public String fdcrAd10Pop1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAd10Service.fdcrAd10Pop1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "보상금 공탁공고 게시판 등록자 정보 조회" );
		return "legal/fdcrAd10Pop1.popup";
	}

	/**
	 * 보상금 공탁공고 게시판 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10Download1.page" )
	public void fdcrAd10Download1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		fdcrAd10Service.fdcrAd10View1( commandMap );
		List list = (List) commandMap.get( "ds_file" );
		for( int i = 0; i < list.size(); i++ ){
			Map map = (Map) list.get( i );
			String bordCd = (String) map.get( "BORD_CD" );
			String bordSeqn = (String) map.get( "BORD_SEQN" );
			if( bordCd.equals( commandMap.get( "BORD_CD" ) ) && bordSeqn.equals( commandMap.get( "BORD_SEQN" ) ) ){
				String filePath = (String) map.get( "FILE_PATH" );
				String fileName = (String) map.get( "FILE_NAME" );
				String realFileName = (String) map.get( "REAL_FILE_NAME" );
				download( request, response, filePath + realFileName, fileName );
			}
		}
	}

	/**
	 * 보상금 공탁공고 게시판 공고 승인
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10Update1.page" )
	public void fdcrAd10Update1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		boolean isSuccess = fdcrAd10Service.fdcrAd10Update1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "보상금 공탁공고 게시판 공고 승인" );
	}

	/**
	 * 보상금 공탁공고 게시판 공고 처리상태 수정
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10Update2.page" )
	public String fdcrAd10Update2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String ANUC_STAT_CD = (String) commandMap.get( "ANUC_STAT_CD" );
		System.out.println( "ANUC_STAT_CD:::" + ANUC_STAT_CD );
		boolean isSuccess = fdcrAd10Service.fdcrAd10Update2( commandMap );
		if( ANUC_STAT_CD.equals( "3" ) ){
			fdcrAd10Update1( model, commandMap, request, response );
		}
		returnAjaxString( response, isSuccess );
		System.out.println( "보상금 공탁공고 게시판 공고 처리상태 수정" );
		if( isSuccess ){
			return returnUrl( model, "수정했습니다.", "/console/legal/fdcrAd10List2.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legal/fdcrAd10List2.page" );
		}
	}

	/**
	 * 보상금 공탁공고 게시판 공고 삭제
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10Delete1.page" )
	public String fdcrAd10Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		boolean isSuccess = fdcrAd10Service.fdcrAd10Delete1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "보상금 공탁공고 게시판 공고 삭제" );

		if( isSuccess ){
			return returnUrl( model, "삭제했습니다.", "/console/legal/fdcrAd10List2.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legal/fdcrAd10List2.page" );
		}
	}

	/**
	 * 보상금 공탁공고 게시판 공고내용 보완 팝업 폼
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10Pop2.page" )
	public String fdcrAd10Pop2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "SEND_MAIL_ADDR", "" );
		commandMap.put( "BIG_CODE", 89 );
		fdcrAd10Service.fdcrAd10Pop2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_file", commandMap.get( "ds_file" ) );
		model.addAttribute( "ds_code", commandMap.get( "ds_code" ) );
		model.addAttribute( "ds_genre", commandMap.get( "ds_genre" ) );

		System.out.println( "보상금 공탁공고 게시판 공고내용 보완팝업 폼" );
		return "legal/fdcrAd10Pop2.popup";
	}

	/**
	 * 보상금 공탁공고 게시판 공고내용 수정 및 보완이력 등록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	/*
	 * @RequestMapping( value = "/console/legal/fdcrAd10SuplRegi1.page" ) public
	 * void fdcrAd10SuplRegi1( ModelMap model, Map<String, Object> commandMap,
	 * HttpServletRequest request, HttpServletResponse response ) throws
	 * Exception{ commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
	 * commandMap.put( "SUPL_IDS", EgovWebUtil.getString( commandMap, "SUPL_IDS"
	 * ) ); commandMap.put( "SUPL_ITEM_CDS", EgovWebUtil.getString( commandMap,
	 * "SUPL_ITEM_CDS" ) ); commandMap.put( "PRE_ITEMS", EgovWebUtil.getString(
	 * commandMap, "PRE_ITEMS" ) ); commandMap.put( "POST_ITEMS",
	 * EgovWebUtil.getString( commandMap, "POST_ITEMS" ) ); boolean isSuccess =
	 * fdcrAd10Service.fdcrAd10SuplRegi1( commandMap ); returnAjaxString(
	 * response, isSuccess ); System.out.println(
	 * "보상금 공탁공고 게시판 공고내용 수정 및 보완이력 등록 " ); }
	 */

	/**
	 * 보상금 공탁공고 게시판 공고내용 수정 및 보완이력 등록
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10SuplRegi1.page" )
	public String fdcrAd10SuplRegi1( Map<String, Object> commandMap,
		ModelMap model,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 기존 컬럼
		Map<String, Object> info = fdcrAd10Service.fdcrAd10Detail( commandMap );

		String ANUC_ITEM_1 = EgovWebUtil.getString( info, "ANUC_ITEM_1" );
		String ANUC_ITEM_2 = EgovWebUtil.getString( info, "ANUC_ITEM_2" );
		String ANUC_ITEM_3 = EgovWebUtil.getString( info, "ANUC_ITEM_3" );
		String ANUC_ITEM_4 = EgovWebUtil.getString( info, "ANUC_ITEM_4" );
		String ANUC_ITEM_5 = EgovWebUtil.getString( info, "ANUC_ITEM_5" );
		String ANUC_ITEM_6 = EgovWebUtil.getString( info, "ANUC_ITEM_6" );
		String ANUC_ITEM_7 = EgovWebUtil.getString( info, "ANUC_ITEM_7" );
		String ANUC_ITEM_8 = EgovWebUtil.getString( info, "ANUC_ITEM_8" );

		// 넘긴 파라미터
		String TXT_ANUC_ITEM_1 = request.getParameter( "ANUC_ITEM_1" );
		String TXT_ANUC_ITEM_2 = request.getParameter( "ANUC_ITEM_2" );
		String TXT_ANUC_ITEM_3 = request.getParameter( "ANUC_ITEM_3" );
		String TXT_ANUC_ITEM_4 = request.getParameter( "ANUC_ITEM_4" );
		String TXT_ANUC_ITEM_5 = request.getParameter( "ANUC_ITEM_5" );
		String TXT_ANUC_ITEM_6 = request.getParameter( "ANUC_ITEM_6" );
		String TXT_ANUC_ITEM_7 = request.getParameter( "ANUC_ITEM_7" );
		String TXT_ANUC_ITEM_8 = request.getParameter( "ANUC_ITEM_8" );

		int supl_count = 0;
		ArrayList<Map<String, Object>> suplList = new ArrayList<Map<String, Object>>();

		if( !ANUC_ITEM_1.equals( TXT_ANUC_ITEM_1 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_1 );
			param.put( "post_item", TXT_ANUC_ITEM_1 );
			param.put( "supl_item_cd", "10" );
			param.put( "supl_item", "1. 저작물의 제호" );
			suplList.add( param );
			supl_count++;
		}
		if( !ANUC_ITEM_2.equals( TXT_ANUC_ITEM_2 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_2 );
			param.put( "post_item", TXT_ANUC_ITEM_2 );
			param.put( "supl_item_cd", "20" );
			param.put( "supl_item", "2. 저작자 및 저작재산권자의 성명" );
			suplList.add( param );
			supl_count++;
		}
		if( !ANUC_ITEM_3.equals( TXT_ANUC_ITEM_3 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_3 );
			param.put( "post_item", TXT_ANUC_ITEM_3 );
			param.put( "supl_item_cd", "30" );
			param.put( "supl_item", "3. 저작물 이용의 내용" );
			suplList.add( param );
			supl_count++;
		}
		if( !ANUC_ITEM_4.equals( TXT_ANUC_ITEM_4 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_4 );
			param.put( "post_item", TXT_ANUC_ITEM_4 );
			param.put( "supl_item_cd", "40" );
			param.put( "supl_item", "4. 공탁금액" );
			suplList.add( param );
			supl_count++;
		}
		if( !ANUC_ITEM_5.equals( TXT_ANUC_ITEM_5 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_5 );
			param.put( "post_item", TXT_ANUC_ITEM_5 );
			param.put( "supl_item_cd", "50" );
			param.put( "supl_item", "5. 공탁소의 명칭 및 소재지" );
			suplList.add( param );
			supl_count++;
		}
		if( !ANUC_ITEM_6.equals( TXT_ANUC_ITEM_6 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_6 );
			param.put( "post_item", TXT_ANUC_ITEM_6 );
			param.put( "supl_item_cd", "60" );
			param.put( "supl_item", "6. 공탁근거" );
			suplList.add( param );
			supl_count++;
		}
		if( !ANUC_ITEM_7.equals( TXT_ANUC_ITEM_7 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_7 );
			param.put( "post_item", TXT_ANUC_ITEM_7 );
			param.put( "supl_item_cd", "71" );
			param.put( "supl_item", "7. 저작물이용자의 주소" );
			suplList.add( param );
			supl_count++;
		}
		if( !ANUC_ITEM_8.equals( TXT_ANUC_ITEM_8 ) ){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "pre_item", ANUC_ITEM_8 );
			param.put( "post_item", TXT_ANUC_ITEM_8 );
			param.put( "supl_item_cd", "72" );
			param.put( "supl_item", "8. 저작물이용자의 성명" );
			suplList.add( param );
			supl_count++;
		}
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		if( suplList.size() != 0 ){
			commandMap.put( "suplList", suplList );
		}

		boolean isSuccess = fdcrAd10Service.fdcrAd10SuplRegi1( commandMap );
		if( isSuccess ){
			return returnUrl(
				model,
				"수정했습니다.",
				"/console/legal/fdcrAd10View1.page?BORD_CD=" + EgovWebUtil.getString( commandMap, "BORD_CD" )
					+ "&BORD_SEQN=" + EgovWebUtil.getString( commandMap, "BORD_SEQN" ) );
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/legal/fdcrAd10View1.page?BORD_CD=" + EgovWebUtil.getString( commandMap, "BORD_CD" )
					+ "&BORD_SEQN=" + EgovWebUtil.getString( commandMap, "BORD_SEQN" ) );
		}
	}

	/**
	 * 보상금 공탁공고 게시판 보완내역 메일 발신함 조회
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10SuplSendList1.page" )
	public String fdcrAd10SuplSendList1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String MSG_STOR_CD = EgovWebUtil.getString( commandMap, "MSG_STOR_CD" );
		if( "".equals( MSG_STOR_CD ) ){
			commandMap.put( "MSG_STOR_CD", "5" );
		}

		String KEYWORD = EgovWebUtil.getString( commandMap, "KEYWORD" );
		if( !"".equals( KEYWORD ) ){
			KEYWORD = URLDecoder.decode( KEYWORD, "UTF-8" );
			commandMap.put( "KEYWORD", KEYWORD );
		}

		fdcrAd10Service.fdcrAd10SuplSendList1( commandMap );

		List ds_list = (List) commandMap.get( "ds_list" );
		model.addAttribute( "ds_list", ds_list );

		System.out.println( "보상금 공탁공고 게시판 보완내역 메일 발신함 조회" );
		return "legal/fdcrAd10SuplSendList1.popup";
	}

	/**
	 * 보상금 공탁공고 게시판 공고정보 보완내역 팝업
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd10SuplListPop1.page" )
	public String fdcrAd10SuplListPop1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String MSG_STOR_CD = EgovWebUtil.getString( commandMap, "MSG_STOR_CD" );
		if( "".equals( MSG_STOR_CD ) ){
			commandMap.put( "MSG_STOR_CD", "5" );
		}

		fdcrAd10Service.fdcrAd10SuplListPop1( commandMap );

		List ds_list = (List) commandMap.get( "ds_list" );
		model.addAttribute( "ds_list", ds_list );

		System.out.println( "보상금 공탁공고 게시판 공고정보 보완내역 팝업" );
		return "legal/fdcrAd10SuplListPop1.popup";
	}

}
