package kr.or.copyright.mls.mobile.model;

public class Book{
	private int
		crId
	;
	
	private String
		title
		, bookTitle
		, subTitle
		, publisher
		, firstEditionYear
		, writer
		, trustYnStr
		, krtraTrustYnStr
	;

	public int getCrId() {
		return crId;
	}

	public void setCrId(int crId) {
		this.crId = crId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getFirstEditionYear() {
		return firstEditionYear;
	}

	public void setFirstEditionYear(String firstEditionYear) {
		this.firstEditionYear = firstEditionYear;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getTrustYnStr() {
		return trustYnStr;
	}

	public void setTrustYnStr(String trustYnStr) {
		this.trustYnStr = trustYnStr;
	}

	public String getKrtraTrustYnStr() {
		return krtraTrustYnStr;
	}

	public void setKrtraTrustYnStr(String krtraTrustYnStr) {
		this.krtraTrustYnStr = krtraTrustYnStr;
	}
	
	
}
