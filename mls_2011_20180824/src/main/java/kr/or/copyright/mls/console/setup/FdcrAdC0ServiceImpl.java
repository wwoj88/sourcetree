package kr.or.copyright.mls.console.setup;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd0301Dao;
import kr.or.copyright.mls.console.setup.inter.FdcrAdC0Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdC0Service" )
public class FdcrAdC0ServiceImpl extends CommandService implements FdcrAdC0Service{

	// OldDao adminEvnSetDao
	@Resource( name = "fdcrAd0301Dao" )
	private FdcrAd0301Dao fdcrAd0301Dao;

	/**
	 * 상당한노력 자동화 설정 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdC0List1( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd0301Dao.statProcSetList( commandMap );
		commandMap.put( "ds_list_stat", list );
	}

	/**
	 * 상당한노력 자동화 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdC0Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd0301Dao.statProcSetUpdateYn( commandMap );
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

}
