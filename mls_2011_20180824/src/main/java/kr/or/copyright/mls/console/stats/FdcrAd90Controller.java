package kr.or.copyright.mls.console.stats;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Service;
import kr.or.copyright.mls.console.stats.inter.FdcrAd90Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 통계관리 > 기간별접속통계
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd90Controller extends DefaultController{

	@Resource( name = "fdcrAd90Service" )
	private FdcrAd90Service fdcrAd90Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd90Controller.class );

	/**
	 * 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd90List1.page" )
	public String fdcrAd90List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd90List1 Start" );
		// 파라미터 셋팅
		String GUBUN = EgovWebUtil.getString( commandMap, "GUBUN" );
		String FROM = EgovWebUtil.getString( commandMap, "FROM" );
		String TO = EgovWebUtil.getString( commandMap, "TO" );
		if(!"".equals(GUBUN) && !"".equals(FROM) && !"".equals(TO)){
			fdcrAd90Service.fdcrAd90List1( commandMap );	
		}
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "TOTAL", commandMap.get( "TOTAL" ) );
		logger.debug( "fdcrAd90List1 End" );
		return "/stats/fdcrAd90List1.tiles";
	}
	
	/**
	 * 월별 접속자수 목록
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd90List2.page" )
	public String fdcrAd90List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd90List2 Start" );
		// 파라미터 셋팅
		String GUBUN = EgovWebUtil.getString( commandMap, "GUBUN" );
		String FROM = EgovWebUtil.getString( commandMap, "FROM" );
		String TO = EgovWebUtil.getString( commandMap, "TO" );
		String YEARMONTH = EgovWebUtil.getString( commandMap, "YEARMONTH" );
		if(!"".equals(GUBUN) && !"".equals(FROM) && !"".equals(TO) && !"".equals(YEARMONTH)){
			fdcrAd90Service.fdcrAd90List2( commandMap );	
		}
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "TOTAL", commandMap.get( "TOTAL" ) );
		logger.debug( "fdcrAd90List2 End" );
		return "/stats/fdcrAd90List2.popup";
	}
	
	/**
	 * 기간별 접속 통계 엑셀 다운로드
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd90Down1.page" )
	public String fdcrAd90Down1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd90Down1 Start" );
		// 파라미터 셋팅
		String GUBUN = EgovWebUtil.getString( commandMap, "GUBUN" );
		String FROM = EgovWebUtil.getString( commandMap, "FROM" );
		String TO = EgovWebUtil.getString( commandMap, "TO" );
		if(!"".equals(GUBUN) && !"".equals(FROM) && !"".equals(TO)){
			fdcrAd90Service.fdcrAd90List1( commandMap );	
		}
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "TOTAL", commandMap.get( "TOTAL" ) );
		model.addAttribute( "ExcelDown", "Y" );
		logger.debug( "fdcrAd90Down1 End" );
		return "/stats/fdcrAd90Down1";
	}
}
