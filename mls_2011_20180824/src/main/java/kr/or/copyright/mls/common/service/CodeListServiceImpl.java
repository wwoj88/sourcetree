package kr.or.copyright.mls.common.service;

import java.util.List;

import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.common.dao.CodeListDao;
import kr.or.copyright.mls.common.model.CodeList;

public class CodeListServiceImpl extends BaseService implements CodeListService{

	private CodeListDao codeListDao;
	
	public void setCodeListDao(CodeListDao codeListDao){
		this.codeListDao = codeListDao;
	}
	
	public List<CodeList> commonCodeList(CodeList codeList){
		return codeListDao.commonCodeList(codeList);
	}
	
}
