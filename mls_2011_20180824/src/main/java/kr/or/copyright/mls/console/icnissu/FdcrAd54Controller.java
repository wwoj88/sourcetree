package kr.or.copyright.mls.console.icnissu;

import java.net.URLDecoder;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ICN 발급관리 > 도서
 * 
 * @author ljh
 */
@Controller
public class FdcrAd54Controller extends DefaultController{

	@Resource( name = "fdcrAd54Service" )
	private FdcrAd54ServiceImpl fdcrAd54Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd54Controller.class );

	/**
	 * 도서저작물 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/icnissu/fdcrAd54List1.page" )
	public String fdcrAd54List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "TRST_ORGN_CODE", "" );
		String srch_icnxStatCode = EgovWebUtil.getString( commandMap, "srch_icnxStatCode" );
		String srch_icnxNumb = EgovWebUtil.getString( commandMap, "srch_icnxNumb" );
		String srch_title = EgovWebUtil.getString( commandMap, "srch_title" );
		String srch_AlbumTitle = EgovWebUtil.getString( commandMap, "srch_AlbumTitle" );
		String srch_lyricist = EgovWebUtil.getString( commandMap, "srch_lyricist" );
		String srch_composer = EgovWebUtil.getString( commandMap, "srch_composer" );
		String srch_singer = EgovWebUtil.getString( commandMap, "srch_singer" );
		
		if( !"".equals( srch_icnxStatCode ) ){
			srch_icnxStatCode = URLDecoder.decode( srch_icnxStatCode, "UTF-8" );
			commandMap.put( "srch_icnxStatCode", srch_icnxStatCode );
		}
		if( !"".equals( srch_icnxNumb ) ){
			srch_icnxNumb = URLDecoder.decode( srch_icnxNumb, "UTF-8" );
			commandMap.put( "srch_icnxNumb", srch_icnxNumb );
		}
		if( !"".equals( srch_title ) ){
			srch_title = URLDecoder.decode( srch_title, "UTF-8" );
			commandMap.put( "srch_title", srch_title );
		}
		if( !"".equals( srch_AlbumTitle ) ){
			srch_AlbumTitle = URLDecoder.decode( srch_AlbumTitle, "UTF-8" );
			commandMap.put( "srch_AlbumTitle", srch_AlbumTitle );
		}
		if( !"".equals( srch_lyricist ) ){
			srch_lyricist = URLDecoder.decode( srch_lyricist, "UTF-8" );
			commandMap.put( "srch_lyricist", srch_lyricist );
		}
		if( !"".equals( srch_composer ) ){
			srch_composer = URLDecoder.decode( srch_composer, "UTF-8" );
			commandMap.put( "srch_composer", srch_composer );
		}
		if( !"".equals( srch_singer ) ){
			srch_singer = URLDecoder.decode( srch_singer, "UTF-8" );
			commandMap.put( "srch_singer", srch_singer );
		}

		fdcrAd54Service.fdcrAd54List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "도서저작물 목록 조회" );
		return "icnissu/fdcrAd54List1.tiles";
	}

	/**
	 * 도서저작물 상세
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/icnissu/fdcrAd54View1.page" )
	public String fdcrAd54View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "PRPS_MAST_KEY", "" );
		commandMap.put( "PRPS_IDNT_CODE", "" );
		commandMap.put( "CR_ID", "" );
		commandMap.put( "ALBUM_ID", "" );
		commandMap.put( "NR_ID", "" );
		commandMap.put( "PRPS_IDNT", "" ); // 없어도될듯?
		commandMap.put( "PRPS_IDNT_NR", "" ); // 없어도될듯?
		commandMap.put( "PRPS_IDNT_ME", "" ); // 없어도될듯?

		fdcrAd54Service.fdcrAd54View1( commandMap );
		model.addAttribute( "ds_music_detail", commandMap.get( "ds_music_detail" ) );
		System.out.println( "도서저작물 상세" );
		return "test";
	}

	/**
	 * 도서저작물 CLMS 전송 업데이트
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/icnissu/fdcrAd54Update1.page" )
	public void fdcrAd54Update1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "ICNX_STAT_CODE", "" );
		commandMap.put( "ICNX_NUMB", "" );
		commandMap.put( "LICENSOR_ID", "" );
		commandMap.put( "PRPS_IDNT_CODE", "" );
		commandMap.put( "LICENSOR", "" );
		commandMap.put( "TRANSLATOR_ID", "" );
		commandMap.put( "TRANSLATOR", "" );
		commandMap.put( "PRPS_IDNT", "" );
		commandMap.put( "PRPS_IDNT_ME", "" );
		commandMap.put( "PRPS_IDNT_NR", "" );
		String[] PRPS_MAST_KEYS = request.getParameterValues( "PRPS_MAST_KEY" );
		commandMap.put( "PRPS_MAST_KEY", PRPS_MAST_KEYS );

		boolean isSuccess = fdcrAd54Service.fdcrAd54Update1( commandMap );
		returnAjaxString( response, isSuccess );
	}

}
