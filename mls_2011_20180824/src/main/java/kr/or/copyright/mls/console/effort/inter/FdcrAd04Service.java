package kr.or.copyright.mls.console.effort.inter;

import java.util.Map;

public interface FdcrAd04Service{

	/**
	 * 이의 신청 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd04List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한노력 상세
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd04UpdateForm1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 대상 저작물 - 상세
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd04UpdateForm2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 처리상태 수정(신청,보완,신청승인,반려)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd04Update1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁 공고 게시판 - 공고승인
	 * 
	 * @throws Exception
	 */
	public boolean fdcrAd04Update2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd04Update3( Map<String, Object> commandMap ) throws Exception;
}
