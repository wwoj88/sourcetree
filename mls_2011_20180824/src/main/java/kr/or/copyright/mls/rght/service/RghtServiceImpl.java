package kr.or.copyright.mls.rght.service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.rght.dao.RghtDao;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;

import com.tobesoft.platform.data.Dataset;

public class RghtServiceImpl extends BaseService implements RghtService {

	//private Log logger = LogFactory.getLog(getClass());

	private RghtDao rghtDao;
	
	public void setRghtDao(RghtDao rghtDao){
		this.rghtDao = rghtDao; 
	}
  
	
	
	// 내권리찾기 음악리스트 조회 
	public void muscRghtList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     

		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) rghtDao.muscRghtList(map);		
		List pageCount = (List) rghtDao.muscRghtListCount(map); 
	
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		addList("ds_page", pageCount);	

	}

	
	
	
	// 내권리찾기(음악상세조회)	 
	public void muscRghtDetail() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) rghtDao.muscRghtDetail(map);		
			
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);

	}	
	
	
	
	
	// 내권리찾기(어문  조회)
	public void bookRghtList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) rghtDao.bookRghtList(map);		
		List pageCount = (List) rghtDao.bookRghtListCount(map); 
	
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		addList("ds_page", pageCount);	

	}	
	
	
	
	// 내권리찾기(어문상세조회)	 
	public void bookRghtDetail() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) rghtDao.bookRghtDetail(map);		
			
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);

	}	
	
	
	
	
	// 내권리찾기(이미지 조회)
	public void imgeRghtList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) rghtDao.imgeRghtList(map);		
		List pageCount = (List) rghtDao.imgeRghtListCount(map); 
	
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		addList("ds_page", pageCount);	

	}	
	
	
	// 내권리찾기(저작권자 조회) 음악
	public void muscList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List List  = (List) rghtDao.muscList(map);		
		List pageCount = (List) rghtDao.muscListCount(map); 
		
		//hmap를 dataset로 변환한다.
		addList("ds_list",  List);	
		addList("ds_page", pageCount);	
		
	}	
	
	

	// 내권리찾기(저작권자 조회) 음악
	public void bookList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) rghtDao.bookList(map);		
		List pageCount = (List) rghtDao.bookListCount(map); 
		
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		addList("ds_page", pageCount);			


	}		

	
	
	// 내권리찾기 상세
	public void rghtDetlList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) rghtDao.rghtDetlList(map);		
		//List totDealStat = (List)rghtDao.rghtTotalDealStat(map);	// 전체처리상태 값
		List tempList2	= (List)rghtDao.rghtTempList(map);				//  저작물리스트 저작물명
		List fileList		 = (List)rghtDao.rghtFileList(map);				// ML_PRPS_ATTC
		
		map.put("ListGubun", "Y");
		List tempList	= (List)rghtDao.rghtTempList(map);				//  저작물리스트 신탁기관별 저작물명
		
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		//addList("ds_totDealStat"		, totDealStat);
		addList("ds_temp2", tempList2);
		addList("ds_temp", tempList);
		addList("ds_fileList", fileList);

	}	
	
	
	
	
	// 내권리찾기 신청
	public void rghtSave() throws Exception {

		Dataset ds_List = getDataset("ds_list");   
		Dataset ds_temp = getDataset("ds_temp");   
		Dataset ds_fileList 		= getDataset("ds_fileList");							// 첨부파일 리스트
		Dataset ds_userInfo	= getDataset("gds_userInfo");						// 로그인정보
		
//ds_fileList.printDataset();
		
		System.out.println("count ::::"+ds_temp.getRowCount());
		
		Map map = getMap(ds_List, 0 );
		
		int prpsMastKey = rghtDao.prpsMastKey();									// MAX(ML_PRPS.PRPS_SEQN)+1 set
		
		map.put("PRPS_MAST_KEY", prpsMastKey);
		
		if( ds_fileList.getRowCount()>0 ) 
			map.put("ATTC_YSNO", "Y");
		else 
			map.put("ATTC_YSNO", "N");
		
		//내권리찾기 신청 master 입력 
		rghtDao.prpsMasterInsert(map);
		
		int i201 = 0; int i202 = 0; int i203 = 0; int i204 = 0; int i205 = 0;
		
		for( int i=0;i<ds_temp.getRowCount();i++ )
		{
			ds_temp.printDataset();			
			
			// temp의 row갯수만큼 넣는다.
			ds_List.setColumn(0, "PRPS_IDNT", ds_temp.getColumn(i, "CR_ID"));
			ds_List.setColumn(0, "ICN_NUMB", ds_temp.getColumn(i, "ICN_NUMB"));
			
			map = getMap(ds_List, 0 );
			map.put("PRPS_MAST_KEY", prpsMastKey);

//ds_List.printDataset();
			
			String trstOrgnCode  = "";
			
			if( ds_List.getColumn(0, "CHK_201").toString().equals("1"))
			{
				i201++;
				trstOrgnCode = "201";
				
				rghtDao.prpsDetail201Insert(map); 
			}

			if(ds_List.getColumn(0, "CHK_202").toString().equals("1"))
			{
				i202++;
				trstOrgnCode = "202";
				
				rghtDao.prpsDetail202Insert(map);
			}
			
			if(ds_List.getColumn(0, "CHK_203").toString().equals("1"))
			{
				i203++;
				trstOrgnCode = "203";
				
				rghtDao.prpsDetail203Insert(map);
			}
			
			if(ds_List.getColumn(0, "CHK_204").toString().equals("1"))
			{
				i204++;
				trstOrgnCode = "204";
				
				rghtDao.prpsDetail204Insert(map);
			}			
			
			if(ds_List.getColumn(0, "CHK_205").toString().equals("1"))
			{
				i205++;
				trstOrgnCode = "205";
				
				rghtDao.prpsDetail205Insert(map);
			}		
			
			
			
			// 첨부파일
			if( (ds_List.getColumn(0, "CHK_201").toString().equals("1") && i201==1)
				||	(ds_List.getColumn(0, "CHK_202").toString().equals("1") && i202==1)
				||	(ds_List.getColumn(0, "CHK_203").toString().equals("1") && i203==1)
				||	(ds_List.getColumn(0, "CHK_204").toString().equals("1") && i204==1)
				||	(ds_List.getColumn(0, "CHK_205").toString().equals("1") && i205==1)
				)
			{
				for( int fi = 0; fi<ds_fileList.getRowCount() && ds_fileList != null ; fi++)
				{
	//System.out.println("======================================>> "+fi+"::"+ds_fileList.getRowStatus(fi));				
					
					Map fileMap = getMap(ds_fileList, fi);
					
					
					//if( ds_fileList.getRowStatus(fi).equals("insert") == true ) 
					//{
						
	//System.out.println("======================================>> "+fileMap.toString());									
					
						byte[] file = ds_fileList.getColumn(fi, "CLIENT_FILE").getBinary();
						
						String fileName = ds_fileList.getColumnAsString(fi, "REAL_FILE_NAME");
						
						Map upload = FileUtil.uploadMiFile(fileName, file);
						
						fileMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
						fileMap.put("FILE_PATH", upload.get("FILE_PATH"));
						//fileMap.put("PRPS_SEQN", prpsSeqnMax);
						fileMap.put("PRPS_DIVS", ds_List.getColumn(0, "PRPS_DIVS"));
						fileMap.put("PRPS_IDNT", ds_List.getColumn(0, "PRPS_IDNT"));
						fileMap.put("USER_IDNT", ds_List.getColumn(0, "USER_IDNT"));
						
						fileMap.put("TRST_ORGN_CODE", trstOrgnCode);
						fileMap.put("PRPS_MAST_KEY", prpsMastKey);
						
						rghtDao.rghtFileInsert(fileMap);
						
					//}
				}// end for.. 첨부파일
			}
		}
		
		// 메일, SMS		
		
		Map infoMap = getMap(ds_userInfo, 0);
	
		//----------------------------- mail send------------------------------//
		
	    String host = Constants.MAIL_SERVER_IP;							// smtp서버
	    String to     = infoMap.get("U_MAIL")+"";					// 수신EMAIL
	    String toName = map.get("USER_NAME").toString();			// 수신인
	    String subject = "[저작권찾기] 내권리찾기신청 완료";
	    String idntName = getMap(ds_temp, 0).get("TITLE")+"";		// 신청대상명
	    
	    // 이미지의 경우 컬럼명이 다르다.
	    if(idntName.equals("null") || idntName.equals("NULL") || idntName.equals(""))
	    	idntName = getMap(ds_temp, 0).get("WORK_NAME")+"";		// 신청대상명WORK_NAME
	    
	    if( ds_temp.getRowCount() > 0)
	    	idntName  += " 외 "+ ( ds_temp.getRowCount()-1 )+ "건";
	      
	    String from = Constants.SYSTEM_MAIL_ADDRESS;  //발신인 주소 - 시스템 대표 메일
	    String fromName = Constants.SYSTEM_NAME;
	      
	    if( infoMap.get("U_MAIL_RECE_YSNO").equals("Y") && to != null && to != "")  {
		
	    	//------------------------- mail 정보 ----------------------------------//
		    MailInfo info = new MailInfo();
		    info.setFrom(from);
		    info.setFromName(fromName);
		    info.setHost(host);
		    info.setEmail(to);
		    info.setEmailName(toName);
		    info.setSubject(subject);
		      
		    StringBuffer sb = new StringBuffer();   
		    
		    
		    
		    sb.append("<div class=\"mail_title\">"+toName+"님, 내권리찾기신청이  완료되었습니다. </div>");
		    sb.append("<div class=\"mail_contents\"> ");
		    sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
		    sb.append("		<tr> ");
		    sb.append("			<td> ");
		    sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
// 변경부분
		    sb.append("					<tr> ");
		    sb.append("						<td class=\"tdLabel\">신청 저작물명</td> ");
		    sb.append("						<td class=\"tdData\">"+ idntName+"</td> ");
		    sb.append("					</tr> ");
// 변경부분
//		    sb.append("					<tr> ");
//		    sb.append("						<td class=\"tdLabel\">이메일 주소</td> ");
//		    sb.append("						<td class=\"tdData\"><a href=\"mailto:"+Constants.SYSTEM_MAIL_ADDRESS+"\" onFocus='this.blur()'>"+Constants.SYSTEM_MAIL_ADDRESS+"</a></td> ");
//		    sb.append("					</tr> ");
//		    sb.append("					<tr> ");
//		    sb.append("						<td class=\"tdLabel\">전화번호</td> ");
//		    sb.append("						<td class=\"tdData\">"+Constants.SYSTEM_TELEPHONE+"</td> ");
//		    sb.append("					</tr> ");
		    sb.append("				</table> ");
		    sb.append("			</td> ");
		    sb.append("		</tr> ");
		    sb.append("	</table> ");
		    sb.append("</div> ");

		    info.setMessage(new String(MailMessage.getChangeInfoMailText(sb.toString())));
    	    MailManager manager = MailManager.getInstance();
	    	
    	    info = manager.sendMail(info);
	    	if(info.isSuccess()){
	    	  System.out.println(">>>>>>>>>>>> message success :"+info.getEmail() );
	    	}else{
	    	  System.out.println(">>>>>>>>>>>> message false   :"+info.getEmail() );
	    	}
	    }
	      
	    String smsTo = infoMap.get("U_MOBL_PHON")+"";
	    String smsFrom = Constants.SYSTEM_TELEPHONE;
	    
	    String smsMessage = "[저작권찾기] "+ idntName+" - 저작권찾기신청이  완료되었습니다.";
	    
	    if( infoMap.get("U_SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {
		
	    	SMSManager smsManager = new SMSManager();
		  	
		  	System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
	    }	
	    
	// end email, sms
		
        // 강명표 추가 START
		Map connMap = getMap(ds_List, 0 );
		connMap.put("CONN_URL", "저작권찾기신청 등록");
		rghtDao.insertConnInfo(connMap);
		// 강명표 추가 END
	}	
	
	
	
	// 내권리찾기 수정
	public void rghtModi() throws Exception {

		Dataset ds_List = getDataset("ds_list");   
		Dataset ds_temp = getDataset("ds_temp2");   
		Dataset ds_fileList		= getDataset("ds_fileList");							// 첨부파일 리스트
		
		Map map = getMap(ds_List, 0 );
		
		// 수정처리 전에 현재 최대상태값을 확인
		String maxDealStat = "";
		List maxDealStatList = rghtDao.getMaxDealStat(map);
		
		maxDealStat = (String)((HashMap)maxDealStatList.get(0)).get("MAX_DEAL_STAT");
		
System.out.println("==========================================");		
System.out.println("maxDealStat::::"+maxDealStat);
System.out.println("==========================================");		

		// 수정처리 - 최대상태값이 1 신청인 경우
		if(maxDealStat.equals("1")) {
			
			String prpsMastKey = map.get("PRPS_MAST_KEY").toString();
			
			map.put("PRPS_MAST_KEY", prpsMastKey);
			
			if( ds_fileList.getRowCount()>0 ) 
				map.put("ATTC_YSNO", "Y");
			else 
				map.put("ATTC_YSNO", "N");
			
			//내권리찾기 신청 master 수정 
			rghtDao.prpsMasterUpdate(map);
		
			//내권리찾기 신청 DETAIL 삭제후 입력 		
			rghtDao.prpsDetailDelete(map);
			
			int i201 = 0; int i202 = 0; int i203 = 0; int i204 = 0; int i205 = 0;
			int iFiCnt = 0;
			
			for( int i=0;i<ds_temp.getRowCount();i++ )
			{
	
				// temp의 row갯수만큼 넣는다.
				ds_List.setColumn(0, "PRPS_IDNT", ds_temp.getColumn(i, "PRPS_IDNT"));
				ds_List.setColumn(0, "ICN_NUMB", ds_temp.getColumn(i, "ICN_NUMB"));
				
				Map tempMap = getMap(ds_temp, i);
				
				map = getMap(ds_List, 0 );
				map.put("PRPS_MAST_KEY", prpsMastKey);
				map.put("PRPS_IDNT", tempMap.get("PRPS_IDNT"));
				
				String trstOrgnCode  = "";
					
				if( ds_List.getColumn(0, "CHK_201").toString().equals("1"))
				{
					i201++;
					trstOrgnCode = "201";
					rghtDao.prpsDetail201Insert(map);
				}
		
				if(ds_List.getColumn(0, "CHK_202").toString().equals("1"))
				{
					i202++;
					trstOrgnCode = "202";
					rghtDao.prpsDetail202Insert(map);
				}
				
				if(ds_List.getColumn(0, "CHK_203").toString().equals("1"))
				{
					i203++;
					trstOrgnCode = "203";
					rghtDao.prpsDetail203Insert(map);
				}
				
				if(ds_List.getColumn(0, "CHK_204").toString().equals("1"))
				{
					i204++;
					trstOrgnCode = "204";
					rghtDao.prpsDetail204Insert(map);
				}			
				
				if(ds_List.getColumn(0, "CHK_205").toString().equals("1"))
				{
					i205++;
					trstOrgnCode = "205";
					rghtDao.prpsDetail205Insert(map);
				}			
				
				
				// 첨부파일삭제
				for( int fi = 0; iFiCnt == 0 && fi<ds_fileList.getDeleteRowCount(); fi++ )
				{
					Map deleteMap = getDeleteMap(ds_fileList, fi);
					rghtDao.rghtFileDelete(deleteMap);
					
					String fileName = ds_fileList.getDeleteColumn(fi, "REAL_FILE_NAME").toString();
					
					 try 
					 {
						 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
					     if (file.exists()) 
					     { 
					    	 file.delete(); 
					     }
					  } 
					  catch (Exception e) 
					  {
					   e.printStackTrace();
					  } 
				}
				
				iFiCnt++;
				
				// 첨부파일등록
				if( (ds_List.getColumn(0, "CHK_201").toString().equals("1") && i201==1)
						||	(ds_List.getColumn(0, "CHK_202").toString().equals("1") && i202==1)
						||	(ds_List.getColumn(0, "CHK_203").toString().equals("1") && i203==1)
						||	(ds_List.getColumn(0, "CHK_204").toString().equals("1") && i204==1)
						||	(ds_List.getColumn(0, "CHK_205").toString().equals("1") && i205==1)
						)
				{
					for( int fi=0; fi<ds_fileList.getRowCount() && ds_fileList != null; fi++ )
					{
						Map fileMap = getMap(ds_fileList, fi);
						
					//	if( ds_fileList.getRowStatus(fi).equals("insert") == true ) {
						
						if( fileMap.get("FILE_PATH")== null || fileMap.get("FILE_PATH").toString().length()==0 ) {
								
							byte[] file = ds_fileList.getColumn(fi, "CLIENT_FILE").getBinary();
							
							String fileName = ds_fileList.getColumnAsString(fi, "REAL_FILE_NAME");
							
							Map upload = FileUtil.uploadMiFile(fileName, file);
							
							//fileMap.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
							fileMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
							fileMap.put("FILE_PATH", upload.get("FILE_PATH"));
					
						} else {
							rghtDao.rghtFileDelete(fileMap);
						}
							fileMap.put("PRPS_DIVS", ds_List.getColumn(0, "PRPS_DIVS"));
							fileMap.put("PRPS_IDNT", ds_List.getColumn(0, "PRPS_IDNT"));
							fileMap.put("USER_IDNT", ds_List.getColumn(0, "USER_IDNT"));
							
							fileMap.put("TRST_ORGN_CODE", trstOrgnCode);
							fileMap.put("PRPS_MAST_KEY", prpsMastKey);
							
							rghtDao.rghtFileInsert(fileMap);
					// }
					}
					
				}
				
			}
			
	        // 강명표 추가 START
			map.put("CONN_URL", "내권리찾기신청 수정");
			rghtDao.insertConnInfo(map);
			// 강명표 추가 END
		
		}	// end if.. 수정처리
		
		addList("ds_maxDealStat"			, maxDealStatList);
	}		
	
	// 내권리찾기 삭제
	public void rghtDelete() throws Exception {

		Dataset ds_List = getDataset("ds_list");   
		Dataset ds_fileList = getDataset("ds_fileList");				// 첨부파일
		
		Map map = getMap(ds_List, 0 );
		
		// 삭제처리 전에 현재 최대상태값을 확인
		String maxDealStat = "";
		List maxDealStatList = rghtDao.getMaxDealStat(map);
		
		maxDealStat = (String)((HashMap)maxDealStatList.get(0)).get("MAX_DEAL_STAT");
		
System.out.println("==========================================");		
System.out.println("maxDealStat::::"+maxDealStat);
System.out.println("==========================================");		

		// 수정처리 - 최대상태값이 1 신청인 경우
		if(maxDealStat.equals("1")) {
		
			//내권리찾기 신청 master 삭제 
			rghtDao.prpsMasterDelete(map);
		
			//내권리찾기 신청 DETAIL 삭제 		
			rghtDao.prpsDetailDelete(map);
			
			
			// 첨부파일삭제
			for( int fi = 0; fi<ds_fileList.getDeleteRowCount(); fi++ )
			{
				Map deleteMap = getDeleteMap(ds_fileList, fi);
				rghtDao.rghtFileDelete(deleteMap);
				
				String fileName = ds_fileList.getDeleteColumn(fi, "REAL_FILE_NAME").toString();
				
				 try 
				 {
					 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
				     if (file.exists()) 
				     { 
				    	 file.delete(); 
				     }
				  } 
				  catch (Exception e) 
				  {
				   e.printStackTrace();
				  } 
			}
			
	        // 강명표 추가 START
			map.put("CONN_URL", "내권리찾기신청 삭제");
			rghtDao.insertConnInfo(map);
			// 강명표 추가 END
	
		}	// end if.. 삭제처리.
		
		addList("ds_maxDealStat"			, maxDealStatList);
	}	
	
	
	// 내권리찾기 처리결과조회
	public void rghtDetlDescList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) rghtDao.rghtDetlDescList(map);		
			
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);

	}		
}
