package kr.or.copyright.mls.adminStatProc.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface AdminStatProcPopupService {

	byte[] statProcTargPopupExelDown(Map<String, Object> map) throws Exception;

	byte[] statProcPopupExelDown(HashMap<String, Object> map) throws Exception;
	
	byte[] statProcPopupExelDown2(Map<String, Object> map) throws Exception;	

	byte[] statProcWorksPopupExelDown(Map<String, Object> map) throws Exception;
	
	byte[] statProcInfoExelDown(Map<String, Object> map) throws Exception;

}
