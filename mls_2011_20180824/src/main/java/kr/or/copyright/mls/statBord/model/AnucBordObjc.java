package kr.or.copyright.mls.statBord.model;

import java.util.List;

public class AnucBordObjc {
	private long bordCd;			//게시판구분
	private int bordSeqn;		//게시물순번
	private int statObjcId;		//이의제기 id
	private String rgstIdnt;	//등록자 id 
	private String rgstDttm;	//등록일자
	private int statObjcCd;		//처리상태
	private String chngDttm;	//처리일자
	private String statObjcMemo;//신청처리 비고
	private String objcDesc;	//내용
	private int attcSeqn;		//첨부파일순번
	private List fileList;
	private List shisList;		
	private int worksId;
	private String statObjcCdName;
	@Override
	public String toString() {
		return "AnucBordObjc [bordCd=" + bordCd + ", bordSeqn=" + bordSeqn
				+ ", statObjcId=" + statObjcId + ", rgstIdnt=" + rgstIdnt
				+ ", rgstDttm=" + rgstDttm + ", statObjcCd=" + statObjcCd
				+ ", chngDttm=" + chngDttm + ", statObjcMemo=" + statObjcMemo
				+ ", objcDesc=" + objcDesc + ", attcSeqn=" + attcSeqn
				+ ", fileList=" + fileList + ", shisList=" + shisList
				+ ", worksId=" + worksId + ", statObjcCdName=" + statObjcCdName
				+ "]";
	}
	public long getBordCd() {
		return bordCd;
	}
	public void setBordCd(long bordCd) {
		this.bordCd = bordCd;
	}
	public int getBordSeqn() {
		return bordSeqn;
	}
	public void setBordSeqn(int bordSeqn) {
		this.bordSeqn = bordSeqn;
	}
	public int getStatObjcId() {
		return statObjcId;
	}
	public void setStatObjcId(int statObjcId) {
		this.statObjcId = statObjcId;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	public int getStatObjcCd() {
		return statObjcCd;
	}
	public void setStatObjcCd(int statObjcCd) {
		this.statObjcCd = statObjcCd;
	}
	public String getChngDttm() {
		return chngDttm;
	}
	public void setChngDttm(String chngDttm) {
		this.chngDttm = chngDttm;
	}
	public String getStatObjcMemo() {
		return statObjcMemo;
	}
	public void setStatObjcMemo(String statObjcMemo) {
		this.statObjcMemo = statObjcMemo;
	}
	public String getObjcDesc() {
		return objcDesc;
	}
	public void setObjcDesc(String objcDesc) {
		this.objcDesc = objcDesc;
	}
	public int getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(int attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public List getFileList() {
		return fileList;
	}
	public void setFileList(List fileList) {
		this.fileList = fileList;
	}
	public List getShisList() {
		return shisList;
	}
	public void setShisList(List shisList) {
		this.shisList = shisList;
	}
	public int getWorksId() {
		return worksId;
	}
	public void setWorksId(int worksId) {
		this.worksId = worksId;
	}
	public String getStatObjcCdName() {
		return statObjcCdName;
	}
	public void setStatObjcCdName(String statObjcCdName) {
		this.statObjcCdName = statObjcCdName;
	}
}
