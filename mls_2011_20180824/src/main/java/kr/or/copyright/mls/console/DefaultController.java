package kr.or.copyright.mls.console;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.mls.support.constant.Constants;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class DefaultController {

     private static final Logger logger = LoggerFactory.getLogger(DefaultController.class);

     /**
      * ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq,savePath,extNames); <br>
      * 파일명 : F_fileName <br>
      * 파일확장자 : F_extName <br>
      * 파일사이즈 : F_filesize <br>
      * 원본파일명 : F_orgFileName <br>
      * 파일저장경로 : F_saveFilePath <br>
      * 
      * @param mreq
      * @param savePath
      * @param extNames
      * @return
      */
     protected ArrayList<Map<String, Object>> fileUpload(MultipartHttpServletRequest mreq, String savePath, String[] extNames) {

          return fileUpload(mreq, savePath, extNames, false);
     }

     /**
      * ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq,savePath,extNames); <br>
      * 파일명 : F_fileName <br>
      * 파일확장자 : F_extName <br>
      * 파일사이즈 : F_filesize <br>
      * 원본파일명 : F_orgFileName <br>
      * 파일저장경로 : F_saveFilePath <br>
      * 
      * @param mreq
      * @param savePath
      * @param extNames
      * @return
      */
     protected ArrayList<Map<String, Object>> fileUploadCivilServices(MultipartHttpServletRequest mreq, String savePath, boolean random) {


          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
          List<MultipartFile> fileList = null;

          Iterator<?> iter = mreq.getFileNames();
          while (iter.hasNext()) {
               String fileFieldName = (String) iter.next();
               fileList = mreq.getFiles(fileFieldName);
          }

          for (int i = 0; i < fileList.size(); i++) {
               MultipartFile file = fileList.get(i);
               if (!file.isEmpty() && file.getSize() > 0) {
                    String fieldName = file.getName();
                    String orgFileFullName = file.getOriginalFilename();
                    String extName = orgFileFullName.substring(orgFileFullName.lastIndexOf("."));
                    String extName1 = orgFileFullName.substring(orgFileFullName.lastIndexOf(".") + 1);
                    String fileName = EgovWebUtil.getDate("yyyyMMddHHmmssSS") + i;
                    String fileFullName = fileName + extName;
                    if (random) {
                         fileFullName = EgovWebUtil.uploadMiFile(orgFileFullName);
                    }

                    long sizeInBytes = file.getSize();
                    /* 확장자 체크 */
                    if (null == extName1 && "".equals(extName1))
                         return null;

                    logger.debug("파일 [fieldName] : " + fieldName + "<br/>");
                    logger.debug("파일 [orgFileFullName] : " + orgFileFullName + "<br/>");
                    logger.debug("파일 [fileFullName] : " + fileFullName + "<br/>");
                    logger.debug("파일 [extName] : " + extName + "<br/>");
                    logger.debug("파일 [sizeInBytes] : " + sizeInBytes + "<br/>");

                    try {
                         File dir = new File(savePath);
                         if (!dir.exists()) {
                              dir.mkdir();
                         }
                         File transFilePath = new File(savePath, fileFullName);
                         file.transferTo(transFilePath);

                    } catch (IOException ex) {
                         // ex.printStackTrace();
                         System.out.println("실행중IOException 에러발생 ");
                    }

                    /* 파일 정보 저장 */
                    Map<String, Object> fileMap = new HashMap<String, Object>();
                    fileMap.put("F_fileName", fileFullName);
                    fileMap.put("F_extName", extName);
                    fileMap.put("F_filesize", sizeInBytes);
                    fileMap.put("F_orgFileName", orgFileFullName);
                    fileMap.put("F_saveFilePath", savePath);
                    uploadList.add(fileMap);
               }
          }

          return uploadList;
     }

     /**
      * ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq,savePath,extNames); <br>
      * 파일명 : F_fileName <br>
      * 파일확장자 : F_extName <br>
      * 파일사이즈 : F_filesize <br>
      * 원본파일명 : F_orgFileName <br>
      * 파일저장경로 : F_saveFilePath <br>
      * 
      * @param mreq
      * @param savePath
      * @param extNames
      * @return
      */
     protected ArrayList<Map<String, Object>> fileUpload(MultipartHttpServletRequest mreq, String savePath, String[] extNames, boolean random) {

          System.out.println("defaultcontroller upload to date2: " + savePath);
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
          Iterator<?> iter = mreq.getFileNames();
          int i = 0;
          while (iter.hasNext()) {
               String fileFieldName = (String) iter.next();
               MultipartFile file = mreq.getFile(fileFieldName);
               if (!file.isEmpty() && file.getSize() > 0) {
                    String fieldName = file.getName();
                    String orgFileFullName = file.getOriginalFilename();
                    String extName = orgFileFullName.substring(orgFileFullName.lastIndexOf("."));
                    String extName1 = orgFileFullName.substring(orgFileFullName.lastIndexOf(".") + 1);
                    String fileName = EgovWebUtil.getDate("yyyyMMddHHmmssSS") + i;
                    String fileFullName = fileName + extName;
                    if (random) {
                         fileFullName = EgovWebUtil.uploadMiFile(orgFileFullName);
                    }

                    long sizeInBytes = file.getSize();
                    /* 확장자 체크 */
                    if (null != extName1 && !"".equals(extName1)) {
                         if (null != extNames && extNames.length > 0) {
                              System.out.println("defaultcontroller upload to date: " + savePath);
                              boolean isExtCheck = false;
                              for (String compareExt : extNames) {
                                   if ((compareExt.toUpperCase()).equals(extName1.toUpperCase())) {
                                        isExtCheck = true;
                                   }
                              }
                              if (!isExtCheck) {
                                   continue;
                              }
                         }
                    }
                    logger.debug("파일 [fieldName] : " + fieldName + "<br/>");
                    logger.debug("파일 [orgFileFullName] : " + orgFileFullName + "<br/>");
                    logger.debug("파일 [fileFullName] : " + fileFullName + "<br/>");
                    logger.debug("파일 [extName] : " + extName + "<br/>");
                    logger.debug("파일 [sizeInBytes] : " + sizeInBytes + "<br/>");

                    try {
                         File dir = new File(savePath);
                         if (!dir.exists()) {
                              dir.mkdir();
                         }
                         File transFilePath = new File(savePath, fileFullName);
                         file.transferTo(transFilePath);

                    } catch (IOException ex) {
                         // ex.printStackTrace();
                         System.out.println("실행중IOException 에러발생 ");
                    }

                    /* 파일 정보 저장 */
                    Map<String, Object> fileMap = new HashMap<String, Object>();
                    fileMap.put("F_fileName", fileFullName);
                    fileMap.put("F_extName", extName);
                    fileMap.put("F_filesize", sizeInBytes);
                    fileMap.put("F_orgFileName", orgFileFullName);
                    fileMap.put("F_saveFilePath", savePath);
                    uploadList.add(fileMap);
                    i++;
               }
          }

          return uploadList;
     }

     /**
      * 파일 다운로드
      * 
      * @param request
      * @param response
      * @throws Exception
      */
     protected void download(HttpServletRequest request, HttpServletResponse response, String path, String fileName) throws Exception {

          File file = new File(path);
          response.setContentType("applicaiton/download;charset=utf-8");
          response.setContentLength((int) file.length());
          String userAgent = request.getHeader("User-Agent");

          if (userAgent != null && userAgent.indexOf("MSIE 5.5") > -1) { // MS
               // IE
               // 5.5
               // 이하
               response.setHeader("Content-Disposition", "filename=" + URLEncoder.encode(fileName, "UTF-8") + ";");
          } else if (userAgent != null && userAgent.indexOf("MSIE") > -1 || userAgent.indexOf("rv:11.0") > 0) { // MS
               // IE
               // (보통은6.x
               // 이상
               // 가정)
               response.setHeader("Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(fileName, "UTF-8") + ";");
          } else { // 모질라나 오페라
                   // response.setHeader( "Content-Disposition","attachment; filename=" + new String(
                   // fileName.getBytes( "UTF-8" ), "latin1" ) + ";" );
               response.setHeader("Content-Disposition", "attachment; filename=\"" + new String(fileName.getBytes("UTF-8"), "latin1") + "\";"); // 데이터 인코딩이 바이너리 파일임을 명시
          }

          response.setHeader("Content-Transfer-Encoding", "binary");

          if (file.exists()) {
               int read = 0;
               byte[] b = new byte[1024 * 1024 * 2]; // 파일 크기

               BufferedInputStream fin = null;
               BufferedOutputStream outs = null;
               try {
                    fin = new BufferedInputStream(new FileInputStream(file));
                    outs = new BufferedOutputStream(response.getOutputStream());
                    while ((read = fin.read(b)) != -1) {
                         outs.write(b, 0, read);
                    }
                    outs.flush();
                    outs.close();
                    fin.close();
               } catch (RuntimeException e) {
                    // e.printStackTrace();
                    System.out.println("실행중 에러발생 ");
               } finally {
                    if (outs != null) {
                         outs.close();
                    }
                    if (fin != null) {
                         fin.close();
                    }
               }
          } else {
               throw new Exception("파일이 없습니다.");
          }

     }

     /**
      * ajax로 성공 or 실패 처리
      * 
      * @param response
      * @param jsonObject
      * @throws Exception
      */
     protected void returnAjaxString(HttpServletResponse response, boolean isSuccess) throws Exception {

          try {
               response.setCharacterEncoding("UTF-8");
               PrintWriter out = response.getWriter();
               if (isSuccess) {
                    out.print("Y");
               } else {
                    out.print("cancle");
               }
          } catch (RuntimeException e) {
               // e.printStackTrace();
               System.out.println("실행중 에러발생 ");
          }
     }

     /**
      * ajax로 성공 or 실패 처리
      * 
      * @param response
      * @param jsonObject
      * @throws Exception
      */
     protected void returnAjaxString(HttpServletResponse response, String msg) throws Exception {

          try {
               response.setCharacterEncoding("EUC-KR");
               PrintWriter out = response.getWriter();
               out.print(msg);
          } catch (RuntimeException e) {
               // e.printStackTrace();
               System.out.println("실행중 에러발생 ");
          }
     }

     /**
      * JSONArray 객체로 리턴할때
      * 
      * @param response
      * @param list
      * @throws Exception
      */
     protected void returnAjaxJsonArray(HttpServletResponse response, ArrayList<?> list) throws Exception {

          try {
               response.setHeader("cache-control", "no-cache");
               response.setHeader("expires", "-1");
               response.setHeader("pragma", "no-cache");
               response.setContentType("application/json");

               ServletOutputStream out = response.getOutputStream();

               if (list != null && list.size() > 0) {
                    JSONArray jsonArray = null;
                    if (list.get(0) instanceof ArrayList) {
                         jsonArray = new JSONArray(list.toArray());
                    } else if (list.get(0) instanceof Map) {
                         jsonArray = new JSONArray(list.toArray());
                    }
                    out.write(jsonArray.toString().getBytes("EUC-KR"));
               }
          } catch (RuntimeException e) {
               // e.printStackTrace();
               System.out.println("실행중 에러발생 ");
          }
     }


     /**
      * insert, update , delete 처리후 리턴
      * 
      * @param response
      * @param jsonObject
      * @throws Exception
      */
     protected String returnUrl(ModelMap model, String msg, String url) throws Exception {

          model.addAttribute("msg", msg);
          model.addAttribute("url", url);
          return returnUrl(model, msg, url, null);
     }

     /**
      * insert, update , delete 처리후 리턴
      * 
      * @param response
      * @param jsonObject
      * @throws Exception
      */
     protected String returnUrl2(ModelMap model, String msg, String url) throws Exception {

          model.addAttribute("msg", msg);
          model.addAttribute("url", url);
          return returnUrl2(model, msg, url, null);
     }

     /**
      * insert, update , delete 처리후 리턴
      * 
      * @param response
      * @param jsonObject
      * @throws Exception
      */
     protected String returnUrl(ModelMap model, String msg, String url, String target) throws Exception {

          model.addAttribute("msg", msg);
          model.addAttribute("url", url);
          System.out.println("url  : " + url);
          model.addAttribute("target", target);
          return "common/redirect";
     }

     /**
      * insert, update , delete 처리후 리턴
      * 
      * @param response
      * @param jsonObject
      * @throws Exception
      */
     protected String returnUrl2(ModelMap model, String msg, String url, String target) throws Exception {

          model.addAttribute("msg", msg);
          model.addAttribute("url", url);
          model.addAttribute("target", target);
          return "common/redirect2";
     }

     /**
      * 페이징 공통
      * 
      * @param StringTagHandler
      * @param StringTagHandler
      * @throws Exception
      */
     protected Map<String, Object> pagination(Map<String, Object> commandMap, int totCnt) throws Exception {

          return pagination(commandMap, totCnt, 0);
     }

     /**
      * 페이징 공통
      * 
      * @param StringTagHandler
      * @param StringTagHandler
      * @throws Exception
      */
     protected Map<String, Object> pagination(Map<String, Object> commandMap, int totCnt, int recodeSize) throws Exception {

          int currentPageNo = 1;
          int defaultRecordCountPerPage = Integer.parseInt(new String(Constants.getProperty("page.recordsize.per.page")));
          int defaultPageSize = Integer.parseInt(new String(Constants.getProperty("page.pagesize")));
          String pageIndex = (String) commandMap.get("pageIndex");

          if (commandMap.containsKey("pageIndex")) {

               if (pageIndex.equals("undefined") || pageIndex.equals("")) {
                    commandMap.put("pageIndex", "1");
               } else {
                    currentPageNo = Integer.parseInt(pageIndex);

               }
          } else {
               commandMap.put("pageIndex", "1");
          }

          // PaginationInfo에 필수 정보를 넣어 준다.
          PaginationInfo paginationInfo = new PaginationInfo();
          paginationInfo.setCurrentPageNo(currentPageNo); // 현재 페이지 번호
          // 한 페이지에 게시되는 게시물 건수
          if (recodeSize == 0) {
               paginationInfo.setRecordCountPerPage(defaultRecordCountPerPage);
          } else {
               paginationInfo.setRecordCountPerPage(recodeSize);
          }

          /* 페이징 리스트의 사이즈 */
          int pageSize = defaultPageSize;
          if (pageSize > 0) {
               paginationInfo.setPageSize(pageSize);
          }


          int firstRecordIndex = paginationInfo.getFirstRecordIndex();
          int lastRecordIndex = paginationInfo.getLastRecordIndex();
          int recordCountPerPage = paginationInfo.getRecordCountPerPage();
          commandMap.put("FIRSTINDEX", firstRecordIndex);
          commandMap.put("LASTINDEX", lastRecordIndex);
          commandMap.put("FROM_NO", firstRecordIndex);
          commandMap.put("TO_NO", lastRecordIndex);
          commandMap.put("RECORDCOUNTPERPAGE", recordCountPerPage);

          paginationInfo.setTotalRecordCount(totCnt); // 전체 게시물 건 수
          commandMap.put("paginationInfo", paginationInfo);
          return commandMap;

     }
}
