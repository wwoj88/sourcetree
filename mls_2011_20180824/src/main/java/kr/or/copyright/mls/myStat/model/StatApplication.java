package kr.or.copyright.mls.myStat.model;

import java.lang.reflect.Field;
import java.util.List;

import kr.or.copyright.mls.common.BaseObject;

public class StatApplication extends BaseObject {

	public String toString(){
		Class Cls = this.getClass();
		Field[] fie = Cls.getDeclaredFields();      //PUBLIC인 변수를 받는다.

		StringBuffer sb = new StringBuffer();
		sb.append(" @@@ "); sb.append(Cls.getName()); sb.append(" info @@@ ");

		try {
			for( int j = 0; j < fie.length; j++ ){
				/* 특정 필드의 접근제어자가 private 나 protected 로 접근이 불가능 하면
				* 임시로 접근 가능하게 수정 */
				if( !fie[j].isAccessible() ) {
					fie[j].setAccessible( true );
				}
	
				String key = fie[j].getName();      //변수명
				//String value =  (String)Cls.getField(key).get(this);
				Object value = fie[j].get(this);
	
				if( j!=0 ){
					sb.append(" , "); 
				}
				sb.append(key); sb.append(" = "); sb.append(value);
			}
		}catch (Exception e) {
			System.out.println(e);
		}
		return sb.toString();
	}
	
	private String applyWriteYmd;               //신청서작성YMD
	private int applyWriteSeq;                  //신청서작성SEQ
	private String chngDttm;                    //변경일자
	private String applyType01;                 //신청구분_저작물
	private String applyType02;                 //신청구분_실연
	private String applyType03;                 //신청구분_음반
	private String applyType04;                 //신청구분_방송
	private String applyType05;                 //신청구분_데이터베이스
	private int applyWorksCnt;					//저작물 건수
	private String applyWorksTitl;              //저작물 제호/제목
	private String applyWorksKind;              //저작물 종류
	private String applyWorksForm;              //저작물 형태 및 수량
	private String usexDesc;                    //이용의 내용
	private String applyReas;                   //승인신청사유
	private String cpstAmnt;                    //보상금액
	private int applyRawCd;                     //신청서명_저작권번CD
	private String applrName;                   //신청인_성명/법인명
	private String applrResdCorpNumb;           //신청인_주민번호/법인번호
	private String applrAddr;                   //신청인_주소
	private String applrTelx;                   //신청인_전화번호
	private String applyProxyName;              //대리인_성명/법인명
	private String applyProxyResdCorpNumb;      //대리인_주민번호/법인번호
	private String applyProxyAddr;              //대리인_주소
	private String applyProxyTelx;              //대리인_전화번호
	private String applyDate;                   //신청일자
	private String applyNo;                     //신청번호
	private String receiptNo;                   //접수번호
	private int statCd;                         //진행상태CD
	private String statDttm;                    //진행상태 변경일자
	private String rgstDttm;
	private String rgstIdnt;
	private String rgstNm;						// 등록자명
	private String modiDttm;
	private String modiIdnt;
	
	/*20120813_정병호*/
	private String applyDivsCd;			//신청구분코드
	
	private int prevCnt;
	
	public int getPrevCnt() {
	    return prevCnt;
	}
	public void setPrevCnt(int prevCnt) {
	    this.prevCnt = prevCnt;
	}

	private String[] chkWorksId;
	
	public String[] getChkWorksId() {
	    return chkWorksId;
	}
	public void setChkWorksId(String[] chkWorksId) {
	    this.chkWorksId = chkWorksId;
	}

	private String applyTypeNm;
	private String statCdNm;
	private String dummyApplrResdCorpNumb;
	private String dummyApplyProxyResdCorpNumb;
	
	// 첨부화일 관련 
	private List fileList;
	private List delFileList;
	private String[] fileNameCd;
	private String[] fileDelYn;
	private String[] hddnFile;
	private String[] fileName;
	private String[] realFileName;
	private String[] fileSize;
	private String[] filePath;
	
	// 명세서 정보 관련
	private List statApplyWorksList;
	
	// 인증서정보 관련
	private ApplySign applySign;
	
	private String newApplyWriteYmd;               //신청서작성YMD
	private int newApplyWriteSeq;                  //신청서작성SEQ
	
	//  조회조건
	private String srchApplyType;
	private String srchApplyFrDt;
	private String srchApplyToDt;
	private String srchApplyWorksTitl;
	private int srchStatCd;
	private String srchRgstIdnt;
	
	//	페이징
	private int nowPage;  
	private int startRow; 
	private int endRow; 
	private int totalRow;
	
	// 결제정보
	private String lgdMid;
	private String lgdOid;
	private String lgdAmount;
	private String lgdBuyer;
	private String lgdTimestamp;
	private String lgdMertkey;
	private String lgdRespcode;
	private String lgdRespmsg;
	private String lgdTid;
	
	
	public String getApplyDivsCd() {
	    return applyDivsCd;
	}
	public void setApplyDivsCd(String applyDivsCd) {
	    this.applyDivsCd = applyDivsCd;
	}
	
	public String getLgdTid() {
		return lgdTid;
	}
	public void setLgdTid(String lgdTid) {
		this.lgdTid = lgdTid;
	}
	public String getLgdAmount() {
		return lgdAmount;
	}
	public void setLgdAmount(String lgdAmount) {
		this.lgdAmount = lgdAmount;
	}
	public String getLgdBuyer() {
		return lgdBuyer;
	}
	public void setLgdBuyer(String lgdBuyer) {
		this.lgdBuyer = lgdBuyer;
	}
	public String getLgdMertkey() {
		return lgdMertkey;
	}
	public void setLgdMertkey(String lgdMertkey) {
		this.lgdMertkey = lgdMertkey;
	}
	public String getLgdMid() {
		return lgdMid;
	}
	public void setLgdMid(String lgdMid) {
		this.lgdMid = lgdMid;
	}
	public String getLgdOid() {
		return lgdOid;
	}
	public void setLgdOid(String lgdOid) {
		this.lgdOid = lgdOid;
	}
	public String getLgdRespcode() {
		return lgdRespcode;
	}
	public void setLgdRespcode(String lgdRespcode) {
		this.lgdRespcode = lgdRespcode;
	}
	public String getLgdRespmsg() {
		return lgdRespmsg;
	}
	public void setLgdRespmsg(String lgdRespmsg) {
		this.lgdRespmsg = lgdRespmsg;
	}
	public String getLgdTimestamp() {
		return lgdTimestamp;
	}
	public void setLgdTimestamp(String lgdTimestamp) {
		this.lgdTimestamp = lgdTimestamp;
	}
	public String getRgstNm() {
		return rgstNm;
	}
	public void setRgstNm(String rgstNm) {
		this.rgstNm = rgstNm;
	}
	public String getApplrAddr() {
		return applrAddr;
	}
	public void setApplrAddr(String applrAddr) {
		this.applrAddr = applrAddr;
	}
	public String getApplrName() {
		return applrName;
	}
	public void setApplrName(String applrName) {
		this.applrName = applrName;
	}
	public String getApplrResdCorpNumb() {
		return applrResdCorpNumb;
	}
	public void setApplrResdCorpNumb(String applrResdCorpNumb) {
		this.applrResdCorpNumb = applrResdCorpNumb;
	}
	public String getApplrTelx() {
		return applrTelx;
	}
	public void setApplrTelx(String applrTelx) {
		this.applrTelx = applrTelx;
	}
	public String getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	public String getApplyProxyAddr() {
		return applyProxyAddr;
	}
	public void setApplyProxyAddr(String applyProxyAddr) {
		this.applyProxyAddr = applyProxyAddr;
	}
	public String getApplyProxyName() {
		return applyProxyName;
	}
	public void setApplyProxyName(String applyProxyName) {
		this.applyProxyName = applyProxyName;
	}
	public String getApplyProxyResdCorpNumb() {
		return applyProxyResdCorpNumb;
	}
	public void setApplyProxyResdCorpNumb(String applyProxyResdCorpNumb) {
		this.applyProxyResdCorpNumb = applyProxyResdCorpNumb;
	}
	public String getApplyProxyTelx() {
		return applyProxyTelx;
	}
	public void setApplyProxyTelx(String applyProxyTelx) {
		this.applyProxyTelx = applyProxyTelx;
	}
	public int getApplyRawCd() {
		return applyRawCd;
	}
	public void setApplyRawCd(int applyRawCd) {
		this.applyRawCd = applyRawCd;
	}
	public String getApplyReas() {
		return applyReas;
	}
	public void setApplyReas(String applyReas) {
		this.applyReas = applyReas;
	}
	public String getApplyType01() {
		return applyType01;
	}
	public void setApplyType01(String applyType01) {
		this.applyType01 = applyType01;
	}
	public String getApplyType02() {
		return applyType02;
	}
	public void setApplyType02(String applyType02) {
		this.applyType02 = applyType02;
	}
	public String getApplyType03() {
		return applyType03;
	}
	public void setApplyType03(String applyType03) {
		this.applyType03 = applyType03;
	}
	public String getApplyType04() {
		return applyType04;
	}
	public void setApplyType04(String applyType04) {
		this.applyType04 = applyType04;
	}
	public String getApplyType05() {
		return applyType05;
	}
	public void setApplyType05(String applyType05) {
		this.applyType05 = applyType05;
	}
	public String getApplyTypeNm() {
		return applyTypeNm;
	}
	public void setApplyTypeNm(String applyTypeNm) {
		this.applyTypeNm = applyTypeNm;
	}
	public String getApplyWorksForm() {
		return applyWorksForm;
	}
	public void setApplyWorksForm(String applyWorksForm) {
		this.applyWorksForm = applyWorksForm;
	}
	public String getApplyWorksKind() {
		return applyWorksKind;
	}
	public void setApplyWorksKind(String applyWorksKind) {
		this.applyWorksKind = applyWorksKind;
	}
	public String getApplyWorksTitl() {
		return applyWorksTitl;
	}
	public void setApplyWorksTitl(String applyWorksTitl) {
		this.applyWorksTitl = applyWorksTitl;
	}
	public int getApplyWriteSeq() {
		return applyWriteSeq;
	}
	public void setApplyWriteSeq(int applyWriteSeq) {
		this.applyWriteSeq = applyWriteSeq;
	}
	public String getApplyWriteYmd() {
		return applyWriteYmd;
	}
	public void setApplyWriteYmd(String applyWriteYmd) {
		this.applyWriteYmd = applyWriteYmd;
	}
	public String getChngDttm() {
		return chngDttm;
	}
	public void setChngDttm(String chngDttm) {
		this.chngDttm = chngDttm;
	}
	public String getCpstAmnt() {
		return cpstAmnt;
	}
	public void setCpstAmnt(String cpstAmnt) {
		this.cpstAmnt = cpstAmnt;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
	public String[] getFileDelYn() {
		return fileDelYn;
	}
	public void setFileDelYn(String[] fileDelYn) {
		this.fileDelYn = fileDelYn;
	}
	public List getFileList() {
		return fileList;
	}
	public void setFileList(List fileList) {
		this.fileList = fileList;
	}
	public String[] getFileName() {
		return fileName;
	}
	public void setFileName(String[] fileName) {
		this.fileName = fileName;
	}
	public String[] getFileNameCd() {
		return fileNameCd;
	}
	public void setFileNameCd(String[] fileNameCd) {
		this.fileNameCd = fileNameCd;
	}
	public String[] getFilePath() {
		return filePath;
	}
	public void setFilePath(String[] filePath) {
		this.filePath = filePath;
	}
	public String[] getFileSize() {
		return fileSize;
	}
	public void setFileSize(String[] fileSize) {
		this.fileSize = fileSize;
	}
	public String[] getHddnFile() {
		return hddnFile;
	}
	public void setHddnFile(String[] hddnFile) {
		this.hddnFile = hddnFile;
	}
	public String getModiDttm() {
		return modiDttm;
	}
	public void setModiDttm(String modiDttm) {
		this.modiDttm = modiDttm;
	}
	public String getModiIdnt() {
		return modiIdnt;
	}
	public void setModiIdnt(String modiIdnt) {
		this.modiIdnt = modiIdnt;
	}
	public int getNowPage() {
		return nowPage;
	}
	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}
	public String[] getRealFileName() {
		return realFileName;
	}
	public void setRealFileName(String[] realFileName) {
		this.realFileName = realFileName;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getSrchApplyFrDt() {
		return srchApplyFrDt;
	}
	public void setSrchApplyFrDt(String srchApplyFrDt) {
		this.srchApplyFrDt = srchApplyFrDt;
	}
	public String getSrchApplyToDt() {
		return srchApplyToDt;
	}
	public void setSrchApplyToDt(String srchApplyToDt) {
		this.srchApplyToDt = srchApplyToDt;
	}
	public String getSrchApplyType() {
		return srchApplyType;
	}
	public void setSrchApplyType(String srchApplyType) {
		this.srchApplyType = srchApplyType;
	}
	public String getSrchApplyWorksTitl() {
		return srchApplyWorksTitl;
	}
	public void setSrchApplyWorksTitl(String srchApplyWorksTitl) {
		this.srchApplyWorksTitl = srchApplyWorksTitl;
	}
	public String getSrchRgstIdnt() {
		return srchRgstIdnt;
	}
	public void setSrchRgstIdnt(String srchRgstIdnt) {
		this.srchRgstIdnt = srchRgstIdnt;
	}
	public int getSrchStatCd() {
		return srchStatCd;
	}
	public void setSrchStatCd(int srchStatCd) {
		this.srchStatCd = srchStatCd;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getStatCd() {
		return statCd;
	}
	public void setStatCd(int statCd) {
		this.statCd = statCd;
	}
	public String getStatCdNm() {
		return statCdNm;
	}
	public void setStatCdNm(String statCdNm) {
		this.statCdNm = statCdNm;
	}
	public String getStatDttm() {
		return statDttm;
	}
	public void setStatDttm(String statDttm) {
		this.statDttm = statDttm;
	}
	public int getTotalRow() {
		return totalRow;
	}
	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}
	public String getUsexDesc() {
		return usexDesc;
	}
	public void setUsexDesc(String usexDesc) {
		this.usexDesc = usexDesc;
	}
	public List getDelFileList() {
		return delFileList;
	}
	public void setDelFileList(List delFileList) {
		this.delFileList = delFileList;
	}
	public int getApplyWorksCnt() {
		return applyWorksCnt;
	}
	public void setApplyWorksCnt(int applyWorksCnt) {
		this.applyWorksCnt = applyWorksCnt;
	}
	public List getStatApplyWorksList() {
		return statApplyWorksList;
	}
	public void setStatApplyWorksList(List statApplyWorksList) {
		this.statApplyWorksList = statApplyWorksList;
	}
	public ApplySign getApplySign() {
		return applySign;
	}
	public void setApplySign(ApplySign applySign) {
		this.applySign = applySign;
	}
	public int getNewApplyWriteSeq() {
		return newApplyWriteSeq;
	}
	public void setNewApplyWriteSeq(int newApplyWriteSeq) {
		this.newApplyWriteSeq = newApplyWriteSeq;
	}
	public String getNewApplyWriteYmd() {
		return newApplyWriteYmd;
	}
	public void setNewApplyWriteYmd(String newApplyWriteYmd) {
		this.newApplyWriteYmd = newApplyWriteYmd;
	}
	public String getDummyApplrResdCorpNumb() {
		return dummyApplrResdCorpNumb;
	}
	public void setDummyApplrResdCorpNumb(String dummyApplrResdCorpNumb) {
		this.dummyApplrResdCorpNumb = dummyApplrResdCorpNumb;
	}
	public String getDummyApplyProxyResdCorpNumb() {
		return dummyApplyProxyResdCorpNumb;
	}
	public void setDummyApplyProxyResdCorpNumb(String dummyApplyProxyResdCorpNumb) {
		this.dummyApplyProxyResdCorpNumb = dummyApplyProxyResdCorpNumb;
	}
	
		
}