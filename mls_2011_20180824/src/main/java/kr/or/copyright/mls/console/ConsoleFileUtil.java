package kr.or.copyright.mls.console;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ConsoleFileUtil {
  public void FileDownLoad(String path, String newFileName, String orgFileName, HttpServletRequest request, HttpServletResponse response) throws IOException {
    File file = new File(path + newFileName);
    // File file2 = new File(savepath+returnMap.get( "orgFileName" ));
    String userAgent = request.getHeader("User-Agent");
    boolean ie = userAgent.indexOf("MSIE") > -1 || userAgent.indexOf("rv:11") > -1;
    String fileName = orgFileName;

    if (ie) {
      fileName = URLEncoder.encode(orgFileName, "utf-8");
      fileName = fileName.replaceAll("\\+", "%20");
    } else {
      fileName = new String(fileName.getBytes(), "iso-8859-1");
    }
    System.out.println(fileName);
    response.setCharacterEncoding("UTF-8");
    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\";");

    FileInputStream fis = new FileInputStream(file);
    BufferedInputStream bis = new BufferedInputStream(fis);
    ServletOutputStream so = response.getOutputStream();
    BufferedOutputStream bos = new BufferedOutputStream(so);

    byte[] data = new byte[2048];
    int input = 0;
    while ((input = bis.read(data)) != -1) {
      bos.write(data, 0, input);
      bos.flush();
    }

    if (bos != null)
      bos.close();
    if (bis != null)
      bis.close();
    if (so != null)
      so.close();
    if (fis != null)
      fis.close();
  }
}
