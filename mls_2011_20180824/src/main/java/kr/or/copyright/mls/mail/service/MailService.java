package kr.or.copyright.mls.mail.service;

import kr.or.copyright.mls.mail.model.Mail;

public interface MailService {
	
	// 수신거부 insert
	public int insertRejc(Mail mail)throws Exception;
	
	// 수신거부 delete
	public int deleteRejc(Mail mail)throws Exception;

	// 이메일 수신확인
	public int updateEmailRecv(Mail mail) throws Exception; 
	
}
