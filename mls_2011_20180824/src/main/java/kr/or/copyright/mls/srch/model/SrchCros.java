package kr.or.copyright.mls.srch.model;

public class SrchCros {
	private String contTitle;			//제목
	private String regId;				//등록번호
	private String regDate;				//등록일자
	private String regPart1Name;		//등록부문1
	private String regPart2Name;		//등록부문
	private String authorName;			//저작자
	private String regReason;			//등록원인
	private String regCoptHodrName;		//등록관리자
	private String disposalAddr1;		//주소
	private String contClassNameCd;		//장르이름

	private String genreCd;				//장르번호
	private int pageNo;					//페이징
	private int startNo;
	private int endNo;
	
	public String getContTitle() {
		return contTitle;
	}
	public void setContTitle(String contTitle) {
		this.contTitle = contTitle;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegPart1Name() {
		return regPart1Name;
	}
	public void setRegPart1Name(String regPart1Name) {
		this.regPart1Name = regPart1Name;
	}
	public String getRegPart2Name() {
		return regPart2Name;
	}
	public void setRegPart2Name(String regPart2Name) {
		this.regPart2Name = regPart2Name;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getRegReason() {
		return regReason;
	}
	public void setRegReason(String regReason) {
		this.regReason = regReason;
	}
	public String getRegCoptHodrName() {
		return regCoptHodrName;
	}
	public void setRegCoptHodrName(String regCoptHodrName) {
		this.regCoptHodrName = regCoptHodrName;
	}
	public String getDisposalAddr1() {
		return disposalAddr1;
	}
	public void setDisposalAddr1(String disposalAddr1) {
		this.disposalAddr1 = disposalAddr1;
	}
	public String getContClassNameCd() {
		return contClassNameCd;
	}
	public void setContClassNameCd(String contClassNameCd) {
		this.contClassNameCd = contClassNameCd;
	}
	public String getGenreCd() {
		return genreCd;
	}
	public void setGenreCd(String genreCd) {
		this.genreCd = genreCd;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getStartNo() {
		return startNo;
	}
	public void setStartNo(int startNo) {
		this.startNo = startNo;
	}
	public int getEndNo() {
		return endNo;
	}
	public void setEndNo(int endNo) {
		this.endNo = endNo;
	}

	
}
