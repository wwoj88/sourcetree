package kr.or.copyright.mls.statBord.model;

public class AnucBordSupl {

	
	/* 저작권자 조회공고, 보상금 공탁공고 ,상당한노력 저작권자 조회 공고 보완내역 관련 DTO*/
	
	private long suplId;		//보완ID
	private long suplSeq;		//보완차수
	private long suplCd;		//보완구분CD
	
	private long bordCd;		//게시판 구분CD
	private long bordSeqn;		//게시판 순번
	
	private long worksId;		//저작물 ID	

	
	private String suplItem;		//보완 항목
	private String suplItemCd;	//보완 항목 코드
	private String rgstName;	// 등록자 이름
	private String rgstIdnt;	//등록자 ID
	private String rgstDttm;	//등록 날짜
	private String preItem;	//이전항목
	private String postItem;	//이후항목
	private String modiItem;	//이전항목+이후항목
	
	
	@Override
	public String toString() {
		return "AnucBordSupl [suplId=" + suplId + ", suplSeq=" + suplSeq
				+ ", suplCd=" + suplCd + ", bordCd=" + bordCd + ", bordSeqn="
				+ bordSeqn + ", worksId=" + worksId + ", suplItem=" + suplItem
				+ ", suplItemCd=" + suplItemCd + ", rgstName=" + rgstName
				+ ", rgstIdnt=" + rgstIdnt + ", rgstDttm=" + rgstDttm
				+ ", preItem=" + preItem + ", postItem=" + postItem
				+ ", modiItem=" + modiItem + "]";
	}
	public AnucBordSupl() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AnucBordSupl(long suplId, long suplSeq, long suplCd, long bordCd,
			long bordSeqn, long worksId, String suplItem, String suplItemCd,
			String rgstName, String rgstIdnt, String rgstDttm, String preItem,
			String postItem, String modiItem) {
		super();
		this.suplId = suplId;
		this.suplSeq = suplSeq;
		this.suplCd = suplCd;
		this.bordCd = bordCd;
		this.bordSeqn = bordSeqn;
		this.worksId = worksId;
		this.suplItem = suplItem;
		this.suplItemCd = suplItemCd;
		this.rgstName = rgstName;
		this.rgstIdnt = rgstIdnt;
		this.rgstDttm = rgstDttm;
		this.preItem = preItem;
		this.postItem = postItem;
		this.modiItem = modiItem;
	}
	public String getModiItem() {
		return modiItem;
	}
	public void setModiItem(String modiItem) {
		this.modiItem = modiItem;
	}
	public String getRgstName() {
		return rgstName;
	}
	public void setRgstName(String rgstName) {
		this.rgstName = rgstName;
	}
	public long getBordCd() {
		return bordCd;
	}
	public void setBordCd(long bordCd) {
		this.bordCd = bordCd;
	}
	public long getBordSeqn() {
		return bordSeqn;
	}
	public void setBordSeqn(long bordSeqn) {
		this.bordSeqn = bordSeqn;
	}
	public long getSuplSeq() {
		return suplSeq;
	}
	public void setSuplSeq(long suplSeq) {
		this.suplSeq = suplSeq;
	}
	public String getSuplItem() {
		return suplItem;
	}
	public void setSuplItem(String suplItem) {
		this.suplItem = suplItem;
	}
	public String getSuplItemCd() {
		return suplItemCd;
	}
	public void setSuplItemCd(String suplItemCd) {
		this.suplItemCd = suplItemCd;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	public String getPreItem() {
		return preItem;
	}
	public void setPreItem(String preItem) {
		this.preItem = preItem;
	}
	public String getPostItem() {
		return postItem;
	}
	public void setPostItem(String postItem) {
		this.postItem = postItem;
	}
	public long getSuplId() {
		return suplId;
	}
	public void setSuplId(long suplId) {
		this.suplId = suplId;
	}
	public long getSuplCd() {
		return suplCd;
	}
	public void setSuplCd(long suplCd) {
		this.suplCd = suplCd;
	}
	public long getWorksId() {
		return worksId;
	}
	public void setWorksId(long worksId) {
		this.worksId = worksId;
	}
	
	
	
}
