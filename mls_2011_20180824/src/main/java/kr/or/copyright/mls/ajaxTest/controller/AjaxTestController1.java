package kr.or.copyright.mls.ajaxTest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import kr.or.copyright.mls.ajaxTest.service.CodeListService;
import kr.or.copyright.mls.user.model.ZipCodeRoad;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AjaxTestController1{
	
	private static final Log LOG = LogFactory.getLog(AjaxTestController1.class);
	
	@Resource(name="CodeListBO")
	private CodeListService codeListBO;
	
	@RequestMapping("/ajaxTest/zip.do")
	protected ModelAndView getJsonZip(HttpServletRequest request) throws Exception{
		ModelAndView modelAndView = new ModelAndView("jsonReport");
		Map<String, String> params = new HashMap<String, String>();
		String sidoNm = ServletRequestUtils.getStringParameter(request, "sidoNm");
		System.out.println("sidoNmsidoNmsidoNmsidoNm: "+sidoNm);
		params.put("sidoNm", sidoNm);
		List<ZipCodeRoad> codeList = codeListBO.getRoadCodeList(params);
		modelAndView.addObject("codeList",codeList);
		return modelAndView;
	}
}