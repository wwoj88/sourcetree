package kr.or.copyright.mls.console.rcept;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.console.ConsoleFileUtil;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.ExcelUtil;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd17Service;
import kr.or.copyright.mls.support.util.IPUtil;
import kr.or.copyright.mls.support.util.SessionUtil;
import kr.or.copyright.mls.support.util.UserConnectLogUtil;
import net.sf.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.tobesoft.platform.data.Dataset;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 관리저작물 접수 및 처리 > 위탁관리저작물 > 목록
 * 
 * @author ljh
 */
@Controller
public class FdcrAd17Controller extends DefaultController {

     @Resource(name = "fdcrAd17Service")
     private FdcrAd17Service fdcrAd17Service;
     // @Resource( name = "userConnectLogUtil" )
     // private UserConnectLogUtil userConnectLogUtil;
     private static final Logger logger = LoggerFactory.getLogger(FdcrAd17Controller.class);
     public static final int RETURN_TYPE_STRING = 1;

     /**
      * 위탁관리 저작물 보고 저장
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17Insert2.page")
     public void fdcrAd17Insert2(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", "");
          commandMap.put("GENRE_CODE", "");
          commandMap.put("USER_IDNT", "");
          commandMap.put("USER_NAME", "");

          boolean isSuccess = fdcrAd17Service.fdcrAd17Insert2(commandMap);
          returnAjaxString(response, isSuccess);
     }

     /**
      * 엑셀다운로드
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     // private String savepath = "D:\\Git\\mls_2011\\web\\upload\\form\\";
     private String savepath = "/home/right4me/web/upload/form/";

     @RequestMapping(value = "/console/rcept/fdcrAd17Down1.page")
     public void fdcrAd17Down1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {


          // 파라미터 셋팅
          String genreCode = EgovWebUtil.getString(commandMap, "GENRE_CODE");
          String filePath = "";
          String fileName = "";

          if ("1".equals(genreCode)) {
               fileName = "upload_sample01_music.xlsx";
               filePath = "위탁관리저작물_01음악_업로드샘플.xlsx";
          } else if ("2".equals(genreCode)) {
               fileName = "upload_sample02_literary_work.xlsx";
               filePath = "위탁관리저작물_02어문_업로드샘플.xlsx";
          } else if ("3".equals(genreCode)) {
               fileName = "upload_sample03_script.xlsx";
               filePath = "위탁관리저작물_03방송대본_업로드샘플.xlsx";
          } else if ("4".equals(genreCode)) {
               fileName = "upload_sample04_movie.xlsx";
               filePath = "위탁관리저작물_04영화_업로드샘플.xlsx";
          } else if ("5".equals(genreCode)) {
               fileName = "upload_sample05_broadcast.xlsx";
               filePath = "위탁관리저작물_05방송_업로드샘플.xlsx";
          } else if ("6".equals(genreCode)) {
               fileName = "upload_sample06_news.xlsx";
               filePath = "위탁관리저작물_06뉴스_업로드샘플.xlsx";
          } else if ("7".equals(genreCode)) {
               fileName = "upload_sample07_art.xlsx";
               filePath = "위탁관리저작물_07미술_업로드샘플.xlsx";
          } else if ("8".equals(genreCode)) {
               fileName = "upload_sample08_image.xlsx";
               filePath = "위탁관리저작물_08이미지_업로드샘플.xlsx";
          } else if ("99".equals(genreCode)) {
               fileName = "upload_sample99_orher.xlsx";
               filePath = "위탁관리저작물_99기타_업로드샘플.xlsx";
          }
          if (null != fileName && !"".equals(fileName)) {
               ConsoleFileUtil fileDwonUtil = new ConsoleFileUtil();
               fileDwonUtil.FileDownLoad(savepath, filePath, fileName, request, response);

               // download( request, response, filePath, fileName );
          }
     }

     /**
      * 일괄등록 엑셀 업로드 후 조회
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */

     @RequestMapping(value = "/console/rcept/fdcrAd17Upload1.page")
     public String fdcrAd17Upload1(ModelMap model, Map<String, Object> commandMap, MultipartHttpServletRequest mreq, HttpServletRequest request, HttpServletResponse response) throws Exception {
          // logger.debug( "fdcrAd17Upload1 Call : " + request.getContextPath());
          // 담을 arrayList
          // 성공리스트

          // userConnectLogUtil.userConnectLog( request , "fdcrAd17Upload1");


          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
          // 실패리스트
          ArrayList<Map<String, Object>> uploadFailList = new ArrayList<Map<String, Object>>();

          String uploadPath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path"));

          ArrayList<Map<String, Object>> fileinfo = fileUpload(mreq, uploadPath, null, true);
          // String F_saveFilePath = (String) fileinfo.get(0).get("F_saveFilePath");
          String file1 = uploadPath + fileinfo.get(0).get("F_fileName");
          String fileName = (String) fileinfo.get(0).get("F_orgFileName");

          ArrayList<ArrayList<Map<String, Object>>> excelDataList = ExcelUtil.get(file1, RETURN_TYPE_STRING);
          // System.out.println( "excelDataList : " + excelDataList);
          ArrayList<Map<String, Object>> excelDataList1 = excelDataList.get(0);
          // System.out.println( "excelDataList1 : " + excelDataList1);
          String genreCode = EgovWebUtil.getString(commandMap, "GENRE_CODE");
          String reptChrrName = EgovWebUtil.getString(commandMap, "REPT_CHRR_NAME");
          System.out.println("reptChrrName  : " + reptChrrName);
          System.out.println("reptChrrName  : " + reptChrrName);
          String reptChrrPosi = EgovWebUtil.getString(commandMap, "REPT_CHRR_POSI");
          String reptChrrTelx = EgovWebUtil.getString(commandMap, "REPT_CHRR_TELX");
          String reptChrrMail = EgovWebUtil.getString(commandMap, "REPT_CHRR_MAIL");

          Map<String, Object> exMap = new HashMap<String, Object>();
          Map<String, Object> failMap = new HashMap<String, Object>();

          if ("1".equals(genreCode)) { /* 음악 */
               /* 반복문 시작 */
               for (int i = 12; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 12);
                    System.out.println("총 갯수:::::::::::::::" + exMap.get("TOT_CNT"));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    System.out.println("excelDataList의 행:" + excelDataList1.size());
                    System.out.println("excelDataList의 열:" + excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         if (valueName != null) {
                              valueName = valueName.replace("\n", "");
                         }

                         System.out.println(keyName + " = " + valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("NATION_CD", "0");
                              } else {

                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || columnCheck(valueName, createArrayList("1", "2"))) {
                                        failMap.put("NATION_CD", valueName);

                                   } else {
                                        exMap.put("NATION_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 165 || valueName.length() == 0) {
                                   failMap.put("ALBUM_TITLE", valueName);
                              } else {
                                   exMap.put("ALBUM_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              exMap.put("ALBUM_LABLE", valueName);
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("DISK_SIDE", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 20 || valueName.length() == 0) {
                                        failMap.put("DISK_SIDE", valueName);

                                   } else {
                                        exMap.put("DISK_SIDE", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("TRACK_NO", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() == 0) {
                                        failMap.put("TRACK_NO", valueName);

                                   } else {
                                        exMap.put("TRACK_NO", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if (valueName.length() > 8) {
                                   valueName = "0";
                              }
                              if (valueName.equals("없음")) {
                                   exMap.put("ALBUM_ISSU_YEAR", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8) {
                                        failMap.put("ALBUM_ISSU_YEAR", valueName);

                                   } else {
                                        exMap.put("ALBUM_ISSU_YEAR", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("LYRC", valueName);

                              } else {
                                   exMap.put("LYRC", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              // System.out.println( "COMP VALUE : "+ valueName);
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   // System.out.println( "COMP FAIL : "+ valueName);
                                   failMap.put("COMP", valueName);

                              } else {
                                   // System.out.println( "COMP EX : "+ valueName);
                                   exMap.put("COMP", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("ARRA", valueName);

                              } else {
                                   exMap.put("ARRA", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_12")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("TRAN", valueName);

                              } else {
                                   exMap.put("TRAN", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_13")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("SING", valueName);

                              } else {
                                   exMap.put("SING", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_14")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 1300 || valueName.length() == 0) {
                                   failMap.put("PERF", valueName);

                              } else {
                                   exMap.put("PERF", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_15")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("PROD", valueName);

                              } else {
                                   exMap.put("PROD", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_16")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_17")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("LICE_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() > 3 || columnCheck(valueName, createArrayList("1", "2", "3"))) {
                                        failMap.put("LICE_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("LICE_MGNT_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_18")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("PERF_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() > 3 || columnCheck(valueName, createArrayList("1", "2", "3"))) {
                                        failMap.put("PERF_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("PERF_MGNT_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_19")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("PROD_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 3 || columnCheck(valueName, createArrayList("1", "2", "3"))) {
                                        failMap.put("PROD_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("PROD_MGNT_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_20")) {
                              if (valueName == null || "".equals(valueName) || valueName.equals("없음")) {
                                   exMap.put("UCI", "0");
                              } else {
                                   if (valueName.length() > 200 || valueName.length() == 0) {
                                        failMap.put("UCI", valueName);

                                   } else {
                                        exMap.put("UCI", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_21")) {
                              if (valueName == null || "".equals(valueName) || valueName.equals("없음")) {
                                   exMap.put("RATIO", "0");
                              } else {
                                   if (valueName.length() > 200 || valueName.length() == 0) {
                                        failMap.put("RATIO", valueName);

                                   } else {
                                        exMap.put("RATIO", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }
                    System.out.println("errCnt   :::" + errCnt);
                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         System.out.println("isSuccess : " + isSuccess);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }
               /* 반복문 끝 */
          } else if ("2".equals(genreCode)) { /* 어문 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);
                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("WORKS_TITLE", valueName);
                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_SUB_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_SUB_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("CRT_YEAR", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 4 || valueName.length() == 0) {
                                        failMap.put("CRT_YEAR", valueName);
                                   } else {
                                        exMap.put("CRT_YEAR", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("BOOK_TITLE", valueName);
                              } else {
                                   exMap.put("BOOK_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("BOOK_PUBLISHER", valueName);
                              } else {
                                   exMap.put("BOOK_PUBLISHER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("BOOK_ISSU_YEAR", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 4) {
                                        failMap.put("BOOK_ISSU_YEAR", valueName);
                                   } else {
                                        exMap.put("BOOK_ISSU_YEAR", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("COPT_NAME", valueName);
                              } else {
                                   exMap.put("COPT_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 4 || valueName.length() == 0
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);
                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    // System.out.println( "exMap.keySet() :" + exMap.keySet());
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println("failKey :::" + failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);
                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("3".equals(genreCode)) { /* 방송대본 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    System.out.println("총 갯수:::::::::::::::" + exMap.get("TOT_CNT"));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    System.out.println("excelDataList의 행:" + excelDataList1.size());
                    System.out.println("excelDataList의 열:" + excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);
                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 165) {
                                   failMap.put("WORKS_ORIG_TITLE", valueName);
                              } else {
                                   exMap.put("WORKS_ORIG_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 6 || valueName.length() == 0) {
                                        failMap.put("SCRT_GENRE_CD", valueName);
                                   } else {
                                        exMap.put("SCRT_GENRE_CD", valueName);
                                   }
                              } else {
                                   exMap.put("SCRT_GENRE_CD", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 200 || valueName.length() == 0) {
                                        failMap.put("SCRP_SUBJ_CD", valueName);
                                   } else {
                                        exMap.put("SCRP_SUBJ_CD", valueName);
                                   }
                              } else {
                                   exMap.put("SCRP_SUBJ_CD", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("ORIG_WRITER", valueName);
                              } else {
                                   exMap.put("ORIG_WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 130) {
                                   failMap.put("PLAYER", valueName);
                              } else {
                                   exMap.put("PLAYER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 10) {
                                        failMap.put("BROD_ORD_SEQ", valueName);

                                   } else {
                                        exMap.put("BROD_ORD_SEQ", valueName);
                                   }
                              } else {
                                   exMap.put("BROD_ORD_SEQ", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("BORD_DATE", valueName);
                                   } else {
                                        exMap.put("BORD_DATE", valueName);
                                   }
                              } else {
                                   exMap.put("BORD_DATE", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if (valueName.length() > 65 || "없음".equals(valueName)) {
                                   failMap.put("MAKE_CPY", valueName);

                              } else {
                                   exMap.put("MAKE_CPY", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("WRITER", valueName);

                              } else {
                                   exMap.put("WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_12")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("DIRECTOR", valueName);

                              } else {
                                   exMap.put("DIRECTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_13")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }

                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("4".equals(genreCode)) { /* 영화 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 4 || valueName.length() == 0) {
                                        failMap.put("CRT_YEAR", valueName);

                                   } else {
                                        exMap.put("CRT_YEAR", valueName);
                                   }
                              } else {
                                   exMap.put("CRT_YEAR", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("PLAYER", valueName);

                              } else {
                                   exMap.put("PLAYER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("DIRECT", valueName);

                              } else {
                                   exMap.put("DIRECT", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("WRITER", valueName);

                              } else {
                                   exMap.put("WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 130 || valueName.length() == 0) {
                                   failMap.put("DIRECTOR", valueName);

                              } else {
                                   exMap.put("DIRECTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("PRODUCER", valueName);

                              } else {
                                   exMap.put("PRODUCER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("DISTRIBUTOR", valueName);

                              } else {
                                   exMap.put("DISTRIBUTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("INVESTOR", valueName);

                              } else {
                                   exMap.put("INVESTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("5".equals(genreCode)) { /* 방송 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (valueName.equals("없음") || !"99".equals(valueName) && valueName.length() > 1) {
                                   exMap.put("BORD_GENRE_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 6 || valueName.length() == 0
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5", "6", "7", "99"))) {
                                        if (!"99".equals(valueName)) {
                                             failMap.put("BORD_GENRE_CD", valueName);
                                        }
                                   } else {
                                        exMap.put("BORD_GENRE_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("MAKE_YEAR", valueName);

                                   } else {
                                        exMap.put("MAKE_YEAR", valueName);
                                   }
                              } else {
                                   exMap.put("MAKE_YEAR", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 10 || valueName.length() == 0) {
                                        failMap.put("BROD_ORD_SEQ", valueName);

                                   } else {
                                        exMap.put("BROD_ORD_SEQ", valueName);
                                   }
                              } else {
                                   exMap.put("BROD_ORD_SEQ", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("DIRECTOR", valueName);

                              } else {
                                   exMap.put("DIRECTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("MAKE_CPY", valueName);

                              } else {
                                   exMap.put("MAKE_CPY", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 130 || valueName.length() == 0) {
                                   failMap.put("PLAYER", valueName);

                              } else {
                                   exMap.put("PLAYER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("WRITER", valueName);

                              } else {
                                   exMap.put("WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }


                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("6".equals(genreCode)) { /* 뉴스 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);
                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 10 || valueName.length() == 0) {
                                   if (!"99".equals(valueName)) {
                                        failMap.put("PAPE_NO", valueName);
                                   }
                              } else {
                                   exMap.put("PAPE_NO", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 33) {
                                   failMap.put("PAPE_KIND", valueName);

                              } else {
                                   exMap.put("PAPE_KIND", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("CONTRIBUTOR", valueName);

                              } else {
                                   exMap.put("CONTRIBUTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("REPOTER", valueName);

                              } else {
                                   exMap.put("REPOTER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 6) {
                                   failMap.put("REVISION_ID", valueName);

                              } else {
                                   exMap.put("REVISION_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("ARTICL_DATE", valueName);

                                   } else {
                                        exMap.put("ARTICL_DATE", valueName);
                                   }
                              } else {
                                   exMap.put("ARTICL_DATE", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null) {
                                   failMap.put("ARTICL_ISSU_DTTM", valueName);
                              } else {
                                   exMap.put("ARTICL_ISSU_DTTM", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("PROVIDER", valueName);
                              } else {
                                   exMap.put("PROVIDER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("7".equals(genreCode)) { /* 미술 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (!"".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 160) {
                                        failMap.put("WORKS_SUB_TITLE", valueName);
                                   } else {
                                        exMap.put("WORKS_SUB_TITLE", valueName);
                                   }
                              } else {
                                   exMap.put("WORKS_SUB_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 33) {
                                   failMap.put("KIND", valueName);

                              } else {
                                   exMap.put("KIND", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("MAKE_DATE", valueName);

                                   } else {
                                        exMap.put("MAKE_DATE", valueName);
                                   }
                              } else {
                                   exMap.put("MAKE_DATE", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("SOURCE_INFO", valueName);

                              } else {
                                   exMap.put("SOURCE_INFO", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 165) {
                                   failMap.put("SIZE_INFO", valueName);

                              } else {
                                   exMap.put("SIZE_INFO", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("RESOLUTION", valueName);

                              } else {
                                   exMap.put("RESOLUTION", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 165) {
                                   failMap.put("MAIN_MTRL", valueName);

                              } else {
                                   exMap.put("MAIN_MTRL", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("TXTR", valueName);

                              } else {
                                   exMap.put("TXTR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1000) {
                                        failMap.put("IMG_URL", valueName);

                                   } else {
                                        exMap.put("IMG_URL", valueName);
                                   }
                              } else {
                                   exMap.put("IMG_URL", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_12")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 333) {
                                        failMap.put("DETL_URL", valueName);

                                   } else {
                                        exMap.put("DETL_URL", valueName);
                                   }
                              } else {
                                   exMap.put("DETL_URL", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_13")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("WRITER", valueName);

                              } else {
                                   exMap.put("WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_14")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("POSS_DATE", valueName);

                                   } else {
                                        exMap.put("POSS_DATE", valueName);
                                   }
                              } else {
                                   exMap.put("POSS_DATE", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_15")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("POSS_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("POSS_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_16")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                         // System.out.println( "art fail Map : " + failMap);
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("8".equals(genreCode)) { /* 이미지 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    System.out.println("총 갯수:::::::::::::::" + exMap.get("TOT_CNT"));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    System.out.println("excelDataList의 행:" + excelDataList1.size());
                    System.out.println("excelDataList의 열:" + excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();

                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         System.out.println(keyName + " = " + valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 2500 || valueName.length() == 0) {
                                        failMap.put("IMAGE_DESC", valueName);
                                   } else {
                                        exMap.put("IMAGE_DESC", valueName);
                                   }
                              } else {
                                   exMap.put("IMAGE_DESC", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 650 || valueName.length() == 0) {
                                        failMap.put("KEYWORD", valueName);

                                   } else {
                                        exMap.put("KEYWORD", valueName);
                                   }
                              } else {
                                   exMap.put("KEYWORD", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                   failMap.put("CRT_YEAR", valueName);

                              } else {
                                   exMap.put("CRT_YEAR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 300 || valueName.length() == 0) {
                                   failMap.put("WRITER", valueName);

                              } else {
                                   exMap.put("WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 180 || valueName.length() == 0) {
                                   failMap.put("PRODUCER", valueName);

                              } else {
                                   exMap.put("PRODUCER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 180 || valueName.length() == 0) {
                                        failMap.put("IMAGE_URL", valueName);

                                   } else {
                                        exMap.put("IMAGE_URL", valueName);
                                   }
                              } else {
                                   exMap.put("IMAGE_URL", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 1 || !"Y".equals(valueName) && !"N".equals(valueName)) {
                                   failMap.put("IMAGE_OPEN_YN", valueName);

                              } else {
                                   exMap.put("IMAGE_OPEN_YN", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if (valueName == null || valueName.length() > 37) {
                                   failMap.put("ICNX_NUMB", valueName);

                              } else {
                                   exMap.put("ICNX_NUMB", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("99".equals(genreCode)) { /* 기타 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 6 || valueName.length() == 0
                                        || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5", "99"))) {
                                   failMap.put("SIDE_GENRE_CD", valueName);
                              } else {
                                   exMap.put("SIDE_GENRE_CD", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 160) {
                                        failMap.put("WORKS_TITLE", valueName);
                                   } else {
                                        exMap.put("WORKS_TITLE", valueName);
                                   }
                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("COPT_NAME", valueName);

                              } else {
                                   exMap.put("COPT_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 4 || valueName.length() == 0) {
                                        failMap.put("CRT_YEAR", valueName);

                                   } else {
                                        exMap.put("CRT_YEAR", valueName);
                                   }
                              } else {
                                   exMap.put("CRT_YEAR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("PUBL_MEDI", valueName);

                              } else {
                                   exMap.put("PUBL_MEDI", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("PUBL_DATE", valueName);

                                   } else {
                                        exMap.put("PUBL_DATE", valueName);
                                   }
                              } else {
                                   exMap.put("PUBL_DATE", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          }


          // 보고연월
          GregorianCalendar d = new GregorianCalendar();
          String reptYYYYTmp = String.valueOf(d.get(Calendar.YEAR));
          String reptMMTmp = String.valueOf(((d.get(Calendar.MONTH) + 1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH) + 1) : String.valueOf(d.get(Calendar.MONTH) + 1));

          String reptYYYY = reptYYYYTmp.replaceAll(" ", "");
          String reptMM = reptMMTmp.replaceAll(" ", "");
          String commName = ConsoleLoginUser.getCommname();

          model.addAttribute("commName", commName);
          model.addAttribute("reptYYYY", reptYYYY);
          model.addAttribute("reptMM", reptMM);
          model.addAttribute("genreCode", genreCode);
          model.addAttribute("reptChrrName", reptChrrName);
          model.addAttribute("reptChrrPosi", reptChrrPosi);
          model.addAttribute("reptChrrTelx", reptChrrTelx);
          model.addAttribute("reptChrrMail", reptChrrMail);
          model.addAttribute("commName", commName);
          model.addAttribute("fileName", fileName);

          model.addAttribute("uploadList", uploadList);
          model.addAttribute("uploadFailList", uploadFailList);
          model.addAttribute("exMap", exMap);
          model.addAttribute("failMap", failMap);

          // System.out.println("uploadFailList::::::::::"+uploadFailList);
          if (uploadFailList.size() > 0) {
               return "rcept/fdcrAd17UploadForm1.tiles";
          } else {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = EgovWebUtil.getString(commandMap, "REPT_CHRR_NAME");
               String reptChrrMail2 = EgovWebUtil.getString(commandMap, "REPT_CHRR_MAIL");
               String COMM_NAME = EgovWebUtil.getString(commandMap, "COMM_NAME");
               String genreCode2 = EgovWebUtil.getString(commandMap, "GENRE_CODE");
               if (genreCode2.equals("1")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 12);
               } else if (genreCode2.equals("2")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("3")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("4")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("5")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("6")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("7")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("8")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("99")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               }
               mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               mailMap.put("COMM_NAME", COMM_NAME);

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 일괄수정 엑셀 업로드 후 조회
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17Upload2.page")
     public String fdcrAd17Upload2(ModelMap model, Map<String, Object> commandMap, MultipartHttpServletRequest mreq, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 담을 arrayList
          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
          // 실패리스트
          ArrayList<Map<String, Object>> uploadFailList = new ArrayList<Map<String, Object>>();

          String uploadPath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path"));
          ArrayList<Map<String, Object>> fileinfo = fileUpload(mreq, uploadPath, null, true);

          String file1 = uploadPath + fileinfo.get(0).get("F_fileName");
          String fileName = (String) fileinfo.get(0).get("F_orgFileName");
          // String file1 = EgovWebUtil.getString( commandMap, "file1" );

          ArrayList<ArrayList<Map<String, Object>>> excelDataList = ExcelUtil.get(file1, RETURN_TYPE_STRING);
          ArrayList<Map<String, Object>> excelDataList1 = excelDataList.get(0);

          String genreCode = EgovWebUtil.getString(commandMap, "GENRE_CODE");
          String reptChrrName = EgovWebUtil.getString(commandMap, "REPT_CHRR_NAME");
          String reptChrrPosi = EgovWebUtil.getString(commandMap, "REPT_CHRR_POSI");
          String reptChrrTelx = EgovWebUtil.getString(commandMap, "REPT_CHRR_TELX");
          String reptChrrMail = EgovWebUtil.getString(commandMap, "REPT_CHRR_MAIL");

          Map<String, Object> exMap = new HashMap<String, Object>();
          Map<String, Object> failMap = new HashMap<String, Object>();

          if ("1".equals(genreCode)) { /* 음악 */
               /* 반복문 시작 */
               for (int i = 12; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 12);
                    System.out.println("총 갯수:::::::::::::::" + exMap.get("TOT_CNT"));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    System.out.println("excelDataList의 행:" + excelDataList1.size());
                    System.out.println("excelDataList의 열:" + excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         if (valueName != null) {
                              valueName = valueName.replace("\n", "");
                         }

                        // System.out.println(keyName + " = " + valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("NATION_CD", "0");
                              } else {

                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || columnCheck(valueName, createArrayList("1", "2"))) {
                                        failMap.put("NATION_CD", valueName);

                                   } else {
                                        exMap.put("NATION_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 165 || valueName.length() == 0) {
                                   failMap.put("ALBUM_TITLE", valueName);
                              } else {
                                   exMap.put("ALBUM_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              exMap.put("ALBUM_LABLE", valueName);
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("DISK_SIDE", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 20 || valueName.length() == 0) {
                                        failMap.put("DISK_SIDE", valueName);

                                   } else {
                                        exMap.put("DISK_SIDE", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("TRACK_NO", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() == 0) {
                                        failMap.put("TRACK_NO", valueName);

                                   } else {
                                        exMap.put("TRACK_NO", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if (valueName.length() > 8) {
                                   valueName = "0";
                              }
                              if (valueName.equals("없음")) {
                                   exMap.put("ALBUM_ISSU_YEAR", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8) {
                                        failMap.put("ALBUM_ISSU_YEAR", valueName);

                                   } else {
                                        exMap.put("ALBUM_ISSU_YEAR", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("LYRC", valueName);

                              } else {
                                   exMap.put("LYRC", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              // System.out.println( "COMP VALUE : "+ valueName);
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   // System.out.println( "COMP FAIL : "+ valueName);
                                   failMap.put("COMP", valueName);

                              } else {
                                   // System.out.println( "COMP EX : "+ valueName);
                                   exMap.put("COMP", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("ARRA", valueName);

                              } else {
                                   exMap.put("ARRA", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_12")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("TRAN", valueName);

                              } else {
                                   exMap.put("TRAN", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_13")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("SING", valueName);

                              } else {
                                   exMap.put("SING", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_14")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 1300 || valueName.length() == 0) {
                                   failMap.put("PERF", valueName);

                              } else {
                                   exMap.put("PERF", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_15")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("PROD", valueName);

                              } else {
                                   exMap.put("PROD", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_16")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_17")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("LICE_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() > 3 || columnCheck(valueName, createArrayList("1", "2", "3"))) {
                                        failMap.put("LICE_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("LICE_MGNT_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_18")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("PERF_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() > 3 || columnCheck(valueName, createArrayList("1", "2", "3"))) {
                                        failMap.put("PERF_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("PERF_MGNT_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_19")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("PROD_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 3 || columnCheck(valueName, createArrayList("1", "2", "3"))) {
                                        failMap.put("PROD_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("PROD_MGNT_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_20")) {
                              if (valueName == null || "".equals(valueName) || valueName.equals("없음")) {
                                   exMap.put("UCI", "0");
                              } else {
                                   if (valueName.length() > 200 || valueName.length() == 0) {
                                        failMap.put("UCI", valueName);

                                   } else {
                                        exMap.put("UCI", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_21")) {
                              if (valueName == null || "".equals(valueName) || valueName.equals("없음")) {
                                   exMap.put("RATIO", "0");
                              } else {
                                   if (valueName.length() > 200 || valueName.length() == 0) {
                                        failMap.put("RATIO", valueName);

                                   } else {
                                        exMap.put("RATIO", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }
               /* 반복문 끝 */
          } else if ("2".equals(genreCode)) { /* 어문 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    System.out.println("excelDataList의 행:" + excelDataList1.size());
                    System.out.println("excelDataList의 열:" + excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         System.out.println(keyName + " = " + valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);
                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("WORKS_TITLE", valueName);
                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_SUB_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_SUB_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("CRT_YEAR", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 4 || valueName.length() == 0) {
                                        failMap.put("CRT_YEAR", valueName);
                                   } else {
                                        exMap.put("CRT_YEAR", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("BOOK_TITLE", valueName);
                              } else {
                                   exMap.put("BOOK_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("BOOK_PUBLISHER", valueName);
                              } else {
                                   exMap.put("BOOK_PUBLISHER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("BOOK_ISSU_YEAR", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 4) {
                                        failMap.put("BOOK_ISSU_YEAR", valueName);
                                   } else {
                                        exMap.put("BOOK_ISSU_YEAR", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("COPT_NAME", valueName);
                              } else {
                                   exMap.put("COPT_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 4 || valueName.length() == 0
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);
                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    // System.out.println( "exMap.keySet() :" + exMap.keySet());
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println("failKey :::" + failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);
                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("3".equals(genreCode)) { /* 방송대본 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    System.out.println("총 갯수:::::::::::::::" + exMap.get("TOT_CNT"));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    System.out.println("excelDataList의 행:" + excelDataList1.size());
                    System.out.println("excelDataList의 열:" + excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);
                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 165) {
                                   failMap.put("WORKS_ORIG_TITLE", valueName);
                              } else {
                                   exMap.put("WORKS_ORIG_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 6 || valueName.length() == 0) {
                                        failMap.put("SCRT_GENRE_CD", valueName);
                                   } else {
                                        exMap.put("SCRT_GENRE_CD", valueName);
                                   }
                              } else {
                                   exMap.put("SCRT_GENRE_CD", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 200 || valueName.length() == 0) {
                                        failMap.put("SCRP_SUBJ_CD", valueName);
                                   } else {
                                        exMap.put("SCRP_SUBJ_CD", valueName);
                                   }
                              } else {
                                   exMap.put("SCRP_SUBJ_CD", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("ORIG_WRITER", valueName);
                              } else {
                                   exMap.put("ORIG_WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 130) {
                                   failMap.put("PLAYER", valueName);
                              } else {
                                   exMap.put("PLAYER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 10) {
                                        failMap.put("BROD_ORD_SEQ", valueName);

                                   } else {
                                        exMap.put("BROD_ORD_SEQ", valueName);
                                   }
                              } else {
                                   exMap.put("BROD_ORD_SEQ", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("BORD_DATE", valueName);
                                   } else {
                                        exMap.put("BORD_DATE", valueName);
                                   }
                              } else {
                                   exMap.put("BORD_DATE", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if (valueName.length() > 65 || "없음".equals(valueName)) {
                                   failMap.put("MAKE_CPY", valueName);

                              } else {
                                   exMap.put("MAKE_CPY", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("WRITER", valueName);

                              } else {
                                   exMap.put("WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_12")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("DIRECTOR", valueName);

                              } else {
                                   exMap.put("DIRECTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_13")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }

                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("4".equals(genreCode)) { /* 영화 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 4 || valueName.length() == 0) {
                                        failMap.put("CRT_YEAR", valueName);

                                   } else {
                                        exMap.put("CRT_YEAR", valueName);
                                   }
                              } else {
                                   exMap.put("CRT_YEAR", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 333 || valueName.length() == 0) {
                                   failMap.put("PLAYER", valueName);

                              } else {
                                   exMap.put("PLAYER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("DIRECT", valueName);

                              } else {
                                   exMap.put("DIRECT", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("WRITER", valueName);

                              } else {
                                   exMap.put("WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 130 || valueName.length() == 0) {
                                   failMap.put("DIRECTOR", valueName);

                              } else {
                                   exMap.put("DIRECTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("PRODUCER", valueName);

                              } else {
                                   exMap.put("PRODUCER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("DISTRIBUTOR", valueName);

                              } else {
                                   exMap.put("DISTRIBUTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("INVESTOR", valueName);

                              } else {
                                   exMap.put("INVESTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("5".equals(genreCode)) { /* 방송 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (valueName.equals("없음") || !"99".equals(valueName) && valueName.length() > 1) {
                                   exMap.put("BORD_GENRE_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 6 || valueName.length() == 0
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5", "6", "7", "99"))) {
                                        if (!"99".equals(valueName)) {
                                             failMap.put("BORD_GENRE_CD", valueName);
                                        }
                                   } else {
                                        exMap.put("BORD_GENRE_CD", valueName);
                                   }
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("MAKE_YEAR", valueName);

                                   } else {
                                        exMap.put("MAKE_YEAR", valueName);
                                   }
                              } else {
                                   exMap.put("MAKE_YEAR", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 10 || valueName.length() == 0) {
                                        failMap.put("BROD_ORD_SEQ", valueName);

                                   } else {
                                        exMap.put("BROD_ORD_SEQ", valueName);
                                   }
                              } else {
                                   exMap.put("BROD_ORD_SEQ", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("DIRECTOR", valueName);

                              } else {
                                   exMap.put("DIRECTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("MAKE_CPY", valueName);

                              } else {
                                   exMap.put("MAKE_CPY", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 130 || valueName.length() == 0) {
                                   failMap.put("PLAYER", valueName);

                              } else {
                                   exMap.put("PLAYER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65 || valueName.length() == 0) {
                                   failMap.put("WRITER", valueName);

                              } else {
                                   exMap.put("WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }


                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("6".equals(genreCode)) { /* 뉴스 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);
                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 10 || valueName.length() == 0) {
                                   if (!"99".equals(valueName)) {
                                        failMap.put("PAPE_NO", valueName);
                                   }
                              } else {
                                   exMap.put("PAPE_NO", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 33) {
                                   failMap.put("PAPE_KIND", valueName);

                              } else {
                                   exMap.put("PAPE_KIND", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("CONTRIBUTOR", valueName);

                              } else {
                                   exMap.put("CONTRIBUTOR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("REPOTER", valueName);

                              } else {
                                   exMap.put("REPOTER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 6) {
                                   failMap.put("REVISION_ID", valueName);

                              } else {
                                   exMap.put("REVISION_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("ARTICL_DATE", valueName);

                                   } else {
                                        exMap.put("ARTICL_DATE", valueName);
                                   }
                              } else {
                                   exMap.put("ARTICL_DATE", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null) {
                                   failMap.put("ARTICL_ISSU_DTTM", valueName);
                              } else {
                                   exMap.put("ARTICL_ISSU_DTTM", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("PROVIDER", valueName);
                              } else {
                                   exMap.put("PROVIDER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("7".equals(genreCode)) { /* 미술 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (!"".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 160) {
                                        failMap.put("WORKS_SUB_TITLE", valueName);
                                   } else {
                                        exMap.put("WORKS_SUB_TITLE", valueName);
                                   }
                              } else {
                                   exMap.put("WORKS_SUB_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 33) {
                                   failMap.put("KIND", valueName);

                              } else {
                                   exMap.put("KIND", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("MAKE_DATE", valueName);

                                   } else {
                                        exMap.put("MAKE_DATE", valueName);
                                   }
                              } else {
                                   exMap.put("MAKE_DATE", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("SOURCE_INFO", valueName);

                              } else {
                                   exMap.put("SOURCE_INFO", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 165) {
                                   failMap.put("SIZE_INFO", valueName);

                              } else {
                                   exMap.put("SIZE_INFO", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("RESOLUTION", valueName);

                              } else {
                                   exMap.put("RESOLUTION", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 165) {
                                   failMap.put("MAIN_MTRL", valueName);

                              } else {
                                   exMap.put("MAIN_MTRL", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("TXTR", valueName);

                              } else {
                                   exMap.put("TXTR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1000) {
                                        failMap.put("IMG_URL", valueName);

                                   } else {
                                        exMap.put("IMG_URL", valueName);
                                   }
                              } else {
                                   exMap.put("IMG_URL", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_12")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 333) {
                                        failMap.put("DETL_URL", valueName);

                                   } else {
                                        exMap.put("DETL_URL", valueName);
                                   }
                              } else {
                                   exMap.put("DETL_URL", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_13")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 333) {
                                   failMap.put("WRITER", valueName);

                              } else {
                                   exMap.put("WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_14")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("POSS_DATE", valueName);

                                   } else {
                                        exMap.put("POSS_DATE", valueName);
                                   }
                              } else {
                                   exMap.put("POSS_DATE", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_15")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("POSS_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("POSS_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_16")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                         // System.out.println( "art fail Map : " + failMap);
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("8".equals(genreCode)) { /* 이미지 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();

                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 160) {
                                   failMap.put("WORKS_TITLE", valueName);

                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 2500 || valueName.length() == 0) {
                                        failMap.put("IMAGE_DESC", valueName);
                                   } else {
                                        exMap.put("IMAGE_DESC", valueName);
                                   }
                              } else {
                                   exMap.put("IMAGE_DESC", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 650 || valueName.length() == 0) {
                                        failMap.put("KEYWORD", valueName);

                                   } else {
                                        exMap.put("KEYWORD", valueName);
                                   }
                              } else {
                                   exMap.put("KEYWORD", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                   failMap.put("CRT_YEAR", valueName);

                              } else {
                                   exMap.put("CRT_YEAR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 300 || valueName.length() == 0) {
                                   failMap.put("WRITER", valueName);

                              } else {
                                   exMap.put("WRITER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 180 || valueName.length() == 0) {
                                   failMap.put("PRODUCER", valueName);

                              } else {
                                   exMap.put("PRODUCER", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 180 || valueName.length() == 0) {
                                        failMap.put("IMAGE_URL", valueName);

                                   } else {
                                        exMap.put("IMAGE_URL", valueName);
                                   }
                              } else {
                                   exMap.put("IMAGE_URL", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_9")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 1 || !"Y".equals(valueName) && !"N".equals(valueName)) {
                                   failMap.put("IMAGE_OPEN_YN", valueName);

                              } else {
                                   exMap.put("IMAGE_OPEN_YN", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_10")) {
                              if (valueName == null || valueName.length() > 37) {
                                   failMap.put("ICNX_NUMB", valueName);

                              } else {
                                   exMap.put("ICNX_NUMB", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_11")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          } else if ("99".equals(genreCode)) { /* 기타 */
               /* 반복문 시작 */
               for (int i = 3; i < excelDataList1.size(); i++) {

                    exMap.put("TOT_CNT", excelDataList1.size() - 3);
                    // System.out.println("총 갯수:::::::::::::::"+exMap.get( "TOT_CNT" ));

                    Map<String, Object> excelDataList2 = excelDataList1.get(i);
                    // System.out.println("excelDataList의 행:"+excelDataList1.size());
                    // System.out.println("excelDataList의 열:"+excelDataList2.size());

                    exMap.put("genreCode", genreCode);
                    exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
                    exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
                    exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
                    exMap.put("reptChrrName", reptChrrName);
                    exMap.put("reptChrrPosi", reptChrrPosi);
                    exMap.put("reptChrrTelx", reptChrrTelx);
                    exMap.put("reptChrrMail", reptChrrMail);

                    excelDataList2.put("idx", String.valueOf(i));
                    Set key = excelDataList2.keySet();
                    for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                         String keyName = (String) iterator.next();
                         String valueName = (String) excelDataList2.get(keyName);
                         // System.out.println(keyName +" = " +valueName);

                         if (keyName.equals("0_" + i + "_0")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("RGST_ORGN_NAME", valueName);

                              } else {
                                   exMap.put("RGST_ORGN_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_1")) {
                              if ("없음".equals(valueName) || "".equals(valueName) || valueName == null || valueName.length() > 100) {
                                   failMap.put("COMM_WORKS_ID", valueName);

                              } else {
                                   exMap.put("COMM_WORKS_ID", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_2")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 6 || valueName.length() == 0
                                        || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5", "99"))) {
                                   failMap.put("SIDE_GENRE_CD", valueName);
                              } else {
                                   exMap.put("SIDE_GENRE_CD", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_3")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 160) {
                                        failMap.put("WORKS_TITLE", valueName);
                                   } else {
                                        exMap.put("WORKS_TITLE", valueName);
                                   }
                              } else {
                                   exMap.put("WORKS_TITLE", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_4")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("COPT_NAME", valueName);

                              } else {
                                   exMap.put("COPT_NAME", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_5")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 4 || valueName.length() == 0) {
                                        failMap.put("CRT_YEAR", valueName);

                                   } else {
                                        exMap.put("CRT_YEAR", valueName);
                                   }
                              } else {
                                   exMap.put("CRT_YEAR", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_6")) {
                              if ("".equals(valueName) || valueName == null || valueName.length() > 65) {
                                   failMap.put("PUBL_MEDI", valueName);

                              } else {
                                   exMap.put("PUBL_MEDI", valueName);
                              }
                         } else if (keyName.equals("0_" + i + "_7")) {
                              if (!"없음".equals(valueName)) {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 8 || valueName.length() == 0) {
                                        failMap.put("PUBL_DATE", valueName);

                                   } else {
                                        exMap.put("PUBL_DATE", valueName);
                                   }
                              } else {
                                   exMap.put("PUBL_DATE", "0");
                              }
                         } else if (keyName.equals("0_" + i + "_8")) {
                              if (valueName.equals("없음")) {
                                   exMap.put("COMM_MGNT_CD", "0");
                              } else {
                                   if ("".equals(valueName) || valueName == null || valueName.length() > 1 || valueName.length() == 0 || valueName.length() > 5
                                             || columnCheck(valueName, createArrayList("1", "2", "3", "4", "5"))) {
                                        failMap.put("COMM_MGNT_CD", valueName);

                                   } else {
                                        exMap.put("COMM_MGNT_CD", valueName);
                                   }
                              }
                         }
                    }
                    Set key2 = exMap.keySet();
                    int errCnt = 0;
                    Iterator iterator = key2.iterator();
                    while (iterator.hasNext()) {
                         Object exKey = iterator.next();
                         // System.out.println(exKey + ": " + exMap.get(exKey));
                    }

                    Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
                    while (keySetIteratorFail.hasNext()) {
                         String failKey = keySetIteratorFail.next();
                         // System.out.println(failKey + ": " + exMap.get(failKey));
                         errCnt++;
                    }

                    if (errCnt < 1) {
                         uploadList.add(exMap);

                         boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                         returnAjaxString(response, isSuccess);
                         exMap.clear();
                    } else {
                         uploadFailList.add(excelDataList2);
                         errCnt = 0;
                         failMap.clear();
                    }
               }

               /* 반복문 끝 */
          }


          // 보고연월
          GregorianCalendar d = new GregorianCalendar();
          String reptYYYYTmp = String.valueOf(d.get(Calendar.YEAR));
          String reptMMTmp = String.valueOf(((d.get(Calendar.MONTH) + 1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH) + 1) : String.valueOf(d.get(Calendar.MONTH) + 1));

          String reptYYYY = reptYYYYTmp.replaceAll(" ", "");
          String reptMM = reptMMTmp.replaceAll(" ", "");
          String commName = ConsoleLoginUser.getCommname();

          model.addAttribute("commName", commName);
          model.addAttribute("reptYYYY", reptYYYY);
          model.addAttribute("reptMM", reptMM);
          model.addAttribute("genreCode", genreCode);
          model.addAttribute("reptChrrName", reptChrrName);
          model.addAttribute("reptChrrPosi", reptChrrPosi);
          model.addAttribute("reptChrrTelx", reptChrrTelx);
          model.addAttribute("reptChrrMail", reptChrrMail);
          model.addAttribute("commName", commName);
          model.addAttribute("fileName", fileName);

          model.addAttribute("uploadList", uploadList);
          model.addAttribute("uploadFailList", uploadFailList);
          model.addAttribute("exMap", exMap);
          model.addAttribute("failMap", failMap);

          // System.out.println("uploadFailList::::::::::"+uploadFailList);
          if (uploadFailList.size() > 0) {
               return "rcept/fdcrAd17UploadForm2.tiles";
          } else {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = EgovWebUtil.getString(commandMap, "REPT_CHRR_NAME");
               String reptChrrMail2 = EgovWebUtil.getString(commandMap, "REPT_CHRR_MAIL");
               String COMM_NAME = EgovWebUtil.getString(commandMap, "COMM_NAME");
               String genreCode2 = EgovWebUtil.getString(commandMap, "GENRE_CODE");
               if (genreCode2.equals("1")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 12);
               } else if (genreCode2.equals("2")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("3")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("4")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("5")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("6")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("7")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("8")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               } else if (genreCode2.equals("99")) {
                    mailMap.put("TOT_CNT", excelDataList1.size() - 3);
               }
               mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               mailMap.put("COMM_NAME", COMM_NAME);

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List2.page");
          }
     }


     /*
      * @RequestMapping( value = "/console/rcept/fdcrAd17Upload2.page" ) public String fdcrAd17Upload2(
      * ModelMap model, Map<String, Object> commandMap, MultipartHttpServletRequest mreq,
      * HttpServletRequest request, HttpServletResponse response ) throws Exception{
      * 
      * //담을 arrayList ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
      * 
      * String uploadPath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty(
      * "file_upload_path" ) ); ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath,
      * null, true );
      * 
      * String file1 = uploadPath+fileinfo.get(0).get("F_fileName"); //String file1 =
      * EgovWebUtil.getString( commandMap, "file1" );
      * 
      * ArrayList<ArrayList<Map<String,Object>>> excelDataList = ExcelUtil.get(file1,
      * RETURN_TYPE_STRING); ArrayList<Map<String,Object>> excelDataList1 = excelDataList.get(0);
      * 
      * String genreCode = EgovWebUtil.getString( commandMap, "GENRE_CODE" );
      * 
      * if( "1".equals( genreCode ) ){ 음악 반복문 시작 for(int i=12; i<excelDataList1.size(); i++ ){
      * 
      * Map<String,Object> excelDataList2 = excelDataList1.get(i);
      * System.out.println("excelDataList의 행:"+excelDataList1.size());
      * System.out.println("excelDataList의 열:"+excelDataList2.size());
      * 
      * Set key = excelDataList2.keySet(); for (Iterator iterator = key.iterator(); iterator.hasNext();)
      * { String keyName = (String) iterator.next(); String valueName = (String)
      * excelDataList2.get(keyName); System.out.println(keyName +" = " +valueName); commandMap.put(
      * "valueName"+i, valueName ); } uploadList.add(excelDataList2); } 반복문 끝 }else if( "2".equals(
      * genreCode ) ){ 어문 반복문 시작 for(int i=3; i<excelDataList1.size(); i++ ){
      * 
      * Map<String,Object> excelDataList2 = excelDataList1.get(i);
      * System.out.println("excelDataList의 행:"+excelDataList1.size());
      * System.out.println("excelDataList의 열:"+excelDataList2.size());
      * 
      * Set key = excelDataList2.keySet(); for (Iterator iterator = key.iterator(); iterator.hasNext();)
      * { String keyName = (String) iterator.next(); String valueName = (String)
      * excelDataList2.get(keyName); System.out.println(keyName +" = " +valueName); commandMap.put(
      * "valueName"+i, valueName ); } uploadList.add(excelDataList2); } 반복문 끝 }else if( "3".equals(
      * genreCode ) ){ 방송대본 반복문 시작 for(int i=3; i<excelDataList1.size(); i++ ){
      * 
      * Map<String,Object> excelDataList2 = excelDataList1.get(i);
      * System.out.println("excelDataList의 행:"+excelDataList1.size());
      * System.out.println("excelDataList의 열:"+excelDataList2.size());
      * 
      * Set key = excelDataList2.keySet(); for (Iterator iterator = key.iterator(); iterator.hasNext();)
      * { String keyName = (String) iterator.next(); String valueName = (String)
      * excelDataList2.get(keyName); System.out.println(keyName +" = " +valueName); commandMap.put(
      * "valueName"+i, valueName ); } uploadList.add(excelDataList2); } 반복문 끝 }else if( "4".equals(
      * genreCode ) ){ 영화 반복문 시작 for(int i=3; i<excelDataList1.size(); i++ ){
      * 
      * Map<String,Object> excelDataList2 = excelDataList1.get(i);
      * System.out.println("excelDataList의 행:"+excelDataList1.size());
      * System.out.println("excelDataList의 열:"+excelDataList2.size());
      * 
      * Set key = excelDataList2.keySet(); for (Iterator iterator = key.iterator(); iterator.hasNext();)
      * { String keyName = (String) iterator.next(); String valueName = (String)
      * excelDataList2.get(keyName); System.out.println(keyName +" = " +valueName); commandMap.put(
      * "valueName"+i, valueName ); } uploadList.add(excelDataList2); } 반복문 끝 }else if( "5".equals(
      * genreCode ) ){ 방송 반복문 시작 for(int i=3; i<excelDataList1.size(); i++ ){
      * 
      * Map<String,Object> excelDataList2 = excelDataList1.get(i);
      * System.out.println("excelDataList의 행:"+excelDataList1.size());
      * System.out.println("excelDataList의 열:"+excelDataList2.size());
      * 
      * Set key = excelDataList2.keySet(); for (Iterator iterator = key.iterator(); iterator.hasNext();)
      * { String keyName = (String) iterator.next(); String valueName = (String)
      * excelDataList2.get(keyName); System.out.println(keyName +" = " +valueName); commandMap.put(
      * "valueName"+i, valueName ); } uploadList.add(excelDataList2); } 반복문 끝 }else if( "6".equals(
      * genreCode ) ){ 뉴스 반복문 시작 for(int i=3; i<excelDataList1.size(); i++ ){
      * 
      * Map<String,Object> excelDataList2 = excelDataList1.get(i);
      * System.out.println("excelDataList의 행:"+excelDataList1.size());
      * System.out.println("excelDataList의 열:"+excelDataList2.size());
      * 
      * Set key = excelDataList2.keySet(); for (Iterator iterator = key.iterator(); iterator.hasNext();)
      * { String keyName = (String) iterator.next(); String valueName = (String)
      * excelDataList2.get(keyName); System.out.println(keyName +" = " +valueName); commandMap.put(
      * "valueName"+i, valueName ); } uploadList.add(excelDataList2); } 반복문 끝 }else if( "7".equals(
      * genreCode ) ){ 미술 반복문 시작 for(int i=3; i<excelDataList1.size(); i++ ){
      * 
      * Map<String,Object> excelDataList2 = excelDataList1.get(i);
      * System.out.println("excelDataList의 행:"+excelDataList1.size());
      * System.out.println("excelDataList의 열:"+excelDataList2.size());
      * 
      * Set key = excelDataList2.keySet(); for (Iterator iterator = key.iterator(); iterator.hasNext();)
      * { String keyName = (String) iterator.next(); String valueName = (String)
      * excelDataList2.get(keyName); System.out.println(keyName +" = " +valueName); commandMap.put(
      * "valueName"+i, valueName ); } uploadList.add(excelDataList2); } 반복문 끝 }else if( "8".equals(
      * genreCode ) ){ 이미지 반복문 시작 for(int i=3; i<excelDataList1.size(); i++ ){
      * 
      * Map<String,Object> excelDataList2 = excelDataList1.get(i);
      * System.out.println("excelDataList의 행:"+excelDataList1.size());
      * System.out.println("excelDataList의 열:"+excelDataList2.size());
      * 
      * Set key = excelDataList2.keySet(); for (Iterator iterator = key.iterator(); iterator.hasNext();)
      * { String keyName = (String) iterator.next(); String valueName = (String)
      * excelDataList2.get(keyName); System.out.println(keyName +" = " +valueName); commandMap.put(
      * "valueName"+i, valueName ); } uploadList.add(excelDataList2); } 반복문 끝 }else if( "99".equals(
      * genreCode ) ){ 기타 반복문 시작 for(int i=3; i<excelDataList1.size(); i++ ){
      * 
      * Map<String,Object> excelDataList2 = excelDataList1.get(i);
      * System.out.println("excelDataList의 행:"+excelDataList1.size());
      * System.out.println("excelDataList의 열:"+excelDataList2.size());
      * 
      * Set key = excelDataList2.keySet(); for (Iterator iterator = key.iterator(); iterator.hasNext();)
      * { String keyName = (String) iterator.next(); String valueName = (String)
      * excelDataList2.get(keyName); System.out.println(keyName +" = " +valueName); commandMap.put(
      * "valueName"+i, valueName ); } uploadList.add(excelDataList2); } 반복문 끝 }
      * 
      * 
      * //보고연월 GregorianCalendar d = new GregorianCalendar(); String reptYYYYTmp =
      * String.valueOf(d.get(Calendar.YEAR)); String reptMMTmp =
      * String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
      * String.valueOf(d.get(Calendar.MONTH)+1));
      * 
      * String reptYYYY = reptYYYYTmp.replaceAll(" ", ""); String reptMM = reptMMTmp.replaceAll(" ", "");
      * String commName = ConsoleLoginUser.getCommname(); model.addAttribute("commName", commName);
      * model.addAttribute("reptYYYY", reptYYYY); model.addAttribute("reptMM", reptMM);
      * model.addAttribute("genreCode", genreCode);
      * 
      * model.addAttribute( "uploadList", uploadList ); return "rcept/fdcrAd17UploadForm2.tiles"; }
      */

     /**
      * 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17Insert1.page")
     public String fdcrAd17Insert1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 파라미터 셋팅
          // userConnectLogUtil.userConnectLog( request , "fdcrAd17Insert1");
          // System.out.println( "fdcrAd17Insert Call" );
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          String genreCd = request.getParameter("genreCd");
          // String genreCd = EgovWebUtil.getString( commandMap, "genreCd" );

          if (genreCd == "1" || genreCd.equals("1")) { // --music
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String NATION_CD = EgovWebUtil.getString(commandMap, "NATION_CD");
               String ALBUM_TITLE = EgovWebUtil.getString(commandMap, "ALBUM_TITLE");
               String ALBUM_LABLE = EgovWebUtil.getString(commandMap, "ALBUM_LABLE");
               String DISK_SIDE = EgovWebUtil.getString(commandMap, "DISK_SIDE");
               String TRACK_NO = EgovWebUtil.getString(commandMap, "TRACK_NO");
               String ALBUM_ISSU_YEAR = EgovWebUtil.getString(commandMap, "ALBUM_ISSU_YEAR");
               String LYRC = EgovWebUtil.getString(commandMap, "LYRC");
               String COMP = EgovWebUtil.getString(commandMap, "COMP");
               String ARRA = EgovWebUtil.getString(commandMap, "ARRA");
               String TRAN = EgovWebUtil.getString(commandMap, "TRAN");
               String SING = EgovWebUtil.getString(commandMap, "SING");
               String PERF = EgovWebUtil.getString(commandMap, "PERF");
               String PROD = EgovWebUtil.getString(commandMap, "PROD");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String LICE_MGNT_CD = EgovWebUtil.getString(commandMap, "LICE_MGNT_CD");
               String PERF_MGNT_CD = EgovWebUtil.getString(commandMap, "PERF_MGNT_CD");
               String PROD_MGNT_CD = EgovWebUtil.getString(commandMap, "PROD_MGNT_CD");
               String UCI = EgovWebUtil.getString(commandMap, "UCI");
               String RATIO = EgovWebUtil.getString(commandMap, "RATIO");
               String TOT_CNT = EgovWebUtil.getString(commandMap, "tot_cnt");
               // System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);

               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(NATION_CD)) {
                    NATION_CD = URLDecoder.decode(NATION_CD, "UTF-8");
                    commandMap.put("NATION_CD", NATION_CD);
               }
               if (!"".equals(ALBUM_TITLE)) {
                    ALBUM_TITLE = URLDecoder.decode(ALBUM_TITLE, "UTF-8");
                    commandMap.put("ALBUM_TITLE", ALBUM_TITLE);
               }
               if (!"".equals(ALBUM_LABLE)) {
                    ALBUM_LABLE = URLDecoder.decode(ALBUM_LABLE, "UTF-8");
                    commandMap.put("ALBUM_LABLE", ALBUM_LABLE);
               }
               if (!"".equals(DISK_SIDE)) {
                    DISK_SIDE = URLDecoder.decode(DISK_SIDE, "UTF-8");
                    commandMap.put("DISK_SIDE", DISK_SIDE);
               }
               if (!"".equals(TRACK_NO)) {
                    TRACK_NO = URLDecoder.decode(TRACK_NO, "UTF-8");
                    commandMap.put("TRACK_NO", TRACK_NO);
               }
               if (!"".equals(ALBUM_ISSU_YEAR)) {
                    ALBUM_ISSU_YEAR = URLDecoder.decode(ALBUM_ISSU_YEAR, "UTF-8");
                    commandMap.put("ALBUM_ISSU_YEAR", ALBUM_ISSU_YEAR);
               }
               if (!"".equals(LYRC)) {
                    LYRC = URLDecoder.decode(LYRC, "UTF-8");
                    commandMap.put("LYRC", LYRC);
               }
               if (!"".equals(COMP)) {
                    COMP = URLDecoder.decode(COMP, "UTF-8");
                    commandMap.put("COMP", COMP);
               }
               if (!"".equals(ARRA)) {
                    ARRA = URLDecoder.decode(ARRA, "UTF-8");
                    commandMap.put("ARRA", ARRA);
               }
               if (!"".equals(TRAN)) {
                    TRAN = URLDecoder.decode(TRAN, "UTF-8");
                    commandMap.put("TRAN", TRAN);
               }
               if (!"".equals(SING)) {
                    SING = URLDecoder.decode(SING, "UTF-8");
                    commandMap.put("SING", SING);
               }
               if (!"".equals(PERF)) {
                    PERF = URLDecoder.decode(PERF, "UTF-8");
                    commandMap.put("PERF", PERF);
               }
               if (!"".equals(PROD)) {
                    PROD = URLDecoder.decode(PROD, "UTF-8");
                    commandMap.put("PROD", PROD);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (!"".equals(LICE_MGNT_CD)) {
                    LICE_MGNT_CD = URLDecoder.decode(LICE_MGNT_CD, "UTF-8");
                    commandMap.put("LICE_MGNT_CD", LICE_MGNT_CD);
               }
               if (!"".equals(PERF_MGNT_CD)) {
                    PERF_MGNT_CD = URLDecoder.decode(PERF_MGNT_CD, "UTF-8");
                    commandMap.put("PERF_MGNT_CD", PERF_MGNT_CD);
               }
               if (!"".equals(PROD_MGNT_CD)) {
                    PROD_MGNT_CD = URLDecoder.decode(PROD_MGNT_CD, "UTF-8");
                    commandMap.put("PROD_MGNT_CD", PROD_MGNT_CD);
               }
               if (!"".equals(UCI)) {
                    UCI = URLDecoder.decode(UCI, "UTF-8");
                    commandMap.put("UCI", UCI);
               }
               if (!"".equals(RATIO)) {
                    RATIO = URLDecoder.decode(RATIO, "UTF-8");
                    commandMap.put("RATIO", RATIO);
               }

               if (NATION_CD == "없음" || "없음".equals(NATION_CD)) {
                    commandMap.put("NATION_CD", "0");
               }
               if (DISK_SIDE == "없음" || "없음".equals(DISK_SIDE)) {
                    commandMap.put("DISK_SIDE", "0");
               }
               if (TRACK_NO == "없음" || "없음".equals(TRACK_NO)) {
                    commandMap.put("TRACK_NO", "0");
               }
               if (ALBUM_ISSU_YEAR == "없음" || "없음".equals(ALBUM_ISSU_YEAR)) {
                    commandMap.put("ALBUM_ISSU_YEAR", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
               if (LICE_MGNT_CD == "없음" || "없음".equals(LICE_MGNT_CD)) {
                    commandMap.put("LICE_MGNT_CD", "0");
               }
               if (PERF_MGNT_CD == "없음" || "없음".equals(PERF_MGNT_CD)) {
                    commandMap.put("PERF_MGNT_CD", "0");
               }
               if (PROD_MGNT_CD == "없음" || "없음".equals(PROD_MGNT_CD)) {
                    commandMap.put("PROD_MGNT_CD", "0");
               }
               if (UCI == "없음" || "없음".equals(UCI)) {
                    commandMap.put("UCI", "0");
               }
               if (RATIO == "없음" || "없음".equals(RATIO)) {
                    commandMap.put("RATIO", "0");
               }
               if (!"".equals(TOT_CNT)) {
                    TOT_CNT = URLDecoder.decode(TOT_CNT, "UTF-8");
                    commandMap.put("TOT_CNT", TOT_CNT);
               }

          } else if (genreCd == "2" || genreCd.equals("2")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String NATION_CD = EgovWebUtil.getString(commandMap, "NATION_CD");
               String WORKS_SUB_TITLE = EgovWebUtil.getString(commandMap, "WORKS_SUB_TITLE");
               String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
               String BOOK_TITLE = EgovWebUtil.getString(commandMap, "BOOK_TITLE");
               String BOOK_PUBLISHER = EgovWebUtil.getString(commandMap, "BOOK_PUBLISHER");
               String BOOK_ISSU_YEAR = EgovWebUtil.getString(commandMap, "BOOK_ISSU_YEAR");
               String COPT_NAME = EgovWebUtil.getString(commandMap, "COPT_NAME");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String TOT_CNT = EgovWebUtil.getString(commandMap, "tot_cnt");
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(NATION_CD)) {
                    NATION_CD = URLDecoder.decode(NATION_CD, "UTF-8");
                    commandMap.put("NATION_CD", NATION_CD);
               }
               if (!"".equals(WORKS_SUB_TITLE)) {
                    WORKS_SUB_TITLE = URLDecoder.decode(WORKS_SUB_TITLE, "UTF-8");
                    commandMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
               }
               if (!"".equals(CRT_YEAR)) {
                    CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
                    commandMap.put("CRT_YEAR", CRT_YEAR);
               }
               if (!"".equals(BOOK_TITLE)) {
                    BOOK_TITLE = URLDecoder.decode(BOOK_TITLE, "UTF-8");
                    commandMap.put("BOOK_TITLE", BOOK_TITLE);
               }
               if (!"".equals(BOOK_PUBLISHER)) {
                    BOOK_PUBLISHER = URLDecoder.decode(BOOK_PUBLISHER, "UTF-8");
                    commandMap.put("BOOK_PUBLISHER", BOOK_PUBLISHER);
               }
               if (!"".equals(BOOK_ISSU_YEAR)) {
                    BOOK_ISSU_YEAR = URLDecoder.decode(BOOK_ISSU_YEAR, "UTF-8");
                    commandMap.put("BOOK_ISSU_YEAR", BOOK_ISSU_YEAR);
               }
               if (!"".equals(COPT_NAME)) {
                    COPT_NAME = URLDecoder.decode(COPT_NAME, "UTF-8");
                    commandMap.put("COPT_NAME", COPT_NAME);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (CRT_YEAR == "없음" || "없음".equals(CRT_YEAR)) {
                    commandMap.put("CRT_YEAR", "0");
               }
               if (BOOK_ISSU_YEAR == "없음" || "없음".equals(BOOK_ISSU_YEAR)) {
                    commandMap.put("BOOK_ISSU_YEAR", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
               if (!"".equals(TOT_CNT)) {
                    TOT_CNT = URLDecoder.decode(TOT_CNT, "UTF-8");
                    commandMap.put("TOT_CNT", TOT_CNT);
               }
          } else if (genreCd == "3" || genreCd.equals("3")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String WORKS_ORIG_TITLE = EgovWebUtil.getString(commandMap, "WORKS_ORIG_TITLE");
               String SCRT_GENRE_CD = EgovWebUtil.getString(commandMap, "SCRT_GENRE_CD");
               String SCRP_SUBJ_CD = EgovWebUtil.getString(commandMap, "SCRP_SUBJ_CD");
               String ORIG_WRITER = EgovWebUtil.getString(commandMap, "ORIG_WRITER");
               String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
               String BROD_ORD_SEQ = EgovWebUtil.getString(commandMap, "BROD_ORD_SEQ");
               String BORD_DATE = EgovWebUtil.getString(commandMap, "BORD_DATE");
               String MAKE_CPY = EgovWebUtil.getString(commandMap, "MAKE_CPY");
               String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
               String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String TOT_CNT = EgovWebUtil.getString(commandMap, "tot_cnt");
               System.out.println("TOT_CNT::::::::::::::::::::::::::::::::" + TOT_CNT);
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(WORKS_ORIG_TITLE)) {
                    WORKS_ORIG_TITLE = URLDecoder.decode(WORKS_ORIG_TITLE, "UTF-8");
                    commandMap.put("WORKS_ORIG_TITLE", WORKS_ORIG_TITLE);
               }
               if (!"".equals(SCRT_GENRE_CD)) {
                    SCRT_GENRE_CD = URLDecoder.decode(SCRT_GENRE_CD, "UTF-8");
                    commandMap.put("SCRT_GENRE_CD", SCRT_GENRE_CD);
               }
               if (!"".equals(SCRP_SUBJ_CD)) {
                    SCRP_SUBJ_CD = URLDecoder.decode(SCRP_SUBJ_CD, "UTF-8");
                    commandMap.put("SCRP_SUBJ_CD", SCRP_SUBJ_CD);
               }
               if (!"".equals(ORIG_WRITER)) {
                    ORIG_WRITER = URLDecoder.decode(ORIG_WRITER, "UTF-8");
                    commandMap.put("ORIG_WRITER", ORIG_WRITER);
               }
               if (!"".equals(PLAYER)) {
                    PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
                    commandMap.put("PLAYER", PLAYER);
               }
               if (!"".equals(BROD_ORD_SEQ)) {
                    BROD_ORD_SEQ = URLDecoder.decode(BROD_ORD_SEQ, "UTF-8");
                    commandMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
               }
               if (!"".equals(BORD_DATE)) {
                    BORD_DATE = URLDecoder.decode(BORD_DATE, "UTF-8");
                    commandMap.put("BORD_DATE", BORD_DATE);
               }
               if (!"".equals(MAKE_CPY)) {
                    MAKE_CPY = URLDecoder.decode(MAKE_CPY, "UTF-8");
                    commandMap.put("MAKE_CPY", MAKE_CPY);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(DIRECTOR)) {
                    DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
                    commandMap.put("DIRECTOR", DIRECTOR);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (SCRT_GENRE_CD == "없음" || "없음".equals(SCRT_GENRE_CD)) {
                    commandMap.put("SCRT_GENRE_CD", "0");
               }
               if (SCRP_SUBJ_CD == "없음" || "없음".equals(SCRP_SUBJ_CD)) {
                    commandMap.put("SCRP_SUBJ_CD", "0");
               }
               if (BROD_ORD_SEQ == "없음" || "없음".equals(BROD_ORD_SEQ)) {
                    commandMap.put("BROD_ORD_SEQ", "0");
               }
               if (BORD_DATE == "없음" || "없음".equals(BORD_DATE)) {
                    commandMap.put("BORD_DATE", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
               if (!"".equals(TOT_CNT)) {
                    TOT_CNT = URLDecoder.decode(TOT_CNT, "UTF-8");
                    commandMap.put("TOT_CNT", TOT_CNT);
               }
          } else if (genreCd == "4" || genreCd.equals("4")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
               String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
               String DIRECT = EgovWebUtil.getString(commandMap, "DIRECT");
               String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
               String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
               String PRODUCER = EgovWebUtil.getString(commandMap, "PRODUCER");
               String DISTRIBUTOR = EgovWebUtil.getString(commandMap, "DISTRIBUTOR");
               String INVESTOR = EgovWebUtil.getString(commandMap, "INVESTOR");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String TOT_CNT = EgovWebUtil.getString(commandMap, "tot_cnt");
               // System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(CRT_YEAR)) {
                    CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
                    commandMap.put("CRT_YEAR", CRT_YEAR);
               }
               if (!"".equals(PLAYER)) {
                    PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
                    commandMap.put("PLAYER", PLAYER);
               }
               if (!"".equals(DIRECT)) {
                    DIRECT = URLDecoder.decode(DIRECT, "UTF-8");
                    commandMap.put("DIRECT", DIRECT);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(DIRECTOR)) {
                    DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
                    commandMap.put("DIRECTOR", DIRECTOR);
               }
               if (!"".equals(PRODUCER)) {
                    PRODUCER = URLDecoder.decode(PRODUCER, "UTF-8");
                    commandMap.put("PRODUCER", PRODUCER);
               }
               if (!"".equals(DISTRIBUTOR)) {
                    DISTRIBUTOR = URLDecoder.decode(DISTRIBUTOR, "UTF-8");
                    commandMap.put("DISTRIBUTOR", DISTRIBUTOR);
               }
               if (!"".equals(INVESTOR)) {
                    INVESTOR = URLDecoder.decode(INVESTOR, "UTF-8");
                    commandMap.put("INVESTOR", INVESTOR);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (CRT_YEAR == "없음" || "없음".equals(CRT_YEAR)) {
                    commandMap.put("CRT_YEAR", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
               if (!"".equals(TOT_CNT)) {
                    TOT_CNT = URLDecoder.decode(TOT_CNT, "UTF-8");
                    commandMap.put("TOT_CNT", TOT_CNT);
               }
          } else if (genreCd == "5" || genreCd.equals("5")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String BORD_GENRE_CD = EgovWebUtil.getString(commandMap, "BORD_GENRE_CD");
               String MAKE_YEAR = EgovWebUtil.getString(commandMap, "MAKE_YEAR");
               String BROD_ORD_SEQ = EgovWebUtil.getString(commandMap, "BROD_ORD_SEQ");
               String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
               String MAKE_CPY = EgovWebUtil.getString(commandMap, "MAKE_CPY");
               String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
               String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String TOT_CNT = EgovWebUtil.getString(commandMap, "tot_cnt");
               // System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(BORD_GENRE_CD)) {
                    BORD_GENRE_CD = URLDecoder.decode(BORD_GENRE_CD, "UTF-8");
                    commandMap.put("BORD_GENRE_CD", BORD_GENRE_CD);
               }
               if (!"".equals(MAKE_YEAR)) {
                    MAKE_YEAR = URLDecoder.decode(MAKE_YEAR, "UTF-8");
                    commandMap.put("MAKE_YEAR", MAKE_YEAR);
               }
               if (!"".equals(BROD_ORD_SEQ)) {
                    BROD_ORD_SEQ = URLDecoder.decode(BROD_ORD_SEQ, "UTF-8");
                    commandMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
               }
               if (!"".equals(DIRECTOR)) {
                    DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
                    commandMap.put("DIRECTOR", DIRECTOR);
               }
               if (!"".equals(MAKE_CPY)) {
                    MAKE_CPY = URLDecoder.decode(MAKE_CPY, "UTF-8");
                    commandMap.put("MAKE_CPY", MAKE_CPY);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(PLAYER)) {
                    PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
                    commandMap.put("PLAYER", PLAYER);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (BORD_GENRE_CD == "없음" || "없음".equals(BORD_GENRE_CD)) {
                    commandMap.put("BORD_GENRE_CD", "0");
               }
               if (MAKE_YEAR == "없음" || "없음".equals(MAKE_YEAR)) {
                    commandMap.put("MAKE_YEAR", "0");
               }
               if (BROD_ORD_SEQ == "없음" || "없음".equals(BROD_ORD_SEQ)) {
                    commandMap.put("BROD_ORD_SEQ", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
               if (!"".equals(TOT_CNT)) {
                    TOT_CNT = URLDecoder.decode(TOT_CNT, "UTF-8");
                    commandMap.put("TOT_CNT", TOT_CNT);
               }
          } else if (genreCd == "6" || genreCd.equals("6")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String PAPE_NO = EgovWebUtil.getString(commandMap, "PAPE_NO");
               String PAPE_KIND = EgovWebUtil.getString(commandMap, "PAPE_KIND");
               String CONTRIBUTOR = EgovWebUtil.getString(commandMap, "CONTRIBUTOR");
               String REPOTER = EgovWebUtil.getString(commandMap, "REPOTER");
               String REVISION_ID = EgovWebUtil.getString(commandMap, "REVISION_ID");
               String ARTICL_DATE = EgovWebUtil.getString(commandMap, "ARTICL_DATE");
               String ARTICL_ISSU_DTTM = EgovWebUtil.getString(commandMap, "ARTICL_ISSU_DTTM");
               String PROVIDER = EgovWebUtil.getString(commandMap, "PROVIDER");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String TOT_CNT = EgovWebUtil.getString(commandMap, "tot_cnt");
               // System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(PAPE_NO)) {
                    PAPE_NO = URLDecoder.decode(PAPE_NO, "UTF-8");
                    commandMap.put("PAPE_NO", PAPE_NO);
               }
               if (!"".equals(PAPE_KIND)) {
                    PAPE_KIND = URLDecoder.decode(PAPE_KIND, "UTF-8");
                    commandMap.put("PAPE_KIND", PAPE_KIND);
               }
               if (!"".equals(CONTRIBUTOR)) {
                    CONTRIBUTOR = URLDecoder.decode(CONTRIBUTOR, "UTF-8");
                    commandMap.put("CONTRIBUTOR", CONTRIBUTOR);
               }
               if (!"".equals(REPOTER)) {
                    REPOTER = URLDecoder.decode(REPOTER, "UTF-8");
                    commandMap.put("REPOTER", REPOTER);
               }
               if (!"".equals(REVISION_ID)) {
                    REVISION_ID = URLDecoder.decode(REVISION_ID, "UTF-8");
                    commandMap.put("REVISION_ID", REVISION_ID);
               }
               if (!"".equals(ARTICL_DATE)) {
                    ARTICL_DATE = URLDecoder.decode(ARTICL_DATE, "UTF-8");
                    commandMap.put("ARTICL_DATE", ARTICL_DATE);
               }
               if (!"".equals(ARTICL_ISSU_DTTM)) {
                    ARTICL_ISSU_DTTM = URLDecoder.decode(ARTICL_ISSU_DTTM, "UTF-8");
                    commandMap.put("ARTICL_ISSU_DTTM", ARTICL_ISSU_DTTM);
               }
               if (!"".equals(PROVIDER)) {
                    PROVIDER = URLDecoder.decode(PROVIDER, "UTF-8");
                    commandMap.put("PROVIDER", PROVIDER);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (ARTICL_DATE == "없음" || "없음".equals(ARTICL_DATE)) {
                    commandMap.put("ARTICL_DATE", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
               if (!"".equals(TOT_CNT)) {
                    TOT_CNT = URLDecoder.decode(TOT_CNT, "UTF-8");
                    commandMap.put("TOT_CNT", TOT_CNT);
               }
          } else if (genreCd == "7" || genreCd.equals("7")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String WORKS_SUB_TITLE = EgovWebUtil.getString(commandMap, "WORKS_SUB_TITLE");
               String KIND = EgovWebUtil.getString(commandMap, "KIND");
               String MAKE_DATE = EgovWebUtil.getString(commandMap, "MAKE_DATE");
               String SOURCE_INFO = EgovWebUtil.getString(commandMap, "SOURCE_INFO");
               String SIZE_INFO = EgovWebUtil.getString(commandMap, "SIZE_INFO");
               String RESOLUTION = EgovWebUtil.getString(commandMap, "RESOLUTION");
               String MAIN_MTRL = EgovWebUtil.getString(commandMap, "MAIN_MTRL");
               String TXTR = EgovWebUtil.getString(commandMap, "TXTR");
               String IMG_URL = EgovWebUtil.getString(commandMap, "IMG_URL");
               String DETL_URL = EgovWebUtil.getString(commandMap, "DETL_URL");
               String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
               String POSS_DATE = EgovWebUtil.getString(commandMap, "POSS_DATE");
               String POSS_ORGN_NAME = EgovWebUtil.getString(commandMap, "POSS_ORGN_NAME");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String TOT_CNT = EgovWebUtil.getString(commandMap, "tot_cnt");
               // System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(WORKS_SUB_TITLE)) {
                    WORKS_SUB_TITLE = URLDecoder.decode(WORKS_SUB_TITLE, "UTF-8");
                    commandMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
               }
               if (!"".equals(KIND)) {
                    KIND = URLDecoder.decode(KIND, "UTF-8");
                    commandMap.put("KIND", KIND);
               }
               if (!"".equals(MAKE_DATE)) {
                    MAKE_DATE = URLDecoder.decode(MAKE_DATE, "UTF-8");
                    commandMap.put("MAKE_DATE", MAKE_DATE);
               }
               if (!"".equals(SOURCE_INFO)) {
                    SOURCE_INFO = URLDecoder.decode(SOURCE_INFO, "UTF-8");
                    commandMap.put("SOURCE_INFO", SOURCE_INFO);
               }
               if (!"".equals(SIZE_INFO)) {
                    SIZE_INFO = URLDecoder.decode(SIZE_INFO, "UTF-8");
                    commandMap.put("SIZE_INFO", SIZE_INFO);
               }
               if (!"".equals(RESOLUTION)) {
                    RESOLUTION = URLDecoder.decode(RESOLUTION, "UTF-8");
                    commandMap.put("RESOLUTION", RESOLUTION);
               }
               if (!"".equals(MAIN_MTRL)) {
                    MAIN_MTRL = URLDecoder.decode(MAIN_MTRL, "UTF-8");
                    commandMap.put("MAIN_MTRL", MAIN_MTRL);
               }
               if (!"".equals(TXTR)) {
                    TXTR = URLDecoder.decode(TXTR, "UTF-8");
                    commandMap.put("TXTR", TXTR);
               }
               if (!"".equals(IMG_URL)) {
                    IMG_URL = URLDecoder.decode(IMG_URL, "UTF-8");
                    commandMap.put("IMG_URL", IMG_URL);
               }
               if (!"".equals(DETL_URL)) {
                    DETL_URL = URLDecoder.decode(DETL_URL, "UTF-8");
                    commandMap.put("DETL_URL", DETL_URL);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(POSS_DATE)) {
                    POSS_DATE = URLDecoder.decode(POSS_DATE, "UTF-8");
                    commandMap.put("POSS_DATE", POSS_DATE);
               }
               if (!"".equals(POSS_ORGN_NAME)) {
                    POSS_ORGN_NAME = URLDecoder.decode(POSS_ORGN_NAME, "UTF-8");
                    commandMap.put("POSS_ORGN_NAME", POSS_ORGN_NAME);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (MAKE_DATE == "없음" || "없음".equals(MAKE_DATE)) {
                    commandMap.put("MAKE_DATE", "0");
               }
               if (IMG_URL == "없음" || "없음".equals(IMG_URL)) {
                    commandMap.put("IMG_URL", "0");
               }
               if (DETL_URL == "없음" || "없음".equals(DETL_URL)) {
                    commandMap.put("DETL_URL", "0");
               }
               if (POSS_DATE == "없음" || "없음".equals(POSS_DATE)) {
                    commandMap.put("POSS_DATE", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
               if (!"".equals(TOT_CNT)) {
                    TOT_CNT = URLDecoder.decode(TOT_CNT, "UTF-8");
                    commandMap.put("TOT_CNT", TOT_CNT);
               }
          } else if (genreCd == "8" || genreCd.equals("8")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String IMAGE_DESC = EgovWebUtil.getString(commandMap, "IMAGE_DESC");
               String KEYWORD = EgovWebUtil.getString(commandMap, "KEYWORD");
               String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
               String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
               String PRODUCER = EgovWebUtil.getString(commandMap, "PRODUCER");
               String IMAGE_URL = EgovWebUtil.getString(commandMap, "IMAGE_URL");
               String IMAGE_OPEN_YN = EgovWebUtil.getString(commandMap, "IMAGE_OPEN_YN");
               String ICNX_NUMB = EgovWebUtil.getString(commandMap, "ICNX_NUMB");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String TOT_CNT = EgovWebUtil.getString(commandMap, "tot_cnt");
               // System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(IMAGE_DESC)) {
                    IMAGE_DESC = URLDecoder.decode(IMAGE_DESC, "UTF-8");
                    commandMap.put("IMAGE_DESC", IMAGE_DESC);
               }
               if (!"".equals(KEYWORD)) {
                    KEYWORD = URLDecoder.decode(KEYWORD, "UTF-8");
                    commandMap.put("KEYWORD", KEYWORD);
               }
               if (!"".equals(CRT_YEAR)) {
                    CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
                    commandMap.put("CRT_YEAR", CRT_YEAR);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(PRODUCER)) {
                    PRODUCER = URLDecoder.decode(PRODUCER, "UTF-8");
                    commandMap.put("PRODUCER", PRODUCER);
               }
               if (!"".equals(IMAGE_URL)) {
                    IMAGE_URL = URLDecoder.decode(IMAGE_URL, "UTF-8");
                    commandMap.put("IMAGE_URL", IMAGE_URL);
               }
               if (!"".equals(IMAGE_OPEN_YN)) {
                    IMAGE_OPEN_YN = URLDecoder.decode(IMAGE_OPEN_YN, "UTF-8");
                    commandMap.put("IMAGE_OPEN_YN", IMAGE_OPEN_YN);
               }
               if (!"".equals(ICNX_NUMB)) {
                    ICNX_NUMB = URLDecoder.decode(ICNX_NUMB, "UTF-8");
                    commandMap.put("ICNX_NUMB", ICNX_NUMB);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (IMAGE_DESC == "없음" || "없음".equals(IMAGE_DESC)) {
                    commandMap.put("IMAGE_DESC", "0");
               }
               if (KEYWORD == "없음" || "없음".equals(KEYWORD)) {
                    commandMap.put("KEYWORD", "0");
               }
               if (IMAGE_URL == "없음" || "없음".equals(IMAGE_URL)) {
                    commandMap.put("IMAGE_URL", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
               if (!"".equals(TOT_CNT)) {
                    TOT_CNT = URLDecoder.decode(TOT_CNT, "UTF-8");
                    commandMap.put("TOT_CNT", TOT_CNT);
               }
          } else if (genreCd == "99" || genreCd.equals("99")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String SIDE_GENRE_CD = EgovWebUtil.getString(commandMap, "SIDE_GENRE_CD");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String COPT_NAME = EgovWebUtil.getString(commandMap, "COPT_NAME");
               String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
               String PUBL_MEDI = EgovWebUtil.getString(commandMap, "PUBL_MEDI");
               String PUBL_DATE = EgovWebUtil.getString(commandMap, "PUBL_DATE");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String TOT_CNT = EgovWebUtil.getString(commandMap, "tot_cnt");
               // System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(SIDE_GENRE_CD)) {
                    SIDE_GENRE_CD = URLDecoder.decode(SIDE_GENRE_CD, "UTF-8");
                    commandMap.put("SIDE_GENRE_CD", SIDE_GENRE_CD);
               }
               if (!"".equals(COPT_NAME)) {
                    COPT_NAME = URLDecoder.decode(COPT_NAME, "UTF-8");
                    commandMap.put("COPT_NAME", COPT_NAME);
               }
               if (!"".equals(CRT_YEAR)) {
                    CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
                    commandMap.put("CRT_YEAR", CRT_YEAR);
               }
               if (!"".equals(PUBL_MEDI)) {
                    PUBL_MEDI = URLDecoder.decode(PUBL_MEDI, "UTF-8");
                    commandMap.put("PUBL_MEDI", PUBL_MEDI);
               }
               if (!"".equals(PUBL_DATE)) {
                    PUBL_DATE = URLDecoder.decode(PUBL_DATE, "UTF-8");
                    commandMap.put("PUBL_DATE", PUBL_DATE);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (PUBL_DATE == "없음" || "없음".equals(PUBL_DATE)) {
                    commandMap.put("PUBL_DATE", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
               if (!"".equals(TOT_CNT)) {
                    TOT_CNT = URLDecoder.decode(TOT_CNT, "UTF-8");
                    commandMap.put("TOT_CNT", TOT_CNT);
               }
          }

          ArrayList<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
          excelList.add(commandMap);
          boolean isSuccess = fdcrAd17Service.fdcrAd17Insert1(commandMap, excelList);
          returnAjaxString(response, isSuccess);

          if (isSuccess) {
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 일괄수정
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17Update1.page")
     public String fdcrAd17Update1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          String GENRE_CODE = EgovWebUtil.getString(commandMap, "GENRE_CD");

          if (GENRE_CODE == "1" || GENRE_CODE.equals("1")) { // --music
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String NATION_CD = EgovWebUtil.getString(commandMap, "NATION_CD");
               String ALBUM_TITLE = EgovWebUtil.getString(commandMap, "ALBUM_TITLE");
               String ALBUM_LABLE = EgovWebUtil.getString(commandMap, "ALBUM_LABLE");
               String DISK_SIDE = EgovWebUtil.getString(commandMap, "DISK_SIDE");
               String TRACK_NO = EgovWebUtil.getString(commandMap, "TRACK_NO");
               String ALBUM_ISSU_YEAR = EgovWebUtil.getString(commandMap, "ALBUM_ISSU_YEAR");
               String LYRC = EgovWebUtil.getString(commandMap, "LYRC");
               String COMP = EgovWebUtil.getString(commandMap, "COMP");
               String ARRA = EgovWebUtil.getString(commandMap, "ARRA");
               String TRAN = EgovWebUtil.getString(commandMap, "TRAN");
               String SING = EgovWebUtil.getString(commandMap, "SING");
               String PERF = EgovWebUtil.getString(commandMap, "PERF");
               String PROD = EgovWebUtil.getString(commandMap, "PROD");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String LICE_MGNT_CD = EgovWebUtil.getString(commandMap, "LICE_MGNT_CD");
               String PERF_MGNT_CD = EgovWebUtil.getString(commandMap, "PERF_MGNT_CD");
               String PROD_MGNT_CD = EgovWebUtil.getString(commandMap, "PROD_MGNT_CD");
               String UCI = EgovWebUtil.getString(commandMap, "UCI");
               String RATIO = EgovWebUtil.getString(commandMap, "RATIO");
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(NATION_CD)) {
                    NATION_CD = URLDecoder.decode(NATION_CD, "UTF-8");
                    commandMap.put("NATION_CD", NATION_CD);
               }
               if (!"".equals(ALBUM_TITLE)) {
                    ALBUM_TITLE = URLDecoder.decode(ALBUM_TITLE, "UTF-8");
                    commandMap.put("ALBUM_TITLE", ALBUM_TITLE);
               }
               if (!"".equals(ALBUM_LABLE)) {
                    ALBUM_LABLE = URLDecoder.decode(ALBUM_LABLE, "UTF-8");
                    commandMap.put("ALBUM_LABLE", ALBUM_LABLE);
               }
               if (!"".equals(DISK_SIDE)) {
                    DISK_SIDE = URLDecoder.decode(DISK_SIDE, "UTF-8");
                    commandMap.put("DISK_SIDE", DISK_SIDE);
               }
               if (!"".equals(TRACK_NO)) {
                    TRACK_NO = URLDecoder.decode(TRACK_NO, "UTF-8");
                    commandMap.put("TRACK_NO", TRACK_NO);
               }
               if (!"".equals(ALBUM_ISSU_YEAR)) {
                    ALBUM_ISSU_YEAR = URLDecoder.decode(ALBUM_ISSU_YEAR, "UTF-8");
                    commandMap.put("ALBUM_ISSU_YEAR", ALBUM_ISSU_YEAR);
               }
               if (!"".equals(LYRC)) {
                    LYRC = URLDecoder.decode(LYRC, "UTF-8");
                    commandMap.put("LYRC", LYRC);
               }
               if (!"".equals(COMP)) {
                    COMP = URLDecoder.decode(COMP, "UTF-8");
                    commandMap.put("COMP", COMP);
               }
               if (!"".equals(ARRA)) {
                    ARRA = URLDecoder.decode(ARRA, "UTF-8");
                    commandMap.put("ARRA", ARRA);
               }
               if (!"".equals(TRAN)) {
                    TRAN = URLDecoder.decode(TRAN, "UTF-8");
                    commandMap.put("TRAN", TRAN);
               }
               if (!"".equals(SING)) {
                    SING = URLDecoder.decode(SING, "UTF-8");
                    commandMap.put("SING", SING);
               }
               if (!"".equals(PERF)) {
                    PERF = URLDecoder.decode(PERF, "UTF-8");
                    commandMap.put("PERF", PERF);
               }
               if (!"".equals(PROD)) {
                    PROD = URLDecoder.decode(PROD, "UTF-8");
                    commandMap.put("PROD", PROD);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (!"".equals(LICE_MGNT_CD)) {
                    LICE_MGNT_CD = URLDecoder.decode(LICE_MGNT_CD, "UTF-8");
                    commandMap.put("LICE_MGNT_CD", LICE_MGNT_CD);
               }
               if (!"".equals(PERF_MGNT_CD)) {
                    PERF_MGNT_CD = URLDecoder.decode(PERF_MGNT_CD, "UTF-8");
                    commandMap.put("PERF_MGNT_CD", PERF_MGNT_CD);
               }
               if (!"".equals(PROD_MGNT_CD)) {
                    PROD_MGNT_CD = URLDecoder.decode(PROD_MGNT_CD, "UTF-8");
                    commandMap.put("PROD_MGNT_CD", PROD_MGNT_CD);
               }
               if (!"".equals(UCI)) {
                    UCI = URLDecoder.decode(UCI, "UTF-8");
                    commandMap.put("UCI", UCI);
               }
               if (!"".equals(RATIO)) {
                    RATIO = URLDecoder.decode(RATIO, "UTF-8");
                    commandMap.put("RATIO", RATIO);
               }
               if (NATION_CD == "없음" || "없음".equals(NATION_CD)) {
                    commandMap.put("NATION_CD", "0");
               }
               if (DISK_SIDE == "없음" || "없음".equals(DISK_SIDE)) {
                    commandMap.put("DISK_SIDE", "0");
               }
               if (TRACK_NO == "없음" || "없음".equals(TRACK_NO)) {
                    commandMap.put("TRACK_NO", "0");
               }
               if (ALBUM_ISSU_YEAR == "없음" || "없음".equals(ALBUM_ISSU_YEAR)) {
                    commandMap.put("ALBUM_ISSU_YEAR", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
               if (LICE_MGNT_CD == "없음" || "없음".equals(LICE_MGNT_CD)) {
                    commandMap.put("LICE_MGNT_CD", "0");
               }
               if (PERF_MGNT_CD == "없음" || "없음".equals(PERF_MGNT_CD)) {
                    commandMap.put("PERF_MGNT_CD", "0");
               }
               if (PROD_MGNT_CD == "없음" || "없음".equals(PROD_MGNT_CD)) {
                    commandMap.put("PROD_MGNT_CD", "0");
               }
               if (UCI == "없음" || "없음".equals(UCI)) {
                    commandMap.put("UCI", "0");
               }
               if (RATIO == "없음" || "없음".equals(RATIO)) {
                    commandMap.put("RATIO", "0");
               }
          } else if (GENRE_CODE == "2" || GENRE_CODE.equals("2")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String NATION_CD = EgovWebUtil.getString(commandMap, "NATION_CD");
               String WORKS_SUB_TITLE = EgovWebUtil.getString(commandMap, "WORKS_SUB_TITLE");
               String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
               String BOOK_TITLE = EgovWebUtil.getString(commandMap, "BOOK_TITLE");
               String BOOK_PUBLISHER = EgovWebUtil.getString(commandMap, "BOOK_PUBLISHER");
               String BOOK_ISSU_YEAR = EgovWebUtil.getString(commandMap, "BOOK_ISSU_YEAR");
               String COPT_NAME = EgovWebUtil.getString(commandMap, "COPT_NAME");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(NATION_CD)) {
                    NATION_CD = URLDecoder.decode(NATION_CD, "UTF-8");
                    commandMap.put("NATION_CD", NATION_CD);
               }
               if (!"".equals(WORKS_SUB_TITLE)) {
                    WORKS_SUB_TITLE = URLDecoder.decode(WORKS_SUB_TITLE, "UTF-8");
                    commandMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
               }
               if (!"".equals(CRT_YEAR)) {
                    CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
                    commandMap.put("CRT_YEAR", CRT_YEAR);
               }
               if (!"".equals(BOOK_TITLE)) {
                    BOOK_TITLE = URLDecoder.decode(BOOK_TITLE, "UTF-8");
                    commandMap.put("BOOK_TITLE", BOOK_TITLE);
               }
               if (!"".equals(BOOK_PUBLISHER)) {
                    BOOK_PUBLISHER = URLDecoder.decode(BOOK_PUBLISHER, "UTF-8");
                    commandMap.put("BOOK_PUBLISHER", BOOK_PUBLISHER);
               }
               if (!"".equals(BOOK_ISSU_YEAR)) {
                    BOOK_ISSU_YEAR = URLDecoder.decode(BOOK_ISSU_YEAR, "UTF-8");
                    commandMap.put("BOOK_ISSU_YEAR", BOOK_ISSU_YEAR);
               }
               if (!"".equals(COPT_NAME)) {
                    COPT_NAME = URLDecoder.decode(COPT_NAME, "UTF-8");
                    commandMap.put("COPT_NAME", COPT_NAME);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMP", COMM_MGNT_CD);
               }
               if (CRT_YEAR == "없음" || "없음".equals(CRT_YEAR)) {
                    commandMap.put("CRT_YEAR", "0");
               }
               if (BOOK_ISSU_YEAR == "없음" || "없음".equals(BOOK_ISSU_YEAR)) {
                    commandMap.put("BOOK_ISSU_YEAR", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
          } else if (GENRE_CODE == "3" || GENRE_CODE.equals("3")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String WORKS_ORIG_TITLE = EgovWebUtil.getString(commandMap, "WORKS_ORIG_TITLE");
               String SCRT_GENRE_CD = EgovWebUtil.getString(commandMap, "SCRT_GENRE_CD");
               String SCRP_SUBJ_CD = EgovWebUtil.getString(commandMap, "SCRP_SUBJ_CD");
               String ORIG_WRITER = EgovWebUtil.getString(commandMap, "ORIG_WRITER");
               String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
               String BROD_ORD_SEQ = EgovWebUtil.getString(commandMap, "BROD_ORD_SEQ");
               String BORD_DATE = EgovWebUtil.getString(commandMap, "BORD_DATE");
               String MAKE_CPY = EgovWebUtil.getString(commandMap, "MAKE_CPY");
               String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
               String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(WORKS_ORIG_TITLE)) {
                    WORKS_ORIG_TITLE = URLDecoder.decode(WORKS_ORIG_TITLE, "UTF-8");
                    commandMap.put("WORKS_ORIG_TITLE", WORKS_ORIG_TITLE);
               }
               if (!"".equals(SCRT_GENRE_CD)) {
                    SCRT_GENRE_CD = URLDecoder.decode(SCRT_GENRE_CD, "UTF-8");
                    commandMap.put("SCRT_GENRE_CD", SCRT_GENRE_CD);
               }
               if (!"".equals(SCRP_SUBJ_CD)) {
                    SCRP_SUBJ_CD = URLDecoder.decode(SCRP_SUBJ_CD, "UTF-8");
                    commandMap.put("SCRP_SUBJ_CD", SCRP_SUBJ_CD);
               }
               if (!"".equals(ORIG_WRITER)) {
                    ORIG_WRITER = URLDecoder.decode(ORIG_WRITER, "UTF-8");
                    commandMap.put("ORIG_WRITER", ORIG_WRITER);
               }
               if (!"".equals(PLAYER)) {
                    PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
                    commandMap.put("PLAYER", PLAYER);
               }
               if (!"".equals(BROD_ORD_SEQ)) {
                    BROD_ORD_SEQ = URLDecoder.decode(BROD_ORD_SEQ, "UTF-8");
                    commandMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
               }
               if (!"".equals(BORD_DATE)) {
                    BORD_DATE = URLDecoder.decode(BORD_DATE, "UTF-8");
                    commandMap.put("BORD_DATE", BORD_DATE);
               }
               if (!"".equals(MAKE_CPY)) {
                    MAKE_CPY = URLDecoder.decode(MAKE_CPY, "UTF-8");
                    commandMap.put("MAKE_CPY", MAKE_CPY);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(DIRECTOR)) {
                    DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
                    commandMap.put("DIRECTOR", DIRECTOR);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (SCRT_GENRE_CD == "없음" || "없음".equals(SCRT_GENRE_CD)) {
                    commandMap.put("SCRT_GENRE_CD", "0");
               }
               if (SCRP_SUBJ_CD == "없음" || "없음".equals(SCRP_SUBJ_CD)) {
                    commandMap.put("SCRP_SUBJ_CD", "0");
               }
               if (BROD_ORD_SEQ == "없음" || "없음".equals(BROD_ORD_SEQ)) {
                    commandMap.put("BROD_ORD_SEQ", "0");
               }
               if (BORD_DATE == "없음" || "없음".equals(BORD_DATE)) {
                    commandMap.put("BORD_DATE", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
          } else if (GENRE_CODE == "4" || GENRE_CODE.equals("4")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
               String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
               String DIRECT = EgovWebUtil.getString(commandMap, "DIRECT");
               String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
               String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
               String PRODUCER = EgovWebUtil.getString(commandMap, "PRODUCER");
               String DISTRIBUTOR = EgovWebUtil.getString(commandMap, "DISTRIBUTOR");
               String INVESTOR = EgovWebUtil.getString(commandMap, "INVESTOR");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(CRT_YEAR)) {
                    CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
                    commandMap.put("CRT_YEAR", CRT_YEAR);
               }
               if (!"".equals(PLAYER)) {
                    PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
                    commandMap.put("PLAYER", PLAYER);
               }
               if (!"".equals(DIRECT)) {
                    DIRECT = URLDecoder.decode(DIRECT, "UTF-8");
                    commandMap.put("DIRECT", DIRECT);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(DIRECTOR)) {
                    DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
                    commandMap.put("DIRECTOR", DIRECTOR);
               }
               if (!"".equals(PRODUCER)) {
                    PRODUCER = URLDecoder.decode(PRODUCER, "UTF-8");
                    commandMap.put("PRODUCER", PRODUCER);
               }
               if (!"".equals(DISTRIBUTOR)) {
                    DISTRIBUTOR = URLDecoder.decode(DISTRIBUTOR, "UTF-8");
                    commandMap.put("DISTRIBUTOR", DISTRIBUTOR);
               }
               if (!"".equals(INVESTOR)) {
                    INVESTOR = URLDecoder.decode(INVESTOR, "UTF-8");
                    commandMap.put("INVESTOR", INVESTOR);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (CRT_YEAR == "없음" || "없음".equals(CRT_YEAR)) {
                    commandMap.put("CRT_YEAR", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
          } else if (GENRE_CODE == "5" || GENRE_CODE.equals("5")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String BORD_GENRE_CD = EgovWebUtil.getString(commandMap, "BORD_GENRE_CD");
               String MAKE_YEAR = EgovWebUtil.getString(commandMap, "MAKE_YEAR");
               String BROD_ORD_SEQ = EgovWebUtil.getString(commandMap, "BROD_ORD_SEQ");
               String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
               String MAKE_CPY = EgovWebUtil.getString(commandMap, "MAKE_CPY");
               String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
               String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(BORD_GENRE_CD)) {
                    BORD_GENRE_CD = URLDecoder.decode(BORD_GENRE_CD, "UTF-8");
                    commandMap.put("BORD_GENRE_CD", BORD_GENRE_CD);
               }
               if (!"".equals(MAKE_YEAR)) {
                    MAKE_YEAR = URLDecoder.decode(MAKE_YEAR, "UTF-8");
                    commandMap.put("MAKE_YEAR", MAKE_YEAR);
               }
               if (!"".equals(BROD_ORD_SEQ)) {
                    BROD_ORD_SEQ = URLDecoder.decode(BROD_ORD_SEQ, "UTF-8");
                    commandMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
               }
               if (!"".equals(DIRECTOR)) {
                    DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
                    commandMap.put("DIRECTOR", DIRECTOR);
               }
               if (!"".equals(MAKE_CPY)) {
                    MAKE_CPY = URLDecoder.decode(MAKE_CPY, "UTF-8");
                    commandMap.put("MAKE_CPY", MAKE_CPY);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(PLAYER)) {
                    PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
                    commandMap.put("PLAYER", PLAYER);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (BORD_GENRE_CD == "없음" || "없음".equals(BORD_GENRE_CD)) {
                    commandMap.put("BORD_GENRE_CD", "0");
               }
               if (MAKE_YEAR == "없음" || "없음".equals(MAKE_YEAR)) {
                    commandMap.put("MAKE_YEAR", "0");
               }
               if (BROD_ORD_SEQ == "없음" || "없음".equals(BROD_ORD_SEQ)) {
                    commandMap.put("BROD_ORD_SEQ", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
          } else if (GENRE_CODE == "6" || GENRE_CODE.equals("6")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String PAPE_NO = EgovWebUtil.getString(commandMap, "PAPE_NO");
               String PAPE_KIND = EgovWebUtil.getString(commandMap, "PAPE_KIND");
               String CONTRIBUTOR = EgovWebUtil.getString(commandMap, "CONTRIBUTOR");
               String REPOTER = EgovWebUtil.getString(commandMap, "REPOTER");
               String REVISION_ID = EgovWebUtil.getString(commandMap, "REVISION_ID");
               String ARTICL_DATE = EgovWebUtil.getString(commandMap, "ARTICL_DATE");
               String ARTICL_ISSU_DTTM = EgovWebUtil.getString(commandMap, "ARTICL_ISSU_DTTM");
               String PROVIDER = EgovWebUtil.getString(commandMap, "PROVIDER");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(PAPE_NO)) {
                    PAPE_NO = URLDecoder.decode(PAPE_NO, "UTF-8");
                    commandMap.put("PAPE_NO", PAPE_NO);
               }
               if (!"".equals(PAPE_KIND)) {
                    PAPE_KIND = URLDecoder.decode(PAPE_KIND, "UTF-8");
                    commandMap.put("PAPE_KIND", PAPE_KIND);
               }
               if (!"".equals(CONTRIBUTOR)) {
                    CONTRIBUTOR = URLDecoder.decode(CONTRIBUTOR, "UTF-8");
                    commandMap.put("CONTRIBUTOR", CONTRIBUTOR);
               }
               if (!"".equals(REPOTER)) {
                    REPOTER = URLDecoder.decode(REPOTER, "UTF-8");
                    commandMap.put("REPOTER", REPOTER);
               }
               if (!"".equals(REVISION_ID)) {
                    REVISION_ID = URLDecoder.decode(REVISION_ID, "UTF-8");
                    commandMap.put("REVISION_ID", REVISION_ID);
               }
               if (!"".equals(ARTICL_DATE)) {
                    ARTICL_DATE = URLDecoder.decode(ARTICL_DATE, "UTF-8");
                    commandMap.put("ARTICL_DATE", ARTICL_DATE);
               }
               if (!"".equals(ARTICL_ISSU_DTTM)) {
                    ARTICL_ISSU_DTTM = URLDecoder.decode(ARTICL_ISSU_DTTM, "UTF-8");
                    commandMap.put("ARTICL_ISSU_DTTM", ARTICL_ISSU_DTTM);
               }
               if (!"".equals(PROVIDER)) {
                    PROVIDER = URLDecoder.decode(PROVIDER, "UTF-8");
                    commandMap.put("PROVIDER", PROVIDER);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (ARTICL_DATE == "없음" || "없음".equals(ARTICL_DATE)) {
                    commandMap.put("ARTICL_DATE", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
          } else if (GENRE_CODE == "7" || GENRE_CODE.equals("7")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String WORKS_SUB_TITLE = EgovWebUtil.getString(commandMap, "WORKS_SUB_TITLE");
               String KIND = EgovWebUtil.getString(commandMap, "KIND");
               String MAKE_DATE = EgovWebUtil.getString(commandMap, "MAKE_DATE");
               String SOURCE_INFO = EgovWebUtil.getString(commandMap, "SOURCE_INFO");
               String SIZE_INFO = EgovWebUtil.getString(commandMap, "SIZE_INFO");
               String RESOLUTION = EgovWebUtil.getString(commandMap, "RESOLUTION");
               String MAIN_MTRL = EgovWebUtil.getString(commandMap, "MAIN_MTRL");
               String TXTR = EgovWebUtil.getString(commandMap, "TXTR");
               String IMG_URL = EgovWebUtil.getString(commandMap, "IMG_URL");
               String DETL_URL = EgovWebUtil.getString(commandMap, "DETL_URL");
               String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
               String POSS_DATE = EgovWebUtil.getString(commandMap, "POSS_DATE");
               String POSS_ORGN_NAME = EgovWebUtil.getString(commandMap, "POSS_ORGN_NAME");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(WORKS_SUB_TITLE)) {
                    WORKS_SUB_TITLE = URLDecoder.decode(WORKS_SUB_TITLE, "UTF-8");
                    commandMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
               }
               if (!"".equals(KIND)) {
                    KIND = URLDecoder.decode(KIND, "UTF-8");
                    commandMap.put("KIND", KIND);
               }
               if (!"".equals(MAKE_DATE)) {
                    MAKE_DATE = URLDecoder.decode(MAKE_DATE, "UTF-8");
                    commandMap.put("MAKE_DATE", MAKE_DATE);
               }
               if (!"".equals(SOURCE_INFO)) {
                    SOURCE_INFO = URLDecoder.decode(SOURCE_INFO, "UTF-8");
                    commandMap.put("SOURCE_INFO", SOURCE_INFO);
               }
               if (!"".equals(SIZE_INFO)) {
                    SIZE_INFO = URLDecoder.decode(SIZE_INFO, "UTF-8");
                    commandMap.put("SIZE_INFO", SIZE_INFO);
               }
               if (!"".equals(RESOLUTION)) {
                    RESOLUTION = URLDecoder.decode(RESOLUTION, "UTF-8");
                    commandMap.put("RESOLUTION", RESOLUTION);
               }
               if (!"".equals(MAIN_MTRL)) {
                    MAIN_MTRL = URLDecoder.decode(MAIN_MTRL, "UTF-8");
                    commandMap.put("MAIN_MTRL", MAIN_MTRL);
               }
               if (!"".equals(TXTR)) {
                    TXTR = URLDecoder.decode(TXTR, "UTF-8");
                    commandMap.put("TXTR", TXTR);
               }
               if (!"".equals(IMG_URL)) {
                    IMG_URL = URLDecoder.decode(IMG_URL, "UTF-8");
                    commandMap.put("IMG_URL", IMG_URL);
               }
               if (!"".equals(DETL_URL)) {
                    DETL_URL = URLDecoder.decode(DETL_URL, "UTF-8");
                    commandMap.put("DETL_URL", DETL_URL);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(POSS_DATE)) {
                    POSS_DATE = URLDecoder.decode(POSS_DATE, "UTF-8");
                    commandMap.put("POSS_DATE", POSS_DATE);
               }
               if (!"".equals(POSS_ORGN_NAME)) {
                    POSS_ORGN_NAME = URLDecoder.decode(POSS_ORGN_NAME, "UTF-8");
                    commandMap.put("POSS_ORGN_NAME", POSS_ORGN_NAME);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (MAKE_DATE == "없음" || "없음".equals(MAKE_DATE)) {
                    commandMap.put("MAKE_DATE", "0");
               }
               if (IMG_URL == "없음" || "없음".equals(IMG_URL)) {
                    commandMap.put("IMG_URL", "0");
               }
               if (DETL_URL == "없음" || "없음".equals(DETL_URL)) {
                    commandMap.put("DETL_URL", "0");
               }
               if (POSS_DATE == "없음" || "없음".equals(POSS_DATE)) {
                    commandMap.put("POSS_DATE", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
          } else if (GENRE_CODE == "8" || GENRE_CODE.equals("8")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String IMAGE_DESC = EgovWebUtil.getString(commandMap, "IMAGE_DESC");
               String KEYWORD = EgovWebUtil.getString(commandMap, "KEYWORD");
               String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
               String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
               String PRODUCER = EgovWebUtil.getString(commandMap, "PRODUCER");
               String IMAGE_URL = EgovWebUtil.getString(commandMap, "IMAGE_URL");
               String IMAGE_OPEN_YN = EgovWebUtil.getString(commandMap, "IMAGE_OPEN_YN");
               String ICNX_NUMB = EgovWebUtil.getString(commandMap, "ICNX_NUMB");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(IMAGE_DESC)) {
                    IMAGE_DESC = URLDecoder.decode(IMAGE_DESC, "UTF-8");
                    commandMap.put("IMAGE_DESC", IMAGE_DESC);
               }
               if (!"".equals(KEYWORD)) {
                    KEYWORD = URLDecoder.decode(KEYWORD, "UTF-8");
                    commandMap.put("KEYWORD", KEYWORD);
               }
               if (!"".equals(CRT_YEAR)) {
                    CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
                    commandMap.put("CRT_YEAR", CRT_YEAR);
               }
               if (!"".equals(WRITER)) {
                    WRITER = URLDecoder.decode(WRITER, "UTF-8");
                    commandMap.put("WRITER", WRITER);
               }
               if (!"".equals(PRODUCER)) {
                    PRODUCER = URLDecoder.decode(PRODUCER, "UTF-8");
                    commandMap.put("PRODUCER", PRODUCER);
               }
               if (!"".equals(IMAGE_URL)) {
                    IMAGE_URL = URLDecoder.decode(IMAGE_URL, "UTF-8");
                    commandMap.put("IMAGE_URL", IMAGE_URL);
               }
               if (!"".equals(IMAGE_OPEN_YN)) {
                    IMAGE_OPEN_YN = URLDecoder.decode(IMAGE_OPEN_YN, "UTF-8");
                    commandMap.put("IMAGE_OPEN_YN", IMAGE_OPEN_YN);
               }
               if (!"".equals(ICNX_NUMB)) {
                    ICNX_NUMB = URLDecoder.decode(ICNX_NUMB, "UTF-8");
                    commandMap.put("ICNX_NUMB", ICNX_NUMB);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (IMAGE_DESC == "없음" || "없음".equals(IMAGE_DESC)) {
                    commandMap.put("IMAGE_DESC", "0");
               }
               if (KEYWORD == "없음" || "없음".equals(KEYWORD)) {
                    commandMap.put("KEYWORD", "0");
               }
               if (IMAGE_URL == "없음" || "없음".equals(IMAGE_URL)) {
                    commandMap.put("IMAGE_URL", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
          } else if (GENRE_CODE == "99" || GENRE_CODE.equals("99")) {
               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String SIDE_GENRE_CD = EgovWebUtil.getString(commandMap, "SIDE_GENRE_CD");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String COPT_NAME = EgovWebUtil.getString(commandMap, "COPT_NAME");
               String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
               String PUBL_MEDI = EgovWebUtil.getString(commandMap, "PUBL_MEDI");
               String PUBL_DATE = EgovWebUtil.getString(commandMap, "PUBL_DATE");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    commandMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(SIDE_GENRE_CD)) {
                    SIDE_GENRE_CD = URLDecoder.decode(SIDE_GENRE_CD, "UTF-8");
                    commandMap.put("SIDE_GENRE_CD", SIDE_GENRE_CD);
               }
               if (!"".equals(COPT_NAME)) {
                    COPT_NAME = URLDecoder.decode(COPT_NAME, "UTF-8");
                    commandMap.put("COPT_NAME", COPT_NAME);
               }
               if (!"".equals(CRT_YEAR)) {
                    CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
                    commandMap.put("CRT_YEAR", CRT_YEAR);
               }
               if (!"".equals(PUBL_MEDI)) {
                    PUBL_MEDI = URLDecoder.decode(PUBL_MEDI, "UTF-8");
                    commandMap.put("PUBL_MEDI", PUBL_MEDI);
               }
               if (!"".equals(PUBL_DATE)) {
                    PUBL_DATE = URLDecoder.decode(PUBL_DATE, "UTF-8");
                    commandMap.put("PUBL_DATE", PUBL_DATE);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               }
               if (PUBL_DATE == "없음" || "없음".equals(PUBL_DATE)) {
                    commandMap.put("PUBL_DATE", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    commandMap.put("COMM_MGNT_CD", "0");
               }
          }

          ArrayList<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
          boolean isSuccess = fdcrAd17Service.fdcrAd17Update1(commandMap, excelList);
          returnAjaxString(response, isSuccess);

          if (isSuccess) {
               return returnUrl(model, "수정보고가 완료되었습니다.", "/console/rcept/fdcrAd17List2.page");
          } else {
               return returnUrl(model, "수정보고에 실패했습니다.", "/console/rcept/fdcrAd17List2.page");
          }
     }

     /**
      * 일괄등록 등록 전 화면
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17List1.page")
     public String fdcrAd17List1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd17List1 Start");

          ArrayList<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();

          // 보고연월
          GregorianCalendar d = new GregorianCalendar();
          String reptYYYYTmp = String.valueOf(d.get(Calendar.YEAR));
          String reptMMTmp = String.valueOf(((d.get(Calendar.MONTH) + 1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH) + 1) : String.valueOf(d.get(Calendar.MONTH) + 1));

          String reptYYYY = reptYYYYTmp.replaceAll(" ", "");
          String reptMM = reptMMTmp.replaceAll(" ", "");
          // System.out.println("ConsoleLoginUser.getUserName()::" + ConsoleLoginUser.getCommname());
          String commName = ConsoleLoginUser.getCommname();
          String userId = ConsoleLoginUser.getUserId();
          String REPT_CHRR_MAIL = ConsoleLoginUser.getUserEmail();
          String REPT_CHRR_NAME = ConsoleLoginUser.getUserName();
          String REPT_CHRR_TELX = ConsoleLoginUser.getMoblPhon();
          String REPT_CHRR_POSI = ConsoleLoginUser.getUserPosition();

          model.addAttribute("userId", userId);
          model.addAttribute("REPT_CHRR_TELX", REPT_CHRR_TELX);
          model.addAttribute("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
          model.addAttribute("REPT_CHRR_NAME", REPT_CHRR_NAME);
          model.addAttribute("REPT_CHRR_POSI", REPT_CHRR_POSI);
          model.addAttribute("commName", commName);
          model.addAttribute("reptYYYY", reptYYYY);
          model.addAttribute("reptMM", reptMM);
          logger.debug("fdcrAd17List1 End");
          return "rcept/fdcrAd17List1.tiles";
     }

     /**
      * 일괄수정 등록 전 화면
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17List2.page")
     public String fdcrAd17List2(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd17List2 Start");

          ArrayList<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
          // 보고연월
          GregorianCalendar d = new GregorianCalendar();
          String reptYYYYTmp = String.valueOf(d.get(Calendar.YEAR));
          String reptMMTmp = String.valueOf(((d.get(Calendar.MONTH) + 1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH) + 1) : String.valueOf(d.get(Calendar.MONTH) + 1));

          String reptYYYY = reptYYYYTmp.replaceAll(" ", "");
          String reptMM = reptMMTmp.replaceAll(" ", "");
          String commName = ConsoleLoginUser.getCommname();
          String userId = ConsoleLoginUser.getUserId();
          String REPT_CHRR_MAIL = ConsoleLoginUser.getUserEmail();
          String REPT_CHRR_NAME = ConsoleLoginUser.getUserName();
          String REPT_CHRR_TELX = ConsoleLoginUser.getMoblPhon();
          String REPT_CHRR_POSI = ConsoleLoginUser.getUserPosition();

          model.addAttribute("userId", userId);
          model.addAttribute("REPT_CHRR_TELX", REPT_CHRR_TELX);
          model.addAttribute("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
          model.addAttribute("REPT_CHRR_NAME", REPT_CHRR_NAME);
          model.addAttribute("REPT_CHRR_POSI", REPT_CHRR_POSI);
          model.addAttribute("commName", commName);
          model.addAttribute("reptYYYY", reptYYYY);
          model.addAttribute("reptMM", reptMM);
          /*
           * model.addAttribute("commName", commName); model.addAttribute("reptYYYY", reptYYYY);
           * model.addAttribute("reptMM", reptMM);
           */
          logger.debug("fdcrAd17List2 End");
          return "rcept/fdcrAd17List2.tiles";
     }

     /**
      * 보고저작물 없음 시 등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17InsertNodata.page")
     public String fdcrAd17InsertNodata(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 파라미터 셋팅
          String TRST_ORGN_CODE = EgovWebUtil.getString(commandMap, ConsoleLoginUser.getTrstOrgnCode());
          String REPT_CHRR_NAME = EgovWebUtil.getString(commandMap, "REPT_CHRR_NAME");
          String REPT_CHRR_POSI = EgovWebUtil.getString(commandMap, "REPT_CHRR_POSI");
          String REPT_CHRR_MAIL = EgovWebUtil.getString(commandMap, "REPT_CHRR_MAIL");
          String REPT_CHRR_TELX = EgovWebUtil.getString(commandMap, "REPT_CHRR_TELX");
          String COMM_NAME = EgovWebUtil.getString(commandMap, "REPT_CHRR_NAME");
          String COMM_TELX = EgovWebUtil.getString(commandMap, "REPT_CHRR_TELX");
          String COMM_REPS_NAME = EgovWebUtil.getString(commandMap, "REPT_CHRR_NAME");
          String USER_NAME = EgovWebUtil.getString(commandMap, ConsoleLoginUser.getUserName());
          String USER_IDNT = EgovWebUtil.getString(commandMap, ConsoleLoginUser.getUserId());
          commandMap.put("GENRE_CODE", "0");

          if (!"".equals(TRST_ORGN_CODE)) {
               TRST_ORGN_CODE = URLDecoder.decode(TRST_ORGN_CODE, "UTF-8");
               commandMap.put("TRST_ORGN_CODE", TRST_ORGN_CODE);
          }
          if (!"".equals(REPT_CHRR_NAME)) {
               REPT_CHRR_NAME = URLDecoder.decode(REPT_CHRR_NAME, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
          }
          if (!"".equals(REPT_CHRR_POSI)) {
               REPT_CHRR_POSI = URLDecoder.decode(REPT_CHRR_POSI, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
          }
          if (!"".equals(REPT_CHRR_MAIL)) {
               REPT_CHRR_MAIL = URLDecoder.decode(REPT_CHRR_MAIL, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
          }
          if (!"".equals(REPT_CHRR_TELX)) {
               REPT_CHRR_TELX = URLDecoder.decode(REPT_CHRR_TELX, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
          }
          if (!"".equals(COMM_NAME)) {
               COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
               commandMap.put("COMM_NAME", COMM_NAME);
          }
          if (!"".equals(COMM_TELX)) {
               COMM_TELX = URLDecoder.decode(COMM_TELX, "UTF-8");
               commandMap.put("COMM_TELX", COMM_TELX);
          }
          if (!"".equals(COMM_REPS_NAME)) {
               COMM_REPS_NAME = URLDecoder.decode(COMM_REPS_NAME, "UTF-8");
               commandMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
          }
          if (!"".equals(USER_NAME)) {
               USER_NAME = URLDecoder.decode(USER_NAME, "UTF-8");
               commandMap.put("USER_NAME", USER_NAME);
          }
          if (!"".equals(USER_IDNT)) {
               USER_IDNT = URLDecoder.decode(USER_IDNT, "UTF-8");
               commandMap.put("USER_IDNT", USER_IDNT);
          }

          boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNodata(commandMap);
          returnAjaxString(response, isSuccess);

          if (isSuccess) {
               return returnUrl(model, "저장했습니다.", "/console/rcept/fdcrAd17List1.page");
          } else {
               return returnUrl(model, "실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 보고기관 검색
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17Pop1.page")
     public String fdcrAd17Pop1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 파라미터 셋팅
          String TRST_ORGN_DIVS_CODE = EgovWebUtil.getString(commandMap, "TRST_ORGN_DIVS_CODE");
          String SCH_CONAME = EgovWebUtil.getString(commandMap, "SCH_CONAME");
          if (!"".equals(TRST_ORGN_DIVS_CODE)) {
               TRST_ORGN_DIVS_CODE = URLDecoder.decode(TRST_ORGN_DIVS_CODE, "UTF-8");
               commandMap.put("TRST_ORGN_DIVS_CODE", TRST_ORGN_DIVS_CODE);
          }
          if (!"".equals(SCH_CONAME)) {
               SCH_CONAME = URLDecoder.decode(SCH_CONAME, "UTF-8");
               commandMap.put("USER_IDNT", SCH_CONAME);
          }

          fdcrAd17Service.fdcrAd17Pop1(commandMap);
          model.addAttribute("ds_list", commandMap.get("ds_list"));
          model.addAttribute("paginationInfo", commandMap.get("paginationInfo"));
          return "rcept/fdcrAd17Pop1.popup";
     }

     /**
      * 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17Insert.page")
     public String fdcrAd17Insert(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {
          // 파라미터 셋팅

          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          String GENRE_CODE = EgovWebUtil.getString(commandMap, "GENRE_CODE");
          // System.out.println("GENRE_CODE::::::::::"+GENRE_CODE);

          String[] uploadList = request.getParameterValues("uploadList");
          // String ss = (String) commandMap.get( "0_3_2" );
          // System.out.println("아아아아아아아아아아아아아아아아아아아아아아아ㅏㅇ아아아아아아ㅏㅇ::::::"+EgovWebUtil.getString(
          // commandMap, "0_3_2" ));
          System.out.println("uploadList.length:::::::::::::" + uploadList.length);
          /*
           * for(int i=0; i<uploadList.length; i++ ){ System.out.println("uploadList 시작!!" + uploadList[i]); }
           */

          /*
           * if(GENRE_CODE == "1" || GENRE_CODE.equals( "1" )){ // --music String RGST_ORGN_NAME =
           * EgovWebUtil.getString( commandMap, "RGST_ORGN_NAME" ); String COMM_WORKS_ID =
           * EgovWebUtil.getString( commandMap, "COMM_WORKS_ID" ); String WORKS_TITLE = EgovWebUtil.getString(
           * commandMap, "WORKS_TITLE" ); String NATION_CD = EgovWebUtil.getString( commandMap, "NATION_CD" );
           * String ALBUM_TITLE = EgovWebUtil.getString( commandMap, "ALBUM_TITLE" ); String ALBUM_LABLE =
           * EgovWebUtil.getString( commandMap, "ALBUM_LABLE" ); String DISK_SIDE = EgovWebUtil.getString(
           * commandMap, "DISK_SIDE" ); String TRACK_NO = EgovWebUtil.getString( commandMap, "TRACK_NO" );
           * String ALBUM_ISSU_YEAR = EgovWebUtil.getString( commandMap, "ALBUM_ISSU_YEAR" ); String LYRC =
           * EgovWebUtil.getString( commandMap, "LYRC" ); String COMP = EgovWebUtil.getString( commandMap,
           * "COMP" ); String ARRA = EgovWebUtil.getString( commandMap, "ARRA" ); String TRAN =
           * EgovWebUtil.getString( commandMap, "TRAN" ); String SING = EgovWebUtil.getString( commandMap,
           * "SING" ); String PERF = EgovWebUtil.getString( commandMap, "PERF" ); String PROD =
           * EgovWebUtil.getString( commandMap, "PROD" ); String COMM_MGNT_CD = EgovWebUtil.getString(
           * commandMap, "COMM_MGNT_CD" ); String LICE_MGNT_CD = EgovWebUtil.getString( commandMap,
           * "LICE_MGNT_CD" ); String PERF_MGNT_CD = EgovWebUtil.getString( commandMap, "PERF_MGNT_CD" );
           * String PROD_MGNT_CD = EgovWebUtil.getString( commandMap, "PROD_MGNT_CD" ); String UCI =
           * EgovWebUtil.getString( commandMap, "UCI" ); String RATIO = EgovWebUtil.getString( commandMap,
           * "RATIO" ); String TOT_CNT = EgovWebUtil.getString( commandMap, "tot_cnt" );
           * System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);
           * 
           * if( !"".equals( RGST_ORGN_NAME ) ){ RGST_ORGN_NAME = URLDecoder.decode( RGST_ORGN_NAME, "UTF-8"
           * ); commandMap.put( "RGST_ORGN_NAME", RGST_ORGN_NAME ); } if( !"".equals( COMM_WORKS_ID ) ){
           * COMM_WORKS_ID = URLDecoder.decode( COMM_WORKS_ID, "UTF-8" ); commandMap.put( "COMM_WORKS_ID",
           * COMM_WORKS_ID ); } if( !"".equals( WORKS_TITLE ) ){ WORKS_TITLE = URLDecoder.decode( WORKS_TITLE,
           * "UTF-8" ); commandMap.put( "WORKS_TITLE", WORKS_TITLE ); } if( !"".equals( NATION_CD ) ){
           * NATION_CD = URLDecoder.decode( NATION_CD, "UTF-8" ); commandMap.put( "NATION_CD", NATION_CD ); }
           * if( !"".equals( ALBUM_TITLE ) ){ ALBUM_TITLE = URLDecoder.decode( ALBUM_TITLE, "UTF-8" );
           * commandMap.put( "ALBUM_TITLE", ALBUM_TITLE ); } if( !"".equals( ALBUM_LABLE ) ){ ALBUM_LABLE =
           * URLDecoder.decode( ALBUM_LABLE, "UTF-8" ); commandMap.put( "ALBUM_LABLE", ALBUM_LABLE ); } if(
           * !"".equals( DISK_SIDE ) ){ DISK_SIDE = URLDecoder.decode( DISK_SIDE, "UTF-8" ); commandMap.put(
           * "DISK_SIDE", DISK_SIDE ); } if( !"".equals( TRACK_NO ) ){ TRACK_NO = URLDecoder.decode( TRACK_NO,
           * "UTF-8" ); commandMap.put( "TRACK_NO", TRACK_NO ); } if( !"".equals( ALBUM_ISSU_YEAR ) ){
           * ALBUM_ISSU_YEAR = URLDecoder.decode( ALBUM_ISSU_YEAR, "UTF-8" ); commandMap.put(
           * "ALBUM_ISSU_YEAR", ALBUM_ISSU_YEAR ); } if( !"".equals( LYRC ) ){ LYRC = URLDecoder.decode( LYRC,
           * "UTF-8" ); commandMap.put( "LYRC", LYRC ); } if( !"".equals( COMP ) ){ COMP = URLDecoder.decode(
           * COMP, "UTF-8" ); commandMap.put( "COMP", COMP ); } if( !"".equals( ARRA ) ){ ARRA =
           * URLDecoder.decode( ARRA, "UTF-8" ); commandMap.put( "ARRA", ARRA ); } if( !"".equals( TRAN ) ){
           * TRAN = URLDecoder.decode( TRAN, "UTF-8" ); commandMap.put( "TRAN", TRAN ); } if( !"".equals( SING
           * ) ){ SING = URLDecoder.decode( SING, "UTF-8" ); commandMap.put( "SING", SING ); } if( !"".equals(
           * PERF ) ){ PERF = URLDecoder.decode( PERF, "UTF-8" ); commandMap.put( "PERF", PERF ); } if(
           * !"".equals( PROD ) ){ PROD = URLDecoder.decode( PROD, "UTF-8" ); commandMap.put( "PROD", PROD );
           * } if( !"".equals( COMM_MGNT_CD ) ){ COMM_MGNT_CD = URLDecoder.decode( COMM_MGNT_CD, "UTF-8" );
           * commandMap.put( "COMM_MGNT_CD", COMM_MGNT_CD ); } if( !"".equals( LICE_MGNT_CD ) ){ LICE_MGNT_CD
           * = URLDecoder.decode( LICE_MGNT_CD, "UTF-8" ); commandMap.put( "LICE_MGNT_CD", LICE_MGNT_CD ); }
           * if( !"".equals( PERF_MGNT_CD ) ){ PERF_MGNT_CD = URLDecoder.decode( PERF_MGNT_CD, "UTF-8" );
           * commandMap.put( "PERF_MGNT_CD", PERF_MGNT_CD ); } if( !"".equals( PROD_MGNT_CD ) ){ PROD_MGNT_CD
           * = URLDecoder.decode( PROD_MGNT_CD, "UTF-8" ); commandMap.put( "PROD_MGNT_CD", PROD_MGNT_CD ); }
           * if( !"".equals( UCI ) ){ UCI = URLDecoder.decode( UCI, "UTF-8" ); commandMap.put( "UCI", UCI ); }
           * if( !"".equals( RATIO ) ){ RATIO = URLDecoder.decode( RATIO, "UTF-8" ); commandMap.put( "RATIO",
           * RATIO ); }
           * 
           * if(NATION_CD == "없음" || "없음".equals( NATION_CD )){ commandMap.put( "NATION_CD", "0" ); }
           * if(DISK_SIDE == "없음" || "없음".equals( DISK_SIDE )){ commandMap.put( "DISK_SIDE", "0" ); }
           * if(TRACK_NO == "없음" || "없음".equals( TRACK_NO )){ commandMap.put( "TRACK_NO", "0" ); }
           * if(ALBUM_ISSU_YEAR == "없음" || "없음".equals( ALBUM_ISSU_YEAR )){ commandMap.put( "ALBUM_ISSU_YEAR",
           * "0" ); } if(COMM_MGNT_CD == "없음" || "없음".equals( COMM_MGNT_CD )){ commandMap.put( "COMM_MGNT_CD",
           * "0" ); } if(LICE_MGNT_CD == "없음" || "없음".equals( LICE_MGNT_CD )){ commandMap.put( "LICE_MGNT_CD",
           * "0" ); } if(PERF_MGNT_CD == "없음" || "없음".equals( PERF_MGNT_CD )){ commandMap.put( "PERF_MGNT_CD",
           * "0" ); } if(PROD_MGNT_CD == "없음" || "없음".equals( PROD_MGNT_CD )){ commandMap.put( "PROD_MGNT_CD",
           * "0" ); } if(UCI == "없음" || "없음".equals( UCI )){ commandMap.put( "UCI", "0" ); } if(RATIO == "없음"
           * || "없음".equals( RATIO )){ commandMap.put( "RATIO", "0" ); } if( !"".equals( TOT_CNT ) ){ TOT_CNT
           * = URLDecoder.decode( TOT_CNT, "UTF-8" ); commandMap.put( "TOT_CNT", TOT_CNT ); }
           * 
           * }else if(GENRE_CODE == "2" || GENRE_CODE.equals( "2" )){ String RGST_ORGN_NAME =
           * EgovWebUtil.getString( commandMap, "RGST_ORGN_NAME" ); String COMM_WORKS_ID =
           * EgovWebUtil.getString( commandMap, "COMM_WORKS_ID" ); String WORKS_TITLE = EgovWebUtil.getString(
           * commandMap, "WORKS_TITLE" ); String NATION_CD = EgovWebUtil.getString( commandMap, "NATION_CD" );
           * String WORKS_SUB_TITLE = EgovWebUtil.getString( commandMap, "WORKS_SUB_TITLE" ); String CRT_YEAR
           * = EgovWebUtil.getString( commandMap, "CRT_YEAR" ); String BOOK_TITLE = EgovWebUtil.getString(
           * commandMap, "BOOK_TITLE" ); String BOOK_PUBLISHER = EgovWebUtil.getString( commandMap,
           * "BOOK_PUBLISHER" ); String BOOK_ISSU_YEAR = EgovWebUtil.getString( commandMap, "BOOK_ISSU_YEAR"
           * ); String COPT_NAME = EgovWebUtil.getString( commandMap, "COPT_NAME" ); String COMM_MGNT_CD =
           * EgovWebUtil.getString( commandMap, "COMM_MGNT_CD" ); String TOT_CNT = EgovWebUtil.getString(
           * commandMap, "tot_cnt" ); System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);
           * if( !"".equals( RGST_ORGN_NAME ) ){ RGST_ORGN_NAME = URLDecoder.decode( RGST_ORGN_NAME, "UTF-8"
           * ); commandMap.put( "RGST_ORGN_NAME", RGST_ORGN_NAME ); } if( !"".equals( COMM_WORKS_ID ) ){
           * COMM_WORKS_ID = URLDecoder.decode( COMM_WORKS_ID, "UTF-8" ); commandMap.put( "COMM_WORKS_ID",
           * COMM_WORKS_ID ); } if( !"".equals( WORKS_TITLE ) ){ WORKS_TITLE = URLDecoder.decode( WORKS_TITLE,
           * "UTF-8" ); commandMap.put( "WORKS_TITLE", WORKS_TITLE ); } if( !"".equals( NATION_CD ) ){
           * NATION_CD = URLDecoder.decode( NATION_CD, "UTF-8" ); commandMap.put( "NATION_CD", NATION_CD ); }
           * if( !"".equals( WORKS_SUB_TITLE ) ){ WORKS_SUB_TITLE = URLDecoder.decode( WORKS_SUB_TITLE,
           * "UTF-8" ); commandMap.put( "WORKS_SUB_TITLE", WORKS_SUB_TITLE ); } if( !"".equals( CRT_YEAR ) ){
           * CRT_YEAR = URLDecoder.decode( CRT_YEAR, "UTF-8" ); commandMap.put( "CRT_YEAR", CRT_YEAR ); } if(
           * !"".equals( BOOK_TITLE ) ){ BOOK_TITLE = URLDecoder.decode( BOOK_TITLE, "UTF-8" );
           * commandMap.put( "BOOK_TITLE", BOOK_TITLE ); } if( !"".equals( BOOK_PUBLISHER ) ){ BOOK_PUBLISHER
           * = URLDecoder.decode( BOOK_PUBLISHER, "UTF-8" ); commandMap.put( "BOOK_PUBLISHER", BOOK_PUBLISHER
           * ); } if( !"".equals( BOOK_ISSU_YEAR ) ){ BOOK_ISSU_YEAR = URLDecoder.decode( BOOK_ISSU_YEAR,
           * "UTF-8" ); commandMap.put( "BOOK_ISSU_YEAR", BOOK_ISSU_YEAR ); } if( !"".equals( COPT_NAME ) ){
           * COPT_NAME = URLDecoder.decode( COPT_NAME, "UTF-8" ); commandMap.put( "COPT_NAME", COPT_NAME ); }
           * if( !"".equals( COMM_MGNT_CD ) ){ COMM_MGNT_CD = URLDecoder.decode( COMM_MGNT_CD, "UTF-8" );
           * commandMap.put( "COMP", COMM_MGNT_CD ); } if(CRT_YEAR == "없음" || "없음".equals( CRT_YEAR )){
           * commandMap.put( "CRT_YEAR", "0" ); } if(BOOK_ISSU_YEAR == "없음" || "없음".equals( BOOK_ISSU_YEAR )){
           * commandMap.put( "BOOK_ISSU_YEAR", "0" ); } if(COMM_MGNT_CD == "없음" || "없음".equals( COMM_MGNT_CD
           * )){ commandMap.put( "COMM_MGNT_CD", "0" ); } if( !"".equals( TOT_CNT ) ){ TOT_CNT =
           * URLDecoder.decode( TOT_CNT, "UTF-8" ); commandMap.put( "TOT_CNT", TOT_CNT ); } }else
           * if(GENRE_CODE == "3" || GENRE_CODE.equals( "3" )){ String RGST_ORGN_NAME = EgovWebUtil.getString(
           * commandMap, "RGST_ORGN_NAME" ); String COMM_WORKS_ID = EgovWebUtil.getString( commandMap,
           * "COMM_WORKS_ID" ); String WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
           * String WORKS_ORIG_TITLE = EgovWebUtil.getString( commandMap, "WORKS_ORIG_TITLE" ); String
           * SCRT_GENRE_CD = EgovWebUtil.getString( commandMap, "SCRT_GENRE_CD" ); String SCRP_SUBJ_CD =
           * EgovWebUtil.getString( commandMap, "SCRP_SUBJ_CD" ); String ORIG_WRITER = EgovWebUtil.getString(
           * commandMap, "ORIG_WRITER" ); String PLAYER = EgovWebUtil.getString( commandMap, "PLAYER" );
           * String BROD_ORD_SEQ = EgovWebUtil.getString( commandMap, "BROD_ORD_SEQ" ); String BORD_DATE =
           * EgovWebUtil.getString( commandMap, "BORD_DATE" ); String MAKE_CPY = EgovWebUtil.getString(
           * commandMap, "MAKE_CPY" ); String WRITER = EgovWebUtil.getString( commandMap, "WRITER" ); String
           * DIRECTOR = EgovWebUtil.getString( commandMap, "DIRECTOR" ); String COMM_MGNT_CD =
           * EgovWebUtil.getString( commandMap, "COMM_MGNT_CD" ); String TOT_CNT = EgovWebUtil.getString(
           * commandMap, "tot_cnt" ); System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);
           * if( !"".equals( RGST_ORGN_NAME ) ){ RGST_ORGN_NAME = URLDecoder.decode( RGST_ORGN_NAME, "UTF-8"
           * ); commandMap.put( "RGST_ORGN_NAME", RGST_ORGN_NAME ); } if( !"".equals( COMM_WORKS_ID ) ){
           * COMM_WORKS_ID = URLDecoder.decode( COMM_WORKS_ID, "UTF-8" ); commandMap.put( "COMM_WORKS_ID",
           * COMM_WORKS_ID ); } if( !"".equals( WORKS_TITLE ) ){ WORKS_TITLE = URLDecoder.decode( WORKS_TITLE,
           * "UTF-8" ); commandMap.put( "WORKS_TITLE", WORKS_TITLE ); } if( !"".equals( WORKS_ORIG_TITLE ) ){
           * WORKS_ORIG_TITLE = URLDecoder.decode( WORKS_ORIG_TITLE, "UTF-8" ); commandMap.put(
           * "WORKS_ORIG_TITLE", WORKS_ORIG_TITLE ); } if( !"".equals( SCRT_GENRE_CD ) ){ SCRT_GENRE_CD =
           * URLDecoder.decode( SCRT_GENRE_CD, "UTF-8" ); commandMap.put( "SCRT_GENRE_CD", SCRT_GENRE_CD ); }
           * if( !"".equals( SCRP_SUBJ_CD ) ){ SCRP_SUBJ_CD = URLDecoder.decode( SCRP_SUBJ_CD, "UTF-8" );
           * commandMap.put( "SCRP_SUBJ_CD", SCRP_SUBJ_CD ); } if( !"".equals( ORIG_WRITER ) ){ ORIG_WRITER =
           * URLDecoder.decode( ORIG_WRITER, "UTF-8" ); commandMap.put( "ORIG_WRITER", ORIG_WRITER ); } if(
           * !"".equals( PLAYER ) ){ PLAYER = URLDecoder.decode( PLAYER, "UTF-8" ); commandMap.put( "PLAYER",
           * PLAYER ); } if( !"".equals( BROD_ORD_SEQ ) ){ BROD_ORD_SEQ = URLDecoder.decode( BROD_ORD_SEQ,
           * "UTF-8" ); commandMap.put( "BROD_ORD_SEQ", BROD_ORD_SEQ ); } if( !"".equals( BORD_DATE ) ){
           * BORD_DATE = URLDecoder.decode( BORD_DATE, "UTF-8" ); commandMap.put( "BORD_DATE", BORD_DATE ); }
           * if( !"".equals( MAKE_CPY ) ){ MAKE_CPY = URLDecoder.decode( MAKE_CPY, "UTF-8" ); commandMap.put(
           * "MAKE_CPY", MAKE_CPY ); } if( !"".equals( WRITER ) ){ WRITER = URLDecoder.decode( WRITER, "UTF-8"
           * ); commandMap.put( "WRITER", WRITER ); } if( !"".equals( DIRECTOR ) ){ DIRECTOR =
           * URLDecoder.decode( DIRECTOR, "UTF-8" ); commandMap.put( "DIRECTOR", DIRECTOR ); } if( !"".equals(
           * COMM_MGNT_CD ) ){ COMM_MGNT_CD = URLDecoder.decode( COMM_MGNT_CD, "UTF-8" ); commandMap.put(
           * "COMM_MGNT_CD", COMM_MGNT_CD ); } if(SCRT_GENRE_CD == "없음" || "없음".equals( SCRT_GENRE_CD )){
           * commandMap.put( "SCRT_GENRE_CD", "0" ); } if(SCRP_SUBJ_CD == "없음" || "없음".equals( SCRP_SUBJ_CD
           * )){ commandMap.put( "SCRP_SUBJ_CD", "0" ); } if(BROD_ORD_SEQ == "없음" || "없음".equals( BROD_ORD_SEQ
           * )){ commandMap.put( "BROD_ORD_SEQ", "0" ); } if(BORD_DATE == "없음" || "없음".equals( BORD_DATE )){
           * commandMap.put( "BORD_DATE", "0" ); } if(COMM_MGNT_CD == "없음" || "없음".equals( COMM_MGNT_CD )){
           * commandMap.put( "COMM_MGNT_CD", "0" ); } if( !"".equals( TOT_CNT ) ){ TOT_CNT =
           * URLDecoder.decode( TOT_CNT, "UTF-8" ); commandMap.put( "TOT_CNT", TOT_CNT ); } }else
           * if(GENRE_CODE == "4" || GENRE_CODE.equals( "4" )){ String RGST_ORGN_NAME = EgovWebUtil.getString(
           * commandMap, "RGST_ORGN_NAME" ); String COMM_WORKS_ID = EgovWebUtil.getString( commandMap,
           * "COMM_WORKS_ID" ); String WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
           * String CRT_YEAR = EgovWebUtil.getString( commandMap, "CRT_YEAR" ); String PLAYER =
           * EgovWebUtil.getString( commandMap, "PLAYER" ); String DIRECT = EgovWebUtil.getString( commandMap,
           * "DIRECT" ); String WRITER = EgovWebUtil.getString( commandMap, "WRITER" ); String DIRECTOR =
           * EgovWebUtil.getString( commandMap, "DIRECTOR" ); String PRODUCER = EgovWebUtil.getString(
           * commandMap, "PRODUCER" ); String DISTRIBUTOR = EgovWebUtil.getString( commandMap, "DISTRIBUTOR"
           * ); String INVESTOR = EgovWebUtil.getString( commandMap, "INVESTOR" ); String COMM_MGNT_CD =
           * EgovWebUtil.getString( commandMap, "COMM_MGNT_CD" ); String TOT_CNT = EgovWebUtil.getString(
           * commandMap, "tot_cnt" ); System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);
           * if( !"".equals( RGST_ORGN_NAME ) ){ RGST_ORGN_NAME = URLDecoder.decode( RGST_ORGN_NAME, "UTF-8"
           * ); commandMap.put( "RGST_ORGN_NAME", RGST_ORGN_NAME ); } if( !"".equals( COMM_WORKS_ID ) ){
           * COMM_WORKS_ID = URLDecoder.decode( COMM_WORKS_ID, "UTF-8" ); commandMap.put( "COMM_WORKS_ID",
           * COMM_WORKS_ID ); } if( !"".equals( WORKS_TITLE ) ){ WORKS_TITLE = URLDecoder.decode( WORKS_TITLE,
           * "UTF-8" ); commandMap.put( "WORKS_TITLE", WORKS_TITLE ); } if( !"".equals( CRT_YEAR ) ){ CRT_YEAR
           * = URLDecoder.decode( CRT_YEAR, "UTF-8" ); commandMap.put( "CRT_YEAR", CRT_YEAR ); } if(
           * !"".equals( PLAYER ) ){ PLAYER = URLDecoder.decode( PLAYER, "UTF-8" ); commandMap.put( "PLAYER",
           * PLAYER ); } if( !"".equals( DIRECT ) ){ DIRECT = URLDecoder.decode( DIRECT, "UTF-8" );
           * commandMap.put( "DIRECT", DIRECT ); } if( !"".equals( WRITER ) ){ WRITER = URLDecoder.decode(
           * WRITER, "UTF-8" ); commandMap.put( "WRITER", WRITER ); } if( !"".equals( DIRECTOR ) ){ DIRECTOR =
           * URLDecoder.decode( DIRECTOR, "UTF-8" ); commandMap.put( "DIRECTOR", DIRECTOR ); } if( !"".equals(
           * PRODUCER ) ){ PRODUCER = URLDecoder.decode( PRODUCER, "UTF-8" ); commandMap.put( "PRODUCER",
           * PRODUCER ); } if( !"".equals( DISTRIBUTOR ) ){ DISTRIBUTOR = URLDecoder.decode( DISTRIBUTOR,
           * "UTF-8" ); commandMap.put( "DISTRIBUTOR", DISTRIBUTOR ); } if( !"".equals( INVESTOR ) ){ INVESTOR
           * = URLDecoder.decode( INVESTOR, "UTF-8" ); commandMap.put( "INVESTOR", INVESTOR ); } if(
           * !"".equals( COMM_MGNT_CD ) ){ COMM_MGNT_CD = URLDecoder.decode( COMM_MGNT_CD, "UTF-8" );
           * commandMap.put( "COMM_MGNT_CD", COMM_MGNT_CD ); } if(CRT_YEAR == "없음" || "없음".equals( CRT_YEAR
           * )){ commandMap.put( "CRT_YEAR", "0" ); } if(COMM_MGNT_CD == "없음" || "없음".equals( COMM_MGNT_CD )){
           * commandMap.put( "COMM_MGNT_CD", "0" ); } if( !"".equals( TOT_CNT ) ){ TOT_CNT =
           * URLDecoder.decode( TOT_CNT, "UTF-8" ); commandMap.put( "TOT_CNT", TOT_CNT ); } }else
           * if(GENRE_CODE == "5" || GENRE_CODE.equals( "5" )){ String RGST_ORGN_NAME = EgovWebUtil.getString(
           * commandMap, "RGST_ORGN_NAME" ); String COMM_WORKS_ID = EgovWebUtil.getString( commandMap,
           * "COMM_WORKS_ID" ); String WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
           * String BORD_GENRE_CD = EgovWebUtil.getString( commandMap, "BORD_GENRE_CD" ); String MAKE_YEAR =
           * EgovWebUtil.getString( commandMap, "MAKE_YEAR" ); String BROD_ORD_SEQ = EgovWebUtil.getString(
           * commandMap, "BROD_ORD_SEQ" ); String DIRECTOR = EgovWebUtil.getString( commandMap, "DIRECTOR" );
           * String MAKE_CPY = EgovWebUtil.getString( commandMap, "MAKE_CPY" ); String PLAYER =
           * EgovWebUtil.getString( commandMap, "PLAYER" ); String WRITER = EgovWebUtil.getString( commandMap,
           * "WRITER" ); String COMM_MGNT_CD = EgovWebUtil.getString( commandMap, "COMM_MGNT_CD" ); String
           * TOT_CNT = EgovWebUtil.getString( commandMap, "tot_cnt" );
           * System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT); if( !"".equals(
           * RGST_ORGN_NAME ) ){ RGST_ORGN_NAME = URLDecoder.decode( RGST_ORGN_NAME, "UTF-8" );
           * commandMap.put( "RGST_ORGN_NAME", RGST_ORGN_NAME ); } if( !"".equals( COMM_WORKS_ID ) ){
           * COMM_WORKS_ID = URLDecoder.decode( COMM_WORKS_ID, "UTF-8" ); commandMap.put( "COMM_WORKS_ID",
           * COMM_WORKS_ID ); } if( !"".equals( WORKS_TITLE ) ){ WORKS_TITLE = URLDecoder.decode( WORKS_TITLE,
           * "UTF-8" ); commandMap.put( "WORKS_TITLE", WORKS_TITLE ); } if( !"".equals( BORD_GENRE_CD ) ){
           * BORD_GENRE_CD = URLDecoder.decode( BORD_GENRE_CD, "UTF-8" ); commandMap.put( "BORD_GENRE_CD",
           * BORD_GENRE_CD ); } if( !"".equals( MAKE_YEAR ) ){ MAKE_YEAR = URLDecoder.decode( MAKE_YEAR,
           * "UTF-8" ); commandMap.put( "MAKE_YEAR", MAKE_YEAR ); } if( !"".equals( BROD_ORD_SEQ ) ){
           * BROD_ORD_SEQ = URLDecoder.decode( BROD_ORD_SEQ, "UTF-8" ); commandMap.put( "BROD_ORD_SEQ",
           * BROD_ORD_SEQ ); } if( !"".equals( DIRECTOR ) ){ DIRECTOR = URLDecoder.decode( DIRECTOR, "UTF-8"
           * ); commandMap.put( "DIRECTOR", DIRECTOR ); } if( !"".equals( MAKE_CPY ) ){ MAKE_CPY =
           * URLDecoder.decode( MAKE_CPY, "UTF-8" ); commandMap.put( "MAKE_CPY", MAKE_CPY ); } if( !"".equals(
           * WRITER ) ){ WRITER = URLDecoder.decode( WRITER, "UTF-8" ); commandMap.put( "WRITER", WRITER ); }
           * if( !"".equals( PLAYER ) ){ PLAYER = URLDecoder.decode( PLAYER, "UTF-8" ); commandMap.put(
           * "PLAYER", PLAYER ); } if( !"".equals( WRITER ) ){ WRITER = URLDecoder.decode( WRITER, "UTF-8" );
           * commandMap.put( "WRITER", WRITER ); } if( !"".equals( COMM_MGNT_CD ) ){ COMM_MGNT_CD =
           * URLDecoder.decode( COMM_MGNT_CD, "UTF-8" ); commandMap.put( "COMM_MGNT_CD", COMM_MGNT_CD ); }
           * if(BORD_GENRE_CD == "없음" || "없음".equals( BORD_GENRE_CD )){ commandMap.put( "BORD_GENRE_CD", "0"
           * ); } if(MAKE_YEAR == "없음" || "없음".equals( MAKE_YEAR )){ commandMap.put( "MAKE_YEAR", "0" ); }
           * if(BROD_ORD_SEQ == "없음" || "없음".equals( BROD_ORD_SEQ )){ commandMap.put( "BROD_ORD_SEQ", "0" ); }
           * if(COMM_MGNT_CD == "없음" || "없음".equals( COMM_MGNT_CD )){ commandMap.put( "COMM_MGNT_CD", "0" ); }
           * if( !"".equals( TOT_CNT ) ){ TOT_CNT = URLDecoder.decode( TOT_CNT, "UTF-8" ); commandMap.put(
           * "TOT_CNT", TOT_CNT ); } }else if(GENRE_CODE == "6" || GENRE_CODE.equals( "6" )){ String
           * RGST_ORGN_NAME = EgovWebUtil.getString( commandMap, "RGST_ORGN_NAME" ); String COMM_WORKS_ID =
           * EgovWebUtil.getString( commandMap, "COMM_WORKS_ID" ); String WORKS_TITLE = EgovWebUtil.getString(
           * commandMap, "WORKS_TITLE" ); String PAPE_NO = EgovWebUtil.getString( commandMap, "PAPE_NO" );
           * String PAPE_KIND = EgovWebUtil.getString( commandMap, "PAPE_KIND" ); String CONTRIBUTOR =
           * EgovWebUtil.getString( commandMap, "CONTRIBUTOR" ); String REPOTER = EgovWebUtil.getString(
           * commandMap, "REPOTER" ); String REVISION_ID = EgovWebUtil.getString( commandMap, "REVISION_ID" );
           * String ARTICL_DATE = EgovWebUtil.getString( commandMap, "ARTICL_DATE" ); String ARTICL_ISSU_DTTM
           * = EgovWebUtil.getString( commandMap, "ARTICL_ISSU_DTTM" ); String PROVIDER =
           * EgovWebUtil.getString( commandMap, "PROVIDER" ); String COMM_MGNT_CD = EgovWebUtil.getString(
           * commandMap, "COMM_MGNT_CD" ); String TOT_CNT = EgovWebUtil.getString( commandMap, "tot_cnt" );
           * System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT); if( !"".equals(
           * RGST_ORGN_NAME ) ){ RGST_ORGN_NAME = URLDecoder.decode( RGST_ORGN_NAME, "UTF-8" );
           * commandMap.put( "RGST_ORGN_NAME", RGST_ORGN_NAME ); } if( !"".equals( COMM_WORKS_ID ) ){
           * COMM_WORKS_ID = URLDecoder.decode( COMM_WORKS_ID, "UTF-8" ); commandMap.put( "COMM_WORKS_ID",
           * COMM_WORKS_ID ); } if( !"".equals( WORKS_TITLE ) ){ WORKS_TITLE = URLDecoder.decode( WORKS_TITLE,
           * "UTF-8" ); commandMap.put( "WORKS_TITLE", WORKS_TITLE ); } if( !"".equals( PAPE_NO ) ){ PAPE_NO =
           * URLDecoder.decode( PAPE_NO, "UTF-8" ); commandMap.put( "PAPE_NO", PAPE_NO ); } if( !"".equals(
           * PAPE_KIND ) ){ PAPE_KIND = URLDecoder.decode( PAPE_KIND, "UTF-8" ); commandMap.put( "PAPE_KIND",
           * PAPE_KIND ); } if( !"".equals( CONTRIBUTOR ) ){ CONTRIBUTOR = URLDecoder.decode( CONTRIBUTOR,
           * "UTF-8" ); commandMap.put( "CONTRIBUTOR", CONTRIBUTOR ); } if( !"".equals( REPOTER ) ){ REPOTER =
           * URLDecoder.decode( REPOTER, "UTF-8" ); commandMap.put( "REPOTER", REPOTER ); } if( !"".equals(
           * REVISION_ID ) ){ REVISION_ID = URLDecoder.decode( REVISION_ID, "UTF-8" ); commandMap.put(
           * "REVISION_ID", REVISION_ID ); } if( !"".equals( ARTICL_DATE ) ){ ARTICL_DATE = URLDecoder.decode(
           * ARTICL_DATE, "UTF-8" ); commandMap.put( "ARTICL_DATE", ARTICL_DATE ); } if( !"".equals(
           * ARTICL_ISSU_DTTM ) ){ ARTICL_ISSU_DTTM = URLDecoder.decode( ARTICL_ISSU_DTTM, "UTF-8" );
           * commandMap.put( "ARTICL_ISSU_DTTM", ARTICL_ISSU_DTTM ); } if( !"".equals( PROVIDER ) ){ PROVIDER
           * = URLDecoder.decode( PROVIDER, "UTF-8" ); commandMap.put( "PROVIDER", PROVIDER ); } if(
           * !"".equals( COMM_MGNT_CD ) ){ COMM_MGNT_CD = URLDecoder.decode( COMM_MGNT_CD, "UTF-8" );
           * commandMap.put( "COMM_MGNT_CD", COMM_MGNT_CD ); } if(ARTICL_DATE == "없음" || "없음".equals(
           * ARTICL_DATE )){ commandMap.put( "ARTICL_DATE", "0" ); } if(COMM_MGNT_CD == "없음" || "없음".equals(
           * COMM_MGNT_CD )){ commandMap.put( "COMM_MGNT_CD", "0" ); } if( !"".equals( TOT_CNT ) ){ TOT_CNT =
           * URLDecoder.decode( TOT_CNT, "UTF-8" ); commandMap.put( "TOT_CNT", TOT_CNT ); } }else
           * if(GENRE_CODE == "7" || GENRE_CODE.equals( "7" )){ String RGST_ORGN_NAME = EgovWebUtil.getString(
           * commandMap, "RGST_ORGN_NAME" ); String COMM_WORKS_ID = EgovWebUtil.getString( commandMap,
           * "COMM_WORKS_ID" ); String WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
           * String WORKS_SUB_TITLE = EgovWebUtil.getString( commandMap, "WORKS_SUB_TITLE" ); String KIND =
           * EgovWebUtil.getString( commandMap, "KIND" ); String MAKE_DATE = EgovWebUtil.getString(
           * commandMap, "MAKE_DATE" ); String SOURCE_INFO = EgovWebUtil.getString( commandMap, "SOURCE_INFO"
           * ); String SIZE_INFO = EgovWebUtil.getString( commandMap, "SIZE_INFO" ); String RESOLUTION =
           * EgovWebUtil.getString( commandMap, "RESOLUTION" ); String MAIN_MTRL = EgovWebUtil.getString(
           * commandMap, "MAIN_MTRL" ); String TXTR = EgovWebUtil.getString( commandMap, "TXTR" ); String
           * IMG_URL = EgovWebUtil.getString( commandMap, "IMG_URL" ); String DETL_URL =
           * EgovWebUtil.getString( commandMap, "DETL_URL" ); String WRITER = EgovWebUtil.getString(
           * commandMap, "WRITER" ); String POSS_DATE = EgovWebUtil.getString( commandMap, "POSS_DATE" );
           * String POSS_ORGN_NAME = EgovWebUtil.getString( commandMap, "POSS_ORGN_NAME" ); String
           * COMM_MGNT_CD = EgovWebUtil.getString( commandMap, "COMM_MGNT_CD" ); String TOT_CNT =
           * EgovWebUtil.getString( commandMap, "tot_cnt" );
           * System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT); if( !"".equals(
           * RGST_ORGN_NAME ) ){ RGST_ORGN_NAME = URLDecoder.decode( RGST_ORGN_NAME, "UTF-8" );
           * commandMap.put( "RGST_ORGN_NAME", RGST_ORGN_NAME ); } if( !"".equals( COMM_WORKS_ID ) ){
           * COMM_WORKS_ID = URLDecoder.decode( COMM_WORKS_ID, "UTF-8" ); commandMap.put( "COMM_WORKS_ID",
           * COMM_WORKS_ID ); } if( !"".equals( WORKS_TITLE ) ){ WORKS_TITLE = URLDecoder.decode( WORKS_TITLE,
           * "UTF-8" ); commandMap.put( "WORKS_TITLE", WORKS_TITLE ); } if( !"".equals( WORKS_SUB_TITLE ) ){
           * WORKS_SUB_TITLE = URLDecoder.decode( WORKS_SUB_TITLE, "UTF-8" ); commandMap.put(
           * "WORKS_SUB_TITLE", WORKS_SUB_TITLE ); } if( !"".equals( KIND ) ){ KIND = URLDecoder.decode( KIND,
           * "UTF-8" ); commandMap.put( "KIND", KIND ); } if( !"".equals( MAKE_DATE ) ){ MAKE_DATE =
           * URLDecoder.decode( MAKE_DATE, "UTF-8" ); commandMap.put( "MAKE_DATE", MAKE_DATE ); } if(
           * !"".equals( SOURCE_INFO ) ){ SOURCE_INFO = URLDecoder.decode( SOURCE_INFO, "UTF-8" );
           * commandMap.put( "SOURCE_INFO", SOURCE_INFO ); } if( !"".equals( SIZE_INFO ) ){ SIZE_INFO =
           * URLDecoder.decode( SIZE_INFO, "UTF-8" ); commandMap.put( "SIZE_INFO", SIZE_INFO ); } if(
           * !"".equals( RESOLUTION ) ){ RESOLUTION = URLDecoder.decode( RESOLUTION, "UTF-8" );
           * commandMap.put( "RESOLUTION", RESOLUTION ); } if( !"".equals( MAIN_MTRL ) ){ MAIN_MTRL =
           * URLDecoder.decode( MAIN_MTRL, "UTF-8" ); commandMap.put( "MAIN_MTRL", MAIN_MTRL ); } if(
           * !"".equals( TXTR ) ){ TXTR = URLDecoder.decode( TXTR, "UTF-8" ); commandMap.put( "TXTR", TXTR );
           * } if( !"".equals( IMG_URL ) ){ IMG_URL = URLDecoder.decode( IMG_URL, "UTF-8" ); commandMap.put(
           * "IMG_URL", IMG_URL ); } if( !"".equals( DETL_URL ) ){ DETL_URL = URLDecoder.decode( DETL_URL,
           * "UTF-8" ); commandMap.put( "DETL_URL", DETL_URL ); } if( !"".equals( WRITER ) ){ WRITER =
           * URLDecoder.decode( WRITER, "UTF-8" ); commandMap.put( "WRITER", WRITER ); } if( !"".equals(
           * POSS_DATE ) ){ POSS_DATE = URLDecoder.decode( POSS_DATE, "UTF-8" ); commandMap.put( "POSS_DATE",
           * POSS_DATE ); } if( !"".equals( POSS_ORGN_NAME ) ){ POSS_ORGN_NAME = URLDecoder.decode(
           * POSS_ORGN_NAME, "UTF-8" ); commandMap.put( "POSS_ORGN_NAME", POSS_ORGN_NAME ); } if( !"".equals(
           * COMM_MGNT_CD ) ){ COMM_MGNT_CD = URLDecoder.decode( COMM_MGNT_CD, "UTF-8" ); commandMap.put(
           * "COMM_MGNT_CD", COMM_MGNT_CD ); } if(MAKE_DATE == "없음" || "없음".equals( MAKE_DATE )){
           * commandMap.put( "MAKE_DATE", "0" ); } if(IMG_URL == "없음" || "없음".equals( IMG_URL )){
           * commandMap.put( "IMG_URL", "0" ); } if(DETL_URL == "없음" || "없음".equals( DETL_URL )){
           * commandMap.put( "DETL_URL", "0" ); } if(POSS_DATE == "없음" || "없음".equals( POSS_DATE )){
           * commandMap.put( "POSS_DATE", "0" ); } if(COMM_MGNT_CD == "없음" || "없음".equals( COMM_MGNT_CD )){
           * commandMap.put( "COMM_MGNT_CD", "0" ); } if( !"".equals( TOT_CNT ) ){ TOT_CNT =
           * URLDecoder.decode( TOT_CNT, "UTF-8" ); commandMap.put( "TOT_CNT", TOT_CNT ); } }else
           * if(GENRE_CODE == "8" || GENRE_CODE.equals( "8" )){ String RGST_ORGN_NAME = EgovWebUtil.getString(
           * commandMap, "RGST_ORGN_NAME" ); String COMM_WORKS_ID = EgovWebUtil.getString( commandMap,
           * "COMM_WORKS_ID" ); String WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
           * String IMAGE_DESC = EgovWebUtil.getString( commandMap, "IMAGE_DESC" ); String KEYWORD =
           * EgovWebUtil.getString( commandMap, "KEYWORD" ); String CRT_YEAR = EgovWebUtil.getString(
           * commandMap, "CRT_YEAR" ); String WRITER = EgovWebUtil.getString( commandMap, "WRITER" ); String
           * PRODUCER = EgovWebUtil.getString( commandMap, "PRODUCER" ); String IMAGE_URL =
           * EgovWebUtil.getString( commandMap, "IMAGE_URL" ); String IMAGE_OPEN_YN = EgovWebUtil.getString(
           * commandMap, "IMAGE_OPEN_YN" ); String ICNX_NUMB = EgovWebUtil.getString( commandMap, "ICNX_NUMB"
           * ); String COMM_MGNT_CD = EgovWebUtil.getString( commandMap, "COMM_MGNT_CD" ); String TOT_CNT =
           * EgovWebUtil.getString( commandMap, "tot_cnt" );
           * System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT); if( !"".equals(
           * RGST_ORGN_NAME ) ){ RGST_ORGN_NAME = URLDecoder.decode( RGST_ORGN_NAME, "UTF-8" );
           * commandMap.put( "RGST_ORGN_NAME", RGST_ORGN_NAME ); } if( !"".equals( COMM_WORKS_ID ) ){
           * COMM_WORKS_ID = URLDecoder.decode( COMM_WORKS_ID, "UTF-8" ); commandMap.put( "COMM_WORKS_ID",
           * COMM_WORKS_ID ); } if( !"".equals( WORKS_TITLE ) ){ WORKS_TITLE = URLDecoder.decode( WORKS_TITLE,
           * "UTF-8" ); commandMap.put( "WORKS_TITLE", WORKS_TITLE ); } if( !"".equals( IMAGE_DESC ) ){
           * IMAGE_DESC = URLDecoder.decode( IMAGE_DESC, "UTF-8" ); commandMap.put( "IMAGE_DESC", IMAGE_DESC
           * ); } if( !"".equals( KEYWORD ) ){ KEYWORD = URLDecoder.decode( KEYWORD, "UTF-8" );
           * commandMap.put( "KEYWORD", KEYWORD ); } if( !"".equals( CRT_YEAR ) ){ CRT_YEAR =
           * URLDecoder.decode( CRT_YEAR, "UTF-8" ); commandMap.put( "CRT_YEAR", CRT_YEAR ); } if( !"".equals(
           * WRITER ) ){ WRITER = URLDecoder.decode( WRITER, "UTF-8" ); commandMap.put( "WRITER", WRITER ); }
           * if( !"".equals( PRODUCER ) ){ PRODUCER = URLDecoder.decode( PRODUCER, "UTF-8" ); commandMap.put(
           * "PRODUCER", PRODUCER ); } if( !"".equals( IMAGE_URL ) ){ IMAGE_URL = URLDecoder.decode(
           * IMAGE_URL, "UTF-8" ); commandMap.put( "IMAGE_URL", IMAGE_URL ); } if( !"".equals( IMAGE_OPEN_YN )
           * ){ IMAGE_OPEN_YN = URLDecoder.decode( IMAGE_OPEN_YN, "UTF-8" ); commandMap.put( "IMAGE_OPEN_YN",
           * IMAGE_OPEN_YN ); } if( !"".equals( ICNX_NUMB ) ){ ICNX_NUMB = URLDecoder.decode( ICNX_NUMB,
           * "UTF-8" ); commandMap.put( "ICNX_NUMB", ICNX_NUMB ); } if( !"".equals( COMM_MGNT_CD ) ){
           * COMM_MGNT_CD = URLDecoder.decode( COMM_MGNT_CD, "UTF-8" ); commandMap.put( "COMM_MGNT_CD",
           * COMM_MGNT_CD ); } if(IMAGE_DESC == "없음" || "없음".equals( IMAGE_DESC )){ commandMap.put(
           * "IMAGE_DESC", "0" ); } if(KEYWORD == "없음" || "없음".equals( KEYWORD )){ commandMap.put( "KEYWORD",
           * "0" ); } if(IMAGE_URL == "없음" || "없음".equals( IMAGE_URL )){ commandMap.put( "IMAGE_URL", "0" ); }
           * if(COMM_MGNT_CD == "없음" || "없음".equals( COMM_MGNT_CD )){ commandMap.put( "COMM_MGNT_CD", "0" ); }
           * if( !"".equals( TOT_CNT ) ){ TOT_CNT = URLDecoder.decode( TOT_CNT, "UTF-8" ); commandMap.put(
           * "TOT_CNT", TOT_CNT ); } }else if(GENRE_CODE == "99" || GENRE_CODE.equals( "99" )){ String
           * RGST_ORGN_NAME = EgovWebUtil.getString( commandMap, "RGST_ORGN_NAME" ); String COMM_WORKS_ID =
           * EgovWebUtil.getString( commandMap, "COMM_WORKS_ID" ); String SIDE_GENRE_CD =
           * EgovWebUtil.getString( commandMap, "SIDE_GENRE_CD" ); String WORKS_TITLE = EgovWebUtil.getString(
           * commandMap, "WORKS_TITLE" ); String COPT_NAME = EgovWebUtil.getString( commandMap, "COPT_NAME" );
           * String CRT_YEAR = EgovWebUtil.getString( commandMap, "CRT_YEAR" ); String PUBL_MEDI =
           * EgovWebUtil.getString( commandMap, "PUBL_MEDI" ); String PUBL_DATE = EgovWebUtil.getString(
           * commandMap, "PUBL_DATE" ); String COMM_MGNT_CD = EgovWebUtil.getString( commandMap,
           * "COMM_MGNT_CD" ); String TOT_CNT = EgovWebUtil.getString( commandMap, "tot_cnt" );
           * System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT); if( !"".equals(
           * RGST_ORGN_NAME ) ){ RGST_ORGN_NAME = URLDecoder.decode( RGST_ORGN_NAME, "UTF-8" );
           * commandMap.put( "RGST_ORGN_NAME", RGST_ORGN_NAME ); } if( !"".equals( COMM_WORKS_ID ) ){
           * COMM_WORKS_ID = URLDecoder.decode( COMM_WORKS_ID, "UTF-8" ); commandMap.put( "COMM_WORKS_ID",
           * COMM_WORKS_ID ); } if( !"".equals( WORKS_TITLE ) ){ WORKS_TITLE = URLDecoder.decode( WORKS_TITLE,
           * "UTF-8" ); commandMap.put( "WORKS_TITLE", WORKS_TITLE ); } if( !"".equals( SIDE_GENRE_CD ) ){
           * SIDE_GENRE_CD = URLDecoder.decode( SIDE_GENRE_CD, "UTF-8" ); commandMap.put( "SIDE_GENRE_CD",
           * SIDE_GENRE_CD ); } if( !"".equals( COPT_NAME ) ){ COPT_NAME = URLDecoder.decode( COPT_NAME,
           * "UTF-8" ); commandMap.put( "COPT_NAME", COPT_NAME ); } if( !"".equals( CRT_YEAR ) ){ CRT_YEAR =
           * URLDecoder.decode( CRT_YEAR, "UTF-8" ); commandMap.put( "CRT_YEAR", CRT_YEAR ); } if( !"".equals(
           * PUBL_MEDI ) ){ PUBL_MEDI = URLDecoder.decode( PUBL_MEDI, "UTF-8" ); commandMap.put( "PUBL_MEDI",
           * PUBL_MEDI ); } if( !"".equals( PUBL_DATE ) ){ PUBL_DATE = URLDecoder.decode( PUBL_DATE, "UTF-8"
           * ); commandMap.put( "PUBL_DATE", PUBL_DATE ); } if( !"".equals( COMM_MGNT_CD ) ){ COMM_MGNT_CD =
           * URLDecoder.decode( COMM_MGNT_CD, "UTF-8" ); commandMap.put( "COMM_MGNT_CD", COMM_MGNT_CD ); }
           * if(PUBL_DATE == "없음" || "없음".equals( PUBL_DATE )){ commandMap.put( "PUBL_DATE", "0" ); }
           * if(COMM_MGNT_CD == "없음" || "없음".equals( COMM_MGNT_CD )){ commandMap.put( "COMM_MGNT_CD", "0" ); }
           * if( !"".equals( TOT_CNT ) ){ TOT_CNT = URLDecoder.decode( TOT_CNT, "UTF-8" ); commandMap.put(
           * "TOT_CNT", TOT_CNT ); } }
           */

          ArrayList<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
          excelList.add(commandMap);
          boolean isSuccess = fdcrAd17Service.fdcrAd17Insert1(commandMap, excelList);
          returnAjaxString(response, isSuccess);

          if (isSuccess) {
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17InsertAgain.page")
     public String fdcrAd17InsertAgain(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 담을 arrayList
          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
          // 실패리스트
          ArrayList<Map<String, Object>> uploadFailList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          String genreCode = EgovWebUtil.getString(commandMap, "GENRE_CODE");
          String reptChrrName = EgovWebUtil.getString(commandMap, "REPT_CHRR_NAME");
          String reptChrrPosi = EgovWebUtil.getString(commandMap, "REPT_CHRR_POSI");
          String reptChrrTelx = EgovWebUtil.getString(commandMap, "REPT_CHRR_TELX");
          String reptChrrMail = EgovWebUtil.getString(commandMap, "REPT_CHRR_MAIL");

          Map<String, Object> exMap = new HashMap<String, Object>();
          Map<String, Object> failMap = new HashMap<String, Object>();

          if (genreCode == "2" || genreCode.equals("2")) {

               exMap.put("genreCode", genreCode);
               exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
               exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
               exMap.put("USER_NAME", ConsoleLoginUser.getUserName());
               exMap.put("reptChrrName", reptChrrName);
               exMap.put("reptChrrPosi", reptChrrPosi);
               exMap.put("reptChrrTelx", reptChrrTelx);
               exMap.put("reptChrrMail", reptChrrMail);

               String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
               String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
               String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
               String NATION_CD = EgovWebUtil.getString(commandMap, "NATION_CD");
               String WORKS_SUB_TITLE = EgovWebUtil.getString(commandMap, "WORKS_SUB_TITLE");
               String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
               String BOOK_TITLE = EgovWebUtil.getString(commandMap, "BOOK_TITLE");
               String BOOK_PUBLISHER = EgovWebUtil.getString(commandMap, "BOOK_PUBLISHER");
               String BOOK_ISSU_YEAR = EgovWebUtil.getString(commandMap, "BOOK_ISSU_YEAR");
               String COPT_NAME = EgovWebUtil.getString(commandMap, "COPT_NAME");
               String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
               String TOT_CNT = EgovWebUtil.getString(commandMap, "tot_cnt");
               // System.out.println("TOT_CNT::::::::::::::::::::::::::::::::"+TOT_CNT);

               if (!"".equals(RGST_ORGN_NAME)) {
                    RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
                    exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               }
               if (!"".equals(COMM_WORKS_ID)) {
                    COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
                    exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               }
               if (!"".equals(WORKS_TITLE)) {
                    WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
                    exMap.put("WORKS_TITLE", WORKS_TITLE);
               }
               if (!"".equals(NATION_CD)) {
                    NATION_CD = URLDecoder.decode(NATION_CD, "UTF-8");
                    exMap.put("NATION_CD", NATION_CD);
               }
               if (!"".equals(WORKS_SUB_TITLE)) {
                    WORKS_SUB_TITLE = URLDecoder.decode(WORKS_SUB_TITLE, "UTF-8");
                    exMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
               }
               if (!"".equals(CRT_YEAR)) {
                    CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
                    exMap.put("CRT_YEAR", CRT_YEAR);
               }
               if (!"".equals(BOOK_TITLE)) {
                    BOOK_TITLE = URLDecoder.decode(BOOK_TITLE, "UTF-8");
                    exMap.put("BOOK_TITLE", BOOK_TITLE);
               }
               if (!"".equals(BOOK_PUBLISHER)) {
                    BOOK_PUBLISHER = URLDecoder.decode(BOOK_PUBLISHER, "UTF-8");
                    exMap.put("BOOK_PUBLISHER", BOOK_PUBLISHER);
               }
               if (!"".equals(BOOK_ISSU_YEAR)) {
                    BOOK_ISSU_YEAR = URLDecoder.decode(BOOK_ISSU_YEAR, "UTF-8");
                    exMap.put("BOOK_ISSU_YEAR", BOOK_ISSU_YEAR);
               }
               if (!"".equals(COPT_NAME)) {
                    COPT_NAME = URLDecoder.decode(COPT_NAME, "UTF-8");
                    exMap.put("COPT_NAME", COPT_NAME);
               }
               if (!"".equals(COMM_MGNT_CD)) {
                    COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
                    exMap.put("COMP", COMM_MGNT_CD);
               }
               if (CRT_YEAR == "없음" || "없음".equals(CRT_YEAR)) {
                    exMap.put("CRT_YEAR", "0");
               }
               if (BOOK_ISSU_YEAR == "없음" || "없음".equals(BOOK_ISSU_YEAR)) {
                    exMap.put("BOOK_ISSU_YEAR", "0");
               }
               if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
                    exMap.put("COMM_MGNT_CD", "0");
               }
               if (!"".equals(TOT_CNT)) {
                    TOT_CNT = URLDecoder.decode(TOT_CNT, "UTF-8");
                    exMap.put("TOT_CNT", TOT_CNT);
               }

               Set key2 = exMap.keySet();
               int errCnt = 0;
               Iterator iterator = key2.iterator();
               while (iterator.hasNext()) {
                    Object exKey = iterator.next();
                    // System.out.println(exKey + ": " + exMap.get(exKey));
               }

               Iterator<String> keySetIteratorFail = failMap.keySet().iterator();
               while (keySetIteratorFail.hasNext()) {
                    String failKey = keySetIteratorFail.next();
                    // System.out.println(failKey + ": " + exMap.get(failKey));
                    errCnt++;
               }

               if (errCnt < 1) {
                    uploadList.add(exMap);

                    boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
                    returnAjaxString(response, isSuccess);
                    exMap.clear();
               } else {
                    // uploadFailList.add(failMap);
                    uploadFailList.add(failMap);
               }
          }

          return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");
     }

     /**
      * 음악 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17MusicInsert.page")
     public String fdcrAd17MusicInsert(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "1");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String NATION_CD = EgovWebUtil.getString(commandMap, "NATION_CD");
          String ALBUM_TITLE = EgovWebUtil.getString(commandMap, "ALBUM_TITLE");
          String ALBUM_LABLE = EgovWebUtil.getString(commandMap, "ALBUM_LABLE");
          String DISK_SIDE = EgovWebUtil.getString(commandMap, "DISK_SIDE");
          String TRACK_NO = EgovWebUtil.getString(commandMap, "TRACK_NO");
          String ALBUM_ISSU_YEAR = EgovWebUtil.getString(commandMap, "ALBUM_ISSU_YEAR");
          String LYRC = EgovWebUtil.getString(commandMap, "LYRC");
          String COMP = EgovWebUtil.getString(commandMap, "COMP");
          String ARRA = EgovWebUtil.getString(commandMap, "ARRA");
          String TRAN = EgovWebUtil.getString(commandMap, "TRAN");
          String SING = EgovWebUtil.getString(commandMap, "SING");
          String PERF = EgovWebUtil.getString(commandMap, "PERF");
          String PROD = EgovWebUtil.getString(commandMap, "PROD");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String LICE_MGNT_CD = EgovWebUtil.getString(commandMap, "LICE_MGNT_CD");
          String PERF_MGNT_CD = EgovWebUtil.getString(commandMap, "PERF_MGNT_CD");
          String PROD_MGNT_CD = EgovWebUtil.getString(commandMap, "PROD_MGNT_CD");
          String UCI = EgovWebUtil.getString(commandMap, "UCI");
          String RATIO = EgovWebUtil.getString(commandMap, "RATIO");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "1");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(NATION_CD)) {
               NATION_CD = URLDecoder.decode(NATION_CD, "UTF-8");
               commandMap.put("NATION_CD", NATION_CD);
               exMap.put("NATION_CD", NATION_CD);
          }
          if (!"".equals(ALBUM_TITLE)) {
               ALBUM_TITLE = URLDecoder.decode(ALBUM_TITLE, "UTF-8");
               commandMap.put("ALBUM_TITLE", ALBUM_TITLE);
               exMap.put("ALBUM_TITLE", ALBUM_TITLE);
          }
          if (!"".equals(ALBUM_LABLE)) {
               ALBUM_LABLE = URLDecoder.decode(ALBUM_LABLE, "UTF-8");
               commandMap.put("ALBUM_LABLE", ALBUM_LABLE);
               exMap.put("ALBUM_LABLE", ALBUM_LABLE);
          }
          if (!"".equals(DISK_SIDE)) {
               DISK_SIDE = URLDecoder.decode(DISK_SIDE, "UTF-8");
               commandMap.put("DISK_SIDE", DISK_SIDE);
               exMap.put("DISK_SIDE", DISK_SIDE);
          }
          if (!"".equals(TRACK_NO)) {
               TRACK_NO = URLDecoder.decode(TRACK_NO, "UTF-8");
               commandMap.put("TRACK_NO", TRACK_NO);
               exMap.put("TRACK_NO", TRACK_NO);
          }
          if (!"".equals(ALBUM_ISSU_YEAR)) {
               ALBUM_ISSU_YEAR = URLDecoder.decode(ALBUM_ISSU_YEAR, "UTF-8");
               commandMap.put("ALBUM_ISSU_YEAR", ALBUM_ISSU_YEAR);
               exMap.put("ALBUM_ISSU_YEAR", ALBUM_ISSU_YEAR);
          }
          if (!"".equals(LYRC)) {
               LYRC = URLDecoder.decode(LYRC, "UTF-8");
               commandMap.put("LYRC", LYRC);
               exMap.put("LYRC", LYRC);
          }
          if (!"".equals(COMP)) {
               COMP = URLDecoder.decode(COMP, "UTF-8");
               commandMap.put("COMP", COMP);
               exMap.put("COMP", COMP);
          }
          if (!"".equals(ARRA)) {
               ARRA = URLDecoder.decode(ARRA, "UTF-8");
               commandMap.put("ARRA", ARRA);
               exMap.put("ARRA", ARRA);
          }
          if (!"".equals(TRAN)) {
               TRAN = URLDecoder.decode(TRAN, "UTF-8");
               commandMap.put("TRAN", TRAN);
               exMap.put("TRAN", TRAN);
          }
          if (!"".equals(SING)) {
               SING = URLDecoder.decode(SING, "UTF-8");
               commandMap.put("SING", SING);
               exMap.put("SING", SING);
          }
          if (!"".equals(PERF)) {
               PERF = URLDecoder.decode(PERF, "UTF-8");
               commandMap.put("PERF", PERF);
               exMap.put("PERF", PERF);
          }
          if (!"".equals(PROD)) {
               PROD = URLDecoder.decode(PROD, "UTF-8");
               commandMap.put("PROD", PROD);
               exMap.put("PROD", PROD);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(LICE_MGNT_CD)) {
               LICE_MGNT_CD = URLDecoder.decode(LICE_MGNT_CD, "UTF-8");
               commandMap.put("LICE_MGNT_CD", LICE_MGNT_CD);
               exMap.put("LICE_MGNT_CD", LICE_MGNT_CD);
          }
          if (!"".equals(PERF_MGNT_CD)) {
               PERF_MGNT_CD = URLDecoder.decode(PERF_MGNT_CD, "UTF-8");
               commandMap.put("PERF_MGNT_CD", PERF_MGNT_CD);
               exMap.put("PERF_MGNT_CD", PERF_MGNT_CD);
          }
          if (!"".equals(PROD_MGNT_CD)) {
               PROD_MGNT_CD = URLDecoder.decode(PROD_MGNT_CD, "UTF-8");
               commandMap.put("PROD_MGNT_CD", PROD_MGNT_CD);
               exMap.put("PROD_MGNT_CD", PROD_MGNT_CD);
          }
          if (!"".equals(UCI)) {
               UCI = URLDecoder.decode(UCI, "UTF-8");
               commandMap.put("UCI", UCI);
               exMap.put("UCI", UCI);
          }
          if (!"".equals(RATIO)) {
               RATIO = URLDecoder.decode(RATIO, "UTF-8");
               commandMap.put("RATIO", RATIO);
               exMap.put("RATIO", RATIO);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }

          if (NATION_CD == "없음" || "없음".equals(NATION_CD)) {
               commandMap.put("NATION_CD", "0");
               exMap.put("NATION_CD", "0");
          }
          if (DISK_SIDE == "없음" || "없음".equals(DISK_SIDE)) {
               commandMap.put("DISK_SIDE", "0");
               exMap.put("DISK_SIDE", "0");
          }
          if (TRACK_NO == "없음" || "없음".equals(TRACK_NO)) {
               commandMap.put("TRACK_NO", "0");
               exMap.put("TRACK_NO", "0");
          }
          if (ALBUM_ISSU_YEAR == "없음" || "없음".equals(ALBUM_ISSU_YEAR)) {
               commandMap.put("ALBUM_ISSU_YEAR", "0");
               exMap.put("ALBUM_ISSU_YEAR", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (LICE_MGNT_CD == "없음" || "없음".equals(LICE_MGNT_CD)) {
               commandMap.put("LICE_MGNT_CD", "0");
               exMap.put("LICE_MGNT_CD", "0");
          }
          if (PERF_MGNT_CD == "없음" || "없음".equals(PERF_MGNT_CD)) {
               commandMap.put("PERF_MGNT_CD", "0");
               exMap.put("PERF_MGNT_CD", "0");
          }
          if (PROD_MGNT_CD == "없음" || "없음".equals(PROD_MGNT_CD)) {
               commandMap.put("PROD_MGNT_CD", "0");
               exMap.put("PROD_MGNT_CD", "0");
          }
          if (UCI == "없음" || "없음".equals(UCI)) {
               commandMap.put("UCI", "0");
               exMap.put("UCI", "0");
          }
          if (RATIO == "없음" || "없음".equals(RATIO)) {
               commandMap.put("RATIO", "0");
               exMap.put("RATIO", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 음악 일괄수정
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17MusicUpdate.page")
     public String fdcrAd17MusicUpdate(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "1");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String NATION_CD = EgovWebUtil.getString(commandMap, "NATION_CD");
          String ALBUM_TITLE = EgovWebUtil.getString(commandMap, "ALBUM_TITLE");
          String ALBUM_LABLE = EgovWebUtil.getString(commandMap, "ALBUM_LABLE");
          String DISK_SIDE = EgovWebUtil.getString(commandMap, "DISK_SIDE");
          String TRACK_NO = EgovWebUtil.getString(commandMap, "TRACK_NO");
          String ALBUM_ISSU_YEAR = EgovWebUtil.getString(commandMap, "ALBUM_ISSU_YEAR");
          String LYRC = EgovWebUtil.getString(commandMap, "LYRC");
          String COMP = EgovWebUtil.getString(commandMap, "COMP");
          String ARRA = EgovWebUtil.getString(commandMap, "ARRA");
          String TRAN = EgovWebUtil.getString(commandMap, "TRAN");
          String SING = EgovWebUtil.getString(commandMap, "SING");
          String PERF = EgovWebUtil.getString(commandMap, "PERF");
          String PROD = EgovWebUtil.getString(commandMap, "PROD");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String LICE_MGNT_CD = EgovWebUtil.getString(commandMap, "LICE_MGNT_CD");
          String PERF_MGNT_CD = EgovWebUtil.getString(commandMap, "PERF_MGNT_CD");
          String PROD_MGNT_CD = EgovWebUtil.getString(commandMap, "PROD_MGNT_CD");
          String UCI = EgovWebUtil.getString(commandMap, "UCI");
          String RATIO = EgovWebUtil.getString(commandMap, "RATIO");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "1");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(NATION_CD)) {
               NATION_CD = URLDecoder.decode(NATION_CD, "UTF-8");
               commandMap.put("NATION_CD", NATION_CD);
               exMap.put("NATION_CD", NATION_CD);
          }
          if (!"".equals(ALBUM_TITLE)) {
               ALBUM_TITLE = URLDecoder.decode(ALBUM_TITLE, "UTF-8");
               commandMap.put("ALBUM_TITLE", ALBUM_TITLE);
               exMap.put("ALBUM_TITLE", ALBUM_TITLE);
          }
          if (!"".equals(ALBUM_LABLE)) {
               ALBUM_LABLE = URLDecoder.decode(ALBUM_LABLE, "UTF-8");
               commandMap.put("ALBUM_LABLE", ALBUM_LABLE);
               exMap.put("ALBUM_LABLE", ALBUM_LABLE);
          }
          if (!"".equals(DISK_SIDE)) {
               DISK_SIDE = URLDecoder.decode(DISK_SIDE, "UTF-8");
               commandMap.put("DISK_SIDE", DISK_SIDE);
               exMap.put("DISK_SIDE", DISK_SIDE);
          }
          if (!"".equals(TRACK_NO)) {
               TRACK_NO = URLDecoder.decode(TRACK_NO, "UTF-8");
               commandMap.put("TRACK_NO", TRACK_NO);
               exMap.put("TRACK_NO", TRACK_NO);
          }
          if (!"".equals(ALBUM_ISSU_YEAR)) {
               ALBUM_ISSU_YEAR = URLDecoder.decode(ALBUM_ISSU_YEAR, "UTF-8");
               commandMap.put("ALBUM_ISSU_YEAR", ALBUM_ISSU_YEAR);
               exMap.put("ALBUM_ISSU_YEAR", ALBUM_ISSU_YEAR);
          }
          if (!"".equals(LYRC)) {
               LYRC = URLDecoder.decode(LYRC, "UTF-8");
               commandMap.put("LYRC", LYRC);
               exMap.put("LYRC", LYRC);
          }
          if (!"".equals(COMP)) {
               COMP = URLDecoder.decode(COMP, "UTF-8");
               commandMap.put("COMP", COMP);
               exMap.put("COMP", COMP);
          }
          if (!"".equals(ARRA)) {
               ARRA = URLDecoder.decode(ARRA, "UTF-8");
               commandMap.put("ARRA", ARRA);
               exMap.put("ARRA", ARRA);
          }
          if (!"".equals(TRAN)) {
               TRAN = URLDecoder.decode(TRAN, "UTF-8");
               commandMap.put("TRAN", TRAN);
               exMap.put("TRAN", TRAN);
          }
          if (!"".equals(SING)) {
               SING = URLDecoder.decode(SING, "UTF-8");
               commandMap.put("SING", SING);
               exMap.put("SING", SING);
          }
          if (!"".equals(PERF)) {
               PERF = URLDecoder.decode(PERF, "UTF-8");
               commandMap.put("PERF", PERF);
               exMap.put("PERF", PERF);
          }
          if (!"".equals(PROD)) {
               PROD = URLDecoder.decode(PROD, "UTF-8");
               commandMap.put("PROD", PROD);
               exMap.put("PROD", PROD);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               COMM_MGNT_CD = URLDecoder.decode(COMM_MGNT_CD, "UTF-8");
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(LICE_MGNT_CD)) {
               LICE_MGNT_CD = URLDecoder.decode(LICE_MGNT_CD, "UTF-8");
               commandMap.put("LICE_MGNT_CD", LICE_MGNT_CD);
               exMap.put("LICE_MGNT_CD", LICE_MGNT_CD);
          }
          if (!"".equals(PERF_MGNT_CD)) {
               PERF_MGNT_CD = URLDecoder.decode(PERF_MGNT_CD, "UTF-8");
               commandMap.put("PERF_MGNT_CD", PERF_MGNT_CD);
               exMap.put("PERF_MGNT_CD", PERF_MGNT_CD);
          }
          if (!"".equals(PROD_MGNT_CD)) {
               PROD_MGNT_CD = URLDecoder.decode(PROD_MGNT_CD, "UTF-8");
               commandMap.put("PROD_MGNT_CD", PROD_MGNT_CD);
               exMap.put("PROD_MGNT_CD", PROD_MGNT_CD);
          }
          if (!"".equals(UCI)) {
               UCI = URLDecoder.decode(UCI, "UTF-8");
               commandMap.put("UCI", UCI);
               exMap.put("UCI", UCI);
          }
          if (!"".equals(RATIO)) {
               RATIO = URLDecoder.decode(RATIO, "UTF-8");
               commandMap.put("RATIO", RATIO);
               exMap.put("RATIO", RATIO);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }

          if (NATION_CD == "없음" || "없음".equals(NATION_CD)) {
               commandMap.put("NATION_CD", "0");
               exMap.put("NATION_CD", "0");
          }
          if (DISK_SIDE == "없음" || "없음".equals(DISK_SIDE)) {
               commandMap.put("DISK_SIDE", "0");
               exMap.put("DISK_SIDE", "0");
          }
          if (TRACK_NO == "없음" || "없음".equals(TRACK_NO)) {
               commandMap.put("TRACK_NO", "0");
               exMap.put("TRACK_NO", "0");
          }
          if (ALBUM_ISSU_YEAR == "없음" || "없음".equals(ALBUM_ISSU_YEAR)) {
               commandMap.put("ALBUM_ISSU_YEAR", "0");
               exMap.put("ALBUM_ISSU_YEAR", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (LICE_MGNT_CD == "없음" || "없음".equals(LICE_MGNT_CD)) {
               commandMap.put("LICE_MGNT_CD", "0");
               exMap.put("LICE_MGNT_CD", "0");
          }
          if (PERF_MGNT_CD == "없음" || "없음".equals(PERF_MGNT_CD)) {
               commandMap.put("PERF_MGNT_CD", "0");
               exMap.put("PERF_MGNT_CD", "0");
          }
          if (PROD_MGNT_CD == "없음" || "없음".equals(PROD_MGNT_CD)) {
               commandMap.put("PROD_MGNT_CD", "0");
               exMap.put("PROD_MGNT_CD", "0");
          }
          if (UCI == "없음" || "없음".equals(UCI)) {
               commandMap.put("UCI", "0");
               exMap.put("UCI", "0");
          }
          if (RATIO == "없음" || "없음".equals(RATIO)) {
               commandMap.put("RATIO", "0");
               exMap.put("RATIO", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17UpdateNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 방송대본 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17ScriptInsert.page")
     public String fdcrAd17ScriptInsert(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "3");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String WORKS_ORIG_TITLE = EgovWebUtil.getString(commandMap, "WORKS_ORIG_TITLE");
          String SCRT_GENRE_CD = EgovWebUtil.getString(commandMap, "SCRT_GENRE_CD");
          String SCRP_SUBJ_CD = EgovWebUtil.getString(commandMap, "SCRP_SUBJ_CD");
          String ORIG_WRITER = EgovWebUtil.getString(commandMap, "ORIG_WRITER");
          String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
          String BROD_ORD_SEQ = EgovWebUtil.getString(commandMap, "BROD_ORD_SEQ");
          String BORD_DATE = EgovWebUtil.getString(commandMap, "BORD_DATE");
          String MAKE_CPY = EgovWebUtil.getString(commandMap, "MAKE_CPY");
          String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
          String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "3");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(WORKS_ORIG_TITLE)) {
               WORKS_ORIG_TITLE = URLDecoder.decode(WORKS_ORIG_TITLE, "UTF-8");
               commandMap.put("WORKS_ORIG_TITLE", WORKS_ORIG_TITLE);
               exMap.put("WORKS_ORIG_TITLE", WORKS_ORIG_TITLE);
          }
          if (!"".equals(SCRT_GENRE_CD)) {
               SCRT_GENRE_CD = URLDecoder.decode(SCRT_GENRE_CD, "UTF-8");
               commandMap.put("SCRT_GENRE_CD", SCRT_GENRE_CD);
               exMap.put("SCRT_GENRE_CD", SCRT_GENRE_CD);
          }
          if (!"".equals(SCRP_SUBJ_CD)) {
               SCRP_SUBJ_CD = URLDecoder.decode(SCRP_SUBJ_CD, "UTF-8");
               commandMap.put("SCRP_SUBJ_CD", SCRP_SUBJ_CD);
               exMap.put("SCRP_SUBJ_CD", SCRP_SUBJ_CD);
          }
          if (!"".equals(ORIG_WRITER)) {
               ORIG_WRITER = URLDecoder.decode(ORIG_WRITER, "UTF-8");
               commandMap.put("ORIG_WRITER", ORIG_WRITER);
               exMap.put("ORIG_WRITER", ORIG_WRITER);
          }
          if (!"".equals(PLAYER)) {
               PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
               commandMap.put("PLAYER", PLAYER);
               exMap.put("PLAYER", PLAYER);
          }
          if (!"".equals(BROD_ORD_SEQ)) {
               BROD_ORD_SEQ = URLDecoder.decode(BROD_ORD_SEQ, "UTF-8");
               commandMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
               exMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
          }
          if (!"".equals(BORD_DATE)) {
               BORD_DATE = URLDecoder.decode(BORD_DATE, "UTF-8");
               commandMap.put("BORD_DATE", BORD_DATE);
               exMap.put("BORD_DATE", BORD_DATE);
          }
          if (!"".equals(MAKE_CPY)) {
               MAKE_CPY = URLDecoder.decode(MAKE_CPY, "UTF-8");
               commandMap.put("MAKE_CPY", MAKE_CPY);
               exMap.put("MAKE_CPY", MAKE_CPY);
          }
          if (!"".equals(WRITER)) {
               WRITER = URLDecoder.decode(WRITER, "UTF-8");
               commandMap.put("WRITER", WRITER);
               exMap.put("WRITER", WRITER);
          }
          if (!"".equals(DIRECTOR)) {
               DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
               commandMap.put("DIRECTOR", DIRECTOR);
               exMap.put("DIRECTOR", DIRECTOR);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (SCRT_GENRE_CD == "없음" || "없음".equals(SCRT_GENRE_CD)) {
               commandMap.put("SCRT_GENRE_CD", "0");
               exMap.put("SCRT_GENRE_CD", "0");
          }
          if (SCRP_SUBJ_CD == "없음" || "없음".equals(SCRP_SUBJ_CD)) {
               commandMap.put("SCRP_SUBJ_CD", "0");
               exMap.put("SCRP_SUBJ_CD", "0");
          }
          if (BROD_ORD_SEQ == "없음" || "없음".equals(BROD_ORD_SEQ)) {
               commandMap.put("BROD_ORD_SEQ", "0");
               exMap.put("BROD_ORD_SEQ", "0");
          }
          if (BORD_DATE == "없음" || "없음".equals(BORD_DATE)) {
               commandMap.put("BORD_DATE", "0");
               exMap.put("BORD_DATE", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 방송대본 일괄수정
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17ScriptUpdate.page")
     public String fdcrAd17ScriptUpdate(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "3");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String WORKS_ORIG_TITLE = EgovWebUtil.getString(commandMap, "WORKS_ORIG_TITLE");
          String SCRT_GENRE_CD = EgovWebUtil.getString(commandMap, "SCRT_GENRE_CD");
          String SCRP_SUBJ_CD = EgovWebUtil.getString(commandMap, "SCRP_SUBJ_CD");
          String ORIG_WRITER = EgovWebUtil.getString(commandMap, "ORIG_WRITER");
          String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
          String BROD_ORD_SEQ = EgovWebUtil.getString(commandMap, "BROD_ORD_SEQ");
          String BORD_DATE = EgovWebUtil.getString(commandMap, "BORD_DATE");
          String MAKE_CPY = EgovWebUtil.getString(commandMap, "MAKE_CPY");
          String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
          String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "3");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(WORKS_ORIG_TITLE)) {
               WORKS_ORIG_TITLE = URLDecoder.decode(WORKS_ORIG_TITLE, "UTF-8");
               commandMap.put("WORKS_ORIG_TITLE", WORKS_ORIG_TITLE);
               exMap.put("WORKS_ORIG_TITLE", WORKS_ORIG_TITLE);
          }
          if (!"".equals(SCRT_GENRE_CD)) {
               SCRT_GENRE_CD = URLDecoder.decode(SCRT_GENRE_CD, "UTF-8");
               commandMap.put("SCRT_GENRE_CD", SCRT_GENRE_CD);
               exMap.put("SCRT_GENRE_CD", SCRT_GENRE_CD);
          }
          if (!"".equals(SCRP_SUBJ_CD)) {
               SCRP_SUBJ_CD = URLDecoder.decode(SCRP_SUBJ_CD, "UTF-8");
               commandMap.put("SCRP_SUBJ_CD", SCRP_SUBJ_CD);
               exMap.put("SCRP_SUBJ_CD", SCRP_SUBJ_CD);
          }
          if (!"".equals(ORIG_WRITER)) {
               ORIG_WRITER = URLDecoder.decode(ORIG_WRITER, "UTF-8");
               commandMap.put("ORIG_WRITER", ORIG_WRITER);
               exMap.put("ORIG_WRITER", ORIG_WRITER);
          }
          if (!"".equals(PLAYER)) {
               PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
               commandMap.put("PLAYER", PLAYER);
               exMap.put("PLAYER", PLAYER);
          }
          if (!"".equals(BROD_ORD_SEQ)) {
               BROD_ORD_SEQ = URLDecoder.decode(BROD_ORD_SEQ, "UTF-8");
               commandMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
               exMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
          }
          if (!"".equals(BORD_DATE)) {
               BORD_DATE = URLDecoder.decode(BORD_DATE, "UTF-8");
               commandMap.put("BORD_DATE", BORD_DATE);
               exMap.put("BORD_DATE", BORD_DATE);
          }
          if (!"".equals(MAKE_CPY)) {
               MAKE_CPY = URLDecoder.decode(MAKE_CPY, "UTF-8");
               commandMap.put("MAKE_CPY", MAKE_CPY);
               exMap.put("MAKE_CPY", MAKE_CPY);
          }
          if (!"".equals(WRITER)) {
               WRITER = URLDecoder.decode(WRITER, "UTF-8");
               commandMap.put("WRITER", WRITER);
               exMap.put("WRITER", WRITER);
          }
          if (!"".equals(DIRECTOR)) {
               DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
               commandMap.put("DIRECTOR", DIRECTOR);
               exMap.put("DIRECTOR", DIRECTOR);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (SCRT_GENRE_CD == "없음" || "없음".equals(SCRT_GENRE_CD)) {
               commandMap.put("SCRT_GENRE_CD", "0");
               exMap.put("SCRT_GENRE_CD", "0");
          }
          if (SCRP_SUBJ_CD == "없음" || "없음".equals(SCRP_SUBJ_CD)) {
               commandMap.put("SCRP_SUBJ_CD", "0");
               exMap.put("SCRP_SUBJ_CD", "0");
          }
          if (BROD_ORD_SEQ == "없음" || "없음".equals(BROD_ORD_SEQ)) {
               commandMap.put("BROD_ORD_SEQ", "0");
               exMap.put("BROD_ORD_SEQ", "0");
          }
          if (BORD_DATE == "없음" || "없음".equals(BORD_DATE)) {
               commandMap.put("BORD_DATE", "0");
               exMap.put("BORD_DATE", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17UpdateNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }


     /**
      * 어문 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17BookInsert.page")
     public String fdcrAd17BookInsert(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "2");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String NATION_CD = EgovWebUtil.getString(commandMap, "NATION_CD");
          String WORKS_SUB_TITLE = EgovWebUtil.getString(commandMap, "WORKS_SUB_TITLE");
          String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
          String BOOK_TITLE = EgovWebUtil.getString(commandMap, "BOOK_TITLE");
          String BOOK_PUBLISHER = EgovWebUtil.getString(commandMap, "BOOK_PUBLISHER");
          String BOOK_ISSU_YEAR = EgovWebUtil.getString(commandMap, "BOOK_ISSU_YEAR");
          String COPT_NAME = EgovWebUtil.getString(commandMap, "COPT_NAME");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "2");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(NATION_CD)) {
               NATION_CD = URLDecoder.decode(NATION_CD, "UTF-8");
               commandMap.put("NATION_CD", NATION_CD);
               exMap.put("NATION_CD", NATION_CD);
          }
          if (!"".equals(WORKS_SUB_TITLE)) {
               WORKS_SUB_TITLE = URLDecoder.decode(WORKS_SUB_TITLE, "UTF-8");
               commandMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
               exMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
          }
          if (!"".equals(CRT_YEAR)) {
               CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
               commandMap.put("CRT_YEAR", CRT_YEAR);
               exMap.put("CRT_YEAR", CRT_YEAR);
          }
          if (!"".equals(BOOK_TITLE)) {
               BOOK_TITLE = URLDecoder.decode(BOOK_TITLE, "UTF-8");
               commandMap.put("BOOK_TITLE", BOOK_TITLE);
               exMap.put("BOOK_TITLE", BOOK_TITLE);
          }
          if (!"".equals(BOOK_PUBLISHER)) {
               BOOK_PUBLISHER = URLDecoder.decode(BOOK_PUBLISHER, "UTF-8");
               commandMap.put("BOOK_PUBLISHER", BOOK_PUBLISHER);
               exMap.put("BOOK_PUBLISHER", BOOK_PUBLISHER);
          }
          if (!"".equals(BOOK_ISSU_YEAR)) {
               BOOK_ISSU_YEAR = URLDecoder.decode(BOOK_ISSU_YEAR, "UTF-8");
               commandMap.put("BOOK_ISSU_YEAR", BOOK_ISSU_YEAR);
               exMap.put("BOOK_ISSU_YEAR", BOOK_ISSU_YEAR);
          }
          if (!"".equals(COPT_NAME)) {
               COPT_NAME = URLDecoder.decode(COPT_NAME, "UTF-8");
               commandMap.put("COPT_NAME", COPT_NAME);
               exMap.put("COPT_NAME", COPT_NAME);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (CRT_YEAR == "없음" || "없음".equals(CRT_YEAR)) {
               commandMap.put("CRT_YEAR", "0");
               exMap.put("CRT_YEAR", "0");
          }
          if (BOOK_ISSU_YEAR == "없음" || "없음".equals(BOOK_ISSU_YEAR)) {
               commandMap.put("BOOK_ISSU_YEAR", "0");
               exMap.put("BOOK_ISSU_YEAR", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 어문 일괄수정
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17BookUpdate.page")
     public String fdcrAd17BookUpdate(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "2");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String NATION_CD = EgovWebUtil.getString(commandMap, "NATION_CD");
          String WORKS_SUB_TITLE = EgovWebUtil.getString(commandMap, "WORKS_SUB_TITLE");
          String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
          String BOOK_TITLE = EgovWebUtil.getString(commandMap, "BOOK_TITLE");
          String BOOK_PUBLISHER = EgovWebUtil.getString(commandMap, "BOOK_PUBLISHER");
          String BOOK_ISSU_YEAR = EgovWebUtil.getString(commandMap, "BOOK_ISSU_YEAR");
          String COPT_NAME = EgovWebUtil.getString(commandMap, "COPT_NAME");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "2");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(NATION_CD)) {
               NATION_CD = URLDecoder.decode(NATION_CD, "UTF-8");
               commandMap.put("NATION_CD", NATION_CD);
               exMap.put("NATION_CD", NATION_CD);
          }
          if (!"".equals(WORKS_SUB_TITLE)) {
               WORKS_SUB_TITLE = URLDecoder.decode(WORKS_SUB_TITLE, "UTF-8");
               commandMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
               exMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
          }
          if (!"".equals(CRT_YEAR)) {
               CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
               commandMap.put("CRT_YEAR", CRT_YEAR);
               exMap.put("CRT_YEAR", CRT_YEAR);
          }
          if (!"".equals(BOOK_TITLE)) {
               BOOK_TITLE = URLDecoder.decode(BOOK_TITLE, "UTF-8");
               commandMap.put("BOOK_TITLE", BOOK_TITLE);
               exMap.put("BOOK_TITLE", BOOK_TITLE);
          }
          if (!"".equals(BOOK_PUBLISHER)) {
               BOOK_PUBLISHER = URLDecoder.decode(BOOK_PUBLISHER, "UTF-8");
               commandMap.put("BOOK_PUBLISHER", BOOK_PUBLISHER);
               exMap.put("BOOK_PUBLISHER", BOOK_PUBLISHER);
          }
          if (!"".equals(BOOK_ISSU_YEAR)) {
               BOOK_ISSU_YEAR = URLDecoder.decode(BOOK_ISSU_YEAR, "UTF-8");
               commandMap.put("BOOK_ISSU_YEAR", BOOK_ISSU_YEAR);
               exMap.put("BOOK_ISSU_YEAR", BOOK_ISSU_YEAR);
          }
          if (!"".equals(COPT_NAME)) {
               COPT_NAME = URLDecoder.decode(COPT_NAME, "UTF-8");
               commandMap.put("COPT_NAME", COPT_NAME);
               exMap.put("COPT_NAME", COPT_NAME);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (CRT_YEAR == "없음" || "없음".equals(CRT_YEAR)) {
               commandMap.put("CRT_YEAR", "0");
               exMap.put("CRT_YEAR", "0");
          }
          if (BOOK_ISSU_YEAR == "없음" || "없음".equals(BOOK_ISSU_YEAR)) {
               commandMap.put("BOOK_ISSU_YEAR", "0");
               exMap.put("BOOK_ISSU_YEAR", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17UpdateNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 영화 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17MovieInsert.page")
     public String fdcrAd17MovieInsert(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "4");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
          String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
          String DIRECT = EgovWebUtil.getString(commandMap, "DIRECT");
          String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
          String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
          String PRODUCER = EgovWebUtil.getString(commandMap, "PRODUCER");
          String DISTRIBUTOR = EgovWebUtil.getString(commandMap, "DISTRIBUTOR");
          String INVESTOR = EgovWebUtil.getString(commandMap, "INVESTOR");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "4");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(CRT_YEAR)) {
               CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
               commandMap.put("CRT_YEAR", CRT_YEAR);
               exMap.put("CRT_YEAR", CRT_YEAR);
          }
          if (!"".equals(PLAYER)) {
               PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
               commandMap.put("PLAYER", PLAYER);
               exMap.put("PLAYER", PLAYER);
          }
          if (!"".equals(DIRECT)) {
               DIRECT = URLDecoder.decode(DIRECT, "UTF-8");
               commandMap.put("DIRECT", DIRECT);
               exMap.put("DIRECT", DIRECT);
          }
          if (!"".equals(WRITER)) {
               WRITER = URLDecoder.decode(WRITER, "UTF-8");
               commandMap.put("WRITER", WRITER);
               exMap.put("WRITER", WRITER);
          }
          if (!"".equals(DIRECTOR)) {
               DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
               commandMap.put("DIRECTOR", DIRECTOR);
               exMap.put("DIRECTOR", DIRECTOR);
          }
          if (!"".equals(PRODUCER)) {
               PRODUCER = URLDecoder.decode(PRODUCER, "UTF-8");
               commandMap.put("PRODUCER", PRODUCER);
               exMap.put("PRODUCER", PRODUCER);
          }
          if (!"".equals(DISTRIBUTOR)) {
               DISTRIBUTOR = URLDecoder.decode(DISTRIBUTOR, "UTF-8");
               commandMap.put("DISTRIBUTOR", DISTRIBUTOR);
               exMap.put("DISTRIBUTOR", DISTRIBUTOR);
          }
          if (!"".equals(INVESTOR)) {
               INVESTOR = URLDecoder.decode(INVESTOR, "UTF-8");
               commandMap.put("INVESTOR", INVESTOR);
               exMap.put("INVESTOR", INVESTOR);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (CRT_YEAR == "없음" || "없음".equals(CRT_YEAR)) {
               commandMap.put("CRT_YEAR", "0");
               exMap.put("CRT_YEAR", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 영화 일괄수정
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17MovieUpdate.page")
     public String fdcrAd17MovieUpdate(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "4");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
          String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
          String DIRECT = EgovWebUtil.getString(commandMap, "DIRECT");
          String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
          String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
          String PRODUCER = EgovWebUtil.getString(commandMap, "PRODUCER");
          String DISTRIBUTOR = EgovWebUtil.getString(commandMap, "DISTRIBUTOR");
          String INVESTOR = EgovWebUtil.getString(commandMap, "INVESTOR");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "4");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(CRT_YEAR)) {
               CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
               commandMap.put("CRT_YEAR", CRT_YEAR);
               exMap.put("CRT_YEAR", CRT_YEAR);
          }
          if (!"".equals(PLAYER)) {
               PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
               commandMap.put("PLAYER", PLAYER);
               exMap.put("PLAYER", PLAYER);
          }
          if (!"".equals(DIRECT)) {
               DIRECT = URLDecoder.decode(DIRECT, "UTF-8");
               commandMap.put("DIRECT", DIRECT);
               exMap.put("DIRECT", DIRECT);
          }
          if (!"".equals(WRITER)) {
               WRITER = URLDecoder.decode(WRITER, "UTF-8");
               commandMap.put("WRITER", WRITER);
               exMap.put("WRITER", WRITER);
          }
          if (!"".equals(DIRECTOR)) {
               DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
               commandMap.put("DIRECTOR", DIRECTOR);
               exMap.put("DIRECTOR", DIRECTOR);
          }
          if (!"".equals(PRODUCER)) {
               PRODUCER = URLDecoder.decode(PRODUCER, "UTF-8");
               commandMap.put("PRODUCER", PRODUCER);
               exMap.put("PRODUCER", PRODUCER);
          }
          if (!"".equals(DISTRIBUTOR)) {
               DISTRIBUTOR = URLDecoder.decode(DISTRIBUTOR, "UTF-8");
               commandMap.put("DISTRIBUTOR", DISTRIBUTOR);
               exMap.put("DISTRIBUTOR", DISTRIBUTOR);
          }
          if (!"".equals(INVESTOR)) {
               INVESTOR = URLDecoder.decode(INVESTOR, "UTF-8");
               commandMap.put("INVESTOR", INVESTOR);
               exMap.put("INVESTOR", INVESTOR);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (CRT_YEAR == "없음" || "없음".equals(CRT_YEAR)) {
               commandMap.put("CRT_YEAR", "0");
               exMap.put("CRT_YEAR", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17UpdateNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 방송 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17BroadcastInsert.page")
     public String fdcrAd17BroadcastInsert(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "5");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String BORD_GENRE_CD = EgovWebUtil.getString(commandMap, "BORD_GENRE_CD");
          String MAKE_YEAR = EgovWebUtil.getString(commandMap, "MAKE_YEAR");
          String BROD_ORD_SEQ = EgovWebUtil.getString(commandMap, "BROD_ORD_SEQ");
          String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
          String MAKE_CPY = EgovWebUtil.getString(commandMap, "MAKE_CPY");
          String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
          String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "5");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(BORD_GENRE_CD)) {
               BORD_GENRE_CD = URLDecoder.decode(BORD_GENRE_CD, "UTF-8");
               commandMap.put("BORD_GENRE_CD", BORD_GENRE_CD);
               exMap.put("BORD_GENRE_CD", BORD_GENRE_CD);
          }
          if (!"".equals(MAKE_YEAR)) {
               MAKE_YEAR = URLDecoder.decode(MAKE_YEAR, "UTF-8");
               commandMap.put("MAKE_YEAR", MAKE_YEAR);
               exMap.put("MAKE_YEAR", MAKE_YEAR);
          }
          if (!"".equals(BROD_ORD_SEQ)) {
               BROD_ORD_SEQ = URLDecoder.decode(BROD_ORD_SEQ, "UTF-8");
               commandMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
               exMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
          }
          if (!"".equals(DIRECTOR)) {
               DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
               commandMap.put("DIRECTOR", DIRECTOR);
               exMap.put("DIRECTOR", DIRECTOR);
          }
          if (!"".equals(MAKE_CPY)) {
               MAKE_CPY = URLDecoder.decode(MAKE_CPY, "UTF-8");
               commandMap.put("MAKE_CPY", MAKE_CPY);
               exMap.put("MAKE_CPY", MAKE_CPY);
          }
          if (!"".equals(PLAYER)) {
               PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
               commandMap.put("PLAYER", PLAYER);
               exMap.put("PLAYER", PLAYER);
          }
          if (!"".equals(WRITER)) {
               WRITER = URLDecoder.decode(WRITER, "UTF-8");
               commandMap.put("WRITER", WRITER);
               exMap.put("WRITER", WRITER);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (BORD_GENRE_CD == "없음" || "없음".equals(BORD_GENRE_CD)) {
               commandMap.put("BORD_GENRE_CD", "0");
               exMap.put("BORD_GENRE_CD", "0");
          }
          if (MAKE_YEAR == "없음" || "없음".equals(MAKE_YEAR)) {
               commandMap.put("MAKE_YEAR", "0");
               exMap.put("MAKE_YEAR", "0");
          }
          if (BROD_ORD_SEQ == "없음" || "없음".equals(BROD_ORD_SEQ)) {
               commandMap.put("BROD_ORD_SEQ", "0");
               exMap.put("BROD_ORD_SEQ", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 방송 일괄수정
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17BroadcastUpdate.page")
     public String fdcrAd17BroadcastUpdate(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "5");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String BORD_GENRE_CD = EgovWebUtil.getString(commandMap, "BORD_GENRE_CD");
          String MAKE_YEAR = EgovWebUtil.getString(commandMap, "MAKE_YEAR");
          String BROD_ORD_SEQ = EgovWebUtil.getString(commandMap, "BROD_ORD_SEQ");
          String DIRECTOR = EgovWebUtil.getString(commandMap, "DIRECTOR");
          String MAKE_CPY = EgovWebUtil.getString(commandMap, "MAKE_CPY");
          String PLAYER = EgovWebUtil.getString(commandMap, "PLAYER");
          String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "5");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(BORD_GENRE_CD)) {
               BORD_GENRE_CD = URLDecoder.decode(BORD_GENRE_CD, "UTF-8");
               commandMap.put("BORD_GENRE_CD", BORD_GENRE_CD);
               exMap.put("BORD_GENRE_CD", BORD_GENRE_CD);
          }
          if (!"".equals(MAKE_YEAR)) {
               MAKE_YEAR = URLDecoder.decode(MAKE_YEAR, "UTF-8");
               commandMap.put("MAKE_YEAR", MAKE_YEAR);
               exMap.put("MAKE_YEAR", MAKE_YEAR);
          }
          if (!"".equals(BROD_ORD_SEQ)) {
               BROD_ORD_SEQ = URLDecoder.decode(BROD_ORD_SEQ, "UTF-8");
               commandMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
               exMap.put("BROD_ORD_SEQ", BROD_ORD_SEQ);
          }
          if (!"".equals(DIRECTOR)) {
               DIRECTOR = URLDecoder.decode(DIRECTOR, "UTF-8");
               commandMap.put("DIRECTOR", DIRECTOR);
               exMap.put("DIRECTOR", DIRECTOR);
          }
          if (!"".equals(MAKE_CPY)) {
               MAKE_CPY = URLDecoder.decode(MAKE_CPY, "UTF-8");
               commandMap.put("MAKE_CPY", MAKE_CPY);
               exMap.put("MAKE_CPY", MAKE_CPY);
          }
          if (!"".equals(PLAYER)) {
               PLAYER = URLDecoder.decode(PLAYER, "UTF-8");
               commandMap.put("PLAYER", PLAYER);
               exMap.put("PLAYER", PLAYER);
          }
          if (!"".equals(WRITER)) {
               WRITER = URLDecoder.decode(WRITER, "UTF-8");
               commandMap.put("WRITER", WRITER);
               exMap.put("WRITER", WRITER);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (BORD_GENRE_CD == "없음" || "없음".equals(BORD_GENRE_CD)) {
               commandMap.put("BORD_GENRE_CD", "0");
               exMap.put("BORD_GENRE_CD", "0");
          }
          if (MAKE_YEAR == "없음" || "없음".equals(MAKE_YEAR)) {
               commandMap.put("MAKE_YEAR", "0");
               exMap.put("MAKE_YEAR", "0");
          }
          if (BROD_ORD_SEQ == "없음" || "없음".equals(BROD_ORD_SEQ)) {
               commandMap.put("BROD_ORD_SEQ", "0");
               exMap.put("BROD_ORD_SEQ", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17UpdateNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 뉴스 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17NewsInsert.page")
     public String fdcrAd17NewsInsert(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "6");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String PAPE_NO = EgovWebUtil.getString(commandMap, "PAPE_NO");
          String PAPE_KIND = EgovWebUtil.getString(commandMap, "PAPE_KIND");
          String CONTRIBUTOR = EgovWebUtil.getString(commandMap, "CONTRIBUTOR");
          String REPOTER = EgovWebUtil.getString(commandMap, "REPOTER");
          String REVISION_ID = EgovWebUtil.getString(commandMap, "REVISION_ID");
          String ARTICL_DATE = EgovWebUtil.getString(commandMap, "ARTICL_DATE");
          String ARTICL_ISSU_DTTM = EgovWebUtil.getString(commandMap, "ARTICL_ISSU_DTTM");
          String PROVIDER = EgovWebUtil.getString(commandMap, "PROVIDER");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          System.out.println("총건수?????????????!!!!!!!!!!!!:" + tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "6");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(PAPE_NO)) {
               PAPE_NO = URLDecoder.decode(PAPE_NO, "UTF-8");
               commandMap.put("PAPE_NO", PAPE_NO);
               exMap.put("PAPE_NO", PAPE_NO);
          }
          if (!"".equals(PAPE_KIND)) {
               PAPE_KIND = URLDecoder.decode(PAPE_KIND, "UTF-8");
               commandMap.put("PAPE_KIND", PAPE_KIND);
               exMap.put("PAPE_KIND", PAPE_KIND);
          }
          if (!"".equals(CONTRIBUTOR)) {
               CONTRIBUTOR = URLDecoder.decode(CONTRIBUTOR, "UTF-8");
               commandMap.put("CONTRIBUTOR", CONTRIBUTOR);
               exMap.put("CONTRIBUTOR", CONTRIBUTOR);
          }
          if (!"".equals(REPOTER)) {
               REPOTER = URLDecoder.decode(REPOTER, "UTF-8");
               commandMap.put("REPOTER", REPOTER);
               exMap.put("REPOTER", REPOTER);
          }
          if (!"".equals(REVISION_ID)) {
               REVISION_ID = URLDecoder.decode(REVISION_ID, "UTF-8");
               commandMap.put("REVISION_ID", REVISION_ID);
               exMap.put("REVISION_ID", REVISION_ID);
          }
          if (!"".equals(ARTICL_DATE)) {
               ARTICL_DATE = URLDecoder.decode(ARTICL_DATE, "UTF-8");
               commandMap.put("ARTICL_DATE", ARTICL_DATE);
               exMap.put("ARTICL_DATE", ARTICL_DATE);
          }
          if (!"".equals(ARTICL_ISSU_DTTM)) {
               ARTICL_ISSU_DTTM = URLDecoder.decode(ARTICL_ISSU_DTTM, "UTF-8");
               commandMap.put("ARTICL_ISSU_DTTM", ARTICL_ISSU_DTTM);
               exMap.put("ARTICL_ISSU_DTTM", ARTICL_ISSU_DTTM);
          }
          if (!"".equals(PROVIDER)) {
               PROVIDER = URLDecoder.decode(PROVIDER, "UTF-8");
               commandMap.put("PROVIDER", PROVIDER);
               exMap.put("PROVIDER", PROVIDER);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (ARTICL_DATE == "없음" || "없음".equals(ARTICL_DATE)) {
               commandMap.put("ARTICL_DATE", "0");
               exMap.put("ARTICL_DATE", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 뉴스 일괄수정
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17NewsUpdate.page")
     public String fdcrAd17NewsUpdate(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "6");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String PAPE_NO = EgovWebUtil.getString(commandMap, "PAPE_NO");
          String PAPE_KIND = EgovWebUtil.getString(commandMap, "PAPE_KIND");
          String CONTRIBUTOR = EgovWebUtil.getString(commandMap, "CONTRIBUTOR");
          String REPOTER = EgovWebUtil.getString(commandMap, "REPOTER");
          String REVISION_ID = EgovWebUtil.getString(commandMap, "REVISION_ID");
          String ARTICL_DATE = EgovWebUtil.getString(commandMap, "ARTICL_DATE");
          String ARTICL_ISSU_DTTM = EgovWebUtil.getString(commandMap, "ARTICL_ISSU_DTTM");
          String PROVIDER = EgovWebUtil.getString(commandMap, "PROVIDER");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "6");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(PAPE_NO)) {
               PAPE_NO = URLDecoder.decode(PAPE_NO, "UTF-8");
               commandMap.put("PAPE_NO", PAPE_NO);
               exMap.put("PAPE_NO", PAPE_NO);
          }
          if (!"".equals(PAPE_KIND)) {
               PAPE_KIND = URLDecoder.decode(PAPE_KIND, "UTF-8");
               commandMap.put("PAPE_KIND", PAPE_KIND);
               exMap.put("PAPE_KIND", PAPE_KIND);
          }
          if (!"".equals(CONTRIBUTOR)) {
               CONTRIBUTOR = URLDecoder.decode(CONTRIBUTOR, "UTF-8");
               commandMap.put("CONTRIBUTOR", CONTRIBUTOR);
               exMap.put("CONTRIBUTOR", CONTRIBUTOR);
          }
          if (!"".equals(REPOTER)) {
               REPOTER = URLDecoder.decode(REPOTER, "UTF-8");
               commandMap.put("REPOTER", REPOTER);
               exMap.put("REPOTER", REPOTER);
          }
          if (!"".equals(REVISION_ID)) {
               REVISION_ID = URLDecoder.decode(REVISION_ID, "UTF-8");
               commandMap.put("REVISION_ID", REVISION_ID);
               exMap.put("REVISION_ID", REVISION_ID);
          }
          if (!"".equals(ARTICL_DATE)) {
               ARTICL_DATE = URLDecoder.decode(ARTICL_DATE, "UTF-8");
               commandMap.put("ARTICL_DATE", ARTICL_DATE);
               exMap.put("ARTICL_DATE", ARTICL_DATE);
          }
          if (!"".equals(ARTICL_ISSU_DTTM)) {
               ARTICL_ISSU_DTTM = URLDecoder.decode(ARTICL_ISSU_DTTM, "UTF-8");
               commandMap.put("ARTICL_ISSU_DTTM", ARTICL_ISSU_DTTM);
               exMap.put("ARTICL_ISSU_DTTM", ARTICL_ISSU_DTTM);
          }
          if (!"".equals(PROVIDER)) {
               PROVIDER = URLDecoder.decode(PROVIDER, "UTF-8");
               commandMap.put("PROVIDER", PROVIDER);
               exMap.put("PROVIDER", PROVIDER);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (ARTICL_DATE == "없음" || "없음".equals(ARTICL_DATE)) {
               commandMap.put("ARTICL_DATE", "0");
               exMap.put("ARTICL_DATE", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17UpdateNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 미술 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17ArtInsert.page")
     public String fdcrAd17ArtInsert(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "7");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String WORKS_SUB_TITLE = EgovWebUtil.getString(commandMap, "WORKS_SUB_TITLE");
          String KIND = EgovWebUtil.getString(commandMap, "KIND");
          String MAKE_DATE = EgovWebUtil.getString(commandMap, "MAKE_DATE");
          String SOURCE_INFO = EgovWebUtil.getString(commandMap, "SOURCE_INFO");
          String SIZE_INFO = EgovWebUtil.getString(commandMap, "SIZE_INFO");
          String RESOLUTION = EgovWebUtil.getString(commandMap, "RESOLUTION");
          String MAIN_MTRL = EgovWebUtil.getString(commandMap, "MAIN_MTRL");
          String TXTR = EgovWebUtil.getString(commandMap, "TXTR");
          String IMG_URL = EgovWebUtil.getString(commandMap, "IMG_URL");
          String DETL_URL = EgovWebUtil.getString(commandMap, "DETL_URL");
          String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
          String POSS_DATE = EgovWebUtil.getString(commandMap, "POSS_DATE");
          String POSS_ORGN_NAME = EgovWebUtil.getString(commandMap, "POSS_ORGN_NAME");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "7");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(WORKS_SUB_TITLE)) {
               WORKS_SUB_TITLE = URLDecoder.decode(WORKS_SUB_TITLE, "UTF-8");
               commandMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
               exMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
          }
          if (!"".equals(KIND)) {
               KIND = URLDecoder.decode(KIND, "UTF-8");
               commandMap.put("KIND", KIND);
               exMap.put("KIND", KIND);
          }
          if (!"".equals(MAKE_DATE)) {
               MAKE_DATE = URLDecoder.decode(MAKE_DATE, "UTF-8");
               commandMap.put("MAKE_DATE", MAKE_DATE);
               exMap.put("MAKE_DATE", MAKE_DATE);
          }
          if (!"".equals(SOURCE_INFO)) {
               SOURCE_INFO = URLDecoder.decode(SOURCE_INFO, "UTF-8");
               commandMap.put("SOURCE_INFO", SOURCE_INFO);
               exMap.put("SOURCE_INFO", SOURCE_INFO);
          }
          if (!"".equals(SIZE_INFO)) {
               SIZE_INFO = URLDecoder.decode(SIZE_INFO, "UTF-8");
               commandMap.put("SIZE_INFO", SIZE_INFO);
               exMap.put("SIZE_INFO", SIZE_INFO);
          }
          if (!"".equals(RESOLUTION)) {
               RESOLUTION = URLDecoder.decode(RESOLUTION, "UTF-8");
               commandMap.put("RESOLUTION", RESOLUTION);
               exMap.put("RESOLUTION", RESOLUTION);
          }
          if (!"".equals(MAIN_MTRL)) {
               MAIN_MTRL = URLDecoder.decode(MAIN_MTRL, "UTF-8");
               commandMap.put("MAIN_MTRL", MAIN_MTRL);
               exMap.put("MAIN_MTRL", MAIN_MTRL);
          }
          if (!"".equals(TXTR)) {
               TXTR = URLDecoder.decode(TXTR, "UTF-8");
               commandMap.put("TXTR", TXTR);
               exMap.put("TXTR", TXTR);
          }
          if (!"".equals(IMG_URL)) {
               IMG_URL = URLDecoder.decode(IMG_URL, "UTF-8");
               commandMap.put("IMG_URL", IMG_URL);
               exMap.put("IMG_URL", IMG_URL);
          }
          if (!"".equals(DETL_URL)) {
               DETL_URL = URLDecoder.decode(DETL_URL, "UTF-8");
               commandMap.put("DETL_URL", DETL_URL);
               exMap.put("DETL_URL", DETL_URL);
          }
          if (!"".equals(WRITER)) {
               WRITER = URLDecoder.decode(WRITER, "UTF-8");
               commandMap.put("WRITER", WRITER);
               exMap.put("WRITER", WRITER);
          }
          if (!"".equals(POSS_DATE)) {
               POSS_DATE = URLDecoder.decode(POSS_DATE, "UTF-8");
               commandMap.put("POSS_DATE", POSS_DATE);
               exMap.put("POSS_DATE", POSS_DATE);
          }
          if (!"".equals(POSS_ORGN_NAME)) {
               POSS_ORGN_NAME = URLDecoder.decode(POSS_ORGN_NAME, "UTF-8");
               commandMap.put("POSS_ORGN_NAME", POSS_ORGN_NAME);
               exMap.put("POSS_ORGN_NAME", POSS_ORGN_NAME);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (MAKE_DATE == "없음" || "없음".equals(MAKE_DATE)) {
               commandMap.put("MAKE_DATE", "0");
               exMap.put("MAKE_DATE", "0");
          }
          if (IMG_URL == "없음" || "없음".equals(IMG_URL)) {
               commandMap.put("IMG_URL", "0");
               exMap.put("IMG_URL", "0");
          }
          if (DETL_URL == "없음" || "없음".equals(DETL_URL)) {
               commandMap.put("DETL_URL", "0");
               exMap.put("DETL_URL", "0");
          }
          if (POSS_DATE == "없음" || "없음".equals(POSS_DATE)) {
               commandMap.put("POSS_DATE", "0");
               exMap.put("POSS_DATE", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 미술 일괄수정
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17ArtUpdate.page")
     public String fdcrAd17ArtUpdate(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "7");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String WORKS_SUB_TITLE = EgovWebUtil.getString(commandMap, "WORKS_SUB_TITLE");
          String KIND = EgovWebUtil.getString(commandMap, "KIND");
          String MAKE_DATE = EgovWebUtil.getString(commandMap, "MAKE_DATE");
          String SOURCE_INFO = EgovWebUtil.getString(commandMap, "SOURCE_INFO");
          String SIZE_INFO = EgovWebUtil.getString(commandMap, "SIZE_INFO");
          String RESOLUTION = EgovWebUtil.getString(commandMap, "RESOLUTION");
          String MAIN_MTRL = EgovWebUtil.getString(commandMap, "MAIN_MTRL");
          String TXTR = EgovWebUtil.getString(commandMap, "TXTR");
          String IMG_URL = EgovWebUtil.getString(commandMap, "IMG_URL");
          String DETL_URL = EgovWebUtil.getString(commandMap, "DETL_URL");
          String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
          String POSS_DATE = EgovWebUtil.getString(commandMap, "POSS_DATE");
          String POSS_ORGN_NAME = EgovWebUtil.getString(commandMap, "POSS_ORGN_NAME");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "7");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(WORKS_SUB_TITLE)) {
               WORKS_SUB_TITLE = URLDecoder.decode(WORKS_SUB_TITLE, "UTF-8");
               commandMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
               exMap.put("WORKS_SUB_TITLE", WORKS_SUB_TITLE);
          }
          if (!"".equals(KIND)) {
               KIND = URLDecoder.decode(KIND, "UTF-8");
               commandMap.put("KIND", KIND);
               exMap.put("KIND", KIND);
          }
          if (!"".equals(MAKE_DATE)) {
               MAKE_DATE = URLDecoder.decode(MAKE_DATE, "UTF-8");
               commandMap.put("MAKE_DATE", MAKE_DATE);
               exMap.put("MAKE_DATE", MAKE_DATE);
          }
          if (!"".equals(SOURCE_INFO)) {
               SOURCE_INFO = URLDecoder.decode(SOURCE_INFO, "UTF-8");
               commandMap.put("SOURCE_INFO", SOURCE_INFO);
               exMap.put("SOURCE_INFO", SOURCE_INFO);
          }
          if (!"".equals(SIZE_INFO)) {
               SIZE_INFO = URLDecoder.decode(SIZE_INFO, "UTF-8");
               commandMap.put("SIZE_INFO", SIZE_INFO);
               exMap.put("SIZE_INFO", SIZE_INFO);
          }
          if (!"".equals(RESOLUTION)) {
               RESOLUTION = URLDecoder.decode(RESOLUTION, "UTF-8");
               commandMap.put("RESOLUTION", RESOLUTION);
               exMap.put("RESOLUTION", RESOLUTION);
          }
          if (!"".equals(MAIN_MTRL)) {
               MAIN_MTRL = URLDecoder.decode(MAIN_MTRL, "UTF-8");
               commandMap.put("MAIN_MTRL", MAIN_MTRL);
               exMap.put("MAIN_MTRL", MAIN_MTRL);
          }
          if (!"".equals(TXTR)) {
               TXTR = URLDecoder.decode(TXTR, "UTF-8");
               commandMap.put("TXTR", TXTR);
               exMap.put("TXTR", TXTR);
          }
          if (!"".equals(IMG_URL)) {
               IMG_URL = URLDecoder.decode(IMG_URL, "UTF-8");
               commandMap.put("IMG_URL", IMG_URL);
               exMap.put("IMG_URL", IMG_URL);
          }
          if (!"".equals(DETL_URL)) {
               DETL_URL = URLDecoder.decode(DETL_URL, "UTF-8");
               commandMap.put("DETL_URL", DETL_URL);
               exMap.put("DETL_URL", DETL_URL);
          }
          if (!"".equals(WRITER)) {
               WRITER = URLDecoder.decode(WRITER, "UTF-8");
               commandMap.put("WRITER", WRITER);
               exMap.put("WRITER", WRITER);
          }
          if (!"".equals(POSS_DATE)) {
               POSS_DATE = URLDecoder.decode(POSS_DATE, "UTF-8");
               commandMap.put("POSS_DATE", POSS_DATE);
               exMap.put("POSS_DATE", POSS_DATE);
          }
          if (!"".equals(POSS_ORGN_NAME)) {
               POSS_ORGN_NAME = URLDecoder.decode(POSS_ORGN_NAME, "UTF-8");
               commandMap.put("POSS_ORGN_NAME", POSS_ORGN_NAME);
               exMap.put("POSS_ORGN_NAME", POSS_ORGN_NAME);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (MAKE_DATE == "없음" || "없음".equals(MAKE_DATE)) {
               commandMap.put("MAKE_DATE", "0");
               exMap.put("MAKE_DATE", "0");
          }
          if (IMG_URL == "없음" || "없음".equals(IMG_URL)) {
               commandMap.put("IMG_URL", "0");
               exMap.put("IMG_URL", "0");
          }
          if (DETL_URL == "없음" || "없음".equals(DETL_URL)) {
               commandMap.put("DETL_URL", "0");
               exMap.put("DETL_URL", "0");
          }
          if (POSS_DATE == "없음" || "없음".equals(POSS_DATE)) {
               commandMap.put("POSS_DATE", "0");
               exMap.put("POSS_DATE", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17UpdateNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 이미지 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17ImageInsert.page")
     public String fdcrAd17ImageInsert(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "8");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String IMAGE_DESC = EgovWebUtil.getString(commandMap, "IMAGE_DESC");
          String KEYWORD = EgovWebUtil.getString(commandMap, "KEYWORD");
          String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
          String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
          String PRODUCER = EgovWebUtil.getString(commandMap, "PRODUCER");
          String IMAGE_URL = EgovWebUtil.getString(commandMap, "IMAGE_URL");
          String IMAGE_OPEN_YN = EgovWebUtil.getString(commandMap, "IMAGE_OPEN_YN");
          String ICNX_NUMB = EgovWebUtil.getString(commandMap, "ICNX_NUMB");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "8");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(IMAGE_DESC)) {
               IMAGE_DESC = URLDecoder.decode(IMAGE_DESC, "UTF-8");
               commandMap.put("IMAGE_DESC", IMAGE_DESC);
               exMap.put("IMAGE_DESC", IMAGE_DESC);
          }
          if (!"".equals(KEYWORD)) {
               KEYWORD = URLDecoder.decode(KEYWORD, "UTF-8");
               commandMap.put("KEYWORD", KEYWORD);
               exMap.put("KEYWORD", KEYWORD);
          }
          if (!"".equals(CRT_YEAR)) {
               CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
               commandMap.put("CRT_YEAR", CRT_YEAR);
               exMap.put("CRT_YEAR", CRT_YEAR);
          }
          if (!"".equals(WRITER)) {
               WRITER = URLDecoder.decode(WRITER, "UTF-8");
               commandMap.put("WRITER", WRITER);
               exMap.put("WRITER", WRITER);
          }
          if (!"".equals(PRODUCER)) {
               PRODUCER = URLDecoder.decode(PRODUCER, "UTF-8");
               commandMap.put("PRODUCER", PRODUCER);
               exMap.put("PRODUCER", PRODUCER);
          }
          if (!"".equals(IMAGE_URL)) {
               IMAGE_URL = URLDecoder.decode(IMAGE_URL, "UTF-8");
               commandMap.put("IMAGE_URL", IMAGE_URL);
               exMap.put("IMAGE_URL", IMAGE_URL);
          }
          if (!"".equals(IMAGE_OPEN_YN)) {
               IMAGE_OPEN_YN = URLDecoder.decode(IMAGE_OPEN_YN, "UTF-8");
               commandMap.put("IMAGE_OPEN_YN", IMAGE_OPEN_YN);
               exMap.put("IMAGE_OPEN_YN", IMAGE_OPEN_YN);
          }
          if (!"".equals(ICNX_NUMB)) {
               ICNX_NUMB = URLDecoder.decode(ICNX_NUMB, "UTF-8");
               commandMap.put("ICNX_NUMB", ICNX_NUMB);
               exMap.put("ICNX_NUMB", ICNX_NUMB);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (IMAGE_DESC == "없음" || "없음".equals(IMAGE_DESC)) {
               commandMap.put("IMAGE_DESC", "0");
               exMap.put("IMAGE_DESC", "0");
          }
          if (KEYWORD == "없음" || "없음".equals(KEYWORD)) {
               commandMap.put("KEYWORD", "0");
               exMap.put("KEYWORD", "0");
          }
          if (IMAGE_URL == "없음" || "없음".equals(IMAGE_URL)) {
               commandMap.put("IMAGE_URL", "0");
               exMap.put("IMAGE_URL", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 이미지 일괄수정
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17ImageUpdate.page")
     public String fdcrAd17ImageUpdate(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "8");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String IMAGE_DESC = EgovWebUtil.getString(commandMap, "IMAGE_DESC");
          String KEYWORD = EgovWebUtil.getString(commandMap, "KEYWORD");
          String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
          String WRITER = EgovWebUtil.getString(commandMap, "WRITER");
          String PRODUCER = EgovWebUtil.getString(commandMap, "PRODUCER");
          String IMAGE_URL = EgovWebUtil.getString(commandMap, "IMAGE_URL");
          String IMAGE_OPEN_YN = EgovWebUtil.getString(commandMap, "IMAGE_OPEN_YN");
          String ICNX_NUMB = EgovWebUtil.getString(commandMap, "ICNX_NUMB");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "8");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(IMAGE_DESC)) {
               IMAGE_DESC = URLDecoder.decode(IMAGE_DESC, "UTF-8");
               commandMap.put("IMAGE_DESC", IMAGE_DESC);
               exMap.put("IMAGE_DESC", IMAGE_DESC);
          }
          if (!"".equals(KEYWORD)) {
               KEYWORD = URLDecoder.decode(KEYWORD, "UTF-8");
               commandMap.put("KEYWORD", KEYWORD);
               exMap.put("KEYWORD", KEYWORD);
          }
          if (!"".equals(CRT_YEAR)) {
               CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
               commandMap.put("CRT_YEAR", CRT_YEAR);
               exMap.put("CRT_YEAR", CRT_YEAR);
          }
          if (!"".equals(WRITER)) {
               WRITER = URLDecoder.decode(WRITER, "UTF-8");
               commandMap.put("WRITER", WRITER);
               exMap.put("WRITER", WRITER);
          }
          if (!"".equals(PRODUCER)) {
               PRODUCER = URLDecoder.decode(PRODUCER, "UTF-8");
               commandMap.put("PRODUCER", PRODUCER);
               exMap.put("PRODUCER", PRODUCER);
          }
          if (!"".equals(IMAGE_URL)) {
               IMAGE_URL = URLDecoder.decode(IMAGE_URL, "UTF-8");
               commandMap.put("IMAGE_URL", IMAGE_URL);
               exMap.put("IMAGE_URL", IMAGE_URL);
          }
          if (!"".equals(IMAGE_OPEN_YN)) {
               IMAGE_OPEN_YN = URLDecoder.decode(IMAGE_OPEN_YN, "UTF-8");
               commandMap.put("IMAGE_OPEN_YN", IMAGE_OPEN_YN);
               exMap.put("IMAGE_OPEN_YN", IMAGE_OPEN_YN);
          }
          if (!"".equals(ICNX_NUMB)) {
               ICNX_NUMB = URLDecoder.decode(ICNX_NUMB, "UTF-8");
               commandMap.put("ICNX_NUMB", ICNX_NUMB);
               exMap.put("ICNX_NUMB", ICNX_NUMB);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (IMAGE_DESC == "없음" || "없음".equals(IMAGE_DESC)) {
               commandMap.put("IMAGE_DESC", "0");
               exMap.put("IMAGE_DESC", "0");
          }
          if (KEYWORD == "없음" || "없음".equals(KEYWORD)) {
               commandMap.put("KEYWORD", "0");
               exMap.put("KEYWORD", "0");
          }
          if (IMAGE_URL == "없음" || "없음".equals(IMAGE_URL)) {
               commandMap.put("IMAGE_URL", "0");
               exMap.put("IMAGE_URL", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17UpdateNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 기타 일괄등록
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17SideInsert.page")
     public String fdcrAd17SideInsert(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "99");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String SIDE_GENRE_CD = EgovWebUtil.getString(commandMap, "SIDE_GENRE_CD");
          String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
          String COPT_NAME = EgovWebUtil.getString(commandMap, "COPT_NAME");
          String PUBL_MEDI = EgovWebUtil.getString(commandMap, "PUBL_MEDI");
          String PUBL_DATE = EgovWebUtil.getString(commandMap, "PUBL_DATE");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);
          // System.out.println("총건수?????????????!!!!!!!!!!!!:"+tot_cnt_int);

          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "99");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(SIDE_GENRE_CD)) {
               SIDE_GENRE_CD = URLDecoder.decode(SIDE_GENRE_CD, "UTF-8");
               commandMap.put("SIDE_GENRE_CD", SIDE_GENRE_CD);
               exMap.put("SIDE_GENRE_CD", SIDE_GENRE_CD);
          }
          if (!"".equals(COPT_NAME)) {
               COPT_NAME = URLDecoder.decode(COPT_NAME, "UTF-8");
               commandMap.put("COPT_NAME", COPT_NAME);
               exMap.put("COPT_NAME", COPT_NAME);
          }
          if (!"".equals(CRT_YEAR)) {
               CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
               commandMap.put("CRT_YEAR", CRT_YEAR);
               exMap.put("CRT_YEAR", CRT_YEAR);
          }
          if (!"".equals(PUBL_MEDI)) {
               PUBL_MEDI = URLDecoder.decode(PUBL_MEDI, "UTF-8");
               commandMap.put("PUBL_MEDI", PUBL_MEDI);
               exMap.put("PUBL_MEDI", PUBL_MEDI);
          }
          if (!"".equals(PUBL_DATE)) {
               PUBL_DATE = URLDecoder.decode(PUBL_DATE, "UTF-8");
               commandMap.put("PUBL_DATE", PUBL_DATE);
               exMap.put("PUBL_DATE", PUBL_DATE);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (PUBL_DATE == "없음" || "없음".equals(PUBL_DATE)) {
               commandMap.put("PUBL_DATE", "0");
               exMap.put("PUBL_DATE", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17InsertNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);
               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     /**
      * 기타 일괄수정
      * 
      * @param model
      * @param commandMap
      * @param mreq
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd17SideUpdate.page")
     public String fdcrAd17SideUpdate(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 성공리스트
          ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          commandMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("USER_NAME", ConsoleLoginUser.getUserName());
          commandMap.put("genreCd", "99");

          String RGST_ORGN_NAME = EgovWebUtil.getString(commandMap, "RGST_ORGN_NAME");
          String COMM_WORKS_ID = EgovWebUtil.getString(commandMap, "COMM_WORKS_ID");
          String WORKS_TITLE = EgovWebUtil.getString(commandMap, "WORKS_TITLE");
          String SIDE_GENRE_CD = EgovWebUtil.getString(commandMap, "SIDE_GENRE_CD");
          String CRT_YEAR = EgovWebUtil.getString(commandMap, "CRT_YEAR");
          String COPT_NAME = EgovWebUtil.getString(commandMap, "COPT_NAME");
          String PUBL_MEDI = EgovWebUtil.getString(commandMap, "PUBL_MEDI");
          String PUBL_DATE = EgovWebUtil.getString(commandMap, "PUBL_DATE");
          String COMM_MGNT_CD = EgovWebUtil.getString(commandMap, "COMM_MGNT_CD");
          String TOT_CNT = request.getParameter("tot_cnt");
          int tot_cnt_int = Integer.parseInt(TOT_CNT);


          String reptChrrName = request.getParameter("reptChrrName");
          String reptChrrPosi = request.getParameter("reptChrrPosi");
          String reptChrrTelx = request.getParameter("reptChrrTelx");
          String reptChrrMail = request.getParameter("reptChrrMail");

          Map<String, Object> exMap = new HashMap<String, Object>();
          exMap.put("genreCode", "99");
          exMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          exMap.put("USER_IDNT", ConsoleLoginUser.getUserId());
          exMap.put("USER_NAME", ConsoleLoginUser.getUserName());

          if (!"".equals(RGST_ORGN_NAME)) {
               RGST_ORGN_NAME = URLDecoder.decode(RGST_ORGN_NAME, "UTF-8");
               commandMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
               exMap.put("RGST_ORGN_NAME", RGST_ORGN_NAME);
          }
          if (!"".equals(COMM_WORKS_ID)) {
               COMM_WORKS_ID = URLDecoder.decode(COMM_WORKS_ID, "UTF-8");
               commandMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
               exMap.put("COMM_WORKS_ID", COMM_WORKS_ID);
          }
          if (!"".equals(WORKS_TITLE)) {
               WORKS_TITLE = URLDecoder.decode(WORKS_TITLE, "UTF-8");
               commandMap.put("WORKS_TITLE", WORKS_TITLE);
               exMap.put("WORKS_TITLE", WORKS_TITLE);
          }
          if (!"".equals(SIDE_GENRE_CD)) {
               SIDE_GENRE_CD = URLDecoder.decode(SIDE_GENRE_CD, "UTF-8");
               commandMap.put("SIDE_GENRE_CD", SIDE_GENRE_CD);
               exMap.put("SIDE_GENRE_CD", SIDE_GENRE_CD);
          }
          if (!"".equals(COPT_NAME)) {
               COPT_NAME = URLDecoder.decode(COPT_NAME, "UTF-8");
               commandMap.put("COPT_NAME", COPT_NAME);
               exMap.put("COPT_NAME", COPT_NAME);
          }
          if (!"".equals(CRT_YEAR)) {
               CRT_YEAR = URLDecoder.decode(CRT_YEAR, "UTF-8");
               commandMap.put("CRT_YEAR", CRT_YEAR);
               exMap.put("CRT_YEAR", CRT_YEAR);
          }
          if (!"".equals(PUBL_MEDI)) {
               PUBL_MEDI = URLDecoder.decode(PUBL_MEDI, "UTF-8");
               commandMap.put("PUBL_MEDI", PUBL_MEDI);
               exMap.put("PUBL_MEDI", PUBL_MEDI);
          }
          if (!"".equals(PUBL_DATE)) {
               PUBL_DATE = URLDecoder.decode(PUBL_DATE, "UTF-8");
               commandMap.put("PUBL_DATE", PUBL_DATE);
               exMap.put("PUBL_DATE", PUBL_DATE);
          }
          if (!"".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
               exMap.put("COMM_MGNT_CD", COMM_MGNT_CD);
          }
          if (!"".equals(reptChrrName)) {
               reptChrrName = URLDecoder.decode(reptChrrName, "UTF-8");
               commandMap.put("REPT_CHRR_NAME", reptChrrName);
               exMap.put("reptChrrName", reptChrrName);
          }
          if (!"".equals(reptChrrPosi)) {
               reptChrrPosi = URLDecoder.decode(reptChrrPosi, "UTF-8");
               commandMap.put("REPT_CHRR_POSI", reptChrrPosi);
               exMap.put("reptChrrPosi", reptChrrPosi);
          }
          if (!"".equals(reptChrrTelx)) {
               reptChrrTelx = URLDecoder.decode(reptChrrTelx, "UTF-8");
               commandMap.put("REPT_CHRR_TELX", reptChrrTelx);
               exMap.put("reptChrrTelx", reptChrrTelx);
          }
          if (!"".equals(reptChrrMail)) {
               reptChrrMail = URLDecoder.decode(reptChrrMail, "UTF-8");
               commandMap.put("REPT_CHRR_MAIL", reptChrrMail);
               exMap.put("reptChrrMail", reptChrrMail);
          }
          if (PUBL_DATE == "없음" || "없음".equals(PUBL_DATE)) {
               commandMap.put("PUBL_DATE", "0");
               exMap.put("PUBL_DATE", "0");
          }
          if (COMM_MGNT_CD == "없음" || "없음".equals(COMM_MGNT_CD)) {
               commandMap.put("COMM_MGNT_CD", "0");
               exMap.put("COMM_MGNT_CD", "0");
          }
          if (!"".equals(tot_cnt_int)) {
               commandMap.put("TOT_CNT", tot_cnt_int);
               exMap.put("TOT_CNT", tot_cnt_int);
          }

          uploadList.add(exMap);

          boolean isSuccess = fdcrAd17Service.fdcrAd17UpdateNew(exMap, uploadList);
          returnAjaxString(response, isSuccess);
          exMap.clear();

          if (isSuccess) {
               Map<String, Object> mailMap = new HashMap<String, Object>();
               String reptChrrName2 = request.getParameter("reptChrrName");
               String reptChrrMail2 = request.getParameter("reptChrrMail");
               String COMM_NAME = request.getParameter("commName");
               String TOT_CNT2 = request.getParameter("tot_cnt");
               int tot_cnt_int2 = Integer.parseInt(TOT_CNT2);
               mailMap.put("TOT_CNT", tot_cnt_int2);

               if (!"".equals(reptChrrName2)) {
                    reptChrrName2 = URLDecoder.decode(reptChrrName2, "UTF-8");
                    mailMap.put("REPT_CHRR_NAME", reptChrrName2);
               }
               if (!"".equals(reptChrrMail2)) {
                    reptChrrMail2 = URLDecoder.decode(reptChrrMail2, "UTF-8");
                    mailMap.put("REPT_CHRR_MAIL", reptChrrMail2);
               }
               if (!"".equals(COMM_NAME)) {
                    COMM_NAME = URLDecoder.decode(COMM_NAME, "UTF-8");
                    mailMap.put("COMM_NAME", COMM_NAME);
               }

               fdcrAd17Service.fdcrAd17SendMail(mailMap);

               return returnUrl2(model, "보고가 완료되었습니다.", "/console/rcept/fdcrAd17List1.page");

          } else {
               return returnUrl(model, "보고에 실패했습니다.", "/console/rcept/fdcrAd17List1.page");
          }
     }

     private ArrayList<String> createArrayList(String... strings) {

          ArrayList<String> returnArrayList = new ArrayList<String>();

          for (String column : strings) {
               returnArrayList.add(column);
          }
          return returnArrayList;
     }

     private boolean columnCheck(String valueName, ArrayList<String> columnBanList) {

          boolean columnCheckFlag = true;

          for (String column : columnBanList) {
               if (column.equals(valueName)) {
                    columnCheckFlag = false;
                    break;
               }
          }
          return columnCheckFlag;
     }

}
