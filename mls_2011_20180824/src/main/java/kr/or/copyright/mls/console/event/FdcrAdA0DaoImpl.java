package kr.or.copyright.mls.console.event;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.event.inter.FdcrAdA0Dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository( "fdcrAdA0Dao" )
public class FdcrAdA0DaoImpl extends EgovComAbstractDAO implements FdcrAdA0Dao{

	public String getSaveEventSeq(){
		return (String) getSqlMapClientTemplate().queryForObject( "eventMgnt.getSaveEventSeq" );
	}

	public void addEvent( Map map ){
		getSqlMapClientTemplate().insert( "eventMgnt.addEvent", map );
	}

	public void addEventAgree( Map map ) throws Exception{
		getSqlMapClientTemplate().insert( "eventMgnt.addEventAgree", map );// ����
	}

	public List getEventList(){
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getEventList" );
	}

	public void delEventList( Map map ){
		getSqlMapClientTemplate().update( "eventMgnt.delEventList", map );
	}

	public List getEventDetl( Map map ){
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getEventDetl", map );
	}

	public List getEventDetlAgree( Map map ){
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getEventDetlAgree", map );
	}

	public void updateEvent( Map mData ){
		getSqlMapClientTemplate().update( "eventMgnt.updateEvent", mData );
	}

	public void delEventAgree( Map mData ){
		getSqlMapClientTemplate().delete( "eventMgnt.delEventAgree", mData );
	}

	public Map getCommItem( Map mData ){
		return (Map) getSqlMapClientTemplate().queryForObject( "eventMgnt.getCommItem", mData );
	}

	public void addCommItem( Map mData ){
		getSqlMapClientTemplate().insert( "eventMgnt.addMultiQustItemMgnt", mData );
	}

	public void uptCommItem( Map mData ){
		getSqlMapClientTemplate().update( "eventMgnt.uptCommItem", mData );
	}

	public Map getEventPart( Map mData ){
		return (Map) getSqlMapClientTemplate().queryForObject( "eventMgnt.getEventPart", mData );
	}

	public List getEventPartList( Map mData ){
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getEventPartList", mData );
	}

	public List getEventPartRsltList( Map mData ){
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getEventPartRsltList", mData );
	}

	@Transactional
	public void uptWinY( ArrayList<Map> list ) throws Exception{
		for( int i = 0; i < list.size(); i++ ){
			getSqlMapClientTemplate().update( "eventMgnt.uptWinY", list.get( i ) );
		}
		/*
		 * getSqlMapClientTemplate().getSqlMapClient().startBatch(); for (int i
		 * = 0; i < list.size(); i++) {
		 * getSqlMapClientTemplate().getSqlMapClient
		 * ().update("eventMgnt.uptWinY", list.get(i)); }
		 * getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		 */

		/*
		 * final ArrayList<Map> flist = list;
		 * getSqlMapClientTemplate().execute(new SqlMapClientCallback() { public
		 * Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException{
		 * executor.startBatch(); for (int i = 0; i < flist.size(); i++) {
		 * executor.update("eventMgnt.uptWinY", flist.get(i)); }
		 * executor.executeBatch(); return null; } });
		 */
	}

	@Transactional
	public void uptWinN( ArrayList<Map> list ) throws Exception{
		for( int i = 0; i < list.size(); i++ ){
			getSqlMapClientTemplate().update( "eventMgnt.uptWinN", list.get( i ) );
		}
		/*
		 * final ArrayList<Map> flist = list;
		 * getSqlMapClientTemplate().execute(new SqlMapClientCallback() { public
		 * Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException{
		 * executor.startBatch(); for (int i = 0; i < flist.size(); i++) {
		 * executor.update("eventMgnt.uptWinN", flist.get(i)); }
		 * executor.executeBatch(); return null; } });
		 */
	}

	public List getSelRandomWinning( Map mData ){
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getSelRandomWinning", mData );
	}

	public List getEventStatList() throws Exception{
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getEventStatList" );
	}

	public List getEventQnaList( Map map ) throws Exception{
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getEventQnaList", map );
	}

	public List getEventQnaTotal( Map map ) throws Exception{
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getEventQnaTotal", map );
	}

	public List getEventQnaDetail( Map map ) throws Exception{
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getEventQnaDetail", map );
	}

	public void delEventQnaDetail( Map map ) throws Exception{
		getSqlMapClientTemplate().update( "eventMgnt.delEventQnaDetail", map );
	}

	public void uptEventQnaAnswRegi( Map map ) throws Exception{
		getSqlMapClientTemplate().update( "eventMgnt.uptEventQnaAnswRegi", map );
	}

	public void delEventQnaMenuOpen( Map map ) throws Exception{
		getSqlMapClientTemplate().delete( "eventMgnt.delEventQnaMenuOpen", map );
	}

	public void addEventQnaMenuOpen( Map map ) throws Exception{
		getSqlMapClientTemplate().insert( "eventMgnt.addEventQnaMenuOpen", map );
	}

	public List getEventQnaMenuOpen() throws Exception{
		return getSqlMapClientTemplate().queryForList( "eventMgnt.getEventQnaMenuOpen" );
	}

}
