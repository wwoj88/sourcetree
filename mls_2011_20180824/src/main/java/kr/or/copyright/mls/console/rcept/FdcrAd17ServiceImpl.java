package kr.or.copyright.mls.console.rcept;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd15Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd17Service;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.IPUtil;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.NumberUtil;
import org.springframework.stereotype.Service;
import com.tobesoft.platform.data.Dataset;

@Service("fdcrAd17Service")
public class FdcrAd17ServiceImpl extends CommandService implements FdcrAd17Service {

     @Resource(name = "fdcrAd15Dao")
     private FdcrAd15Dao fdcrAd15Dao;

     /**
      * 위탁관리 저작물 보고 저장
      * 
      * @param commandMap
      * @return
      * @throws Exception
      */
     public boolean fdcrAd17Insert2(Map<String, Object> commandMap) throws Exception {

          boolean result = false;
          try {
               fdcrAd15Dao.adminWorksMgntNoDataInsert(commandMap);
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     /**
      * 일괄등록
      * 
      * @param commandMap
      * @param excelList
      * @return
      * @throws Exception
      */
     public boolean fdcrAd17Insert1(Map<String, Object> commandMap, ArrayList<Map<String, Object>> excelList) throws Exception {

          boolean result = false;
          try {
               List resultList = new ArrayList();
               List errorList = new ArrayList();

               String cntFlag = ""; // 스크립트 for문이 돈 횟수

               int cnt = 0;

               GregorianCalendar d = new GregorianCalendar();

               // 보고연월
               String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR))
                         + String.valueOf(((d.get(Calendar.MONTH) + 1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH) + 1) : String.valueOf(d.get(Calendar.MONTH) + 1));

               String reptYmd = reptYmdTmp.replaceAll(" ", "");

               // 장르코드
               String genreCd = (String) commandMap.get("genreCd");

               // 등록자아이디
               String rgstIdnt = (String) commandMap.get("USER_IDNT");

               // 위탁관리기관ID
               String trstOrgnCode = (String) commandMap.get("TRST_ORGN_CODE");

               // 총 등록건수
               String TOT_CNT = (String) commandMap.get("TOT_CNT");

               commandMap.put("REPT_YMD", reptYmd);// 보고연월
               commandMap.put("GENRE_CD", genreCd);// 장르코드
               commandMap.put("RGST_IDNT", rgstIdnt);// 등록자아이디

               commandMap.put("REPT_SEQN", fdcrAd15Dao.getReptSeqn(commandMap));
               fdcrAd15Dao.adminCommWorksReportInsertNew(commandMap);

               // 보고 카운트 조회 후 총 카운트갯수 = 카운트 수 맞으면 메일 발송(즉, 마지막 for문이 돌아갈때 최종 메일1번만 발송)
               commandMap.put("TRST_ORGN_CODE", trstOrgnCode);// 기관코드
               cntFlag = fdcrAd15Dao.getReptCnt(commandMap);

               ArrayList inList = new ArrayList();
               // for( int i = 0; i < excelList.size(); i++ ){
               // rFlag = fdcrAd15Dao.adminCommWorksReportInsert( commandMap );
               Map map = null;
               try {
                    map = excelList.get(0);
                    // seq를 사용하여 worksId 생성
                    // map.put( "TRST_ORGN_CODE", trstOrgnCode );// 위탁관리기관아이디
                    map.put("TRST_ORGN_CODE", trstOrgnCode);
                    map.put("GENRE_CD", genreCd);// 장르코드
                    if (fdcrAd15Dao.getExistCommWorks(map) == 0) {
                         map.put("REPT_YMD", reptYmd);// 보고년월
                         map.put("GENRE_CD", genreCd);// 장르코드
                         map.put("RGST_IDNT", rgstIdnt);// 등록자아이디
                         map.put("TRST_ORGN_CODE", trstOrgnCode); // 20170310추가
                         map.put("REPT_CNT", cntFlag); // 20170310추가
                         map.put("WORKS_ID", fdcrAd15Dao.getCommWorksId());// 저작물아이디
                         inList.add(map);
                         cnt++;
                    } else {
                         resultList.add(map);
                    }


                    // if( ( cnt % 100 == 0 || i == excelList.size() - 1 ) && inList.size() > 0 ){
                    if (fdcrAd15Dao.commWorksInsertBacth(inList) == null) {
                         try {
                              if (genreCd.equals("1")) {
                                   fdcrAd15Dao.commMusicInsertBacth(inList);// 음악
                              } else if (genreCd.equals("2")) {
                                   fdcrAd15Dao.commBookInsertBacth(inList);// 어문
                              } else if (genreCd.equals("3")) {
                                   fdcrAd15Dao.commScriptInsertBacth(inList);// 방송대본
                              } else if (genreCd.equals("4")) {
                                   fdcrAd15Dao.commMovieInsertBacth(inList);// 영화
                              } else if (genreCd.equals("5")) {
                                   fdcrAd15Dao.commBroadcastInsertBacth(inList);// 방송
                              } else if (genreCd.equals("6")) {
                                   fdcrAd15Dao.commNewsInsertBacth(inList);// 뉴스
                              } else if (genreCd.equals("7")) {
                                   fdcrAd15Dao.commArtInsertBacth(inList);// 미술
                              } else if (genreCd.equals("8")) {
                                   fdcrAd15Dao.commImageInsertBacth(inList);// 이미지
                              } else if (genreCd.equals("99")) {
                                   fdcrAd15Dao.commSideInsertBacth(inList);// 기타
                              }
                         } catch (Exception e) {
                              for (int j = 0; j < inList.size(); j++) {
                                   fdcrAd15Dao.adminCommWorksDelete((Map) inList.get(j));
                              }
                              errorList.addAll(inList);
                              e.printStackTrace();
                         }
                    } else {
                         for (int j = 0; j < inList.size(); j++) {
                              fdcrAd15Dao.adminCommWorksDelete((Map) inList.get(j));
                         }
                         errorList.addAll(inList);
                    }
                    inList.clear();
                    // }
               } catch (Exception e) {
                    errorList.addAll(inList);
                    inList.clear();
                    e.printStackTrace();
               }
               // }
               inList = null;

               if (errorList.size() > 0) {
                    Map map2 = null;
                    try {
                         for (int j = 0; j < errorList.size(); j++) {
                              map2 = (Map) errorList.get(j);
                              if (fdcrAd15Dao.adminCommWorksInsert(map2)) {
                                   try {
                                        boolean bool = false;
                                        if (genreCd.equals("1")) {
                                             bool = fdcrAd15Dao.adminCommMusicInsert(map2);// 음악
                                        } else if (genreCd.equals("2")) {
                                             bool = fdcrAd15Dao.adminCommBookInsert(map2);// 어문
                                        } else if (genreCd.equals("3")) {
                                             bool = fdcrAd15Dao.adminCommScriptInsert(map2);// 방송대본
                                        } else if (genreCd.equals("4")) {
                                             bool = fdcrAd15Dao.adminCommMovieInsert(map2);// 영화
                                        } else if (genreCd.equals("5")) {
                                             bool = fdcrAd15Dao.adminCommBroadcastInsert(map2);// 방송
                                        } else if (genreCd.equals("6")) {
                                             bool = fdcrAd15Dao.adminCommNewsInsert(map2);// 뉴스
                                        } else if (genreCd.equals("7")) {
                                             bool = fdcrAd15Dao.adminCommArtInsert(map2);// 미술
                                        } else if (genreCd.equals("8")) {
                                             bool = fdcrAd15Dao.adminCommImageInsert(map2);// 이미지
                                        } else if (genreCd.equals("99")) {
                                             bool = fdcrAd15Dao.adminCommSideInsert(map2);// 기타
                                        }
                                        if (bool) {
                                             cnt++;
                                        } else {
                                             fdcrAd15Dao.adminCommWorksDelete(map2);
                                             resultList.add(map2);
                                        }
                                   } catch (Exception e) {
                                        fdcrAd15Dao.adminCommWorksDelete(map2);
                                        resultList.add(map2);
                                        e.printStackTrace();
                                   }
                              } else {
                                   resultList.add(map2);
                              }
                              map2 = null;
                         }
                    } catch (Exception e) {
                         if (map2 != null) {
                              resultList.add(map2);
                         }
                         System.out.println("등록에러2 : worksMgntInsert()");
                         e.printStackTrace();
                    }
               }

               commandMap.put("REPT_WORKS_CONT", (cnt - errorList.size()));

               /*
                * if( cntFlag == TOT_CNT || cntFlag.equals( TOT_CNT ) ){
                * System.out.println("메일을 보냅니다!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                * 
                * 메일보내기
                * 
                * String host = Constants.MAIL_SERVER_IP; // smtp서버 String from = Constants.SYSTEM_MAIL_ADDRESS; //
                * 발신인 주소 - 시스템 대표 메일 String fromName = Constants.SYSTEM_NAME; String to = (String) commandMap.get(
                * "REPT_CHRR_MAIL" ); String toName = (String) commandMap.get( "REPT_CHRR_NAME" );
                * 
                * String subject = ""; // 메일 제목
                * 
                * MailInfo info = new MailInfo();
                * 
                * info.setFrom( from ); info.setFromName( fromName ); info.setHost( host ); info.setEmail( to );
                * info.setEmailName( toName );
                * 
                * if( to.length() > 0 ){ // 완료시간 Calendar cal = Calendar.getInstance(); SimpleDateFormat sdf = new
                * SimpleDateFormat( "yyyy-MM-dd aa hh:mm:ss" ); String rgstDttm = sdf.format( cal.getTime() );
                * 
                * // 기관명 String commName = (String) commandMap.get( "COMM_NAME" );
                * 
                * // 사용자 정보 셋팅
                * 
                * String userIdnt = (String) userMap.get("USER_IDNT"); String userName = (String)
                * userMap.get("USER_NAME"); String userEmail = (String) userMap.get("U_MAIL"); String userSms =
                * (String) userMap.get("MOBL_PHON");
                * 
                * // 총 등록(또는 수정)건 수 String reptWorksCont = NumberUtil.getCommaNumber( ( cnt - errorList.size() ) );
                * String mailTitlDivs = "등록"; String mailTitl = "[저작권찾기] 위탁관리 저작물 보고";
                * 
                * subject = mailTitl + " " + mailTitlDivs + "이 완료되었습니다.";
                * 
                * StringBuffer sb = new StringBuffer(); sb.append( "				<tr>" ); sb.append(
                * "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">"
                * ); sb.append( "					<p style=\"font-weight:bold; background:url(" + Constants.DOMAIN_HOME +
                * "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 보고 정보</p>"
                * ); sb.append( "					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">"
                * ); sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME +
                * "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 보고기관 : " + commName + "</li>"
                * ); sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME +
                * "/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">" + mailTitlDivs + " 완료 일자 : "
                * + rgstDttm + "</li>" ); sb.append( "						<li style=\"background:url(" +
                * Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">" +
                * mailTitlDivs + " 보고 건수 : " + TOT_CNT + " 건 </li>" ); //
                * sb.append("						<li style=\"background:url("+Constants.
                * DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 담당자 정보 : "
                * +userName+" (email:"+ // userEmail +", mobile: "+userSms+")</li>"); sb.append( "					</ul>"
                * ); sb.append( "					</td>" ); sb.append( "				</tr>" );
                * 
                * info.setMessage( sb.toString() ); info.setSubject( subject ); info.setMessage( new String(
                * MailMessage.getChangeInfoMailText2( info ) ) ); } MailManager manager =
                * MailManager.getInstance();
                * 
                * info = manager.sendMail( info );
                * 
                * if( info.isSuccess() ){ System.out.println( ">>>>>>>>>>>> 1. message success :" + info.getEmail()
                * ); fdcrAd15Dao.adminCommWorksCntUpdate( commandMap ); }else{ System.out.println(
                * ">>>>>>>>>>>> 1. message false   :" + info.getEmail() ); fdcrAd15Dao.adminCommWorksCntUpdate(
                * commandMap ); } }
                */

               if (genreCd.equals("1")) {
                    commandMap.put("ds_excel_music", resultList);// 음악
               } else if (genreCd.equals("2")) {
                    commandMap.put("ds_excel_book", resultList);// 어문
               } else if (genreCd.equals("3")) {
                    commandMap.put("ds_excel_script", resultList);// 방송대본
               } else if (genreCd.equals("4")) {
                    commandMap.put("ds_excel_movie", resultList);// 영화
               } else if (genreCd.equals("5")) {
                    commandMap.put("ds_excel_broadcast", resultList);// 방송
               } else if (genreCd.equals("6")) {
                    commandMap.put("ds_excel_news", resultList);// 뉴스
               } else if (genreCd.equals("7")) {
                    commandMap.put("ds_excel_art", resultList);// 미술
               } else if (genreCd.equals("8")) {
                    commandMap.put("ds_excel_image", resultList);// 이미지
               } else if (genreCd.equals("99")) {
                    commandMap.put("ds_excel_side", resultList);// 기타
               }

               resultList = null;
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     /**
      * 엑셀데이터 일괄수정
      * 
      * @param commandMap
      * @param excelList
      * @throws Exception
      */
     public boolean fdcrAd17Update1(Map<String, Object> commandMap, ArrayList<Map<String, Object>> excelList) throws Exception {

          boolean result = false;
          try {
               List returnList = null;
               int cnt = 0;
               String genreCd = (String) commandMap.get("GENRE_CODE");
               commandMap.put("GENRE_CD", genreCd);// 장르코드
               if (commandMap.get("GENRE_CODE").equals("1")) {// 어문 OR 도서
                    returnList = fdcrAd15Dao.adminCommMusicUpdate(commandMap, excelList);
                    commandMap.put("ds_excel_music", returnList);
               } else if (commandMap.get("GENRE_CODE").equals("2")) {// 어문
                    returnList = fdcrAd15Dao.adminCommBookUpdate(commandMap, excelList);
                    commandMap.put("ds_excel_book", returnList);
               } else if (commandMap.get("GENRE_CODE").equals("3")) {// 방송대본
                    returnList = fdcrAd15Dao.adminCommScriptUpdate(commandMap, excelList);
                    commandMap.put("ds_excel_script", returnList);
               } else if (commandMap.get("GENRE_CODE").equals("4")) {// 영화
                    returnList = fdcrAd15Dao.adminCommMovieUpdate(commandMap, excelList);
                    commandMap.put("ds_excel_movie", returnList);
               } else if (commandMap.get("GENRE_CODE").equals("5")) {// 방송
                    returnList = fdcrAd15Dao.adminCommBroadcastUpdate(commandMap, excelList);
                    commandMap.put("ds_excel_broadcast", returnList);
               } else if (commandMap.get("GENRE_CODE").equals("6")) {// 뉴스
                    returnList = fdcrAd15Dao.adminCommNewsUpdate(commandMap, excelList);
                    commandMap.put("ds_excel_news", returnList);
               } else if (commandMap.get("GENRE_CODE").equals("7")) {// 미술
                    returnList = fdcrAd15Dao.adminCommArtUpdate(commandMap, excelList);
                    commandMap.put("ds_excel_art", returnList);
               } else if (commandMap.get("GENRE_CODE").equals("8")) {// 이미지
                    returnList = fdcrAd15Dao.adminCommImageUpdate(commandMap, excelList);
                    commandMap.put("ds_excel_image", returnList);
               } else if (commandMap.get("GENRE_CODE").equals("99")) {// 기타
                    returnList = fdcrAd15Dao.adminCommSideUpdate(commandMap, excelList);
                    commandMap.put("ds_excel_side", returnList);
               }

               /*
                * 메일보내기
                */

               String host = Constants.MAIL_SERVER_IP; // smtp서버
               String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
               String fromName = Constants.SYSTEM_NAME;
               String to = (String) commandMap.get("REPT_CHRR_MAIL");
               String toName = (String) commandMap.get("REPT_CHRR_NAME");

               String subject = ""; // 메일 제목

               MailInfo info = new MailInfo();

               info.setFrom(from);
               info.setFromName(fromName);
               info.setHost(host);
               info.setEmail(to);
               info.setEmailName(toName);

               if (to.length() > 0) {
                    // 완료시간
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd aa hh:mm:ss");
                    String rgstDttm = sdf.format(cal.getTime());

                    // 기관명
                    String commName = (String) commandMap.get("COMM_NAME");

                    // 사용자 정보 셋팅
                    /*
                     * String userIdnt = (String) userMap.get("USER_IDNT"); String userName = (String)
                     * userMap.get("USER_NAME"); String userEmail = (String) userMap.get("U_MAIL"); String userSms =
                     * (String) userMap.get("MOBL_PHON");
                     */
                    // 총 등록(또는 수정)건 수
                    String reptWorksCont = NumberUtil.getCommaNumber((cnt - returnList.size()));
                    String mailTitlDivs = "수정";
                    String mailTitl = "[저작권찾기] 위탁관리 저작물 보고";

                    subject = mailTitl + " " + mailTitlDivs + "이 완료되었습니다.";

                    StringBuffer sb = new StringBuffer();
                    sb.append("				<tr>");
                    sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">");
                    sb.append("					<p style=\"font-weight:bold; background:url(" + Constants.DOMAIN_HOME
                              + "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 보고 정보</p>");
                    sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
                    sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 보고기관 : " + commName + "</li>");
                    sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">" + mailTitlDivs + " 완료 일자 : "
                              + rgstDttm + "</li>");
                    sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">" + mailTitlDivs + " 보고 건수 : "
                              + reptWorksCont + " 건 </li>");
                    // sb.append(" <li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif)
                    // no-repeat 0 8px; padding-left:12px;\"> 담당자 정보 : "+userName+" (email:"+
                    // userEmail +", mobile: "+userSms+")</li>");
                    sb.append("					</ul>");
                    sb.append("					</td>");
                    sb.append("				</tr>");

                    info.setMessage(sb.toString());
                    info.setSubject(subject);
                    info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
               }
               MailManager manager = MailManager.getInstance();

               info = manager.sendMail(info);

               if (info.isSuccess()) {
                    System.out.println(">>>>>>>>>>>> 1. message success :" + info.getEmail());
               } else {
                    System.out.println(">>>>>>>>>>>> 1. message false   :" + info.getEmail());
               }
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     /**
      * 등록여부확인
      * 
      * @param commandMap
      * @param excelList
      * @throws Exception
      */
     public void fdcrAd17List1(Map<String, Object> commandMap, ArrayList<Map<String, Object>> excelList) throws Exception {

          if (commandMap.get("GENRE_CODE").equals("1")) {// 음악
               List list = (List) fdcrAd15Dao.worksMgntChkCnt(commandMap, excelList);
               commandMap.put("ds_count", list);
          } else if (commandMap.get("GENRE_CODE").equals("2")) {// 어문
               List list = (List) fdcrAd15Dao.worksMgntChkCnt(commandMap, excelList);
               commandMap.put("ds_count", list);
          } else if (commandMap.get("GENRE_CODE").equals("3")) {// 방송대본
               List list = (List) fdcrAd15Dao.worksMgntChkCnt(commandMap, excelList);
               commandMap.put("ds_count", list);
          } else if (commandMap.get("GENRE_CODE").equals("4")) {// 영화
               List list = (List) fdcrAd15Dao.worksMgntChkCnt(commandMap, excelList);
               commandMap.put("ds_count", list);
          } else if (commandMap.get("GENRE_CODE").equals("5")) {// 방송
               List list = (List) fdcrAd15Dao.worksMgntChkCnt(commandMap, excelList);
               commandMap.put("ds_count", list);
          } else if (commandMap.get("GENRE_CODE").equals("6")) {// 뉴스
               List list = (List) fdcrAd15Dao.worksMgntChkCnt(commandMap, excelList);
               commandMap.put("ds_count", list);
          } else if (commandMap.get("GENRE_CODE").equals("7")) {// 미술
               List list = (List) fdcrAd15Dao.worksMgntChkCnt(commandMap, excelList);
               commandMap.put("ds_count", list);
          } else if (commandMap.get("GENRE_CODE").equals("99")) {// 기타
               List list = (List) fdcrAd15Dao.worksMgntChkCnt(commandMap, excelList);
               commandMap.put("ds_count", list);
          }
     }

     public boolean fdcrAd17InsertNodata(Map<String, Object> commandMap) throws Exception {

          boolean result = false;
          try {

               int cnt = 0;
               Map<String, Object> reportMap = new HashMap<String, Object>();
               GregorianCalendar d = new GregorianCalendar();

               // 보고연월
               String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR))
                         + String.valueOf(((d.get(Calendar.MONTH) + 1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH) + 1) : String.valueOf(d.get(Calendar.MONTH) + 1));

               String reptYmd = reptYmdTmp.replaceAll(" ", "");

               // 장르코드
               String genreCd = (String) commandMap.get("GENRE_CODE");

               // 등록자아이디
               // String rgstIdnt = (String) commandMap.get("USER_IDNT");

               // 위탁관리기관ID
               // String trstOrgnCode = (String) commandMap.get("TRST_ORGN_CODE");
               String REPT_CHRR_NAME = (String) commandMap.get("REPT_CHRR_NAME");
               String REPT_CHRR_POSI = (String) commandMap.get("REPT_CHRR_POSI");
               String REPT_CHRR_MAIL = (String) commandMap.get("REPT_CHRR_MAIL");
               String REPT_CHRR_TELX = (String) commandMap.get("REPT_CHRR_TELX");
               String COMM_NAME = (String) commandMap.get("COMM_NAME");
               String COMM_TELX = (String) commandMap.get("COMM_TELX");
               String COMM_REPS_NAME = (String) commandMap.get("COMM_REPS_NAME");

               reportMap.put("REPT_YMD", reptYmd);// 보고연월
               reportMap.put("GENRE_CD", genreCd);// 장르코드
               // reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
               reportMap.put("RGST_IDNT", ConsoleLoginUser.getUserId());// 등록자아이디
               reportMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());// 위탁관리기관 코드

               reportMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);// 장르코드
               reportMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);// 장르코드
               reportMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);// 장르코드
               reportMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);// 장르코드
               reportMap.put("COMM_NAME", COMM_NAME);// 장르코드
               reportMap.put("COMM_TELX", COMM_TELX);// 장르코드
               reportMap.put("COMM_REPS_NAME", COMM_REPS_NAME);// 장르코드

               adminCommWorksReportInsert(reportMap);
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     // 위탁관리 저작물 보고정보 등록(공통)
     public boolean adminCommWorksReportInsert(Map<String, Object> commandMap) throws Exception {

          boolean result = false;
          try {
               fdcrAd15Dao.adminCommWorksReportInsert(commandMap);
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     public void fdcrAd17Pop1(Map<String, Object> commandMap) throws Exception {

          // DAO호출
          String TRST_ORGN_DIVS_CODE = EgovWebUtil.getString(commandMap, "TRST_ORGN_DIVS_CODE");
          String SCH_CONAME = EgovWebUtil.getString(commandMap, "SCH_CONAME");

          List pageList = (List) fdcrAd15Dao.trstOrgnCoNameListCount(commandMap); // 페이징카운트
          int totCnt = ((BigDecimal) ((Map) pageList.get(0)).get("COUNT")).intValue();
          pagination(commandMap, totCnt, 0);
          List list = (List) fdcrAd15Dao.trstOrgnCoNameList(commandMap);
          commandMap.put("ds_list", list); // 로그인 사용자 정보

     }

     public boolean fdcrAd17InsertNew(Map<String, Object> exMap, ArrayList<Map<String, Object>> uploadList) throws Exception {

          boolean result = false;
          try {
               List resultList = new ArrayList();
               List errorList = new ArrayList();


               // 1. 장르코드 가져오기
               int genreCode = Integer.parseInt((exMap.get("genreCode") == null ? "0" : (String) exMap.get("genreCode")));

               if (genreCode == 0) {
                    throw new Exception("장르코드 NULL : worksMgntInsert");
               }


               int rFlag = 0;// 리포트FLAG
               int cntFlag = 0; // 스크립트 for문이 돈 횟수
               boolean cFlag = true;// 저작물FLAG
               boolean gFlag = true;// 장르별저작물FLAG
               int cnt = 0;


               // 2.보고년월 가져오기
               GregorianCalendar d = new GregorianCalendar();
               String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) + String.valueOf(((d.get(Calendar.MONTH) + 1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH) + 1) : String.valueOf(d.get(Calendar.MONTH) + 1));

               String reptYmd = reptYmdTmp.replaceAll(" ", "");

               String genreCd = (String) exMap.get("genreCode"); // 장르코드
               String rgstIdnt = (String) exMap.get("USER_IDNT"); // 등록자아이디
               String trstOrgnCode = (String) exMap.get("TRST_ORGN_CODE"); // 위탁관리기관ID
               String reptChrrName = (String) exMap.get("reptChrrName"); // 담당자
               String reptChrrPosi = (String) exMap.get("reptChrrPosi"); // 담당자 직위
               String reptChrrTelx = (String) exMap.get("reptChrrTelx"); // 담당자 전화번호
               String reptChrrMail = (String) exMap.get("reptChrrMail"); // 담당자 메일
               int TOT_CNT = (Integer) exMap.get("TOT_CNT"); // 엑셀 row수

               exMap.put("REPT_YMD", reptYmd);// 보고연월
               exMap.put("GENRE_CD", genreCd);// 장르코드
               exMap.put("RGST_IDNT", rgstIdnt);// 등록자아이디
               exMap.put("TRST_ORGN_CODE", trstOrgnCode);// 위탁관리기관ID
               exMap.put("TOT_CNT", TOT_CNT); // 엑셀 row수
               exMap.put("REPT_CHRR_NAME", reptChrrName); // 보고자명
               exMap.put("REPT_CHRR_POSI", reptChrrPosi); // 보고자 직책
               exMap.put("REPT_CHRR_TELX", reptChrrTelx); // 보고자 전화번호
               exMap.put("REPT_CHRR_MAIL", reptChrrMail); // 보고자 메일

               // 3.같은 달, 같은 날에 등록한 이력이 있는지 검색
               cntFlag = fdcrAd15Dao.getExistReptCnt(exMap);

               ArrayList inList = new ArrayList();
               Map map = null;
               try {
                    map = uploadList.get(0);
                    //System.out.println("MAP : "+map.toString());
                    // seq를 사용하여 worksId 생성
                    map.put("TRST_ORGN_CODE", trstOrgnCode);
                    map.put("GENRE_CD", genreCd);// 장르코드
                    if (fdcrAd15Dao.getExistCommWorks(map) == 0) { // 4. 같은 worksId로 등록한 저작물이 존재하는지 여부 체크
                         map.put("REPT_YMD", reptYmd);// 보고년월
                         map.put("GENRE_CD", genreCd);// 장르코드
                         map.put("RGST_IDNT", rgstIdnt);// 등록자아이디
                         map.put("TRST_ORGN_CODE", trstOrgnCode); // 20170310추가
                         map.put("REPT_CNT", cntFlag); // 20170310추가
                         map.put("WORKS_ID", fdcrAd15Dao.getCommWorksId());// 저작물아이디
                         inList.add(map); // 4-1. worksId가 없으면 inList에 add
                         cnt++;
                         if (cntFlag > 0) { // 3-1. 같은 달, 같은 날에 등록한 이력이 있는경우,
                              exMap.put("REPT_SEQN", fdcrAd15Dao.getReptSeqn(exMap));
                              exMap.put("REPT_WORKS_CONT", fdcrAd15Dao.getReptWorksCont(exMap) + 1);
                              exMap.put("MODI_WORKS_CONT", fdcrAd15Dao.getModiWorksCont(exMap));// 기존 보고저작물건수 + 신규 보고저작물건수
                              // exMap.put( "MODI_WORKS_CONT", TOT_CNT ); //MODI엔 신규 저작물 건수만
                              fdcrAd15Dao.adminCommWorksReportUpdate(exMap);
                         } else { // 3-2. 이력이 없는 경우,
                              exMap.put("REPT_WORKS_CONT", "1"); // 보고저작물건수
                              rFlag = fdcrAd15Dao.adminCommWorksReportInsert(exMap);
                         }
                    } else {
                         
                         resultList.add(map); // 4-2. worksId가 이미 존재하면 resultList(중복)에 add
                    }

                    System.out.println("inList   " + inList.size());
                    // if( ( cnt % 100 == 0 || 0 == uploadList.size() - 1 ) && inList.size() > 0 ){
                    if (inList.size() > 0) { // 5. 저작물 저장
                         if (fdcrAd15Dao.commWorksInsertBacth(inList) == null) {
                              try {
                                   switch (genreCode) {
                                        case 1:
                                             fdcrAd15Dao.commMusicInsertBacth(inList);
                                             break;// 음악
                                        case 2:
                                             fdcrAd15Dao.commBookInsertBacth(inList);
                                             break;// 어문
                                        case 3:
                                             fdcrAd15Dao.commScriptInsertBacth(inList);
                                             break;// 방송대본
                                        case 4:
                                             fdcrAd15Dao.commMovieInsertBacth(inList);
                                             break;// 영화
                                        case 5:
                                             fdcrAd15Dao.commBroadcastInsertBacth(inList);
                                             break;// 방송
                                        case 6:
                                             fdcrAd15Dao.commNewsInsertBacth(inList);
                                             break;// 뉴스
                                        case 7:
                                             fdcrAd15Dao.commArtInsertBacth(inList);
                                             break;// 미술
                                        case 8:
                                             fdcrAd15Dao.commImageInsertBacth(inList);
                                             break;// 이미지
                                        case 99:
                                             fdcrAd15Dao.commSideInsertBacth(inList);
                                             break;// 기타
                                        default:
                                             break;
                                   }
                              } catch (Exception e) {
                                   for (int j = 0; j < inList.size(); j++) {
                                        fdcrAd15Dao.adminCommWorksDelete((Map) inList.get(j));
                                   }
                                   errorList.addAll(inList);
                                   e.printStackTrace();
                              }
                         } else {
                              for (int j = 0; j < inList.size(); j++) {
                                   fdcrAd15Dao.adminCommWorksDelete((Map) inList.get(j));
                              }
                              errorList.addAll(inList);
                         }
                         inList.clear();
                    }
               } catch (Exception e) {
                    errorList.addAll(inList);
                    inList.clear();
                    e.printStackTrace();
               }
               inList = null;

               if (errorList.size() > 0) {
                    Map map2 = null;
                    try {
                         for (int j = 0; j < errorList.size(); j++) {
                              map2 = (Map) errorList.get(j);
                              if (fdcrAd15Dao.adminCommWorksInsert(map2)) {
                                   try {
                                        boolean bool = false;
                                        switch (genreCode) {
                                             case 1:
                                                  bool = fdcrAd15Dao.adminCommMusicInsert(map2);
                                                  break;// 음악
                                             case 2:
                                                  bool = fdcrAd15Dao.adminCommBookInsert(map2);
                                                  break;// 어문
                                             case 3:
                                                  bool = fdcrAd15Dao.adminCommScriptInsert(map2);
                                                  break;// 방송대본
                                             case 4:
                                                  bool = fdcrAd15Dao.adminCommMovieInsert(map2);
                                                  break;// 영화
                                             case 5:
                                                  bool = fdcrAd15Dao.adminCommBroadcastInsert(map2);
                                                  break;// 방송
                                             case 6:
                                                  bool = fdcrAd15Dao.adminCommNewsInsert(map2);
                                                  break;// 뉴스
                                             case 7:
                                                  bool = fdcrAd15Dao.adminCommArtInsert(map2);
                                                  break;// 미술
                                             case 8:
                                                  bool = fdcrAd15Dao.adminCommImageInsert(map2);
                                                  break;// 이미지
                                             case 99:
                                                  bool = fdcrAd15Dao.adminCommSideInsert(map2);
                                                  break;// 기타
                                             default:
                                                  break;
                                        }
                                        if (bool) {
                                             cnt++;
                                        } else {
                                             fdcrAd15Dao.adminCommWorksDelete(map2);
                                             resultList.add(map2);
                                        }
                                   } catch (Exception e) {
                                        fdcrAd15Dao.adminCommWorksDelete(map2);
                                        resultList.add(map2);
                                        e.printStackTrace();
                                   }
                              } else {
                                   resultList.add(map2);
                              }
                              map2 = null;
                         }
                    } catch (Exception e) {
                         if (map2 != null) {
                              resultList.add(map2);
                         }
                         System.out.println("등록에러2 : worksMgntInsert()");
                         e.printStackTrace();
                    }
               }
               resultList = null;
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     public void fdcrAd17SendMail(Map<String, Object> mailMap) {

          // System.out.println("메일을 보냅니다!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

          // 메일보내기
          String host = Constants.MAIL_SERVER_IP; // smtp서버
          String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
          String fromName = Constants.SYSTEM_NAME;
          String to = (String) mailMap.get("REPT_CHRR_MAIL");
          String toName = (String) mailMap.get("REPT_CHRR_NAME");

          String subject = ""; // 메일 제목

          MailInfo info = new MailInfo();

          info.setFrom(from);
          info.setFromName(fromName);
          info.setHost(host);
          info.setEmail(to);
          info.setEmailName(toName);

          if (to.length() > 0) {
               // 완료시간
               Calendar cal = Calendar.getInstance();
               SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd aa hh:mm:ss");
               String rgstDttm = sdf.format(cal.getTime());

               // 기관명
               String commName = (String) mailMap.get("COMM_NAME");
               // String TOT_CNT = (String) mailMap.get( "TOT_CNT" );
               int int_tot_cnt = (Integer) mailMap.get("TOT_CNT");

               // 총 등록(또는 수정)건 수
               String reptWorksCont = NumberUtil.getCommaNumber((int_tot_cnt));
               String mailTitlDivs = "등록";
               String mailTitl = "[저작권찾기] 위탁관리 저작물 보고";

               subject = mailTitl + " " + mailTitlDivs + "이 완료되었습니다.";

               StringBuffer sb = new StringBuffer();
               sb.append("				<tr>");
               sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">");
               sb.append("					<p style=\"font-weight:bold; background:url(" + Constants.DOMAIN_HOME
                         + "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 보고 정보</p>");
               sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
               sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 보고기관 : " + commName + "</li>");
               sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">" + mailTitlDivs + " 완료 일자 : " + rgstDttm
                         + "</li>");
               sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">" + mailTitlDivs + " 보고 건수 : "
                         + int_tot_cnt + " 건 </li>");
               // sb.append(" <li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif)
               // no-repeat 0 8px; padding-left:12px;\"> 담당자 정보 : "+userName+" (email:"+
               // userEmail +", mobile: "+userSms+")</li>");
               sb.append("					</ul>");
               sb.append("					</td>");
               sb.append("				</tr>");

               info.setMessage(sb.toString());
               info.setSubject(subject);
               info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
          }
          MailManager manager = MailManager.getInstance();

          info = manager.sendMail(info);

          if (info.isSuccess()) {
               System.out.println(">>>>>>>>>>>> 1. message success :" + info.getEmail());
               // fdcrAd15Dao.adminCommWorksCntUpdate( mailMap );
               mailMap.clear();
          } else {
               System.out.println(">>>>>>>>>>>> 1. message false   :" + info.getEmail());
               // fdcrAd15Dao.adminCommWorksCntUpdate( mailMap );
               mailMap.clear();
          }
     }

     public boolean fdcrAd17UpdateNew(Map<String, Object> exMap, ArrayList<Map<String, Object>> uploadList) throws Exception {

          boolean result = false;
          try {
               List resultList = new ArrayList();
               List errorList = new ArrayList();


               // 1. 장르코드 가져오기
               int genreCode = Integer.parseInt((exMap.get("genreCode") == null ? "0" : (String) exMap.get("genreCode")));

               if (genreCode == 0) {
                    throw new Exception("장르코드 NULL : worksMgntInsert");
               }


               int rFlag = 0;// 리포트FLAG
               int cntFlag = 0; // 스크립트 for문이 돈 횟수
               boolean cFlag = true;// 저작물FLAG
               boolean gFlag = true;// 장르별저작물FLAG
               int cnt = 0;


               // 2.보고년월 가져오기
               GregorianCalendar d = new GregorianCalendar();
               String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR))
                         + String.valueOf(((d.get(Calendar.MONTH) + 1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH) + 1) : String.valueOf(d.get(Calendar.MONTH) + 1));

               String reptYmd = reptYmdTmp.replaceAll(" ", "");

               String genreCd = (String) exMap.get("genreCode"); // 장르코드
               String rgstIdnt = (String) exMap.get("USER_IDNT"); // 등록자아이디
               String trstOrgnCode = (String) exMap.get("TRST_ORGN_CODE"); // 위탁관리기관ID
               String reptChrrName = (String) exMap.get("reptChrrName"); // 담당자
               String reptChrrPosi = (String) exMap.get("reptChrrPosi"); // 담당자 직위
               String reptChrrTelx = (String) exMap.get("reptChrrTelx"); // 담당자 전화번호
               String reptChrrMail = (String) exMap.get("reptChrrMail"); // 담당자 메일
               int TOT_CNT = (Integer) exMap.get("TOT_CNT"); // 엑셀 row수

               exMap.put("REPT_YMD", reptYmd);// 보고연월
               exMap.put("GENRE_CD", genreCd);// 장르코드
               exMap.put("RGST_IDNT", rgstIdnt);// 등록자아이디
               exMap.put("TRST_ORGN_CODE", trstOrgnCode);// 위탁관리기관ID
               exMap.put("TOT_CNT", TOT_CNT); // 엑셀 row수
               exMap.put("REPT_CHRR_NAME", reptChrrName); // 보고자명
               exMap.put("REPT_CHRR_POSI", reptChrrPosi); // 보고자 직책
               exMap.put("REPT_CHRR_TELX", reptChrrTelx); // 보고자 전화번호
               exMap.put("REPT_CHRR_MAIL", reptChrrMail); // 보고자 메일

               // 3.같은 달, 같은 날에 등록한 이력이 있는지 검색
               cntFlag = fdcrAd15Dao.getExistReptCnt(exMap);

               if (cntFlag > 0) { // 3-1. 같은 달, 같은 날에 등록한 이력이 있는경우,
                    exMap.put("REPT_SEQN", fdcrAd15Dao.getReptSeqn(exMap));
                    exMap.put("REPT_WORKS_CONT", fdcrAd15Dao.getReptWorksCont(exMap)); // 기존 보고저작물건수 + 신규 보고저작물건수
                    exMap.put("MODI_WORKS_CONT", fdcrAd15Dao.getModiWorksCont(exMap) + 1); // MODI엔 신규 저작물 건수만
                    fdcrAd15Dao.adminCommWorksReportUpdate(exMap);
               } else { // 3-2. 이력이 없는 경우,
                        // exMap.put( "REPT_WORKS_CONT", "1" ); //보고저작물건수
                    exMap.put("MODI_WORKS_CONT", "1"); // MODI엔 신규 저작물 건수만
                    rFlag = fdcrAd15Dao.adminCommWorksReportInsert(exMap);
               }
               ArrayList inList = new ArrayList();
               Map map = null;
               try {
                    map = uploadList.get(0);
                    // seq를 사용하여 worksId 생성
                    map.put("TRST_ORGN_CODE", trstOrgnCode);
                    map.put("GENRE_CD", genreCd);// 장르코드
                    map.put("REPT_YMD", reptYmd);// 보고년월
                    map.put("GENRE_CD", genreCd);// 장르코드
                    map.put("MODI_IDNT", rgstIdnt);// 등록자아이디
                    map.put("TRST_ORGN_CODE", trstOrgnCode); // 20170310추가
                    map.put("REPT_CNT", cntFlag); // 20170310추가
                    int worksId = Integer.parseInt(fdcrAd15Dao.getCommWorksIdForUpdate(map));
                    if (worksId == 0) {
                         cFlag = false;
                    } else {// 저작물id를 구했으므로 수정처리를 해야함
                         map.put("WORKS_ID", worksId);// 저작물아이디
                         inList.add(map); // 4-2. worksId가 이미 존재하면 resultList(중복)에 add
                         cnt++;
                         System.out.println("inList:::::::::::::::::::::::::::::::::::::::" + inList);
                         try {
                              switch (genreCode) {
                                   case 1:
                                        fdcrAd15Dao.commMusicUpdate(map);
                                        break;// 음악
                                   case 2:
                                        fdcrAd15Dao.commBookUpdate(map);
                                        break;// 어문
                                   case 3:
                                        fdcrAd15Dao.commScriptUpdate(map);
                                        break;// 방송대본
                                   case 4:
                                        fdcrAd15Dao.commMovieUpdate(map);
                                        break;// 영화
                                   case 5:
                                        fdcrAd15Dao.commBroadcastUpdate(map);
                                        break;// 방송
                                   case 6:
                                        fdcrAd15Dao.commNewsUpdate(map);
                                        break;// 뉴스
                                   case 7:
                                        fdcrAd15Dao.commArtUpdate(map);
                                        break;// 미술
                                   case 8:
                                        fdcrAd15Dao.commImageUpdate(map);
                                        break;// 이미지
                                   case 99:
                                        fdcrAd15Dao.commSideUpdate(map);
                                        break;// 기타
                                   default:
                                        break;
                              }
                              gFlag = true;
                         } catch (Exception e) {
                              gFlag = false;
                         }
                         if (gFlag) {
                              try {
                                   fdcrAd15Dao.commWorksUpdate(map);
                                   cFlag = true;
                              } catch (Exception e) {
                                   cFlag = false;
                              }
                         }
                    }
               } catch (Exception e) {
                    errorList.addAll(inList);
                    inList.clear();
                    e.printStackTrace();
               }
               inList = null;
               resultList = null;
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }
}
