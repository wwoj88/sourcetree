package kr.or.copyright.mls.mobile.stat.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.mobile.stat.model.MobileStat;
import kr.or.copyright.mls.mobile.stat.model.MobileStatFile;

public interface MobileStatService {
	
	public List<MobileStat> selectStat(MobileStat mobileStat);
	public int countStat(Map params);
	public List<MobileStat> detailStat(int bordSeqn);
	public List<MobileStatFile> fileSelectStat(int bordSeqn);
}
