package kr.or.copyright.mls.common.common.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tobesoft.platform.data.ColumnInfo;
import com.tobesoft.platform.data.Dataset;
import com.tobesoft.platform.data.DatasetList;
import com.tobesoft.platform.data.VariableList;
import com.tobesoft.platform.data.Variant;

public class MiDataUtil {

	//final static private Log logger = LogFactory.getLog(MiDataUtil.class);

	public static void mapToVariable(VariableList vl, HashMap map)
			throws Exception {
		Object[] columnNames = map.keySet().toArray();

		for (int i = 0; i < columnNames.length; i++) {
			Object value = map.get(columnNames[i]);
			vl.add(columnNames[i].toString(), value);
		}

		return;
	}

	public static void listToDataset(DatasetList dl, List list)
			throws Exception {

		// Dataset dataset = getDataset(id);
		Dataset dataset = new Dataset();
		dl.add("empList", dataset);

		if (list == null || list.isEmpty()) {
			return;
		}

		HashMap first = (HashMap) list.get(0);

		Object[] columnNames = first.keySet().toArray();
		for (int i = 0; i < columnNames.length; i++) {

			// log.debug(columnNames[i]);

			Object value = first.get(columnNames[i]);

			if (value == null || value instanceof String) {
				dataset.addColumn(columnNames[i].toString(),
						ColumnInfo.COLUMN_TYPE_STRING, 255);
			} else if (value instanceof BigDecimal || value instanceof Double) {
				dataset.addColumn(columnNames[i].toString(),
						ColumnInfo.COLUMN_TYPE_DECIMAL, 255);
			}
		}

		for (int i = 0; i < list.size(); i++) {

			HashMap item = (HashMap) list.get(i);
			int row = dataset.appendRow();

			for (int j = 0; j < columnNames.length; j++) {
				String columnName = columnNames[j].toString();
				dataset.setColumn(row, columnNames[j].toString(),
						getValueWithNullCheck(item, columnName));
			}
		}

		return;
	}

	/**
	 * Map 객체의 해당 Object Key 값의 null 체크
	 * 
	 * @param map
	 *            - Map 객체
	 * @param objKey
	 *            - propertie 명
	 * @return
	 */
	private static String getValueWithNullCheck(HashMap map, String objKey)
			throws Exception {
		Object result = map.get(objKey);
		return result == null ? null : result.toString();
	}

	/**
	 * List 객체를 Dataset 객체로 변환하여 반환한다.
	 * 
	 * @param id
	 *            - 생성될 Dataset ID
	 * @param list
	 *            - 변환 대상 List 객체
	 * @return
	 */
	public static Dataset listToDataset(String id, List list) throws Exception {

		return listToDataset(id, list, null);
	}

	/**
	 * List 객체를 Dataset 객체로 변환하여 반환한다.
	 * 
	 * @param id
	 *            - 생성될 Dataset ID
	 * @param list
	 *            - 변환 대상 List 객체
	 * @return
	 */
	public static Dataset listToDataset(String id, List list, String addColNm)
			throws Exception {

		// Dataset dataset = new Dataset(id, CommonConstants.CHAR_SET);
		Dataset dataset = new Dataset(id);

		if (list == null || list.isEmpty()) {

			return dataset;
		}

		Map map = (Map) list.get(0);
		Object[] colNames = map.keySet().toArray();

		String colName;
		Object colValue;

		for (int i = 0; i < colNames.length; i++) {

			colName = colNames[i].toString();
			colValue = map.get(colNames[i]);

			// log.debug("Object Type : " + map.get(colNames[i]));

			if (colValue instanceof BigDecimal || colValue instanceof Double) {

				dataset.addColumn(colName, ColumnInfo.COLUMN_TYPE_DECIMAL, 256);

			} else if (colValue instanceof java.sql.Date) {

				dataset.addColumn(colName, ColumnInfo.COLUMN_TYPE_DATE, 256);
				// dataset.addColumn(colName, ColumnInfo.COLUMN_TYPE_DECIMAL,
				// 256);

			} else {

				dataset.addColumn(colName, ColumnInfo.COLUMN_TYPE_STRING, 256);
			}
		}

		if (addColNm != null && !"".equals(addColNm)) {

			dataset.addColumn(addColNm, ColumnInfo.COLUMN_TYPE_STRING, 256);
		}

		int row;

		for (int i = 0; i < list.size(); i++) {

			map = (Map) list.get(i);

			row = dataset.appendRow();

			for (int j = 0; j < colNames.length; j++) {

				colName = colNames[j].toString();
				colValue = map.get(colName);

				/* DB CHAR 타입 사용 시 trim 처리, 전각 space 문자 trim 처리 */
				if (colValue instanceof String) {

					colValue = map.get(colName) == null ? "" : ((String) map
							.get(colName)).replaceAll("　", " ").trim();
				}

				dataset.setColumn(row, colName, new Variant(colValue));
			}
		}
		return dataset;
	}

	/**
	 * List 객체를 공통코드 Dataset으로 변환하여 반환하다.
	 * 
	 * @param id
	 *            - 생성될 Dataset ID
	 * @param list
	 *            - 변환 대상 List 객체
	 * @return
	 */
	public static Dataset listToCommCdDs(Map mapCombo, List list)
			throws Exception {

		String sPrefix = "ds_code";

		String sHeadAll = "- 전체 -";
		String sHeadSel = "- 선택 -";
		String sAddColNm = "_CDNAME";

		String sDsId = (String) mapCombo.get("CDGUBN");

		// N - Normal, A - All(- 전체 -), S - Select(- 선택 -)
		String sHeadFlag = StringUtils.defaultIfEmpty((String) mapCombo.get("HEADFLAG"), "N");
					
		List lstCode = new ArrayList();

		Map map;

		for (int i = 0; i < list.size(); i++) {

			map = (Map) list.get(i);

			if (sDsId.equals(map.get("CDGUBN"))) {

				lstCode.add(map);
			}
		}


		if (lstCode == null || lstCode.isEmpty()) {

//			return new Dataset(sPrefix + sDsId, CommonConstants.CHAR_SET);
			return new Dataset(sPrefix + sDsId);
		}

		Dataset ds = listToDataset(sPrefix + sDsId, lstCode, sAddColNm);

		for (int i = 0; i < ds.getRowCount(); i++) {

			// ds.setColumn(i, "CDNAME", ds.getColumnAsString(i, "CDCODE") + " "
			// + ds.getColumnAsString(i, "CDNAME"));
			ds.setColumn(i, sAddColNm, ds.getColumnAsString(i, "CDCODE") + " "
					+ ds.getColumnAsString(i, "CDNAME"));
		}

		if ("A".equals(sHeadFlag)) {

			ds.insertRow(0);
			ds.setColumn(0, "CDNAME", sHeadAll);
			ds.setColumn(0, sAddColNm, sHeadAll);

		} else if ("S".equals(sHeadFlag)) {

			ds.insertRow(0);
			ds.setColumn(0, "CDNAME", sHeadSel);
			ds.setColumn(0, sAddColNm, sHeadSel);
		}

		return ds;
	}

	/**
	 * Dataset의 특정 Row를 HashMap Type으로 반환한다.
	 * 
	 * @param dataset
	 *            Dataset ID
	 * @param rnum
	 *            Dataset Row Index
	 * @return HashMap Object
	 */
	public static HashMap getMapFromDataset(Dataset dataset, int rnum)
			throws Exception {

		if (dataset == null || dataset.getRowCount() == 0) {

			return null;
		}

		HashMap result = new HashMap();

		for (int i = 0; i < dataset.getColumnCount(); i++) {

			String columnName = dataset.getColumnId(i);

			result.put(columnName, dataset.getColumnAsObject(rnum, columnName));
		}
		return result;
	}
}
