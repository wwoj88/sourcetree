package kr.or.copyright.mls.rghtPrps.model;

import kr.or.copyright.mls.common.BaseObject;

public class PrpsAttc extends BaseObject {

	private String PRPS_MAST_KEY;
	private String USER_IDNT;
	private String PRPS_DIVS;
	private String PRPS_SEQN;
	private String TRST_ORGN_CODE;
	private String ATTC_SEQN;
	private String FILE_NAME;
	private String FILE_PATH;
	private String FILE_SIZE;
	private String REAL_FILE_NAME;
	private String RGST_IDNT;
	private String UPDT_IDNT;
	private String MODI_IDNT;
	public String getPRPS_MAST_KEY() {
		return PRPS_MAST_KEY;
	}
	public void setPRPS_MAST_KEY(String prps_mast_key) {
		PRPS_MAST_KEY = prps_mast_key;
	}
	public String getUSER_IDNT() {
		return USER_IDNT;
	}
	public void setUSER_IDNT(String user_idnt) {
		USER_IDNT = user_idnt;
	}
	public String getPRPS_DIVS() {
		return PRPS_DIVS;
	}
	public void setPRPS_DIVS(String prps_divs) {
		PRPS_DIVS = prps_divs;
	}
	public String getPRPS_SEQN() {
		return PRPS_SEQN;
	}
	public void setPRPS_SEQN(String prps_seqn) {
		PRPS_SEQN = prps_seqn;
	}
	public String getTRST_ORGN_CODE() {
		return TRST_ORGN_CODE;
	}
	public void setTRST_ORGN_CODE(String trst_orgn_code) {
		TRST_ORGN_CODE = trst_orgn_code;
	}
	public String getATTC_SEQN() {
		return ATTC_SEQN;
	}
	public void setATTC_SEQN(String attc_seqn) {
		ATTC_SEQN = attc_seqn;
	}
	public String getFILE_NAME() {
		return FILE_NAME;
	}
	public void setFILE_NAME(String file_name) {
		FILE_NAME = file_name;
	}
	public String getFILE_PATH() {
		return FILE_PATH;
	}
	public void setFILE_PATH(String file_path) {
		FILE_PATH = file_path;
	}
	public String getFILE_SIZE() {
		return FILE_SIZE;
	}
	public void setFILE_SIZE(String file_size) {
		FILE_SIZE = file_size;
	}
	public String getREAL_FILE_NAME() {
		return REAL_FILE_NAME;
	}
	public void setREAL_FILE_NAME(String real_file_name) {
		REAL_FILE_NAME = real_file_name;
	}
	public String getRGST_IDNT() {
		return RGST_IDNT;
	}
	public void setRGST_IDNT(String rgst_idnt) {
		RGST_IDNT = rgst_idnt;
	}
	public String getUPDT_IDNT() {
		return UPDT_IDNT;
	}
	public void setUPDT_IDNT(String updt_idnt) {
		UPDT_IDNT = updt_idnt;
	}
	public String getMODI_IDNT() {
		return MODI_IDNT;
	}
	public void setMODI_IDNT(String modi_idnt) {
		MODI_IDNT = modi_idnt;
	}
}
