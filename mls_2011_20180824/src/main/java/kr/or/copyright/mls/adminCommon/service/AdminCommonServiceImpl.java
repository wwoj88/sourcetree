package kr.or.copyright.mls.adminCommon.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import kr.or.copyright.mls.adminCommon.dao.AdminCommonDao;
import kr.or.copyright.mls.common.service.BaseService;
import com.softforum.xdbe.xCrypto;
import com.tobesoft.platform.data.Dataset;

public class AdminCommonServiceImpl extends BaseService implements AdminCommonService {

     // private Log logger = LogFactory.getLog(getClass());

     private AdminCommonDao adminCommonDao;

     public void setAdminCommonDao(AdminCommonDao adminCommonDao) {

          this.adminCommonDao = adminCommonDao;
     }


     // adminCommonList 조회
     public void login() throws Exception {

          /* pwsd암호화 str 20121106 정병호 */
          xCrypto.RegisterEx("normal", 2, new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "normal");
          xCrypto.RegisterEx("pattern7", 2, new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
          /* pwsd암호화 end 20121106 정병호 */

          boolean isValid = true;
          String msg = "";
          String passWord = "";
          Map result = new HashMap();

          Dataset ds_condition = getDataset("ds_condition");

          /* pwsd암호화 str 20121106 정병호 */
          passWord = ds_condition.getColumnAsString(0, "passWord");

          String sOutput_H = null; // 사용자 입력값 암호화
          if (passWord.length() < 40) {// 사용자입력 패스워드
               passWord = passWord.toUpperCase();
               sOutput_H = xCrypto.Hash(6, passWord);
          } else {
               sOutput_H = passWord;
          }

          /* pwsd암호화 end 20121106 정병호 */

          Map map = getMap(ds_condition, 0);

          map.put("passWord", sOutput_H);

          // DAO호출
          List list = (List) adminCommonDao.login(map);

          if (list == null || list.size() < 1) {
               msg = "해당 아이디가 없습니다..";
               isValid = false;
          } else {
               result = (Map) list.get(0);
               result.put("IP", map.get("ip"));

               String pswd = result.get("PSWD").toString();

               if (!sOutput_H.equals(pswd))/* pwsd암호화 20121106 정병호 */
               {
                    msg = "패스워드를 확인하세요.";
                    isValid = false;
               } else {
                    List menuList = (List) adminCommonDao.selectLoginMenu(result);

                    for (int i = 0; i < menuList.size(); i++) {
                         String formId = (String) ((HashMap) menuList.get(i)).get("FORMID");
                         String formTemp = "";
                         String paramTemp = "";

                         if (formId != null && formId.length() > 0) {
                              int idx = formId.indexOf("?");

                              if (idx > -1) {

                                   // FORMID
                                   formTemp = formId.substring(0, idx);
                                   ((HashMap) menuList.get(i)).put("FORMID", formTemp);

                                   // PARAM
                                   paramTemp = formId.substring((idx + 1), formId.length());
                                   StringTokenizer st = new StringTokenizer(paramTemp, "&");
                                   ArrayList aTempList = new ArrayList();

                                   while (st.hasMoreTokens()) {
                                        aTempList.add(st.nextToken());
                                   }

                                   String param = "";
                                   for (int k = 0; k < aTempList.size(); k++) {
                                        param = param + aTempList.get(k) + " ";
                                        System.out.println(">>> PARAM 확인 : " + param);
                                   }
                                   ((HashMap) menuList.get(i)).put("PARAM", param);
                                   System.out.println(" <<<< PARAM : " + ((HashMap) menuList.get(i)).get("PARAM"));
                              }

                         } else {
                              ((HashMap) menuList.get(i)).put("PARAM", " ");
                              System.out.println(" <<<< PARAM : " + ((HashMap) menuList.get(i)).get("PARAM"));
                         }
                    }
                    addList("gds_menu_info", menuList); // 로그인 사용자 메뉴
               }
          }

          if (!isValid) {
               setResultMessage(-1001, msg);
               return;
          } else {

               // 로그 저장 14.11.24
               Map logMap = new HashMap();
               logMap.put("MANAGER_ID", map.get("userId"));
               logMap.put("MENU_URL", "login");
               logMap.put("PROC_STATUS", "로그인");
               logMap.put("IP_ADDRESS", map.get("ip"));
               adminCommonDao.insertAdminLogDo(logMap);


               addList("gds_common", result); // 로그인 사용자 정보

               setResultMessage(0, "Success!");

          }
     }

     /**
      * 2014.11.24 관리자 로그 저장
      */
     public void insertAdminLogDo() throws Exception {

          Dataset gds_admin_log = getDataset("gds_admin_log");

          Map map = getMap(gds_admin_log, 0);

          adminCommonDao.insertAdminLogDo(map);

     }

}
