package kr.or.copyright.mls.console.effort;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd04Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 상당한노력(신청 및 조회공고) > 이의신청
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd04Controller extends DefaultController{

	@Resource( name = "fdcrAd04Service" )
	private FdcrAd04Service fdcrAd04Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd04Controller.class );

	/**
	 * 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd04List1.page" )
	public String fdcrAd04List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd04List1 Start" );
		String tabIndex = EgovWebUtil.getString( commandMap, "TAB_INDEX" );
		if( "".equals( tabIndex ) ){
			tabIndex = "0";
		}
		commandMap.put( "TAB_INDEX", tabIndex );
		model.addAttribute( "commandMap", commandMap );
		return "effort/fdcrAd04List1.tiles";
	}

	/**
	 * 목록
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd04List1Sub1.page" )
	public String fdcrAd04List1Sub1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd04List1 Start" );
		EgovWebUtil.ajaxCommonEncoding( request, commandMap );
		// 파라미터 셋팅
		int tabIndex = EgovWebUtil.getToInt( commandMap, "TAB_INDEX" );
		commandMap.put( "SCH_STAT_OBJC_CD", tabIndex+1 ); // tab index

		fdcrAd04Service.fdcrAd04List1( commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		model.addAttribute( "ds_count", commandMap.get( "ds_count" ) );
		model.addAttribute( "ds_count0", ( (List) commandMap.get( "ds_count" ) ).get( 0 ) );
		model.addAttribute( "ds_count1", ( (List) commandMap.get( "ds_count" ) ).get( 1 ) );
		model.addAttribute( "ds_count2", ( (List) commandMap.get( "ds_count" ) ).get( 2 ) );
		model.addAttribute( "commandMap", commandMap );
		logger.debug( "fdcrAd04List1 End" );
		return "effort/fdcrAd04List1Sub1";
	}
	
	/**
	 * 상당한노력 상세
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd04UpdateForm1.page" )
	public String fdcrAd04UpdateForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd04UpdateForm1 Start" );
		// 파라미터 셋팅
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "OPEN_YN", "" );
		commandMap.put( "BORD_SEQN", "" );

		fdcrAd04Service.fdcrAd04UpdateForm1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_file", commandMap.get( "ds_file" ) );
		model.addAttribute( "ds_object", commandMap.get( "ds_object" ) );
		model.addAttribute( "ds_object_file", commandMap.get( "ds_object_file" ) );
		model.addAttribute( "ds_supl_list", commandMap.get( "ds_supl_list" ) );
		logger.debug( "fdcrAd04UpdateForm1 End" );
		return "test";
	}

	/**
	 * 법정허락 대상 저작물 - 상세
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd04UpdateForm2.page" )
	public String fdcrAd04UpdateForm2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd04UpdateForm2 Start" );
		// 파라미터 셋팅
		commandMap.put( "WORKS_ID", "" );
		commandMap.put( "OPEN_YN", "" );

		fdcrAd04Service.fdcrAd04UpdateForm2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_object", commandMap.get( "ds_object" ) );
		model.addAttribute( "ds_object_file", commandMap.get( "ds_object_file" ) );
		logger.debug( "fdcrAd04UpdateForm2 End" );
		return "test";
	}

	/**
	 * 상당한노력 상세 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings( "rawtypes" )
	@RequestMapping( value = "/console/effort/fdcrAd04Download1.page" )
	public void fdcrAd04Download1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "OPEN_YN", "" );
		commandMap.put( "BORD_SEQN", "" );

		fdcrAd04Service.fdcrAd04UpdateForm1( commandMap );
		List list = (List) commandMap.get( "ds_list" );
		for( int i = 0; i < list.size(); i++ ){
			Map map = (Map) list.get( i );
			String worksId = (String) map.get( "WORKS_ID" );
			if( worksId.equals( commandMap.get( "WORKS_ID" ) ) ){
				String filePath = (String) map.get( "FILE_PATH" );
				String fileName = (String) map.get( "FILE_NAME" );
				String realFileName = (String) map.get( "REAL_FILE_NAME" );
				download( request, response, filePath + realFileName, fileName );
			}
		}
	}

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 처리상태 수정(신청,보완,신청승인,반려)
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd04Update1.page" )
	public void fdcrAd04Update1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "OPEN_YN", "" );
		commandMap.put( "BORD_SEQN", "" );

		boolean isSuccess = fdcrAd04Service.fdcrAd04Update1( commandMap );
		returnAjaxString( response, isSuccess );
	}

	/**
	 * 보상금 공탁 공고 게시판 - 공고승인
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd04Update2.page" )
	public void fdcrAd04Update2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "OPEN_YN", "" );
		commandMap.put( "BORD_SEQN", "" );

		boolean isSuccess = fdcrAd04Service.fdcrAd04Update2( commandMap );
		returnAjaxString( response, isSuccess );
	}

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 삭제
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd04Update3.page" )
	public void fdcrAd04Update3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "OPEN_YN", "" );
		commandMap.put( "BORD_SEQN", "" );

		boolean isSuccess = fdcrAd04Service.fdcrAd04Update3( commandMap );
		returnAjaxString( response, isSuccess );
	}

}
