package kr.or.copyright.mls.common.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;
import com.dreamsecurity.jcaos.asn1.re;

import com.tagfree.util.URLEncoder;
import kr.or.copyright.common.userLogin.controller.LoginFormController;

public class DownloadFile extends AbstractView {

     private static final Log LOG = LogFactory.getLog(LoginFormController.class);

     // 다운로드 파일의 컨텐트 타입을 지정
     @Override
     public String getContentType() {

          return "application/octet-stream";
     }

     @Override
     protected void renderMergedOutputModel(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {

          try {


               String fileName = (String) map.get("fileName");
               String realFileName = (String) map.get("realFileName");
               
               String path = (String) map.get("path");
               if (fileName == null || fileName.equals("")) {
                    fileName = request.getParameter("filename");
            
               }

               System.out.println("FILE22NAME :::  " + fileName);


               String encodedFilename = "";


               String header = request.getHeader("User-Agent");

               if (header.indexOf("MSIE") > -1) {

                    encodedFilename = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");

               }

               else if (header.indexOf("Trident") > -1) {

                    encodedFilename = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");

               }

               else if (header.indexOf("Chrome") > -1) {

                    StringBuffer sb = new StringBuffer();

                    for (int i = 0; i < fileName.length(); i++) {

                         char c = fileName.charAt(i);

                         if (c > '~') {

                              sb.append(URLEncoder.encode("" + c, "UTF-8"));

                         }

                         else {

                              sb.append(c);

                         }

                    }

                    encodedFilename = sb.toString();

               }

               else if (header.indexOf("Opera") > -1) {

                    encodedFilename = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";

               }

               else if (header.indexOf("Safari") > -1) {

                    encodedFilename = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";

                    encodedFilename = URLDecoder.decode(encodedFilename);

               } else {

                    encodedFilename = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";

                    encodedFilename = URLDecoder.decode(encodedFilename);

               }


               System.out.println("다운로드..");
               String filepath2 =null;
               if(realFileName ==null) {
                    filepath2 = fileName;
               }else {
                    filepath2= realFileName;
               }
               // 업로드 경로가 저장된 파일 객체
               File file = new File(path + filepath2);
   
               // 파일 다운로드
               response.setContentType(this.getContentType());
               response.setContentLength((int) file.length());// 파일 크기 설정
               // 다운로드 파일에 대한 설정
               response.setHeader("Content-Disposition", "attachment; filename=\"" + encodedFilename + "\";"); // 데이터 인코딩이 바이너리 파일임을 명시

               response.setHeader("Content-Transfer-encoding", "binary");
               // 실제 업로드 파일을 inputStream 으로 읽어서
               // response 에 연결된 OutputStream으로 전송하겠다.
               OutputStream os = response.getOutputStream();
               FileInputStream fis = new FileInputStream(file);
               FileCopyUtils.copy(fis, os);
               System.out.println("다운로드.." + file.getName());
          } catch (Exception e) {
               // TODO: handle exception
               e.printStackTrace();
               //return;
               //
          }

     }
}
