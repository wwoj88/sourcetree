package kr.or.copyright.mls.console.cnsgn;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.cnsgn.inter.FdcrAd59Service;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd15Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tobesoft.platform.data.Dataset;

@Service( "fdcrAd59Service" )
public class FdcrAd59ServiceImpl extends CommandService implements FdcrAd59Service{

	// old AdminWorksMgntDao
	@Autowired
	private FdcrAd15Dao fdcrAd15Dao;

	/**
	 * 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd59List1( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd15Dao.worksMgntReptView( commandMap );
		List list_0 = (List) fdcrAd15Dao.worksMgntReptViewCoCnt( commandMap );

		commandMap.put( "ds_list", list );
		commandMap.put( "ds_list_0", list_0 );
	}

	/**
	 * 월별 보고현황 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd59List2( Map<String, Object> commandMap ) throws Exception{
		//월별 보고정보
		List dsReport = (List)fdcrAd15Dao.worksMgntReptInfo(commandMap);
		
		//월별,장르별 통계 정보
		List dsCount = (List)fdcrAd15Dao.worksMgntReptView(commandMap);
		
		//월별 보고현황 목록 카운트
		List pageList = (List) fdcrAd15Dao.totalRowWorksMgntList( commandMap ); // 페이징카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
				
		//월별 저작물 리스트
		List list = (List)fdcrAd15Dao.worksMgntList(commandMap);
		
		commandMap.put("ds_count", dsCount);
		commandMap.put("ds_report", dsReport);
		commandMap.put("ds_list", list);
		commandMap.put( "ds_page", pageList );
	}

	public void fdcrAd59List3( Map<String, Object> commandMap ) throws Exception{
		//월별 보고정보
		List dsReport = (List)fdcrAd15Dao.worksMgntReptInfo(commandMap);
		
		//월별,장르별 통계 정보
		List dsCount = (List)fdcrAd15Dao.worksMgntReptView(commandMap);
		
		//월별 보고현황 목록 카운트
		List pageList = (List) fdcrAd15Dao.totalRowWorksMgntList( commandMap ); // 페이징카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
				
		//월별 저작물 리스트
		List list = (List)fdcrAd15Dao.worksMgntListMonth(commandMap);
		
		commandMap.put("ds_count", dsCount);
		commandMap.put("ds_report", dsReport);
		commandMap.put("ds_list", list);
		commandMap.put( "ds_page", pageList );
	}
}
