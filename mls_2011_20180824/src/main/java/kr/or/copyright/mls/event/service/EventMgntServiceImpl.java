package kr.or.copyright.mls.event.service;


import java.util.List;

import kr.or.copyright.mls.event.dao.EventMgntDao;
import kr.or.copyright.mls.event.model.EventMgntVO;
import kr.or.copyright.mls.event.model.QnABoardVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EventMgntServiceImpl implements EventMgntService{

	@Autowired
	EventMgntDao eventMgntDao;
	
	public List getEventUserList(EventMgntVO vo) {
		return (List) eventMgntDao.getEventUserList(vo);
	}

	public int getEventUserListTotal(EventMgntVO vo) {
		return eventMgntDao.getEventUserListTotal(vo);
	}

	public EventMgntVO getEventUserDetail(EventMgntVO vo) {
		return eventMgntDao.getEventUserDetail(vo);
	}

	public List getEventView02List(EventMgntVO vo) {
		return eventMgntDao.getEventView02List(vo);
	}

	public int getEventView02Total(EventMgntVO vo) {
		return eventMgntDao.getEventView02Total(vo);
	}

	public List getEventAgreeItem(EventMgntVO vo) {
		return eventMgntDao.getEventAgreeItem(vo);
	}

	@Transactional
	public void addEventView02(EventMgntVO vo) throws Exception {
		vo.setPart_cnt(eventMgntDao.getEventPartCnt(vo)+"");
		eventMgntDao.addEventPart(vo);
		eventMgntDao.addEventPartRslt(vo);
	}

	public int getEventPartCnt(EventMgntVO vo) {
		return eventMgntDao.getEventPartCnt(vo);
	}

	@Transactional
	public void addEventView01(EventMgntVO vo) throws Exception {
		vo.setPart_cnt(eventMgntDao.getEventPartCnt(vo)+"");
		
		eventMgntDao.addEventPart(vo);
		
		for (int i = 0; i < vo.getAction1().size(); i++) {
			EventMgntVO vv = (EventMgntVO) vo.getAction1().get(i);
			vv.setUser_idnt(vo.getUser_idnt());
			vv.setPart_cnt(vo.getPart_cnt());
			eventMgntDao.addEventPartRslt(vv);
		}
	}

	public List getWinningList(EventMgntVO vo) {
		return eventMgntDao.getWinningList(vo);
	}

	public int getWinningListTotal(EventMgntVO vo) {
		return eventMgntDao.getWinningListTotal(vo);
	}

	public String getTopMenu(int i) {
		return eventMgntDao.getTopMenu(i);
	}

	public List getQnaList(QnABoardVO vo) throws Exception {
		return (List) eventMgntDao.getQnaList(vo);
	}

	public int getQnaListCnt(QnABoardVO vo) throws Exception {
		return eventMgntDao.getQnaListCnt(vo);
	}

	public void addQnaRegi(QnABoardVO vo) throws Exception {
		eventMgntDao.addQnaRegi(vo);
	}

	public QnABoardVO getQnaDetail(QnABoardVO vo) throws Exception {
		return eventMgntDao.getQnaDetail(vo);
	}

	public void delQnaDelete(QnABoardVO vo) {
		eventMgntDao.delQnaDelete(vo);
	}

	public void uptQnaModi(QnABoardVO vo) {
		eventMgntDao.uptQnaModi(vo);
	}

}
