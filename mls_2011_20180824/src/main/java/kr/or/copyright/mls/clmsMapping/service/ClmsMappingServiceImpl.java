package kr.or.copyright.mls.clmsMapping.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.clmsMapping.dao.ClmsMappingDao;
import kr.or.copyright.mls.common.service.BaseService;

import com.tobesoft.platform.data.Dataset;

public class ClmsMappingServiceImpl extends BaseService implements ClmsMappingService {

	//private Log logger = LogFactory.getLog(getClass());
	
	private ClmsMappingDao clmsMappingDao;
	
	public void setClmsMappingDao(ClmsMappingDao clmsMappingDao){
		this.clmsMappingDao = clmsMappingDao;
	}
	
	/**
	 * 매핑처리 화면 조회
	 */
	public void mappingListForm() throws Exception {
		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		Map map = getMap(ds_condition, 0);
		
		List kappTotalRow = new ArrayList();
		List kappList = new ArrayList();
		List komcaList = new ArrayList();
		List icnTotalRow = new ArrayList();
		List icnList = new ArrayList();
		
		// KAPP 리스트		
		Map kappMap = map;
		kappMap.put("TREAT_YN", "N");
		
		kappTotalRow =  (List) clmsMappingDao.kappListTotalRow(kappMap);		// 총건수
		kappList = (List) clmsMappingDao.kappList(kappMap);		// TREAT_YN = 'N'
		
		// ICN 맵핑결과
		Map icnMap = map;
		icnMap.put("TREAT_YN", "Y");
		icnTotalRow = (List) clmsMappingDao.icnListTotalRow(icnMap);
		icnList = (List) clmsMappingDao.icnList(icnMap);		// TREAT_YN = 'Y'
		
		//hmap를 dataset로 변환한다.
		addList("ds_page", kappTotalRow);
		addList("ds_page2", icnTotalRow);
		addList("ds_unident_kapp_data", kappList);
		addList("ds_icn_mapping", icnList);
		addList("ds_music_works", komcaList);
	}
	
	
	/** 
	 * kapp 리스트 조회
	 */
	public void kappList() throws Exception {
		Dataset ds_condition = getDataset("ds_condition"); 
		Map map = getMap(ds_condition, 0);
		List kappTotalRow = new ArrayList();
		List kappList = new ArrayList();
		
//		 KAPP 리스트		
		Map kappMap = map;
		kappMap.put("TREAT_YN", "N");
		
		kappTotalRow =  (List) clmsMappingDao.kappListTotalRow(kappMap);		// 총건수
		kappList = (List) clmsMappingDao.kappList(kappMap);		// TREAT_YN = 'N'
		
		addList("ds_page", kappTotalRow);
		addList("ds_unident_kapp_data", kappList);
	}
	
	
	/**
	 * komca 리스트 조회
	 */
	public void komcaList() throws Exception {
		Dataset ds_condition = getDataset("ds_condition"); 
		Map map = getMap(ds_condition, 0);
		
		List komcaList = new ArrayList();
		
		String schSinger = (String)map.get("SCH_SINGER");
		String schSongTitle = (String)map.get("SCH_SONGTITLE");
		String gubun = (String)map.get("GUBUN");
		
		if( schSinger == null ) {
			schSinger = "";
		}
		if( schSongTitle == null ) {
			schSongTitle = "";
		}
		
		if( gubun == null){
			gubun = "";
		}
		
		if(  schSinger.length()>0 || schSongTitle.length()>0 ) {
			komcaList = (List) clmsMappingDao.komcaList(map);		// TREAT_YN = 'N'
		}
		
		if( gubun.equals("LIST")) {		// KAPP리스트에서 더블클릭하여 검색된 경우 가수와 곡명 동시 검색일때 데이터가 없으면 곡명으로 재검색
			if( schSinger.length()>0 && schSongTitle.length()>0) {	// 가수 & 곡명 검색
				if( komcaList.size() == 0 ) {	// 가수&곡명으로 검색했을때 데이터가 없을경우
					map.put("SCH_SINGER", "");
					komcaList = (List) clmsMappingDao.komcaList(map);		// TREAT_YN = 'N'
				}
			}
		}
		
		addList("ds_condition",map);
		addList("ds_music_works", komcaList);
	}
	
	
	/**
	 * ICN 맵핑처리
	 * @throws Exception
	 */
	public void icnUpdate() throws Exception {
		Dataset ds_condition = getDataset("ds_condition"); 
		Map map = getMap(ds_condition, 0);
		
		clmsMappingDao.icnUpdate(map);
	}
	
	
	/**
	 * ICN 맵핑결과 조회
	 * @throws Exception
	 */
	public void icnList() throws Exception {
		Dataset ds_condition = getDataset("ds_condition"); 
		
		ds_condition.printDataset();
		Map map = getMap(ds_condition, 0);
		
		List icnList = new ArrayList();
		List icnTotalRow = new ArrayList();
		String treatYn = (String)map.get("TREAT_YN");

		if( treatYn != null ){
			
//			 ICN 맵핑결과
			Map icnMap = map;
			icnMap.put("TREAT_YN", treatYn);
			
			icnTotalRow =  (List) clmsMappingDao.icnListTotalRow(icnMap);		// 총건수
			icnList = (List) clmsMappingDao.icnList(icnMap);		// TREAT_YN = 'Y'
		}
		
		addList("ds_page2", icnTotalRow);
		addList("ds_icn_mapping", icnList);
	}
	
	
	/** 
	 * ICN 결과에서 선택된 항목 삭제(반려)
	 * @throws Exception
	 */
	public void icnDelete() throws Exception {
		Dataset ds_condition = getDataset("ds_condition");
		
		ds_condition.printDataset();
		Map map = getMap(ds_condition, 0);
		
		clmsMappingDao.icnDelete(map);	
		
	}
}
