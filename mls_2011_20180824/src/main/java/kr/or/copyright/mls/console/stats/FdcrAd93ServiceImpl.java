package kr.or.copyright.mls.console.stats;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.stats.inter.FdcrAd87Dao;
import kr.or.copyright.mls.console.stats.inter.FdcrAd93Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd93Service" )
public class FdcrAd93ServiceImpl extends CommandService implements FdcrAd93Service{

	// old AdminConnDao
	@Resource( name = "fdcrAd87Dao" )
	private FdcrAd87Dao fdcrAd87Dao;

	/**
	 * 저작권찾기신청 및 처리통계(임시)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd93List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd87Dao.rsltList( commandMap );

		commandMap.put( "ds_list", list ); // 로그인 사용자 정보
	}
}
