package kr.or.copyright.mls.console.rcept;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 관리저작물 접수 및 처리 > 미분배 보상금 대상 저작물 > 도서관 > 목록
 * 
 * @author ljh
 */
@Controller
public class FdcrAd23Controller extends DefaultController{

	@Resource( name = "fdcrAd23Service" )
	private FdcrAd23ServiceImpl fdcrAd23Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd23Controller.class );

	/**
	 * 미분배보상금 도서관 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd23List1.page" )
	public String fdcrAd23List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		String GUBUN = EgovWebUtil.getString( commandMap, "GUBUN" );
		String TITLE = EgovWebUtil.getString( commandMap, "TITLE" );
		
		//TODO 기관코드 주석
		commandMap.put( "TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode() );
		if(!ConsoleLoginUser.getTrstOrgnCode().equals( "200" ) && !ConsoleLoginUser.getTrstOrgnCode().equals( "205" )){
			System.out.println("ConsoleLoginUser.getTrstOrgnCode()::::::::::::::::"+ConsoleLoginUser.getTrstOrgnCode());
			return returnUrl(model, "해당단체만 접근 가능합니다.", "/console/main/main.page?menuId=126");
		}
		
		fdcrAd23Service.fdcrAd23List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_List" ) );
		model.addAttribute( "ds_count", commandMap.get( "totCnt" ) );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		System.out.println( "미분배보상금 도서관 목록 조회" );
		return "rcept/fdcrAd23List1.tiles";
	}

	/**
	 * 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	// 화면단에서 구성
}
