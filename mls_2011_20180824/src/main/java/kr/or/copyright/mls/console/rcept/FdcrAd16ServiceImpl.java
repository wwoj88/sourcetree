package kr.or.copyright.mls.console.rcept;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd15Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd16Service;

import org.apache.poi.hssf.util.HSSFColor.BLACK;
import org.springframework.stereotype.Service;

@Service( "fdcrAd16Service" )
public class FdcrAd16ServiceImpl extends CommandService implements FdcrAd16Service{

	@Resource( name = "fdcrAd15Dao" )
	private FdcrAd15Dao fdcrAd15Dao;

	/**
	 * 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd16List1( Map<String, Object> commandMap ) throws Exception{
		List pageList = (List) fdcrAd15Dao.totalRowWorksMgntList( commandMap );
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		List list = (List) fdcrAd15Dao.worksMgntList( commandMap );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_count", totCnt );
		//commandMap.put( "ds_page", pageList );
	}
	
	/**
	 * 보고년월 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public List getYyyyMm(Map<String, Object> commandMap) throws Exception{
		return fdcrAd15Dao.getYyyyMm(commandMap);
	}

	/**
	 * 엑셀다운로드
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd16Down1( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd15Dao.worksMgntNewExcel( commandMap );
		commandMap.put( "ds_list", list );
	}
	
	
	/**
	 * 위탁저작물 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd16Delete( String[] deleteList, Map<String, Object> commandMap ) throws Exception{
		
		String[] tableNameList = {"ML_COMM_MUSIC","ML_COMM_BOOK","ML_COMM_SCRIPT","ML_COMM_MOVIE","ML_COMM_BROADCAST","ML_COMM_NEWS","ML_COMM_ART","ML_COMM_IMAGE","ML_COMM_SIDE"};
		String[] backupTableNames = {"ML_COMM_MUSIC_DELETE_HISTORY","ML_COMM_BOOK_DELETE_HISTORY","ML_COMM_SCRIPT_DELETE_HISTORY","ML_COMM_MOVIE_DELETE_HISTORY"
				,"ML_COMM_BROADCAST_DEL_HIS","ML_COMM_NEWS_DELETE_HISTORY","ML_COMM_ART_DELETE_HISTORY","ML_COMM_IMAGE_DELETE_HISTORY","ML_COMM_SIDE_DELETE_HISTORY"};
		List<List> reportCountList = new ArrayList<List>();
		Map<String, Object> consoleUser = (Map<String, Object>) commandMap.get( "CONSOLE_USER" );

		//RGST_DTTM , TRST_ORGN_CODE
		
		for(int i = 0 ; i < deleteList.length ; i ++ ) {
			
			List<Map<String, Object>> resultList = fdcrAd15Dao.fdcrAd16SelectWork( deleteList[i] );
			
			int GENRE_CD = Integer.parseInt( resultList.get( 0 ).get( "GENRE_CD" ).toString() );
			System.out.println( "GENRE_CD : " + GENRE_CD );
			if(GENRE_CD==99)
			{
				GENRE_CD=9;
			}
			String tableName = tableNameList[GENRE_CD-1];
			String backupTableName = backupTableNames[GENRE_CD-1];
			
			Map model = new HashMap<String, String>();
			model.put( "tableName", tableName );
			model.put( "backupTableName", backupTableName );
			model.put( "works_id", deleteList[i] );
			model.put( "rgst_dttm",  resultList.get( 0 ).get( "RGST_DTTM" ));
			model.put( "trst_orgn_code",  resultList.get( 0 ).get( "TRST_ORGN_CODE" ));
			model.put( "deleteUserTrstOrgnCode", consoleUser.get( "TRST_ORGN_CODE" ) );
			model.put( "deleteUserIdnt", consoleUser.get( "USER_ID" ) );
			model.put( "rgstOrgnName", resultList.get( 0 ).get( "RGST_ORGN_NAME" ) );
			
			reportCountList.add( resultList );
			
			fdcrAd15Dao.fdcrAd16AddHistory( model );
			fdcrAd15Dao.fdcrAd16AddDeleteUserIdHistory( model );
			fdcrAd15Dao.fdcrAd16Delete( model );
			
		}
		
		List<List> deleteCountList = new ArrayList<List>();
		
		for(int i = 0 ; i < reportCountList.size() ; i ++) {
			Map resultMap = (Map) reportCountList.get( i ).get( 0 );
			List deleteCountList2 = new ArrayList();
			if(deleteCountList.size() == 0 ) {
				deleteCountList2.add( (Map) reportCountList.get( i ).get( 0 ) );
				deleteCountList.add( deleteCountList2 );
			}else if(deleteCountList.size() != 0){
				
				int deleteCountListSize = deleteCountList.size();
				for(int j = 0 ; j < deleteCountListSize ; j ++ ) {
					Map countMap = (Map)deleteCountList.get( j ).get( 0 );	
				
					if(countMap.get( "GENRE_CD" ).equals( resultMap.get( "GENRE_CD" ) )
						&& countMap.get( "RGST_IDNT" ).equals( resultMap.get( "RGST_IDNT" )) 
						&& countMap.get( "REPT_YMD" ).equals( resultMap.get( "REPT_YMD" ) ) ) {

						List countList = (List) deleteCountList.get( j );
						countList.add( resultMap );
					}else if(!(countMap.get( "GENRE_CD" ).equals( resultMap.get( "GENRE_CD" ) )
						&& countMap.get( "RGST_IDNT" ).equals( resultMap.get( "RGST_IDNT" )) 
						&& countMap.get( "REPT_YMD" ).equals( resultMap.get( "REPT_YMD" ) ))){
						
						if((deleteCountListSize-1) ==j) {
							deleteCountList2.add( resultMap );
							deleteCountList.add( deleteCountList2 );
						}
						
					}
					
				}
			}
			
		}
		
		for(int i = 0 ; i < deleteCountList.size() ; i++) {
			
			Map reportSelectMap= (Map) deleteCountList.get( i ).get(0);
			
			List<Map> worksReportList = fdcrAd15Dao.selectWorksReport(reportSelectMap) ;
			if(
				reportSelectMap.get( "GENRE_CD" ).equals( worksReportList.get(0).get( "GENRE_CD" ) )
				&&reportSelectMap.get( "TRST_ORGN_CODE" ).equals( worksReportList.get(0).get( "TRST_ORGN_CODE" ) )
				&&reportSelectMap.get( "REPT_YMD" ).equals( worksReportList.get(0).get( "REPT_YMD" ) )
				) {
				worksReportList.get( 0 ).put( "REPT_WORKS_CONT", deleteCountList.get( i ).size()*-1 );
				worksReportList.get( 0 ).put( "RGST_IDNT", consoleUser.get( "USER_ID" ) );
				
				int REPT_SEQN = Integer.parseInt( worksReportList.get( 0 ).get( "REPT_SEQN").toString() )+1;
				worksReportList.get( 0 ).put( "REPT_SEQN", REPT_SEQN );
				
				fdcrAd15Dao.adminCommWorksReportInsertNew( worksReportList.get( 0 ) );
			}
		}
		
		
		
		
	}

	public void fdcrAd16ImageList( Map<String, Object> commandMap ) throws Exception{
		List pageList = (List) fdcrAd15Dao.totalRowImageCnt( commandMap );
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		//int pageCnt = Integer.parseInt( (String)commandMap.get( "pageIndex" ) ) * 15 ;
		pagination( commandMap, totCnt, 15);
		System.out.println( "commandMap.get(TO_NO) : " + commandMap.get("TO_NO"));
		System.out.println( "commandMap.get(TO_NO) : " + commandMap.get( "FROM_NO"));
		List list = (List)fdcrAd15Dao.fdcrAd16ImageList(commandMap);
		//List list = (List) fdcrAd15Dao.worksMgntList( commandMap );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_count", totCnt );
		
	}

	public void fdcrAd16ArtList( Map<String, Object> commandMap ) throws Exception{
		List pageList = (List) fdcrAd15Dao.totalRowArtCnt( commandMap );
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 15);
		List list = (List)fdcrAd15Dao.fdcrAd16ArtList(commandMap);
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_count", totCnt );
	}
	
	
}
