package kr.or.copyright.mls.common.common.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;


public class RequestWrapper extends HttpServletRequestWrapper 
{
	
	public RequestWrapper(HttpServletRequest request) {
		super(request);
	}
	
       
	public String[] getParameterValues(String parameter) {

		String[] values = super.getParameterValues(parameter);
		
		if(values==null){
			return null;			
		}
		
		for (int i = 0; i < values.length; i++) {			
			if (values[i] != null) {				
				/*StringBuffer strBuff = new StringBuffer();
				for (int j = 0; j < values[i].length(); j++) {
					char c = values[i].charAt(j);
					switch (c) {
					case '<':
						strBuff.append("&lt;");
						break;
					case '>':
						strBuff.append("&gt;");
						break;
					//case '&':
						//strBuff.append("&amp;");
						//break;
					case '"':
						strBuff.append("&quot;");
						break;
					case '\'':
						strBuff.append("&apos;");
						break;
					default:
						strBuff.append(c);
						break;
					}
				}				*/
				
				String ret = values[i];
				
				ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
		        ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
		         	        
		        ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
		        ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
		        	        
		        ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
		        ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
		         	        
		        ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "");
		        ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "");
		        	        
		        ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "");
		        ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "");
		        
		        ret = ret.replaceAll("\"", "&quot;");
		        ret = ret.replaceAll("\'", "&#39;");
		        
		        ret = ret.replaceAll("<", "&lt;");
			    ret = ret.replaceAll(">", "&gt;");
			    ret = ret.replaceAll("&", "");
				values[i] = ret;
			} else {
				values[i] = null;
			}
		}
		
		return values;
	}

	public String getParameter(String parameter) {
		
		String value = super.getParameter(parameter);
		
		if(value==null){
			return null;
		}
		
/*		StringBuffer strBuff = new StringBuffer();

		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			switch (c) {
			case '<':
				strBuff.append("&lt;");
				break;
			case '>':
				strBuff.append("&gt;");
				break;
			//case '&':
				//strBuff.append("&amp;");
				//break;
			case '"':
				strBuff.append("&quot;");
				break;
			case '\'':
				strBuff.append("&apos;");
				break;	
			default:
				strBuff.append(c);
				break;
			}
		}*/
		
		String ret = value;

		ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
    ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
       
    ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
    ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
        	        
    ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
    ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
         	        
    ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "");
    ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "");
        	        
    ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "");
    ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "");
        
    ret = ret.replaceAll("\"", "&quot;");
    ret = ret.replaceAll("\'", "&#39;");
        
    ret = ret.replaceAll("<", "&lt;");
	  ret = ret.replaceAll(">", "&gt;");
	  ret = ret.replaceAll("../", "");
	 
	 
	   
	   
        
		value = ret;
	 
		return value;
	}
  
}