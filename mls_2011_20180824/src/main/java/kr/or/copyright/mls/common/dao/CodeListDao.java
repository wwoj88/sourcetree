package kr.or.copyright.mls.common.dao;

import java.util.List;

import kr.or.copyright.mls.common.model.CodeList;

public interface CodeListDao {

	List<CodeList> commonCodeList(CodeList codeList);
	
}
