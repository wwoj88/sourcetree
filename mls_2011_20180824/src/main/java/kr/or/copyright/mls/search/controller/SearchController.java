package kr.or.copyright.mls.search.controller;

import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.servlet.ModelAndView;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.search.service.WNSearch;
import kr.or.copyright.mls.srch.service.SrchService;
import kr.or.copyright.mls.support.util.SessionUtil;
import static kr.or.copyright.mls.search.service.WNConstants.*;
import static kr.or.copyright.mls.search.service.WNUtils.getCheckReqXSS;
import static kr.or.copyright.mls.search.service.WNUtils.parseInt;
import static kr.or.copyright.mls.search.service.WNUtils.getCurrentDate;


@Controller
public class SearchController {

     private Logger log = Logger.getLogger(this.getClass());

     @javax.annotation.Resource(name = "SrchService")
     private SrchService SrchService;

     @RequestMapping(value = "/search/search.do")
     public ModelAndView search(HttpServletRequest request) throws Exception {

          ModelAndView mav = new ModelAndView();

          WNSearch wnsearch = null;
          // 실시간 검색어 화면 출력 여부 체크
          boolean isRealTimeKeyword = false;
          // 오타 후 추천 검색어 화면 출력 여부 체크
          boolean useSuggestedQuery = true;
          String suggestQuery = "";

          // 인기 검색어 화면 출력 여부 체크
          boolean isPopKeyword = true;

          // 디버깅 보기 설정
          boolean isDebug = true;

          int TOTALVIEWCOUNT = 3; // 통합검색시 출력건수
          int COLLECTIONVIEWCOUNT = 160; // 더보기시 출력건수

          String START_DATE = "1970.01.01"; // 기본 시작일

          // 날짜얻어오기
          Calendar nowDate = Calendar.getInstance();
          int nowYear = nowDate.get(Calendar.YEAR);
          int nowMonth = nowDate.get(Calendar.MONTH) + 1;
          int nowDay = nowDate.get(Calendar.DAY_OF_MONTH);
          // 결과 시작 넘버
          int startCount = parseInt(getCheckReqXSS(request, "startCount", "0"), 0);// 시작 번호
          // int listCount = parseInt(getCheckReqXSS(request, "listCount", "10"), 10);//old
          String query = getCheckReqXSS(request, "query", ""); // 검색어
          String collection = getCheckReqXSS(request, "collection", "ALL"); // 컬렉션이름

          String rt = getCheckReqXSS(request, "rt", ""); // 결과내 재검색 체크필드
          String rt2 = getCheckReqXSS(request, "rt2", ""); // 결과내 재검색 체크필드
          String requery = getCheckReqXSS(request, "reQuery", ""); // 결과내 검색어
          String realQuery = getCheckReqXSS(request, "realQuery", ""); // 결과내 검색어

          // String mode = getCheckReqXSS(request, "mode", "basic"); //통합검색(basic),상세검색(detail)
          String sort = getCheckReqXSS(request, "sort", "RANK"); // 정렬필드
          String sortOrder = getCheckReqXSS(request, "sortOrder", "DESC"); // 정렬필드

          String range = getCheckReqXSS(request, "range", "0"); // 기간관련필드
          String startDate = getCheckReqXSS(request, "startDate", ""); // 시작날짜
          String endDate = getCheckReqXSS(request, "endDate", getCurrentDate());
          String writer = getCheckReqXSS(request, "writer", ""); // 작성자
          String searchField = getCheckReqXSS(request, "searchField", ""); // 검색필드

          String strOperation = ""; // operation 조건 필드
          String exquery = ""; // exquery 조건 필드
          int totalCount = 0;

          String[] searchFields = null;

          /// ==================================================================================//
          String sfield = "";
          String searchResult = "";
          String genreCd = getCheckReqXSS(request, "genreCd", "total"); // 페이지 구분자
          String menuFlag = getCheckReqXSS(request, "menuFlag", ""); // 페이지 구분자
          String collectionName = collection;

          System.out.println("==================================");
          System.out.println("startCount : " + startCount);
          //System.out.println( "listCount : " + listCount );
          System.out.println("query : " + query);
          System.out.println("collection : " + collection);
          System.out.println("sort : " + sort);
          System.out.println("sortOrder : " + sortOrder);
          System.out.println("genreCd : " + genreCd);
          System.out.println("menuFlag : " + menuFlag);
          System.out.println("==================================");


          try {

               // 상세검색 검색 필드 설정이 되었을때
               /*
                * String [] field = request.getParameterValues("sfield"); if ( field != null ) { for ( int x=0;
                * x<field.length;x++) { sfield = sfield + field[x] + ","; } } else sfield = "ALL";
                */

               if (!searchField.equals("")) {
                    // 작성자
                    if (!writer.equals("")) {
                         exquery = "<WRITER:" + writer + ">";
                    }
               } else {
                    searchField = "ALL";
               }

               /*
                * if ( range.equals("0") ) { startDate = ""; endDate = ""; }
                */

               String[] collections = null;
               if (collection.equals("ALL")) { // 통합검색인 경우
                    collections = COLLECTIONS;
               } else if (collection.contains(",")) { // 다중 collection 선택인 경우
                    collections = collection.split(",");
               } else { // 개별검색인 경우
                    collections = new String[] {collection};
               }

               // String search = query ;
               /*
                * String strOperation = "" ; //operation 조건 필드 String exquery = "" ; //exquery 조건 필드 int totalCount
                * = 0;
                */

               /*
                * if ( rt.equals("1") && !requery.equals("") ) { search = query + " " + requery; } else if (
                * rt2.equals("1") && !requery.equals("") ) { search = requery ; }
                */
               if (requery.equals("1")) {
                    realQuery = query + " " + realQuery;
               } else if (!requery.equals("2")) {
                    realQuery = query;
               }

               // String[] searchFields = null;
               /*
                * if ( !writer.equals("") ) { exquery = "<WRITER:" + writer + ">"; }
                */
               
               wnsearch = new WNSearch(isDebug, false, collections, searchFields, USE_RESULT_JSON);
               
               // int viewResultCount = listCount;
               int viewResultCount = COLLECTIONVIEWCOUNT;
               if (collection.equals("ALL") || collection.equals(""))
                    viewResultCount = TOTALVIEWCOUNT;
                   
               for (int i = 0; i < collections.length; i++) {

                    // 출력건수
                    wnsearch.setCollectionInfoValue(collections[i], PAGE_INFO, startCount + "," + viewResultCount);

                    // 검색어가 없으면 DATE_RANGE 로 전체 데이터 출력
                    if (!query.equals("")) {
                         wnsearch.setCollectionInfoValue(collections[i], SORT_FIELD, sort + "/" + sortOrder);
                    } else {
                         wnsearch.setCollectionInfoValue(collections[i], DATE_RANGE, "1970/01/01,2030/12/31,-");
                         wnsearch.setCollectionInfoValue(collections[i], SORT_FIELD, sort + "/" + sortOrder);
                    }
                    /*
                     * //sfield 값이 있으면 설정, 없으면 기본검색필드 if ( !sfield.equals("") && sfield.indexOf("ALL") == -1 )
                     * wnsearch.setCollectionInfoValue(collections[i], SEARCH_FIELD, sfield );
                     */

                    // searchField 값이 있으면 설정, 없으면 기본검색필드
                    if (!searchField.equals("") && !searchField.equals("WRITER") && searchField.indexOf("ALL") == -1) {
                         wnsearch.setCollectionInfoValue(collections[i], SEARCH_FIELD, searchField);
                    }


                    // operation 설정
                    if (!strOperation.equals(""))
                         wnsearch.setCollectionInfoValue(collections[i], FILTER_OPERATION, strOperation);


                    // exquery 설정
                    if (!exquery.equals(""))
                         wnsearch.setCollectionInfoValue(collections[i], EXQUERY_FIELD, exquery);


                    /*
                     * //기간 설정 , 날짜가 모두 있을때 if ( !startDate.equals("") && !endDate.equals("") )
                     * wnsearch.setCollectionInfoValue(collections[i], DATE_RANGE, startDate.replaceAll("-","/") + "," +
                     * endDate.replaceAll("-","/") + ",-"); //기간 설정 , 시작날짜만 있을때에는 뒤에 날짜를 오늘날짜로 셋팅한다. else if (
                     * !startDate.equals("") && endDate.equals("") ) wnsearch.setCollectionInfoValue(collections[i],
                     * DATE_RANGE, startDate.replaceAll("-","/") + "," + nowYear + "/" + nowMonth + "/" + nowDay +
                     * ",-");
                     */
                    // 기간 설정 , 날짜가 모두 있을때
                    if (!startDate.equals("") && !endDate.equals("")) {
                         wnsearch.setCollectionInfoValue(collections[i], DATE_RANGE, startDate.replaceAll("[.]", "/") + "," + endDate.replaceAll("[.]", "/") + ",-");
                    }
               }

               // wnsearch.search(search, false);
               // System.out.println( "realQuery : " + realQuery );
        
               wnsearch.search(realQuery, isRealTimeKeyword, CONNECTION_CLOSE, useSuggestedQuery);
               searchResult = wnsearch.getResultJson();

               WNSearch wnsearch2 = new WNSearch(isDebug, false, collections, searchFields, USE_RESULT_STRING);

               for (int i = 0; i < COLLECTIONS.length; i++) {

                    // 출력건수
                    wnsearch2.setCollectionInfoValue(COLLECTIONS[i], PAGE_INFO, startCount + "," + viewResultCount);

                    // 검색어가 없으면 DATE_RANGE 로 전체 데이터 출력
                    if (!query.equals("")) {
                         wnsearch2.setCollectionInfoValue(COLLECTIONS[i], SORT_FIELD, sort + "/" + sortOrder);
                    } else {
                         wnsearch2.setCollectionInfoValue(COLLECTIONS[i], DATE_RANGE, "1970/01/01,2030/12/31,-");
                         wnsearch2.setCollectionInfoValue(COLLECTIONS[i], SORT_FIELD, sort + "/" + sortOrder);
                    }

                    // searchField 값이 있으면 설정, 없으면 기본검색필드
                    if (!searchField.equals("") && !searchField.equals("WRITER") && searchField.indexOf("ALL") == -1) {
                         wnsearch2.setCollectionInfoValue(COLLECTIONS[i], SEARCH_FIELD, searchField);
                    }

                    // operation 설정
                    if (!strOperation.equals("")) {
                         wnsearch2.setCollectionInfoValue(COLLECTIONS[i], FILTER_OPERATION, strOperation);
                    }

                    // exquery 설정
                    if (!exquery.equals("")) {
                         wnsearch2.setCollectionInfoValue(COLLECTIONS[i], EXQUERY_FIELD, exquery);
                    }

                    // 기간 설정 , 날짜가 모두 있을때
                    if (!startDate.equals("") && !endDate.equals("")) {
                         wnsearch2.setCollectionInfoValue(COLLECTIONS[i], DATE_RANGE, startDate.replaceAll("[.]", "/") + "," + endDate.replaceAll("[.]", "/") + ",-");
                    }
               } ;

               wnsearch2.search(realQuery, isRealTimeKeyword, CONNECTION_CLOSE, useSuggestedQuery);

               String debugMsg = wnsearch.printDebug() != null ? wnsearch.printDebug().trim() : "";
               if (!debugMsg.trim().equals("")) {
                    //System.out.println(debugMsg);
               }


               String thisCollection = "";
               if (useSuggestedQuery) {
                    suggestQuery = wnsearch.suggestedQuery;
               }

               // json
               JSONArray resultjson = new JSONArray();
               JSONObject SearchQueryCount = new JSONObject();
               JSONObject colcount = new JSONObject();

               for (int i = 0; i < COLLECTIONS.length; i++) {
                    totalCount += wnsearch2.getResultTotalCount(COLLECTIONS[i]);
                    colcount.put(COLLECTIONS[i], wnsearch2.getResultTotalCount(COLLECTIONS[i]));
               }
            
               JSONParser parser = new JSONParser();
               System.out.println(wnsearch.getResultJson());
               Object obj = parser.parse(wnsearch.getResultJson());
               JSONObject jsonObj = (JSONObject) obj;
               jsonObj.put("SearchColCount", colcount);
               
               //System.out.println("jsonObj : " + jsonObj.toString().replace("<!HS>", ""));

               mav.addObject("SearchColCount", jsonObj);


          } catch (Exception e) {
          } finally {
               if (wnsearch != null)
                    wnsearch.closeServer();
          }
          if (null != query && !"".equals(query.trim())) {
               SrchService.insertSrch(query);
          }
          User user = SessionUtil.getSession(request);
          // String sessUserIdnt = user.getUserIdnt();

          mav.addObject("user", user);
          mav.addObject("query", query);
          mav.addObject("genreCd", genreCd);
          mav.addObject("menuFlag", menuFlag);
          mav.addObject("collectionName", collectionName);

          String subPageStr = getCheckReqXSS(request, "subPageStr", ""); // 결과내 재검색 체크필드
    
    
          mav.addObject("startCount", startCount/10 );
          
          System.out.println( "subPageStr : " + subPageStr );
          System.out.println( "collection : " +collection);
          System.out.println( "collectionName : " +collectionName);
          System.out.println( "startCount : " +startCount);
          System.out.println( "startCount/10 : " +startCount/10);
          
          if (subPageStr.equals("") || subPageStr.equals(null)) {
               mav.setViewName("search/search");
          } else if (subPageStr.equals("dg")) {
               mav.setViewName("search/search_dg");
          }else if (subPageStr.equals("book")) {
               mav.setViewName("search/search_book");
          }else if (collection.equals("reg_copyright")) {
               mav.setViewName("search/search_reg");
          } else {
               mav.setViewName("search/search_sub");
          }
          System.out.println(searchResult.toString());

          mav.addObject("searchResult", searchResult.replace("<!HS>", "").replace("<!HE>", ""));
          return mav;
     }

}
