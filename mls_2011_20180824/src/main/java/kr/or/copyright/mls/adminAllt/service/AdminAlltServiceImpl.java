package kr.or.copyright.mls.adminAllt.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminAllt.dao.AdminAlltDao;
import kr.or.copyright.mls.adminIcn.dao.AdminIcnDao;
import kr.or.copyright.mls.common.service.BaseService;

import com.tobesoft.platform.data.Dataset;

public class AdminAlltServiceImpl extends BaseService implements AdminAlltService {

	//private Log logger = LogFactory.getLog(getClass());

	private AdminAlltDao adminAlltDao;
	private AdminIcnDao adminIcnDao; 
	
	public void setAdminAlltDao(AdminAlltDao adminAlltDao){
		this.adminAlltDao = adminAlltDao;
	}
	
	public void setAdminIcnDao(AdminIcnDao adminIcnDao) {
		this.adminIcnDao = adminIcnDao;
	}
	
	public void alltInmtDetl() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminAlltDao.alltInmtDetl(map);
		
		// club컬럼을 String로 변환한다.
		// setClub( list, "BORD_DESC" );
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);

	}
	
	public void alltInmtSave() throws Exception {

		Dataset ds_List = getDataset("ds_list");   
		Dataset ds_Condition = getDataset("ds_condition");

		// UPDATE처리
		Map map = getMap(ds_List, 0 );
		Map conditionMap = getMap(ds_Condition, 0);
		
		List cnt = adminAlltDao.alltInmtCount(conditionMap);
		
		for( int i=0;i<ds_List.getRowCount();i++ ) {
			
			Map trstGroupMap = getMap(ds_List, i);
			
			trstGroupMap.put("INMT_DIVS", conditionMap.get("INMT_DIVS"));	// 보상금 종류
			trstGroupMap.put("ORGN_CODE", conditionMap.get("ORGN_CODE"));	// 기관코드	
			
			if(cnt.size() == 0){
				adminAlltDao.alltInmtSave(trstGroupMap); 
			}else{
				adminAlltDao.alltInmtUpdate(trstGroupMap); 
			}
		}
		
		// 상태처리 후 상태값 구한다.
		Map conMap = getMap(ds_Condition, 0);
		List list = (List) adminAlltDao.alltInmtDetl(map);

		addList("ds_List", list);
	}
	
	public void alltInmtList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminAlltDao.alltInmtList(map);
		
		// club컬럼을 String로 변환한다.
		// setClub( list, "BORD_DESC" );
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);

	}
	
	/**
	 * @throws Exception
	 * @Date 2012.11.20
	 * @author 정병호
	 */
	public void alltInmtMgntList() throws Exception{
	    Dataset ds_condition = getDataset("ds_condition");
	    
	    Map map = getMap(ds_condition, 0);
	   
	   String curYear = (String) map.get("END_YEAR");
	   String maxYear = adminAlltDao.alltInmtMgntMaxYear();
	   
	   if(!curYear.equals(maxYear)){
	       for( int i=0;i<2;i++ ) {
		   HashMap insertMap = new HashMap();
		   insertMap.put("INMT_YEAR", curYear);
		   insertMap.put("HALF", (i+1)+"");
		   for(int j=0; j<8; j++){
		       	    if(j==0){
		       		insertMap.put("INMT_DIVS", "1");
		       		insertMap.put("INMT_ITEMS", "1");
				adminAlltDao.alltInmtMgntInsert(insertMap);
			    }else if(j==1){
				insertMap.put("INMT_DIVS", "1");
				insertMap.put("INMT_ITEMS", "2");
				adminAlltDao.alltInmtMgntInsert(insertMap);
			    }else if(j==2){
				insertMap.put("INMT_DIVS", "1");
				insertMap.put("INMT_ITEMS", "3");
				adminAlltDao.alltInmtMgntInsert(insertMap);
			    }else if(j==3){
				insertMap.put("INMT_DIVS", "2");
				insertMap.put("INMT_ITEMS", "1");
				adminAlltDao.alltInmtMgntInsert(insertMap);
			    }else if(j==4){
				insertMap.put("INMT_DIVS", "2");
				insertMap.put("INMT_ITEMS", "2");
				adminAlltDao.alltInmtMgntInsert(insertMap);
			    }else if(j==5){
				insertMap.put("INMT_DIVS", "2");
				insertMap.put("INMT_ITEMS", "3");
				adminAlltDao.alltInmtMgntInsert(insertMap);
			    }else if(j==6){
				insertMap.put("INMT_DIVS", "3");
				insertMap.put("INMT_ITEMS", "4");
				adminAlltDao.alltInmtMgntInsert(insertMap);
			    }else if(j==7){
				insertMap.put("INMT_DIVS", "3");
				insertMap.put("INMT_ITEMS", "5");
				adminAlltDao.alltInmtMgntInsert(insertMap);
			    }
		   }
	       }
	   }
	    
	    
	    List list = (List) adminAlltDao.alltInmtMgntList(map);
	    
	    addList("ds_List", list);
	    
	}
	
	public void alltInmtYsNoUpdate() throws Exception {

		Dataset ds_List = getDataset("ds_list");   
		Dataset ds_Condition = getDataset("ds_condition");

		// UPDATE처리
		Map map = getMap(ds_List, 0 );
		Map conditionMap = getMap(ds_Condition, 0);
		
		List cnt = adminAlltDao.alltInmtCount(conditionMap);
		
		for( int i=0;i<ds_List.getRowCount();i++ ) {
			
			Map trstGroupMap = getMap(ds_List, i);
			
			if(trstGroupMap.get("OPEN_YSNO").equals("0")){
				trstGroupMap.put("ORGN_CODE", "N");
			}else if(trstGroupMap.get("OPEN_YSNO").equals("1")){
				trstGroupMap.put("ORGN_CODE", "Y");
			}
			
			String OPEN_YSNO = trstGroupMap.get("OPEN_YSNO").toString();
			
			if(OPEN_YSNO.equals("1.0")){
				
				adminAlltDao.alltInmtAllUpdate(trstGroupMap); 
				adminAlltDao.alltInmtYsNoUpdate(trstGroupMap); 
			}	
		}
		
		// 상태처리 후 상태값 구한다.
		Map conMap = getMap(ds_Condition, 0);
		List list = (List) adminAlltDao.alltInmtList(map);

		addList("ds_List", list);
	}
	
	
	public void alltInmtMgntUpdate() throws Exception{
	    
	    Dataset ds_List = getDataset("ds_list");   
	    Dataset ds_Condition = getDataset("ds_condition");
	    
	    Map conditionMap = getMap(ds_Condition, 0);
	    
	    
	    
	    for( int i=0;i<ds_List.getRowCount();i++ ) {
		Map listMap = getMap(ds_List, i );
		HashMap updateMap = new HashMap();
		updateMap.put("INMT_YEAR", listMap.get("INMT_YEAR"));
		updateMap.put("HALF", listMap.get("HALF"));
		
		Double test = (Double) listMap.get("OPEN_YSNO");
		
		String ysno = Double.toString(test);
		
		if(ysno.equals("1.0")){
		    updateMap.put("OPEN_YSNO", "Y");
		}else{
		    updateMap.put("OPEN_YSNO", "N");
		}
		
		for(int j=0; j<8; j++){
		    if(j==0){
			updateMap.put("INMT_DIVS", "1");
			updateMap.put("INMT_ITEMS", "1");
			updateMap.put("ALLT_INMT", listMap.get("A1"));
			adminAlltDao.alltInmtMgntUpdate(updateMap);
		    }else if(j==1){
			updateMap.put("INMT_DIVS", "1");
			updateMap.put("INMT_ITEMS", "2");
			updateMap.put("ALLT_INMT", listMap.get("A2"));
			adminAlltDao.alltInmtMgntUpdate(updateMap);
		    }else if(j==2){
			updateMap.put("INMT_DIVS", "1");
			updateMap.put("INMT_ITEMS", "3");
			updateMap.put("ALLT_INMT", listMap.get("A3"));
			adminAlltDao.alltInmtMgntUpdate(updateMap);
		    }else if(j==3){
			updateMap.put("INMT_DIVS", "2");
			updateMap.put("INMT_ITEMS", "1");
			updateMap.put("ALLT_INMT", listMap.get("B1"));
			adminAlltDao.alltInmtMgntUpdate(updateMap);
		    }else if(j==4){
			updateMap.put("INMT_DIVS", "2");
			updateMap.put("INMT_ITEMS", "2");
			updateMap.put("ALLT_INMT", listMap.get("B2"));
			adminAlltDao.alltInmtMgntUpdate(updateMap);
		    }else if(j==5){
			updateMap.put("INMT_DIVS", "2");
			updateMap.put("INMT_ITEMS", "3");
			updateMap.put("ALLT_INMT", listMap.get("B3"));
			adminAlltDao.alltInmtMgntUpdate(updateMap);
		    }else if(j==6){
			updateMap.put("INMT_DIVS", "3");
			updateMap.put("INMT_ITEMS", "4");
			updateMap.put("ALLT_INMT", listMap.get("C1"));
			adminAlltDao.alltInmtMgntUpdate(updateMap);
		    }else if(j==7){
			updateMap.put("INMT_DIVS", "3");
			updateMap.put("INMT_ITEMS", "5");
			updateMap.put("ALLT_INMT", listMap.get("C2"));
			adminAlltDao.alltInmtMgntUpdate(updateMap);
		    }
		}
	    }
	    
	    
	    
	    
	    
	    List list = (List) adminAlltDao.alltInmtMgntList(conditionMap);
	    
	    addList("ds_List", list);
	    
	}
	
	
	/////////////////////////////////////////////////////////////////////////////
	
//	 신청 음악저작물 목록 조회
	public void muscList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List) adminIcnDao.selectMuscList(map);
		
		addList("ds_list", list);
	}
}
