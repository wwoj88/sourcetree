package kr.or.copyright.mls.adminRght.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.common.info.ResponseInfo;
import kr.or.common.info.TaLicenseDetailInfo;
import kr.or.common.info.TaLicenseMasterInfo;
import kr.or.crt.CopyRightManageServiceProxy;

import kr.or.copyright.mls.adminRght.dao.AdminRghtDao;
import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.rght.dao.RghtDao;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;

import com.tobesoft.platform.data.Dataset;

public class AdminRghtServiceImpl extends BaseService implements AdminRghtService {

	//private Log logger = LogFactory.getLog(getClass());

	private AdminRghtDao adminRghtDao;
	private RghtDao rghtDao;
	
	public void setAdminRghtDao(AdminRghtDao adminRghtDao){
		this.adminRghtDao = adminRghtDao;
	}
	public void setRghtDao(RghtDao rghtDao){
		this.rghtDao = rghtDao; 
	}
	
	// adminRghtList 조회 
	public void rghtList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     

		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminRghtDao.rghtList(map); 
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);

	}
	
	
	// adminRghtDetlList 조회 
	public void rghtDetlList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     

//		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//처리상태 update
		//adminRghtDao.rghtDetlUpdate(map);

		//DAO호출
		List list = (List) adminRghtDao.rghtDetlList(map); 
		List fileList		 = (List)adminRghtDao.rghtFileList(map);	
		
		List workList = new ArrayList();
		List tempList = new ArrayList();
		
		if(map.get("PRPS_DIVS").equals("V")){ // 영화
			workList = (List) adminRghtDao.rghtMvieTempList(map);
			map.put("ListGubun", "Y");
			tempList = (List) adminRghtDao.rghtMvieTempList(map);
		}else if(map.get("PRPS_DIVS").equals("C")){ // 방송대본 
			workList = (List) adminRghtDao.rghtScriptTempList(map);
			map.put("ListGubun", "Y");
			tempList = (List) adminRghtDao.rghtScriptTempList(map);
		}else if(map.get("PRPS_DIVS").equals("R")){ // 방송 
			workList = (List) adminRghtDao.rghtBroadcastTempList(map);
			map.put("ListGubun", "Y");
			tempList = (List) adminRghtDao.rghtBroadcastTempList(map);
		}else if(map.get("PRPS_DIVS").equals("N")){ // 뉴스 
			workList = (List) adminRghtDao.rghtNewsTempList(map);
			map.put("ListGubun", "Y");
			tempList = (List) adminRghtDao.rghtNewsTempList(map);
		}else{
			workList	= (List)adminRghtDao.rghtTempList(map);				//  저작물리스트 저작물명
			map.put("ListGubun", "Y");
			tempList	= (List)adminRghtDao.rghtTempList(map);				//  저작물리스트 신탁기관별 저작물명
		}
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_fileList", fileList);
		addList("ds_works", workList);
		addList("ds_temp", tempList);

	}	
	
	// 신청저작물 상세조회
	public void selectWorksInfo() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);

		String prpsDivs = map.get("PRPS_DIVS").toString();
		List list = null;
		
		if(prpsDivs.equals("M")){ // 음악
			list = (List) adminRghtDao.rghtMuscDetail(map);
		}else if(prpsDivs.equals("O")){  // 어문
			list = (List) adminRghtDao.rghtBookDetail(map);
		}else if(prpsDivs.equals("C")){  // 방송대본
			list = (List) adminRghtDao.rghtScriptDetail(map);
		}else if(prpsDivs.equals("V")){  // 영화 
			list = (List) adminRghtDao.rghtMvieDetail(map);
		}else if(prpsDivs.equals("R")){  // 방송 
			list = (List) adminRghtDao.rghtBroadcastDetail(map);
		}else if(prpsDivs.equals("N")){  // 뉴스 
			list = (List) adminRghtDao.rghtNewsDetail(map);
		}
		
		addList("ds_works_info", list);
		
	}
	
	// adminBoardList 저장  
	public void rghtSave() throws Exception {

		Dataset ds_List = getDataset("ds_list");   
		Dataset ds_Temp = getDataset("ds_temp");
		Dataset ds_Condition = getDataset("ds_condition");

		// UPDATE처리
		Map map = getMap(ds_List, 0 );
		Map conditionMap = getMap(ds_Condition, 0);
		
		String userId = map.get("userId").toString();
		String caId = conditionMap.get("GUBUN_TRST_ORGN_CODE").toString();
		String prpsDivs = conditionMap.get("PRPS_DIVS").toString();
		
		String row_status = null;

		// 통합저작권자아이디 저장(201008 수정)
	//	adminRghtDao.userIcnUpdate(map);
		
		for( int i=0;i<ds_Temp.getRowCount();i++ ) {
			
			row_status = ds_Temp.getRowStatus(i);
			
			Map trstGroupMap = getMap(ds_Temp, i);
			
			if(row_status.equals("update") == true){
				
				trstGroupMap.put("userId", userId );
				
				adminRghtDao.rghtRsltUpdate(trstGroupMap); 
				
				// 이미지 저작권자 정보 저장 : 복전협(205)인 경우만..
				if(caId.equals("205") && prpsDivs.equals("I")){
					
					if(map.get("PRPS_RGHT_CODE").equals("3")) {
						trstGroupMap.put("IMAGE_COPT_HODR", trstGroupMap.get("COPT_HODR"));
					}
					
					adminRghtDao.imgeUpdate(trstGroupMap);
				}
		
				if(map.get("PRPS_RGHT_CODE").equals("3")){  // 이용자 권리조회 
					
					if(!prpsDivs.equals("X")){
						if(caId.equals("201")){ // 음저협 
							if(trstGroupMap.get("LYRICIST") != null){
								trstGroupMap.put("RGHT_ROLE_CODE", "101");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("LYRICIST"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
							if(trstGroupMap.get("COMPOSER") != null){
								trstGroupMap.put("RGHT_ROLE_CODE", "102");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("COMPOSER"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
							if(trstGroupMap.get("ARRANGER") != null){
								trstGroupMap.put("RGHT_ROLE_CODE", "103");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("ARRANGER"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}else if(caId.equals("202")){  // 음실연
							if(trstGroupMap.get("SINGER") != null){  // 가창 
								trstGroupMap.put("RGHT_ROLE_CODE", "112");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("SINGER"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
							if(trstGroupMap.get("PLAYER") != null){  // 연주 
								trstGroupMap.put("RGHT_ROLE_CODE", "113");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("PLAYER"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
							if(trstGroupMap.get("CONDUCTOR") != null){  // 지휘 
								trstGroupMap.put("RGHT_ROLE_CODE", "114");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("CONDUCTOR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}else if(caId.equals("203")){  // 음제협 
							if(trstGroupMap.get("PRODUCER") != null){  // 앨범제작  
								trstGroupMap.put("RGHT_ROLE_CODE", "111");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("PRODUCER"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}else if(caId.equals("204")){  // 문예협
							if(trstGroupMap.get("LICENSOR_NAME_KOR") != null){  // 역자
								trstGroupMap.put("RGHT_ROLE_CODE", "201");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("LICENSOR_NAME_KOR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
							if(trstGroupMap.get("TRANSLATOR") != null){  // 지휘 
								trstGroupMap.put("RGHT_ROLE_CODE", "202");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("TRANSLATOR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
							// (이미지)
							if(trstGroupMap.get("COPT_HODR") != null){  // 저작권자 
								trstGroupMap.put("RGHT_ROLE_CODE", "301");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("COPT_HODR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						} else if(caId.equals("205")){  // 복전협
							if(trstGroupMap.get("LICENSOR_NAME_KOR") != null){  // 저자 
								trstGroupMap.put("RGHT_ROLE_CODE", "201");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("LICENSOR_NAME_KOR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
							if(trstGroupMap.get("TRANSLATOR") != null){  // 역자
								trstGroupMap.put("RGHT_ROLE_CODE", "202");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("TRANSLATOR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
							
							// (이미지)
							if(trstGroupMap.get("COPT_HODR") != null){  // 저작권자
								trstGroupMap.put("RGHT_ROLE_CODE", "301");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("COPT_HODR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}else if(caId.equals("216") && prpsDivs.equals("I")){  // 문화콘텐츠진흥원(이미지)
							if(trstGroupMap.get("COPT_HODR") != null){  // 저작권자 
								trstGroupMap.put("RGHT_ROLE_CODE", "301");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("COPT_HODR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}else if(caId.equals("206")){  // 방작협 
							if(trstGroupMap.get("WRITER") != null){  // 작가 
								trstGroupMap.put("RGHT_ROLE_CODE", "501");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("WRITER"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}else if(caId.equals("211")){  // 영산협
//							if(trstGroupMap.get("MOVIE_PRODUCER") != null){  // 제작사 
//								trstGroupMap.put("RGHT_ROLE_CODE", "401");
//								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("MOVIE_PRODUCER"));
//								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
//							}
//							if(trstGroupMap.get("INVESTOR") != null){  // 투자사 
//								trstGroupMap.put("RGHT_ROLE_CODE", "402");
//								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("INVESTOR"));
//								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
//							}
							if(trstGroupMap.get("DISTRIBUTOR") != null){  // 배급사 
								trstGroupMap.put("RGHT_ROLE_CODE", "403");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("DISTRIBUTOR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}else if(caId.equals("212")){  // 한국시나리오작가협회
							if(trstGroupMap.get("WRITER") != null){  // 작가 
								trstGroupMap.put("RGHT_ROLE_CODE", "404");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("WRITER"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}else if(caId.equals("213")){  // 한국영화제작가협회
							if(trstGroupMap.get("MOVIE_PRODUCER") != null){  // 제작사 
								trstGroupMap.put("RGHT_ROLE_CODE", "401");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("MOVIE_PRODUCER"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
							if(trstGroupMap.get("INVESTOR") != null){  // 투자사 
								trstGroupMap.put("RGHT_ROLE_CODE", "402");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("INVESTOR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
							if(trstGroupMap.get("DISTRIBUTOR") != null){  // 배급사 
								trstGroupMap.put("RGHT_ROLE_CODE", "403");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("DISTRIBUTOR"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}else if(caId.equals("214")){  // 한국방송실연자협회
							if(trstGroupMap.get("MAKER") != null){  // 제작사 
								trstGroupMap.put("RGHT_ROLE_CODE", "601");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("MAKER"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}else if(caId.equals("215")){  // 언론진흥재단
							if(trstGroupMap.get("PROVIDER") != null){  // 제작사 
								trstGroupMap.put("RGHT_ROLE_CODE", "701");
								trstGroupMap.put("HOLD_NAME", trstGroupMap.get("PROVIDER"));
								adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
							}
						}
					}else{
						//if(trstGroupMap.get("SIDE_COPT_HODR") != null){  // 지휘 
							trstGroupMap.put("RGHT_ROLE_CODE", "001");
							trstGroupMap.put("HOLD_NAME", trstGroupMap.get("SIDE_COPT_HODR"));
							adminRghtDao.prpsRsltRghtUpdate(trstGroupMap); 
						//}
					}
					
				}
				
			}
			
		}
		
		// 메일, SMS
		Map infoMap = getMap(ds_List, 0);
		
		infoMap.put("DEAL_STAT", "");					
		int totalCnt = adminRghtDao.rghtRsltCnt(infoMap);  // 전체 결과 Cnt
		
		infoMap.put("DEAL_STAT", "4");
		int doneCnt = adminRghtDao.rghtRsltCnt(infoMap);  // 완료 Cnt

		if( (totalCnt-doneCnt )==0) {
		//if( map.get("DEAL_STAT").equals("4") ){
			
			
			
			//----------------------------- mail send------------------------------//
			
		    String host = Constants.MAIL_SERVER_IP;							// smtp서버
		    String to     = infoMap.get("U_MAIL").toString();					// 수신EMAIL
		    String toName = infoMap.get("USER_NAME").toString();			// 수신인
		    String subject = "[저작권찾기] 저작권찾기신청 처리완료";
		    String idntName = adminRghtDao.rghtPrpsTitle(infoMap); //infoMap.get("PRPS_IDNT_NAME").toString();		// 신청대상명
		      
		    String from = Constants.SYSTEM_MAIL_ADDRESS;  //발신인 주소 - 시스템 대표 메일
		    String fromName = Constants.SYSTEM_NAME;
		      
		    if( infoMap.get("U_MAIL_RECE_YSNO").equals("Y") && to != null && to != "")  {
			
		    	//------------------------- mail 정보 ----------------------------------//
			    MailInfo info = new MailInfo();
			    info.setFrom(from);
			    info.setFromName(fromName);
			    info.setHost(host);
			    info.setEmail(to);
			    info.setEmailName(toName);
			    info.setSubject(subject);
			      
			    StringBuffer sb = new StringBuffer();   
			    
			    
			    
			    sb.append("<div class=\"mail_title\">"+toName+"님, 저작권찾기신청이 처리완료 되었습니다. </div>");
			    sb.append("<div class=\"mail_contents\"> ");
			    sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
			    sb.append("		<tr> ");
			    sb.append("			<td> ");
			    sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
	// 변경부분
			    sb.append("					<tr> ");
			    sb.append("						<td class=\"tdLabel\">신청 저작물명</td> ");
			    sb.append("						<td class=\"tdData\">"+ idntName+"</td> ");
			    sb.append("					</tr> ");
	// 변경부분
//			    sb.append("					<tr> ");
//			    sb.append("						<td class=\"tdLabel\">이메일 주소</td> ");
//			    sb.append("						<td class=\"tdData\"><a href=\"mailto:"+Constants.SYSTEM_MAIL_ADDRESS+"\" onFocus='this.blur()'>"+Constants.SYSTEM_MAIL_ADDRESS+"</a></td> ");
//			    sb.append("					</tr> ");
//			    sb.append("					<tr> ");
//			    sb.append("						<td class=\"tdLabel\">전화번호</td> ");
//			    sb.append("						<td class=\"tdData\">"+Constants.SYSTEM_TELEPHONE+"</td> ");
//			    sb.append("					</tr> ");
			    sb.append("				</table> ");
			    sb.append("			</td> ");
			    sb.append("		</tr> ");
			    sb.append("	</table> ");
			    sb.append("</div> ");

			    info.setMessage(new String(MailMessage.getChangeInfoMailText(sb.toString())));
	    	    MailManager manager = MailManager.getInstance();
		    	
	    	    info = manager.sendMail(info);
		    	if(info.isSuccess()){
		    	  System.out.println(">>>>>>>>>>>> message success :"+info.getEmail() );
		    	}else{
		    	  System.out.println(">>>>>>>>>>>> message false   :"+info.getEmail() );
		    	}
		    }
		      
		    String smsTo = infoMap.get("U_MOBL_PHON").toString();
		    String smsFrom = Constants.SYSTEM_TELEPHONE;
		    
		    String smsMessage = "[저작권찾기] "+ idntName+" - 저작권찾기신청 처리완료 되었습니다.";
		    
		    if( infoMap.get("U_SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {
			
		    	SMSManager smsManager = new SMSManager();
			  	
			  	System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
		    }	
		    
		}	// end email, sms
		
		
		// 상태처리 후 상태값 구한다.
		Map conMap = getMap(ds_Condition, 0);
		List dealStat = (List) adminRghtDao.rghtDealStat(conMap);

		addList("ds_dealStat", dealStat);
	}
	
	
	// 삭제처리
	public void rghtDelete() throws Exception {
/*
		Dataset ds_List = getDataset("ds_list");   

		Map map = getMap(ds_List, 0 );

		adminRghtDao.rghtDelete(map);
*/
		Dataset ds_List = getDataset("ds_list");   
		Dataset ds_fileList = getDataset("ds_fileList");				// 첨부파일
		
		Map map = getMap(ds_List, 0 );
		
		// 삭제처리 전에 현재 최대상태값을 확인
		String maxDealStat = "";
		List maxDealStatList = rghtDao.getMaxDealStat(map);
		
		maxDealStat = (String)((HashMap)maxDealStatList.get(0)).get("MAX_DEAL_STAT");
		
System.out.println("==========================================");		
System.out.println("maxDealStat::::"+maxDealStat);
System.out.println("==========================================");		

		// 수정처리 - 최대상태값이 1 신청인 경우
		if(maxDealStat.equals("1")) {
		
			try{
				//내권리찾기 신청 master 삭제(ML_PRPS) 
				rghtDao.prpsMasterDelete(map);
			
				//내권리찾기 신청 DETAIL 삭제(ML_PRPS_RSLT)  		
				rghtDao.prpsDetailDelete(map);
				
				//내권리찾기 신청 권리정보 삭제(ML_PRPS_RSLT_RGHT)
				adminRghtDao.prpsRsltRghtDelete(map);
	
				// 첨부파일삭제
				for( int fi = 0; fi<ds_fileList.getRowCount(); fi++ )
				{
					Map deleteMap = getMap(ds_fileList, fi);
			
					rghtDao.rghtFileDelete(deleteMap);
					
					String fileName = ds_fileList.getColumn(fi, "REAL_FILE_NAME").toString();
	
					 try 
					 {
						 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
					     if (file.exists()) 
					     { 
					    	 file.delete(); 
					     }
					  } 
					  catch (Exception e) 
					  {
					   e.printStackTrace();
					  } 
				}
				
		        // 강명표 추가 START
				map.put("CONN_URL", "내권리찾기신청 삭제");
				rghtDao.insertConnInfo(map);
				// 강명표 추가 END
			}catch(Exception e){
				e.printStackTrace();
			}
		}	// end if.. 삭제처리.
		
		addList("ds_maxDealStat"			, maxDealStatList);
		
	}	
	
	
	// adminBoardList 저장  
	public void rghtEndUpdate() throws Exception {

		Dataset ds_List = getDataset("ds_list");   

		// UPDATE처리
		Map map = getMap(ds_List, 0 );

		adminRghtDao.rghtEndUpdate(map);

	}
	
	
	
	
	// 통합저작권자ID신
	public void getIcnNumb() throws Exception {

		Dataset ds_icnCondition = getDataset("ds_icnCondition");     

System.out.println("11111111111111111111111");
		ds_icnCondition.printDataset();

		
		//  Icn number 채번
		CopyRightManageServiceProxy copyRightProxy = new CopyRightManageServiceProxy();
		ResponseInfo response = new ResponseInfo();		
		
		TaLicenseMasterInfo masterInfo = new TaLicenseMasterInfo(); 
		TaLicenseDetailInfo detailInfo = new TaLicenseDetailInfo(); 
		
		masterInfo.setLicensor_name_kor( ds_icnCondition.getColumnAsString(0, "licensorName") );//권리자한글명
		masterInfo.setRegister_type(ds_icnCondition.getColumnAsString(0, "registerType"));//사업자유형 1주민  2사업자
		masterInfo.setId_type_code( Integer.parseInt(ds_icnCondition.getColumnAsString(0, "idTypeCode")) );//식별번호구분 1개인  3사업자
		masterInfo.setId_no(ds_icnCondition.getColumnAsString(0, "idNo"));//식별번호

		masterInfo.setIpin_num("");//아이핀넘버
		masterInfo.setInsert_id ("200"); 

		detailInfo.setCa_id(Integer.parseInt(ds_icnCondition.getColumnAsString(0, "caId")));//신탁단체코드
		detailInfo.setInsert_id ("200"); 
		
		masterInfo.setDetailInfo(detailInfo);
		
		response = copyRightProxy.isrtCopyRightHolder(masterInfo);
		
		String[] result = null;
		
		if(response.getErrcode().equals("100")){
			result  =	response.getIntegrateArray()[0].getIntegrate_id().split("<>");
			System.out.println("result====>"+result[0]);
		}
	
		Map map =  getMap(ds_icnCondition, 0);
		map.put("ICN_NUMB", result[0]);
		
		//DAO호출
		List list = (List) adminRghtDao.getIcnNumb(map); 
		
		//hmap를 dataset로 변환한다.
		addList("ds_icn", list);

	}	
}
