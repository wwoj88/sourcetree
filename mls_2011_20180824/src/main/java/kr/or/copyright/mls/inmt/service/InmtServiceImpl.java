package kr.or.copyright.mls.inmt.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.inmt.dao.InmtDao;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;
import kr.or.copyright.mls.common.utils.FileUtil;


import com.tobesoft.platform.data.Dataset;

public class InmtServiceImpl extends BaseService implements InmtService {

//	private Log logger = LogFactory.getLog(getClass());

	private InmtDao inmtDao;
	
	public void setImitDao(InmtDao inmtDao){
		this.inmtDao = inmtDao;
	}

	// inmtList 조회 
	public void inmtList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) inmtDao.inmtList(map);		
		List pageCount = (List) inmtDao.inmtListCount(map); 
	
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		addList("ds_page", pageCount);	

	}
	
	// inmt 조회
	public void inmtSelect() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");  
		
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List prpsList	 = (List)inmtDao.inmtPrpsSelect(map);			// ML_PRPS
		List rsltList 	 = (List)inmtDao.inmtPrpsRsltList(map);		// ML_PRPS_RSLT
		List fileList		 = (List)inmtDao.inmtFileList(map);				// ML_PRPS_ATTC
		List totDealStat = (List)inmtDao.inmtTotalDealStat(map);	// 전체처리상태 값
		List kappList 	 = null;														// ML_KAPP_PRPS
		List fokapoList = null;														// ML_FOKAPO_PRPS
		List krtraList 	 = null;														// ML_KRTRA_PRPS
		List krtraList_2 	 = null;														// ML_KRTRA_PRPS
		List tempList	 = (List)inmtDao.inmtTempList(map);			// 신청대상명
		List trstList		 = new ArrayList();									// 관련 신탁데이타
		
		/*
		HashMap tempMap = new HashMap();
		tempMap.put("SDSR_NAME", ((HashMap)prpsList.get(0)).get("PRPS_IDNT_NAME"));			// 대상 저작물 명
		tempMap.put("INMT_SEQN",  ((HashMap)prpsList.get(0)).get("PRPS_IDNT"));						// 대상 저작물 아이디
		tempMap.put("PRPS_DIVS",  ((HashMap)prpsList.get(0)).get("PRPS_DIVS"));						// 대상 저작물 구분값
		tempMap.put("KAPP",  ((HashMap)prpsList.get(0)).get("KAPP"));						// 대상 저작물 구분값
		tempMap.put("FOKAPO",  ((HashMap)prpsList.get(0)).get("FOKAPO"));						// 대상 저작물 구분값
		tempMap.put("KRTRA",  ((HashMap)prpsList.get(0)).get("KRTRA"));						// 대상 저작물 구분값
		
		tempList.add(tempMap);
		*/
		
		// 1. rsltList - trst_org_code 값에 따라  관련 테이블 데이타를 가져온다.
		
		String attFileYn = "";
		String offxListRecp = "";
		int iTrst = 0; int i202=0;  int i203=0;  int i205=0;  int i205_2=0; 
		
//		if( !((HashMap)prpsList.get(0)).get("OFFX_LINE_RECP").equals("Y") )
//		{
			
		for( int rs = 0; rs< rsltList.size(); rs++) {
			
			HashMap hMap = (HashMap)rsltList.get(rs);
			
			// 음제협
			if( hMap.get("TRST_ORGN_CODE").equals("203") ) {
				
				kappList = (List)inmtDao.inmtKappList(map);
				
				if(kappList.size()>0) {
					attFileYn = (String) ((HashMap)kappList.get(0)).get("ATTC_YSNO");
					offxListRecp = (String) ((HashMap)kappList.get(0)).get("OFFX_LINE_RECP");
				}
				
			}
			
			// 음실연
			else if( hMap.get("TRST_ORGN_CODE").equals("202") ) {
				
				fokapoList = (List)inmtDao.inmtFokapoList(map);
				
				if(fokapoList.size()>0)
				{
					attFileYn = (String) ((HashMap)fokapoList.get(0)).get("ATTC_YSNO");
					offxListRecp = (String) ((HashMap)fokapoList.get(0)).get("OFFX_LINE_RECP");
				}
			}
			
			// 복제협
			
			else if( hMap.get("TRST_ORGN_CODE").equals("205") && hMap.get("PRPS_DIVS").equals("S") ) {
				
				map.put("TRST_ORGN_CODE", hMap.get("TRST_ORGN_CODE"));
				map.put("PRPS_DIVS", hMap.get("PRPS_DIVS"));
				
				krtraList = (List)inmtDao.inmtKrtraList(map);
				
				if(krtraList.size()>0)
				{
					attFileYn = (String) ((HashMap)krtraList.get(0)).get("ATTC_YSNO");
					offxListRecp = (String) ((HashMap)krtraList.get(0)).get("OFFX_LINE_RECP");
				}
			}
			
			else if( hMap.get("TRST_ORGN_CODE").equals("205")  && hMap.get("PRPS_DIVS").equals("L")) {
				
				map.put("TRST_ORGN_CODE", hMap.get("TRST_ORGN_CODE"));
				map.put("PRPS_DIVS", hMap.get("PRPS_DIVS"));
				
				krtraList_2 = (List)inmtDao.inmtKrtraList(map);
				
				if(krtraList_2.size()>0)
				{
					attFileYn = (String) ((HashMap)krtraList_2.get(0)).get("ATTC_YSNO");
					offxListRecp = (String) ((HashMap)krtraList_2.get(0)).get("OFFX_LINE_RECP");
				}
			}

			if( hMap.get("TRST_ORGN_CODE").equals("203") && (++i203)==1 
					|| hMap.get("TRST_ORGN_CODE").equals("202") && (++i202)==1 
					|| hMap.get("TRST_ORGN_CODE").equals("205") && hMap.get("PRPS_DIVS").equals("S")  && (++i205)==1 
					|| hMap.get("TRST_ORGN_CODE").equals("205") && hMap.get("PRPS_DIVS").equals("L")  && (++i205_2)==1 ) {
				
				HashMap trMap = new HashMap();
				
				if( hMap.get("TRST_ORGN_CODE").equals("205") && hMap.get("PRPS_DIVS").equals("L"))
					trMap.put("TRST_ORGN_CODE", "205_2");	
				else
					trMap.put("TRST_ORGN_CODE", hMap.get("TRST_ORGN_CODE"));			// 분류코드값 : 신탁기관
				
				trMap.put("DEAL_STAT", hMap.get("DEAL_STAT"));								// 상태 값
				trMap.put("RSLT_DESC", hMap.get("RSLT_DESC"));								// 처리결과
				trMap.put("TRST_ATTCHFILE_YN", attFileYn);										// 파일첨부 YN 값
				trMap.put("OFFX_LINE_RECP", offxListRecp);										// 오프라인접수 YN 값
				trMap.put("INMT_AMNT", hMap.get("INMT_AMNT"));								// 보상금액
	
				trstList.add(iTrst, trMap);		
				
				iTrst++;
			}
		}
		
		//}
		addList("ds_list"				, prpsList);
		addList("ds_fileList" 		, fileList);
		addList("ds_totDealStat"		, totDealStat);
		addList("ds_list_trst"		, trstList);
		addList("ds_list_kapp"		, kappList);
		addList("ds_list_fokapo"	, fokapoList);
		addList("ds_list_krtra"		, krtraList);
		addList("ds_list_krtra2"	, krtraList_2);
		addList("ds_temp"			, tempList);
		
	}

	// inmtSave 저장
	public void inmtSave() throws Exception {
		
		Dataset ds_list 			= getDataset("ds_list"); 								// 기본정보 리스트
		Dataset ds_fileList 		= getDataset("ds_fileList");							// 첨부파일 리스트
		Dataset ds_list_trst 	= getDataset("ds_list_trst"); 						// 신탁구분 리스트
		Dataset ds_list_kapp 	= getDataset("ds_list_kapp"); 						// 음제협 리스트
		Dataset ds_list_fokapo = getDataset("ds_list_fokapo"); 				// 음실연 리스트
		Dataset ds_list_krtra	= getDataset("ds_list_krtra"); 						// 복전협 리스트
		Dataset ds_temp 		= getDataset("ds_temp"); 							// 신청대상(선택된저작물) 리스트
		Dataset ds_userInfo	= getDataset("gds_userInfo");						// 로그인정보
		
/*		
		ds_temp.printDataset();	
		ds_list_trst.printDataset();		
		ds_list_kapp.printDataset();		
		ds_list_fokapo.printDataset();		
		ds_list_krtra.printDataset();	
		ds_fileList.printDataset();
*/
		// INSERT, UPDATE 처리
			
		// row 가 하나밖에 없으므로... 
		Map map = getMap(ds_list, 0);
		Map infoMap = getMap(ds_userInfo, 0);
		
		String userIdnt = (String)map.get("USER_IDNT");
		
		int prpsMastKey  = inmtDao.prpsMastKey();								// MAX(ML_PRPS.PRPS_MAST_KEY)+1 set
		int prpsSeqnMax = 0;
	
		int bPrpsSeqnMax = 0;
		int sPrpsSeqnMax = 0;
		int lPrpsSeqnMax = 0;
		
		int iBcnt = 0; int iScnt = 0; int iLcnt = 0; int intiB2cnt=1;
		
		// 선택저작물에 대해..
		for( int te = 0 ; te<ds_temp.getRowCount() ; te++ ) {
			
			Map tempMap = getMap(ds_temp, te);
			
			String prpsIdnt = (String)tempMap.get("INMT_SEQN");				 // 선택저작물 seq값
			String prpsDivs = (String)tempMap.get("PRPS_DIVS");				// 신청구분값	B,S,L
			
			String kapp =  (String)tempMap.get("KAPP");
			String fokapo = (String)tempMap.get("FOKAPO");
			String krtra = (String)tempMap.get("KRTRA");
			
			map.put("prpsIdnt", 			prpsIdnt);				
			map.put("prpsMastKey", 	""+prpsMastKey);	
			
			if( (prpsDivs.equals("B") && iBcnt == 0) || (prpsDivs.equals("S") && iScnt == 0) || (prpsDivs.equals("L") && iLcnt == 0) ){

				prpsSeqnMax = inmtDao.prpsSeqnMax();							// MAX(ML_PRPS.PRPS_SEQN)+1 set
			
				map.put("prpsSeqnMax", 	""+prpsSeqnMax);
				map.put("prpsDivs", 			prpsDivs);	
				
				inmtDao.inmtPspsInsert(map);				// insert ML_PRPS
				
				if(prpsDivs.equals("B")) 
					bPrpsSeqnMax = prpsSeqnMax;
				else if(prpsDivs.equals("S")) 
					sPrpsSeqnMax = prpsSeqnMax;
				else if(prpsDivs.equals("L")) 
					lPrpsSeqnMax = prpsSeqnMax;
			
			}
			
				
			if(prpsDivs.equals("B")) {
				iBcnt++;	prpsSeqnMax = bPrpsSeqnMax;
			}
			else if(prpsDivs.equals("S")) {
				iScnt++;	prpsSeqnMax = sPrpsSeqnMax;
			}
			else if(prpsDivs.equals("L")) {
				iLcnt++;	prpsSeqnMax = lPrpsSeqnMax;
			}


			
	
			// 신탁구분값에 대하여..
			for( int k = 0 ; k<ds_list_trst.getRowCount() ; k++ ) {
				
				Map trstMap = getMap(ds_list_trst, k);
				
				String trstOrgnCode = (String)trstMap.get("TRST_ORGN_CODE");		// 신탁구분 코드
				String attcYsNo = "N";
				String offxLineRecp = "N";
				
				if((String)trstMap.get("TRST_ATTCHFILE_YN")!=null )
					attcYsNo = (String)trstMap.get("TRST_ATTCHFILE_YN");		// 신탁별 첨부파일등록 여부정보
				
				if((String)trstMap.get("OFFX_LINE_RECP")!=null )
					offxLineRecp = (String)trstMap.get("OFFX_LINE_RECP");		// 오프라인접수 여부정보
				
				map.put("trstOrgnCode", 			trstOrgnCode);			
				
				//  신탁단체별 신청처리결과 기본값 insert
				//inmtDao.inmtPrpsRsltInsert(map);		// insert ML_PRPS_RSLT
				
				int iCnt= 0;
				
				// 음제협
				if(trstOrgnCode.equals("203") && kapp.equals("Y")) {
					
					for( int km = 0 ; km<ds_list_kapp.getRowCount(); km++ ){
						
						Map kappMap = getMap(ds_list_kapp, km);
						
						kappMap.put("prpsMastKey", ""+prpsMastKey);
						
						kappMap.put("userIdnt", userIdnt);
						kappMap.put("prpsSeqnMax", ""+prpsSeqnMax);
						kappMap.put("trstOrgnCode", trstOrgnCode);
						kappMap.put("prpsIdnt", prpsIdnt);									// 선택저작물 seq값
						kappMap.put("prpsDivs", prpsDivs);									// 신청구분값	B,S,L
						kappMap.put("attcYsNo", attcYsNo);
						kappMap.put("offxLineRecp", offxLineRecp);
						
						if( kappMap.get("PRPS_IDNT").equals(prpsIdnt))
						{
							iCnt++;
							inmtDao.inmtKappInsert(kappMap);		
						}
					}
				}	// end if.. 음제협
				
				// 음실연
				else if(trstOrgnCode.equals("202") && fokapo.equals("Y")) {
					
					for( int km = 0 ; km<ds_list_fokapo.getRowCount(); km++ ){
						//iCnt++;
						
						Map fokapoMap = getMap(ds_list_fokapo, km);
						
						fokapoMap.put("prpsMastKey", ""+prpsMastKey);
						
						fokapoMap.put("userIdnt", userIdnt);
						fokapoMap.put("prpsSeqnMax", ""+prpsSeqnMax);
						fokapoMap.put("trstOrgnCode", trstOrgnCode);
						fokapoMap.put("prpsIdnt", prpsIdnt);									// 선택저작물 seq값
						fokapoMap.put("prpsDivs", prpsDivs);									// 신청구분값	B,S,L
						fokapoMap.put("attcYsNo", attcYsNo);
						fokapoMap.put("offxLineRecp", offxLineRecp);
						
						if( fokapoMap.get("PRPS_IDNT").equals(prpsIdnt))
						{
							iCnt++;
							inmtDao.inmtFokapoInsert(fokapoMap);		
						}
					}
				}	// end if.. 음실연
				
				// 복제협 - 교과용
				else if( trstOrgnCode.equals("205") && krtra.equals("Y") && prpsDivs.equals("S") ) {
					
					for( int km = 0 ; km<ds_list_krtra.getRowCount(); km++ ){
						//iCnt++;
						Map krtraMap = getMap(ds_list_krtra, km);
						
						krtraMap.put("prpsMastKey", ""+prpsMastKey);
						
						krtraMap.put("userIdnt", userIdnt);
						krtraMap.put("prpsSeqnMax", ""+prpsSeqnMax);
						krtraMap.put("trstOrgnCode", trstOrgnCode);
						krtraMap.put("prpsIdnt", prpsIdnt);									// 선택저작물 seq값
						krtraMap.put("prpsDivs", prpsDivs);									// 신청구분값	B,S,L
						krtraMap.put("attcYsNo", attcYsNo);
						krtraMap.put("offxLineRecp", offxLineRecp);
						
						if( krtraMap.get("PRPS_IDNT").equals(prpsIdnt) && krtraMap.get("PRPS_DIVS").equals("S"))
						{
							iCnt++;
							inmtDao.inmtKrtraInsert(krtraMap);		
						}
					}
				}	// end if.. 복제협1
				
//				 복제협 -  도서관
				else if(trstOrgnCode.equals("205_2") && krtra.equals("Y") && prpsDivs.equals("L") ) {
					
					for( int km = 0 ; km<ds_list_krtra.getRowCount(); km++ ){
						//iCnt++;
						
						Map krtraMap = getMap(ds_list_krtra, km);
						
						krtraMap.put("prpsMastKey", ""+prpsMastKey);
						
						krtraMap.put("userIdnt", userIdnt);
						krtraMap.put("prpsSeqnMax", ""+prpsSeqnMax);
						krtraMap.put("trstOrgnCode", "205");
						krtraMap.put("prpsIdnt", prpsIdnt);									// 선택저작물 seq값
						krtraMap.put("prpsDivs", prpsDivs);									// 신청구분값	B,S,L
						krtraMap.put("attcYsNo", attcYsNo);
						krtraMap.put("offxLineRecp", offxLineRecp);
						
						if(krtraMap.get("PRPS_IDNT").equals(prpsIdnt) && krtraMap.get("PRPS_DIVS").equals("L"))
						{
							iCnt++;
							inmtDao.inmtKrtraInsert(krtraMap);		
						}
					}
				}	// end if.. 복제협2
			
				if(iCnt>0)
				{			
					map.put("prpsSeqnMax", 	""+prpsSeqnMax);
					map.put("prpsDivs", 			prpsDivs);	
					
					if(trstOrgnCode.equals("205_2") && prpsDivs.equals("L") )
						map.put("trstOrgnCode", "205");
					
					//신탁단체별 신청처리결과 기본값 insert
					inmtDao.inmtPrpsRsltInsert(map);		// insert ML_PRPS_RSLT
				}
			}	// end for.. 신탁 구분 
			
			// 선택저작물에대한 첨부파일등록
			if( prpsDivs.equals("B") || (prpsDivs.equals("S") && iScnt == 1) || (prpsDivs.equals("L") && iLcnt == 1) ){
				
				for( int fi = 0; fi<ds_fileList.getRowCount() && ds_fileList != null; fi++) {
					
					Map fileMap = getMap(ds_fileList, fi);
					
					if( ds_fileList.getRowStatus(fi).equals("insert") == true ) {
						
						byte[] file = ds_fileList.getColumn(fi, "CLIENT_FILE").getBinary();
						
						String fileName = ds_fileList.getColumnAsString(fi, "REAL_FILE_NAME");
						
						Map upload = FileUtil.uploadMiFile(fileName, file);
						
						//fileMap.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
						
						fileMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
						fileMap.put("FILE_PATH", upload.get("FILE_PATH"));
						
						fileMap.put("prpsMastKey", ""+prpsMastKey);
						fileMap.put("PRPS_SEQN", ""+prpsSeqnMax);
						fileMap.put("PRPS_DIVS", prpsDivs);

						String fileOrgnCode = fileMap.get("TRST_ORGN_CODE").toString();
							
						if(  ( prpsDivs.equals("B")  && kapp.equals("Y") && fileOrgnCode.equals("203") && iBcnt==1 ) 
								|| ( prpsDivs.equals("B")  && fokapo.equals("Y") && fileOrgnCode.equals("202") && intiB2cnt==1 ) 
								|| ( prpsDivs.equals("S")  && krtra.equals("Y") && fileOrgnCode.equals("205") )  
								||  ( prpsDivs.equals("L") &&  krtra.equals("Y") && fileOrgnCode.equals("205_2") )  ) 
						{
							
							// if fileOrgnCode==205_2 인경우 205로 셋팅한다.
	
							if( prpsDivs.equals("B")  && fokapo.equals("Y") && fileOrgnCode.equals("202") )
								intiB2cnt++;
							
							if( prpsDivs.equals("L") && fileOrgnCode.equals("205_2") )
								fileMap.put("TRST_ORGN_CODE", "205");
							
							inmtDao.inmtFileInsert(fileMap);
						}
						
					}
					
				} // end for.. 첨부파일
			}
		
		}	// end for.. 선택저작물

		

		// 메일, SMS

		//----------------------------- mail send------------------------------//
		
	    String host = Constants.MAIL_SERVER_IP;							// smtp서버
	    String to     = infoMap.get("U_MAIL")+"";					// 수신EMAIL
	    String toName = map.get("PRPS_NAME").toString();			// 수신인
	    String subject = "[저작권찾기] 보상금신청 완료";
	    String idntName = getMap(ds_temp, 0).get("SDSR_NAME")+"";		// 신청대상명
	    
	    if( ds_temp.getRowCount() > 0)
	    	idntName  += " 외 "+ ( ds_temp.getRowCount()-1 )+ "건";
	      
	    String from = Constants.SYSTEM_MAIL_ADDRESS;  //발신인 주소 - 시스템 대표 메일
	    String fromName = Constants.SYSTEM_NAME;
	      
	    if( infoMap.get("U_MAIL_RECE_YSNO").equals("Y") && to != null && to != "")  {
		
	    	//------------------------- mail 정보 ----------------------------------//
		    MailInfo info = new MailInfo();
		    info.setFrom(from);
		    info.setFromName(fromName);
		    info.setHost(host);
		    info.setEmail(to);
		    info.setEmailName(toName);
		    info.setSubject(subject);
		      
		    StringBuffer sb = new StringBuffer();   
		    
		    
		    
		    sb.append("<div class=\"mail_title\">"+toName+"님, 보상금신청이  완료되었습니다. </div>");
		    sb.append("<div class=\"mail_contents\"> ");
		    sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
		    sb.append("		<tr> ");
		    sb.append("			<td> ");
		    sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
// 변경부분
		    sb.append("					<tr> ");
		    sb.append("						<td class=\"tdLabel\">신청 저작물명</td> ");
		    sb.append("						<td class=\"tdData\">"+ idntName+"</td> ");
		    sb.append("					</tr> ");
// 변경부분
//		    sb.append("					<tr> ");
//		    sb.append("						<td class=\"tdLabel\">이메일 주소</td> ");
//		    sb.append("						<td class=\"tdData\"><a href=\"mailto:"+Constants.SYSTEM_MAIL_ADDRESS+"\" onFocus='this.blur()'>"+Constants.SYSTEM_MAIL_ADDRESS+"</a></td> ");
//		    sb.append("					</tr> ");
//		    sb.append("					<tr> ");
//		    sb.append("						<td class=\"tdLabel\">전화번호</td> ");
//		    sb.append("						<td class=\"tdData\">"+Constants.SYSTEM_TELEPHONE+"</td> ");
//		    sb.append("					</tr> ");
		    sb.append("				</table> ");
		    sb.append("			</td> ");
		    sb.append("		</tr> ");
		    sb.append("	</table> ");
		    sb.append("</div> ");

		    info.setMessage(new String(MailMessage.getChangeInfoMailText(sb.toString())));
    	    MailManager manager = MailManager.getInstance();
	    	
    	    info = manager.sendMail(info);
	    	if(info.isSuccess()){
	    	  System.out.println(">>>>>>>>>>>> message success :"+info.getEmail() );
	    	}else{
	    	  System.out.println(">>>>>>>>>>>> message false   :"+info.getEmail() );
	    	}
	    }
	      
	    String smsTo = infoMap.get("U_MOBL_PHON")+"";
	    String smsFrom = Constants.SYSTEM_TELEPHONE;
	    
	    String smsMessage = "[저작권찾기] "+ idntName+" - 보상금신청이  완료되었습니다.";
	    
	    if( infoMap.get("U_SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {
		
	    	SMSManager smsManager = new SMSManager();
		  	
		  	System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
	    }	
	    
	// end email, sms
		
		// 강명표 추가 START
		map.put("CONN_URL", "보상금신청 등록");
		inmtDao.insertConnInfo(map);
		// 강명표 추가 END
	}
	
	
	// inmt 수정
	public void inmtModi() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");  						// PRPS_SEQN
		
		Dataset ds_list 			= getDataset("ds_list"); 								// 기본정보 리스트
		Dataset ds_fileList		= getDataset("ds_fileList");							// 첨부파일 리스트
		Dataset ds_list_trst 	= getDataset("ds_list_trst"); 						// 신탁구분 리스트
		Dataset ds_list_kapp 	= getDataset("ds_list_kapp"); 						// 음제협 리스트
		Dataset ds_list_fokapo = getDataset("ds_list_fokapo"); 				// 음실연 리스트
		Dataset ds_list_krtra 	= getDataset("ds_list_krtra"); 						// 복전협 리스트
		Dataset ds_temp 		= getDataset("ds_temp"); 							// 신청대상(선택된저작물) 리스트
/*
		ds_temp.printDataset();	
		ds_list_trst.printDataset();		
		ds_list_kapp.printDataset();		
		ds_list_fokapo.printDataset();		
		ds_list_krtra.printDataset();	
		ds_fileList.printDataset();
*/		

		// row 가 하나밖에 없으므로... 
		Map map = getMap(ds_list, 0);
		Map conMap = getMap(ds_condition, 0);
		
		// 수정처리 전에 현재 최대상태값을 확인
		String maxDealStat = "";
		List maxDealStatList = inmtDao.getMaxDealStat(conMap);
		
		maxDealStat = (String)((HashMap)maxDealStatList.get(0)).get("MAX_DEAL_STAT");
		
System.out.println("==========================================");		
System.out.println("maxDealStat::::"+maxDealStat);
System.out.println("==========================================");		

		// 수정처리 - 최대상태값이 1 신청인 경우
		if(maxDealStat.equals("1")) {
			
			String userIdnt = (String)map.get("USER_IDNT");				// 로그인 정보_아이디
			String rgstDttm = (String)map.get("RGST_DTTM");			// 등록일자.
			String prpsMastKey = (String)conMap.get("PRPS_MAST_KEY");	// PRPS_MAST_KEY :: 키값
			
			inmtDao.inmtKappDelete(conMap);									// DELETE ML_KAPP_PRPS
			inmtDao.inmtFokapoDelete(conMap);								// DELETE ML_FOKAPO_PRPS
			inmtDao.inmtKrtraDelete(conMap);									// DELETE ML_KRTRA_PRPS
			inmtDao.inmtPrpsRsltDelete(conMap);								// DELETE ML_PRPS_RSLT
			inmtDao.inmtPrpsDelete(conMap);									// DELETE ML_PRPS
			
			int prpsSeqnMax = 0;
			
			int bPrpsSeqnMax = 0;
			int sPrpsSeqnMax = 0;
			int lPrpsSeqnMax = 0;
			
			int iBcnt = 0; int iScnt = 0; int iLcnt = 0; int intiB2cnt=1;
			int iFiCnt = 0;
	
		
			// 선택저작물에 대해..
			for( int te = 0 ; te<ds_temp.getRowCount() ; te++ ) {
				
				Map tempMap = getMap(ds_temp, te);
				
				String prpsIdnt = tempMap.get("INMT_SEQN").toString().replace(".0","0");				 // 선택저작물 seq값
				String prpsDivs = (String)tempMap.get("PRPS_DIVS");				// 신청구분값	B,S,L
				
				String kapp =  (String)tempMap.get("KAPP");
				String fokapo = (String)tempMap.get("FOKAPO");
				String krtra = (String)tempMap.get("KRTRA");
				
				map.put("prpsIdnt", 			prpsIdnt);	
				map.put("prpsMastKey", 	prpsMastKey);	
				
				if( (prpsDivs.equals("B") && iBcnt == 0) || (prpsDivs.equals("S") && iScnt == 0) || (prpsDivs.equals("L") && iLcnt == 0) ){
	
					prpsSeqnMax = inmtDao.prpsSeqnMax();							// MAX(ML_PRPS.PRPS_SEQN)+1 set
				
					map.put("prpsSeqnMax", 	""+prpsSeqnMax);
					map.put("prpsDivs", 			prpsDivs);	
					map.put("rgstDttm", rgstDttm);
					
					inmtDao.inmtPspsInsert(map);				// insert ML_PRPS
					
					if(prpsDivs.equals("B")) 
						bPrpsSeqnMax = prpsSeqnMax;
					else if(prpsDivs.equals("S")) 
						sPrpsSeqnMax = prpsSeqnMax;
					else if(prpsDivs.equals("L")) 
						lPrpsSeqnMax = prpsSeqnMax;
				
				}
				
					
				if(prpsDivs.equals("B")) {
					iBcnt++;	prpsSeqnMax = bPrpsSeqnMax;
				}
				else if(prpsDivs.equals("S")) {
					iScnt++;	prpsSeqnMax = sPrpsSeqnMax;
				}
				else if(prpsDivs.equals("L")) {
					iLcnt++;	prpsSeqnMax = lPrpsSeqnMax;
				}
	
				
				//	신탁구분값에 대하여..
				for( int k = 0 ; k<ds_list_trst.getRowCount() ; k++ ) {
					
					Map trstMap = getMap(ds_list_trst, k);
					
					String trstOrgnCode = (String)trstMap.get("TRST_ORGN_CODE");		// 신탁구분 코드
					String attcYsNo = "N";
					String offxLineRecp = "N";
					
					if((String)trstMap.get("TRST_ATTCHFILE_YN")!=null )
						attcYsNo = (String)trstMap.get("TRST_ATTCHFILE_YN");		// 신탁별 첨부파일등록 여부정보
					
					if((String)trstMap.get("OFFX_LINE_RECP")!=null )
						offxLineRecp = (String)trstMap.get("OFFX_LINE_RECP");		// 오프라인접수 여부정보
					
					map.put("trstOrgnCode", 			trstOrgnCode);			
					
					int iCnt= 0;
					
					// 음제협
					if(trstOrgnCode.equals("203") && kapp.equals("Y")) {
						
						for( int km = 0 ; km<ds_list_kapp.getRowCount(); km++ ){
							
							Map kappMap = getMap(ds_list_kapp, km);
							
							kappMap.put("prpsMastKey", ""+prpsMastKey);
							
							kappMap.put("userIdnt", userIdnt);
							kappMap.put("prpsSeqnMax", ""+prpsSeqnMax);
							kappMap.put("trstOrgnCode", trstOrgnCode);
							kappMap.put("prpsIdnt", prpsIdnt);									// 선택저작물 seq값
							kappMap.put("prpsDivs", prpsDivs);									// 신청구분값	B,S,L
							kappMap.put("attcYsNo", attcYsNo);
							kappMap.put("offxLineRecp", offxLineRecp);
							kappMap.put("rgstDttm", rgstDttm);
							
							if( kappMap.get("PRPS_IDNT").equals(prpsIdnt))
							{	
								iCnt++;
								inmtDao.inmtKappInsert(kappMap);		
							}
						}
					}	// end if.. 음제협
					
					// 음실연
					else if(trstOrgnCode.equals("202") && fokapo.equals("Y")) {
						
						for( int km = 0 ; km<ds_list_fokapo.getRowCount(); km++ ){
							
							Map fokapoMap = getMap(ds_list_fokapo, km);
							
							fokapoMap.put("prpsMastKey", ""+prpsMastKey);
							
							fokapoMap.put("userIdnt", userIdnt);
							fokapoMap.put("prpsSeqnMax", ""+prpsSeqnMax);
							fokapoMap.put("trstOrgnCode", trstOrgnCode);
							fokapoMap.put("prpsIdnt", prpsIdnt);									// 선택저작물 seq값
							fokapoMap.put("prpsDivs", prpsDivs);									// 신청구분값	B,S,L
							fokapoMap.put("attcYsNo", attcYsNo);
							fokapoMap.put("offxLineRecp", offxLineRecp);
							fokapoMap.put("rgstDttm", rgstDttm);
							
							if( fokapoMap.get("PRPS_IDNT").equals(prpsIdnt))
							{ 
								iCnt++;
								inmtDao.inmtFokapoInsert(fokapoMap);		
							}
						}
					}	// end if.. 음실연
					
					// 복제협 - 교과용
					else if( trstOrgnCode.equals("205") && krtra.equals("Y") && prpsDivs.equals("S") ) {
						
						for( int km = 0 ; km<ds_list_krtra.getRowCount(); km++ ){

							Map krtraMap = getMap(ds_list_krtra, km);
							
							krtraMap.put("prpsMastKey", ""+prpsMastKey);
							
							krtraMap.put("userIdnt", userIdnt);
							krtraMap.put("prpsSeqnMax", ""+prpsSeqnMax);
							krtraMap.put("trstOrgnCode", trstOrgnCode);
							krtraMap.put("prpsIdnt", prpsIdnt);									// 선택저작물 seq값
							krtraMap.put("prpsDivs", prpsDivs);									// 신청구분값	B,S,L
							krtraMap.put("attcYsNo", attcYsNo);
							krtraMap.put("offxLineRecp", offxLineRecp);
							krtraMap.put("rgstDttm", rgstDttm);
							
							if( krtraMap.get("PRPS_IDNT").equals(prpsIdnt) && krtraMap.get("PRPS_DIVS").equals("S"))
							{
								iCnt++;
								inmtDao.inmtKrtraInsert(krtraMap);		
							}
						}
					}	// end if.. 복제협1
					
					//	복제협 -  도서관
					else if(trstOrgnCode.equals("205_2") && krtra.equals("Y") && prpsDivs.equals("L") ) {
						
						for( int km = 0 ; km<ds_list_krtra.getRowCount(); km++ ){
							
							Map krtraMap = getMap(ds_list_krtra, km);
							
							krtraMap.put("prpsMastKey", ""+prpsMastKey);
							
							krtraMap.put("userIdnt", userIdnt);
							krtraMap.put("prpsSeqnMax", ""+prpsSeqnMax);
							krtraMap.put("trstOrgnCode", "205");
							krtraMap.put("prpsIdnt", prpsIdnt);									// 선택저작물 seq값
							krtraMap.put("prpsDivs", prpsDivs);									// 신청구분값	B,S,L
							krtraMap.put("attcYsNo", attcYsNo);
							krtraMap.put("offxLineRecp", offxLineRecp);
							krtraMap.put("rgstDttm", rgstDttm);
							
							if(krtraMap.get("PRPS_IDNT").equals(prpsIdnt) && krtraMap.get("PRPS_DIVS").equals("L"))
							{
								iCnt++;
								inmtDao.inmtKrtraInsert(krtraMap);		
							}
						}
					}	// end if.. 복제협2    
				
					if(iCnt>0)
					{	
						map.put("prpsSeqnMax", 	""+prpsSeqnMax);
						map.put("prpsDivs", 			prpsDivs);	
						
						if(trstOrgnCode.equals("205_2") && prpsDivs.equals("L") )
							map.put("trstOrgnCode", "205");
						
						//신탁단체별 신청처리결과 기본값 insert
						inmtDao.inmtPrpsRsltInsert(map);		// insert ML_PRPS_RSLT
					}
				}	// end for.. 신탁 구분
	
				// 첨부파일삭제
				for( int fi=0; iFiCnt == 0 && fi<ds_fileList.getDeleteRowCount(); fi++) {
					
					Map deleteMap = getDeleteMap(ds_fileList, fi);
					inmtDao.inmtFileDeleteOne(deleteMap);
					
					String fileName = ds_fileList.getDeleteColumn(fi, "REAL_FILE_NAME").toString();
					
					 try 
					 {
						 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
					     if (file.exists()) 
					     { 
					    	 file.delete(); 
					     }
					  } 
					  catch (Exception e) 
					  {
					   e.printStackTrace();
					  } 
				}
				
				iFiCnt++;
			
				// 선택저작물에대한 첨부파일등록
	
				for( int fi = 0; fi<ds_fileList.getRowCount(); fi++) {
					
					Map fileMap = getMap(ds_fileList, fi);
					
					String fileOrgnCode = fileMap.get("TRST_ORGN_CODE").toString();
					
					if(  ( prpsDivs.equals("B")  && kapp.equals("Y") && fileOrgnCode.equals("203") && iBcnt==1 ) 
							|| ( prpsDivs.equals("B")  && fokapo.equals("Y") && fileOrgnCode.equals("202") && intiB2cnt==1 ) 
							|| ( prpsDivs.equals("S")  && krtra.equals("Y") && fileOrgnCode.equals("205") && iScnt == 1 )  
							||  ( prpsDivs.equals("L") &&  krtra.equals("Y") && fileOrgnCode.equals("205_2") && iLcnt == 1 )  ) 
					{
						
						if( fileMap.get("FILE_PATH")== null || fileMap.get("FILE_PATH").toString().length()==0 ) {
							
							byte[] file = ds_fileList.getColumn(fi, "CLIENT_FILE").getBinary();
							
							String fileName = ds_fileList.getColumnAsString(fi, "REAL_FILE_NAME");
							
							Map upload = FileUtil.uploadMiFile(fileName, file);
							
							fileMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
							fileMap.put("FILE_PATH", upload.get("FILE_PATH"));
						
						} else {
							inmtDao.inmtFileDeleteOne(fileMap);
						}
						
						
						fileMap.put("prpsMastKey", ""+prpsMastKey);
						fileMap.put("PRPS_SEQN", ""+prpsSeqnMax);
						fileMap.put("PRPS_DIVS", prpsDivs);

						if( prpsDivs.equals("B")  && fokapo.equals("Y") && fileOrgnCode.equals("202") )
							intiB2cnt++;
						
						if( prpsDivs.equals("L") && fileOrgnCode.equals("205_2") )
							fileMap.put("TRST_ORGN_CODE", "205");
						
						inmtDao.inmtFileInsert(fileMap);
					}

				} // end for.. 첨부파일		
			
			
			}	// end for.. 선택저작물
	
			// 강명표 추가 START
			map.put("CONN_URL", "보상금신청 수정");
			inmtDao.insertConnInfo(map);
			// 강명표 추가 END
		
		} // end if..  수정처리.		
		
		addList("ds_maxDealStat"			, maxDealStatList);
	}
	
	// inmt 삭제
	public void inmtDelete() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");  
		Dataset ds_fileList = getDataset("ds_fileList");				// 첨부파일
		
		Map deleteMap = getMap(ds_condition, 0);

		// 삭제처리 전에 현재 최대상태값을 확인
		String maxDealStat = "";
		List maxDealStatList = inmtDao.getMaxDealStat(deleteMap);
		
		maxDealStat = (String)((HashMap)maxDealStatList.get(0)).get("MAX_DEAL_STAT");
		
System.out.println("==========================================");		
System.out.println("maxDealStat::::"+maxDealStat);
System.out.println("==========================================");		

		// 삭제처리 - 최대상태값이 1 신청인 경우
		if(maxDealStat.equals("1")) {
			
			inmtDao.inmtKappDelete(deleteMap);			// DELETE ML_KAPP_PRPS
			inmtDao.inmtFokapoDelete(deleteMap);		// DELETE ML_FOKAPO_PRPS
			inmtDao.inmtKrtraDelete(deleteMap);			// DELETE ML_KRTRA_PRPS
			inmtDao.inmtPrpsRsltDelete(deleteMap);		// DELETE ML_PRPS_RSLT
			inmtDao.inmtPrpsDelete(deleteMap);			// DELETE ML_PRPS
			
			
			// 첨부파일삭제
			for( int fi=0; fi<ds_fileList.getDeleteRowCount(); fi++) {
				
				Map fileDelMap = getDeleteMap(ds_fileList, fi);
				inmtDao.inmtFileDelete(fileDelMap);
				
				String fileName = ds_fileList.getDeleteColumn(fi, "REAL_FILE_NAME").toString();
				
				 try 
				 {
					 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
				     if (file.exists()) 
				     { 
				    	 file.delete(); 
				     }
				  } 
				  catch (Exception e) 
				  {
				   e.printStackTrace();
				  } 
			}
		
		// 강명표 추가 START
		/*
		Map map = getMap(ds_condition, 0);
		map.put("CONN_URL", "보상금신청 삭제");
		inmtDao.insertConnInfo(map);
		*/
		// 강명표 추가 END
		
		} // end if.. 삭제처리
		
		addList("ds_maxDealStat"			, maxDealStatList);
	}
}
