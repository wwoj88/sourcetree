package kr.or.copyright.mls.adminStatProc.dao;

import java.util.List;
import java.util.Map;

public interface AdminStatProcPopupDao {

	List<String> statProcTargPopupExelDown(Map<String, Object> map) throws Exception;

	List<String> statProcPopupExelDown(Map<String, Object> map) throws Exception;
	
	List<String> statProcPopupExelDown2(Map<String, Object> map) throws Exception;

	List<String> statProcWorksPopupExelDown(Map<String, Object> map) throws Exception;
	
	List<String> statProcInfoExelDown(Map<String, Object> map) throws Exception;
	
	
}
