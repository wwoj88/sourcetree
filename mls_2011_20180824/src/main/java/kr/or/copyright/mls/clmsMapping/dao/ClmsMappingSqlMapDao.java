package kr.or.copyright.mls.clmsMapping.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class ClmsMappingSqlMapDao extends SqlMapClientDaoSupport implements ClmsMappingDao {
	
	public List kappListTotalRow(Map map) {
		return getSqlMapClientTemplate().queryForList("ClmsMapping.kappListTotalRow",map);
	}
	
	public List kappList(Map map) {
		return getSqlMapClientTemplate().queryForList("ClmsMapping.kappList",map);
	}
	
	public List komcaList(Map map) {
		return getSqlMapClientTemplate().queryForList("ClmsMapping.komcaList",map);
	}
	
	public void icnUpdate(Map map) {
		getSqlMapClientTemplate().update("ClmsMapping.icnUpdate",map);
	}
	
	public List icnListTotalRow(Map map) {
		return getSqlMapClientTemplate().queryForList("ClmsMapping.icnListTotalRow",map);
	}
	
	public List icnList(Map map) {
		return getSqlMapClientTemplate().queryForList("ClmsMapping.icnList",map);
	}
	
	public void icnDelete(Map map) {
		getSqlMapClientTemplate().update("ClmsMapping.icnDelete",map);
	}
	
}
