package kr.or.copyright.mls.adminUser.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class AdminUserSqlMapDao extends SqlMapClientDaoSupport implements AdminUserDao {

	// 회원정보 목록조회
	public List userList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.userList",map);
	}
	
	//회원정보 상세조회
	public List userDetlList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.userDetlList",map);
	}	
	
	//신탁단체 담당자목록 조회
	public List trstOrgnMgnbList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.trstOrgnMgnbList",map);
	}	

	//신탁단체 담당자 상세 조회
	public List trstOrgnMgnbDetlList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.trstOrgnMgnbDetlList",map);
	}
	
	
	//신탁단체 담당자 입력
	public void trstOrgnMgnbDetlInsert(Map map) {
		getSqlMapClientTemplate().insert("AdminUser.trstOrgnMgnbDetlInsert",map);
	}	
	
	//신탁단체 담당자 수정
	public void trstOrgnMgnbDetlUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.trstOrgnMgnbDetlUpdate",map);
	}	
	
	//담당자 ,회원 비밀번호 수정
	public void userMgnbPswdUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.userMgnbPswdUpdate",map);
	}
	
	// 그룹 메뉴 조회 하기
	public List adminGroupMenuInfo(Map map){
		return getSqlMapClientTemplate().queryForList("AdminUser.adminGroupMenuInfo",map);
	}
	
	// 관리자 아이디 중복체크
	public int adminIdCheck(Map map){
		int iUserCnt = (Integer)getSqlMapClientTemplate().queryForObject("AdminUser.adminIdCheck", map);
		return iUserCnt;
	}

	public List trstOrgnCoNameList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.trstOrgnCoNameList",map);
	}

	public void updateClmsUserInfo(Map map) {
		getSqlMapClientTemplate().update("AdminUser.updateClmsUserInfo",map);
		
	}
	
	// 대표담당자정보 수정
	public void orgnMgnbUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminUser.orgnMgnbUpdate",map);
	}	
	
	// 대표담당자 존재여부 체크
	public int orgnMgnbCheck(Map map){
		int iUserCnt = (Integer)getSqlMapClientTemplate().queryForObject("AdminUser.orgnMgnbCheck", map);
		return iUserCnt;
	}
}
