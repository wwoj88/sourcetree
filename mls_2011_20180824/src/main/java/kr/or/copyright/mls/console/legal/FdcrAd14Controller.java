package kr.or.copyright.mls.console.legal;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.legal.inter.FdcrAd14Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 법정허락 > 결제집결표
 * 
 * @author ljh
 */
@Controller
public class FdcrAd14Controller extends DefaultController{

	@Resource( name = "fdcrAd14Service" )
	private FdcrAd14Service fdcrAd14Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd14Controller.class );

	/**
	 * 결제집결표 목록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd14List1.page" )
	public String fdcrAd14List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "SCH_LGD_OID", "" );
		commandMap.put( "FROM", "" );
		commandMap.put( "TO", "" );

		fdcrAd14Service.fdcrAd14List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		System.out.println( "결제집결표 목록" );
		return "legal/fdcrAd14List1.tiles";
	}

	/**
	 * 결제집결표 엑셀다운로드
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */

	// 화면단에서 스크립트로 처리,

}
