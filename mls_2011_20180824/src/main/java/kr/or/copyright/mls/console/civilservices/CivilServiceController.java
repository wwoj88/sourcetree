package kr.or.copyright.mls.console.civilservices;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.AbstractView;

import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import kr.or.copyright.common.userLogin.controller.LoginFormController;
import kr.or.copyright.mls.adminCommon.dao.AdminCommonDao;
import kr.or.copyright.mls.console.ConsoleFileUtil;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovMultipartResolver;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.civilservices.inter.CivilServicesService;
import kr.or.copyright.mls.console.civilservices.model.CivilServicesBoardDTO;
import kr.or.copyright.mls.console.civilservices.model.CivilServicesDTO;
import kr.or.copyright.mls.console.event.FdcrAdA0Controller;
import kr.or.copyright.mls.console.event.FdcrAdA0ServiceImpl;


/**
 * 민원접수 처리 현황
 */
@Controller
public class CivilServiceController extends DefaultController{
	

     @Resource(name = "adminCommonDao")
     private AdminCommonDao adminCommonDao;
     
	@Resource( name = "CivilServicesService" )
	private CivilServicesService civilServiceService;
	//private String savepath = "D:\\Git\\mls_2011\\web\\upload\\civilserviceupload\\";
	private String savepath = "/home/right4me_test/web/upload/civilserviceupload/"; //right4me_test
	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA0Controller.class );
	
	@RequestMapping( value = "/console/civilservice/civilservice.page" )
	public String getByList( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception {
		
		System.out.println( "CivilServiceController call" );
		
		if(commandMap.get( "GUBUN" )!=null&&!commandMap.get( "GUBUN" ).equals(""))
		{
			int gubun= Integer.parseInt( (String) commandMap.get( "GUBUN" ) );
			String srchTitle= (String) commandMap.get( "SRCH_TITLE" );
			
			{
				switch(gubun){
					case 1:
						commandMap.put( "USER_NAME",  srchTitle);
						break;
					case 2:
						commandMap.put( "CIVIL_TITLE",  srchTitle);
						break;
					case 3:
						commandMap.put( "BOARD_CONTENT",  srchTitle);
						break;
					default:
						break;
				}
			}	
		}
		
		request.setAttribute( "CONSOLE_USER", request.getSession().getAttribute("CONSOLE_USER") );
		////////////////////////////////////////////////////////////////
		int pageIndex =1;
		 if(request.getParameter( "pageIndex" )!=null) {
			 pageIndex = Integer.parseInt(request.getParameter( "pageIndex" )); 
		 }
		 
		commandMap.put( "pageNum",  pageIndex);
		
		List civilServiceList = civilServiceService.selectCivilServices(commandMap);
		int civilServiceCount = civilServiceService.selectCountCivilServices();
		
		/** paging */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(pageIndex);
		paginationInfo.setRecordCountPerPage(10);
		paginationInfo.setPageSize(10);
		paginationInfo.setTotalRecordCount(civilServiceCount);
		model.addAttribute("paginationInfo", paginationInfo);
		request.setAttribute( "commandMap", commandMap );
		request.setAttribute( "ds_list", civilServiceList);
		//////////////////////////////////////////////////////////////////
		
		//통계
		
		List selectCivilStats = civilServiceService.selectCivilStats(commandMap);
		System.out.println( selectCivilStats );
		request.setAttribute( "selectCivilStats", selectCivilStats );
		return "/civilservice/civilservicelist.tiles";
	}
	
	@RequestMapping( value = "/console/civilservice/civilserviceView.page" )
	public String civilserviceView( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception {

		List returnList = civilServiceService.selectCivilServicesByCivilSeqn(commandMap);
		List returnPerson = civilServiceService.selectCivilServicesPerson((Map<String,Object>)returnList.get( 0 ));
		List returnFileList = civilServiceService.selectCivilServicesFile(commandMap);
		List commentList = civilServiceService.selectCivilServicesComment(commandMap);

		
	  String USER_IDNT = request.getParameter("USER_IDNT");
    HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    String ip = req.getHeader("X-FORWARDED-FOR");
    if (ip == null) {
         ip = req.getRemoteAddr();
    }
    Map logMap = new HashMap();
    System.out.println(request.getRequestURI());

    logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
    logMap.put("PROC_STATUS", "민원접수처리현황" );
    logMap.put("PROC_ID",  "민원접수처리현황 상세조회");
    logMap.put("MENU_URL", request.getRequestURI());
    logMap.put("IP_ADDRESS", ip);

    adminCommonDao.insertAdminLogDo(logMap);
		
		
		request.setAttribute( "returnPerson", returnPerson );
		request.setAttribute( "returnList", returnList );
		System.out.println( "returnList : " + returnList);
		request.setAttribute( "returnFileList", returnFileList );
		request.setAttribute( "commentList", commentList );
		request.setAttribute( "CONSOLE_USER", request.getSession().getAttribute("CONSOLE_USER") );
		return "/civilservice/civilserviceView.tiles";
	}
	
	@RequestMapping( value = "/console/civilservice/civilservicePopup.page" )
	public String openPopup( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception {
		
		request.setAttribute( "CONSOLE_USER", request.getSession().getAttribute("CONSOLE_USER") );
		
		return "/civilservice/civilservicelistInsertWindow.popup";
	}

	@RequestMapping( value = "/console/civilservice/srchPerformersIdnt.page")
	public void srchPerformersIdnt( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception {

		String performersIdnt = (String)commandMap.get( "performersIdnt" );
		
		response.setCharacterEncoding( "UTF-8" );
		PrintWriter out = response.getWriter();
		out.print( performersIdnt );
	}
	
	
	
	@RequestMapping( value = "/console/civilservice/commentCivilServiceInsert.page" )
	public String commentCivilServiceInsert( ModelMap model, 
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response 
		) throws Exception {
		
		String homeNumber2 = (String)commandMap.get( "homeNumber2" ); 
		Map consoleUser = (Map) request.getSession().getAttribute("CONSOLE_USER");
		Date date = new Date();

		commandMap.put( "answRgstIdnt",  consoleUser.get( "USER_ID" ));
		commandMap.put("answRgstDttm",date);
	
		civilServiceService.updateCivilServiceState(commandMap);
		
		List returnList = civilServiceService.selectCivilServicesComment( commandMap );
		
		if(returnList.size()==0)
		{
			commandMap.put( "civilTitle","comment");
			commandMap.put( "pswd","1234");
			commandMap.put( "civilCategory","0");
			commandMap.put( "civilKind","0");
			commandMap.put( "inqrCont","0");
			commandMap.put( "rgstIdnt",consoleUser.get( "USER_ID" ));
			commandMap.put( "commentSeqnNum", commandMap.get( "civilSeqn" ) );
			commandMap.put( "boardContent", commandMap.get( "boardContent" ) );
			commandMap.put( "civilDeth", "1" );
			commandMap.put( "rgstIdnt",  consoleUser.get( "USER_ID" ));
			
			civilServiceService.insertCivilServices( commandMap );
		}else {
			commandMap.put( "civilTitle","comment");
			commandMap.put( "pswd","1234");
			commandMap.put( "civilCategory","0");
			commandMap.put( "civilKind","0");
			commandMap.put( "inqrCont","0");
			commandMap.put( "rgstIdnt",consoleUser.get( "USER_ID" ));
			commandMap.put( "commentSeqnNum", commandMap.get( "civilSeqn" ) );
			commandMap.put( "boardContent", commandMap.get( "boardContent" ) );
			commandMap.put( "civilDeth", "1" );
			commandMap.put( "rgstIdnt",  consoleUser.get( "USER_ID" ));
			
			civilServiceService.updateCivilServicesComment( commandMap );
		}
		
		 String USER_IDNT = request.getParameter("USER_IDNT");
	    HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	    String ip = req.getHeader("X-FORWARDED-FOR");
	    if (ip == null) {
	         ip = req.getRemoteAddr();
	    }
	    Map logMap = new HashMap();
	    System.out.println(request.getRequestURI());

	    logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
	    logMap.put("PROC_STATUS", "민원접수처리현황" );
	    logMap.put("PROC_ID",  "민원접수처리현황 수정");
	    logMap.put("MENU_URL", request.getRequestURI());
	    logMap.put("IP_ADDRESS", ip);

	    adminCommonDao.insertAdminLogDo(logMap);
    
    
		
		return "forward:/console/civilservice/civilservice.page";
	}
	
	@RequestMapping( value = "/console/civilservice/civilserviceInsert.page")
	public String civilserviceInsert( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		MultipartHttpServletRequest mreq,
		HttpServletResponse response ) throws Exception {
		
		if(commandMap.get( "SERVICE_REQUESTER_COMPANY" )==null||commandMap.get( "SERVICE_REQUESTER_COMPANY" ).equals( "" )) {
			commandMap.put( "SERVICE_REQUESTER_COMPANY", commandMap.get( "SERVICE_REQUESTER_COMPANY_KR" ) );
		}
		
		if(commandMap.get( "SERVICE_REQUESTER" )==null||commandMap.get( "SERVICE_REQUESTER" ).equals( "" )) {
			commandMap.put( "SERVICE_REQUESTER", commandMap.get( "SERVICE_REQUESTER_KR" ) );
		}
		
		if(!commandMap.get( "homeNumber2" ).equals( "" )|| commandMap.get( "homeNumber2" )!=null) {
			String homeNumber = commandMap.get( "homeNumber1" )+"-"+commandMap.get( "homeNumber2" )+"-"+commandMap.get( "homeNumber3" );
			commandMap.put( "homeNumber", homeNumber );
		}
		
		String phoneNumber = commandMap.get( "phoneNumber1" )+"-"+commandMap.get( "phoneNumber2" )+"-"+commandMap.get( "phoneNumber3" );
		
		
		commandMap.put( "phoneNumber", phoneNumber );

		civilServiceService.insertCivilServices( commandMap );
		String civilSeqn = civilServiceService.civilServicesFromCivilSeqn();
		List returnList = fileUploadCivilServices( mreq, savepath, false);
		
		List<CivilServicesDTO> CivilServicesDTOList = autoSettingFileDto(civilSeqn, returnList);	
		if(CivilServicesDTOList.size() != 0)
		{
			for(int i = 0 ; i < CivilServicesDTOList.size() ; i++) {
				civilServiceService.insertCivilServicesFile(CivilServicesDTOList.get( i ));
			}
		}

		request.setAttribute( "CONSOLE_USER", request.getSession().getAttribute("CONSOLE_USER") );
		request.setAttribute( "colseFlag", "colseFlag" );

		return "/civilservice/civilservicelistInsertWindow.popup";
	}
	
	public List<CivilServicesDTO> autoSettingFileDto(String civilSeqn,List fileList) {
		List<CivilServicesDTO> civilServicesDTOList = new ArrayList<CivilServicesDTO>(); 
	
		for(int i = 0 ; i < fileList.size() ; i++) {
			CivilServicesDTO civilServicesDTO = new CivilServicesDTO();
			civilServicesDTO.setCivilSeqn( Integer.parseInt( civilSeqn ) );
			Map map = (Map)fileList.get(i);
			civilServicesDTO.setNewFileName((String)map.get( "F_fileName" ));
			civilServicesDTO.setOrgFileName((String)map.get( "F_orgFileName" ));
			civilServicesDTOList.add( civilServicesDTO );
		}
		
		return civilServicesDTOList;
	}
	@RequestMapping( value = "/console/civilservice/civilserviceFileDown.page")
	public void civilserviceFileDown( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response) throws IOException {

		List returnFile = civilServiceService.selectCivilServicesDownLoadFile(commandMap);

		Map returnMap = (HashMap)returnFile.get( 0 );
		String newFileName = (String)returnMap.get( "newFileName" );
		String orgFileName = (String)returnMap.get( "orgFileName" );
		
		ConsoleFileUtil fileDwonUtil = new ConsoleFileUtil();
		fileDwonUtil.FileDownLoad( savepath, newFileName, orgFileName, request, response );
	}
	
	@RequestMapping( value = "/console/civilservice/civilserviceExcelDown.page")
	public String civilserviceExcelDown( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response) throws IOException {

		List returnList = civilServiceService.selectCivilServicesExcelList( commandMap );
		model.addAttribute( "ds_list", returnList);

		return "/civilservice/civilservicelistDown";
	}	
	
	@RequestMapping( value = "/console/civilservice/civilserviceDelete.page")
	public String civilserviceDelete( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response) throws IOException {

		List<Map> returnList = civilServiceService.selectCivilServicesFile(commandMap);

		if(returnList.size()!=0) {
			civilServiceService.deleteCivilServicesFile(returnList);
			//실 파일 삭제
			for(int i = 0 ;  i < returnList.size() ; i++)
			{
				System.out.println( returnList.get( i ).get( "newFileName" ) );
				File file = new File( savepath+returnList.get( i ).get( "newFileName" ));
				file.delete();
			}
		}
		Map consoleUser= (HashMap)request.getSession().getAttribute("CONSOLE_USER");
		Date date = new Date();
		
		commandMap.put( "civilState", "1" );
		commandMap.put( "answRgstIdnt", consoleUser.get( "USER_ID" ) );
		commandMap.put( "answRgstDttm", date );
		
		civilServiceService.updateCivilServiceState(commandMap);
		
		List commentList = civilServiceService.selectCivilServicesComment(commandMap);
		if(commentList.size()!=0) {
			civilServiceService.deleteCivilServicesComment(commentList);
		}
		
		List<Map> deleteList = civilServiceService.selectCivilServicesByCivilSeqn(commandMap);
		civilServiceService.deleteCivilServicesByCivilSeqn(deleteList);
		
		return "forward:/console/civilservice/civilservice.page";
	}
	
	
	@RequestMapping( value ="/console/civilservice/deleteCivilserviceComment.page")
	public String deleteCivilserviceComment( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response) throws IOException {

		List<Map> deleteList = new ArrayList<Map>();
		deleteList.add( commandMap );
		
		civilServiceService.deleteCivilServicesComment(deleteList);
		civilServiceService.deleteCivilServicesState(deleteList);
		
		return "forward:/console/civilservice/civilserviceView.page";
	}
	
	@RequestMapping( value = "/console/civilservice/personSrchPopup.page" )
	public String personSrchPopup( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception {
		
		return "/civilservice/personSrchWindow.popup";
	}
	
	
	@RequestMapping( value = "/console/civilservice/personSrch.page" )
	public String companySrch( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception {

		List returnPersonList = civilServiceService.personSrch(commandMap);
		request.setAttribute( "returnList", returnPersonList );

		return "/civilservice/personSrchWindow.popup";
	}
}
