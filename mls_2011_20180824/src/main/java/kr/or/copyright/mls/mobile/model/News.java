package kr.or.copyright.mls.mobile.model;

public class News {
	private int 
		crId
	;
	
	private String
		title
		, articlPubcSdate
		, articlPubcTime
		, providerNm
		, linkPageUrl
		, trustYnStr
	;

	public int getCrId() {
		return crId;
	}

	public void setCrId(int crId) {
		this.crId = crId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArticlPubcSdate() {
		return articlPubcSdate;
	}

	public void setArticlPubcSdate(String articlPubcSdate) {
		this.articlPubcSdate = articlPubcSdate;
	}

	public String getArticlPubcTime() {
		return articlPubcTime;
	}

	public void setArticlPubcTime(String articlPubcTime) {
		this.articlPubcTime = articlPubcTime;
	}

	public String getProviderNm() {
		return providerNm;
	}

	public void setProviderNm(String providerNm) {
		this.providerNm = providerNm;
	}

	public String getLinkPageUrl() {
		return linkPageUrl;
	}

	public void setLinkPageUrl(String linkPageUrl) {
		this.linkPageUrl = linkPageUrl;
	}

	public String getTrustYnStr() {
		return trustYnStr;
	}

	public void setTrustYnStr(String trustYnStr) {
		this.trustYnStr = trustYnStr;
	}
		
}
