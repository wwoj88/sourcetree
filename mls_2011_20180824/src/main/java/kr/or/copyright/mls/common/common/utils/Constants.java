package kr.or.copyright.mls.common.common.utils;

public class Constants {

	public static final int DEFAULT_ROW_PER_PAGE = 10;
	public static final int DEFAULT_PAGE_NO = 1;
	
	public final static String INSERT = "insert";
	public final static String UPDATE = "update";
	public final static String DELETE = "delete";
}

