package kr.or.copyright.mls.srch.model;

public class SrchComm {
	private String genreCd;
	private String genreCdName;
	private int worksId;
	private String worksTitle;
	private int pageNo;
	private int startNo;
	private int endNo;
	private String trstOrgnCodeName;
	
	//음악
	private String albumTitle;  		//앨범명
	private String comp;				//작곡가
	private String albumIssuYear;		//앨범발매년도
	private String lyrc;				//작사가
	private String sing;				//가수명
	private String perf;				//연주자
	private String arra;				//편곡가
	private String tran;				//역사가
	private String prod;				//음반제작자
	//어문
	private String bookTitle;			//도서명
	private String bookPublisher;		//출판사
	private String bookIssuYear;		//발행연도
	private String coptName;			//저작자
	private String worksSubTitle;		//부제
	private String crtYear;				//창작연도
	//방송대본
	private String scrtGenreCd;			//장르
	private String scrpSubjCd;			//소재
	private String scrpSubjCd2;			//소재2
	private String worksOrigTitle;		//원작명
	private String OrigWriter;			//원작작가
	private String bordDate;			//방송일자
	private String brodOrdSeq;			//방송회차
	private String player;				//출연진
	private String makeCpy;				//제작사
	private String writer;				//작가
	private String director;			//연출자
	//영화
	private String direct;				//감독
	private String Producer;			//제작자
	private String distributor;			//배급사
	private String investor;			//투자자
	//방송
	private String bordGenreCd;			//장르
	private String makeYear;			//제작연도
	//뉴스
	private String articlDate;			//기사일자
	private String papeNo;				//지면번호
	private String papeKind;			//지면면종
	private String contributor;			//기고자
	private String repoter;				//기자
	private String provider;			//언론사
	//미술
	private String Kind;				//분류
	private String sourceInfo;			//출처
	private String mainMtrl;			//주재료
	private String txtr;				//구조및특징
	private String possOrgnName;		//소장기관명
	private String possDate;			//소장년월일
	//기타
	private String sideGenreCdName;		//저작물종류
	private String publMedi;			//공표매체
	private String publDate;			//공표일자
	private String makeDate;
	public String getGenreCd() {
		return genreCd;
	}
	public void setGenreCd(String genreCd) {
		this.genreCd = genreCd;
	}
	public String getGenreCdName() {
		return genreCdName;
	}
	public void setGenreCdName(String genreCdName) {
		this.genreCdName = genreCdName;
	}
	public int getWorksId() {
		return worksId;
	}
	public void setWorksId(int worksId) {
		this.worksId = worksId;
	}
	public String getWorksTitle() {
		return worksTitle;
	}
	public void setWorksTitle(String worksTitle) {
		this.worksTitle = worksTitle;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getStartNo() {
		return startNo;
	}
	public void setStartNo(int startNo) {
		this.startNo = startNo;
	}
	public int getEndNo() {
		return endNo;
	}
	public void setEndNo(int endNo) {
		this.endNo = endNo;
	}
	public String getAlbumTitle() {
		return albumTitle;
	}
	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}
	public String getComp() {
		return comp;
	}
	public void setComp(String comp) {
		this.comp = comp;
	}
	public String getAlbumIssuYear() {
		return albumIssuYear;
	}
	public void setAlbumIssuYear(String albumIssuYear) {
		this.albumIssuYear = albumIssuYear;
	}
	public String getLyrc() {
		return lyrc;
	}
	public void setLyrc(String lyrc) {
		this.lyrc = lyrc;
	}
	public String getSing() {
		return sing;
	}
	public void setSing(String sing) {
		this.sing = sing;
	}
	public String getPerf() {
		return perf;
	}
	public void setPerf(String perf) {
		this.perf = perf;
	}
	public String getArra() {
		return arra;
	}
	public void setArra(String arra) {
		this.arra = arra;
	}
	public String getTran() {
		return tran;
	}
	public void setTran(String tran) {
		this.tran = tran;
	}
	public String getProd() {
		return prod;
	}
	public void setProd(String prod) {
		this.prod = prod;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getBookPublisher() {
		return bookPublisher;
	}
	public void setBookPublisher(String bookPublisher) {
		this.bookPublisher = bookPublisher;
	}
	public String getBookIssuYear() {
		return bookIssuYear;
	}
	public void setBookIssuYear(String bookIssuYear) {
		this.bookIssuYear = bookIssuYear;
	}
	public String getCoptName() {
		return coptName;
	}
	public void setCoptName(String coptName) {
		this.coptName = coptName;
	}
	public String getWorksSubTitle() {
		return worksSubTitle;
	}
	public void setWorksSubTitle(String worksSubTitle) {
		this.worksSubTitle = worksSubTitle;
	}
	public String getCrtYear() {
		return crtYear;
	}
	public void setCrtYear(String crtYear) {
		this.crtYear = crtYear;
	}
	public String getScrtGenreCd() {
		return scrtGenreCd;
	}
	public void setScrtGenreCd(String scrtGenreCd) {
		this.scrtGenreCd = scrtGenreCd;
	}
	public String getScrpSubjCd() {
		return scrpSubjCd;
	}
	public void setScrpSubjCd(String scrpSubjCd) {
		this.scrpSubjCd = scrpSubjCd;
	}
	public String getScrpSubjCd2() {
		return scrpSubjCd2;
	}
	public void setScrpSubjCd2(String scrpSubjCd2) {
		this.scrpSubjCd2 = scrpSubjCd2;
	}
	public String getWorksOrigTitle() {
		return worksOrigTitle;
	}
	public void setWorksOrigTitle(String worksOrigTitle) {
		this.worksOrigTitle = worksOrigTitle;
	}
	public String getOrigWriter() {
		return OrigWriter;
	}
	public void setOrigWriter(String origWriter) {
		OrigWriter = origWriter;
	}
	public String getBordDate() {
		return bordDate;
	}
	public void setBordDate(String bordDate) {
		this.bordDate = bordDate;
	}
	public String getBrodOrdSeq() {
		return brodOrdSeq;
	}
	public void setBrodOrdSeq(String brodOrdSeq) {
		this.brodOrdSeq = brodOrdSeq;
	}
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player = player;
	}
	public String getMakeCpy() {
		return makeCpy;
	}
	public void setMakeCpy(String makeCpy) {
		this.makeCpy = makeCpy;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getDirect() {
		return direct;
	}
	public void setDirect(String direct) {
		this.direct = direct;
	}
	public String getProducer() {
		return Producer;
	}
	public void setProducer(String producer) {
		Producer = producer;
	}
	public String getDistributor() {
		return distributor;
	}
	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}
	public String getInvestor() {
		return investor;
	}
	public void setInvestor(String investor) {
		this.investor = investor;
	}
	public String getBordGenreCd() {
		return bordGenreCd;
	}
	public void setBordGenreCd(String bordGenreCd) {
		this.bordGenreCd = bordGenreCd;
	}
	public String getMakeYear() {
		return makeYear;
	}
	public void setMakeYear(String makeYear) {
		this.makeYear = makeYear;
	}
	public String getArticlDate() {
		return articlDate;
	}
	public void setArticlDate(String articlDate) {
		this.articlDate = articlDate;
	}
	public String getPapeNo() {
		return papeNo;
	}
	public void setPapeNo(String papeNo) {
		this.papeNo = papeNo;
	}
	public String getPapeKind() {
		return papeKind;
	}
	public void setPapeKind(String papeKind) {
		this.papeKind = papeKind;
	}
	public String getContributor() {
		return contributor;
	}
	public void setContributor(String contributor) {
		this.contributor = contributor;
	}
	public String getRepoter() {
		return repoter;
	}
	public void setRepoter(String repoter) {
		this.repoter = repoter;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getKind() {
		return Kind;
	}
	public void setKind(String kind) {
		Kind = kind;
	}
	public String getSourceInfo() {
		return sourceInfo;
	}
	public void setSourceInfo(String sourceInfo) {
		this.sourceInfo = sourceInfo;
	}
	public String getMainMtrl() {
		return mainMtrl;
	}
	public void setMainMtrl(String mainMtrl) {
		this.mainMtrl = mainMtrl;
	}
	public String getTxtr() {
		return txtr;
	}
	public void setTxtr(String txtr) {
		this.txtr = txtr;
	}
	public String getPossOrgnName() {
		return possOrgnName;
	}
	public void setPossOrgnName(String possOrgnName) {
		this.possOrgnName = possOrgnName;
	}
	public String getPossDate() {
		return possDate;
	}
	public void setPossDate(String possDate) {
		this.possDate = possDate;
	}
	public String getSideGenreCdName() {
		return sideGenreCdName;
	}
	public void setSideGenreCdName(String sideGenreCdName) {
		this.sideGenreCdName = sideGenreCdName;
	}
	public String getPublMedi() {
		return publMedi;
	}
	public void setPublMedi(String publMedi) {
		this.publMedi = publMedi;
	}
	public String getPublDate() {
		return publDate;
	}
	public void setPublDate(String publDate) {
		this.publDate = publDate;
	}
	public String getMakeDate() {
		return makeDate;
	}
	public void setMakeDate(String makeDate) {
		this.makeDate = makeDate;
	}

	public String getTrstOrgnCodeName() {
		return trstOrgnCodeName;
	}
	public void setTrstOrgnCodeName(String trstOrgnCodeName) {
		this.trstOrgnCodeName = trstOrgnCodeName;
	}
	



	
}
