package kr.or.copyright.mls.console.rightreqst;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 저작권찾기 신청 > 저작권찾기 신청 목록
 * 
 * @author ljh
 */
@Controller
public class FdcrAd35Controller extends DefaultController{

	@Resource( name = "fdcrAd35Service" )
	private FdcrAd35ServiceImpl fdcrAd35Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd35Controller.class );

	/**
	 * 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rightreqst/fdcrAd35List1.page" )
	public String fdcrAd35List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "GUBUN1", "" );
		commandMap.put( "GUBUN2", "" );
		commandMap.put( "GUBUN3", "" );
		commandMap.put( "GUBUN4", "" );
		commandMap.put( "PRPS_TITE", "" );
		commandMap.put( "USER", "" );
		commandMap.put( "DEAL_STAT", "" );
		commandMap.put( "START_RGST_DTTM", "" );
		commandMap.put( "END_RGST_DTTM", "" );
		commandMap.put( "TRST_ORGN_CODE", "" );
		commandMap.put( "FROM_NO", "" );
		commandMap.put( "TO_NO", "" );
		commandMap.put( "DIVS", "10" );

		fdcrAd35Service.fdcrAd35List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		//model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		System.out.println( "신청 목록 조회" );
		return "rightreqst/fdcrAd35List1.tiles";
	}

	/**
	 * 상세 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rightreqst/fdcrAd35View1.page" )
	public String fdcrAd35View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "USER_IDNT", "" );
		commandMap.put( "PRPS_IDNT", "" );
		commandMap.put( "PRPS_DIVS", "" );
		commandMap.put( "PRPS_SEQN", "" );
		commandMap.put( "TRST_ORGN_CODE", "" );
		commandMap.put( "PRPS_MAST_KEY", "" ); // 유형

		fdcrAd35Service.fdcrAd35View1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_fileList", commandMap.get( "ds_fileList" ) );
		model.addAttribute( "ds_works", commandMap.get( "ds_works" ) );
		model.addAttribute( "ds_temp", commandMap.get( "ds_temp" ) );

		System.out.println( "상세 조회" );
		return "test";
	}

	/**
	 * 신청인 정보 조회
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rightreqst/fdcrAd35View2.page" )
	public String fdcrAd35View2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "USER_IDNT", "" );

		fdcrAd35Service.fdcrAd35View2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "신청자 정보 조회" );
		return "test";
	}

	/**
	 * 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rightreqst/fdcrAd35Download1.page" )
	public void fdcrAd35Download1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "PRPS_MAST_KEY", "" );

		fdcrAd35Service.fdcrAd35View1( commandMap );

		List list = (List) commandMap.get( "ds_fileList" );
		for( int i = 0; i < list.size(); i++ ){
			Map map = (Map) list.get( i );
			String prpsMastKey = (String) map.get( "PRPS_MAST_KEY" );
			if( prpsMastKey.equals( commandMap.get( "PRPS_MAST_KEY" ) ) ){
				String filePath = (String) map.get( "FILE_PATH" );
				String fileName = (String) map.get( "FILE_NAME" );
				String realFileName = (String) map.get( "REAL_FILE_NAME" );
				download( request, response, filePath + realFileName, fileName );
			}
		}
	}

	/**
	 * 신청저작물 상세정보 팝업
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rightreqst/fdcrAd35Pop1.page" )
	public String fdcrAd35Pop1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "PRPS_DIVS", "" );
		commandMap.put( "PRPS_IDNT", "" );
		commandMap.put( "PRPS_IDNT_NR", "" );
		commandMap.put( "PRPS_IDNT_ME", "" );

		fdcrAd35Service.fdcrAd35Pop1( commandMap );
		model.addAttribute( "ds_works_info", commandMap.get( "ds_works_info" ) );
		System.out.println( "신청저작물 상세정보 팝업" );
		return "test";
	}

	/**
	 * 처리결과 삭제
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rightreqst/fdcrAd35Delete1.page" )
	public void fdcrAd35Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "TRST_ORGN_CODE", "" );

		String[] PRPS_MAST_KEYS = request.getParameterValues( "PRPS_MAST_KEY" );
		String[] PRPS_SEQNS = request.getParameterValues( "PRPS_SEQN" );
		String[] ATTC_SEQNS = request.getParameterValues( "ATTC_SEQN" );
		String[] REAL_FILE_NAMES = request.getParameterValues( "REAL_FILE_NAME" );
		commandMap.put( "PRPS_MAST_KEY", PRPS_MAST_KEYS );
		commandMap.put( "PRPS_SEQN", PRPS_SEQNS );
		commandMap.put( "ATTC_SEQN", ATTC_SEQNS );
		commandMap.put( "REAL_FILE_NAME", REAL_FILE_NAMES );

		boolean isSuccess = fdcrAd35Service.fdcrAd35Delete1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "처리결과 삭제" );
	}

	/**
	 * 처리결과 저장
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rightreqst/fdcrAd35Insert1.page" )
	public void fdcrAd35Insert1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "GUBUN_TRST_ORGN_CODE", "" );
		commandMap.put( "userId", "" );

		String[] PRPS_MAST_KEYS = request.getParameterValues( "PRPS_MAST_KEY" );
		String[] PRPS_IDNTS = request.getParameterValues( "PRPS_IDNT" );
		String[] PRPS_DIVSS = request.getParameterValues( "PRPS_DIVS" );
		String[] TRST_ORGN_CODES = request.getParameterValues( "TRST_ORGN_CODE" );
		String[] RGHT_ROLE_CODES = request.getParameterValues( "RGHT_ROLE_CODE" );

		commandMap.put( "PRPS_MAST_KEY", PRPS_MAST_KEYS );
		commandMap.put( "PRPS_IDNT", PRPS_IDNTS );
		commandMap.put( "PRPS_DIVS", PRPS_DIVSS );
		commandMap.put( "TRST_ORGN_CODE", TRST_ORGN_CODES );
		commandMap.put( "RGHT_ROLE_CODE", RGHT_ROLE_CODES );

		boolean isSuccess = fdcrAd35Service.fdcrAd35Insert1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "처리결과 저장" );
	}
}
