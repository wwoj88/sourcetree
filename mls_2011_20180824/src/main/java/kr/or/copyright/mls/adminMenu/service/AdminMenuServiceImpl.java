package kr.or.copyright.mls.adminMenu.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import kr.or.copyright.mls.adminMenu.dao.AdminMenuDao;
import kr.or.copyright.mls.common.service.BaseService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.tobesoft.platform.data.Dataset;

public class AdminMenuServiceImpl extends BaseService implements AdminMenuService{

	private Log logger = LogFactory.getLog(getClass());

	private AdminMenuDao adminMenuDao;
	
	public void setAdminMenuDao(AdminMenuDao adminMenuDao){
		this.adminMenuDao = adminMenuDao;
	}
	
	// 메뉴 목록조회
	public void selectMenuList() throws Exception{
		
		// DAO호출
		List list = (List) adminMenuDao.selectMenuList();
		addList("ds_menu_info", list);
		
	}
	
	// 메뉴 저장처리 
	public void menuUpdate() throws Exception{
		
//		try{
//			daoMgr.startTransaction();
		
			Dataset ds_menu_info = getDataset("ds_menu_info");   
			Dataset gds_common = getDataset("gds_common");
			
			// 수정 아이디 세팅
			Map ssMap = getMap(gds_common, 0);
			String managerId = ssMap.get("USER_ID").toString();
	
			String row_status = null;
			
			// 삭제 처리
			for(int i = 0; i < ds_menu_info.getDeleteRowCount(); i++){
				
				Map deleteMap = getDeleteMap(ds_menu_info, i );
				
				adminMenuDao.menuDelete(deleteMap);
					
			}
			
			// 추가, 수정 처리
			for(int i = 0; i < ds_menu_info.getRowCount(); i++){
				
				row_status = ds_menu_info.getRowStatus(i);
				
				Map map =  getMap(ds_menu_info, i);
				
				if(row_status.equals("insert") == true){
	
					map.put("RGST_IDNT", managerId);
					map.put("MODI_IDNT", managerId);
					
					String menuUrl = map.get("MENU_URL").toString();
					StringTokenizer st = new StringTokenizer(menuUrl, "/");
					
					ArrayList aList = new ArrayList();
					while(st.hasMoreTokens()){
						
						aList.add(st.nextToken());
	
					}
					if(aList.size()==2) {
						map.put("PREFIX", aList.get(0));
						map.put("FORMID", aList.get(1));
					} else if(aList.size()==1) {
						map.put("PREFIX", aList.get(0));
						map.put("FORMID", "");
					} else {
						map.put("PREFIX", "");
						map.put("FORMID", "");
					}
					
					adminMenuDao.menuInsert(map);
					
				}else if(row_status.equals("update") == true){
					
					map.put("MODI_IDNT", managerId);
					
					String menuUrl = map.get("MENU_URL").toString();
					StringTokenizer st = new StringTokenizer(menuUrl, "/");
					
					ArrayList bList = new ArrayList();
					while(st.hasMoreTokens()){
						
						bList.add(st.nextToken());
	
					}
					if(bList.size()==2) {
						map.put("PREFIX", bList.get(0));
						map.put("FORMID", bList.get(1));
					} else if(bList.size()==1) {
						map.put("PREFIX", bList.get(0));
						map.put("FORMID", "");
					} else {
						map.put("PREFIX", "");
						map.put("FORMID", "");
					}
					
					adminMenuDao.menuUpdate(map);
					
				}
			}
			
//			daoMgr.commitTransaction();
//		}finally{
//			daoMgr.endTransaction();
//			logger.debug("###### menuInfo Insert Failed ######### ");
//		}
		
	}
	
	// 그룹 목록 조회
	public void selectGroupList() throws Exception{
		
		List list = (List)adminMenuDao.selectGroupList(); 
		
		addList("ds_group_info", list);
		
	}
	
	// 그룹 메뉴 조회하기
	public void groupMenuInfo() throws Exception{
		
		Dataset ds_group_info = getDataset("ds_group_info");
		
		Map map = getMap(ds_group_info, 0);
		
		List groupList = (List)adminMenuDao.groupInfo(map);
		
		List groupMenuList =adminMenuDao.groupMenuInfo(map);
		
		addList("ds_group_info", groupList);
		addList("ds_menu_info", groupMenuList);
	}
	
	// 그룹  등록하기
	public void groupInsert() throws Exception{
		
		try{
//			daoMgr.startTransaction();
//			
			Dataset ds_group_info = getDataset("ds_group_info");
			Dataset ds_group_menu = getDataset("ds_group_menu");
			
			Map groupMap = getMap(ds_group_info, 0);
			
			int groupId = adminMenuDao.groupIdMax(groupMap);
logger.debug("그룹 아이디 값 확인 : " + groupId);		
			//ds_group_menu.printDataset();			
	
			groupMap.put("GROUP_SEQ", groupId);
			
			adminMenuDao.groupInsert(groupMap);
		
logger.debug("##########선택 된 메뉴 개수 확인 : " + ds_group_menu.getRowCount());			
			for(int i=0; i < ds_group_menu.getRowCount(); i++){
				
				Map groupMenuMap = getMap(ds_group_menu,i);
				groupMenuMap.put("GROUP_SEQ", groupId);
			
				adminMenuDao.groupMenuInsert(groupMenuMap);
				
			}
		}catch(Exception e){
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
//			
//			daoMgr.commitTransaction();
//		}finally{
//			daoMgr.endTransaction();
//			logger.debug("###### menuInfo Insert Failed ######### ");
//		}
		
	}
	
	// 그룹, 그룹메뉴 삭제하기
	public void groupDelete() throws Exception{
		
//		try{
//			daoMgr.startTransaction();
//			
			Dataset ds_group_info = getDataset("ds_group_info");
			
			Map groupMap = getDeleteMap(ds_group_info, 0);
			
			adminMenuDao.groupDelete(groupMap);
			adminMenuDao.groupMenuDelete(groupMap);
		
//			daoMgr.commitTransaction();
//		}finally{
//			daoMgr.endTransaction();
//		}
		
	}
	
	// 그룹, 그룹메뉴 수정하기
	public void groupMenuUpdate() throws Exception{
		
//		 try{
//				daoMgr.startTransaction();
//				
			Dataset ds_group_info = getDataset("ds_group_info");
			Dataset ds_group_menu = getDataset("ds_group_menu");
			
			Map groupMap = getMap(ds_group_info,0);
			
		    adminMenuDao.groupUpdate(groupMap);
		    adminMenuDao.groupMenuDelete(groupMap);	// 조건에 맞는 그룹메뉴 삭제하기 
		    
		    // 그룹메뉴 다시 등록하기
	   
logger.debug("##########선택 된 메뉴 개수 확인 : " + ds_group_menu.getRowCount());			
			for(int i=0; i < ds_group_menu.getRowCount(); i++){
				
				Map groupMenuMap = getMap(ds_group_menu,i);
			
				adminMenuDao.groupMenuInsert(groupMenuMap);
				
			}
			
//			daoMgr.commitTransaction();
//		}finally{
//			daoMgr.endTransaction();
//		}
		
	}
	
}
