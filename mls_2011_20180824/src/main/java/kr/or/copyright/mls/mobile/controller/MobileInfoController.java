package kr.or.copyright.mls.mobile.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.mobile.model.*;
import kr.or.copyright.mls.mobile.service.MobileInfoService;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class MobileInfoController extends MultiActionController {
	private MobileInfoService mobileInfoService;
	private static int pageSize = 10; 
	
	public void list(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
	}
	
	public ModelAndView musicList(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		HashMap<String,Object> searchWord = new HashMap<String,Object>();
		String keyWord = ServletRequestUtils.getStringParameter(request, "keyWord");
		searchWord.put("keyWord", keyWord);
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		searchWord.put("pageNo", pageNo);
		List<Music> list = mobileInfoService.musicList(searchWord);
		int totalCnt;
		if(ServletRequestUtils.getStringParameter(request, "keyWord")!=null && ServletRequestUtils.getStringParameter(request, "keyWord")!="") {
			totalCnt =  mobileInfoService.musicListCnt(searchWord);
		} else {
			totalCnt = 100;
		}
		ModelAndView mav = new ModelAndView();
		Map<String,Object> viewObject = new HashMap<String,Object>();
		viewObject.put("nowPage", pageNo);
		viewObject.put("keyWord", keyWord);
		viewObject.put("totalCnt", totalCnt);
		mav.setViewName("mobile/info/music/musicList");
		mav.addObject("viewObject", viewObject);
		mav.addObject("musicList", list);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView musicDetl(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		HashMap<String,Object> searchWord = new HashMap<String, Object>();
		searchWord.put("crId", ServletRequestUtils.getIntParameter(request, "crId"));
		searchWord.put("nrId", ServletRequestUtils.getIntParameter(request, "nrId"));
		searchWord.put("albumId", ServletRequestUtils.getIntParameter(request, "albumId"));
		
		Music music = mobileInfoService.musicDetl(searchWord);
		mav.setViewName("mobile/info/music/musicDetl");
		mav.addObject("musicDetl", music);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	
	public ModelAndView bookList(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		HashMap<String,Object> searchWord = new HashMap<String,Object>();
		String keyWord = ServletRequestUtils.getStringParameter(request, "keyWord");
		searchWord.put("keyWord", keyWord);
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		searchWord.put("pageNo", pageNo);
		List<Book> list = mobileInfoService.bookList(searchWord);
		int totalCnt;
		if(ServletRequestUtils.getStringParameter(request, "keyWord")!=null && ServletRequestUtils.getStringParameter(request, "keyWord")!="") {
			totalCnt =  mobileInfoService.bookListCnt(searchWord);
		} else {
			totalCnt = 100;
		}
		ModelAndView mav = new ModelAndView();
		Map<String,Object> viewObject = new HashMap<String,Object>();
		viewObject.put("nowPage", pageNo);
		viewObject.put("keyWord", ServletRequestUtils.getStringParameter(request, "keyWord"));
		viewObject.put("totalCnt", totalCnt);
		mav.setViewName("mobile/info/books/bookList");
		mav.addObject("bookList", list);
		mav.addObject("viewObject", viewObject);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView bookDetl(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		HashMap<String,Object> searchWord = new HashMap<String, Object>();
		searchWord.put("crId", ServletRequestUtils.getIntParameter(request, "crId"));
		
		Book book = mobileInfoService.bookDetl(searchWord);
		mav.setViewName("mobile/info/books/bookDetl");
		mav.addObject("bookDetl", book);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView newsList(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		HashMap<String,Object> searchWord = new HashMap<String,Object>();
		String keyWord = ServletRequestUtils.getStringParameter(request, "keyWord");
		searchWord.put("keyWord", keyWord);
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		searchWord.put("pageNo", pageNo);
		List<News> list = mobileInfoService.newsList(searchWord);
		int totalCnt;
		if(ServletRequestUtils.getStringParameter(request, "keyWord")!=null && ServletRequestUtils.getStringParameter(request, "keyWord")!="") {
			totalCnt =  mobileInfoService.newsListCnt(searchWord);
		} else {
			totalCnt = 100;
		}
		ModelAndView mav = new ModelAndView();
		Map<String,Object> viewObject = new HashMap<String,Object>();
		viewObject.put("nowPage", pageNo);
		viewObject.put("keyWord", keyWord);
		viewObject.put("totalCnt", totalCnt);
		mav.setViewName("mobile/info/news/newsList");
		mav.addObject("newsList", list);
		mav.addObject("viewObject", viewObject);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView newsDetl(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		HashMap<String,Object> searchWord = new HashMap<String, Object>();
		searchWord.put("crId", ServletRequestUtils.getIntParameter(request, "crId"));
		
		News news = mobileInfoService.newsDetl(searchWord);
		mav.setViewName("mobile/info/news/newsDetl");
		mav.addObject("newsDetl", news);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView scriptList(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		HashMap<String,Object> searchWord = new HashMap<String,Object>();
		String keyWord = ServletRequestUtils.getStringParameter(request, "keyWord");
		searchWord.put("keyWord", keyWord);
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		searchWord.put("pageNo", pageNo);
		List<Script> list = mobileInfoService.scriptList(searchWord);
		int totalCnt;
		if(ServletRequestUtils.getStringParameter(request, "keyWord")!=null && ServletRequestUtils.getStringParameter(request, "keyWord")!="") {
			totalCnt =  mobileInfoService.scriptListCnt(searchWord);
		} else {
			totalCnt = 100;
		}
		ModelAndView mav = new ModelAndView();
		Map<String,Object> viewObject = new HashMap<String,Object>();
		viewObject.put("nowPage", pageNo);
		viewObject.put("keyWord", keyWord);
		viewObject.put("totalCnt", totalCnt);
		mav.setViewName("mobile/info/script/scriptList");
		mav.addObject("scriptList", list);
		mav.addObject("viewObject", viewObject);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView scriptDetl(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		HashMap<String,Object> searchWord = new HashMap<String, Object>();
		searchWord.put("crId", ServletRequestUtils.getIntParameter(request, "crId"));
		
		Script script = mobileInfoService.scriptDetl(searchWord);
		mav.setViewName("mobile/info/script/scriptDetl");
		mav.addObject("scriptDetl", script);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView imageList(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		HashMap<String,Object> searchWord = new HashMap<String,Object>();
		String keyWord = ServletRequestUtils.getStringParameter(request, "keyWord");
		searchWord.put("keyWord", keyWord);
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		searchWord.put("pageNo", pageNo);
		List<Image> list = mobileInfoService.imageList(searchWord);
		int totalCnt;
		if(ServletRequestUtils.getStringParameter(request, "keyWord")!=null && ServletRequestUtils.getStringParameter(request, "keyWord")!="") {
			totalCnt =  mobileInfoService.imageListCnt(searchWord);
		} else {
			totalCnt = 100;
		}
		ModelAndView mav = new ModelAndView();
		Map<String,Object> viewObject = new HashMap<String,Object>();
		viewObject.put("nowPage", pageNo);
		viewObject.put("keyWord", keyWord);
		viewObject.put("totalCnt", totalCnt);
		mav.setViewName("mobile/info/image/imageList");
		mav.addObject("imageList", list);
		mav.addObject("viewObject", viewObject);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView imageDetl(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		HashMap<String,Object> searchWord = new HashMap<String, Object>();
		searchWord.put("imgeSeqn", ServletRequestUtils.getIntParameter(request, "imgeSeqn"));
		
		Image image = mobileInfoService.imageDetl(searchWord);
		
		if(image.getImageUrl() != null) {
			BufferedImage i = null;
			URL url = new URL(image.getImageUrl());
		    i = ImageIO.read(url);
		    System.out.println(i.getWidth()+",,"+i.getHeight());
	        mav.addObject("imge_x", i.getWidth()+"px");
	        mav.addObject("image_y", i.getHeight()+"px");
		}
		mav.setViewName("mobile/info/image/imageDetl");
		mav.addObject("imageDetl", image);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView movieList(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		HashMap<String,Object> searchWord = new HashMap<String,Object>();
		String keyWord = ServletRequestUtils.getStringParameter(request, "keyWord");
		searchWord.put("keyWord", keyWord);
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		searchWord.put("pageNo", pageNo);
		List<Movie> list = mobileInfoService.movieList(searchWord);
		int totalCnt;
		if(ServletRequestUtils.getStringParameter(request, "keyWord")!=null && ServletRequestUtils.getStringParameter(request, "keyWord")!="") {
			totalCnt =  mobileInfoService.movieListCnt(searchWord);
		} else {
			totalCnt = 100;
		}
		ModelAndView mav = new ModelAndView();
		Map<String,Object> viewObject = new HashMap<String,Object>();
		viewObject.put("nowPage", pageNo);
		viewObject.put("keyWord", keyWord);
		viewObject.put("totalCnt", totalCnt);
		mav.setViewName("mobile/info/movie/movieList");
		mav.addObject("movieList", list);
		mav.addObject("viewObject", viewObject);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView movieDetl(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		HashMap<String,Object> searchWord = new HashMap<String, Object>();
		searchWord.put("crId", ServletRequestUtils.getIntParameter(request, "crId"));
		searchWord.put("nrId", ServletRequestUtils.getIntParameter(request, "nrId"));
		searchWord.put("albumId", ServletRequestUtils.getIntParameter(request, "albumId"));
		Movie movie = mobileInfoService.movieDetl(searchWord);
		mav.setViewName("mobile/info/movie/movieDetl");
		mav.addObject("movieDetl", movie);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView broadcastList(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		HashMap<String,Object> searchWord = new HashMap<String,Object>();
		String keyWord = ServletRequestUtils.getStringParameter(request, "keyWord");
		searchWord.put("keyWord", keyWord);
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		searchWord.put("pageNo", pageNo);
		List<Broadcast> list = mobileInfoService.broadcastList(searchWord);
		int totalCnt;
		if(ServletRequestUtils.getStringParameter(request, "keyWord")!=null && ServletRequestUtils.getStringParameter(request, "keyWord")!="") {
			totalCnt =  mobileInfoService.broadcastListCnt(searchWord);
		} else {
			totalCnt = 100;
		}
		ModelAndView mav = new ModelAndView();
		Map<String,Object> viewObject = new HashMap<String,Object>();
		viewObject.put("nowPage", pageNo);
		viewObject.put("keyWord", keyWord);
		viewObject.put("totalCnt", totalCnt);
		mav.setViewName("mobile/info/broadcast/broadcastList");
		mav.addObject("broadcastList", list);
		mav.addObject("viewObject", viewObject);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public ModelAndView broadcastDetl(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		HashMap<String,Object> searchWord = new HashMap<String, Object>();
		searchWord.put("crId", ServletRequestUtils.getIntParameter(request, "crId"));
		
		Broadcast broadcast = mobileInfoService.broadcastDetl(searchWord);
		mav.setViewName("mobile/info/broadcast/broadcastDetl");
		mav.addObject("broadcastDetl", broadcast);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	
	public MobileInfoService getMobileInfoService() {
		return mobileInfoService;
	}
	public void setMobileInfoService(MobileInfoService mobileInfoService) {
		this.mobileInfoService = mobileInfoService;
	}
	
}
