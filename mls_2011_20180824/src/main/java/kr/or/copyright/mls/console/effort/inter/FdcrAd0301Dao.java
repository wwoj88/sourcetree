package kr.or.copyright.mls.console.effort.inter;

import java.util.List;
import java.util.Map;

public interface FdcrAd0301Dao {


	public List selectMailMgntList(Map map);

	public void deleteMailMgntList(Map map);

	public void insertMailMgntList(Map map);

	public int isCheckMailMgntList(Map map);

	public List SelectMailMgntCodeList(Map map);
	
	public List statProcSetList(Map map);
	
	public void statProcSetUpdateYn(Map map);
	
	public List selectMailCommWorksInfo();	
	
	public int isCheckMailCommWorksInfo();	

	public List selectMailInmtWorksInfo();

	public int isCheckMailInmtWorksInfo();
}
