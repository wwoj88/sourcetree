package kr.or.copyright.mls.sample.service;

public interface SampleService {

	public void excelImport() throws Exception;
	
	public void sampleList() throws Exception;
	
	public void sampleSave() throws Exception;

	public void musicList() throws Exception;

	
}
