package kr.or.copyright.mls.console.icnissu;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.common.info.RequestProductInfo;
import kr.or.common.info.TaBooksInfo;
import kr.or.common.info.TaWorksInfo;
import kr.or.common.info.TaWorksLicensorInfo;
import kr.or.common.new_info.none.NoneAlbumInfo;
import kr.or.common.new_info.none.NoneAlbumMusicInfo;
import kr.or.common.new_info.none.NoneRequestInfo;
import kr.or.common.new_info.none.NoneResponseInfo;
import kr.or.common.new_info.none.NoneWorksInfo;
import kr.or.common.new_info.none.NoneWorksLicensorInfo;
import kr.or.common.new_info.none.NoneWorksPerformerInfo;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.icnissu.inter.FdcrAd53Dao;
import kr.or.copyright.mls.console.icnissu.inter.FdcrAd53Service;
import kr.or.copyright.mls.support.util.StringUtil;
import kr.or.none.NoneMgmtServiceProxy;

import org.springframework.stereotype.Service;

@Service( "fdcrAd53Service" )
public class FdcrAd53ServiceImpl extends CommandService implements FdcrAd53Service{

	@Resource( name = "fdcrAd53Dao" )
	private FdcrAd53Dao fdcrAd53Dao;

	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	/**
	 * 음악저작물 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd53List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd53Dao.selectMuscList( commandMap );

		commandMap.put( "ds_list", list );

	}

	/**
	 * 음악저작물 상세
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd53View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd53Dao.selectMuscDeatilList( commandMap );

		commandMap.put( "ds_music_detail", list );

	}

	/**
	 * 음악저작물 ICN발급 업데이트
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd53Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			String[] PRPS_MAST_KEYS = (String[]) commandMap.get( "PRPS_MAST_KEY" );
			String PRPS_IDNT = (String) commandMap.get( "PRPS_IDNT" );
			String PRPS_IDNT_ME = (String) commandMap.get( "PRPS_IDNT_ME" );
			String PRPS_IDNT_NR = (String) commandMap.get( "PRPS_IDNT_NR" );

			Map<String, Object> conMap = new HashMap<String, Object>();
			String TRST_ORGN_CODE = commandMap.get( "TRST_ORGN_CODE" ).toString();

			boolean bResult = false;

			int totCnt = PRPS_MAST_KEYS.length;
			int succCnt = 0;
			int failCnt = 0;

			for( int i = 0; i < totCnt; i++ ){

				Map<String, Object> icnMap = new HashMap<String, Object>();
				icnMap.put( "TRST_ORGN_CODE", TRST_ORGN_CODE );
				icnMap.put( "PRPS_IDNT", PRPS_IDNT );
				icnMap.put( "PRPS_IDNT_ME", PRPS_IDNT_ME );
				icnMap.put( "PRPS_IDNT_NR", PRPS_IDNT_NR );
				icnMap.put( "PRPS_MAST_KEY", PRPS_MAST_KEYS[i] );

				bResult = false;

				// 1. 연게 호출메소드 적용이 필요함
				bResult = icnCall( "M", icnMap );

				// 2. 연계 호출 정상종료 후 상태값 변경
				if( bResult ){
					succCnt++;
					fdcrAd53Dao.updateMuscIcnStat( icnMap );
				}else{
					failCnt++;
				}

			}

			System.out.println( "" );
			System.out.println( "=========================================================" );
			System.out.println( "================== [음악저작물] ICN 발급요청 ==================" );
			System.out.println( "* 전체 : " + totCnt + "건" );
			System.out.println( "* 성공 : " + succCnt + "건" );
			System.out.println( "* 실패 : " + failCnt + "건" );
			System.out.println( "=========================================================" );

			// 결과값 추가
			List resultList = new ArrayList();

			Map resultMap = new HashMap();

			resultMap.put( "totCnt", totCnt );
			resultMap.put( "succCnt", succCnt );
			resultMap.put( "failCnt", failCnt );

			resultList.add( resultMap );

			commandMap.put( "ds_result", resultList );

			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	// 연계 호출메소드 적용
	public static boolean icnCall( String divs,
		Map icnMap ){

		boolean bResult = false;

		String sResult = "";

		if( divs.equals( "M" ) ) sResult = isrtNoneMusicInfo( icnMap );
		else if( divs.equals( "O" ) ) sResult = isrtNoneBooksInfo( icnMap );
		// sample();

		if( sResult.equals( "100" ) ) bResult = true;

		return bResult;
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static String isrtNoneMusicInfo( Map muscMap ){

		NoneMgmtServiceProxy proxy = new NoneMgmtServiceProxy();
		NoneResponseInfo response = new NoneResponseInfo();
		NoneRequestInfo requestInfo = new NoneRequestInfo();

		// 저작물 kdy
		String PRPS_IDNT = StringUtil.nullToEmpty( (String) muscMap.get( "PRPS_IDNT" ) );
		String PRPS_IDNT_ME = StringUtil.nullToEmpty( (String) muscMap.get( "PRPS_IDNT_ME" ) );
		String PRPS_IDNT_NR = StringUtil.nullToEmpty( (String) muscMap.get( "PRPS_IDNT_NR" ) );

		// 저작물 data
		String title = StringUtil.nullToEmpty( (String) muscMap.get( "TITLE" ) ); // 저작물명
		String albm_titl = StringUtil.nullToEmpty( (String) muscMap.get( "ALBUM_TITLE" ) ); // 앨범명
		String edit = "";
		String issu_date = StringUtil.nullToEmpty( (String) muscMap.get( "ALBUM_ISSUE_DATE" ) );
		String albm_type_code = "";
		String albm_medi_type_code = "";
		String totl_trck = StringUtil.nullToEmpty( (String) muscMap.get( "TOTAL_TRACK" ) );

		String disk_side = StringUtil.nullToEmpty( (String) muscMap.get( "DISK_SIDE" ) );
		String disk_numb = StringUtil.nullToEmpty( (String) muscMap.get( "DISK_NO" ) );
		String trck_numb = StringUtil.nullToEmpty( (String) muscMap.get( "TRACK_NO" ) );
		String perf_time = StringUtil.nullToEmpty( (String) muscMap.get( "PERFORMANCE_TIME" ) );
		String lyrc = StringUtil.nullToEmpty( (String) muscMap.get( "LYRICS" ) );

		String LYRICIST = StringUtil.nullToEmpty( (String) muscMap.get( "LYRICIST" ) );
		String COMPOSER = StringUtil.nullToEmpty( (String) muscMap.get( "COMPOSER" ) );
		String ARRANGER = StringUtil.nullToEmpty( (String) muscMap.get( "ARRANGER" ) );
		String PRODUCER = StringUtil.nullToEmpty( (String) muscMap.get( "PRODUCER" ) );
		String SINGER = StringUtil.nullToEmpty( (String) muscMap.get( "SINGER" ) );
		String PLAYER = StringUtil.nullToEmpty( (String) muscMap.get( "PLAYER" ) );
		String CONDUCTOR = StringUtil.nullToEmpty( (String) muscMap.get( "CONDUCTOR" ) );

		String Integrate_id = "99999999000090000";

		String trstOrgnCode = (String) muscMap.get( "TRST_ORGN_CODE" ); // 로그인
																		// 기관

		String rgrt_chnl_code = "4"; // 등록 기관코드
		if( trstOrgnCode.equals( "201" ) ) rgrt_chnl_code = "4";
		if( trstOrgnCode.equals( "203" ) ) rgrt_chnl_code = "5";
		if( trstOrgnCode.equals( "204" ) ) rgrt_chnl_code = "6";

		try{

			// 저작물 정보
			NoneWorksInfo noneWorksInfo = new NoneWorksInfo();

			noneWorksInfo.setTitl( title ); // 저작물제목
			noneWorksInfo.setSubx_titl( "" ); // 저작물 부제목
			noneWorksInfo.setGnre_code( "1" ); // 장르 (1:음악)
			noneWorksInfo.setInou_type_code( "1" ); // 국내외 구분 (1:국내)

			noneWorksInfo.setRgrt_chnl_code( rgrt_chnl_code ); // 등록경로- 음저:4,
																// 음제:5, 음실:6

			requestInfo.setNoneWorksInfo( noneWorksInfo );

			/*
			 * NoneWorksLicensorInfo [] noneWorksLicensorArray = new
			 * NoneWorksLicensorInfo[3]; NoneWorksLicensorInfo
			 * noneWorksLicensorInfo1 = new NoneWorksLicensorInfo();
			 * NoneWorksLicensorInfo noneWorksLicensorInfo2 = new
			 * NoneWorksLicensorInfo(); NoneWorksLicensorInfo
			 * noneWorksLicensorInfo3 = new NoneWorksLicensorInfo();
			 */

			int liceCnt = 0;

			if( LYRICIST.length() > 0 ) liceCnt++;
			if( COMPOSER.length() > 0 ) liceCnt++;
			if( ARRANGER.length() > 0 ) liceCnt++;

			NoneWorksLicensorInfo[] noneWorksLicensorArray = new NoneWorksLicensorInfo[liceCnt];

			NoneWorksLicensorInfo noneWorksLicensorInfo = null;

			int k = 0;

			// 작사
			if( LYRICIST.length() > 0 ){

				noneWorksLicensorInfo = new NoneWorksLicensorInfo();

				noneWorksLicensorInfo.setIntegrate_id( Integrate_id ); // 통합
																		// 저작권자
																		// 아이디
				noneWorksLicensorInfo.setCa_id( "201" ); // 권리 기관ID
				noneWorksLicensorInfo.setLice_role_code( "1" ); // 저작권자역할CD 작사
																// 1, 작곡 2, 편곡 3
				noneWorksLicensorInfo.setLice_name( LYRICIST ); // 권리자명(저작권자이름)

				noneWorksLicensorArray[k] = noneWorksLicensorInfo;

				k++;
			}

			// 작곡
			if( COMPOSER.length() > 0 ){

				noneWorksLicensorInfo = new NoneWorksLicensorInfo();

				noneWorksLicensorInfo.setIntegrate_id( Integrate_id ); // 통합
																		// 저작권자
																		// 아이디
				noneWorksLicensorInfo.setCa_id( "201" ); // 권리 기관ID
				noneWorksLicensorInfo.setLice_role_code( "2" ); // 저작권자역할CD 작사
																// 1, 작곡 2, 편곡 3
				noneWorksLicensorInfo.setLice_name( COMPOSER ); // 권리자명(저작권자이름)

				noneWorksLicensorArray[k] = noneWorksLicensorInfo;

				k++;
			}

			// 작곡
			if( ARRANGER.length() > 0 ){

				noneWorksLicensorInfo = new NoneWorksLicensorInfo();

				noneWorksLicensorInfo.setIntegrate_id( Integrate_id ); // 통합
																		// 저작권자
																		// 아이디
				noneWorksLicensorInfo.setCa_id( "201" ); // 권리 기관ID
				noneWorksLicensorInfo.setLice_role_code( "3" ); // 저작권자역할CD 작사
																// 1, 작곡 2, 편곡 3
				noneWorksLicensorInfo.setLice_name( ARRANGER ); // 권리자명(저작권자이름)

				noneWorksLicensorArray[k] = noneWorksLicensorInfo;

				k++;
			}

			requestInfo.setNoneWorksLicensorArray( noneWorksLicensorArray );

			// 앨범 정보
			NoneAlbumInfo noneAlbumInfo = new NoneAlbumInfo();

			noneAlbumInfo.setInou_type_code( 1 ); // 국내외 구분 (1:국내)
			noneAlbumInfo.setAlbm_titl( albm_titl ); // 앨범 제목
			noneAlbumInfo.setAlbm_subx_titl( "" ); // 앨범부제목
			noneAlbumInfo.setEdit( edit ); // 에디션
			noneAlbumInfo.setIssu_date( issu_date );
			// noneAlbumInfo.setAlbm_type_code(0); // 앨범 형태 코드
			// noneAlbumInfo.setAlbm_medi_type_code(0); // 앨범 미디어 타입
			noneAlbumInfo.setTotl_trck( totl_trck ); // 총트랙수

			requestInfo.setNoneAlbumInfo( noneAlbumInfo );

			// 앨범 음원 정보
			NoneAlbumMusicInfo noneAlbumMusicInfo = new NoneAlbumMusicInfo();

			noneAlbumMusicInfo.setRigt_cr_idnt( PRPS_IDNT );
			noneAlbumMusicInfo.setRigt_nr_idnt( PRPS_IDNT_NR );
			noneAlbumMusicInfo.setRigt_albm_idnt( PRPS_IDNT_ME );

			noneAlbumMusicInfo.setDisk_side( disk_side ); // 디스크면
			noneAlbumMusicInfo.setDisk_numb( disk_numb ); // 디스크 번호
			noneAlbumMusicInfo.setTrck_numb( trck_numb ); // 앨범 트랙 번호
			noneAlbumMusicInfo.setPerf_time( perf_time ); // 실연 시간
			noneAlbumMusicInfo.setTitl( title ); // 음원제목(곡제목)
			noneAlbumMusicInfo.setSubx_titl( "" ); // 음원부제목(곡부제목)
			noneAlbumMusicInfo.setLyrc( lyrc ); // 가사 1소절

			requestInfo.setNoneAlbumMusicInfo( noneAlbumMusicInfo );

			int perfCnt = 0;

			if( PRODUCER.length() > 0 ) perfCnt++;
			if( SINGER.length() > 0 ) perfCnt++;
			if( PLAYER.length() > 0 ) perfCnt++;
			if( CONDUCTOR.length() > 0 ) perfCnt++;

			NoneWorksPerformerInfo[] noneWorksPerformerArray = new NoneWorksPerformerInfo[perfCnt];

			NoneWorksPerformerInfo noneWorksPerformerInfo = null;

			int kk = 0;

			// 앨범제작
			if( PRODUCER.length() > 0 ){

				noneWorksPerformerInfo = new NoneWorksPerformerInfo();

				noneWorksPerformerInfo.setIntegrate_id( Integrate_id ); // 통합
																		// 저작권자
																		// 아이디
				noneWorksPerformerInfo.setCa_id( "203" ); // 권리 기관ID
				noneWorksPerformerInfo.setPerf_role_code( "5" ); // 저작권자역할CD 작사
																	// 1, 작곡 2,
																	// 편곡 3
				noneWorksPerformerInfo.setPerf_name( PRODUCER ); // 권리자명(저작권자이름)

				noneWorksPerformerArray[kk] = noneWorksPerformerInfo;

				kk++;
			}

			// 가창
			if( SINGER.length() > 0 ){

				noneWorksPerformerInfo = new NoneWorksPerformerInfo();

				noneWorksPerformerInfo.setIntegrate_id( Integrate_id ); // 통합
																		// 저작권자
																		// 아이디
				noneWorksPerformerInfo.setCa_id( "202" ); // 권리 기관ID
				noneWorksPerformerInfo.setPerf_role_code( "1" ); // 저작권자역할CD 작사
																	// 1, 작곡 2,
																	// 편곡 3
				noneWorksPerformerInfo.setPerf_name( SINGER ); // 권리자명(저작권자이름)

				noneWorksPerformerArray[kk] = noneWorksPerformerInfo;

				kk++;
			}

			// 연주
			if( PLAYER.length() > 0 ){

				noneWorksPerformerInfo = new NoneWorksPerformerInfo();

				noneWorksPerformerInfo.setIntegrate_id( Integrate_id ); // 통합
																		// 저작권자
																		// 아이디
				noneWorksPerformerInfo.setCa_id( "202" ); // 권리 기관ID
				noneWorksPerformerInfo.setPerf_role_code( "2" ); // 저작권자역할CD 작사
																	// 1, 작곡 2,
																	// 편곡 3
				noneWorksPerformerInfo.setPerf_name( PLAYER ); // 권리자명(저작권자이름)

				noneWorksPerformerArray[kk] = noneWorksPerformerInfo;

				kk++;
			}

			// 지휘
			if( CONDUCTOR.length() > 0 ){

				noneWorksPerformerInfo = new NoneWorksPerformerInfo();

				noneWorksPerformerInfo.setIntegrate_id( Integrate_id ); // 통합
																		// 저작권자
																		// 아이디
				noneWorksPerformerInfo.setCa_id( "202" ); // 권리 기관ID
				noneWorksPerformerInfo.setPerf_role_code( "3" ); // 저작권자역할CD 작사
																	// 1, 작곡 2,
																	// 편곡 3
				noneWorksPerformerInfo.setPerf_name( CONDUCTOR ); // 권리자명(저작권자이름)

				noneWorksPerformerArray[kk] = noneWorksPerformerInfo;

				kk++;
			}

			requestInfo.setNoneWorksPerformerArray( noneWorksPerformerArray );

			response = proxy.isrtNoneMusicInfo( requestInfo );

			// 성공 : 100
			System.out.println( "ERROR CODE : " + response.getErrcode() );
			System.out.println( "ERROR MESSAGE : " + response.getErrmessage() );

		}
		catch( RemoteException e ){
			e.printStackTrace();

			// return "-100";
		}

		return response.getErrcode();
	}

	public static String isrtNoneBooksInfo( Map booksMap ){

		NoneMgmtServiceProxy proxy = new NoneMgmtServiceProxy();
		RequestProductInfo reqProductInfo = new RequestProductInfo();
		NoneResponseInfo response = new NoneResponseInfo();

		// 저작물 data
		String title = StringUtil.nullToEmpty( (String) booksMap.get( "TITLE" ) );
		String ICNX_NUMB = StringUtil.nullToEmpty( (String) booksMap.get( "ICNX_NUMB" ) );
		String TRUST_YN = StringUtil.nullToEmpty( (String) booksMap.get( "TRUST_YN" ) );

		// 도서 data
		String BOOK_TITLE = StringUtil.nullToEmpty( (String) booksMap.get( "BOOK_TITLE" ) );
		String PUBLISHER = StringUtil.nullToEmpty( (String) booksMap.get( "PUBLISHER" ) );
		int FIRST_EDITION_YEAR = StringUtil.nullToZeroInt( (String) booksMap.get( "FIRST_EDITION_YEAR" ) );
		String PUBLISH_TYPE = StringUtil.nullToEmpty( (String) booksMap.get( "PUBLISH_TYPE" ) );

		// 저작권자 data
		String LICENSOR = StringUtil.nullToEmpty( (String) booksMap.get( "LICENSOR" ) );
		String LICENSOR_ID = StringUtil.nullToEmpty( (String) booksMap.get( "LICENSOR_ID" ) );

		String TRANSLATOR = StringUtil.nullToEmpty( (String) booksMap.get( "TRANSLATOR" ) );
		String TRANSLATOR_ID = StringUtil.nullToEmpty( (String) booksMap.get( "TRANSLATOR_ID" ) );

		try{

			TaWorksInfo taWorksInfo = new TaWorksInfo(); // 저작물 정보

			taWorksInfo.setTitle( title ); // 저작물 제목
			taWorksInfo.setSubtitle( "" ); // 저작물 부제목
			taWorksInfo.setGenre( 2 ); // 장르

			taWorksInfo.setInout_type( 1 ); // 국내외 구분
			taWorksInfo.setIswc( "" ); // iswc
			taWorksInfo.setKswc( "" ); // kswc

			taWorksInfo.setCrh_id_of_ca( ICNX_NUMB ); // 기관 저작권 관리 번호 (복전협관리번호)

			taWorksInfo.setTrust_yn( TRUST_YN ); // 신탁 여부
			// taWorksInfo.setAgency_yn("N"); // 대리 중개여부
			taWorksInfo.setInsert_id( "RIGHT4ME" ); // 등록자 아이디
			taWorksInfo.setModify_id( "RIGHT4ME" );

			reqProductInfo.setTaWorksInfo( taWorksInfo );

			int liceCnt = 0;

			if( LICENSOR_ID.length() > 0 ) liceCnt++;
			if( TRANSLATOR_ID.length() > 0 ) liceCnt++;

			TaWorksLicensorInfo[] taWorksLicensorArray = new TaWorksLicensorInfo[liceCnt];

			TaWorksLicensorInfo taWorksLicensorInfo = null;

			int k = 0;

			// 저자
			if( LICENSOR_ID.length() > 0 ){

				taWorksLicensorInfo = new TaWorksLicensorInfo();

				taWorksLicensorInfo.setIntegrate_id( LICENSOR_ID ); // 통합 저작권자
																	// 아이디
				taWorksLicensorInfo.setCa_id( 205 ); // 권리 관리 기관 아이디
				taWorksLicensorInfo.setLicensor_role( 5 ); // 통합 저자권자 역할 코드(11)
															// 5:저가 6: 역자

				taWorksLicensorInfo.setTrust_yn( TRUST_YN ); // 신탁 여부
				taWorksLicensorInfo.setTrust_date( "" ); // 신탁 일자
				taWorksLicensorInfo.setCancel_of_trust_date( "" ); // 신탁 해지 일자

				taWorksLicensorInfo.setLicensor_yn( 0 ); // 저작자 구분 코드

				taWorksLicensorInfo.setLicensor_name_kor( LICENSOR ); // 권리자 명
				taWorksLicensorInfo.setInsert_id( "RIGHT4ME" ); // 등록자 아이디
				taWorksLicensorInfo.setModify_id( "RIGHT4ME" );

				taWorksLicensorArray[k] = taWorksLicensorInfo;

				k++;
			}

			// 역자
			if( TRANSLATOR_ID.length() > 0 ){

				taWorksLicensorInfo = new TaWorksLicensorInfo();

				taWorksLicensorInfo.setIntegrate_id( TRANSLATOR_ID ); // 통합 저작권자
																		// 아이디
				taWorksLicensorInfo.setCa_id( 205 ); // 권리 관리 기관 아이디
				taWorksLicensorInfo.setLicensor_role( 6 ); // 통합 저자권자 역할 코드(11)
															// 5:저가 6: 역자

				taWorksLicensorInfo.setTrust_yn( TRUST_YN ); // 신탁 여부
				taWorksLicensorInfo.setTrust_date( "" ); // 신탁 일자
				taWorksLicensorInfo.setCancel_of_trust_date( "" ); // 신탁 해지 일자

				taWorksLicensorInfo.setLicensor_yn( 0 ); // 저작자 구분 코드

				taWorksLicensorInfo.setLicensor_name_kor( TRANSLATOR ); // 권리자 명
				taWorksLicensorInfo.setInsert_id( "RIGHT4ME" ); // 등록자 아이디
				taWorksLicensorInfo.setModify_id( "RIGHT4ME" );

				taWorksLicensorArray[k] = taWorksLicensorInfo;

				k++;
			}

			reqProductInfo.setTaWorksLicensorArray( taWorksLicensorArray );

			// 도서정보
			TaBooksInfo[] taBooksArray = new TaBooksInfo[1];

			TaBooksInfo taBooksInfo = new TaBooksInfo();

			taBooksInfo.setTitle( BOOK_TITLE ); // 도서제목
			taBooksInfo.setTitle_eng( "" ); // 도서 영문 제목
			taBooksInfo.setSubtitle( "" ); // 도서 부제목

			// taBooksInfo.setPub_country(1); // 발행 국가 코드
			// taBooksInfo.setPub_lang(1); // 발행 언어
			// taBooksInfo.setMaterial_type(2); // 매체 형태 코드

			taBooksInfo.setFirst_edition_year( FIRST_EDITION_YEAR ); // 최초 발행 년도
			taBooksInfo.setPublisher( PUBLISHER ); // 발행자명
			// taBooksInfo.setPublish_type(1); // 자료 유형 코드
			// taBooksInfo.setRetrieve_type(2); // 검색 분류 코드
			taBooksInfo.setKrtra_licensor_id( ICNX_NUMB ); // 기관저작권관리번호(2차 복전협)
			taBooksInfo.setKrtra_trust_yn( TRUST_YN ); // 신탁 여부

			taBooksInfo.setInsert_id( "RIGHT4ME" );

			taBooksArray[0] = taBooksInfo;

			reqProductInfo.setTaBooksArray( taBooksArray );

			response = proxy.isrtNoneBooksInfo( reqProductInfo );

			System.out.println( "ERROR CODE : " + response.getErrcode() );
			System.out.println( "ERROR MESSAGE : " + response.getErrmessage() );
		}
		catch( RemoteException e ){
			e.printStackTrace();

			return "-100";
		}

		return response.getErrcode();
	}

}
