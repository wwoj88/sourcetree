package kr.or.copyright.mls.mobile.dao;

import java.util.HashMap;
import java.util.List;

import kr.or.copyright.mls.mobile.model.Book;
import kr.or.copyright.mls.mobile.model.Broadcast;
import kr.or.copyright.mls.mobile.model.Image;
import kr.or.copyright.mls.mobile.model.Movie;
import kr.or.copyright.mls.mobile.model.Music;
import kr.or.copyright.mls.mobile.model.News;
import kr.or.copyright.mls.mobile.model.Script;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class MobileInfoSqlMapDao extends SqlMapClientDaoSupport implements MobileInfoDao {
	
	@SuppressWarnings("unchecked")
	public List<Music> musicList(HashMap<String,Object> searchWord) {
		return (List<Music>)getSqlMapClientTemplate().queryForList("music.list", searchWord);
	}
	
	public int musicListCnt(HashMap<String,Object> searchWord) {
		return (Integer)getSqlMapClientTemplate().queryForObject("music.list.tcnt", searchWord);
	}
	
	public Music musicDetl(HashMap<String,Object> searchWord) {
		return (Music)getSqlMapClientTemplate().queryForObject("music.detl", searchWord);
	}
	
	@SuppressWarnings("unchecked")
	public List<Book> bookList(HashMap<String,Object> searchWord) {
		return (List<Book>)getSqlMapClientTemplate().queryForList("book.list",searchWord);
	}
	
	public int bookListCnt(HashMap<String,Object> searchWord) {
		return (Integer)getSqlMapClientTemplate().queryForObject("book.list.tcnt", searchWord);
	}
	
	public Book bookDetl(HashMap<String,Object> searchWord) {
		return (Book)getSqlMapClientTemplate().queryForObject("book.detl", searchWord);
	}
	
	@SuppressWarnings("unchecked")
	public List<News> newsList(HashMap<String,Object> searchWord) {
		return (List<News>)getSqlMapClientTemplate().queryForList("news.list",searchWord);
	}
	
	public int newsListCnt(HashMap<String,Object> searchWord) {
		return (Integer)getSqlMapClientTemplate().queryForObject("news.list.tcnt", searchWord);
	}
	
	public News newsDetl(HashMap<String,Object> searchWord) {
		return (News)getSqlMapClientTemplate().queryForObject("news.detl", searchWord);
	}
	
	@SuppressWarnings("unchecked")
	public List<Script> scriptList(HashMap<String,Object> searchWord) {
		return (List<Script>)getSqlMapClientTemplate().queryForList("script.list",searchWord);
	}
	
	public int scriptListCnt(HashMap<String,Object> searchWord) {
		return (Integer)getSqlMapClientTemplate().queryForObject("script.list.tcnt", searchWord);
	}
	
	public Script scriptDetl(HashMap<String,Object> searchWord) {
		return (Script)getSqlMapClientTemplate().queryForObject("script.detl", searchWord);
	}   
	
	@SuppressWarnings("unchecked")
	public List<Image> imageList(HashMap<String,Object> searchWord) {
		return (List<Image>)getSqlMapClientTemplate().queryForList("image.list",searchWord);
	}
	
	public int imageListCnt(HashMap<String,Object> searchWord) {
		return (Integer)getSqlMapClientTemplate().queryForObject("image.list.tcnt", searchWord);
	}
	
	public Image imageDetl(HashMap<String,Object> searchWord) {
		return (Image)getSqlMapClientTemplate().queryForObject("image.detl", searchWord);
	}   
	
	@SuppressWarnings("unchecked")
	public List<Movie> movieList(HashMap<String,Object> searchWord) {
		return (List<Movie>)getSqlMapClientTemplate().queryForList("movie.list",searchWord);
	}
	
	public int movieListCnt(HashMap<String,Object> searchWord) {
		return (Integer)getSqlMapClientTemplate().queryForObject("movie.list.tcnt", searchWord);
	}
	
	public Movie movieDetl(HashMap<String,Object> searchWord) {
		return (Movie)getSqlMapClientTemplate().queryForObject("movie.detl", searchWord);
	}   
	
	@SuppressWarnings("unchecked")
	public List<Broadcast> broadcastList(HashMap<String,Object> searchWord) {
		return (List<Broadcast>)getSqlMapClientTemplate().queryForList("broadcast.list",searchWord);
	}
	
	public int broadcastListCnt(HashMap<String,Object> searchWord) {
		return (Integer)getSqlMapClientTemplate().queryForObject("broadcast.list.tcnt", searchWord);
	}
	
	public Broadcast broadcastDetl(HashMap<String,Object> searchWord) {
		return (Broadcast)getSqlMapClientTemplate().queryForObject("broadcast.detl", searchWord);
	}  
	
}
