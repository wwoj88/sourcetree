package kr.or.copyright.mls.mobile.model;

public class Stat {
	private int
		bordSeqn
		, menuSeqn
		, threaded
	;
	
	private String 
		tite
		, bordDesc
		, gubun
		, licensorName
		, genre
		, apprDttm
	;

	public String getApprDttm() {
		return apprDttm;
	}

	public void setApprDttm(String apprDttm) {
		this.apprDttm = apprDttm;
	}

	public int getBordSeqn() {
		return bordSeqn;
	}

	public void setBordSeqn(int bordSeqn) {
		this.bordSeqn = bordSeqn;
	}

	public int getMenuSeqn() {
		return menuSeqn;
	}

	public void setMenuSeqn(int menuSeqn) {
		this.menuSeqn = menuSeqn;
	}

	public int getThreaded() {
		return threaded;
	}

	public void setThreaded(int threaded) {
		this.threaded = threaded;
	}

	public String getTite() {
		return tite;
	}

	public void setTite(String tite) {
		this.tite = tite;
	}

	public String getBordDesc() {
		return bordDesc;
	}

	public void setBordDesc(String bordDesc) {
		this.bordDesc = bordDesc;
	}

	public String getGubun() {
		return gubun;
	}

	public void setGubun(String gubun) {
		this.gubun = gubun;
	}

	public String getLicensorName() {
		return licensorName;
	}

	public void setLicensorName(String licensorName) {
		this.licensorName = licensorName;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	
}
