package kr.or.copyright.mls.ajaxTest.dao;

import java.util.List;

public interface UserListDAO {
	
	public List<String> getNameListForSuggest(String namePrefix);
		

}
