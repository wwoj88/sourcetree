package kr.or.copyright.mls.console.cmpgn;

import java.io.PrintWriter;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.cmpgn.inter.FdcrAdB3Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 캠페인 > 설문 참여현황
 * 
 * @author wizksy
 */
@Controller
public class FdcrAdB3Controller extends DefaultController{

	@Resource( name = "fdcrAdB3Service" )
	private FdcrAdB3Service fdcrAdB3Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdB3Controller.class );

	/**
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/cmpgn/fdcrAdB3List1.page" )
	public String fdcrAdB3List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		fdcrAdB3Service.fdcrAdB3List1( commandMap );
		model.addAttribute( "ds_List", commandMap.get( "ds_List" ) );
		logger.debug( "fdcrAdB3List1 Start" );
		return "cmpgn/fdcrAdB3List1.tiles";
	}
	
	/**
	 * 엑셀 다운로드
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/cmpgn/fdcrAdB3Down1.page" )
	public String fdcrAdB3Down1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		fdcrAdB3Service.fdcrAdB3List1( commandMap );
		model.addAttribute("TO", EgovWebUtil.getDate( "yyyyMMdd" ));
		model.addAttribute( "ds_List", commandMap.get( "ds_List" ) );
		return "cmpgn/fdcrAdB3Down1";
	}
	
	/**
	 * 설문 조회
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/cmpgn/fdcrAdB3View1.page" )
	public String fdcrAdB3View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		Map<String, Object> info = (Map<String, Object>)fdcrAdB3Service.fdcrAdB3View1( commandMap );
		model.addAttribute( "info", info );
		logger.debug( "fdcrAdB3View1 Start" );
		return "cmpgn/fdcrAdB3View1.tiles";
	}
	
	/**
	 * 첨부파일 다운로드
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/cmpgn/fdcrAdB3Down2.page" )
	public void fdcrAdB3Down2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		Map<String, Object> info = fdcrAdB3Service.fdcrAdB3View1( commandMap );
		
		String fileRealNm = EgovWebUtil.getString( info, "CAMP_FILE_RNME" );
		String fileNm = EgovWebUtil.getString( info, "CAMP_FILE_NAME" );
		String savePath = EgovWebUtil.getString( info, "CAMP_FILE_PATH" );
		String downPath = savePath + fileNm;
		try{
			if( !"".equals( fileRealNm ) ){
				download( request, response, downPath, fileRealNm );
			}
		}catch(Exception e){
			response.sendRedirect( "/console/notFile.jsp" );
			e.printStackTrace();
		}
	}
}
