package kr.or.copyright.mls.adminWorksMgnt.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.tobesoft.platform.data.Dataset;

/**
 * Class 내용 기술
 *
 * @author : 정병호
 * @since : 20120910
 * @version : 1.0
 * @see: 위탁관리 저작물 보고
 *
 */
public interface AdminWorksMgntDao {
    
    //위탁관리 음악저작물정보 등록 1
    public List adminCommMusicInsert(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    //위탁관리 음악저작물정보 수정 1
    public List adminCommMusicUpdate(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
   
    
    
    //위탁관리 어문저작물정보 등록 2
    public List adminCommBookInsert(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    //위탁관리 어문저작물정보 수정 2
    public List adminCommBookUpdate(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    
    
    //위탁관리 방송대본저작물정보 등록 3
    public List adminCommScriptInsert(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    //위탁관리 방송대본저작물정보 수정 3
    public List adminCommScriptUpdate(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    
    
    //위탁관리 영화저작물정보 등록 4
    public List adminCommMovieInsert(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    //위탁관리 영화저작물정보 수정 4
    public List adminCommMovieUpdate(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    
    
    //위탁관리 방송저작물정보 등록 5
    public List adminCommBroadcastInsert(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    //위탁관리 방송저작물정보 수정 5
    public List adminCommBroadcastUpdate(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    
    
    //위탁관리 뉴스저작물정보 등록 6
    public List adminCommNewsInsert(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    //위탁관리 뉴스저작물정보 수정 6
    public List adminCommNewsUpdate(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    

    
    //위탁관리 미술저작물정보 등록 7
    public List adminCommArtInsert(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    //위탁관리 미술저작물정보 수정 7
    public List adminCommArtUpdate(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;

    //위탁관리 이미지저작물정보 등록 8
    public List adminCommImageInsert(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    //위탁관리 이미지저작물정보 수정 8
    public List adminCommImageUpdate(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    
    //위탁관리 기타저작물정보 등록 9
    public List adminCommSideInsert(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    //위탁관리 기타저작물정보 수정 9
    public List adminCommSideUpdate(Dataset ds_condition, Dataset ds_report, Dataset ds_excel) throws Exception;
    
    
    //위탁관리 저작물 목록 조회
    public List worksMgntList(Map map);
    
    //위탁관리 저작물 목록 조회 20141126 이병원
    public List worksMgntList_01(Map map);
    
    //위탁관리 저작물 음악 조회
    public List worksMgntList_01_music(Map map);
    
    //  위탁관리 저작물 어문 조회
    public List worksMgntList_01_book(Map map);
    
    //  위탁관리 저작물 방송대본 조회
    public List worksMgntList_01_script(Map map);
    
    //  위탁관리 저작물 영화 조회
    public List worksMgntList_01_movie(Map map);
    
    //  위탁관리 저작물 방송 조회
    public List worksMgntList_01_broadcast(Map map);
    
    //  위탁관리 저작물 뉴스 조회
    public List worksMgntList_01_news(Map map);
    
    //  위탁관리 저작물 미술 조회
    public List worksMgntList_01_art(Map map);
    
    //  위탁관리 저작물 이미지 조회
    public List worksMgntList_01_image(Map map);
    
    //  위탁관리 저작물 기타 조회
    public List worksMgntList_01_side(Map map);
    
    //위탁관리 저작물 작성기관명 조회
    public List searchOrgnName(Map map);
    
    //위탁관리 저작물 목록 페이징 정보
    public List totalRowWorksMgntList(Map map);
    
    //위탁관리 저작물 목록 페이징 2014 11 26 이병원
    public List totalRowWorksMgntList_01(Map map);
    
    //위탁관리 월별 보고현황 조회
    public List worksMgntReptView(Map map);
    
    
    //위탁관리 월별 보고정보
    public List worksMgntReptInfo(Map map);
    
    
    //위탁관리 저작물 보고 기관정보 조회(기관코드, ID)
    public List worksMgntReptInfo2(Map map);
    
    //위탁관리 저작물 기존 등록여부 조회(기관코드,내부관리ID,기관명)
    public List worksMgntChkCnt(Dataset ds_condition, Dataset ds_excel) throws Exception;
    
    // 등록부연계 월별 등록현황 조회
    public List crosWorksMgntReptView(Map map);
    
    //보고저작물 없음
    public void adminWorksMgntNoDataInsert(Dataset ds_condition, Dataset ds_report) throws Exception;
    
    public List worksMgntReptMonthView(Map map);
    
    public List worksMgntReptMonthCoCnt(Map map);
    
    public List worksMgntReptViewCoCnt(Map map);
    
    public List worksMgntAppDate(Map map);
    
    public List worksMgntReptYearView(Map map);
    
    public int adminCommWorksReportInsert(Map map) throws SQLException;
    public int getCommWorksId();
    
    public int getExistCommWorks(Map map) throws Exception;
    
    public String commWorksInsertBacth(List list) throws Exception;
    public String commMusicInsertBacth(List list) throws Exception;
    public String commBookInsertBacth(List list) throws Exception;
    public String commScriptInsertBacth(List list) throws Exception;
    public String commMovieInsertBacth(List list) throws Exception;
    public String commBroadcastInsertBacth(List list) throws Exception;
    public String commNewsInsertBacth(List list) throws Exception;
    public String commArtInsertBacth(List list) throws Exception;
    public String commImageInsertBacth(List list) throws Exception;
    public String commSideInsertBacth(List list) throws Exception;
    public void adminCommWorksDelete(Map map) throws Exception;
    
    public boolean adminCommWorksInsert(Map map);
    public boolean adminCommMusicInsert(Map map);
    public boolean adminCommBookInsert(Map map);
    public boolean adminCommScriptInsert(Map map);
    public boolean adminCommMovieInsert(Map map);
    public boolean adminCommBroadcastInsert(Map map);
    public boolean adminCommNewsInsert(Map map);
    public boolean adminCommArtInsert(Map map);
    public boolean adminCommImageInsert(Map map);
    public boolean adminCommSideInsert(Map map);
    
    
    public void adminCommWorksReportUpdate(Map map);

  //위탁관리 저작물 목록 엑셀 다운로드 
    public List worksMgntExcelDown(Map map);
    
}
    
