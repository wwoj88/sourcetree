package kr.or.copyright.mls.console;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * �???�? 리�???? ?��?��?? ???? MessageSource ?��?��???��?? �? ReloadableResourceBundleMessageSource ?��???��?? 구�??�?
 * @author 공�?��??�??? �?�??? ?�문�?
 * @since 2009.06.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << �????��??(Modification Information) >>
 *   
 *   ??????      ??????           ?????��??
 *  -------    --------    ---------------------------
 *   2009.03.11  ?�문�?          �?�? ????
 *
 * </pre>
 */

public class EgovMessageSource extends ReloadableResourceBundleMessageSource implements MessageSource {

	private ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource;

	/**
	 * getReloadableResourceBundleMessageSource() 
	 * @param reloadableResourceBundleMessageSource - resource MessageSource
	 * @return ReloadableResourceBundleMessageSource
	 */	
	public void setReloadableResourceBundleMessageSource(ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource) {
		this.reloadableResourceBundleMessageSource = reloadableResourceBundleMessageSource;
	}
	
	/**
	 * getReloadableResourceBundleMessageSource() 
	 * @return ReloadableResourceBundleMessageSource
	 */	
	public ReloadableResourceBundleMessageSource getReloadableResourceBundleMessageSource() {
		return reloadableResourceBundleMessageSource;
	}
	
	/**
	 * ?????? �??��? 조�??
	 * @param code - �??��? �???
	 * @return String
	 */	
	public String getMessage(String code) {
		return getReloadableResourceBundleMessageSource().getMessage(code, null, Locale.getDefault());
	}

}
