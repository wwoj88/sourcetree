package kr.or.copyright.mls.console.email.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface FdcrAdA5Service{

	/**
	 * 메시지 관리 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA5List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 메시지 관리 상세 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA5View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 메시지 삭제
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA5Delete1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 메시지 수정
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA5Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

}
