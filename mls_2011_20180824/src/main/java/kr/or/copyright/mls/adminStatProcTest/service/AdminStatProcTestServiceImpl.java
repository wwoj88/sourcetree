package kr.or.copyright.mls.adminStatProcTest.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.tobesoft.platform.data.Dataset;

import kr.or.copyright.mls.adminStatProcTest.dao.AdminStatProcTestDao;
import kr.or.copyright.mls.common.service.BaseService;

public class AdminStatProcTestServiceImpl extends BaseService implements AdminStatProcTestService{
	
	
	private AdminStatProcTestDao adminStatProcDao;
	
	public void setAdminStatProcTestDao(AdminStatProcTestDao adminStatProcDao){
		this.adminStatProcDao = adminStatProcDao;
	}
	
	
	
	// 상당한노력 진행현황 조회
	public void statProcList() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List) adminStatProcDao.selectStatProcAllLogList(map);
		//List works_list = (List) adminStatProcDao.selectStatWorksAllLogList(map);
		
		//addList("ds_works_list", works_list);
		addList("ds_list", list);
	}
	
	// 대상저작물 수집-프로시져 호출
	public void getStatProcOrd() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		adminStatProcDao.callProcGetStatProcOrd();
		List list = (List) adminStatProcDao.selectStatProcAllLogList(map);
		
		addList("ds_list", list);
	}
	
	// 상당한 노력 진행(기존)
	public void goStatProcV10() throws Exception{
		Dataset ds_list_proc = getDataset("ds_list_proc");
		
		ds_list_proc.printDataset();
		
		Map map = getMap(ds_list_proc, 0);
		
		map.get("ORD");
		map.get("WORKS_DIVS_CD");
		
		int iBase = 10000; // 프로시저 실행 구분 건수
		int iMod = 0;
		int iTargCnt = 0; 
		int iRoop = 0;
		
		iTargCnt = ds_list_proc.getColumnAsInteger(0,"TARG_WORKS_CONT");//(Integer) map.get("TARG_WORKS_CONT");
		
		/*
		if(ds_list_proc.getColumnAsString(0, "WORKS_DIVS_CD") == "1") // 미분배
			map.put("WORKS_DIVS_CD", "1,2,3");
		else 
			map.put("WORKS_DIVS_CD", "4");	// 거소불명
		*/
		
		// 단위 건수 셋팅
		iRoop = (iTargCnt/iBase);
		iMod = (iTargCnt%iBase);
		
		System.out.println("=iTargCnt : "+iTargCnt);
		System.out.println("=iRoop : "+iRoop);
		System.out.println("=iMod : "+iMod);
		if(iMod > 0 ) iRoop++;
		System.out.println("=iRoop : "+iRoop);
		
		int iFrom = 0;
		int iTo = 0;
		
		// 전체로그 : start
		for(int i=0; i<iRoop; i++) {
			
			iFrom = (i*iBase)+1;
			iTo = (i*iBase)+iBase;
			if(iTo>iTargCnt) iTo = iTargCnt;
			
			map.put("FROM", iFrom);
			map.put("TO", iTo);
			
			System.out.println("*** roop test >> "+i+" :: "+iFrom+", "+iTo);
			// call 프로시저
			adminStatProcDao.callStatProc(map);
			
		}
		// 전체로그 : end
	}
	
	// 상당한 노력 진행(프로세스 분기 20121108)
	public void goStatProc() throws Exception{
		Dataset ds_list_proc = getDataset("ds_list_proc");
		
		ds_list_proc.printDataset();
		
		Map map = getMap(ds_list_proc, 0);
		
		map.get("ORD");
		map.get("WORKS_DIVS_CD");
		
		int iBase = 500; // 프로시저 실행 구분 건수
		int iMod = 0;
		int iTargCnt = 0; 
		int iRoop = 0;
		
		iTargCnt = ds_list_proc.getColumnAsInteger(0,"TARG_WORKS_CONT");
		
		// 단위 건수 셋팅
		iRoop = (iTargCnt/iBase);
		iMod = (iTargCnt%iBase);
		
		System.out.println("=iTargCnt : "+iTargCnt);
		System.out.println("=iRoop : "+iRoop);
		System.out.println("=iMod : "+iMod);
		if(iMod > 0 ) iRoop++;
		System.out.println("=iRoop : "+iRoop);
		
		int iFrom = 0;
		int iTo = 0;
		
		// 01. 상당한노력 공고
		for(int i=0; i<iRoop; i++) {
			
			iFrom = (i*iBase)+1;
			iTo = (i*iBase)+iBase;
			if(iTo>iTargCnt) iTo = iTargCnt;
			
			map.put("FROM", iFrom);
			map.put("TO", iTo);
			
			System.out.println("*** 공고 roop test >> "+i+" :: "+iFrom+", "+iTo);
			
			// call 프로시저 
			adminStatProcDao.callStatProc01(map);
			
		}
		
		// 02. 매칭
		for(int i=0; i<iRoop; i++) {
			
			iFrom = (i*iBase)+1;
			iTo = (i*iBase)+iBase;
			if(iTo>iTargCnt) iTo = iTargCnt;
			
			map.put("FROM", iFrom);
			map.put("TO", iTo);
			
			System.out.println("*** 매칭 roop test >> "+i+" :: "+iFrom+", "+iTo);
			
			// call 프로시저 
			adminStatProcDao.callStatProc02(map);
			
		}
	}
	
	public void statProcPopup() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List) adminStatProcDao.statProcPopup(map);
		
		addList("ds_list", list);
		
	}
	
}
