package kr.or.copyright.mls.common.dao;

import java.util.List;

import kr.or.copyright.mls.common.model.CodeList;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class CodeListSqlMapDao extends SqlMapClientDaoSupport implements CodeListDao{

	public List<CodeList> commonCodeList(CodeList codeList){
		return getSqlMapClientTemplate().queryForList("CodeList.commonCodeList", codeList);
	}
	
}
