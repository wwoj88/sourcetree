package kr.or.copyright.mls.mobile.service;

import java.util.HashMap;
import java.util.List;

import kr.or.copyright.mls.mobile.model.FAQ;
import kr.or.copyright.mls.mobile.model.Stat;

public interface MobileService {
	List<Stat> statList(HashMap<String,Object> hm);
	int statListCnt(HashMap<String,Object> hm);
	Stat statDetl(int bordSeqn);
	
	List<FAQ> faqList(HashMap<String,Object> hm);
	int faqListCnt(HashMap<String,Object> hm);
	void insertUserMobileCount();//이용자현황 카운트
	void insertUserMobileCount(int sysCd);//이용자현황 카운트 (시스템구분)
}
