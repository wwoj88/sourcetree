package kr.or.copyright.mls.adminRght.dao;

import java.util.List;
import java.util.Map;

public interface AdminRghtDao {

	// 내권리찾기 조회
	public List rghtList(Map map);

	// 내권리찾기  상태변경
	public void rghtDetlUpdate(Map map);	
	
	// 내권리찾기 저작물목록
	public List rghtTempList(Map map);
	
	// 권리찾기 저작물목록(영화)
	public List rghtMvieTempList(Map map);
	
	// 권리찾기 저작물목록(방송대본)
	public List rghtScriptTempList(Map map);
	
	// 권리찾기 저작물목록(방송)
	public List rghtBroadcastTempList(Map map);

	// 권리찾기 저작물목록(뉴스)
	public List rghtNewsTempList(Map map);
	
	// 내권리찾기 상세조회
	public List rghtDetlList(Map map);	
	
	// 신청저작물 상세조회(음악)
	public List rghtMuscDetail(Map map);
	
	// 신청저작물 상세조회(어문)
	public List rghtBookDetail(Map map);
	
	// 신청저작물 상세조회(방송대본)
	public List rghtScriptDetail(Map map);
	
	// 신청저작물 상세조회(영화)
	public List rghtMvieDetail(Map map);
	
	// 신청저작물 상세조회(방송)
	public List rghtBroadcastDetail(Map map);
	
	// 신청저작물 상세조회(뉴스)
	public List rghtNewsDetail(Map map);
	
	// 내권리찾기 완료처리 
	public void rghtEndUpdate(Map map);
	
	// 내권리찾기 권리정보 삭제처리 
	public void prpsRsltRghtDelete(Map map);	
	
	// USER TABLE 통합권자 아이디 UPDATE
	public void userIcnUpdate(Map map);
	
	// 내권리찾기 상세저장
	public void rghtUpdate(Map map);
	
	// 내권리찾기 상세저장
	public void rghtRsltUpdate(Map map);
	
	// 내권리찾기 권리정보 수정
	public void prpsRsltRghtUpdate(Map map);
	
	// 이미지저작물 저작권자정보 수정
	public void imgeUpdate(Map map);
	
	// 통합저작권자 아이디 조회 
	public List getIcnNumb(Map map);	
	
	public List rghtFileList(Map map);	
	
	public int rghtRsltCnt(Map map);
	
	public String rghtPrpsTitle(Map map);
	
	public List rghtDealStat(Map map);
}
