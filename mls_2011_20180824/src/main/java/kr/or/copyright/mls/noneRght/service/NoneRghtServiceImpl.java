package kr.or.copyright.mls.noneRght.service;

import java.util.ArrayList;
import java.util.List;

import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.noneRght.dao.NoneRghtDao;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

public class NoneRghtServiceImpl extends BaseService implements NoneRghtService{

	private NoneRghtDao noneRghtDao;
	
	public void setNoneRghtDao (NoneRghtDao noneRghtDao) {
		this.noneRghtDao = noneRghtDao;
	}
	
    //음악목록
	public ListResult muscRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps) {
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = noneRghtDao.muscRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
				
		if(totalRow>0)
			list = noneRghtDao.muscRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		return listResult;
	}

	// 어문목록
	public ListResult bookRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps) {
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = noneRghtDao.bookRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
				
		if(totalRow>0)
			list = noneRghtDao.bookRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		return listResult;
		
		
	}
	
	// 방송대본목록
	public ListResult scriptRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps) {
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = noneRghtDao.scriptRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
				
		if(totalRow>0)
			list = noneRghtDao.scriptRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		return listResult;
		
	}
	
	// 이미지목록
	public ListResult imageRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps) {
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = noneRghtDao.imageRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
				
		if(totalRow>0)
			list = noneRghtDao.imageRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		return listResult;
	}
	
	// 영화목록
	public ListResult mvieRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps) {
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = noneRghtDao.mvieRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
				
		if(totalRow>0)
			list = noneRghtDao.mvieRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		return listResult;
		
	}

	// 방송목록
	public ListResult broadcastRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps) {

		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = noneRghtDao.broadcastRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
				
		if(totalRow>0)
			list = noneRghtDao.broadcastRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		return listResult;
		
	}
	
	// 뉴스목록
	public ListResult newsRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps){
		
		List list = new ArrayList();
		int totalRow = 0;
		
		if (rghtPrps.getSTotalRow().length() == 0)
			totalRow = noneRghtDao.newsRghtCount(rghtPrps);
		else 
			totalRow = Integer.parseInt(rghtPrps.getSTotalRow());
				
		if(totalRow>0)
			list = noneRghtDao.newsRghtList(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		return listResult;
		
	}
}
