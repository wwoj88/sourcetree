package kr.or.copyright.mls.console.stats;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.stats.inter.FdcrAd87Dao;
import kr.or.copyright.mls.console.stats.inter.FdcrAd95Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd95Service" )
public class FdcrAd95ServiceImpl extends CommandService implements FdcrAd95Service{

	// old AdminConnDao
	@Resource( name = "fdcrAd87Dao" )
	private FdcrAd87Dao fdcrAd87Dao;

	/**
	 * 법정허락 신청 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List1( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List list1 = (List) fdcrAd87Dao.statYearPrpsApplyList( commandMap );
		List list2 = (List) fdcrAd87Dao.statPrpsWorksList( commandMap );
		List list3 = (List) fdcrAd87Dao.statYearInmtList( commandMap );
		List list4 = (List) fdcrAd87Dao.statYearQuProcList( commandMap );
		
		int PRE_YEAR = 0;
		for(int i=0;i<list4.size();i++){
			Map<String,Object> map = (Map<String,Object>)list4.get( i );
			int YEAR = EgovWebUtil.getToInt( map, "YEAR" );
			if(PRE_YEAR!=YEAR){
				int ROWS = getListCount(list4, YEAR);
				map.put( "ROWS", ROWS );
			}
			
			PRE_YEAR = YEAR;
		}
		List list5 = (List) fdcrAd87Dao.statYearGenreProcList( commandMap );
		PRE_YEAR = 0;
		for(int i=0;i<list5.size();i++){
			Map<String,Object> map = (Map<String,Object>)list5.get( i );
			int YEAR = EgovWebUtil.getToInt( map, "YEAR" );
			if(PRE_YEAR!=YEAR){
				int ROWS = getListCount(list5, YEAR);
				map.put( "ROWS", ROWS );
			}
			
			PRE_YEAR = YEAR;
		}

		commandMap.put( "ds_list1", list1 );
		commandMap.put( "ds_list2", list2 );
		commandMap.put( "ds_list3", list3 );
		commandMap.put( "ds_list4", list4 );
		commandMap.put( "ds_list5", list5 );

	}
	
	public int getListCount(List list, int year){
		int count = 0;
		for(int i=0;i<list.size();i++){
			Map<String,Object> map = (Map<String,Object>)list.get( i );
			int YEAR = EgovWebUtil.getToInt( map, "YEAR" );
			if(YEAR == year){
				count++;
			}
		}
		return count;
	}

	/**
	 * 법정허락 이용승인 건수 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List2( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list1 = (List) fdcrAd87Dao.statYearPrpsApplyList( commandMap );

		commandMap.put( "ds_list1", list1 );
	}

	/**
	 * 법정허락 대상 저작물 현황
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List3( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list2 = (List) fdcrAd87Dao.statPrpsWorksList( commandMap );

		commandMap.put( "ds_list2", list2 );
	}

	/**
	 * 보상금 공탁서 공고 건수 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List4( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list3 = (List) fdcrAd87Dao.statYearInmtList( commandMap );

		commandMap.put( "ds_list3", list3 );
	}

	/**
	 * 상당한노력 신청 건수 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List5( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list4 = (List) fdcrAd87Dao.statYearQuProcList( commandMap );

		commandMap.put( "ds_list4", list4 );
	}

	/**
	 * 상당한 노력 신청 유형별 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List6( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list5 = (List) fdcrAd87Dao.statYearGenreProcList( commandMap );

		commandMap.put( "ds_list5", list5 );
	}

	/**
	 * 법정허락 이용승인 명세서 목록(팝업)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List7( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd87Dao.statPrpsApplyList( commandMap );

		commandMap.put( "ds_list", list );
	}
}
