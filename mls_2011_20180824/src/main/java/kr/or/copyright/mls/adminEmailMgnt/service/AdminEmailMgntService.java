package kr.or.copyright.mls.adminEmailMgnt.service;

import java.util.List;
import java.util.Map;



public interface AdminEmailMgntService {
	
	
	public void selectEmailAddrList() throws Exception;
	
	public void insertEmail() throws Exception;
	
	public void selectEmailMsgList() throws Exception;
	
	public void selectEmailMsg() throws Exception;

	public void updateEmail() throws Exception;
	
	public void deleteEmailMsg() throws Exception;
	
	public void preSendEmail() throws Exception;
	
	public int sendEmail(List<Map> sendList, Map mailMap) throws Exception;
	
	public void selectSendEmailList() throws Exception;
	
	public void selectSendEmailView() throws Exception;
	
	public void selectAllEmailAddrList() throws Exception;
	
	public void selectScMailingList() throws Exception;
	
	public void insertUpdateScMailing() throws Exception;
	
	public void monthMailScheduling() throws Exception;
	
	public boolean preCheckScheduling(String cd) throws Exception;
	
}
