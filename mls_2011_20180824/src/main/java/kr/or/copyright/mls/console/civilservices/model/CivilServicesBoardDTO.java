package kr.or.copyright.mls.console.civilservices.model;

import java.util.Date;
import java.util.List;

public class CivilServicesBoardDTO{
	int civilSeqn;
	int commentSeqnNum;
	String civilTitle;
	String boardContent;
	String civilDeth;
	String pswd;
	String civilState;
	String civilCategory;
	String civilKind;
	int inqrCont;
	String performersIdnt;
	String rgstIdnt;
	Date rgstDttm;
	String answRgstIdnt;
	Date answRgstDttm;
	String deltYn;
	List<CivilServicesDTO> civilServicesDTO; 
	public CivilServicesBoardDTO(){
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getCivilSeqn(){
		return civilSeqn;
	}
	
	public void setCivilSeqn( int civilSeqn ){
		this.civilSeqn = civilSeqn;
	}
	
	public int getCommentSeqnNum(){
		return commentSeqnNum;
	}
	
	public void setCommentSeqnNum( int commentSeqnNum ){
		this.commentSeqnNum = commentSeqnNum;
	}
	
	public String getCivilTitle(){
		return civilTitle;
	}
	
	public void setCivilTitle( String civilTitle ){
		this.civilTitle = civilTitle;
	}
	
	public String getBoardContent(){
		return boardContent;
	}
	
	public void setBoardContent( String boardContent ){
		this.boardContent = boardContent;
	}
	
	public String getCivilDeth(){
		return civilDeth;
	}
	
	public void setCivilDeth( String civilDeth ){
		this.civilDeth = civilDeth;
	}
	
	public String getPswd(){
		return pswd;
	}
	
	public void setPswd( String pswd ){
		this.pswd = pswd;
	}
	
	public String getCivilState(){
		return civilState;
	}
	
	public void setCivilState( String civilState ){
		this.civilState = civilState;
	}
	
	public String getCivilCategory(){
		return civilCategory;
	}
	
	public void setCivilCategory( String civilCategory ){
		this.civilCategory = civilCategory;
	}
	
	public String getCivilKind(){
		return civilKind;
	}
	
	public void setCivilKind( String civilKind ){
		this.civilKind = civilKind;
	}
	
	public int getInqrCont(){
		return inqrCont;
	}
	
	public void setInqrCont( int inqrCont ){
		this.inqrCont = inqrCont;
	}
	
	public String getPerformersIdnt(){
		return performersIdnt;
	}
	
	public void setPerformersIdnt( String performersIdnt ){
		this.performersIdnt = performersIdnt;
	}
	
	public String getRgstIdnt(){
		return rgstIdnt;
	}
	
	public void setRgstIdnt( String rgstIdnt ){
		this.rgstIdnt = rgstIdnt;
	}
	
	public Date getRgstDttm(){
		return rgstDttm;
	}
	
	public void setRgstDttm( Date rgstDttm ){
		this.rgstDttm = rgstDttm;
	}
	
	public String getAnswRgstIdnt(){
		return answRgstIdnt;
	}
	
	public void setAnswRgstIdnt( String answRgstIdnt ){
		this.answRgstIdnt = answRgstIdnt;
	}
	
	public Date getAnswRgstDttm(){
		return answRgstDttm;
	}
	
	public void setAnswRgstDttm( Date answRgstDttm ){
		this.answRgstDttm = answRgstDttm;
	}
	
	public String getDeltYn(){
		return deltYn;
	}
	
	public void setDeltYn( String deltYn ){
		this.deltYn = deltYn;
	}
	
	public List<CivilServicesDTO> getCivilServicesDTO(){
		return civilServicesDTO;
	}

	
	public void setCivilServicesDTO( List<CivilServicesDTO> civilServicesDTO ){
		this.civilServicesDTO = civilServicesDTO;
	}

	@Override
	public String toString(){
		return "CivilServicesBoardDTO [civilSeqn=" + civilSeqn + ", commentSeqnNum=" + commentSeqnNum + ", civilTitle="
			+ civilTitle + ", boardContent=" + boardContent + ", civilDeth=" + civilDeth + ", pswd=" + pswd
			+ ", civilState=" + civilState + ", civilCategory=" + civilCategory + ", civilKind=" + civilKind
			+ ", inqrCont=" + inqrCont + ", performersIdnt=" + performersIdnt + ", rgstIdnt=" + rgstIdnt + ", rgstDttm="
			+ rgstDttm + ", answRgstIdnt=" + answRgstIdnt + ", answRgstDttm=" + answRgstDttm + ", deltYn=" + deltYn
			+ "]";
	}
	
}
