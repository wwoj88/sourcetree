package kr.or.copyright.mls.miplatform.dao;

import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.ibatis.sqlmap.client.SqlMapException;

public class AdminSqlMapDao extends SqlMapClientDaoSupport implements AdminDao{

	public Map findAdminByLoginId(String loginId) throws SqlMapException{
	
		return (Map) getSqlMapClientTemplate().queryForObject("Admin.findByLoginId",
				loginId);
	}
}
