package kr.or.copyright.mls.event.controller;

import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.board.model.BoardFile;
import kr.or.copyright.mls.event.model.Event;
import kr.or.copyright.mls.event.service.EventService;
import kr.or.copyright.mls.support.util.FileUploadUtil;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class EventController extends MultiActionController{

	private EventService eventService;
	
	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

	public ModelAndView list(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		//return new ModelAndView("event/eventRenewal_01");
		return new ModelAndView("event/right4me");
	}
	
	public ModelAndView apply(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		return new ModelAndView("event/right4me_apply");
	}
	
	public ModelAndView prize(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		return new ModelAndView("event/right4me_prize");
	}
	
	public ModelAndView getfileSize(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		long reFileSize = 0;
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request); 
		
		Iterator fileNameIterator = multipartRequest.getFileNames();
		String realUploadPath = kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path_event");				// 수신EMAIL
		
		while(fileNameIterator.hasNext()) {
			MultipartFile multiFile= multipartRequest.getFile((String)fileNameIterator.next());
		
			reFileSize = multiFile.getSize();
		}
		
		StringBuffer sb = new StringBuffer();
		
		sb.delete(0, sb.length());
		sb.append("<script type=\"text/JavaScript\">");
		sb.append("parent.frames.setfileSize('"+reFileSize+"');");
		sb.append("</script>");
		
		PrintWriter out = respone.getWriter();
		out.print(sb);
		out.close();		
		
		return null;
			
	}
	
	public ModelAndView checkCampPart(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request); 
		
		Event model = new Event();
		
		bind(multipartRequest, model);
		
		int iFlag = 400;	// 함수 호출시 파라미터 성공:1 실패:-1
		
		int count = eventService.checkCampPart(model);
		
		if( count>0 ) iFlag = -400;
		
		

		StringBuffer sb = new StringBuffer();
		
		sb.delete(0, sb.length());
		sb.append("<script type=\"text/JavaScript\">");
		sb.append("parent.frames.isSucc('"+iFlag+"');");
		sb.append("</script>");
		
		PrintWriter out = respone.getWriter();
		out.print(sb);
		out.close();		
		
		return null;
	}
	
	
	// 등록
	public ModelAndView submitEvent(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request); 
		
		Event model = new Event();
		
		bind(multipartRequest, model);
		
		int iFlag = 100;	// 함수 호출시 파라미터 	
		
		// 처리 결과 값에 따른 처리.
		// -400 : 참가정보 중복
		// -100 : 파일 사이즈 오류
		// -200 : 참가자 정보 입력오류
		// -300 : 설문등록 정보 입력오류
		//  300 : 성공 
		
		// File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		//List fileList = new ArrayList();

		
		String realUploadPath = kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path_event");				// 수신EMAIL
		
		System.out.println(">>>>>>>>>>"+realUploadPath);
		
		while(fileNameIterator.hasNext()) {
			MultipartFile multiFile= multipartRequest.getFile((String)fileNameIterator.next());
		
			if( multiFile.getSize()>0 ) {
				
				// 1. 파일 사이즈 체크 : 5MB 보다 큰경우 return : -100
				if(multiFile.getSize() > (1048576 * 5)) {
				
					iFlag = -100;
				
				} else {
					
					iFlag = 100;
					
					BoardFile boardFile = FileUploadUtil.uploadFormFiles(multiFile, realUploadPath);
					
					model.setCamp_file_name(boardFile.getFileName());
					model.setCamp_file_rnme(boardFile.getRealFileName());
					model.setCamp_file_path(boardFile.getFilePath());
					model.setCamp_file_size(boardFile.getFileSize());
					
				}
			}
		}
		
		
		// 2. 참가자 정보 및 파일 업로드 (insert ml_camp_part_info)
		if(iFlag>0) {
			
			iFlag = eventService.insertCampPartInfo(model);   // 성공:200 실패:-200
			
		} 
		
		// 3. 설문등록 (insert ml_camp_rslt)
		if(iFlag>0) {
			
			for( int k=0; iFlag>0 && k<model.getItem_rslt_arr().length; k++) {
				
				//System.out.println(k+">> "+model.getItem_rslt_arr()[k]);
				
				model.setItem_seqn(model.getItem_seqn_arr()[k]);
				model.setItem_view_seqn(model.getItem_view_seqn_arr()[k]);
				model.setItem_rslt(model.getItem_rslt_arr()[k]);
				model.setItem_rslt_etc(model.getItem_rslt_etc_arr()[k]);
				
				iFlag = eventService.insertCampPartRslt(model);
				
			}
		}
	
		
		StringBuffer sb = new StringBuffer();
		
		sb.delete(0, sb.length());
		sb.append("<script type=\"text/JavaScript\">");
		sb.append("parent.frames.isSucc('"+iFlag+"');");
		sb.append("</script>");
		
		PrintWriter out = respone.getWriter();
		out.print(sb);
		out.close();		
		
		return null;
	}
}
