package kr.or.copyright.mls.console.cnsgn;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.cnsgn.inter.FdcrAd58Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 위탁관리저작물 보고 > 보고 > 저작물보고 등록
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd58Controller extends DefaultController{

	@Resource( name = "fdcrAd58Service" )
	private FdcrAd58Service fdcrAd58Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd58Controller.class );

	/**
	 * 저작물보고 등록 - 보고대상 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/cnsgn/fdcrAd58List1.page" )
	public String fdcrAd58List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd58List1 Start" );
		System.out.println( "commandMap : " + commandMap );
		// 파라미터 셋팅
		/*
		commandMap.put( "ca_id", "" );
		commandMap.put( "cr_id", "" );
		commandMap.put( "SCH_CA_ID", "" );
		commandMap.put( "SCH_COMBO", "" );
		commandMap.put( "SCH_TEXT", "" );
		commandMap.put( "F_DATE", "" );
		commandMap.put( "E_DATE", "" );
		commandMap.put( "FROM_NO", "" );
		commandMap.put( "TO_NO", "" );
		*/
		String SCH_CA_ID = EgovWebUtil.getString( commandMap, "SCH_CA_ID" );
		String GENRE = EgovWebUtil.getString( commandMap, "GENRE" );
		System.out.println("SCH_CA_ID:::::::::::::"+SCH_CA_ID);
		System.out.println("GENRE:::::::::::::"+GENRE);
		
		fdcrAd58Service.fdcrAd58List1( commandMap );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		model.addAttribute( "ds_page2", commandMap.get( "ds_page2" ) );
		model.addAttribute( "ds_report_List", commandMap.get( "ds_report_List" ) );
		model.addAttribute( "ds_date", commandMap.get( "ds_date" ) );
		model.addAttribute( "commandMap", commandMap );
		System.out.println( "commandMap : " + commandMap );
		logger.debug( "fdcrAd58List1 End" );
		return "cnsgn/fdcrAd58List1.tiles";
	}

	/**
	 * 저작물조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/cnsgn/fdcrAd58List2.page" )
	public String fdcrAd58List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd58List2 Start" );
		// 파라미터 셋팅
		commandMap.put( "USER_ID", "" );
		commandMap.put( "NAME", "" );
		commandMap.put( "JOB", "" );
		commandMap.put( "PHONENUM", "" );
		commandMap.put( "GENRE", "" );
		commandMap.put( "SCH_CA_ID", "" );
		commandMap.put( "SCH_COMBO", "" );
		commandMap.put( "SCH_TEXT", "" );
		commandMap.put( "F_DATE", "" );
		commandMap.put( "E_DATE", "" );

		fdcrAd58Service.fdcrAd58List2( commandMap );
		model.addAttribute( "ds_target_music", commandMap.get( "ds_target_music" ) );
		model.addAttribute( "ds_target_book", commandMap.get( "ds_pads_target_bookge2" ) );
		model.addAttribute( "commandMap", commandMap );
		logger.debug( "fdcrAd58List2 End" );
		return "test";
	}

	/**
	 * 일괄등록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/cnsgn/fdcrAd58Insert1.page" )
	public void fdcrAd58Insert1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd58Insert1 Start" );
		// 파라미터 셋팅
		commandMap.put( "USER_ID", "" );
		commandMap.put( "NAME", "" );
		commandMap.put( "JOB", "" );
		commandMap.put( "PHONENUM", "" );
		commandMap.put( "GENRE", "" );
		commandMap.put( "SCH_CA_ID", "" );
		commandMap.put( "SCH_COMBO", "" );
		commandMap.put( "SCH_TEXT", "" );
		commandMap.put( "F_DATE", "" );
		commandMap.put( "E_DATE", "" );
		ArrayList<Map<String, Object>> ds_target = new ArrayList<Map<String, Object>>();
		int genreCode = EgovWebUtil.getToInt( commandMap, "GENRE" );
		String[] names = null;
		switch( genreCode ){
			case 1:
				names =
					new String[]{
							"RGST_ORGN_NAME",
							"COMM_WORKS_ID",
							"WORKS_TITLE",
							"NATION_CD",
							"ALBUM_TITLE",
							"ALBUM_LABLE",
							"DISK_SIDE",
							"TRACK_NO",
							"ALBUM_ISSU_YEAR",
							"LYRC",
							"COMP",
							"ARRA",
							"TRAN",
							"SING",
							"PERF",
							"PROD",
							"COMM_MGNT_CD",
							"LICE_MGNT_CD",
							"PERF_MGNT_CD",
							"PROD_MGNT_CD",
							"CHK",
							"NUM",
							"ROW_ERR_CNT",
							"DUP_YN",
							"REPT_YMD",
							"GENRE",
							"RGST_IDNT",
							"WORKS_ID" };
				break;// 음악
			case 2:
				names =
					new String[]{
							"RGST_ORGN_NAME",
							"COMM_WORKS_ID",
							"WORKS_TITLE",
							"WORKS_SUB_TITLE",
							"CRT_YEAR",
							"BOOK_TITLE",
							"BOOK_PUBLISHER",
							"BOOK_ISSU_YEAR",
							"COPT_NAME",
							"COMM_MGNT_CD",
							"CHK",
							"NUM",
							"ROW_ERR_CNT",
							"DUP_YN" };
				break;// 어문
			default:
				throw new Exception( "장르코드 Error OR Null : worksMgntInsert" );
		}
		ds_target = EgovWebUtil.convertFromArrToArray( request, names );
		commandMap.put( "ds_target", ds_target );
		boolean isSuccess = fdcrAd58Service.fdcrAd58Insert1( commandMap );
		returnAjaxString( response, isSuccess );
		logger.debug( "fdcrAd58Insert1 End" );
	}

	/**
	 * 일괄 수정
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/cnsgn/fdcrAd58Update1.page" )
	public void fdcrAd58Update1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd58Insert1 Start" );
		// 파라미터 셋팅
		commandMap.put( "USER_ID", "" );
		commandMap.put( "NAME", "" );
		commandMap.put( "JOB", "" );
		commandMap.put( "PHONENUM", "" );
		commandMap.put( "GENRE", "" );
		commandMap.put( "SCH_CA_ID", "" );
		commandMap.put( "SCH_COMBO", "" );
		commandMap.put( "SCH_TEXT", "" );
		commandMap.put( "F_DATE", "" );
		commandMap.put( "E_DATE", "" );
		ArrayList<Map<String, Object>> ds_target = new ArrayList<Map<String, Object>>();
		int genreCode = EgovWebUtil.getToInt( commandMap, "GENRE" );
		String[] names = null;
		switch( genreCode ){
			case 1:
				names =
					new String[]{
							"RGST_ORGN_NAME",
							"COMM_WORKS_ID",
							"WORKS_TITLE",
							"NATION_CD",
							"ALBUM_TITLE",
							"ALBUM_LABLE",
							"DISK_SIDE",
							"TRACK_NO",
							"ALBUM_ISSU_YEAR",
							"LYRC",
							"COMP",
							"ARRA",
							"TRAN",
							"SING",
							"PERF",
							"PROD",
							"COMM_MGNT_CD",
							"LICE_MGNT_CD",
							"PERF_MGNT_CD",
							"PROD_MGNT_CD",
							"CHK",
							"NUM",
							"ROW_ERR_CNT",
							"DUP_YN",
							"REPT_YMD",
							"GENRE",
							"RGST_IDNT",
							"WORKS_ID" };
				break;// 음악
			case 2:
				names =
					new String[]{
							"RGST_ORGN_NAME",
							"COMM_WORKS_ID",
							"WORKS_TITLE",
							"WORKS_SUB_TITLE",
							"CRT_YEAR",
							"BOOK_TITLE",
							"BOOK_PUBLISHER",
							"BOOK_ISSU_YEAR",
							"COPT_NAME",
							"COMM_MGNT_CD",
							"CHK",
							"NUM",
							"ROW_ERR_CNT",
							"DUP_YN" };
				break;// 어문
			default:
				throw new Exception( "장르코드 Error OR Null : worksMgntInsert" );
		}
		ds_target = EgovWebUtil.convertFromArrToArray( request, names );
		commandMap.put( "ds_target", ds_target );
		boolean isSuccess = fdcrAd58Service.fdcrAd58Update1( commandMap );
		returnAjaxString( response, isSuccess );
		logger.debug( "fdcrAd58Insert1 End" );
	}
}
