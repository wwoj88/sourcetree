package kr.or.copyright.mls.console.legal;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.adminCommon.dao.AdminCommonDao;
import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Service;
import kr.or.copyright.mls.myStat.model.StatApplication;
import kr.or.copyright.mls.myStat.model.StatApplicationShis;
import kr.or.copyright.mls.myStat.model.StatApplyWorks;
import kr.or.copyright.mls.myStat.service.MyStatService;
import kr.or.copyright.mls.support.util.SessionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

/**
 * 법정허락 > 법정허락 이용승인신청 현황
 * 
 * @author ljh
 */
@Controller
public class FdcrAd06Controller extends DefaultController {

     @Resource(name = "fdcrAd06Service")
     private FdcrAd06Service fdcrAd06Service;

     @Resource(name = "myStatService")
     private MyStatService myStatService;

     @Resource(name = "consoleCommonService")
     private ConsoleCommonService consoleCommonService;
     

     @Resource(name = "adminCommonDao")
     private AdminCommonDao adminCommonDao;

     private static final Logger logger = LoggerFactory.getLogger(FdcrAd06Controller.class);

     /**
      * 법정허락 이용승인신청 목록
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06List1.page")
     public String fdcrAd06List1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          String GUBUN1 = EgovWebUtil.getString(commandMap, "GUBUN1");
          String SCH_APPLY_TYPE = EgovWebUtil.getString(commandMap, "SCH_APPLY_TYPE");
          String SCH_STAT_CD = EgovWebUtil.getString(commandMap, "SCH_STAT_CD");
          String SCH_NO = EgovWebUtil.getString(commandMap, "SCH_NO");
          String SCH_APPLY_WORKS_TITL = EgovWebUtil.getString(commandMap, "SCH_APPLY_WORKS_TITL");

          if (!"".equals(GUBUN1)) {
               GUBUN1 = URLDecoder.decode(GUBUN1, "UTF-8");
               commandMap.put("GUBUN1", GUBUN1);
          }
          if (!"".equals(SCH_APPLY_TYPE)) {
               SCH_APPLY_TYPE = URLDecoder.decode(SCH_APPLY_TYPE, "UTF-8");
               commandMap.put("SCH_APPLY_TYPE", SCH_APPLY_TYPE);
          }
          if (!"".equals(SCH_STAT_CD)) {
               SCH_STAT_CD = URLDecoder.decode(SCH_STAT_CD, "UTF-8");
               commandMap.put("SCH_STAT_CD", SCH_STAT_CD);
          }
          if (!"".equals(SCH_NO)) {
               SCH_NO = URLDecoder.decode(SCH_NO, "UTF-8");
               commandMap.put("SCH_NO", SCH_NO);
          }
          if (!"".equals(SCH_APPLY_WORKS_TITL)) {
               SCH_APPLY_WORKS_TITL = URLDecoder.decode(SCH_APPLY_WORKS_TITL, "UTF-8");
               commandMap.put("SCH_APPLY_WORKS_TITL", SCH_APPLY_WORKS_TITL);
          }

          ArrayList<Map<String, Object>> list = fdcrAd06Service.fdcrAd06List1(commandMap);

          model.addAttribute("list", list);
          model.addAttribute("commandMap", commandMap);
          model.addAttribute("ds_count", commandMap.get("ds_count"));
          model.addAttribute("paginationInfo", commandMap.get("paginationInfo"));

          System.out.println("법정허락 이용승인 신청 현황 목록");
          return "legal/fdcrAd06List1.tiles";
     }

     /**
      * 법정허락 이용승인신청 상세
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06View1.page")
     public String fdcrAd06View1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          String WORKS_SEQN = EgovWebUtil.getString(commandMap, "WORKS_SEQN");

          Map<String, Object> info = fdcrAd06Service.fdcrAd06View1(commandMap);
          model.addAttribute("info", info);

          ArrayList<Map<String, Object>> attachList = fdcrAd06Service.selectAttachFileList(commandMap);
          model.addAttribute("attachList", attachList);

          if (WORKS_SEQN.equals("") || WORKS_SEQN == null) {
               commandMap.put("WORKS_SEQN", "1");
          } else {
               commandMap.put("WORKS_SEQN", WORKS_SEQN);
          }
          try {
               // 법정허락 이용승인신청 명세서 목록
               ArrayList<Map<String, Object>> applyWorks = fdcrAd06Service.adminStatApplyWorksSelect(commandMap);
               model.addAttribute("applyWorksList", applyWorks);
          } catch (Exception e) {
               e.printStackTrace();
          }
          try {
               // 법정허락 이용승인신청 명세서 상세
               ArrayList<Map<String, Object>> applyWorksInfo = fdcrAd06Service.adminStatApplyWorksSelectInfo(commandMap);
               model.addAttribute("applyWorksInfo", applyWorksInfo);
          } catch (Exception e) {
               e.printStackTrace();
          }
          
          

          
          String USER_IDNT = request.getParameter("USER_IDNT");
          HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
          String ip = req.getHeader("X-FORWARDED-FOR");
          if (ip == null) {
               ip = req.getRemoteAddr();
          }
          Map logMap = new HashMap();
          System.out.println(request.getRequestURI());

          logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
          logMap.put("PROC_STATUS", "법정허락 이용승인 신청 " );
          logMap.put("PROC_ID",  " 법정허락 이용승인 신청  상세조회");
          logMap.put("MENU_URL", request.getRequestURI());
          logMap.put("IP_ADDRESS", ip);

          adminCommonDao.insertAdminLogDo(logMap);
          System.out.println("법정허락 이용승인 신청 현황 상세");
          return "legal/fdcrAd06View1.tiles";
     }

     /**
      * 법정허락 이용승인신청 신청서 수정 폼
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06UpdateForm1.page")
     public String fdcrAd06UpdateForm1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          /*
           * fdcrAd06Service.fdcrAd06UpdateForm1( commandMap ); List ds_list = (List) commandMap.get(
           * "ds_list" ); model.addAttribute( "ds_list", ds_list );
           */
          String WORKS_SEQN = EgovWebUtil.getString(commandMap, "WORKS_SEQN");

          Map<String, Object> info = fdcrAd06Service.fdcrAd06View1(commandMap);
          model.addAttribute("info", info);

          ArrayList<Map<String, Object>> attachList = fdcrAd06Service.selectAttachFileList(commandMap);
          model.addAttribute("attachList", attachList);

          if (WORKS_SEQN.equals("") || WORKS_SEQN == null) {
               commandMap.put("WORKS_SEQN", "1");
          } else {
               commandMap.put("WORKS_SEQN", WORKS_SEQN);
          }

          // 법정허락 이용승인신청 명세서 목록
          ArrayList<Map<String, Object>> applyWorks = fdcrAd06Service.adminStatApplyWorksSelect(commandMap);
          model.addAttribute("applyWorksList", applyWorks);

          // 법정허락 이용승인신청 명세서 상세
          ArrayList<Map<String, Object>> applyWorksInfo = fdcrAd06Service.adminStatApplyWorksSelectInfo(commandMap);
          model.addAttribute("applyWorksInfo", applyWorksInfo);


          System.out.println("법정허락 이용승인 신청 현황 수정화면");
          return "legal/fdcrAd06UpdateForm1.tiles";
     }

     /**
      * 법정허락 이용승인신청 신청서 수정
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06Update1.page")
     public String fdcrAd06Update1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          String APPLY_WRITE_YMD = EgovWebUtil.getString(commandMap, "APPLY_WRITE_YMD");
          String APPLY_WRITE_SEQ = EgovWebUtil.getString(commandMap, "APPLY_WRITE_SEQ");
          String WORKS_SEQN = EgovWebUtil.getString(commandMap, "WORKS_SEQN");
          String ATTC_SEQN = EgovWebUtil.getString(commandMap, "ATTC_SEQN");
          commandMap.put("RGST_IDNT", ConsoleLoginUser.getUserId());
          // List ds_stat_appl = (List)commandMap.get("ds_stat_appl");
          // model.addAttribute("ds_stat_appl", ds_stat_appl);
          System.out.println("법정허락 이용승인 신청 현황 수정");

          boolean isSuccess = fdcrAd06Service.fdcrAd06Update1(commandMap);
          returnAjaxString(response, isSuccess);

          if (isSuccess) {
               
               String USER_IDNT = request.getParameter("USER_IDNT");
               HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
               String ip = req.getHeader("X-FORWARDED-FOR");
               if (ip == null) {
                    ip = req.getRemoteAddr();
               }
               Map logMap = new HashMap();
               System.out.println(request.getRequestURI());

               logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
               logMap.put("PROC_STATUS", "법정허락 이용승인 신청 " );
               logMap.put("PROC_ID", " 법정허락 이용승인 신청  수정");
               logMap.put("MENU_URL", request.getRequestURI());
               logMap.put("IP_ADDRESS", ip);

               adminCommonDao.insertAdminLogDo(logMap);
               
               return returnUrl(model, "저장했습니다.", "/console/legal/fdcrAd06View1.page?APPLY_WRITE_YMD=" + APPLY_WRITE_YMD + "&APPLY_WRITE_SEQ=" + APPLY_WRITE_SEQ);
          } else {
               return returnUrl(model, "실패했습니다.", "/console/legal/fdcrAd06View1.page?APPLY_WRITE_YMD=" + APPLY_WRITE_YMD + "&APPLY_WRITE_SEQ=" + APPLY_WRITE_SEQ);
          }
     }

     /**
      * 법정허락 이용승인신청 신청서 삭제
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06Delete1.page")
     public String fdcrAd06Delete1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          commandMap.put("STAT_CD", "11");
          commandMap.put("RGST_IDNT", ConsoleLoginUser.getUserId());
          String APPLY_WRITE_YMD = EgovWebUtil.getString(commandMap, "APPLY_WRITE_YMD");
          String APPLY_WRITE_SEQ = EgovWebUtil.getString(commandMap, "APPLY_WRITE_SEQ");

          System.out.println("법정허락 이용승인 신청 현황 삭제");
          boolean isSuccess = fdcrAd06Service.fdcrAd06Delete1(commandMap);
          returnAjaxString(response, isSuccess);

          if (isSuccess) {
               
               String USER_IDNT = request.getParameter("USER_IDNT");
               HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
               String ip = req.getHeader("X-FORWARDED-FOR");
               if (ip == null) {
                    ip = req.getRemoteAddr();
               }
               Map logMap = new HashMap();
               System.out.println(request.getRequestURI());

               logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
               logMap.put("PROC_STATUS", "법정허락 이용승인 신청 " );
               logMap.put("PROC_ID",   " 법정허락 이용승인 신청  삭제");
               logMap.put("MENU_URL", request.getRequestURI());
               logMap.put("IP_ADDRESS", ip);

               adminCommonDao.insertAdminLogDo(logMap);
               
               return returnUrl(model, "삭제했습니다.", "/console/legal/fdcrAd06List1.page");
          } else {
               return returnUrl(model, "실패했습니다.", "/console/legal/fdcrAd06List1.page");
          }
     }

     /**
      * 법정허락 이용승인신청서, 이용승인신청명세서 출력
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06Report1.page")
     public ModelAndView fdcrAd06Report1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          StatApplication srchParam = new StatApplication();
          /* 조회조건 str */
          srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(request, "srchApplyType"));
          srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(request, "srchApplyFrDt"));
          srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(request, "srchApplyToDt"));
          srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(request, "srchApplyWorksTitl"));
          srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(request, "srchStatCd", 0));
          srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
          /* 조회조건 end */

          String mode = ServletRequestUtils.getStringParameter(request, "mode", ""); // R
          // :
          // 레포팅화면
          String report = ServletRequestUtils.getStringParameter(request, "report", ""); // 레포팅
          // 파일
          String modal = ServletRequestUtils.getStringParameter(request, "modal", "0"); // 레포팅
          // 파일
          // 모달구분

          User user = SessionUtil.getSession(request);

          if (!mode.equals("R")) {
               if (user == null) {
                    ModelAndView mv = new ModelAndView("redirect:/user/user.page?method=goSgInstall");
                    return mv;
               }
          }

          // 공통코드 조회 시작
          // 신청서구분 코드 조회
          CodeList code = new CodeList();
          code.setBigCode("50");
          List applyTypeList = consoleCommonService.commonCodeList(code);

          // 공표방법 코드 조회
          code.setBigCode("52");
          List publMediList = consoleCommonService.commonCodeList(code);

          // 신청서 상태
          int statCd = ServletRequestUtils.getIntParameter(request, "stat_cd", 0);

          String applyWriteYmd = ServletRequestUtils.getStringParameter(request, "apply_write_ymd");
          int applyWriteSeq = ServletRequestUtils.getIntParameter(request, "apply_write_seq", 0);

          StatApplication param = new StatApplication();
          param.setApplyWriteYmd(applyWriteYmd);
          param.setApplyWriteSeq(applyWriteSeq);

          // 이용승인 신청서 조회
          StatApplication statApplication = myStatService.statApplication(param);

          // 이용승인 신청서 첨부파일 조회
          List fileList = myStatService.statAttcFile(param);
          statApplication.setFileList(fileList);

          StatApplicationShis statApplicationShis = new StatApplicationShis();
          statApplicationShis.setApplyWriteYmd(applyWriteYmd);
          statApplicationShis.setApplyWriteSeq(applyWriteSeq);

          // 이용승인 신청서 상태변경 내역 조회
          List sHisList = myStatService.statApplicationShisList(statApplicationShis);

          StatApplyWorks statApplyWorks = new StatApplyWorks();
          statApplyWorks.setApplyWriteYmd(applyWriteYmd);
          statApplyWorks.setApplyWriteSeq(applyWriteSeq);
          // 이용승인 명세서 조회
          List statApplyWorksList = myStatService.statApplyWorksList(statApplyWorks);

          ModelAndView mv = new ModelAndView("myStat/statPrpsView");

          // 레포팅
          if (mode.equals("R")) {
               mv = new ModelAndView(report);
               mv.addObject("modal", modal);
          }

          mv.addObject("statApplication", statApplication);
          mv.addObject("sHisList", sHisList);
          mv.addObject("statApplyWorksList", statApplyWorksList);
          mv.addObject("publMediList", publMediList);

          mv.addObject("srchParam", srchParam);
          mv.addObject("applyTypeList", applyTypeList);

          return mv;
     }

     /**
      * 법정허락 이용승인신청 진행상태 내역조회
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06History1.page")
     public String fdcrAd06History1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          fdcrAd06Service.fdcrAd06History1(commandMap);
          List ds_list = (List) commandMap.get("ds_list");
          model.addAttribute("ds_list", ds_list);

          System.out.println("법정허락 이용승인 신청 진행상태 내역조회");
          return "legal/fdcrAd06Pop1.popup";
     }

     /**
      * 법정허락 이용승인신청 진행상태 수정
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06StatChange1.page")
     public String fdcrAd06StatChange1(ModelMap model, Map<String, Object> commandMap, MultipartHttpServletRequest mreq, HttpServletRequest request, HttpServletResponse response) throws Exception {

          String APPLY_WRITE_YMD = EgovWebUtil.getString(commandMap, "APPLY_WRITE_YMD");
          String APPLY_WRITE_SEQ = EgovWebUtil.getString(commandMap, "APPLY_WRITE_SEQ");
          String RECEIPT_NO = EgovWebUtil.getString(commandMap, "RECEIPT_NO");
          String RGST_IDNT = EgovWebUtil.getString(commandMap, "RGST_IDNT");
          String STAT_CD = EgovWebUtil.getString(commandMap, "STAT_CD");
          String STAT_MEMO = EgovWebUtil.getString(commandMap, "STAT_MEMO");
          String ATTC_SEQN = EgovWebUtil.getString(commandMap, "file1");
          commandMap.put("USER_ID", ConsoleLoginUser.getUserId());

          System.out.println("APPLY_WRITE_YMD:::::::::" + APPLY_WRITE_YMD);
          System.out.println("APPLY_WRITE_SEQ:::::::::" + APPLY_WRITE_SEQ);
          System.out.println("RECEIPT_NO::::::::" + RECEIPT_NO);
          System.out.println("STAT_CD::::::::" + STAT_CD);
          System.out.println("STAT_MEMO::::::::" + STAT_MEMO);
          System.out.println("ATTC_SEQN::::::::" + ATTC_SEQN);
          System.out.println("RGST_IDNT::::::::" + RGST_IDNT);
          // 파일 업로드
          String uploadPath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path"));
          ArrayList<Map<String, Object>> fileinfo = fileUpload(mreq, uploadPath, null, true);

          boolean isSuccess = fdcrAd06Service.fdcrAd06StatChange1(commandMap, fileinfo);
          returnAjaxString(response, isSuccess);
          System.out.println("법정허락 이용승인 신청 진행상태 수정");

          if (isSuccess) {
               return returnUrl(model, "진행상태를 수정했습니다.", "/console/legal/fdcrAd06List1.page");
          } else {
               return returnUrl(model, "실패했습니다.", "/console/legal/fdcrAd06List1.page");
          }
     }

     /**
      * 법정허락 이용승인신청 심의결과 내역조회
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06History2.page")
     public String fdcrAd06History2(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          fdcrAd06Service.fdcrAd06History2(commandMap);
          List ds_list = (List) commandMap.get("ds_list");
          model.addAttribute("ds_list", ds_list);

          System.out.println("법정허락 이용승인 신청 심의결과 내역조회");
          return "legal/fdcrAd06Pop2.popup";
     }

     /**
      * 법정허락 이용승인신청 심의결과 수정
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06StatWorksChange.page")
     public String fdcrAd06StatWorksChange1(ModelMap model, Map<String, Object> commandMap, MultipartHttpServletRequest mreq, HttpServletRequest request, HttpServletResponse response)
               throws Exception {

          /*
           * commandMap.put( "ATTACH_FILE_PATH", request.getParameterValues( "ATTACH_FILE_PATH" ) );
           * commandMap.put( "ATTACH_FILE_SIZE", request.getParameterValues( "ATTACH_FILE_SIZE" ) );
           * commandMap.put( "ATTACH_FILE_CONTENT", request.getParameterValues( "ATTACH_FILE_CONTENT" ) );
           * commandMap.put( "RGST_IDNT", request.getParameterValues( "RGST_IDNT" ) ); commandMap.put(
           * "APPLY_WRITE_YMD", request.getParameterValues( "APPLY_WRITE_YMD" ) ); commandMap.put(
           * "APPLY_WRITE_SEQ", request.getParameterValues( "APPLY_WRITE_SEQ" ) ); commandMap.put( "USER_ID",
           * request.getParameterValues( "USER_ID" ) ); commandMap.put( "STAT_RSLT_CD",
           * request.getParameterValues( "STAT_RSLT_CD" ) ); commandMap.put( "LGMT_AMNT",
           * request.getParameterValues( "LGMT_AMNT" ) ); commandMap.put( "LGMT_PLAC_NAME",
           * request.getParameterValues( "LGMT_PLAC_NAME" ) ); commandMap.put( "LGMT_GRAN",
           * request.getParameterValues( "LGMT_GRAN" ) ); commandMap.put( "LGMT_PERI",
           * request.getParameterValues( "LGMT_PERI" ) ); commandMap.put( "WORKS_SEQN",
           * request.getParameterValues( "WORKS_SEQN" ) ); commandMap.put( "STAT_RSLT_MEMO",
           * request.getParameterValues( "STAT_RSLT_MEMO" ) ); commandMap.put( "ATTC_SEQN",
           * request.getParameterValues( "ATTC_SEQN" ) );
           * 
           * fdcrAd06Service.fdcrAd06StatWorksChange1( commandMap );
           * 
           * System.out.println( "법정허락 이용승인 신청 심의결과 수정" );
           */
          String APPLY_WRITE_YMD = EgovWebUtil.getString(commandMap, "APPLY_WRITE_YMD");
          String APPLY_WRITE_SEQ = EgovWebUtil.getString(commandMap, "APPLY_WRITE_SEQ");
          String WORKS_SEQN = EgovWebUtil.getString(commandMap, "WORKS_SEQN");
          String RECEIPT_NO = EgovWebUtil.getString(commandMap, "RECEIPT_NO");
          String STAT_RSLT_CD = EgovWebUtil.getString(commandMap, "STAT_RSLT_CD");
          String STAT_RSLT_MEMO = EgovWebUtil.getString(commandMap, "STAT_RSLT_MEMO");
          String ATTC_SEQN = EgovWebUtil.getString(commandMap, "file2");
          String LGMT_AMNT = EgovWebUtil.getString(commandMap, "LGMT_AMNT");
          String LGMT_PLAC_NAME = EgovWebUtil.getString(commandMap, "LGMT_PLAC_NAME");
          String LGMT_GRAN = EgovWebUtil.getString(commandMap, "LGMT_GRAN");
          String LGMT_PERI = EgovWebUtil.getString(commandMap, "LGMT_PERI");
          commandMap.put("USER_ID", ConsoleLoginUser.getUserId());

          // 파일 업로드
          String uploadPath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path"));
          ArrayList<Map<String, Object>> fileinfo = fileUpload(mreq, uploadPath, null, true);

          boolean isSuccess = fdcrAd06Service.fdcrAd06StatWorksChange1(commandMap, fileinfo);
          returnAjaxString(response, isSuccess);
          System.out.println("법정허락 이용승인 신청 심의결과 수정");

          if (isSuccess) {
               return returnUrl(model, "심의결과 상태를 수정했습니다.", "/console/legal/fdcrAd06List1.page");
          } else {
               return returnUrl(model, "실패했습니다.", "/console/legal/fdcrAd06List1.page");
          }
     }

     /**
      * 법정허락 이용승인신청공고 게시판 등록 팝업 폼
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06Pop3.page")
     public String fdcrAd06Pop3(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          String WORKS_SEQN = EgovWebUtil.getString(commandMap, "WORKS_SEQN");
          String APPLY_WRITE_YMD = EgovWebUtil.getString(commandMap, "APPLY_WRITE_YMD");
          String APPLY_WRITE_SEQ = EgovWebUtil.getString(commandMap, "APPLY_WRITE_SEQ");

          Map<String, Object> info = fdcrAd06Service.fdcrAd06View1(commandMap);
          model.addAttribute("info", info);

          // 법정허락 이용승인신청 명세서 목록
          ArrayList<Map<String, Object>> applyWorks = fdcrAd06Service.adminStatApplyWorksSelect(commandMap);
          model.addAttribute("applyWorksList", applyWorks);

          // 법정허락 이용승인신청 명세서 상세
          ArrayList<Map<String, Object>> applyWorksInfo = fdcrAd06Service.adminStatApplyWorksSelectInfo(commandMap);
          model.addAttribute("applyWorksInfo", applyWorksInfo);

          System.out.println("법정허락 이용승인신청공고 게시판 등록 팝업 화면");
          return "legal/fdcrAd06Pop3.popup";
     }

     /**
      * 법정허락 이용승인신청공고 게시판 등록
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06Regi1.page")
     public String fdcrAd06Regi1(ModelMap model, Map<String, Object> commandMap, MultipartHttpServletRequest mreq, HttpServletRequest request, HttpServletResponse response) throws Exception {

          commandMap.put("BORD_CD", "3");
          commandMap.put("OPEN_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("RGST_IDNT", ConsoleLoginUser.getUserId());

          String APPLY_WRITE_YMD = EgovWebUtil.getString(commandMap, "APPLY_WRITE_YMD");
          String APPLY_WRITE_SEQ = EgovWebUtil.getString(commandMap, "APPLY_WRITE_SEQ");

          // 넘긴 파라미터
          String TITE = EgovWebUtil.getString(commandMap, "TITE");
          String DIVS_CD = EgovWebUtil.getString(commandMap, "DIVS_CD");
          String RECEIPT_NO = EgovWebUtil.getString(commandMap, "RECEIPT_NO");
          String ANUC_ITEM_1 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_1");
          String ANUC_ITEM_2 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_2");
          String ANUC_ITEM_3 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_3");
          String ANUC_ITEM_4 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_4");
          String ANUC_ITEM_5 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_5");
          String ANUC_ITEM_6 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_6");
          String ANUC_ITEM_7 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_7");
          String ANUC_ITEM_8 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_8");
          String ANUC_ITEM_9 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_9");
          String ANUC_ITEM_10 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_10");
          String ANUC_ITEM_11 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_11");
          System.out.println("법정허락 이용승인신청공고 게시판 등록");

          // 파일 업로드
          String uploadPath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path"));
          ArrayList<Map<String, Object>> fileinfo = fileUpload(mreq, uploadPath, null, true);

          boolean isSuccess = fdcrAd06Service.fdcrAd06Regi1(commandMap, fileinfo);
          if (isSuccess) {
               return returnUrl(model, "저장했습니다.", "/console/legal/fdcrAd06View1.page?APPLY_WRITE_YMD=" + APPLY_WRITE_YMD + "&APPLY_WRITE_SEQ=" + APPLY_WRITE_SEQ);
          } else {
               return returnUrl(model, "실패했습니다.", "/console/legal/fdcrAd06View1.page?APPLY_WRITE_YMD=" + APPLY_WRITE_YMD + "&APPLY_WRITE_SEQ=" + APPLY_WRITE_SEQ);
          }
     }

     /**
      * 법정허락 이용승인신청공고 게시판 수정
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06Modi1.page")
     public void fdcrAd06Modi1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response, MultipartHttpServletRequest mreq) throws Exception {

          commandMap.put("BORD_CD", "");
          commandMap.put("TITE", "");
          commandMap.put("DIVS_CD", "");
          commandMap.put("WORKS_ID", "");
          commandMap.put("RECEIPT_NO", "");
          commandMap.put("ANUC_ITEM_1", "");
          commandMap.put("ANUC_ITEM_2", "");
          commandMap.put("ANUC_ITEM_3", "");
          commandMap.put("ANUC_ITEM_4", "");
          commandMap.put("ANUC_ITEM_5", "");
          commandMap.put("ANUC_ITEM_6", "");
          commandMap.put("ANUC_ITEM_7", "");
          commandMap.put("ANUC_ITEM_8", "");
          commandMap.put("ANUC_ITEM_9", "");
          commandMap.put("ANUC_ITEM_10", "");
          commandMap.put("ANUC_ITEM_11", "");
          commandMap.put("OPEN_IDNT", "");
          commandMap.put("RGST_IDNT", "");
          fdcrAd06Service.fdcrAd06Modi1(commandMap);
          System.out.println("법정허락 이용승인신청공고 게시판 수정");
     }

     /**
      * 법정허락 이용승인공고 게시판 등록 팝업 폼
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06Pop4.page")
     public String fdcrAd06Pop4(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          /*
           * commandMap.put( "APPLR_NAME", "" ); commandMap.put( "WORKS_TITL", "" ); commandMap.put(
           * "COPT_HODR_NAME", "" ); commandMap.put( "RECEIPT_NO", "" ); commandMap.put( "BORD_CD", "" );
           * commandMap.put( "BORD_SEQN", "" ); commandMap.put( "OPEN_NAME", "" ); commandMap.put(
           * "UPDATE_FG", "" ); fdcrAd06Service.fdcrAd06RegiForm2( commandMap );
           * 
           * List detailList = (List) commandMap.get( "ds_list" ); List fileList = (List) commandMap.get(
           * "ds_file" ); List suplList = (List) commandMap.get( "ds_supl_list" ); model.addAttribute(
           * "detailList", detailList ); model.addAttribute( "fileList", fileList ); model.addAttribute(
           * "suplList", suplList );
           */

          String WORKS_SEQN = EgovWebUtil.getString(commandMap, "WORKS_SEQN");
          String APPLY_WRITE_YMD = EgovWebUtil.getString(commandMap, "APPLY_WRITE_YMD");
          String APPLY_WRITE_SEQ = EgovWebUtil.getString(commandMap, "APPLY_WRITE_SEQ");

          Map<String, Object> info = fdcrAd06Service.fdcrAd06View1(commandMap);
          model.addAttribute("info", info);

          // 법정허락 이용승인신청 명세서 목록
          ArrayList<Map<String, Object>> applyWorks = fdcrAd06Service.adminStatApplyWorksSelect(commandMap);
          model.addAttribute("applyWorksList", applyWorks);

          // 법정허락 이용승인신청 명세서 상세
          ArrayList<Map<String, Object>> applyWorksInfo = fdcrAd06Service.adminStatApplyWorksSelectInfo(commandMap);
          model.addAttribute("applyWorksInfo", applyWorksInfo);

          System.out.println("법정허락 이용승인공고 게시판 등록 팝업 화면");
          return "legal/fdcrAd06Pop4.popup";
     }

     /**
      * 법정허락 이용승인공고 게시판 등록
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06Regi2.page")
     public String fdcrAd06Regi2(ModelMap model, Map<String, Object> commandMap, MultipartHttpServletRequest mreq, HttpServletRequest request, HttpServletResponse response) throws Exception {

          /*
           * commandMap.put( "BORD_CD", "" ); commandMap.put( "WORKS_ID", "" ); commandMap.put( "DIVS_CD", ""
           * ); commandMap.put( "RECEIPT_NO", "" ); commandMap.put( "TITE", "" ); commandMap.put(
           * "ANUC_ITEM_1", "" ); commandMap.put( "ANUC_ITEM_2", "" ); commandMap.put( "ANUC_ITEM_3", "" );
           * commandMap.put( "ANUC_ITEM_4", "" ); commandMap.put( "ANUC_ITEM_5", "" ); commandMap.put(
           * "ANUC_ITEM_6", "" ); commandMap.put( "ANUC_ITEM_7", "" ); commandMap.put( "RGST_IDNT", "" );
           * commandMap.put( "OPEN_IDNT", "" ); fdcrAd06Service.fdcrAd06Regi2( commandMap );
           * System.out.println( "법정허락 이용승인공고 게시판 등록" );
           */

          commandMap.put("BORD_CD", "4");
          commandMap.put("OPEN_IDNT", ConsoleLoginUser.getUserId());
          commandMap.put("RGST_IDNT", ConsoleLoginUser.getUserId());

          String APPLY_WRITE_YMD = EgovWebUtil.getString(commandMap, "APPLY_WRITE_YMD");
          String APPLY_WRITE_SEQ = EgovWebUtil.getString(commandMap, "APPLY_WRITE_SEQ");

          // 넘긴 파라미터
          String TITE = EgovWebUtil.getString(commandMap, "TITE");
          String DIVS_CD = EgovWebUtil.getString(commandMap, "DIVS_CD");
          String RECEIPT_NO = EgovWebUtil.getString(commandMap, "RECEIPT_NO");
          String ANUC_ITEM_1 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_1");
          String ANUC_ITEM_2 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_2");
          String ANUC_ITEM_3 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_3");
          String ANUC_ITEM_4 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_4");
          String ANUC_ITEM_5 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_5");
          String ANUC_ITEM_6 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_6");
          String ANUC_ITEM_7 = EgovWebUtil.getString(commandMap, "ANUC_ITEM_7");
          System.out.println("법정허락 이용승인공고 게시판 등록");

          // 파일 업로드
          String uploadPath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path"));
          ArrayList<Map<String, Object>> fileinfo = fileUpload(mreq, uploadPath, null, true);

          boolean isSuccess = fdcrAd06Service.fdcrAd06Regi2(commandMap, fileinfo);
          if (isSuccess) {
               return returnUrl(model, "저장했습니다.", "/console/legal/fdcrAd06View1.page?APPLY_WRITE_YMD=" + APPLY_WRITE_YMD + "&APPLY_WRITE_SEQ=" + APPLY_WRITE_SEQ);
          } else {
               return returnUrl(model, "실패했습니다.", "/console/legal/fdcrAd06View1.page?APPLY_WRITE_YMD=" + APPLY_WRITE_YMD + "&APPLY_WRITE_SEQ=" + APPLY_WRITE_SEQ);
          }
     }

     /**
      * 법정허락 이용승인공고 게시판 수정
      * 
      * @param request
      * @param commandMap
      * @param response
      * @param model
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06Modi2.page")
     public void fdcrAd06Modi2(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response, MultipartHttpServletRequest mreq) throws Exception {

          commandMap.put("BORD_CD", "");
          commandMap.put("WORKS_ID", "");
          commandMap.put("DIVS_CD", "");
          commandMap.put("RECEIPT_NO", "");
          commandMap.put("TITE", "");
          commandMap.put("ANUC_ITEM_1", "");
          commandMap.put("ANUC_ITEM_2", "");
          commandMap.put("ANUC_ITEM_3", "");
          commandMap.put("ANUC_ITEM_4", "");
          commandMap.put("ANUC_ITEM_5", "");
          commandMap.put("ANUC_ITEM_6", "");
          commandMap.put("ANUC_ITEM_7", "");
          commandMap.put("RGST_IDNT", "");
          commandMap.put("OPEN_IDNT", "");
          fdcrAd06Service.fdcrAd06Modi2(commandMap);
          System.out.println("법정허락 이용승인공고 게시판 수정");
     }

     /**
      * 게시판 첨부 파일 다운로드
      * 
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/legal/fdcrAd06Down1.page")
     public void fdcrAd06Down1(HttpServletRequest request, Map<String, Object> commandMap, HttpServletResponse response) throws Exception {

          String fileRealNm = "";
          String fileNm = "";
          String savePath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path"));
          try {
               Map<String, Object> fileInfo = fdcrAd06Service.selectAttachFileInfo(commandMap);
               fileRealNm = EgovWebUtil.getString(fileInfo, "REAL_FILE_NAME");
               fileNm = EgovWebUtil.getString(fileInfo, "FILE_NAME");
               System.out.println("fileNm" + fileNm);
               System.out.println("fileRealNm" + fileRealNm);
               System.out.println("fileNm" + fileNm);
          } catch (Exception e) {
               System.out.println("ERROR");
               e.printStackTrace();
          }

          logger.debug(savePath);


          String downPath = savePath + fileRealNm;
          System.out.println(downPath);
          download(request, response, downPath, fileNm);
          /*
           * if( !"".equals( fileNm ) ){ download( request, response, downPath, fileNm ); }
           */
     }

}
