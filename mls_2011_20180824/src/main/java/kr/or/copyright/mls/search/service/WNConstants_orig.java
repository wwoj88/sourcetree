package kr.or.copyright.mls.search.service;

/**
 * 
 * <pre>
 * <p> Title: WNCollection </p>
 * <p> Description: 검색환경 설정 상수</p>
 * </pre>
 *
 * @author WISEnut
 * @created: 2014. 5. 27.
 * @modified:
 * @version
 *
 */
public class WNConstants_orig {
	public final static int CONNECTION_TIMEOUT = 20000;
	public final static String CHARSET = "UTF-8";
	public final static int REALTIME_COUNT=100;
	public final static int PAGE_SCALE = 10; //view page list count

	public final static int CONNECTION_KEEP = 0; //recevive mode
	public final static int CONNECTION_REUSE = 2;
	public final static int CONNECTION_CLOSE = 3;

	public final static int ASC = 0; //order
	public final static int DESC = 1; //order

	public final static int USE_KMA_OFFOFF = 0; //synonym, morpheme
	public final static int USE_KMA_ONON = 1;
	public final static int USE_KMA_ONOFF = 2;

	public final static int USE_RESULT_STRING = 0; //result data type	
	public final static int USE_RESULT_XML = 1;
	public final static int USE_RESULT_JSON = 2;
	public final static int USE_RESULT_DUPLICATE_STRING = 3; //uid result data type	
	public final static int USE_RESULT_DUPLICATE_XML = 4;
	public final static int USE_RESULT_DUPLICATE_JSON = 5;

	public final static int IS_CASE_ON = 1; //case on, off
	public final static int IS_CASE_OFF = 0;

	public final static int HI_SUM_OFFOFF = 0; //summarizing, highlighting
	public final static int HI_SUM_OFFON = 1;
	public final static int HI_SUM_ONOFF = 2;
	public final static int HI_SUM_ONON = 3;

	public final static int COMMON_OR_WHEN_NORESULT_OFF = 0;
	public final static int COMMON_OR_WHEN_NORESULT_ON = 1;

	public final static int INDEX_NAME = 0;
	public final static int COLLECTION_NAME = 1;
	public final static int PAGE_INFO = 2;
	public final static int ANALYZER = 3;
	public final static int SORT_FIELD = 4;
	public final static int RANKING_OPTION = 5;
	public final static int SEARCH_FIELD = 6;
	public final static int RESULT_FIELD = 7;
	public final static int DATE_RANGE = 8;
	public final static int RANK_RANGE = 9;
	public final static int EXQUERY_FIELD = 10;
	public final static int COLLECTION_QUERY =11;
	public final static int BOOST_QUERY =12;
	public final static int FILTER_OPERATION = 13;
	public final static int GROUP_BY = 14;
	public final static int GROUP_SORT_FIELD = 15;
	public final static int CATEGORY_BOOST = 16;
	public final static int CATEGORY_GROUPBY = 17;
	public final static int CATEGORY_QUERY = 18;
	public final static int PROPERTY_GROUP = 19;
	public final static int PREFIX_FIELD = 20;
	public final static int FAST_ACCESS = 21;
	public final static int MULTI_GROUP_BY = 22;
	public final static int AUTH_QUERY = 23;
	public final static int DEDUP_SORT_FIELD = 24;
	public final static int COLLECTION_KOR = 25;	
	public final static int HIGHLIGHT_FIELD = 26;	

	public final static int MERGE_COLLECTION_NAME = 0;
	public final static int MERGE_MAPPING_COLLECTION_NAME = 1;
	public final static int MERGE_PAGE_INFO = 2;
	public final static int MERGE_RESULT_FIELD = 3;
	public final static int MERGE_MAPPING_RESULT_FIELD = 4;
	public final static int MERGE_MULTI_GROUP_BY_FIELD = 5;
	public final static int MERGE_MAPPING_MULTI_GROUP_BY_FIELD = 6;
	public final static int MERGE_CATEGORY_GROUPBY_FIELD = 7;
	public final static int MERGE_MAPPING_CATEGORY_GROUPBY_FIELD = 8;

	//가상 통합 컬렉션을 사용하지 않을 경우 아래와 같이MERGE_COLLECTIONS에 정의한다.
	//static String[] MERGE_COLLECTIONS = new String[]{""};

	//가상 통합 컬렉션을 사용할 경우 아래와 같이MERGE_COLLECTIONS에 정의한다.
	//public String[] MERGE_COLLECTIONS = new String[]{"merge_sample_bbs"}; 
	/*
public class WNCollection {

	public String[][] MERGE_COLLECTION_INFO = null;

	WNCollection(){

		//가상 통합 컬렉션을 사용할 경우, mapping되는 collection들의 정보를 정의한다.
		MERGE_COLLECTION_INFO = new String[][]
		{
			{
				"merge_sample_bbs", // set merge collection name
				"sample_bbs/sample_edu", // set collection name, delimiter: /
				"0,3",  // set merge collection pageinfo (start,count)
				"DOCID,TITLE,WRITER,CONTENT",// set merge document field
				"DOCID,TITLE,WRITER,CONTENT/DOCID,TITLE,WRITER,CONTENT", // set document field, delimiter: /
				"", // set merge collection multi-group-by field
				"", // set merge collection multi-group-by field, delimiter: /
				"", // set merge collection category-group-by field
				""  // set collection multi-group-by field, delimiter: /
			}					
		};
	 */

//	public final static String SEARCH_IP="127.0.0.1";
//	public final static String MANAGER_IP="127.0.0.1";

	//개발
	/*public final static String SEARCH_IP="222.231.43.55";
	public final static int SEARCH_PORT=7000;
	public final static String MANAGER_IP="222.231.43.55";
	public final static int MANAGER_PORT=7800;//*/
	
	//운영
	public final static String SEARCH_IP="222.231.43.119";
	public final static int SEARCH_PORT=7001;
	public final static String MANAGER_IP="222.231.43.119";
	public final static int MANAGER_PORT=7801;//*/


	public static String[] COLLECTIONS = new String[]{"cf_art","cf_broadcast","cf_etc","cf_image","cf_literature","cf_movie","cf_music","cf_news","cf_photo","cf_reg","cf_scenario","reg_copyright","dg_album","dg_art","dg_art_licensor","dg_book","dg_broadcast","dg_broadcast_licensor","dg_character","dg_character_licensor","dg_content","dg_content_licensor","dg_literature","dg_literature_licensor","dg_movie","dg_movie_licensor","dg_music_licensor","dg_music1","dg_music2","dg_news","dg_news_licensor","dg_public","dg_public_licensor","dg_scenario","dg_scenario_licensor","dg_authcopy","dg_regcopy","dg_notice"};
	public static String[] COLLECTIONS_NAME = new String[]{"cf_art","cf_broadcast","cf_etc","cf_image","cf_literature","cf_movie","cf_music","cf_news","cf_photo","cf_reg","cf_scenario","reg_copyright","dg_album","dg_art","dg_art_licensor","dg_book","dg_broadcast","dg_broadcast_licensor","dg_character","dg_character_licensor","dg_content","dg_content_licensor","dg_literature","dg_literature_licensor","dg_movie","dg_movie_licensor","dg_music_licensor","dg_music1","dg_music2","dg_news","dg_news_licensor","dg_public","dg_public_licensor","dg_scenario","dg_scenario_licensor","dg_authcopy","dg_regcopy","dg_notice"};
	public static String[] MERGE_COLLECTIONS = new String[]{""};
	public class WNCollection{
	public String[][] MERGE_COLLECTION_INFO = null;
	public String[][] COLLECTION_INFO = null;
		WNCollection(){
			COLLECTION_INFO = new String[][]
			{
			{
			"cf_art", // set index name
			"cf_art", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,GENRE_CD,SOURCE_INFO,MAIN_MTRL,TXTR,WRITER,POSS_ORGN_NAME,POSS_DATE,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,IMAGE_URL,IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,GENRE_CD,MAKE_DATE,SOURCE_INFO,MAIN_MTRL,TXTR,WRITER,POSS_ORGN_NAME,POSS_DATE,RGST_ORGN_NAME,WORKS_ID,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_art" // collection display name
			}
         ,
			{
			"cf_broadcast", // set index name
			"cf_broadcast", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,BORD_GENRE_CD,BROD_ORD_SEQ,DIRECTOR,MAKE_CPY,WRITER",// set search field
			"DOCID,DATE,WORKS_TITLE,BORD_GENRE_CD,MAKE_YEAR,BROD_ORD_SEQ,DIRECTOR,MAKE_CPY,WRITER,CRT_YEAR,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_broadcast" // collection display name
			}
         ,
			{
			"cf_etc", // set index name
			"cf_etc", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,SIDE_GENRE_CD,PUBL_MEDI,COPT_NAME,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,SIDE_GENRE_CD,MAKE_DATE,PUBL_MEDI,PUBL_DATE,COPT_NAME,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_etc" // collection display name
			}
         ,
			{
			"cf_image", // set index name
			"cf_image", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,WRITER,KEYWORD,PRODUCER,IMAGE_DESC,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,IMAGE_URL,IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,WRITER,KEYWORD,PRODUCER,IMAGE_DESC,RGST_ORGN_NAME,WORKS_ID,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_image" // collection display name
			}
         ,
			{
			"cf_literature", // set index name
			"cf_literature", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,WORKS_SUB_TITLE,BOOK_TITLE,BOOK_PUBLISHER,COPT_NAME,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,WORKS_SUB_TITLE,BOOK_TITLE,BOOK_ISSU_YEAR,BOOK_PUBLISHER,COPT_NAME,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_literature" // collection display name
			}
         ,
			{
			"cf_movie", // set index name
			"cf_movie", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,PLAYER,DIRECT,WRITER,DIRECTOR,PRODUCER,DISTRIBUTOR,INVESTOR,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,PLAYER,DIRECT,WRITER,DIRECTOR,PRODUCER,DISTRIBUTOR,INVESTOR,CRT_YEAR,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_movie" // collection display name
			}
         ,
			{
			"cf_music", // set index name
			"cf_music", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,ALBUM_TITLE,LYRC,COMP,ARRA,TRAN,SING,PERF,PROD,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,ALBUM_TITLE,LYRC,COMP,ARRA,TRAN,SING,PERF,PROD,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_music" // collection display name
			}
         ,
			{
			"cf_news", // set index name
			"cf_news", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,PAPE_NO,PAPE_KIND,CONTRIBUTOR,REPOTER,PROVIDER,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,ARTICL_DATE,PAPE_NO,PAPE_KIND,CONTRIBUTOR,REPOTER,PROVIDER,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_news" // collection display name
			}
         ,
			{
			"cf_photo", // set index name
			"cf_photo", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,WRITER,KEYWORD,PRODUCER,IMAGE_DESC,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,IMAGE_URL,IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,WRITER,KEYWORD,PRODUCER,IMAGE_DESC,RGST_ORGN_NAME,WORKS_ID,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_photo" // collection display name
			}
         ,
			{
			"cf_reg", // set index name
			"cf_reg", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,REG_COPT_HODR_NAME,REG_REASON,AUTHOR_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,REG_COPT_HODR_NAME,REG_REASON,AUTHOR_NAME,REG_ID,REG_DATE,REG_PART1_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_reg" // collection display name
			}
         ,
			{
			"cf_scenario", // set index name
			"cf_scenario", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"BORD_DATE,WORKS_TITLE,BROD_ORD_SEQ,PLAYER,MAKE_CPY,WRITER,DIRECTOR,RGST_ORGN_NAME,SCRT_GENRE_CD,SCRP_SUBJ_CD,ORIG_WRITER,ALIAS",// set search field
			"DOCID,DATE,WORKS_TITLE,WORKS_ORIG_TITLE,BORD_DATE,BROD_ORD_SEQ,PLAYER,MAKE_CPY,WRITER,DIRECTOR,RGST_ORGN_NAME,SCRT_GENRE_CD,SCRP_SUBJ_CD,ORIG_WRITER,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_scenario" // collection display name
			}
         ,
			{
			"reg_copyright", // set index name
			"reg_copyright", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			//"reg_kind,reg_class,author_name,cont_title,cont_class,reg_id,pjt_id,cont_text,reg_part1,disposal_name,grant_name,trans_scope",// set search field
			"AUTHOR_NAME,AUTHOR_JUMIN,CONT_TITLE,REG_ID,DISPOSAL_NAME,GRANT_NAME",// set search field
			"DOCID,DATE,REG_RNK,REG_ID,REG_PART1,REG_DT,REG_CAUS_TXT,CONT_TITLE,AUTHOR_NAME,CONT_TXT,CONT_CLASS_CD,CONT_CLASS_NAME,ALIAS",// set document field
			//"DOCID,DATE,reg_order,reg_id,reg_class,reg_part1,reg_date,reg_reason,chg_type,chg_type_desc,cont_title,author_name,reg_kind,cont_text,cont_class,cont_class_name,cont_class_etc,pjt_id,trans_scope,disposal_name,grant_name,tablecode,isopenyn,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"reg_copyright" // collection display name
			}
         ,
         {
				"dg_album", // set index name
				"dg_album", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"ALBUM_TITLE,PRODUCER",// set search field
				"DOCID,DATE,ALBUM_TITLE,ALBUM_LABLE_NAME,ALBUM_PRODUCED_CRH,DISTRIBUTION_COMPANY,PRODUCER,EDITION,ALBUM_TYPE,ALBUM_MEDIA_TYPE,INOUT_TYPE,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"INOUT_TYPE,ALBUM_TYPE,ALBUM_MEDIA_TYPE,PRODUCER_P,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_album" // collection display name
				}
	         ,
				{
				"dg_art", // set index name
				"dg_art", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE",// set search field
				"DOCID,DATE,TITLE,SUBTITLE,INOUT_TYPE,STDCRHID,TRUST_YN,ART_WORK_ID,ART_TTL,ART_SUB_TTL,ART_DIV_INFO,MAKE_DT_INFO,CLTN_DT_INFO,CLTN_ORG_NM,SRC_INFO,SIZE_INFO,MAIN_MTRL,STRU_FTRE,RFRC_INFO,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ART_DIV_INFO,MAIN_MTRL,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_art" // collection display name
				}
	         ,
				{
				"dg_art_licensor", // set index name
				"dg_art_licensor", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"LICENSOR_NAME_KOR",// set search field
				"DOCID,DATE,LICENSOR_YMD,STAGE_NAME_SEQ,LICENSORID,CA_ID,LICENSORROLE,LICENSOR_NAME_KOR,LICENSOR_SEQ,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_art_licensor" // collection display name
				}
	         ,
				{
				"dg_book", // set index name
				"dg_book", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE,INOUT_TYPE,PUB_LANG,PUBLISHER",// set search field
				"DOCID,DATE,BOOK_NR_ID,WRITER,TRANSLATOR,TITLE,FIRST_EDITION_YEAR,PUBLISHER,INOUT_TYPE,PUB_LANG,MATERIAL_TYPE,PUBLISH_TYPE,RETRIEVE_TYPE,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"FIRST_EDITION_YEAR,INOUT_TYPE_P,PUB_LANG_P,MATERIAL_TYPE,PUBLISHER_P,PUBLISH_TYPE,RETRIEVE_TYPE,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_book" // collection display name
				}
	         ,
				{
				"dg_broadcast", // set index name
				"dg_broadcast", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE",// set search field
				"DOCID,DATE,STDCRHID,PROGRAM_CODE,PROGRAM_NAME,ORIGIN_TITLE,GNRE_CODE,GNRE_CODE_NAME,MAKE_YMD,RUNNING_TIME,MAKE_TYPE,MAKE_TYPE_KOR,MAKE_NAME,MAKE_TOTAL,MAKE_PD,PRODUCER,CONNER_NO,HOMEPAGE,BROAD_NUM,SUB_TITLE,TITLE,SUBTITLE,CRH_ID_OF_CA,WRITER,STARTDAY,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"GNRE_CODE,MAKE_TYPE,MAKE_NAME,MAKE_PD,PRODUCER,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_broadcast" // collection display name
				}
	         ,
				{
				"dg_broadcast_licensor", // set index name
				"dg_broadcast_licensor", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"LICENSOR_NAME_KOR",// set search field
				"DOCID,DATE,LICENSOR_YMD,STAGE_NAME_SEQ,LICENSORID,CA_ID,LICENSORROLE,LICENSOR_NAME_KOR,LICENSOR_SEQ,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_broadcast_licensor" // collection display name
				}
	         ,
				{
				"dg_character", // set index name
				"dg_character", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"CHAR_NM_KOR",// set search field
				"DOCID,DATE,CHAR_WORK_ID,CHAR_NM_KOR,CHAR_NM_ENG,CHAR_DESC,CHAR_RMK,WORK_CPY_NM_KOR,WORK_CPY_NM_ENG,WORK_NM_KOR,WORK_NM_ENG,WORK_GENRE_CD,WORKGENRENM,WORK_CRTE_YEAR,WORK_CPRT_REG_NUM,WORK_CPRT_REG_DT,WORK_CCL_TYPE,WORK_CPRT_INDC,LICENSORNAME,CRH_ID_OF_CA,STDCRHID,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"WORK_GENRE_CD,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_character" // collection display name
				}
	         ,
				{
				"dg_character_licensor", // set index name
				"dg_character_licensor", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"LICENSOR_NAME_KOR",// set search field
				"DOCID,DATE,LICENSOR_YMD,STAGE_NAME_SEQ,LICENSORID,CA_ID,LICENSORROLE,WORKCNT,LICENSOR_NAME_KOR,LICENSOR_NAME_ENG,NATINAME,LICENSOR_SEQ,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_character_licensor" // collection display name
				}
	         ,
				{
				"dg_content", // set index name
				"dg_content", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE",// set search field
				"DOCID,DATE,TITLE,WRITER,STDCRHID,INSERT_DATE,PROG_CODE,PROG_NAME,ORGI_NAME,MEDI_CODE_NM,MEDI_CODE,CHNL_CODE_NM,CHNL_CODE,BROAD_GENR_NM,BROAD_GENR,PROG_ID,PROG_ORDSEQ,SUBPR_NAME,SMPRG_NAME,BROAD_DATE,PART_NAME,MAIN_STORY,PROD_QULTY_NM,PROD_QULTY,PTABL_TITLE,PROG_GRAD,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"PROG_NAME,MEDI_CODE,CHNL_CODE,SUBPR_NAME,PROD_QULTY,PTABL_TITLE,PROG_GRAD,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_content" // collection display name
				}
	         ,
				{
				"dg_content_licensor", // set index name
				"dg_content_licensor", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"LICENSOR_NAME_KOR",// set search field
				"DOCID,DATE,LICENSOR_YMD,STAGE_NAME_SEQ,LICENSORID,CA_ID,LICENSORROLE,LICENSOR_NAME_KOR,LICENSOR_SEQ,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_content_licensor" // collection display name
				}
	         ,
				{
				"dg_literature", // set index name
				"dg_literature", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE,BOOKTITLE",// set search field
				"DOCID,DATE,BOOK_NR_ID,TITLE,WRITER,TRANSLATOR,BOOKTITLE,FIRST_EDITION_YEAR,PUBLISHER,INOUT_TYPE,PUB_LANG,MATERIAL_TYPE,PUBLISH_TYPE,RETRIEVE_TYPE,STDCRHID,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"FIRST_EDITION_YEAR,INOUT_TYPE,PUB_LANG,MATERIAL_TYPE,PUBLISHER,PUBLISH_TYPE,RETRIEVE_TYPE,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_literature" // collection display name
				}
	         ,
				{
				"dg_literature_licensor", // set index name
				"dg_literature_licensor", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"LICENSOR_NAME_KOR",// set search field
				"DOCID,DATE,LICENSOR_YMD,STAGE_NAME_SEQ,LICENOSRID,CA_ID,LICENSORROLE,LICENSOR_NAME_KOR,LICENSOR_SEQ,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_literature_licensor" // collection display name
				}
	         ,
				{
				"dg_movie", // set index name
				"dg_movie", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE,GENRE_NAME",// set search field
				"DOCID,DATE,TITLE,STDCRHID,INSERT_DATE,GENRE,GENRE_NAME,MOVIE_TYPE,MOVIE_TYPE_NAME,VIEW_GRADE,VIEW_GRADE_NAME,RUN_TIME,PRODUCE_YEAR,RELEASE_DATE,LEADING_ACTOR,DIRECTOR,PRODUCER,INVESTOR,FORMAT_CODE_NAME,TITLE_ENG,TITLE_ORG,TITLE_ROMA,MEDIA_ID,MEDIA_CODE,PRODUCE_DATE,DISTRIBUTOR,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"GENRE,MOVIE_TYPE,VIEW_GRADE,PRODUCE_YEAR,PRODUCER,LEADING_ACTOR,DIRECTOR,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_movie" // collection display name
				}
	         ,
				{
				"dg_movie_licensor", // set index name
				"dg_movie_licensor", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"LICENSOR_NAME_KOR",// set search field
				"DOCID,DATE,LICENSOR_YMD,STAGE_NAME_SEQ,LICENOSRID,CA_ID,LICENSORROLE,LICENSOR_NAME_KOR,LICENSOR_SEQ,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_movie_licensor" // collection display name
				}
	         ,
				{
				"dg_music_licensor", // set index name
				"dg_music_licensor", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"LICENSOR_NAME_KOR",// set search field
				"DOCID,DATE,LICENSOR_YMD,STAGE_NAME_SEQ,LICENOSRID,CA_ID,LICENSORROLE,LICENSOR_NAME_KOR,LICENSOR_SEQ,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_music_licensor" // collection display name
				}
	         ,
				{
				"dg_music1", // set index name
				"dg_music1", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE,LYRICIST,COMPOSER,SINGER",// set search field
				"DOCID,DATE,ALBUM_TITLE,ALBUM_ID,NR_ID,TITLE,SUBTITLE,MUTITLE,MUSUBTITLE,INOUT_TYPE,SINGER,PLAYER,CONDUCTOR,FEATURING,LYRICIST,COMPOSER,ARRANGER,TRANSLATOR,STDCRHID,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS,INOUT_TYPE,LYRICIST_P,COMPOSER_P,SINGER_P", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_music1" // collection display name
				}
	         ,
				{
				"dg_music2", // set index name
				"dg_music2", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE,SUBTITLE,LYRICIST,COMPOSER",// set search field
				"DOCID,DATE,TITLE,SUBTITLE,INOUT_TYPE,LYRICIST,COMPOSER,ARRANGER,TRANSLATOR,STDCRHID,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS,INOUT_TYPE,LYRICIST_P,COMPOSER_P", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_music2" // collection display name
				}
	         ,
				{
				"dg_news", // set index name
				"dg_news", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE",// set search field
				"DOCID,DATE,TITLE,SUBTITLE,GENRE,PAPEREDITION,REPORTERNM,PROVIDERVCD,LICENSORID,ARTICLPUBCSDATE,ARTICLDT,STDCRHID,CAID,CANAME,REVISIONID,PAPERNO,PAPERKIND,PUBSERIALYNM,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"PROVIDERVCD,REPORTERNM,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_news" // collection display name
				}
	         ,
				{
				"dg_news_licensor", // set index name
				"dg_news_licensor", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"LICENSOR_NAME_KOR",// set search field
				"DOCID,DATE,LICENSOR_YMD,STAGE_NAME_SEQ,LICENOSRID,CA_ID,LICENSORROLE,LICENSOR_NAME_KOR,LICENSOR_SEQ,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_news_licensor" // collection display name
				}
	         ,
				{
				"dg_public", // set index name
				"dg_public", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE",// set search field
				"DOCID,DATE,TYPE_DIV_L_CD,TYPEDIVLNM,TYPE_DIV_M_CD,TYPEDIVMNM,MEAN_DIV_L_CD,MEANDIVLNM,MEAN_DIV_M_CD,MEANDIVMNM,ANNC_NATN,UCI,PUBLCRID,TITLE,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS,TYPE_DIV_L_CD,ANNC_NATN", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_public" // collection display name
				}
	         ,
				{
				"dg_public_licensor", // set index name
				"dg_public_licensor", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"LICENSOR_NAME_KOR",// set search field
				"DOCID,DATE,LICENSOR_YMD,STAGE_NAME_SEQ,LICENOSRID,CA_ID,LICENSORROLE,LICENSOR_NAME_KOR,LICENSOR_SEQ,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_public_licensor" // collection display name
				}
	         ,
				{
				"dg_scenario", // set index name
				"dg_scenario", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"TITLE",// set search field
				"DOCID,DATE,TITLE,WRITER,STDCRHID,INSERT_DATE,GENRE_KINDNM,GENRE_KIND,SUBJ_KIND,PROD_TITLE,PROD_SUBTITLE,DIRECT,ORIG_WORK,ORIG_WRITER,PLAYERS,SYNNOPSIS,BROAD_ORD,BROAD_DATE,BROAD_MEDINM,BROAD_MEDI,BROAD_STATNM,BROAD_STAT,MAKER,MC,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"GENRE_KIND,SUBJ_KIND,ORIG_WORK,ORIG_WRITER,PLAYERS,BROAD_MEDINM,BROAD_STATNM,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_scenario" // collection display name
				}
	         ,
				{
				"dg_scenario_licensor", // set index name
				"dg_scenario_licensor", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"LICENSOR_NAME_KOR",// set search field
				"DOCID,DATE,LICENSOR_YMD,STAGE_NAME_SEQ,LICENOSRID,CA_ID,LICENSORROLE,LICENSOR_NAME_KOR,LICENSOR_SEQ,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_scenario_licensor" // collection display name
				}
	         ,
				{
				"dg_authcopy", // set index name
				"dg_authcopy", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"CRAS_TITLE,CRAS_WRITER,CRAS_CLASS_CD",// set search field
				"DOCID,DATE,CRAS_CERT_ID,CRAS_TITLE,CRAS_WRITER,CRAS_VALID_DATE,CRAS_CLASS_CD,CRAS_CLASS_NAME,CRAS_CONT,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"CRAS_CLASS_CD_P,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_authcopy" // collection display name
				}
	         ,
				{
				"dg_regcopy", // set index name
				"dg_regcopy", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"CONT_TITLE,AUTHOR_NAME,CONT_CLASS_CD",// set search field
				"DOCID,DATE,SYS_ID,REG_ID,CONT_TITLE,AUTHOR_NAME,DISPOSAL_NAME,CONT_CLASS_CD,CONT_CLASS_NAME,REG_PART1_NAME,REG_PART2_NAME,REG_REASON,REG_KIND,REG_DATE,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"CONT_CLASS_CD_P,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_regcopy" // collection display name
				}
	         ,
				{
				"dg_notice", // set index name
				"dg_notice", // set collection name
				"0,3",  // set pageinfo (start,count)
				"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
				"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
				"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
				"ORGIN_SJ,AUTHOR_LIST_NM,WRT_TY",// set search field
				"DOCID,DATE,WRT_SN,ORGIN_SJ,WRT_TY,WRT_TY_NM,WRT_FILE_TY,WRT_FILE_TY_NM,CRT_DE,AUTHOR_LIST_NM,TAG_NM_LIST,LICENSE_CD,LICENSE_CD_NM,LICENSE_IMG_URL,REG_DT,LINK_URL,THUMB_URL,ALIAS",// set document field
				"", // set date range
				"", // set rank range
				"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
				"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
				"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
				"", // set filter operation (<fieldname:operator:value>)
				"", // set groupby field(field, count)
				"", // set sort field group(field/order,field/order,...)
				"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
				"", // set categoryGroupBy (fieldname:value)
				"", // set categoryQuery (fieldname:value)
				"", // set property group (fieldname,min,max, groupcount)
				"WRT_TY_P,ALIAS", // use check prefix query filed
				"", // set use check fast access field
				"", // set multigroupby field
				"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
				"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
				"dg_notice" // collection display name
				}
         
			};
		}
	}
}