package kr.or.copyright.mls.adminIcn.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class AdminIcnSqlMapDao extends SqlMapClientDaoSupport implements AdminIcnDao{
	
	// 음악 ICN발급목록
	public List selectMuscList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminIcn.selectMuscList", map);
	}
	
	// 음악 상세
	public List selectMuscDeatilList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminIcn.selectMuscDeatilList", map);
	}
	
	// 음악 ICN 정보 update
	public void updateMuscIcnStat(Map map) {
		getSqlMapClientTemplate().update("AdminIcn.updateMuscIcnStat", map);
	}
	
	// 도서 ICN발급목록
	public List selectBooksList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminIcn.selectBooksList", map);
	}
	
	// 도서 ICN 정보 update
	public void updateBooksIcnStat(Map map) {
		getSqlMapClientTemplate().update("AdminIcn.updateBooksIcnStat", map);
	}
	
	// 도서 저작권자 통합저작권자ID update (저자)
	public void updateBooksLicensorId_201(Map map) {
		getSqlMapClientTemplate().update("AdminIcn.updateBooksLicensorId_201", map);
	}
	
	// 도서 저작권자 통합저작권자ID update (역자)
	public void updateBooksLicensorId_202(Map map) {
		getSqlMapClientTemplate().update("AdminIcn.updateBooksLicensorId_202", map);
	}	
	
	// 개인저작물 목록
	public List selectPrivWorksList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminIcn.selectPrivWorksList", map);
	}
	
	// 개인저작물 상세정보
	public List selectPrivWorksDetailList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminIcn.selectPrivWorksDetailList", map);
	}
	
	// 개인저작물 첨부파일
	public List selectPrivWorksFileList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminIcn.selectPrivWorksFileList", map);
	}
	
	// get 개인저작물 위원회 ID
	public List getCommId(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminIcn.getCommId", map);
	}
	
	// 개인저작물 위원회ID update
	public void updateCommId(Map map) {
		getSqlMapClientTemplate().update("AdminIcn.updateCommId",map);
	}
	
	// 개인저작물 ICN 발급정보 update
	public void privWorksUpdate(Map map) {
		getSqlMapClientTemplate().update("AdminIcn.privWorksUpdate",map);
	}
}
