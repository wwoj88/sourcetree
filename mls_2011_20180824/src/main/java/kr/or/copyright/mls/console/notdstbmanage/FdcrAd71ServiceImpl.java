package kr.or.copyright.mls.console.notdstbmanage;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.notdstbmanage.inter.FdcrAd71Dao;
import kr.or.copyright.mls.console.notdstbmanage.inter.FdcrAd71Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd71Service" )
public class FdcrAd71ServiceImpl extends CommandService implements FdcrAd71Service{

	@Resource( name = "fdcrAd71Dao" )
	private FdcrAd71Dao fdcrAd71Dao;

	/**
	 * 방송음악 미분배보상금 관리 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd71List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd71Dao.alltInmtDetl( commandMap );

		commandMap.put( "ds_List", list );

	}

	/**
	 * 방송음악 등록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd71Insert1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			// UPDATE처리
			String INMT_DIVS = commandMap.get( "INMT_DIVS" ).toString();
			String INMT_YEAR = (String) commandMap.get( "INMT_YEAR" );
			String[] QUARTERS = (String[]) commandMap.get( "QUARTER" );
			String[] ALLT_INMTS = (String[]) commandMap.get( "ALLT_INMT" );
			
			commandMap.put( "ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode() );
			commandMap.put( "INMT_YEAR", INMT_YEAR );

			List cnt = fdcrAd71Dao.alltInmtCount( commandMap );

			for( int i = 0; i < QUARTERS.length; i++ ){

				Map<String, Object> param = new HashMap<String, Object>();

				param.put( "INMT_DIVS", INMT_DIVS );
				param.put( "INMT_YEAR", INMT_YEAR );
				param.put( "QUARTER", QUARTERS[i] );
				param.put( "ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode() );
				param.put( "ALLT_INMT", ALLT_INMTS[i] );

				if( cnt.size() == 0 ){
					fdcrAd71Dao.alltInmtSave( param );
				}else{
					fdcrAd71Dao.alltInmtUpdate( param );
				}
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
