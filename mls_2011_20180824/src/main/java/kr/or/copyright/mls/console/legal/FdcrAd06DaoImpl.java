package kr.or.copyright.mls.console.legal;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAd06Dao" )
public class FdcrAd06DaoImpl extends EgovComAbstractDAO implements FdcrAd06Dao{

	/**
	 * 법정허락 신청목록 목록
	 * 
	 * @param commandMap
	 * @return
	 */
	public ArrayList<Map<String, Object>> adminStatPrpsList( Map<String, Object> commandMap ){
		return (ArrayList<Map<String, Object>>) getSqlMapClientTemplate().queryForList( "AdminStatMgnt.adminStatPrpsList", commandMap );
	}
	
	// 법정허락 신청목록 COUNT
	public List adminStatPrpsListCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatMgnt.adminStatPrpsListCount", map );
	}

	// 법정허락 이용승인 신청서 조회
	/*
	public Map<String, Object> adminStatApplicationSelect( Map map ){
		return (Map<String, Object>) getSqlMapClientTemplate().queryForList( "AdminStatMgnt.adminStatApplicationSelect", map );
	}
	*/
	
	/**
	 * 법정허락 이용승인 신청서 조회
	 * 
	 * @return
	 */
	@SuppressWarnings( "unchecked" )
	public Map<String, Object> adminStatApplicationSelect( Map<String, Object> commandMap ){
		return (Map<String, Object>) selectByPk( "AdminStatMgnt.adminStatApplicationSelect", commandMap );
	}

	/**
	 * 법정허락신청 첨부서류 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings( "unchecked" )
	public ArrayList<Map<String, Object>> adminStatAttcFileSelect( Map<String, Object> commandMap ) throws SQLException{
		return (ArrayList<Map<String, Object>>) list( "AdminStatMgnt.adminStatAttcFileSelect", commandMap );
	}
	
	/**
	 * 법정허락신청 첨부서류 정보
	 * 
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings( "unchecked" )
	public Map<String, Object> adminStatAttcFileInfo( Map<String, Object> commandMap ) throws SQLException{
		return (Map<String, Object>) selectByPk( "AdminStatMgnt.adminStatAttcFileSelect", commandMap );
	}

	/**
	 * 법정허락 이용승인신청 명세서 목록
	 * 
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> adminStatApplyWorksSelect( Map<String, Object> commandMap ){
		return (ArrayList<Map<String, Object>>) getSqlMapClientTemplate().queryForList( "AdminStatMgnt.adminStatApplyWorksSelect", commandMap );
	}
	
	/**
	 * 법정허락 이용승인신청 명세서 상세
	 * 
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> adminStatApplyWorksSelectInfo( Map<String, Object> commandMap ){
		return (ArrayList<Map<String, Object>>) getSqlMapClientTemplate().queryForList( "AdminStatMgnt.adminStatApplyWorksSelectInfo", commandMap );
	}
	
	// 법정허락 이용승인신청 진행상태 내역
	public List adminStatApplicationShisSelect( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatMgnt.adminStatApplicationShisSelect", map );
	}

	// 법정허락 이용승인신청 심의결과상태 내역
	public List adminStatApplyWorksShisSelect( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatMgnt.adminStatApplyWorksShisSelect", map );
	}

	// 접수번호 시퀀스 조회
	public String adminStatApplicationReceiptSeqSelect(){
		return (String) getSqlMapClientTemplate().queryForObject( "AdminStatMgnt.adminStatApplicationReceiptSeqSelect" );
	}

	// 이용승인 신청서 수정
	public void adminStatApplicationUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatMgnt.adminStatApplicationUpdate", map );
	}

	// 이용승인 신청서 상태변경내역 등록
	public void adminStatApplicationShisInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatApplicationShisInsert", map );
	}

	// 첨부파일 등록
	public void adminFileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminFileInsert", map );
	}

	// 첨부파일 수정
	public void adminFileDel( Map map ){
		getSqlMapClientTemplate().delete( "AdminStatMgnt.adminFileDel", map );
	}

	// 법정허락신청 첨부서류 등록 20120831 정병호
	public void adminStatAttcFileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatAttcFileInsert", map );
	}

	// 법정허락신청 첨부서류 삭제 20141106 이병원
	public void adminStatAttcFileDel( Map map ){
		getSqlMapClientTemplate().delete( "AdminStatMgnt.adminStatAttcFileDel", map );
	}

	// 법정허락신청 이용승인신청명세서 등록 20120831 정병호
	public void adminStatWorksInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatWorksInsert", map );
	}

	// 이용승인신청 명세서 수정
	public void adminStatApplyWorksUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatMgnt.adminStatApplyWorksUpdate", map );
	}

	// 이용승인신청 명세서 상태변경내역 등록
	public void adminStatApplyWorksShisInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatApplyWorksShisInsert", map );
	}

	// adminStatPrpsRegi 법정허락 관리자 등록 20120829 정병호
	public void adminStatPrpsRegi( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatPrpsRegi", map );
	}

	// adminStatPrpsUpte 이용승인신청서 수정 2014 11 06 이병원
	public void adminStatPrpsUpte( Map map ){
		getSqlMapClientTemplate().update( "AdminStatMgnt.adminStatPrpsUpte", map );
	}

	// adminStatPrpsShisRegiUpte 이용승인신청서 상태변경내역 수정 2014 11 21 이병원
	public void adminStatPrpsShisRegi2( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatPrpsShisRegi2", map );
	}

	// adminStatWorksDel 명세서 수정2014 11 06 이병원
	public void adminStatWorksDel( Map map ){
		getSqlMapClientTemplate().delete( "AdminStatMgnt.adminStatWorksDel", map );
	}

	// WORKS_SEQN 조회
	public int adminStatWorksSeqn( Map map ){
		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminStatMgnt.adminStatWorksSeqn", map );
	}

	// adminStatPrpsRegiSelect 법정허락 관리 데이터 중복 확인 20141020 이병원
	public int adminStatPrpsRegiSelect( Map map ){
		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminStatMgnt.adminStatPrpsRegiSelect", map );
	}

	// adminStatPrpsRegiAll 법정허락 관리자 일괄등록 20141020 이병원
	public void adminStatPrpsRegiAll( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatPrpsRegiAll", map );
	}

	// adminStatPrpsRegi 법정허락 관리자 이용승인신청서 상태변경내역 등록 20120829 정병호
	public void adminStatPrpsShisRegi( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatPrpsShisRegi", map );
	}

	// 관리자 이용승인 신청 상태변경내역 등록 20141022 이병원 등록
	// adminStatPrpsShisRegiAll
	public void adminStatPrpsShisRegiAll( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatPrpsShisRegiAll", map );
	}

	// adminStatWorksInsertAll
	public void adminStatWorksInsertAll( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatWorksInsertAll", map );
	}

	// 법정허락 결제집결표 목록
	public List cPayList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatMgnt.cPayList", map );
	}

	// 이용승인 신청서 수정 진행상태
	public void updateStatApplicationStat( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.updateStatApplicationStat", map );
	}

	// 이용승인 신청서SHIS 등록
	public void insertStatApplicationShis( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.insertStatApplicationShis", map );
	}

	public void adminStatPrpsRegiAllNew( Map<String, Object> param ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatPrpsRegiAllNew", param );
	}

	public void adminStatPrpsShisRegiAllNew( Map<String, Object> param ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatPrpsShisRegiAllNew", param );
	}

	public void adminStatWorksInsertAllNew( Map<String, Object> param ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatWorksInsertAllNew", param );
	}
	
	//2017년 신규 법정허락 이용승인신청서 등록
	public void adminStatPrpsRegiNew(Map map){
	    getSqlMapClientTemplate().insert("AdminStatMgnt.adminStatPrpsRegiNew",map);
	}
	
	//2017년 신규 법정허락 상태변경내역 등록
	public void adminStatPrpsShisRegiNew(Map map){
	    getSqlMapClientTemplate().insert("AdminStatMgnt.adminStatPrpsShisRegiNew",map);
	}

	public void adminStatWorksInsertNew( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatMgnt.adminStatWorksInsertNew", map );
	}

}
