package kr.or.copyright.mls.console.effort;

import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Service;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 상당한노력(신청 및 조회공고) > 상당한노력 수행
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd03Controller extends DefaultController{

	@Resource( name = "fdcrAd03Service" )
	private FdcrAd03Service fdcrAd03Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd03Controller.class );

	/**
	 * 상당한노력 진행현황 조회 <br>
	 * 상당한노력(신청 및 조회공고)>상당한노력 수행 : trstOrgnCode=199<br>
	 * 관리저작물 접수 및 처리 >상당한노력 >진행현황 : trstOrgnCode=200<br>
	 * 상당한 노력 관리>상당한노력 진행현황 : trstOrgnCode=<br>
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03List1.page" )
	public String fdcrAd03List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03List1 Start" );
		// 파라미터 셋팅
		commandMap.put( "SCHEDULE_SRCH", "" );
		commandMap.put( "P_SCH_YN", "N" );
		commandMap.put( "P_EXCP_STAT_CD", "" );
//		String[] result = request.getParameterValues( "result" );
//		commandMap.put( "result", result );

		fdcrAd03Service.fdcrAd03List1( commandMap );
		model.addAttribute( "ds_works_list", commandMap.get( "ds_works_list" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_set_list", commandMap.get( "ds_set_list" ) );
		logger.debug( "fdcrAd03List1 End" );
		return "effort/fdcrAd03List1.tiles";
	}

	/**
	 * 대상저작물 수집-프로시져 호출
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03List2.page" )
	public String fdcrAd03List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03List2 Start" );
		// 파라미터 셋팅
		commandMap.put( "P_SCH_YN", "N" );
		commandMap.put( "SCHEDULE_SRCH", "" );
//		String[] result = request.getParameterValues( "result" );
//		commandMap.put( "result", result );

		fdcrAd03Service.fdcrAd03List2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "fdcrAd03List2 End" );
		return "effort/fdcrAd03List2";
	}

	/**
	 * 대상저작물 수집 - 법정허락 대상물
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03List3.page" )
	public String fdcrAd03List3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03List3 Start" );
		// 파라미터 셋팅
		commandMap.put( "SCHEDULE_SRCH", "" );
		commandMap.put( "P_SCH_YN", "N" );

		fdcrAd03Service.fdcrAd03List3( commandMap );
		model.addAttribute( "ds_works_list", commandMap.get( "ds_works_list" ) );
		logger.debug( "fdcrAd03List3 End" );
		return "effort/fdcrAd03List3";
	}

	/**
	 * 대상 저작물 목록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03List4.page" )
	public String fdcrAd03List4( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03List4 Start" );
		// 파라미터 셋팅
		String STAT_CD = EgovWebUtil.getString( commandMap, "STAT_CD" );
		String returnUrl = "";
		if("4".equals(STAT_CD)){
			fdcrAd03Service.fdcrAd03List5( commandMap );
			returnUrl = "/effort/fdcrAd03List4Sub1.popup";
		}else{
			fdcrAd03Service.fdcrAd03List4( commandMap );
			returnUrl = "/effort/fdcrAd03List4.popup";
		}
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "commandMap", commandMap );
		logger.debug( "fdcrAd03List4 End" );
		return returnUrl;
	}

	/**
	 * 대상 저작물 목록 팝업 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03Down1.page" )
	public String fdcrAd03Down1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03Down1 Start" );
		// 파라미터 셋팅
		commandMap.put( "SCHEDULE_SRCH", "" );
		commandMap.put( "P_SCH_YN", "N" );
		commandMap.put( "P_EXCP_STAT_CD", "" );
//		String[] result = request.getParameterValues( "result" );
//		commandMap.put( "result", result );

		fdcrAd03Service.fdcrAd03List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ExcelDown", "Y" );
		
		logger.debug( "fdcrAd03Down1 End" );
		return "effort/fdcrAd03Down1";
	}

	/**
	 * 상당한노력 현황 조회 - 결과보기팝업
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03List5.page" )
	public String fdcrAd03List5( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03List5 Start" );
		
		String returnUrl = "";
		String SEARCH_CD = EgovWebUtil.getString( commandMap, "SEARCH_CD" );
		if("0".equals(SEARCH_CD)){
			returnUrl = "/effort/fdcrAd03List5.popup";
		}else if("1".equals(SEARCH_CD)){
			returnUrl = "/effort/fdcrAd03List5Sub1.popup";
		}else if("2".equals(SEARCH_CD)){
			returnUrl = "/effort/fdcrAd03List5Sub1.popup";
		}
		
		String WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
		if( !"".equals( WORKS_TITLE ) ){
			WORKS_TITLE = URLDecoder.decode( WORKS_TITLE, "UTF-8" );
			commandMap.put( "WORKS_TITLE", WORKS_TITLE );
		}
		fdcrAd03Service.fdcrAd03List5( commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		logger.debug( "fdcrAd03List5 End" );
		return returnUrl;
	}
	
	
	@RequestMapping( value = "/console/effort/fdcrAd03List5Ajax.page" )
	public void fdcrAd03List5Ajax( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03List5 Start" );
		
		System.out.println( "commandMap : "+commandMap );
		
		String returnUrl = "";
		String SEARCH_CD = EgovWebUtil.getString( commandMap, "SEARCH_CD" );
		if("0".equals(SEARCH_CD)){
			returnUrl = "/effort/fdcrAd03List5.popup";
		}else if("1".equals(SEARCH_CD)){
			returnUrl = "/effort/fdcrAd03List5Sub1.popup";
		}else if("2".equals(SEARCH_CD)){
			returnUrl = "/effort/fdcrAd03List5Sub1.popup";
		}
		
		String WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
		if( !"".equals( WORKS_TITLE ) ){
			WORKS_TITLE = URLDecoder.decode( WORKS_TITLE, "UTF-8" );
			System.out.println("WORKS_TITLE : " + WORKS_TITLE);
			commandMap.put( "WORKS_TITLE", WORKS_TITLE );
		}
		fdcrAd03Service.fdcrAd03List5( commandMap );
		System.out.println( commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		
		response.setCharacterEncoding( "utf-8" );
		
		PrintWriter out = response.getWriter();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put( "commandMap", commandMap );
		out.print( jsonObject );
		logger.debug( "fdcrAd03List5 End" );
		
	}
	
	@RequestMapping( value = "/console/effort/fdcrAd03List5_loading.page" )
	public String fdcrAd03List5_loading( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03List5 End" );
		String returnUrl = "/effort/fdcrAd03List5_loading.popup";
		return returnUrl;
	}
	
	@RequestMapping( value = "/console/effort/fdcrAd03List5Popup.page" )
	public String fdcrAd03List5Popup( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03List5 End" );
		String returnUrl = "/effort/fdcrAd03List5_ajax.popup";
		
		String ORD = EgovWebUtil.getString( commandMap, "ORD" );
		String YYYYMM = EgovWebUtil.getString( commandMap, "YYYYMM" );
		String WORKS_DIVS_CD = EgovWebUtil.getString( commandMap, "WORKS_DIVS_CD" );
		String SEARCH_CD = EgovWebUtil.getString( commandMap, "SEARCH_CD" );
		
		System.out.println( "ORD : " +ORD );
		System.out.println( "YYYYMM : " +YYYYMM );
		System.out.println( "WORKS_DIVS_CD : " +WORKS_DIVS_CD );
		System.out.println( "SEARCH_CD : " +SEARCH_CD );
		
		model.addAttribute( "ORD", ORD );
		model.addAttribute( "YYYYMM", YYYYMM );
		model.addAttribute( "WORKS_DIVS_CD", WORKS_DIVS_CD );
		model.addAttribute( "SEARCH_CD", SEARCH_CD );
		
		return returnUrl;
	}
	
	
	/**
	 * 법정허락 현황 조회 - 결과보기팝업
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03List7.page" )
	public String fdcrAd03List7( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03List7 Start" );
		
		fdcrAd03Service.fdcrAd03List7( commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		logger.debug( "fdcrAd03List5 End" );
		return "/effort/fdcrAd03List7.popup";
	}
	
	/**
	 * 법정허락 현황 조회 - 결과보기팝업
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03Down3.page" )
	public String fdcrAd03Down3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03Down3 Start" );
		
		fdcrAd03Service.fdcrAd03Down3( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ExcelDown", "Y" );
		logger.debug( "fdcrAd03Down3 End" );
		return "/effort/fdcrAd03Down3";
	}


	/**
	 * 상당한노력 - 실행(자동화관리 1.저장), 상당한 노력 진행(프로세스 분기 20121108)
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03Exe1.page" )
	public void fdcrAd03Exe1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03Exe1 Start" );
		// 파라미터 셋팅
		boolean isSuccess = fdcrAd03Service.fdcrAd03Exe1( commandMap );
		returnAjaxString( response, isSuccess );
		logger.debug( "fdcrAd03Exe1 End" );
	}

	/**
	 * 상당한 노력 예외처리완료
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03Exe2.page" )
	public void fdcrAd03Exe2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03Exe2 Start" );
		// 파라미터 셋팅
		boolean isSuccess = fdcrAd03Service.fdcrAd03Exe2( commandMap );
		returnAjaxString( response, isSuccess );
		logger.debug( "fdcrAd03Exe2 End" );
	}

	/**
	 * 법정허락 대상 전환 실행
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03Exe3.page" )
	public void fdcrAd03Exe3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03Exe3 Start" );
		// 파라미터 셋팅
		boolean isSuccess = fdcrAd03Service.fdcrAd03Exe3( commandMap );
		returnAjaxString( response, isSuccess );
		logger.debug( "fdcrAd03Exe3 End" );
	}

	/**
	 * 상당한노력 자동화 수행(스케쥴링 (분기. 20121109) + 자동화설정추가(20131018))
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03Schedule1.page" )
	public void fdcrAd03Schedule1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03Schedule1 Start" );
		// 파라미터 셋팅
		commandMap.put( "STAT_ORD", "" );
		commandMap.put( "WORKS_DIVS_CD", "" );
		commandMap.put( "YYYYMM", "" );
		commandMap.put( "P_SCH_YN", "" );
		commandMap.put( "TARG_WORKS_CONT", "" );

		boolean isSuccess = fdcrAd03Service.fdcrAd03Schedule1( commandMap );
		returnAjaxString( response, isSuccess );
		logger.debug( "fdcrAd03Schedule1 End" );
	}

	/**
	 * 법정허락 전환 자동화
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03Schedule2.page" )
	public void fdcrAd03Schedule2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03Schedule2 Start" );
		// 파라미터 셋팅
		commandMap.put( "STAT_ORD", "" );
		commandMap.put( "WORKS_DIVS_CD", "" );
		commandMap.put( "YYYYMM", "" );
		commandMap.put( "P_SCH_YN", "" );
		commandMap.put( "TARG_WORKS_CONT", "" );

		boolean isSuccess = fdcrAd03Service.fdcrAd03Schedule2( commandMap );
		returnAjaxString( response, isSuccess );
		logger.debug( "fdcrAd03Schedule2 End" );
	}

	/**
	 * 상당한노력 자동화 설정 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03List6.page" )
	public String fdcrAd03List6( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03List5 Start" );
		// 파라미터 셋팅
		commandMap.put( "SET_CD", "" );

		fdcrAd03Service.fdcrAd03List6( commandMap );
		model.addAttribute( "ds_list_stat", commandMap.get( "ds_list_stat" ) );
		logger.debug( "fdcrAd03List6 End" );
		return "test";
	}

	/**
	 * 상당한노력_진행현황 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03Down2.page" )
	public String fdcrAd03Down2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03Down2 Start" );
		String returnUrl = "";
		String SEARCH_CD = EgovWebUtil.getString( commandMap, "SEARCH_CD" );
		if("0".equals(SEARCH_CD)){
			returnUrl = "/effort/fdcrAd03Down2";
		}else if("1".equals(SEARCH_CD)){
			returnUrl = "/effort/fdcrAd03Down2Sub1";
		}else if("2".equals(SEARCH_CD)){
			returnUrl = "/effort/fdcrAd03Down2Sub1";
		}
		
		String WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
		if( !"".equals( WORKS_TITLE ) ){
			WORKS_TITLE = URLDecoder.decode( WORKS_TITLE, "UTF-8" );
			commandMap.put( "WORKS_TITLE", WORKS_TITLE );
		}
		fdcrAd03Service.fdcrAd03Down2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ExcelDown", "Y" );
		logger.debug( "fdcrAd03Down2 End" );
		return returnUrl;
	}

	/**
	 * 대상저작물전환, 예외처리 취소
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd03Update1.page" )
	public void fdcrAd03Update1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd03Schedule2 Start" );
		// 파라미터 셋팅
		commandMap.put( "USER_ID", ConsoleLoginUser.getUserId() );
		boolean isSuccess = fdcrAd03Service.fdcrAd03Update1( commandMap );
		returnAjaxString( response, isSuccess );
		logger.debug( "fdcrAd03Update1 End" );
	}

}
