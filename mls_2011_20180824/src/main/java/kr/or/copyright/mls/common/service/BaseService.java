package kr.or.copyright.mls.common.service;

import java.io.Reader;
import java.lang.reflect.Method;
import java.sql.Clob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.utils.MiDataUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tobesoft.platform.PlatformRequest;
import com.tobesoft.platform.data.Dataset;
import com.tobesoft.platform.data.DatasetList;
import com.tobesoft.platform.data.PlatformData;
import com.tobesoft.platform.data.VariableList;
import com.tobesoft.platform.data.Variant;

public class BaseService {

	private final Log logger = LogFactory.getLog(getClass());
	
	private Map reqParam;
	private VariableList miParam;
	private DatasetList datasets;
	private PlatformData miData;

	public PlatformData execute(String methodName, PlatformRequest miRequest,
			Map param) throws Exception {
	
		reqParam = param;
		miParam = miRequest.getVariableList();
		datasets = miRequest.getDatasetList();

		miData = new PlatformData();
		
		logger.debug("reqParam : " + reqParam);
		logger.debug("miParam : " + miParam);
		logger.debug("datasets : " + datasets);
		
		Class[] types = null;
		
		Method method = this.getClass().getMethod(methodName, types);
		method.invoke(this, null);
		return miData;
	}
	
	/**
	 * Get방식으로 전달된 파라미터 값을 반환한다.
	 * 
	 * @param key 파라미터 Name
	 * @return sRtn 파라미터 Value
	 * @throws Exception
	 */
	public String getReqParam(String key) throws Exception {

		String sRtn = "";

		String[] saVal = (String[]) reqParam.get(key);

		if (saVal != null) {

			for (int i = 0; i < saVal.length; i++) {

				sRtn += saVal[i] + "^";
			}
			sRtn = sRtn.substring(0, sRtn.length() - 1);
		}
		return sRtn;
	}

	/**
	 * VariableList Type으로 전달된 파라미터 값을 반환한다.
	 * @param key 파라미터 Key
	 * @return 파라마터 Value
	 * @throws Exception
	 */
	public String getParam(String key) throws Exception {

		return miParam.getValueAsString(key);
	}


	/**
	 * Request 객체로 전달된 Dataset을 반환한다.
	 * @param id Dataset ID
	 * @return Dataset Object
	 * @throws Exception
	 */
	public Dataset getDataset(String id) throws Exception {

		return datasets.get(id);
	}

	/**
	 * Request 객체로 전달된 Dataset을 HashMap Type으로 반환한다.
	 * 
	 * @param dsId Dataset ID
	 * @return HashMap Object
	 */
	public HashMap getMap(String dsId) throws Exception {

		return getMap(dsId, 0);
	}

	/**
	 * Request 객체로 전달된 Dataset의 특정 Row를 HashMap Type으로 반환한다.
	 * 
	 * @param dsId Dataset ID
	 * @param rnum Dataset Row Index
	 * @return HashMap Object
	 */
	public HashMap getMap(String dsId, int rnum) throws Exception {

		Dataset dataset = getDataset(dsId);

		if (dataset == null || dataset.getRowCount() == 0) {

			return null;
		}

		HashMap result = new HashMap();

		for (int i = 0; i < dataset.getColumnCount(); i++) {

			String columnName = dataset.getColumnId(i);

			result.put(columnName, dataset.getColumnAsObject(rnum, columnName));
		}
		return result;
	}

	/**
	 * Dataset의 특정 Row를 HashMap Type으로 반환한다.
	 * 
	 * @param ds Dataset Object
	 * @param rnum Dataset Row Index
	 * @return HashMap Object
	 */
	public HashMap getMap(Dataset ds, int rnum ) throws Exception {

		if (ds == null || ds.getRowCount() == 0) {

			return null;
		}
		
		/**
		 * @수정자: 정병호
		 * @수정날짜: 2012.07.25
		 * @str
		 */
		HashMap tmp = new HashMap();
		

		for (int i = 0; i < ds.getColumnCount(); i++) {
		    String columnName = ds.getColumnId(i);
		    tmp.put(columnName, ds.getColumnAsObject(rnum, columnName));
		}
		
		Iterator iTer = tmp.keySet().iterator();
		
		HashMap result = new HashMap();
		Object key = "";
		Object value = "";
		while(iTer.hasNext()){
		    key = iTer.next();
		    value = tmp.get(key);
		    //System.out.println("key: "+key);
		    if(value instanceof String){
			
			
			String ret = value.toString();
         	        
		        ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
		        ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
		                
		        ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
		        ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
		         	        
		        ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
		        ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
		         	        
		        ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "");
		        ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "");
		         	        
		        ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "");
		        ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "");
		         
		        ret = ret.replaceAll("\"", "&quot;");
		        ret = ret.replaceAll("\'", "&#39;");
		        
		        ret = ret.replaceAll("<", "&lt;");
		        ret = ret.replaceAll("<", "&gt;");
			
			value = ret;
		    }
		    
		    result.put(key, value);
		}
		return result;
		/**
		 * @수정자: 정병호
		 * @수정날짜: 2012.07.25
		 * @end
		 */
	}
	
	/**
	 * Dataset의 특정 Row를 HashMap Type으로 반환한다.
	 * 
	 * @param ds Dataset Object
	 * @param rnum Dataset Row Index
	 * @return HashMap Object
	 */
	public HashMap getDeleteMap(Dataset ds, int rnum ) throws Exception {

		if (ds == null || ds.getDeleteRowCount() == 0) {

			return null;
		}

		HashMap result = new HashMap();

		for (int i = 0; i < ds.getColumnCount(); i++) {

			String columnName = ds.getColumnId(i);
//			String columnType = ds.getColumnAsObject(i, columnName);
//			String col = ds.getColumnAsObject(i);
				
			result.put(columnName, ds.getDeleteColumn(rnum, columnName).toString().replace(".0", "") );
		}

		return result;
	}

	
	
	/**
	 * Dataset Type을 List Type으로 변환하여 반환한다.
	 * 
	 * @param dataset Dataset
	 * @return List Object
	 */
	public List dsToList(Dataset dataset) throws Exception {

		if (dataset == null || dataset.getRowCount() == 0) {

			return null;
		}
		int count = dataset.getColumnCount();
		Map map = null;
		List list = new ArrayList();

		for (int j = 0; j < dataset.getRowCount(); j++) {

			map = new HashMap();

			for (int i = 0; i < count; i++) {
				
				String columnName = dataset.getColumnId(i);
				map.put(columnName, dataset.getColumnAsObject(j, columnName));
			}
			list.add(map);
		}
		return list;
	}



	/**
	 * Map 객체를 Dataset 객체로 변환하여 PlatformData(miData) 객체에 저장한다.
	 * 
	 * @param id 생성될 Dataset ID
	 * @param map 변환될 Map 객체
	 */
	public void addList(String id, Map map) throws Exception {

		List list = new ArrayList();
		
		if(map!=null && map.size()!=0) {
			
			list.add(map);
		}
		miData.addDataset(MiDataUtil.listToDataset(id, list));
	}

	
	/**
	 * List 객체를 Dataset 객체로 변환하여 PlatformData(miData) 객체에 저장한다.
	 * 
	 * @param id 생성될 Dataset ID
	 * @param items 변환될 List 객체
	 */
	public void addList(String id, List list) throws Exception {

		miData.addDataset(MiDataUtil.listToDataset(id, list));
	}

	
	/**
	 * Dataset 객체를 PlatformData(miData) 객체에 저장한다.
	 * 
	 * @param ds
	 * @throws Exception
	 */
	public void addDataset(Dataset ds) throws Exception {

		miData.addDataset(ds);
	}

	
	/**
	 * 공통코드 조회 결과를 PlatformData(miData) 객체에 저장한다.
	 * @param id  공통코드 구분 Dataset Id
	 * @param list 공통코드 조회 결과 List
	 * @throws Exception
	 */
	public void addCommCode(String id, List list) throws Exception {

		for (int i = 0; i < getDataset(id).getRowCount(); i++) {

			miData.addDataset(MiDataUtil.listToCommCdDs(getMap(id, i), list));
		}
	}

	
	/**
	 * UI로 넘겨줄 parameter의 key/value를 PlatformData(miData) 객체에 저장한다.
	 * 
	 * @param name parameter key
	 * @param value parameter value
	 */
	public void addParam(String name, String value) {

		Variant var = new Variant(value);

		miData.addVariable(name, var);
	}

	
	/**
	 * UI로 넘겨줄 parameter의 key 와 값을 PlatformData(miData) 객체에 저장한다.
	 * 
	 * @param name parameter key
	 * @param value parameter value
	 */
	public void addParam(String name, int value) {

		Variant var = new Variant(value);

		miData.addVariable(name, var);
	}	
	
	/**
	 * UI로 전달할 메시지를 PlatformData 객체에 저장한다.
	 * 
	 * @param msg Return 메시지
	 */
	public void setResultMessage(String msg) {

		setResultMessage(-1, msg);
	}

	/**
	 * UI로 전달할 메시지를 PlatformData 객체에 저장한다.
	 * 
	 * @param rtnCd Return 코드
	 * @param msg Return 메시지
	 */
	public void setResultMessage(int code, String msg) {

		addParam("ErrorCode", code);
		addParam("ErrorMsg", msg);
	}
	
	
	/**
	 * Dataset의 특정 Row를 HashMap Type으로 반환한다.
	 * 
	 * @param ds Dataset Object
	 * @param rnum Dataset Row Index
	 * @return HashMap Object
	 */
	public List setClub(List list, String colum ) throws Exception {

		HashMap map_ = null;
		

		for( int i=0; i < list.size(); i++ )
		{
			map_ = (HashMap)list.get(i);
			Clob bordDesc = (Clob) map_.get(colum);
			
			if( bordDesc != null)
			{
		        Reader reader = bordDesc.getCharacterStream();
		        char bordDesc_char[] = new char[(int)bordDesc.length()];
		        reader.read(bordDesc_char);
		        
		        String outputBordDesc = new String(bordDesc_char);
		        
		        map_.put("BORD_DESC", outputBordDesc);
		        list.set(i, map_);
			}
			
		}
		
		return  list;
	}	
}


