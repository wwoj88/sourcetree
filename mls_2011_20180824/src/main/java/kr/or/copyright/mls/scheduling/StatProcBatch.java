package kr.or.copyright.mls.scheduling;

import java.text.SimpleDateFormat;

import kr.or.copyright.mls.adminStatProc.service.AdminStatProcService;

import org.springframework.stereotype.Controller;

@Controller
public class StatProcBatch {
	
	@javax.annotation.Resource(name = "adminStatProcService")
	private AdminStatProcService adminStatProcService;
	
	
	public void execute() throws Exception{
		
		//long time = System.currentTimeMillis();         
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");         
		//System.out.println("Cron trigger statProcBatch (59 second): current time = " + sdf.format(time));    
		
		System.out.println("=====================================");
		System.out.println("=====================================");
		System.out.println("상당한노력 스케쥴러 실행!!!!!");
		System.out.println("=====================================");
		System.out.println("=====================================");
		
		adminStatProcService.statProcScheduling();
	}

}
