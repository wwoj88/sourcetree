package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;


public interface FdcrAd97Service{

	/**
	 * 조회
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd97List1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * old worksMgntMnthList
	 * 월별 보고현황 조회
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd97List2(Map<String, Object> commandMap) throws Exception;
	
	/**
	 * old worksMgntList
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd97List3(Map<String, Object> commandMap) throws Exception;
}
