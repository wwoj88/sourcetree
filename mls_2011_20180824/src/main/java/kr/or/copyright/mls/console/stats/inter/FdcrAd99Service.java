package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;

public interface FdcrAd99Service{

	/**
	 * 등록부 연계 등록현황 - 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd99List1( Map<String, Object> commandMap ) throws Exception;
}
