package kr.or.copyright.mls.inmtPrps.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.inmtPrps.model.InmtPrps;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;
import kr.or.copyright.mls.common.common.model.ListResult;

public interface InmtPrpsService {

	public String maxYear(InmtPrps params);
	
	public ListResult inmtList(int pageNo, int rowPerPage, InmtPrps params);
	
	public List chkInmtList(InmtPrps params);
	
	public int prpsMastKey();
	
	public int prpsSeqnMax();
	
	public void inmtPspsInsert(Map map);
	
	public void inmtKappInsert(Map map);
	
	public void inmtFokapoInsert(Map map);
	
	public void inmtKrtraInsert(Map map);
	
	public void inmtPrpsRsltInsert(Map map);
	
	public void insertConnInfo(Map map);
	
	public String getMaxDealStat(RghtPrps params);
	
	public void inmtKappDelete(RghtPrps params);
	
	public void inmtFokapoDelete(RghtPrps params);
	
	public void inmtKrtraDelete(RghtPrps params);
	
	public void inmtPrpsRsltDelete(RghtPrps params);
	
	public void inmtPrpsDelete(RghtPrps params);
	
	public void inmtFileDeleteOne(InmtPrps params);
	
	public List inmtPrpsHistList(RghtPrps params);
	
	public List inmtPrpsTopList(Map map);

	public List inmtPrpsAmntList( Map map );
	
	public int muscMaxAmnt();
}