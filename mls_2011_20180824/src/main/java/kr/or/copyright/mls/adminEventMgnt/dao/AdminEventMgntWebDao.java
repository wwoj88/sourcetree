package kr.or.copyright.mls.adminEventMgnt.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminEventMgnt.model.MlEventItem;

public interface AdminEventMgntWebDao {
	
	public void addMultiQustItemMgnt(Map map);
	public Map getMultiQustItemMgntSeq(Map map);
	public void addMultiQustItemMgntChoice(Map map);
	public List getEventItemList(MlEventItem vo);
	public List getEventItemChoiceList(MlEventItem vo);
	public void addEventCorans(Map map) throws Exception;
	public void uptCoransSetN(MlEventItem vo);
	public void uptCoransSetY(Map map);
	public void delEventCorans(MlEventItem vo);
	public void delMlEventCorans(MlEventItem vo);
	public void delMlEventItemChoice(MlEventItem vo);
	public void delMlEventItem(MlEventItem vo);
	public void uptEventItemSeqn(MlEventItem vv);
	public MlEventItem getEventItemDetl(MlEventItem vo);
	public void uptMultiQustItemMgnt(MlEventItem vo);
	public String getEventTitle(MlEventItem vo);
		
}
