package kr.or.copyright.mls.noneRght.dao;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

public class NoneRghtSqlMapDao extends SqlMapClientDaoSupport implements NoneRghtDao{

	// 음악목록 건수
	public int muscRghtCount(RghtPrps rghtPrps) {
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("NoneRght.muscNoneRghtListCount", rghtPrps));
	}
	
	// 음악목록 
	public List muscRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("NoneRght.muscNoneRghtList", rghtPrps);
	} 
	
	// 도서목록 건수
	public int bookRghtCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("NoneRght.bookNoneRghtCount", rghtPrps));
	}
	
	// 도서 목록
	public List bookRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("NoneRght.bookNoneRghtList", rghtPrps);
	}
	
	// 방송대본 건수
	public int scriptRghtCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("NoneRght.scriptNoneRghtCount", rghtPrps));
	}
	
	// 방송대본 목록
	public List scriptRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("NoneRght.scriptNoneRghtList", rghtPrps);
	}
	
	// 이미지목록 건수
	public int imageRghtCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("NoneRght.imageNoneRghtCount", rghtPrps));
	}
	
	// 이미지 목록
	public List imageRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("NoneRght.imageNoneRghtList", rghtPrps);
	}
	
	// 영화목록 건수
	public int mvieRghtCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("NoneRght.mvieNoneRghtCount", rghtPrps));
	}
	
	// 영화 목록
	public List mvieRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("NoneRght.mvieNoneRghtList", rghtPrps);
	}
	
	// 방송목록 건수
	public int broadcastRghtCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("NoneRght.broadcastNoneRghtCount", rghtPrps));
	}
	
	// 방송 목록
	public List broadcastRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("NoneRght.broadcastNoneRghtList", rghtPrps);
	}
	
	//  뉴스목록 건수
	public int newsRghtCount(RghtPrps rghtPrps) {
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("NoneRght.newsNoneRghtCount", rghtPrps));
	}
	
	//  뉴스목록
	public List newsRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("NoneRght.newsNoneRghtList", rghtPrps);
	}
}
