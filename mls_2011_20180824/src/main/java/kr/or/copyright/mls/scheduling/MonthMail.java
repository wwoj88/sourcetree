package kr.or.copyright.mls.scheduling;

import java.text.SimpleDateFormat;

import kr.or.copyright.mls.adminEmailMgnt.service.AdminEmailMgntService;
import kr.or.copyright.mls.adminStatProc.service.AdminStatProcService;

import org.springframework.stereotype.Controller;

@Controller
public class MonthMail {
	
	@javax.annotation.Resource(name = "AdminEmailMgntService")
	private AdminEmailMgntService adminEmailMgntService;
	
	
	public void execute() throws Exception{
		
		long time = System.currentTimeMillis();         
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");         
		System.out.println("Cron trigger statProcBatch (59 second): current time = " + sdf.format(time));    
		
		adminEmailMgntService.monthMailScheduling();
	}

}
