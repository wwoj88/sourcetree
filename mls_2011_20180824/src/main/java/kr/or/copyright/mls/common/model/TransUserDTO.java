package kr.or.copyright.mls.common.model;

import java.util.List;

public class TransUserDTO extends CorpUserInfo {

     private String userId;
     private String userPwd;
     private String membSeq;
     private String membId;
     private String membName;
     private String membBirth;
     private String sexCode;
     private String membCelnum;
     private String membEmail;
     private String membEmailRecpYn;
     private String membSmsRecpYn;
     private String membCi;
     private String ssoClientId;
     private String siteMembId;
     private String membType;
     private String membDivis;
     private String membCorpnum;
     private String membBuisnRegnum;
     private String compZipcd;
     private String compAddr;
     private String compDetAddr;
     private String mangName;
     private String mangCelnum;
     
     private List<CorpUserInfo> corpUserInfo;

     
     
     
     public List<CorpUserInfo> getCorpUserInfo() {
     
          return corpUserInfo;
     }



     
     public void setCorpUserInfo(List<CorpUserInfo> corpUserInfo) {
     
          this.corpUserInfo = corpUserInfo;
     }



     public String getMembType() {

          return membType;
     }



     public void setMembType(String membType) {

          this.membType = membType;
     }



     public String getMembDivis() {

          return membDivis;
     }



     public void setMembDivis(String membDivis) {

          this.membDivis = membDivis;
     }



     public String getMembCorpnum() {

          return membCorpnum;
     }



     public void setMembCorpnum(String membCorpnum) {

          this.membCorpnum = membCorpnum;
     }



     public String getMembBuisnRegnum() {

          return membBuisnRegnum;
     }



     public void setMembBuisnRegnum(String membBuisnRegnum) {

          this.membBuisnRegnum = membBuisnRegnum;
     }



     public String getCompZipcd() {

          return compZipcd;
     }



     public void setCompZipcd(String compZipcd) {

          this.compZipcd = compZipcd;
     }



     public String getCompAddr() {

          return compAddr;
     }



     public void setCompAddr(String compAddr) {

          this.compAddr = compAddr;
     }



     public String getCompDetAddr() {

          return compDetAddr;
     }



     public void setCompDetAddr(String compDetAddr) {

          this.compDetAddr = compDetAddr;
     }



     public String getMangName() {

          return mangName;
     }



     public void setMangName(String mangName) {

          this.mangName = mangName;
     }



     public String getMangCelnum() {

          return mangCelnum;
     }



     public void setMangCelnum(String mangCelnum) {

          this.mangCelnum= mangCelnum;
     }



     public String getMembEmail() {

          return membEmail;
     }



     public void setMembEmailEncry(String membEmail) {

          this.membEmail= membEmail;
     }


     public String getMembSeq() {

          return membSeq;
     }


     public void setMembSeq(String membSeq) {

          this.membSeq = membSeq;
     }


     public String getMembId() {

          return membId;
     }


     public void setMembId(String membId) {

          this.membId = membId;
     }


     public String getMembName() {

          return membName;
     }


     public void setMembName(String membName) {

          this.membName = membName;
     }


     public String getMembBirth() {

          return membBirth;
     }


     public void setMembBirth(String membBirth) {

          this.membBirth = membBirth;
     }


     public String getSexCode() {

          return sexCode;
     }


     public void setSexCode(String sexCode) {

          this.sexCode = sexCode;
     }


     public String getMembCelnum() {

          return membCelnum;
     }


     public void setMembCelnum(String membCelnum) {

          this.membCelnum = membCelnum;
     }




     public String getMembEmailRecpYn() {

          return membEmailRecpYn;
     }


     public void setMembEmailRecpYn(String membEmailRecpYn) {

          this.membEmailRecpYn = membEmailRecpYn;
     }


     public String getMembSmsRecpYn() {

          return membSmsRecpYn;
     }


     public void setMembSmsRecpYn(String membSmsRecpYn) {

          this.membSmsRecpYn = membSmsRecpYn;
     }


     public String getMembCi() {

          return membCi;
     }


     public void setMembCi(String membCi) {

          this.membCi = membCi;
     }


     public String getSsoClientId() {

          return ssoClientId;
     }


     public void setSsoClientId(String ssoClientId) {

          this.ssoClientId = ssoClientId;
     }


     public String getSiteMembId() {

          return siteMembId;
     }


     public void setSiteMembId(String siteMembId) {

          this.siteMembId = siteMembId;
     }

     public String getUserId() {

          return userId;
     }

     public void setUserId(String userId) {

          this.userId = userId;
     }

     public String getUserPwd() {

          return userPwd;
     }

     public void setUserPwd(String userPwd) {

          this.userPwd = userPwd;
     }



}
