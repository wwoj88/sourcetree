package kr.or.copyright.mls.console.legal;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd11Service;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;

import org.springframework.stereotype.Service;

import com.softforum.xdbe.xCrypto;

@Service( "fdcrAd11Service" )
public class FdcrAd11ServiceImpl extends CommandService implements FdcrAd11Service{

	// AdminStatMgntSqlMapDao
	@Resource( name = "fdcrAd06Dao" )
	private FdcrAd06Dao fdcrAd06Dao;

	// AdminStatBoardSqlMapDao
	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	// AdminCommonSqlMapDao
	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	/**
	 * 법정허락 대상저작물 공고 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd11List1( Map<String, Object> commandMap ) throws Exception{
		
		List pageList = (List) fdcrAd01Dao.statBord05RowList( commandMap ); // 페이징카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		
		// DAO 호출
		List list = (List) fdcrAd01Dao.statBord05List( commandMap );
		commandMap.put( "ds_list", list );
	}

	/**
	 * 법정허락 대상저작물 공고 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd11View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd01Dao.statBord05Detail( commandMap );
		List objectList = (List) fdcrAd01Dao.statBord05Object( commandMap );
		List objectFileList = (List) fdcrAd01Dao.statBord05ObjectFile( commandMap );

		commandMap.put( "ds_list", list );
		commandMap.put( "ds_object", objectList );
		commandMap.put( "ds_object_file", objectFileList );
	}

	/**
	 * 법정허락 대상저작물 공고 게시판 등록자 정보 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd11Pop1( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List list2 = (List) consoleCommonDao.userDetlList( commandMap );

		/* resd복호화 str 20121108 정병호 */
		/*
		xCrypto.RegisterEx(
			"pattern7",
			2,
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
			"pool1",
			"mcst_db",
			"mcst_owner",
			"mcst_table",
			"pattern7" );

		Map list = new HashMap();
		list = (Map) list2.get( 0 );

		String RESD_CORP_NUMB = "";

		// 주민번호가 notnull 인 경우만.
		if( list.get( "RESD_CORP_NUMB" ) != null
			&& list.get( "RESD_CORP_NUMB" ).toString().replaceAll( " ", "" ).length() > 0 ){
			RESD_CORP_NUMB = xCrypto.Decrypt( "pattern7", (String) list.get( "RESD_CORP_NUMB" ) ).replaceAll( " ", "" );// (원본
																														// 주민번호
																														// 복호화)

			String RESD_CORP_NUMB1 = RESD_CORP_NUMB.substring( 0, 6 );
			String RESD_CORP_NUMB2 = ""; // RESD_CORP_NUMB.substring(6);

			list.put( "RESD_CORP_NUMB", RESD_CORP_NUMB1 + "-" + RESD_CORP_NUMB2 );
		}
		
		commandMap.put( "ds_list", list );
		*/
		commandMap.put( "ds_list", list2 );

	}

	/**
	 * 이의제기 진행상태 수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd11Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{

			/*
			String[] STAT_OBJC_CDS = (String[]) commandMap.get( "STAT_OBJC_CD" );
			String[] STAT_OBJC_RSLT_CDS = (String[]) commandMap.get( "STAT_OBJC_RSLT_CD" );
			String[] FILE_NAMES = (String[]) commandMap.get( "FILE_NAME" );
			String[] ATTACH_FILE_CONTENT0S = (String[]) commandMap.get( "ATTACH_FILE_CONTENT0" );
			String[] FILE_PATHS = (String[]) commandMap.get( "FILE_PATH" );
			String[] REAL_FILE_NAMES = (String[]) commandMap.get( "REAL_FILE_NAME" );
			String[] FILE_SIZES = (String[]) commandMap.get( "FILE_SIZE" );
			String[] ATTACH_FILE_SIZE0S = (String[]) commandMap.get( "ATTACH_FILE_SIZE0" );
			String[] WORKS_IDS = (String[]) commandMap.get( "WORKS_ID" );
			*/
			
			String STAT_OBJC_CD = (String) commandMap.get( "STAT_OBJC_CD" );
			String STAT_OBJC_RSLT_CD = (String) commandMap.get( "STAT_OBJC_RSLT_CD" );
			String WORKS_ID = (String) commandMap.get( "WORKS_ID" );

			for( int i = 0; i < STAT_OBJC_CD.length(); i++ ){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "STAT_OBJC_CD", STAT_OBJC_CD );
				param.put( "STAT_OBJC_RSLT_CD", STAT_OBJC_RSLT_CD );
				
				int attachSeqn = consoleCommonDao.getNewAttcSeqn();
				
				// 이의제기 신청정보 수정
				fdcrAd01Dao.statBordObjcUpdate( param );

				if( commandMap.get( WORKS_ID ) == null || commandMap.get( WORKS_ID ).toString().equals( "" ) ){

					// 이의제기 진행상태 처리결과 -기각일때
					if( commandMap.get( STAT_OBJC_RSLT_CD).toString().equals( "2" ) ){
						fdcrAd01Dao.statBordObjcRsltUpdate( commandMap );
					}
				}

				// 이의제기신청 처리내역등록
				fdcrAd01Dao.statBordObjcShisInsert( commandMap );
			}
			/*
			for( int i = 0; i < STAT_OBJC_CDS.length; i++ ){

				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "STAT_OBJC_CD", STAT_OBJC_CDS[i] );
				param.put( "STAT_OBJC_RSLT_CD", STAT_OBJC_RSLT_CDS[i] );

				int attachSeqn = consoleCommonDao.getNewAttcSeqn();
				String fileName = (String) commandMap.get( FILE_NAMES[i] );

				if( fileName != null && !fileName.equals( "" ) ){
					commandMap.put( "ATTC_SEQN", attachSeqn );

					String filePath = "";
					String realFileNm = "";
					String fileSize = "";
					byte[] file = null;
					Map upload = null;

					if( commandMap.get( ATTACH_FILE_CONTENT0S[i] ) == null
						|| commandMap.get( ATTACH_FILE_CONTENT0S[i] ).toString().equals( "" ) ){
						filePath = (String) commandMap.get( FILE_PATHS[i] );
						realFileNm = (String) commandMap.get( REAL_FILE_NAMES[i] );
						fileSize = (String) commandMap.get( FILE_SIZES[i] );
					}else{
						file = (byte[]) commandMap.get( ATTACH_FILE_CONTENT0S[i] );
						upload = FileUtil.uploadMiFile( fileName, file );
						filePath = upload.get( "FILE_PATH" ).toString();
						realFileNm = upload.get( "REAL_FILE_NAME" ).toString();
						fileSize = commandMap.get( ATTACH_FILE_SIZE0S[i] ).toString();
					}

					Map fileMap = new HashMap();
					fileMap.put( "ATTC_SEQN", attachSeqn );
					fileMap.put( "FILE_ATTC_CD", "BO" );
					fileMap.put( "FILE_NAME", fileName );
					fileMap.put( "FILE_PATH", filePath );
					fileMap.put( "FILE_SIZE", fileSize );
					fileMap.put( "REAL_FILE_NAME", realFileNm );
					fileMap.put( "RGST_IDNT", commandMap.get( "USER_ID" ) );

					// 첨부파일 등록
					fdcrAd06Dao.adminFileInsert( fileMap );
				}

				// 이의제기 신청정보 수정
				fdcrAd01Dao.statBordObjcUpdate( param );

				if( commandMap.get( WORKS_IDS[i] ) == null || commandMap.get( WORKS_IDS[i] ).toString().equals( "" ) ){

					// 이의제기 진행상태 처리결과 -기각일때
					if( commandMap.get( STAT_OBJC_RSLT_CDS[i] ).toString().equals( "2" ) ){
						fdcrAd01Dao.statBordObjcRsltUpdate( commandMap );
					}
				}

				// 이의제기신청 처리내역등록
				fdcrAd01Dao.statBordObjcShisInsert( commandMap );

				// 승인, 거절 시 메일,sms 보내기
				if( commandMap.get( "RGST_IDNT" ) != null
					&& ( commandMap.get( "STAT_OBJC_CD" ).equals( "2" ) || commandMap.get( "STAT_OBJC_CD" ).equals( "3" ) ) ){
					// 메일 보내기
					Map memMap = new HashMap();
					memMap.put( "USER_IDNT", commandMap.get( "RGST_IDNT" ) );
					List memList = consoleCommonDao.selectMemberInfo( memMap );
					memMap = (Map) memList.get( 0 );

					String statNm = "";
					if( commandMap.get( "STAT_OBJC_CD" ).equals( "2" ) ){
						statNm = "접수";
					}else if( commandMap.get( "STAT_OBJC_CD" ).equals( "3" ) ){
						statNm = "처리완료";
					}

					String rsltNm = "";
					if( commandMap.get( "STAT_OBJC_RSLT_CD" ).equals( "1" ) ){
						rsltNm = "승인";
					}else if( commandMap.get( "STAT_OBJC_RSLT_CD" ).equals( "2" ) ){
						rsltNm = "기각";
					}

					String host = Constants.MAIL_SERVER_IP; // smtp서버
					String to = memMap.get( "MAIL" ).toString(); // 수신EMAIL
					String toName = memMap.get( "USER_NAME" ).toString(); // 수신인
					String title = commandMap.get( "TITLE" ).toString();
					String subject = "[저작권찾기] 이의제기 " + statNm;

					String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템
																	// 대표 메일
					String fromName = Constants.SYSTEM_NAME;

					if( memMap.get( "MAIL_RECE_YSNO" ).equals( "Y" ) && to != null && to != "" ){

						// ------------------------- mail 정보
						// ----------------------------------//
						MailInfo info = new MailInfo();
						info.setFrom( from );
						info.setFromName( fromName );
						info.setHost( host );
						info.setEmail( to );
						info.setEmailName( toName );
						info.setSubject( subject );

						StringBuffer sb = new StringBuffer();

						sb.append( "				<tr>" );
						sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">" );
						sb.append( "					<p style=\"font-weight:bold; background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 이의제기 "
							+ statNm + " 정보</p>" );
						sb.append( "					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">" );
						sb.append( "						<li style=\"background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자 이름 : " + toName
							+ " </li>" );
						sb.append( "						<li style=\"background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : "
							+ commandMap.get( "TITLE" ) + " </li>" );
						sb.append( "						<li style=\"background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청일자 : "
							+ commandMap.get( "OBJC_RGST_DTTM" ) + " </li>" );
						sb.append( "						<li style=\"background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 처리상태 : " + statNm
							+ " </li>" );
						sb.append( "						<li style=\"background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 처리결과 : " + rsltNm
							+ " </li>" );
						sb.append( "					</ul>" );
						sb.append( "					</td>" );
						sb.append( "				</tr>" );

						info.setMessage( sb.toString() );
						info.setMessage( new String( MailMessage.getChangeInfoMailText2( info ) ) );

						
						 * sb.append("<div class=\"mail_title\">" + toName +
						 * "님, " +title+"의 이의제기 신청이  "+statNm+" 되었습니다. </div>");
						 * sb.append("<div class=\"mail_contents\"> ");
						 * sb.append(
						 * "	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "
						 * ); sb.append("		<tr> "); sb.append("			<td> ");
						 * sb.append(
						 * "				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> "
						 * ); // 변경부분 sb.append("					<tr> ");
						 * sb.append("						<td class=\"tdLabel\">제목(제호)</td> "
						 * ); sb.append("						<td class=\"tdData\">" +
						 * map.get("TITLE") + "</td> ");
						 * sb.append("					</tr> "); sb.append("					<tr> ");
						 * sb.append("						<td class=\"tdLabel\">신청일자</td> ");
						 * sb.append("						<td class=\"tdData\">" +
						 * map.get("OBJC_RGST_DTTM") + "</td> ");
						 * sb.append("					</tr> "); sb.append("					<tr> ");
						 * sb.append("						<td class=\"tdLabel\">처리상태</td> ");
						 * sb.append("						<td class=\"tdData\">" + statNm +
						 * "</td> "); sb.append("					</tr> ");
						 * sb.append("					<tr> ");
						 * sb.append("						<td class=\"tdLabel\">처리결과</td> ");
						 * sb.append("						<td class=\"tdData\">" + rsltNm +
						 * "</td> "); sb.append("					</tr> ");
						 * sb.append("				</table> "); sb.append("			</td> ");
						 * sb.append("		</tr> "); sb.append("	</table> ");
						 * sb.append("</div> "); info.setMessage(new
						 * String(MailMessage
						 * .getChangeInfoMailText(sb.toString())));
						 
						MailManager manager = MailManager.getInstance();

						info = manager.sendMail( info );
						if( info.isSuccess() ){
							System.out.println( ">>>>>>>>>>>> message success :" + info.getEmail() );
						}else{
							System.out.println( ">>>>>>>>>>>> message false   :" + info.getEmail() );
						}

					}

					// sms 보내기
					String smsTo = memMap.get( "MOBL_PHON" ).toString();
					String smsFrom = Constants.SYSTEM_TELEPHONE;

					String smsMessage = "[저작권찾기]" + title + "의 이의제기 신청이 " + statNm + " 되었습니다. ";

					if( memMap.get( "SMS_RECE_YSNO" ) != null ){
						if( memMap.get( "SMS_RECE_YSNO" ).equals( "Y" ) && smsTo != null && smsTo != "" ){

							SMSManager smsManager = new SMSManager();

							System.out.println( "return = " + smsManager.sendSMS( smsTo, smsFrom, smsMessage ) );
						}
					}

				} // end email, sms
			}
			*/
			
			
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 이의제기 진행상태내역조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd11Pop2( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd01Dao.adminStatObjcShisSelect( commandMap );
		commandMap.put( "ds_list", list );
	}

}
