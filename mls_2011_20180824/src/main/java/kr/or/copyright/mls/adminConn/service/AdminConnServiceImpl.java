package kr.or.copyright.mls.adminConn.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminConn.dao.AdminConnDao;
import kr.or.copyright.mls.common.service.BaseService;
import com.tobesoft.platform.data.Dataset;

public class AdminConnServiceImpl extends BaseService implements AdminConnService {

	//private Log logger = LogFactory.getLog(getClass());

	private AdminConnDao adminConnDao;
	
	public void setAdminConnDao(AdminConnDao adminConnDao){
		this.adminConnDao = adminConnDao;
	}
	
	
	// 회원정보 목록조회
	public void monthList() throws Exception {

		//boolean isValid = true;
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminConnDao.monthList(map); 

		addList("ds_list", list); // 로그인 사용자 정보

	}
	
	// 회원정보 목록조회
	public void dayList() throws Exception {

		//boolean isValid = true;
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminConnDao.dayList(map); 

		addList("ds_list", list); // 로그인 사용자 정보

	}	
	
	// 기간별 접속 통계
	public void connStatPeriList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);
		
		List list = null;
		if("0".equals((String)map.get("GUBUN"))){
			list = (List) adminConnDao.adminStatPeriList(map);
		}else{
			list = (List) adminConnDao.adminStatPeriList2(map);
		}
		
		addList("ds_list", list);
	}
	
	// 기간별 접속 통계(월별)
	public void connStatPeriMonthList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);
		
		List list = null;
		if("0".equals((String)map.get("GUBUN"))){
			list = (List) adminConnDao.adminStatPeriMonthList(map);
		}else{
			list = (List) adminConnDao.adminStatPeriMonthList2(map);
		}
		
		addList("ds_list", list);
	}
	
	// 보상금/내권리찾기 통계
	public void rsltList() throws Exception {

		//boolean isValid = true;
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminConnDao.rsltList(map); 

		addList("ds_list", list); // 로그인 사용자 정보

	}		
	
	// 저작권찾기/보상금 기간별 통계
	public void rsltPeriList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminConnDao.rsltPeriList(map); 

		addList("ds_list", list); // 로그인 사용자 정보
	}
	
	// 접속내역
	public void userList() throws Exception {

	//	boolean isValid = true;
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminConnDao.userList(map); 

		addList("ds_list", list); // 로그인 사용자 정보

	}		
	
	// 접속내역 삭제
	public void userListDelete() throws Exception {
		
		Dataset ds_delList = getDataset("ds_delList");  
//ds_delList.printDataset();		
		for( int i = 0; i<ds_delList.getRowCount(); i++ ) {
			
			Map deleteMap = getMap(ds_delList, i);
//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+deleteMap.toString());
			adminConnDao.userListDelete(deleteMap);
		}
	}


	public void statYearApplyList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminConnDao.statYearApplyList(map);

		addList("ds_list1", list); 
		
	}


	public void statYearSpplyList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminConnDao.statYearSpplyList(map); 

		addList("ds_list2", list); 
		
	}


	public void statYearLgmtAmntList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminConnDao.statYearLgmtAmntList(map);
		addList("ds_list3", list); 
		
	}
	
	public void connStatAllList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list1 = (List) adminConnDao.statYearPrpsApplyList(map);
		List list2 = (List) adminConnDao.statPrpsWorksList(map);
		List list3 = (List) adminConnDao.statYearInmtList(map);
		List list4 = (List) adminConnDao.statYearQuProcList(map);
		List list5 = (List) adminConnDao.statYearGenreProcList(map);
		
		
		addList("ds_list1", list1); 
		addList("ds_list2", list2); 
		addList("ds_list3", list3); 
		addList("ds_list4", list4); 
		addList("ds_list5", list5);
		
		
	}
	
	public void statYearPrpsApplyList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list1 = (List) adminConnDao.statYearPrpsApplyList(map);
		
		addList("ds_list1", list1);
		
		
	}
	public void statPrpsWorksList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);		
	
		//DAO호출
		List list2 = (List) adminConnDao.statPrpsWorksList(map);	
	
		addList("ds_list2", list2); 		
			
					
	}
	public void statYearInmtList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list3 = (List) adminConnDao.statYearInmtList(map);
		
		addList("ds_list3", list3); 		
		
	}
	public void statYearQuProcList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list4 = (List) adminConnDao.statYearQuProcList(map);
		
		addList("ds_list4", list4);		
		
	}
	public void statYearGenreProcList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list5 = (List) adminConnDao.statYearGenreProcList(map);
		
		addList("ds_list5", list5); 		
		
	}	
	

	
	public void statPrpsApplyList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);		
	
		//DAO호출
		List list = (List) adminConnDao.statPrpsApplyList(map);	
	
		addList("ds_list", list); 		
			
					
	}
	
	
	
}
