package kr.or.copyright.mls.console.rcept;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd32Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 관리저작물 접수 및 처리 > 상당한노력 > 진행현황
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd32Controller extends DefaultController{

	@Autowired
	private FdcrAd32Service fdcrAd32Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd32Controller.class );

	/**
	 * 상당한노력 진행현황 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd32List1.page" )
	public String fdcrAd32List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd32List1 Start" );
		// 파라미터 셋팅
		commandMap.put( "SCHEDULE_SRCH", "" );
		commandMap.put( "P_TRST_ORGN_CODE", "" );
		commandMap.put( "P_EXCP_STAT_CD", "" );
		String[] result = request.getParameterValues( "result" );
		commandMap.put( "result", result );

		fdcrAd32Service.fdcrAd32List1( commandMap );
		model.addAttribute( "ds_works_list", commandMap.get( "ds_works_list" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_set_list", commandMap.get( "ds_set_list" ) );
		logger.debug( "fdcrAd32List1 End" );
		return "test";
	}
}
