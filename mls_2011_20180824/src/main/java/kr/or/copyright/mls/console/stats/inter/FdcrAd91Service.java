package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;


public interface FdcrAd91Service{
	/**
	 * 접속내역 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd91List1( Map<String, Object> commandMap ) throws Exception;
}
