package kr.or.copyright.mls.console.ntcn.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface FdcrAd75Dao{

	/**
	 * 공통 첨부파일 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> selectAttachFileList( Map<String, Object> commandMap ) throws SQLException;

	/**
	 * 공통 첨부파일 정보
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> selectAttachFileInfo( Map<String, Object> commandMap ) throws SQLException;

	/**
	 * 첨부파일 등록
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public int insertBoardFile( Map<String, Object> commandMap ) throws SQLException;

	/**
	 * 게시물 목록 카운트
	 * 
	 * @return
	 * @throws SQLException
	 */
	public int selectListCount( Map<String, Object> commandMap ) throws SQLException;

	/**
	 * 게시물 목록 정보
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> selectList( Map<String, Object> commandMap ) throws SQLException;

	/**
	 * 게시물 조회
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> selectView( Map<String, Object> commandMap ) throws SQLException;

	/**
	 * 등록
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public int insertBoardInfo( Map<String, Object> commandMap ) throws SQLException;

	/**
	 * 수정
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public int updateBoardInfo( Map<String, Object> commandMap ) throws SQLException;

	/**
	 * 삭제
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public int deleteBoardInfo( Map<String, Object> commandMap ) throws SQLException;

	/**
	 * 삭제
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public int deleteBoardFile( Map<String, Object> commandMap ) throws SQLException;
}
