package kr.or.copyright.mls.support.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import kr.or.copyright.mls.support.constant.LoginConstants;
import kr.or.copyright.mls.support.constant.SignContants;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.common.userLogin.model.UserLoginDTO;
import kr.or.copyright.mls.support.dto.SignDTO;
import signgate.crypto.util.Base64Util;
import signgate.crypto.util.CertUtil;
import signgate.crypto.util.CipherUtil;
import signgate.crypto.util.FileUtil;
import signgate.crypto.util.SignUtil;

/**
 * 공인인증서 정보를 확인하는 Utility Class
 * 
 * @author 이상유
 * @version $Id: SignCertUtil.java,v 1.3 2014/08/06 11:33:19 sora Exp $
 */
public class SignCertUtil {

	private Logger log = Logger.getLogger(this.getClass());
	
	// 허용할 인증서 정책 OID 리스트 (범용 인증서 OID 리스트)
	public static final String[] AllowedPolicyOIDs = SignContants.AllowedPolicyOIDs;
	
	// 인증서 관련 변수
	byte[] ServerSignCert = null;
	byte[] ServerSignKey = null;
	byte[] ServerKmCert = null;
	byte[] ServerKmKey = null;
	String ServerSignCertPem = "";
	String ServerKmCertPem = "";
	
	private String SignCertMessage = "";	// 인증서 메시지 저장용 변수
	private String CertDN = "";	// 인증서 DN 정보 저장용 변수
	private String SerialNumber = "";	// 인증서에서 SerialNumber 값 저장용 변수
	private String OriginalMessage = "";	// 전자서명 원본 값
	private String SignValue = "";	// 전자서명 값
	private String CertNotBefore = "";	// 인증서에서 유효성 시작 시점 저장용 변수
	private String CertNotAfter = "";	// 인증서에서 유효성 종료 시점 저장용 변수
	
	
	/**
	 * SignCertUtil 생성
	 */
	public SignCertUtil(){		
	}

	/**
	 * 서버 인증서 정보를 초기화
	 * 
	 * @throws Exception
	 */
	private void signInit() throws Exception {
		
		try {
			
			log.info("SignContants.ServerSignCertFile : "+SignContants.ServerSignCertFile);
			log.info("SignContants.ServerSignKeyFile : "+SignContants.ServerSignKeyFile);
			log.info("SignContants.ServerKmCertFile : "+SignContants.ServerKmCertFile);
			log.info("SignContants.ServerKmKeyFile : "+SignContants.ServerKmKeyFile);
			
			// 서버 인증서 정보 초기화
			ServerSignCert = FileUtil.readBytesFromFileName(SignContants.ServerSignCertFile);
			ServerSignKey = FileUtil.readBytesFromFileName(SignContants.ServerSignKeyFile);
			ServerKmCert = FileUtil.readBytesFromFileName(SignContants.ServerKmCertFile);
			ServerKmKey = FileUtil.readBytesFromFileName(SignContants.ServerKmKeyFile);
	
			ServerSignCertPem = CertUtil.derToPem( ServerSignCert );
			ServerKmCertPem = CertUtil.derToPem( ServerKmCert );
		} catch (Exception e) {
			log.info("Exception : "+e.getStackTrace());
		}
	}

	/**
	 * 공인인증서 및 전자인증정보 확인
	 * 
	 * @param _LogInDTO
	 * @param errors
	 * @return
	 * @throws Exception
	 */
	private boolean signValidate(SignDTO paramDTO) throws Exception {
		boolean rtnValue = false;

		// 전자서명 정보를 확인한다.
		if(paramDTO.getMemberType().equals(LoginConstants.MEMBER_TYPE_1) 
				|| paramDTO.getMemberType().equals(LoginConstants.MEMBER_TYPE_4) 
				|| paramDTO.getMemberType().equals(LoginConstants.MEMBER_TYPE_5)){
			
			// 개인(1), 임의단체(4), 외국인(5)
			if(paramDTO.getSsnNo().equals("")){
				//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"주민등록번호 정보가 존재하지 않습니다.");
				setSignCertMessage("주민등록번호 정보가 존재하지 않습니다.");
				log.info("signValidate - paramDTO.getSsnNo() : "+getSignCertMessage());
				return rtnValue;
			}
		} else if(paramDTO.getMemberType().equals(LoginConstants.MEMBER_TYPE_2) 
				|| paramDTO.getMemberType().equals(LoginConstants.MEMBER_TYPE_3)) {
			// 법인사용자(2), 개인사용자(3)
			if(paramDTO.getCrnNo().equals("")){
				//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"사업자번호 정보가 존재하지 않습니다.");
				setSignCertMessage("사업자번호 정보가 존재하지 않습니다.");
				log.info("signValidate - paramDTO.getSsnNo() : "+getSignCertMessage());
				return rtnValue;
			}
		} else {
			// 디폴트 : 법인사용자(3), 개인사용자(4)
			if(paramDTO.getCrnNo().equals("")){
				//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"사업자번호 정보가 존재하지 않습니다.");
				setSignCertMessage("사업자번호 정보가 존재하지 않습니다.");
				log.info("signValidate - paramDTO.getSsnNo() : "+getSignCertMessage());
				return rtnValue;
			}
		}

		if(paramDTO.getCertId().equals("")){
			//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"인증서 추적을 위한 정보가 존재하지 않습니다.");
			setSignCertMessage("인증서 추적을 위한 정보가 존재하지 않습니다.");
			log.info("signValidate - paramDTO.getCertId() : "+getSignCertMessage());
			return rtnValue;
		}
		if(paramDTO.getUserSignCert().equals("")){
			//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"공인인증서 정보가 존재하지 않습니다.");
			setSignCertMessage("공인인증서 정보가 존재하지 않습니다.");
			log.info("signValidate - paramDTO.getUserSignCert() : "+getSignCertMessage());
			return rtnValue;
		}
		if(paramDTO.getUserSignValue().equals("")){
			//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"전자서명 값 정보가 존재하지 않습니다.");
			setSignCertMessage("전자서명 값 정보가 존재하지 않습니다.");
			log.info("signValidate - paramDTO.getUserSignValue() : "+getSignCertMessage());
			return rtnValue;
		}
		if(paramDTO.getEncryptedSessionKey().equals("")){
			//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"비밀키 정보가 존재하지 않습니다.");
			setSignCertMessage("비밀키 정보가 존재하지 않습니다.");
			log.info("signValidate - paramDTO.getEncryptedSessionKey() : "+getSignCertMessage());
			return rtnValue;
		}
		if(paramDTO.getEncryptedUserRandomNumber().equals("")){
			//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"인증서 소유자 확인을 위한 정보가 없습니다.");
			setSignCertMessage("인증서 소유자 확인을 위한 정보가 없습니다.");
			log.info("signValidate - paramDTO.getEncryptedUserRandomNumber() : "+getSignCertMessage());
			return rtnValue;
		}
		if(paramDTO.getEncryptedUserSSN().equals("")){
			//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"암호화 정보가 존재하지 않습니다.");
			setSignCertMessage("암호화 정보가 존재하지 않습니다.");
			log.info("signValidate - paramDTO.getEncryptedUserSSN() : "+getSignCertMessage());
			return rtnValue;
		}
		return true;
	}
	
	/**
	 * 전자서명 정보를 확인한다.
	 * 
	 * @param request
	 * @param paramDTO
	 * @return
	 * @throws Exception
	 */
	public boolean chkSignCert(HttpServletRequest request, SignDTO paramDTO) throws Exception {
		boolean rtnValue = true;
		
		log.info("SignDTO : "+paramDTO.toString() );
		
		HttpSession session = request.getSession();
//		User logindto = (User)request.getSession(true).getAttribute(LoginConstants.LOGIN_SESSION_ATTRIBUTE);
		User logindto = SessionUtil.getSession(request);

		// 주민등록번호(사업자등록번호)와 전자서명정보가 전달된 경우 공인인증서 확인 절차 수행
		if((!paramDTO.getCrnNo().equals("") || !paramDTO.getSsnNo().equals("")) && !paramDTO.getEncryptedSessionKey().equals("")){
			
			// 서버 인증서 정보를 초기화 한다.
			signInit();
			
			boolean chkCertID = true;
			
			// 로그인 사용자 정보와 전달된 정보를 확인한다.
			if(paramDTO.getMemberType().equals(LoginConstants.MEMBER_TYPE_1) || paramDTO.getMemberType().equals(LoginConstants.MEMBER_TYPE_4) || paramDTO.getMemberType().equals(LoginConstants.MEMBER_TYPE_5)){
				// 개인(1), 임의단체(4), 외국인(5)
				
				// 개인의 경우 : 회원정보 주민등록번호 미수집으로 인한 비교문 삭제 - 20140801
				/*
				if (!paramDTO.getSsnNo().equals(logindto.getSsnNo())){
					chkCertID = false;
					setSignCertMessage(logindto.getSsnNo().length()+"주민등록번호 정보가 일치하지 않습니다."+paramDTO.getSsnNo().length());
					return rtnValue;
				}*/
			} else if(paramDTO.getMemberType().equals(LoginConstants.MEMBER_TYPE_2) || paramDTO.getMemberType().equals(LoginConstants.MEMBER_TYPE_3)){
				// 법인사업자(2), 개인사업자(3)
				if (!paramDTO.getCrnNo().equals(logindto.getCrnNo())){
					chkCertID = false;
					setSignCertMessage("사업자번호 정보가 일치하지 않습니다.");
					return rtnValue;
				}
			} else {
				// 디폴트 : 법인사업자(2), 개인사업자(3)
				if (!paramDTO.getCrnNo().equals(logindto.getCrnNo())){
					chkCertID = false;
					setSignCertMessage("사업자번호 정보가 일치하지 않습니다.");
					return rtnValue;
				}
			}
			
			if (chkCertID){
				
				log.info("User 주민등록번호 --------->" + paramDTO.getSsnNo());
				log.info("User 사업자등록번호 --------->" + paramDTO.getCrnNo());
				log.info("User 인증서 추적을 위한 정보 --------->" + paramDTO.getCertId());
				log.info("User 공인인증서 정보 --------->" + paramDTO.getUserSignCert());
				log.info("User 전자서명 값  --------->" + paramDTO.getUserSignValue());
				log.info("User 비밀키 정보 --------->" + paramDTO.getEncryptedSessionKey());
				log.info("User 인증서 소유자 확인을 위한 정보 --------->" + paramDTO.getEncryptedUserRandomNumber());
				log.info("User 주민등록번호 암호화 정보 --------->" + paramDTO.getEncryptedUserSSN());
			
				if(signValidate(paramDTO)){
					
					String strChallenge =  (String) session.getAttribute( SignContants.CHALLENGE ); // 사용자 인증 데이터를 검사할 때 필요한 nonce를 얻는다.
					
					log.info("strChallenge : "+strChallenge );
					log.info("ServerKmKey : "+ServerKmKey );
					log.info("EncryptedSessionKey : "+paramDTO.getEncryptedSessionKey() );
					
					// 암호화 된 정보를 검증한다.
					// PC에서 데이터를 암호화 할 때 사용한 비밀키(세션키)를 서버의 암호화용 개인키를 사용하여 복호화한다.
					byte[] SessionKey = null;
					SessionKey = CipherUtil.decryptRSA( ServerKmKey, SignContants.ServerKeyPassword, paramDTO.getEncryptedSessionKey() );
	
					if ( SessionKey == null ){
						//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"세션키 복호화에 실패하였습니다.");
						setSignCertMessage("세션키 복호화에 실패하였습니다.");
						return rtnValue;
					} else {
	
						// 위에서 구한 비밀키를 사용하여 암호화된 데이터를 복호화한다.
						CipherUtil cipher = new CipherUtil();
						byte[] UserRandomNumber = null;
						byte[] UserSSN = null;
						
						cipher.decryptInit( SessionKey );
						UserRandomNumber	= cipher.decryptUpdate( Base64Util.decode( paramDTO.getEncryptedUserRandomNumber() ));
						UserSSN 		= cipher.decryptUpdate( Base64Util.decode( paramDTO.getEncryptedUserSSN() ));
						cipher.decryptFinal();
						
						log.info("UserRandomNumber : "+UserRandomNumber );
						log.info("UserSSN : "+UserSSN );
						if ( UserRandomNumber == null || UserSSN == null ){
							//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"복호화에 실패하였습니다.");
							setSignCertMessage("복호화에 실패하였습니다.");
							return rtnValue;
						} else {
						
							String strUserRandomNumber = new String( UserRandomNumber );
							String strUserSSN = new String( UserSSN );
							
							// 위에서 복호화하여 얻은 값들에 대해서 전자서명 검사를 수행한다.
							SignUtil sign = new SignUtil();
							String strOriginalMessage = paramDTO.getOriginalMessage();
							
							// BASE64 로 저장된 원본 메시지를 DECODE 하여 SignDTO 에 설정한다.
							
							byte[] decodedBytes = Base64Util.decode( strOriginalMessage );
							strOriginalMessage = new String( decodedBytes );
							
							setOriginalMessage(strOriginalMessage);
							
							log.info("paramDTO.getOriginalMessage : "+paramDTO.getOriginalMessage() );
							log.info("strOriginalMessage : "+strOriginalMessage );
			
							sign.verifyInit( paramDTO.getUserSignCert().getBytes() );
							sign.verifyUpdate( strOriginalMessage.getBytes() );
							
							log.info("UserSignValue : "+paramDTO.getUserSignValue() );
							
							if ( !sign.verifyFinal( Base64Util.decode( paramDTO.getUserSignValue() ) )) {
								//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"전자서명 검사에 실패하였습니다.");
								setSignCertMessage("전자서명 검사에 실패하였습니다.");
								log.info("sign.verifyFinal( Base64Util.decode( paramDTO.getUserSignValue() : false");
								return rtnValue;
							} else {
								setSignValue(paramDTO.getUserSignValue());
								
								// 사용자의 전자서명용 인증서에서 필요한 각종 정보를 구한다.
								CertUtil cert = null;
								
								cert = new CertUtil( paramDTO.getUserSignCert().getBytes() );
								
								// 인증서에서 추출된 정보를 설정
								setCertDN(cert.getSubjectDN());	// 인증서에서 DN 값 추출
								setSerialNumber(cert.getSerialNumber());	// 인증서에서 SerialNumber 값 추출
								setCertNotBefore(cert.getNotBefore());	// 인증서에서 유효성 시작 시점 값 추출
								setCertNotAfter(cert.getNotAfter());		// 인증서에서 유효성 종료 시점 값 추출
								
								// 주민등록번호/사업자등록번호를 이용하여 인증서 소유자를 확인하는 검사를 한다.
								// 사용자 등록 페이지라면 사용자가 입력한 주민등록번호/사업자등록번호를 사용하고,
								// 로그인 페이지라면, DB에 저장된 사용자의 주민등록번호/사업자등록번호를 사용한다.
								log.info("strUserSSN : "+strUserSSN);
								log.info("strUserRandomNumber : "+strUserRandomNumber);
								if ( !cert.isValidUser( strUserSSN, strUserRandomNumber) ) {
									//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"사용자 본인확인 검사에 실패하였습니다");
									setSignCertMessage("사용자 본인확인 검사에 실패하였습니다.");
									log.info("cert.isValidUser( strUserSSN, strUserRandomNumber) : false");
									return rtnValue;
								} else {
									
									// 사용자의 인증서가 서비스에 사용할 수 있는 등급의 인증서인지 검사한다.
									// 예) 특수 목적용 인증서는 일반적인 서비스에 사용할 수 없다. 법인용 서비스를 이용하기 위해서 개인용 인증서를 사용할 수는 없다.
// 테스트를위해 주석처리									
//									if ( !cert.isValidPolicyOid( AllowedPolicyOIDs ) ){
//										//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"사용자 본인확인 검사에 실패하였습니다");
//										setSignCertMessage("허용되지 않는 정책의 인증서입니다.\\n용도제한용 인증서는 전자서명에 사용하실 수 없습니다.\\n범용인증서를 사용하여 주시기 바랍니다.");
//										log.info("cert.isValidPolicyOid( AllowedPolicyOIDs ) : false");
//										return rtnValue;
//									} else {
//										
//										// 사용자가 사용한 인증서가 현재 유효한지 확인한다. --> 폐지되거나 유효기간이 만료된 인증서를 사용하여 생성한 전자서명에는 법적 효력이 없다!!
//										// 인증서가 폐지되었는지 상태 정보는 공인인증기관의 LDAP 서버에서 얻는다. 이를 위하여 WAS에서 6개 공인인증기관의 LDAP서버에 접속 할 수 있어야 한다.!!
//										// 공인인증기관의 LDAP 서버의 포트는 389이다.
//										
//// 테스트를위해 주석처리
//									
//										log.info("CRLCacheDirectory : "+SignContants.CRLCacheDirectory);
///*										
										if ( !cert.isValid( true, SignContants.CRLCacheDirectory ) ){
											//errors.rejectValue(SignContants.SIGN_VALIDATE,SignContants.SIGN_VALIDATE,"사용자 인증서 유효성 검사에 실패하였습니다");
											setSignCertMessage("사용자 인증서 유효성 검사에 실패하였습니다.");
											log.info("cert.isValid( true, SignContants.CRLCacheDirectory ) : false");
											return rtnValue;
										}
//*/								
//									}
								}
							}
						}
					}
				} else {
					return rtnValue;
				}
			}			
			rtnValue = true;
		} else {
			//errors.rejectValue(SignContants.SIGN_VALIDATE,"전자서명 정보가 존재하지 않습니다.");
			setSignCertMessage("전자서명 정보가 존재하지 않습니다.");
			return rtnValue;
		}
		
		setSignCertMessage("정상적인 전자서명 정보입니다.");
		return rtnValue;
	}

	/**
	 * 전자서명 처리결과 반환
	 * 
	 * @return SignCertMessage
	 */
	public String getSignCertMessage() {
		return SignCertMessage;
	}

	/**
	 * 전자서명 처리결과 설정
	 * 
	 * @param signCertMessage the SignCertMessage 
	 */
	public void setSignCertMessage(String signCertMessage) {
		SignCertMessage = signCertMessage;
	}

	/**
	 * 인증서 DN 정보 반환
	 * 
	 * @return CertDN
	 */
	public String getCertDN() {
		return CertDN;
	}

	/**
	 * 인증서 DN 정보 설정
	 * 
	 * @param certDN the CertDN 
	 */
	public void setCertDN(String certDN) {
		CertDN = certDN;
	}

	/**
	 * 인증서 SerialNumber 정보 반환
	 * 
	 * @return SerialNumber
	 */
	public String getSerialNumber() {
		return SerialNumber;
	}

	/**
	 * 인증서 SerialNumber 정보 설정
	 * 
	 * @param serialNumber the SerialNumber 
	 */
	public void setSerialNumber(String serialNumber) {
		SerialNumber = serialNumber;
	}

	/**
	 * 인증서 유효성 시작 시점 반환
	 * 
	 * @return
	 */
	public String getCertNotBefore() {
		return CertNotBefore;
	}

	/**
	 * 인증서 유효성 시작 시점 설정
	 * 
	 * @param certNotBefore
	 */
	public void setCertNotBefore(String certNotBefore) {
		CertNotBefore = certNotBefore;
	}

	/**
	 * 인증서 유효성 종료 시점 반환
	 * 
	 * @return
	 */
	public String getCertNotAfter() {
		return CertNotAfter;
	}

	/**
	 * 인증서 유효성 종료 시점 설정
	 * 
	 * @param certNotAfter
	 */
	public void setCertNotAfter(String certNotAfter) {
		CertNotAfter = certNotAfter;
	}

	/**
	 * 전자서명 원본 값 반환
	 * 
	 * @return OriginalMessage
	 */
	public String getOriginalMessage() {
		return OriginalMessage;
	}

	/**
	 * 전자서명 원본 값 설정
	 * 
	 * @param originalMessage
	 */
	public void setOriginalMessage(String originalMessage) {
		this.OriginalMessage = originalMessage;
	}

	/**
	 * 전자서명 값 반환
	 * 
	 * @return SignValue
	 */
	public String getSignValue() {
		return SignValue;
	}

	/**
	 * 전자서명 값 반환
	 * 
	 * @param signValue
	 */
	public void setSignValue(String signValue) {
		this.SignValue = signValue;
	}

}
