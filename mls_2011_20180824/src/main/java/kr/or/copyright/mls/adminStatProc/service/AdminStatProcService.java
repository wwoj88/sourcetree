package kr.or.copyright.mls.adminStatProc.service;

public interface AdminStatProcService {
	
	// 상당한노력 진행현황
	public void statProcList() throws Exception;
		
	// 대상저작물 수집
	public void getStatProcOrd() throws Exception;

	// 상당한노력 진행
	public void goStatProc() throws Exception;
	public void goStatProcV10() throws Exception;
	
	// 법정허락 대상저작물 수집
	public void getStatWorksOrd() throws Exception;
	
	// 법정허락 대상 전환
	public void goChangeStatWorks() throws Exception;
	
	//  스케쥴러 실행
	public void statProcScheduling()throws Exception;
	public void statWorksScheduling() throws Exception;
	
	//미분배보상금저작물 보고현황
	public void inmtReptView() throws Exception;
	
	//거소불명저작물 신청현황
	public void nonWorksReptView() throws Exception;
	
	// 상당한노력 대상목록
	public void statProcTargPopup() throws Exception;
	
	// 상당한노력 진행현황
	public void statProcPopup() throws Exception;
	
	// 법정허락 대상 전환
	public void statProcWorksPopup() throws Exception;
	
	public void worksEntReptView() throws Exception;
	
	public void reptMgntList() throws Exception;
	
	public void reptMgntUpdate() throws Exception;
	
	// 상당한노력 매칭 정보
	public void statProcInfo() throws Exception;
	
/*	//신청 담당자 메일수신관리
	public void selectMailMgntList() throws Exception;
	
	public void isCheckMailMgntList() throws Exception;*/
	
	
}
