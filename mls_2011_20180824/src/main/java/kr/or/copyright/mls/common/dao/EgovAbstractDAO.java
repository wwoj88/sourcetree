/*
 * Copyright 2008-2009 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kr.or.copyright.mls.common.dao;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.ibatis.sqlmap.client.SqlMapClient;


public class EgovAbstractDAO extends SqlMapClientDaoSupport {


    protected EgovAbstractDAO() {
        // PMD abstract Rule - If the class is intended
        // to be used as a base class only (not to be
        // instantiated
        // directly)
        // a protected constructor can be provided
        // prevent direct instantiation
    }


    @Resource(name = "sqlMapClient")
    public void setSuperSqlMapClient(SqlMapClient sqlMapClient) {
        super.setSqlMapClient(sqlMapClient);
    }


    public Object insert(String queryId, Object parameterObject) {
        return getSqlMapClientTemplate().insert(queryId, parameterObject);
    }

 
    public int update(String queryId, Object parameterObject) {
        return getSqlMapClientTemplate().update(queryId, parameterObject);
    }


    public int delete(String queryId, Object parameterObject) {
        return getSqlMapClientTemplate().delete(queryId, parameterObject);
    }


    public Object selectByPk(String queryId, Object parameterObject) {
        return getSqlMapClientTemplate().queryForObject(queryId,
            parameterObject);
    }

    @SuppressWarnings("unchecked")
    public List list(String queryId, Object parameterObject) {
        return getSqlMapClientTemplate().queryForList(queryId, parameterObject);
    }


    @SuppressWarnings("unchecked")
    public List listWithPaging(String queryId, Object parameterObject,
            int pageIndex, int pageSize) {
        int skipResults = pageIndex * pageSize;
        int maxResults = (pageIndex * pageSize) + pageSize;
        return getSqlMapClientTemplate().queryForList(queryId, parameterObject,
            skipResults, maxResults);
    }

}
