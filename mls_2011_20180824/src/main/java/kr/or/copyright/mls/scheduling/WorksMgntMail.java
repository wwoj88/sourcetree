package kr.or.copyright.mls.scheduling;

import java.text.SimpleDateFormat;

import kr.or.copyright.mls.adminEmailMgnt.service.AdminEmailMgntService;
import kr.or.copyright.mls.adminEvnSet.service.AdminEvnSetService;
import kr.or.copyright.mls.adminStatProc.service.AdminStatProcService;

import org.springframework.stereotype.Controller;

@Controller
public class WorksMgntMail {
	

	@javax.annotation.Resource(name = "AdminEvnSetService")
	private AdminEvnSetService adminEvnSetService;
	
	public void execute() throws Exception{
		
		long time = System.currentTimeMillis();         
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");         
		System.out.println("Cron trigger statProcBatch (59 second): current time = " + sdf.format(time));    
		
		adminEvnSetService.WorksMgntMailScheduling();
	}

}
