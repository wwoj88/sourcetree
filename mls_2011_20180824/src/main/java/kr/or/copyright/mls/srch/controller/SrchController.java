package kr.or.copyright.mls.srch.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionListener;
import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.srch.model.SrchCert;
import kr.or.copyright.mls.srch.model.SrchComm;
import kr.or.copyright.mls.srch.model.SrchCros;
import kr.or.copyright.mls.srch.model.SrchStat;
import kr.or.copyright.mls.srch.model.SrchStatDTO;
import kr.or.copyright.mls.srch.service.SrchService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class SrchController {

     private Logger log = Logger.getLogger(this.getClass());

     @javax.annotation.Resource(name = "SrchService")
     private SrchService SrchService;


     @RequestMapping(value = "/srchList/codeList.do")
     public ModelAndView codeList(HttpServletRequest request) throws Exception {

          String srchDivs = ServletRequestUtils.getStringParameter(request, "srchDivs");
          List<SrchStat> codeList = SrchService.codeList(srchDivs);

          ModelAndView mav = new ModelAndView("jsonReport");
          mav.addObject("codeList", codeList);
          return mav;
     }

     @RequestMapping("srchList.do")
     public ModelAndView srchList(HttpServletRequest request, HttpServletResponse response) throws Exception {

          // 20170905 검색 가중치 생성
          SrchStatDTO srchStatDTO = new SrchStatDTO();
          // srchStatDTO.setGenre_cd( Integer.parseInt( ServletRequestUtils.getStringParameter(request,
          // "genreCd", "0") ) );
          srchStatDTO.setSrch_word(ServletRequestUtils.getStringParameter(request, "worksTitle", ""));
          /* SrchService.insertSrch( worksTitle ); */
          String menuFlag = ServletRequestUtils.getStringParameter(request, "menuFlag", "");
          request.setAttribute("menuFlag", menuFlag);
          String genreCd = ServletRequestUtils.getStringParameter(request, "genreCd", "0"); // 장르 값
          request.setAttribute("genreCd", genreCd);

          String gubunVal = ServletRequestUtils.getStringParameter(request, "gubunVal", null);// 검색인지 아닌지 구분코드
          String worksTitle = ServletRequestUtils.getStringParameter(request, "worksTitle", ""); // 제목 검색 값

          String srchDivs = ServletRequestUtils.getStringParameter(request, "srchDivs", "00");
          int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
          int from = Constants.DEFAULT_ROW_PER_PAGE * (pageNo - 1) + 1;
          int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
          String more = ServletRequestUtils.getStringParameter(request, "more", "N"); // 더보기 값
          String srchDiv = ""; // 장르 big_code 담을 String
          int statCount = 0; // 각 게시판 Count
          int commCount = 0;
          int crosCount = 0;
          if (srchDivs.equals("01")) {
               srchDiv = "35";
          } else if (srchDivs.equals("02")) {
               srchDiv = "79";
          } else if (srchDivs.equals("03")) {
               srchDiv = "72";
          }
          List<SrchStat> codeList = SrchService.codeList(srchDiv); // 장르 코드
          System.out.println("srchDivs :::::" + srchDivs);
          System.out.println("srchDiv :::::" + srchDiv);
          System.out.println("codeList :::::" + codeList);
          String urlCd = ServletRequestUtils.getStringParameter(request, "urlCd", "01");// 검색엔진(01),DB검색(02)
          // urlCd = "02";
          ModelAndView mv = new ModelAndView();
          if (urlCd.equals("02")) {
               mv.setViewName("srch/srchListDB");
          } else {
               mv.setViewName("srch/srchList");
          }

          System.out.println("----------04----------");
          List<SrchStat> srch = new ArrayList<SrchStat>();// 각 게시판별 리스트
          List<SrchComm> comm = new ArrayList<SrchComm>();
          List<SrchCros> cros = new ArrayList<SrchCros>();
          System.out.println("genreCd : " + genreCd);
          System.out.println("gubunVal:" + gubunVal);
          System.out.println("srchDivs:" + srchDivs);
          System.out.println("----------05:" + gubunVal + ":----------");
          if (gubunVal != null) {
               if (gubunVal.equals("F")) { // F = 검색 Key
                    if (srchDivs.equals("01")) { // 01 = 법정허락 검색
                         System.out.println("srchDivs == 01");
                         HashMap<Object, Object> params = new HashMap<Object, Object>();
                         params.put("worksTitle", worksTitle);
                         params.put("more", more);
                         params.put("genreCd", genreCd);
                         params.put("srchDivs", srchDivs);
                         params.put("startNo", from);
                         params.put("endNo", to);
                         List<SrchStat> findStatWorks = SrchService.findStatWorks(params);
                         statCount = SrchService.countStatWorks(params);

                         for (int i = 0; i < findStatWorks.size(); i++) {
                              params.put("worksId", findStatWorks.get(i).getWorksId());
                              params.put("worksDivsCd", findStatWorks.get(i).getWorksDivsCd());
                              List<SrchStat> findStatDetl = SrchService.findStatDetl(params);

                              srch.addAll(findStatDetl);
                              try {
                                   if (findStatWorks.get(i).getWorksDivsCd() == 4) {
                                        List<SrchStat> coptHodr = SrchService.getCoptHodr(params);
                                        if (coptHodr.size() != 0) {
                                             String copt = "";
                                             for (int ii = 0; ii < coptHodr.size(); ii++) {
                                                  copt += "," + coptHodr.get(ii).getCoptHodr();
                                             }
                                             copt = copt.substring(1, copt.length());
                                             srch.get(i).setCoptHodr(copt);
                                        }
                                   } // if end
                              } catch (IndexOutOfBoundsException e) {
                                   e.printStackTrace();
                              }
                         } // for end
                    } else if (srchDivs.equals("02")) {// 02 = 저작권등록부
                         System.out.println("srchDivs == 02");
                         HashMap<Object, Object> params = new HashMap<Object, Object>();
                         params.put("contTitle", worksTitle);
                         params.put("more", more);
                         params.put("genreCd", genreCd);
                         params.put("startNo", from);
                         params.put("endNo", to);
                         cros = SrchService.findCrosWorks(params);
                         crosCount = SrchService.countCrosWorks(params);
                    } else if (srchDivs.equals("03")) {// 03 = 위탁관리
                         System.out.println("srchDivs == 03");
                         HashMap<Object, Object> params = new HashMap<Object, Object>();
                         params.put("worksTitle", worksTitle);
                         params.put("more", more);
                         params.put("genreCd", genreCd);
                         params.put("startNo", from);
                         params.put("endNo", to);
                         List<SrchComm> findCommWorks = SrchService.findCommWorks(params);
                         commCount = SrchService.countCommWorks(params);
                         for (int i = 0; i < findCommWorks.size(); i++) {
                              params.put("worksId", findCommWorks.get(i).getWorksId());
                              params.put("genreCd", findCommWorks.get(i).getGenreCd());
                              List<SrchComm> findCommDetl = SrchService.findCommDetl(params);
                              String genre = findCommWorks.get(i).getGenreCd();
                              if (genre.equals("3")) {
                                   findCommDetl.get(0).setBordDate(StringDateFormat(findCommDetl.get(0).getBordDate()));
                              } else if (genre.equals("6")) {
                                   findCommDetl.get(0).setArticlDate(StringDateFormat(findCommDetl.get(0).getArticlDate()));
                              } else if (genre.equals("7")) {
                                   findCommDetl.get(0).setMakeDate(StringDateFormat(findCommDetl.get(0).getMakeDate()));
                                   findCommDetl.get(0).setPossDate(StringDateFormat(findCommDetl.get(0).getPossDate()));
                              } else if (genre.equals("99")) {
                                   findCommDetl.get(0).setMakeDate(StringDateFormat(findCommDetl.get(0).getMakeDate()));
                                   findCommDetl.get(0).setPublDate(StringDateFormat(findCommDetl.get(0).getPublDate()));
                              }
                              comm.addAll(findCommDetl);
                         }

                    } else if (srchDivs.equals("00") || srchDivs == null || "".equals(srchDivs)) {
                         // 통합검색
                         /*
                          * }else if(srchDivs.equals("00")){ //통합검색 원문
                          */ HashMap<Object, Object> params = new HashMap<Object, Object>();
                         System.out.println("srchDivs == null");
                         System.out.println("worksTitle : " + worksTitle);
                         System.out.println("more : " + more);
                         System.out.println("genreCd : " + genreCd);
                         System.out.println("srchDivs : " + srchDivs);
                         System.out.println("startNo : " + from);
                         System.out.println("endNo : " + to);
                         params.put("worksTitle", worksTitle);
                         params.put("more", more);
                         params.put("genreCd", genreCd);
                         params.put("srchDivs", srchDivs);
                         params.put("startNo", from);
                         params.put("endNo", to);
                         List<SrchStat> findStatWorks = SrchService.findStatWorks(params);
                         System.out.println(findStatWorks);
                         statCount = SrchService.countStatWorks(params);
                         for (int i = 0; i < findStatWorks.size(); i++) {
                              params.put("worksId", findStatWorks.get(i).getWorksId());
                              params.put("worksDivsCd", findStatWorks.get(i).getWorksDivsCd());
                              List<SrchStat> findStatDetl = SrchService.findStatDetl(params);
                              srch.addAll(findStatDetl);
                              try {
                                   if (findStatWorks.get(i).getWorksDivsCd() == 4) {
                                        List<SrchStat> coptHodr = SrchService.getCoptHodr(params);
                                        if (coptHodr.size() != 0) {
                                             String copt = "";
                                             for (int ii = 0; ii < coptHodr.size(); ii++) {
                                                  copt += "," + coptHodr.get(ii).getCoptHodr();
                                             }
                                             copt = copt.substring(1, copt.length());
                                             srch.get(i).setCoptHodr(copt);
                                        }
                                   } // if end
                              } catch (IndexOutOfBoundsException e) {
                                   e.printStackTrace();
                              }
                         } // for end
                         List<SrchComm> findCommWorks = SrchService.findCommWorks(params);
                         commCount = SrchService.countCommWorks(params);
                         for (int ii = 0; ii < findCommWorks.size(); ii++) {
                              params.put("worksId", findCommWorks.get(ii).getWorksId());
                              params.put("genreCd", findCommWorks.get(ii).getGenreCd());

                              /* log.info("params  = " + findCommWorks.get(ii).getGenreCd()); */
                              List<SrchComm> findCommDetl = SrchService.findCommDetl(params);
                              String genre = findCommWorks.get(ii).getGenreCd();
                              if (genre.equals("3")) {
                                   findCommDetl.get(0).setBordDate(StringDateFormat(findCommDetl.get(0).getBordDate()));
                              } else if (genre.equals("6")) {
                                   findCommDetl.get(0).setArticlDate(StringDateFormat(findCommDetl.get(0).getArticlDate()));
                              } else if (genre.equals("7")) {
                                   findCommDetl.get(0).setMakeDate(StringDateFormat(findCommDetl.get(0).getMakeDate()));
                                   findCommDetl.get(0).setPossDate(StringDateFormat(findCommDetl.get(0).getPossDate()));
                              } else if (genre.equals("99")) {
                                   findCommDetl.get(0).setMakeDate(StringDateFormat(findCommDetl.get(0).getMakeDate()));
                                   findCommDetl.get(0).setPublDate(StringDateFormat(findCommDetl.get(0).getPublDate()));
                              }
                              comm.addAll(findCommDetl);
                         } // for end
                         HashMap<Object, Object> param = new HashMap<Object, Object>();
                         param.put("contTitle", worksTitle);
                         param.put("more", more);
                         param.put("genreCd", genreCd);
                         param.put("startNo", from);
                         param.put("endNo", to);
                         cros = SrchService.findCrosWorks(param);
                         crosCount = SrchService.countCrosWorks(param);
                    } else {// 검색 if end
                         System.out.println("srchDivs == end");
                    }
               }
               if (srchDivs.equals("01")) {
                    mv.addObject("srch", srch);
               } else if (srchDivs.equals("02")) {
                    mv.addObject("cros", cros);
               } else if (srchDivs.equals("03")) {
                    mv.addObject("comm", comm);
               } else if (srchDivs.equals("00")) {
                    mv.addObject("srch", srch);
                    mv.addObject("comm", comm);
                    mv.addObject("cros", cros);
               }
               if (null != worksTitle && !"".equals(worksTitle.trim())) {
                    SrchService.insertSrch(worksTitle);
               }
               mv.addObject("srchSize", notation(statCount));
               mv.addObject("commSize", notation(commCount));
               mv.addObject("crosSize", notation(crosCount));
               mv.addObject("srchCount", statCount);
               mv.addObject("commCount", commCount);
               mv.addObject("crosCount", crosCount);
               mv.addObject("worksTitle", worksTitle); // 검색값 물고가는 OBJECT들
               mv.addObject("genreCd", genreCd);
               mv.addObject("srchDivs", srchDivs);
               mv.addObject("pageNo", pageNo);
               mv.addObject("more", more);
               mv.addObject("gubunVal", gubunVal);

          }
          mv.addObject("codeList", codeList);
          return mv;
     }

     public static String notation(double d) {

          return NumberFormat.getInstance().format(d);
     }

     public static String StringDateFormat(String dateStr) {

          if (dateStr.equals("없음") || dateStr.equals(" ") || dateStr.length() < 8)
               return dateStr;


          String formatDateStr = "";

          formatDateStr += dateStr.substring(0, 4) + '.';
          formatDateStr += dateStr.substring(4, 6) + '.';
          formatDateStr += dateStr.substring(6, 8) + ' ';

          return formatDateStr;
     }

     @RequestMapping("certList.do")
     public ModelAndView certList(HttpServletRequest request, HttpServletResponse response) throws Exception {

          String gubunVal = ServletRequestUtils.getStringParameter(request, "gubunVal", null);// 검색인지 아닌지 구분코드
          String worksTitle = ServletRequestUtils.getStringParameter(request, "worksTitle"); // 제목 검색 값
          String genreCdName = ServletRequestUtils.getStringParameter(request, "genreCd", "total"); // 장르 값
          String srchDivs = ServletRequestUtils.getStringParameter(request, "srchDivs", "0");
          String resrch = ServletRequestUtils.getStringParameter(request, "resrch", "no");
          int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
          int from = Constants.DEFAULT_ROW_PER_PAGE * (pageNo - 1) + 1;
          int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
          String mode = ServletRequestUtils.getStringParameter(request, "mode", "N");
          Map params = new HashMap();

          params.put("worksTitle", worksTitle);
          params.put("startNo", from);
          params.put("endNo", to);
          params.put("genreCdName", genreCdName);
          params.put("mode", mode);
          // srchCert.setGenreCdName(genreCdName);
          // params.put("more", "CE");
          // System.out.println(worksTitle);
          // System.out.println(from);
          // System.out.println(to);
          List<SrchCert> certList = SrchService.findCertWorks(params);
          int countCert = SrchService.countFindCertWorks(params);

          ModelAndView mv = new ModelAndView("srch/cert_result");
          mv.addObject("certList", certList);
          mv.addObject("worksTitle", worksTitle);
          mv.addObject("pageNo", pageNo);
          mv.addObject("mode", mode);
          mv.addObject("genreCdName", genreCdName);
          mv.addObject("srchDivs", srchDivs);
          mv.addObject("gubunVal", gubunVal);
          mv.addObject("countCert", countCert);

          return mv;
     }
}
