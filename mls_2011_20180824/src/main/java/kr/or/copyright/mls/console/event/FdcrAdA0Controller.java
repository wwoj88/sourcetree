package kr.or.copyright.mls.console.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 이벤트 등록 및 관리 > 이벤트 등록
 * 
 * @author ljh
 */
@Controller
public class FdcrAdA0Controller extends DefaultController{

	@Resource( name = "fdcrAdA0Service" )
	private FdcrAdA0ServiceImpl fdcrAdA0Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA0Controller.class );

	@RequestMapping( value = "/console/event/fdcrAdA0WriteForm1.page" )
	public String fdcrAdA0WriteForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		
		return "/event/fdcrAdA0WriteForm1.tiles";
	}
	/**
	 * 이벤트 등록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA0Insert1.page" )
	public String fdcrAdA0Insert1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		commandMap.put( "OPER_YN", EgovWebUtil.getString( commandMap, "OPER_YN" ) );
		commandMap.put( "OPEN_YN_PROP", EgovWebUtil.getString( commandMap, "OPEN_YN_PROP" ) );
		commandMap.put( "OPEN_YN_DTTM_PART", EgovWebUtil.getString( commandMap, "OPEN_YN_DTTM_PART" ) );
		commandMap.put( "OPEN_YN_WIN_ANUC_DATE", EgovWebUtil.getString( commandMap, "OPEN_YN_WIN_ANUC_DATE" ) );
		commandMap.put( "OPEN_YN_WIN_ANUC_DESC", EgovWebUtil.getString( commandMap, "OPEN_YN_WIN_ANUC_DESC" ) );
		commandMap.put( "OPEN_YN_WIN_CNT", EgovWebUtil.getString( commandMap, "OPEN_YN_WIN_CNT" ) );
		commandMap.put( "OPEN_YN_IMAGE", EgovWebUtil.getString( commandMap, "OPEN_YN_IMAGE" ) );
		commandMap.put( "PART_DUPL_YN", EgovWebUtil.getString( commandMap, "PART_DUPL_YN" ) );
		
		
		ArrayList<Map<String,Object>> agreeList = new ArrayList<Map<String,Object>>();
		String AGREEYN_1 = EgovWebUtil.getString( commandMap, "AGREE_ITEM_CD_1" );
		String ITEM_DESC_1 = EgovWebUtil.getString( commandMap, "ITEM_DESC_1" );
		agreeList.add( getAgreeMap("1", AGREEYN_1, ITEM_DESC_1) );

		String AGREEYN_2 = EgovWebUtil.getString( commandMap, "AGREE_ITEM_CD_2" );
		String ITEM_DESC_2 = EgovWebUtil.getString( commandMap, "ITEM_DESC_2" );
		agreeList.add( getAgreeMap("2", AGREEYN_2, ITEM_DESC_2) );
		
		String AGREEYN_99 = EgovWebUtil.getString( commandMap, "AGREE_ITEM_CD_99" );
		String ITEM_DESC_99 = EgovWebUtil.getString( commandMap, "ITEM_DESC_99" );
		agreeList.add( getAgreeMap("99", AGREEYN_99, ITEM_DESC_99) );
		
		commandMap.put( "agreeList", agreeList );

		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAdA0Service.fdcrAdA0Insert1( commandMap, fileinfo );
		if( isSuccess ){
			return returnUrl(
				model,
				"저장했습니다.",
				"/console/event/fdcrAdA0WriteForm1.page");
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/event/fdcrAdA0WriteForm1.page");
		}
	}
	
	public Map<String,Object> getAgreeMap(String AGREE_ITEM_CD, String AGREEYN, String ITEM_DESC){
		String ITEM_TITLE = "";
		if(null!=AGREE_ITEM_CD){
			if("1".equals(AGREE_ITEM_CD)){
				ITEM_TITLE = "개인정보 수집·이용에 대한 동의";
			}else if("2".equals(AGREE_ITEM_CD)){
				ITEM_TITLE = "개인정보 취급위탁에 대한 동의";
			}else if("99".equals(AGREE_ITEM_CD)){
				ITEM_TITLE = "기타";
			}
		}
		Map<String,Object> agreeMap = new HashMap<String,Object>();
		agreeMap.put( "AGREE_ITEM_CD", AGREE_ITEM_CD );
		agreeMap.put( "AGREEYN", AGREEYN );
		agreeMap.put( "ITEM_TITLE", ITEM_TITLE );
		agreeMap.put( "ITEM_DESC", ITEM_DESC );
		return agreeMap;
	}

}
