package kr.or.copyright.mls.search.service;

/**
 * 
 * <pre>
 * <p> Title: WNCollection </p>
 * <p> Description: 검색환경 설정 상수</p>
 * </pre>
 *
 * @author WISEnut
 * @created: 2014. 5. 27.
 * @modified:
 * @version
 *
 */
public class WNConstants_20181022_new {
	public final static int CONNECTION_TIMEOUT = 20000;
	public final static String CHARSET = "UTF-8";
	public final static int REALTIME_COUNT=100;
	public final static int PAGE_SCALE = 10; //view page list count

	public final static int CONNECTION_KEEP = 0; //recevive mode
	public final static int CONNECTION_REUSE = 2;
	public final static int CONNECTION_CLOSE = 3;

	public final static int ASC = 0; //order
	public final static int DESC = 1; //order

	public final static int USE_KMA_OFFOFF = 0; //synonym, morpheme
	public final static int USE_KMA_ONON = 1;
	public final static int USE_KMA_ONOFF = 2;

	public final static int USE_RESULT_STRING = 0; //result data type	
	public final static int USE_RESULT_XML = 1;
	public final static int USE_RESULT_JSON = 2;
	public final static int USE_RESULT_DUPLICATE_STRING = 3; //uid result data type	
	public final static int USE_RESULT_DUPLICATE_XML = 4;
	public final static int USE_RESULT_DUPLICATE_JSON = 5;

	public final static int IS_CASE_ON = 1; //case on, off
	public final static int IS_CASE_OFF = 0;

	public final static int HI_SUM_OFFOFF = 0; //summarizing, highlighting
	public final static int HI_SUM_OFFON = 1;
	public final static int HI_SUM_ONOFF = 2;
	public final static int HI_SUM_ONON = 3;

	public final static int COMMON_OR_WHEN_NORESULT_OFF = 0;
	public final static int COMMON_OR_WHEN_NORESULT_ON = 1;

	public final static int INDEX_NAME = 0;
	public final static int COLLECTION_NAME = 1;
	public final static int PAGE_INFO = 2;
	public final static int ANALYZER = 3;
	public final static int SORT_FIELD = 4;
	public final static int RANKING_OPTION = 5;
	public final static int SEARCH_FIELD = 6;
	public final static int RESULT_FIELD = 7;
	public final static int DATE_RANGE = 8;
	public final static int RANK_RANGE = 9;
	public final static int EXQUERY_FIELD = 10;
	public final static int COLLECTION_QUERY =11;
	public final static int BOOST_QUERY =12;
	public final static int FILTER_OPERATION = 13;
	public final static int GROUP_BY = 14;
	public final static int GROUP_SORT_FIELD = 15;
	public final static int CATEGORY_BOOST = 16;
	public final static int CATEGORY_GROUPBY = 17;
	public final static int CATEGORY_QUERY = 18;
	public final static int PROPERTY_GROUP = 19;
	public final static int PREFIX_FIELD = 20;
	public final static int FAST_ACCESS = 21;
	public final static int MULTI_GROUP_BY = 22;
	public final static int AUTH_QUERY = 23;
	public final static int DEDUP_SORT_FIELD = 24;
	public final static int COLLECTION_KOR = 25;	
	public final static int HIGHLIGHT_FIELD = 26;	

	public final static int MERGE_COLLECTION_NAME = 0;
	public final static int MERGE_MAPPING_COLLECTION_NAME = 1;
	public final static int MERGE_PAGE_INFO = 2;
	public final static int MERGE_RESULT_FIELD = 3;
	public final static int MERGE_MAPPING_RESULT_FIELD = 4;
	public final static int MERGE_MULTI_GROUP_BY_FIELD = 5;
	public final static int MERGE_MAPPING_MULTI_GROUP_BY_FIELD = 6;
	public final static int MERGE_CATEGORY_GROUPBY_FIELD = 7;
	public final static int MERGE_MAPPING_CATEGORY_GROUPBY_FIELD = 8;

	//가상 통합 컬렉션을 사용하지 않을 경우 아래와 같이MERGE_COLLECTIONS에 정의한다.
	static String[] MERGE_COLLECTIONS = new String[]{""};

	//가상 통합 컬렉션을 사용할 경우 아래와 같이MERGE_COLLECTIONS에 정의한다.
	//public String[] MERGE_COLLECTIONS = new String[]{"merge_sample_bbs"}; 
	/*
public class WNCollection {

	public String[][] MERGE_COLLECTION_INFO = null;

	WNCollection(){

		//가상 통합 컬렉션을 사용할 경우, mapping되는 collection들의 정보를 정의한다.
		MERGE_COLLECTION_INFO = new String[][]
		{
			{
				"merge_sample_bbs", // set merge collection name
				"sample_bbs/sample_edu", // set collection name, delimiter: /
				"0,3",  // set merge collection pageinfo (start,count)
				"DOCID,TITLE,WRITER,CONTENT",// set merge document field
				"DOCID,TITLE,WRITER,CONTENT/DOCID,TITLE,WRITER,CONTENT", // set document field, delimiter: /
				"", // set merge collection multi-group-by field
				"", // set merge collection multi-group-by field, delimiter: /
				"", // set merge collection category-group-by field
				""  // set collection multi-group-by field, delimiter: /
			}					
		};
	 */

	static String SEARCH_IP="222.231.43.119";
	static int SEARCH_PORT=7001;
	static String MANAGER_IP="222.231.43.119";
	static int MANAGER_PORT=7801;

	public final static String[] COLLECTIONS = new String[]{"cf_art","cf_broadcast","cf_etc","cf_image","cf_literature","cf_movie","cf_music","cf_news","cf_photo","cf_reg","cf_scenario","reg_copyright"};
	public final static String[] COLLECTIONS_NAME = new String[]{"cf_art","cf_broadcast","cf_etc","cf_image","cf_literature","cf_movie","cf_music","cf_news","cf_photo","cf_reg","cf_scenario","reg_copyright"};
	//public String[] MERGE_COLLECTIONS = new String[]{""};
	public class WNCollection{
	public String[][] MERGE_COLLECTION_INFO = null;
	public String[][] COLLECTION_INFO = null;
		WNCollection(){
			COLLECTION_INFO = new String[][]
			{
			{
			"cf_art", // set index name
			"cf_art", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,GENRE_CD,SOURCE_INFO,MAIN_MTRL,TXTR,WRITER,POSS_ORGN_NAME,POSS_DATE,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,IMAGE_URL,IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,GENRE_CD,MAKE_DATE,SOURCE_INFO,MAIN_MTRL,TXTR,WRITER,POSS_ORGN_NAME,POSS_DATE,RGST_ORGN_NAME,WORKS_ID,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_art" // collection display name
			}
         ,
			{
			"cf_broadcast", // set index name
			"cf_broadcast", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,BORD_GENRE_CD,BROD_ORD_SEQ,DIRECTOR,MAKE_CPY,WRITER",// set search field
			"DOCID,DATE,WORKS_TITLE,BORD_GENRE_CD,MAKE_YEAR,BROD_ORD_SEQ,DIRECTOR,MAKE_CPY,WRITER,CRT_YEAR,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_broadcast" // collection display name
			}
         ,
			{
			"cf_etc", // set index name
			"cf_etc", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,SIDE_GENRE_CD,PUBL_MEDI,COPT_NAME,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,SIDE_GENRE_CD,MAKE_DATE,PUBL_MEDI,PUBL_DATE,COPT_NAME,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_etc" // collection display name
			}
         ,
			{
			"cf_image", // set index name
			"cf_image", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,WRITER,KEYWORD,PRODUCER,IMAGE_DESC,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,IMAGE_URL,IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,WRITER,KEYWORD,PRODUCER,IMAGE_DESC,RGST_ORGN_NAME,WORKS_ID,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_image" // collection display name
			}
         ,
			{
			"cf_literature", // set index name
			"cf_literature", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,WORKS_SUB_TITLE,BOOK_TITLE,BOOK_PUBLISHER,COPT_NAME,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,WORKS_SUB_TITLE,BOOK_TITLE,BOOK_ISSU_YEAR,BOOK_PUBLISHER,COPT_NAME,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_literature" // collection display name
			}
         ,
			{
			"cf_movie", // set index name
			"cf_movie", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,PLAYER,DIRECT,WRITER,DIRECTOR,PRODUCER,DISTRIBUTOR,INVESTOR,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,PLAYER,DIRECT,WRITER,DIRECTOR,PRODUCER,DISTRIBUTOR,INVESTOR,CRT_YEAR,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_movie" // collection display name
			}
         ,
			{
			"cf_music", // set index name
			"cf_music", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,ALBUM_TITLE,LYRC,COMP,ARRA,TRAN,SING,PERF,PROD,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,ALBUM_TITLE,LYRC,COMP,ARRA,TRAN,SING,PERF,PROD,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_music" // collection display name
			}
         ,
			{
			"cf_news", // set index name
			"cf_news", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,PAPE_NO,PAPE_KIND,CONTRIBUTOR,REPOTER,PROVIDER,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,ARTICL_DATE,PAPE_NO,PAPE_KIND,CONTRIBUTOR,REPOTER,PROVIDER,RGST_ORGN_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_news" // collection display name
			}
         ,
			{
			"cf_photo", // set index name
			"cf_photo", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,WRITER,KEYWORD,PRODUCER,IMAGE_DESC,RGST_ORGN_NAME",// set search field
			"DOCID,DATE,IMAGE_URL,IMAGE_THUMBNAIL_NAME,IMAGE_NAME,WORKS_TITLE,WORKS_SUB_TITLE,WRITER,KEYWORD,PRODUCER,IMAGE_DESC,RGST_ORGN_NAME,WORKS_ID,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_photo" // collection display name
			}
         ,
			{
			"cf_reg", // set index name
			"cf_reg", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"WORKS_TITLE,REG_COPT_HODR_NAME,REG_REASON,AUTHOR_NAME",// set search field
			"DOCID,DATE,WORKS_TITLE,REG_COPT_HODR_NAME,REG_REASON,AUTHOR_NAME,REG_ID,REG_DATE,REG_PART1_NAME,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_reg" // collection display name
			}
         ,
			{
			"cf_scenario", // set index name
			"cf_scenario", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"BORD_DATE,WORKS_TITLE,BROD_ORD_SEQ,PLAYER,MAKE_CPY,WRITER,DIRECTOR,RGST_ORGN_NAME,SCRT_GENRE_CD,SCRP_SUBJ_CD,ORIG_WRITER,ALIAS",// set search field
			"DOCID,DATE,WORKS_TITLE,WORKS_ORIG_TITLE,BORD_DATE,BROD_ORD_SEQ,PLAYER,MAKE_CPY,WRITER,DIRECTOR,RGST_ORGN_NAME,SCRT_GENRE_CD,SCRP_SUBJ_CD,ORIG_WRITER,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"cf_scenario" // collection display name
			}
         ,
			{
			"reg_copyright", // set index name
			"reg_copyright", // set collection name
			"0,3",  // set pageinfo (start,count)
			"1,0,0,0,0", // set query analyzer (useKMA,isCase,useOriginal,useSynonym, duplcated detection)
			"RANK/DESC,DATE/DESC",  // set sort field (field,order) multi sort '/'
			"basic,rpfmo,100",  // set sort field (field,order) multi sort '/'
			"reg_kind,reg_class,author_name,cont_title,cont_class,reg_id,pjt_id,cont_text,reg_part1,disposal_name,grant_name,trans_scope",// set search field
			"DOCID,DATE,reg_order,reg_id,reg_class,reg_part1,reg_date,reg_reason,chg_type,chg_type_desc,cont_title,author_name,reg_kind,cont_text,cont_class,cont_class_name,cont_class_etc,pjt_id,trans_scope,disposal_name,grant_name,tablecode,isopenyn,ALIAS",// set document field
			"", // set date range
			"", // set rank range
			"", // set prefix query, example: <fieldname:contains:value1>|<fieldname:contains:value2>/1,  (fieldname:contains:value) and ' ', or '|', not '!' / operator (AND:1, OR:0)
			"", // set collection query (<fieldname:contains:value^weight | value^weight>/option...) and ' ', or '|'
			"", // set boost query (<fieldname:contains:value> | <field3:contains:value>...) and ' ', or '|'
			"", // set filter operation (<fieldname:operator:value>)
			"", // set groupby field(field, count)
			"", // set sort field group(field/order,field/order,...)
			"", // set categoryBoost(fieldname,matchType,boostID,boostKeyword)
			"", // set categoryGroupBy (fieldname:value)
			"", // set categoryQuery (fieldname:value)
			"", // set property group (fieldname,min,max, groupcount)
			"ALIAS", // use check prefix query filed
			"", // set use check fast access field
			"", // set multigroupby field
			"", // set auth query (Auth Target Field, Auth Collection, Auth Reference Field, Authority Query)
			"", // set Duplicate Detection Criterion Field, RANK/DESC,DATE/DESC
			"reg_copyright" // collection display name
			}
			};
		}
	}
}
