package kr.or.copyright.mls.console.setup;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 설정 > 신청 담당자/담당자 메일수신관리
 * 
 * @author ljh
 */
@Controller
public class FdcrAdB9Controller extends DefaultController{

	@Resource( name = "fdcrAdB9Service" )
	private FdcrAdB9ServiceImpl fdcrAdB9Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdB9Controller.class );

	/**
	 * 신청 담당자/담당자 메일수신관리 목록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/setup/fdcrAdB9List1.page" )
	public String fdcrAdB9List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "BIG_CODE", "" );
		commandMap.put( "MGNT_DIVS", "" );
		fdcrAdB9Service.fdcrAdB9List1( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "신청 담당자 메일수신관리 목록" );
		return "test";
	}

	/**
	 * 신청기능구분 코드목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/setup/fdcrAdB9List2.page" )
	public void fdcrAdB9List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "BIG_CODE", "83" );
		fdcrAdB9Service.fdcrAdB9List2( commandMap );

		model.addAttribute( "ds_gubun", commandMap.get( "ds_gubun" ) );
		System.out.println( "신청기능구분 코드목록 조회" );
	}

	/**
	 * 메일 신청 담당자 삭제
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/setup/fdcrAdB9Delete1.page" )
	public void fdcrAdB9Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String[] MGNT_DIVSS = request.getParameterValues( "MGNT_DIVS" );
		String[] USER_IDNTS = request.getParameterValues( "USER_IDNT" );
		commandMap.put( "MGNT_DIVS", MGNT_DIVSS );
		commandMap.put( "USER_IDNT", USER_IDNTS );
		commandMap.put( "MGNT_CODE", "" );

		boolean isSuccess = fdcrAdB9Service.fdcrAdB9Delete1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "메일 신청자 삭제" );
	}

	/**
	 * 중복확인
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/setup/fdcrAdB9List3.page" )
	public void fdcrAdB9List3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "MGNT_IDNT", "" );
		commandMap.put( "MGNT_DIVS", "" );
		commandMap.put( "MGNT_CODE", "" );
		fdcrAdB9Service.fdcrAdB9List3( commandMap );

		model.addAttribute( "ds_condition", commandMap.get( "ds_condition" ) );
		System.out.println( "중복확인" );
	}

	/**
	 * 메일 신청 담당자 추가
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/setup/fdcrAdB9Insert1.page" )
	public void fdcrAdB9Insert1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "MGNT_IDNT", "" );
		commandMap.put( "MGNT_DIVS", "" );
		commandMap.put( "MGNT_CODE", "" );

		boolean isSuccess = fdcrAdB9Service.fdcrAdB9Insert1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "메일 신청 담당자 추가" );
	}

	/**
	 * 추가 담당자 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/setup/fdcrAdB9List4.page" )
	public void fdcrAdB9List4( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "TRST_ORGN_CODE", "" );
		commandMap.put( "USER_IDNT", "" );
		commandMap.put( "USER_NAME", "" );
		commandMap.put( "GUBUN", "" );
		commandMap.put( "COMM_NAME", "" );
		fdcrAdB9Service.fdcrAdB9List4( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "담당자 목록 조회" );
	}

}
