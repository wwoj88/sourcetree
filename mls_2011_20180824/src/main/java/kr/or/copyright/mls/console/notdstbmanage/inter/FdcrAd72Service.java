package kr.or.copyright.mls.console.notdstbmanage.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd72Service{

	/**
	 * 도서관 미분배보상금 관리 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd72List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 도서관 보상금 등록현황 등록
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd72Insert1( Map<String, Object> commandMap ) throws Exception;

}
