package kr.or.copyright.mls.console.event.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAdA3Service{

	/**
	 * 이벤트 현황조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA3List1( Map<String, Object> commandMap ) throws Exception;

}
