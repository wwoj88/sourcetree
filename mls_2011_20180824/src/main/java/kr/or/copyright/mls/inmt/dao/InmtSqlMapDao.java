package kr.or.copyright.mls.inmt.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class InmtSqlMapDao extends SqlMapClientDaoSupport implements InmtDao {
	
	public List inmtList(Map map) {
		return getSqlMapClientTemplate().queryForList("Inmt.inmtList",map);
	}	
	
	public List inmtListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Inmt.inmtListCount",map);
	}
	
	public int prpsMastKey() {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("Inmt.prpsMastKey",null);
		return max.intValue(); 
	}
	
	public int prpsSeqnMax() {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("Inmt.prpsSeqnMax",null);
		return max.intValue(); 
	}
	
	public void inmtPspsInsert(Map map) {
		getSqlMapClientTemplate().insert("Inmt.inmtPspsInsert", map);
	}
	
	public void inmtPrpsRsltInsert(Map map) {
		getSqlMapClientTemplate().insert("Inmt.inmtPrpsRsltInsert", map);
	}
	
	public void inmtKappInsert(Map map) {
		getSqlMapClientTemplate().insert("Inmt.inmtKappInsert", map);
	}
	
	public void inmtFokapoInsert(Map map) {
		getSqlMapClientTemplate().insert("Inmt.inmtFokapoInsert", map);
	}
	
	public void inmtKrtraInsert(Map map) {
		getSqlMapClientTemplate().insert("Inmt.inmtKrtraInsert", map);
	}
	
	public List inmtTotalDealStat(Map map){
		return getSqlMapClientTemplate().queryForList("Inmt.inmtTotalDealStat", map);
	}
	
	public List inmtTempList(Map map) {
		return getSqlMapClientTemplate().queryForList("Inmt.inmtTempList" , map);
	}	
	
	public List inmtPrpsSelect(Map map) {
		return getSqlMapClientTemplate().queryForList("Inmt.inmtPrpsSelect" , map);
	}	
	
	public List inmtPrpsRsltList(Map map) {
		return getSqlMapClientTemplate().queryForList("Inmt.inmtPrpsRsltList" , map);
	}	
	
	public List inmtKappList(Map map) {
		return getSqlMapClientTemplate().queryForList("Inmt.inmtKappList" , map);
	}
	
	public List inmtFokapoList(Map map) {
		return getSqlMapClientTemplate().queryForList("Inmt.inmtFokapoList" , map);
	}
	
	public List inmtKrtraList(Map map) {
		return getSqlMapClientTemplate().queryForList("Inmt.inmtKrtraList" , map);
	}
	
	public void inmtPrpsUpdate(Map map) {
		getSqlMapClientTemplate().update("Inmt.inmtPrpsUpdate", map);
	}
	
	public void inmtPrpsDelete(Map map) {
		getSqlMapClientTemplate().delete("Inmt.inmtPrpsDelete", map);
	}
	
	public void inmtPrpsRsltDelete(Map map) {
		getSqlMapClientTemplate().delete("Inmt.inmtPrpsRsltDelete", map);
	}
	
	public void inmtKappDelete(Map map) {
		getSqlMapClientTemplate().delete("Inmt.inmtKappDelete", map);
	}
	
	public void inmtFokapoDelete(Map map) {
		getSqlMapClientTemplate().delete("Inmt.inmtFokapoDelete", map);
	}
	
	public void inmtKrtraDelete(Map map) {
		getSqlMapClientTemplate().delete("Inmt.inmtKrtraDelete", map);
	}
	
	
	public List inmtFileList(Map map) {
		return getSqlMapClientTemplate().queryForList("Inmt.inmtFileList", map);
	}
	
	public void inmtFileInsert(Map map) {
		getSqlMapClientTemplate().insert("Inmt.inmtFileInsert", map);
	}
	
	public void inmtFileDelete(Map map) {
		getSqlMapClientTemplate().delete("Inmt.inmtFileDelete", map);
	}
	
	public void inmtFileDeleteOne(Map map) {
		getSqlMapClientTemplate().delete("Inmt.inmtFileDeleteOne", map);
	}
	
	public List getMaxDealStat(Map map) {
		return getSqlMapClientTemplate().queryForList("Inmt.getMaxDealStat", map);
	}
	
	// 강명표 추가
	public void insertConnInfo(Map map) {
		getSqlMapClientTemplate().insert("Inmt.insertConnInfo", map);
	}
}
