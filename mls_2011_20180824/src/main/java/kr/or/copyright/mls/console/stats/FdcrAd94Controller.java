package kr.or.copyright.mls.console.stats;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Service;
import kr.or.copyright.mls.console.stats.inter.FdcrAd94Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 통계관리 > 저작권찾기신청 및 처리통계(임시)
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd94Controller extends DefaultController{

	@Resource( name = "fdcrAd94Service" )
	private FdcrAd94Service fdcrAd94Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd94Controller.class );

	/**
	 * 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd94List1.page" )
	public String fdcrAd94List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd94List1 Start" );
		// 파라미터 셋팅
		commandMap.put( "YEAR_MONTH", "" );

		fdcrAd94Service.fdcrAd94List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "fdcrAd94List1 End" );
		return "test";
	}
}
