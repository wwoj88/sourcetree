package kr.or.copyright.mls.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ImageFileView {

	/** 파일 뷰
	 * 루트경로 이외에 이미지를 불러올때 씀
	 * ex) <img src="/imageview.do?filePath=C:/dev/mls_new/upload/event/bullet1.gif"/>
	 * @param req
	 * @param res
	 * @param filePath
	 * @throws Exception
	 */
	@RequestMapping("/imageview")
	public void getImageView(HttpServletRequest req, HttpServletResponse res, String filePath) throws Exception {

		String str = filePath.substring(filePath.lastIndexOf(".")+1).toLowerCase();//확장자 뽑기
		String arr = "gif jpg png bmp";//가능 확장자
		
		if (arr.indexOf(str) >= 0) {
			File file = new File(filePath);
			if(file.exists()){
				res.setContentType("image/jpeg");
				//res.setHeader("Content-Disposition", "attachment;filename=" + fileName);             
				FileInputStream fi = new FileInputStream(file);
	            OutputStream out1 = res.getOutputStream();
	            do
	            {
	              int d = fi.read();
	              out1.write(d);
	            } while(fi.available() != 0);
	              fi.close();
	            out1.close();
			}
		}else{
			throw(new Exception("Not Image File /imageview.do"));
		}
	}
}