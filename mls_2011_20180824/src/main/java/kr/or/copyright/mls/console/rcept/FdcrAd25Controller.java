package kr.or.copyright.mls.console.rcept;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.ExcelUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 관리저작물 접수 및 처리 > 미분배 보상금 대상 저작물 > 도서관 > 수정보고
 * 
 * @author ljh
 */
@Controller
public class FdcrAd25Controller extends DefaultController{

	@Resource( name = "fdcrAd22Service" )
	private FdcrAd22ServiceImpl fdcrAd22Service;

	@Resource( name = "fdcrAd24Service" )
	private FdcrAd24ServiceImpl fdcrAd24Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd25Controller.class );
	public static final int RETURN_TYPE_STRING = 1;

	/**
	 * 미분배보상금 도서관 저작물 보고일 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd25List1.page" )
	public String fdcrAd25List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "CD", "2" );
		if(!ConsoleLoginUser.getTrstOrgnCode().equals( "200" ) && !ConsoleLoginUser.getTrstOrgnCode().equals( "205" )){
			System.out.println("ConsoleLoginUser.getTrstOrgnCode()::::::::::::::::"+ConsoleLoginUser.getTrstOrgnCode());
			return returnUrl(model, "해당단체만 접근 가능합니다.", "/console/main/main.page?menuId=126");
		}
		
		fdcrAd24Service.fdcrAd24List1( commandMap );
		model.addAttribute( "ds_list_0", commandMap.get( "ds_list_0" ) );
		System.out.println( "미분배보상금 도서관 저작물 보고일 조회" );
		return "rcept/fdcrAd25List1.tiles";
	}

	/**
	 * 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd25Down1.page" )
	public void fdcrAd25Down1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		//commandMap.put( "FILE_URL", "" );
		String filePath = "";
		String fileName = "";

		//fileName = ".xlsx";
		//filePath = "/home/right4me/web/upload/form/미분배도서관업로드샘플.xlsx";
		fileName = "LIBR_SAMPLE.xlsx";
		filePath = "/home/right4me_test/web/upload/form/LIBR_SAMPLE.xlsx";

		if( null != fileName && !"".equals( fileName ) ){
			download( request, response, filePath, fileName );
		}
	}

	/**
	 * 엑셀 업로드 후 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd25Upload1.page" )
	public String fdcrAd25Upload1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		//담을 arrayList
		ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
		
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );
		
		String file1  = uploadPath+fileinfo.get(0).get("F_fileName");
		//String file1 = EgovWebUtil.getString( commandMap, "file1" );
		
		ArrayList<ArrayList<Map<String,Object>>> excelDataList = ExcelUtil.get(file1, RETURN_TYPE_STRING);
		ArrayList<Map<String,Object>> excelDataList1 = excelDataList.get(0);
		
		/* 반복문 시작 */
		for(int i=3; i<excelDataList1.size(); i++ ){
			
		Map<String,Object> excelDataList2 = excelDataList1.get(i);
		System.out.println("excelDataList의 행:"+excelDataList1.size());
		System.out.println("excelDataList의 열:"+excelDataList2.size());
		
		Set key = excelDataList2.keySet();
		  for (Iterator iterator = key.iterator(); iterator.hasNext();) {
               String keyName = (String) iterator.next();
               String valueName = (String) excelDataList2.get(keyName);
               System.out.println(keyName +" = " +valueName);
               commandMap.put( "valueName"+i, valueName );
		  }
          uploadList.add(excelDataList2);
		}
		/* 반복문 끝 */
		// TODO 엑셀 조회
		model.addAttribute( "uploadList", uploadList );
		return "rcept/fdcrAd25UploadForm1.tiles";
	}

	/**
	 * 일괄수정(방송음악/도서관/수업목적/교과용)
	 * 
	 * @param model
	 * @param commandMap
	 * @param mreq
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd25Insert1.page" )
	public String fdcrAd25Insert1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		//String RGST_ORGN_CODE = EgovWebUtil.getString( commandMap, "trstOrgnCode" );
		commandMap.put( "RGST_ORGN_CODE", "205" );
		commandMap.put( "TRST_ORGN_CODE", "205" );
		commandMap.put( "USER_IDNT", ConsoleLoginUser.getUserId() );
		commandMap.put( "USER_NAME", "" );
		commandMap.put( "GENRE", "L" );
		
		String MGNT_INMT_SEQN = EgovWebUtil.getString( commandMap, "MGNT_INMT_SEQN" );
		String WORK_NAME = EgovWebUtil.getString( commandMap, "WORK_NAME" );
		String COPT_HODR = EgovWebUtil.getString( commandMap, "COPT_HODR" );
		String USEX_LIBR = EgovWebUtil.getString( commandMap, "USEX_LIBR" );
		String LISH_COMP = EgovWebUtil.getString( commandMap, "LISH_COMP" );
		String PUBC_YEAR = EgovWebUtil.getString( commandMap, "PUBC_YEAR" );
		String YYMM = EgovWebUtil.getString( commandMap, "YYMM" );
		String ALLT_YSNO = EgovWebUtil.getString( commandMap, "ALLT_YSNO" );
		String ALLT_DATE = EgovWebUtil.getString( commandMap, "ALLT_DATE" );
		String ALLT_AMNT = EgovWebUtil.getString( commandMap, "ALLT_AMNT" );
		
		if( !"".equals( MGNT_INMT_SEQN ) ){
			MGNT_INMT_SEQN = URLDecoder.decode( MGNT_INMT_SEQN, "UTF-8" );
			commandMap.put( "MGNT_INMT_SEQN", MGNT_INMT_SEQN );
		}
		if( !"".equals( WORK_NAME ) ){
			WORK_NAME = URLDecoder.decode( WORK_NAME, "UTF-8" );
			commandMap.put( "WORK_NAME", WORK_NAME );
		}
		if( !"".equals( COPT_HODR ) ){
			COPT_HODR = URLDecoder.decode( COPT_HODR, "UTF-8" );
			commandMap.put( "COPT_HODR", COPT_HODR );
		}
		if( !"".equals( USEX_LIBR ) ){
			USEX_LIBR = URLDecoder.decode( USEX_LIBR, "UTF-8" );
			commandMap.put( "USEX_LIBR", USEX_LIBR );
		}
		if( !"".equals( LISH_COMP ) ){
			LISH_COMP = URLDecoder.decode( LISH_COMP, "UTF-8" );
			commandMap.put( "LISH_COMP", LISH_COMP );
		}
		if( !"".equals( PUBC_YEAR ) ){
			PUBC_YEAR = URLDecoder.decode( PUBC_YEAR, "UTF-8" );
			commandMap.put( "PUBC_YEAR", PUBC_YEAR );
		}
		if( !"".equals( YYMM ) ){
			YYMM = URLDecoder.decode( YYMM, "UTF-8" );
			commandMap.put( "YYMM", YYMM );
		}
		if( !"".equals( ALLT_YSNO ) ){
			ALLT_YSNO = URLDecoder.decode( ALLT_YSNO, "UTF-8" );
			commandMap.put( "ALLT_YSNO", ALLT_YSNO );
		}
		if( !"".equals( ALLT_DATE ) ){
			ALLT_DATE = URLDecoder.decode( ALLT_DATE, "UTF-8" );
			commandMap.put( "ALLT_DATE", ALLT_DATE );
		}
		if( !"".equals( ALLT_AMNT ) ){
			ALLT_AMNT = URLDecoder.decode( ALLT_AMNT, "UTF-8" );
			commandMap.put( "ALLT_AMNT", ALLT_AMNT );
		}

		ArrayList<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
		excelList.add( commandMap );
		boolean isSuccess = fdcrAd22Service.fdcrAd22Insert1( commandMap, excelList );
		returnAjaxString( response, isSuccess );
		
		if( isSuccess ){
			return returnUrl(
				model,
				"저장했습니다.",
				"/console/rcept/fdcrAd25List1.page");
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/rcept/fdcrAd25List1.page");
		}
	}
}
