package kr.or.copyright.mls.console.effort;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAd03Dao" )
public class FdcrAd03DaoImpl extends EgovComAbstractDAO implements FdcrAd03Dao{

	// ---------------------------------------상당한노력 proc start
	// --------------------------------------

	public List selectStatProcAllLogList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcAllLogList", map );
	}

	public List selectStatWorksAllLogList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statWorksAllLogList", map );
	}

	public void callProcGetStatProcOrd( Map map ){

		getSqlMapClientTemplate().queryForObject( "AdminStatProc.callGetStatProcord", map );

	}

	public void callStatProc( Map map ){

		getSqlMapClientTemplate().queryForObject( "AdminStatProc.callStatProc", map );

	}

	public void callStatProc01( Map map ){

		getSqlMapClientTemplate().queryForObject( "AdminStatProc.callStatProc01", map );

	}

	public void callStatProc02( Map map ){

		getSqlMapClientTemplate().queryForObject( "AdminStatProc.callStatProc02", map );

	}

	public void callGetStatWorksord( Map map ){

		getSqlMapClientTemplate().queryForObject( "AdminStatProc.callGetStatWorksord", map );

	}

	public void callChangeStatWorks( Map map ){

		getSqlMapClientTemplate().queryForObject( "AdminStatProc.callChangeStatWorks", map );

	}

	public void statProcAllLogUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatProc.statProcAllLogUpdate", map );
	}

	public void chgStatWorksUpdateYn( Map map ){

		getSqlMapClientTemplate().update( "AdminStatProc.chgStatWorksUpdateYn", map );
	}

	public List statProcTargPopListCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcTargPopListCount", map );
	}

	public List statProcTargPopList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcTargPopList", map );
	}

	public List statProcTargExcpListCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcTargExcpListCount", map );
	}

	public List statProcTargExcpList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcTargExcpList", map );
	}

	public List statProcTargWorksMailList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcTargWorksMailList", map );
	}

	// ---------------------------------------상당한노력 proc end --------------------------------------//

	public List inmtReptView( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.inmtReptView", map );
	}

	public List nonWorksReptView( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.nonWorksReptView", map );
	}

	public List worksEntReptView( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.worksEntReptView", map );
	}

	public List statProcPopup( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcPopup", map );
	}
	
	public List statProcPopupNewExcel( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcPopupNewExcel", map );
	}
	

	public List statProcWorksPopup( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcWorksPopup", map );
	}
	
	public List statProcWorksPopupNewExcel( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcWorksPopupNewExcel", map );
	}

	public List statProcWorksPopListCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcWorksPopListCount", map );
	}

	public List reptMgntList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.reptMgntList", map );
	}

	public void reptMgntUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatProc.reptMgntUpdate", map );
	}

	public void reptMgntUpdateYn( Map map ){
		getSqlMapClientTemplate().update( "AdminStatProc.reptMgntUpdateYn", map );
	}

	public List statProcInfo( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcInfo", map );
	}
	
	public List statProcInfoNewExcel( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcInfoNewExcel", map );
	}
	
	public List statProcInfoCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcInfoCount", map );
	}

	public List statProcPopListCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatProc.statProcPopListCount", map );
	}

	public void insertMailList( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatProc.insertMailList", map );
	}

}
