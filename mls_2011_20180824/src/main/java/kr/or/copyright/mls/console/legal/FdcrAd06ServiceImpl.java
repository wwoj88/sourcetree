package kr.or.copyright.mls.console.legal;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.annotation.Resource;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Service;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;
import org.springframework.stereotype.Service;
import com.softforum.xdbe.xCrypto;

@Service("fdcrAd06Service")
public class FdcrAd06ServiceImpl extends CommandService implements FdcrAd06Service {

     // AdminStatMgntSqlMapDao
     @Resource(name = "fdcrAd06Dao")
     private FdcrAd06Dao fdcrAd06Dao;

     // AdminStatBoardSqlMapDao
     @Resource(name = "fdcrAd01Dao")
     private FdcrAd01Dao fdcrAd01Dao;

     // AdminCommonSqlMapDao
     @Resource(name = "consoleCommonDao")
     private ConsoleCommonDao consoleCommonDao;

     /**
      * 법정허락 이용승인 신청 목록
      * 
      * @param commandMap
      * @return
      * @throws Exception
      */
     public ArrayList<Map<String, Object>> fdcrAd06List1(Map<String, Object> commandMap) throws Exception {

          List pageList = (List) fdcrAd06Dao.adminStatPrpsListCount(commandMap); // 페이징카운트
          int totCnt = ((BigDecimal) ((Map) pageList.get(0)).get("COUNT")).intValue();
          pagination(commandMap, totCnt, 0);
          commandMap.put("ds_count", totCnt);
          return fdcrAd06Dao.adminStatPrpsList(commandMap);
     }

     /**
      * 법정허락 이용승인 신청 상세
      * 
      * @param commandMap
      * @return
      * @throws SQLException
      */
     /*
      * public Map<String, Object> fdcrAd06View1( Map<String, Object> commandMap ) throws Exception{ //
      * DAO 호출 // 법정허락 이용승인 신청서 조회 List statAppl = (List) fdcrAd06Dao.adminStatApplicationSelect(
      * commandMap ); Map statApplObj = new HashMap(); resd복호화 str 20121108 정병호 xCrypto.RegisterEx(
      * "pattern7", 2, new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" )
      * ), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7" ); Map statApplObj = new HashMap();
      * statApplObj = (Map) statAppl.get( 0 ); String APPLR_RESD_CORP_NUMB = xCrypto.Decrypt( "pattern7",
      * (String) statApplObj.get( "APPLR_RESD_CORP_NUMB" ) );// (원본 // 신청인주민번호 // 복호화) String
      * APPLY_PROXY_RESD_CORP_NUMB = xCrypto.Decrypt( "pattern7", (String) statApplObj.get(
      * "APPLY_PROXY_RESD_CORP_NUMB" ) );// (원본 // 대리인주민번호 // 복호화) String RGST_RESD_CORP_NUMB =
      * xCrypto.Decrypt( "pattern7", (String) statApplObj.get( "RGST_RESD_CORP_NUMB" ) );// (원본 //
      * 사용자테이블에서 // 등록자주민번호 // 복호화) String APPLR_RESD_CORP_NUMB_N = APPLR_RESD_CORP_NUMB.replaceAll( "-",
      * "" );// 신청인주민번호 // -제거 String APPLY_PROXY_RESD_CORP_NUMB_N = ""; // 대리인 경우 if널체크 해야함 if(
      * APPLY_PROXY_RESD_CORP_NUMB != null && !APPLY_PROXY_RESD_CORP_NUMB.equals( "" ) ){
      * APPLY_PROXY_RESD_CORP_NUMB_N = APPLY_PROXY_RESD_CORP_NUMB.replaceAll( "-", "" );// 대리인주민번호 // -제거
      * } statApplObj.put( "APPLR_RESD_CORP_NUMB", APPLR_RESD_CORP_NUMB ); statApplObj.put(
      * "APPLY_PROXY_RESD_CORP_NUMB", APPLY_PROXY_RESD_CORP_NUMB ); statApplObj.put(
      * "RGST_RESD_CORP_NUMB", RGST_RESD_CORP_NUMB ); statApplObj.put( "APPLR_RESD_CORP_NUMB_N",
      * APPLR_RESD_CORP_NUMB_N ); statApplObj.put( "APPLY_PROXY_RESD_CORP_NUMB_N",
      * APPLY_PROXY_RESD_CORP_NUMB_N ); //statAppl.add( 0, statApplObj ); // 법정허락신청 첨부서류 List attchList =
      * (List) fdcrAd06Dao.adminStatAttcFileSelect( commandMap ); // 법정허락 이용승인신청 명세서 List applyWorks =
      * (List) fdcrAd06Dao.adminStatApplyWorksSelect( commandMap ); commandMap.put( "ds_stat_appl",
      * statAppl ); commandMap.put( "ds_stat_file", attchList ); commandMap.put( "ds_stat_works",
      * applyWorks ); commandMap.put( "ds_ml_file", attchList ); return commandMap; }
      */

     /**
      * 법정허락 이용승인 신청 상세
      * 
      * @return
      * @throws SQLException
      */
     public Map<String, Object> fdcrAd06View1(Map<String, Object> commandMap) throws SQLException {

          return fdcrAd06Dao.adminStatApplicationSelect(commandMap);
     }

     /**
      * 법정허락신청 첨부서류 목록
      * 
      * @param commandMap
      * @return
      * @throws SQLException
      */
     public ArrayList<Map<String, Object>> selectAttachFileList(Map<String, Object> commandMap) throws SQLException {

          return fdcrAd06Dao.adminStatAttcFileSelect(commandMap);
     }

     /**
      * 법정허락신청 첨부서류 정보
      * 
      * @return
      * @throws SQLException
      */
     public Map<String, Object> selectAttachFileInfo(Map<String, Object> commandMap) throws SQLException {

          return fdcrAd06Dao.adminStatAttcFileInfo(commandMap);
     }

     /**
      * 법정허락신청 이용승인 명세서 목록
      * 
      * @return
      * @throws SQLException
      */
     public ArrayList<Map<String, Object>> adminStatApplyWorksSelect(Map<String, Object> commandMap) throws SQLException {

          return fdcrAd06Dao.adminStatApplyWorksSelect(commandMap);
     }

     /**
      * 법정허락신청 이용승인 명세서 정보
      * 
      * @return
      * @throws SQLException
      */
     public ArrayList<Map<String, Object>> adminStatApplyWorksSelectInfo(Map<String, Object> commandMap) throws SQLException {

          return fdcrAd06Dao.adminStatApplyWorksSelectInfo(commandMap);
     }

     /**
      * 법정허락 이용승인 신청서 수정 폼
      * 
      * @param commandMap
      * @return
      * @throws SQLException
      */
     public void fdcrAd06UpdateForm1(Map<String, Object> commandMap) throws Exception {

          // DAO 호출
          // 법정허락 이용승인 신청서 조회
          List statAppl = (List) fdcrAd06Dao.adminStatApplicationSelect(commandMap);

          /* resd복호화 str 20121108 정병호 */
          xCrypto.RegisterEx("pattern7", 2, new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");

          Map statApplObj = new HashMap();
          statApplObj = (Map) statAppl.get(0);

          String APPLR_RESD_CORP_NUMB = xCrypto.Decrypt("pattern7", (String) statApplObj.get("APPLR_RESD_CORP_NUMB"));// (원본
          // 신청인주민번호
          // 복호화)
          String APPLY_PROXY_RESD_CORP_NUMB = xCrypto.Decrypt("pattern7", (String) statApplObj.get("APPLY_PROXY_RESD_CORP_NUMB"));// (원본
          // 대리인주민번호
          // 복호화)
          String RGST_RESD_CORP_NUMB = xCrypto.Decrypt("pattern7", (String) statApplObj.get("RGST_RESD_CORP_NUMB"));// (원본
          // 사용자테이블에서
          // 등록자주민번호
          // 복호화)

          String APPLR_RESD_CORP_NUMB_N = APPLR_RESD_CORP_NUMB.replaceAll("-", "");// 신청인주민번호
          // -제거
          String APPLY_PROXY_RESD_CORP_NUMB_N = "";

          // 대리인 경우 if널체크 해야함
          if (APPLY_PROXY_RESD_CORP_NUMB != null && !APPLY_PROXY_RESD_CORP_NUMB.equals("")) {
               APPLY_PROXY_RESD_CORP_NUMB_N = APPLY_PROXY_RESD_CORP_NUMB.replaceAll("-", "");// 대리인주민번호
               // -제거
          }

          statApplObj.put("APPLR_RESD_CORP_NUMB", APPLR_RESD_CORP_NUMB);
          statApplObj.put("APPLY_PROXY_RESD_CORP_NUMB", APPLY_PROXY_RESD_CORP_NUMB);
          statApplObj.put("RGST_RESD_CORP_NUMB", RGST_RESD_CORP_NUMB);
          statApplObj.put("APPLR_RESD_CORP_NUMB_N", APPLR_RESD_CORP_NUMB_N);
          statApplObj.put("APPLY_PROXY_RESD_CORP_NUMB_N", APPLY_PROXY_RESD_CORP_NUMB_N);

          statAppl.add(0, statApplObj);

          // 법정허락신청 첨부서류
          List attchList = (List) fdcrAd06Dao.adminStatAttcFileSelect(commandMap);
          // 법정허락 이용승인신청 명세서
          List applyWorks = (List) fdcrAd06Dao.adminStatApplyWorksSelect(commandMap);

          commandMap.put("ds_stat_appl", statAppl);
          commandMap.put("ds_stat_file", attchList);
          commandMap.put("ds_stat_works", applyWorks);
          commandMap.put("ds_ml_file", attchList);

     }

     /**
      * 법정허락 이용승인 신청서 수정
      * 
      * @param commandMap
      * @return
      * @throws SQLException
      */
     public boolean fdcrAd06Update1(Map<String, Object> commandMap) throws Exception {

          boolean result = false;
          try {

               String RGST_IDNT = (String) commandMap.get("RGST_IDNT");
               String APPLY_WRITE_YMD = (String) commandMap.get("APPLY_WRITE_YMD");
               String APPLY_WRITE_SEQ = (String) commandMap.get("APPLY_WRITE_SEQ");
               String WORKS_SEQN = (String) commandMap.get("WORKS_SEQN");
               String ATTC_SEQN = (String) commandMap.get("ATTC_SEQN");

               // --주민번호 암호화 시작
               /*
                * xCrypto.RegisterEx( "pattern7", 2, new String(
                * kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ), "pool1", "mcst_db",
                * "mcst_owner", "mcst_table", "pattern7" );
                * 
                * String sOutput7 = null; String sString = (String) commandMap.get( "APPLR_RESD_CORP_NUMB" );
                * sOutput7 = xCrypto.Encrypt( "pattern7", sString );
                * 
                * String sOutput7_1 = null; String sString_1 = (String) commandMap.get(
                * "APPLY_PROXY_RESD_CORP_NUMB" ); sOutput7_1 = xCrypto.Encrypt( "pattern7", sString_1 );
                * 
                * commandMap.put( "APPLR_RESD_CORP_NUMB", sOutput7 ); commandMap.put( "APPLY_PROXY_RESD_CORP_NUMB",
                * sOutput7_1 );
                */
               // --주민번호 암호화 끝

               Map statAttcFileMap = null;
               Map dsFileMap = null;
               Map fileMap = null;

               // 명세서 삭제
               for (int i = 0; i < WORKS_SEQN.length(); i++) {

                    fdcrAd06Dao.adminStatWorksDel(commandMap);
               }

               /*
                * if(!"".equals( ATTC_SEQN ) && ATTC_SEQN != null){ // 첨부파일 삭제 for( int i = 0; i <
                * ATTC_SEQN.length(); i++ ){ String attc_seqn = (String) commandMap.get( "ATTC_SEQN" ); if(
                * attc_seqn != null && !attc_seqn.equals( "" ) ){ //dsFileMap = getMap( ds_file_tmp, i );
                * statAttcFileMap = new HashMap(); statAttcFileMap.put( "ATTC_SEQN", commandMap.get( "ATTC_SEQN" )
                * ); statAttcFileMap.put( "APPLY_WRITE_YMD", commandMap.get( "APPLY_WRITE_YMD" ) );
                * statAttcFileMap.put( "APPLY_WRITE_SEQ", commandMap.get( "APPLY_WRITE_SEQ" ) );
                * 
                * // 첨부 서류삭제 fdcrAd06Dao.adminStatAttcFileDel( statAttcFileMap ); // 첨부 파일삭제
                * fdcrAd06Dao.adminFileDel( statAttcFileMap ); } } }
                */

               // 이용승인신청서 수정
               fdcrAd06Dao.adminStatPrpsUpte(commandMap);
               // 이용승인신청서 상태변경
               fdcrAd06Dao.adminStatPrpsShisRegi2(commandMap);

               // 첨부파일 재등록
               /*
                * if(!"".equals( ATTC_SEQN ) && ATTC_SEQN != null){ for( int j = 0; j < ATTC_SEQN.length(); j++ ){
                * //dsFileMap = getMap( ds_ml_file, j ); statAttcFileMap = new HashMap(); fileMap = new HashMap();
                * int attachSeqn = consoleCommonDao.getNewAttcSeqn(); String fileName = (String)
                * commandMap.get("FILE_NAME" ); if( fileName != null && !fileName.equals( "" ) ){ String filePath =
                * ""; String realFileNm = ""; String fileSize = ""; byte[] file = null; Map upload = null; String
                * write_ymd = (String) commandMap.get( "APPLY_WRITE_YMD" ); String write_seq = (String)
                * commandMap.get( "APPLY_WRITE_SEQ" ); if( commandMap.get( "ATTACH_FILE_CONTENT" + j ) == null ||
                * commandMap.get( "ATTACH_FILE_CONTENT" + j ).toString().equals( "" ) ){ filePath = (String)
                * commandMap.get( "FILE_PATH" ); realFileNm = (String) commandMap.get("REAL_FILE_NAME" ); fileSize
                * = (String) commandMap.get("FILE_SIZE" ); }else{ file = ( (byte[])
                * commandMap.get("ATTACH_FILE_CONTENT" + j ) ); upload = FileUtil.uploadMiFile( fileName, file );
                * filePath = upload.get( "FILE_PATH" ).toString(); realFileNm = upload.get( "REAL_FILE_NAME"
                * ).toString(); fileSize = commandMap.get("ATTACH_FILE_SIZE" + j ).toString(); } fileMap.put(
                * "ATTC_SEQN", attachSeqn ); fileMap.put( "FILE_ATTC_CD", commandMap.get( "FILE_ATTC_CD" ) );
                * fileMap.put( "FILE_NAME_CD", commandMap.get( "FILE_NAME_CD" ) ); fileMap.put( "FILE_NAME",
                * fileName ); fileMap.put( "FILE_PATH", filePath ); fileMap.put( "FILE_SIZE", fileSize );
                * fileMap.put( "REAL_FILE_NAME", realFileNm ); fileMap.put( "RGST_IDNT", commandMap.get(
                * "RGST_IDNT" ) ); // 첨부파일 // 등록 fdcrAd06Dao.adminFileInsert( fileMap ); statAttcFileMap.put(
                * "ATTC_SEQN", attachSeqn ); statAttcFileMap.put( "APPLY_WRITE_YMD", write_ymd );
                * statAttcFileMap.put( "APPLY_WRITE_SEQ", write_seq ); statAttcFileMap.put( "MODI_IDNT",
                * commandMap.get( "RGST_IDNT" ) ); // 첨부서류 등록 fdcrAd06Dao.adminStatAttcFileInsert( statAttcFileMap
                * ); } } }
                */

               // 명세서 재등록
               for (int j = 0; j < WORKS_SEQN.length(); j++) {

                    Map<String, Object> param = new HashMap<String, Object>();

                    int works_seqn = fdcrAd06Dao.adminStatWorksSeqn(commandMap);

                    param.put("APPLY_WRITE_YMD", commandMap.get("APPLY_WRITE_YMD"));
                    param.put("APPLY_WRITE_SEQ", commandMap.get("APPLY_WRITE_SEQ"));
                    param.put("WORKS_SEQN", works_seqn);

                    fdcrAd06Dao.adminStatWorksInsert(param);
               }

               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     /**
      * 법정허락 이용승인 신청서 삭제
      * 
      * @param commandMap
      * @return
      * @throws SQLException
      */
     public boolean fdcrAd06Delete1(Map<String, Object> commandMap) throws Exception {

          boolean result = false;
          try {
               String RGST_IDNT = (String) commandMap.get("RGST_IDNT");
               String APPLY_WRITE_YMD = (String) commandMap.get("APPLY_WRITE_YMD");
               String APPLY_WRITE_SEQ = (String) commandMap.get("APPLY_WRITE_SEQ");
               String STAT_CD = (String) commandMap.get("STAT_CD");
               fdcrAd06Dao.updateStatApplicationStat(commandMap);
               fdcrAd06Dao.insertStatApplicationShis(commandMap);
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     /**
      * 법정허락 이용승인 진행상태 내역조회
      * 
      * @param commandMap
      * @return
      * @throws SQLException
      */
     public void fdcrAd06History1(Map<String, Object> commandMap) throws Exception {

          // DAO 호출
          // 법정허락신청 진행상태 변경내역조회
          List list = (List) fdcrAd06Dao.adminStatApplicationShisSelect(commandMap);

          commandMap.put("ds_list", list);

     }

     /**
      * 법정허락 이용승인 진행상태 수정
      * 
      * @param commandMap
      * @throws SQLException
      */
     public boolean fdcrAd06StatChange1(Map<String, Object> commandMap, ArrayList<Map<String, Object>> fileList) throws Exception {

          boolean result = false;
          try {
               String ds_stat_appl = (String) commandMap.get("STAT_CD");

               for (int i = 0; i < ds_stat_appl.length(); i++) {

                    Map<String, Object> param = new HashMap<String, Object>();

                    String USER_ID = (String) commandMap.get("USER_ID");
                    String APPLY_WRITE_YMD = (String) commandMap.get("APPLY_WRITE_YMD");
                    String APPLY_WRITE_SEQ = (String) commandMap.get("APPLY_WRITE_SEQ");
                    String RECEIPT_NO = (String) commandMap.get("RECEIPT_NO");
                    String STAT_CD = (String) commandMap.get("STAT_CD");
                    String STAT_MEMO = (String) commandMap.get("STAT_MEMO");
                    String ATTC_SEQNS = (String) commandMap.get("ATTC_SEQN");
                    String RGST_IDNT = (String) commandMap.get("RGST_IDNT");
                    param.put("RGST_IDNT", RGST_IDNT);
                    param.put("APPLY_WRITE_YMD", APPLY_WRITE_YMD);
                    param.put("APPLY_WRITE_SEQ", APPLY_WRITE_SEQ);
                    param.put("RECEIPT_NO", RECEIPT_NO);
                    param.put("STAT_CD", STAT_CD);
                    param.put("STAT_MEMO", STAT_MEMO);
                    param.put("ATTC_SEQN", ATTC_SEQNS);

                    SimpleDateFormat _SimpleDateFormat = new SimpleDateFormat("yyyy", new Locale("ko", "KOREA"));
                    String yyyymmdd = _SimpleDateFormat.format(new Date());

                    String receiptYmd = yyyymmdd;
                    String receiptSeq = fdcrAd06Dao.adminStatApplicationReceiptSeqSelect(); // 접수번호
                    // 시퀀스
                    // 조회
                    String receiptNo = receiptYmd + "-" + receiptSeq;

                    if (commandMap.get("STAT_CD").equals("8")) { // 접수완료
                         commandMap.put("RECEIPT_NO", receiptNo);
                    }
                    /*
                     * if( row_status.equals("update") == true ) {
                     */
                    /* 첨부파일 관련............................주석일단.. */
                    /*
                     * int attachSeqn = commonDao.getNewAttcSeqn(); String fileName = ds_stat_appl.getColumnAsString(i,
                     * "FILE_NAME"); if(fileName != null && !fileName.equals("")) { applMap.put("ATTC_SEQN",
                     * attachSeqn); String filePath = ""; String realFileNm = ""; String fileSize = ""; byte[] file =
                     * null; Map upload = null;
                     * 
                     * if(ds_stat_appl.getColumn(i, "ATTACH_FILE_CONTENT0") == null ||
                     * ds_stat_appl.getColumn(i,"ATTACH_FILE_CONTENT0").toString().equals("")){ filePath =
                     * ds_stat_appl.getColumnAsString(i, "FILE_PATH"); realFileNm = ds_stat_appl.getColumnAsString(i,
                     * "REAL_FILE_NAME"); fileSize = ds_stat_appl.getColumnAsString(i, "FILE_SIZE"); }else{ file =
                     * ds_stat_appl.getColumn(i,"ATTACH_FILE_CONTENT0").getBinary(); upload =
                     * FileUtil.uploadMiFile(fileName, file); filePath = upload.get("FILE_PATH").toString(); realFileNm
                     * = upload.get("REAL_FILE_NAME").toString(); fileSize = ds_stat_appl.getColumn(i,
                     * "ATTACH_FILE_SIZE0").toString(); } Map fileMap = new HashMap(); fileMap.put("ATTC_SEQN",
                     * attachSeqn); fileMap.put("FILE_ATTC_CD", "ST"); fileMap.put("FILE_NAME", fileName);
                     * fileMap.put("FILE_PATH", filePath); fileMap.put("FILE_SIZE", fileSize);
                     * fileMap.put("REAL_FILE_NAME", realFileNm); fileMap.put("RGST_IDNT", applMap.get("USER_ID")); //
                     * 첨부파일 등록 adminStatMgntDao.adminFileInsert(fileMap); }
                     */
                    int attachSeqn = consoleCommonDao.getNewAttcSeqn();

                    Map<String, Object> fileInfo = new HashMap<String, Object>();
                    if (fileList != null && fileList.size() > 0) {

                         fileInfo = fileList.get(0);
                         param.put("ATTC_SEQN", attachSeqn);

                         Map fileMap = new HashMap();
                         fileMap.put("ATTC_SEQN", attachSeqn);
                         fileMap.put("FILE_ATTC_CD", "ST");
                         fileMap.put("FILE_NAME_CD", 99);
                         fileMap.put("FILE_NAME", fileInfo.get("F_fileName"));
                         fileMap.put("FILE_PATH", fileInfo.get("F_saveFilePath"));
                         fileMap.put("FILE_SIZE", fileInfo.get("F_filesize"));
                         fileMap.put("REAL_FILE_NAME", fileInfo.get("F_orgFileName"));
                         fileMap.put("RGST_IDNT", param.get("RGST_IDNT"));
                         fileMap.put("WORKS_ID", param.get("WORKS_ID"));

                         // 첨부파일 등록
                         fdcrAd06Dao.adminFileInsert(fileMap);
                         fdcrAd01Dao.statBord06FileInsert(fileMap);
                    }

                    // 이용승인신청 명세서 수정
                    fdcrAd06Dao.adminStatApplicationUpdate(param);
                    // 이용승인신청 명세서 상태변경내역 등록
                    fdcrAd06Dao.adminStatApplicationShisInsert(param);

                    // 보완요청, 반려, 결제요청 시 메일,sms 보내기
                    if (commandMap.get("STAT_CD").equals("3") || commandMap.get("STAT_CD").equals("6") || commandMap.get("STAT_CD").equals("7")) {
                         // 메일 보내기
                         Map memMap = new HashMap();
                         memMap.put("USER_IDNT", commandMap.get("RGST_IDNT"));
                         List memList = consoleCommonDao.selectMemberInfo(memMap);
                         // memMap = (Map) memList.get( 0 );

                         for (int k = 0; k < memList.size(); k++) {

                              memMap = (HashMap) memList.get(k);

                         }

                         String statNm = "";
                         if (commandMap.get("STAT_CD").equals("3")) {
                              statNm = "보완요청";
                         } else if (commandMap.get("STAT_CD").equals("6")) {
                              statNm = "반려";
                         } else if (commandMap.get("STAT_CD").equals("7")) {
                              statNm = "결제요청";
                         }

                         String host = Constants.MAIL_SERVER_IP; // smtp서버
                         String to = memMap.get("MAIL").toString(); // 수신EMAIL
                         String toName = memMap.get("USER_NAME").toString(); // 수신인
                         String subject = "[저작권찾기] 법정허락 이용신청 " + statNm;

                         String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템
                         // 대표
                         // 메일
                         String fromName = Constants.SYSTEM_NAME;

                         if (memMap.get("MAIL_RECE_YSNO").equals("Y") && to != null && to != "") {

                              // ------------------------- mail 정보
                              // ----------------------------------//
                              MailInfo info = new MailInfo();
                              info.setFrom(from);
                              info.setFromName(fromName);
                              info.setHost(host);
                              info.setEmail(to);
                              info.setEmailName(toName);
                              info.setSubject(subject);

                              StringBuffer sb = new StringBuffer();

                              sb.append("				<tr>");
                              sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
                              sb.append("					<p style=\"font-weight:bold; background:url(" + kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
                                        + "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 법정허락 이용신청 " + statNm + " 정보</p>");
                              sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
                              sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자 이름 : " + toName
                                        + " </li>");
                              sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청번호 : "
                                        + commandMap.get("APPLY_NO") + " </li>");
                              sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : "
                                        + commandMap.get("APPLY_WORKS_TITL") + " </li>");
                              sb.append("					</ul>");
                              sb.append("					</td>");
                              sb.append("				</tr>");

                              info.setMessage(sb.toString());
                              info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
                              System.out.println(new String(MailMessage.getChangeInfoMailText2(info)));
                              MailManager manager = MailManager.getInstance();

                              info = manager.sendMail(info);
                              // ////////////////////테스트 위해 주석처리.
                              if (info.isSuccess()) {
                                   System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());
                              } else {
                                   System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
                              }

                         }

                         // sms 보내기
                         String smsTo = memMap.get("MOBL_PHON").toString();
                         String smsFrom = Constants.SYSTEM_TELEPHONE;

                         String smsMessage = "[저작권찾기] 법정허락 이용신청이 " + statNm + " 되었습니다.";

                         if (memMap.get("SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {

                              SMSManager smsManager = new SMSManager();

                              System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage)); ////// 테스트위해 주석처리.
                         }

                    } // end email, sms

               }
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;

     }

     /*
      * }
      */

     /**
      * 법정허락 이용승인 심의결과 내역조회
      * 
      * @param commandMap
      * @return
      * @throws SQLException
      */
     public void fdcrAd06History2(Map<String, Object> commandMap) throws Exception {

          // DAO 호출
          // 법정허락신청 심의결과상태 변경내역조회
          List list = (List) fdcrAd06Dao.adminStatApplyWorksShisSelect(commandMap);

          commandMap.put("ds_list", list);
     }

     /**
      * 법정허락 이용승인 심의결과 상태 수정
      * 
      * @param commandMap
      * @throws SQLException
      */
     public boolean fdcrAd06StatWorksChange1(Map<String, Object> commandMap, ArrayList<Map<String, Object>> fileList) throws Exception {

          boolean result = false;
          try {
               // String[] ds_stat_appl = (String[]) commandMap.get( "STAT_RSLT_CD"
               // );
               // String row_status = null;

               String ds_stat_appl = (String) commandMap.get("STAT_RSLT_CD");

               for (int i = 0; i < ds_stat_appl.length(); i++) {

                    Map<String, Object> param = new HashMap<String, Object>();
                    /*
                     * String[] USER_IDS = (String[]) commandMap.get( "USER_ID" ); String[] APPLY_WRITE_YMDS =
                     * (String[]) commandMap.get( "APPLY_WRITE_YMD" ); String[] APPLY_WRITE_SEQS = (String[])
                     * commandMap.get( "APPLY_WRITE_SEQ" ); String[] RGST_IDNTS = (String[]) commandMap.get( "RGST_IDNT"
                     * ); String[] STAT_RSLT_CDS = (String[]) commandMap.get( "STAT_RSLT_CD" ); String[] LGMT_AMNTS =
                     * (String[]) commandMap.get( "LGMT_AMNT" ); String[] LGMT_PLAC_NAMES = (String[]) commandMap.get(
                     * "LGMT_PLAC_NAME" ); String[] LGMT_GRANS = (String[]) commandMap.get( "LGMT_GRAN" ); String[]
                     * LGMT_PERIS = (String[]) commandMap.get( "LGMT_PERI" ); String[] WORKS_SEQNS = (String[])
                     * commandMap.get( "WORKS_SEQN" ); String[] STAT_RSLT_MEMOS = (String[]) commandMap.get(
                     * "STAT_RSLT_MEMO" ); String[] ATTC_SEQNS = (String[]) commandMap.get( "ATTC_SEQN" ); String[]
                     * ATTACH_FILE_PATHS = (String[]) commandMap.get( "ATTACH_FILE_PATH" ); String[] ATTACH_FILE_SIZES =
                     * (String[]) commandMap.get( "ATTACH_FILE_SIZE" ); String[] ATTACH_FILE_CONTENTS = (String[])
                     * commandMap.get( "ATTACH_FILE_CONTENT" ); param.put( "USER_ID", USER_IDS[i] ); param.put(
                     * "APPLY_WRITE_YMD", APPLY_WRITE_YMDS[i] ); param.put( "APPLY_WRITE_SEQ", APPLY_WRITE_SEQS[i] );
                     * param.put( "RGST_IDNT", RGST_IDNTS[i] ); param.put( "STAT_RSLT_CD", STAT_RSLT_CDS[i] );
                     * param.put( "LGMT_AMNT", LGMT_AMNTS[i] ); param.put( "LGMT_PLAC_NAME", LGMT_PLAC_NAMES[i] );
                     * param.put( "LGMT_GRAN", LGMT_GRANS[i] ); param.put( "LGMT_PERI", LGMT_PERIS[i] ); param.put(
                     * "WORKS_SEQN", WORKS_SEQNS[i] ); param.put( "STAT_RSLT_MEMO", STAT_RSLT_MEMOS[i] ); param.put(
                     * "LGMT_PLAC_NAME", LGMT_PLAC_NAMES[i] ); param.put( "LGMT_PLAC_NAME", LGMT_PLAC_NAMES[i] );
                     * param.put( "LGMT_PLAC_NAME", LGMT_PLAC_NAMES[i] ); param.put( "ATTC_SEQN", ATTC_SEQNS[i] );
                     * param.put( "ATTACH_FILE_PATH", ATTACH_FILE_PATHS[i] ); param.put( "ATTACH_FILE_SIZE",
                     * ATTACH_FILE_SIZES[i] ); param.put( "ATTACH_FILE_CONTENT", ATTACH_FILE_CONTENTS[i] ); String[]
                     * FILE_NAMES = (String[]) commandMap.get( "FILE_NAME" ); param.put( "FILE_NAME", FILE_NAMES[i] );
                     * String[] ATTACH_FILE_CONTENT0S = (String[]) commandMap.get( "ATTACH_FILE_CONTENT0" ); param.put(
                     * "ATTACH_FILE_CONTENT0", ATTACH_FILE_CONTENT0S[0] ); String[] REAL_FILE_NAMES = (String[])
                     * commandMap.get( "REAL_FILE_NAME" ); param.put( "REAL_FILE_NAME", REAL_FILE_NAMES[i] ); String[]
                     * FILE_PATHS = (String[]) commandMap.get( "FILE_PATH" ); param.put( "FILE_PATH", FILE_PATHS[i] );
                     * String[] FILE_SIZES = (String[]) commandMap.get( "FILE_SIZE" ); param.put( "FILE_SIZE",
                     * FILE_SIZES[i] ); String[] ATTACH_FILE_SIZE0S = (String[]) commandMap.get( "ATTACH_FILE_SIZE0" );
                     * param.put( "ATTACH_FILE_SIZE0", ATTACH_FILE_SIZE0S[0] );
                     */

                    String USER_ID = (String) commandMap.get("USER_ID");
                    String APPLY_WRITE_YMD = (String) commandMap.get("APPLY_WRITE_YMD");
                    String APPLY_WRITE_SEQ = (String) commandMap.get("APPLY_WRITE_SEQ");
                    String WORKS_SEQN = (String) commandMap.get("WORKS_SEQN");
                    String RECEIPT_NO = (String) commandMap.get("RECEIPT_NO");
                    String STAT_RSLT_CD = (String) commandMap.get("STAT_RSLT_CD");
                    String STAT_RSLT_MEMO = (String) commandMap.get("STAT_RSLT_MEMO");
                    String ATTC_SEQNS = (String) commandMap.get("ATTC_SEQN");
                    String LGMT_AMNT = (String) commandMap.get("LGMT_AMNT");
                    String LGMT_PLAC_NAME = (String) commandMap.get("LGMT_PLAC_NAME");
                    String LGMT_GRAN = (String) commandMap.get("LGMT_GRAN");
                    String LGMT_PERI = (String) commandMap.get("LGMT_PERI");
                    param.put("USER_ID", USER_ID);
                    param.put("APPLY_WRITE_YMD", APPLY_WRITE_YMD);
                    param.put("APPLY_WRITE_SEQ", APPLY_WRITE_SEQ);
                    param.put("WORKS_SEQN", WORKS_SEQN);
                    param.put("RECEIPT_NO", RECEIPT_NO);
                    param.put("STAT_RSLT_CD", STAT_RSLT_CD);
                    param.put("STAT_RSLT_MEMO", STAT_RSLT_MEMO);
                    param.put("ATTC_SEQN", ATTC_SEQNS);
                    param.put("LGMT_AMNT", LGMT_AMNT);
                    param.put("LGMT_PLAC_NAME", LGMT_PLAC_NAME);
                    param.put("LGMT_GRAN", LGMT_GRAN);
                    param.put("LGMT_PERI", LGMT_PERI);

                    /* 일단 주석처리 파일첨부 부분 보류(08.22) */
                    /*
                     * if( row_status.equals( "insert" ) == true ){ int attachSeqn = consoleCommonDao.getNewAttcSeqn();
                     * String fileName = FILE_NAMES[i]; if( fileName != null && !fileName.equals( "" ) ){ param.put(
                     * "ATTC_SEQN", attachSeqn ); String filePath = ""; String realFileNm = ""; String fileSize = "";
                     * byte[] file = null; Map upload = null; if( ATTACH_FILE_CONTENT0S[i] == null ||
                     * ATTACH_FILE_CONTENT0S[i].toString().equals( "" ) ){ filePath = FILE_PATHS[i]; realFileNm =
                     * REAL_FILE_NAMES[i]; fileSize = FILE_SIZES[i]; }else{ file = ATTACH_FILE_CONTENT0S[0].getBytes();
                     * upload = FileUtil.uploadMiFile( fileName, file ); filePath = upload.get( "FILE_PATH"
                     * ).toString(); realFileNm = upload.get( "REAL_FILE_NAME" ).toString(); fileSize =
                     * ATTACH_FILE_SIZE0S[0].toString(); } Map fileMap = new HashMap(); fileMap.put( "ATTC_SEQN",
                     * attachSeqn ); fileMap.put( "FILE_ATTC_CD", "ST" ); fileMap.put( "FILE_NAME", fileName );
                     * fileMap.put( "FILE_PATH", filePath); fileMap.put( "FILE_SIZE", fileSize );
                     * fileMap.put("REAL_FILE_NAME", realFileNm ); fileMap.put( "RGST_IDNT",param.get( "USER_ID" ) );
                     * fileMap.put( "MODI_IDNT",param.get( "USER_ID" ) ); fileMap.put( "APPLY_WRITE_YMD",param.get(
                     * "APPLY_WRITE_YMD" ) ); fileMap.put("APPLY_WRITE_SEQ", param.get( "APPLY_WRITE_SEQ" ) ); // 첨부파일
                     * 등록 fdcrAd06Dao.adminFileInsert( fileMap ); fdcrAd06Dao.adminStatAttcFileInsert( fileMap ); }
                     */

                    int attachSeqn = consoleCommonDao.getNewAttcSeqn();

                    Map<String, Object> fileInfo = new HashMap<String, Object>();
                    if (fileList != null && fileList.size() > 0) {

                         fileInfo = fileList.get(0);
                         param.put("ATTC_SEQN", attachSeqn);

                         Map fileMap = new HashMap();
                         fileMap.put("ATTC_SEQN", attachSeqn);
                         fileMap.put("FILE_ATTC_CD", "ST");
                         fileMap.put("FILE_NAME_CD", 99);
                         fileMap.put("FILE_NAME", fileInfo.get("F_fileName"));
                         fileMap.put("FILE_PATH", fileInfo.get("F_saveFilePath"));
                         fileMap.put("FILE_SIZE", fileInfo.get("F_filesize"));
                         fileMap.put("REAL_FILE_NAME", fileInfo.get("F_orgFileName"));
                         fileMap.put("RGST_IDNT", param.get("RGST_IDNT"));
                         fileMap.put("WORKS_ID", param.get("WORKS_ID"));

                         // 첨부파일 등록
                         fdcrAd06Dao.adminFileInsert(fileMap);
                         fdcrAd01Dao.statBord06FileInsert(fileMap);
                    }

                    // 이용승인신청 명세서 수정
                    fdcrAd06Dao.adminStatApplyWorksUpdate(param);
                    // 이용승인신청 명세서 상태변경내역 등록
                    fdcrAd06Dao.adminStatApplyWorksShisInsert(param);

                    // 승인, 거절 시 메일,sms 보내기
                    if (param.get("STAT_RSLT_CD").equals("2") || param.get("STAT_RSLT_CD").equals("3")) {
                         // 메일 보내기
                         Map memMap = new HashMap();
                         memMap.put("USER_IDNT", param.get("RGST_IDNT"));
                         List memList = consoleCommonDao.selectMemberInfo(memMap);
                         memMap = (Map) memList.get(0);

                         String statNm = "";
                         if (param.get("STAT_RSLT_CD").equals("2")) {
                              statNm = "승인";
                         } else if (param.get("STAT_RSLT_CD").equals("3")) {
                              statNm = "거절";
                         }

                         String host = Constants.MAIL_SERVER_IP; // smtp서버
                         String to = memMap.get("MAIL").toString(); // 수신EMAIL
                         String toName = memMap.get("USER_NAME").toString(); // 수신인
                         String subject = "[저작권찾기] 법정허락 이용신청 명세서 " + statNm;

                         String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 -
                         // 시스템
                         // 대표 메일
                         String fromName = Constants.SYSTEM_NAME;

                         if (memMap.get("MAIL_RECE_YSNO").equals("Y") && to != null && to != "") {

                              // ------------------------- mail 정보
                              // ----------------------------------//
                              MailInfo info = new MailInfo();
                              info.setFrom(from);
                              info.setFromName(fromName);
                              info.setHost(host);
                              info.setEmail(to);
                              info.setEmailName(toName);
                              info.setSubject(subject);

                              StringBuffer sb = new StringBuffer();

                              sb.append("				<tr>");
                              sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
                              sb.append("					<p style=\"font-weight:bold; background:url(" + kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
                                        + "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 법정허락 이용신청 명세서 " + statNm + " 정보</p>");
                              sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
                              sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자 이름 : " + toName
                                        + " </li>");
                              sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청번호 : "
                                        + param.get("APPLY_WRITE_YMD") + "-" + param.get("APPLY_WRITE_SEQ") + " </li>");
                              sb.append("						<li style=\"background:url(" + Constants.DOMAIN_HOME + "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : "
                                        + param.get("WORKS_TITL") + " </li>");
                              sb.append("					</ul>");
                              sb.append("					</td>");
                              sb.append("				</tr>");

                              info.setMessage(sb.toString());
                              info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));

                              MailManager manager = MailManager.getInstance();

                              info = manager.sendMail(info);
                              if (info.isSuccess()) {
                                   System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());
                              } else {
                                   System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
                              }

                         }

                         // sms 보내기
                         String smsTo = memMap.get("MOBL_PHON").toString();
                         String smsFrom = Constants.SYSTEM_TELEPHONE;

                         String smsMessage = "[저작권찾기] 법정허락 이용신청 명세서" + param.get("WORKS_TITL") + "가 " + statNm + " 되었습니다.";

                         if (memMap.get("SMS_RECE_YSNO") != null) {
                              if (memMap.get("SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {

                                   SMSManager smsManager = new SMSManager();

                                   System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
                              }
                         }
                    } // end email, sms
                      // }
               }
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;

     }

     /**
      * 법정허락 이용승인신청공고 게시판 등록 팝업 폼
      * 
      * @param commandMap
      * @return
      * @throws SQLException
      */
     public void fdcrAd06RegiForm1(Map<String, Object> commandMap) throws Exception {

          // DAO 호출
          List detailList = (List) fdcrAd01Dao.statBord01Detail(commandMap);
          List fileList = (List) fdcrAd01Dao.statBord01File(commandMap);
          List objectList = (List) fdcrAd01Dao.statBord01Object(commandMap);
          List objectFileList = (List) fdcrAd01Dao.statBord01ObjectFile(commandMap);
          // 보완요청 LIST
          List suplList = (List) fdcrAd01Dao.selectBordSuplItemList(commandMap);

          commandMap.put("detailList", detailList);
          commandMap.put("fileList", fileList);
          commandMap.put("objectList", objectList);
          commandMap.put("objectFileList", objectFileList);
          commandMap.put("suplList", suplList);
     }

     /**
      * 법정허락 이용승인신청공고 게시판 등록
      * 
      * @param commandMap
      * @throws SQLException
      */
     public boolean fdcrAd06Regi1(Map<String, Object> commandMap, ArrayList<Map<String, Object>> fileList) throws Exception {

          boolean result = false;
          try {
               // Dataset ds_file = getDataset("ds_file");

               // File Insert
               /*
                * for( int i=0;i<ds_file.getRowCount();i++ ) { row_status = ds_file.getRowStatus(i); Map map =
                * getMap(ds_file, i ); if(row_status.equals("insert") == true ) { int attachSeqn
                * =commonDao.getNewAttcSeqn(); int bordSeqn = adminStatBoardDao.getMaxBordSeqn(); Map upload=null;
                * byte[] file = null; file = ds_file.getColumn(i, "CLIENT_FILE").getBinary(); String fileName =
                * ds_file.getColumnAsString(i, "REAL_FILE_NAME"); upload = FileUtil.uploadMiFile2(fileName, file);
                * map.put("BORD_SEQN", bordSeqn); map.put("ATTC_SEQN", attachSeqn); map.put("REAL_FILE_NAME",
                * upload.get("REAL_FILE_NAME")); map.put("FILE_PATH", upload.get("FILE_PATH"));
                * map.put("FILE_NAME", fileName); System.out.println("########################################");
                * System.out.println("########################################");
                * System.out.println("########################################"); System.out.println(fileName);
                * System.out.println("########################################");
                * System.out.println("########################################");
                * System.out.println("########################################");
                * adminStatBoardDao.mlBord02FileInsert(map); //첨부파일 등록 adminStatBoardDao.statBord02FileInsert(map);
                * //공고게시판 첨부파일 등록 } }
                */

               String[] CLIENT_FILES = (String[]) commandMap.get("CLIENT_FILE");
               int attachSeqn = consoleCommonDao.getNewAttcSeqn();

               Map<String, Object> fileInfo = new HashMap<String, Object>();
               if (fileList != null && fileList.size() > 0) {
                    fileInfo = fileList.get(0);
                    commandMap.put("ATTC_SEQN", attachSeqn);

                    Map fileMap = new HashMap();
                    Map upload = null;
                    byte[] file = null;
                    file = (byte[]) fileMap.get(CLIENT_FILES[attachSeqn]);
                    String fileName = (String) fileMap.get("REAL_FILE_NAME");
                    upload = FileUtil.uploadMiFile2(fileName, file);

                    fileMap.put("ATTC_SEQN", attachSeqn);
                    fileMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
                    fileMap.put("FILE_PATH", upload.get("FILE_PATH"));
                    fileMap.put("FILE_NAME", fileName);

                    fdcrAd01Dao.mlBord02FileInsert(fileMap); // 첨부파일 등록
                    fdcrAd01Dao.statBord02FileInsert(fileMap); // 공고게시판 첨부파일 등록
               }

               // 공고내용 INSERT
               fdcrAd01Dao.statBord01Insert(commandMap); // 공고게시판 등록
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     /**
      * 법정허락 이용승인신청공고 게시판 수정
      * 
      * @param commandMap
      * @throws SQLException
      */
     public boolean fdcrAd06Modi1(Map<String, Object> commandMap) throws Exception {

          boolean result = false;
          try {
               fdcrAd01Dao.statBord01Update(commandMap); // 공고게시판 수정

               // File update
               /*
                * for( int i=0;i<ds_file.getRowCount();i++ ) { Map map = getMap(ds_file, i ); row_status =
                * ds_file.getRowStatus(i); Map upload=null; byte[] file = null; file = ds_file.getColumn(i,
                * "CLIENT_FILE").getBinary(); if(file !=null){ String fileName = ds_file.getColumnAsString(i,
                * "REAL_FILE_NAME"); int attachSeqn = commonDao.getNewAttcSeqn(); upload =
                * FileUtil.uploadMiFile2(fileName, file); map.put("ATTC_SEQN", attachSeqn);
                * map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME")); map.put("FILE_PATH",
                * upload.get("FILE_PATH")); map.put("FILE_NAME", fileName);
                * adminStatBoardDao.mlBord02FileInsert(map); //첨부파일 등록 adminStatBoardDao.statBord02FileInsert(map);
                * //공고게시판 첨부파일 등록 } } //FILE DELETE처리 for( int i = 0 ; i< ds_file.getDeleteRowCount() ; i++ ) { Map
                * deleteMap = getDeleteMap(ds_file, i ); adminStatBoardDao.statBord02FileDelete(deleteMap); String
                * fileName = ds_file.getDeleteColumn(i, "REAL_FILE_NAME").toString(); try { File file = new
                * File((String)deleteMap.get("FILE_PATH"), fileName); if (file.exists()) { file.delete(); } } catch
                * (Exception e) { e.printStackTrace(); } }
                */
               fdcrAd01Dao.statBord02Modi(commandMap); // 수정시 modi_dttm,modi_idnt
               // 업데이트
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     /**
      * 법정허락 이용승인공고 게시판 등록 팝업 폼
      * 
      * @param commandMap
      * @return
      * @throws SQLException
      */
     public void fdcrAd06RegiForm2(Map<String, Object> commandMap) throws Exception {

          // DAO호출
          List detailList = (List) fdcrAd01Dao.statBordDetail(commandMap);
          List fileList = (List) fdcrAd01Dao.statBord01File(commandMap);
          // 보완요청 LIST
          List suplList = (List) fdcrAd01Dao.selectBordSuplItemList(commandMap);

          commandMap.put("ds_list", detailList);
          commandMap.put("ds_file", fileList);
          commandMap.put("ds_supl_list", suplList);

     }

     /**
      * 법정허락 이용승인공고 게시판 등록
      * 
      * @param commandMap
      * @throws SQLException
      */
     public boolean fdcrAd06Regi2(Map<String, Object> commandMap, ArrayList<Map<String, Object>> fileList) throws Exception {

          boolean result = false;
          try {
               // Dataset ds_file = getDataset("ds_file");
               // File Insert

               /*
                * for( int i=0;i<ds_file.getRowCount();i++ ) { Map map = getMap(ds_file, i ); row_status =
                * ds_file.getRowStatus(i); if(row_status.equals("insert") == true ) { int attachSeqn =
                * commonDao.getNewAttcSeqn(); int bordSeqn = adminStatBoardDao.getMaxBordSeqn(); Map upload=null;
                * byte[] file = null; file = ds_file.getColumn(i, "CLIENT_FILE").getBinary();
                * System.out.println("#############################");
                * System.out.println("#############################"); System.out.println(file); String fileName =
                * ds_file.getColumnAsString(i, "REAL_FILE_NAME"); System.out.println(fileName); upload =
                * FileUtil.uploadMiFile2(fileName, file); map.put("BORD_SEQN",bordSeqn); map.put("ATTC_SEQN",
                * attachSeqn); map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME")); map.put("FILE_PATH",
                * upload.get("FILE_PATH")); map.put("FILE_NAME", fileName);
                * adminStatBoardDao.mlBord02FileInsert(map); //첨부파일 등록 adminStatBoardDao.statBord02FileInsert(map);
                * //공고게시판 첨부파일 등록 } }
                */
               String[] CLIENT_FILES = (String[]) commandMap.get("CLIENT_FILE");
               int attachSeqn = consoleCommonDao.getNewAttcSeqn();

               Map<String, Object> fileInfo = new HashMap<String, Object>();
               if (fileList != null && fileList.size() > 0) {
                    fileInfo = fileList.get(0);
                    commandMap.put("ATTC_SEQN", attachSeqn);

                    Map fileMap = new HashMap();
                    Map upload = null;
                    byte[] file = null;
                    file = (byte[]) fileMap.get(CLIENT_FILES[attachSeqn]);
                    String fileName = (String) fileMap.get("REAL_FILE_NAME");
                    upload = FileUtil.uploadMiFile2(fileName, file);

                    fileMap.put("ATTC_SEQN", attachSeqn);
                    fileMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
                    fileMap.put("FILE_PATH", upload.get("FILE_PATH"));
                    fileMap.put("FILE_NAME", fileName);

                    fdcrAd01Dao.mlBord02FileInsert(fileMap); // 첨부파일 등록
                    fdcrAd01Dao.statBord02FileInsert(fileMap); // 공고게시판 첨부파일 등록
               }

               // 공고내용 INSERT
               fdcrAd01Dao.statBord02Regi(commandMap); // 공고게시판 등록
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     /**
      * 법정허락 이용승인공고 게시판 수정
      * 
      * @param commandMap
      * @throws SQLException
      */
     public boolean fdcrAd06Modi2(Map<String, Object> commandMap) throws Exception {

          boolean result = false;
          try {
               // Dataset ds_file = getDataset("ds_file");

               // 공고내용 INSERT
               fdcrAd01Dao.statBord02Regi(commandMap); // 공고게시판 등록

               // File Insert
               /*
                * for( int i=0;i<ds_file.getRowCount();i++ ) { Map map = getMap(ds_file, i ); row_status =
                * ds_file.getRowStatus(i); if( row_status.equals("insert") == true ) { int attachSeqn =
                * commonDao.getNewAttcSeqn(); int bordSeqn = adminStatBoardDao.getMaxBordSeqn(); Map upload=null;
                * byte[] file = null; file = ds_file.getColumn(i, "CLIENT_FILE").getBinary();
                * System.out.println("#############################");
                * System.out.println("#############################"); System.out.println(file); String fileName =
                * ds_file.getColumnAsString(i, "REAL_FILE_NAME"); System.out.println(fileName); upload =
                * FileUtil.uploadMiFile2(fileName, file); map.put("BORD_SEQN", bordSeqn); map.put("ATTC_SEQN",
                * attachSeqn); map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME")); map.put("FILE_PATH",
                * upload.get("FILE_PATH")); map.put("FILE_NAME", fileName);
                * adminStatBoardDao.mlBord02FileInsert(map); //첨부파일 등록 adminStatBoardDao.statBord02FileInsert(map);
                * //공고게시판 첨부파일 등록 } } //FILE DELETE처리 for( int i = 0 ; i< ds_file.getDeleteRowCount() ; i++ ) { Map
                * deleteMap = getDeleteMap(ds_file, i ); adminStatBoardDao.statBord02FileDelete(deleteMap); String
                * fileName = ds_file.getDeleteColumn(i, "REAL_FILE_NAME").toString(); try { File file = new
                * File((String)deleteMap.get("FILE_PATH"), fileName); if (file.exists()) { file.delete(); } } catch
                * (Exception e) { e.printStackTrace(); } }
                */
               fdcrAd01Dao.statBord02Modi(commandMap); // 수정시 modi_dttm,modi_idnt
               // 업데이트
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

}
