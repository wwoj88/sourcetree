package kr.or.copyright.mls.rght.dao;

import java.util.List;
import java.util.Map;
 
public interface RghtDao {

//	public int updateBoard(Board board);
	      
	// 내권리찾기 음악리스트 조회 
	public List muscRghtList(Map map);
	public List muscRghtListCount(Map map);
	
	 
	// 내권리찾기(음악상세조회)	
	public List muscRghtDetail(Map map);
	
	
	// 내권리찾기 어문리스트 조회 
	public List bookRghtList(Map map);
	public List bookRghtListCount(Map map);		

	// 내권리찾기(어문상세조회)	
	public List bookRghtDetail(Map map);
		
	
	// 내권리찾기 이미지리스트 조회 
	public List imgeRghtList(Map map);
	public List imgeRghtListCount(Map map);
	
	
	// 내권리찾기 저작권자리스트 음악 	
	public List muscList(Map map);
	public List muscListCount(Map map);		
	
	// 내권리찾기 저작권자리스트 어
	public List bookList(Map map);	
	public List bookListCount(Map map);
	
	// 전체 처리결과
	public List rghtTotalDealStat(Map map);
	
	//내권리찾기 상세조회-저작물리스트
	public List rghtTempList(Map map);
	
	//내권리찾기 상세조회
	public List rghtDetlList(Map map);
	
	//내권리찾기 접수 PK값
	public int prpsMastKey();
	
	//내권리찾기 신청 master 입력 
	public void prpsMasterInsert(Map map);
	
	//내권리찾기 신청 master 수정 
	public void prpsMasterUpdate(Map map);	
	
	//내권리찾기 신청 Master 삭제후 입력 
	public void prpsMasterDelete(Map map);
	
	//내권리찾기 신청 DETAIL 삭제후 입력 
	public void prpsDetailDelete(Map map);	
	
	
	public void prpsDetail201Insert(Map map);
	public void prpsDetail202Insert(Map map);	
	public void prpsDetail203Insert(Map map);
	public void prpsDetail204Insert(Map map);
	public void prpsDetail205Insert(Map map);	

	public List rghtDetlDescList(Map map);
	
	
	public List rghtFileList(Map map);
	
	public void rghtFileInsert(Map map);
	
	public void rghtFileDelete(Map map);
	
	// 최대처리결과값
	public List getMaxDealStat(Map map);
	
   //	 강명표 추가
	public void insertConnInfo(Map map);
}
