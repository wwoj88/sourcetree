package kr.or.copyright.mls.statBord.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.extend.RightDAOImpl;
import kr.or.copyright.mls.statBord.model.AnucBord;
import kr.or.copyright.mls.statBord.model.AnucBordAttcFile;
import kr.or.copyright.mls.statBord.model.AnucBordFile;
import kr.or.copyright.mls.statBord.model.AnucBordObjc;
import kr.or.copyright.mls.statBord.model.AnucBordObjcFile;
import kr.or.copyright.mls.statBord.model.AnucBordSupl;
import kr.or.copyright.mls.statBord.model.AnucNonAttcFile;
import kr.or.copyright.mls.statBord.model.Code;

import org.springframework.stereotype.Repository;


@Repository("AnucBordDao")
public class AnucBordSqlMapDao extends RightDAOImpl implements AnucBordDao{
	@SuppressWarnings("unchecked")
	public List<AnucBord> SelTest(){
		return getSqlMapClientTemplate().queryForList("AnucBord.SelTest");
	}
	public List<Code> getCodeList(Map params){
		return (List<Code>)getSqlMapClientTemplate().queryForList("AnucBord.codeListNonAnuc",params);
	}
	public void insertAnuc(AnucBord anucBord){	//공고게시판 등록
		getSqlMapClientTemplate().insert("AnucBord.insertAnuc", anucBord);
		
	}
	public void fileInsertAnuc1(AnucBordFile anucBordFile){
		getSqlMapClientTemplate().insert("AnucBord.fileInsertAnuc1", anucBordFile);
	}
	public void fileInsertAnuc2(AnucBordAttcFile anucBordAttcFile){
		getSqlMapClientTemplate().insert("AnucBord.fileInsertAnuc2",anucBordAttcFile);
	}
	public int getBordSeqn(){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AnucBord.getBordSeqn"));
	}
	@SuppressWarnings("unchecked")
	public List<AnucBordFile> fileSelectAnuc(int bordSeqn){
		return getSqlMapClientTemplate().queryForList("AnucBord.fileSelectAnuc", bordSeqn);
	}
	@SuppressWarnings("unchecked")
	public List<AnucBordFile> fileSelectObjc(int statObjcId){
		return getSqlMapClientTemplate().queryForList("AnucBord.fileSelectObjc", statObjcId);
	}
	public void fileInsertObjc(AnucBordObjcFile anucBordObjcFile){
		getSqlMapClientTemplate().insert("AnucBord.fileInsertObjc", anucBordObjcFile);
	}
	public int deleteAnuc(int bordSeqn){	
		return getSqlMapClientTemplate().update("AnucBord.deleteAnuc", bordSeqn);
	}
	@SuppressWarnings("unchecked")
	public List<AnucBord> selectAnuc(AnucBord anucBord){		//공고게시판 리스트
		return getSqlMapClientTemplate().queryForList("AnucBord.selectAnuc", anucBord); 
		
	}
	
	@SuppressWarnings("unchecked")
	public List<AnucBord> detailAnuc(int bordSeqn){		//공고게시판 상세보기
		return getSqlMapClientTemplate().queryForList("AnucBord.detailAnuc", bordSeqn);
	}
	
	public void updateInqrContAnuc(int bordSeqn){		//조회수 증가
		getSqlMapClientTemplate().update("AnucBord.updateInqrContAnuc", bordSeqn);
	}
	
	@SuppressWarnings("unchecked")
	public List<AnucBord> findAnuc(AnucBord anucBord){	//공고게시판 검색
		return getSqlMapClientTemplate().queryForList("AnucBord.findAnuc", anucBord);
	}
	@SuppressWarnings("unchecked")
	public List<AnucBord> findAnucAll(AnucBord anucBord){ //전체검색
		return getSqlMapClientTemplate().queryForList("AnucBord.findAnucAll", anucBord);
	}
	@SuppressWarnings("unchecked")
	public List<AnucBord> findGenreAnuc(AnucBord anucBord){
		return getSqlMapClientTemplate().queryForList("AnucBord.findGenreAnuc", anucBord);
	}
	public int findCount(AnucBord anucBord){  //검색 갯수
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AnucBord.findCount", anucBord));
	}
	public int countAnuc(AnucBord anucBord){ //공고게시판 갯수
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AnucBord.countAnuc", anucBord));
	}
	public int countAnucObjc(int bordSeqn){ //이의제기 갯수
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AnucBord.countAnucObjc", bordSeqn));
	}
	public void insertAnucObjc(AnucBordObjc anucBordObjc){ //이의제기 등록
		getSqlMapClientTemplate().insert("AnucBord.insertAnucObjc", anucBordObjc);
	}
	public int updateAnucBordObjcYn(int bordSeqn){
		return getSqlMapClientTemplate().update("AnucBord.updateAnucBordObjcYn", bordSeqn);
	}
	@SuppressWarnings("unchecked")
	public List<AnucBordObjc> selectAnucObjc(int bordSeqn,int bordCd){ //이의제기 목록
		Map<Object, Object> param = new HashMap<Object, Object>();
		param.put("bordSeqn", bordSeqn);
		param.put("bordCd", bordCd);
		return getSqlMapClientTemplate().queryForList("AnucBord.selectAnucObjc",param);
	}
	public List<AnucBordObjc> selectAnucObjcShis(int statObjcId){
		return  getSqlMapClientTemplate().queryForList("AnucBord.selectAnucObjcShis", statObjcId);
	}
	//이의제기 삭제 3종세트
	public int deleteAnucBordObjcNon(int statObjcId){
		return getSqlMapClientTemplate().delete("AnucBord.deleteAnucBordObjcNon", statObjcId);
	}
	public int deleteAnucBordObjcStat(int statObjcId){
		return getSqlMapClientTemplate().delete("AnucBord.deleteAnucBordObjcStat", statObjcId);
	}
	public int deleteAnucBordObjc(int statObjcId){
		return getSqlMapClientTemplate().delete("AnucBord.deleteAnucBordObjc", statObjcId);
	}
	//이의제기 삭제 3종세트
	//이의제기 파일 삭제 
	public List getObjcFileAttc(int statObjcId){
		return getSqlMapClientTemplate().queryForList("AnucBord.getObjcFileAttc", statObjcId);
	}
	public int deleteObjcFile(int attcSeqn){
		return getSqlMapClientTemplate().delete("AnucBord.deleteObjcFile", attcSeqn);
	}
	public int deleteObjcAttcFile(int attcSeqn){
		return getSqlMapClientTemplate().delete("AnucBord.deleteObjcAttcFile", attcSeqn);
	}
	//이의제기 파일 삭제 
	public int updateAnucObjc(AnucBordObjc anucBordObjc){
		return getSqlMapClientTemplate().update("AnucBord.updateAnucObjc",anucBordObjc);
	}
	public void updateAnucBordObjcYnDelete(int bordSeqn){
		getSqlMapClientTemplate().update("AnucBord.updateAnucBordObjcYnDelete", bordSeqn);
	}
	
	public void updateAnucBord(AnucBord anucBord){
		getSqlMapClientTemplate().update("AnucBord.updateAnucBord", anucBord);
	}
	public void deleteAttcFile(int attcSeqn){
		getSqlMapClientTemplate().delete("AnucBord.deleteAttcFile", attcSeqn);
	}
	public void deleteAnucBord(int bordSeqn){
		getSqlMapClientTemplate().update("AnucBord.deleteAnucBord", bordSeqn);
	}
	//---------------------- 거소불명 --------------------------------
	public int getWorksId(){
		return (Integer) getSqlMapClientTemplate().queryForObject("AnucBord.getWorksId");
	}
	@SuppressWarnings("unchecked")
	public List<AnucBord> selectNonAnuc(AnucBord anucBord){
		return getSqlMapClientTemplate().queryForList("AnucBord.selectNonAnuc", anucBord);
	}
	public void insertNonAnuc(AnucBord anucBord){
		getSqlMapClientTemplate().insert("AnucBord.insertNonAnuc", anucBord);
	}
	public void insertNonAnuc2(AnucBord anucBord){
		getSqlMapClientTemplate().insert("AnucBord.insertNonAnuc2", anucBord);
	}
	public void insertNonAnuc3(AnucBord anucBord){
		getSqlMapClientTemplate().insert("AnucBord.insertNonAnuc3", anucBord);
	}
	public void insertCopthodr(AnucBord anucBord){
		getSqlMapClientTemplate().insert("AnucBord.insertCopthodr", anucBord);
	}
	public void fileInsertNon(AnucBordFile anucBordFile){
		getSqlMapClientTemplate().insert("AnucBord.fileInsertNon",anucBordFile);
	}
	public void fileInsertNonAnuc(AnucNonAttcFile anucNonAttcFile){
		getSqlMapClientTemplate().insert("AnucBord.fileInsertNonAnuc", anucNonAttcFile);
	}
	@SuppressWarnings("unchecked")
	public List<AnucBord> detailNonAnuc(int worksId){		
		return getSqlMapClientTemplate().queryForList("AnucBord.detailNonAnuc", worksId); 
	}
	@SuppressWarnings("unchecked")
	public List<AnucBord> detailNonCopthodr(AnucBord anucBord){
		return getSqlMapClientTemplate().queryForList("AnucBord.detailNonCopthodr", anucBord);
	}
	@SuppressWarnings("unchecked")
	public List<AnucBord> detailGetMaker(int worksId){
		return getSqlMapClientTemplate().queryForList("AnucBord.detailGetMaker", worksId);
	}
	@SuppressWarnings("unchecked")
	public List<AnucBordFile> fileSelectNonAnuc(int worksId){
		return getSqlMapClientTemplate().queryForList("AnucBord.fileSelectNonAnuc", worksId);
	}
	public List<AnucBord> NonResult(int worksId){
		return getSqlMapClientTemplate().queryForList("AnucBord.NonResult", worksId);
	}
	public void updateNonAnuc(AnucBord anucBord){
		getSqlMapClientTemplate().update("AnucBord.updateNonAnuc", anucBord);
	}
	public void updateStatWorks(AnucBord anucBord){
		getSqlMapClientTemplate().update("AnucBord.updateStatWorks", anucBord);
	}
	public void updateNonCoptHodr(AnucBord anucBord){
		getSqlMapClientTemplate().update("AnucBord.updateNonCoptHodr", anucBord);
	}
	
	public List getNonFileAttc(int worksId){
		return getSqlMapClientTemplate().queryForList("AnucBord.getNonFileAttc", worksId);
	}
	public void deleteNonAnuc(int worksId){
		getSqlMapClientTemplate().update("AnucBord.deleteNonAnuc", worksId);
	}
	public int countNonAnuc(String rgstIdnt){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AnucBord.countNonAnuc", rgstIdnt));
	}
	public int deleteNonAttcFile(int attcSeqn){
		return getSqlMapClientTemplate().delete("AnucBord.deleteNonAttcFile", attcSeqn);
	}
	public int deletecoptHodr(int worksId){
		return getSqlMapClientTemplate().delete("AnucBord.deletecoptHodr", worksId);
	}
	@SuppressWarnings("unchecked")
	public List<AnucBord> findNonAnuc(Map param){
		return getSqlMapClientTemplate().queryForList("AnucBord.findNonAnuc", param);
	}
	public int countFindNonAnuc(Map param){
		return  Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AnucBord.countFindNonAnuc", param));
	}
//---------------------법정허락 이용승인 신청 ----------------
	public void insertStatObjc(AnucBordObjc anucBordObjc){
		getSqlMapClientTemplate().insert("AnucBord.insertStatObjc", anucBordObjc);
	}
	public int countStatObjc(int worksId){ //이의제기 갯수
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AnucBord.countStatObjc", worksId));
	}
	public List<AnucBordObjc> selectStatObjc(int worksId){ //이의제기 목록
		return getSqlMapClientTemplate().queryForList("AnucBord.selectStatObjc",worksId);
	} 
	public int deleteStatObjc(int statObjcId){
		return getSqlMapClientTemplate().delete("AnucBord.deleteStatObjc", statObjcId);
	}
	public void updateStatWorksObjcYnDelete(int worksId){
		getSqlMapClientTemplate().update("AnucBord.updateStatWorksObjcYnDelete", worksId);
	}
	public void updateStatWorksObjcYn(int worksId){
		getSqlMapClientTemplate().update("AnucBord.updateStatWorksObjcYn", worksId);
	}
	public List StatObjcPop(AnucBord anucBord){
		return getSqlMapClientTemplate().queryForList("AnucBord.StatObjcPop", anucBord);
	}
	// 보상금 조회공고, 저작권자 조회공고 보완내역 불러오기
	public List<AnucBordSupl> selectBordSuplItemList(int bordSeqn, int bordCd) {
		Map<Object, Object> param = new HashMap<Object, Object>();
		param.put("bordSeqn", bordSeqn);
		param.put("bordCd", bordCd);
		if(bordCd==5){
			param.put("bigCode", 89);			
		}
		else{
			param.put("bigCode", 88);
		}
		
		return getSqlMapClientTemplate().queryForList("AnucBord.selectBordSuplItemList",param);
		
	}
	
	// 상당한노력신청 - 저작권자 조회공고 보완목록 불러오기
	public List<AnucBordSupl> selectWorksSuplItemList(int worksId) { 
		return getSqlMapClientTemplate().queryForList("AnucBord.selectWorksSuplItemList",worksId);
	}
	
	
	public int statBo07TotalCount(Map dataMap){
		return (Integer) getSqlMapClientTemplate().queryForObject("AnucBord.statBo07TotalCount",dataMap);
	}
	//기승인 법정허락 저작물 리스트
	public List statBo07List(Map dataMap){
		return getSqlMapClientTemplate().queryForList("AnucBord.selectStatBo07List", dataMap);
	}

}
