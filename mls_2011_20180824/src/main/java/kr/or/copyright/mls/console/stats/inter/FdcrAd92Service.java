package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;


public interface FdcrAd92Service{
	/**
	 * 보상금신청 및 처리통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd92List1( Map<String, Object> commandMap ) throws Exception;
}
