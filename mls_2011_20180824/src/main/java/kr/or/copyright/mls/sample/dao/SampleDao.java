package kr.or.copyright.mls.sample.dao;

import java.util.List;
import java.util.Map;

public interface SampleDao {

//	public int updateBoard(Board board);
	
	public List sampleList(Map map);

	public void sampleInsert(Map map);

	public void sampleUpdate(Map map);
	
	public void sampleDelete(Map map);	
	
	
	public List musicList(Map map);
	
	public List musicListCount(Map map);	
	
}
