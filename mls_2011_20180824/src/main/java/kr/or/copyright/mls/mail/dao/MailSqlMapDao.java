package kr.or.copyright.mls.mail.dao;

import kr.or.copyright.mls.common.extend.RightDAOImpl;
import kr.or.copyright.mls.mail.model.Mail;

import org.springframework.stereotype.Repository;

@Repository("MailDao")
public class MailSqlMapDao extends RightDAOImpl implements MailDao {
	
	// insert 수신거부목록
	public void insertMailRejcList(Mail mail) {
			getSqlMapClientTemplate().insert("Mail.insertMailRejcList", mail);
	}
	
	// delete 수신거부목록
	public void deleteMailRejcList(Mail mail) {
			getSqlMapClientTemplate().insert("Mail.deleteMailRejcList", mail);
	}
	public void updateEmailRecv(Mail mail) {
		getSqlMapClientTemplate().update("Mail.updateEmailRecv", mail);
		
	}
}
