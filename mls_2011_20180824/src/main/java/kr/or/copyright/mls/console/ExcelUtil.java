package kr.or.copyright.mls.console;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExcelUtil {

     private static final Logger LOGGER = LoggerFactory.getLogger(ExcelUtil.class);
     private static final int EXCEL_TYPE_HSSF = 1;
     private static final int EXCEL_TYPE_XSSF = 2;
     public static final int RETURN_TYPE_STRING = 1;
     public static final int RETURN_TYPE_OBJECT = 2;

     public static int getExcelType(String filename) {

          int type = 0;


          filename = filename.toUpperCase();
           System.out.println( "filename : " + filename );

          if ((filename.endsWith(".XLS"))) {
               System.out.println("HSSF : " + filename);
               type = EXCEL_TYPE_HSSF;
          } else if ((filename.endsWith(".XLSX"))) {
               System.out.println("XSSF : " + filename);
               type = EXCEL_TYPE_XSSF;
          }

          /*
           * FileInputStream fin = null; XSSFWorkbook xssfWorkbook = null;
           * 
           * try {
           * 
           * fin = new FileInputStream(filename); xssfWorkbook = new XSSFWorkbook(fin); type =
           * EXCEL_TYPE_XSSF; }catch(Exception e){ e.printStackTrace(); }finally{ try { if (xssfWorkbook !=
           * null) { xssfWorkbook = null; } if (fin != null) { fin.close(); } } catch (IOException e) {
           * e.printStackTrace(); } };
           * 
           * 
           * if( type == 0){
           * 
           * 
           * fin = null; HSSFWorkbook hssfWorkbook = null; try { fin = new FileInputStream(filename);
           * hssfWorkbook = new HSSFWorkbook(fin); type = EXCEL_TYPE_HSSF; }catch(Exception e){
           * e.printStackTrace(); }finally{ try { if (hssfWorkbook != null) { hssfWorkbook = null; } if (fin
           * != null) { fin.close(); } } catch (IOException e) { e.printStackTrace(); } }; }
           */

          return type;
     }

     public static void read(String filename) {

          int excelType = getExcelType(filename);
          // System.out.println( "filename : " + filename );
          // System.out.println( "excelType : " + excelType );
          switch (excelType) {
               case EXCEL_TYPE_HSSF:
                    PoiHssfUtil.read(filename);
                    break;
               case EXCEL_TYPE_XSSF:
                    PoiXssfUtil.read(filename);
                    break;
          }
     }

     public static ArrayList<ArrayList<Map<String, Object>>> get(String filename, int returntype) {

          ArrayList<ArrayList<Map<String, Object>>> excelDataList = new ArrayList<ArrayList<Map<String, Object>>>();
          int excelType = getExcelType(filename);
          // System.out.println("excelType : " + excelType );
          // System.out.println( "EXCEL_TYPE_HSSF :" + EXCEL_TYPE_HSSF );
          // System.out.println( "EXCEL_TYPE_XSSF :" + EXCEL_TYPE_XSSF );
          switch (excelType) {
               case EXCEL_TYPE_HSSF:
                    excelDataList = PoiHssfUtil.getData(filename, returntype);
                    break;
               case EXCEL_TYPE_XSSF:
                    excelDataList = PoiXssfUtil.getData(filename, returntype);
                    break;
          }
          // System.out.println( excelDataList );
          return excelDataList;
     }

     public static void main(String[] args) throws Exception {

          ArrayList<ArrayList<Map<String, Object>>> excelDataList = ExcelUtil.get("C:\\project\\excel\\test.xlsx", RETURN_TYPE_STRING);
          ArrayList<Map<String, Object>> excelDataList1 = excelDataList.get(0);

          /* 반복문 시작 */
          for (int i = 3; i < 11; i++) {

               Map<String, Object> excelDataList2 = excelDataList1.get(i);
               // System.out.println("excelDataList의 행:"+excelDataList1.size());
               // System.out.println("excelDataList의 열:"+excelDataList2.size());

               Set key = excelDataList2.keySet();
               for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                    String keyName = (String) iterator.next();
                    String valueName = (String) excelDataList2.get(keyName);
                    // System.out.println(keyName +" = " +valueName);
               }
          }
          /* 반복문 끝 */

     }
}
