package kr.or.copyright.mls.common.service;

import java.util.List;
import java.util.Map;

public interface CommonService {
	
	// 유저정보조회 
	public void getUserInfo() throws Exception;

	// 첨부파일 Main(ML_FILE) 신규SEQ 조회
	public int getNewAttcSeqn();
	
	public List getMgntMail(Map params);
	
}
