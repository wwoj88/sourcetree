package kr.or.copyright.mls.console;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CommonController extends DefaultController{

	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;

	private static final Logger logger = LoggerFactory.getLogger( CommonController.class );

	/**
	 * 로그인페이지
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/common/loginPage.page" )
	public String loginPage( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		System.out.println(  "loginPage start :?" +ConsoleLoginUser.isLogin());

		if(ConsoleLoginUser.isLogin()){
			return "redirect:/console/main/main.page";
		}else{
			return "common/login.tiles";
		}
	}
	
	/**
	 * 로그인
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/common/login.page" )
	public String login( ModelMap model,		Map<String, Object> commandMap,		HttpServletRequest request,		HttpServletResponse response ) throws Exception{
		logger.debug( "login start" );
		//System.out.println( "console login call" );
		String ip =EgovWebUtil.getClientIP( request );
		ip = ip.replace(".", "");
		
/*		ip = ip.substring(0,7);
		System.out.println(ip);
		if(!ip.equals("2192501") && !ip.equals("1921687")  && !ip.equals("1921686")&& !ip.equals("1921688") && !ip.equals("1921689") && !ip.equals("1921684") && !ip.equals("2222314")) {
		     System.out.println("!@!@#!@#");
		     return returnUrl(model, "저작권위탁관리업 시스템을 이용해주요","/console/common/loginPage.page");
		}

*/
		
		
		commandMap.put( "ip", EgovWebUtil.getClientIP( request ) );
	  System.out.println("ip  EgovWebUtil.getClientIP( request ) : " + EgovWebUtil.getClientIP( request ) );
		consoleCommonService.login( commandMap );
		boolean isValid = (Boolean)commandMap.get( "isValid" );
		String msg = (String)commandMap.get( "msg" );
		//System.out.println( "isValid : "+isValid );
		if(isValid){
			Map<String,Object> userInfo = (Map<String,Object>)commandMap.get( "userInfo" );
			
			//System.out.println( "userInfo : "+userInfo );
			request.getSession().setAttribute( "CONSOLE_USER", userInfo );
			//System.out.println( " ConsoleLoginUser.isLogin()  : "+ConsoleLoginUser.isLogin()   );
			return returnUrl(model, msg,"/console/main/main.page");
	/*		return  "main/main.tiles";*/
		}else{
			System.out.println( "userInfotest : " );
			return returnUrl(model, msg,"/console/common/loginPage.page");
		}
		
	}
	
	
	/**
	 * 로그아웃
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/common/logout.page" )
	public String logout( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "logout start" );
		ConsoleLoginUser.logout();
		return returnUrl(model, "","/console/common/loginPage.page");
	}
	
	/**
	 * 관리자 메인
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/main/main.page" )
		public String main( ModelMap model,		Map<String, Object> commandMap,		HttpServletRequest request,		HttpServletResponse response ) throws Exception{
		logger.debug( "main start" );
		System.out.println( "MAinSTART" );
		return "main/main.tiles";
	}
	
	/**
	 * 공통기능 <br/>
	 * 회원 정보 조회 팝업 : /console/common/memberInfo.page
	 */

	/**
	 * 회원 정보 조회 팝업
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/common/memberInfo.page" )
	public String memberInfo( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		consoleCommonService.memberInfo( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		return "test";
	}
	
	/**
	 * 기관/단체 조회 팝업
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/common/trstOrgnCoNameList.page" )
	public String trstOrgnCoNameList( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		consoleCommonService.trstOrgnCoNameList( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		return "test";
	}
	
}
