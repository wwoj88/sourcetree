package kr.or.copyright.mls.inmtPrps.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.inmtPrps.dao.InmtPrpsDao;
import kr.or.copyright.mls.inmtPrps.model.InmtPrps;
import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.common.common.service.BaseService;
//import kr.or.copyright.mls.support.dao.DaoConfig;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

public class InmtPrpsServiceImpl extends BaseService implements InmtPrpsService {

	//private Log logger = LogFactory.getLog(getClass());
	
	private InmtPrpsDao inmtPrpsDao;
	//private DaoManager daoMgr;

	public void setInmtPrpsDao(InmtPrpsDao inmtPrpsDao) {
//		daoMgr = DaoConfig.getDaoManager();
		this.inmtPrpsDao = inmtPrpsDao;
	}
	
	public String maxYear(InmtPrps params) {
		String maxYear = inmtPrpsDao.maxYear(params);
		return maxYear;
	}
	
	public ListResult inmtList(int pageNo, int rowPerPage, InmtPrps params) {
	    int totalRow = 0;
	    List list = null;
	    
	    if(params.getSrchDIVS().equals("1")){//방송음악
		totalRow = inmtPrpsDao.muscInmtCount(params);
		list = inmtPrpsDao.muscInmtList(params);
	    }else if(params.getSrchDIVS().equals("2")){//교과용
		totalRow = inmtPrpsDao.subjInmtCount(params);
		list = inmtPrpsDao.subjInmtList(params);
	    }else if(params.getSrchDIVS().equals("3")){//도서관
		totalRow = inmtPrpsDao.librInmtCount(params);
		list = inmtPrpsDao.librInmtList(params);
	    }else if(params.getSrchDIVS().equals("4")){//도서관
		totalRow = inmtPrpsDao.lssnInmtCount(params);
		list = inmtPrpsDao.lssnInmtList(params);
	    }

        ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
        
        return listResult;
	}
	
	public int muscMaxAmnt() {
		return inmtPrpsDao.muscMaxAmnt();
	}
	
	public List chkInmtList(InmtPrps params) {
		List list = inmtPrpsDao.chkInmtList(params);
		return list;
	}
	
	public int prpsMastKey() {
		int prpsMastKey	= inmtPrpsDao.prpsMastKey();
		return prpsMastKey;
	}
	
	public int prpsSeqnMax() {
		int prpsSeqnMax = inmtPrpsDao.prpsSeqnMax();
		return prpsSeqnMax;
	}
	
	public void inmtPspsInsert(Map params) {
		inmtPrpsDao.inmtPspsInsert(params);
	}

	public void inmtKappInsert(Map params) {
		inmtPrpsDao.inmtKappInsert(params);
	}
	
	public void inmtFokapoInsert(Map params) {
		inmtPrpsDao.inmtFokapoInsert(params);
	}
	
	public void inmtKrtraInsert(Map params) {
		inmtPrpsDao.inmtKrtraInsert(params);
	}
	
	public void inmtPrpsRsltInsert(Map params) {
		inmtPrpsDao.inmtPrpsRsltInsert(params);
	}
	
	public void insertConnInfo(Map params) {
		inmtPrpsDao.insertConnInfo(params);
	}
	
	public String getMaxDealStat(RghtPrps params) {
		String maxDealStat = inmtPrpsDao.getMaxDealStat(params);
		return maxDealStat;
	}

	public void inmtKappDelete(RghtPrps params) {
		inmtPrpsDao.inmtKappDelete(params);
	}
	
	public void inmtFokapoDelete(RghtPrps params) {
		inmtPrpsDao.inmtFokapoDelete(params);
	}
	
	public void inmtKrtraDelete(RghtPrps params) {
		inmtPrpsDao.inmtKrtraDelete(params);
	}
	
	public void inmtPrpsRsltDelete(RghtPrps params) {
		inmtPrpsDao.inmtPrpsRsltDelete(params);
	}
	
	public void inmtPrpsDelete(RghtPrps params) {
		inmtPrpsDao.inmtPrpsDelete(params);
	}
	
	public void inmtFileDeleteOne(InmtPrps params) {
		inmtPrpsDao.inmtFileDeleteOne(params);
	}
	
	public List inmtPrpsHistList(RghtPrps params) {
		List list = inmtPrpsDao.inmtPrpsHistList(params);
		return list;
	}
	
	public List inmtPrpsTopList(Map params) {
		return inmtPrpsDao.inmtPrpsTopList(params);
	}

	public List inmtPrpsAmntList( Map params ){
		return inmtPrpsDao.inmtPrpsAmntList(params);
	}
}