package kr.or.copyright.mls.mail.dao;

import kr.or.copyright.mls.mail.model.Mail;

public interface MailDao {
	
	//insert 수신거부목록
	public void insertMailRejcList(Mail mail);
	
	//delete 수신거부목록
	public void deleteMailRejcList(Mail mail);
	
	// update 수신확인
	public void updateEmailRecv(Mail mail); 

}
