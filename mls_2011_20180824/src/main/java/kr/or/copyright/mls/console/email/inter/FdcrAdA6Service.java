package kr.or.copyright.mls.console.email.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAdA6Service{

	/**
	 * 발신함 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA6List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 발신함 상세 조회(송수신내역/메일의내용)
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA6View1( Map<String, Object> commandMap ) throws Exception;

}
