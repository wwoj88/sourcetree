package kr.or.copyright.mls.common.service;

import java.util.List;

import kr.or.copyright.mls.common.model.CodeList;

public interface CodeListService {

	List<CodeList> commonCodeList(CodeList codeList);
	
}
