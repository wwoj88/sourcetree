package kr.or.copyright.mls.inmt.dao;

import java.util.List;
import java.util.Map;

public interface InmtDao {
	
	public List inmtList(Map map);
	
	public List inmtListCount(Map map);	
	
	public int prpsMastKey();
	
	public int prpsSeqnMax();
	
	public void inmtPspsInsert(Map map);
	
	public void inmtPrpsRsltInsert(Map map);
	
	public void inmtKappInsert(Map map);
	
	public void inmtFokapoInsert(Map map);
	
	public void inmtKrtraInsert(Map map);
	
	
	public List inmtTotalDealStat(Map map);
	
	public List inmtTempList(Map map);
	
	public List inmtPrpsSelect(Map map);
	
	public List inmtPrpsRsltList(Map map);
	
	public List inmtKappList(Map map);
	
	public List inmtFokapoList(Map map);
	
	public List inmtKrtraList(Map map);
	
	
	public void inmtPrpsUpdate(Map map);
	
	
	public void inmtPrpsDelete(Map map);
	
	public void inmtPrpsRsltDelete(Map map);
	
	public void inmtKappDelete(Map map);
	
	public void inmtFokapoDelete(Map map);
	
	public void inmtKrtraDelete(Map map);
	
	
	public List inmtFileList(Map map);
	
	public void inmtFileInsert(Map map);
	
	public void inmtFileDelete(Map map);
	
	public void inmtFileDeleteOne(Map map);
	
	
	// 처리상태 최대값 구한다.
	public List getMaxDealStat(Map map);
	
	// 강명표 추가
	public void insertConnInfo(Map map);
}
