package kr.or.copyright.mls.article.service;

import java.util.List;

import kr.or.copyright.mls.article.model.Article;

public interface ArticleService {
    public List<Article> getArticleList(int communityId);

    public Article getArticleInfo(int articleId); 

    public int writeArticle(Article article); 

    public void removeArticle(int articleId); 

}
