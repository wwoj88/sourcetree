package kr.or.copyright.mls.console.stats;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Service;
import kr.or.copyright.mls.console.stats.inter.FdcrAd92Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 통계관리 > 보상금신청 및 처리통계
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd92Controller extends DefaultController{

	@Resource( name = "fdcrAd92Service" )
	private FdcrAd92Service fdcrAd92Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd92Controller.class );

	/**
	 * 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd92List1.page" )
	public String fdcrAd92List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd92List1 Start" );
		// 파라미터 셋팅
		commandMap.put( "YEAR_MONTH", "" );

		fdcrAd92Service.fdcrAd92List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "fdcrAd92List1 End" );
		return "test";
	}
}
