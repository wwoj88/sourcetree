package kr.or.copyright.mls.mobile.stat.contoller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.mobile.model.Common;
import kr.or.copyright.mls.mobile.stat.model.MobileStat;
import kr.or.copyright.mls.mobile.stat.model.MobileStatFile;
import kr.or.copyright.mls.mobile.stat.service.MobileStatService;

import org.apache.log4j.Logger;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class MoblieStatController extends MultiActionController{
	private MobileStatService mobileStatService;
	private Logger log = Logger.getLogger(this.getClass());
	
	public void setMobileStatService(MobileStatService mobileStatService) {
		this.mobileStatService = mobileStatService;
	}


	public ModelAndView statBo01List(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		MobileStat mobileStat = new MobileStat();
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		int from = Constants.DEFAULT_ROW_PER_PAGE * (pageNo - 1) + 1;
        int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		String tite = ServletRequestUtils.getStringParameter(request, "keyWord");        
        Map<Object, Object> params = new HashMap<Object, Object>();
        params.put("bordCd", 1);
        params.put("tite", tite);
        int total = mobileStatService.countStat(params);
        mobileStat.setNowPage(pageNo);
        mobileStat.setStartRow(from);
        mobileStat.setEndRow(to);
        mobileStat.setBordCd(1);
        mobileStat.setTite(tite);
        List<MobileStat> statList = mobileStatService.selectStat(mobileStat);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mobile/statBord/statBo01List");
		mav.addObject("mobileStat", statList);
		mav.addObject("pageNo", pageNo);
		mav.addObject("total", total);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	public ModelAndView statBo01Detl(HttpServletRequest request,
			HttpServletResponse response)throws Exception{
		int bordSeqn = ServletRequestUtils.getIntParameter(request, "bordSeqn");
		List<MobileStat> detlList = mobileStatService.detailStat(bordSeqn); 
		List<MobileStatFile> fileList = mobileStatService.fileSelectStat(bordSeqn);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mobile/statBord/statBo01Detl");
		mav.addObject("mobileStat", detlList.get(0));
		mav.addObject("commonHtm", Common.getCommonHtm());
		mav.addObject("fileList", fileList);
		return mav;
	}
	public ModelAndView statBo03List(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		MobileStat mobileStat = new MobileStat();
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		int from = Constants.DEFAULT_ROW_PER_PAGE * (pageNo - 1) + 1;
        int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
        String tite = ServletRequestUtils.getStringParameter(request, "keyWord");
        Map<Object, Object> params = new HashMap<Object, Object>();
        params.put("bordCd", 3);
        params.put("tite", tite);
        int total = mobileStatService.countStat(params);
        mobileStat.setNowPage(pageNo);
        mobileStat.setStartRow(from);
        mobileStat.setEndRow(to);
        mobileStat.setBordCd(3);
        mobileStat.setTite(tite);
        List<MobileStat> list = mobileStatService.selectStat(mobileStat);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mobile/statBord/statBo03List");
		mav.addObject("mobileStat", list);
		mav.addObject("pageNo", pageNo);
		mav.addObject("total", total);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	public ModelAndView statBo03Detl(HttpServletRequest request,
			HttpServletResponse response)throws Exception{
		int bordSeqn = ServletRequestUtils.getIntParameter(request, "bordSeqn");
		List<MobileStat> detlList = mobileStatService.detailStat(bordSeqn); 
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mobile/statBord/statBo03Detl");
		mav.addObject("mobileStat", detlList.get(0));
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	public ModelAndView statBo04List(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		MobileStat mobileStat = new MobileStat();
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		int from = Constants.DEFAULT_ROW_PER_PAGE * (pageNo - 1) + 1;
        int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
        String tite = ServletRequestUtils.getStringParameter(request, "keyWord");
        Map<Object, Object> params = new HashMap<Object, Object>();
        params.put("bordCd", 4);
        params.put("tite", tite);
        int total = mobileStatService.countStat(params);
        mobileStat.setNowPage(pageNo);
        mobileStat.setStartRow(from);
        mobileStat.setEndRow(to);
        mobileStat.setBordCd(4);
        mobileStat.setTite(tite);
        List<MobileStat> list = mobileStatService.selectStat(mobileStat);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mobile/statBord/statBo04List");
		mav.addObject("mobileStat", list);
		mav.addObject("pageNo", pageNo);
		mav.addObject("total", total);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	public ModelAndView statBo04Detl(HttpServletRequest request,
			HttpServletResponse response)throws Exception{
		int bordSeqn = ServletRequestUtils.getIntParameter(request, "bordSeqn");
		List<MobileStat> detlList = mobileStatService.detailStat(bordSeqn); 
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mobile/statBord/statBo04Detl");
		mav.addObject("mobileStat", detlList.get(0));
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	public ModelAndView statBo05List(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		MobileStat mobileStat = new MobileStat();
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		int from = Constants.DEFAULT_ROW_PER_PAGE * (pageNo - 1) + 1;
        int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
        String tite = ServletRequestUtils.getStringParameter(request, "keyWord");
        Map<Object, Object> params = new HashMap<Object, Object>();
        params.put("bordCd", 5);
        params.put("tite", tite);
        int total = mobileStatService.countStat(params);
        mobileStat.setNowPage(pageNo);
        mobileStat.setStartRow(from);
        mobileStat.setEndRow(to);
        mobileStat.setBordCd(5);
        mobileStat.setTite(tite);
        List<MobileStat> list = mobileStatService.selectStat(mobileStat);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mobile/statBord/statBo05List");
		mav.addObject("mobileStat", list);
		mav.addObject("pageNo", pageNo);
		mav.addObject("total", total);
		mav.addObject("commonHtm", Common.getCommonHtm());
		return mav;
	}
	public ModelAndView statBo05Detl(HttpServletRequest request,
			HttpServletResponse response)throws Exception{
		int bordSeqn = ServletRequestUtils.getIntParameter(request, "bordSeqn");
		List<MobileStat> detlList = mobileStatService.detailStat(bordSeqn); 
		List<MobileStatFile> fileList = mobileStatService.fileSelectStat(bordSeqn);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mobile/statBord/statBo05Detl");
		mav.addObject("mobileStat", detlList.get(0));
		mav.addObject("commonHtm", Common.getCommonHtm());
		mav.addObject("fileList", fileList);
		return mav;
	}
	
}
