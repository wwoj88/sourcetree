package kr.or.copyright.mls.support.util;

import java.io.*;

public class FileUtil {
	 private FileUtil()
	    {
	    }
 
	    public static StringBuffer read(String fileName)
	        throws IOException
	    {
	        StringBuffer out;
	        File fileIn = new File(fileName);
	        FileInputStream inStream = null;
	        out = new StringBuffer();
	        if(!fileIn.exists())
	            fileIn = new File(searchFile(fileName));
	        try
	        {
	            inStream = new FileInputStream(fileIn);
	            int bytesRead = 0;
	            for(byte buffer[] = new byte[1024]; (bytesRead = inStream.read(buffer)) != -1; buffer = new byte[1024])
	                out.append(new String(buffer));

	        }
	        catch(IOException e)
	        {
	            throw e;
	        }
	        finally
	        {
	            if(inStream != null)
	                try
	                {
	                    inStream.close();
	                }
	                catch(IOException ioexception) { }
	        }
	        
	        return out;
	    }

	    public static void copy(String fileName, String targetName)
	        throws IOException
	    {
	        File fileIn = new File(fileName);
	        File fileOut = new File(targetName);
	        FileInputStream inStream = null;
	        FileOutputStream outStream = null;
	        if(!fileIn.exists())
	            fileIn = new File(searchFile(fileName));
	        try
	        {
	            inStream = new FileInputStream(fileIn);
	            outStream = new FileOutputStream(fileOut);
	            int bytesRead = 0;
	            for(byte buffer[] = new byte[1024]; (bytesRead = inStream.read(buffer)) != -1; buffer = new byte[1024])
	                outStream.write(buffer, 0, bytesRead);

	        }
	        catch(IOException e)
	        {
	            throw e;
	        }
	        finally
	        {
	            if(inStream != null)
	                try
	                {
	                    inStream.close();
	                }
	                catch(IOException ioexception) { }
	            if(outStream != null)
	                try
	                {
	                    outStream.close();
	                }
	                catch(IOException ioexception1) { }
	        }
	    }

	    public static void move(String fileName, String targetName)
	        throws IOException
	    {
	        copy(fileName, targetName);
	        delete(fileName);
	    }

	    public static void delete(String fileName)
	    {
	        File fileIn = new File(fileName);
	        if(!fileIn.exists())
	            fileIn = new File(searchFile(fileName));
	        if(fileIn.exists())
	            try
	            {
	                fileIn.delete();
	            }
	            catch(Exception e)
	            {
	            }
	    }

	    public static void delete(String dirStr, String fileName)
	    {
	        File fileIn = new File(dirStr + File.separator + fileName);
	        if(!fileIn.exists())
	            fileIn = new File(searchFile(fileName));
	        if(fileIn.exists())
	            try
	            {
	                fileIn.delete();
	            }
	            catch(Exception e)
	            {
	            }
	    }

	    public static String searchFile(String fileName)
	    {
	        String retFile = "";
	        String tmpFile = "";
	        String filePath;
	        if(fileName.endsWith(File.separator))
	            filePath = fileName;
	        else
	            filePath = fileName.substring(0, fileName.lastIndexOf(File.separator));
	        tmpFile = fileName.substring(filePath.length() + 1);
	        File searchDir = new File(filePath);
	        String dirList[] = searchDir.list();
	        for(int i = 0; i < dirList.length; i++)
	            if(tmpFile.equalsIgnoreCase(dirList[i]))
	                retFile = filePath + dirList[i];

	        return retFile;
	    }
}
