package kr.or.copyright.mls.console.rcept;



import java.net.URLDecoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd15Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 관리저작물 접수 및 처리 > 위탁관리저작물 > 통계
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd15Controller extends DefaultController{

	@Autowired
	private FdcrAd15Service fdcrAd15Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd15Controller.class );

	/**
	 * 위탁관리저작물 보고현황 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd15List1.page" )
	public String fdcrAd15List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd01List1 Start" );
		// 파라미터 셋팅
		String YYYY = EgovWebUtil.getString( commandMap, "YYYY" );
		String firstMM = EgovWebUtil.getString( commandMap, "firstMM" );
		String lastMM = EgovWebUtil.getString( commandMap, "lastMM" );
		String A = EgovWebUtil.getString( commandMap, "A" );
		System.out.println( "A"+A );
		if(A.equals( "" ) || A == null || "".equals( A )) {
			return "rcept/fdcrAd15List1.tiles";
		}
		if(YYYY.equals( "" ) || YYYY == null || "".equals( YYYY )){
			commandMap.put( "REPT_YYYY", YYYY );
		}else{
			commandMap.put( "REPT_YYYY", YYYY );
		}
		
		if(!firstMM.equals( "" )&&!lastMM.equals( "" )){
			commandMap.put( "YYYY", null );
			commandMap.put( "REPT_YYYY", null );
			commandMap.put( "firstMM", YYYY+firstMM );
			commandMap.put( "lastMM", YYYY+lastMM );
		}
		
		//TODO 기관코드 주석
		commandMap.put( "TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode() );
		//commandMap.put( "TRST_ORGN_CODE", "202");
		
		commandMap.put( "DIV", "10" );

		fdcrAd15Service.fdcrAd15List1( commandMap );
		model.addAttribute( "REPT_YYYY", YYYY );
		model.addAttribute( "REPT_firstMM", firstMM );
		model.addAttribute( "REPT_lastMM", lastMM );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_list_0", commandMap.get( "ds_list_0" ) );
		logger.debug( "fdcrAd15List1 End" );
		return "rcept/fdcrAd15List1.tiles";
	}

	/**
	 * 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd15Down1.page" )
	public String fdcrAd15Down1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd15Down1 Start" );
		// 파라미터 셋팅
		commandMap.put( "TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode() );
		String YYYY = request.getParameter( "YYYY" );
		commandMap.put( "YYYY", YYYY );
		commandMap.put( "DIV", "10" );
		
		fdcrAd15Service.fdcrAd15Down1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_list_0", commandMap.get( "ds_list_0" ) );
		logger.debug( "fdcrAd15Down1 End" );
		return "rcept/fdcrAd15Down1";
	}
	
	/**
	 * 위탁관리저작물 보고현황 장르별 목록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd15List2.page" )
	public String fdcrAd15List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd15List2 Start" );
		// 파라미터 셋팅
		//commandMap.put( "YYYY", "" );
		//commandMap.put( "GENRE_CD", "" );
		String YYYY = EgovWebUtil.getString( commandMap, "YYYY" );
		String GENRE_CD = EgovWebUtil.getString( commandMap, "GENRE_CD" );

		fdcrAd15Service.fdcrAd15List2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "fdcrAd15List2 End" );
		if("0".equals( GENRE_CD ) || GENRE_CD == "0"){
			return "rcept/fdcrAd15NoDataPop1.popup";
		}else{
			return "rcept/fdcrAd15Pop1.popup";
		}
	}

	/**
	 * 위탁관리저작물 보고현황 장르별 목록 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd15Down23.page" )
	public String fdcrAd15Down23( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd15Down2 Start" );
		// 파라미터 셋팅
		commandMap.put( "YYYY", "" );
		commandMap.put( "GENRE_CD", "" );

		fdcrAd15Service.fdcrAd15List2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "fdcrAd15Down2 End" );
		return "test";
	}

	/**
	 * 위탁관리저작물 보고현황 월별 목록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd15List3.page" )
	public String fdcrAd15List3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd15List3 Start" );
		// 파라미터 셋팅
		//commandMap.put( "REPT_YMD", "" );
		String REPT_YMD = EgovWebUtil.getString( commandMap, "REPT_YMD" );

		fdcrAd15Service.fdcrAd15List3( commandMap );
		model.addAttribute( "REPT_YMD", REPT_YMD );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_list_0", commandMap.get( "ds_list_0" ) );
		logger.debug( "fdcrAd15List3 End" );
		return "rcept/fdcrAd15Pop2.popup";
	}

	/**
	 * 위탁관리저작물 보고현황 월별 목록 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd15Down2.page" )
	public String fdcrAd15Down2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd15Down2 Start" );
		// 파라미터 셋팅
		String REPT_YMD = request.getParameter( "REPT_YMD" );
		commandMap.put( "REPT_YMD", REPT_YMD );
		fdcrAd15Service.fdcrAd15Down2( commandMap );
		model.addAttribute( "REPT_YMD", REPT_YMD );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_list_0", commandMap.get( "ds_list_0" ) );
		
		logger.debug( "fdcrAd15Down2 End" );
		return "rcept/fdcrAd15Down2";
	}

	/**
	 * 월별 보고현황 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd15List4.page" )
	public String fdcrAd15List4( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd15List4 Start" );
		// 파라미터 셋팅
		String TRST_ORGN_CODE = EgovWebUtil.getString( commandMap, "TRST_ORGN_CODE" );
		String YYYYMM = EgovWebUtil.getString( commandMap, "YYYYMM" );
		String YYYY = EgovWebUtil.getString( commandMap, "YYYY" );
		commandMap.put( "DIV", "20" );
		
		//월별 보고정보 목록 파라미터
		String GUBUN = EgovWebUtil.getString( commandMap, "SCH_GUBUN" );
		String TITLE = EgovWebUtil.getString( commandMap, "SCH_TITLE" );
		if( !"".equals( TITLE ) ){
			TITLE = URLDecoder.decode( TITLE, "UTF-8" );
			commandMap.put( "TITLE", TITLE );
		}
		//commandMap.put( "FROM_NO", "" ); // 페이지 시작
		//commandMap.put( "TO_NO", "" ); // 페이지 끝
		
		commandMap.put( "GUBUN", GUBUN );
		commandMap.put( "TITLE", TITLE );
		commandMap.put( "TRST_ORGN_CODE", TRST_ORGN_CODE );
		commandMap.put( "YYYYMM", YYYYMM );
		commandMap.put( "YYYYMM2", YYYYMM );
		commandMap.put( "YYYY", YYYY );
		

		fdcrAd15Service.fdcrAd15List4( commandMap );
		model.addAttribute( "ds_count", commandMap.get( "ds_count" ) );
		model.addAttribute( "ds_report", commandMap.get( "ds_report" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		
		logger.debug( "fdcrAd15List4 End" );
		return "rcept/fdcrAd15Pop3.popup";
	}

	/**
	 * 월별 보고현황 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd15List5.page" )
	public String fdcrAd15List5( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd15List5 Start" );
		// 파라미터 셋팅
		commandMap.put( "GUBUN", "" );// 구분
		commandMap.put( "TITLE", "" ); // 제목/내용
		commandMap.put( "FROM_NO", "" ); // 페이지 시작
		commandMap.put( "TO_NO", "" ); // 페이지 끝
		commandMap.put( "YYYYMM", "" ); // 년월
		fdcrAd15Service.fdcrAd15List5( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		logger.debug( "fdcrAd15List5 End" );
		return "test";
	}
}
