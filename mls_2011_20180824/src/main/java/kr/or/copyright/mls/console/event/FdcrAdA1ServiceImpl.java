package kr.or.copyright.mls.console.event;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.event.inter.FdcrAdA0Dao;
import kr.or.copyright.mls.console.event.inter.FdcrAdA1Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdA1Service" )
public class FdcrAdA1ServiceImpl extends CommandService implements FdcrAdA1Service{

	@Resource( name = "fdcrAdA0Dao" )
	private FdcrAdA0Dao fdcrAdA0Dao;

	/**
	 * 이벤트 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA1List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA0Dao.getEventList();

		commandMap.put( "ds_list", list );

	}

	/**
	 * 이벤트 상세
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA1View2( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List ds_data = (List) fdcrAdA0Dao.getEventDetl( commandMap );// 상세화면데이터
		Map<String,Object> info = new HashMap<String,Object>();
		if(ds_data!=null){
			info = (Map<String,Object>)ds_data.get( 0 );
		}
		List rs_agree = (List) fdcrAdA0Dao.getEventDetlAgree( commandMap );// 동의항목
		String AGREE_ITEM_CD_1 = "N";
		String AGREE_ITEM_CD_2 = "N";
		String AGREE_ITEM_CD_99 = "N";
		
		String ITEM_DESC_1 = "";
		String ITEM_DESC_2 = "";
		String ITEM_DESC_99 = "";
		
		if(rs_agree!=null){
			for(int i=0;i<rs_agree.size();i++){
				Map<String,Object> agree = (Map<String,Object>)rs_agree.get( i );
				String cd = EgovWebUtil.getString(agree, "AGREE_ITEM_CD");
				String desc = EgovWebUtil.getString(agree, "ITEM_DESC");
				
				if("1".equals(cd)){
					AGREE_ITEM_CD_1 = "Y";
					ITEM_DESC_1 = desc;
				}else if("2".equals(cd)){
					AGREE_ITEM_CD_2 = "Y";
					ITEM_DESC_2 = desc;
				}else if("99".equals(cd)){
					AGREE_ITEM_CD_99 = "Y";
					ITEM_DESC_99 = desc;
				}
			}
		}
		
		info.put( "AGREE_ITEM_CD_1", AGREE_ITEM_CD_1 );
		info.put( "AGREE_ITEM_CD_2", AGREE_ITEM_CD_2 );
		info.put( "AGREE_ITEM_CD_99", AGREE_ITEM_CD_99 );
		
		info.put( "ITEM_DESC_1", ITEM_DESC_1 );
		info.put( "ITEM_DESC_2", ITEM_DESC_2 );
		info.put( "ITEM_DESC_99", ITEM_DESC_99 );

		commandMap.put( "info", info );

	}

	/**
	 * 이벤트 재등록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA1Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String[] AGREEYNS = (String[]) commandMap.get( "AGREEYN" );
			String[] ITEM_DESCS = (String[]) commandMap.get( "ITEM_DESC" );
			String[] ITEM_TITLES = (String[]) commandMap.get( "ITEM_TITLE" );
			String[] AGREE_ITEM_CDS = (String[]) commandMap.get( "AGREE_ITEM_CD" );

			String seq = fdcrAdA0Dao.getSaveEventSeq();// 시퀀스

			// 재등록일때
			commandMap.put( "OPER_YN", "N" );
			commandMap.put( "EVENT_ID", seq );

			// 파일업로드
			String fileName = (String) commandMap.get( "IMAGE_FILE_PATH" );
			if( "Y".equals( commandMap.get( "FILE_UP_YN" ) ) ){
				byte[] file = (byte[]) commandMap.get( "CLIENT_FILE" );
				Map upload = FileUtil.uploadMiFile( fileName, file, "event/" );
				commandMap.put( "IMAGE_FILE_PATH", upload.get( "FILE_PATH" ) + "" + upload.get( "REAL_FILE_NAME" ) );
				commandMap.put( "IMAGE_FILE_NAME", commandMap.get( "IMAGE_FILE_NAME" ) );
			}

			String fileName2 = (String) commandMap.get( "IMAGE_FILE_PATH_WIN" );
			if( "Y".equals( commandMap.get( "FILE_UP_WIN_YN" ) ) ){
				byte[] file = (byte[]) commandMap.get( "CLIENT_FILE_WIN" );
				Map upload = FileUtil.uploadMiFile( fileName2, file, "event/" );
				commandMap.put( "IMAGE_FILE_PATH_WIN", upload.get( "FILE_PATH" ) + "" + upload.get( "REAL_FILE_NAME" ) );
				commandMap.put( "IMAGE_FILE_NAME_WIN", commandMap.get( "IMAGE_FILE_NAME_WIN" ) );
			}

			fdcrAdA0Dao.addEvent( commandMap );// 등록

			for( int i = 0; i < AGREEYNS.length; i++ ){
				Map<String, Object> mAgree = new HashMap<String, Object>();
				mAgree.put( "AGREEYN", AGREEYNS[i] );
				mAgree.put( "ITEM_DESC", ITEM_DESCS[i] );
				mAgree.put( "ITEM_TITLE", ITEM_TITLES[i] );
				mAgree.put( "AGREE_ITEM_CD", AGREE_ITEM_CDS[i] );
				mAgree.put( "EVENT_ID", seq );
				if( "Y".equals( mAgree.get( "AGREEYN" ) ) ){
					fdcrAdA0Dao.addEventAgree( mAgree );// 동의항목 등록
				}
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 이벤트 수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA1Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String[] AGREEYNS = (String[]) commandMap.get( "AGREEYN" );
			String[] ITEM_DESCS = (String[]) commandMap.get( "ITEM_DESC" );
			String[] ITEM_TITLES = (String[]) commandMap.get( "ITEM_TITLE" );
			String[] AGREE_ITEM_CDS = (String[]) commandMap.get( "AGREE_ITEM_CD" );

			// 파일업로드
			String fileName = (String) commandMap.get( "IMAGE_FILE_PATH" );
			if( "Y".equals( commandMap.get( "FILE_UP_YN" ) ) ){
				byte[] file = (byte[]) commandMap.get( "CLIENT_FILE" );
				Map upload = FileUtil.uploadMiFile( fileName, file, "event/" );
				commandMap.put( "IMAGE_FILE_PATH", upload.get( "FILE_PATH" ) + "" + upload.get( "REAL_FILE_NAME" ) );
				commandMap.put( "IMAGE_FILE_NAME", commandMap.get( "IMAGE_FILE_NAME" ) );
			}

			String fileName2 = (String) commandMap.get( "IMAGE_FILE_PATH_WIN" );
			if( "Y".equals( commandMap.get( "FILE_UP_WIN_YN" ) ) ){
				byte[] file = (byte[]) commandMap.get( "CLIENT_FILE_WIN" );
				Map upload = FileUtil.uploadMiFile( fileName2, file, "event/" );
				commandMap.put( "IMAGE_FILE_PATH_WIN", upload.get( "FILE_PATH" ) + "" + upload.get( "REAL_FILE_NAME" ) );
				commandMap.put( "IMAGE_FILE_NAME_WIN", commandMap.get( "IMAGE_FILE_NAME_WIN" ) );
			}

			fdcrAdA0Dao.updateEvent( commandMap );// 등록

			fdcrAdA0Dao.delEventAgree( commandMap );// 전체 삭제
			
			ArrayList<Map<String,Object>> agreeList = (ArrayList<Map<String,Object>>)commandMap.get( "agreeList" );
			if(null!=agreeList){
				for( int i = 0; i < agreeList.size(); i++ ){
					Map<String, Object> mAgree = agreeList.get( i );
					mAgree.put( "EVENT_ID", commandMap.get( "EVENT_ID" ) );
					if( "Y".equals( (String)mAgree.get( "AGREEYN" ) ) ){
						fdcrAdA0Dao.addEventAgree( mAgree );// 동의항목 등록
					}
				}
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 이벤트 선택삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA1Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			String[] EVENT_IDS = (String[]) commandMap.get( "EVENT_ID" );
			for( int i = 0; i < EVENT_IDS.length; i++ ){

				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "EVENT_ID", EVENT_IDS[i] );

				fdcrAdA0Dao.delEventList( param ); // 삭제상태 업데이트
			}
			fdcrAdA1List1( commandMap );// 리스트 호출
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 댓글형 이벤트 상세
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA1View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		Map<String,Object> map = fdcrAdA0Dao.getCommItem( commandMap );
		if(map!=null){
			commandMap.put( "mode", "u" );
		}else{
			commandMap.put( "mode", "i" );
			
		}
		commandMap.put( "ds_data", map );

	}

	/**
	 * 댓글형 이벤트 댓글 등록/수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA1Insert2( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			String mode = EgovWebUtil.getString( commandMap, "mode" );
			if("i".equals(mode)){
				// 등록일때
				fdcrAdA0Dao.addCommItem( commandMap );
			}else if("u".equals(mode)){
				// 수정일때
				fdcrAdA0Dao.uptCommItem( commandMap );
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 참여 및 응답현황 목록 팝업 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA1Pop1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		Map<String,Object> ds_event = fdcrAdA0Dao.getEventPart( commandMap );
		List ds_list = (List) fdcrAdA0Dao.getEventPartList( commandMap );
		List ds_list_rslt = (List) fdcrAdA0Dao.getEventPartRsltList( commandMap );
		
		int colCnt = 0;
		ArrayList<Map<String,Object>> headerList = new ArrayList<Map<String,Object>>();
		if(null!=ds_list_rslt && ds_list_rslt.size() > 0){
			
			Map<String,Object> basicRslt = (Map<String,Object>)ds_list_rslt.get( 0 );
			String basicUserIdnt = EgovWebUtil.getString( basicRslt, "USER_IDNT" );
			int basicPartCnt = EgovWebUtil.getInt( basicRslt, "PART_CNT" );
			
			for(int i=0;i<ds_list_rslt.size();i++){
				Map<String,Object> rslt = (Map<String,Object>)ds_list_rslt.get( i );
				String USER_IDNT = EgovWebUtil.getString( rslt, "USER_IDNT" );
				int PART_CNT = EgovWebUtil.getInt( rslt, "PART_CNT" );
				if(basicUserIdnt.equals(USER_IDNT) && basicPartCnt == PART_CNT){
					headerList.add( rslt );
					colCnt++;
				}else{
					i = ds_list_rslt.size();
				}
			}
			
			int rslt_no = 0;
			for(int i=0;i<ds_list.size();i++){
				Map<String,Object> info = (Map<String,Object>)ds_list.get( i );
				for(int j=0;j<colCnt;j++){
					info.put( "COL"+(j+1), ((Map<String,Object>)ds_list_rslt.get(j+rslt_no)).get("RSLT_DESC") );
				}
				rslt_no = rslt_no+colCnt;
			}
		}

		
		commandMap.put( "ds_event", ds_event );
		commandMap.put( "ds_list", ds_list );
		commandMap.put( "ds_list_rslt", ds_list_rslt );
		commandMap.put( "headerList", headerList );
		commandMap.put( "colCnt", colCnt );

	}

	/**
	 * 참여 및 응답현황 목록 엑셀다운로드
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA1ExcelDown1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List ds_event = (List) fdcrAdA0Dao.getEventPart( commandMap );
		List ds_list = (List) fdcrAdA0Dao.getEventPartList( commandMap );
		List ds_list_rslt = (List) fdcrAdA0Dao.getEventPartRsltList( commandMap );

		commandMap.put( "ds_event", ds_event );
		commandMap.put( "ds_list", ds_list );
		commandMap.put( "ds_list_rslt", ds_list_rslt );

	}

	/**
	 * 참여 및 응답현황/당첨자 자동선정 당첨자 등록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA1Insert3( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			ArrayList<Map> list = new ArrayList<Map>();
			String[] EVENT_IDS = (String[]) commandMap.get( "EVENT_ID" );
			String[] USER_IDNTS = (String[]) commandMap.get( "USER_IDNT" );
			String[] PART_CNTS = (String[]) commandMap.get( "PART_CNT" );

			if( EVENT_IDS != null ){
				for( int i = 0; i < EVENT_IDS.length; i++ ){
					Map<String, Object> map = new HashMap<String, Object>();
					map.put( "EVENT_ID", EVENT_IDS[i] );
					map.put( "USER_IDNT", USER_IDNTS[i] );
					map.put( "PART_CNT", PART_CNTS[i] );
					list.add( map );
				}
				fdcrAdA0Dao.uptWinY( list );
			}

			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 참여 및 응답현황 당첨자 취소
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA1Insert4( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			ArrayList<Map> list = new ArrayList<Map>();
			String[] EVENT_IDS = (String[]) commandMap.get( "EVENT_ID" );
			String[] USER_IDNTS = (String[]) commandMap.get( "USER_IDNT" );
			String[] PART_CNTS = (String[]) commandMap.get( "PART_CNT" );

			if( EVENT_IDS != null ){
				for( int i = 0; i < EVENT_IDS.length; i++ ){
					Map<String, Object> map = new HashMap<String, Object>();
					map.put( "EVENT_ID", EVENT_IDS[i] );
					map.put( "USER_IDNT", USER_IDNTS[i] );
					map.put( "PART_CNT", PART_CNTS[i] );
					list.add( map );
				}
				fdcrAdA0Dao.uptWinN( list );
			}

			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 당첨자 자동선정 목록 팝업 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA1Pop2( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		Map ds_event = (Map) fdcrAdA0Dao.getEventPart( commandMap );

		commandMap.put( "ds_event", ds_event );

	}

	/**
	 * 당첨자 자동선정 선정하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA1List2( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List ds_list = (List) fdcrAdA0Dao.getSelRandomWinning( commandMap );

		commandMap.put( "ds_list", ds_list );

	}

}
