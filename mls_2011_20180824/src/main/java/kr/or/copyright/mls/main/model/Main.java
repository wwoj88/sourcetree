package kr.or.copyright.mls.main.model;

import java.util.List;

import kr.or.copyright.mls.common.BaseObject;

public class Main extends BaseObject {

	private long bordSeqn;
	private long menuSeqn;
	private long threaded;
	private long inmtSeqn;
	private String tite;
	private String workTitle;
	private String titleOrg;
	private String insertDate;
	private String genre;
	private String genreCode;
	private String licensorNameKor;
	private String licensorNameKorOrg;
	private String realFileName;
	private String rgstKindCode;
	private List qnaList;
	private List workList;
	private List inmtList;
	private List notiList;
	private List lawList;
	private List workNoneList;
	private List promPotoList;
	private List promMovieList;
	private String inmt;
	private String inmt202;
	private String inmt203;
	private String inmtL;
	private String inmtS;
	private String gubun;
	private String gubunName;
		
	//엄동규추가
	private String divsCdName;
	private String genreCdName;
	private String openDttm;
	private int bordCd;
	private int divsCd;
	private String rgstDttm;
	private int worksId;
	
	//20171207
	private String anucItem4;
	
	public String getAnucItem4(){
		return anucItem4;
	}
	
	public void setAnucItem4( String anucItem4 ){
		this.anucItem4 = anucItem4;
	}
	//이재현추가
	private String mediOpenDate;

	public long getBordSeqn() {
		return bordSeqn;
	}
	public void setBordSeqn(long bordSeqn) {
		this.bordSeqn = bordSeqn;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public List getInmtList() {
		return inmtList;
	}
	public void setInmtList(List inmtList) {
		this.inmtList = inmtList;
	}
	public long getInmtSeqn() {
		return inmtSeqn;
	}
	public void setInmtSeqn(long inmtSeqn) {
		this.inmtSeqn = inmtSeqn;
	}
	public String getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}
	public String getLicensorNameKor() {
		return licensorNameKor;
	}
	public void setLicensorNameKor(String licensorNameKor) {
		this.licensorNameKor = licensorNameKor;
	}
	public long getMenuSeqn() {
		return menuSeqn;
	}
	public void setMenuSeqn(long menuSeqn) {
		this.menuSeqn = menuSeqn;
	}
	public List getQnaList() {
		return qnaList;
	}
	public void setQnaList(List qnaList) {
		this.qnaList = qnaList;
	}
	public long getThreaded() {
		return threaded;
	}
	public void setThreaded(long threaded) {
		this.threaded = threaded;
	}
	public String getTite() {
		return tite;
	}
	public void setTite(String tite) {
		this.tite = tite;
	}
	public List getWorkList() {
		return workList;
	}
	public void setWorkList(List workList) {
		this.workList = workList;
	}
	public String getWorkTitle() {
		return workTitle;
	}
	public void setWorkTitle(String workTitle) {
		this.workTitle = workTitle;
	}
	public List getNotiList() {
		return notiList;
	}
	public void setNotiList(List notiList) {
		this.notiList = notiList;
	}
	public List getPromPotoList() {
		return promPotoList;
	}
	public void setPromPotoList(List promPotoList) {
		this.promPotoList = promPotoList;
	}
	public List getPromMovieList() {
		return promMovieList;
	}
	public void setPromMovieList(List promMovieList) {
		this.promMovieList = promMovieList;
	}
	public String getRealFileName() {
		return realFileName;
	}
	public void setRealFileName(String realFileName) {
		this.realFileName = realFileName;
	}
	public String getRgstKindCode() {
		return rgstKindCode;
	}
	public void setRgstKindCode(String rgstKindCode) {
		this.rgstKindCode = rgstKindCode;
	}
	public List getLawList() {
		return lawList;
	}
	public void setLawList(List lawList) {
		this.lawList = lawList;
	}
	public List getWorkNoneList() {
		return workNoneList;
	}
	public void setWorkNoneList(List workNoneList) {
		this.workNoneList = workNoneList;
	}
	public String getInmt() {
		return inmt;
	}
	public void setInmt(String inmt) {
		this.inmt = inmt;
	}
	public String getInmt202() {
		return inmt202;
	}
	public void setInmt202(String inmt202) {
		this.inmt202 = inmt202;
	}
	public String getInmt203() {
		return inmt203;
	}
	public void setInmt203(String inmt203) {
		this.inmt203 = inmt203;
	}
	public String getInmtL() {
		return inmtL;
	}
	public void setInmtL(String inmtL) {
		this.inmtL = inmtL;
	}
	public String getInmtS() {
		return inmtS;
	}
	public void setInmtS(String inmtS) {
		this.inmtS = inmtS;
	}
	public String getGubun() {
		return gubun;
	}
	public void setGubun(String gubun) {
		this.gubun = gubun;
	}
	public String getGubunName() {
		return gubunName;
	}
	public void setGubunName(String gubunName) {
		this.gubunName = gubunName;
	}
	public String getGenreCode() {
		return genreCode;
	}
	public void setGenreCode(String genreCode) {
		this.genreCode = genreCode;
	}
	public String getLicensorNameKorOrg() {
		return licensorNameKorOrg;
	}
	public void setLicensorNameKorOrg(String licensorNameKorOrg) {
		this.licensorNameKorOrg = licensorNameKorOrg;
	}
	public String getTitleOrg() {
		return titleOrg;
	}
	public void setTitleOrg(String titleOrg) {
		this.titleOrg = titleOrg;
	}
	public String getDivsCdName() {
		return divsCdName;
	}
	public void setDivsCdName(String divsCdName) {
		this.divsCdName = divsCdName;
	}
	public String getGenreCdName() {
		return genreCdName;
	}
	public void setGenreCdName(String genreCdName) {
		this.genreCdName = genreCdName;
	}
	public String getOpenDttm() {
		return openDttm;
	}
	public void setOpenDttm(String openDttm) {
		this.openDttm = openDttm;
	}
	public int getBordCd() {
		return bordCd;
	}
	public void setBordCd(int bordCd) {
		this.bordCd = bordCd;
	}
	public int getDivsCd() {
		return divsCd;
	}
	public void setDivsCd(int divsCd) {
		this.divsCd = divsCd;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	public int getWorksId() {
		return worksId;
	}
	public void setWorksId(int worksId) {
		this.worksId = worksId;
	}
	public String getMediOpenDate(){
		return mediOpenDate;
	}
	public void setMediOpenDate( String mediOpenDate ){
		this.mediOpenDate = mediOpenDate;
	}
	
	
}
