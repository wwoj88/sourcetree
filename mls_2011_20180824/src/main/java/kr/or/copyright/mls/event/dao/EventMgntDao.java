package kr.or.copyright.mls.event.dao;

import java.util.List;

import kr.or.copyright.mls.event.model.EventMgntVO;
import kr.or.copyright.mls.event.model.QnABoardVO;

public interface EventMgntDao {

	List getEventUserList(EventMgntVO vo);

	int getEventUserListTotal(EventMgntVO vo);

	EventMgntVO getEventUserDetail(EventMgntVO vo);

	List getEventView02List(EventMgntVO vo);

	int getEventView02Total(EventMgntVO vo);

	List getEventAgreeItem(EventMgntVO vo);

	int getEventPartCnt(EventMgntVO vo);

	void addEventPart(EventMgntVO vo);

	void addEventPartRslt(EventMgntVO vo) throws Exception;

	List getWinningList(EventMgntVO vo);

	int getWinningListTotal(EventMgntVO vo);

	String getTopMenu(int i);

	List getQnaList(QnABoardVO vo);

	int getQnaListCnt(QnABoardVO vo);

	void addQnaRegi(QnABoardVO vo);

	QnABoardVO getQnaDetail(QnABoardVO vo);

	void delQnaDelete(QnABoardVO vo);

	void uptQnaModi(QnABoardVO vo);

}