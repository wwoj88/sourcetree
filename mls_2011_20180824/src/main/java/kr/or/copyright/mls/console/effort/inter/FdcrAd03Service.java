package kr.or.copyright.mls.console.effort.inter;

import java.util.Map;

public interface FdcrAd03Service{

	/**
	 * 상당한노력 진행현황 조회
	 * 
	 * @throws Exception
	 */
	public void fdcrAd03List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 대상저작물 수집-프로시져 호출
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 대상저작물 수집-프로시져 호출
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List3( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 대상 저작물 목록 팝업
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List4( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한노력 현황 조회 - 결과보기팝업
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List5( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한노력 현황 엑셀다운로드
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03Down2( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 법정허락 결과 팝업 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List7(Map<String, Object> commandMap) throws Exception;
	
	/**
	 * 법정허락 결과 엑셀 다운로드
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03Down3(Map<String, Object> commandMap) throws Exception;
	
	/**
	 * 상당한노력 - 실행, 상당한 노력 진행(프로세스 분기 20121108)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Exe1( Map<String, Object> commandMap ) throws Exception;

	// 상당한노력 결과 메일링 (거소불명 신청건만 사용)
	public void sendResultMail( Map map ) throws Exception;

	public void sendMail( Map mailMap ) throws Exception;

	/**
	 * 상당한 노력 예외처리완료
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Exe2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 대상 전환 실행
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Exe3( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한노력 자동화 수행(스케쥴링 (분기. 20121109) + 자동화설정추가(20131018))
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Schedule1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 전환 자동화
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Schedule2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한노력 자동화 설정 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List6( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 대상저작물전환, 예외처리 취소
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Update1( Map<String, Object> commandMap ) throws Exception;
}
