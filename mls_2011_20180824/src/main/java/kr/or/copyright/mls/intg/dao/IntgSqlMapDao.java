package kr.or.copyright.mls.intg.dao;

import java.util.List;

import kr.or.copyright.mls.intg.model.Intg;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class IntgSqlMapDao extends SqlMapClientDaoSupport implements IntgDao {

	//private Log logger = LogFactory.getLog(getClass());
	
	public String srchImgCnt(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForObject("Intg.srchImgCnt", IntgDTO).toString();
	}
	
	public List srchImg(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForList("Intg.srchImg", IntgDTO);
	}
	
	public List detailImg(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForList("Intg.detailImg", IntgDTO);
	}
	
	public String srchBrctCnt(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForObject("Intg.srchBrctCnt", IntgDTO).toString();
	}
	
	public List srchBrct(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForList("Intg.srchBrct", IntgDTO);
	}
	
	public List detailBrct(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForList("Intg.detailBrct", IntgDTO);
	}

	public String srchSubjCnt(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForObject("Intg.srchSubjCnt", IntgDTO).toString();
	}
	
	public List srchSubj(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForList("Intg.srchSubj", IntgDTO);
	}
	
	public List detailSubj(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForList("Intg.detailSubj", IntgDTO);
	}
	
	public String srchLibrCnt(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForObject("Intg.srchLibrCnt", IntgDTO).toString();
	}
	
	public List srchLibr(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForList("Intg.srchLibr", IntgDTO);
	}
	
	public List detailLibr(Intg IntgDTO) {
		return getSqlMapClientTemplate().queryForList("Intg.detailLibr", IntgDTO);
	}
}
