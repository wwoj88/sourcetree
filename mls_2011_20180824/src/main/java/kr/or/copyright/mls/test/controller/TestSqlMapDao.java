package kr.or.copyright.mls.test.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log.output.db.DefaultDataSource;
import org.springframework.orm.ibatis.SqlMapClientFactoryBean;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;

import kr.or.copyright.mls.console.EgovComAbstractDAO;

/*public class TestDaoImpl extends EgovComAbstractDAO implements TestDao{*/
public class TestSqlMapDao extends SqlMapClientDaoSupport implements TestDao{
	public imageVo testServiceMethod(int worksid) {
		return (imageVo) getSqlMapClientTemplate().queryForObject("testSql.selectImage",worksid);
	}
}
