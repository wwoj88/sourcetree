package kr.or.copyright.mls.console.civilservices.model;


public class CivilServicesDTO{
	int civilFileSeqn;
	int civilSeqn;
	String orgFileName;
	String newFileName;
	public CivilServicesDTO(){
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getCivilFileSeqn(){
		return civilFileSeqn;
	}
	
	public void setCivilFileSeqn( int civilFileSeqn ){
		this.civilFileSeqn = civilFileSeqn;
	}
	
	public int getCivilSeqn(){
		return civilSeqn;
	}
	
	public void setCivilSeqn( int civilSeqn ){
		this.civilSeqn = civilSeqn;
	}
	
	public String getOrgFileName(){
		return orgFileName;
	}
	
	public void setOrgFileName( String orgFileName ){
		this.orgFileName = orgFileName;
	}
	
	public String getNewFileName(){
		return newFileName;
	}
	
	public void setNewFileName( String newFileName ){
		this.newFileName = newFileName;
	}

	@Override
	public String toString(){
		return "CivilServicesDTO [civilFileSeqn=" + civilFileSeqn + ", civilSeqn=" + civilSeqn + ", orgFileName="
			+ orgFileName + ", newFileName=" + newFileName + "]";
	}
	
}
