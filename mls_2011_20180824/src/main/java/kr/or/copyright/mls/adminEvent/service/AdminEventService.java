package kr.or.copyright.mls.adminEvent.service;

public interface AdminEventService {
	
	public void campPartList() throws Exception;
	
	public void eventMgntList() throws Exception;
	
	public void eventMgntDetail() throws Exception;
	
	public void eventMgntSave() throws Exception;
}
 