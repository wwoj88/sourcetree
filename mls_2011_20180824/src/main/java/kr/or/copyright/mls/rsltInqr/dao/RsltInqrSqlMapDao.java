package kr.or.copyright.mls.rsltInqr.dao;

import java.util.List;

import kr.or.copyright.mls.inmtPrps.model.InmtPrps;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class RsltInqrSqlMapDao extends SqlMapClientDaoSupport implements RsltInqrDao{
	
	//private Log logger = LogFactory.getLog(getClass());
	
	// 신청내역리스트 
	public List rsltInqrList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.rsltInqrList", rghtPrps);
	}
	
	// 신청내역 건수 
	public int rsltInqrCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("RsltInqr.rsltInqrCount", rghtPrps));
	}
	
	// 권리찾기신청 기본정보 select
	public RghtPrps rghtPrpsDetl (RghtPrps rghtPrps){
		return (RghtPrps)getSqlMapClientTemplate().queryForObject("RsltInqr.rghtPrpsDetl", rghtPrps);
	}
	
	// 권리찾기 첨부파일리스트 
	public List rghtFileList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.rghtFileList", rghtPrps);
	}
	
	// 권리찾기 신청처리결과 목록
	public List rsltTrstWorksList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.rsltTrstWorksList", rghtPrps);
	}
	
	// 권리찾기 신청처리결과 상세
	public RghtPrps rsltTrstWorksDesc(RghtPrps rghtPrps){
		return (RghtPrps)getSqlMapClientTemplate().queryForObject("RsltInqr.rsltTrstWorksDesc", rghtPrps);
	}
	
	// 보상금 저작물목록
	public RghtPrps inmtRsltSelect(RghtPrps rghtPrps){
		return (RghtPrps)getSqlMapClientTemplate().queryForObject("RsltInqr.inmtRsltSelect", rghtPrps);
	}
	
	// 음악 권리찾기 저작물목록
	public List muscPrpsList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.muscPrpsList", rghtPrps);
	}

	// 어문 권리찾기 저작물목록
	public List bookPrpsList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.bookPrpsList", rghtPrps);
	}
	
	//	 뉴스 권리찾기 저작물목록
	public List newsPrpsList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.newsPrpsList", rghtPrps);
	}
	
	// 방송대본 권리찾기 저작물목록
	public List scriptPrpsList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.scriptPrpsList", rghtPrps);
	}
	
	// 이미지 권리찾기 저작물목록
	public List imagePrpsList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.imagePrpsList", rghtPrps);
	}
	
	// 영화 권리찾기 저작물목록
	public List mviePrpsList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.mviePrpsList", rghtPrps);
	}
	
	// 방송 권리찾기 저작물목록
	public List broadcastPrpsList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.broadcastPrpsList", rghtPrps);
	}
	
	// 보상금신청 기본정보 select
	public InmtPrps inmtPrpsDetl (RghtPrps rghtPrps){
		return (InmtPrps)getSqlMapClientTemplate().queryForObject("RsltInqr.inmtPrpsDetl", rghtPrps);
	}
	
	// 보상금신청처리결과 조회
	public List inmtPrpsRsltList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.listInmtPrpsRslt", rghtPrps);
	}
	
	// 신청목적구분코드(DEAL_STAT)
	public String inmtDealStatTotal(RghtPrps rghtPrps){
		String DealStat = (String)getSqlMapClientTemplate().queryForObject("RsltInqr.inmtDealStatTotal",rghtPrps);
		return DealStat; 
	}
	
	// 보상금신청저작물 목록
	public List inmtWorkList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.inmtWorkList", rghtPrps);
	}
	
	// 보상금 음제협 신청내역
	public List inmtKappRsltList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.inmtKappRsltList", rghtPrps);
	}
	
	// 보상금 음실연 신청내역
	public List inmtFokapoRsltList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.inmtFokapoRsltList", rghtPrps);
	}

	// 보상금 복전협 신청내역
	public List inmtKrtraRsltList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.inmtKrtraRsltList", rghtPrps);
	}

	// 보상금신청 저작물내역
	public List inmtTempRsltList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.inmtTempRsltList", rghtPrps);
	}

	// 보상금신청 첨부파일내역
	public List inmtFileList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.inmtFileList", rghtPrps);
	}

	// 보상금신청 전체처리상태 값
	public String inmtTotalDealStat(RghtPrps rghtPrps){
		String TotalDealStat = (String)getSqlMapClientTemplate().queryForObject("RsltInqr.inmtTotalDealStat", rghtPrps);
		return TotalDealStat;
	}
	
	// maxDealStat 구하기
	public String getMaxDealStat(RghtPrps rghtPrps){
		String DealStat = (String)getSqlMapClientTemplate().queryForObject("RsltInqr.maxDealStat",rghtPrps);
		return DealStat; 
	}
	
	// 신청내역테이블 삭제
	public void prpsDelete(RghtPrps rghtPrps){
		getSqlMapClientTemplate().delete("RsltInqr.prpsDelete", rghtPrps);
	}
	
	// 신청처리결과테이블 삭제
	public void prpsRsltDelete(RghtPrps rghtPrps){
		getSqlMapClientTemplate().delete("RsltInqr.prpsRsltDelete", rghtPrps);
	}
	
	// 음제협신청내역테이블 삭제
	public void kappPrpsDelete(RghtPrps rghtPrps){
		getSqlMapClientTemplate().delete("RsltInqr.kappPrpsDelete", rghtPrps);
	}
	
	// 음실연신청내역테이블 삭제
	public void fokapoPrpsDelete(RghtPrps rghtPrps){
		getSqlMapClientTemplate().delete("RsltInqr.fokapoPrpsDelete", rghtPrps);
	}
	
	// 복전협신청내역테이블 삭제
	public void krtraPrpsDelete(RghtPrps rghtPrps){
		getSqlMapClientTemplate().delete("RsltInqr.krtraPrpsDelete", rghtPrps);
	}
	
	// 신청내역<저작자>권리정보테이블 삭제
	public void prpsRsltRghtDelete(RghtPrps rghtPrps){
		getSqlMapClientTemplate().delete("RsltInqr.prpsRsltRghtDelete", rghtPrps);
	}
	
	// 첨부파일 내역테이블 삭제
	public void prpsFileDelete(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().delete("RsltInqr.prpsFileDelete", rghtPrps);
	}
	
	// 기타 권리찾기 저작물목록
	public List sidePrpsList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RsltInqr.sidePrpsList", rghtPrps);
	}
}
