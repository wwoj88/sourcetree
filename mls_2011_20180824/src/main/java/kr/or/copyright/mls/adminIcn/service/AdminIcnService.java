package kr.or.copyright.mls.adminIcn.service;


public interface AdminIcnService {

	public void muscList() throws Exception;
	
	public void bookList() throws Exception;
	
	public void privWorksList() throws Exception;
	
	public void privWorksDetailList() throws Exception;
	
	public void privWorksSave () throws Exception;
	
	public void getCommId() throws Exception;
	
	public void commIdSave() throws Exception;
	
	public void muscDetail() throws Exception;
	
	public void muscIcnUpdate() throws Exception;
}
