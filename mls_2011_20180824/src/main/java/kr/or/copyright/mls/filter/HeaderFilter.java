package kr.or.copyright.mls.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class HeaderFilter implements Filter{
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
		throws IOException, ServletException
	{
		((HttpServletResponse)res).setHeader("Strict-Transport-Security", "max-age=86400; includeSubDomains; preload");
		((HttpServletResponse)res).setHeader("X-Frame-Options", "SAMEORIGIN");
		//((HttpServletResponse)res).setHeader("Content-Security-Policy", "default-src 'self'");//크롬에 문제 생김 적용하지 않기로함
		/*((HttpServletResponse)res).setHeader("Content-Security-Policy", "script-src 'self’ ‘unsafe-eval'; object-src 'self'");*/
		//((HttpServletResponse)res).setHeader("X-XSS-Protection"," 1; mode=block");//나이스 신용인증 문제 생김
		((HttpServletResponse)res).addHeader("X-Content-Type-Options", "nosniff");
		((HttpServletResponse)res).addHeader("X-Download-Options","noopen");
		chain.doFilter(req, res);
	}

	public void destroy() {}
	public void init(FilterConfig arg0) throws ServletException {}
}
