package kr.or.copyright.mls.adminStatBoard.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import kr.or.copyright.mls.adminCommon.dao.AdminCommonDao;
import kr.or.copyright.mls.adminEmailMgnt.service.AdminEmailMgntService;
import kr.or.copyright.mls.adminStatBoard.dao.AdminStatBoardDao;
import kr.or.copyright.mls.adminStatMgnt.dao.AdminStatMgntDao;
import kr.or.copyright.mls.common.dao.CommonDao;
import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;
import kr.or.copyright.mls.support.util.StringUtil;

import com.tobesoft.platform.data.Dataset;

public class AdminStatBoardServiceImpl extends BaseService implements AdminStatBoardService{
	
	private AdminStatBoardDao adminStatBoardDao;
	private AdminStatMgntDao adminStatMgntDao;
	private AdminCommonDao adminCommonDao;
	private CommonDao commonDao;
	
	@Autowired
	private AdminEmailMgntService adminEmailMgntService;
	
	public void setAdminStatMgntDao(AdminStatMgntDao adminStatMgntDao){
		this.adminStatMgntDao = adminStatMgntDao;
	}
	
	public void setAdminStatBoardDao(AdminStatBoardDao adminStatBoardDao) {
		this.adminStatBoardDao = adminStatBoardDao;
	}
	
	public void setCommonDao(CommonDao commonDao){
		this.commonDao = commonDao;
	}
	
	public void setAdminCommonDao(AdminCommonDao adminCommonDao){
		this.adminCommonDao = adminCommonDao;
	}
	
	
	//이용승인신청 공고 - 엑셀다운 2014-11-10 이병원
	public void statBord01ExcelDown() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);
		
		List list = (List)adminStatBoardDao.statBord01ExcelDown(map);
		
		addList("ds_exceldown", list);
		
	}
	
	
	// 이용승인신청 공고 - 목록
	public void statBord01List() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List)adminStatBoardDao.statBord01List(map);
		List pageList = (List) adminStatBoardDao.statBord01ListCount(map);
		List pageCount = (List) adminStatBoardDao.statBord01RowCount(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_page", pageList);
		addList("ds_count", pageCount);
	}
	
	// 이용승인신청 공고 ,상당한 노력공고,이의신청 현황 - 상세
	public void statBord01Detail() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		List detailList = (List)adminStatBoardDao.statBord01Detail(map);
		List fileList = (List)adminStatBoardDao.statBord01File(map);
		List objectList = (List)adminStatBoardDao.statBord01Object(map);
		List objectFileList = (List)adminStatBoardDao.statBord01ObjectFile(map);
		// 보완요청 LIST
		List suplList = (List) adminStatBoardDao.selectBordSuplItemList(map);	
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", detailList);
		addList("ds_file", fileList);
		addList("ds_object", objectList);
		addList("ds_object_file", objectFileList);
		addList("ds_supl_list", suplList);
		
	}
	// 이용승인신청 공고- 저장
	public void statBord01Save() throws Exception {
		
		Dataset ds_list = getDataset("ds_list");     
		Dataset ds_file = getDataset("ds_file");
		
		String  row_status = null;
	
		//공고내용  INSERT
		for( int i=0;i<ds_list.getRowCount();i++ )
		{
			row_status = ds_list.getRowStatus(i);
			
			Map map1 = getMap(ds_list, i );
			if( row_status.equals("insert") == true )
			{
				adminStatBoardDao.statBord01Insert(map1); //공고게시판 등록
			}
		}
		
		//File Insert
		for( int i=0;i<ds_file.getRowCount();i++ )
		{
			row_status = ds_file.getRowStatus(i);
			Map map = getMap(ds_file, i );
			
			if( row_status.equals("insert") == true )
			{
				int attachSeqn = commonDao.getNewAttcSeqn();
				int bordSeqn = adminStatBoardDao.getMaxBordSeqn();
			
				Map upload=null;
				byte[] file = null;
			
				file = ds_file.getColumn(i, "CLIENT_FILE").getBinary();
				String fileName = ds_file.getColumnAsString(i, "REAL_FILE_NAME");
				upload = FileUtil.uploadMiFile2(fileName, file);
				
				map.put("BORD_SEQN", bordSeqn);
				map.put("ATTC_SEQN", attachSeqn);
				map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				map.put("FILE_PATH", upload.get("FILE_PATH"));
				map.put("FILE_NAME", fileName);
				
				
				System.out.println("########################################");
				System.out.println("########################################");
				System.out.println("########################################");
				System.out.println(fileName);
				System.out.println("########################################");
				System.out.println("########################################");
				System.out.println("########################################");
			
				adminStatBoardDao.mlBord02FileInsert(map); //첨부파일 등록
				adminStatBoardDao.statBord02FileInsert(map); //공고게시판 첨부파일 등록
			}
		}
		
	}
	public void statBord01RegiSearch() throws Exception{
		
		Dataset ds_recno = getDataset("ds_recno");
		ArrayList li = new ArrayList();
		
		for(int i=0; i<ds_recno.getRowCount(); i++){
			
			Map map = getMap(ds_recno,i);
			Map map2 = new HashMap();
			
			int receipt_no = adminStatBoardDao.statBord01Select(map);
			
			if(receipt_no !=0){
				
				map2.put("RECEIPT_NO", map.get("RECEIPT_NO"));
				
				li.add(map2);
				
			}
		}
		addList("ds_recno",li);
		
	}
	
	// 이용승인신청 공고 - 일괄등록
	public void statBord01RegiAll() throws Exception {
		
		Dataset ds_list = getDataset("ds_excel_regiAll");
		Dataset ds_recno = getDataset("ds_recno");
		
		ArrayList li = new ArrayList();
		Map map1 = getMap(ds_list,0);
		
		//등록자 아이디
		String openIdnt = (String)map1.get("OPEN_IDNT");
		//게시판구분 CD
		String bordCd = (String)map1.get("BORD_CD");	

		//	일괄등록, 파일등록 INSERT
		for(int i=0; i<ds_list.getRowCount(); i++){
			
			Map map = null;
			map = getMap(ds_list, i);
				
			String divs_cd = (String)map.get("DIVS_CD");
			
			//코드 구분
			if(divs_cd.equals("1")){
				divs_cd = "4";
			}else if(divs_cd.equals("2")){
				divs_cd = "1";
			}else if(divs_cd.equals("3")){
				divs_cd = "5";
			}

			map.put("DIVS_CD", divs_cd);
			map.put("OPEN_IDNT", openIdnt);
			map.put("RGST_IDNT", openIdnt);
			map.put("BORD_CD", bordCd);
			
		
			adminStatBoardDao.statBord01RegiAll(map); //일괄 등록
			
			String scan_yn = (String) map.get("SCAN_YN");
			
			if(scan_yn.equals("Y")){
				
				int attachSeqn = commonDao.getNewAttcSeqn(); 
				int bordSeqn = adminStatBoardDao.getMaxBordSeqn();
				
				Map upload=null;
				byte[] file = null;
				
				file = ds_list.getColumn(i, "CLIENT_FILE").getBinary();
				String fileName = ds_list.getColumnAsString(i, "REAL_FILE_NAME");
				
				upload = FileUtil.uploadMiFile2(fileName, file);
				
				map.put("BORD_SEQN", bordSeqn);
				map.put("ATTC_SEQN", attachSeqn);
				
				map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				map.put("FILE_PATH", upload.get("FILE_PATH"));
				map.put("FILE_NAME", fileName);

				
				adminStatBoardDao.mlBord02FileInsert(map); //첨부파일 등록
				adminStatBoardDao.statBord02FileInsert(map); //공고게시판 첨부파일 등록				
				
			}								
		}
		addList("ds_recno", li);		
	}
	
	// 이용승인신청 공고- 수정
	public void statBord01Modi() throws Exception {
		
		Dataset ds_list = getDataset("ds_list");     
		Dataset ds_file = getDataset("ds_file");
		Dataset ds_condition = getDataset("ds_condition");
		
		String  row_status = null;
		
		Map map2 =  getMap(ds_condition, 0);
	
		//공고내용   UPDATE처리
		for( int i=0;i<ds_list.getRowCount();i++ )
		{
			row_status = ds_list.getRowStatus(i);
			Map map1 = getMap(ds_list, i );
			
			if( row_status.equals("update") == true )
			{
				adminStatBoardDao.statBord01Update(map1); //공고게시판 수정
			}
		}
		
		//File update
		for( int i=0;i<ds_file.getRowCount();i++ )
		{
			Map map = getMap(ds_file, i );
			row_status = ds_file.getRowStatus(i);
			
			Map upload=null;
			byte[] file = null;
				
			file = ds_file.getColumn(i, "CLIENT_FILE").getBinary();
				
				if(file !=null){
					String fileName = ds_file.getColumnAsString(i, "REAL_FILE_NAME");
					int attachSeqn = commonDao.getNewAttcSeqn();
			
					upload = FileUtil.uploadMiFile2(fileName, file);
				
					map.put("ATTC_SEQN", attachSeqn);
					map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
					map.put("FILE_PATH", upload.get("FILE_PATH"));
					map.put("FILE_NAME", fileName);
				
					adminStatBoardDao.mlBord02FileInsert(map); //첨부파일 등록
					adminStatBoardDao.statBord02FileInsert(map); //공고게시판 첨부파일 등록
					
				}
			
		 }
		
		//FILE DELETE처리
		for( int i = 0 ; i< ds_file.getDeleteRowCount() ; i++ )
		{
			Map deleteMap = getDeleteMap(ds_file, i );
			adminStatBoardDao.statBord02FileDelete(deleteMap);
			
			String fileName =  ds_file.getDeleteColumn(i, "REAL_FILE_NAME").toString();
			
			 try 
			 {
				 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
			     if (file.exists()) 
			     { 
			    	 file.delete(); 
			     }
			  } 
			  catch (Exception e) 
			  {
			   e.printStackTrace();
			  } 
 	    }
		adminStatBoardDao.statBord02Modi(map2); //수정시 modi_dttm,modi_idnt 업데이트
	}
	
	
	// 이용승인신청 이의제기 진행상태 변경
	public void statObjcChange() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");   
		Dataset ds_object2 = getDataset("ds_object2");
		
		Map conditionMap = getMap(ds_condition, 0);
		
		String  row_status = null;
		
		
		for( int i=0; i<ds_object2.getRowCount();i++ ){
			
			row_status = ds_object2.getRowStatus(i);
			Map map = getMap(ds_object2, i );
			
			if( row_status.equals("insert") == true )
			{
				int attachSeqn = commonDao.getNewAttcSeqn();
				String fileName = ds_object2.getColumnAsString(i, "FILE_NAME");
			
				if(fileName != null && !fileName.equals(""))
				{
					map.put("ATTC_SEQN", attachSeqn);
					
					String filePath = "";
					String realFileNm = "";
					String fileSize = "";
					byte[] file = null;
					Map upload = null;
					
					if(ds_object2.getColumn(i, "ATTACH_FILE_CONTENT0") == null || ds_object2.getColumn(i, "ATTACH_FILE_CONTENT0").toString().equals(""))
					{
						filePath = ds_object2.getColumnAsString(i, "FILE_PATH");
						realFileNm = ds_object2.getColumnAsString(i, "REAL_FILE_NAME");
						fileSize = ds_object2.getColumnAsString(i, "FILE_SIZE");
					}else{
						file = ds_object2.getColumn(i, "ATTACH_FILE_CONTENT0").getBinary();
						upload = FileUtil.uploadMiFile(fileName, file);
						filePath = upload.get("FILE_PATH").toString();
						realFileNm = upload.get("REAL_FILE_NAME").toString();
						fileSize = ds_object2.getColumn(i, "ATTACH_FILE_SIZE0").toString();
					}
					
					Map fileMap = new HashMap();
					fileMap.put("ATTC_SEQN", attachSeqn);
					fileMap.put("FILE_ATTC_CD", "BO");
					fileMap.put("FILE_NAME", fileName);
					fileMap.put("FILE_PATH", filePath);
					fileMap.put("FILE_SIZE", fileSize);
					fileMap.put("REAL_FILE_NAME", realFileNm);
					fileMap.put("RGST_IDNT", map.get("USER_ID"));
					
					// 첨부파일 등록
					adminStatMgntDao.adminFileInsert(fileMap);
				}
				
				// 이의제기 신청정보 수정
				adminStatBoardDao.statBordObjcUpdate(map);
				
				if(ds_object2.getColumn(i, "WORKS_ID") == null || ds_object2.getColumn(i, "WORKS_ID").toString().equals("")){
				
					//이의제기 진행상태 처리결과 -기각일때
					if(ds_object2.getColumn(i, "STAT_OBJC_RSLT_CD").toString().equals("2")){
						adminStatBoardDao.statBordObjcRsltUpdate(map);
					}
				}
				
				// 이의제기신청 처리내역등록
				adminStatBoardDao.statBordObjcShisInsert(map);

				// 승인, 거절 시 메일,sms 보내기
				if(conditionMap.get("RGST_IDNT")!=null && (map.get("STAT_OBJC_CD").equals("2") || map.get("STAT_OBJC_CD").equals("3")) )
				{
					// 메일 보내기
					Map memMap = new HashMap();
					memMap.put("USER_IDNT", conditionMap.get("RGST_IDNT"));
					List memList = adminCommonDao.selectMemberInfo(memMap);
					memMap = (Map)memList.get(0);
					
					String statNm = "";
					if(map.get("STAT_OBJC_CD").equals("2")){
						statNm = "접수";
					}else if(map.get("STAT_OBJC_CD").equals("3")){
						statNm = "처리완료";
					}
					
					String rsltNm = "";
					if(map.get("STAT_OBJC_RSLT_CD").equals("1")){
						rsltNm = "승인";
					}else if(map.get("STAT_OBJC_RSLT_CD").equals("2")){
						rsltNm = "기각";
					}
					
					String host = Constants.MAIL_SERVER_IP; // smtp서버
					String to = memMap.get("MAIL").toString(); // 수신EMAIL
					String toName = memMap.get("USER_NAME").toString(); // 수신인
					String title = map.get("TITLE").toString();
					String subject = "[저작권찾기] 이의제기 "+statNm;
					
					String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
					String fromName = Constants.SYSTEM_NAME;
					
					if (memMap.get("MAIL_RECE_YSNO").equals("Y") && to != null && to != "") {
						
						// ------------------------- mail 정보
						// ----------------------------------//
						MailInfo info = new MailInfo();
						info.setFrom(from);
						info.setFromName(fromName);
						info.setHost(host);
						info.setEmail(to);
						info.setEmailName(toName);
						info.setSubject(subject);

						StringBuffer sb = new StringBuffer();
					    
					    
						sb.append("				<tr>");
						sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
						sb.append("					<p style=\"font-weight:bold; background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 이의제기 "+statNm+" 정보</p>");
						sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
						sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자 이름 : "+toName+" </li>");	
						sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : " + map.get("TITLE") +" </li>");
						sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청일자 : " + map.get("OBJC_RGST_DTTM") +" </li>");	
						sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 처리상태 : "+ statNm +" </li>");
						sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 처리결과 : "+ rsltNm +" </li>");
						sb.append("					</ul>");
						sb.append("					</td>");
						sb.append("				</tr>");

					    info.setMessage(sb.toString());
						info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
						
/*						
						sb.append("<div class=\"mail_title\">" + toName + "님, " +title+"의 이의제기 신청이  "+statNm+" 되었습니다. </div>");
						sb.append("<div class=\"mail_contents\"> ");
						sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
						sb.append("		<tr> ");
						sb.append("			<td> ");
						sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
						// 변경부분
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">제목(제호)</td> ");
						sb.append("						<td class=\"tdData\">" + map.get("TITLE") + "</td> ");
						sb.append("					</tr> ");
						
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">신청일자</td> ");
						sb.append("						<td class=\"tdData\">" + map.get("OBJC_RGST_DTTM") + "</td> ");
						sb.append("					</tr> ");
						
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">처리상태</td> ");
						sb.append("						<td class=\"tdData\">" + statNm + "</td> ");
						sb.append("					</tr> ");
						
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">처리결과</td> ");
						sb.append("						<td class=\"tdData\">" + rsltNm + "</td> ");
						sb.append("					</tr> ");
						
						
						sb.append("				</table> ");
						sb.append("			</td> ");
						sb.append("		</tr> ");
						sb.append("	</table> ");
						sb.append("</div> ");
						
						info.setMessage(new String(MailMessage.getChangeInfoMailText(sb.toString())));*/
						MailManager manager = MailManager.getInstance();

						info = manager.sendMail(info);
						if (info.isSuccess()) {
							System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());
						} else {
							System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
						}
						
					}
					
					// sms 보내기
					String smsTo = memMap.get("MOBL_PHON").toString();
					String smsFrom = Constants.SYSTEM_TELEPHONE;
					
					String smsMessage = "[저작권찾기]" +title+ "의 이의제기 신청이 "+statNm+" 되었습니다. ";
					
					if(memMap.get("SMS_RECE_YSNO") != null){
						if (memMap.get("SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {

							SMSManager smsManager = new SMSManager();

							System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
						}
					}
					
				} // end email, sms
			}
		}
	}
	
	
	// 이용승인신청 이의제기 진행상태내역조회
	public void adminStatObjcHist() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		List list = (List) adminStatBoardDao.adminStatObjcShisSelect(map);
		
		addList("ds_list", list);
	}
	
	
	//이용승인신청 승인공고 - 목록
	public void statBord02List() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);
	
		//DAO호출
		List list = (List)adminStatBoardDao.statBord02List(map);
		List pageList = (List)adminStatBoardDao.statBord0203RowList(map); //페이징카운트
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_page", pageList);
	}
	
	//이용승인신청 승인공고 - 엑셀다운 2014-11-11 이병
	public void statBord02ExcelDown() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);
		
		//엑셀 데이터 호출
		List list = (List)adminStatBoardDao.statBord02ExcelDown(map);
		
		addList("ds_exceldown", list);
		
	}

	
	//이용승인신청 승인공고(statBord02), 보상금 공탁공고(statBord03)- 상세
	public void statBordDetail() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition"); 			
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List detailList = (List)adminStatBoardDao.statBordDetail(map);
		List fileList = (List)adminStatBoardDao.statBord01File(map);		
		// 보완요청 LIST
		List suplList = (List) adminStatBoardDao.selectBordSuplItemList(map);	
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", detailList);
		addList("ds_file", fileList);
		addList("ds_supl_list", suplList);	
				
	}
	
	
	// 이용승인신청 승인공고 게시판 - 등록
	public void statBord02Regi() throws Exception {

		Dataset ds_list = getDataset("ds_list");
		Dataset ds_file = getDataset("ds_file");  
		
		String  row_status = null;
		
		//공고내용  INSERT
		for( int i=0;i<ds_list.getRowCount();i++ )
		{
			row_status = ds_list.getRowStatus(i);
			Map map1 = getMap(ds_list, i );
			
			if( row_status.equals("insert") == true )
			{
				adminStatBoardDao.statBord02Regi(map1); //공고게시판 등록
			}
		}
		
		
		//File Insert
		for( int i=0;i<ds_file.getRowCount();i++ )
		{
			Map map = getMap(ds_file, i );
			row_status = ds_file.getRowStatus(i);
			
			if( row_status.equals("insert") == true )
			{	
				int attachSeqn = commonDao.getNewAttcSeqn();
				int bordSeqn = adminStatBoardDao.getMaxBordSeqn();
			
				Map upload=null;
				byte[] file = null;
			
				file = ds_file.getColumn(i, "CLIENT_FILE").getBinary();
				System.out.println("#############################");
				System.out.println("#############################");
				System.out.println(file);
				String fileName = ds_file.getColumnAsString(i, "REAL_FILE_NAME");
				System.out.println(fileName);
				upload = FileUtil.uploadMiFile2(fileName, file);
				
				map.put("BORD_SEQN", bordSeqn);
				map.put("ATTC_SEQN", attachSeqn);
				map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				map.put("FILE_PATH", upload.get("FILE_PATH"));
				map.put("FILE_NAME", fileName);
			
				adminStatBoardDao.mlBord02FileInsert(map); //첨부파일 등록
				adminStatBoardDao.statBord02FileInsert(map); //공고게시판 첨부파일 등록	
			}
		}
		
	}
	public void statBord02RegiSearch() throws Exception{
		
		Dataset ds_recno = getDataset("ds_recno");
		ArrayList li = new ArrayList();
		
		for(int i=0; i<ds_recno.getRowCount(); i++){
			
			Map map = getMap(ds_recno,i);
			Map map2 = new HashMap();
			
			int receipt_no = adminStatBoardDao.statBord02Select(map);
			
			if(receipt_no !=0){
				
				map2.put("RECEIPT_NO", map.get("RECEIPT_NO"));
				li.add(map2);
			}
		}
		addList("ds_recno",li);
		
		
	}
	
	// 이용승인신청 승인공고 게시판 - 일괄등록
	public void statBord02RegiAll() throws Exception {
		
		Dataset ds_excel = getDataset("ds_excel_regiAll");
		Dataset ds_recno = getDataset("ds_recno");
		
		ArrayList li = new ArrayList();
		Map map = getMap(ds_excel, 0);
		Map map1 = null;
		//등록자 아이디
		String openIdnt = (String)map.get("OPEN_IDNT");
		
		//게시판구분 CD
		String bordCd = (String)map.get("BORD_CD");	
			
		//공고내용  INSERT
		for( int i=0;i<ds_excel.getRowCount();i++ ){	
			
			map1 = getMap(ds_excel, i);

			String divs_cd = (String)map.get("DIVS_CD");
			
			//코드 구분
			if(divs_cd.equals("1")){
				divs_cd = "4";
			}else if(divs_cd.equals("2")){
				divs_cd = "1";
			}else if(divs_cd.equals("3")){
				divs_cd = "5";
			}

			map1.put("DIVS_CD", divs_cd);
			map1.put("OPEN_IDNT", openIdnt);
			map1.put("RGST_IDNT", openIdnt);
			map1.put("BORD_CD", bordCd);
			
			adminStatBoardDao.statBord02RegiAll(map1);	
			
			String scan_yn = (String)map1.get("SCAN_YN");
			
			if(scan_yn.equals("Y")){
				
				int attachSeqn = commonDao.getNewAttcSeqn(); 
				int bordSeqn = adminStatBoardDao.getMaxBordSeqn();
				
				Map upload=null;
				byte[] file = null;
				
				file = ds_excel.getColumn(i, "CLIENT_FILE").getBinary();

				String fileName = ds_excel.getColumnAsString(i, "REAL_FILE_NAME");
				upload = FileUtil.uploadMiFile2(fileName, file);
				
				map1.put("BORD_SEQN", bordSeqn);
				map1.put("ATTC_SEQN", attachSeqn);
				
				map1.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				map1.put("FILE_PATH", upload.get("FILE_PATH"));
				map1.put("FILE_NAME", fileName);

				
				adminStatBoardDao.mlBord02FileInsert(map1); //첨부파일 등록
				adminStatBoardDao.statBord02FileInsert(map1); //공고게시판 첨부파일 등록	
		
			}
		}
		addList("ds_recno", li);					
	}
	
	// 이용승인신청 승인공고 게시판 - 수정
	public void statBord02Modi() throws Exception {

		Dataset ds_list = getDataset("ds_list");
		Dataset ds_file = getDataset("ds_file");  
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map2 =  getMap(ds_condition, 0);
		String  row_status = null;
	
		//공고내용  UPDATE처리
		for( int i=0;i<ds_list.getRowCount();i++ )
		{
			row_status = ds_list.getRowStatus(i);
			Map map1 = getMap(ds_list, i );
			
			if( row_status.equals("update") == true )
			{
				adminStatBoardDao.statBord02Update(map1); //공고게시판 수정
			}
		}
		
		//File update
		for( int i=0;i<ds_file.getRowCount();i++ )
		{
			Map map = getMap(ds_file, i );
			
			row_status = ds_file.getRowStatus(i);
		
			Map upload=null;
			byte[] file = null;
			
			file = ds_file.getColumn(i, "CLIENT_FILE").getBinary();
			
			if(file != null){
				
			String fileName = ds_file.getColumnAsString(i, "REAL_FILE_NAME");
			int attachSeqn = commonDao.getNewAttcSeqn();
			
			upload = FileUtil.uploadMiFile2(fileName, file);
			
			map.put("ATTC_SEQN", attachSeqn);
			map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
			map.put("FILE_PATH", upload.get("FILE_PATH"));
			map.put("FILE_NAME", fileName);
			
			adminStatBoardDao.mlBord02FileInsert(map); //첨부파일 등록
			adminStatBoardDao.statBord02FileInsert(map); //공고게시판 첨부파일 등록
			
			}
		}
		
		//FILE DELETE처리
		for( int i = 0 ; i< ds_file.getDeleteRowCount() ; i++ )
		{
			Map deleteMap = getDeleteMap(ds_file, i );
			
			adminStatBoardDao.statBord02FileDelete(deleteMap);
			String fileName =  ds_file.getDeleteColumn(i, "REAL_FILE_NAME").toString();
			
			 try 
			 {
				 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
			     if (file.exists()) 
			     { 
			    	 file.delete(); 
			     }
			  } 
			  catch (Exception e) 
			  {
			   e.printStackTrace();
			  } 
 	    }
		adminStatBoardDao.statBord02Modi(map2); //수정시 modi_dttm,modi_idnt 업데이트
	}
	
	//이용승인신청 승인공고 게시판 - 삭제
	public void statBord02Delete() throws Exception {
		Dataset ds_list = getDataset("ds_list");
		Map map = getMap(ds_list,0);
		
		adminStatBoardDao.statBord02Delete(map);
		
	}
	
	//보상금 공탁 공고 게시판 - 목록
	public void statBord03List() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");    
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List)adminStatBoardDao.statBord03List(map);
		List pageList = (List)adminStatBoardDao.statBord0203RowList(map); //페이징카운트
		List pageCount = (List)adminStatBoardDao.statBord03RowCount(map); //카운트
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_page", pageList);
		addList("ds_count", pageCount);
		
	}
	
	
	//보상금 공탁 공고 게시판 - 공고승인
	public void statBord03Mgnt()throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition,0);
		
		adminStatBoardDao.statBord03Mgnt(map);
		
	}

	
	//상당한 노력 공고 게시판 -목록
	public void statBord04List() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);
				
		//DAO호출
		List list = (List)adminStatBoardDao.statBord04List(map);
		List pageList = (List)adminStatBoardDao.statBord04RowList(map); //페이징카운트
		List pageCount = (List)adminStatBoardDao.statBord04RowCount(map); //카운트
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_page", pageList);
		addList("ds_count", pageCount);
		
	}
	
	//법정허락 대상 저작물 -목록
	public void statBord05List() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");  
		Map map = getMap(ds_condition, 0);
				
		//DAO호출
		List list = (List)adminStatBoardDao.statBord05List(map);
		List pageList = (List)adminStatBoardDao.statBord05RowList(map); //페이징카운트
		List pageCount = (List)adminStatBoardDao.statBord05RowCount(map); //페이징카운트
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_page", pageList);
		addList("ds_count", pageCount);
		
	}
	
	//법정허락 대상 저작물 - 상세
	public void statBord05Detail() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List)adminStatBoardDao.statBord05Detail(map);
		List objectList = (List)adminStatBoardDao.statBord05Object(map);
		List objectFileList = (List)adminStatBoardDao.statBord05ObjectFile(map);
	
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_object", objectList);
		addList("ds_object_file", objectFileList);
		
	}

	public void objeList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);
				
		//DAO호출
		List list = (List)adminStatBoardDao.objeList(map);
		List pageList = (List)adminStatBoardDao.objeRowList(map); //페이징 카운트
		//List pageCount = (List) adminStatBoardDao.objeRowCount(map);
		

		HashMap countMap1 = (HashMap) pageList.get(0);
		String resultCnt = countMap1.get("COUNT").toString();//구분값 1 카운트
		
		HashMap countMap = new HashMap();
		countMap.put("COUNT", resultCnt);
		
		
		List pageCount = new ArrayList();
		pageCount.add(countMap);
		
		HashMap aaa = (HashMap) pageCount.get(0);
		System.out.println(aaa.get("COUNT"));
		for(int i=2; i<4; i++){
		    map.put("SCH_STAT_OBJC_CD", Integer.toString(i));
		    List innerPageList = (List)adminStatBoardDao.objeRowList(map); //페이징 카운트
		    
		    countMap1 = (HashMap) innerPageList.get(0);
		    resultCnt = countMap1.get("COUNT").toString();//구분값 1 카운트
			
		    countMap = new HashMap();
		    countMap.put("COUNT", resultCnt);
			
		    pageCount.add(countMap);
		    
		    
		    int j = i-1;
		    aaa = (HashMap) pageCount.get(j);
		    System.out.println(aaa.get("COUNT"));
		}
		
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_page", pageList);
		addList("ds_count", pageCount);
		
		
	}

	public void statBord06List() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     

		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List)adminStatBoardDao.statBord06List(map);
		List pageList = (List) adminStatBoardDao.statBord06RowList(map);
		List pageCount = (List) adminStatBoardDao.statBord06RowCount(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_page", pageList);
		addList("ds_count", pageCount);		
		
	}

	public void statBord06Detail() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List)adminStatBoardDao.statBord06Detail(map); //상세보기
		List shistFile = (List)adminStatBoardDao.statBord06File(map); //첨부파일
		List coptHodrList = (List)adminStatBoardDao.statBord06CoptHodr(map); //저작권자
		List shistList = (List)adminStatBoardDao.statBord06Shist(map); //진행상태 상세
		List suplList = (List)adminStatBoardDao.selectWorksSuplItemList(map); //진행상태 상세
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_file", shistFile);
		addList("ds_copt_hodr", coptHodrList);
		addList("ds_shist", shistList);
		addList("ds_supl_list", suplList);
		
	}

	public void stat06ShistChange() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition"); 
		Dataset ds_shist = getDataset("ds_shist");
		
		Map conditionMap = getMap(ds_condition, 0);
		
		String  row_status = null;
		
		for( int i=0; i<ds_shist.getRowCount();i++ ){
			
			row_status = ds_shist.getRowStatus(i);
			
			Map map = getMap(ds_shist, i );
			
			if( row_status.equals("update") == true )
			{
				int attachSeqn = commonDao.getNewAttcSeqn();
				String fileName = ds_shist.getColumnAsString(i, "FILE_NAME");
			
				if(fileName != null && !fileName.equals(""))
				{
					map.put("ATTC_SEQN", attachSeqn);
					
					String filePath = "";
					String realFileNm = "";
					String fileSize = "";
					byte[] file = null;
					Map upload = null;
					
					if(ds_shist.getColumn(i, "ATTACH_FILE_CONTENT0") == null || ds_shist.getColumn(i, "ATTACH_FILE_CONTENT0").toString().equals(""))
					{
						filePath = ds_shist.getColumnAsString(i, "FILE_PATH");
						realFileNm = ds_shist.getColumnAsString(i, "REAL_FILE_NAME");
						fileSize = ds_shist.getColumnAsString(i, "FILE_SIZE");
					}else{
						file = ds_shist.getColumn(i, "ATTACH_FILE_CONTENT0").getBinary();
						upload = FileUtil.uploadMiFile(fileName, file);
						filePath = upload.get("FILE_PATH").toString();
						realFileNm = upload.get("REAL_FILE_NAME").toString();
						fileSize = ds_shist.getColumn(i, "ATTACH_FILE_SIZE0").toString();
					}
					
					Map fileMap = new HashMap();
					fileMap.put("ATTC_SEQN", attachSeqn);
					fileMap.put("FILE_ATTC_CD", "BO");
					fileMap.put("FILE_NAME_CD", 99);
					fileMap.put("FILE_NAME", fileName);
					fileMap.put("FILE_PATH", filePath);
					fileMap.put("FILE_SIZE", fileSize);
					fileMap.put("REAL_FILE_NAME", realFileNm);
					fileMap.put("RGST_IDNT", map.get("RGST_IDNT"));
					fileMap.put("WORKS_ID", map.get("WORKS_ID"));
					
					// 첨부파일 등록
					adminStatMgntDao.adminFileInsert(fileMap);
					adminStatBoardDao.statBord06FileInsert(fileMap);
				}
				
				// 진행상태 정보 수정
				adminStatBoardDao.statBord06ShistUpdate(map);
				
				//진행상태 처리완료-승인일때 업데이트
				if(map.get("RSLT_CD").equals("1")){
				adminStatBoardDao.statBord06RsltMgnt(map);
				}
				// 진행상태  처리내역등록
				adminStatBoardDao.statBord06ShistInsert(map);
				
				// 승인, 거절 시 메일,sms 보내기
				if(conditionMap.get("RGST_IDNT")!=null && (map.get("STAT_CD").equals(2.0) || map.get("STAT_CD").equals(3.0)) )
				{
					// 메일 보내기
					Map memMap = new HashMap();
					memMap.put("USER_IDNT", conditionMap.get("RGST_IDNT"));
					List memList = adminCommonDao.selectMemberInfo(memMap);
					memMap = (Map)memList.get(0);
					
					String statNm = "";
					if(map.get("STAT_CD").equals(2.0)){
						statNm = "접수";
					}else if(map.get("STAT_CD").equals(3.0)){
						statNm = "처리완료";
					}
					String rsltNm = "";
					if(map.get("RSLT_CD").equals("1")){
						rsltNm = "신청승인";
						statNm = "신청승인";
					}else if(map.get("RSLT_CD").equals("2")){
						rsltNm = "신청기각";
						statNm = "신청기각";
					}
					String host = Constants.MAIL_SERVER_IP; // smtp서버
					String to = memMap.get("MAIL").toString(); // 수신EMAIL
					String toName = memMap.get("USER_NAME").toString(); // 수신인
					String title = map.get("TITLE").toString();
					String subject = "[저작권찾기] 저작권자 찾기 위한 상당한 노력 신청  "+statNm;
					
					String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
					String fromName = Constants.SYSTEM_NAME;
					
					if (memMap.get("MAIL_RECE_YSNO").equals("Y") && to != null && to != "") {
						
						// ------------------------- mail 정보
						// ----------------------------------//
						MailInfo info = new MailInfo();
						info.setFrom(from);
						info.setFromName(fromName);
						info.setHost(host);
						info.setEmail(to);
						info.setEmailName(toName);
						info.setSubject(subject);

						StringBuffer sb = new StringBuffer();
						
					    
						sb.append("				<tr>");
						sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
						sb.append("					<p style=\"font-weight:bold; background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 저작권자 찾기 위한 상당한 노력 신청 "+statNm+" 정보</p>");
						sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
						sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자 이름 : "+toName+" </li>");	
						sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : " + title +" </li>");
						sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청일자 : " + map.get("RGST_DTTM") +" </li>");	
						sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 처리상태 : "+ statNm +" </li>");
						sb.append("					</ul>");
						sb.append("					</td>");
						sb.append("				</tr>");

					    info.setMessage(sb.toString());
						info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
						
					/*	sb.append("<div class=\"mail_title\">" + toName + "님, " +title+"의 저작권자 찾기 위한 상당한 노력 신청이  ["+statNm+"] 되었습니다. </div>");
						sb.append("<div class=\"mail_contents\"> ");
						sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
						sb.append("		<tr> ");
						sb.append("			<td> ");
						sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
						// 변경부분
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">제호</td> ");
						sb.append("						<td class=\"tdData\">" + map.get("TITLE") + "</td> ");
						sb.append("					</tr> ");
						
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">신청일자</td> ");
						sb.append("						<td class=\"tdData\">" + map.get("RGST_DTTM") + "</td> ");
						sb.append("					</tr> ");
						
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">처리상태/결과</td> ");
						sb.append("						<td class=\"tdData\">" + statNm + "</td> ");
						sb.append("					</tr> ");
						
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">처리결과</td> ");
						sb.append("						<td class=\"tdData\">" + rsltNm + "</td> ");
						sb.append("					</tr> ");
						
						
						
						sb.append("				</table> ");
						sb.append("			</td> ");
						sb.append("		</tr> ");
						sb.append("	</table> ");
						sb.append("</div> ");
						
						info.setMessage(new String(MailMessage.getChangeInfoMailText(sb.toString())));*/
						MailManager manager = MailManager.getInstance();

						info = manager.sendMail(info);
						if (info.isSuccess()) {
							System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());
						} else {
							System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
						}
						
					}
					
					// sms 보내기
					String smsTo = memMap.get("MOBL_PHON").toString();
					String smsFrom = Constants.SYSTEM_TELEPHONE;
					
					String smsMessage = "[저작권찾기]" +title+ "의 저작권자 찾기 위한 상당한 노력 신청이 ["+statNm+"] 되었습니다. ";
					
					if(memMap.get("SMS_RECE_YSNO") != null){
						if (memMap.get("SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {

							SMSManager smsManager = new SMSManager();

							System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
						}
					}
					
				} // end email, sms
			}
		}
	}
	
	public void statBord06ShistList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     

		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List)adminStatBoardDao.statBord06ShistList(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		
	}
	
	//법정허락 대상 저작물 -목록 삭제
		public void statBord07Update() throws Exception {
			
			Dataset ds_delList = getDataset("ds_delList");  
			//ds_delList.printDataset();		
		for( int i = 0; i<ds_delList.getRowCount(); i++ ) {
						
			Map deleteMap = getMap(ds_delList, i);
			//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+deleteMap.toString());
			adminStatBoardDao.statBord07Update(deleteMap);
			
		}
	
	}
		

	// 법정허락 대상 저작물 - 일괄 삭제
			public void statBord07UpdateAll() throws Exception {
				
					
					Dataset ds_excel = getDataset("ds_excel");  
				//	Dataset ds_fileList = getDataset("ds_fileList");				// 첨부파일
					
					
					// 삭제처리 전에 현재 최대상태값을 확인
					List chkDelList = new ArrayList();
					
//					String sChk = "N";			// 신청이상 상태 유무
					String sRowNum = "";		// rownum
					
					for( int i = 0; i<ds_excel.getRowCount(); i++ ) {
						
						Map deleteMap = getMap(ds_excel, i);
						
					}
					
					// return 할 List 생성
					HashMap chkDelMap = new HashMap();
					
					//chkDelMap.put("CHK", sChk);
					chkDelMap.put("ROW_NUM", sRowNum);
					
					chkDelList.add(0,chkDelMap);
				
					
					// 삭제처리 
						for( int i = 0; i<ds_excel.getRowCount(); i++ ) {
							
							Map deleteMap = getMap(ds_excel, i);
							
							adminStatBoardDao.statBord07UpdateAll(deleteMap);			// DELETE ML_KAPP_PRPS
						}
						
						// 강명표 추가 START
						Map map = getMap(ds_excel, 0);
						// 강명표 추가 END
						
					}	

		
	// 메인화면 - 미공고 목록 (저작권자 조회공고, 보상금 공탁)
	public void mainStatBordList01() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     

		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List)adminStatBoardDao.mainStatBordList01(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
	}
	
	// 메인화면 - 이의신청 목록 (이용승인신청공고, 저작권자 조회공고, 법정허락대상물)
	public void mainStatBordList02() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     

		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List)adminStatBoardDao.mainStatBordList02(map);
		
		
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
	}
	
	//  저작권자 조회공고, 보상금 공탁공고 처리상태 수정(신청,보완,신청승인,반려)
	public void statBordStatUpdate() throws Exception{
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);
		
		adminStatBoardDao.statBordStatUpdate(map);
	}
	//  저작권자 조회공고, 보상금 공탁공고 삭제
	public void statBordStatDeltUpdate() throws Exception{
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);
		
		adminStatBoardDao.statBordStatDeltUpdate(map);
	}
	//  저작권자 조회공고, 보상금 공탁공고 상세보기(보완처리용)
	public void statBordSuplDetail() throws Exception {
		Dataset ds_condition = getDataset("ds_condition");
		Dataset ds_genre = getDataset("ds_genre");
		
		Map map = getMap(ds_condition, 0);
		Map map2 = getMap(ds_genre, 0);
		
		List detailList = (List)adminStatBoardDao.statBord01Detail(map);
		List fileList = (List)adminStatBoardDao.statBord01File(map);
		List suplCdList = (List)adminStatBoardDao.selectCdList(map);
		List genreCdList = (List)adminStatBoardDao.selectCdList(map2);
		
		addList("ds_list", detailList);
		addList("ds_file", fileList);
		addList("ds_code", suplCdList);	
		addList("ds_genre", genreCdList);
		
		
	}
	//  저작권자 조회공고, 보상금 공탁공고 공고정보 수정 및 보완이력 등록 
	public void statBordSuplRegi() throws Exception {
		Dataset ds_supl = getDataset("ds_supl");
		Dataset ds_list = getDataset("ds_list");
		Dataset ds_admin = getDataset("ds_admin");
		
		List<Map> sendList = new ArrayList();		
		List<Map> contentList = new ArrayList();
		Map sendMap = new HashMap();
		Map mailMap = new HashMap();
		String msgStorCd ="";	
		
		Map map2 = getMap(ds_supl, 0);
		int suplId=adminStatBoardDao.selectMaxSuplId();
		int suplSeq=adminStatBoardDao.selectMaxSuplSeq(map2);
		int bordCd = Integer.parseInt(map2.get("BORD_CD").toString());
		
		if(bordCd==1){
			map2.put("SUPL_CD", 1);	
			msgStorCd="4";
		}
		else if (bordCd==5){
			map2.put("SUPL_CD", 2);
			msgStorCd="5";
		}
		map2.put("SUPL_SEQ", suplSeq);
		map2.put("SUPL_ID", suplId);		
		adminStatBoardDao.suplIdInsert(map2);
		adminStatBoardDao.suplHistInsert(map2);
		

		for(int i=0;i<ds_supl.getRowCount();i++){			
			Map map = getMap(ds_supl, i);
			map.put("SUPL_SEQ", suplSeq);
			map.put("SUPL_ID", suplId);			
			adminStatBoardDao.suplIdItemInsert(map);		
			
			//메일 발송 내용 
			Map contentMap = new HashMap();
			String preItem = (String)map.get("PRE_ITEM");
			String postItem = (String)map.get("POST_ITEM");
			
			contentMap.put("RNUM", i+1);			
			contentMap.put("SUPL_ITEM", map.get("SUPL_ITEM"));
			contentMap.put("CONTENT","["+preItem+"] 에서 ["+postItem+"] (으)로 보완");
			
			contentList.add(contentMap);			
		}
		
		Map map3 = getMap(ds_list, 0);
		adminStatBoardDao.statBordSuplUpdate(map3);	
		
		
		/* 메일 발송준비 */
		
		String tite=(String)map3.get("TITE");//저작물 제호

		StringBuffer sb = new StringBuffer();				
		sb.append("				<tr>");
		sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">");
		sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">보완정보</p>");
		sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
		sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 대상 저작물 : "+tite+"</li>");
		//sb.append("						<li style=\"background:url(/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">신청일자 : 0000.00.00</li>");
		sb.append("					</ul>");
		sb.append("					</td>");
		sb.append("				</tr>");
		sb.append("				<tr>");
		sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
		sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">진행현황</p>");
		sb.append("					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">");
		sb.append("						<thead>");
		sb.append("							<tr>");
		sb.append("								<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"10%\">번호</th>");				
		sb.append("								<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">공고항목</th>");
		sb.append("								<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">보완 세부내역</th>");
		sb.append("							</tr>");
		sb.append("						</thead>");
		sb.append("						<tbody>");
		for(int i=0;i<contentList.size();i++){	
			sb.append("							<tr>");		
			sb.append("							<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+contentList.get(i).get("RNUM")+"</td>");
			sb.append("							<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+contentList.get(i).get("SUPL_ITEM")+"</td>");
			sb.append("							<td style=\"padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+contentList.get(i).get("CONTENT")+"</td>");
			sb.append("							</tr>");
		}
		sb.append("						</tbody>");
		sb.append("					</table>");
		sb.append("				</td>");
		sb.append("			</tr>");		
		
		mailMap.put("CONTENT_HTML", new String(sb));	
		
		Map adminMap = getMap(ds_admin, 0);
		
		mailMap.put("TITLE", "공고정보 보완처리가 완료되었습니다.");// 메일제목
		mailMap.put("MSG_STOR_CD", msgStorCd);
		mailMap.put("RGST_IDNT",(String)adminMap.get("RGST_IDNT"));
		mailMap.put("SEND_MAIL_ADDR",(String)adminMap.get("SEND_MAIL_ADDR"));
		mailMap.put("SUPL_ID",suplId);
	
		sendMap.put("RECV_MAIL_ADDR", (String)map3.get("RECV_MAIL_ADDR"));
		sendMap.put("RECV_CD", 1);
		sendMap.put("RECV_NAME",(String)map3.get("RGST_NAME"));
		sendMap.put("SEND_MAIL_ADDR",(String)adminMap.get("SEND_MAIL_ADDR"));
		
		sendList.add(sendMap);
		/*	mailMap 필요 컬럼	  	
		 *	MSG_STOR_CD - 메세지함 구분코드
		 *	TITLE - 메일제목
		 *	CONTENT - 메일내용 txt
		 *	CONTENT_HTML - 메일내용 HTML
		 *	RGST_IDNT - 발신자 아이디
		 *	SEND_MAIL_ADDR - 발신자 메일주소
		 *	SUPL_ID - 보완 아이디
		 * 
		 *  sendList Map 필요 컬럼
		 *  RECV_CD - 수신구분코드
		 *  RECV_MAIL_ADDR - 수신자 메일 주소
		 *  RECV_NAME - 수신자 이름
		 *  SEND_MAIL_ADDR - 발신자 메일주소	   
		 */
		
		adminEmailMgntService.sendEmail(sendList, mailMap);

		
	}
	
	//상당한노력신청 저작권자 조회 공고내용 보완
	public void statBord06SuplDetail() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List)adminStatBoardDao.statBord06Detail(map); //상세보기
		List shistFile = (List)adminStatBoardDao.statBord06File(map); //첨부파일
		List suplCdList = (List)adminStatBoardDao.selectCdList(map);
		
	
		//hmap를 dataset로 변환한다.
		addList("ds_list", list);
		addList("ds_file", shistFile);
		addList("ds_code", suplCdList);		
	}
	
	//상당한노력신청 저작권자 조회 공고내용 수정 및 보완이력 등록 
	public void statBord06SuplRegi() throws Exception {
		
		Dataset ds_supl = getDataset("ds_supl");
		Dataset ds_list = getDataset("ds_list");		
	
		Dataset ds_admin = getDataset("ds_admin");
		
		List<Map> sendList = new ArrayList();
		List<Map> contentList = new ArrayList();
		Map sendMap = new HashMap();
		Map mailMap = new HashMap();
		String msgStorCd ="6";
		
		
		Map map2 = getMap(ds_supl, 0);		
		int suplId=adminStatBoardDao.selectMaxSuplId();
		int suplSeq=adminStatBoardDao.selectMaxWorksSuplSeq(map2);		
		
		map2.put("SUPL_CD", 3);
		map2.put("SUPL_SEQ", suplSeq);
		map2.put("SUPL_ID", suplId);		

		adminStatBoardDao.suplIdInsert(map2);
		adminStatBoardDao.suplWorksInsert(map2);
		
		for(int i=0;i<ds_supl.getRowCount();i++){			
			Map map = getMap(ds_supl, i);
			map.put("SUPL_SEQ", suplSeq);
			map.put("SUPL_ID", suplId);			
			adminStatBoardDao.suplIdItemInsert(map);
			
			
			//메일 발송 내용 
			Map contentMap = new HashMap();
			String preItem = (String)map.get("PRE_ITEM");
			String postItem = (String)map.get("POST_ITEM");
			
			contentMap.put("RNUM", i+1);			
			contentMap.put("SUPL_ITEM", map.get("SUPL_ITEM"));
			contentMap.put("CONTENT","["+preItem+"] 에서 ["+postItem+"] (으)로 보완");
			
			contentList.add(contentMap);	
			
		}
		Map map3 = getMap(ds_list, 0);
		adminStatBoardDao.statBord06SuplUpdate(map3);
		
		
		/* 메일 발송준비 */
		
		String tite=(String)map3.get("WORKS_TITLE");//저작물 제호

		StringBuffer sb = new StringBuffer();				
		sb.append("				<tr>");
		sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">");
		sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">보완정보</p>");
		sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
		sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 대상 저작물 : "+tite+"</li>");
		//sb.append("						<li style=\"background:url(/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">신청일자 : 0000.00.00</li>");
		sb.append("					</ul>");
		sb.append("					</td>");
		sb.append("				</tr>");
		sb.append("				<tr>");
		sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
		sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">진행현황</p>");
		sb.append("					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">");
		sb.append("						<thead>");
		sb.append("							<tr>");
		sb.append("								<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"10%\">번호</th>");				
		sb.append("								<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">공고항목</th>");
		sb.append("								<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">보완 세부내역</th>");
		sb.append("							</tr>");
		sb.append("						</thead>");
		sb.append("						<tbody>");
		for(int i=0;i<contentList.size();i++){	
			sb.append("							<tr>");		
			sb.append("							<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+contentList.get(i).get("RNUM")+"</td>");
			sb.append("							<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+contentList.get(i).get("SUPL_ITEM")+"</td>");
			sb.append("							<td style=\"padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+contentList.get(i).get("CONTENT")+"</td>");
			sb.append("							</tr>");
		}
		sb.append("						</tbody>");
		sb.append("					</table>");
		sb.append("				</td>");
		sb.append("			</tr>");		
		
		mailMap.put("CONTENT_HTML", new String(sb));	
		
		Map adminMap = getMap(ds_admin, 0);
		
		mailMap.put("TITLE", "상당한노력 신청정보 보완처리가 완료되었습니다.");// 메일제목
		mailMap.put("MSG_STOR_CD", msgStorCd);
		mailMap.put("RGST_IDNT",(String)adminMap.get("RGST_IDNT"));
		mailMap.put("SEND_MAIL_ADDR",(String)adminMap.get("SEND_MAIL_ADDR"));
		mailMap.put("SUPL_ID",suplId);
	
		sendMap.put("RECV_MAIL_ADDR", (String)map3.get("RECV_MAIL_ADDR"));
		sendMap.put("RECV_CD", 1);
		sendMap.put("RECV_NAME",(String)map3.get("RGST_NAME"));
		sendMap.put("SEND_MAIL_ADDR",(String)adminMap.get("SEND_MAIL_ADDR"));
		
		sendList.add(sendMap);
		/*	mailMap 필요 컬럼	  	
		 *	MSG_STOR_CD - 메세지함 구분코드
		 *	TITLE - 메일제목
		 *	CONTENT - 메일내용 txt
		 *	CONTENT_HTML - 메일내용 HTML
		 *	RGST_IDNT - 발신자 아이디
		 *	SEND_MAIL_ADDR - 발신자 메일주소
		 *	SUPL_ID - 보완 아이디
		 * 
		 *  sendList Map 필요 컬럼
		 *  RECV_CD - 수신구분코드
		 *  RECV_MAIL_ADDR - 수신자 메일 주소
		 *  RECV_NAME - 수신자 이름
		 *  SEND_MAIL_ADDR - 발신자 메일주소	   
		 */
		
		adminEmailMgntService.sendEmail(sendList, mailMap);

	
	}
	
	//상당한노력신청 - 저작권자 조회공고 삭제
	public void statBord06DeltUpdate() throws Exception{
		Dataset ds_condition = getDataset("ds_condition");	
		Map map = getMap(ds_condition, 0);
		adminStatBoardDao.statBord06DeltUpdate(map);
	}
	// 신청정보 보완안내 메일발신내역
	public void selectBordSuplSendList() throws Exception{
		Dataset ds_condition = getDataset("ds_condition");	
		Map map = getMap(ds_condition, 0);
		List list = (List) adminStatBoardDao.selectBordSuplSendList(map);
		
		addList("ds_list", list);
		
	}
	
	// 메일발송건 별 보완내역 조회
	public void statBordSuplList() throws Exception{
		Dataset ds_condition = getDataset("ds_condition");	
		Map map = getMap(ds_condition, 0);
		List list = new ArrayList();
		String msgStorCd = StringUtil.nullToEmpty((String)map.get("MSG_STOR_CD"));
		//System.out.println(msgStorCd);
		if("6".equals(msgStorCd)){	//상당한 노력
			list = (List) adminStatBoardDao.selectWorksSuplItemList(map);	
		}else if("5".equals(msgStorCd)){//보상금 공탁공고(5)
			map.put("BIG_CODE", "89");//보상금 공탁공고 보완항목 코드값
			list = (List) adminStatBoardDao.selectBordSuplItemList(map);						
		}else if("4".equals(msgStorCd)){//저작권자 조회공고(4), 
			map.put("BIG_CODE", "88");//저작권자 조회공고 보완항목 코드값
			list = (List) adminStatBoardDao.selectBordSuplItemList(map);	
		}	
		addList("ds_supl_list", list);
		
	}
	

}