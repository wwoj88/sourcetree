package kr.or.copyright.mls.myStat.model;

import kr.or.copyright.mls.common.BaseObject;

public class StatApplicationShis extends BaseObject {
	
	private String applyWriteYmd;   //신청서작성YMD
	private int applyWriteSeq;      //신청서작성SEQ
	private int statCd;             //진행상태CD
	private String chngDttm;        //변경일자
	private String rgstIdnt;
	private String statMemo;        //진행상태비고
	private int attcSeqn;
	
	private String statCdNm;
	
	public int getApplyWriteSeq() {
		return applyWriteSeq;
	}
	public void setApplyWriteSeq(int applyWriteSeq) {
		this.applyWriteSeq = applyWriteSeq;
	}
	public String getApplyWriteYmd() {
		return applyWriteYmd;
	}
	public void setApplyWriteYmd(String applyWriteYmd) {
		this.applyWriteYmd = applyWriteYmd;
	}
	public int getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(int attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public String getChngDttm() {
		return chngDttm;
	}
	public void setChngDttm(String chngDttm) {
		this.chngDttm = chngDttm;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public int getStatCd() {
		return statCd;
	}
	public void setStatCd(int statCd) {
		this.statCd = statCd;
	}
	public String getStatMemo() {
		return statMemo;
	}
	public void setStatMemo(String statMemo) {
		this.statMemo = statMemo;
	}
	public String getStatCdNm() {
		return statCdNm;
	}
	public void setStatCdNm(String statCdNm) {
		this.statCdNm = statCdNm;
	}

	
}