package kr.or.copyright.mls.stat.model;

/**
 * @Class Name : MlStatBordVO.java
 * @Description : MlStatBord VO class
 * @Modification Information
 *
 * @author ����ȣ
 * @since 2012.08.06
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public class MlStatWorksVO extends MlStatWorksDefaultVO{
    private static final long serialVersionUID = 1L;
    
    /** WORKS_ID */
    private String worksId;
    
    /** GENRE_CD */
    private String genreCd;
    
    /** RGST_DTTM */
    private String rgstDttm;
    
    /** RGST_IDNT */
    private String rgstIdnt;
    
    /** MODI_DTTM */
    private String modiDttm;
    
    /** MODI_IDNT */
    private String modiIdnt;
    
    /** WORKS_TITLE */
    private String worksTitle;
    
    /** WORKS_DIVS_CD */
    private String worksDivsCd;
    
    /** SYST_EFFORT_STAT_CD */
    private String systEffortStatCd;
    
    /** STAT_WORKS_YN */
    private String statWorksYn;
    
    /** SYST_EFFORT_STAT_DTTM */
    private String systEffortStatDttm;
    
    /** OBJC_YN */
    private String objcYn;
    
    
    
    /** ADD table DIV */
    private String tDivs;
    
    /** ADD openDttm */
    private String openDttm;
    
    /** ADD coptHodr */
    private String coptHodr;
    
    /** ADD mediOpenDate */
    private String mediOpenDate;
    


    public String getOpenDttm() {
        return openDttm;
    }

    public void setOpenDttm(String openDttm) {
        this.openDttm = openDttm;
    }

    public String getCoptHodr() {
        return coptHodr;
    }

    public void setCoptHodr(String coptHodr) {
        this.coptHodr = coptHodr;
    }

    public String getMediOpenDate() {
        return mediOpenDate;
    }

    public void setMediOpenDate(String mediOpenDate) {
        this.mediOpenDate = mediOpenDate;
    }

    @Override
    public String toString() {
	return "MlStatWorksVO [worksId=" + worksId + ", genreCd=" + genreCd
		+ ", rgstDttm=" + rgstDttm + ", rgstIdnt=" + rgstIdnt
		+ ", modiDttm=" + modiDttm + ", modiIdnt=" + modiIdnt
		+ ", worksTitle=" + worksTitle + ", worksDivsCd=" + worksDivsCd
		+ ", systEffortStatCd=" + systEffortStatCd + ", statWorksYn="
		+ statWorksYn + ", systEffortStatDttm=" + systEffortStatDttm
		+ ", objcYn=" + objcYn + ", tDivs=" + tDivs + ", openDttm="
		+ openDttm + ", coptHodr=" + coptHodr + ", mediOpenDate="
		+ mediOpenDate + "]";
    }

    public String gettDivs() {
        return tDivs;
    }

    public void settDivs(String tDivs) {
        this.tDivs = tDivs;
    }

    public String getWorksId() {
        return this.worksId;
    }
    
    public void setWorksId(String worksId) {
        this.worksId = worksId;
    }
    
    public String getGenreCd() {
        return this.genreCd;
    }
    
    public void setGenreCd(String genreCd) {
        this.genreCd = genreCd;
    }
    
    public String getRgstDttm() {
        return this.rgstDttm;
    }
    
    public void setRgstDttm(String rgstDttm) {
        this.rgstDttm = rgstDttm;
    }
    
    public String getRgstIdnt() {
        return this.rgstIdnt;
    }
    
    public void setRgstIdnt(String rgstIdnt) {
        this.rgstIdnt = rgstIdnt;
    }
    
    public String getModiDttm() {
        return this.modiDttm;
    }
    
    public void setModiDttm(String modiDttm) {
        this.modiDttm = modiDttm;
    }
    
    public String getModiIdnt() {
        return this.modiIdnt;
    }
    
    public void setModiIdnt(String modiIdnt) {
        this.modiIdnt = modiIdnt;
    }
    
    public String getWorksTitle() {
        return this.worksTitle;
    }
    
    public void setWorksTitle(String worksTitle) {
        this.worksTitle = worksTitle;
    }
    
    public String getWorksDivsCd() {
        return this.worksDivsCd;
    }
    
    public void setWorksDivsCd(String worksDivsCd) {
        this.worksDivsCd = worksDivsCd;
    }
    
    public String getSystEffortStatCd() {
        return this.systEffortStatCd;
    }
    
    public void setSystEffortStatCd(String systEffortStatCd) {
        this.systEffortStatCd = systEffortStatCd;
    }
    
    public String getStatWorksYn() {
        return this.statWorksYn;
    }
    
    public void setStatWorksYn(String statWorksYn) {
        this.statWorksYn = statWorksYn;
    }
    
    public String getSystEffortStatDttm() {
        return this.systEffortStatDttm;
    }
    
    public void setSystEffortStatDttm(String systEffortStatDttm) {
        this.systEffortStatDttm = systEffortStatDttm;
    }
    
    public String getObjcYn() {
        return this.objcYn;
    }
    

    public void setObjcYn(String objcYn) {
        this.objcYn = objcYn;
    }
}
