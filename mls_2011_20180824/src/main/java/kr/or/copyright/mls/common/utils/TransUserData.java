package kr.or.copyright.mls.common.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.util.RedirectUtils;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.tagfree.util.URLEncoder;
import kr.or.copyright.common.userLogin.PasswordMismatchException;
import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.dao.UserLoginDao;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.common.userLogin.service.UserLoginService;
import kr.or.copyright.common.userLogin.controller.TestSSOLogin;
import kr.or.copyright.common.userLogin.controller.TestSSOValid;
import kr.or.copyright.mls.common.model.TransUserDTO;
import kr.or.copyright.mls.support.util.SessionUtil;
import kr.or.copyright.mls.user.service.UserService;
import kr.or.copyright.mls.user.service.UserServiceImpl;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@Controller
public class TransUserData {

     @Autowired
     private UserLoginService userLoginService;

     private static String clientId = "fa48f99639b2418c956c1642d8e8e02e";

     private static String testRecUrl = "http://www.findcopyright.or.kr:8080/recTransUser.do";
     private static String testFailUrl = "http://www.findcopyright.or.kr:8080/transUser.do";
     private static String testSecUrl = "http://www.findcopyright.or.kr:8080/sendTransUser.do?userIdnt=";
     private static String testSecCorUrl = "http://www.findcopyright.or.kr:8080/sendTransCorpUser.do?userIdnt=";
     private static String testResultUrl = "http://www.findcopyright.or.kr:8080/recTransUser.do";
     private static String testHome = "http://www.findcopyright.or.kr:8080";

     private static String recUrl = "https://www.findcopyright.or.kr/recTransUser.do";
     private static String failUrl = "https://www.findcopyright.or.kr/transUser.do";
     private static String SecUrl = "https://www.findcopyright.or.kr/sendTransUser.do?userIdnt=";
     private static String SecCorUrl = "https://www.findcopyright.or.kr/sendTransCorpUser.do?userIdnt=";
     private static String HomeUrl = "https://www.findcopyright.or.kr/";

     @RequestMapping(value = "/transUser.do", method = RequestMethod.POST)
     @ResponseBody
     public JSONObject TransUser(@RequestBody TransUserDTO trans) throws Exception {

          SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");

          String format_time1 = format1.format(System.currentTimeMillis());

          String id = trans.getUserId();
          String pwd = trans.getUserPwd();

          JSONObject data = new JSONObject();
          User user = new User();
          user.setUserIdnt(id);
          user.setPswd(pwd);

          user = userLoginService.checkUser(user);
          
          if (user.getUserDivs().equals("01") || user.getUserDivs() == "01") {
               try {

                    String rec = URLEncoder.encode(recUrl, "UTF-8");
                    String failreturn = URLEncoder.encode(failUrl, "UTF-8");

                    if (user.getUserIdnt().equals("FAIL")) {

                         data.put("resultCode", "FAIL");
                         data.put("returnSiteCode", "SITE006");
                         data.put("returnResultUrl", null);
                         data.put("transDataUrl", null);

                         return data;

                    } else {

                         String check = user.getUserIdnt();
                         String transcheck = user.getSso_loginyn();
                         String userDivs = user.getUserDivs();

                         if (check != null && (transcheck != null && transcheck.equals("Y"))) {

                              data.put("resultCode", "Already");
                              data.put("returnSiteCode", "SITE006");
                              data.put("returnResultUrl", null);
                              data.put("transDataUrl", null);
                              return data;
                         } else {

                              String sucreturn = URLEncoder.encode(SecUrl + user.getUserIdnt() + "&date=" + format_time1 + "2", "UTF-8");
                              System.out.println("sucreturn:: " + sucreturn);
                              data.put("resultCode", "SUCCESS");
                              data.put("returnSiteCode", "SITE006");
                              data.put("returnResultUrl", recUrl);
                              data.put("transDataUrl", sucreturn);
                              return data;
                         }
                    }

               } catch (Exception e) {
                    data.put("resultCode", "ERROR");
                    data.put("returnSiteCode", "SITE006");
                    data.put("returnResultUrl", null);
                    data.put("transDataUrl", null);
                    return data;
               }
          } else {
               try {

                    String rec = URLEncoder.encode(recUrl, "UTF-8");
                    String failreturn = URLEncoder.encode(failUrl, "UTF-8");

                    if (user.getUserIdnt().equals("FAIL")) {

                         data.put("resultCode", "FAIL");
                         data.put("returnSiteCode", "SITE006");
                         data.put("returnResultUrl", null);
                         data.put("transDataUrl", null);

                         return data;

                    } else {

                         String check = user.getUserIdnt();
                         String transcheck = user.getSso_loginyn();
                         String userDivs = user.getUserDivs();

                         if (check != null && (transcheck != null && transcheck.equals("Y"))) {

                              data.put("resultCode", "Already");
                              data.put("returnSiteCode", "SITE006");
                              data.put("returnResultUrl", null);
                              data.put("transDataUrl", null);
                              return data;
                         } else {

                              String sucreturn = URLEncoder.encode(SecCorUrl + user.getUserIdnt() + "&date=" + format_time1 + "2", "UTF-8");
                              System.out.println("sucreturn:: " + sucreturn);
                              data.put("resultCode", "SUCCESS");
                              data.put("returnSiteCode", "SITE006");
                              data.put("returnResultUrl", recUrl);
                              data.put("transDataUrl", sucreturn);
                              return data;
                         }
                    }

               } catch (Exception e) {
                    data.put("resultCode", "ERROR");
                    data.put("returnSiteCode", "SITE006");
                    data.put("returnResultUrl", null);
                    data.put("transDataUrl", null);
                    return data;
               }
          }

          /*
           * JSONObject response = new JSONObject(); response.put("response", data);
           */

     }


     // 연동되어있는 로그인
     @RequestMapping(value = "/ssoLogin.do", method = RequestMethod.POST)
     public ModelAndView TestLog(HttpServletRequest request, User user) throws Exception {

          try {

               user.setUserIdnt(request.getParameter("userIdnt"));
               user.setPswd(request.getParameter("pswd"));

               TestSSOLogin.testLog(request, user);

               user = SessionUtil.getSSO2(request);

               String membId = user.getUserIdnt();
               String membName = user.getUserName();
               String membEmail = user.getMail();


               // System.out.println(" membId::"+ user.getUserIdnt() );


               user = userLoginService.userlogin2(user);

               if (user == null) {

                    User user2 = new User();
                    user2.setUserIdnt(membId);
                    user2.setUserName(membName);
                    user2.setMail(membEmail);
                    user2.setSso_loginyn("Y");
                    SessionUtil.setUserSession(request, user2);

               } else {

                    SessionUtil.setUserSession(request, user);
               }

          } catch (Exception e) {
               e.printStackTrace();
               request.setAttribute("errorMessage", "사용자 정보가 올바르지 않습니다.");
               return new ModelAndView("/userLogin/userLogn");
          }

          return new ModelAndView("redirect:/main/main.do");
     }

     @RequestMapping(value = "/ssoMain.do", method = RequestMethod.POST)
     public ModelAndView SsoMain(HttpServletRequest request) throws Exception {

          try {

               String access_Token = request.getParameter("accessToken");
               String refresh_Token = request.getParameter("refreshToken");


               // System.out.println( "access_Token "+access_Token );
               // System.out.println( "refresh_token "+refresh_Token );

               TestSSOValid.main(request);
               HttpSession session = request.getSession();

               session.setAttribute("access_token", access_Token);
               session.setAttribute("refresh_token", refresh_Token);

               User user = new User();

               user = SessionUtil.getSSO2(request);

               String membId = user.getUserIdnt();
               String membName = user.getUserName();
               String membEmail = user.getMail();

               user = userLoginService.userlogin2(user);

               //
               if (user == null) {

                    User user2 = new User();
                    user2.setUserIdnt(membId);
                    user2.setUserName(membName);
                    user2.setMail(membEmail);
                    user2.setSso_loginyn("Y");
                    SessionUtil.setUserSession(request, user2);

               } else {

                    SessionUtil.setUserSession(request, user);
               }

          } catch (Exception e) {

               request.setAttribute("errorMessage", "다시 로그인해 주십시오.");
               return new ModelAndView("/userLogin/userLogn");
          }

          return new ModelAndView("redirect:/main/main.do");
     }



     // 실패시 에러 반납해야할거같은데..
     @RequestMapping(value = "/recTransUser.do", method = RequestMethod.POST)
     @ResponseBody
     public void RecTransUser(@RequestBody TransUserDTO trans, HttpServletRequest request) throws Exception {

          System.out.println(" ::::::: 결과처리 ::::::: ");

          System.out.println("siteMembId  :" + trans.getSiteMembId());
          System.out.println("membSeq    : " + trans.getMembSeq());
          System.out.println("clientId  :  " + trans.getSsoClientId());
          System.out.println("membId  :    " + trans.getMembId());
          String checkClienId = trans.getSsoClientId();

          if (!checkClienId.equals(clientId)) {

          } else {


               HttpSession session = request.getSession();

               System.out.println("SessionUtil.getSession( request ) : " + SessionUtil.getSession(request));

               session.removeAttribute("sessUserIdnt");
               session.removeAttribute("sessUserName");
               session.removeAttribute("sessSsoYn");
               session.removeAttribute("sessMail");
               session.removeAttribute("sessMoblPhon");
               session.removeAttribute("sessUserDivs");

               session.removeAttribute("sessClmsAgrYn");
               session.removeAttribute("sessClmsUserIdnt");
               session.removeAttribute("sessClmsPswd");
               session.removeAttribute("access_token");
               session.removeAttribute("refresh_token");
               User user = new User();
               user.setUserIdnt(trans.getSiteMembId());
               user.setSso_login_id(trans.getMembId());
               user.setSso_login_seq(trans.getMembSeq());

               // UPDATE

               userLoginService.updateSso(user);

               userLoginService.insertSsoHistory(user);



          }

     }

     /*
      * check = 0 ( ID or PWD ERROR ) = 1 ( Already Trans ) = 2 ( Success ) = 3 ( ERRPR ) transDataUrl -
      * 개인
      */
     @RequestMapping(value = "/sendTransUser.do", method = RequestMethod.POST)
     @ResponseBody
     public JSONObject SendTransUser(HttpServletRequest request) throws Exception {

          SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");

          String format_time1 = format1.format(System.currentTimeMillis());

          User user = new User();
          JSONObject data = new JSONObject();

          String userIdnt = request.getParameter("userIdnt");
          String date = (request.getParameter("date").substring(0, 8));
          String check = request.getParameter("date").substring(8);
          // System.out.println(userIdnt);
          // System.out.println( "check : " + check );

          if (!format_time1.equals(date) || !check.equals("2")) {

               // System.out.println( format_time1 );
               // System.out.println( date );

               data.put("membName", null);
               data.put("membCelnum", null);
               data.put("membEmail", null);
               data.put("membCelnum", null);
               data.put("ssoClientId", clientId);
               data.put("siteMembId", null);

               return data;
          } else {

               user.setUserIdnt(userIdnt);
               user = userLoginService.transData(user);
               // System.out.println("user");
               // System.out.println(user.toString());
               String regEx = "(\\d{3})(\\d{3,4})(\\d{4})";
               String mobile = "";

               if (user.getMoblPhon() != null) {
                    mobile = user.getMoblPhon().replaceAll(regEx, "$1-$2-$3");
               }

               data.put("membName", user.getUserName());
               data.put("membCelnum", mobile);
               data.put("membEmail", user.getMail());
               data.put("ssoClientId", clientId);
               data.put("siteMembId", userIdnt);

               return data;
          }

     }


     /*
      * check = 0 ( ID or PWD ERROR ) = 1 ( Already Trans ) = 2 ( Success ) = 3 ( ERRPR ) transDataUrl -
      * 개인
      */
     @RequestMapping(value = "/sendTransCorpUser.do", method = RequestMethod.POST)
     @ResponseBody
     public JSONObject sendTransCorpUser(HttpServletRequest request) throws Exception {

          SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");

          String format_time1 = format1.format(System.currentTimeMillis());

          User user = new User();
          JSONObject data = new JSONObject();

          String userIdnt = request.getParameter("userIdnt");
          String date = (request.getParameter("date").substring(0, 8));
          String check = request.getParameter("date").substring(8);
          System.out.println(userIdnt);
          System.out.println("check : " + check);

          if (!format_time1.equals(date) || !check.equals("2")) {

               // System.out.println( format_time1 );
               // System.out.println( date );

               data.put("membName", null);
               data.put("membBuisnRegnum", null);
               data.put("ssoClientId", clientId);
               data.put("siteMembId", null);

               return data;
          } else {

               user.setUserIdnt(userIdnt);
               user = userLoginService.transData(user);
               // System.out.println("user");
               // System.out.println(user.toString());
               // String regEx = "(\\d{3})(\\d{3,4})(\\d{4})";
               String regEx2 = "(\\d{3})(\\d{2})(\\d{5})";
               String getCrnNo = "";
               System.out.println("getCrnNo :: " + user.getCrnNo().replaceAll(regEx2, "$1-$2-$3"));
               if (user.getCrnNo() != null) {

                    getCrnNo = user.getCrnNo().replaceAll(regEx2, "$1-$2-$3");
                    // System.out.println("getCrnNo::"+getCrnNo);
               }

               data.put("membName", user.getUserName());
               data.put("membBuisnRegnum", getCrnNo);
               data.put("ssoClientId", clientId);
               data.put("siteMembId", userIdnt);


               return data;
          }

     }

     // 통합회원로그인으로 보내줌 연동을위해
     @RequestMapping(value = "/ssoLogin2.do", method = RequestMethod.POST)
     public ModelAndView ssoLogin2(HttpServletRequest request) throws Exception {

          SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");

          String format_time1 = format1.format(System.currentTimeMillis());

          String id = request.getParameter("userIdnt");
          String pwd = request.getParameter("pswd");
          User user = new User();
          JSONObject data = new JSONObject();
          ModelAndView mav = new ModelAndView();
          user.setUserIdnt(id);
          user.setPswd(pwd);

          user = userLoginService.checkUser(user);
          String rec = URLEncoder.encode(testRecUrl, "UTF-8");
          String failreturn = URLEncoder.encode(testFailUrl, "UTF-8");
          String home = URLEncoder.encode(testHome, "UTF-8");


          try {

               if (user.getUserIdnt().equals("FAIL")) {

                    String fail = URLEncoder.encode(testSecUrl + user.getUserIdnt() + "&date=" + format_time1 + "0", "UTF-8");

                    request.setAttribute("returnSiteCode", "SITE006");
                    request.setAttribute("returnSiteUrl", home);
                    request.setAttribute("returnResultUrl", rec);
                    request.setAttribute("transDataUrl", fail);

                    return new ModelAndView("/userLogin/transData");
               } else {
                    /*
                     * System.out.println( "check " + user.getUserIdnt() ); System.out.println( "transcheck " +
                     * user.getSso_loginyn());
                     */
                    String fail2 = URLEncoder.encode(testSecUrl + user.getUserIdnt() + "&date=" + format_time1 + "1", "UTF-8");
                    String check = user.getUserIdnt();
                    String transcheck = user.getSso_loginyn();

                    if (check != null && (transcheck != null && transcheck.equals("Y"))) {

                         request.setAttribute("returnSiteCode", "SITE006");
                         request.setAttribute("returnSiteUrl", home);
                         request.setAttribute("returnResultUrl", rec);
                         request.setAttribute("transDataUrl", fail2);
                         return new ModelAndView("/userLogin/transData");
                    } else {
                         String sucreturn = URLEncoder.encode(SecUrl + user.getUserIdnt() + "&date=" + format_time1 + "2", "UTF-8");

                         request.setAttribute("returnSiteCode", "SITE006");
                         request.setAttribute("returnSiteUrl", home);
                         request.setAttribute("returnResultUrl", rec);
                         request.setAttribute("transDataUrl", sucreturn);

                         return new ModelAndView("/userLogin/transData");

                    }
               }

          } catch (Exception e) {
               String fail3 = URLEncoder.encode(testSecUrl + user.getUserIdnt() + "&date=" + format_time1 + "3", "UTF-8");
               request.setAttribute("returnSiteCode", "SITE006");
               request.setAttribute("returnSiteUrl", home);
               request.setAttribute("returnResultUrl", rec);
               request.setAttribute("transDataUrl", fail3);

               return new ModelAndView("/userLogin/transData");
          }

     }
}
