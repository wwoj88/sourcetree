package kr.or.copyright.mls.common.model;

import java.util.ArrayList;

public class CodeList {

	private String code;
	private String codeName;
	private String bigCode;
	private String genre;
	private String midCode;
	private String caId;
	private String memberId;
	private String caCd;
	private String associationType;
	private ArrayList<String>  arrBigCode = new ArrayList<String>();
	private ArrayList<String>  arrMidCode = new ArrayList<String>();
	private String codeDesc;
	
	public ArrayList<String> getArrBigCode() {
		return arrBigCode;
	}
	public void setArrBigCode(ArrayList<String> arrBigCode) {
		this.arrBigCode = arrBigCode;
	}
	public ArrayList<String> getArrMidCode() {
		return arrMidCode;
	}
	public void setArrMidCode(ArrayList<String> arrMidCode) {
		this.arrMidCode = arrMidCode;
	}
	public String getAssociationType() {
		return associationType;
	}
	public void setAssociationType(String associationType) {
		this.associationType = associationType;
	}
	public String getBigCode() {
		return bigCode;
	}
	public void setBigCode(String bigCode) {
		this.bigCode = bigCode;
	}
	public String getCaCd() {
		return caCd;
	}
	public void setCaCd(String caCd) {
		this.caCd = caCd;
	}
	public String getCaId() {
		return caId;
	}
	public void setCaId(String caId) {
		this.caId = caId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCodeDesc() {
		return codeDesc;
	}
	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getMidCode() {
		return midCode;
	}
	public void setMidCode(String midCode) {
		this.midCode = midCode;
	}
	
}
