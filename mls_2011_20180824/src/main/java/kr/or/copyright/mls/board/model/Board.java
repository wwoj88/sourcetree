package kr.or.copyright.mls.board.model;

import java.util.List;

import kr.or.copyright.mls.common.BaseObject;

public class Board extends BaseObject {

	private String srchDivs;
	private String srchText;
	private String page_no;
	private long bordSeqn;
	private long menuSeqn;
	private long parentThreaded;
	private long prevThreaded;
	private long threaded;
	private long attcSeqn;
	private String tite;
	private String pswd;
	private int inqrCont;
	private String mail;
	private String url;
	private int deth;
	private int noidx;
	private String deltYsno;
	private String htmlYsno;
	private String rgstIdnt;
	private String rgstDate;
	private String modiIdnt;
	private String modiDate;
	private String bordDesc;
	private String bordDescTag;
	private String fileName;
	private String filePath;
	private String fileSize;
	private String realFileName;
	private int fileCont;
	private List fileList;
	private int from;
	private int to;
	private String fullFile;
	private String rgstKindCode;
	private String gubun;
	private String apprDttm;
	private String clmsTite;
	private String icn;
	private String genre;
	private String crhIdOfCa;
	private String kappLicensorId;
	private String krtraLicensorId;
	private String updateFg;
	private String gubunName;
	private String crId;
	private String nrId;
	private String albumId;
	private String bookNrId;
	private String genreName;
	private String workFileName;
	private int fild_1;
	private int fild_2;
	private int fild_3;
	private int fild_4;
	private int fild_5;
	private int fild_6;
	private int fild_7;
	private int fild_8;
	private int fild_9;
	private int fild_10;
	private int fild_11;
	private int fild_99;
	private String licensorName;
	
	/* 마이페이지 묻고답하기  정병호 추가 start */
	private String rgstName;
	private String replRgstName;
	private int attcCont;
	private int replYn;
	private String replBordSeqn;
	private String replMenuSeqn;
	private String replThreaded;
	private String replBordDesc;
	private String replBordDescTag;
	private String replRgstIdnt;
	private String replRgstDate;
	private String srchStartDate;
	private String srchEndDate;

	
	public String getSrchEndDate() {
		return srchEndDate;
	}
	public void setSrchEndDate(String srchEndDate) {
		this.srchEndDate = srchEndDate;
	}
	public String getSrchStartDate() {
		return srchStartDate;
	}
	public void setSrchStartDate(String srchStartDate) {
		this.srchStartDate = srchStartDate;
	}
	public int getAttcCont() {
		return attcCont;
	}
	public void setAttcCont(int attcCont) {
		this.attcCont = attcCont;
	}
	public String getReplBordDesc() {
		return replBordDesc;
	}
	public void setReplBordDesc(String replBordDesc) {
		this.replBordDesc = replBordDesc;
	}
	
	public String getReplRgstDate() {
		return replRgstDate;
	}
	public void setReplRgstDate(String replRgstDate) {
		this.replRgstDate = replRgstDate;
	}
	public String getReplRgstIdnt() {
		return replRgstIdnt;
	}
	public void setReplRgstIdnt(String replRgstIdnt) {
		this.replRgstIdnt = replRgstIdnt;
	}
	public String getReplBordSeqn() {
		return replBordSeqn;
	}
	public void setReplBordSeqn(String replBordSeqn) {
		this.replBordSeqn = replBordSeqn;
	}
	public String getReplMenuSeqn() {
		return replMenuSeqn;
	}
	public void setReplMenuSeqn(String replMenuSeqn) {
		this.replMenuSeqn = replMenuSeqn;
	}
	public String getReplThreaded() {
		return replThreaded;
	}
	public void setReplThreaded(String replThreaded) {
		this.replThreaded = replThreaded;
	}
	public int getReplYn() {
		return replYn;
	}
	public void setReplYn(int replYn) {
		this.replYn = replYn;
	}
	/* 정병호 추가 end */
	
	
	public long getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(long attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public String getBordDesc() {
		return bordDesc;
	}
	public void setBordDesc(String bordDesc) {
		this.bordDesc = bordDesc;
	}
	public long getBordSeqn() {
		return bordSeqn;
	}
	public void setBordSeqn(long bordSeqn) {
		this.bordSeqn = bordSeqn;
	}
	public String getDeltYsno() {
		return deltYsno;
	}
	public void setDeltYsno(String deltYsno) {
		this.deltYsno = deltYsno;
	}
	public int getDeth() {
		return deth;
	}
	public void setDeth(int deth) {
		this.deth = deth;
	}
	public int getFileCont() {
		return fileCont;
	}
	public void setFileCont(int fileCont) {
		this.fileCont = fileCont;
	}
	public List getFileList() {
		return fileList;
	}
	public void setFileList(List fileList) {
		this.fileList = fileList;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
	public String getHtmlYsno() {
		return htmlYsno;
	}
	public void setHtmlYsno(String htmlYsno) {
		this.htmlYsno = htmlYsno;
	}
	public int getInqrCont() {
		return inqrCont;
	}
	public void setInqrCont(int inqrCont) {
		this.inqrCont = inqrCont;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public long getMenuSeqn() {
		return menuSeqn;
	}
	public void setMenuSeqn(long menuSeqn) {
		this.menuSeqn = menuSeqn;
	}
	public String getModiDate() {
		return modiDate;
	}
	public void setModiDate(String modiDate) {
		this.modiDate = modiDate;
	}
	public String getModiIdnt() {
		return modiIdnt;
	}
	public void setModiIdnt(String modiIdnt) {
		this.modiIdnt = modiIdnt;
	}
	public int getNoidx() {
		return noidx;
	}
	public void setNoidx(int noidx) {
		this.noidx = noidx;
	}
	public String getPage_no() {
		return page_no;
	}
	public void setPage_no(String page_no) {
		this.page_no = page_no;
	}
	public long getParentThreaded() {
		return parentThreaded;
	}
	public void setParentThreaded(long parentThreaded) {
		this.parentThreaded = parentThreaded;
	}
	public long getPrevThreaded() {
		return prevThreaded;
	}
	public void setPrevThreaded(long prevThreaded) {
		this.prevThreaded = prevThreaded;
	}
	public String getPswd() {
		return pswd;
	}
	public void setPswd(String pswd) {
		this.pswd = pswd;
	}
	public String getRealFileName() {
		return realFileName;
	}
	public void setRealFileName(String realFileName) {
		this.realFileName = realFileName;
	}
	public String getRgstDate() {
		return rgstDate;
	}
	public void setRgstDate(String rgstDate) {
		this.rgstDate = rgstDate;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getSrchDivs() {
		return srchDivs;
	}
	public void setSrchDivs(String srchDivs) {
		this.srchDivs = srchDivs;
	}
	public String getSrchText() {
		return srchText;
	}
	public void setSrchText(String srchText) {
		this.srchText = srchText;
	}
	public long getThreaded() {
		return threaded;
	}
	public void setThreaded(long threaded) {
		this.threaded = threaded;
	}
	public String getTite() {
		return tite;
	}
	public void setTite(String tite) {
		this.tite = tite;
	}
	public int getTo() {
		return to;
	}
	public void setTo(int to) {
		this.to = to;
	}
	public String getFullFile() {
		return fullFile;
	}
	public void setFullFile(String fullFile) {
		this.fullFile = fullFile;
	}
	public String getRgstKindCode() {
		return rgstKindCode;
	}
	public void setRgstKindCode(String rgstKindCode) {
		this.rgstKindCode = rgstKindCode;
	}
	public String getGubun() {
		return gubun;
	}
	public void setGubun(String gubun) {
		this.gubun = gubun;
	}
	public String getApprDttm() {
		return apprDttm;
	}
	public void setApprDttm(String apprDttm) {
		this.apprDttm = apprDttm;
	}
	public String getClmsTite() {
		return clmsTite;
	}
	public void setClmsTite(String clmsTite) {
		this.clmsTite = clmsTite;
	}
	public String getIcn() {
		return icn;
	}
	public void setIcn(String icn) {
		this.icn = icn;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getCrhIdOfCa() {
		return crhIdOfCa;
	}
	public void setCrhIdOfCa(String crhIdOfCa) {
		this.crhIdOfCa = crhIdOfCa;
	}
	public String getKappLicensorId() {
		return kappLicensorId;
	}
	public void setKappLicensorId(String kappLicensorId) {
		this.kappLicensorId = kappLicensorId;
	}
	public String getKrtraLicensorId() {
		return krtraLicensorId;
	}
	public void setKrtraLicensorId(String krtraLicensorId) {
		this.krtraLicensorId = krtraLicensorId;
	}
	public String getUpdateFg() {
		return updateFg;
	}
	public void setUpdateFg(String updateFg) {
		this.updateFg = updateFg;
	}
	public String getGubunName() {
		return gubunName;
	}
	public void setGubunName(String gubunName) {
		this.gubunName = gubunName;
	}
	public String getCrId() {
		return crId;
	}
	public void setCrId(String crId) {
		this.crId = crId;
	}
	public String getNrId() {
		return nrId;
	}
	public void setNrId(String nrId) {
		this.nrId = nrId;
	}
	public String getAlbumId() {
		return albumId;
	}
	public void setAlbumId(String albumId) {
		this.albumId = albumId;
	}
	public String getBookNrId() {
		return bookNrId;
	}
	public void setBookNrId(String bookNrId) {
		this.bookNrId = bookNrId;
	}
	public String getGenreName() {
		return genreName;
	}
	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}
	public String getWorkFileName() {
		return workFileName;
	}
	public void setWorkFileName(String workFileName) {
		this.workFileName = workFileName;
	}
	public int getFild_1() {
		return fild_1;
	}
	public void setFild_1(int fild_1) {
		this.fild_1 = fild_1;
	}
	public int getFild_2() {
		return fild_2;
	}
	public void setFild_2(int fild_2) {
		this.fild_2 = fild_2;
	}
	public int getFild_3() {
		return fild_3;
	}
	public void setFild_3(int fild_3) {
		this.fild_3 = fild_3;
	}
	public int getFild_4() {
		return fild_4;
	}
	public void setFild_4(int fild_4) {
		this.fild_4 = fild_4;
	}
	public int getFild_5() {
		return fild_5;
	}
	public void setFild_5(int fild_5) {
		this.fild_5 = fild_5;
	}
	public int getFild_6() {
		return fild_6;
	}
	public void setFild_6(int fild_6) {
		this.fild_6 = fild_6;
	}
	public int getFild_7() {
		return fild_7;
	}
	public void setFild_7(int fild_7) {
		this.fild_7 = fild_7;
	}
	public int getFild_8() {
		return fild_8;
	}
	public void setFild_8(int fild_8) {
		this.fild_8 = fild_8;
	}
	public int getFild_9() {
		return fild_9;
	}
	public void setFild_9(int fild_9) {
		this.fild_9 = fild_9;
	}
	public int getFild_10() {
		return fild_10;
	}
	public void setFild_10(int fild_10) {
		this.fild_10 = fild_10;
	}
	public int getFild_11() {
		return fild_11;
	}
	public void setFild_11(int fild_11) {
		this.fild_11 = fild_11;
	}
	public int getFild_99() {
		return fild_99;
	}
	public void setFild_99(int fild_99) {
		this.fild_99 = fild_99;
	}
	public String getLicensorName() {
		return licensorName;
	}
	public void setLicensorName(String licensorName) {
		this.licensorName = licensorName;
	}
	public String getReplRgstName() {
		return replRgstName;
	}
	public void setReplRgstName(String replRgstName) {
		this.replRgstName = replRgstName;
	}
	public String getRgstName() {
		return rgstName;
	}
	public void setRgstName(String rgstName) {
		this.rgstName = rgstName;
	}
	public String getBordDescTag() {
		return bordDescTag;
	}
	public void setBordDescTag(String bordDescTag) {
		this.bordDescTag = bordDescTag;
	}
	public String getReplBordDescTag() {
		return replBordDescTag;
	}
	public void setReplBordDescTag(String replBordDescTag) {
		this.replBordDescTag = replBordDescTag;
	}
}
