package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;


public interface FdcrAd96Service{
	/**
	 * 연도별 신청건수, 승인건수
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd96List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 연도별 저작물, 실연, 음반, 방송, 데이터베이스별 건수
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd96List2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 연도별 보상금액별
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd96List3( Map<String, Object> commandMap ) throws Exception;
}
