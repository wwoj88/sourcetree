package kr.or.copyright.mls.console.legal;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.legal.inter.FdcrAd12Service;
import kr.or.copyright.mls.myStat.service.MyStatService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 법정허락 > 법정허락 대상저작물 관리
 * 
 * @author ljh
 */
@Controller
public class FdcrAd12Controller extends DefaultController{

	@Resource( name = "fdcrAd12Service" )
	private FdcrAd12Service fdcrAd12Service;

	@Resource( name = "myStatService" )
	private MyStatService myStatService;

	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd12Controller.class );

	/**
	 * 법정허락 대상저작물 관리 목록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd12List1.page" )
	public String fdcrAd12List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		/*
		commandMap.put( "FROM_NO", "" );
		commandMap.put( "TO_NO", "" );
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "TAB_INDEX", "" );
		*/
		//commandMap.put( "SCH_WORKS_DIVS_CD", "" );
		//commandMap.put( "SCH_STAT_OBJC_CD", "" );
		//commandMap.put( "SCH_WORKS_TITLE", "" );
		//commandMap.put( "SCH_LYRI_WRTR", "" );

		fdcrAd12Service.fdcrAd12List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		//model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		//model.addAttribute( "ds_count", commandMap.get( "ds_count" ) );
		System.out.println( "법정허락 대상저작물 관리 목록" );
		return "legal/fdcrAd12List1.tiles";
	}

	/**
	 * 법정허락 대상저작물 선택 삭제
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd12Delete1.page" )
	public String fdcrAd12Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		//String[] WORKS_IDS = request.getParameterValues( "WORKS_ID" );
		//commandMap.put( "WORKS_ID", WORKS_IDS );
		//commandMap.put( "WORKS_ID", WORKS_IDS );
		//String WORKS_ID = EgovWebUtil.getString( commandMap, "WORKS_ID" );
		String WORKS_ID = EgovWebUtil.getString( commandMap, "WORKS_ID" );

		boolean isSuccess = fdcrAd12Service.fdcrAd12Delete1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "법정허락 대상저작물 선택 삭제" );
		
		if( isSuccess ){
			return returnUrl(
				model,
				"삭제했습니다.",
				"/console/legal/fdcrAd12List1.page");
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/legal/fdcrAd12List1.page");
		}
	}

}
