package kr.or.copyright.mls.console.stats;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.stats.inter.FdcrAd88Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 통계관리	 > 일별접속통계
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd88Controller extends DefaultController{

	@Resource( name = "fdcrAd88Service" )
	private FdcrAd88Service fdcrAd88Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd88Controller.class );

	/**
	 * 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd88List1.page" )
	public String fdcrAd88List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd88List1 Start" );
		// 파라미터 셋팅
		String DATE = EgovWebUtil.getString( commandMap, "DATE" );
		if(!"".equals(DATE)){
			fdcrAd88Service.fdcrAd88List1( commandMap );
		}
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "fdcrAd88List1 End" );
		return "/stats/fdcrAd88List1.tiles";
	}
}
