package kr.or.copyright.mls.console.event.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface FdcrAdA1Service{

	/**
	 * 이벤트 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA1List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 이벤트 상세 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA1View2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 이벤트 재등록
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA1Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 이벤트 수정
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA1Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 이벤트 선택삭제
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA1Delete1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 댓글형 이벤트 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA1View1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 댓글형 이벤트 댓글 등록/수정
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA1Insert2( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 참여 및 응답현황 목록 팝업 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA1Pop1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 참여 및 응답현황 목록 엑셀다운로드
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA1ExcelDown1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 참여 및 응답현황/당첨자 자동선정 당첨자 등록
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA1Insert3( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 참여 및 응답현황 당첨자 취소
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA1Insert4( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 당첨자 자동선정 목록 팝업 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA1Pop2( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 당첨자 자동선정 선정하기
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA1List2( Map<String, Object> commandMap ) throws Exception;

}
