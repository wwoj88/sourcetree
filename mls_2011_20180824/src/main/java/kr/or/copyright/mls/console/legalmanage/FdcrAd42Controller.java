package kr.or.copyright.mls.console.legalmanage;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 법정허락 관리 > 법정허락 저작물 목록
 * 
 * @author ljh
 */
@Controller
public class FdcrAd42Controller extends DefaultController{

	@Resource( name = "fdcrAd42Service" )
	private FdcrAd42ServiceImpl fdcrAd42Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd42Controller.class );

	/**
	 * 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42List1.page" )
	public String fdcrAd42List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		String GUBUN = EgovWebUtil.getString( commandMap, "GUBUN" );
		String TITLE = EgovWebUtil.getString( commandMap, "TITLE" );
		String CLMS_TITLE = EgovWebUtil.getString( commandMap, "CLMS_TITLE" );
		String FILD_1 = EgovWebUtil.getString( commandMap, "FILD_1" );
		String FILD_2 = EgovWebUtil.getString( commandMap, "FILD_2" );
		String FILD_3 = EgovWebUtil.getString( commandMap, "FILD_3" );
		String FILD_4 = EgovWebUtil.getString( commandMap, "FILD_4" );
		String FILD_5 = EgovWebUtil.getString( commandMap, "FILD_5" );
		String FILD_6 = EgovWebUtil.getString( commandMap, "FILD_6" );
		String FILD_7 = EgovWebUtil.getString( commandMap, "FILD_7" );
		String FILD_8 = EgovWebUtil.getString( commandMap, "FILD_8" );
		String FILD_9 = EgovWebUtil.getString( commandMap, "FILD_9" );
		String FILD_10 = EgovWebUtil.getString( commandMap, "FILD_10" );
		String FILD_11 = EgovWebUtil.getString( commandMap, "FILD_11" );
		String FILD_99 = EgovWebUtil.getString( commandMap, "FILD_99" );
		
		if( !"".equals( GUBUN ) ){
			GUBUN = URLDecoder.decode( GUBUN, "UTF-8" );
			commandMap.put( "GUBUN", GUBUN );
		}
		if( !"".equals( TITLE ) ){
			TITLE = URLDecoder.decode( TITLE, "UTF-8" );
			commandMap.put( "TITLE", TITLE );
		}
		if( !"".equals( CLMS_TITLE ) ){
			CLMS_TITLE = URLDecoder.decode( CLMS_TITLE, "UTF-8" );
			commandMap.put( "CLMS_TITLE", CLMS_TITLE );
		}

		fdcrAd42Service.fdcrAd42List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_List" ) );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "commandMap", commandMap );
		System.out.println( "목록 조회" );
		return "legalmanage/fdcrAd42List1.tiles";
	}

	/**
	 * 저작물 상세 및 수정 폼
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42UpdateForm1.page" )
	public String fdcrAd42UpdateForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		String BORD_SEQN = EgovWebUtil.getString( commandMap, "BORD_SEQN" );
		String MENU_SEQN = EgovWebUtil.getString( commandMap, "MENU_SEQN" );

		Map<String, Object> info = fdcrAd42Service.statDetailList( commandMap );
		model.addAttribute( "info", info );
		
		ArrayList<Map<String, Object>> ds_file = fdcrAd42Service.statFileList( commandMap );
		model.addAttribute( "ds_file", ds_file );
		
		//fdcrAd42Service.fdcrAd42UpdateForm1( commandMap );
		//model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		//model.addAttribute( "ds_file", commandMap.get( "ds_file" ) );
		System.out.println( "저작물 상세 및 수정 폼" );
		return "legalmanage/fdcrAd42UpdateForm1.tiles";
	}

	/**
	 * CLMS 저작물 목록 조회 팝업 폼
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42Popup1.page" )
	public String fdcrAd42Popup1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		//fdcrAd42Service.fdcrAd42Popup1( commandMap );
		//model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "CLMS 저작물 목록 조회 팝업 폼" );
		return "legalmanage/fdcrAd42Popup1.popup";
	}
	
	/**
	 * CLMS 저작물 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42List2.page" )
	public String fdcrAd42List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		String GUBUN = EgovWebUtil.getString( commandMap, "GUBUN" );
		String TITLE = EgovWebUtil.getString( commandMap, "TITLE" );

		fdcrAd42Service.fdcrAd42Popup1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "CLMS 저작물 목록 조회" );
		System.out.println( "GUBUN:::::::::::::::"+GUBUN );
		return "legalmanage/fdcrAd42Popup1.popup";
	}

	/**
	 * CLMS 저작물 상세 조회(음악,어문 제외)
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42View1.page" )
	public String fdcrAd42View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		String PRPS_IDNT = EgovWebUtil.getString( commandMap, "CR_ID" );
		String PRPS_DIVS = EgovWebUtil.getString( commandMap, "PRPS_DIVS" );
		commandMap.put( "PRPS_IDNT", PRPS_IDNT );
		
		fdcrAd42Service.fdcrAd42View1( commandMap );
		model.addAttribute( "ds_works_info", commandMap.get( "ds_works_info" ) );
		System.out.println( "CLMS 저작물 상세 조회" );
		System.out.println("PRPS_DIVS::::::"+PRPS_DIVS);
		if(PRPS_DIVS == "C" || PRPS_DIVS.equals( "C" )){
			return "legalmanage/fdcrAd42View1.popup";
		}else if(PRPS_DIVS == "V" || PRPS_DIVS.equals( "V" )){
			return "legalmanage/fdcrAd42View2.popup";
		}else if(PRPS_DIVS == "R" || PRPS_DIVS.equals( "R" )){
			//return "legalmanage/fdcrAd42View3.popup";
			//ICN번호 없어서 화면 없음
			return null;
		}else if(PRPS_DIVS == "N" || PRPS_DIVS.equals( "N" )){
			//return "legalmanage/fdcrAd42View4.popup";
			//ICN번호 없어서 화면 없음
			return null;
		}else{
			return null;
		}
	}

	/**
	 * CLMS 저작물 상세 조회(음악,어문)
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42View2.page" )
	public String fdcrAd42View2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		/*
		commandMap.put( "GENRE", "" );
		commandMap.put( "CR_ID", "" );
		commandMap.put( "ALBUM_ID", "" );
		commandMap.put( "NR_ID", "" );
		commandMap.put( "BOOK_NR_ID", "" );
		commandMap.put( "WORK_FILE_NAME", "" );
		commandMap.put( "PRPS_DIVS", "" );
		*/
		String PRPS_IDNT = EgovWebUtil.getString( commandMap, "CR_ID" );
		String PRPS_DIVS = EgovWebUtil.getString( commandMap, "PRPS_DIVS" );
		commandMap.put( "PRPS_IDNT", PRPS_IDNT );
		
		fdcrAd42Service.fdcrAd42View2( commandMap );
		model.addAttribute( "ds_works_info", commandMap.get( "ds_works_info" ) );
		System.out.println( "CLMS 저작물 상세 조회(음악,어문)" );
		if(PRPS_DIVS == "M" || PRPS_DIVS.equals( "M" )){
			return "legalmanage/fdcrAd42View5.popup";
		}else if(PRPS_DIVS == "O" || PRPS_DIVS.equals( "O" )){
			return "legalmanage/fdcrAd42View6.popup";
		}else{
			return null;
		}
	}

	/**
	 * 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42Download1.page" )
	public void fdcrAd42Download1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		String BORD_SEQN = EgovWebUtil.getString( commandMap, "BORD_SEQN" );
		String MENU_SEQN = EgovWebUtil.getString( commandMap, "MENU_SEQN" );

		ArrayList<Map<String, Object>> ds_file = fdcrAd42Service.statFileList( commandMap );
		//List list = (List) commandMap.get( "ds_detail" );
		for( int i = 0; i < ds_file.size(); i++ ){
			Map map = (Map) ds_file.get( i );
			String bordSeqn = (String) map.get( "BORD_SEQN" );
			String menuSeqn = (String) map.get( "MENU_SEQN" );
			if( bordSeqn.equals( commandMap.get( "BORD_SEQN" ) ) && menuSeqn.equals( commandMap.get( "MENU_SEQN" ) ) ){
				String filePath = (String) map.get( "FILE_PATH" );
				String fileName = (String) map.get( "FILE_NAME" );
				String realFileName = (String) map.get( "REAL_FILE_NAME" );
				download( request, response, filePath + realFileName, fileName );
			}
		}
	}

	/**
	 * 저작물 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42Update1.page" )
	public String fdcrAd42Update1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		String BORD_SEQN = EgovWebUtil.getString( commandMap, "BORD_SEQN" );
		String MENU_SEQN = EgovWebUtil.getString( commandMap, "MENU_SEQN" );
		
		String[] RGST_IDNTS = request.getParameterValues( "RGST_IDNT" );
		String[] MAILS = request.getParameterValues( "MAIL" );
		String[] TITES = request.getParameterValues( "TITE" );
		String[] BORD_DESCS = request.getParameterValues( "BORD_DESC" );
		String[] GUBUNS = request.getParameterValues( "GUBUN" );
		String[] APPR_DTTMS = request.getParameterValues( "APPR_DTTM" );
		String[] CLMS_TITES = request.getParameterValues( "CLMS_TITE" );
		String[] ICNS = request.getParameterValues( "ICN" );
		String[] GENRES = request.getParameterValues( "GENRE" );
		String[] CR_IDS = request.getParameterValues( "CR_ID" );
		String[] NR_IDS = request.getParameterValues( "NR_ID" );
		String[] ALBUM_IDS = request.getParameterValues( "ALBUM_ID" );
		String[] BOOK_NR_IDS = request.getParameterValues( "BOOK_NR_ID" );
		String[] WORK_FILE_NAMES = request.getParameterValues( "WORK_FILE_NAME" );
		String[] LICENSOR_NAMES = request.getParameterValues( "LICENSOR_NAME" );
		
		String[] FILE_NAMES = request.getParameterValues( "FILE_NAME" );
		String[] FILE_PATHS = request.getParameterValues( "FILE_PATH" );
		String[] FILE_SIZES = request.getParameterValues( "FILE_SIZE" );
		String[] REAL_FILE_NAMES = request.getParameterValues( "REAL_FILE_NAME" );
		String[] ATTC_SEQNS = request.getParameterValues( "ATTC_SEQN" );
		String[] THREADEDS = request.getParameterValues( "THREADED" );
		String[] CLIENT_FILES = request.getParameterValues( "CLIENT_FILE" );
		
		//필드 파라미터
		String FILD_1 = request.getParameter( "FILD_1" );
		String FILD_2 = request.getParameter( "FILD_2" );
		String FILD_3 = request.getParameter( "FILD_3" );
		String FILD_4 = request.getParameter( "FILD_4" );
		String FILD_5 = request.getParameter( "FILD_5" );
		String FILD_6 = request.getParameter( "FILD_6" );
		String FILD_7 = request.getParameter( "FILD_7" );
		String FILD_8 = request.getParameter( "FILD_8" );
		String FILD_9 = request.getParameter( "FILD_9" );
		String FILD_10 = request.getParameter( "FILD_10" );
		String FILD_11 = request.getParameter( "FILD_11" );
		String FILD_12 = request.getParameter( "FILD_12" );

		commandMap.put( "RGST_IDNT", RGST_IDNTS );
		commandMap.put( "MAIL", MAILS );
		commandMap.put( "TITE", TITES );
		commandMap.put( "BORD_DESC", BORD_DESCS );
		commandMap.put( "GUBUN", GUBUNS );
		commandMap.put( "APPR_DTTM", APPR_DTTMS );
		commandMap.put( "CLMS_TITE", CLMS_TITES );
		commandMap.put( "ICN", ICNS );
		commandMap.put( "GENRE", GENRES );
		commandMap.put( "CR_ID", CR_IDS );
		commandMap.put( "NR_ID", NR_IDS );
		commandMap.put( "ALBUM_ID", ALBUM_IDS );
		commandMap.put( "BOOK_NR_ID", BOOK_NR_IDS );
		commandMap.put( "WORK_FILE_NAME", WORK_FILE_NAMES );
		commandMap.put( "LICENSOR_NAME", LICENSOR_NAMES );
		
		commandMap.put( "FILE_NAME", FILE_NAMES );
		commandMap.put( "FILE_PATH", FILE_PATHS );
		commandMap.put( "FILE_SIZE", FILE_SIZES );
		commandMap.put( "REAL_FILE_NAME", REAL_FILE_NAMES );
		commandMap.put( "ATTC_SEQN", ATTC_SEQNS );
		commandMap.put( "THREADED", THREADEDS );
		commandMap.put( "CLIENT_FILE", CLIENT_FILES );

		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAd42Service.fdcrAd42Update1( commandMap, fileinfo );
		returnAjaxString( response, isSuccess );
		
		if( isSuccess ){
			return returnUrl( model, "수정했습니다.", "/console/legalmanage/fdcrAd42List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legalmanage/fdcrAd42List1.page" );
		}
	}
	
	/**
	 * 저작물 삭제
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42Delete1.page" )
	public String fdcrAd42Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String BORD_SEQN = EgovWebUtil.getString( commandMap, "BORD_SEQN" );
		String MENU_SEQN = EgovWebUtil.getString( commandMap, "MENU_SEQN" );
		
		String[] TITES = request.getParameterValues( "TITE" );
		String[] FILE_NAMES = request.getParameterValues( "FILE_NAME" );
		String[] REAL_FILE_NAMES = request.getParameterValues( "REAL_FILE_NAME" );
		String[] ATTC_SEQNS = request.getParameterValues( "ATTC_SEQN" );
		
		commandMap.put( "TITE", TITES );
		commandMap.put( "FILE_NAME", FILE_NAMES );
		commandMap.put( "REAL_FILE_NAME", REAL_FILE_NAMES );
		commandMap.put( "ATTC_SEQN", ATTC_SEQNS );
		
		System.out.println( "저작물 삭제" );
		boolean isSuccess = fdcrAd42Service.fdcrAd42Delete1( commandMap );
		returnAjaxString( response, isSuccess );
		
		if( isSuccess ){
			return returnUrl( model, "삭제했습니다.", "/console/legalmanage/fdcrAd42List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legalmanage/fdcrAd42List1.page" );
		}
	}
	
	/**
	 * 저작물 등록 폼 
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42WriteForm1.page" )
	public String fdcrAd42WriteForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		System.out.println( "저작물 등록 폼" );
		return "legalmanage/fdcrAd42WriteForm1.tiles";
	}
	
	/**
	 * 저작물 등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legalmanage/fdcrAd42Insert1.page" )
	public String fdcrAd42Insert1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String[] RGST_IDNTS = request.getParameterValues( "RGST_IDNT" );
		String[] MAILS = request.getParameterValues( "MAIL" );
		String[] TITES = request.getParameterValues( "TITE" );
		String[] BORD_DESCS = request.getParameterValues( "BORD_DESC" );
		String[] GUBUNS = request.getParameterValues( "GUBUN" );
		String[] APPR_DTTMS = request.getParameterValues( "APPR_DTTM" );
		String[] CLMS_TITES = request.getParameterValues( "CLMS_TITE" );
		String[] ICNS = request.getParameterValues( "ICN" );
		String[] GENRES = request.getParameterValues( "GENRE" );
		String[] CR_IDS = request.getParameterValues( "CR_ID" );
		String[] NR_IDS = request.getParameterValues( "NR_ID" );
		String[] ALBUM_IDS = request.getParameterValues( "ALBUM_ID" );
		String[] BOOK_NR_IDS = request.getParameterValues( "BOOK_NR_ID" );
		String[] WORK_FILE_NAMES = request.getParameterValues( "WORK_FILE_NAME" );
		String[] LICENSOR_NAMES = request.getParameterValues( "LICENSOR_NAME" );
		
		String[] FILE_NAMES = request.getParameterValues( "FILE_NAME" );
		String[] FILE_PATHS = request.getParameterValues( "FILE_PATH" );
		String[] FILE_SIZES = request.getParameterValues( "FILE_SIZE" );
		String[] REAL_FILE_NAMES = request.getParameterValues( "REAL_FILE_NAME" );
		String[] ATTC_SEQNS = request.getParameterValues( "ATTC_SEQN" );
		String[] THREADEDS = request.getParameterValues( "THREADED" );
		String[] CLIENT_FILES = request.getParameterValues( "CLIENT_FILE" );
		
		//필드 파라미터
		String FILD_1 = request.getParameter( "FILD_1" );
		String FILD_2 = request.getParameter( "FILD_2" );
		String FILD_3 = request.getParameter( "FILD_3" );
		String FILD_4 = request.getParameter( "FILD_4" );
		String FILD_5 = request.getParameter( "FILD_5" );
		String FILD_6 = request.getParameter( "FILD_6" );
		String FILD_7 = request.getParameter( "FILD_7" );
		String FILD_8 = request.getParameter( "FILD_8" );
		String FILD_9 = request.getParameter( "FILD_9" );
		String FILD_10 = request.getParameter( "FILD_10" );
		String FILD_11 = request.getParameter( "FILD_11" );
		String FILD_12 = request.getParameter( "FILD_12" );

		commandMap.put( "RGST_IDNT", RGST_IDNTS );
		commandMap.put( "MAIL", MAILS );
		commandMap.put( "TITE", TITES );
		commandMap.put( "BORD_DESC", BORD_DESCS );
		commandMap.put( "GUBUN", GUBUNS );
		commandMap.put( "APPR_DTTM", APPR_DTTMS );
		commandMap.put( "CLMS_TITE", CLMS_TITES );
		commandMap.put( "ICN", ICNS );
		commandMap.put( "GENRE", GENRES );
		commandMap.put( "CR_ID", CR_IDS );
		commandMap.put( "NR_ID", NR_IDS );
		commandMap.put( "ALBUM_ID", ALBUM_IDS );
		commandMap.put( "BOOK_NR_ID", BOOK_NR_IDS );
		commandMap.put( "WORK_FILE_NAME", WORK_FILE_NAMES );
		commandMap.put( "LICENSOR_NAME", LICENSOR_NAMES );
		
		commandMap.put( "FILE_NAME", FILE_NAMES );
		commandMap.put( "FILE_PATH", FILE_PATHS );
		commandMap.put( "FILE_SIZE", FILE_SIZES );
		commandMap.put( "REAL_FILE_NAME", REAL_FILE_NAMES );
		commandMap.put( "ATTC_SEQN", ATTC_SEQNS );
		commandMap.put( "THREADED", THREADEDS );
		commandMap.put( "CLIENT_FILE", CLIENT_FILES );
		
		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAd42Service.fdcrAd42Insert1( commandMap, fileinfo );
		returnAjaxString( response, isSuccess );
		
		if( isSuccess ){
			return returnUrl( model, "등록했습니다.", "/console/legalmanage/fdcrAd42List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legalmanage/fdcrAd42List1.page" );
		}
	}

}
