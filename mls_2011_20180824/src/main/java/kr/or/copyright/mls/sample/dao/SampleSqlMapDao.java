package kr.or.copyright.mls.sample.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class SampleSqlMapDao extends SqlMapClientDaoSupport implements SampleDao {

	/*
	public void addBoard(Board board) {
		getSqlMapClientTemplate().insert("Board.insert", board);
	}

	public int updateBoard(Board board) {
		return 0;
	}
*/
	public List sampleList(Map map) {
		return getSqlMapClientTemplate().queryForList("Sample.sampleList",map);
	}

	public void sampleInsert(Map map) {
		getSqlMapClientTemplate().insert("Sample.sampleInsert",map);
	}	
	
	public void sampleUpdate(Map map) {
		getSqlMapClientTemplate().insert("Sample.sampleUpdate",map);
	}	
	
	public void sampleDelete(Map map) {
		getSqlMapClientTemplate().insert("Sample.sampleDelete",map);
	}	
	
	
	
	public List musicList(Map map) {
		return getSqlMapClientTemplate().queryForList("Sample.musicList",map);
	}	
	
	public List musicListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Sample.musicListCount",map);
	}		
}
