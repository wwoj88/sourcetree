package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;


public interface FdcrAd87Service{

	/**
	 * 월별 접속 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd87List1( Map<String, Object> commandMap ) throws Exception;
}
