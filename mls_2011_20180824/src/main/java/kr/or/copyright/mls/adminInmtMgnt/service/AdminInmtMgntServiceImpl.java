package kr.or.copyright.mls.adminInmtMgnt.service;

//import java.util.Map;

import java.util.List;
import java.util.Map;

import com.tobesoft.platform.data.Dataset;

import kr.or.copyright.mls.adminInmtMgnt.dao.AdminInmtMgntDao;
import kr.or.copyright.mls.common.dao.CommonDao;
import kr.or.copyright.mls.common.service.BaseService;


public class AdminInmtMgntServiceImpl extends BaseService implements
		AdminInmtMgntService {
	
	private AdminInmtMgntDao adminInmtMgntDao;
	
	public void setAdminInmtMgntDao(AdminInmtMgntDao adminInmtMgntDao){
		this.adminInmtMgntDao = adminInmtMgntDao;
	}
	// 엑셀 업로드
	public void adminInmtUpload() throws Exception {
		
		Dataset ds_excel = getDataset("ds_excel");
		Dataset ds_genre = getDataset("ds_genre");
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_genre, 0);
		// 0번째 row는 컬럼정보이므로 제외
		//for( int i=1; i<ds_sheet.getRowCount(); i++) {
			
		//	Map map = getMap(ds_sheet, i);
		
		List returnList = null;
		
		if(map.get("GENRE").equals("M")){ // 방송음악
			returnList = adminInmtMgntDao.brctInmtInsert(ds_excel,ds_condition);
			// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 20151027
			adminInmtMgntDao.updateMuscStatYn (map);
		}else if(map.get("GENRE").equals("S")){ // 교과용
			returnList = adminInmtMgntDao.subjInmtInsert(ds_excel,ds_condition);
			// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 20151027
			adminInmtMgntDao.updateSubjStatYn (map);
		}else if(map.get("GENRE").equals("L")){ // 도서관
			returnList = adminInmtMgntDao.librInmtInsert(ds_excel,ds_condition);
			// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 20151027
			adminInmtMgntDao.updateLibrStatYn (map);
		}else if(map.get("GENRE").equals("I")){ // 이미지
			returnList = adminInmtMgntDao.imgeInsert(ds_excel);
		}else if(map.get("GENRE").equals("V")){ // 영화
			returnList = adminInmtMgntDao.movieInsert(ds_excel);
		}else if(map.get("GENRE").equals("A")){ // 수업목적
			returnList = adminInmtMgntDao.lssnInmtInsert(ds_excel,ds_condition);
			// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 20151027
			adminInmtMgntDao.updateLssnStatYn (map);
		}
			
		//}
		
		addList("ds_excel", returnList);
	}
	
	// 엑셀 업로드(수정)
	public void adminInmtModiUpload() throws Exception {
		
		Dataset ds_excel = getDataset("ds_excel");
		Dataset ds_genre = getDataset("ds_genre");
		
		Map map = getMap(ds_genre, 0);
		// 0번째 row는 컬럼정보이므로 제외
		//for( int i=1; i<ds_sheet.getRowCount(); i++) {
			
		//	Map map = getMap(ds_sheet, i);
		
		List returnList = null;
		
		if(map.get("GENRE").equals("S")){ // 교과용
			returnList = adminInmtMgntDao.subjInmtUpdate(ds_excel);
		}
		
		addList("ds_excel", returnList);
	}
	
	//엑셀 업로드(수정) //20120904 정병호
	public void adminInmtUpdateUpload() throws Exception{
	    
		Dataset ds_excel = getDataset("ds_excel");
	    Dataset ds_genre = getDataset("ds_genre");
	    Dataset ds_condition = getDataset("ds_condition");
	    
	    Map map = getMap(ds_genre, 0);
	    
	    List returnList = null;
	    
	    if(map.get("GENRE").equals("S")){//교과용
	    	returnList = adminInmtMgntDao.subjInmtUpdateUpload(ds_excel,ds_condition);
	    }else if(map.get("GENRE").equals("L")){ // 도서관
	    	returnList = adminInmtMgntDao.librInmtUpdateUpload(ds_excel,ds_condition);
	    }else if(map.get("GENRE").equals("M")){ // 방송음악
	    	returnList = adminInmtMgntDao.brctInmtUpdateUpload(ds_excel,ds_condition);
	    }else if(map.get("GENRE").equals("A")){ // 수업목적
	    	returnList = adminInmtMgntDao.lssnInmtUpdateUpload(ds_excel,ds_condition);
	    }
	    
	    addList("ds_excel", returnList);
	}
	
	// inmtMgntMuscList 조회 
	public void inmtMgntMuscList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminInmtMgntDao.inmtMgntMuscList(map); 
		List pageList = (List)adminInmtMgntDao.totalRowinmtMgntMuscList(map);
		// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 
		adminInmtMgntDao.updateMuscStatYn (map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);
		
	}
	
	// inmtMgntLibrList 조회 
	public void inmtMgntLibrList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminInmtMgntDao.inmtMgntLibrList(map); 
		List pageList = (List)adminInmtMgntDao.totalRowinmtMgntLibrList(map);
		// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 
		adminInmtMgntDao.updateLibrStatYn (map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);
		
	}
	
	// inmtMgntSubjList 조회 
	public void inmtMgntSubjList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminInmtMgntDao.inmtMgntSubjList(map); 
		List pageList = (List)adminInmtMgntDao.totalRowinmtMgntSubjList(map);
		// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 
		// 최초 수동업데이트(16.6.21)로 주석처리
		//adminInmtMgntDao.updateSubjStatYn (map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);
		
	}
	
	// inmtMgntMovieList 조회 
	public void inmtMgntMovieList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminInmtMgntDao.inmtMgntMovieList(map); 
		List pageList = (List)adminInmtMgntDao.totalRowinmtMgntMovieList(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);
		
	}
	
	// inmtMgntImageList 조회 
	public void inmtMgntImageList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminInmtMgntDao.inmtMgntImageList(map); 
		List pageList = (List)adminInmtMgntDao.totalRowinmtMgntImageList(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);
		
	}
	
	// inmtWorksReport 조회
	public void inmtMgntReptView() throws Exception{
	    Dataset ds_condition = getDataset("ds_condition");
	    
	    Map map = getMap(ds_condition, 0);
	    
	    //DAO 호출
	    List list = (List)adminInmtMgntDao.inmtMgntReptView(map);
	    
	    addList("ds_List",list);
	}
	
	// inmtMgntLssnList 조회 
		public void inmtMgntLssnList() throws Exception {

			Dataset ds_condition = getDataset("ds_condition");     
			
			// Dataset 확인_log성
			ds_condition.printDataset();
			
			Map map = getMap(ds_condition, 0);
			
			//DAO호출
			List list = (List) adminInmtMgntDao.inmtMgntLssnList(map); 
			List pageList = (List)adminInmtMgntDao.totalRowinmtMgntLssnList(map);
			// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트 
			adminInmtMgntDao.updateLssnStatYn (map);
			
			//hmap를 dataset로 변환한다.
			addList("ds_List", list);
			addList("ds_page", pageList);
			
		}
}