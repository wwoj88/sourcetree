package kr.or.copyright.mls.adminEventMgnt.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.adminEventMgnt.model.MlEventItem;
import kr.or.copyright.mls.adminEventMgnt.service.AdminEventMgntWebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminEventMgntController {
	
	@Autowired
	public AdminEventMgntWebService adminEventMgntWebService;
	
	/**
	 * 문항관리 뷰
	 * @param req
	 * @param res
	 * @param vo
	 * @return
	 */
	@RequestMapping("/admin/eventMgnt/multiQustMgnt")
	public ModelAndView multiQustMgntView(HttpServletRequest req, HttpServletResponse res, MlEventItem vo) {
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/admin/eventMgnt/multiQustMgnt");
		mv.addObject("list",adminEventMgntWebService.getEventItemList(vo));
		mv.addObject("eventTitle",adminEventMgntWebService.getEventTitle(vo));
		return mv;
	}

	/**
	 * 문항관리 액션
	 * @param req
	 * @param res
	 * @param vo
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/admin/eventMgnt/multiQustMgntAction")
	public ModelAndView multiQustMgntAction(HttpServletRequest req, HttpServletResponse res, MlEventItem vo) throws Exception {
		
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(req);
		
		String event_id = multipartRequest.getParameter("event_id");
		String user_id = multipartRequest.getParameter("user_id");
		String item_id_arr[] = multipartRequest.getParameterValues("item_id");
		String item_type_cd_arr[] = multipartRequest.getParameterValues("item_type_cd");
		
		//값셋팅
		vo.setEvent_id(event_id);
		vo.setUser_id(user_id);
		vo.setItem_id_arr(item_id_arr);
		vo.setItem_type_cd_arr(item_type_cd_arr);
		
		ArrayList<HashMap<String, String>> action1 = new ArrayList<HashMap<String,String>>();//텍스트계열용
		ArrayList<HashMap> action2 = new ArrayList<HashMap>();//항목계열용
		for (int i = 0; i < item_id_arr.length; i++) {
			String itemId = item_id_arr[i];
			switch (Integer.parseInt(item_type_cd_arr[i])) {
				case 1:
					if (multipartRequest.getParameter("obj_"+itemId) != null && !"".equals(multipartRequest.getParameter("obj_"+itemId))) {
						HashMap mm = new HashMap();
						mm.put("EVENT_ID", event_id);
						mm.put("ITEM_ID", itemId);
						mm.put("RSLT_DESC", multipartRequest.getParameter("obj_"+itemId));
						mm.put("RGST_IDNT", user_id);
						action1.add(mm);
					}
				break;
				case 2:
					if (multipartRequest.getParameter("obj_"+itemId) != null && !"".equals(multipartRequest.getParameter("obj_"+itemId))) {
						HashMap m2 = new HashMap();
						m2.put("EVENT_ID", event_id);
						m2.put("ITEM_ID", itemId);
						m2.put("RSLT_DESC", multipartRequest.getParameter("obj_"+itemId));
						m2.put("RGST_IDNT", user_id);
						action1.add(m2);
					}
				break;
				case 3:
					if (multipartRequest.getParameterValues("obj_"+itemId) != null) {
						HashMap m3 = new HashMap();
						m3.put("EVENT_ID", event_id);
						m3.put("ITEM_ID", itemId);
						m3.put("RGST_IDNT", user_id);
						m3.put("CORANS_ARR", multipartRequest.getParameterValues("obj_"+itemId));
						action2.add(m3);
					}
				break;
				default:	break;
			}
		}
		
		//vo셋팅
		vo.setAction1(action1);
		vo.setAction2(action2);
		
		
		adminEventMgntWebService.uptEventItemSeqn(vo);//순서변경
		adminEventMgntWebService.addEventCorans(vo);//답변등록
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/admin/eventMgnt/multiQustMgnt.do?event_id="+event_id+"&user_id="+user_id+"&scss=y");
		return mv;
	}	
	

	
	/**
	 * 항목설정
	 * @param req
	 * @param res
	 * @return
	 */
	@RequestMapping("/admin/eventMgnt/multiQustItemMgnt")
	public ModelAndView multiQustItemMgntView(HttpServletRequest req, HttpServletResponse res) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/admin/eventMgnt/multiQustItemMgnt");
		return mv;
	}
	
	/**
	 * 항목설정 액션
	 * @param req
	 * @param res
	 * @return
	 */
	@RequestMapping("/admin/eventMgnt/multiQustItemMgntAction")
	public ModelAndView multiQustItemMgntAction(HttpServletRequest req, HttpServletResponse res) {
		String event_id = req.getParameter("event_id");
		String user_id = req.getParameter("user_id");
		
		//seq
		HashMap param = new HashMap();
		param.put("EVENT_ID", event_id);
		Map mSeq = adminEventMgntWebService.getMultiQustItemMgntSeq(param);
		
		//문항
		String item_type_cd = req.getParameter("item_type_cd");
		String item_desc = req.getParameter("item_desc");
		String max_choice_cnt = req.getParameter("max_choice_cnt");
		
		HashMap ml_event_item = new HashMap();
		ml_event_item.put("EVENT_ID", event_id);
		ml_event_item.put("ITEM_ID", mSeq.get("SEQ"));
		ml_event_item.put("ITEM_TYPE_CD", item_type_cd);
		ml_event_item.put("ITEM_DESC", item_desc);
		if ("3".equals(item_type_cd)) {
			ml_event_item.put("MAX_CHOICE_CNT", max_choice_cnt);
		}
		ml_event_item.put("RGST_IDNT", user_id);
		
		adminEventMgntWebService.addMultiQustItemMgnt(ml_event_item);//문항add

		//항목
		if ("3".equals(item_type_cd)) {
			String reqDesc[] = req.getParameterValues("item_choice_desc");
			String reqEtcYN[] = req.getParameterValues("item_choice_etc_yn");
			
			ArrayList list = new ArrayList();
			for (int i = 0; i < reqDesc.length; i++) {
				HashMap map = new HashMap();
				map.put("EVENT_ID", event_id);
				map.put("ITEM_ID", mSeq.get("SEQ"));
				map.put("ITEM_CHOICE_DESC", reqDesc[i]);
				map.put("ETC_YN", reqEtcYN[i]);
				list.add(map);
			}
			adminEventMgntWebService.addMultiQustItemMgntChoice(list);//항목add
		}
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/admin/eventMgnt/multiQustMgnt.do?event_id="+event_id+"&user_id="+user_id);
		return mv;
	}
	
	/**문항 삭제
	 * @param req
	 * @param res
	 * @param vo
	 * @return
	 */
	@RequestMapping("/admin/eventMgnt/delMultiQustItemMgnt")
	public ModelAndView delMultiQustItemMgnt(HttpServletRequest req, HttpServletResponse res, MlEventItem vo) {
		adminEventMgntWebService.delMultiQustItemMgnt(vo);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/admin/eventMgnt/multiQustMgnt.do?event_id="+vo.getEvent_id()+"&user_id="+vo.getUser_id());
		return mv;
	}
	
	/**문항수정
	 * @param req
	 * @param res
	 * @param vo
	 * @return
	 */
	@RequestMapping("/admin/eventMgnt/uptMultiQustItemMgnt.do")
	public ModelAndView uptMultiQustItemMgntView(HttpServletRequest req, HttpServletResponse res, MlEventItem vo) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/admin/eventMgnt/multiQustItemMgnt");
		mv.addObject("edata",adminEventMgntWebService.uptMultiQustItemMgntView(vo));
		mv.addObject("stat","upt");
		return mv;
	}
	
	/**문항수정 액숀
	 * @param req
	 * @param res
	 * @param vo
	 * @return
	 */
	@RequestMapping("/admin/eventMgnt/uptMultiQustItemMgntAction.do")
	public ModelAndView uptMultiQustItemMgntAction(HttpServletRequest req, HttpServletResponse res, MlEventItem vo) {
		//vo.setItem_choice_desc_arr(req.getParameterValues("item_choice_desc"));
		//vo.setItem_choice_etc_yn_arr(req.getParameterValues("item_choice_etc_yn"));
		
		adminEventMgntWebService.uptMultiQustItemMgnt(vo);
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/admin/eventMgnt/multiQustMgnt.do?event_id="+vo.getEvent_id()+"&user_id="+vo.getUser_id());
		return mv;
	}
}
