package kr.or.copyright.mls.user.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.softforum.xdbe.xCrypto;

import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.user.dao.UserDao;
import kr.or.copyright.mls.user.model.ZipCodeRoad;

public class UserServiceImpl extends BaseService implements UserService {

	//private Log logger = LogFactory.getLog(getClass());
	
	private UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public Map findUserList(Map params) { 
		
		/*resdCorpNumb암호화 str 20121106 성혜진*/
		String sOutput7        = null;
		String sString = (String) params.get("resdCorpNumb");
		xCrypto.RegisterEx("pattern7", 2, new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		sOutput7    = xCrypto.Encrypt("pattern7", sString);
		params.put("resdCorpNumb", sOutput7);
		/*resdCorpNumb암호화 end 2012110 성혜진*/
		
		Map userList = userDao.findUserList(params);
		
		return userList;
	}
	
	public List findPostNumb(Map params) {
		return userDao.findPostNumb(params);
	}
	
	public List findPostRoadNumb(Map params) {
		return userDao.findPostRoadNumb(params);
	}
	
	public Map findUserIdnt(Map params) {
		return userDao.findUserIdnt(params);
	}
	
	public Map findClmsUserIdnt1(Map params) {
		return userDao.findClmsUserIdnt1(params);
	}
	
	public Map findClmsUserIdnt2(Map params) {
		return userDao.findClmsUserIdnt2(params);
	}
	
	public int chkDupLoginId(Map params) {
		return userDao.chkDupLoginId(params);
	}
	
	public void insertUser(Map params) {
		
		/*pwsd,resdCorpNumb암호화 str 20121106 성혜진*/
		String sOutput_H        = null;
		String sOutput_H1        = null;
		String sString = ((String) params.get("pswd")).toUpperCase();
		sOutput_H	= xCrypto.Hash(6, sString);//단방향 암호화(sOutput)
		
		String sString1 = (String) params.get("clmsUserPswd");
		
		if(sString1.length() < 44){
		    sOutput_H1	= xCrypto.Hash(6, sString1);//단방향 암호화(sOutput)
		    params.put("clmsUserPswd", sOutput_H1);
		}
		
		params.put("pswd", sOutput_H);
		
		String sOutput7        = null;
		String rString = (String) params.get("resdCorpNumb");
		xCrypto.RegisterEx("pattern7", 2, new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		sOutput7    = xCrypto.Encrypt("pattern7", rString);
		params.put("resdCorpNumb", sOutput7);
		/*pwsd,resdCorpNumb암호화 end 20121106 성혜진*/ 
		
		userDao.insertUser(params);
	}
	
	public Map selectUserInfo(Map params) {
	    	
	    	Map userInfo = userDao.selectUserInfo(params);
	    	
	    	/*resd복호화 str 20121107 정병호*/
	    	String resdCorpNumb = (String) userInfo.get("RESD_CORP_NUMB");
	    	
	    	if(!resdCorpNumb.equals("ipin")){
	    	    xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
	    	    resdCorpNumb = xCrypto.Decrypt("pattern7", (String) userInfo.get("RESD_CORP_NUMB"));
	    		//resdCorpNumb="8807091066711";
	    	}
	    	
	    	String userDivs = (String) userInfo.get("USER_DIVS");
	    	String uResdCorpNumb = null;
	    	
	    	if(userDivs.equals("03")){
	    	    uResdCorpNumb = (String) userInfo.get("U_RESD_CORP_NUMB");
	    	}else{
	    	    uResdCorpNumb = xCrypto.Decrypt("pattern7", (String) userInfo.get("U_RESD_CORP_NUMB"));
	    		//resdCorpNumb="8807091066711";
		}
	    	
	    	userInfo.put("RESD_CORP_NUMB", resdCorpNumb);
	    	userInfo.put("U_RESD_CORP_NUMB", uResdCorpNumb);
	    	
	    	if(!userDivs.equals("03")){
	    	    if(!resdCorpNumb.equals("ipin")){
	    		if(resdCorpNumb.replaceAll(" ", "").length() == 13){
	    		    String resdCorpNumb1 = resdCorpNumb.substring(0, 6);	    	    
	    		    String resdCorpNumb2 = resdCorpNumb.substring(6);
	    		    userInfo.put("RESD_CORP_NUMB1", resdCorpNumb1);
	    		    userInfo.put("RESD_CORP_NUMB2", resdCorpNumb2);
	    		}else{
	    		    userInfo.put("RESD_CORP_NUMB1", "");
	    		    userInfo.put("RESD_CORP_NUMB2", "");
	    		}
	    	    }
	    	}
	    	
	    	String corpNumb = (String) userInfo.get("CORP_NUMB");
	    	
	    	if(userDivs.equals("01")){
	    	    if(!resdCorpNumb.equals("ipin")){
	    		if(resdCorpNumb.replaceAll(" ", "").length() == 13){
	    		    String resdCorpNumbView = resdCorpNumb.substring(0, 6)+"*******";	    	    
	    		    String resdCorpNumbView2 = resdCorpNumb.substring(0, 7)+"******";
	    		    userInfo.put("RESD_CORP_NUMB_VIEW", resdCorpNumbView);
	    		    userInfo.put("RESD_CORP_NUMB_VIEW2", resdCorpNumbView2);
	    		}else{
	    		    userInfo.put("RESD_CORP_NUMB_VIEW", corpNumb);
	    		    userInfo.put("RESD_CORP_NUMB_VIEW2", corpNumb);
	    		}
	    	    }else{
	    		userInfo.put("RESD_CORP_NUMB", "");
	    	    }
	    	}
	    	/*resd복호화 end 20121107 정병호*/
	    	

	    	return userInfo;
	}
	
	public void updateUserInfo(Map params) {
	
		/*pwsd암호화 str 20121106 성혜진*/
		String sOutput_H        = null;
		String sOutput_H1        = null;
		String sString = ((String) params.get("pswd")).toUpperCase();
		sOutput_H	= xCrypto.Hash(6, sString);//단방향 암호화(sOutput)
		
		String sString1 = (String) params.get("clmsUserPswd");
		
		if(sString1.length() < 44){
		    sOutput_H1	= xCrypto.Hash(6, sString1);//단방향 암호화(sOutput)
		    params.put("clmsUserPswd", sOutput_H1);
		}
		
		params.put("pswd", sOutput_H);
		
		
		/*pwsd암호화 end 2012110 성혜진*/
		
		userDao.updateUserInfo(params);
	}
	/* 양재석 추가 start */
	public Map getInmtUserInfo(Map params) {
		return (Map) userDao.getInmtUserInfo(params);
	}
	/* 양재석 추가 end */
	public Map userIdntPswdSrch(Map params) {
		
		/*resdCorpNumb암호화 str 20121106 성혜진*/
		String sOutput7        = null;
		String sString = (String) params.get("resdCorpNumb");
		xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		sOutput7    = xCrypto.Encrypt("pattern7", sString);
		params.put("resdCorpNumb", sOutput7);
		/*resdCorpNumb암호화 end 20121106 성혜진*/
		
		return userDao.userIdntPswdSrch(params);
	}
	
	public Map userPswdSrch(Map params) {
		
		/*resdCorpNumb암호화 str 20121106 성혜진*/
		/*String sOutput7        = null;
		String sString = (String) params.get("resdCorpNumb");
		xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		sOutput7    = xCrypto.Encrypt("pattern7", sString);
		params.put("resdCorpNumb", sOutput7);*/
		/*resdCorpNumb암호화 end 20121106 성혜진*/
		
		return userDao.userPswdSrch(params);
	}
	
	public void detlUserInfo(Map params) {
		userDao.detlUserInfo(params);
	}
	
	public Map selectClmsUserInfo(Map params) {
		
		/*resdCorpNumb암호화 str 20121112 성혜진*/	
		String rOutput7        = null;
		String rString = (String) params.get("resdCorpNumb");
		xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		rOutput7    = xCrypto.Encrypt("pattern7", rString);
		params.put("resdCorpNumb", rOutput7);
		/*resdCorpNumb암호화 str 20121112 성혜진*/
		return userDao.selectClmsUserInfo(params);
	}
	
	public void insertClmsUserInfo(Map params) {
		
		String pOutput_H        = null;
		String pString = ((String) params.get("pswd")).toUpperCase();
		pOutput_H	= xCrypto.Hash(6, pString);//단방향 암호화(sOutput)
		
		String rOutput7        = null;
		String rString = (String) params.get("resdCorpNumb");
		xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		rOutput7    = xCrypto.Encrypt("pattern7", rString);
		
		String cpOutput_H        = null;
		String cpString = (String) params.get("pswd");
		cpOutput_H	= xCrypto.Hash(6, cpString);//단방향 암호화(sOutput)
		
		params.put("pswd", pOutput_H);
		params.put("resdCorpNumb", rOutput7);
		params.put("clmsPswd", cpOutput_H);
		
		userDao.insertClmsUserInfo(params);
	}
	
	public void insertClmsUserInfo2(Map params) {
		
		String pOutput_H        = null;
		String pString = ((String) params.get("pswd")).toUpperCase();
		pOutput_H	= xCrypto.Hash(6, pString);//단방향 암호화(sOutput)
		
		String rOutput7        = null;
		String rString = (String) params.get("resdCorpNumb");
		xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		rOutput7    = xCrypto.Encrypt("pattern7", rString);
		
		String cpOutput_H        = null;
		String cpString = (String) params.get("clmsUserPswd");
		cpOutput_H	= xCrypto.Hash(6, cpString);//단방향 암호화(sOutput)
		
		params.put("pswd", pOutput_H);
		params.put("resdCorpNumb", rOutput7);
		params.put("clmsUserPswd", cpOutput_H);
		
		userDao.insertClmsUserInfo2(params);
	}
	
	public void updateClmsUserInfo(Map params) {
		
		
		/*********Clms주석처리**********
		240줄********************************/
		/*pwsd암호화 str 20121107 성혜진*/
		String sOutput_H        = null;
		String sString = ((String) params.get("pswd")).toUpperCase();
		sOutput_H	= xCrypto.Hash(6, sString);//단방향 암호화(sOutput)
		params.put("pswd", sOutput_H);
		/*pwsd암호화 end 20121107 성혜진*/
	
		
	
		/* 비밀번호 대문자 처리 (주석처리)*/
		//String sString = ((String) params.get("pswd")).toUpperCase(); 
		//params.put("pswd", sString);
	    
		userDao.updateClmsUserInfo(params);
	}

	public List<ZipCodeRoad> getInitRoadCodeList() {
	    return userDao.getInitRoadCodeList();
	}

	public List<ZipCodeRoad> getRoadCodeList(Map params) {
		  return userDao.getRoadCodeList(params);
	}

	public void updateUserPswd(Map params) {
		
		/*pwsd암호화 str 20121114 성혜진
		String sOutput_H        = null;
		String sString = ((String) params.get("pswd")).toUpperCase();
		sOutput_H	= xCrypto.Hash(6, sString);//단방향 암호화(sOutput)
		params.put("pswd", sOutput_H);
		pwsd암호화 end 20121114 성혜진*/
		userDao.updateUserPswd(params);
	}

	public void updateRegiSsnNo(Map params) {
	    String sOutput7        = null;
	    String sString = (String) params.get("resdCorpNumb");
	    xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
	    sOutput7    = xCrypto.Encrypt("pattern7", sString);
	    params.put("resdCorpNumb", sOutput7);
	    userDao.updateRegiSsnNo(params);
	}

	public void updateUserPswdInfo(Map userMap) {
		
		String userIdnt = (String) userMap.get("userIdnt");
		String pswd = (String) userMap.get("pswd");
		String sOutput_H        = null;
		sOutput_H	= xCrypto.Hash(6, pswd.toUpperCase());//단방향 암호화(sOutput) 20170807 수정
		//sOutput_H	= xCrypto.Hash(6, pswd);//단방향 암호화(sOutput)
		
		userMap.put("userIdnt", userIdnt);
		userMap.put("pswd", sOutput_H);
		
		userDao.updateUserPswdInfo(userMap);
		
	}
	
	public List findSchCommName(Map params) {
		return userDao.findSchCommName(params);
	}
	
	public boolean insertAdmin(Map userMap) throws Exception{
		boolean result = false;
	   
		try{
			String TRST_ORGN_DIVS_CODE = (String)userMap.get( "TRST_ORGN_DIVS_CODE" );
			if( "5".equals( TRST_ORGN_DIVS_CODE ) ){
				int newTrstOrgnCd = userDao.getNewTrstOrgnCd();
				userMap.put( "TRST_ORGN_CODE", newTrstOrgnCd);
				//commandMap.put( "TRST_ORGN_CODE", "9000");
			}
			
			/* pwsd암호화 str 20121108 성혜진 */
			String sOutput_H = null;
			String PSWD = ((String) userMap.get("PSWD")).toUpperCase();
			sOutput_H = xCrypto.Hash( 6, PSWD );// 단방향 암호화(sOutput)
			userMap.put( "PSWD", sOutput_H );
			
			/* pwsd암호화 end 20121108 성혜진 */
			userDao.insertAdmin( userMap );
			result = true;
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

}
