package kr.or.copyright.mls.console.mber.inter;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface FdcrAd83Dao{

	// 회원정보 목록조회
	public List userList( Map map );
	
	/**
	 * 회원정보 카운트
	 * @return
	 * @throws SQLException
	 */
	public int userListCount( Map<String, Object> commandMap ) throws SQLException;
	
	/**
	 * 신탁단체 회원정보 카운트
	 * @return
	 * @throws SQLException
	 */
	public int trstOrgnMgnbListCount( Map<String, Object> commandMap ) throws SQLException;

	// 회원정보 상세조회
	public List userDetlList( Map map );

	// clms회원 비밀번호수정
	public void updateClmsUserInfo( Map map );

	// 신탁단체 담당자목록 조회
	public List trstOrgnMgnbList( Map map );

	// 신탁단체 담당자 상세 조회
	public List trstOrgnMgnbDetlList( Map map );

	// 신탁단체 담당자 입력
	public void trstOrgnMgnbDetlInsert( Map map );

	// 신탁단체 담당자 수정
	public void trstOrgnMgnbDetlUpdate( Map map );

	// 담당자,회원 비밀번호 수정
	public void userMgnbPswdUpdate( Map map );

	// 담당자,ML_CODE 검색
	public List fdcrAd83MlCodeSelectList( Map map );
	
	//담당자 ,업체명으로 ML_USER_INFO 검색
	public List fdcrAd83CommNameSelectList( Map map );
	
	//담당자 ,업체명으로 ML_COMM_WORKS 검색
	public List fdcrAd83commNameSelectListByCommWorks(Map map);
	
	//담당자 ,ML_USER_INFO user_name 수정
	public void fdcrAd83UserNameUpdate( Map map );
	
	//담당자 ,업체명 ML_USER_INFO COMM_NAME 수정
	public void fdcrAd83UserInfoCommNameUpdate( Map map );
	
	//담당자 ,업체명 변경 히스토리
	public void insertCommNameUpdateHistory(Map map) ;
	
	//담당자 ,업체명 ML_COMM_WORKS COMM_NAME 수정
	public void fdcrAd83MlCommWorksCommNameUpdate(Map map);
	
	//담당자 ,업체명 ML_COMM_WORKS_REPORT COMM_NAME 수정
	public void fdcrAd83CommWorksReportCommNameUpdate( Map map );
	
	// 담당자,CLMS 업체명 수정
	public void updateClmsUserUpdate( Map map );
	
	//담당자,ML_USER_INFO 업체 기준 삭제
	public void fdcrAd83CompanyDelete( Map map );
	
	//담당자,ML_USER_INFO 업체 삭제 데이터 백업
	public void fdcrAd83UserBackup( Map map );
	
	//담당자,ML_COMM_WORK_REPORT 업체 삭제 데이터 백업
	public void fdcrAd83CommWorksReportCommNameBackup( Map map );
	
	//담당자,ML_COMM_WORK_REPORT 업체 데이터 삭제
	public void fdcrAd83CommWorksReportCommNameDelete( Map map );
	
	//담당자,ML_COMM_WORKS 업체 삭제 데이터 백업 
	public void fdcrAd83MlCommWorksCommNameBackup( Map map );
	
	//담당자,ML_COMM_WORKS 업체 데이터 삭제
	public void fdcrAd83MlCommWorksCommNameDelete( Map map );
	
	//담당자,ML_COMM_TABLES 업체 삭제 데이터 백업
	public void fdcrAd83CommWorksTablesCommNameBackup(  Map map  );
	
	//담당자,ML_COMM_TABLES 업체 데이터 삭제
	public void fdcrAd83CommWorksTablesCommNameDelete(  Map map  );

	//담당자,ML_COMM_TABLES 업체 데이터 검색
	public List fdcrAd83CommWorksTablesCommNameSelect(  Map map  );
	
	// 그룹 메뉴 조회 하기
	public List adminGroupMenuInfo( Map map );

	// 관리자 아이디 중복체크
	public int adminIdCheck( Map map );

	// 기관/단체 목록 조회
	public List trstOrgnCoNameList( Map map );

	// 대표담당자정보 수정
	public void orgnMgnbUpdate( Map map );

	// 대표담당자 존재여부 체크
	public int orgnMgnbCheck( Map map );

	public int getNewTrstOrgnCd();

	public int trstOrgnCoNameListCount( Map<String, Object> commandMap ) throws SQLException;

	public void fncUpdateFailCnt( Map<String, Object> commandMap );

}
