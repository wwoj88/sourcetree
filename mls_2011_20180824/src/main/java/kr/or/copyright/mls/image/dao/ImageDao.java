package kr.or.copyright.mls.image.dao;

import kr.or.copyright.mls.image.model.ImageDTO;

public interface ImageDao{
	public ImageDTO getImageByWorkId(int worksid );
	public ImageDTO getPhotoByWorkId(int worksid );
}
