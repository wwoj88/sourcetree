package kr.or.copyright.mls.article.dao;

import java.util.List;

import javax.annotation.Resource;

import kr.or.copyright.mls.article.model.Article;
import kr.or.copyright.mls.common.extend.RightDAOImpl;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;

@Repository("ArticleDAO")
public class ArticleSqlMapDao extends RightDAOImpl implements ArticleDAO {
	
	private static final String NAMESPACE = "article.";
	
	public int insertArticle(Article article) {
		return (Integer) getSqlMapClientTemplate().insert(NAMESPACE + "insertArticle", article);
	}

	public void deleteArticle(int articleId) {
		getSqlMapClientTemplate().delete(NAMESPACE + "deleteArticle", articleId);
	}

	@SuppressWarnings("unchecked")
	public List<Article> selectArticleList(int communityId) {
		return (List<Article>) getSqlMapClientTemplate().queryForList(NAMESPACE + "selectArticleList", communityId);
	}

	public Article selectArticleInfo(int articleId) {
		return (Article)getSqlMapClientTemplate().queryForObject(NAMESPACE + "selectArticleInfo", articleId);

	}

}
