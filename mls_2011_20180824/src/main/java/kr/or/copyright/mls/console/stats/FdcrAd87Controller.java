package kr.or.copyright.mls.console.stats;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Service;
import kr.or.copyright.mls.console.stats.inter.FdcrAd87Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 통계관리	 > 월별접속통계
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd87Controller extends DefaultController{

	@Resource( name = "fdcrAd87Service" )
	private FdcrAd87Service fdcrAd87Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd87Controller.class );

	/**
	 * 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd87List1.page" )
	public String fdcrAd87List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd87List1 Start" );
		// 파라미터 셋팅
		String YEAR_MONTH = EgovWebUtil.getString( commandMap, "YEAR_MONTH" );
		if(!"".equals(YEAR_MONTH)){
			fdcrAd87Service.fdcrAd87List1( commandMap );
		}
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "fdcrAd87List1 End" );
		return "/stats/fdcrAd87List1.tiles";
	}
}
