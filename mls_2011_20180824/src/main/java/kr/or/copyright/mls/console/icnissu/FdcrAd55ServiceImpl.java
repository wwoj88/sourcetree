package kr.or.copyright.mls.console.icnissu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.icnissu.inter.FdcrAd53Dao;
import kr.or.copyright.mls.console.icnissu.inter.FdcrAd55Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd55Service" )
public class FdcrAd55ServiceImpl extends CommandService implements FdcrAd55Service{

	@Resource( name = "fdcrAd53Dao" )
	private FdcrAd53Dao fdcrAd53Dao;

	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	/**
	 * 개인저작물 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd55List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd53Dao.selectPrivWorksList( commandMap );

		commandMap.put( "ds_list", list );

	}

	/**
	 * 개인저작물 상세
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd55View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd53Dao.selectPrivWorksDetailList( commandMap );
		List fileList = (List) fdcrAd53Dao.selectPrivWorksFileList( commandMap );

		commandMap.put( "ds_list", list );
		commandMap.put( "ds_fileList", fileList );

	}

	/**
	 * ICN 발급번호 채번
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd55IcnInfo1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd53Dao.getCommId( commandMap );

		commandMap.put( "ds_icnInfo", list );
	}

	/**
	 * ICN 발급번호 저장
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd55Insert1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			// DAO 호출
			fdcrAd53Dao.updateCommId( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * ICN 정보 저장
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd55Insert2( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			// DAO 호출
			String[] CR_IDS = (String[]) commandMap.get( "CR_ID" );

			String row_status = null;
			String updateFg = null;

			// update
			for( int i = 0; i < CR_IDS.length; i++ ){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "CR_ID", CR_IDS[i] );

				// row_status = ds_list.getRowStatus(i);
				row_status = "";

				updateFg = (String) commandMap.get( "UPDATE_FG" );
				param.put( "UPDATE_FG", updateFg );

				if( row_status.equals( "update" ) == true ){
					fdcrAd53Dao.privWorksUpdate( param );
				}
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
