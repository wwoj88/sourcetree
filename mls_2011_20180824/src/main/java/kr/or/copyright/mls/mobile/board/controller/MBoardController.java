package kr.or.copyright.mls.mobile.board.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.mobile.board.model.MBoard;
import kr.or.copyright.mls.mobile.board.service.MBoardService;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class MBoardController extends MultiActionController {

	private MBoardService MBoardService;
	
	
	public void setMBoardService (MBoardService MBoardService) {
		this.MBoardService = MBoardService;
	}
	
	public ModelAndView faqList(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		return list(request, respone, "1");
	}
	
	public ModelAndView list(HttpServletRequest request,
			HttpServletResponse respone, String menuSeqn) throws Exception {
		
		ModelAndView mv = null;
		
		Map params = new HashMap();
		
		
		params.put("menuSeqn", menuSeqn);
		params.put("PAGE_NO", ServletRequestUtils.getIntParameter(request, "page_no", 1));
		params.put("srchText", ServletRequestUtils.getStringParameter(request, "srchText", ""));
		
		int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
		
		if(menuSeqn.equals("8")) {
			mv = new ModelAndView ("mobile/ftaqList", "list", MBoardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		} else {
			System.out.println("menuSeqn" + menuSeqn);
			mv = new ModelAndView ("mobile/board/ftaqList", "list", MBoardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		}
			
		mv.addObject("params", params);
		return mv;
		
	}
	
	public ModelAndView view(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
	
		ModelAndView mv = null;
		
		MBoard board = new MBoard();
		bind(request, board);
		
		mv = new ModelAndView("mobile/board/ftaqView", "board", MBoardService.boardView(board));
		mv.addObject("srcBoard", board);
		
		return mv;
	}
}
