package kr.or.copyright.mls.console.setup;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 설정 > 보고저작물관리
 * 
 * @author wizksy
 */
@Controller
public class FdcrAdB8Controller extends DefaultController{

	@Resource( name = "fdcrAdB8Service" )
	private FdcrAdB8ServiceImpl fdcrAdB8Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdB8Controller.class );

	/**
	 * 보고저작물 등록기간 관리
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/setup/fdcrAdB8List1.page" )
	public String fdcrAdB8List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdB8List1 Start" );
		fdcrAdB8Service.fdcrAdB8List1( commandMap );
		model.addAttribute( "ds_list_0", commandMap.get( "ds_list_0" ) );
		return "setup/fdcrAdB8List1.tiles";
	}

	/**
	 * 보고저작물 등록기간 수정
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/setup/fdcrAdB8Update1.page" )
	public String fdcrAdB8Update1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdB0Update1 Start" );
		boolean isSuccess = fdcrAdB8Service.fdcrAdB8Update1( commandMap );
		if( isSuccess ){
			return returnUrl( model, "저장했습니다.", "/console/setup/fdcrAdB8List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/setup/fdcrAdB8List1.page" );
		}
	}
}
