package kr.or.copyright.mls.image.controller;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import kr.or.copyright.mls.image.model.ImageDTO;
import kr.or.copyright.mls.image.service.ImageService;
import signgate.provider.ec.codec.CorruptedCodeException;


public class ImageController extends MultiActionController{
	private static final Logger logger = LoggerFactory.getLogger( ImageController.class );
	
	private ImageService imageService;
	private ImageDTO imageDTO;
	
	public ModelAndView defaultMethod(HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException, GeneralSecurityException, CorruptedCodeException, UnsupportedEncodingException {
		int worksid= Integer.parseInt( request.getParameter( "worksid" ) );
		int genreCode= Integer.parseInt( request.getParameter( "genreCode" ) );
		
		try {
			imageDTO = imageService.getImageByWorkId(worksid,genreCode);
			System.out.println( imageDTO );
		}catch (Exception e) {
			e.printStackTrace();
		}
		request.setCharacterEncoding("euc-kr");	// EUC-KR�� ����		
		request.setAttribute( "ImageDTO", imageDTO );
		
		return new ModelAndView("imgviewer/imgviewer");
	}
	
	public ImageService getImageService(){
		return imageService;
	}
	
	public void setImageService( ImageService imageService ){
		this.imageService = imageService;
	}
}
