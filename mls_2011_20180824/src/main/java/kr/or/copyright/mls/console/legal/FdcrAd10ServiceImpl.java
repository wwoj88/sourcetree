package kr.or.copyright.mls.console.legal;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd10Service;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.util.StringUtil;

import org.springframework.stereotype.Service;

import com.softforum.xdbe.xCrypto;

@Service( "fdcrAd10Service" )
public class FdcrAd10ServiceImpl extends CommandService implements FdcrAd10Service{

	// AdminStatMgntSqlMapDao
	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	// AdminCommonSqlMapDao
	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	/**
	 * 보상금 공탁공고 게시판 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10List1( Map<String, Object> commandMap ) throws Exception{

		List pageList = (List) fdcrAd01Dao.statBord03RowCount( commandMap ); // 페이징카운트
		//List pageList2 = (List) fdcrAd01Dao.statBord0203RowList( commandMap ); // 페이징카운트
		//List pageList = (List) fdcrAd01Dao.statBord03RowCount( commandMap ); // 페이징카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 1 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		
		// DAO 호출
		List list = (List) fdcrAd01Dao.statBord03List( commandMap );
		//List pageList = (List) fdcrAd01Dao.statBord0203RowList( commandMap ); // 페이징카운트
		//List pageCount = (List) fdcrAd01Dao.statBord03RowCount( commandMap ); // 카운트
		//pagination( commandMap, pageList, 0 );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_count", pageList );
		//commandMap.put( "ds_count", pageCount );

	}

	/**
	 * 보상금 공탁공고 게시판 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List detailList = (List) fdcrAd01Dao.statBordDetail( commandMap );
		List fileList = (List) fdcrAd01Dao.statBord01File( commandMap );
		// 보완요청 LIST
		List suplList = (List) fdcrAd01Dao.selectBordSuplItemList( commandMap );

		commandMap.put( "ds_list", detailList );
		commandMap.put( "ds_file", fileList );
		commandMap.put( "ds_supl_list", suplList );
	}

	/**
	 * 보상금 공탁공고 게시판 등록자 정보 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10Pop1( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List list2 = (List) consoleCommonDao.userDetlList( commandMap );

		/* resd복호화 str 20121108 정병호 */
		
		xCrypto.RegisterEx(
			"pattern7",
			2,
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
			"pool1",
			"mcst_db",
			"mcst_owner",
			"mcst_table",
			"pattern7" );

		Map list = new HashMap();
		list = (Map) list2.get( 0 );

		String RESD_CORP_NUMB = "";

		// 주민번호가 notnull 인 경우만.
		if( list.get( "RESD_CORP_NUMB" ) != null
			&& list.get( "RESD_CORP_NUMB" ).toString().replaceAll( " ", "" ).length() > 0 ){
			RESD_CORP_NUMB = xCrypto.Decrypt( "pattern7", (String) list.get( "RESD_CORP_NUMB" ) ).replaceAll( " ", "" );// (원본
																														// 주민번호
																														// 복호화)

			String RESD_CORP_NUMB1 = RESD_CORP_NUMB.substring( 0, 6 );
			String RESD_CORP_NUMB2 = ""; // RESD_CORP_NUMB.substring(6);

			list.put( "RESD_CORP_NUMB", RESD_CORP_NUMB1 + "-" + RESD_CORP_NUMB2 );
		}
		
		commandMap.put( "ds_list", list );
		
		
		commandMap.put( "ds_list", list2 );


	}

	/**
	 * 보상금 공탁공고 게시판 승인 공고
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd10Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBord03Mgnt( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 보상금 공탁공고 게시판 승인 처리상태 수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd10Update2( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBordStatUpdate( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 보상금 공탁공고 게시판 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd10Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBordStatDeltUpdate( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 보상금 공탁공고 게시판 공고내용 보완팝업 폼
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10Pop2( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List detailList = (List) fdcrAd01Dao.statBord01Detail( commandMap );
		List fileList = (List) fdcrAd01Dao.statBord01File( commandMap );
		List suplCdList = (List) fdcrAd01Dao.selectCdList( commandMap );
		List genreCdList = (List) fdcrAd01Dao.selectCdList( commandMap );
		// 보완요청 LIST

		commandMap.put( "ds_list", detailList );
		commandMap.put( "ds_file", fileList );
		commandMap.put( "ds_code", suplCdList );
		commandMap.put( "ds_genre", genreCdList );
	}

	/**
	 * 보상금 공탁공고 게시판 공고내용 수정 및 보완이력 등록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd10SuplRegi1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			List<Map> sendList = new ArrayList();
			List<Map> contentList = new ArrayList();
			Map sendMap = new HashMap();
			Map mailMap = new HashMap();
			String msgStorCd = "";
			
			ArrayList<Map<String, Object>> suplList = (ArrayList<Map<String, Object>>) commandMap.get( "suplList" );
			String RGST_IDNT = (String) commandMap.get( "RGST_IDNT" );
			
			int suplId = fdcrAd01Dao.selectMaxSuplId();
			int suplSeq = fdcrAd01Dao.selectMaxSuplSeq( commandMap );
			int bordCd = Integer.parseInt( commandMap.get( "BORD_CD" ).toString() );
			if( bordCd == 1 ){
				commandMap.put( "SUPL_CD", 1 );
				msgStorCd = "4";
			}else if( bordCd == 5 ){
				commandMap.put( "SUPL_CD", 2 );
				msgStorCd = "5";
			}
			commandMap.put( "SUPL_SEQ", suplSeq );
			commandMap.put( "SUPL_ID", suplId );
			fdcrAd01Dao.suplIdInsert( commandMap );
			fdcrAd01Dao.suplHistInsert( commandMap );
			
			
			for( int i = 0; i < suplList.size(); i++ ){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "SUPL_ID", suplId );
				param.put( "SUPL_SEQ", suplSeq );
				
				param.put( "SUPL_ITEM", suplList.get( i ).get( "supl_item" ) );
				param.put( "SUPL_ITEM_CD", suplList.get( i ).get( "supl_item_cd" ) );
				param.put( "PRE_ITEM", suplList.get( i ).get( "pre_item" ) );
				param.put( "POST_ITEM", suplList.get( i ).get( "post_item" ) );
				param.put( "RGST_IDNT", RGST_IDNT );
				
				fdcrAd01Dao.suplIdItemInsert( param );

				// 메일 발송 내용
				Map contentMap = new HashMap();
				String preItem = (String) param.get( "PRE_ITEM" );
				String postItem = (String) param.get( "POST_ITEM" );

				contentMap.put( "RNUM", i + 1 );
				contentMap.put( "SUPL_ITEM", param.get( "SUPL_ITEM" ) );
				contentMap.put( "CONTENT", "[" + preItem + "] 에서 [" + postItem + "] (으)로 보완" );

				contentList.add( contentMap );
			}
			

			fdcrAd01Dao.statBordSuplUpdate( commandMap );

			/* 메일 발송준비 */

			
			String tite = (String) commandMap.get( "TITE" );// 저작물 제호

			StringBuffer sb = new StringBuffer();
			sb.append( "				<tr>" );
			sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">" );
			sb.append( "					<p style=\"font-weight:bold; background:url(" + Constants.DOMAIN_HOME
				+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">보완정보</p>" );
			sb.append( "					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">" );
			sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME
				+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 대상 저작물 : " + tite + "</li>" );
			// sb.append("						<li style=\"background:url(/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">신청일자 : 0000.00.00</li>");
			sb.append( "					</ul>" );
			sb.append( "					</td>" );
			sb.append( "				</tr>" );
			sb.append( "				<tr>" );
			sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">" );
			sb.append( "					<p style=\"font-weight:bold; background:url(" + Constants.DOMAIN_HOME
				+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">진행현황</p>" );
			sb.append( "					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">" );
			sb.append( "						<thead>" );
			sb.append( "							<tr>" );
			sb.append( "								<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"10%\">번호</th>" );
			sb.append( "								<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">공고항목</th>" );
			sb.append( "								<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">보완 세부내역</th>" );
			sb.append( "							</tr>" );
			sb.append( "						</thead>" );
			sb.append( "						<tbody>" );
			for( int i = 0; i < contentList.size(); i++ ){
				sb.append( "							<tr>" );
				sb.append( "							<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"
					+ contentList.get( i ).get( "RNUM" ) + "</td>" );
				sb.append( "							<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"
					+ contentList.get( i ).get( "SUPL_ITEM" ) + "</td>" );
				sb.append( "							<td style=\"padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"
					+ contentList.get( i ).get( "CONTENT" ) + "</td>" );
				sb.append( "							</tr>" );
			}
			sb.append( "						</tbody>" );
			sb.append( "					</table>" );
			sb.append( "				</td>" );
			sb.append( "			</tr>" );

			mailMap.put( "CONTENT_HTML", new String( sb ) );

			mailMap.put( "TITLE", "공고정보 보완처리가 완료되었습니다." );// 메일제목
			mailMap.put( "MSG_STOR_CD", msgStorCd );
			mailMap.put( "RGST_IDNT", (String) commandMap.get( "RGST_IDNT" ) );
			mailMap.put( "SEND_MAIL_ADDR", (String) commandMap.get( "SEND_MAIL_ADDR" ) );
			mailMap.put( "SUPL_ID", suplId );
			sendMap.put( "RECV_MAIL_ADDR", (String) commandMap.get( "RECV_MAIL_ADDR" ) );
			sendMap.put( "RECV_CD", 1 );
			sendMap.put( "RECV_NAME", (String) commandMap.get( "RGST_NAME" ) );
			sendMap.put( "SEND_MAIL_ADDR", (String) commandMap.get( "SEND_MAIL_ADDR" ) );

			sendList.add( sendMap );
			/*
			 * mailMap 필요 컬럼 MSG_STOR_CD - 메세지함 구분코드 TITLE - 메일제목 CONTENT - 메일내용
			 * txt CONTENT_HTML - 메일내용 HTML RGST_IDNT - 발신자 아이디 SEND_MAIL_ADDR -
			 * 발신자 메일주소 SUPL_ID - 보완 아이디 sendList Map 필요 컬럼 RECV_CD - 수신구분코드
			 * RECV_MAIL_ADDR - 수신자 메일 주소 RECV_NAME - 수신자 이름 SEND_MAIL_ADDR -
			 * 발신자 메일주소
			 */
			consoleCommonService.sendEmail( sendList, mailMap );
			 
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 보상금 공탁공고 게시판 보완내역 메일 발신함 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10SuplSendList1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd01Dao.selectBordSuplSendList( commandMap );
		commandMap.put( "ds_list", list );

	}

	/**
	 * 보상금 공탁공고 게시판 공고정보 보완내역 팝업
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10SuplListPop1( Map<String, Object> commandMap ) throws Exception{

		List list = new ArrayList();
		// DAO 호출
		String msgStorCd = StringUtil.nullToEmpty( (String) commandMap.get( "MSG_STOR_CD" ) );
		// System.out.println(msgStorCd);
		if( "6".equals( msgStorCd ) ){ // 상당한 노력
			list = (List) fdcrAd01Dao.selectWorksSuplItemList( commandMap );
		}else if( "5".equals( msgStorCd ) ){// 보상금 공탁공고(5)
			commandMap.put( "BIG_CODE", "89" );// 보상금 공탁공고 보완항목 코드값
			list = (List) fdcrAd01Dao.selectBordSuplItemList( commandMap );
		}else if( "4".equals( msgStorCd ) ){// 저작권자 조회공고(4),
			commandMap.put( "BIG_CODE", "88" );// 저작권자 조회공고 보완항목 코드값
			list = (List) fdcrAd01Dao.selectBordSuplItemList( commandMap );
		}
		commandMap.put( "ds_list", list );
	}

	/**
	 * 보상금 공탁공고 게시판 공고내용 불러오기
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> fdcrAd10Detail( Map<String, Object> commandMap ) throws Exception{
		return fdcrAd01Dao.statBord10Detail( commandMap );
	}
	
}
