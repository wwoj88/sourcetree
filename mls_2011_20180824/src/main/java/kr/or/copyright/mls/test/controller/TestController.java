package kr.or.copyright.mls.test.controller;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import kr.or.copyright.mls.statBord.service.AnucBordService;
import signgate.provider.ec.codec.CorruptedCodeException;


public class TestController extends MultiActionController{
	private static final Logger logger = LoggerFactory.getLogger( TestController.class );
	
	private TestDao testDao;
	private imageVo ImageVo;
	public void setTestDao( TestDao testDao ){
		this.testDao = testDao;
	}

	public ModelAndView testMethod(HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException, GeneralSecurityException, CorruptedCodeException, UnsupportedEncodingException {
		
		int worksid= Integer.parseInt( request.getParameter( "worksid" ) );
		System.out.println( worksid );
		
		try {
				ImageVo = testDao.testServiceMethod(worksid);
				System.out.println( ImageVo );
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		/*String requestString = request.getParameter( "test" );
		System.out.println( "testMethod call" );
		
		Aes256_new aes= new Aes256_new();
		String key = "abcdefghijklmnopqrstuvwxyz123456";
		String req_str = replace(requestString);
		System.out.println( req_str );
		String plainText;
		String encodeText;
		;
		// Encrypt
		plainText  = "imcore.net";
		encodeText = AES256Cipher.AES_Encode(plainText, key);		
		System.out.println("AES256_Encode : "+encodeText);
		 
		// Decrypt
		String decodeText;
		decodeText = AES256Cipher.AES_Decode(req_str, key);
		System.out.println("AES256_Decode : "+decodeText);
*/
		request.setAttribute( "ImageVo", ImageVo );
		
		return new ModelAndView("imgviewer/imgviewer");
	}
	
	public String replace(String url) {
	    url= url.replace("%26","&"); 
	    url= url.replace("%2B","+"); 
	    return url;
	}
}
