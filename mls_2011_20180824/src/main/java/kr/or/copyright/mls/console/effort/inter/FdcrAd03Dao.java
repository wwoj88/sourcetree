package kr.or.copyright.mls.console.effort.inter;

import java.util.List;
import java.util.Map;


public interface FdcrAd03Dao {
	
	public List selectStatProcAllLogList(Map map);
	
	public List selectStatWorksAllLogList(Map map);
	
	public void callProcGetStatProcOrd(Map map); 
	
	public void callStatProc(Map map);
	
	public void callStatProc01(Map map);
	
	public void callStatProc02(Map map);
	
	public void callGetStatWorksord(Map map);
	
	public void callChangeStatWorks(Map map);
	
	public void statProcAllLogUpdate(Map map);
	
	public void chgStatWorksUpdateYn(Map map);
	
	public List statProcTargPopListCount(Map map);
	
	public List statProcTargPopList(Map map);
	
	public List statProcTargExcpListCount(Map map);
	
	public List statProcTargExcpList(Map map);
	
	public List statProcTargWorksMailList(Map map);
	
	public List inmtReptView(Map map);
	
	public List nonWorksReptView(Map map);
	
	public List statProcPopup(Map map);
	
	public List statProcPopupNewExcel( Map map );
	
	public List statProcPopListCount (Map map);
	
	public List statProcWorksPopListCount(Map map);

	public List statProcWorksPopup(Map map);
	
	public List statProcWorksPopupNewExcel( Map map );
	
	public List worksEntReptView(Map map);
	
	public List reptMgntList(Map map);
	
	public void reptMgntUpdate(Map map);
	
	public void reptMgntUpdateYn(Map map);
	
	// 상당한노력 매칭 정보 
	public List statProcInfo(Map map);
	
	public List statProcInfoNewExcel( Map map );
	
	// 상당한노력 매칭 정보 카운트
	public List statProcInfoCount(Map map);
	
	/*public List selectMailMgntList(Map map);
	
	public void deleteMailMgntList(Map map);
	
	public void insertMailMgntList(Map map);
	
	public int isCheckMailMgntList(Map map);
	
	public List SelectMailMgntCodeList(Map map);*/
	
	public void insertMailList(Map map);
	
}
