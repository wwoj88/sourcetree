package kr.or.copyright.mls.console.legalmanage.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface FdcrAd42Dao{

	public List faqList( Map map );

	public List totalRowFaqList( Map map );

	public List faqDetailList( Map map );

	public void faqInsert( Map map );

	public void faqUpdate( Map map );

	public void faqDelete( Map map );

	public List faqFileList( Map map );

	public void faqFileInsert( Map map );

	public void faqFileUpdate( Map map );

	public void faqFileDelete( Map map );

	public List qnaList( Map map );

	/* 양재석 추가 start */
	public List totalRowQnaList( Map map );

	/* 양재석 추가 end */

	public List qnaDetailList( Map map );

	public void qnaInsert( Map map );

	public int bordSeqnMax();

	public void updateReply( Map map );

	public void qnaUpdate( Map map );

	public void qnaDeleteAll( Map map );

	public void qnaDelete( Map map );

	public List qnaFileList( Map map );

	public List qnaFileAllList( Map map );

	public void updateReplyFile( Map map );

	public void qnaFileInsert( Map map );

	public void qnaFileDeleteAll( Map map );

	public void qnaFileDelete( Map map );

	/* 양재석 추가 start */
	public List sysmNotiList( Map map );

	public List totalRowSysmNotiList( Map map );

	public List sysmNotiDetailList( Map map );

	public void sysmNotiInsert( Map map );

	public void sysmNotiUpdate( Map map );

	public void sysmNotiDelete( Map map );

	public List sysmNotiFileList( Map map );

	public void sysmNotiFileInsert( Map map );

	public void sysmNotiFileUpdate( Map map );

	public void sysmNotiFileDelete( Map map );

	public List notiList( Map map );

	public List totalRowNotiList( Map map );

	public List notiDetailList( Map map );

	public void notiInsert( Map map );

	public void notiUpdate( Map map );

	public void notiDelete( Map map );

	public List notiFileList( Map map );

	public void notiFileInsert( Map map );

	public void notiFileUpdate( Map map );

	public void notiFileDelete( Map map );

	public List promList( Map map );

	public List totalRowPromList( Map map );

	public List promDetailList( Map map );

	public void promInsert( Map map );

	public void promUpdate( Map map );

	public void promDelete( Map map );

	public List promFileList( Map map );

	public void promFileInsert( Map map );

	public void promFileUpdate( Map map );

	public void promFileDelete( Map map );

	public List statList( Map map );

	public List totalRowStatList( Map map );

	public void statInsert( Map map );

	public void statFileInsert( Map map );

	public List clmsWorksList( Map map );

	public List totalClmsWorksList( Map map );

	public List clmsBookList( Map map );

	public List totalClmsBookList( Map map );

	public List clmsMuscDetail( Map map );

	public List clmsBookDetail( Map map );

	/* 양재석 추가 end */

	public void statFileDelete( Map map );

	public void statDelete( Map map );

	public void statUpdate( Map map );

	public Map<String, Object> statDetailList( Map<String, Object> commandMap );
	
	public ArrayList<Map<String, Object>> statFileList( Map<String, Object> commandMap ) throws SQLException;

	public List clmsBroadList( Map map );

	public List totalClmsBroadList( Map map );

	public List clmsImageList( Map map );

	public List totalClmsImageList( Map map );

	public List clmsMovieList( Map map );

	public List totalClmsMovieList( Map map );

	public List clmsBroadCastList( Map map );

	public List totalClmsBroadCastList( Map map );

	public void statFildDelete( Map map );

	public void statFildInsert( Map map );

	public int statSeqn();

	public List admSysmNotiList( Map map );
}
