package kr.or.copyright.mls.adminRght.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class AdminRghtSqlMapDao extends SqlMapClientDaoSupport implements AdminRghtDao {

	// 내권리찾기 조회
	public List rghtList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtList",map);
	}
	
	// 내권리찾기 저작물목록
	public List rghtTempList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtTempList",map);
	}
	
	// 권리찾기 저작물목록(영화)
	public List rghtMvieTempList(Map map){
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtMvieTempList",map);
	}
	
	// 권리찾기 저작물목록(방송대본)
	public List rghtScriptTempList(Map map){
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtScriptTempList",map);
	}
	
	// 권리찾기 저작물목록(방송)
	public List rghtBroadcastTempList(Map map){
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtBroadcastTempList",map);
	}
	
	// 권리찾기 저작물목록(뉴스)
	public List rghtNewsTempList(Map map){
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtNewsTempList",map);
	}
	
	// USER TABLE 통합권자 아이디 UPDATE
	public void rghtDetlUpdate(Map map) {
		getSqlMapClientTemplate().insert("AdminRght.rghtDetlUpdate",map);
	}	
	
	// 내권리찾기 완료처리 
	public void rghtEndUpdate(Map map) {
		getSqlMapClientTemplate().insert("AdminRght.rghtEndUpdate",map);
	}	

	// 내권리찾기 신청 권리정보 삭제(ML_PRPS_RSLT_RGHT)
	public void prpsRsltRghtDelete(Map map) {
		getSqlMapClientTemplate().delete("AdminRght.prpsRsltRghtDelete",map);
	}	
	
	// 내권리찾기 상세조회
	public List rghtDetlList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtDetlList",map);
	}	
	
	//	 신청저작물 상세조회(음악)
	public List rghtMuscDetail(Map map){
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtMuscDetail",map);
	}
	
	// 신청저작물 상세조회(어문)
	public List rghtBookDetail(Map map){
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtBookDetail",map);
	}
	
	// 신청저작물 상세조회(방송대본)
	public List rghtScriptDetail(Map map){
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtScriptDetail",map);
	}
	
	// 신청저작물 상세조회(영화)
	public List rghtMvieDetail(Map map){
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtMvieDetail",map);
	}
	
	// 신청저작물 상세조회(방송)
	public List rghtBroadcastDetail(Map map){
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtBroadcastDetail",map);
	}
	
	// 신청저작물 상세조회(뉴스)
	public List rghtNewsDetail(Map map){
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtNewsDetail",map);
	}
	
	// USER TABLE 통합권자 아이디 UPDATE
	public void userIcnUpdate(Map map) {
		getSqlMapClientTemplate().insert("AdminRght.userIcnUpdate",map);
	}	
	
	// 내권리찾기 저장
	public void rghtUpdate(Map map) {
		getSqlMapClientTemplate().insert("AdminRght.rghtUpdate",map);
	}		
	
	// 내권리찾기 저장
	public void rghtRsltUpdate(Map map) {
		getSqlMapClientTemplate().insert("AdminRght.rghtRsltUpdate",map);
	}		
	
	// 내권리찾기 권리정보 수정
	public void prpsRsltRghtUpdate(Map map){
		getSqlMapClientTemplate().update("AdminRght.prpsRsltRghtUpdate",map);
	}
	
	// 이미지저작물>저작권정보 수정
	public void imgeUpdate(Map map){
		getSqlMapClientTemplate().update("AdminRght.imgeUpdate",map);
	}
	// 내권리찾기 상세조회
	public List getIcnNumb(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminRght.getIcnNumb",map);
	}		
	
	// 첨부파일
	public List rghtFileList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtFileList", map);
	}
	
	public int rghtRsltCnt(Map map) {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("AdminRght.rghtRsltCnt", map);
		return max.intValue(); 
	}
	
	public String rghtPrpsTitle(Map map) {
		String prpsTitile = (String)getSqlMapClientTemplate().queryForObject("AdminRght.rghtPrpsTitle", map);
		return prpsTitile.toString();
	}
	
	public List rghtDealStat(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminRght.rghtDealStat", map);
	}
}
