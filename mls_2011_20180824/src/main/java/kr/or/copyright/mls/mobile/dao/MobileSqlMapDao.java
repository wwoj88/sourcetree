package kr.or.copyright.mls.mobile.dao;

import java.util.HashMap;
import java.util.List;

import kr.or.copyright.mls.mobile.model.FAQ;
import kr.or.copyright.mls.mobile.model.Stat;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class MobileSqlMapDao extends SqlMapClientDaoSupport implements MobileDao {
	@SuppressWarnings("unchecked")
	public List<Stat> statList(HashMap<String,Object> hm) {
		return (List<Stat>)getSqlMapClientTemplate().queryForList("stat.list", hm);
	}
	
	public int statListCnt(HashMap<String,Object> hm) {
		return (Integer)getSqlMapClientTemplate().queryForObject("stat.list.tcnt", hm);
	}
	
	public Stat statDetl(int bordSeqn) {
		return (Stat)getSqlMapClientTemplate().queryForObject("stat.view", bordSeqn);
	}
	
	@SuppressWarnings("unchecked")
	public List<FAQ> faqList(HashMap<String,Object> hm) {
		return (List<FAQ>)getSqlMapClientTemplate().queryForList("faq.list", hm);
	}
	
	public int faqListCnt(HashMap<String,Object> hm) {
		return (Integer)getSqlMapClientTemplate().queryForObject("faq.list.tcnt", hm);
	}

	public void insertUserMobileCount() {
		getSqlMapClientTemplate().insert("MobileBoard.insertUserMobileCount");
	}
	
	public void insertUserMobileCount(int sysCd) {
		getSqlMapClientTemplate().insert("MobileBoard.insertUserMobileCount", sysCd);
	}
}
