package kr.or.copyright.mls.noneRght.dao;

import java.util.List;

import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

public interface NoneRghtDao {
	public int muscRghtCount(RghtPrps rghtPrps);
	public List muscRghtList(RghtPrps rghtPrps);
	public int bookRghtCount(RghtPrps rghtPrps);
	public List bookRghtList(RghtPrps rghtPrps);
	public int scriptRghtCount(RghtPrps rghtPrps);
	public List scriptRghtList(RghtPrps rghtPrps);
	public int imageRghtCount(RghtPrps rghtPrps);
	public List imageRghtList(RghtPrps rghtPrps);
	public int mvieRghtCount(RghtPrps rghtPrps);
	public List mvieRghtList(RghtPrps rghtPrps);
	public int broadcastRghtCount(RghtPrps rghtPrps);
	public List broadcastRghtList(RghtPrps rghtPrps);
	public int newsRghtCount(RghtPrps rghtPrps);
	public List newsRghtList(RghtPrps rghtPrps);
}
