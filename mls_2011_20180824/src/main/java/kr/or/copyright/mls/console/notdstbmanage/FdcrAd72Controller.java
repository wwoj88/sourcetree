package kr.or.copyright.mls.console.notdstbmanage;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 미분배 보상금 관리 > 도서관
 * 
 * @author ljh
 */
@Controller
public class FdcrAd72Controller extends DefaultController{

	@Resource( name = "fdcrAd72Service" )
	private FdcrAd72ServiceImpl fdcrAd72Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd72Controller.class );

	/**
	 * 미분배보상금 관리 > 도서관 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/notdstbmanage/fdcrAd72List1.page" )
	public String fdcrAd72List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		String INMT_YEAR = EgovWebUtil.getString( commandMap, "INMT_YEAR" );
		if(INMT_YEAR.equals( "" ) || INMT_YEAR == null){
			commandMap.put( "INMT_YEAR", "2016" );
		}
		commandMap.put( "ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode() );
		commandMap.put( "INMT_DIVS", "L" );

		fdcrAd72Service.fdcrAd72List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_List" ) );
		System.out.println( "도서관 보상금 관리 목록 조회" );
		return "notdstbmanage/fdcrAd72List1.tiles";
	}

	/**
	 * 미분배보상금 관리 > 도서관 등록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/notdstbmanage/fdcrAd72Insert1.page" )
	public String fdcrAd72Insert1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "INMT_DIVS", "L" );

		String inmtYears = EgovWebUtil.getString( commandMap, "INMT_YEAR" );
		String[] quarters = request.getParameterValues( "quarter" );
		String[] alltInmts = request.getParameterValues( "alltInmt" );
		commandMap.put( "INMT_YEAR", inmtYears );
		commandMap.put( "QUARTER", quarters );
		commandMap.put( "ALLT_INMT", alltInmts );

		boolean isSuccess = fdcrAd72Service.fdcrAd72Insert1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "도서관 등록" );
		
		if( isSuccess ){
			return returnUrl( model, "등록했습니다.", "/console/notdstbmanage/fdcrAd72List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/notdstbmanage/fdcrAd72List1.page" );
		}
	}

}
