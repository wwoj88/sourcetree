package kr.or.copyright.mls.statBord.controller;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.common.common.utils.XssUtil;
import kr.or.copyright.mls.common.mail.SendMail;
import kr.or.copyright.mls.common.service.CommonService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.srch.model.SrchComm;
import kr.or.copyright.mls.srch.model.SrchCros;
import kr.or.copyright.mls.srch.model.SrchStat;
import kr.or.copyright.mls.srch.service.SrchService;
import kr.or.copyright.mls.statBord.model.AnucBord;
import kr.or.copyright.mls.statBord.model.AnucBordFile;
import kr.or.copyright.mls.statBord.model.AnucBordObjc;
import kr.or.copyright.mls.statBord.model.AnucBordSupl;
import kr.or.copyright.mls.statBord.model.Code;
import kr.or.copyright.mls.statBord.service.AnucBordService;
import kr.or.copyright.mls.support.util.CommonUtil2;
import kr.or.copyright.mls.support.util.FileUploadUtil;
import kr.or.copyright.mls.support.util.SessionUtil;
import kr.or.copyright.mls.support.util.StringUtil;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;

import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class AnucBordController{

	private Logger log = Logger.getLogger( this.getClass() );
	private List<AnucBord> list;
	private List fileList;
	private String realUploadPath = FileUtil.uploadPath();

	@javax.annotation.Resource( name = "commonService" )
	private CommonService commonService;

	@javax.annotation.Resource( name = "AnucBordService" )
	private AnucBordService AnucBordService;

	@Resource( name = "SrchService" )
	private SrchService srchService;

	@Autowired
	private MessageSourceAccessor message;
	// ---------------- 공고 게시판 & 법정허락(statBo 01,03, 04, 05 List, Detl, Regi, Find)
	// ----------------

	@RequestMapping( value = "/statBord/test.do" )
	public ModelAndView test() throws Exception{
		// List<AnucBord> list= AnucBordService.SelTest();
		// int totalRow = 23;
		// int bigCode = 35;
		// Map<Object, Object> params = new HashMap<Object, Object>();
		// params.put("bigCode", bigCode);
		// List<Code> CodeList = AnucBordService.getCodeList(params);
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo01RegiPop" );
		// mav.addObject("CodeList", CodeList);
		// mav.addObject("AnucBord", list);
		// mav.addObject("totalRow", totalRow);
		return mav;
	}

	@RequestMapping( "/statBord/tests.do" )
	public ModelAndView deleteFile(){

		// AnucBordService.deleteTest(attcSeqn);
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/testt" );
		return mav;
	}

	// 파일크기 체크
	@RequestMapping( value = "/statBord/fileSizeCk.do", method = RequestMethod.POST )
	public ModelAndView fileSizeCk( HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		System.out.println( "????" );
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );
		int fileSize = 0;
		Iterator fileNameIterator = multipartRequest.getFileNames();
		System.out.println( fileNameIterator );
		while( fileNameIterator.hasNext() ){
			System.out.println( "5" );
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				fileSize = (int) multiFile.getSize();
			}
		}
		ModelAndView mav = new ModelAndView( "jsonReport" );
		mav.addObject( "fileSize", fileSize );
		return mav;
	}

	// 리스트
	@RequestMapping( method = RequestMethod.GET, value = "/statBord/statBo01List.do" )
	public ModelAndView statBo01List( @RequestParam( value = "bordCd", required = true ) int bordCd,
		@RequestParam( value = "divsCd", required = true ) int divsCd,
		HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{
		AnucBord srchParam = new AnucBord();
		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		String openYn = "Y";
		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setBordCd( bordCd );
		srchParam.setDivsCd( divsCd );
		srchParam.setOpenYn( openYn );
		int totalRow = AnucBordService.countAnuc( srchParam );
		list = AnucBordService.selectAnuc( srchParam );
		if( divsCd == 3 ){
			for( int i = 0; i < list.size(); i++ ){
				int count = AnucBordService.countAnucObjc( (int) list.get( i ).getBordSeqn() );
				list.get( i ).setObjcCount( count );
			}
		}
		int genreCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", genreCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );

		int bordNo = (int) bordCd;
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo01List" );
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "bordCd", bordNo );
		mav.addObject( "divsCd", divsCd );
		mav.addObject( "codeList", ListCode );
		return mav;
	}

	// 등록 화면
	@RequestMapping( "/statBord/statBo01Regi.do" )
	public ModelAndView statBo01Regi( @RequestParam( value = "bordCd", required = true ) int bordCd ) throws Exception{
		ModelAndView mav = new ModelAndView();
		if( bordCd == 5 ){
			mav.setViewName( "/statBord/statBo05Regi" );
		}else{
			mav.setViewName( "/statBord/statBo01Regi" );
		}
		int bigCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", bigCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );
		mav.addObject( "bordCd", bordCd );
		mav.addObject( "codeList", ListCode );
		return mav;
	}

	// 등록 ( insert )
	@RequestMapping( value = "/statBord/statBo01Write.do", method = RequestMethod.POST )
	public ModelAndView statBo01Write( @ModelAttribute AnucBord anucBord,
		HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{
		// 파일첨부
		AnucBordFile anucBordFile = new AnucBordFile();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );

		anucBord.setBordCd( StringUtil.nullToZero( multipartRequest.getParameter( "bordCd" ) ) );
		anucBord.setBordSeqn( AnucBordService.getBordSeqn() );
		anucBord.setRgstIdnt( multipartRequest.getParameter( "rgstIdnt" ) );
		anucBord.setModiIdnt( multipartRequest.getParameter( "modiIdnt" ) );
		anucBord.setTite( XssUtil.unscript( multipartRequest.getParameter( "tite" ) ) );
		String bordDesc = multipartRequest.getParameter( "bordDesc" );
		if( bordDesc != null ){
			bordDesc = bordDesc.replace( "\r\n", "<br>" );
			anucBord.setBordDesc( XssUtil.unscript( bordDesc ) );
		}
		anucBord.setDivsCd( StringUtil.nullToZero( multipartRequest.getParameter( "divsCd" ) ) );
		anucBord.setGenreCd( StringUtil.nullToZero( multipartRequest.getParameter( "genreCd" ) ) );
		anucBord.setAnucItem1( XssUtil.unscript( multipartRequest.getParameter( "anucItem1" ) ) );
		anucBord.setAnucItem2( XssUtil.unscript( multipartRequest.getParameter( "anucItem2" ) ) );
		anucBord.setAnucItem3( XssUtil.unscript( multipartRequest.getParameter( "anucItem3" ) ) );
		anucBord.setAnucItem4( XssUtil.unscript( multipartRequest.getParameter( "anucItem4" ) ) );
		anucBord.setAnucItem5( XssUtil.unscript( multipartRequest.getParameter( "anucItem5" ) ) );
		anucBord.setAnucItem6( XssUtil.unscript( multipartRequest.getParameter( "anucItem6" ) ) );
		anucBord.setAnucItem7( XssUtil.unscript( multipartRequest.getParameter( "anucItem7" ) ) );
		anucBord.setAnucItem8( XssUtil.unscript( multipartRequest.getParameter( "anucItem8" ) ) );
		anucBord.setAnucItem9( XssUtil.unscript( multipartRequest.getParameter( "anucItem9" ) ) );
		anucBord.setAnucItem10( XssUtil.unscript( multipartRequest.getParameter( "anucItem10" ) ) );
		anucBord.setAnucItem11( XssUtil.unscript( multipartRequest.getParameter( "anucItem11" ) ) );
		anucBord.setAnucItem12( XssUtil.unscript( multipartRequest.getParameter( "anucItem12" ) ) );
		anucBord.setAnucItem13( multipartRequest.getParameter( "anucItem13" ) );
		anucBord.setAnucItem14( multipartRequest.getParameter( "anucItem14" ) );
		anucBord.setAnucItem15( multipartRequest.getParameter( "anucItem15" ) );

		anucBordFile.setFileName( multipartRequest.getParameter( "fileName" ) );
		anucBordFile.setFilePath( multipartRequest.getParameter( "filePath" ) );
		anucBordFile.setFileSize( multipartRequest.getParameter( "fileSize" ) );

		// File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();

		while( fileNameIterator.hasNext() ){
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
				fileList.add( anucBordFile );
			}
		}
		anucBord.setFileList( fileList );
		AnucBordService.insertAnuc( anucBord );

		// 등록후 리스트 페이징
		int bordNo = (int) anucBord.getBordCd();
		AnucBord srchParam = new AnucBord();

		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		String openYn = "Y";
		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setBordCd( anucBord.getBordCd() );
		srchParam.setOpenYn( openYn );
		int totalRow = AnucBordService.countAnuc( srchParam );
		list = AnucBordService.selectAnuc( srchParam );
		ModelAndView mav = new ModelAndView();
		if( bordNo == 1 ){
			mav.setViewName( "/statBord/statBo01List" );
		}else if( bordNo == 2 ){
			mav.setViewName( "/statBord/statBo03List" );
		}else if( bordNo == 3 ){
			mav.setViewName( "/statBord/statBo04List" );
		}else{
			mav.setViewName( "/statBord/statBo05List" );
		}
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "bordCd", bordNo );
		return mav;
	}

	// 상세보기+이의제기
	@RequestMapping( method = RequestMethod.GET, value = "/statBord/statBo01Detl.do" )
	public ModelAndView statBo01Detl( @RequestParam( value = "bordSeqn", required = true ) int bordSeqn,
		@RequestParam( value = "bordCd", required = true ) int bordCd,
		@RequestParam( value = "divsCd", required = true ) int divsCd ) throws Exception{
		int count = AnucBordService.countAnucObjc( bordSeqn ); // 이의제기 갯수
		List<AnucBord> list2 = AnucBordService.detailAnuc( bordSeqn ); // 게시글 상세보기 게시글 내용
		List<AnucBordFile> list4 = AnucBordService.fileSelectAnuc( bordSeqn ); // 게시판 파일
		List<AnucBordObjc> list3 = AnucBordService.selectAnucObjc( bordSeqn, bordCd ); // 이의제기 댓글 리스트

		int bordNo = (int) bordCd;
		AnucBord anucBord = list2.get( 0 );

		for( int i = 0; i < list3.size(); i++ ){
			int statObjcId = list3.get( i ).getStatObjcId();
			List<AnucBordFile> list5 = AnucBordService.fileSelectObjc( statObjcId ); // 이의제기 파일
			List<AnucBordObjc> list6 = AnucBordService.selectAnucObjcShis( statObjcId );
			list3.get( i ).setShisList( list6 );
			list3.get( i ).setFileList( list5 );
		}

		ModelAndView mav = new ModelAndView();
		if( bordCd == 3 ){
			mav.setViewName( "/statBord/statBo03Detl" );
		}else if( bordCd == 4 ){
			mav.setViewName( "/statBord/statBo04Detl" );
		}else if( bordCd == 5 ){
			mav.setViewName( "/statBord/statBo05Detl" );
		}else{
			mav.setViewName( "/statBord/statBo01Detl" );
		}
		mav.addObject( "AnucBord", anucBord );
		mav.addObject( "AnucBordObjc", list3 );
		mav.addObject( "count", count );
		mav.addObject( "fileList", list4 );
		mav.addObject( "bordCd", bordNo );
		mav.addObject( "divsCd", divsCd );
		return mav;
	}

	// 리스트 검색
	@RequestMapping( method = RequestMethod.GET, value = "/statBord/statBordFind.do" )
	public ModelAndView statBordFind( HttpServletRequest request,
		HttpServletResponse respone,
		@RequestParam( value = "bordCd", required = true ) int bordCd,
		@RequestParam( value = "tite", required = true ) String tite,
		@RequestParam( value = "genreCd", required = true ) int genreCd,
		@RequestParam( value = "divsCd", required = true ) int divsCd ) throws Exception{
		System.out.println( "statBordFind start" );
		AnucBord srchParam = new AnucBord();

		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		String openYn = "Y";
		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setGenreCd( genreCd );
		srchParam.setTite( tite.trim() );
		srchParam.setBordCd( bordCd );
		srchParam.setDivsCd( divsCd );
		srchParam.setOpenYn( openYn );
		String title = tite;
		System.out.println( "title : " + title );
		System.out.println( "srchParam : " + srchParam );
		list = AnucBordService.findAnuc( srchParam );
		int bordNo = (int) bordCd;
		int totalRow = AnucBordService.findCount( srchParam );
		if( divsCd == 3 ){
			for( int i = 0; i < list.size(); i++ ){
				int count = AnucBordService.countAnucObjc( (int) list.get( i ).getBordSeqn() );
				list.get( i ).setObjcCount( count );
				log.info( "######### Count ##########" + count );
			}
		}

		int genreCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", genreCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );

		ModelAndView mav = new ModelAndView();
		if( bordCd == 1 ){
			mav.setViewName( "/statBord/statBo01List" );
		}else if( bordCd == 3 ){
			mav.setViewName( "/statBord/statBo03List" );
		}else if( bordCd == 4 ){
			mav.setViewName( "/statBord/statBo04List" );
		}else{
			mav.setViewName( "/statBord/statBo05List" );
		}
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "title", title );
		mav.addObject( "bordCd", bordNo );
		mav.addObject( "divsCd", divsCd );
		mav.addObject( "genreCd", genreCd );
		mav.addObject( "codeList", ListCode );
		return mav;
	}

	// 이의제기 등록
	@RequestMapping( "/statBord/objcInsert.do" )
	public ModelAndView objcInsert( @ModelAttribute AnucBordObjc anucBordObjc,
		HttpServletRequest request,
		HttpServletResponse respone,
		@RequestParam( value = "bordSeqn", required = true ) int bordSeqn,
		@RequestParam( value = "bordCd", required = true ) int bordCd ) throws Exception{
		int bordNo = (int) bordCd;

		// 파일첨부
		AnucBordFile anucBordFile = new AnucBordFile();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );
		anucBordObjc.setStatObjcId( 0 );
		anucBordObjc.setRgstIdnt( multipartRequest.getParameter( "rgstIdnt" ) );
		String objcDesc = multipartRequest.getParameter( "objcDesc" );
		objcDesc = objcDesc.replace( "\r\n", "<br>" );
		anucBordObjc.setObjcDesc( XssUtil.unscript( objcDesc ) );
		anucBordObjc.setBordCd( bordNo );
		int divsCd = StringUtil.nullToZeroInt( multipartRequest.getParameter( "divsCd" ) );
		anucBordFile.setFileName( multipartRequest.getParameter( "fileName" ) );
		anucBordFile.setFilePath( multipartRequest.getParameter( "filePath" ) );
		anucBordFile.setFileSize( multipartRequest.getParameter( "fileSize" ) );

		// File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();

		while( fileNameIterator.hasNext() ){
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
				fileList.add( anucBordFile );
			}
		}
		anucBordObjc.setFileList( fileList );
		AnucBordService.insertAnucObjc( anucBordObjc );
		int count = AnucBordService.countAnucObjc( bordSeqn ); // 이의제기 갯수
		List<AnucBord> list2 = AnucBordService.detailAnuc( bordSeqn ); // 게시글 상세보기
		List<AnucBordObjc> list3 = AnucBordService.selectAnucObjc( bordSeqn, bordCd ); // 이의제기 리스트
		List<AnucBordFile> list4 = AnucBordService.fileSelectAnuc( bordSeqn ); // 게시판 파일
		for( int i = 0; i < list3.size(); i++ ){
			int statObjcId = list3.get( i ).getStatObjcId();
			List<AnucBordFile> list5 = AnucBordService.fileSelectObjc( statObjcId ); // 이의제기 파일
			List<AnucBordObjc> list6 = AnucBordService.selectAnucObjcShis( statObjcId );
			list3.get( i ).setShisList( list6 );
			list3.get( i ).setFileList( list5 );
		}
		AnucBord anucBord = list2.get( 0 );
		ModelAndView mav = new ModelAndView();
		if( bordCd == 1 ){
			mav.setViewName( "/statBord/statBo01Detl" );
		}else if( bordCd == 3 ){
			mav.setViewName( "/statBord/statBo03Detl" );
		}

		anucBord.setMgntDivs( multipartRequest.getParameter( "mgntDivs" ) );
		anucBord.setWorksTitle( multipartRequest.getParameter( "worksTitl" ) );

		User userInfo = SessionUtil.getSession( request );

		HashMap userMap = new HashMap();

		// 신청제목
		userMap.put( "WORKS_TITL", anucBord.getWorksTitle() );

		// 사용자 정보 셋팅
		userMap.put( "USER_IDNT", userInfo.getUserIdnt() );
		userMap.put( "USER_NAME", userInfo.getUserName() );
		userMap.put( "U_MAIL", userInfo.getMail() );
		userMap.put( "MOBL_PHON", userInfo.getMoblPhon() );

		if( anucBord.getMgntDivs().equals( "BO02_01" ) ){
			userMap.put( "MGNT_DIVS", "BO02_01" );
			userMap.put( "MAIL_TITL", "법정허락 신청 대상저작물 이의 " );
			userMap.put( "MAIL_TITL_DIVS", "신청" );
		}else if( anucBord.getMgntDivs().equals( "BO02_02" ) ){
			userMap.put( "MGNT_DIVS", "BO02_02" );
			userMap.put( "MAIL_TITL", "이용승인신청공고 이의 " );
			userMap.put( "MAIL_TITL_DIVS", "신청" );
		}else if( anucBord.getMgntDivs().equals( "BO02_03" ) ){
			userMap.put( "MGNT_DIVS", "BO02_03" );
			userMap.put( "MAIL_TITL", "저작권자 조회공고 이의 " );
			userMap.put( "MAIL_TITL_DIVS", "신청" );
		}

		List list = new ArrayList();

		list = commonService.getMgntMail( userMap );

		int listSize = list.size();

		String to[] = new String[listSize];
		String toName[] = new String[listSize];

		for( int i = 0; i < listSize; i++ ){
			HashMap map = (HashMap) list.get( i );
			toName[i] = (String) map.get( "USER_NAME" );
			to[i] = (String) map.get( "MAIL" );
		}

		userMap.put( "TO", to );
		userMap.put( "TO_NAME", toName );

		SendMail.send( userMap );

		mav.addObject( "AnucBord", anucBord );
		mav.addObject( "AnucBordObjc", list3 );
		mav.addObject( "count", count );
		mav.addObject( "fileList", list4 );
		mav.addObject( "bordCd", bordNo );
		mav.addObject( "divsCd", divsCd );
		return mav;
	}

	// 이의 제기 삭제
	@RequestMapping( value = "/statBord/objcDelete.do" )
	public ModelAndView objcDelete( @RequestParam( value = "statObjcId", required = true ) int statObjcId,
		@RequestParam( value = "bordSeqn", required = true ) int bordSeqn,
		@RequestParam( value = "bordCd", required = true ) int bordCd,
		@RequestParam( value = "divsCd", required = true ) int divsCd ) throws Exception{
		ModelAndView mav = new ModelAndView();

		AnucBordService.deleteAnucBordObjc( statObjcId );
		int count = AnucBordService.countAnucObjc( bordSeqn ); // 이의제기 갯수
		List<AnucBord> list2 = AnucBordService.detailAnuc( bordSeqn ); // 게시글 상세보기
		List<AnucBordObjc> list3 = AnucBordService.selectAnucObjc( bordSeqn, bordCd ); // 이의제기 리스트
		List<AnucBordFile> list4 = AnucBordService.fileSelectAnuc( bordSeqn ); // 게시판 파일
		for( int i = 0; i < list3.size(); i++ ){
			int statObjcId2 = list3.get( i ).getStatObjcId();
			List<AnucBordFile> list5 = AnucBordService.fileSelectObjc( statObjcId2 ); // 이의제기 파일
			List<AnucBordObjc> list6 = AnucBordService.selectAnucObjcShis( statObjcId2 );
			list3.get( i ).setShisList( list6 );
			list3.get( i ).setFileList( list5 );
		}
		if( count == 0 ){
			AnucBordService.updateAnucBordObjcYnDelete( bordSeqn );
		}
		AnucBord anucBord = list2.get( 0 );
		if( bordCd == 1 ){
			mav.setViewName( "/statBord/statBo01Detl" );
		}else if( bordCd == 3 ){
			mav.setViewName( "/statBord/statBo03Detl" );
		}
		mav.addObject( "AnucBord", anucBord );
		mav.addObject( "AnucBordObjc", list3 );
		mav.addObject( "count", count );
		mav.addObject( "fileList", list4 );
		mav.addObject( "bordCd", bordCd );
		mav.addObject( "divsCd", divsCd );
		return mav;
	}

	@RequestMapping( value = "/statBord/objcUpdate.do" )
	public ModelAndView objcUpdate( @ModelAttribute AnucBordObjc anucBordObjc,
		@RequestParam( value = "bordSeqn", required = true ) int bordSeqn,
		@RequestParam( value = "bordCd", required = true ) int bordCd,
		@RequestParam( value = "statObjcId", required = true ) int statObjcId,
		HttpServletRequest request ) throws Exception{
		AnucBordFile anucBordFile = new AnucBordFile();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );

		ModelAndView mav = new ModelAndView();
		String objcDesc = multipartRequest.getParameter( "objcDesc" );
		anucBordObjc.setRgstIdnt( multipartRequest.getParameter( "rgstIdnt" ) );
		objcDesc = objcDesc.replace( "\r\n", "<br>" );
		anucBordObjc.setObjcDesc( XssUtil.unscript( objcDesc ) );
		anucBordObjc.setStatObjcId( statObjcId );
		String divsCd = multipartRequest.getParameter( "divsCdUp" );
		// 파일첨부
		anucBordFile.setFileName( multipartRequest.getParameter( "fileName" ) );
		anucBordFile.setFilePath( multipartRequest.getParameter( "filePath" ) );
		anucBordFile.setFileSize( multipartRequest.getParameter( "fileSize" ) );

		String fileSeqn = multipartRequest.getParameter( "fileCk" );
		String fileLeng = multipartRequest.getParameter( "fileLength" );
		if( fileLeng != null || !fileLeng.equals( "0" ) ){
			int fileLength = Integer.parseInt( fileLeng );
			if( fileLength > 0 ){
				String[] fileCk = fileSeqn.split( "," );
				for( int i = 0; i < fileCk.length; i++ ){
					int attcSeqn = Integer.parseInt( fileCk[i] );
					AnucBordService.deleteObjcFile( attcSeqn );
				}
			}
		}
		// File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();

		while( fileNameIterator.hasNext() ){
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
				fileList.add( anucBordFile );
			}
		}
		anucBordObjc.setFileList( fileList );
		AnucBordService.updateAnucObjc( anucBordObjc );

		int count = AnucBordService.countAnucObjc( bordSeqn ); // 이의제기 갯수
		List<AnucBord> list2 = AnucBordService.detailAnuc( bordSeqn ); // 게시글 상세보기
		List<AnucBordObjc> list3 = AnucBordService.selectAnucObjc( bordSeqn, bordCd ); // 이의제기 리스트
		List<AnucBordFile> list4 = AnucBordService.fileSelectAnuc( bordSeqn ); // 게시판 파일
		for( int i = 0; i < list3.size(); i++ ){
			int statObjcId2 = list3.get( i ).getStatObjcId();
			List<AnucBordFile> list5 = AnucBordService.fileSelectObjc( statObjcId2 ); // 이의제기 파일
			List<AnucBordObjc> list6 = AnucBordService.selectAnucObjcShis( statObjcId2 );
			list3.get( i ).setShisList( list6 );
			list3.get( i ).setFileList( list5 );
		}
		AnucBord anucBord = list2.get( 0 );
		if( bordCd == 1 ){
			mav.setViewName( "/statBord/statBo01Detl" );
		}else if( bordCd == 3 ){
			mav.setViewName( "/statBord/statBo03Detl" );
		}
		mav.addObject( "AnucBord", anucBord );
		mav.addObject( "AnucBordObjc", list3 );
		mav.addObject( "count", count );
		mav.addObject( "fileList", list4 );
		mav.addObject( "bordCd", bordCd );
		mav.addObject( "divsCd", divsCd );
		return mav;
	}

	// ---------------------------- 마이페이지(거소 불명) ------------------------------

	@RequestMapping( method = RequestMethod.GET, value = "/statBord/statBo06List.do" )
	public ModelAndView statBo06List( @RequestParam( value = "bordCd", required = true ) int bordCd,
		HttpServletRequest request,
		HttpServletResponse response ){

		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();

		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}

		AnucBord srchParam = new AnucBord();
		int totalRow = AnucBordService.countNonAnuc( sessUserIdnt );
		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setRgstIdnt( sessUserIdnt );
		if( bordCd != 1 ){
			list = AnucBordService.selectNonAnuc( srchParam );
		}else{
			list = AnucBordService.selectAnuc( srchParam );
		}
		Date date = new Date();
		SimpleDateFormat yearForm = new SimpleDateFormat( "yyyy" );
		SimpleDateFormat dayForm = new SimpleDateFormat( "yyyyMMdd" );
		String thisMonth = yearForm.format( date ) + "0101";
		String thisDay = dayForm.format( date );

		int genreCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", genreCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );

		int gubunCode = 77;
		Map<Object, Object> param = new HashMap<Object, Object>();
		param.put( "bigCode", gubunCode );
		List<Code> gubunCd = AnucBordService.getCodeList( param );
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo06List" );
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "codeList", ListCode );
		mav.addObject( "gubunList", gubunCd );
		mav.addObject( "defaultJoinDay", thisMonth );
		mav.addObject( "defaultEndDay", thisDay );
		mav.addObject( "Success", 0 );
		return mav;
	}

	@RequestMapping( value = "/statBord/codeList.do" )
	public ModelAndView codeList( HttpServletRequest request ) throws Exception{
		Map<Object, Object> params = new HashMap<Object, Object>();
		int bigCode = ServletRequestUtils.getIntParameter( request, "bigCode", 0 );
		params.put( "bigCode", bigCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );

		ModelAndView mav = new ModelAndView( "jsonReport" );
		mav.addObject( "list", ListCode );
		return mav;
	}

	@RequestMapping( "/statBord/deletecoptHodr.do" )
	public ModelAndView deletecoptHodr( @RequestParam( value = "coptHodrSeqn" ) int coptHodrSeqn ) throws Exception{
		AnucBordService.deletecoptHodr( coptHodrSeqn );
		ModelAndView mav = new ModelAndView();
		return mav;

	}

	// 검색
	@RequestMapping( method = RequestMethod.GET, value = "/statBord/statBo06Find.do" )
	public ModelAndView statBo06Find( HttpServletRequest request,
		@RequestParam( value = "genreCd", required = true ) int genreCd,
		@RequestParam( value = "worksTitle", required = true ) String worksTitle,
		@RequestParam( value = "joinDay", required = true ) String joinDay,
		@RequestParam( value = "endDay", required = true ) String endDay,
		@RequestParam( value = "statCd", required = true ) String statCd ) throws Exception{
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();
		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}
		AnucBord anucBord = new AnucBord();
		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;

		anucBord.setGenreCd( genreCd );
		anucBord.setNowPage( pageNo );
		anucBord.setStartRow( from );
		anucBord.setEndRow( to );
		anucBord.setRgstIdnt( sessUserIdnt );

		// 검색 시작
		int endDays = Integer.parseInt( endDay ) + 1;// 오전 0시 기준이므로 해당날짜를 포항하려면 하루를 더 더한다.
		String endDays2 = String.valueOf( endDays );
		log.info( endDays2 );
		HashMap<Object, Object> findParams = new HashMap<Object, Object>();
		findParams.put( "genreCd", genreCd );
		findParams.put( "worksTitle", worksTitle );
		findParams.put( "joinDay", joinDay );
		findParams.put( "endDay", endDays2 );
		findParams.put( "rgstIdnt", sessUserIdnt );
		findParams.put( "pageNo", pageNo );
		findParams.put( "startRow", from );
		findParams.put( "endRow", to );
		findParams.put( "statCd", statCd );

		List<AnucBord> list = AnucBordService.findNonAnuc( findParams );
		// 검색된 카운트
		int totalRow = AnucBordService.countFindNonAnuc( findParams );
		// 검색끝
		int bigCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", bigCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );

		int gubunCode = 77;
		Map<Object, Object> param = new HashMap<Object, Object>();
		param.put( "bigCode", gubunCode );
		List<Code> gubunCd = AnucBordService.getCodeList( param );

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo06List" );
		mav.addObject( "AnucBord", list );
		mav.addObject( "codeList", ListCode );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", anucBord );
		mav.addObject( "gubunList", gubunCd );
		mav.addObject( "defaultJoinDay", joinDay );
		mav.addObject( "defaultEndDay", endDay );
		mav.addObject( "title", worksTitle );
		mav.addObject( "genreCd", genreCd );
		mav.addObject( "statCd", statCd );
		return mav;
	}

	// 등록화면(거소불명)
	@RequestMapping( "/statBord/statBo06Regi.do" )
	public ModelAndView statBo06Regi( HttpServletRequest request ) throws Exception{
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();

		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}
		int bigCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", bigCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo06Regi" );
		mav.addObject( "codeList", ListCode );
		return mav;
	}

	// 다건등록화면(거소불명)
	@RequestMapping( "/statBord/statBo06RegiMulti.do" )
	public ModelAndView statBo06RegiMulti( HttpServletRequest request ) throws Exception{
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();

		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}
		int bigCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", bigCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo06RegiMulti" );
		mav.addObject( "codeList", ListCode );
		mav.addObject( "totalRow", 0 );
		return mav;
	}

	@RequestMapping( "/statBord/fileDownLoad.do" )
	public ModelAndView fileDownLoad( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		return new ModelAndView( "common/FileDownload" );
	}

	// 다건등록 엑셀 업로드(거소불명)
	@RequestMapping( "/statBord/statBo06RegiMultiExcelUp.do" )
	public ModelAndView statBo06RegiMultiExcelUp( HttpServletRequest request ) throws Exception{
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();

		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}
		request.setCharacterEncoding( "UTF-8" );
		AnucBordFile anucBordFile = new AnucBordFile();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );

		anucBordFile.setFileName( multipartRequest.getParameter( "fileName" ) );
		anucBordFile.setFilePath( multipartRequest.getParameter( "filePath" ) );
		anucBordFile.setFileSize( multipartRequest.getParameter( "fileSize" ) );
		// File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();

		while( fileNameIterator.hasNext() ){
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
				fileList.add( anucBordFile );
			}
		}
		String exe = "";
		if( !StringUtil.isEmpty( anucBordFile.getRealFileName() ) ){
			exe = anucBordFile.getRealFileName().substring(
				anucBordFile.getRealFileName().lastIndexOf( "." ) + 1,
				anucBordFile.getRealFileName().length() );
		}
		String value = "";
		List<AnucBord> excelData = new ArrayList<AnucBord>();

		String bflag = "N";
		try{

			if( exe.equals( "xlsx" ) ){// 엑셀 2007이상

				XSSFWorkbook xworkBook = new XSSFWorkbook(
					new FileInputStream(
						new File( anucBordFile.getFilePath() + "\\" + anucBordFile.getRealFileName() ) ) );

				XSSFSheet xsheet = null;
				XSSFRow xrow = null;
				XSSFCell xcell = null;

				xsheet = xworkBook.getSheetAt( 0 );// 첫 시트
				int rows = xsheet.getPhysicalNumberOfRows(); // 전체 행 갯수
				int countRow = 1;
				for( int i = 5; i < rows; i++, countRow++ ){
					String errorFlag = "";
					xrow = xsheet.getRow( i );
					int cells = xrow.getPhysicalNumberOfCells();// 한 행에 들어 있는 열 데이터 갯수
					AnucBord anucBord = new AnucBord();

					for( int j = 0; j < cells; j++ ){

						int cellCount = 0;
						xcell = xrow.getCell( j );

						switch( xcell.getCellType() ){
							case XSSFCell.CELL_TYPE_NUMERIC: // 숫자형 데이터
								value = "" + (int) xcell.getNumericCellValue();
								break;

							case XSSFCell.CELL_TYPE_STRING: // 문자형 데이터

								value = "" + xcell.getStringCellValue().trim();
								break;

							case XSSFCell.CELL_TYPE_BLANK:
								value = "";
								break;
							/*
							 * case XSSFCell.CELL_TYPE_FORMULA : // 수식형 데이터 break;
							 */
						}

						if( j == cellCount++ ){// 순번 셀 검사
							try{
								anucBord.setInsertRow( Integer.parseInt( value ) );
								if( value.length() == 0 || !CommonUtil2.isNumber( value ) ){
									errorFlag += "A" + ( j ) + "@";
								}
							}
							catch( NumberFormatException ex ){
								anucBord.setInsertRow( -1 );
								errorFlag += "A" + ( j ) + "@";
							}

						}
						if( j == cellCount++ ){ // 신청사유 CODE 검사
							anucBord.setRgstReasCdAndEtc( value );
							if( value.indexOf( "#" ) != -1 ){
								String rgstReas[] = value.split( "#" );// 기타일경우 # 뒤에 사유입력
								if( rgstReas[0].replace( " ", "" ).length() == 0
									|| !CommonUtil2.isNumber( rgstReas[0].replace( " ", "" ) )
									|| Integer.parseInt( rgstReas[0].replace( " ", "" ) ) != 6 ){
									errorFlag += "A" + ( j ) + "@";
								}

							}else{
								if( value.replace( " ", "" ).length() == 0
									|| !CommonUtil2.isNumber( value.replace( " ", "" ) )
									|| Integer.parseInt( value.replace( " ", "" ) ) > 6
									|| value.replace( " ", "" ).length() > 2 ){
									errorFlag += "A" + ( j ) + "@";
								}

							}
						}
						if( j == cellCount++ ){ // 저작물정보 장르CODE 검사
							try{
								anucBord.setGenreCd( Integer.parseInt( value.replace( " ", "" ) ) );
								if( value.length() == 0 || !CommonUtil2.isNumber( value )
									|| Integer.parseInt( value ) > 10 ){
									errorFlag += "A" + ( j ) + "@";
								}
							}
							catch( NumberFormatException ex ){
								// System.err.println("잘못된 입력입니다.숫자만 입력해주세요.");
								anucBord.setGenreCd( -1 );
								errorFlag += "A" + ( j ) + "@";
							}
						}
						if( j == cellCount++ ){ // 저작물정보 제호& 저작권자조회공고 정보 3.저작물의 제호 검사
							anucBord.setTite( value );
							anucBord.setWorksTitle( value );
							if( value.length() == 0 || value.length() > 250 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){// 저작물정보 저작권자정보 검사
							anucBord.setCoptHodrCdAndName( value );

							if( value.length() == 0 ){
								errorFlag += "A" + ( j ) + "@";
							}else{
								String[] co = value.split( "," );
								for( int a = 0; a < co.length; a++ ){
									String[] coNa = co[a].split( "#" );
									if( !CommonUtil2.isNumber( coNa[0].replace( " ", "" ) ) || coNa[1].length() == 0 ){
										errorFlag += "A" + ( j ) + "@"; // #앞에 코드값이 숫자가 아닌경우 에러
									}
								}
							}
						}

						if( j == cellCount++ ){ // 저작권자조회공고 정보 취지 검사
							anucBord.setAnucItem1( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 성명
							anucBord.setAnucItem2( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 주소
							anucBord.setAnucItem3( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 전화번호
							anucBord.setAnucItem4( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 저작물의 제호

							/*
							 * if(value.length()==0||value.length()>500){ errorFlag += "A"+(j)+"@"; }
							 */
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공표표시된 지적재산권자 성명
							anucBord.setAnucItem5( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 저작물발행자
							anucBord.setAnucItem6( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공표년월일
							anucBord.setAnucItem7( value );
							if( value.length() == 0 || value.length() > 500 || !CommonUtil2.isValid( value ) ){
								// 날짜형식이다를때
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작물의 이용 목적
							anucBord.setAnucItem8( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자 이름
							anucBord.setAnucItem9( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";
							}
						}
						if( j == cellCount++ ){// 저작권자조회공고 정보 공고자의 주소
							anucBord.setAnucItem10( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자의 연락처
							anucBord.setAnucItem11( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자의 담당자
							anucBord.setAnucItem12( value );
						}

						anucBord.setCountRow( countRow );
						anucBord.setErrorFlag( errorFlag );

					}
					excelData.add( countRow - 1, anucBord );
				}
			}else if( exe.equals( "xls" ) ){// 엑셀 2003버젼

				HSSFWorkbook hworkBook = new HSSFWorkbook(
					new FileInputStream(
						new File( anucBordFile.getFilePath() + "\\" + anucBordFile.getRealFileName() ) ) );

				HSSFSheet hsheet = null;
				HSSFRow hrow = null;
				HSSFCell hcell = null;

				hsheet = hworkBook.getSheetAt( 0 );// 첫 시트
				int rows = hsheet.getPhysicalNumberOfRows(); // 전체 행 갯수
				int countRow = 1;
				for( int i = 5; i < rows; i++, countRow++ ){
					String errorFlag = "";
					hrow = hsheet.getRow( i );
					int cells = hrow.getPhysicalNumberOfCells();// 한 행에 들어 있는 열 데이터 갯수
					AnucBord anucBord = new AnucBord();

					for( int j = 0; j < cells; j++ ){

						int cellCount = 0;
						hcell = hrow.getCell( j );

						switch( hcell.getCellType() ){
							case HSSFCell.CELL_TYPE_NUMERIC: // 숫자형 데이터
								value = "" + (int) hcell.getNumericCellValue();
								break;

							case HSSFCell.CELL_TYPE_STRING: // 문자형 데이터

								value = "" + hcell.getStringCellValue().trim();
								break;

							case HSSFCell.CELL_TYPE_BLANK:
								value = "";
								break;
							/*
							 * case XSSFCell.CELL_TYPE_FORMULA : // 수식형 데이터 break;
							 */
						}

						if( j == cellCount++ ){// 순번 셀 검사
							try{
								anucBord.setInsertRow( Integer.parseInt( value ) );
								if( value.length() == 0 || !CommonUtil2.isNumber( value ) ){
									errorFlag += "A" + ( j ) + "@";
								}
							}
							catch( NumberFormatException ex ){
								// System.err.println("잘못된 입력입니다.숫자만 입력해주세요.");
								anucBord.setInsertRow( -1 );
								errorFlag += "A" + ( j ) + "@";
							}

						}
						if( j == cellCount++ ){ // 신청사유 CODE 검사
							anucBord.setRgstReasCdAndEtc( value );
							if( value.indexOf( "#" ) != -1 ){
								String rgstReas[] = value.split( "#" );// 기타일경우 # 뒤에 사유입력
								// System.out.println("길이 : " + rgstReas.length);
								if( rgstReas[0].replace( " ", "" ).length() == 0
									|| !CommonUtil2.isNumber( rgstReas[0].replace( " ", "" ) )
									|| Integer.parseInt( rgstReas[0].replace( " ", "" ) ) != 6 ){
									errorFlag += "A" + ( j ) + "@";
								}

							}else{
								// anucBord.setRgstReasCd(Integer.parseInt(value.replace(" ","")));
								if( value.replace( " ", "" ).length() == 0
									|| !CommonUtil2.isNumber( value.replace( " ", "" ) )
									|| Integer.parseInt( value.replace( " ", "" ) ) > 6
									|| value.replace( " ", "" ).length() > 2 ){
									errorFlag += "A" + ( j ) + "@";
								}

							}
						}
						if( j == cellCount++ ){ // 저작물정보 장르CODE 검사
							try{
								anucBord.setGenreCd( Integer.parseInt( value.replace( " ", "" ) ) );
								if( value.length() == 0 || !CommonUtil2.isNumber( value )
									|| Integer.parseInt( value ) > 10 ){
									errorFlag += "A" + ( j ) + "@";
								}
							}
							catch( NumberFormatException ex ){
								// System.err.println("잘못된 입력입니다.숫자만 입력해주세요.");
								anucBord.setGenreCd( -1 );
								errorFlag += "A" + ( j ) + "@";
							}
						}
						if( j == cellCount++ ){ // 저작물정보 제호& 저작권자조회공고 정보 3.저작물의 제호 검사
							anucBord.setTite( value );
							anucBord.setWorksTitle( value );
							if( value.length() == 0 || value.length() > 250 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){// 저작물정보 저작권자정보 검사
							anucBord.setCoptHodrCdAndName( value );

							if( value.length() == 0 ){
								errorFlag += "A" + ( j ) + "@";
							}else{
								String[] co = value.split( "," );
								for( int a = 0; a < co.length; a++ ){
									String[] coNa = co[a].split( "#" );
									if( !CommonUtil2.isNumber( coNa[0].replace( " ", "" ) ) || coNa[1].length() == 0 ){
										errorFlag += "A" + ( j ) + "@"; // #앞에 코드값이 숫자가 아닌경우 에러
									}
								}
							}
						}

						if( j == cellCount++ ){ // 저작권자조회공고 정보 취지 검사
							anucBord.setAnucItem1( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 성명
							anucBord.setAnucItem2( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 주소
							anucBord.setAnucItem3( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 전화번호
							anucBord.setAnucItem4( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 저작물의 제호

							/*
							 * if(value.length()==0||value.length()>500){ errorFlag += "A"+(j)+"@"; }
							 */
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공표표시된 지적재산권자 성명
							anucBord.setAnucItem5( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 저작물발행자
							anucBord.setAnucItem6( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공표년월일
							anucBord.setAnucItem7( value );
							if( value.length() == 0 || value.length() > 500 || !CommonUtil2.isValid( value ) ){
								// 날짜형식이다를때
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작물의 이용 목적
							anucBord.setAnucItem8( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자 이름
							anucBord.setAnucItem9( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";
							}
						}
						if( j == cellCount++ ){// 저작권자조회공고 정보 공고자의 주소
							anucBord.setAnucItem10( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자의 연락처
							anucBord.setAnucItem11( value );
							if( value.length() == 0 || value.length() > 500 ){
								errorFlag += "A" + ( j ) + "@";

							}
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자의 담당자
							anucBord.setAnucItem12( value );
						}

						anucBord.setCountRow( countRow );
						anucBord.setErrorFlag( errorFlag );

					}
					excelData.add( countRow - 1, anucBord );
				}

			}
			bflag = "Y";
		}
		catch( Exception e ){
			e.printStackTrace();
			bflag = "N";
		}

		/*
		 * int bigCode = 35; Map<Object, Object> params = new HashMap<Object, Object>();
		 * params.put("bigCode", bigCode); List<Code> ListCode =
		 * AnucBordService.getCodeList(params);
		 */
		// ModelAndView mav= new ModelAndView();
		// mav.setViewName("/statBord/subStatBo06RegiMulti");
		ModelAndView mav = new ModelAndView( "jsonReport" );

		// mav.addObject("codeList", ListCode);
		mav.addObject( "bflag", bflag );
		mav.addObject( "excelData", excelData );
		mav.addObject( "realFileName", anucBordFile.getRealFileName() );
		mav.addObject( "filePath", anucBordFile.getFilePath() );
		return mav;

	}

	// 다건등록(거소불명)
	@RequestMapping( value = "/statBord/statBo06ExcelWrite.do", method = RequestMethod.POST )
	public ModelAndView statBo06ExcelWrite( /* @ModelAttribute AnucBord anucBord, */
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// request.setCharacterEncoding("UTF-8");
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );

		int result = 0;

		String mgntDivs = XssUtil.unscript( multipartRequest.getParameter( "mgntDivs" ) );
		String rgstIdnt = XssUtil.unscript( multipartRequest.getParameter( "rgstIdnt" ) );

		int totalRow = StringUtil.nullToZeroInt( multipartRequest.getParameter( "totalRow" ) );
		String[] countRow = XssUtil.unscriptArr( multipartRequest.getParameterValues( "countRow" ) );

		String[] selTiteValue = XssUtil.unscriptArr( multipartRequest.getParameterValues( "selTite" ) );

		String realFileName = XssUtil.unscript( multipartRequest.getParameter( "realFileName" ) );
		String filePath = XssUtil.unscript( multipartRequest.getParameter( "filePath" ) );
		String exe = realFileName.substring( realFileName.lastIndexOf( "." ) + 1, realFileName.length() );
		String value = "";

		String[] worksTitls = new String[totalRow];
		// List<AnucBord> excelData = new ArrayList<AnucBord>();
		try{

			if( exe.equals( "xlsx" ) ){// 엑셀 2007이상

				XSSFWorkbook xworkBook =
					new XSSFWorkbook( new FileInputStream( new File( filePath + "\\" + realFileName ) ) );

				XSSFSheet xsheet = null;
				XSSFRow xrow = null;
				XSSFCell xcell = null;

				xsheet = xworkBook.getSheetAt( 0 );// 첫 시트
				int rows = xsheet.getPhysicalNumberOfRows(); // 전체 행 갯수
				for( int i = 5, count = 0; i < rows; i++, count++ ){
					xrow = xsheet.getRow( i );
					int cells = xrow.getPhysicalNumberOfCells();// 한 행에 들어 있는 열 데이터 갯수
					AnucBord anucBord2 = new AnucBord();

					for( int j = 0; j < cells; j++ ){

						int cellCount = 0;
						xcell = xrow.getCell( j );

						switch( xcell.getCellType() ){
							case XSSFCell.CELL_TYPE_NUMERIC: // 숫자형 데이터
								value = "" + (int) xcell.getNumericCellValue();
								break;

							case XSSFCell.CELL_TYPE_STRING: // 문자형 데이터

								value = "" + xcell.getStringCellValue().trim();
								break;

							case XSSFCell.CELL_TYPE_BLANK:
								value = "";
								break;
							/*
							 * case XSSFCell.CELL_TYPE_FORMULA : // 수식형 데이터 break;
							 */
						}
						if( j == cellCount++ ){// 순번 셀 검사
							try{
								anucBord2.setInsertRow( Integer.parseInt( value ) );
							}
							catch( NumberFormatException ex ){
								System.err.println( "잘못된 입력입니다.숫자만 입력해주세요." );

							}

						}
						if( j == cellCount++ ){// 신청사유 CODE
							if( value.indexOf( "#" ) != -1 ){
								String rgstReas[] = value.split( "#" );// 기타일경우 # 뒤에 사유입력
								if( StringUtil.nullToZeroInt( rgstReas[0].replace( " ", "" ) ) == 6 ){
									anucBord2.setRgstReasCd( Integer.parseInt( rgstReas[0].replace( " ", "" ) ) );
									anucBord2.setRgstReasEtc( rgstReas[1] );
								}
							}else{
								anucBord2.setRgstReasCd( Integer.parseInt( value.replace( " ", "" ) ) );
							}

						}
						if( j == cellCount++ ){ // 저작물정보 장르CODE
							try{
								anucBord2.setGenreCd( Integer.parseInt( value.replace( " ", "" ) ) );
							}
							catch( NumberFormatException ex ){
								System.err.println( "잘못된 입력입니다.숫자만 입력해주세요." );

							}
						}
						if( j == cellCount++ ){ // 저작물정보 제호& 저작권자조회공고 정보 3.저작물의 제호
							anucBord2.setTite( value );
							anucBord2.setWorksTitle( value );
							worksTitls[i - 5] = value;

						}
						if( j == cellCount++ ){// 저작물정보 저작권자정보
							anucBord2.setCoptHodrCdAndName( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 취지
							anucBord2.setAnucItem1( value.replace( "\r\n", "<br>" ) );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 성명
							anucBord2.setAnucItem2( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 주소
							anucBord2.setAnucItem3( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 전화번호
							anucBord2.setAnucItem4( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 저작물의 제호

						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공표표시된 지적재산권자 성명
							anucBord2.setAnucItem5( value );

						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 저작물발행자
							anucBord2.setAnucItem6( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공표년월일
							anucBord2.setAnucItem7( value );
						}
						if( j == cellCount++ ){ // 저작물의 이용 목적
							anucBord2.setAnucItem8( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자 이름
							anucBord2.setAnucItem9( value );

						}
						if( j == cellCount++ ){// 저작권자조회공고 정보 공고자의 주소
							anucBord2.setAnucItem10( value );

						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자의 연락처
							anucBord2.setAnucItem11( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자의 담당자
							anucBord2.setAnucItem12( value );
						}
					}

					int worksId = AnucBordService.getWorksId();
					anucBord2.setWorksId( worksId );
					anucBord2.setRgstIdnt( rgstIdnt );
					anucBord2.setMgntDivs( mgntDivs );

					// File Upload

					Iterator fileNameIterator = multipartRequest.getFileNames();
					AnucBordFile anucBordFile = new AnucBordFile();
					List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();

					for( int j = 0; j < selTiteValue.length; j++ ){
						if( StringUtil.nullToZeroInt( countRow[count] ) == StringUtil.nullToZeroInt(
							selTiteValue[j] ) ){// 셀렉트박스에 부여한 엑셀 ROW 값
							if( fileNameIterator.hasNext() ){
								MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
								if( multiFile.getSize() > 0 ){
									anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
									fileList.add( anucBordFile );
								}
							}
						}else{
							if( fileNameIterator.hasNext() ){
								fileNameIterator.next();// 다음순서로 넘김
							}
						}
					}

					anucBord2.setFileList( fileList );
					result = AnucBordService.insertNonAnuc( anucBord2 );

					// 저작권자 및 실연자 정보
					List<AnucBord> coptHodrList = new ArrayList<AnucBord>();
					if( result == 1 ){
						String[] co = anucBord2.getCoptHodrCdAndName().split( "," );
						for( int a = 0; a < co.length; a++ ){
							AnucBord CoptHodr = new AnucBord();
							CoptHodr.setWorksId( worksId );

							String[] coNa = co[a].split( "#" );
							int coco = Integer.parseInt( coNa[0].replace( " ", "" ) );

							String coName = coNa[1];
							CoptHodr.setCoptHodrName( coName );
							CoptHodr.setCoptHodrRoleCd( coco );
							coptHodrList.add( CoptHodr );
							// AnucBordService.insertCopthodr(CoptHodr);
						}
					}

					/*
					 * for(int i=0;i<anucBordList.size();i++){
					 * AnucBordService.insertNonAnuc(anucBordList.get(i)); }
					 */
					for( int j = 0; j < coptHodrList.size(); j++ ){
						result = AnucBordService.insertCopthodr( coptHodrList.get( j ) );
					}

				}

			}else if( exe.equals( "xls" ) ){// 엑셀 2003버젼

				HSSFWorkbook hworkBook =
					new HSSFWorkbook( new FileInputStream( new File( filePath + "\\" + realFileName ) ) );

				HSSFSheet hsheet = null;
				HSSFRow hrow = null;
				HSSFCell hcell = null;

				hsheet = hworkBook.getSheetAt( 0 );// 첫 시트
				int rows = hsheet.getPhysicalNumberOfRows(); // 전체 행 갯수
				for( int i = 5, count = 0; i < rows; i++, count++ ){
					hrow = hsheet.getRow( i );
					int cells = hrow.getPhysicalNumberOfCells();// 한 행에 들어 있는 열 데이터 갯수
					AnucBord anucBord2 = new AnucBord();

					for( int j = 0; j < cells; j++ ){

						int cellCount = 0;
						hcell = hrow.getCell( j );

						switch( hcell.getCellType() ){
							case HSSFCell.CELL_TYPE_NUMERIC: // 숫자형 데이터
								value = "" + (int) hcell.getNumericCellValue();
								break;

							case HSSFCell.CELL_TYPE_STRING: // 문자형 데이터

								value = "" + hcell.getStringCellValue().trim();
								break;

							case HSSFCell.CELL_TYPE_BLANK:
								value = "";
								break;
							/*
							 * case XSSFCell.CELL_TYPE_FORMULA : // 수식형 데이터 break;
							 */
						}

						if( j == cellCount++ ){// 순번 셀 검사
							try{
								anucBord2.setInsertRow( Integer.parseInt( value ) );
							}
							catch( NumberFormatException ex ){
								System.err.println( "잘못된 입력입니다.숫자만 입력해주세요." );

							}

						}
						if( j == cellCount++ ){// 신청사유 CODE
							if( value.indexOf( "#" ) != -1 ){
								String rgstReas[] = value.split( "#" );// 기타일경우 # 뒤에 사유입력
								if( StringUtil.nullToZeroInt( rgstReas[0].replace( " ", "" ) ) == 6 ){
									anucBord2.setRgstReasCd( Integer.parseInt( rgstReas[0].replace( " ", "" ) ) );
									anucBord2.setRgstReasEtc( rgstReas[1] );
								}
							}else{
								anucBord2.setRgstReasCd( Integer.parseInt( value.replace( " ", "" ) ) );
							}

						}
						if( j == cellCount++ ){ // 저작물정보 장르CODE
							try{
								anucBord2.setGenreCd( Integer.parseInt( value.replace( " ", "" ) ) );
							}
							catch( NumberFormatException ex ){
								System.err.println( "잘못된 입력입니다.숫자만 입력해주세요." );

							}
						}
						if( j == cellCount++ ){ // 저작물정보 제호& 저작권자조회공고 정보 3.저작물의 제호
							anucBord2.setTite( value );
							anucBord2.setWorksTitle( value );
							worksTitls[i - 5] = value;

						}
						if( j == cellCount++ ){// 저작물정보 저작권자정보
							anucBord2.setCoptHodrCdAndName( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 취지
							anucBord2.setAnucItem1( value.replace( "\r\n", "<br>" ) );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 성명
							anucBord2.setAnucItem2( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 주소
							anucBord2.setAnucItem3( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 재산권자 전화번호
							anucBord2.setAnucItem4( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 저작물의 제호

						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공표표시된 지적재산권자 성명
							anucBord2.setAnucItem5( value );

						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 저작물발행자
							anucBord2.setAnucItem6( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공표년월일
							anucBord2.setAnucItem7( value );
						}
						if( j == cellCount++ ){ // 저작물의 이용 목적
							anucBord2.setAnucItem8( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자 이름
							anucBord2.setAnucItem9( value );

						}
						if( j == cellCount++ ){// 저작권자조회공고 정보 공고자의 주소
							anucBord2.setAnucItem10( value );

						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자의 연락처
							anucBord2.setAnucItem11( value );
						}
						if( j == cellCount++ ){ // 저작권자조회공고 정보 공고자의 담당자
							anucBord2.setAnucItem12( value );
						}
					}

					int worksId = AnucBordService.getWorksId();
					// AnucBord CoptHodr = new AnucBord();
					anucBord2.setWorksId( worksId );
					anucBord2.setRgstIdnt( rgstIdnt );
					anucBord2.setMgntDivs( mgntDivs );

					// File Upload

					Iterator fileNameIterator = multipartRequest.getFileNames();
					AnucBordFile anucBordFile = new AnucBordFile();
					List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();

					for( int j = 0; j < selTiteValue.length; j++ ){
						if( StringUtil.nullToZeroInt( countRow[count] ) == StringUtil.nullToZeroInt(
							selTiteValue[j] ) ){// 셀렉트박스에 부여한 엑셀 ROW 값
							if( fileNameIterator.hasNext() ){
								MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
								if( multiFile.getSize() > 0 ){
									anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
									fileList.add( anucBordFile );
								}
							}
						}else{
							if( fileNameIterator.hasNext() ){
								fileNameIterator.next();// 다음순서로 넘김
							}
						}
					}

					anucBord2.setFileList( fileList );
					// anucBordList.add(anucBord);
					result = AnucBordService.insertNonAnuc( anucBord2 );

					// 저작권자 및 실연자 정보
					List<AnucBord> coptHodrList = new ArrayList<AnucBord>();
					if( result == 1 ){
						String[] co = anucBord2.getCoptHodrCdAndName().split( "," );
						for( int a = 0; a < co.length; a++ ){
							AnucBord CoptHodr = new AnucBord();
							CoptHodr.setWorksId( worksId );

							String[] coNa = co[a].split( "#" );
							int coco = Integer.parseInt( coNa[0].replace( " ", "" ) );

							String coName = coNa[1];
							CoptHodr.setCoptHodrName( coName );
							CoptHodr.setCoptHodrRoleCd( coco );
							coptHodrList.add( CoptHodr );
							// AnucBordService.insertCopthodr(CoptHodr);
						}
					}

					/*
					 * for(int i=0;i<anucBordList.size();i++){
					 * AnucBordService.insertNonAnuc(anucBordList.get(i)); }
					 */
					for( int j = 0; j < coptHodrList.size(); j++ ){
						// System.out.println(coptHodrList.get(j).getCoptHodrName());
						result = AnucBordService.insertCopthodr( coptHodrList.get( j ) );
					}

				}

			}
		}
		catch( Exception e ){
			e.printStackTrace();
		}

		if( result == 1 ){
			User userInfo = SessionUtil.getSession( request );

			HashMap userMap = new HashMap();
			// 신청제목
			String workstitl = worksTitls[0] + " 외 " + ( totalRow - 1 ) + "건";
			userMap.put( "WORKS_TITL", workstitl );

			// 사용자 정보 셋팅
			userMap.put( "USER_IDNT", userInfo.getUserIdnt() );
			userMap.put( "USER_NAME", userInfo.getUserName() );
			userMap.put( "U_MAIL", userInfo.getMail() );
			userMap.put( "MOBL_PHON", userInfo.getMoblPhon() );
			userMap.put( "MGNT_DIVS", "BO06" );
			userMap.put( "MAIL_TITL", "저작권자 찾기위한 상당한 노력 " );
			userMap.put( "MAIL_TITL_DIVS", "다건 신청" );

			List list = new ArrayList();

			list = commonService.getMgntMail( userMap );

			int listSize = list.size();

			String to[] = new String[listSize];
			String toName[] = new String[listSize];

			for( int i = 0; i < listSize; i++ ){
				HashMap map = (HashMap) list.get( i );
				toName[i] = (String) map.get( "USER_NAME" );
				to[i] = (String) map.get( "MAIL" );
			}

			userMap.put( "TO", to );
			userMap.put( "TO_NAME", toName );

			SendMail.send( userMap );

		}

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo06List" );
		mav.addObject( "Success", 2 );
		return mav;
	}

	// 등록(거소불명)
	@RequestMapping( value = "/statBord/statBo06Write.do", method = RequestMethod.POST )
	public ModelAndView statBo06Write( @ModelAttribute AnucBord anucBord,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// System.out.println( "/statBord/statBo06Write.do call" );
		// System.out.println( "###0" );
		// 파일첨부
		AnucBord CoptHodr = new AnucBord();
		int worksId = AnucBordService.getWorksId();
		// System.out.println( worksId );
		AnucBordFile anucBordFile = new AnucBordFile();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );
		// System.out.println( "###1" );
		anucBord.setWorksId( worksId );
		anucBord.setRgstIdnt( multipartRequest.getParameter( "rgstIdnt" ) );
		anucBord.setWorksTitle( XssUtil.unscript( multipartRequest.getParameter( "worksTitle" ) ) );
		anucBord.setGenreCd( StringUtil.nullToZero( multipartRequest.getParameter( "genreCd" ) ) );
		String anucItem1 = multipartRequest.getParameter( "anucItem1" );
		anucItem1 = anucItem1.replace( "\r\n", "<br>" );
		anucBord.setAnucItem1( XssUtil.unscript( anucItem1 ) );
		anucBord.setAnucItem2( XssUtil.unscript( multipartRequest.getParameter( "anucItem2" ) ) );
		anucBord.setAnucItem3( XssUtil.unscript( multipartRequest.getParameter( "anucItem3" ) ) );
		anucBord.setAnucItem4( XssUtil.unscript( multipartRequest.getParameter( "anucItem4" ) ) );
		anucBord.setAnucItem5( XssUtil.unscript( multipartRequest.getParameter( "anucItem5" ) ) );
		anucBord.setAnucItem6( XssUtil.unscript( multipartRequest.getParameter( "anucItem6" ) ) );
		anucBord.setAnucItem7( XssUtil.unscript( multipartRequest.getParameter( "anucItem7" ) ) );
		anucBord.setAnucItem8( XssUtil.unscript( multipartRequest.getParameter( "anucItem8" ) ) );
		anucBord.setAnucItem9( XssUtil.unscript( multipartRequest.getParameter( "anucItem9" ) ) );
		anucBord.setAnucItem10( XssUtil.unscript( multipartRequest.getParameter( "anucItem10" ) ) );
		anucBord.setAnucItem11( XssUtil.unscript( multipartRequest.getParameter( "anucItem11" ) ) );
		anucBord.setAnucItem12( XssUtil.unscript( multipartRequest.getParameter( "anucItem12" ) ) );
		anucBord.setMgntDivs( multipartRequest.getParameter( "mgntDivs" ) );
		anucBord.setRgstReasCd( StringUtil.nullToZeroInt( multipartRequest.getParameter( "rgstReasCd" ) ) );
		if( StringUtil.nullToZeroInt( multipartRequest.getParameter( "rgstReasCd" ) ) == 6 ){
			anucBord.setRgstReasEtc( XssUtil.unscript( multipartRequest.getParameter( "rgstReasEtc" ) ) );
		}
		CoptHodr.setWorksId( worksId );
		CoptHodr.setCoptHodrsLength( StringUtil.nullToZero( multipartRequest.getParameter( "coptHodrsLength" ) ) );
		String coptHodrNm = ( XssUtil.unscript( multipartRequest.getParameter( "coptHodrName" ) ) );
		String coptHodrRole = multipartRequest.getParameter( "coptHodrRoleCd" );
		// System.out.println( "###2" );
		anucBordFile.setFileName( multipartRequest.getParameter( "fileName" ) );
		anucBordFile.setFilePath( multipartRequest.getParameter( "filePath" ) );
		anucBordFile.setFileSize( multipartRequest.getParameter( "fileSize" ) );
		// File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();

		while( fileNameIterator.hasNext() ){
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
				fileList.add( anucBordFile );
			}
		}
		// 저작권자 및 실연자 정보
		anucBord.setFileList( fileList );
		AnucBordService.insertNonAnuc( anucBord );
		if( CoptHodr.getCoptHodrsLength() != 0 ){
			for( int i = 0; i < CoptHodr.getCoptHodrsLength(); i++ ){
				String[] co = coptHodrRole.split( "," ); // CoptHodrRoleCd
				int coco = Integer.parseInt( co[i] ); // 스트링으로 받아온값을
														// 인트로 형변환
				String[] coNa = coptHodrNm.split( "," ); // CoptHodrName
				String coName = coNa[i];

				CoptHodr.setCoptHodrName( coName );
				CoptHodr.setCoptHodrRoleCd( coco );
				AnucBordService.insertCopthodr( CoptHodr );
			}
		}
		User userInfo = SessionUtil.getSession( request );
		// System.out.println( "###3" );
		HashMap userMap = new HashMap();

		// 신청제목
		userMap.put( "WORKS_TITL", anucBord.getWorksTitle() );

		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyy.MM.dd HH:mm:ss", Locale.KOREA );
		Date currentTime = new Date();
		String mTime = mSimpleDateFormat.format( currentTime );

		String[] commNames = { "유통지원팀" };
		String[] date = { mTime };
		String[] genreList = {
				"어문저작물",
				"음악저작물",
				"연극저작물",
				"미술저작물",
				"건축저작물:",
				"사진저작물",
				"영상저작물",
				"도형저작물",
				"컴퓨터프로그램저작물",
				"편집저작물",
				"2차적 저작물",
				"음반",
				"실연",
				"기타" };
		String[] genreArray = new String[1];
		String[] sumAllCount = { "1" };
		// 사용자 정보 셋팅

		if( anucBord.getGenreCd() == 99 ){
			genreArray[0] = genreList[13];
		}else{
			genreArray[0] = genreList[(int) anucBord.getGenreCd() - 1];
		}

		userMap.put( "SUM_ALL_COUNT", sumAllCount );
		userMap.put( "GENRE", genreArray );
		userMap.put( "COMM_NAME", commNames );
		userMap.put( "RGST_DTTM", date );
		userMap.put( "USER_IDNT", userInfo.getUserIdnt() );
		userMap.put( "USER_NAME", userInfo.getUserName() );
		userMap.put( "U_MAIL", userInfo.getMail() );
		userMap.put( "MOBL_PHON", userInfo.getMoblPhon() );
		// userMap.put("MGNT_DIVS", "BO06");
		// 메일 수신자 수정 cpmadmin
		userMap.put( "MGNT_DIVS", "REPORT_WORKS" );

		userMap.put( "MAIL_TITL", "저작권자 찾기위한 상당한 노력 " );
		userMap.put( "MAIL_TITL_DIVS", "신청" );

		List list = new ArrayList();

		list = commonService.getMgntMail( userMap );
		// System.out.println( "###4" );
		int listSize = list.size();

		String to[] = new String[listSize];
		String toName[] = new String[listSize];

		for( int i = 0; i < listSize; i++ ){
			HashMap map = (HashMap) list.get( i );
			toName[i] = (String) map.get( "USER_NAME" );
			to[i] = (String) map.get( "MAIL" );
		}

		userMap.put( "TO", to );
		userMap.put( "TO_NAME", toName );

		SendMail.send( userMap );
		// System.out.println( "###5" );
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo06List" );
		mav.addObject( "Success", 1 );
		return mav;
	}

	// 거소불명 상세보기+이의제기
	@RequestMapping( method = RequestMethod.GET, value = "/statBord/statBo06Detl.do" )
	public ModelAndView statBo06Detl( @RequestParam( value = "worksId", required = true ) int worksId,
		@RequestParam( value = "mode", required = false ) String mode,
		@RequestParam( value = "report", required = false ) String report,
		@RequestParam( value = "modal", required = false, defaultValue = "0" ) String modal,
		HttpServletRequest request ) throws Exception{

		AnucBord coptHodr = new AnucBord();
		List<AnucBord> list2 = AnucBordService.detailNonAnuc( worksId ); // 게시글 상세보기 게시글 내용
		List<AnucBordFile> list4 = AnucBordService.fileSelectNonAnuc( worksId ); // 게시판 파일
		List<SrchComm> commList = new ArrayList<SrchComm>();
		List<SrchCros> crosList = new ArrayList<SrchCros>();
		List<SrchStat> confList = new ArrayList<SrchStat>();
		List<AnucBordSupl> SuplList = AnucBordService.selectWorksSuplItemList( worksId );// 보완내역 조회

		User user = SessionUtil.getSession( request );
		if( !"R".equals( mode ) ){
			String sessUserIdnt = user.getUserIdnt();

			if( sessUserIdnt == null ){
				return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
			}
		}

		AnucBord anucBord = list2.get( 0 );
		int gubunVal = (int) anucBord.getGenreCd();
		int gubunCd = 0;
		if( gubunVal == 1 ){
			gubunCd = 62;
		}else if( gubunVal == 2 ){
			gubunCd = 63;
		}else if( gubunVal == 3 ){
			gubunCd = 64;
		}else if( gubunVal == 4 ){
			gubunCd = 65;
		}else if( gubunVal == 5 ){
			gubunCd = 66;
		}else if( gubunVal == 6 ){
			gubunCd = 67;
		}else if( gubunVal == 7 ){
			gubunCd = 68;
		}else if( gubunVal == 8 ){
			gubunCd = 69;
		}else gubunCd = 0;
		coptHodr.setWorksId( worksId );
		coptHodr.setBigCode( gubunCd );
		List<AnucBord> list3 = AnucBordService.detailNonCopthodr( coptHodr );
		ModelAndView mav = new ModelAndView();

		for( int i = 0; i < list2.size(); i++ ){
			if( list2.get( i ).getStatWorksYn().equals( "N" ) && list2.get( i ).getSystEffortStatCd() == 4
				&& list2.get( i ).getStatRsltCd().equals( "1" ) ){
				List<AnucBord> worksList = AnucBordService.NonResult( worksId );
				if( worksList.get( i ).getMappWorksDivsCd() == 2 ){
					HashMap<Object, Object> params = new HashMap<Object, Object>();
					params.put( "worksId", worksList.get( i ).getMappWorksId() );
					params.put( "genreCd", worksList.get( i ).getGenreCd() );
					commList = srchService.findCommDetl( params );
					mav.addObject( "comm", commList );
					mav.addObject( "commSize", commList.size() );
				}else if( worksList.get( i ).getMappWorksDivsCd() == 1 ){
					HashMap<Object, Object> params = new HashMap<Object, Object>();
					params.put( "contTitle", worksList.get( i ).getWorksTitle() );
					params.put( "genreCd", worksList.get( i ).getGenreCd() );
					crosList = srchService.findCrosWorks( params );
					mav.addObject( "cros", crosList );
					mav.addObject( "crosSize", crosList.size() );
				}else{
					HashMap<Object, Object> params = new HashMap<Object, Object>();
					params.put( "worksId", worksList.get( i ).getMappWorksId() );
					params.put( "worksDivsCd", 6 );
					confList = srchService.findStatDetl( params );
					mav.addObject( "conf", confList );
					mav.addObject( "confSize", confList.size() );
				}
			}
		}
		mav.setViewName( "/statBord/statBo06Detl" );
		if( "R".equals( mode ) ){
			mav = new ModelAndView( report );
			mav.addObject( "modal", modal );
		}
		mav.addObject( "AnucBord", anucBord );
		mav.addObject( "fileList", list4 );
		mav.addObject( "coptHodr", list3 );
		mav.addObject( "SuplList", SuplList );

		return mav;
	}

	// 거소불명 수정 폼
	@RequestMapping( "/statBord/statBo06Update.do" )
	public ModelAndView statBo06Update( @RequestParam( value = "worksId", required = true ) int worksId,
		HttpServletRequest request ) throws Exception{
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();

		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}
		List<AnucBord> list2 = AnucBordService.detailNonAnuc( worksId ); // 게시글 상세보기 게시글 내용
		List<AnucBordFile> list4 = AnucBordService.fileSelectNonAnuc( worksId ); // 게시판 파일
		AnucBord anucBord = new AnucBord();
		AnucBord form = list2.get( 0 );
		String anucItem1 = form.getAnucItem1();
		anucItem1 = anucItem1.replace( "<br>", "\r\n" );
		form.setAnucItem1( anucItem1 );
		int listSize = list4.size();
		int bigCode = 35;
		int gubunVal = (int) form.getGenreCd();
		int gubunCd = 0;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", bigCode );
		if( gubunVal == 1 ){
			gubunCd = 62;
		}else if( gubunVal == 2 ){
			gubunCd = 63;
		}else if( gubunVal == 3 ){
			gubunCd = 64;
		}else if( gubunVal == 4 ){
			gubunCd = 65;
		}else if( gubunVal == 5 ){
			gubunCd = 66;
		}else if( gubunVal == 6 ){
			gubunCd = 67;
		}else if( gubunVal == 7 ){
			gubunCd = 68;
		}else if( gubunVal == 8 ){
			gubunCd = 69;
		}else gubunCd = 0;
		anucBord.setWorksId( worksId );
		anucBord.setBigCode( gubunCd );
		List<AnucBord> list3 = AnucBordService.detailNonCopthodr( anucBord ); // 저작자 목록
		List<Code> ListCode = AnucBordService.getCodeList( params ); // 코드리스트
		Map<Object, Object> param = new HashMap<Object, Object>();
		param.put( "bigCode", gubunCd );
		List<Code> ListCopt = AnucBordService.getCodeList( param ); // 코드리스트
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo06Update" );
		mav.addObject( "AnucBord", form );
		mav.addObject( "fileList", list4 );
		mav.addObject( "listSize", listSize );
		mav.addObject( "codeList", ListCode );
		mav.addObject( "coptHodr", list3 );
		mav.addObject( "coptSize", list3.size() );
		mav.addObject( "ListCopt", ListCopt );
		return mav;
	}

	// 거소불명 수정
	@SuppressWarnings( "finally" )
	@RequestMapping( "/statBord/statBo06UpdateProc.do" )
	public ModelAndView statBo06UpdateProc( HttpServletRequest request,
		@RequestParam( value = "worksId", required = true ) int worksId ) throws Exception{
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		// 파일첨부
		AnucBordFile anucBordFile = new AnucBordFile();
		AnucBord anucBord = new AnucBord();
		AnucBord CoptHodr = new AnucBord();
		multipartRequest = multipartResolver.resolveMultipart( request );
		anucBord.setRgstIdnt( multipartRequest.getParameter( "rgstIdnt" ) );
		anucBord.setWorksTitle( XssUtil.unscript( multipartRequest.getParameter( "worksTitle" ) ) );
		anucBord.setWorksId( worksId );
		anucBord.setGenreCd( StringUtil.nullToZeroInt( multipartRequest.getParameter( "genreCd" ) ) );
		anucBord.setAnucItem1( XssUtil.unscript( multipartRequest.getParameter( "anucItem1" ) ) );
		anucBord.setAnucItem2( XssUtil.unscript( multipartRequest.getParameter( "anucItem2" ) ) );
		anucBord.setAnucItem3( XssUtil.unscript( multipartRequest.getParameter( "anucItem3" ) ) );
		anucBord.setAnucItem4( XssUtil.unscript( multipartRequest.getParameter( "anucItem4" ) ) );
		anucBord.setAnucItem5( XssUtil.unscript( multipartRequest.getParameter( "anucItem5" ) ) );
		anucBord.setAnucItem6( XssUtil.unscript( multipartRequest.getParameter( "anucItem6" ) ) );
		anucBord.setAnucItem7( multipartRequest.getParameter( "anucItem7" ) );
		anucBord.setAnucItem8( XssUtil.unscript( multipartRequest.getParameter( "anucItem8" ) ) );
		anucBord.setAnucItem9( XssUtil.unscript( multipartRequest.getParameter( "anucItem9" ) ) );
		anucBord.setAnucItem10( XssUtil.unscript( multipartRequest.getParameter( "anucItem10" ) ) );
		anucBord.setAnucItem11( XssUtil.unscript( multipartRequest.getParameter( "anucItem11" ) ) );
		anucBord.setAnucItem12( XssUtil.unscript( multipartRequest.getParameter( "anucItem12" ) ) );
		anucBord.setAnucItem13( multipartRequest.getParameter( "anucItem13" ) );
		anucBord.setAnucItem14( multipartRequest.getParameter( "anucItem14" ) );
		anucBord.setAnucItem15( multipartRequest.getParameter( "anucItem15" ) );
		anucBord.setRgstReasCd( StringUtil.nullToZeroInt( multipartRequest.getParameter( "rgstReasCd" ) ) );
		if( anucBord.getRgstReasCd() == 6 ){
			anucBord.setRgstReasEtc( XssUtil.unscript( multipartRequest.getParameter( "rgstReasEtc" ) ) );
		}

		CoptHodr.setWorksId( worksId );
		CoptHodr.setCoptHodrsLength( StringUtil.nullToZero( multipartRequest.getParameter( "coptHodrsLength" ) ) );
		String coptHodrNm = ( XssUtil.unscript( multipartRequest.getParameter( "coptHodrName" ) ) );
		String coptHodrRole = multipartRequest.getParameter( "coptHodrRoleCd" );
		anucBordFile.setFileName( multipartRequest.getParameter( "fileName" ) );
		anucBordFile.setFilePath( multipartRequest.getParameter( "filePath" ) );
		anucBordFile.setFileSize( multipartRequest.getParameter( "fileSize" ) );

		String fileCheck = multipartRequest.getParameter( "fileCheck" );
		int fileLength = ( StringUtil.nullToZeroInt( multipartRequest.getParameter( "fileLength" ) ) );
		// File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();

		while( fileNameIterator.hasNext() ){
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
				fileList.add( anucBordFile );
			}
		}
		anucBord.setFileList( fileList );

		AnucBordService.updateNonAnuc( anucBord );
		AnucBordService.deletecoptHodr( worksId );
		String[] co = coptHodrRole.split( "," ); // CoptHodrRoleCd
		String[] coNa = coptHodrNm.split( "," ); // CoptHodrName
		for( int i = 0; i < CoptHodr.getCoptHodrsLength(); i++ ){
			int coco = Integer.parseInt( co[i] ); // 스트링으로 받아온값을
			String coName = coNa[i];
			CoptHodr.setCoptHodrName( coName );
			CoptHodr.setCoptHodrRoleCd( coco );
			AnucBordService.insertCopthodr( CoptHodr );
		}
		if( fileLength > 0 ){
			String[] fileCk = fileCheck.split( "," );
			for( int i = 0; i < fileCk.length; i++ ){
				int attcSeqn = Integer.parseInt( fileCk[i] );
				AnucBordService.deleteNonFile( attcSeqn );
			}
		}
		List<AnucBord> list2 = AnucBordService.detailNonAnuc( worksId ); // 게시글 상세보기 게시글 내용
		List<AnucBordFile> list4 = AnucBordService.fileSelectNonAnuc( worksId ); // 게시판 파일
		AnucBord form = list2.get( 0 );

		int gubunVal = (int) form.getGenreCd();
		int gubunCd = 0;
		if( gubunVal == 1 ){
			gubunCd = 62;
		}else if( gubunVal == 2 ){
			gubunCd = 63;
		}else if( gubunVal == 3 ){
			gubunCd = 64;
		}else if( gubunVal == 4 ){
			gubunCd = 65;
		}else if( gubunVal == 5 ){
			gubunCd = 66;
		}else if( gubunVal == 6 ){
			gubunCd = 67;
		}else if( gubunVal == 7 ){
			gubunCd = 68;
		}else if( gubunVal == 8 ){
			gubunCd = 69;
		}else gubunCd = 0;
		anucBord.setWorksId( worksId );
		anucBord.setBigCode( gubunCd );
		List<AnucBord> list3 = AnucBordService.detailNonCopthodr( anucBord ); // 저작자 목록

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo06Detl" );
		mav.addObject( "AnucBord", form );
		mav.addObject( "fileList", list4 );
		mav.addObject( "coptHodr", list3 );
		return mav;
	}

	@RequestMapping( "/statBord/statBo06Delete.do" )
	public ModelAndView statBo06Delete( @RequestParam( value = "worksId", required = true ) int worksId,
		@RequestParam( value = "bordCd", required = true ) int bordCd,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		AnucBordService.deleteNonAnuc( worksId );
		AnucBord srchParam = new AnucBord();
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();

		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}
		int totalRow = AnucBordService.countNonAnuc( sessUserIdnt );
		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setRgstIdnt( sessUserIdnt );
		if( bordCd != 1 ){
			list = AnucBordService.selectNonAnuc( srchParam );
		}else{
			list = AnucBordService.selectAnuc( srchParam );
		}
		Date date = new Date();
		SimpleDateFormat yearForm = new SimpleDateFormat( "yyyy" );
		SimpleDateFormat dayForm = new SimpleDateFormat( "yyyyMMdd" );
		String thisMonth = yearForm.format( date ) + "0101";
		String thisDay = dayForm.format( date );

		int genreCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", genreCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );

		int gubunCode = 59;
		Map<Object, Object> param = new HashMap<Object, Object>();
		param.put( "bigCode", gubunCode );
		List<Code> gubunCd = AnucBordService.getCodeList( param );
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo06List" );
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "codeList", ListCode );
		mav.addObject( "gubunList", gubunCd );
		mav.addObject( "defaultJoinDay", thisMonth );
		mav.addObject( "defaultEndDay", thisDay );

		return mav;
	}

	// ---------------------- 법정허락 공고 ------------------------------

	@RequestMapping( method = RequestMethod.GET, value = "/statBord/courtFind.do" )
	public ModelAndView courtFind( HttpServletRequest request,
		HttpServletResponse respone,
		@RequestParam( value = "bordCd", required = true ) int bordCd,
		@RequestParam( value = "tite", required = true ) String tite
	// @RequestParam(value="genreCd", required = true)int genreCd
	) throws Exception{

		AnucBord srchParam = new AnucBord();

		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		String openYn = "Y";
		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		// srchParam.setGenreCd(genreCd);
		srchParam.setTite( tite );
		srchParam.setBordCd( bordCd );
		srchParam.setOpenYn( openYn );
		String title = tite;
		// int genCd = genreCd;
		list = AnucBordService.findAnuc( srchParam );
		int bordNo = (int) bordCd;
		int totalRow = AnucBordService.findCount( srchParam );
		ModelAndView mav = new ModelAndView();
		if( bordCd == 3 ){
			mav.setViewName( "/statBord/statBo03List" );
		}else if( bordCd == 4 ){
			mav.setViewName( "/statBord/statBo04List" );
		}else{
			mav.setViewName( "/statBord/statBo05List" );
		}
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "title", title );
		// mav.addObject("genreCd", genCd);
		mav.addObject( "bordCd", bordNo );
		return mav;
	}

	@RequestMapping( "/statBord/statBo03List.do" )
	public ModelAndView statBo03List( @RequestParam( value = "bordCd", required = true ) int bordCd,
		HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{
		AnucBord srchParam = new AnucBord();

		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		String openYn = "Y";
		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setBordCd( bordCd );
		srchParam.setOpenYn( openYn );
		list = AnucBordService.selectAnuc( srchParam );
		int totalRow = AnucBordService.countAnuc( srchParam );
		int bordNo = (int) bordCd;
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo03List" );
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "bordCd", bordNo );
		return mav;
	}

	@RequestMapping( "/statBord/statBo04List.do" )
	public ModelAndView statBo04List( @RequestParam( value = "bordCd", required = true ) int bordCd,
		HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		AnucBord srchParam = new AnucBord();

		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		String openYn = "Y";
		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setBordCd( bordCd );
		srchParam.setOpenYn( openYn );
		list = AnucBordService.selectAnuc( srchParam );
		int totalRow = AnucBordService.countAnuc( srchParam );
		int bordNo = (int) bordCd;
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo04List" );
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "bordCd", bordNo );
		return mav;
	}

	@RequestMapping( "/statBord/statBo05List.do" )
	public ModelAndView statBo05List( @RequestParam( value = "bordCd", required = true ) int bordCd,
		HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{
		AnucBord srchParam = new AnucBord();

		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		String openYn = "Y";
		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setBordCd( bordCd );
		srchParam.setOpenYn( openYn );
		list = AnucBordService.selectAnuc( srchParam );
		int totalRow = AnucBordService.countAnuc( srchParam );
		int bordNo = (int) bordCd;
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo05List" );
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "bordCd", bordNo );
		return mav;
	}

	@RequestMapping( value = "/statBord/test.do", method = RequestMethod.POST )
	public ModelAndView statBo01WriteMytest( @ModelAttribute AnucBord anucBord,
		HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		// getFileNames를 통해 구하고자 하는 파일 이름들을 구하면 Iterator 형태로 넘어온 file들의 이름들이 리턴된다.
		Iterator fileNameIter = multipartRequest.getFileNames();

		// Iterator 형태로 추출된 파일들의 이름을 키값으로 하여, while문을 돌면서 넘어온 파일들의 정보를 추출한다.
		while( fileNameIter.hasNext() ){
			System.out.println( "Strfin" + fileNameIter.hasNext() );
			String key = (String) fileNameIter.next();
			System.out.println( key + "key" );
			MultipartFile file = multipartRequest.getFile( key );
			System.out.println( "file" + file.getName() );
			System.out.println( "file" + file.getOriginalFilename() );
			System.out.println( "file" + file.getSize() );
		}

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo01ListMy" );
		mav.addObject( "Success", 1 );
		return mav;
	}

	// 마이페이지 시작------------------
	@RequestMapping( value = "/statBord/statBo01WriteMy.do", method = RequestMethod.POST )
	public ModelAndView statBo01WriteMy( @ModelAttribute AnucBord anucBord,
		HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{
		// 파일첨부
		AnucBordFile anucBordFile = new AnucBordFile();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

		// multipartRequest = multipartResolver.resolveMultipart(request);
		anucBord.setBordCd( StringUtil.nullToZero( multipartRequest.getParameter( "bordCd" ) ) );
		anucBord.setBordSeqn( AnucBordService.getBordSeqn() );
		anucBord.setRgstIdnt( multipartRequest.getParameter( "rgstIdnt" ) );
		// anucBord.setModiIdnt(multipartRequest.getParameter("rgstIdnt"));
		anucBord.setTite( XssUtil.unscript( multipartRequest.getParameter( "tite" ) ) );
		String bordDesc = multipartRequest.getParameter( "bordDesc" );
		if( bordDesc != null ){
			bordDesc = bordDesc.replace( "\r\n", "<br>" );
			anucBord.setBordDesc( XssUtil.unscript( bordDesc ) );
		}
		anucBord.setDivsCd( StringUtil.nullToZero( multipartRequest.getParameter( "divsCd" ) ) );
		anucBord.setGenreCd( StringUtil.nullToZero( multipartRequest.getParameter( "genreCd" ) ) );
		anucBord.setAnucItem1( XssUtil.unscript( multipartRequest.getParameter( "anucItem1" ) ) );
		anucBord.setAnucItem2( XssUtil.unscript( multipartRequest.getParameter( "anucItem2" ) ) );
		anucBord.setAnucItem3( XssUtil.unscript( multipartRequest.getParameter( "anucItem3" ) ) );
		anucBord.setAnucItem4( XssUtil.unscript( multipartRequest.getParameter( "anucItem4" ) ) );
		anucBord.setAnucItem5( XssUtil.unscript( multipartRequest.getParameter( "anucItem5" ) ) );
		anucBord.setAnucItem6( XssUtil.unscript( multipartRequest.getParameter( "anucItem6" ) ) );
		anucBord.setAnucItem7( XssUtil.unscript( multipartRequest.getParameter( "anucItem7" ) ) );
		anucBord.setAnucItem8( XssUtil.unscript( multipartRequest.getParameter( "anucItem8" ) ) );
		anucBord.setAnucItem9( XssUtil.unscript( multipartRequest.getParameter( "anucItem9" ) ) );
		anucBord.setAnucItem10( XssUtil.unscript( multipartRequest.getParameter( "anucItem10" ) ) );
		anucBord.setAnucItem11( XssUtil.unscript( multipartRequest.getParameter( "anucItem11" ) ) );
		anucBord.setAnucItem12( XssUtil.unscript( multipartRequest.getParameter( "anucItem12" ) ) );
		anucBord.setAnucItem13( multipartRequest.getParameter( "anucItem13" ) );
		anucBord.setAnucItem14( multipartRequest.getParameter( "anucItem14" ) );
		anucBord.setAnucItem15( multipartRequest.getParameter( "anucItem15" ) );
		anucBord.setMgntDivs( multipartRequest.getParameter( "mgntDivs" ) );

		anucBordFile.setFileName( multipartRequest.getParameter( "fileName" ) );
		anucBordFile.setFilePath( multipartRequest.getParameter( "filePath" ) );
		anucBordFile.setFileSize( multipartRequest.getParameter( "fileSize" ) );

		// File Upload

		System.out.println( "FILE STATRT" );
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();

		while( fileNameIterator.hasNext() ){
			String key = (String) fileNameIterator.next();
			MultipartFile multiFile = multipartRequest.getFile( key );
			System.out.println( "UPLOAD" );
			if( multiFile.getSize() > 0 ){
				anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
				fileList.add( anucBordFile );
			}
		}
		anucBord.setFileList( fileList );
		AnucBordService.insertAnuc( anucBord );

		User userInfo = SessionUtil.getSession( request );

		HashMap userMap = new HashMap();

		// 신청제목
		userMap.put( "WORKS_TITL", anucBord.getTite() );

		// 사용자 정보 셋팅
		userMap.put( "USER_IDNT", userInfo.getUserIdnt() );
		userMap.put( "USER_NAME", userInfo.getUserName() );
		userMap.put( "U_MAIL", userInfo.getMail() );
		userMap.put( "MOBL_PHON", userInfo.getMoblPhon() );

		if( anucBord.getMgntDivs().equals( "BO01" ) ){
			userMap.put( "MGNT_DIVS", "BO01" );
			userMap.put( "MAIL_TITL", "저작권자 조회공고 " );
			userMap.put( "MAIL_TITL_DIVS", "등록" );
		}else if( anucBord.getMgntDivs().equals( "BO05" ) ){
			userMap.put( "MGNT_DIVS", "BO05" );
			userMap.put( "MAIL_TITL", "보상금 공탁공고 " );
			userMap.put( "MAIL_TITL_DIVS", "신청" );
		}

		List list = new ArrayList();

		list = commonService.getMgntMail( userMap );

		int listSize = list.size();

		String to[] = new String[listSize];
		String toName[] = new String[listSize];

		for( int i = 0; i < listSize; i++ ){
			HashMap map = (HashMap) list.get( i );
			toName[i] = (String) map.get( "USER_NAME" );
			to[i] = (String) map.get( "MAIL" );
		}

		userMap.put( "TO", to );
		userMap.put( "TO_NAME", toName );

		// SendMail.send(userMap);

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo01ListMy" );
		mav.addObject( "Success", 1 );
		return mav;
	}

	// 마이페이지 상당한 노력 리스트
	@RequestMapping( "/statBord/statBo01ListMy.do" )
	public ModelAndView statBo01ListMy( @RequestParam( value = "bordCd", required = true ) int bordCd,
		HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();

		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}
		AnucBord srchParam = new AnucBord();

		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;

		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setBordCd( bordCd );
		srchParam.setRgstIdnt( sessUserIdnt );

		int totalRow = AnucBordService.countAnuc( srchParam );
		list = AnucBordService.selectAnuc( srchParam );
		for( int i = 0; i < list.size(); i++ ){
			int count = AnucBordService.countAnucObjc( (int) list.get( i ).getBordSeqn() );
			list.get( i ).setObjcCount( count );
		}
		int bordNo = (int) bordCd;

		int genreCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", genreCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );

		Date date = new Date();
		SimpleDateFormat yearForm = new SimpleDateFormat( "yyyy" );
		SimpleDateFormat dayForm = new SimpleDateFormat( "yyyyMMdd" );
		String thisMonth = yearForm.format( date ) + "0101";
		String thisDay = dayForm.format( date );

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo01ListMy" );
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "bordCd", bordNo );
		mav.addObject( "defaultJoinDay", thisMonth );
		mav.addObject( "defaultEndDay", thisDay );
		mav.addObject( "codeList", ListCode );
		return mav;
	}

	// 글삭제 (공고게시판 논리삭제)
	@RequestMapping( "/statBord/statBo01Delete.do" )
	public ModelAndView statBo01Delete( @RequestParam( value = "bordSeqn", required = true ) int bordSeqn,
		@RequestParam( value = "bordCd", required = true ) int bordCd,
		HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{
		AnucBordService.deleteAnucBord( bordSeqn );
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();

		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}
		AnucBord srchParam = new AnucBord();

		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;

		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setBordCd( bordCd );
		srchParam.setRgstIdnt( sessUserIdnt );

		int genreCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", genreCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );

		int totalRow = AnucBordService.countAnuc( srchParam );
		list = AnucBordService.selectAnuc( srchParam );
		for( int i = 0; i < list.size(); i++ ){
			int count = AnucBordService.countAnucObjc( (int) list.get( i ).getBordSeqn() );
			list.get( i ).setObjcCount( count );
		}
		int bordNo = (int) bordCd;

		Date date = new Date();
		SimpleDateFormat yearForm = new SimpleDateFormat( "yyyy" );
		SimpleDateFormat dayForm = new SimpleDateFormat( "yyyyMMdd" );
		String thisMonth = yearForm.format( date ) + "0101";
		String thisDay = dayForm.format( date );

		ModelAndView mav = new ModelAndView();
		if( bordCd == 1 ){
			mav.setViewName( "/statBord/statBo01ListMy" );
		}else if( bordCd == 5 ){
			mav.setViewName( "/statBord/statBo05ListMy" );
		}
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "bordCd", bordNo );
		mav.addObject( "defaultJoinDay", thisMonth );
		mav.addObject( "defaultEndDay", thisDay );
		mav.addObject( "codeList", ListCode );
		return mav;
	}

	// 마이페이지 검색
	@RequestMapping( "/statBord/statBo01ListMyFind.do" )
	public ModelAndView statBo01ListMyFind( HttpServletRequest request,
		@RequestParam( value = "bordCd", required = true ) int bordCd,
		@RequestParam( value = "joinDay", required = true ) String joinDay,
		@RequestParam( value = "endDay", required = true ) String endDay,
		@RequestParam( value = "title", required = true ) String title ) throws Exception{

		AnucBord srchParam = new AnucBord();
		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int genreCd = ServletRequestUtils.getIntParameter( request, "genreCd", 0 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();
		System.out.println( "sessUserIdnt: "+sessUserIdnt );
		System.out.println( "sessUserIdnt: "+sessUserIdnt );
		System.out.println( "sessUserIdnt: "+sessUserIdnt );
		System.out.println( "sessUserIdnt: "+sessUserIdnt );
		
		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}
		// 검색 시작
		int endDays = Integer.parseInt( endDay ) + 1;
		String endDays2 = String.valueOf( endDays );

		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setGenreCd( genreCd );
		srchParam.setTite( title );
		srchParam.setBordCd( bordCd );
		srchParam.setRgstIdnt( sessUserIdnt );
		srchParam.setEndDay( endDays2 );
		srchParam.setJoinDay( joinDay );
		int genCd = genreCd;
		list = AnucBordService.findAnuc( srchParam );

		for( int i = 0; i < list.size(); i++ ){
			int count = AnucBordService.countAnucObjc( (int) list.get( i ).getBordSeqn() );
			list.get( i ).setObjcCount( count );
		}
		int genreCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", genreCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );

		int totalRow = AnucBordService.findCount( srchParam );
		ModelAndView mav = new ModelAndView();
		if( bordCd == 5 ){
			mav.setViewName( "/statBord/statBo05ListMy" );
		}else{
			mav.setViewName( "/statBord/statBo01ListMy" );
		}
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "title", title );
		mav.addObject( "genreCd", genCd );
		mav.addObject( "bordCd", bordCd );
		mav.addObject( "defaultJoinDay", joinDay );
		mav.addObject( "defaultEndDay", endDay );
		mav.addObject( "codeList", ListCode );
		return mav;
	}

	// 상세보기+이의제기
	@RequestMapping( method = RequestMethod.GET, value = "/statBord/statBo01DetlMy.do" )
	public ModelAndView statBo01DetlMy( @RequestParam( value = "bordSeqn", required = true ) int bordSeqn,
		@RequestParam( value = "bordCd", required = true ) int bordCd,
		@RequestParam( value = "mode", required = false ) String mode,
		@RequestParam( value = "report", required = false ) String report,
		@RequestParam( value = "modal", required = false, defaultValue = "0" ) String modal ) throws Exception{

		int count = AnucBordService.countAnucObjc( bordSeqn ); // 이의제기 갯수
		List<AnucBord> list2 = AnucBordService.detailAnuc( bordSeqn ); // 게시글 상세보기 게시글 내용
		List<AnucBordFile> list4 = AnucBordService.fileSelectAnuc( bordSeqn ); // 게시판 파일
		List<AnucBordObjc> list3 = AnucBordService.selectAnucObjc( bordSeqn, bordCd ); // 이의제기 댓글 리스트

		AnucBord anucBord = list2.get( 0 );
		for( int i = 0; i < list3.size(); i++ ){
			int statObjcId = list3.get( i ).getStatObjcId();
			List<AnucBordFile> list5 = AnucBordService.fileSelectObjc( statObjcId ); // 이의제기 파일
			List<AnucBordObjc> list6 = AnucBordService.selectAnucObjcShis( statObjcId );
			log.info( list6.toString() );
			list3.get( i ).setShisList( list6 );
			list3.get( i ).setFileList( list5 );
		}

		List<AnucBordSupl> list7 = AnucBordService.selectBordSuplItemList( bordSeqn, bordCd ); // 공고정보 보완내역

		ModelAndView mav = new ModelAndView();
		if( bordCd == 5 ){
			mav.setViewName( "/statBord/statBo05DetlMy" );
		}else{
			mav.setViewName( "/statBord/statBo01DetlMy" );
		}
		if( "R".equals( mode ) ){
			mav = new ModelAndView( report );
			mav.addObject( "modal", modal );
		}
		mav.addObject( "SuplList", list7 );
		mav.addObject( "AnucBord", anucBord );
		mav.addObject( "AnucBordObjc", list3 );
		mav.addObject( "count", count );
		mav.addObject( "fileList", list4 );
		mav.addObject( "bordCd", bordCd );

		return mav;
	}

	// 마이페이지 보상금공탁
	@RequestMapping( "/statBord/statBo05ListMy.do" )
	public ModelAndView statBo05ListMy( @RequestParam( value = "bordCd", required = true ) int bordCd,
		HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{
		AnucBord srchParam = new AnucBord();

		int pageNo = ServletRequestUtils.getIntParameter( request, "page_no", 1 );
		int from = Constants.DEFAULT_ROW_PER_PAGE * ( pageNo - 1 ) + 1;
		int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();

		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}
		srchParam.setNowPage( pageNo );
		srchParam.setStartRow( from );
		srchParam.setEndRow( to );
		srchParam.setBordCd( bordCd );
		srchParam.setRgstIdnt( sessUserIdnt );

		int totalRow = AnucBordService.countAnuc( srchParam );
		list = AnucBordService.selectAnuc( srchParam );
		for( int i = 0; i < list.size(); i++ ){
			int count = AnucBordService.countAnucObjc( (int) list.get( i ).getBordSeqn() );
			list.get( i ).setObjcCount( count );
		}
		int bordNo = (int) bordCd;

		Date date = new Date();
		SimpleDateFormat yearForm = new SimpleDateFormat( "yyyy" );
		SimpleDateFormat dayForm = new SimpleDateFormat( "yyyyMMdd" );
		String thisMonth = yearForm.format( date ) + "0101";
		String thisDay = dayForm.format( date );

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo05ListMy" );
		mav.addObject( "AnucBord", list );
		mav.addObject( "totalRow", totalRow );
		mav.addObject( "AnucBordnum", srchParam );
		mav.addObject( "bordCd", bordNo );
		mav.addObject( "defaultJoinDay", thisMonth );
		mav.addObject( "defaultEndDay", thisDay );
		return mav;
	}

	// 등록화면(마이페이지 상당한노력공고)
	@RequestMapping( "/statBord/statBo01RegiPop.do" )
	public ModelAndView statBo01RegiPop() throws Exception{
		int bigCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", bigCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo01RegiPop" );
		mav.addObject( "codeList", ListCode );
		return mav;
	}

	// 등록화면(마이페이지 보상금공탁공고)
	@RequestMapping( "/statBord/statBo05RegiPop.do" )
	public ModelAndView statBo05RegiPop() throws Exception{
		int bigCode = 35;
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", bigCode );
		List<Code> ListCode = AnucBordService.getCodeList( params );
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo05RegiPop" );
		mav.addObject( "codeList", ListCode );
		return mav;
	}

	@RequestMapping( "/statBord/statBo02ObjcPop.do" )
	public ModelAndView statBo02RegiPop( @RequestParam( value = "worksId" ) int worksId,
		@RequestParam( value = "divsCd" ) int divsCd ) throws Exception{
		int bigCode = 35;
		Map<Object, Object> code = new HashMap<Object, Object>();
		code.put( "bigCode", bigCode );
		AnucBord anucBord = new AnucBord();
		anucBord.setWorksId( worksId );
		anucBord.setBigCode( bigCode );
		anucBord.setDivsCd( divsCd );
		ModelAndView mav = new ModelAndView();

		List<AnucBord> list = AnucBordService.StatObjcPop( anucBord );
		List<AnucBordObjc> list3 = AnucBordService.selectStatObjc( worksId );
		List<AnucBord> list2 = AnucBordService.detailNonCopthodr( anucBord );
		int count = AnucBordService.countStatObjc( worksId );
		if( list3.size() != 0 ){
			for( int i = 0; i < list3.size(); i++ ){
				int statObjcId = list3.get( i ).getStatObjcId();
				List<AnucBordFile> list5 = AnucBordService.fileSelectObjc( statObjcId ); // 이의제기 파일
				List<AnucBordObjc> list6 = AnucBordService.selectAnucObjcShis( statObjcId );
				log.info( list6.toString() );
				list3.get( i ).setShisList( list6 );
				list3.get( i ).setFileList( list5 );
			}
		}
		mav.setViewName( "/stat/statBo02ObjcPop" );
		mav.addObject( "AnucBord", list.get( 0 ) );
		mav.addObject( "coptHodr", list2 );
		mav.addObject( "AnucBordObjc", list3 );
		mav.addObject( "count", count );
		return mav;
	}

	@RequestMapping( "/statBord/statBo02ObjcInsert.do" )
	public ModelAndView insertStatObjc( @ModelAttribute AnucBordObjc anucBordObjc,
		HttpServletRequest request,
		HttpServletResponse respone,
		@RequestParam( value = "worksId", required = true ) int worksId ) throws Exception{

		int bigCode = 35;
		Map<Object, Object> code = new HashMap<Object, Object>();
		code.put( "bigCode", bigCode );
		AnucBord anucBord = new AnucBord();
		anucBord.setWorksId( worksId );
		anucBord.setBigCode( bigCode );

		// 파일첨부
		AnucBordFile anucBordFile = new AnucBordFile();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );

		anucBordObjc.setRgstIdnt( multipartRequest.getParameter( "rgstIdnt" ) );
		String objcDesc = multipartRequest.getParameter( "objcDesc" );
		objcDesc = objcDesc.replace( "\r\n", "<br>" );
		anucBordObjc.setObjcDesc( XssUtil.unscript( objcDesc ) );

		anucBord.setMgntDivs( multipartRequest.getParameter( "mgntDivs" ) );
		anucBord.setWorksTitle( multipartRequest.getParameter( "worksTitl" ) );

		anucBordFile.setFileName( multipartRequest.getParameter( "fileName" ) );
		anucBordFile.setFilePath( multipartRequest.getParameter( "filePath" ) );
		anucBordFile.setFileSize( multipartRequest.getParameter( "fileSize" ) );

		// File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();

		while( fileNameIterator.hasNext() ){
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
				fileList.add( anucBordFile );
			}
		}
		anucBordObjc.setFileList( fileList );
		AnucBordService.insertStatObjc( anucBordObjc );

		User userInfo = SessionUtil.getSession( request );

		HashMap userMap = new HashMap();

		// 신청제목
		userMap.put( "WORKS_TITL", anucBord.getWorksTitle() );

		// 사용자 정보 셋팅
		userMap.put( "USER_IDNT", userInfo.getUserIdnt() );
		userMap.put( "USER_NAME", userInfo.getUserName() );
		userMap.put( "U_MAIL", userInfo.getMail() );
		userMap.put( "MOBL_PHON", userInfo.getMoblPhon() );

		if( anucBord.getMgntDivs().equals( "BO02_01" ) ){
			userMap.put( "MGNT_DIVS", "BO02_01" );
			userMap.put( "MAIL_TITL", "법정허락 신청 대상저작물 이의 " );
			userMap.put( "MAIL_TITL_DIVS", "신청" );
		}

		List list = new ArrayList();

		list = commonService.getMgntMail( userMap );

		int listSize = list.size();

		String to[] = new String[listSize];
		String toName[] = new String[listSize];

		for( int i = 0; i < listSize; i++ ){
			HashMap map = (HashMap) list.get( i );
			toName[i] = (String) map.get( "USER_NAME" );
			to[i] = (String) map.get( "MAIL" );
		}

		userMap.put( "TO", to );
		userMap.put( "TO_NAME", toName );

		SendMail.send( userMap );

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/stat/statBo02ObjcPop" );
		mav.addObject( "Success", 1 );
		return mav;
	}

	// 이의제기 삭제
	@RequestMapping( value = "/statBord/statObjcDelete.do" )
	public ModelAndView statObjcDelete( @RequestParam( value = "statObjcId", required = true ) int statObjcId,
		@RequestParam( value = "worksId", required = true ) int worksId ) throws Exception{

		AnucBordService.deleteStatObjc( statObjcId );

		int bigCode = 35;
		Map<Object, Object> code = new HashMap<Object, Object>();
		code.put( "bigCode", bigCode );
		AnucBord anucBord = new AnucBord();
		anucBord.setWorksId( worksId );
		anucBord.setBigCode( bigCode );

		List<AnucBord> list = AnucBordService.detailNonAnuc( worksId );
		List<AnucBord> list2 = AnucBordService.detailNonCopthodr( anucBord );
		List<AnucBordObjc> list3 = AnucBordService.selectStatObjc( worksId );
		int count = AnucBordService.countStatObjc( worksId );
		if( count == 0 ){
			AnucBordService.updateStatWorksObjcYnDelete( worksId );
		}
		for( int i = 0; i < list3.size(); i++ ){
			int statObjcIds = list3.get( i ).getStatObjcId();
			List<AnucBordFile> list5 = AnucBordService.fileSelectObjc( statObjcIds ); // 이의제기 파일
			List<AnucBordObjc> list6 = AnucBordService.selectAnucObjcShis( statObjcIds );
			log.info( list6.toString() );
			list3.get( i ).setShisList( list6 );
			list3.get( i ).setFileList( list5 );
		}

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/stat/statBo02ObjcPop" );
		mav.addObject( "AnucBord", list.get( 0 ) );
		mav.addObject( "coptHodr", list2 );
		mav.addObject( "AnucBordObjc", list3 );
		mav.addObject( "count", count );
		return mav;
	}

	@RequestMapping( value = "/statBord/statObjcUpdate.do" )
	public ModelAndView statObjcUpdate( @ModelAttribute AnucBordObjc anucBordObjc,
		@RequestParam( value = "worksId", required = true ) int worksId,
		HttpServletRequest request ) throws Exception{
		String objcDesc = ServletRequestUtils.getStringParameter( request, "objcDesc" );
		objcDesc = objcDesc.replace( "\r\n", "<br>" );
		anucBordObjc.setObjcDesc( XssUtil.unscript( objcDesc ) );
		AnucBordService.updateAnucObjc( anucBordObjc );

		int bigCode = 35;
		Map<Object, Object> code = new HashMap<Object, Object>();
		code.put( "bigCode", bigCode );
		AnucBord anucBord = new AnucBord();
		anucBord.setWorksId( worksId );
		anucBord.setBigCode( bigCode );

		List<AnucBord> list = AnucBordService.detailNonAnuc( worksId );
		List<AnucBord> list2 = AnucBordService.detailNonCopthodr( anucBord );
		List<AnucBordObjc> list3 = AnucBordService.selectStatObjc( worksId );
		int count = AnucBordService.countStatObjc( worksId );

		for( int i = 0; i < list3.size(); i++ ){
			int statObjcIds = list3.get( i ).getStatObjcId();
			List<AnucBordFile> list5 = AnucBordService.fileSelectObjc( statObjcIds ); // 이의제기 파일
			List<AnucBordObjc> list6 = AnucBordService.selectAnucObjcShis( statObjcIds );
			log.info( list6.toString() );
			list3.get( i ).setShisList( list6 );
			list3.get( i ).setFileList( list5 );
		}

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/stat/statBo02ObjcPop" );
		mav.addObject( "AnucBord", list.get( 0 ) );
		mav.addObject( "coptHodr", list2 );
		mav.addObject( "AnucBordObjc", list3 );
		mav.addObject( "count", count );
		return mav;
	}

	@RequestMapping( "/statBord/statBo05Modi.do" )
	public ModelAndView statBo05Modi( @RequestParam( value = "bordSeqn", required = true ) int bordSeqn )
		throws Exception{
		List<AnucBord> list2 = AnucBordService.detailAnuc( bordSeqn ); // 게시글 상세보기 게시글 내용
		List<AnucBordFile> list4 = AnucBordService.fileSelectAnuc( bordSeqn ); // 게시판 파일
		log.info( list2.toString() );
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo05Modi" );
		mav.addObject( "fileList", list4 );
		mav.addObject( "AnucBord", list2.get( 0 ) );
		mav.addObject( "listSize", list4.size() );
		return mav;
	}

	@SuppressWarnings( "finally" )
	@RequestMapping( "/statBord/statBo05ModiProc.do" )
	public ModelAndView statBo05ModiProc( HttpServletRequest request ) throws Exception{
		AnucBord anucBord = new AnucBord();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		// 파일첨부
		AnucBordFile anucBordFile = new AnucBordFile();

		multipartRequest = multipartResolver.resolveMultipart( request );
		anucBord.setTite( XssUtil.unscript( multipartRequest.getParameter( "tite" ) ) );
		anucBord.setBordDesc( XssUtil.unscript( multipartRequest.getParameter( "bordDesc" ) ) );
		anucBord.setRgstIdnt( multipartRequest.getParameter( "rgstIdnt" ) );

		anucBord.setModiIdnt( multipartRequest.getParameter( "modiIdnt" ) );
		anucBord.setAnucItem1( XssUtil.unscript( multipartRequest.getParameter( "anucItem1" ) ) );
		anucBord.setAnucItem2( XssUtil.unscript( multipartRequest.getParameter( "anucItem2" ) ) );
		anucBord.setAnucItem3( XssUtil.unscript( multipartRequest.getParameter( "anucItem3" ) ) );
		anucBord.setAnucItem4( XssUtil.unscript( multipartRequest.getParameter( "anucItem4" ) ) );
		anucBord.setAnucItem5( XssUtil.unscript( multipartRequest.getParameter( "anucItem5" ) ) );
		anucBord.setAnucItem6( XssUtil.unscript( multipartRequest.getParameter( "anucItem6" ) ) );
		anucBord.setAnucItem7( XssUtil.unscript( multipartRequest.getParameter( "anucItem7" ) ) );
		anucBord.setAnucItem8( XssUtil.unscript( multipartRequest.getParameter( "anucItem8" ) ) );
		// anucBord.setAnucItem9(multipartRequest.getParameter("anucItem9"));
		// anucBord.setAnucItem10(multipartRequest.getParameter("anucItem10"));
		// anucBord.setAnucItem11(multipartRequest.getParameter("anucItem11"));
		// anucBord.setAnucItem12(multipartRequest.getParameter("anucItem12"));
		// anucBord.setAnucItem13(multipartRequest.getParameter("anucItem13"));
		// anucBord.setAnucItem14(multipartRequest.getParameter("anucItem14"));
		// anucBord.setAnucItem15(multipartRequest.getParameter("anucItem15"));
		anucBord.setBordSeqn( StringUtil.nullToZero( multipartRequest.getParameter( "bordSeqn" ) ) );
		anucBord.setBordCd( StringUtil.nullToZero( multipartRequest.getParameter( "bordCd" ) ) );
		anucBordFile.setFileName( multipartRequest.getParameter( "fileName" ) );
		anucBordFile.setFilePath( multipartRequest.getParameter( "filePath" ) );
		anucBordFile.setFileSize( multipartRequest.getParameter( "fileSize" ) );

		String fileCheck = multipartRequest.getParameter( "fileCheck" ); // 삭제된 파일 체크
		int fileLength = StringUtil.nullToZeroInt( multipartRequest.getParameter( "fileLength" ) );
		// File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		log.info( fileNameIterator );
		List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();
		while( fileNameIterator.hasNext() ){
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
				fileList.add( anucBordFile );
			}
		}
		anucBord.setFileList( fileList );
		try{
			AnucBordService.updateAnucBord( anucBord );
			String[] fileCk = fileCheck.split( "," );
			log.info( fileCk );
			for( int i = 0; i <= fileLength; i++ ){
				int attcSeqn = Integer.parseInt( fileCk[i] );
				AnucBordService.deleteAttcFile( attcSeqn );
			}
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		finally{
			int bordSeqn = (int) anucBord.getBordSeqn();
			List<AnucBord> list2 = AnucBordService.detailAnuc( bordSeqn ); // 게시글 상세보기 게시글 내용
			List<AnucBordFile> list4 = AnucBordService.fileSelectAnuc( bordSeqn ); // 게시판 파일
			ModelAndView mav = new ModelAndView();
			mav.setViewName( "/statBord/statBo05DetlMy" );
			mav.addObject( "fileList", list4 );
			mav.addObject( "AnucBord", list2.get( 0 ) );
			return mav;
		}
	}

	@RequestMapping( "/statBord/statBo01Modi.do" )
	public ModelAndView statBo01Modi( @RequestParam( value = "bordSeqn", required = true ) int bordSeqn )
		throws Exception{
		List<AnucBord> list2 = AnucBordService.detailAnuc( bordSeqn ); // 게시글 상세보기 게시글 내용
		List<AnucBordFile> list4 = AnucBordService.fileSelectAnuc( bordSeqn ); // 게시판 파일
		HashMap<Object, Object> params = new HashMap<Object, Object>();
		params.put( "bigCode", 35 );
		List<Code> ListCode = AnucBordService.getCodeList( params );
		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo01Modi" );
		mav.addObject( "fileList", list4 );
		mav.addObject( "AnucBord", list2.get( 0 ) );
		mav.addObject( "listSize", list4.size() );
		mav.addObject( "codeList", ListCode );
		return mav;
	}

	@SuppressWarnings( "finally" )
	@RequestMapping( "/statBord/statBo01ModiProc.do" )
	public ModelAndView statBo01ModiProc( HttpServletRequest request ) throws Exception{
		AnucBord anucBord = new AnucBord();
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		// MultipartHttpServletRequest multipartRequest;
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		// 파일첨부
		AnucBordFile anucBordFile = new AnucBordFile();

		// multipartRequest = multipartResolver.resolveMultipart(request);
		anucBord.setTite( XssUtil.unscript( multipartRequest.getParameter( "tite" ) ) );
		String bordDesc = multipartRequest.getParameter( "bordDesc" );
		bordDesc = bordDesc.replace( "\r\n", "<br>" );
		anucBord.setBordDesc( XssUtil.unscript( bordDesc ) );
		anucBord.setBordSeqn( StringUtil.nullToZeroInt( multipartRequest.getParameter( "bordSeqn" ) ) );
		anucBord.setGenreCd( StringUtil.nullToZero( multipartRequest.getParameter( "genreCd" ) ) );

		anucBord.setRgstIdnt ( multipartRequest.getParameter( "rgstIdnt" ) );

		anucBord.setModiIdnt( multipartRequest.getParameter( "modiIdnt" ) );
		anucBord.setAnucItem1( XssUtil.unscript( multipartRequest.getParameter( "anucItem1" ) ) );
		anucBord.setAnucItem2( XssUtil.unscript( multipartRequest.getParameter( "anucItem2" ) ) );
		anucBord.setAnucItem3( XssUtil.unscript( multipartRequest.getParameter( "anucItem3" ) ) );
		anucBord.setAnucItem4( XssUtil.unscript( multipartRequest.getParameter( "anucItem4" ) ) );
		anucBord.setAnucItem5( XssUtil.unscript( multipartRequest.getParameter( "anucItem5" ) ) );
		anucBord.setAnucItem6( XssUtil.unscript( multipartRequest.getParameter( "anucItem6" ) ) );
		anucBord.setAnucItem7( XssUtil.unscript( multipartRequest.getParameter( "anucItem7" ) ) );
		anucBord.setAnucItem8( XssUtil.unscript( multipartRequest.getParameter( "anucItem8" ) ) );
		anucBord.setAnucItem9( XssUtil.unscript( multipartRequest.getParameter( "anucItem9" ) ) );
		anucBord.setAnucItem10( XssUtil.unscript( multipartRequest.getParameter( "anucItem10" ) ) );
		anucBord.setAnucItem11( XssUtil.unscript( multipartRequest.getParameter( "anucItem11" ) ) );
		anucBord.setAnucItem12( XssUtil.unscript( multipartRequest.getParameter( "anucItem12" ) ) );
		anucBord.setAnucItem13( XssUtil.unscript( multipartRequest.getParameter( "anucItem13" ) ) );
		anucBord.setAnucItem14( multipartRequest.getParameter( "anucItem14" ) );
		anucBord.setAnucItem15( multipartRequest.getParameter( "anucItem15" ) );
		anucBord.setBordSeqn( StringUtil.nullToZero( multipartRequest.getParameter( "bordSeqn" ) ) );
		anucBord.setBordCd( StringUtil.nullToZero( multipartRequest.getParameter( "bordCd" ) ) );
		anucBordFile.setFileName( multipartRequest.getParameter( "fileName" ) );
		anucBordFile.setFilePath( multipartRequest.getParameter( "filePath" ) );
		anucBordFile.setFileSize( multipartRequest.getParameter( "fileSize" ) );
		
		String fileCheck = multipartRequest.getParameter( "fileCheck" ); // 삭제된 파일 체크
	
		int fileLength = StringUtil.nullToZeroInt( multipartRequest.getParameter( "fileLength" ) );
		
		// File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
	
		List<AnucBordFile> fileList = new ArrayList<AnucBordFile>();
		while( fileNameIterator.hasNext() ){
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				anucBordFile = FileUploadUtil.uploadFormFilesStatBord( multiFile, realUploadPath );
				fileList.add( anucBordFile );
			}
		}
		anucBord.setFileList( fileList );
		try{
			AnucBordService.updateAnucBord( anucBord );
			String[] fileCk = fileCheck.split( "," );
			
			for( int i = 0; i <= fileLength-1; i++ ){
				int attcSeqn = Integer.parseInt( fileCk[i] );

				AnucBordService.deleteAttcFile( attcSeqn );
			}
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		finally{
			int bordSeqn = (int) anucBord.getBordSeqn();
			int count = AnucBordService.countAnucObjc( bordSeqn ); // 이의제기 갯수
			List<AnucBord> list2 = AnucBordService.detailAnuc( bordSeqn ); // 게시글 상세보기 게시글 내용
			List<AnucBordFile> list4 = AnucBordService.fileSelectAnuc( bordSeqn ); // 게시판 파일
			HashMap<Object, Object> params = new HashMap<Object, Object>();
			params.put( "bigCode", 35 );
			List<Code> ListCode = AnucBordService.getCodeList( params );
			// ModelAndView mav = new ModelAndView();
			// mav.setViewName("/statBord/statBo01DetlMy");
			// mav.addObject("fileList",list4);
			// mav.addObject("AnucBord", list2.get(0));
			// mav.addObject("count", count);
			return new ModelAndView( "redirect:/statBord/statBo01ListMy.do?bordCd=1" );
		}
	}

	// 기승인 법정허락 저작물 리스트
	@RequestMapping( "/statBord/statBo07List.do" )
	public ModelAndView statBo07List( @RequestParam( value = "pageNo", required = true, defaultValue = "1" ) int pageNo,
		HttpServletRequest request ) throws Exception{

		String genreStr = ServletRequestUtils.getStringParameter( request, "genreStr" );
		System.out.println( "genreStr : " + genreStr );
		String title = ServletRequestUtils.getStringParameter( request, "title" );
		System.out.println( "title : " + title );
		Map dataMap = new HashMap();
		dataMap.put( "pageNo", pageNo );
		dataMap.put( "genreStr", genreStr );
		dataMap.put( "title", title );

	//	int totalRow = AnucBordService.statBo07TotalCount( dataMap );
	//	List returnList = AnucBordService.statBo07List( dataMap );

		/** paging */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo( pageNo );

		ModelAndView mav = new ModelAndView();
		mav.setViewName( "/statBord/statBo07List" );

		mav.addObject( "genreStr", genreStr );
		mav.addObject( "title", title );

		//mav.addObject( "totalRow", totalRow );
		//mav.addObject( "returnList", returnList );
		mav.addObject( "paginationInfo", paginationInfo );
		return mav;
	}

	// 증명서 출력 등록되지 않은 위탁관리 저작물
	@RequestMapping( "/statBord/statBo08Select.do" )
	public ModelAndView statBo08Select( HttpServletRequest request ) throws Exception{
		User user = SessionUtil.getSession( request );
		String sessUserIdnt = user.getUserIdnt();
		String query = ServletRequestUtils.getStringParameter( request, "query" );
		System.out.println( "query : " + query );
		if( sessUserIdnt == null ){
			return new ModelAndView( "redirect:/user/user.do?method=goLogin" );
		}else{
			System.out.println( "user : " + user );
		}
		ModelAndView mav = new ModelAndView();
		mav.addObject( "user", user );
		mav.addObject( "query", query );
		mav.setViewName( "/statBord/statBo08Select" );

		return mav;
	}
}
