package kr.or.copyright.mls.mobile.service;

import java.util.HashMap;
import java.util.List;

import kr.or.copyright.mls.mobile.dao.MobileInfoDao;
import kr.or.copyright.mls.mobile.model.Book;
import kr.or.copyright.mls.mobile.model.Broadcast;
import kr.or.copyright.mls.mobile.model.Image;
import kr.or.copyright.mls.mobile.model.Movie;
import kr.or.copyright.mls.mobile.model.Music;
import kr.or.copyright.mls.mobile.model.News;
import kr.or.copyright.mls.mobile.model.Script;

public class MobileInfoServiceImpl implements MobileInfoService {
	private MobileInfoDao mobileInfoDao;
	
	public List<Music> musicList(HashMap<String,Object> searchWord) {
		return mobileInfoDao.musicList(searchWord);
	}
	
	public int musicListCnt(HashMap<String, Object> searchWord) {
		return mobileInfoDao.musicListCnt(searchWord);
	}
	
	public Music musicDetl(HashMap<String,Object> searchWord) {
		return mobileInfoDao.musicDetl(searchWord);
	}
	
	public List<Book> bookList(HashMap<String,Object> searchWord) {
		return mobileInfoDao.bookList(searchWord);
	}
	
	public int bookListCnt(HashMap<String,Object> searchWord) {
		return mobileInfoDao.bookListCnt(searchWord);
	}
	
	public Book bookDetl(HashMap<String,Object> searchWord) {
		return mobileInfoDao.bookDetl(searchWord);
	}
	
	public List<News> newsList(HashMap<String,Object> searchWord) {
		return mobileInfoDao.newsList(searchWord);
	}
	
	public int newsListCnt(HashMap<String,Object> searchWord) {
		return mobileInfoDao.newsListCnt(searchWord);
	}
	
	public News newsDetl(HashMap<String,Object> searchWord) {
		return mobileInfoDao.newsDetl(searchWord);
	}
	
	public List<Script> scriptList(HashMap<String,Object> searchWord) {
		return mobileInfoDao.scriptList(searchWord);
	}
	
	public int scriptListCnt(HashMap<String,Object> searchWord) {
		return mobileInfoDao.scriptListCnt(searchWord);
	}
	
	public Script scriptDetl(HashMap<String,Object> searchWord) {
		return mobileInfoDao.scriptDetl(searchWord);
	}
	
	public List<Image> imageList(HashMap<String,Object> searchWord) {
		return mobileInfoDao.imageList(searchWord);
	}
	
	public int imageListCnt(HashMap<String,Object> searchWord) {
		return mobileInfoDao.imageListCnt(searchWord);
	}
	
	public Image imageDetl(HashMap<String,Object> searchWord) {
		return mobileInfoDao.imageDetl(searchWord);
	}
	
	public List<Movie> movieList(HashMap<String,Object> searchWord) {
		return mobileInfoDao.movieList(searchWord);
	}
	
	public int movieListCnt(HashMap<String,Object> searchWord) {
		return mobileInfoDao.movieListCnt(searchWord);
	}
	
	public Movie movieDetl(HashMap<String,Object> searchWord) {
		return mobileInfoDao.movieDetl(searchWord);
	}
	
	public List<Broadcast> broadcastList(HashMap<String,Object> searchWord) {
		return mobileInfoDao.broadcastList(searchWord);
	}
	
	public int broadcastListCnt(HashMap<String,Object> searchWord) {
		return mobileInfoDao.broadcastListCnt(searchWord);
	}
	
	public Broadcast broadcastDetl(HashMap<String,Object> searchWord) {
		return mobileInfoDao.broadcastDetl(searchWord);
	}
	
	
	
	public MobileInfoDao getMobileInfoDao() {
		return mobileInfoDao;
	}

	public void setMobileInfoDao(MobileInfoDao mobileInfoDao) {
		this.mobileInfoDao = mobileInfoDao;
	}

}
