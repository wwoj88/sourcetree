package kr.or.copyright.mls.console;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.dreamsecurity.magicline.config.Logger;

@Controller( "consoleInterceptor" )
public class ConsoleInterceptor extends HandlerInterceptorAdapter{

	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;

	@Override
	public boolean preHandle( HttpServletRequest request,		HttpServletResponse response,		Object handler ) throws Exception{
		String strUrl = request.getRequestURI();
		if( strUrl.startsWith( "/console/" ) ){
			Enumeration<?> param = request.getParameterNames();
			String strParam = "";
			int i = 0;
			while( param.hasMoreElements() ){
				String name = (String) param.nextElement();
				String value = request.getParameter( name );
				if( i == 0 ){
					strParam += name + "=" + value;
				}else{
					strParam += "&" + name + "=" + value;
				}
				i++;
			}
			String url = "";

			if( !"".equals( strParam ) ){
				url = request.getRequestURI() + "?" + strParam;
			}else{
				url = request.getRequestURI();

			}

			String[] exceptUrl = new String[]{ "/console/common/loginPage.page", "/console/common/login.page" };
			boolean isExceptUrl = false;
			//System.out.println( "exceptUrl :" + exceptUrl.toString() );
			for( int j = 0; j < exceptUrl.length; j++ ){
				if( url.indexOf( exceptUrl[j] ) > -1 ){
					System.out.println( "url.indexOf( exceptUrl[j]123 ) :" + url.indexOf( exceptUrl[j] ) );
					isExceptUrl = true;
					break;
				}
			}
			System.out.println( "LOGIN 1 :" + isExceptUrl );
			System.out.println( "LOGIN 3 :" + ConsoleLoginUser.isLogin() );
			if( !isExceptUrl ){
				System.out.println( "LOGIN 2 :" + ConsoleLoginUser.isLogin() );
				if( !ConsoleLoginUser.isLogin() ){
					response.sendRedirect( "/console/notlogin.jsp" );
					return false;
				}
			}

			String curMenuId = request.getParameter( "menuId" );
			if( null != curMenuId ){
				request.getSession().setAttribute( "menuId", curMenuId );
			}
		}

		return true;
	}

	@Override
	public void postHandle( HttpServletRequest request,		HttpServletResponse response,		Object handler,		ModelAndView modelAndView ) throws Exception{
		String strUrl = request.getRequestURI();

		String isExcelDown = "N";

		if( modelAndView != null ){
			isExcelDown = (String) ( modelAndView.getModelMap().get( "ExcelDown" ) != null
				? modelAndView.getModelMap().get( "ExcelDown" ) : "N" );
		}

		System.out.println( "## strUrl : " + strUrl );
		if( isExcelDown != null && "Y".equals( isExcelDown ) ){
			response.setContentType( "application/vnd.ms-excel" );
		}else{
			if( strUrl.startsWith( "/console/" ) ){
				String contentType = response.getContentType();
				if( "applicaiton/download;charset=utf-8".equals( contentType )	|| "XMLHttpRequest".equals( request.getHeader( "X-Requested-With" ) ) ){
					
				}else{
					System.out.println( "## ConsoleLoginUser.isLogin() : " + ConsoleLoginUser.isLogin() );

					if( ConsoleLoginUser.isLogin() ){
						try{
							// �޴����� ����
							// header location
							// �α�������
							Map<String, Object> loginInfo = ConsoleLoginUser.getConsoleLoginInfo();

							// ���� �޴� ����
							int curMenuId = EgovWebUtil.toInt( (String) request.getSession().getAttribute( "menuId" ) );
							// ���� �޴� ����
							Map<String, Object> curMenuInfo = null;
							// ���� �׷�SEQ
							String curGroupYmd = EgovWebUtil.getString( loginInfo, "GROUP_YMD" );
							int curGroupSeq = EgovWebUtil.getToInt( loginInfo, "GROUP_SEQ" );

							// ��ü �޴� ��ȸ
							Map<String, Object> commandMap = new HashMap<String, Object>();
							commandMap.put( "GROUP_YMD", curGroupYmd );
							commandMap.put( "GROUP_SEQ", curGroupSeq );
							System.out.println( "## commandMap : " + commandMap );
							ArrayList<Map<String, Object>> menuList =
								(ArrayList<Map<String, Object>>) consoleCommonService.selectMenuGroupList( commandMap );
							System.out.println( "## menuList : " + menuList.size() );
							// ����޴����� ��ȸ
							for( int i = 0; i < menuList.size(); i++ ){
								Map<String, Object> info = menuList.get( i );
								int menuId = EgovWebUtil.getInt( info, "MENU_ID" );

								if( menuId == curMenuId ){
									curMenuInfo = info;
									break;
								}
							}
							if( modelAndView != null ){
								if( null != loginInfo ){
									modelAndView.addObject( "loginInfo", loginInfo );
								}
								modelAndView.addObject( "curMenuInfo", curMenuInfo );
								modelAndView.addObject( "leftMenu", getLeftMenu( menuList, curMenuInfo ) );
							}

						}
						catch( Exception e ){
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	public boolean hasMenu( String[] menuArr,
		int menuId ){
		boolean is = false;
		if( menuArr != null ){
			for( String menu : menuArr ){
				int menuInt = EgovWebUtil.getToInt( menu );
				if( menuInt == menuId ){
					is = true;
					break;
				}
			}
		}
		return is;
	}

	public String getLeftMenu( ArrayList<Map<String, Object>> leftMenuList,
		Map<String, Object> curMenuInfo ){
		String html = "";
		int preMenuDepth = 0;
		int curMenuId = EgovWebUtil.getInt( curMenuInfo, "MENU_ID" );
		String menuArrStr = EgovWebUtil.getString( curMenuInfo, "MENU_ID_ARR" );
		String[] menuArr = menuArrStr.split( "\\," );

		for( int i = 0; i < leftMenuList.size(); i++ ){
			Map<String, Object> info = leftMenuList.get( i );
			String url = EgovWebUtil.getString( info, "MENU_URL" );
			String menuTitle = EgovWebUtil.getString( info, "MENU_TITLE" );
			int menuId = EgovWebUtil.getInt( info, "MENU_ID" );
			int parentMenuId = EgovWebUtil.getInt( info, "PARENT_MENU_ID" );
			int menuDepth = EgovWebUtil.getInt( info, "MENU_DEPTH" );
			int childCnt = EgovWebUtil.getInt( info, "CHILD_CNT" );
			int menuDepthCap = preMenuDepth - menuDepth;

			if( menuDepthCap > 0 ){
				for( int j = 0; j < menuDepthCap; j++ ){
					html += "</li></ul>";
				}
			}else{
				if( i > 0 && menuDepthCap == 0 ){
					html += "</li>";
				}
			}

			if( menuDepth == 1 ){
				if( hasMenu( menuArr, menuId ) ){
					html += "<li class=\"active treeview\"><a href=\"#\"><span>" + menuTitle
						+ "</span> <i class=\"fa fa-angle-left pull-right\"></i></a>";
				}else{
					html += "<li class=\"treeview\"><a href=\"#\"><span>" + menuTitle
						+ "</span> <i class=\"fa fa-angle-left pull-right\"></i></a>";
				}
			}else{
				if( childCnt > 0 ){
					if( hasMenu( menuArr, menuId ) ){
						html +=
							"<li class=\"active\"><a href=\"#\"><i class=\"fa fa-circle-o\"></i>" + menuTitle + "</a>";
					}else{
						html += "<li><a href=\"#\"><i class=\"fa fa-circle-o\"></i>" + menuTitle + "</a>";
					}
				}else{
					if( hasMenu( menuArr, menuId ) ){
						html += "<li class=\"active\"><a href=\"#\" onclick=\"fncGoMenu('" + url + "', '" + menuId
							+ "', 'Y');\"><i class=\"fa fa-circle-o\"></i>" + menuTitle + "</a>";
					}else{
						html += "<li><a href=\"#\" onclick=\"fncGoMenu('" + url + "', '" + menuId
							+ "', 'Y');\"><i class=\"fa fa-circle-o\"></i>" + menuTitle + "</a>";
					}
				}
			}

			if( childCnt > 0 ){
				html += "<ul class=\"treeview-menu\">";
			}
			preMenuDepth = menuDepth;
		}

		return html;
	}
}
