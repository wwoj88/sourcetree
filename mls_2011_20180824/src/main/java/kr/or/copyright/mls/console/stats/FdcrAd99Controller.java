package kr.or.copyright.mls.console.stats;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.stats.inter.FdcrAd99Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 통계관리 > 등록부 연계 등록현황
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd99Controller extends DefaultController{

	@Resource( name = "fdcrAd99Service" )
	private FdcrAd99Service fdcrAd99Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd99Controller.class );

	/**
	 * 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd99List1.page" )
	public String fdcrAd99List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd99List1 Start" );
		// 파라미터 셋팅
		fdcrAd99Service.fdcrAd99List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "TOTAL", commandMap.get( "TOTAL" ) );
		logger.debug( "fdcrAd99List1 End" );
		return "/stats/fdcrAd99List1.tiles";
	}
}
