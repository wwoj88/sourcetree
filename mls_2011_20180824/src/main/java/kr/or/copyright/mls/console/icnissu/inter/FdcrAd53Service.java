package kr.or.copyright.mls.console.icnissu.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd53Service{

	/**
	 * 음악저작물 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd53List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 음악저작물 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd53View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 음악저작물 ICN발급 업데이트
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd53Update1( Map<String, Object> commandMap ) throws Exception;

}
