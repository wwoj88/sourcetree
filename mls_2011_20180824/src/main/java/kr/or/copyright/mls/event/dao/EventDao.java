package kr.or.copyright.mls.event.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.event.model.Event;

public interface EventDao {

	public int checkCampPart(Event event);
	
	public void insertCampPartInfo (Event event);
	
	public void insertCampPartRslt (Event event);
	
	public List campPartList(Map map);
}
