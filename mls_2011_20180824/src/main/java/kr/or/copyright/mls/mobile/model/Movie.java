package kr.or.copyright.mls.mobile.model;

public class Movie {
	private int
		crId,
		nrId,
		albumId
	;
	
	private String
		title
		, director
		, leadingActor
		, produceYear
		, genreStr
		, movieType
		, runTime
		, viewGrade
		, postUrs
		, producer
		, investor
		, distributor
		, trustYnStr
	;

	public int getCrId() {
		return crId;
	}

	public void setCrId(int crId) {
		this.crId = crId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getLeadingActor() {
		return leadingActor;
	}

	public void setLeadingActor(String leadingActor) {
		this.leadingActor = leadingActor;
	}

	public String getProduceYear() {
		return produceYear;
	}

	public void setProduceYear(String produceYear) {
		this.produceYear = produceYear;
	}

	public String getGenreStr() {
		return genreStr;
	}

	public void setGenreStr(String genreStr) {
		this.genreStr = genreStr;
	}

	public String getMovieType() {
		return movieType;
	}

	public void setMovieType(String movieType) {
		this.movieType = movieType;
	}

	public String getRunTime() {
		return runTime;
	}

	public void setRunTime(String runTime) {
		this.runTime = runTime;
	}

	public String getViewGrade() {
		return viewGrade;
	}

	public void setViewGrade(String viewGrade) {
		this.viewGrade = viewGrade;
	}

	public String getPostUrs() {
		return postUrs;
	}

	public void setPostUrs(String postUrs) {
		this.postUrs = postUrs;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public String getInvestor() {
		return investor;
	}

	public void setInvestor(String investor) {
		this.investor = investor;
	}

	public String getDistributor() {
		return distributor;
	}

	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}

	public String getTrustYnStr() {
		return trustYnStr;
	}

	public void setTrustYnStr(String trustYnStr) {
		this.trustYnStr = trustYnStr;
	}

	public int getAlbumId() {
		return albumId;
	}

	public void setAlbumId(int albumId) {
		this.albumId = albumId;
	}

	public int getNrId() {
		return nrId;
	}

	public void setNrId(int nrId) {
		this.nrId = nrId;
	}


}
