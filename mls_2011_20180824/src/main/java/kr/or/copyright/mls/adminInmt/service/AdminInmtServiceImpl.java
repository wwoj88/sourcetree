package kr.or.copyright.mls.adminInmt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.inmt.dao.InmtDao;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;

import kr.or.copyright.mls.adminInmt.dao.AdminInmtDao;
import kr.or.copyright.mls.adminInmt.service.AdminInmtService;

import com.tobesoft.platform.data.Dataset;

public class AdminInmtServiceImpl extends BaseService implements
		AdminInmtService {

	//private Log logger = LogFactory.getLog(getClass());

	private AdminInmtDao adminInmtDao;

	private InmtDao inmtDao;

	public void setAdminInmtDao(AdminInmtDao adminInmtDao) {
		this.adminInmtDao = adminInmtDao;
	}

	public void setInmtDao(InmtDao inmtDao) {
		this.inmtDao = inmtDao;
	}

	// adminInmtList 조회
	public void inmtList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");

		// ds_condition.printDataset();

		Map map = getMap(ds_condition, 0);

		map.put("GUBUN_TRST_ORGN_CODE", map.get("TRST_ORGN_CODE"));
		
		// DAO 호출		
		List list = (List) adminInmtDao.inmtList(map);
		List pageCount = (List) adminInmtDao.inmtListCount(map);

		addList("ds_list", list);
		addList("ds_page", pageCount);
	}

	// inmt 조회
	public void inmtSelect() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");

		Map map = getMap(ds_condition, 0);

		map.put("prpsSeqn", map.get("PRPS_SEQN"));

		// DAO 호출
		List prpsList = (List) inmtDao.inmtPrpsSelect(map); // ML_PRPS
		List rsltList = (List) inmtDao.inmtPrpsRsltList(map); // ML_PRPS_RSLT
		List kappList = null; // ML_KAPP_PRPS
		List fokapoList = null; // ML_FOKAPO_PRPS
		List krtraList = null; // ML_KRTRA_PRPS
		//List tempList = new ArrayList(); // 신청대상명
		List trstList = new ArrayList(); // 관련 신탁데이타

		// HashMap tempMap = new HashMap();
		// tempMap.put("SDSR_NAME",
		// ((HashMap)prpsList.get(0)).get("PRPS_IDNT_NAME")); // 대상 저작물 명
		// tempMap.put("INMT_SEQN",
		// ((HashMap)prpsList.get(0)).get("PRPS_IDNT")); // 대상 저작물 아이디
		// tempMap.put("PRPS_DIVS",
		// ((HashMap)prpsList.get(0)).get("PRPS_DIVS")); // 대상 저작물 구분값

		// tempList.add(tempMap);

		// 1. rsltList - trst_org_code 값에 따라 관련 테이블 데이타를 가져온다.

		String attFileYn = "";

		for (int rs = 0; rs < rsltList.size(); rs++) {

			HashMap hMap = (HashMap) rsltList.get(rs);

			// 음제협
			if (hMap.get("TRST_ORGN_CODE").equals("203")) {

				kappList = (List) inmtDao.inmtKappList(map);

				attFileYn = (String) ((HashMap) kappList.get(0))
						.get("ATTC_YSNO");

			}

			// 음실연
			else if (hMap.get("TRST_ORGN_CODE").equals("202")) {

				fokapoList = (List) inmtDao.inmtFokapoList(map);

				attFileYn = (String) ((HashMap) fokapoList.get(0))
						.get("ATTC_YSNO");
			}

			// 복제협

			else if (hMap.get("TRST_ORGN_CODE").equals("205")) {

				krtraList = (List) inmtDao.inmtKrtraList(map);

				attFileYn = (String) ((HashMap) krtraList.get(0))
						.get("ATTC_YSNO");
			}

			HashMap trMap = new HashMap();

			trMap.put("TRST_ORGN_CODE", hMap.get("TRST_ORGN_CODE")); // 분류코드값
																		// :
																		// 신탁기관
			trMap.put("DEAL_STAT", hMap.get("DEAL_STAT")); // 상태 값
			trMap.put("RSLT_DESC", hMap.get("RSLT_DESC")); // 처리결과
			trMap.put("TRST_ATTCHFILE_YN", attFileYn); // 파일첨부 YN 값

			trstList.add(rs, trMap);

		}

		addList("ds_list", prpsList);
		addList("ds_list_trst", trstList);
		addList("ds_list_kapp", kappList);
		addList("ds_list_fokapo", fokapoList);
		addList("ds_list_krtra", krtraList);
		// addList("ds_temp" , tempList);

	}

	// inmt 처리결과 조회
	public void inmtRsltSelect() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");

		// ds_condition.printDataset();

		Map conMap = getMap(ds_condition, 0);

		if (conMap.get("TRST_ORGN_CODE").equals("205_2"))
			conMap.put("TRST_ORGN_CODE", "205");

		// DAO 호출
		List rsltList = (List) adminInmtDao.inmtRsltList(conMap);

		addList("ds_list", rsltList);
	}

	// inmt 처리결과 저장
	public void inmtRsltUpdate() throws Exception {

		Dataset ds_condition = getDataset("ds_condition"); // 기본정보 리스트
		Dataset ds_list = getDataset("ds_list"); // MAIL, SMS 정보

		Dataset ds_listTrstGroup = getDataset("ds_listTrstGroup");

		Map conMap = getMap(ds_condition, 0);
		Map infoMap = getMap(ds_list, 0);

		// 기존 처리결과 등록정보있는지 Check -> 등록정보 없는 경우 update 한다.
		// conMap.put("TRST_ORGN_CODE", map.get("TRST_ORGN_CODE"));
		// List rsltList = (List)adminInmtDao.inmtRsltList(conMap);

		// map.put("rgstIdnt", ((HashMap)rsltList.get(0)).get("RGST_IDNT")); //
		// 결과등록자 정보 put

		// map.put("prpsSeqn", conMap.get("PRPS_SEQN"));

		int iCnt = 0;
		String userId = conMap.get("USER_ID").toString();

		for (int i = 0; i < ds_listTrstGroup.getRowCount(); i++) {

			Map trstGroupMap = getMap(ds_listTrstGroup, i);

			if (trstGroupMap.get("DEAL_STAT").equals("4"))
				iCnt++;

			trstGroupMap.put("userId", userId);

			trstGroupMap.put("rgstIdnt", trstGroupMap.get("RGST_IDNT"));

			if (trstGroupMap.get("ORGN_TRST_CODE").equals("205_2"))
				trstGroupMap.put("ORGN_TRST_CODE", "205");

			adminInmtDao.inmtRsltUpdate(trstGroupMap);
			
			// 분배여부 업데이트 한다. (20110107 추가)
			if (trstGroupMap.get("ALLT_YSNO")!=null){
				
				adminInmtDao.inmtUpdate(trstGroupMap);
				
			}
		}

		// 보상회원여부 update
		String inmtUserYsno = infoMap.get("INMT_USER_YSNO").toString();

		if (inmtUserYsno.equals("N") && iCnt > 0) {

			infoMap.put("inmtUserYsno", "Y");
			adminInmtDao.inmtUserYsnoUpdate(infoMap);
		}

		/*
		 * // 보상완료구분 기능 삭제. 20090620
		 * 
		 * map.put("prpsIdnt", conMap.get("PRPS_IDNT")); map.put("prpsDivs",
		 * conMap.get("PRPS_DIVS")); map.put("alltYsno",
		 * conMap.get("ALLT_YSNO"));
		 *  // 보상완료구분 UPDATE adminInmtDao.inmtUpdate(map);
		 */

		// 메일, SMS
		conMap.put("DEAL_STAT", "");
		int totalCnt = adminInmtDao.inmtRsltCnt(conMap); // 전체 결과 Cnt

		conMap.put("DEAL_STAT", "4");
		int doneCnt = adminInmtDao.inmtRsltCnt(conMap); // 완료 Cnt

		if ((totalCnt - doneCnt) == 0) {
			// if( map.get("DEAL_STAT").equals("4") ){

			// ----------------------------- mail
			// send------------------------------//

			String host = Constants.MAIL_SERVER_IP; // smtp서버
			String to = infoMap.get("U_MAIL").toString(); // 수신EMAIL
			String toName = infoMap.get("PRPS_NAME").toString(); // 수신인
			String subject = "[저작권찾기] 보상금신청 처리완료";
			String idntName = adminInmtDao.inmtPrpsTitle(conMap); // infoMap.get("PRPS_IDNT_NAME").toString();
																	// // 신청대상명

			String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표
															// 메일
			String fromName = Constants.SYSTEM_NAME;

			if (infoMap.get("U_MAIL_RECE_YSNO").equals("Y") && to != null
					&& to != "") {

				// ------------------------- mail 정보
				// ----------------------------------//
				MailInfo info = new MailInfo();
				info.setFrom(from);
				info.setFromName(fromName);
				info.setHost(host);
				info.setEmail(to);
				info.setEmailName(toName);
				info.setSubject(subject);

				StringBuffer sb = new StringBuffer();

				sb.append("<div class=\"mail_title\">" + toName
						+ "님, 보상금신청이 처리완료 되었습니다. </div>");
				sb.append("<div class=\"mail_contents\"> ");
				sb
						.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
				sb.append("		<tr> ");
				sb.append("			<td> ");
				sb
						.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
				// 변경부분
				sb.append("					<tr> ");
				sb.append("						<td class=\"tdLabel\">신청 저작물명</td> ");
				sb.append("						<td class=\"tdData\">" + idntName + "</td> ");
				sb.append("					</tr> ");
				// 변경부분
//				sb.append("					<tr> ");
//				sb.append("						<td class=\"tdLabel\">이메일 주소</td> ");
//				sb.append("						<td class=\"tdData\"><a href=\"mailto:"
//						+ Constants.SYSTEM_MAIL_ADDRESS
//						+ "\" onFocus='this.blur()'>"
//						+ Constants.SYSTEM_MAIL_ADDRESS + "</a></td> ");
//				sb.append("					</tr> ");
//				sb.append("					<tr> ");
//				sb.append("						<td class=\"tdLabel\">전화번호</td> ");
//				sb.append("						<td class=\"tdData\">"
//						+ Constants.SYSTEM_TELEPHONE + "</td> ");
//				sb.append("					</tr> ");
				sb.append("				</table> ");
				sb.append("			</td> ");
				sb.append("		</tr> ");
				sb.append("	</table> ");
				sb.append("</div> ");

				info.setMessage(new String(MailMessage.getChangeInfoMailText(sb
						.toString())));
				MailManager manager = MailManager.getInstance();

				info = manager.sendMail(info);
				if (info.isSuccess()) {
					System.out.println(">>>>>>>>>>>> message success :"
							+ info.getEmail());
				} else {
					System.out.println(">>>>>>>>>>>> message false   :"
							+ info.getEmail());
				}
			}

			String smsTo = infoMap.get("U_MOBL_PHON").toString();
			String smsFrom = Constants.SYSTEM_TELEPHONE;

			String smsMessage = "[저작권찾기] " + idntName
					+ " - 보상금신청 처리완료 되었습니다.";

			if (infoMap.get("U_SMS_RECE_YSNO").equals("Y") && smsTo != null
					&& smsTo != "") {

				SMSManager smsManager = new SMSManager();

				System.out.println("return = "
						+ smsManager.sendSMS(smsTo, smsFrom, smsMessage));
			}

		} // end email, sms

		// 상태처리 후 상태값 구한다.
		List dealStat = (List) adminInmtDao.inmtDealStat(conMap);

		addList("ds_dealStat", dealStat);
	}
	
	// adminInmtList 조회
	public void admInmtList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");

		// ds_condition.printDataset();

		Map map = getMap(ds_condition, 0);

		map.put("GUBUN_TRST_ORGN_CODE", map.get("TRST_ORGN_CODE"));
		
		// DAO 호출		
		List list = (List) adminInmtDao.admInmtList(map);

		addList("ds_list_prps", list);
	}
	
	// adminInmtList 조회
	public void admInmtList2() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");

		// ds_condition.printDataset();

		Map map = getMap(ds_condition, 0);

		map.put("GUBUN_TRST_ORGN_CODE", map.get("TRST_ORGN_CODE"));
		
		// DAO 호출		
		List list = (List) adminInmtDao.admInmtList(map);

		addList("ds_list_inmt", list);
	}
}
