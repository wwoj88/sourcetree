package kr.or.copyright.mls.console.event;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.event.inter.FdcrAdA0Dao;
import kr.or.copyright.mls.console.event.inter.FdcrAdA2Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdA2Service" )
public class FdcrAdA2ServiceImpl extends CommandService implements FdcrAdA2Service{

	@Resource( name = "fdcrAdA0Dao" )
	private FdcrAdA0Dao fdcrAdA0Dao;

	/**
	 * 게시물 질문조회 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA2List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA0Dao.getEventQnaList( commandMap );
		List pageList = (List) fdcrAdA0Dao.getEventQnaTotal( commandMap );

		commandMap.put( "ds_List", list );
		commandMap.put( "ds_page", pageList );

	}

	/**
	 * 게시판 사용여부 체크
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA2List2( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA0Dao.getEventQnaMenuOpen();

		commandMap.put( "ds_condition", list );

	}

	/**
	 * 게시판 사용여부 수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA2Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAdA0Dao.delEventQnaMenuOpen( commandMap );
			fdcrAdA0Dao.addEventQnaMenuOpen( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 게시물 상세조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA2View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA0Dao.getEventQnaDetail( commandMap );

		commandMap.put( "ds_list", list );

	}

	/**
	 * 게시물 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA2Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAdA0Dao.delEventQnaDetail( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 게시글 답변 등록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA2Regi1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAdA0Dao.uptEventQnaAnswRegi( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
