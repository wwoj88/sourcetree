package kr.or.copyright.mls.image.model;


public class ImageDTO{

	//ML_COMM_IMAGE & ML_COMM_ART
	private int works_id;
	private String writer;
	private String detl_url;
	private String rgst_idnt;
	private String rgst_dttn;
	private String modi_idnt;
	private String modi_dttm;


	//ML_COMM_IMAGE
	private String image_desc;
	private String keyword;
	private String producer;
	private String image_open_yn;
	private String image_url;
	
	//ML_COMM_ART
	private String kind;
	private String make_date;
	private String size_info;
	private String img_url;
	private String resolution;
	private String main_mtrl;
	private String txtr;
	private String poss_date;
	private String poss_orgn_name;

	//ML_COMM_WORKS
	private int genre_cd;
	private String nation_cd;
	private String works_title;
	private String works_sub_title;
	private String crt_year;
	
	public int getWorks_id(){
		return works_id;
	}
	
	public void setWorks_id( int works_id ){
		this.works_id = works_id;
	}
	
	public String getImage_desc(){
		return image_desc;
	}
	
	public void setImage_desc( String image_desc ){
		this.image_desc = image_desc;
	}
	
	public String getKeyword(){
		return keyword;
	}
	
	public void setKeyword( String keyword ){
		this.keyword = keyword;
	}
	
	public String getWriter(){
		return writer;
	}
	
	public void setWriter( String writer ){
		this.writer = writer;
	}
	
	public String getProducer(){
		return producer;
	}
	
	public void setProducer( String producer ){
		this.producer = producer;
	}
	
	public String getImage_url(){
		return image_url;
	}
	
	public void setImage_url( String image_url ){
		this.image_url = image_url;
	}
	
	public String getImage_open_yn(){
		return image_open_yn;
	}
	
	public void setImage_open_yn( String image_open_yn ){
		this.image_open_yn = image_open_yn;
	}
	
	public String getRgst_idnt(){
		return rgst_idnt;
	}
	
	public void setRgst_idnt( String rgst_idnt ){
		this.rgst_idnt = rgst_idnt;
	}
	
	public String getRgst_dttn(){
		return rgst_dttn;
	}
	
	public void setRgst_dttn( String rgst_dttn ){
		this.rgst_dttn = rgst_dttn;
	}
	
	public String getModi_idnt(){
		return modi_idnt;
	}
	
	public void setModi_idnt( String modi_idnt ){
		this.modi_idnt = modi_idnt;
	}
	
	public String getModi_dttm(){
		return modi_dttm;
	}
	
	public void setModi_dttm( String modi_dttm ){
		this.modi_dttm = modi_dttm;
	}
	//ML_COMM_WORKS
	
	public int getGenre_cd(){
		return genre_cd;
	}

	
	public void setGenre_cd( int genre_cd ){
		this.genre_cd = genre_cd;
	}

	
	public String getNation_cd(){
		return nation_cd;
	}

	
	public void setNation_cd( String nation_cd ){
		this.nation_cd = nation_cd;
	}

	
	public String getWorks_title(){
		return works_title;
	}

	
	public void setWorks_title( String works_title ){
		this.works_title = works_title;
	}

	
	public String getWorks_sub_title(){
		return works_sub_title;
	}

	
	public void setWorks_sub_title( String works_sub_title ){
		this.works_sub_title = works_sub_title;
	}

	
	public String getCrt_year(){
		return crt_year;
	}

	
	public void setCrt_year( String crt_year ){
		this.crt_year = crt_year;
	}

	
	public String getDetl_url(){
		return detl_url;
	}

	
	public void setDetl_url( String detl_url ){
		this.detl_url = detl_url;
	}

	
	public String getKind(){
		return kind;
	}

	
	public void setKind( String kind ){
		this.kind = kind;
	}

	
	public String getMake_date(){
		return make_date;
	}

	
	public void setMake_date( String make_date ){
		this.make_date = make_date;
	}

	
	public String getSize_info(){
		return size_info;
	}

	
	public void setSize_info( String size_info ){
		this.size_info = size_info;
	}

	
	public String getImg_url(){
		return img_url;
	}

	
	public void setImg_url( String img_url ){
		this.img_url = img_url;
	}

	
	public String getResolution(){
		return resolution;
	}

	
	public void setResolution( String resolution ){
		this.resolution = resolution;
	}

	
	public String getMain_mtrl(){
		return main_mtrl;
	}

	
	public void setMain_mtrl( String main_mtrl ){
		this.main_mtrl = main_mtrl;
	}

	
	public String getTxtr(){
		return txtr;
	}

	
	public void setTxtr( String txtr ){
		this.txtr = txtr;
	}

	
	public String getPoss_date(){
		return poss_date;
	}

	
	public void setPoss_date( String poss_date ){
		this.poss_date = poss_date;
	}

	
	public String getPoss_orgn_name(){
		return poss_orgn_name;
	}

	
	public void setPoss_orgn_name( String poss_orgn_name ){
		this.poss_orgn_name = poss_orgn_name;
	}

	//toString
	@Override
	public String toString(){
		return "ImageDTO [works_id=" + works_id + ", writer=" + writer + ", detl_url=" + detl_url + ", rgst_idnt="
			+ rgst_idnt + ", rgst_dttn=" + rgst_dttn + ", modi_idnt=" + modi_idnt + ", modi_dttm=" + modi_dttm
			+ ", image_desc=" + image_desc + ", keyword=" + keyword + ", producer=" + producer + ", image_open_yn="
			+ image_open_yn + ", image_url=" + image_url + ", kind=" + kind + ", make_date=" + make_date
			+ ", size_info=" + size_info + ", img_url=" + img_url + ", resolution=" + resolution + ", main_mtrl="
			+ main_mtrl + ", txtr=" + txtr + ", poss_date=" + poss_date + ", poss_orgn_name=" + poss_orgn_name
			+ ", genre_cd=" + genre_cd + ", nation_cd=" + nation_cd + ", works_title=" + works_title
			+ ", works_sub_title=" + works_sub_title + ", crt_year=" + crt_year + "]";
	}
}
