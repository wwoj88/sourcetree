package kr.or.copyright.mls.ajaxTest.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.ajaxTest.model.Code;
import kr.or.copyright.mls.user.model.ZipCodeRoad;

public interface CodeListDAO {
	public List<Code> getInitCodeList();
	
	public List<Code> getCodeList(Map parms);
	
	public List<ZipCodeRoad> getRoadCodeList(Map params);
	
	public List<ZipCodeRoad> getInitRoadCodeList();
	
}
