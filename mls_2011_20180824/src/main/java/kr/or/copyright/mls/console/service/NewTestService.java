package kr.or.copyright.mls.console.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.dao.NewTestDao;

import org.springframework.stereotype.Service;

@Service( "newTestService" )
public class NewTestService extends CommandService{
	
	
	@Resource( name = "newTestDao" )
	private NewTestDao newTestDao;
	

	public Map<String,Object> statBord06List(Map<String,Object> commandMap) throws Exception {
		
		//DAOȣ��
		List list = (List)newTestDao.statBord06List(commandMap);
		commandMap.put("ds_list", list);	
		return commandMap;
	}
}