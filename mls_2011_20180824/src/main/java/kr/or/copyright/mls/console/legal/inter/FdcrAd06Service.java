package kr.or.copyright.mls.console.legal.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface FdcrAd06Service{

	/**
	 * 법정허락 이용승인 신청 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> fdcrAd06List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인 신청 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> fdcrAd06View1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 법정허락 이용승인 첨부파일 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> selectAttachFileList( Map<String, Object> commandMap ) throws SQLException;
	
	/**
	 * 법정허락 이용승인 첨부파일 정보
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> selectAttachFileInfo( Map<String, Object> commandMap ) throws SQLException;
	
	/**
	 * 법정허락 이용승인 명세서 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> adminStatApplyWorksSelect( Map<String, Object> commandMap ) throws SQLException;
	
	/**
	 * 법정허락 이용승인 명세서 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> adminStatApplyWorksSelectInfo( Map<String, Object> commandMap )throws SQLException;

	/**
	 * 법정허락 이용승인 신청서 수정 폼
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd06UpdateForm1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인 신청서 수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd06Update1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인 신청서 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd06Delete1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인 진행상태 내역조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd06History1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인 진행상태 수정
	 * 
	 * @param commandMap
	 * @param fileinfo 
	 * @throws SQLException
	 */
	public boolean fdcrAd06StatChange1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList) throws Exception;

	/**
	 * 법정허락 이용승인 심의결과 내역조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd06History2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인 심의결과 상태 수정
	 * 
	 * @param commandMap
	 * @throws SQLException
	 */
	public boolean fdcrAd06StatWorksChange1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 법정허락 이용승인신청공고 게시판 등록 팝업 폼
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd06RegiForm1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인신청공고 게시판 등록
	 * 
	 * @param commandMap
	 * @throws SQLException
	 */
	public boolean fdcrAd06Regi1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 법정허락 이용승인신청공고 게시판 수정
	 * 
	 * @param commandMap
	 * @throws SQLException
	 */
	public boolean fdcrAd06Modi1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인공고 게시판 등록 팝업 폼
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd06RegiForm2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인공고 게시판 등록
	 * 
	 * @param commandMap
	 * @throws SQLException
	 */
	public boolean fdcrAd06Regi2( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 법정허락 이용승인공고 게시판 수정
	 * 
	 * @param commandMap
	 * @throws SQLException
	 */
	public boolean fdcrAd06Modi2( Map<String, Object> commandMap ) throws Exception;

}
