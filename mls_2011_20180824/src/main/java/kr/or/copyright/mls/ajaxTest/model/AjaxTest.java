package kr.or.copyright.mls.ajaxTest.model;

public class AjaxTest {
	private String superdeptid;
	
	private String departmentid;
	
	private String searchName;
	

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getDepartmentid() {
		return departmentid;
	}

	public void setDepartmentid(String departmentid) {
		this.departmentid = departmentid;
	}

	public String getSuperdeptid() {
		return superdeptid;
	}

	public void setSuperdeptid(String superdeptid) {
		this.superdeptid = superdeptid;
	}
}
