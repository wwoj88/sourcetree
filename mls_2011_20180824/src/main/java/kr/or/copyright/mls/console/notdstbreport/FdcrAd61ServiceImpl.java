package kr.or.copyright.mls.console.notdstbreport;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.notdstbreport.inter.FdcrAd61Service;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd20Dao;

import org.springframework.stereotype.Service;

@Service( "fdcrAd61Service" )
public class FdcrAd61ServiceImpl extends CommandService implements FdcrAd61Service{

	@Resource( name = "fdcrAd20Dao" )
	private FdcrAd20Dao fdcrAd20Dao;

	/**
	 * 등록현황 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd61List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd20Dao.inmtMgntReptView( commandMap );

		commandMap.put( "ds_List", list );

	}

}
