package kr.or.copyright.mls.console.event;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.event.inter.FdcrAdA0Dao;
import kr.or.copyright.mls.console.event.inter.FdcrAdA3Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdA3Service" )
public class FdcrAdA3ServiceImpl extends CommandService implements FdcrAdA3Service{

	@Resource( name = "fdcrAdA0Dao" )
	private FdcrAdA0Dao fdcrAdA0Dao;

	/**
	 * 이벤트 현황조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA3List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA0Dao.getEventStatList();

		commandMap.put( "ds_list", list );

	}

}
