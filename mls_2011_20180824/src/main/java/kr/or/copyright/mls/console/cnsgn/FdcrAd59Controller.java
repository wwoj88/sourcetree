package kr.or.copyright.mls.console.cnsgn;

import java.net.URLDecoder;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.cnsgn.inter.FdcrAd59Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 위탁관리저작물 보고 > 보고현황 > 보고현황조회
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd59Controller extends DefaultController {

     @Resource(name = "fdcrAd59Service")
     private FdcrAd59Service fdcrAd59Service;

     private static final Logger logger = LoggerFactory.getLogger(FdcrAd59Controller.class);

     /**
      * 조회
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/cnsgn/fdcrAd59List1.page")
     public String fdcrAd59List1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd59List1 Start");
          // 파라미터 셋팅
          /*
           * String YYYY = EgovWebUtil.getString( commandMap, "YYYY" ); String YYYYMM = EgovWebUtil.getString(
           * commandMap, "YYYYMM" );
           * 
           * if(YYYY.equals( "" ) || YYYY == null){ commandMap.put( "DIV", "10" ); }else{ //commandMap.put(
           * "DIV", "20" ); }
           * 
           * if(YYYYMM.equals( "" ) || YYYYMM == null){ YYYYMM = ""; } //TODO 기관코드 주석 commandMap.put(
           * "TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode() ); //commandMap.put( "TRST_ORGN_CODE",
           * "202");
           * 
           * commandMap.put( "DIV", "10" ); commandMap.put( "YYYY", YYYY ); commandMap.put( "YYYYMM", YYYYMM
           * );
           */
          String YYYY = EgovWebUtil.getString(commandMap, "YYYY");
          if (YYYY.equals("") || YYYY == null || "".equals(YYYY)) {
               model.addAttribute("REPT_YYYY", YYYY);
               model.addAttribute("ds_list", commandMap.get("ds_list"));
               model.addAttribute("ds_list_0", commandMap.get("ds_list_0"));
               model.addAttribute("commandMap", commandMap);
               logger.debug("fdcrAd59List1 End");
               return "cnsgn/fdcrAd59List1.tiles";
          } else {
               commandMap.put("REPT_YYYY", YYYY);


               // TODO 기관코드 주석
               commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
               // commandMap.put( "TRST_ORGN_CODE", "202");

               commandMap.put("DIV", "10");

               fdcrAd59Service.fdcrAd59List1(commandMap);
               model.addAttribute("REPT_YYYY", YYYY);
               model.addAttribute("ds_list", commandMap.get("ds_list"));
               model.addAttribute("ds_list_0", commandMap.get("ds_list_0"));
               model.addAttribute("commandMap", commandMap);
               logger.debug("fdcrAd59List1 End");
               return "cnsgn/fdcrAd59List1.tiles";
          }
     }

     /**
      * 월별 보고현황 조회
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/cnsgn/fdcrAd59List2.page")
     public String fdcrAd59List2(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd59List2 Start");
          System.out.println("fdcrAd59List2 Start");
          // 파라미터 셋팅
          String YYYYMM = EgovWebUtil.getString(commandMap, "YYYYMM");
          commandMap.put("DIV", "20");
          String YYYYMM2 = EgovWebUtil.getString(commandMap, "YYYYMM2");
          // 월별 보고정보 목록 파라미터
          String GUBUN = EgovWebUtil.getString(commandMap, "SCH_GUBUN");
          String TITLE = EgovWebUtil.getString(commandMap, "SCH_TITLE");
          if (!"".equals(TITLE)) {
               TITLE = URLDecoder.decode(TITLE, "UTF-8");
               commandMap.put("TITLE", TITLE);
          }

          commandMap.put("GUBUN", GUBUN);
          commandMap.put("TITLE", TITLE);
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          // commandMap.put( "TRST_ORGN_CODE", "201" );//임시 ↑이걸로 해야함.
          commandMap.put("YYYYMM", YYYYMM);
          System.out.println("YYYYMM  :" + YYYYMM2);
          System.out.println("YYYYMM2  :" + YYYYMM2);
          if (YYYYMM2 == null || YYYYMM2.equals("")) {
               commandMap.put("YYYYMM2", YYYYMM);
          }
          System.out.println("YYYYMM2  :" + commandMap.get("YYYYMM2"));
          fdcrAd59Service.fdcrAd59List2(commandMap);
          model.addAttribute("ds_count", commandMap.get("ds_count"));
          model.addAttribute("ds_report", commandMap.get("ds_report"));
          model.addAttribute("ds_list", commandMap.get("ds_list"));
          model.addAttribute("ds_page", commandMap.get("ds_page"));
          model.addAttribute("commandMap", commandMap);
          model.addAttribute("paginationInfo", commandMap.get("paginationInfo"));

          logger.debug("fdcrAd59List2 End");
          return "cnsgn/fdcrAd59List2.tiles";
     }

     /**
      * 엑셀다운로드
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/cnsgn/fdcrAd59Down1.page")
     public String fdcrAd16Down1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          System.out.println("fdcrAd59Down1 Start");
          logger.debug("fdcrAd59Down1 Start");
          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          // String TITLE = EgovWebUtil.getString( commandMap, "TITLE" );
          String TITLE = request.getParameter("TITLE");
          if (!"".equals(TITLE)) {
               TITLE = URLDecoder.decode(TITLE, "UTF-8");
               commandMap.put("TITLE", TITLE);
          }
          String YYYYMM = EgovWebUtil.getString(commandMap, "YYYYMM");
          // String GUBUN = EgovWebUtil.getString( commandMap, "GUBUN" );
          String GUBUN = request.getParameter("GUBUN");

          commandMap.put("YYYYMM", YYYYMM);
          commandMap.put("GUBUN", GUBUN);
          System.out.println("YYYYMM:" + YYYYMM + "//GUBUN:" + GUBUN + "//TITLE:" + TITLE);

          fdcrAd59Service.fdcrAd59List3(commandMap);
          model.addAttribute("ds_count", commandMap.get("ds_count"));
          model.addAttribute("ds_report", commandMap.get("ds_report"));
          model.addAttribute("ds_list", commandMap.get("ds_list"));
          model.addAttribute("ds_page", commandMap.get("ds_page"));
          model.addAttribute("commandMap", commandMap);
          model.addAttribute("paginationInfo", commandMap.get("paginationInfo"));

          logger.debug("fdcrAd59Down1 End");
          return "cnsgn/fdcrAd59Down1";
     }
}
