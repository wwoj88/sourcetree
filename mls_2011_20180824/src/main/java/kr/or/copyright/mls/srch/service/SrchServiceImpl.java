package kr.or.copyright.mls.srch.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.srch.dao.SrchDao;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;


@Service("SrchService")
public class SrchServiceImpl extends BaseService implements SrchService {
	private Logger log = Logger.getLogger(this.getClass());
	@Resource(name="SrchDao")
	//private SrchDao dao;
	
	private SrchDao srchDao;
	
	public void setSrchDao(SrchDao srchDao){
		this.srchDao = srchDao;
	}
	
	public List codeList(String srchDivs){
		return srchDao.codeList(srchDivs);
	}
	public List findStatWorks(Map params){
		return srchDao.findStatWorks(params);
	}
	public int countStatWorks(Map params){
		return srchDao.countStatWorks(params);
	}
	public List findStatDetl(Map params){
		return srchDao.findStatDetl(params);
	}
	public List getCoptHodr(Map params){
		return srchDao.getCoptHodr(params);
	}
	
	public List findCommWorks(Map params){
		return srchDao.findCommWorks(params);
	}
	public List findCommDetl(Map params){
		return srchDao.findCommDetl(params);
	}
	public int countCommWorks(Map params){
		return srchDao.countCommWorks(params);
	}
	
	public List findCrosWorks(Map params){
		return srchDao.findCrosWorks(params);
	}
	public int countCrosWorks(Map params){
		return srchDao.countCrosWorks(params);
	}
	
	public List findCertWorks(Map params){
		return srchDao.findCertWorks(params);
		
	}
	public int countFindCertWorks(Map params){
		return srchDao.countFindCertWorks(params);
		
	}
	
	//검색어 조회 이력 추가 150923
	public void insertSrch(String worksTitle) throws Exception{
	//Dataset ds_condition = getDataset("ds_condition");
		srchDao.insertSrch(worksTitle);
	}

	public List getSearchWordList() throws Exception{
		return srchDao.getSearchWordList();
	}
}