package kr.or.copyright.mls.mail.model;

public class Mail {
	
	private String mail_rejc_cd;	// 수신거부코드
	private String mail_addr;		// 메일주소
	private String send_msg_id;		//수신확인코드
	
	
	public String getMail_addr() {
		return mail_addr;
	}
	public void setMail_addr(String mail_addr) {
		this.mail_addr = mail_addr;
	}
	public String getMail_rejc_cd() {
		return mail_rejc_cd;
	}
	public void setMail_rejc_cd(String mail_rejc_cd) {
		this.mail_rejc_cd = mail_rejc_cd;
	}	
	public String getSend_msg_id() {
		return send_msg_id;
	}
	public void setSend_msg_id(String send_msg_id) {
		this.send_msg_id = send_msg_id;
	}
}
