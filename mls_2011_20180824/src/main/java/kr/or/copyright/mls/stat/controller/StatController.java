package kr.or.copyright.mls.stat.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.or.copyright.mls.support.util.StringUtil;
import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.common.mail.SendMail;
import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.common.service.CodeListService;
import kr.or.copyright.mls.common.service.CommonService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.myStat.model.ApplySign;
import kr.or.copyright.mls.myStat.model.File;
import kr.or.copyright.mls.myStat.model.StatApplication;
import kr.or.copyright.mls.myStat.model.StatApplyWorks;
import kr.or.copyright.mls.myStat.service.MyStatService;
import kr.or.copyright.mls.rghtPrps.model.PrpsAttc;
import kr.or.copyright.mls.stat.model.MlStatWorksDefaultVO;
import kr.or.copyright.mls.stat.model.MlStatWorksVO;
import kr.or.copyright.mls.stat.service.StatService;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.constant.SignContants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.dto.SignDTO;
import kr.or.copyright.mls.support.util.FileUploadUtil;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;
import kr.or.copyright.mls.support.util.SessionUtil;
import kr.or.copyright.mls.support.util.SignCertUtil;
import kr.or.copyright.mls.user.service.UserService;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;

import com.tagfree.util.URLEncoder;

import kr.or.copyright.mls.common.common.utils.XssUtil;

import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@RequestMapping( "/stat" )
@Controller
public class StatController{

	private Logger log = Logger.getLogger( this.getClass() );

	@Resource( name = "StatService" )
	private StatService statService;

	@Resource( name = "codeListService" )
	private CodeListService codeListService;

	@Resource( name = "userService" )
	private UserService userService;

	@Resource( name = "myStatService" )
	private MyStatService myStatService;

	@Resource( name = "commonService" )
	private CommonService commonService;

	private String realUploadPath = FileUtil.uploadPath();

	CodeList code = new CodeList();

	@ModelAttribute( "genreList" )
	private List<CodeList> genreList(){
		code.setBigCode( "35" );
		return codeListService.commonCodeList( code );
	}

	@ModelAttribute( "worksDivsList" )
	private List<CodeList> worksDivsList(){
		code.setBigCode( "57" );
		return codeListService.commonCodeList( code );
	}

	@ModelAttribute( "applyTypeList" )
	private List<CodeList> applyTypeList(){
		code.setBigCode( "50" );
		return codeListService.commonCodeList( code );
	}

	@ModelAttribute( "publMediList" )
	private List<CodeList> publMediList(){
		code.setBigCode( "52" );
		return codeListService.commonCodeList( code );
	}

	@RequestMapping( value = "/statSrch" )
	public String selectMlStatWorksListView( @ModelAttribute( "mlStatWorksVO" ) MlStatWorksVO mlStatWorksVO,
		ModelMap model ) throws Exception{
		model.addAttribute( "mlStatWorksVO", mlStatWorksVO );
		return "stat/statBo02List";
	}

	@RequestMapping( value = "/subStatList" )
	public String selectSubMlStatWorksListView(
		@RequestParam( value = "pageNo", required = true, defaultValue = "1" ) int pageNo,
		@ModelAttribute( "mlStatWorksVO" ) MlStatWorksVO mlStatWorksVO,
		HttpServletRequest request,
		ModelMap model ) throws Exception{
		/** paging */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo( pageNo );
		paginationInfo.setRecordCountPerPage( mlStatWorksVO.getPageUnit() );
		paginationInfo.setPageSize( mlStatWorksVO.getPageSize() );

		mlStatWorksVO.setFirstIndex( paginationInfo.getFirstRecordIndex() );
		mlStatWorksVO.setLastIndex( paginationInfo.getLastRecordIndex() );
		mlStatWorksVO.setRecordCountPerPage( 10 );

		model.addAttribute( "mlStatWorksList", statService.selectMlStatWorksList( mlStatWorksVO ) );
		model.addAttribute( "mlStatWorksVO", mlStatWorksVO );
		int totCnt = statService.selectMlStatWorksListTotCnt( mlStatWorksVO );
		paginationInfo.setTotalRecordCount( totCnt );
		model.addAttribute( "paginationInfo", paginationInfo );

		// HttpSession session = request.getSession();

		// session.setAttribute("isCompleteStep01", "Y");

		// String isCompleteStep01 = (String)session.getAttribute("isCompleteStep01");

		return "stat/subStatBo02List";
	}

	// 법정허락 신청정보 등록 step1
	@RequestMapping( value = "/statPrpsMain" )
	public ModelAndView statPrps( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		HttpSession session = request.getSession();

		session.removeAttribute( "isCompleteStep01" );
		session.removeAttribute( "isCompleteStep01_1" );
		session.removeAttribute( "isCompleteStep01_2" );
		session.removeAttribute( "isCompleteStep02" );
		session.removeAttribute( "applyWriteYmd" );
		session.removeAttribute( "applyWriteSeq" );

		String isModi = ServletRequestUtils.getStringParameter( request, "isModi", "01" );

		String isMy = ServletRequestUtils.getStringParameter( request, "isMy", "" );

		MlStatWorksVO srchVO = new MlStatWorksVO();
		StatApplication srchParam = new StatApplication();

		Map params = new HashMap();
		User user = SessionUtil.getSession( request );
		params.put( "userIdnt", user.getUserIdnt() );

		// 사용자 조회
		Map userInfo = userService.selectUserInfo( params );
		//Map userInfo = null;

		String myWorksId = ServletRequestUtils.getStringParameter( request, "worksId" );
		if( isMy.equals( "" ) ){
			/* 조회조건 str */
			srchVO.setPageNum( ServletRequestUtils.getStringParameter( request, "pageNo" ) );
			srchVO.setSearchCondition( ServletRequestUtils.getStringParameter( request, "searchCondition" ) );
			srchVO.setSearchKeyword1( ServletRequestUtils.getStringParameter( request, "searchKeyword1" ) );
			srchVO.setSearchWorksId( myWorksId );
			/* 조회조건 end */
		}else{
			/* 조회조건 str */
			srchParam.setSrchApplyType( ServletRequestUtils.getStringParameter( request, "srchApplyType" ) );
			srchParam.setSrchApplyFrDt( ServletRequestUtils.getStringParameter( request, "srchApplyFrDt" ) );
			srchParam.setSrchApplyToDt( ServletRequestUtils.getStringParameter( request, "srchApplyToDt" ) );
			srchParam.setSrchApplyWorksTitl( ServletRequestUtils.getStringParameter( request, "srchApplyWorksTitl" ) );
			srchParam.setSrchStatCd( ServletRequestUtils.getIntParameter( request, "srchStatCd", 0 ) );
			srchParam.setNowPage( ServletRequestUtils.getIntParameter( request, "page_no", 1 ) );
			srchParam.setSrchRgstIdnt( user.getUserIdnt() );
			/* 조회조건 end */
		}

		ModelAndView mv = new ModelAndView( "stat/statPrpsMain" );
		if( isModi.equals( "01" ) ){// 등록화면
			String actionDiv = ServletRequestUtils.getStringParameter( request, "action_div" );

			if( actionDiv == null || actionDiv == "" ){
				actionDiv = "new";
			}

			mv = new ModelAndView( "stat/statPrpsMain" );

			if( srchVO.getSearchWorksId() == null ){
				srchVO.setSearchWorksId( "" );
			}

			boolean isEmpty = srchVO.getSearchWorksId().equals( "" );

			MlStatWorksDefaultVO defaultVO = new MlStatWorksDefaultVO();
			defaultVO.setSearchWorksId( myWorksId );
			if( defaultVO.getSearchWorksId() == null ){
				defaultVO.setSearchWorksId( "" );
			}

			boolean isEmpty1 = defaultVO.getSearchWorksId().equals( "" );

			if( isMy.equals( "" ) ){
				if( !isEmpty ){
					mv.addObject( "mlStatWorksList", statService.checkSelectMlStatWorksList( srchVO ) );
				}
			}else{
				if( !isEmpty1 ){
					mv.addObject( "mlStatWorksList", statService.checkSelectMlStatWorksList( defaultVO ) );
				}
			}
			mv.addObject( "isModi", isModi );
			mv.addObject( "userInfo", userInfo );
			mv.addObject( "srchVO", srchVO );
		}else{// 수정화면
			StatApplication param = new StatApplication();
			param.setApplyWriteYmd( ServletRequestUtils.getStringParameter( request, "apply_write_ymd" ) );
			param.setApplyWriteSeq( ServletRequestUtils.getIntParameter( request, "apply_write_seq", 0 ) );

			// 이용승인 신청서tmp 조회
			int statCd = ServletRequestUtils.getIntParameter( request, "stat_cd" );
			StatApplication statApplication = null;
			if( statCd == 1 ){
				statApplication = myStatService.statApplicationTmp( param );
			}else{
				statApplication = myStatService.statApplication( param );
			}

			// 이용승인 신청서tmp 첨부파일 조회
			List fileList = null;
			if( statCd == 1 ){
				fileList = myStatService.statAttcFileTmp( param );
			}else{
				fileList = myStatService.statAttcFile( param );
			}
			statApplication.setFileList( fileList );

			mv = new ModelAndView( "stat/statPrpsMain" );

			boolean isEmpty = srchVO.getSearchWorksId().equals( "" );

			MlStatWorksDefaultVO defaultVO = new MlStatWorksDefaultVO();
			defaultVO.setSearchWorksId( myWorksId );
			if( defaultVO.getSearchWorksId() == null ){
				defaultVO.setSearchWorksId( "" );
			}

			boolean isEmpty1 = defaultVO.getSearchWorksId().equals( "" );

			if( isMy.equals( "" ) ){
				if( !isEmpty ){
					mv.addObject( "mlStatWorksList", statService.checkSelectMlStatWorksList( srchVO ) );
				}
			}else{
				if( !isEmpty1 ){
					mv.addObject( "mlStatWorksList", statService.checkSelectMlStatWorksList( defaultVO ) );
				}
			}
			mv.addObject( "isModi", isModi );
			mv.addObject( "statApplication", statApplication );
			mv.addObject( "userInfo", userInfo );
			mv.addObject( "srchVO", srchVO );
			mv.addObject( "isMy", isMy );
			if( isMy.equals( "Y" ) ){
				mv.addObject( "worksId", myWorksId );
				mv.addObject( "srchParam", srchParam );
			}
		}
		return mv;
	}

	// 법정허락 신청 step1 대상저작물 추가검색
	@RequestMapping( value = "/statPopSrchList" )
	public String statPopSrchList( @RequestParam( value = "pageNo", required = true, defaultValue = "1" ) int pageNo,
		@ModelAttribute( "mlStatWorksVO" ) MlStatWorksVO mlStatWorksVO,
		ModelMap model ) throws Exception{

		/** paging */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo( pageNo );
		paginationInfo.setRecordCountPerPage( mlStatWorksVO.getPageUnit() );
		paginationInfo.setPageSize( mlStatWorksVO.getPageSize() );

		mlStatWorksVO.setFirstIndex( paginationInfo.getFirstRecordIndex() );
		mlStatWorksVO.setLastIndex( paginationInfo.getLastRecordIndex() );
		mlStatWorksVO.setRecordCountPerPage( 10 );

		model.addAttribute( "mlStatWorksList", statService.selectMlStatWorksList( mlStatWorksVO ) );
		model.addAttribute( "mlStatWorksVO", mlStatWorksVO );
		int totCnt = statService.selectMlStatWorksListTotCnt( mlStatWorksVO );
		paginationInfo.setTotalRecordCount( totCnt );
		model.addAttribute( "paginationInfo", paginationInfo );

		return "stat/statPopSrchList";
	}

	// 법정허락 신청 step1 임시저장
	@RequestMapping( value = "/statPrpsList", method = RequestMethod.POST )
	public ModelAndView tmpStatPrps1SaveDo( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		HttpSession session = request.getSession();

		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		//MultipartHttpServletRequest multipartRequest;
		//multipartRequest = multipartResolver.resolveMultipart( request );
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		StatApplication srchParam = new StatApplication();

		MlStatWorksVO srchVO = new MlStatWorksVO();

		String isMy = ServletRequestUtils.getStringParameter( multipartRequest, "isMy", "" );

		String statCd = ServletRequestUtils.getStringParameter( multipartRequest, "statCd", "1" );

		Map params = new HashMap();
		User user = SessionUtil.getSession( request );
		params.put( "userIdnt", user.getUserIdnt() );
		// 사용자 조회
		Map userInfo = userService.selectUserInfo( params );

		String myWorksId = ServletRequestUtils.getStringParameter( multipartRequest, "worksId" );

		/* 조회조건 str */
		if( isMy.equals( "" ) ){
			srchVO.setPageNum( ServletRequestUtils.getStringParameter( multipartRequest, "pageNo" ) );
			srchVO.setSearchCondition( ServletRequestUtils.getStringParameter( multipartRequest, "searchCondition" ) );
			srchVO.setSearchKeyword1( ServletRequestUtils.getStringParameter( multipartRequest, "searchKeyword1" ) );
			srchVO.setSearchWorksId( ServletRequestUtils.getStringParameter( multipartRequest, "worksId" ) );
			/* 조회조건 end */
		}else{
			/* 조회조건 str */
			srchParam.setSrchApplyType( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyType" ) );
			srchParam.setSrchApplyFrDt( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyFrDt" ) );
			srchParam.setSrchApplyToDt( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyToDt" ) );
			srchParam.setSrchApplyWorksTitl(
				ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyWorksTitl" ) );
			srchParam.setSrchStatCd( ServletRequestUtils.getIntParameter( multipartRequest, "srchStatCd", 0 ) );
			srchParam.setNowPage( ServletRequestUtils.getIntParameter( multipartRequest, "page_no", 1 ) );
			srchParam.setSrchRgstIdnt( user.getUserIdnt() );
			/* 조회조건 end */
		}

		/*
		 * 조회조건 str srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyType"));
		 * srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyFrDt"));
		 * srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyToDt"));
		 * srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyWorksTitl"));
		 * srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(
		 * multipartRequest, "srchStatCd", 0));
		 * srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no",
		 * 1)); 조회조건 end
		 */

		String existYn = ServletRequestUtils.getStringParameter( multipartRequest, "existYn" ); // 이용신청명세서 첨부여부

		String actionDiv = ServletRequestUtils.getStringParameter( multipartRequest, "action_div" );

		String processDiv = "UPDATE";

		String applyWriteYmd = ServletRequestUtils.getStringParameter( multipartRequest, "applyWriteYmd" );
		int applyWriteSeq = ServletRequestUtils.getIntParameter( multipartRequest, "applyWriteSeq", 0 );
		int applyWorksCnt = ServletRequestUtils.getIntParameter( multipartRequest, "applyWorksCnt", 1 );

		if( applyWriteYmd == null || applyWriteYmd.equals( "" ) ){
			processDiv = "INSERT";

			DecimalFormat df = new DecimalFormat( "00" );
			Calendar currentCal = Calendar.getInstance();
			currentCal.add( currentCal.DATE, 0 );
			applyWriteYmd =
				Integer.toString( currentCal.get( Calendar.YEAR ) ) + df.format( currentCal.get( Calendar.MONTH ) + 1 )
					+ df.format( currentCal.get( Calendar.DAY_OF_MONTH ) );

			applyWriteSeq = myStatService.newApplyWriteSeq( "TMP" );
		}

		String isCompleteStep01 = (String) session.getAttribute( "isCompleteStep01" );

		if( isCompleteStep01 != null ){
			String sessApplyWriteYmd = (String) session.getAttribute( "applyWriteYmd" );
			int sessApplyWriteSeq = (Integer) session.getAttribute( "applyWriteSeq" );
			applyWriteYmd = sessApplyWriteYmd;
			applyWriteSeq = sessApplyWriteSeq;
		}

		StatApplication param = new StatApplication();
		param.setApplyWriteYmd( applyWriteYmd );
		param.setApplyWriteSeq( applyWriteSeq );
		param.setApplyType01( ServletRequestUtils.getStringParameter( multipartRequest, "applyType01", "0" ) );
		param.setApplyType02( ServletRequestUtils.getStringParameter( multipartRequest, "applyType02", "0" ) );
		param.setApplyType03( ServletRequestUtils.getStringParameter( multipartRequest, "applyType03", "0" ) );
		param.setApplyType04( ServletRequestUtils.getStringParameter( multipartRequest, "applyType04", "0" ) );
		param.setApplyType05( ServletRequestUtils.getStringParameter( multipartRequest, "applyType05", "0" ) );
		param.setApplyWorksCnt( applyWorksCnt );
		param.setApplyWorksTitl(XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyWorksTitl" ) ) );
		param.setApplyWorksKind(XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyWorksKind" ) ) );
		param.setApplyWorksForm(XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyWorksForm" ) ) );
		param.setUsexDesc( XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "usexDesc" ) ) );
		param.setApplyReas(	XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyReas" ) ) );
		param.setCpstAmnt( XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "cpstAmnt" ) ) );
		param.setApplrName(	XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applrName" ) ) );
		param.setApplrResdCorpNumb(	XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applrResdCorpNumb" ) ) );
		param.setApplrAddr(XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applrAddr" ) ) );
		param.setApplrTelx(XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applrTelx" ) ) );
		param.setApplyDivsCd(ServletRequestUtils.getStringParameter( multipartRequest, "applyDivsCd" ) );
		param.setApplyProxyName(XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyProxyName" ) ) );
		param.setApplyProxyResdCorpNumb(XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyProxyResdCorpNumb" ) ) );
		param.setApplyProxyAddr(XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyProxyAddr" ) ) );
		param.setApplyProxyTelx(XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyProxyTelx" ) ) );

		param.setApplyRawCd( ServletRequestUtils.getIntParameter( multipartRequest, "applyRawCd", 0 ) );
		if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
			param.setStatCd( 4 ); // 진행상황 : 보완임시저장
		}else{
			param.setStatCd( 1 ); // 진행상황 : 임시저장
		}
		param.setRgstIdnt( ServletRequestUtils.getStringParameter( multipartRequest, "rgstIdnt" ) );

		param.setChkWorksId( ServletRequestUtils.getStringParameters( multipartRequest, "iChk" ) ); // 해당 []값으로 명세서 테이블
																									// 추가

		param.setFileNameCd( ServletRequestUtils.getStringParameters( multipartRequest, "fileNameCd" ) );
		param.setFileDelYn( ServletRequestUtils.getStringParameters( multipartRequest, "fileDelYn" ) );
		param.setHddnFile( ServletRequestUtils.getStringParameters( multipartRequest, "hddnFile" ) );
		param.setFileName( ServletRequestUtils.getStringParameters( multipartRequest, "fileName" ) );
		param.setRealFileName( ServletRequestUtils.getStringParameters( multipartRequest, "realFileName" ) );
		param.setFileSize( ServletRequestUtils.getStringParameters( multipartRequest, "fileSize" ) );
		param.setFilePath( ServletRequestUtils.getStringParameters( multipartRequest, "filePath" ) );

		 long startTime = System.currentTimeMillis();

		 System.out.println(">>>>>>>>>>>>>>>>:::::::시작하는 시간::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+startTime+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		/* 첨부화일 업로드 시작 */
		int[] attcSeqn = ServletRequestUtils.getIntParameters( multipartRequest, "attcSeqn" );
		String[] fileNameCd = ServletRequestUtils.getStringParameters( multipartRequest, "fileNameCd" );
		String[] fileDelYn = ServletRequestUtils.getStringParameters( multipartRequest, "fileDelYn" );
		String[] hddnFile = ServletRequestUtils.getStringParameters( multipartRequest, "hddnFile" );

		
		
		System.out.println( Arrays.toString( attcSeqn ) );
		System.out.println( Arrays.toString( fileNameCd ) );
		System.out.println( Arrays.toString( hddnFile ) );
		System.out.println( Arrays.toString( fileDelYn ) );
		System.out.println( "attcSeqn ::" + attcSeqn );
		System.out.println( "fileNameCd:: " + fileNameCd );
		System.out.println( "fileNameCd:: " + fileNameCd );
		System.out.println( "hddnFile :: " + hddnFile );
		
		// 첨부파일 중 수정 된 첨부파일에 대한 정보 수집
		List delFileList = new ArrayList();

		for( int i = 0; i < attcSeqn.length; i++ ){
			File delFileInfo = new File();
			if( fileDelYn[i].equals( "Y" ) ){
				if( attcSeqn[i] == 0 ){
					break;
				}
				System.out.println( "attcSeqn ::" + attcSeqn[i] );
				System.out.println( "fileNameCd ::" + fileNameCd[i] );
				delFileInfo.setAttcSeqn( attcSeqn[i] );
				delFileInfo.setFileAttcCd( "ST" );
				delFileInfo.setFileNameCd( fileNameCd[i] );
				delFileInfo.setApplyWriteYmd( applyWriteYmd );
				delFileInfo.setApplyWriteSeq( applyWriteSeq );

				delFileList.add( delFileInfo );
			}
		}
		param.setDelFileList( delFileList );

		// 새로 첨부된 첨부파일에 대한 정보 수집
		List fileList = new ArrayList();

		Iterator<String> fileNameIterator = multipartRequest.getFileNames();
		
		System.out.println( "multipartRequest.getFileNames() :" + multipartRequest.getFileNames() );
		System.out.println( "fileNameIterator11 :::" + fileNameIterator );
		System.out.println( fileNameIterator.hasNext() );
		
		int newAttcSeqn = commonService.getNewAttcSeqn();

		while( fileNameIterator.hasNext() ){
			String fileName = fileNameIterator.next();
			System.out.println( "INSERT FILE2   +  :" + fileName );
			MultipartFile multiFile = multipartRequest.getFile( fileName );
			if( multiFile.getSize() > 0 ){
				PrpsAttc uploadInfo = FileUploadUtil.uploadPrpsFiles( multiFile, realUploadPath );
				System.out.println( "################ " + uploadInfo.getFILE_NAME() );
				File fileInfo = new File();
				for( int fm = 0; fm < hddnFile.length; fm++ ){
					if( hddnFile[fm].toUpperCase().equals( uploadInfo.getFILE_NAME().toUpperCase() ) ){
						fileInfo.setAttcSeqn( newAttcSeqn );
						fileInfo.setFileAttcCd( "ST" );
						fileInfo.setFileNameCd( fileNameCd[fm] );
						fileInfo.setFileName( hddnFile[fm] );
						fileInfo.setFilePath( uploadInfo.getFILE_PATH() );
						fileInfo.setFileSize( Integer.parseInt( uploadInfo.getFILE_SIZE() ) );
						fileInfo.setRealFileName( uploadInfo.getREAL_FILE_NAME() );
						fileInfo.setApplyWriteYmd( applyWriteYmd );
						fileInfo.setApplyWriteSeq( applyWriteSeq );
						fileInfo.setRgstIdnt( ServletRequestUtils.getStringParameter( multipartRequest, "rgstIdnt" ) );
						hddnFile[fm] = "";
						break;
					}
				}

				fileList.add( fileInfo );
			}
			newAttcSeqn++;
		}

		 long endTime = System.currentTimeMillis();
		param.setFileList( fileList );
		 System.out.println(">>>>>>>>>>>>>>>>:::::::끝나는	// 시간::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+endTime+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		// long durationTime = endTime - startTime;
		// System.out.println("milli second : " + durationTime );
		// System.out.println("second = milli second / 1000 : " + (durationTime
		// / 1000));

		if( isCompleteStep01 == null ){
			if( processDiv.equals( "INSERT" ) ){
				myStatService.insertTmpStatPrps( request, param );
			}else{
				if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
					myStatService.updateStatPrps( request, param );
				}else{
					myStatService.updateTmpStatPrps( request, param );
				}
			}
		}

		ModelAndView mv = new ModelAndView( "stat/statPrpsList" );
		if( actionDiv.equals( "goStep2" ) ){
			srchParam.setApplyWriteYmd( applyWriteYmd );
			srchParam.setApplyWriteSeq( applyWriteSeq );

			StatApplyWorks statApplyWorks = new StatApplyWorks();
			statApplyWorks.setApplyWriteYmd( applyWriteYmd );
			statApplyWorks.setApplyWriteSeq( applyWriteSeq );

			List statApplyWorksList;

			if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
				statApplyWorksList = myStatService.statApplyWorksList( statApplyWorks );
			}else{
				statApplyWorksList = myStatService.statApplyWorksListTmp( statApplyWorks );
			}

			mv = new ModelAndView( "stat/statPrpsList", "statApplyWorksList", statApplyWorksList );
			if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
				mv.addObject( "statCd", 4 );
			}else{
				mv.addObject( "statCd", 1 );
			}
			mv.addObject( "applyWorksCnt", applyWorksCnt );
			mv.addObject( "srchParam", srchParam );
			mv.addObject( "existYn", existYn );
			mv.addObject( "srchVO", srchVO );
			mv.addObject( "isMy", isMy );
			if( isMy.equals( "Y" ) ){
				mv.addObject( "worksId", myWorksId );
			}

		}else if( actionDiv.equals( "goList" ) ){
			if( isMy.equals( "" ) ){
				mv = new ModelAndView( "redirect:/stat/statSrch.do" );
			}else{
				mv = new ModelAndView( "redirect:/myStat/statRsltInqrList.do" );
			}
		}
		session.setAttribute( "isCompleteStep01", "Y" );
		session.setAttribute( "applyWriteYmd", applyWriteYmd );
		session.setAttribute( "applyWriteSeq", applyWriteSeq );

		return mv;
	}

	// 법정허락 신청 step2 임시저장
	@RequestMapping( value = "/statPrpsDetl" )
	public ModelAndView tmpStatPrps2SaveDo( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		StatApplication srchParam = new StatApplication();
		/*
		 * 조회조건 str srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(
		 * request, "srchApplyType"));
		 * srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter( request,
		 * "srchApplyFrDt"));
		 * srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter( request,
		 * "srchApplyToDt"));
		 * srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(
		 * request, "srchApplyWorksTitl"));
		 * srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(request,
		 * "srchStatCd", 0));
		 * srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no",
		 * 1)); 조회조건 end
		 */

		String isMy = ServletRequestUtils.getStringParameter( request, "isMy", "" );

		String statCd = ServletRequestUtils.getStringParameter( request, "stat_cd", "1" );

		Map params = new HashMap();
		User user = SessionUtil.getSession( request );
		params.put( "userIdnt", user.getUserIdnt() );
		// 사용자 조회
		Map userInfo = userService.selectUserInfo( params );

		MlStatWorksVO srchVO = new MlStatWorksVO();

		if( isMy.equals( "" ) ){
			/* 조회조건 str */
			srchVO.setPageNum( ServletRequestUtils.getStringParameter( request, "pageNo" ) );
			srchVO.setSearchCondition( ServletRequestUtils.getStringParameter( request, "searchCondition" ) );
			srchVO.setSearchKeyword1( ServletRequestUtils.getStringParameter( request, "searchKeyword1" ) );
		}else{
			/* 조회조건 str */
			srchParam.setSrchApplyType( ServletRequestUtils.getStringParameter( request, "srchApplyType" ) );
			srchParam.setSrchApplyFrDt( ServletRequestUtils.getStringParameter( request, "srchApplyFrDt" ) );
			srchParam.setSrchApplyToDt( ServletRequestUtils.getStringParameter( request, "srchApplyToDt" ) );
			srchParam.setSrchApplyWorksTitl( ServletRequestUtils.getStringParameter( request, "srchApplyWorksTitl" ) );
			srchParam.setSrchStatCd( ServletRequestUtils.getIntParameter( request, "srchStatCd", 0 ) );
			srchParam.setNowPage( ServletRequestUtils.getIntParameter( request, "page_no", 1 ) );
			srchParam.setSrchRgstIdnt( user.getUserIdnt() );
			/* 조회조건 end */
		}

		String existYn = ServletRequestUtils.getStringParameter( request, "existYn" ); // 이용신청명세서 첨부여부

		String actionDiv = ServletRequestUtils.getStringParameter( request, "action_div" );

		String applyWriteYmd = ServletRequestUtils.getStringParameter( request, "applyWriteYmd" );
		int applyWriteSeq = ServletRequestUtils.getIntParameter( request, "applyWriteSeq", 0 );

		ModelAndView mv = new ModelAndView( "stat/statPrpsDetl" );
		if( actionDiv.equals( "goStep3" ) ){
			srchParam.setApplyWriteYmd( applyWriteYmd );
			srchParam.setApplyWriteSeq( applyWriteSeq );

			StatApplication param = new StatApplication();
			param.setApplyWriteYmd( ServletRequestUtils.getStringParameter( request, "applyWriteYmd" ) );
			param.setApplyWriteSeq( ServletRequestUtils.getIntParameter( request, "applyWriteSeq", 0 ) );
			if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
				StatApplication statApplication = myStatService.statApplication( param );

				List fileList2 = myStatService.statAttcFile( param );
				statApplication.setFileList( fileList2 );

				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd( applyWriteYmd );
				statApplyWorks.setApplyWriteSeq( applyWriteSeq );

				List statApplyWorksList = myStatService.statApplyWorksList( statApplyWorks );

				mv = new ModelAndView( "stat/statPrpsDetl" );
				mv.addObject( "userInfo", userInfo );
				mv.addObject( "statApplication", statApplication );
				mv.addObject( "statApplyWorksList", statApplyWorksList );
				mv.addObject( "srchParam", srchParam );
				mv.addObject( "srchVO", srchVO );
				mv.addObject( "isMy", isMy );
			}else{
				StatApplication statApplication = myStatService.statApplicationTmp( param );

				List fileList2 = myStatService.statAttcFileTmp( param );
				statApplication.setFileList( fileList2 );

				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd( applyWriteYmd );
				statApplyWorks.setApplyWriteSeq( applyWriteSeq );

				List statApplyWorksList = myStatService.statApplyWorksListTmp( statApplyWorks );

				mv = new ModelAndView( "stat/statPrpsDetl" );
				mv.addObject( "userInfo", userInfo );
				mv.addObject( "statApplication", statApplication );
				mv.addObject( "statApplyWorksList", statApplyWorksList );
				mv.addObject( "srchParam", srchParam );
				mv.addObject( "srchVO", srchVO );
				mv.addObject( "isMy", isMy );
			}

		}else if( actionDiv.equals( "goList" ) ){
			if( isMy.equals( "" ) ){
				mv = new ModelAndView( "redirect:/stat/statSrch.do" );
			}else{
				mv = new ModelAndView( "redirect:/myStat/statRsltInqrList.do" );
			}
		}
		return mv;
	}

	// 인증서 확인
	@RequestMapping( value = "/chkSignCert" )
	public ModelAndView chkSignCert( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		String result = "";

		if( !request.getParameter( "certId" ).equals( "" ) && !request.getParameter( "certId" ).equals( "null" )
			&& !request.getParameter( "certId" ).equals( null ) ){
			/*************** 공인 인증서 전자서명 확인 ********************/
			SignCertUtil signCertUtil = new SignCertUtil(); // 전자인증 확인을 위한
			// Utility

			request.setCharacterEncoding( "euc-kr" ); // EUC-KR로 설정

			SignDTO signDTO = new SignDTO(); // 전자인증 정보 저장용 DTO

			signDTO.setChallenge( ServletRequestUtils.getStringParameter( request, "Challenge" ) );
			signDTO.setMemberType( ServletRequestUtils.getStringParameter( request, "memberType" ) );
			signDTO.setSsnNo( ServletRequestUtils.getStringParameter( request, "ssnNo" ) );
			signDTO.setCrnNo( ServletRequestUtils.getStringParameter( request, "corpNo" ) );
			signDTO.setCertId( ServletRequestUtils.getStringParameter( request, "certId" ) );
			signDTO.setUserSignCert( ServletRequestUtils.getStringParameter( request, "userSignCert" ) );
			signDTO.setOriginalMessage( ServletRequestUtils.getStringParameter( request, "originalMessage" ) );
			signDTO.setUserSignValue( ServletRequestUtils.getStringParameter( request, "userSignValue" ) );
			signDTO.setEncryptedSessionKey( ServletRequestUtils.getStringParameter( request, "encryptedSessionKey" ) );
			signDTO.setEncryptedUserRandomNumber(
				ServletRequestUtils.getStringParameter( request, "encryptedUserRandomNumber" ) );
			signDTO.setEncryptedUserSSN( ServletRequestUtils.getStringParameter( request, "encryptedUserSSN" ) );

			// 전자서명 인증 확인
			if( !signCertUtil.chkSignCert( request, signDTO ) ){
				log.debug(
					"#################singCertMessage=###" + signCertUtil.getSignCertMessage()
						+ "#######################" );
				request.getSession( true ).setAttribute(
					SignContants.SIGN_VALIDATE,
					signCertUtil.getSignCertMessage() );

				result = signCertUtil.getSignCertMessage();

			}

			request.getSession( true ).removeAttribute( SignContants.SIGN_VALIDATE );

			/*
			 * 전자인증정보에서 DN, SerialNumber 정보 추출 > TA_TMP_MUSIC_APPLY_CONFIRM
			 * signCertUtil.getCertDN(); // DN signCertUtil.getSerialNumber(); //
			 * SerialNumber signDTO.getUserSignCert(); // 인증서정보
			 * signCertUtil.getOriginalMessage(); // 원본 메시지 signCertUtil.getSerialNumber();
			 * // 전자서명값
			 */
			/**********************************************************/

			ApplySign applySign = new ApplySign();

			applySign.setSignDn( signCertUtil.getCertDN() );
			applySign.setSignSn( signCertUtil.getSerialNumber() );
			applySign.setSignCert( signDTO.getUserSignCert() );
			applySign.setSignOrgnData( signCertUtil.getOriginalMessage() );
			applySign.setSignDate( signCertUtil.getSignValue() );

			request.getSession( true ).setAttribute( "applySign", applySign );
		}

		PrintWriter out = respone.getWriter();

		out.print( result );

		out.close();

		return null;
	}

	// 법정허락 신청 실행
	@RequestMapping( value = "/statPrpsInsert" )
	public ModelAndView statPrpsInsertDo( HttpServletRequest request,		HttpServletResponse respone ) throws Exception{
		String isMy = ServletRequestUtils.getStringParameter( request, "isMy" );

		String applyWriteYmd = ServletRequestUtils.getStringParameter( request, "applyWriteYmd" );
		int applyWriteSeq = ServletRequestUtils.getIntParameter( request, "applyWriteSeq", 0 );

		ApplySign applySign = new ApplySign();

		StatApplication param = new StatApplication();
		param.setApplyWriteYmd( ServletRequestUtils.getStringParameter( request, "applyWriteYmd" ) );
		param.setApplyWriteSeq( ServletRequestUtils.getIntParameter( request, "applyWriteSeq", 0 ) );
		param.setStatCd( ServletRequestUtils.getIntParameter( request, "stat_cd", 2 ) );
		param.setRgstIdnt( ServletRequestUtils.getStringParameter( request, "rgstIdnt" ) );

		// 신규 신청 번호 생성 str
		DecimalFormat df = new DecimalFormat( "00" );
		Calendar currentCal = Calendar.getInstance();
		currentCal.add( currentCal.DATE, 0 );
		String newApplyWriteYmd = Integer.toString( currentCal.get( Calendar.YEAR ) )			+ df.format( currentCal.get( Calendar.MONTH ) + 1 ) + df.format( currentCal.get( Calendar.DAY_OF_MONTH ) );
		int newApplyWriteSeq = myStatService.newApplyWriteSeq( "" );

		param.setNewApplyWriteYmd( newApplyWriteYmd );
		param.setNewApplyWriteSeq( newApplyWriteSeq );

		applySign.setApplyWriteYmd( newApplyWriteYmd );
		applySign.setApplyWriteSeq( newApplyWriteSeq );

		request.getSession( true ).removeAttribute( "applySign" );

		param.setApplySign( applySign );
		try{
			myStatService.insertStatPrps( param );
		}
		catch( Exception e ){

		}
		// 메일 보내기
		Map params = new HashMap();
		User user = SessionUtil.getSession( request );
		params.put( "userIdnt", user.getUserIdnt() );

		// 사용자 조회
		Map userInfo = userService.selectUserInfo( params );

		StatApplication newParam = new StatApplication();
		newParam.setApplyWriteYmd( newApplyWriteYmd );
		newParam.setApplyWriteSeq( newApplyWriteSeq );

		StatApplication statApplication = myStatService.statApplication( newParam );

		String statNm = "신청완료";

		String host = kr.or.copyright.mls.support.constant.Constants.MAIL_SERVER_IP; // smtp서버
		String to = userInfo.get( "MAIL" ).toString(); // 수신EMAIL
		String toName = userInfo.get( "USER_NAME" ).toString(); // 수신인
		String subject = "[저작권찾기] 법정허락 이용신청 " + statNm;

		String from = kr.or.copyright.mls.support.constant.Constants.SYSTEM_MAIL_ADDRESS; // 발신인
		// 주소
		// -
		// 시스템
		// 대표
		// 메일
		String fromName = kr.or.copyright.mls.support.constant.Constants.SYSTEM_NAME;
		
		if( userInfo.get( "MAIL_RECE_YSNO" ) != null ){

			if( userInfo.get( "MAIL_RECE_YSNO" ).equals( "Y" ) && to != null && to != "" ){

				// ------------------------- mail 정보
				// ----------------------------------//
				MailInfo info = new MailInfo();
				info.setFrom( from );
				info.setFromName( fromName );
				info.setHost( host );
				info.setEmail( to );
				info.setEmailName( toName );
				info.setSubject( subject );

				StringBuffer sb = new StringBuffer();

				sb.append( "				<tr>" );
				sb.append(
					"					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">" );
				sb.append(
					"					<p style=\"font-weight:bold; background:url(" + Constants.DOMAIN_HOME
						+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 법정허락 이용신청 "
						+ statNm + " 정보</p>" );
				sb.append(
					"					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">" );
				sb.append(
					"						<li style=\"background:url(" + Constants.DOMAIN_HOME
						+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자 이름 : " + toName
						+ " </li>" );
				sb.append(
					"						<li style=\"background:url(" + Constants.DOMAIN_HOME
						+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청번호 : " + newApplyWriteYmd
						+ "-" + newApplyWriteSeq + " </li>" );
				sb.append(
					"						<li style=\"background:url(" + Constants.DOMAIN_HOME
						+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : "
						+ statApplication.getApplyWorksTitl() + " </li>" );
				sb.append( "					</ul>" );
				sb.append( "					</td>" );
				sb.append( "				</tr>" );

				info.setMessage( sb.toString() );
				info.setSubject( subject );
				info.setMessage( new String( MailMessage.getChangeInfoMailText2( info ) ) );

				/*
				 * sb.append("<div class=\"mail_title\">" + toName + "님, 법정허락 이용신청이 " + statNm +
				 * " 되었습니다. </div>"); sb.append("<div class=\"mail_contents\"> "); sb.
				 * append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "
				 * ); sb.append("		<tr> "); sb.append("			<td> "); sb.
				 * append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> "
				 * ); // 변경부분 sb.append("					<tr> ");
				 * sb.append("						<td class=\"tdLabel\">신청번호</td> ");
				 * sb.append("						<td class=\"tdData\">" + newApplyWriteYmd +
				 * "-" + newApplyWriteSeq + "</td> "); sb.append("					</tr> ");
				 * sb.append("					<tr> ");
				 * sb.append("						<td class=\"tdLabel\">제호(제목)</td> ");
				 * sb.append("						<td class=\"tdData\">" +
				 * statApplication.getApplyWorksTitl() + "</td> ");
				 * sb.append("					</tr> "); sb.append("				</table> ");
				 * sb.append("			</td> "); sb.append("		</tr> ");
				 * sb.append("	</table> "); sb.append("</div> "); info.setMessage(new
				 * String(MailMessage.getChangeInfoMailText(sb .toString())));
				 */
				MailManager manager = MailManager.getInstance();

				info = manager.sendMail( info );
				if( info.isSuccess() ){
					System.out.println( ">>>>>>>>>>>> message success :" + info.getEmail() );
				}else{
					System.out.println( ">>>>>>>>>>>> message false   :" + info.getEmail() );
				}

			}
			// 관리자한테 메일 보내기
			HashMap userMap = new HashMap();

			// 신청제목
			userMap.put( "WORKS_TITL", statApplication.getApplyWorksTitl() );

			// 신청번호
			userMap.put( "APPLY_NO", newApplyWriteYmd + "-" + newApplyWriteSeq );

			// 사용자 정보 셋팅
			userMap.put( "USER_IDNT", user.getUserIdnt() );
			userMap.put( "USER_NAME", user.getUserName() );
			userMap.put( "U_MAIL", user.getMail() );
			userMap.put( "MOBL_PHON", user.getMoblPhon() );

			userMap.put( "MGNT_DIVS", "ST" );
			userMap.put( "MAIL_TITL", "법정허락 이용신청이 완료되었습니다." );

			List list = new ArrayList();

			list = commonService.getMgntMail( userMap );

			int listSize = list.size();

			String to1[] = new String[listSize];
			String toName1[] = new String[listSize];

			for( int i = 0; i < listSize; i++ ){
				HashMap map = (HashMap) list.get( i );
				toName1[i] = (String) map.get( "USER_NAME" );
				to1[i] = (String) map.get( "MAIL" );
			}

			userMap.put( "TO", to1 );
			userMap.put( "TO_NAME", toName1 );

			SendMail.send( userMap );

		}
		// sms 보내기
		String smsTo = (String) userInfo.get( "MOBL_PHON" );
		String smsFrom = kr.or.copyright.mls.support.constant.Constants.SYSTEM_TELEPHONE;

		String smsMessage = "[저작권찾기] 법정허락 이용신청이 " + statNm + " 되었습니다.";

		if( userInfo.get( "SMS_RECE_YSNO" ) != null && userInfo.get( "SMS_RECE_YSNO" ).equals( "Y" ) && smsTo != null
			&& smsTo != "" ){

			SMSManager smsManager = new SMSManager();

			System.out.println( "return = " + smsManager.sendSMS( smsTo, smsFrom, smsMessage ) );
		}

		ModelAndView mv = new ModelAndView();

		if( isMy.equals( "" ) ){
			mv = new ModelAndView( "redirect:/stat/statSrch.do" );
		}else{
			mv = new ModelAndView( "redirect:/myStat/statRsltInqrList.do" );
		}

		return mv;

	}

	// 법정허락 신청정보 등록 step1
	@RequestMapping( value = "/statPrpsMain_priv" )
	public ModelAndView statPrps_priv( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		HttpSession session = request.getSession();

		session.removeAttribute( "isCompleteStep01" );
		session.removeAttribute( "isCompleteStep01_1" );
		session.removeAttribute( "isCompleteStep01_2" );
		session.removeAttribute( "isCompleteStep02" );
		session.removeAttribute( "applyWriteYmd" );
		session.removeAttribute( "applyWriteSeq" );

		String isModi = ServletRequestUtils.getStringParameter( request, "isModi", "01" );

		MlStatWorksVO srchVO = new MlStatWorksVO();
		/* 조회조건 str */
		srchVO.setPageNum( ServletRequestUtils.getStringParameter( request, "pageNo" ) );
		srchVO.setSearchCondition( ServletRequestUtils.getStringParameter( request, "searchCondition" ) );
		srchVO.setSearchKeyword1( ServletRequestUtils.getStringParameter( request, "searchKeyword1" ) );
		/* 조회조건 end */

		Map params = new HashMap();
		User user = SessionUtil.getSession( request );
		params.put( "userIdnt", user.getUserIdnt() );

		// 사용자 조회
		 Map userInfo = userService.selectUserInfo(params);
		//Map userInfo = null;
		ModelAndView mv = new ModelAndView( "stat/statPrpsMain_priv" );
		if( isModi.equals( "01" ) ){// 등록화면
			System.out.println( "###################등록화면##########################" );
			String actionDiv = ServletRequestUtils.getStringParameter( request, "action_div" );

			if( actionDiv == null || actionDiv == "" ){
				actionDiv = "new";
			}

			mv = new ModelAndView( "stat/statPrpsMain_priv" );

			mv.addObject( "isModi", isModi );
			mv.addObject( "userInfo", userInfo );
			mv.addObject( "srchVO", srchVO );
		}else{// 수정화면
			System.out.println( "###################수정화면##########################" );
			StatApplication param = new StatApplication();
			param.setApplyWriteYmd( ServletRequestUtils.getStringParameter( request, "apply_write_ymd" ) );
			param.setApplyWriteSeq( ServletRequestUtils.getIntParameter( request, "apply_write_seq", 0 ) );

			System.out.println( param.getApplyWriteYmd() );
			System.out.println( param.getApplyWriteSeq() );

			// 이용승인 신청서tmp 조회
			int statCd = ServletRequestUtils.getIntParameter( request, "stat_cd" );
			StatApplication statApplication = null;
			if( statCd == 1 ){
				System.out.println( "###################statCd == 1##########################" );
				statApplication = myStatService.statApplicationTmp( param );
			}else{
				statApplication = myStatService.statApplication( param );
			}

			// 이용승인 신청서tmp 첨부파일 조회
			List fileList = null;
			if( statCd == 1 ){
				fileList = myStatService.statAttcFileTmp( param );
			}else{
				fileList = myStatService.statAttcFile( param );
			}
			statApplication.setFileList( fileList );

			mv = new ModelAndView( "stat/statPrpsMain_priv" );

			mv.addObject( "isModi", isModi );
			mv.addObject( "statApplication", statApplication );
			mv.addObject( "userInfo", userInfo );
			mv.addObject( "srchVO", srchVO );
		}
		return mv;
	}

	// 법정허락 신청정보 등록 step1_수정화면
	@RequestMapping( value = "/statPrpsMain_priv_modi" )
	public ModelAndView statPrps_priv_modi( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		HttpSession session = request.getSession();

		session.removeAttribute( "isCompleteStep01" );
		session.removeAttribute( "isCompleteStep01_1" );
		session.removeAttribute( "isCompleteStep01_2" );
		session.removeAttribute( "isCompleteStep02" );
		session.removeAttribute( "applyWriteYmd" );
		session.removeAttribute( "applyWriteSeq" );

		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );

		///////////////////////////////////////////////// 모디를 멀티파트로 받아야함
		String isModi = ServletRequestUtils.getStringParameter( multipartRequest, "isModi", "01" );
		String isMy = ServletRequestUtils.getStringParameter( multipartRequest, "isMy", "" );

		MlStatWorksVO srchVO = new MlStatWorksVO();
		StatApplication srchParam = new StatApplication();

		Map params = new HashMap();
		User user = SessionUtil.getSession( request );
		params.put( "userIdnt", user.getUserIdnt() );

		// 사용자 조회
		Map userInfo = userService.selectUserInfo( params );

		if( isMy.equals( "" ) ){
			/* 조회조건 str */
			srchVO.setPageNum( ServletRequestUtils.getStringParameter( multipartRequest, "pageNo" ) );
			srchVO.setSearchCondition( ServletRequestUtils.getStringParameter( multipartRequest, "searchCondition" ) );
			srchVO.setSearchKeyword1( ServletRequestUtils.getStringParameter( multipartRequest, "searchKeyword1" ) );
			/* 조회조건 end */
		}else{
			/* 조회조건 str */
			srchParam.setSrchApplyType( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyType" ) );
			srchParam.setSrchApplyFrDt( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyFrDt" ) );
			srchParam.setSrchApplyToDt( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyToDt" ) );
			srchParam.setSrchApplyWorksTitl(
				ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyWorksTitl" ) );
			srchParam.setSrchStatCd( ServletRequestUtils.getIntParameter( multipartRequest, "srchStatCd", 0 ) );
			srchParam.setNowPage( ServletRequestUtils.getIntParameter( multipartRequest, "page_no", 1 ) );
			srchParam.setSrchRgstIdnt( user.getUserIdnt() );
			/* 조회조건 end */
		}

		ModelAndView mv = new ModelAndView( "stat/statPrpsMain_priv" );
		if( isModi.equals( "01" ) ){// 등록화면
			System.out.println( "###################등록화면##########################" );
			String actionDiv = ServletRequestUtils.getStringParameter( multipartRequest, "action_div" );

			if( actionDiv == null || actionDiv == "" ){
				actionDiv = "new";
			}

			mv = new ModelAndView( "stat/statPrpsMain_priv" );

			mv.addObject( "isModi", isModi );
			mv.addObject( "userInfo", userInfo );
			mv.addObject( "srchVO", srchVO );
		}else{// 수정화면
			System.out.println( "###################수정화면##########################" );
			StatApplication param = new StatApplication();
			param.setApplyWriteYmd( ServletRequestUtils.getStringParameter( multipartRequest, "apply_write_ymd" ) );
			param.setApplyWriteSeq( ServletRequestUtils.getIntParameter( multipartRequest, "apply_write_seq", 0 ) );

			System.out.println( param.getApplyWriteYmd() );
			System.out.println( param.getApplyWriteSeq() );

			// 이용승인 신청서tmp 조회
			int statCd = ServletRequestUtils.getIntParameter( multipartRequest, "stat_cd" );
			StatApplication statApplication = null;
			if( statCd == 1 ){
				System.out.println( "###################statCd == 1##########################" );
				statApplication = myStatService.statApplicationTmp( param );
			}else{
				statApplication = myStatService.statApplication( param );
			}

			// 이용승인 신청서tmp 첨부파일 조회
			List fileList = null;
			if( statCd == 1 ){
				fileList = myStatService.statAttcFileTmp( param );
			}else{
				fileList = myStatService.statAttcFile( param );
			}
			statApplication.setFileList( fileList );

			mv = new ModelAndView( "stat/statPrpsMain_priv" );

			mv.addObject( "isModi", isModi );
			mv.addObject( "statApplication", statApplication );
			mv.addObject( "userInfo", userInfo );
			mv.addObject( "isMy", isMy );
			mv.addObject( "srchVO", srchVO );
			mv.addObject( "srchParam", srchParam );
		}
		return mv;
	}

	// 법정허락 신청 step1 임시저장
	@RequestMapping( value = "/statPrpsList_priv", method = RequestMethod.POST )
	public ModelAndView tmpStatPrps1SaveDo_priv( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		HttpSession session = request.getSession();

		// remove 세션 없애기
		session.removeAttribute( "isCompleteStep01_1" );

		String isCompleteStep01_2 = (String) session.getAttribute( "isCompleteStep01_2" );

		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

		// multipartRequest = multipartResolver.resolveMultipart(request);

		StatApplication srchParam = new StatApplication();

		MlStatWorksVO srchVO = new MlStatWorksVO();

		String isMy = ServletRequestUtils.getStringParameter( multipartRequest, "isMy", "" );

		int prevCnt = ServletRequestUtils.getIntParameter( multipartRequest, "prevCnt", 1 );

		String actionDiv = ServletRequestUtils.getStringParameter( multipartRequest, "action_div" );
		String statCd = "";
		if( actionDiv.equals( "updateFile" ) ){
			statCd = ServletRequestUtils.getStringParameter( multipartRequest, "stat_cd", "1" );
		}else{
			statCd = ServletRequestUtils.getStringParameter( multipartRequest, "statCd", "1" );
		}

		Map params = new HashMap();
		User user = SessionUtil.getSession( request );
		params.put( "userIdnt", user.getUserIdnt() );
		System.out.println( "UserIdnt ::: " + user.getUserIdnt() );
		// 사용자 조회
		/* Map userInfo = userService.selectUserInfo( params );*/
		 Map userInfo = null; 

		if( isMy.equals( "" ) ){
			/* 조회조건 str */
			srchVO.setPageNum( ServletRequestUtils.getStringParameter( multipartRequest, "pageNo" ) );
			srchVO.setSearchCondition( ServletRequestUtils.getStringParameter( multipartRequest, "searchCondition" ) );
			srchVO.setSearchKeyword1( ServletRequestUtils.getStringParameter( multipartRequest, "searchKeyword1" ) );
			/* 조회조건 end */
		}else{
			/* 조회조건 str */
			srchParam.setSrchApplyType( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyType" ) );
			srchParam.setSrchApplyFrDt( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyFrDt" ) );
			srchParam.setSrchApplyToDt( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyToDt" ) );
			srchParam.setSrchApplyWorksTitl(
				ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyWorksTitl" ) );
			srchParam.setSrchStatCd( ServletRequestUtils.getIntParameter( multipartRequest, "srchStatCd", 0 ) );
			srchParam.setNowPage( ServletRequestUtils.getIntParameter( multipartRequest, "page_no", 1 ) );
			srchParam.setSrchRgstIdnt( user.getUserIdnt() );
			/* 조회조건 end */
		}

		/*
		 * 조회조건 str srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyType"));
		 * srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyFrDt"));
		 * srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyToDt"));
		 * srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyWorksTitl"));
		 * srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(
		 * multipartRequest, "srchStatCd", 0));
		 * srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no",
		 * 1)); 조회조건 end
		 */

		String existYn = ServletRequestUtils.getStringParameter( multipartRequest, "existYn" ); // 이용신청명세서 첨부여부
		String processDiv = "UPDATE";

		String applyWriteYmd = ServletRequestUtils.getStringParameter( multipartRequest, "applyWriteYmd" );
		int applyWriteSeq = ServletRequestUtils.getIntParameter( multipartRequest, "applyWriteSeq", 0 );
		int applyWorksCnt = ServletRequestUtils.getIntParameter( multipartRequest, "applyWorksCnt", 1 );

		if( applyWriteYmd == null || applyWriteYmd.equals( "" ) ){
			processDiv = "INSERT";

			DecimalFormat df = new DecimalFormat( "00" );
			Calendar currentCal = Calendar.getInstance();
			currentCal.add( currentCal.DATE, 0 );
			applyWriteYmd =
				Integer.toString( currentCal.get( Calendar.YEAR ) ) + df.format( currentCal.get( Calendar.MONTH ) + 1 )
					+ df.format( currentCal.get( Calendar.DAY_OF_MONTH ) );

			applyWriteSeq = myStatService.newApplyWriteSeq( "TMP" );
		}

		String isCompleteStep01 = (String) session.getAttribute( "isCompleteStep01" );

		if( isCompleteStep01 != null ){
			String sessApplyWriteYmd = (String) session.getAttribute( "applyWriteYmd" );
			int sessApplyWriteSeq = (Integer) session.getAttribute( "applyWriteSeq" );
			applyWriteYmd = sessApplyWriteYmd;
			applyWriteSeq = sessApplyWriteSeq;
		}

		StatApplication param = new StatApplication();
		param.setApplyWriteYmd( applyWriteYmd );
		param.setApplyWriteSeq( applyWriteSeq );
		param.setApplyType01( ServletRequestUtils.getStringParameter( multipartRequest, "applyType01", "0" ) );
		param.setApplyType02( ServletRequestUtils.getStringParameter( multipartRequest, "applyType02", "0" ) );
		param.setApplyType03( ServletRequestUtils.getStringParameter( multipartRequest, "applyType03", "0" ) );
		param.setApplyType04( ServletRequestUtils.getStringParameter( multipartRequest, "applyType04", "0" ) );
		param.setApplyType05( ServletRequestUtils.getStringParameter( multipartRequest, "applyType05", "0" ) );
		param.setApplyWorksCnt( applyWorksCnt );
		param.setPrevCnt( prevCnt );
		param.setApplyWorksTitl(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyWorksTitl" ) ) );
		param.setApplyWorksKind(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyWorksKind" ) ) );
		param.setApplyWorksForm(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyWorksForm" ) ) );
		param.setUsexDesc( XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "usexDesc" ) ) );
		param.setApplyReas(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyReas" ) ) );
		param.setCpstAmnt( XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "cpstAmnt" ) ) );

		param.setApplrName(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applrName" ) ) );
		param.setApplrResdCorpNumb(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applrResdCorpNumb" ) ) );
		param.setApplrAddr(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applrAddr" ) ) );
		param.setApplrTelx(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applrTelx" ) ) );

		param.setApplyDivsCd( ServletRequestUtils.getStringParameter( multipartRequest, "applyDivsCd" ) );

		param.setApplyProxyName(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyProxyName" ) ) );
		param.setApplyProxyResdCorpNumb(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyProxyResdCorpNumb" ) ) );
		param.setApplyProxyAddr(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyProxyAddr" ) ) );
		param.setApplyProxyTelx(
			XssUtil.unscript( ServletRequestUtils.getStringParameter( multipartRequest, "applyProxyTelx" ) ) );

		param.setApplyRawCd( ServletRequestUtils.getIntParameter( multipartRequest, "applyRawCd", 0 ) );
		if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
			param.setStatCd( 4 ); // 진행상황 : 보완임시저장
		}else{
			param.setStatCd( 1 ); // 진행상황 : 임시저장
		}
		param.setRgstIdnt( ServletRequestUtils.getStringParameter( multipartRequest, "rgstIdnt" ) );
		param.setModiIdnt( ServletRequestUtils.getStringParameter( multipartRequest, "rgstIdnt" ) );
		System.out.println( "rgstIdnt :::" + ServletRequestUtils.getStringParameter( multipartRequest, "rgstIdnt" ) );
		param.setFileNameCd( ServletRequestUtils.getStringParameters( multipartRequest, "fileNameCd" ) );
		param.setFileDelYn( ServletRequestUtils.getStringParameters( multipartRequest, "fileDelYn" ) );
		param.setHddnFile( ServletRequestUtils.getStringParameters( multipartRequest, "hddnFile" ) );
		param.setFileName( ServletRequestUtils.getStringParameters( multipartRequest, "fileName" ) );
		param.setRealFileName( ServletRequestUtils.getStringParameters( multipartRequest, "realFileName" ) );
		param.setFileSize( ServletRequestUtils.getStringParameters( multipartRequest, "fileSize" ) );
		param.setFilePath( ServletRequestUtils.getStringParameters( multipartRequest, "filePath" ) );

		long startTime = System.currentTimeMillis();

		System.out.println(
			">>>>>>>>>>>>>>>>:::::::시작하는 시간::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + startTime
				+ ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" );

		/* 첨부화일 업로드 시작 */
		int[] attcSeqn = ServletRequestUtils.getIntParameters( multipartRequest, "attcSeqn" );
		String[] fileNameCd = ServletRequestUtils.getStringParameters( multipartRequest, "fileNameCd" );
		String[] fileDelYn = ServletRequestUtils.getStringParameters( multipartRequest, "fileDelYn" );
		String[] hddnFile = ServletRequestUtils.getStringParameters( multipartRequest, "hddnFile" );

		System.out.println( Arrays.toString( attcSeqn ) );
		System.out.println( Arrays.toString( fileNameCd ) );
		System.out.println( Arrays.toString( hddnFile ) );
		System.out.println( Arrays.toString( fileDelYn ) );
		System.out.println( "attcSeqn ::" + attcSeqn );
		System.out.println( "fileNameCd:: " + fileNameCd );
		System.out.println( "fileNameCd:: " + fileNameCd );

		System.out.println( "hddnFile :: " + hddnFile );

		// 첨부파일 중 수정 된 첨부파일에 대한 정보 수집
		List delFileList = new ArrayList();

		for( int i = 0; i < attcSeqn.length; i++ ){
			File delFileInfo = new File();
			if( fileDelYn[i].equals( "Y" ) ){
				if( attcSeqn[i] == 0 ){
					break;
				}
				System.out.println( "ATTCSEQ NO :: " + attcSeqn[i] );
				System.out.println( "FIELNAMECD NO ::" + fileNameCd[i] );
				delFileInfo.setAttcSeqn( attcSeqn[i] );
				delFileInfo.setFileAttcCd( "ST" );
				delFileInfo.setFileNameCd( fileNameCd[i] );
				delFileInfo.setApplyWriteYmd( applyWriteYmd );
				delFileInfo.setApplyWriteSeq( applyWriteSeq );

				delFileList.add( delFileInfo );
			}
		}
		param.setDelFileList( delFileList );

		// 새로 첨부된 첨부파일에 대한 정보 수집
		List fileList = new ArrayList();

		Iterator<String> fileNameIterator = multipartRequest.getFileNames();

		System.out.println( "multipartRequest.getFileNames() :" + multipartRequest.getFileNames() );
		System.out.println( "fileNameIterator22 :::" + fileNameIterator );
		System.out.println( fileNameIterator.hasNext() );
		int newAttcSeqn = commonService.getNewAttcSeqn();

		while( fileNameIterator.hasNext() ){
			String fileName = fileNameIterator.next();
			System.out.println( "INSERT FILE2   +  :" + fileName );
			MultipartFile multiFile = multipartRequest.getFile( fileName );
			System.out.println( multiFile.getSize() );
			if( multiFile.getSize() > 0 ){
				PrpsAttc uploadInfo = FileUploadUtil.uploadPrpsFiles( multiFile, realUploadPath );
				System.out.println( "################ " + uploadInfo.getFILE_NAME() );
				File fileInfo = new File();
				for( int fm = 0; fm < hddnFile.length; fm++ ){
					if( hddnFile[fm].toUpperCase().equals( uploadInfo.getFILE_NAME().toUpperCase() ) ){
						fileInfo.setAttcSeqn( newAttcSeqn );
						fileInfo.setFileAttcCd( "ST" );
						fileInfo.setFileNameCd( fileNameCd[fm] );
						fileInfo.setFileName( hddnFile[fm] );
						fileInfo.setFilePath( uploadInfo.getFILE_PATH() );
						fileInfo.setFileSize( Integer.parseInt( uploadInfo.getFILE_SIZE() ) );
						fileInfo.setRealFileName( uploadInfo.getREAL_FILE_NAME() );
						fileInfo.setApplyWriteYmd( applyWriteYmd );
						fileInfo.setApplyWriteSeq( applyWriteSeq );
						fileInfo.setRgstIdnt( ServletRequestUtils.getStringParameter( multipartRequest, "rgstIdnt" ) );
						hddnFile[fm] = "";
						break;
					}
				}

				fileList.add( fileInfo );
			}
			newAttcSeqn++;
		}
		System.out.println( "INSERT FILE3" );
		// long endTime = System.currentTimeMillis();
		param.setFileList( fileList );
		// System.out.println(">>>>>>>>>>>>>>>>:::::::끝나는
		// 시간::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+endTime+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		// long durationTime = endTime - startTime;
		// System.out.println("milli second : " + durationTime );
		// System.out.println("second = milli second / 1000 : " + (durationTime
		// / 1000));
		boolean isExcel = false;

		// isExcel true를 함으로서 이용신청서 데이터베이스는 수정하지 않고 파일만 수정하겠다.
		if( isCompleteStep01_2 == null ){
			if( actionDiv.equals( "updateFile" ) ){
				isExcel = true;
				if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
					myStatService.updateStatPrps_priv( request, param, isExcel, existYn );
				}else{
					myStatService.updateTmpStatPrps_priv( request, param, isExcel, existYn );
				}
				session.setAttribute( "isCompleteStep01_2", "Y" );
			}
		}

		HashMap<String, Object> resultMap = null;
		if( isCompleteStep01 == null ){
			if( processDiv.equals( "INSERT" ) ){
				myStatService.insertTmpStatPrps_priv( request, param, existYn );
			}else{
				if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
					myStatService.updateStatPrps_priv( request, param, isExcel, existYn );
				}else{
					myStatService.updateTmpStatPrps_priv( request, param, isExcel, existYn );
				}
			}
		}

		if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
			fileList = myStatService.statAttcFile( param );
		}else{
			fileList = myStatService.statAttcFileTmp( param );
		}

		param.setFileList( fileList );

		List<StatApplyWorks> resultList = null;
		boolean isAllEmpty = false;
		File fileInfo = null;
		int writeRow = 0;
		int errRow = 0;

		if( existYn.equals( "Y" ) ){
			if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
				resultMap = (HashMap<String, Object>) myStatService.selectExcelStatApplyWorksInfo( param );

			}else{
				resultMap = (HashMap<String, Object>) myStatService.selectExcelStatApplyWorksInfoTmp( param );
			}
			resultList = (List<StatApplyWorks>) resultMap.get( "result" );
			isAllEmpty = (Boolean) resultMap.get( "isAllEmpty" );
			fileInfo = (File) resultMap.get( "fileInfo" );
			writeRow = (Integer) resultMap.get( "writeRow" );
			errRow = (Integer) resultMap.get( "errRow" );
		}

		ModelAndView mv = new ModelAndView( "stat/statPrpsList_priv" );
		if( actionDiv.equals( "goStep2" ) || actionDiv.equals( "updateFile" ) ){
			srchParam.setApplyWriteYmd( applyWriteYmd );
			srchParam.setApplyWriteSeq( applyWriteSeq );

			// 명세서 리턴
			/*
			 * StatApplyWorks statApplyWorks = new StatApplyWorks();
			 * statApplyWorks.setApplyWriteYmd(applyWriteYmd);
			 * statApplyWorks.setApplyWriteSeq(applyWriteSeq);
			 */
			/*
			 * List statApplyWorksList = myStatService
			 * .statApplyWorksListTmp(statApplyWorks);
			 */
			/*
			 * mv = new ModelAndView("stat/statPrpsList", "statApplyWorksList",
			 * statApplyWorksList)
			 */
			if( existYn.equals( "Y" ) ){
				if( resultList == null && !isAllEmpty ){
					mv.addObject( "message", "첨부파일을 1000건 이하로 입력해주세요." );
				}else if( resultList == null && isAllEmpty ){
					mv.addObject( "message", "첨부파일을 1건 이상 입력해주세요." );
				}else{
					mv.addObject( "statApplyWorksList", resultList );
					mv.addObject( "fileInfo", fileInfo );
				}
				mv.addObject( "writeRow", writeRow );
				mv.addObject( "errRow", errRow );
			}else{
				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd( applyWriteYmd );
				statApplyWorks.setApplyWriteSeq( applyWriteSeq );

				List statApplyWorksList;

				if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
					statApplyWorksList = myStatService.statApplyWorksList( statApplyWorks );
				}else{
					statApplyWorksList = myStatService.statApplyWorksListTmp( statApplyWorks );
				}
				mv.addObject( "statApplyWorksList", statApplyWorksList );
				System.out.println( "statApplyWorksList : " + statApplyWorksList );
			}
			StatApplyWorks statApplyWorks = new StatApplyWorks();
			statApplyWorks.setApplyWriteYmd( applyWriteYmd );
			statApplyWorks.setApplyWriteSeq( applyWriteSeq );

			List statApplyWorksList;

			if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
				statApplyWorksList = myStatService.statApplyWorksList( statApplyWorks );
			}else{
				statApplyWorksList = myStatService.statApplyWorksListTmp( statApplyWorks );
			}
			mv.addObject( "statApplyWorksList", statApplyWorksList );
			System.out.println( "statApplyWorksList : " + statApplyWorksList );
			mv.addObject( "srchParam", srchParam );
			mv.addObject( "srchVO", srchVO );
			mv.addObject( "applyWorksCnt", applyWorksCnt );
			mv.addObject( "existYn", existYn );
			mv.addObject( "isChk", "01" );
			mv.addObject( "userInfo", userInfo );
			mv.addObject( "isMy", isMy );
			if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
				mv.addObject( "statCd", 4 );
			}else{
				mv.addObject( "statCd", 1 );
			}

		}else if( actionDiv.equals( "goList" ) ){
			if( isMy.equals( "" ) ){
				mv = new ModelAndView( "redirect:/stat/statSrch.do" );
			}else{
				mv = new ModelAndView( "redirect:/myStat/statRsltInqrList.do" );
			}

		}

		session.setAttribute( "isCompleteStep01", "Y" );
		session.setAttribute( "applyWriteYmd", applyWriteYmd );
		session.setAttribute( "applyWriteSeq", applyWriteSeq );

		return mv;
	}

	@RequestMapping( value = "/ww", method = RequestMethod.POST )
	public ModelAndView ww( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{
		HttpSession session = request.getSession();

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Iterator<String> fileNames = multipartRequest.getFileNames();

		while( fileNames.hasNext() ){
			String fileName = fileNames.next();
			System.out.println( fileName );
			MultipartFile mFile = multipartRequest.getFile( fileName );
			System.out.println( mFile.getSize() );

		}
		return null;
	}

	// 법정허락 신청 step1 임시저장
	@RequestMapping( value = "/statPrpsList_privU", method = RequestMethod.POST )
	public ModelAndView tmpStatPrps1SaveDo_privU( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		HttpSession session = request.getSession();

		// remove 세션 없애기
		session.removeAttribute( "isCompleteStep01_2" );

		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );

		StatApplication srchParam = new StatApplication();

		MlStatWorksVO srchVO = new MlStatWorksVO();

		String isMy = ServletRequestUtils.getStringParameter( multipartRequest, "isMy", "" );

		String statCd = ServletRequestUtils.getStringParameter( multipartRequest, "stat_cd", "1" );

		Map params = new HashMap();
		User user = SessionUtil.getSession( request );
		params.put( "userIdnt", user.getUserIdnt() );

		// 사용자 조회
		Map userInfo = userService.selectUserInfo( params );

		if( isMy.equals( "" ) ){
			/* 조회조건 str */
			srchVO.setPageNum( ServletRequestUtils.getStringParameter( multipartRequest, "pageNo" ) );
			srchVO.setSearchCondition( ServletRequestUtils.getStringParameter( multipartRequest, "searchCondition" ) );
			srchVO.setSearchKeyword1( ServletRequestUtils.getStringParameter( multipartRequest, "searchKeyword1" ) );
			/* 조회조건 end */
		}else{
			/* 조회조건 str */
			srchParam.setSrchApplyType( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyType" ) );
			srchParam.setSrchApplyFrDt( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyFrDt" ) );
			srchParam.setSrchApplyToDt( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyToDt" ) );
			srchParam.setSrchApplyWorksTitl(
				ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyWorksTitl" ) );
			srchParam.setSrchStatCd( ServletRequestUtils.getIntParameter( multipartRequest, "srchStatCd", 0 ) );
			srchParam.setNowPage( ServletRequestUtils.getIntParameter( multipartRequest, "page_no", 1 ) );
			srchParam.setSrchRgstIdnt( user.getUserIdnt() );
			/* 조회조건 end */
		}

		/*
		 * 조회조건 str srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyType"));
		 * srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyFrDt"));
		 * srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyToDt"));
		 * srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(
		 * multipartRequest, "srchApplyWorksTitl"));
		 * srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(
		 * multipartRequest, "srchStatCd", 0));
		 * srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no",
		 * 1)); 조회조건 end
		 */

		String existYn = ServletRequestUtils.getStringParameter( multipartRequest, "existYn" ); // 이용신청명세서 첨부여부

		String actionDiv = ServletRequestUtils.getStringParameter( multipartRequest, "action_div" );

		String processDiv = "UPDATE";

		String applyWriteYmd = ServletRequestUtils.getStringParameter( multipartRequest, "applyWriteYmd" );
		int applyWriteSeq = ServletRequestUtils.getIntParameter( multipartRequest, "applyWriteSeq", 0 );
		int applyWorksCnt = ServletRequestUtils.getIntParameter( multipartRequest, "applyWorksCnt", 1 );

		if( applyWriteYmd == null || applyWriteYmd.equals( "" ) ){
			processDiv = "INSERT";

			DecimalFormat df = new DecimalFormat( "00" );
			Calendar currentCal = Calendar.getInstance();
			currentCal.add( currentCal.DATE, 0 );
			applyWriteYmd =
				Integer.toString( currentCal.get( Calendar.YEAR ) ) + df.format( currentCal.get( Calendar.MONTH ) + 1 )
					+ df.format( currentCal.get( Calendar.DAY_OF_MONTH ) );

			applyWriteSeq = myStatService.newApplyWriteSeq( "TMP" );
		}

		// String isCompleteStep01 = (String)session.getAttribute("isCompleteStep01");
		String isCompleteStep01_1 = (String) session.getAttribute( "isCompleteStep01_1" );

		String sessApplyWriteYmd = (String) session.getAttribute( "applyWriteYmd" );
		int sessApplyWriteSeq = (Integer) session.getAttribute( "applyWriteSeq" );
		applyWriteYmd = sessApplyWriteYmd;
		applyWriteSeq = sessApplyWriteSeq;

		StatApplication param = new StatApplication();
		param.setApplyWriteYmd( applyWriteYmd );
		param.setApplyWriteSeq( applyWriteSeq );

		param.setFileNameCd( ServletRequestUtils.getStringParameters( multipartRequest, "fileNameCd" ) );
		param.setFileDelYn( ServletRequestUtils.getStringParameters( multipartRequest, "fileDelYn" ) );
		param.setHddnFile( ServletRequestUtils.getStringParameters( multipartRequest, "hddnFile" ) );
		param.setFileName( ServletRequestUtils.getStringParameters( multipartRequest, "fileName" ) );
		param.setRealFileName( ServletRequestUtils.getStringParameters( multipartRequest, "realFileName" ) );
		param.setFileSize( ServletRequestUtils.getStringParameters( multipartRequest, "fileSize" ) );
		param.setFilePath( ServletRequestUtils.getStringParameters( multipartRequest, "filePath" ) );

		param.setRgstIdnt( ServletRequestUtils.getStringParameter( multipartRequest, "rgstIdnt" ) );
		param.setModiIdnt( ServletRequestUtils.getStringParameter( multipartRequest, "rgstIdnt" ) );

		// long startTime = System.currentTimeMillis();

		// System.out.println(">>>>>>>>>>>>>>>>:::::::시작하는
		// 시간::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+startTime+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		/* 첨부화일 업로드 시작 */
		int[] attcSeqn = ServletRequestUtils.getIntParameters( multipartRequest, "attcSeqn" );
		String[] fileNameCd = ServletRequestUtils.getStringParameters( multipartRequest, "fileNameCd" );
		String[] fileDelYn = ServletRequestUtils.getStringParameters( multipartRequest, "fileDelYn" );
		String[] hddnFile = ServletRequestUtils.getStringParameters( multipartRequest, "hddnFile" );

		// 첨부파일 중 수정 된 첨부파일에 대한 정보 수집
		List delFileList = new ArrayList();

		for( int i = 0; i < attcSeqn.length; i++ ){
			File delFileInfo = new File();
			if( fileDelYn[i].equals( "Y" ) ){
				if( attcSeqn[i] == 0 ){
					break;
				}
				System.out.println( attcSeqn[i] );
				System.out.println( fileNameCd[i] );
				delFileInfo.setAttcSeqn( attcSeqn[i] );
				delFileInfo.setFileAttcCd( "ST" );
				delFileInfo.setFileNameCd( fileNameCd[i] );
				delFileInfo.setApplyWriteYmd( applyWriteYmd );
				delFileInfo.setApplyWriteSeq( applyWriteSeq );

				delFileList.add( delFileInfo );
			}
		}
		param.setDelFileList( delFileList );

		// 새로 첨부된 첨부파일에 대한 정보 수집
		List fileList = new ArrayList();

		Iterator fileNameIterator = multipartRequest.getFileNames();
		System.out.println( "fileNameIterator3 :::" + fileNameIterator );
		int newAttcSeqn = commonService.getNewAttcSeqn();

		while( fileNameIterator.hasNext() ){
			MultipartFile multiFile = multipartRequest.getFile( (String) fileNameIterator.next() );
			if( multiFile.getSize() > 0 ){
				PrpsAttc uploadInfo = FileUploadUtil.uploadPrpsFiles( multiFile, realUploadPath );
				System.out.println( "################ " + uploadInfo.getFILE_NAME() );
				File fileInfo = new File();
				for( int fm = 0; fm < hddnFile.length; fm++ ){
					if( hddnFile[fm].toUpperCase().equals( uploadInfo.getFILE_NAME().toUpperCase() ) ){
						fileInfo.setAttcSeqn( newAttcSeqn );
						fileInfo.setFileAttcCd( "ST" );
						fileInfo.setFileNameCd( fileNameCd[fm] );
						fileInfo.setFileName( hddnFile[fm] );
						fileInfo.setFilePath( uploadInfo.getFILE_PATH() );
						fileInfo.setFileSize( Integer.parseInt( uploadInfo.getFILE_SIZE() ) );
						fileInfo.setRealFileName( uploadInfo.getREAL_FILE_NAME() );
						fileInfo.setApplyWriteYmd( applyWriteYmd );
						fileInfo.setApplyWriteSeq( applyWriteSeq );
						fileInfo.setRgstIdnt( ServletRequestUtils.getStringParameter( multipartRequest, "rgstIdnt" ) );
						hddnFile[fm] = "";
						break;
					}
				}

				fileList.add( fileInfo );
			}
			newAttcSeqn++;
		}

		// long endTime = System.currentTimeMillis();
		param.setFileList( fileList );
		// System.out.println(">>>>>>>>>>>>>>>>:::::::끝나는
		// 시간::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+endTime+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		// long durationTime = endTime - startTime;
		// System.out.println("milli second : " + durationTime );
		// System.out.println("second = milli second / 1000 : " + (durationTime
		// / 1000));

		boolean isExcel = false;

		if( isCompleteStep01_1 == null ){
			if( actionDiv.equals( "updateFile" ) ){
				isExcel = true;
				if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
					myStatService.updateStatPrps_priv( request, param, isExcel, existYn );
				}else{
					myStatService.updateTmpStatPrps_priv( request, param, isExcel, existYn );
				}
				session.setAttribute( "isCompleteStep01_1", "Y" );
			}
		}
		HashMap<String, Object> resultMap = null;
		List<StatApplyWorks> resultList = null;
		boolean isAllEmpty = false;
		File fileInfo = null;
		int writeRow = 0;
		int errRow = 0;

		if( existYn.equals( "Y" ) ){
			if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
				resultMap = (HashMap<String, Object>) myStatService.selectExcelStatApplyWorksInfo( param );

			}else{
				resultMap = (HashMap<String, Object>) myStatService.selectExcelStatApplyWorksInfoTmp( param );
			}
			resultList = (List<StatApplyWorks>) resultMap.get( "result" );
			isAllEmpty = (Boolean) resultMap.get( "isAllEmpty" );
			fileInfo = (File) resultMap.get( "fileInfo" );
			writeRow = (Integer) resultMap.get( "writeRow" );
			errRow = (Integer) resultMap.get( "errRow" );
		}

		ModelAndView mv = new ModelAndView( "stat/statPrpsList_priv" );
		if( actionDiv.equals( "goStep2" ) || actionDiv.equals( "updateFile" ) ){
			srchParam.setApplyWriteYmd( applyWriteYmd );
			srchParam.setApplyWriteSeq( applyWriteSeq );

			if( existYn.equals( "Y" ) ){
				if( resultList == null && !isAllEmpty ){
					mv.addObject( "message", "첨부파일을 1000건 이하로 입력해주세요." );
				}else if( resultList == null && isAllEmpty ){
					mv.addObject( "message", "첨부파일을 1건 이상 입력해주세요." );
				}else{
					mv.addObject( "statApplyWorksList", resultList );
					mv.addObject( "fileInfo", fileInfo );
				}
				mv.addObject( "writeRow", writeRow );
				mv.addObject( "errRow", errRow );
			}

			mv.addObject( "statCd", 1 );
			mv.addObject( "applyWorksCnt", applyWorksCnt );
			mv.addObject( "srchParam", srchParam );
			mv.addObject( "existYn", existYn );
			mv.addObject( "srchVO", srchVO );
			mv.addObject( "isChk", "02" );
			mv.addObject( "userInfo", userInfo );
			mv.addObject( "isMy", isMy );

			if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
				mv.addObject( "statCd", 4 );
			}else{
				mv.addObject( "statCd", 1 );
			}

		}else if( actionDiv.equals( "goList" ) ){
			if( isMy.equals( "" ) ){
				mv = new ModelAndView( "redirect:/stat/statSrch.do" );
			}else{
				mv = new ModelAndView( "redirect:/myStat/statRsltInqrList.do" );
			}
		}
		return mv;
	}

	// 법정허락 신청_priv step2 임시저장
	@RequestMapping( value = "/statPrpsDetl_priv" )
	public ModelAndView tmpStatPrps2SaveDo_priv( HttpServletRequest request,		HttpServletResponse respone ) throws Exception{

		HttpSession session = request.getSession();

		StatApplication srchParam = new StatApplication();
		MlStatWorksVO srchVO = new MlStatWorksVO();

		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );

		String isMy = ServletRequestUtils.getStringParameter( multipartRequest, "isMy", "" );

		String statCd = ServletRequestUtils.getStringParameter( multipartRequest, "stat_cd", "1" );

		Map params = new HashMap();
		User user = SessionUtil.getSession( request );
		params.put( "userIdnt", user.getUserIdnt() );
		// 사용자 조회
		Map userInfo = userService.selectUserInfo( params );

		if( isMy.equals( "" ) ){
			/* 조회조건 str */
			srchVO.setPageNum( ServletRequestUtils.getStringParameter( multipartRequest, "pageNo" ) );
			srchVO.setSearchCondition( ServletRequestUtils.getStringParameter( multipartRequest, "searchCondition" ) );
			srchVO.setSearchKeyword1( ServletRequestUtils.getStringParameter( multipartRequest, "searchKeyword1" ) );
			srchVO.setSearchWorksId( ServletRequestUtils.getStringParameter( multipartRequest, "worksId" ) );
		}else{
			/* 조회조건 str */
			srchParam.setSrchApplyType( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyType" ) );
			srchParam.setSrchApplyFrDt( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyFrDt" ) );
			srchParam.setSrchApplyToDt( ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyToDt" ) );
			srchParam.setSrchApplyWorksTitl(
				ServletRequestUtils.getStringParameter( multipartRequest, "srchApplyWorksTitl" ) );
			srchParam.setSrchStatCd( ServletRequestUtils.getIntParameter( multipartRequest, "srchStatCd", 0 ) );
			srchParam.setNowPage( ServletRequestUtils.getIntParameter( multipartRequest, "page_no", 1 ) );
			srchParam.setSrchRgstIdnt( user.getUserIdnt() );
			/* 조회조건 end */
		}
		String existYn = ServletRequestUtils.getStringParameter( multipartRequest, "existYn" ); // 이용신청명세서 첨부여부

		String actionDiv = ServletRequestUtils.getStringParameter( multipartRequest, "action_div" );

		String applyWriteYmd = ServletRequestUtils.getStringParameter( multipartRequest, "applyWriteYmd" );
		int applyWriteSeq = ServletRequestUtils.getIntParameter( multipartRequest, "applyWriteSeq", 0 );

		List paramList = new ArrayList();

		if( !existYn.equals( "Y" ) ){
			String applyType01 = multipartRequest.getParameter( "applyType01" );
			String applyType02 = multipartRequest.getParameter( "applyType02" );
			String applyType03 = multipartRequest.getParameter( "applyType03" );
			String applyType04 = multipartRequest.getParameter( "applyType04" );
			String applyType05 = multipartRequest.getParameter( "applyType05" );
			String[] worksTitl = XssUtil.unscriptArr( multipartRequest.getParameterValues( "worksTitl" ) );
			String[] worksKind = XssUtil.unscriptArr( multipartRequest.getParameterValues( "worksKind" ) );
			String[] worksForm = XssUtil.unscriptArr( multipartRequest.getParameterValues( "worksForm" ) );
			String[] worksDesc = XssUtil.unscriptArr( multipartRequest.getParameterValues( "worksDesc" ) );
			String[] publYmd = XssUtil.unscriptArr( multipartRequest.getParameterValues( "publYmd" ) );
			String[] publNatn = XssUtil.unscriptArr( multipartRequest.getParameterValues( "publNatn" ) );
			String publMediCd = multipartRequest.getParameter( "publMediCd" );
			String[] publMediEtc = XssUtil.unscriptArr( multipartRequest.getParameterValues( "publMediEtc" ) );
			String[] publMedi = XssUtil.unscriptArr( multipartRequest.getParameterValues( "publMedi" ) );
			String[] coptHodrName = XssUtil.unscriptArr( multipartRequest.getParameterValues( "coptHodrName" ) );
			String[] coptHodrTelxNumb =
				XssUtil.unscriptArr( multipartRequest.getParameterValues( "coptHodrTelxNumb" ) );
			String[] coptHodrAddr = XssUtil.unscriptArr( multipartRequest.getParameterValues( "coptHodrAddr" ) );

			for( int i = 0; i < worksTitl.length; i++ ){
				StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
				statApplyWorksInfo.setApplyWriteYmd( applyWriteYmd );
				statApplyWorksInfo.setApplyWriteSeq( applyWriteSeq );
				statApplyWorksInfo.setWorksSeqn( i + 1 );
				statApplyWorksInfo.setApplyType01( StringUtil.nullToEmpty( applyType01.substring( i, i + 1 ) ) );
				statApplyWorksInfo.setApplyType02( StringUtil.nullToEmpty( applyType02.substring( i, i + 1 ) ) );
				statApplyWorksInfo.setApplyType03( StringUtil.nullToEmpty( applyType03.substring( i, i + 1 ) ) );
				statApplyWorksInfo.setApplyType04( StringUtil.nullToEmpty( applyType04.substring( i, i + 1 ) ) );
				statApplyWorksInfo.setApplyType05( StringUtil.nullToEmpty( applyType05.substring( i, i + 1 ) ) );
				statApplyWorksInfo.setWorksTitl( StringUtil.nullToEmpty( worksTitl[i] ) );
				statApplyWorksInfo.setWorksKind( StringUtil.nullToEmpty( worksKind[i] ) );
				statApplyWorksInfo.setWorksForm( StringUtil.nullToEmpty( worksForm[i] ) );
				statApplyWorksInfo.setWorksDesc( StringUtil.nullToEmpty( worksDesc[i] ) );
				statApplyWorksInfo.setPublYmd( StringUtil.nullToEmpty( publYmd[i] ) );
				statApplyWorksInfo.setPublNatn( StringUtil.nullToEmpty( publNatn[i] ) );
				String newPublMediCd = publMediCd.substring( 0, publMediCd.indexOf( '!' ) );
				publMediCd = publMediCd.substring( publMediCd.indexOf( '!' ) + 1 );
				statApplyWorksInfo.setPublMediCd( Integer.parseInt( newPublMediCd ) );
				statApplyWorksInfo.setPublMediEtc( StringUtil.nullToEmpty( publMediEtc[i] ) );
				statApplyWorksInfo.setPublMedi( StringUtil.nullToEmpty( publMedi[i] ) );
				statApplyWorksInfo.setCoptHodrName( StringUtil.nullToEmpty( coptHodrName[i] ) );
				statApplyWorksInfo.setCoptHodrTelxNumb( StringUtil.nullToEmpty( coptHodrTelxNumb[i] ) );
				statApplyWorksInfo.setCoptHodrAddr( StringUtil.nullToEmpty( coptHodrAddr[i] ) );

				paramList.add( statApplyWorksInfo );
			}
		}
		ModelAndView mv = new ModelAndView( "stat/statPrpsDetl_priv" );
		if( actionDiv.equals( "goStep3" ) || actionDiv.equals( "goList" ) ){
			srchParam.setApplyWriteYmd( applyWriteYmd );
			srchParam.setApplyWriteSeq( applyWriteSeq );

			params.put( "userIdnt", user.getUserIdnt() );

			// 사용자 조회
			StatApplication param = new StatApplication();
			param.setApplyWriteYmd( ServletRequestUtils.getStringParameter( multipartRequest, "applyWriteYmd" ) );
			param.setApplyWriteSeq( ServletRequestUtils.getIntParameter( multipartRequest, "applyWriteSeq", 0 ) );
			if( statCd.equals( "3" ) || statCd.equals( "4" ) ){
				StatApplication statApplication = myStatService.statApplication( param );

				String isCompleteStep02 = (String) session.getAttribute( "isCompleteStep02" );

				if( isCompleteStep02 == null ){
					if( existYn.equals( "Y" ) ){
						List fileList2 = myStatService.statAttcFile( param );
						statApplication.setFileList( fileList2 );
						// 등록한 엑셀 파일 명세서 테이블에 넣기
						myStatService.insertExcelStatApplyWorksInfo( statApplication );
					}else{
						List fileList2 = myStatService.statAttcFile( param );
						statApplication.setFileList( fileList2 );
						myStatService.insertStatApplyWorksList( paramList );
					}
				}
				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd( applyWriteYmd );
				statApplyWorks.setApplyWriteSeq( applyWriteSeq );

				List statApplyWorksList = myStatService.statApplyWorksList( statApplyWorks );

				mv = new ModelAndView( "stat/statPrpsDetl_priv" );
				mv.addObject( "userInfo", userInfo );
				mv.addObject( "statApplication", statApplication );
				mv.addObject( "statApplyWorksList", statApplyWorksList );
				mv.addObject( "srchParam", srchParam );
				mv.addObject( "srchVO", srchVO );
				mv.addObject( "isMy", isMy );
				if( actionDiv.equals( "goList" ) ){
					if( isMy.equals( "" ) ){
						mv = new ModelAndView( "redirect:/stat/statSrch.do" );
					}else{
						mv = new ModelAndView( "redirect:/myStat/statRsltInqrList.do" );
					}
				}
			}else{
				StatApplication statApplication = myStatService.statApplicationTmp( param );
				String isCompleteStep02 = (String) session.getAttribute( "isCompleteStep02" );
				if( isCompleteStep02 == null ){
					if( existYn.equals( "Y" ) ){
						List fileList2 = myStatService.statAttcFileTmp( param );
						statApplication.setFileList( fileList2 );
						// 등록한 엑셀 파일 명세서 테이블에 넣기
						myStatService.insertExcelStatApplyWorksInfoTmp( statApplication );
					}else{
						List fileList2 = myStatService.statAttcFileTmp( param );
						statApplication.setFileList( fileList2 );
						myStatService.insertStatApplyWorksList_tmp( paramList, "P" );
					}
				}
				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd( applyWriteYmd );
				statApplyWorks.setApplyWriteSeq( applyWriteSeq );

				List statApplyWorksList = myStatService.statApplyWorksListTmp( statApplyWorks );

				mv = new ModelAndView( "stat/statPrpsDetl_priv" );
				mv.addObject( "userInfo", userInfo );
				mv.addObject( "statApplication", statApplication );
				mv.addObject( "statApplyWorksList", statApplyWorksList );
				mv.addObject( "srchParam", srchParam );
				mv.addObject( "srchVO", srchVO );
				mv.addObject( "isMy", isMy );
				if( actionDiv.equals( "goList" ) ){
					if( isMy.equals( "" ) ){
						mv = new ModelAndView( "redirect:/stat/statSrch.do" );
					}else{
						mv = new ModelAndView( "redirect:/myStat/statRsltInqrList.do" );
					}
				}
			}
		}
		session.setAttribute( "isCompleteStep02", "Y" );
		return mv;
	}

	@RequestMapping( value = "/statFileSave.do", method = RequestMethod.POST )
	@ResponseBody
	public ModelAndView statFileSave( HttpServletRequest request,	HttpServletResponse respone,@RequestParam( "fileObj" ) MultipartFile file,@RequestParam("fileName") String fn) throws Exception{
		
	
		HttpSession session = request.getSession();

		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest;
		multipartRequest = multipartResolver.resolveMultipart( request );

		 String [] charsets = {"UTF-8","EUC-KR","ISO-8859-1", "CP1251", "KSC5601"};

		  for ( String charset: charsets ) {
			 String text = URLEncoder.encode(file.getOriginalFilename(), charset);
			
		  }
		
		
		User user = SessionUtil.getSession( request );
		
		String extName = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf( "." ),file.getOriginalFilename().length() );
		String fileName2=  URLDecoder.decode( ServletRequestUtils.getStringParameter( multipartRequest, "fileName", "" ), "UTF-8" );
		System.out.println( "fileName2 :::" + fileName2 );
		System.out.println( fileName2.substring(fileName2.lastIndexOf( "\\" )+1,fileName2.length() ));
		fileName2= fileName2.substring(fileName2.lastIndexOf( "\\" )+1,fileName2.length() );
		int newAttcSeqn = commonService.getNewAttcSeqn();
		String fileType = ServletRequestUtils.getStringParameter( multipartRequest, "fileType", "" );
		String attchcd = "ST";
		//String fileName = file.getOriginalFilename();
		String fileName = fileName2;
		String saveFileName = genSaveFileName( extName );
		String path = writeFile( file, saveFileName );
		int fileSize = (int) file.getSize();
		String applyWriteYmd = "";
		String sessApplyWriteYmd = (String) session.getAttribute( "applyWriteYmd" );
		int sessApplyWriteSeq = (Integer) session.getAttribute( "applyWriteSeq" );

		DecimalFormat df = new DecimalFormat( "00" );
		Calendar currentCal = Calendar.getInstance();
		currentCal.add( currentCal.DATE, 0 );
		applyWriteYmd = Integer.toString( currentCal.get( Calendar.YEAR ) )	+ df.format( currentCal.get( Calendar.MONTH ) + 1 ) + df.format( currentCal.get( Calendar.DAY_OF_MONTH ) );

		int applyWriteSeq = myStatService.newApplyWriteSeq( "TMP" );

		List FileList = new ArrayList();

		File fileInfo = new File();

		fileInfo.setApplyWriteSeq( sessApplyWriteSeq );
		fileInfo.setApplyWriteYmd( sessApplyWriteYmd );
		fileInfo.setAttcSeqn( newAttcSeqn );
		fileInfo.setRgstIdnt( user.getUserIdnt() );
		fileInfo.setFilePath( path );
		fileInfo.setFileAttcCd( attchcd );
		fileInfo.setFileName( fileName );
		fileInfo.setFileSize( fileSize );
		fileInfo.setRealFileName( saveFileName );
		fileInfo.setFileNameCd( fileType );
		/* new String(fileName.getBytes("euc-kr"), "utf-8") */

		System.out.println( "1" + newAttcSeqn );
		System.out.println( "2" + applyWriteYmd );
		System.out.println( "3" + applyWriteSeq );
		FileList.add( fileInfo );

		myStatService.insertTmpStatPrps_new( FileList );

		return null;
	}

	private String genSaveFileName( String extName ){
		String fileName = "";

		Calendar calendar = Calendar.getInstance();
		fileName += calendar.get( Calendar.YEAR );
		fileName += calendar.get( Calendar.MONTH );
		fileName += calendar.get( Calendar.DATE );
		fileName += calendar.get( Calendar.HOUR );
		fileName += calendar.get( Calendar.MINUTE );
		fileName += calendar.get( Calendar.SECOND );
		fileName += calendar.get( Calendar.MILLISECOND );
		fileName += extName;

		return fileName;
	}

	private String writeFile( MultipartFile multipartFile,
		String saveFileName ) throws IOException{
		boolean result = false;
		String path = realUploadPath;
		/* String path = "/upload/"; */
		byte[] data = multipartFile.getBytes();
		FileOutputStream fos = new FileOutputStream( path + saveFileName );
		fos.write( data );
		fos.close();

		return path;
	}

	@RequestMapping( value = "/statPrpsDetl2" )
	public ModelAndView tmptosave( HttpServletRequest request,
		HttpServletResponse respone ) throws Exception{

		ModelAndView mv = new ModelAndView( "stat/statPrpsDetl" );
		StatApplication srchParam = new StatApplication();
		String isMy = ServletRequestUtils.getStringParameter( request, "isMy", "" );

		String statCd = ServletRequestUtils.getStringParameter( request, "stat_cd", "1" );

		Map params = new HashMap();
		User user = SessionUtil.getSession( request );
		params.put( "userIdnt", user.getUserIdnt() );
		// 사용자 조회
		Map userInfo = userService.selectUserInfo( params );

		MlStatWorksVO srchVO = new MlStatWorksVO();

		if( isMy.equals( "" ) ){
			/* 조회조건 str */
			srchVO.setPageNum( ServletRequestUtils.getStringParameter( request, "pageNo" ) );
			srchVO.setSearchCondition( ServletRequestUtils.getStringParameter( request, "searchCondition" ) );
			srchVO.setSearchKeyword1( ServletRequestUtils.getStringParameter( request, "searchKeyword1" ) );
		}else{
			/* 조회조건 str */
			srchParam.setSrchApplyType( ServletRequestUtils.getStringParameter( request, "srchApplyType" ) );
			srchParam.setSrchApplyFrDt( ServletRequestUtils.getStringParameter( request, "srchApplyFrDt" ) );
			srchParam.setSrchApplyToDt( ServletRequestUtils.getStringParameter( request, "srchApplyToDt" ) );
			srchParam.setSrchApplyWorksTitl( ServletRequestUtils.getStringParameter( request, "srchApplyWorksTitl" ) );
			srchParam.setSrchStatCd( ServletRequestUtils.getIntParameter( request, "srchStatCd", 0 ) );
			srchParam.setNowPage( ServletRequestUtils.getIntParameter( request, "page_no", 1 ) );
			srchParam.setSrchRgstIdnt( user.getUserIdnt() );
			/* 조회조건 end */
		}

		String existYn = ServletRequestUtils.getStringParameter( request, "existYn" ); // 이용신청명세서 첨부여부

		String actionDiv = ServletRequestUtils.getStringParameter( request, "action_div" );

		String applyWriteYmd = ServletRequestUtils.getStringParameter( request, "applyWriteYmd" );
		int applyWriteSeq = ServletRequestUtils.getIntParameter( request, "applyWriteSeq", 0 );

		srchParam.setApplyWriteYmd( applyWriteYmd );
		srchParam.setApplyWriteSeq( applyWriteSeq );

		StatApplication param = new StatApplication();
		param.setApplyWriteYmd( ServletRequestUtils.getStringParameter( request, "applyWriteYmd" ) );
		param.setApplyWriteSeq( ServletRequestUtils.getIntParameter( request, "applyWriteSeq", 0 ) );

		StatApplication statApplication = myStatService.statApplicationTmp( param );

		/*
		 * List fileList2 = myStatService.statAttcFileTmp(param);
		 * statApplication.setFileList(fileList2);
		 */

		StatApplyWorks statApplyWorks = new StatApplyWorks();
		statApplyWorks.setApplyWriteYmd( applyWriteYmd );
		statApplyWorks.setApplyWriteSeq( applyWriteSeq );

		List statApplyWorksList = myStatService.statApplyWorksListTmp( statApplyWorks );

		List<File> fileList2 = myStatService.statAttcFileTmp( param );
		
		mv = new ModelAndView( "stat/statPrpsDetl" );
		mv.addObject( "userInfo", userInfo );
		mv.addObject( "statApplication", statApplication );
		mv.addObject( "statApplyWorksList", statApplyWorksList );
		mv.addObject( "srchParam", srchParam );
		mv.addObject( "srchVO", srchVO );
		mv.addObject( "isMy", isMy );
		mv.addObject( "fileList2", fileList2 );

		/*
		 * ModelAndView mv = new ModelAndView("stat/statPrpsDetl"); StatApplication
		 * param = new StatApplication();
		 * param.setApplyWriteYmd(ServletRequestUtils.getStringParameter( request,
		 * "applyWriteYmd"));
		 * param.setApplyWriteSeq(ServletRequestUtils.getIntParameter(request,
		 * "applyWriteSeq", 0)); // StatApplication statApplication =
		 * myStatService.statApplication(param); List<File> fileList2 =
		 * myStatService.statAttcFileTmp(param);
		 */

		/* mv.addObject( "fileList2" ,fileList2); */

		return mv;

	}

}
