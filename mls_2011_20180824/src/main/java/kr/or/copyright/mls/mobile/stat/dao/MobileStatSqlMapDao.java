package kr.or.copyright.mls.mobile.stat.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.mobile.stat.model.MobileStat;
import kr.or.copyright.mls.mobile.stat.model.MobileStatFile;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class MobileStatSqlMapDao extends SqlMapClientDaoSupport implements MobileStatDao {

	public List<MobileStat> selectStat(MobileStat mobileStat){
		return getSqlMapClientTemplate().queryForList("mobileStat.selectStat", mobileStat);
	}
	public int countStat(Map params){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("mobileStat.countStat", params));
	}
	public List<MobileStat> detailStat(int bordSeqn){
		return getSqlMapClientTemplate().queryForList("mobileStat.detailStat", bordSeqn);
	}
	public List<MobileStatFile> fileSelectStat(int bordSeqn){
		return getSqlMapClientTemplate().queryForList("mobileStat.fileSelectStat", bordSeqn);
	}
}
