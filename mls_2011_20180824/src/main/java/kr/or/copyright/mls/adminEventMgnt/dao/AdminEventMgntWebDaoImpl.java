package kr.or.copyright.mls.adminEventMgnt.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminEventMgnt.model.MlEventItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;

@Repository
public class AdminEventMgntWebDaoImpl extends SqlMapClientDaoSupport implements AdminEventMgntWebDao{

	@Autowired
	public AdminEventMgntWebDaoImpl(SqlMapClient sqlMapClient) {
		super();
		setSqlMapClient(sqlMapClient);
	}
	
	public void addMultiQustItemMgnt(Map map) {
		getSqlMapClientTemplate().insert("eventMgnt.addMultiQustItemMgnt", map);
	}

	public Map getMultiQustItemMgntSeq(Map map) {
		return (HashMap) getSqlMapClientTemplate().queryForObject("eventMgnt.getMultiQustItemMgntSeq", map);
	}

	public void addMultiQustItemMgntChoice(Map map) {
		getSqlMapClientTemplate().insert("eventMgnt.addMultiQustItemMgntChoice", map);
	}

	public List getEventItemList(MlEventItem vo) {
		return getSqlMapClientTemplate().queryForList("eventMgnt.getEventItemList",vo);
	}

	public List getEventItemChoiceList(MlEventItem vo) {
		return getSqlMapClientTemplate().queryForList("eventMgnt.getEventItemChoiceList", vo);
	}

	public void addEventCorans(Map map) throws Exception {
		getSqlMapClientTemplate().insert("eventMgnt.addEventCorans", map);
	}

	public void uptCoransSetN(MlEventItem vo) {
		getSqlMapClientTemplate().update("eventMgnt.uptCoransSetN",vo);
	}

	public void uptCoransSetY(Map map) {
		getSqlMapClientTemplate().update("eventMgnt.uptCoransSetY", map);
	}

	public void delEventCorans(MlEventItem vo) {
		getSqlMapClientTemplate().delete("eventMgnt.delEventCorans",vo);
	}

	public void delMlEventCorans(MlEventItem vo) {
		getSqlMapClientTemplate().delete("eventMgnt.delMlEventCorans", vo);
	}

	public void delMlEventItemChoice(MlEventItem vo) {
		getSqlMapClientTemplate().delete("eventMgnt.delMlEventItemChoice",vo);
	}

	public void delMlEventItem(MlEventItem vo) {
		getSqlMapClientTemplate().delete("eventMgnt.delMlEventItem",vo);
	}

	public void uptEventItemSeqn(MlEventItem vv) {
		getSqlMapClientTemplate().update("eventMgnt.uptEventItemSeqn",vv);
	}

	public MlEventItem getEventItemDetl(MlEventItem vo) {
		return (MlEventItem) getSqlMapClientTemplate().queryForObject("eventMgnt.getEventItemDetl",vo);
	}

	public void uptMultiQustItemMgnt(MlEventItem vo) {
		getSqlMapClientTemplate().update("eventMgnt.uptMultiQustItemMgnt", vo);
	}

	public String getEventTitle(MlEventItem vo) {
		return (String) getSqlMapClientTemplate().queryForObject("eventMgnt.getEventTitle",vo);
	}

}
