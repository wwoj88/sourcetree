package kr.or.copyright.mls.console;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import egovframework.rte.fdl.cmmn.exception.handler.ExceptionHandler;

/**
 * @Class Name : EgovComExcepHndlr.java
 * @Description : 공�?��??�??��?? exception �?�? ?��????
 * @Modification Information
 *
 *    ??????       ??????         ?????��??
 *    -------        -------     -------------------
 *    2009. 3. 13.     ?��?��??
 *
 * @author 공�?? ??�??? �?�??? ?��?��??
 * @since 2009. 3. 13.
 * @version
 * @see
 *
 */
public class EgovComExcepHndlr implements ExceptionHandler {

    protected Log log = LogFactory.getLog(this.getClass());

    /*
    @Resource(name = "otherSSLMailSender")
    private SimpleSSLMail mailSender;
     */
    /**
     * �????? Exception?? �?리�????.
     */
    public void occur(Exception ex, String packageName) {
	//log.debug(" EgovServiceExceptionHandler run...............");
	try {
	    //mailSender. send(ex, packageName);
	    //log.debug(" sending a alert mail  is completed ");
	    log.error(packageName, ex);
	} catch (Exception e) {
	    //e.printStackTrace();
		log.fatal(packageName, ex);// 2011.10.10 보�????�? ????조�?
	    //throw new RuntimeException(ex);	
	}
    }
}
