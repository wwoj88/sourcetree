package kr.or.copyright.mls.console.email;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.email.inter.FdcrAdA10Dao;
import kr.or.copyright.mls.console.email.inter.FdcrAdA10Service;

import org.springframework.stereotype.Service;


@Service( "fdcrAdA10Service" )
public class FdcrAdA10ServiceImpl extends CommandService implements FdcrAdA10Service{
	
	@Resource( name = "fdcrAdA10Dao" )
	private FdcrAdA10Dao fdcrAdA10Dao;
	
	/**
	 * 메일폼관리 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA10List1( Map<String, Object> commandMap ) throws Exception{
		// TODO Auto-generated method stub

	}
	
	
	/**
	 * 그룹별 메일폼목록
	 * 
	 * @return
	 */
	public List selectFormGroupList(Map<String, Object> commandMap) throws Exception {
		return fdcrAdA10Dao.selectFormGroupList( commandMap );
	}
	
	// 메뉴 조회
	public Map<String, Object> selectFormInfo(Map<String, Object> commandMap) throws Exception {
		return fdcrAdA10Dao.selectFormInfo( commandMap );
	}
	
	/**
	 * 메뉴 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean formUpdate( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAdA10Dao.formUpdate( commandMap );
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}


	/**
	  * <PRE>
	  * 간략 : 
	  * 상세 : 
	  * <PRE>
	  * @param commandMap
	  * @return 
	  */
	public boolean formInsert( Map<String, Object> commandMap ){
		boolean result = false;
		try{
			fdcrAdA10Dao.formInsert( commandMap );
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}


	/**
	  * <PRE>
	  * 간략 : 
	  * 상세 : 
	  * <PRE>
	  * @param commandMap
	  * @return 
	  */
	public boolean formDelete( Map<String, Object> commandMap ){
		boolean result = false;
		try{
			fdcrAdA10Dao.formDelete( commandMap );
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

}
