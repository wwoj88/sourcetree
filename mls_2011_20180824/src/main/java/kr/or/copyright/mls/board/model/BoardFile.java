package kr.or.copyright.mls.board.model;

import java.util.Date;
import java.util.List;

import kr.or.copyright.mls.common.BaseObject;

public class BoardFile extends BaseObject {

	private String bordSeqn;
	private long menuSeqn;
	private String threaded;
	private long attcSeqn;
	private String fileSize;
	private String fileName;
	private String filePath;
	private String realFileName;
	private String rgstIdnt;
	private String rgstDttm;
	private String modiIdnt;
	private Date modiDate;
	private List fileList;

	public long getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(long attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public String getBordSeqn() {
		return bordSeqn;
	}
	public void setBordSeqn(String bordSeqn) {
		this.bordSeqn = bordSeqn;
	}
	public List getFileList() {
		return fileList;
	}
	public void setFileList(List fileList) {
		this.fileList = fileList;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public long getMenuSeqn() {
		return menuSeqn;
	}
	public void setMenuSeqn(long menuSeqn) {
		this.menuSeqn = menuSeqn;
	}
	public Date getModiDate() {
		return modiDate;
	}
	public void setModiDate(Date modiDate) {
		this.modiDate = modiDate;
	}
	public String getModiIdnt() {
		return modiIdnt;
	}
	public void setModiIdnt(String modiIdnt) {
		this.modiIdnt = modiIdnt;
	}
	public String getRealFileName() {
		return realFileName;
	}
	public void setRealFileName(String realFileName) {
		this.realFileName = realFileName;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getThreaded() {
		return threaded;
	}
	public void setThreaded(String threaded) {
		this.threaded = threaded;
	}

	


}
