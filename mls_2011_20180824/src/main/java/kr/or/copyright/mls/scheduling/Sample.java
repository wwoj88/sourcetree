package kr.or.copyright.mls.scheduling;

import java.text.SimpleDateFormat;


public class Sample {
	
	public void execute() throws Exception{
		
		long time = System.currentTimeMillis();         
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");         
		System.out.println("Cron trigger sample (5 second): current time = " + sdf.format(time));    
	}

}
