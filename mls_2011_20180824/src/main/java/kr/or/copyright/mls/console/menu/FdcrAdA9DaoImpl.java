package kr.or.copyright.mls.console.menu;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.menu.inter.FdcrAdA9Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAdA9Dao" )
public class FdcrAdA9DaoImpl extends EgovComAbstractDAO implements FdcrAdA9Dao{

	// 메뉴 목록조회
	public List selectMenuList(){
		return getSqlMapClientTemplate().queryForList( "AdminMenu.selectMenuList", "" );
	}

	// 메뉴 삭제
	public void menuDelete( Map map ){
		getSqlMapClientTemplate().update( "AdminMenu.menuDelete", map );
	}

	// 메뉴 등록
	public int menuInsert( Map map ){
		return (Integer)getSqlMapClientTemplate().insert( "AdminMenu.menuInsert", map );
	}

	// 메뉴 수정
	public void menuUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminMenu.menuUpdate", map );
	}

	// 그룹 목록 조회
	public List selectGroupList(){
		return getSqlMapClientTemplate().queryForList( "AdminMenu.selectGroupList", "" );
	}

	// 그룹 상세 조회
	public Map groupInfo( Map map ){
		return (Map)getSqlMapClientTemplate().queryForObject( "AdminMenu.groupInfo", map );
	}

	// 그룹 메뉴 조회
	public List groupMenuInfo( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminMenu.groupMenuInfo", map );
	}

	// 그룹 아이디 값 세팅
	public int groupIdMax( Map map ){
		Integer totalRow = (Integer) getSqlMapClientTemplate().queryForObject( "AdminMenu.groupIdMax", map );
		return totalRow.intValue();
	}

	// 그룹 등록
	public void groupInsert( Map map ){
		getSqlMapClientTemplate().update( "AdminMenu.groupInsert", map );
	}

	// 그룹 메뉴 등록
	public void groupMenuInsert( Map map ){
		getSqlMapClientTemplate().update( "AdminMenu.groupMenuInsert", map );
	}

	// 그룹 삭제
	public void groupDelete( Map map ){
		getSqlMapClientTemplate().update( "AdminMenu.groupDelete", map );
	}

	// 그룹 메뉴 삭제
	public void groupMenuDelete( Map map ){
		getSqlMapClientTemplate().update( "AdminMenu.groupMenuDelete", map );
	}

	// 그룹 수정
	public void groupUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminMenu.groupUpdate", map );
	}

	// 메뉴 조회
	public Map<String, Object> selectMenuInfo(Map map){
		return (Map<String,Object>)getSqlMapClientTemplate().queryForObject( "AdminMenu.selectMenuInfo", map );
	}
}
