package kr.or.copyright.mls.console.mber.inter;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface LogSearchDao{
	// 회원정보 목록조회
	public List logList( Map map );
	
	/**
	 * 회원정보 카운트
	 * @return
	 * @throws SQLException
	 */
	public int logListCount( Map<String, Object> commandMap ) throws SQLException;
	
	
}
