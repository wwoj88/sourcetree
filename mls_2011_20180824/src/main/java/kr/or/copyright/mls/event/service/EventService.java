package kr.or.copyright.mls.event.service;

import kr.or.copyright.mls.event.model.Event;

public interface EventService {

	public int checkCampPart(Event event);
	
	public int insertCampPartInfo(Event event) throws Exception;
	
	public int insertCampPartRslt(Event event) throws Exception;
	
	public void campPartList() throws Exception;
}
