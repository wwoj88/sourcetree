package kr.or.copyright.mls.console.rcept;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd16Service;
import org.openxmlformats.schemas.presentationml.x2006.main.STIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 관리저작물 접수 및 처리 > 위탁관리저작물 > 목록
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd16Controller extends DefaultController {

     @Resource(name = "fdcrAd16Service")
     private FdcrAd16Service fdcrAd16Service;

     private static final Logger logger = LoggerFactory.getLogger(FdcrAd16Controller.class);

     /**
      * 월별 보고현황 목록 조회
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd16List1.page")
     public String fdcrAd16List1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd16List1 Start");
          System.out.println("/console/rcept/fdcrAd16List1.page START");
          String USERID = ConsoleLoginUser.getUserId();
          // 파라미터 셋팅
          // commandMap.put( "GUBUN", "" );// 구분
          // commandMap.put( "TITLE", "" ); // 제목/내용
          // commandMap.put( "FROM_NO", "" ); // 페이지 시작
          // commandMap.put( "TO_NO", "" ); // 페이지 끝
          // commandMap.put( "YYYYMM", "" ); // 년월
          String GUBUN = EgovWebUtil.getString(commandMap, "GUBUN"); // 구분
          String TITLE = EgovWebUtil.getString(commandMap, "TITLE"); // 제목/내용
          String YYYYMM = EgovWebUtil.getString(commandMap, "YYYYMM"); // 년월
          String YYYYMM2 = EgovWebUtil.getString(commandMap, "YYYYMM2"); // 년월
          String A = EgovWebUtil.getString(commandMap, "A");
          if (A.equals("") || A == null || ("").equals(A)) {
               List timeList = fdcrAd16Service.getYyyyMm(commandMap);
               List timeList2 = fdcrAd16Service.getYyyyMm(commandMap);
               pagination(commandMap, 0, 0);
               //System.out.println(commandMap.get("ds_list"));
               model.addAttribute("ds_list", null);
               model.addAttribute("timeList", timeList);
               model.addAttribute("timeList2", timeList2);
               model.addAttribute("commandMap", commandMap);
               model.addAttribute("paginationInfo", commandMap.get("paginationInfo"));
               model.addAttribute("ds_count", 0);
               model.addAttribute("USERID", USERID);
               return "rcept/fdcrAd16List1.tiles";
          }
          if (!"".equals(TITLE)) {
               TITLE = URLDecoder.decode(TITLE, "UTF-8");
               commandMap.put("TITLE", TITLE);
          }
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          System.out.println("/console/rcept/fdcrAd16List1.page TRST_ORGN_CODE : " + commandMap.get("TRST_ORGN_CODE"));
          fdcrAd16Service.fdcrAd16List1(commandMap);
          List timeList = fdcrAd16Service.getYyyyMm(commandMap);
          List timeList2 = fdcrAd16Service.getYyyyMm(commandMap);
          System.out.println(commandMap.get("ds_list"));
          System.out.println(commandMap.get("paginationInfo"));
          model.addAttribute("ds_list", commandMap.get("ds_list"));
          model.addAttribute("timeList", timeList);
          model.addAttribute("timeList2", timeList2);
          model.addAttribute("commandMap", commandMap);
          model.addAttribute("paginationInfo", commandMap.get("paginationInfo"));
          model.addAttribute("ds_count", commandMap.get("ds_count"));
          model.addAttribute("USERID", USERID);
          logger.debug("fdcrAd16List1 End");
          return "rcept/fdcrAd16List1.tiles";
     }

     /**
      * 엑셀다운로드
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/rcept/fdcrAd16Down1.page")
     public String fdcrAd16Down1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd16Down1 Start");
          // 파라미터 셋팅
          commandMap.put("TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode());
          // String TITLE = EgovWebUtil.getString( commandMap, "TITLE" );
          String TITLE = request.getParameter("TITLE");
          if (!"".equals(TITLE)) {
               TITLE = URLDecoder.decode(TITLE, "UTF-8");
               commandMap.put("TITLE", TITLE);
          }
          String YYYYMM = EgovWebUtil.getString(commandMap, "YYYYMM");
          String YYYYMM2 = EgovWebUtil.getString(commandMap, "YYYYMM2");
          String GUBUN = request.getParameter("GUBUN");

          commandMap.put("YYYYMM", YYYYMM);
          commandMap.put("YYYYMM2", YYYYMM2);
          commandMap.put("GUBUN", GUBUN);
          System.out.println("YYYYMM:" + YYYYMM + "//YYYYMM2:" + YYYYMM2 + "//GUBUN:" + GUBUN + "//TITLE:" + TITLE);
          fdcrAd16Service.fdcrAd16Down1(commandMap);
          model.addAttribute("ds_list", commandMap.get("ds_list"));
          logger.debug("fdcrAd16Down1 End");
          return "rcept/fdcrAd16Down1";
     }

     @RequestMapping(value = "/console/rcept/fdcrAd16Delete.page")
     public String fdcrAd16Delete(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd16Delete Start");
          System.out.println("/console/rcept/fdcrAd16Delete.page START");

          String deleteWorksId = (String) commandMap.get("deleteList");
          String[] deleteList = deleteWorksId.split(",");
          commandMap.put("CONSOLE_USER", request.getSession().getAttribute("CONSOLE_USER"));
          fdcrAd16Service.fdcrAd16Delete(deleteList, commandMap);

          return "forward:/console/rcept/fdcrAd16List1.page";
     }

     @RequestMapping(value = "/console/rcept/fdcrAd16ImageList.page")
     public String fdcrAd16ImageList(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd16ImageList Start");
          logger.debug(" TITLE  : " + commandMap.get("TITLE"));
          logger.debug(" TITLE TEXT : " + commandMap.get("TITLE"));
          System.out.println("GUBUN : " + commandMap.get("GUBUN"));
          System.out.println("selectType : " + commandMap.get("selectType"));

          String GUBUN = EgovWebUtil.getString(commandMap, "GUBUN"); // 구분
          String TITLE = EgovWebUtil.getString(commandMap, "TITLE"); // 제목/내용
          logger.debug(" TITLE  : " + TITLE);
          if (commandMap.get("selectType") != null) {
               if (commandMap.get("selectType").equals("works_title")) {
                    commandMap.put("WORKS_TITLE", "WORKS_TITLE");
               } else if (commandMap.get("selectType").equals("writer")) {
                    commandMap.put("WRITER", "WRITER");
               }
          }


          if (commandMap.get("GUBUN") == null || commandMap.get("GUBUN").equals("undefined")) {
               fdcrAd16Service.fdcrAd16ArtList(commandMap);
          } else if (commandMap.get("GUBUN").equals("10")) {
               fdcrAd16Service.fdcrAd16ArtList(commandMap);
          } else {
               fdcrAd16Service.fdcrAd16ImageList(commandMap);
          }
          commandMap.put("GUBUN", GUBUN);
          System.out.println(commandMap.get("TITLE"));
          model.addAttribute("ds_list", commandMap.get("ds_list"));
          model.addAttribute("commandMap", commandMap);
          model.addAttribute("paginationInfo", commandMap.get("paginationInfo"));
          model.addAttribute("ds_count", commandMap.get("ds_count"));
          return "rcept/fdcrAd16ImageList.tiles";
     }
}


