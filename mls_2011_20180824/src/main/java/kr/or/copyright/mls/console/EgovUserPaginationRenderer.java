/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kr.or.copyright.mls.console;

import java.text.MessageFormat;

import egovframework.rte.ptl.mvc.tags.ui.pagination.AbstractPaginationRenderer;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**  
 * @Class Name : ImagePaginationRenderer.java
 * @Description : ImagePaginationRenderer Class
 * @Modification Information  
 * @
 * @  ??????      ??????              ?????��??
 * @ ---------   ---------   -------------------------------
 * @ 2009.03.16           �?�?????
 * 
 * @author �?�??????????? ?��????�? �?�???
 * @since 2009. 03.16
 * @version 1.0
 * @see
 * 
 *  Copyright (C) by MOPAS All right reserved.
 */
public class EgovUserPaginationRenderer extends AbstractPaginationRenderer {
	
    /**
    * PaginationRenderer
	* 
    * @see �?�??????????? ?��????�? �?�???
    */
	public EgovUserPaginationRenderer() {

//		//String strWebDir = "/egovframework.guideprogram.basic/images/egovframework/cmmn/"; // localhost
//		String strWebDir = "/###ARTIFACT_ID###/images/egovframework/cmmn/";
//
//		firstPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\">" +
//							"<image src='" + strWebDir + "btn_page_pre10.gif' border=0/></a>&#160;"; 
//        previousPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\">" +
//        						"<image src='" + strWebDir + "btn_page_pre1.gif' border=0/></a>&#160;";
//        currentPageLabel = "<strong>{0}</strong>&#160;";
//        otherPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\">{2}</a>&#160;";
//        nextPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\">" +
//        					"<image src='" + strWebDir + "btn_page_next10.gif' border=0/></a>&#160;";
//        lastPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\">" +
//        					"<image src='" + strWebDir + "btn_page_next1.gif' border=0/></a>&#160;";
		firstPageLabel = "<a href=\"#\" class=\"pre2\" onclick=\"{0}({1}); return false;\"><img src=\"/images/common/btn_pre2.gif\" alt=\"�???\" /></a>";
		previousPageLabel =	"<a href=\"#\" class=\"pre\" onclick=\"{0}({1}); return false;\"><img src=\"/images/common/btn_pre1.gif\" alt=\"?��??\" /></a>";		
		currentPageLabel = "<a href=\"#\" class=\"on\">{0}</a>";		
		otherPageLabel = "<a href=\"#\" onclick=\"{0}({1}); return false;\">{2}</a>";		
		nextPageLabel = "<a href=\"#\" class=\"next\" onclick=\"{0}({1}); return false;\"><img src=\"/images/common/btn_next1.gif\" alt=\"?��??\" /></a>";
		lastPageLabel = "<a href=\"#\" class=\"next2\" onclick=\"{0}({1}); return false;\"><img src=\"/images/common/btn_next2.gif\" alt=\"??\" /></a>";
	}
	
	@Override
	public String renderPagination(PaginationInfo paginationInfo,
			String jsFunction) {	
		
		StringBuffer strBuff = new StringBuffer();
        int firstPageNo = paginationInfo.getFirstPageNo();
        int firstPageNoOnPageList = paginationInfo.getFirstPageNoOnPageList();
        int totalPageCount = paginationInfo.getTotalPageCount();
        int pageSize = paginationInfo.getPageSize();
        int lastPageNoOnPageList = paginationInfo.getLastPageNoOnPageList();
        int currentPageNo = paginationInfo.getCurrentPageNo();
        int lastPageNo = paginationInfo.getLastPageNo();
        
        boolean lineCheck = false;
        if(totalPageCount < pageSize){
        	lineCheck = true;
        }
        if(totalPageCount > pageSize){
            if(firstPageNoOnPageList > pageSize)
            {
            	strBuff.append( MessageFormat.format(firstPageLabel, new Object[] {jsFunction, Integer.toString(firstPageNo)}));
                strBuff.append( MessageFormat.format(previousPageLabel, new Object[] {jsFunction, Integer.toString(firstPageNoOnPageList - 1)}));
            } else
            {
                strBuff.append(MessageFormat.format(firstPageLabel, new Object[] {jsFunction, Integer.toString(firstPageNo)}));
                strBuff.append(MessageFormat.format(previousPageLabel, new Object[] {jsFunction, Integer.toString(firstPageNo)}));
            }
        }
        
        for(int i = firstPageNoOnPageList; i <= lastPageNoOnPageList; i++){
            if(i == currentPageNo){
                strBuff.append(MessageFormat.format(currentPageLabel, new Object[] {Integer.toString(i)}));
            }else{
            	if(i == lastPageNo && lineCheck){
            		strBuff.append(MessageFormat.format(otherPageLabel, new Object[] {jsFunction, Integer.toString(i), Integer.toString(i)}));
            	}else{
            		strBuff.append(MessageFormat.format(otherPageLabel, new Object[] {jsFunction, Integer.toString(i), Integer.toString(i)}));
            	}
            }
        }

        if(totalPageCount > pageSize){
            if(lastPageNoOnPageList < totalPageCount)
            {
                strBuff.append(MessageFormat.format(nextPageLabel, new Object[] {jsFunction, Integer.toString(firstPageNoOnPageList + pageSize)}));
                strBuff.append(MessageFormat.format(lastPageLabel, new Object[] {jsFunction, Integer.toString(lastPageNo)}));
            } else
            {
                strBuff.append(MessageFormat.format(nextPageLabel, new Object[] {
                    jsFunction, Integer.toString(lastPageNo)
                }));
                strBuff.append(MessageFormat.format(lastPageLabel, new Object[] {
                    jsFunction, Integer.toString(lastPageNo)
                }));
            }
        }
        return strBuff.toString();
	}
}
