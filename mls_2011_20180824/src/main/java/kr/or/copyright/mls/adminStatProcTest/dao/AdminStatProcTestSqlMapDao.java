package kr.or.copyright.mls.adminStatProcTest.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class AdminStatProcTestSqlMapDao extends SqlMapClientDaoSupport implements AdminStatProcTestDao{

	//---------------------------------------상당한노력 proc start --------------------------------------
	
	public List selectStatProcAllLogList(Map map){
		return getSqlMapClientTemplate().queryForList("AdminStatProc_test.statProcAllLogList",map);
	}
		
	public void callProcGetStatProcOrd(){
		
		getSqlMapClientTemplate().queryForObject("AdminStatProc_test.callGetStatProcord");
		
	}
	
	public void callStatProc(Map map){
		
		getSqlMapClientTemplate().queryForObject("AdminStatProc_test.callStatProc", map);
		
	}
	
	public void callStatProc01(Map map){
			
			getSqlMapClientTemplate().queryForObject("AdminStatProc_test.callStatProc01", map);
			
		}
	
	public void callStatProc02(Map map){
		
		getSqlMapClientTemplate().queryForObject("AdminStatProc_test.callStatProc02", map);
		
	}
	
	public List statProcPopup(Map map) {
		
		return getSqlMapClientTemplate().queryForList("AdminStatProc_test.statProcPopup",map);
	}
	
}
