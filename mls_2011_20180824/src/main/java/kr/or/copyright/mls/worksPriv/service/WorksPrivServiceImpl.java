package kr.or.copyright.mls.worksPriv.service;

import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import kr.or.copyright.mls.board.model.Board;
import kr.or.copyright.mls.board.model.BoardFile;
import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.support.util.FileUploadUtil;
import kr.or.copyright.mls.worksPriv.dao.WorksPrivDao;
import kr.or.copyright.mls.worksPriv.model.WorksPriv;
import kr.or.copyright.mls.worksPriv.model.WorksPrivFile;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

//@TransactionConfiguration(defaultRollback = true)
public class WorksPrivServiceImpl extends BaseService implements WorksPrivService {
	
	private WorksPrivDao worksPrivDao;
	
	public void setWorksPrivDao(WorksPrivDao worksPrivDao) {
		this.worksPrivDao = worksPrivDao;
	}

	public ListResult findWorksPrivList(int pageNo, int rowPerPage, Map params) {
		
        int from = rowPerPage * (pageNo-1) + 1;
        int to = rowPerPage * pageNo;
        
        params.put("FROM", ""+from);
        params.put("TO", ""+to);
        int totalRow = 0;
        
//		List list = worksPrivDao.findBoardList(params);
		List list = null;
		
		String menuSeqn = (String)params.get("menuSeqn");
		if(menuSeqn.equals("1")){
			totalRow = worksPrivDao.findWorksPrivCountO(params); //참여마당 개인페이지
		}else{
			totalRow = worksPrivDao.findWorksPrivCount(params); //마이페이지
		}
		
		if(menuSeqn.equals("1")){
			list = worksPrivDao.findWorksPrivListO(params); //참여마당 개인페이지
		}else{
			list = worksPrivDao.findWorksPrivList(params); //마이페이지
		}
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
	
		return listResult;
	}
	
	public List<CodeList> findCodeList(CodeList codeList){
		
		return worksPrivDao.findCodeList(codeList);
	}
	
	@Transactional(readOnly = false)
	public int goInsert(WorksPriv worksPriv) throws Exception{
		try {
			int threaded = worksPrivDao.worksPrivMax(worksPriv);
			
	//		daoMgr.startTransaction();
			worksPriv.setCrId(threaded);
			
			worksPrivDao.goInsert(worksPriv);
			
			
			
			//BoardFile boardFile = new BoardFile();
			WorksPrivFile worksPrivFile = new WorksPrivFile();
			
			for(int i=0; i < worksPriv.getFileList().size(); i++) {
				
	
				worksPrivFile = (WorksPrivFile) worksPriv.getFileList().get(i);
				worksPrivFile.setCrId(threaded);
				worksPrivDao.goInsertFile(worksPrivFile);
			}
		//		daoMgr.commitTransaction();
		} catch(Exception e){
			e.printStackTrace();
			
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return -1;
		} 
		return 1;
	}
	
	public WorksPriv goView(WorksPriv worksPriv){
		worksPriv = worksPrivDao.goView(worksPriv);
		worksPriv.setFileList(worksPrivDao.worksPrivFileList(worksPriv));
		
		return worksPriv;
		
	}
	
	public void goUpdate(WorksPriv worksPriv){
		worksPrivDao.goUpdate(worksPriv);
		
		WorksPrivFile worksPrivFile = new WorksPrivFile();
		
		for(int i=0; i < worksPriv.getFileList().size(); i++){
			worksPrivFile = (WorksPrivFile)worksPriv.getFileList().get(i);
			worksPrivFile.setCrId(worksPriv.getCrId());
			
			worksPrivDao.goInsertFile(worksPrivFile);
		}
	}
	
	public void goDelete(WorksPriv worksPriv){
		worksPrivDao.goDelete(worksPriv);
	}
	
	public void fileDelete(String[] chk){
		WorksPrivFile worksPrivFile = new WorksPrivFile();
		String[] fname = new String[2];
		
		for(int i=0; i<chk.length; i++){
			StringTokenizer st = new StringTokenizer(chk[i],",");
			
			while(st.hasMoreTokens()){
				fname[0] = st.nextToken();
				fname[1] = st.nextToken();
			}
			FileUploadUtil.deleteFormFiles(fname[1], new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_realUpload_path"))); // 로컬용
			//FileUploadUtil.deleteFormFiles(fname[1], "/home/tmax/mls/web/upload/"); // 실서버용
			//FileUploadUtil.deleteFormFiles(fname[1], "/home/tmax_dev/mls/web/upload/"); // 개발서버용
			
			worksPrivFile.setAttcSeqn(Long.parseLong(fname[0]));
			worksPrivDao.fileDelete(worksPrivFile);
		}
	}
}
