package kr.or.copyright.mls.mail.controller;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.mail.model.Mail;
import kr.or.copyright.mls.mail.service.MailService;
import kr.or.copyright.mls.support.util.StringUtil;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.softforum.xdbe.xCrypto;

@Controller
public class MailController extends MultiActionController {
	private Logger log = Logger.getLogger(this.getClass());
	
	@javax.annotation.Resource(name = "MailService")
	private MailService MailService;
	
	@RequestMapping(value = "/mail/insertRejc.do")
	public ModelAndView insertRejc(HttpServletRequest request
			,HttpServletResponse respone)throws Exception{
		
		int iResult = 0;
		Mail rejcMail = new Mail();
		
		bind(request, rejcMail);
		
		// 필수항목이 입력된 경우에만 insert 한다.
		// return int : 1(정상처리), -1(비정상처리)
		if(rejcMail.getMail_rejc_cd() != null && rejcMail.getMail_addr() != null)
			iResult = MailService.insertRejc(rejcMail); 
		
		ModelAndView mv = new ModelAndView("/common/mailRejcRslt");
		
		return mv;
		
	}
	
	@RequestMapping(value = "/mail/deleteRejc.do")
	public ModelAndView deleteRejc(HttpServletRequest request
			,HttpServletResponse respone)throws Exception{
		
		int iResult = 0;
		Mail rejcMail = new Mail();
		
		bind(request, rejcMail);
		
		// 필수항목이 입력된 경우에만 insert 한다.
		// return int : 1(정상처리)
		if(rejcMail.getMail_rejc_cd() != null && rejcMail.getMail_addr() != null)
			iResult = MailService.deleteRejc(rejcMail); 
		
		ModelAndView mv = new ModelAndView("/common/mailRejcRslt"); 
		
		return mv;
		
	}
	
	@RequestMapping(value = "/mail/updateEmailRecv.do")
	public ModelAndView updateEmailRecv(HttpServletRequest request
			,HttpServletResponse respone)throws Exception{
		
		int iResult = 0;
		Mail recvMail = new Mail();
		recvMail.setSend_msg_id(StringUtil.nullToEmpty(request.getParameter("send_msg_id")));
		
		//bind(request, rejcMail);

		
		if(!recvMail.getSend_msg_id().equals(""))
			iResult = MailService.updateEmailRecv(recvMail);
		
		ModelAndView mv = new ModelAndView("/common/mailRejcRslt");		
		return mv;
	}
	
	
	@RequestMapping(value = "/mail/insertRejcForm.do")
	public ModelAndView insertRejcForm(HttpServletRequest request
			,HttpServletResponse respone)throws Exception{
		
		String mail_addr = request.getParameter("addr");
		String mail_rejc_cd = request.getParameter("rejc");
		
		ModelAndView mv = new ModelAndView("/common/mailRejcRslt");
		mv.addObject("gubun", "insert");
		mv.addObject("mail_addr", mail_addr);
		mv.addObject("mail_rejc_cd", mail_rejc_cd);
		return mv;
	}
	
}
