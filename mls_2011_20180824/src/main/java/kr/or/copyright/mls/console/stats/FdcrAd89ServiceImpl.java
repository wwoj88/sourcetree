package kr.or.copyright.mls.console.stats;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.stats.inter.FdcrAd87Dao;
import kr.or.copyright.mls.console.stats.inter.FdcrAd89Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd89Service" )
public class FdcrAd89ServiceImpl extends CommandService implements FdcrAd89Service{

	// old AdminConnDao
	@Resource( name = "fdcrAd87Dao" )
	private FdcrAd87Dao fdcrAd87Dao;

	/**
	 * 보상금 처리 및 신청 통계(기간별)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd89List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd87Dao.rsltList( commandMap );

		commandMap.put( "ds_list", list ); // 로그인 사용자 정보
	}

}
