package kr.or.copyright.mls.console.stats;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.stats.inter.FdcrAd87Dao;
import kr.or.copyright.mls.console.stats.inter.FdcrAd90Service;

import org.springframework.stereotype.Service;

import com.tobesoft.platform.data.Dataset;

@Service( "fdcrAd90Service" )
public class FdcrAd90ServiceImpl extends CommandService implements FdcrAd90Service{

	// old AdminConnDao
	@Resource( name = "fdcrAd87Dao" )
	private FdcrAd87Dao fdcrAd87Dao;

	/**
	 * 기간별 접속 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd90List1( Map<String, Object> commandMap ) throws Exception{
		List list = null;
		if("0".equals((String)commandMap.get("GUBUN"))){
			list = (List) fdcrAd87Dao.adminStatPeriList(commandMap);
		}else{
			list = (List) fdcrAd87Dao.adminStatPeriList2(commandMap);
		}
		
		int PRE_YEAR = 0;
		int TOTAL = 0;
		boolean yearStart = true;
		for(int i=0;i<list.size();i++){
			Map<String,Object> map = (Map<String,Object>)list.get( i );
			int YEAR = EgovWebUtil.getToInt( map, "YEAR" );
			int USERCOUNT = EgovWebUtil.getToInt( map, "USERCOUNT" );
			String YEARMONTH = EgovWebUtil.getString( map, "YEARMONTH" );
			
			if(yearStart){
				map.put( "yearStart", "Y" );
			}else{
				map.put( "yearStart", "N" );
			}
			if("합계".equals(YEARMONTH)){
				
			}
			if(!"합계".equals(YEARMONTH)){
				map.put( "YEARMONTH_PARAM", YEAR+YEARMONTH );
				TOTAL += USERCOUNT;
			}
			if(PRE_YEAR == YEAR){
				map.put( "YEAR", "" );
			}else if(PRE_YEAR != YEAR){
				PRE_YEAR = YEAR;
			}
		}
		commandMap.put( "TOTAL", TOTAL );
		
		commandMap.put("ds_list", list);

	}
	
	// 기간별 접속 통계(월별)
	//connStatPeriMonthList
	public void fdcrAd90List2(Map<String,Object> commandMap) throws Exception {
		List list = null;
		if("0".equals((String)commandMap.get("GUBUN"))){
			list = (List) fdcrAd87Dao.adminStatPeriMonthList(commandMap);
		}else{
			list = (List) fdcrAd87Dao.adminStatPeriMonthList2(commandMap);
		}
		commandMap.put("ds_list", list);
		
		int TOTAL = 0;
		for(int i=0;i<list.size();i++){
			Map<String,Object> map = (Map<String,Object>)list.get( i );
			int YEAR = EgovWebUtil.getToInt( map, "YEAR" );
			int USERCOUNT = EgovWebUtil.getToInt( map, "USERCOUNT" );
			String YEARMONTH = EgovWebUtil.getString( map, "YEARMONTH" );
			if(!"합계".equals(YEARMONTH)){
				TOTAL += USERCOUNT;
			}
		}
		commandMap.put( "TOTAL", TOTAL );
	}
}
