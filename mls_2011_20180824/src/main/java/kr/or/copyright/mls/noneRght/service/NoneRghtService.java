package kr.or.copyright.mls.noneRght.service;

import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

public interface NoneRghtService {
	public ListResult muscRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	public ListResult bookRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	public ListResult scriptRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	public ListResult imageRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	public ListResult mvieRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	public ListResult broadcastRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	public ListResult newsRghtList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
}
