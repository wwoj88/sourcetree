package kr.or.copyright.mls.myStat.model;

import kr.or.copyright.mls.common.BaseObject;

public class StatApplyWorksShis extends BaseObject {
	
	private String applyWriteYmd;       //신청서작성YMD
	private int applyWriteSeq;          //신청서작성SEQ
	private int worksSeqn;              //명세서순번
	private String chngDttm;            //변경일자
	private int statRsltCd;             //심의결과CD
	private String rgstIdnt;
	private String statRsltMemo;        //심의결과비고
	private int attcSeqn;
	
	
	public int getApplyWriteSeq() {
		return applyWriteSeq;
	}
	public void setApplyWriteSeq(int applyWriteSeq) {
		this.applyWriteSeq = applyWriteSeq;
	}
	public String getApplyWriteYmd() {
		return applyWriteYmd;
	}
	public void setApplyWriteYmd(String applyWriteYmd) {
		this.applyWriteYmd = applyWriteYmd;
	}
	public int getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(int attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public String getChngDttm() {
		return chngDttm;
	}
	public void setChngDttm(String chngDttm) {
		this.chngDttm = chngDttm;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public int getStatRsltCd() {
		return statRsltCd;
	}
	public void setStatRsltCd(int statRsltCd) {
		this.statRsltCd = statRsltCd;
	}
	public String getStatRsltMemo() {
		return statRsltMemo;
	}
	public void setStatRsltMemo(String statRsltMemo) {
		this.statRsltMemo = statRsltMemo;
	}
	public int getWorksSeqn() {
		return worksSeqn;
	}
	public void setWorksSeqn(int worksSeqn) {
		this.worksSeqn = worksSeqn;
	}
	
	
}