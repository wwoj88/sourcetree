package kr.or.copyright.mls.support.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * <pre>
 * 시스템에서 활용될만한 날자 관련 기능을 제공하는 기능을 정의한 class이다.
 * 외부에서 사용할수 있는 모든 메서드는 static으로 구성되어 있다.
 * 
 * sample :
 * 
 *     System.out.println(DateUtil.getDate());
 *     System.out.println(DateUtil.getDay(&quot;20031011&quot;));
 *     System.out.println(DateUtil.getEngDay(&quot;20031011&quot;));
 *     System.out.println(DateUtil.getHanDay(&quot;20031011&quot;));
 *     System.out.println(DateUtil.toLunarDate(&quot;20031025&quot;));
 *     System.out.println(DateUtil.toYYYYMMDDDate(&quot;20031025&quot;));
 *     System.out.println(DateUtil.isAfter(&quot;20031025&quot;, &quot;2001/03/01&quot;));
 *     System.out.println(DateUtil.getNextDateSkipLastHoliday(&quot;20031001&quot;,2)); 
 * 
 * result :
 * 
 *     2003/10/06
 *     7
 *     Saturday
 *     토
 *     2003/10/01
 *     20031025
 *     true
 *     2003/10/04
 * 
 * 
 * </pre>
 */

public final class DateUtil {

	/**
	 * Default Date Format 변수.
	 */
	private static SimpleDateFormat dateFormat = null;

	static {
		try {
			dateFormat = new SimpleDateFormat("yyyy-MM-dd",
					java.util.Locale.KOREA);
		} catch (Exception e) {
			dateFormat = new SimpleDateFormat("yyyyMMdd",
					java.util.Locale.KOREA);
		}
	}
	
	 /**
     * 8자리의 현재 날짜를 얻어오는 함수
     *
     * @return      8 자리의 현재 날짜	( 20020301 )

     */
	public static String getCurrDateNumber() {
		java.util.TimeZone zone = java.util.TimeZone.getTimeZone("GMT+9:00");			
		java.util.Calendar c = java.util.Calendar.getInstance(zone);		

		String returnDate 	= 	"" +  c.get(java.util.Calendar.YEAR        );
		String currentMonth =  	"" + (c.get(java.util.Calendar.MONTH)  + 1 );
		String currentDay   =  	"" +  c.get(java.util.Calendar.DAY_OF_MONTH);
		
		returnDate			+= 	addZeroFirst(currentMonth, 2);
		returnDate			+= 	addZeroFirst(currentDay  , 2);

		return (returnDate) ;
	}

	/**
     * 6자리의 현재 시간을 얻어오는 함수
     *
     * @return      현재시간 (130904)
    */
	public static String getCurrTimeNumber() {		
		java.util.TimeZone zone = java.util.TimeZone.getTimeZone("GMT+9:00");			
		java.util.Calendar c 	= java.util.Calendar.getInstance(zone);
	
		String currentHour 		= 	"" + c.get(java.util.Calendar.HOUR_OF_DAY 	) ;
		String currentMinute	= 	"" + c.get(java.util.Calendar.MINUTE      	) ;
		String currentSecond   	= 	"" + c.get(java.util.Calendar.SECOND		) ;

		String returnTime		= 	"";
		returnTime				+= 	addZeroFirst(currentHour  , 2);
		returnTime				+= 	addZeroFirst(currentMinute, 2);
		returnTime				+= 	addZeroFirst(currentSecond, 2);

		return (returnTime) ;
	}
	
	/**
     * 8자리의 현재 시간을 얻어오는 함수
     *
    */
	public static String getCurrTimeString() {	
		String CurrTime = getCurrTimeNumber();
		return (ChangeTimeToString(CurrTime)) ;
	}
	
	/**
     * 시간 형식 바꾸기 6->8
     *
     * @param       firstTimeType 바꾸고자 하는 시간 (130904)
     * @return      바뀐시간 형식 (13:09:04)
    */
	public static String ChangeTimeToString(String firstTimeType) {
	
		if(firstTimeType.length() != 6) return("TimeType is wrong");
		
		String tmpHour  	= firstTimeType.trim().substring(0,2);
		String tmpMinute 	= firstTimeType.trim().substring(2,4);
		String tmpSecond   	= firstTimeType.trim().substring(4);
		
		return (tmpHour + ":" + tmpMinute + ":" + tmpSecond) ;
	}
	
	/**
     * 원하는 자릿수만큼 문자열 앞에 0을 채워주는 함수
     *
     * @param       changeIntValue 바꾸고자 하는 숫자 
     * @param       반환되어질 전체 자릿수
     * @return      0 이 채워진 숫자
    */
	public static String addZeroFirst(String changeIntValue, int strNum) {
		if (strNum < 0) return ("Parameter two is wrong");
		int intValueLength = changeIntValue.trim().length();
		
		if (intValueLength >= strNum) {
			return (changeIntValue);
		} else {
			String returnValue = changeIntValue;
			for (int i = 0 ; i < (strNum - intValueLength) ; i++) {
				returnValue = "0" + returnValue;
			}
			return (returnValue);
		}
	}
	
	/**
	 * 음력/양력 변환에 필요한 정보
	 */
	private static final int dt[] = new int[163];

	/**
	 * 음력/양력 변환에 필요한 정보
	 */
	private static final int dateCount[] = { 31, 0, 31, 30, 31, 30, 31, 31, 30,
			31, 30, 31 };

	/**
	 * 음력/양력 변환에 필요한 정보
	 */
	private static final int kk[] = {
	/* 1881 */1, 2, 1, 2, 1, 2, 2, 3, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 1, 2, 2,
			1, 2, 2, 0, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2, 0, 2, 1, 1, 2, 1,
			3, 2, 1, 2, 2, 1, 2, 2, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 0, 2,
			1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 0, 2, 2, 1, 2, 3, 2, 1, 1, 2, 1,
			2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1, 0, 2, 1, 2, 2, 1, 2,
			1, 2, 1, 2, 1, 2, 0, 1, 2, 3, 2, 1, 2, 2, 1, 2, 1, 2, 1, 2,

			/* 1891 */1, 2, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 0, 1, 1, 2, 1, 1, 2,
			3, 2, 2, 1, 2, 2, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 0, 1, 2,
			1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 0, 2, 1, 2, 1, 2, 3, 1, 2, 1, 2, 1,
			2, 1, 2, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 0, 1, 2, 2, 1, 2, 1, 2,
			1, 2, 1, 2, 1, 0, 2, 1, 2, 3, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2,
			1, 2, 1, 2, 2, 1, 2, 1, 2, 0, 1, 2, 1, 1, 2, 1, 2, 2, 3, 2, 2, 1,
			2,

			/* 1901 */1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1, 0, 2, 1, 2, 1, 1, 2,
			1, 2, 1, 2, 2, 2, 0, 1, 2, 1, 2, 1, 3, 2, 1, 1, 2, 2, 1, 2, 2, 2,
			1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 0, 2, 2, 1, 2, 2, 1, 1, 2, 1, 2, 1,
			2, 0, 1, 2, 2, 1, 4, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 2,
			1, 2, 1, 2, 1, 0, 2, 1, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2, 0, 1, 2, 3,
			1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1,
			0,

			/* 1911 */2, 1, 2, 1, 1, 2, 3, 1, 2, 2, 1, 2, 2, 2, 1, 2, 1, 1, 2,
			1, 1, 2, 2, 1, 2, 0, 2, 2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 0, 2, 2,
			1, 2, 2, 3, 1, 2, 1, 2, 1, 1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1,
			2, 0, 1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 0, 2, 1, 3, 2, 1, 2, 2,
			1, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 0, 1, 2, 1,
			1, 2, 1, 2, 3, 2, 2, 1, 2, 2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2, 2,
			0,

			/* 1921 */2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 0, 2, 1, 2, 2, 1, 3,
			2, 1, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 1, 2, 0, 2, 1,
			2, 1, 2, 2, 1, 2, 1, 2, 1, 1, 0, 2, 1, 2, 2, 3, 2, 1, 2, 2, 1, 2,
			1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1, 0, 2, 1, 1, 2, 1, 2, 1,
			2, 2, 1, 2, 2, 0, 1, 2, 3, 1, 2, 1, 1, 2, 2, 1, 2, 2, 2, 1, 2, 1,
			1, 2, 1, 1, 2, 1, 2, 2, 2, 0, 1, 2, 2, 1, 1, 2, 3, 1, 2, 1, 2, 2,
			1,

			/* 1931 */2, 2, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 0, 2, 2, 2, 1, 2, 1,
			2, 1, 1, 2, 1, 2, 0, 1, 2, 2, 1, 2, 4, 1, 2, 1, 2, 1, 1, 2, 1, 2,
			1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 0, 1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2,
			1, 0, 2, 1, 1, 4, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 1, 2, 1, 1, 2,
			1, 2, 2, 2, 1, 0, 2, 2, 1, 1, 2, 1, 1, 4, 1, 2, 2, 1, 2, 2, 2, 1,
			1, 2, 1, 1, 2, 1, 2, 1, 2, 0, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1,
			0,

			/* 1941 */2, 2, 1, 2, 2, 1, 4, 1, 1, 2, 1, 2, 1, 2, 1, 2, 2, 1, 2,
			2, 1, 2, 1, 1, 2, 0, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2, 0, 1, 1,
			2, 1, 4, 1, 2, 1, 2, 2, 1, 2, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 1,
			2, 0, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1, 2, 0, 2, 2, 3, 1, 2, 1, 1,
			2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 0, 2, 2, 1,
			2, 1, 2, 1, 3, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1,
			0,

			/* 1951 */2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 0, 1, 2, 1, 2, 1, 4,
			2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 1, 2, 2, 1, 2, 2, 1, 2, 2, 0, 1, 1,
			2, 1, 1, 2, 1, 2, 2, 1, 2, 2, 0, 2, 1, 1, 4, 1, 1, 2, 1, 2, 1, 2,
			2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 0, 2, 1, 2, 1, 2, 1, 1,
			2, 3, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 0, 1, 2, 2,
			1, 2, 1, 2, 1, 2, 1, 2, 1, 0, 2, 1, 2, 1, 2, 2, 3, 2, 1, 2, 1, 2,
			1,

			/* 1961 */2, 1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 0, 1, 2, 1, 1, 2, 1,
			2, 2, 1, 2, 2, 1, 0, 2, 1, 2, 1, 3, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1,
			2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 0, 1, 2, 1, 2, 1, 1, 2, 1, 1, 2, 2,
			1, 0, 2, 2, 2, 3, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 1,
			2, 1, 2, 1, 2, 0, 1, 2, 2, 1, 2, 1, 2, 3, 2, 1, 2, 1, 2, 1, 2, 1,
			2, 1, 2, 2, 1, 2, 1, 2, 1, 0, 2, 1, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2,
			0,

			/* 1971 */1, 2, 1, 1, 2, 3, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1,
			2, 1, 2, 2, 2, 1, 0, 2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 2, 1, 0, 2, 2,
			1, 2, 3, 1, 2, 1, 1, 2, 2, 1, 2, 2, 2, 1, 2, 1, 1, 2, 1, 1, 2, 1,
			2, 0, 2, 2, 1, 2, 1, 2, 1, 2, 3, 2, 1, 1, 2, 2, 1, 2, 2, 1, 2, 1,
			2, 1, 2, 1, 1, 0, 2, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 0, 2, 1, 1,
			2, 1, 2, 4, 1, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2,
			0,

			/* 1981 */1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2, 2, 0, 2, 1, 2, 1, 3, 2,
			1, 1, 2, 2, 1, 2, 2, 2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 0, 2, 1,
			2, 2, 1, 1, 2, 1, 1, 2, 3, 2, 2, 1, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1,
			2, 0, 1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 1, 1, 0, 2, 1, 2, 2, 1, 2, 3,
			2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1, 0, 2, 1, 1,
			2, 1, 2, 1, 2, 2, 1, 2, 2, 0, 1, 2, 1, 1, 2, 3, 1, 2, 1, 2, 2, 2,
			2,

			/* 1991 */1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 0, 1, 2, 2, 1, 1, 2,
			1, 1, 2, 1, 2, 2, 0, 1, 2, 2, 3, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2,
			2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 0, 1, 2, 2, 1, 2, 2, 1, 2, 3, 2, 1,
			1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2, 0, 1, 1, 2, 1, 2, 1, 2,
			2, 1, 2, 2, 1, 0, 2, 1, 1, 2, 1, 3, 2, 2, 1, 2, 2, 2, 1, 2, 1, 1,
			2, 1, 1, 2, 1, 2, 2, 2, 1, 0, 2, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1,
			0,

			/* 2001 */2, 2, 2, 1, 3, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 2,
			1, 1, 2, 1, 2, 1, 0, 2, 2, 1, 2, 2, 1, 2, 1, 1, 2, 1, 2, 0, 1, 2,
			3, 2, 2, 1, 2, 1, 2, 2, 1, 1, 2, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1,
			2, 0, 1, 1, 2, 1, 2, 1, 2, 3, 2, 2, 1, 2, 2, 1, 1, 2, 1, 1, 2, 1,
			2, 2, 2, 1, 2, 0, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1, 2, 0, 2, 2, 1,
			1, 2, 3, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2,
			0,

			/* 2011 */2, 1, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1, 0, 2, 1, 2, 4, 2, 1,
			2, 1, 1, 2, 1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 0, 1, 2,
			1, 2, 1, 2, 1, 2, 2, 3, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2,
			2, 0, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1, 2, 2, 0, 2, 1, 1, 2, 1, 3, 2,
			1, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 0, 2, 1, 2,
			1, 2, 1, 1, 2, 1, 2, 1, 2, 0, 2, 1, 2, 2, 3, 2, 1, 1, 2, 1, 2, 1,
			2,

			/* 2021 */1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 0, 2, 1, 2, 1, 2, 2,
			1, 2, 1, 2, 1, 2, 0, 1, 2, 3, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 2,
			1, 1, 2, 1, 2, 2, 1, 2, 2, 1, 0, 2, 1, 2, 1, 1, 2, 3, 2, 1, 2, 2,
			2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 0, 1, 2, 1, 2, 1, 1, 2,
			1, 1, 2, 2, 2, 0, 1, 2, 2, 1, 2, 3, 1, 2, 1, 1, 2, 2, 1, 2, 2, 1,
			2, 2, 1, 1, 2, 1, 1, 2, 2, 0, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1,
			0,

			/* 2031 */2, 1, 2, 3, 2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2,
			2, 1, 2, 2, 1, 2, 0, 1, 2, 1, 1, 2, 1, 2, 3, 2, 2, 2, 1, 2, 1, 2,
			1, 1, 2, 1, 2, 1, 2, 2, 2, 1, 0, 2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1,
			2, 0, 2, 2, 1, 2, 1, 1, 4, 1, 1, 2, 1, 2, 2, 2, 2, 1, 2, 1, 1, 2,
			1, 1, 2, 1, 2, 0, 2, 2, 1, 2, 1, 2, 1, 2, 1, 1, 2, 1, 0, 2, 2, 1,
			2, 2, 3, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 1,
			0,

			/* 2041 */2, 1, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2, 0, 1, 2, 3, 1, 2, 1,
			2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2, 2, 0 };

	/**
	 * DDate default constructor
	 */
	public DateUtil() {
	}

	/**
	 * pBasisDate이 pComparativeDate보다 이전일인지를 반환한다.
	 * 
	 * @param pBasisDate
	 *            기준이 되는 일자.
	 * @param pComparativeDate
	 *            비교 대상 일자.
	 * @return 기준일이 입력일 보다 이전인지 여부를 반환
	 */
	public static boolean isBefore(String pBasisDate, String pComparativeDate) {

		java.util.Calendar cal1 = toCalendar(pBasisDate);
		java.util.Calendar cal2 = toCalendar(pComparativeDate);

		return cal1.before(cal2);
	}

	/**
	 * pBasisDate이 pComparativeDate보다 이후일인지를 반환한다.
	 * 
	 * @param pBasisDate
	 *            기준이 되는 일자.
	 * @param pComparativeDate
	 *            비교 대상 일자.
	 * @return 기준일이 입력일 보다 이후인지 여부를 반환
	 */
	public static boolean isAfter(String pBasisDate, String pComparativeDate) {

		java.util.Calendar cal1 = toCalendar(pBasisDate);
		java.util.Calendar cal2 = toCalendar(pComparativeDate);

		return cal1.after(cal2);
	}

	/**
	 * pFromDate와 pToDate 사이에 몇일이 있는지를 검사한다.
	 * 
	 * <pre>
	 *      example :
	 * 
	 *         System.out.println ( DateUtil.getDaysBetween(&quot;2002.10.15&quot;, &quot;2002.10.10&quot;) );
	 *         System.out.println ( DateUtil.getDaysBetween(&quot;2002.10.10&quot;, &quot;2002.10.15&quot;) );
	 *         System.out.println ( DateUtil.getDaysBetween(&quot;20021030&quot;, &quot;20021120&quot;) );
	 * 
	 * 
	 *      result :
	 * 
	 *             5
	 *             5
	 *            21
	 * 
	 * 
	 * </pre>
	 * 
	 * @param pFromDate
	 *            시작일
	 * @param pToDate
	 *            종료일
	 * @return 두 날자사이의 일자
	 */
	public static int getDaysBetween(String pFromDate, String pToDate) {

		java.util.Calendar cal1 = toCalendar(pFromDate);
		java.util.Calendar cal2 = toCalendar(pToDate);

		if (cal1.getTime().compareTo(cal2.getTime()) == -1)
			return (int) ((cal2.getTime().getTime() - cal1.getTime().getTime()) / (1000 * 60 * 60 * 24));

		return (int) ((cal1.getTime().getTime() - cal2.getTime().getTime()) / (1000 * 60 * 60 * 24));
	}

	/**
	 * 현재 날짜가 포함된 달의 마지막 일자를 구한다.
	 * 
	 * <pre>
	 *      example :
	 * 
	 *            System.out.println ( DateUtil.getLastDate() );
	 * 
	 *      result :
	 * 
	 *            30
	 * 
	 * 
	 * </pre>
	 * 
	 * @return 마지막일자
	 */
	public static int getLastDate() {
		GregorianCalendar calendar = new GregorianCalendar();
		return calendar.get(Calendar.DATE);
	}

	/**
	 * pDate가 포함된 달의 마지막 일자를 구한다.
	 * 
	 * <pre>
	 *      example :
	 * 
	 *            System.out.println ( DateUtil.getLastDate(&quot;20021030&quot;) );
	 *            System.out.println ( DateUtil.getLastDate(&quot;2002.02.10&quot;) );
	 *            System.out.println ( DateUtil.getLastDate(&quot;020410&quot;) );
	 * 
	 * 
	 *      result :
	 * 
	 *            31
	 *            28
	 *            30
	 * 
	 * 
	 * </pre>
	 * 
	 * @param pDate
	 *            기준일
	 * @return 마지막일자
	 */
	public static int getLastDate(String pDate) {

		java.util.Calendar cal = toCalendar(pDate);

		cal.add(Calendar.MONTH, 1);
		cal.set(Calendar.DATE, 1);
		cal.add(Calendar.DATE, -1);

		return cal.get(Calendar.DATE);
	}

	/**
	 * pDate 부터 pDays 만큼의 날자를 구한다. pDays에 음수(-) 값은 지원하지 않는다.
	 * 
	 * <pre>
	 *      example :
	 * 
	 *            System.out.println ( DateUtil.getNextDate(&quot;20021030&quot; , 10) );
	 *            System.out.println ( DateUtil.getNextDate(&quot;2002.02.10&quot;, 20) );
	 *            System.out.println ( DateUtil.getNextDate(&quot;020410&quot;, 30) );
	 *            System.out.println ( DateUtil.getNextDate(&quot;20021030&quot; , -10) );
	 * 
	 * 
	 *        result :
	 * 
	 *            20021109
	 *            20020302
	 *            20020510
	 *            [DateUtil.getNextDayNum] : Int value under 0 is not allowed and raw data will be returned
	 *            20021030
	 * 
	 * 
	 * </pre>
	 * 
	 * @param pDate
	 *            기준 날짜
	 * @param pDays
	 *            더해질 일자 ( 0 보다 커야 한다. )
	 * @return 변환된날짜
	 */
	public static String getNextDate(String pDate, int pDays) {

		if (pDays < 0) {
			return pDate;
		}

		java.util.Calendar cal = toCalendar(pDate);

		cal.add(Calendar.DATE, pDays);

		return dateFormat.format(cal.getTime());
	}

	/**
	 * pDate(YYYYMMDD)에서 pDays일자 만큼 전의 날짜를 구해준다.
	 * 
	 * <pre>
	 *     example:
	 * 
	 *             System.out.println ( DateUtil.getPrevDayNum(&quot;20020306&quot;, 7) );
	 *             System.out.println ( DateUtil.getPrevDayNum(&quot;2002.05.15&quot;, 30) );
	 * 
	 * 
	 *         result :
	 * 
	 *             20020227
	 *             20020415
	 * </pre>
	 * 
	 * @param pDate
	 *            기준일
	 * @param pDays
	 *            몇일전
	 * @return 수정된 일자
	 */
	public static String getPrevDate(String pDate, int pDays) {
		if (pDays < 0) {
			return pDate;
		}

		java.util.Calendar cal = toCalendar(pDate);

		cal.add(Calendar.DATE, -(pDays));

		return dateFormat.format(cal.getTime());
	}

	/**
	 * pDate가 포함된 달로 부터 pWeeks 주 뒤의 일자 구한다. pWeeks 음수값을 지원하지 않는다.
	 * 
	 * <pre>
	 *      example :
	 * 
	 *             System.out.println ( DateUtil.getNextWeekDate(&quot;20020630&quot;, 4) );
	 *             //4주뒤
	 *             System.out.println ( DateUtil.getNextWeekDate(&quot;2002.05.15&quot;, 2) );
	 *             //2주뒤
	 * 
	 * 
	 *      result :
	 * 
	 *             20020728
	 *             20020529
	 * 
	 * 
	 * </pre>
	 * 
	 * @param pDate
	 *            기준일
	 * @param pWeeks
	 *            몇주후
	 * @return 기준일에서 몇주후 의 날자
	 */
	public static String getNextWeekDate(String pDate, int pWeeks) {
		if (pWeeks < 0) {
			return pDate;
		}

		java.util.Calendar cal = toCalendar(pDate);

		cal.add(Calendar.DATE, pWeeks * 7);

		return dateFormat.format(cal.getTime());
	}

	/**
	 * pDate가 포함된 달로 부터 pWeeks 주 전의 일자 구한다. <br>
	 * pWeeks는 음수 값을 가질 수 없다.
	 * 
	 * <pre>
	 *         example :
	 * 
	 *                 System.out.println ( DateUtil.getPrevWeekDate(&quot;20020321&quot;, 3) );
	 *                 System.out.println ( DateUtil.getPrevWeekDate(&quot;2002.05.30&quot;, 2) );
	 * 
	 * 
	 *         result :
	 * 
	 *                 20020228
	 *                 20020516
	 * </pre>
	 * 
	 * @param pDate
	 *            기준일
	 * @param pWeeks
	 *            몇주전
	 * @return 기준일에서 몇주전의 날자
	 */
	public static String getPrevWeekDate(String pDate, int pWeeks) {
		if (pWeeks < 0) {
			return pDate;
		}

		java.util.Calendar cal = toCalendar(pDate);

		cal.add(Calendar.DATE, pWeeks * (-7));

		return dateFormat.format(cal.getTime());
	}

	/*
	 * * pDate가 포함된 달의 다음달을 구한다. input type은 YYYYMM 만 지원한다.
	 * 
	 * <pre> example :
	 * 
	 * System.out.println ( DateUtil.getNextMonth("200206") );
	 * System.out.println ( DateUtil.getNextMonth("200208") );
	 * System.out.println ( DateUtil.getNextMonth("2002067") );
	 * 
	 * result :
	 * 
	 * 200207 200209 [DDate] Input Format you inserted is not allowed and
	 * original data will be returned. 2002067
	 * 
	 * </pre> @param pDate 기준일 @return 기준일의 다음 년월
	 * 
	 * public static String getNextMonth(String pDate) { String tempDate = "";
	 * if (pDate.length() != 6) { tempDate = toYYYYMMDDDate(pDate);
	 * 
	 * if ( tempDate.length() !=8) { LLog.lafj.println( "[DDate] Input Format
	 * you inserted is not allowed and original data will be returned."); return
	 * pDate; } } SimpleDateFormat yyyyMMFormat = new SimpleDateFormat("yyyyMM",
	 * java.util.Locale.KOREA); String year = pDate.substring(0, 4); String
	 * month = pDate.substring(4, 6);
	 * 
	 * java.util.Calendar cal =
	 * java.util.Calendar.getInstance(java.util.Locale.KOREA);
	 * 
	 * cal.set(Integer.parseInt(year), Integer.parseInt(month), 1);
	 * 
	 * return yyyyMMFormat.format(cal.getTime()); }
	 */
	/*
	 * pDate(YYYYMM) 가 포함된 달의 이전 달을 반환한다. YYYYMM 포맷만 지원한다.
	 * 
	 * <pre> example :
	 * 
	 * System.out.println ( DateUtil.getPrevMonth("200203") );
	 * System.out.println ( DateUtil.getPrevMonth("200205") );
	 * 
	 * 
	 * result :
	 * 
	 * 200202 200204
	 * 
	 * 
	 * </pre> @param pDate 기준일 @return 기준일의 이전 년월
	 * 
	 * public static String getPrevMonth(String pDate) { if (pDate.length() !=
	 * 6) { LLog.lafj.println( "[DDate] Input Format you inserted is not allowed
	 * and original data will be returned."); return pDate; } SimpleDateFormat
	 * yyyyMMFormat = new SimpleDateFormat("yyyyMM", java.util.Locale.KOREA);
	 * String year = pDate.substring(0, 4); String month = pDate.substring(4,
	 * 6);
	 * 
	 * java.util.Calendar cal =
	 * java.util.Calendar.getInstance(java.util.Locale.KOREA);
	 * 
	 * cal.set(Integer.parseInt(year), Integer.parseInt(month) - 1, 1);
	 * 
	 * cal.add(Calendar.MONTH, -1);
	 * 
	 * return yyyyMMFormat.format(cal.getTime()); }
	 */
	/**
	 * pDate에서 pMonths만큼 다음달의 날자를 구한다.
	 * 
	 * <pre>
	 *        example :
	 * 
	 *                System.out.println ( DateUtil.getNextMonthDate(&quot;20020603&quot;,3) );
	 *                System.out.println ( DateUtil.getNextMonthDate(&quot;20020906&quot;,4) );
	 *                System.out.println ( DateUtil.getNextMonthDate(&quot;20020130&quot;,1) );
	 *                // 다음달에 해당일이 없을 경우 마지막 날자 표시
	 * 
	 *        result :
	 * 
	 *                20020903
	 *                20030106
	 *                20020228
	 * 
	 * 
	 * </pre>
	 * 
	 * @param pDate
	 *            기준일
	 * @param pMonths
	 *            개월수
	 * @return 입력된 날자에서 pMonth 이후 달의 날자
	 */
	public static String getNextMonthDate(String pDate, int pMonths) {
		if (pMonths < 0) {
			return pDate;
		}

		java.util.Calendar cal = toCalendar(pDate);

		cal.add(Calendar.MONTH, pMonths);

		return dateFormat.format(cal.getTime());
	}

	/**
	 * pDate날자에서 pMonth만큼 이전 달의 날자를 구한다.
	 * 
	 * <pre>
	 *        example :
	 * 
	 *                System.out.println ( DateUtil.getPrevMonthDate(&quot;20020603&quot;,3) );
	 *                System.out.println ( DateUtil.getPrevMonthDate(&quot;20020206&quot;,4) );
	 *                System.out.println ( DateUtil.getPrevMonthDate(&quot;20020330&quot;,1) );
	 *                // 다음달에 해당일이 없을 경우 마지막 날자 표시
	 * 
	 *            result :
	 * 
	 *                20020303
	 *                20011006
	 *                20020228
	 * 
	 * 
	 * </pre>
	 * 
	 * @param pDate
	 *            기준일
	 * @param pMonth
	 *            개월수
	 * @return 입력된 날자에서 pMonth 이전 달의 날자
	 */
	public static String getPrevMonthDate(String pDate, int pMonth) {
		if (pMonth < 0) {
			return pDate;
		}

		java.util.Calendar cal = toCalendar(pDate);

		cal.add(Calendar.MONTH, -(pMonth));

		return dateFormat.format(cal.getTime());
	}

	/**
	 * pDate날자가 포함된 년도의 윤달 여부를 반환한다.
	 * 
	 * @param pDate
	 *            입력된 일자
	 * @return 윤달 여부
	 */
	public static boolean isLeapYear(String pDate) {
		java.util.GregorianCalendar gCal = new GregorianCalendar();
		if (pDate.length() == 4) {
			gCal.setTime(new Date(toCalendar(pDate + "0101").getTime()
					.getTime()));
		} else {
			gCal.setTime(new Date(toCalendar(pDate).getTime().getTime()));
		}

		return gCal.isLeapYear(gCal.get(Calendar.YEAR));
	}

	/**
	 * 오늘의 요일을 한글로 반환한다.
	 * 
	 * @return 한글로된 요일정보
	 */
	public static String getHanDay() {
		return getHanDay(getDate());
	}

	/**
	 * 해당일자의 요일을 한글로 반환한다.
	 * 
	 * @param pDate
	 *            입력된 일자
	 * @return 한글로된 요일정보
	 */
	public static String getHanDay(String pDate) {

		java.util.Calendar cal = toCalendar(pDate);
		SimpleDateFormat dateForm = new SimpleDateFormat("EEE",
				java.util.Locale.KOREA);

		return dateForm.format(cal.getTime());
	}

	/**
	 * 오늘의 요일을 영어로 반환한다.
	 * 
	 * @return 영어로된 요일정보
	 */
	public static String getEngDay() {
		return getEngDay(getDate());
	}

	/**
	 * 해당일자의 요일을 영어로 반환한다.
	 * 
	 * @param pDate
	 *            입력된 일자
	 * @return 영어로된 요일정보
	 */
	public static String getEngDay(String pDate) {

		java.util.Calendar cal = toCalendar(pDate);

		SimpleDateFormat dateForm = new SimpleDateFormat("EEEEEEEE",
				java.util.Locale.ENGLISH);

		return dateForm.format(cal.getTime());
	}

	/**
	 * 오늘의 요일을 반환한다.
	 * 
	 * @return 요일정보 int 값
	 */
	public static int getDay() {
		GregorianCalendar calendar = new GregorianCalendar();
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * 해당일자의 요일을 반환한다.
	 * 
	 * @param pDate
	 *            입력된 일자
	 * @return 요일정보 int 값
	 */
	public static int getDay(String pDate) {

		java.util.Calendar cal = toCalendar(pDate);

		return cal.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * 서버를 기준으로 현재 날짜를 사용자가 지정한 구분자를 이용하여 반환한다.
	 * 
	 * <pre>
	 * example :
	 * 
	 *      System.out.println ( &quot;오늘 날짜 : &quot; + DateUtil.getDBDate(&quot;/&quot;) );
	 * 
	 * result :
	 * 
	 *      오늘 날짜 : 2002/10/09
	 * 
	 * 
	 * 
	 * </pre>
	 * 
	 * @param seperator
	 *            날자/월/일 사이에 구분자
	 * @return String 서버일자반환
	 */
	public static String getDate(String seperator) {
		String sDate = getDate();
		return sDate.substring(0, 4) + seperator + sDate.substring(4, 6)
				+ seperator + sDate.substring(6, 8);
	}

	/**
	 * 이번달을 알려준다. 실제 자바에서의 1월은 0을 리턴하나, getMonth() 메소드에서는 실제 월과 일치하는 인트값을 리턴한다.
	 * 
	 * <pre>
	 * example :
	 * 
	 *      System.out.println ( &quot;이번달 : &quot; + DateUtil.getMonth() );
	 * 
	 * result :
	 * 
	 *      이번달 : 11
	 * 
	 * 
	 * 
	 * </pre>
	 * 
	 * @return String 서버일자반환
	 */
	public static int getMonth() {
		GregorianCalendar calendar = new GregorianCalendar();
		return calendar.get(Calendar.MONTH) + 1;
	}

	/**
	 * 이번달을 알려준다. 실제 자바에서의 1월은 0을 리턴하나, getMonth() 메소드에서는 실제 월과 일치하는 인트값을 리턴한다.
	 * 
	 * <pre>
	 * example :
	 * 
	 *      System.out.println ( &quot;이번달 : &quot; + DateUtil.getMonth() );
	 * 
	 * result :
	 * 
	 *      이번달 : 11
	 * 
	 * 
	 * 
	 * </pre>
	 * 
	 * @return String 서버일자반환
	 */
	public static String getEngMonth() {
		GregorianCalendar calendar = new GregorianCalendar();

		SimpleDateFormat monthForm = new SimpleDateFormat("MMMMM",
				java.util.Locale.ENGLISH);

		return monthForm.format(calendar.getTime());
	}

	public static String getShortEngMonth() {
		GregorianCalendar calendar = new GregorianCalendar();

		SimpleDateFormat monthForm = new SimpleDateFormat("MMM",
				java.util.Locale.ENGLISH);

		return monthForm.format(calendar.getTime());
	}

	/**
	 * 서버를 기준으로 현재 날짜를 laf.xml에서 등록한 Default 형태로 반환한다.
	 * 
	 * <pre>
	 * example :
	 * 
	 *      System.out.println ( &quot;오늘 날짜 : &quot; + DateUtil.getDBDate() );
	 * 
	 * result :
	 * 
	 *      오늘 날짜 : 20021009
	 * 
	 * 
	 * 
	 * </pre>
	 * 
	 * @return String 서버일자반환
	 */
	public static String getDate() {
		return dateFormat.format(new java.util.Date());
	}

	/**
	 * 연도를 받아서 완전한 네자리 연도로 변환한다.
	 * 
	 * <pre>
	 * ex&gt; 85 &gt;&gt; 1985, 1985 &gt;&gt; 1985, 985 &gt;&gt; 1985, 01 &gt;&gt; 2001, 01 &gt;&gt; 2001, 1 &gt;&gt; 2001
	 * </pre>
	 * 
	 * @param argYear
	 *            변환할 년도
	 * @return String 정상적인 경우 네자리 년도, 오류시에는 Blank String을 반환
	 */
	public static String getYear(String argYear) {
		String year = "";

		if (argYear != null)
			year = argYear.trim();

		if (year.length() > 4 || year.length() == 0) {
			return "";
		}

		for (int inx = 0; inx < year.length(); inx++) {
			if (!java.lang.Character.isDigit(year.charAt(inx))) {
				return "";
			}
		}

		if (year.length() == 4)
			return year;

		switch (year.length()) // 년도가 한자리 혹은 세자리로 입력되는 특수 경우 처리
		{
		case 1:
			year = "0" + year;
			break;
		case 3:
			year = year.substring(1, 3);
			break;
		default:
			break;
		}

		if (Integer.parseInt(year) > 50)
			year = "19" + year;
		else
			year = "20" + year;

		return year;
	}

	/**
	 * 입력한 일자 유형을 표준형식 'YYYY.MM.DD'으로 변환해서 반환한다.
	 * 
	 * @param argDate
	 *            대상 일자( 1998.01.02, 98.01.02, 19980102, 980102 등 )
	 * @return 변환된 문자열 또는 잘못된 일자의 경우 Blank String을 반환
	 */
	public static String toBaseDate(String argDate) {
		String formattedDate = toYYYYMMDDDate(argDate);

		try {
			Integer.parseInt(formattedDate);
		} catch (NumberFormatException ne) {
			return "";
		}

		if (formattedDate != "") {
			return formattedDate.substring(0, 4) + "."
					+ formattedDate.substring(4, 6) + "."
					+ formattedDate.substring(6, 8);
		}

		return argDate;
	}

	/**
	 * 어떤 날자가( 1998.01.02, 98.01.02, 19980102, 980102 ) 들어오건 간에 YYYYMMDD로 변환한다.
	 * 
	 * @param argDate
	 *            변환할 일자( 1998.01.02, 98.01.02, 19980102, 980102 등 )
	 * @return 변환된 일자 또는 잘못된 일자의 경우 Blank String을 반환
	 */
	public static String toYYYYMMDDDate(String argDate) {
		boolean isMunja = false;
		boolean isCorrectArg = true;
		String subArg = "";
		String date = "";
		String result = "";

		if (argDate != null)
			subArg = argDate.trim();

		for (int inx = 0; inx < subArg.length(); inx++) {
			if (java.lang.Character.isLetter(subArg.charAt(inx))
					|| subArg.charAt(inx) == ' ') {
				isCorrectArg = false;
				break;
			}
		}

		if (!isCorrectArg) {
			return "";
		}

		if (subArg.length() != 8) {
			if (subArg.length() != 6 && subArg.length() != 10) {
				return "";
			}

			if (subArg.length() == 6) {
				if (Integer.parseInt(subArg.substring(0, 2)) > 50)
					date = "19";
				else
					date = "20";

				result = date + subArg;
			}

			if (subArg.length() == 10)
				result = subArg.substring(0, 4) + subArg.substring(5, 7)
						+ subArg.substring(8, 10);
		} else {// 8자린 경우 ( 98.01.02, 19980102 )

			try {
				Integer.parseInt(subArg);
			} catch (NumberFormatException ne) {
				isMunja = true;
			}

			if (isMunja) // 98.01.02 혹은 98/01/02 형식의 포맷일 경우
			{
				date = subArg.substring(0, 2) + subArg.substring(3, 5)
						+ subArg.substring(6, 8);
				if (Integer.parseInt(subArg.substring(0, 2)) > 50)
					result = "19" + date;
				else
					result = "20" + date;
			} else
				// 19980102 형식의 포맷인 경우
				return subArg;
		}
		return result;
	}

	/**
	 * 입력된 일자에 해당하는 음력일자를 반환한다.
	 * 
	 * @param argDate
	 *            변환할 일자( 1998.01.02, 98.01.02, 19980102, 980102 등 )
	 * @return 음력일자
	 */
	public static String toLunarDate(String argDate) {
		int lyear, lmonth, lday = 0;
		int i, j, m1, m2, jcount = 0;
		boolean ll = false;
		long td, td0, td1 = 0;

		for (i = 0; i < 163; i++) {
			dt[i] = 0;
			for (j = 0; j < 12; j++) {
				switch (kk[i * 13 + j]) {
				case 1:
				case 3:
					dt[i] = dt[i] + 29;
					break;
				case 2:
				case 4:
					dt[i] = dt[i] + 30;
				}
			}

			switch (kk[i * 13 + 12]) {
			case 0:
				break;
			case 1:
			case 3:
				dt[i] = dt[i] + 29;
				break;
			case 2:
			case 4:
				dt[i] = dt[i] + 30;
				break;
			}
		}
		td1 = 1880 * 365L + 1880 / 4 - 1880 / 100 + 1880 / 400 + 30;

		String standardDate = toYYYYMMDDDate(argDate);
		int syear = Integer.parseInt(standardDate.substring(0, 4));
		int smonth = Integer.parseInt(standardDate.substring(4, 6));
		int sday = Integer.parseInt(standardDate.substring(6, 8));

		long k11 = (long) (syear - 1);
		long td2 = k11 * 365L + k11 / 4L - k11 / 100L + k11 / 400L;
		ll = syear % 400 == 0 || syear % 100 != 0 && syear % 4 == 0;
		if (ll)
			dateCount[1] = 29;
		else
			dateCount[1] = 28;

		for (i = 0; i < smonth - 1; i++)
			td2 = td2 + (long) dateCount[i];

		td2 = td2 + (long) sday;

		/* ## 1881. 1. 30. - syear. smonth. sday. ## */
		td = td2 - td1 + 1;

		/* ## Lunar Year Caculation ## */
		td0 = (long) dt[0];
		for (i = 0; i < 163; i++) {
			if (td <= td0)
				break;
			td0 = td0 + (long) dt[i + 1];
		}
		lyear = i + 1881; /* Calculated Lunar Year */

		/* ## Lunar Month Calculation ## */
		td0 = td0 - (long) dt[i];
		td = td - td0;

		if (kk[i * 13 + 12] != 0)
			jcount = 13;
		else
			jcount = 12;
		m2 = 0;

		for (j = 0; j < jcount; j++) {
			if (kk[i * 13 + j] <= 2)
				m2++;
			if (kk[i * 13 + j] <= 2)
				m1 = kk[i * 13 + j] + 28;
			else
				m1 = kk[i * 13 + j] + 26;
			if (td <= (long) m1)
				break;
			td = td - (long) m1;
		}

		lmonth = m2; /* Calculated Lunar Month */

		lday = (int) td; /* Calculated Lunar Day */

		i = (int) ((td2 + 4L) % 10L);
		j = (int) ((td2 + 2L) % 12L);

		Date lunarDate = createDate(lyear, lmonth, lday);

		return dateFormat.format(lunarDate);
	}

	/**
	 * 입력된 음력일자에 해당하는 양력일자를 반환한다.
	 * 
	 * @param argDate
	 *            변환할 일자( 1998.01.02, 98.01.02, 19980102, 980102 등 )
	 * @return 양력일자
	 */
	public static String toSolarDate(String argDate) {
		return toSolarDate(argDate, false);
	}

	/**
	 * 입력된 음력일자에 해당하는 양력일자를 반환한다.
	 * 
	 * @param argDate
	 *            변환할 일자( 1998.01.02, 98.01.02, 19980102, 980102 등 )
	 * @param isLeap
	 *            윤달 여부
	 * @return 양력일자
	 */
	public static String toSolarDate(String argDate, boolean isLeap) {
		int syear, smonth, sday = 0;
		int i, j, m1, m2 = 0;
		boolean leap = false;
		long td = 0;

		for (i = 0; i < 163; i++) {
			dt[i] = 0;
			for (j = 0; j < 12; j++) {
				switch (kk[i * 13 + j]) {
				case 1:
				case 3:
					dt[i] = dt[i] + 29;
					break;
				case 2:
				case 4:
					dt[i] = dt[i] + 30;
				}
			}

			switch (kk[i * 13 + 12]) {
			case 0:
				break;
			case 1:
			case 3:
				dt[i] = dt[i] + 29;
				break;
			case 2:
			case 4:
				dt[i] = dt[i] + 30;
				break;
			}
		}

		String standardDate = toYYYYMMDDDate(argDate);
		int lyear = Integer.parseInt(standardDate.substring(0, 4));
		int lmonth = Integer.parseInt(standardDate.substring(4, 6));
		int lday = Integer.parseInt(standardDate.substring(6, 8));

		boolean isLeapYear = isLeap;

		if (!isLeapYear && !verifyDate(lyear, lmonth, lday, "To Solar-")) {
			throw new IllegalArgumentException("Date Range Error");
		}

		if (isLeapYear && !verifyDate(lyear, lmonth, lday, "To Solar+")) {
			throw new IllegalArgumentException("Date Range or Leap Check Error");
		}

		m1 = -1;
		td = 0L;

		if (lyear != 1881) {
			m1 = lyear - 1882;
			for (i = 0; i <= m1; i++) {
				for (j = 0; j < 13; j++)
					td = td + (long) kk[i * 13 + j];
				if (kk[i * 13 + 12] == 0)
					td = td + 336L;
				else
					td = td + 362L;
			}
		}

		m1++;
		int n2 = lmonth - 1;
		m2 = -1;

		while (true) {
			m2++;
			if (kk[m1 * 13 + m2] > 2) {
				td = td + 26L + (long) kk[m1 * 13 + m2];
				n2++;
			} else if (m2 == n2)
				break;
			else
				td = td + 28L + (long) kk[m1 * 13 + m2];
		}
		;

		if (isLeapYear)
			td = td + 28L + (long) kk[m1 * 13 + m2];

		td = td + (long) lday + 29L;
		m1 = 1880;

		while (true) {
			m1++;
			leap = m1 % 400 == 0 || m1 % 100 != 0 && m1 % 4 == 0;
			if (leap)
				m2 = 366;
			else
				m2 = 365;
			if (td < (long) m2)
				break;
			td = td - (long) m2;
		}
		;

		syear = m1;
		dateCount[1] = m2 - 337;
		m1 = 0;
		while (true) {
			m1++;
			if (td <= (long) dateCount[m1 - 1])
				break;
			td = td - (long) dateCount[m1 - 1];
		}
		;

		smonth = m1;
		sday = (int) td;

		long y = syear - 1L;

		td = (long) (y * 365) + (long) (y / 4) - (long) (y / 100)
				+ (long) (y / 400);

		leap = syear % 400 == 0 || syear % 100 != 0 && syear % 4 == 0;

		if (leap)
			dateCount[1] = 29;
		else
			dateCount[1] = 28;

		for (i = 0; i < smonth - 1; i++)
			td = td + (long) dateCount[i];
		td = td + (long) sday;

		i = (int) (td % 10L);
		i = (i + 4) % 10;
		j = (int) (td % 12L);
		j = (j + 2) % 12;

		Date solarDate = createDate(syear, smonth, sday);

		return dateFormat.format(solarDate);
	}

	/**
	 * 입력된 일자를 Calendar 객체로 반환한다.
	 * 
	 * @param pDate
	 *            변환할 일자( 1998.01.02, 98.01.02, 19980102, 980102 등 )
	 * @return 해당일자에 해당하는 Calendar
	 */
	public static Calendar toCalendar(String pDate) {
		String date = toYYYYMMDDDate(pDate);

		return createCalendar(Integer.parseInt(date.substring(0, 4)), Integer
				.parseInt(date.substring(4, 6)), Integer.parseInt(date
				.substring(6, 8)));
	}

	/**
	 * 입력된 일자를 Date 객체로 반환한다.
	 * 
	 * @param pDate
	 *            변환할 일자( 1998.01.02, 98.01.02, 19980102, 980102 등 )
	 * @return 해당일자에 해당하는 Calendar
	 */
	public static Date toDate(String pDate) {
		String date = toYYYYMMDDDate(pDate);

		return createDate(Integer.parseInt(date.substring(0, 4)), Integer
				.parseInt(date.substring(4, 6)), Integer.parseInt(date
				.substring(6, 8)));
	}

	/**
	 * 입력된 일자를 Date 객체로 반환한다.
	 * 
	 * @param year
	 *            년
	 * @param month
	 *            월
	 * @param date
	 *            일
	 * @return 해당일자에 해당하는 Date
	 */
	public static Date createDate(int year, int month, int date) {
		return createCalendar(year, month, date).getTime();
	}

	/**
	 * 입력된 정보를 Date 객체로 반환한다.
	 * 
	 * @param year
	 *            년
	 * @param month
	 *            월
	 * @param date
	 *            일
	 * @param hour
	 *            시
	 * @param minute
	 *            분
	 * @return 해당일자에 해당하는 Date
	 */
	public static Date createDate(int year, int month, int date, int hour,
			int minute) {
		return createCalendar(year, month, date, hour, minute).getTime();
	}

	/**
	 * 입력된 정보를 Date 객체로 반환한다.
	 * 
	 * @param year
	 *            년
	 * @param month
	 *            월
	 * @param date
	 *            일
	 * @param hour
	 *            시
	 * @param minute
	 *            분
	 * @param second
	 *            초
	 * @return 해당일자에 해당하는 Date
	 */
	public static Date createDate(int year, int month, int date, int hour,
			int minute, int second) {
		return createCalendar(year, month, date, hour, minute, second)
				.getTime();
	}

	/**
	 * 입력된 일자를 Calendar 객체로 반환한다.
	 * 
	 * @param year
	 *            년
	 * @param month
	 *            월
	 * @param date
	 *            일
	 * @return 해당일자에 해당하는 Calendar
	 */
	public static Calendar createCalendar(int year, int month, int date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(year, month - 1, date);
		return calendar;
	}

	/**
	 * 입력된 정보를 Calendar 객체로 반환한다.
	 * 
	 * @param year
	 *            년
	 * @param month
	 *            월
	 * @param date
	 *            일
	 * @param hour
	 *            시
	 * @param minute
	 *            분
	 * @return 해당일자에 해당하는 Calendar
	 */
	public static Calendar createCalendar(int year, int month, int date,
			int hour, int minute) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(year, month - 1, date, hour, minute);
		return calendar;
	}

	/**
	 * 입력된 정보를 Calendar 객체로 반환한다.
	 * 
	 * @param year
	 *            년
	 * @param month
	 *            월
	 * @param date
	 *            일
	 * @param hour
	 *            시
	 * @param minute
	 *            분
	 * @param second
	 *            초
	 * @return 해당일자에 해당하는 Calendar
	 */
	public static Calendar createCalendar(int year, int month, int date,
			int hour, int minute, int second) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(year, month - 1, date, hour, minute, second);
		return calendar;
	}

	/**
	 * 입력된 일자가 양력이나 음력으로 변환 가능한지 여부를 반환한다.
	 * 
	 * @param year
	 *            년
	 * @param month
	 *            월
	 * @param day
	 *            일
	 * @param whatToDo
	 *            음력 또는 양력 으로 변환여부
	 * @return 변환 가능한지에 대한 boolean 값
	 */
	private static boolean verifyDate(int year, int month, int day,
			String whatToDo) {
		if (year < 1881 || year > 2043 || month < 1 || month > 12) {
			return false;
		}

		if (whatToDo.equals("To Lunar")) {
			if (day > dateCount[month - 1])
				return false;
		}

		if (whatToDo.equals("To Solar+")) {
			if (kk[(year - 1881) * 13 + 12] < 1)
				return false;
			if (kk[(year - 1881) * 13 + month] < 3)
				return false;
			if ((kk[(year - 1881) * 13 + month] + 26) < day)
				return false;
		}

		if (whatToDo.equals("To Solar-")) {
			int j = month - 1;
			for (int i = 1; i <= 12; i++)
				if (kk[(year - 1881) * 13 + i - 1] > 2)
					j++;
			if (day > (kk[(year - 1881) * 13 + j] + 28))
				return false;
		}
		return true;
	}
	
//	---------------------------------------------------------------------------------
    // 함수 설명 :	원하는 자릿수만큼 0을 앞에 채워주는 함수
	//---------------------------------------------------------------------------------    
	//	changeIntValue		|		strNum
	//---------------------------------------------------------------------------------    
	//	Value				|		반환되어질 전체 자릿수
	//---------------------------------------------------------------------------------    
    // 반 환 값  :   0 이 채워진 숫자		(01,001,000002, .... )
	//---------------------------------------------------------------------------------    	
	public static String AddZeroFirst(String changeIntValue, int strNum) {
		if (strNum < 0) return ("Parameter two is wrong");
		int intValueLength = changeIntValue.trim().length();
		
		if (intValueLength >= strNum) {
			return (changeIntValue);
		} else {
			String returnValue = changeIntValue;
			for (int i = 0 ; i < (strNum - intValueLength) ; i++) {
				returnValue = "0" + returnValue;
			}
			return (returnValue);
		}
	}
	
//	---------------------------------------------------------------------------------
    // 함수 설명 :	현재 날짜 함수
	//---------------------------------------------------------------------------------    
	// 인자 설명 :	없다
	//---------------------------------------------------------------------------------    
    // 반 환 값  :  8 자리의 현재 날짜	( 20000904 )
	//---------------------------------------------------------------------------------    
	public static String GetCurrDateNumber() {
		java.util.TimeZone zone = java.util.TimeZone.getTimeZone("GMT+9:00");			
		java.util.Calendar c = java.util.Calendar.getInstance(zone);		

		String returnDate 	= 	"" +  c.get(java.util.Calendar.YEAR        );
		String currentMonth =  	"" + (c.get(java.util.Calendar.MONTH)  + 1 );
		String currentDay   =  	"" +  c.get(java.util.Calendar.DAY_OF_MONTH);
		
		returnDate			+= 	AddZeroFirst(currentMonth, 2);
		returnDate			+= 	AddZeroFirst(currentDay  , 2);

		return (returnDate) ;
	}
	
	/**
	 * 년을 SELECT박스로
	 * @param i 
	 * @param sChkValue 선택년도
	 * @return
	 */
	public static String ComboListYear(int eYear, String sChkValue){
		StringBuffer sb = new StringBuffer();
		
		String sDate = GetCurrDateNumber();
		String sYear = "";
		int iYear = 0;
		if(sDate != null && sDate.length()>0){
			sYear = sDate.substring(0,4);
		}
		iYear = Integer.parseInt(sYear);
		
		//for(int i=(iYear-iNum); i<(iYear+1); i++){
		
		for(int i=eYear; i<(iYear+1); i++){
			if(sChkValue.equals(String.valueOf(i))){
				sb.append(" <option value=\""+i+"\" selected>"+i+"</option>");
			} else {
				if((i==iYear) && (sChkValue.length()==0)) {
					sb.append(" <option value=\""+i+"\" selected>"+i+"</option>");
				} else {
					sb.append(" <option value=\""+i+"\">"+i+"</option>");
				}
			}
			
		}
		
		return sb.toString();
		
	}
	
	/**
	 * 현재시간
	 * yyyy : 년도
	 * MM : 월
	 * dd : 일
	 * hh : 시간( 12시간단위로 1-12)
	 * HH : 시간( 24시간단위로 1-24)
	 * mm : 분
	 * ss : 초
	 * SSS : Millsecond
	 * @return
	 */
	public static String getDateTime() {
		java.text.SimpleDateFormat f = new java.text.SimpleDateFormat("yyyyMMddhhmmssSSS");
		f.setTimeZone(java.util.TimeZone.getTimeZone("Asia/Seoul"));
		return f.format(new java.util.Date());
	}
}