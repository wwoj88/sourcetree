package kr.or.copyright.mls.support.constant;

import kr.or.copyright.mls.support.constant.Constants;

/**
 * KICA (한국정보인증) 서버 인증서 정보
 * 
 * @author 이상유
 * @version $Id: SignContants.java,v 1.1 2012/07/02 01:05:48 yagin14 Exp $
 */
public class SignContants {

	public static final String Cert_Directory = "ServerCertPath";	// 서버 인증서 경로 Property 명
	public static final String Cert_Passwd = "ServerKeyPassword";	// 서버 인증서 비밀번호 Property 명

	public static final String signCertFile = "signCert.der"; // 서버 전자서명용 인증서 파일명
	public static final String signPriFile = "signPri.key"; // 서버 전자서명용 개인키 파일명
	public static final String kmCertFile = "kmCert.der"; // 서버 암호화용 인증서 파일명
	public static final String kmPriFile = "kmPri.key"; // 서버 암호화용 개인키 파일명
	public static final String CRLPath = "CRL"; // 인증서 폐지목록 저장 디렉토리명
	
	/** 서버인증서 경로 **/
	public static String ServerCertPath = Constants.getProperty(Cert_Directory); // 서버 인증서 저장 디렉토리
	public static String CRLCacheDirectory = ServerCertPath + CRLPath; // 인증서 폐지목록 저장 디렉토리
	
	public static String ServerSignCertFile = ServerCertPath + signCertFile; // 서버 전자서명용 인증서 파일명
	public static String ServerSignKeyFile = ServerCertPath + signPriFile; // 서버 전자서명용 개인키 파일명
	public static String ServerKmCertFile = ServerCertPath + kmCertFile; // 서버 암호화용 인증서 파일명
	public static String ServerKmKeyFile = ServerCertPath + kmPriFile; // 서버 암호화용 개인키 파일명
	
	public static String ServerKeyPassword = Constants.getProperty(Cert_Passwd); // 서버 인증서(개인키) 비밀번호
	
	
	// 허용할 인증서 정책 OID 리스트 (범용 인증서 OID 리스트)
	public static final String[] AllowedPolicyOIDs = {
		"1.2.410.200004.5.2.1.2",	//한국정보인증(개인)
		"1.2.410.200004.5.2.1.1",	//한국정보인증(법인)
		"1.2.410.200004.5.1.1.5",	//증권전산(개인)
		"1.2.410.200004.5.1.1.7",	//증권전산(법인)
		"1.2.410.200005.1.1.1",	//금융결제원(개인)
		"1.2.410.200005.1.1.5",	//금융결제원(법인)
		"1.2.410.200004.5.3.1.9",	//한국전산원(개인)
		"1.2.410.200004.5.3.1.2",	//한국전산원(법인)
		"1.2.410.200004.5.3.1.1",	//한국전산원(기관)
		"1.2.410.200004.5.4.1.1",	//전자인증(개인)
		"1.2.410.200004.5.4.1.2",	//전자인증(법인)
		"1.2.410.200012.1.1.1",	//한국무역정보통신(개인)
		"1.2.410.200012.1.1.3",	//한국무역정보통신(법인)
		"1.2.410.200004.5.2.1.6.181"	//저작권위원회 인증서 (용도제한-저작권라이선스 전자계약 및 문화부)
	};
	
	public static final String CHALLENGE = "CHALLENGE"; // 재사용공격을 막기 위해서 사용자 인증 데이터에 매번 변하는 값(nonce)을 SESSION 에 저장하기 위한 속성명
	public static final String SIGN_VALIDATE = "signValidate"; // 인증서 유효성 검사시 발생되는 오류정보를 저장하기 위한 속성명


}
