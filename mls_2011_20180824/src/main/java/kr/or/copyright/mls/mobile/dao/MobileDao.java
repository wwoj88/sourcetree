package kr.or.copyright.mls.mobile.dao;

import java.util.HashMap;
import java.util.List;

import kr.or.copyright.mls.mobile.model.FAQ;
import kr.or.copyright.mls.mobile.model.Stat;

public interface MobileDao {
	List<Stat> statList(HashMap<String,Object> hm);
	int statListCnt(HashMap<String,Object> hm);
	Stat statDetl(int bordSeqn);
	
	List<FAQ> faqList(HashMap<String,Object> hm);
	int faqListCnt(HashMap<String,Object> hm);
	void insertUserMobileCount();
	void insertUserMobileCount(int sysCd);
}
