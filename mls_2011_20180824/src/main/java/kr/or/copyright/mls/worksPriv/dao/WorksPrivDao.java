package kr.or.copyright.mls.worksPriv.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.board.model.Board;
import kr.or.copyright.mls.board.model.BoardFile;
import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.worksPriv.model.WorksPriv;
import kr.or.copyright.mls.worksPriv.model.WorksPrivFile;

public interface WorksPrivDao {
	
//	LIST 마이페이지 총 갯수
	public int findWorksPrivCount(Map params);
	
//	LIST 참여마당 총 갯수
	public int findWorksPrivCountO(Map params);

//	LIST 마이페이지 
	public List findWorksPrivList(Map params);

//	LIST 참여마당
	public List findWorksPrivListO(Map params);
	
//	분야 내용
	public List<CodeList> findCodeList(CodeList codeList);
	
//	개인저작물 등록  최신 번호 가져오기
	public int worksPrivMax(WorksPriv worksPriv);
	
//	개인저작물 등록 하기
	public void goInsert(WorksPriv worksPriv);
	
//  개인저작물 파일 등록하기
	public void goInsertFile(WorksPrivFile worksPrivFile);

//	개인저작물 상세보기
	public WorksPriv goView(WorksPriv worksPriv);
	
//	개인저작물 파일 리스트
	public List worksPrivFileList(WorksPriv worksPriv);
	
//	파일 삭제
	public void fileDelete(WorksPrivFile worksPrivFile);
	
//	개인저작물 수정
	public void goUpdate(WorksPriv worksPriv);
	
//	개인저작물 삭제
	public void goDelete(WorksPriv worksPriv);
}
