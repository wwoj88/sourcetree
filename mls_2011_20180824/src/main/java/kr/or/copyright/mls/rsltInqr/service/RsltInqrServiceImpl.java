package kr.or.copyright.mls.rsltInqr.service;

import java.util.List;

import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.inmtPrps.model.InmtPrps;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;
import kr.or.copyright.mls.rsltInqr.dao.RsltInqrDao;

public class RsltInqrServiceImpl extends BaseService  implements RsltInqrService{

	private RsltInqrDao rsltInqrDao;

	public void setRsltInqrDao( RsltInqrDao rsltInqrDao) {
		this.rsltInqrDao = rsltInqrDao;
	}
	
	// 신청내역목록
	public ListResult rsltInqrList(int pageNo, int rowPerPage, RghtPrps rghtPrps){
		
		List list = rsltInqrDao.rsltInqrList(rghtPrps);
		
		int totalRow = rsltInqrDao.rsltInqrCount(rghtPrps);
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
		
		return listResult;
		
	}
	
	// 권리찾기 신청상세 기본사항
	public RghtPrps rghtPrpsDetl(RghtPrps rghtPrps){
		return rsltInqrDao.rghtPrpsDetl(rghtPrps);
	}
	
	// 권리찾기 첨부파일리스트
	public List rghtFileList(RghtPrps rghtPrps){
		return rsltInqrDao.rghtFileList(rghtPrps);
	}
	
	// 권리찾기 신청처리결과 목록
	public List rsltTrstWorksList(RghtPrps rghtPrps){
		return rsltInqrDao.rsltTrstWorksList(rghtPrps);
	}
	
	// 권리찾기 신청처리결과 상세
	public RghtPrps rsltTrstWorksDesc(RghtPrps rghtPrps){
		RghtPrps dto = new RghtPrps();
		
		if(rghtPrps.getDIVS()!=null){
			dto =  rsltInqrDao.rsltTrstWorksDesc(rghtPrps);
		}else{
			if(rghtPrps.getTRST_ORGN_CODE().equals("205_2"))
				rghtPrps.setTRST_ORGN_CODE("205");
			
			dto =  rsltInqrDao.inmtRsltSelect(rghtPrps);
		}
		return dto; 
	}
	
	// 음악 권리찾기 저작물목록
	public List rghtPrpsWorkList(RghtPrps rghtPrps){
		
		List workList = null;
		
		if(rghtPrps.getDIVS().equals("M"))
			workList =  rsltInqrDao.muscPrpsList(rghtPrps);
		if(rghtPrps.getDIVS().equals("O"))
			workList =  rsltInqrDao.bookPrpsList(rghtPrps);
		if(rghtPrps.getDIVS().equals("N"))
			workList =  rsltInqrDao.newsPrpsList(rghtPrps);
		if(rghtPrps.getDIVS().equals("C"))
			workList =  rsltInqrDao.scriptPrpsList(rghtPrps);
		if(rghtPrps.getDIVS().equals("I"))
			workList =  rsltInqrDao.imagePrpsList(rghtPrps);
		if(rghtPrps.getDIVS().equals("V"))
			workList =  rsltInqrDao.mviePrpsList(rghtPrps);
		if(rghtPrps.getDIVS().equals("R"))
			workList =  rsltInqrDao.broadcastPrpsList(rghtPrps);
		if(rghtPrps.getDIVS().equals("X"))
			workList =  rsltInqrDao.sidePrpsList(rghtPrps);
		
		return workList;
	}
	
	// 보상금 신청상세 기본사항(보상금신청 신청자 정보)
	public InmtPrps inmtPrpsDetl(RghtPrps rghtPrps){
		return rsltInqrDao.inmtPrpsDetl(rghtPrps);
	}
	
	// 보상금신청처리결과 조회(보상금신청 저작물 목록)
	public List inmtPrpsRsltList(RghtPrps rghtPrps){
		return rsltInqrDao.inmtPrpsRsltList(rghtPrps);
	}
	
	// 신청목적구분코드(DEAL_STAT)
	public String inmtDealStatTotal(RghtPrps rghtPrps){
		String inmtDealStatTotal = rsltInqrDao.inmtDealStatTotal(rghtPrps);
		return inmtDealStatTotal;
	}
	
	// 보상금신청저작물 목록
	public List inmtWorkList(RghtPrps rghtPrps){
		return rsltInqrDao.inmtWorkList(rghtPrps);
	}
	
	// 보상금 음제협 신청내역
	public List inmtKappRsltList(RghtPrps rghtPrps){
		return rsltInqrDao.inmtKappRsltList(rghtPrps);
	}
	
	// 보상금 음실연 신청내역
	public List inmtFokapoRsltList(RghtPrps rghtPrps){
		return rsltInqrDao.inmtFokapoRsltList(rghtPrps);
	}
	
	// 보상금 복전협 신청내역
	public List inmtKrtraRsltList(RghtPrps rghtPrps){
		return rsltInqrDao.inmtKrtraRsltList(rghtPrps);
	}
	
	// 보상금신청 저작물내역
	public List inmtTempRsltList(RghtPrps rghtPrps){
		return rsltInqrDao.inmtTempRsltList(rghtPrps);
	}

	// 보상금신청 첨부파일내역
	public List inmtFileList(RghtPrps rghtPrps){
		return rsltInqrDao.inmtFileList(rghtPrps);
	}

	// 보상금신청 전체처리상태 값
	public String inmtTotalDealStat(RghtPrps rghtPrps){
		return rsltInqrDao.inmtTotalDealStat(rghtPrps);
	}
	
	// maxDealStat 구하기
	public String isValidDealStat(RghtPrps rghtPrps){
		
		String maxDealStat = rsltInqrDao.getMaxDealStat(rghtPrps);
		return maxDealStat;
		
	}
	
	// 신청내역삭제 
	public void deleteRsltInqr(RghtPrps rghtPrps){
		
		try{
			
			rsltInqrDao.prpsDelete(rghtPrps);
			rsltInqrDao.prpsRsltDelete(rghtPrps);
			rsltInqrDao.kappPrpsDelete(rghtPrps);
			rsltInqrDao.fokapoPrpsDelete(rghtPrps);
			rsltInqrDao.krtraPrpsDelete(rghtPrps);
			rsltInqrDao.prpsRsltRghtDelete(rghtPrps);
			
			rsltInqrDao.prpsFileDelete(rghtPrps);
		}finally{
			
		}
		
	}
}
