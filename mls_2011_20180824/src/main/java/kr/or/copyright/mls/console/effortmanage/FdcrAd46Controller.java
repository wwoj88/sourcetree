package kr.or.copyright.mls.console.effortmanage;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.effortmanage.inter.FdcrAd46Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 상당한 노력 관리 > 거소불명저작물 신청현황
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd46Controller extends DefaultController{

	@Resource( name = "fdcrAd46Service" )
	private FdcrAd46Service fdcrAd46Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd46Controller.class );

	/**
	 * 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effortmanage/fdcrAd46List1.page" )
	public String fdcrAd46List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd46List1 Start" );
		// 파라미터 셋팅
		commandMap.put( "YYYY", "" );

		fdcrAd46Service.fdcrAd46List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "fdcrAd46List1 End" );
		return "test";
	}
}
