package kr.or.copyright.mls.console.event;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.event.inter.FdcrAdA0Dao;
import kr.or.copyright.mls.console.event.inter.FdcrAdA0Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdA0Service" )
public class FdcrAdA0ServiceImpl extends CommandService implements FdcrAdA0Service{

	@Resource( name = "fdcrAdA0Dao" )
	private FdcrAdA0Dao fdcrAdA0Dao;

	/**
	 * 이벤트 등록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings( "unchecked" )
	public boolean fdcrAdA0Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String seq = fdcrAdA0Dao.getSaveEventSeq();// 시퀀스

			commandMap.put( "EVENT_ID", seq );

			// 파일업로드
			String fileName = (String) commandMap.get( "IMAGE_FILE_PATH" );
			if( "Y".equals( commandMap.get( "FILE_UP_YN" ) ) ){
				byte[] file = (byte[]) commandMap.get( "CLIENT_FILE" );
				Map upload = FileUtil.uploadMiFile( fileName, file, "event/" );
				commandMap.put( "IMAGE_FILE_PATH", upload.get( "FILE_PATH" ) + "" + upload.get( "REAL_FILE_NAME" ) );
				commandMap.put( "IMAGE_FILE_NAME", commandMap.get( "IMAGE_FILE_NAME" ) );
			}

			String fileName2 = (String) commandMap.get( "IMAGE_FILE_PATH_WIN" );
			if( "Y".equals( commandMap.get( "FILE_UP_WIN_YN" ) ) ){
				byte[] file = (byte[]) commandMap.get( "CLIENT_FILE_WIN" );
				Map upload = FileUtil.uploadMiFile( fileName2, file, "event/" );
				commandMap.put( "IMAGE_FILE_PATH_WIN", upload.get( "FILE_PATH" ) + "" + upload.get( "REAL_FILE_NAME" ) );
				commandMap.put( "IMAGE_FILE_NAME_WIN", commandMap.get( "IMAGE_FILE_NAME_WIN" ) );
			}

			fdcrAdA0Dao.addEvent( commandMap );// 등록

			ArrayList<Map<String,Object>> agreeList = (ArrayList<Map<String,Object>>)commandMap.get( "agreeList" );
			if(null!=agreeList){
				for( int i = 0; i < agreeList.size(); i++ ){
					Map<String, Object> mAgree = agreeList.get( i );
					mAgree.put( "EVENT_ID", seq );
					if( "Y".equals( (String)mAgree.get( "AGREEYN" ) ) ){
						fdcrAdA0Dao.addEventAgree( mAgree );// 동의항목 등록
					}
				}
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
