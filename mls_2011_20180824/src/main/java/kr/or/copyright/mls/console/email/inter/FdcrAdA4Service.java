package kr.or.copyright.mls.console.email.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface FdcrAdA4Service{

	/**
	 * 주소록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA4List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 전체 주소록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA4List2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 메일 저장하기
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA4Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 메일 보내기
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA4Insert2( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;
	
	/**
	  * <PRE>
	  * 간략 : 메일 메세지 상세보기
	  * 상세 : 메일 메세지 상세보기
	  * <PRE>
	  * @param commandMap
	  * @throws Exception 
	  */
	public void fdcrAdA4View1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	  * <PRE>
	  * 간략 :메일 수정하기 
	  * 상세 :메일 수정하기 
	  * <PRE>
	  * @param commandMap
	  * @param fileList
	  * @return
	  * @throws Exception 
	  */
	public boolean fdcrAdA4Update1( Map<String, Object> commandMap, ArrayList<Map<String, Object>> fileList ) throws Exception;
	

}
