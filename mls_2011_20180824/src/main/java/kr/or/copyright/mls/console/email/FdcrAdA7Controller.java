package kr.or.copyright.mls.console.email;

import java.net.URLDecoder;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.support.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 안내메일관리 > 보완메일 발신내역
 * 
 * @author ljh
 */
@Controller
public class FdcrAdA7Controller extends DefaultController{

	@Resource( name = "fdcrAdA7Service" )
	private FdcrAdA7ServiceImpl fdcrAdA7Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA7Controller.class );

	/**
	 * 신청정보 보완안내 메일 발신내역 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA7List1.page" )
	public String fdcrAdA7List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String msgStorCd = StringUtil.nullToEmpty( (String) commandMap.get( "MSG_STOR_CD" ) );
		if("".equals(msgStorCd)){
			commandMap.put( "MSG_STOR_CD", "4" );
		}else{
			commandMap.put( "MSG_STOR_CD", msgStorCd );
		}
		
		String KEYWORD = EgovWebUtil.getString( commandMap, "KEYWORD" );
		if( !"".equals( KEYWORD ) ){
			KEYWORD = URLDecoder.decode( KEYWORD, "UTF-8" );
			commandMap.put( "KEYWORD", KEYWORD );
		}
		fdcrAdA7Service.fdcrAdA7List1( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "commandMap", commandMap );
		System.out.println( "보완안내 메일 발신내역 조회" );
		return "email/fdcrAdA7List1.tiles";
	}
	
	/**
	  * <PRE>
	  * 간략 : 
	  * 상세 : 
	  * <PRE>
	  * @param model
	  * @param commandMap
	  * @param request
	  * @param response
	  * @return
	  * @throws Exception 
	  */
	@RequestMapping( value = "/console/email/fdcrAdA7List1Sub1.page" )
	public String fdcrAdA7List1Sub1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA7Service.fdcrAdA7List1( commandMap );
		
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		return "email/fdcrAdA7List1Sub1";
	}

	/**
	 * 공고정보 보완내역 팝업
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA7Pop1.page" )
	public String fdcrAdA7Pop1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "MSG_STOR_CD", request.getParameter("MSG_STOR_CD"));
		commandMap.put( "WORKS_ID", "" );
		commandMap.put( "SUPL_ID", request.getParameter("SUPL_ID"));
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "BORD_SEQN", "" );
		fdcrAdA7Service.fdcrAdA7Pop1( commandMap );

		model.addAttribute( "ds_supl_list", commandMap.get( "ds_supl_list" ) );
		System.out.println( "공고정보 보완내역 팝업" );
		return "email/fdcrAdA7Pop1.popup";
	}

}
