package kr.or.copyright.mls.console.legal;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd07Service;
import kr.or.copyright.mls.myStat.service.MyStatService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 법정허락 > 법정허락 신청등록(오프라인)
 * 
 * @author ljh
 */
@Controller
public class FdcrAd07Controller extends DefaultController{

	@Resource( name = "fdcrAd07Service" )
	private FdcrAd07Service fdcrAd07Service;

	@Resource( name = "myStatService" )
	private MyStatService myStatService;

	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;
	
	// AdminCommonSqlMapDao
	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;
	
	// AdminCommonSqlMapDao
	@Resource( name = "fdcrAd06Dao" )
	private FdcrAd06Dao fdcrAd06Dao;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd07Controller.class );

	/**
	 * 법정허락 신청등록(오프라인)등록 폼
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd07RegiForm1.page" )
	public String fdcrAd07RegiForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String searchStr = EgovWebUtil.getString( commandMap, "searchStr" );

		if( !"".equals( searchStr ) ){
			searchStr = URLDecoder.decode( searchStr, "UTF-8" );
			commandMap.put( "searchStr", searchStr );
		}
		model.addAttribute( "commandMap", commandMap );

		String MENU_SEQN = EgovWebUtil.getString( commandMap, "MENU_SEQN" );

		System.out.println( "법정허락 신청등록(오프라인)등록 폼" );
		return "legal/fdcrAd07WriteForm1.tiles";
	}

	/**
	 * 법정허락 신청등록(오프라인)등록
	 * 
	 * @param commandMap
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd07Regi1.page" )
	public String fdcrAd07Regi1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		/*
		String[] WORKS_SEQNS = request.getParameterValues( "WORKS_SEQN" );
		String[] APPLY_TYPE_01S = request.getParameterValues( "APPLY_TYPE_01" );
		String[] APPLY_TYPE_02S = request.getParameterValues( "APPLY_TYPE_02" );
		String[] APPLY_TYPE_03S = request.getParameterValues( "APPLY_TYPE_03" );
		String[] APPLY_TYPE_04S = request.getParameterValues( "APPLY_TYPE_04" );
		String[] APPLY_TYPE_05S = request.getParameterValues( "APPLY_TYPE_05" );
		String[] WORKS_TITLS = request.getParameterValues( "WORKS_TITL" );
		String[] WORKS_KINDS = request.getParameterValues( "WORKS_KIND" );
		String[] WORKS_FORMS = request.getParameterValues( "WORKS_FORM" );
		String[] WORKS_DESCS = request.getParameterValues( "WORKS_DESC" );
		String[] PUBL_YMDS = request.getParameterValues( "PUBL_YMD" );
		String[] PUBL_NATNS = request.getParameterValues( "PUBL_NATN" );
		String[] PUBL_MEDI_CDS = request.getParameterValues( "PUBL_MEDI_CD" );
		String[] PUBL_MEDIS = request.getParameterValues( "PUBL_MEDI" );
		String[] COPT_HODR_NAMES = request.getParameterValues( "COPT_HODR_NAME" );
		String[] COPT_HODR_TELX_NUMBS = request.getParameterValues( "COPT_HODR_TELX_NUMB" );
		String[] COPT_HODR_ADDRS = request.getParameterValues( "COPT_HODR_ADDR" );
		String[] PUBL_MEDI_ETCS = request.getParameterValues( "PUBL_MEDI_ETC" );

		commandMap.put( "WORKS_SEQN", WORKS_SEQNS );
		commandMap.put( "APPLY_TYPE_01", APPLY_TYPE_01S );
		commandMap.put( "APPLY_TYPE_02", APPLY_TYPE_02S );
		commandMap.put( "APPLY_TYPE_03", APPLY_TYPE_03S );
		commandMap.put( "APPLY_TYPE_04", APPLY_TYPE_04S );
		commandMap.put( "APPLY_TYPE_05", APPLY_TYPE_05S );
		commandMap.put( "WORKS_TITL", WORKS_TITLS );
		commandMap.put( "WORKS_KIND", WORKS_KINDS );
		commandMap.put( "WORKS_FORM", WORKS_FORMS );
		commandMap.put( "WORKS_DESC", WORKS_DESCS );
		commandMap.put( "PUBL_YMD", PUBL_YMDS );
		commandMap.put( "PUBL_NATN", PUBL_NATNS );
		commandMap.put( "PUBL_MEDI_CD", PUBL_MEDI_CDS );
		commandMap.put( "PUBL_MEDI", PUBL_MEDIS );
		commandMap.put( "COPT_HODR_NAME", COPT_HODR_NAMES );
		commandMap.put( "COPT_HODR_TELX_NUMB", COPT_HODR_TELX_NUMBS );
		commandMap.put( "COPT_HODR_ADDR", COPT_HODR_ADDRS );
		commandMap.put( "PUBL_MEDI_ETC", PUBL_MEDI_ETCS );
		*/
		
		/*이용 승인신청 정보 Parameter*/
		//String[] MENU_SEQN = request.getParameterValues( "MENU_SEQN" );
		String APPLY_NO = request.getParameter( "APPLY_NO" );
		String RECEIPT_NO = request.getParameter( "RECEIPT_NO" );
		String APPLY_TYPE_01 = request.getParameter( "APPLY_TYPE_01" );
		String APPLY_TYPE_02 = request.getParameter( "APPLY_TYPE_02" );
		String APPLY_TYPE_03 = request.getParameter( "APPLY_TYPE_03" );
		String APPLY_TYPE_04 = request.getParameter( "APPLY_TYPE_04" );
		String APPLY_TYPE_05 = request.getParameter( "APPLY_TYPE_05" );
		String APPLY_WORKS_CNT = request.getParameter( "APPLY_WORKS_CNT" );
		String APPLY_WORKS_TITL = request.getParameter( "APPLY_WORKS_TITL" );
		String APPLY_WORKS_KIND = request.getParameter( "APPLY_WORKS_KIND" );
		String APPLY_WORKS_FORM = request.getParameter( "APPLY_WORKS_FORM" );
		String USEX_DESC = request.getParameter( "USEX_DESC" );
		String APPLY_REAS = request.getParameter( "APPLY_REAS" );
		String CPST_AMNT = request.getParameter( "CPST_AMNT" );
		String APPLR_NAME = request.getParameter( "APPLR_NAME" );
		String APPLR_ADDR = request.getParameter( "APPLR_ADDR" );
		String APPLR_TELX = request.getParameter( "APPLR_TELX" );
		String APPLR_RESD_CORP_NUMB = request.getParameter( "APPLR_RESD_CORP_NUMB" );
		String APPLY_PROXY_NAME = request.getParameter( "APPLY_PROXY_NAME" );
		String APPLY_PROXY_ADDR = request.getParameter( "APPLY_PROXY_ADDR" );
		String APPLY_PROXY_TELX = request.getParameter( "APPLY_PROXY_TELX" );
		String APPLY_PROXY_RESD_CORP_NUMB = request.getParameter( "APPLY_PROXY_RESD_CORP_NUMB" );
		String APPLY_RAW_CD = request.getParameter( "APPLY_RAW_CD" );
		
		//commandMap.put( "MENU_SEQN", MENU_SEQN );
		commandMap.put( "APPLY_NO", APPLY_NO );
		commandMap.put( "RECEIPT_NO", RECEIPT_NO );
		if("".equals( APPLY_TYPE_01 ) || APPLY_TYPE_01 == null){
			commandMap.put( "APPLY_TYPE_01", "N" );
		}else{
			commandMap.put( "APPLY_TYPE_01", APPLY_TYPE_01 );
		}
		if("".equals( APPLY_TYPE_02 ) || APPLY_TYPE_02 == null){
			commandMap.put( "APPLY_TYPE_02", "N" );
		}else{
			commandMap.put( "APPLY_TYPE_02", APPLY_TYPE_02 );
		}
		if("".equals( APPLY_TYPE_03 ) || APPLY_TYPE_03 == null){
			commandMap.put( "APPLY_TYPE_03", "N" );
		}else{
			commandMap.put( "APPLY_TYPE_03", APPLY_TYPE_03 );
		}
		if("".equals( APPLY_TYPE_04 ) || APPLY_TYPE_04 == null){
			commandMap.put( "APPLY_TYPE_04", "N" );
		}else{
			commandMap.put( "APPLY_TYPE_04", APPLY_TYPE_04 );
		}
		if("".equals( APPLY_TYPE_05 ) || APPLY_TYPE_05 == null){
			commandMap.put( "APPLY_TYPE_05", "N" );
		}else{
			commandMap.put( "APPLY_TYPE_05", APPLY_TYPE_05 );
		}
		commandMap.put( "APPLY_WORKS_CNT", APPLY_WORKS_CNT );
		commandMap.put( "APPLY_WORKS_TITL", APPLY_WORKS_TITL );
		commandMap.put( "APPLY_WORKS_KIND", APPLY_WORKS_KIND );
		commandMap.put( "APPLY_WORKS_FORM", APPLY_WORKS_FORM );
		commandMap.put( "USEX_DESC", USEX_DESC );
		commandMap.put( "APPLY_REAS", APPLY_REAS );
		commandMap.put( "CPST_AMNT", CPST_AMNT );
		commandMap.put( "APPLR_NAME", APPLR_NAME );
		commandMap.put( "APPLR_ADDR", APPLR_ADDR );
		commandMap.put( "APPLR_TELX", APPLR_TELX );
		commandMap.put( "APPLR_RESD_CORP_NUMB", APPLR_RESD_CORP_NUMB );
		commandMap.put( "APPLY_PROXY_NAME", APPLY_PROXY_NAME );
		commandMap.put( "APPLY_PROXY_ADDR", APPLY_PROXY_ADDR );
		commandMap.put( "APPLY_PROXY_TELX", APPLY_PROXY_TELX );
		commandMap.put( "APPLY_PROXY_RESD_CORP_NUMB", APPLY_PROXY_RESD_CORP_NUMB );
		commandMap.put( "APPLY_RAW_CD", APPLY_RAW_CD );
		
		/*이용 승인신청 명세서 정보 Parameter*/
		String[] WORKS_SEQN = request.getParameterValues( "WORKS_SEQN" );
		String[] APPLY_TYPE_01_2 = request.getParameterValues( "APPLY_TYPE_01_2" );
		String[] APPLY_TYPE_02_2 = request.getParameterValues( "APPLY_TYPE_02_2" );
		String[] APPLY_TYPE_03_2 = request.getParameterValues( "APPLY_TYPE_03_2" );
		String[] APPLY_TYPE_04_2 = request.getParameterValues( "APPLY_TYPE_04_2" );
		String[] APPLY_TYPE_05_2 = request.getParameterValues( "APPLY_TYPE_05_2" );
		String[] APPLY_WORKS_TITL3 = request.getParameterValues( "APPLY_WORKS_TITL3" );
		String[] APPLY_WORKS_KIND3 = request.getParameterValues( "APPLY_WORKS_KIND3" );
		String[] APPLY_WORKS_FORM3 = request.getParameterValues( "APPLY_WORKS_FORM3" );
		String[] WORKS_DESC = request.getParameterValues( "WORKS_DESC" );
		String[] PUBL_YMD = request.getParameterValues( "PUBL_YMD" );
		String[] PUBL_NATN = request.getParameterValues( "PUBL_NATN" );
		String[] PUBL_MEDI_CD = request.getParameterValues( "PUBL_MEDI_CD" );
		String[] PUBL_MEDI = request.getParameterValues( "PUBL_MEDI" );
		String[] COPT_HODR_NAME = request.getParameterValues( "COPT_HODR_NAME" );
		String[] COPT_HODR_TELX_NUMB = request.getParameterValues( "COPT_HODR_TELX_NUMB" );
		String[] COPT_HODR_ADDR = request.getParameterValues( "COPT_HODR_ADDR" );
		//String[] PUBL_MEDI_ETC = request.getParameterValues( "PUBL_MEDI_ETC" );
		
		commandMap.put( "WORKS_SEQN", WORKS_SEQN );
		if("".equals( APPLY_TYPE_01_2 ) || APPLY_TYPE_01_2 == null){
			commandMap.put( "APPLY_TYPE_01_2", "N" );
		}else{
			commandMap.put( "APPLY_TYPE_01_2", APPLY_TYPE_01_2 );
		}
		if("".equals( APPLY_TYPE_02_2 ) || APPLY_TYPE_02_2 == null){
			commandMap.put( "APPLY_TYPE_02_2", "N" );
		}else{
			commandMap.put( "APPLY_TYPE_02_2", APPLY_TYPE_02_2 );
		}
		if("".equals( APPLY_TYPE_03_2 ) || APPLY_TYPE_03_2 == null){
			commandMap.put( "APPLY_TYPE_03_2", "N" );
		}else{
			commandMap.put( "APPLY_TYPE_03_2", APPLY_TYPE_03_2 );
		}
		if("".equals( APPLY_TYPE_04_2 ) || APPLY_TYPE_04_2 == null){
			commandMap.put( "APPLY_TYPE_04_2", "N" );
		}else{
			commandMap.put( "APPLY_TYPE_04_2", APPLY_TYPE_04_2 );
		}
		if("".equals( APPLY_TYPE_05_2 ) || APPLY_TYPE_05_2 == null){
			commandMap.put( "APPLY_TYPE_05_2", "N" );
		}else{
			commandMap.put( "APPLY_TYPE_05_2", APPLY_TYPE_05_2 );
		}
		/*
		commandMap.put( "APPLY_TYPE_01_2", APPLY_TYPE_01_2 );
		commandMap.put( "APPLY_TYPE_02_2", APPLY_TYPE_02_2 );
		commandMap.put( "APPLY_TYPE_03_2", APPLY_TYPE_03_2 );
		commandMap.put( "APPLY_TYPE_04_2", APPLY_TYPE_04_2 );
		commandMap.put( "APPLY_TYPE_05_2", APPLY_TYPE_05_2 );
		*/
		commandMap.put( "APPLY_WORKS_TITL3", APPLY_WORKS_TITL3 );
		commandMap.put( "APPLY_WORKS_KIND3", APPLY_WORKS_KIND3 );
		commandMap.put( "APPLY_WORKS_FORM3", APPLY_WORKS_FORM3 );
		commandMap.put( "WORKS_DESC", WORKS_DESC );
		commandMap.put( "PUBL_YMD", PUBL_YMD );
		commandMap.put( "PUBL_NATN", PUBL_NATN );
		commandMap.put( "PUBL_MEDI_CD", PUBL_MEDI_CD );
		commandMap.put( "PUBL_MEDI", PUBL_MEDI );
		commandMap.put( "COPT_HODR_NAME", COPT_HODR_NAME );
		commandMap.put( "COPT_HODR_TELX_NUMB", COPT_HODR_TELX_NUMB );
		commandMap.put( "COPT_HODR_ADDR", COPT_HODR_ADDR );
		//commandMap.put( "PUBL_MEDI_ETC", PUBL_MEDI_ETC );
		
		// 파일 업로드
		/*
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );
		boolean isSuccess = fdcrAd07Service.fdcrAd07Regi1( commandMap, fileinfo );
		*/
		boolean isSuccess = fdcrAd07Service.fdcrAd07Insert( commandMap );
		returnAjaxString( response, isSuccess );
		
		if( isSuccess ){
			fileInsert( mreq, commandMap, null );
			return returnUrl(
				model,
				"등록되었습니다.",
				"/console/legal/fdcrAd07RegiForm1.page" );
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/legal/fdcrAd07RegiForm1.page" );
		}
	}
	
	public void fileInsert( MultipartHttpServletRequest mreq,
		Map<String, Object> commandMap,
		String[] extNames ) throws Exception{
		String savePath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> files = fileUpload( mreq,savePath,null,true);
		for( int i = 0; i < files.size(); i++ ){
			Map<String, Object> file = files.get( i );
			Map<String, Object> param = new HashMap<String, Object>();
			int attachSeqn = consoleCommonDao.getNewAttcSeqn();
			//int bordSeqn = fdcrAd01Dao.getMaxBordSeqn();
			
			param.put("ATTC_SEQN", attachSeqn);
			//param.put("FILE_ATTC_CD", EgovWebUtil.getString( file, "FILE_ATTC_CD" ));
			//param.put("FILE_NAME_CD", EgovWebUtil.getString( file, "FILE_NAME_CD" ));
			param.put("FILE_ATTC_CD", "ST");
			param.put("FILE_NAME_CD", "2");
			param.put("FILE_NAME", EgovWebUtil.getString( file, "F_fileName" ) );
			param.put("FILE_PATH", EgovWebUtil.getString( file, "F_saveFilePath" ) );
			param.put("FILE_SIZE", file.get( "F_filesize" ) );
			param.put("REAL_FILE_NAME", EgovWebUtil.getString( file, "F_orgFileName" ) );
			param.put("RGST_IDNT", ConsoleLoginUser.getUserId());
		
			//param.put("APPLY_WRITE_YMD", applyWriteYmd);
			//param.put("APPLY_WRITE_SEQ", applyWriteSeq);
			param.put("APPLY_WRITE_YMD", commandMap.get( "APPLY_WRITE_YMD" ));
			param.put("APPLY_WRITE_SEQ", commandMap.get( "APPLY_WRITE_SEQ" ));
			param.put("MODI_IDNT", param.get("RGST_IDNT"));
			fdcrAd06Dao.adminFileInsert(param);
			fdcrAd06Dao.adminStatAttcFileInsert(param);
		}
	}

	/**
	 * 법정허락 신청등록(오프라인)일괄등록 폼
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd07RegiForm2.page" )
	public String fdcrAd07RegiForm2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		System.out.println( "법정허락 신청등록(오프라인)일괄등록 폼" );
		return "test";
	}

	/**
	 * 법정허락 신청등록(오프라인)일괄등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd07Regi2.page" )
	public void fdcrAd07Regi2( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		String[] APPLR_RESD_CORP_NUMBS = request.getParameterValues( "APPLR_RESD_CORP_NUMB" );
		String[] APPLY_PROXY_RESD_CORP_NUMBS = request.getParameterValues( "APPLY_PROXY_RESD_CORP_NUMB" );
		String[] WORKS_SEQNS = request.getParameterValues( "NO2" );
		String[] RGST_IDNTS = request.getParameterValues( "USER_ID" );

		String[] APPLY_WRITE_YMDS = request.getParameterValues( "APPLY_WRITE_YMD" );
		String[] APPLY_WRITE_SEQS = request.getParameterValues( "APPLY_WRITE_SEQ" );
		String[] APPLY_TYPE_01S = request.getParameterValues( "APPLY_TYPE_01" );
		String[] APPLY_TYPE_02S = request.getParameterValues( "APPLY_TYPE_02" );
		String[] APPLY_TYPE_03S = request.getParameterValues( "APPLY_TYPE_03" );
		String[] APPLY_TYPE_04S = request.getParameterValues( "APPLY_TYPE_04" );
		String[] APPLY_TYPE_05S = request.getParameterValues( "APPLY_TYPE_05" );
		String[] WORKS_TITLS = request.getParameterValues( "WORKS_TITL" );
		String[] WORKS_KINDS = request.getParameterValues( "WORKS_KIND" );
		String[] WORKS_FORMS = request.getParameterValues( "WORKS_FORM" );
		String[] WORKS_DESCS = request.getParameterValues( "WORKS_DESC" );
		String[] PUBL_YMDS = request.getParameterValues( "PUBL_YMD" );
		String[] PUBL_NATNS = request.getParameterValues( "PUBL_NATN" );
		String[] PUBL_MEDI_CDS = request.getParameterValues( "PUBL_MEDI_CD" );
		String[] PUBL_MEDIS = request.getParameterValues( "PUBL_MEDI" );
		String[] COPT_HODR_NAMES = request.getParameterValues( "COPT_HODR_NAME" );
		String[] COPT_HODR_TELX_NUMBS = request.getParameterValues( "COPT_HODR_TELX_NUMB" );
		String[] COPT_HODR_ADDRS = request.getParameterValues( "COPT_HODR_ADDR" );
		String[] PUBL_MEDI_ETCS = request.getParameterValues( "PUBL_MEDI_ETC" );

		commandMap.put( "APPLY_WRITE_YMD", APPLY_WRITE_YMDS );
		commandMap.put( "APPLY_WRITE_SEQ", APPLY_WRITE_SEQS );
		commandMap.put( "WORKS_SEQN", WORKS_SEQNS );
		commandMap.put( "APPLY_TYPE_01", APPLY_TYPE_01S );
		commandMap.put( "APPLY_TYPE_02", APPLY_TYPE_02S );
		commandMap.put( "APPLY_TYPE_03", APPLY_TYPE_03S );
		commandMap.put( "APPLY_TYPE_04", APPLY_TYPE_04S );
		commandMap.put( "APPLY_TYPE_05", APPLY_TYPE_05S );
		commandMap.put( "WORKS_TITL", WORKS_TITLS );
		commandMap.put( "WORKS_KIND", WORKS_KINDS );
		commandMap.put( "WORKS_FORM", WORKS_FORMS );
		commandMap.put( "WORKS_DESC", WORKS_DESCS );
		commandMap.put( "PUBL_YMD", PUBL_YMDS );
		commandMap.put( "PUBL_NATN", PUBL_NATNS );
		commandMap.put( "PUBL_MEDI_CD", PUBL_MEDI_CDS );
		commandMap.put( "PUBL_MEDI", PUBL_MEDIS );
		commandMap.put( "COPT_HODR_NAME", COPT_HODR_NAMES );
		commandMap.put( "COPT_HODR_TELX_NUMB", COPT_HODR_TELX_NUMBS );
		commandMap.put( "COPT_HODR_ADDR", COPT_HODR_ADDRS );
		commandMap.put( "PUBL_MEDI_ETC", PUBL_MEDI_ETCS );

		commandMap.put( "APPLR_RESD_CORP_NUMB", APPLR_RESD_CORP_NUMBS );
		commandMap.put( "APPLY_PROXY_RESD_CORP_NUMB", APPLY_PROXY_RESD_CORP_NUMBS );
		commandMap.put( "RGST_IDNT", RGST_IDNTS );

		boolean isSuccess = fdcrAd07Service.fdcrAd07Regi2( commandMap );
		returnAjaxString( response, isSuccess );
	}

	/**
	 * 법정허락신청 일괄등록 엑셀양식 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd07Download1.page" )
	public void fdcrAd07Download1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "FILE_URL", "" );
		String filePath = "";
		String fileName = "";

		fileName = "01.이용승인신청정보 입력양식.xlsx";
		filePath = "/home/right4me/web/upload/form/01.이용승인신청정보 입력양식.xlsx";

		if( null != fileName && !"".equals( fileName ) ){
			download( request, response, filePath, fileName );
		}
	}

	/**
	 * 법정허락신청 일괄등록 엑셀양식 업로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd07Upload1.page" )
	public void fdcrAd07Upload1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );
		// TODO 엑셀 조회

		// boolean isSuccess = fdcrAd07Service.fdcrAd07Upload1( fileinfo );
		// returnAjaxString( response, isSuccess );
	}

}
