package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;


public interface FdcrAd88Service{
	/**
	 * 일별 접속 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd88List1( Map<String, Object> commandMap ) throws Exception;
}
