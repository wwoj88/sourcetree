package kr.or.copyright.mls.console.event;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 이벤트 등록 및 관리 > Q&A 게시판 운영관리
 * 
 * @author ljh
 */
@Controller
public class FdcrAdA2Controller extends DefaultController{

	@Resource( name = "fdcrAdA2Service" )
	private FdcrAdA2ServiceImpl fdcrAdA2Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA2Controller.class );

	/**
	 * 게시물 질문조회 목록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA2List1.page" )
	public String fdcrAdA2List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA2Service.fdcrAdA2List1( commandMap );

		model.addAttribute( "ds_List", commandMap.get( "ds_List" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		System.out.println( "게시물 질문조회 목록" );
		return "test";
	}

	/**
	 * 게시판 사용여부 체크
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA2List2.page" )
	public void fdcrAdA2List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA2Service.fdcrAdA2List2( commandMap );

		model.addAttribute( "ds_condition", commandMap.get( "ds_condition" ) );
		System.out.println( "게시판 사용유무 체크" );

	}

	/**
	 * 게시판 사용여부 수정
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA2Update1.page" )
	public void fdcrAdA2Update1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "MENUSEQN", "" );
		commandMap.put( "OPEN_YSNO", "" );

		boolean isSuccess = fdcrAdA2Service.fdcrAdA2Update1( commandMap );
		returnAjaxString( response, isSuccess );
	}

	/**
	 * 게시물 상세조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA2View1.page" )
	public String fdcrAdA2View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "BORD_SEQN", "" );
		commandMap.put( "MENU_SEQN", "" );
		commandMap.put( "THREADED", "" );

		fdcrAdA2Service.fdcrAdA2View1( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "게시물 상세조회" );
		return "test";
	}

	/**
	 * 게시물 삭제
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA2Delete1.page" )
	public void fdcrAdA2Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "BORD_SEQN", "" );
		commandMap.put( "MENU_SEQN", "" );
		commandMap.put( "THREADED", "" );

		boolean isSuccess = fdcrAdA2Service.fdcrAdA2Delete1( commandMap );
		returnAjaxString( response, isSuccess );
	}

	/**
	 * 게시물 답변 등록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA2Regi1.page" )
	public void fdcrAdA2Regi1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "BORD_SEQN", "" );
		commandMap.put( "MENU_SEQN", "" );
		commandMap.put( "THREADED", "" );

		boolean isSuccess = fdcrAdA2Service.fdcrAdA2Regi1( commandMap );
		returnAjaxString( response, isSuccess );
	}

}
