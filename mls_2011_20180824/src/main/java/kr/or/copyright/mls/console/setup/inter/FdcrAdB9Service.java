package kr.or.copyright.mls.console.setup.inter;

import java.util.Map;

public interface FdcrAdB9Service{

	/**
	 * 신청 담당자 메일수신관리 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB9List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 신청기능구분 코드목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB9List2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 메일 신청 담당자 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdB9Delete1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 중복확인
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB9List3( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 메일 신청 담당자 추가
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdB9Insert1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 담당자 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB9List4( Map<String, Object> commandMap ) throws Exception;
}
