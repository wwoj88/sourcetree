package kr.or.copyright.mls.adminEvnSet.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.or.copyright.mls.adminEmailMgnt.service.AdminEmailMgntService;
import kr.or.copyright.mls.adminEmailMgnt.service.AdminEmailMgntServiceImpl;
import kr.or.copyright.mls.adminEvnSet.dao.AdminEvnSetDao;
import kr.or.copyright.mls.common.mail.SendMail;
import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.util.StringUtil;

import com.tobesoft.platform.data.Dataset;

@Service("AdminEvnSetService")
public class AdminEvnSetServiceImpl extends BaseService implements AdminEvnSetService {
	@Autowired
	private AdminEvnSetDao adminEvnSetDao;
	
	public void setAdminEvnSetDao(AdminEvnSetDao adminEvnSetDao){
		this.adminEvnSetDao = adminEvnSetDao;
	}
	@javax.annotation.Resource(name = "AdminEmailMgntService")
	private AdminEmailMgntService adminEmailMgntService;	
	public void setAdminEmailMgntService(
			AdminEmailMgntService adminEmailMgntService) {
		this.adminEmailMgntService = adminEmailMgntService;
	}
	  

	//신청 담당자 메일수신관리
	public void selectMailMgntList() throws Exception{
		
		
		Dataset ds_menu = getDataset("ds_menu1");		
		Map map = getMap(ds_menu, 0);
		
		// DAO 호출
		List list = (List) adminEvnSetDao.selectMailMgntList(map);
		
		addList("ds_list", list);
	}
	
	// 그룹, 그룹메뉴 삭제하기
	public void deleteMailMgntList() throws Exception{
		
	    	Dataset ds_list = getDataset("ds_list");
	    	Dataset ds_regi = getDataset("ds_regi");	    	
	    	
	    	Map regiMap = getMap(ds_regi, 0);	    
	    	
	    	for(int i=0; i<ds_list.getRowCount(); i++){
	    	    Map dsMap = getMap(ds_list, i);
	    	    dsMap.put("MGNT_CODE", regiMap.get("MGNT_CODE"));
	    	    
	    	    if(dsMap.get("CHK").equals("1")){ 
	    	    	adminEvnSetDao.deleteMailMgntList(dsMap);
	    	    }
	    	}
	    	
	    	//List list = (List) adminStatProcDao.selectMailMgntList();
		
		//addList("ds_list", list);
	}
	
	public void insertMailMgntList() throws Exception{
	    Dataset ds_regi = getDataset("ds_regi");
	    
	    Map dsMap = getMap(ds_regi, 0);
	    
	    adminEvnSetDao.insertMailMgntList(dsMap);
	}
	
	public void isCheckMailMgntList() throws Exception{
		
		Dataset ds_list = getDataset("ds_regi");     
		Map map = getMap(ds_list, 0);
		
		// 관리자ID체크
		int iUserCnt = adminEvnSetDao.isCheckMailMgntList(map); 
		HashMap h = new HashMap();
		h.put("ID_CHECK_CNT", iUserCnt);
		
		// hmap를 dataset로 변환한다.
		addList("ds_condition", h);
		
	}

	public void SelectMailMgntCodeList() throws Exception{
		
		Dataset ds_menu = getDataset("ds_menu1");
		
		Map map = getMap(ds_menu, 0);
		
		List list = (List) adminEvnSetDao.SelectMailMgntCodeList(map);
				
		// hmap를 dataset로 변환한다.
		addList("ds_gubun", list);
		
		
	}
	
	
	// 상당한노력 자동화 설정 조회
	public void statProcSetList() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
	    Map map = getMap(ds_condition, 0);
	    
	    List list = (List) adminEvnSetDao.statProcSetList(map);
	    
	    addList("ds_list_stat", list);
	}
	
	// 상당한노력 자동화 수정
	public void statProcSetUpdate() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		adminEvnSetDao.statProcSetUpdateYn(map);
	}
	
	
	
	// 저작물보고 관리자 메일발송 스케쥴링(위탁관리저작물, 미분배보상금)
	public void WorksMgntMailScheduling() throws Exception{		
		try {
			int count = 0;
			
			count = adminEvnSetDao.isCheckMailCommWorksInfo();
			
			if(count!=0){				
				String mgntDivs = "REPORT_WORKS";
				Map map=new HashMap();
				
				map.put("BIG_CODE", "85");			
				map.put("MGNT_DIVS", mgntDivs);
				//map.put("MGNT_DIVS", "REPORT_INMT");
				
				List recvList= (List) adminEvnSetDao.selectMailMgntList(map);
				List<Map> mailList =(List) adminEvnSetDao.selectMailCommWorksInfo();	
				
				
				String to[]=new String[recvList.size()];
				String toName[] = new String[recvList.size()];
				
				String commName[]=new String[mailList.size()];
				String rgstDttm[]=new String[mailList.size()];
				String genre[]=new String[mailList.size()];
				String sumAllCount[]=new String[mailList.size()];

				for(int i=0;i<recvList.size();i++){
					Map recvMap=(Map)recvList.get(i);
					toName[i]=(String)recvMap.get("USER_NAME_IDNT");
					to[i] = (String) recvMap.get("MAIL");
				
				}
				
				for(int i=0;i<mailList.size();i++){
					Map tmpMailMap=mailList.get(i);
					commName[i]=(String)tmpMailMap.get("COMM_NAME");			
					rgstDttm[i]=(String)tmpMailMap.get("RGST_DTTM");
					genre[i]=(String)tmpMailMap.get("GENRE");
					sumAllCount[i]=(String)tmpMailMap.get("SUM_ALL_COUNT");
				}
			
				String mailTitl="위탁관리저작물 보고";
				Map mailMap=new HashMap();
				mailMap.put("MGNT_DIVS", mgntDivs);
				mailMap.put("MAIL_TITL", mailTitl);
				mailMap.put("TO", to);
				mailMap.put("TO_NAME", toName);
				mailMap.put("COMM_NAME", commName);
				mailMap.put("RGST_DTTM", rgstDttm);
				mailMap.put("GENRE", genre);
				mailMap.put("SUM_ALL_COUNT", sumAllCount);					
	
				SendMail.send(mailMap);
					
			}
			
			
			count = adminEvnSetDao.isCheckMailInmtWorksInfo();
			
			if(count!=0){
				
				Map map=new HashMap();

				String mgntDivs = "REPORT_INMT";			
			
				map.put("BIG_CODE", "85");			
				map.put("MGNT_DIVS", mgntDivs);
				
				List recvList= (List) adminEvnSetDao.selectMailMgntList(map);
				List<Map> mailList =(List) adminEvnSetDao.selectMailInmtWorksInfo();
				
				
				String to[]=new String[recvList.size()];
				String toName[] = new String[recvList.size()];
				
				String commName[]=new String[mailList.size()];
				String rgstDttm[]=new String[mailList.size()];
				String inmt[]=new String[mailList.size()];
				String sumAllCount[]=new String[mailList.size()];

				for(int i=0;i<recvList.size();i++){
					Map recvMap=(Map)recvList.get(i);
					Map sendMap=new HashMap();					
					toName[i]=(String)recvMap.get("USER_NAME_IDNT");
					to[i] = (String) recvMap.get("MAIL");
				
				}
				
				for(int i=0;i<mailList.size();i++){
					Map tmpMailMap=mailList.get(i);
					commName[i]=(String)tmpMailMap.get("COMM_NAME");			
					rgstDttm[i]=(String)tmpMailMap.get("RGST_DTTM");
					inmt[i]=(String)tmpMailMap.get("INMT");
					sumAllCount[i]=(String)tmpMailMap.get("SUM_ALL_COUNT");
				}
			
				String mailTitl="미분배보상금 보고";
				Map mailMap=new HashMap();
				mailMap.put("MGNT_DIVS", mgntDivs);
				mailMap.put("MAIL_TITL", mailTitl);
				mailMap.put("TO", to);
				mailMap.put("TO_NAME", toName);
				mailMap.put("COMM_NAME", commName);
				mailMap.put("RGST_DTTM", rgstDttm);
				mailMap.put("INMT", inmt);
				mailMap.put("SUM_ALL_COUNT", sumAllCount);
				
				SendMail.send(mailMap);
					
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	
	/*	public void WorksMgntMailScheduling() throws Exception{		
			try {
				int count = 0;
				
				count = adminEvnSetDao.isCheckMailCommWorksInfo();
				
				if(count!=0){				
					
					Map map=new HashMap();
					
					map.put("BIG_CODE", "85");			
					map.put("MGNT_DIVS", "REPORT_WORKS");
					//map.put("MGNT_DIVS", "REPORT_INMT");
					
					List recvList= (List) adminEvnSetDao.selectMailMgntList(map);
					List<Map> mailList =(List) adminEvnSetDao.selectMailCommWorksInfo();			
					if(mailList.size()!=0){
						Map mailMap=new HashMap();
						List<Map> sendList = new ArrayList<Map>();
						
						String mailTitl="위탁관리저작물 보고";
						
						StringBuffer subSb = new StringBuffer();
						subSb.append("						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">    \n");
						subSb.append("							<thead>    \n");
						subSb.append("								<tr>    \n");
						subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">기관명</th>    \n");
						subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">보고일자</th>    \n");	
						subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"20%\">저작물분류</th>    \n");
						subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">건수</th>    \n");
						subSb.append("								</tr>    \n");
						subSb.append("							</thead>    \n");
						subSb.append("							<tbody>    \n");
						for(int i=0;i<mailList.size();i++){
							Map tmpMailMap=mailList.get(i);
							String commName=(String)tmpMailMap.get("COMM_NAME");			
							String rgstDttm=(String)tmpMailMap.get("RGST_DTTM");
							String genre=(String)tmpMailMap.get("GENRE");
							String sumAllCount=(String)tmpMailMap.get("SUM_ALL_COUNT");
					
							subSb.append("							<tr>    \n");		
							subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+commName+"</td>    \n");
							subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+rgstDttm+"</td>    \n");
							subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+genre+"</td>    \n");
							subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+sumAllCount+"</td>    \n");
							subSb.append("							</tr>    \n");			
												
						}			
						subSb.append("						</tbody>    \n");
						subSb.append("					</table>    \n");
						
						StringBuffer sb = new StringBuffer();
						sb.append("				<tr>    \n");
						sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">    \n");
						sb.append("						<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">"+mailTitl+" 정보</p>    \n");
						sb.append( subSb.toString() );			
						sb.append("					</td>    \n");
						sb.append("				</tr>    \n");
						String contentHtml=new String(sb);
						
						String mailTitle="["+(String)mailList.get(0).get("RGST_DTTM")+"]"+mailTitl+" 현황안내";
						mailMap.put("TITLE", mailTitle);
						mailMap.put("CONTENT_HTML", contentHtml);//메일 내용
						mailMap.put("MSG_STOR_CD", "2");//발신함코드
						
						for(int i=0;i<recvList.size();i++){
							Map recvMap=(Map)recvList.get(i);
							Map sendMap=new HashMap();
							
							sendMap.put("RECV_CD","1");//참조 없음
							sendMap.put("RECV_MAIL_ADDR",recvMap.get("MAIL"));
							sendMap.put("RECV_NAME",recvMap.get("USER_NAME_IDNT"));
							
							sendList.add(sendMap);
							
						}
						
		
						adminEmailMgntService.sendEmail(sendList, mailMap);
						
					}
				}
				
				count = adminEvnSetDao.isCheckMailInmtWorksInfo();
				
				if(count!=0){
					
					Map map=new HashMap();
					
					map.put("BIG_CODE", "85");			
					map.put("MGNT_DIVS", "REPORT_INMT");
					
					List recvList= (List) adminEvnSetDao.selectMailMgntList(map);
					List<Map> mailList =(List) adminEvnSetDao.selectMailInmtWorksInfo();			
					if(mailList.size()!=0){
						Map mailMap=new HashMap();
						List<Map> sendList = new ArrayList<Map>();
						
						String mailTitl="미분배보상금 보고";
						
						StringBuffer subSb = new StringBuffer();
						subSb.append("						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">    \n");
						subSb.append("							<thead>    \n");
						subSb.append("								<tr>    \n");
						subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">기관명</th>    \n");
						subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">보고일자</th>    \n");	
						subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"20%\">보상금분류</th>    \n");
						subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">건수</th>    \n");
						subSb.append("								</tr>    \n");
						subSb.append("							</thead>    \n");
						subSb.append("							<tbody>    \n");
						for(int i=0;i<mailList.size();i++){
							Map tmpMailMap=mailList.get(i);
							String commName=(String)tmpMailMap.get("COMM_NAME");			
							String rgstDttm=(String)tmpMailMap.get("RGST_DTTM");
							String inmt=(String)tmpMailMap.get("INMT");
							String sumAllCount=(String)tmpMailMap.get("SUM_ALL_COUNT");
					
							subSb.append("							<tr>    \n");		
							subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+commName+"</td>    \n");
							subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+rgstDttm+"</td>    \n");
							subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+inmt+"</td>    \n");
							subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+sumAllCount+"</td>    \n");
							subSb.append("							</tr>    \n");			
												
						}			
						subSb.append("						</tbody>    \n");
						subSb.append("					</table>    \n");
						
						StringBuffer sb = new StringBuffer();
						sb.append("				<tr>    \n");
						sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">    \n");
						sb.append("						<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">"+mailTitl+" 정보</p>    \n");
						sb.append( subSb.toString() );			
						sb.append("					</td>    \n");
						sb.append("				</tr>    \n");
						String contentHtml=new String(sb);
						
						String mailTitle="["+(String)mailList.get(0).get("RGST_DTTM")+"]"+mailTitl+" 현황안내";
						mailMap.put("TITLE", mailTitle);
						mailMap.put("CONTENT_HTML", contentHtml);//메일 내용
						mailMap.put("MSG_STOR_CD", "2");//발신함코드
						
						for(int i=0;i<recvList.size();i++){
							Map recvMap=(Map)recvList.get(i);
							Map sendMap=new HashMap();
							
							sendMap.put("RECV_CD","1");//참조 없음
							sendMap.put("RECV_MAIL_ADDR",recvMap.get("MAIL"));
							sendMap.put("RECV_NAME",recvMap.get("USER_NAME_IDNT"));
							
							sendList.add(sendMap);
							
						}
						
		
						adminEmailMgntService.sendEmail(sendList, mailMap);
						
					}
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
					
			
		}*/

	
	
	
}
