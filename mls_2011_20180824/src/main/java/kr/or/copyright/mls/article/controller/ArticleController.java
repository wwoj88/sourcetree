package kr.or.copyright.mls.article.controller;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.validation.Valid;

import kr.or.copyright.mls.article.model.Article;
import kr.or.copyright.mls.article.service.ArticleService;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class ArticleController implements InitializingBean {
    private static final Log LOG = LogFactory.getLog(ArticleController.class);
    
    @javax.annotation.Resource(name="ArticleBO")
    private ArticleService articleBO;
    
    @Autowired
    private MessageSourceAccessor message;

    @Value("file:${uploadPath}")
    private Resource uploadPath;
    

    
    @RequestMapping(value = "/article/form.do")
    public ModelAndView writeForm(
            @RequestParam(value = "communityId", required = true, defaultValue = "1") int communityId) {
        ModelAndView mv = new ModelAndView();

        mv.setViewName("article/ArticleWriteForm");
        mv.addObject("communityId", communityId);

        return mv;
    }

    
    @RequestMapping(value = "/article/write.do", method = RequestMethod.POST)
    public ModelAndView write(@Valid Article articleVO, BindingResult result){
    	
    	ModelAndView mv = new ModelAndView();
    	
    	int articleId = 0;
    	
    	if(result.hasErrors()){
    		List<ObjectError> list = result.getAllErrors();
    		for(ObjectError e: list){
    			LOG.error("ObjectError : "+e);
    		}
    		
    		mv.setViewName("article/ArticleWriteForm");
    		mv.addObject("articleVO",articleVO);
    		
    		return mv;
    	}
    	
    	LOG.debug("### articleVO : "+ articleVO);
    	
    	//writeFile(articleVO.getFilename());
    	articleVO.setReLevel(0);
        articleVO.setReDepth(0);
        
        articleId = articleBO.writeArticle(articleVO);
    	
        LOG.debug(" #### articleId : " + articleId);

        mv.setViewName("redirect:/article/list.do?communityId=" + articleVO.getCommunityId());
        mv.addObject("article",articleVO);

    	return mv; 
    }
    
    @RequestMapping(value = "/article/remove.do")
    public ModelAndView remove(@ModelAttribute(value = "articleVO") Article article) {

        articleBO.removeArticle(article.getArticleId());

        ModelAndView mv = new ModelAndView();
        mv.setViewName("article/ArticleList");
        mv.addObject("articleList", articleBO.getArticleList(article.getCommunityId()));
        mv.addObject("communityId", article.getCommunityId());
        mv.addObject("msg", message.getMessage("article.remove.success"));

        return mv;
    }
    
    @RequestMapping(value = "/article/view.do", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam(value = "articleId", required = true, defaultValue = "1") int articleId) {

        Article articleVO = articleBO.getArticleInfo(articleId);
        ModelAndView mv = new ModelAndView();

        mv.setViewName("article/ArticleViewForm");
        mv.addObject("article", articleVO);

        return mv;
    }


    
    private void writeFile(CommonsMultipartFile[] multipartFiles){
    	OutputStream out = null;
    	
    	try{
    		for(CommonsMultipartFile multipartFile : multipartFiles){
    			out = new FileOutputStream(uploadPath.getFile().getAbsolutePath() + "/"+multipartFile.getOriginalFilename());
    			BufferedInputStream bis = new BufferedInputStream(multipartFile.getInputStream());
    			byte[] buffer = new byte[8106];
    			int read;
    			
    			while((read = bis.read(buffer)) > 0){
    				out.write(buffer,0,read);
    			}
    			IOUtils.closeQuietly(out);
    		}
    	}catch(IOException ioe){
    		LOG.error(ioe);
    	}finally{
    		IOUtils.closeQuietly(out);
    	}
    }

	public void afterPropertiesSet() throws Exception {
		Assert.notNull(uploadPath,"FileUpload Path must be defined!");
		//LOG.debug(" ######### uploadPath : " + uploadPath.getFile().getAbsolutePath());
        if (!uploadPath.getFile().exists()) {
            uploadPath.getFile().mkdirs();
        }
	}
	
	private List<Article> articleList;
	
	@RequestMapping(value = "/article/list.do")
    public ModelAndView list(@RequestParam(value = "communityId", required = true, defaultValue = "1") int communityId) {
		
        articleList = articleBO.getArticleList(communityId);
        ModelAndView mv = new ModelAndView();

        mv.setViewName("article/ArticleList");
        mv.addObject("articleList", articleList);
        mv.addObject("communityId", communityId);

        return mv;
    }

}
