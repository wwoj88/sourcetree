package kr.or.copyright.mls.console.effort;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd04Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd04Service" )
public class FdcrAd04ServiceImpl extends CommandService implements FdcrAd04Service{

	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	/**
	 * 이의 신청 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd04List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List pageList = (List) fdcrAd01Dao.objeRowList( commandMap ); // 페이징 카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		
		List list = (List) fdcrAd01Dao.objeList( commandMap );
		
		// List pageCount = (List) adminStatBoardDao.objeRowCount(map);
		
		HashMap countMap1 = (HashMap) pageList.get( 0 );
		String resultCnt = countMap1.get( "COUNT" ).toString();// 구분값 1 카운트

		HashMap countMap = new HashMap();
		countMap.put( "COUNT", resultCnt );

		List pageCount = new ArrayList();
		pageCount.add( countMap );

		HashMap aaa = (HashMap) pageCount.get( 0 );
		System.out.println( aaa.get( "COUNT" ) );
		for( int i = 2; i < 4; i++ ){
			commandMap.put( "SCH_STAT_OBJC_CD", Integer.toString( i ) );
			List innerPageList = (List) fdcrAd01Dao.objeRowList( commandMap ); // 페이징
																				// 카운트

			countMap1 = (HashMap) innerPageList.get( 0 );
			resultCnt = countMap1.get( "COUNT" ).toString();// 구분값 1 카운트

			countMap = new HashMap();
			countMap.put( "COUNT", resultCnt );

			pageCount.add( countMap );

			int j = i - 1;
			aaa = (HashMap) pageCount.get( j );
			System.out.println( aaa.get( "COUNT" ) );
		}

		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageList );
		commandMap.put( "ds_count", pageCount );
	}

	/**
	 * 상당한노력 상세
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd04UpdateForm1( Map<String, Object> commandMap ) throws Exception{

		List detailList = (List) fdcrAd01Dao.statBord01Detail( commandMap );
		List fileList = (List) fdcrAd01Dao.statBord01File( commandMap );
		List objectList = (List) fdcrAd01Dao.statBord01Object( commandMap );
		List objectFileList = (List) fdcrAd01Dao.statBord01ObjectFile( commandMap );
		// 보완요청 LIST
		List suplList = (List) fdcrAd01Dao.selectBordSuplItemList( commandMap );

		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_list", detailList );
		commandMap.put( "ds_file", fileList );
		commandMap.put( "ds_object", objectList );
		commandMap.put( "ds_object_file", objectFileList );
		commandMap.put( "ds_supl_list", suplList );

	}

	/**
	 * 법정허락 대상 저작물 - 상세
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd04UpdateForm2( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd01Dao.statBord05Detail( commandMap );
		List objectList = (List) fdcrAd01Dao.statBord05Object( commandMap );
		List objectFileList = (List) fdcrAd01Dao.statBord05ObjectFile( commandMap );

		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_object", objectList );
		commandMap.put( "ds_object_file", objectFileList );
	}

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 처리상태 수정(신청,보완,신청승인,반려)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd04Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBordStatUpdate( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 보상금 공탁 공고 게시판 - 공고승인
	 * 
	 * @throws Exception
	 */
	public boolean fdcrAd04Update2( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBord03Mgnt( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd04Update3( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBordStatDeltUpdate( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}
}
