package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;

public interface FdcrAd95Service{

	/**
	 * 법정허락 신청 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인 건수 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 대상 저작물 현황
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List3( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁서 공고 건수 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List4( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한노력 신청 건수 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List5( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한 노력 신청 유형별 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List6( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 이용승인 명세서 목록(팝업)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd95List7( Map<String, Object> commandMap ) throws Exception;

}
