package kr.or.copyright.mls.adminEventMgnt.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public interface AdminEventMgntDao {
	public String getSaveEventSeq();
	public void addEvent(Map map);
	public void addEventAgree(Map map) throws Exception;
	public List getEventList();
	public void delEventList(Map map);
	public List getEventDetl(Map map);//상세처음페이지
	public List getEventDetlAgree(Map map);//상세처음페이지
	public void updateEvent(Map mData);//수정
	public void delEventAgree(Map mData);
	public Map getCommItem(Map mData);
	public void addCommItem(Map mData);
	public void uptCommItem(Map mData);
	public Map getEventPart(Map mData);
	public List getEventPartList(Map mData);
	public List getEventPartRsltList(Map mData);
	public void uptWinY(ArrayList<Map> list) throws Exception;
	public void uptWinN(ArrayList<Map> list) throws Exception;
	public List getSelRandomWinning(Map mData);
	public List getEventStatList() throws Exception;
	public List getEventQnaList(Map map) throws Exception;
	public List getEventQnaTotal(Map map) throws Exception;
	public List getEventQnaDetail(Map map) throws Exception;
	public void delEventQnaDetail(Map map) throws Exception;
	public void uptEventQnaAnswRegi(Map map) throws Exception;
	public void delEventQnaMenuOpen(Map map) throws Exception;
	public void addEventQnaMenuOpen(Map map) throws Exception;
	public List getEventQnaMenuOpen() throws Exception;
}
