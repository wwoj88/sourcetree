package kr.or.copyright.mls.common.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

//import java.io.UnsupportedEncodingException;

import kr.or.copyright.mls.support.util.StringUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileUtil {

	private final static Log logger = LogFactory.getLog(FileUtil.class);
	
	public static String read(String file) throws Exception {

		BufferedReader in = null;
		StringBuffer buffer = new StringBuffer();
		String str = null;

		try {
			in = new BufferedReader(new FileReader(file));

			while ((str = in.readLine()) != null) {
				buffer.append(str).append('\n');
			}

		} finally {
			if (in != null) {
				in.close();
			}

		}

		return buffer.toString();
	}

	public static Properties getProperties(String file) throws Exception {

		return getProperties(file, false);
	}

	public static Properties getProperties(String file, boolean addToSystemProps)
			throws Exception {

		InputStream is = null;
		FileInputStream fis = null;
		Properties props = null;

		try {
			// fis = new FileInputStream(file);
			is = FileUtil.class.getClassLoader().getResourceAsStream(file);

			props = (addToSystemProps) ? new Properties(System.getProperties())
					: new Properties();
			props.load(is);
			// props.load(fis);

		} finally {

			if (fis != null) {
				fis.close();
			}
		}

		return props;
	}

	public static Map uploadMiFile(String fileName, byte[] file) throws Exception {

//		UUID uuid = null;
		String uuidFileName = null;
		
		FileOutputStream s = null;
		ByteArrayInputStream is = null;
		
		//String uploadPath = "/home/tmax/mls/web/upload/";  // 실서버
//		String uploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버
		//String uploadPath = "D:/temp/";  // 로컬용
//		String uploadPath = "D:/server/mls/mls_2010/upload/";  // 로컬용
		String uploadPath = uploadPath();
		
		byte[] in = null;
		
		int len = 0;  
		int offset = 0;
		
		try{
//			uuid = UUID.randomUUID();
			
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat formatter2=new SimpleDateFormat("yyyyMMddhhmmss",Locale.KOREA);
			String period = formatter2.format(cal.getTime());
			
			StringUtil stringUtil = new StringUtil();
			int inputNum[] = stringUtil.getRandomNum(4);
			String inputNumFull = "" + inputNum[0] + inputNum[1] + inputNum[2] + inputNum[3];
			
//			uuidFileName = uuid + "." + getFileExt(fileName);
			uuidFileName = period + inputNumFull + "." + getFileExt(fileName);
	
//			String ext = getFileExt(fileName);
			logger.debug("uuidFileName : " + uuidFileName);
			
			//Properties props = getProperties("config.properties");
			//uploadPath = props.getProperty("upload.path");
			
			is = new ByteArrayInputStream(file);
			
			s = new FileOutputStream(uploadPath + uuidFileName);
			
			in = new byte[(int)file.length];
			
System.out.println("uploadPath + uuidFileName===>"+uploadPath + uuidFileName);			
			
			while ((len = is.read(in, offset, in.length)) != -1 );
			len = is.read(in, offset, in.length);
			
		}finally{
			
			s.write(in);			//서버로 파일 write
			is.close();
			s.close();
		}
		
		logger.debug("uploadPath : " + uploadPath);
		
		Map map = new HashMap();
		map.put("FILE_PATH", uploadPath);
		map.put("REAL_FILE_NAME", uuidFileName);
		
		return map;

	}
	
	public static Map uploadMiFile2(String fileName, byte[] file) throws Exception {

//		UUID uuid = null;
		String uuidFileName = null;
		
		FileOutputStream s = null;
		ByteArrayInputStream is = null;
		
		//String uploadPath = "/home/tmax/mls/web/upload/";  // 실서버
//		String uploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버
		//String uploadPath = "D:/temp/";  // 로컬용
//		String uploadPath = "D:/server/mls/mls_2010/upload/";  // 로컬용
		String uploadPath = uploadPath2();
		
		byte[] in = null;
		
		int len = 0;  
		int offset = 0;
		
		try{
//			uuid = UUID.randomUUID();
			
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat formatter2=new SimpleDateFormat("yyyyMMddhhmmss",Locale.KOREA);
			String period = formatter2.format(cal.getTime());
			
			StringUtil stringUtil = new StringUtil();
			int inputNum[] = stringUtil.getRandomNum(4);
			String inputNumFull = "" + inputNum[0] + inputNum[1] + inputNum[2] + inputNum[3];
			
//			uuidFileName = uuid + "." + getFileExt(fileName);
			uuidFileName = period + inputNumFull + "." + getFileExt(fileName);
	
//			String ext = getFileExt(fileName);
			logger.debug("uuidFileName : " + uuidFileName);
			
			//Properties props = getProperties("config.properties");
			//uploadPath = props.getProperty("upload.path");
			
			is = new ByteArrayInputStream(file);
			
			s = new FileOutputStream(uploadPath + uuidFileName);
			
			in = new byte[(int)file.length];
			
System.out.println("uploadPath + uuidFileName===>"+uploadPath + uuidFileName);			
			
			while ((len = is.read(in, offset, in.length)) != -1 );
			len = is.read(in, offset, in.length);
			
		}finally{
			
			s.write(in);			//서버로 파일 write
			is.close();
			s.close();
		}
		
		logger.debug("uploadPath : " + uploadPath);
		
		Map map = new HashMap();
		map.put("FILE_PATH", uploadPath);
		map.put("REAL_FILE_NAME", uuidFileName);
		
		return map;

	}
	
	/**
	 * plusDir 추가 경로 ex) event/
	 * @param fileName
	 * @param file
	 * @param plusDir
	 * @return
	 * @throws Exception
	 */
	public static Map uploadMiFile(String fileName, byte[] file, String plusDir) throws Exception {

//		UUID uuid = null;
		String uuidFileName = null;
		
		FileOutputStream s = null;
		ByteArrayInputStream is = null;
		
		//String uploadPath = "/home/tmax/mls/web/upload/";  // 실서버
//		String uploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버
		//String uploadPath = "D:/temp/";  // 로컬용
//		String uploadPath = "D:/server/mls/mls_2010/upload/";  // 로컬용
		String uploadPath = uploadPath()+plusDir;
		
		byte[] in = null;
		
		int len = 0;  
		int offset = 0;
		
		try{
//			uuid = UUID.randomUUID();
			
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat formatter2=new SimpleDateFormat("yyyyMMddhhmmss",Locale.KOREA);
			String period = formatter2.format(cal.getTime());
			
			StringUtil stringUtil = new StringUtil();
			int inputNum[] = stringUtil.getRandomNum(4);
			String inputNumFull = "" + inputNum[0] + inputNum[1] + inputNum[2] + inputNum[3];
			
//			uuidFileName = uuid + "." + getFileExt(fileName);
			uuidFileName = period + inputNumFull + "." + getFileExt(fileName);
	
//			String ext = getFileExt(fileName);
			logger.debug("uuidFileName : " + uuidFileName);
			
			//Properties props = getProperties("config.properties");
			//uploadPath = props.getProperty("upload.path");
			
			is = new ByteArrayInputStream(file);
			
			s = new FileOutputStream(uploadPath + uuidFileName);
			
			in = new byte[(int)file.length];
			
System.out.println("uploadPath + uuidFileName===>"+uploadPath + uuidFileName);			
			
			while ((len = is.read(in, offset, in.length)) != -1 );
			len = is.read(in, offset, in.length);
			
		}finally{
			
			s.write(in);			//서버로 파일 write
			is.close();
			s.close();
		}
		
		logger.debug("uploadPath : " + uploadPath);
		
		Map map = new HashMap();
		map.put("FILE_PATH", uploadPath);
		map.put("REAL_FILE_NAME", uuidFileName);
		
		return map;

	}

	public static String getFileExt(String fileName) {

		return StringUtils.substringAfterLast(fileName, ".");
	}

	public static Map uploadPromMiFile(String fileName, byte[] file) throws Exception {

//		UUID uuid = null;
		String uuidFileName = null;
		
		FileOutputStream s = null;
		ByteArrayInputStream is = null;
		
		
		//String uploadPath = "D:/Server/mls/mls_2010/web/promUpload/";  // 로컬용
		//String uploadPath = "D:/01.project/04.mls_new/web/promUpload/";  // 이관서버용
		
		String uploadPath =  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path_prom"));;
		
		byte[] in = null;
		
		int len = 0;  
		int offset = 0;
		
		try{
//			uuid = UUID.randomUUID();
			
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat formatter2=new SimpleDateFormat("yyyyMMddhhmmss",Locale.KOREA);
			String period = formatter2.format(cal.getTime());
			
			StringUtil stringUtil = new StringUtil();
			int inputNum[] = stringUtil.getRandomNum(4);
			String inputNumFull = "" + inputNum[0] + inputNum[1] + inputNum[2] + inputNum[3];
			
//			uuidFileName = uuid + "." + getFileExt(fileName);
			uuidFileName = period + inputNumFull + "." + getFileExt(fileName);
	
//			String ext = getFileExt(fileName);
			logger.debug("uuidFileName : " + uuidFileName);
			
			//Properties props = getProperties("config.properties");
			//uploadPath = props.getProperty("upload.path");
			
			is = new ByteArrayInputStream(file);
			
			s = new FileOutputStream(uploadPath + uuidFileName);
			
			in = new byte[(int)file.length];
			
System.out.println("uploadPath + uuidFileName===>"+uploadPath + uuidFileName);			
			
			while ((len = is.read(in, offset, in.length)) != -1 );
			len = is.read(in, offset, in.length);
			
		}finally{
			
			s.write(in);			//서버로 파일 write
			is.close();
			s.close();
		}
		
		logger.debug("uploadPath : " + uploadPath);
		
		Map map = new HashMap();
		map.put("FILE_PATH", uploadPath);
		map.put("REAL_FILE_NAME", uuidFileName);
		
		return map;

	}
	
	public static String uploadPath(){		
		
		//String uploadPath = "/home/tmax/mls/web/upload/";  // 실서버
       //String uploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버
//		String uploadPath = "D:/temp/";  // 로컬용
		//String uploadPath = "D:/Server/mls/mls_2010/upload/";  // 로컬용
		
		String uploadPath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path"));
//		String uploadPath = "D:/01.project/04.mls_new/upload/"; // 이관서버용
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println(uploadPath);
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		return uploadPath;
	}
	
	public static String uploadPath2(){		
		
		//String uploadPath = "/home/tmax/mls/web/upload/";  // 실서버
   //String uploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버
//		String uploadPath = "D:/temp/";  // 로컬용
		//String uploadPath = "D:/Server/mls/mls_2010/upload/";  // 로컬용
		
		String uploadPath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_upload_path2"));
//		String uploadPath = "D:/01.project/04.mls_new/upload/"; // 이관서버용
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println(uploadPath);
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		return uploadPath;
	}
	/*
	 * public static void main(String args[]){
	 * 
	 * UUID uuid = UUID.randomUUID();
	 * 
	 * String uuidFileName = uuidFileName + "." +
	 * 
	 * 
	 * System.out.println(uuid.toString());
	 * 
	 * uuid = UUID.randomUUID(); System.out.println(uuid.toString());
	 * 
	 * uuid = UUID.randomUUID(); System.out.println(uuid.toString());
	 * 
	 * uuid = UUID.randomUUID(); System.out.println(uuid.toString());
	 * 
	 * uuid = UUID.randomUUID(); System.out.println(uuid.toString());
	 * 
	 * uuid = UUID.randomUUID(); System.out.println(uuid.toString()); }
	 */
}


