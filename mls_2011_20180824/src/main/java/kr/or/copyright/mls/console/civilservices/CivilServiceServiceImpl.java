package kr.or.copyright.mls.console.civilservices;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import kr.or.copyright.mls.adminStatProc.service.AdminStatProcService;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.civilservices.inter.CivilServicesDAO;
import kr.or.copyright.mls.console.civilservices.inter.CivilServicesService;
import kr.or.copyright.mls.console.civilservices.model.CivilServicesDTO;

@Repository( "CivilServicesService" )
public class CivilServiceServiceImpl extends CommandService implements CivilServicesService{
	
	@Resource( name = "CivilServicesDAO" )
	private CivilServicesDAO civilServicesDAO;
	
	public void insertCivilServices( Map<String, Object> commandMap ){
		civilServicesDAO.insertCivilServices( commandMap );
	}

	public String civilServicesFromCivilSeqn(){
		return civilServicesDAO.civilServicesFromCivilSeqn();
	}
	
	public void insertCivilServicesFile(CivilServicesDTO civilServicesDTO) {
		civilServicesDAO.insertCivilServicesFile(civilServicesDTO);
	}

	public List selectCivilServices(Map<String, Object> commandMap){
		return (List)civilServicesDAO.selectCivilServices(commandMap);
	}
	
	public int selectCountCivilServices(){
		return civilServicesDAO.selectCountCivilServices();
	}

	public List selectCivilServicesByCivilSeqn( Map<String, Object> commandMap ){
		return civilServicesDAO.selectCivilServicesByCivilSeqn(commandMap);
	}
	
	public List selectCivilServicesFile( Map<String, Object> commandMap ){
		return civilServicesDAO.selectCivilServicesFile(commandMap);
	}
	public void updateCivilServiceState(Map<String, Object> commandMap) {
		civilServicesDAO.updateCivilServiceState(commandMap);
	}
	
	public List selectCivilServicesComment( Map<String, Object> commandMap ){
	 	return civilServicesDAO.selectCivilServicesComment(commandMap);
	}
	
	public List selectCivilServicesDownLoadFile( Map<String, Object> commandMap ){
		return civilServicesDAO.selectCivilServicesDownLoadFile(commandMap);
	}
	
	public List selectCivilServicesExcelList( Map<String, Object> commandMap ){
		return civilServicesDAO.selectCivilServicesExcelList(commandMap);
	}
	
	public void updateCivilServicesComment( Map<String, Object> commandMap ){
		civilServicesDAO.updateCivilServicesComment(commandMap);
	}

	public void deleteCivilServicesFile( List<Map> deleteList ){
		for(int i = 0 ;  i < deleteList.size() ; i ++)
		{
			String deleteFileNum = String.valueOf(deleteList.get( i ).get("civilFileSeqn"));
			civilServicesDAO.deleteCivilServicesFile(deleteFileNum);
		}
		
	}

	public void deleteCivilServicesByCivilSeqn( List<Map> deleteList ){
		for(int i = 0 ;  i < deleteList.size() ; i ++)
		{
			String deleteFileNum = String.valueOf(deleteList.get( i ).get("civilSeqn"));
			civilServicesDAO.deleteCivilServicesByCivilSeqn(deleteFileNum);
		}
		
	}

	public void deleteCivilServicesComment( List<Map> deleteCommentList ){
		for(int i = 0 ;  i < deleteCommentList.size() ; i ++)
		{
			String deleteFileNum = String.valueOf(deleteCommentList.get( i ).get("civilSeqn"));
			civilServicesDAO.deleteCivilServicesComment(deleteFileNum);
		}
	}

	public void deleteCivilServicesState( List<Map> deleteCommentList ){
		for(int i = 0 ;  i < deleteCommentList.size() ; i ++)
		{
			String deleteCivilServiceNum = String.valueOf(deleteCommentList.get( i ).get("civilSeqn"));
			civilServicesDAO.deleteCivilServicesState(deleteCivilServiceNum);
		}
	}

	public List companySrch( Map<String, Object> companyNameList ){
		return civilServicesDAO.companySrch(companyNameList);
	}

	public List personSrch( Map<String, Object> commandMap ){
		return civilServicesDAO.personSrch(commandMap);
	}

	public List selectCivilServicesPerson( Map<String, Object> returnList ){
		return civilServicesDAO.selectCivilServicesPerson(returnList);
	}

	public List selectCivilStats( Map<String, Object> commandMap ){
		return civilServicesDAO.selectCivilStats();
	}
}
