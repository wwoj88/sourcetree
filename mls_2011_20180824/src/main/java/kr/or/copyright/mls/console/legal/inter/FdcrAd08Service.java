package kr.or.copyright.mls.console.legal.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface FdcrAd08Service{

	/**
	 * 이용승인 신청 공고 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> fdcrAd08List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 이용승인 신청 공고 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd08View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 이용승인 신청 공고 수정 폼
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd08UpdateForm1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 이용승인 신청 공고 수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd08Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 법정허락 이용승인 신청서 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd08Delete1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 이용승인신청공고 게시판 등록 폼
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd08WriteForm1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 이용승인신청공고 게시판 등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd08Regi1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 이용승인신청 일괄등록 파일 업로드
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd08Upload1( ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 이용승인신청공고 게시판 일괄등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd08Regi2( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 이용승인 신청 공고 목록 엑셀다운로드
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd08ExcelDown1( Map<String, Object> commandMap ) throws Exception;
}
