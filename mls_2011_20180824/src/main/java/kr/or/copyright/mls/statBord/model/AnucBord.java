package kr.or.copyright.mls.statBord.model;

import java.util.List;


public class AnucBord {
	private int statObjcId;		//이의제기 구분
	private long bordCd;		//게시판 구분CD
	private long bordSeqn;		//게시판 순번
	private String openYn;		//공고 여부
	private String openDttm;	//공고 날짜
	private String rgstIdnt;	//등록자 ID
	private String rgstDttm;	//등록 날짜
	private String modiIdnt;	//수정자 ID
	private String modiDttm;	//수정 날짜
	private String tite;		//제목
	private String bordDesc;	//내용
	private String deltYsno;	//삭제유무
	private String objcYn;		//이의제기 신청 여부
	private long worksId;		//저작물 ID
	private String receiptNo;	//이용승인신청접수번호
	private long divsCd;		//저작물 구분CD
	private int inqrCont;		//조회수
	private String anucItem1;	//공고항목1
	private String anucItem2;
	private String anucItem3;
	private String anucItem4;
	private String anucItem5;
	private String anucItem6;
	private String anucItem7;
	private String anucItem8;
	private String anucItem9;
	private String anucItem10;
	private String anucItem11;
	private String anucItem12;
	private String anucItem13;
	private String anucItem14;
	private String anucItem15;	//공고항목 15
	private long genreCd;		//장르
	private String genreCdName;	//장르 Name
	private String divsCdName; 	//구분 Name
	private int objcCount;
	//첨부파일
	private List fileList;
	
	//	페이징
	private int nowPage;  
	private int startRow; 
	private int endRow; 
	private int totalRow;
	private int rowNo;
	
	//거소불명 
	private String midCode;
	private String CodeName;
	private int coptHodrRoleCd;
	private String coptHodrRoleCdName;
	private int coptHodrSeqn;
    private String coptHodrName;
    private String worksTitle;
    private String systEffortStatCdName;
    private int systEffortStatCd;
    private long coptHodrsLength;
    private int bigCode;
    private int coptHodrMax;
    private int coptHodrMin;
    private String joinDay;
    private String endDay;
	private String defaultJoinDay;
	private String defaultEndDay;	
	private int rgstReasCd;				//라디오박스 Value
	private String rgstReasEtc;			//라디오박스 Etc
	private String statCd;
	private String statRsltCd;
	private String statCdName;
    private String statRsltCdName;
    private String statWorksYn;
    
    private int worksSeqn;
    private int mappWorksDivsCd;
    private int mappWorksId;
    private String worksDttm;
    private String remark;
    private String mgntDivs;
    
    private String suplYn; // 보완여부
    private long anucStatCd; // 공고신청처리상태CD
    
    private String errorFlag; // 다건 업로드시 에러 체크
    private int insertRow; // 다건 업로드시 사용자가 입력한 순번 셀
    private int countRow; // 다건 업로드시 엑셀 row 갯수에 따른 순번
    private String coptHodrCdAndName; // 다건 업로드시 저작권자정보(코드+저작자)
    private String rgstReasCdAndEtc; // 다건 업로드시 신청사유(코드+기타)
    
    //20171207
    
    public void setRgstReasCdAndEtc(String rgstReasCdAndEtc) {
		this.rgstReasCdAndEtc = rgstReasCdAndEtc;
	}
    public String getRgstReasCdAndEtc() {
		return rgstReasCdAndEtc;
	}
    
    public String getCoptHodrCdAndName() {
		return coptHodrCdAndName;
	}
    public void setCoptHodrCdAndName(String coptHodrCdAndName) {
		this.coptHodrCdAndName = coptHodrCdAndName;
	}

    public int getInsertRow() {
		return insertRow;
	}
	public void setInsertRow(int insertRow) {
		this.insertRow = insertRow;
	}
	public int getCountRow() {
		return countRow;
	}
	public void setCountRow(int countRow) {
		this.countRow = countRow;
	}
	public String getErrorFlag() {
	   return errorFlag;
    }
    public void setErrorFlag(String errorFlag) {
    	this.errorFlag = errorFlag;
    }
	public String getSuplYn() {
		return suplYn;
	}
	public void setSuplYn(String suplYn) {
		this.suplYn = suplYn;
	}
	public long getAnucStatCd() {
		return anucStatCd;
	}
	public void setAnucStatCd(long anucStatCd) {
		this.anucStatCd = anucStatCd;
	}
	
	public String getMgntDivs() {
        return mgntDivs;
    }
    public void setMgntDivs(String mgntDivs) {
        this.mgntDivs = mgntDivs;
    }
	public int getStatObjcId() {
		return statObjcId;
	}
	public void setStatObjcId(int statObjcId) {
		this.statObjcId = statObjcId;
	}
	public long getBordCd() {
		return bordCd;
	}
	public void setBordCd(long bordCd) {
		this.bordCd = bordCd;
	}
	public long getBordSeqn() {
		return bordSeqn;
	}
	public void setBordSeqn(long bordSeqn) {
		this.bordSeqn = bordSeqn;
	}
	public String getOpenYn() {
		return openYn;
	}
	public void setOpenYn(String openYn) {
		this.openYn = openYn;
	}
	public String getOpenDttm() {
		return openDttm;
	}
	public void setOpenDttm(String openDttm) {
		this.openDttm = openDttm;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	public String getModiIdnt() {
		return modiIdnt;
	}
	public void setModiIdnt(String modiIdnt) {
		this.modiIdnt = modiIdnt;
	}
	public String getModiDttm() {
		return modiDttm;
	}
	public void setModiDttm(String modiDttm) {
		this.modiDttm = modiDttm;
	}
	public String getTite() {
		return tite;
	}
	public void setTite(String tite) {
		this.tite = tite;
	}
	public String getBordDesc() {
		return bordDesc;
	}
	public void setBordDesc(String bordDesc) {
		this.bordDesc = bordDesc;
	}
	public String getDeltYsno() {
		return deltYsno;
	}
	public void setDeltYsno(String deltYsno) {
		this.deltYsno = deltYsno;
	}
	public String getObjcYn() {
		return objcYn;
	}
	public void setObjcYn(String objcYn) {
		this.objcYn = objcYn;
	}
	public long getWorksId() {
		return worksId;
	}
	public void setWorksId(long worksId) {
		this.worksId = worksId;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public long getDivsCd() {
		return divsCd;
	}
	public void setDivsCd(long divsCd) {
		this.divsCd = divsCd;
	}
	public int getInqrCont() {
		return inqrCont;
	}
	public void setInqrCont(int inqrCont) {
		this.inqrCont = inqrCont;
	}
	public String getAnucItem1() {
		return anucItem1;
	}
	public void setAnucItem1(String anucItem1) {
		this.anucItem1 = anucItem1;
	}
	public String getAnucItem2() {
		return anucItem2;
	}
	public void setAnucItem2(String anucItem2) {
		this.anucItem2 = anucItem2;
	}
	public String getAnucItem3() {
		return anucItem3;
	}
	public void setAnucItem3(String anucItem3) {
		this.anucItem3 = anucItem3;
	}
	public String getAnucItem4() {
		return anucItem4;
	}
	public void setAnucItem4(String anucItem4) {
		this.anucItem4 = anucItem4;
	}
	public String getAnucItem5() {
		return anucItem5;
	}
	public void setAnucItem5(String anucItem5) {
		this.anucItem5 = anucItem5;
	}
	public String getAnucItem6() {
		return anucItem6;
	}
	public void setAnucItem6(String anucItem6) {
		this.anucItem6 = anucItem6;
	}
	public String getAnucItem7() {
		return anucItem7;
	}
	public void setAnucItem7(String anucItem7) {
		this.anucItem7 = anucItem7;
	}
	public String getAnucItem8() {
		return anucItem8;
	}
	public void setAnucItem8(String anucItem8) {
		this.anucItem8 = anucItem8;
	}
	public String getAnucItem9() {
		return anucItem9;
	}
	public void setAnucItem9(String anucItem9) {
		this.anucItem9 = anucItem9;
	}
	public String getAnucItem10() {
		return anucItem10;
	}
	public void setAnucItem10(String anucItem10) {
		this.anucItem10 = anucItem10;
	}
	public String getAnucItem11() {
		return anucItem11;
	}
	public void setAnucItem11(String anucItem11) {
		this.anucItem11 = anucItem11;
	}
	public String getAnucItem12() {
		return anucItem12;
	}
	public void setAnucItem12(String anucItem12) {
		this.anucItem12 = anucItem12;
	}
	public String getAnucItem13() {
		return anucItem13;
	}
	public void setAnucItem13(String anucItem13) {
		this.anucItem13 = anucItem13;
	}
	public String getAnucItem14() {
		return anucItem14;
	}
	public void setAnucItem14(String anucItem14) {
		this.anucItem14 = anucItem14;
	}
	public String getAnucItem15() {
		return anucItem15;
	}
	public void setAnucItem15(String anucItem15) {
		this.anucItem15 = anucItem15;
	}
	public long getGenreCd() {
		return genreCd;
	}
	public void setGenreCd(long genreCd) {
		this.genreCd = genreCd;
	}
	public String getGenreCdName() {
		return genreCdName;
	}
	public void setGenreCdName(String genreCdName) {
		this.genreCdName = genreCdName;
	}
	public String getDivsCdName() {
		return divsCdName;
	}
	public void setDivsCdName(String divsCdName) {
		this.divsCdName = divsCdName;
	}
	public int getObjcCount() {
		return objcCount;
	}
	public void setObjcCount(int objcCount) {
		this.objcCount = objcCount;
	}
	public List getFileList() {
		return fileList;
	}
	public void setFileList(List fileList) {
		this.fileList = fileList;
	}
	public int getNowPage() {
		return nowPage;
	}
	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
	public int getTotalRow() {
		return totalRow;
	}
	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}
	public int getRowNo() {
		return rowNo;
	}
	public void setRowNo(int rowNo) {
		this.rowNo = rowNo;
	}
	public String getMidCode() {
		return midCode;
	}
	public void setMidCode(String midCode) {
		this.midCode = midCode;
	}
	public String getCodeName() {
		return CodeName;
	}
	public void setCodeName(String codeName) {
		CodeName = codeName;
	}
	public int getCoptHodrRoleCd() {
		return coptHodrRoleCd;
	}
	public void setCoptHodrRoleCd(int coptHodrRoleCd) {
		this.coptHodrRoleCd = coptHodrRoleCd;
	}
	public String getCoptHodrRoleCdName() {
		return coptHodrRoleCdName;
	}
	public void setCoptHodrRoleCdName(String coptHodrRoleCdName) {
		this.coptHodrRoleCdName = coptHodrRoleCdName;
	}
	public int getCoptHodrSeqn() {
		return coptHodrSeqn;
	}
	public void setCoptHodrSeqn(int coptHodrSeqn) {
		this.coptHodrSeqn = coptHodrSeqn;
	}
	public String getCoptHodrName() {
		return coptHodrName;
	}
	public void setCoptHodrName(String coptHodrName) {
		this.coptHodrName = coptHodrName;
	}
	public String getWorksTitle() {
		return worksTitle;
	}
	public void setWorksTitle(String worksTitle) {
		this.worksTitle = worksTitle;
	}
	public String getSystEffortStatCdName() {
		return systEffortStatCdName;
	}
	public void setSystEffortStatCdName(String systEffortStatCdName) {
		this.systEffortStatCdName = systEffortStatCdName;
	}
	public int getSystEffortStatCd() {
		return systEffortStatCd;
	}
	public void setSystEffortStatCd(int systEffortStatCd) {
		this.systEffortStatCd = systEffortStatCd;
	}
	public long getCoptHodrsLength() {
		return coptHodrsLength;
	}
	public void setCoptHodrsLength(long coptHodrsLength) {
		this.coptHodrsLength = coptHodrsLength;
	}
	public int getBigCode() {
		return bigCode;
	}
	public void setBigCode(int bigCode) {
		this.bigCode = bigCode;
	}
	public int getCoptHodrMax() {
		return coptHodrMax;
	}
	public void setCoptHodrMax(int coptHodrMax) {
		this.coptHodrMax = coptHodrMax;
	}
	public int getCoptHodrMin() {
		return coptHodrMin;
	}
	public void setCoptHodrMin(int coptHodrMin) {
		this.coptHodrMin = coptHodrMin;
	}
	public String getJoinDay() {
		return joinDay;
	}
	public void setJoinDay(String joinDay) {
		this.joinDay = joinDay;
	}
	public String getEndDay() {
		return endDay;
	}
	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}
	public String getDefaultJoinDay() {
		return defaultJoinDay;
	}
	public void setDefaultJoinDay(String defaultJoinDay) {
		this.defaultJoinDay = defaultJoinDay;
	}
	public String getDefaultEndDay() {
		return defaultEndDay;
	}
	public void setDefaultEndDay(String defaultEndDay) {
		this.defaultEndDay = defaultEndDay;
	}
	public int getRgstReasCd() {
		return rgstReasCd;
	}
	public void setRgstReasCd(int rgstReasCd) {
		this.rgstReasCd = rgstReasCd;
	}
	public String getRgstReasEtc() {
		return rgstReasEtc;
	}
	public void setRgstReasEtc(String rgstReasEtc) {
		this.rgstReasEtc = rgstReasEtc;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getStatRsltCd() {
		return statRsltCd;
	}
	public void setStatRsltCd(String statRsltCd) {
		this.statRsltCd = statRsltCd;
	}
	public String getStatCdName() {
		return statCdName;
	}
	public void setStatCdName(String statCdName) {
		this.statCdName = statCdName;
	}
	public String getStatRsltCdName() {
		return statRsltCdName;
	}
	public void setStatRsltCdName(String statRsltCdName) {
		this.statRsltCdName = statRsltCdName;
	}
	public String getStatWorksYn() {
		return statWorksYn;
	}
	public void setStatWorksYn(String statWorksYn) {
		this.statWorksYn = statWorksYn;
	}
	public int getWorksSeqn() {
		return worksSeqn;
	}
	public void setWorksSeqn(int worksSeqn) {
		this.worksSeqn = worksSeqn;
	}
	public int getMappWorksDivsCd() {
		return mappWorksDivsCd;
	}
	public void setMappWorksDivsCd(int mappWorksDivsCd) {
		this.mappWorksDivsCd = mappWorksDivsCd;
	}
	public int getMappWorksId() {
		return mappWorksId;
	}
	public void setMappWorksId(int mappWorksId) {
		this.mappWorksId = mappWorksId;
	}
	public String getWorksDttm() {
		return worksDttm;
	}
	public void setWorksDttm(String worksDttm) {
		this.worksDttm = worksDttm;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
    
    
}