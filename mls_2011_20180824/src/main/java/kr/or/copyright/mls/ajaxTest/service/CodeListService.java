package kr.or.copyright.mls.ajaxTest.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.ajaxTest.model.Code;
import kr.or.copyright.mls.user.model.ZipCodeRoad;

public interface CodeListService {
	public Map<String, String> getInitCodeList();
	
	public List<Code> getCodeList(Map params);
	
	public List<ZipCodeRoad> getInitRoadCodeList();

	public List<ZipCodeRoad> getRoadCodeList(Map params);
	
}
