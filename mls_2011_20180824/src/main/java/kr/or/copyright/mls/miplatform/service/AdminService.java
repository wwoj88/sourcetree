package kr.or.copyright.mls.miplatform.service;

public interface AdminService {
	
	public void login() throws Exception;
	
	public void logout() throws Exception;
}
