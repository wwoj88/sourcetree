package kr.or.copyright.mls.console.mber;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Dao;
import kr.or.copyright.mls.console.mber.inter.FdcrAd84Service;
import kr.or.copyright.mls.console.menu.inter.FdcrAdA9Dao;

import org.springframework.stereotype.Service;

import com.softforum.xdbe.xCrypto;
import com.tobesoft.platform.data.Dataset;

@Service( "fdcrAd84Service" )
public class FdcrAd84ServiceImpl extends CommandService implements FdcrAd84Service{

	// old AdminUserDao
	@Resource( name = "fdcrAd83Dao" )
	private FdcrAd83Dao fdcrAd83Dao;

	// old AdminMenuDao
	@Resource( name = "fdcrAdA9Dao" )
	private FdcrAdA9Dao fdcrAdA9Dao;

	/**
	 * 신탁단체 담당자목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd84List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		int totCnt = fdcrAd83Dao.trstOrgnMgnbListCount( commandMap );
		pagination( commandMap, totCnt, 0 );
		List list = (List) fdcrAd83Dao.trstOrgnMgnbList( commandMap );
		commandMap.put( "ds_list", list ); // 로그인 사용자 정보
	}

	/**
	 * 신탁단체 담당자 상세 조회, 내정보조회 상세
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd84UpdateForm1( Map<String, Object> commandMap ) throws Exception{
		List groupList = (List) fdcrAdA9Dao.selectGroupList();

		commandMap.put( "ds_group_info", groupList );

		if( commandMap.get( "GUBUN" ).equals( "edit" ) ){ // 수정
			// DAO호출
			List list = (List) fdcrAd83Dao.trstOrgnMgnbDetlList( commandMap );
			HashMap mapTemp = new HashMap();
			if( list.size() > 0 ){
				// 그룹 메뉴 조회 하기
				mapTemp = new HashMap();
				mapTemp.clear();
				mapTemp = (HashMap) list.get( 0 );

				List listMenu = (List) fdcrAd83Dao.adminGroupMenuInfo( mapTemp );

				commandMap.put( "ds_menu_info", listMenu );
			}
			commandMap.put( "ds_list", mapTemp ); // 로그인 사용자 정보
		}

	}
	
	public ArrayList<Map<String, Object>> adminGroupMenuInfo( Map<String, Object> commandMap ) throws Exception{
		return (ArrayList<Map<String, Object>>) fdcrAd83Dao.adminGroupMenuInfo( commandMap );
	}
	

	/**
	 * 신탁단체 담당자 입력
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd84Insert1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			String TRST_ORGN_DIVS_CODE = (String)commandMap.get( "TRST_ORGN_DIVS_CODE" );
			if( "5".equals( TRST_ORGN_DIVS_CODE ) ){
				int newTrstOrgnCd = fdcrAd83Dao.getNewTrstOrgnCd();
				commandMap.put( "TRST_ORGN_CODE", newTrstOrgnCd);
				//commandMap.put( "TRST_ORGN_CODE", "9000");
			}
			
			/* pwsd암호화 str 20121108 성혜진 */
//			String sOutput_H = null;
//			String PSWD = ( (String) commandMap.get( "PSWD" ) ).toUpperCase();
//			sOutput_H = xCrypto.Hash( 6, PSWD );// 단방향 암호화(sOutput)
//			commandMap.put( "PSWD", sOutput_H );
			commandMap.put( "PSWD", "D/4avRoIIVNTwjPW4AlhPpXuxCU4Mqdhryj/N6xaFQw=" );
			/* pwsd암호화 end 20121108 성혜진 */
			fdcrAd83Dao.trstOrgnMgnbDetlInsert( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 신탁단체 담당자 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd84Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			// 대표담당자 정보 수정
			String ORGN_MGNB_YSNO = (String) commandMap.get( "ORGN_MGNB_YSNO" );

			// 해당기관의 담당자 ORGN_MGNB_YSNO= 'N'
			if( ORGN_MGNB_YSNO.equals( "Y" ) ) fdcrAd83Dao.orgnMgnbUpdate( commandMap );

			fdcrAd83Dao.trstOrgnMgnbDetlUpdate( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 관리자 아이디 중복체크
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public int fdcrAd84Update2( Map<String, Object> commandMap ) throws Exception{
		// 관리자ID체크
		return fdcrAd83Dao.adminIdCheck( commandMap );
	}
	
	/**
	 * 대표담당자 존재여부 체크
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public int fdcrAd84Update3( Map<String, Object> commandMap ) throws Exception{
		// 관리자ID체크
		int iUserCnt = fdcrAd83Dao.orgnMgnbCheck( commandMap );
		return iUserCnt;
	}
	
	/**
	 * 기관/단체 조회
	 * @param commandMap
	 * @throws SQLException 
	 */
	public void trstOrgnCoNameList( Map<String, Object> commandMap ) throws Exception{
		//return (ArrayList<Map<String,Object>>)fdcrAd83Dao.trstOrgnCoNameList(commandMap);
		// DAO호출
		int totCnt = fdcrAd83Dao.trstOrgnCoNameListCount( commandMap );
		pagination( commandMap, totCnt, 0 );
		ArrayList<Map<String,Object>> list = (ArrayList) fdcrAd83Dao.trstOrgnCoNameList( commandMap );
		commandMap.put( "ds_list", list ); // 로그인 사용자 정보
	}
}
