package kr.or.copyright.mls.console.rcept;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd0301Dao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd32Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd32Service" )
public class FdcrAd32ServiceImpl extends CommandService implements FdcrAd32Service{

	// OldDao adminStatProcDao
	@Resource( name = "fdcrAd03Dao" )
	private FdcrAd03Dao fdcrAd03Dao;

	// OldDao adminEvnSetDao
	@Resource( name = "fdcrAd0301Dao" )
	private FdcrAd0301Dao fdcrAd0301Dao;

	/**
	 * 상당한노력 진행현황 조회
	 * 
	 * @throws Exception
	 */
	public void fdcrAd32List1( Map<String, Object> commandMap ) throws Exception{
		// DAO 호출
		List list = (List) fdcrAd03Dao.selectStatProcAllLogList( commandMap );
		List works_list = (List) fdcrAd03Dao.selectStatWorksAllLogList( commandMap );
		List set_list = (List) fdcrAd0301Dao.statProcSetList( commandMap );

		commandMap.put( "ds_works_list", works_list );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_set_list", set_list );
	}

}
