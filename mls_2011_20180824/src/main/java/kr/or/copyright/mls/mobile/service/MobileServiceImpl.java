package kr.or.copyright.mls.mobile.service;

import java.util.HashMap;
import java.util.List;

import kr.or.copyright.mls.mobile.dao.MobileDao;
import kr.or.copyright.mls.mobile.model.FAQ;
import kr.or.copyright.mls.mobile.model.Stat;

public class MobileServiceImpl implements MobileService {
	private MobileDao mobileDao;
	
	public List<Stat> statList(HashMap<String,Object> hm) {
		return mobileDao.statList(hm);
	}
	
	public int statListCnt(HashMap<String,Object> hm) {
		return mobileDao.statListCnt(hm);
	}
	
	public Stat statDetl(int bordSeqn) {
		return mobileDao.statDetl(bordSeqn);
	}
	
	public List<FAQ> faqList(HashMap<String,Object> hm) {
		return mobileDao.faqList(hm);
	}
	
	public int faqListCnt(HashMap<String,Object> hm) {
		return mobileDao.faqListCnt(hm);
	}
	
	public MobileDao getMobileDao() {
		return mobileDao;
	}

	public void setMobileDao(MobileDao mobileDao) {
		this.mobileDao = mobileDao;
	}

	public void insertUserMobileCount() {
		mobileDao.insertUserMobileCount();
	}
	
	public void insertUserMobileCount(int sysCd) {
		mobileDao.insertUserMobileCount(sysCd);
	}	
}
