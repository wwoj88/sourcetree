package kr.or.copyright.mls.support.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import kr.or.copyright.mls.board.model.BoardFile;
import kr.or.copyright.mls.rghtPrps.model.PrpsAttc;
import kr.or.copyright.mls.statBord.model.AnucBordFile;
import kr.or.copyright.mls.worksPriv.model.WorksPrivFile;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.MultipartFile;
   
public class FileUploadUtil {
    protected Log logger = LogFactory.getLog(FileUploadUtil.class);
   
	//private static String realUploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버용
	private static String realUploadPath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_realUpload_path")); // 로컬용
	//private String realUploadPath = "/home/tmax/mls/web/upload/";  // 실서버용
	//private String realUploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버용
	
    public String getRealUploadPath() {
		return realUploadPath;
	}
   
	public void setRealUploadPath(String realUploadPath) {
		this.realUploadPath = realUploadPath;
	}     

	/* 파일명 생성 - rename */
	public static String makeFileName(String originName, String attName){
		
		String originFileNm = originName;
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat formatter2=new SimpleDateFormat("yyyyMMddhhmmss",Locale.KOREA);
		String period = formatter2.format(cal.getTime());
		
		StringUtil stringUtil = new StringUtil();
		int inputNum[] = stringUtil.getRandomNum(4);
		String inputNumFull = "" + inputNum[0] + inputNum[1] + inputNum[2] + inputNum[3];
		
		int ii = 0;
		ii = originFileNm.lastIndexOf(".");
		
		String endNm = originFileNm.substring(ii);
		
		originFileNm = period + inputNumFull + attName +endNm;
		
		System.out.println("originFileNm >>>> "+originFileNm);
		
		return originFileNm;
	}
	
	public String uploadFile(MultipartFile file, String fileName) throws Exception {
        InputStream stream;
        
        try {
            stream = file.getInputStream(); 

            // write the file to the file specified
            OutputStream bos = new FileOutputStream(realUploadPath + fileName);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }
            bos.close();
            stream.close();
            
            if (logger.isDebugEnabled()) {
                logger.debug("The file has been written to \"" + realUploadPath
                        + fileName);
            }
            
            return realUploadPath + fileName;
            
        } catch (FileNotFoundException e) {
            throw new Exception(e);
        } catch (IOException e) {
        	throw new Exception(e);
        }
    }
	
	public static BoardFile uploadFormFiles(MultipartFile formFile,	String realPath) {
		InputStream stream;
		String originFileNm = formFile.getOriginalFilename();
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat formatter2=new SimpleDateFormat("yyyyMMddhhmmss",Locale.KOREA);
		String period = formatter2.format(cal.getTime());
		
		StringUtil stringUtil = new StringUtil();
		int inputNum[] = stringUtil.getRandomNum(4);
		String inputNumFull = "" + inputNum[0] + inputNum[1] + inputNum[2] + inputNum[3];
		
		int ii = 0;
		ii = originFileNm.lastIndexOf(".");
		
		String endNm = originFileNm.substring(ii);
		
		originFileNm = period + inputNumFull + endNm;
		
		/*
		File fExists = new File(realPath + originFileNm);
		
		if( fExists.exists() ){
			int ii = 0;
			ii = originFileNm.lastIndexOf(".");
			
			String startNm = originFileNm.substring(0,ii);
			String endNm = originFileNm.substring(ii);
			
			originFileNm = startNm+period+endNm;
		}*/
		
		try {
			stream = formFile.getInputStream();
			OutputStream bos = new FileOutputStream(realPath + originFileNm);//tempFileName
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
				bos.write(buffer, 0, bytesRead);
			}
			bos.close();
			stream.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		BoardFile boardFile = new BoardFile();
		boardFile.setFileName(formFile.getOriginalFilename());
		boardFile.setFileSize(formFile.getSize() + "");
		boardFile.setRealFileName(originFileNm);
		boardFile.setFilePath(realPath);

		return boardFile;
	}
	
	public static AnucBordFile uploadFormFilesStatBord(MultipartFile formFile,	String realPath) {
		InputStream stream;
		String originFileNm = formFile.getOriginalFilename();
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat formatter2=new SimpleDateFormat("yyyyMMddhhmmss",Locale.KOREA);
		String period = formatter2.format(cal.getTime());
		
		StringUtil stringUtil = new StringUtil();
		int inputNum[] = stringUtil.getRandomNum(4);
		String inputNumFull = "" + inputNum[0] + inputNum[1] + inputNum[2] + inputNum[3];
		
		int ii = 0;
		ii = originFileNm.lastIndexOf(".");
		
		String endNm = originFileNm.substring(ii);
		
		originFileNm = period + inputNumFull + endNm;
		
		/*
		File fExists = new File(realPath + originFileNm);
		
		if( fExists.exists() ){
			int ii = 0;
			ii = originFileNm.lastIndexOf(".");
			
			String startNm = originFileNm.substring(0,ii);
			String endNm = originFileNm.substring(ii);
			
			originFileNm = startNm+period+endNm;
		}*/
		
		try {
			stream = formFile.getInputStream();
			OutputStream bos = new FileOutputStream(realPath + originFileNm);//tempFileName
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
				bos.write(buffer, 0, bytesRead);
			}
			bos.close();
			stream.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		AnucBordFile anucBordFile = new AnucBordFile();
		anucBordFile.setFileName(formFile.getOriginalFilename());
		anucBordFile.setFileSize(formFile.getSize() + "");
		anucBordFile.setRealFileName(originFileNm);
		anucBordFile.setFilePath(realPath);

		return anucBordFile;
	}
	public static WorksPrivFile uploadFormFilesW(MultipartFile formFile,	String realPath) {
		InputStream stream;
		String originFileNm = formFile.getOriginalFilename();
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat formatter2=new SimpleDateFormat("yyyyMMddhhmmss",Locale.KOREA);
		String period = formatter2.format(cal.getTime());
		
		StringUtil stringUtil = new StringUtil();
		int inputNum[] = stringUtil.getRandomNum(4);
		String inputNumFull = "" + inputNum[0] + inputNum[1] + inputNum[2] + inputNum[3];
		
		int ii = 0;
		ii = originFileNm.lastIndexOf(".");
		
		String endNm = originFileNm.substring(ii);
		
		originFileNm = period + inputNumFull + endNm;
		
		/*
		File fExists = new File(realPath + originFileNm);
		
		if( fExists.exists() ){
			int ii = 0;
			ii = originFileNm.lastIndexOf(".");
			
			String startNm = originFileNm.substring(0,ii);
			String endNm = originFileNm.substring(ii);
			
			originFileNm = startNm+period+endNm;
		}*/
		
		try {
			stream = formFile.getInputStream();
			OutputStream bos = new FileOutputStream(realPath + originFileNm);//tempFileName
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
				bos.write(buffer, 0, bytesRead);
			}
			bos.close();
			stream.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		WorksPrivFile worksPrivFile = new WorksPrivFile();
		worksPrivFile.setFileName(formFile.getOriginalFilename());
		worksPrivFile.setFileSize(formFile.getSize() + "");
		worksPrivFile.setRealFileName(originFileNm);
		worksPrivFile.setFilePath(realPath);

		return worksPrivFile;
	}

	public static void deleteFormFiles(String fileNm, String realPath) {
		
		try {
		   File file = new File(realPath, fileNm);
		   if (file.exists()) { 
			 file.delete(); 
		   }
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public static PrpsAttc uploadPrpsFiles(MultipartFile formFile,	String realPath) {
		InputStream stream;
		String originFileNm = formFile.getOriginalFilename();
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat formatter2=new SimpleDateFormat("yyyyMMddhhmmss",Locale.KOREA);
		String period = formatter2.format(cal.getTime());
		
		StringUtil stringUtil = new StringUtil();
		int inputNum[] = stringUtil.getRandomNum(4);
		String inputNumFull = "" + inputNum[0] + inputNum[1] + inputNum[2] + inputNum[3];
		
		int ii = 0;
		ii = originFileNm.lastIndexOf(".");
		
		String endNm = originFileNm.substring(ii);
		
		originFileNm = period + inputNumFull + endNm;
		
		/*
		File fExists = new File(realPath + originFileNm);
		
		if( fExists.exists() ){
			int ii = 0;
			ii = originFileNm.lastIndexOf(".");
			
			String startNm = originFileNm.substring(0,ii);
			String endNm = originFileNm.substring(ii);
			
			originFileNm = startNm+period+endNm;
		}*/
		
		try {
			stream = formFile.getInputStream();
			OutputStream bos = new FileOutputStream(realPath + originFileNm);//tempFileName
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
				bos.write(buffer, 0, bytesRead);
			}
			bos.close();
			stream.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		PrpsAttc prpsAttc = new PrpsAttc();
		prpsAttc.setREAL_FILE_NAME(originFileNm);
		prpsAttc.setFILE_PATH(realPath);
		prpsAttc.setFILE_NAME(formFile.getOriginalFilename());
		prpsAttc.setFILE_SIZE(formFile.getSize() + "");
		
		return prpsAttc;
	}
	
	public String GetCurrDateNumber() {
		java.util.TimeZone zone = java.util.TimeZone.getTimeZone("GMT+9:00");			
		java.util.Calendar c = java.util.Calendar.getInstance(zone);		

		String returnDate 	= 	"" +  c.get(java.util.Calendar.YEAR        );
		String currentMonth =  	"" + (c.get(java.util.Calendar.MONTH)  + 1 );
		String currentDay   =  	"" +  c.get(java.util.Calendar.DAY_OF_MONTH);
		
		returnDate			+= 	AddZeroFirst(currentMonth, 2);
		returnDate			+= 	AddZeroFirst(currentDay  , 2);

		return (returnDate) ;
	}
	
	public static String getYearMonth() {
		java.util.TimeZone zone = java.util.TimeZone.getTimeZone("GMT+9:00");			
		java.util.Calendar c = java.util.Calendar.getInstance(zone);		

		String returnDate 	= 	"" +  c.get(java.util.Calendar.YEAR        );
		String currentMonth =  	"" + (c.get(java.util.Calendar.MONTH)  + 1 );
		returnDate			+= 	AddZeroFirst(currentMonth, 2);
		return (returnDate) ;
	}

	public static String AddZeroFirst(String changeIntValue, int strNum) {
		if (strNum < 0) return ("Parameter two is wrong");
		int intValueLength = changeIntValue.trim().length();
		
		if (intValueLength >= strNum) {
			return (changeIntValue);
		} else {
			String returnValue = changeIntValue;
			for (int i = 0 ; i < (strNum - intValueLength) ; i++) {
				returnValue = "0" + returnValue;
			}
			return (returnValue);
		}
	}
	
	public static String getRelactionFilePath() {
	    return "/"+getYearMonth().substring(0,4)+"/"+getYearMonth().substring(4,6);
	}
	
	private static boolean isFolderFlag = false;
	
	public boolean isSaveFolder() {
	    return isFolderFlag;
	}
	
	
	/**
	 * 일반파일 저장경로
	 * @return
	 */
	public static String getAbsoluteFilePath(String fileUploadPath) {
		
		if(fileUploadPath.length()==0)
			fileUploadPath = realUploadPath;
		
	    String sYearMonth = getYearMonth();
	    String sSaveDirPath = fileUploadPath + sYearMonth.substring(0,4);
	    
		File mkdirFolder = new File(sSaveDirPath);
		if (!mkdirFolder.isDirectory())	{
		    isFolderFlag = mkdirFolder.mkdir();
			if (isFolderFlag==false) {
			   // log.error(sSaveDirPath+" 폴더생성 실패");
			    System.out.println(sSaveDirPath+" 폴더생성 실패");
			    return null;
			}
			else {
			   // log.info(sSaveDirPath+" 폴더생성");
			    System.out.println(sSaveDirPath+" 폴더생성");
			}			
		}
		else {
		    isFolderFlag = true;
		}
		
		sSaveDirPath = fileUploadPath + sYearMonth.substring(0,4)+"/"+sYearMonth.substring(4,6);
		mkdirFolder = new File(sSaveDirPath);
		
		if (!mkdirFolder.isDirectory())	{
		    isFolderFlag = mkdirFolder.mkdir();

			if (isFolderFlag==false) {
			    //log.error(sSaveDirPath+" 폴더생성 실패");
				 System.out.println(sSaveDirPath+" 폴더생성 실패");
			    return null;
			}
			else {
			   // log.info(sSaveDirPath+" 폴더생성");
				 System.out.println(sSaveDirPath+" 폴더생성");
			    return sSaveDirPath;
			}
		}
		else {
		    isFolderFlag = true;
		    return sSaveDirPath;
		}
	}

	
	  /**
     * 파일이 실재로 존재하는지 알아내는 함수
     *
     * @param   sFilePath 검사 하고자 하는 파일 경로
     * @return  파일이 존재하면 true, 아니면 false;
  */  
  public static boolean getIsFile(String sFilePath){
   	  boolean isFileOk = false;
   	  File fExfile = new File(sFilePath);
   	  isFileOk = fExfile.isFile();
   	  return isFileOk;
  }	
}
