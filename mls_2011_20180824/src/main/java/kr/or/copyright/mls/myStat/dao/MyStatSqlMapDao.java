package kr.or.copyright.mls.myStat.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.myStat.model.ApplySign;
import kr.or.copyright.mls.myStat.model.File;
import kr.or.copyright.mls.myStat.model.StatApplication;
import kr.or.copyright.mls.myStat.model.StatApplicationShis;
import kr.or.copyright.mls.myStat.model.StatApplyWorks;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

import com.softforum.xdbe.xCrypto;



@Repository("myStatDao")
public class MyStatSqlMapDao extends SqlMapClientDaoSupport implements MyStatDao {
	
    
	//이용승인 신청 현황 목록 및 total count 조회
	public int statRsltInqrListCount(StatApplication params) {
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("MyStat.statRsltInqrListCount", params));
	}
	public List statRsltInqrList(StatApplication params) {
		return getSqlMapClientTemplate().queryForList("MyStat.statRsltInqrList", params);
	}
	

	//신청서 신규 차수 조회
	public int newTmpApplyWriteSeq() {
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("MyStat.newTmpApplyWriteSeq"));
	}
	public int newApplyWriteSeq() {
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("MyStat.newApplyWriteSeq"));
	}
	public int newApplyWriteSeq2(String ymd){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("MyStat.newApplyWriteSeq2", ymd));
	}
	

	//이용승인 신청서tmp 등록
	public void insertStatApplicationTmp(StatApplication params) {

	    /*resd암호화 str 20121107 정병호*/
	    String applrResdCorpNumb = params.getApplrResdCorpNumb();//신청인주민번호 암호화하기
	    String applyProxyResdCorpNumb = params.getApplyProxyResdCorpNumb();//대리인주민번호 암호화하기
	    //xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
	    System.out.println( "applrResdCorpNumb ::"+applrResdCorpNumb );
	    System.out.println( "applyProxyResdCorpNumb ::"+applyProxyResdCorpNumb );
	    String applrResdCorpNumb7 = null;
	   	String applyProxyResdCorpNumb7 = null;
	    applrResdCorpNumb7 = xCrypto.Encrypt("pattern7", applrResdCorpNumb);
	    applyProxyResdCorpNumb7 = xCrypto.Encrypt("pattern7", applyProxyResdCorpNumb);
	//암호화
	    //applrResdCorpNumb7=applrResdCorpNumb;
	    //applyProxyResdCorpNumb7=applyProxyResdCorpNumb;
	    System.out.println( "applrResdCorpNumb7 ::" +applrResdCorpNumb7 );
	    System.out.println( "applyProxyResdCorpNumb7 ::" +applyProxyResdCorpNumb7 );    
	    
	    params.setApplrResdCorpNumb(applrResdCorpNumb7);
	    params.setApplyProxyResdCorpNumb(applyProxyResdCorpNumb7);
	    /*resd암호화 end 20121107 정병호*/
	    
	    getSqlMapClientTemplate().insert("MyStat.insertStatApplicationTmp", params);
		
	}
	//첨부파일 등록
	public void insertFile(List params) {
		
		for(int i = 0; i < params.size(); i++) {
			File fileInfo = (File)params.get(i);
			getSqlMapClientTemplate().insert("MyStat.insertFile", fileInfo);
		}
		
	}
	//첨부파일 이용승인 신청tmp 맵핑정보 등록
	public void insertStatAttcFileTmp(List params) {
		
		for(int i = 0; i < params.size(); i++) {
			File fileInfo = (File)params.get(i);
			getSqlMapClientTemplate().insert("MyStat.insertStatAttcFileTmp", fileInfo);
		}
		
	}
	//첨부파일 이용승인 신청tmp 맵핑정보 등록
	public void insertStatAttcFile(List params) {
		
		for(int i = 0; i < params.size(); i++) {
			File fileInfo = (File)params.get(i);
			getSqlMapClientTemplate().insert("MyStat.insertStatAttcFile2", fileInfo);
		}
		
	}

	//이용승인 신청서tmp 조회
	public StatApplication statApplicationTmp(StatApplication params) {
	    /*resd복호화 str 20121107 정병호*/
	    StatApplication statApplication = (StatApplication)getSqlMapClientTemplate().queryForObject("MyStat.statApplicationTmp", params);
	    
	    //xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
	    String applrResdCorpNumb = statApplication.getApplrResdCorpNumb();//신청인 주민번호(암호)
	    String applyProxyResdCorpNumb = statApplication.getApplyProxyResdCorpNumb();//대리인 주민번호(암호)
	    
	    String applrResdCorpNumb7 = null;
	    String applyProxyResdCorpNumb7 = null;
	    
	    String dummyApplrResdCorpNumb = null;//신청인 주민번호 *패턴
	    String dummyApplyProxyResdCorpNumb = null;//대리인 주민번호 *패턴
	    //암호화
	    applrResdCorpNumb7 = xCrypto.Decrypt("pattern7", applrResdCorpNumb);//신청인 주민번호(복호)
	    //applrResdCorpNumb7=applrResdCorpNumb;

	    //applrResdCorpNumb7 = applrResdCorpNumb7.replaceAll(" ", "");
	    statApplication.setApplrResdCorpNumb(applrResdCorpNumb7);//복호화셋팅
	    dummyApplrResdCorpNumb = applrResdCorpNumb7.substring(0, 7)+"*******";
	    statApplication.setDummyApplrResdCorpNumb(dummyApplrResdCorpNumb);
	    
	    if(applyProxyResdCorpNumb != null && !applyProxyResdCorpNumb.equals("")){
		applyProxyResdCorpNumb7 = xCrypto.Decrypt("pattern7", applyProxyResdCorpNumb);//대리인 주민번호(복호)
	         //암호화
	   //	applyProxyResdCorpNumb7=	applyProxyResdCorpNumb;
		applyProxyResdCorpNumb7 = applyProxyResdCorpNumb7.replaceAll(" ", "");
		statApplication.setApplyProxyResdCorpNumb(applyProxyResdCorpNumb7);//복호화셋팅
		dummyApplyProxyResdCorpNumb = applyProxyResdCorpNumb7.substring(0, 7)+"*******";
		statApplication.setDummyApplyProxyResdCorpNumb(dummyApplyProxyResdCorpNumb);
	    }
	  
	    return statApplication;
	    /*resd복호화 end 20121107 정병호*/
	}
	
	//이용승인 신청서 조회
	public StatApplication statApplication(StatApplication params) {
	    /*resd복호화 str 20121107 정병호*/
	    StatApplication statApplication = (StatApplication)getSqlMapClientTemplate().queryForObject("MyStat.statApplication", params);
	    //암호화
	    xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
	    String applrResdCorpNumb = statApplication.getApplrResdCorpNumb();//신청인 주민번호(암호)
	    String applyProxyResdCorpNumb = statApplication.getApplyProxyResdCorpNumb();//대리인 주민번호(암호)
	    
	    String applrResdCorpNumb7 = null;
	    String applyProxyResdCorpNumb7 = null;
	    
	    String dummyApplrResdCorpNumb = null;//신청인 주민번호 *패턴
	    String dummyApplyProxyResdCorpNumb = null;//대리인 주민번호 *패턴
	    //암호화
	    applrResdCorpNumb7 = xCrypto.Decrypt("pattern7", applrResdCorpNumb);//신청인 주민번호(복호)
	   // applrResdCorpNumb7=applrResdCorpNumb;
	    applrResdCorpNumb7 = applrResdCorpNumb7.replaceAll(" ", "");
	    statApplication.setApplrResdCorpNumb(applrResdCorpNumb7);//복호화셋팅
	    dummyApplrResdCorpNumb = applrResdCorpNumb7.substring(0, 7)+"*******";
	    statApplication.setDummyApplrResdCorpNumb(dummyApplrResdCorpNumb);
	    
	    if(applyProxyResdCorpNumb != null && !applyProxyResdCorpNumb.equals("")){
//암호화
	         applyProxyResdCorpNumb7 = xCrypto.Decrypt("pattern7", applyProxyResdCorpNumb);//대리인 주민번호(복호)
	        // applyProxyResdCorpNumb7=applyProxyResdCorpNumb;
		applyProxyResdCorpNumb7 = applyProxyResdCorpNumb7.replaceAll(" ", "");
		statApplication.setApplyProxyResdCorpNumb(applyProxyResdCorpNumb7);//복호화셋팅
		dummyApplyProxyResdCorpNumb = applyProxyResdCorpNumb7.substring(0, 7)+"*******";
		statApplication.setDummyApplyProxyResdCorpNumb(dummyApplyProxyResdCorpNumb);
	    }
	    /*
	    System.out.println("##########################");
	    System.out.println("##########################");
	    System.out.println("##########################");
	    System.out.println(statApplication.toString());
	    System.out.println("##########################");
	    System.out.println("##########################");
	    System.out.println("##########################");
	    */
	    return statApplication;
	    /*resd복호화 end 20121107 정병호*/
	}
	//이용승인 신청서 상태변경내역 조회
	public List statApplicationShisList(StatApplicationShis params) {
		return getSqlMapClientTemplate().queryForList("MyStat.statApplicationShisList", params);
	}
	//이용승인 신청서tmp 첨부파일 조회
	public List statAttcFileTmp(StatApplication params) {
		return getSqlMapClientTemplate().queryForList("MyStat.statAttcFileTmp", params);
	}
	
	//엑셀신청명세서 첨부파일 조회
	public File excelStatAttcFile(File params) {
		return (File)getSqlMapClientTemplate().queryForObject("MyStat.excelStatAttcFile", params);
	}
	
	//엑셀신청명세서 tmp 첨부파일 조회
	public File excelStatAttcFileTmp(File params) {
	    return (File)getSqlMapClientTemplate().queryForObject("MyStat.excelStatAttcFileTmp", params);
	}
	
	//이용승인 신청서 첨부파일 조회
	public List statAttcFile(StatApplication params) {
		return getSqlMapClientTemplate().queryForList("MyStat.statAttcFile", params);
	}

	//이용승인 신청서tmp 수정
	public void updateStatApplicationTmp(StatApplication params) {
	    /*resd암호화 str 20121107 정병호*/
	    String applrResdCorpNumb = params.getApplrResdCorpNumb();//신청인주민번호 암호화하기
	    String applyProxyResdCorpNumb = params.getApplyProxyResdCorpNumb();//대리인주민번호 암호화하기
	    xCrypto.RegisterEx("pattern7", 2, new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
	    String applrResdCorpNumb7 = null;
	    String applyProxyResdCorpNumb7 = null;
	    applrResdCorpNumb7 = xCrypto.Encrypt("pattern7", applrResdCorpNumb);
	    applyProxyResdCorpNumb7 = xCrypto.Encrypt("pattern7", applyProxyResdCorpNumb);
	    
	    params.setApplrResdCorpNumb(applrResdCorpNumb7);
	    params.setApplyProxyResdCorpNumb(applyProxyResdCorpNumb7);
	    /*resd암호화 end 20121107 정병호*/
	    
	    getSqlMapClientTemplate().insert("MyStat.updateStatApplicationTmp", params);
	}
	//이용승인 신청서 수정
	public void updateStatApplication(StatApplication params) {
	    /*resd암호화 str 20121107 정병호*/
	    String applrResdCorpNumb = params.getApplrResdCorpNumb();//신청인주민번호 암호화하기
	    String applyProxyResdCorpNumb = params.getApplyProxyResdCorpNumb();//대리인주민번호 암호화하기
	    xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
	    String applrResdCorpNumb7 = null;
	    String applyProxyResdCorpNumb7 = null;
	    applrResdCorpNumb7 = xCrypto.Encrypt("pattern7", applrResdCorpNumb);
	    applyProxyResdCorpNumb7 = xCrypto.Encrypt("pattern7", applyProxyResdCorpNumb);
	    
	    params.setApplrResdCorpNumb(applrResdCorpNumb7);
	    params.setApplyProxyResdCorpNumb(applyProxyResdCorpNumb7);
	    /*resd암호화 end 20121107 정병호*/
	    getSqlMapClientTemplate().insert("MyStat.updateStatApplication", params);
	}
	//이용승인 신청서 수정 진행상태
	public void updateStatApplicationStat(StatApplication params) {
	    getSqlMapClientTemplate().insert("MyStat.updateStatApplicationStat", params);
	}
	//첨부파일 삭제
	public void deleteFile(List params) {
		
		for(int i = 0; i < params.size(); i++) {
			File fileInfo = (File)params.get(i);
			getSqlMapClientTemplate().insert("MyStat.deleteFile", fileInfo);
		}
		
	}
	//첨부파일 이용승인 신청tmp 맴핑정보 삭제
	public void deleteStatAttcFileTmp(List params) {
		
		for(int i = 0; i < params.size(); i++) {
			File fileInfo = (File)params.get(i);
			getSqlMapClientTemplate().insert("MyStat.deleteStatAttcFileTmp", fileInfo);
		}
		
	}
	//첨부파일 이용승인 신청tmp 맴핑정보 삭제
	public void deleteStatAttcFile(List params) {
		
		for(int i = 0; i < params.size(); i++) {
			File fileInfo = (File)params.get(i);
			getSqlMapClientTemplate().insert("MyStat.deleteStatAttcFile", fileInfo);
		}
		
	}

	//이용승인 명세서tmp 삭제
	public void deleteStatApplyWorksTmp(StatApplyWorks params) {
		
		getSqlMapClientTemplate().delete("MyStat.deleteStatApplyWorksTmp", params);
	}
	
	//이용승인 명세서tmp seq삭제
	public void deleteWorksSeqnStatApplyWorksTmp(List params){
	    for(int i = 0; i < params.size(); i++) {
		StatApplyWorks paramInfo = (StatApplyWorks)params.get(i);
		getSqlMapClientTemplate().delete("MyStat.deleteWorksSeqnStatApplyWorksTmp", paramInfo);
	    }
	}
	
	//이용승인 선택 명세서tmp 등록
	public void insertStatApplyWorksTmp(List params) {
		for(int i = 0; i < params.size(); i++) {
			StatApplyWorks paramInfo = (StatApplyWorks)params.get(i);
			getSqlMapClientTemplate().insert("MyStat.insertStatApplyWorksTmpS", paramInfo);
		}
	}
	
	//이용승인 개인명세서tmp 등록
	public void insertStatApplyWorksTmp_P(List params) {
	    for(int i = 0; i < params.size(); i++) {
		StatApplyWorks paramInfo = (StatApplyWorks)params.get(i);
		getSqlMapClientTemplate().insert("MyStat.insertStatApplyWorksTmp", paramInfo);
	    }
	}
	
	//이용승인 명세서 삭제
	public void deleteStatApplyWorks(StatApplyWorks params) {
		getSqlMapClientTemplate().delete("MyStat.deleteStatApplyWorks", params);
	}
	
	//이용승인 명세서 수정
	public void updateStatApplyWorks(StatApplyWorks params){
	    getSqlMapClientTemplate().update("MyStat.updateStatApplyWorks", params);
	}
	
	//이용승인 명세서 seq삭제
	public void deleteWorksSeqnStatApplyWorks(List params){
	    for(int i = 0; i < params.size(); i++) {
		StatApplyWorks paramInfo = (StatApplyWorks)params.get(i);
		getSqlMapClientTemplate().delete("MyStat.deleteWorksSeqnStatApplyWorks", paramInfo);
	    }
	}
	
	//이용승인 명세서 등록
	public void insertStatApplyWorks(List params) {
		
		for(int i = 0; i < params.size(); i++) {
			StatApplyWorks paramInfo = (StatApplyWorks)params.get(i);
			getSqlMapClientTemplate().insert("MyStat.insertStatApplyWorks2", paramInfo);
		}
		
	}

	//이용승인 신청서 등록
	public void insertStatApplication(StatApplication params) {
		getSqlMapClientTemplate().insert("MyStat.insertStatApplication", params);
	}
	//이용승인 신청서SHIS 등록
	public void insertStatApplicationShis(StatApplication params) {
		getSqlMapClientTemplate().insert("MyStat.insertStatApplicationShis", params);
	}
	//첨부파일 이용승인 신청 맵핑정보 등록
	public void insertStatAttcFile(StatApplication params) {
		getSqlMapClientTemplate().insert("MyStat.insertStatAttcFile", params);
	}
	//이용승인 명세서 등록
	public void insertStatApplyWorks(StatApplication params) {
		getSqlMapClientTemplate().insert("MyStat.insertStatApplyWorks", params);
	}
	//이용승인 신청 인증서정보 등록
	public void insertApplySign(ApplySign params) {
		getSqlMapClientTemplate().insert("MyStat.insertApplySign", params);
	}
	//이용승인 신청 TMP 삭제
	public void deleteStatPrpsTmp(StatApplication params) {
		getSqlMapClientTemplate().delete("MyStat.deleteStatApplyWorksTmp2", params);
		getSqlMapClientTemplate().delete("MyStat.deleteStatAttcFileTmp2", params);
		getSqlMapClientTemplate().delete("MyStat.deleteStatApplicationTmp", params);
	}
	//이용승인 명세서tmp 조회
	public List statApplyWorksListTmp(StatApplyWorks params) {
		return getSqlMapClientTemplate().queryForList("MyStat.statApplyWorksListTmp", params);
	}
	//이용승인 명세서 조회
	public List statApplyWorksList(StatApplyWorks params) {
		return getSqlMapClientTemplate().queryForList("MyStat.statApplyWorksList", params);
	}
	
	// 법정허락 결제정보 등록
	public void insertStatPayStatus(StatApplication params){
		getSqlMapClientTemplate().insert("MyStat.insertStatPayStatus", params);
	}
	
	// 법정허락 결제정보 수정
	public void updateStatPayStatus(StatApplication params){
		getSqlMapClientTemplate().update("MyStat.updateStatPayStatus", params);
	}
	
	// 이용승인신청명세서 임시저장 카운트 20120906 정병호
	public int getStatApplyWorksCount(StatApplication params) {
	    return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("MyStat.getStatApplyWorksCount", params));
	}
}