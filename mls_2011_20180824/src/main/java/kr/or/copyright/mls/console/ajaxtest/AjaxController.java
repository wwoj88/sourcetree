package kr.or.copyright.mls.console.ajaxtest;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AjaxController{
	
	@RequestMapping(value = "/console/ajaxTest/ajaxTest.page")
	public String ajaxTestMethod( ModelMap model,Map<String, Object> commandMap,HttpServletRequest request,HttpServletResponse response){
		System.out.println( "ajaxTest Call" );
		return null;
	}
}
