package kr.or.copyright.mls.console.email;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 안내메일관리 > 메일폼 관리
 * 
 * @author ljh
 */
@Controller
public class FdcrAdA10Controller extends DefaultController{

	@Resource( name = "fdcrAdA10Service" )
	private FdcrAdA10ServiceImpl fdcrAdA10Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA10Controller.class );

	/**
	 * 메일폼관리 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA10List1.page" )
	public String fdcrAdA10List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		return "email/fdcrAdA10List1.tiles";
	}
	
	/**
	 * 메일폼 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA10View1.page" )
	public String fdcrAdA10View1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA10View1 Start" );
		Map<String, Object> form = fdcrAdA10Service.selectFormInfo( commandMap );
		model.addAttribute( "form", form );
		model.addAttribute( "commandMap", commandMap );
		return "email/fdcrAdA10View1";
	}
	
	/**
	 * 메뉴 수정
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA10Update1.page" )
	public void fdcrAdA10Update1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA9Update1 Start" );
		//AJAX 한글 인코딩 처리
		EgovWebUtil.ajaxCommonEncoding(request,commandMap);
		
		commandMap.put( "MODI_IDNT", ConsoleLoginUser.getUserId() );
		boolean isSuccess = fdcrAdA10Service.formUpdate( commandMap );
		if( isSuccess ){
			returnAjaxString( response, EgovWebUtil.getToInt( "MENU_ID" ) + "" );
		}else{
			returnAjaxString( response, isSuccess );
		}
	}
	
	/**
	 * 메뉴 조회
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA10WriteForm1.page" )
	public String fdcrAdA10WriteForm1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA10WriteForm1 Start" );
		Map<String, Object> form = fdcrAdA10Service.selectFormInfo( commandMap );
		model.addAttribute( "form", form );
		model.addAttribute( "commandMap", commandMap );
		return "email/fdcrAdA10WriteForm1";
	}
	
	/**
	 * 메뉴 등록
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA10Write1.page" )
	public void fdcrAdA10Write1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA10Write1 Start" );
		//AJAX 한글 인코딩 처리
		EgovWebUtil.ajaxCommonEncoding(request,commandMap);
		
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		commandMap.put( "MODI_IDNT", ConsoleLoginUser.getUserId() );
		boolean isSuccess = fdcrAdA10Service.formInsert( commandMap );
		if( isSuccess ){
			returnAjaxString( response, EgovWebUtil.getToInt(commandMap, "MENU_ID" ) + "" );
		}else{
			returnAjaxString( response, isSuccess );
		}
	}
	
	/**
	 * 메뉴 삭제
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA10Delete1.page" )
	public void fdcrAdA10Delete1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA10Delete1 Start" );
		//AJAX 한글 인코딩 처리
		EgovWebUtil.ajaxCommonEncoding(request,commandMap);
		
		commandMap.put( "MODI_IDNT", ConsoleLoginUser.getUserId() );
		boolean isSuccess = fdcrAdA10Service.formDelete( commandMap );
		returnAjaxString( response, isSuccess );
	}
	
	
	/**
	 * 메일폼 트리
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA10List1Sub1.page" )
	public void fdcrAdA10List1Sub1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception {
		
		logger.debug( "fdcrAdA9List1Sub1 Start" );
		ArrayList<Map<String, Object>> treeList = new ArrayList<Map<String, Object>>();
		Map<String, Object> rootTree = new HashMap<String, Object>();
		rootTree.put( "id", "formId_0" );
		rootTree.put( "text", "전체보기" );
		rootTree.put( "icon", "folder" );
		
		Map<String, Object> rootTreeState = new HashMap<String, Object>();
		rootTreeState.put( "opened", true );
		rootTree.put( "state", rootTreeState );
		treeList.add( rootTree );
		
		try{
			ArrayList<Map<String, Object>> formList =
				(ArrayList<Map<String, Object>>) fdcrAdA10Service.selectFormGroupList( commandMap );
			for( int i = 0; i < formList.size(); i++ ){
				Map<String, Object> map = formList.get( i );
				String formId = ( (BigDecimal) map.get( "FORM_ID" ) ).intValue() + "";
				String upperFormId = ( (BigDecimal) map.get( "PARENT_FORM_ID" ) ).intValue() + "";
				String formNm = (String) map.get( "FORM_TITLE" );
				int childCnt = EgovWebUtil.getToInt( map, "CHILD_CNT" );

				Map<String, Object> tree = new HashMap<String, Object>();
				tree.put( "id", "formId_" + formId );
				tree.put( "text", formNm );
				Map<String, Object> treeState = new HashMap<String, Object>();
				treeState.put( "opened", true );
				
				if( childCnt > 0 ){
					tree.put( "icon", "folder" );
				}else{
					tree.put( "icon", "file" );
				}
				
				tree.put( "state", treeState );
				tree.put( "parent", "formId_" + upperFormId );
				setChildNode( treeList, tree );
			}
		}
		catch( RuntimeException e ){
			// e.printStackTrace();
			System.out.println( "실행중 에러발생 " );
		}
		returnAjaxJsonArray( response, treeList );
	}
	
	public void setChildNode( ArrayList<Map<String, Object>> list,
		Map<String, Object> node ){
		String parent = (String) node.get( "parent" );

		for( int i = 0; i < list.size(); i++ ){
			Map<String, Object> compareMap = list.get( i );
			String compMenuId = (String) compareMap.get( "id" );
			ArrayList<Map<String, Object>> compChildList = (ArrayList<Map<String, Object>>) compareMap.get( "children" );

			if( compMenuId.equals( parent ) ){
				ArrayList<Map<String, Object>> childList = (ArrayList<Map<String, Object>>) compareMap.get( "children" );
				if( null != childList && childList.size() > 0 ){
					childList.add( node );
				}else{
					childList = new ArrayList<Map<String, Object>>();
					childList.add( node );
				}
				compareMap.put( "children", childList );
			}else{
				if( null != compChildList ){
					setChildNode( compChildList, node );
				}
			}
		}
	}
	
}
