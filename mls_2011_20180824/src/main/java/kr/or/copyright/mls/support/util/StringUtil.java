package kr.or.copyright.mls.support.util;

import java.util.Random;

import kr.or.copyright.mls.support.constant.Constants;

/************************************************************************************************
* 작성자  구  분  작성(수정)일   내용 요약(추가/수정사항 포함)         관련 Class/method       
*************************************************************************************************
* 김효상  최초작성  2006,08,16   1.isEmpty : 공백 검사
*                             2. nullToEmpty : null값 공백치환
*                             3. nullToEmptyChk : checkbox null값 "N"으로 치환                            
*************************************************************************************************/

public class StringUtil{
	
/**
 * null 또는 "" 공백 검사.
 * @param String
 * @return boolean
 */
    public static boolean isEmpty(String str){
        if (str == null || str.length() < 1) return true;

        return false;
    }  
/**
 * null 값을 "" 공백 문자로 바꿔준다.
 * @param String
 * @return String
 */
    public static String nullToEmpty(String str) {
    	String strVal = str;
        if(strVal == null || strVal.trim().equals("")) {
            return "";
        }
        return strVal;
    }	
    public static String[] nullToEmptyArray(String[] str) {
    	String[] strVal = str;
        if(strVal == null) {
        	strVal = new String[0];
            return strVal;
        }
        return strVal;
    }
	/**
	 * null 값을 0 문자로 바꿔준다.
	 * @param String
	 * @return String
	 */
    public static long nullToZero(String str) {
        if(str == null || str.trim().equals("")) {
            return 0;
        }
        return Long.parseLong(str);
    }	
    
	/**
	 * null 값을 0 문자로 바꿔준다.
	 * @param String
	 * @return String
	 */
    public static int nullToZeroInt(String str) {
        if(str == null || str.trim().equals("")) {
        	System.out.println( "STR :::"+ str);
            return 0;
        }
        return Integer.parseInt(str);
    }	

    
	/**
	 * null 값을 끝 일자로 바꿔준다. (2999-12-31)
	 * @param String
	 * @return String
	 */
    public static String nullToEndDate(String str) {
        if(str == null) {
            return "2999-12-31";
        }
        return str;
    }	

	/**
	 * null 값을 "N" 문자로 바꿔준다.
	 * @param String
	 * @return String
	 */
    public static String nullToEmptyChk(String str) {
        if(str == null || !str.equals(Constants.YES)) {
            return Constants.NO;
        }
        return str;
    }
    /**
     * 파일경로에서 확장자만 추출해서 반환한다.
     * @param String
     * @return String
     */
    public static String getFileExt(String filePath) {
        int index = filePath.lastIndexOf(".");

        if (index == -1) {
            return "";
        }

        return filePath.substring(index + 1, filePath.length());
    }
    
    /**
     * "-"를 제거해준다.
     * @param String
     * @return String
     */
    
    public static String removeDash(String s){
        if ( s == null )
        {
            return null;
        }

        if ( s.indexOf("-") != -1 )
        {
            StringBuffer buf = new StringBuffer();

            for(int i=0;i<s.length();i++)
            {
                char c = s.charAt(i);
                if ( c != '-')
                {
                    buf.append(c);
                } 
            }
            return buf.toString();
        } 
        return s;
    }
    
    /**
     * 개행문자 <BR>붙이기
     * @param String
     * @return String
     */
    public static String appendHtmlBr(String comment)
    {
       int length = comment.length();
    
       StringBuffer buffer = new StringBuffer();
         if(comment.indexOf("<table") == -1){    
           for (int i = 0; i < length; ++i){
              String comp = comment.substring(i, i+1);
              
              if ("\r".compareTo(comp) == 0){
                 comp = comment.substring(++i, i+1);
                 if ("\n".compareTo(comp) == 0){
                    buffer.append("<BR>\r");
                 }
                 else{
                    buffer.append("\r");
                 }
              }
    
              buffer.append(comp);
           }
         }
		 else{
			 return comment;
		 }

       return buffer.toString();
    }
    
    /**
     * 암호화 인코딩
     * @param String
     * @return String
     */
    public static String encoding(String pwd) throws Exception{     
        int i = 0;
            int emp_no1 = 0;
            int emp_no2 = 0;
            int emp_no3 = 0;
            int result1 = 0;
            byte ch ;       
            String result = "";
              
        try{ 
            int ccc = pwd.length();
            for (i=0;i<3;i++ ){           
                    ch  = (byte)pwd.charAt(i);        
                
                    if(i==0){
                        switch(ch){
                            case 0: 
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                emp_no1 = (int)ch;
                                break ;
                            default : 
                                emp_no1 = 4;
                                break;
                        }
                    }
                    else if(i==1){
                        switch(ch){
                            case 0: 
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                emp_no2 = 5;
                                break;
                            default : 
                                emp_no2 = 3;
                                break;
                        }
                    }
                    else if(i==2){
                        switch(ch){
                            case 0: 
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                emp_no1 = (int)ch;
                               break;
                            default : 
                                emp_no1 = 2;
                               break;
                        }
                    }           
            }       

            for(i = 0; i<ccc; i++){
                  int aaa = 0;
                  aaa = i%6;
                  char ddd = pwd.charAt(i); 

                  switch(aaa){
                     case 0:
                        result1 = (int)ddd - emp_no2;
                        result = result + (char)result1;
                        break;
                     case 1:
                        result1 = (int)ddd - emp_no3;
                        result = result + (char)result1;
                        break;
                            
                    case 2:
                        result1 = (int)ddd - emp_no1;
                        result = result + (char)result1;
                        break;      
                        
                    case 3:
                        result1 = (int)ddd - emp_no3 - emp_no2;
                        result = result + (char)result1;
                        break;      
                    case 4:
                        result1 = (int)ddd - emp_no1 - emp_no3;
                        result = result + (char)result1;
                        break;                   
                    case 5:
                        result1 = (int)ddd - emp_no1 - emp_no2;
                        result = result + (char)result1;
                        break;  
                 }                  
            }       
            
        }       
        catch (Exception e){
            System.out.println("** Error in encoding : " + e.getMessage());
        }
        return result;
    } 
    
    /**
     * 암호화 디코딩
     * @param String
     * @return String
     */
    public static String decoding(String pwd) throws Exception{     
        int i = 0;
            int emp_no1 = 0;
            int emp_no2 = 0;
            int emp_no3 = 0;
            int result1 = 0;
            byte ch ;       
            String result = "";
              
        try{ 
            int ccc = pwd.length();
            for (i=0;i<3;i++ ){           
                    ch  = (byte)pwd.charAt(i);        
                
                    if(i==0){
                        switch(ch)
                        {
                            case 0: 
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                emp_no1 = (int)ch;
                                break ;
                            default : 
                                emp_no1 = 4;
                                break;
                        }
                    }
                    else if(i==1){
                        switch(ch){
                            case 0: 
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                emp_no2 = 5;
                                break;
                            default : 
                                emp_no2 = 3;
                                break;
                        }
                    }
                    else if(i==2){
                        switch(ch){
                            case 0: 
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                emp_no1 = (int)ch;
                               break;
                            default : 
                                emp_no1 = 2;
                               break;
                        }
                    }           
            }       

            for(i = 0; i<ccc; i++){
                  int aaa = 0;
                  aaa = i%6;
                  char ddd = pwd.charAt(i); 

                  switch(aaa){
                     case 0:
                        result1 = (int)ddd + emp_no2;
                        result = result + (char)result1;
                        break;
                     case 1:
                        result1 = (int)ddd + emp_no3;
                        result = result + (char)result1;
                        break;
                            
                    case 2:
                        result1 = (int)ddd + emp_no1;
                        result = result + (char)result1;
                        break;      
                        
                    case 3:
                        result1 = (int)ddd + emp_no3 + emp_no2;
                        result = result + (char)result1;
                        break;      
                    case 4:
                        result1 = (int)ddd + emp_no1 + emp_no3;
                        result = result + (char)result1;
                        break;                   
                    case 5:
                        result1 = (int)ddd + emp_no1 + emp_no2;
                        result = result + (char)result1;
                        break;  
                 }                  
            }       
            
        }       
        catch (Exception e){
            System.out.println("** Error in encoding : " + e.getMessage());
        }
        return result;
    }
    
	public int[] getRandomNum(int cnt){
	    int[] arrNum = new int[cnt];

	    Random random = new Random();
	    for(int i=0;i<cnt; i++){
	        int c=random.nextInt(9);
	        arrNum[i] = c;
	     
	    }
	    return arrNum;
	}
	
	public String change_en_to_kr(String str)
    {
        try
        {
            str = new String(str.getBytes("ISO8859-1"), "KSC5601");
        }
        catch(Exception exception) { }
        return str;
    }

    public String change_kr_to_en(String str)
    {
        try
        {
            str = new String(str.getBytes("KSC5601"), "ISO8859-1"); 
        }
        catch(Exception exception) { }
        return str;
    }
    
}