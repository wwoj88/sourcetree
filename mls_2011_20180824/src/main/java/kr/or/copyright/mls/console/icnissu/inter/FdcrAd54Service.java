package kr.or.copyright.mls.console.icnissu.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd54Service{

	/**
	 * 도서저작물 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd54List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 도서저작물 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd54View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 도서저작물 CLMS전송 업데이트
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd54Update1( Map<String, Object> commandMap ) throws Exception;

}
