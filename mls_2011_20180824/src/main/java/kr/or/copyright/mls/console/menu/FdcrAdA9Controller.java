package kr.or.copyright.mls.console.menu;

import java.math.BigDecimal;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.menu.inter.FdcrAdA9Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 메뉴관리 > 메뉴관리
 * 
 * @author wizksy
 */
@Controller
public class FdcrAdA9Controller extends DefaultController{

	@Resource( name = "fdcrAdA9Service" )
	private FdcrAdA9Service fdcrAdA9Service;

	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA9Controller.class );

	/**
	 * 메뉴 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdA9List1.page" )
	public String fdcrAdA9List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA9List1 Start" );
		return "menu/fdcrAdA9List1.tiles";
	}

	/**
	 * 메뉴 트리
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdA9List1Sub1.page" )
	public void fdcrAdA9List1Sub1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA9List1Sub1 Start" );
		ArrayList<Map<String, Object>> treeList = new ArrayList<Map<String, Object>>();
		Map<String, Object> rootTree = new HashMap<String, Object>();
		rootTree.put( "id", "menuId_0" );
		rootTree.put( "text", "전체보기" );
		rootTree.put( "icon", "folder" );
		Map<String, Object> rootTreeState = new HashMap<String, Object>();
		rootTreeState.put( "opened", true );
		rootTree.put( "state", rootTreeState );
		treeList.add( rootTree );
		
		try{
			ArrayList<Map<String, Object>> menuList =
				(ArrayList<Map<String, Object>>) consoleCommonService.selectMenuGroupList( commandMap );
			for( int i = 0; i < menuList.size(); i++ ){
				Map<String, Object> map = menuList.get( i );
				String menuId = ( (BigDecimal) map.get( "MENU_ID" ) ).intValue() + "";
				String upperMenuId = ( (BigDecimal) map.get( "PARENT_MENU_ID" ) ).intValue() + "";
				String menuNm = (String) map.get( "MENU_TITLE" );
				int childCnt = EgovWebUtil.getToInt( map, "CHILD_CNT" );

				Map<String, Object> tree = new HashMap<String, Object>();
				tree.put( "id", "menuId_" + menuId );
				tree.put( "text", menuNm );
				Map<String, Object> treeState = new HashMap<String, Object>();
				treeState.put( "opened", true );
				if( childCnt > 0 ){
					tree.put( "icon", "folder" );
				}else{
					tree.put( "icon", "file" );
				}
				// if("11001".equals( menuId )){
				// treeState.put( "selected", true );
				// }
				tree.put( "state", treeState );
				tree.put( "parent", "menuId_" + upperMenuId );
				setChildNode( treeList, tree );
			}
		}
		catch( RuntimeException e ){
			// e.printStackTrace();
			System.out.println( "실행중 에러발생 " );
		}
		returnAjaxJsonArray( response, treeList );
	}

	/**
	 * 메뉴 조회
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdA9View1.page" )
	public String fdcrAdA9View1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA9View1 Start" );
		Map<String, Object> menu = fdcrAdA9Service.selectMenuInfo( commandMap );
		model.addAttribute( "menu", menu );
		model.addAttribute( "commandMap", commandMap );
		return "menu/fdcrAdA9View1";
	}

	/**
	 * 메뉴 조회
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdA9WriteForm1.page" )
	public String fdcrAdA9WriteForm1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA9View1 Start" );
		Map<String, Object> menu = fdcrAdA9Service.selectMenuInfo( commandMap );
		model.addAttribute( "menu", menu );
		model.addAttribute( "commandMap", commandMap );
		return "menu/fdcrAdA9WriteForm1";
	}

	/**
	 * 메뉴 등록
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdA9Write1.page" )
	public void fdcrAdA9Write1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA9Write1 Start" );
		//AJAX 한글 인코딩 처리
		EgovWebUtil.ajaxCommonEncoding(request,commandMap);
		
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		commandMap.put( "MODI_IDNT", ConsoleLoginUser.getUserId() );
		boolean isSuccess = fdcrAdA9Service.fdcrAdA9Write1( commandMap );
		if( isSuccess ){
			returnAjaxString( response, EgovWebUtil.getToInt(commandMap, "MENU_ID" ) + "" );
		}else{
			returnAjaxString( response, isSuccess );
		}
	}

	/**
	 * 메뉴 수정
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdA9Update1.page" )
	public void fdcrAdA9Update1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA9Update1 Start" );
		//AJAX 한글 인코딩 처리
		EgovWebUtil.ajaxCommonEncoding(request,commandMap);
		
		commandMap.put( "MODI_IDNT", ConsoleLoginUser.getUserId() );
		boolean isSuccess = fdcrAdA9Service.fdcrAdA9Update1( commandMap );
		if( isSuccess ){
			returnAjaxString( response, EgovWebUtil.getToInt( "MENU_ID" ) + "" );
		}else{
			returnAjaxString( response, isSuccess );
		}
	}
	
	/**
	 * 메뉴 삭제
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdA9Delete1.page" )
	public void fdcrAdA9Delete1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdA9Delete1 Start" );
		//AJAX 한글 인코딩 처리
		EgovWebUtil.ajaxCommonEncoding(request,commandMap);
		
		commandMap.put( "MODI_IDNT", ConsoleLoginUser.getUserId() );
		boolean isSuccess = fdcrAdA9Service.fdcrAdA9Delete1( commandMap );
		returnAjaxString( response, isSuccess );
	}

	public void setChildNode( ArrayList<Map<String, Object>> list,
		Map<String, Object> node ){
		String parent = (String) node.get( "parent" );

		for( int i = 0; i < list.size(); i++ ){
			Map<String, Object> compareMap = list.get( i );
			String compMenuId = (String) compareMap.get( "id" );
			ArrayList<Map<String, Object>> compChildList = (ArrayList<Map<String, Object>>) compareMap.get( "children" );

			if( compMenuId.equals( parent ) ){
				ArrayList<Map<String, Object>> childList = (ArrayList<Map<String, Object>>) compareMap.get( "children" );
				if( null != childList && childList.size() > 0 ){
					childList.add( node );
				}else{
					childList = new ArrayList<Map<String, Object>>();
					childList.add( node );
				}
				compareMap.put( "children", childList );
			}else{
				if( null != compChildList ){
					setChildNode( compChildList, node );
				}
			}
		}
	}
}
