package kr.or.copyright.mls.console.cnsgn;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.cnsgn.inter.AdminWorksRegistation;
import kr.or.copyright.mls.console.cnsgn.inter.FdcrAd58Dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tobesoft.platform.data.Dataset;

@Repository( "fdcrAd58Dao" )
public class FdcrAd58DaoImpl extends EgovComAbstractDAO implements FdcrAd58Dao{

	public HashMap getMap( Dataset ds,
		int rnum ) throws Exception{

		if( ds == null || ds.getRowCount() == 0 ){
			return null;
		}
		HashMap result = new HashMap();

		for( int i = 0; i < ds.getColumnCount(); i++ ){

			String columnName = ds.getColumnId( i );

			result.put( columnName, ds.getColumnAsObject( rnum, columnName ) );
		}

		return result;
	}

	public List reportListTotalRowMusic( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20150601");
		// map.put("E_DATE", "20150701");
		return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportListTotalRowMusic", map );
	}

	public List reportListTotalRowMusic2( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20150601");
		// map.put("E_DATE", "20150701");
		return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportListTotalRowMusic2", map );
	}

	public List reportListMusic( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20150601");
		// map.put("E_DATE", "20150701");
		return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportListMusic", map );
	}

	@SuppressWarnings( "unchecked" )
	public ArrayList<Map<String, Object>> reportListTotalRowBook( Map map ) throws SQLException{
		List report_List_date = new ArrayList();
		//report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		//Map map2 = (Map) report_List_date.get( 0 );
		//map.put( "F_DATE", map2.get( "F_DATE" ) );
		//map.put( "E_DATE", map2.get( "E_DATE" ) );
		 map.put("F_DATE", "20150601");
		 map.put("E_DATE", "20150701");
		//return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportListTotalRowBook", map );
		return (ArrayList<Map<String, Object>>) list( "AdminWorksRegistration.reportListTotalRowBook", map );
	}
	
	@SuppressWarnings( "unchecked" )
	public ArrayList<Map<String, Object>> reportListTotalRowBook2( Map map ) throws SQLException{
		List report_List_date = new ArrayList();
		//report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		//Map map2 = (Map) report_List_date.get( 0 );
		//map.put( "F_DATE", map2.get( "F_DATE" ) );
		//map.put( "E_DATE", map2.get( "E_DATE" ) );
		 map.put("F_DATE", "20150601");
		 map.put("E_DATE", "20150701");
		//return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportListTotalRowBook2", map );
		 return (ArrayList<Map<String, Object>>) list( "AdminWorksRegistration.reportListTotalRowBook2", map );
	}

	@SuppressWarnings( "unchecked" )
	public ArrayList<Map<String, Object>> reportListBook( Map map ) throws SQLException{
		List report_List_date = new ArrayList();
		//report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		//Map map2 = (Map) report_List_date.get( 0 );
		//map.put( "F_DATE", map2.get( "F_DATE" ) );
		//map.put( "E_DATE", map2.get( "E_DATE" ) );
		 map.put("F_DATE", "20150601");
		 map.put("E_DATE", "20150701");
		//return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportListBook", map );
		 return (ArrayList<Map<String, Object>>) list( "AdminWorksRegistration.reportListBook", map );
	}

	public List reportListAgentTotalRow( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20150601");
		// map.put("E_DATE", "20150701");
		return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportListAgentTotalRow", map );
	}

	public List reportListAgentTotalRow2( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20150601");
		// map.put("E_DATE", "20150701");
		return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportListAgentTotalRow2", map );
	}

	public List reportListAgent( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20150601");
		// map.put("E_DATE", "20150701");
		return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportListAgent", map );
	}

	public List getYearMonth() throws Exception{
		return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.getYearMonth", null );
	}

	public void targetReflash( AdminWorksRegistation dto ) throws Exception{
		getSqlMapClientTemplate().delete( "AdminWorksRegistration.targetReflash", dto );
	}

	public void targetInsertMusic( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20150601");
		// map.put("E_DATE", "20150701");
		getSqlMapClientTemplate().insert( "AdminWorksRegistration.targetInsertMusic", map );
	}

	public List targetMusicList( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20150601");
		// map.put("E_DATE", "20150701");
		return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.targetMusicList", map );
	}

	public void targetDeleteMusic( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20150601");
		// map.put("E_DATE", "20150701");
		getSqlMapClientTemplate().delete( "AdminWorksRegistration.targetDeleteMusic", map );
	}

	public void commWorksIdDelete( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20150601");
		// map.put("E_DATE", "20150701");
		getSqlMapClientTemplate().delete( "AdminWorksRegistration.commWorksIdDelete", map );

	}

	public int adminCommWorksReportInsert( Map map ) throws Exception{

		int rFlag = 0;

		try{
			rFlag =
				(Integer) getSqlMapClientTemplate().insert( "AdminWorksRegistration.adminCommWorksReportInsert", map );
		}
		catch( Exception e ){
			rFlag = 0;
		}

		return rFlag;
	}

	public int getExistCommWorks( Map map ) throws Exception{
		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminWorksRegistration.getExistAdminCommWorks", map );
	}

	public int getCommWorksId() throws Exception{
		int worksId =
			Integer.parseInt( "" + getSqlMapClientTemplate().queryForObject( "AdminWorksRegistration.getMlCommWorksId" ) );
		return worksId;
	}

	public String commWorksInsertBacth( List list ) throws Exception{
		return (String) getSqlMapClientTemplate().insert( "AdminWorksRegistration.mlCommWorksInsertBacth", list );
	}

	public String commMusicInsertBacth( List list ) throws Exception{
		return (String) getSqlMapClientTemplate().insert( "AdminWorksRegistration.mlCommMusicInsertBacth", list );
	}

	public void adminCommWorksDelete( Map map ) throws Exception{
		getSqlMapClientTemplate().delete( "AdminWorksRegistration.adminMlCommWorksDelete", map );
	}

	public boolean adminCommWorksInsert( Map map ) throws Exception{

		boolean cFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminWorksRegistration.adminCommWorksInsert", map );
			cFlag = true;
		}
		catch( Exception e ){
			cFlag = false;
		}

		return cFlag;
	}

	public boolean adminCommMusicInsert( Map map ) throws Exception{

		boolean gFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminWorksRegistration.adminMiCommMusicInsert", map );
			gFlag = true;
		}
		catch( Exception e ){
			gFlag = false;
		}

		return gFlag;
	}

	@Transactional( readOnly = false )
	public void adminCommWorksReportUpdate( Map map ) throws Exception{
		getSqlMapClientTemplate().update( "AdminWorksRegistration.adminCommWorksReportUpdate", map );
	}

	public List adminCommMusicUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> ds_target ) throws Exception{
		List resultList = new ArrayList();

		int rFlag = 0;// 리포트FLAG&REPT_SEQN
		boolean cFlag = true;// 저작물FLAG
		boolean gFlag = true;// 장르별저작물FLAG

		int cnt = 0; // 수정 성공 cnt

		GregorianCalendar d = new GregorianCalendar();

		// 보고연월
		String reptYmdTmp =
			String.valueOf( d.get( Calendar.YEAR ) )
				+ String.valueOf( ( ( d.get( Calendar.MONTH ) + 1 ) < 10 ) ? "0"
					+ String.valueOf( d.get( Calendar.MONTH ) + 1 ) : String.valueOf( d.get( Calendar.MONTH ) + 1 ) );

		String reptYmd = reptYmdTmp.replaceAll( " ", "" );

		// 장르코드
		String genreCd = (String) commandMap.get( "GENRE_CODE" );

		// 등록자아이디
		String rgstIdnt = (String) commandMap.get( "USER_IDNT" );

		// 위탁관리기관ID
		String trstOrgnCode = (String) commandMap.get( "TRST_ORGN_CODE" );

		commandMap.put( "REPT_YMD", reptYmd );// 보고연월
		commandMap.put( "GENRE_CD", genreCd );// 장르코드
		commandMap.put( "MODI_IDNT", rgstIdnt );// 등록자아이디

		rFlag = adminCommWorksReportInsert( commandMap );

		if( rFlag == 0 ){

			for( int i = 0; i < ds_target.size(); i++ ){
				Map map = null;
				map = ds_target.get( i );
				resultList.add( map );
			}

		}else{
			getSqlMapClientTemplate().getSqlMapClient().startBatch();
			for( int i = 0; i < ds_target.size(); i++ ){

				Map map = null;

				try{
					map = ds_target.get( i );
					map.put( "REPT_YMD", reptYmd );// 보고년월
					map.put( "GENRE_CD", genreCd );// 장르코드
					map.put( "MODI_IDNT", rgstIdnt );// 수정자아이디
					map.put( "TRST_ORGN_CODE", trstOrgnCode );// 위탁관리기관아이디
					int worksId =
						Integer.parseInt( ""
							+ getSqlMapClientTemplate().queryForObject(
								"AdminWorksRegistration.getForUpdateAdminCommWorksId",
								map ) );
					if( worksId == 0 ){
						cFlag = false;
					}else{// 저작물id를 구했으므로 수정처리를 해야함
						map.put( "WORKS_ID", worksId );// 저작물아이디
						try{
							getSqlMapClientTemplate().getSqlMapClient().update(
								"AdminWorksRegistration.adminCommMusicUpdate",
								map );
							gFlag = true;
						}
						catch( Exception e ){
							gFlag = false;
						}
						if( gFlag ){
							try{
								getSqlMapClientTemplate().getSqlMapClient().update(
									"AdminWorksRegistration.adminCommWorksUpdate",
									map );
								cFlag = true;
							}
							catch( Exception e ){
								cFlag = false;
							}
						}
					}
					if( cFlag && gFlag ){
						cnt++;
					}
					if( !cFlag || !gFlag ){
						resultList.add( map );
					}
				}
				catch( Exception e ){
					e.printStackTrace();
					resultList.add( map );
				}
			}
			getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		}

		commandMap.put( "MODI_WORKS_CONT", cnt );
		commandMap.put( "REPT_SEQN", rFlag );

		if( rFlag != 0 ){
			adminCommWorksReportUpdate( commandMap );
		}
		return resultList;
	}

	public List adminCommBookUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> ds_target ) throws Exception{
		List resultList = new ArrayList();

		int rFlag = 0;// 리포트FLAG&REPT_SEQN
		boolean cFlag = true;// 저작물FLAG
		boolean gFlag = true;// 장르별저작물FLAG

		int cnt = 0; // 수정 성공 cnt

		GregorianCalendar d = new GregorianCalendar();

		// 보고연월
		String reptYmdTmp =
			String.valueOf( d.get( Calendar.YEAR ) )
				+ String.valueOf( ( ( d.get( Calendar.MONTH ) + 1 ) < 10 ) ? "0"
					+ String.valueOf( d.get( Calendar.MONTH ) + 1 ) : String.valueOf( d.get( Calendar.MONTH ) + 1 ) );

		String reptYmd = reptYmdTmp.replaceAll( " ", "" );

		// 장르코드
		String genreCd = (String) commandMap.get( "GENRE_CODE" );

		// 등록자아이디
		String rgstIdnt = (String) commandMap.get( "USER_IDNT" );

		// 위탁관리기관ID
		String trstOrgnCode = (String) commandMap.get( "TRST_ORGN_CODE" );

		commandMap.put( "REPT_YMD", reptYmd );// 보고연월
		commandMap.put( "GENRE_CD", genreCd );// 장르코드
		commandMap.put( "MODI_IDNT", rgstIdnt );// 등록자아이디

		rFlag = adminCommWorksReportInsert( commandMap );

		if( rFlag == 0 ){
			for( int i = 0; i < ds_target.size(); i++ ){
				Map map = null;
				map = ds_target.get( i );
				resultList.add( map );
			}
		}else{
			getSqlMapClientTemplate().getSqlMapClient().startBatch();
			for( int i = 0; i < ds_target.size(); i++ ){
				Map map = null;
				try{
					map = ds_target.get( i );
					map.put( "REPT_YMD", reptYmd );// 보고년월
					map.put( "GENRE_CD", genreCd );// 장르코드
					map.put( "MODI_IDNT", rgstIdnt );// 수정자아이디
					map.put( "TRST_ORGN_CODE", trstOrgnCode );// 위탁관리기관아이디

					int worksId =
						Integer.parseInt( ""
							+ getSqlMapClientTemplate().queryForObject(
								"AdminWorksRegistration.getForUpdateAdminCommWorksId",
								map ) );

					if( worksId == 0 ){
						cFlag = false;
					}else{// 저작물id를 구했으므로 수정처리를 해야함
						map.put( "WORKS_ID", worksId );// 저작물아이디
						try{
							getSqlMapClientTemplate().getSqlMapClient().update(
								"AdminWorksRegistration.adminCommBookUpdate",
								map );
							gFlag = true;
						}
						catch( Exception e ){
							gFlag = false;
						}
						// gFlag = adminCommBookUpdate(map);

						if( gFlag ){
							try{
								getSqlMapClientTemplate().getSqlMapClient().update(
									"AdminWorksRegistration.commWorksUpdate",
									map );
								cFlag = true;
							}
							catch( Exception e ){
								cFlag = false;
							}
							// cFlag = adminCommWorksUpdate(map);
						}
					}
					if( cFlag && gFlag ){
						cnt++;
					}
					if( !cFlag || !gFlag ){
						resultList.add( map );
					}
				}
				catch( Exception e ){
					e.printStackTrace();
					resultList.add( map );
				}
			}
			getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		}
		commandMap.put( "MODI_WORKS_CONT", cnt );
		commandMap.put( "REPT_SEQN", rFlag );

		if( rFlag != 0 ){
			adminCommWorksReportUpdate( commandMap );
		}

		return resultList;
	}

	/*
	 * public List adminCommScriptUpdate(Dataset ds_condition, Dataset
	 * ds_report, Dataset ds_target) throws Exception { // TODO Auto-generated
	 * method stub return null; } public List adminCommMovieUpdate(Dataset
	 * ds_condition, Dataset ds_report, Dataset ds_target) throws Exception { //
	 * TODO Auto-generated method stub return null; } public List
	 * adminCommBroadcastUpdate(Dataset ds_condition, Dataset ds_report, Dataset
	 * ds_target) throws Exception { // TODO Auto-generated method stub return
	 * null; } public List adminCommNewsUpdate(Dataset ds_condition, Dataset
	 * ds_report, Dataset ds_target) throws Exception { // TODO Auto-generated
	 * method stub return null; } public List adminCommArtUpdate(Dataset
	 * ds_condition, Dataset ds_report, Dataset ds_target) throws Exception { //
	 * TODO Auto-generated method stub return null; } public List
	 * adminCommImageUpdate(Dataset ds_condition, Dataset ds_report, Dataset
	 * ds_target) throws Exception { // TODO Auto-generated method stub return
	 * null; } public List adminCommSideUpdate(Dataset ds_condition, Dataset
	 * ds_report, Dataset ds_target) throws Exception { // TODO Auto-generated
	 * method stub return null; }
	 */
	public void targetDeleteBook( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20151001");
		// map.put("E_DATE", "20151031");
		getSqlMapClientTemplate().delete( "AdminWorksRegistration.targetDeleteBook", map );

	}

	public void targetInsertBook( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20151001");
		// map.put("E_DATE", "20151031");
		getSqlMapClientTemplate().insert( "AdminWorksRegistration.targetInsertBook", map );
	}

	public List targetBookList( Map map ) throws Exception{
		List report_List_date = new ArrayList();
		report_List_date = getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.reportDateVal", map );
		Map map2 = (Map) report_List_date.get( 0 );
		map.put( "F_DATE", map2.get( "F_DATE" ) );
		map.put( "E_DATE", map2.get( "E_DATE" ) );
		// map.put("F_DATE", "20151001");
		// map.put("E_DATE", "20151031");
		return getSqlMapClientTemplate().queryForList( "AdminWorksRegistration.targetBookList", map );
	}

	public String commBookInsertBacth( List list ) throws Exception{
		return (String) getSqlMapClientTemplate().insert( "AdminWorksRegistration.adminCommBookInsertBacth", list );
	}

	public boolean adminCommBookInsert( Map map ) throws Exception{
		boolean gFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminWorksRegistration.adminCommBookInsert", map );
			gFlag = true;
		}
		catch( Exception e ){
			gFlag = false;
		}

		return gFlag;
	}

}
