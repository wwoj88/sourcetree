package kr.or.copyright.mls.console.ntcn;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.ntcn.inter.FdcrAd75Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
public class FdcrAd75Controller extends DefaultController{

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd75Controller.class );

	@Resource( name = "fdcrAd75Service" )
	private FdcrAd75Service fdcrAd75Service;

	/**
	 * ckeditor 이미지 파일 다운로드
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/cmm/bbs/ckeditorImgDown.page" )
	public void ckeditorImgDown( HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "########## ckeditorImgDown start #############" );
		String savePath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		String fileName = request.getParameter( "fileName" );
		if( null != fileName ){
			download( request, response, savePath + fileName, fileName );
		}
		logger.debug( "########## ckeditorImgDown end #############" );
	}

	/**
	 * ckeditor 이미지 업로드
	 * 
	 * @param request
	 * @param mreq
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/cmm/bbs/ckeditorImgUpload.page" )
	public String ckeditorImgUpload( ModelMap model,
		HttpServletRequest request,
		MultipartHttpServletRequest mreq,
		HttpServletResponse response ) throws Exception{
		String callbackFunction = request.getParameter( "CKEditorFuncNum" );

		try{
			ArrayList<Map<String, Object>> uploadFileList = ckeditorImgUpload( mreq );
			for( Map<String, Object> file : uploadFileList ){
				String filePath = (String) file.get( "F_fileName" );
				String fileDownPath = "/console/cmm/bbs/ckeditorImgDown.page?fileName=" + filePath;

				model.addAttribute( "callbackFunction", callbackFunction );
				model.addAttribute( "fileDownPath", fileDownPath );
			}
		}
		catch( RuntimeException e ){
			System.out.println( "실행중 에러발생 " );
		}
		return "/console/cmm/bbs/cmmImg";
	}

	private ArrayList<Map<String, Object>> ckeditorImgUpload( MultipartHttpServletRequest mreq ){
		ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
		String savePath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );

		Iterator<?> iter = mreq.getFileNames();
		int i = 0;
		while( iter.hasNext() ){
			String fileFieldName = (String) iter.next();
			MultipartFile file = mreq.getFile( fileFieldName );
			if( !file.isEmpty() && file.getSize() > 0 ){
				String fieldName = file.getName();
				String orgFileFullName = file.getOriginalFilename();
				String orgFileName = orgFileFullName.substring( 0, orgFileFullName.lastIndexOf( "." ) );
				String fileName = EgovWebUtil.getDate( "yyyyMMddHHmmssSS" ) + "_" + i;
				String extName = orgFileFullName.substring( orgFileFullName.lastIndexOf( "." ) + 1 );
				String fileFullName = fileName + "." + extName;
				long sizeInBytes = file.getSize();

				/* 파일 저장 */
				try{
					File dir = new File( savePath );
					if( !dir.exists() ){
						dir.mkdir();
					}
					File transFilePath = new File( savePath, fileFullName );
					file.transferTo( transFilePath );
				}
				catch( IOException ex ){
					// ex.printStackTrace();
					System.out.println( "실행중IOException 에러발생 " );
				}

				/* 파일 정보 저장 */
				Map<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put( "F_fileName", fileFullName );
				fileMap.put( "F_extName", extName );
				fileMap.put( "F_filesize", sizeInBytes );
				fileMap.put( "F_orgFileName", orgFileFullName );
				fileMap.put( "F_savePath", savePath );
				fileMap.put( "F_saveFilePath", savePath + fileFullName );
				uploadList.add( fileMap );
				i++;
			}
		}

		return uploadList;
	}

	/**
	 * 게시물 목록 MENU_SEQN 1 : FAQ 2 : QNA 3 : 관리자 공지사항 4 : 공지사항 5 : 홍보자료
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/ntcn/fdcrAd75List1.page" )
	public String fdcrAd75List1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		String TITLE = EgovWebUtil.getString( commandMap, "TITLE" );

		if( !"".equals( TITLE ) ){
			TITLE = URLDecoder.decode( TITLE, "UTF-8" );
			commandMap.put( "TITLE", TITLE );
		}

		int totCnt = fdcrAd75Service.selectListCount( commandMap );
		commandMap = pagination( commandMap, totCnt, 20 );
		ArrayList<Map<String, Object>> list = fdcrAd75Service.selectList( commandMap );

		model.addAttribute( "list", list );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );

		String MENU_SEQN = EgovWebUtil.getString( commandMap, "MENU_SEQN" );
		return "ntcn/fdcrAd75List1.tiles";
	}

	/**
	 * 게시물 조회
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/ntcn/fdcrAd75View1.page" )
	public String fdcrAd75View1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{

		String searchStr = EgovWebUtil.getString( commandMap, "searchStr" );

		if( !"".equals( searchStr ) ){
			searchStr = URLDecoder.decode( searchStr, "UTF-8" );
			commandMap.put( "searchStr", searchStr );
		}

		Map<String, Object> info = fdcrAd75Service.selectView( commandMap );
		info.put( "BORD_DESC", EgovWebUtil.getString( info, "BORD_DESC" ) );
		model.addAttribute( "info", info );

		ArrayList<Map<String, Object>> attachList = fdcrAd75Service.selectAttachFileList( commandMap );

		model.addAttribute( "attachList", attachList );
		model.addAttribute( "commandMap", commandMap );

		String MENU_SEQN = EgovWebUtil.getString( commandMap, "MENU_SEQN" );
		return "ntcn/fdcrAd75View1.tiles";
	}

	/**
	 * 게시물 쓰기화면
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/ntcn/fdcrAd75WriteForm1.page" )
	public String fdcrAd75WriteForm1( Map<String, Object> commandMap,
		ModelMap model ) throws Exception{
		String searchStr = EgovWebUtil.getString( commandMap, "searchStr" );

		if( !"".equals( searchStr ) ){
			searchStr = URLDecoder.decode( searchStr, "UTF-8" );
			commandMap.put( "searchStr", searchStr );
		}
		model.addAttribute( "commandMap", commandMap );

		String MENU_SEQN = EgovWebUtil.getString( commandMap, "MENU_SEQN" );
		return "ntcn/fdcrAd75WriteForm1.tiles";
	}

	/**
	 * 게시물 쓰기
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/ntcn/fdcrAd75Write1.page" )
	public String fdcrAd75Write1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		MultipartHttpServletRequest mreq,
		HttpServletResponse response ) throws Exception{
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		commandMap.put( "RGST_NAME", ConsoleLoginUser.getUserName() );
		commandMap.put( "MAIL", ConsoleLoginUser.getUserEmail() );
		commandMap.put( "BORD_DESC_TAG", EgovWebUtil.getString( commandMap, "BORD_DESC_TAG" ) );
		commandMap.put( "RGST_KIND_CODE", EgovWebUtil.getString( commandMap, "RGST_KIND_CODE" ) );

		/* 일련번호 */
		boolean isSuccess = fdcrAd75Service.insertBoardInfo( commandMap );
		if( isSuccess ){
			fileInsert( mreq, commandMap, null );
			return returnUrl(
				model,
				"등록했습니다.",
				"/console/ntcn/fdcrAd75List1.page?MENU_SEQN=" + EgovWebUtil.getString( commandMap, "MENU_SEQN" ));
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/ntcn/fdcrAd75WriteForm1.page?MENU_SEQN=" + EgovWebUtil.getString( commandMap, "MENU_SEQN" ));
		}
	}

	public void fileInsert( MultipartHttpServletRequest mreq,
		Map<String, Object> commandMap,
		String[] extNames ) throws Exception{
		String savePath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> files = fileUpload( mreq,savePath,null,true);
		for( int i = 0; i < files.size(); i++ ){
			Map<String, Object> file = files.get( i );
			Map<String, Object> param = new HashMap<String, Object>();
			param.put( "MENU_SEQN", EgovWebUtil.getString( commandMap, "MENU_SEQN" ) );
			param.put( "BORD_SEQN", EgovWebUtil.getToInt( commandMap, "BORD_SEQN" ) );
			param.put( "FILE_NAME", EgovWebUtil.getString( file, "F_fileName" ) );
			param.put( "FILE_PATH", EgovWebUtil.getString( file, "F_saveFilePath" ) );
			param.put( "FILE_SIZE", file.get( "F_filesize" ) );
			param.put( "REAL_FILE_NAME", EgovWebUtil.getString( file, "F_orgFileName" ) );
			param.put( "RGST_IDNT", ConsoleLoginUser.getUserId());
			fdcrAd75Service.insertBoardFile( param );
		}
	}

	/**
	 * 게시물 수정화면
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/ntcn/fdcrAd75UpdateForm1.page" )
	public String fdcrAd75UpdateForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		Map<String, Object> info = fdcrAd75Service.selectView( commandMap );
		info.put( "BORD_DESC", EgovWebUtil.getString( info, "BORD_DESC" ) );
		model.addAttribute( "info", info );

		ArrayList<Map<String, Object>> attachList = fdcrAd75Service.selectAttachFileList( commandMap );
		model.addAttribute( "attachList", attachList );
		model.addAttribute( "commandMap", commandMap );

		String MENU_SEQN = EgovWebUtil.getString( commandMap, "MENU_SEQN" );
		return "ntcn/fdcrAd75UpdateForm1.tiles";
	}

	/**
	 * 게시물 수정
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/ntcn/fdcrAd75Update1.page" )
	public String fdcrAd75Update1( Map<String, Object> commandMap,
		ModelMap model,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		commandMap.put( "RGST_NAME", ConsoleLoginUser.getUserName() );
		commandMap.put( "MAIL", ConsoleLoginUser.getUserEmail() );
		commandMap.put( "BORD_DESC_TAG", EgovWebUtil.getString( commandMap, "BORD_DESC_TAG" ) );
		commandMap.put( "RGST_KIND_CODE", EgovWebUtil.getString( commandMap, "RGST_KIND_CODE" ) );
		
		boolean isSuccess = fdcrAd75Service.updateBoardInfo( commandMap );
		if( isSuccess ){
			fileInsert( mreq, commandMap, null );
			String[] deleteFile = request.getParameterValues( "orgFileDel" );
			if( null != deleteFile ){
				for( int i = 0; i < deleteFile.length; i++ ){
					Map<String, Object> param = new HashMap<String, Object>();
					param.put( "ATTC_SEQN", deleteFile[i] );
					param.put( "MENU_SEQN", EgovWebUtil.getString( commandMap, "MENU_SEQN" ) );
					param.put( "BORD_SEQN", EgovWebUtil.getToInt( commandMap, "BORD_SEQN" ) );

					Map<String, Object> file = fdcrAd75Service.selectAttachFileInfo( param );
					fdcrAd75Service.deleteBoardFile( param );
					try{
						String filePath = EgovWebUtil.getString( file, "FILE_PATH" );
						String realFileName = EgovWebUtil.getString( file, "REAL_FILE_NAME" );
						File f = new File( filePath + realFileName );
						if( f.exists() ){
							f.delete();
						}
					}
					catch( Exception e ){
						e.printStackTrace();
					}
				}
			}
			return returnUrl(
				model,
				"수정했습니다.",
				"/console/ntcn/fdcrAd75View1.page?MENU_SEQN=" + EgovWebUtil.getString( commandMap, "MENU_SEQN" )
					+ "&BORD_SEQN=" + EgovWebUtil.getString( commandMap, "BORD_SEQN" ) );
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/ntcn/fdcrAd75UpdateForm1.page?MENU_SEQN=" + EgovWebUtil.getString( commandMap, "MENU_SEQN" )
				+ "&BORD_SEQN=" + EgovWebUtil.getString( commandMap, "BORD_SEQN" ) );
		}
	}

	/**
	 * 게시물 삭제
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/ntcn/fdcrAd75Delete1.page" )
	public String fdcrAd75Delete1( Map<String, Object> commandMap,
		ModelMap model,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		boolean isSuccess = fdcrAd75Service.deleteBoardInfo( commandMap );

		if( isSuccess ){
			return returnUrl(
				model,
				"삭제했습니다.",
				"/console/ntcn/fdcrAd75List1.page?MENU_SEQN=" + EgovWebUtil.getString( commandMap, "MENU_SEQN" ));
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/ntcn/fdcrAd75View1.page?MENU_SEQN=" + EgovWebUtil.getString( commandMap, "MENU_SEQN" )
				+ "&BORD_SEQN=" + EgovWebUtil.getString( commandMap, "BORD_SEQN" ) );
		}
	}

	/**
	 * 게시판 첨부 파일 다운로드
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/ntcn/fdcrAd75Down1.page" )
	public void fdcrAd75Down1( HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{

		String savePath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		Map<String, Object> fileInfo = fdcrAd75Service.selectAttachFileInfo( commandMap );

		String fileRealNm = EgovWebUtil.getString( fileInfo, "REAL_FILE_NAME" );
		String fileNm = EgovWebUtil.getString( fileInfo, "FILE_NAME" );
		String downPath = savePath + fileNm;
		if( !"".equals( fileRealNm ) ){
			download( request, response, downPath, fileRealNm );
		}
	}

	/**
	 * 첨부파일 목록
	 * 
	 * @param request
	 * @param commandMap
	 * @param model
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/ntcn/fdcrAd75List2.page" )
	public String fdcrAd75List2( HttpServletRequest request,
		Map<String, Object> commandMap,
		ModelMap model,
		HttpServletResponse response ) throws Exception{

		ArrayList<Map<String, Object>> fileList = fdcrAd75Service.selectAttachFileList( commandMap );
		model.addAttribute( "fileList", fileList );
		return "/cmm/bbs/cmmBbsFileList";
	}

	/**
	 * 파일 업로드
	 * 
	 * @param mreq
	 * @return 업로드 파일정보
	 */
	protected ArrayList<Map<String, Object>> bbsFileUpload( MultipartHttpServletRequest mreq,
		Map<String, Object> commandMap,
		String[] extNames ){

		ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
		String brdid = EgovWebUtil.getString( commandMap, "brdid" );
		String savePath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );

		Iterator<?> iter = mreq.getFileNames();
		int i = 0;
		while( iter.hasNext() ){
			String fileFieldName = (String) iter.next();
			MultipartFile file = mreq.getFile( fileFieldName );
			if( !file.isEmpty() && file.getSize() > 0 ){
				String fieldName = file.getName();
				String orgFileFullName = file.getOriginalFilename();
				String orgFileName = orgFileFullName.substring( 0, orgFileFullName.lastIndexOf( "." ) );
				String extName = orgFileFullName.substring( orgFileFullName.lastIndexOf( "." ) );
				String extName1 = orgFileFullName.substring( orgFileFullName.lastIndexOf( "." ) + 1 );
				String fileType = "";
				if( i < 9 ){
					fileType = "0" + ( i + 1 );
				}else{
					fileType = "" + ( i + 1 );
				}
				String fileFullName = brdid + "_REQ_" + fileType + extName;
				long sizeInBytes = file.getSize();

				/* 확장자 체크 */
				if( null != extName1 && !"".equals( extName1 ) ){
					if( null != extNames && extNames.length > 0 ){
						boolean isExtCheck = false;
						for( String compareExt : extNames ){
							if( ( compareExt.toUpperCase() ).equals( extName1.toUpperCase() ) ){
								isExtCheck = true;
							}
						}
						if( !isExtCheck ){
							continue;
						}
					}
				}

				logger.debug( "파일 [fieldName] : " + fieldName + "<br/>" );
				logger.debug( "파일 [orgFileFullName] : " + orgFileFullName + "<br/>" );
				logger.debug( "파일 [fileFullName] : " + fileFullName + "<br/>" );
				logger.debug( "파일 [extName] : " + extName + "<br/>" );
				logger.debug( "파일 [sizeInBytes] : " + sizeInBytes + "<br/>" );

				/* 파일 저장 */
				try{
					File dir = new File( savePath );
					if( !dir.exists() ){
						dir.mkdir();
					}
					File transFilePath = new File( savePath, fileFullName );
					file.transferTo( transFilePath );
				}
				catch( IOException ex ){
					// ex.printStackTrace();
					System.out.println( "실행중IOException 에러발생 " );
				}

				/* 파일 정보 저장 */
				Map<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put( "F_fileName", fileFullName );
				fileMap.put( "F_extName", extName );
				fileMap.put( "F_filesize", sizeInBytes );
				fileMap.put( "F_orgFileName", orgFileFullName );
				fileMap.put( "F_filePath", savePath );
				fileMap.put( "F_saveFilePath", savePath + fileFullName );
				fileMap.put( "F_fileType", fileType );
				uploadList.add( fileMap );
				i++;
			}
		}

		return uploadList;
	}
}
