package kr.or.copyright.mls.console.rightreqst;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.rightreqst.inter.FdcrAd3501Dao;
import kr.or.copyright.mls.console.rightreqst.inter.FdcrAd35Dao;
import kr.or.copyright.mls.console.rightreqst.inter.FdcrAd35Service;
import kr.or.copyright.mls.rght.dao.RghtDao;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;

import org.springframework.stereotype.Service;

import com.softforum.xdbe.xCrypto;

@Service( "fdcrAd35Service" )
public class FdcrAd35ServiceImpl extends CommandService implements FdcrAd35Service{

	@Resource( name = "fdcrAd35Dao" )
	private FdcrAd35Dao fdcrAd35Dao;

	@Resource( name = "fdcrAd3501Dao" )
	private FdcrAd3501Dao fdcrAd3501Dao;

	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	@Resource( name = "rghtDao" )
	private RghtDao rghtDao;

	/**
	 * 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd35List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd35Dao.inmtList( commandMap );
		//List pageCount = (List) fdcrAd35Dao.inmtListCount( commandMap );

		commandMap.put( "ds_list", list );
		//commandMap.put( "ds_page", pageCount );

	}

	/**
	 * 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public void fdcrAd35View1( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List list = (List) fdcrAd3501Dao.rghtDetlList( commandMap );
		List fileList = (List) fdcrAd3501Dao.rghtFileList( commandMap );

		List workList = new ArrayList();
		List tempList = new ArrayList();

		if( commandMap.get( "PRPS_DIVS" ).equals( "V" ) ){ // 영화
			workList = (List) fdcrAd3501Dao.rghtMvieTempList( commandMap );
			commandMap.put( "ListGubun", "Y" );
			tempList = (List) fdcrAd3501Dao.rghtMvieTempList( commandMap );
		}else if( commandMap.get( "PRPS_DIVS" ).equals( "C" ) ){ // 방송대본
			workList = (List) fdcrAd3501Dao.rghtScriptTempList( commandMap );
			commandMap.put( "ListGubun", "Y" );
			tempList = (List) fdcrAd3501Dao.rghtScriptTempList( commandMap );
		}else if( commandMap.get( "PRPS_DIVS" ).equals( "R" ) ){ // 방송
			workList = (List) fdcrAd3501Dao.rghtBroadcastTempList( commandMap );
			commandMap.put( "ListGubun", "Y" );
			tempList = (List) fdcrAd3501Dao.rghtBroadcastTempList( commandMap );
		}else if( commandMap.get( "PRPS_DIVS" ).equals( "N" ) ){ // 뉴스
			workList = (List) fdcrAd3501Dao.rghtNewsTempList( commandMap );
			commandMap.put( "ListGubun", "Y" );
			tempList = (List) fdcrAd3501Dao.rghtNewsTempList( commandMap );
		}else{
			workList = (List) fdcrAd3501Dao.rghtTempList( commandMap ); // 저작물리스트
																		// 저작물명
			commandMap.put( "ListGubun", "Y" );
			tempList = (List) fdcrAd3501Dao.rghtTempList( commandMap ); // 저작물리스트
		}

		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_fileList", fileList );
		commandMap.put( "ds_works", workList );
		commandMap.put( "ds_temp", tempList );
	}

	/**
	 * 신청자 정보 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd35View2( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List list2 = (List) consoleCommonDao.userDetlList( commandMap );

		/* resd복호화 str 20121108 정병호 */
		xCrypto.RegisterEx(
			"pattern7",
			2,
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
			"pool1",
			"mcst_db",
			"mcst_owner",
			"mcst_table",
			"pattern7" );

		Map list = new HashMap();
		list = (Map) list2.get( 0 );

		String RESD_CORP_NUMB = "";

		// 주민번호가 notnull 인 경우만.
		if( list.get( "RESD_CORP_NUMB" ) != null
			&& list.get( "RESD_CORP_NUMB" ).toString().replaceAll( " ", "" ).length() > 0 ){
			RESD_CORP_NUMB = xCrypto.Decrypt( "pattern7", (String) list.get( "RESD_CORP_NUMB" ) ).replaceAll( " ", "" ); // 원본
																															// 주민번호
																															// 복호화

			String RESD_CORP_NUMB1 = RESD_CORP_NUMB.substring( 0, 6 );
			String RESD_CORP_NUMB2 = ""; // RESD_CORP_NUMB.substring(6);

			list.put( "RESD_CORP_NUMB", RESD_CORP_NUMB1 + "-" + RESD_CORP_NUMB2 );
		}

		commandMap.put( "ds_list", list );

	}

	/**
	 * 신청저작물 상세정보 팝업
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public void fdcrAd35Pop1( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		String prpsDivs = commandMap.get( "PRPS_DIVS" ).toString();
		List list = null;

		if( prpsDivs.equals( "M" ) ){ // 음악
			list = (List) fdcrAd3501Dao.rghtMuscDetail( commandMap );
		}else if( prpsDivs.equals( "O" ) ){ // 어문
			list = (List) fdcrAd3501Dao.rghtBookDetail( commandMap );
		}else if( prpsDivs.equals( "C" ) ){ // 방송대본
			list = (List) fdcrAd3501Dao.rghtScriptDetail( commandMap );
		}else if( prpsDivs.equals( "V" ) ){ // 영화
			list = (List) fdcrAd3501Dao.rghtMvieDetail( commandMap );
		}else if( prpsDivs.equals( "R" ) ){ // 방송
			list = (List) fdcrAd3501Dao.rghtBroadcastDetail( commandMap );
		}else if( prpsDivs.equals( "N" ) ){ // 뉴스
			list = (List) fdcrAd3501Dao.rghtNewsDetail( commandMap );
		}
		commandMap.put( "ds_works_info", list );
	}

	/**
	 * 처리결과 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd35Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			// 삭제처리 전에 현재 최대상태값을 확인
			String maxDealStat = "";
			List maxDealStatList = rghtDao.getMaxDealStat( commandMap );

			maxDealStat = (String) ( (HashMap) maxDealStatList.get( 0 ) ).get( "MAX_DEAL_STAT" );

			System.out.println( "==========================================" );
			System.out.println( "maxDealStat::::" + maxDealStat );
			System.out.println( "==========================================" );

			// 수정처리 - 최대상태값이 1 신청인 경우
			if( maxDealStat.equals( "1" ) ){

				try{
					// 내권리찾기 신청 master 삭제(ML_PRPS)
					rghtDao.prpsMasterDelete( commandMap );

					// 내권리찾기 신청 DETAIL 삭제(ML_PRPS_RSLT)
					rghtDao.prpsDetailDelete( commandMap );

					// 내권리찾기 신청 권리정보 삭제(ML_PRPS_RSLT_RGHT)
					fdcrAd3501Dao.prpsRsltRghtDelete( commandMap );

					String[] PRPS_MAST_KEYS = (String[]) commandMap.get( "PRPS_MAST_KEY" );
					String[] PRPS_SEQNS = (String[]) commandMap.get( "PRPS_SEQN" );
					String[] ATTC_SEQNS = (String[]) commandMap.get( "ATTC_SEQN" );
					String[] REAL_FILE_NAMES = (String[]) commandMap.get( "REAL_FILE_NAME" );

					// 첨부파일삭제
					for( int fi = 0; fi < PRPS_MAST_KEYS.length; fi++ ){
						Map<String, Object> param = new HashMap<String, Object>();

						param.put( "PRPS_MAST_KEY", PRPS_MAST_KEYS[fi] );
						param.put( "PRPS_SEQN", PRPS_SEQNS[fi] );
						param.put( "ATTC_SEQN", ATTC_SEQNS[fi] );

						rghtDao.rghtFileDelete( param );

						String fileName = commandMap.get( REAL_FILE_NAMES[fi] ).toString();

						try{
							File file = new File( (String) commandMap.get( "FILE_PATH" ), fileName );
							if( file.exists() ){
								file.delete();
							}
						}
						catch( Exception e ){
							e.printStackTrace();
						}
					}
					commandMap.put( "CONN_URL", "내권리찾기신청 삭제" );
					rghtDao.insertConnInfo( commandMap );
				}
				catch( Exception e ){
					e.printStackTrace();
				}
			} // end if.. 삭제처리.
			commandMap.put( "ds_maxDealStat", maxDealStatList );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 처리결과 및 신청정보 등록
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd35Insert1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			// UPDATE처리
			String userId = commandMap.get( "userId" ).toString();
			String caId = commandMap.get( "GUBUN_TRST_ORGN_CODE" ).toString();
			String prpsDivs = commandMap.get( "PRPS_DIVS" ).toString();

			String[] PRPS_MAST_KEYS = (String[]) commandMap.get( "PRPS_MAST_KEY" );
			String[] PRPS_IDNTS = (String[]) commandMap.get( "PRPS_IDNT" );
			String[] PRPS_DIVSS = (String[]) commandMap.get( "PRPS_DIVS" );
			String[] TRST_ORGN_CODES = (String[]) commandMap.get( "TRST_ORGN_CODE" );
			String[] RGHT_ROLE_CODES = (String[]) commandMap.get( "RGHT_ROLE_CODE" );

			for( int i = 0; i < PRPS_MAST_KEYS.length; i++ ){
				Map<String, Object> param = new HashMap<String, Object>();

				param.put( "userId", userId );
				fdcrAd3501Dao.rghtRsltUpdate( param );

				// 이미지 저작권자 정보 저장 : 복전협(205)인 경우만..
				if( caId.equals( "205" ) && prpsDivs.equals( "I" ) ){

					if( commandMap.get( "PRPS_RGHT_CODE" ).equals( "3" ) ){
						param.put( "IMAGE_COPT_HODR", commandMap.get( "COPT_HODR" ) );
					}
					fdcrAd3501Dao.imgeUpdate( param );
				}

				if( commandMap.get( "PRPS_RGHT_CODE" ).equals( "3" ) ){ // 이용자
																		// 권리조회

					if( !prpsDivs.equals( "X" ) ){
						if( caId.equals( "201" ) ){ // 음저협
							if( commandMap.get( "LYRICIST" ) != null ){
								param.put( "RGHT_ROLE_CODE", "101" );
								param.put( "HOLD_NAME", commandMap.get( "LYRICIST" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
							if( commandMap.get( "COMPOSER" ) != null ){
								param.put( "RGHT_ROLE_CODE", "102" );
								param.put( "HOLD_NAME", commandMap.get( "COMPOSER" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
							if( commandMap.get( "ARRANGER" ) != null ){
								param.put( "RGHT_ROLE_CODE", "103" );
								param.put( "HOLD_NAME", commandMap.get( "ARRANGER" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "202" ) ){ // 음실연
							if( commandMap.get( "SINGER" ) != null ){ // 가창
								param.put( "RGHT_ROLE_CODE", "112" );
								param.put( "HOLD_NAME", commandMap.get( "SINGER" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
							if( commandMap.get( "PLAYER" ) != null ){ // 연주
								param.put( "RGHT_ROLE_CODE", "113" );
								param.put( "HOLD_NAME", commandMap.get( "PLAYER" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
							if( commandMap.get( "CONDUCTOR" ) != null ){ // 지휘
								param.put( "RGHT_ROLE_CODE", "114" );
								param.put( "HOLD_NAME", commandMap.get( "CONDUCTOR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "203" ) ){ // 음제협
							if( commandMap.get( "PRODUCER" ) != null ){ // 앨범제작
								param.put( "RGHT_ROLE_CODE", "111" );
								param.put( "HOLD_NAME", commandMap.get( "PRODUCER" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "204" ) ){ // 문예협
							if( commandMap.get( "LICENSOR_NAME_KOR" ) != null ){ // 역자
								param.put( "RGHT_ROLE_CODE", "201" );
								param.put( "HOLD_NAME", commandMap.get( "LICENSOR_NAME_KOR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
							if( commandMap.get( "TRANSLATOR" ) != null ){ // 지휘
								param.put( "RGHT_ROLE_CODE", "202" );
								param.put( "HOLD_NAME", commandMap.get( "TRANSLATOR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
							// (이미지)
							if( commandMap.get( "COPT_HODR" ) != null ){ // 저작권자
								param.put( "RGHT_ROLE_CODE", "301" );
								param.put( "HOLD_NAME", commandMap.get( "COPT_HODR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "205" ) ){ // 복전협
							if( commandMap.get( "LICENSOR_NAME_KOR" ) != null ){ // 저자
								param.put( "RGHT_ROLE_CODE", "201" );
								param.put( "HOLD_NAME", commandMap.get( "LICENSOR_NAME_KOR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
							if( commandMap.get( "TRANSLATOR" ) != null ){ // 역자
								param.put( "RGHT_ROLE_CODE", "202" );
								param.put( "HOLD_NAME", commandMap.get( "TRANSLATOR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}

							// (이미지)
							if( commandMap.get( "COPT_HODR" ) != null ){ // 저작권자
								param.put( "RGHT_ROLE_CODE", "301" );
								param.put( "HOLD_NAME", commandMap.get( "COPT_HODR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "216" ) && prpsDivs.equals( "I" ) ){ // 문화콘텐츠진흥원(이미지)
							if( commandMap.get( "COPT_HODR" ) != null ){ // 저작권자
								param.put( "RGHT_ROLE_CODE", "301" );
								param.put( "HOLD_NAME", commandMap.get( "COPT_HODR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "206" ) ){ // 방작협
							if( commandMap.get( "WRITER" ) != null ){ // 작가
								param.put( "RGHT_ROLE_CODE", "501" );
								param.put( "HOLD_NAME", commandMap.get( "WRITER" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "211" ) ){ // 영산협
							if( commandMap.get( "DISTRIBUTOR" ) != null ){ // 배급사
								param.put( "RGHT_ROLE_CODE", "403" );
								param.put( "HOLD_NAME", commandMap.get( "DISTRIBUTOR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "212" ) ){ // 한국시나리오작가협회
							if( commandMap.get( "WRITER" ) != null ){ // 작가
								param.put( "RGHT_ROLE_CODE", "404" );
								param.put( "HOLD_NAME", commandMap.get( "WRITER" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "213" ) ){ // 한국영화제작가협회
							if( commandMap.get( "MOVIE_PRODUCER" ) != null ){ // 제작사
								param.put( "RGHT_ROLE_CODE", "401" );
								param.put( "HOLD_NAME", commandMap.get( "MOVIE_PRODUCER" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
							if( commandMap.get( "INVESTOR" ) != null ){ // 투자사
								param.put( "RGHT_ROLE_CODE", "402" );
								param.put( "HOLD_NAME", commandMap.get( "INVESTOR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
							if( commandMap.get( "DISTRIBUTOR" ) != null ){ // 배급사
								param.put( "RGHT_ROLE_CODE", "403" );
								param.put( "HOLD_NAME", commandMap.get( "DISTRIBUTOR" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "214" ) ){ // 한국방송실연자협회
							if( commandMap.get( "MAKER" ) != null ){ // 제작사
								param.put( "RGHT_ROLE_CODE", "601" );
								param.put( "HOLD_NAME", commandMap.get( "MAKER" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}else if( caId.equals( "215" ) ){ // 언론진흥재단
							if( commandMap.get( "PROVIDER" ) != null ){ // 제작사
								param.put( "RGHT_ROLE_CODE", "701" );
								param.put( "HOLD_NAME", commandMap.get( "PROVIDER" ) );
								fdcrAd3501Dao.prpsRsltRghtUpdate( param );
							}
						}
					}else{
						param.put( "RGHT_ROLE_CODE", "001" );
						param.put( "HOLD_NAME", commandMap.get( "SIDE_COPT_HODR" ) );
						fdcrAd3501Dao.prpsRsltRghtUpdate( param );
					}

				}
			}

			// 메일, SMS
			Map<String, Object> infoMap = new HashMap<String, Object>();

			infoMap.put( "DEAL_STAT", "" );
			int totalCnt = fdcrAd3501Dao.rghtRsltCnt( infoMap ); // 전체 결과 Cnt

			infoMap.put( "DEAL_STAT", "4" );
			int doneCnt = fdcrAd3501Dao.rghtRsltCnt( infoMap ); // 완료 Cnt

			if( ( totalCnt - doneCnt ) == 0 ){

				// ----------------------------- mail
				// send------------------------------//

				String host = Constants.MAIL_SERVER_IP; // smtp서버
				String to = infoMap.get( "U_MAIL" ).toString(); // 수신EMAIL
				String toName = infoMap.get( "USER_NAME" ).toString(); // 수신인
				String subject = "[저작권찾기] 저작권찾기신청 처리완료";
				String idntName = fdcrAd3501Dao.rghtPrpsTitle( infoMap ); // infoMap.get("PRPS_IDNT_NAME").toString();
																			// //
																			// 신청대상명

				String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표
																// 메일
				String fromName = Constants.SYSTEM_NAME;

				if( infoMap.get( "U_MAIL_RECE_YSNO" ).equals( "Y" ) && to != null && to != "" ){

					// ------------------------- mail 정보
					// ----------------------------------//
					MailInfo info = new MailInfo();
					info.setFrom( from );
					info.setFromName( fromName );
					info.setHost( host );
					info.setEmail( to );
					info.setEmailName( toName );
					info.setSubject( subject );

					StringBuffer sb = new StringBuffer();

					sb.append( "<div class=\"mail_title\">" + toName + "님, 저작권찾기신청이 처리완료 되었습니다. </div>" );
					sb.append( "<div class=\"mail_contents\"> " );
					sb.append( "	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> " );
					sb.append( "		<tr> " );
					sb.append( "			<td> " );
					sb.append( "				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> " );
					// 변경부분
					sb.append( "					<tr> " );
					sb.append( "						<td class=\"tdLabel\">신청 저작물명</td> " );
					sb.append( "						<td class=\"tdData\">" + idntName + "</td> " );
					sb.append( "					</tr> " );
					sb.append( "				</table> " );
					sb.append( "			</td> " );
					sb.append( "		</tr> " );
					sb.append( "	</table> " );
					sb.append( "</div> " );

					info.setMessage( new String( MailMessage.getChangeInfoMailText( sb.toString() ) ) );
					MailManager manager = MailManager.getInstance();

					info = manager.sendMail( info );
					if( info.isSuccess() ){
						System.out.println( ">>>>>>>>>>>> message success :" + info.getEmail() );
					}else{
						System.out.println( ">>>>>>>>>>>> message false   :" + info.getEmail() );
					}
				}

				String smsTo = infoMap.get( "U_MOBL_PHON" ).toString();
				String smsFrom = Constants.SYSTEM_TELEPHONE;

				String smsMessage = "[저작권찾기] " + idntName + " - 저작권찾기신청 처리완료 되었습니다.";

				if( infoMap.get( "U_SMS_RECE_YSNO" ).equals( "Y" ) && smsTo != null && smsTo != "" ){

					SMSManager smsManager = new SMSManager();

					System.out.println( "return = " + smsManager.sendSMS( smsTo, smsFrom, smsMessage ) );
				}

			} // end email, sms

			// 상태처리 후 상태값 구한다.
			Map<String, Object> conMap = new HashMap<String, Object>();
			List dealStat = (List) fdcrAd3501Dao.rghtDealStat( conMap );
			commandMap.put( "ds_dealStat", dealStat );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}
}
