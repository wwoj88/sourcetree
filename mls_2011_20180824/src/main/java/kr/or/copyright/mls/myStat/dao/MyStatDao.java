package kr.or.copyright.mls.myStat.dao;

import java.util.List;

import kr.or.copyright.mls.myStat.model.ApplySign;
import kr.or.copyright.mls.myStat.model.File;
import kr.or.copyright.mls.myStat.model.StatApplication;
import kr.or.copyright.mls.myStat.model.StatApplicationShis;
import kr.or.copyright.mls.myStat.model.StatApplyWorks;


public interface MyStatDao {
	
	//이용승인 신청 현황 목록 및 total count 조회
	public int statRsltInqrListCount(StatApplication params);
	public List statRsltInqrList(StatApplication params);
	
	
	//신청서 신규 차수 조회
	public int newTmpApplyWriteSeq();
	public int newApplyWriteSeq();
	public int newApplyWriteSeq2(String ymd);
	
	
	public void updateStatApplyWorks(StatApplyWorks params);
	
	//이용승인 신청서tmp 등록
	public void insertStatApplicationTmp(StatApplication params);
	//첨부파일 등록
	public void insertFile(List params);
	//첨부파일 이용승인 신청tmp 맵핑정보 등록
	public void insertStatAttcFileTmp(List params);
	//첨부파일 이용승인 신청 맵핑정보 등록
	public void insertStatAttcFile(List params);
	
	//이용승인 신청서tmp 조회
	public StatApplication statApplicationTmp(StatApplication params);
	//이용승인 신청서 조회
	public StatApplication statApplication(StatApplication params);
	//이용승인 신청서 상태변경내역 조회
	public List statApplicationShisList(StatApplicationShis params);
	//이용승인 신청서tmp 첨부파일 조회
	public List statAttcFileTmp(StatApplication params);
	//엑셀신청명세서 tmp 첨부파일 조회
	public File excelStatAttcFileTmp(File params);
	
	//엑셀신청명세서 첨부파일 조회
	public File excelStatAttcFile(File params);
	
	//이용승인 신청서 첨부파일 조회
	public List statAttcFile(StatApplication params);
	
	
	//이용승인 신청서tmp 수정
	public void updateStatApplicationTmp(StatApplication params);
	//이용승인 신청서 수정
	public void updateStatApplication(StatApplication params);
	//이용승인 신청서 수정 진행상태
	public void updateStatApplicationStat(StatApplication params);
	//첨부파일 삭제
	public void deleteFile(List params);
	//첨부파일 이용승인 신청tmp 맴핑정보 삭제
	public void deleteStatAttcFileTmp(List params);
	//첨부파일 이용승인 신청 맴핑정보 삭제
	public void deleteStatAttcFile(List params);
	
	//이용승인 명세서tmp 삭제
	public void deleteStatApplyWorksTmp(StatApplyWorks params);
	
	//이용승인 명세서tmp seq삭제
	public void deleteWorksSeqnStatApplyWorksTmp(List params);
	    
	//이용승인 명세서tmp 등록
	public void insertStatApplyWorksTmp(List params);
	
	public void insertStatApplyWorksTmp_P(List params);
	
	//이용승인 명세서 삭제
	public void deleteStatApplyWorks(StatApplyWorks params);
	
	//이용승인 명세서 seq삭제
	public void deleteWorksSeqnStatApplyWorks(List params);
	
	//이용승인 명세서 등록
	public void insertStatApplyWorks(List params);

	//이용승인 신청서 등록
	public void insertStatApplication(StatApplication params);
	//이용승인 신청서 SHIS 등록
	public void insertStatApplicationShis(StatApplication params);
	//첨부파일 이용승인 신청 맵핑정보 등록
	public void insertStatAttcFile(StatApplication params);
	//이용승인 명세서 등록
	public void insertStatApplyWorks(StatApplication params);
	//이용승인 신청 인증서정보 등록
	public void insertApplySign(ApplySign params);
	//이용승인 신청 TMP 삭제
	public void deleteStatPrpsTmp(StatApplication params);
	
	//이용승인 명세서 tmp 조회
	public List statApplyWorksListTmp(StatApplyWorks params);
	
	//이용승인 명세서 조회
	public List statApplyWorksList(StatApplyWorks params);
	
	// 법정허락 결제정보 등록
	public void insertStatPayStatus(StatApplication params);
	
	// 법정허락 결제정보 수정
	public void updateStatPayStatus(StatApplication params);
	
	// 이용승인신청명세서 임시저장 카운트 20120906 정병호
	public int getStatApplyWorksCount(StatApplication params);
	
}