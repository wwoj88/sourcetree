package kr.or.copyright.mls.miplatform.dao;

import java.util.Map;

import com.ibatis.sqlmap.client.SqlMapException;

public interface AdminDao {

	public Map findAdminByLoginId(String loginId) throws SqlMapException;
}
