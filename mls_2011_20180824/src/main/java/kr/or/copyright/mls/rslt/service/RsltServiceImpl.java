package kr.or.copyright.mls.rslt.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.rslt.dao.RsltDao;

import com.tobesoft.platform.data.Dataset;

public class RsltServiceImpl extends BaseService implements RsltService {

	//private Log logger = LogFactory.getLog(getClass());

	private RsltDao rsltDao;
	
	public void setRsltDao(RsltDao rsltDao){
		this.rsltDao = rsltDao;
	}


	// rsltList 조회 
	public void prpsRsltList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");    
		//Dataset ds_loginfo	   = getDataset("ds_loginfo");												삭제요
		
		ds_condition.printDataset();
		
		//Map loginfoMap = getMap(ds_loginfo, 0);															삭제요
		Map map = getMap(ds_condition, 0);
		
		//map.put("userIdnt", (String)loginfoMap.get("USER_IDNT"));		// login_id set 	삭제요
		
		//DAO호출
		List list  = (List) rsltDao.prpsRsltList(map);		
		List pageCount = (List) rsltDao.prpsRsltListCount(map); 
		
	
	
		// 처리결과값 가져온다.
/*		
 		List rsltStatList = new ArrayList();
		int listSize = list.size();
		
		for(int i = 0; i<listSize; i++) {
			
			HashMap hMap = (HashMap)list.get(i);

			map.put("prpsSeqn", hMap.get("PRPS_SEQN"));
			
			List rsltList = (List)rsltDao.inmtPrpsRsltList(map);		// ML_PRPS_RSLT
			
	//		Collections.sort(rsltList);
			
			// 처리결과값 제어     
			
			int rsltListSize = rsltList.size();
			ArrayList aList = new ArrayList();
			
			for( int rs = 0; rs<rsltListSize; rs++) {
				
				HashMap rsMap = (HashMap)rsltList.get(rs);
				
			//	iDealStat = (String)rsMap.get("DEAL_STAT");
				
				aList.add(Integer.parseInt((String)rsMap.get("DEAL_STAT")));
			}
			
			
			Collections.sort(aList);
  
			int iMax = (Integer)Collections.max(aList);
			int iMin = (Integer)Collections.min(aList);
			int totalStat = 0;

System.out.println(i+"------iMin, "+iMin);						// min
System.out.println(i+"------iMax, "+iMax);		// max			
		
			if(  iMax == 1 ) {
				totalStat = 1;
			} else if( iMax == 2 ) {
				totalStat = 2;  
			} else if( iMax == 3 ) {
				totalStat = 3; 
			} else if(  iMax == 4 ) {

				if( iMin == 4  )
					totalStat = 4;
				else {

					boolean bFlag = false;
					
					for(int m =aList.size()-1 ; m>-1; m--) {
					
						if( iMax != (Integer)aList.get(m)) {						
							totalStat = (Integer)aList.get(m);
							bFlag = true;
						}
					
						if(bFlag) break;
					}
					
				}			
			}

			hMap.put("DEAL_STAT_STR", totalStat);
			rsltStatList.add(i, hMap);
			//list.add(i, hMap);
			
		}
*/		
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		addList("ds_page", pageCount);	
	}
	
	
	// rslt 삭제
	public void prpsRsltDelete() throws Exception {
		
		Dataset ds_delList = getDataset("ds_delList");  
	//	Dataset ds_fileList = getDataset("ds_fileList");				// 첨부파일
		
		
		// 삭제처리 전에 현재 최대상태값을 확인
		List chkDelList = new ArrayList();
		
		String sChk = "N";			// 신청이상 상태 유무
		String sRowNum = "";		// rownum
		
		for( int i = 0; i<ds_delList.getRowCount(); i++ ) {
			
			Map deleteMap = getMap(ds_delList, i);
			
			if( !rsltDao.getMaxDealStat(deleteMap).equals("1") ) {
				
				sChk = "Y"; sRowNum = i+""; break;
			}
		}
		
		// return 할 List 생성
		HashMap chkDelMap = new HashMap();
		
		chkDelMap.put("CHK", sChk);
		chkDelMap.put("ROW_NUM", sRowNum);
		
		chkDelList.add(0,chkDelMap);
	
		
		// 삭제처리 - 처리결과가 신청이상이 없는 경우만.
		if( sChk.equals("N") ) {
			
			for( int i = 0; i<ds_delList.getRowCount(); i++ ) {
				
				Map deleteMap = getMap(ds_delList, i);
				
				rsltDao.inmtKappDelete(deleteMap);			// DELETE ML_KAPP_PRPS
				rsltDao.inmtFokapoDelete(deleteMap);		// DELETE ML_FOKAPO_PRPS
				rsltDao.inmtKrtraDelete(deleteMap);			// DELETE ML_KRTRA_PRPS
				rsltDao.inmtPrpsRsltDelete(deleteMap);		// DELETE ML_PRPS_RSLT
				rsltDao.inmtPrpsDelete(deleteMap);			// DELETE ML_PRPS
				
		/*		//내권리찾기 신청 master 삭제 
				rghtDao.prpsMasterDelete(deleteMap);
			
				//내권리찾기 신청 DETAIL 삭제 		
				rghtDao.prpsDetailDelete(deleteMap);
		*/
			
				List fileList = (List)rsltDao.prpsFileList(deleteMap);
				
				// 실제파일 삭제
				for( int f=0; f<fileList.size(); f++)
				{
					Map map = (Map)fileList.get(f);
					
					 try 
					 {
						 File file = new File(map.get("FILE_PATH").toString(), map.get("REAL_FILE_NAME").toString());
					     if (file.exists()) 
					     { 
					    	 file.delete(); 
					     }
					  } 
					  catch (Exception e) 
					  {
					   e.printStackTrace();
					  } 									
				}
				
				// 디비파일삭제
				rsltDao.prpsFileDelete(deleteMap);
			}
			
			// 강명표 추가 START
			Map map = getMap(ds_delList, 0);
			map.put("CONN_URL", "신청결과조회 일괄삭제");
			rsltDao.insertConnInfo(map);
			// 강명표 추가 END
			
		}	// end if.. 삭제처리
		
		addList("ds_chkDel", chkDelList);
	}
	
}
