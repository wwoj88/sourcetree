package kr.or.copyright.mls.console.stats;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.stats.inter.FdcrAd87Dao;
import kr.or.copyright.mls.console.stats.inter.FdcrAd91Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd91Service" )
public class FdcrAd91ServiceImpl extends CommandService implements FdcrAd91Service{

	// old AdminConnDao
	@Resource( name = "fdcrAd87Dao" )
	private FdcrAd87Dao fdcrAd87Dao;

	/**
	 * 접속내역 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd91List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd87Dao.userList( commandMap );

		commandMap.put( "ds_list", list ); // 로그인 사용자 정보

	}
}
