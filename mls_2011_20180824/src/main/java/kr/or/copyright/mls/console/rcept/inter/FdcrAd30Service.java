package kr.or.copyright.mls.console.rcept.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd30Service{

	/**
	 * 미분배보상금 교과용 저작물 보고일 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd30List1( Map<String, Object> commandMap ) throws Exception;

}
