package kr.or.copyright.mls.adminStatBoard.dao;

import java.util.List;
import java.util.Map;

public interface AdminStatBoardDao {
	
	// 이용승인신청 공고 목록 카운트
	public List statBord01ListCount(Map map);
	
	//이용승인신청 공고 - 엑셀다운 2014-11-10 이병원
	public List statBord01ExcelDown(Map map);
	
	// 이용승인신청 공고 목록
	public List statBord01List(Map map);
	
	// 이용승인신청 공고 카운트
	public List statBord01RowCount(Map map);
	
	// 이용승인신청 공고 ,상당한 노력공고 - 상세
	public List statBord01Detail(Map map);
	
	// 이용승인신청 공고 등록
	public void statBord01Insert(Map map);
	
	// 이용승인신청 공고 데이터 중복 확인
	public int statBord01Select(Map map);
	
	// 이용승인신청 공고 일괄등록
	public void statBord01RegiAll(Map map);
	
	// 이용승인신청 공고 수정
	public void statBord01Update(Map map);
	
	// 이용승인신청 공고 첨부파일
	public List statBord01File(Map map);
	
	// 이용승인신청 공고 이의제기
	public List statBord01Object(Map map);
	
	// 이용승인신청 공고 이의제기 첨부파일
	public List statBord01ObjectFile(Map map);
	
	// 이의제기 신청정보 수정
	public void statBordObjcUpdate(Map map);
	
	// 이의제기 진행상태 처리결과 수정
	public void statBordObjcRsltUpdate(Map map);
	
	// 이의제기신청 처리내역등록
	public void statBordObjcShisInsert(Map map);
	
	// 이의제기 처리내역 목록조회
	public List adminStatObjcShisSelect(Map map);
	
	//이용승인신청 승인공고 게시판 - 목록
	public List statBord02List(Map map);
	
	//이용승인신청 승인공고 게시판 - 엑셀다운
	public List statBord02ExcelDown(Map map);
	
	//이용승인신청 승인공고(statBord02) , 보상금 공탁 공고 게시판(statBord03) - 상세
	public List statBordDetail(Map map);
	
	//이용승인신청 승인 공고 등록
	public void statBord02Regi(Map map);
	
	//이용승인신청 승인공고 일괄등록
	public void statBord02RegiAll(Map map);
	
	//이용승인신청 승인공고 데이터 중복 확인
	public int statBord02Select(Map map1);
	
	//이용승인 신청 승인공고 게시판 - 첨부파일 등록 1
	public void mlBord02FileInsert(Map map);
	
	//이용승인 신청 승인공고 게시판 - 첨부파일 등록 2
	public void statBord02FileInsert(Map map);
	
	// 이용승인신청 승인 공고 수정
	public void statBord02Update(Map map);
	
	// 이용승인신청 승인 공고 첨부파일 추가시 수정
	public void statBord02Modi(Map map);
	
	//  MAX_BORD_SEQ 조회
	public int getMaxBordSeqn();
	
	//이용승인신청 승인 공고 삭제
	public void statBord02Delete(Map map);
	
	//첨부파일 삭제
	public void statBord02FileDelete(Map map);
	
	//이용승인신청 승인공고 , 보상금 공탁 공고 게시판 - 목록COUNT
	public List statBord0203RowList(Map map);
	
	//보상금 공탁 공고 게시판 - 목록
	public List statBord03List(Map map);
	
	//보상금 공탁 공고 게시판 - COUNT
	public List statBord03RowCount(Map map);
	
	//보상금 공탁 공고 게시판 - 공고 승인
	public void statBord03Mgnt(Map map);
	
	//상당한 노력 공고 게시판 - 목록 
	public List statBord04List(Map map);
	
	//상당한 노력공고  게시판 - 목록COUNT
	public List statBord04RowList(Map map);
	
	//상당한 노력공고  게시판 - COUNT
	public List statBord04RowCount(Map map);
	
	//법정허락 대상 저작물 - 목록
	public List statBord05List(Map map);
	
	//법정허락 대상 저작물 - 목록COUNT
	public List statBord05RowList(Map map);
	
	//법정허락 대상 저작물 - COUNT
	public List statBord05RowCount(Map map);
	
	//법정허락 대상 저작물 - 상세
	public List statBord05Detail(Map map);
	
	// 법정허락 대상 저작물 이의제기
	public List statBord05Object(Map map);
	
	// 법정허락 대상 저작물 이의제기 첨부파일
	public List statBord05ObjectFile(Map map);
	
	//이의신청 현황 - 목록
	public List objeList(Map map);
	
	//이의신청 현황 - 목록COUNT
	public List objeRowList(Map map);
	
	//저작권자 찾기 위한 상당한 노력 신청 - 목록 
	public List statBord06List(Map map);
	
	//저작권자 찾기 위한 상당한 노력 신청 - 목록COUNT
	public List statBord06RowList(Map map);
	
	//저작권자 찾기 위한 상당한 노력 신청 - COUNT
	public List statBord06RowCount(Map map);
	
	//저작권자 찾기 위한 상당한 노력 신청  - 상세
	public List statBord06Detail(Map map);
	
	//저작권자 찾기 위한 상당한 노력 신청 (첨부파일) - 상세
	public List statBord06File(Map map);
	
	// 저작권자 찾기 위한 상당한 노력 (진행상태) - 상세
	public List statBord06Shist(Map map);
	
	// 저작권자 찾기 위한 상당한 노력 (진행상태 처리내역 팝업)- 상세
	public List statBord06ShistList(Map map);
	
	// 저작권자 찾기 위한 상당한 노력 신청(저작권자 정보) - 상세
	public List statBord06CoptHodr(Map map);
	
	//저작권자 찾기 위한 상당한 노력 신청 (진행상태) -수정
	public void statBord06ShistUpdate(Map map);
	
	// 저작권자 찾기 위한 상당한 노력 신청 (진행상태 처리내역)  -등록
	public void statBord06ShistInsert(Map map);
	
	//저작권자 찾기 위한 상당한 노력 신청 (진행상태-처리완료(승인)) -수정
	public void statBord06RsltMgnt(Map map);
	
	//저작권자 찾기 위한 상당한 노력 신청 진행상태- 첨부파일 등록 2
	public void statBord06FileInsert(Map map);
	
	// 메인화면 - 미공고 목록 (저작권자 조회공고, 보상금 공탁)
	public List mainStatBordList01 (Map map);
	
	// 메인화면 - 이의신청 목록 (이용승인신청공고, 저작권자 조회공고, 법정허락대상물)
	public List mainStatBordList02 (Map map);
	
	
	// 저작권자 조회공고, 보상금 공탁공고 처리상태 수정(신청,보완,신청승인,반려,)
	public void statBordStatUpdate(Map map);
	
	// 저작권자 조회공고, 보상금 공탁공고 삭제 
	public void statBordStatDeltUpdate(Map map);
	
	// 저작권자 조회공고, 보상금 공탁공고 보완항목 코드 목록
	public List selectCdList(Map map);
	
	// 저작권자 조회공고, 보상금 공탁공고 보완내역 보완차수 최대값	+ 1
	public int selectMaxSuplSeq(Map map);
	
	//저작권자 조회공고, 보상금 공탁공고 보완내역 보완ID 최대값 + 1
	public int selectMaxSuplId();
	
	//상당한노력신청 - 저작권자 조회공고 보완내역 보완차수 최대값 + 1
	public int selectMaxWorksSuplSeq(Map map);
	
	// 저작권자 조회공고, 보상금 공탁공고 보완내역 등록
	public void suplIdInsert(Map map);
	
	// 저작권자 조회공고, 보상금 공탁공고 보완목록 등록	
	public void suplHistInsert(Map map);
	
	// 상당한노력신청 - 저작권자 조회공고 보완목록 등록
	public void suplWorksInsert(Map map);
	
	// 저작권자 조회공고, 보상금 공탁공고 보완항목 등록
	public void suplIdItemInsert(Map map);
	
	// 저작권자 조회공고, 보상금 공탁공고 보완수정
	public void statBordSuplUpdate(Map map); 
	
	// 상당한노력신청 저작권자 조회 공고내용 수정(보완) -->
	public void statBord06SuplUpdate(Map map);
	
	// 저작권자 조회공고, 보상금 공탁공고 보완목록 불러오기
	public List selectBordSuplItemList(Map map);
	
	//상당한노력신청 - 저작권자 조회공고 보완목록 불러오기 -->
	public List selectWorksSuplItemList(Map map);

	//  상당한노력신청 - 저작권자 조회공고 삭제
	public void statBord06DeltUpdate(Map map);
	
	// 신청정보 보안안내 메일 발신내역
	public List selectBordSuplSendList(Map map);
	
	// 법정허락 대상저작물 관리 목록 삭제
	public void statBord07Update(Map map);
	
	// 법정허락 대상저작물 관리 목록 일괄 삭제
	public void statBord07UpdateAll(Map map);
}
