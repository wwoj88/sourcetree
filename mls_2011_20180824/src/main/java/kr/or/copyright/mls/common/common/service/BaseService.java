package kr.or.copyright.mls.common.common.service;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.utils.MiDataUtil;

import org.apache.commons.lang.StringUtils;

import com.tobesoft.platform.PlatformRequest;
import com.tobesoft.platform.data.Dataset;
import com.tobesoft.platform.data.DatasetList;
import com.tobesoft.platform.data.PlatformData;
import com.tobesoft.platform.data.VariableList;
import com.tobesoft.platform.data.Variant;

public class BaseService {

//	private final Log logger = LogFactory.getLog(getClass());
	
	private Map reqParam;
	private VariableList miParam;
	private DatasetList datasets;
	private PlatformData miData;

	public PlatformData execute(String methodName, PlatformRequest miRequest,
			Map param) throws Exception {
	
		reqParam = param;
		miParam = miRequest.getVariableList();
		datasets = miRequest.getDatasetList();
		miData = new PlatformData();
		
//		logger.debug("reqParam : " + reqParam);
//		logger.debug("miParam : " + miParam);
//		logger.debug("datasets : " + datasets);
		
		Class[] types = null;
		Method method = this.getClass().getMethod(methodName, types);
		method.invoke(this, null);
		
		return miData;
	}
	
	public String getReqParam(String key) throws Exception {

		String sRtn = "";

		String[] saVal = (String[]) reqParam.get(key);

		if (saVal != null) {

			for (int i = 0; i < saVal.length; i++) {

				sRtn += saVal[i] + "^";
			}
			sRtn = sRtn.substring(0, sRtn.length() - 1);
		}
		return sRtn;
	}

	public String getParam(String key) throws Exception {

		return miParam.getValueAsString(key);
	}


	public Dataset getDataset(String id) throws Exception {

		return datasets.get(id);
	}


	public HashMap getMap(String dsId) throws Exception {

		return getMap(dsId, 0);
	}

	public HashMap getMap(String dsId, int rnum) throws Exception {

		Dataset dataset = getDataset(dsId);

		if (dataset == null || dataset.getRowCount() == 0) {

			return null;
		}

		HashMap result = new HashMap();

		for (int i = 0; i < dataset.getColumnCount(); i++) {

			String columnName = dataset.getColumnId(i);

			result.put(columnName, dataset.getColumnAsObject(rnum, columnName));
		}
		return result;
	}

	public Map getMap(Dataset ds, int rnum) throws Exception {

		if (ds == null || ds.getRowCount() == 0) {

			return null;
		}

		HashMap result = new HashMap();

		for (int i = 0; i < ds.getColumnCount(); i++) {

			String columnName = ds.getColumnId(i);

			result.put(columnName, ds.getColumnAsObject(rnum, columnName));
		}
		return result;
	}
	
	public Map getDeleteMap(Dataset ds, int rnum) throws Exception {
		
		if (ds == null || ds.getDeleteRowCount() == 0) {

			return null;
		}		
		
		Map result = new HashMap();
		
		for(int i = 0; i < ds.getColumnCount(); i++) {
			
			String columnName = ds.getColumnId(i);
			result.put(columnName, StringUtils.replace(ds.getDeleteColumn(rnum, columnName).toString(), ".0", ""));
		}
		
		return result;
	}

	public List dsToList(Dataset dataset) throws Exception {

		if (dataset == null || dataset.getRowCount() == 0) {

			return null;
		}
		int count = dataset.getColumnCount();
		Map map = null;
		List list = new ArrayList();

		for (int j = 0; j < dataset.getRowCount(); j++) {

			map = new HashMap();

			for (int i = 0; i < count; i++) {

				String columnName = dataset.getColumnId(i);
				map.put(columnName, dataset.getColumnAsObject(j, columnName));
			}
			list.add(map);
		}
		return list;
	}

	public void addList(String id, Map map) throws Exception {

		List list = new ArrayList();
		
		if(map!=null && map.size()!=0) {
			
			list.add(map);
		}
		miData.addDataset(MiDataUtil.listToDataset(id, list));
	}

	public void addList(String id, List list) throws Exception {

		miData.addDataset(MiDataUtil.listToDataset(id, list));
	}

	public void addDataset(Dataset ds) throws Exception {

		miData.addDataset(ds);
	}

	public void addCommCode(String id, List list) throws Exception {

		for (int i = 0; i < getDataset(id).getRowCount(); i++) {

			miData.addDataset(MiDataUtil.listToCommCdDs(getMap(id, i), list));
		}
	}

	public void addParam(String name, String value) {

		Variant var = new Variant(value);

		miData.addVariable(name, var);
	}

	public void addParam(String name, int value) {

		Variant var = new Variant(value);

		miData.addVariable(name, var);
	}	
	
	public void setResultMessage(String msg) {

		setResultMessage(-1, msg);
	}

	public void setResultMessage(int code, String msg) {

		addParam("ErrorCode", code);
		addParam("ErrorMsg", msg);
	}
}


