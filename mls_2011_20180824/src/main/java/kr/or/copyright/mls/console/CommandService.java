package kr.or.copyright.mls.console;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import kr.or.copyright.mls.support.constant.Constants;
import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

public class CommandService extends AbstractServiceImpl {

     @Resource(name = "consoleCommonService")
     protected ConsoleCommonService consoleCommonService;

     @Resource(name = "consoleCommonDao")
     protected ConsoleCommonDao consoleCommonDao;

     /**
      * 페이징 공통
      * 
      * @param StringTagHandler
      * @param StringTagHandler
      * @throws Exception
      */
     protected Map<String, Object> pagination(Map<String, Object> commandMap, int totCnt, int recodeSize) throws Exception {

          int defaultRecordCountPerPage = Integer.parseInt(new String(Constants.getProperty("page.recordsize.per.page")));
          int defaultPageSize = Integer.parseInt(new String(Constants.getProperty("page.pagesize")));
          int currentPageNo = 1;
          String pageIndex = (String) commandMap.get("pageIndex");

          if (commandMap.containsKey("pageIndex")) {

               if (pageIndex.equals("undefined") || pageIndex.equals("")) {
                    commandMap.put("pageIndex", "1");
               } else {
                    currentPageNo = Integer.parseInt(pageIndex);

               }
          } else {
               commandMap.put("pageIndex", "1");
          }

          // PaginationInfo에 필수 정보를 넣어 준다.
          PaginationInfo paginationInfo = new PaginationInfo();
          paginationInfo.setCurrentPageNo(currentPageNo); // 현재 페이지 번호
          // 한 페이지에 게시되는 게시물 건수
          if (recodeSize == 0) {
               paginationInfo.setRecordCountPerPage(defaultRecordCountPerPage);
          } else {
               paginationInfo.setRecordCountPerPage(recodeSize);
          }

          /* 페이징 리스트의 사이즈 */
          int pageSize = EgovWebUtil.getInt(commandMap, "pageSize");
          if (pageSize > 0) {
               paginationInfo.setPageSize(pageSize);
          } else {
               paginationInfo.setPageSize(defaultPageSize);
          }

          int firstRecordIndex = paginationInfo.getFirstRecordIndex();
          int lastRecordIndex = paginationInfo.getLastRecordIndex();
          int recordCountPerPage = paginationInfo.getRecordCountPerPage();
          commandMap.put("FIRSTINDEX", firstRecordIndex);
          commandMap.put("LASTINDEX", lastRecordIndex);
          commandMap.put("FROM_NO", firstRecordIndex);
          commandMap.put("TO_NO", lastRecordIndex);

          commandMap.put("RECORDCOUNTPERPAGE", recordCountPerPage);
          paginationInfo.setTotalRecordCount(totCnt); // 전체 게시물 건 수
          commandMap.put("paginationInfo", paginationInfo);
          return commandMap;

     }
}
