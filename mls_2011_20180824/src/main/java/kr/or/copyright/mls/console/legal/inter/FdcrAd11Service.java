package kr.or.copyright.mls.console.legal.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd11Service{

	/**
	 * 법정허락 대상저작물 공고 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd11List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 대상저작물 공고 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd11View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 대상저작물 공고 게시판 등록자 정보 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd11Pop1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 이의제기 진행상태 수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd11Update1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 이의제기 진행상태 내역조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd11Pop2( Map<String, Object> commandMap ) throws Exception;
}
