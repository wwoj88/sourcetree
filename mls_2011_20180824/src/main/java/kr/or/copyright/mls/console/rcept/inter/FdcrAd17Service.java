package kr.or.copyright.mls.console.rcept.inter;

import java.util.ArrayList;
import java.util.Map;

/**
 * Class 내용 기술
 * 
 * @author : 정병호
 * @since : 20120910
 * @version : 1.0
 * @see: 위탁관리 저작물 보고
 */
public interface FdcrAd17Service {

     /**
      * 위탁관리 저작물 보고 저장
      * 
      * @param commandMap
      * @return
      * @throws Exception
      */
     public boolean fdcrAd17Insert2(Map<String, Object> commandMap) throws Exception;

     /**
      * 일괄등록
      * 
      * @param commandMap
      * @param excelList
      * @return
      * @throws Exception
      */
     public boolean fdcrAd17Insert1(Map<String, Object> commandMap, ArrayList<Map<String, Object>> excelList) throws Exception;

     /**
      * 엑셀데이터 일괄수정
      * 
      * @param commandMap
      * @param excelList
      * @throws Exception
      */
     public boolean fdcrAd17Update1(Map<String, Object> commandMap, ArrayList<Map<String, Object>> excelList) throws Exception;

     /**
      * 등록여부확인
      * 
      * @param commandMap
      * @param excelList
      * @throws Exception
      */
     public void fdcrAd17List1(Map<String, Object> commandMap, ArrayList<Map<String, Object>> excelList) throws Exception;

     /**
      * 보고저작물 없음 시 등록
      * 
      * @param commandMap
      * @throws Exception
      */
     public boolean fdcrAd17InsertNodata(Map<String, Object> commandMap) throws Exception;

     /**
      * 보고기관 검색
      * 
      * @param commandMap
      * @throws Exception
      */
     public void fdcrAd17Pop1(Map<String, Object> commandMap) throws Exception;

     /**
      * 일괄등록 신규
      * 
      * @param commandMap
      * @throws Exception
      */
     public boolean fdcrAd17InsertNew(Map<String, Object> exMap, ArrayList<Map<String, Object>> uploadList) throws Exception;

     public void fdcrAd17SendMail(Map<String, Object> mailMap);

     /**
      * 일괄수정 신규
      * 
      * @param commandMap
      * @throws Exception
      */
     public boolean fdcrAd17UpdateNew(Map<String, Object> exMap, ArrayList<Map<String, Object>> uploadList) throws Exception;

}
