package kr.or.copyright.mls.console.stats;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.stats.inter.FdcrAd87Dao;
import kr.or.copyright.mls.console.stats.inter.FdcrAd96Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd96Service" )
public class FdcrAd96ServiceImpl extends CommandService implements FdcrAd96Service{

	// old AdminConnDao
	@Resource( name = "fdcrAd87Dao" )
	private FdcrAd87Dao fdcrAd87Dao;

	/**
	 * 연도별 신청건수, 승인건수
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd96List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd87Dao.statYearApplyList( commandMap );
		commandMap.put( "ds_list1", list );
	}

	/**
	 * 연도별 저작물, 실연, 음반, 방송, 데이터베이스별 건수
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd96List2( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd87Dao.statYearSpplyList( commandMap );
		commandMap.put( "ds_list2", list );
	}

	/**
	 * 연도별 보상금액별
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd96List3( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd87Dao.statYearLgmtAmntList( commandMap );
		commandMap.put( "ds_list3", list );
	}
}
