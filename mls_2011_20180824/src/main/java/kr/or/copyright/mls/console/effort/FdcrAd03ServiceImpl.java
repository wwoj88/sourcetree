package kr.or.copyright.mls.console.effort;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.mail.SendMail;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd0301Dao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Service;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;

import org.springframework.stereotype.Service;

import com.tobesoft.platform.data.Dataset;

@Service( "fdcrAd03Service" )
public class FdcrAd03ServiceImpl extends CommandService implements FdcrAd03Service{

	// OldDao adminStatProcDao
	@Resource( name = "fdcrAd03Dao" )
	private FdcrAd03Dao fdcrAd03Dao;

	// OldDao adminEvnSetDao
	@Resource( name = "fdcrAd0301Dao" )
	private FdcrAd0301Dao fdcrAd0301Dao;

	/**
	 * 상당한노력 진행현황 조회
	 * 
	 * @throws Exception
	 */
	public void fdcrAd03List1( Map<String, Object> commandMap ) throws Exception{
		// DAO 호출
		List list = (List) fdcrAd03Dao.selectStatProcAllLogList( commandMap );
		List works_list = (List) fdcrAd03Dao.selectStatWorksAllLogList( commandMap );
		List set_list = (List) fdcrAd0301Dao.statProcSetList( commandMap );

		commandMap.put( "ds_works_list", works_list );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_set_list", set_list );
	}

	/**
	 * 대상저작물 수집-프로시져 호출
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List2( Map<String, Object> commandMap ) throws Exception{
		// DAO 호출
		fdcrAd03Dao.callProcGetStatProcOrd( commandMap );
		List list = (List) fdcrAd03Dao.selectStatProcAllLogList( commandMap );

		commandMap.put( "ds_list", list );
	}

	/**
	 * 법정허락 대상저작물 수집-프로시져 호출
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List3( Map<String, Object> commandMap ) throws Exception{
		// DAO 호출
		// fdcrAd03Dao.callGetStatWorksord( commandMap );
		List list = (List) fdcrAd03Dao.selectStatWorksAllLogList( commandMap );

		commandMap.put( "ds_works_list", list );
	}

	/**
	 * 대상 저작물 목록 팝업
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List4( Map<String, Object> commandMap ) throws Exception{
		String worksDivsCd = commandMap.get( "WORKS_DIVS_CD" ).toString();

		List excpList = new ArrayList();
		List excpPageCount = new ArrayList();

		if( worksDivsCd.equals( "1" ) ){
			excpList = (List) fdcrAd03Dao.statProcTargExcpList( commandMap );
			excpPageCount = (List) fdcrAd03Dao.statProcPopListCount( commandMap );
		}

		List pageCount = (List) fdcrAd03Dao.statProcTargPopListCount( commandMap );
		int totCnt = ((BigDecimal)((Map)pageCount.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		
		List list = (List) fdcrAd03Dao.statProcTargPopList( commandMap );
		
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageCount );
		commandMap.put( "ds_excpList", excpList );
		commandMap.put( "ds_page_excp", excpPageCount );
	}

	/**
	 * 상당한노력 현황 조회 - 결과보기팝업
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List5( Map<String, Object> commandMap ) throws Exception{
		// DAO 호출
		List pageCount = (List) fdcrAd03Dao.statProcPopListCount( commandMap );
		int totCnt = ( (BigDecimal) ( (Map) pageCount.get( 0 ) ).get( "COUNT" ) ).intValue();
		pagination( commandMap, totCnt, 0 );
		List list = (List) fdcrAd03Dao.statProcPopup( commandMap );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageCount );
	}
	
	/**
	 * 상당한노력 현황 조회 - 결과보기팝업
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List5Ajax( Map<String, Object> commandMap ) throws Exception{
		// DAO 호출
		List pageCount = (List) fdcrAd03Dao.statProcPopListCount( commandMap );
		int totCnt = ( (BigDecimal) ( (Map) pageCount.get( 0 ) ).get( "COUNT" ) ).intValue();
		pagination( commandMap, totCnt, 0 );
		List list = (List) fdcrAd03Dao.statProcPopup( commandMap );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageCount );
	}
	
	/**
	 * 상당한노력 현황 엑셀다운로드
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03Down2( Map<String, Object> commandMap ) throws Exception{
		// DAO 호출
		List list = (List) fdcrAd03Dao.statProcPopupNewExcel( commandMap );
		commandMap.put( "ds_list", list );
	}
	
	/**
	 * 법정허락 결과 팝업 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List7(Map<String, Object> commandMap) throws Exception {
		// DAO 호출
		List pageCount = (List) fdcrAd03Dao.statProcWorksPopListCount(commandMap);
		int totCnt = ( (BigDecimal) ( (Map) pageCount.get( 0 ) ).get( "COUNT" ) ).intValue();
		pagination( commandMap, totCnt, 0 );
		List list = (List) fdcrAd03Dao.statProcWorksPopup(commandMap);
		
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageCount );
		
	}
	
	/**
	 * 법정허락 결과 엑셀 다운로드
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03Down3(Map<String, Object> commandMap) throws Exception {
		List list = (List) fdcrAd03Dao.statProcWorksPopupNewExcel(commandMap);
		commandMap.put( "ds_list", list );
	}

	/**
	 * 상당한노력 - 실행, 상당한 노력 진행(프로세스 분기 20121108)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Exe1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			int iBase = 500; // 프로시저 실행 구분 건수
			int iMod = 0;
			int iTargCnt = 0;
			int iRoop = 0;

			iTargCnt = EgovWebUtil.getToInt( commandMap, "TARG_WORKS_CONT" );

			// 단위 건수 셋팅
			iRoop = ( iTargCnt / iBase );
			iMod = ( iTargCnt % iBase );

			System.out.println( "=iTargCnt : " + iTargCnt );
			System.out.println( "=iRoop : " + iRoop );
			System.out.println( "=iMod : " + iMod );
			if( iMod > 0 ) iRoop++;
			System.out.println( "=iRoop : " + iRoop );

			int iFrom = 0;
			int iTo = 0;

			// 01. 상당한노력 공고
			for( int i = 0; i < iRoop; i++ ){

				iFrom = ( i * iBase ) + 1;
				iTo = ( i * iBase ) + iBase;
				if( iTo > iTargCnt ) iTo = iTargCnt;

				commandMap.put( "FROM", iFrom );
				commandMap.put( "TO", iTo );

				System.out.println( "*** 공고 roop >> " + i + " :: " + iFrom + ", " + iTo );

				// call 프로시저
				fdcrAd03Dao.callStatProc01( commandMap );

			}

			// 02. 매칭
			for( int i = 0; i < iRoop; i++ ){

				iFrom = ( i * iBase ) + 1;
				iTo = ( i * iBase ) + iBase;
				if( iTo > iTargCnt ) iTo = iTargCnt;

				commandMap.put( "FROM", iFrom );
				commandMap.put( "TO", iTo );

				System.out.println( "*** 매칭 roop >> " + i + " :: " + iFrom + ", " + iTo );

				// call 프로시저
				fdcrAd03Dao.callStatProc02( commandMap );

			}

			// 메일링 (거소불명 대상조회 +send) + 예외처리 아닌경우
			if( commandMap.get( "WORKS_DIVS_CD" ).equals( "2" ) && !preCheckSchedule( "20" ) ){ // 거소불명

				commandMap.put( "STAT_PROC_YN", "N" ); // 수행대상 제외
				commandMap.put( "STAT_CHNG_YN", "N" );
				commandMap.put( "WORKS_DIVS_CD", "4" );

				// 메일공통정보 set
				commandMap.put( "MAIL_TITL", "저작권자 찾기 위한 상당한노력 수행 완료" );
				// map.put("MAIL_MENT", "");
				commandMap.put( "MAIL_RESULT", "법정허락 전환이 불가능한 저작물 입니다." );

				this.sendResultMail( commandMap );
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	// 스케쥴링 사전체크 (1. 자동화설정/)
	private boolean preCheckSchedule( String setCd ){

		boolean bFlag = false;
		HashMap map = new HashMap();

		map.put( "SET_CD", setCd );

		// 0. 자동화 설정여부 확인
		List set_list = (List) fdcrAd0301Dao.statProcSetList( map );
		HashMap setMap = (HashMap) set_list.get( 0 );

		if( setMap.get( "USE_YN" ).equals( "1" ) ) bFlag = true;

		return bFlag;
	}

	// 상당한노력 결과 메일링 (거소불명 신청건만 사용)
	public void sendResultMail( Map map ) throws Exception{

		List mailList = (List) fdcrAd03Dao.statProcTargWorksMailList( map );

		for( int i = 0; i < mailList.size(); i++ ){

			Map mailMap = (HashMap) mailList.get( i );

			mailMap.put( "MAIL_TITL", map.get( "MAIL_TITL" ) );
			mailMap.put( "MAIL_RESULT", map.get( "MAIL_RESULT" ) );
			mailMap.put( "MAIL_WORKS_INFO", mailMap.get( "WORKS_TITLE" ) + " (" + mailMap.get( "RGST_DTTM" ) + ")" );

			this.sendMail( mailMap );
		}
	}

	public void sendMail( Map mailMap ) throws Exception{

		if( mailMap.get( "MAIL_ADDR" ) != null ){

			System.out.println( "==================================================" );
			System.out.println( ">> " + mailMap.get( "MAIL_ADDR" ).toString() );
			System.out.println( "==================================================" );

			String subject = "";

			String host = Constants.MAIL_SERVER_IP; // smtp서버
			String to = mailMap.get( "MAIL_ADDR" ).toString(); // 수신EMAIL
			String toName = mailMap.get( "USER_NAME" ).toString(); // 수신인

			String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
			String fromName = Constants.SYSTEM_NAME;

			String mailTitl = (String) mailMap.get( "MAIL_TITL" );
			String mailWorksInfo = (String) mailMap.get( "MAIL_WORKS_INFO" ); // 저작물정보
			String mailResult = (String) mailMap.get( "MAIL_RESULT" ); // 수행결과

			// ------------------------- mail 정보
			// ----------------------------------//
			MailInfo info = new MailInfo();

			info.setFrom( from );
			info.setFromName( fromName );
			info.setHost( host );
			info.setEmail( to );
			info.setEmailName( toName );
			info.setSubject( subject );

			subject = mailTitl + "이 완료되었습니다.";

			StringBuffer sb = new StringBuffer();
			sb.append( "				<tr>    \n" );
			sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">    \n" );
			sb.append( "						<p style=\"font-weight:bold; background:url(" + Constants.DOMAIN_HOME
				+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">" + mailTitl
				+ " 정보</p>    \n" );

			sb.append( "						<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">    \n" );

			sb.append( "							<li style=\"background:url(" + Constants.DOMAIN_HOME
				+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">신청정보 : " + mailWorksInfo
				+ "</li>    \n" );
			sb.append( "							<li style=\"background:url(" + Constants.DOMAIN_HOME
				+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">수행결과 : " + mailResult
				+ "</li>    \n" );
			sb.append( "						</ul>    \n" );

			sb.append( "					</td>    \n" );
			sb.append( "				</tr>    \n" );

			info.setMessage( sb.toString() );
			info.setSubject( subject );
			info.setMessage( new String( MailMessage.getChangeInfoMailText2( info ) ) );

			MailManager manager = MailManager.getInstance();

			// //////////////////////////////////////////////////////////////////////////////
			/*
			 * HashMap insertMap = new HashMap(); insertMap.put("EMAIL_TITLE",
			 * info.getSubject()); insertMap.put("EMAIL_DESC",
			 * info.getMessage()); insertMap.put("EMAIL_ADDR", info.getEmail());
			 * adminStatProcDao.insertMailList(insertMap);
			 */
			// ////////////////////////////////////////////////////////////////////////////
			// //

			info = manager.sendMail( info );

			if( info.isSuccess() ){
				System.out.println( ">>>>>>>>>>>> message success :" + info.getEmail() );
			}else{
				System.out.println( ">>>>>>>>>>>> message false   :" + info.getEmail() );
			}

		}
	}

	/**
	 * 상당한 노력 예외처리완료
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Exe2( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd03Dao.statProcAllLogUpdate( commandMap );

			// 메일링 (거소불명 대상조회 +send) + 예외처리 경우
			if( commandMap.get( "WORKS_DIVS_CD" ).equals( "2" ) && preCheckSchedule( "20" ) ){ // 거소불명

				commandMap.put( "STAT_PROC_YN", "N" ); // 수행대상 제외
				commandMap.put( "STAT_CHNG_YN", "N" );
				commandMap.put( "WORKS_DIVS_CD", "4" );

				// 메일공통정보 set
				commandMap.put( "MAIL_TITL", "저작권자 찾기 위한 상당한노력 수행 완료" );
				// map.put("MAIL_MENT", "");
				commandMap.put( "MAIL_RESULT", "법정허락 전환이 불가능한 저작물 입니다." );

				this.sendResultMail( commandMap );
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 법정허락 대상 전환 실행
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Exe3( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			int iBase = 10; // 프로시저 실행 구분 건수
			int iMod = 0;
			int iTargCnt = 0;
			int iRoop = 0;

			iTargCnt = EgovWebUtil.getToInt( commandMap, "TARG_WORKS_CONT" );

			/*
			 * if(ds_list_proc.getColumnAsString(0, "WORKS_DIVS_CD") == "1") //
			 * 미분배 map.put("WORKS_DIVS_CD", "1,2,3"); else
			 * map.put("WORKS_DIVS_CD", "4"); // 거소불명
			 */

			// 단위 건수 셋팅
			iRoop = ( iTargCnt / iBase );
			iMod = ( iTargCnt % iBase );

			System.out.println( "=iTargCnt : " + iTargCnt );
			System.out.println( "=iRoop : " + iRoop );
			System.out.println( "=iMod : " + iMod );
			if( iMod > 0 ) iRoop++;
			System.out.println( "=iRoop : " + iRoop );

			int iFrom = 0;
			int iTo = 0;

			// 전체로그 : start
			for( int i = 0; i < iRoop; i++ ){

				iFrom = ( i * iBase ) + 1;
				iTo = ( i * iBase ) + iBase;
				if( iTo > iTargCnt ) iTo = iTargCnt;

				commandMap.put( "FROM", iFrom );
				commandMap.put( "TO", iTo );

				System.out.println( "*** roop test >> " + i + " :: " + iFrom + ", " + iTo );
				// call 프로시저
				fdcrAd03Dao.callChangeStatWorks( commandMap );

			}
			// 전체로그 : end

			// 메일링 (거소불명 대상조회 +send)
			int WORKS_DIVS_CONT_02 = EgovWebUtil.getToInt( commandMap, "WORKS_DIVS_CONT_02" );
			if( WORKS_DIVS_CONT_02 > 0 ){ // 거소불명 카운트가 0보다 클때
				commandMap.put( "STAT_CHNG_YN", "Y" ); // 전환대상
				commandMap.put( "STAT_PROC_YN", "Y" );
				commandMap.put( "WORKS_DIVS_CD", "4" );

				// 메일공통정보 set
				commandMap.put( "MAIL_TITL", "저작권자 찾기 위한 상당한노력 수행 완료" );
				// map.put("MAIL_MENT", "");
				commandMap.put( "MAIL_RESULT", "법정허락 대상 저작물로 전환이 완료되었습니다. 법정허락 이용승인신청이 가능합니다." );

				this.sendResultMail( commandMap );
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 상당한노력 자동화 수행(스케쥴링 (분기. 20121109) + 자동화설정추가(20131018))
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Schedule1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			boolean bFlag = false; // 스케쥴링 실행 플래그
			String sMailMent = "상당한노력 수행 대상저작물 전환대상이 없습니다. <BR/> 예외처리 대상이 존재하는 경우 [완료]처리해주세요.";
			List resultList = new ArrayList();

			try{

				HashMap map = new HashMap();

				// 0. 자동화 설정여부 확인
				bFlag = preCheckSchedule( "10" ); // 10 : 상당한노력 대상저작물 자동수행, 20 :
													// 예외처리직접수행 ,30 : 법정허락전환
													// 자동수행

				if( bFlag ){

					// 1. 상당한 노력 대상 저작물 수집
					map.put( "P_SCH_YN", "Y" ); // 자동화 "Y"
					map.put( "P_TRST_ORGN_CODE", "199" );
					fdcrAd03Dao.callProcGetStatProcOrd( map );
					map.put( "P_TRST_ORGN_CODE", "200" );
					fdcrAd03Dao.callProcGetStatProcOrd( map );

					map.put( "P_TRST_ORGN_CODE", "" );
					map.put( "SCHEDULE_SRCH", "Y" );

					// 2. 실행대상 저작물 수집
					ArrayList list = (ArrayList) fdcrAd03Dao.selectStatProcAllLogList( map );

					// 3. 실행 -- 수집완료인건만; (STAT_CD") == "2")
					System.out.println( "     >>>>> 상당한노력 START " );

					if( list.size() > 0 ){

						for( int iTot = 0; iTot < list.size(); iTot++ ){

							HashMap statMap = (HashMap) list.get( iTot );

							// 수집완료인건만..
							if( Integer.parseInt( String.valueOf( statMap.get( "STAT_CD" ) ) ) == 2 ){

								// 결과조회용 key 값 수집
								resultList.add( statMap.get( "ORD" ) + "|" + statMap.get( "WORKS_DIVS_CD" ) );

								int iBase = 10; // 프로시저 실행 구분 건수
								int iMod = 0;
								int iTargCnt = 0;
								int iRoop = 0;

								iTargCnt = Integer.parseInt( String.valueOf( statMap.get( "TARG_WORKS_CONT" ) ) );

								// 단위 건수 셋팅
								iRoop = ( iTargCnt / iBase );
								iMod = ( iTargCnt % iBase );

								System.out.println( "=iTargCnt : " + iTargCnt );
								System.out.println( "=iRoop : " + iRoop );
								System.out.println( "=iMod : " + iMod );
								if( iMod > 0 ) iRoop++;
								System.out.println( "=iRoop : " + iRoop );

								int iFrom = 0;
								int iTo = 0;

								// 01. 상당한노력 공고
								for( int i = 0; i < iRoop; i++ ){

									iFrom = ( i * iBase ) + 1;
									iTo = ( i * iBase ) + iBase;
									if( iTo > iTargCnt ) iTo = iTargCnt;

									statMap.put( "FROM", iFrom );
									statMap.put( "TO", iTo );

									System.out.println( "*** 공고 roop >> " + i + " :: " + iFrom + ", " + iTo );

									// call 프로시저
									fdcrAd03Dao.callStatProc01( statMap );

								} // ..end 01. 상당한노력 공고

								// 02. 매칭
								for( int i = 0; i < iRoop; i++ ){

									iFrom = ( i * iBase ) + 1;
									iTo = ( i * iBase ) + iBase;
									if( iTo > iTargCnt ) iTo = iTargCnt;

									statMap.put( "FROM", iFrom );
									statMap.put( "TO", iTo );
									// statMap.put("P_SCH_YN", "Y"); // 자동화 "Y"

									System.out.println( "*** 매칭 roop >> " + i + " :: " + iFrom + ", " + iTo );

									// call 프로시저
									fdcrAd03Dao.callStatProc02( statMap );

								} // ..end 02.매칭

							} // .. end if

							// 메일링 (거소불명 대상조회 +send) + 예외처리 아닌경우
							if( String.valueOf( statMap.get( "WORKS_DIVS_CD" ) ).equals( "2" )
								&& !preCheckSchedule( "20" ) ){ // 거소불명

								statMap.put( "STAT_PROC_YN", "N" ); // 수행대상 제외
								statMap.put( "STAT_CHNG_YN", "N" );
								statMap.put( "WORKS_DIVS_CD", "4" );

								// 메일공통정보 set
								statMap.put( "MAIL_TITL", "저작권자 찾기 위한 상당한노력 수행 완료" );
								// map.put("MAIL_MENT", "");
								statMap.put( "MAIL_RESULT", "법정허락 전환이 불가능한 저작물 입니다." );

								this.sendResultMail( statMap );
							}

						} // ..end for

					}

					/* *** 메일링 설정 START ********** */
					HashMap mailMap = new HashMap();
					mailMap.put( "result", resultList );

					int excpTargCnt = 0; // 예외처리 대상여부

					if( list.size() > 0 ){

						ArrayList mailList = (ArrayList) fdcrAd03Dao.selectStatProcAllLogList( mailMap );

						// 메일전달정보
						int mailListSize = mailList.size();

						int ord[] = new int[mailListSize];
						String yyyymm[] = new String[mailListSize];
						String works_divs_name[] = new String[mailListSize];
						int rslt_cont_ys[] = new int[mailListSize];
						int rslt_cont_no[] = new int[mailListSize];

						for( int i = 0; i < mailListSize; i++ ){
							HashMap subMap = (HashMap) mailList.get( i );

							if( String.valueOf( subMap.get( "EXCP_STAT_CD" ) ).equals( "20" ) ) excpTargCnt++;

							ord[i] = Integer.parseInt( String.valueOf( subMap.get( "ORD" ) ) );
							yyyymm[i] = (String) subMap.get( "YYYYMM" );
							works_divs_name[i] = (String) subMap.get( "WORKS_DIVS_NAME" );

							rslt_cont_ys[i] = Integer.parseInt( String.valueOf( subMap.get( "RSLT_CONT_1ST_YS" ) ) );
							rslt_cont_ys[i] += Integer.parseInt( String.valueOf( subMap.get( "RSLT_CONT_2ND_YS" ) ) );
							rslt_cont_ys[i] += Integer.parseInt( String.valueOf( subMap.get( "RSLT_CONT_3RD_YS" ) ) );

							rslt_cont_no[i] = Integer.parseInt( String.valueOf( subMap.get( "RSLT_CONT_1ST_NO" ) ) );
						}

						mailMap.put( "ORD", ord );
						mailMap.put( "YYYYMM", yyyymm );
						mailMap.put( "WORKS_DIVS_NAME", works_divs_name );
						mailMap.put( "RSLT_CONT_YS", rslt_cont_ys );
						mailMap.put( "RSLT_CONT_NO", rslt_cont_no );

						// 1)예외처리 직접수행 여부에 따라 안내메일 구성 다름
						// if( preCheckSchedule("20") ){
						if( preCheckSchedule( "20" ) && excpTargCnt > 0 ){
							// 1-1) 직접수행 : 완료안내 및 예외처리 요청 mailing
							sMailMent = "상당한노력 예외처리 직접수행을 진행하여 [완료]처리해주세요.";
						}else{
							// 1-2) none : 완료안내 및 법정허락전환 자동수행 안내 mailing
							sMailMent = "법정허락 대상저작물 전환을 직접수행해주세요.";

							if( preCheckSchedule( "30" ) ){
								sMailMent = "자동화설정에 따라 법정허락 대상저작물 전환이 자동수행됩니다.";
								// 법정허락 자동수행 호출
							}
						}
					}

					mailMap.put( "MAIL_MENT", sMailMent );
					mailMap.put( "MAIL_TITL", "상당한노력 자동화 수행" );
					mailMap.put( "MGNT_DIVS", "STAT_PROC" );

					List reclist = consoleCommonDao.getMgntMail( mailMap );

					int listSize = reclist.size();

					String to[] = new String[listSize];
					String toName[] = new String[listSize];

					for( int i = 0; i < listSize; i++ ){
						HashMap subMap = (HashMap) reclist.get( i );

						toName[i] = (String) subMap.get( "USER_NAME" );
						to[i] = (String) subMap.get( "MAIL" );
					}

					mailMap.put( "TO", to );
					mailMap.put( "TO_NAME", toName );

					SendMail.send( mailMap );

					/* *** 메일링 설정 END ********** */
					if( ( !preCheckSchedule( "20" ) || ( preCheckSchedule( "20" ) && excpTargCnt == 0 ) )
						&& preCheckSchedule( "30" ) ){
						fdcrAd03Schedule2( commandMap ); // 법정허락 자동수행 호출
					}

				} // .. end if
				System.out.println( "     >>>>> 상당한노력 END " );

			}
			catch( Exception e ){
				e.printStackTrace();
			}
			finally{

			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 법정허락 전환 자동화
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Schedule2( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			boolean bFlag = false; // 스케쥴링 실행 플래그
			String sMailMent = "법정허락 대상저작물 전환대상이 없습니다.";
			List resultList = new ArrayList();

			try{

				HashMap map = new HashMap();

				// 0. 자동화 설정여부 확인
				bFlag = preCheckSchedule( "30" ); // 10 : 상당한노력 대상저작물 자동수행, 20 :
													// 예외처리직접수행 ,30 : 법정허락전환
													// 자동수행

				if( bFlag ){

					// 1. 법정허락 전환 대상 저작물 수집
					map.put( "P_SCH_YN", "Y" ); // 자동화 "Y"
					map.put( "SCHEDULE_SRCH", "Y" );

					fdcrAd03Dao.callGetStatWorksord( map );

					// 2. 실행대상 저작물 수집
					ArrayList list = (ArrayList) fdcrAd03Dao.selectStatWorksAllLogList( map );

					// 3. 실행 -- 수집완료인건만; (STAT_CD") == "2")
					System.out.println( "     >>>>> 법정허락전환 START " );

					if( list.size() > 0 ){

						for( int iTot = 0; iTot < list.size(); iTot++ ){

							HashMap statMap = (HashMap) list.get( iTot );

							// 수집완료인건만..
							if( Integer.parseInt( String.valueOf( statMap.get( "STAT_CD" ) ) ) == 2 ){

								// 결과조회용 key 값 수집
								resultList.add( statMap.get( "STAT_ORD" ) + "|" + statMap.get( "YYYYMM" ) );

								int iBase = 10; // 프로시저 실행 구분 건수
								int iMod = 0;
								int iTargCnt = 0;
								int iRoop = 0;

								iTargCnt = Integer.parseInt( String.valueOf( statMap.get( "TARG_WORKS_CONT" ) ) );

								// 단위 건수 셋팅
								iRoop = ( iTargCnt / iBase );
								iMod = ( iTargCnt % iBase );

								System.out.println( "=iTargCnt : " + iTargCnt );
								System.out.println( "=iRoop : " + iRoop );
								System.out.println( "=iMod : " + iMod );
								if( iMod > 0 ) iRoop++;
								System.out.println( "=iRoop : " + iRoop );

								int iFrom = 0;
								int iTo = 0;

								// 전체로그 : start
								for( int i = 0; i < iRoop; i++ ){

									iFrom = ( i * iBase ) + 1;
									iTo = ( i * iBase ) + iBase;
									if( iTo > iTargCnt ) iTo = iTargCnt;

									statMap.put( "FROM", iFrom );
									statMap.put( "TO", iTo );

									System.out.println( "*** 전환 roop >> " + i + " :: " + iFrom + ", " + iTo );
									// call 프로시저
									fdcrAd03Dao.callChangeStatWorks( statMap );

								} // .. end for

								// 메일링 (거소불명 대상조회 +send)
								if( Integer.parseInt( String.valueOf( statMap.get( "WORKS_DIVS_CONT_02" ) ) ) > 0 ){ // 거소불명
																														// 카운트가
																														// 0보다
																														// 클때
									statMap.put( "STAT_CHNG_YN", "Y" ); // 전환대상
									statMap.put( "STAT_PROC_YN", "Y" );
									statMap.put( "WORKS_DIVS_CD", "4" );

									// 메일공통정보 set
									statMap.put( "MAIL_TITL", "저작권자 찾기 위한 상당한노력 수행 완료" );
									// map.put("MAIL_MENT", "");
									statMap.put( "MAIL_RESULT", "법정허락 대상 저작물로 전환이 완료되었습니다. 법정허락 이용승인신청이 가능합니다." );

									this.sendResultMail( statMap );
								}

							} // .. end if

						} // ..end for

					}

					/* *** 메일링 설정 START ********** */
					HashMap mailMap = new HashMap();
					mailMap.put( "result", resultList );

					if( list.size() > 0 ){

						sMailMent = "";

						ArrayList mailList = (ArrayList) fdcrAd03Dao.selectStatWorksAllLogList( mailMap );

						// 메일전달정보
						int mailListSize = mailList.size();

						int ord[] = new int[mailListSize];
						String yyyymm[] = new String[mailListSize];
						int targ_works_cont[] = new int[mailListSize];
						int rslt_cont[] = new int[mailListSize];

						for( int i = 0; i < mailListSize; i++ ){
							HashMap subMap = (HashMap) mailList.get( i );

							ord[i] = Integer.parseInt( String.valueOf( subMap.get( "STAT_ORD" ) ) );
							yyyymm[i] = (String) subMap.get( "YYYYMM" );
							targ_works_cont[i] = Integer.parseInt( String.valueOf( subMap.get( "TARG_WORKS_CONT" ) ) );
							rslt_cont[i] = Integer.parseInt( String.valueOf( subMap.get( "RSLT_CONT" ) ) );
						}

						mailMap.put( "ORD", ord );
						mailMap.put( "YYYYMM", yyyymm );
						mailMap.put( "TARG_WORKS_CONT", targ_works_cont );
						mailMap.put( "RSLT_CONT", rslt_cont );
					}

					// 법정허락 전환 수행완료 메일링
					mailMap.put( "MAIL_MENT", sMailMent );
					mailMap.put( "MAIL_TITL", "법정허락 대상저작물 전환 자동화 수행" );
					mailMap.put( "MGNT_DIVS", "STATWORK_PROC" );

					List reclist = consoleCommonDao.getMgntMail( mailMap );

					int listSize = reclist.size();

					String to[] = new String[listSize];
					String toName[] = new String[listSize];

					for( int i = 0; i < listSize; i++ ){
						HashMap subMap = (HashMap) reclist.get( i );

						toName[i] = (String) subMap.get( "USER_NAME" );
						to[i] = (String) subMap.get( "MAIL" );
					}

					mailMap.put( "TO", to );
					mailMap.put( "TO_NAME", toName );

					SendMail.send( mailMap );
					/* *** 메일링 설정 END ********** */

				} // .. end if
				System.out.println( "     >>>>> 법정허락전환  END " );

				// 법정허락대상 전환완료 mailing 추가

			}
			catch( Exception e ){
				e.printStackTrace();
			}
			finally{

			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 상당한노력 자동화 설정 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd03List6( Map<String, Object> commandMap ) throws Exception{

		List list = (List) fdcrAd0301Dao.statProcSetList( commandMap );

		commandMap.put( "ds_list_stat", list );
	}

	/**
	 * 대상저작물전환, 예외처리 취소
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd03Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd03Dao.chgStatWorksUpdateYn( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}
}
