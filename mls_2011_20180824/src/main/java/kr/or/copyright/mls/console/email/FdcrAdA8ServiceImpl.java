package kr.or.copyright.mls.console.email;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.email.inter.FdcrAdA4Dao;
import kr.or.copyright.mls.console.email.inter.FdcrAdA8Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdA8Service" )
public class FdcrAdA8ServiceImpl extends CommandService implements FdcrAdA8Service{

	@Resource( name = "fdcrAdA4Dao" )
	private FdcrAdA4Dao fdcrAdA4Dao;

	/**
	 * 월별 자동메일 발송 관리 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA8List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA4Dao.selectScMailingList( commandMap );
		List list2 = (List) fdcrAdA4Dao.selectScMailingMsgId( commandMap );

		commandMap.put( "ds_list", list );
		commandMap.put( "ds_msg_Id", list2 );

	}

	/**
	 * 발신내역 수신확인 팝업
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA8Pop1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA4Dao.selectSendEmailView( commandMap );
		List fileList = (List) fdcrAdA4Dao.selectMailAttcList( commandMap );

		commandMap.put( "ds_view", list );
		commandMap.put( "ds_detail", fileList );

	}

	/**
	 * 발송 관리 저장
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA8Insert1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			String[] CHECKED_VALUE = (String[]) commandMap.get( "CHECKED_VALUE" );
			//String CHECKED_VALUE = (String) commandMap.get( "CHECKED_VALUE" );
		    //String[] array;
		    //array = CHECKED_VALUE.split("_");
			for( int i = 0; i < CHECKED_VALUE.length; i++ ){
				String[] VALUE = CHECKED_VALUE[i].split( "_" );
			
				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "MSG_ID", commandMap.get( "MSG_ID" ) );

				Map<String, Object> map2 = new HashMap<String, Object>();
				map2.put( "MAILING_MGNT_CD",  commandMap.get( "MAILING_MGNT_CD" ) );
				map2.put( "YEAR", commandMap.get( "YEAR" ) );
				map2.put( "MSG_STOR_CD", commandMap.get( "MSG_STOR_CD" ) );

				Map<String, Object> map3 = new HashMap<String, Object>();
				map3.put( "MSG_ID", map.get( "MSG_ID" ) );
				map3.put( "MAILING_MGNT_CD", map2.get( "MAILING_MGNT_CD" ) );
				//map3.put( "USE_YSNO", VALUE[0] );
				//map3.put( "YEAR_MONTH", VALUE[1] );
				map3.put( "USE_YSNO", VALUE[0] );
				map3.put( "YEAR_MONTH", VALUE[1] );
				
				int count = fdcrAdA4Dao.selectScMailingCount( map3 );

				if( count > 0 ){
					fdcrAdA4Dao.updateScMailng( map3 );
				}else{
					map3.put( "STAT_CD", 0 );
					map3.put( "START_DTTM",null);
					map3.put( "END_DTTM", null );
					fdcrAdA4Dao.insertScMailing( map3 );
				}
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 메일 주소록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA8List2( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list2 = (List) fdcrAdA4Dao.selectEmailAddrGroupList( commandMap );
		List list = (List) fdcrAdA4Dao.selectEmailAddrList( commandMap );

		commandMap.put( "ds_list", list );
		commandMap.put( "ds_menu", list2 );

	}

}
