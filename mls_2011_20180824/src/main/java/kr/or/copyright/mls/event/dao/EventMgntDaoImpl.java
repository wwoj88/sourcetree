package kr.or.copyright.mls.event.dao;

import java.util.List;

import kr.or.copyright.mls.event.model.EventMgntVO;
import kr.or.copyright.mls.event.model.QnABoardVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;

@Repository
public class EventMgntDaoImpl extends SqlMapClientDaoSupport implements EventMgntDao{

    @Autowired
    public EventMgntDaoImpl(SqlMapClient sqlMapClient) {
        super();
        setSqlMapClient(sqlMapClient);
    }

	public List getEventUserList(EventMgntVO vo) {
		return getSqlMapClientTemplate().queryForList("eventMgnt.getEventUserList", vo);
	}

	public int getEventUserListTotal(EventMgntVO vo) {
		return (Integer) getSqlMapClientTemplate().queryForObject("eventMgnt.getEventUserListTotal",vo);
	}

	public EventMgntVO getEventUserDetail(EventMgntVO vo) {
		return (EventMgntVO) getSqlMapClientTemplate().queryForObject("eventMgnt.getEventUserDetail", vo);
	}

	public List getEventView02List(EventMgntVO vo) {
		return getSqlMapClientTemplate().queryForList("eventMgnt.getEventView02List", vo);
	}

	public int getEventView02Total(EventMgntVO vo) {
		return (Integer) getSqlMapClientTemplate().queryForObject("eventMgnt.getEventView02Total", vo);
	}

	public List getEventAgreeItem(EventMgntVO vo) {
		return getSqlMapClientTemplate().queryForList("eventMgnt.getEventAgreeItem", vo);
	}

	public int getEventPartCnt(EventMgntVO vo) {
		return (Integer) getSqlMapClientTemplate().queryForObject("eventMgnt.getEventPartCnt", vo);
	}

	public void addEventPart(EventMgntVO vo) {
		getSqlMapClientTemplate().insert("eventMgnt.addEventPart", vo);
	}

	public void addEventPartRslt(EventMgntVO vo) throws Exception {
		getSqlMapClientTemplate().insert("eventMgnt.addEventPartRslt", vo);
	}

	public List getWinningList(EventMgntVO vo) {
		return getSqlMapClientTemplate().queryForList("eventMgnt.getWinningList", vo);
	}

	public int getWinningListTotal(EventMgntVO vo) {
		return (Integer) getSqlMapClientTemplate().queryForObject("eventMgnt.getWinningListTotal",vo);
	}

	public String getTopMenu(int i) {
		return (String) getSqlMapClientTemplate().queryForObject("eventMgnt.getTopMenu", i);
	}

	public List getQnaList(QnABoardVO vo) {
		return getSqlMapClientTemplate().queryForList("eventMgnt.getQnaList", vo);
	}

	public int getQnaListCnt(QnABoardVO vo) {
		return (Integer) getSqlMapClientTemplate().queryForObject("eventMgnt.getQnaListCnt",vo);
	}

	public void addQnaRegi(QnABoardVO vo) {
		getSqlMapClientTemplate().insert("eventMgnt.addQnaRegi", vo);
	}

	public QnABoardVO getQnaDetail(QnABoardVO vo) {
		return (QnABoardVO) getSqlMapClientTemplate().queryForObject("eventMgnt.getQnaDetail", vo);
	}

	public void delQnaDelete(QnABoardVO vo) {
		getSqlMapClientTemplate().update("eventMgnt.delQnaDelete", vo);
	}

	public void uptQnaModi(QnABoardVO vo) {
		getSqlMapClientTemplate().update("eventMgnt.uptQnaModi", vo);
	}
	
}
