package kr.or.copyright.mls.statBord.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.statBord.model.AnucBord;
import kr.or.copyright.mls.statBord.model.AnucBordFile;
import kr.or.copyright.mls.statBord.model.AnucBordObjc;
import kr.or.copyright.mls.statBord.model.AnucBordSupl;
import kr.or.copyright.mls.statBord.model.Code;


public interface AnucBordService {
	public List<AnucBord> SelTest();
	//-----------------공고게시판----------------
	public int insertAnuc(AnucBord vo);
	public int getBordSeqn();
	public void deleteAnucBord(int bordSeqn);
	public List<AnucBord> selectAnuc(AnucBord anucBord);
	public List<AnucBordFile> fileSelectAnuc(int bordSeqn);
	public List<AnucBordFile> fileSelectObjc(int statObjcId);
	public List<AnucBord> detailAnuc(int bordSeqn);
	public List<AnucBord> findAnuc(AnucBord anucBord); 
	public int countAnuc(AnucBord anucBord);
	public int findCount(AnucBord anucBord);
	public int insertAnucObjc(AnucBordObjc anucBordObjc);
	public List<AnucBordObjc> selectAnucObjc(int bordSeqn,int bordCd);
	public List<AnucBordObjc> selectAnucObjcShis(int statObjcId);
	public int countAnucObjc(int bordSeqn);
	public int deleteAnucBordObjc(int statObjcId);			
	public List getObjcFileAttc(int statObjcId);
	public int updateAnucObjc(AnucBordObjc anucBordObjc);
	public void updateAnucBord(AnucBord anucBord);
	public void deleteAttcFile(int attcSeqn);
	public int deleteObjcFile(int attcSeqn);
	//-----------------거소불명-------------------
	public int getWorksId();
	public List<AnucBord> selectNonAnuc(AnucBord anucBord);
	public int insertNonAnuc(AnucBord anucBord);
	public int insertCopthodr(AnucBord anucBord);
	public List<AnucBord> detailGetMaker(int worksId);
	public List<AnucBord> detailNonAnuc(int worksId);
	public List<AnucBord> detailNonCopthodr(AnucBord anucBord);
	public List<AnucBordFile> fileSelectNonAnuc(int worksId);
	public void updateNonAnuc(AnucBord anucBord);
	public void updateNonCoptHodr(AnucBord anucBord);
	public void deleteNonAnuc(int worksId);
	public List<AnucBord> NonResult(int worksId);
	public int countNonAnuc(String rgstIdnt);
	public int deleteNonFile(int attcSeqn);
	public List<Code> getCodeList(Map params);
	public int deletecoptHodr(int worksId);
	public List<AnucBord> findNonAnuc(Map param);
	public int countFindNonAnuc(Map param);
	//-------------------법정허락 이용승인신청-----------------------
	public void insertStatObjc(AnucBordObjc anucBordObjc);
	public int countStatObjc(int worksId);
	public List<AnucBordObjc> selectStatObjc(int worksId);
	public int deleteStatObjc(int statObjcId);
	public void updateAnucBordObjcYnDelete(int bordSeqn);
	public void updateStatWorksObjcYnDelete(int worksId);
	
	public List StatObjcPop(AnucBord anucBord);
	
	//-------------------------보완------------------------------------
	public List<AnucBordSupl> selectBordSuplItemList(int bordSeqn, int bordCd);// 저작권자 조회공고, 보상금 공탁공고 보완내역 불러오기	
	public List<AnucBordSupl> selectWorksSuplItemList(int worksId);// 상당한노력신청 - 저작권자 조회공고 보완목록 불러오기
	
	//==========================기승인 법정허락 저작물=========================================
	public int statBo07TotalCount(Map dataMap);
	public List statBo07List(Map dataMap);
}	

