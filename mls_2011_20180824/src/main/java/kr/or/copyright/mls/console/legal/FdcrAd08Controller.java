package kr.or.copyright.mls.console.legal;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd08Service;
import kr.or.copyright.mls.console.ntcn.inter.FdcrAd75Service;
import kr.or.copyright.mls.myStat.service.MyStatService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 법정허락 > 이용승인신청 공고
 * 
 * @author ljh
 */
@Controller
public class FdcrAd08Controller extends DefaultController{

	@Resource( name = "fdcrAd08Service" )
	private FdcrAd08Service fdcrAd08Service;

	@Resource( name = "fdcrAd75Service" )
	private FdcrAd75Service fdcrAd75Service;
	
	@Resource( name = "myStatService" )
	private MyStatService myStatService;

	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;
	
	// AdminStatBoardSqlMapDao
	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	// AdminCommonSqlMapDao
	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd08Controller.class );

	/**
	 * 이용승인신청 공고 목록|이의제기 목록(구분자:OBJC_YN:Y->이의제기)
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08List1.page" )
	public String fdcrAd08List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		
		String SCH_WORKS_DIVS_CD = EgovWebUtil.getString( commandMap, "SCH_WORKS_DIVS_CD" );
		String SCH_STAT_OBJC_CD = EgovWebUtil.getString( commandMap, "SCH_STAT_OBJC_CD" );
		String SCH_RECEIPT_NO = EgovWebUtil.getString( commandMap, "SCH_RECEIPT_NO" );
		String SCH_TITL = EgovWebUtil.getString( commandMap, "SCH_TITL" );
		String OBJC_YN = EgovWebUtil.getString( commandMap, "OBJC_YN" );
		
		if( !"".equals( SCH_RECEIPT_NO ) ){
			SCH_RECEIPT_NO = URLDecoder.decode( SCH_RECEIPT_NO, "UTF-8" );
			commandMap.put( "SCH_RECEIPT_NO", SCH_RECEIPT_NO );
		}
		if( !"".equals( SCH_TITL ) ){
			SCH_TITL = URLDecoder.decode( SCH_TITL, "UTF-8" );
			commandMap.put( "SCH_TITL", SCH_TITL );
		}
		
		commandMap.put( "BORD_CD", "3" );

		ArrayList<Map<String, Object>> list = fdcrAd08Service.fdcrAd08List1( commandMap );

		model.addAttribute( "list", list );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );

		System.out.println( "이용승인 신청 공고 목록" );
		return "legal/fdcrAd08List1.tiles";
	}
	
	/**
	 * 이의제기 목록(구분자:OBJC_YN:Y->이의제기)
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08List2.page" )
	public String fdcrAd08List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		
		String SCH_WORKS_DIVS_CD = EgovWebUtil.getString( commandMap, "SCH_WORKS_DIVS_CD" );
		String SCH_STAT_OBJC_CD = EgovWebUtil.getString( commandMap, "SCH_STAT_OBJC_CD" );
		String SCH_RECEIPT_NO = EgovWebUtil.getString( commandMap, "SCH_RECEIPT_NO" );
		String SCH_TITL = EgovWebUtil.getString( commandMap, "SCH_TITL" );
		String OBJC_YN = EgovWebUtil.getString( commandMap, "OBJC_YN" );
		
		if( !"".equals( SCH_WORKS_DIVS_CD ) ){
			SCH_WORKS_DIVS_CD = URLDecoder.decode( SCH_WORKS_DIVS_CD, "UTF-8" );
			commandMap.put( "SCH_WORKS_DIVS_CD", SCH_WORKS_DIVS_CD );
		}
		if( !"".equals( SCH_STAT_OBJC_CD ) ){
			SCH_STAT_OBJC_CD = URLDecoder.decode( SCH_STAT_OBJC_CD, "UTF-8" );
			commandMap.put( "SCH_STAT_OBJC_CD", SCH_STAT_OBJC_CD );
		}
		if( !"".equals( SCH_RECEIPT_NO ) ){
			SCH_RECEIPT_NO = URLDecoder.decode( SCH_RECEIPT_NO, "UTF-8" );
			commandMap.put( "SCH_RECEIPT_NO", SCH_RECEIPT_NO );
		}
		if( !"".equals( SCH_TITL ) ){
			SCH_TITL = URLDecoder.decode( SCH_TITL, "UTF-8" );
			commandMap.put( "SCH_TITL", SCH_TITL );
		}
		
		commandMap.put( "OBJC_YN", "Y" );
		/* BORD_CD:3 이용승인신청 공고 -> 데이터가 없어서 임시 4번으로 테스트*/
		commandMap.put( "BORD_CD", "4" );

		ArrayList<Map<String, Object>> list = fdcrAd08Service.fdcrAd08List1( commandMap );

		model.addAttribute( "list", list );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );

		System.out.println( "이용승인 신청 공고 목록" );
		return "legal/fdcrAd08List2.tiles";
	}

	/**
	 * 법정허락 이용승인신청 상세
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08View1.page" )
	public String fdcrAd08View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAd08Service.fdcrAd08View1( commandMap );
		model.addAttribute( "detailList", commandMap.get( "detailList" ) );
		model.addAttribute( "fileList", commandMap.get( "fileList" ) );
		model.addAttribute( "objectList", commandMap.get( "objectList" ) );
		model.addAttribute( "objectFileList", commandMap.get( "objectFileList" ) );
		model.addAttribute( "suplList", commandMap.get( "suplList" ) );
		System.out.println( "이용승인 신청 공고 상세" );
		return "legal/fdcrAd08View1.tiles";
	}

	/**
	 * 이용승인신청 공고 게시판 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08Download1.page" )
	public void fdcrAd08Download1( ModelMap model,		Map<String, Object> commandMap,		HttpServletRequest request,		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		fdcrAd08Service.fdcrAd08View1( commandMap );
		List list = (List) commandMap.get( "fileList" );
		for( int i = 0; i < list.size(); i++ ){
			Map map = (Map) list.get( i );
			String bordCd =  String.valueOf( map.get( "BORD_CD" ));
			String bordSeqn =  String.valueOf(map.get( "BORD_SEQN" ));
			
			
			if( bordCd.equals( commandMap.get( "BORD_CD" ) ) && bordSeqn.equals( commandMap.get( "BORD_SEQN" ) ) ){
				String filePath = (String) map.get( "FILE_PATH" );
				String fileName = (String) map.get( "FILE_NAME" );
				String realFileName = (String) map.get( "REAL_FILE_NAME" );
				System.out.println( filePath );
				System.out.println( fileName );
				System.out.println( realFileName );
				if(bordCd.equals( "5" )) {
				download( request, response, filePath + realFileName, fileName );
				}else {
				download( request, response, filePath + fileName, realFileName );
				}
			}
		}
	}

	/**
	 * 이용승인신청 공고 수정 폼
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08UpdateForm1.page" )
	public String fdcrAd08UpdateForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAd08Service.fdcrAd08UpdateForm1( commandMap );
		model.addAttribute( "detailList", commandMap.get( "detailList" ) );
		model.addAttribute( "fileList", commandMap.get( "fileList" ) );
		model.addAttribute( "objectList", commandMap.get( "objectList" ) );
		model.addAttribute( "objectFileList", commandMap.get( "objectFileList" ) );
		model.addAttribute( "suplList", commandMap.get( "suplList" ) );

		System.out.println( "이용승인 신청 공고 수정화면" );
		return "legal/fdcrAd08UpdateForm1.tiles";
	}

	/**
	 * 이용승인신청 공고 수정
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08Update1.page" )
	public String fdcrAd08Update1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response,@RequestParam(value="orgFileDel") List<String> values ) throws Exception{
	     String[] orgFileDel = request.getParameterValues("orgFileDel");

	     System.out.println(values);
	     System.out.println(values.toString());
		String BORD_CD = EgovWebUtil.getString( commandMap, "BORD_CD" );
		String BORD_SEQN = EgovWebUtil.getString( commandMap, "BORD_SEQN" );
		String WORKS_ID = EgovWebUtil.getString( commandMap, "WORKS_ID" );
		String TITE = EgovWebUtil.getString( commandMap, "TITE" );
		String DIVS_CD = EgovWebUtil.getString( commandMap, "DIVS_CD" );
		String RECEIPT_NO = EgovWebUtil.getString( commandMap, "RECEIPT_NO" );
		String ANUC_ITEM_1 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_1" );
		String ANUC_ITEM_2 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_2" );
		String ANUC_ITEM_3 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_3" );
		String ANUC_ITEM_4 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_4" );
		String ANUC_ITEM_5 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_5" );
		String ANUC_ITEM_6 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_6" );
		String ANUC_ITEM_7 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_7" );
		String ANUC_ITEM_8 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_8" );
		String ANUC_ITEM_9 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_9" );
		String ANUC_ITEM_10 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_10" );
		String ANUC_ITEM_11 = EgovWebUtil.getString( commandMap, "ANUC_ITEM_11" );
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		String ATTC_SEQN = EgovWebUtil.getString( commandMap, "file1" );
		
		// 파일 업로드
		String uploadPath =	new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAd08Service.fdcrAd08Update1( commandMap, fileinfo );
		returnAjaxString( response, isSuccess );
		System.out.println( "법정허락 이용승인 신청 공고 수정" );
		if( isSuccess ){
			return returnUrl( model, "수정했습니다.", "/console/legal/fdcrAd08List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legal/fdcrAd08List1.page" );
		}
	}

	/**
	 * 이용승인신청 공고 삭제
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08Delete1.page" )
	public String fdcrAd08Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "MODI_IDNT", ConsoleLoginUser.getUserId() );
		boolean isSuccess = fdcrAd08Service.fdcrAd08Delete1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "법정허락 이용승인 신청 공고 삭제" );
		if( isSuccess ){
			return returnUrl( model, "삭제했습니다.", "/console/legal/fdcrAd08List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legal/fdcrAd08List1.page" );
		}
	}

	/**
	 * 이용승인신청 공고 게시판 등록 폼
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08WriteForm1.page" )
	public String fdcrAd08WriteForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "BORD_CD", "" );
		commandMap.put( "BORD_SEQN", "" );

		fdcrAd08Service.fdcrAd08WriteForm1( commandMap );

		List detailList = (List) commandMap.get( "detailList" );
		List fileList = (List) commandMap.get( "fileList" );
		List objectList = (List) commandMap.get( "objectList" );
		List objectFileList = (List) commandMap.get( "objectFileList" );
		List suplList = (List) commandMap.get( "suplList" );
		model.addAttribute( "detailList", detailList );
		model.addAttribute( "fileList", fileList );
		model.addAttribute( "objectList", objectList );
		model.addAttribute( "objectFileList", objectFileList );
		model.addAttribute( "suplList", suplList );

		System.out.println( "이용승인신청 공고 게시판 등록 폼" );
		return "legal/fdcrAd08WriteForm1.tiles";
	}

	/**
	 * 이용승인신청공고 게시판 등록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08Regi1.page" )
	public String fdcrAd08Regi1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response,
		MultipartHttpServletRequest mreq ) throws Exception{

		commandMap.put( "BORD_CD", "3" );
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		
		String[] TITES = request.getParameterValues( "TITE" );
		String[] RECEIPT_NOS = request.getParameterValues( "RECEIPT_NO" );
		String[] DIVS_CDS = request.getParameterValues( "DIVS_CD" );
		String[] WORKS_IDS = request.getParameterValues( "WORKS_ID" );
		String[] ANUC_ITEM_1S = request.getParameterValues( "ANUC_ITEM_1" );
		String[] ANUC_ITEM_2S = request.getParameterValues( "ANUC_ITEM_2" );
		String[] ANUC_ITEM_3S = request.getParameterValues( "ANUC_ITEM_3" );
		String[] ANUC_ITEM_4S = request.getParameterValues( "ANUC_ITEM_4" );
		String[] ANUC_ITEM_5S = request.getParameterValues( "ANUC_ITEM_5" );
		String[] ANUC_ITEM_6S = request.getParameterValues( "ANUC_ITEM_6" );
		String[] ANUC_ITEM_7S = request.getParameterValues( "ANUC_ITEM_7" );
		String[] ANUC_ITEM_8S = request.getParameterValues( "ANUC_ITEM_8" );
		String[] ANUC_ITEM_9S = request.getParameterValues( "ANUC_ITEM_9" );
		String[] ANUC_ITEM_10S = request.getParameterValues( "ANUC_ITEM_10" );
		String[] ANUC_ITEM_11S = request.getParameterValues( "ANUC_ITEM_11" );
		String[] ATTC_SEQNS = request.getParameterValues( "file1" );
		commandMap.put( "TITE", TITES );
		commandMap.put( "RECEIPT_NO", RECEIPT_NOS );
		commandMap.put( "DIVS_CD", DIVS_CDS );
		commandMap.put( "WORKS_ID", WORKS_IDS );
		commandMap.put( "ANUC_ITEM_1", ANUC_ITEM_1S );
		commandMap.put( "ANUC_ITEM_2", ANUC_ITEM_2S );
		commandMap.put( "ANUC_ITEM_3", ANUC_ITEM_3S );
		commandMap.put( "ANUC_ITEM_4", ANUC_ITEM_4S );
		commandMap.put( "ANUC_ITEM_5", ANUC_ITEM_5S );
		commandMap.put( "ANUC_ITEM_6", ANUC_ITEM_6S );
		commandMap.put( "ANUC_ITEM_7", ANUC_ITEM_7S );
		commandMap.put( "ANUC_ITEM_8", ANUC_ITEM_8S );
		commandMap.put( "ANUC_ITEM_9", ANUC_ITEM_9S );
		commandMap.put( "ANUC_ITEM_10", ANUC_ITEM_10S );
		commandMap.put( "ANUC_ITEM_11", ANUC_ITEM_11S );
		commandMap.put( "ATTC_SEQN", ATTC_SEQNS );

		// 파일 업로드
		
		String uploadPath =	new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );
		

		boolean isSuccess = fdcrAd08Service.fdcrAd08Regi1( commandMap );
		returnAjaxString( response, isSuccess );
		if( isSuccess ){
			fileInsert( mreq, commandMap, null );
			return returnUrl( model, "저장했습니다.", "/console/legal/fdcrAd08List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legal/fdcrAd08List1.page" );
		}
	}
	
	public void fileInsert( MultipartHttpServletRequest mreq,
		Map<String, Object> commandMap,
		String[] extNames ) throws Exception{
		String savePath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> files = fileUpload( mreq,savePath,null,true);
		for( int i = 0; i < files.size(); i++ ){
			Map<String, Object> file = files.get( i );
			Map<String, Object> param = new HashMap<String, Object>();
			int attachSeqn = consoleCommonDao.getNewAttcSeqn();
			int bordSeqn = fdcrAd01Dao.getMaxBordSeqn();
			//param.put( "MENU_SEQN", EgovWebUtil.getString( commandMap, "MENU_SEQN" ) );
			param.put( "BORD_CD", "3" );
			param.put( "BORD_SEQN", bordSeqn );
			param.put( "ATTC_SEQN", attachSeqn );
			param.put( "FILE_NAME", EgovWebUtil.getString( file, "F_fileName" ) );
			param.put( "FILE_PATH", EgovWebUtil.getString( file, "F_saveFilePath" ) );
			param.put( "FILE_SIZE", file.get( "F_filesize" ) );
			param.put( "REAL_FILE_NAME", EgovWebUtil.getString( file, "F_orgFileName" ) );
			param.put( "RGST_IDNT", ConsoleLoginUser.getUserId());
			fdcrAd01Dao.mlBord02FileInsert( param ); // 첨부파일 등록
			fdcrAd01Dao.statBord02FileInsert( param ); // 공고게시판 첨부파일 등록
			
			//fdcrAd75Service.insertBoardFile( param );
		}
	}

	/**
	 * 이용승인신청 일괄등록 엑셀양식 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08Download2.page" )
	public void fdcrAd08Download2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "FILE_URL", "" );
		String filePath = "";
		String fileName = "";

		fileName = "03.승인공고 입력양식.xlsx";
		filePath = "/home/right4me/web/upload/form/03.승인공고 입력양식.xlsx";

		if( null != fileName && !"".equals( fileName ) ){
			download( request, response, filePath, fileName );
		}
	}

	/**
	 * 이용승인신청 일괄등록 엑셀양식 업로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08Upload1.page" )
	public void fdcrAd08Upload1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );
		// TODO 엑셀 조회

		// boolean isSuccess = fdcrAd08Service.fdcrAd08Upload1( fileinfo );
		// returnAjaxString( response, isSuccess );
	}

	/**
	 * 이용승인신청 공고 일괄등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08Regi2.page" )
	public void fdcrAd08Regi2( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "RGST_IDNT", "" );
		commandMap.put( "OPEN_IDNT", "" );
		String[] TITES = request.getParameterValues( "TITE" );
		String[] RECEIPT_NOS = request.getParameterValues( "RECEIPT_NO" );
		String[] DIVS_CDS = request.getParameterValues( "DIVS_CD" );
		String[] WORKS_IDS = request.getParameterValues( "WORKS_ID" );
		String[] ANUC_ITEM_1S = request.getParameterValues( "ANUC_ITEM_1" );
		String[] ANUC_ITEM_2S = request.getParameterValues( "ANUC_ITEM_2" );
		String[] ANUC_ITEM_3S = request.getParameterValues( "ANUC_ITEM_3" );
		String[] ANUC_ITEM_4S = request.getParameterValues( "ANUC_ITEM_4" );
		String[] ANUC_ITEM_5S = request.getParameterValues( "ANUC_ITEM_5" );
		String[] ANUC_ITEM_6S = request.getParameterValues( "ANUC_ITEM_6" );
		String[] ANUC_ITEM_7S = request.getParameterValues( "ANUC_ITEM_7" );
		String[] ANUC_ITEM_8S = request.getParameterValues( "ANUC_ITEM_8" );
		String[] ANUC_ITEM_9S = request.getParameterValues( "ANUC_ITEM_9" );
		String[] ANUC_ITEM_10S = request.getParameterValues( "ANUC_ITEM_10" );
		String[] ANUC_ITEM_11S = request.getParameterValues( "ANUC_ITEM_11" );
		String[] OPEN_DTTMS = request.getParameterValues( "OPEN_DTTM" );

		commandMap.put( "TITE", TITES );
		commandMap.put( "RECEIPT_NO", RECEIPT_NOS );
		commandMap.put( "DIVS_CD", DIVS_CDS );
		commandMap.put( "WORKS_ID", WORKS_IDS );
		commandMap.put( "ANUC_ITEM_1", ANUC_ITEM_1S );
		commandMap.put( "ANUC_ITEM_2", ANUC_ITEM_2S );
		commandMap.put( "ANUC_ITEM_3", ANUC_ITEM_3S );
		commandMap.put( "ANUC_ITEM_4", ANUC_ITEM_4S );
		commandMap.put( "ANUC_ITEM_5", ANUC_ITEM_5S );
		commandMap.put( "ANUC_ITEM_6", ANUC_ITEM_6S );
		commandMap.put( "ANUC_ITEM_7", ANUC_ITEM_7S );
		commandMap.put( "ANUC_ITEM_8", ANUC_ITEM_8S );
		commandMap.put( "ANUC_ITEM_9", ANUC_ITEM_9S );
		commandMap.put( "ANUC_ITEM_10", ANUC_ITEM_10S );
		commandMap.put( "ANUC_ITEM_11", ANUC_ITEM_11S );
		commandMap.put( "OPEN_DTTM", OPEN_DTTMS );

		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAd08Service.fdcrAd08Regi2( commandMap, fileinfo );
		returnAjaxString( response, isSuccess );
	}

	/**
	 * 이용승인신청 공고 목록 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08ExcelDown1.page" )
	public String fdcrAd08ExcelDown1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "FROM_NO", "" );
		commandMap.put( "TO_NO", "" );
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "OBJC_YN", "" );
		commandMap.put( "SCH_WORKS_DIVS_CD", "" );
		commandMap.put( "SCH_STAT_OBJC_CD", "" );
		commandMap.put( "SCH_RECEIPT_NO", "" );
		commandMap.put( "SCH_TITL", "" );

		fdcrAd08Service.fdcrAd08ExcelDown1( commandMap );
		model.addAttribute( "ds_exceldown", commandMap.get( "ds_exceldown" ) );

		System.out.println( "이용승인 신청 공고 목록 엑셀다운로드" );
		return "test";
	}
	
	/**
	 * 게시판 첨부 파일 다운로드
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd08Down1.page" )
	public void fdcrAd08Down1( HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{

		String savePath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		Map<String, Object> fileInfo = fdcrAd75Service.selectAttachFileInfo( commandMap );

		String fileRealNm = EgovWebUtil.getString( fileInfo, "REAL_FILE_NAME" );
		String fileNm = EgovWebUtil.getString( fileInfo, "FILE_NAME" );
		String downPath = savePath + fileNm;
		if( !"".equals( fileRealNm ) ){
			download( request, response, downPath, fileRealNm );
		}
	}

}
