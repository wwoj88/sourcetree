package kr.or.copyright.mls.console.rcept.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tobesoft.platform.data.Dataset;

/**
 * Class 내용 기술
 * 
 * @author : 정병호
 * @since : 20120910
 * @version : 1.0
 * @see: 위탁관리 저작물 보고
 */
public interface FdcrAd15Dao{

	// 위탁관리 음악저작물정보 일괄등록1
	public List adminCommMusicInsert( Dataset ds_condition,
		Dataset ds_report,
		Dataset ds_excel ) throws Exception;

	// 위탁관리 음악저작물정보 일괄수정1
	public List adminCommMusicUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	// 위탁관리 어문저작물정보 일괄등록2
	public List adminCommBookInsert( Dataset ds_condition,
		Dataset ds_report,
		Dataset ds_excel ) throws Exception;

	// 위탁관리 어문저작물정보 일괄수정2
	public List adminCommBookUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	// 위탁관리 방송대본저작물정보 일괄등록3
	public List adminCommScriptInsert( Dataset ds_condition,
		Dataset ds_report,
		Dataset ds_excel ) throws Exception;

	// 위탁관리 방송대본저작물정보 일괄수정3
	public List adminCommScriptUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	// 위탁관리 영화저작물정보 일괄등록4
	public List adminCommMovieInsert( Dataset ds_condition,
		Dataset ds_report,
		Dataset ds_excel ) throws Exception;

	// 위탁관리 영화저작물정보 일괄수정4
	public List adminCommMovieUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	// 위탁관리 방송저작물정보 일괄등록5
	public List adminCommBroadcastInsert( Dataset ds_condition,
		Dataset ds_report,
		Dataset ds_excel ) throws Exception;

	// 위탁관리 방송저작물정보 일괄수정5
	public List adminCommBroadcastUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	// 위탁관리 뉴스저작물정보 일괄등록6
	public List adminCommNewsInsert( Dataset ds_condition,
		Dataset ds_report,
		Dataset ds_excel ) throws Exception;

	// 위탁관리 뉴스저작물정보 일괄수정6
	public List adminCommNewsUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	// 위탁관리 미술 저작물정보 일괄등록7
	public List adminCommArtInsert( Dataset ds_condition,
		Dataset ds_report,
		Dataset ds_excel ) throws Exception;

	// 위탁관리 미술저작물정보 일괄수정7
	public List adminCommArtUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	// 위탁관리 이미지 저작물정보 일괄등록8
	public List adminCommImageInsert( Dataset ds_condition,
		Dataset ds_report,
		Dataset ds_excel ) throws Exception;

	// 위탁관리 이미지저작물정보 일괄수정8
	public List adminCommImageUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	// 위탁관리 기타저작물정보 일괄등록9
	public List adminCommSideInsert( Dataset ds_condition,
		Dataset ds_report,
		Dataset ds_excel ) throws Exception;

	// 위탁관리 기타저작물정보 일괄수정9
	public List adminCommSideUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	// 위탁관리 저작물 보고정보 등록(공통)
	public int adminCommWorksReportInsert( Map map ) throws SQLException;

	// 위탁관리 저작물 보고정보(등록건수등록) 수정(공통)
	public void adminCommWorksReportUpdate( Map map );

	// 위탁관리 저작물 등록(공통)
	public boolean adminCommWorksInsert( Map map );

	// 위탁관리 저작물 등록(공통)
	public boolean adminCommWorksUpdate( Map map );

	// 위탁관리 저작물 음악 등록1
	public boolean adminCommMusicInsert( Map map );

	// 위탁관리 저작물 어문 등록2
	public boolean adminCommBookInsert( Map map );

	// 위탁관리 저작물 방송대본 등록3
	public boolean adminCommScriptInsert( Map map );

	// 위탁관리 저작물 영화 등록4
	public boolean adminCommMovieInsert( Map map );

	// 위탁관리 저작물 방송 등록5
	public boolean adminCommBroadcastInsert( Map map );

	// 위탁관리 저작물 뉴스 등록6
	public boolean adminCommNewsInsert( Map map );

	// 위탁관리 저작물 미술 등록7
	public boolean adminCommArtInsert( Map map );

	// 위탁관리 저작물 이미지 등록8
	public boolean adminCommImageInsert( Map map );

	// 위탁관리 저작물 기타 등록9
	public boolean adminCommSideInsert( Map map );

	// 위탁관리 저작물 음악 수정
	public boolean adminCommMusicUpdate( Map map );

	// 위탁관리 저작물 어문 수정2
	public boolean adminCommBookUpdate( Map map );

	// 위탁관리 저작물 방송대본 수정3
	public boolean adminCommScriptUpdate( Map map );

	// 위탁관리 저작물 영화 수정4
	public boolean adminCommMovieUpdate( Map map );

	// 위탁관리 저작물 방송 수정5
	public boolean adminCommBroadcastUpdate( Map map );

	// 위탁관리 저작물 뉴스 수정6
	public boolean adminCommNewsUpdate( Map map );

	// 위탁관리 저작물 미술 수정7
	public boolean adminCommArtUpdate( Map map );

	// 위탁관리 저작물 기타 수정8
	public boolean adminCommSideUpdate( Map map );

	// 위탁관리 저작물 works_id 생성(공통)
	public int getCommWorksId();

	// 위탁관리 저작물 삭제(공통)
	public void adminCommWorksDelete( Map map );

	public HashMap getMap( Dataset ds,
		int rnum ) throws Exception;

	// 위탁관리 저작물 목록 조회
	public List worksMgntList( Map map );
	
	// 위탁관리 저작물 목록 월별 조회
	public List worksMgntListMonth( Map map );

	// 위탁관리 저작물 목록조회 20141126 이병원
	public List worksMgntList_01( Map map );

	// 위탁관리 저작물 음악 조회
	public List worksMgntList_01_music( Map map );

	// 위탁관리 저작물 어문 조회
	public List worksMgntList_01_book( Map map );

	// 위탁관리 저작물 방송대본 조회
	public List worksMgntList_01_script( Map map );

	// 위탁관리 저작물 영화 조회
	public List worksMgntList_01_movie( Map map );

	// 위탁관리 저작물 방송 조회
	public List worksMgntList_01_broadcast( Map map );

	// 위탁관리 저작물 뉴스 조회
	public List worksMgntList_01_news( Map map );

	// 위탁관리 저작물 미술 조회
	public List worksMgntList_01_art( Map map );

	// 위탁관리 저작물 이미지 조회
	public List worksMgntList_01_image( Map map );

	// 위탁관리 저작물 기타 조회
	public List worksMgntList_01_side( Map map );

	// 위탁관리 저작물 작성기관명 조회
	public List searchOrgnName( Map map );

	// 위탁관리 저작물 목록 페이징 정보
	public List totalRowWorksMgntList( Map map );

	// 위탁관리 저작물 목록 페이징 20141126 이병원
	public List totalRowWorksMgntList_01( Map map );

	// 위탁관리 월별 보고현황 조회
	public List worksMgntReptView( Map map );

	// 위탁관리 월별 정보
	public List worksMgntReptInfo( Map map );

	// 위탁관리 최신 기관 정보(기관코드, ID)
	public List worksMgntReptInfo2( Map map );

	// 등록부연계 월별 등록현황 조회
	public List crosWorksMgntReptView( Map map );

	public void adminWorksMgntNoDataInsert( Map<String, Object> commandMap ) throws Exception;

	// 위탁관리 저작물 기존 등록여부 조회(기관코드,내부관리ID,기관명)
	public List worksMgntChkCnt( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

	public List worksMgntReptMonthView( Map map );

	public List worksMgntReptMonthCoCnt( Map map );

	public List worksMgntReptViewCoCnt( Map map );

	public List worksMgntAppDate( Map map );

	public List worksMgntReptYearView( Map map );

	public int getExistCommWorks( Map map ) throws Exception;

	public String commWorksInsertBacth( List list ) throws Exception;

	public String commMusicInsertBacth( List list ) throws Exception;

	public String commBookInsertBacth( List list ) throws Exception;

	public String commScriptInsertBacth( List list ) throws Exception;

	public String commMovieInsertBacth( List list ) throws Exception;

	public String commBroadcastInsertBacth( List list ) throws Exception;

	public String commNewsInsertBacth( List list ) throws Exception;

	public String commArtInsertBacth( List list ) throws Exception;

	public String commImageInsertBacth( List list ) throws Exception;

	public String commSideInsertBacth( List list ) throws Exception;

	// 위탁관리 저작물 목록 엑셀 다운로드
	public List worksMgntExcelDown( Map map );

	public List getYyyyMm( Map<String, Object> commandMap );

	public List trstOrgnCoNameList( Map<String, Object> commandMap );

	public List trstOrgnCoNameListCount( Map<String, Object> commandMap );

	public String getReptCnt( Map<String, Object> commandMap ) throws Exception;

	public void adminCommWorksCntUpdate( Map<String, Object> commandMap );

	public int getExistReptCnt( Map<String, Object> exMap ) throws Exception;

	public String getReptSeqn(Map<String, Object> commandMap) throws Exception;

	public void adminCommWorksReportInsertNew( Map<String, Object> commandMap );

	public int getReptWorksCont( Map<String, Object> exMap )throws Exception;

	public String getCommWorksIdForUpdate(Map<String, Object> map) throws Exception;

	public void commMusicUpdate( Map map ) throws Exception;

	public void commBookUpdate( Map map ) throws Exception;

	public void commScriptUpdate( Map map ) throws Exception;

	public void commMovieUpdate( Map map )throws Exception;

	public void commBroadcastUpdate( Map map ) throws Exception;

	public void commNewsUpdate( Map map ) throws Exception;

	public void commArtUpdate( Map map ) throws Exception;

	public void commImageUpdate( Map map ) throws Exception;

	public void commSideUpdate( Map map ) throws Exception;

	public void commWorksUpdate( Map map ) throws Exception;

	public int getModiWorksCont( Map<String, Object> exMap ) throws Exception;

	public List worksMgntNewExcel( Map map );

	public List worksMgntReptViewNewExcel( Map<String, Object> commandMap );

	public List worksMgntReptViewCoCntNewExcel( Map<String, Object> commandMap );

	public List worksMgntReptMonthViewNewExcel( Map<String, Object> commandMap );

	public List worksMgntReptMonthCoCntNewExcel( Map<String, Object> commandMap );
	
	public void fdcrAd16AddHistory( Map model ) throws Exception;
	
	public List fdcrAd16SelectWork( String string ) throws Exception;

	public void fdcrAd16Delete( Map model ) throws Exception;
	
	public void fdcrAd16UpdateReport(Map model) throws Exception;

	public void fdcrAd16AddDeleteUserIdHistory( Map model ) throws Exception;

	public List selectWorksReport( Map reportSelectMap ) throws Exception;

	public List fdcrAd16ImageList( Map<String, Object> commandMap ) throws Exception;

	public List fdcrAd16ArtList( Map<String, Object> commandMap ) throws Exception;

	public List totalRowArtCnt( Map<String, Object> commandMap );
	
	public List totalRowImageCnt( Map<String, Object> commandMap );

}
