package kr.or.copyright.mls.console.legal.inter;

import java.util.ArrayList;
import java.util.Map;

public interface FdcrAd07Service{

	/**
	 * 법정허락 신청등록(오프라인)등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd07Regi1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 법정허락 신청등록(오프라인)일괄등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd07Regi2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 신청등록(오프라인)파일 업로드
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd07Upload1( ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 법정허락 신청등록(오프라인)등록_신규
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd07Insert( Map<String, Object> commandMap ) throws Exception;
}
