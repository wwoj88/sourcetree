package kr.or.copyright.mls.common.dao;

import java.util.List;
import java.util.Map;
 
public interface CommonDao {

//	public int updateBoard(Board board);
	      
	// 내권리찾기 사용자 조회 
	public List getUserInfo(Map map);
	
	// 내권리찾기 보상회원 조회 
	public List getInmtUserInfo(Map map);

	// 첨부파일 Main(ML_FILE) 신규SEQ 조회
	public int getNewAttcSeqn();
	
	public List getMgntMail(Map map);

}
