package kr.or.copyright.mls.console.mber.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface FdcrAd84Service{

	/**
	 * 신탁단체 담당자목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd84List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 신탁단체 담당자 상세 조회, 내정보조회 상세
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd84UpdateForm1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Map<String, Object>> adminGroupMenuInfo( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 신탁단체 담당자 입력
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd84Insert1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 신탁단체 담당자 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd84Update1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 관리자 아이디 중복체크
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public int fdcrAd84Update2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 대표담당자 존재여부 체크
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public int fdcrAd84Update3( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 기관/단체 조회
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 * @throws Exception 
	 */
	public void trstOrgnCoNameList(Map<String, Object> commandMap) throws Exception;
}
