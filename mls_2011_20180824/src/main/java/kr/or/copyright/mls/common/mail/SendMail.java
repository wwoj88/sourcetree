package kr.or.copyright.mls.common.mail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.ajaxTest.dao.CodeListDAO;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;

public class SendMail {
    
    public static void send(Map userMap) throws Exception{
	
		String type = (String) userMap.get("MGNT_DIVS");
		
		String host = Constants.MAIL_SERVER_IP;	//smtp서버
		String from = Constants.SYSTEM_MAIL_ADDRESS;  //발신인 주소 - 시스템 대표 메일
		String fromName = Constants.SYSTEM_NAME;
		String to[] = (String[]) userMap.get("TO");
		String toName[] = (String[]) userMap.get("TO_NAME");
		System.out.println( "userMap : " + userMap );
		//관리자 정보를 map으로 담고 해당 정보로 셋팅을 해서 보낸다.
	    //String to     =  (String) userMap.get("MAIL");	// 수신인은 관리자 이므로 복수가 나올수 있음 map으로 담음
		//String toName =  (String) userMap.get("USER_NAME"); //수신인 이름 관리자 복수가 나올수 있음 map으로 담음
		
	    /*
    	to[0] = "bhjeong@yagins.com";
    	to[1] = "jbh1827@nate.com";
    	
    	toName[0] = "병호";
    	toName[1] = "정병호";
	     */
		
		
		String subject = "";	//메일 제목
		
		MailInfo info = new MailInfo();
		
	    info.setFrom(from);
	    info.setFromName(fromName);
	    info.setHost(host);
	    info.setArrEmail(to);
	    info.setArrEmailName(toName);
	    
		if(to.length > 0) {
		
			if(type.substring(0,2).equals("BO")){//저작권자 조회공고
			    	
				Calendar cal = Calendar.getInstance( );
			        
				//신청날짜
				String rgstDttm = String.format("%04d-%02d-%02d",			cal.get(Calendar.YEAR),     (cal.get(Calendar.MONTH) + 1),       cal.get(Calendar.DAY_OF_MONTH));
				 
				//신청제목
				String worksTitl = (String) userMap.get("WORKS_TITL");
				
				//사용자 정보 셋팅 
				String userIdnt = (String) userMap.get("USER_IDNT");
				String userName = (String) userMap.get("USER_NAME");
				String userEmail = (String) userMap.get("U_MAIL");
				String userSms = (String) userMap.get("MOBL_PHON");
				String mailTitlDivs = (String) userMap.get("MAIL_TITL_DIVS");
				String mailTitl = (String) userMap.get("MAIL_TITL");
				
				subject = "[저작권찾기] "+mailTitl+mailTitlDivs+"이 완료되었습니다.";
			    
			    StringBuffer sb = new StringBuffer();   
				sb.append("				<tr>");
				sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
				sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">"+mailTitlDivs+" 정보</p>");
				sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : "+worksTitl+"</li>");
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">"+mailTitlDivs+"날짜 : "+rgstDttm+"</li>");				
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">"+mailTitlDivs+"자 정보 : "+userName+" (email:"+ userEmail +", mobile: "+userSms+")</li>");				
				//sb.append("						<li style=\"background:url(/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">신청일자 : 0000.00.00</li>");
				sb.append("					</ul>");
				sb.append("					</td>");
				sb.append("				</tr>");
	
			    info.setMessage(sb.toString());
			    info.setSubject(subject);
				info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
			    
				    
			}else if(type.equals("ST")){//법정허락신청
		
				//신청제목
				String worksTitl = (String) userMap.get("WORKS_TITL");
				
				//사용자 정보 셋팅 
				String userIdnt = (String) userMap.get("USER_IDNT");
				String userName = (String) userMap.get("USER_NAME");
				String userEmail = (String) userMap.get("U_MAIL");
				String userSms = (String) userMap.get("MOBL_PHON");
				String mailTitlDivs = (String) userMap.get("MAIL_TITL_DIVS");
				String mailTitl = (String) userMap.get("MAIL_TITL");
				String applyNo = (String) userMap.get("APPLY_NO");
				
				subject = "[저작권찾기] "+mailTitl;
							
			    StringBuffer sb = new StringBuffer();   
				sb.append("				<tr>");
				sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
				sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">신청 정보</p>");
				sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청번호 : "+applyNo+"</li>");
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목): "+worksTitl+"</li>");				
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자자 정보 : "+userName+" (email:"+ userEmail +", mobile: "+userSms+")</li>");				
				sb.append("					</ul>");
				sb.append("					</td>");
				sb.append("				</tr>");
	
			    info.setMessage(sb.toString());
			    info.setSubject(subject);
				info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
			
			}
			else if(type.equals("STAT_PROC") || type.equals("STATWORK_PROC") || type.equals("STAT_PROC_EXCP")){//상당한노력 자동화 수행결과 안내관련
				
				String mailTitl = (String) userMap.get("MAIL_TITL");
				String mailMent = (String) userMap.get("MAIL_MENT");
				
				int ord[] = (int[]) userMap.get("ORD");
				String yyyymm[] = (String[]) userMap.get("YYYYMM");
				String works_divs_name[] = (String[]) userMap.get("WORKS_DIVS_NAME");
				int rslt_cont_ys[] = (int[]) userMap.get("RSLT_CONT_YS");
				int rslt_cont_no[] = (int[]) userMap.get("RSLT_CONT_NO");
				int targ_works_cont[] = (int[]) userMap.get("TARG_WORKS_CONT");
				int rslt_cont[] = (int[]) userMap.get("RSLT_CONT");
				
				subject = "[저작권찾기] "+mailTitl+"이 완료되었습니다.";
				
				StringBuffer subSb = new StringBuffer();
				
				if(type.equals("STAT_PROC") && yyyymm!=null){
					subSb.append("						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">    \n");
					subSb.append("							<thead>    \n");
					subSb.append("								<tr>    \n");
					subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"15%\">기준년월</th>    \n");
					subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"10%\">차수</th>    \n");	
					subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">대상저작물</th>    \n");
					subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">상당한노력 수행결과(매칭/미매칭)</th>    \n");
					subSb.append("								</tr>    \n");
					subSb.append("							</thead>    \n");
					subSb.append("							<tbody>    \n");
					
					for(int i=0;i<ord.length;i++){	
						subSb.append("							<tr>    \n");		
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+yyyymm[i]+"</td>    \n");
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+ord[i]+"</td>    \n");
						subSb.append("								<td style=\"padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+works_divs_name[i]+"</td>    \n");
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+rslt_cont_ys[i]+"/"+rslt_cont_no[i]+"</td>    \n");
						subSb.append("							</tr>    \n");
					}
					
					subSb.append("						</tbody>    \n");
					subSb.append("					</table>    \n");
				}else if(type.equals("STATWORK_PROC") && yyyymm!=null){
					subSb.append("						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">    \n");
					subSb.append("							<thead>    \n");
					subSb.append("								<tr>    \n");
					subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"15%\">기준년월</th>    \n");
					subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"10%\">차수</th>    \n");	
					subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">대상저작물</th>    \n");
					subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">전환저작물</th>    \n");
					subSb.append("								</tr>    \n");
					subSb.append("							</thead>    \n");
					subSb.append("							<tbody>    \n");
					
					for(int i=0;i<ord.length;i++){	
						subSb.append("							<tr>    \n");		
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+yyyymm[i]+"</td>    \n");
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+ord[i]+"</td>    \n");
						subSb.append("								<td style=\"padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+targ_works_cont[i]+"</td>    \n");
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+rslt_cont[i]+"</td>    \n");
						subSb.append("							</tr>    \n");
					}
					
					subSb.append("						</tbody>    \n");
					subSb.append("					</table>    \n");
				}
				
			    StringBuffer sb = new StringBuffer();   
			    
				sb.append("				<tr>    \n");
				sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">    \n");
				sb.append("						<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">"+mailTitl+" 정보</p>    \n");
				
				if(mailMent.length()>0) {
					sb.append("						<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">    \n");
					sb.append("							<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">"+mailMent+"</li>    \n");
					sb.append("						</ul>    \n");
				}
				
				sb.append( subSb.toString() );
				
				sb.append("					</td>    \n");
				sb.append("				</tr>    \n");
	
			    info.setMessage(sb.toString());
			    info.setSubject(subject);
				info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
			}
			else if(type.equals("REPORT_WORKS") || type.equals("REPORT_INMT")){//저작물보고 일별 보고결과 안내관련
						
				String commName[]=(String[])userMap.get("COMM_NAME");
				String rgstDttm[]=(String[])userMap.get("RGST_DTTM");;
				String genre[]=(String[])userMap.get("GENRE");
				String sumAllCount[]=(String[])userMap.get("SUM_ALL_COUNT");
				String inmt[]=(String[])userMap.get("INMT");
				String mailTitl=(String)userMap.get("MAIL_TITL");				
							
				StringBuffer subSb = new StringBuffer();			
			
				subSb.append("						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">    \n");
				subSb.append("							<thead>    \n");
				subSb.append("								<tr>    \n");
				subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">기관명</th>    \n");
				subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">보고일자</th>    \n");
				if(type.equals("REPORT_WORKS")){
					subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"20%\">저작물분류</th>    \n");
					
				}else if(type.equals("REPORT_INMT")){
					subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"20%\">보상금분류</th>    \n");
				}
				subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">건수</th>    \n");
				subSb.append("								</tr>    \n");
				subSb.append("							</thead>    \n");
				subSb.append("							<tbody>    \n");
				for(int i=0;i<commName.length;i++){				
					subSb.append("							<tr>    \n");		
					subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+commName[i]+"</td>    \n");
					subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+rgstDttm[i]+"</td>    \n");
					
					if(type.equals("REPORT_WORKS")){
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+genre[i]+"</td>    \n");
						
					}else if(type.equals("REPORT_INMT")){
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+inmt[i]+"</td>    \n");
					}
					
					subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+sumAllCount[i]+"</td>    \n");
					subSb.append("							</tr>    \n");			
										
				}			
				subSb.append("						</tbody>    \n");
				subSb.append("					</table>    \n");
				
				StringBuffer sb = new StringBuffer();
				sb.append("				<tr>    \n");
				sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">    \n");
				sb.append("						<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">"+mailTitl+" 정보</p>    \n");
				sb.append( subSb.toString() );			
				sb.append("					</td>    \n");
				sb.append("				</tr>    \n");
				
				subject="[저작권찾기] "+mailTitl+" 현황안내 ("+rgstDttm[0]+")";	
				
			   
	
			    info.setMessage(sb.toString());
			    info.setSubject(subject);
				info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
			}else if(type.equals( "DISCONNECTED" )) {
				String commName[]=(String[])userMap.get("COMM_NAME");
				String USER_IDNT = (String) userMap.get( "USER_IDNT" );
				String rgstDttm=(String)userMap.get("RGST_DTTM");;
				String genre[]=(String[])userMap.get("GENRE");
				String sumAllCount[]=(String[])userMap.get("SUM_ALL_COUNT");
				String inmt[]=(String[])userMap.get("INMT");
				String mailTitl=(String)userMap.get("MAIL_TITL");				
							
				StringBuffer subSb = new StringBuffer();			
			
				subSb.append("						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">    \n");
				subSb.append("							<thead>    \n");
				subSb.append("								<tr>    \n");
				subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">아이디</th>    \n");
				subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">보고일자</th>    \n");
				subSb.append("								</tr>    \n");
				subSb.append("							</thead>    \n");
				subSb.append("							<tbody>    \n");
				subSb.append("							<tr>    \n");		
				subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+USER_IDNT+"</td>    \n");
				subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+rgstDttm+"</td>    \n");
				subSb.append("							</tr>    \n");			
				subSb.append("						</tbody>    \n");
				subSb.append("					</table>    \n");
				
				StringBuffer sb = new StringBuffer();
				sb.append("				<tr>    \n");
				sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">    \n");
				sb.append("						<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">"+mailTitl+" 정보</p>    \n");
				sb.append( subSb.toString() );			
				sb.append("					</td>    \n");
				sb.append("				</tr>    \n");
				
				subject="[저작권찾기] "+mailTitl+" ("+rgstDttm+")";	
				
			   
	
			    info.setMessage(sb.toString());
			    info.setSubject(subject);
				info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
				//System.out.println( subSb );
			}
			
			MailManager manager = MailManager.getInstance();
			
		    info = manager.sendMailAll(info);
		    	
		    if (info.isSuccess()) {
			System.out.println(">>>>>>>>>>>> 1. message success :"+ info.getEmail());
		    } else {
			System.out.println(">>>>>>>>>>>> 1. message false   :"+ info.getEmail());
		    }
		}
    }
}
