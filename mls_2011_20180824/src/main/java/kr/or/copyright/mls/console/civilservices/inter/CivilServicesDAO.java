package kr.or.copyright.mls.console.civilservices.inter;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.civilservices.model.CivilServicesDTO;


public interface CivilServicesDAO{
	public void  insertCivilServices(Map<String, Object> commandMap);
	public String civilServicesFromCivilSeqn();
	public void insertCivilServicesFile(CivilServicesDTO civilServicesDTO);
	public List selectCivilServices(Map<String, Object> commandMap);
	public int selectCountCivilServices();
	public List selectCivilServicesByCivilSeqn( Map<String, Object> commandMap );
	public List selectCivilServicesFile(Map<String, Object> commandMap);
	public void updateCivilServiceState(Map<String, Object> commandMap);
	public List selectCivilServicesComment(Map<String, Object> commandMap);
	public List selectCivilServicesDownLoadFile(Map<String, Object> commandMap);
	public List selectCivilServicesExcelList(Map<String, Object> commandMap);
	public void updateCivilServicesComment(Map<String, Object> commandMap);
	public void deleteCivilServicesFile(String deleteFileNum);
	public void deleteCivilServicesByCivilSeqn(String deleteFileNum);
	public void deleteCivilServicesComment(String deleteFileNum);
	public void deleteCivilServicesState(String deleteCivilServiceNum);
	public List companySrch(Map<String, Object> companyNameList);
	public List personSrch(Map<String, Object> commandMap);
	public List selectCivilServicesPerson(Map<String, Object> returnList);
	public List selectCivilStats();
}
