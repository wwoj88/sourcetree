package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;


public interface FdcrAd90Service{
	/**
	 * 기간별 접속 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd90List1( Map<String, Object> commandMap ) throws Exception;
	
	// 기간별 접속 통계(월별)
	//connStatPeriMonthList
	public void fdcrAd90List2(Map<String,Object> commandMap) throws Exception;
}
