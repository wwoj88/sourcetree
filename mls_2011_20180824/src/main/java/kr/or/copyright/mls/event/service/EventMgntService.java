package kr.or.copyright.mls.event.service;

import java.util.List;

import kr.or.copyright.mls.event.model.EventMgntVO;
import kr.or.copyright.mls.event.model.QnABoardVO;

public interface EventMgntService {

	List getEventUserList(EventMgntVO vo);

	int getEventUserListTotal(EventMgntVO vo);

	EventMgntVO getEventUserDetail(EventMgntVO vo);

	List getEventView02List(EventMgntVO vo);

	int getEventView02Total(EventMgntVO vo);

	List getEventAgreeItem(EventMgntVO vo);

	void addEventView02(EventMgntVO vo) throws Exception;

	int getEventPartCnt(EventMgntVO vo);

	void addEventView01(EventMgntVO vo)  throws Exception;

	List getWinningList(EventMgntVO vo);

	int getWinningListTotal(EventMgntVO vo);

	String getTopMenu(int i);

	List getQnaList(QnABoardVO vo) throws Exception;

	int getQnaListCnt(QnABoardVO vo) throws Exception;

	void addQnaRegi(QnABoardVO vo) throws Exception;

	QnABoardVO getQnaDetail(QnABoardVO vo) throws Exception;

	void delQnaDelete(QnABoardVO vo);

	void uptQnaModi(QnABoardVO vo);

	
}
