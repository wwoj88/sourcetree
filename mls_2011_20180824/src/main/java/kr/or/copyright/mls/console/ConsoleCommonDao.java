package kr.or.copyright.mls.console;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.model.CodeList;

import org.springframework.stereotype.Repository;

@Repository( "consoleCommonDao" )
public class ConsoleCommonDao extends EgovComAbstractDAO{

	/**
	 * 회원정보 상세조회
	 * 
	 * @param map
	 * @return
	 */
	public List userDetlList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminUser.userDetlList", map );
	}

	/**
	 * 코드정보 상세조회
	 * 
	 * @param code
	 * @return
	 */
	public List commonCodeList( CodeList code ){
		return getSqlMapClientTemplate().queryForList( "CodeList.commonCodeList", code );
	}

	/**
	 * 이용승인신청 수정 - sms 회원정보 상세조회
	 * 
	 * @param code
	 * @return
	 */
	public List selectMemberInfo( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminCommon.selectMemberInfo", map );
	}

	/**
	 * 이메일 메세지함 SEQ 번호 max+1 조회
	 * 
	 * @return
	 */
	public int selectMaxMsgIdSeq(){
		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminEmailMgnt.selectMaxMsgIdSeq" );
	}

	/**
	 * 이메일 작성내용 저장
	 * 
	 * @param map
	 */
	public void insertEmailMsg( Map map ){
		getSqlMapClientTemplate().insert( "AdminEmailMgnt.insertEmailMsg", map );

	}

	/**
	 * @param map
	 */
	public void insertMailAttc( Map map ){
		getSqlMapClientTemplate().insert( "AdminEmailMgnt.insertMailAttc", map );

	}

	/**
	 * @param map
	 */
	public void updateEndDttmMsg( Map map ){
		getSqlMapClientTemplate().update( "AdminEmailMgnt.updateEndDttmMsg", map );
	}

	/**
	 * 이메일 수신자 정보 저장
	 * 
	 * @param map
	 */
	public void insertEmailRecvInfo( Map map ){
		getSqlMapClientTemplate().insert( "AdminEmailMgnt.insertEmailRecvInfo", map );
	}

	/**
	 * 이메일 전송목록 저장
	 * 
	 * @param map
	 */
	public void insertSendList( Map map ){
		getSqlMapClientTemplate().insert( "AdminEmailMgnt.insertSendList", map );

	}

	/**
	 * 신청정보보완내역 메일발신내역 저장
	 * 
	 * @param map
	 */
	public void insertSuplList( Map map ){

		getSqlMapClientTemplate().insert( "AdminEmailMgnt.insertSuplList", map );
	}

	/**
	 * @param map
	 */
	public void updateSendScMailng( Map map ){
		getSqlMapClientTemplate().update( "AdminEmailMgnt.updateSendScMailng", map );

	}

	/**
	 * 첨부파일 Main(ML_FILE) 신규SEQ 조회
	 * 
	 * @return
	 */
	public int getNewAttcSeqn(){
		return Integer.parseInt( "" + getSqlMapClientTemplate().queryForObject( "Common.getNewAttcSeqn" ) );
	}
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	public List getMgntMail(Map map){
	    return getSqlMapClientTemplate().queryForList("Common.getMgntMail",map);
	}
	
	/**
	 * 기관/단체 조회
	 * @param map
	 * @return
	 */
	public List trstOrgnCoNameList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminUser.trstOrgnCoNameList",map);
	}
	
	/**
	 * 그룹별 메뉴목록
	 * 
	 * @return
	 */
	public List selectMenuGroupList(Map map){
		return getSqlMapClientTemplate().queryForList( "AdminMenu.selectMenuGroupList", map );
	}
	
	/**
	 * 로그인
	 * @param map
	 * @return
	 */
	public Map login(Map map) {
		return (Map)getSqlMapClientTemplate().queryForObject("AdminCommon.login",map);
	}
	
	/**
	 * 로그인 한 관리자 메뉴 조회
	 * 
	 * @param map
	 * @return
	 */
	public List selectLoginMenu(Map map){
		return getSqlMapClientTemplate().queryForList("AdminCommon.selectLoginMenu",map);
	}
	
	/**
	 * 관리자 로그 저장
	 * 
	 * @param map
	 */
	public void insertAdminLogDo(Map map) {
		getSqlMapClientTemplate().insert("AdminCommon.insertAdminLogDo", map);
	}
	
}
