package kr.or.copyright.mls.support.constant;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.Properties;

import kr.or.copyright.mls.support.util.FileUploadUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Constants {
	
	protected static Log logger = LogFactory.getLog(FileUploadUtil.class);
	
	 /*
	  * sign Image
	  */
	public final static String backbackImage = " [ ";
	public final static String goImage       = " ] ";
	public final static String backImage     = " [ ";
   
	/*
	 * 페이지 당 보여줄 목록의 수
	 */
	public final static int PAGE_SIZE=9;
	public final static int ROW_LIMIT=10;

	/*
	 * 여부 상수
	 */
	public final static String YES = "Y"; 
	public final static String NO = "N"; 
	   
	/*
	 * 공통코드그룹
	 */
	public final static String GRP_CD_PROC_STTS = "CD001";		//처리상태
	public final static String GRP_CD_ATTCH_FILE = "CD010";		//첨부파일
	public final static String GRP_CD_TRANS_TYPE = "CD003";	//전송형태
	public final static String GRP_CD_ANNEX_TYPE = "CD017";		//부속서종류
	public final static String GRP_CD_YES_NO = "CD002";		//여부구분
	public final static String GRP_CD_RORG_TYPE = "CD007";		//권리단체구분
	public final static String GRP_CD_CTRT_PRT_TYPE = "CD013";		//계약내용출력구분
	public final static String GRP_CD_AGRM_TYPE = "CD014";		//약관구분
	public final static String GRP_CD_MCMPY_TYPE = "CD016";		//이동통신사구분
	public final static String GRP_CD_INFO_UFEE_UNIT = "CD015";		//정보이용료단위
	public final static String GRP_CD_CTRT_TYPE = "CD021";		//계약구분
	public final static String GRP_CD_ATTCH_FILE_RF = "CD023";		//권리단체첨부파일
	public final static String GRP_CD_ATTCH_FILE_RF_RIGHT = "CD026";		//권리신청첨부파일
	public final static String GRP_CD_RF_USER_TYPE = "CD022";		//내권리찾기회원구분
	
	/*
	 * 처리상태 코드
	 */
	public final static String LIC_REQ_TYPE_CD_REQ = "01"; /* 신청 */
	public final static String LIC_REQ_TYPE_CD_RCPT = "02"; /* 접수 */
	public final static String LIC_REQ_TYPE_CD_CNFR = "03"; /* 협의 */
	public final static String LIC_REQ_TYPE_CD_RJCT = "05"; /* 거절 */
	public final static String LIC_REQ_TYPE_CD_CTRT = "06"; /* 계약 */
	public final static String LIC_REQ_TYPE_CD_CANCLE = "07"; /* 취소 */
	public final static String LIC_REQ_TYPE_CD_QUIT = "08"; /* 해지 */
	public final static String LIC_REQ_TYPE_CD_ADMIT = "09";	/* 승인 */
	
	/*
	 * 관리자로 로그인
	 */
	public final static String LOGIN_BY_ADMIN = "1";	
	
	/*
	 * 서비스사업자로 로그인
	 */
	public final static String LOGIN_BY_CP_USER = "2";	
	
	/*
	 * 일반사업자로 로그인
	 */
	public final static String LOGIN_BY_NONCP_USER = "3";
	
	/*
	 * CPM 시스템관리자 로그인
	 */
	public final static String LOGIN_BY_CPM = "4";
	
	/*
	 * CPM 일반관리자 로그인
	 */
	public final static String LOGIN_BY_CPM_ADMIN = "5";
	
	/*
	 * 시스템관리자로 로그인
	 */
	public final static String LOGIN_BY_SYSADMIN = "9";	

	
	/*
	 * 저심위 권리단체코드
	 */
	public final static String COPYRIGHT_ORG_CD = "00000";	
	public final static String KOMCA_ORG_CD = "00001";//KOMCA
	public final static String KAPP_ORG_CD = "00002";//음제협
	public final static String KPA_ORG_CD = "00003";//예단연
	public final static String KOMCA_NAME = "komca";//KOMCA
	public final static String KAPP_NAME = "kapp";//음제협
	public final static String KPA_NAME = "kpa";//예단연
	public final static String CPMADMIN_ID = "CPMADMIN";	// 권리찾기 관리자 ID
	public final static String CPMKOMCA_ID = "CPMKOMCAADMIN";	// KOMCA CPM관리자 ID
	public final static String CPMKAPP_ID = "CPMKAPPADMIN";		// 음제협 CPM관리자 ID
	public final static String CPMKAPO_ID = "CPMKPAADMIN";		// 예단연 CPM관리자 ID
	
	/*
	 * 이용자메뉴클래스 - 시스템안내
	 */
	public final static String MENU_CLASS_SYSGUIDE = "01";	//시스템안내
	public final static String MENU_CLASS_LICENSE = "02";	//이용허락
	public final static String MENU_CLASS_INFOSRCH = "03";	//정보조회	
	public final static String MENU_CLASS_CUSTOMER = "04";	//고객센터	

	/*
	 * 이용허락 계약해지신청 - 전송형태, 이동통신사, 처리상태
	 */
	public final static String TRANS_TYPE_GRPCD = "CD003";
	public final static String MCMPY_GRPCD = "CD016";
	public final static String PROC_STTS_GRPCD = "CD001";
	public final static String RJCT_CD = "05";
	public final static String REQ_CD = "01";
	
	/*
	 * 유무선구분
	 */
	public final static String TRANS_TYPE_CD_CABLE = "01";
	public final static String TRANS_TYPE_CD_WLESS = "02";
	
	
	/*
	 * 
	 */
	public final static String CTRT_CONT_PRT_TYPE_CTRT = "01"; // 계약서
	public final static String CTRT_CONT_PRT_TYPE_ANNEX = "02"; //부속서

	/*
	 * 법인,개인사업자구분
	 */
	public final static String COMNCD_CORPORATION = "01";	//법인
	public final static String COMNCD_PERSONAL = "02";		//개인

	/*
	 * 계약서 출력구분
	 */
	public final static String COMNCD_CTRT_PRT = "01";		//계약서단위 출력
	public final static String COMNCD_ANNEX_PRT = "02";		//부속서단위 출력
	
	/*
	 * 일반사업자이용허락신청 - 방문자수코드
	 */
	public final static String VISIT_CNT_GRPCD = "CD012";
	
	/*
	 * 협회이용자관리 - 이용자구분
	 */
	public final static String CMPY_TYPE_GRP_CD= "CD008";


	/*
	 * 권리단체구분코드
	 */
	public final static String COMNCD_PRXY_CMPY = "02";		//대리중개업체
	public final static String COMNCD_TRUST_ORG = "01";		//신탁기관
	
	/*
	 * 게시판구분코드
	 */
	public final static String BOARD_NOTICE = "1";   //공지사항 
	public final static String BOARD_QNA = "2";      //Q&A
	public final static String BOARD_FAQ = "3";	     //FAQ
	public final static String BOARD_PDS = "4";	     //자료실
	public final static String BOARD_BIZ_MOVE = "5"; //업계동향
	public final static String BOARD_FAQ_CPM = "6";	// 권리찾기 FAQ
	public final static String BOARD_QNA_CPM = "7";	// 권리찾기 QNA	

	/*
	 * 부속서구분코드
	 */
	public final static String COMNCD_ANNEX_NONE = "00";		//부속서없음
	public final static String COMNCD_ANNEX_CABLE = "01";		//유선부속서
	public final static String COMNCD_ANNEX_WLESS = "02";		//무선부속서
	public final static String COMNCD_ANNEX_ARS = "03";			//ARS부속서
	
	
	/*
	 * 파일경로
	 */
	//public final static String FILE_UPLOAD_DIR = "D:/webroot/mls_prj/WebContent/upload/";
	public final static String FILE_UPLOAD_DIR = "/home/MLS/mls_web/upload/";

	/*
	 * 계약구분코드
	 */
	public final static String COMNCD_MAIN_CTRT = "01";		//본계약
	public final static String COMNCD_ANNEX_CTRT = "02";		//부속계약
	
	/*
	 * 서비스형태구분코드
	 */
	public final static String SRVC_CLASS_CD = "CD024";
	public final static String SRVC_BGM_CD = "02";
	public final static String SRVC_COMMON_CD = "01";
	
	/*
	 * 홈페이지배경음악신청 사용료 형식
	 */
	public final static String VISIT_CNT_BASIC_CD = "01";	// 방문자수(5000명 미만)
	public final static String USE_TERM_BASIC_VALUE = "1";	// 사용기간(1개월)
	
	/*
	 *  내권리찾기 회원정보 수정
	 */
	public final static String COMNCD_ATTCH_CD = "04";		// 첨부파일 구분
	public final static String COMNCD_REL_DOC_CD = "05";	// 저작물 관려서류 구분
	
	/** 환경 파일명 **/
	public static String ConstantsPropertiesFile = "constants.properties";	// 인증서 정보 프로퍼티 파일
	
	/*
	 * 서버 타입
	 */
	public static final String server_type = "ServerType";	// 서버 타입 Property 명
	public static String SERVER_TYPE = getProperty(server_type);	// 서버 타입
	
	/*
	 * 메일 서버 IP
	 */
	public static final String mail_server_ip = "mail_server_ip";	// 메일 서버 IP Property 명
	public static String MAIL_SERVER_IP = getProperty(mail_server_ip);	// 메일 서버 IP
	
	/*
	 * 시스템 대표 메일 주소
	 */
	public static final String system_maill_address = "system_maill_address";	// 시스템 대표 메일 주소 Property 명
	public static String SYSTEM_MAIL_ADDRESS = getProperty(system_maill_address);	// 시스템 대표 메일 주소
	
	public static String SYSTEM_NAME = "저작권찾기사이트";

	/*
	 * 시스템 연락처 - 전화번호
	 */
	public static final String system_telephone = "system_telephone";	// 시스템 연락처 - 전화번호 Property 명
	public static String SYSTEM_TELEPHONE = getProperty(system_telephone);	// 시스템 연락처 - 전화번호
	
	/*
	 * 시스템 도메인 - HOME
	 */
	public static final String domain_home = "domain_home";	// 시스템 도메인 - HOME Property 명
	public static String DOMAIN_HOME = getProperty(domain_home);	// 시스템 도메인 - HOME

	/*
	 * 시스템 도메인 - HOME
	 */
	public static final String domain_home_forpc = "domain_home_forpc";	// 시스템 도메인 - HOME Property 명
	public static String DOMAIN_HOME_FORPC = getProperty(domain_home_forpc);	// 시스템 도메인 - HOME

	/*
	 * 시스템 도메인 - 뮤직
	 */
	public static final String domain_music = "domain_music";	// 시스템 도메인 - 뮤직 Property 명
	public static String DOMAIN_MUSIC = getProperty(domain_music);	// 시스템 도메인 - 뮤직

	/*
	 * 시스템 도메인 - 어문
	 */
	public static final String domain_book = "domain_book";	// 시스템 도메인 - 어문 Property 명
	public static String DOMAIN_BOOK = getProperty(domain_book);	// 시스템 도메인 - 어문
	
	/*
	 * 시스템 이름 - 저작권 찾기
	 */
	public static final String findCopyRight_name = "findCopyRight_name";	// 시스템 이름 - 저작권 찾기 Property 명
	public static String FINDCOPYRIGHT_NAME = getProperty(findCopyRight_name);	// 시스템 이름 - 저작권 찾기

    /**
     * 특정 키값을 반환한다.
     */
    public static String getProperty(String keyName) {
        String value = null;
  
        try {
            Properties props = new Properties();
            File oFile = new File(getResource(ConstantsPropertiesFile).getFile());
            FileInputStream fis = new FileInputStream(oFile);
            props.load(new java.io.BufferedInputStream(fis));
            value = props.getProperty(keyName).trim();
            fis.close();
        } catch (java.lang.Exception e) {
        	logger.error(e.toString());
        }
            return value;
    }
    
    /**
     * ClassLoader로 해당 properties 파일의 위치를 찾아온다.
     */
    public static URL getResource(String sUrl) {
    	URL oUrl = null;
    	
    	try {
    		oUrl = Thread.currentThread().getContextClassLoader().getResource(sUrl);
    	} catch (Exception e){
    		logger.error(e.toString());
    	}
    	
    	return oUrl;
    }

}