package kr.or.copyright.mls.mobile.model;

public class FAQ {
	private int
		bordSeqn
		, menuSeqn
		, threaded
	;
	
	private String 
		tite
		, bordDesc
	;

	public int getBordSeqn() {
		return bordSeqn;
	}

	public void setBordSeqn(int bordSeqn) {
		this.bordSeqn = bordSeqn;
	}

	public int getMenuSeqn() {
		return menuSeqn;
	}

	public void setMenuSeqn(int menuSeqn) {
		this.menuSeqn = menuSeqn;
	}

	public int getThreaded() {
		return threaded;
	}

	public void setThreaded(int threaded) {
		this.threaded = threaded;
	}

	public String getTite() {
		return tite;
	}

	public void setTite(String tite) {
		this.tite = tite;
	}

	public String getBordDesc() {
		return bordDesc;
	}

	public void setBordDesc(String bordDesc) {
		this.bordDesc = bordDesc;
	}

}
