package kr.or.copyright.mls.console.notdstbreport;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 미분배보상금 저작물보고 > 등록현황
 * 
 * @author ljh
 */
@Controller
public class FdcrAd61Controller extends DefaultController{

	@Resource( name = "fdcrAd61Service" )
	private FdcrAd61ServiceImpl fdcrAd61Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd61Controller.class );

	/**
	 * 등록현황 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/notdstbreport/fdcrAd61List1.page" )
	public String fdcrAd61List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		String REPT_YYYY = EgovWebUtil.getString( commandMap, "REPT_YYYY" );
		if(REPT_YYYY.equals( "" ) || REPT_YYYY == null){
			commandMap.put( "REPT_YYYY", "2016" );
		}
		commandMap.put( "TRST_ORGN_CODE", ConsoleLoginUser.getTrstOrgnCode() );

		fdcrAd61Service.fdcrAd61List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_List" ) );
		System.out.println( "등록현황 조회" );
		return "notdstbreport/fdcrAd61List1.tiles";
	}

}
