package kr.or.copyright.mls.console.effort;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd02Service;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Dao;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;

import org.springframework.stereotype.Service;

@Service( "fdcrAd02Service" )
public class FdcrAd02ServiceImpl extends CommandService implements FdcrAd02Service{

	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	@Resource( name = "fdcrAd06Dao" )
	private FdcrAd06Dao fdcrAd06Dao;

	/**
	 * 상당한 노력 신청 접수 - 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd02List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List pageList = (List) fdcrAd01Dao.statBord06RowList( commandMap );
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		
		List list = (List) fdcrAd01Dao.statBord06List( commandMap );
		List pageCount = (List) fdcrAd01Dao.statBord06RowCount( commandMap );
		
		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageList );
		commandMap.put( "ds_count", pageCount );
	}

	/**
	 * 상당한 노력 신청 접수 - 상세조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd02UpdateForm1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd01Dao.statBord06Detail( commandMap ); // 상세보기
		int genreCd = 0;
		int bigCode = 0;
		if(null!=list && list.size() > 0){
			Map<String,Object> detailMap = (Map<String,Object>)list.get( 0 );
			genreCd = EgovWebUtil.getInt( detailMap, "GENRE_CD" );
			if(genreCd == 1){
				bigCode = 62;
			} else if(genreCd==2){
				bigCode = 63;
			}else if(genreCd==3){
				bigCode = 64;
			}else if(genreCd==4){
				bigCode = 65;
			}else if(genreCd==5){
				bigCode = 66;
			}else if(genreCd==6){
				bigCode = 67;
			}else if(genreCd==7){
				bigCode = 68;
			}else if(genreCd==8){
				bigCode = 69;
			}else {
				bigCode= 0;
			}
		}
		
		commandMap.put( "BIG_CODE", bigCode );
		
		List shistFile = (List) fdcrAd01Dao.statBord06File( commandMap ); // 첨부파일
		List coptHodrList = (List) fdcrAd01Dao.statBord06CoptHodr( commandMap ); // 저작권자
		List shistList = (List) fdcrAd01Dao.statBord06Shist( commandMap ); // 진행상태
		List suplList = (List) fdcrAd01Dao.selectWorksSuplItemList( commandMap ); // 진행상태

		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_file", shistFile );
		commandMap.put( "ds_copt_hodr", coptHodrList );
		commandMap.put( "ds_shist", shistList );
		commandMap.put( "ds_supl_list", suplList );

	}

	/**
	 * 상당한 노력 신청 접수 - 저작권자 조회 공고내용 보완
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd02UpdateForm2( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd01Dao.statBord06Detail( commandMap ); // 상세보기
		List shistFile = (List) fdcrAd01Dao.statBord06File( commandMap ); // 첨부파일
		List suplCdList = (List) fdcrAd01Dao.selectCdList( commandMap );

		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_file", shistFile );
		commandMap.put( "ds_code", suplCdList );
	}

	/**
	 * 상당한노력신청 - 저작권자 조회 공고내용 수정 및 보완이력 등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd02Update2( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			List<Map> sendList = new ArrayList();
			List<Map> contentList = new ArrayList();
			Map sendMap = new HashMap();
			Map mailMap = new HashMap();
			String msgStorCd = "6";

			ArrayList<Map<String, Object>> suplList = (ArrayList<Map<String, Object>>) commandMap.get( "suplList" );
			String RGST_IDNT = (String) commandMap.get( "RGST_IDNT" );
			
			int suplId = fdcrAd01Dao.selectMaxSuplId();
			int suplSeq = fdcrAd01Dao.selectMaxWorksSuplSeq( commandMap );

			commandMap.put( "SUPL_CD", 3 );
			commandMap.put( "SUPL_SEQ", suplSeq );
			commandMap.put( "SUPL_ID", suplId );

			fdcrAd01Dao.suplIdInsert( commandMap );
			fdcrAd01Dao.suplWorksInsert( commandMap );


			for( int i = 0; i < suplList.size(); i++ ){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "SUPL_ID", suplId );
				param.put( "SUPL_SEQ", suplSeq );
				
				param.put( "SUPL_ITEM", suplList.get( i ).get( "supl_item" ) );
				param.put( "SUPL_ITEM_CD", suplList.get( i ).get( "supl_item_cd" ) );
				param.put( "PRE_ITEM", suplList.get( i ).get( "pre_item" ) );
				param.put( "POST_ITEM", suplList.get( i ).get( "post_item" ) );
				param.put( "RGST_IDNT", RGST_IDNT );
				
				fdcrAd01Dao.suplIdItemInsert( param );

				// 메일 발송 내용
				Map contentMap = new HashMap();
				String preItem = (String) param.get( "PRE_ITEM" );
				String postItem = (String) param.get( "POST_ITEM" );

				contentMap.put( "RNUM", i + 1 );
				contentMap.put( "SUPL_ITEM", param.get( "SUPL_ITEM" ) );
				contentMap.put( "CONTENT", "[" + preItem + "] 에서 [" + postItem + "] (으)로 보완" );

				contentList.add( contentMap );

			}
			fdcrAd01Dao.statBord06SuplUpdate( commandMap );

			/* 메일 발송준비 */

			String tite = (String) commandMap.get( "WORKS_TITLE" );// 저작물 제호

			StringBuffer sb = new StringBuffer();
			sb.append( "				<tr>" );
			sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">" );
			sb.append( "					<p style=\"font-weight:bold; background:url(" + Constants.DOMAIN_HOME
				+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">보완정보</p>" );
			sb.append( "					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">" );
			sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME
				+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 대상 저작물 : " + tite + "</li>" );
			// sb.append("						<li style=\"background:url(/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">신청일자 : 0000.00.00</li>");
			sb.append( "					</ul>" );
			sb.append( "					</td>" );
			sb.append( "				</tr>" );
			sb.append( "				<tr>" );
			sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">" );
			sb.append( "					<p style=\"font-weight:bold; background:url(" + Constants.DOMAIN_HOME
				+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">진행현황</p>" );
			sb.append( "					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">" );
			sb.append( "						<thead>" );
			sb.append( "							<tr>" );
			sb.append( "								<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"10%\">번호</th>" );
			sb.append( "								<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">공고항목</th>" );
			sb.append( "								<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">보완 세부내역</th>" );
			sb.append( "							</tr>" );
			sb.append( "						</thead>" );
			sb.append( "						<tbody>" );
			for( int i = 0; i < contentList.size(); i++ ){
				sb.append( "							<tr>" );
				sb.append( "							<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"
					+ contentList.get( i ).get( "RNUM" ) + "</td>" );
				sb.append( "							<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"
					+ contentList.get( i ).get( "SUPL_ITEM" ) + "</td>" );
				sb.append( "							<td style=\"padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"
					+ contentList.get( i ).get( "CONTENT" ) + "</td>" );
				sb.append( "							</tr>" );
			}
			sb.append( "						</tbody>" );
			sb.append( "					</table>" );
			sb.append( "				</td>" );
			sb.append( "			</tr>" );

			mailMap.put( "CONTENT_HTML", new String( sb ) );

			mailMap.put( "TITLE", "상당한노력 신청정보 보완처리가 완료되었습니다." );// 메일제목
			mailMap.put( "MSG_STOR_CD", msgStorCd );
			mailMap.put( "RGST_IDNT", (String) commandMap.get( "RGST_IDNT" ) );
			mailMap.put( "SEND_MAIL_ADDR", (String) commandMap.get( "SEND_MAIL_ADDR" ) );
			mailMap.put( "SUPL_ID", suplId );

			sendMap.put( "RECV_MAIL_ADDR", (String) commandMap.get( "RECV_MAIL_ADDR" ) );
			sendMap.put( "RECV_CD", 1 );
			sendMap.put( "RECV_NAME", (String) commandMap.get( "RGST_NAME" ) );
			sendMap.put( "SEND_MAIL_ADDR", (String) commandMap.get( "SEND_MAIL_ADDR" ) );

			sendList.add( sendMap );
			/*
			 * mailMap 필요 컬럼 MSG_STOR_CD - 메세지함 구분코드 TITLE - 메일제목 CONTENT - 메일내용
			 * txt CONTENT_HTML - 메일내용 HTML RGST_IDNT - 발신자 아이디 SEND_MAIL_ADDR -
			 * 발신자 메일주소 SUPL_ID - 보완 아이디 sendList Map 필요 컬럼 RECV_CD - 수신구분코드
			 * RECV_MAIL_ADDR - 수신자 메일 주소 RECV_NAME - 수신자 이름 SEND_MAIL_ADDR -
			 * 발신자 메일주소
			 */

			consoleCommonService.sendEmail( sendList, mailMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 진행상태 내역
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd02List2( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List list = (List) fdcrAd01Dao.statBord06ShistList( commandMap );

		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_list", list );
	}

	/**
	 * 진행상태 저장
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd02Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String[] worksIds = (String[]) commandMap.get( "worksIds" );
			String[] statCds = (String[]) commandMap.get( "statCds" );
			String[] rsltCds = (String[]) commandMap.get( "rsltCds" );

			for( int i = 0; i < worksIds.length; i++ ){
				Map param = new HashMap();
				param.put( "WORKS_ID", worksIds[i] );
				param.put( "STAT_CD", statCds[i] );
				param.put( "RSLT_CD", rsltCds[i] );
				param.put( "STAT_MEMO", commandMap.get( "STAT_MEMO" ) );
				param.put( "RGST_IDNT", commandMap.get( "RGST_IDNT" ) );

				int attachSeqn = consoleCommonDao.getNewAttcSeqn();

				Map<String, Object> fileInfo = new HashMap<String, Object>();
				if( fileList != null && fileList.size() > 0 ){

					fileInfo = fileList.get( 0 );
					param.put( "ATTC_SEQN", attachSeqn );

					Map fileMap = new HashMap();
					fileMap.put( "ATTC_SEQN", attachSeqn );
					fileMap.put( "FILE_ATTC_CD", "BO" );
					fileMap.put( "FILE_NAME_CD", 99 );
					fileMap.put( "FILE_NAME", fileInfo.get( "F_fileName" ) );
					fileMap.put( "FILE_PATH", fileInfo.get( "F_saveFilePath" ) );
					fileMap.put( "FILE_SIZE", fileInfo.get( "F_filesize" ) );
					fileMap.put( "REAL_FILE_NAME", fileInfo.get( "F_orgFileName" ) );
					fileMap.put( "RGST_IDNT", param.get( "RGST_IDNT" ) );
					fileMap.put( "WORKS_ID", param.get( "WORKS_ID" ) );

					// 첨부파일 등록
					fdcrAd06Dao.adminFileInsert( fileMap );
					fdcrAd01Dao.statBord06FileInsert( fileMap );
				}

				// 진행상태 정보 수정
				fdcrAd01Dao.statBord06ShistUpdate( param );

				// 진행상태 처리완료-승인일때 업데이트
				if( param.get( "RSLT_CD" ).equals( "1" ) ){
					fdcrAd01Dao.statBord06RsltMgnt( param );
				}
				// 진행상태 처리내역등록
				fdcrAd01Dao.statBord06ShistInsert( param );

				// 승인, 거절 시 메일,sms 보내기
				if( commandMap.get( "RGST_IDNT" ) != null
					&& ( param.get( "STAT_CD" ).equals( 2.0 ) || param.get( "STAT_CD" ).equals( 3.0 ) ) ){
					// 메일 보내기
					Map memMap = new HashMap();
					memMap.put( "USER_IDNT", param.get( "RGST_IDNT" ) );
					List memList = consoleCommonDao.selectMemberInfo( memMap );
					memMap = (Map) memList.get( 0 );

					String statNm = "";
					if( param.get( "STAT_CD" ).equals( "2" ) ){
						statNm = "접수";
					}else if( param.get( "STAT_CD" ).equals( "3" ) ){
						statNm = "처리완료";
					}
					String rsltNm = "";
					if( param.get( "RSLT_CD" ).equals( "1" ) ){
						rsltNm = "신청승인";
						statNm = "신청승인";
					}else if( param.get( "RSLT_CD" ).equals( "2" ) ){
						rsltNm = "신청기각";
						statNm = "신청기각";
					}
					String host = Constants.MAIL_SERVER_IP; // smtp서버
					String to = memMap.get( "MAIL" ).toString(); // 수신EMAIL
					String toName = memMap.get( "USER_NAME" ).toString(); // 수신인
					String title = param.get( "TITLE" ).toString();
					String subject = "[저작권찾기] 저작권자 찾기 위한 상당한 노력 신청  " + statNm;

					String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템
																	// 대표 메일
					String fromName = Constants.SYSTEM_NAME;

					if( memMap.get( "MAIL_RECE_YSNO" ).equals( "Y" ) && to != null && to != "" ){

						// ------------------------- mail 정보
						// ----------------------------------//
						MailInfo info = new MailInfo();
						info.setFrom( from );
						info.setFromName( fromName );
						info.setHost( host );
						info.setEmail( to );
						info.setEmailName( toName );
						info.setSubject( subject );

						StringBuffer sb = new StringBuffer();

						sb.append( "				<tr>" );
						sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">" );
						sb.append( "					<p style=\"font-weight:bold; background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 저작권자 찾기 위한 상당한 노력 신청 "
							+ statNm + " 정보</p>" );
						sb.append( "					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">" );
						sb.append( "						<li style=\"background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자 이름 : " + toName
							+ " </li>" );
						sb.append( "						<li style=\"background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : " + title
							+ " </li>" );
						sb.append( "						<li style=\"background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청일자 : "
							+ param.get( "RGST_DTTM" ) + " </li>" );
						sb.append( "						<li style=\"background:url("
							+ kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME
							+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 처리상태 : " + statNm
							+ " </li>" );
						sb.append( "					</ul>" );
						sb.append( "					</td>" );
						sb.append( "				</tr>" );

						info.setMessage( sb.toString() );
						info.setMessage( new String( MailMessage.getChangeInfoMailText2( info ) ) );

						/*
						 * sb.append("<div class=\"mail_title\">" + toName +
						 * "님, " +title+"의 저작권자 찾기 위한 상당한 노력 신청이  ["+statNm+
						 * "] 되었습니다. </div>");
						 * sb.append("<div class=\"mail_contents\"> ");
						 * sb.append(
						 * "	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> "
						 * ); sb.append("		<tr> "); sb.append("			<td> ");
						 * sb.append(
						 * "				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> "
						 * ); // 변경부분 sb.append("					<tr> ");
						 * sb.append("						<td class=\"tdLabel\">제호</td> ");
						 * sb.append("						<td class=\"tdData\">" +
						 * map.get("TITLE") + "</td> ");
						 * sb.append("					</tr> "); sb.append("					<tr> ");
						 * sb.append("						<td class=\"tdLabel\">신청일자</td> ");
						 * sb.append("						<td class=\"tdData\">" +
						 * map.get("RGST_DTTM") + "</td> ");
						 * sb.append("					</tr> "); sb.append("					<tr> ");
						 * sb.
						 * append("						<td class=\"tdLabel\">처리상태/결과</td> ");
						 * sb.append("						<td class=\"tdData\">" + statNm +
						 * "</td> "); sb.append("					</tr> ");
						 * sb.append("					<tr> ");
						 * sb.append("						<td class=\"tdLabel\">처리결과</td> ");
						 * sb.append("						<td class=\"tdData\">" + rsltNm +
						 * "</td> "); sb.append("					</tr> ");
						 * sb.append("				</table> "); sb.append("			</td> ");
						 * sb.append("		</tr> "); sb.append("	</table> ");
						 * sb.append("</div> "); info.setMessage(new
						 * String(MailMessage
						 * .getChangeInfoMailText(sb.toString())));
						 */
						MailManager manager = MailManager.getInstance();

						info = manager.sendMail( info );
						if( info.isSuccess() ){
							System.out.println( ">>>>>>>>>>>> message success :" + info.getEmail() );
						}else{
							System.out.println( ">>>>>>>>>>>> message false   :" + info.getEmail() );
						}

					}

					// sms 보내기
					String smsTo = memMap.get( "MOBL_PHON" ).toString();
					String smsFrom = Constants.SYSTEM_TELEPHONE;

					String smsMessage = "[저작권찾기]" + title + "의 저작권자 찾기 위한 상당한 노력 신청이 [" + statNm + "] 되었습니다. ";

					if( memMap.get( "SMS_RECE_YSNO" ) != null ){
						if( memMap.get( "SMS_RECE_YSNO" ).equals( "Y" ) && smsTo != null && smsTo != "" ){

							SMSManager smsManager = new SMSManager();

							System.out.println( "return = " + smsManager.sendSMS( smsTo, smsFrom, smsMessage ) );
						}
					}

				} // end email, sms

			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 상당한노력신청 - 저작권자 조회공고 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd02Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBord06DeltUpdate( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 첨부파일 조회
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public List fdcrAd02List3( Map<String, Object> commandMap ) throws Exception{
		return fdcrAd01Dao.statBord06File( commandMap );
	}
	
	/**
	 * 상세조회
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public List fdcrAd02List4( Map<String, Object> commandMap ) throws Exception{
		return fdcrAd01Dao.statBord06Detail(commandMap);
	}
}
