package kr.or.copyright.mls.article.service;

import java.util.List;

import javax.annotation.Resource;

import kr.or.copyright.mls.article.dao.ArticleDAO;
import kr.or.copyright.mls.article.model.Article;

import org.springframework.stereotype.Service;

@Service("ArticleBO")
public class ArticleServiceImpl implements ArticleService {
	
	@Resource(name="ArticleDAO")
	private ArticleDAO articleDAO;

	public List<Article> getArticleList(int communityId) {
		 return articleDAO.selectArticleList(communityId);
	}

	public Article getArticleInfo(int articleId) {
		return articleDAO.selectArticleInfo(articleId);
	}

	public int writeArticle(Article article) {
		 article.setReLevel(0);
	     article.setReDepth(0);
	     int articleId = articleDAO.insertArticle(article);
	     return articleId;
	}

	public void removeArticle(int articleId) {
		 articleDAO.deleteArticle(articleId);
	}
}

