package kr.or.copyright.mls.imit.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class ImitSqlMapDao extends SqlMapClientDaoSupport implements ImitDao {
	
	public List imitList(Map map) {
		return getSqlMapClientTemplate().queryForList("Imit.imitList",map);
	}	
	
	public List imitListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Imit.imitListCount",map);
	}

}
