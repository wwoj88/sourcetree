package kr.or.copyright.mls.mobile.dao;

import java.util.HashMap;
import java.util.List;

import kr.or.copyright.mls.mobile.model.Book;
import kr.or.copyright.mls.mobile.model.Broadcast;
import kr.or.copyright.mls.mobile.model.Image;
import kr.or.copyright.mls.mobile.model.Movie;
import kr.or.copyright.mls.mobile.model.Music;
import kr.or.copyright.mls.mobile.model.News;
import kr.or.copyright.mls.mobile.model.Script;

public interface MobileInfoDao {
	List<Music> musicList(HashMap<String,Object> searchWord);
	int musicListCnt(HashMap<String,Object> searchWord);
	Music musicDetl(HashMap<String,Object> searchWord);
	
	List<Book> bookList(HashMap<String,Object> searchWord);
	int bookListCnt(HashMap<String,Object> searchWord);
	Book bookDetl(HashMap<String,Object> searchWord);
	
	List<News> newsList(HashMap<String,Object> searchWord);
	int newsListCnt(HashMap<String,Object> searchWord);
	News newsDetl(HashMap<String,Object> searchWord);
	
	List<Script> scriptList(HashMap<String,Object> searchWord);
	int scriptListCnt(HashMap<String,Object> searchWord);
	Script scriptDetl(HashMap<String,Object> searchWord);
	
	List<Image> imageList(HashMap<String,Object> searchWord);
	int imageListCnt(HashMap<String,Object> searchWord);
	Image imageDetl(HashMap<String,Object> searchWord);
	
	List<Movie> movieList(HashMap<String,Object> searchWord);
	int movieListCnt(HashMap<String,Object> searchWord);
	Movie movieDetl(HashMap<String,Object> searchWord);
	
	List<Broadcast> broadcastList(HashMap<String,Object> searchWord);
	int broadcastListCnt(HashMap<String,Object> searchWord);
	Broadcast broadcastDetl(HashMap<String,Object> searchWord);
	
}
