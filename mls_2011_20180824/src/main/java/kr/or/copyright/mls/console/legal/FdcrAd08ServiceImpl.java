package kr.or.copyright.mls.console.legal;

import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd08Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd08Service" )
public class FdcrAd08ServiceImpl extends CommandService implements FdcrAd08Service{

	// AdminStatMgntSqlMapDao
	@Resource( name = "fdcrAd06Dao" )
	private FdcrAd06Dao fdcrAd06Dao;

	// AdminStatBoardSqlMapDao
	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	// AdminCommonSqlMapDao
	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	/**
	 * 이용승인 신청 공고 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception 
	 */
	public ArrayList<Map<String, Object>> fdcrAd08List1( Map<String, Object> commandMap ) throws Exception{
		List pageList = (List) fdcrAd01Dao.statBord01ListCount( commandMap ); // 페이징카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		
		return fdcrAd01Dao.statBord01List( commandMap );
	}

	/**
	 * 이용승인 신청 공고 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd08View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List detailList = (List) fdcrAd01Dao.statBord01Detail( commandMap );
		List fileList = (List) fdcrAd01Dao.statBord01File( commandMap );
		List objectList = (List) fdcrAd01Dao.statBord01Object( commandMap );
		List objectFileList = (List) fdcrAd01Dao.statBord01ObjectFile( commandMap );
		// 보완요청 LIST
		List suplList = (List) fdcrAd01Dao.selectBordSuplItemList( commandMap );

		commandMap.put( "detailList", detailList );
		commandMap.put( "fileList", fileList );
		commandMap.put( "objectList", objectList );
		commandMap.put( "objectFileList", objectFileList );
		commandMap.put( "suplList", suplList );
	}

	/**
	 * 이용승인 신청 공고 수정 폼
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd08UpdateForm1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List detailList = (List) fdcrAd01Dao.statBord01Detail( commandMap );
		List fileList = (List) fdcrAd01Dao.statBord01File( commandMap );
		List objectList = (List) fdcrAd01Dao.statBord01Object( commandMap );
		List objectFileList = (List) fdcrAd01Dao.statBord01ObjectFile( commandMap );
		// 보완요청 LIST
		List suplList = (List) fdcrAd01Dao.selectBordSuplItemList( commandMap );

		commandMap.put( "detailList", detailList );
		commandMap.put( "fileList", fileList );
		commandMap.put( "objectList", objectList );
		commandMap.put( "objectFileList", objectFileList );
		commandMap.put( "suplList", suplList );
	}

	/**
	 * 이용승인 신청 공고 수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd08Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			
			String BORD_CD = (String) commandMap.get( "BORD_CD" );
			String BORD_SEQN = (String) commandMap.get( "BORD_SEQN" );
			String TITE = (String) commandMap.get( "TITE" );
			String RECEIPT_NO = (String) commandMap.get( "RECEIPT_NO" );
			String DIVS_CD = (String) commandMap.get( "DIVS_CD" );
			String WORKS_ID = (String) commandMap.get( "WORKS_ID" );
			String ANUC_ITEM_1 = (String) commandMap.get( "ANUC_ITEM_1" );
			String ANUC_ITEM_2 = (String) commandMap.get( "ANUC_ITEM_2" );
			String ANUC_ITEM_3 = (String) commandMap.get( "ANUC_ITEM_3" );
			String ANUC_ITEM_4 = (String) commandMap.get( "ANUC_ITEM_4" );
			String ANUC_ITEM_5 = (String) commandMap.get( "ANUC_ITEM_5" );
			String ANUC_ITEM_6 = (String) commandMap.get( "ANUC_ITEM_6" );
			String ANUC_ITEM_7 = (String) commandMap.get( "ANUC_ITEM_7" );
			String ANUC_ITEM_8 = (String) commandMap.get( "ANUC_ITEM_8" );
			String ANUC_ITEM_9 = (String) commandMap.get( "ANUC_ITEM_9" );
			String ANUC_ITEM_10 = (String) commandMap.get( "ANUC_ITEM_10" );
			String ANUC_ITEM_11 = (String) commandMap.get( "ANUC_ITEM_11" );
			String ATTC_SEQNS[] = (String[]) commandMap.get( "ATTC_SEQN" );
			//String REAL_FILE_NAMES[] = (String[]) commandMap.get( "REAL_FILE_NAME" );

			// 공고내용 UPDATE처리
			//for( int i = 0; i < TITE.length(); i++ ){
				Map<String, Object> param = new HashMap<String, Object>();

				param.put( "BORD_CD", BORD_CD );
				param.put( "BORD_SEQN", BORD_SEQN );
				param.put( "TITE", TITE );
				param.put( "RECEIPT_NO", RECEIPT_NO );
				param.put( "DIVS_CD", DIVS_CD );
				//param.put( "WORKS_ID", WORKS_IDS[i] );
				param.put( "ANUC_ITEM_1", ANUC_ITEM_1 );
				param.put( "ANUC_ITEM_2", ANUC_ITEM_2 );
				param.put( "ANUC_ITEM_3", ANUC_ITEM_3 );
				param.put( "ANUC_ITEM_4", ANUC_ITEM_4 );
				param.put( "ANUC_ITEM_5", ANUC_ITEM_5 );
				param.put( "ANUC_ITEM_6", ANUC_ITEM_6 );
				param.put( "ANUC_ITEM_7", ANUC_ITEM_7 );
				param.put( "ANUC_ITEM_8", ANUC_ITEM_8 );
				param.put( "ANUC_ITEM_9", ANUC_ITEM_9 );
				param.put( "ANUC_ITEM_10", ANUC_ITEM_10 );
				param.put( "ANUC_ITEM_11", ANUC_ITEM_11 );

				fdcrAd01Dao.statBord01Update( param ); // 공고게시판 수정
			//}

			// File update
		
			Map<String, Object> fileInfo = new HashMap<String, Object>();
			if( fileList != null && fileList.size() > 0 ){
			     for( int i = 0; i < fileList.size(); i++ ){
			          int attachSeqn = consoleCommonDao.getNewAttcSeqn();
			          
			          System.out.println(fileList.get(i));
			          System.out.println(fileList.get(i).get( "F_orgFileName" ));
			          System.out.println(fileList.get(i).get( "F_saveFilePath" ));
			          System.out.println(fileList.get(i).get( "F_fileName" ));
			          Map fileMap = new HashMap();
			          fileMap.put( "ATTC_SEQN", attachSeqn );
			          fileMap.put( "BORD_CD", BORD_CD );
			          fileMap.put( "BORD_SEQN", BORD_SEQN );
			          fileMap.put( "REAL_FILE_NAME", fileList.get(i).get( "F_orgFileName" ) );
			          fileMap.put( "FILE_PATH", fileList.get(i).get( "F_saveFilePath" ) );
			          fileMap.put( "FILE_NAME", fileList.get(i).get( "F_fileName" ) );
			          fileMap.put( "FILE_SIZE", fileList.get(i).get( "F_filesize" ) );
			          fdcrAd01Dao.mlBord02FileInsert( fileMap ); // 첨부파일 등록
			          fdcrAd01Dao.statBord02FileInsert( fileMap ); // 공고게시판 첨부파일 등록
			     }
/*				fileInfo = fileList.get( 0 );
				commandMap.put( "ATTC_SEQN", attachSeqn );
				System.out.println((String) commandMap.get( "REAL_FILE_NAME" ));

				Map fileMap = new HashMap();
				Map upload = null;
				byte[] file = null;
				//file = (byte[]) commandMap.get( "CLIENT_FILE" );
				file = (byte[]) commandMap.get( ATTC_SEQNS[attachSeqn] );
				String fileName = (String) commandMap.get( "REAL_FILE_NAME" );
				//file = (byte[]) fileMap.get( CLIENT_FILES[attachSeqn] );
				//String fileName = (String) fileMap.get( "REAL_FILE_NAME" );
				upload = FileUtil.uploadMiFile2( fileName, file );

				fileMap.put( "ATTC_SEQN", attachSeqn );
				fileMap.put( "REAL_FILE_NAME", upload.get( "REAL_FILE_NAME" ) );
				fileMap.put( "FILE_PATH", upload.get( "FILE_PATH" ) );
				fileMap.put( "FILE_NAME", fileName );

				fdcrAd01Dao.mlBord02FileInsert( fileMap ); // 첨부파일 등록
				fdcrAd01Dao.statBord02FileInsert( fileMap ); // 공고게시판 첨부파일 등록
*/			}

			// FILE DELETE처리
/*			for( int i = 0; i < fileList.size(); i++ ){
				Map fileDeleteMap = new HashMap();
				fdcrAd01Dao.statBord02FileDelete( fileDeleteMap );

				//String fileName = fileDeleteMap.get( REAL_FILE_NAMES[i] ).toString();
				String fileName = (String) commandMap.get( "REAL_FILE_NAME" );

				try{
					File file = new File( (String) fileDeleteMap.get( "FILE_PATH" ), fileName );
					if( file.exists() ){
						file.delete();
					}
				}
				catch( Exception e ){
					e.printStackTrace();
				}
			} */
			fdcrAd01Dao.statBord02Modi( commandMap ); // 수정시 modi_dttm,modi_idnt
														// 업데이트
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 법정허락 이용승인 신청서 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd08Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd01Dao.statBord02Delete( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 이용승인신청공고 게시판 등록 폼
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd08WriteForm1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List detailList = (List) fdcrAd01Dao.statBord01Detail( commandMap );
		List fileList = (List) fdcrAd01Dao.statBord01File( commandMap );
		List objectList = (List) fdcrAd01Dao.statBord01Object( commandMap );
		List objectFileList = (List) fdcrAd01Dao.statBord01ObjectFile( commandMap );
		// 보완요청 LIST
		List suplList = (List) fdcrAd01Dao.selectBordSuplItemList( commandMap );

		commandMap.put( "detailList", detailList );
		commandMap.put( "fileList", fileList );
		commandMap.put( "objectList", objectList );
		commandMap.put( "objectFileList", objectFileList );
		commandMap.put( "suplList", suplList );

	}

	/**
	 * 이용승인신청공고 게시판 등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd08Regi1( Map<String, Object> commandMap) throws Exception{
		boolean result = false;
		try{
			String BORD_CD = (String) commandMap.get( "BORD_CD" );
			String[] TITES = (String[]) commandMap.get( "TITE" );
			String[] RECEIPT_NOS = (String[]) commandMap.get( "RECEIPT_NO" );
			String[] DIVS_CDS = (String[]) commandMap.get( "DIVS_CD" );
			String[] WORKS_IDS = (String[]) commandMap.get( "WORKS_ID" );
			String[] ANUC_ITEM_1S = (String[]) commandMap.get( "ANUC_ITEM_1" );
			String[] ANUC_ITEM_2S = (String[]) commandMap.get( "ANUC_ITEM_2" );
			String[] ANUC_ITEM_3S = (String[]) commandMap.get( "ANUC_ITEM_3" );
			String[] ANUC_ITEM_4S = (String[]) commandMap.get( "ANUC_ITEM_4" );
			String[] ANUC_ITEM_5S = (String[]) commandMap.get( "ANUC_ITEM_5" );
			String[] ANUC_ITEM_6S = (String[]) commandMap.get( "ANUC_ITEM_6" );
			String[] ANUC_ITEM_7S = (String[]) commandMap.get( "ANUC_ITEM_7" );
			String[] ANUC_ITEM_8S = (String[]) commandMap.get( "ANUC_ITEM_8" );
			String[] ANUC_ITEM_9S = (String[]) commandMap.get( "ANUC_ITEM_9" );
			String[] ANUC_ITEM_10S = (String[]) commandMap.get( "ANUC_ITEM_10" );
			String[] ANUC_ITEM_11S = (String[]) commandMap.get( "ANUC_ITEM_11" );

			//String[] ATTC_SEQNS = (String[]) commandMap.get( "ATTC_SEQN" );
			//String[] REAL_FILE_NAMES = (String[]) commandMap.get( "REAL_FILE_NAME" );

			String rgst_idnt = (String) commandMap.get( "RGST_IDNT" );

			for( int i = 0; i < RECEIPT_NOS.length; i++ ){

				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "BORD_CD", BORD_CD );
				param.put( "TITE", TITES[i] );
				param.put( "RECEIPT_NO", RECEIPT_NOS[i] );
				param.put( "DIVS_CD", DIVS_CDS[i] );
				//param.put( "WORKS_ID", WORKS_IDS[i] );
				param.put( "ANUC_ITEM_1", ANUC_ITEM_1S[i] );
				param.put( "ANUC_ITEM_2", ANUC_ITEM_2S[i] );
				param.put( "ANUC_ITEM_3", ANUC_ITEM_3S[i] );
				param.put( "ANUC_ITEM_4", ANUC_ITEM_4S[i] );
				param.put( "ANUC_ITEM_5", ANUC_ITEM_5S[i] );
				param.put( "ANUC_ITEM_6", ANUC_ITEM_6S[i] );
				param.put( "ANUC_ITEM_7", ANUC_ITEM_7S[i] );
				param.put( "ANUC_ITEM_8", ANUC_ITEM_8S[i] );
				param.put( "ANUC_ITEM_9", ANUC_ITEM_9S[i] );
				param.put( "ANUC_ITEM_10", ANUC_ITEM_10S[i] );
				param.put( "ANUC_ITEM_11", ANUC_ITEM_11S[i] );
				param.put( "RGST_IDNT", rgst_idnt );

				fdcrAd01Dao.statBord01Insert( param ); // 공고게시판 등록
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 이용승인신청 일괄등록 파일 업로드
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd08Upload1( ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			int attachSeqn = consoleCommonDao.getNewAttcSeqn();

			Map<String, Object> fileInfo = new HashMap<String, Object>();
			if( fileList != null && fileList.size() > 0 ){

				fileInfo = fileList.get( 0 );

				Map fileMap = new HashMap();
				fileMap.put( "ATTC_SEQN", attachSeqn );
				fileMap.put( "FILE_ATTC_CD", "BO" );
				fileMap.put( "FILE_NAME_CD", 99 );
				fileMap.put( "FILE_NAME", fileInfo.get( "F_fileName" ) );
				fileMap.put( "FILE_PATH", fileInfo.get( "F_saveFilePath" ) );
				fileMap.put( "FILE_SIZE", fileInfo.get( "F_filesize" ) );
				fileMap.put( "REAL_FILE_NAME", fileInfo.get( "F_orgFileName" ) );

				Map statAttcFileMap = new HashMap();

				statAttcFileMap.put( "ATTC_SEQN", attachSeqn );
				statAttcFileMap.put( "MODI_IDNT", fileMap.get( "RGST_IDNT" ) );

				// 첨부파일 등록
				fdcrAd06Dao.adminFileInsert( fileMap );
				fdcrAd01Dao.statBord06FileInsert( statAttcFileMap );
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 이용승인신청공고 게시판 일괄등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd08Regi2( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String[] TITES = (String[]) commandMap.get( "TITE" );
			String[] RECEIPT_NOS = (String[]) commandMap.get( "RECEIPT_NO" );
			String[] DIVS_CDS = (String[]) commandMap.get( "DIVS_CD" );
			String[] WORKS_IDS = (String[]) commandMap.get( "WORKS_ID" );
			String[] APPLY_TYPE_01S = (String[]) commandMap.get( "APPLY_TYPE_01" );
			String[] APPLY_TYPE_02S = (String[]) commandMap.get( "APPLY_TYPE_02" );
			String[] APPLY_TYPE_03S = (String[]) commandMap.get( "APPLY_TYPE_03" );
			String[] APPLY_TYPE_04S = (String[]) commandMap.get( "APPLY_TYPE_04" );
			String[] APPLY_TYPE_05S = (String[]) commandMap.get( "APPLY_TYPE_05" );
			String[] APPLY_TYPE_06S = (String[]) commandMap.get( "APPLY_TYPE_06" );
			String[] APPLY_TYPE_07S = (String[]) commandMap.get( "APPLY_TYPE_07" );
			String[] APPLY_TYPE_08S = (String[]) commandMap.get( "APPLY_TYPE_08" );
			String[] APPLY_TYPE_09S = (String[]) commandMap.get( "APPLY_TYPE_09" );
			String[] APPLY_TYPE_10S = (String[]) commandMap.get( "APPLY_TYPE_10" );
			String[] APPLY_TYPE_11S = (String[]) commandMap.get( "APPLY_TYPE_11" );
			String[] OPEN_DTTMS = (String[]) commandMap.get( "OPEN_DTTM" );

			String[] CLIENT_FILES = (String[]) commandMap.get( "CLIENT_FILE" );
			String[] REAL_FILE_NAMES = (String[]) commandMap.get( "REAL_FILE_NAME" );

			String rgst_idnt = (String) commandMap.get( "RGST_IDNT" );

			// 등록자 아이디
			String openIdnt = (String) commandMap.get( "OPEN_IDNT" );
			// 게시판구분 CD
			String bordCd = (String) commandMap.get( "BORD_CD" );

			// 일괄등록, 파일등록 INSERT
			for( int i = 0; i < WORKS_IDS.length; i++ ){
				String divs_cd = (String) commandMap.get( "DIVS_CD" );
				// 코드 구분
				if( divs_cd.equals( "1" ) ){
					divs_cd = "4";
				}else if( divs_cd.equals( "2" ) ){
					divs_cd = "1";
				}else if( divs_cd.equals( "3" ) ){
					divs_cd = "5";
				}

				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "TITE", TITES[i] );
				param.put( "RECEIPT_NO", RECEIPT_NOS[i] );
				param.put( "DIVS_CD", DIVS_CDS[i] );
				param.put( "WORKS_ID", WORKS_IDS[i] );
				param.put( "APPLY_TYPE_01", APPLY_TYPE_01S[i] );
				param.put( "APPLY_TYPE_02", APPLY_TYPE_02S[i] );
				param.put( "APPLY_TYPE_03", APPLY_TYPE_03S[i] );
				param.put( "APPLY_TYPE_04", APPLY_TYPE_04S[i] );
				param.put( "APPLY_TYPE_05", APPLY_TYPE_05S[i] );
				param.put( "APPLY_TYPE_06", APPLY_TYPE_06S[i] );
				param.put( "APPLY_TYPE_07", APPLY_TYPE_07S[i] );
				param.put( "APPLY_TYPE_08", APPLY_TYPE_08S[i] );
				param.put( "APPLY_TYPE_09", APPLY_TYPE_09S[i] );
				param.put( "APPLY_TYPE_10", APPLY_TYPE_10S[i] );
				param.put( "APPLY_TYPE_11", APPLY_TYPE_11S[i] );
				param.put( "OPEN_DTTM", OPEN_DTTMS[i] );
				param.put( "OPEN_IDNT", openIdnt );
				param.put( "RGST_IDNT", openIdnt );
				param.put( "BORD_CD", bordCd );

				fdcrAd01Dao.statBord01RegiAll( param ); // 일괄 등록
			}

			// File update
			int attachSeqn = consoleCommonDao.getNewAttcSeqn();
			String scan_yn = (String) commandMap.get( "SCAN_YN" );
			Map<String, Object> fileInfo = new HashMap<String, Object>();
			if( fileList != null && fileList.size() > 0 ){
				fileInfo = fileList.get( 0 );
				int bordSeqn = fdcrAd01Dao.getMaxBordSeqn();
				commandMap.put( "ATTC_SEQN", attachSeqn );

				Map fileMap = new HashMap();
				Map upload = null;
				byte[] file = null;
				file = (byte[]) fileMap.get( CLIENT_FILES[attachSeqn] );
				String fileName = (String) fileMap.get( "REAL_FILE_NAME" );
				upload = FileUtil.uploadMiFile2( fileName, file );

				fileMap.put( "BORD_SEQN", bordSeqn );
				fileMap.put( "ATTC_SEQN", attachSeqn );
				fileMap.put( "REAL_FILE_NAME", upload.get( "REAL_FILE_NAME" ) );
				fileMap.put( "FILE_PATH", upload.get( "FILE_PATH" ) );
				fileMap.put( "FILE_NAME", fileName );

				fdcrAd01Dao.mlBord02FileInsert( fileMap ); // 첨부파일 등록
				fdcrAd01Dao.statBord02FileInsert( fileMap ); // 공고게시판 첨부파일 등록
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 이용승인 신청 공고 목록 엑셀다운로드
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd08ExcelDown1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd01Dao.statBord01ExcelDown( commandMap );
		commandMap.put( "ds_exceldown", list );

	}

}
