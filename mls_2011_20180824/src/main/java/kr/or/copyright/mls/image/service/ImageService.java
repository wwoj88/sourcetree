package kr.or.copyright.mls.image.service;

import kr.or.copyright.mls.image.model.ImageDTO;

public interface ImageService{
	public ImageDTO getImageByWorkId(int works_id,int genreCode);
}
