package kr.or.copyright.mls.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.dao.CommonDao;
import kr.or.copyright.mls.common.dao.CommonSqlMapDao;

import com.tobesoft.platform.data.Dataset;

public class CommonServiceImpl extends BaseService implements CommonService {

//	private Log logger = LogFactory.getLog(getClass());

	private CommonDao commonDao;
	
	public void setCommonDao(CommonDao commonDao){
		this.commonDao = commonDao; 
	}
  
	 
	
	// 내권리찾기 사용자 조회
	public void getUserInfo() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     

		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) commonDao.getUserInfo(map);		
		List inmtList = null;
		
		// 보상회원정보 조회
		if( map.get("GUBUN")!=null && map.get("GUBUN").equals("INMT") ) {
		
			if( ((HashMap)list.get(0)).get("INMT_USER_YSNO").equals("Y") ) {
				
				inmtList = (List)commonDao.getInmtUserInfo(map);
			}
		}

	
		//hmap를 dataset로 변환한다.
		addList("gds_userInfo",  list);
		addList("ds_userInfo", inmtList);
	}

	
	// 첨부파일 Main(ML_FILE) 신규SEQ 조회
	public int getNewAttcSeqn() {System.out.println("@@@@@@@@@@@@ CommonService");
		int newAttcSeqn = 0;
		newAttcSeqn = commonDao.getNewAttcSeqn();
		
		return newAttcSeqn;
	}
	
	public List getMgntIdnt(Map map){
	    return commonDao.getMgntMail(map);
	}
	
	public List getMgntMail(Map params){
	    return commonDao.getMgntMail(params);
	}
}
