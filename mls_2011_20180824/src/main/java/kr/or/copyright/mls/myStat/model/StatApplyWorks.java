package kr.or.copyright.mls.myStat.model;

import java.lang.reflect.Field;

import kr.or.copyright.mls.common.BaseObject;

public class StatApplyWorks extends BaseObject {

	/*public String toString(){
		Class Cls = this.getClass();
		Field[] fie = Cls.getDeclaredFields();      //PUBLIC인 변수를 받는다.

		StringBuffer sb = new StringBuffer();
		sb.append(" @@@ "); sb.append(Cls.getName()); sb.append(" info @@@ ");

		try {
			for( int j = 0; j < fie.length; j++ ){
				 특정 필드의 접근제어자가 private 나 protected 로 접근이 불가능 하면
				* 임시로 접근 가능하게 수정 
				if( !fie[j].isAccessible() ) {
					fie[j].setAccessible( true );
				}
	
				String key = fie[j].getName();      //변수명
				//String value =  (String)Cls.getField(key).get(this);
				Object value = fie[j].get(this);
	
				if( j!=0 ){
					sb.append(" , "); 
				}
				sb.append(key); sb.append(" = "); sb.append(value);
			}
		}catch (Exception e) {
			System.out.println(e);
		}
		return sb.toString();
	}*/
	
	
	/** APPLY_TYPE */
	private String applyType;
	
	@Override
	public String toString() {
	    return "StatApplyWorks [applyType=" + applyType
		    + ", applyWriteYmd=" + applyWriteYmd + ", applyWriteSeq="
		    + applyWriteSeq + ", chngDttm=" + chngDttm + ", worksSeqn="
		    + worksSeqn + ", applyType01=" + applyType01
		    + ", applyType02=" + applyType02 + ", applyType03="
		    + applyType03 + ", applyType04=" + applyType04
		    + ", applyType05=" + applyType05 + ", worksTitl="
		    + worksTitl + ", worksKind=" + worksKind + ", worksForm="
		    + worksForm + ", worksDesc=" + worksDesc + ", publYmd="
		    + publYmd + ", publNatn=" + publNatn + ", publMediCd="
		    + publMediCd + ", publMediEtc=" + publMediEtc
		    + ", publMedi=" + publMedi + ", coptHodrName="
		    + coptHodrName + ", coptHodrTelxNumb=" + coptHodrTelxNumb
		    + ", coptHodrAddr=" + coptHodrAddr + ", statRsltCd="
		    + statRsltCd + ", statRsltDttm=" + statRsltDttm
		    + ", worksId=" + worksId + ", isUse=" + isUse + ", errMsg="
		    + errMsg + "]";
	}

	private String applyWriteYmd;       //신청서작성YMD
	private int applyWriteSeq;          //신청서작성SEQ
	private String chngDttm;            //변경일자
	private int worksSeqn;              //명세서순번
	private String applyType01;         //신청구분_저작물
	private String applyType02;         //신청구분_실연
	private String applyType03;         //신청구분_음반
	private String applyType04;         //신청구분_방송
	private String applyType05;         //신청구분_데이터베이스
	private String worksTitl;           //제호/제목
	private String worksKind;           //종류
	private String worksForm;           //형태 및 수량
	private String worksDesc;           //신청물의 내용
	private String publYmd;             //공표년월일
	private String publNatn;            //공표국가
	private int publMediCd;             //공표방법CD
	private String publMediEtc;         //공표방법_기타
	private String publMedi;            //공표매체종류
	private String coptHodrName;        //권리자_성명/법인명
	private String coptHodrTelxNumb;    //권리자_전화번호
	private String coptHodrAddr;        //권리자_주소
	private int statRsltCd;             //심의결과CD
	private String statRsltDttm;
	private String worksId;		    //저작물ID
	
	
	public String getWorksId() {
	    return worksId;
	}
	public void setWorksId(String worksId) {
	    this.worksId = worksId;
	}
	public String getApplyType01() {
		return applyType01;
	}
	public void setApplyType01(String applyType01) {
		this.applyType01 = applyType01;
	}
	public String getApplyType02() {
		return applyType02;
	}
	public void setApplyType02(String applyType02) {
		this.applyType02 = applyType02;
	}
	public String getApplyType03() {
		return applyType03;
	}
	public void setApplyType03(String applyType03) {
		this.applyType03 = applyType03;
	}
	public String getApplyType04() {
		return applyType04;
	}
	public void setApplyType04(String applyType04) {
		this.applyType04 = applyType04;
	}
	public String getApplyType05() {
		return applyType05;
	}
	public void setApplyType05(String applyType05) {
		this.applyType05 = applyType05;
	}
	public int getApplyWriteSeq() {
		return applyWriteSeq;
	}
	public void setApplyWriteSeq(int applyWriteSeq) {
		this.applyWriteSeq = applyWriteSeq;
	}
	public String getApplyWriteYmd() {
		return applyWriteYmd;
	}
	public void setApplyType(String applyType) {
	    this.applyType = applyType;
	}
	public String getApplyType() {
	    return applyType;
	}
	public void setApplyWriteYmd(String applyWriteYmd) {
		this.applyWriteYmd = applyWriteYmd;
	}
	public String getChngDttm() {
		return chngDttm;
	}
	public void setChngDttm(String chngDttm) {
		this.chngDttm = chngDttm;
	}
	public String getCoptHodrAddr() {
		return coptHodrAddr;
	}
	public void setCoptHodrAddr(String coptHodrAddr) {
		this.coptHodrAddr = coptHodrAddr;
	}
	public String getCoptHodrName() {
		return coptHodrName;
	}
	public void setCoptHodrName(String coptHodrName) {
		this.coptHodrName = coptHodrName;
	}
	public String getCoptHodrTelxNumb() {
		return coptHodrTelxNumb;
	}
	public void setCoptHodrTelxNumb(String coptHodrTelxNumb) {
		this.coptHodrTelxNumb = coptHodrTelxNumb;
	}
	public String getPublMedi() {
		return publMedi;
	}
	public void setPublMedi(String publMedi) {
		this.publMedi = publMedi;
	}
	public int getPublMediCd() {
		return publMediCd;
	}
	public void setPublMediCd(int publMediCd) {
		this.publMediCd = publMediCd;
	}
	public String getPublNatn() {
		return publNatn;
	}
	public void setPublNatn(String publNatn) {
		this.publNatn = publNatn;
	}
	public String getPublYmd() {
		return publYmd;
	}
	public void setPublYmd(String publYmd) {
		this.publYmd = publYmd;
	}
	public int getStatRsltCd() {
		return statRsltCd;
	}
	public void setStatRsltCd(int statRsltCd) {
		this.statRsltCd = statRsltCd;
	}
	public String getStatRsltDttm() {
		return statRsltDttm;
	}
	public void setStatRsltDttm(String statRsltDttm) {
		this.statRsltDttm = statRsltDttm;
	}
	public String getWorksDesc() {
		return worksDesc;
	}
	public void setWorksDesc(String worksDesc) {
		this.worksDesc = worksDesc;
	}
	public String getWorksForm() {
		return worksForm;
	}
	public void setWorksForm(String worksForm) {
		this.worksForm = worksForm;
	}
	public String getWorksKind() {
		return worksKind;
	}
	public void setWorksKind(String worksKind) {
		this.worksKind = worksKind;
	}
	public int getWorksSeqn() {
		return worksSeqn;
	}
	public void setWorksSeqn(int worksSeqn) {
		this.worksSeqn = worksSeqn;
	}
	public String getWorksTitl() {
		return worksTitl;
	}
	public void setWorksTitl(String worksTitl) {
		this.worksTitl = worksTitl;
	}
	public String getPublMediEtc() {
		return publMediEtc;
	}
	public void setPublMediEtc(String publMediEtc) {
		this.publMediEtc = publMediEtc;
	}
	
        private String isUse;
    
        private String errMsg;
	
        public String getIsUse() {
            return isUse;
        }
    
        public void setIsUse(String isUse) {
    		this.isUse = isUse;
        }
    
        public String getErrMsg() {
    		return errMsg;
        }
    
        public void setErrMsg(String errMsg) {
            this.errMsg = errMsg;
        }
	
	
}