package kr.or.copyright.mls.miplatform.service;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.miplatform.dao.AdminDao;

public class AdminServiceImpl extends BaseService implements AdminService {

	final private Log logger = LogFactory.getLog(getClass());

	private AdminDao adminDao;

	public void setAdminDao(AdminDao adminDao) {
		this.adminDao = adminDao;
	}

	public void login() throws Exception {

		boolean isValid = true;
		String msg = "";
		
		String loginId = (String) getParam("p_userid");
		String passwd  = (String) getParam("p_passwd");

		Map userMap = null;
		
		if (StringUtils.isEmpty(loginId)) {
			msg = "아이디를 입력해주세요.";
			isValid = false;
		}

		if (isValid && StringUtils.isEmpty(passwd)) {
			msg = "비밀번호를 입력해주세요.";
			isValid = false;
		}

		if(isValid){
			userMap = adminDao.findAdminByLoginId(loginId);
			logger.debug("userMap : " + userMap);
			
			if(userMap == null || userMap.size() < 1){
				msg = "로그인정보를 확인하여 주시기바랍니다.";
				isValid = false;
			}else{
				if(!StringUtils.equals(passwd, (String)userMap.get("PSWD"))){
					msg = "로그인정보를 확인하여 주시기바랍니다.";
					isValid = false;
				}
			}
		}
		
		if (!isValid) {
			setResultMessage(-1001, msg);
			return;
		}else{
			addList("gdsUser", userMap); // 로그인 사용자 정보뉴
			setResultMessage(0, "Success!");
		}
	}

	public void logout() throws Exception {
		
		setResultMessage(0, "Success!");
	}
}

