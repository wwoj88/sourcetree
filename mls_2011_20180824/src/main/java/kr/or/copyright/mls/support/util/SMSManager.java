/*
 * 작성된 날짜: 2008. 2. 18.
 *
 * TODO 생성된 파일에 대한 템플리트를 변경하려면 다음으로 이동하십시오.
 * 창 - 환경 설정 - Java - 코드 스타일 - 코드 템플리트
 */
package kr.or.copyright.mls.support.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SMSManager {
	
	private Connection con;							// SMS 서버에 접속하기 위한 Connection. MS-SQL 용
	
    /**
	 * SMS 테스트 확인
	 * @param CB (사용자고유명(user_dn)과 인증서일련번호(cert_sn)로 인증서존재여부 확인)
	 * @return SMS 서버에 접속될 경우 1, 접속오류발생시 < 0 값이 return 된다.
	 */
    public int connectSQL(){
    	int ret = 0;
    	
    	//String sServer = "61.104.38.39";				// SMS 서버 IP 또는 서버명
    	//String sUser = "ems";								// SMS 서버 DB 접속 아이디
    	//String sPass = "ems_manager";				// SMS 서버 DB 접속 패스워드
    	
    	/*String sServer = "61.104.38.15";			// SMS 서버 IP 또는 서버명
    	String sUser = "wwwsocop";					// SMS 서버 DB 접속 아이디
    	String sPass = "w15Dbop";					// SMS 서버 DB 접속 패스워드
    	 */    	
    	
    	//String sServer = "61.104.39.26";			// SMS 서버 IP 또는 서버명
    	String sServer = "222.231.43.26";			// SMS 서버 IP 또는 서버명
    	String sUser = "smsmanager";					// SMS 서버 DB 접속 아이디
    	String sPass = "eksanswk2014";					// SMS 서버 DB 접속 패스워드



	    try{
	        //Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
	       
	    	//System.out.println("SMS 발송용 MS-SQL 드라이버 로딩 성공");
	    	
	    	//con = DriverManager.getConnection("jdbc:microsoft:sqlserver://" + sServer + ":1433;User=" + sUser + ";Password=" + sPass);
	    	
	    	//System.out.println("SMS 발송용 MS-SQL DB 연결");	    	
	    	
	    	Class.forName("oracle.jdbc.driver.OracleDriver");
	       
	    	System.out.println("SMS 발송용 ORACLE 드라이버 로딩 성공");
	    	
	    	con = DriverManager.getConnection("jdbc:oracle:thin:@"+ sServer + ":1521:ora10g",sUser,sPass);
	    	
	    	System.out.println("SMS 발송용 ORACLE DB 연결");

	        
	        ret = 1;

	    }catch(ClassNotFoundException e){
	    	//System.out.println("SMS 발송용 MS-SQL 드라이버 로딩 실패");
	    	System.out.println("SMS 발송용 ORACLE 드라이버 로딩 실패");
	    	e.printStackTrace();
	        ret = -1;
	    }catch(SQLException e){
	    	//System.out.println("SMS 발송용 MS-SQL DB 연결 실패");
	    	System.out.println("SMS 발송용 ORACLE DB 연결 실패");
	        e.printStackTrace();
	        ret = -2;
	    }
	    
	    return ret;
	}

    public int disconnectSQL() 
	{
    	int ret = 0;
    	
	    try{
	    	con.close();

	    	//System.out.println("SMS 발송용 MS-SQL DB 종료");
	    	System.out.println("SMS 발송용 ORACLE DB 종료");
	    	
	        ret = 1;

	    }catch(Exception e){
	    	//System.out.println("SMS 발송용 MS-SQL DB 종료 실패");
	    	System.out.println("SMS 발송용 ORACLE DB 종료 실패");
	    	e.printStackTrace();
	        ret = -11;
	    }finally{
	    	try{ if(con != null) con.close(); } catch(Exception ex) {} 
	    }
	    
	    return ret;
	}

    // sTo : 받는 사람의 전화번호
    // sFrom : 보내는 사람의 전화번호, 현재는 문화부 대표전화
    // sMessage : 문자메세지 내용
	public int sendSMS(String sTo, String sFrom, String sMessage){

		PreparedStatement 	ps			= null;
		
		StringBuffer	sbQuery	= new StringBuffer();

		int iResult = 0;
		int iErrorCode = 0;
		
		iErrorCode = connectSQL();												// 현재 class 에서 SMS 용 MS-SQL 에 접속
		
		if(iErrorCode != 1) {
			iResult = iErrorCode;
			
			return iResult;
		}
		
		try {

			sbQuery.delete(0, sbQuery.length());

			//sbQuery.append(" INSERT INTO EM_TRAN ");
			//sbQuery.append(" ( TRAN_PHONE, TRAN_CALLBACK, TRAN_STATUS, TRAN_DATE, TRAN_MSG ) ");
			//sbQuery.append(" VALUES ");
			//sbQuery.append(" ( ?, ?, '1', getdate() , ? ) ");
			
			
			/*sbQuery.append(" INSERT INTO WWWSOCOP.SC_TRAN ");
			sbQuery.append(" (TR_NUM, TR_SENDDATE, TR_SENDSTAT, TR_PHONE, TR_CALLBACK ,TR_MSG) ");
			sbQuery.append(" VALUES ");
			sbQuery.append(" (WWWSOCOP.SC_SEQUENCE.NEXTVAL, SYSDATE, '0', ?, ?, ?) ");*/
			
			sbQuery.append(" INSERT INTO SC_TRAN  ");
			sbQuery.append(" (TR_NUM ,TR_SENDDATE , TR_SENDSTAT ,TR_MSGTYPE ,TR_PHONE ,TR_CALLBACK , TR_MSG) ");
			sbQuery.append(" VALUES ");
			sbQuery.append(" (SC_TRAN_SEQ.NEXTVAL, SYSDATE, '0', '0', ?, ?, ?) ");

			
			ps = con.prepareStatement(sbQuery.toString());
			
			synchronized(ps){
				ps.clearParameters();
				
				ps.setString(1, sTo);
				ps.setString(2, sFrom);
				ps.setString(3, sMessage);

				ps.executeUpdate();
			}

			ps.close(); 
			
			ps = null;
			
			iResult = 1;
			
		}catch(SQLException e){
			System.out.println("SMS DB SQL 오류 : " + e);
			iResult = -21;
		}catch(Exception e){
			System.out.println("SMS 프로그램 오류 : " + e);
			iResult = -22;
		}finally{
			try { if(ps != null) ps.close(); } catch(Exception e) {}
			disconnectSQL();											// 현재 class 에서 SMS 용 MS-SQL 에 접속 종료
		}
		
		return iResult;
	}

}
