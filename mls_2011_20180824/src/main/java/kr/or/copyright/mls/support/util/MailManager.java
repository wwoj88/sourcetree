package kr.or.copyright.mls.support.util;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.user.controller.UserController;

import org.apache.log4j.Logger;
 

/**
 * @version 1.0
 * @author park -  작성: 2008. 03. 13 
 * 
 */

public class MailManager {
	private Logger log = Logger.getLogger(this.getClass());
	
	private static MailManager instance = null;
	
	public static MailManager getInstance() {
		if (instance == null)
			synchronized (UserController.class) {
				if (instance == null)
					instance = new MailManager();
			}
		return instance;
	}


	/**
	 * @version 1.0
	 * @author cho in cheul -  작성: 2008. 05. 28 
	 * @return MailInfo
	 * @throws UnsupportedEncodingException 
	 * @
	 */
	public MailInfo sendMail(MailInfo info) {
		
		info.setSuccess(true);
		log.debug(">>>>>>>>>>> sendMail(MailInfo info) :"+info.toString());
		
        String host = info.getHost(); 	//smtp서버
        String to = info.getEmail();  	//수신인 주소
        String toName = info.getEmailName();  	//수신인 성명
        String from = info.getFrom();	//발신인 주소
        String fromName = info.getFromName();	//발신인 주소

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        Session session = Session.getInstance(props);
        try {
        	MimeMessage msg = new MimeMessage(session); //메세지 내용 담당 클래스인 MimeMessage 객체 생성
        
        	if(fromName != null && !fromName.equals("")){
        		msg.setFrom(new InternetAddress(from, MimeUtility.encodeText(fromName,"EUC-KR","B")));    //발신자 의 IP
        	} else {
        		msg.setFrom(new InternetAddress(from));    //발신자 의 IP
        	}
        	
        	if(toName != null && !toName.equals("")){
        		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to, MimeUtility.encodeText(toName,"EUC-KR","B")));//수신자의 IP (수신자가 다수일 경우 배열로 선언)
        	} else {
        		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));//수신자의 IP (수신자가 다수일 경우 배열로 선언)
        	}
            msg.setSubject(MimeUtility.encodeText(info.getSubject(),"EUC-KR","B")); 
            msg.setSentDate(new Date());        //날짜 구함
            info.setSendDttm(msg.getSentDate());
            msg.setContent(info.getMessage(),"text/html; charset=EUC-KR");
            Transport.send(msg);             //메일발송
        } catch (MessagingException mex) {
            mex.printStackTrace();
            info.setSuccess(false);
        } catch( UnsupportedEncodingException ux){
        	ux.printStackTrace();
            info.setSuccess(false);
        } catch( Exception e){
        	e.printStackTrace();
            info.setSuccess(false);
        }
        return info;
	}
	
	// 다중수신자의 경우.
	public MailInfo sendMailAll(MailInfo info) {
		
		info.setSuccess(true);
		
		System.out.println(">>>>>>>>>>> sendMail(MailInfo info) :"+info.toString());
		
        String host = info.getHost(); 	//smtp서버
        String to[] = info.getArrEmail();  	//수신인 주소
        String toName[] = info.getArrEmailName();  	//수신인 성명
        String from = info.getFrom();	//발신인 주소
        String fromName = info.getFromName();	//발신인 주소

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        Session session = Session.getInstance(props);
        
        try {
        	MimeMessage msg = new MimeMessage(session); //메세지 내용 담당 클래스인 MimeMessage 객체 생성
        
        	if(fromName != null && !fromName.equals("")){
        		msg.setFrom(new InternetAddress(from, MimeUtility.encodeText(fromName,"EUC-KR","B")));    //발신자 의 IP
        	} else {
        		msg.setFrom(new InternetAddress(from));    //발신자 의 IP
        	}
        	
        	//String toOne = "srkim@yagins.com"; String toOneName = "김소라";
        	
        	if(toName != null && !toName.equals("")){
        		//msg.setRecipient(Message.RecipientType.TO, new InternetAddress(toOne, MimeUtility.encodeText(toOneName,"EUC-KR","B")));//수신자의 IP (수신자가 다수일 경우 배열로 선언)
        		
        		int toSize = to.length;
        		InternetAddress address[] = new InternetAddress[toSize];

        		int i=0;

        		for(i=0;i<toSize;i++){
        			
        			System.out.println("to[i] ="+to[i]);
        			address[i] = new InternetAddress(to[i], MimeUtility.encodeText(toName[i],"EUC-KR","B"));
        		}
        		
        		msg.setRecipients(Message.RecipientType.TO, address);


        	} else {
        		//msg.setRecipient(Message.RecipientType.TO, new InternetAddress(toOne));//수신자의 IP (수신자가 다수일 경우 배열로 선언)
        		
        		int toSize = to.length;
        		InternetAddress[] address = new InternetAddress[toSize];

        		int i=0;

        		for(i=0;i<toSize;i++){
        			address[i] = new InternetAddress(to[i]);
        		}
        		
        		msg.setRecipients(Message.RecipientType.TO, address);
        		
        	}
        	
            msg.setSubject(MimeUtility.encodeText(info.getSubject(),"EUC-KR","B")); 
            msg.setSentDate(new Date());        //날짜 구함
            msg.setContent(info.getMessage(),"text/html; charset=EUC-KR");
            
            Transport.send(msg);             //메일발송
            
        } catch (MessagingException mex) {
            mex.printStackTrace();
            info.setSuccess(false);
        } catch( UnsupportedEncodingException ux){
        	ux.printStackTrace();
            info.setSuccess(false);
        } catch( Exception e){
        	e.printStackTrace();
            info.setSuccess(false);
        }
        return info;
	}

	public MailInfo sendMail(MailInfo info,String emailList[]){
		
		info.setSuccess(true);
		log.debug(">>>>>>>>>>> sendMail(MailInfo info) :"+info.toString());
		
        String host = info.getHost();	//smtp서버
        String from = info.getFrom();	//발신인 주소
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        Session session = Session.getInstance(props);
        try {
        	for(int i=0;i<emailList.length;i++){
	            MimeMessage msg = new MimeMessage(session); //메세지 내용 담당 클래스인 MimeMessage 객체 생성
	            msg.setFrom(new InternetAddress(from));    //발신자 의 IP
	            String to = emailList[i];  //수신인 주소
	            InternetAddress address = new InternetAddress(to);//수신자의 IP (수신자가 다수일 경우 배열로 선언)
	            msg.setRecipient(Message.RecipientType.TO, address);
	            msg.setSubject(info.getSubject()); 
	            msg.setSentDate(new Date());        //날짜 구함
	            msg.setText(info.getEmail());
	            Transport.send(msg);             //메일발송
        	}
        }
        
        catch (MessagingException mex) {
            mex.printStackTrace();
            info.setSuccess(false);
        }
        return info;
	}
	
//	public MailInfo sendMail(String email,String message,String subject){
//	MailInfo info =new MailInfo();
//    info.setSuccess(true);
//    info.setMail(email);
//
//    String host = "210.95.50.98";//smtp서버
//    String to = email;  //수신인 주소
//    String from = "jxmail@210.95.50.98";  //발신인 주소
//    Properties props = new Properties();
//    props.put("mail.smtp.host", host);
//    Session session = Session.getInstance(props);
//    try {
//     MimeMessage msg = new MimeMessage(session); //메세지 내용 담당 클래스인 MimeMessage 객체 생성
//        msg.setFrom(new InternetAddress(from));    //발신자 의 IP
//        InternetAddress address = new InternetAddress(to);//수신자의 IP (수신자가 다수일 경우 배열로 선언)
//        msg.setRecipient(Message.RecipientType.TO, address);
//        msg.setSubject(subject); 
//        msg.setSentDate(new Date());        //날짜 구함
//        msg.setText(message);
//        Transport.send(msg);             //메일발송
//    }
//    
//    catch (MessagingException mex) {
//        mex.printStackTrace();
//        info.setSuccess(false);
//        info.setMail(email);
//    }
//    return info;
//}
}
	
	
