package kr.or.copyright.mls.console.menu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.menu.inter.FdcrAdA9Dao;
import kr.or.copyright.mls.console.menu.inter.FdcrAdA9Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdA9Service" )
public class FdcrAdA9ServiceImpl extends CommandService implements FdcrAdA9Service{

	// old AdminUserDao
	@Resource( name = "fdcrAdA9Dao" )
	private FdcrAdA9Dao fdcrAdA9Dao;

	/**
	 * 메뉴 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA9List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAdA9Dao.selectMenuList();
		commandMap.put( "ds_menu_info", list );
	}

	/**
	 * 메뉴 등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdA9Write1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			int menuId = fdcrAdA9Dao.menuInsert( commandMap );
			commandMap.put( "MENU_ID", menuId );
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 메뉴 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdA9Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAdA9Dao.menuUpdate( commandMap );
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 메뉴 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdA9Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAdA9Dao.menuDelete( commandMap );
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 메뉴 저장처리
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void menuUpdate( Map<String, Object> commandMap ) throws Exception{
		// 수정 아이디 세팅
		String managerId = commandMap.get( "USER_ID" ).toString();

		String row_status = null;

		String[] delMenuIds = (String[]) commandMap.get( "MENU_ID" );
		// 삭제 처리
		for( int i = 0; i < delMenuIds.length; i++ ){
			Map<String, Object> deleteMap = new HashMap<String, Object>();
			deleteMap.put( "MENU_ID", delMenuIds[i] );
			fdcrAdA9Dao.menuDelete( deleteMap );
		}
		String[] MENU_ID = (String[]) commandMap.get( "MENU_ID" );
		String[] MENU_TITLE = (String[]) commandMap.get( "MENU_TITLE" );
		String[] THREADED = (String[]) commandMap.get( "THREADED" );
		String[] MENU_DEPTH = (String[]) commandMap.get( "MENU_DEPTH" );
		String[] MENU_ORDER = (String[]) commandMap.get( "MENU_ORDER" );
		String[] MENU_URL = (String[]) commandMap.get( "MENU_URL" );
		String[] MENU_KIND = (String[]) commandMap.get( "MENU_KIND" );
		String[] MODI_IDNT = (String[]) commandMap.get( "MODI_IDNT" );
		String[] MENU_DESC = (String[]) commandMap.get( "MENU_DESC" );
		String[] PARENT = (String[]) commandMap.get( "PARENT" );
		String[] PREFIX = (String[]) commandMap.get( "PREFIX" );
		String[] FORMID = (String[]) commandMap.get( "FORMID" );

		// 추가, 수정 처리
		for( int i = 0; i < MENU_ID.length; i++ ){
			Map<String, Object> insertMap = new HashMap<String, Object>();
			insertMap.put( "MENU_ID", MENU_ID[i] );
			insertMap.put( "MENU_TITLE", MENU_TITLE[i] );
			insertMap.put( "THREADED", THREADED[i] );
			insertMap.put( "MENU_DEPTH", MENU_DEPTH[i] );
			insertMap.put( "MENU_ORDER", MENU_ORDER[i] );
			insertMap.put( "MENU_URL", MENU_URL[i] );
			insertMap.put( "MENU_KIND", MENU_KIND[i] );
			insertMap.put( "MODI_IDNT", MODI_IDNT[i] );
			insertMap.put( "MENU_DESC", MENU_DESC[i] );
			insertMap.put( "PARENT", PARENT[i] );
			insertMap.put( "PREFIX", PREFIX[i] );
			insertMap.put( "FORMID", FORMID[i] );
			fdcrAdA9Dao.menuInsert( insertMap );
		}
	}

	/**
	 * 그룹 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void selectGroupList( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAdA9Dao.selectGroupList();
		commandMap.put( "ds_group_info", list );
	}

	/**
	 * 그룹 메뉴 조회하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void groupMenuInfo( Map<String, Object> commandMap ) throws Exception{
		List groupList = (List) fdcrAdA9Dao.groupInfo( commandMap );
		List groupMenuList = fdcrAdA9Dao.groupMenuInfo( commandMap );
		commandMap.put( "ds_group_info", groupList );
		commandMap.put( "ds_menu_info", groupMenuList );
	}

	/**
	 * 그룹 등록하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean groupInsert( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			int groupId = fdcrAdA9Dao.groupIdMax( commandMap );
			commandMap.put( "GROUP_SEQ", groupId );
			fdcrAdA9Dao.groupInsert( commandMap );
	
			String[] GROUP_YMD = (String[]) commandMap.get( "GROUP_YMD" );
			String[] MENU_ID = (String[]) commandMap.get( "MENU_ID" );
			String[] MODI_IDNT = (String[]) commandMap.get( "MODI_IDNT" );
	
			for( int i = 0; i < MENU_ID.length; i++ ){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "GROUP_YMD", GROUP_YMD[i] );
				map.put( "MENU_ID", MENU_ID[i] );
				map.put( "MODI_IDNT", MODI_IDNT[i] );
				map.put( "GROUP_SEQ", commandMap.get( "GROUP_SEQ" ) );
				fdcrAdA9Dao.groupMenuInsert( map );
			}
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 그룹 메뉴 삭제하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean groupDelete( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAdA9Dao.groupDelete( commandMap );
			fdcrAdA9Dao.groupMenuDelete( commandMap );
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 그룹 메뉴 수정하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean groupMenuUpdate( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAdA9Dao.groupUpdate( commandMap );
			fdcrAdA9Dao.groupMenuDelete( commandMap ); // 조건에 맞는 그룹메뉴 삭제하기
	
			String[] GROUP_YMD = (String[]) commandMap.get( "GROUP_YMD" );
			String[] GROUP_SEQ = (String[]) commandMap.get( "GROUP_SEQ" );
			String[] MENU_ID = (String[]) commandMap.get( "MENU_ID" );
			String[] MODI_IDNT = (String[]) commandMap.get( "MODI_IDNT" );
			// 그룹메뉴 다시 등록하기
	
			for( int i = 0; i < GROUP_YMD.length; i++ ){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "GROUP_YMD", GROUP_YMD[i] );
				map.put( "GROUP_SEQ", GROUP_SEQ[i] );
				map.put( "MENU_ID", MENU_ID[i] );
				map.put( "MODI_IDNT", MODI_IDNT[i] );
	
				fdcrAdA9Dao.groupMenuInsert( map );
	
			}
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 메뉴 조회
	 * @return
	 */
	public Map<String, Object> selectMenuInfo(Map<String, Object> commandMap) throws Exception{
		return fdcrAdA9Dao.selectMenuInfo(commandMap);
	}
}
