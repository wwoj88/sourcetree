package kr.or.copyright.mls.adminAllt.service;


public interface AdminAlltService {

	public void alltInmtDetl() throws Exception;	
	
	public void alltInmtSave() throws Exception;	
	
	public void alltInmtList() throws Exception;
	
	public void alltInmtYsNoUpdate() throws Exception;	
	
	public void alltInmtMgntUpdate() throws Exception;	
	
	/**
	 * @throws Exception
	 * @Date 2012.11.20
	 * @author ����ȣ
	 */
	public void alltInmtMgntList() throws Exception;
}
