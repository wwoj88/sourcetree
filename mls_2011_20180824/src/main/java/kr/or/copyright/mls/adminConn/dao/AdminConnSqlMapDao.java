package kr.or.copyright.mls.adminConn.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class AdminConnSqlMapDao extends SqlMapClientDaoSupport implements AdminConnDao {

	// 회원정보 목록조회
	public List monthList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.monthList",map);
	}
	
	// 회원정보 목록조회
	public List dayList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.dayList",map);
	}	
	
	// 기간별 접속 통계목록
	public List adminStatPeriList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.adminStatPeriList", map);
	}
	
	// 기간별 접속 통계목록(월별)
	public List adminStatPeriMonthList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.adminStatPeriMonthList", map);
	}
		
	//	내권리찾기, 보상금 통계
	public List rsltList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.rsltList",map);
	}	
	
	// 저작권찾기, 보상금 기간별 통계
	public List rsltPeriList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.rsltPeriList", map);
	}
	
	//	접속내역
	public List userList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.userList",map);
	}	
	
	// 접속내역 삭제
	public void userListDelete(Map map) {
		getSqlMapClientTemplate().delete("AdminConn.userListDelete", map);
	}

	//법정허락 연도별 신청/승인건수
	public List statYearApplyList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.statYearApplyList",map);
	}

	//법정허락 연도별 저작물,실연,음반,방송,DB 신청/승인건수
	public List statYearSpplyList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.statYearSpplyList",map);
	}
	
	//법정허락 연도별 보상금액 
	public List statYearLgmtAmntList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.statYearLgmtAmntList",map);
	}

	// 기간별 접속 통계목록 모바일
	public List adminStatPeriList2(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.adminStatPeriList2",map);
	}

	// 기간별 접속 통계목록 모바일 (월별) 
	public List adminStatPeriMonthList2(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.adminStatPeriMonthList2",map);
	}
	
	
	//법정허락 이용승인 건수 통계(년도별)
	public List statYearPrpsApplyList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.statYearPrpsApplyList",map);
	}	
	//보상금공탁서 공고 건수 통계(년도별)
	public List statYearInmtList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.statYearInmtList",map);
	}
		
	//상당한노력 신청 건수 통계(년도별 - 분기별) 
	public List statYearQuProcList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.statYearQuProcList",map);
	}	
	//상당한노력 신청 건수 통계(년도별 - 장르별)
	public List statYearGenreProcList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.statYearGenreProcList",map);
	}
	//법정허락 대상 저작물 현황
	public List statPrpsWorksList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.statPrpsWorksList",map);
	}
	
	public List statPrpsApplyList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminConn.statPrpsApplyList",map);
		
	}


	

}
