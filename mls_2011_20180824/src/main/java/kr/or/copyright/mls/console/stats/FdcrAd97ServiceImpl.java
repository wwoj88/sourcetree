package kr.or.copyright.mls.console.stats;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd15Dao;
import kr.or.copyright.mls.console.stats.inter.FdcrAd97Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tobesoft.platform.data.Dataset;

@Service( "fdcrAd97Service" )
public class FdcrAd97ServiceImpl extends CommandService implements FdcrAd97Service{
	
	// OldDao adminStatProcDao
	@Resource( name = "fdcrAd03Dao" )
	private FdcrAd03Dao fdcrAd03Dao;
	
	// old AdminWorksMgntDao
	@Autowired
	private FdcrAd15Dao fdcrAd15Dao;

	
	/**
	 * 조회
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd97List1( Map<String, Object> commandMap ) throws Exception {
	    List list = (List)fdcrAd03Dao.worksEntReptView(commandMap);
	    commandMap.put("ds_list", list);
	}
	
	/**
	 * old worksMgntMnthList
	 * 월별 보고현황 조회
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd97List2(Map<String, Object> commandMap) throws Exception {
		//월별 보고정보
		List dsReport = (List)fdcrAd15Dao.worksMgntReptInfo(commandMap);
		Map<String,Object> reportMap = null;
		if(null!=dsReport){
			reportMap = (Map<String,Object>)dsReport.get( 0 );
		}
		
		//월별,장르별 통계 정보
		List dsCount = (List)fdcrAd15Dao.worksMgntReptView(commandMap);
		Map<String,Object> countMap = null;
		if(null!=dsCount){
			countMap = (Map<String,Object>)dsCount.get( 0 );
		}
		//월별 저작물 리스트
		commandMap.put("ds_count", countMap);
		commandMap.put("ds_report", reportMap);
	}
	
	/**
	 * old worksMgntList
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd97List3(Map<String, Object> commandMap) throws Exception {
		List pageList = (List)fdcrAd15Dao.totalRowWorksMgntList(commandMap);
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		List list = (List)fdcrAd15Dao.worksMgntList(commandMap);
		
		commandMap.put("ds_list", list);
		commandMap.put("ds_page", pageList);
		
    }
}
