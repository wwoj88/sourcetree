package kr.or.copyright.mls.console.rcept;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd15Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd15Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service( "fdcrAd15Service" )
public class FdcrAd15ServiceImpl extends CommandService implements FdcrAd15Service{

	// old AdminWorksMgntDao
	@Autowired
	private FdcrAd15Dao fdcrAd15Dao;

	/**
	 * 위탁관리저작물 보고현황 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15List1( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd15Dao.worksMgntReptView( commandMap );
		List list_0 = (List) fdcrAd15Dao.worksMgntReptViewCoCnt( commandMap );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_list_0", list_0 );
	}

	/**
	 * 위탁관리저작물 장르별 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15List2( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd15Dao.worksMgntReptYearView( commandMap );
		commandMap.put( "ds_list", list );
	}

	/**
	 * 위탁관리저작물 월별 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15List3( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd15Dao.worksMgntReptMonthView( commandMap );
		List list_0 = (List) fdcrAd15Dao.worksMgntReptMonthCoCnt( commandMap );

		commandMap.put( "ds_list", list );
		commandMap.put( "ds_list_0", list_0 );
	}

	/**
	 * 월별 보고현황 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15List4( Map<String, Object> commandMap ) throws Exception{
		// 월별 보고정보
		// 월별,장르별통계정보
		List dsReport = (List) fdcrAd15Dao.worksMgntReptInfo( commandMap );
		// 월별저작물리스트
		List dsCount = (List) fdcrAd15Dao.worksMgntReptView( commandMap );

		//월별 보고현황 목록 카운트
		List pageList = (List) fdcrAd15Dao.totalRowWorksMgntList( commandMap ); // 페이징카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		
		//월별 보고현황 목록
		List list = (List) fdcrAd15Dao.worksMgntList( commandMap );
		
		commandMap.put( "ds_count", dsCount );
		commandMap.put( "ds_report", dsReport );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageList );
	}

	/**
	 * 월별 보고현황 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15List5( Map<String, Object> commandMap ) throws Exception{
		List pageList = (List) fdcrAd15Dao.totalRowWorksMgntList( commandMap );
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		List list = (List) fdcrAd15Dao.worksMgntList( commandMap );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageList );

	}

	/**
	 * 엑셀다운로드
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15Down1( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd15Dao.worksMgntReptViewNewExcel( commandMap );
		List list_0 = (List) fdcrAd15Dao.worksMgntReptViewCoCntNewExcel( commandMap );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_list_0", list_0 );
	}

	public void fdcrAd15Down2( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd15Dao.worksMgntReptMonthViewNewExcel( commandMap );
		List list_0 = (List) fdcrAd15Dao.worksMgntReptMonthCoCntNewExcel( commandMap );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_list_0", list_0 );
	}
}
