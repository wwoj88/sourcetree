package kr.or.copyright.mls.console.effort;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAd01Dao" )
public class FdcrAd01DaoImpl extends EgovComAbstractDAO implements FdcrAd01Dao{

	// 이용승인신청 공고 목록 카운트
	public List statBord01ListCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord01ListCount", map );
	}

	// 이용승인신청 공고 - 엑셀다운 2014-11-10 이병원
	public List statBord01ExcelDown( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord01ExcelDown", map );
	}

	public List statBord01RowCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord01RowCount", map );
	}

	// 이용승인신청 공고 목록
	public ArrayList<Map<String, Object>> statBord01List( Map<String, Object> commandMap ){
		return (ArrayList<Map<String, Object>>) getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord01List", commandMap );
	}

	// 이용승인신청 공고 상세
	public List statBord01Detail( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord01Detail", map );
	}

	// 이용승인신청 등록
	public void statBord01Insert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.statBord01Insert", map );
	}

	// 이용승인신청 데이터 중복확인
	public int statBord01Select( Map map ){
		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminStatBoard.statBord01Select", map );
	}

	// 이용승인신청 일괄등록
	public void statBord01RegiAll( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.statBord01RegiAll", map );
	}

	public void statBord01Update( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBord01Update", map );

	}

	// 이용승인신청 공고 첨부파일
	public List statBord01File( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord01File", map );
	}

	// 이용승인신청 공고 이의제기
	public List statBord01Object( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord01Object", map );
	}

	// 이용승인신청 공고 이의제기 첨부파일
	public List statBord01ObjectFile( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord01ObjectFile", map );
	}

	// 이의제기 신청정보 수정
	public void statBordObjcUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBordObjcUpdate", map );
	}

	public void statBordObjcRsltUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBordObjcRsltUpdate", map );
	}

	// 이의제기신청 처리내역등록
	public void statBordObjcShisInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.statBordObjcShisInsert", map );
	}

	// 이의제기 처리내역 목록조회
	public List adminStatObjcShisSelect( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.adminStatObjcShisSelect", map );
	}

	// 이용승인신청 승인 공고 목록
	public ArrayList<Map<String, Object>> statBord02List( Map<String, Object> commandMap ){
		return (ArrayList<Map<String, Object>>) getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord02List", commandMap );
	}

	// 이용승인신청 승인공고 - 엑셀다운
	public List statBord02ExcelDown( Map map ){

		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord02ExcelDown", map );

	}

	public List statBordDetail( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBordDetail", map );
	}

	public void statBord02Regi( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.statBord02Regi", map );
	}

	// 이용승인신청 승인공고 일괄등록
	public void statBord02RegiAll( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.statBord02RegiAll", map );
	}

	// 이용승인신청 승인공고 데이터 중복 확인
	public int statBord02Select( Map map1 ){
		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminStatBoard.statBord02Select", map1 );
	}

	public void mlBord02FileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.mlBord02FileInsert", map );
	}

	public void statBord02FileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.statBord02FileInsert", map );
	}

	public void statBord02Update( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBord02Update", map );
	}

	public void statBord02Modi( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBord02Modi", map );

	}

	public int getMaxBordSeqn(){
		return Integer.parseInt( "" + getSqlMapClientTemplate().queryForObject( "AdminStatBoard.getMaxBordSeqn" ) );
	}

	public void statBord02FileDelete( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.statBord02FileDelete", map );
	}

	public void statBord02Delete( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBord02Delete", map );
	}

	public List statBord0203RowList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord0203RowList", map );
	}

	public List statBord03List( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord03List", map );
	}

	public List statBord03RowCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord03RowCount", map );
	}

	public void statBord03Mgnt( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBord03Mgnt", map );
	}

	public List statBord04List( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord04List", map );
	}

	public List statBord04RowList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord04RowList", map );
	}

	public List statBord04RowCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord04RowCount", map );
	}

	public List statBord05List( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord05List", map );
	}

	public List statBord05RowList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord05RowList", map );
	}

	public List statBord05RowCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord05RowCount", map );
	}

	public List statBord05Detail( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord05Detail", map );
	}

	public List statBord05Object( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord05Object", map );
	}

	public List statBord05ObjectFile( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord05ObjectFile", map );
	}

	public List objeList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.objeList", map );
	}

	public List objeRowList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.objeRowList", map );
	}

	public List statBord06List( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord06List", map );
	}

	public List statBord06RowList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord06RowList", map );
	}

	public List statBord06RowCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord06RowCount", map );
	}

	public List statBord06Detail( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord06Detail", map );
	}

	public List statBord06File( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord06File", map );
	}

	public List statBord06Shist( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord06Shist", map );
	}

	public List statBord06ShistList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord06ShistList", map );
	}

	public List statBord06CoptHodr( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.statBord06CoptHodr", map );
	}

	public void statBord06ShistUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBord06ShistUpdate", map );
	}

	public void statBord06ShistInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.statBord06ShistInsert", map );

	}

	public void statBord06RsltMgnt( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBord06RsltMgnt", map );

	}

	public void statBord06FileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.statBord06FileInsert", map );

	}

	public List mainStatBordList01( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.mainStatBordList01", map );
	}

	public List mainStatBordList02( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.mainStatBordList02", map );
	}

	// 저작권자 조회공고, 보상금 공탁공고 처리상태 수정(신청,보완,신청승인,반려,)
	public void statBordStatUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBordStatUpdate", map );
	}

	// 저작권자 조회공고, 보상금 공탁공고 삭제
	public void statBordStatDeltUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBordStatDeltUpdate", map );
	}

	// 저작권자 조회공고, 보상금 공탁공고 보완항목 코드 목록
	public List selectCdList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.selectCdList", map );
	}

	// 저작권자 조회공고, 보상금 공탁공고 보완내역 보완차수 최대값 + 1
	public int selectMaxSuplSeq( Map map ){
		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminStatBoard.selectMaxSuplSeq", map );
	}

	// 저작권자 조회공고, 보상금 공탁공고 보완내역 보완ID 최대값 + 1
	public int selectMaxSuplId(){

		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminStatBoard.selectMaxSuplId" );
	}

	// 상당한노력신청 - 저작권자 조회공고 보완내역 보완차수 최대값 + 1
	public int selectMaxWorksSuplSeq( Map map ){

		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminStatBoard.selectMaxWorksSuplSeq", map );
	}

	// 저작권자 조회공고, 보상금 공탁공고 보완내역 등록
	public void suplIdInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.suplIdInsert", map );
	}

	// 저작권자 조회공고, 보상금 공탁공고 보완목록 등록
	public void suplHistInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.suplHistInsert", map );
	}

	// 상당한노력신청 - 저작권자 조회공고 보완목록 등록
	public void suplWorksInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.suplWorksInsert", map );
	}

	// 저작권자 조회공고, 보상금 공탁공고 보완항목 등록
	public void suplIdItemInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminStatBoard.suplIdItemInsert", map );
	}

	public void statBordSuplUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBordSuplUpdate", map );

	}

	// 상당한노력신청 저작권자 조회 공고내용 수정(보완) -->
	public void statBord06SuplUpdate( Map map ){

		getSqlMapClientTemplate().update( "AdminStatBoard.statBord06SuplUpdate", map );
	}

	// 저작권자 조회공고 보완목록 불러오기
	public List selectBordSuplItemList( Map map ){

		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.selectBordSuplItemList", map );

	}

	// 상당한노력신청 - 저작권자 조회공고 보완목록 불러오기 -->
	public List selectWorksSuplItemList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.selectWorksSuplItemList", map );
	}

	// 상당한노력신청 - 저작권자 조회공고 삭제
	public void statBord06DeltUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminStatBoard.statBord06DeltUpdate", map );
	}

	// 신청정보 보완안내 메일발신내역-->
	public List selectBordSuplSendList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminStatBoard.selectBordSuplSendList", map );
	}

	// 법정허락 대상저작물 목록 삭제
	public void statBord07Update( Map map ){
		// TODO Auto-generated method stub
		getSqlMapClientTemplate().update( "AdminStatBoard.statBord07Update", map );
	}

	// 법정허락 대상저작물 목록 일괄 삭제
	public void statBord07UpdateAll( Map map ){
		// TODO Auto-generated method stub
		getSqlMapClientTemplate().update( "AdminStatBoard.statBord07UpdateAll", map );
	}

	// 보상금 공탁공고 내용 불러오기
	public Map<String, Object> statBord10Detail( Map<String, Object> commandMap ){
		// TODO Auto-generated method stub
		return (Map<String, Object>) selectByPk( "AdminStatBoard.statBord01Detail", commandMap );
	}

}
