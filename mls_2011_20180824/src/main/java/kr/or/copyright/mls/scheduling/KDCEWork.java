package kr.or.copyright.mls.scheduling;

import java.text.SimpleDateFormat;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class KDCEWork extends SqlMapClientDaoSupport{
	
	public void execute() throws Exception{
		
		long time = System.currentTimeMillis();         
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");         
		System.out.println( "KDCEWork INSERT" );
		System.out.println( "getSqlMapClientTemplate().insert(\"KDCEWork.KDCE_Music_Report\") : " + getSqlMapClientTemplate().insert( "KDCEWork.KDCE_Music_Report") );
		System.out.println( "getSqlMapClientTemplate().insert(\"KDCEWork.KDCE_Music\") : " + getSqlMapClientTemplate().insert( "KDCEWork.KDCE_Music") );
		System.out.println( "getSqlMapClientTemplate().insert(\"KDCEWork.KDCE_Book_Report\") : " + getSqlMapClientTemplate().insert( "KDCEWork.KDCE_Book_Report") );
		System.out.println( "getSqlMapClientTemplate().insert(\"KDCEWork.KDCE_Book\") : " + getSqlMapClientTemplate().insert( "KDCEWork.KDCE_Book") );
		System.out.println( "getSqlMapClientTemplate().insert(\"KDCEWork.KDCE_News_Report\") : " + getSqlMapClientTemplate().insert( "KDCEWork.KDCE_News_Report") );
		System.out.println( "getSqlMapClientTemplate().insert(\"KDCEWork.KDCE_News\") : " + getSqlMapClientTemplate().insert( "KDCEWork.KDCE_News") );
		System.out.println( "getSqlMapClientTemplate().insert(\"KDCEWork.KDCE_Art_Report\") : " + getSqlMapClientTemplate().insert( "KDCEWork.KDCE_Art_Report") );
		System.out.println( "getSqlMapClientTemplate().insert(\"KDCEWork.KDCE_Art\") : " + getSqlMapClientTemplate().insert( "KDCEWork.KDCE_Art") );
		/*System.out.println( "getSqlMapClientTemplate().insert(\"KDCEWork.selectDate\") : " + getSqlMapClientTemplate().insert( "KDCEWork.KDCE_Movie_Report") );
		System.out.println( "getSqlMapClientTemplate().insert(\"KDCEWork.selectDate\") : " + getSqlMapClientTemplate().insert( "KDCEWork.KDCE_Movie") );*/
	}

}
