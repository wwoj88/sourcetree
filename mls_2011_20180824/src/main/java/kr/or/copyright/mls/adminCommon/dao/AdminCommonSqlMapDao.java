package kr.or.copyright.mls.adminCommon.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class AdminCommonSqlMapDao extends SqlMapClientDaoSupport implements AdminCommonDao {

	public List login(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminCommon.login",map);
	}
	
	// 로그인 한 관리자 메뉴 조회
	public List selectLoginMenu(Map map){
		return getSqlMapClientTemplate().queryForList("AdminCommon.selectLoginMenu",map);
	}
	
	// 회원 정보 조회
	public List selectMemberInfo(Map map){
		return getSqlMapClientTemplate().queryForList("AdminCommon.selectMemberInfo",map);
	}
	
	// 관리자 로그 저장
	public void insertAdminLogDo(Map map) {
		getSqlMapClientTemplate().insert("AdminCommon.insertAdminLogDo", map);
	}
	
}
