package kr.or.copyright.mls.console.rwmnyreqst;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.rightreqst.inter.FdcrAd35Dao;
import kr.or.copyright.mls.console.rwmnyreqst.inter.FdcrAd36Service;
import kr.or.copyright.mls.inmt.dao.InmtDao;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;

import org.springframework.stereotype.Service;

import com.softforum.xdbe.xCrypto;

@Service( "fdcrAd36Service" )
public class FdcrAd36ServiceImpl extends CommandService implements FdcrAd36Service{

	@Resource( name = "fdcrAd35Dao" )
	private FdcrAd35Dao fdcrAd35Dao;

	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	@Resource( name = "inmtDao" )
	private InmtDao inmtDao;

	/**
	 * 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd36List1( Map<String, Object> commandMap ) throws Exception{

		Map<String, Object> param = new HashMap<String, Object>();
		param.put( "GUBUN_TRST_ORGN_CODE", commandMap.get( "TRST_ORGN_CODE" ) );

		// DAO 호출
		List list = (List) fdcrAd35Dao.inmtList( commandMap );
		List pageCount = (List) fdcrAd35Dao.inmtListCount( commandMap );

		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageCount );

	}

	/**
	 * 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public void fdcrAd36View1( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List prpsList = (List) inmtDao.inmtPrpsSelect( commandMap ); // ML_PRPS
		List rsltList = (List) inmtDao.inmtPrpsRsltList( commandMap ); // ML_PRPS_RSLT
		List fileList = (List) inmtDao.inmtFileList( commandMap ); // ML_PRPS_ATTC
		List totDealStat = (List) inmtDao.inmtTotalDealStat( commandMap ); // 전체처리상태
																			// 값
		List kappList = null; // ML_KAPP_PRPS
		List fokapoList = null; // ML_FOKAPO_PRPS
		List krtraList = null; // ML_KRTRA_PRPS
		List krtraList_2 = null; // ML_KRTRA_PRPS
		List tempList = (List) inmtDao.inmtTempList( commandMap ); // 신청대상명
		List trstList = new ArrayList(); // 관련 신탁데이타

		String attFileYn = "";
		String offxListRecp = "";
		int iTrst = 0;
		int i202 = 0;
		int i203 = 0;
		int i205 = 0;
		int i205_2 = 0;

		for( int rs = 0; rs < rsltList.size(); rs++ ){

			HashMap hMap = (HashMap) rsltList.get( rs );

			// 음제협
			if( hMap.get( "TRST_ORGN_CODE" ).equals( "203" ) ){

				kappList = (List) inmtDao.inmtKappList( commandMap );

				if( kappList.size() > 0 ){
					attFileYn = (String) ( (HashMap) kappList.get( 0 ) ).get( "ATTC_YSNO" );
					offxListRecp = (String) ( (HashMap) kappList.get( 0 ) ).get( "OFFX_LINE_RECP" );
				}

			}

			// 음실연
			else if( hMap.get( "TRST_ORGN_CODE" ).equals( "202" ) ){

				fokapoList = (List) inmtDao.inmtFokapoList( commandMap );

				if( fokapoList.size() > 0 ){
					attFileYn = (String) ( (HashMap) fokapoList.get( 0 ) ).get( "ATTC_YSNO" );
					offxListRecp = (String) ( (HashMap) fokapoList.get( 0 ) ).get( "OFFX_LINE_RECP" );
				}
			}

			// 복제협

			else if( hMap.get( "TRST_ORGN_CODE" ).equals( "205" ) && hMap.get( "PRPS_DIVS" ).equals( "S" ) ){

				commandMap.put( "TRST_ORGN_CODE", hMap.get( "TRST_ORGN_CODE" ) );
				commandMap.put( "PRPS_DIVS", hMap.get( "PRPS_DIVS" ) );

				krtraList = (List) inmtDao.inmtKrtraList( commandMap );

				if( krtraList.size() > 0 ){
					attFileYn = (String) ( (HashMap) krtraList.get( 0 ) ).get( "ATTC_YSNO" );
					offxListRecp = (String) ( (HashMap) krtraList.get( 0 ) ).get( "OFFX_LINE_RECP" );
				}
			}

			else if( hMap.get( "TRST_ORGN_CODE" ).equals( "205" ) && hMap.get( "PRPS_DIVS" ).equals( "L" ) ){

				commandMap.put( "TRST_ORGN_CODE", hMap.get( "TRST_ORGN_CODE" ) );
				commandMap.put( "PRPS_DIVS", hMap.get( "PRPS_DIVS" ) );

				krtraList_2 = (List) inmtDao.inmtKrtraList( commandMap );

				if( krtraList_2.size() > 0 ){
					attFileYn = (String) ( (HashMap) krtraList_2.get( 0 ) ).get( "ATTC_YSNO" );
					offxListRecp = (String) ( (HashMap) krtraList_2.get( 0 ) ).get( "OFFX_LINE_RECP" );
				}
			}

			if( hMap.get( "TRST_ORGN_CODE" ).equals( "203" ) && ( ++i203 ) == 1
				|| hMap.get( "TRST_ORGN_CODE" ).equals( "202" ) && ( ++i202 ) == 1
				|| hMap.get( "TRST_ORGN_CODE" ).equals( "205" ) && hMap.get( "PRPS_DIVS" ).equals( "S" )
				&& ( ++i205 ) == 1 || hMap.get( "TRST_ORGN_CODE" ).equals( "205" )
				&& hMap.get( "PRPS_DIVS" ).equals( "L" ) && ( ++i205_2 ) == 1 ){

				HashMap trMap = new HashMap();

				if( hMap.get( "TRST_ORGN_CODE" ).equals( "205" ) && hMap.get( "PRPS_DIVS" ).equals( "L" ) ) trMap.put(
					"TRST_ORGN_CODE",
					"205_2" );
				else trMap.put( "TRST_ORGN_CODE", hMap.get( "TRST_ORGN_CODE" ) ); // 분류코드값
																					// :
																					// 신탁기관

				trMap.put( "DEAL_STAT", hMap.get( "DEAL_STAT" ) ); // 상태 값
				trMap.put( "RSLT_DESC", hMap.get( "RSLT_DESC" ) ); // 처리결과
				trMap.put( "TRST_ATTCHFILE_YN", attFileYn ); // 파일첨부 YN 값
				trMap.put( "OFFX_LINE_RECP", offxListRecp ); // 오프라인접수 YN 값
				trMap.put( "INMT_AMNT", hMap.get( "INMT_AMNT" ) ); // 보상금액

				trstList.add( iTrst, trMap );

				iTrst++;
			}
		}
		commandMap.put( "ds_list", prpsList );
		commandMap.put( "ds_fileList", fileList );
		commandMap.put( "ds_totDealStat", totDealStat );
		commandMap.put( "ds_list_trst", trstList );
		commandMap.put( "ds_list_kapp", kappList );
		commandMap.put( "ds_list_fokapo", fokapoList );
		commandMap.put( "ds_list_krtra", krtraList );
		commandMap.put( "ds_list_krtra2", krtraList_2 );
		commandMap.put( "ds_temp", tempList );
	}

	/**
	 * 신청자 정보 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd36View2( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List list2 = (List) consoleCommonDao.userDetlList( commandMap );

		/* resd복호화 str 20121108 정병호 */
		xCrypto.RegisterEx(
			"pattern7",
			2,
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
			"pool1",
			"mcst_db",
			"mcst_owner",
			"mcst_table",
			"pattern7" );

		Map list = new HashMap();
		list = (Map) list2.get( 0 );

		String RESD_CORP_NUMB = "";

		// 주민번호가 notnull 인 경우만.
		if( list.get( "RESD_CORP_NUMB" ) != null
			&& list.get( "RESD_CORP_NUMB" ).toString().replaceAll( " ", "" ).length() > 0 ){
			RESD_CORP_NUMB = xCrypto.Decrypt( "pattern7", (String) list.get( "RESD_CORP_NUMB" ) ).replaceAll( " ", "" ); // 원본
																															// 주민번호
																															// 복호화

			String RESD_CORP_NUMB1 = RESD_CORP_NUMB.substring( 0, 6 );
			String RESD_CORP_NUMB2 = ""; // RESD_CORP_NUMB.substring(6);

			list.put( "RESD_CORP_NUMB", RESD_CORP_NUMB1 + "-" + RESD_CORP_NUMB2 );
		}

		commandMap.put( "ds_list", list );

	}

	/**
	 * 신청저작물 상세정보 팝업
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public void fdcrAd36Pop1( Map<String, Object> commandMap ) throws Exception{

		// if (conMap.get("TRST_ORGN_CODE").equals("205_2"))
		// conMap.put("TRST_ORGN_CODE", "205");

		// DAO 호출
		List rsltList = (List) fdcrAd35Dao.inmtRsltList( commandMap );

		commandMap.put( "ds_list", rsltList );

	}

	/**
	 * 신청내역 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd36Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			// 삭제처리 전에 현재 최대상태값을 확인
			String maxDealStat = "";
			List maxDealStatList = inmtDao.getMaxDealStat( commandMap );

			maxDealStat = (String) ( (HashMap) maxDealStatList.get( 0 ) ).get( "MAX_DEAL_STAT" );

			System.out.println( "==========================================" );
			System.out.println( "maxDealStat::::" + maxDealStat );
			System.out.println( "==========================================" );

			String[] PRPS_MAST_KEYS = (String[]) commandMap.get( "PRPS_MAST_KEY" );
			String[] PRPS_SEQNS = (String[]) commandMap.get( "PRPS_SEQN" );
			String[] ATTC_SEQNS = (String[]) commandMap.get( "ATTC_SEQN" );
			String[] REAL_FILE_NAMES = (String[]) commandMap.get( "REAL_FILE_NAME" );

			// 삭제처리 - 최대상태값이 1 신청인 경우
			if( maxDealStat.equals( "1" ) ){

				inmtDao.inmtKappDelete( commandMap ); // DELETE ML_KAPP_PRPS
				inmtDao.inmtFokapoDelete( commandMap ); // DELETE ML_FOKAPO_PRPS
				inmtDao.inmtKrtraDelete( commandMap ); // DELETE ML_KRTRA_PRPS
				inmtDao.inmtPrpsRsltDelete( commandMap ); // DELETE ML_PRPS_RSLT
				inmtDao.inmtPrpsDelete( commandMap ); // DELETE ML_PRPS

				// 첨부파일삭제
				for( int fi = 0; fi < PRPS_MAST_KEYS.length; fi++ ){

					Map<String, Object> param = new HashMap<String, Object>();

					param.put( "PRPS_MAST_KEY", PRPS_MAST_KEYS[fi] );
					param.put( "PRPS_SEQN", PRPS_SEQNS[fi] );
					param.put( "ATTC_SEQN", ATTC_SEQNS[fi] );

					// Map fileDelMap = getDeleteMap(ds_fileList, fi);
					inmtDao.inmtFileDelete( param );

					String fileName = commandMap.get( REAL_FILE_NAMES[fi] ).toString();

					try{
						File file = new File( (String) commandMap.get( "FILE_PATH" ), fileName );
						if( file.exists() ){
							file.delete();
						}
					}
					catch( Exception e ){
						e.printStackTrace();
					}
				}

			} // end if.. 삭제처리
			commandMap.put( "ds_maxDealStat", maxDealStatList );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 처리결과 및 신청정보 등록
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd36Insert1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			// UPDATE처리
			String userId = commandMap.get( "USER_ID" ).toString();
			String prpsDivs = commandMap.get( "PRPS_DIVS" ).toString();

			String[] PRPS_MAST_KEYS = (String[]) commandMap.get( "PRPS_MAST_KEY" );
			String[] PRPS_IDNTS = (String[]) commandMap.get( "PRPS_IDNT" );
			String[] PRPS_DIVSS = (String[]) commandMap.get( "PRPS_DIVS" );
			String[] TRST_ORGN_CODES = (String[]) commandMap.get( "TRST_ORGN_CODE" );

			int iCnt = 0;

			for( int i = 0; i < PRPS_IDNTS.length; i++ ){

				Map<String, Object> param = new HashMap<String, Object>();

				param.put( "PRPS_MAST_KEY", PRPS_MAST_KEYS[i] );
				param.put( "PRPS_IDNT", PRPS_IDNTS[i] );
				param.put( "PRPS_DIVS", PRPS_DIVSS[i] );
				param.put( "TRST_ORGN_CODE", TRST_ORGN_CODES[i] );

				if( commandMap.get( "DEAL_STAT" ).equals( "4" ) ) iCnt++;

				param.put( "userId", userId );

				param.put( "rgstIdnt", commandMap.get( "RGST_IDNT" ) );

				if( commandMap.get( "ORGN_TRST_CODE" ).equals( "205_2" ) ) param.put( "ORGN_TRST_CODE", "205" );

				fdcrAd35Dao.inmtRsltUpdate( param );

				// 분배여부 업데이트 한다. (20110107 추가)
				if( commandMap.get( "ALLT_YSNO" ) != null ){

					fdcrAd35Dao.inmtUpdate( param );

				}
			}

			// 보상회원여부 update
			String inmtUserYsno = commandMap.get( "INMT_USER_YSNO" ).toString();

			if( inmtUserYsno.equals( "N" ) && iCnt > 0 ){

				commandMap.put( "inmtUserYsno", "Y" );
				fdcrAd35Dao.inmtUserYsnoUpdate( commandMap );
			}

			// 메일, SMS
			commandMap.put( "DEAL_STAT", "" );
			int totalCnt = fdcrAd35Dao.inmtRsltCnt( commandMap ); // 전체 결과 Cnt

			commandMap.put( "DEAL_STAT", "4" );
			int doneCnt = fdcrAd35Dao.inmtRsltCnt( commandMap ); // 완료 Cnt

			if( ( totalCnt - doneCnt ) == 0 ){
				// ----------------------------- mail
				// send------------------------------//

				String host = Constants.MAIL_SERVER_IP; // smtp서버
				String to = commandMap.get( "U_MAIL" ).toString(); // 수신EMAIL
				String toName = commandMap.get( "PRPS_NAME" ).toString(); // 수신인
				String subject = "[저작권찾기] 보상금신청 처리완료";
				String idntName = fdcrAd35Dao.inmtPrpsTitle( commandMap ); // infoMap.get("PRPS_IDNT_NAME").toString();
																			// //
																			// 신청대상명

				String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표
																// 메일
				String fromName = Constants.SYSTEM_NAME;

				if( commandMap.get( "U_MAIL_RECE_YSNO" ).equals( "Y" ) && to != null && to != "" ){

					// ------------------------- mail 정보
					// ----------------------------------//
					MailInfo info = new MailInfo();
					info.setFrom( from );
					info.setFromName( fromName );
					info.setHost( host );
					info.setEmail( to );
					info.setEmailName( toName );
					info.setSubject( subject );

					StringBuffer sb = new StringBuffer();

					sb.append( "<div class=\"mail_title\">" + toName + "님, 보상금신청이 처리완료 되었습니다. </div>" );
					sb.append( "<div class=\"mail_contents\"> " );
					sb.append( "	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> " );
					sb.append( "		<tr> " );
					sb.append( "			<td> " );
					sb.append( "				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> " );
					// 변경부분
					sb.append( "					<tr> " );
					sb.append( "						<td class=\"tdLabel\">신청 저작물명</td> " );
					sb.append( "						<td class=\"tdData\">" + idntName + "</td> " );
					sb.append( "					</tr> " );
					sb.append( "				</table> " );
					sb.append( "			</td> " );
					sb.append( "		</tr> " );
					sb.append( "	</table> " );
					sb.append( "</div> " );

					info.setMessage( new String( MailMessage.getChangeInfoMailText( sb.toString() ) ) );
					MailManager manager = MailManager.getInstance();

					info = manager.sendMail( info );
					if( info.isSuccess() ){
						System.out.println( ">>>>>>>>>>>> message success :" + info.getEmail() );
					}else{
						System.out.println( ">>>>>>>>>>>> message false   :" + info.getEmail() );
					}
				}

				String smsTo = commandMap.get( "U_MOBL_PHON" ).toString();
				String smsFrom = Constants.SYSTEM_TELEPHONE;

				String smsMessage = "[저작권찾기] " + idntName + " - 보상금신청 처리완료 되었습니다.";

				if( commandMap.get( "U_SMS_RECE_YSNO" ).equals( "Y" ) && smsTo != null && smsTo != "" ){

					SMSManager smsManager = new SMSManager();

					System.out.println( "return = " + smsManager.sendSMS( smsTo, smsFrom, smsMessage ) );
				}

			} // end email, sms

			// 상태처리 후 상태값 구한다.
			List dealStat = (List) fdcrAd35Dao.inmtDealStat( commandMap );

			commandMap.put( "ds_dealStat", dealStat );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}
}
