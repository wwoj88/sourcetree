package kr.or.copyright.mls.inmtPrps.model;

import kr.or.copyright.mls.common.BaseObject;

public class InmtPrps extends BaseObject {

	private String CHK;
	private long INMT_SEQN;
	private String SDSR_NAME;
	private String SDSR_SNAME;
	private String MUCI_NAME;
	private String YYMM;
	private String DIVS;
	private String PRPS_DIVS;
	private String KAPP;
	private String FOKAPO;
	private String KRTRA;
	private String OFER_ETPR;
	private String BRCT_CONT;
	private String LYRI_WRTR;
	private String COMS_WRTR;
	private String ARRG_WRTR;
	private String ALBM_NAME;
	private String DUES_CODE;
	private String DATA_TYPE;
	private String USEX_TYPE;
	private String SELG_YSNO;
	private String USEX_LIBR;
	private String OUPT_PAGE;
	private String CTRL_NUMB;
	private String LISH_COMP;
	private String WORK_CODE;
	private String CARY_DIVS;
	private String SCHL;
	private String BOOK_DIVS;
	private String BOOK_SIZE_DIVS;
	private String SCHL_YEAR_DIVS;
	private String PUBC_CONT;
	private String AUTR_DIVS;
	private String WORK_KIND;
	private String USEX_PAGE;
	private String WORK_DIVS;
	private String SUBJ_NAME;
	
	private String USEX_YEAR;
	private String DEAL_STAT_FLAG;
	private String ROW_NUM;
	

	private String[] keyId;
	
	private String PRPS_DOBL_CODE;		// 동시신청 코드
	private String PRPS_DOBL_KEY;
	private String RGHT_PRPS_MAST_KEY;	// 저작권찾기 마스터 키값
	private String INMT_PRPS_MAST_KEY;
	
	
	//조회조건
	private String srchDIVS;
	private String srchSdsrName;
	private String srchMuciName;
	private String srchYymm;
	private String srchAlbmName;

	//페이징
	private int nowPage;  
	private int startRow; 
	private int endRow; 
	private int totalRow;
	
	//기본정보
	private String TRST_203;
	private String TRST_202;
	private String TRST_205;
	private String TRST_205_2;
	
	private String PRPS_SEQN;
	private String PRPS_IDNT;
	private String PRPS_IDNT_CODE;
	private String HOME_TELX_NUMB;
	private String BUSI_TELX_NUMB;
	private String MOBL_PHON;
	private String FAXX_NUMB;
	private String MAIL;
	private String HOME_ADDR;
	private String BUSI_ADDR;
	private String BANK_NAME;
	private String ACCT_NUMB;
	private String DPTR;
	private String PRPS_DESC;
	private String PRPS_IDNT_NAME;
	private String ALLT_YSNO;
	private String USER_IDNT;
	private String PRPS_NAME;  // 성명 
	private String RESD_CORP_NUMB;
	private String RESD_CORP_NUMB_VIEW;  // 주민번호,사업자번호
	private String RESD_NUMB_STR;
	private String RESD_NUMB;
	private String CORP_NUMB;
	private String RGST_DTTM;
	private String RSGT_DTTM_STR;
	private String U_MAIL_RECE_YSNO;
	private String U_SMS_RECE_YSNO;
	private String U_MAIL;
	private String U_MOBL_PHON;
	private String INMT_USER_YSNO;
	
	private String DEAL_STAT_NAME;
	
	private String PRPS_CNT;
	
	// 음제협
	private String PRPS_MAST_KEY;
	private String ORGN_TRST_CODE;
	private String KAPP_PRPS_SEQN;
	private String DISK_NAME;
	private String CD_CODE;
	private String SONG_NAME;
	private String SNER_NAME;
	private String DATE_ISSU_STR;
	private String DATE_ISSU;
	
	private String NATN_NAME;
	private String RGHT_GRND;
	private String MUSC_GNRE;
	private String MUSC_GNRE_STR;
	private String ATTC_YSNO;
	private String OFFX_LINE_RECP;
	private String DEAL_STAT;
	private String RSLT_DESC;
	private String RGST_IDNT;
	
	// 음실연
	private String FOKAPO_PRPS_SEQN;
	private String PEMR_RLNM;
	private String PEMR_STNM;
	private String GRUP_NAME;
	private String PDTN_NAME;
	private String ABUM_NAME;

	private String ABUM_DATE_ISSU_STR;
	private String ABUM_DATE_ISSU;
	private String PEMS_KIND;
	private String PEMS_DESC;
	private String CTBT_RATE;
	private String UPDT_IDNT_NAME;
	private String UPDT_DTTM;
	
	//복전협
	private String COPT_KIND;
	private String WORK_NAME;
	private String WRTR_NAME;
	private String BOOK_CNCN;
	private String SCYR;
	private String SCTR;
	private String SJET;
	
	private String RTPS_RLNM;
	private String RTPS_CRNM;
	private String RTPS_STNM;
	
	private String ATTC_SEQN;
	private String FILE_NAME;
	private String FILE_PATH;
	private String FILE_SIZE;
	private String REAL_FILE_NAME;
	private String TRST_ORGN_CODE;
	
	
	/*20121002 정병호 추가*/
	private String WORKS_ID;
	private String MGNT_INMT_SEQN;
	
	/*20151130 수업목적 추가*/
	private String USEX_SITE;
	
	/*20180716 추가*/
	private String ALLT_AMNT;
	private String UNDIVIDED_AMNT;
	private String ADIVIDEND_AMNT;
	private String MAIN_PERFORMERS_STATE;
	private String SUB_PERFORMERS_STATE;
	private String CONDUCTOR_STATE;
	
	/*20180831추가*/
	private String LATTER_AMNT;
	
	/*20181127*/
	private String SORT;
	
	
	public String getSORT(){
		return SORT;
	}

	public void setSORT( String sORT ){
		SORT = sORT;
	}

	public String getLATTER_AMNT(){
		return LATTER_AMNT;
	}
	
	public void setLATTER_AMNT( String lATTER_AMNT ){
		LATTER_AMNT = lATTER_AMNT;
	}

	public String getALLT_AMNT(){
		return ALLT_AMNT;
	}
	
	public void setALLT_AMNT( String aLLT_AMNT ){
		ALLT_AMNT = aLLT_AMNT;
	}
	
	public String getUNDIVIDED_AMNT(){
		return UNDIVIDED_AMNT;
	}
	
	public void setUNDIVIDED_AMNT( String uNDIVIDED_AMNT ){
		UNDIVIDED_AMNT = uNDIVIDED_AMNT;
	}
	
	public String getADIVIDEND_AMNT(){
		return ADIVIDEND_AMNT;
	}
	
	public void setADIVIDEND_AMNT( String aDIVIDEND_AMNT ){
		ADIVIDEND_AMNT = aDIVIDEND_AMNT;
	}
	
	public String getMAIN_PERFORMERS_STATE(){
		return MAIN_PERFORMERS_STATE;
	}
	
	public void setMAIN_PERFORMERS_STATE( String mAIN_PERFORMERS_STATE ){
		MAIN_PERFORMERS_STATE = mAIN_PERFORMERS_STATE;
	}
	
	public String getSUB_PERFORMERS_STATE(){
		return SUB_PERFORMERS_STATE;
	}
	
	public void setSUB_PERFORMERS_STATE( String sUB_PERFORMERS_STATE ){
		SUB_PERFORMERS_STATE = sUB_PERFORMERS_STATE;
	}
	
	public String getCONDUCTOR_STATE(){
		return CONDUCTOR_STATE;
	}
	
	public void setCONDUCTOR_STATE( String cONDUCTOR_STATE ){
		CONDUCTOR_STATE = cONDUCTOR_STATE;
	}
	public String getCHK() {
		return CHK;
	}
	public String getWORKS_ID() {
	    return WORKS_ID;
	}
	public void setWORKS_ID(String wORKS_ID) {
	    WORKS_ID = wORKS_ID;
	}
	public String getMGNT_INMT_SEQN() {
	    return MGNT_INMT_SEQN;
	}
	public void setMGNT_INMT_SEQN(String mGNT_INMT_SEQN) {
	    MGNT_INMT_SEQN = mGNT_INMT_SEQN;
	}
	public void setCHK(String cHK) {
		CHK = cHK;
	}
	public long getINMT_SEQN() {
		return INMT_SEQN;
	}
	public void setINMT_SEQN(long iNMTSEQN) {
		INMT_SEQN = iNMTSEQN;
	}
	public String getSDSR_NAME() {
		return SDSR_NAME;
	}
	public void setSDSR_NAME(String sDSRNAME) {
		SDSR_NAME = sDSRNAME;
	}
	public String getMUCI_NAME() {
		return MUCI_NAME;
	}
	public void setMUCI_NAME(String mUCINAME) {
		MUCI_NAME = mUCINAME;
	}
	public String getYYMM() {
		return YYMM;
	}
	public void setYYMM(String yYMM) {
		YYMM = yYMM;
	}
	public String getDIVS() {
		return DIVS;
	}
	public void setDIVS(String dIVS) {
		DIVS = dIVS;
	}
	public String getPRPS_DIVS() {
		return PRPS_DIVS;
	}
	public void setPRPS_DIVS(String pRPSDIVS) {
		PRPS_DIVS = pRPSDIVS;
	}
	public String getKAPP() {
		return KAPP;
	}
	public void setKAPP(String kAPP) {
		KAPP = kAPP;
	}
	public String getFOKAPO() {
		return FOKAPO;
	}
	public void setFOKAPO(String fOKAPO) {
		FOKAPO = fOKAPO;
	}
	public String getKRTRA() {
		return KRTRA;
	}
	public void setKRTRA(String kRTRA) {
		KRTRA = kRTRA;
	}
	public String getOFER_ETPR() {
		return OFER_ETPR;
	}
	public void setOFER_ETPR(String oFERETPR) {
		OFER_ETPR = oFERETPR;
	}
	public String getBRCT_CONT() {
		return BRCT_CONT;
	}
	public void setBRCT_CONT(String bRCTCONT) {
		BRCT_CONT = bRCTCONT;
	}
	public String getLYRI_WRTR() {
		return LYRI_WRTR;
	}
	public void setLYRI_WRTR(String lYRIWRTR) {
		LYRI_WRTR = lYRIWRTR;
	}
	public String getCOMS_WRTR() {
		return COMS_WRTR;
	}
	public void setCOMS_WRTR(String cOMSWRTR) {
		COMS_WRTR = cOMSWRTR;
	}
	public String getARRG_WRTR() {
		return ARRG_WRTR;
	}
	public void setARRG_WRTR(String aRRGWRTR) {
		ARRG_WRTR = aRRGWRTR;
	}
	public String getALBM_NAME() {
		return ALBM_NAME;
	}
	public void setALBM_NAME(String aLBMNAME) {
		ALBM_NAME = aLBMNAME;
	}
	public String getDUES_CODE() {
		return DUES_CODE;
	}
	public void setDUES_CODE(String dUESCODE) {
		DUES_CODE = dUESCODE;
	}
	public String getDATA_TYPE() {
		return DATA_TYPE;
	}
	public void setDATA_TYPE(String dATATYPE) {
		DATA_TYPE = dATATYPE;
	}
	public String getUSEX_TYPE() {
		return USEX_TYPE;
	}
	public void setUSEX_TYPE(String uSEXTYPE) {
		USEX_TYPE = uSEXTYPE;
	}
	public String getSELG_YSNO() {
		return SELG_YSNO;
	}
	public void setSELG_YSNO(String sELGYSNO) {
		SELG_YSNO = sELGYSNO;
	}
	public String getUSEX_LIBR() {
		return USEX_LIBR;
	}
	public void setUSEX_LIBR(String uSEXLIBR) {
		USEX_LIBR = uSEXLIBR;
	}
	public String getOUPT_PAGE() {
		return OUPT_PAGE;
	}
	public void setOUPT_PAGE(String oUPTPAGE) {
		OUPT_PAGE = oUPTPAGE;
	}
	public String getCTRL_NUMB() {
		return CTRL_NUMB;
	}
	public void setCTRL_NUMB(String cTRLNUMB) {
		CTRL_NUMB = cTRLNUMB;
	}
	public String getLISH_COMP() {
		return LISH_COMP;
	}
	public void setLISH_COMP(String lISHCOMP) {
		LISH_COMP = lISHCOMP;
	}
	public String getWORK_CODE() {
		return WORK_CODE;
	}
	public void setWORK_CODE(String wORKCODE) {
		WORK_CODE = wORKCODE;
	}
	public String getCARY_DIVS() {
		return CARY_DIVS;
	}
	public void setCARY_DIVS(String cARYDIVS) {
		CARY_DIVS = cARYDIVS;
	}
	public String getSCHL() {
		return SCHL;
	}
	public void setSCHL(String sCHL) {
		SCHL = sCHL;
	}
	public String getBOOK_DIVS() {
		return BOOK_DIVS;
	}
	public void setBOOK_DIVS(String bOOKDIVS) {
		BOOK_DIVS = bOOKDIVS;
	}
	public String getBOOK_SIZE_DIVS() {
		return BOOK_SIZE_DIVS;
	}
	public void setBOOK_SIZE_DIVS(String bOOKSIZEDIVS) {
		BOOK_SIZE_DIVS = bOOKSIZEDIVS;
	}
	public String getSCHL_YEAR_DIVS() {
		return SCHL_YEAR_DIVS;
	}
	public void setSCHL_YEAR_DIVS(String sCHLYEARDIVS) {
		SCHL_YEAR_DIVS = sCHLYEARDIVS;
	}
	public String getPUBC_CONT() {
		return PUBC_CONT;
	}
	public void setPUBC_CONT(String pUBCCONT) {
		PUBC_CONT = pUBCCONT;
	}
	public String getAUTR_DIVS() {
		return AUTR_DIVS;
	}
	public void setAUTR_DIVS(String aUTRDIVS) {
		AUTR_DIVS = aUTRDIVS;
	}
	public String getWORK_KIND() {
		return WORK_KIND;
	}
	public void setWORK_KIND(String wORKKIND) {
		WORK_KIND = wORKKIND;
	}
	public String getUSEX_PAGE() {
		return USEX_PAGE;
	}
	public void setUSEX_PAGE(String uSEXPAGE) {
		USEX_PAGE = uSEXPAGE;
	}
	public String getWORK_DIVS() {
		return WORK_DIVS;
	}
	public void setWORK_DIVS(String wORKDIVS) {
		WORK_DIVS = wORKDIVS;
	}
	public String getSUBJ_NAME() {
		return SUBJ_NAME;
	}
	public void setSUBJ_NAME(String sUBJNAME) {
		SUBJ_NAME = sUBJNAME;
	}
	public String[] getKeyId() {
		return keyId;
	}
	public void setKeyId(String[] keyId) {
		this.keyId = keyId;
	}
	public String getSrchDIVS() {
		return srchDIVS;
	}
	public void setSrchDIVS(String srchDIVS) {
		this.srchDIVS = srchDIVS;
	}
	public String getSrchSdsrName() {
		return srchSdsrName;
	}
	public void setSrchSdsrName(String srchSdsrName) {
		this.srchSdsrName = srchSdsrName;
	}
	public String getSrchMuciName() {
		return srchMuciName;
	}
	public void setSrchMuciName(String srchMuciName) {
		this.srchMuciName = srchMuciName;
	}
	public String getSrchYymm() {
		return srchYymm;
	}
	public void setSrchYymm(String srchYymm) {
		this.srchYymm = srchYymm;
	}
	public int getNowPage() {
		return nowPage;
	}
	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
	public int getTotalRow() {
		return totalRow;
	}
	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}
	public String getTRST_203() {
		return TRST_203;
	}
	public void setTRST_203(String tRST_203) {
		TRST_203 = tRST_203;
	}
	public String getTRST_202() {
		return TRST_202;
	}
	public void setTRST_202(String tRST_202) {
		TRST_202 = tRST_202;
	}
	public String getTRST_205() {
		return TRST_205;
	}
	public void setTRST_205(String tRST_205) {
		TRST_205 = tRST_205;
	}
	public String getTRST_205_2() {
		return TRST_205_2;
	}
	public void setTRST_205_2(String tRST_205_2) {
		TRST_205_2 = tRST_205_2;
	}
	public String getPRPS_SEQN() {
		return PRPS_SEQN;
	}
	public void setPRPS_SEQN(String pRPSSEQN) {
		PRPS_SEQN = pRPSSEQN;
	}
	public String getPRPS_IDNT() {
		return PRPS_IDNT;
	}
	public void setPRPS_IDNT(String pRPSIDNT) {
		PRPS_IDNT = pRPSIDNT;
	}
	public String getHOME_TELX_NUMB() {
		return HOME_TELX_NUMB;
	}
	public void setHOME_TELX_NUMB(String hOMETELXNUMB) {
		HOME_TELX_NUMB = hOMETELXNUMB;
	}
	public String getBUSI_TELX_NUMB() {
		return BUSI_TELX_NUMB;
	}
	public void setBUSI_TELX_NUMB(String bUSITELXNUMB) {
		BUSI_TELX_NUMB = bUSITELXNUMB;
	}
	public String getMOBL_PHON() {
		return MOBL_PHON;
	}
	public void setMOBL_PHON(String mOBLPHON) {
		MOBL_PHON = mOBLPHON;
	}
	public String getFAXX_NUMB() {
		return FAXX_NUMB;
	}
	public void setFAXX_NUMB(String fAXXNUMB) {
		FAXX_NUMB = fAXXNUMB;
	}
	public String getMAIL() {
		return MAIL;
	}
	public void setMAIL(String mAIL) {
		MAIL = mAIL;
	}
	public String getHOME_ADDR() {
		return HOME_ADDR;
	}
	public void setHOME_ADDR(String hOMEADDR) {
		HOME_ADDR = hOMEADDR;
	}
	public String getBUSI_ADDR() {
		return BUSI_ADDR;
	}
	public void setBUSI_ADDR(String bUSIADDR) {
		BUSI_ADDR = bUSIADDR;
	}
	public String getBANK_NAME() {
		return BANK_NAME;
	}
	public void setBANK_NAME(String bANKNAME) {
		BANK_NAME = bANKNAME;
	}
	public String getACCT_NUMB() {
		return ACCT_NUMB;
	}
	public void setACCT_NUMB(String aCCTNUMB) {
		ACCT_NUMB = aCCTNUMB;
	}
	public String getDPTR() {
		return DPTR;
	}
	public void setDPTR(String dPTR) {
		DPTR = dPTR;
	}
	public String getPRPS_DESC() {
		return PRPS_DESC;
	}
	public void setPRPS_DESC(String pRPSDESC) {
		PRPS_DESC = pRPSDESC;
	}
	public String getPRPS_IDNT_NAME() {
		return PRPS_IDNT_NAME;
	}
	public void setPRPS_IDNT_NAME(String pRPSIDNTNAME) {
		PRPS_IDNT_NAME = pRPSIDNTNAME;
	}
	public String getALLT_YSNO() {
		return ALLT_YSNO;
	}
	public void setALLT_YSNO(String aLLTYSNO) {
		ALLT_YSNO = aLLTYSNO;
	}
	public String getUSER_IDNT() {
		return USER_IDNT;
	}
	public void setUSER_IDNT(String uSERIDNT) {
		USER_IDNT = uSERIDNT;
	}
	public String getPRPS_NAME() {
		return PRPS_NAME;
	}
	public void setPRPS_NAME(String pRPSNAME) {
		PRPS_NAME = pRPSNAME;
	}
	public String getRESD_CORP_NUMB() {
		return RESD_CORP_NUMB;
	}
	public void setRESD_CORP_NUMB(String rESDCORPNUMB) {
		RESD_CORP_NUMB = rESDCORPNUMB;
	}
	public String getRESD_CORP_NUMB_VIEW() {
		return RESD_CORP_NUMB_VIEW;
	}
	public void setRESD_CORP_NUMB_VIEW(String rESDCORPNUMBVIEW) {
		RESD_CORP_NUMB_VIEW = rESDCORPNUMBVIEW;
	}
	public String getRESD_NUMB_STR() {
		return RESD_NUMB_STR;
	}
	public void setRESD_NUMB_STR(String rESDNUMBSTR) {
		RESD_NUMB_STR = rESDNUMBSTR;
	}
	public String getRESD_NUMB() {
		return RESD_NUMB;
	}
	public void setRESD_NUMB(String rESDNUMB) {
		RESD_NUMB = rESDNUMB;
	}
	public String getCORP_NUMB() {
		return CORP_NUMB;
	}
	public void setCORP_NUMB(String cORPNUMB) {
		CORP_NUMB = cORPNUMB;
	}
	public String getRGST_DTTM() {
		return RGST_DTTM;
	}
	public void setRGST_DTTM(String rGSTDTTM) {
		RGST_DTTM = rGSTDTTM;
	}
	public String getRSGT_DTTM_STR() {
		return RSGT_DTTM_STR;
	}
	public void setRSGT_DTTM_STR(String rSGTDTTMSTR) {
		RSGT_DTTM_STR = rSGTDTTMSTR;
	}
	public String getU_MAIL_RECE_YSNO() {
		return U_MAIL_RECE_YSNO;
	}
	public void setU_MAIL_RECE_YSNO(String uMAILRECEYSNO) {
		U_MAIL_RECE_YSNO = uMAILRECEYSNO;
	}
	public String getU_SMS_RECE_YSNO() {
		return U_SMS_RECE_YSNO;
	}
	public void setU_SMS_RECE_YSNO(String uSMSRECEYSNO) {
		U_SMS_RECE_YSNO = uSMSRECEYSNO;
	}
	public String getU_MAIL() {
		return U_MAIL;
	}
	public void setU_MAIL(String uMAIL) {
		U_MAIL = uMAIL;
	}
	public String getU_MOBL_PHON() {
		return U_MOBL_PHON;
	}
	public void setU_MOBL_PHON(String uMOBLPHON) {
		U_MOBL_PHON = uMOBLPHON;
	}
	public String getINMT_USER_YSNO() {
		return INMT_USER_YSNO;
	}
	public void setINMT_USER_YSNO(String iNMTUSERYSNO) {
		INMT_USER_YSNO = iNMTUSERYSNO;
	}
	public String getDEAL_STAT_NAME() {
		return DEAL_STAT_NAME;
	}
	public void setDEAL_STAT_NAME(String dEALSTATNAME) {
		DEAL_STAT_NAME = dEALSTATNAME;
	}
	public String getPRPS_MAST_KEY() {
		return PRPS_MAST_KEY;
	}
	public void setPRPS_MAST_KEY(String pRPSMASTKEY) {
		PRPS_MAST_KEY = pRPSMASTKEY;
	}
	public String getORGN_TRST_CODE() {
		return ORGN_TRST_CODE;
	}
	public void setORGN_TRST_CODE(String oRGNTRSTCODE) {
		ORGN_TRST_CODE = oRGNTRSTCODE;
	}
	public String getKAPP_PRPS_SEQN() {
		return KAPP_PRPS_SEQN;
	}
	public void setKAPP_PRPS_SEQN(String kAPPPRPSSEQN) {
		KAPP_PRPS_SEQN = kAPPPRPSSEQN;
	}
	public String getDISK_NAME() {
		return DISK_NAME;
	}
	public void setDISK_NAME(String dISKNAME) {
		DISK_NAME = dISKNAME;
	}
	public String getCD_CODE() {
		return CD_CODE;
	}
	public void setCD_CODE(String cDCODE) {
		CD_CODE = cDCODE;
	}
	public String getSONG_NAME() {
		return SONG_NAME;
	}
	public void setSONG_NAME(String sONGNAME) {
		SONG_NAME = sONGNAME;
	}
	public String getSNER_NAME() {
		return SNER_NAME;
	}
	public void setSNER_NAME(String sNERNAME) {
		SNER_NAME = sNERNAME;
	}
	public String getDATE_ISSU_STR() {
		return DATE_ISSU_STR;
	}
	public void setDATE_ISSU_STR(String dATEISSUSTR) {
		DATE_ISSU_STR = dATEISSUSTR;
	}
	public String getDATE_ISSU() {
		return DATE_ISSU;
	}
	public void setDATE_ISSU(String dATEISSU) {
		DATE_ISSU = dATEISSU;
	}
	public String getNATN_NAME() {
		return NATN_NAME;
	}
	public void setNATN_NAME(String nATNNAME) {
		NATN_NAME = nATNNAME;
	}
	public String getRGHT_GRND() {
		return RGHT_GRND;
	}
	public void setRGHT_GRND(String rGHTGRND) {
		RGHT_GRND = rGHTGRND;
	}
	public String getMUSC_GNRE() {
		return MUSC_GNRE;
	}
	public void setMUSC_GNRE(String mUSCGNRE) {
		MUSC_GNRE = mUSCGNRE;
	}
	public String getMUSC_GNRE_STR() {
		return MUSC_GNRE_STR;
	}
	public void setMUSC_GNRE_STR(String mUSCGNRESTR) {
		MUSC_GNRE_STR = mUSCGNRESTR;
	}
	public String getATTC_YSNO() {
		return ATTC_YSNO;
	}
	public void setATTC_YSNO(String aTTCYSNO) {
		ATTC_YSNO = aTTCYSNO;
	}
	public String getOFFX_LINE_RECP() {
		return OFFX_LINE_RECP;
	}
	public void setOFFX_LINE_RECP(String oFFXLINERECP) {
		OFFX_LINE_RECP = oFFXLINERECP;
	}
	public String getDEAL_STAT() {
		return DEAL_STAT;
	}
	public void setDEAL_STAT(String dEALSTAT) {
		DEAL_STAT = dEALSTAT;
	}
	public String getRSLT_DESC() {
		return RSLT_DESC;
	}
	public void setRSLT_DESC(String rSLTDESC) {
		RSLT_DESC = rSLTDESC;
	}
	public String getRGST_IDNT() {
		return RGST_IDNT;
	}
	public void setRGST_IDNT(String rGSTIDNT) {
		RGST_IDNT = rGSTIDNT;
	}
	public String getFOKAPO_PRPS_SEQN() {
		return FOKAPO_PRPS_SEQN;
	}
	public void setFOKAPO_PRPS_SEQN(String fOKAPOPRPSSEQN) {
		FOKAPO_PRPS_SEQN = fOKAPOPRPSSEQN;
	}
	public String getPEMR_RLNM() {
		return PEMR_RLNM;
	}
	public void setPEMR_RLNM(String pEMRRLNM) {
		PEMR_RLNM = pEMRRLNM;
	}
	public String getPEMR_STNM() {
		return PEMR_STNM;
	}
	public void setPEMR_STNM(String pEMRSTNM) {
		PEMR_STNM = pEMRSTNM;
	}
	public String getGRUP_NAME() {
		return GRUP_NAME;
	}
	public void setGRUP_NAME(String gRUPNAME) {
		GRUP_NAME = gRUPNAME;
	}
	public String getPDTN_NAME() {
		return PDTN_NAME;
	}
	public void setPDTN_NAME(String pDTNNAME) {
		PDTN_NAME = pDTNNAME;
	}
	public String getABUM_NAME() {
		return ABUM_NAME;
	}
	public void setABUM_NAME(String aBUMNAME) {
		ABUM_NAME = aBUMNAME;
	}
	public String getABUM_DATE_ISSU_STR() {
		return ABUM_DATE_ISSU_STR;
	}
	public void setABUM_DATE_ISSU_STR(String aBUMDATEISSUSTR) {
		ABUM_DATE_ISSU_STR = aBUMDATEISSUSTR;
	}
	public String getABUM_DATE_ISSU() {
		return ABUM_DATE_ISSU;
	}
	public void setABUM_DATE_ISSU(String aBUMDATEISSU) {
		ABUM_DATE_ISSU = aBUMDATEISSU;
	}
	public String getPEMS_KIND() {
		return PEMS_KIND;
	}
	public void setPEMS_KIND(String pEMSKIND) {
		PEMS_KIND = pEMSKIND;
	}
	public String getPEMS_DESC() {
		return PEMS_DESC;
	}
	public void setPEMS_DESC(String pEMSDESC) {
		PEMS_DESC = pEMSDESC;
	}
	public String getCTBT_RATE() {
		return CTBT_RATE;
	}
	public void setCTBT_RATE(String cTBTRATE) {
		CTBT_RATE = cTBTRATE;
	}
	public String getUPDT_IDNT_NAME() {
		return UPDT_IDNT_NAME;
	}
	public void setUPDT_IDNT_NAME(String uPDTIDNTNAME) {
		UPDT_IDNT_NAME = uPDTIDNTNAME;
	}
	public String getUPDT_DTTM() {
		return UPDT_DTTM;
	}
	public void setUPDT_DTTM(String uPDTDTTM) {
		UPDT_DTTM = uPDTDTTM;
	}
	public String getCOPT_KIND() {
		return COPT_KIND;
	}
	public void setCOPT_KIND(String cOPTKIND) {
		COPT_KIND = cOPTKIND;
	}
	public String getWORK_NAME() {
		return WORK_NAME;
	}
	public void setWORK_NAME(String wORKNAME) {
		WORK_NAME = wORKNAME;
	}
	public String getWRTR_NAME() {
		return WRTR_NAME;
	}
	public void setWRTR_NAME(String wRTRNAME) {
		WRTR_NAME = wRTRNAME;
	}
	public String getBOOK_CNCN() {
		return BOOK_CNCN;
	}
	public void setBOOK_CNCN(String bOOKCNCN) {
		BOOK_CNCN = bOOKCNCN;
	}
	public String getSCYR() {
		return SCYR;
	}
	public void setSCYR(String sCYR) {
		SCYR = sCYR;
	}
	public String getSCTR() {
		return SCTR;
	}
	public void setSCTR(String sCTR) {
		SCTR = sCTR;
	}
	public String getSJET() {
		return SJET;
	}
	public void setSJET(String sJET) {
		SJET = sJET;
	}
	public String getRTPS_RLNM() {
		return RTPS_RLNM;
	}
	public void setRTPS_RLNM(String rTPSRLNM) {
		RTPS_RLNM = rTPSRLNM;
	}
	public String getRTPS_CRNM() {
		return RTPS_CRNM;
	}
	public void setRTPS_CRNM(String rTPSCRNM) {
		RTPS_CRNM = rTPSCRNM;
	}
	public String getRTPS_STNM() {
		return RTPS_STNM;
	}
	public void setRTPS_STNM(String rTPSSTNM) {
		RTPS_STNM = rTPSSTNM;
	}
	public String getATTC_SEQN() {
		return ATTC_SEQN;
	}
	public void setATTC_SEQN(String aTTCSEQN) {
		ATTC_SEQN = aTTCSEQN;
	}
	public String getFILE_NAME() {
		return FILE_NAME;
	}
	public void setFILE_NAME(String fILENAME) {
		FILE_NAME = fILENAME;
	}
	public String getFILE_PATH() {
		return FILE_PATH;
	}
	public void setFILE_PATH(String fILEPATH) {
		FILE_PATH = fILEPATH;
	}
	public String getFILE_SIZE() {
		return FILE_SIZE;
	}
	public void setFILE_SIZE(String fILESIZE) {
		FILE_SIZE = fILESIZE;
	}
	public String getREAL_FILE_NAME() {
		return REAL_FILE_NAME;
	}
	public void setREAL_FILE_NAME(String rEALFILENAME) {
		REAL_FILE_NAME = rEALFILENAME;
	}
	public String getTRST_ORGN_CODE() {
		return TRST_ORGN_CODE;
	}
	public void setTRST_ORGN_CODE(String tRSTORGNCODE) {
		TRST_ORGN_CODE = tRSTORGNCODE;
	}
	public String getPRPS_CNT() {
		return PRPS_CNT;
	}
	public void setPRPS_CNT(String prps_cnt) {
		PRPS_CNT = prps_cnt;
	}
	public String getPRPS_DOBL_CODE() {
		return PRPS_DOBL_CODE;
	}
	public void setPRPS_DOBL_CODE(String prps_dobl_code) {
		PRPS_DOBL_CODE = prps_dobl_code;
	}
	public String getRGHT_PRPS_MAST_KEY() {
		return RGHT_PRPS_MAST_KEY;
	}
	public void setRGHT_PRPS_MAST_KEY(String rght_prps_mast_key) {
		RGHT_PRPS_MAST_KEY = rght_prps_mast_key;
	}
	public String getPRPS_IDNT_CODE() {
		return PRPS_IDNT_CODE;
	}
	public void setPRPS_IDNT_CODE(String prps_idnt_code) {
		PRPS_IDNT_CODE = prps_idnt_code;
	}
	public String getINMT_PRPS_MAST_KEY() {
		return INMT_PRPS_MAST_KEY;
	}
	public void setINMT_PRPS_MAST_KEY(String inmt_prps_mast_key) {
		INMT_PRPS_MAST_KEY = inmt_prps_mast_key;
	}
	public String getPRPS_DOBL_KEY() {
		return PRPS_DOBL_KEY;
	}
	public void setPRPS_DOBL_KEY(String prps_dobl_key) {
		PRPS_DOBL_KEY = prps_dobl_key;
	}
	public String getSrchAlbmName() {
		return srchAlbmName;
	}
	public void setSrchAlbmName(String srchAlbmName) {
		this.srchAlbmName = srchAlbmName;
	}
	public String getUSEX_YEAR() {
		return USEX_YEAR;
	}
	public void setUSEX_YEAR(String usex_year) {
		USEX_YEAR = usex_year;
	}
	public String getSDSR_SNAME() {
		return SDSR_SNAME;
	}
	public void setSDSR_SNAME(String sdsr_sname) {
		SDSR_SNAME = sdsr_sname;
	}
	public String getDEAL_STAT_FLAG() {
		return DEAL_STAT_FLAG;
	}
	public void setDEAL_STAT_FLAG(String deal_stat_flag) {
		DEAL_STAT_FLAG = deal_stat_flag;
	}
	public String getROW_NUM() {
		return ROW_NUM;
	}
	public void setROW_NUM(String rOW_NUM) {
		ROW_NUM = rOW_NUM;
	}
	public String getUSEX_SITE() {
		return USEX_SITE;
	}
	public void setUSEX_SITE(String usex_year) {
		USEX_SITE = usex_year;
	}
}
