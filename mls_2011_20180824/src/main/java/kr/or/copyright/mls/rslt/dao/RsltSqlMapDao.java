package kr.or.copyright.mls.rslt.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class RsltSqlMapDao extends SqlMapClientDaoSupport implements RsltDao {
	
	public List prpsRsltList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rslt.prpsRsltList",map);
	}	
	
	public List prpsRsltListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Rslt.prpsRsltListCount",map);
	}

	public List inmtPrpsRsltList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rslt.inmtPrpsRsltList" , map);
	}	
	
	public void inmtPrpsDelete(Map map) {
		getSqlMapClientTemplate().delete("Rslt.inmtPrpsDelete", map);
	}
	
	public void inmtPrpsRsltDelete(Map map) {
		getSqlMapClientTemplate().delete("Rslt.inmtPrpsRsltDelete", map);
	}
	
	public void inmtKappDelete(Map map) {
		getSqlMapClientTemplate().delete("Rslt.inmtKappDelete", map);
	}
	
	public void inmtFokapoDelete(Map map) {
		getSqlMapClientTemplate().delete("Rslt.inmtFokapoDelete", map);
	}
	
	public void inmtKrtraDelete(Map map) {
		getSqlMapClientTemplate().delete("Rslt.inmtKrtraDelete", map);
	}
	
	
	public List prpsFileList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rslt.prpsFileList", map);
	}
	
	public void prpsFileInsert(Map map) {
		getSqlMapClientTemplate().insert("Rslt.prpsFileInsert", map);
	}
	
	public void prpsFileDelete(Map map) {
		getSqlMapClientTemplate().delete("Rslt.prpsFileDelete", map);
	}
	
	public String getMaxDealStat(Map map) {
		String DealStat = (String)getSqlMapClientTemplate().queryForObject("Rslt.getMaxDealStat",map);
		return DealStat; 
	}
	
	// 강명표 추가
	public void insertConnInfo(Map map) {
		getSqlMapClientTemplate().insert("Rslt.insertConnInfo", map);
	}
}
