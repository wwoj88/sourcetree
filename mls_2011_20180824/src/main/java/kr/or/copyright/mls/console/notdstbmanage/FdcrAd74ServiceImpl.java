package kr.or.copyright.mls.console.notdstbmanage;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.notdstbmanage.inter.FdcrAd71Dao;
import kr.or.copyright.mls.console.notdstbmanage.inter.FdcrAd74Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd74Service" )
public class FdcrAd74ServiceImpl extends CommandService implements FdcrAd74Service{

	@Resource( name = "fdcrAd71Dao" )
	private FdcrAd71Dao fdcrAd71Dao;

	/**
	 * 미분배보상금 관리 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd74List1( Map<String, Object> commandMap ) throws Exception{
		/*
		String curYear = (String) commandMap.get( "END_YEAR" );
		String maxYear = fdcrAd71Dao.alltInmtMgntMaxYear();

		if( !curYear.equals( maxYear ) ){
			for( int i = 0; i < 2; i++ ){
				HashMap insertMap = new HashMap();
				insertMap.put( "INMT_YEAR", curYear );
				insertMap.put( "HALF", ( i + 1 ) + "" );
				for( int j = 0; j < 8; j++ ){
					if( j == 0 ){
						insertMap.put( "INMT_DIVS", "1" );
						insertMap.put( "INMT_ITEMS", "1" );
						fdcrAd71Dao.alltInmtMgntInsert( insertMap );
					}else if( j == 1 ){
						insertMap.put( "INMT_DIVS", "1" );
						insertMap.put( "INMT_ITEMS", "2" );
						fdcrAd71Dao.alltInmtMgntInsert( insertMap );
					}else if( j == 2 ){
						insertMap.put( "INMT_DIVS", "1" );
						insertMap.put( "INMT_ITEMS", "3" );
						fdcrAd71Dao.alltInmtMgntInsert( insertMap );
					}else if( j == 3 ){
						insertMap.put( "INMT_DIVS", "2" );
						insertMap.put( "INMT_ITEMS", "1" );
						fdcrAd71Dao.alltInmtMgntInsert( insertMap );
					}else if( j == 4 ){
						insertMap.put( "INMT_DIVS", "2" );
						insertMap.put( "INMT_ITEMS", "2" );
						fdcrAd71Dao.alltInmtMgntInsert( insertMap );
					}else if( j == 5 ){
						insertMap.put( "INMT_DIVS", "2" );
						insertMap.put( "INMT_ITEMS", "3" );
						fdcrAd71Dao.alltInmtMgntInsert( insertMap );
					}else if( j == 6 ){
						insertMap.put( "INMT_DIVS", "3" );
						insertMap.put( "INMT_ITEMS", "4" );
						fdcrAd71Dao.alltInmtMgntInsert( insertMap );
					}else if( j == 7 ){
						insertMap.put( "INMT_DIVS", "3" );
						insertMap.put( "INMT_ITEMS", "5" );
						fdcrAd71Dao.alltInmtMgntInsert( insertMap );
					}
				}
			}
		}
		*/

		List list = (List) fdcrAd71Dao.alltInmtMgntList( commandMap );
		commandMap.put( "ds_List", list );

	}

	/**
	 * 미분배 보상금 관리 등록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd74Insert1(Map<String, Object> commandMap) throws Exception{
		boolean result = false;
		try{
			/*
			String[] INMT_YEARS = (String[]) commandMap.get( "INMT_YEAR" );
			String[] OPEN_YSNOS = (String[]) commandMap.get( "OPEN_YSNO" );
			String[] HALFS = (String[]) commandMap.get( "HALF" );
			String[] TOT_ALLT_INMTS = (String[]) commandMap.get( "TOT_ALLT_INMT" );
			String[] A1S = (String[]) commandMap.get( "A1" );
			String[] A2S = (String[]) commandMap.get( "A2" );
			String[] A3S = (String[]) commandMap.get( "A3" );
			String[] B1S = (String[]) commandMap.get( "B1" );
			String[] B2S = (String[]) commandMap.get( "B2" );
			String[] B3S = (String[]) commandMap.get( "B3" );
			String[] C1S = (String[]) commandMap.get( "C1" );
			String[] C2S = (String[]) commandMap.get( "C2" );
			*/
			String INMT_YEAR = (String) commandMap.get( "INMT_YEAR" );
			String HALF = (String) commandMap.get( "HALF" );
			String A1 = (String) commandMap.get( "A1" );
			String A2 = (String) commandMap.get( "A2" );
			String A3 = (String) commandMap.get( "A3" );
			String B1 = (String) commandMap.get( "B1" );
			String B2 = (String) commandMap.get( "B2" );
			String B3 = (String) commandMap.get( "B3" );
			String C1 = (String) commandMap.get( "C1" );
			String C2 = (String) commandMap.get( "C2" );
			String TOT_ALLT_INMT = (String) commandMap.get( "TOT_ALLT_INMT" );
			String OPEN_YSNO = (String) commandMap.get( "OPEN_YSNO" );
			
			//for( int i = 0; i < OPEN_YSNOS.length; i++ ){
				Map<String, Object> listMap = new HashMap<String, Object>();
				listMap.put( "OPEN_YSNO", OPEN_YSNO );
				listMap.put( "INMT_YEAR", INMT_YEAR );
				listMap.put( "HALF", HALF );
				listMap.put( "TOT_ALLT_INMT", TOT_ALLT_INMT );
				listMap.put( "A1", A1 );
				listMap.put( "A2", A2 );
				listMap.put( "A3", A3 );
				listMap.put( "B1", B1 );
				listMap.put( "B2", B2 );
				listMap.put( "B3", B3 );
				listMap.put( "C1", C1 );
				listMap.put( "C2", C2 );

				HashMap updateMap = new HashMap();
				updateMap.put( "INMT_YEAR", listMap.get( "INMT_YEAR" ) );
				updateMap.put( "HALF", listMap.get( "HALF" ) );
				if(listMap.get( "OPEN_YSNO" ).equals( "1" ) || listMap.get( "OPEN_YSNO" ) == "1"){
					updateMap.put( "OPEN_YSNO", "Y" );
				}else{
					updateMap.put( "OPEN_YSNO", "N" );
				}
				
				for( int j = 0; j < 8; j++ ){
					if( j == 0 ){
						updateMap.put( "INMT_DIVS", "1" );
						updateMap.put( "INMT_ITEMS", "1" );
						updateMap.put( "ALLT_INMT", listMap.get( "A1" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 1 ){
						updateMap.put( "INMT_DIVS", "1" );
						updateMap.put( "INMT_ITEMS", "2" );
						updateMap.put( "ALLT_INMT", listMap.get( "A2" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 2 ){
						updateMap.put( "INMT_DIVS", "1" );
						updateMap.put( "INMT_ITEMS", "3" );
						updateMap.put( "ALLT_INMT", listMap.get( "A3" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 3 ){
						updateMap.put( "INMT_DIVS", "2" );
						updateMap.put( "INMT_ITEMS", "1" );
						updateMap.put( "ALLT_INMT", listMap.get( "B1" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 4 ){
						updateMap.put( "INMT_DIVS", "2" );
						updateMap.put( "INMT_ITEMS", "2" );
						updateMap.put( "ALLT_INMT", listMap.get( "B2" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 5 ){
						updateMap.put( "INMT_DIVS", "2" );
						updateMap.put( "INMT_ITEMS", "3" );
						updateMap.put( "ALLT_INMT", listMap.get( "B3" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 6 ){
						updateMap.put( "INMT_DIVS", "3" );
						updateMap.put( "INMT_ITEMS", "4" );
						updateMap.put( "ALLT_INMT", listMap.get( "C1" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 7 ){
						updateMap.put( "INMT_DIVS", "3" );
						updateMap.put( "INMT_ITEMS", "5" );
						updateMap.put( "ALLT_INMT", listMap.get( "C2" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}
				}
			//}
			
			/*
			for( int i = 0; i < HALFS.length; i++ ){

				Map<String, Object> listMap = new HashMap<String, Object>();
				listMap.put( "OPEN_YSNO", OPEN_YSNOS[i] );
				listMap.put( "INMT_YEAR", INMT_YEAR );
				listMap.put( "HALF", HALFS[i] );
				listMap.put( "TOT_ALLT_INMT", TOT_ALLT_INMTS[i] );
				listMap.put( "A1", A1S[i] );
				listMap.put( "A2", A2S[i] );
				listMap.put( "A3", A3S[i] );
				listMap.put( "B1", B1S[i] );
				listMap.put( "B2", B2S[i] );
				listMap.put( "B3", B3S[i] );
				listMap.put( "C1", C1S[i] );
				listMap.put( "C2", C2S[i] );

				HashMap updateMap = new HashMap();
				updateMap.put( "INMT_YEAR", listMap.get( "INMT_YEAR" ) );
				updateMap.put( "HALF", listMap.get( "HALF" ) );
				if(listMap.get( "OPEN_YSNO" ).equals( "1" ) || listMap.get( "OPEN_YSNO" ) == "1"){
					updateMap.put( "OPEN_YSNO", "Y" );
				}else{
					updateMap.put( "OPEN_YSNO", "N" );
				}

				//Double test = (Double) listMap.get( "OPEN_YSNO" );

				//String ysno = Double.toString( test );

				
				if( ysno.equals( "1.0" ) ){
					updateMap.put( "OPEN_YSNO", "Y" );
				}else{
					updateMap.put( "OPEN_YSNO", "N" );
				}
				
				
				for( int j = 0; j < 8; j++ ){
					if( j == 0 ){
						updateMap.put( "INMT_DIVS", "1" );
						updateMap.put( "INMT_ITEMS", "1" );
						updateMap.put( "ALLT_INMT", listMap.get( "A1" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 1 ){
						updateMap.put( "INMT_DIVS", "1" );
						updateMap.put( "INMT_ITEMS", "2" );
						updateMap.put( "ALLT_INMT", listMap.get( "A2" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 2 ){
						updateMap.put( "INMT_DIVS", "1" );
						updateMap.put( "INMT_ITEMS", "3" );
						updateMap.put( "ALLT_INMT", listMap.get( "A3" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 3 ){
						updateMap.put( "INMT_DIVS", "2" );
						updateMap.put( "INMT_ITEMS", "1" );
						updateMap.put( "ALLT_INMT", listMap.get( "B1" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 4 ){
						updateMap.put( "INMT_DIVS", "2" );
						updateMap.put( "INMT_ITEMS", "2" );
						updateMap.put( "ALLT_INMT", listMap.get( "B2" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 5 ){
						updateMap.put( "INMT_DIVS", "2" );
						updateMap.put( "INMT_ITEMS", "3" );
						updateMap.put( "ALLT_INMT", listMap.get( "B3" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 6 ){
						updateMap.put( "INMT_DIVS", "3" );
						updateMap.put( "INMT_ITEMS", "4" );
						updateMap.put( "ALLT_INMT", listMap.get( "C1" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}else if( j == 7 ){
						updateMap.put( "INMT_DIVS", "3" );
						updateMap.put( "INMT_ITEMS", "5" );
						updateMap.put( "ALLT_INMT", listMap.get( "C2" ) );
						fdcrAd71Dao.alltInmtMgntUpdate( updateMap );
					}
				}
			}
			*/

			List list = (List) fdcrAd71Dao.alltInmtMgntList( commandMap );
			commandMap.put( "ds_List", list );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
