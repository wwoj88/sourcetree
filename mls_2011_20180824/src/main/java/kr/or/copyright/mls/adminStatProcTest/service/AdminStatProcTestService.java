package kr.or.copyright.mls.adminStatProcTest.service;

public interface AdminStatProcTestService {
	
	// 상당한노력 진행현황
	public void statProcList() throws Exception;
		
	// 대상저작물 수집
	public void getStatProcOrd() throws Exception;

	// 상당한노력 진행
	public void goStatProc() throws Exception;
	public void goStatProcV10() throws Exception;
	
	// 결과조회
	public void statProcPopup() throws Exception;
}
