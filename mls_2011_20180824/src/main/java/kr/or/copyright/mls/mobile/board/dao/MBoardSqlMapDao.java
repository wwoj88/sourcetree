package kr.or.copyright.mls.mobile.board.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.mobile.board.model.MBoard;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class MBoardSqlMapDao extends SqlMapClientDaoSupport implements MBoardDao {

	
	public List findBoardList(Map params) {

		return getSqlMapClientTemplate().queryForList("MobileBoard.getBoardList", params);
	}
	
	public int findBoardCount(Map params) {

		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("MobileBoard.findBoardCount", params));
	}
	
	public MBoard boardView(MBoard board) {
		return (MBoard)getSqlMapClientTemplate().queryForObject("MobileBoard.boardView", board);
	}

	public void insertUserMobileCount() {
		getSqlMapClientTemplate().insert("MobileBoard.insertUserMobileCount");
	}
}
