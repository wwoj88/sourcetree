package kr.or.copyright.mls.console.mber;

import java.net.URLDecoder;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.mber.inter.FdcrAd84Service;
import kr.or.copyright.mls.console.mber.inter.LogSearchService;

@Controller
public class LogSearch extends DefaultController{
	@Resource( name = "logSearchService" )
	private LogSearchService logSearchService;
	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;
	
	@RequestMapping( value = "/console/mber/log.page" )
	public String fdcrAd84List1( ModelMap model,Map<String, Object> commandMap,		HttpServletRequest request,		HttpServletResponse response ) throws Exception{

		logSearchService.logList( commandMap );
		
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );

		return "/mber/logList.tiles";
	}

}
