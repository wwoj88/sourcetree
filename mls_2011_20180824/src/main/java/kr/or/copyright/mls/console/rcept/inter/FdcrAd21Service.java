package kr.or.copyright.mls.console.rcept.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface FdcrAd21Service{

	/**
	 * 미분배보상금 방송음악 저작물 보고일 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd21List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 일괄등록
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd21Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception;

}
