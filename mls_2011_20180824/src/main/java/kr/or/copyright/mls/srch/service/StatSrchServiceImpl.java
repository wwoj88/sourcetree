package kr.or.copyright.mls.srch.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.srch.dao.StatSrchDao;

import com.tobesoft.platform.data.Dataset;

public class StatSrchServiceImpl extends BaseService implements StatSrchService {

	//private Log logger = LogFactory.getLog(getClass());

	private StatSrchDao statSrchDao;
	
	public void setStatSrchDao(StatSrchDao statSrchDao){
		this.statSrchDao = statSrchDao;
	}

	// statSrchList 조회 
	public void statSrchList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition"); 
		String  SRCH_YMD   = null;
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		SRCH_YMD =  ds_condition.getColumnAsString(0, "SRCH_YMD");
		map.put("SRCH_YMD", SRCH_YMD);
		
		//DAO호출
		List list  = (List) statSrchDao.statSrchList(map);		
		//List pageCount = (List) statSrchDao.statSrchListCount(map); 
	
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		//addList("ds_page", pageCount);	

	}

	
}
