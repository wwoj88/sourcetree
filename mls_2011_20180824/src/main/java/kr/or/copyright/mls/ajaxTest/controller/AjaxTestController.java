package kr.or.copyright.mls.ajaxTest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import kr.or.copyright.mls.ajaxTest.model.AjaxTest;
import kr.or.copyright.mls.ajaxTest.model.Code;
import kr.or.copyright.mls.ajaxTest.service.CodeListService;
import kr.or.copyright.mls.ajaxTest.service.UserListService;
import kr.or.copyright.mls.common.utils.AjaxXmlView;
import kr.or.copyright.mls.user.model.ZipCodeRoad;
import kr.or.copyright.mls.user.service.UserService;
import net.sourceforge.ajaxtags.xml.AjaxXmlBuilder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AjaxTestController{
	
	private static final Log LOG = LogFactory.getLog(AjaxTestController.class);
	
	@Resource(name="CodeListBO")
	private CodeListService codeListBO;
	
	@Resource(name="UserListBO")
	private UserListService userListBO;
	
	@Resource(name="userService")
	private UserService userService;
	
	
	@RequestMapping(value = "/ajaxTest/test.page")
	public String getAjaxTest(ModelMap model){
		System.out.println( "ajaxTest call" );
		/*AjaxTest vo = new AjaxTest();
		model.addAttribute("AjaxTestVO",vo);*/
		return "ajaxTest/ajaxTest";
	}
	
	@SuppressWarnings("unused")
	@ModelAttribute("initZip")
	private List<ZipCodeRoad> initZip() {
		return userService.getInitRoadCodeList();
	}
	
	@SuppressWarnings("unused")
	@ModelAttribute("deptInfoOneDepthCategory")
	private Map<String, String> referenceDataOneDepthDept() {
		return codeListBO.getInitCodeList();
	}
	
	@RequestMapping("/ajaxTest/autoSelectDept.do")
	protected ModelAndView getDeptInfoforSelectTag(@RequestParam("superdeptid") String superdeptid) throws Exception{
		Map<String, String> params = new HashMap<String, String>();
		params.put("superdeptid", superdeptid);
		List<Code> codeList = codeListBO.getCodeList(params);
		ModelAndView model = new ModelAndView(new AjaxXmlView());
		model.addObject("ajaxXml", (new AjaxXmlBuilder()).addItems(codeList, "codeName", "midCode", true).toString());
		return model;
	}
	
	//사원정보 리스트 페이지에서 검색입력창(사원이름)에 사용되는 자동완성기능
	@RequestMapping("/ajaxTest/suggestName.do")
	protected ModelAndView suggestName(@RequestParam("searchName") String searchName){
		
		ModelAndView model = new ModelAndView(new AjaxXmlView());
		List<String> nameList = userListBO.getNameListForSuggest(searchName);
		
		AjaxXmlBuilder ajaxXmlBuilder = new AjaxXmlBuilder();
		
		for(String name:nameList){
			ajaxXmlBuilder.addItem(name, name, false);
		}
		model.addObject("ajaxXml",ajaxXmlBuilder.toString());
		return model;
	}
	
	@RequestMapping("/ajaxTest/jsonTest.do")
	protected ModelAndView getJsonTest(HttpServletRequest request) throws Exception{
		ModelAndView modelAndView = new ModelAndView("jsonReport");
		Map<String, String> params = new HashMap<String, String>();
		params.put("superdeptid", "1");
		System.out.println( "JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ" );
		List<Code> codeList = codeListBO.getCodeList(params);
		modelAndView.addObject("codeList",codeList);
		return modelAndView;
	}
	
	/*@RequestMapping("/ajaxTest/zip.do")
	protected ModelAndView getJsonZip(HttpServletRequest request) throws Exception{
		ModelAndView modelAndView = new ModelAndView("jsonReport");
		Map<String, String> params = new HashMap<String, String>();
		String sidoNm = ServletRequestUtils.getStringParameter(request, "sidoNm");
		System.out.println("sidoNmsidoNmsidoNmsidoNm: "+sidoNm);
		params.put("sidoNm", sidoNm);
		List<ZipCodeRoad> codeList = codeListBO.getRoadCodeList(params);
		modelAndView.addObject("codeList",codeList);
		return modelAndView;
	}*/
}