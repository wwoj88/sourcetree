package kr.or.copyright.mls.imit.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.imit.dao.ImitDao;

import com.tobesoft.platform.data.Dataset;

public class ImitServiceImpl extends BaseService implements ImitService {

	//private Log logger = LogFactory.getLog(getClass());

	private ImitDao imitDao;
	
	public void setImitDao(ImitDao imitDao){
		this.imitDao = imitDao;
	}

	// imitList 조회 
	public void imitList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) imitDao.imitList(map);		
		List pageCount = (List) imitDao.imitListCount(map); 
	
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		addList("ds_page", pageCount);	

	}

	
}
