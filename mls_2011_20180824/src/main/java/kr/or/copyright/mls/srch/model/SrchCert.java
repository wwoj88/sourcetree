package kr.or.copyright.mls.srch.model;

public class SrchCert {
	private String genreCd;
	private String genreCdName;
	private String genreCdScndName;
	private String worksTitle;
	private int pageNo;
	private int startNo;
	private int endNo;
	private int rowNo;
	private String applyCertNo;
	private String certName;
	private String certDy;
	private int worksSort;
	
	public SrchCert() {
		// TODO Auto-generated constructor stub
	}
	public SrchCert(String genreCd, String genreCdName, String genreCdScndName,
			String worksTitle, int pageNo, int startNo, int endNo, int rowNo,
			String applyCertNo, String certName, String certDy, int worksSort) {
		super();
		this.genreCd = genreCd;
		this.genreCdName = genreCdName;
		this.genreCdScndName = genreCdScndName;
		this.worksTitle = worksTitle;
		this.pageNo = pageNo;
		this.startNo = startNo;
		this.endNo = endNo;
		this.rowNo = rowNo;
		this.applyCertNo = applyCertNo;
		this.certName = certName;
		this.certDy = certDy;
		this.worksSort = worksSort;
	}
	@Override
	public String toString() {
		return "SrchCert [genreCd=" + genreCd + ", genreCdName=" + genreCdName
				+ ", genreCdScndName=" + genreCdScndName + ", worksTitle="
				+ worksTitle + ", pageNo=" + pageNo + ", startNo=" + startNo
				+ ", endNo=" + endNo + ", rowNo=" + rowNo + ", applyCertNo="
				+ applyCertNo + ", certName=" + certName + ", certDy=" + certDy
				+ ", worksSort=" + worksSort + "]";
	}
	public String getGenreCd() {
		return genreCd;
	}
	public void setGenreCd(String genreCd) {
		this.genreCd = genreCd;
	}
	public String getGenreCdName() {
		return genreCdName;
	}
	public void setGenreCdName(String genreCdName) {
		this.genreCdName = genreCdName;
	}
	public String getGenreCdScndName() {
		return genreCdScndName;
	}
	public void setGenreCdScndName(String genreCdScndName) {
		this.genreCdScndName = genreCdScndName;
	}
	public String getWorksTitle() {
		return worksTitle;
	}
	public void setWorksTitle(String worksTitle) {
		this.worksTitle = worksTitle;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getStartNo() {
		return startNo;
	}
	public void setStartNo(int startNo) {
		this.startNo = startNo;
	}
	public int getEndNo() {
		return endNo;
	}
	public void setEndNo(int endNo) {
		this.endNo = endNo;
	}
	public int getRowNo() {
		return rowNo;
	}
	public void setRowNo(int rowNo) {
		this.rowNo = rowNo;
	}
	public String getApplyCertNo() {
		return applyCertNo;
	}
	public void setApplyCertNo(String applyCertNo) {
		this.applyCertNo = applyCertNo;
	}
	public String getCertName() {
		return certName;
	}
	public void setCertName(String certName) {
		this.certName = certName;
	}
	public String getCertDy() {
		return certDy;
	}
	public void setCertDy(String certDy) {
		this.certDy = certDy;
	}
	public int getWorksSort() {
		return worksSort;
	}
	public void setWorksSort(int worksSort) {
		this.worksSort = worksSort;
	}

	

	

	
}
