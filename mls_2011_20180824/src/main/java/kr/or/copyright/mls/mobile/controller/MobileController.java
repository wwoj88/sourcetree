package kr.or.copyright.mls.mobile.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import kr.or.copyright.mls.common.utils.CommonUtil;
import kr.or.copyright.mls.mobile.board.service.MBoardService;
import kr.or.copyright.mls.mobile.model.Common;
import kr.or.copyright.mls.mobile.model.FAQ;
import kr.or.copyright.mls.mobile.model.Stat;
import kr.or.copyright.mls.mobile.service.MobileService;

public class MobileController extends MultiActionController {
	private MobileService mobileService;
	private MBoardService mBoardService;
	public MBoardService getmBoardService() {
		return mBoardService;
	}

	public void setmBoardService(MBoardService mBoardService) {
		this.mBoardService = mBoardService;
	}

	/* mobile index */
	public ModelAndView main(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		mobileService.insertUserMobileCount(1);//이용자현황 카운트
		ModelAndView mav = new ModelAndView();
		mav.addObject("commonHtm", Common.getCommonHtm());
		mav.setViewName("mobile/main");
		return mav;
	}
	
	/* 접속카운트 수집 */	
	public ModelAndView insertUserCount(HttpServletRequest request, HttpServletResponse respone) throws Exception{
		
		int sysCd = ServletRequestUtils.getIntParameter(request, "sysCd", 1);	// 1: 저작권찾기, 2: CLMS
		mobileService.insertUserMobileCount(sysCd);//이용자현황 카운트
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("mobile/result");
		return mav;
	}
	
	public ModelAndView worksInfo(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		mav.addObject("commonHtm", Common.getCommonHtm());
		mav.setViewName("mobile/info/worksInfo");
		return mav;
	}
	
	public ModelAndView guide(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		mav.addObject("commonHtm", Common.getCommonHtm());
		mav.setViewName("mobile/guide/guideInfo");
		return mav;
	}
	
	public ModelAndView statList(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
//		HashMap<String,Object> hm = new HashMap<String, Object>();
//		String keyWord = ServletRequestUtils.getStringParameter(request, "keyWord");
//		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo",1);
//		hm.put("keyWord", keyWord);
//		hm.put("pageNo", pageNo);
//		List<Stat> statList = mobileService.statList(hm);
//		int totalCnt = mobileService.statListCnt(hm);
//		Map<String,Object> viewObject = new HashMap<String,Object>();
//		viewObject.put("nowPage", pageNo);
//		viewObject.put("keyWord", keyWord);
//		if(totalCnt>100 && "".equals(keyWord)) {
//			viewObject.put("totalCnt", 100);
//		} else {
//			viewObject.put("totalCnt", totalCnt);
//		}
//		mav.addObject("statList",statList);
//		mav.addObject("viewObject", viewObject);
		mav.addObject("commonHtm", Common.getCommonHtm());
		mav.setViewName("mobile/statBord/statBo00List");
		return mav;
	}
	
	public ModelAndView statDetl(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		Stat stat = mobileService.statDetl(ServletRequestUtils.getIntParameter(request, "bordSeqn"));
		stat.setBordDesc(CommonUtil.replaceBr(stat.getBordDesc(),true));
		mav.addObject("commonHtm", Common.getCommonHtm());		
		mav.setViewName("mobile/stat/statDetl");
		mav.addObject("stat",stat);
		return mav;
	}
	

	public ModelAndView ftaqList(HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		
		int pageNo = ServletRequestUtils.getIntParameter(request, "pageNo", 1);
		HashMap<String,Object> hm = new HashMap<String,Object>();
		hm.put("pageNo", pageNo);
		List<FAQ> faqList = mobileService.faqList(hm);
		int totalCnt = mobileService.faqListCnt(hm);
		Map<String,Object> viewObject = new HashMap<String,Object>();
		viewObject.put("nowPage", pageNo);
		viewObject.put("totalCnt", totalCnt);
		
		mav.addObject("faqList",faqList);
		mav.addObject("viewObject", viewObject);
		mav.addObject("commonHtm", Common.getCommonHtm());
		mav.setViewName("mobile/faq/ftaqList");
		return mav;
	}
	
	public MobileService getMobileService() {
		return mobileService;
	}

	public void setMobileService(MobileService mobileService) {
		this.mobileService = mobileService;
	}
	
	public String toKor(String str) {
		try
		{
			if(str == null){return "";}
			str = new String(str.getBytes("8859_1"),"EUC-KR");
		}
		catch (Exception e)
		{
			System.out.println("toKor error : " + e);
		}
		return str;
	}
}
