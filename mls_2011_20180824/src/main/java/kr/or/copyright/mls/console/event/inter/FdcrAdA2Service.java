package kr.or.copyright.mls.console.event.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAdA2Service{

	/**
	 * 게시물 질문조회 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA2List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 게시판 사용여부 체크
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA2List2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 게시판 사용여부 수정
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA2Update1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 게시물 상세조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA2View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 게시물 삭제
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA2Delete1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 게시물 답변 등록
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA2Regi1( Map<String, Object> commandMap ) throws Exception;

}
