package kr.or.copyright.mls.adminStatBoard.service;

import java.util.List;
import java.util.Map;

public interface AdminStatBoardService {
	
	// 이용승인신청 공고 - 목록
	public void statBord01List() throws Exception;
	
	// 이용승인신청 공고 - 엑셀다운
	public void statBord01ExcelDown() throws Exception;
	
	// 이용승인신청 공고 ,상당한 노력공고  - 상세
	public void statBord01Detail() throws Exception;
	
	// 이용승인신청 공고 - 등록
	public void statBord01Save() throws Exception;
	
	// 이용승인신청 공고 - 일괄등록
	public void statBord01RegiAll() throws Exception;
	
	// 이용승인신청 공고 접수번호 중복확인
	public void statBord01RegiSearch() throws Exception;
	// 이용승인신청 공고 - 수정
	public void statBord01Modi() throws Exception;
	
	// 이용승인신청 이의제기 진행상태 변경
	public void statObjcChange()throws Exception;
	
	// 이용승인신청 이의제기 진행상태내역조회
	public void adminStatObjcHist() throws Exception;
	
	// 이용승인신청 승인공고 - 목록
	public void statBord02List() throws Exception;
	
	//이용승인신청 승인공고 - 엑셀다운
	public void statBord02ExcelDown() throws Exception;
	
	//이용승인신청 승인공고(statBord02), 보상금 공탁공고(statBord03)- 상세
	public void statBordDetail() throws Exception;
	
	// 이용승인신청 승인공고 게시판 - 등록
	public void statBord02Regi()throws Exception;
	
	// 이용승인신청 승인공고 게시판 - 일괄등록
	public void statBord02RegiAll() throws Exception;
	
	// 이용승인신청 승인공고 접수번호 중복데이터 확인
	public void statBord02RegiSearch() throws Exception;
	
	// 이용승인신청 승인공고 게시판 - 수정
	public void statBord02Modi()throws Exception;
	
	// 이용승인신청 승인공고 게시판 - 삭제
	public void statBord02Delete()throws Exception;

	//보상금 공탁 공고 게시판 - 목록
	public void statBord03List() throws Exception;
	
	//보상금 공탁 공고 게시판 - 공고승인
	public void statBord03Mgnt()throws Exception;
	
	//상당한 노력 공고 게시판 -목록
	public void statBord04List() throws Exception;
	
	//법정허락 대상 저작물 -목록
	public void statBord05List() throws Exception;
	
	//법정허락 대상 저작물 -상세
	public void statBord05Detail() throws Exception;
	
	//이의신청 현황 -목록
	public void objeList() throws Exception;
	
	//저작권자 찾기 위한 상당한 노력 신청 - 목록
	public void statBord06List() throws Exception;
	
	//저작권자 찾기 위한 상당한 노력 신청 - 상세
	public void statBord06Detail() throws Exception;
	
	//저작권자 찾기 위한 상당한 노력 신청 (진행상태)- 변경
	public void stat06ShistChange()throws Exception;
	
	//저작권자 찾기 위한 상당한 노력 신청 (진행상태 처리내역)- 목록,상세
	public void statBord06ShistList() throws Exception;
	
	// 메인화면 - 미공고 목록 (저작권자 조회공고, 보상금 공탁)
	public void mainStatBordList01() throws Exception;
	
	// 메인화면 - 이의신청 목록 (이용승인신청공고, 저작권자 조회공고, 법정허락대상물)
	public void mainStatBordList02() throws Exception;
	
	// 저작권자 조회공고, 보상금 공탁공고 처리상태 수정(신청,보완,신청승인,반려)
	public void statBordStatUpdate() throws Exception;
	
	// 저작권자 조회공고, 보상금 공탁공고 삭제 
	public void statBordStatDeltUpdate() throws Exception;
	
	public void statBordSuplDetail() throws Exception;
	
	// 저작권자 조회공고 보완정보 등록
	public void statBordSuplRegi() throws Exception;
	
	//상당한노력신청 저작권자 조회 공고내용 수정 및 보완이력 등록 	
	public void statBord06SuplRegi() throws Exception;
	
	//  상당한노력신청 - 저작권자 조회공고 삭제
	public void statBord06DeltUpdate() throws Exception;
	// 신청정보 보완안내 메일 발신내역	
	public void selectBordSuplSendList() throws Exception;
	
	public void statBordSuplList() throws Exception;
}


