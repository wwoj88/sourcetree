package kr.or.copyright.mls.myStat.controller;

import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.common.mail.SendMail;
import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.common.service.CodeListService;
import kr.or.copyright.mls.common.service.CommonService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.myStat.model.ApplySign;
import kr.or.copyright.mls.myStat.model.File;
import kr.or.copyright.mls.myStat.model.StatApplication;
import kr.or.copyright.mls.myStat.model.StatApplicationShis;
import kr.or.copyright.mls.myStat.model.StatApplyWorks;
import kr.or.copyright.mls.myStat.service.MyStatService;
import kr.or.copyright.mls.rghtPrps.model.PrpsAttc;
import kr.or.copyright.mls.stat.model.MlStatWorksDefaultVO;
import kr.or.copyright.mls.stat.service.StatService;
import kr.or.copyright.mls.support.constant.SignContants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.dto.SignDTO;
import kr.or.copyright.mls.support.util.FileUploadUtil;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;
import kr.or.copyright.mls.support.util.SessionUtil;
import kr.or.copyright.mls.support.util.SignCertUtil;
import kr.or.copyright.mls.support.util.StringUtil;
import kr.or.copyright.mls.user.service.UserService;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

@RequestMapping("/myStat")
@Controller
public class MyStatController extends MultiActionController {
    
        @Resource(name="StatService")
        private StatService statService;
        
	private Logger log = Logger.getLogger(this.getClass());
	
	@javax.annotation.Resource(name="commonService")
	private CommonService commonService; 
	
	@javax.annotation.Resource(name="userService")
	private UserService userService;
	
	@javax.annotation.Resource(name="myStatService")
	private MyStatService myStatService;
	
	@javax.annotation.Resource(name="codeListService")
	private CodeListService codeListService;
		
	//private String realUploadPath = "D:/server/mls/mls_2010/upload/";  // 로컬용
	//private String realUploadPath = "C:/home/tmax/mls/web/upload/";  // 로컬용
	//private String realUploadPath = "/home/tmax/mls/web/upload/";  // 실서버용
	//private String realUploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버용
	private String realUploadPath = FileUtil.uploadPath();
    	
        CodeList code = new CodeList();
    
        @ModelAttribute("genreList")
        private List<CodeList> genreList() {
    		code.setBigCode("35");
    		return codeListService.commonCodeList(code);
        }
    
        @ModelAttribute("worksDivsList")
        private List<CodeList> worksDivsList() {
    		code.setBigCode("57");
    		return codeListService.commonCodeList(code);
        }
    
        @ModelAttribute("applyTypeList")
        private List<CodeList> applyTypeList() {
    		code.setBigCode("50");
    		return codeListService.commonCodeList(code);
        }
    
        @ModelAttribute("publMediList")
        private List<CodeList> publMediList() {
    		code.setBigCode("52");
    		return codeListService.commonCodeList(code);
        }
        
        @ModelAttribute("statCdList")
        private List<CodeList> statCdList() {
    		code.setBigCode("51");
    		return codeListService.commonCodeList(code);
        }
	    
	
	
	//법정허락 신청현황 목록 조회		//20120822 정병호 수정
	@RequestMapping(value = "/statRsltInqrList")
	public ModelAndView statRsltInqrList(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		Map params = new HashMap();
		User user = SessionUtil.getSession(request);
		
		StatApplication srchParam = new StatApplication();
		/* 조회조건 str */
		srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(request, "srchApplyType"));
		srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(request, "srchApplyFrDt"));
		srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(request, "srchApplyToDt"));
		srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(request, "srchApplyWorksTitl"));
		srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(request, "srchStatCd", 0));
		srchParam.setSrchRgstIdnt(user.getUserIdnt());
		/* 조회조건 end */
		
		int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
		int from = Constants.DEFAULT_ROW_PER_PAGE * (pageNo - 1) + 1;
                int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;
        
                srchParam.setNowPage(pageNo);
                srchParam.setStartRow(from);
                srchParam.setEndRow(to);

		List list = myStatService.statRsltInqrList(srchParam);
		int totalRow = myStatService.totalRowStatRsltInqrList(srchParam);
		
		ModelAndView mv = new ModelAndView("myStat/statRsltInqrList");
		mv.addObject("list", list);
		mv.addObject("totalRow", totalRow);
		mv.addObject("srchParam", srchParam);
		return mv;
	}
	
	//법정허락 신청 step1
	@RequestMapping(value = "/statPrps")
	public ModelAndView statPrps(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		Map params = new HashMap();
		User user = SessionUtil.getSession(request);
		params.put("userIdnt", user.getUserIdnt());
		
		// 사용자 조회
		Map userInfo = userService.selectUserInfo(params);

		// 공통코드 조회 시작
		// 신청서구분 코드 조회
		CodeList code = new CodeList();
		code.setBigCode("50");
		List applyTypeList = codeListService.commonCodeList(code);
		
		StatApplication srchParam = new StatApplication();
		/* 조회조건 str */
		srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(request, "srchApplyType"));
		srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(request, "srchApplyFrDt"));
		srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(request, "srchApplyToDt"));
		srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(request, "srchApplyWorksTitl"));
		srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(request, "srchStatCd", 0));
        srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		/* 조회조건 end */
		
		int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
		int from = Constants.DEFAULT_ROW_PER_PAGE * (pageNo - 1) + 1;
        int to = Constants.DEFAULT_ROW_PER_PAGE * pageNo;

        srchParam.setNowPage(pageNo);
        srchParam.setStartRow(from);
        srchParam.setEndRow(to);
        
        String actionDiv = ServletRequestUtils.getStringParameter(request, "action_div");
        
        if(actionDiv == null || actionDiv == ""){
        	actionDiv = "new";
        }
        
		ModelAndView mv = new ModelAndView("myStat/statPrps_step1");
		if(actionDiv.equals("new")) {

			mv = new ModelAndView("myStat/statPrps_step1");
			mv.addObject("srchParam", srchParam);
			mv.addObject("userInfo", userInfo);
			mv.addObject("applyTypeList", applyTypeList);
			
		}else if(actionDiv.equals("modi")) {
			mv = new ModelAndView("myStat/statPrpsModi_step1");
			mv.addObject("srchParam", srchParam);
		}
		
		return mv;
	}
	
	
	//법정허락 신청 step1 임시저장
	@RequestMapping(value = "/tmpStatPrps1SaveDo")
	public ModelAndView tmpStatPrps1SaveDo(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request);
		
		StatApplication srchParam = new StatApplication();
		/* 조회조건 str */
		srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(multipartRequest, "srchApplyType"));
		srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(multipartRequest, "srchApplyFrDt"));
		srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(multipartRequest, "srchApplyToDt"));
		srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(multipartRequest, "srchApplyWorksTitl"));
		srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(multipartRequest, "srchStatCd", 0));
        srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		/* 조회조건 end */
		
        String existYn = ServletRequestUtils.getStringParameter(multipartRequest, "existYn");	//이용신청명세서 첨부여부
        
        String actionDiv = ServletRequestUtils.getStringParameter(multipartRequest, "action_div");
		String processDiv = "UPDATE";

        String applyWriteYmd = ServletRequestUtils.getStringParameter(multipartRequest, "applyWriteYmd");
		int applyWriteSeq = ServletRequestUtils.getIntParameter(multipartRequest, "applyWriteSeq", 0);
		int applyWorksCnt = ServletRequestUtils.getIntParameter(multipartRequest, "applyWorksCnt", 1);

		if(applyWriteYmd == null || applyWriteYmd.equals("")){
			processDiv = "INSERT";

			DecimalFormat df = new DecimalFormat("00");
			Calendar currentCal = Calendar.getInstance();
			currentCal.add(currentCal.DATE, 0);
	        applyWriteYmd = Integer.toString(currentCal.get(Calendar.YEAR)) + df.format(currentCal.get(Calendar.MONTH)+1) + df.format(currentCal.get(Calendar.DAY_OF_MONTH));
	        
	        applyWriteSeq = myStatService.newApplyWriteSeq("TMP");
		}

        StatApplication param = new StatApplication();
        param.setApplyWriteYmd(applyWriteYmd);
        param.setApplyWriteSeq(applyWriteSeq);
        param.setApplyType01(ServletRequestUtils.getStringParameter(multipartRequest, "applyType01"));
        param.setApplyType02(ServletRequestUtils.getStringParameter(multipartRequest, "applyType02"));
        param.setApplyType03(ServletRequestUtils.getStringParameter(multipartRequest, "applyType03"));
        param.setApplyType04(ServletRequestUtils.getStringParameter(multipartRequest, "applyType04"));
        param.setApplyType05(ServletRequestUtils.getStringParameter(multipartRequest, "applyType05"));
        param.setApplyWorksCnt(applyWorksCnt);
        param.setApplyWorksTitl(ServletRequestUtils.getStringParameter(multipartRequest, "applyWorksTitl"));
        param.setApplyWorksKind(ServletRequestUtils.getStringParameter(multipartRequest, "applyWorksKind"));
        param.setApplyWorksForm(ServletRequestUtils.getStringParameter(multipartRequest, "applyWorksForm"));
        param.setUsexDesc(ServletRequestUtils.getStringParameter(multipartRequest, "usexDesc"));
        param.setApplyReas(ServletRequestUtils.getStringParameter(multipartRequest, "applyReas"));
        param.setCpstAmnt(ServletRequestUtils.getStringParameter(multipartRequest, "cpstAmnt"));
        
        param.setApplrName(ServletRequestUtils.getStringParameter(multipartRequest, "applrName"));
        param.setApplrResdCorpNumb(ServletRequestUtils.getStringParameter(multipartRequest, "applrResdCorpNumb"));
        param.setApplrAddr(ServletRequestUtils.getStringParameter(multipartRequest, "applrAddr"));
        param.setApplrTelx(ServletRequestUtils.getStringParameter(multipartRequest, "applrTelx"));
        
        param.setApplyProxyName(ServletRequestUtils.getStringParameter(multipartRequest, "applyProxyName"));
        param.setApplyProxyResdCorpNumb(ServletRequestUtils.getStringParameter(multipartRequest, "applyProxyResdCorpNumb"));
        param.setApplyProxyAddr(ServletRequestUtils.getStringParameter(multipartRequest, "applyProxyAddr"));
        param.setApplyProxyTelx(ServletRequestUtils.getStringParameter(multipartRequest, "applyProxyTelx"));
        
        param.setApplyRawCd(ServletRequestUtils.getIntParameter(multipartRequest, "applyRawCd", 0));
        param.setStatCd(1);		// 진행상황 : 임시저장
        param.setRgstIdnt(ServletRequestUtils.getStringParameter(multipartRequest, "rgstIdnt"));
        
        param.setFileNameCd(ServletRequestUtils.getStringParameters(multipartRequest, "fileNameCd"));
        param.setFileDelYn(ServletRequestUtils.getStringParameters(multipartRequest, "fileDelYn"));
        param.setHddnFile(ServletRequestUtils.getStringParameters(multipartRequest, "hddnFile"));
        param.setFileName(ServletRequestUtils.getStringParameters(multipartRequest, "fileName"));
        param.setRealFileName(ServletRequestUtils.getStringParameters(multipartRequest, "realFileName"));
        param.setFileSize(ServletRequestUtils.getStringParameters(multipartRequest, "fileSize"));
        param.setFilePath(ServletRequestUtils.getStringParameters(multipartRequest, "filePath"));
        
       // long startTime = System.currentTimeMillis();

        
       // System.out.println(">>>>>>>>>>>>>>>>:::::::시작하는 시간::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+startTime+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		/* 첨부화일 업로드 시작 */
        int[] attcSeqn = ServletRequestUtils.getIntParameters(multipartRequest, "attcSeqn");
	String[] fileNameCd = ServletRequestUtils.getStringParameters(multipartRequest, "fileNameCd");
	String[] fileDelYn = ServletRequestUtils.getStringParameters(multipartRequest, "fileDelYn");
	String[] hddnFile = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFile");
		
		//첨부파일 중 수정 된 첨부파일에 대한 정보 수집
		List delFileList = new ArrayList();

		for(int i = 0; i < attcSeqn.length; i++) {
			File delFileInfo = new File();
			if(fileDelYn[i].equals("Y")) {
				if(attcSeqn[i] == 0) {
					break;
				}
				System.out.println(attcSeqn[i]);
				System.out.println(fileNameCd[i]);
				delFileInfo.setAttcSeqn(attcSeqn[i]);
				delFileInfo.setFileAttcCd("ST");
				delFileInfo.setFileNameCd(fileNameCd[i]);
				delFileInfo.setApplyWriteYmd(applyWriteYmd);
				delFileInfo.setApplyWriteSeq(applyWriteSeq);
				
				delFileList.add(delFileInfo);
			}
		}
		param.setDelFileList(delFileList);

		//새로 첨부된 첨부파일에 대한 정보 수집
		List fileList = new ArrayList();
		
		Iterator fileNameIterator = multipartRequest.getFileNames();
		
		
		int newAttcSeqn = commonService.getNewAttcSeqn();
		
		while(fileNameIterator.hasNext()) {
			MultipartFile multiFile= multipartRequest.getFile((String)fileNameIterator.next());
			if(multiFile.getSize()>0) {
				PrpsAttc uploadInfo = FileUploadUtil.uploadPrpsFiles(multiFile, realUploadPath);
				System.out.println("################ " +uploadInfo.getFILE_NAME());
				File fileInfo = new File();
				for(int fm = 0; fm < hddnFile.length; fm++){
					if(hddnFile[fm].toUpperCase().equals(uploadInfo.getFILE_NAME().toUpperCase())){
						fileInfo.setAttcSeqn(newAttcSeqn);
						fileInfo.setFileAttcCd("ST");
						fileInfo.setFileNameCd(fileNameCd[fm]);
						fileInfo.setFileName(hddnFile[fm]);
						fileInfo.setFilePath(uploadInfo.getFILE_PATH());
						fileInfo.setFileSize(Integer.parseInt(uploadInfo.getFILE_SIZE()));
						fileInfo.setRealFileName(uploadInfo.getREAL_FILE_NAME());
						fileInfo.setApplyWriteYmd(applyWriteYmd);
						fileInfo.setApplyWriteSeq(applyWriteSeq);
						fileInfo.setRgstIdnt(ServletRequestUtils.getStringParameter(multipartRequest, "rgstIdnt")); 
						hddnFile[fm] = "";
						break;
					}
				}
				
				fileList.add(fileInfo);
			}
			newAttcSeqn++;
		}
		
		
		//long endTime = System.currentTimeMillis();
		param.setFileList(fileList);
		//System.out.println(">>>>>>>>>>>>>>>>:::::::끝나는 시간::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+endTime+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		//long durationTime = endTime - startTime;
		//System.out.println("milli second : " + durationTime );
		//System.out.println("second  = milli second / 1000 : " + (durationTime / 1000));
		
		
		if(processDiv.equals("INSERT")) {
    		myStatService.insertTmpStatPrps(request, param);
		} else {
    		myStatService.updateTmpStatPrps(request, param);
		}
		
		ModelAndView mv = new ModelAndView("myStat/statPrps_step2");
		if(actionDiv.equals("goStep2")) {
			if(applyWorksCnt < 6) {
				srchParam.setApplyWriteYmd(applyWriteYmd);
				srchParam.setApplyWriteSeq(applyWriteSeq);
				
				// 공통코드 조회 시작
				// 신청서구분 코드 조회
				CodeList code = new CodeList();
				code.setBigCode("50");
				List applyTypeList = codeListService.commonCodeList(code);
	
				// 공표방법 코드 조회
				code.setBigCode("52");
				List publMediList = codeListService.commonCodeList(code);
	
				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd(applyWriteYmd);
				statApplyWorks.setApplyWriteSeq(applyWriteSeq);
				
				List statApplyWorksList = myStatService.statApplyWorksListTmp(statApplyWorks);
				
				mv = new ModelAndView("myStat/statPrps_step2", "statApplyWorksList", statApplyWorksList);
				mv.addObject("statCd", 1);
				mv.addObject("applyWorksCnt", applyWorksCnt);
				mv.addObject("srchParam", srchParam);
				mv.addObject("applyTypeList", applyTypeList);
				mv.addObject("publMediList", publMediList);
				mv.addObject("existYn", existYn);
				
			}else{
				srchParam.setApplyWriteYmd(applyWriteYmd);
				srchParam.setApplyWriteSeq(applyWriteSeq);

				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd(applyWriteYmd);
				statApplyWorks.setApplyWriteSeq(applyWriteSeq);
				myStatService.deleteStatApplyWorksTmp(statApplyWorks);
				
				// 공통코드 조회 시작
				// 신청서구분 코드 조회
				CodeList code = new CodeList();
				code.setBigCode("50");
				List applyTypeList = codeListService.commonCodeList(code);

				// 공표방법 코드 조회
				code.setBigCode("52");
				List publMediList = codeListService.commonCodeList(code);

				Map params = new HashMap();
				User user = SessionUtil.getSession(request);
				params.put("userIdnt", user.getUserIdnt());
				
				// 사용자 조회
				Map userInfo = userService.selectUserInfo(params);

				StatApplication param2 = new StatApplication();
				param2.setApplyWriteYmd(applyWriteYmd);
				param2.setApplyWriteSeq(applyWriteSeq);
				
				StatApplication statApplication = myStatService.statApplicationTmp(param2);
				
				List fileList2 = myStatService.statAttcFileTmp(param2);
				statApplication.setFileList(fileList2);
				
				List statApplyWorksList = myStatService.statApplyWorksListTmp(statApplyWorks);
				
				mv = new ModelAndView("myStat/statPrps_step3");
				mv.addObject("userInfo", userInfo);
				mv.addObject("statApplication", statApplication);
				mv.addObject("statApplyWorksList", statApplyWorksList);
				mv.addObject("srchParam", srchParam);
				mv.addObject("applyTypeList", applyTypeList);
				mv.addObject("publMediList", publMediList);
			}
			
		}else if(actionDiv.equals("goList")) {
			mv = new ModelAndView("redirect:/myStat/statRsltInqrList.do");
		}
		
		return mv;
	}
	

	//법정허락 신청 step2 임시저장
	@RequestMapping(value = "/tmpStatPrps2SaveDo")
	public ModelAndView tmpStatPrps2SaveDo(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		StatApplication srchParam = new StatApplication();
		/* 조회조건 str */
		srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(request, "srchApplyType"));
		srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(request, "srchApplyFrDt"));
		srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(request, "srchApplyToDt"));
		srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(request, "srchApplyWorksTitl"));
		srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(request, "srchStatCd", 0));
        srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		/* 조회조건 end */
        
        String existYn = ServletRequestUtils.getStringParameter(request, "existYn");	//이용신청명세서 첨부여부
		
        String actionDiv = ServletRequestUtils.getStringParameter(request, "action_div");

        String applyWriteYmd = ServletRequestUtils.getStringParameter(request, "applyWriteYmd");
		int applyWriteSeq = ServletRequestUtils.getIntParameter(request, "applyWriteSeq", 0);
		
		String applyType01        = request.getParameter("applyType01");
		String applyType02        = request.getParameter("applyType02");
		String applyType03        = request.getParameter("applyType03");
		String applyType04        = request.getParameter("applyType04");
		String applyType05        = request.getParameter("applyType05");
		String[] worksTitl          = request.getParameterValues("worksTitl");
		String[] worksKind          = request.getParameterValues("worksKind");
		String[] worksForm          = request.getParameterValues("worksForm");
		String[] worksDesc          = request.getParameterValues("worksDesc");
		String[] publYmd            = request.getParameterValues("publYmd");
		String[] publNatn           = request.getParameterValues("publNatn");
		String publMediCd            = request.getParameter("publMediCd");
		String[] publMediEtc		= request.getParameterValues("publMediEtc");
		String[] publMedi           = request.getParameterValues("publMedi");
		String[] coptHodrName       = request.getParameterValues("coptHodrName");
		String[] coptHodrTelxNumb   = request.getParameterValues("coptHodrTelxNumb");
		String[] coptHodrAddr       = request.getParameterValues("coptHodrAddr");

		List paramList = new ArrayList();
		for(int i = 0; i < worksTitl.length; i++) {
			StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
			
			statApplyWorksInfo.setApplyWriteYmd(applyWriteYmd);
			statApplyWorksInfo.setApplyWriteSeq(applyWriteSeq);
			statApplyWorksInfo.setWorksSeqn(i+1);
			statApplyWorksInfo.setApplyType01(StringUtil.nullToEmpty(applyType01.substring(i, i+1)));
			statApplyWorksInfo.setApplyType02(StringUtil.nullToEmpty(applyType02.substring(i, i+1)));
			statApplyWorksInfo.setApplyType03(StringUtil.nullToEmpty(applyType03.substring(i, i+1)));
			statApplyWorksInfo.setApplyType04(StringUtil.nullToEmpty(applyType04.substring(i, i+1)));
			statApplyWorksInfo.setApplyType05(StringUtil.nullToEmpty(applyType05.substring(i, i+1)));
			statApplyWorksInfo.setWorksTitl(StringUtil.nullToEmpty(worksTitl[i]));
			statApplyWorksInfo.setWorksKind(StringUtil.nullToEmpty(worksKind[i]));
			statApplyWorksInfo.setWorksForm(StringUtil.nullToEmpty(worksForm[i]));
			statApplyWorksInfo.setWorksDesc(StringUtil.nullToEmpty(worksDesc[i]));
			statApplyWorksInfo.setPublYmd(StringUtil.nullToEmpty(publYmd[i]));
			statApplyWorksInfo.setPublNatn(StringUtil.nullToEmpty(publNatn[i]));
			String newPublMediCd = publMediCd.substring(0, publMediCd.indexOf('!'));
			publMediCd = publMediCd.substring(publMediCd.indexOf('!')+1);
			statApplyWorksInfo.setPublMediCd(Integer.parseInt(newPublMediCd));
			statApplyWorksInfo.setPublMediEtc(StringUtil.nullToEmpty(publMediEtc[i]));
			statApplyWorksInfo.setPublMedi(StringUtil.nullToEmpty(publMedi[i]));
			statApplyWorksInfo.setCoptHodrName(StringUtil.nullToEmpty(coptHodrName[i]));
			statApplyWorksInfo.setCoptHodrTelxNumb(StringUtil.nullToEmpty(coptHodrTelxNumb[i]));
			statApplyWorksInfo.setCoptHodrAddr(StringUtil.nullToEmpty(coptHodrAddr[i]));
			
			paramList.add(statApplyWorksInfo);
		}
		//기존에 등록된 이용승인 신청 명세서 삭제 및 신규 등록
		myStatService.insertTmpStatApplyWorksList(paramList);
		
		ModelAndView mv = new ModelAndView("myStat/statPrps_step3");
		if(actionDiv.equals("goStep3")) {

			if(existYn.equals("Y")){
				srchParam.setApplyWriteYmd(applyWriteYmd);
				srchParam.setApplyWriteSeq(applyWriteSeq);
	
				// 공통코드 조회 시작
				// 신청서구분 코드 조회
				CodeList code = new CodeList();
				code.setBigCode("50");
				List applyTypeList = codeListService.commonCodeList(code);
	
				// 공표방법 코드 조회
				code.setBigCode("52");
				List publMediList = codeListService.commonCodeList(code);
	
				Map params = new HashMap();
				User user = SessionUtil.getSession(request);
				params.put("userIdnt", user.getUserIdnt());
				
				// 사용자 조회
				Map userInfo = userService.selectUserInfo(params);
	
				StatApplication param = new StatApplication();
				param.setApplyWriteYmd(ServletRequestUtils.getStringParameter(request, "applyWriteYmd"));
				param.setApplyWriteSeq(ServletRequestUtils.getIntParameter(request, "applyWriteSeq", 0));
				
				StatApplication statApplication = myStatService.statApplicationTmp(param);
				
				List fileList2 = myStatService.statAttcFileTmp(param);
				statApplication.setFileList(fileList2);
				
				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd(applyWriteYmd);
				statApplyWorks.setApplyWriteSeq(applyWriteSeq);
				
				List statApplyWorksList = myStatService.statApplyWorksListTmp(statApplyWorks);
				
				mv = new ModelAndView("myStat/statPrps_step3");
				mv.addObject("userInfo", userInfo);
				mv.addObject("statApplication", statApplication);
				mv.addObject("statApplyWorksList", statApplyWorksList);
				mv.addObject("srchParam", srchParam);
				mv.addObject("applyTypeList", applyTypeList);
				mv.addObject("publMediList", publMediList);
				
			}else{
				mv = new ModelAndView("redirect:/myStat/statPrpsModi.do?srchApplyWorksTitl="+srchParam.getSrchApplyWorksTitl());
				mv.addObject("srchApplyType", srchParam.getSrchApplyType());
				mv.addObject("srchApplyFrDt", srchParam.getSrchApplyFrDt());
				mv.addObject("srchApplyToDt", srchParam.getSrchApplyToDt());
//				mv.addObject("srchApplyWorksTitl", srchParam.getSrchApplyWorksTitl());
				mv.addObject("srchStatCd", srchParam.getSrchStatCd());
				mv.addObject("page_no", srchParam.getNowPage());
				mv.addObject("stat_cd", ServletRequestUtils.getStringParameter(request, "stat_cd"));
				mv.addObject("apply_write_ymd", applyWriteYmd);
				mv.addObject("apply_write_seq", applyWriteSeq);
			}
			
		}else if(actionDiv.equals("goList")) {
			mv = new ModelAndView("redirect:/myStat/statRsltInqrList.do");
		}
		
		return mv;
	}
	
	
	//법정허락 신청 step1 보완저장
	@RequestMapping(value = "/statPrps1SaveDo")
	public ModelAndView statPrps1SaveDo(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request);
		
		StatApplication srchParam = new StatApplication();
		/* 조회조건 str */
		srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(multipartRequest, "srchApplyType"));
		srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(multipartRequest, "srchApplyFrDt"));
		srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(multipartRequest, "srchApplyToDt"));
		srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(multipartRequest, "srchApplyWorksTitl"));
		srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(multipartRequest, "srchStatCd", 0));
        srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		/* 조회조건 end */
        
        String existYn = ServletRequestUtils.getStringParameter(multipartRequest, "existYn");	//이용신청명세서 첨부여부
		
        String actionDiv = ServletRequestUtils.getStringParameter(multipartRequest, "action_div");
		String processDiv = "UPDATE";

        String applyWriteYmd = ServletRequestUtils.getStringParameter(multipartRequest, "applyWriteYmd");
		int applyWriteSeq = ServletRequestUtils.getIntParameter(multipartRequest, "applyWriteSeq", 0);
		int applyWorksCnt = ServletRequestUtils.getIntParameter(multipartRequest, "applyWorksCnt", 1);

		if(applyWriteYmd == null || applyWriteYmd.equals("")){
			processDiv = "INSERT";

			DecimalFormat df = new DecimalFormat("00");
			Calendar currentCal = Calendar.getInstance();
			currentCal.add(currentCal.DATE, 0);
	        applyWriteYmd = Integer.toString(currentCal.get(Calendar.YEAR)) + df.format(currentCal.get(Calendar.MONTH)+1) + df.format(currentCal.get(Calendar.DAY_OF_MONTH));
	        
	        applyWriteSeq = myStatService.newApplyWriteSeq("TMP");
		}

        StatApplication param = new StatApplication();
        param.setApplyWriteYmd(applyWriteYmd);
        param.setApplyWriteSeq(applyWriteSeq);
        param.setApplyType01(ServletRequestUtils.getStringParameter(multipartRequest, "applyType01"));
        param.setApplyType02(ServletRequestUtils.getStringParameter(multipartRequest, "applyType02"));
        param.setApplyType03(ServletRequestUtils.getStringParameter(multipartRequest, "applyType03"));
        param.setApplyType04(ServletRequestUtils.getStringParameter(multipartRequest, "applyType04"));
        param.setApplyType05(ServletRequestUtils.getStringParameter(multipartRequest, "applyType05"));
        param.setApplyWorksCnt(applyWorksCnt);
        param.setApplyWorksTitl(ServletRequestUtils.getStringParameter(multipartRequest, "applyWorksTitl"));
        param.setApplyWorksKind(ServletRequestUtils.getStringParameter(multipartRequest, "applyWorksKind"));
        param.setApplyWorksForm(ServletRequestUtils.getStringParameter(multipartRequest, "applyWorksForm"));
        param.setUsexDesc(ServletRequestUtils.getStringParameter(multipartRequest, "usexDesc"));
        param.setApplyReas(ServletRequestUtils.getStringParameter(multipartRequest, "applyReas"));
        param.setCpstAmnt(ServletRequestUtils.getStringParameter(multipartRequest, "cpstAmnt"));
        
        param.setApplrName(ServletRequestUtils.getStringParameter(multipartRequest, "applrName"));
        param.setApplrResdCorpNumb(ServletRequestUtils.getStringParameter(multipartRequest, "applrResdCorpNumb"));
        param.setApplrAddr(ServletRequestUtils.getStringParameter(multipartRequest, "applrAddr"));
        param.setApplrTelx(ServletRequestUtils.getStringParameter(multipartRequest, "applrTelx"));
        
        param.setApplyProxyName(ServletRequestUtils.getStringParameter(multipartRequest, "applyProxyName"));
        param.setApplyProxyResdCorpNumb(ServletRequestUtils.getStringParameter(multipartRequest, "applyProxyResdCorpNumb"));
        param.setApplyProxyAddr(ServletRequestUtils.getStringParameter(multipartRequest, "applyProxyAddr"));
        param.setApplyProxyTelx(ServletRequestUtils.getStringParameter(multipartRequest, "applyProxyTelx"));
        
        param.setApplyRawCd(ServletRequestUtils.getIntParameter(multipartRequest, "applyRawCd", 0));
        param.setStatCd(4);		// 진행상황 : 보완임시저장
        param.setRgstIdnt(ServletRequestUtils.getStringParameter(multipartRequest, "rgstIdnt"));
        
        param.setFileNameCd(ServletRequestUtils.getStringParameters(multipartRequest, "fileNameCd"));
        param.setFileDelYn(ServletRequestUtils.getStringParameters(multipartRequest, "fileDelYn"));
        param.setHddnFile(ServletRequestUtils.getStringParameters(multipartRequest, "hddnFile"));
        param.setFileName(ServletRequestUtils.getStringParameters(multipartRequest, "fileName"));
        param.setRealFileName(ServletRequestUtils.getStringParameters(multipartRequest, "realFileName"));
        param.setFileSize(ServletRequestUtils.getStringParameters(multipartRequest, "fileSize"));
        param.setFilePath(ServletRequestUtils.getStringParameters(multipartRequest, "filePath"));
        
        

		/* 첨부화일 업로드 시작 */
        int[] attcSeqn = ServletRequestUtils.getIntParameters(multipartRequest, "attcSeqn");
		String[] fileNameCd = ServletRequestUtils.getStringParameters(multipartRequest, "fileNameCd");
		String[] fileDelYn = ServletRequestUtils.getStringParameters(multipartRequest, "fileDelYn");
		String[] hddnFile = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFile");
		
		//첨부파일 중 수정 된 첨부파일에 대한 정보 수집
		List delFileList = new ArrayList();
		for(int i = 0; i < attcSeqn.length; i++) {
			File delFileInfo = new File();
			if(fileDelYn[i].equals("Y")) {
				if(attcSeqn[i] == 0) {
					break;
				}
				delFileInfo.setAttcSeqn(attcSeqn[i]);
				delFileInfo.setFileAttcCd("ST");
				delFileInfo.setFileNameCd(fileNameCd[i]);
				delFileInfo.setApplyWriteYmd(applyWriteYmd);
				delFileInfo.setApplyWriteSeq(applyWriteSeq);
				
				delFileList.add(delFileInfo);
			}
		}
		param.setDelFileList(delFileList);

		//새로 첨부된 첨부파일에 대한 정보 수집
		List fileList = new ArrayList();
		
		Iterator fileNameIterator = multipartRequest.getFileNames();
		
		int newAttcSeqn = commonService.getNewAttcSeqn();
		
		while(fileNameIterator.hasNext()) {
			MultipartFile multiFile= multipartRequest.getFile((String)fileNameIterator.next());
			if(multiFile.getSize()>0) {
				PrpsAttc uploadInfo = FileUploadUtil.uploadPrpsFiles(multiFile, realUploadPath);
				File fileInfo = new File();
				for(int fm = 0; fm < hddnFile.length; fm++){
					if(hddnFile[fm].toUpperCase().equals(uploadInfo.getFILE_NAME().toUpperCase())){
						fileInfo.setAttcSeqn(newAttcSeqn);
						fileInfo.setFileAttcCd("ST");
						fileInfo.setFileNameCd(fileNameCd[fm]);
						fileInfo.setFileName(hddnFile[fm]);
						fileInfo.setFilePath(uploadInfo.getFILE_PATH());
						fileInfo.setFileSize(Integer.parseInt(uploadInfo.getFILE_SIZE()));
						fileInfo.setRealFileName(uploadInfo.getREAL_FILE_NAME());
						fileInfo.setApplyWriteYmd(applyWriteYmd);
						fileInfo.setApplyWriteSeq(applyWriteSeq);
						fileInfo.setRgstIdnt(ServletRequestUtils.getStringParameter(multipartRequest, "rgstIdnt"));
						
						hddnFile[fm] = "";
						break;
					}
				}
				
				fileList.add(fileInfo);
			}
			newAttcSeqn++;
		}
		
		param.setFileList(fileList);
        
		myStatService.updateStatPrps(request, param);
        
		ModelAndView mv = new ModelAndView("myStat/statPrps_step2");
		if(actionDiv.equals("goStep2")) {
			if(applyWorksCnt < 6) {
				srchParam.setApplyWriteYmd(applyWriteYmd);
				srchParam.setApplyWriteSeq(applyWriteSeq);
				
				// 공통코드 조회 시작
				// 신청서구분 코드 조회
				CodeList code = new CodeList();
				code.setBigCode("50");
				List applyTypeList = codeListService.commonCodeList(code);
	
				// 공표방법 코드 조회
				code.setBigCode("52");
				List publMediList = codeListService.commonCodeList(code);
	
				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd(applyWriteYmd);
				statApplyWorks.setApplyWriteSeq(applyWriteSeq);
				
				List statApplyWorksList = myStatService.statApplyWorksList(statApplyWorks);
				
				mv = new ModelAndView("myStat/statPrps_step2", "statApplyWorksList", statApplyWorksList);
				mv.addObject("statCd", 4);
				mv.addObject("applyWorksCnt", applyWorksCnt);
				mv.addObject("srchParam", srchParam);
				mv.addObject("applyTypeList", applyTypeList);
				mv.addObject("publMediList", publMediList);
				mv.addObject("existYn", existYn);
				
			}else{
				srchParam.setApplyWriteYmd(applyWriteYmd);
				srchParam.setApplyWriteSeq(applyWriteSeq);

				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd(applyWriteYmd);
				statApplyWorks.setApplyWriteSeq(applyWriteSeq);
				myStatService.deleteStatApplyWorks(statApplyWorks);
				
				// 공통코드 조회 시작
				// 신청서구분 코드 조회
				CodeList code = new CodeList();
				code.setBigCode("50");
				List applyTypeList = codeListService.commonCodeList(code);

				// 공표방법 코드 조회
				code.setBigCode("52");
				List publMediList = codeListService.commonCodeList(code);

				Map params = new HashMap();
				User user = SessionUtil.getSession(request);
				params.put("userIdnt", user.getUserIdnt());
				
				// 사용자 조회
				Map userInfo = userService.selectUserInfo(params);

				StatApplication param2 = new StatApplication();
				param2.setApplyWriteYmd(applyWriteYmd);
				param2.setApplyWriteSeq(applyWriteSeq);
				
				StatApplication statApplication = myStatService.statApplication(param2);
				
				List fileList2 = myStatService.statAttcFile(param2);
				statApplication.setFileList(fileList2);
				
				List statApplyWorksList = myStatService.statApplyWorksList(statApplyWorks);
				
				mv = new ModelAndView("myStat/statPrps_step3");
				mv.addObject("userInfo", userInfo);
				mv.addObject("statApplication", statApplication);
				mv.addObject("statApplyWorksList", statApplyWorksList);
				mv.addObject("srchParam", srchParam);
				mv.addObject("applyTypeList", applyTypeList);
				mv.addObject("publMediList", publMediList);
			}
			
		}else if(actionDiv.equals("goList")) {
			mv = new ModelAndView("redirect:/myStat/statRsltInqrList.do");
		}
		
		return mv;
	}
	

	//법정허락 신청 step2 보완임시저장
	@RequestMapping(value = "/statPrps2SaveDo")
	public ModelAndView statPrps2SaveDo(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		StatApplication srchParam = new StatApplication();
		/* 조회조건 str */
		srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(request, "srchApplyType"));
		srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(request, "srchApplyFrDt"));
		srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(request, "srchApplyToDt"));
		srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(request, "srchApplyWorksTitl"));
		srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(request, "srchStatCd", 0));
        srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		/* 조회조건 end */
        
        String existYn = ServletRequestUtils.getStringParameter(request, "existYn");	//이용신청명세서 첨부여부
		
        String actionDiv = ServletRequestUtils.getStringParameter(request, "action_div");

        String applyWriteYmd = ServletRequestUtils.getStringParameter(request, "applyWriteYmd");
		int applyWriteSeq = ServletRequestUtils.getIntParameter(request, "applyWriteSeq", 0);
		
		String applyType01        = request.getParameter("applyType01");
		String applyType02        = request.getParameter("applyType02");
		String applyType03        = request.getParameter("applyType03");
		String applyType04        = request.getParameter("applyType04");
		String applyType05        = request.getParameter("applyType05");
		String[] worksTitl          = request.getParameterValues("worksTitl");
		String[] worksKind          = request.getParameterValues("worksKind");
		String[] worksForm          = request.getParameterValues("worksForm");
		String[] worksDesc          = request.getParameterValues("worksDesc");
		String[] publYmd            = request.getParameterValues("publYmd");
		String[] publNatn           = request.getParameterValues("publNatn");
		String publMediCd            = request.getParameter("publMediCd");
		String[] publMediEtc		= request.getParameterValues("publMediEtc");
		String[] publMedi           = request.getParameterValues("publMedi");
		String[] coptHodrName       = request.getParameterValues("coptHodrName");
		String[] coptHodrTelxNumb   = request.getParameterValues("coptHodrTelxNumb");
		String[] coptHodrAddr       = request.getParameterValues("coptHodrAddr");

		List paramList = new ArrayList();
		for(int i = 0; i < worksTitl.length; i++) {
			StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
			
			statApplyWorksInfo.setApplyWriteYmd(applyWriteYmd);
			statApplyWorksInfo.setApplyWriteSeq(applyWriteSeq);
			statApplyWorksInfo.setWorksSeqn(i+1);
			statApplyWorksInfo.setApplyType01(StringUtil.nullToEmpty(applyType01.substring(i, i+1)));
			statApplyWorksInfo.setApplyType02(StringUtil.nullToEmpty(applyType02.substring(i, i+1)));
			statApplyWorksInfo.setApplyType03(StringUtil.nullToEmpty(applyType03.substring(i, i+1)));
			statApplyWorksInfo.setApplyType04(StringUtil.nullToEmpty(applyType04.substring(i, i+1)));
			statApplyWorksInfo.setApplyType05(StringUtil.nullToEmpty(applyType05.substring(i, i+1)));
			statApplyWorksInfo.setWorksTitl(StringUtil.nullToEmpty(worksTitl[i]));
			statApplyWorksInfo.setWorksKind(StringUtil.nullToEmpty(worksKind[i]));
			statApplyWorksInfo.setWorksForm(StringUtil.nullToEmpty(worksForm[i]));
			statApplyWorksInfo.setWorksDesc(StringUtil.nullToEmpty(worksDesc[i]));
			statApplyWorksInfo.setPublYmd(StringUtil.nullToEmpty(publYmd[i]));
			statApplyWorksInfo.setPublNatn(StringUtil.nullToEmpty(publNatn[i]));
			String newPublMediCd = publMediCd.substring(0, publMediCd.indexOf('!'));
			publMediCd = publMediCd.substring(publMediCd.indexOf('!')+1);
			statApplyWorksInfo.setPublMediCd(Integer.parseInt(newPublMediCd));
			statApplyWorksInfo.setPublMediEtc(StringUtil.nullToEmpty(publMediEtc[i]));
			statApplyWorksInfo.setPublMedi(StringUtil.nullToEmpty(publMedi[i]));
			statApplyWorksInfo.setCoptHodrName(StringUtil.nullToEmpty(coptHodrName[i]));
			statApplyWorksInfo.setCoptHodrTelxNumb(StringUtil.nullToEmpty(coptHodrTelxNumb[i]));
			statApplyWorksInfo.setCoptHodrAddr(StringUtil.nullToEmpty(coptHodrAddr[i]));
			
			paramList.add(statApplyWorksInfo);
		}
		//기존에 등록된 이용승인 신청 명세서 삭제 및 신규 등록
		myStatService.insertStatApplyWorksList(paramList);
		
		ModelAndView mv = new ModelAndView("myStat/statPrps_step3");
		if(actionDiv.equals("goStep3")) {

			if(existYn.equals("Y")){
				srchParam.setApplyWriteYmd(applyWriteYmd);
				srchParam.setApplyWriteSeq(applyWriteSeq);
	
				// 공통코드 조회 시작
				// 신청서구분 코드 조회
				CodeList code = new CodeList();
				code.setBigCode("50");
				List applyTypeList = codeListService.commonCodeList(code);
	
				// 공표방법 코드 조회
				code.setBigCode("52");
				List publMediList = codeListService.commonCodeList(code);
	
				Map params = new HashMap();
				User user = SessionUtil.getSession(request);
				params.put("userIdnt", user.getUserIdnt());
				
				// 사용자 조회
				Map userInfo = userService.selectUserInfo(params);
	
				StatApplication param = new StatApplication();
				param.setApplyWriteYmd(ServletRequestUtils.getStringParameter(request, "applyWriteYmd"));
				param.setApplyWriteSeq(ServletRequestUtils.getIntParameter(request, "applyWriteSeq", 0));
				
				StatApplication statApplication = myStatService.statApplication(param);
				
				List fileList2 = myStatService.statAttcFile(param);
				statApplication.setFileList(fileList2);
				
				StatApplyWorks statApplyWorks = new StatApplyWorks();
				statApplyWorks.setApplyWriteYmd(applyWriteYmd);
				statApplyWorks.setApplyWriteSeq(applyWriteSeq);
				
				List statApplyWorksList = myStatService.statApplyWorksList(statApplyWorks);
				
				mv = new ModelAndView("myStat/statPrps_step3");
				mv.addObject("userInfo", userInfo);
				mv.addObject("statApplication", statApplication);
				mv.addObject("statApplyWorksList", statApplyWorksList);
				mv.addObject("srchParam", srchParam);
				mv.addObject("applyTypeList", applyTypeList);
				mv.addObject("publMediList", publMediList);
				
			}else{
				mv = new ModelAndView("redirect:/myStat/statPrpsModi.do?srchApplyWorksTitl="+srchParam.getSrchApplyWorksTitl());
				mv.addObject("srchApplyType", srchParam.getSrchApplyType());
				mv.addObject("srchApplyFrDt", srchParam.getSrchApplyFrDt());
				mv.addObject("srchApplyToDt", srchParam.getSrchApplyToDt());
//				mv.addObject("srchApplyWorksTitl", srchParam.getSrchApplyWorksTitl());
				mv.addObject("srchStatCd", srchParam.getSrchStatCd());
				mv.addObject("page_no", srchParam.getNowPage());
				mv.addObject("stat_cd", ServletRequestUtils.getStringParameter(request, "stat_cd"));
				mv.addObject("apply_write_ymd", applyWriteYmd);
				mv.addObject("apply_write_seq", applyWriteSeq);
			}
			
		}else if(actionDiv.equals("goList")) {
			mv = new ModelAndView("redirect:/myStat/statRsltInqrList.do");
		}
		
		return mv;
	}
	
	
	//법정허락 신청 실행
	@RequestMapping(value = "/statPrpsInsertDo")
	public ModelAndView statPrpsInsertDo(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

        String applyWriteYmd = ServletRequestUtils.getStringParameter(request, "applyWriteYmd");
		int applyWriteSeq = ServletRequestUtils.getIntParameter(request, "applyWriteSeq", 0);
		
		ApplySign applySign = new ApplySign();
		
		//인증정보가 존재하는 경우 전자서명 확인 로직 수행
		if(!request.getParameter("certId").equals("") && !request.getParameter("certId").equals("null") && !request.getParameter("certId").equals(null)){
			/*************** 공인 인증서 전자서명 확인 ********************/
			SignCertUtil signCertUtil = new SignCertUtil();		// 전자인증 확인을 위한 Utility
			
			request.setCharacterEncoding("euc-kr");	// EUC-KR로 설정
			
			SignDTO signDTO = new SignDTO();	// 전자인증 정보 저장용 DTO

			signDTO.setChallenge(ServletRequestUtils.getStringParameter(request,"Challenge"));
			signDTO.setMemberType(ServletRequestUtils.getStringParameter(request,"memberType"));
			signDTO.setSsnNo(ServletRequestUtils.getStringParameter(request,"ssnNo"));
			signDTO.setCrnNo(ServletRequestUtils.getStringParameter(request,"corpNo"));
			signDTO.setCertId(ServletRequestUtils.getStringParameter(request,"certId"));
			signDTO.setUserSignCert(ServletRequestUtils.getStringParameter(request,"userSignCert"));
			signDTO.setOriginalMessage(ServletRequestUtils.getStringParameter(request,"originalMessage"));
			signDTO.setUserSignValue(ServletRequestUtils.getStringParameter(request,"userSignValue"));
			signDTO.setEncryptedSessionKey(ServletRequestUtils.getStringParameter(request,"encryptedSessionKey"));
			signDTO.setEncryptedUserRandomNumber(ServletRequestUtils.getStringParameter(request,"encryptedUserRandomNumber"));
			signDTO.setEncryptedUserSSN(ServletRequestUtils.getStringParameter(request,"encryptedUserSSN"));
						
			// 전자서명 인증 확인 
			if(!signCertUtil.chkSignCert(request, signDTO)){
				log.debug("#################singCertMessage=###"+signCertUtil.getSignCertMessage()+"#######################");
				request.getSession(true).setAttribute(SignContants.SIGN_VALIDATE, signCertUtil.getSignCertMessage());
				
				ModelAndView mvError = null;
				
				//일반음반
				mvError = new ModelAndView("myStat/statPrps_step3");
				return mvError;			
			}

			request.getSession(true).removeAttribute(SignContants.SIGN_VALIDATE);
			
			/* 
			 * 전자인증정보에서 DN, SerialNumber 정보 추출 > TA_TMP_MUSIC_APPLY_CONFIRM
			 * 
			 * signCertUtil.getCertDN();	// DN
			 * signCertUtil.getSerialNumber();	// SerialNumber
			 * signDTO.getUserSignCert();	// 인증서정보
			 * signCertUtil.getOriginalMessage();	// 원본 메시지
			 * signCertUtil.getSerialNumber();	// 전자서명값
			 */
			/**********************************************************/			
			applySign.setSignDn(signCertUtil.getCertDN());
			applySign.setSignSn(signCertUtil.getSerialNumber());
			applySign.setSignCert(signDTO.getUserSignCert());
			applySign.setSignOrgnData(signCertUtil.getOriginalMessage());
			applySign.setSignDate(signCertUtil.getSignValue());

		}
		
		StatApplication param = new StatApplication();
		param.setApplyWriteYmd(ServletRequestUtils.getStringParameter(request, "applyWriteYmd"));
		param.setApplyWriteSeq(ServletRequestUtils.getIntParameter(request, "applyWriteSeq", 0));
		param.setStatCd(ServletRequestUtils.getIntParameter(request, "stat_cd", 2));
        param.setRgstIdnt(ServletRequestUtils.getStringParameter(request, "rgstIdnt"));
		
		//신규 신청 번호 생성 str
		DecimalFormat df = new DecimalFormat("00");
		Calendar currentCal = Calendar.getInstance();
		currentCal.add(currentCal.DATE, 0);
        String newApplyWriteYmd = Integer.toString(currentCal.get(Calendar.YEAR)) + df.format(currentCal.get(Calendar.MONTH)+1) + df.format(currentCal.get(Calendar.DAY_OF_MONTH));
        
        int newApplyWriteSeq = myStatService.newApplyWriteSeq("");
        
        param.setNewApplyWriteYmd(newApplyWriteYmd);
        param.setNewApplyWriteSeq(newApplyWriteSeq);
        
        applySign.setApplyWriteYmd(newApplyWriteYmd);
        applySign.setApplyWriteSeq(newApplyWriteSeq);
        param.setApplySign(applySign);
        //신규 신청번호 생성 end
        
        myStatService.insertStatPrps(param);
		
		// 메일 보내기
		Map params = new HashMap();
		User user = SessionUtil.getSession(request);
		params.put("userIdnt", user.getUserIdnt());
		
		// 사용자 조회
		Map userInfo = userService.selectUserInfo(params);


		StatApplication newParam = new StatApplication();
        newParam.setApplyWriteYmd(newApplyWriteYmd);
        newParam.setApplyWriteSeq(newApplyWriteSeq);
		
		StatApplication statApplication = myStatService.statApplication(newParam);
		
		String statNm = "신청완료";
		
		String host = kr.or.copyright.mls.support.constant.Constants.MAIL_SERVER_IP; // smtp서버
		String to = userInfo.get("MAIL").toString(); // 수신EMAIL
		String toName = userInfo.get("USER_NAME").toString(); // 수신인
		String subject = "[저작권찾기] 법정허락 이용신청 "+statNm;
		
		String from = kr.or.copyright.mls.support.constant.Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
		String fromName = kr.or.copyright.mls.support.constant.Constants.SYSTEM_NAME;
		
		if (userInfo.get("MAIL_RECE_YSNO").equals("Y") && to != null && to != "") {
			
			// ------------------------- mail 정보
			// ----------------------------------//
			MailInfo info = new MailInfo();
			info.setFrom(from);
			info.setFromName(fromName);
			info.setHost(host);
			info.setEmail(to);
			info.setEmailName(toName);
			info.setSubject(subject);

			StringBuffer sb = new StringBuffer();
			
		    
		    
			sb.append("				<tr>");
			sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
			sb.append("					<p style=\"font-weight:bold; background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 법정허락 이용신청 "+statNm+" 정보</p>");
			sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
			sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자 이름 : "+toName+" </li>");	
			sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청번호 : "+ newApplyWriteYmd + "-" + newApplyWriteSeq+" </li>");
			sb.append("						<li style=\"background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : " + statApplication.getApplyWorksTitl()+" </li>");				
			sb.append("					</ul>");
			sb.append("					</td>");
			sb.append("				</tr>");

		    info.setMessage(sb.toString());
		    info.setSubject(subject);
			info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
			
			/*sb.append("<div class=\"mail_title\">" + toName + "님, 법정허락 이용신청이 "+statNm+" 되었습니다. </div>");
			sb.append("<div class=\"mail_contents\"> ");
			sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
			sb.append("		<tr> ");
			sb.append("			<td> ");
			sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
			// 변경부분
			sb.append("					<tr> ");
			sb.append("						<td class=\"tdLabel\">신청번호</td> ");
			sb.append("						<td class=\"tdData\">" + newApplyWriteYmd+"-"+newApplyWriteSeq + "</td> ");
			sb.append("					</tr> ");
			sb.append("					<tr> ");
			sb.append("						<td class=\"tdLabel\">제호(제목)</td> ");
			sb.append("						<td class=\"tdData\">" + statApplication.getApplyWorksTitl() + "</td> ");
			sb.append("					</tr> ");
			sb.append("				</table> ");
			sb.append("			</td> ");
			sb.append("		</tr> ");
			sb.append("	</table> ");
			sb.append("</div> ");
			
			info.setMessage(new String(MailMessage.getChangeInfoMailText(sb
					.toString())));*/
			MailManager manager = MailManager.getInstance();

			info = manager.sendMail(info);
			if (info.isSuccess()) {
				System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());
			} else {
				System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
			}
			
		}
		
		//관리자한테 메일 보내기
		HashMap userMap = new HashMap();
		
		//신청제목
		userMap.put("WORKS_TITL",statApplication.getApplyWorksTitl());
		
		//신청번호
		userMap.put("APPLY_NO",newApplyWriteYmd+"-"+newApplyWriteSeq);
		
		//사용자 정보 셋팅 
		userMap.put("USER_IDNT",user.getUserIdnt());
		userMap.put("USER_NAME",user.getUserName());
		userMap.put("U_MAIL",user.getMail());
		userMap.put("MOBL_PHON",user.getMoblPhon());
		
		userMap.put("MGNT_DIVS", "ST");
		userMap.put("MAIL_TITL", "법정허락 이용신청이 완료되었습니다.");
		
		List list = new ArrayList();
		
		list = commonService.getMgntMail(userMap);
		
		int listSize = list.size();
		
		String to1[] = new String[listSize];
	    	String toName1[] = new String[listSize];
	    	
	    	for(int i = 0; i<listSize; i++){
	    	    HashMap map = (HashMap) list.get(i);
	    	    toName1[i] = (String) map.get("USER_NAME");
	    	    to1[i] = (String) map.get("MAIL");
	    	}
		
	    	userMap.put("TO", to1);
	    	userMap.put("TO_NAME", toName1);
		
		SendMail.send(userMap);
		
		// sms 보내기
		String smsTo = userInfo.get("MOBL_PHON").toString();
		String smsFrom = kr.or.copyright.mls.support.constant.Constants.SYSTEM_TELEPHONE;
		
		String smsMessage = "[저작권찾기] 법정허락 이용신청이 "+statNm+" 되었습니다.";
		
		if (userInfo.get("SMS_RECE_YSNO") != null && userInfo.get("SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {

			SMSManager smsManager = new SMSManager();

			System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
		}
		
        
		return new ModelAndView("redirect:/myStat/statRsltInqrList.do");
	}
	
	
	//법정허락 신청 실행			//20120824 정병호
	@RequestMapping(value = "/statPrpsUpdate")
	public ModelAndView statPrpsUpdateDo(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		String applyWriteYmd = ServletRequestUtils.getStringParameter(request, "applyWriteYmd");
		int applyWriteSeq = ServletRequestUtils.getIntParameter(request, "applyWriteSeq", 0);
		
		StatApplication param = new StatApplication();
		param.setApplyWriteYmd(ServletRequestUtils.getStringParameter(request, "applyWriteYmd"));
		param.setApplyWriteSeq(ServletRequestUtils.getIntParameter(request, "applyWriteSeq", 0));
		param.setStatCd(ServletRequestUtils.getIntParameter(request, "stat_cd", 2));
		param.setRgstIdnt(ServletRequestUtils.getStringParameter(request, "rgstIdnt"));

        param.setNewApplyWriteYmd(applyWriteYmd);
        param.setNewApplyWriteSeq(applyWriteSeq);
        
//		applySign.setApplyWriteYmd(newApplyWriteYmd);
//		applySign.setApplyWriteSeq(newApplyWriteSeq);
//		param.setApplySign(applySign);
		System.out.println( " param : " + param );
		myStatService.updateStatPrps(param);
		
		// 메일 보내기
		Map params = new HashMap();
		User user = SessionUtil.getSession(request);
		params.put("userIdnt", user.getUserIdnt());
		
		// 사용자 조회
		Map userInfo = userService.selectUserInfo(params);

		StatApplication statApplication = myStatService.statApplication(param);
		
		String statNm = "보완완료";
		
		String host = kr.or.copyright.mls.support.constant.Constants.MAIL_SERVER_IP; // smtp서버
		String to = userInfo.get("MAIL").toString(); // 수신EMAIL
		String toName = userInfo.get("USER_NAME").toString(); // 수신인
		String subject = "[저작권찾기] 법정허락 이용신청 "+statNm;
		
		String from = kr.or.copyright.mls.support.constant.Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
		String fromName = kr.or.copyright.mls.support.constant.Constants.SYSTEM_NAME;
		
		if (userInfo.get("MAIL_RECE_YSNO").equals("Y") && to != null && to != "") {
			
			// ------------------------- mail 정보
			// ----------------------------------//
			MailInfo info = new MailInfo();
			info.setFrom(from);
			info.setFromName(fromName);
			info.setHost(host);
			info.setEmail(to);
			info.setEmailName(toName);
			info.setSubject(subject);

			StringBuffer sb = new StringBuffer();
			
			sb.append("<div class=\"mail_title\">" + toName + "님, 법정허락 이용신청이 "+statNm+" 되었습니다. </div>");
			sb.append("<div class=\"mail_contents\"> ");
			sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
			sb.append("		<tr> ");
			sb.append("			<td> ");
			sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
			// 변경부분
			sb.append("					<tr> ");
			sb.append("						<td class=\"tdLabel\">신청번호</td> ");
			sb.append("						<td class=\"tdData\">" + statApplication.getApplyWriteYmd()+"-"+statApplication.getApplyWriteSeq() + "</td> ");
			sb.append("					</tr> ");
			sb.append("					<tr> ");
			sb.append("						<td class=\"tdLabel\">제호(제목)</td> ");
			sb.append("						<td class=\"tdData\">" + statApplication.getApplyWorksTitl() + "</td> ");
			sb.append("					</tr> ");
			sb.append("				</table> ");
			sb.append("			</td> ");
			sb.append("		</tr> ");
			sb.append("	</table> ");
			sb.append("</div> ");
			
			info.setMessage(new String(MailMessage.getChangeInfoMailText(sb.toString())));
			MailManager manager = MailManager.getInstance();

			info = manager.sendMail(info);
			if (info.isSuccess()) {
				System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());
			} else {
				System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
			}
			
		}
		
		HashMap userMap = new HashMap();
		
		//신청제목
		userMap.put("WORKS_TITL",statApplication.getApplyWorksTitl());
		
		//신청번호
		userMap.put("APPLY_NO",statApplication.getApplyWriteYmd()+"-"+statApplication.getApplyWriteSeq());
		
		//사용자 정보 셋팅 
		userMap.put("USER_IDNT",user.getUserIdnt());
		userMap.put("USER_NAME",user.getUserName());
		userMap.put("U_MAIL",user.getMail());
		userMap.put("MOBL_PHON",user.getMoblPhon());
		
		userMap.put("MGNT_DIVS", "ST");
		userMap.put("MAIL_TITL", "법정허락 이용신청이 보완완료 되었습니다.");
		
		List list = new ArrayList();
		
		list = commonService.getMgntMail(userMap);
		
		int listSize = list.size();
		
		String to1[] = new String[listSize];
	    	String toName1[] = new String[listSize];
	    	
	    	for(int i = 0; i<listSize; i++){
	    	    HashMap map = (HashMap) list.get(i);
	    	    toName1[i] = (String) map.get("USER_NAME");
	    	    to1[i] = (String) map.get("MAIL");
	    	}
		
	    	userMap.put("TO", to1);
	    	userMap.put("TO_NAME", toName1);
		
		SendMail.send(userMap);	
		
		// sms 보내기
		String smsTo = userInfo.get("MOBL_PHON").toString();
		String smsFrom = kr.or.copyright.mls.support.constant.Constants.SYSTEM_TELEPHONE;
		
		String smsMessage = "[저작권찾기] 법정허락 이용신청이 "+statNm+" 되었습니다.";
		
		if (userInfo.get("SMS_RECE_YSNO") != null && userInfo.get("SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {

			SMSManager smsManager = new SMSManager();

			System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
		}
		
		return new ModelAndView("redirect:/myStat/statRsltInqrList.do");
	}
	
	
	//법정허락 신청 상세 조회			//정병호 20120823 수정
	@RequestMapping(value = "/statPrpsView")
	public ModelAndView statPrpsDetl(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		StatApplication srchParam = new StatApplication();
		/* 조회조건 str */
		srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(request, "srchApplyType"));
		srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(request, "srchApplyFrDt"));
		srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(request, "srchApplyToDt"));
		srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(request, "srchApplyWorksTitl"));
		srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(request, "srchStatCd", 0));
        srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		/* 조회조건 end */
        
        String mode			= ServletRequestUtils.getStringParameter(request, "mode","");			// R : 레포팅화면
		String report			= ServletRequestUtils.getStringParameter(request, "report","");	// 레포팅 파일
		String modal 			= ServletRequestUtils.getStringParameter(request, "modal","0");	// 레포팅 파일 모달구분
		
		User user = SessionUtil.getSession(request);
		
		if(!mode.equals("R")){
			if(user == null){
				ModelAndView mv = new ModelAndView("redirect:/user/user.do?method=goSgInstall");
				return mv;
			}
		}
		
		// 공통코드 조회 시작
		// 신청서구분 코드 조회
		CodeList code = new CodeList();
		code.setBigCode("50");
		List applyTypeList = codeListService.commonCodeList(code);
		
		// 공표방법 코드 조회
		code.setBigCode("52");
		List publMediList = codeListService.commonCodeList(code);

//		Map params = new HashMap();
//		User user = SessionUtil.getSession(request);
//		params.put("userIdnt", user.getUserIdnt());
//		
//		// 사용자 조회
//		Map userInfo = userService.selectUserInfo(params);
		
		//신청서 상태
		int statCd = ServletRequestUtils.getIntParameter(request, "stat_cd", 0);

		String applyWriteYmd = ServletRequestUtils.getStringParameter(request, "apply_write_ymd");
		int applyWriteSeq = ServletRequestUtils.getIntParameter(request, "apply_write_seq", 0);
		
		
		StatApplication param = new StatApplication();
		param.setApplyWriteYmd(applyWriteYmd);
		param.setApplyWriteSeq(applyWriteSeq);
		
		//이용승인 신청서 조회
		StatApplication statApplication = myStatService.statApplication(param);
		
		//이용승인 신청서 첨부파일 조회
		List fileList = myStatService.statAttcFile(param);
		statApplication.setFileList(fileList);
		
		StatApplicationShis statApplicationShis = new StatApplicationShis();
		statApplicationShis.setApplyWriteYmd(applyWriteYmd);
		statApplicationShis.setApplyWriteSeq(applyWriteSeq);
		
		//이용승인 신청서 상태변경 내역 조회
		List sHisList = myStatService.statApplicationShisList(statApplicationShis);
		
		StatApplyWorks statApplyWorks = new StatApplyWorks();
		statApplyWorks.setApplyWriteYmd(applyWriteYmd);
		statApplyWorks.setApplyWriteSeq(applyWriteSeq);
		//이용승인 명세서 조회
		List statApplyWorksList = myStatService.statApplyWorksList(statApplyWorks);
		
		ModelAndView mv = new ModelAndView("myStat/statPrpsView");
		
		// 레포팅
		if(mode.equals("R")){
			mv = new ModelAndView(report);
			mv.addObject("modal", modal);
		}
		
		mv.addObject("statApplication", statApplication);
		mv.addObject("sHisList", sHisList);
		mv.addObject("statApplyWorksList", statApplyWorksList);
		mv.addObject("publMediList", publMediList);
		
		mv.addObject("srchParam", srchParam);
//		mv.addObject("userInfo", userInfo);
		mv.addObject("applyTypeList", applyTypeList);
		
		return mv;
	}
	
	
	//법정허락 신청 실행
	@RequestMapping(value = "/statPrpsCancel")
	public ModelAndView statPrpsCancelDo(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		String applyWriteYmd = ServletRequestUtils.getStringParameter(request, "applyWriteYmd");
		int applyWriteSeq = ServletRequestUtils.getIntParameter(request, "applyWriteSeq", 0);
		
		StatApplication param = new StatApplication();
		param.setApplyWriteYmd(ServletRequestUtils.getStringParameter(request, "applyWriteYmd"));
		param.setApplyWriteSeq(ServletRequestUtils.getIntParameter(request, "applyWriteSeq", 0));
		param.setStatCd(ServletRequestUtils.getIntParameter(request, "stat_cd", 2));
		param.setRgstIdnt(ServletRequestUtils.getStringParameter(request, "rgstIdnt"));

        param.setNewApplyWriteYmd(applyWriteYmd);
        param.setNewApplyWriteSeq(applyWriteSeq);
        
//		applySign.setApplyWriteYmd(newApplyWriteYmd);
//		applySign.setApplyWriteSeq(newApplyWriteSeq);
//		param.setApplySign(applySign);
		
		myStatService.updateStatPrps(param);
		
		return new ModelAndView("redirect:/myStat/statRsltInqrList.do");
	}

	
	//법정허락 신청 실행
	@RequestMapping(value = "/statPrpsDeleteDo")
	public ModelAndView statPrpsDeleteDo(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		String[] applyNo = ServletRequestUtils.getRequiredStringParameters(request, "chk");
		
		List delList = new ArrayList();
		for(int i = 0; i < applyNo.length; i++ ) {
			StatApplication param = new StatApplication();
			param.setApplyWriteYmd(applyNo[i].substring(1, applyNo[i].indexOf('-')));
			param.setApplyWriteSeq(Integer.parseInt(applyNo[i].substring(applyNo[i].indexOf('-')+1)));
			delList.add(param);
			System.out.println("###"+param.getApplyWriteYmd()+":::"+param.getApplyWriteSeq());
		}
		
		myStatService.deleteStatPrps(delList);
		
		return new ModelAndView("redirect:/myStat/statRsltInqrList.do");
	}
	
	
	//법정허락 신청 수정 화면_선택저작물		/20120822 정병호 수정
	@RequestMapping(value = "/statPrpsMainModi")
	public ModelAndView statPrpsModi(HttpServletRequest request,			HttpServletResponse respone) throws Exception {	    
		HttpSession session = request.getSession();
		
		session.removeAttribute("isCompleteStep01");
		session.removeAttribute("isCompleteStep01_1");
		session.removeAttribute("isCompleteStep01_2");
		session.removeAttribute("isCompleteStep02");
		session.removeAttribute("applyWriteYmd");
		session.removeAttribute("applyWriteSeq");

		StatApplication srchParam = new StatApplication();
		/* 조회조건 str */
		srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(request, "srchApplyType"));
		srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(request, "srchApplyFrDt"));
		srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(request, "srchApplyToDt"));
		srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(request, "srchApplyWorksTitl"));
		srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(request, "srchStatCd", 0));
		srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		/* 조회조건 end */

		int statCd = ServletRequestUtils.getIntParameter(request, "stat_cd", 2);
        
		Map params = new HashMap();
		User user = SessionUtil.getSession(request);
		params.put("userIdnt", user.getUserIdnt());
		
		// 사용자 조회
		Map userInfo = userService.selectUserInfo(params);
		
		StatApplication param = new StatApplication();
		param.setApplyWriteYmd(ServletRequestUtils.getStringParameter(request, "apply_write_ymd"));
		param.setApplyWriteSeq(ServletRequestUtils.getIntParameter(request, "apply_write_seq", 0));

		// 이용승인 신청서tmp 조회
		StatApplication statApplication = null;
		if(statCd == 1) {
			statApplication = myStatService.statApplicationTmp(param);
		}else{
			statApplication = myStatService.statApplication(param);
		}
		
		// 이용승인 신청서tmp 첨부파일 조회
		List fileList = null;
		if(statCd == 1) {
			fileList = myStatService.statAttcFileTmp(param);
		}else{
			fileList = myStatService.statAttcFile(param);
		}
		statApplication.setFileList(fileList);
		
		ModelAndView mv =new ModelAndView("stat/statPrpsMain");
		
		StatApplyWorks statApplyWorks = new StatApplyWorks();
		statApplyWorks.setApplyWriteYmd(param.getApplyWriteYmd());
		statApplyWorks.setApplyWriteSeq(param.getApplyWriteSeq());
		
		
		List statApplyWorksList = null;
		if(statCd == 1) {
		    statApplyWorksList = myStatService.statApplyWorksListTmp(statApplyWorks);
		}else{
		    statApplyWorksList = myStatService.statApplyWorksList(statApplyWorks);
		}
		
		String strWorksId = "";
		
		for(int i =0; i<statApplyWorksList.size(); i++){
		    HashMap worksObj = (HashMap) statApplyWorksList.get(i);
		    String worksId = worksObj.get("WORKSID").toString();
		    if(i==0){
			strWorksId+=worksId;
		    }else{
			strWorksId+=","+worksId;
		    }
		}
		strWorksId = strWorksId.replaceAll(" ", ""); 
		
		
		MlStatWorksDefaultVO defaultVO = new MlStatWorksDefaultVO();
		defaultVO.setSearchWorksId(strWorksId);
		
		boolean isEmpty = defaultVO.getSearchWorksId().equals("");

            	if (!isEmpty) {
            	    mv.addObject("mlStatWorksList",	  statService.checkSelectMlStatWorksList(defaultVO));
            	}
		mv.addObject("srchParam", srchParam);
		mv.addObject("statApplication", statApplication);
		mv.addObject("userInfo", userInfo);
		mv.addObject("isMy","Y");
		mv.addObject("isModi","02");
		mv.addObject("worksId", strWorksId);
		
		return mv;
	}
	
	//법정허락 신청 수정 화면_개인저작물		/20120822 정병호 수정
	@RequestMapping(value = "/statPrpsMainModi_priv")
	public ModelAndView statPrpsModi_priv(HttpServletRequest request,HttpServletResponse respone) throws Exception {
	    
		HttpSession session = request.getSession();
		
		session.removeAttribute("isCompleteStep01");
		session.removeAttribute("isCompleteStep01_1");
		session.removeAttribute("isCompleteStep01_2");
		session.removeAttribute("isCompleteStep02");
		session.removeAttribute("applyWriteYmd");
		session.removeAttribute("applyWriteSeq");

		StatApplication srchParam = new StatApplication();
		/* 조회조건 str */
		srchParam.setSrchApplyType(ServletRequestUtils.getStringParameter(request, "srchApplyType"));
		srchParam.setSrchApplyFrDt(ServletRequestUtils.getStringParameter(request, "srchApplyFrDt"));
		srchParam.setSrchApplyToDt(ServletRequestUtils.getStringParameter(request, "srchApplyToDt"));
		srchParam.setSrchApplyWorksTitl(ServletRequestUtils.getStringParameter(request, "srchApplyWorksTitl"));
		srchParam.setSrchStatCd(ServletRequestUtils.getIntParameter(request, "srchStatCd", 0));
        srchParam.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		/* 조회조건 end */

        	int statCd = ServletRequestUtils.getIntParameter(request, "stat_cd", 2);
        
		Map params = new HashMap();
		User user = SessionUtil.getSession(request);
		params.put("userIdnt", user.getUserIdnt());
		
		// 사용자 조회
		Map userInfo = userService.selectUserInfo(params);
		/*Map userInfo = null;*/
		
		StatApplication param = new StatApplication();
		param.setApplyWriteYmd(ServletRequestUtils.getStringParameter(request, "apply_write_ymd"));
		param.setApplyWriteSeq(ServletRequestUtils.getIntParameter(request, "apply_write_seq", 0));

		// 이용승인 신청서tmp 조회
		StatApplication statApplication = null;
		if(statCd == 1) {
			statApplication = myStatService.statApplicationTmp(param);
		}else{
			statApplication = myStatService.statApplication(param);
		}
		
		// 이용승인 신청서tmp 첨부파일 조회
		List fileList = null;
		if(statCd == 1) {
			fileList = myStatService.statAttcFileTmp(param);
		}else{
			fileList = myStatService.statAttcFile(param);
		}
		statApplication.setFileList(fileList);

		StatApplicationShis statApplicationShis = new StatApplicationShis();
		statApplicationShis.setApplyWriteYmd(param.getApplyWriteYmd());
		statApplicationShis.setApplyWriteSeq(param.getApplyWriteSeq());
		List sHisList = myStatService.statApplicationShisList(statApplicationShis);
		ModelAndView mv =new ModelAndView("stat/statPrpsMain_priv");

		mv.addObject("srchParam", srchParam);
		mv.addObject("statApplication", statApplication);
		mv.addObject("sHisList", sHisList);
		mv.addObject("userInfo", userInfo);
		mv.addObject("isMy","Y");
		mv.addObject("isModi","02");
		
		return mv;
	}
	
	// 법정허락 결제창 호출
	@RequestMapping(value = "/statPrps_cPay")
	public ModelAndView statPrps_cPay(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		String applyWriteYmd = ServletRequestUtils.getStringParameter(request, "apply_write_ymd");
		String applyWriteSeq = ServletRequestUtils.getStringParameter(request, "apply_write_seq");
		String applyWorksCnt = ServletRequestUtils.getStringParameter(request, "apply_works_cnt");
		String worksTitl = ServletRequestUtils.getStringParameter(request, "worksTitl");
		
		ModelAndView mv =new ModelAndView("myStat/statPrps_cPay");
		mv.addObject("applyWriteYmd", applyWriteYmd);
		mv.addObject("applyWriteSeq", applyWriteSeq);
		mv.addObject("applyWorksCnt", applyWorksCnt);
		mv.addObject("worksTitl", worksTitl);
		
		return mv;
		
	}
	
	// 법정허락 결제정보 등록
	@RequestMapping(value = "/statPrps_insertPay")
	public ModelAndView statPrps_insertPay(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		User user = SessionUtil.getSession(request);
		if(user == null){
			ModelAndView mv = new ModelAndView("/user/user.do?method=goSgInstall");
			return mv;
		}
		
		StatApplication param = new StatApplication();
		
		param.setApplyWriteYmd(ServletRequestUtils.getStringParameter(request, "applyWriteYmd"));
		param.setApplyWriteSeq(ServletRequestUtils.getIntParameter(request, "applyWriteSeq"));
		param.setLgdMid(ServletRequestUtils.getStringParameter(request, "LGD_MID"));
		param.setLgdBuyer(user.getUserName());
		param.setLgdAmount(ServletRequestUtils.getStringParameter(request, "LGD_AMOUNT"));
		param.setLgdOid(ServletRequestUtils.getStringParameter(request, "LGD_OID"));
		param.setLgdTimestamp(ServletRequestUtils.getStringParameter(request, "LGD_TIMESTAMP"));
		
		myStatService.insertStatPayStatus(param);
		
		PrintWriter out = response.getWriter();
		out.print("true");
		out.close();
		
		return null;
		
	}
	
	// 법정허락 결제완료 처리
	@RequestMapping(value = "/statPrps_successPay")
	public ModelAndView statPrps_successPay(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		User user = SessionUtil.getSession(request);
		if(user == null){
			ModelAndView mv = new ModelAndView("/user/user.do?method=goSgInstall");
			return mv;
		}
		request.setCharacterEncoding("EUC-KR");
		StatApplication param = new StatApplication();
		
		param.setApplyWriteYmd(ServletRequestUtils.getStringParameter(request, "applyWriteYmd"));
		param.setApplyWriteSeq(ServletRequestUtils.getIntParameter(request, "applyWriteSeq"));
		param.setLgdRespcode(ServletRequestUtils.getStringParameter(request, "LGD_RESPCODE"));
		param.setLgdRespmsg(ServletRequestUtils.getStringParameter(request, "LGD_RESPMSG"));
		param.setApplyWorksTitl(ServletRequestUtils.getStringParameter(request, "worksTitl"));
		
		if(param.getLgdRespcode().equals("0000")){  // 결제성공
			
			param.setLgdTid(ServletRequestUtils.getStringParameter(request, "LGD_TID")); // LG유플러스 거래번호
			param.setStatCd(12);  // 결제 완료
			param.setRgstIdnt(user.getUserIdnt());
			param.setNewApplyWriteYmd(param.getApplyWriteYmd());
			param.setNewApplyWriteSeq(param.getApplyWriteSeq());
			
			myStatService.updateStatPrps(param);
			
			HashMap userMap = new HashMap();
			
			//신청제목
			userMap.put("WORKS_TITL",param.getApplyWorksTitl());
			
			//신청번호
			userMap.put("APPLY_NO",param.getApplyWriteYmd()+"-"+param.getApplyWriteSeq());
			
			//사용자 정보 셋팅 
			userMap.put("USER_IDNT",user.getUserIdnt());
			userMap.put("USER_NAME",user.getUserName());
			userMap.put("U_MAIL",user.getMail());
			userMap.put("MOBL_PHON",user.getMoblPhon());
			
			userMap.put("MGNT_DIVS", "ST");
			userMap.put("MAIL_TITL", "법정허락 수수료 결제가 완료되었습니다.");
			
			List list = new ArrayList();
			
			list = commonService.getMgntMail(userMap);
			
			int listSize = list.size();
			
			String to1[] = new String[listSize];
		    	String toName1[] = new String[listSize];
		    	
		    	for(int i = 0; i<listSize; i++){
		    	    HashMap map = (HashMap) list.get(i);
		    	    toName1[i] = (String) map.get("USER_NAME");
		    	    to1[i] = (String) map.get("MAIL");
		    	}
			
		    	userMap.put("TO", to1);
		    	userMap.put("TO_NAME", toName1);
			
			SendMail.send(userMap);	
			
		}else{
			//param.setLgdRespmsg("결제실패");
		}
		
		myStatService.updateStatPayStatus(param);
		
		response.setContentType("text/html; charset=utf-8");
		
		StringBuffer sb = new StringBuffer();
		
		sb.delete(0, sb.length());
		sb.append("<script type=\"text/JavaScript\">");
		sb.append("opener.location.replace('/myStat/statRsltInqrList.do');");
		sb.append("window.close();");
		sb.append("</script>");
		
		PrintWriter out = response.getWriter();
		out.print(sb);
		out.close();
		
		//ModelAndView mv = new ModelAndView();
		
		
		return null;
//		PrintWriter out = response.getWriter();
//		out.print("true");
//		out.close();
//		
//		return null;
		
	}
}

