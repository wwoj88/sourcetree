package kr.or.copyright.mls.user.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.user.model.ZipCodeRoad;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class UserSqlMapDao extends SqlMapClientDaoSupport implements UserDao {

	public Map findUserList(Map params) {
		return (Map) getSqlMapClientTemplate().queryForObject("User.getUserList", params);
	}
	
	public List findPostNumb(Map params) {
		return (List) getSqlMapClientTemplate().queryForList("User.getPostNumb", params);
	}

	public List findPostRoadNumb(Map params) {
		return (List) getSqlMapClientTemplate().queryForList("User.getPostRoadNumb", params);
	}
	
	public Map findUserIdnt(Map params) {
		return (Map) getSqlMapClientTemplate().queryForObject("User.getUserIdnt", params);
	}
	
	public Map findClmsUserIdnt1(Map params) {
		return (Map) getSqlMapClientTemplate().queryForObject("User.getClmsUserIdnt1", params);
	}
	
	public Map findClmsUserIdnt2(Map params) {
		return (Map) getSqlMapClientTemplate().queryForObject("User.getClmsUserIdnt2", params);
	}
	
	public void insertUser(Map params) {
		getSqlMapClientTemplate().insert("User.insertUserInfo", params);
	}
	
	public Map selectUserInfo(Map params) {
		return (Map) getSqlMapClientTemplate().queryForObject("User.selectUserInfo", params);
	}
	
	public void updateUserInfo(Map params) {
		getSqlMapClientTemplate().update("User.updateUserInfo", params);
	}
	/* 양재석 추가 start */
	public Map getInmtUserInfo(Map params) {
		return (Map) getSqlMapClientTemplate().queryForObject("User.getInmtUserInfo", params);
	}
	/* 양재석 추가 end */
	public Map userIdntPswdSrch(Map params) {

//		if ("Y".equals(params.get("pswdSrch"))) {
//			return (Map) getSqlMapClientTemplate().queryForObject("User.userPswdSrch", params);
//		} else {
//			return (Map) getSqlMapClientTemplate().queryForObject("User.userIdntSrch", params);
//		}
		return (Map) getSqlMapClientTemplate().queryForObject("User.userIdntSrch", params);
	}
	
	public Map userPswdSrch(Map params) {

		return (Map) getSqlMapClientTemplate().queryForObject("User.userPswdSrch", params);
	}
	
	public void detlUserInfo(Map params) {
		getSqlMapClientTemplate().update("User.detlUserInfo", params);
	}
	
	public Map selectClmsUserInfo(Map params) {
		return (Map) getSqlMapClientTemplate().queryForObject("User.selectClmsUserInfo", params);
	}
	
	public void insertClmsUserInfo(Map params) {
		getSqlMapClientTemplate().insert("User.insertClmsUserInfo", params);
	}
	
	public void insertClmsUserInfo2(Map params) {
		getSqlMapClientTemplate().insert("User.insertClmsUserInfo2", params);
	}
	
	public void updateClmsUserInfo(Map params) {
		getSqlMapClientTemplate().update("User.updateClmsUserInfo", params);
	}

	@SuppressWarnings("unchecked")
	public List<ZipCodeRoad> getInitRoadCodeList() {
	    return (List<ZipCodeRoad>)getSqlMapClientTemplate().queryForList("User.getInitRoadCodeList");
	}

	@SuppressWarnings("unchecked")
	public List<ZipCodeRoad> getRoadCodeList(Map params) {
	    return (List<ZipCodeRoad>)getSqlMapClientTemplate().queryForList("User.getRoadCodeList",params);
	}

	public void updateUserPswd(Map params) {
		getSqlMapClientTemplate().update("User.updateUserPswd", params);
	}

	public void updateRegiSsnNo(Map params) {
	    getSqlMapClientTemplate().update("User.updateRegiSsnNo", params);
	}

	public void updateUserPswdInfo(Map userMap) {
		getSqlMapClientTemplate().update("User.updateUserPswdInfo", userMap);
		
	}
	
	public List findSchCommName(Map params) {
		return (List) getSqlMapClientTemplate().queryForList("User.findSchCommName", params);
	}
	
	public int chkDupLoginId(Map params) {
		return (Integer)getSqlMapClientTemplate().queryForObject("User.chkDupLoginId", params);
	}
	
	public void insertAdmin(Map params) {
		getSqlMapClientTemplate().insert( "User.insertAdmin", params );
	}
	
	public int getNewTrstOrgnCd(){
		return (Integer)getSqlMapClientTemplate().queryForObject("User.getNewTrstOrgnCd");
	}

}
