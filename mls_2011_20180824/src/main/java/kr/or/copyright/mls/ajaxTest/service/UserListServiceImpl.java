package kr.or.copyright.mls.ajaxTest.service;

import java.util.List;

import javax.annotation.Resource;

import kr.or.copyright.mls.ajaxTest.dao.UserListDAO;

import org.springframework.stereotype.Service;

@Service("UserListBO")
public class UserListServiceImpl implements UserListService {

	@Resource(name="UserListDAO")
	private UserListDAO userListDAO;
	
	public List<String> getNameListForSuggest(String namePrefix) {
		return userListDAO.getNameListForSuggest(namePrefix);
	}
}
