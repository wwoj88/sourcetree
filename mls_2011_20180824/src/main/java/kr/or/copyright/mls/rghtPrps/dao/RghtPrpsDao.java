package kr.or.copyright.mls.rghtPrps.dao;

import java.util.List;

import kr.or.copyright.mls.rghtPrps.model.PrpsAttc;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

public interface RghtPrpsDao {
	
	public String getMuscRecFile(RghtPrps rghtPrps);
	
	public List muscPrpsList(RghtPrps rghtPrps);
	
	public int muscRghtCount(RghtPrps rghtPrps);
	
	public List muscRghtList(RghtPrps rghtPrps);
	
	public RghtPrps muscRghtDetail(RghtPrps rghtPrps);
		
	public List bookPrpsList(RghtPrps rghtPrps);
	
	public int bookRghtCount(RghtPrps rghtPrps);
	
	public List bookRghtList(RghtPrps rghtPrps);
	
	public RghtPrps bookRghtDetail(RghtPrps rghtPrps);
	
	// 방송대본
	public List scriptRghtList(RghtPrps rghtPrps);
	
	public int scriptRghtCount(RghtPrps rghtPrps);
	
	public RghtPrps scriptRghtDetail(RghtPrps rghtPrps);
	
	public List scriptPrpsList(RghtPrps rghtPrps);
	
	// 이미지 
	public int imageRghtCount(RghtPrps rghtPrps);
	
	public List imageRghtList(RghtPrps rghtPrps);
	
	public List imagePrpsList(RghtPrps rghtPrps);
	
	// 영화 
	public int mvieRghtCount(RghtPrps rghtPrps);
	
	public List mvieRghtList(RghtPrps rghtPrps);
	
	public List mviePrpsList(RghtPrps rghtPrps);
	
	public RghtPrps mvieRghtDetail(RghtPrps rghtPrps);
	
	// 방송
	public int broadcastRghtCount(RghtPrps rghtPrps);
	
	public List broadcastRghtList(RghtPrps rghtPrps);
	
	public RghtPrps broadcastRghtDetail(RghtPrps rghtPrps);
	
	public List broadcastPrpsList(RghtPrps rghtPrps);
	
	// 뉴스
	public int newsRghtCount(RghtPrps rghtPrps);
	
	public List newsRghtList(RghtPrps rghtPrps);
	
	public RghtPrps newsRghtDetail(RghtPrps rghtPrps);
	
	public List newsPrpsList(RghtPrps rghtPrps);
	
	public int prpsMastKey();
	
	public void prpsMasterInsert(RghtPrps rghtPrps);
	
	public void prpsMasterUpdate(RghtPrps rghtPrps);
	
	public void prpsRsltInsert(RghtPrps rghtPrps);
	
	public void prpsRsltRghtInsert(RghtPrps rghtPrps);
	
	public int getCrId();
	
	public int getNrId(RghtPrps rghtPrps);
	
	public int getAlbumId();
	
	public int getBookNrId(RghtPrps rghtPrps);
	
	public int getMovieMediaId(RghtPrps rghtPrps);
	
	public void worksInsert(RghtPrps rghtPrps);
	
	public void musicInsert(RghtPrps rghtPrps);
	
	public void albumInsert(RghtPrps rghtPrps);
	
	public void bookInsert(RghtPrps rghtPrps);
	
	public void scriptInsert(RghtPrps rghtPrps);
	
	public void imageInsert(RghtPrps rghtPrps);
	
	public void movieInsert(RghtPrps rghtPrps);
	
	public void movieMediaInsert(RghtPrps rghtPrps);
	
	public void broadcastInsert(RghtPrps rghtPrps);
	
	public void insertPrpsAttc(PrpsAttc prpsAttc);
	
	public List rghtPrpsHistList(RghtPrps rghtPrps);
	
	public void sideInsert(RghtPrps rghtPrps);
	
	public void newsInsert(RghtPrps rghtPrps);
}
