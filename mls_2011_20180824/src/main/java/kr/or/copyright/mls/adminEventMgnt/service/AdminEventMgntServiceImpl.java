package kr.or.copyright.mls.adminEventMgnt.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminEventMgnt.dao.AdminEventMgntDao;
import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.common.utils.FileUtil;

import com.tobesoft.platform.data.Dataset;

public class AdminEventMgntServiceImpl extends BaseService implements AdminEventMgntService {

	private AdminEventMgntDao adminEventMgntDao;
	
	public void setAdminEventMgntDao(AdminEventMgntDao adminEventMgntDao) {
		this.adminEventMgntDao = adminEventMgntDao;
	}

	//이벤트등록
	public void saveEvent() throws Exception {
		String seq = adminEventMgntDao.getSaveEventSeq();//시퀀스
		Dataset dsData = getDataset("ds_data");
		Map mData = getMap("ds_data", 0);
		
		//재등록일때
		if("edit".equals(mData.get("GUBUN"))){
			mData.put("OPER_YN", "N");
		}
		mData.put("EVENT_ID", seq);
		
		//파일업로드
		String fileName = dsData.getColumnAsString(0, "IMAGE_FILE_PATH");
		if ("Y".equals(dsData.getColumnAsString(0, "FILE_UP_YN"))) {
		//if (fileName != null && !"".equals(fileName)) {
			byte[] file = dsData.getColumn(0, "CLIENT_FILE").getBinary();
			Map upload = FileUtil.uploadMiFile(fileName, file, "event/");
			mData.put("IMAGE_FILE_PATH", upload.get("FILE_PATH")+""+upload.get("REAL_FILE_NAME"));
			mData.put("IMAGE_FILE_NAME", dsData.getColumnAsString(0, "IMAGE_FILE_NAME"));
		}
		
		String fileName2 = dsData.getColumnAsString(0, "IMAGE_FILE_PATH_WIN");
		if ("Y".equals(dsData.getColumnAsString(0, "FILE_UP_WIN_YN"))) {
		//if (fileName2 != null && !"".equals(fileName2)) {
			byte[] file = dsData.getColumn(0, "CLIENT_FILE_WIN").getBinary();
			Map upload = FileUtil.uploadMiFile(fileName2, file, "event/");
			mData.put("IMAGE_FILE_PATH_WIN", upload.get("FILE_PATH")+""+upload.get("REAL_FILE_NAME"));
			mData.put("IMAGE_FILE_NAME_WIN", dsData.getColumnAsString(0, "IMAGE_FILE_NAME_WIN"));
		}
		
		adminEventMgntDao.addEvent(mData);//등록
		
		Dataset dsAgree = getDataset("ds_agree");
		for (int i = 0; i < dsAgree.getRowCount(); i++) {
			Map mAgree = getMap("ds_agree",i);
			mAgree.put("EVENT_ID", seq);
			if ("Y".equals(mAgree.get("AGREEYN"))) {
				adminEventMgntDao.addEventAgree(mAgree);//동의항목 등록
			}
		}
	}

	//리스트
	public void getEventList() throws Exception {
		// DataSet return
		addList("ds_list", adminEventMgntDao.getEventList());
	}

	//리스트 이벤트 삭제
	public void delEventList() throws Exception {
		Dataset ds = getDataset("ds_delList");
		Map m = getMap("ds_delList");
		for (int i = 0; i < ds.getRowCount(); i++) {
			Map map = getMap("ds_delList",i);
			adminEventMgntDao.delEventList(map); //삭제상태 업데이트
		}
		getEventList();//리스트 호출
	}

	public void getEventDetl() throws Exception {
		Dataset ds = getDataset("ds_data");
		Map map = getMap("ds_data");
		addList("ds_data", adminEventMgntDao.getEventDetl(map));//상세화면데이터
		addList("rs_agree", adminEventMgntDao.getEventDetlAgree(map));//동의항목
	}

	public void updateEvent() throws Exception {
		Dataset dsData = getDataset("ds_data");
		Map mData = getMap("ds_data", 0);
		
		//파일업로드
		String fileName = dsData.getColumnAsString(0, "IMAGE_FILE_PATH");
		if ("Y".equals(dsData.getColumnAsString(0, "FILE_UP_YN"))) {
		//if (fileName != null && !"".equals(fileName)) {
			byte[] file = dsData.getColumn(0, "CLIENT_FILE").getBinary();
			Map upload = FileUtil.uploadMiFile(fileName, file, "event/");
			mData.put("IMAGE_FILE_PATH", upload.get("FILE_PATH")+""+upload.get("REAL_FILE_NAME"));
			mData.put("IMAGE_FILE_NAME", dsData.getColumnAsString(0, "IMAGE_FILE_NAME"));
		}
		
		String fileName2 = dsData.getColumnAsString(0, "IMAGE_FILE_PATH_WIN");
		if ("Y".equals(dsData.getColumnAsString(0, "FILE_UP_WIN_YN"))) {
		//if (fileName2 != null && !"".equals(fileName2)) {
			byte[] file = dsData.getColumn(0, "CLIENT_FILE_WIN").getBinary();
			Map upload = FileUtil.uploadMiFile(fileName2, file, "event/");
			mData.put("IMAGE_FILE_PATH_WIN", upload.get("FILE_PATH")+""+upload.get("REAL_FILE_NAME"));
			mData.put("IMAGE_FILE_NAME_WIN", dsData.getColumnAsString(0, "IMAGE_FILE_NAME_WIN"));
		}
		
		adminEventMgntDao.updateEvent(mData);//등록
		
		Dataset dsAgree = getDataset("ds_agree");
		adminEventMgntDao.delEventAgree(mData);//전체 삭제
		for (int i = 0; i < dsAgree.getRowCount(); i++) {
			Map mAgree = getMap("ds_agree",i);
			mAgree.put("EVENT_ID", mData.get("EVENT_ID"));
			if ("Y".equals(mAgree.get("AGREEYN"))) {
				adminEventMgntDao.addEventAgree(mAgree);//동의항목 등록
			}
		}
	}
	
	
	public void getCommItem() throws Exception{
		Dataset dsData = getDataset("ds_data");
		Map mData = getMap("ds_data", 0);
		addList("ds_data", adminEventMgntDao.getCommItem(mData));
	}
	
	public void addCommItem() throws Exception{
		Dataset dsData = getDataset("ds_data");
		Map mData = getMap("ds_data", 0);
		adminEventMgntDao.addCommItem(mData);
	}
	
	public void uptCommItem() throws Exception{
		Dataset dsData = getDataset("ds_data");
		Map mData = getMap("ds_data", 0);
		adminEventMgntDao.uptCommItem(mData);
	}
	
	
	public void getEventPart()  throws Exception{
		Dataset dsData = getDataset("ds_event");
		Map mData = getMap("ds_event", 0);
		
		addList("ds_event",adminEventMgntDao.getEventPart(mData));
		addList("ds_list", adminEventMgntDao.getEventPartList(mData));
		addList("ds_list_rslt", adminEventMgntDao.getEventPartRsltList(mData));
	}

	public void uptWinY() throws Exception {
		Dataset dsDelList = getDataset("ds_delList");
		ArrayList<Map> list = new ArrayList<Map>();
		if(dsDelList != null){
			for (int i = 0; i < dsDelList.getRowCount(); i++) {
				list.add(getMap("ds_delList",i));
			}
			adminEventMgntDao.uptWinY(list);
		}
	}

	public void uptWinCancel() throws Exception {
		Dataset dsDelList = getDataset("ds_delList");
		ArrayList<Map> list = new ArrayList<Map>();
		if(dsDelList != null){
			for (int i = 0; i < dsDelList.getRowCount(); i++) {
				list.add(getMap("ds_delList",i));
			}
			adminEventMgntDao.uptWinN(list);
		}
	}
	
	public void getSelWinning() throws Exception {
		Dataset dsData = getDataset("ds_event");
		Map mData = getMap("ds_event", 0);
		
		addList("ds_event",adminEventMgntDao.getEventPart(mData));
	}
	
	public void getSelRandomWinning() throws Exception{
		Dataset dsData = getDataset("ds_event");
		Map mData = getMap("ds_event", 0);
		
		addList("ds_list",adminEventMgntDao.getSelRandomWinning(mData));
	}

	public void getEventStatList() throws Exception {
		addList("ds_list", adminEventMgntDao.getEventStatList());
	}

	
	/* qna게시판 리스트
	 * @see kr.or.copyright.mls.adminEventMgnt.service.AdminEventMgntService#getEventQnaList()
	 */
	public void getEventQnaList() throws Exception {
		Dataset ds_condition = getDataset("ds_condition");     

		Map map = getMap(ds_condition, 0);
		
		List list = adminEventMgntDao.getEventQnaList(map);
		List pageList = (List)adminEventMgntDao.getEventQnaTotal(map);
		
		addList("ds_List", list);
		addList("ds_page", pageList);
	}

	public void getEventQnaDetail() throws Exception {
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);
		
		List list = adminEventMgntDao.getEventQnaDetail(map);
		
		addList("ds_list", list);
	}
	
	public void delEventQnaDetail() throws Exception {
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);
		
		adminEventMgntDao.delEventQnaDetail(map);
	}
	
	public void uptEventQnaAnswRegi() throws Exception {
		Dataset ds_list = getDataset("ds_list");     
		Map map = getMap(ds_list, 0);
		
		adminEventMgntDao.uptEventQnaAnswRegi(map);
	}

	public void uptEventQnaMenuOpen() throws Exception {
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);
		adminEventMgntDao.delEventQnaMenuOpen(map);
		adminEventMgntDao.addEventQnaMenuOpen(map);
	}

	public void getEventQnaMenuOpen() throws Exception {
		
		addList("ds_condition", adminEventMgntDao.getEventQnaMenuOpen());
	}

}
