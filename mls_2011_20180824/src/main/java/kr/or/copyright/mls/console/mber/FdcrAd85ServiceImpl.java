package kr.or.copyright.mls.console.mber;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Dao;
import kr.or.copyright.mls.console.mber.inter.FdcrAd85Service;
import kr.or.copyright.mls.console.menu.inter.FdcrAdA9Dao;

import org.springframework.stereotype.Service;

@Service( "fdcrAd85Service" )
public class FdcrAd85ServiceImpl extends CommandService implements FdcrAd85Service{

	// old AdminUserDao
	@Resource( name = "fdcrAd83Dao" )
	private FdcrAd83Dao fdcrAd83Dao;

	// old AdminMenuDao
	@Resource( name = "fdcrAdA9Dao" )
	private FdcrAdA9Dao fdcrAdA9Dao;

	/**
	 * 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd85UpdateForm1( Map<String, Object> commandMap ) throws Exception{
		List groupList = (List) fdcrAdA9Dao.selectGroupList();

		commandMap.put( "ds_group_info", groupList );

		if( commandMap.get( "GUBUN" ).equals( "edit" ) ){ // 수정
			// DAO호출
			List list = (List) fdcrAd83Dao.trstOrgnMgnbDetlList( commandMap );

			if( list.size() > 0 ){
				// 그룹 메뉴 조회 하기
				HashMap mapTemp = new HashMap();
				mapTemp.clear();
				mapTemp = (HashMap) list.get( 0 );

				List listMenu = (List) fdcrAd83Dao.adminGroupMenuInfo( mapTemp );

				commandMap.put( "ds_menu_info", listMenu );
			}
			commandMap.put( "ds_list", list ); // 로그인 사용자 정보
		}

	}

	/**
	 * 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd85Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			// 대표담당자 정보 수정
			String ORGN_MGNB_YSNO = (String) commandMap.get( "ORGN_MGNB_YSNO" );

			// 해당기관의 담당자 ORGN_MGNB_YSNO= 'N'
			if( ORGN_MGNB_YSNO.equals( "Y" ) ) fdcrAd83Dao.orgnMgnbUpdate( commandMap );

			fdcrAd83Dao.trstOrgnMgnbDetlUpdate( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
