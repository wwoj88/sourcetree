package kr.or.copyright.mls.adminBoard.service;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminBoard.dao.AdminBoardDao;
import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;

import com.tobesoft.platform.data.Dataset;

public class AdminBoardServiceImpl extends BaseService implements AdminBoardService {

	//private Log logger = LogFactory.getLog(getClass());

	private AdminBoardDao adminBoardDao;
	
	public void setAdminBoardDao(AdminBoardDao adminBoardDao){
		this.adminBoardDao = adminBoardDao;
	}
	
	
	// adminBoardList 조회 
	public void faqList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.faqList(map); 
		List pageList = (List)adminBoardDao.totalRowFaqList(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);
		
	}
	
	
	// faqDetailList 조회 
	public void faqDetailList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.faqDetailList(map);
		List detailList = (List) adminBoardDao.faqFileList(map);
		
		// club컬럼을 String로 변환한다.
		// setClub( list, "BORD_DESC" );
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_detail", detailList);

	}	
	

	// adminBoardList 저장  
	public void faqSave() throws Exception {

		Dataset ds_list   = getDataset("ds_list");   
		Dataset ds_detail = getDataset("ds_detail");   

		String  row_status = null;
		String  updateFg   = null;

//System.out.println("======================================== 1");		
		
		//detail DELETE처리
		for( int i = 0 ; ds_detail != null && i< ds_detail.getDeleteRowCount() ; i++ )
		{
//System.out.println("======================================== 2, "+i);		
			Map deleteMap = getDeleteMap(ds_detail, i );
			adminBoardDao.faqFileDelete(deleteMap); 
			
			String fileName =  ds_detail.getDeleteColumn(i, "REAL_FILE_NAME").toString();
			
			 try 
			 {	
				 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
			     if (file.exists()) 
			     { 
			    	 file.delete(); 
			     }
			  } 
			  catch (Exception e) 
			  {
			   e.printStackTrace();
			  } 
 	    }
		
		//Master DELETE처리
		for( int i = 0 ; i< ds_list.getDeleteRowCount() ; i++ )
		{
//System.out.println("======================================== 3, "+i);		
			Map deleteMap = getDeleteMap(ds_list, i );
			adminBoardDao.faqDelete(deleteMap); 
		}		

		//Master  INSERT,  UPDATE처리
		for( int i=0;i<ds_list.getRowCount();i++ )
		{

			row_status = ds_list.getRowStatus(i);
			
			Map map = getMap(ds_list, i );
			
			updateFg =  ds_list.getColumnAsString(0, "UPDATE_FG");
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true  )
			{	
				adminBoardDao.faqInsert(map); 
			}
			else if( row_status.equals("update") == true )
			{
				adminBoardDao.faqUpdate(map); 
			}
		
		}
		
		
		//detail  INSERT,  UPDATE처리
		for( int i=0;i<ds_detail.getRowCount();i++ )
		{

			row_status = ds_detail.getRowStatus(i);

			Map map = getMap(ds_detail, i );
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true )
			{	
				byte[] file = ds_detail.getColumn(i, "CLIENT_FILE").getBinary();
				
				String fileName = ds_detail.getColumnAsString(i, "REAL_FILE_NAME");
				
				Map upload = FileUtil.uploadMiFile(fileName, file);
				
//				map.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
				map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				map.put("FILE_PATH", upload.get("FILE_PATH"));
				map.put("FILE_NAME", fileName);
				
				adminBoardDao.faqFileInsert(map); 
			}
/*			
			else if( row_status.equals("update") == true )
			{

				//String fileName =  ds_detail.getDeleteColumn(i, "REAL_FILE_NAME").toString();
				  
				String fileName =  ds_detail.getColumnAsString(i, "REAL_FILE_NAME");
				
				try 
				 {
					 File file = new File("C://mls//web//upload//", fileName);
				     if (file.exists()) 
				     { 
				    	 file.delete(); 
				     }
				  } 
				  catch (Exception e) 
				  {
				   e.printStackTrace();
				  } 
				  
				adminBoardDao.faqFileUpdate(map); 
			}
*/	
		}

	}

	// qnaList 조회 
	public void qnaList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     

		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.qnaList(map);
		/* 양재석 추가 start */
		List pageList = (List)adminBoardDao.totalRowQnaList(map);
		/* 양재석 추가 end */
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		/* 양재석 추가 start */
		addList("ds_page", pageList);
		/* 양재석 추가 start */
	}
	
	// qnaDetailList 조회 
	public void qnaDetailList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.qnaDetailList(map);
		List detailList = (List) adminBoardDao.qnaFileList(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_detail", detailList);

	}	
	
	// adminBoardList 저장  
	public void qnaSave() throws Exception {


		Dataset ds_list = getDataset("ds_list");   
		Dataset ds_detail = getDataset("ds_detail");   
		
		String row_status = null;
		String  updateFg   = null;
		String prpsSeqn = "";
		
		
		//FILE DELETE처리
		for( int i = 0 ; i< ds_detail.getDeleteRowCount() ; i++ )
		{
			Map deleteMap = getDeleteMap(ds_detail, i );
			adminBoardDao.qnaFileDelete(deleteMap); 
			
			String fileName =  ds_detail.getDeleteColumn(i, "REAL_FILE_NAME").toString();
			
			 try 
			 {
				 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
			     if (file.exists()) 
			     { 
			    	 file.delete(); 
			     }
			  } 
			  catch (Exception e) 
			  {
			   e.printStackTrace();
			  } 
 	    }
		
		// DELETE처리
		for( int i = 0 ; i< ds_list.getDeleteRowCount() ; i++ )
		{
			Map deleteMap = getDeleteMap(ds_list, i );
			
			if( deleteMap.get("DETH").equals("0")) 
			{
				long prevThreaded = (Long . parseLong(deleteMap.get("THREADED").toString()) -1) /1000 * 1000;
				deleteMap.put("prevThreaded", ""+prevThreaded);
				
				// 파일목록
				List fileList = (List)adminBoardDao.qnaFileAllList(deleteMap);
				
				// 디비파일삭제
				adminBoardDao.qnaFileDeleteAll(deleteMap);
				
				// 실제파일삭제
				for( int f=0; f<fileList.size(); f++)
				{
					Map map = (Map)fileList.get(f);
					
					 try 
					 {
						 File file = new File(map.get("FILE_PATH").toString(), map.get("REAL_FILE_NAME").toString());
					     if (file.exists()) 
					     { 
					    	 file.delete(); 
					     }
					  } 
					  catch (Exception e) 
					  {
					   e.printStackTrace();
					  } 									
				}
				
				
				adminBoardDao.qnaDeleteAll(deleteMap); 
			}
			else
			{
				adminBoardDao.qnaDelete(deleteMap); 
			}
		}		
		
		//INSERT,  UPDATE처리
		System.out.println("======================================== 0, "+ds_list.getRowCount());			
		for( int i=0;i<ds_list.getRowCount();i++ )
		{
			row_status = ds_list.getRowStatus(i);

			System.out.println("======================================== 1, "+i+", "+row_status);				
			
			Map map = getMap(ds_list, i );
			
			updateFg =  ds_list.getColumnAsString(0, "UPDATE_FG");
			
			// 리플 INSERT
			if(map.get("GUBUN") !=null && map.get("GUBUN").equals("Re")){

				// 관련 게시물 threaded 업데이트
				long prevThreaded = (Long . parseLong(map.get("THREADED").toString()) -1) /1000 * 1000;
				
				map.put("prevThreaded", ""+prevThreaded);
				
				adminBoardDao.updateReply(map);
				
				map.put("bordSeqn", ""+(Long . parseLong(map.get("THREADED").toString()) -1));
				map.put("deth", ""+(Long . parseLong(map.get("DETH").toString()) +1));				
				
				adminBoardDao.qnaInsert(map); 
			}
			// INSERT
			else if( (map.get("GUBUN")==null || map.get("GUBUN").equals("") ) && row_status.equals("insert") == true )
			{	
				int bordSeqnMax = adminBoardDao.bordSeqnMax();
				
				map.put("bordSeqn", ""+bordSeqnMax);
				map.put("deth", "0");

				adminBoardDao.qnaInsert(map); 
			}
			// UPDATE
			else if( row_status.equals("update") == true )
			{
				System.out.println("======================================== 2, ");	
				adminBoardDao.qnaUpdate(map); 
				
				map.put("bordSeqn", map.get("BORD_SEQN"));// 파일첨부시 사용
			}
			
			
			// 파일첨부시 사용 : 첨부파일만 사용된경우.
			
			prpsSeqn = map.get("bordSeqn").toString();

			System.out.println("======================================== 3, "+prpsSeqn);		
			
		}
		
		
		// DETAIL 첨부파일 처리
		for( int k=0;k<ds_detail.getRowCount();k++ )
		{
			System.out.println("======================================== 4, "+k);		
			row_status = ds_detail.getRowStatus(k);

			Map detailMap = getMap(ds_detail, k );
			
			// 파일첨부시 PRPS_SEQN SET
			if( ds_list.getRowCount() == 0 )
				prpsSeqn = detailMap.get("BORD_SEQN").toString();
			
			detailMap.put("UPDATE_FG", updateFg);
			detailMap.put("bordSeqn", prpsSeqn);
			
			if( row_status.equals("insert") == true )
			{	
				byte[] file = ds_detail.getColumn(k, "CLIENT_FILE").getBinary();
				
				String fileName = ds_detail.getColumnAsString(k, "REAL_FILE_NAME");
				
				Map upload = FileUtil.uploadMiFile(fileName, file);
				
//				detailMap.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
				detailMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				detailMap.put("FILE_PATH", upload.get("FILE_PATH"));
				detailMap.put("FILE_NAME", fileName);
				
				adminBoardDao.qnaFileInsert(detailMap); 
			}			
		}
		
	}	
	
	/* 양재석 추가 start */
	// sysmNotiList 조회 
	public void sysmNotiList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.sysmNotiList(map);
		List pageList = (List)adminBoardDao.totalRowSysmNotiList(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);

	}
	
	// sysmNotiDetailList 조회 
	public void sysmNotiDetailList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.sysmNotiDetailList(map);
		List detailList = (List) adminBoardDao.sysmNotiFileList(map);
		
		// club컬럼을 String로 변환한다.
		// setClub( list, "BORD_DESC" );
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_detail", detailList);

	}	

	// sysmNotiList 저장  
	public void sysmNotiSave() throws Exception {

		Dataset ds_list   = getDataset("ds_list");   
		Dataset ds_detail = getDataset("ds_detail");   

		ds_list.printDataset();
		ds_detail.printDataset();
		
		String  row_status = null;
		String  updateFg   = null;

		//detail DELETE처리
		for( int i = 0 ; ds_detail != null && i< ds_detail.getDeleteRowCount() ; i++ )
		{

			Map deleteMap = getDeleteMap(ds_detail, i );
			adminBoardDao.sysmNotiFileDelete(deleteMap); 
			
			String fileName =  ds_detail.getDeleteColumn(i, "REAL_FILE_NAME").toString();
			
			 try 
			 {	
				 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
			     if (file.exists()) 
			     { 
			    	 file.delete(); 
			     }
			  } 
			  catch (Exception e) 
			  {
			   e.printStackTrace();
			  } 
 	    }
		
		//Master DELETE처리
		for( int i = 0 ; i< ds_list.getDeleteRowCount() ; i++ )
		{
		
			Map deleteMap = getDeleteMap(ds_list, i );
			adminBoardDao.sysmNotiDelete(deleteMap); 
		}		

		//Master  INSERT,  UPDATE처리
		for( int i=0;i<ds_list.getRowCount();i++ )
		{

			row_status = ds_list.getRowStatus(i);
			
			Map map = getMap(ds_list, i );
			
			updateFg =  ds_list.getColumnAsString(0, "UPDATE_FG");
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true  )
			{	
				adminBoardDao.sysmNotiInsert(map); 
				
				// 기관담당자들 email 발송
				sendMail(map.get("TITE").toString());
			}
			else if( row_status.equals("update") == true )
			{
				adminBoardDao.sysmNotiUpdate(map); 
			}
		
		}
		
		//detail  INSERT,  UPDATE처리
		for( int i=0;i<ds_detail.getRowCount();i++ )
		{

			row_status = ds_detail.getRowStatus(i);

			Map map = getMap(ds_detail, i );
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true )
			{	
				byte[] file = ds_detail.getColumn(i, "CLIENT_FILE").getBinary();
				
				String fileName = ds_detail.getColumnAsString(i, "REAL_FILE_NAME");
				
				Map upload = FileUtil.uploadMiFile(fileName, file);
				
//				map.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
				map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				map.put("FILE_PATH", upload.get("FILE_PATH"));
				map.put("FILE_NAME", fileName);
				
				adminBoardDao.sysmNotiFileInsert(map); 
			}
		}
	}

	
	// notiList 조회 
	public void notiList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.notiList(map); 
		List pageList = (List)adminBoardDao.totalRowNotiList(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);

	}
	
	// notiDetailList 조회 
	public void notiDetailList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.notiDetailList(map);
		List detailList = (List) adminBoardDao.notiFileList(map);
		
		// club컬럼을 String로 변환한다.
		// setClub( list, "BORD_DESC" );
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_detail", detailList);

	}	

	// notiList 저장  
	public void notiSave() throws Exception {

		Dataset ds_list   = getDataset("ds_list");   
		Dataset ds_detail = getDataset("ds_detail");   

		ds_list.printDataset();
		ds_detail.printDataset();
		
		String  row_status = null;
		String  updateFg   = null;

		//detail DELETE처리
		for( int i = 0 ; ds_detail != null && i< ds_detail.getDeleteRowCount() ; i++ )
		{

			Map deleteMap = getDeleteMap(ds_detail, i );
			adminBoardDao.notiFileDelete(deleteMap); 
			
			String fileName =  ds_detail.getDeleteColumn(i, "REAL_FILE_NAME").toString();
			
			 try 
			 {	
				 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
			     if (file.exists()) 
			     { 
			    	 file.delete(); 
			     }
			  } 
			  catch (Exception e) 
			  {
			   e.printStackTrace();
			  } 
 	    }
		
		//Master DELETE처리
		for( int i = 0 ; i< ds_list.getDeleteRowCount() ; i++ )
		{
		
			Map deleteMap = getDeleteMap(ds_list, i );
			adminBoardDao.notiDelete(deleteMap); 
		}		

		//Master  INSERT,  UPDATE처리
		for( int i=0;i<ds_list.getRowCount();i++ )
		{

			row_status = ds_list.getRowStatus(i);
			
			Map map = getMap(ds_list, i );
			
			updateFg =  ds_list.getColumnAsString(0, "UPDATE_FG");
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true  )
			{	
				adminBoardDao.notiInsert(map); 
			}
			else if( row_status.equals("update") == true )
			{
				adminBoardDao.notiUpdate(map); 
			}
		
		}
		
		//detail  INSERT,  UPDATE처리
		for( int i=0;i<ds_detail.getRowCount();i++ )
		{

			row_status = ds_detail.getRowStatus(i);

			Map map = getMap(ds_detail, i );
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true )
			{	
				byte[] file = ds_detail.getColumn(i, "CLIENT_FILE").getBinary();
				
				String fileName = ds_detail.getColumnAsString(i, "REAL_FILE_NAME");
				
				Map upload = FileUtil.uploadMiFile(fileName, file);
				
//				map.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
				map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				map.put("FILE_PATH", upload.get("FILE_PATH"));
				map.put("FILE_NAME", fileName);
				
				adminBoardDao.notiFileInsert(map); 
			}
		}
	}

	
	// promList 조회 
	public void promList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.promList(map); 
		List pageList = (List)adminBoardDao.totalRowPromList(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);
		
	}
	
	
	// promDetailList 조회 
	public void promDetailList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.promDetailList(map);
		List detailList = (List) adminBoardDao.promFileList(map);
		
		// club컬럼을 String로 변환한다.
		// setClub( list, "BORD_DESC" );
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_detail", detailList);

	}	
	

	// promList 저장  
	public void promSave() throws Exception {

		Dataset ds_list   = getDataset("ds_list");   
		Dataset ds_detail = getDataset("ds_detail");   

		String  row_status = null;
		String  updateFg   = null;
		String  type = null;

//System.out.println("======================================== 1");		
		
		//detail DELETE처리
		for( int i = 0 ; ds_detail != null && i< ds_detail.getDeleteRowCount() ; i++ )
		{
//System.out.println("======================================== 2, "+i);		
			Map deleteMap = getDeleteMap(ds_detail, i );
			adminBoardDao.promFileDelete(deleteMap); 
			
			String fileName =  ds_detail.getDeleteColumn(i, "REAL_FILE_NAME").toString();
			
			 try 
			 {	
				 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
			     if (file.exists()) 
			     { 
			    	 file.delete(); 
			     }
			  } 
			  catch (Exception e) 
			  {
			   e.printStackTrace();
			  } 
 	    }
		
		//Master DELETE처리
		for( int i = 0 ; i< ds_list.getDeleteRowCount() ; i++ )
		{
//System.out.println("======================================== 3, "+i);		
			Map deleteMap = getDeleteMap(ds_list, i );
			adminBoardDao.promDelete(deleteMap); 
		}		

		//Master  INSERT,  UPDATE처리
		for( int i=0;i<ds_list.getRowCount();i++ )
		{

			row_status = ds_list.getRowStatus(i);
			
			Map map = getMap(ds_list, i );
			
			updateFg =  ds_list.getColumnAsString(0, "UPDATE_FG");
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true  )
			{	
				adminBoardDao.promInsert(map); 
			}
			else if( row_status.equals("update") == true )
			{
				adminBoardDao.promUpdate(map); 
			}
		
		}
		
		//detail  INSERT,  UPDATE처리
		for( int i=0;i<ds_detail.getRowCount();i++ )
		{

			row_status = ds_detail.getRowStatus(i);

			Map map = getMap(ds_detail, i );
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true )
			{	
				byte[] file = ds_detail.getColumn(i, "CLIENT_FILE").getBinary();
				
				String fileName = ds_detail.getColumnAsString(i, "REAL_FILE_NAME");
				
				type = ds_list.getColumnAsString(i, "MENU_SEQN");
				
				Map upload = FileUtil.uploadPromMiFile(fileName, file);
				
//				map.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
				map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				map.put("FILE_PATH", upload.get("FILE_PATH"));
				map.put("FILE_NAME", fileName);
				
				adminBoardDao.promFileInsert(map); 
			}
		}
	}
	
	// 법정허락 저작물 조회 
	public void statList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.statList(map);
		List pageList = (List)adminBoardDao.totalRowStatList(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);

	}
	
	// adminBoardList 저장  
	public void statSave() throws Exception {

		Dataset ds_list   = getDataset("ds_list");   
		Dataset ds_detail = getDataset("ds_detail");   

		String  row_status = null;
		String  updateFg   = null;
		
		String fild = null;  
		String name = "";

		//detail DELETE처리
		for( int i = 0 ; ds_detail != null && i< ds_detail.getDeleteRowCount() ; i++ )
		{
			Map deleteMap = getDeleteMap(ds_detail, i );
			adminBoardDao.statFileDelete(deleteMap); 
			
			String fileName =  ds_detail.getDeleteColumn(i, "REAL_FILE_NAME").toString();
			
			 try 
			 {	
				 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
			     if (file.exists()) 
			     { 
			    	 file.delete(); 
			     }
			  } 
			  catch (Exception e) 
			  {
			   e.printStackTrace();
			  } 
 	    }
		
		//Master DELETE처리
		for( int i = 0 ; i< ds_list.getDeleteRowCount() ; i++ )
		{
			Map deleteMap = getDeleteMap(ds_list, i );
			adminBoardDao.statDelete(deleteMap); 
			adminBoardDao.statFildDelete(deleteMap); 
		}		

		int statSeqn = adminBoardDao.statSeqn();
		
		//Master  INSERT,  UPDATE처리
		for( int i=0;i<ds_list.getRowCount();i++ )
		{
			row_status = ds_list.getRowStatus(i);
			
			Map map = getMap(ds_list, i );
			
			updateFg =  ds_list.getColumnAsString(0, "UPDATE_FG");
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true  )
			{	
				map.put("SEQN", statSeqn);
				adminBoardDao.statInsert(map); 
			}
			else if( row_status.equals("update") == true )
			{	
				adminBoardDao.statUpdate(map); 
				map.put("SEQN", ds_list.getColumnAsString(0, "BORD_SEQN"));
			}
			
			adminBoardDao.statFildDelete(map);
			
			for(int x=1;x<13;x++){
				if(x==12){
					x = 99;
				}
				name = "FILD_"+x;
				fild = ds_list.getColumnAsString(0, name);
				
				if(fild.equals("1")){
					map.put("FILD", x);
					adminBoardDao.statFildInsert(map); 
				}
			}
		
		}
		
		//detail  INSERT,  UPDATE처리
		for( int i=0;i<ds_detail.getRowCount();i++ )
		{

			row_status = ds_detail.getRowStatus(i);

			Map map = getMap(ds_detail, i );
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true )
			{	
				byte[] file = ds_detail.getColumn(i, "CLIENT_FILE").getBinary();
				
				String fileName = ds_detail.getColumnAsString(i, "REAL_FILE_NAME");
				
				Map upload = FileUtil.uploadMiFile(fileName, file);
				
//				map.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
				map.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				map.put("FILE_PATH", upload.get("FILE_PATH"));
				map.put("FILE_NAME", fileName);
				
				adminBoardDao.statFileInsert(map); 
			}
		}
	}
	
	public void statDetailList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.statDetailList(map);
		List detailList = (List) adminBoardDao.statFileList(map);
		
		// club컬럼을 String로 변환한다.
		// setClub( list, "BORD_DESC" );
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_detail", detailList);

	}
	
	// clms 저작물 조회  
	public void clmsWorksList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		String gubun = map.get("GUBUN").toString();
		List list = null;
		List pageList = null;
		
		if(gubun.equals("1")){	// 음악
			list = (List) adminBoardDao.clmsWorksList(map);
			pageList = (List)adminBoardDao.totalClmsWorksList(map);
		}else if(gubun.equals("2")){	// 어문
			list = (List) adminBoardDao.clmsBookList(map);
			pageList = (List)adminBoardDao.totalClmsBookList(map);
		}else if(gubun.equals("3")){	// 방송대본
			list = (List) adminBoardDao.clmsBroadList(map);
			pageList = (List)adminBoardDao.totalClmsBroadList(map);
		}else if(gubun.equals("4")){	// 방송
			list = (List) adminBoardDao.clmsBroadCastList(map);
			pageList = (List)adminBoardDao.totalClmsBroadCastList(map);
		}else if(gubun.equals("5")){	// 영화
			list = (List) adminBoardDao.clmsMovieList(map);
			pageList = (List)adminBoardDao.totalClmsMovieList(map);
		}else if(gubun.equals("6")){	// 이미지
			list = (List) adminBoardDao.clmsImageList(map);
			pageList = (List)adminBoardDao.totalClmsImageList(map);
		}
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);
		addList("ds_page", pageList);

	}
	
	//clms 저작물 상세 조회  
//	public void clmsWorksDetail() throws Exception {
//
//		Dataset ds_condition = getDataset("ds_condition");     
//		
//		Map map = getMap(ds_condition, 0);		
//		
//		//DAO호출
//		List list = (List) adminBoardDao.clmsWorksDetail(map); 
//		
//		addList("ds_list", list); // 로그인 사용자 정보
//
//	}
	
	
	//clms 저작물 상세 조회  
	public void clmsWorksDetail() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");     
		Map map = getMap(ds_condition, 0);

		String prpsDivs = map.get("PRPS_DIVS").toString();
		List list = null;
		
		if(prpsDivs.equals("M")){ // 음악
			list = (List) adminBoardDao.clmsMuscDetail(map);
		}else if(prpsDivs.equals("O")){  // 어문
			list = (List) adminBoardDao.clmsBookDetail(map);
		}
		
		addList("ds_works_info", list);
		
	}
	
	public void admSysmNotiList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		// Dataset 확인_log성
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) adminBoardDao.admSysmNotiList(map);
		
		//hmap를 dataset로 변환한다.
		addList("ds_list_noti", list);
	}
	
	
	// 담당자 email 발송
	public void sendMail(String bordTitle) throws UnsupportedEncodingException {
		
		MailInfo mailInfo = new MailInfo();
		
		mailInfo.setFrom(Constants.SYSTEM_MAIL_ADDRESS);  //발신인 주소 - 시스템 대표 메일
		mailInfo.setFromName(Constants.SYSTEM_NAME);
		mailInfo.setHost(Constants.MAIL_SERVER_IP);
		
		
		ArrayList toList = new ArrayList(); 
		ArrayList toNameList = new ArrayList();
		
		
		// test 메일발송
		
		/*
		toList.add("srkim@yagins.com");
		//toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_201").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_201").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add("srkim@yagins.com");
		//toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_201").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_202").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add("kammiya@yagins.com");
		//toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_201").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_203").getBytes("ISO-8859-1"), "EUC-KR"));
		*/
		// test 메일발송 완료
		
		
		
		// real 메일방송 setting

		// 1. 보상금
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_mail_202").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_202").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_mail_203").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_203").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_mail_205").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_205").getBytes("ISO-8859-1"), "EUC-KR"));
		
		
		// 2. 저작권찾기
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_201").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_201").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_202").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_202").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_203").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_203").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_204").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_204").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_205").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_205").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_206").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_206").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_211").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_211").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_212").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_212").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_213").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_213").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_214").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_214").getBytes("ISO-8859-1"), "EUC-KR"));
		
		toList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_mail_215").getBytes("ISO-8859-1"), "EUC-KR"));
		toNameList.add(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("rghtPrps_name_215").getBytes("ISO-8859-1"), "EUC-KR"));
		
		String to[] = new String[toList.size()];
		String toName[] = new String[toList.size()];
		String toStr = "";
		
		for( int kk=0; kk<toList.size(); kk++) {
			
			System.out.println((String)toList.get(kk));
			
			to[kk] = (String)toList.get(kk);
			toName[kk] = (String)toNameList.get(kk);
			
			toStr += ", "+to[kk]+"("+toName[kk]+")" ;
		}
		
		mailInfo.setArrEmail(to);
		mailInfo.setArrEmailName(toName);
		
		mailInfo.setSubject(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("mgntBord_mailto_subj").getBytes("ISO-8859-1"), "EUC-KR"));
		
		StringBuffer  message = new StringBuffer();

		message.append("<div class=\"mail_title\"></div>");
		message.append("<div class=\"mail_contents\"> ");
		message.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
		message.append("		<tr> ");
		message.append("			<td> ");
		message.append("				<table width=\"100%\" border=\"0\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\"> ");

		message.append("					<tr> ");
	  	message.append("						<td align=\"center\"> ");
	    
		message.append("<br/>");
		message.append("<b>"+mailInfo.getSubject()+"</b>");
		
		message.append("<br/>");
		message.append( "<br/><br/>");
		message.append( "<b>관리자 공지사항 : <u>"+bordTitle+"</u></b>");
		message.append("<br/>");
		message.append("<br/>");
		message.append( "<b>상세내용은 저작권찾기사이트에서 확인 가능합니다.</b>");
	    
		message.append("						</td> ");
		message.append("					</tr> ");
		message.append("				</table> ");
		   
		message.append("			</td> ");
		message.append("		</tr> ");
		message.append("	</table> ");
		message.append("</div> ");
	    
		mailInfo.setMessage(new String(MailMessage.getChangeInfoMailText(message.toString())));
		
		MailManager manager = MailManager.getInstance();
		mailInfo = manager.sendMailAll(mailInfo);
		
		if(mailInfo.isSuccess()){
		  System.out.println(">>>>>>>>>>>> 1. message success :"+toStr );
		}else{
		  System.out.println(">>>>>>>>>>>> 1. message false   :"+toStr );
		}
		
	}
}
