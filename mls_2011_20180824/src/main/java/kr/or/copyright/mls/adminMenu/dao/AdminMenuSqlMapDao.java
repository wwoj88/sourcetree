package kr.or.copyright.mls.adminMenu.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class AdminMenuSqlMapDao extends SqlMapClientDaoSupport implements AdminMenuDao{
	
	// 메뉴 목록조회
	public List selectMenuList(){
		return getSqlMapClientTemplate().queryForList("AdminMenu.selectMenuList","");
	}
	
	// 메뉴 삭제
	public void menuDelete(Map map){
		getSqlMapClientTemplate().update("AdminMenu.menuDelete",map);
	}
	
	// 메뉴 등록
	public void menuInsert(Map map){
		getSqlMapClientTemplate().update("AdminMenu.menuInsert",map);
	}
	
	// 메뉴 수정
	public void menuUpdate(Map map){
		getSqlMapClientTemplate().update("AdminMenu.menuUpdate",map);
	}
	
	// 그룹 목록 조회
	public List selectGroupList(){
		return getSqlMapClientTemplate().queryForList("AdminMenu.selectGroupList","");
	}
	
	// 그룹 상세 조회
	public List groupInfo(Map map){
		return getSqlMapClientTemplate().queryForList("AdminMenu.groupInfo",map);
	}
	
	// 그룹 메뉴 조회
	public List groupMenuInfo(Map map){
		return getSqlMapClientTemplate().queryForList("AdminMenu.groupMenuInfo",map);
	}
	
	// 그룹 아이디 값 세팅
	public int groupIdMax(Map map){
		Integer totalRow = (Integer) getSqlMapClientTemplate().queryForObject("AdminMenu.groupIdMax", map);
		return totalRow.intValue();
	}
	
	// 그룹 등록
	public void groupInsert(Map map){
		getSqlMapClientTemplate().update("AdminMenu.groupInsert",map);
	}
	
	// 그룹 메뉴 등록
	public void groupMenuInsert(Map map){
		getSqlMapClientTemplate().update("AdminMenu.groupMenuInsert",map);
	}
	
	// 그룹 삭제
	public void groupDelete(Map map){
		getSqlMapClientTemplate().update("AdminMenu.groupDelete",map);
	}
	
	// 그룹 메뉴 삭제
	public void groupMenuDelete(Map map){
		getSqlMapClientTemplate().update("AdminMenu.groupMenuDelete",map);
	}
	
	// 그룹 수정
	public void groupUpdate(Map map){
		getSqlMapClientTemplate().update("AdminMenu.groupUpdate",map);
	}
}
