package kr.or.copyright.mls.console.stats;

import java.net.URLDecoder;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.stats.inter.FdcrAd97Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 통계관리 > 위탁관리업체 보고현황
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd97Controller extends DefaultController{

	@Resource( name = "fdcrAd97Service" )
	private FdcrAd97Service fdcrAd97Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd97Controller.class );

	/**
	 * 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd97List1.page" )
	public String fdcrAd97List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd97List1 Start" );
		// 파라미터 셋팅
		String YYYY = EgovWebUtil.getString( commandMap, "YYYY" );
		if("".equals(YYYY)){
			commandMap.put( "YYYY", EgovWebUtil.getDate( "yyyy" ) ) ;
		}
		commandMap.put( "DIV", "10" ) ;
		fdcrAd97Service.fdcrAd97List1( commandMap );
		model.addAttribute( "commandMap", commandMap);
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "fdcrAd97List1 End" );
		return "/stats/fdcrAd97List1.tiles";
	}
	
	/**
	 * 월별 보고현황 조회
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd97List2.page" )
	public String fdcrAd97List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd97List2 Start" );
		// 파라미터 셋팅
		String YYYYMM = EgovWebUtil.getString( commandMap, "YYYYMM" );
		if(!"".equals(YYYYMM)){
			commandMap.put( "YYYY", YYYYMM.substring( 0,4 ) ) ;
			commandMap.put( "MM", YYYYMM.substring( 4,6 ) ) ;
		}
		commandMap.put( "DIV", "20" ) ;
		fdcrAd97Service.fdcrAd97List2( commandMap );
		model.addAttribute( "commandMap", commandMap);
		model.addAttribute( "ds_count", commandMap.get( "ds_count" ) );
		model.addAttribute( "ds_report", commandMap.get( "ds_report" ) );
		logger.debug( "fdcrAd97List2 End" );
		return "/stats/fdcrAd97List2.popup";
	}
	
	
	/**
	 * 월별 보고현황 조회
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd97List3.page" )
	public String fdcrAd97List3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		EgovWebUtil.ajaxCommonEncoding( request, commandMap );
		logger.debug( "fdcrAd97List3 Start" );
		// 파라미터 셋팅
		String YYYYMM = EgovWebUtil.getString( commandMap, "YYYYMM" );
		if(!"".equals(YYYYMM)){
			commandMap.put( "YYYY", YYYYMM.substring( 0,4 ) ) ;
			commandMap.put( "MM", YYYYMM.substring( 4,6 ) ) ;
		}
		String TITLE = EgovWebUtil.getString( commandMap, "TITLE" );
		if( !"".equals( TITLE ) ){
			TITLE = URLDecoder.decode( TITLE, "UTF-8" );
			commandMap.put( "TITLE", TITLE );
		}
		
		fdcrAd97Service.fdcrAd97List3( commandMap );
		model.addAttribute( "commandMap", commandMap);
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		logger.debug( "fdcrAd97List3 End" );
		return "/stats/fdcrAd97List3";
	}
	
}
