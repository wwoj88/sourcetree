package kr.or.copyright.mls.intg.model;

import java.util.ArrayList;
import java.util.List;

import kr.or.copyright.mls.common.BaseObject;

public class Intg extends BaseObject {

	private String srchDivs;
	private String srchText;
	private String page_no;
	private long bordSeqn;
	private long menuSeqn;
	private long parentThreaded;
	private long prevThreaded;
	private long threaded;
	private long attcSeqn;
	private String tite;
	private String pswd;
	private int inqrCont;
	private String mail;
	private int deth;
	private int noidx;
	private String deltYsno;
	private String htmlYsno;
	private String rgstIdnt;
	private String rgstDate;
	private String modiIdnt;
	private String modiDate;
	private String bordDesc;
	private String fileName;
	private String filePath;
	private String fileSize;
	private String realFileName;
	private int fileCont;
	private List fileList;
	
	/////////////////////////////
	
	private String gubun = "";
	private String title = "";
	private String srchWord = "";
	private ArrayList resultWordList = null;
	private String more = "";
	private int from = 1;		// 기본 검색 시작건수
	private int to = 3;			// 기본 검색 종료건수
	
	private String imgeSeq = "";		// 이미지seq
	private String workName = "";	// 이미지명
	private String lishComp = "";		// 출판사
	private String coptHodr = "";		// 저작권자
	private String usexYear = "";		// 이용년도
	
	private String inmtSeqn = "";		// 방송음악 seq
	private String sdsrName = "";	// 음원명
	private String muciName = "";	// 뮤지션이름
	private String yymm = "";			// 이용년도
	
	private String workKind = "";		// 저작물종류
	private String wterDivs = "";		// 집필진
	private String schlClss = "";		// 학급
	private String itemName = "";	// 과목명
	private String subjDivs = "";		// 도서구분
	private String bookKindDivs = "";	// 권별구분
	private String schlTerm = "";		// 학기
	private String usexDivs = "";		// 저작물이용구분
	private String usexPage  = "";	// 이용페이지
	private String usexMunt = "";		// 이용량
	private String Divs      = "";		// 분류
	
	private String  oferEtpr = "";		// 제공업체
	private String   brctCont = "";	// 방송횟수
	private String   lyriWrtr = "";		// 작사가
	private String  comsWrtr = "";	// 작곡가
	private String  arrgWrtr = "";		// 편곡자
	private String  albmName = "";	// 앨범명
	
	private String workCode  = "";	// 저작물코드
	private String caryDivs  = "";	// 반입구분
	private String schl 		 = "";	// 학교
	private String bookDivs  = "";	// 도서구분
	private String subjName  = "";	// 교과목명칭
	private String bookSizeDivs  = "";	// 권별구분
	private String schlYearDivs  = "";	// 학기구분
	private String pubcCont  = "";	// 발행부수
	private String autrDivs = ""; 	// 집필진구분
	private String pubcYear  = "";	// 출판년도
	private String workDivs = ""; 	// 저작물구분
	
	private String dataType  = ""; 	// 자료형태
	private String usexType = ""; 	//  이용형태
	private String selgYsno  = ""; 	// 판매유무
	private String usexLibr = ""; 	//  이용도서관
	private String ouptPage = ""; 	//  출력페이지
    
	public String getAlbmName() {
		return albmName;
	}
	public void setAlbmName(String albmName) {
		this.albmName = albmName;
	}
	public String getArrgWrtr() {
		return arrgWrtr;
	}
	public void setArrgWrtr(String arrgWrtr) {
		this.arrgWrtr = arrgWrtr;
	}
	public String getBrctCont() {
		return brctCont;
	}
	public void setBrctCont(String brctCont) {
		this.brctCont = brctCont;
	}
	public String getComsWrtr() {
		return comsWrtr;
	}
	public void setComsWrtr(String comsWrtr) {
		this.comsWrtr = comsWrtr;
	}
	public String getLyriWrtr() {
		return lyriWrtr;
	}
	public void setLyriWrtr(String lyriWrtr) {
		this.lyriWrtr = lyriWrtr;
	}
	public String getOferEtpr() {
		return oferEtpr;
	}
	public void setOferEtpr(String oferEtpr) {
		this.oferEtpr = oferEtpr;
	}
	public String getInmtSeqn() {
		return inmtSeqn;
	}
	public void setInmtSeqn(String inmtSeqn) {
		this.inmtSeqn = inmtSeqn;
	}
	public String getMuciName() {
		return muciName;
	}
	public void setMuciName(String muciName) {
		this.muciName = muciName;
	}
	public String getSdsrName() {
		return sdsrName;
	}
	public void setSdsrName(String sdsrName) {
		this.sdsrName = sdsrName;
	}
	public String getYymm() {
		return yymm;
	}
	public void setYymm(String yymm) {
		this.yymm = yymm;
	}
	public String getMore() {
		return more;
	}
	public void setMore(String more) {
		this.more = more;
	}
	public long getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(long attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public String getBordDesc() {
		return bordDesc;
	}
	public void setBordDesc(String bordDesc) {
		this.bordDesc = bordDesc;
	}
	public long getBordSeqn() {
		return bordSeqn;
	}
	public void setBordSeqn(long bordSeqn) {
		this.bordSeqn = bordSeqn;
	}
	public String getDeltYsno() {
		return deltYsno;
	}
	public void setDeltYsno(String deltYsno) {
		this.deltYsno = deltYsno;
	}
	public int getDeth() {
		return deth;
	}
	public void setDeth(int deth) {
		this.deth = deth;
	}
	public int getFileCont() {
		return fileCont;
	}
	public void setFileCont(int fileCont) {
		this.fileCont = fileCont;
	}
	public List getFileList() {
		return fileList;
	}
	public void setFileList(List fileList) {
		this.fileList = fileList;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
	public String getHtmlYsno() {
		return htmlYsno;
	}
	public void setHtmlYsno(String htmlYsno) {
		this.htmlYsno = htmlYsno;
	}
	public int getInqrCont() {
		return inqrCont;
	}
	public void setInqrCont(int inqrCont) {
		this.inqrCont = inqrCont;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public long getMenuSeqn() {
		return menuSeqn;
	}
	public void setMenuSeqn(long menuSeqn) {
		this.menuSeqn = menuSeqn;
	}
	public String getModiDate() {
		return modiDate;
	}
	public void setModiDate(String modiDate) {
		this.modiDate = modiDate;
	}
	public String getModiIdnt() {
		return modiIdnt;
	}
	public void setModiIdnt(String modiIdnt) {
		this.modiIdnt = modiIdnt;
	}
	public int getNoidx() {
		return noidx;
	}
	public void setNoidx(int noidx) {
		this.noidx = noidx;
	}
	public String getPage_no() {
		return page_no;
	}
	public void setPage_no(String page_no) {
		this.page_no = page_no;
	}
	public long getParentThreaded() {
		return parentThreaded;
	}
	public void setParentThreaded(long parentThreaded) {
		this.parentThreaded = parentThreaded;
	}
	public long getPrevThreaded() {
		return prevThreaded;
	}
	public void setPrevThreaded(long prevThreaded) {
		this.prevThreaded = prevThreaded;
	}
	public String getPswd() {
		return pswd;
	}
	public void setPswd(String pswd) {
		this.pswd = pswd;
	}
	public String getRealFileName() {
		return realFileName;
	}
	public void setRealFileName(String realFileName) {
		this.realFileName = realFileName;
	}
	public String getRgstDate() {
		return rgstDate;
	}
	public void setRgstDate(String rgstDate) {
		this.rgstDate = rgstDate;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getSrchDivs() {
		return srchDivs;
	}
	public void setSrchDivs(String srchDivs) {
		this.srchDivs = srchDivs;
	}
	public String getSrchText() {
		return srchText;
	}
	public void setSrchText(String srchText) {
		this.srchText = srchText;
	}
	public long getThreaded() {
		return threaded;
	}
	public void setThreaded(long threaded) {
		this.threaded = threaded;
	}
	public String getTite() {
		return tite;
	}
	public void setTite(String tite) {
		this.tite = tite;
	}
	public int getTo() {
		return to;
	}
	public void setTo(int to) {
		this.to = to;
	}
	public ArrayList getResultWordList() {
		return resultWordList;
	}
	public void setResultWordList(ArrayList resultWordList) {
		this.resultWordList = resultWordList;
	}
	public String getWorkName() {
		return workName;
	}
	public void setWorkName(String workName) {
		this.workName = workName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGubun() {
		return gubun;
	}
	public void setGubun(String gubun) {
		this.gubun = gubun;
	}
	public String getSrchWord() {
		return srchWord;
	}
	public void setSrchWord(String srchWord) {
		this.srchWord = srchWord;
	}
	public String getCoptHodr() {
		return coptHodr;
	}
	public void setCoptHodr(String coptHodr) {
		this.coptHodr = coptHodr;
	}
	public String getImgeSeq() {
		return imgeSeq;
	}
	public void setImgeSeq(String imgeSeq) {
		this.imgeSeq = imgeSeq;
	}
	public String getLishComp() {
		return lishComp;
	}
	public void setLishComp(String lishComp) {
		this.lishComp = lishComp;
	}
	public String getUsexYear() {
		return usexYear;
	}
	public void setUsexYear(String usexYear) {
		this.usexYear = usexYear;
	}
	public String getWorkKind() {
		return workKind;
	}
	public void setWorkKind(String workKind) {
		this.workKind = workKind;
	}
	public String getBookKindDivs() {
		return bookKindDivs;
	}
	public void setBookKindDivs(String bookKindDivs) {
		this.bookKindDivs = bookKindDivs;
	}
	public String getDivs() {
		return Divs;
	}
	public void setDivs(String divs) {
		Divs = divs;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getSchlClss() {
		return schlClss;
	}
	public void setSchlClss(String schlClss) {
		this.schlClss = schlClss;
	}
	public String getSchlTerm() {
		return schlTerm;
	}
	public void setSchlTerm(String schlTerm) {
		this.schlTerm = schlTerm;
	}
	public String getSubjDivs() {
		return subjDivs;
	}
	public void setSubjDivs(String subjDivs) {
		this.subjDivs = subjDivs;
	}
	public String getUsexDivs() {
		return usexDivs;
	}
	public void setUsexDivs(String usexDivs) {
		this.usexDivs = usexDivs;
	}
	public String getUsexMunt() {
		return usexMunt;
	}
	public void setUsexMunt(String usexMunt) {
		this.usexMunt = usexMunt;
	}
	public String getUsexPage() {
		return usexPage;
	}
	public void setUsexPage(String usexPage) {
		this.usexPage = usexPage;
	}
	public String getWterDivs() {
		return wterDivs;
	}
	public void setWterDivs(String wterDivs) {
		this.wterDivs = wterDivs;
	}
	public String getAutrDivs() {
		return autrDivs;
	}
	public void setAutrDivs(String autrDivs) {
		this.autrDivs = autrDivs;
	}
	public String getBookDivs() {
		return bookDivs;
	}
	public void setBookDivs(String bookDivs) {
		this.bookDivs = bookDivs;
	}
	public String getBookSizeDivs() {
		return bookSizeDivs;
	}
	public void setBookSizeDivs(String bookSizeDivs) {
		this.bookSizeDivs = bookSizeDivs;
	}
	public String getCaryDivs() {
		return caryDivs;
	}
	public void setCaryDivs(String caryDivs) {
		this.caryDivs = caryDivs;
	}
	public String getPubcCont() {
		return pubcCont;
	}
	public void setPubcCont(String pubcCont) {
		this.pubcCont = pubcCont;
	}
	public String getPubcYear() {
		return pubcYear;
	}
	public void setPubcYear(String pubcYear) {
		this.pubcYear = pubcYear;
	}
	public String getSchl() {
		return schl;
	}
	public void setSchl(String schl) {
		this.schl = schl;
	}
	public String getSchlYearDivs() {
		return schlYearDivs;
	}
	public void setSchlYearDivs(String schlYearDivs) {
		this.schlYearDivs = schlYearDivs;
	}
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public String getWorkCode() {
		return workCode;
	}
	public void setWorkCode(String workCode) {
		this.workCode = workCode;
	}
	public String getWorkDivs() {
		return workDivs;
	}
	public void setWorkDivs(String workDivs) {
		this.workDivs = workDivs;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getOuptPage() {
		return ouptPage;
	}
	public void setOuptPage(String ouptPage) {
		this.ouptPage = ouptPage;
	}
	public String getSelgYsno() {
		return selgYsno;
	}
	public void setSelgYsno(String selgYsno) {
		this.selgYsno = selgYsno;
	}
	public String getUsexLibr() {
		return usexLibr;
	}
	public void setUsexLibr(String usexLibr) {
		this.usexLibr = usexLibr;
	}
	public String getUsexType() {
		return usexType;
	}
	public void setUsexType(String usexType) {
		this.usexType = usexType;
	}

	
}
