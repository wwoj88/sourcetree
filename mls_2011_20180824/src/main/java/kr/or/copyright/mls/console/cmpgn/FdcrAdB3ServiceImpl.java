package kr.or.copyright.mls.console.cmpgn;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.cmpgn.inter.FdcrAdB3Dao;
import kr.or.copyright.mls.console.cmpgn.inter.FdcrAdB3Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdB3Service" )
public class FdcrAdB3ServiceImpl extends CommandService implements FdcrAdB3Service{

	// old AdminEventDao
	@Resource( name = "fdcrAdB3Dao" )
	private FdcrAdB3Dao fdcrAdB3Dao;
	
	/**
	 * 설문 참여 목록
	 */
	public void fdcrAdB3List1(Map<String,Object> commandMap) throws Exception{
		// DAO호출
		List list = (List)fdcrAdB3Dao.campPartList(commandMap);
		// DataSet return
		commandMap.put("ds_List", list);
	}
	
	/**
	 * 설문 참여 조회
	 */
	public Map fdcrAdB3View1(Map<String,Object> commandMap) throws Exception{
		List list = (List)fdcrAdB3Dao.campPartList(commandMap);
		Map info = (Map)list.get( 0 );
		return info;
	}

}
