package kr.or.copyright.mls.console.email;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.email.inter.FdcrAdA4Dao;
import kr.or.copyright.mls.console.email.inter.FdcrAdA6Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdA6Service" )
public class FdcrAdA6ServiceImpl extends CommandService implements FdcrAdA6Service{

	@Resource( name = "fdcrAdA4Dao" )
	private FdcrAdA4Dao fdcrAdA4Dao;

	/**
	 * 발신함 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA6List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA4Dao.selectSendEmailList();

		commandMap.put( "ds_list", list );

	}

	/**
	 * 발신함 상세 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA6View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA4Dao.selectSendEmailView( commandMap );
		List fileList = (List) fdcrAdA4Dao.selectMailAttcList( commandMap );

		commandMap.put( "ds_view", list );
		commandMap.put( "ds_detail", fileList );

	}

}
