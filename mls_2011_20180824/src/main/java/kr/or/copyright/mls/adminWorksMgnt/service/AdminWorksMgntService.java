package kr.or.copyright.mls.adminWorksMgnt.service;

public interface AdminWorksMgntService {
    
    public void worksMgntInsert() throws Exception;
    
    public void worksMgntUpdate() throws Exception;
    
    public void worksMgntList() throws Exception;
    
    public void worksMgntReptView() throws Exception;
    
    public void worksMgntMnthList() throws Exception;
    
    public void worksMgntNoDataInsert() throws Exception;
    
    public void worksMgntReptMonthView() throws Exception;
    
    public void worksMgntAppDate() throws Exception;
    
    public void worksMgntReptYearView() throws Exception;
    
    public void worksMgntReptInfo2() throws Exception;
    
    public void worksMgntChkCnt() throws Exception;
    
    public void worksMgntList_01() throws Exception;
    
    public void worksMgntExcelDown() throws Exception;
  
}
