package kr.or.copyright.mls.mobile.model;

public class Script {
	public int
		crId
	;
	
	public String
	   title
	   , writer
	   , broadStat
	   , genreStr
	   , subjKind
	   , direct
	   , trustYnStr
	   , broadDate
	   , subtitle
	;

	public String getBroadDate() {
		return broadDate;
	}

	public void setBroadDate(String broadDate) {
		this.broadDate = broadDate;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public int getCrId() {
		return crId;
	}

	public void setCrId(int crId) {
		this.crId = crId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getBroadStat() {
		return broadStat;
	}

	public void setBroadStat(String broadStat) {
		this.broadStat = broadStat;
	}

	public String getGenreStr() {
		return genreStr;
	}

	public void setGenreStr(String genreStr) {
		this.genreStr = genreStr;
	}

	public String getSubjKind() {
		return subjKind;
	}

	public void setSubjKind(String subjKind) {
		this.subjKind = subjKind;
	}

	public String getDirect() {
		return direct;
	}

	public void setDirect(String direct) {
		this.direct = direct;
	}

	public String getTrustYnStr() {
		return trustYnStr;
	}

	public void setTrustYnStr(String trustYnStr) {
		this.trustYnStr = trustYnStr;
	}

}
