package kr.or.copyright.mls.console.menu;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.menu.inter.FdcrAdA9Dao;
import kr.or.copyright.mls.console.menu.inter.FdcrAdB0Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdB0Service" )
public class FdcrAdB0ServiceImpl extends CommandService implements FdcrAdB0Service{

	// old AdminUserDao
	@Resource( name = "fdcrAdA9Dao" )
	private FdcrAdA9Dao fdcrAdA9Dao;

	/**
	 * 그룹 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB0List1( Map<String, Object> commandMap ) throws Exception{
		commandMap.put( "ds_group_info", fdcrAdA9Dao.selectGroupList() );
	}

	/**
	 * 그룹 메뉴 조회하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB0View1( Map<String, Object> commandMap ) throws Exception{
		commandMap.put( "ds_group_info", fdcrAdA9Dao.groupInfo( commandMap ) );
	}
	
	/**
	 * 그룹 메뉴 조회하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB0List2( Map<String, Object> commandMap ) throws Exception{
		commandMap.put( "ds_group_menu", fdcrAdA9Dao.groupMenuInfo( commandMap ) );
	}

	/**
	 * 그룹 등록하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean groupInsert( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			int groupId = fdcrAdA9Dao.groupIdMax( commandMap );
			commandMap.put( "GROUP_SEQ", groupId );
			fdcrAdA9Dao.groupInsert( commandMap );
	
			String[] GROUP_YMD = (String[]) commandMap.get( "GROUP_YMD" );
			String[] MENU_ID = (String[]) commandMap.get( "MENU_ID" );
			String[] MODI_IDNT = (String[]) commandMap.get( "MODI_IDNT" );
	
			for( int i = 0; i < MENU_ID.length; i++ ){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "GROUP_YMD", GROUP_YMD[i] );
				map.put( "MENU_ID", MENU_ID[i] );
				map.put( "MODI_IDNT", MODI_IDNT[i] );
				map.put( "GROUP_SEQ", commandMap.get( "GROUP_SEQ" ) );
				fdcrAdA9Dao.groupMenuInsert( map );
			}
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 그룹 메뉴 삭제하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean groupDelete( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAdA9Dao.groupDelete( commandMap );
			fdcrAdA9Dao.groupMenuDelete( commandMap );
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 그룹 메뉴 수정하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdB0Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			if(commandMap.get( "GROUP_YMD" ) == null || "".equals( commandMap.get( "GROUP_YMD" ) )){
				Date date = new Date();
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
				commandMap.put( "GROUP_YMD", format.format(date) );
				commandMap.put( "GROUP_SEQ", fdcrAdA9Dao.groupIdMax(commandMap) );
				fdcrAdA9Dao.groupInsert( commandMap );
			}else{
				fdcrAdA9Dao.groupUpdate( commandMap );
				fdcrAdA9Dao.groupMenuDelete( commandMap ); // 조건에 맞는 그룹메뉴 삭제하기
			}
			
			String[] MENU_ID = (String[]) commandMap.get( "MENU_ID" );
			// 그룹메뉴 다시 등록하기
	
			for( int i = 0; i < MENU_ID.length; i++ ){
				String value = MENU_ID[i];
				value = value.substring( value.indexOf( "_" )+1 );
				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "GROUP_YMD", commandMap.get( "GROUP_YMD" ) );
				map.put( "GROUP_SEQ", commandMap.get( "GROUP_SEQ" ) );
				map.put( "MENU_ID", value );
				map.put( "MODI_IDNT", commandMap.get( "MODI_IDNT" ) );
				fdcrAdA9Dao.groupMenuInsert( map );
	
			}
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	/** 
	  * <PRE>
	  * 간략 : 그룹 삭제
	  * 상세 : 그룹 삭제
	  * <PRE>
	  * @see kr.or.copyright.mls.console.menu.inter.FdcrAdB0Service#fdcrAdB0Delete1(java.util.Map)
	  */
	public boolean fdcrAdB0Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			
			String[] GROUP = null;
			if(commandMap.get( "GROUP_VALUE" ) instanceof String[]){
				GROUP = (String[]) commandMap.get( "GROUP_VALUE" );
			} else {
				GROUP = new String[1];
				GROUP[0] = (String) commandMap.get( "GROUP_VALUE" );
			}
			
			for( int i = 0; i < GROUP.length; i++ ){
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "GROUP_YMD", GROUP[i].split( "_" )[0]);
				map.put( "GROUP_SEQ", GROUP[i].split( "_" )[1]);
				
				fdcrAdA9Dao.groupDelete( map );
				fdcrAdA9Dao.groupMenuDelete( map ); // 조건에 맞는 그룹메뉴 삭제하기
	
			}
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	
	
}
