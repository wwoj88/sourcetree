package kr.or.copyright.mls.console.rcept.inter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class 내용 기술
 * 
 * @author : 정병호
 * @since : 20120910
 * @version : 1.0
 * @see: 위탁관리 저작물 보고
 */
public interface FdcrAd16Service{

	/**
	 * 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd16List1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 보고년월 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public List getYyyyMm( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 엑셀다운로드
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd16Down1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 위탁저작물 삭제
	 * 
	 * @param deleteList
	 * @throws Exception
	 */
	public void fdcrAd16Delete( String[] deleteList, Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 이미지 리스트 select
	 * 
	 * @param selectImageList
	 * @throws Exception
	 */
	public void fdcrAd16ImageList( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 미술 리스트 select
	 * 
	 * @param selectImageList
	 * @throws Exception
	 */
	public void fdcrAd16ArtList( Map<String, Object> commandMap ) throws Exception;
	
}
