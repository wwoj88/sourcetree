package kr.or.copyright.mls.console.email;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.support.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 안내메일관리 > 월별 자동메일 발신관리
 * 
 * @author ljh
 */
@Controller
public class FdcrAdA8Controller extends DefaultController{

	@Resource( name = "fdcrAdA8Service" )
	private FdcrAdA8ServiceImpl fdcrAdA8Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA8Controller.class );

	/**
	 * 월별 자동메일 발송 관리 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA8List1.page" )
	public String fdcrAdA8List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		Date date = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		
		commandMap.put( "YEAR", "".equals( StringUtil.nullToEmpty( request.getParameter( "YEAR" ))) ? String.valueOf(calendar.get(Calendar.YEAR)+1) : request.getParameter( "YEAR" ) );
		commandMap.put( "MAILING_MGNT_CD", "" );
		commandMap.put( "MSG_STOR_CD", "".equals(StringUtil.nullToEmpty( request.getParameter( "MSG_STOR_CD" ))) ? "11" : request.getParameter( "MSG_STOR_CD" ) );
		fdcrAdA8Service.fdcrAdA8List1( commandMap );

		model.addAttribute( "commandMap", commandMap);
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_msg_Id", commandMap.get( "ds_msg_Id" ) );
		logger.debug( "월별 자동메일 발송 관리 목록 조회" );
		return "email/fdcrAdA8List1.tiles";
	}
	
	/**
	  * <PRE>
	  * 간략 : 
	  * 상세 : 
	  * <PRE>
	  * @param model
	  * @param commandMap
	  * @param request
	  * @param response
	  * @return
	  * @throws Exception 
	  */
	@RequestMapping( value = "/console/email/fdcrAdA8List1Sub1.page" )
	public String fdcrAdA8List1Sub1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "YEAR", request.getParameter( "YEAR" ) );
		String msgStorCd = request.getParameter( "MSG_STOR_CD" );
		commandMap.put( "MSG_STOR_CD", msgStorCd );
		
		logger.debug(commandMap.toString());
		fdcrAdA8Service.fdcrAdA8List1( commandMap );
		
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_msg_Id", commandMap.get( "ds_msg_Id" ) );
		model.addAttribute( "msgStorCd", msgStorCd );
		return "email/fdcrAdA8List1Sub1";
	}

	/**
	 * 발신내역 수신확인 팝업
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA8Pop1.page" )
	public String fdcrAdA8Pop1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA8Service.fdcrAdA8Pop1( commandMap );

		model.addAttribute( "ds_view", commandMap.get( "ds_view" ) );
		model.addAttribute( "ds_detail", commandMap.get( "ds_detail" ) );
		logger.debug( "발신내역 수신확인 팝업" );
		return "email/fdcrAdA8Pop1.popup";
	}
	
	/**
	 * 발송 관리 저장
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA8Insert1.page" )
	public String fdcrAdA8Insert1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		boolean isSuccess = fdcrAdA8Service.fdcrAdA8Insert1( commandMap );
		returnAjaxString( response, isSuccess );
		logger.debug( "발송 관리 저장" );
		if( isSuccess ){
			return returnUrl(
				model,
				"저장했습니다.",
				"/console/email/fdcrAdA8List1.page");
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/email/fdcrAdA8List1.page");
		}
	}

	/**
	 * 메일 주소록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA8List2.page" )
	public String fdcrAdA8List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "RECV_REJC_YN", "" );
		commandMap.put( "RECV_REJC_YN", "" );
		fdcrAdA8Service.fdcrAdA8List2( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_menu", commandMap.get( "ds_menu" ) );
		logger.debug( "메일 주소록 조회" );
		return "test";
	}

}
