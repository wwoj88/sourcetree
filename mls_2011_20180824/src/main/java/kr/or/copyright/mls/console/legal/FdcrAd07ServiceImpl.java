package kr.or.copyright.mls.console.legal;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd07Service;
import kr.or.copyright.mls.myStat.dao.MyStatDao;

import org.springframework.stereotype.Service;

import com.softforum.xdbe.xCrypto;
import com.tobesoft.platform.data.Dataset;

@Service( "fdcrAd07Service" )
public class FdcrAd07ServiceImpl extends CommandService implements FdcrAd07Service{

	// AdminStatMgntSqlMapDao
	@Resource( name = "fdcrAd06Dao" )
	private FdcrAd06Dao fdcrAd06Dao;

	// AdminStatBoardSqlMapDao
	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	// MyStatSqlMapDao
	@Resource( name = "myStatDao" )
	private MyStatDao myStatDao;

	// AdminCommonSqlMapDao
	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	/**
	 * 법정허락 신청등록(오프라인)등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd07Regi1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String[] WORKS_SEQNS = (String[]) commandMap.get( "WORKS_SEQN" );
			String[] APPLY_TYPE_01S = (String[]) commandMap.get( "APPLY_TYPE_01" );
			String[] APPLY_TYPE_02S = (String[]) commandMap.get( "APPLY_TYPE_02" );
			String[] APPLY_TYPE_03S = (String[]) commandMap.get( "APPLY_TYPE_03" );
			String[] APPLY_TYPE_04S = (String[]) commandMap.get( "APPLY_TYPE_04" );
			String[] APPLY_TYPE_05S = (String[]) commandMap.get( "APPLY_TYPE_05" );
			String[] WORKS_TITLS = (String[]) commandMap.get( "WORKS_TITL" );
			String[] WORKS_KINDS = (String[]) commandMap.get( "WORKS_KIND" );
			String[] WORKS_FORMS = (String[]) commandMap.get( "WORKS_FORM" );
			String[] WORKS_DESCS = (String[]) commandMap.get( "WORKS_DESC" );
			String[] PUBL_YMDS = (String[]) commandMap.get( "PUBL_YMD" );
			String[] PUBL_NATNS = (String[]) commandMap.get( "PUBL_NATN" );
			String[] PUBL_MEDI_CDS = (String[]) commandMap.get( "PUBL_MEDI_CD" );
			String[] PUBL_MEDIS = (String[]) commandMap.get( "PUBL_MEDI" );
			String[] COPT_HODR_NAMES = (String[]) commandMap.get( "COPT_HODR_NAME" );
			String[] COPT_HODR_TELX_NUMBS = (String[]) commandMap.get( "COPT_HODR_TELX_NUMB" );
			String[] COPT_HODR_ADDRS = (String[]) commandMap.get( "COPT_HODR_ADDR" );
			String[] PUBL_MEDI_ETCS = (String[]) commandMap.get( "PUBL_MEDI_ETC" );

			DecimalFormat df = new DecimalFormat( "00" );
			Calendar currentCal = Calendar.getInstance();
			currentCal.add( currentCal.DATE, 0 );

			String applyWriteYmd =
				Integer.toString( currentCal.get( Calendar.YEAR ) ) + df.format( currentCal.get( Calendar.MONTH ) + 1 )
					+ df.format( currentCal.get( Calendar.DAY_OF_MONTH ) );

			int applyWriteSeq = myStatDao.newApplyWriteSeq();

			commandMap.put( "APPLY_WRITE_YMD", applyWriteYmd );
			commandMap.put( "APPLY_WRITE_SEQ", applyWriteSeq );

			/* resd암호화 str 20121106 정병호 */

			xCrypto.RegisterEx(
				"normal",
				2,
				new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
				"pool1",
				"mcst_db",
				"mcst_owner",
				"mcst_table",
				"normal" );
			xCrypto.RegisterEx(
				"pattern7",
				2,
				new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
				"pool1",
				"mcst_db",
				"mcst_owner",
				"mcst_table",
				"pattern7" );

			String sOutput7 = null;
			String sString = (String) commandMap.get( "APPLR_RESD_CORP_NUMB" );
			sOutput7 = xCrypto.Encrypt( "pattern7", sString );

			String sOutput7_1 = null;
			String sString_1 = (String) commandMap.get( "APPLY_PROXY_RESD_CORP_NUMB" );
			sOutput7_1 = xCrypto.Encrypt( "pattern7", sString_1 );

			commandMap.put( "APPLR_RESD_CORP_NUMB", sOutput7 );

			commandMap.put( "APPLY_PROXY_RESD_CORP_NUMB", sOutput7_1 );

			/* resd암호화 str 20121106 정병호 */

			fdcrAd06Dao.adminStatPrpsRegi( commandMap );
			fdcrAd06Dao.adminStatPrpsShisRegi( commandMap );
			/*
			 * for(int i=0; i<ds_ml_file.getRowCount(); i++){ Map dsFileMap =
			 * getMap(ds_ml_file, i); int attachSeqn =
			 * commonDao.getNewAttcSeqn(); String fileName =
			 * ds_ml_file.getColumnAsString(i, "FILE_NAME"); if(fileName != null
			 * && !fileName.equals("")){ String filePath = ""; String realFileNm
			 * = ""; String fileSize = ""; byte[] file = null; Map upload =
			 * null; if(ds_ml_file.getColumn(i, "ATTACH_FILE_CONTENT"+i) == null
			 * || ds_ml_file.getColumn(i,
			 * "ATTACH_FILE_CONTENT"+i).toString().equals("")){ filePath =
			 * ds_ml_file.getColumnAsString(i, "FILE_PATH"); realFileNm =
			 * ds_ml_file.getColumnAsString(i, "REAL_FILE_NAME"); fileSize =
			 * ds_ml_file.getColumnAsString(i, "FILE_SIZE"); } else { file =
			 * ds_ml_file.getColumn(i, "ATTACH_FILE_CONTENT"+i).getBinary();
			 * upload = FileUtil.uploadMiFile(fileName, file); filePath =
			 * upload.get("FILE_PATH").toString(); realFileNm =
			 * upload.get("REAL_FILE_NAME").toString(); fileSize =
			 * ds_ml_file.getColumn(i, "ATTACH_FILE_SIZE"+i).toString(); } Map
			 * fileMap = new HashMap(); fileMap.put("ATTC_SEQN", attachSeqn);
			 * fileMap.put("FILE_ATTC_CD", dsFileMap.get("FILE_ATTC_CD"));
			 * fileMap.put("FILE_NAME_CD", dsFileMap.get("FILE_NAME_CD"));
			 * fileMap.put("FILE_NAME", fileName); fileMap.put("FILE_PATH",
			 * filePath); fileMap.put("FILE_SIZE", fileSize);
			 * fileMap.put("REAL_FILE_NAME", realFileNm);
			 * fileMap.put("RGST_IDNT", dsFileMap.get("RGST_IDNT")); Map
			 * statAttcFileMap = new HashMap();
			 * statAttcFileMap.put("APPLY_WRITE_YMD", applyWriteYmd);
			 * statAttcFileMap.put("APPLY_WRITE_SEQ", applyWriteSeq);
			 * statAttcFileMap.put("ATTC_SEQN", attachSeqn);
			 * statAttcFileMap.put("MODI_IDNT", dsFileMap.get("RGST_IDNT"));
			 * adminStatMgntDao.adminFileInsert(fileMap);
			 * adminStatMgntDao.adminStatAttcFileInsert(statAttcFileMap); } }
			 */

			int attachSeqn = consoleCommonDao.getNewAttcSeqn();

			Map<String, Object> fileInfo = new HashMap<String, Object>();
			if( fileList != null && fileList.size() > 0 ){

				fileInfo = fileList.get( 0 );
				commandMap.put( "ATTC_SEQN", attachSeqn );

				Map fileMap = new HashMap();
				fileMap.put( "ATTC_SEQN", attachSeqn );
				fileMap.put( "FILE_ATTC_CD", "BO" );
				fileMap.put( "FILE_NAME_CD", 99 );
				fileMap.put( "FILE_NAME", fileInfo.get( "F_fileName" ) );
				fileMap.put( "FILE_PATH", fileInfo.get( "F_saveFilePath" ) );
				fileMap.put( "FILE_SIZE", fileInfo.get( "F_filesize" ) );
				fileMap.put( "REAL_FILE_NAME", fileInfo.get( "F_orgFileName" ) );
				fileMap.put( "RGST_IDNT", commandMap.get( "RGST_IDNT" ) );

				Map statAttcFileMap = new HashMap();

				statAttcFileMap.put( "APPLY_WRITE_YMD", applyWriteYmd );
				statAttcFileMap.put( "APPLY_WRITE_SEQ", applyWriteSeq );
				statAttcFileMap.put( "ATTC_SEQN", attachSeqn );
				statAttcFileMap.put( "MODI_IDNT", fileMap.get( "RGST_IDNT" ) );

				// 첨부파일 등록
				fdcrAd06Dao.adminFileInsert( fileMap );
				fdcrAd01Dao.statBord06FileInsert( statAttcFileMap );
			}

			// 명세서만 for문 돌리면됨 -> 명세서 테이블 컬럼20개만 배열[]로 받아오자 controller에서
			for( int i = 0; i < WORKS_SEQNS.length; i++ ){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "APPLY_WRITE_YMD", commandMap.get( "APPLY_WRITE_YMD" ) );
				param.put( "APPLY_WRITE_SEQ", commandMap.get( "APPLY_WRITE_SEQ" ) );
				param.put( "WORKS_SEQN", WORKS_SEQNS[i] );
				param.put( "APPLY_TYPE_01", APPLY_TYPE_01S[i] );
				param.put( "APPLY_TYPE_02", APPLY_TYPE_02S[i] );
				param.put( "APPLY_TYPE_03", APPLY_TYPE_03S[i] );
				param.put( "APPLY_TYPE_04", APPLY_TYPE_04S[i] );
				param.put( "APPLY_TYPE_05", APPLY_TYPE_05S[i] );
				param.put( "WORKS_TITL", WORKS_TITLS[i] );
				param.put( "WORKS_KIND", WORKS_KINDS[i] );
				param.put( "WORKS_FORM", WORKS_FORMS[i] );
				param.put( "WORKS_DESC", WORKS_DESCS[i] );
				param.put( "PUBL_YMD", PUBL_YMDS[i] );
				param.put( "PUBL_NATN", PUBL_NATNS[i] );
				param.put( "PUBL_MEDI_CD", PUBL_MEDI_CDS[i] );
				param.put( "PUBL_MEDI", PUBL_MEDIS[i] );
				param.put( "COPT_HODR_NAME", COPT_HODR_NAMES[i] );
				param.put( "COPT_HODR_TELX_NUMB", COPT_HODR_TELX_NUMBS[i] );
				param.put( "COPT_HODR_ADDR", COPT_HODR_ADDRS[i] );
				param.put( "PUBL_MEDI_ETC", PUBL_MEDI_ETCS[i] );
				fdcrAd06Dao.adminStatWorksInsert( param );
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 법정허락 신청등록(오프라인)일괄등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd07Regi2( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			String[] APPLR_RESD_CORP_NUMBS = (String[]) commandMap.get( "APPLR_RESD_CORP_NUMB" );
			String[] APPLY_PROXY_RESD_CORP_NUMBS = (String[]) commandMap.get( "APPLY_PROXY_RESD_CORP_NUMB" );
			String[] WORKS_SEQNS = (String[]) commandMap.get( "WORKS_SEQN" );
			String[] RGST_IDNTS = (String[]) commandMap.get( "RGST_IDNT" );
			String[] APPLY_WRITE_YMDS = (String[]) commandMap.get( "APPLY_WRITE_YMD" );

			String[] APPLY_WRITE_SEQS = (String[]) commandMap.get( "APPLY_WRITE_SEQ" );
			String[] APPLY_TYPE_01S = (String[]) commandMap.get( "APPLY_TYPE_01" );
			String[] APPLY_TYPE_02S = (String[]) commandMap.get( "APPLY_TYPE_02" );
			String[] APPLY_TYPE_03S = (String[]) commandMap.get( "APPLY_TYPE_03" );
			String[] APPLY_TYPE_04S = (String[]) commandMap.get( "APPLY_TYPE_04" );
			String[] APPLY_TYPE_05S = (String[]) commandMap.get( "APPLY_TYPE_05" );
			String[] WORKS_TITLS = (String[]) commandMap.get( "WORKS_TITL" );
			String[] WORKS_KINDS = (String[]) commandMap.get( "WORKS_KIND" );
			String[] WORKS_FORMS = (String[]) commandMap.get( "WORKS_FORM" );
			String[] WORKS_DESCS = (String[]) commandMap.get( "WORKS_DESC" );
			String[] PUBL_YMDS = (String[]) commandMap.get( "PUBL_YMD" );
			String[] PUBL_NATNS = (String[]) commandMap.get( "PUBL_NATN" );
			String[] PUBL_MEDI_CDS = (String[]) commandMap.get( "PUBL_MEDI_CD" );
			String[] PUBL_MEDIS = (String[]) commandMap.get( "PUBL_MEDI" );
			String[] COPT_HODR_NAMES = (String[]) commandMap.get( "COPT_HODR_NAME" );
			String[] COPT_HODR_TELX_NUMBS = (String[]) commandMap.get( "COPT_HODR_TELX_NUMB" );
			String[] COPT_HODR_ADDRS = (String[]) commandMap.get( "COPT_HODR_ADDR" );
			String[] PUBL_MEDI_ETCS = (String[]) commandMap.get( "PUBL_MEDI_ETC" );

			String rgst_idnt = (String) commandMap.get( "RGST_IDNT" );
			int applyWriteSeq = 0;
			// String[] tmpYmd = "";

			for( int i = 0; i < WORKS_SEQNS.length; i++ ){

				Map<String, Object> param = new HashMap<String, Object>();
				// param.put( "SUPL_ITEM_CD", suplItemCds[i] );

				if( APPLY_WRITE_YMDS[i] != null ){

					String[] tmpYmd = (String[]) commandMap.get( "APPLY_WRITE_YMD" );
					applyWriteSeq = myStatDao.newApplyWriteSeq2( tmpYmd[i] );

					param.put( "APPLY_WRITE_YMD", tmpYmd[i] );
					param.put( "APPLY_WRITE_SEQ", applyWriteSeq );
					param.put( "RGST_IDNT", rgst_idnt );

					xCrypto.RegisterEx(
						"normal",
						2,
						new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
						"pool1",
						"mcst_db",
						"mcst_owner",
						"mcst_table",
						"normal" );
					xCrypto.RegisterEx(
						"pattern7",
						2,
						new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
						"pool1",
						"mcst_db",
						"mcst_owner",
						"mcst_table",
						"pattern7" );

					String sOutput7 = null;
					String sString = (String) commandMap.get( "APPLR_RESD_CORP_NUMB" );
					sOutput7 = xCrypto.Encrypt( "pattern7", sString );

					String sOutput7_1 = null;
					String sString_1 = (String) commandMap.get( "APPLY_PROXY_RESD_CORP_NUMB" );
					sOutput7_1 = xCrypto.Encrypt( "pattern7", sString_1 );

					param.put( "APPLR_RESD_CORP_NUMB", sOutput7 );
					param.put( "APPLY_PROXY_RESD_CORP_NUMB", sOutput7_1 );

					fdcrAd06Dao.adminStatPrpsRegiAll( param );
					fdcrAd06Dao.adminStatPrpsShisRegiAll( param );
					fdcrAd06Dao.adminStatWorksInsertAll( param );

				}else{
					String[] tmpYmd = (String[]) commandMap.get( "APPLY_WRITE_YMD" );
					param.put( "APPLY_WRITE_YMD", tmpYmd[i] );
					param.put( "APPLY_WRITE_SEQ", applyWriteSeq );
					param.put( "RGST_IDNT", rgst_idnt );

					fdcrAd06Dao.adminStatWorksInsertAll( param );

				}
			}

			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 법정허락 신청등록(오프라인)파일 업로드
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd07Upload1( ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			int attachSeqn = consoleCommonDao.getNewAttcSeqn();

			Map<String, Object> fileInfo = new HashMap<String, Object>();
			if( fileList != null && fileList.size() > 0 ){

				fileInfo = fileList.get( 0 );

				Map fileMap = new HashMap();
				fileMap.put( "ATTC_SEQN", attachSeqn );
				fileMap.put( "FILE_ATTC_CD", "BO" );
				fileMap.put( "FILE_NAME_CD", 99 );
				fileMap.put( "FILE_NAME", fileInfo.get( "F_fileName" ) );
				fileMap.put( "FILE_PATH", fileInfo.get( "F_saveFilePath" ) );
				fileMap.put( "FILE_SIZE", fileInfo.get( "F_filesize" ) );
				fileMap.put( "REAL_FILE_NAME", fileInfo.get( "F_orgFileName" ) );

				Map statAttcFileMap = new HashMap();

				statAttcFileMap.put( "ATTC_SEQN", attachSeqn );
				statAttcFileMap.put( "MODI_IDNT", fileMap.get( "RGST_IDNT" ) );

				// 첨부파일 등록
				fdcrAd06Dao.adminFileInsert( fileMap );
				fdcrAd01Dao.statBord06FileInsert( statAttcFileMap );
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	public boolean fdcrAd07Insert( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			
		    DecimalFormat df = new DecimalFormat("00");
		    Calendar currentCal = Calendar.getInstance();
		    currentCal.add(currentCal.DATE, 0);
		    
		    String  applyWriteYmd = Integer.toString(currentCal.get(Calendar.YEAR))
		    + df.format(currentCal.get(Calendar.MONTH) + 1)
		    + df.format(currentCal.get(Calendar.DAY_OF_MONTH));
		    
		    int applyWriteSeq = myStatDao.newApplyWriteSeq();
		    
		    commandMap.put("APPLY_WRITE_YMD", applyWriteYmd);
		    commandMap.put("APPLY_WRITE_SEQ", applyWriteSeq);
		    commandMap.put("RGST_IDNT", ConsoleLoginUser.getUserId());
		    
		    /*resd암호화 str 20121106 정병호*/
		    
		    xCrypto.RegisterEx("normal", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "normal");
		    xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		    
		    String sOutput7 = null;
		    String sString = (String)commandMap.get("APPLR_RESD_CORP_NUMB");
		    sOutput7    = xCrypto.Encrypt("pattern7",sString );

		    String sOutput7_1 = null;
		    String sString_1 = (String)commandMap.get("APPLY_PROXY_RESD_CORP_NUMB");
		    sOutput7_1    = xCrypto.Encrypt("pattern7",sString_1);
		    
		    
		    commandMap.put("APPLR_RESD_CORP_NUMB", sOutput7);
		    
		    commandMap.put("APPLY_PROXY_RESD_CORP_NUMB", sOutput7_1);
		    
		    /*resd암호화 str 20121106 정병호*/
		    
		    fdcrAd06Dao.adminStatPrpsRegiNew(commandMap);
		    fdcrAd06Dao.adminStatPrpsShisRegiNew(commandMap);
		    
		    /* 첨부파일 관련
		    for(int i=0; i<ds_ml_file.getRowCount(); i++){
				Map dsFileMap = getMap(ds_ml_file, i);
				
				int attachSeqn = commonDao.getNewAttcSeqn();
				String fileName = ds_ml_file.getColumnAsString(i, "FILE_NAME");
				
				if(fileName != null && !fileName.equals("")){
	                String filePath = "";
	                String realFileNm = "";
	                String fileSize = "";
	                byte[] file = null;
	                Map upload = null;
	                    
	                if(ds_ml_file.getColumn(i, "ATTACH_FILE_CONTENT"+i) == null || ds_ml_file.getColumn(i, "ATTACH_FILE_CONTENT"+i).toString().equals("")){
				
						filePath = ds_ml_file.getColumnAsString(i, "FILE_PATH");
						realFileNm = ds_ml_file.getColumnAsString(i, "REAL_FILE_NAME");
						fileSize = ds_ml_file.getColumnAsString(i, "FILE_SIZE");
	                } else {
	    		    	file = ds_ml_file.getColumn(i, "ATTACH_FILE_CONTENT"+i).getBinary();
	    		    	upload = FileUtil.uploadMiFile(fileName, file);
	    		    	filePath = upload.get("FILE_PATH").toString();
	    		    	realFileNm = upload.get("REAL_FILE_NAME").toString();
	    		    	fileSize = ds_ml_file.getColumn(i, "ATTACH_FILE_SIZE"+i).toString();
	                }
	                	Map fileMap = new HashMap();
	                	fileMap.put("ATTC_SEQN", attachSeqn);
					fileMap.put("FILE_ATTC_CD", dsFileMap.get("FILE_ATTC_CD"));
					fileMap.put("FILE_NAME_CD", dsFileMap.get("FILE_NAME_CD"));
					fileMap.put("FILE_NAME", fileName);
					fileMap.put("FILE_PATH", filePath);
					fileMap.put("FILE_SIZE", fileSize);
					fileMap.put("REAL_FILE_NAME", realFileNm);
					fileMap.put("RGST_IDNT", dsFileMap.get("RGST_IDNT"));
				
					Map statAttcFileMap = new HashMap();
				
					statAttcFileMap.put("APPLY_WRITE_YMD", applyWriteYmd);
					statAttcFileMap.put("APPLY_WRITE_SEQ", applyWriteSeq);
					statAttcFileMap.put("ATTC_SEQN", attachSeqn);
					statAttcFileMap.put("MODI_IDNT", dsFileMap.get("RGST_IDNT"));
					adminStatMgntDao.adminFileInsert(fileMap);
					adminStatMgntDao.adminStatAttcFileInsert(statAttcFileMap);
				}
		    }
		    */
		    
		    /* 명세서 등록 */
		    String[] worksSeq = null;
		    worksSeq = (String[])commandMap.get("WORKS_SEQN");
		    for(int i=0; i<worksSeq.length; i++){
		    	String[] WORKS_DESC = (String[])commandMap.get("WORKS_DESC");
		    	String[] PUBL_YMD = (String[])commandMap.get("PUBL_YMD");
		    	String[] PUBL_NATN = (String[])commandMap.get("PUBL_NATN");
		    	String[] PUBL_MEDI_CD = (String[])commandMap.get("PUBL_MEDI_CD");
		    	String[] PUBL_MEDI = (String[])commandMap.get("PUBL_MEDI");
		    	String[] COPT_HODR_NAME = (String[])commandMap.get("COPT_HODR_NAME");
		    	String[] COPT_HODR_TELX_NUMB = (String[])commandMap.get("COPT_HODR_TELX_NUMB");
		    	String[] COPT_HODR_ADDR = (String[])commandMap.get("COPT_HODR_ADDR");
				//Map dsWorksMap = getMap(ds_stat_works,i);
		    	commandMap.put("WORKS_SEQN", worksSeq[i]);
		    	commandMap.put("APPLY_WRITE_YMD", applyWriteYmd);
		    	commandMap.put("APPLY_WRITE_SEQ", applyWriteSeq);
		    	commandMap.put("WORKS_DESC", WORKS_DESC[i]);
		    	commandMap.put("PUBL_YMD", PUBL_YMD[i]);
		    	commandMap.put("PUBL_NATN", PUBL_NATN[i]);
		    	commandMap.put("PUBL_MEDI_CD", PUBL_MEDI_CD[i]);
		    	commandMap.put("PUBL_MEDI", PUBL_MEDI[i]);
		    	commandMap.put("COPT_HODR_NAME", COPT_HODR_NAME[i]);
		    	commandMap.put("COPT_HODR_TELX_NUMB", COPT_HODR_TELX_NUMB[i]);
		    	commandMap.put("COPT_HODR_ADDR", COPT_HODR_ADDR[i]);
		    	fdcrAd06Dao.adminStatWorksInsertNew(commandMap);
		    }

			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}
	
	
	/* 법정허락 오프라인 일괄등록
	String rgst_idnt = ConsoleLoginUser.getUserId();
	int applyWriteSeq = 0;
	String tmpYmd = "";

	String APPLY_WRITE_YMD = (String) commandMap.get( "APPLY_WRITE_YMD" ); 

	Map<String, Object> param = new HashMap<String, Object>();

	if( APPLY_WRITE_YMD != null ){

		tmpYmd = (String) commandMap.get( "APPLY_WRITE_YMD" );
		applyWriteSeq = myStatDao.newApplyWriteSeq2( tmpYmd );

		param.put( "APPLY_WRITE_YMD", tmpYmd );
		param.put( "APPLY_WRITE_SEQ", applyWriteSeq );
		param.put( "RGST_IDNT", rgst_idnt );
		
		xCrypto.RegisterEx(
			"normal",
			2,
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
			"pool1",
			"mcst_db",
			"mcst_owner",
			"mcst_table",
			"normal" );
		xCrypto.RegisterEx(
			"pattern7",
			2,
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
			"pool1",
			"mcst_db",
			"mcst_owner",
			"mcst_table",
			"pattern7" );

		String sOutput7 = null;
		String sString = (String) commandMap.get( "APPLR_RESD_CORP_NUMB" );
		sOutput7 = xCrypto.Encrypt( "pattern7", sString );

		String sOutput7_1 = null;
		String sString_1 = (String) commandMap.get( "APPLY_PROXY_RESD_CORP_NUMB" );
		sOutput7_1 = xCrypto.Encrypt( "pattern7", sString_1 );

		param.put( "APPLR_RESD_CORP_NUMB", sOutput7 );
		param.put( "APPLY_PROXY_RESD_CORP_NUMB", sOutput7_1 );

		fdcrAd06Dao.adminStatPrpsRegiAllNew( param );
		fdcrAd06Dao.adminStatPrpsShisRegiAllNew( param );
		fdcrAd06Dao.adminStatWorksInsertAllNew( param );

	}else{
		param.put( "APPLY_WRITE_YMD", tmpYmd );
		param.put( "APPLY_WRITE_SEQ", applyWriteSeq );
		param.put( "RGST_IDNT", rgst_idnt );

		fdcrAd06Dao.adminStatWorksInsertAllNew( param );
	}
	*/

}
