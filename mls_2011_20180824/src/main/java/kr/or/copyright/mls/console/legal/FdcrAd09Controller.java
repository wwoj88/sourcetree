package kr.or.copyright.mls.console.legal;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd09Service;
import kr.or.copyright.mls.myStat.service.MyStatService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 법정허락 > 이용승인신청 승인 공고
 * 
 * @author ljh
 */
@Controller
public class FdcrAd09Controller extends DefaultController{

	@Resource( name = "fdcrAd09Service" )
	private FdcrAd09Service fdcrAd09Service;

	@Resource( name = "myStatService" )
	private MyStatService myStatService;

	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;
	
	// AdminStatBoardSqlMapDao
	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	// AdminCommonSqlMapDao
	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd09Controller.class );

	/**
	 * 이용승인신청 승인 공고 목록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09List1.page" )
	public String fdcrAd09List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String SCH_RECEIPT_NO = EgovWebUtil.getString( commandMap, "SCH_RECEIPT_NO" );
		String SCH_WORKS_DIVS_CD = EgovWebUtil.getString( commandMap, "SCH_WORKS_DIVS_CD" );
		String SCH_TITL = EgovWebUtil.getString( commandMap, "SCH_TITL" );

		if( !"".equals( SCH_RECEIPT_NO ) ){
			SCH_RECEIPT_NO = URLDecoder.decode( SCH_RECEIPT_NO, "UTF-8" );
			commandMap.put( "SCH_RECEIPT_NO", SCH_RECEIPT_NO );
		}
		if( !"".equals( SCH_WORKS_DIVS_CD ) ){
			SCH_WORKS_DIVS_CD = URLDecoder.decode( SCH_WORKS_DIVS_CD, "UTF-8" );
			commandMap.put( "SCH_WORKS_DIVS_CD", SCH_WORKS_DIVS_CD );
		}
		if( !"".equals( SCH_TITL ) ){
			SCH_TITL = URLDecoder.decode( SCH_TITL, "UTF-8" );
			commandMap.put( "SCH_TITL", SCH_TITL );
		}

		commandMap.put( "BORD_CD", "4" );

		ArrayList<Map<String, Object>> list = fdcrAd09Service.fdcrAd09List1( commandMap );

		model.addAttribute( "list", list );
		model.addAttribute( "ds_count", commandMap.get( "totCnt" ) );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );

		System.out.println( "이용승인신청 승인 공고 목록" );
		return "legal/fdcrAd09List1.tiles";
	}

	/**
	 * 이용승인신청 승인 공고 상세
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09View1.page" )
	public String fdcrAd09View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAd09Service.fdcrAd09View1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "detailList" ) );
		model.addAttribute( "fileList", commandMap.get( "fileList" ) );
		model.addAttribute( "ds_supl_list", commandMap.get( "suplList" ) );
		System.out.println( "이용승인신청 승인 공고 상세" );
		return "legal/fdcrAd09View1.tiles";
	}

	/**
	 * 이용승인신청 승인 공고 게시판 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09Download1.page" )
	public void fdcrAd09Download1( ModelMap model,		Map<String, Object> commandMap,		HttpServletRequest request,		HttpServletResponse response ) throws Exception{
		fdcrAd09Service.fdcrAd09View1( commandMap );
		List list = (List) commandMap.get( "fileList" );
		for( int i = 0; i < list.size(); i++ ){
			Map map = (Map) list.get( i );
			String bordCd =  String.valueOf( map.get( "BORD_CD" ));
			String bordSeqn =  String.valueOf(map.get( "BORD_SEQN" ));
			if( bordCd.equals( commandMap.get( "BORD_CD" ) ) && bordSeqn.equals( commandMap.get( "BORD_SEQN" ) ) ){
				String filePath = (String) map.get( "FILE_PATH" );
				String fileName = (String) map.get( "FILE_NAME" );
				String realFileName = (String) map.get( "REAL_FILE_NAME" );
				System.out.println(  filePath);
				System.out.println(  fileName);
				System.out.println(  realFileName);
				download( request, response, filePath + fileName, realFileName );
			}
		}
	}

	/**
	 * 이용승인신청 승인 공고 수정 폼
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09UpdateForm1.page" )
	public String fdcrAd09UpdateForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAd09Service.fdcrAd09UpdateForm1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "detailList" ) );
		model.addAttribute( "ds_file", commandMap.get( "fileList" ) );
		model.addAttribute( "ds_supl_list", commandMap.get( "suplList" ) );

		System.out.println( "이용승인 신청 승인 공고 수정화면" );
		return "legal/fdcrAd09UpdateForm1.tiles";
	}

	/**
	 * 이용승인신청 공고 수정
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09Update1.page" )
	public String fdcrAd09Update1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String[] WORKS_IDS = request.getParameterValues( "WORKS_ID" );
		String[] DIVS_CDS = request.getParameterValues( "DIVS_CD" );
		String[] RECEIPT_NOS = request.getParameterValues( "RECEIPT_NO" );
		String[] TITES = request.getParameterValues( "TITE" );
		String[] ANUC_ITEM_1S = request.getParameterValues( "ANUC_ITEM_1" );
		String[] ANUC_ITEM_2S = request.getParameterValues( "ANUC_ITEM_2" );
		String[] ANUC_ITEM_3S = request.getParameterValues( "ANUC_ITEM_3" );
		String[] ANUC_ITEM_4S = request.getParameterValues( "ANUC_ITEM_4" );
		String[] ANUC_ITEM_5S = request.getParameterValues( "ANUC_ITEM_5" );
		String[] ANUC_ITEM_6S = request.getParameterValues( "ANUC_ITEM_6" );
		String[] ANUC_ITEM_7S = request.getParameterValues( "ANUC_ITEM_7" );
		String[] ATTC_SEQNS = request.getParameterValues( "file1" );

		commandMap.put( "WORKS_ID", WORKS_IDS );
		commandMap.put( "DIVS_CD", DIVS_CDS );
		commandMap.put( "RECEIPT_NO", RECEIPT_NOS );
		commandMap.put( "TITE", TITES );
		commandMap.put( "ANUC_ITEM_1", ANUC_ITEM_1S );
		commandMap.put( "ANUC_ITEM_2", ANUC_ITEM_2S );
		commandMap.put( "ANUC_ITEM_3", ANUC_ITEM_3S );
		commandMap.put( "ANUC_ITEM_4", ANUC_ITEM_4S );
		commandMap.put( "ANUC_ITEM_5", ANUC_ITEM_5S );
		commandMap.put( "ANUC_ITEM_6", ANUC_ITEM_6S );
		commandMap.put( "ANUC_ITEM_7", ANUC_ITEM_7S );
		commandMap.put( "ATTC_SEQN", ATTC_SEQNS );

		// 파일 업로드
		String uploadPath =	new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAd09Service.fdcrAd09Update1( commandMap, fileinfo );
		returnAjaxString( response, isSuccess );
		System.out.println( "이용승인신청 승인 공고 게시판 수정" );
		
		if( isSuccess ){
			return returnUrl( model, "수정했습니다.", "/console/legal/fdcrAd09List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legal/fdcrAd09List1.page" );
		}
	}

	/**
	 * 이용승인신청 승인 공고 삭제
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09Delete1.page" )
	public String fdcrAd09Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		
		commandMap.put( "MODI_IDNT", ConsoleLoginUser.getUserId() );

		boolean isSuccess = fdcrAd09Service.fdcrAd09Delete1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "이용승인신청 승인 공고 삭제" );
		
		if( isSuccess ){
			return returnUrl( model, "삭제했습니다.", "/console/legal/fdcrAd09List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legal/fdcrAd09List1.page" );
		}
	}

	/**
	 * 이용승인신청 승인 공고 게시판 등록 폼
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09WriteForm1.page" )
	public String fdcrAd09WriteForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "BORD_CD", "" );
		commandMap.put( "BORD_SEQN", "" );

		fdcrAd09Service.fdcrAd09RegiForm1( commandMap );

		List detailList = (List) commandMap.get( "detailList" );
		List fileList = (List) commandMap.get( "fileList" );
		List suplList = (List) commandMap.get( "suplList" );
		model.addAttribute( "ds_list", detailList );
		model.addAttribute( "ds_file", fileList );
		model.addAttribute( "ds_supl_list", suplList );

		System.out.println( "이용승인신청 승인 공고 게시판 등록 폼" );
		return "legal/fdcrAd09WriteForm1.tiles";
	}

	/**
	 * 이용승인신청 승인 공고 게시판 등록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09Regi1.page" )
	public String fdcrAd09Regi1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response,
		MultipartHttpServletRequest mreq ) throws Exception{

		commandMap.put( "BORD_CD", "4" );
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );

		String[] WORKS_IDS = request.getParameterValues( "WORKS_ID" );
		String[] DIVS_CDS = request.getParameterValues( "DIVS_CD" );
		String[] RECEIPT_NOS = request.getParameterValues( "RECEIPT_NO" );
		String[] TITES = request.getParameterValues( "TITE" );
		String[] ANUC_ITEM_1S = request.getParameterValues( "ANUC_ITEM_1" );
		String[] ANUC_ITEM_2S = request.getParameterValues( "ANUC_ITEM_2" );
		String[] ANUC_ITEM_3S = request.getParameterValues( "ANUC_ITEM_3" );
		String[] ANUC_ITEM_4S = request.getParameterValues( "ANUC_ITEM_4" );
		String[] ANUC_ITEM_5S = request.getParameterValues( "ANUC_ITEM_5" );
		String[] ANUC_ITEM_6S = request.getParameterValues( "ANUC_ITEM_6" );
		String[] ANUC_ITEM_7S = request.getParameterValues( "ANUC_ITEM_7" );
		String[] ATTC_SEQNS = request.getParameterValues( "file1" );

		commandMap.put( "WORKS_ID", WORKS_IDS );
		commandMap.put( "DIVS_CD", DIVS_CDS );
		commandMap.put( "RECEIPT_NO", RECEIPT_NOS );
		commandMap.put( "TITE", TITES );
		commandMap.put( "ANUC_ITEM_1", ANUC_ITEM_1S );
		commandMap.put( "ANUC_ITEM_2", ANUC_ITEM_2S );
		commandMap.put( "ANUC_ITEM_3", ANUC_ITEM_3S );
		commandMap.put( "ANUC_ITEM_4", ANUC_ITEM_4S );
		commandMap.put( "ANUC_ITEM_5", ANUC_ITEM_5S );
		commandMap.put( "ANUC_ITEM_6", ANUC_ITEM_6S );
		commandMap.put( "ANUC_ITEM_7", ANUC_ITEM_7S );
		commandMap.put( "ATTC_SEQN", ATTC_SEQNS );

		// 파일 업로드
		String uploadPath =	new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAd09Service.fdcrAd09Regi1( commandMap );
		returnAjaxString( response, isSuccess );

		System.out.println( "이용승인신청 승인 공고 게시판 등록" );
		if( isSuccess ){
			fileInsert( mreq, commandMap, null );
			return returnUrl( model, "저장했습니다.", "/console/legal/fdcrAd09List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/legal/fdcrAd09List1.page" );
		}
	}
	
	public void fileInsert( MultipartHttpServletRequest mreq,
		Map<String, Object> commandMap,
		String[] extNames ) throws Exception{
		String savePath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> files = fileUpload( mreq,savePath,null,true);
		for( int i = 0; i < files.size(); i++ ){
			Map<String, Object> file = files.get( i );
			Map<String, Object> param = new HashMap<String, Object>();
			int attachSeqn = consoleCommonDao.getNewAttcSeqn();
			int bordSeqn = fdcrAd01Dao.getMaxBordSeqn();
			//param.put( "MENU_SEQN", EgovWebUtil.getString( commandMap, "MENU_SEQN" ) );
			param.put( "BORD_CD", "4" );
			param.put( "BORD_SEQN", bordSeqn );
			param.put( "ATTC_SEQN", attachSeqn );
			param.put( "FILE_NAME", EgovWebUtil.getString( file, "F_fileName" ) );
			param.put( "FILE_PATH", EgovWebUtil.getString( file, "F_saveFilePath" ) );
			param.put( "FILE_SIZE", file.get( "F_filesize" ) );
			param.put( "REAL_FILE_NAME", EgovWebUtil.getString( file, "F_orgFileName" ) );
			param.put( "RGST_IDNT", ConsoleLoginUser.getUserId());
			fdcrAd01Dao.mlBord02FileInsert( param ); // 첨부파일 등록
			fdcrAd01Dao.statBord02FileInsert( param ); // 공고게시판 첨부파일 등록
		}
	}

	/**
	 * 이용승인신청 승인 공고 일괄등록 엑셀양식 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09Download2.page" )
	public void fdcrAd09Download2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "FILE_URL", "" );
		String filePath = "";
		String fileName = "";

		fileName = "03.승인공고 입력양식.xlsx";
		filePath = "/home/right4me/web/upload/form/03.승인공고 입력양식.xlsx";

		if( null != fileName && !"".equals( fileName ) ){
			download( request, response, filePath, fileName );
		}
	}

	/**
	 * 이용승인신청 승인 공고 일괄등록 엑셀양식 업로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09Upload1.page" )
	public void fdcrAd09Upload1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );
		// TODO 엑셀 조회

		// boolean isSuccess = fdcrAd09Service.fdcrAd09Upload1( fileinfo );
		// returnAjaxString( response, isSuccess );
	}

	/**
	 * 이용승인신청 승인 공고 일괄등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09Regi2.page" )
	public void fdcrAd09Regi2( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "RGST_IDNT", "" );
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "OPEN_IDNT", "" );

		String[] WORKS_IDS = request.getParameterValues( "WORKS_ID" );
		String[] DIVS_CDS = request.getParameterValues( "DIVS_CD" );
		String[] RECEIPT_NOS = request.getParameterValues( "RECEIPT_NO" );
		String[] TITES = request.getParameterValues( "TITE" );
		String[] ANUC_ITEM_1S = request.getParameterValues( "ANUC_ITEM_1" );
		String[] ANUC_ITEM_2S = request.getParameterValues( "ANUC_ITEM_2" );
		String[] ANUC_ITEM_3S = request.getParameterValues( "ANUC_ITEM_3" );
		String[] ANUC_ITEM_4S = request.getParameterValues( "ANUC_ITEM_4" );
		String[] ANUC_ITEM_5S = request.getParameterValues( "ANUC_ITEM_5" );
		String[] ANUC_ITEM_6S = request.getParameterValues( "ANUC_ITEM_6" );
		String[] ANUC_ITEM_7S = request.getParameterValues( "ANUC_ITEM_7" );

		commandMap.put( "WORKS_ID", WORKS_IDS );
		commandMap.put( "DIVS_CD", DIVS_CDS );
		commandMap.put( "RECEIPT_NO", RECEIPT_NOS );
		commandMap.put( "TITE", TITES );
		commandMap.put( "ANUC_ITEM_1", ANUC_ITEM_1S );
		commandMap.put( "ANUC_ITEM_2", ANUC_ITEM_2S );
		commandMap.put( "ANUC_ITEM_3", ANUC_ITEM_3S );
		commandMap.put( "ANUC_ITEM_4", ANUC_ITEM_4S );
		commandMap.put( "ANUC_ITEM_5", ANUC_ITEM_5S );
		commandMap.put( "ANUC_ITEM_6", ANUC_ITEM_6S );
		commandMap.put( "ANUC_ITEM_7", ANUC_ITEM_7S );

		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAd09Service.fdcrAd09Regi2( commandMap, fileinfo );
		returnAjaxString( response, isSuccess );
	}

	/**
	 * 이용승인신청 승인 공고 목록 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd09ExcelDown1.page" )
	public String fdcrAd09ExcelDown1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "FROM_NO", "" );
		commandMap.put( "TO_NO", "" );
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "OBJC_YN", "" );
		commandMap.put( "SCH_WORKS_DIVS_CD", "" );
		commandMap.put( "SCH_STAT_OBJC_CD", "" );
		commandMap.put( "SCH_RECEIPT_NO", "" );
		commandMap.put( "SCH_TITL", "" );

		fdcrAd09Service.fdcrAd09ExcelDown1( commandMap );
		model.addAttribute( "ds_exceldown", commandMap.get( "ds_exceldown" ) );

		System.out.println( "이용승인신청 승인 공고 목록 엑셀다운로드" );
		return "test";
	}

}
