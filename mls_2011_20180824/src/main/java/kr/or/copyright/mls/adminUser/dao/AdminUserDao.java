package kr.or.copyright.mls.adminUser.dao;

import java.util.List;
import java.util.Map;

public interface AdminUserDao {

	// 회원정보 목록조회
	public List userList(Map map);

	//회원정보 상세조회
	public List userDetlList(Map map);
	
	//clms회원 비밀번호수정
	public void updateClmsUserInfo(Map map);
	
	//신탁단체 담당자목록 조회
	public List trstOrgnMgnbList(Map map);	

	//신탁단체 담당자 상세 조회
	public List trstOrgnMgnbDetlList(Map map);		

	//신탁단체 담당자 입력
	public void trstOrgnMgnbDetlInsert(Map map);	
	
	//신탁단체 담당자 수정
	public void trstOrgnMgnbDetlUpdate(Map map);
	
	// 담당자,회원 비밀번호 수정
	public void userMgnbPswdUpdate(Map map);
	
	// 그룹 메뉴 조회 하기
	public List adminGroupMenuInfo(Map map);
	
	// 관리자 아이디 중복체크
	public int adminIdCheck(Map map);
	
	//기관/단체 목록 조회
	public List trstOrgnCoNameList(Map map);
	
	// 대표담당자정보 수정
	public void orgnMgnbUpdate(Map map);
	
	// 대표담당자 존재여부 체크
	public int orgnMgnbCheck(Map map);
	
}
