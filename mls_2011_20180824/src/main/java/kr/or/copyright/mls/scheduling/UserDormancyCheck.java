package kr.or.copyright.mls.scheduling;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.common.userLogin.service.UserDormancyService;
import kr.or.copyright.mls.common.mail.SendMail;
import kr.or.copyright.mls.common.service.CommonService;
import kr.or.copyright.mls.common.service.CommonServiceImpl;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd20Dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

@Controller
public class UserDormancyCheck {
	

	@javax.annotation.Resource(name = "commonService")
	private CommonService commonService = new CommonServiceImpl();
	protected Log log = LogFactory.getLog(this.getClass());
	
	@javax.annotation.Resource(name = "userDormancyService")
	private UserDormancyService userDormancyService;
	
	public void setUserDormancyService( UserDormancyService userDormancyService ){
		this.userDormancyService = userDormancyService;
	}

	public void execute() throws Exception{
System.out.println( "userDormancyService call" );
		
		List<Map> resultList= (List)userDormancyService.userDormancyCheck();
		//System.out.println( "resultList : " +resultList );
		System.out.println( "resultList.size : " +resultList.size() );
		
		System.out.println( "resultList : "+ resultList );
		System.out.println( "resultList.size() : "+ resultList.size() );
		System.out.println( "==================이하 메일========================" );
		System.out.println( "sendMailTest : " );
		HashMap userMap = new HashMap();
		//신청제목
		userMap.put("WORKS_TITL","SEND MAIL TEST");
		userMap.put("MGNT_DIVS", "DISCONNECTED");
		userMap.put("MAIL_TITL", "휴면 계정 전환 안내 ");
		userMap.put("MAIL_TITL_DIVS", "신청");
		
		List list = new ArrayList();
		String to[] = new String[resultList.size()];
		String toName[] = new String[resultList.size()];
		for(int i = 0 ; i < resultList.size() ; i ++) {
			//System.out.println( "resultList.get( i ) : " + resultList.get( i ));
			Map resultUser = resultList.get( i );
			System.out.println( resultUser );
			System.out.println(resultUser.get("MAIL" ) );
			System.out.println( resultUser.get("USER_NAME") );
			
			System.out.println( userMap.get( "MGNT_DIVS" ));
			System.out.println( userMap );
			
			
			to[i] = (String) resultUser.get("MAIL" );
			toName[i] = (String) resultUser.get("USER_NAME");
			
		}
		
		userMap.put("TO", to);
		userMap.put("TO_NAME", toName);
		SendMail.send(userMap);	
		
		//////////////////////////////////////////////////////////////////////////////////////
		
		List<Map> dormancyList = userDormancyService.userDormancyList();
		System.out.println( "dormancyList : " + dormancyList);
		
		userDormancyService.dormancyMove(dormancyList);
		
		System.out.println( "userDormancyService call" );
		
		//System.out.println( "resultList : " +resultList );
		System.out.println( "resultList.size : " +dormancyList.size() );
		
		System.out.println( "resultList : "+ dormancyList );
		System.out.println( "resultList.size() : "+ dormancyList.size() );
		System.out.println( "==================이하 메일========================" );
		System.out.println( "sendMailTest : " );
		HashMap userMap2 = new HashMap();
		//신청제목
		userMap2.put("WORKS_TITL","SEND MAIL TEST");
		userMap2.put("MGNT_DIVS", "DISCONNECTED");
		userMap2.put("MAIL_TITL", "휴면 계정 전환 안내 ");
		userMap2.put("MAIL_TITL_DIVS", "신청");
		
		List list2 = new ArrayList();
		String to2[] = new String[dormancyList.size()];
		String toName2[] = new String[dormancyList.size()];
		for(int i = 0 ; i < dormancyList.size() ; i ++) {
			//System.out.println( "resultList.get( i ) : " + resultList.get( i ));
			Map resultUser = dormancyList.get( i );
			System.out.println( resultUser );
			System.out.println(resultUser.get("MAIL" ) );
			System.out.println( resultUser.get("USER_NAME") );
			
			System.out.println( userMap2.get( "MGNT_DIVS" ));
			System.out.println( userMap2 );
			
			System.out.println( to2[i] );
			System.out.println( toName2[i] );
			to2[i] = (String) resultUser.get("MAIL" );
			toName2[i] = (String) resultUser.get("USER_NAME");
			
		}
		
		userMap2.put("TO", to2);
		userMap2.put("TO_NAME", toName2);
		SendMail.send(userMap2);
	}

}
