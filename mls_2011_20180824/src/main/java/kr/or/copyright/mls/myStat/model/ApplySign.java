package kr.or.copyright.mls.myStat.model;

import kr.or.copyright.mls.common.BaseObject;

public class ApplySign extends BaseObject {
	
	private String applyWriteYmd;   //신청서작성YMD
	private int applyWriteSeq;      //신청서작성SEQ
	private String signDn;          //인증서 사용자ID
	private String signSn;          //인증서 일련번호
	private String signCert;        //인증서 정보
	private String signOrgnData;    //전자서명 원본메세지
	private String signDate;        //전자서명 값
	private String signDttm;        //인증검증일시
	
	
	public int getApplyWriteSeq() {
		return applyWriteSeq;
	}
	public void setApplyWriteSeq(int applyWriteSeq) {
		this.applyWriteSeq = applyWriteSeq;
	}
	public String getApplyWriteYmd() {
		return applyWriteYmd;
	}
	public void setApplyWriteYmd(String applyWriteYmd) {
		this.applyWriteYmd = applyWriteYmd;
	}
	public String getSignCert() {
		return signCert;
	}
	public void setSignCert(String signCert) {
		this.signCert = signCert;
	}
	public String getSignDate() {
		return signDate;
	}
	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}
	public String getSignDn() {
		return signDn;
	}
	public void setSignDn(String signDn) {
		this.signDn = signDn;
	}
	public String getSignDttm() {
		return signDttm;
	}
	public void setSignDttm(String signDttm) {
		this.signDttm = signDttm;
	}
	public String getSignOrgnData() {
		return signOrgnData;
	}
	public void setSignOrgnData(String signOrgnData) {
		this.signOrgnData = signOrgnData;
	}
	public String getSignSn() {
		return signSn;
	}
	public void setSignSn(String signSn) {
		this.signSn = signSn;
	}

	
}