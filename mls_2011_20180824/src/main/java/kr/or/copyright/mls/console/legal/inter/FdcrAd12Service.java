package kr.or.copyright.mls.console.legal.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd12Service{

	/**
	 * 법정허락 대상저작물 관리 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd12List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 법정허락 대상저작물 선택 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd12Delete1( Map<String, Object> commandMap ) throws Exception;

}
