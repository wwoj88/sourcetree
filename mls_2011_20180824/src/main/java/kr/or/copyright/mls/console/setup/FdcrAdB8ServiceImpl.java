package kr.or.copyright.mls.console.setup;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;
import kr.or.copyright.mls.console.setup.inter.FdcrAdB8Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdB8Service" )
public class FdcrAdB8ServiceImpl extends CommandService implements FdcrAdB8Service{

	// OldDao adminStatProcDao
	@Resource( name = "fdcrAd03Dao" )
	private FdcrAd03Dao fdcrAd03Dao;

	/**
	 * 보고저작물 등록기간 관리
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB8List1( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd03Dao.reptMgntList( commandMap );
		commandMap.put( "ds_list_0", list );
	}

	/**
	 * 보고저작물 등록기간 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdB8Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			if( commandMap.get( "YSNO" ).equals( "Y" ) ){
				fdcrAd03Dao.reptMgntUpdate( commandMap );
			}else{
				fdcrAd03Dao.reptMgntUpdateYn( commandMap );
			}
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
}
