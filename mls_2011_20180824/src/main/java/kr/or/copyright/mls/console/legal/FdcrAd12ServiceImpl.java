package kr.or.copyright.mls.console.legal;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd12Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd12Service" )
public class FdcrAd12ServiceImpl extends CommandService implements FdcrAd12Service{

	// AdminStatBoardSqlMapDao
	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	/**
	 * 법정허락 대상저작물 관리 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd12List1( Map<String, Object> commandMap ) throws Exception{

		List pageList = (List) fdcrAd01Dao.statBord05RowList( commandMap ); // 페이징카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		
		// DAO 호출
		List list = (List) fdcrAd01Dao.statBord05List( commandMap );
		//List pageList = (List) fdcrAd01Dao.statBord05RowList( commandMap ); // 페이징카운트
		//List pageCount = (List) fdcrAd01Dao.statBord05RowCount( commandMap ); // 카운트
		//pagination( commandMap, pageList, 0 );
		commandMap.put( "ds_list", list );
		//commandMap.put( "ds_page", pageList );
		//commandMap.put( "ds_count", pageCount );

	}

	/**
	 * 법정허락 대상저작물 선택 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd12Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			//String[] WORKS_IDS = (String[]) commandMap.get( "WORKS_ID" );
			String WORKS_ID = EgovWebUtil.getString( commandMap, "WORKS_ID" );

			//for( int i = 0; i < WORKS_ID.length(); i++ ){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "WORKS_ID", WORKS_ID );
				fdcrAd01Dao.statBord07Update( param );
			//}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
