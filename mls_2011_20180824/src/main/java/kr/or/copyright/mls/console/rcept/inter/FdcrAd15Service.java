package kr.or.copyright.mls.console.rcept.inter;

import java.util.Map;

/**
 * Class 내용 기술
 * 
 * @author : 정병호
 * @since : 20120910
 * @version : 1.0
 * @see: 위탁관리 저작물 보고
 */
public interface FdcrAd15Service{

	/**
	 * 위탁관리저작물 보고현황 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 위탁관리저작물 장르별 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15List2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 위탁관리저작물 월별 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15List3( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 월별 보고현황 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15List4( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 월별 보고현황 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd15List5( Map<String, Object> commandMap ) throws Exception;

	public void fdcrAd15Down1( Map<String, Object> commandMap ) throws Exception;

	public void fdcrAd15Down2( Map<String, Object> commandMap ) throws Exception;

}
