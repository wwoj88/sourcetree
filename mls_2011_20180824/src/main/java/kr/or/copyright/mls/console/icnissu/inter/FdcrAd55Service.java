package kr.or.copyright.mls.console.icnissu.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd55Service{

	/**
	 * 개인저작물 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd55List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 개인저작물 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd55View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * ICN 발급번호 채번
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd55IcnInfo1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * ICN 발급번호 저장
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd55Insert1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * ICN 정보 저장
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd55Insert2( Map<String, Object> commandMap ) throws Exception;

}
