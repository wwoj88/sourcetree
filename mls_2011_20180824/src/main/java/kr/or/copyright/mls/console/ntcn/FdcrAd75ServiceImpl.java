package kr.or.copyright.mls.console.ntcn;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ntcn.inter.FdcrAd75Dao;
import kr.or.copyright.mls.console.ntcn.inter.FdcrAd75Service;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service( "fdcrAd75Service" )
public class FdcrAd75ServiceImpl extends CommandService implements FdcrAd75Service{

	private Logger logger = Logger.getLogger( this.getClass() );
	
	@Resource( name = "fdcrAd75Dao" )
	private FdcrAd75Dao fdcrAd75Dao;

	/**
	 * 공통 첨부파일 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> selectAttachFileList( Map<String, Object> commandMap ) throws SQLException{
		return fdcrAd75Dao.selectAttachFileList( commandMap );
	}

	/**
	 * 공통 첨부파일 정보
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> selectAttachFileInfo( Map<String, Object> commandMap ) throws SQLException{
		return fdcrAd75Dao.selectAttachFileInfo( commandMap );
	}

	/**
	 * 첨부파일 등록
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public boolean insertBoardFile( Map<String, Object> commandMap ) throws SQLException{
		boolean isSuccess = false;
		try{
			int result = fdcrAd75Dao.insertBoardFile( commandMap );
			if( result > 0 ){
				isSuccess = true;
			}else{
				isSuccess = false;
			}
		}
		catch( SQLException e ){
			logger.error( e.getMessage() );
		}
		return isSuccess;
	}

	/**
	 * 게시물 목록 카운트
	 * 
	 * @return
	 * @throws SQLException
	 */
	public int selectListCount( Map<String, Object> commandMap ) throws SQLException{
		return fdcrAd75Dao.selectListCount( commandMap );
	}

	/**
	 * 게시물 목록 정보
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> selectList( Map<String, Object> commandMap ) throws SQLException{
		return fdcrAd75Dao.selectList( commandMap );
	}

	/**
	 * 게시물 조회
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> selectView( Map<String, Object> commandMap ) throws SQLException{
		return fdcrAd75Dao.selectView( commandMap );
	}

	/**
	 * 등록
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public boolean insertBoardInfo( Map<String, Object> commandMap ) throws SQLException{
		boolean isSuccess = false;
		try{
			int result = fdcrAd75Dao.insertBoardInfo( commandMap );
			commandMap.put( "BORD_SEQN", result );
			if( result > 0 ){
				isSuccess = true;
			}else{
				isSuccess = false;
			}
		}
		catch( SQLException e ){
			logger.error( e.getMessage() );
			e.printStackTrace();
		}
		return isSuccess;
	}

	/**
	 * 수정
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public boolean updateBoardInfo( Map<String, Object> commandMap ) throws SQLException{
		boolean isSuccess = false;
		try{
			int result = fdcrAd75Dao.updateBoardInfo( commandMap );
			if( result > 0 ){
				isSuccess = true;
			}else{
				isSuccess = false;
			}
		}
		catch( SQLException e ){
			logger.error( e.getMessage() );
		}
		return isSuccess;
	}

	/**
	 * 삭제
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public boolean deleteBoardInfo( Map<String, Object> commandMap ) throws SQLException{
		boolean isSuccess = false;
		try{
			int result = fdcrAd75Dao.deleteBoardInfo( commandMap );
			if( result > 0 ){
				isSuccess = true;
			}else{
				isSuccess = false;
			}
		}
		catch( SQLException e ){
			logger.error( e.getMessage() );
		}
		return isSuccess;
	}

	/**
	 * 삭제
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public boolean deleteBoardFile( Map<String, Object> commandMap ) throws SQLException{
		boolean isSuccess = false;
		try{
			int result = fdcrAd75Dao.deleteBoardFile( commandMap );
			if( result > 0 ){
				isSuccess = true;
			}else{
				isSuccess = false;
			}
		}
		catch( SQLException e ){
			logger.error( e.getMessage() );
		}
		return isSuccess;
	}

}
