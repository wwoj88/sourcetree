package kr.or.copyright.mls.adminStatMgnt.service;

public interface AdminStatMgntService {

	public void adminStatPrpsList() throws Exception;
	
	public void adminStatPrpsDetl() throws Exception;
	
	//법정허락 신청 삭제
	public void adminStatPrpsDel() throws Exception;
	
	public void adminStatPrpsHist() throws Exception;
	
	// adminStatPrpsWorksHist 조회
	public void adminStatPrpsWorksHist() throws Exception;
	
	// adminStatPrpsChange 조회
	public void adminStatPrpsChange() throws Exception;
	
	// adminStatPrpsWorksChange 조회
	public void adminStatPrpsWorksChange() throws Exception;
	
	// adminStatPrpsRegi 법정허락 관리자 등록 20120829 정병호
	public void adminStatPrpsRegi() throws Exception;
	
	//adminStatPrpsUpte 법정허락 관리자 수정 2014-11-06 이병원
	public void adminStatPrpsUpte() throws Exception;
	
	//adminStatPrpsRegiAll 법정허락 관리자 일괄등록 20141020 이병원
	public void adminStatPrpsRegiAll() throws Exception;
	
	//중복데이터 확인
	public void adminStatPrpsSearch() throws Exception; 
	
	public void cPayList() throws Exception;
	
}
