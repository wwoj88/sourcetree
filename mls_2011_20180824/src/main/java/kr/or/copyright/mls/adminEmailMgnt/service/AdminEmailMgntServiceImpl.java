package kr.or.copyright.mls.adminEmailMgnt.service;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import kr.or.copyright.mls.adminEmailMgnt.dao.AdminEmailMgntDao;
import kr.or.copyright.mls.adminStatProc.dao.AdminStatProcDao;
import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.StringUtil;

import com.ibatis.dao.client.Dao;
import com.tobesoft.platform.data.Dataset;

@Service("AdminEmailMgntService")
public class AdminEmailMgntServiceImpl extends BaseService implements AdminEmailMgntService {
	
	@Autowired
	private AdminEmailMgntDao adminEmailMgntDao;
	
	public void setAdminEmailMgntDao(AdminEmailMgntDao adminEmailMgntDao){
		this.adminEmailMgntDao = adminEmailMgntDao;
	}
	
	@Autowired
	private AdminStatProcDao adminStatProcDao;
	
	public void setAdminStatProcDao(AdminStatProcDao adminStatProcDao) {
		this.adminStatProcDao = adminStatProcDao;
	}

	//메일 주소록 불러오기
	public void selectEmailAddrList() throws Exception {
		Map map = new HashMap();
		
		List list2 = (List) adminEmailMgntDao.selectEmailAddrGroupList(map);		
		List list = (List) adminEmailMgntDao.selectEmailAddrList(map);
		
		addList("ds_list", list);
		addList("ds_menu", list2);
		
	}
	
	//메일 저장
	public void insertEmail() throws Exception {
		
		Dataset ds_mail = getDataset("ds_mail");
		Dataset ds_send = getDataset("ds_send");
		Dataset ds_ref = getDataset("ds_ref");
		Dataset ds_detail = getDataset("ds_detail");
		
		String row_status;
		try {

			int msgIdSeq = adminEmailMgntDao.selectMaxMsgIdSeq();	
			
			Map map = getMap(ds_mail, 0);
			map.put("MSG_ID", msgIdSeq);
			adminEmailMgntDao.insertEmailMsg(map);
			
			//detail  INSERT,  UPDATE처리
			for( int i=0;i<ds_detail.getRowCount();i++ )
			{

				row_status = ds_detail.getRowStatus(i);

				Map fileMap = getMap(ds_detail, i );
				
				if( row_status.equals("insert") == true )
				{	
					byte[] file = ds_detail.getColumn(i, "CLIENT_FILE").getBinary();
					
					String fileName = ds_detail.getColumnAsString(i, "REAL_FILE_NAME");
					
					Map upload = FileUtil.uploadMiFile(fileName, file,"mail/");
					
//					map.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
					fileMap.put("MSG_ID", msgIdSeq);
					fileMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
					fileMap.put("FILE_PATH", upload.get("FILE_PATH"));
					fileMap.put("FILE_NAME", fileName);
				
					adminEmailMgntDao.insertMailAttc(fileMap);
				}
			}
			
			for(int i=0; i<ds_send.getRowCount(); i++){
	    	    Map map2 = getMap(ds_send, i);
	    	    map2.put("MSG_ID", msgIdSeq);
	    	    map2.put("RECV_SEQN", i+1);
	    	    adminEmailMgntDao.insertEmailRecvInfo(map2); 	   
	    	}
			for(int j=0;j<ds_ref.getRowCount();j++){
				 Map map3 = getMap(ds_ref, j);
				 map3.put("MSG_ID", msgIdSeq);
				 map3.put("RECV_SEQN", j+1);
				 adminEmailMgntDao.insertEmailRecvInfo(map3);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	//메일 메시지 목록 불러오기
	public void selectEmailMsgList() throws Exception {
		Dataset ds_condition = getDataset("ds_condition");		
		Map map = getMap(ds_condition,0);
		
		List list = (List) adminEmailMgntDao.selectEmailMsgList(map);	
		addList("ds_list", list);
	}
	
	//메일 메세지 상세보기
	public void selectEmailMsg() throws Exception {	
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition,0);
		
		List list = (List) adminEmailMgntDao.selectEmailMsg(map);
		List list2= (List) adminEmailMgntDao.selectEmailAddrListByMsgId(map);
		List fileList= (List) adminEmailMgntDao.selectMailAttcList(map);
		
		addList("ds_mail", list);
		addList("ds_send2", list2);
		addList("ds_detail", fileList);
		
	}
	//메일 메시지 수정
	public void updateEmail() throws Exception {
		
		Dataset ds_mail = getDataset("ds_mail");
		Dataset ds_send = getDataset("ds_send");
		Dataset ds_condition = getDataset("ds_condition");
		Dataset ds_ref = getDataset("ds_ref");
		Dataset ds_detail = getDataset("ds_detail");
		
		String row_status;
		try {
			boolean flag = false;
			Map tempMap = getMap(ds_condition, 0);

			
			Map map = getMap(ds_mail, 0);
			
			int msgIdSeq = Integer.parseInt((String) (tempMap.get("MSG_ID")==null? "0":tempMap.get("MSG_ID")));
			map.put("MSG_ID", msgIdSeq);
			
			adminEmailMgntDao.updateEmailMsg(map);
			
			//첨부파일 delete 처리
			for( int i = 0 ; ds_detail != null && i< ds_detail.getDeleteRowCount() ; i++ )
			{
				Map deleteMap = getDeleteMap(ds_detail, i );
				adminEmailMgntDao.deleteMailAttc(deleteMap); 
				
				String fileName =  ds_detail.getDeleteColumn(i, "REAL_FILE_NAME").toString();
				
				 try 
				 {	
					 File file = new File((String)deleteMap.get("FILE_PATH"), fileName);
				     if (file.exists()) 
				     { 
				    	 file.delete(); 
				     }
				  } 
				  catch (Exception e) 
				  {
				   e.printStackTrace();
				  } 
	 	    }		
			
			//detail  INSERT,  UPDATE처리		
			for( int i=0;i<ds_detail.getRowCount();i++ )
			{

				row_status = ds_detail.getRowStatus(i);

				Map fileMap = getMap(ds_detail, i );
				if( row_status.equals("insert") == true )
				{	
					byte[] file = ds_detail.getColumn(i, "CLIENT_FILE").getBinary();
					
					String fileName = ds_detail.getColumnAsString(i, "REAL_FILE_NAME");
					
					Map upload = FileUtil.uploadMiFile(fileName, file,"mail/");
					
//					map.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
					fileMap.put("MSG_ID", msgIdSeq);
					fileMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
					fileMap.put("FILE_PATH", upload.get("FILE_PATH"));
					fileMap.put("FILE_NAME", fileName);
				
					adminEmailMgntDao.insertMailAttc(fileMap);
				}
			}
			
			flag = adminEmailMgntDao.deleteEmailRecvInfo(tempMap);			
			if(flag){
				for(int i=0; i<ds_send.getRowCount(); i++){
		    	    Map map2 = getMap(ds_send, i);
		    	    map2.put("MSG_ID", msgIdSeq);
		    	    map2.put("RECV_SEQN", i+1);
		    	    adminEmailMgntDao.insertEmailRecvInfo(map2); 	   
		    	}
				
				for(int j=0;j<ds_ref.getRowCount();j++){
					 Map map3 = getMap(ds_ref, j);
					 map3.put("MSG_ID", msgIdSeq);
					 map3.put("RECV_SEQN", j+1);
					 adminEmailMgntDao.insertEmailRecvInfo(map3);
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	//메일 리스트 삭제
	public void deleteEmailMsg() throws Exception{
		Dataset ds_list = getDataset("ds_list");
		for(int i=0;i<ds_list.getRowCount();i++){
			Map map = getMap(ds_list, i);
			adminEmailMgntDao.deleteEmailMsg(map);
		}
		
	}
	
	public void preSendEmail() throws Exception{
		
		Dataset ds_mail = getDataset("ds_mail");//메일내용
		Dataset ds_all_send = getDataset("ds_all_send");//수신자 주소록
		Dataset ds_ref = getDataset("ds_ref");//참조 주소록
		Dataset ds_detail = getDataset("ds_detail");//첨부파일
		Map mailMap = getMap(ds_mail, 0);	
		
		List<Map> sendList = new ArrayList();
		List<Map> fileList = new ArrayList();
		String row_status;
		//첨부파일 insert
		for( int i=0;i<ds_detail.getRowCount();i++ )
		{

			row_status = ds_detail.getRowStatus(i);
			Map fileMap = getMap(ds_detail, i );
			if( row_status.equals("insert") == true )
			{	
				// 메일보내기 폼에서 바로 파일첨부를 해서 전송하는 경우
				byte[] file = ds_detail.getColumn(i, "CLIENT_FILE").getBinary();
				
				String fileName = ds_detail.getColumnAsString(i, "REAL_FILE_NAME");
				
				Map upload = FileUtil.uploadMiFile(fileName, file,"mail/");
				
//				map.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
				/*
				fileMap.put("MSG_ID", msgIdSeq);
				adminEmailMgntDao.insertMailAttc(fileMap); */
				fileMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				fileMap.put("FILE_PATH", upload.get("FILE_PATH"));
				fileMap.put("FILE_NAME", fileName);
				
				fileList.add(fileMap);
			}else if(row_status.equals("normal")==true){
				// 메일 저장함에 있던 파일을 목록으로 불러와서 전송하는 경우, 해당 파일을 복사해서 다시 넣어준다.
				String fileFullPath = (String)fileMap.get("FILE_PATH")+(String)fileMap.get("REAL_FILE_NAME");
				FileInputStream in  = new FileInputStream(fileFullPath);
				int iSize = (int) Double.parseDouble(String.valueOf(fileMap.get("FILE_SIZE")));
				byte[] file = new byte[iSize];
				int count=0;
				while((count=in.read(file))!=-1){				
					
				}
				
				String fileName = ds_detail.getColumnAsString(i, "REAL_FILE_NAME");
				
				Map upload = FileUtil.uploadMiFile(fileName, file,"mail/");
				
//				map.put("REAL_FILE_NAME", upload.get("ATTC_FILE"));		// 파일저장명 put
				/*
				fileMap.put("MSG_ID", msgIdSeq);
				adminEmailMgntDao.insertMailAttc(fileMap); */
				fileMap.put("REAL_FILE_NAME", upload.get("REAL_FILE_NAME"));
				fileMap.put("FILE_PATH", upload.get("FILE_PATH"));
				
				fileList.add(fileMap);
			}
			
		}
		if(fileList.size()!=0){
			mailMap.put("fileList", fileList);
		}

		
		StringBuffer sb = new StringBuffer();				
		sb.append("				<tr> \n");
		sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\"> \n");
		sb.append("					<p style=\"margin:10px 0 0 0;\">"+StringUtil.appendHtmlBr((String)mailMap.get("CONTENT"))+"</p>	\n");
		sb.append("					</td>	\n");
		sb.append("				</tr>	\n");
		
		// 첨부파일 다운로드 태그 생성
		if(fileList.size()!=0){
			sb.append("				<tr>	\n");
			sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">	\n");
			sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">첨부파일</p>	\n");
			sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
			
			for( int i=0;i<fileList.size();i++){
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> "
						+"<a href=\""+Constants.DOMAIN_HOME+"/board/board.do?method=fileDownLoad&filePath="+fileList.get(i).get("FILE_PATH")+"&fileName="+fileList.get(i).get("FILE_NAME")
						+"&realFileName="+fileList.get(i).get("REAL_FILE_NAME")+"\">"+fileList.get(i).get("FILE_NAME")+"</a></li>	\n");	 	
			}
			sb.append("					</ul>	\n");
			sb.append("					</td>	\n");
			sb.append("				</tr>	\n");
			
		}
		
		String contentHtml=new String(sb);
		
		mailMap.put("CONTENT_HTML", contentHtml);
		mailMap.put("MAIL_REJC_CD", "1");//수신거부코드
		
		for(int i=0;i<ds_all_send.getRowCount();i++){
			Map sendMap = getMap(ds_all_send, i);
			sendList.add(sendMap);		
		}
		
		for(int j=0;j<ds_ref.getRowCount();j++){
			 Map map3 = getMap(ds_ref, j);
			 sendList.add(map3);
		}
		
		
		sendEmail(sendList,mailMap);
		
	}
	/*	mailMap 필요 컬럼	  	
	 *	MSG_STOR_CD - 메세지함 구분코드
	 *	TITLE - 메일제목
	 *	CONTENT - 메일내용 txt
	 *	CONTENT_HTML - 메일내용 HTML
	 *	RGST_IDNT - 발신자 아이디
	 *	SEND_MAIL_ADDR - 발신자 메일주소
	 *	SUPL_ID - 보완내역 발신시에만 필요
	 * 
	 *  sendList Map 필요 컬럼
	 *  RECV_CD - 수신구분코드
	 *  RECV_MAIL_ADDR - 수신자 메일 주소
	 *  RECV_NAME - 수신자 이름
	 *  SEND_MAIL_ADDR - 발신자 메일주소(mailMap에서 얻음)	   
	 */
	public int sendEmail(List<Map> sendList, Map mailMap) throws Exception {
		
		String host = Constants.MAIL_SERVER_IP; // smtp서버
		//	String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
		String from = StringUtil.nullToEmpty((String)mailMap.get("SEND_MAIL_ADDR"));
		if(from.equals("")){
			from = Constants.SYSTEM_MAIL_ADDRESS;
		}
		String fromName = Constants.SYSTEM_NAME;
		String subject = "[저작권찾기] "+(String)mailMap.get("TITLE");
		String contentHtml = (String)mailMap.get("CONTENT_HTML");
		String mailRejcCd = (String)mailMap.get("MAIL_REJC_CD");//수신거부코드
		
		int iResult = 0;	
		
		int msgIdSeq=adminEmailMgntDao.selectMaxMsgIdSeq();	//메일함 seq
		Date startDate =  new Date();
		Date endDate = null;
		
		//발신함 메일 저장
		mailMap.put("MSG_ID", msgIdSeq);
		mailMap.put("START_DTTM_TRNS",startDate);		
		adminEmailMgntDao.insertEmailMsg(mailMap);			
		
		//첨부파일 저장
		
		List<Map> fileList = new ArrayList<Map>();
		
		if(mailMap.get("fileList")!=null){
			fileList = (List<Map>) mailMap.get("fileList");
		}
		
		for(int i=0;i<fileList.size();i++){
			Map fileMap = fileList.get(i);
			fileMap.put("MSG_ID", msgIdSeq);
			adminEmailMgntDao.insertMailAttc(fileMap);	
		}
		
		
		try {
			
			for(int i=0;i<sendList.size();i++){
				String to = StringUtil.nullToEmpty((String)sendList.get(i).get("RECV_MAIL_ADDR")); // 수신EMAIL
				String toName = StringUtil.nullToEmpty((String)sendList.get(i).get("RECV_NAME")); // 수신인
			
				MailInfo info = new MailInfo();
				info.setFrom(from);
				info.setFromName(fromName);
				info.setHost(host);
				info.setEmail(to);
				info.setEmailName(toName);
				info.setSubject(subject);
				info.setMessage(contentHtml);
				info.setMailRejcCd(mailRejcCd);
				
				
				//메일발신내역 ID 셋팅 = 메세지함ID+메일주소앞부분+일련번호
				String id[] = to.split("@");
				String encodeId = ""+msgIdSeq+id[0]+i;
	
				/*if(mailMap.get("MSG_STOR_CD").equals("2")){//발신함 저장 메세지만 메일수신확인소스 삽입
					StringBuffer sb = new StringBuffer();
					sb.append("<img src=\"http://new.right4me.or.kr/mail/updateEmailRecv.do?send_msg_id="+encodeId+"\" width=\"0\" height=\"0\">");
					
					info.setMailReadHtml(new String(sb));
				}*/
				
				StringBuffer sb = new StringBuffer();
				sb.append("<img src=\""+Constants.DOMAIN_HOME+"/mail/updateEmailRecv.do?send_msg_id="+encodeId+"\" width=\"0\" height=\"0\">");
				
				
				info.setMailReadHtml(new String(sb));
				
				sendList.get(i).put("MSG_ID", msgIdSeq);
				sendList.get(i).put("SEND_MAIL_ADDR",from);
				sendList.get(i).put("SEND_MSG_ID", encodeId);
				sendList.get(i).put("RECV_SEQN", i+1);//수신자정보 수신자 순번
				
				
				info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
				MailManager manager = MailManager.getInstance();
				if(i%100==0&&i!=0){
					System.out.println(i+"번째 메일입니다. 잠시 후 발송됩니다.\n");
					try {
						Thread.sleep(5*1000*60);
						info = manager.sendMail(info);
					} catch (Exception e) {
						e.printStackTrace();
					}					
					
				}else{
					info = manager.sendMail(info);
				}
				
				if (info.isSuccess()) {
					System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());					
					sendList.get(i).put("SEND_DTTM", info.getSendDttm());
				} else {
					System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
					//운영에서는 주석처리하세요!
					//sendList.get(i).put("SEND_DTTM", info.getSendDttm());
				}
			// HTML 확인을 위한 임시테이블에 메일전송 정보 넣기(운영,개발서버에서는 주석처리)
/*				HashMap insertMap = new HashMap();
				insertMap.put("EMAIL_TITLE", info.getSubject());
				insertMap.put("EMAIL_DESC", info.getMessage());
				insertMap.put("EMAIL_ADDR", to);
				adminStatProcDao.insertMailList(insertMap);			
			*/
				
				
			}
			//발신함 발신완료시간 업데이트 
			endDate = new Date();		
			mailMap.put("END_DTTM_TRNS", endDate);
			adminEmailMgntDao.updateEndDttmMsg(mailMap);
			
			
			
			for(int i=0; i<sendList.size(); i++){				
	    	   adminEmailMgntDao.insertEmailRecvInfo(sendList.get(i));//수신자 정보 저장
	    	   adminEmailMgntDao.insertSendList(sendList.get(i));//메일 발신내역 저장	    	       
	    	}
			
			HashMap insertMap = new HashMap();
			
			//보완내역 메일발신내역 등록
			String msgStorCd = StringUtil.nullToEmpty((String)mailMap.get("MSG_STOR_CD"));
			if("4".equals(msgStorCd)||"5".equals(msgStorCd)||"6".equals(msgStorCd))	{//보완내역 발신함 코드
				List<Map> suplList = new ArrayList();
				
				for(int j=0;j<sendList.size();j++){
					Map suplMap = new HashMap();
					suplMap.put("SUPL_ID",mailMap.get("SUPL_ID"));
					suplMap.put("MSG_STOR_CD",msgStorCd);
					suplMap.put("SEND_MSG_ID",sendList.get(j).get("SEND_MSG_ID"));
					suplList.add(suplMap);
				}
				
				for(int k=0;k<suplList.size();k++){
					adminEmailMgntDao.insertSuplList(suplList.get(k));						
				}				
				
			}
			String malingMgntCd= StringUtil.nullToEmpty((String)mailMap.get("MAILING_MGNT_CD"));
			//자동메일링 관리 메일발신내역 등록
			if("1".equals(malingMgntCd)||"2".equals(malingMgntCd))	{
				Map map = new HashMap();
				long time = System.currentTimeMillis();         
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
				
				map.put("MAILING_MGNT_CD", malingMgntCd);
				map.put("YYYYMM",sdf.format(time));
				map.put("MSG_ID",msgIdSeq);
				map.put("START_DTTM", startDate);
				map.put("END_DTTM", endDate);
				adminEmailMgntDao.updateSendScMailng(map);
			}
			
			iResult = 1;
			
		}catch (Exception e) {
			iResult = -1;
		}
		finally {
			return iResult;
		}
		
	}

	
	public void selectSendEmailList() throws Exception {
		
		List list = (List) adminEmailMgntDao.selectSendEmailList();	
		addList("ds_list", list);
		
	}
	public void selectSendEmailView() throws Exception {
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition,0);
		
		List list = (List) adminEmailMgntDao.selectSendEmailView(map);	
		List fileList= (List) adminEmailMgntDao.selectMailAttcList(map);
		
		addList("ds_view", list);
		addList("ds_detail", fileList);
		
	}
	
	//업체구분별 메일 주소록 불러오기(신탁단체/대리중개, 수신거부제외등)
	public void selectAllEmailAddrList() throws Exception {
		Dataset ds_all = getDataset("ds_all");
		List list = new ArrayList();
		
		for(int i=0;i<ds_all.getRowCount();i++){
			Map map = getMap(ds_all,i);
			List templist = (List) adminEmailMgntDao.selectEmailAddrList(map);
			list.addAll(templist);
		}			
		addList("ds_all_send", list);
		
	}
	public void selectScMailingList() throws Exception{
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition,0);
		List list = (List) adminEmailMgntDao.selectScMailingList(map);
		List list2 = (List) adminEmailMgntDao.selectScMailingMsgId(map);
		addList("ds_list", list);
		addList("ds_msg_Id", list2);
		
	}
	

	public void insertUpdateScMailing() throws Exception{
		Dataset ds_list = getDataset("ds_list");
		Dataset ds_msg_Id = getDataset("ds_msg_Id");
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_msg_Id,0);
		Map map2 = getMap(ds_condition,0);
		for(int i=0;i<ds_list.getRowCount();i++){
			Map map3 = getMap(ds_list,i);
			map3.put("MSG_ID",map.get("MSG_ID"));
			map3.put("MAILING_MGNT_CD",map2.get("MAILING_MGNT_CD"));
			
			int count=adminEmailMgntDao.selectScMailingCount(map3);
			
			if(count>0){
				adminEmailMgntDao.updateScMailng(map3);			
			}else{
				map3.put("STAT_CD",0);
				adminEmailMgntDao.insertScMailing(map3);
			}
		}
		
	}
	//매월 자동메일 스케쥴링 전송 체크
	public boolean preCheckScheduling(String cd) throws Exception{
		
		boolean check = false;
		Map map = new HashMap();
		
		long time = System.currentTimeMillis();         
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		
		map.put("MAILING_MGNT_CD", cd);
		map.put("YYYYMM",sdf.format(time));
		
		List list= (List)adminEmailMgntDao.selectScMailing(map);
		Map listMap = (HashMap)list.get(0);
		if("Y".equals((String)listMap.get("USE_YSNO"))){
			check = true;
		}

		return check;
			
		
	}	
	
	public void monthMailScheduling() throws Exception{		
		boolean bFlag = false;	// 스케쥴링 실행 플래그
		
		
		try {
			/* 위탁관리 저작물 보고 안내 자동메일링 */
			bFlag=preCheckScheduling("1");
			if(bFlag){
				Map mailMap = new HashMap();
				List<Map> sendList = new ArrayList<Map>();

				Map map = new HashMap();
				map.put("MSG_STOR_CD","11");//검색을 위해 코드값 넣기
				List list=(List) adminEmailMgntDao.selectEmailMsg(map);//메일함 메세지 불러오기
				mailMap = (Map)list.get(0);
				
				StringBuffer sb = new StringBuffer();				
				sb.append("				<tr>");
				sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">");
				sb.append("					<p>"+StringUtil.appendHtmlBr((String)mailMap.get("CONTENT"))+"</p>");
				
				sb.append("					</td>");
				sb.append("				</tr>");
				String contentHtml=new String(sb);
				
				mailMap.put("CONTENT_HTML", contentHtml);//메일 내용
				mailMap.put("MAIL_REJC_CD", "1");//메일수신거부코드
				mailMap.put("MSG_STOR_CD", "2");//발신함코드
				mailMap.put("MAILING_MGNT_CD", "1");//자동메일링 코드
				
				map.put("MSG_ID",mailMap.get("MSG_ID"));
				
				List recvList=(List) adminEmailMgntDao.selectEmailAddrListByMsgId(map);
				
				for(int i=0;i<recvList.size();i++){
					Map recvMap=(Map)recvList.get(i);
					String groupId= String.valueOf(recvMap.get("GROUP_ID"));
					String groupIdParent= String.valueOf(recvMap.get("GROUP_ID_PARENT"));
					if("1".equals(groupIdParent)||"2".equals(groupIdParent)){//그룹메일 지정시
						if("11".equals(groupId)||"21".equals(groupId)){
							map.put("RECV_REJC_YN", "N");//수신거부 조건						
						}
						map.put("RECV_GROUP_ID", groupIdParent);	
						List allSendList= (List) adminEmailMgntDao.selectEmailAddrList(map);
						sendList.addAll(allSendList);
					}else{
						sendList.add(recvMap);
					}
				}
				
				sendEmail(sendList, mailMap);				
			}
			/* 미분배보상금대상 저작물보고안내 자동메일링 */
			bFlag=preCheckScheduling("2");
			if(bFlag){		
				Map mailMap = new HashMap();
				List<Map> sendList = new ArrayList<Map>();
				Map map = new HashMap();
				map.put("MSG_STOR_CD","12");
				List list=(List) adminEmailMgntDao.selectEmailMsg(map);//메일함 메세지 불러오기
				mailMap = (Map)list.get(0);
							
				StringBuffer sb = new StringBuffer();				
				sb.append("				<tr>");
				sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
				sb.append("					<p>"+StringUtil.appendHtmlBr((String)mailMap.get("CONTENT"))+"</p>");
				
				sb.append("					</td>");
				sb.append("				</tr>");
				String contentHtml=new String(sb);
				
				mailMap.put("CONTENT_HTML", contentHtml);//메일 내용
				mailMap.put("MAIL_REJC_CD", "1");//메일수신거부코드
				mailMap.put("MSG_STOR_CD", "2");//발신함코드
				mailMap.put("MAILING_MGNT_CD", "2");//자동메일링 코드
				
				map.put("MSG_ID",mailMap.get("MSG_ID"));
				
				List recvList=(List) adminEmailMgntDao.selectEmailAddrListByMsgId(map);
				
				for(int i=0;i<recvList.size();i++){
					Map recvMap=(Map)recvList.get(i);
					String groupId= String.valueOf(recvMap.get("GROUP_ID"));
					String groupIdParent= String.valueOf(recvMap.get("GROUP_ID_PARENT"));
					if("1".equals(groupIdParent)||"2".equals(groupIdParent)){//그룹메일 지정시
						if("11".equals(groupId)||"21".equals(groupId)){
							map.put("RECV_REJC_YN", "N");//수신거부 조건						
						}
						map.put("RECV_GROUP_ID", groupIdParent);	
						List allSendList= (List) adminEmailMgntDao.selectEmailAddrList(map);
						sendList.addAll(allSendList);
					}else{						
						sendList.add(recvMap);				
					}
				}				
				sendEmail(sendList, mailMap);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
}
