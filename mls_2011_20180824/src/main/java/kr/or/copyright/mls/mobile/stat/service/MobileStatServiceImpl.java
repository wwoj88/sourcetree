package kr.or.copyright.mls.mobile.stat.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.mobile.stat.dao.MobileStatDao;
import kr.or.copyright.mls.mobile.stat.model.MobileStat;
import kr.or.copyright.mls.mobile.stat.model.MobileStatFile;

public class MobileStatServiceImpl extends BaseService implements MobileStatService {

private MobileStatDao mobileStatDao;

public void setMobileStatDao(MobileStatDao mobileStatDao) {
	this.mobileStatDao = mobileStatDao;
}

	public List<MobileStat> selectStat(MobileStat mobileStat){
		return mobileStatDao.selectStat(mobileStat);
	}
	public int countStat(Map params){
		return mobileStatDao.countStat(params);
	}
	public List<MobileStat> detailStat(int bordSeqn){
		return mobileStatDao.detailStat(bordSeqn);
	}
	public List<MobileStatFile> fileSelectStat(int bordSeqn){
		return mobileStatDao.fileSelectStat(bordSeqn);
	}
}
