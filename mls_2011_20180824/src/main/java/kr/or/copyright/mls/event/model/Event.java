package kr.or.copyright.mls.event.model;

import kr.or.copyright.mls.common.BaseObject;

public class Event extends BaseObject {
	
	
	private String camp_part_seqn;
	private String camp_part_name;
	private String camp_part_zipx_code;
	private String camp_part_addr;
	private String camp_part_addr_detl;
	private String camp_part_telx;
	private String rgst_dttm;
	private String mgnt_levl_code;
	
	private String camp_file_name;
	private String camp_file_rnme;
	private String camp_file_path;
	private String camp_file_size;
	
	private String item_seqn;
	private String item_view_seqn;
	private String item_rslt;
	private String item_rslt_etc;
	
	private String[] item_seqn_arr;
	private String[] item_view_seqn_arr;
	private String[] item_rslt_arr;
	private String[] item_rslt_etc_arr;
	
	
	public String[] getItem_rslt_etc_arr() {
		return item_rslt_etc_arr;
	}
	public void setItem_rslt_etc_arr(String[] item_rslt_etc_arr) {
		this.item_rslt_etc_arr = item_rslt_etc_arr;
	}
	public String getCamp_file_name() {
		return camp_file_name;
	}
	public void setCamp_file_name(String camp_file_name) {
		this.camp_file_name = camp_file_name;
	}
	public String getCamp_file_path() {
		return camp_file_path;
	}
	public void setCamp_file_path(String camp_file_path) {
		this.camp_file_path = camp_file_path;
	}
	public String getCamp_part_addr_detl() {
		return camp_part_addr_detl;
	}
	public void setCamp_part_addr_detl(String camp_part_addr_detl) {
		this.camp_part_addr_detl = camp_part_addr_detl;
	}
	public String getCamp_part_zipx_code() {
		return camp_part_zipx_code;
	}
	public void setCamp_part_zipx_code(String camp_part_zipx_code) {
		this.camp_part_zipx_code = camp_part_zipx_code;
	}
	public String[] getItem_rslt_arr() {
		return item_rslt_arr;
	}
	public void setItem_rslt_arr(String[] item_rslt_arr) {
		this.item_rslt_arr = item_rslt_arr;
	}
	public String getCamp_part_addr() {
		return camp_part_addr;
	}
	public void setCamp_part_addr(String camp_part_addr) {
		this.camp_part_addr = camp_part_addr;
	}
	public String getCamp_part_name() {
		return camp_part_name;
	}
	public void setCamp_part_name(String camp_part_name) {
		this.camp_part_name = camp_part_name;
	}
	public String getCamp_part_seqn() {
		return camp_part_seqn;
	}
	public void setCamp_part_seqn(String camp_part_seqn) {
		this.camp_part_seqn = camp_part_seqn;
	}
	public String getCamp_part_telx() {
		return camp_part_telx;
	}
	public void setCamp_part_telx(String camp_part_telx) {
		this.camp_part_telx = camp_part_telx;
	}
	public String getItem_rslt() {
		return item_rslt;
	}
	public void setItem_rslt(String item_rslt) {
		this.item_rslt = item_rslt;
	}
	public String getItem_seqn() {
		return item_seqn;
	}
	public void setItem_seqn(String item_seqn) {
		this.item_seqn = item_seqn;
	}
	public String getItem_view_seqn() {
		return item_view_seqn;
	}
	public void setItem_view_seqn(String item_view_seqn) {
		this.item_view_seqn = item_view_seqn;
	}
	public String getMgnt_levl_code() {
		return mgnt_levl_code;
	}
	public void setMgnt_levl_code(String mgnt_levl_code) {
		this.mgnt_levl_code = mgnt_levl_code;
	}
	public String getRgst_dttm() {
		return rgst_dttm;
	}
	public void setRgst_dttm(String rgst_dttm) {
		this.rgst_dttm = rgst_dttm;
	}
	public String getCamp_file_rnme() {
		return camp_file_rnme;
	}
	public void setCamp_file_rnme(String camp_file_rnme) {
		this.camp_file_rnme = camp_file_rnme;
	}
	public String getCamp_file_size() {
		return camp_file_size;
	}
	public void setCamp_file_size(String camp_file_size) {
		this.camp_file_size = camp_file_size;
	}
	public String[] getItem_seqn_arr() {
		return item_seqn_arr;
	}
	public void setItem_seqn_arr(String[] item_seqn_arr) {
		this.item_seqn_arr = item_seqn_arr;
	}
	public String[] getItem_view_seqn_arr() {
		return item_view_seqn_arr;
	}
	public void setItem_view_seqn_arr(String[] item_view_seqn_arr) {
		this.item_view_seqn_arr = item_view_seqn_arr;
	}
	public String getItem_rslt_etc() {
		return item_rslt_etc;
	}
	public void setItem_rslt_etc(String item_rslt_etc) {
		this.item_rslt_etc = item_rslt_etc;
	}
	
	

}
