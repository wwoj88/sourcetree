package kr.or.copyright.mls.commonCode.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.commonCode.dao.CommonCodeDao;

import com.tobesoft.platform.data.Dataset;

public class CommonCodeServiceImpl extends BaseService implements CommonCodeService {

	//private Log logger = LogFactory.getLog(getClass());

	private CommonCodeDao commonCodeDao;
	
	public void setCommonCodeDao(CommonCodeDao commonCodeDao){
		this.commonCodeDao = commonCodeDao;
	}
	
	
	// sampleList 조회 
	public void commonCodeList() throws Exception {

		Dataset gds_CommonCode = getDataset("gds_CommonCode");     
		
		for( int i=0;i<gds_CommonCode.getRowCount();i++ )
		{		
			Map map = getMap(gds_CommonCode, i );
			
			//DAO호출
			List list = (List) commonCodeDao.commonCodeList(map); 
			
			//hmap를 dataset로 변환한다.
			addList(gds_CommonCode.getColumnAsString(i, "outDs"), list);
			
		}	
	}
		
}
