package kr.or.copyright.mls.console.email;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.menu.FdcrAdA9Controller;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.tobesoft.platform.data.Dataset;

/**
 * 안내메일관리 > 메일작성
 * 
 * @author ljh
 */
@Controller
public class FdcrAdA4Controller extends DefaultController{

	@Resource( name = "fdcrAdA4Service" )
	private FdcrAdA4ServiceImpl fdcrAdA4Service;
	
	@Resource( name = "fdcrAdA9Controller" )
	private FdcrAdA9Controller fdcrAdA9Controller;
	
	@Resource( name = "consoleCommonService" )
	protected ConsoleCommonService consoleCommonService;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA4Controller.class );

	/**
	 * 메일 작성 폼
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA4WriteForm1.page" )
	public String fdcrAdA4WriteForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		System.out.println( "메일 작성 폼" );
		return "email/fdcrAdA4WriteForm1.tiles";
	}
	
	/**
	 * 메일 주소록 조회 팝업
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA4Pop1.page" )
	public String fdcrAdA4Pop1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		System.out.println( "메일 주소록 조회" );
		return "email/fdcrAdA4Pop1.popup";
	}
	
	/**
	 * 메뉴 트리
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA4Sub1.page" )
	public void fdcrAdA9List1Sub1( ModelMap model,
		HttpServletRequest request,  
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		
		logger.debug( "fdcrAdA9List1Sub1 Start" );
		ArrayList<Map<String, Object>> treeList = new ArrayList<Map<String, Object>>();
		Map<String, Object> rootTree = new HashMap<String, Object>();
		rootTree.put( "id", "menuId_0" );
		rootTree.put( "text", "전체보기" );
		rootTree.put( "icon", "folder" );
		Map<String, Object> rootTreeState = new HashMap<String, Object>();
		rootTreeState.put( "opened", true );
		rootTree.put( "state", rootTreeState );
		treeList.add( rootTree );
		
		try{
			fdcrAdA4Service.fdcrAdA4List1( commandMap );
			ArrayList<Map<String, Object>> menuList =
				(ArrayList<Map<String, Object>>) commandMap.get( "ds_menu" );
			for( int i = 0; i < menuList.size(); i++ ){
				Map<String, Object> map = menuList.get( i );
				String menuId = ( (BigDecimal) map.get( "GROUP_ID" ) ).intValue() + "";
				String upperMenuId = ( (BigDecimal) map.get( "GROUP_ID_PARENT" ) ).intValue() + "";
				String menuNm = (String) map.get( "GROUP_NAME" );
				int childCnt = EgovWebUtil.getToInt( map, "CHILD_CNT" );

				Map<String, Object> tree = new HashMap<String, Object>();
				tree.put( "id", "menuId_" + menuId );
				tree.put( "text", menuNm );
				Map<String, Object> treeState = new HashMap<String, Object>();
				treeState.put( "opened", true );
				if( childCnt > 0 ){
					tree.put( "icon", "folder" );
				}else{
					tree.put( "icon", "file" );
				}
				tree.put( "state", treeState );
				tree.put( "parent", "menuId_" + upperMenuId );
				fdcrAdA9Controller.setChildNode( treeList, tree );
			}
		}
		catch( RuntimeException e ){
			// e.printStackTrace();
			System.out.println( "실행중 에러발생 " );
		}
		returnAjaxJsonArray( response, treeList );
	}
	
	/**
	  * <PRE>
	  * 간략 : 
	  * 상세 : 
	  * <PRE>
	  * @param model
	  * @param commandMap
	  * @param request
	  * @param response
	  * @return
	  * @throws Exception 
	  */
	@RequestMapping( value = "/console/email/fdcrAdA4Pop1View1.page" )
	public String fdcrAdA4View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		
		System.out.println( "commandMap " + commandMap.toString() );
		
		fdcrAdA4Service.fdcrAdA4List1( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_group", commandMap.get( "ds_menu" ) );
		
		System.out.println( "메일 주소록 조회" );
		return "email/fdcrAdA4Pop1View1";
	}
	
	/**
	 * 메일 주소록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA4List1.page" )
	public String fdcrAdA4List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA4Service.fdcrAdA4List1( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_menu", commandMap.get( "ds_menu" ) );
		
		System.out.println( "메일 주소록 조회" );
		return "email/fdcrAdA4List1.tiles";
	}
	
	/**
	 * 전체 메일 주소록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA4List2.page" )
	public String fdcrAdA4List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String[] RECV_GROUP_IDS = request.getParameterValues( "RECV_GROUP_ID" );
		String[] RECV_REJC_YNS = request.getParameterValues( "RECV_REJC_YN" );
		commandMap.put( "RECV_GROUP_ID", RECV_GROUP_IDS );
		commandMap.put( "RECV_REJC_YN", RECV_REJC_YNS );
		
		fdcrAdA4Service.fdcrAdA4List2( commandMap );

		model.addAttribute( "ds_all_send", commandMap.get( "ds_all_send" ) );
		System.out.println( "전체 메일 주소록 조회" );
		return "test";
	}

	/**
	 * 메일 저장
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA4Insert1.page" )
	public String fdcrAdA4Insert1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		
		System.out.println(commandMap.toString());
		String TYPE = StringUtil.nullToEmpty( (String) commandMap.get( "TYPE" ) );
		commandMap.put("RGST_IDNT", ConsoleLoginUser.getUserId());
		
		
		/*String[] MSG_IDS = request.getParameterValues( "MSG_ID" );
		String[] RECV_CDS = request.getParameterValues( "RECV_CD" );
		String[] RECV_SEQNS = request.getParameterValues( "RECV_SEQN" );
		String[] RECV_NAMES = request.getParameterValues( "RECV_NAME" );
		String[] RECV_PERS_IDS = request.getParameterValues( "RECV_PERS_ID" );
		String[] RECV_MAIL_ADDRS = request.getParameterValues( "RECV_MAIL_ADDR" );
		String[] RECV_GROUP_IDS = request.getParameterValues( "RECV_GROUP_ID" );
		String[] RECV_GROUP_NAMES = request.getParameterValues( "RECV_GROUP_NAME" );
		String[] FILE_NAMES = request.getParameterValues( "FILE_NAME" );
		String[] FILE_PATHS = request.getParameterValues( "FILE_PATH" );
		String[] FILE_SIZES = request.getParameterValues( "FILE_SIZE" );
		String[] REAL_FILE_NAMES = request.getParameterValues( "REAL_FILE_NAME" );
		String[] RGST_IDNTS = request.getParameterValues( "RGST_IDNT" );
		commandMap.put( "MSG_ID", MSG_IDS );
		commandMap.put( "RECV_CD", RECV_CDS );
		commandMap.put( "RECV_SEQN", RECV_SEQNS );
		commandMap.put( "RECV_NAME", RECV_NAMES );
		commandMap.put( "RECV_PERS_ID", RECV_PERS_IDS );
		commandMap.put( "RECV_MAIL_ADDR", RECV_MAIL_ADDRS );
		commandMap.put( "RECV_GROUP_ID", RECV_GROUP_IDS );
		commandMap.put( "RECV_GROUP_NAME", RECV_GROUP_NAMES );
		commandMap.put( "FILE_NAME", FILE_NAMES );
		commandMap.put( "FILE_PATH", FILE_PATHS );
		commandMap.put( "FILE_SIZE", FILE_SIZES );
		commandMap.put( "REAL_FILE_NAME", REAL_FILE_NAMES );
		commandMap.put( "RGST_IDNT", RGST_IDNTS );
		
		*/
		
		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		//ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAdA4Service.fdcrAdA4Insert1( commandMap, null ); //fileinfo
		
		logger.debug( "메일 저장" );
		String url = "";
		if(TYPE.equals( "monthList" )){
			url="/console/email/fdcrAdA8List1.page";
		}else{
			url="/console/email/fdcrAdA5List1.page";
		}
		
		if( isSuccess ){
			return returnUrl(
				model,
				"저장 했습니다.",
				url);
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				url);
		}
	}

	/**
	 * 메일 전송
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA4Insert2.page" )
	public void fdcrAdA4Insert2( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String[] MSG_IDS = request.getParameterValues( "MSG_ID" );
		String[] RECV_CDS = request.getParameterValues( "RECV_CD" );
		String[] RECV_SEQNS = request.getParameterValues( "RECV_SEQN" );
		String[] RECV_NAMES = request.getParameterValues( "RECV_NAME" );
		String[] RECV_PERS_IDS = request.getParameterValues( "RECV_PERS_ID" );
		String[] RECV_MAIL_ADDRS = request.getParameterValues( "RECV_MAIL_ADDR" );
		String[] RECV_GROUP_IDS = request.getParameterValues( "RECV_GROUP_ID" );
		String[] RECV_GROUP_NAMES = request.getParameterValues( "RECV_GROUP_NAME" );
		String[] FILE_NAMES = request.getParameterValues( "FILE_NAME" );
		String[] FILE_PATHS = request.getParameterValues( "FILE_PATH" );
		String[] FILE_SIZES = request.getParameterValues( "FILE_SIZE" );
		String[] REAL_FILE_NAMES = request.getParameterValues( "REAL_FILE_NAME" );
		String[] RGST_IDNTS = request.getParameterValues( "RGST_IDNT" );
		commandMap.put( "MSG_ID", MSG_IDS );
		commandMap.put( "RECV_CD", RECV_CDS );
		commandMap.put( "RECV_SEQN", RECV_SEQNS );
		commandMap.put( "RECV_NAME", RECV_NAMES );
		commandMap.put( "RECV_PERS_ID", RECV_PERS_IDS );
		commandMap.put( "RECV_MAIL_ADDR", RECV_MAIL_ADDRS );
		commandMap.put( "RECV_GROUP_ID", RECV_GROUP_IDS );
		commandMap.put( "RECV_GROUP_NAME", RECV_GROUP_NAMES );
		commandMap.put( "FILE_NAME", FILE_NAMES );
		commandMap.put( "FILE_PATH", FILE_PATHS );
		commandMap.put( "FILE_SIZE", FILE_SIZES );
		commandMap.put( "REAL_FILE_NAME", REAL_FILE_NAMES );
		commandMap.put( "RGST_IDNT", RGST_IDNTS );

		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAdA4Service.fdcrAdA4Insert2( commandMap, fileinfo );
		returnAjaxString( response, isSuccess );
		System.out.println( "메일 전송" );
	}
	
	/**
	 *  메일관리 팝업
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA4Pop2.page" )
	public String fdcrAdA4Pop2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "MSG_ID", StringUtil.nullToEmpty(request.getParameter( "MSG_ID" )));
		commandMap.put( "GUBUN", StringUtil.nullToEmpty(request.getParameter( "GUBUN" )));
		commandMap.put( "TYPE", StringUtil.nullToEmpty(request.getParameter( "TYPE" )));
		commandMap.put( "MSG_STOR_CD", StringUtil.nullToEmpty(request.getParameter( "MSG_STOR_CD" )));
		fdcrAdA4Service.fdcrAdA4View1( commandMap );

		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "ds_mail", commandMap.get( "ds_mail" ) );
		model.addAttribute( "ds_send2", commandMap.get( "ds_send2" ) );
		model.addAttribute( "ds_detail", commandMap.get( "ds_detail" ) );
		
		logger.debug( "메일관리 팝업" );
		return "email/fdcrAdA4Pop2.popup";
	}
	
	/**
	  * <PRE>
	  * 간략 : 메일 수정
	  * 상세 : 메일 수정
	  * <PRE>
	  * @param model
	  * @param commandMap
	  * @param mreq
	  * @param request
	  * @param response
	  * @return
	  * @throws Exception 
	  */
	@RequestMapping( value = "/console/email/fdcrAdA4Update1.page" )
	public String fdcrAdA4Update1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		
		System.out.println(commandMap.toString());
		
		commandMap.put("RGST_IDNT", ConsoleLoginUser.getUserId());
		
		String TYPE = request.getParameter( "TYPE" );
		String MSG_ID = request.getParameter( "MSG_ID" );
		String GUBUN = request.getParameter( "GUBUN" );
		String MSG_STOR_CD = request.getParameter( "MSG_STOR_CD" );
		String TITLE = request.getParameter( "TITLE" );
		String CONTENT = request.getParameter( "CONTENT" );
		String RGST_IDNT = ConsoleLoginUser.getUserId();
		String[] RECV_CDS = request.getParameterValues( "RECV_CD" );
		String[] RECV_SEQNS = request.getParameterValues( "RECV_SEQN" );
		String[] RECV_NAMES = request.getParameterValues( "RECV_NAME" );
		String[] RECV_PERS_IDS = request.getParameterValues( "RECV_PERS_ID" );
		String[] RECV_MAIL_ADDRS = request.getParameterValues( "RECV_MAIL_ADDR" );
		String[] RECV_GROUP_IDS = request.getParameterValues( "RECV_GROUP_ID" );
		String[] RECV_GROUP_NAMES = request.getParameterValues( "RECV_GROUP_NAME" );
		String[] FILE_NAMES = request.getParameterValues( "FILE_NAME" );
		String[] FILE_PATHS = request.getParameterValues( "FILE_PATH" );
		String[] FILE_SIZES = request.getParameterValues( "FILE_SIZE" );
		String[] REAL_FILE_NAMES = request.getParameterValues( "REAL_FILE_NAME" );
		commandMap.put( "MSG_ID", MSG_ID );
		commandMap.put( "RECV_CD", RECV_CDS );
		commandMap.put( "RECV_SEQN", RECV_SEQNS );
		commandMap.put( "RECV_NAME", RECV_NAMES );
		commandMap.put( "RECV_PERS_ID", RECV_PERS_IDS );
		commandMap.put( "RECV_MAIL_ADDR", RECV_MAIL_ADDRS );
		commandMap.put( "RECV_GROUP_ID", RECV_GROUP_IDS );
		commandMap.put( "RECV_GROUP_NAME", RECV_GROUP_NAMES );
		commandMap.put( "FILE_NAME", FILE_NAMES );
		commandMap.put( "FILE_PATH", FILE_PATHS );
		commandMap.put( "FILE_SIZE", FILE_SIZES );
		commandMap.put( "REAL_FILE_NAME", REAL_FILE_NAMES );
		commandMap.put( "RGST_IDNT", RGST_IDNT );
		commandMap.put( "CONTENT", CONTENT );
		commandMap.put( "TITLE", TITLE );
		commandMap.put( "MSG_STOR_CD", MSG_STOR_CD );
		commandMap.put( "GUBUN", GUBUN );
		
		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		//ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAdA4Service.fdcrAdA4Update1( commandMap, null ); //fileinfo
		
		logger.debug( "메일 수정" );
		String url = "";
		if(TYPE.equals( "monthList" )){
			url="/console/email/fdcrAdA8List1.page";
		}else{
			url="/console/email/fdcrAdA5List1.page";
		}
		if( isSuccess ){
			return returnUrl(
				model,
				"저장 했습니다.",
				url);
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				url);
		}
	}
	
}
