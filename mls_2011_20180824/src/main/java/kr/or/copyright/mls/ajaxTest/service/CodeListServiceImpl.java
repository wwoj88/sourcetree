package kr.or.copyright.mls.ajaxTest.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.ajaxTest.dao.CodeListDAO;
import kr.or.copyright.mls.ajaxTest.model.Code;
import kr.or.copyright.mls.user.model.ZipCodeRoad;

import org.springframework.stereotype.Service;

@Service("CodeListBO")
public class CodeListServiceImpl implements CodeListService {
	
    	@Resource(name="CodeListDAO")
	private CodeListDAO codeListDAO;
	
	public Map<String, String> getInitCodeList() {
		List<Code> result = (List<Code>)codeListDAO.getInitCodeList();
		Map<String, String> resultMap = new HashMap<String, String>();
		for(Code vo : result){
			resultMap.put(vo.getBigCode(), vo.getCodeName());
		}
		return resultMap;
	}


	public List<Code> getCodeList(Map params) {
		return codeListDAO.getCodeList(params);
	}


	public List<ZipCodeRoad> getInitRoadCodeList() {
	    // TODO Auto-generated method stub
	    return codeListDAO.getInitRoadCodeList();
	}


	public List<ZipCodeRoad> getRoadCodeList(Map params) {
	    // TODO Auto-generated method stub
	    return codeListDAO.getRoadCodeList(params);
	}
}
