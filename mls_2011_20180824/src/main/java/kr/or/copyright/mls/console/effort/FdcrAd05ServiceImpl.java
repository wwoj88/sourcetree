package kr.or.copyright.mls.console.effort;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;
import kr.or.copyright.mls.console.effort.inter.FdcrAd05Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd05Service" )
public class FdcrAd05ServiceImpl extends CommandService implements FdcrAd05Service{

	@Resource( name = "fdcrAd03Dao" )
	private FdcrAd03Dao fdcrAd03Dao;

	/**
	 * ��� ��ȸ
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd05List1( Map<String, Object> commandMap ) throws Exception{
		// DAO ȣ��
		List pageCount = (List) fdcrAd03Dao.statProcInfoCount( commandMap );
		int totCnt = ((BigDecimal)((Map)pageCount.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		
		List list = (List) fdcrAd03Dao.statProcInfo( commandMap );
		commandMap.put( "ds_list", list );
		commandMap.put( "ds_page", pageCount );
	}
	
	/**
	 * �����ٿ�ε�
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd05Down1( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd03Dao.statProcInfoNewExcel( commandMap );
		commandMap.put( "ds_list", list );
	}
	
}
