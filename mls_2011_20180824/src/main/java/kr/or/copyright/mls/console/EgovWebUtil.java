package kr.or.copyright.mls.console;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import kr.or.copyright.mls.support.util.StringUtil;
import oracle.sql.CLOB;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 교차접속 스크립트 공격 취약성 방지(파라미터 문자열 교체)
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 
 *   수정일      수정자           수정내용
 *  -------    	--------    ---------------------------
 *   2011.10.10  한성곤          최초 생성
 * 
 * </pre>
 */

public class EgovWebUtil{

	private static final Logger logger = LoggerFactory.getLogger( EgovWebUtil.class );

	public static String clearXSSMinimum( String value ){
		if( value == null || value.trim().equals( "" ) ){
			return "";
		}

		String returnValue = value;

		returnValue = returnValue.replaceAll( "&", "&amp;" );
		returnValue = returnValue.replaceAll( "<", "&lt;" );
		returnValue = returnValue.replaceAll( ">", "&gt;" );
		returnValue = returnValue.replaceAll( "\"", "&#34;" );
		returnValue = returnValue.replaceAll( "\'", "&#39;" );
		return returnValue;
	}

	public static String clearXSSMaximum( String value ){
		String returnValue = value;
		returnValue = clearXSSMinimum( returnValue );

		returnValue = returnValue.replaceAll( "%00", null );

		returnValue = returnValue.replaceAll( "%", "&#37;" );

		// \\. => .

		returnValue = returnValue.replaceAll( "\\.\\./", "" ); // ../
		returnValue = returnValue.replaceAll( "\\.\\.\\\\", "" ); // ..\
		returnValue = returnValue.replaceAll( "\\./", "" ); // ./
		returnValue = returnValue.replaceAll( "%2F", "" );

		return returnValue;
	}

	public static String filePathBlackList( String value ){
		String returnValue = value;
		if( returnValue == null || returnValue.trim().equals( "" ) ){
			return "";
		}

		returnValue = returnValue.replaceAll( "\\.\\./", "" ); // ../
		returnValue = returnValue.replaceAll( "\\.\\.\\\\", "" ); // ..\

		return returnValue;
	}

	/**
	 * 행안부 보안취약점 점검 조치 방안.
	 * 
	 * @param value
	 * @return
	 */
	public static String filePathReplaceAll( String value ){
		String returnValue = value;
		if( returnValue == null || returnValue.trim().equals( "" ) ){
			return "";
		}

		returnValue = returnValue.replaceAll( "/", "" );
		returnValue = returnValue.replaceAll( "\\", "" );
		returnValue = returnValue.replaceAll( "\\.\\.", "" ); // ..
		returnValue = returnValue.replaceAll( "&", "" );

		return returnValue;
	}

	public static String filePathWhiteList( String value ){
		return value;
	}

	public static boolean isIPAddress( String str ){
		Pattern ipPattern = Pattern.compile( "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}" );

		return ipPattern.matcher( str ).matches();
	}

	public static String removeCRLF( String parameter ){
		return parameter.replaceAll( "\r", "" ).replaceAll( "\n", "" );
	}

	public static String removeSQLInjectionRisk( String parameter ){
		return parameter.replaceAll( "\\p{Space}", "" ).replaceAll( "\\*", "" ).replaceAll( "%", "" ).replaceAll(
			";",
			"" ).replaceAll( "-", "" ).replaceAll( "\\+", "" ).replaceAll( ",", "" );
	}

	public static String removeOSCmdRisk( String parameter ){
		return parameter.replaceAll( "\\p{Space}", "" ).replaceAll( "\\*", "" ).replaceAll( "|", "" ).replaceAll(
			";",
			"" );
	}

	/**
	 * String to Int
	 * 
	 * @param format
	 * @return
	 */
	public static int toInt( String value,
		int defaultInt ){
		try{
			return NumberUtils.createInteger( value );
		}
		catch( RuntimeException e ){
			return defaultInt;
		}
	}

	/**
	 * String to Int
	 * 
	 * @param format
	 * @return
	 */
	public static int toInt( String value ){
		try{
			return NumberUtils.createInteger( value );
		}
		catch( RuntimeException e ){
			return 0;
		}
	}

	/**
	 * Int to String
	 * 
	 * @param format
	 * @return
	 */
	public static String toString( int value ){
		try{
			return Integer.toString( value );
		}
		catch( RuntimeException e ){
			System.out.println( e.getMessage() );
			return "";
		}
	}

	/**
	 * String 가져오기
	 * 
	 * @param name
	 * @return
	 */
	public static String getString( Map<String, Object> commandMap,
		String name ){
		String value = "";
		if(commandMap!=null){
			Object obj = commandMap.get( name );
			if( null != obj && obj instanceof String ){
				value = (String) obj;
			}else if( null != obj && obj instanceof oracle.sql.CLOB ){
				// CLOB clob = (CLOB) obj;
				// try{
				// value = clob.stringValue();
				// }
				// catch( SQLException e ){
				// logger.error("getString Error:"+e.getMessage());
				// }
				value = weblogicClobToString( (Clob) obj );
			}else if( null != obj && obj instanceof Clob ){
				value = weblogicClobToString( (Clob) obj );
			}else if( obj instanceof Integer ){
				int temp = (Integer) obj;
				value = temp+"";
			}else if( obj instanceof BigDecimal ){
				int temp = ( (BigDecimal) obj ).intValue();
				value = temp+"";
			}else{
				try{
					value = (String) obj;
					if( null == value ) value = "";
				}
				catch( Exception e ){
					value = "";
				}
			}
		}
		return value;
	}

	/**
	 * 지정된 2개의 크기만큼 스트링을 잘라서 리턴한다. 스크링 길이가 모자라면 모자란 만큼만 넣는다. null string 처리
	 * 
	 * @param src
	 *            스트링
	 * @param first
	 *            0-first 만큼 자른다.
	 * @param second
	 *            firsht~second 만큼 자른다.
	 * @param size
	 *            생성 배열 크기
	 * @return String[]
	 */
	public static String[] substring2( String src,
		int first,
		int second ){

		String[] ret = { "", "" };
		if( src == null ) return ret;
		if( first > src.length() ){
			ret[0] = src;
			return ret;
		}else{
			ret[0] = src.substring( 0, first );
		}

		if( second > src.length() ){
			ret[1] = src.substring( first );
		}else{
			ret[1] = src.substring( first, second );
		}
		return ret;

	}

	/**
	 * 문자열자르기 (구분자이용)
	 * 
	 * @param strSize
	 * @return
	 */
	public static String getSplitn( String value,
		String value1,
		String value2 ){

		if( value == null ) return "";
		String[] tmp = value.split( value1 );

		if( tmp.length <= Integer.parseInt( value2 ) ){
			return "";
		}
		return tmp[Integer.parseInt( value2 )];
	}

	public static String weblogicClobToString( Clob clob ){
		StringBuffer clobdata = new StringBuffer();

		if( clob != null ){
			try{
				Reader reader = clob.getCharacterStream();

				char[] buffer = new char[1024];
				int byteRead;

				while( ( byteRead = reader.read( buffer, 0, 1024 ) ) != -1 ){
					clobdata.append( buffer, 0, byteRead );
				}
			}
			catch( SQLException ex ){
				logger.error( "weblogicClobToString Error:" + ex.getMessage() );
			}
			catch( IOException e ){
				logger.error( "weblogicClobToString Error:" + e.getMessage() );
			}
		}
		return clobdata.toString();
	}

	/**
	 * format 파라미터의 날짜형식으로 오늘 날짜 구하기
	 * 
	 * @param format
	 * @return
	 */
	public static String getDate( String format ){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat( format );
		return sdf.format( date );
	}

	/**
	 * 날짜앞에 '0' 채워넣기
	 * 
	 * @param iLen
	 *            : 변환될 길이
	 * @param strVal
	 *            : 변환할 값
	 * @return
	 */
	public static String getChangFormatDate( int iLen,
		String strVal ){

		String rtnVal = "";

		int compNum = iLen - strVal.length();
		if( compNum > 0 ){
			for( int i = 0; i < compNum; i++ ){
				strVal = "0" + strVal;
			}
		}

		rtnVal = strVal;

		return rtnVal;
	}

	public static File getFile( String path ){
		File file = null;
		if( !"".equals( path ) ){
			file = new File( path );
			if( file.exists() && file.isFile() ){
				return file;
			}
		}
		return file;
	}

	public static int getInt( Map<String, Object> map,
		String name ){
		int value = 0;
		if(map!=null){
			Object obj = map.get( name );
			if( null != obj ){
				if( obj instanceof Integer ){
					value = (Integer) obj;
				}else if( obj instanceof BigDecimal ){
					value = ( (BigDecimal) obj ).intValue();
				}
			}
		}
		return value;
	}

	public static long getLong( Map<String, Object> map,
		String name ){
		long value = 0;
		Object obj = map.get( name );
		if( null != obj ){
			if( obj instanceof Long ){
				value = (Long) obj;
			}else if( obj instanceof BigDecimal ){
				value = ( (BigDecimal) obj ).longValue();
			}else if( obj instanceof String ){
				try{
					value = Long.parseLong( (String) obj );
				}
				catch( Exception e ){
					value = 0;
				}
			}
		}
		return value;
	}

	public static float getFloat( Map<String, Object> map,
		String name ){
		float value = 0;
		Object obj = map.get( name );
		if( null != obj ){
			if( obj instanceof Float ){
				value = (Float) obj;
			}else if( obj instanceof BigDecimal ){
				value = ( (BigDecimal) obj ).floatValue();
			}else if( obj instanceof String ){
				try{
					value = Float.parseFloat( (String) obj );
				}
				catch( Exception e ){
					value = 0;
				}
			}
		}
		return value;
	}

	public static int getToInt( Map<String, Object> map,
		String name ){
		int value = 0;
		Object o = map.get( name );
		if( o instanceof String ){
			String obj = getString( map, name );
			try{
				value = Integer.parseInt( obj );
			}
			catch( NullPointerException e ){
				value = 0;
			}
			catch( NumberFormatException e ){
				value = 0;
			}
		}else if( o instanceof BigDecimal ){
			value = ( (BigDecimal) o ).intValue();
		}else if( o instanceof Integer ){
			value = getInt( map, name );
		}
		return value;
	}

	public static String[] getStringArray( Map<String, Object> map,
		String name ){
		String[] value = null;
		Object obj = map.get( name );
		if( null != obj && obj instanceof String ){
			value = new String[]{ (String) obj };
		}else if( null != obj && obj instanceof String[] ){
			value = (String[]) obj;
		}
		return value;
	}

	public static String getStr( String[] args,
		String gubun ){
		String result = "";
		if( null != args && args.length > 0 ){
			for( int i = 0; i < args.length; i++ ){
				String s = args[i];
				if( i != 0 ){
					result += gubun;
				}
				result += s;
			}
		}
		return result;
	}

	public static String[] split( String arg,
		String gubun ){
		if( arg != null ){
			String[] args = arg.split( "\\" + gubun );
			return args;
		}else{
			return null;
		}
	}

	public static ArrayList<Map<String, Object>> copyArrayList( ArrayList<Map<String, Object>> list ){
		ArrayList<Map<String, Object>> temp = new ArrayList<Map<String, Object>>();
		for( Map<String, Object> map : list ){
			Map<String, Object> newMap = new HashMap<String, Object>();
			for( Map.Entry<String, Object> entry : map.entrySet() ){
				newMap.put( entry.getKey(), entry.getValue() );
			}
			temp.add( newMap );
		}
		return temp;
	}

	public static String getFileExt( String fileName ){
		return StringUtils.substringAfterLast( fileName, "." );
	}

	/**
	 * 랜덤 파일명 가져오기
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public static String uploadMiFile( String fileName ){
		String uuidFileName = null;

		FileOutputStream s = null;
		ByteArrayInputStream is = null;
		byte[] in = null;
		int len = 0;
		int offset = 0;

		try{
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat formatter2 = new SimpleDateFormat( "yyyyMMddhhmmss", Locale.KOREA );
			String period = formatter2.format( cal.getTime() );

			StringUtil stringUtil = new StringUtil();
			int inputNum[] = stringUtil.getRandomNum( 4 );
			String inputNumFull = "" + inputNum[0] + inputNum[1] + inputNum[2] + inputNum[3];
			uuidFileName = period + inputNumFull + "." + getFileExt( fileName );
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return uuidFileName;
	}

	public static ArrayList<Map<String, Object>> convertFromArrToArray( HttpServletRequest request,
		String[] names ){
		ArrayList<Map<String, Object>> target = new ArrayList<Map<String, Object>>();
		if( null != names && names.length > 0 ){
			String baseName = names[0];
			String[] arrs = request.getParameterValues( baseName );
			int totalCount = arrs.length;
			int count = names.length;

			for( int j = 0; j < totalCount; j++ ){
				Map<String, Object> map = new HashMap<String, Object>();
				for( int i = 0; i < count; i++ ){
					map.put( names[i], request.getParameterValues( names[i] )[j] );
				}
				target.add( map );
			}
		}
		return target;
	}

	public static String getClientIP( HttpServletRequest request ) throws Exception{

		String ip = request.getHeader( "X-Forwarded-For" );
		if( ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase( ip ) ){
			ip = request.getHeader( "Proxy-Client-IP" );
		}
		if( ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase( ip ) ){
			ip = request.getHeader( "WL-Proxy-Client-IP" );
		}
		if( ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase( ip ) ){
			ip = request.getHeader( "HTTP_CLIENT_IP" );
		}
		if( ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase( ip ) ){
			ip = request.getHeader( "HTTP_X_FORWARDED_FOR" );
		}
		if( ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase( ip ) ){
			ip = request.getRemoteAddr();
		}

		return ip;
	}

	public static int getToInt( String str ){
		int value = 0;
		try{
			value = Integer.parseInt( str );
		}
		catch( NullPointerException e ){
			value = 0;
		}
		catch( NumberFormatException e ){
			value = 0;
		}
		return value;
	}

	public static void ajaxCommonEncoding( HttpServletRequest request,
		Map<String, Object> commandMap ){
		if( "XMLHttpRequest".equals( request.getHeader( "X-Requested-With" ) ) ){
			Enumeration<?> param = request.getParameterNames();
			while( param.hasMoreElements() ){
				String name = (String) param.nextElement();
				String value;
				try{
					value = URLDecoder.decode( request.getParameter( name ), "UTF-8" );
					commandMap.put( name, value );
				}
				catch( UnsupportedEncodingException e ){
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 코드 값 조회
	 * @param codeList
	 * @param midCode
	 * @return
	 */
	public static String getCodeName(ArrayList<Map<String,Object>> codeList, int midCode){
		String codeName = "";
		if(null!=codeList && codeList.size() > 0){
			for(Map<String,Object> code : codeList){
				int c = EgovWebUtil.getToInt( code, "MID_CODE" );
				if(midCode == c){
					codeName = EgovWebUtil.getString( code, "CODE_NAME" );
					break;
				}
			}
		}
		return codeName;
	}
}
