package kr.or.copyright.mls.mobile.model;

public class Music {
	private int 
		crId
		, nrId
		, albumId
		, trackNo
	;
	
	private String 
		title
		, albumTitle
		, issuedDate
		, composer		/*�۰�*/
		, lyricist		/*�ۻ�*/
		, arranger		/*����*/
		, singer		/*��â*/
		, albumTypeStr
		, musicTypeStr
		, player		/*����*/
		, kappTrustYnStr
		, kappTrustYn
		, trustYn
		, performanceTime
		, pakTrustYn
	;

	public int getCrId() {
		return crId;
	}

	public void setCrId(int crId) {
		this.crId = crId;
	}

	public int getNrId() {
		return nrId;
	}

	public void setNrId(int nrId) {
		this.nrId = nrId;
	}

	public int getAlbumId() {
		return albumId;
	}

	public void setAlbumId(int albumId) {
		this.albumId = albumId;
	}

	public int getTrackNo() {
		return trackNo;
	}

	public void setTrackNo(int trackNo) {
		this.trackNo = trackNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAlbumTitle() {
		return albumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}

	public String getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(String issuedDate) {
		this.issuedDate = issuedDate;
	}

	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public String getLyricist() {
		return lyricist;
	}

	public void setLyricist(String lyricist) {
		this.lyricist = lyricist;
	}

	public String getArranger() {
		return arranger;
	}

	public void setArranger(String arranger) {
		this.arranger = arranger;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public String getAlbumTypeStr() {
		return albumTypeStr;
	}

	public void setAlbumTypeStr(String albumTypeStr) {
		this.albumTypeStr = albumTypeStr;
	}

	public String getMusicTypeStr() {
		return musicTypeStr;
	}

	public void setMusicTypeStr(String musicTypeStr) {
		this.musicTypeStr = musicTypeStr;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public String getKappTrustYnStr() {
		return kappTrustYnStr;
	}

	public void setKappTrustYnStr(String kappTrustYnStr) {
		this.kappTrustYnStr = kappTrustYnStr;
	}

	public String getKappTrustYn() {
		return kappTrustYn;
	}

	public void setKappTrustYn(String kappTrustYn) {
		this.kappTrustYn = kappTrustYn;
	}

	public String getTrustYn() {
		return trustYn;
	}

	public void setTrustYn(String trustYn) {
		this.trustYn = trustYn;
	}

	public String getPerformanceTime() {
		return performanceTime;
	}

	public void setPerformanceTime(String performanceTime) {
		this.performanceTime = performanceTime;
	}

	public String getPakTrustYn() {
		return pakTrustYn;
	}

	public void setPakTrustYn(String pakTrustYn) {
		this.pakTrustYn = pakTrustYn;
	}
	
}
