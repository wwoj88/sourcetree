package kr.or.copyright.mls.worksPriv.model;

import java.util.Date;
import java.util.List;

import kr.or.copyright.mls.common.BaseObject;

public class WorksPrivFile extends BaseObject {
	private long crId;
	private long attcSeqn;
	private String fileSize;
	private String fileName;
	private String filePath;
	private String realFileName;
	private String rgstIdnt;
	private String rgstDttm;
	private String modiIdnt;
	private Date modiDate;
	private List fileList;
	
	public long getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(long attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public long getCrId() {
		return crId;
	}
	public void setCrId(long crId) {
		this.crId = crId;
	}
	public List getFileList() {
		return fileList;
	}
	public void setFileList(List fileList) {
		this.fileList = fileList;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public Date getModiDate() {
		return modiDate;
	}
	public void setModiDate(Date modiDate) {
		this.modiDate = modiDate;
	}
	public String getModiIdnt() {
		return modiIdnt;
	}
	public void setModiIdnt(String modiIdnt) {
		this.modiIdnt = modiIdnt;
	}
	public String getRealFileName() {
		return realFileName;
	}
	public void setRealFileName(String realFileName) {
		this.realFileName = realFileName;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}

}
