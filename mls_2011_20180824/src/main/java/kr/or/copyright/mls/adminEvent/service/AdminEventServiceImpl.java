package kr.or.copyright.mls.adminEvent.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminEvent.dao.AdminEventDao;
import kr.or.copyright.mls.common.service.BaseService;

import com.tobesoft.platform.data.Dataset;

public class AdminEventServiceImpl extends BaseService implements AdminEventService {

	//private Log logger = LogFactory.getLog(getClass());

	private AdminEventDao adminEventDao; 

	public void setAdminEventDao(AdminEventDao adminEventDao) {
		this.adminEventDao = adminEventDao;
	}
	
	public void campPartList() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		
		ds_condition.printDataset();
		Map map = getMap(ds_condition, 0);
		

		// DAO호출
		List list = (List)adminEventDao.campPartList(map);
		
		// DataSet return
		addList("ds_List", list);
	}
	
	// 이벤트 목록
	public void eventMgntList () throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출 
		List list = (List)adminEventDao.eventMgntList(map);
		
		addList("ds_List", list);	
		
	}
	
	// 이벤트 상세
	public void eventMgntDetail() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List)adminEventDao.eventMgntDetail(map);
		
		addList("ds_list", list);
	}
	
	// 이벤트 저장
	public void eventMgntSave() throws Exception {
		
		Dataset ds_list = getDataset("ds_list");
		
		String row_status = null;
		String updateFg = null;
		
		// 삭제
		for( int i = 0; i<ds_list.getDeleteRowCount(); i++) {
			
			Map deleteMap = getDeleteMap(ds_list, i);
			adminEventDao.eventMgntDelete(deleteMap);
		}
		
		// insert, update
		for(int i = 0; i<ds_list.getRowCount(); i++) {
			
			row_status = ds_list.getRowStatus(i);
			
			Map map = getMap(ds_list, i);
			
			updateFg = ds_list.getColumnAsString(0, "UPDATE_FG");
			map.put("UPDATE_FG", updateFg);
			
			if( row_status.equals("insert") == true  ) {
				adminEventDao.eventMgntInsert(map);
			}else if( row_status.equals("update") == true ) {
				adminEventDao.eventMgntUpdate(map);
			}
		}
		
	}
}
