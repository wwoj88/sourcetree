package kr.or.copyright.mls.board.service;

import java.util.ArrayList;
import java.util.Map;

import kr.or.copyright.mls.board.model.Board;
import kr.or.copyright.mls.board.model.BoardFile;
import kr.or.copyright.mls.common.common.model.ListResult;

public interface BoardService {

	public ListResult findBoardList(int pageNo, int rowPerPage, Map params);
	
	int insertQust(Board board) throws Exception;
	
	public void updateQust(Board board);
	
	public void fileDelete(String[] chk);
	public void fileMultDelete(ArrayList<BoardFile> chk);
	public void updateInqrCont(Board board);

	public Board boardView(Board board);
	
	public Board isValidPswd(Board board);

	public void deleteQust(Board board);
	
	public void insertComm(Board board);
	
	public void updateStatInqrCont(Board board);
	
}
