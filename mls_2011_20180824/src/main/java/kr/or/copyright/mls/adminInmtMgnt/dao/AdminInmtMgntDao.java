package kr.or.copyright.mls.adminInmtMgnt.dao;

import java.util.List;
import java.util.Map;

import com.tobesoft.platform.data.Dataset;

public interface AdminInmtMgntDao {
	
	public  List brctInmtInsert(Dataset ds1, Dataset ds2) throws Exception;
	
	public  List subjInmtInsert(Dataset ds1, Dataset ds2) throws Exception;
	
	public  List librInmtInsert(Dataset ds1, Dataset ds2) throws Exception;
	
	public  List lssnInmtInsert(Dataset ds1, Dataset ds2) throws Exception;
	
	public List subjInmtUpdate(Dataset ds);
	
	public List inmtMgntMuscList(Map map);
	
	public List totalRowinmtMgntMuscList(Map map);
	
	public List inmtMgntLibrList(Map map);
	
	public List totalRowinmtMgntLibrList(Map map);
	
	public List inmtMgntSubjList(Map map);
	
	public List totalRowinmtMgntSubjList(Map map);
	
	public List inmtMgntLssnList(Map map);
	
	public List totalRowinmtMgntLssnList(Map map);
	
	public  List imgeInsert(Dataset ds);
	
	public  List movieInsert(Dataset ds);
	
	public List inmtMgntMovieList(Map map);
	
	public List totalRowinmtMgntMovieList(Map map);
	
	public List inmtMgntImageList(Map map);
	
	public List totalRowinmtMgntImageList(Map map);
	
	public List subjInmtUpdateUpload(Dataset ds1, Dataset ds2) throws Exception;
	
	public List librInmtUpdateUpload(Dataset ds1, Dataset ds2) throws Exception;
	
	public List brctInmtUpdateUpload(Dataset ds1, Dataset ds2) throws Exception;
	
	public List lssnInmtUpdateUpload(Dataset ds1, Dataset ds2) throws Exception;
	
	public List inmtMgntReptView(Map map);

	public void updateMuscStatYn(Map map);

	public void updateLibrStatYn(Map map);

	public void updateSubjStatYn(Map map);

	public void updateLssnStatYn(Map map);

}
