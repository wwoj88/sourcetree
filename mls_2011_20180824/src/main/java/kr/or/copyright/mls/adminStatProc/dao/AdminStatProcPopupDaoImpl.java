package kr.or.copyright.mls.adminStatProc.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;

@Repository
public class AdminStatProcPopupDaoImpl extends SqlMapClientDaoSupport implements AdminStatProcPopupDao{

	@Autowired
	public AdminStatProcPopupDaoImpl(SqlMapClient sqlMapClient) {
		super();
		setSqlMapClient(sqlMapClient);
	}

	public List<String> statProcTargPopupExelDown(Map<String, Object> map) throws Exception {
		return getSqlMapClientTemplate().queryForList("AdminStatProc.statProcTargPopupExelDown", map);
	}
	

	public List<String> statProcPopupExelDown(Map<String, Object> map) throws Exception {
		return getSqlMapClientTemplate().queryForList("AdminStatProc.statProcPopupExelDown", map);
	}
	
	public List<String> statProcPopupExelDown2(Map<String, Object> map) throws Exception {
		return getSqlMapClientTemplate().queryForList("AdminStatProc.statProcPopupExelDown2", map);
	}
	
	public List<String> statProcWorksPopupExelDown(Map<String, Object> map) throws Exception {
		return getSqlMapClientTemplate().queryForList("AdminStatProc.statProcWorksPopupExelDown", map);
	}
	
	public List<String> statProcInfoExelDown(Map<String, Object> map) throws Exception {
		return getSqlMapClientTemplate().queryForList("AdminStatProc.statProcInfoExelDown", map);
	}

}
