package kr.or.copyright.mls.console.rwmnyreqst;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 보상금 신청 > 보상금 신청 목록
 * 
 * @author ljh
 */
@Controller
public class FdcrAd36Controller extends DefaultController{

	@Resource( name = "fdcrAd36Service" )
	private FdcrAd36ServiceImpl fdcrAd36Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd36Controller.class );

	/**
	 * 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rwmnyreqst/fdcrAd36List1.page" )
	public String fdcrAd36List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "GUBUN1", "" );
		commandMap.put( "GUBUN2", "" );
		commandMap.put( "GUBUN3", "" );
		commandMap.put( "PRPS_TITE", "" );
		commandMap.put( "USER", "" );
		commandMap.put( "DEAL_STAT", "" );
		commandMap.put( "START_RGST_DTTM", "" );
		commandMap.put( "END_RGST_DTTM", "" );
		commandMap.put( "TRST_ORGN_CODE", "" );
		commandMap.put( "FROM_NO", "" );
		commandMap.put( "TO_NO", "" );
		commandMap.put( "DIVS", "20" );

		fdcrAd36Service.fdcrAd36List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		System.out.println( "신청 목록 조회" );
		return "test";
	}

	/**
	 * 상세 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rwmnyreqst/fdcrAd36View1.page" )
	public String fdcrAd36View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "USER_IDNT", "" );
		commandMap.put( "PRPS_IDNT", "" );
		commandMap.put( "PRPS_DIVS", "" );
		commandMap.put( "PRPS_SEQN", "" );
		commandMap.put( "DEAL_STAT_NAME", "" );
		commandMap.put( "PRPS_TITE", "" );
		commandMap.put( "TRST_ORGN_CODE", "" );
		commandMap.put( "PRPS_MAST_KEY", "" );

		fdcrAd36Service.fdcrAd36View1( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_fileList", commandMap.get( "ds_fileList" ) );
		model.addAttribute( "ds_totDealStat", commandMap.get( "ds_totDealStat" ) );
		model.addAttribute( "ds_list_trst", commandMap.get( "ds_list_trst" ) );
		model.addAttribute( "ds_list_kapp", commandMap.get( "ds_list_kapp" ) );
		model.addAttribute( "ds_list_fokapo", commandMap.get( "ds_list_fokapo" ) );
		model.addAttribute( "ds_list_krtra", commandMap.get( "ds_list_krtra" ) );
		model.addAttribute( "ds_list_krtra2", commandMap.get( "ds_list_krtra2" ) );
		model.addAttribute( "ds_temp", commandMap.get( "ds_temp" ) );

		System.out.println( "상세 조회" );
		return "test";
	}

	/**
	 * 신청인 정보 조회
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rwmnyreqst/fdcrAd36View2.page" )
	public String fdcrAd36View2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "USER_IDNT", "" );

		fdcrAd36Service.fdcrAd36View2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "신청자 정보 조회" );
		return "test";
	}

	/**
	 * 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rwmnyreqst/fdcrAd36Download1.page" )
	public void fdcrAd36Download1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "PRPS_MAST_KEY", "" );

		fdcrAd36Service.fdcrAd36View1( commandMap );

		List list = (List) commandMap.get( "ds_fileList" );
		for( int i = 0; i < list.size(); i++ ){
			Map map = (Map) list.get( i );
			String prpsMastKey = (String) map.get( "PRPS_MAST_KEY" );
			if( prpsMastKey.equals( commandMap.get( "PRPS_MAST_KEY" ) ) ){
				String filePath = (String) map.get( "FILE_PATH" );
				String fileName = (String) map.get( "FILE_NAME" );
				String realFileName = (String) map.get( "REAL_FILE_NAME" );
				download( request, response, filePath + realFileName, fileName );
			}
		}
	}

	/**
	 * 처리결과 상세정보 팝업
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rwmnyreqst/fdcrAd36Pop1.page" )
	public String fdcrAd36Pop1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "ORGN_TRST_CODE", "" );
		commandMap.put( "PRPS_IDNT", "" );
		commandMap.put( "PRPS_SEQN", "" );
		commandMap.put( "DEAL_STAT", "" );
		commandMap.put( "RSLT_DESC", "" );

		fdcrAd36Service.fdcrAd36Pop1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "처리결과 상세정보 팝업" );
		return "test";
	}

	/**
	 * 신청내역 삭제
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rwmnyreqst/fdcrAd36Delete1.page" )
	public void fdcrAd36Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "TRST_ORGN_CODE", "" );

		String[] PRPS_MAST_KEYS = request.getParameterValues( "PRPS_MAST_KEY" );
		String[] PRPS_SEQNS = request.getParameterValues( "PRPS_SEQN" );
		String[] ATTC_SEQNS = request.getParameterValues( "ATTC_SEQN" );
		String[] REAL_FILE_NAMES = request.getParameterValues( "REAL_FILE_NAME" );
		commandMap.put( "PRPS_MAST_KEY", PRPS_MAST_KEYS );
		commandMap.put( "PRPS_SEQN", PRPS_SEQNS );
		commandMap.put( "ATTC_SEQN", ATTC_SEQNS );
		commandMap.put( "REAL_FILE_NAME", REAL_FILE_NAMES );

		boolean isSuccess = fdcrAd36Service.fdcrAd36Delete1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "신청내역 삭제" );
	}

	/**
	 * 처리결과 및 신청정보 저장
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rwmnyreqst/fdcrAd36Insert1.page" )
	public void fdcrAd36Insert1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "userId", "" );

		String[] PRPS_MAST_KEYS = request.getParameterValues( "PRPS_MAST_KEY" );
		String[] PRPS_IDNTS = request.getParameterValues( "PRPS_IDNT" );
		String[] PRPS_DIVSS = request.getParameterValues( "PRPS_DIVS" );
		String[] TRST_ORGN_CODES = request.getParameterValues( "TRST_ORGN_CODE" );

		commandMap.put( "PRPS_MAST_KEY", PRPS_MAST_KEYS );
		commandMap.put( "PRPS_IDNT", PRPS_IDNTS );
		commandMap.put( "PRPS_DIVS", PRPS_DIVSS );
		commandMap.put( "TRST_ORGN_CODE", TRST_ORGN_CODES );

		boolean isSuccess = fdcrAd36Service.fdcrAd36Insert1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "처리결과 및 신청정보 저장" );
	}
}
