package kr.or.copyright.mls.sample.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.sample.dao.SampleDao;

import com.tobesoft.platform.data.Dataset;

public class SampleServiceImpl extends BaseService implements SampleService {

	//private Log logger = LogFactory.getLog(getClass());

	private SampleDao sampleDao;
	
	public void setSampleDao(SampleDao sampleDao){
		this.sampleDao = sampleDao;
	}
	
	// ExcelImport
	public void excelImport() throws Exception {
		
		
		System.out.println("==================================================");
		System.out.println("call excelImport()");
		
		Dataset ds_excel = getDataset("ds_excel");
		
//		ds_excel.printDataset();
		
		for( int i=0; i<ds_excel.getRowCount(); i++) {
			
			Map map = getMap(ds_excel, i);
			
			sampleDao.sampleInsert(map); 
		}
		
		System.out.println("==================================================");
		System.out.println("end excelImport()");
	}
	
	
	// sampleList 조회 
	public void sampleList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list = (List) sampleDao.sampleList(map); 
		
		//hmap를 dataset로 변환한다.
		addList("ds_List", list);

	}

	// sampleList 저장  
	public void sampleSave() throws Exception {

		Dataset ds_List = getDataset("ds_List");   

		String row_status = null;

		
		// DELETE처리
		for( int i = 0 ; i< ds_List.getDeleteRowCount() ; i++ )
		{
			Map deleteMap = getDeleteMap(ds_List, i );
			sampleDao.sampleDelete(deleteMap); 
		}		
		
		//INSERT,  UPDATE처리
		for( int i=0;i<ds_List.getRowCount();i++ )
		{

			row_status = ds_List.getRowStatus(i);

			Map map = getMap(ds_List, i );
			
			if( row_status.equals("insert") == true )
			{
				//System.out.println("insert");
				//System.out.println(map.toString());
				sampleDao.sampleInsert(map); 
			}
			else if( row_status.equals("update") == true )
			{
				//System.out.println("update");
				//System.out.println(map.toString());				
				sampleDao.sampleUpdate(map); 
			}
	
		}
		
	}


	
	// sampleList 조회 
	public void musicList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) sampleDao.musicList(map);		
		List pageCount = (List) sampleDao.musicListCount(map); 
	
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		addList("ds_page", pageCount);		

	}	
	/*
	 * public void setBoardDao(BoardDao boardDao) { this.boardDao = boardDao; }
	 * 
	 * public void addBoard(Board board) { boardDao.addBoard(board); }
	 * 
	 * public int updateBoard(Board board) { return boardDao.updateBoard(board); }
	 * 
	 * public List findBoardList() {
	 * 
	 * List list = boardDao.findBoardList(); logger.debug(list);
	 * 
	 * return list; // return boardDao.findBoardList(); }
	 */
	
	
}
