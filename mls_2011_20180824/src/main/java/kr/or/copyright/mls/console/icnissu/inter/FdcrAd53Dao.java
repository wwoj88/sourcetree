package kr.or.copyright.mls.console.icnissu.inter;

import java.util.List;
import java.util.Map;

public interface FdcrAd53Dao{

	public List selectMuscList( Map map );

	public List selectMuscDeatilList( Map map );

	public void updateMuscIcnStat( Map map );

	public List selectBooksList( Map map );

	public void updateBooksIcnStat( Map map );

	public void updateBooksLicensorId_201( Map map );

	public void updateBooksLicensorId_202( Map map );

	public List selectPrivWorksList( Map map );

	public List selectPrivWorksDetailList( Map map );

	public List selectPrivWorksFileList( Map map );

	public void privWorksUpdate( Map map );

	public List getCommId( Map map );

	public void updateCommId( Map map );
}
