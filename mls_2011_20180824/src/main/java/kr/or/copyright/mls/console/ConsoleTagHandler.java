package kr.or.copyright.mls.console;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @version : 2015. 11. 29.
 * @author : 김성용(wizksy@asadal.com)
 * @author : 김성용
 * @분류 :
 * @수정이력 :
 */
public class ConsoleTagHandler extends TagSupport{

	private static final Logger logger = LoggerFactory.getLogger( ConsoleTagHandler.class );
	
	private static final long serialVersionUID = 1L;

	private String func;
	private String value;
	private String value1;
	private String value2;
	private String value3;
	private List list;

	public String getFunc(){
		return func;
	}

	
	public List getList(){
		return list;
	}
	
	public void setList( List list ){
		this.list = list;
	}

	public void setFunc(
		String func ){
		this.func = func;
	}

	public String getValue(){
		return value;
	}

	public void setValue(
		String value ){
		this.value = value;
	}

	public String getValue1(){
		return value1;
	}

	public void setValue1(
		String value1 ){
		this.value1 = value1;
	}
	
	public String getValue2(){
		return value2;
	}

	public void setValue2(
		String value2 ){
		this.value2 = value2;
	}
	
	public String getValue3(){
		return value3;
	}

	public void setValue3(
		String value3 ){
		this.value3 = value3;
	}

	@Override
	public int doStartTag() throws JspException{
		String result = "";
		try{
			if( !"".equals( func ) ){
				if( "strFileSize".equals( func ) ){
					result = convertFileSize( value );
				}else if( "getFileIcon".equals( func ) ){
					result = getFileIcon( value );
				}else if( "isChecked".equals( func ) ){
					result = isChecked( value, value1 );
				}else if( "isSelected".equals( func ) ){
					result = isSelected( value, value1 );
				}else if( "formatNumber".equals( func ) ){
					result = formatNumber( value );
				}else if( "getSubstring".equals( func ) ){
					result = getSubstring( value, value1,  value2);
				}else if( "getSplitn".equals( func ) ){
					result = getSplitn( value, value1,  value2);
				}else if( "getDate".equals( func ) ){
					result = getDate( value, value1, value2);
				}else if( "getYmdSelectBox".equals( func ) ){
					result = getYmdSelectBox( value, value1);
				}else if( "getYearSelectBox".equals( func ) ){
					result = getYearSelectBox( value, value1, value2, value3);
				}else if( "getYearSelectBox0".equals( func ) ){
					result = getYearSelectBox0( value, value1, value2, value3);
				}else if( "getMonthSelectBox".equals( func ) ){
					result = getMonthSelectBox( value, value1, value2);
				}else if( "getDaySelectBox".equals( func ) ){
					result = getDaySelectBox( value, value1, value2);
				}else if( "isClassOn".equals( func ) ){
					result = isClassOn( value, value1);
				}else if( "isParentClassOn".equals( func ) ){
					result = isParentClassOn( value, value1);
				}else if( "findRow".equals( func ) ){
					result = findRow( list, value, value1, value2);
				}
				
				
			}
			JspWriter out = pageContext.getOut();
			out.print( result );
		}
		catch( IOException e ){
//			e.printStackTrace();
			System.out.println("실행중 에러발생 ");
		}

		return SKIP_BODY;
	}
	
	public String findRow(List list, String value, String value1, String value2){
		String returnValue = "";
		try{
			if(null!=list && list.size() > 0){
				for(int i=0;i<list.size();i++){
					Map map = (Map)list.get( i );
					String str = EgovWebUtil.getString( map, value );
					if(str.equals(value1)){
						returnValue = value2;
						break;
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return returnValue;
	}
	/**
	 * 파일 사이즈 단위표시
	 * 
	 * @param strSize
	 * @return
	 */
	public String convertFileSize(
		String strSize ){
		String result = "";

		int size = 0;
		String unit = "byte";
		try{
			size = Integer.parseInt( strSize );
		}
		catch( RuntimeException e ){
			size = 0;
		}
		double dSize = 0;
		if( size > 1024 ){
			dSize = size / 1024;
			unit = "Kb";
		}else{
			dSize = size;
		}

		if( dSize > 1024 ){
			dSize = dSize / 1024;
			unit = "Mb";
		}

		if( dSize > 1024 ){
			dSize = dSize / 1024;
			unit = "Gb";
		}
		NumberFormat nf = NumberFormat.getNumberInstance();
		result = nf.format( dSize ) + " " + unit;
		return result;
	}

	/**
	 * 파일 사이즈 단위표시
	 * 
	 * @param strSize
	 * @return
	 */
	public String getFileIcon(
		String fileName ){
		String result = "";
		if( null != fileName && !"".equals( fileName ) ){
			String ext = ( fileName.substring( fileName.lastIndexOf( "." ) + 1 ) ).toUpperCase();
			if( "XLS".equals( ext ) || "XLSX".equals( ext ) ){
				result = "<img src='/console/custom/img/icon/excel_icon.png' alt='엑셀아이콘' />";
			}else if( "HWP".equals( ext ) ){
				result = "<img src='/console/custom/img/icon/hwp_icon.png' alt='한글아이콘' />";
			}else if( "DOC".equals( ext )  || "DOCX".equals( ext ) ){
				result = "<img src='/console/custom/img/icon/word_icon.png' alt='워드아이콘' />";
			}else if( "PPT".equals( ext )  || "PPTX".equals( ext ) ){
				result = "<img src='/console/custom/img/icon/ppt_icon.png' alt='파워포인트아이콘' />";
			}else if( "RAR".equals( ext ) ){
				result = "<img src='/console/custom/img/icon/rar_icon.png' alt='RAR아이콘' />";
			}else if( "ZIP".equals( ext ) ){
				result = "<img src='/console/custom/img/icon/zip_icon.png' alt='ZIP아이콘' />";
			}else if( "PDF".equals( ext ) ){
				result = "<img src='/console/custom/img/icon/pdf_icon.png' alt='PDF아이콘' />";
			}else if( "JPG".equals( ext )  || "PNG".equals( ext )   || "GIF".equals( ext )   || "BMP".equals( ext )){
				result = "<img src='/console/custom/img/icon/img_icon.png' alt='이미지아이콘' />";
			}else{
				result = "<img src='/console/custom/img/icon/file_icon.png' alt='파일다운로드아이콘' />";
			}
		}

		return result;
	}

	/**
	 * radio, check checked
	 * 
	 * @param strSize
	 * @return
	 */
	public String isChecked(
		String value,
		String value1 ){
		String result = "";
		if( null != value && !"".equals( value ) ){
			if( value.equals( value1 ) ){
				result = "checked";
			}
		}
		return result;
	}

	/**
	 * select selected
	 * 
	 * @param strSize
	 * @return
	 */
	public String isSelected(
		String value,
		String value1 ){
		String result = "";
		if( null != value && !"".equals( value ) ){
			if( value.equals( value1 ) ){
				result = "selected";
			}
		}
		return result;
	}

	/**
	 * select selected
	 * 
	 * @param strSize
	 * @return
	 */
	public String getDate(
		String value,
		String value1,
		String value2){
		String result = "";
		
		try {
			Date date = null;
			if(!"".equals(value)){
				date = new SimpleDateFormat(value1).parse(value);
			}else{
				date = new Date();
			}
			SimpleDateFormat sdf = new SimpleDateFormat(value2);
			result = sdf.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}

	public String formatNumber(
		String value ){
		String result = "";
		if(null!=value && !"".equals(value)){
			int number = NumberUtils.createInteger( value );
	//		int digit = NumberUtils.createInteger( value1 );
			NumberFormat nf = NumberFormat.getIntegerInstance();
	//		nf.setMinimumIntegerDigits( digit );
			result = nf.format( number );
		}
		return result;
	}
	
	/**
	 * 문자열 자르기
	 * 
	 * @param strSize
	 * @return
	 */
	public String getSubstring(
		String value,
		String value1,
		String value2){
		
		int startIdx = Integer.parseInt( value1 );
		int endIdx = Integer.parseInt( value2 );
				
		String result = "";
		if(null!=value && !"".equals(value)){
			if(value.length() >= endIdx){
				try{
					result = value.substring( startIdx, endIdx );
				}catch(StringIndexOutOfBoundsException e){
				}
			}
		}
		return result;
	}
	
	/**
	 * 문자열자르기 (구분자이용)
	 * 
	 * @param strSize
	 * @return
	 */
	public String getSplitn(
		String value,
		String value1,
		String value2){
		
    	if(value == null) return "";
    	String[] tmp = value.split(value1);
    	
    	if(tmp.length<=Integer.parseInt( value2 )){
    		return "";
    	}
    	return tmp[Integer.parseInt( value2 )];
	}
	
	/**
	 * 년월일 셀렉트 박스 만들기
	 * 
	 * @param strSize
	 * @return
	 */
	public String getYmdSelectBox(
		String value,
		String value1){
		StringBuffer result = new StringBuffer();

		if(null==value1 || "".equals(value1)){
			value1 = "ss";
		}
				
		SimpleDateFormat fommatter = new SimpleDateFormat("yyyyMMdd");
		String selDate = fommatter.format(Calendar.getInstance().getTime());
		String selyear = selDate.substring(0,4);
		int y = EgovWebUtil.toInt(selyear);
		String syear = "";
		String smonth = "";
		String sday = "";
		if(null!=value && !"".equals(value) && value.length() == 8){
			syear = value.substring(0,4);
			smonth = value.substring(4,6);
			sday = value.substring(6,8);
		}
		try{
			result.append("<select id=\""+value1+"year\" name=\""+value1+"year\">");
			for(int i=1950;i<=y;i++){
				result.append("<option value=\""+i+"\" "+isSelected(i+"",syear)+">"+i+"</option>");	
			}
			result.append("</select>");
			
			result.append("<select id=\""+value1+"month\" name=\""+value1+"month\">");
			for(int i=1;i<=12;i++){
				if(i<10){
					result.append("<option value=\""+"0"+i+"\" "+isSelected("0"+i,smonth)+">"+i+"</option>");	
				}else{
					result.append("<option value=\""+i+"\" "+isSelected(i+"",smonth)+">"+i+"</option>");	
				}
			}
			result.append("</select>");
			
			result.append("<select id=\""+value1+"day\" name=\""+value1+"day\">");
			for(int i=1;i<=31;i++){
				if(i<10){
					result.append("<option value=\""+"0"+i+"\" "+isSelected("0"+i,sday)+">"+i+"</option>");		
				}else{
					result.append("<option value=\""+i+"\" "+isSelected(i+"",sday)+">"+i+"</option>");	
				}
			}
			result.append("</select>");
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return result.toString();
	}
	
	
	/**
	 * 년 셀렉트 박스 만들기
	 * 
	 * @param strSize
	 * @return
	 */
	public String getYearSelectBox(
		String value,
		String value1,
		String value2,
		String value3){
		StringBuffer result = new StringBuffer();

		Calendar cldr = Calendar.getInstance();
		int thisYear = cldr.get(Calendar.YEAR);
		int nextYear = thisYear - 11;
		int thisYear2 = thisYear + 1;
				
		try{
			result.append("<select id=\""+value1+"\" name=\""+value1+"\" class=\""+value3+"\">");
			if(value2!=null && "Y".equals(value2)){
				result.append("<option value=\"\">-전체-</option>");	
			}
			for(int i=thisYear2;i>=nextYear;i--){
				result.append("<option value=\""+i+"\" "+isSelected(i+"",value)+">"+i+"</option>");	
			}
			result.append("</select>");
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return result.toString();
	}
	
	/**
	 * 년 셀렉트 박스 만들기(기존)
	 * 
	 * @param strSize
	 * @return
	 */
	public String getYearSelectBox0(
		String value,
		String value1,
		String value2,
		String value3){
		StringBuffer result = new StringBuffer();

		Calendar cldr = Calendar.getInstance();
		int thisYear = cldr.get(Calendar.YEAR);
		System.out.println("thisYear:::::::::::::"+thisYear);
		int nextYear = thisYear - 10;
				
		try{
			result.append("<select id=\""+value1+"\" name=\""+value1+"\" class=\""+value3+"\">");
			if(value2!=null && "Y".equals(value2)){
				result.append("<option value=\"\">-전체-</option>");	
			}
			for(int i=thisYear;i>=nextYear;i--){
				result.append("<option value=\""+i+"\" "+isSelected(i+"",value)+">"+i+"</option>");	
			}
			result.append("</select>");
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return result.toString();
	}
	
	/**
	 * 월 셀렉트 박스 만들기
	 * 
	 * @param strSize
	 * @return
	 */
	public String getMonthSelectBox(
		String value,
		String value1,
		String value2){
		StringBuffer result = new StringBuffer();

		Calendar cldr = Calendar.getInstance();
		int thisMonth = cldr.get(Calendar.MONTH)+1;
		if("".equals(value)){
			if(thisMonth < 10){
				value = "0"+thisMonth;
			}else{
				value = thisMonth+"";
			}
			
		}
		String disabledStr = "";
		if("N".equals(value2)){
			disabledStr = "disabled";
		}
		try{
			result.append("<select id=\""+value1+"\" "+disabledStr+" name=\""+value1+"\" =\""+value1+"\">");
			result.append("<option value=\"\">-전체-</option>");	
			for(int i=1;i<=12;i++){
				if(i<10){
					result.append("<option value=\""+"0"+i+"\" "+isSelected("0"+i,value)+">"+i+"</option>");	
				}else{
					result.append("<option value=\""+i+"\" "+isSelected(i+"",value)+">"+i+"</option>");	
				}
			}
			result.append("</select>");
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return result.toString();
	}
	
	/**
	 * 월 셀렉트 박스 만들기
	 * 
	 * @param strSize
	 * @return
	 */
	public String getDaySelectBox(
		String value,
		String value1,
		String value2){
		StringBuffer result = new StringBuffer();

		Calendar cldr = Calendar.getInstance();
		int thisMonth = cldr.get(Calendar.DATE);
		if("".equals(value)){
			if(thisMonth < 10){
				value = "0"+thisMonth;
			}else{
				value = thisMonth+"";
			}
			
		}
		String disabledStr = "";
		if("N".equals(value2)){
			disabledStr = "disabled";
		}
		try{
			result.append("<select id=\""+value1+"\" "+disabledStr+" name=\""+value1+"\">");
			for(int i=1;i<=31;i++){
				if(i<10){
					result.append("<option value=\""+"0"+i+"\" "+isSelected("0"+i,value)+">"+i+"</option>");	
				}else{
					result.append("<option value=\""+i+"\" "+isSelected(i+"",value)+">"+i+"</option>");	
				}
			}
			result.append("</select>");
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return result.toString();
	}
	
	/**
	 * class on
	 * 
	 * @param strSize
	 * @return
	 */
	public String isClassOn(
		String value,
		String value1 ){
		String result = "";
		if( null != value && !"".equals( value ) ){
			if( value.equals( value1 ) ){
				result = "class=\"on\"";
			}
		}
		return result;
	}
	
	
	/**
	 * class on
	 * 
	 * @param strSize
	 * @return
	 */
	public String isParentClassOn(
		String value,
		String value1 ){
		String result = "";
		if( null != value && !"".equals( value ) ){
			if(value1.startsWith(value)){
				result = "class=\"on\"";
			}
		}
		return result;
	}
}
