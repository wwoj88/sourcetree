package kr.or.copyright.mls.console.ntcn;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.ntcn.inter.FdcrAd75Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAd75Dao" )
public class FdcrAd75DaoImpl extends EgovComAbstractDAO implements FdcrAd75Dao{

	/**
	 * 공통 첨부파일 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings( "unchecked" )
	public ArrayList<Map<String, Object>> selectAttachFileList( Map<String, Object> commandMap ) throws SQLException{
		return (ArrayList<Map<String, Object>>) list( "FdcrAd75.selectAttachFileList", commandMap );
	}

	/**
	 * 공통 첨부파일 정보
	 * 
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings( "unchecked" )
	public Map<String, Object> selectAttachFileInfo( Map<String, Object> commandMap ) throws SQLException{
		return (Map<String, Object>) selectByPk( "FdcrAd75.selectAttachFileInfo", commandMap );
	}

	/**
	 * 첨부파일 등록
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public int insertBoardFile( Map<String, Object> commandMap ) throws SQLException{
		insert( "FdcrAd75.insertBoardFile", commandMap );
		return 1;
	}

	/**
	 * 게시물 목록 카운트
	 * 
	 * @return
	 * @throws SQLException
	 */
	public int selectListCount( Map<String, Object> commandMap ) throws SQLException{
		return (Integer) selectByPk( "FdcrAd75.selectListCount", commandMap );
	}

	/**
	 * 게시물 목록 정보
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings( "unchecked" )
	public ArrayList<Map<String, Object>> selectList( Map<String, Object> commandMap ) throws SQLException{
		return (ArrayList<Map<String, Object>>) list( "FdcrAd75.selectList", commandMap );
	}

	/**
	 * 게시물 조회
	 * 
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings( "unchecked" )
	public Map<String, Object> selectView( Map<String, Object> commandMap ) throws SQLException{
		return (Map<String, Object>) selectByPk( "FdcrAd75.selectView", commandMap );
	}

	/**
	 * 등록
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public int insertBoardInfo( Map<String, Object> commandMap ) throws SQLException{
		return (Integer)insert( "FdcrAd75.insertBoardInfo", commandMap );
	}

	/**
	 * 수정
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public int updateBoardInfo( Map<String, Object> commandMap ) throws SQLException{
		return (int) update( "FdcrAd75.updateBoardInfo", commandMap );
	}

	/**
	 * 삭제
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public int deleteBoardInfo( Map<String, Object> commandMap ) throws SQLException{
		return (int) delete( "FdcrAd75.deleteBoardInfo", commandMap );
	}

	/**
	 * 삭제
	 * 
	 * @param menuVO
	 * @return
	 * @throws SQLException
	 */
	public int deleteBoardFile( Map<String, Object> commandMap ) throws SQLException{
		return (int) delete( "FdcrAd75.deleteBoardFile", commandMap );
	}
}
