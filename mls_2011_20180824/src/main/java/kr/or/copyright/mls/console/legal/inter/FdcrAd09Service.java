package kr.or.copyright.mls.console.legal.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface FdcrAd09Service{

	/**
	 * �̿���� ��û ���� ���� ���
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> fdcrAd09List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * �̿���ν�û ���� ���� ��
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd09View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * �̿���ν�û ���� ���� ���� ��
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd09UpdateForm1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * �̿���ν�û ���� ���� ����
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd09Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * �̿���ν�û ���� ���� ����
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd09Delete1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * �̿���ν�û ���� ���� �Խ��� ��� ��
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd09RegiForm1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * �̿���ν�û ���� ���� �Խ��� ���
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd09Regi1( Map<String, Object> commandMap  ) throws Exception;

	/**
	 * �̿���ν�û ���� ���� �ϰ���� ���� ���ε�
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd09Upload1( ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * �̿���ν�û ���� ���� �Խ��� �ϰ����
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd09Regi2( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * �̿���ν�û ���� ���� ��� �����ٿ�ε�
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd09ExcelDown1( Map<String, Object> commandMap ) throws Exception;
}
