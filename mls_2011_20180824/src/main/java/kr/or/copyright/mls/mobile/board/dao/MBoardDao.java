package kr.or.copyright.mls.mobile.board.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.mobile.board.model.MBoard;

public interface MBoardDao {
	
	public List findBoardList(Map params);
	
	public int findBoardCount(Map params);
	
	public MBoard boardView(MBoard board);
}
