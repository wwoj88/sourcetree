package kr.or.copyright.mls.console.legalmanage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.legalmanage.inter.FdcrAd42Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAd42Dao" )
public class FdcrAd42DaoImpl extends EgovComAbstractDAO implements FdcrAd42Dao{

	public List faqList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.faqList", map );
	}

	public List totalRowFaqList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalRowFaqList", map );
	}

	public void faqUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.faqUpdate", map );
	}

	public void faqInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.faqInsert", map );
	}

	public void faqDelete( Map map ){
		System.out.println( map.toString() );
		getSqlMapClientTemplate().insert( "AdminBoard.faqDelete", map );
	}

	public List faqDetailList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.faqDetailList", map );
	}

	public void faqFileUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.faqFileUpdate", map );
	}

	public void faqFileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.faqFileInsert", map );
	}

	public void faqFileDelete( Map map ){
		System.out.println( map.toString() );
		getSqlMapClientTemplate().insert( "AdminBoard.faqFileDelete", map );
	}

	public List qnaList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.qnaList", map );
	}

	/* 양재석 추가 start */
	public List totalRowQnaList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalRowQnaList", map );
	}

	/* 양재석 추가 end */

	public List qnaDetailList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.qnaDetailList", map );
	}

	public List faqFileList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.faqFileList", map );
	}

	public void qnaInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.qnaInsert", map );
	}

	public int bordSeqnMax(){
		Integer max = (Integer) getSqlMapClientTemplate().queryForObject( "AdminBoard.bordSeqnMax", null );
		return max.intValue();
	}

	public void updateReply( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.updateReply", map );
	}

	public void qnaUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.qnaUpdate", map );
	}

	public void qnaDeleteAll( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.qnaDeleteAll", map );
	}

	public void qnaDelete( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.qnaDelete", map );
	}

	public List qnaFileList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.qnaFileList", map );
	}

	public List qnaFileAllList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.qnaFileAllList", map );
	}

	public void updateReplyFile( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.updateReplyFile", map );
	}

	public void qnaFileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.qnaFileInsert", map );
	}

	public void qnaFileDeleteAll( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.qnaFileDeleteAll", map );
	}

	public void qnaFileDelete( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.qnaFileDelete", map );
	}

	/* 양재석 추가 start */
	public List sysmNotiList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.sysmNotiList", map );
	}

	public List totalRowSysmNotiList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalRowSysmNotiList", map );
	}

	public void sysmNotiUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.sysmNotiUpdate", map );
	}

	public void sysmNotiInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.sysmNotiInsert", map );
	}

	public void sysmNotiDelete( Map map ){
		System.out.println( map.toString() );
		getSqlMapClientTemplate().insert( "AdminBoard.sysmNotiDelete", map );
	}

	public List sysmNotiDetailList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.sysmNotiDetailList", map );
	}

	public void sysmNotiFileUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.sysmNotiFileUpdate", map );
	}

	public void sysmNotiFileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.sysmNotiFileInsert", map );
	}

	public void sysmNotiFileDelete( Map map ){
		System.out.println( map.toString() );
		getSqlMapClientTemplate().insert( "AdminBoard.sysmNotiFileDelete", map );
	}

	public List sysmNotiFileList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.sysmNotiFileList", map );
	}

	public List notiList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.notiList", map );
	}

	public List totalRowNotiList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalRowNotiList", map );
	}

	public void notiUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.notiUpdate", map );
	}

	public void notiInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.notiInsert", map );
	}

	public void notiDelete( Map map ){
		System.out.println( map.toString() );
		getSqlMapClientTemplate().insert( "AdminBoard.notiDelete", map );
	}

	public List notiDetailList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.notiDetailList", map );
	}

	public void notiFileUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.notiFileUpdate", map );
	}

	public void notiFileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.notiFileInsert", map );
	}

	public void notiFileDelete( Map map ){
		System.out.println( map.toString() );
		getSqlMapClientTemplate().insert( "AdminBoard.notiFileDelete", map );
	}

	public List notiFileList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.notiFileList", map );
	}

	public List promList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.promList", map );
	}

	public List totalRowPromList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalRowPromList", map );
	}

	public void promUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.promUpdate", map );
	}

	public void promInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.promInsert", map );
	}

	public void promDelete( Map map ){
		System.out.println( map.toString() );
		getSqlMapClientTemplate().insert( "AdminBoard.promDelete", map );
	}

	public List promFileList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.promFileList", map );
	}

	public List promDetailList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.promDetailList", map );
	}

	public void promFileUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.promFileUpdate", map );
	}

	public void promFileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.promFileInsert", map );
	}

	public void promFileDelete( Map map ){
		System.out.println( map.toString() );
		getSqlMapClientTemplate().insert( "AdminBoard.promFileDelete", map );
	}

	public List statList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.statList", map );
	}

	public List totalRowStatList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalRowStatList", map );
	}

	public void statInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.statInsert", map );
	}

	public void statFileInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.statFileInsert", map );
	}

	public List clmsWorksList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.clmsWorksList", map );
	}

	public List totalClmsWorksList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalClmsWorksList", map );
	}

	public List clmsBookList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.clmsBookList", map );
	}

	public List totalClmsBookList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalClmsBookList", map );
	}

	public List clmsMuscDetail( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.clmsMuscDetail", map );
	}

	public List clmsBookDetail( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.clmsBookDetail", map );
	}

	/* 양재석 추가 end */

	public void statFileDelete( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.statFileDelete", map );
	}

	public void statDelete( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.statDelete", map );
	}

	public void statUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminBoard.statUpdate", map );
	}

	@SuppressWarnings( "unchecked" )
	public Map<String, Object> statDetailList( Map<String, Object> commandMap ){
		return (Map<String, Object>) selectByPk( "AdminBoard.statDetailList", commandMap );
	}

	@SuppressWarnings( "unchecked" )
	public ArrayList<Map<String, Object>> statFileList( Map<String, Object> commandMap ) throws SQLException{
		return (ArrayList<Map<String, Object>>) list( "AdminBoard.statFileList", commandMap );
	}

	public List clmsBroadList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.clmsBroadList", map );
	}

	public List totalClmsBroadList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalClmsBroadList", map );
	}

	public List clmsImageList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.clmsImageList", map );
	}

	public List totalClmsImageList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalClmsImageList", map );
	}

	public List clmsMovieList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.clmsMovieList", map );
	}

	public List totalClmsMovieList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalClmsMovieList", map );
	}

	public List clmsBroadCastList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.clmsBroadCastList", map );
	}

	public List totalClmsBroadCastList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.totalClmsBroadCastList", map );
	}

	public void statFildDelete( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.statFildDelete", map );
	}

	public void statFildInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminBoard.statFildInsert", map );
	}

	public int statSeqn(){
		Integer max = (Integer) getSqlMapClientTemplate().queryForObject( "AdminBoard.statSeqn", null );
		return max.intValue();
	}

	public List admSysmNotiList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminBoard.admSysmNotiList", map );
	}

}
