package kr.or.copyright.mls.main.dao;

import java.util.List;

import kr.or.copyright.mls.main.model.Main;

public interface MainDao {

	public List qustList(Main main);
	
	public List inmtList(Main main);
	
	public List workList(Main main);
	
	public void insertUserCount();
	
	public List notiList(Main main);
	
	public List promPotoList(Main main);
	
	public List promMovieList(Main main);
	
	public List lawList(Main main);
	
	public List workNoneList(Main main);
	
	public String alltInmt();
	public String alltInmt202(Main main);
	public String alltInmt203(Main main);
	public String alltInmtL(Main main);
	public String alltInmtS(Main main);
	public List anucBord01();
	public List anucBord06();
	public List anucBord03();
	public List anucBord04();
	public List anucBord05();
	public List QnABord();
	public List statSrch();
	public List notiList();

	public List anucBord07();
}
