package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;

public interface FdcrAd98Service{

	/**
	 * 검색어 순위 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd98List1( Map<String, Object> commandMap ) throws Exception;
}
