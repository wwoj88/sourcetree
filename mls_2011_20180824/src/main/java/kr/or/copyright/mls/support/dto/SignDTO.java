package kr.or.copyright.mls.support.dto;

/**
 * 공인인증서 관련정보용
 * 
 * @author 이상유
 * @version $Id: SignDTO.java,v 1.1 2012/07/02 01:06:03 yagin14 Exp $
 */
public class SignDTO {

	/** 공인 인증 및 전자서명에 사용되는 Attributes **/
	private String MemberType;				// 사용자 유형(MEMBER_TYPE)
	private String SsnNo;					// 전자서명용 주민등록번호
	private String CrnNo;					// 전자서명용 사업자등록번호
	private String CertId;					// 전자서명용 CertID
	private String UserSignCert;			// 공인인증서 정보
	private String UserSignValue;			// 전자서명 값
	private String EncryptedSessionKey;		// 비밀키를 암호화한 정보
	private String EncryptedUserRandomNumber;	// 공인인증서 확인을 위한 정보
	private String EncryptedUserSSN;		// 공인인증서를 통하여 암호화 된 주민등록번호
	
	private String Challenge;	// 로그인 사용자 인증 데이터를 매번 다르게 하기 위하여 WAS 서버에서 랜덤하게 생성한 nonce 값
	private String OriginalMessage;			// 전자서명을 위한 원본 메시지 
	private String SignValidate;			// 인증서 검사정보를 설정한다.
	
	/**
	 * @return the CertId
	 */
	public String getCertId() {
		return CertId;
	}
	/**
	 * @param certID the CertId to set
	 */
	public void setCertId(String certID) {
		CertId = certID;
	}
	
	/**
	 * @return the SsnNo
	 */
	public String getSsnNo() {
		return SsnNo;
	}
	/**
	 * @param ssnNo the CertId to set
	 */
	public void setSsnNo(String ssnNo) {
		SsnNo = ssnNo;
	}
	/**
	 * @return the UserSignCert
	 */
	public String getUserSignCert() {
		return UserSignCert;
	}
	/**
	 * @param userSignCert the UserSignCert to set
	 */
	public void setUserSignCert(String userSignCert) {
		UserSignCert = userSignCert;
	}
	/**
	 * @return the EncryptedSessionKey
	 */
	public String getEncryptedSessionKey() {
		return EncryptedSessionKey;
	}
	/**
	 * @param encryptedSessionKey the EncryptedSessionKey to set
	 */
	public void setEncryptedSessionKey(String encryptedSessionKey) {
		EncryptedSessionKey = encryptedSessionKey;
	}
	/**
	 * @return the EncryptedUserRandomNumber
	 */
	public String getEncryptedUserRandomNumber() {
		return EncryptedUserRandomNumber;
	}
	/**
	 * @param encryptedUserRandomNumber the EncryptedUserRandomNumber to set
	 */
	public void setEncryptedUserRandomNumber(String encryptedUserRandomNumber) {
		EncryptedUserRandomNumber = encryptedUserRandomNumber;
	}
	/**
	 * @return the EncryptedUserSSN
	 */
	public String getEncryptedUserSSN() {
		return EncryptedUserSSN;
	}
	/**
	 * @param encryptedUserSSN the EncryptedUserSSN to set
	 */
	public void setEncryptedUserSSN(String encryptedUserSSN) {
		EncryptedUserSSN = encryptedUserSSN;
	}
	/**
	 * @return the UserSignValue
	 */
	public String getUserSignValue() {
		return UserSignValue;
	}
	/**
	 * @param userSignValue the UserSignValue to set
	 */
	public void setUserSignValue(String userSignValue) {
		UserSignValue = userSignValue;
	}
	/**
	 * @return the SignValidate
	 */
	public String getSignValidate() {
		return SignValidate;
	}
	/**
	 * @param signValidate the SignValidate to set
	 */
	public void setSignValidate(String signValidate) {
		this.SignValidate = signValidate;
	}	
	/**
	 * @return the CrnNo
	 */
	public String getCrnNo() {
		return CrnNo;
	}	
	/**
	 * @param crnNo the CrnNo to set
	 */
	public void setCrnNo(String crnNo) {
		CrnNo = crnNo;
	}
	/**
	 * @return the Challenge
	 */
	public String getChallenge() {
		return Challenge;
	}
	/**
	 * @param challenge the Challenge to set
	 */
	public void setChallenge(String challenge) {
		Challenge = challenge;
	}
	/**
	 * @return the OriginalMessage
	 */
	public String getOriginalMessage() {
		return OriginalMessage;
	}
	/**
	 * @param originalMessage the OriginalMessage to set
	 */
	public void setOriginalMessage(String originalMessage) {
		OriginalMessage = originalMessage;
	}
	public String getMemberType() {
		return MemberType;
	}
	public void setMemberType(String memberType) {
		MemberType = memberType;
	}
}
