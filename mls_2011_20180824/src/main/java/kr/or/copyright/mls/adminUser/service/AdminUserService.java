package kr.or.copyright.mls.adminUser.service;


public interface AdminUserService {
	
	// 회원정보 목록조회
	public void userList() throws Exception;

	//회원정보 상세조회
	public void userDetlList() throws Exception;
	
	//신탁단체 담당자목록 조회
	public void trstOrgnMgnbList() throws Exception;	
	
	//신탁단체 담당자 상세 조회
	public void trstOrgnMgnbDetlList() throws Exception;	
	
	// 관리자 아이디 중복체크
	public void adminIdCheck() throws Exception;
	
	//신탁단체 담당자 입력
	public void trstOrgnMgnbDetlInsert() throws Exception;	
	
	//신탁단체 담당자 수정
	public void trstOrgnMgnbDetlUpdate() throws Exception;	
	
	//담당자,회원  비밀번호 수정
	public void userMgnbPswdUpdate() throws Exception;	
	
	//기관/단체 목록 조회
	public void trstOrgnCoNameList() throws Exception;	

	// 대표담당자 존재여부 체크
	public void orgnMgnbCheck() throws Exception;
}
