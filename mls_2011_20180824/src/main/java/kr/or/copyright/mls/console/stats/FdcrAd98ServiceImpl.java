package kr.or.copyright.mls.console.stats;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.stats.inter.FdcrAd98Dao;
import kr.or.copyright.mls.console.stats.inter.FdcrAd98Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd98Service" )
public class FdcrAd98ServiceImpl extends CommandService implements FdcrAd98Service{

	// old StatSrchDao
	@Resource( name = "fdcrAd98Dao" )
	private FdcrAd98Dao fdcrAd98Dao;

	/**
	 * 검색어 순위 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd98List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd98Dao.statSrchList( commandMap );
		commandMap.put( "ds_list", list );
	}
}
