package kr.or.copyright.mls.rsltInqr.service;

import java.util.List;

import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.inmtPrps.model.InmtPrps;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

public interface RsltInqrService {

	public ListResult rsltInqrList(int pageNo, int rowPerPage, RghtPrps rghtPrps);
	
	public RghtPrps rghtPrpsDetl(RghtPrps rghtPrps);
	
	public List rghtFileList(RghtPrps rghtPrps);
	
	public List rsltTrstWorksList(RghtPrps rghtPrps);
	
	public RghtPrps rsltTrstWorksDesc(RghtPrps rghtPrps);
	
	public List rghtPrpsWorkList(RghtPrps rghtPrps);
	
	public InmtPrps inmtPrpsDetl (RghtPrps rghtPrps);
	
	public List inmtPrpsRsltList(RghtPrps rghtPrps);
	
	public String inmtDealStatTotal(RghtPrps rghtPrps);
	
	public List inmtWorkList(RghtPrps rghtPrps);
	
	public List inmtKappRsltList(RghtPrps rghtPrps);
	
	public List inmtFokapoRsltList(RghtPrps rghtPrps);

	public List inmtKrtraRsltList(RghtPrps rghtPrps);
	
	public List inmtTempRsltList(RghtPrps rghtPrps);

	public List inmtFileList(RghtPrps rghtPrps);

	public String inmtTotalDealStat(RghtPrps rghtPrps);
	
	public String isValidDealStat(RghtPrps rghtPrps);
	
	public void deleteRsltInqr(RghtPrps rghtPrps);
	
}
