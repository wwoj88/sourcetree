package kr.or.copyright.mls.user.model;

public class ZipCodeRoad {
    
    private String sidoNm;
    private String gugunNm;
    private String zipcode;
    
    @Override
    public String toString() {
	return "ZipCodeRoad [sidoNm=" + sidoNm + ", gugunNm=" + gugunNm
		+ ", zipcode=" + zipcode + "]";
    }
    public String getSidoNm() {
        return sidoNm;
    }
    public void setSidoNm(String sidoNm) {
        this.sidoNm = sidoNm;
    }
    public String getGugunNm() {
        return gugunNm;
    }
    public void setGugunNm(String gugunNm) {
        this.gugunNm = gugunNm;
    }
    public String getZipcode() {
        return zipcode;
    }
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
    

}
