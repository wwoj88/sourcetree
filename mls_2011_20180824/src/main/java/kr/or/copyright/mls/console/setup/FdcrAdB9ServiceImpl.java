package kr.or.copyright.mls.console.setup;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd0301Dao;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Dao;
import kr.or.copyright.mls.console.setup.inter.FdcrAdB9Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdB9Service" )
public class FdcrAdB9ServiceImpl extends CommandService implements FdcrAdB9Service{

	@Resource( name = "fdcrAd83Dao" )
	private FdcrAd83Dao fdcrAd83Dao;

	@Resource( name = "fdcrAd0301Dao" )
	private FdcrAd0301Dao fdcrAd0301Dao;

	/**
	 * 신청 담당자 메일수신관리 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB9List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		/*
		 * 기존 소스에 주석처리 되어있음 
		 * List list = (List) fdcrAd03Dao.selectMailMgntList();
		 * commandMap.put( "ds_list", list ); 
		 * 기존소스에 서비스단 adminStatProcService 로 표시되어있지만 오류인듯, 
		 * 소스 검색 결과 adminEvnSetService
		 */

		// DAO 호출
		List list = (List) fdcrAd0301Dao.selectMailMgntList( commandMap );
		commandMap.put( "ds_list", list );
	}

	/**
	 * 신청기능구분 코드목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB9List2( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd0301Dao.SelectMailMgntCodeList( commandMap );

		commandMap.put( "ds_gubun", list );

	}

	/**
	 * 메일 신청 담당자 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdB9Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			String[] MGNT_DIVSS = (String[]) commandMap.get( "MGNT_DIVS" );
			String[] USER_IDNTS = (String[]) commandMap.get( "USER_IDNT" );
			String MGNT_CODE = (String) commandMap.get( "MGNT_CODE" );

			for( int i = 0; i < MGNT_DIVSS.length; i++ ){
				Map<String, Object> dsMap = new HashMap<String, Object>();
				dsMap.put( "MGNT_CODE", commandMap.get( "MGNT_CODE" ) );
				dsMap.put( "MGNT_DIVS", MGNT_DIVSS[i] );
				dsMap.put( "USER_IDNT", USER_IDNTS[i] );

				if( dsMap.get( "CHK" ).equals( "1" ) ){
					fdcrAd0301Dao.deleteMailMgntList( dsMap );
				}
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 중복확인
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB9List3( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		// 관리자ID체크
		int iUserCnt = fdcrAd0301Dao.isCheckMailMgntList( commandMap );
		HashMap h = new HashMap();
		h.put( "ID_CHECK_CNT", iUserCnt );

		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_condition", h );

	}

	/**
	 * 메일 신청 담당자 추가
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdB9Insert1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			fdcrAd0301Dao.insertMailMgntList( commandMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 담당자 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB9List4( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd83Dao.trstOrgnMgnbList( commandMap );

		commandMap.put( "ds_list", list );

	}

}
