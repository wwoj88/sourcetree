package kr.or.copyright.mls.inmtPrps.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.common.common.utils.XssUtil;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.inmtPrps.model.InmtPrps;
import kr.or.copyright.mls.inmtPrps.service.InmtPrpsService;
import kr.or.copyright.mls.rghtPrps.model.PrpsAttc;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;
import kr.or.copyright.mls.rghtPrps.service.RghtPrpsService;
import kr.or.copyright.mls.rsltInqr.service.RsltInqrService;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.FileUploadUtil;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;
import kr.or.copyright.mls.support.util.SessionUtil;
import kr.or.copyright.mls.support.util.StringUtil;
import kr.or.copyright.mls.user.service.UserService;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import com.dreamsecurity.jcaos.asn1.mc;

public class InmtPrpsController extends MultiActionController {

     // private Log logger = LogFactory.getLog(getClass());

     private InmtPrpsService inmtPrpsService;
     private RghtPrpsService rghtPrpsService;
     private RsltInqrService rsltInqrService;
     private UserService userService;

     // private String realUploadPath = "D:/server/mls/mls_2010/upload/"; // 로컬용
     // private String realUploadPath = "C:/home/tmax/mls/web/upload/"; // 로컬용
     // private String realUploadPath = "/home/tmax/mls/web/upload/"; // 실서버용
     // private String realUploadPath = "/home/tmax_dev/mls/web/upload/"; // 개발서버용
     private String realUploadPath = FileUtil.uploadPath();

     public void setInmtPrpsService(InmtPrpsService inmtPrpsService) {
          this.inmtPrpsService = inmtPrpsService;
     }

     public void setUserService(UserService userService) {
          this.userService = userService;
     }

     public void setRghtPrpsService(RghtPrpsService rghtPrpsService) {
          this.rghtPrpsService = rghtPrpsService;
     }

     public void setRsltInqrService(RsltInqrService rsltInqrService) {
          this.rsltInqrService = rsltInqrService;
     }

     // 보상금신청 화면
     public ModelAndView list(HttpServletRequest request, HttpServletResponse respone) throws Exception {
          // System.out.println("##############################");
          // System.out.println("list");
          // System.out.println("##############################");
          // System.out.println("srchDIVS
          // ="+ServletRequestUtils.getStringParameter(request, "srchDIVS"));
          // System.out.println("srchSdsrName
          // ="+ServletRequestUtils.getStringParameter(request, "srchSdsrName"));
          // System.out.println("srchMuciName
          // ="+ServletRequestUtils.getStringParameter(request, "srchMuciName"));
          // System.out.println("srchYymm
          // ="+ServletRequestUtils.getStringParameter(request, "srchYymm"));
          // System.out.println("##############################");
         // System.out.println("/inmtPrps/inmtPrps.do list call");

          String gubun = ServletRequestUtils.getStringParameter(request, "gubun", ""); // 통합검색에서 넘어온경우 :
                                                                                       // totalSearch 기본
                                                                                       // 검색어 set 하지 않는다.

          // --현재 날자를 구해서 조회조건 생성 start
          Date date = new Date();
          SimpleDateFormat todayform = new SimpleDateFormat("yyyy");
          String thisYear = todayform.format(date);
          int startYear = Integer.parseInt(thisYear);
          int endYear = 2002;
          String srchDIVS = ServletRequestUtils.getStringParameter(request, "srchDIVS");

          ArrayList srchYear = new ArrayList();
          for (int a = 0; a <= startYear - endYear; a++) {
               srchYear.add(a, startYear - a);
          }
          // --현재 날자를 구해서 조회조건 생성 end

          // --해당 구분의 데이터 중 최고 년도를 조회 start
          InmtPrps dto = new InmtPrps();
          dto.setSrchDIVS(ServletRequestUtils.getStringParameter(request, "srchDIVS")); // 구분
          dto.setSrchSdsrName(ServletRequestUtils.getStringParameter(request, "srchSdsrName")); // 곡명
                                                                                                // /저작물명
                                                                                                // /저작물명
          dto.setSrchMuciName(ServletRequestUtils.getStringParameter(request, "srchMuciName")); // 가수명
                                                                                                // /저작자
                                                                                                // /저자
          dto.setSrchYymm(ServletRequestUtils.getStringParameter(request, "srchYymm")); // 방송년도 /출판년도
                                                                                        // /발행년도
          // String maxYear = inmtPrpsService.maxYear(dto);
          // --해당 구분의 데이터 중 최고 년도를 조회 end

          ModelAndView mv = new ModelAndView("inmtPrps/inmtList");
          if (ServletRequestUtils.getStringParameter(request, "srchYymm") == null && !gubun.equals("totalSearch")) {
               dto.setSrchYymm("");
          } else if (gubun.equals("totalSearch")) {
               dto.setSrchYymm("");
          } else {
               dto.setSrchYymm(ServletRequestUtils.getStringParameter(request, "srchYymm"));
          }
          // int maxAmnt = inmtPrpsService.muscMaxAmnt();
          // mv.addObject("maxAmnt",maxAmnt );

          mv.addObject("srchParam", dto);
          mv.addObject("thisYear", thisYear);
          mv.addObject("srchYear", srchYear);
          mv.addObject("srchDIVS", srchDIVS);

          mv.addObject("gubun", gubun); // 통합검색인 경우.
          

          return mv;
     }

     // 보상금신청 화면-iframe
     public ModelAndView muscInmtList(HttpServletRequest request, HttpServletResponse respone) throws Exception {
          // System.out.println("##############################");
          // System.out.println("muscInmtList");
          // System.out.println("##############################");
          // System.out.println("DIVS ="+ServletRequestUtils.getStringParameter(request,
          // "srchDIVS"));
          // System.out.println("srchSdsrName
          // ="+ServletRequestUtils.getStringParameter(request, "srchSdsrName"));
          // System.out.println("srchMuciName
          // ="+ServletRequestUtils.getStringParameter(request, "srchMuciName"));
          // System.out.println("srchYymm
          // ="+ServletRequestUtils.getStringParameter(request, "srchYymm"));
          // System.out.println("##############################");

          /* 20120220 검색조건 화면 수정 //정병호 */
          //System.out.println("/inmtPrps/inmtPrps.do srch call");
          // 공통 //음악,교과,도서관
          String srchYymm = ServletRequestUtils.getStringParameter(request, "srchYymm");
          String srchSdsrName = ServletRequestUtils.getStringParameter(request, "srchSdsrName");
          String srchMuciName = ServletRequestUtils.getStringParameter(request, "srchMuciName");
          String srchAlbmName = ServletRequestUtils.getStringParameter(request, "srchAlbmName");

          String trstOrgnCode = ServletRequestUtils.getStringParameter(request, "trstOrgnCode");
          String alltAmnt = ServletRequestUtils.getStringParameter(request, "alltAmnt");
          String latterAmnt = ServletRequestUtils.getStringParameter(request, "latterAmnt");
          String sort = ServletRequestUtils.getStringParameter(request, "sort");
          /*
           * String[] sortArr = request.getParameterValues( "sort" ); System.out.println( "sort : "+
           * request.getParameterValues( "sort" ) ); System.out.println( "sortArr : "+ sortArr.toString() );
           */
          //System.out.println("trstOrgnCode : " + trstOrgnCode);
          //System.out.println(" sort : " + sort);
          if (latterAmnt != null) {
               latterAmnt = latterAmnt.replaceAll(",", "");
          }
          if (alltAmnt != null) {
               alltAmnt = alltAmnt.replaceAll(",", "");
          }
          /* END //정병호 */

          InmtPrps dto = new InmtPrps();

          String srchDIVS = ServletRequestUtils.getStringParameter(request, "srchDIVS");
          // System.out.println( "latterAmnt : " + latterAmnt );
          dto.setSORT(sort);
          dto.setSrchDIVS(srchDIVS); // 구분
          dto.setTRST_ORGN_CODE(trstOrgnCode);
          dto.setALLT_AMNT(alltAmnt);
          dto.setLATTER_AMNT(latterAmnt);
          dto.setSrchSdsrName(ServletRequestUtils.getStringParameter(request, "srchSdsrName")); // 곡명
                                                                                                // /저작물명
                                                                                                // /저작물명
          dto.setSrchMuciName(ServletRequestUtils.getStringParameter(request, "srchMuciName")); // 가수명
                                                                                                // /저작자
                                                                                                // /저자
          dto.setSrchYymm(ServletRequestUtils.getStringParameter(request, "srchYymm")); // 방송년도 /출판년도
                                                                                        // /발행년도
          dto.setSrchAlbmName(ServletRequestUtils.getStringParameter(request, "srchAlbmName"));

          dto.setALLT_AMNT(ServletRequestUtils.getStringParameter(request, "alltAmnt"));

          dto.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
          dto.setLATTER_AMNT(ServletRequestUtils.getStringParameter(request, "latterAmnt"));

          int from = Constants.DEFAULT_ROW_PER_PAGE * (dto.getNowPage() - 1) + 1;
          int to = Constants.DEFAULT_ROW_PER_PAGE * dto.getNowPage();

          dto.setStartRow(from);
          dto.setEndRow(to);

          int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
          ModelAndView mv = null;

          /* 20120220 검색조건 화면 수정 //정병호 */
          if (srchDIVS.equals("1")) {
               /*
                * if(srchSdsrName.equals("") && srchMuciName.equals("") && srchAlbmName.equals("")){ mv = new
                * ModelAndView("inmtPrps/muscInmtList"); mv.addObject("emptyYn", "Y"); mv.addObject("srchParam",
                * dto); }else
                */
               {
                    mv = new ModelAndView("inmtPrps/muscInmtList", "inmtList", inmtPrpsService.inmtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, dto));
                    mv.addObject("srchParam", dto);
                    mv.addObject("trstOrgnCode", trstOrgnCode);
               }
          } else if (srchDIVS.equals("2")) {
               /*
                * if(srchYymm.equals("") && srchSdsrName.equals("") && srchMuciName.equals("")){ mv = new
                * ModelAndView("inmtPrps/subjInmtList"); mv.addObject("emptyYn", "Y"); mv.addObject("srchParam",
                * dto); }else
                */
               {
                    mv = new ModelAndView("inmtPrps/subjInmtList", "inmtList", inmtPrpsService.inmtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, dto));
                    mv.addObject("srchParam", dto);
               }
          } else if (srchDIVS.equals("3")) {
               /*
                * if(srchYymm.equals("") && srchSdsrName.equals("") && srchMuciName.equals("")){ mv = new
                * ModelAndView("inmtPrps/librInmtList"); mv.addObject("emptyYn", "Y"); mv.addObject("srchParam",
                * dto); }else
                */
               {
                    mv = new ModelAndView("inmtPrps/librInmtList", "inmtList", inmtPrpsService.inmtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, dto));
                    mv.addObject("srchParam", dto);
               }

          } else if (srchDIVS.equals("4")) {
               /*
                * if(srchYymm.equals("") && srchSdsrName.equals("") && srchMuciName.equals("")){ mv = new
                * ModelAndView("inmtPrps/lssnInmtList"); mv.addObject("emptyYn", "Y"); mv.addObject("srchParam",
                * dto); }else
                */
               {
                    mv = new ModelAndView("inmtPrps/lssnInmtList", "inmtList", inmtPrpsService.inmtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, dto));
                    mv.addObject("srchParam", dto);
               }

          }
          return mv;
     }

     // 보상금신청 입력 화면
     public ModelAndView inmtPrps(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return null;
     }
     /* 보상금 신청 입력화면 주석처리 (사용안함) */
     /*
      * public ModelAndView inmtPrps(HttpServletRequest request, HttpServletResponse respone) throws
      * Exception { // System.out.println("##############################"); //
      * System.out.println("inmtPrps"); // System.out.println("##############################"); //
      * System.out.println("srchDIVS		="+ServletRequestUtils.getStringParameter( request, "srchDIVS"));
      * // System.out.println("srchSdsrName	="+ServletRequestUtils.getStringParameter( request,
      * "srchSdsrName")); // System.out.println("srchMuciName	="+ServletRequestUtils.getStringParameter(
      * request, "srchMuciName")); //
      * System.out.println("srchYymm		="+ServletRequestUtils.getStringParameter( request, "srchYymm"));
      * // System.out.println("##############################"); String srchDIVS =
      * ServletRequestUtils.getStringParameter(request, "srchDIVS"); String gubun =
      * ServletRequestUtils.getStringParameter(request, "gubun"); // 동시신청관련 String prpsDoblCode =
      * ServletRequestUtils.getStringParameter(request, "prpsDoblCode", ""); String rghtPrpsMastKey =
      * ServletRequestUtils.getStringParameter(request, "rghtPrpsMastKey", ""); ModelAndView mv = null;
      * if(srchDIVS.equals("1")){ mv = new ModelAndView("inmtPrps/inmtPrps_musc"); }else
      * if(srchDIVS.equals("2")){ mv = new ModelAndView("inmtPrps/inmtPrps_subj"); }else
      * if(srchDIVS.equals("3")){ mv = new ModelAndView("inmtPrps/inmtPrps_libr"); } //조회조건 설정 InmtPrps
      * dto = new InmtPrps(); dto.setSrchDIVS(srchDIVS); //구분
      * dto.setSrchSdsrName(ServletRequestUtils.getStringParameter(request, "srchSdsrName")); //곡명
      * dto.setSrchMuciName(ServletRequestUtils.getStringParameter(request, "srchMuciName")); //가수명
      * dto.setSrchYymm(ServletRequestUtils.getStringParameter(request, "srchYymm")); //방송년도
      * dto.setNowPage(ServletRequestUtils.getIntParameter(request, "nowPage",1));
      * if(gubun.equals("edit")){ //보상금신청 목록에서 저작물을 선택해서 넘어왔을때 //화면에서 넘어온 값들(보상금신청 선택 목록) String[]
      * inmtSeqn= ServletRequestUtils.getStringParameters(request, "inmtSeqn"); String[] sdsrName=
      * ServletRequestUtils.getStringParameters(request, "sdsrName"); String[] muciName=
      * ServletRequestUtils.getStringParameters(request, "muciName"); String[] yymm=
      * ServletRequestUtils.getStringParameters(request, "yymm"); String[] divs=
      * ServletRequestUtils.getStringParameters(request, "divs"); String[] prpsDivs=
      * ServletRequestUtils.getStringParameters(request, "prpsDivs"); String[] kapp=
      * ServletRequestUtils.getStringParameters(request, "kapp"); String[] fokapo=
      * ServletRequestUtils.getStringParameters(request, "fokapo"); String[] krtra=
      * ServletRequestUtils.getStringParameters(request, "krtra"); String[] oferEtpr=
      * ServletRequestUtils.getStringParameters(request, "oferEtpr"); String[] brctCont=
      * ServletRequestUtils.getStringParameters(request, "brctCont"); String[] lyriWrtr=
      * ServletRequestUtils.getStringParameters(request, "lyriWrtr"); String[] comsWrtr=
      * ServletRequestUtils.getStringParameters(request, "comsWrtr"); String[] arrgWrtr=
      * ServletRequestUtils.getStringParameters(request, "arrgWrtr"); String[] albmName=
      * ServletRequestUtils.getStringParameters(request, "albmName"); String[] duesCode=
      * ServletRequestUtils.getStringParameters(request, "duesCode"); String[] dataType=
      * ServletRequestUtils.getStringParameters(request, "dataType"); String[] usexType=
      * ServletRequestUtils.getStringParameters(request, "usexType"); String[] selgYsno=
      * ServletRequestUtils.getStringParameters(request, "selgYsno"); String[] usexLibr=
      * ServletRequestUtils.getStringParameters(request, "usexLibr"); String[] ouptPage=
      * ServletRequestUtils.getStringParameters(request, "ouptPage"); String[] ctrlNumb=
      * ServletRequestUtils.getStringParameters(request, "ctrlNumb"); String[] lishComp=
      * ServletRequestUtils.getStringParameters(request, "lishComp"); String[] workCode=
      * ServletRequestUtils.getStringParameters(request, "workCode"); String[] caryDivs=
      * ServletRequestUtils.getStringParameters(request, "caryDivs"); String[] schl=
      * ServletRequestUtils.getStringParameters(request, "schl"); String[] bookDivs=
      * ServletRequestUtils.getStringParameters(request, "bookDivs"); String[] bookSizeDivs=
      * ServletRequestUtils.getStringParameters(request, "bookSizeDivs"); String[] schlYearDivs=
      * ServletRequestUtils.getStringParameters(request, "schlYearDivs"); String[] pubcCont=
      * ServletRequestUtils.getStringParameters(request, "pubcCont"); String[] autrDivs=
      * ServletRequestUtils.getStringParameters(request, "autrDivs"); String[] workKind=
      * ServletRequestUtils.getStringParameters(request, "workKind"); String[] usexPage=
      * ServletRequestUtils.getStringParameters(request, "usexPage"); String[] workDivs=
      * ServletRequestUtils.getStringParameters(request, "workDivs"); String[] subjName=
      * ServletRequestUtils.getStringParameters(request, "subjName"); //상단 리스트에서 바로 보상금신청을 했을시 선택한 항목들의 값
      * String ifrmInput = ServletRequestUtils.getStringParameter(request, "ifrmInput"); Map params = new
      * HashMap(); User user = SessionUtil.getSession(request); params.put("userIdnt",
      * user.getUserIdnt()); Map inmtUserInfo = userService.selectUserInfo(params); //사용자 조회 Map userInfo
      * = userService.getInmtUserInfo(params); //보상회원 조회 Map clientInfo = new HashMap(); //신청자 정보 //
      * 기존보상회원인경우. 가장최근 보상완료 신청자정보를 뿌린다. if(userInfo != null){ if(inmtUserInfo != null &&
      * inmtUserInfo.get("INMT_USER_YSNO").equals("Y")){
      * inmtUserInfo.put("TELX_NUMB",userInfo.get("TELX_NUMB"));
      * inmtUserInfo.put("BUSI_TELX_NUMB",userInfo.get("BUSI_TELX_NUMB"));
      * inmtUserInfo.put("MOBL_PHON",userInfo.get("MOBL_PHON"));
      * inmtUserInfo.put("FAXX_NUMB",userInfo.get("FAXX_NUMB"));
      * inmtUserInfo.put("MAIL",userInfo.get("MAIL"));
      * inmtUserInfo.put("HOME_ADDR",userInfo.get("HOME_ADDR"));
      * inmtUserInfo.put("BUSI_ADDR",userInfo.get("BUSI_ADDR"));
      * inmtUserInfo.put("BANK_NAME",userInfo.get("BANK_NAME"));
      * inmtUserInfo.put("ACCT_NUMB",userInfo.get("ACCT_NUMB"));
      * inmtUserInfo.put("DPTR",userInfo.get("DPTR")); } } //신청자정보 설정 // 동시신청인 경우 저작권찾기신청인정보로 연결한다.
      * if(prpsDoblCode.equals("1")) { // 저작권찾기 신청자정보 구한다. RghtPrps rghtPrps = new RghtPrps();
      * rghtPrps.setPRPS_MAST_KEY(rghtPrpsMastKey); rghtPrps = rsltInqrService.rghtPrpsDetl(rghtPrps); //
      * 저작권찾기 신청자정보 clientInfo.put("HOME_TELX_NUMB" ,rghtPrps.getHOME_TELX_NUMB());
      * clientInfo.put("BUSI_TELX_NUMB", strNvl(rghtPrps.getBUSI_TELX_NUMB(),
      * inmtUserInfo.get("BUSI_TELX_NUMB"))); clientInfo.put("MOBL_PHON", strNvl(rghtPrps.getMOBL_PHON(),
      * inmtUserInfo.get("MOBL_PHON"))); clientInfo.put("FAXX_NUMB", strNvl(rghtPrps.getFAXX_NUMB(),
      * inmtUserInfo.get("FAXX_NUMB"))); clientInfo.put("MAIL", strNvl(rghtPrps.getMAIL(),
      * inmtUserInfo.get("MAIL"))); clientInfo.put("HOME_ADDR", strNvl(rghtPrps.getHOME_ADDR(),
      * inmtUserInfo.get("HOME_ADDR"))); clientInfo.put("BUSI_ADDR", strNvl(rghtPrps.getBUSI_ADDR(),
      * inmtUserInfo.get("BUSI_ADDR"))); }else{ clientInfo.put("HOME_TELX_NUMB",
      * inmtUserInfo.get("TELX_NUMB")); clientInfo.put("BUSI_TELX_NUMB",
      * inmtUserInfo.get("BUSI_TELX_NUMB")); clientInfo.put("MOBL_PHON", inmtUserInfo.get("MOBL_PHON"));
      * clientInfo.put("MAIL", inmtUserInfo.get("MAIL")); clientInfo.put("FAXX_NUMB",
      * inmtUserInfo.get("FAXX_NUMB")); clientInfo.put("HOME_ADDR", inmtUserInfo.get("HOME_ADDR"));
      * clientInfo.put("BUSI_ADDR", inmtUserInfo.get("BUSI_ADDR")); } clientInfo.put("USER_IDNT",
      * inmtUserInfo.get("USER_IDNT")); clientInfo.put("PRPS_NAME", inmtUserInfo.get("USER_NAME"));
      * clientInfo.put("RESD_CORP_NUMB", inmtUserInfo.get("U_RESD_CORP_NUMB"));
      * clientInfo.put("RESD_CORP_NUMB_VIEW", inmtUserInfo.get("RESD_CORP_NUMB_VIEW"));
      * clientInfo.put("BANK_NAME", inmtUserInfo.get("BANK_NAME")); clientInfo.put("ACCT_NUMB",
      * inmtUserInfo.get("ACCT_NUMB")); clientInfo.put("DPTR", inmtUserInfo.get("DPTR"));
      * clientInfo.put("KAPP", "N"); clientInfo.put("FOKAPO", "N"); clientInfo.put("KRTRA", "N"); //분류체크
      * String trst_203 = "0"; String trst_202 = "0"; String trst_205 = "0"; String trst_205_2 = "0";
      * //보상금신청 선택 항목에 대한 정보를 selectList로 화면에 넘김 if(ifrmInput != null && ifrmInput.length() > 0){ // 체크박스
      * 값 배열생성 String keyId[] = (ifrmInput.replaceAll("%7C", "|")).split("&iChk="); InmtPrps inmtPrps =
      * new InmtPrps(); inmtPrps.setSrchDIVS(srchDIVS); inmtPrps.setKeyId(keyId); // 동시신청 관련
      * inmtPrps.setPRPS_DOBL_CODE(prpsDoblCode); // 동시신청 코드값 1:저작권 -> 보상금
      * inmtPrps.setRGHT_PRPS_MAST_KEY(rghtPrpsMastKey); // 저작권찾기 마스터 키값 List selectList =
      * inmtPrpsService.chkInmtList(inmtPrps); mv.addObject("selectList",selectList);
      * mv.addObject("selectListCnt",selectList.size()); for(int i = 0; i < selectList.size(); i++){
      * InmtPrps aDto = (InmtPrps) selectList.get(i); if(aDto.getKAPP().equals("Y")){ trst_203 = "1";
      * clientInfo.put("KAPP", "Y"); } if(aDto.getFOKAPO().equals("Y")){ trst_202 = "1";
      * clientInfo.put("FOKAPO", "Y"); } if(aDto.getKRTRA().equals("Y") &&
      * aDto.getPRPS_DIVS().equals("S")){ trst_205 = "1"; clientInfo.put("KRTRA", "Y"); }
      * if(aDto.getKRTRA().equals("Y") && aDto.getPRPS_DIVS().equals("L")){ trst_205_2 = "1";
      * clientInfo.put("KRTRA", "Y"); } } }else{ ArrayList selectList = new ArrayList(); for(int i = 0; i
      * < inmtSeqn.length; i++){ InmtPrps aDto = new InmtPrps();
      * aDto.setINMT_SEQN(StringUtil.nullToZero((inmtSeqn[i]))); aDto.setPRPS_IDNT_CODE("1"); // 디폴트 -
      * 기존정보 aDto.setSDSR_NAME(StringUtil.nullToEmpty(sdsrName[i]));
      * aDto.setMUCI_NAME(StringUtil.nullToEmpty(muciName[i]));
      * aDto.setYYMM(StringUtil.nullToEmpty(yymm[i])); aDto.setDIVS(StringUtil.nullToEmpty(divs[i]));
      * aDto.setPRPS_DIVS(StringUtil.nullToEmpty(prpsDivs[i]));
      * aDto.setKAPP(StringUtil.nullToEmpty(kapp[i])); aDto.setFOKAPO(StringUtil.nullToEmpty(fokapo[i]));
      * aDto.setKRTRA(StringUtil.nullToEmpty(krtra[i]));
      * aDto.setOFER_ETPR(StringUtil.nullToEmpty(oferEtpr[i]));
      * aDto.setBRCT_CONT(StringUtil.nullToEmpty(brctCont[i]));
      * aDto.setLYRI_WRTR(StringUtil.nullToEmpty(lyriWrtr[i]));
      * aDto.setCOMS_WRTR(StringUtil.nullToEmpty(comsWrtr[i]));
      * aDto.setARRG_WRTR(StringUtil.nullToEmpty(arrgWrtr[i]));
      * aDto.setALBM_NAME(StringUtil.nullToEmpty(albmName[i]));
      * aDto.setDUES_CODE(StringUtil.nullToEmpty(duesCode[i]));
      * aDto.setDATA_TYPE(StringUtil.nullToEmpty(dataType[i]));
      * aDto.setUSEX_TYPE(StringUtil.nullToEmpty(usexType[i]));
      * aDto.setSELG_YSNO(StringUtil.nullToEmpty(selgYsno[i]));
      * aDto.setUSEX_LIBR(StringUtil.nullToEmpty(usexLibr[i]));
      * aDto.setOUPT_PAGE(StringUtil.nullToEmpty(ouptPage[i]));
      * aDto.setCTRL_NUMB(StringUtil.nullToEmpty(ctrlNumb[i]));
      * aDto.setLISH_COMP(StringUtil.nullToEmpty(lishComp[i]));
      * aDto.setWORK_CODE(StringUtil.nullToEmpty(workCode[i]));
      * aDto.setCARY_DIVS(StringUtil.nullToEmpty(caryDivs[i]));
      * aDto.setSCHL(StringUtil.nullToEmpty(schl[i]));
      * aDto.setBOOK_DIVS(StringUtil.nullToEmpty(bookDivs[i]));
      * aDto.setBOOK_SIZE_DIVS(StringUtil.nullToEmpty(bookSizeDivs[i]));
      * aDto.setSCHL_YEAR_DIVS(StringUtil.nullToEmpty(schlYearDivs[i]));
      * aDto.setPUBC_CONT(StringUtil.nullToEmpty(pubcCont[i]));
      * aDto.setAUTR_DIVS(StringUtil.nullToEmpty(autrDivs[i]));
      * aDto.setWORK_KIND(StringUtil.nullToEmpty(workKind[i]));
      * aDto.setUSEX_PAGE(StringUtil.nullToEmpty(usexPage[i]));
      * aDto.setWORK_DIVS(StringUtil.nullToEmpty(workDivs[i]));
      * aDto.setSUBJ_NAME(StringUtil.nullToEmpty(subjName[i])); selectList.add(aDto); }
      * mv.addObject("selectList",selectList); mv.addObject("selectListCnt",selectList.size()); for(int i
      * = 0; i < inmtSeqn.length; i++){ if(kapp[i].equals("Y")){ trst_203 = "1"; clientInfo.put("KAPP",
      * "Y"); } if(fokapo[i].equals("Y")){ trst_202 = "1"; clientInfo.put("FOKAPO", "Y"); }
      * if(krtra[i].equals("Y") && prpsDivs[i].equals("S")){ trst_205 = "1"; clientInfo.put("KRTRA",
      * "Y"); } if(krtra[i].equals("Y") && prpsDivs[i].equals("L")){ trst_205_2 = "1";
      * clientInfo.put("KRTRA", "Y"); } } } clientInfo.put("TRST_203", trst_203);
      * clientInfo.put("TRST_202", trst_202); clientInfo.put("TRST_205", trst_205);
      * clientInfo.put("TRST_205_2", trst_205_2); mv.addObject("userInfo",userInfo);
      * mv.addObject("inmtUserInfo",inmtUserInfo); mv.addObject("clientInfo",clientInfo);
      * mv.addObject("firstYN", "Y");
      * mv.addObject("RESD_CORP_NUMB1",inmtUserInfo.get("RESD_CORP_NUMB1"));
      * mv.addObject("RESD_CORP_NUMB2", inmtUserInfo.get("RESD_CORP_NUMB2")); mv.addObject("PRPS_NAME",
      * inmtUserInfo.get("USER_NAME")); }else{ //확인 화면에서 뒤로 가기를 했을시 InmtPrps clientInfo = new InmtPrps();
      * // ArrayList rsltList = new ArrayList(); ArrayList selectList = new ArrayList(); ArrayList
      * fileList = new ArrayList(); ArrayList trstList = new ArrayList(); ArrayList kappList = new
      * ArrayList(); ArrayList fokapoList = new ArrayList(); ArrayList krtraList = new ArrayList();
      * //--기초 정보들 start //----동시신청 정보 start prpsDoblCode =
      * ServletRequestUtils.getStringParameter(request, "prpsDoblCode", ""); // 동시신청 구분코드 rghtPrpsMastKey
      * = ServletRequestUtils.getStringParameter(request, "rghtPrpsMastKey", ""); // 저작권찾기 마스터키
      * //----동시신청 정보 end //----선택 보상금 정보 start String[] prpsIdnt =
      * ServletRequestUtils.getStringParameters(request, "hddnSelectInmtSeqn"); // 선택저작물 seq값 String[]
      * prpsDivs = ServletRequestUtils.getStringParameters(request, "hddnSelectPrpsDivs"); // 신청구분값 B,S,L
      * String[] prpsIdntCode = ServletRequestUtils.getStringParameters(request,
      * "hddnSelectPrpsIdntCode"); String[] kapp = ServletRequestUtils.getStringParameters(request,
      * "hddnSelectKapp"); String[] fokapo = ServletRequestUtils.getStringParameters(request,
      * "hddnSelectFokapo"); String[] krtra = ServletRequestUtils.getStringParameters(request,
      * "hddnSelectKrtra"); String[] sdsrName = ServletRequestUtils.getStringParameters(request,
      * "hddnSelectSdsrName"); String[] muciName = ServletRequestUtils.getStringParameters(request,
      * "hddnSelectMuciName"); String[] yymm = ServletRequestUtils.getStringParameters(request,
      * "hddnSelectYymm"); String[] lishComp = ServletRequestUtils.getStringParameters(request,
      * "hddnSelectLishComp"); String[] usexType = ServletRequestUtils.getStringParameters(request,
      * "hddnSelectUsexType"); //----선택 보상금 정보 end //----신청자 정보 start String userIdnt =
      * ServletRequestUtils.getStringParameter(request, "hddnUserIdnt"); String prpsName =
      * ServletRequestUtils.getStringParameter(request, "txtPrpsName"); String prsdCorpNumbView =
      * ServletRequestUtils.getStringParameter(request, "txtPrsdCorpNumbView"); String prpsDesc =
      * ServletRequestUtils.getStringParameter(request, "txtareaPrpsDesc"); String homeTelxNumb =
      * ServletRequestUtils.getStringParameter(request, "txtHomeTelxNumb"); String busiTelxNumb =
      * ServletRequestUtils.getStringParameter(request, "txtBusiTelxNumb"); String moblPhon =
      * ServletRequestUtils.getStringParameter(request, "txtMoblPhon"); String faxxNumb =
      * ServletRequestUtils.getStringParameter(request, "txtFaxxNumb"); String mail =
      * ServletRequestUtils.getStringParameter(request, "txtMail"); String homeAddr =
      * ServletRequestUtils.getStringParameter(request, "txtHomeAddr"); String busiAddr =
      * ServletRequestUtils.getStringParameter(request, "txtBusiAddr");
      * System.out.println("###1111#################################"+busiAddr+
      * "###############################################"); String bankName =
      * ServletRequestUtils.getStringParameter(request, "txtBankName"); String acctNumb =
      * ServletRequestUtils.getStringParameter(request, "txtAcctNumb"); String dptr =
      * ServletRequestUtils.getStringParameter(request, "txtDptr");
      * System.out.println("###111111#################################"+dptr+
      * "###############################################"); String insertKapp =
      * StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnInsertKapp")); String
      * insertFokapo = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request,
      * "hddnInsertFokapo")); String insertKrtra =
      * StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnInsertKrtra"));
      * //----신청자 정보 end //----음제협에서 사용할 정보 start String[] kappDiskName =
      * ServletRequestUtils.getStringParameters(request, "txtKappDiskName"); String[] kappCdCode =
      * ServletRequestUtils.getStringParameters(request, "txtKappCdCode"); String[] kappSongName =
      * ServletRequestUtils.getStringParameters(request, "hddnKappSongName"); String[] kappPrpsIdnt =
      * ServletRequestUtils.getStringParameters(request, "hddnKappPrpsIdnt"); String[] kappPrpsIdntCode =
      * ServletRequestUtils.getStringParameters(request, "hddnKappPrpsIdntCode"); String[] kappSnerName =
      * ServletRequestUtils.getStringParameters(request, "txtKappSnerName"); String[] kappDateIssu =
      * ServletRequestUtils.getStringParameters(request, "txtKappDateIssu"); String[] kappNatuName =
      * ServletRequestUtils.getStringParameters(request, "txtKappNatuName"); String[] kappRghtGrnd =
      * ServletRequestUtils.getStringParameters(request, "txtKappRghtGrnd"); String[] kappLyriWrtr =
      * ServletRequestUtils.getStringParameters(request, "txtKappLyriWrtr"); String[] kappComsWrtr =
      * ServletRequestUtils.getStringParameters(request, "txtKappComsWrtr"); String[] kappMuscGnre =
      * ServletRequestUtils.getStringParameters(request, "selKappMuscGnre"); //----음제협에서 사용할 정보 end
      * //----음실연에서 사용할 정보 start String fokapoPemrRlnm = ServletRequestUtils.getStringParameter(request,
      * "txtFokapoPemrRlnm"); String fokapoPemrStnm = ServletRequestUtils.getStringParameter(request,
      * "txtFokapoPemrStnm"); String fokapoGrupName = ServletRequestUtils.getStringParameter(request,
      * "txtFokapoGrupName"); String fokapoSesdNumb = ServletRequestUtils.getStringParameter(request,
      * "txtFokapoResdNumb"); String fokapoSesdNumb_1 = ServletRequestUtils.getStringParameter(request,
      * "txtFokapoResdNumb_1"); String fokapoSesdNumb_2 = ServletRequestUtils.getStringParameter(request,
      * "txtFokapoResdNumb_2"); String[] fokapoPrpsIdnt =
      * ServletRequestUtils.getStringParameters(request, "hddnFokapoPrpsIdnt"); String[]
      * fokapoPrpsIdntCode = ServletRequestUtils.getStringParameters(request, "hddnFokapoPrpsIdntCode");
      * String[] fokapoPdtnName = ServletRequestUtils.getStringParameters(request, "hddnFokapoPdtnName");
      * String[] fokapoSnerName = ServletRequestUtils.getStringParameters(request, "txtFokapoSnerName");
      * String[] fokapoAbumName = ServletRequestUtils.getStringParameters(request, "txtFokapoAbumName");
      * String[] fokapoAbumDateIssu = ServletRequestUtils.getStringParameters(request,
      * "txtFokapoAbumDateIssu"); String[] fokapoPemsKind =
      * ServletRequestUtils.getStringParameters(request, "txtFokapoPemsKind"); String[] fokapoPemsDesc =
      * ServletRequestUtils.getStringParameters(request, "txtFokapoPemsDesc"); String[] fokapoCtbtRate =
      * ServletRequestUtils.getStringParameters(request, "txtFokapoCtbtRate"); //----음실연에서 사용할 정보 end
      * //----복전협에서 사용할 정보 start String[] krtraPrpsIdnt =
      * ServletRequestUtils.getStringParameters(request, "hddnKrtraPrpsIdnt"); String[] krtraPrpsIdntCode
      * = ServletRequestUtils.getStringParameters(request, "hddnKrtraPrpsIdntCode"); String[]
      * krtraPrpsDivs = ServletRequestUtils.getStringParameters(request, "hddnKrtraPrpsDivs"); String[]
      * krtraCoprKind = ServletRequestUtils.getStringParameters(request, "hddnKrtraCoprKind"); String[]
      * krtraWorkName = ServletRequestUtils.getStringParameters(request, "hddnKrtraWorkName"); String[]
      * krtraWrtrName = ServletRequestUtils.getStringParameters(request, "hddnKrtraWrtrName"); String[]
      * krtraScyr = ServletRequestUtils.getStringParameters(request, "hddnKrtraScyr"); String[] krtraSctr
      * = ServletRequestUtils.getStringParameters(request, "hddnKrtraSctr"); String[] krtraSjet =
      * ServletRequestUtils.getStringParameters(request, "hddnKrtraSjet"); String[] krtraBookCncn =
      * ServletRequestUtils.getStringParameters(request, "hddnKrtraBookCncn"); String[] krtraWorkDivs =
      * ServletRequestUtils.getStringParameters(request, "hddnKrtraWorkDivs"); String[] krtraUsexType =
      * ServletRequestUtils.getStringParameters(request, "hddnKrtraUsexType"); String krtraRtpsRlnm =
      * ServletRequestUtils.getStringParameter(request, "txtKrtraPemrRlnm"); String krtraRtpsStnm =
      * ServletRequestUtils.getStringParameter(request, "txtKrtraPemrStnm"); String krtraRtpsCrnm =
      * ServletRequestUtils.getStringParameter(request, "txtKrtraGrupName"); String krtraResnNumb =
      * ServletRequestUtils.getStringParameter(request, "txtKrtraResdNumb"); //---- 주민번호 방화벽 테스트 start
      * String krtraResnNumb_1 = ServletRequestUtils.getStringParameter(request,
      * "txtKrtraResdNumb_1",""); String krtraResnNumb_2 =
      * ServletRequestUtils.getStringParameter(request, "txtKrtraResdNumb_2",""); //
      * mv.addObject("resdNumbStr_1", krtraResnNumb_1); // mv.addObject("resdNumbStr_2",
      * krtraResnNumb_2); mv.addObject("RESD_CORP_NUMB1", krtraResnNumb_1);
      * mv.addObject("RESD_CORP_NUMB2", krtraResnNumb_2); //---- 주민번호 방화벽 테스트 end //----복전협에서 사용할 정보 end
      * //----첨부파일 관련정보 start // int fileCnt = ServletRequestUtils.getIntParameter(request,
      * "hddnFileCnt", 0); // String[] fileOrgnCode = ServletRequestUtils.getStringParameters(request,
      * "hddnFileOrgnCode"); // String[] fileName = ServletRequestUtils.getStringParameters(request,
      * "hddnFileName"); String[] getFileOrgnCode= ServletRequestUtils.getStringParameters(request,
      * "hddnGetFileOrgnCode"); String[] getRealFileName=
      * ServletRequestUtils.getStringParameters(request, "hddnGetRealFileName"); String[] getFilePath =
      * ServletRequestUtils.getStringParameters(request, "hddnGetFilePath"); String[] getFileName =
      * ServletRequestUtils.getStringParameters(request, "hddnGetFileName"); String[] getFileSize =
      * ServletRequestUtils.getStringParameters(request, "hddnGetFileSize"); //----첨부파일 관련정보 end
      * //----등록신청이 오프라인이나 첨부파일인지 확인 start String trst203 =
      * ServletRequestUtils.getStringParameter(request, "hddnTrst203"); String trst202 =
      * ServletRequestUtils.getStringParameter(request, "hddnTrst202"); String trst205 =
      * ServletRequestUtils.getStringParameter(request, "hddnTrst205"); String trst205_2 =
      * ServletRequestUtils.getStringParameter(request, "hddnTrst205_2"); String chkKappOff =
      * StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnKappOff")); //offline
      * 접수여부 String chkFokapoOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request,
      * "hddnFokapoOff")); //offline 접수여부 String chkKrtraOff =
      * StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnKrtraOff"));
      * //offline 접수여부 //----등록신청이 오프라인이나 첨부파일인지 확인 end //--기초 정보들 end for(int f = 0; f <
      * getFileOrgnCode.length; f++){ PrpsAttc prpsAttc = new PrpsAttc();
      * prpsAttc.setREAL_FILE_NAME(getRealFileName[f]); prpsAttc.setFILE_PATH(getFilePath[f]);
      * prpsAttc.setFILE_NAME(getFileName[f]); prpsAttc.setFILE_SIZE(getFileSize[f]);
      * prpsAttc.setTRST_ORGN_CODE(getFileOrgnCode[f]); fileList.add(prpsAttc); }
      * clientInfo.setUSER_IDNT(userIdnt); clientInfo.setPRPS_NAME(prpsName);
      * clientInfo.setRESD_CORP_NUMB(prsdCorpNumbView);
      * clientInfo.setRESD_CORP_NUMB_VIEW(prsdCorpNumbView); clientInfo.setHOME_TELX_NUMB(homeTelxNumb);
      * clientInfo.setBUSI_TELX_NUMB(busiTelxNumb); clientInfo.setMOBL_PHON(moblPhon);
      * clientInfo.setFAXX_NUMB(faxxNumb); clientInfo.setHOME_ADDR(homeAddr);
      * clientInfo.setBUSI_ADDR(busiAddr); clientInfo.setBANK_NAME(bankName);
      * clientInfo.setACCT_NUMB(acctNumb); clientInfo.setMAIL(mail); clientInfo.setDPTR(dptr);
      * clientInfo.setTRST_203(trst203); clientInfo.setTRST_202(trst202);
      * clientInfo.setTRST_205(trst205); clientInfo.setTRST_205_2(trst205_2);
      * clientInfo.setPRPS_DESC(prpsDesc); clientInfo.setKAPP(insertKapp);
      * clientInfo.setFOKAPO(insertFokapo); clientInfo.setKRTRA(insertKrtra); int arrCnt = 0; if(trst203
      * != null && trst203.equals("1")){ InmtPrps trstDto = new InmtPrps();
      * trstDto.setTRST_ORGN_CODE("203"); trstDto.setOFFX_LINE_RECP(chkKappOff); trstList.add(trstDto);
      * arrCnt++; } if(trst202 != null && trst202.equals("1")){ InmtPrps trstDto = new InmtPrps();
      * trstDto.setTRST_ORGN_CODE("202"); trstDto.setOFFX_LINE_RECP(chkFokapoOff); trstList.add(trstDto);
      * arrCnt++; mv.addObject("pemrStnm", fokapoPemrStnm); mv.addObject("grupName", fokapoGrupName);
      * mv.addObject("resdNumb", fokapoSesdNumb); // mv.addObject("pemrRlnm", fokapoPemrRlnm); //
      * mv.addObject("resdNumbStr_1", fokapoSesdNumb_1); // mv.addObject("resdNumbStr_2",
      * fokapoSesdNumb_2); mv.addObject("PRPS_NAME", fokapoPemrRlnm); mv.addObject("RESD_CORP_NUMB1",
      * fokapoSesdNumb_1); mv.addObject("RESD_CORP_NUMB2", fokapoSesdNumb_2); mv.addObject("resdNumbStr",
      * fokapoSesdNumb); } if(trst205 != null && trst205.equals("1")){ InmtPrps trstDto = new InmtPrps();
      * trstDto.setTRST_ORGN_CODE("205"); trstDto.setOFFX_LINE_RECP(chkKrtraOff); trstList.add(trstDto);
      * arrCnt++; // mv.addObject("pemrRlnm", krtraRtpsRlnm); mv.addObject("PRPS_NAME", krtraRtpsRlnm);
      * mv.addObject("pemrStnm", krtraRtpsStnm); mv.addObject("grupName", krtraRtpsCrnm);
      * mv.addObject("resdNumb", krtraResnNumb); mv.addObject("resdNumbStr", krtraResnNumb); }
      * if(trst205_2 != null && trst205_2.equals("1")){ InmtPrps trstDto = new InmtPrps();
      * trstDto.setTRST_ORGN_CODE("205_2"); trstDto.setOFFX_LINE_RECP(chkKrtraOff);
      * trstList.add(trstDto); arrCnt++; // mv.addObject("pemrRlnm", krtraRtpsRlnm);
      * mv.addObject("PRPS_NAME", krtraRtpsRlnm); mv.addObject("pemrStnm", krtraRtpsStnm);
      * mv.addObject("grupName", krtraRtpsCrnm); mv.addObject("resdNumb", krtraResnNumb);
      * mv.addObject("resdNumbStr", krtraResnNumb); } for(int ai = 0; ai < prpsIdnt.length; ai++){
      * InmtPrps selectDto = new InmtPrps(); selectDto.setINMT_SEQN(Long.parseLong(prpsIdnt[ai]));
      * selectDto.setPRPS_DIVS(prpsDivs[ai]); selectDto.setPRPS_IDNT_CODE(prpsIdntCode[ai]);
      * selectDto.setKAPP(kapp[ai]); selectDto.setFOKAPO(fokapo[ai]); selectDto.setKRTRA(krtra[ai]);
      * selectDto.setSDSR_NAME(sdsrName[ai]); selectDto.setMUCI_NAME(muciName[ai]);
      * selectDto.setYYMM(yymm[ai]); selectDto.setLISH_COMP(lishComp[ai]);
      * selectDto.setUSEX_TYPE(usexType[ai]); selectList.add(selectDto); } if(insertKapp.equals("Y")){
      * for(int km = 0; km < kappPrpsIdnt.length; km++){ InmtPrps kappDto = new InmtPrps();
      * kappDto.setPRPS_IDNT(kappPrpsIdnt[km]); kappDto.setPRPS_IDNT_CODE(kappPrpsIdntCode[km]); //
      * 신청대상구분 kappDto.setDISK_NAME(kappDiskName[km]); //음반명 kappDto.setCD_CODE(kappCdCode[km]); //CD코드
      * kappDto.setSONG_NAME(kappSongName[km]); //곡명 kappDto.setSNER_NAME(kappSnerName[km]); //가수명
      * kappDto.setDATE_ISSU(kappDateIssu[km]); //발매일 kappDto.setNATN_NAME(kappNatuName[km]); //국가명
      * kappDto.setRGHT_GRND(kappRghtGrnd[km]); //권리근거 kappDto.setLYRI_WRTR(kappLyriWrtr[km]); //작사가
      * kappDto.setCOMS_WRTR(kappComsWrtr[km]); //작곡가 kappDto.setMUSC_GNRE(kappMuscGnre[km]);
      * //음악장르<BR>(테마코드) kappList.add(kappDto); } } if(insertFokapo.equals("Y")){ for(int km = 0; km <
      * fokapoPrpsIdnt.length; km++){ InmtPrps fokapoDto = new InmtPrps();
      * fokapoDto.setPRPS_IDNT(fokapoPrpsIdnt[km]); fokapoDto.setPRPS_IDNT_CODE(fokapoPrpsIdntCode[km]);
      * // 신청대상구분 fokapoDto.setPDTN_NAME(fokapoPdtnName[km]); //작품명
      * fokapoDto.setSNER_NAME(fokapoSnerName[km]); //가수명 fokapoDto.setABUM_NAME(fokapoAbumName[km]);
      * //앨범명 fokapoDto.setABUM_DATE_ISSU(fokapoAbumDateIssu[km]); //앨범발매일
      * fokapoDto.setPEMS_KIND(fokapoPemsKind[km]); //실연종류 fokapoDto.setPEMS_DESC(fokapoPemsDesc[km]);
      * //실연내용 fokapoDto.setCTBT_RATE(fokapoCtbtRate[km]); //기여율(%) fokapoList.add(fokapoDto); } }
      * if(insertKrtra.equals("Y")){ for(int km = 0; km < krtraPrpsIdnt.length; km++){ InmtPrps krtraDto
      * = new InmtPrps(); krtraDto.setINMT_SEQN(Long.parseLong(krtraPrpsIdnt[km]));
      * krtraDto.setPRPS_DIVS(krtraPrpsDivs[km]); krtraDto.setPRPS_IDNT_CODE(krtraPrpsIdntCode[km]);
      * krtraDto.setWORK_NAME(krtraWorkName[km]); krtraDto.setWRTR_NAME(krtraWrtrName[km]);
      * krtraDto.setBOOK_CNCN(krtraBookCncn[km]); krtraDto.setSCTR(krtraSctr[km]);
      * if(krtraPrpsDivs[km].equals("L")){ krtraDto.setUSEX_TYPE(krtraUsexType[km]); }
      * if(krtraPrpsDivs[km].equals("S")){ krtraDto.setCOPT_KIND(krtraCoprKind[km]);
      * krtraDto.setSCYR(krtraScyr[km]); krtraDto.setSJET(krtraSjet[km]);
      * krtraDto.setWORK_DIVS(krtraWorkDivs[km]); } krtraList.add(krtraDto); } }
      * mv.addObject("clientInfo", clientInfo); mv.addObject("selectList", selectList);
      * mv.addObject("selectListCnt", selectList.size()); mv.addObject("fileList", fileList);
      * mv.addObject("trstList", trstList); mv.addObject("kappList", kappList);
      * mv.addObject("fokapoList", fokapoList); mv.addObject("krtraList", krtraList); }
      * mv.addObject("prpsDoblCode", prpsDoblCode); mv.addObject("rghtPrpsMastKey", rghtPrpsMastKey);
      * mv.addObject("srchParam", dto); mv.addObject("srchDIVS", srchDIVS); return mv; }
      */

     // 보상금신청 확인
     public ModelAndView inmtPrpsChk(HttpServletRequest request, HttpServletResponse response) throws Exception {
          // System.out.println("##############################");
          // System.out.println("inmtPrpsChk");
          // System.out.println("##############################");

          CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
          MultipartHttpServletRequest multipartRequest;
          multipartRequest = multipartResolver.resolveMultipart(request);

          InmtPrps clientInfo = new InmtPrps();
          // ArrayList rsltList = new ArrayList();
          ArrayList tempList = new ArrayList();
          ArrayList fileList = new ArrayList();
          ArrayList trstList = new ArrayList();

          ArrayList kappList = new ArrayList();
          ArrayList fokapoList = new ArrayList();
          ArrayList krtraList = new ArrayList();
          ArrayList krtraList_2 = new ArrayList();

          String srchDIVS = ServletRequestUtils.getStringParameter(multipartRequest, "srchDIVS");

          ModelAndView mv = null;
          if (srchDIVS.equals("1")) {
               mv = new ModelAndView("inmtPrps/inmtPrpsChk_musc");
          } else if (srchDIVS.equals("2")) {
               mv = new ModelAndView("inmtPrps/inmtPrpsChk_subj");
          } else if (srchDIVS.equals("3")) {
               mv = new ModelAndView("inmtPrps/inmtPrpsChk_libr");
          }
          // 선택저장물 저장-------------------

          // --기초 정보들 start

          // ----동시신청 정보 start
          String prpsDoblCode = ServletRequestUtils.getStringParameter(multipartRequest, "prpsDoblCode", ""); // 동시신청
                                                                                                              // 구분코드
          String rghtPrpsMastKey = ServletRequestUtils.getStringParameter(multipartRequest, "rghtPrpsMastKey", ""); // 저작권찾기
                                                                                                                    // 마스터키
          // ----동시신청 정보 end

          // ----선택 보상금 정보 start
          String[] prpsIdnt = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectInmtSeqn"); // 선택저작물
                                                                                                               // seq값
          String[] prpsDivs = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectPrpsDivs"); // 신청구분값
                                                                                                               // B,S,L
          String[] prpsIdntCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectPrpsIdntCode");
          String[] kapp = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectKapp");
          String[] fokapo = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectFokapo");
          String[] krtra = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectKrtra");

          String[] sdsrName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectSdsrName");
          String[] muciName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectMuciName");
          String[] yymm = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectYymm");
          String[] lishComp = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectLishComp");
          String[] usexType = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectUsexType");
          // ----선택 보상금 정보 end

          // ----신청자 정보 start
          String userIdnt = ServletRequestUtils.getStringParameter(multipartRequest, "hddnUserIdnt");
          String prpsName = ServletRequestUtils.getStringParameter(multipartRequest, "txtPrpsName");
          String prsdCorpNumbView = ServletRequestUtils.getStringParameter(multipartRequest, "txtPrsdCorpNumbView");
          String prpsDesc = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtareaPrpsDesc"));
          String homeTelxNumb = ServletRequestUtils.getStringParameter(multipartRequest, "txtHomeTelxNumb");
          String busiTelxNumb = ServletRequestUtils.getStringParameter(multipartRequest, "txtBusiTelxNumb");
          String moblPhon = ServletRequestUtils.getStringParameter(multipartRequest, "txtMoblPhon");
          String faxxNumb = ServletRequestUtils.getStringParameter(multipartRequest, "txtFaxxNumb");
          String mail = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtMail"));
          String homeAddr = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtHomeAddr"));
          String busiAddr = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtBusiAddr"));
          System.out.println("###2222#################################" + busiAddr + "###############################################");

          String bankName = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtBankName"));
          String acctNumb = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtAcctNumb"));
          String dptr = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtDptr"));
          System.out.println("###222222#################################" + dptr + "###############################################");
          String insertKapp = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnInsertKapp"));
          String insertFokapo = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnInsertFokapo"));
          String insertKrtra = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnInsertKrtra"));
          // ----신청자 정보 end

          // ----음제협에서 사용할 정보 start
          String[] kappDiskName = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappDiskName");
          String[] kappCdCode = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappCdCode");
          String[] kappSongName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKappSongName");
          String[] kappPrpsIdnt = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKappPrpsIdnt");
          String[] kappPrpsIdntCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKappPrpsIdntCode");
          String[] kappSnerName = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappSnerName");
          String[] kappDateIssu = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappDateIssu");
          String[] kappNatuName = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappNatuName");
          String[] kappRghtGrnd = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappRghtGrnd");
          String[] kappLyriWrtr = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappLyriWrtr");
          String[] kappComsWrtr = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappComsWrtr");
          String[] kappMuscGnre = ServletRequestUtils.getStringParameters(multipartRequest, "selKappMuscGnre");
          // ----음제협에서 사용할 정보 end

          // ----음실연에서 사용할 정보 start
          String fokapoPemrRlnm = ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoPemrRlnm");
          String fokapoPemrStnm = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoPemrStnm"));
          String fokapoGrupName = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoGrupName"));
          String fokapoSesdNumb = ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoResdNumb");
          String fokapoSesdNumb_1 = ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoResdNumb_1");
          String fokapoSesdNumb_2 = ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoResdNumb_2");
          String[] fokapoPrpsIdnt = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFokapoPrpsIdnt");
          String[] fokapoPrpsIdntCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFokapoPrpsIdntCode");
          String[] fokapoPdtnName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFokapoPdtnName");
          String[] fokapoSnerName = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoSnerName");
          String[] fokapoAbumName = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoAbumName");
          String[] fokapoAbumDateIssu = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoAbumDateIssu");
          String[] fokapoPemsKind = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoPemsKind");
          String[] fokapoPemsDesc = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoPemsDesc");
          String[] fokapoCtbtRate = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoCtbtRate");
          // ----음실연에서 사용할 정보 end

          // ----복전협에서 사용할 정보 start
          String[] krtraPrpsIdnt = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraPrpsIdnt");
          String[] krtraPrpsDivs = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraPrpsDivs");
          String[] krtraPrpsIdntCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraPrpsIdntCode");

          String[] krtraCoprKind = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraCoprKind");
          String[] krtraWorkName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraWorkName");
          String[] krtraWrtrName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraWrtrName");
          String[] krtraScyr = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraScyr");
          String[] krtraSctr = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraSctr");
          String[] krtraSjet = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraSjet");
          String[] krtraBookCncn = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraBookCncn");
          String[] krtraWorkDivs = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraWorkDivs");
          String[] krtraUsexType = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraUsexType");

          String krtraRtpsRlnm = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraPemrRlnm"));
          String krtraRtpsStnm = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraPemrStnm"));
          String krtraRtpsCrnm = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraGrupName"));
          String krtraResnNumb = ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraResdNumb");

          // ---- 주민번호 방화벽 테스트 start
          String krtraResnNumb_1 = ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraResdNumb_1", "");
          String krtraResnNumb_2 = ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraResdNumb_2", "");
          // ---- 주민번호 방화벽 테스트 end

          // ----복전협에서 사용할 정보 end

          // ----첨부파일 관련정보 start
          // int fileCnt = ServletRequestUtils.getIntParameter(multipartRequest,
          // "hddnFileCnt", 0);
          String[] fileOrgnCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFileOrgnCode");
          String[] fileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFileName");

          String[] getFileOrgnCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileOrgnCode");
          String[] getRealFileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetRealFileName");
          String[] getFilePath = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFilePath");
          String[] getFileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileName");
          String[] getFileSize = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileSize");
          // ----첨부파일 관련정보 end

          // ----등록신청이 오프라인이나 첨부파일인지 확인 start
          String trst203 = ServletRequestUtils.getStringParameter(multipartRequest, "hddnTrst203");
          String trst202 = ServletRequestUtils.getStringParameter(multipartRequest, "hddnTrst202");
          String trst205 = ServletRequestUtils.getStringParameter(multipartRequest, "hddnTrst205");
          String trst205_2 = ServletRequestUtils.getStringParameter(multipartRequest, "hddnTrst205_2");

          String chkKappOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnKappOff")); // offline
                                                                                                                               // 접수여부
          String chkFokapoOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnFokapoOff")); // offline
                                                                                                                                   // 접수여부
          String chkKrtraOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnKrtraOff")); // offline
                                                                                                                                 // 접수여부
          // ----등록신청이 오프라인이나 첨부파일인지 확인 end
          // --기초 정보들 end

          // ----첨부Upload start
          // File Upload
          for (int f = 0; f < getFileOrgnCode.length; f++) {
               PrpsAttc prpsAttc = new PrpsAttc();

               prpsAttc.setREAL_FILE_NAME(getRealFileName[f]);
               prpsAttc.setFILE_PATH(getFilePath[f]);
               prpsAttc.setFILE_NAME(getFileName[f]);
               prpsAttc.setFILE_SIZE(getFileSize[f]);

               prpsAttc.setTRST_ORGN_CODE(getFileOrgnCode[f]);

               fileList.add(prpsAttc);
          }

          Iterator fileNameIterator = multipartRequest.getFileNames();
          while (fileNameIterator.hasNext()) {
               MultipartFile multiFile = multipartRequest.getFile((String) fileNameIterator.next());
               if (multiFile.getSize() > 0) {
                    PrpsAttc prpsAttc = FileUploadUtil.uploadPrpsFiles(multiFile, realUploadPath);

                    for (int fm = 0; fm < fileName.length; fm++) {
                         if (fileName[fm].toUpperCase().equals(prpsAttc.getFILE_NAME().toUpperCase())) {
                              // if(fileName[fm].equals(prpsAttc.getFILE_NAME())){
                              prpsAttc.setFILE_NAME(fileName[fm]);
                              prpsAttc.setTRST_ORGN_CODE(fileOrgnCode[fm]);
                         }
                    }
                    fileList.add(prpsAttc);
               }
          }

          int i_bytelen = 0;
          // for(int azzzzz = 0; azzzzz < fileList.size(); azzzzz++){
          // PrpsAttc fi = (PrpsAttc) fileList.get(azzzzz);
          // System.out.println("################# " + azzzzz);
          // System.out.println(fi.getREAL_FILE_NAME());
          // System.out.println(fi.getFILE_PATH());
          // System.out.println(fi.getFILE_NAME());
          // System.out.println(fi.getFILE_SIZE());
          // System.out.println(fi.getTRST_ORGN_CODE());
          // }
          // ----첨부Upload end

          InmtPrps dto = new InmtPrps();
          dto.setSrchDIVS(ServletRequestUtils.getStringParameter(multipartRequest, "srchDIVS")); // 구분
          dto.setSrchSdsrName(ServletRequestUtils.getStringParameter(multipartRequest, "srchSdsrName")); // 곡명
                                                                                                         // /저작물명
                                                                                                         // /저작물명
          dto.setSrchMuciName(ServletRequestUtils.getStringParameter(multipartRequest, "srchMuciName")); // 가수명
                                                                                                         // /저작자
                                                                                                         // /저자
          dto.setSrchYymm(ServletRequestUtils.getStringParameter(multipartRequest, "srchYymm")); // 방송년도
                                                                                                 // /출판년도
                                                                                                 // /발행년도

          clientInfo.setUSER_IDNT(userIdnt);
          clientInfo.setPRPS_NAME(prpsName);
          clientInfo.setRESD_CORP_NUMB(prsdCorpNumbView);
          clientInfo.setRESD_CORP_NUMB_VIEW(prsdCorpNumbView);
          clientInfo.setHOME_TELX_NUMB(homeTelxNumb);
          clientInfo.setBUSI_TELX_NUMB(busiTelxNumb);
          clientInfo.setMOBL_PHON(moblPhon);
          clientInfo.setFAXX_NUMB(faxxNumb);
          clientInfo.setHOME_ADDR(homeAddr);
          clientInfo.setBUSI_ADDR(busiAddr);
          clientInfo.setBANK_NAME(bankName);
          clientInfo.setACCT_NUMB(acctNumb);
          clientInfo.setMAIL(mail);
          clientInfo.setDPTR(dptr);
          clientInfo.setTRST_203(trst203);
          clientInfo.setTRST_202(trst202);
          clientInfo.setTRST_205(trst205);
          clientInfo.setTRST_205_2(trst205_2);
          clientInfo.setPRPS_DESC(prpsDesc);
          clientInfo.setKAPP(insertKapp);
          clientInfo.setFOKAPO(insertFokapo);
          clientInfo.setKRTRA(insertKrtra);

          int arrCnt = 0;

          if (trst203 != null && trst203.equals("1")) {
               InmtPrps trstDto = new InmtPrps();

               trstDto.setTRST_ORGN_CODE("203");
               trstDto.setOFFX_LINE_RECP(chkKappOff);

               trstList.add(trstDto);
               arrCnt++;

          }

          if (trst202 != null && trst202.equals("1")) {
               InmtPrps trstDto = new InmtPrps();

               trstDto.setTRST_ORGN_CODE("202");
               trstDto.setOFFX_LINE_RECP(chkFokapoOff);

               trstList.add(trstDto);
               arrCnt++;

               mv.addObject("pemrRlnm", fokapoPemrRlnm);
               mv.addObject("pemrStnm", fokapoPemrStnm);
               mv.addObject("grupName", fokapoGrupName);
               mv.addObject("resdNumb", fokapoSesdNumb);
               mv.addObject("resdNumbStr_1", fokapoSesdNumb_1);
               mv.addObject("resdNumbStr_2", fokapoSesdNumb_2);
               mv.addObject("resdNumbStr", fokapoSesdNumb);

          }

          if (trst205 != null && trst205.equals("1")) {
               InmtPrps trstDto = new InmtPrps();

               trstDto.setTRST_ORGN_CODE("205");
               trstDto.setOFFX_LINE_RECP(chkKrtraOff);

               trstList.add(trstDto);
               arrCnt++;

               mv.addObject("pemrRlnm", krtraRtpsRlnm);
               mv.addObject("pemrStnm", krtraRtpsStnm);
               mv.addObject("grupName", krtraRtpsCrnm);
               mv.addObject("resdNumb", krtraResnNumb);
               mv.addObject("resdNumbStr", krtraResnNumb);
               mv.addObject("resdNumbStr_1", krtraResnNumb_1);
               mv.addObject("resdNumbStr_2", krtraResnNumb_2);

          }

          if (trst205_2 != null && trst205_2.equals("1")) {
               InmtPrps trstDto = new InmtPrps();

               trstDto.setTRST_ORGN_CODE("205_2");
               trstDto.setOFFX_LINE_RECP(chkKrtraOff);

               trstList.add(trstDto);
               arrCnt++;

               mv.addObject("pemrRlnm", krtraRtpsRlnm);
               mv.addObject("pemrStnm", krtraRtpsStnm);
               mv.addObject("grupName", krtraRtpsCrnm);
               mv.addObject("resdNumb", krtraResnNumb);
               mv.addObject("resdNumbStr", krtraResnNumb);
               mv.addObject("resdNumbStr_1", krtraResnNumb_1);
               mv.addObject("resdNumbStr_2", krtraResnNumb_2);
          }

          for (int ai = 0; ai < prpsIdnt.length; ai++) {
               InmtPrps tempDto = new InmtPrps();

               tempDto.setINMT_SEQN(Long.parseLong(prpsIdnt[ai]));
               tempDto.setPRPS_DIVS(prpsDivs[ai]);
               tempDto.setPRPS_IDNT_CODE(prpsIdntCode[ai]);
               tempDto.setKAPP(kapp[ai]);
               tempDto.setFOKAPO(fokapo[ai]);
               tempDto.setKRTRA(krtra[ai]);
               tempDto.setSDSR_NAME(sdsrName[ai]);
               tempDto.setMUCI_NAME(muciName[ai]);
               tempDto.setYYMM(yymm[ai]);
               tempDto.setLISH_COMP(lishComp[ai]);
               tempDto.setUSEX_TYPE(usexType[ai]);

               tempList.add(tempDto);
          }

          if (insertKapp.equals("Y") && trst203.equals("1")) {
               for (int km = 0; km < kappPrpsIdnt.length; km++) {
                    if (kappPrpsIdnt[km] != null && kapp[km].equals("Y")) {
                         InmtPrps kappDto = new InmtPrps();

                         kappDto.setPRPS_IDNT(kappPrpsIdnt[km]);
                         kappDto.setPRPS_IDNT_CODE(kappPrpsIdntCode[km]);
                         kappDto.setDISK_NAME(XssUtil.unscript(kappDiskName[km])); // 음반명
                         kappDto.setCD_CODE(XssUtil.unscript(kappCdCode[km])); // CD코드
                         kappDto.setSONG_NAME(kappSongName[km]); // 곡명
                         kappDto.setSNER_NAME(XssUtil.unscript(kappSnerName[km])); // 가수명
                         kappDto.setDATE_ISSU(kappDateIssu[km]); // 발매일
                         kappDto.setNATN_NAME(XssUtil.unscript(kappNatuName[km])); // 국가명
                         kappDto.setRGHT_GRND(XssUtil.unscript(kappRghtGrnd[km])); // 권리근거
                         kappDto.setLYRI_WRTR(XssUtil.unscript(kappLyriWrtr[km])); // 작사가
                         kappDto.setCOMS_WRTR(XssUtil.unscript(kappComsWrtr[km])); // 작곡가
                         kappDto.setMUSC_GNRE(kappMuscGnre[km]); // 음악장르<BR>(테마코드)

                         kappList.add(kappDto);
                    }
               }
          }

          if (insertFokapo.equals("Y") && trst202.equals("1")) {
               for (int km = 0; km < fokapoPrpsIdnt.length; km++) {
                    if (fokapoPrpsIdnt[km] != null && fokapo[km].equals("Y")) {
                         InmtPrps fokapoDto = new InmtPrps();

                         fokapoDto.setPRPS_IDNT(fokapoPrpsIdnt[km]);
                         fokapoDto.setPRPS_IDNT_CODE(fokapoPrpsIdntCode[km]);
                         fokapoDto.setPDTN_NAME(fokapoPdtnName[km]); // 작품명
                         fokapoDto.setSNER_NAME(XssUtil.unscript(fokapoSnerName[km])); // 가수명
                         fokapoDto.setABUM_NAME(XssUtil.unscript(fokapoAbumName[km])); // 앨범명
                         fokapoDto.setABUM_DATE_ISSU(fokapoAbumDateIssu[km]); // 앨범발매일
                         fokapoDto.setPEMS_KIND(XssUtil.unscript(fokapoPemsKind[km])); // 실연종류
                         fokapoDto.setPEMS_DESC(XssUtil.unscript(fokapoPemsDesc[km])); // 실연내용
                         fokapoDto.setCTBT_RATE(fokapoCtbtRate[km]); // 기여율(%)

                         fokapoList.add(fokapoDto);
                    }
               }
          }

          if (trst205.equals("1")) {
               for (int km = 0; km < krtraPrpsIdnt.length; km++) {
                    if (krtraPrpsIdnt[km] != null && krtra[km].equals("Y")) {
                         InmtPrps krtraDto = new InmtPrps();

                         krtraDto.setINMT_SEQN(Long.parseLong(krtraPrpsIdnt[km]));
                         krtraDto.setPRPS_DIVS(krtraPrpsDivs[km]);
                         krtraDto.setPRPS_IDNT_CODE(krtraPrpsIdntCode[km]);
                         krtraDto.setCOPT_KIND(krtraCoprKind[km]);
                         krtraDto.setWORK_NAME(krtraWorkName[km]);
                         krtraDto.setWRTR_NAME(krtraWrtrName[km]);
                         krtraDto.setSCYR(krtraScyr[km]);
                         krtraDto.setSCTR(krtraSctr[km]);
                         krtraDto.setSJET(krtraSjet[km]);
                         krtraDto.setBOOK_CNCN(krtraBookCncn[km]);
                         krtraDto.setWORK_DIVS(krtraWorkDivs[km]);

                         krtraList.add(krtraDto);
                    }
               }
          }

          if (trst205_2.equals("1")) {
               for (int km = 0; km < krtraPrpsIdnt.length; km++) {
                    if (krtraPrpsIdnt[km] != null && krtra[km].equals("Y")) {
                         InmtPrps krtraDto2 = new InmtPrps();

                         krtraDto2.setINMT_SEQN(Long.parseLong(krtraPrpsIdnt[km]));
                         krtraDto2.setPRPS_DIVS(krtraPrpsDivs[km]);
                         krtraDto2.setPRPS_IDNT_CODE(krtraPrpsIdntCode[km]);

                         krtraDto2.setWORK_NAME(krtraWorkName[km]);
                         krtraDto2.setWRTR_NAME(krtraWrtrName[km]);
                         krtraDto2.setBOOK_CNCN(krtraBookCncn[km]);
                         krtraDto2.setSCTR(krtraSctr[km]);
                         krtraDto2.setUSEX_TYPE(krtraUsexType[km]);

                         krtraList_2.add(krtraDto2);
                    }
               }
          }

          mv.addObject("srchDIVS", srchDIVS);
          mv.addObject("srchParam", dto);

          mv.addObject("clientInfo", clientInfo);
          mv.addObject("tempList", tempList);
          mv.addObject("tempListCnt", tempList.size());
          mv.addObject("fileList", fileList);
          mv.addObject("trstList", trstList);

          mv.addObject("kappList", kappList);
          mv.addObject("fokapoList", fokapoList);
          mv.addObject("krtraList", krtraList);
          mv.addObject("krtraList_2", krtraList_2);

          mv.addObject("prpsDoblCode", prpsDoblCode);
          mv.addObject("rghtPrpsMastKey", rghtPrpsMastKey);

          return mv;
     }

     // 보상금신청 저장
     public ModelAndView inmtPrpsSave(HttpServletRequest request, HttpServletResponse response) throws Exception {
          // System.out.println("##############################");
          // System.out.println("inmtPrpsSave");
          // System.out.println("##############################");
          // System.out.println("DIVS ="+ServletRequestUtils.getStringParameter(request,
          // "DIVS"));
          // System.out.println("srchSdsrName
          // ="+ServletRequestUtils.getStringParameter(request, "srchSdsrName"));
          // System.out.println("srchMuciName
          // ="+ServletRequestUtils.getStringParameter(request, "srchMuciName"));
          // System.out.println("srchYymm
          // ="+ServletRequestUtils.getStringParameter(request, "srchYymm"));
          // System.out.println("##############################");

          // String srchDIVS = ServletRequestUtils.getStringParameter(request,
          // "srchDIVS");

          // 선택저장물 저장-------------------
          int prpsMastKey = inmtPrpsService.prpsMastKey(); // MAX(ML_PRPS.PRPS_MAST_KEY)+1 set
          int prpsSeqnMax = 0;

          int bPrpsSeqnMax = 0;
          int sPrpsSeqnMax = 0;
          int lPrpsSeqnMax = 0;

          int iBcnt = 0;
          int iScnt = 0;
          int iLcnt = 0;
          // int intiB2cnt=1;

          // --기초 정보들 start
          // ----선택 보상금 정보 start
          String[] prpsIdnt = ServletRequestUtils.getStringParameters(request, "hddnSelectInmtSeqn"); // 선택저작물
                                                                                                      // seq값
          String[] prpsIdntCode = ServletRequestUtils.getStringParameters(request, "hddnSelectPrpsIdntCode");
          String[] prpsDivs = ServletRequestUtils.getStringParameters(request, "hddnSelectPrpsDivs"); // 신청구분값
                                                                                                      // B,S,L
          String[] kapp = ServletRequestUtils.getStringParameters(request, "hddnSelectKapp");
          String[] fokapo = ServletRequestUtils.getStringParameters(request, "hddnSelectFokapo");
          String[] krtra = ServletRequestUtils.getStringParameters(request, "hddnSelectKrtra");
          String[] muciName = ServletRequestUtils.getStringParameters(request, "hddnSelectMuciName");
          String[] sdsrName = ServletRequestUtils.getStringParameters(request, "hddnSelectSdsrName");
          // ----선택 보상금 정보 end

          // ----동시신청 정보 start
          String prpsDoblCode = ServletRequestUtils.getStringParameter(request, "prpsDoblCode", ""); // 동시신청
                                                                                                     // 구분코드
          String rghtPrpsMastKey = ServletRequestUtils.getStringParameter(request, "rghtPrpsMastKey", ""); // 저작권찾기
                                                                                                           // 마스터키
          // ----동시신청 정보 end
          // ----신청자 정보 start
          String userIdnt = ServletRequestUtils.getStringParameter(request, "hddnUserIdnt");
          String prpsName = ServletRequestUtils.getStringParameter(request, "txtPrpsName");
          String prpsDesc = ServletRequestUtils.getStringParameter(request, "txtareaPrpsDesc");
          String homeTelxNumb = ServletRequestUtils.getStringParameter(request, "txtHomeTelxNumb");
          String busiTelxNumb = ServletRequestUtils.getStringParameter(request, "txtBusiTelxNumb");
          String moblPhon = ServletRequestUtils.getStringParameter(request, "txtMoblPhon");
          String faxxNumb = ServletRequestUtils.getStringParameter(request, "txtFaxxNumb");
          String mail = ServletRequestUtils.getStringParameter(request, "txtMail");
          String homeAddr = ServletRequestUtils.getStringParameter(request, "txtHomeAddr");
          String busiAddr = ServletRequestUtils.getStringParameter(request, "txtBusiAddr");
          System.out.println("###3333#################################" + busiAddr + "###############################################");
          String bankName = ServletRequestUtils.getStringParameter(request, "txtBankName");
          String acctNumb = ServletRequestUtils.getStringParameter(request, "txtAcctNumb");
          String dptr = ServletRequestUtils.getStringParameter(request, "txtDptr");
          System.out.println("###333333#################################" + dptr + "###############################################");

          String insertKapp = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnInsertKapp"));
          String insertFokapo = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnInsertFokapo"));
          String insertKrtra = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnInsertKrtra"));
          // ----신청자 정보 end

          // ----음제협에서 사용할 정보 start
          String[] kappDiskName = ServletRequestUtils.getStringParameters(request, "txtKappDiskName");
          String[] kappCdCode = ServletRequestUtils.getStringParameters(request, "txtKappCdCode");
          String[] kappSongName = ServletRequestUtils.getStringParameters(request, "hddnKappSongName");
          String[] kappPrpsIdnt = ServletRequestUtils.getStringParameters(request, "hddnKappPrpsIdnt");
          String[] kappPrpsIdntCode = ServletRequestUtils.getStringParameters(request, "hddnKappPrpsIdntCode");
          String[] kappSnerName = ServletRequestUtils.getStringParameters(request, "txtKappSnerName");
          String[] kappDateIssu = ServletRequestUtils.getStringParameters(request, "txtKappDateIssu");
          String[] kappNatuName = ServletRequestUtils.getStringParameters(request, "txtKappNatuName");
          String[] kappRghtGrnd = ServletRequestUtils.getStringParameters(request, "txtKappRghtGrnd");
          String[] kappLyriWrtr = ServletRequestUtils.getStringParameters(request, "txtKappLyriWrtr");
          String[] kappComsWrtr = ServletRequestUtils.getStringParameters(request, "txtKappComsWrtr");
          String[] kappMuscGnre = ServletRequestUtils.getStringParameters(request, "selKappMuscGnre");
          // ----음제협에서 사용할 정보 end

          // ----음실연에서 사용할 정보 start
          String fokapoPemrRlnm = ServletRequestUtils.getStringParameter(request, "txtFokapoPemrRlnm");
          String fokapoPemrStnm = ServletRequestUtils.getStringParameter(request, "txtFokapoPemrStnm");
          String fokapoGrupName = ServletRequestUtils.getStringParameter(request, "txtFokapoGrupName");
          String fokapoSesdNumb = ServletRequestUtils.getStringParameter(request, "txtFokapoResdNumb");
          String fokapoSesdNumb_1 = ServletRequestUtils.getStringParameter(request, "txtFokapoResdNumb_1");
          String fokapoSesdNumb_2 = ServletRequestUtils.getStringParameter(request, "txtFokapoResdNumb_2");
          String[] fokapoPrpsIdnt = ServletRequestUtils.getStringParameters(request, "hddnFokapoPrpsIdnt");
          String[] fokapoPrpsIdntCode = ServletRequestUtils.getStringParameters(request, "hddnFokapoPrpsIdntCode");
          String[] fokapoPdtnName = ServletRequestUtils.getStringParameters(request, "hddnFokapoPdtnName");
          String[] fokapoSnerName = ServletRequestUtils.getStringParameters(request, "txtFokapoSnerName");
          String[] fokapoAbumName = ServletRequestUtils.getStringParameters(request, "txtFokapoAbumName");
          String[] fokapoAbumDateIssu = ServletRequestUtils.getStringParameters(request, "txtFokapoAbumDateIssu");
          String[] fokapoPemsKind = ServletRequestUtils.getStringParameters(request, "txtFokapoPemsKind");
          String[] fokapoPemsDesc = ServletRequestUtils.getStringParameters(request, "txtFokapoPemsDesc");
          String[] fokapoCtbtRate = ServletRequestUtils.getStringParameters(request, "txtFokapoCtbtRate");
          // ----음실연에서 사용할 정보 end

          // ----복전협에서 사용할 정보 start
          String[] krtraPrpsIdnt = ServletRequestUtils.getStringParameters(request, "hddnKrtraPrpsIdnt");
          String[] krtraPrpsIdntCode = ServletRequestUtils.getStringParameters(request, "hddnKrtraPrpsIdntCode");
          String[] krtraPrpsDivs = ServletRequestUtils.getStringParameters(request, "hddnKrtraPrpsDivs");
          String krtraRtpsRlnm = ServletRequestUtils.getStringParameter(request, "txtKrtraPemrRlnm");
          String krtraRtpsStnm = ServletRequestUtils.getStringParameter(request, "txtKrtraPemrStnm");
          String krtraRtpsCrnm = ServletRequestUtils.getStringParameter(request, "txtKrtraGrupName");
          String krtraResnNumb = ServletRequestUtils.getStringParameter(request, "txtKrtraResdNumb");
          String krtraResnNumb_1 = ServletRequestUtils.getStringParameter(request, "txtKrtraResdNumb_1");
          String krtraResnNumb_2 = ServletRequestUtils.getStringParameter(request, "txtKrtraResdNumb_2");

          String[] krtraCoprKind = ServletRequestUtils.getStringParameters(request, "hddnKrtraCoprKind");
          String[] krtraWorkName = ServletRequestUtils.getStringParameters(request, "hddnKrtraWorkName");
          String[] krtraWrtrName = ServletRequestUtils.getStringParameters(request, "hddnKrtraWrtrName");
          String[] krtraScyr = ServletRequestUtils.getStringParameters(request, "hddnKrtraScyr");
          String[] krtraSctr = ServletRequestUtils.getStringParameters(request, "hddnKrtraSctr");
          String[] krtraSjet = ServletRequestUtils.getStringParameters(request, "hddnKrtraSjet");
          String[] krtraBookCncn = ServletRequestUtils.getStringParameters(request, "hddnKrtraBookCncn");
          String[] krtraWorkDivs = ServletRequestUtils.getStringParameters(request, "hddnKrtraWorkDivs");
          String[] krtraUsexType = ServletRequestUtils.getStringParameters(request, "hddnKrtraUsexType");

          // ----복전협에서 사용할 정보 end

          // ----첨부파일 관련정보 start
          String[] getFileOrgnCode = ServletRequestUtils.getStringParameters(request, "hddnGetFileOrgnCode");
          String[] getRealFileName = ServletRequestUtils.getStringParameters(request, "hddnGetRealFileName");
          String[] getFilePath = ServletRequestUtils.getStringParameters(request, "hddnGetFilePath");
          String[] getFileName = ServletRequestUtils.getStringParameters(request, "hddnGetFileName");
          String[] getFileSize = ServletRequestUtils.getStringParameters(request, "hddnGetFileSize");
          // ----첨부파일 관련정보 end

          // ----등록신청이 오프라인이나 첨부파일인지 확인 start
          String trst203 = ServletRequestUtils.getStringParameter(request, "hddnTrst203");
          String trst202 = ServletRequestUtils.getStringParameter(request, "hddnTrst202");
          String trst205 = ServletRequestUtils.getStringParameter(request, "hddnTrst205");
          String trst205_2 = ServletRequestUtils.getStringParameter(request, "hddnTrst205_2");

          String[] arrTrstOrgnCode = new String[4];
          String[] arrOffxLineRecp = new String[4];
          String[] arrTrstAttchfileYn = new String[4];
          int arrCnt = 0;

          if (trst203 != null && trst203.equals("1")) {
               arrTrstOrgnCode[arrCnt] = "203";
               String chkKappOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnKappOff")); // offline
                                                                                                                           // 접수여부
               if (!chkKappOff.equals("Y")) {
                    arrOffxLineRecp[arrCnt] = "N";
                    arrTrstAttchfileYn[arrCnt] = "Y";
               } else {
                    arrOffxLineRecp[arrCnt] = "Y";
                    arrTrstAttchfileYn[arrCnt] = "N";
               }
               arrCnt++;
          }
          if (trst202 != null && trst202.equals("1")) {
               arrTrstOrgnCode[arrCnt] = "202";
               String chkFokapoOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnFokapoOff")); // offline
                                                                                                                               // 접수여부
               if (!chkFokapoOff.equals("Y")) {
                    arrOffxLineRecp[arrCnt] = "N";
                    arrTrstAttchfileYn[arrCnt] = "Y";
               } else {
                    arrOffxLineRecp[arrCnt] = "Y";
                    arrTrstAttchfileYn[arrCnt] = "N";
               }
               arrCnt++;
          }
          if (trst205 != null && trst205.equals("1")) {
               arrTrstOrgnCode[arrCnt] = "205";
               String chkKrtraOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnKrtraOff")); // offline
                                                                                                                             // 접수여부
               if (!chkKrtraOff.equals("Y")) {
                    arrOffxLineRecp[arrCnt] = "N";
                    arrTrstAttchfileYn[arrCnt] = "Y";
               } else {
                    arrOffxLineRecp[arrCnt] = "Y";
                    arrTrstAttchfileYn[arrCnt] = "N";
               }
               arrCnt++;
          }
          if (trst205_2 != null && trst205_2.equals("1")) {
               arrTrstOrgnCode[arrCnt] = "205_2";
               String chkKrtraOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(request, "hddnKrtraOff")); // offline
                                                                                                                             // 접수여부
               if (!chkKrtraOff.equals("Y")) {
                    arrOffxLineRecp[arrCnt] = "N";
                    arrTrstAttchfileYn[arrCnt] = "Y";
               } else {
                    arrOffxLineRecp[arrCnt] = "Y";
                    arrTrstAttchfileYn[arrCnt] = "N";
               }
               arrCnt++;
          }
          // ----등록신청이 오프라인이나 첨부파일인지 확인 end
          // --기초 정보들 end

          // --신청내역 등록
          Map map = new HashMap();
          map.put("userIdnt", userIdnt);
          map.put("prpsName", prpsName);
          map.put("prpsDesc", prpsDesc);
          map.put("homeTelxNumb", homeTelxNumb);
          map.put("busiTelxNumb", busiTelxNumb);
          map.put("moblPhon", moblPhon);
          map.put("faxxNumb", faxxNumb);
          map.put("mail", mail);
          map.put("homeAddr", homeAddr);
          map.put("busiAddr", busiAddr);
          map.put("bankName", bankName);
          map.put("acctNumb", acctNumb);
          map.put("dptr", dptr);

          map.put("prpsDoblCode", prpsDoblCode);
          map.put("rghtPrpsMastKey", rghtPrpsMastKey);

          int iSeqn = 0;
          for (int te = 0; te < prpsIdnt.length; te++) {

               map.put("prpsIdnt", prpsIdnt[te]);
               map.put("prpsIdntCode", prpsIdntCode[te]);
               map.put("prpsMastKey", "" + prpsMastKey);

               // int chkCnt = 0; // 선택저작물에 대한 신탁별 데이타유무 체크값.

               if ((prpsDivs[te].equals("B") && iBcnt == 0) || (prpsDivs[te].equals("S") && iScnt == 0) || (prpsDivs[te].equals("L") && iLcnt == 0)) {

                    prpsSeqnMax = inmtPrpsService.prpsSeqnMax(); // MAX(ML_PRPS.PRPS_SEQN)+1 set

                    map.put("prpsSeqnMax", "" + prpsSeqnMax);
                    map.put("prpsDivs", prpsDivs[te]);

                    inmtPrpsService.inmtPspsInsert(map); // insert ML_PRPS

                    if (prpsDivs[te].equals("B"))
                         bPrpsSeqnMax = prpsSeqnMax;
                    else if (prpsDivs[te].equals("S"))
                         sPrpsSeqnMax = prpsSeqnMax;
                    else if (prpsDivs[te].equals("L"))
                         lPrpsSeqnMax = prpsSeqnMax;
               }

               if (prpsDivs[te].equals("B")) {
                    iBcnt++;
                    prpsSeqnMax = bPrpsSeqnMax;
               } else if (prpsDivs[te].equals("S")) {
                    iScnt++;
                    prpsSeqnMax = sPrpsSeqnMax;
               } else if (prpsDivs[te].equals("L")) {
                    iLcnt++;
                    prpsSeqnMax = lPrpsSeqnMax;
               }

               for (int k = 0; k < arrCnt; k++) {
                    // Map trstMap = new HashMap();
                    map.put("trstOrgnCode", arrTrstOrgnCode[k]);
                    map.put("trstAttchfileYn", arrTrstAttchfileYn[k]);
                    map.put("offxLineRecp", arrOffxLineRecp[k]);

                    String trstOrgnCode = arrTrstOrgnCode[k]; // 신탁구분코드
                    String attcYsNo = "N";
                    String offxLineRecp = "N";

                    if (arrTrstAttchfileYn[k] != null)
                         attcYsNo = arrTrstAttchfileYn[k];
                    if (arrOffxLineRecp[k] != null)
                         offxLineRecp = arrOffxLineRecp[k];

                    int iCnt = 0;
                    // ----음제협 신청내역 등록 start
                    if (insertKapp.equals("Y") && trstOrgnCode.equals("203") && kapp[te].equals("Y")) {
                         for (int km = 0; km < kappPrpsIdnt.length; km++) {
                              Map kappMap = new HashMap();
                              kappMap.put("kappSongName", kappSongName[km]);
                              kappMap.put("kappPrpsIdnt", kappPrpsIdnt[km]);
                              kappMap.put("kappPrpsIdntCode", kappPrpsIdntCode[km]);
                              kappMap.put("kappDiskName", kappDiskName[km]);
                              kappMap.put("kappCdCode", kappCdCode[km]);
                              kappMap.put("kappSnerName", kappSnerName[km]);
                              kappMap.put("kappDateIssu", kappDateIssu[km]);
                              kappMap.put("kappNatuName", kappNatuName[km]);
                              kappMap.put("kappRghtGrnd", kappRghtGrnd[km]);
                              kappMap.put("kappLyriWrtr", kappLyriWrtr[km]);
                              kappMap.put("kappComsWrtr", kappComsWrtr[km]);
                              kappMap.put("kappMuscGnre", kappMuscGnre[km]);
                              kappMap.put("prpsMastKey", "" + prpsMastKey);
                              kappMap.put("userIdnt", userIdnt);
                              kappMap.put("prpsSeqnMax", "" + prpsSeqnMax);
                              kappMap.put("trstOrgnCode", trstOrgnCode);
                              kappMap.put("prpsIdnt", prpsIdnt[te]); // 선택저작물 seq값
                              kappMap.put("prpsDivs", prpsDivs[te]); // 신청구분값 B,S,L
                              kappMap.put("attcYsNo", attcYsNo);
                              kappMap.put("offxLineRecp", offxLineRecp);

                              if (kappMap.get("kappPrpsIdnt").equals(prpsIdnt[te])) {
                                   iCnt++;
                                   inmtPrpsService.inmtKappInsert(kappMap);

                                   iSeqn = km + 1;
                              }
                         }
                    }
                    // ----음제협 신청내역 등록 end

                    // ----음실연 신청내역 등록 start
                    else if (insertFokapo.equals("Y") && trstOrgnCode.equals("202") && fokapo[te].equals("Y")) {
                         for (int km = 0; km < fokapoPrpsIdnt.length; km++) {
                              Map fokapoMap = new HashMap();
                              fokapoMap.put("fokapoPemrRlnm", fokapoPemrRlnm);
                              fokapoMap.put("fokapoPemrStnm", fokapoPemrStnm);
                              fokapoMap.put("fokapoGrupName", fokapoGrupName);
                              fokapoMap.put("fokapoSesdNumb", fokapoSesdNumb);
                              fokapoMap.put("fokapoSesdNumb", fokapoSesdNumb_1 + fokapoSesdNumb_2);
                              fokapoMap.put("fokapoPrpsIdnt", fokapoPrpsIdnt[km]);
                              fokapoMap.put("fokapoPrpsIdntCode", fokapoPrpsIdntCode[km]);
                              fokapoMap.put("fokapoPdtnName", fokapoPdtnName[km]);
                              fokapoMap.put("fokapoSnerName", fokapoSnerName[km]);
                              fokapoMap.put("fokapoAbumName", fokapoAbumName[km]);
                              fokapoMap.put("fokapoAbumDateIssu", fokapoAbumDateIssu[km]);
                              fokapoMap.put("fokapoPemsKind", fokapoPemsKind[km]);
                              fokapoMap.put("fokapoPemsDesc", fokapoPemsDesc[km]);
                              fokapoMap.put("fokapoCtbtRate", fokapoCtbtRate[km]);

                              fokapoMap.put("prpsMastKey", "" + prpsMastKey);
                              fokapoMap.put("userIdnt", userIdnt);
                              fokapoMap.put("prpsSeqnMax", "" + prpsSeqnMax);
                              fokapoMap.put("trstOrgnCode", trstOrgnCode);
                              fokapoMap.put("prpsIdnt", prpsIdnt[te]); // 선택저작물 seq값
                              fokapoMap.put("prpsDivs", prpsDivs[te]); // 신청구분값 B,S,L
                              fokapoMap.put("attcYsNo", attcYsNo);
                              fokapoMap.put("offxLineRecp", offxLineRecp);

                              if (fokapoMap.get("fokapoPrpsIdnt").equals(prpsIdnt[te])) {
                                   iCnt++;
                                   inmtPrpsService.inmtFokapoInsert(fokapoMap);

                                   iSeqn = km + 1;
                              }
                         }
                    }
                    // ----음실연 신청내역 등록 end

                    // ----복전협 신청내역 등록 start
                    // ------교과용 start
                    else if (insertKrtra.equals("Y") && trstOrgnCode.equals("205") && krtra[te].equals("Y") && prpsDivs[te].equals("S")) {
                         for (int km = 0; km < krtraPrpsIdnt.length; km++) {
                              Map krtraMap = new HashMap();
                              krtraMap.put("krtraPrpsIdnt", krtraPrpsIdnt[km]);
                              krtraMap.put("krtraPrpsDivs", krtraPrpsDivs[km]);
                              krtraMap.put("krtraPrpsIdntCode", krtraPrpsIdntCode[km]);
                              krtraMap.put("krtraRtpsRlnm", krtraRtpsRlnm);
                              krtraMap.put("krtraRtpsStnm", krtraRtpsStnm);
                              krtraMap.put("krtraRtpsCrnm", krtraRtpsCrnm);
                              krtraMap.put("krtraResnNumb", krtraResnNumb);
                              krtraMap.put("krtraResnNumb", krtraResnNumb_1 + krtraResnNumb_2);

                              krtraMap.put("krtraCoprKind", krtraCoprKind[km]);
                              krtraMap.put("krtraWorkName", krtraWorkName[km]);
                              krtraMap.put("krtraWrtrName", krtraWrtrName[km]);
                              krtraMap.put("krtraScyr", krtraScyr[km]);
                              krtraMap.put("krtraSctr", krtraSctr[km]);
                              krtraMap.put("krtraSjet", krtraSjet[km]);
                              krtraMap.put("krtraBookCncn", krtraBookCncn[km]);
                              krtraMap.put("krtraWorkDivs", krtraWorkDivs[km]);
                              krtraMap.put("krtraUsexType", krtraWorkDivs[km]); // 동일 컬럼사용으로 인한 처리.

                              krtraMap.put("prpsMastKey", "" + prpsMastKey);
                              krtraMap.put("userIdnt", userIdnt);
                              krtraMap.put("prpsSeqnMax", "" + prpsSeqnMax);
                              krtraMap.put("trstOrgnCode", trstOrgnCode);
                              krtraMap.put("prpsIdnt", prpsIdnt[te]); // 선택저작물 seq값
                              krtraMap.put("prpsDivs", prpsDivs[te]); // 신청구분값 B,S,L
                              krtraMap.put("attcYsNo", attcYsNo);
                              krtraMap.put("offxLineRecp", offxLineRecp);

                              if (krtraMap.get("krtraPrpsIdnt").equals(prpsIdnt[te]) && krtraMap.get("krtraPrpsDivs").equals("S")) {
                                   iCnt++;
                                   inmtPrpsService.inmtKrtraInsert(krtraMap);

                                   iSeqn = km + 1;
                              }
                         }
                    }
                    // ------교과용 end

                    // ------도서관 start
                    else if (insertKrtra.equals("Y") && trstOrgnCode.equals("205_2") && krtra[te].equals("Y") && prpsDivs[te].equals("L")) {
                         for (int km = 0; km < krtraPrpsIdnt.length; km++) {
                              Map krtraMap = new HashMap();
                              krtraMap.put("krtraPrpsIdnt", krtraPrpsIdnt[km]);
                              krtraMap.put("krtraPrpsDivs", krtraPrpsDivs[km]);
                              krtraMap.put("krtraPrpsIdntCode", krtraPrpsIdntCode[km]);
                              krtraMap.put("krtraRtpsRlnm", krtraRtpsRlnm);
                              krtraMap.put("krtraRtpsStnm", krtraRtpsStnm);
                              krtraMap.put("krtraRtpsCrnm", krtraRtpsCrnm);
                              krtraMap.put("krtraResnNumb", krtraResnNumb);
                              krtraMap.put("krtraResnNumb", krtraResnNumb_1 + krtraResnNumb_2);

                              krtraMap.put("krtraWorkName", krtraWorkName[km]);
                              krtraMap.put("krtraWrtrName", krtraWrtrName[km]);
                              krtraMap.put("krtraBookCncn", krtraBookCncn[km]);
                              krtraMap.put("krtraSctr", krtraSctr[km]);
                              krtraMap.put("krtraUsexType", krtraUsexType[km]);

                              krtraMap.put("prpsMastKey", "" + prpsMastKey);
                              krtraMap.put("userIdnt", userIdnt);
                              krtraMap.put("prpsSeqnMax", "" + prpsSeqnMax);
                              krtraMap.put("trstOrgnCode", "205");
                              krtraMap.put("prpsIdnt", prpsIdnt[te]); // 선택저작물 seq값
                              krtraMap.put("prpsDivs", prpsDivs[te]); // 신청구분값 B,S,L
                              krtraMap.put("attcYsNo", attcYsNo);
                              krtraMap.put("offxLineRecp", offxLineRecp);

                              if (krtraMap.get("krtraPrpsIdnt").equals(prpsIdnt[te]) && krtraMap.get("krtraPrpsDivs").equals("L")) {
                                   iCnt++;
                                   inmtPrpsService.inmtKrtraInsert(krtraMap);

                                   iSeqn = km + 1;
                              }
                         }
                    }
                    // ------도서관 end
                    // ----복전협 신청내역 등록 end

                    // ----신청처리결과 등록 start
                    if (iCnt > 0) {
                         map.put("prpsSeqnMax", "" + prpsSeqnMax);
                         map.put("prpsDivs", prpsDivs[te]);
                         // map.put("seqn", te+1);
                         map.put("seqn", iSeqn); // 저작물 순번

                         if (trstOrgnCode.equals("205_2") && prpsDivs[te].equals("L"))
                              map.put("trstOrgnCode", "205");

                         // 신탁단체별 신청처리결과 기본값 insert
                         inmtPrpsService.inmtPrpsRsltInsert(map); // insert ML_PRPS_RSLT
                    }
                    // ----신청처리결과 등록 end
               }
          }

          // ----첨부파일등록 start
          // File Upload
          for (int f = 0; f < getFileOrgnCode.length; f++) {
               PrpsAttc prpsAttc = new PrpsAttc();

               prpsAttc.setREAL_FILE_NAME(getRealFileName[f]);
               prpsAttc.setFILE_PATH(getFilePath[f]);
               prpsAttc.setFILE_NAME(getFileName[f]);
               prpsAttc.setFILE_SIZE(getFileSize[f]);

               if (getFileOrgnCode[f].equals("205_2")) {
                    prpsAttc.setTRST_ORGN_CODE("205");
               } else {
                    prpsAttc.setTRST_ORGN_CODE(getFileOrgnCode[f]);
               }

               if (getFileOrgnCode[f].equals("203") || getFileOrgnCode[f].equals("202")) {
                    prpsAttc.setPRPS_DIVS("B");
               } else if (getFileOrgnCode[f].equals("205")) {
                    prpsAttc.setPRPS_DIVS("S");
               } else {
                    prpsAttc.setPRPS_DIVS("L");
               }

               prpsAttc.setUSER_IDNT(userIdnt);
               prpsAttc.setPRPS_MAST_KEY(prpsMastKey + "");

               rghtPrpsService.insertPrpsAttc(prpsAttc);
          }
          // ----첨부파일등록 end
          // ----------------------------------

          // 메일, sms 발송--------------------
          Map params = new HashMap();
          User user = SessionUtil.getSession(request);

          params.put("userIdnt", user.getUserIdnt());
          Map userInfo = userService.selectUserInfo(params); // 사용자 조회

          String host = kr.or.copyright.mls.support.constant.Constants.MAIL_SERVER_IP; // smtp서버
          String to = userInfo.get("U_MAIL") + ""; // 수신EMAIL
          String toName = map.get("prpsName").toString(); // 수신인
          String subject = "[저작권찾기] 보상금신청 완료";
          // String idntName = muciName[0]; // 신청대상명
          String idntName = sdsrName[0]; // 신청대상명x

          if (prpsIdnt.length > 0)
               // idntName += " 외 "+ ( prpsIdnt.length - 1 )+ "건";
               idntName += " 외 " + (iSeqn - 1) + "건";

          String from = kr.or.copyright.mls.support.constant.Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 -
                                                                                            // 시스템 대표 메일
          String fromName = kr.or.copyright.mls.support.constant.Constants.SYSTEM_NAME;

          System.out.println("#######################");
          System.out.println("idntName = " + idntName);
          // System.out.println("U_MAIL_RECE_YSNO = " + userInfo.get("U_MAIL_RECE_YSNO"));
          // System.out.println("host = " + host);
          // System.out.println("from = " + from);
          // System.out.println("to = " + to);
          // System.out.println("#######################");
          if (userInfo.get("U_MAIL_RECE_YSNO").equals("Y") && to != null && to != "") {

               // ------------------------- mail 정보 ----------------------------------//
               MailInfo info = new MailInfo();
               info.setFrom(from);
               info.setFromName(fromName);
               info.setHost(host);
               info.setEmail(to);
               info.setEmailName(toName);
               info.setSubject(subject);

               StringBuffer sb = new StringBuffer();

               sb.append("<div class=\"mail_title\">" + toName + "님, 보상금신청이  완료되었습니다. </div>");
               sb.append("<div class=\"mail_contents\"> ");
               sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
               sb.append("		<tr> ");
               sb.append("			<td> ");
               sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
               // 변경부분
               sb.append("					<tr> ");
               sb.append("						<td class=\"tdLabel\">신청 저작물명</td> ");
               sb.append("						<td class=\"tdData\">" + idntName + "</td> ");
               sb.append("					</tr> ");
               // 변경부분
               // sb.append(" <tr> ");
               // sb.append(" <td class=\"tdLabel\">담당자 이메일주소</td> ");
               // sb.append(" <td class=\"tdData\"><a
               // href=\"mailto:"+kr.or.copyright.mls.support.constant.Constants.SYSTEM_MAIL_ADDRESS+"\"
               // onFocus='this.blur()'>"+kr.or.copyright.mls.support.constant.Constants.SYSTEM_MAIL_ADDRESS+"</a></td>
               // ");
               // sb.append(" </tr> ");
               // sb.append(" <tr> ");
               // sb.append(" <td class=\"tdLabel\">담당자 전화번호</td> ");
               // sb.append(" <td
               // class=\"tdData\">"+kr.or.copyright.mls.support.constant.Constants.SYSTEM_TELEPHONE+"</td>
               // ");
               // sb.append(" </tr> ");

               sb.append("				</table> ");

               if (iBcnt > 0) {
                    sb.append("			<br/> ");
                    sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
                    sb.append("				<tr> 	 ");
                    sb.append("					<td>  ");
                    sb.append("						<font color=\"blue\" > * 방송음악보상금 신청에 관한 문의는 신청분류에 해당하는 단체로 연락바랍니다.</font> ");
                    sb.append("					</td> ");
                    sb.append("				</tr> ");
                    sb.append("				<tr> ");
                    sb.append("					<td class=\"tdData\">  ");

                    sb.append("						<li><b>한국음악실연자연합회</b></li> ");
                    sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"
                              + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_202").getBytes("ISO-8859-1"), "EUC-KR") + "</u></b>"
                              + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;- " + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_tel_202").getBytes("ISO-8859-1"), "EUC-KR")
                              + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;- " + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_mail_202").getBytes("ISO-8859-1"), "EUC-KR"));

                    sb.append("						<li><b>한국음원제작자협회</b></li> ");
                    sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;- "
                              + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_203").getBytes("ISO-8859-1"), "EUC-KR") + "  "
                              + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_tel_203").getBytes("ISO-8859-1"), "EUC-KR"));

                    sb.append("					</td> ");
                    sb.append("				</tr> ");
                    sb.append("			</table>");
               }

               else if (iScnt > 0) {
                    sb.append("			<br/> ");
                    sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
                    sb.append("				<tr> 	 ");
                    sb.append("					<td>  ");
                    sb.append("						<font color=\"blue\" > * 교과용보상금 신청에 관한 문의는 신청분류에 해당하는 단체로 연락바랍니다.</font> ");
                    sb.append("					</td> ");
                    sb.append("				</tr> ");
                    sb.append("				<tr> ");
                    sb.append("					<td class=\"tdData\">  ");

                    sb.append("						<li><b>한국복사전송권협회</b></li> ");
                    sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"
                              + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_205").getBytes("ISO-8859-1"), "EUC-KR") + "</u></b>"
                              + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;- " + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_tel_205").getBytes("ISO-8859-1"), "EUC-KR")
                              + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;- " + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_mail_205").getBytes("ISO-8859-1"), "EUC-KR"));

                    sb.append("					</td> ");
                    sb.append("				</tr> ");
                    sb.append("			</table>");
               }

               else if (iLcnt > 0) {
                    sb.append("			<br/> ");
                    sb.append("			<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
                    sb.append("				<tr> 	 ");
                    sb.append("					<td>  ");
                    sb.append("						<font color=\"blue\" > * 도서관보상금 신청에 관한 문의는 신청분류에 해당하는 단체로 연락바랍니다.</font> ");
                    sb.append("					</td> ");
                    sb.append("				</tr> ");
                    sb.append("				<tr> ");
                    sb.append("					<td class=\"tdData\">  ");

                    sb.append("						<li><b>한국복사전송권협회</b></li> ");
                    sb.append("						<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>"
                              + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_205").getBytes("ISO-8859-1"), "EUC-KR") + "</u></b>"
                              + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;- " + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_tel_205").getBytes("ISO-8859-1"), "EUC-KR")
                              + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;- " + new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_mail_205").getBytes("ISO-8859-1"), "EUC-KR"));

                    sb.append("					</td> ");
                    sb.append("				</tr> ");
                    sb.append("			</table>");
               }

               sb.append("			</td> ");
               sb.append("		</tr> ");
               sb.append("	</table> ");
               sb.append("</div> ");

               info.setMessage(new String(MailMessage.getChangeInfoMailText(sb.toString())));
               MailManager manager = MailManager.getInstance();

               info = manager.sendMail(info);
               if (info.isSuccess()) {
                    System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());
               } else {
                    System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
               }
          }

          /* 20120709 SMS수정 정병호 */
          String smsTo = userInfo.get("U_MOBL_PHON") + "";
          String smsFrom = kr.or.copyright.mls.support.constant.Constants.SYSTEM_TELEPHONE;

          String smsMessage = "[저작권찾기] " + idntName + " - 보상금신청이  완료되었습니다.";

          if (userInfo.get("U_SMS_RECE_YSNO").equals("Y") && to != null && to != "") {
               SMSManager smsManager = new SMSManager();

               System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
          }
          /* //20120709 SMS수정 */
          // end email, sms

          // ----------------------------------

          // 접속정보 저장---------------------
          map.put("connUrl", "보상금신청 등록");
          inmtPrpsService.insertConnInfo(map);
          // ----------------------------------

          ////////////////////////////////////////////////////////////////////////////////////////////

          MailInfo orgnInfo = new MailInfo();
          orgnInfo.setFrom(from);
          orgnInfo.setFromName(fromName);
          orgnInfo.setHost(host);

          orgnInfo.setPrpsTitle(idntName); // 저작물 명
          orgnInfo.setPrpsName(map.get("prpsName").toString()); // 신청자 이름
          orgnInfo.setPrpsEmail(userInfo.get("U_MAIL") + ""); // 신청자 이메일
          orgnInfo.setPrpsSms(""); // 신청자 모바일

          String orgnTo = "";
          String orgnToName = "";

          // 음제
          if (trst203 != null && trst203.equals("1")) {

               orgnInfo.setSubject("[저작권찾기] 방송음악 보상금이 신청되었습니다. ");
               orgnInfo.setPrpsDivsName("방송음악"); // 장르

               orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_mail_203").getBytes("ISO-8859-1"), "EUC-KR"); // 수신EMAIL
               orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_203").getBytes("ISO-8859-1"), "EUC-KR"); // 수신인

               orgnInfo.setEmail(orgnTo);
               orgnInfo.setEmailName(orgnToName);

               orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
               MailManager manager = MailManager.getInstance();

               orgnInfo = manager.sendMail(orgnInfo);

               if (orgnInfo.isSuccess()) {
                    System.out.println(">>>>>>>>>>>> 1. message success :" + orgnInfo.getEmail());
               } else {
                    System.out.println(">>>>>>>>>>>> 1. message false   :" + orgnInfo.getEmail());
               }
          }
          // 음실련
          if (trst202 != null && trst202.equals("1")) {
               orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_mail_202").getBytes("ISO-8859-1"), "EUC-KR"); // 수신EMAIL
               orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_202").getBytes("ISO-8859-1"), "EUC-KR"); // 수신인

               orgnInfo.setSubject("[저작권찾기] 방송음악 보상금이 신청되었습니다. ");
               orgnInfo.setPrpsDivsName("방송음악"); // 장르
               orgnInfo.setEmail(orgnTo);
               orgnInfo.setEmailName(orgnToName);

               orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
               MailManager manager = MailManager.getInstance();

               orgnInfo = manager.sendMail(orgnInfo);

               if (orgnInfo.isSuccess()) {
                    System.out.println(">>>>>>>>>>>> 1. message success :" + orgnInfo.getEmail());
               } else {
                    System.out.println(">>>>>>>>>>>> 2. message false   :" + orgnInfo.getEmail());
               }
          }

          // 복전 1
          if (trst205 != null && trst205.equals("1")) {
               orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_mail_205").getBytes("ISO-8859-1"), "EUC-KR"); // 수신EMAIL
               orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_205").getBytes("ISO-8859-1"), "EUC-KR"); // 수신인

               orgnInfo.setSubject("[저작권찾기] 교과용 보상금이 신청되었습니다. ");
               orgnInfo.setPrpsDivsName("교과용"); // 장르

               orgnInfo.setEmail(orgnTo);
               orgnInfo.setEmailName(orgnToName);

               orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
               MailManager manager = MailManager.getInstance();

               orgnInfo = manager.sendMail(orgnInfo);

               if (orgnInfo.isSuccess()) {
                    System.out.println(">>>>>>>>>>>> 1. message success :" + orgnInfo.getEmail());
               } else {
                    System.out.println(">>>>>>>>>>>> 2. message false   :" + orgnInfo.getEmail());
               }
          }

          // 문예
          if (trst205_2 != null && trst205_2.equals("1")) {
               orgnTo = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_mail_205_2").getBytes("ISO-8859-1"), "EUC-KR"); // 수신EMAIL
               orgnToName = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("inmtInfo_name_205_2").getBytes("ISO-8859-1"), "EUC-KR"); // 수신인

               orgnInfo.setSubject("[저작권찾기] 도서관 보상금이 신청되었습니다. ");
               orgnInfo.setPrpsDivsName("도서관"); // 장르

               orgnInfo.setEmail(orgnTo);
               orgnInfo.setEmailName(orgnToName);

               orgnInfo.setMessage(new String(MailMessage.getChangeOrgnInfoMailText(orgnInfo)));
               MailManager manager = MailManager.getInstance();

               orgnInfo = manager.sendMail(orgnInfo);

               if (orgnInfo.isSuccess()) {
                    System.out.println(">>>>>>>>>>>> 1. message success :" + orgnInfo.getEmail());
               } else {
                    System.out.println(">>>>>>>>>>>> 2. message false   :" + orgnInfo.getEmail());
               }
          }

          ////////////////////////////////////////////////////////////////////////////////////////////
          InmtPrps dto = new InmtPrps();
          dto.setSrchDIVS(ServletRequestUtils.getStringParameter(request, "srchDIVS")); // 구분
          dto.setSrchSdsrName(ServletRequestUtils.getStringParameter(request, "srchSdsrName")); // 곡명
                                                                                                // /저작물명
                                                                                                // /저작물명
          dto.setSrchMuciName(ServletRequestUtils.getStringParameter(request, "srchMuciName")); // 가수명
                                                                                                // /저작자
                                                                                                // /저자
          dto.setSrchYymm(ServletRequestUtils.getStringParameter(request, "srchYymm")); // 방송년도 /출판년도
                                                                                        // /발행년도

          ModelAndView mv = new ModelAndView("inmtPrps/inmtPrpsSucc");
          mv.addObject("srchParam", dto);

          return mv;
     }

     // 보상금신청 수정저장
     public ModelAndView inmtPrpsUpdate(HttpServletRequest request, HttpServletResponse response) throws Exception {
          System.out.println("##############################");
          System.out.println("inmtPrpsUpdate");
          System.out.println("##############################");
          System.out.println("DIVS			=" + ServletRequestUtils.getStringParameter(request, "DIVS"));
          // System.out.println("srchSdsrName
          // ="+ServletRequestUtils.getStringParameter(request, "srchSdsrName"));
          // System.out.println("srchMuciName
          // ="+ServletRequestUtils.getStringParameter(request, "srchMuciName"));
          // System.out.println("srchYymm
          // ="+ServletRequestUtils.getStringParameter(request, "srchYymm"));
          // System.out.println("##############################");

          CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
          MultipartHttpServletRequest multipartRequest;
          multipartRequest = multipartResolver.resolveMultipart(request);

          RghtPrps dto = new RghtPrps();

          String DIVS = ServletRequestUtils.getStringParameter(multipartRequest, "DIVS", "B");
          String PRPS_MAST_KEY = ServletRequestUtils.getStringParameter(multipartRequest, "PRPS_MAST_KEY");
          String PRPS_SEQN = ServletRequestUtils.getStringParameter(multipartRequest, "PRPS_SEQN");
          String mode = ServletRequestUtils.getStringParameter(multipartRequest, "mode", "U");

          dto.setSrchPrpsDivs(ServletRequestUtils.getStringParameter(multipartRequest, "srchPrpsDivs"));
          dto.setSrchStartDate(ServletRequestUtils.getStringParameter(multipartRequest, "srchStartDate"));
          dto.setSrchEndDate(ServletRequestUtils.getStringParameter(multipartRequest, "srchEndDate"));
          dto.setSrchTitle(ServletRequestUtils.getStringParameter(multipartRequest, "srchTitle"));
          dto.setNowPage(ServletRequestUtils.getIntParameter(multipartRequest, "nowPage"));

          dto.setDIVS(DIVS);
          dto.setPRPS_MAST_KEY(PRPS_MAST_KEY);
          dto.setPRPS_SEQN(PRPS_SEQN);

          // --기초 정보들 start

          // ----동시신청 정보 start
          String prpsDoblCode = ServletRequestUtils.getStringParameter(multipartRequest, "prpsDoblCode", ""); // 동시신청
                                                                                                              // 구분코드
          String prpsDoblKey = ServletRequestUtils.getStringParameter(multipartRequest, "prpsDoblKey", ""); // 동시신청 구분코드
          String rghtPrpsMastKey = ServletRequestUtils.getStringParameter(multipartRequest, "rghtPrpsMastKey", ""); // 저작권찾기
                                                                                                                    // 마스터키
          // ----동시신청 정보 end

          // ----선택 보상금 정보 start
          String[] prpsIdnt = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectInmtSeqn"); // 선택저작물
                                                                                                               // seq값
          String[] prpsDivs = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectPrpsDivs"); // 신청구분값
                                                                                                               // B,S,L
          String[] prpsIdntCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectPrpsIdntCode");
          String[] kapp = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectKapp");
          String[] fokapo = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectFokapo");
          String[] krtra = ServletRequestUtils.getStringParameters(multipartRequest, "hddnSelectKrtra");
          // ----선택 보상금 정보 end

          // ----신청자 정보 start
          String userIdnt = ServletRequestUtils.getStringParameter(multipartRequest, "hddnUserIdnt");
          String prpsName = ServletRequestUtils.getStringParameter(multipartRequest, "txtPrpsName");
          String prpsDesc = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtareaPrpsDesc"));
          String homeTelxNumb = ServletRequestUtils.getStringParameter(multipartRequest, "txtHomeTelxNumb");
          String busiTelxNumb = ServletRequestUtils.getStringParameter(multipartRequest, "txtBusiTelxNumb");
          String moblPhon = ServletRequestUtils.getStringParameter(multipartRequest, "txtMoblPhon");
          String faxxNumb = ServletRequestUtils.getStringParameter(multipartRequest, "txtFaxxNumb");
          String mail = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtMail"));
          String homeAddr = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtHomeAddr"));
          String busiAddr = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtBusiAddr"));
          System.out.println("###4444#################################" + busiAddr + "###############################################");
          String bankName = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtBankName"));
          String acctNumb = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtAcctNumb"));
          String dptr = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtDptr"));

          String rgstDttm = ServletRequestUtils.getStringParameter(multipartRequest, "hddnRgstDttm");

          String insertKapp = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnInsertKapp"));
          String insertFokapo = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnInsertFokapo"));
          String insertKrtra = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnInsertKrtra"));
          // ----신청자 정보 end

          // ----음제협에서 사용할 정보 start
          String[] kappDiskName = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappDiskName");
          String[] kappCdCode = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappCdCode");
          String[] kappSongName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKappSongName");
          String[] kappPrpsIdnt = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKappPrpsIdnt");
          String[] kappPrpsIdntCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKappPrpsIdntCode");
          String[] kappSnerName = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappSnerName");
          String[] kappDateIssu = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappDateIssu");
          String[] kappNatuName = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappNatuName");
          String[] kappRghtGrnd = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappRghtGrnd");
          String[] kappLyriWrtr = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappLyriWrtr");
          String[] kappComsWrtr = ServletRequestUtils.getStringParameters(multipartRequest, "txtKappComsWrtr");
          String[] kappMuscGnre = ServletRequestUtils.getStringParameters(multipartRequest, "selKappMuscGnre");
          // ----음제협에서 사용할 정보 end

          // ----음실연에서 사용할 정보 start
          String fokapoPemrRlnm = ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoPemrRlnm");
          String fokapoPemrStnm = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoPemrStnm"));
          String fokapoGrupName = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoGrupName"));
          String fokapoSesdNumb = ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoResdNumb");
          String fokapoSesdNumb_1 = ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoResdNumb_1");
          String fokapoSesdNumb_2 = ServletRequestUtils.getStringParameter(multipartRequest, "txtFokapoResdNumb_2");
          String[] fokapoPrpsIdnt = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFokapoPrpsIdnt");
          String[] fokapoPrpsIdntCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFokapoPrpsIdntCode");
          String[] fokapoPdtnName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFokapoPdtnName");
          String[] fokapoSnerName = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoSnerName");
          String[] fokapoAbumName = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoAbumName");
          String[] fokapoAbumDateIssu = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoAbumDateIssu");
          String[] fokapoPemsKind = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoPemsKind");
          String[] fokapoPemsDesc = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoPemsDesc");
          String[] fokapoCtbtRate = ServletRequestUtils.getStringParameters(multipartRequest, "txtFokapoCtbtRate");
          // ----음실연에서 사용할 정보 end

          // ----복전협에서 사용할 정보 start
          String[] krtraPrpsIdnt = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraPrpsIdnt");
          String[] krtraPrpsDivs = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraPrpsDivs");
          String[] krtraPrpsIdntCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraPrpsIdntCode");
          String krtraRtpsRlnm = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraPemrRlnm"));
          String krtraRtpsStnm = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraPemrStnm"));
          String krtraRtpsCrnm = XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraGrupName"));
          String krtraResnNumb = ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraResdNumb");
          String krtraResnNumb_1 = ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraResdNumb_1");
          String krtraResnNumb_2 = ServletRequestUtils.getStringParameter(multipartRequest, "txtKrtraResdNumb_2");

          String[] krtraCoprKind = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraCoprKind");
          String[] krtraWorkName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraWorkName");
          String[] krtraWrtrName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraWrtrName");
          String[] krtraScyr = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraScyr");
          String[] krtraSctr = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraSctr");
          String[] krtraSjet = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraSjet");
          String[] krtraBookCncn = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraBookCncn");
          String[] krtraWorkDivs = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraWorkDivs");
          String[] krtraUsexType = ServletRequestUtils.getStringParameters(multipartRequest, "hddnKrtraUsexType");
          // ----복전협에서 사용할 정보 end

          // ----첨부파일 관련정보 start
          int fileCnt = ServletRequestUtils.getIntParameter(multipartRequest, "hddnFileCnt", 0);
          String[] fileOrgnCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFileOrgnCode");
          String[] fileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFileName");

          String[] getFileOrgnCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileOrgnCode");
          String[] getRealFileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetRealFileName");
          String[] getFilePath = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFilePath");
          String[] getFileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileName");
          String[] getFileSize = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileSize");
          // ----첨부파일 관련정보 end
          // --기초 정보들 end

          // 수정처리 전에 현재 최대상태값을 확인
          String maxDealStat = inmtPrpsService.getMaxDealStat(dto);

          // 수정처리 - 최대상태값이 1 신청인 경우
          if (maxDealStat.equals("1")) {
               // System.out.println("##############################");
               // System.out.println("## DELETE ML_KAPP_PRPS");
               // System.out.println("##############################");
               inmtPrpsService.inmtKappDelete(dto); // DELETE ML_KAPP_PRPS

               // System.out.println("##############################");
               // System.out.println("## DELETE ML_FOKAPO_PRPS");
               // System.out.println("##############################");
               inmtPrpsService.inmtFokapoDelete(dto); // DELETE ML_FOKAPO_PRPS

               // System.out.println("##############################");
               // System.out.println("## DELETE ML_KRTRA_PRPS");
               // System.out.println("##############################");
               inmtPrpsService.inmtKrtraDelete(dto); // DELETE ML_KRTRA_PRPS

               // System.out.println("##############################");
               // System.out.println("## DELETE ML_PRPS_RSLT");
               // System.out.println("##############################");
               inmtPrpsService.inmtPrpsRsltDelete(dto); // DELETE ML_PRPS_RSLT

               // System.out.println("##############################");
               // System.out.println("## DELETE ML_PRPS");
               // System.out.println("##############################");
               inmtPrpsService.inmtPrpsDelete(dto); // DELETE ML_PRPS

               // 선택저장물 저장-------------------
               int prpsSeqnMax = 0;

               int bPrpsSeqnMax = 0;
               int sPrpsSeqnMax = 0;
               int lPrpsSeqnMax = 0;

               int iBcnt = 0;
               int iScnt = 0;
               int iLcnt = 0;
               int intiB2cnt = 1;
               int iFiCnt = 0;

               // ----등록신청이 오프라인이나 첨부파일인지 확인 start
               String trst203 = ServletRequestUtils.getStringParameter(multipartRequest, "hddnTrst203");
               String trst202 = ServletRequestUtils.getStringParameter(multipartRequest, "hddnTrst202");
               String trst205 = ServletRequestUtils.getStringParameter(multipartRequest, "hddnTrst205");
               String trst205_2 = ServletRequestUtils.getStringParameter(multipartRequest, "hddnTrst205_2");

               String[] arrTrstOrgnCode = new String[4];
               String[] arrOffxLineRecp = new String[4];
               String[] arrTrstAttchfileYn = new String[4];
               int arrCnt = 0;

               if (trst203 != null && trst203.equals("1")) {
                    arrTrstOrgnCode[arrCnt] = "203";
                    String chkKappOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnKappOff")); // offline
                                                                                                                                         // 접수여부
                    if (!chkKappOff.equals("Y")) {
                         arrOffxLineRecp[arrCnt] = "N";
                         arrTrstAttchfileYn[arrCnt] = "Y";
                    } else {
                         arrOffxLineRecp[arrCnt] = "Y";
                         arrTrstAttchfileYn[arrCnt] = "N";
                    }
                    arrCnt++;
               }
               if (trst202 != null && trst202.equals("1")) {
                    arrTrstOrgnCode[arrCnt] = "202";
                    String chkFokapoOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnFokapoOff")); // offline
                                                                                                                                             // 접수여부
                    if (!chkFokapoOff.equals("Y")) {
                         arrOffxLineRecp[arrCnt] = "N";
                         arrTrstAttchfileYn[arrCnt] = "Y";
                    } else {
                         arrOffxLineRecp[arrCnt] = "Y";
                         arrTrstAttchfileYn[arrCnt] = "N";
                    }
                    arrCnt++;
               }
               if (trst205 != null && trst205.equals("1")) {
                    arrTrstOrgnCode[arrCnt] = "205";
                    String chkKrtraOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnKrtraOff")); // offline
                                                                                                                                           // 접수여부
                    if (!chkKrtraOff.equals("Y")) {
                         arrOffxLineRecp[arrCnt] = "N";
                         arrTrstAttchfileYn[arrCnt] = "Y";
                    } else {
                         arrOffxLineRecp[arrCnt] = "Y";
                         arrTrstAttchfileYn[arrCnt] = "N";
                    }
                    arrCnt++;
               }
               if (trst205_2 != null && trst205_2.equals("1")) {
                    arrTrstOrgnCode[arrCnt] = "205_2";
                    String chkKrtraOff = StringUtil.nullToEmpty(ServletRequestUtils.getStringParameter(multipartRequest, "hddnKrtraOff")); // offline
                                                                                                                                           // 접수여부
                    if (!chkKrtraOff.equals("Y")) {
                         arrOffxLineRecp[arrCnt] = "N";
                         arrTrstAttchfileYn[arrCnt] = "Y";
                    } else {
                         arrOffxLineRecp[arrCnt] = "Y";
                         arrTrstAttchfileYn[arrCnt] = "N";
                    }
                    arrCnt++;
               }
               // ----등록신청이 오프라인이나 첨부파일인지 확인 end

               // --신청내역 등록
               Map map = new HashMap();
               map.put("userIdnt", userIdnt);
               map.put("prpsName", prpsName);
               map.put("prpsDesc", prpsDesc);
               map.put("homeTelxNumb", homeTelxNumb);
               map.put("busiTelxNumb", busiTelxNumb);
               map.put("moblPhon", moblPhon);
               map.put("faxxNumb", faxxNumb);
               map.put("mail", mail);
               map.put("homeAddr", homeAddr);
               map.put("busiAddr", busiAddr);
               map.put("bankName", bankName);
               map.put("acctNumb", acctNumb);
               map.put("dptr", dptr);

               map.put("prpsDoblCode", prpsDoblCode);
               map.put("rghtPrpsMastKey", rghtPrpsMastKey);

               for (int te = 0; te < prpsIdnt.length; te++) {

                    map.put("prpsIdnt", prpsIdnt[te]);
                    map.put("prpsIdntCode", prpsIdntCode[te]);
                    map.put("prpsMastKey", PRPS_MAST_KEY);

                    int chkCnt = 0; // 선택저작물에 대한 신탁별 데이타유무 체크값.

                    if ((prpsDivs[te].equals("B") && iBcnt == 0) || (prpsDivs[te].equals("S") && iScnt == 0) || (prpsDivs[te].equals("L") && iLcnt == 0)) {

                         prpsSeqnMax = inmtPrpsService.prpsSeqnMax(); // MAX(ML_PRPS.PRPS_SEQN)+1 set

                         map.put("prpsSeqnMax", "" + prpsSeqnMax);
                         map.put("prpsDivs", prpsDivs[te]);
                         map.put("rgstDttm", rgstDttm);

                         // System.out.println("##############################");
                         // System.out.println("## INSERT ML_PRPS");
                         // System.out.println("##############################");
                         inmtPrpsService.inmtPspsInsert(map); // insert ML_PRPS

                         if (prpsDivs[te].equals("B"))
                              bPrpsSeqnMax = prpsSeqnMax;
                         else if (prpsDivs[te].equals("S"))
                              sPrpsSeqnMax = prpsSeqnMax;
                         else if (prpsDivs[te].equals("L"))
                              lPrpsSeqnMax = prpsSeqnMax;
                    }

                    if (prpsDivs[te].equals("B")) {
                         iBcnt++;
                         prpsSeqnMax = bPrpsSeqnMax;
                    } else if (prpsDivs[te].equals("S")) {
                         iScnt++;
                         prpsSeqnMax = sPrpsSeqnMax;
                    } else if (prpsDivs[te].equals("L")) {
                         iLcnt++;
                         prpsSeqnMax = lPrpsSeqnMax;
                    }

                    for (int k = 0; k < arrCnt; k++) {
                         Map trstMap = new HashMap();
                         map.put("trstOrgnCode", arrTrstOrgnCode[k]);
                         map.put("trstAttchfileYn", arrTrstAttchfileYn[k]);
                         map.put("offxLineRecp", arrOffxLineRecp[k]);

                         String trstOrgnCode = arrTrstOrgnCode[k]; // 신탁구분코드
                         String attcYsNo = "N";
                         String offxLineRecp = "N";

                         if (arrTrstAttchfileYn[k] != null)
                              attcYsNo = arrTrstAttchfileYn[k];
                         if (arrOffxLineRecp[k] != null)
                              offxLineRecp = arrOffxLineRecp[k];

                         int iCnt = 0;

                         // ----음제협 신청내역 등록 start
                         if (insertKapp.equals("Y") && trstOrgnCode.equals("203") && kapp[te].equals("Y")) {
                              for (int km = 0; km < kappPrpsIdnt.length; km++) {
                                   Map kappMap = new HashMap();
                                   System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>음제협 신청>>>>>>>>>>>>>>>>>>>");
                                   kappMap.put("kappSongName", kappSongName[km]);
                                   kappMap.put("kappPrpsIdnt", kappPrpsIdnt[km]);
                                   kappMap.put("kappPrpsIdntCode", kappPrpsIdntCode[km]);
                                   kappMap.put("kappDiskName", XssUtil.unscript(kappDiskName[km]));
                                   kappMap.put("kappCdCode", XssUtil.unscript(kappCdCode[km]));
                                   kappMap.put("kappSnerName", XssUtil.unscript(kappSnerName[km]));
                                   kappMap.put("kappDateIssu", kappDateIssu[km]);
                                   kappMap.put("kappNatuName", XssUtil.unscript(kappNatuName[km]));
                                   kappMap.put("kappRghtGrnd", XssUtil.unscript(kappRghtGrnd[km]));
                                   kappMap.put("kappLyriWrtr", XssUtil.unscript(kappLyriWrtr[km]));
                                   kappMap.put("kappComsWrtr", XssUtil.unscript(kappComsWrtr[km]));
                                   kappMap.put("kappMuscGnre", kappMuscGnre[km]);
                                   kappMap.put("prpsMastKey", PRPS_MAST_KEY);
                                   kappMap.put("userIdnt", userIdnt);
                                   kappMap.put("prpsSeqnMax", "" + prpsSeqnMax);
                                   kappMap.put("trstOrgnCode", trstOrgnCode);
                                   kappMap.put("prpsIdnt", prpsIdnt[te]); // 선택저작물 seq값
                                   kappMap.put("prpsDivs", prpsDivs[te]); // 신청구분값 B,S,L
                                   kappMap.put("attcYsNo", attcYsNo);
                                   kappMap.put("offxLineRecp", offxLineRecp);
                                   kappMap.put("rgstDttm", rgstDttm);

                                   if (kappMap.get("kappPrpsIdnt").equals(prpsIdnt[te])) {
                                        iCnt++;
                                        // System.out.println("##############################");
                                        // System.out.println("## INSERT ML_KAPP_PRPS");
                                        // System.out.println("##############################");
                                        inmtPrpsService.inmtKappInsert(kappMap);
                                   }
                              }
                              System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>음협 신청202변수str>>>>>>>>>>>>>>>>>>>");

                              System.out.println("insertFokapo" + insertFokapo);
                              System.out.println("trstOrgnCode" + trstOrgnCode);
                              System.out.println("fokapo[te]" + fokapo[te]);

                              System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>음협 신청202변수end>>>>>>>>>>>>>>>>>>>");
                         }
                         // ----음제협 신청내역 등록 end

                         // ----음실연 신청내역 등록 start

                         else if (insertFokapo.equals("Y") && trstOrgnCode.equals("202") && fokapo[te].equals("Y")) {
                              for (int km = 0; km < fokapoPrpsIdnt.length; km++) {
                                   System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>음협 신청>>>>>>>>>>>>>>>>>>>");
                                   Map fokapoMap = new HashMap();
                                   fokapoMap.put("fokapoPemrRlnm", fokapoPemrRlnm);
                                   fokapoMap.put("fokapoPemrStnm", fokapoPemrStnm);
                                   fokapoMap.put("fokapoGrupName", fokapoGrupName);
                                   fokapoMap.put("fokapoSesdNumb", fokapoSesdNumb);
                                   fokapoMap.put("fokapoSesdNumb", fokapoSesdNumb_1 + fokapoSesdNumb_2);
                                   fokapoMap.put("fokapoPrpsIdnt", fokapoPrpsIdnt[km]);
                                   fokapoMap.put("fokapoPrpsIdntCode", fokapoPrpsIdntCode[km]);
                                   fokapoMap.put("fokapoPdtnName", fokapoPdtnName[km]);
                                   fokapoMap.put("fokapoSnerName", XssUtil.unscript(fokapoSnerName[km]));
                                   fokapoMap.put("fokapoAbumName", XssUtil.unscript(fokapoAbumName[km]));
                                   fokapoMap.put("fokapoAbumDateIssu", fokapoAbumDateIssu[km]);
                                   fokapoMap.put("fokapoPemsKind", XssUtil.unscript(fokapoPemsKind[km]));
                                   fokapoMap.put("fokapoPemsDesc", XssUtil.unscript(fokapoPemsDesc[km]));
                                   fokapoMap.put("fokapoCtbtRate", fokapoCtbtRate[km]);

                                   fokapoMap.put("prpsMastKey", PRPS_MAST_KEY);
                                   fokapoMap.put("userIdnt", userIdnt);
                                   fokapoMap.put("prpsSeqnMax", "" + prpsSeqnMax);
                                   fokapoMap.put("trstOrgnCode", trstOrgnCode);
                                   fokapoMap.put("prpsIdnt", prpsIdnt[te]); // 선택저작물 seq값
                                   fokapoMap.put("prpsDivs", prpsDivs[te]); // 신청구분값 B,S,L
                                   fokapoMap.put("attcYsNo", attcYsNo);
                                   fokapoMap.put("offxLineRecp", offxLineRecp);
                                   fokapoMap.put("rgstDttm", rgstDttm);

                                   if (fokapoMap.get("fokapoPrpsIdnt").equals(prpsIdnt[te])) {
                                        iCnt++;
                                        // System.out.println("##############################");
                                        // System.out.println("## INSERT ML_FOKAPO_PRPS");
                                        // System.out.println("##############################");
                                        inmtPrpsService.inmtFokapoInsert(fokapoMap);
                                   }
                              }
                         }
                         // ----음실연 신청내역 등록 end

                         // ----복전협 신청내역 등록 start
                         // ------교과용 start
                         else if (insertKrtra.equals("Y") && trstOrgnCode.equals("205") && krtra[te].equals("Y") && prpsDivs[te].equals("S")) {
                              for (int km = 0; km < krtraPrpsIdnt.length; km++) {

                                   Map krtraMap = new HashMap();

                                   krtraMap.put("krtraPrpsIdnt", krtraPrpsIdnt[km]);
                                   krtraMap.put("krtraPrpsDivs", krtraPrpsDivs[km]);
                                   krtraMap.put("krtraPrpsIdntCode", krtraPrpsIdntCode[km]);
                                   krtraMap.put("krtraRtpsRlnm", krtraRtpsRlnm);
                                   krtraMap.put("krtraRtpsStnm", krtraRtpsStnm);
                                   krtraMap.put("krtraRtpsCrnm", krtraRtpsCrnm);
                                   krtraMap.put("krtraResnNumb", krtraResnNumb);
                                   krtraMap.put("krtraResnNumb", krtraResnNumb_1 + krtraResnNumb_2);

                                   krtraMap.put("krtraCoprKind", krtraCoprKind[km]);
                                   krtraMap.put("krtraWorkName", krtraWorkName[km]);
                                   krtraMap.put("krtraWrtrName", krtraWrtrName[km]);
                                   krtraMap.put("krtraScyr", krtraScyr[km]);
                                   krtraMap.put("krtraSctr", krtraSctr[km]);
                                   krtraMap.put("krtraSjet", krtraSjet[km]);
                                   krtraMap.put("krtraBookCncn", krtraBookCncn[km]);
                                   krtraMap.put("krtraWorkDivs", krtraWorkDivs[km]);
                                   krtraMap.put("krtraUsexType", krtraWorkDivs[km]); // 동일 컬럼사용으로 인한 처리.

                                   krtraMap.put("prpsMastKey", PRPS_MAST_KEY);
                                   krtraMap.put("userIdnt", userIdnt);
                                   krtraMap.put("prpsSeqnMax", "" + prpsSeqnMax);
                                   krtraMap.put("trstOrgnCode", trstOrgnCode);
                                   krtraMap.put("prpsIdnt", prpsIdnt[te]); // 선택저작물 seq값
                                   krtraMap.put("prpsDivs", prpsDivs[te]); // 신청구분값 B,S,L
                                   krtraMap.put("attcYsNo", attcYsNo);
                                   krtraMap.put("offxLineRecp", offxLineRecp);
                                   krtraMap.put("rgstDttm", rgstDttm);

                                   if (krtraMap.get("krtraPrpsIdnt").equals(prpsIdnt[te]) && krtraMap.get("krtraPrpsDivs").equals("S")) {
                                        iCnt++;
                                        // System.out.println("##############################");
                                        // System.out.println("## INSERT ML_KRTRA_PRPS");
                                        // System.out.println("##############################");
                                        inmtPrpsService.inmtKrtraInsert(krtraMap);
                                   }
                              }
                         }
                         // ------교과용 end

                         // ------도서관 start
                         else if (trstOrgnCode.equals("205_2") && krtra[te].equals("Y") && prpsDivs[te].equals("L")) {
                              for (int km = 0; km < krtraPrpsIdnt.length; km++) {
                                   Map krtraMap = new HashMap();
                                   krtraMap.put("krtraPrpsIdnt", krtraPrpsIdnt[km]);
                                   krtraMap.put("krtraPrpsDivs", krtraPrpsDivs[km]);
                                   krtraMap.put("krtraPrpsIdntCode", krtraPrpsIdntCode[km]);
                                   krtraMap.put("krtraRtpsRlnm", krtraRtpsRlnm);
                                   krtraMap.put("krtraRtpsStnm", krtraRtpsStnm);
                                   krtraMap.put("krtraRtpsCrnm", krtraRtpsCrnm);
                                   krtraMap.put("krtraResnNumb", krtraResnNumb);
                                   krtraMap.put("krtraResnNumb", krtraResnNumb_1 + krtraResnNumb_2);

                                   krtraMap.put("krtraWorkName", krtraWorkName[km]);
                                   krtraMap.put("krtraWrtrName", krtraWrtrName[km]);
                                   krtraMap.put("krtraBookCncn", krtraBookCncn[km]);
                                   krtraMap.put("krtraSctr", krtraSctr[km]);
                                   krtraMap.put("krtraUsexType", krtraUsexType[km]);

                                   krtraMap.put("prpsMastKey", PRPS_MAST_KEY);
                                   krtraMap.put("userIdnt", userIdnt);
                                   krtraMap.put("prpsSeqnMax", "" + prpsSeqnMax);
                                   krtraMap.put("trstOrgnCode", "205");
                                   krtraMap.put("prpsIdnt", prpsIdnt[te]); // 선택저작물 seq값
                                   krtraMap.put("prpsDivs", prpsDivs[te]); // 신청구분값 B,S,L
                                   krtraMap.put("attcYsNo", attcYsNo);
                                   krtraMap.put("offxLineRecp", offxLineRecp);
                                   krtraMap.put("rgstDttm", rgstDttm);

                                   if (krtraMap.get("krtraPrpsIdnt").equals(prpsIdnt[te]) && krtraMap.get("krtraPrpsDivs").equals("L")) {
                                        iCnt++;
                                        // System.out.println("##############################");
                                        // System.out.println("## INSERT ML_KRTRA_PRPS");
                                        // System.out.println("##############################");
                                        inmtPrpsService.inmtKrtraInsert(krtraMap);
                                   }
                              }
                         }
                         // ------도서관 end
                         // ----복전협 신청내역 등록 end

                         // ----신청처리결과 등록 start
                         if (iCnt > 0) {
                              map.put("prpsSeqnMax", "" + prpsSeqnMax);
                              map.put("prpsDivs", prpsDivs[te]);
                              map.put("seqn", te + 1); // 저작물 순번

                              if (trstOrgnCode.equals("205_2") && prpsDivs[te].equals("L"))
                                   map.put("trstOrgnCode", "205");

                              // 신탁단체별 신청처리결과 기본값 insert
                              // System.out.println("##############################");
                              // System.out.println("## INSERT ML_PRPS_RSLT");
                              // System.out.println("##############################");
                              inmtPrpsService.inmtPrpsRsltInsert(map); // insert ML_PRPS_RSLT
                         }
                         // ----신청처리결과 등록 end
                    }

                    // ------화면에서 넘어온 값
                    String[] getPrpsMastKey = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFilePrpsMastKey");
                    String[] getPrpsSeqn = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFilePrpsSeqn");
                    String[] getAttcSeqn = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileAttcSeqn");

                    // ------실제 db에 있는 값
                    List fileList = rsltInqrService.inmtFileList(dto);

                    // ----첨부파일등록 start
                    for (int k = 0; k < arrCnt; k++) {
                         if (!arrOffxLineRecp[k].equals("Y")) {

                              // ----첨부파일삭제 start
                              for (int mm = 0; mm < fileList.size(); mm++) {
                                   int fileExist = 0;
                                   InmtPrps fileInfo = (InmtPrps) fileList.get(mm);
                                   for (int lm = 0; lm < getPrpsMastKey.length; lm++) {
                                        // 해당 파일이 존재 하는지 조사
                                        if (fileInfo.getPRPS_MAST_KEY().equals(getPrpsMastKey[lm]) && fileInfo.getPRPS_SEQN().equals(getPrpsSeqn[lm])
                                                  && fileInfo.getATTC_SEQN().equals(getAttcSeqn[lm])) {
                                             // 화면에서 넘어온 값이랑 db에 있는 값이 같으면 해당 첨부파일은 수정이 안된 것
                                             fileExist++;
                                        }
                                   }

                                   // 없다면 삭제된 파일이므로 delete
                                   if (fileExist < 1) {
                                        inmtPrpsService.inmtFileDeleteOne(fileInfo);
                                   }
                              }
                              // ----첨부파일삭제 end

                              // File Upload
                              Iterator fileNameIterator = multipartRequest.getFileNames();
                              List newFileList = new ArrayList();

                              while (fileNameIterator.hasNext()) {
                                   MultipartFile multiFile = multipartRequest.getFile((String) fileNameIterator.next());
                                   if (multiFile.getSize() > 0) {
                                        PrpsAttc prpsAttc = FileUploadUtil.uploadPrpsFiles(multiFile, realUploadPath);
                                        newFileList.add(prpsAttc);
                                   }
                              }

                              PrpsAttc prpsAttc = new PrpsAttc();
                              PrpsAttc prpsAttcFile = new PrpsAttc();
                              System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                              System.out.println("fileStep01");
                              System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                              for (int fi = 0; fi < fileOrgnCode.length; fi++) {
                                   prpsAttc = (PrpsAttc) newFileList.get(fi);
                                   prpsAttcFile.setREAL_FILE_NAME(prpsAttc.getREAL_FILE_NAME());
                                   prpsAttcFile.setFILE_PATH(prpsAttc.getFILE_PATH());
                                   if (fileOrgnCode[fi].equals("205_2")) {
                                        prpsAttcFile.setTRST_ORGN_CODE("205");
                                   } else {
                                        prpsAttcFile.setTRST_ORGN_CODE(fileOrgnCode[fi]);
                                   }

                                   prpsAttcFile.setPRPS_DIVS(prpsDivs[te]);
                                   prpsAttcFile.setUSER_IDNT(userIdnt);
                                   prpsAttcFile.setPRPS_MAST_KEY(PRPS_MAST_KEY);
                                   prpsAttcFile.setFILE_SIZE(prpsAttc.getFILE_SIZE());
                                   prpsAttcFile.setFILE_NAME(fileName[fi]);

                                   rghtPrpsService.insertPrpsAttc(prpsAttcFile);
                                   // ----첨부파일등록 end
                              }
                         } else {
                              // ----첨부파일삭제 start
                              for (int mm = 0; mm < fileList.size(); mm++) {
                                   int fileExist = 0;
                                   InmtPrps fileInfo = (InmtPrps) fileList.get(mm);
                                   inmtPrpsService.inmtFileDeleteOne(fileInfo);
                              }
                              // ----첨부파일등록 end
                         }
                    }
               }
               // ----------------------------------

               // 접속정보 저장---------------------
               map.put("connUrl", "보상금신청 수정");
               inmtPrpsService.insertConnInfo(map);
               // ----------------------------------

          }

          ModelAndView mv = new ModelAndView("rsltInqr/inmtPrpsSucc");
          mv.addObject("srchParam", dto);
          mv.addObject("mode", mode);

          return mv;
     }

     // 보상금신청(교과용) 이미지 미리보기
     public ModelAndView subImgPrev(HttpServletRequest request, HttpServletResponse response) throws Exception {

          String filePath = ServletRequestUtils.getStringParameter(request, "filePath");
          String imgTitle = ServletRequestUtils.getStringParameter(request, "imgTitle");
          ModelAndView mv = new ModelAndView("inmtPrps/subjImgPrev");
          mv.addObject("filePath", filePath);
          mv.addObject("imgTitle", imgTitle);
          return mv;

     }

     // 보상금신청 확인화면
     public ModelAndView inmtPrpsHistList(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          RghtPrps rghtPrps = new RghtPrps();

          bind(request, rghtPrps);

          List list = inmtPrpsService.inmtPrpsHistList(rghtPrps);

          ModelAndView mv = new ModelAndView("inmtPrps/inmtPrpsHist", "inmtPrpsHist", list);

          mv.addObject("totalrow", list.size());
          mv.addObject("DIVS", rghtPrps.getDIVS());

          return mv;
     }

     // 보상금 상위 10개
     public ModelAndView inmtPrpsTopList(HttpServletRequest request, HttpServletResponse respone) throws Exception {
          System.out.println("inmtPrpsTopList");
          String limit = request.getParameter("limit");

          System.out.println("inmtPrpsTopList limit :" + limit);

          Map<String, Object> param = new HashMap<String, Object>();
          param.put("limit", limit);

          // 보상금 전체 금액
          // String allt_amnt = inmtPrpsService.getAlltAmnt(param);

          String allt_amnt = "0";

          List amntList = inmtPrpsService.inmtPrpsAmntList(param);
          List list = inmtPrpsService.inmtPrpsTopList(param);

          ModelAndView mv = new ModelAndView("inmtPrps/inmtPrpsTopList", "allt_amnt", allt_amnt);

          mv.addObject("amntList", amntList);
          mv.addObject("list", list);

          return mv;
     }

     // str1 널이면 str2 반환. 아니면 str1 반환
     public String strNvl(String str1, String str2) {

          StringUtil sUtil = new StringUtil();

          str1 = sUtil.nullToEmpty(str1);
          str2 = sUtil.nullToEmpty(str2);

          if (str1.trim().length() > 0)
               return str1;
          else
               return str2;
     }

     // str1 널이면 str2 반환. 아니면 str1 반환
     public String strNvl(String str1, Object obj2) {

          StringUtil sUtil = new StringUtil();

          String str2 = "";

          if (obj2 != null) {
               str2 = obj2.toString();
          }

          return strNvl(str1, str2);

     }
}
