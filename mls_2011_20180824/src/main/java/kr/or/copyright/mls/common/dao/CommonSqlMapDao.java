package kr.or.copyright.mls.common.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
 
public class CommonSqlMapDao extends SqlMapClientDaoSupport implements CommonDao {
	
	// 내권리찾기 사용자 조회 
	public List getUserInfo(Map map) {
		return getSqlMapClientTemplate().queryForList("Common.getUserInfo",map);
	}	
	
	//	 내권리찾기-보상회원 조회 
	public List getInmtUserInfo(Map map) {
		return getSqlMapClientTemplate().queryForList("Common.getInmtUserInfo",map);
	}	

	// 첨부파일 Main(ML_FILE) 신규SEQ 조회
	public int getNewAttcSeqn() {System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@ CommonSqlMapDao");
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("Common.getNewAttcSeqn"));
	}
	
	public List getMgntMail(Map map){
	    return getSqlMapClientTemplate().queryForList("Common.getMgntMail",map);
	}
}
