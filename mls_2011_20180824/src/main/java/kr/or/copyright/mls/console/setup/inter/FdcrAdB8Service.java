package kr.or.copyright.mls.console.setup.inter;

import java.util.Map;

public interface FdcrAdB8Service{
	
	/**
	 * 보고저작물 등록기간 관리
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB8List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보고저작물 등록기간 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdB8Update1( Map<String, Object> commandMap ) throws Exception;
}
