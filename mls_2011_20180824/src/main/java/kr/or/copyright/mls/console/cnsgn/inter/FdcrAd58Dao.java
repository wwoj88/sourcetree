package kr.or.copyright.mls.console.cnsgn.inter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tobesoft.platform.data.Dataset;

public interface FdcrAd58Dao{

	public List reportListTotalRowMusic( Map map ) throws Exception;

	public List reportListTotalRowMusic2( Map map ) throws Exception;

	public List reportListMusic( Map map ) throws Exception;

	public List reportListTotalRowBook( Map map ) throws Exception;

	public List reportListTotalRowBook2( Map map ) throws Exception;

	public List reportListBook( Map map ) throws Exception;

	public List reportListAgentTotalRow( Map map ) throws Exception;

	public List reportListAgentTotalRow2( Map map ) throws Exception;

	public List reportListAgent( Map map ) throws Exception;

	public List getYearMonth() throws Exception;

	public void targetReflash( AdminWorksRegistation dto ) throws Exception;

	public void targetInsertMusic( Map map ) throws Exception;

	public List targetMusicList( Map map ) throws Exception;

	public void targetDeleteMusic( Map map ) throws Exception;

	public void commWorksIdDelete( Map map ) throws Exception;

	public int adminCommWorksReportInsert( Map map ) throws Exception;

	public int getExistCommWorks( Map map ) throws Exception;

	public int getCommWorksId() throws Exception;

	public String commWorksInsertBacth( List list ) throws Exception;

	public String commMusicInsertBacth( List list ) throws Exception;

	public void adminCommWorksDelete( Map map ) throws Exception;

	public boolean adminCommWorksInsert( Map map ) throws Exception;

	public boolean adminCommMusicInsert( Map map ) throws Exception;

	public void adminCommWorksReportUpdate( Map map ) throws Exception;

	public List adminCommMusicUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> ds_target ) throws Exception;

	public List adminCommBookUpdate( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> ds_target ) throws Exception;

	/*
	 * public List adminCommScriptUpdate(Dataset ds_condition, Dataset
	 * ds_report, Dataset ds_target) throws Exception; public List
	 * adminCommMovieUpdate(Dataset ds_condition, Dataset ds_report, Dataset
	 * ds_target) throws Exception; public List adminCommBroadcastUpdate(Dataset
	 * ds_condition, Dataset ds_report, Dataset ds_target) throws Exception;
	 * public List adminCommNewsUpdate(Dataset ds_condition, Dataset ds_report,
	 * Dataset ds_target) throws Exception; public List
	 * adminCommArtUpdate(Dataset ds_condition, Dataset ds_report, Dataset
	 * ds_target) throws Exception; public List adminCommImageUpdate(Dataset
	 * ds_condition, Dataset ds_report, Dataset ds_target) throws Exception;
	 * public List adminCommSideUpdate(Dataset ds_condition, Dataset ds_report,
	 * Dataset ds_target) throws Exception;
	 */
	public void targetDeleteBook( Map map ) throws Exception;

	public void targetInsertBook( Map map ) throws Exception;

	public List targetBookList( Map map ) throws Exception;

	public String commBookInsertBacth( List list ) throws Exception;

	public boolean adminCommBookInsert( Map map ) throws Exception;

}
