package kr.or.copyright.mls.console.setup.inter;

import java.util.Map;


public interface FdcrAdC0Service{
	/**
	 * 상당한노력 자동화 설정 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdC0List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한노력 자동화 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdC0Update1( Map<String, Object> commandMap ) throws Exception;
}
