package kr.or.copyright.mls.adminConn.dao;

import java.util.List;
import java.util.Map;

public interface AdminConnDao {

	// 회원정보 목록조회
	public List monthList(Map map);

	// 회원정보 목록조회
	public List dayList(Map map);	
	
	// 기간별 접속통계
	public List adminStatPeriList(Map map);
	
	// 기간별 접속통계(월별)
	public List adminStatPeriMonthList(Map map);
	
	// 내권리찾기, 보상금 통계
	public List rsltList(Map map);
	
	// 저작권찾기, 보상금 기간별 통계
	public List rsltPeriList(Map map);
	
	// 접속내역
	public List userList(Map map);
	
	// 접속내역 삭제
	public void userListDelete(Map map);
	
	// 법정허락 연도별 신청/승인건수
	public List statYearApplyList(Map map);
	
	// 법정허락 연도별 저작물,실연,음반,방송,DB 신청/승인건수
	public List statYearSpplyList(Map map);
	
	// 법정허락 연도별 보상금액
	public List statYearLgmtAmntList(Map map);

	//기간별 접속통계 모바일
	public List adminStatPeriList2(Map map);

	public List adminStatPeriMonthList2(Map map);
	
	
	//법정허락 이용승인 건수 통계(년도별)
	public List statYearPrpsApplyList(Map map);
	
	//보상금공탁서 공고 건수 통계(년도별)
	public List statYearInmtList(Map map);
	
		
	//상당한노력 신청 건수 통계(년도별 - 분기별) statYearPrpsApplyList
	public List statYearQuProcList(Map map) ;

	//상당한노력 신청 건수 통계(년도별 - 장르별)
	public List statYearGenreProcList(Map map);
	
	//법정허락 대상 저작물 현황
	public List statPrpsWorksList(Map map);
	
	public List statPrpsApplyList(Map map);
	
	
}
