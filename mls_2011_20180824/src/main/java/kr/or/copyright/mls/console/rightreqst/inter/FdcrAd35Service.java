package kr.or.copyright.mls.console.rightreqst.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd35Service{

	/**
	 * 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd35List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상세 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd35View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 신청인 정보 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd35View2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 신청저작물 상세정보 팝업
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd35Pop1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 처리결과 삭제
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd35Delete1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 처리결과 저장
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd35Insert1( Map<String, Object> commandMap ) throws Exception;

}
