package kr.or.copyright.mls.console.mber;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Dao;
import kr.or.copyright.mls.console.mber.inter.LogSearchDao;
import kr.or.copyright.mls.console.mber.inter.LogSearchService;
import kr.or.copyright.mls.console.menu.inter.FdcrAdA9Dao;
@Service( "logSearchService" )
public class LogSearchServiceImp extends CommandService implements LogSearchService{

	// TODO Auto-generated method stub
	// old AdminUserDao
	@Resource( name = "logSearchDao" )
	private LogSearchDao logSearchDao;

	
	
	public void logList( Map<String, Object> commandMap ) throws Exception{

		
			// DAO호출
			int totCnt = logSearchDao.logListCount( commandMap );
			pagination( commandMap, totCnt, 0 );
			List list = (List) logSearchDao.logList( commandMap );
			System.out.println(list.toString());
			commandMap.put( "ds_list", list ); // 로그인 사용자 정보
		
	}

}

