package kr.or.copyright.mls.ajaxTest.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kr.or.copyright.mls.common.extend.RightDAOImpl;

@Repository("UserListDAO")
public class UserListSqlMapDao extends RightDAOImpl implements UserListDAO {

	private static final String NAMESPACE = "AjaxTest";
	
	@SuppressWarnings("unchecked")
	public List<String> getNameListForSuggest(String namePrefix) {
		return (List<String>)getSqlMapClientTemplate().queryForList(NAMESPACE+".getNameListForSuggest",namePrefix);
	}

}
