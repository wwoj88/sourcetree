package kr.or.copyright.mls.srch.model;

import java.util.Date;
/*
 * 검색 가중치용 20170905
 */
public class SrchStatDTO{
	private String srch_ymd;
	private String srch_word;
	private int srch_hit;
	private Date rgst_dttm;
	private int genre_cd;
	
	public String getSrch_ymd(){
		return srch_ymd;
	}
	
	public void setSrch_ymd( String srch_ymd ){
		this.srch_ymd = srch_ymd;
	}
	
	public String getSrch_word(){
		return srch_word;
	}
	
	public void setSrch_word( String srch_word ){
		this.srch_word = srch_word;
	}
	
	public int getSrch_hit(){
		return srch_hit;
	}
	
	public void setSrch_hit( int srch_hit ){
		this.srch_hit = srch_hit;
	}
	
	public Date getRgst_dttm(){
		return rgst_dttm;
	}
	
	public void setRgst_dttm( Date rgst_dttm ){
		this.rgst_dttm = rgst_dttm;
	}
	
	public int getGenre_cd(){
		return genre_cd;
	}
	
	public void setGenre_cd( int genre_cd ){
		this.genre_cd = genre_cd;
	}

	@Override
	public String toString(){
		return "SrchStatDTO [srch_ymd=" + srch_ymd + ", srch_word=" + srch_word + ", srch_hit=" + srch_hit
			+ ", rgst_dttm=" + rgst_dttm + ", genre_cd=" + genre_cd + "]";
	}
}
