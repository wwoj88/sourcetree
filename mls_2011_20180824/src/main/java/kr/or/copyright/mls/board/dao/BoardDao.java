package kr.or.copyright.mls.board.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.board.model.Board;
import kr.or.copyright.mls.board.model.BoardFile;

public interface BoardDao {

	public void insertQust(Board board);

	public int boardMax(Board board);
	
	public void insertBoardFile(BoardFile boardFile);
	
	public void updateQust(Board board);
	
	public void updateBoardFile(BoardFile boardFile);
	
	public void fileDelete(BoardFile boardFile);
	
	public void updateInqrCont(Board board);
	
	public List findBoardList(Map params);
	
	/* 정병호 추가 start */
	public List findMyQustList(Map params);
	
	public int findMyQustCount(Map params);
	
	public Board myQustBoardView(Board board);
	/* 정병호 추가 end */
	
	public Board boardView(Board board);
	
	/* 양재석 추가 start */
	public List findNotiBoardList(Map params);
	
	public Board notiBoardView(Board board);
	/* 양재석 추가 end */

	public List boardFileList(Board board);

	public int findBoardCount(Map params);
	
	public Board isValidPswd(Board board);
	
	void delete(Board board);
	
	void deleteAll(Board board);
	
	void deleteFileAll(Board board);
	
	// 커뮤니티 게시판 추가(최남식)
	public void insertComm(Board board);
	
	public int findStatBoardCount(Map params);
	
	public List findStatBoardList(Map params);
	
	public void updateStatInqrCont(Board board);
	
	public Board statBoardView(Board board);
	
	public List statBoardFileList(Board board);
}
