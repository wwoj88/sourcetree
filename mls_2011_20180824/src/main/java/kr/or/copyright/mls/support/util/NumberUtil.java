package kr.or.copyright.mls.support.util;

import java.text.NumberFormat;

/**
 * 
 * @author oss
 *
 */
public class NumberUtil {
	
	/**
	 * 숫자에 세자리마다 콤마찍기
	 * @param iNum
	 * @return
	 */
	public static String getCommaNumber(int iNum){
		NumberFormat nf = NumberFormat.getNumberInstance();
		return nf.format(iNum);
	}
	
	
	/**
	 * 숫자에 세자리마다 콤마찍기
	 * @param dNum
	 * @param iDigit 소수점자리
	 * @return
	 */
	public static String getCommaNumber(double dNum, int iDigit) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(iDigit);
		return nf.format(dNum);
		
	}
}