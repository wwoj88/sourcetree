package kr.or.copyright.mls.noneRght.controller;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.noneRght.service.NoneRghtService;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;
import kr.or.copyright.mls.support.util.DateUtil;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class NoneRghtController extends MultiActionController {

	private NoneRghtService noneRghtService;
	
	public void setNoneRghtService(NoneRghtService noneRghtService) {
		this.noneRghtService = noneRghtService;
	}
	
	public ModelAndView list(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		String DIVS = ServletRequestUtils.getStringParameter(request, "DIVS");
		String gubun = ServletRequestUtils.getStringParameter(request, "gubun", "");	// 통합검색에서 넘어온경우 : totalSearch 기본 검색어 set 하지 않는다.
		
		Date date=new Date();
		SimpleDateFormat yearForm = new SimpleDateFormat("yyyy");
		SimpleDateFormat dayForm = new SimpleDateFormat("yyyyMMdd");
		String thisYear= yearForm.format(date);
		String thisMonth = yearForm.format(date)+"0101";
		String thisDay = dayForm.format(date);
		
		ModelAndView mv = null;
		mv = new ModelAndView("noneRght/rghtMain");
		mv.addObject("DIVS", DIVS);  // M:음악 , B:도서, C:방송대본, I:이미지, V:영화, R:방송 
		mv.addObject("srchTitle", ServletRequestUtils.getStringParameter(request, "srchTitle"));  // 곡명
 		mv.addObject("srchProducer", ServletRequestUtils.getStringParameter(request, "srchProducer"));  // 제작사
		mv.addObject("srchAlbumTitle", ServletRequestUtils.getStringParameter(request, "srchAlbumTitle"));  // 앨범명
		mv.addObject("srchSinger", ServletRequestUtils.getStringParameter(request, "srchSinger"));  // 가수명
		mv.addObject("srchLicensor", ServletRequestUtils.getStringParameter(request, "srchLicensor"));  // 저작자명
		
		mv.addObject("srchNoneRole_arr", ServletRequestUtils.getStringParameters(request, "srchNoneRole_arr"))	; 	// 저작권미상항목
		mv.addObject("srchNoneName", ServletRequestUtils.getStringParameter(request, "srchNoneName"));  // 저작자명
		
		if(DIVS.equals("M") && !gubun.equals("totalSearch")){  // 음악

//			mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate",thisMonth));  // 발매일
//			mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate",thisDay));  // 발매일

			mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));  // 발매일
			mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));  // 발매일
			
			
			// ==================== 검색조건이 없는경우. start
			String srchStartDate = ServletRequestUtils.getStringParameter(request, "srchStartDate", "");	// 발매시작일
			String srchEndDate = ServletRequestUtils.getStringParameter(request, "srchEndDate", "");	// 발매종료일
			String srchTitle = ServletRequestUtils.getStringParameter(request, "srchTitle", "");	// 곡명
			String srchAlbumTitle = ServletRequestUtils.getStringParameter(request, "srchAlbumTitle", "");	//앨범;
			String srchLicensor = ServletRequestUtils.getStringParameter(request, "srchLicensor", "");	// 저작자
			String srchSinger = ServletRequestUtils.getStringParameter(request, "srchSinger", "");  // 가수명
			String srchProducer = ServletRequestUtils.getStringParameter(request, "srchProducer", "");  // 제작사
			String srchNoneRole_arr[] = ServletRequestUtils.getStringParameters(request, "srchNoneRole_arr"); 	// 저작권미상항목
			String srchNoneName = ServletRequestUtils.getStringParameter(request, "srchNoneName", "");  // 저작자명
			
			if( srchStartDate.length()==0 && srchEndDate.length() == 0 && srchTitle.length() == 0 
				 && srchAlbumTitle.length() == 0 && srchLicensor.length() ==0 && srchSinger.length() ==0 
				 && srchProducer.length() == 0 && srchNoneName.length() ==0 && srchNoneRole_arr.length == 0){
				
				mv.addObject("srchStartDate_tmp", thisMonth);  // 발매
				mv.addObject("srchEndDate_tmp", thisDay);  // 발매일
			}
				
			// ==================== 검색조건이 없는경우. end
			
		} else if(DIVS.equals("B") && !gubun.equals("totalSearch")){  // 어문
			// 발매일 제어 : 삭제 (20101128)
			//mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate",thisYear));  // 발매일
			mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));  // 발매일
			mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));  // 발매일
		}  else if(DIVS.equals("I")){  // 이미지
			mv.addObject("srchWorkName", ServletRequestUtils.getStringParameter(request, "srchWorkName"));  // 이미지명
			mv.addObject("srchWterDivs", ServletRequestUtils.getStringParameter(request, "srchWterDivs"));  // 집필진
			mv.addObject("srchLishComp", ServletRequestUtils.getStringParameter(request, "srchLishComp"));  // 출판사명 
			mv.addObject("srchCoptHodr", ServletRequestUtils.getStringParameter(request, "srchCoptHodr"));  // 저자명 
			mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));  // 출판년도 
			mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));  // 출판년도
		} else if(DIVS.equals("V")){  // 영화
			mv.addObject("srchViewGrade", ServletRequestUtils.getStringParameter(request, "srchViewGrade"));  // 관람등급
			mv.addObject("srchDirector", ServletRequestUtils.getStringParameter(request, "srchDirector"));  // 감독
			mv.addObject("srchActor", ServletRequestUtils.getStringParameter(request, "srchActor"));  // 출연진
			mv.addObject("srchDistributor", ServletRequestUtils.getStringParameter(request, "srchDistributor"));  // 제작사/배급사/투자사
			mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate")); 
			mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate")); 
		}else if(DIVS.equals("N")){  // 뉴스
			//mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate")); 
			//mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));

			String srchStartDate = ServletRequestUtils.getStringParameter(request, "srchStartDate", "");
			String srchEndDate = ServletRequestUtils.getStringParameter(request, "srchEndDate", "");
			/* 20120220 검색조건 화면 수정 //정병호 */
			mv.addObject("srchStartDate", srchStartDate); 
			mv.addObject("srchEndDate", srchEndDate);
		}else{
			mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate")); 
			mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate")); 
		}
		
		
		
		// 도서 
		mv.addObject("srchPublisher", ServletRequestUtils.getStringParameter(request, "srchPublisher"));  // 출판사
		mv.addObject("srchBookTitle", ServletRequestUtils.getStringParameter(request, "srchBookTitle"));  // 도서명
		mv.addObject("srchLicensorNm", ServletRequestUtils.getStringParameter(request, "srchLicensorNm")); // 저자명 
		
		// 방송대본
		mv.addObject("srchWriter", ServletRequestUtils.getStringParameter(request, "srchWriter"));  // 작가명
		mv.addObject("srchBroadStatName", ServletRequestUtils.getStringParameter(request, "srchBroadStatName"));  // 방송사
		mv.addObject("srchDirect", ServletRequestUtils.getStringParameter(request, "srchDirect"));  // 연출가
		mv.addObject("srchPlayers", ServletRequestUtils.getStringParameter(request, "srchPlayers"));  // 주요출연진
		
		// 방송
		mv.addObject("srchProgName", ServletRequestUtils.getStringParameter(request, "srchProgName"));  // 프로그램명
		mv.addObject("srchMaker", ServletRequestUtils.getStringParameter(request, "srchMaker"));  // 제작자
		
		return mv;
	}
	
	
	//저작물 목록
	public ModelAndView subListForm(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		
		String DIVS = ServletRequestUtils.getStringParameter(request, "DIVS", "M");  // 구분값. 값이 없을 시 음악으로 세팅 

		RghtPrps rghtPrps = new RghtPrps();

		rghtPrps.setSrchTitle(ServletRequestUtils.getStringParameter(request, "srchTitle")); // 곡명
		rghtPrps.setSrchProducer(ServletRequestUtils.getStringParameter(request, "srchProducer")); // 제작사
		rghtPrps.setSrchAlbumTitle(ServletRequestUtils.getStringParameter(request, "srchAlbumTitle")); // 앨범명
		rghtPrps.setSrchSinger(ServletRequestUtils.getStringParameter(request, "srchSinger")); // 가수명
		rghtPrps.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate")); // 발매일
		rghtPrps.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate")); // 발매일
		rghtPrps.setSrchLicensor(ServletRequestUtils.getStringParameter(request, "srchLicensor")); // 저작자명
		rghtPrps.setSrchNoneName(ServletRequestUtils.getStringParameter(request, "srchNoneName")); 
		rghtPrps.setSrchNoneRole_arr(ServletRequestUtils.getStringParameters(request, "srchNoneRole_arr"));; // 저작권미상항목
		
		// 도서 
		rghtPrps.setSrchPublisher(ServletRequestUtils.getStringParameter(request, "srchPublisher"));  // 출판사
		rghtPrps.setSrchBookTitle(ServletRequestUtils.getStringParameter(request, "srchBookTitle"));  // 도서명 
		rghtPrps.setSrchLicensorNm(ServletRequestUtils.getStringParameter(request, "srchLicensorNm"));  // 저자명 
		
		// 방송대본
		rghtPrps.setSrchWriter(ServletRequestUtils.getStringParameter(request, "srchWriter"));  // 작가명
		rghtPrps.setSrchBroadStatName(ServletRequestUtils.getStringParameter(request, "srchBroadStatName")); // 방송사
		
		// 이미지 
		rghtPrps.setSrchWorkName(ServletRequestUtils.getStringParameter(request, "srchWorkName"));  // 이미지명
		rghtPrps.setSrchWterDivs(ServletRequestUtils.getStringParameter(request, "srchWterDivs")); // 집필진
		rghtPrps.setSrchLishComp(ServletRequestUtils.getStringParameter(request, "srchLishComp"));  // 출판사
		rghtPrps.setSrchCoptHodr(ServletRequestUtils.getStringParameter(request, "srchCoptHodr"));  // 출판년도
		
		// 영화
		rghtPrps.setSrchViewGrade(ServletRequestUtils.getStringParameter(request, "srchViewGrade"));  // 도서부제
		rghtPrps.setSrchDirector(ServletRequestUtils.getStringParameter(request, "srchDirector"));  // 감독명 
		rghtPrps.setSrchActor(ServletRequestUtils.getStringParameter(request, "srchActor"));  // 출연진 
		rghtPrps.setSrchDistributor(ServletRequestUtils.getStringParameter(request, "srchDistributor"));  // 배급사
		
		// 방송
		rghtPrps.setSrchProgName(ServletRequestUtils.getStringParameter(request, "srchProgName"));  // 프로그램명
		rghtPrps.setSrchMaker(ServletRequestUtils.getStringParameter(request, "srchMaker")); // 제작자
		
		rghtPrps.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		
		int from = Constants.DEFAULT_ROW_PER_PAGE * (rghtPrps.getNowPage()-1) + 1;
        int to = Constants.DEFAULT_ROW_PER_PAGE * rghtPrps.getNowPage();
        
        rghtPrps.setStartRow(from);
        rghtPrps.setEndRow(to);
        
		int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
		
		ModelAndView mv = null;
		
		//rghtPrpsService.muscRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  rghtPrps);
		if(DIVS.equals("M")){
				
			new ModelAndView("noneRght/muscRghtSrch");
				ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
				mv.addObject("muscList", listResult);
				
				// 조건이 없는 경우 다시 set
				if( !ServletRequestUtils.getStringParameter(request, "srchStartDate_tmp","").equals("") )
					rghtPrps.setSrchStartDate("");
				if( !ServletRequestUtils.getStringParameter(request, "srchEndDate_tmp","").equals("") )
					rghtPrps.setSrchEndDate("");
				
		}else if(DIVS.equals("B")){
			
				mv = new ModelAndView("noneRght/bookRghtSrch");
				ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
				mv.addObject("bookList", listResult);
		
		}else if(DIVS.equals("C")){  // 방송대본 
			
			mv = new ModelAndView("noneRght/scriptRghtSrch");
				ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
				mv.addObject("scriptList", listResult);
		
		}else if(DIVS.equals("I")){
			
				mv = new ModelAndView("noneRght/imageRghtSrch");
				ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
				mv.addObject("imageList", listResult);
		}
		else if(DIVS.equals("V")){
			
				mv = new ModelAndView("noneRght/mvieRghtSrch");
				ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
				mv.addObject("mvieList", listResult);
		
		}else if(DIVS.equals("R")){  // 방송
			
			mv = new ModelAndView("noneRght/broadcastRghtSrch");
				ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
				mv.addObject("scriptList", listResult);
		}
		
		mv.addObject("srchParam", rghtPrps);
		return mv;
	}
	
	
	//저작물 목록
	public ModelAndView subList(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		
		String DIVS = ServletRequestUtils.getStringParameter(request, "DIVS", "M");  // 구분값. 값이 없을 시 음악으로 세팅 
		
		/* 20120220 검색조건 화면 수정 //정병호 */
		String checkArr[] = ServletRequestUtils.getStringParameters(request, "srchNoneRole_arr");
		
		
//		공통
		String srchLicensor_c = ServletRequestUtils.getStringParameter(request, "srchLicensor");
		String srchTitle_c = ServletRequestUtils.getStringParameter(request, "srchTitle", "");
		String srchStartDate_c = ServletRequestUtils.getStringParameter(request, "srchStartDate");
		String srchEndDate_c = ServletRequestUtils.getStringParameter(request, "srchEndDate");
		String srchNoneName_c = ServletRequestUtils.getStringParameter(request, "srchNoneName");
		String srchLicensorNm_c = ServletRequestUtils.getStringParameter(request, "srchLicensorNm");
		int checkLength_c = checkArr.length;
		
		
		//음악
		String srchSinger_c = ServletRequestUtils.getStringParameter(request, "srchSinger");
		String srchAlbumTitle_c = ServletRequestUtils.getStringParameter(request, "srchAlbumTitle");
		String srchProducer_c = ServletRequestUtils.getStringParameter(request, "srchProducer");
		//srchTitle,srchLicensor,srchStartDate srchEndDate,srchNoneName,checkLength 공통
		
		//도서
		String srchBookTitle_c = ServletRequestUtils.getStringParameter(request, "srchBookTitle");
		String srchPublisher_c = ServletRequestUtils.getStringParameter(request, "srchPublisher");
		//srchLicensorNm,srchTitle,srchLicensor,srchStartDate srchEndDate,srchNoneName,checkLength 공통
		
		//뉴스
		String srchProviderNm_c = ServletRequestUtils.getStringParameter(request, "srchProviderNm");
		//srchLicensorNm, srchTitle,srchStartDate srchEndDate,srchNoneName,checkLength  공통
		
		//방송대본
		String srchWriter_c = ServletRequestUtils.getStringParameter(request, "srchWriter");
		String srchDirect_c = ServletRequestUtils.getStringParameter(request, "srchDirect");
		String srchBroadStatName_c = ServletRequestUtils.getStringParameter(request, "srchBroadStatName");
		String srchPlayers_c = ServletRequestUtils.getStringParameter(request, "srchPlayers");
		//srchTitle,srchStartDate srchEndDate,srchNoneName,checkLength 공통
		
		//이미지
		String srchWterDivs_c = ServletRequestUtils.getStringParameter(request, "srchWterDivs");
		String srchWorkName_c = ServletRequestUtils.getStringParameter(request, "srchWorkName", "");
		String srchCoptHodr_c = ServletRequestUtils.getStringParameter(request, "srchCoptHodr");
		//checkLength 공통
		
		//영화
		String srchDirector_c = ServletRequestUtils.getStringParameter(request, "srchDirector");
		String srchDistributor_c = ServletRequestUtils.getStringParameter(request, "srchDistributor");
		String srchActor_c = ServletRequestUtils.getStringParameter(request, "srchActor");
		//srchTitle,srchStartDate srchEndDate,srchNoneName,checkLength 공통
		
		//방송
		String srchMaker_c = ServletRequestUtils.getStringParameter(request, "srchMaker");
		String srchProgName_c = ServletRequestUtils.getStringParameter(request, "srchProgName");
//		srchTitle,srchStartDate srchEndDate,srchNoneName,checkLength 공통
		
		/* END //정병호 */
		
		

		Date date=new Date();
		SimpleDateFormat yearForm = new SimpleDateFormat("yyyy");
		SimpleDateFormat dayForm = new SimpleDateFormat("yyyyMMdd");
		String thisYear= yearForm.format(date);
		String thisMonth = yearForm.format(date)+"0101";
		String thisDay = dayForm.format(date);
		
		RghtPrps rghtPrps = new RghtPrps();

		rghtPrps.setSrchTitle(ServletRequestUtils.getStringParameter(request, "srchTitle", "")); // 곡명, 작품명, 영화명 
		rghtPrps.setSrchTitleLen(rghtPrps.getSrchTitle().length()+"");
		
		rghtPrps.setSrchProducer(ServletRequestUtils.getStringParameter(request, "srchProducer")); // 제작사
		rghtPrps.setSrchAlbumTitle(ServletRequestUtils.getStringParameter(request, "srchAlbumTitle")); // 앨범명
		rghtPrps.setSrchSinger(ServletRequestUtils.getStringParameter(request, "srchSinger")); // 가수명
		rghtPrps.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate")); // 발매일
		rghtPrps.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate")); // 발매일
		rghtPrps.setSrchLicensor(ServletRequestUtils.getStringParameter(request, "srchLicensor")); // 저작자명
		rghtPrps.setSrchNoneName(ServletRequestUtils.getStringParameter(request, "srchNoneName")); 
		rghtPrps.setSrchNonPerf(ServletRequestUtils.getStringParameter(request, "srchNonPerf","")); 	// 미확인실연자
		rghtPrps.setSrchNoneRole_arr(ServletRequestUtils.getStringParameters(request, "srchNoneRole_arr")); // 저작권미상항목
		
		// ==================== 검색조건이 없는경우. start
		String srchStartDate = ServletRequestUtils.getStringParameter(request, "srchStartDate", "");	// 발매시작일
		String srchEndDate = ServletRequestUtils.getStringParameter(request, "srchEndDate", "");	// 발매종료일
		String srchTitle = ServletRequestUtils.getStringParameter(request, "srchTitle", "");	// 곡명
		String srchAlbumTitle = ServletRequestUtils.getStringParameter(request, "srchAlbumTitle", "");	//앨범;
		String srchLicensor = ServletRequestUtils.getStringParameter(request, "srchLicensor", "");	// 저작자
		String srchSinger = ServletRequestUtils.getStringParameter(request, "srchSinger", "");  // 가수명
		String srchProducer = ServletRequestUtils.getStringParameter(request, "srchProducer", "");  // 제작사
		String srchNoneRole_arr[] = ServletRequestUtils.getStringParameters(request, "srchNoneRole_arr"); 	// 저작권미상항목
		String srchNoneName = ServletRequestUtils.getStringParameter(request, "srchNoneName", "");  // 저작자명
		String srchNonPerf = ServletRequestUtils.getStringParameter(request, "srchNonPerf",""); 	// 미확인실연자
		
		// ==================== 검색조건이 없는경우. end
		
		// 도서 
		rghtPrps.setSrchPublisher(ServletRequestUtils.getStringParameter(request, "srchPublisher"));  // 출판사
		rghtPrps.setSrchBookTitle(ServletRequestUtils.getStringParameter(request, "srchBookTitle"));  // 도서명 
		rghtPrps.setSrchLicensorNm(ServletRequestUtils.getStringParameter(request, "srchLicensorNm"));  // 저자명 
		
		// 이미지
		rghtPrps.setSrchWorkName(ServletRequestUtils.getStringParameter(request, "srchWorkName"));  // 이미지명
		rghtPrps.setSrchWterDivs(ServletRequestUtils.getStringParameter(request, "srchWterDivs")); // 작가명
		rghtPrps.setSrchLishComp(ServletRequestUtils.getStringParameter(request, "srchLishComp"));  // 출판사
		rghtPrps.setSrchCoptHodr(ServletRequestUtils.getStringParameter(request, "srchCoptHodr"));  // 출판년도
		
		// 방송대본
		rghtPrps.setSrchWriter(ServletRequestUtils.getStringParameter(request, "srchWriter")); // 작가명
		rghtPrps.setSrchBroadStatName(ServletRequestUtils.getStringParameter(request, "srchBroadStatName"));  // 방송사 
		rghtPrps.setSrchDirect(ServletRequestUtils.getStringParameter(request, "srchDirect"));  // 연출가 
		rghtPrps.setSrchPlayers(ServletRequestUtils.getStringParameter(request, "srchPlayers"));  // 주요출연진 
		
		// 영화
		rghtPrps.setSrchViewGrade(ServletRequestUtils.getStringParameter(request, "srchViewGrade"));  // 도서부제
		rghtPrps.setSrchDirector(ServletRequestUtils.getStringParameter(request, "srchDirector"));  // 감독명 
		rghtPrps.setSrchActor(ServletRequestUtils.getStringParameter(request, "srchActor"));  // 출연진 
		rghtPrps.setSrchDistributor(ServletRequestUtils.getStringParameter(request, "srchDistributor"));  // 배급사
		
		// 방송
		rghtPrps.setSrchProgName(ServletRequestUtils.getStringParameter(request, "srchProgName"));  // 프로그램명
		rghtPrps.setSrchMaker(ServletRequestUtils.getStringParameter(request, "srchMaker")); // 제작자
		
		
		int noneRoleLength = rghtPrps.getSrchNoneRole_arr().length;
		if(DIVS.equals("B")){
			if(rghtPrps.getSrchLicensorNm().trim().equals("") && rghtPrps.getSrchTitle().trim().equals("") && rghtPrps.getSrchLicensor().trim().equals("") 
			&& rghtPrps.getSrchBookTitle().trim().equals("") && rghtPrps.getSrchPublisher().trim().equals("") && rghtPrps.getSrchStartDate().trim().equals("") && rghtPrps.getSrchEndDate().trim().equals("")
			&& noneRoleLength == 0 && rghtPrps.getSrchNoneName().trim().equals("") && rghtPrps.getSrchNonPerf().trim().equals("")){
				
				rghtPrps.setSrchStartDate("2000"); // 발매일
				rghtPrps.setSrchEndDate(thisYear); // 발매일
			}
			
			if(!rghtPrps.getSrchStartDate().trim().equals("")){
				rghtPrps.setSrchStartDate(rghtPrps.getSrchStartDate().substring(0, 4));
			}
			if(!rghtPrps.getSrchEndDate().trim().equals("")){
				rghtPrps.setSrchEndDate(rghtPrps.getSrchEndDate().substring(0, 4));
			}
			
		}else if(DIVS.equals("M")){
			if( srchStartDate.length()==0 && srchEndDate.length() == 0 && srchTitle.length() == 0 
					 && srchAlbumTitle.length() == 0 && srchLicensor.length() ==0 && srchSinger.length() ==0 
					 && srchProducer.length() == 0 && srchNoneName.length() ==0 && srchNoneRole_arr.length == 0){
					
					// 조건이 없는 경우.
					rghtPrps.setSrchStartDate("20090101"); // 발매일
					rghtPrps.setSrchEndDate(thisDay); // 발매일
				}
			if(srchStartDate.length()==0 && srchEndDate.length() == 0 && srchTitle.length() == 0 
					 && srchAlbumTitle.length() == 0 && srchLicensor.length() ==0 && srchSinger.length() ==0 
					 && srchProducer.length() == 0 && srchNoneName.length() ==0 && srchNoneRole_arr.length > 0){
				
					rghtPrps.setSrchStartDate("20090101"); // 발매일
					rghtPrps.setSrchEndDate(thisDay); // 발매일
			}
		}else if(DIVS.equals("V")){
			if(!rghtPrps.getSrchStartDate().trim().equals("")){
				rghtPrps.setSrchStartDate(rghtPrps.getSrchStartDate().substring(0, 4));
			}
			if(!rghtPrps.getSrchEndDate().trim().equals("")){
				rghtPrps.setSrchEndDate(rghtPrps.getSrchEndDate().substring(0, 4));
			}
		}else{
			rghtPrps.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate")); // 발매일
			rghtPrps.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate")); // 발매일
		}
		
		rghtPrps.setSTotalRow(ServletRequestUtils.getStringParameter(request, "sTotalRow", ""));    
		
System.out.println("=============================================================");
System.out.println("sTotalRow >> "+rghtPrps.getSTotalRow());
System.out.println("=============================================================");

		rghtPrps.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		
		int from = Constants.DEFAULT_ROW_PER_PAGE * (rghtPrps.getNowPage()-1) + 1;
        int to = Constants.DEFAULT_ROW_PER_PAGE * rghtPrps.getNowPage();
        
        rghtPrps.setStartRow(from);
        rghtPrps.setEndRow(to);
        
		int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
		
		ModelAndView mv = null;

		if(DIVS.equals("M")){
			if(srchLicensor_c.equals("") && srchTitle_c.equals("") && srchSinger_c.equals("") && srchAlbumTitle_c.equals("") && srchProducer_c.equals("") && srchStartDate_c.equals("") && srchEndDate_c.equals("") && srchNoneName_c.equals("") && checkLength_c == 0){
				mv = new ModelAndView("noneRght/muscRghtSrch");
				mv.addObject("emptyYn", "Y");
			}else{
				mv = new ModelAndView("noneRght/muscRghtSrch", "muscList", noneRghtService.muscRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  rghtPrps));
			}
		}else if(DIVS.equals("B")){
			if(srchLicensorNm_c.equals("") && srchTitle_c.equals("") && srchLicensor_c.equals("") && srchBookTitle_c.equals("") && srchPublisher_c.equals("") && srchStartDate_c.equals("") && srchEndDate_c.equals("") && srchNoneName_c.equals("") && checkLength_c == 0){
				mv = new ModelAndView("noneRght/bookRghtSrch");
				mv.addObject("emptyYn", "Y");
			}else{
				mv = new ModelAndView("noneRght/bookRghtSrch", "bookList", noneRghtService.bookRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  rghtPrps));
			}
		}else if(DIVS.equals("I")){
			if(srchWterDivs_c.equals("") && srchWorkName_c.equals("") && srchCoptHodr_c.equals("") && checkLength_c == 0){
				mv = new ModelAndView("noneRght/imageRghtSrch");
				mv.addObject("emptyYn", "Y");
			}else{
				mv = new ModelAndView("noneRght/imageRghtSrch", "imageList", noneRghtService.imageRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  rghtPrps));
			}
		}else if(DIVS.equals("V")){
			if(srchDirector_c.equals("") && srchTitle_c.equals("") && srchDistributor_c.equals("") && srchActor_c.equals("") && srchStartDate_c.equals("") && srchEndDate_c.equals("") && srchNoneName_c.equals("") && checkLength_c == 0){
				mv = new ModelAndView("noneRght/mvieRghtSrch");
				mv.addObject("emptyYn", "Y");
			}else{
				mv = new ModelAndView("noneRght/mvieRghtSrch", "mvieList", noneRghtService.mvieRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  rghtPrps));
			}
		}else if(DIVS.equals("C")){
			if(srchWriter_c.equals("") && srchTitle_c.equals("") && srchDirect_c.equals("") && srchBroadStatName_c.equals("") && srchPlayers_c.equals("") && srchStartDate_c.equals("") && srchEndDate_c.equals("") && srchNoneName_c.equals("") && checkLength_c == 0){
				mv = new ModelAndView("noneRght/scriptRghtSrch");
				mv.addObject("emptyYn", "Y");
			}else{
				mv = new ModelAndView("noneRght/scriptRghtSrch", "scriptList", noneRghtService.scriptRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  rghtPrps));
			}
		}else if(DIVS.equals("R")){
			if(srchMaker_c.equals("") && srchProgName_c.equals("") && srchTitle_c.equals("") && srchStartDate_c.equals("") && srchEndDate_c.equals("") && srchNoneName_c.equals("") && checkLength_c == 0){
				mv = new ModelAndView("noneRght/broadcastRghtSrch");
				mv.addObject("emptyYn", "Y");
			}else{
				mv = new ModelAndView("noneRght/broadcastRghtSrch", "broadcastList", noneRghtService.broadcastRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  rghtPrps));
			}
		}else if(DIVS.equals("N")){
			if(srchLicensorNm_c.equals("") && srchTitle_c.equals("") && srchStartDate_c.equals("") && srchEndDate_c.equals("") && srchNoneName_c.equals("") && checkLength_c == 0){
				mv = new ModelAndView("noneRght/newsRghtSrch");
				mv.addObject("emptyYn", "Y");
			}else{
				mv = new ModelAndView("noneRght/newsRghtSrch", "newsList", noneRghtService.newsRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  rghtPrps));
			}
		}
		mv.addObject("srchParam", rghtPrps);
		return mv;
	}
}
