package kr.or.copyright.mls.statBord.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.statBord.model.AnucBord;
import kr.or.copyright.mls.statBord.model.AnucBordAttcFile;
import kr.or.copyright.mls.statBord.model.AnucBordFile;
import kr.or.copyright.mls.statBord.model.AnucBordObjc;
import kr.or.copyright.mls.statBord.model.AnucBordObjcFile;
import kr.or.copyright.mls.statBord.model.AnucBordSupl;
import kr.or.copyright.mls.statBord.model.AnucNonAttcFile;
import kr.or.copyright.mls.statBord.model.Code;


public interface AnucBordDao {
	
	//-----------------공고게시판---------------------
	public List<AnucBord> SelTest();
	public void insertAnuc(AnucBord vo);							//등록
	public void fileInsertAnuc1(AnucBordFile anucBordFile);			//파일첨부1(ML_FILE)
	public void fileInsertAnuc2(AnucBordAttcFile anucBordAttcFile); //파일첨부2(공고게시판)
	public int getBordSeqn();
	public List<AnucBordFile> fileSelectAnuc(int bordSeqn);			//파일 리스트(공고게시판)
	public List<AnucBordFile> fileSelectObjc(int statObjcId);		//파일 리스트(이의제기)
	public void fileInsertObjc(AnucBordObjcFile anucBordObjcFile); 	//파일첨부(이의제기)
	public int deleteAnuc(int bordSeqn);							//삭제
	public List<AnucBord> selectAnuc(AnucBord anucBord);			//리스트
	public List<AnucBord> detailAnuc(int bordSeqn);					//상세보기
	public void updateInqrContAnuc(int bordSeqn);					//조회수증가
	public List<AnucBord> findAnuc(AnucBord anucBord); 				//검색
	public int countAnuc(AnucBord anucBord); 						//글 갯수
	public int findCount(AnucBord anucBord); 						//검색된 갯수
	public void updateAnucBord(AnucBord anucBord);					//수정
	public void deleteAttcFile(int attcSeqn);
	public void deleteAnucBord(int bordSeqn);
	//------------------이의제기----------------------
	public int countAnucObjc(int bordSeqn); 							//이의제기 갯수
	public void insertAnucObjc(AnucBordObjc anucBordObjc); 				//이의제기 등록
	public int updateAnucBordObjcYn(int bordSeqn);						//등록후 Objc_YN 수정
	public List<AnucBordObjc> selectAnucObjc(int bordSeqn,int bordCd); 	//이의제기 리스트
	public List<AnucBordObjc> selectAnucObjcShis(int statObjcId);
	public int deleteAnucBordObjc(int statObjcId);						//이의제기 삭제1
	public int deleteAnucBordObjcStat(int statObjcId);					//이의제기 삭제2
	public int deleteAnucBordObjcNon(int statObjcId);					//이의제기 삭제3
	public List getObjcFileAttc(int statObjcId);						//이의제기 파일 삭제1
	public int deleteObjcFile(int attcSeqn);							//이의제기 파일 삭제2
	public int deleteObjcAttcFile(int attcSeqn);						//이의제기 파일 삭제3
	public int updateAnucObjc(AnucBordObjc anucBordObjc);				//이의제기 수정

	public void updateAnucBordObjcYnDelete(int bordSeqn);				//삭제후 OBJC_YN 수정
	//------------------거소불명------------------------
	public int getWorksId();							//WorksId
	public List<AnucBord> selectNonAnuc(AnucBord anucBord);				//거소불명 리스트
	public void insertNonAnuc(AnucBord anucBord);						//거소불명 등록 ml_stat_works
	public void insertNonAnuc2(AnucBord anucBord);						//ml_non_idnt_works
	public void insertNonAnuc3(AnucBord anucBord);						//ml_non_idnt_works_shist
	public void insertCopthodr(AnucBord anucBord);						//ml_non_idnt_works_copthodr
	public void fileInsertNon(AnucBordFile anucBordFile);				//파일등록 ml_file
	public void fileInsertNonAnuc(AnucNonAttcFile anucNonAttcFile);		//거소불명 파일등록
	public List<AnucBord> detailNonAnuc(int worksId);					//거소불명 상세보기
	public List<AnucBord> NonResult(int worksId);						//상당한노력 진행결과
	public List<AnucBord> detailNonCopthodr(AnucBord anucBord);
	public List<AnucBord> detailGetMaker(int worksId);
	public void updateNonAnuc(AnucBord anucBord);						//거소불명 수정(ml_non_idnt_works)
	public void updateStatWorks(AnucBord anucBord);						//거소불명 수정2(ml_stat_works)
	public void updateNonCoptHodr(AnucBord anucBord);					//거소불명 수정3(coptHodr)
	public List<AnucBordFile> fileSelectNonAnuc(int worksId);			//거소불명 파일보기
	public List getNonFileAttc(int worksId);							//거소불명 파일 attc
	public void deleteNonAnuc(int worksId);								//거소불명 논리삭제
	public int countNonAnuc(String rgstIdnt);							//거소불명 갯수
	public int deleteNonAttcFile(int attcSeqn);							//거소불명 파일 삭제
	public List<Code> getCodeList(Map params);							//장르 코드값
	public int deletecoptHodr(int worksId);
	public List<AnucBord> findNonAnuc(Map param);
	public int countFindNonAnuc(Map param);
	//--------------법정허락 이용승인신청----------------------
	public void insertStatObjc(AnucBordObjc anucBordObjc);				//이의제기
	public int countStatObjc(int worksId);								//이의제기 갯수
	public List<AnucBordObjc> selectStatObjc(int worksId);				//이의제기내용
	public int deleteStatObjc(int statObjcId);							//이의제기 삭제
	public void updateStatWorksObjcYn(int worksId);						//OBJC_YN 수정
	public void updateStatWorksObjcYnDelete(int worksId);
	public List StatObjcPop(AnucBord anucBord);
	
	//-------------------------보완------------------------------------
	public List<AnucBordSupl> selectBordSuplItemList(int bordSeqn, int bordCd);// 저작권자 조회공고, 보상금 공탁공고 보완내역 불러오기	
	public List<AnucBordSupl> selectWorksSuplItemList(int worksId);// 상당한노력신청 - 저작권자 조회공고 보완목록 불러오기
	
	//=======================기승인 법정허락 저작물 리스트======================================
	public int statBo07TotalCount(Map dataMap);
	public List statBo07List(Map dataMap);
	
}
