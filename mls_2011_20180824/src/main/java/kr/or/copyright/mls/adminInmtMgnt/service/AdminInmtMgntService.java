package kr.or.copyright.mls.adminInmtMgnt.service;

public interface AdminInmtMgntService {
	
	public void adminInmtUpload() throws Exception;
	
	public void inmtMgntMuscList() throws Exception;
	
	public void inmtMgntLibrList() throws Exception;
	
	public void inmtMgntSubjList() throws Exception;
	
	public void inmtMgntReptView() throws Exception;
	
	public void inmtMgntLssnList() throws Exception;
}