package kr.or.copyright.mls.console.legal;

import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.legal.inter.FdcrAd11Service;
import kr.or.copyright.mls.myStat.service.MyStatService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 법정허락 > 법정허락 대상저작물 공고
 * 
 * @author ljh
 */
@Controller
public class FdcrAd11Controller extends DefaultController{

	@Resource( name = "fdcrAd11Service" )
	private FdcrAd11Service fdcrAd11Service;

	@Resource( name = "myStatService" )
	private MyStatService myStatService;

	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd11Controller.class );

	/**
	 * 법정허락 대상저작물 공고 목록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd11List1.page" )
	public String fdcrAd11List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		
		commandMap.put( "TAB_INDEX", "0" );

		String SCH_WORKS_DIVS_CD = EgovWebUtil.getString( commandMap, "SCH_WORKS_DIVS_CD" );
		String SCH_STAT_OBJC_CD = EgovWebUtil.getString( commandMap, "SCH_STAT_OBJC_CD" );
		String SCH_WORKS_TITLE = EgovWebUtil.getString( commandMap, "SCH_WORKS_TITLE" );
		String SCH_LYRI_WRTR = EgovWebUtil.getString( commandMap, "SCH_LYRI_WRTR" );

		if( !"".equals( SCH_WORKS_DIVS_CD ) ){
			SCH_WORKS_DIVS_CD = URLDecoder.decode( SCH_WORKS_DIVS_CD, "UTF-8" );
			commandMap.put( "SCH_WORKS_DIVS_CD", SCH_WORKS_DIVS_CD );
		}
		if( !"".equals( SCH_STAT_OBJC_CD ) ){
			SCH_STAT_OBJC_CD = URLDecoder.decode( SCH_STAT_OBJC_CD, "UTF-8" );
			commandMap.put( "SCH_STAT_OBJC_CD", SCH_STAT_OBJC_CD );
		}
		if( !"".equals( SCH_WORKS_TITLE ) ){
			SCH_WORKS_TITLE = URLDecoder.decode( SCH_WORKS_TITLE, "UTF-8" );
			commandMap.put( "SCH_WORKS_TITLE", SCH_WORKS_TITLE );
		}
		if( !"".equals( SCH_LYRI_WRTR ) ){
			SCH_LYRI_WRTR = URLDecoder.decode( SCH_LYRI_WRTR, "UTF-8" );
			commandMap.put( "SCH_LYRI_WRTR", SCH_LYRI_WRTR );
		}

		fdcrAd11Service.fdcrAd11List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		System.out.println( "법정허락 대상저작물 공고 목록" );
		return "legal/fdcrAd11List1.tiles";
	}
	
	/**
	 * 이의제기 목록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd11List2.page" )
	public String fdcrAd11List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "TAB_INDEX", "1" );
		
		String SCH_WORKS_DIVS_CD = EgovWebUtil.getString( commandMap, "SCH_WORKS_DIVS_CD" );
		String SCH_STAT_OBJC_CD = EgovWebUtil.getString( commandMap, "SCH_STAT_OBJC_CD" );
		String SCH_WORKS_TITLE = EgovWebUtil.getString( commandMap, "SCH_WORKS_TITLE" );
		String SCH_LYRI_WRTR = EgovWebUtil.getString( commandMap, "SCH_LYRI_WRTR" );
		
		if( !"".equals( SCH_WORKS_DIVS_CD ) ){
			SCH_WORKS_DIVS_CD = URLDecoder.decode( SCH_WORKS_DIVS_CD, "UTF-8" );
			commandMap.put( "SCH_WORKS_DIVS_CD", SCH_WORKS_DIVS_CD );
		}
		if( !"".equals( SCH_STAT_OBJC_CD ) ){
			SCH_STAT_OBJC_CD = URLDecoder.decode( SCH_STAT_OBJC_CD, "UTF-8" );
			commandMap.put( "SCH_STAT_OBJC_CD", SCH_STAT_OBJC_CD );
		}
		if( !"".equals( SCH_WORKS_TITLE ) ){
			SCH_WORKS_TITLE = URLDecoder.decode( SCH_WORKS_TITLE, "UTF-8" );
			commandMap.put( "SCH_WORKS_TITLE", SCH_WORKS_TITLE );
		}
		if( !"".equals( SCH_LYRI_WRTR ) ){
			SCH_LYRI_WRTR = URLDecoder.decode( SCH_LYRI_WRTR, "UTF-8" );
			commandMap.put( "SCH_LYRI_WRTR", SCH_LYRI_WRTR );
		}

		fdcrAd11Service.fdcrAd11List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		//-----20170831 jsp에 필요함으로 추가------------
		model.addAttribute( "paginationInfo",commandMap.get( "paginationInfo" ) );
		//----------------------------------------
		System.out.println( "이의제기 목록" );
		return "legal/fdcrAd11List2.tiles";
	}

	/**
	 * 법정허락 대상저작물 공고 상세
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd11View1.page" )
	public String fdcrAd11View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		fdcrAd11Service.fdcrAd11View1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_object", commandMap.get( "ds_object" ) );
		model.addAttribute( "ds_object_file", commandMap.get( "ds_object_file" ) );

		System.out.println( "법정허락 대상저작물 공고 상세" );
		return "legal/fdcrAd11View1.tiles";
	}

	/**
	 * 법정허락 대상저작물 공고 게시판 등록자/이의제기 신청인 정보 조회
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd11Pop1.page" )
	public String fdcrAd11Pop1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "USER_IDNT", EgovWebUtil.getString( commandMap, "OBJC_RGST_ID" ) );
		fdcrAd11Service.fdcrAd11Pop1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "법정허락 대상저작물 공고 게시판 등록자 정보 조회" );
		return "legal/fdcrAd11Pop1.popup";
	}

	/**
	 * 이의제기 신청정보/신청상태 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd11Download1.page" )
	public void fdcrAd11Download1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		fdcrAd11Service.fdcrAd11View1( commandMap );
		List list = (List) commandMap.get( "ds_object_file" );
		for( int i = 0; i < list.size(); i++ ){
			Map map = (Map) list.get( i );
			String worksId = (String) map.get( "WORKS_ID" );
			if( worksId.equals( commandMap.get( "WORKS_ID" ) ) ){
				String filePath = (String) map.get( "FILE_PATH" );
				String fileName = (String) map.get( "FILE_NAME" );
				String realFileName = (String) map.get( "REAL_FILE_NAME" );
				download( request, response, filePath + realFileName, fileName );
			}
		}

	}

	/**
	 * 이의제기 진행상태 수정
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd11Update1.page" )
	public void fdcrAd11Update1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
/*
		String[] STAT_OBJC_CDS = request.getParameterValues( "STAT_OBJC_CD" );
		String[] STAT_OBJC_RSLT_CDS = request.getParameterValues( "STAT_OBJC_RSLT_CD" );
		String[] FILE_NAMES = request.getParameterValues( "FILE_NAME" );
		String[] ATTACH_FILE_CONTENT0S = request.getParameterValues( "ATTACH_FILE_CONTENT0" );
		String[] FILE_PATHS = request.getParameterValues( "FILE_PATH" );
		String[] REAL_FILE_NAMES = request.getParameterValues( "REAL_FILE_NAME" );
		String[] FILE_SIZES = request.getParameterValues( "FILE_SIZE" );
		String[] ATTACH_FILE_SIZE0S = request.getParameterValues( "ATTACH_FILE_SIZE0" );
		String[] WORKS_IDS = request.getParameterValues( "WORKS_ID" );
		commandMap.put( "STAT_OBJC_CD", STAT_OBJC_CDS );
		commandMap.put( "STAT_OBJC_RSLT_CD", STAT_OBJC_RSLT_CDS );
		commandMap.put( "FILE_NAME", FILE_NAMES );
		commandMap.put( "ATTACH_FILE_CONTENT0", ATTACH_FILE_CONTENT0S );
		commandMap.put( "FILE_PATH", FILE_PATHS );
		commandMap.put( "REAL_FILE_NAME", REAL_FILE_NAMES );
		commandMap.put( "FILE_SIZE", FILE_SIZES );
		commandMap.put( "ATTACH_FILE_SIZE0", ATTACH_FILE_SIZE0S );
		commandMap.put( "WORKS_ID", WORKS_IDS );

		commandMap.put( "TITLE", "" );
		commandMap.put( "STAT_OBJC_ID", "" );
*/
		commandMap.put( "RGST_IDNT", EgovWebUtil.getString( commandMap, "OBJC_RGST_ID" ) );
		commandMap.put( "USER_ID", EgovWebUtil.getString( commandMap, "OBJC_RGST_ID" ) );
		if(!EgovWebUtil.getString( commandMap, "TITE" ).equals( "" )){
			commandMap.put( "TITLE", EgovWebUtil.getString( commandMap, "TITE" ) );
		}else {
			commandMap.put( "TITLE", EgovWebUtil.getString( commandMap, "WORKS_TITLE" ) );
			commandMap.put( "BORD_CD", EgovWebUtil.getString( commandMap, "BORD_CD" ) );
			commandMap.put( "WORKS_ID", EgovWebUtil.getString( commandMap, "WORKS_ID" ) );
			commandMap.put( "STAT_OBJC_ID", EgovWebUtil.getString( commandMap, "STAT_OBJC_ID" ) );
		}

		boolean isSuccess = fdcrAd11Service.fdcrAd11Update1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "이의제기 진행상태 수정" );
	}

	/**
	 * 이의제기 진행상태내역조회
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd11Pop2.page" )
	public String fdcrAd11Pop2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAd11Service.fdcrAd11Pop2( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "이의제기 진행상태내역조회" );
		return "legal/fdcrAd11Pop2.popup";
	}

}
