package kr.or.copyright.mls.support.util;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import kr.or.copyright.common.userLogin.dao.UserLoginDao;
import kr.or.copyright.common.userLogin.model.User;

public class UserConnectLogUtil{

	UserLoginDao userLoginDao;
	
	public UserLoginDao getUserLoginDao(){
		return userLoginDao;
	}
	
	public void setUserLoginDao( UserLoginDao userLoginDao ){
		this.userLoginDao = userLoginDao;
	}


	public void userConnectLog(HttpServletRequest request, String actionProcess) {
		IPUtil ipUtil = new IPUtil();
		String ip = ipUtil.ipCheck();
		User user = new User(); 
		Map userMap = (Map)request.getSession().getAttribute( "CONSOLE_USER");
		user.setUserIdnt( (String)userMap.get( "USER_ID" ) );
		user.setConnectIp( ip );
		user.setActionProcess( actionProcess );
		System.out.println( user );
		System.out.println( userLoginDao );
		userLoginDao.userConnectLog( user );
	}
}
