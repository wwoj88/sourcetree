package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;

public interface FdcrAd89Service{

	/**
	 * 보상금 처리 및 신청 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd89List1( Map<String, Object> commandMap ) throws Exception;
}
