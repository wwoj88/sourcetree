package kr.or.copyright.mls.mobile.stat.model;

public class MobileStatFile {
	private int attcSeqn;				//첨부파일 순번
	private String fileAttcCd;			//구분
	private int fileNameCd;				//첨부파일CD
	private String fileName;			//첨부파일 명
	private String filePath;			//첨부파일 경로
	private String fileSize;			//첨부파일 크기
	private String realFileName;		//실제파일명
	private String rgstIdnt;			//등록자 id
	private String rgstDttm;			//등록일자
	@Override
	public String toString() {
		return "AnucBordFile [attcSeqn=" + attcSeqn + ", fileAttcCd="
				+ fileAttcCd + ", fileNameCd=" + fileNameCd + ", fileName="
				+ fileName + ", filePath=" + filePath + ", fileSize="
				+ fileSize + ", realFileName=" + realFileName + ", rgstIdnt="
				+ rgstIdnt + ", rgstDttm=" + rgstDttm + "]";
	}
	public int getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(int attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public String getFileAttcCd() {
		return fileAttcCd;
	}
	public void setFileAttcCd(String fileAttcCd) {
		this.fileAttcCd = fileAttcCd;
	}
	public int getFileNameCd() {
		return fileNameCd;
	}
	public void setFileNameCd(int fileNameCd) {
		this.fileNameCd = fileNameCd;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getRealFileName() {
		return realFileName;
	}
	public void setRealFileName(String realFileName) {
		this.realFileName = realFileName;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	
	

}