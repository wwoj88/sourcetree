package kr.or.copyright.mls.console.rcept;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 미분배 보상금 대상 저작물 > 통계
 * 
 * @author ljh
 */
@Controller
public class FdcrAd19Controller extends DefaultController{

	@Resource( name = "fdcrAd19Service" )
	private FdcrAd19ServiceImpl fdcrAd19Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd19Controller.class );

	/**
	 * 미분배보상금저작물 보고현황 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/rcept/fdcrAd19List1.page" )
	public String fdcrAd19List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		fdcrAd19Service.fdcrAd19List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "미분배보상금저작물 보고현황 조회" );
		return "rcept/fdcrAd19List1.tiles";
	}

	/**
	 * 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	// 화면단에서 구성
}
