package kr.or.copyright.mls.common.common.model;

import java.util.List;

import kr.or.copyright.mls.common.BaseObject;

public class ListResult extends BaseObject{

	private int totalRow; // 전체 게시물수
	private int pageNo; // 현재 페이지
	private int rowPerPage; // 한 화면에 보여줄 게시물수
	private int pageSize; // 페이지 리스트 사이즈
	private List resultList;
	
	public int getTotalRow() {
		return totalRow;
	}
	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public List getResultList() {
		return resultList;
	}
	public void setResultList(List resultList) {
		this.resultList = resultList;
	}
	
	public ListResult(){
		
	}

	public ListResult(int totalRow, int pageNo, int rowPerPage, List resultList){
		
		this.totalRow = totalRow;
		this.pageNo = pageNo;
		this.rowPerPage = rowPerPage;
		this.pageSize = pageSize;
		this.resultList = resultList;
	}
}

