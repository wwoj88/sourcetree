package kr.or.copyright.mls.mail.service;

import java.sql.SQLException;

import javax.annotation.Resource;

import kr.or.copyright.mls.mail.dao.MailDao;
import kr.or.copyright.mls.mail.model.Mail;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service("MailService")
public class MailServiceImpl implements MailService{
	
	private Logger log = Logger.getLogger(this.getClass());

	@Resource(name="MailDao")
	private MailDao dao;
	
	// 수신거부 insert
	@SuppressWarnings("finally")
	public int insertRejc(Mail mail)throws Exception{
		
		int iResult = 0;
		
		try {
		
			dao.insertMailRejcList(mail);
			iResult = 1;
		
		}catch(DataAccessException ex) {
			
			SQLException se = (SQLException) ((DataAccessException) ex).getRootCause();
			
			// 무결성오류(code=1)는 기존 등록주소임. 정상처리 리턴
			if(se.getErrorCode() == 1) 	iResult = 1;
			else iResult = -1;
			
		}catch(Exception e) {
			//e.printStackTrace();
			iResult = -1;
		}finally {
			return iResult;
		}	
	}
	
	// 수신거부 delete
	public int deleteRejc(Mail mail)throws Exception{
		
		int iResult = 0;
		
		try {
		
			dao.deleteMailRejcList(mail);
			iResult = 1;
		
		}catch(DataAccessException ex) {
			
			SQLException se = (SQLException) ((DataAccessException) ex).getRootCause();
			
			// 무결성오류(code=1)는 기존 등록주소임. 정상처리 리턴
			//if(se.getErrorCode() == 1) 	iResult = 1;
			//else iResult = -1;
			
			System.out.println("****** DataAccessException : {} // {}="+se.getErrorCode());
			System.out.println("****** DataAccessException : {} // {}="+se.getMessage());

			
		}catch(Exception e) {
			//e.printStackTrace();
			iResult = -1;
		}finally {
			return iResult;
		}	
	}
	public int updateEmailRecv(Mail mail) throws Exception {
		int iResult = 0;
		
		try {		
			dao.updateEmailRecv(mail);
			iResult = 1;		
		}catch(Exception e) {
			//e.printStackTrace();
			iResult = -1;
		}finally {
			return iResult;
		}	
		
	}
	
}
