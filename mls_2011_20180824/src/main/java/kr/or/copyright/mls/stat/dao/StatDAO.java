package kr.or.copyright.mls.stat.dao;

import java.util.List;

import kr.or.copyright.mls.stat.model.MlStatWorksDefaultVO;
import kr.or.copyright.mls.stat.model.MlStatWorksVO;

public interface StatDAO {
    public List<MlStatWorksVO> selectMlStatWorksList(MlStatWorksDefaultVO searchVO) throws Exception;
    
    public List<MlStatWorksVO> checkSelectMlStatWorksList(MlStatWorksDefaultVO searchVO) throws Exception;
    
    public int selectMlStatWorksListTotCnt(MlStatWorksDefaultVO searchVO);
    

}
