package kr.or.copyright.mls.console.icnissu;

import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ICN 발급관리 > 개인저작물 목록
 * 
 * @author ljh
 */
@Controller
public class FdcrAd55Controller extends DefaultController{

	@Resource( name = "fdcrAd55Service" )
	private FdcrAd55ServiceImpl fdcrAd55Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd55Controller.class );

	/**
	 * 개인저작물 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/icnissu/fdcrAd55List1.page" )
	public String fdcrAd55List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		String srch_genre = EgovWebUtil.getString( commandMap, "srch_genre" );
		String srch_icnStatCd = EgovWebUtil.getString( commandMap, "srch_icnStatCd" );
		String srch_title = EgovWebUtil.getString( commandMap, "srch_title" );
		String srch_icnxNumb = EgovWebUtil.getString( commandMap, "srch_icnxNumb" );
		String srch_rgstStartDate = EgovWebUtil.getString( commandMap, "srch_rgstStartDate" );
		String srch_rgstEndDate = EgovWebUtil.getString( commandMap, "srch_rgstEndDate" );
		String srch_rgstName = EgovWebUtil.getString( commandMap, "srch_rgstName" );
		
		if( !"".equals( srch_genre ) ){
			srch_genre = URLDecoder.decode( srch_genre, "UTF-8" );
			commandMap.put( "srch_genre", srch_genre );
		}
		if( !"".equals( srch_icnStatCd ) ){
			srch_icnStatCd = URLDecoder.decode( srch_genre, "UTF-8" );
			commandMap.put( "srch_icnStatCd", srch_icnStatCd );
		}
		if( !"".equals( srch_title ) ){
			srch_title = URLDecoder.decode( srch_title, "UTF-8" );
			commandMap.put( "srch_title", srch_title );
		}
		if( !"".equals( srch_icnxNumb ) ){
			srch_icnxNumb = URLDecoder.decode( srch_icnxNumb, "UTF-8" );
			commandMap.put( "srch_icnxNumb", srch_icnxNumb );
		}
		if( !"".equals( srch_rgstStartDate ) ){
			srch_rgstStartDate = URLDecoder.decode( srch_rgstStartDate, "UTF-8" );
			commandMap.put( "srch_rgstStartDate", srch_rgstStartDate );
		}
		if( !"".equals( srch_rgstEndDate ) ){
			srch_rgstEndDate = URLDecoder.decode( srch_rgstEndDate, "UTF-8" );
			commandMap.put( "srch_rgstEndDate", srch_rgstEndDate );
		}
		if( !"".equals( srch_rgstName ) ){
			srch_rgstName = URLDecoder.decode( srch_rgstName, "UTF-8" );
			commandMap.put( "srch_rgstName", srch_rgstName );
		}
		
		fdcrAd55Service.fdcrAd55List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "개인저작물 목록 조회" );
		return "icnissu/fdcrAd55List1.tiles";
	}

	/**
	 * 개인저작물 상세
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/icnissu/fdcrAd55View1.page" )
	public String fdcrAd55View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "CR_ID", "" );
		commandMap.put( "BORD_SEQN", "" );

		fdcrAd55Service.fdcrAd55View1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_fileList", commandMap.get( "ds_fileList" ) );
		System.out.println( "개인저작물 상세" );
		return "test";
	}

	/**
	 * ICN 발급번호 채번
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/icnissu/fdcrAd55IcnInfo1.page" )
	public String fdcrAd55IcnInfo1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "GENRE", "" );

		fdcrAd55Service.fdcrAd55IcnInfo1( commandMap );
		model.addAttribute( "ds_icnInfo", commandMap.get( "ds_icnInfo" ) );
		System.out.println( "ICN 발급번호 채번" );
		return "test";
	}

	/**
	 * 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/icnissu/fdcrAd55Download1.page" )
	public void fdcrAd55Download1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "CR_ID", "" );
		commandMap.put( "BORD_SEQN", "" );

		fdcrAd55Service.fdcrAd55View1( commandMap );

		List list = (List) commandMap.get( "ds_fileList" );
		for( int i = 0; i < list.size(); i++ ){
			Map map = (Map) list.get( i );
			String prpsMastKey = (String) map.get( "PRPS_MAST_KEY" );
			if( prpsMastKey.equals( commandMap.get( "PRPS_MAST_KEY" ) ) ){
				String filePath = (String) map.get( "FILE_PATH" );
				String fileName = (String) map.get( "FILE_NAME" );
				String realFileName = (String) map.get( "REAL_FILE_NAME" );
				download( request, response, filePath + realFileName, fileName );
			}
		}
	}

	/**
	 * ICN 발급번호 저장
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/icnissu/fdcrAd55insert1.page" )
	public void fdcrAd55insert1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "COMM_ID", "" );
		commandMap.put( "COMM_RGST_IDNT", "" );
		commandMap.put( "CR_ID", "" );

		boolean isSuccess = fdcrAd55Service.fdcrAd55Insert1( commandMap );
		returnAjaxString( response, isSuccess );
	}

	/**
	 * ICN 정보 저장
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/icnissu/fdcrAd55insert2.page" )
	public void fdcrAd55insert2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "COMM_STAT_CD", "" );
		commandMap.put( "COMM_STAT_DESC", "" );
		commandMap.put( "CR_ID", "" );
		String[] CR_IDS = request.getParameterValues( "CR_ID" );
		commandMap.put( "CR_ID", CR_IDS );

		boolean isSuccess = fdcrAd55Service.fdcrAd55Insert2( commandMap );
		returnAjaxString( response, isSuccess );
	}

}
