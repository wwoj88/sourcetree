package kr.or.copyright.mls.console.cmpgn.inter;

import java.util.List;
import java.util.Map;

public interface FdcrAdB3Dao {

	public List campPartList(Map map);
	
	public List eventMgntList(Map map);
	
	public List eventMgntDetail(Map map);
	
	public void eventMgntDelete(Map map);
	
	public void eventMgntUpdate(Map map);
	
	public void eventMgntInsert(Map map);
}
