package kr.or.copyright.mls.myStat.model;

import kr.or.copyright.mls.common.BaseObject;

public class File extends BaseObject {

	@Override
    public String toString() {
	return "File [attcSeqn=" + attcSeqn + ", fileAttcCd=" + fileAttcCd
		+ ", fileNameCd=" + fileNameCd + ", fileName=" + fileName
		+ ", filePath=" + filePath + ", fileSize=" + fileSize
		+ ", realFileName=" + realFileName + ", rgstDttm=" + rgstDttm
		+ ", rgstIdnt=" + rgstIdnt + ", applyWriteYmd=" + applyWriteYmd
		+ ", applyWriteSeq=" + applyWriteSeq + ", fileNameCdNm="
		+ fileNameCdNm + "]";
    }
	private int attcSeqn;
	private String fileAttcCd;
	private String fileNameCd;
	private String fileName;
	private String filePath;
	private int fileSize;
	private String realFileName;
	private String rgstDttm;
	private String rgstIdnt;
	
	private String applyWriteYmd;   //신청서작성YMD
	private int applyWriteSeq;      //신청서작성SEQ

	private String fileNameCdNm;
	
	public int getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(int attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public String getFileAttcCd() {
		return fileAttcCd;
	}
	public void setFileAttcCd(String fileAttcCd) {
		this.fileAttcCd = fileAttcCd;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileNameCd() {
		return fileNameCd;
	}
	public void setFileNameCd(String fileNameCd) {
		this.fileNameCd = fileNameCd;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public int getFileSize() {
		return fileSize;
	}
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}
	public String getRealFileName() {
		return realFileName;
	}
	public void setRealFileName(String realFileName) {
		this.realFileName = realFileName;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public int getApplyWriteSeq() {
		return applyWriteSeq;
	}
	public void setApplyWriteSeq(int applyWriteSeq) {
		this.applyWriteSeq = applyWriteSeq;
	}
	public String getApplyWriteYmd() {
		return applyWriteYmd;
	}
	public void setApplyWriteYmd(String applyWriteYmd) {
		this.applyWriteYmd = applyWriteYmd;
	}
	public String getFileNameCdNm() {
		return fileNameCdNm;
	}
	public void setFileNameCdNm(String fileNameCdNm) {
		this.fileNameCdNm = fileNameCdNm;
	}
	
	
}