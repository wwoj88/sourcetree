package kr.or.copyright.mls.console.notdstbmanage;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 미분배 보상금 관리 > 방송음악
 * 
 * @author ljh
 */
@Controller
public class FdcrAd74Controller extends DefaultController{

	@Resource( name = "fdcrAd74Service" )
	private FdcrAd74ServiceImpl fdcrAd74Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd74Controller.class );

	/**
	 * 미분배 보상금 관리 > 미분배 보상금 관리
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/notdstbmanage/fdcrAd74List1.page" )
	public String fdcrAd74List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		/*
		commandMap.put( "INMT_YEAR", "" );
		commandMap.put( "TRST_ORGN_CODE", "" );
		commandMap.put( "END_YEAR", "" );
		commandMap.put( "HALF", "" );
		commandMap.put( "INMT_DIVS", "" );
		commandMap.put( "INMT_ITEMS", "" );
		*/
	
		// 파라미터 셋팅
		String INMT_YEAR = EgovWebUtil.getString( commandMap, "inmtYear" );
		if(INMT_YEAR.equals( "" ) || INMT_YEAR == null || ("").equals( INMT_YEAR )){
			commandMap.put( "INMT_YEAR", INMT_YEAR );
			model.addAttribute( "ds_list", commandMap.get( "ds_List" ) );
			System.out.println( "미분배 보상금 관리 조회" );
			return "notdstbmanage/fdcrAd74List1.tiles";
		}else {
		commandMap.put( "INMT_YEAR", INMT_YEAR );
		/*
		if(INMT_YEAR.equals( "" ) || INMT_YEAR == null){
			commandMap.put( "INMT_YEAR", "2016" );
		}
		 */
		fdcrAd74Service.fdcrAd74List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_List" ) );
		System.out.println( "미분배 보상금 관리 조회" );
		return "notdstbmanage/fdcrAd74List1.tiles";
		}
	}

	/**
	 * 미분배보상금 관리 > 미분배 보상금 관리 등록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/notdstbmanage/fdcrAd74Insert1.page" )
	public String fdcrAd74Insert1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		/*
		String inmtYears = EgovWebUtil.getString( commandMap, "INMT_YEAR" );
		String[] halfs = request.getParameterValues( "half" );
		String[] openYsnos = request.getParameterValues( "openYsno" );
		//String[] openYsnos = request.getParameterValues( "strOpenYsno" );
		String[] totAlltInmts = request.getParameterValues( "totAlltInmt" );
		String[] a1s = request.getParameterValues( "a1" );
		String[] a2s = request.getParameterValues( "a2" );
		String[] a3s = request.getParameterValues( "a3" );
		String[] b1s = request.getParameterValues( "b1" );
		String[] b2s = request.getParameterValues( "b2" );
		String[] b3s = request.getParameterValues( "b3" );
		String[] c1s = request.getParameterValues( "c1" );
		String[] c2s = request.getParameterValues( "c2" );
		commandMap.put( "OPEN_YSNO", openYsnos );
		commandMap.put( "INMT_YEAR", inmtYears );
		commandMap.put( "HALF", halfs );
		commandMap.put( "TOT_ALLT_INMT", totAlltInmts );
		commandMap.put( "A1", a1s );
		commandMap.put( "A2", a2s );
		commandMap.put( "A3", a3s );
		commandMap.put( "B1", b1s );
		commandMap.put( "B2", b2s );
		commandMap.put( "B3", b3s );
		commandMap.put( "C1", c1s );
		commandMap.put( "C2", c2s );
		*/
		
		/*
		String[] OPEN_YSNOS = (String[]) commandMap.get( "openYsno" );
		for( int i = 0; i < OPEN_YSNOS.length; i++ ){
			if(OPEN_YSNOS[i].equals( "1" )){
				String[] INMT_YEARS = request.getParameterValues( "inmtYear" );
				String[] HALFS = request.getParameterValues( "half" );
				String[] A1S = request.getParameterValues( "a1" );
				String[] A2S = request.getParameterValues( "a2" );
				String[] A3S = request.getParameterValues( "a3" );
				String[] B1S = request.getParameterValues( "b1" );
				String[] B2S = request.getParameterValues( "b2" );
				String[] B3S = request.getParameterValues( "b3" );
				String[] C1S = request.getParameterValues( "c1" );
				String[] C2S = request.getParameterValues( "c2" );
				String[] TOT_ALLT_INMTS = request.getParameterValues( "totAlltInmt" );
				commandMap.put( "OPEN_YSNO", OPEN_YSNOS[i] );
				commandMap.put( "INMT_YEAR", INMT_YEARS[i] );
				commandMap.put( "HALF", HALFS[i] );
				commandMap.put( "TOT_ALLT_INMT", TOT_ALLT_INMTS[i] );
				commandMap.put( "A1", A1S[i] );
				commandMap.put( "A2", A2S[i] );
				commandMap.put( "A3", A3S[i] );
				commandMap.put( "B1", B1S[i] );
				commandMap.put( "B2", B2S[i] );
				commandMap.put( "B3", B3S[i] );
				commandMap.put( "C1", C1S[i] );
				commandMap.put( "C2", C2S[i] );
			}
		}
		*/

		String INMT_YEAR = EgovWebUtil.getString( commandMap, "INMT_YEAR" );
		String HALF = EgovWebUtil.getString( commandMap, "HALF" );
		String A1 = EgovWebUtil.getString( commandMap, "A1" );
		String A2 = EgovWebUtil.getString( commandMap, "A2" );
		String A3 = EgovWebUtil.getString( commandMap, "A3" );
		String B1 = EgovWebUtil.getString( commandMap, "B1" );
		String B2 = EgovWebUtil.getString( commandMap, "B2" );
		String B3 = EgovWebUtil.getString( commandMap, "B3" );
		String C1 = EgovWebUtil.getString( commandMap, "C1" );
		String C2 = EgovWebUtil.getString( commandMap, "C2" );
		String TOT_ALLT_INMT = EgovWebUtil.getString( commandMap, "TOT_ALLT_INMT" );
		String OPEN_YSNO = EgovWebUtil.getString( commandMap, "OPEN_YSNO" );
		
		boolean isSuccess = fdcrAd74Service.fdcrAd74Insert1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "미분배 보상금 관리 등록" );
		if( isSuccess ){
			return returnUrl( model, "저장했습니다.", "/console/notdstbmanage/fdcrAd74List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/notdstbmanage/fdcrAd74List1.page" );
		}
	}

}
