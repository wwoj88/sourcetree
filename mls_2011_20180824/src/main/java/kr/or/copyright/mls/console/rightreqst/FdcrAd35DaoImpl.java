package kr.or.copyright.mls.console.rightreqst;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.rightreqst.inter.FdcrAd35Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAd35Dao" )
public class FdcrAd35DaoImpl extends EgovComAbstractDAO implements FdcrAd35Dao{

	public List inmtList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmt.inmtList", map );
	}

	public List inmtListCount( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmt.inmtListCount", map );
	}

	public void inmtRsltUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminInmt.inmtRsltUpdate", map );
	}

	public void inmtUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminInmt.inmtUpdate", map );
	}

	public void inmtUserYsnoUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminInmt.inmtUserYsnoUpdate", map );
	}

	public List inmtRsltList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmt.inmtRsltList", map );
	}

	public int inmtRsltCnt( Map map ){
		Integer max = (Integer) getSqlMapClientTemplate().queryForObject( "AdminInmt.inmtRsltCnt", map );
		return max.intValue();
	}

	public String inmtPrpsTitle( Map map ){
		String prpsTitile = (String) getSqlMapClientTemplate().queryForObject( "AdminInmt.inmtPrpsTitle", map );
		return prpsTitile.toString();
	}

	public List inmtDealStat( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmt.inmtDealStat", map );
	}

	public List admInmtList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmt.admInmtList", map );
	}

}
