package kr.or.copyright.mls.mobile.model;

import java.util.HashMap;
import java.util.Map;

import kr.or.copyright.mls.support.constant.Constants;

public class Common {
	private static final String header = "<header><h1><a href=\"/m/main.do\">저작권찾기</a>"
		+ "</h1><div><a href=\"javascript:history.back();\"><strong>이전</strong></a></div></header>";
private static final String footer = "<footer><div class=\"foot_lk\"><a href=\"/m/main.do\">전체메뉴</a>"
		+ "<a href=\""+Constants.DOMAIN_HOME_FORPC+"\" class=\"pc\">PC버젼</a>"
		+ "<a href=\"javascript:window.scrollTo(0,1);\">맨위로</a></div>COPYRIGHT&copy; <time datetime=\"2011-09-29\">2011</time>"
		+ "<strong>한국저작권위원회</strong>. All right reserved.</footer>";
	public static String getHeader() {
		return header;
	}
	public static String getFooter() {
		return footer;
	}
	
	public static Map<String,Object> commonHtm = new HashMap<String, Object>();
	public static Map<String, Object> getCommonHtm() {
		commonHtm.put("headerHtm", header);
		commonHtm.put("footerHtm", footer);
		return commonHtm;
	}
	public static void setCommonHtm(Map<String, Object> commonHtm) {
		Common.commonHtm = commonHtm;
	}

}
