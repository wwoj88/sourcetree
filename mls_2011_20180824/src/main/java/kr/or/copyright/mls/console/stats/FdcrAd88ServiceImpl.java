package kr.or.copyright.mls.console.stats;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.stats.inter.FdcrAd87Dao;
import kr.or.copyright.mls.console.stats.inter.FdcrAd88Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd88Service" )
public class FdcrAd88ServiceImpl extends CommandService implements FdcrAd88Service{

	// old AdminConnDao
	@Resource( name = "fdcrAd87Dao" )
	private FdcrAd87Dao fdcrAd87Dao;

	/**
	 * 일별 접속 통계
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd88List1( Map<String, Object> commandMap ) throws Exception{
		// DAO호출
		List list = (List) fdcrAd87Dao.dayList( commandMap );

		commandMap.put( "ds_list", list ); // 로그인 사용자 정보

	}
}
