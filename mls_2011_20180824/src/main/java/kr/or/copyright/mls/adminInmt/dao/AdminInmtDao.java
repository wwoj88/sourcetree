package kr.or.copyright.mls.adminInmt.dao;

import java.util.List;
import java.util.Map;

public interface AdminInmtDao {
	
	public List inmtList(Map map);
	
	public List inmtListCount(Map map);	
	
	public void inmtRsltUpdate(Map map);
	
	public void inmtUpdate(Map map);
	
	public void inmtUserYsnoUpdate(Map map);
	
	public List inmtRsltList(Map map);
	
	public int inmtRsltCnt(Map map);
	
	public String inmtPrpsTitle(Map map);
	
	public List inmtDealStat(Map map);
	
	public List admInmtList(Map map);
}
