package kr.or.copyright.mls.adminWorksMgnt.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.tobesoft.platform.data.Dataset;


/**
 * Class 내용 기술
 *
 * @author : 정병호
 * @since : 20120910
 * @version : 1.0
 * @see: 위탁관리 저작물 보고
 *
 */

public class AdminWorksMgntSqlMapDao extends SqlMapClientDaoSupport implements AdminWorksMgntDao {
    
    //위탁관리 음악저작물정보 일괄등록1
    public List adminCommMusicInsert(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
    	
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0;
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
			
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		    
		}else{
			ArrayList inList = new ArrayList();
			for(int i=0; i<ds_excel.getRowCount(); i++){
			
				Map map = null;
				try {
				    //seq를 사용하여 worksId 생성
				    int worksId = getCommWorksId();
		        
				    map = getMap(ds_excel, i);
				    map.put("WORKS_ID", worksId);//저작물아이디
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("RGST_IDNT", rgstIdnt);//등록자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int isExist = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks",map));
				    
				    if(isExist == 0){
				    	
						//위탁관리저작물 등록(공통)
						cFlag = adminCommWorksInsert(map);
						
						if(cFlag){
						    //위탁관리장르별저작물 등록
						    gFlag = adminCommMusicInsert(map);
						    
						    if(gFlag){//위탁관리장르별 등록성공 > cnt 증가해주기!
						    	cnt++;
						    }else{//위탁관리장르별 등록실패 > 등록된 위탁관리저작물 삭제 해주기, 리스트에 현재 저작물 담고 리턴해야함
						    	adminCommWorksDelete(map);
						    }
						}
						
				    }else{
				    	cFlag = false;
				    }
				    
				    if(!cFlag || !gFlag) {
				    	resultList.add(map);
				    }
				}catch (Exception e) {
				    // TODO Auto-generated catch block
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		}
		
		reportMap.put("REPT_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		
		return resultList;
    }
    
    
    //위탁관리 음악저작물정보 일괄수정1 
    public List adminCommMusicUpdate(Dataset ds_condition, Dataset ds_report,
		    Dataset ds_excel) throws Exception {
    	
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG&REPT_SEQN
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0; //수정 성공 cnt
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("MODI_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
			
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		
		}else{
			getSqlMapClientTemplate().getSqlMapClient().startBatch();
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				
		    	Map map = null;
				
		    	try{
				    map = getMap(ds_excel, i);
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("MODI_IDNT", rgstIdnt);//수정자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    int worksId = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getForUpdateCommWorksId",map));
				    if(worksId == 0){
					cFlag = false;
				    }else{//저작물id를 구했으므로 수정처리를 해야함
					map.put("WORKS_ID", worksId);//저작물아이디
					try {
						getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commMusicUpdate",map);
						gFlag=true;
					} catch (Exception e) {
						gFlag=false;
					}
					//gFlag = adminCommMusicUpdate(map);
					if(gFlag){
						try {
							getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commWorksUpdate",map);
							cFlag=true;
						} catch (Exception e) {
							cFlag=false;
						}
					   // cFlag = adminCommWorksUpdate(map);
					}
				    }
				    if(cFlag && gFlag){
					cnt++;
				    }
				    if(!cFlag || !gFlag) {
					resultList.add(map);
				    }
				}catch(Exception e) {
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		   getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		}
		
		reportMap.put("MODI_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		
		return resultList;
    }
    
    
    //위탁관리 어문저작물정보 일괄등록2
    public List adminCommBookInsert(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
		
    	List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0;
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
			}
		}else{
		    for(int i=0; i<ds_excel.getRowCount(); i++){
		    	
				Map map = null;
				try {
				    //seq를 사용하여 worksId 생성
				    int worksId = getCommWorksId();
		        
				    map = getMap(ds_excel, i);
				    map.put("WORKS_ID", worksId);//저작물아이디
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("RGST_IDNT", rgstIdnt);//등록자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int isExist = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks",map));
				    
				    if(isExist == 0){
				    	
						//위탁관리저작물 등록(공통)
						cFlag = adminCommWorksInsert(map);
						
						if(cFlag){
						    //위탁관리장르별저작물 등록
						    gFlag = adminCommBookInsert(map);
						    
						    if(gFlag){//위탁관리장르별 등록성공 > cnt 증가해주기!
						    	cnt++;
						    }else{//위탁관리장르별 등록실패 > 등록된 위탁관리저작물 삭제 해주기, 리스트에 현재 저작물 담고 리턴해야함
						    	adminCommWorksDelete(map);
						    }
						}
						
				    }else{
				    	cFlag = false;
				    }
				    
				    if(!cFlag || !gFlag) {
				    	resultList.add(map);
				    }
				}catch (Exception e) {
				    // TODO Auto-generated catch block
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }    
		}
		
		reportMap.put("REPT_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		return resultList;
    }
    
    //위탁관리 어문저작물정보 일괄수정2 
    public List adminCommBookUpdate(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
	List resultList = new ArrayList();
	
	int rFlag = 0;//리포트FLAG&REPT_SEQN
	boolean cFlag = true;//저작물FLAG
	boolean gFlag = true;//장르별저작물FLAG
	
	Map reportMap = getMap(ds_report, 0);
	Map conditionMap = getMap(ds_condition,0);
	
	int cnt = 0; //수정 성공 cnt
	
	GregorianCalendar d  = new GregorianCalendar();
	
	//보고연월
	String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
        String.valueOf(d.get(Calendar.MONTH)+1));
	
	String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
	
	//장르코드
	String genreCd = (String) conditionMap.get("GENRE_CODE");
	
	//등록자아이디
	String rgstIdnt = (String) conditionMap.get("USER_IDNT");
	
	//위탁관리기관ID
	String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
	
	reportMap.put("REPT_YMD", reptYmd);//보고연월
	reportMap.put("GENRE_CD", genreCd);//장르코드
	reportMap.put("MODI_IDNT", rgstIdnt);//등록자아이디
	
	rFlag = adminCommWorksReportInsert(reportMap);
	
	if(rFlag == 0){
	    for(int i=0; i<ds_excel.getRowCount(); i++){
			Map map = null;
			map = getMap(ds_excel, i);
			resultList.add(map);
	    }
	}else{
		getSqlMapClientTemplate().getSqlMapClient().startBatch();
	    for(int i=0; i<ds_excel.getRowCount(); i++){
			Map map = null;
			try{
			    map = getMap(ds_excel, i);
			    map.put("REPT_YMD", reptYmd);//보고년월
			    map.put("GENRE_CD", genreCd);//장르코드
			    map.put("MODI_IDNT", rgstIdnt);//수정자아이디
			    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
			    
			    int worksId = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getForUpdateCommWorksId",map));
			    
			    if(worksId == 0){
			    	cFlag = false;
			    }else{//저작물id를 구했으므로 수정처리를 해야함
					map.put("WORKS_ID", worksId);//저작물아이디
					try {
						getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commBookUpdate",map);
						gFlag=true;
					} catch (Exception e) {
						gFlag=false;
					}
//					gFlag = adminCommBookUpdate(map);
				
					if(gFlag){
						try {
							getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commWorksUpdate",map);
							cFlag=true;
						} catch (Exception e) {
							cFlag=false;
						}
//					    cFlag = adminCommWorksUpdate(map);
					}
			    }
			    if(cFlag && gFlag){
			    	cnt++;
			    }
			    if(!cFlag || !gFlag) {
			    	resultList.add(map);
			    }
			}catch(Exception e) {
			    e.printStackTrace();
			    resultList.add(map);
			}
	    }
	    getSqlMapClientTemplate().getSqlMapClient().executeBatch();
	}
	reportMap.put("MODI_WORKS_CONT", cnt) ;
	reportMap.put("REPT_SEQN", rFlag);
	
	if(rFlag != 0){
	    adminCommWorksReportUpdate(reportMap);
	}
	
	return resultList;
    }
    

    //위탁관리 방송대본저작물정보 일괄등록3 
    public List adminCommScriptInsert(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0;
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
			Map map = null;
			map = getMap(ds_excel, i);
			resultList.add(map);
		    }
		}else{
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try {
				    //seq를 사용하여 worksId 생성
				    int worksId = getCommWorksId();
		        
				    map = getMap(ds_excel, i);
				    map.put("WORKS_ID", worksId);//저작물아이디
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("RGST_IDNT", rgstIdnt);//등록자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int isExist = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks",map));
				    
				    if(isExist == 0){
						//위탁관리저작물 등록(공통)
						cFlag = adminCommWorksInsert(map);
						
						if(cFlag){
						    //위탁관리장르별저작물 등록
						    gFlag = adminCommScriptInsert(map);
						    
						    if(gFlag){//위탁관리장르별 등록성공 > cnt 증가해주기!
						    	cnt++;
						    }else{//위탁관리장르별 등록실패 > 등록된 위탁관리저작물 삭제 해주기, 리스트에 현재 저작물 담고 리턴해야함
						    	adminCommWorksDelete(map);
						    }
						}
						    
					
				    }else{
				    	cFlag = false;
				    }
				    if(!cFlag || !gFlag) {
				    	resultList.add(map);
				    }
				}catch (Exception e) {
				    // TODO Auto-generated catch block
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }    
		}
		
		reportMap.put("REPT_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		return resultList;
    }
    
    //위탁관리 방송대본저작물정보 일괄수정3 
    public List adminCommScriptUpdate(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG&REPT_SEQN
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0; //수정 성공 cnt
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("MODI_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
			Map map = null;
			map = getMap(ds_excel, i);
			resultList.add(map);
		    }
		}else{
			getSqlMapClientTemplate().getSqlMapClient().startBatch();
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try{
				    map = getMap(ds_excel, i);
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("MODI_IDNT", rgstIdnt);//수정자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int worksId = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getForUpdateCommWorksId",map));
				    
				    if(worksId == 0){
				    	cFlag = false;
				    }else{//저작물id를 구했으므로 수정처리를 해야함
						map.put("WORKS_ID", worksId);//저작물아이디
						try {
							getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commScriptUpdate",map);
							gFlag=true;
						} catch (Exception e) {
							gFlag=false;
						}
//						gFlag = adminCommScriptUpdate(map);
						if(gFlag){
							try {
							getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commWorksUpdate",map);
								cFlag=true;
							} catch (Exception e) {
								cFlag=false;
							}
//						    cFlag = adminCommWorksUpdate(map);
						}
				    }
				    if(cFlag && gFlag){
				    	cnt++;
				    }
				    if(!cFlag || !gFlag) {
				    	resultList.add(map);
				    }
				}catch(Exception e) {
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		    getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		}
		reportMap.put("MODI_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		
		return resultList;
    }

    
    //위탁관리 영화저작물정보 일괄등록4 
    public List adminCommMovieInsert(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
    	
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0;
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
			Map map = null;
			map = getMap(ds_excel, i);
			resultList.add(map);
		    }
		}else{
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try {
				    //seq를 사용하여 worksId 생성
				    int worksId = getCommWorksId();
		        
				    map = getMap(ds_excel, i);
				    map.put("WORKS_ID", worksId);//저작물아이디
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("RGST_IDNT", rgstIdnt);//등록자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int isExist = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks",map));
				    
				    if(isExist == 0){
					//위탁관리저작물 등록(공통)
					cFlag = adminCommWorksInsert(map);
					
					if(cFlag){
					    //위탁관리장르별저작물 등록
					    gFlag = adminCommMovieInsert(map);
					    if(gFlag){//위탁관리장르별 등록성공 > cnt 증가해주기!
					    	cnt++;
					    }else{//위탁관리장르별 등록실패 > 등록된 위탁관리저작물 삭제 해주기, 리스트에 현재 저작물 담고 리턴해야함
					    	adminCommWorksDelete(map);
					    }
					}
					    
					
				    }else{
				    	cFlag = false;
				    }
				    if(!cFlag || !gFlag) {
				    	resultList.add(map);
				    }
				}catch (Exception e) {
				    // TODO Auto-generated catch block
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }    
		}
		
		reportMap.put("REPT_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		return resultList;
    }
    
    //위탁관리 영화저작물정보 일괄수정4 
    public List adminCommMovieUpdate(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
    	
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG&REPT_SEQN
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0; //수정 성공 cnt
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("MODI_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
			Map map = null;
			map = getMap(ds_excel, i);
			resultList.add(map);
		    }
		}else{
			getSqlMapClientTemplate().getSqlMapClient().startBatch();
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try{
				    map = getMap(ds_excel, i);
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("MODI_IDNT", rgstIdnt);//수정자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int worksId = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getForUpdateCommWorksId",map));
				    
				    if(worksId == 0){
				    	cFlag = false;
				    }else{//저작물id를 구했으므로 수정처리를 해야함
						map.put("WORKS_ID", worksId);//저작물아이디
						try {
							getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commMovieUpdate",map);
							gFlag=true;
						} catch (Exception e) {
							gFlag=false;
						}
//						gFlag = adminCommMovieUpdate(map);
						if(gFlag){
							try {
								getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commWorksUpdate",map);
								cFlag=true;
							} catch (Exception e) {
								cFlag=false;
							}
//						    cFlag = adminCommWorksUpdate(map);
						}
				    }
				    if(cFlag && gFlag){
				    	cnt++;
				    }
				    if(!cFlag || !gFlag) {
				    	resultList.add(map);
				    }
				}catch(Exception e) {
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		    getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		}
		reportMap.put("MODI_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		
		return resultList;
    }

    
    //위탁관리 방송저작물정보 일괄등록5 
    public List adminCommBroadcastInsert(Dataset ds_condition,
	    Dataset ds_report, Dataset ds_excel) throws Exception {
    	
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0;
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try {
				    //seq를 사용하여 worksId 생성
				    int worksId = getCommWorksId();
		        
				    map = getMap(ds_excel, i);
				    map.put("WORKS_ID", worksId);//저작물아이디
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("RGST_IDNT", rgstIdnt);//등록자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int isExist = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks",map));
				    
				    if(isExist == 0){
					//위탁관리저작물 등록(공통)
					cFlag = adminCommWorksInsert(map);
					
					if(cFlag){
					    //위탁관리장르별저작물 등록
					    gFlag = adminCommBroadcastInsert(map);
					    if(gFlag){//위탁관리장르별 등록성공 > cnt 증가해주기!
					    	cnt++;
					    }else{//위탁관리장르별 등록실패 > 등록된 위탁관리저작물 삭제 해주기, 리스트에 현재 저작물 담고 리턴해야함
					    	adminCommWorksDelete(map);
					    }
					}
					    
					
				    }else{
				    	cFlag = false;
				    }
				    if(!cFlag || !gFlag) {
				    	resultList.add(map);
				    }
				}catch (Exception e) {
				    // TODO Auto-generated catch block
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }    
		}
		
		reportMap.put("REPT_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		return resultList;
    }
    
    //위탁관리 방송저작물정보 일괄수정5 
    public List adminCommBroadcastUpdate(Dataset ds_condition,
	    Dataset ds_report, Dataset ds_excel) throws Exception {
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG&REPT_SEQN
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0; //수정 성공 cnt
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("MODI_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
			getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try{
				    map = getMap(ds_excel, i);
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("MODI_IDNT", rgstIdnt);//수정자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    int worksId = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getForUpdateCommWorksId",map));
				    if(worksId == 0){
				    	cFlag = false;
				    }else{//저작물id를 구했으므로 수정처리를 해야함
				    	map.put("WORKS_ID", worksId);//저작물아이디
				    	try {
				    		getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commBroadcastUpdate",map);
							gFlag=true;
						} catch (Exception e) {
							gFlag=false;
						}
//				    	gFlag = adminCommBroadcastUpdate(map);
						if(gFlag){
							try {
								getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commWorksUpdate",map);
								cFlag=true;
							} catch (Exception e) {
								cFlag=false;
							}
//						    cFlag = adminCommWorksUpdate(map);
						}
				    }
				    if(cFlag && gFlag){
				    	cnt++;
				    }
				    
				    if(!cFlag || !gFlag) { 
				    	resultList.add(map);
				    }
				}catch(Exception e) {
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		    getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		}
		reportMap.put("MODI_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		
		return resultList;
    }

    
    //위탁관리 뉴스저작물정보 일괄등록6 
    public List adminCommNewsInsert(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {

    	List resultList = new ArrayList();
	
		int rFlag = 0;//리포트FLAG
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0;
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try {
				    //seq를 사용하여 worksId 생성
				    int worksId = getCommWorksId();
		        
				    map = getMap(ds_excel, i);
				    map.put("WORKS_ID", worksId);//저작물아이디
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("RGST_IDNT", rgstIdnt);//등록자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int isExist = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks",map));
				    
				    if(isExist == 0){
					//위탁관리저작물 등록(공통)
					cFlag = adminCommWorksInsert(map);
					
					if(cFlag){
					    //위탁관리장르별저작물 등록
					    gFlag = adminCommNewsInsert(map);
					    if(gFlag){//위탁관리장르별 등록성공 > cnt 증가해주기!
					    	cnt++;
					    }else{//위탁관리장르별 등록실패 > 등록된 위탁관리저작물 삭제 해주기, 리스트에 현재 저작물 담고 리턴해야함
					    	adminCommWorksDelete(map);
					    }
					}
					    
					
				    }else{
				    	cFlag = false;
				    }
				    if(!cFlag || !gFlag) {
				    	resultList.add(map);
				    }
				}catch (Exception e) {
				    // TODO Auto-generated catch block
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }    
		}
		
		reportMap.put("REPT_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		return resultList;
    }
    
    //위탁관리 뉴스저작물정보 일괄수정6 
    public List adminCommNewsUpdate(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
    	
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG&REPT_SEQN
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0; //수정 성공 cnt
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("MODI_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
			getSqlMapClientTemplate().getSqlMapClient().startBatch();
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try{
				    map = getMap(ds_excel, i);
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("MODI_IDNT", rgstIdnt);//수정자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    int worksId = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getForUpdateCommWorksId",map));
				    if(worksId == 0){
				    	cFlag = false;
				    }else{//저작물id를 구했으므로 수정처리를 해야함
						map.put("WORKS_ID", worksId);//저작물아이디
						try {
							getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commNewsUpdate",map);
							gFlag=true;
						} catch (Exception e) {
							gFlag=false;
						}
//						gFlag = adminCommNewsUpdate(map);
						if(gFlag){
							try {
								getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commWorksUpdate",map);
								cFlag=true;
							} catch (Exception e) {
								cFlag=false;
							}
//						    cFlag = adminCommWorksUpdate(map);
						}
				    }
				    if(cFlag && gFlag){
				    	cnt++;
				    }
				    
				    if(!cFlag || !gFlag) { 
				    	resultList.add(map);
				    }
				}catch(Exception e) {
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		    getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		}
		reportMap.put("MODI_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		
		return resultList;
    }

   //위탁관리 미술 저작물정보 일괄등록7 
    public List adminCommArtInsert(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
		List resultList = new ArrayList();
		
		int rFlag = 0; //리포트 flag&seq
		boolean cFlag = true; //저작물 flag
		boolean gFlag = true; //장르별저작물 flag
		
		Map reportMap = getMap(ds_report,0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0;
		
		GregorianCalendar d = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try{
				    //seq를 사용하여 worksId 생성
				    int worksId = getCommWorksId();
				    map = getMap(ds_excel, i);
				    map.put("WORKS_ID", worksId);//저작물아이디
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("RGST_IDNT", rgstIdnt);//등록자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int isExist = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks",map));
				    
				    if(isExist == 0){
						//위탁관리저작물 등록(공통)
						cFlag = adminCommWorksInsert(map);
						if(cFlag){//위탁관리저작물 등록성공 > 장르별 테이블 등록!
						    //위탁관리장르별저작물 등록
						    gFlag = adminCommArtInsert(map);
						    if(gFlag){//위탁관리장르별 등록성공 > cnt 증가해주기!
						    	cnt++;
						    }else{//위탁관리장르별 등록실패 > 등록된 위탁관리저작물 삭제 해주기, 리스트에 현재 저작물 담고 리턴해야함
						    	adminCommWorksDelete(map);
						    }
						}
				    }else{
				    	cFlag = false;
				    }
				    if(!cFlag || !gFlag) {
					resultList.add(map);
				    }
				}catch (Exception e) {
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		}
		reportMap.put("REPT_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		return resultList;
    }
    
   //위탁관리 미술저작물정보 일괄수정7 
    public List adminCommArtUpdate(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
    	
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG&REPT_SEQN
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0; //수정 성공 cnt
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("MODI_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
			getSqlMapClientTemplate().getSqlMapClient().startBatch();
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try{
				    map = getMap(ds_excel, i);
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("MODI_IDNT", rgstIdnt);//수정자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    int worksId = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getForUpdateCommWorksId",map));
				    if(worksId == 0){
				    	cFlag = false;
				    }else{//저작물id를 구했으므로 수정처리를 해야함
						map.put("WORKS_ID", worksId);//저작물아이디
						try {
							getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commArtUpdate",map);
							gFlag=true;
						} catch (Exception e) {
							gFlag=false;
						}
//						gFlag = adminCommArtUpdate(map);
						if(gFlag){
							try {
								getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commWorksUpdate",map);
								cFlag=true;
							} catch (Exception e) {
								cFlag=false;
							}
//						    cFlag = adminCommWorksUpdate(map);
						}
				    }
				    if(cFlag && gFlag){
				    	cnt++;
				    }
				    
				    if(!cFlag || !gFlag) { 
				    	resultList.add(map);
				    }
				}catch(Exception e) {
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		    getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		}
		reportMap.put("MODI_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		
		return resultList;
    }
    
//  위탁관리 이미지 저작물정보 일괄등록8 
    public List adminCommImageInsert(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
		List resultList = new ArrayList();
		
		int rFlag = 0; //리포트 flag&seq
		boolean cFlag = true; //저작물 flag
		boolean gFlag = true; //장르별저작물 flag
		
		Map reportMap = getMap(ds_report,0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0;
		
		GregorianCalendar d = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try{
				    //seq를 사용하여 worksId 생성
				    int worksId = getCommWorksId();
				    map = getMap(ds_excel, i);
				    map.put("WORKS_ID", worksId);//저작물아이디
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("RGST_IDNT", rgstIdnt);//등록자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int isExist = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks",map));
				    
				    if(isExist == 0){
						//위탁관리저작물 등록(공통)
						cFlag = adminCommWorksInsert(map);
						if(cFlag){//위탁관리저작물 등록성공 > 장르별 테이블 등록!
						    //위탁관리장르별저작물 등록
						    gFlag = adminCommImageInsert(map);
						    if(gFlag){//위탁관리장르별 등록성공 > cnt 증가해주기!
						    	cnt++;
						    }else{//위탁관리장르별 등록실패 > 등록된 위탁관리저작물 삭제 해주기, 리스트에 현재 저작물 담고 리턴해야함
						    	adminCommWorksDelete(map);
						    }
						}
				    }else{
				    	cFlag = false;
				    }
				    if(!cFlag || !gFlag) {
					resultList.add(map);
				    }
				}catch (Exception e) {
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		}
		reportMap.put("REPT_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		return resultList;
    }
    
//  위탁관리 이미지저작물정보 일괄수정8 
    public List adminCommImageUpdate(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {

		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG&REPT_SEQN
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0; //수정 성공 cnt
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("MODI_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
			getSqlMapClientTemplate().getSqlMapClient().startBatch();
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try{
				    map = getMap(ds_excel, i);
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("MODI_IDNT", rgstIdnt);//수정자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    int worksId = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getForUpdateCommWorksId",map));
				    if(worksId == 0){
				    	cFlag = false;
				    }else{//저작물id를 구했으므로 수정처리를 해야함
						map.put("WORKS_ID", worksId);//저작물아이디
						try {
							getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commImageUpdate",map);
							gFlag=true;
						} catch (Exception e) {
							gFlag=false;
						}
//						gFlag = adminCommImageUpdate(map);
						if(gFlag){
							try {
								getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commWorksUpdate",map);
								cFlag=true;
							} catch (Exception e) {
								cFlag=false;
							}
//						    cFlag = adminCommWorksUpdate(map);
						}
				    }
				    if(cFlag && gFlag){
				    	cnt++;
				    }
				    
				    if(!cFlag || !gFlag) { 
				    	resultList.add(map);
				    }
				}catch(Exception e) {
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		    getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		}
		reportMap.put("MODI_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		
		return resultList;
    }
    
    //위탁관리 기타저작물정보 일괄등록9    
    public List adminCommSideInsert(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0;
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try {
				    //seq를 사용하여 worksId 생성
				    int worksId = getCommWorksId();
		        
				    map = getMap(ds_excel, i);
				    map.put("WORKS_ID", worksId);//저작물아이디
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("RGST_IDNT", rgstIdnt);//등록자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    
				    int isExist = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks",map));
				    
				    if(isExist == 0){
						//위탁관리저작물 등록(공통)
						cFlag = adminCommWorksInsert(map);
						
						if(cFlag){
						    //위탁관리장르별저작물 등록
						    gFlag = adminCommSideInsert(map);
						    if(gFlag){//위탁관리장르별 등록성공 > cnt 증가해주기!
						    	cnt++;
						    }else{//위탁관리장르별 등록실패 > 등록된 위탁관리저작물 삭제 해주기, 리스트에 현재 저작물 담고 리턴해야함
						    	adminCommWorksDelete(map);
						    }
						}
						    
					
				    }else{
				    	cFlag = false;
				    }
				    if(!cFlag || !gFlag) {
				    	resultList.add(map);
				    }
				}catch (Exception e) {
				    // TODO Auto-generated catch block
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }    
		}
		
		reportMap.put("REPT_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		return resultList;
    }
    
    //위탁관리 기타저작물정보 일괄수정9 
    public List adminCommSideUpdate(Dataset ds_condition, Dataset ds_report,
	    Dataset ds_excel) throws Exception {
		List resultList = new ArrayList();
		
		int rFlag = 0;//리포트FLAG&REPT_SEQN
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0; //수정 성공 cnt
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("MODI_IDNT", rgstIdnt);//등록자아이디
		
		rFlag = adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
			getSqlMapClientTemplate().getSqlMapClient().startBatch();
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				try{
				    map = getMap(ds_excel, i);
				    map.put("REPT_YMD", reptYmd);//보고년월
				    map.put("GENRE_CD", genreCd);//장르코드
				    map.put("MODI_IDNT", rgstIdnt);//수정자아이디
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    int worksId = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getForUpdateCommWorksId",map));
				    if(worksId == 0){
					cFlag = false;
				    }else{//저작물id를 구했으므로 수정처리를 해야함
						map.put("WORKS_ID", worksId);//저작물아이디
						try {
							getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commSideUpdate",map);
							gFlag=true;
						} catch (Exception e) {
							gFlag=false;
						}
//						gFlag = adminCommSideUpdate(map);
						if(gFlag){
							try {
								getSqlMapClientTemplate().getSqlMapClient().update("AdminWorksMgnt.commWorksUpdate",map);
								cFlag=true;
							} catch (Exception e) {
								cFlag=false;
							}
//						    cFlag = adminCommWorksUpdate(map);
						}
				    }
				    if(cFlag && gFlag){
				    	cnt++;
				    }
				    
				    if(!cFlag || !gFlag) { 
				    	resultList.add(map);
				    }
				}catch(Exception e) {
				    e.printStackTrace();
				    resultList.add(map);
				}
		    }
		    getSqlMapClientTemplate().getSqlMapClient().executeBatch();
		}
		reportMap.put("MODI_WORKS_CONT", cnt) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
		    adminCommWorksReportUpdate(reportMap);
		}
		
		return resultList;
    }
    
    
    //위탁관리 저작물 보고정보 등록(공통) 
    public int adminCommWorksReportInsert(Map map) throws SQLException {

	int rFlag = 0;
	
	try {
	    rFlag = (Integer)getSqlMapClientTemplate().insert("AdminWorksMgnt.commWorksReportInsert",map);
	} catch (Exception e) {
	    rFlag = 0;
	}
	
	return rFlag;
    }
    
    //위탁관리 저작물 보고정보(등록건수등록) 수정(공통)
    @Transactional(readOnly = false)
    public void adminCommWorksReportUpdate(Map map){
    	getSqlMapClientTemplate().update("AdminWorksMgnt.commWorksReportUpdate",map);
    }
    
    
    //위탁관리 저작물 등록(공통)
    public boolean adminCommWorksInsert(Map map) {
    	
		boolean cFlag = true;
		
		try {
		    getSqlMapClientTemplate().insert("AdminWorksMgnt.commWorksInsert",map);
		    cFlag = true;
		} catch (Exception e) {
		    cFlag = false;
		}
		
		return cFlag;
    }
    
    //위탁관리 저작물 등록(공통)
    public boolean adminCommWorksUpdate(Map map) {
    	
		boolean cFlag = true;
		
		try {
		    getSqlMapClientTemplate().update("AdminWorksMgnt.commWorksUpdate",map);
		    cFlag = true;
		} catch (Exception e) {
		    cFlag = false;
		}
		
		return cFlag;
    }
    
    //위탁관리 저작물 음악 등록1
    public boolean adminCommMusicInsert(Map map) {
    	
        boolean gFlag = true;
        	
    	try {
    	    getSqlMapClientTemplate().insert("AdminWorksMgnt.commMusicInsert",map);
    	    gFlag = true;
    	} catch (Exception e) {
    	    gFlag = false;
    	}
    	
    	return gFlag;
    }
    
    //위탁관리 저작물 어문 등록2
    public boolean adminCommBookInsert(Map map) {
    	
		boolean gFlag = true;
		
		try {
		    getSqlMapClientTemplate().insert("AdminWorksMgnt.commBookInsert",map);
		    gFlag = true;
		} catch (Exception e) {
		    gFlag = false;
		}
		
		return gFlag;
    }
    
    //위탁관리 저작물 방송대본 등록3
    public boolean adminCommScriptInsert(Map map) {
    	
		boolean gFlag = true;
		
		try {
		    getSqlMapClientTemplate().insert("AdminWorksMgnt.commScriptInsert",map);
		    gFlag = true;
		} catch (Exception e) {
		    gFlag = false;
		}
		
		return gFlag;
    }
    
    
    //위탁관리 저작물 영화 등록4
    public boolean adminCommMovieInsert(Map map) {
		
    	boolean gFlag = true;
		
		try {
		    getSqlMapClientTemplate().insert("AdminWorksMgnt.commMovieInsert",map);
		    gFlag = true;
		} catch (Exception e) {
		    gFlag = false;
		}
		
		return gFlag;
    }
    
    
    //위탁관리 저작물 방송 등록5
    public boolean adminCommBroadcastInsert(Map map) {
		
    	boolean gFlag = true;
		
		try {
		    getSqlMapClientTemplate().insert("AdminWorksMgnt.commBroadcastInsert",map);
		    gFlag = true;
		} catch (Exception e) {
		    gFlag = false;
		}
		
		return gFlag;
    }
    
    
    //위탁관리 저작물 뉴스 등록6
    public boolean adminCommNewsInsert(Map map) {
		
    	boolean gFlag = true;
		
		try {
		    getSqlMapClientTemplate().insert("AdminWorksMgnt.commNewsInsert",map);
		    gFlag = true;
		} catch (Exception e) {
		    gFlag = false;
		}
		
		return gFlag;
    }
    
    //위탁관리 저작물 미술 등록7
    public boolean adminCommArtInsert(Map map) {
		
    	boolean gFlag = true;
		
		try {
		    getSqlMapClientTemplate().insert("AdminWorksMgnt.commArtInsert",map);
		    gFlag = true;
		} catch (Exception e) {
		    gFlag = false;
		}
		
		return gFlag;
    }
    
    //위탁관리 저작물 이미지 등록8
    public boolean adminCommImageInsert(Map map) {
		
    	boolean gFlag = true;
		
		try {
		    getSqlMapClientTemplate().insert("AdminWorksMgnt.commImageInsert",map);
		    gFlag = true;
		} catch (Exception e) {
		    gFlag = false;
		}
		
		return gFlag;
    }
    
    //위탁관리 저작물 기타 등록9
    public boolean adminCommSideInsert(Map map) {
		
    	boolean gFlag = true;
		
		try {
		    getSqlMapClientTemplate().insert("AdminWorksMgnt.commSideInsert",map);
		    gFlag = true;
		} catch (Exception e) {
		    gFlag = false;
		}
		
		return gFlag;
    }
    
    
    
    
    
    //위탁관리 저작물 음악 수정
    public boolean adminCommMusicUpdate(Map map){
		
    	boolean gFlag = true;
		
    	try {
    	    getSqlMapClientTemplate().update("AdminWorksMgnt.commMusicUpdate",map);
    	    gFlag = true;
    	} catch (Exception e) {
    	    gFlag = false;
    	}
    	
    	return gFlag;
    }
    
  //위탁관리 저작물 어문 수정2
    public boolean adminCommBookUpdate(Map map){
    	
    	boolean gFlag = true;
	
    	try {
    	    getSqlMapClientTemplate().update("AdminWorksMgnt.commBookUpdate",map);
    	    gFlag = true;
    	} catch (Exception e) {
    	    gFlag = false;
    	}
    	
    	return gFlag;
    }
    
  //위탁관리 저작물 방송대본 수정3
    public boolean adminCommScriptUpdate(Map map){
	
    	boolean gFlag = true;
	
    	try {
    	    getSqlMapClientTemplate().update("AdminWorksMgnt.commScriptUpdate",map);
    	    gFlag = true;
    	} catch (Exception e) {
    	    gFlag = false;
    	}
    	
    	return gFlag;
    }
    
  //위탁관리 저작물 영화 수정4
    public boolean adminCommMovieUpdate(Map map){
	
    	boolean gFlag = true;
	
    	try {
    	    getSqlMapClientTemplate().update("AdminWorksMgnt.commMovieUpdate",map);
    	    gFlag = true;
    	} catch (Exception e) {
    	    gFlag = false;
    	}
    	
    	return gFlag;
    }
    
  //위탁관리 저작물 방송 수정5
    public boolean adminCommBroadcastUpdate(Map map){
	
    	boolean gFlag = true;
	
    	try {
    	    getSqlMapClientTemplate().update("AdminWorksMgnt.commBroadcastUpdate",map);
    	    gFlag = true;
    	} catch (Exception e) {
    	    gFlag = false;
    	}
    	
    	return gFlag;
    }
    
  //위탁관리 저작물 뉴스 수정6
    public boolean adminCommNewsUpdate(Map map){
	
    	boolean gFlag = true;
	
    	try {
    	    getSqlMapClientTemplate().update("AdminWorksMgnt.commNewsUpdate",map);
    	    gFlag = true;
    	} catch (Exception e) {
    	    gFlag = false;
    	}
    	
    	return gFlag;
    }
    
    //위탁관리 저작물 미술 수정7
    public boolean adminCommArtUpdate(Map map){
	boolean gFlag = true;
	try {
	    getSqlMapClientTemplate().update("AdminWorksMgnt.commArtUpdate",map);
	    gFlag = true;
	} catch (Exception e) {
	    gFlag = false;
	}
	return gFlag;
    }
    
  //위탁관리 저작물 기타 수정8
    public boolean adminCommSideUpdate(Map map){
	
    	boolean gFlag = true;
	
    	try {
    	    getSqlMapClientTemplate().update("AdminWorksMgnt.commSideUpdate",map);
    	    gFlag = true;
    	} catch (Exception e) {
    	    gFlag = false;
    	}
    	
    	return gFlag;
    }
    
    //위탁관리 저작물 works_id 생성(공통)
    public int getCommWorksId(){
	
    	int worksId = Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getCommWorksId"));
    	return worksId;
    }
    
    //위탁관리 저작물 삭제(공통)
    public void adminCommWorksDelete(Map map){
    	getSqlMapClientTemplate().delete("AdminWorksMgnt.adminCommWorksDelete", map);
    }
    
    public HashMap getMap(Dataset ds, int rnum ) throws Exception {

		if (ds == null || ds.getRowCount() == 0) {
		    return null;
		}
		HashMap result = new HashMap();
	
		for (int i = 0; i < ds.getColumnCount(); i++) {
	
		    String columnName = ds.getColumnId(i);
	
		    result.put(columnName, ds.getColumnAsObject(rnum, columnName));
		}
	
		return result;
    }
    
    
    //위탁관리 저작물 목록 조회
    public List worksMgntList(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList",map);
    }
    
    //위탁관리 저작물 목록조회 20141126 이병원
    public List worksMgntList_01(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList_01", map);
    }
    
    //  위탁관리 저작물 음악 조회
    public List worksMgntList_01_music(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList_01_music", map);
    }
    
    //위탁관리 저작물 어문 조회
    public List worksMgntList_01_book(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList_01_book", map);
    }
    
    //  위탁관리 저작물 방송대본 조회
    public List worksMgntList_01_script(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList_01_script", map);
    }
    
    //  위탁관리 저작물 영화 조회
    public List worksMgntList_01_movie(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList_01_movie", map);
    }
    
    //  위탁관리 저작물 방송 조회
    public List worksMgntList_01_broadcast(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList_01_broadcast", map);
    }
    
    //  위탁관리 저작물 뉴스 조회
    public List worksMgntList_01_news(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList_01_news", map);
    }
    
    //  위탁관리 저작물 미술 조회
    public List worksMgntList_01_art(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList_01_art", map);
    }
    
    //  위탁관리 저작물 이미지 조회
    public List worksMgntList_01_image(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList_01_image", map);
    }
    
    //  위탁관리 저작물 기타 조회
    public List worksMgntList_01_side(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntList_01_side", map);
    }
    
    //위탁관리 저작물 작성기관명 조회
    public List searchOrgnName(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.searchName", map);
    }
    
    //위탁관리 저작물 목록 페이징 정보
    public List totalRowWorksMgntList(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.totalRowWorksMgntList",map);
    }
    
    //위탁관리 저작물 목록 페이징 20141126 이병원
    public List totalRowWorksMgntList_01(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.totalRowWorksMgntList_01", map);
    }
    
    //위탁관리 월별 보고현황 조회
    public List worksMgntReptView(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntReptView",map);
    }
    
    
    //위탁관리 월별 정보
    public List worksMgntReptInfo(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntReptInfo",map);
    }
    
    //위탁관리 최신 기관 정보(기관코드, ID)
    public List worksMgntReptInfo2(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntReptInfo2",map);
    }
    
    
    //등록부연계 월별 등록현황 조회
    public List crosWorksMgntReptView(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.crosWorksMgntReptInfo",map);
    }


    public void adminWorksMgntNoDataInsert(Dataset ds_condition,
	    Dataset ds_report) throws Exception {
	
    	Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		int cnt = 0;
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		
		adminCommWorksReportInsert(reportMap);
    }
    
    //위탁관리 저작물 기존 등록여부 조회(기관코드,내부관리ID,기관명)
    public List worksMgntChkCnt(Dataset ds_condition, Dataset ds_excel) throws Exception {
    	
    	Map resultMap = new HashMap();
    	List resultList = new ArrayList();
    	List worksList = new ArrayList(); 
    	Map map = new HashMap();
    	Map conditionMap = getMap(ds_condition,0);    	
    	int allCnt = 0;
    	int updateCnt = 0;    	
    	int insertCnt = 0;
    	
    	allCnt = ds_excel.getRowCount();   	
    	map.put("TRST_ORGN_CODE",conditionMap.get("TRST_ORGN_CODE"));
    	
    	for(int i=0; i<allCnt; i++){        	
    		worksList.add(getMap(ds_excel, i));        	
        }
    	map.put("worksList",worksList);
    	
		// 위탁관리 저작물 기존 등록여부 조회
		updateCnt = (Integer)getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks2",map);
	
        insertCnt = allCnt-updateCnt;
        	
        resultMap.put("INSERT_CNT",insertCnt);
        resultMap.put("UPDATE_CNT",updateCnt);    	
        resultList.add(resultMap);
    	
    	
    	
    	return resultList;
    }
    
    
    
    
    

    public List worksMgntReptMonthView(Map map) {
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntReptMonthView",map);
    }
    
    public List worksMgntReptMonthCoCnt(Map map) {
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntReptMonthCoCnt",map);
    }
    
    public List worksMgntReptViewCoCnt(Map map) {
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntReptViewCoCnt",map);
    }

    public List worksMgntAppDate(Map map) {
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntAppDate",map);
    }

    public List worksMgntReptYearView(Map map) {
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntReptYearView",map);
    }

	public int getExistCommWorks(Map map) throws Exception {
		return (Integer) getSqlMapClientTemplate().queryForObject("AdminWorksMgnt.getExistCommWorks",map);
	}


	public String commWorksInsertBacth(List list) throws Exception {
		return (String) getSqlMapClientTemplate().insert("AdminWorksMgnt.commWorksInsertBacth", list);
	}


	public String commMusicInsertBacth(List list) throws Exception {
		return (String) getSqlMapClientTemplate().insert("AdminWorksMgnt.commMusicInsertBacth", list);
	}


	public String commBookInsertBacth(List list) throws Exception {
		return (String) getSqlMapClientTemplate().insert("AdminWorksMgnt.commBookInsertBacth", list);
	}


	public String commScriptInsertBacth(List list) throws Exception {
		return (String) getSqlMapClientTemplate().insert("AdminWorksMgnt.commScriptInsertBacth", list);
	}


	public String commMovieInsertBacth(List list) throws Exception {
		return (String) getSqlMapClientTemplate().insert("AdminWorksMgnt.commMovieInsertBacth", list);
	}


	public String commBroadcastInsertBacth(List list) throws Exception {
		return (String) getSqlMapClientTemplate().insert("AdminWorksMgnt.commBroadcastInsertBacth", list);
	}


	public String commNewsInsertBacth(List list) throws Exception {
		return (String) getSqlMapClientTemplate().insert("AdminWorksMgnt.commNewsInsertBacth", list);
	}


	public String commArtInsertBacth(List list) throws Exception {
		return (String) getSqlMapClientTemplate().insert("AdminWorksMgnt.commArtInsertBacth", list);
	}
	
	public String commImageInsertBacth(List list) throws Exception {
		return (String) getSqlMapClientTemplate().insert("AdminWorksMgnt.commImageInsertBacth", list);
	}


	public String commSideInsertBacth(List list) throws Exception {
		return (String) getSqlMapClientTemplate().insert("AdminWorksMgnt.commSideInsertBacth", list);
	}
	
	//위탁관리 저작물 목록 엑셀 다운로드
    public List worksMgntExcelDown(Map map){
    	return getSqlMapClientTemplate().queryForList("AdminWorksMgnt.worksMgntExcelDown",map);
    }

}
