package kr.or.copyright.mls.console.event;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 이벤트 등록 및 관리 > 이벤트 현황조회
 * 
 * @author ljh
 */
@Controller
public class FdcrAdA3Controller extends DefaultController{

	@Resource( name = "fdcrAdA3Service" )
	private FdcrAdA3ServiceImpl fdcrAdA3Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA3Controller.class );

	/**
	 * 이벤트 현황조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA3List1.page" )
	public String fdcrAdA3List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA3Service.fdcrAdA3List1( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "이벤트 현황조회" );
		return "/event/fdcrAdA3List1.tiles";
	}

}
