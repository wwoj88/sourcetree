package kr.or.copyright.mls.common.common.utils;

public class XssUtil {
    /**
     * XSS ���� ó��.
     * 
     * @param data
     * @return
     */
     public static String unscript(String data) {
         if (data == null || data.trim().equals("")) {
     	return "";
         }
         	        
         String ret = data;
         	        
         ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
         ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
                 
         ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
         ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
         	        
         ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
         ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
         	        
         ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "");
         ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "");
         	        
         ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "");
         ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "");
         
         ret = ret.replaceAll("\"", "&quot;");
	     ret = ret.replaceAll("\'", "&#39;");
	     
	     ret = ret.replaceAll("<", "&lt;");
	     ret = ret.replaceAll(">", "&gt;");
         System.out.println("ret : "+ret );
         return ret;
     }
     
     public static String[] unscriptArr(String[] data) {
    	 String[] values = data;
 		
 		if(values==null){
 			return null;			
 		}
 		
 		for (int i = 0; i < values.length; i++) {			
 			if (values[i] != null) {				
 			
 				String ret = values[i];
 				
 				ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
 		        ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
 		         	        
 		        ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
 		        ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
 		        	        
 		        ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
 		        ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
 		         	        
 		        ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "");
 		        ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "");
 		        	        
 		        ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "");
 		        ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "");
 		        
 		        ret = ret.replaceAll("\"", "&quot;");
 		        ret = ret.replaceAll("\'", "&#39;");
 		        
 		        ret = ret.replaceAll("<", "&lt;");
 			    ret = ret.replaceAll(">", "&gt;");
 		        
 				values[i] = ret;
 			} else {
 				values[i] = null;
 			}
 		}

		return values;
	}
}
