package kr.or.copyright.mls.console.civilservices;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.civilservices.inter.CivilServicesDAO;
import kr.or.copyright.mls.console.civilservices.model.CivilServicesDTO;

@Repository( "CivilServicesDAO" )
public class CivilServiceDaoImpl  extends EgovComAbstractDAO implements CivilServicesDAO{

	public void insertCivilServices( Map<String, Object> commandMap ){
		getSqlMapClientTemplate().insert("AdminCivilServices.insertCivilServices",commandMap);
	}

	public String civilServicesFromCivilSeqn(){
		return (String)getSqlMapClientTemplate().queryForObject("AdminCivilServices.civilServicesFromCivilSeqn");
	}
	public void insertCivilServicesFile(CivilServicesDTO civilServicesDTO) {
		getSqlMapClientTemplate().insert("AdminCivilServices.insertCivilServicesFile",civilServicesDTO);
	}

	public List selectCivilServices(Map<String, Object> commandMap){
		return getSqlMapClientTemplate().queryForList( "AdminCivilServices.selectCivilServices",commandMap );
	}

	public int selectCountCivilServices(){
		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminCivilServices.selectCountCivilServices" );
	}

	public List selectCivilServicesByCivilSeqn( Map<String, Object> commandMap ){
		return getSqlMapClientTemplate().queryForList("AdminCivilServices.selectCivilServicesByCivilSeqn",commandMap);
	}

	public List selectCivilServicesFile( Map<String, Object> commandMap ){
		return getSqlMapClientTemplate().queryForList("AdminCivilServices.selectCivilServicesFile",commandMap);
	}

	public void updateCivilServiceState( Map<String, Object> commandMap ){
		getSqlMapClientTemplate().update("AdminCivilServices.updateCivilServiceState",commandMap);
	}

	public List selectCivilServicesComment( Map<String, Object> commandMap ){
		return getSqlMapClientTemplate().queryForList("AdminCivilServices.selectCivilServicesComment",commandMap);
	}
	
	public List selectCivilServicesDownLoadFile( Map<String, Object> commandMap ){
		return getSqlMapClientTemplate().queryForList("AdminCivilServices.selectCivilServicesDownLoadFile",commandMap);
	}

	public List selectCivilServicesExcelList( Map<String, Object> commandMap ){
		return getSqlMapClientTemplate().queryForList("AdminCivilServices.selectCivilServicesExcelList",commandMap);
	}

	public void updateCivilServicesComment( Map<String, Object> commandMap ){
		getSqlMapClientTemplate().update("AdminCivilServices.updateCivilServicesComment",commandMap);
	}

	public void deleteCivilServicesFile( String deleteFileNum ){
		getSqlMapClientTemplate().update("AdminCivilServices.deleteCivilServicesFile",deleteFileNum);
	}

	public void deleteCivilServicesByCivilSeqn( String deleteList ){
		getSqlMapClientTemplate().update("AdminCivilServices.deleteCivilServicesByCivilSeqn",deleteList);
	}

	public void deleteCivilServicesComment( String deleteFileNum ){
		getSqlMapClientTemplate().update("AdminCivilServices.deleteCivilServicesComment",deleteFileNum);
	}

	public void deleteCivilServicesState( String deleteCivilServiceNum ){
		getSqlMapClientTemplate().update("AdminCivilServices.deleteCivilServicesState",deleteCivilServiceNum);
	}

	public List companySrch( Map<String, Object> companyNameList ){
		return getSqlMapClientTemplate().queryForList("AdminCivilServices.companySrch",companyNameList);
	}

	public List personSrch( Map<String, Object> commandMap ){
		return getSqlMapClientTemplate().queryForList("AdminCivilServices.personSrch",commandMap);
	}

	public List selectCivilServicesPerson( Map<String, Object> returnList ){
		return  getSqlMapClientTemplate().queryForList("AdminCivilServices.selectCivilServicesPerson",returnList);
	}

	public List selectCivilStats(){
		return getSqlMapClientTemplate().queryForList("AdminCivilServices.selectCivilStats");
	}
}
