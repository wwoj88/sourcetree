package kr.or.copyright.mls.console.legalmanage;

import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.legalmanage.inter.FdcrAd42Dao;
import kr.or.copyright.mls.console.legalmanage.inter.FdcrAd42Service;
import kr.or.copyright.mls.console.rightreqst.inter.FdcrAd3501Dao;

import org.springframework.stereotype.Service;

@Service( "fdcrAd42Service" )
public class FdcrAd42ServiceImpl extends CommandService implements FdcrAd42Service{

	@Resource( name = "fdcrAd3501Dao" )
	private FdcrAd3501Dao fdcrAd3501Dao;

	@Resource( name = "fdcrAd42Dao" )
	private FdcrAd42Dao fdcrAd42Dao;

	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	/**
	 * 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd42List1( Map<String, Object> commandMap ) throws Exception{
		List pageList = (List) fdcrAd42Dao.totalRowStatList( commandMap ); // 페이징카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		// DAO 호출
		List list = (List) fdcrAd42Dao.statList( commandMap );
		commandMap.put( "ds_List", list );

	}

	/**
	 * 저작물 상세 및 수정 폼
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd42UpdateForm1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd42Dao.statDetailList( commandMap );
		List fileList = (List) fdcrAd42Dao.statFileList( commandMap );

		commandMap.put( "ds_list", list );
		commandMap.put( "ds_file", fileList );

	}
	
	/**
	 * 저작물 상세
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> statDetailList( Map<String, Object> commandMap ) throws SQLException{
		return fdcrAd42Dao.statDetailList( commandMap );
	}

	/**
	 * 저작물 상세  첨부서류 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> statFileList( Map<String, Object> commandMap ) throws SQLException{
		return fdcrAd42Dao.statFileList( commandMap );
	}

	/**
	 * CLMS 저작물 목록 조회 팝업
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd42Popup1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		String gubun = commandMap.get( "GUBUN" ).toString();
		List list = null;
		//List pageList = null;

		if( gubun.equals( "1" ) ){ // 음악
			list = (List) fdcrAd42Dao.clmsWorksList( commandMap );
			//pageList = (List) fdcrAd42Dao.totalClmsWorksList( commandMap );
		}else if( gubun.equals( "2" ) ){ // 어문
			list = (List) fdcrAd42Dao.clmsBookList( commandMap );
			//pageList = (List) fdcrAd42Dao.totalClmsBookList( commandMap );
		}else if( gubun.equals( "3" ) ){ // 방송대본
			list = (List) fdcrAd42Dao.clmsBroadList( commandMap );
			//pageList = (List) fdcrAd42Dao.totalClmsBroadList( commandMap );
		}else if( gubun.equals( "4" ) ){ // 방송
			list = (List) fdcrAd42Dao.clmsBroadCastList( commandMap );
			//pageList = (List) fdcrAd42Dao.totalClmsBroadCastList( commandMap );
		}else if( gubun.equals( "5" ) ){ // 영화
			list = (List) fdcrAd42Dao.clmsMovieList( commandMap );
			//pageList = (List) fdcrAd42Dao.totalClmsMovieList( commandMap );
		}else if( gubun.equals( "6" ) ){ // 이미지
			list = (List) fdcrAd42Dao.clmsImageList( commandMap );
			//pageList = (List) fdcrAd42Dao.totalClmsImageList( commandMap );
		}

		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_list", list );
		//commandMap.put( "ds_page", pageList );

	}

	/**
	 * CLMS 저작물 상세 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd42View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		String prpsDivs = commandMap.get( "PRPS_DIVS" ).toString();
		String PRPS_IDNT = (String) commandMap.get( "PRPS_IDNT" );
		System.out.println("PRPS_IDNT::::::"+PRPS_IDNT);
		List list = null;

		if( prpsDivs.equals( "M" ) ){ // 음악
			list = (List) fdcrAd3501Dao.rghtMuscDetail( commandMap );
		}else if( prpsDivs.equals( "O" ) ){ // 어문
			list = (List) fdcrAd3501Dao.rghtBookDetail( commandMap );
		}else if( prpsDivs.equals( "C" ) ){ // 방송대본
			list = (List) fdcrAd3501Dao.rghtScriptDetail( commandMap );
		}else if( prpsDivs.equals( "V" ) ){ // 영화
			list = (List) fdcrAd3501Dao.rghtMvieDetail( commandMap );
		}else if( prpsDivs.equals( "R" ) ){ // 방송
			list = (List) fdcrAd3501Dao.rghtBroadcastDetail( commandMap );
		}else if( prpsDivs.equals( "N" ) ){ // 뉴스
			list = (List) fdcrAd3501Dao.rghtNewsDetail( commandMap );
		}
		commandMap.put( "ds_works_info", list );

	}

	/**
	 * CLMS 저작물 상세 조회(음악,어문)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd42View2( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		String prpsDivs = commandMap.get( "PRPS_DIVS" ).toString();
		List list = null;

		if( prpsDivs.equals( "M" ) ){ // 음악
			list = (List) fdcrAd42Dao.clmsMuscDetail( commandMap );
		}else if( prpsDivs.equals( "O" ) ){ // 어문
			list = (List) fdcrAd42Dao.clmsBookDetail( commandMap );
		}
		commandMap.put( "ds_works_info", list );

	}

	/**
	 * 저작물 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd42Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String[] RGST_IDNTS = (String[]) commandMap.get( "RGST_IDNT" );
			String[] MAILS = (String[]) commandMap.get( "MAIL" );
			String[] TITES = (String[]) commandMap.get( "TITE" );
			String[] BORD_DESCS = (String[]) commandMap.get( "BORD_DESC" );
			String[] GUBUNS = (String[]) commandMap.get( "GUBUN" );

			String[] APPR_DTTMS = (String[]) commandMap.get( "APPR_DTTM" );
			String[] CLMS_TITES = (String[]) commandMap.get( "CLMS_TITE" );
			String[] ICNS = (String[]) commandMap.get( "ICN" );
			String[] GENRES = (String[]) commandMap.get( "GENRE" );
			String[] CR_IDS = (String[]) commandMap.get( "CR_ID" );
			String[] NR_IDS = (String[]) commandMap.get( "NR_ID" );
			String[] ALBUM_IDS = (String[]) commandMap.get( "ALBUM_ID" );
			String[] BOOK_NR_IDS = (String[]) commandMap.get( "BOOK_NR_ID" );
			String[] WORK_FILE_NAMES = (String[]) commandMap.get( "WORK_FILE_NAME" );
			String[] LICENSOR_NAMES = (String[]) commandMap.get( "LICENSOR_NAME" );
			
			String[] FILE_NAMES = (String[]) commandMap.get( "FILE_NAME" );
			String[] FILE_PATHS = (String[]) commandMap.get( "FILE_PATH" );
			String[] FILE_SIZES = (String[]) commandMap.get( "FILE_SIZE" );
			String[] REAL_FILE_NAMES = (String[]) commandMap.get( "REAL_FILE_NAME" );
			String[] ATTC_SEQNS = (String[]) commandMap.get( "ATTC_SEQN" );
			String[] THREADEDS = (String[]) commandMap.get( "THREADED" );
			//String[] UPDATE_FGS = (String[]) commandMap.get( "UPDATE_FG" );
			String[] CLIENT_FILES = (String[]) commandMap.get( "CLIENT_FILE" );
			
			String BORD_SEQN = (String) commandMap.get( "BORD_SEQN" );
			String MENU_SEQN = (String) commandMap.get( "MENU_SEQN" );
			
			String FILD_1 = (String) commandMap.get( "FILD_1" );
			String FILD_2 = (String) commandMap.get( "FILD_2" );
			String FILD_3 = (String) commandMap.get( "FILD_3" );
			String FILD_4 = (String) commandMap.get( "FILD_4" );
			String FILD_5 = (String) commandMap.get( "FILD_5" );
			String FILD_6 = (String) commandMap.get( "FILD_6" );
			String FILD_7 = (String) commandMap.get( "FILD_7" );
			String FILD_8 = (String) commandMap.get( "FILD_8" );
			String FILD_9 = (String) commandMap.get( "FILD_9" );
			String FILD_10 = (String) commandMap.get( "FILD_10" );
			String FILD_11 = (String) commandMap.get( "FILD_11" );
			String FILD_12 = (String) commandMap.get( "FILD_12" );

			// detail DELETE처리
			for( int i = 0; ATTC_SEQNS != null && i < ATTC_SEQNS.length; i++ ){
				Map<String, Object> deleteMap = new HashMap<String, Object>();
				deleteMap.put( "ATTC_SEQN", ATTC_SEQNS[i] );
				deleteMap.put( "THREADED", THREADEDS[i] );
				deleteMap.put( "BORD_SEQN", BORD_SEQN );
				deleteMap.put( "MENU_SEQN", MENU_SEQN );

				fdcrAd42Dao.statFileDelete( deleteMap );

				String fileName = commandMap.get( REAL_FILE_NAMES[i] ).toString();

				try{
					File file = new File( (String) deleteMap.get( "FILE_PATH" ), fileName );
					if( file.exists() ){
						file.delete();
					}
				}
				catch( Exception e ){
					e.printStackTrace();
				}
			}

			// Master DELETE처리
			/*
			for( int i = 0; i < TITES.length; i++ ){
				Map<String, Object> deleteMap = new HashMap<String, Object>();
				deleteMap.put( "BORD_SEQN", BORD_SEQN );
				deleteMap.put( "MENU_SEQN", MENU_SEQN );
				fdcrAd42Dao.statDelete( deleteMap );
				fdcrAd42Dao.statFildDelete( deleteMap );
			}
			*/

			int statSeqn = fdcrAd42Dao.statSeqn();

			// Master INSERT, UPDATE처리
			for( int i = 0; i < TITES.length; i++ ){

				Map<String, Object> param = new HashMap<String, Object>();

				if(RGST_IDNTS != null && RGST_IDNTS.length > 0){
					param.put( "RGST_IDNT", RGST_IDNTS[i] );
				}
				if(MAILS != null && MAILS.length > 0){
					param.put( "MAIL", MAILS[i] );
				}
				if(TITES != null && TITES.length > 0){
					param.put( "TITE", TITES[i] );
				}
				if(BORD_DESCS != null && BORD_DESCS.length > 0){
					param.put( "BORD_DESC", BORD_DESCS[i] );
				}
				if(GUBUNS != null && GUBUNS.length > 0){
					param.put( "GUBUN", GUBUNS[i] );
				}
				if(APPR_DTTMS != null && APPR_DTTMS.length > 0){
					param.put( "APPR_DTTM", APPR_DTTMS[i] );
				}
				if(CLMS_TITES != null && CLMS_TITES.length > 0){
					param.put( "CLMS_TITE", CLMS_TITES[i] );
				}
				if(ICNS != null && ICNS.length > 0){
					param.put( "ICN", ICNS[i] );
				}
				if(GENRES != null && GENRES.length > 0){
					param.put( "GENRE", GENRES[i] );
				}
				if(CR_IDS != null && CR_IDS.length > 0){
					param.put( "CR_ID", CR_IDS[i] );
				}
				if(NR_IDS != null && NR_IDS.length > 0){
					param.put( "NR_ID", NR_IDS[i] );
				}
				if(ALBUM_IDS != null && ALBUM_IDS.length > 0){
					param.put( "ALBUM_ID", ALBUM_IDS[i] );
				}
				if(BOOK_NR_IDS != null && BOOK_NR_IDS.length > 0){
					param.put( "BOOK_NR_ID", BOOK_NR_IDS[i] );
				}
				if(WORK_FILE_NAMES != null && WORK_FILE_NAMES.length > 0){
					param.put( "WORK_FILE_NAME", WORK_FILE_NAMES[i] );
				}
				if(LICENSOR_NAMES != null && LICENSOR_NAMES.length > 0){
					param.put( "LICENSOR_NAME", LICENSOR_NAMES[i] );
				}
				if(FILE_NAMES != null && FILE_NAMES.length > 0){
					param.put( "FILE_NAME", FILE_NAMES[i] );
				}
				if(FILE_PATHS != null && FILE_PATHS.length > 0){
					param.put( "FILE_PATH", FILE_PATHS[i] );
				}
				if(FILE_SIZES != null && FILE_SIZES.length > 0){
					param.put( "FILE_SIZE", FILE_SIZES[i] );
				}
				
				param.put( "MENU_SEQN", MENU_SEQN );
				param.put( "BORD_SEQN", BORD_SEQN );

				fdcrAd42Dao.statUpdate( param );
				param.put( "SEQN", commandMap.get( "BORD_SEQN" ) );

				fdcrAd42Dao.statFildDelete( param );

				for( int x = 1; x < 13; x++ ){
					if( x == 12 ){
						x = 99;
					}
					String to = Integer.toString(x);
					String name = "FILD_" + to;
					//fild = (String) commandMap.get( name );
					String fild = (String) commandMap.get(name);
					System.out.println("fild:::::::::::::::"+fild);

					if( fild != null && fild.length() > 0 ){
						if( fild.equals( "1" ) ){
							param.put( "FILD", x );
							fdcrAd42Dao.statFildInsert( param );
						}
					}
				}

			}

			// detail INSERT, UPDATE처리
			if(ATTC_SEQNS != null && ATTC_SEQNS.length > 0){
				for( int i = 0; i < ATTC_SEQNS.length; i++ ){
	
					Map<String, Object> param = new HashMap<String, Object>();
	
					byte[] file = (byte[]) commandMap.get( CLIENT_FILES[i] );
					String fileName = (String) commandMap.get( REAL_FILE_NAMES[i] );

					Map upload = FileUtil.uploadMiFile( fileName, file );
					param.put( "REAL_FILE_NAME", upload.get( "REAL_FILE_NAME" ) );
					param.put( "FILE_PATH", upload.get( "FILE_PATH" ) );
					param.put( "FILE_NAME", fileName );

					fdcrAd42Dao.statFileInsert( param );
				}
			}

			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 저작물 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
   	public boolean fdcrAd42Delete1( Map<String, Object> commandMap ) throws Exception{

		boolean result = false;
		try{
			String BORD_SEQN = (String) commandMap.get( "BORD_SEQN" );
			String MENU_SEQN = (String) commandMap.get( "MENU_SEQN" );
			
			String[] TITES = (String[]) commandMap.get( "TITE" );
			String[] ATTC_SEQNS = (String[]) commandMap.get( "ATTC_SEQN" );
			String[] THREADEDS = (String[]) commandMap.get( "THREADED" );
			String[] REAL_FILE_NAMES = (String[]) commandMap.get( "REAL_FILE_NAME" );
			
			// detail DELETE처리
				for( int i = 0; ATTC_SEQNS != null && i < ATTC_SEQNS.length; i++ ){
					Map<String, Object> deleteMap = new HashMap<String, Object>();
					deleteMap.put( "ATTC_SEQN", ATTC_SEQNS[i] );
					deleteMap.put( "THREADED", THREADEDS[i] );
					deleteMap.put( "BORD_SEQN", BORD_SEQN );
					deleteMap.put( "MENU_SEQN", MENU_SEQN );

					fdcrAd42Dao.statFileDelete( deleteMap );

					String fileName = commandMap.get( REAL_FILE_NAMES[i] ).toString();

					try{
						File file = new File( (String) deleteMap.get( "FILE_PATH" ), fileName );
						if( file.exists() ){
							file.delete();
						}
					}
					catch( Exception e ){
						e.printStackTrace();
					}
				}

				// Master DELETE처리
				for( int i = 0; i < TITES.length; i++ ){
					Map<String, Object> deleteMap = new HashMap<String, Object>();
					deleteMap.put( "BORD_SEQN", BORD_SEQN );
					deleteMap.put( "MENU_SEQN", MENU_SEQN );
					fdcrAd42Dao.statDelete( deleteMap );
					fdcrAd42Dao.statFildDelete( deleteMap );
				}

			int statSeqn = fdcrAd42Dao.statSeqn();
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	public boolean fdcrAd42Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String[] RGST_IDNTS = (String[]) commandMap.get( "RGST_IDNT" );
			String[] MAILS = (String[]) commandMap.get( "MAIL" );
			String[] TITES = (String[]) commandMap.get( "TITE" );
			String[] BORD_DESCS = (String[]) commandMap.get( "BORD_DESC" );
			String[] GUBUNS = (String[]) commandMap.get( "GUBUN" );

			String[] APPR_DTTMS = (String[]) commandMap.get( "APPR_DTTM" );
			String[] CLMS_TITES = (String[]) commandMap.get( "CLMS_TITE" );
			String[] ICNS = (String[]) commandMap.get( "ICN" );
			String[] GENRES = (String[]) commandMap.get( "GENRE" );
			String[] CR_IDS = (String[]) commandMap.get( "CR_ID" );
			String[] NR_IDS = (String[]) commandMap.get( "NR_ID" );
			String[] ALBUM_IDS = (String[]) commandMap.get( "ALBUM_ID" );
			String[] BOOK_NR_IDS = (String[]) commandMap.get( "BOOK_NR_ID" );
			String[] WORK_FILE_NAMES = (String[]) commandMap.get( "WORK_FILE_NAME" );
			String[] LICENSOR_NAMES = (String[]) commandMap.get( "LICENSOR_NAME" );
			
			String[] FILE_NAMES = (String[]) commandMap.get( "FILE_NAME" );
			String[] FILE_PATHS = (String[]) commandMap.get( "FILE_PATH" );
			String[] FILE_SIZES = (String[]) commandMap.get( "FILE_SIZE" );
			String[] REAL_FILE_NAMES = (String[]) commandMap.get( "REAL_FILE_NAME" );
			String[] ATTC_SEQNS = (String[]) commandMap.get( "ATTC_SEQN" );
			String[] THREADEDS = (String[]) commandMap.get( "THREADED" );
			String[] CLIENT_FILES = (String[]) commandMap.get( "CLIENT_FILE" );
			
			String FILD_1 = (String) commandMap.get( "FILD_1" );
			String FILD_2 = (String) commandMap.get( "FILD_2" );
			String FILD_3 = (String) commandMap.get( "FILD_3" );
			String FILD_4 = (String) commandMap.get( "FILD_4" );
			String FILD_5 = (String) commandMap.get( "FILD_5" );
			String FILD_6 = (String) commandMap.get( "FILD_6" );
			String FILD_7 = (String) commandMap.get( "FILD_7" );
			String FILD_8 = (String) commandMap.get( "FILD_8" );
			String FILD_9 = (String) commandMap.get( "FILD_9" );
			String FILD_10 = (String) commandMap.get( "FILD_10" );
			String FILD_11 = (String) commandMap.get( "FILD_11" );
			String FILD_12 = (String) commandMap.get( "FILD_12" );
			
			// Master DELETE처리
			/*
			for( int i = 0; i < TITES.length; i++ ){
				Map<String, Object> deleteMap = new HashMap<String, Object>();
				deleteMap.put( "BORD_SEQN", BORD_SEQN );
				deleteMap.put( "MENU_SEQN", MENU_SEQN );
				fdcrAd42Dao.statDelete( deleteMap );
				fdcrAd42Dao.statFildDelete( deleteMap );
			}
			*/

			int statSeqn = fdcrAd42Dao.statSeqn();

			// Master INSERT, UPDATE처리
			for( int i = 0; i < TITES.length; i++ ){
				Map<String, Object> param = new HashMap<String, Object>();

				if(RGST_IDNTS != null && RGST_IDNTS.length > 0){
					param.put( "RGST_IDNT", RGST_IDNTS[i] );
				}
				if(MAILS != null && MAILS.length > 0){
					param.put( "MAIL", MAILS[i] );
				}
				if(TITES != null && TITES.length > 0){
					param.put( "TITE", TITES[i] );
				}
				if(BORD_DESCS != null && BORD_DESCS.length > 0){
					param.put( "BORD_DESC", BORD_DESCS[i] );
				}
				if(GUBUNS != null && GUBUNS.length > 0){
					param.put( "GUBUN", GUBUNS[i] );
				}
				if(APPR_DTTMS != null && APPR_DTTMS.length > 0){
					param.put( "APPR_DTTM", APPR_DTTMS[i] );
				}
				if(CLMS_TITES != null && CLMS_TITES.length > 0){
					param.put( "CLMS_TITE", CLMS_TITES[i] );
				}
				if(ICNS != null && ICNS.length > 0){
					param.put( "ICN", ICNS[i] );
				}
				if(GENRES != null && GENRES.length > 0){
					param.put( "GENRE", GENRES[i] );
				}
				if(CR_IDS != null && CR_IDS.length > 0){
					param.put( "CR_ID", CR_IDS[i] );
				}
				if(NR_IDS != null && NR_IDS.length > 0){
					param.put( "NR_ID", NR_IDS[i] );
				}
				if(ALBUM_IDS != null && ALBUM_IDS.length > 0){
					param.put( "ALBUM_ID", ALBUM_IDS[i] );
				}
				if(BOOK_NR_IDS != null && BOOK_NR_IDS.length > 0){
					param.put( "BOOK_NR_ID", BOOK_NR_IDS[i] );
				}
				if(WORK_FILE_NAMES != null && WORK_FILE_NAMES.length > 0){
					param.put( "WORK_FILE_NAME", WORK_FILE_NAMES[i] );
				}
				if(LICENSOR_NAMES != null && LICENSOR_NAMES.length > 0){
					param.put( "LICENSOR_NAME", LICENSOR_NAMES[i] );
				}
				if(FILE_NAMES != null && FILE_NAMES.length > 0){
					param.put( "FILE_NAME", FILE_NAMES[i] );
				}
				if(FILE_PATHS != null && FILE_PATHS.length > 0){
					param.put( "FILE_PATH", FILE_PATHS[i] );
				}
				if(FILE_SIZES != null && FILE_SIZES.length > 0){
					param.put( "FILE_SIZE", FILE_SIZES[i] );
				}
				
				param.put("SEQN", statSeqn);
				fdcrAd42Dao.statInsert(param);

				for( int x = 1; x < 13; x++ ){
					if( x == 12 ){
						x = 99;
					}
					String to = Integer.toString(x);
					String name = "FILD_" + to;
					//fild = (String) commandMap.get( name );
					String fild = (String) commandMap.get(name);
					System.out.println("fild:::::::::::::::"+fild);

					if( fild != null && fild.length() > 0 ){
						if( fild.equals( "1" ) ){
							param.put( "FILD", x );
							fdcrAd42Dao.statFildInsert( param );
						}
					}
				}
			}

			// detail INSERT, UPDATE처리
			if(ATTC_SEQNS != null && ATTC_SEQNS.length > 0){
				for( int i = 0; i < ATTC_SEQNS.length; i++ ){
	
					Map<String, Object> param = new HashMap<String, Object>();
	
					byte[] file = (byte[]) commandMap.get( CLIENT_FILES[i] );

					String fileName = (String) commandMap.get( REAL_FILE_NAMES[i] );

					Map upload = FileUtil.uploadMiFile( fileName, file );

					param.put( "REAL_FILE_NAME", upload.get( "REAL_FILE_NAME" ) );
					param.put( "FILE_PATH", upload.get( "FILE_PATH" ) );
					param.put( "FILE_NAME", fileName );

					fdcrAd42Dao.statFileInsert( param );
				}
			}

			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
