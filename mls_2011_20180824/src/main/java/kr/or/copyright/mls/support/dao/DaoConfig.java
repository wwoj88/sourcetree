package kr.or.copyright.mls.support.dao;

import java.io.Reader;
import java.util.Properties;

import com.ibatis.common.resources.Resources;
import com.ibatis.dao.client.DaoManager;
import com.ibatis.dao.client.DaoManagerBuilder;

public class DaoConfig {

	private static DaoManager daoManager = null;
	private static final String resource = "/WEB-INF/sqlMap/sql-map-config.xml";

	/*
	 * static { daoManager = newDaoManager(null); }
	 */

	public static DaoManager newDaoManager(Properties props) {
		try {
			Reader reader = Resources.getResourceAsReader(resource);
			return DaoManagerBuilder.buildDaoManager(reader, props);
		} catch (Exception e) {
			throw new RuntimeException(
					"Could not initialize DaoConfig.  Cause: " + e, e);
		}
	}

	public static DaoManager getDaoManager() {
		if (daoManager == null)
			synchronized (DaoManager.class) {
				if (daoManager == null)
					daoManager = newDaoManager(null);
			}
		return daoManager;
	}

}