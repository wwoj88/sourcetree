package kr.or.copyright.mls.common.model;


public class CorpUserInfo {
     
     private String membType;
     private String membDivis;
     private String membBuisnRegnum;
     
     public String getMembType() {
     
          return membType;
     }
     
     public void setMembType(String membType) {
     
          this.membType = membType;
     }
     
     public String getMembDivis() {
     
          return membDivis;
     }
     
     public void setMembDivis(String membDivis) {
     
          this.membDivis = membDivis;
     }
     
     public String getMembBuisnRegnum() {
     
          return membBuisnRegnum;
     }
     
     public void setMembBuisnRegnum(String membBuisnRegnum) {
     
          this.membBuisnRegnum = membBuisnRegnum;
     }
     
}
