package kr.or.copyright.mls.console;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

public class ConsoleLoginUser{

	private static final Logger logger = LoggerFactory.getLogger( ConsoleLoginUser.class );
	private static final String CONSOLE_SESSION_VALUE = "CONSOLE_USER";
	
	/**
	 * 로그인 전체 정보
	 * @return
	 */
	@SuppressWarnings( "unchecked" )
	public static Map<String,Object> getConsoleLoginInfo(){
		return (Map<String,Object>)RequestContextHolder.getRequestAttributes().getAttribute(CONSOLE_SESSION_VALUE, RequestAttributes.SCOPE_SESSION);
	}
	
	/**
	 * 로그인 여부
	 * @return
	 */
	public static Boolean isLogin() {
		if (RequestContextHolder.getRequestAttributes() == null) {
			System.out.println( "NULL" );
			return false;
		} else {
			if (RequestContextHolder.getRequestAttributes().getAttribute(CONSOLE_SESSION_VALUE, RequestAttributes.SCOPE_SESSION) == null) {
				System.out.println( "NULL2" );
				return false;
			} else {
				return true;
			}
		}
	}
	/**
	 * 로그인아이디
	 * @return
	 */
	public static String getMoblPhon() {
		Map<String,Object> loginInfo = getConsoleLoginInfo();
		String moblPhon = null;
		if(null!=loginInfo){
			moblPhon = EgovWebUtil.getString( loginInfo, "MOBL_PHON" );
		}
		return moblPhon;
	}
	
	/**
	 * 로그인아이디
	 * @return
	 */
	public static String getUserId() {
		Map<String,Object> loginInfo = getConsoleLoginInfo();
		String userId = null;
	
	
		if(null!=loginInfo){
			userId = EgovWebUtil.getString( loginInfo, "USER_ID" );
	
		}
		return userId;
	}

  
  /**
   * 직책
   * @return
   */
  public static String getUserDepartMent() {
    Map<String,Object> loginInfo = getConsoleLoginInfo();
    String userDepartment = null;
  
  
    if(null!=loginInfo){
         userDepartment = EgovWebUtil.getString( loginInfo, "USER_DEPARTMENT" );
  
    }
    return userDepartment;
  }
  
  

  
  /**
   * 부서
   * @return
   */
  public static String getUserPosition() {
    Map<String,Object> loginInfo = getConsoleLoginInfo();
    String userPosition = null;
  
  
    if(null!=loginInfo){
         userPosition = EgovWebUtil.getString( loginInfo, "USER_POSITION" );
  
    }
    return userPosition;
  }
  
  
	
	
	/**
	 * 로그인 이름
	 * @return
	 */
	public static String getUserName() {
		Map<String,Object> loginInfo = getConsoleLoginInfo();
		String userName = null;
		if(null!=loginInfo){
			userName = EgovWebUtil.getString( loginInfo, "USER_NAME" );
		}
		return userName;
	}
	
	/**
	 * 로그인 메일
	 * @return
	 */
	public static String getUserEmail() {
		Map<String,Object> loginInfo = getConsoleLoginInfo();
		String userEmail = null;
		if(null!=loginInfo){
			userEmail = EgovWebUtil.getString( loginInfo, "MAIL" );
		}
		return userEmail;
	}
	
	/**
	 * 로그아웃 처리
	 * @return
	 */
	public static void logout() {
		RequestContextHolder.getRequestAttributes().setAttribute( CONSOLE_SESSION_VALUE, null, RequestAttributes.SCOPE_SESSION );
	}
	
	/**
	 * 로그인아이디
	 * @return
	 */
	public static String getTrstOrgnCode() {
		Map<String,Object> loginInfo = getConsoleLoginInfo();
		String userId = null;
		if(null!=loginInfo){
			userId = EgovWebUtil.getString( loginInfo, "TRST_ORGN_CODE" );
		}
		return userId;
	}
	
	/**
	 * 로그인아이디
	 * @return
	 */
	public static String getCommname() {
		Map<String,Object> loginInfo = getConsoleLoginInfo();
		String commNm = null;
		if(null!=loginInfo){
			commNm = EgovWebUtil.getString( loginInfo, "COMM_NAME" );
		}
		return commNm;
	}
}
