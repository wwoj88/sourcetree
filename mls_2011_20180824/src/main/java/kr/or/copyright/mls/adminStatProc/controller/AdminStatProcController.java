package kr.or.copyright.mls.adminStatProc.controller;

import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.adminStatProc.service.AdminStatProcPopupService;
import kr.or.copyright.mls.adminStatProc.service.AdminStatProcService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminStatProcController {

	StopWatch sw = new StopWatch("AdminStatProcController");
	
	@Autowired
	private AdminStatProcPopupService adminStatProcPopupService;
	
	@Autowired
	private AdminStatProcService adminStatProcService;
	

	@RequestMapping("/admin/stat/proc/exeldown")
	public void statProcTargPopupExelDown(HttpServletRequest req, HttpServletResponse res) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ORD", req.getParameter("ORD"));
		map.put("YYYYMM", req.getParameter("YYYYMM"));
		map.put("WORKS_DIVS_CD", req.getParameter("WORKS_DIVS_CD"));
		map.put("STAT_CD", req.getParameter("STAT_CD"));
		map.put("FROM",1);
		map.put("TO",15000);
		int add = 15000;
		if (map.get("ORD") != null && map.get("WORKS_DIVS_CD") != null && map.get("STAT_CD") != null) {
			res.setContentType("application/csv");
			res.setHeader("Content-Disposition", "attachment;filename=" + "상당한 노력 수행 대상저작물.csv");
			ServletOutputStream out1 = res.getOutputStream();
			out1.write("대상저작물,장르,\"저작물명\",상당한노력 수행 횟수\r\n".getBytes());//이형식으로감
			out1.flush();
			byte[] data = null;
			while ((data = adminStatProcPopupService.statProcTargPopupExelDown(map)).length > 0) {
				out1.write(data);
				out1.flush();
				data = null;
				map.put("FROM", Integer.parseInt(map.get("FROM")+"")+add);
				map.put("TO", Integer.parseInt(map.get("TO")+"")+add);
			}
			out1.close();
		} else {
			throw new Exception("입력값 미입력 : /admin/stat/proc/exeldown.do");
		}
	}
	
	
	@RequestMapping("/admin/stat/proc/exeldown2")
	public void statProcPopupExelDown(HttpServletRequest req, HttpServletResponse res) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ORD", req.getParameter("ORD"));
		map.put("YYYYMM", req.getParameter("YYYYMM"));
		map.put("WORKS_DIVS_CD", req.getParameter("WORKS_DIVS_CD"));
		map.put("STAT_CD", req.getParameter("STAT_CD"));
		map.put("FROM",1);
		map.put("TO",15000);
		int add = 15000;
		if (map.get("ORD") != null && map.get("YYYYMM") != null && map.get("WORKS_DIVS_CD") != null && map.get("STAT_CD") != null) {
			res.setContentType("application/csv");
			res.setHeader("Content-Disposition", "attachment;filename=" + "상당한 노력 수행 대상저작물("+map.get("YYYYMM")+").csv");
			ServletOutputStream out1 = res.getOutputStream();
			out1.write("대상저작물,장르,\"저작물명\",분배일자\r\n".getBytes());//이형식으로감
			out1.flush();
			byte[] data = null;
			while ((data = adminStatProcPopupService.statProcPopupExelDown(map)).length > 0) {
				out1.write(data);
				out1.flush();
				data = null;
				map.put("FROM", Integer.parseInt(map.get("FROM")+"")+add);
				map.put("TO", Integer.parseInt(map.get("TO")+"")+add);
			}
			out1.close();
		} else {
			throw new Exception("입력값 미입력 : /admin/stat/proc/exeldown2.do");
		}
		
	}
	
	@RequestMapping("/admin/stat/proc/statProcPopupExelDown2")
	public void statProcPopupExelDown2(HttpServletRequest req, HttpServletResponse res) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ORD", req.getParameter("ORD"));
		map.put("YYYYMM", req.getParameter("YYYYMM"));
		map.put("WORKS_DIVS_CD", req.getParameter("WORKS_DIVS_CD"));
		map.put("WORKS_TITLE", req.getParameter("WORKS_TITLE"));
		map.put("FROM",1);
		map.put("TO",15000);
		int add = 15000;
		if (map.get("ORD") != null && map.get("YYYYMM") != null && map.get("WORKS_DIVS_CD") != null) {
			res.setContentType("application/csv");
			res.setHeader("Content-Disposition", "attachment;filename=" + "상당한 노력 진행현황("+map.get("YYYYMM")+").csv");
			ServletOutputStream out1 = res.getOutputStream();
			out1.write("진행차수,기준년월,수행일자,대상저작물,\"저작물명\",1차매칭여부,2차매칭여부,3차매칭여부,공고등록여부,공고일자,\r\n".getBytes());//이형식으로감
			out1.flush();
			byte[] data = null;
			while ((data = adminStatProcPopupService.statProcPopupExelDown2(map)).length > 0) {
				out1.write(data);
				out1.flush();
				data = null;
				map.put("FROM", Integer.parseInt(map.get("FROM")+"")+add);
				map.put("TO", Integer.parseInt(map.get("TO")+"")+add);
			}
			out1.close();
		} else {
			throw new Exception("입력값 미입력 : /admin/stat/proc/statProcPopupExelDown2.do");
		}
		
	}
	
	@RequestMapping("/admin/stat/proc/statProcWorksPopupExelDown")
	public void statProcWorksPopupExelDown(HttpServletRequest req, HttpServletResponse res) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ORD", req.getParameter("ORD"));
		map.put("YYYYMM", req.getParameter("YYYYMM"));
		map.put("FROM",1);
		map.put("TO",15000);
		int add = 15000;
		if (map.get("ORD") != null && map.get("YYYYMM") != null) {
			res.setContentType("application/csv");
			res.setHeader("Content-Disposition", "attachment;filename=" + "법정허락 대상저작물 진행현황("+map.get("YYYYMM")+").csv");
			ServletOutputStream out1 = res.getOutputStream();
			out1.write("기준년월,진행차수,수행일자,대상저작물,\"저작물명\",법정허락대상물 전환여부,\r\n".getBytes());//이형식으로감
			out1.flush();
			byte[] data = null;
			while ((data = adminStatProcPopupService.statProcWorksPopupExelDown(map)).length > 0) {
				out1.write(data);
				out1.flush();
				data = null;
				map.put("FROM", Integer.parseInt(map.get("FROM")+"")+add);
				map.put("TO", Integer.parseInt(map.get("TO")+"")+add);
			}
			out1.close();
		} else {
			throw new Exception("입력값 미입력 : /admin/stat/proc/statProcWorksPopupExelDown.do");
		}
		
	}
	
	@RequestMapping("/admin/stat/proc/statProcInfoExelDown")
	public void statProcInfoExelDown(HttpServletRequest req, HttpServletResponse res) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("works_divs_cd", req.getParameter("works_divs_cd"));
		map.put("works_title", req.getParameter("works_title"));
		map.put("FROM_NO",1);
		map.put("TO_NO",1000);
		int add = 1000;
		if (map.get("works_divs_cd") != null && map.get("works_title") != null) {
			res.setContentType("application/csv");
			res.setHeader("Content-Disposition", "attachment;filename=" + "상당한노력 매칭정보.csv");
			ServletOutputStream out1 = res.getOutputStream();
			out1.write("번호,구분,장르,\"제호\",\"저작물 정보\",순번,구분,\"저작물 정보\",\r\n".getBytes());//이형식으로감
			out1.flush();
			byte[] data = null;
			while ((data = adminStatProcPopupService.statProcInfoExelDown(map)).length > 0) {
				out1.write(data);
				out1.flush();
				data = null;
				map.put("FROM_NO", Integer.parseInt(map.get("FROM_NO")+"")+add);
				map.put("TO_NO", Integer.parseInt(map.get("TO_NO")+"")+add);
			}
			out1.close();
		} else {
			throw new Exception("입력값 미입력 : /admin/stat/proc/statProcInfoExelDown.do");
		}
		
	}
	
}
