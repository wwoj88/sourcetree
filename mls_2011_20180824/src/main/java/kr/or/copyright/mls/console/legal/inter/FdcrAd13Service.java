package kr.or.copyright.mls.console.legal.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface FdcrAd13Service{

	/**
	 * 법정허락 대상저작물 관리 목록(일괄삭제)엑셀양식 업로드
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd13Upload1( ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 법정허락 대상저작물 선택 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd13Delete1( Map<String, Object> commandMap ) throws Exception;

}
