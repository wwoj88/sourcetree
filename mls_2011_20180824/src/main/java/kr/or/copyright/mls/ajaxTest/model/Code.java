package kr.or.copyright.mls.ajaxTest.model;

public class Code {
	
	private String bigCode;
	private String midCode;
	private String codeName;
	
	public String getBigCode() {
		return bigCode;
	}
	public void setBigCode(String bigCode) {
		this.bigCode = bigCode;
	}
	public String getMidCode() {
		return midCode;
	}
	@Override
	public String toString() {
		return "CodeVO [bigCode=" + bigCode + ", midCode=" + midCode
				+ ", codeName=" + codeName + "]";
	}
	public void setMidCode(String midCode) {
		this.midCode = midCode;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	
}
