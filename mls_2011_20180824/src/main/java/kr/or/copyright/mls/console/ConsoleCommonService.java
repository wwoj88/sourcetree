package kr.or.copyright.mls.console;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.StringUtil;

import org.springframework.stereotype.Service;

import com.softforum.xdbe.xCrypto;
import com.tobesoft.platform.data.Dataset;

@Service( "consoleCommonService" )
public class ConsoleCommonService extends CommandService{

	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	/**
	 * 회원정보 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void memberInfo( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List list2 = (List) consoleCommonDao.userDetlList( commandMap );

		/* resd복호화 str 20121108 정병호 */
		xCrypto.RegisterEx(
			"pattern7",
			2,
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "xcry" ) ),
			"pool1",
			"mcst_db",
			"mcst_owner",
			"mcst_table",
			"pattern7" );

		Map list = new HashMap();
		list = (Map) list2.get( 0 );

		String RESD_CORP_NUMB = "";

		// 주민번호가 notnull 인 경우만.
		if( list.get( "RESD_CORP_NUMB" ) != null
			&& list.get( "RESD_CORP_NUMB" ).toString().replaceAll( " ", "" ).length() > 0 ){
			RESD_CORP_NUMB = xCrypto.Decrypt( "pattern7", (String) list.get( "RESD_CORP_NUMB" ) ).replaceAll( " ", "" );// (원본
																														// 주민번호
																														// 복호화)

			String RESD_CORP_NUMB1 = RESD_CORP_NUMB.substring( 0, 6 );
			String RESD_CORP_NUMB2 = ""; // RESD_CORP_NUMB.substring(6);

			list.put( "RESD_CORP_NUMB", RESD_CORP_NUMB1 + "-" + RESD_CORP_NUMB2 );
		}

		commandMap.put( "ds_list", list ); // 로그인 사용자 정보
	}

	/**
	 * 코드정보 조회
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List commonCodeList( CodeList code ) throws Exception{
		// DAO호출
		return consoleCommonDao.commonCodeList( code );
	}

	public int sendEmail( List<Map> sendList,
		Map mailMap ) throws Exception{

		String host = Constants.MAIL_SERVER_IP; // smtp서버
		// String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
		String from = StringUtil.nullToEmpty( (String) mailMap.get( "SEND_MAIL_ADDR" ) );
		if( from.equals( "" ) ){
			from = Constants.SYSTEM_MAIL_ADDRESS;
		}
		String fromName = Constants.SYSTEM_NAME;
		String subject = "[저작권찾기] " + (String) mailMap.get( "TITLE" );
		String contentHtml = (String) mailMap.get( "CONTENT_HTML" );
		String mailRejcCd = (String) mailMap.get( "MAIL_REJC_CD" );// 수신거부코드

		int iResult = 0;

		int msgIdSeq = consoleCommonDao.selectMaxMsgIdSeq(); // 메일함 seq
		Date startDate = new Date();
		Date endDate = null;

		// 발신함 메일 저장
		mailMap.put( "MSG_ID", msgIdSeq );
		mailMap.put( "START_DTTM_TRNS", startDate );
		consoleCommonDao.insertEmailMsg( mailMap );

		// 첨부파일 저장

		List<Map> fileList = new ArrayList<Map>();

		if( mailMap.get( "fileList" ) != null ){
			fileList = (List<Map>) mailMap.get( "fileList" );
		}

		for( int i = 0; i < fileList.size(); i++ ){
			Map fileMap = fileList.get( i );
			fileMap.put( "MSG_ID", msgIdSeq );
			consoleCommonDao.insertMailAttc( fileMap );
		}

		try{

			for( int i = 0; i < sendList.size(); i++ ){
				String to = StringUtil.nullToEmpty( (String) sendList.get( i ).get( "RECV_MAIL_ADDR" ) ); // 수신EMAIL
				String toName = StringUtil.nullToEmpty( (String) sendList.get( i ).get( "RECV_NAME" ) ); // 수신인

				MailInfo info = new MailInfo();
				info.setFrom( from );
				info.setFromName( fromName );
				info.setHost( host );
				info.setEmail( to );
				info.setEmailName( toName );
				info.setSubject( subject );
				info.setMessage( contentHtml );
				info.setMailRejcCd( mailRejcCd );

				// 메일발신내역 ID 셋팅 = 메세지함ID+메일주소앞부분+일련번호
				String id[] = to.split( "@" );
				String encodeId = "" + msgIdSeq + id[0] + i;

				/*
				 * if(mailMap.get("MSG_STOR_CD").equals("2")){//발신함 저장 메세지만
				 * 메일수신확인소스 삽입 StringBuffer sb = new StringBuffer(); sb.append(
				 * "<img src=\"http://new.right4me.or.kr/mail/updateEmailRecv.page?send_msg_id="
				 * +encodeId+"\" width=\"0\" height=\"0\">");
				 * info.setMailReadHtml(new String(sb)); }
				 */

				StringBuffer sb = new StringBuffer();
				sb.append( "<img src=\"" + Constants.DOMAIN_HOME + "/mail/updateEmailRecv.do?send_msg_id=" + encodeId
					+ "\" width=\"0\" height=\"0\">" );

				info.setMailReadHtml( new String( sb ) );

				sendList.get( i ).put( "MSG_ID", msgIdSeq );
				sendList.get( i ).put( "SEND_MAIL_ADDR", from );
				sendList.get( i ).put( "SEND_MSG_ID", encodeId );
				sendList.get( i ).put( "RECV_SEQN", i + 1 );// 수신자정보 수신자 순번

				info.setMessage( new String( MailMessage.getChangeInfoMailText2( info ) ) );
				MailManager manager = MailManager.getInstance();
				if( i % 100 == 0 && i != 0 ){
					System.out.println( i + "번째 메일입니다. 잠시 후 발송됩니다.\n" );
					try{
						Thread.sleep( 5 * 1000 * 60 );
						info = manager.sendMail( info );
					}
					catch( Exception e ){
						e.printStackTrace();
					}

				}else{
					info = manager.sendMail( info );
				}

				if( info.isSuccess() ){
					System.out.println( ">>>>>>>>>>>> message success :" + info.getEmail() );
					sendList.get( i ).put( "SEND_DTTM", info.getSendDttm() );
				}else{
					System.out.println( ">>>>>>>>>>>> message false   :" + info.getEmail() );
					// 운영에서는 주석처리하세요!
					// sendList.get(i).put("SEND_DTTM", info.getSendDttm());
				}
				// HTML 확인을 위한 임시테이블에 메일전송 정보 넣기(운영,개발서버에서는 주석처리)
				/*
				 * HashMap insertMap = new HashMap();
				 * insertMap.put("EMAIL_TITLE", info.getSubject());
				 * insertMap.put("EMAIL_DESC", info.getMessage());
				 * insertMap.put("EMAIL_ADDR", to);
				 * adminStatProcDao.insertMailList(insertMap);
				 */

			}
			// 발신함 발신완료시간 업데이트
			endDate = new Date();
			mailMap.put( "END_DTTM_TRNS", endDate );
			consoleCommonDao.updateEndDttmMsg( mailMap );

			for( int i = 0; i < sendList.size(); i++ ){
				consoleCommonDao.insertEmailRecvInfo( sendList.get( i ) );// 수신자 정보 저장
				consoleCommonDao.insertSendList( sendList.get( i ) );// 메일 발신내역 저장
			}

			HashMap insertMap = new HashMap();

			// 보완내역 메일발신내역 등록
			String msgStorCd = StringUtil.nullToEmpty( (String) mailMap.get( "MSG_STOR_CD" ) );
			if( "4".equals( msgStorCd ) || "5".equals( msgStorCd ) || "6".equals( msgStorCd ) ){// 보완내역
																								// 발신함
																								// 코드
				List<Map> suplList = new ArrayList();

				for( int j = 0; j < sendList.size(); j++ ){
					Map suplMap = new HashMap();
					suplMap.put( "SUPL_ID", mailMap.get( "SUPL_ID" ) );
					suplMap.put( "MSG_STOR_CD", msgStorCd );
					suplMap.put( "SEND_MSG_ID", sendList.get( j ).get( "SEND_MSG_ID" ) );
					suplList.add( suplMap );
				}

				for( int k = 0; k < suplList.size(); k++ ){
					consoleCommonDao.insertSuplList( suplList.get( k ) );
				}

			}
			String malingMgntCd = StringUtil.nullToEmpty( (String) mailMap.get( "MAILING_MGNT_CD" ) );
			// 자동메일링 관리 메일발신내역 등록
			if( "1".equals( malingMgntCd ) || "2".equals( malingMgntCd ) ){
				Map map = new HashMap();
				long time = System.currentTimeMillis();
				SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMM" );

				map.put( "MAILING_MGNT_CD", malingMgntCd );
				map.put( "YYYYMM", sdf.format( time ) );
				map.put( "MSG_ID", msgIdSeq );
				map.put( "START_DTTM", startDate );
				map.put( "END_DTTM", endDate );
				consoleCommonDao.updateSendScMailng( map );
			}

			iResult = 1;

		}
		catch( Exception e ){
			iResult = -1;
		}
		finally{
			return iResult;
		}
	}
	
	public int getNewAttcSeqn(){
		return consoleCommonDao.getNewAttcSeqn();
	}
	
	/**
	 * 기관/단체 조회
	 * @param commandMap
	 * @throws Exception
	 */
	public void trstOrgnCoNameList(Map<String,Object> commandMap) throws Exception {
		//DAO호출
		List list = (List) consoleCommonDao.trstOrgnCoNameList(commandMap);
		commandMap.put("ds_list", list); // 로그인 사용자 정보
		
	}
	
	/**
	 * 그룹별 메뉴목록
	 * 
	 * @return
	 */
	public List selectMenuGroupList(Map<String,Object> commandMap){
		return consoleCommonDao.selectMenuGroupList( commandMap );
	}
	
	
	// adminCommonList 조회 
	public void login(Map<String,Object> commandMap) throws Exception {
		System.out.println( "console login service call" );
	    /*pwsd암호화 str 20121106 정병호*/
		//TODO 암호주석제거
	    //xCrypto.RegisterEx("normal", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "normal");
		//xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		/*pwsd암호화 end 20121106 정병호*/
		
		boolean isValid = true;
		String msg = "";
		String passWord = "";
		

		/*pwsd암호화 str 20121106 정병호*/
		passWord = EgovWebUtil.getString( commandMap, "passWord" );
		//System.out.println( passWord );
		String sOutput_H = null; //사용자 입력값 암호화
		if(passWord.length() < 40){//사용자입력 패스워드
		    passWord = passWord.toUpperCase();
		    //TODO 암호주석제거
		    //sOutput_H = xCrypto.Hash(6, passWord);
		    
		    //mls_dev2 비번
		    sOutput_H = "UBPGUq3XDuuOjZXo1HQsfEJStXCAk/aaCQp7LGNIlnQ=";
		    
		    //mls_test 비번
		    //sOutput_H = "18xH0G0eQ/ZgqMCLonlLfL+HFTlMlIwgc8mfbd9tlxI=";
		}else{
		    sOutput_H = passWord;
		}
		//System.out.println( sOutput_H );
		/*pwsd암호화 end 20121106 정병호*/
		commandMap.put("passWord", sOutput_H);
		
		//DAO호출
		Map<String,Object> userInfo = (HashMap<String,Object>) consoleCommonDao.login(commandMap); 
		
		if(userInfo == null || userInfo.get( "USER_ID" ) == null){
			msg = "해당 아이디가 없습니다..";
			isValid = false;
		}
		else
		{
			userInfo.put("IP", commandMap.get("ip"));
	        
	        String pswd = userInfo.get("PSWD").toString();
	        
			if( !sOutput_H.equals(pswd) )/*pwsd암호화 20121106 정병호*/
			{
				msg = "패스워드를 확인하세요.";
				isValid = false;
			}
			else
			{
				// 로그 저장 14.11.24
				Map logMap = new HashMap();
				logMap.put("MANAGER_ID", commandMap.get("userId"));
				logMap.put("MENU_URL", "login");
				logMap.put("PROC_STATUS", "로그인");
				logMap.put("PROC_ID", null);
				logMap.put("IP_ADDRESS", commandMap.get("ip"));
				consoleCommonDao.insertAdminLogDo(logMap);
			}
		}
		commandMap.put( "msg", msg );
		commandMap.put( "isValid", isValid );
		commandMap.put( "userInfo", userInfo );
	}
}
