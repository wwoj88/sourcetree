package kr.or.copyright.mls.board.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.board.model.Board;
import kr.or.copyright.mls.board.model.BoardFile;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class BoardSqlMapDao extends SqlMapClientDaoSupport implements BoardDao {

	//private Log logger = LogFactory.getLog(getClass());
	
	public void insertQust(Board board) {
		getSqlMapClientTemplate().insert("Board.insertBoard", board);
	}
	
	public int boardMax(Board board) {
		Integer cnt = (Integer)getSqlMapClientTemplate().queryForObject("Board.boardMax",board);
		return cnt.intValue();
	}
	
	public void insertBoardFile(BoardFile boardFile) {
		getSqlMapClientTemplate().insert("Board.insertBoardFile", boardFile);
	}
	
	public void updateQust(Board board) {
		getSqlMapClientTemplate().update("Board.updateQust", board);
	}
	
	public void updateBoardFile(BoardFile boardFile) {
		getSqlMapClientTemplate().update("Board.updateBoardFile", boardFile);
	}
	
	public void fileDelete(BoardFile boardFile) {
		getSqlMapClientTemplate().delete("Board.deleteFileBoard", boardFile);
	}
	
	public Board isValidPswd(Board board) {
		return (Board)getSqlMapClientTemplate().queryForObject("Board.isValidPswd", board);
	}

	public List findBoardList(Map params) {

		return getSqlMapClientTemplate().queryForList("Board.getBoardList", params);
	}
	
	
	public void updateInqrCont(Board board) {
		getSqlMapClientTemplate().update("Board.updateInqrCont", board);
	}

	public Board boardView(Board board) {

		return (Board)getSqlMapClientTemplate().queryForObject("Board.boardView", board);
	}
	
	/* 양재석 추가 start */
	public List findNotiBoardList(Map params) {

		return getSqlMapClientTemplate().queryForList("Board.getNotiBoardList", params);
	}

	public Board notiBoardView(Board board) {

		return (Board)getSqlMapClientTemplate().queryForObject("Board.notiBoardView", board);
	}
	/* 양재석 추가 end */
	
	public List boardFileList(Board board) {

		return (List)getSqlMapClientTemplate().queryForList("Board.boardFileList", board);
	}

	public int findBoardCount(Map params) {

		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("Board.findBoardCount", params));
	}
	
	public void delete(Board board) {
		getSqlMapClientTemplate().delete("Board.deleteBoard", board);
	}
	
	public void deleteAll(Board board) {
		
		getSqlMapClientTemplate().delete("Board.deleteAll", board);
		
	}
	
	public void deleteFileAll(Board board) {
		
		getSqlMapClientTemplate().delete("Board.deleteFileAll", board);
		
	}
	
	// 커뮤니티 게시판 추가(최남식)
	public void insertComm(Board board) {
		getSqlMapClientTemplate().insert("Board.insertBoard", board);
	}
	
	public int findStatBoardCount(Map params) {

		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("Board.findStatBoardCount", params));
	}
	
	public List findStatBoardList(Map params) {

		return getSqlMapClientTemplate().queryForList("Board.findStatBoardList", params);
	}
	
	public void updateStatInqrCont(Board board) {
		getSqlMapClientTemplate().update("Board.updateStatInqrCont", board);
	}
	
	public Board statBoardView(Board board) {

		return (Board)getSqlMapClientTemplate().queryForObject("Board.statBoardView", board);
	}
	
	public List statBoardFileList(Board board) {

		return (List)getSqlMapClientTemplate().queryForList("Board.statBoardFileList", board);
	}
	
	/*마이페이지 묻고답하기 list 정병호 추가 */
	public List findMyQustList(Map params){
		return getSqlMapClientTemplate().queryForList("Board.getMyQustList", params);
	}
	
	public int findMyQustCount(Map params) {

		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("Board.findMyQustCount", params));
	}
	
	/*마이페이지 묻고답하기 View 정병호 추가 */
	public Board myQustBoardView(Board board){
		return (Board)getSqlMapClientTemplate().queryForObject("Board.myQustBoardView", board);
	}
}
