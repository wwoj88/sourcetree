package kr.or.copyright.mls.main.controller;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.common.userLogin.service.UserDormancyService;
import kr.or.copyright.mls.common.mail.SendMail;
import kr.or.copyright.mls.common.service.CommonService;
import kr.or.copyright.mls.common.service.CommonServiceImpl;
import kr.or.copyright.mls.main.model.Main;
import kr.or.copyright.mls.main.service.MainService;
import kr.or.copyright.mls.srch.model.SrchStatDTO;
import kr.or.copyright.mls.srch.service.SrchService;
import kr.or.copyright.mls.support.util.SessionUtil;
import org.json.JSONObject;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import com.ibatis.common.logging.Log;
import com.ibatis.common.logging.LogFactory;

public class MainController extends MultiActionController {

     private Log log = LogFactory.getLog(getClass());

     private MainService mainService;
     private UserDormancyService userDormancyService;
     private SrchService srchService;

     public SrchService getSrchService() {

          return srchService;
     }

     public void setSrchService(SrchService srchService) {

          this.srchService = srchService;
     }

     public void setUserDormancyService(UserDormancyService userDormancyService) {

          this.userDormancyService = userDormancyService;
     }

     public void setMainService(MainService mainService) {

          this.mainService = mainService;
     }

     public ModelAndView goPage(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          /*
           * String instType = ""; if(ServletRequestUtils.getStringParameter(request, "InstType")!=null){
           * instType = ServletRequestUtils.getStringParameter(request, "InstType"); } ModelAndView mv = new
           * ModelAndView("main/index"); mv.addObject("InstType", instType); return mv;
           */

          // 헤더 확인
          /*
           * Enumeration<String> em = request.getHeaderNames(); while(em.hasMoreElements()){ String name =
           * em.nextElement() ; String val = request.getHeader(name) ; System.out.println(name + " : " + val)
           * ; }
           */

          return list(request, respone);
     }

     public ModelAndView list(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          Main main = new Main();

          HttpSession session = request.getSession();
          String sessionid = (String) session.getAttribute("sessionid");
          // System.out.println( "sessionid : " + sessionid );
          if (sessionid == null) {
               session.setAttribute("sessionid", session.getId());
               // System.out.println( "sessionid2 : " + sessionid );
               mainService.insertUserCount();
          }

          String mainType = ServletRequestUtils.getStringParameter(request, "type");
          List<Main> anucBord01 = mainService.anucBord01();
          List<Main> anucBord06 = mainService.anucBord06();
          List<Main> anucBord03 = mainService.anucBord03();
          List<Main> anucBord04 = mainService.anucBord04();
          List<Main> anucBord05 = mainService.anucBord05();
          
          // List<Main> anucBord07 = mainService.anucBord07();
          List<Main> QnABord = mainService.QnABord();
          List<Main> statSrch = mainService.statSrch();
          List<Main> notiList = mainService.notiList();
          // 제목 글자수 제한
          for (int i = 0; i < anucBord01.size(); i++) {
               String anuc01Title = anucBord01.get(i).getTite();
               anucBord01.get(i).setTite(subTitle(anuc01Title, 50));
          }
          for (int i = 0; i < anucBord06.size(); i++) {
               String anuc06Title = anucBord06.get(i).getTite();
               anucBord06.get(i).setTite(subTitle(anuc06Title, 50));
          }

          /*
           * for( int i = 0; i < anucBord07.size(); i++ ){ String anuc07Title = anucBord07.get( i ).getTite();
           * anucBord07.get( i ).setTite( subTitle( anuc07Title, 50 ) ); }
           */
          for (int i = 0; i < anucBord03.size(); i++) {
               String anuc03Title = anucBord03.get(i).getTite();
               anucBord03.get(i).setTite(subTitle(anuc03Title, 50));
          }
          for (int i = 0; i < anucBord04.size(); i++) {
               String anuc04Title = anucBord04.get(i).getTite();
               anucBord04.get(i).setTite(subTitle(anuc04Title, 50));
          }
          for (int i = 0; i < anucBord05.size(); i++) {
               String anuc05Title = anucBord05.get(i).getTite();
               String anuc05anucItem4 = anucBord05.get(i).getAnucItem4();
               anucBord05.get(i).setTite(subTitle(anuc05Title, 50));

               anucBord05.get(i).setAnucItem4((subTitle(anuc05anucItem4, 5)));
               // System.out.println( "test2" + anucBord05.get( i ).getAnucItem4() );
          }
        
          for (int i = 0; i < QnABord.size(); i++) {
               String QnABordTitle = QnABord.get(i).getTite();
               QnABord.get(i).setTite(subTitle(QnABordTitle, 17));
          }

          for (int i = 0; i < statSrch.size(); i++) {
               String statSrchTitle = statSrch.get(i).getWorkTitle();
               statSrch.get(i).setWorkTitle(subTitle(statSrchTitle, 50));
          }


          for (int i = 0; i < notiList.size(); i++) {
               String notiListTitle = notiList.get(i).getTite();
               notiList.get(i).setTite(subTitle(notiListTitle, 18));
          }

          ModelAndView mv = null;

          if (mainType != null) {
               mv = new ModelAndView("main/main_bak", "main", mainService.mainView(main));
          } else {
               // System.out.println( "main New Call" + anucBord07.size() );
               mv = new ModelAndView("main/main");
               mv.addObject("anucBord01", anucBord01);
               mv.addObject("anucBord06", anucBord06);
               mv.addObject("anucBord03", anucBord03);
               mv.addObject("anucBord04", anucBord04);
               mv.addObject("anucBord05", anucBord05);
               // mv.addObject( "anucBord07", anucBord07 );
               mv.addObject("bord01Size", anucBord01.size());
               mv.addObject("bord06Size", anucBord06.size());
               mv.addObject("bord03Size", anucBord03.size());
               mv.addObject("bord04Size", anucBord04.size());
               mv.addObject("bord05Size", anucBord05.size());
               // mv.addObject( "bord07Size", anucBord07.size() );
               mv.addObject("QnABord", QnABord);
               mv.addObject("QnABordSize", QnABord.size());
               mv.addObject("statSrch", statSrch);
               mv.addObject("statSrchSize", statSrch.size());
               mv.addObject("notiList", notiList);
               mv.addObject("notiListSize", notiList.size());
          }

          // mv.addObject("InstType", instType);
          return mv;
     }

     // 미분배 보상금
     public ModelAndView inmtInfo01(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          String alltInmt = divisionMoney(mainService.alltInmt());
          ModelAndView mav = new ModelAndView();
          mav.setViewName("mlsInfo/inmtInfo01");
          mav.addObject("alltInmt", alltInmt);
          return mav;
     }

     // 검색 순위
     public void getSrchList(HttpServletRequest request, HttpServletResponse response
     /* ,Map<String, Object> commandMap */
     ) throws Exception {

          // 실시간 검색어
          //System.out.println("getSrchList call");
          // SrchStatDTO srchStatDTO = new SrchStatDTO();
          List searchWordList = srchService.getSearchWordList();
          //System.out.println("searchWordList : " + searchWordList.toString());
          request.setAttribute("searchWordList", searchWordList);
          // System.out.println( "srchStatDTO : " + srchStatDTO );
          //////////////////////////////////////
          response.setCharacterEncoding("utf-8");

          PrintWriter out = response.getWriter();
          JSONObject jsonObject = new JSONObject();
          jsonObject.put("searchWordList", searchWordList);
          out.print(jsonObject);
          logger.debug("fdcrAd03List5 End");

     }

     public String divisionMoney(String StringVal) {

          String paramString = StringVal;
          int paramLength = paramString.length();
          int loopSize = paramLength / 3;
          int modParam = paramString.length() % 3;
          if (modParam != 0)
               loopSize++;
          //System.out.println("loopSize : " + loopSize);
          int startIndex = 0;
          int endIndex = 3;
          String returnStr = "";
          for (int i = 0; i < loopSize; i++) {
               if (i == 0 && modParam != 0)
                    endIndex = modParam;
               String tmpString = paramString.substring(startIndex, endIndex);
               if (i == (loopSize - 1)) {
                    returnStr += "<span class='pr0'>" + tmpString + "</span>";
               } else {
                    returnStr += "<span>" + tmpString + "</span>";
               }
               if (i == 0 && modParam != 0) {
                    startIndex += modParam;
               } else {
                    startIndex += 3;
               }
               endIndex += 3;
               if (paramLength < endIndex)
                    endIndex = paramLength;
          }
          return returnStr;
     }

     public String subTitle(String str, int size) {

          if (str == null || str.equals("")) {
               return "";
          }
          size *= 3;

          int len = str.length();
          int cnt = 0, index = 0;
          char temp = '0';

          while (index < len && cnt < size) {
               temp = str.charAt(index++);
               if ((32 <= temp && temp <= 126) && (!(35 <= temp && temp <= 38) || temp != 40 || temp != 92)) {
                    cnt += 2;
               } else {
                    cnt += 3;
               }
          }

          if (index < len && size >= cnt)
               str = str.substring(0, index) + "...";
          else if (index < len && size < cnt)
               str = str.substring(0, index - 1) + "...";

          return str;
     }

     public ModelAndView goPersMgnt(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/persMgnt");
     }

     public ModelAndView goUserMgnt(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/userMgnt");
     }

     public ModelAndView goHomeCopyright(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/homeCopyright");
     }

     public ModelAndView goSiteInfo1(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/info01");
     }

     public ModelAndView goSiteInfo2(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/info02");
     }

     public ModelAndView goSiteInfo3(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/info03");
     }

     public ModelAndView goCertPage(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("certInfo/publicKey_guide01");
     }

     public ModelAndView goCertPage2(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("certInfo/publicKey_guide02");
     }

     public ModelAndView goCertPage3(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("certInfo/publicKey_guide03");
     }

     public ModelAndView goRghtInfo(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide01");
     }

     public ModelAndView goRghtInfo1(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide01_01");
     }

     public ModelAndView goRghtInfo2(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide01_02");
     }

     public ModelAndView goRghtInfo3(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide01_03");
     }

     public ModelAndView goRghtInfo4(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide01_04");
     }

     public ModelAndView goRghtInfo5(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide01_05");
     }

     public ModelAndView goRghtInfo6(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide01_06");
     }

     public ModelAndView goInmtInfo(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide02");
     }

     public ModelAndView goInmtInfo1(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide02_01");
     }

     public ModelAndView goInmtInfo2(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide02_02");
     }

     public ModelAndView goInmtInfo3(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide02_03");
     }

     public ModelAndView goInmtInfo4(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide02_04");
     }

     public ModelAndView goInmtInfo5(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide02_05");
     }

     public ModelAndView goInmtInfo6(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("siteInfo/guide02_06");
     }

     public ModelAndView goContactUs(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/contactUs");
     }

     public ModelAndView info01(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          // return new ModelAndView("mlsInfo/info01");
          return new ModelAndView("mlsInfo/guideInfo01");
     }

     public ModelAndView info02(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          // return new ModelAndView("mlsInfo/info02");
          return new ModelAndView("mlsInfo/guideInfo02");
     }

     public ModelAndView info03(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          // return new ModelAndView("mlsInfo/info03");
          return new ModelAndView("mlsInfo/guideInfo03");
     }

     public ModelAndView info04(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          // return new ModelAndView("mlsInfo/info04");
          return new ModelAndView("mlsInfo/guideInfo04");
     }

     public ModelAndView info05(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          // return new ModelAndView("mlsInfo/info05");
          return new ModelAndView("mlsInfo/guideInfo05");
     }

     public ModelAndView info4_pop1(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info4_pop1");
     }

     public ModelAndView info4_pop2(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info4_pop2");
     }

     public ModelAndView info4_pop3(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info4_pop3");
     }

     public ModelAndView info4_pop4(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info4_pop4");
     }

     public ModelAndView info4_pop5(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info4_pop5");
     }

     public ModelAndView info5_pop1(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info5_pop1");
     }

     public ModelAndView info5_pop2(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info5_pop2");
     }

     public ModelAndView info5_pop3(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info5_pop3");
     }

     public ModelAndView info5_pop4(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info5_pop4");
     }

     public ModelAndView info5_pop5(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info5_pop5");
     }

     public ModelAndView info5_pop6(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/info5_pop6");
     }

     public ModelAndView goSiteUs(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/siteUs");
     }

     public ModelAndView bannerList(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/bannerList");
     }

     public ModelAndView goSiteList(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/siteList");
     }

     public ModelAndView worksInfo01(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/worksInfo01");
     }

     public ModelAndView worksInfo02(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/worksInfo02");
     }

     public ModelAndView worksInfo03(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return new ModelAndView("mlsInfo/worksInfo03");
     }

     public ModelAndView insertUserCount(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          mainService.insertUserCount();
          return null;
     }

     @javax.annotation.Resource(name = "commonService")
     private CommonService commonService = new CommonServiceImpl();

     public ModelAndView sendMailTest(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          System.out.println("userDormancyService call");
          SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.KOREA);
          Date currentTime = new Date();
          String dTime = formatter.format(currentTime);

          List<Map> resultList = (List) userDormancyService.userDormancyCheck();
          // System.out.println( "resultList : " +resultList );
          System.out.println("resultList.size : " + resultList.size());

          System.out.println("resultList : " + resultList);
          System.out.println("resultList.size() : " + resultList.size());
          System.out.println("==================이하 메일========================");
          System.out.println("sendMailTest : ");
          HashMap userMap = new HashMap();
          // 신청제목
          userMap.put("WORKS_TITL", "SEND MAIL TEST");
          userMap.put("MGNT_DIVS", "DISCONNECTED");
          userMap.put("MAIL_TITL", "휴면 전환 예정 안내(1개월후 전환 예정)  ");
          userMap.put("MAIL_TITL_DIVS", "신청");
          userMap.put("RGST_DTTM", dTime);
          List list = new ArrayList();
          String to[] = new String[resultList.size()];
          String toName[] = new String[resultList.size()];
          for (int i = 0; i < resultList.size(); i++) {
               // System.out.println( "resultList.get( i ) : " + resultList.get( i
               // ));
               Map resultUser = resultList.get(i);
               /*
                * System.out.println( resultUser ); System.out.println(resultUser.get("MAIL" ) );
                * System.out.println( resultUser.get("USER_NAME") ); System.out.println( userMap.get( "MGNT_DIVS"
                * )); System.out.println( userMap );
                */

               userMap.put("USER_IDNT", resultUser.get("USER_IDNT"));
               to[i] = (String) resultUser.get("MAIL");
               toName[i] = (String) resultUser.get("USER_NAME");
               userMap.put("TO", to);
               userMap.put("TO_NAME", toName);
               SendMail.send(userMap);

          }

          //////////////////////////////////////////////////////////////////////////////////////

          List<Map> dormancyList = userDormancyService.userDormancyList();
          System.out.println("dormancyList : " + dormancyList);

          userDormancyService.dormancyMove(dormancyList);

          System.out.println("userDormancyService call");
          System.out.println("resultList.size : " + dormancyList.size());
          System.out.println("resultList : " + dormancyList);
          System.out.println("resultList.size() : " + dormancyList.size());
          System.out.println("==================이하 메일========================");
          System.out.println("sendMailTest : ");
          HashMap userMap2 = new HashMap();
          // 신청제목
          userMap2.put("WORKS_TITL", "SEND MAIL TEST");
          userMap2.put("MGNT_DIVS", "DISCONNECTED");
          userMap2.put("MAIL_TITL", "휴면 계정 전환 안내 ");
          userMap2.put("MAIL_TITL_DIVS", "신청");
          userMap2.put("RGST_DTTM", dTime);
          List list2 = new ArrayList();
          String to2[] = new String[dormancyList.size()];
          String toName2[] = new String[dormancyList.size()];
          for (int i = 0; i < dormancyList.size(); i++) {
               // System.out.println( "resultList.get( i ) : " + resultList.get( i
               // ));
               Map resultUser = dormancyList.get(i);
               System.out.println(resultUser);
               System.out.println(resultUser.get("MAIL"));
               System.out.println(resultUser.get("USER_NAME"));
               System.out.println(userMap2.get("MGNT_DIVS"));
               System.out.println(userMap2);
               System.out.println(to2[i]);
               System.out.println(toName2[i]);
               userMap2.put("USER_IDNT", resultUser.get("USER_IDNT"));
               to2[i] = (String) resultUser.get("MAIL");
               toName2[i] = (String) resultUser.get("USER_NAME");
               userMap2.put("TO", to2);
               userMap2.put("TO_NAME", toName2);
               SendMail.send(userMap2);

          }

          return null;
     }
}
