package kr.or.copyright.mls.adminUser.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminMenu.dao.AdminMenuDao;
import kr.or.copyright.mls.adminUser.dao.AdminUserDao;
import kr.or.copyright.mls.common.service.BaseService;

import com.softforum.xdbe.xCrypto;
import com.tobesoft.platform.data.Dataset;

public class AdminUserServiceImpl extends BaseService implements AdminUserService {

	//private Log logger = LogFactory.getLog(getClass());

	private AdminUserDao adminUserDao;
	private AdminMenuDao adminMenuDao;
	
	public void setAdminUserDao(AdminUserDao adminUserDao){
		this.adminUserDao = adminUserDao;
	}
	
	public void setAdminMenuDao(AdminMenuDao adminMenuDao){
		this.adminMenuDao = adminMenuDao;
	}
	
	// 회원정보 목록조회
	public void userList() throws Exception {

		//boolean isValid = true;
		//String msg = "";
		//String passWord = "";
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminUserDao.userList(map); 

		addList("ds_list", list); // 로그인 사용자 정보

	}
	
	
	//회원정보 상세조회
	public void userDetlList() throws Exception {

		//boolean isValid = true;
		//String msg = "";
		//String passWord = "";
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		/*RESD_CORP_NUMB복호화 str 20121108 성혜진
		
		//DAO호출
		List list2 = (List) adminUserDao.userDetlList(map); 
		
		Map list = new HashMap();		
		list = (Map)list2.get(0);	
		
		String RESD_CORP_NUMB = list.get("RESD_CORP_NUMB").toString();	
		String RESD_CORP_DECODED = null;
			
		xCrypto.RegisterEx("pattern7", 2, "C:\\TmaxSoft\\JEUS6.0\\lib\\application\\xdsp_pool.properties", "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		RESD_CORP_DECODED   = xCrypto.Decrypt("pattern7", RESD_CORP_NUMB);	
		
		list.put("RESD_CORP_NUMB", RESD_CORP_DECODED);
		
		RESD_CORP_NUMB복호화 end 20121108 성혜진*/
		
		
		//DAO호출
		List list2 = (List) adminUserDao.userDetlList(map); 
		
		/*resd복호화 str 20121108 정병호*/
		xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		
		Map list = new HashMap();		
		list = (Map)list2.get(0);	
		
		String RESD_CORP_NUMB = "";
		
		// 주민번호가 notnull 인 경우만.
		if( list.get("RESD_CORP_NUMB")!= null && list.get("RESD_CORP_NUMB").toString().replaceAll(" ", "").length()>0 ) {
			RESD_CORP_NUMB = xCrypto.Decrypt("pattern7", (String) list.get("RESD_CORP_NUMB")).replaceAll(" ", "");//(원본 주민번호 복호화)
			
			String RESD_CORP_NUMB1 = RESD_CORP_NUMB.substring(0, 6);
			String RESD_CORP_NUMB2 = ""; //RESD_CORP_NUMB.substring(6);
			
			list.put("RESD_CORP_NUMB", RESD_CORP_NUMB1+"-"+RESD_CORP_NUMB2);
		}
			
		
		
		
		addList("ds_list", list); // 로그인 사용자 정보
	}
	
	
	//신탁단체 담당자목록 조회
	public void trstOrgnMgnbList() throws Exception {

		//boolean isValid = true;
		//String msg = "";
		//String passWord = ""; 
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminUserDao.trstOrgnMgnbList(map); 

		addList("ds_list", list); // 로그인 사용자 정보

	}
	
	//신탁단체 담당자 상세 조회, 내정보조회 상세 
	public void trstOrgnMgnbDetlList() throws Exception {

		//boolean isValid = true;
		//String msg = "";
		//String passWord = "";
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		List groupList = (List)adminMenuDao.selectGroupList(); 

		addList("ds_group_info", groupList);
	
		if(map.size() > 0){
			if(map.get("GUBUN").equals("edit")){ // 수정
				//	DAO호출
				List list = (List) adminUserDao.trstOrgnMgnbDetlList(map); 
				
				if(list.size()>0) {
					//	 그룹 메뉴 조회 하기
					HashMap mapTemp = new HashMap();
					mapTemp.clear();
					mapTemp = (HashMap)list.get(0);
					
					List listMenu = (List)adminUserDao.adminGroupMenuInfo(mapTemp);
					
					addList("ds_menu_info", listMenu);
				}
				addList("ds_list", list); // 로그인 사용자 정보
			}
		}

	}
	
	// 관리자 아이디 중복체크
	public void adminIdCheck() throws Exception{
		
		Dataset ds_list = getDataset("ds_list");     
		Map map = getMap(ds_list, 0);
		
		// 관리자ID체크
		int iUserCnt = adminUserDao.adminIdCheck(map); 
		HashMap h = new HashMap();
		h.put("ID_CHECK_CNT", iUserCnt);
		
		// hmap를 dataset로 변환한다.
		addList("ds_condition", h);
		
	}
	
	//신탁단체 담당자 입력
	public void trstOrgnMgnbDetlInsert() throws Exception {

		Dataset ds_List = getDataset("ds_list");   

		Map map = getMap(ds_List, 0 );
		
		/*pwsd암호화 str 20121108 성혜진*/
		String sOutput_H        = null;
		String PSWD = ((String)map.get("PSWD")).toUpperCase();
		sOutput_H	= xCrypto.Hash(6, PSWD);//단방향 암호화(sOutput)
		map.put("PSWD", sOutput_H);
		/*pwsd암호화 end 20121108 성혜진*/
		
		adminUserDao.trstOrgnMgnbDetlInsert(map);
	}	
	
	
	
	
	//신탁단체 담당자 수정
	public void trstOrgnMgnbDetlUpdate() throws Exception {

		Dataset ds_List = getDataset("ds_list");   

		Map map = getMap(ds_List, 0 );
			
		
		// 대표담당자 정보 수정 
		String ORGN_MGNB_YSNO = (String)map.get("ORGN_MGNB_YSNO");
		
		// 해당기관의 담당자 ORGN_MGNB_YSNO= 'N'
		if(ORGN_MGNB_YSNO.equals("Y"))	
			adminUserDao.orgnMgnbUpdate(map);
		
		adminUserDao.trstOrgnMgnbDetlUpdate(map); 
		
	}
	
	//담당자,회원 비밀번호 수정
	public void userMgnbPswdUpdate() throws Exception {

		Dataset ds_Condition = getDataset("ds_condition");   
		Map map = getMap(ds_Condition, 0 );
		
		/*pwsd암호화 str 20121108 성혜진*/
		String sOutput_H        = null;
		String passWord = ((String)map.get("passWord")).toUpperCase();
		sOutput_H	= xCrypto.Hash(6, passWord);//단방향 암호화(sOutput)
		map.put("passWord", sOutput_H);
		/*pwsd암호화 end 20121108 성혜진*/
		
		adminUserDao.userMgnbPswdUpdate(map); 
		
		if(map.get("CLMS_AGR_YN").equals("Y")){
			
			//Clms비밀번호 대문자처리 (주석처리(아래두줄))
			//String passWord2 = passWord.toUpperCase();
			//map.put("cprPswd", passWord2);
			
			/***********Clms비밀번호 암호화 주석처리****************
			**********************************************/
		    map.put("cprPswd", sOutput_H);
			
			map.put("clmsUserIdnt", map.get("CLMS_USER_IDNT"));
			
			adminUserDao.updateClmsUserInfo(map);
			
		}
	}

	public void trstOrgnCoNameList() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");     
		
		Map map = getMap(ds_condition, 0);		
		
		//DAO호출
		List list = (List) adminUserDao.trstOrgnCoNameList(map);

		addList("ds_list", list); // 로그인 사용자 정보
		
	}
	
	// 대표담당자 존재여부 체크
	public void orgnMgnbCheck() throws Exception{
		
		Dataset ds_list = getDataset("ds_list");     
		Map map = getMap(ds_list, 0);
		
		// 관리자ID체크
		int iUserCnt = adminUserDao.orgnMgnbCheck(map); 
		HashMap h = new HashMap();
		h.put("ORGN_MGNB_CHECK_CNT", iUserCnt);
		
		// hmap를 dataset로 변환한다.
		addList("ds_condition", h);
	}
}
