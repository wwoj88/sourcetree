package kr.or.copyright.mls.adminStatMgnt.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.adminCommon.dao.AdminCommonDao;
import kr.or.copyright.mls.adminStatMgnt.dao.AdminStatMgntDao;
import kr.or.copyright.mls.common.dao.CommonDao;
import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.myStat.dao.MyStatDao;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;

import com.softforum.xdbe.xCrypto;
import com.tobesoft.platform.data.Dataset;

public class AdminStatMgntServiceImpl extends BaseService implements AdminStatMgntService{

	private AdminStatMgntDao adminStatMgntDao;
	private CommonDao commonDao;
	private AdminCommonDao adminCommonDao;
	
	@Resource(name="myStatDao")
	private MyStatDao myStatDao;
	
	public void setAdminStatMgntDao(AdminStatMgntDao adminStatMgntDao){
		this.adminStatMgntDao = adminStatMgntDao;
	}
	
	public void setCommonDao(CommonDao commonDao){
		this.commonDao = commonDao;
	}
	
	public void setAdminCommonDao(AdminCommonDao adminCommonDao){
		this.adminCommonDao = adminCommonDao;
	}
	
	// adminStatPrpsList 조회
	public void adminStatPrpsList() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List) adminStatMgntDao.adminStatPrpsList(map);
		List pageCount = (List) adminStatMgntDao.adminStatPrpsListCount(map);
		
		addList("ds_list", list);
		addList("ds_page", pageCount);
		
	}
	
	// adminStatPrpsDetl 조회
	public void adminStatPrpsDetl() throws Exception{
		
	    Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// 법정허락 이용승인 신청서 조회
		List statAppl = (List) adminStatMgntDao.adminStatApplicationSelect(map);
		
		/*resd복호화 str 20121108 정병호*/
		xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		 
		Map statApplObj = new HashMap();
		statApplObj = (Map)statAppl.get(0);
		
		String APPLR_RESD_CORP_NUMB = xCrypto.Decrypt("pattern7", (String) statApplObj.get("APPLR_RESD_CORP_NUMB"));//(원본 신청인주민번호 복호화)
		String APPLY_PROXY_RESD_CORP_NUMB = xCrypto.Decrypt("pattern7", (String) statApplObj.get("APPLY_PROXY_RESD_CORP_NUMB"));//(원본 대리인주민번호 복호화)
		String RGST_RESD_CORP_NUMB = xCrypto.Decrypt("pattern7", (String) statApplObj.get("RGST_RESD_CORP_NUMB"));//(원본 사용자테이블에서 등록자주민번호 복호화)
		
		String APPLR_RESD_CORP_NUMB_N = APPLR_RESD_CORP_NUMB.replaceAll("-", "");//신청인주민번호 -제거
		String APPLY_PROXY_RESD_CORP_NUMB_N = "";
		
		//대리인 경우 if널체크 해야함
		if(APPLY_PROXY_RESD_CORP_NUMB != null && !APPLY_PROXY_RESD_CORP_NUMB.equals("")){
		    APPLY_PROXY_RESD_CORP_NUMB_N = APPLY_PROXY_RESD_CORP_NUMB.replaceAll("-", "");//대리인주민번호 -제거
		}
		
		statApplObj.put("APPLR_RESD_CORP_NUMB", APPLR_RESD_CORP_NUMB);
		statApplObj.put("APPLY_PROXY_RESD_CORP_NUMB", APPLY_PROXY_RESD_CORP_NUMB);
		statApplObj.put("RGST_RESD_CORP_NUMB", RGST_RESD_CORP_NUMB);
		statApplObj.put("APPLR_RESD_CORP_NUMB_N", APPLR_RESD_CORP_NUMB_N);
		statApplObj.put("APPLY_PROXY_RESD_CORP_NUMB_N", APPLY_PROXY_RESD_CORP_NUMB_N);
		
		statAppl.add(0, statApplObj);
		
		// 법정허락신청 첨부서류
		List attchList = (List) adminStatMgntDao.adminStatAttcFileSelect(map);
		// 법정허락 이용승인신청 명세서
		List applyWorks = (List) adminStatMgntDao.adminStatApplyWorksSelect(map);
		
		addList("ds_stat_appl", statAppl);
		addList("ds_stat_file", attchList);
		addList("ds_stat_works", applyWorks);
		addList("ds_ml_file", attchList);
		
		/*resd복호화 end 20121108 정병호*/
		
		
		
		/* 복호화 주석처리
		 * Map statAppl = new HashMap();
		statAppl = (Map)statAppl2.get(0);
		
		String APPLR_RESD_CORP_NUMB = statAppl.get("APPLR_RESD_CORP_NUMB").toString();
		String APPLY_PROXY_RESD_CORP_NUMB = statAppl.get("APPLY_PROXY_RESD_CORP_NUMB").toString();
		
		String APPLR_RESD_DECODED = null;
		String APPLY_PROXY_DECODED = null;
		
		xCrypto.RegisterEx("pattern7", 2, "C:\\TmaxSoft\\JEUS6.0\\lib\\application\\xdsp_pool.properties", "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		
		APPLR_RESD_DECODED   = xCrypto.Decrypt("pattern7", APPLR_RESD_CORP_NUMB);
		APPLY_PROXY_DECODED = xCrypto.Decrypt("pattern7", APPLY_PROXY_RESD_CORP_NUMB);
		
		System.out.println("########APPLR_RESD_DECODED##########"+APPLR_RESD_DECODED);
		System.out.println("#############APPLY_PROXY_DECODED########"+APPLY_PROXY_DECODED);
		
		statAppl.put("APPLR_RESD_CORP_NUMB", APPLR_RESD_DECODED);
		statAppl.put("APPLY_PROXY_RESD_CORP_NUMB", APPLY_PROXY_DECODED);*/
	}
	
	//법정허락 이용신청 삭제
	public void adminStatPrpsDel() throws Exception{
		
		Dataset ds_stat_appl = getDataset("ds_stat_appl"); 
		Map map = getMap(ds_stat_appl,0);
		List statList = new ArrayList();
		
		adminStatMgntDao.updateStatApplicationStat(map);
		adminStatMgntDao.insertStatApplicationShis(map);
		
		statList.add(map);
		
		addList("ds_stat_appl", statList);
		
		
	}
	
	// adminStatPrpsHist 조회
	public void adminStatPrpsHist() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// 법정허락신청 진행상태 변경내역조회
		List list = (List) adminStatMgntDao.adminStatApplicationShisSelect(map);
		
		addList("ds_list", list);
		
	}
	
	// adminStatPrpsWorksHist 조회
	public void adminStatPrpsWorksHist() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// 법정허락신청 심의결과상태 변경내역조회
		List list = (List) adminStatMgntDao.adminStatApplyWorksShisSelect(map);
		
		addList("ds_list", list);
		
	}
	
	// 법정허락 이용 승인 신청 진행상태 변경
	public void adminStatPrpsChange() throws Exception{
		
		Dataset ds_stat_appl = getDataset("ds_stat_appl");
		
		String  row_status = null;
		
		for( int i=0; i<ds_stat_appl.getRowCount();i++ ){
			
			row_status = ds_stat_appl.getRowStatus(i);
			Map applMap = getMap(ds_stat_appl, i);
			
			SimpleDateFormat _SimpleDateFormat = new SimpleDateFormat("yyyy", new Locale("ko","KOREA"));
			String yyyymmdd = _SimpleDateFormat.format(new Date());	
			
			String receiptYmd = yyyymmdd;
			String receiptSeq = adminStatMgntDao.adminStatApplicationReceiptSeqSelect(); // 접수번호 시퀀스 조회
			String receiptNo = receiptYmd+"-"+receiptSeq;
			
			if(applMap.get("STAT_CD").equals("8")){  // 접수완료
				applMap.put("RECEIPT_NO", receiptNo);
			}
			
			if( row_status.equals("update") == true )
			{
				int attachSeqn = commonDao.getNewAttcSeqn();
				String fileName = ds_stat_appl.getColumnAsString(i, "FILE_NAME");
			
				if(fileName != null && !fileName.equals(""))
				{
					applMap.put("ATTC_SEQN", attachSeqn);
					
					String filePath = "";
					String realFileNm = "";
					String fileSize = "";
					byte[] file = null;
					Map upload = null;
					
					if(ds_stat_appl.getColumn(i, "ATTACH_FILE_CONTENT0") == null || ds_stat_appl.getColumn(i, "ATTACH_FILE_CONTENT0").toString().equals("")){
						
						filePath = ds_stat_appl.getColumnAsString(i, "FILE_PATH");
						realFileNm = ds_stat_appl.getColumnAsString(i, "REAL_FILE_NAME");
						fileSize = ds_stat_appl.getColumnAsString(i, "FILE_SIZE");
					}else{
						file = ds_stat_appl.getColumn(i, "ATTACH_FILE_CONTENT0").getBinary();
						upload = FileUtil.uploadMiFile(fileName, file);
						filePath = upload.get("FILE_PATH").toString();
						realFileNm = upload.get("REAL_FILE_NAME").toString();
						fileSize = ds_stat_appl.getColumn(i, "ATTACH_FILE_SIZE0").toString();
					}
					
					Map fileMap = new HashMap();
					fileMap.put("ATTC_SEQN", attachSeqn);
					fileMap.put("FILE_ATTC_CD", "ST");
					fileMap.put("FILE_NAME", fileName);
					fileMap.put("FILE_PATH", filePath);
					fileMap.put("FILE_SIZE", fileSize);
					fileMap.put("REAL_FILE_NAME", realFileNm);
					fileMap.put("RGST_IDNT", applMap.get("USER_ID"));
					
					// 첨부파일 등록
					adminStatMgntDao.adminFileInsert(fileMap);
				}
				
				// 이용승인신청 명세서 수정
				adminStatMgntDao.adminStatApplicationUpdate(applMap);
				// 이용승인신청 명세서 상태변경내역 등록
				adminStatMgntDao.adminStatApplicationShisInsert(applMap);
				
				// 보완요청, 반려, 결제요청 시 메일,sms 보내기
				if(applMap.get("STAT_CD").equals("3") || applMap.get("STAT_CD").equals("6") || applMap.get("STAT_CD").equals("7"))
				{
					// 메일 보내기
					Map memMap = new HashMap();
					memMap.put("USER_IDNT", applMap.get("RGST_IDNT"));
					List memList = adminCommonDao.selectMemberInfo(memMap);
					memMap = (Map)memList.get(0);
					
					String statNm = "";
					if(applMap.get("STAT_CD").equals("3")){
						statNm = "보완요청";
					}else if(applMap.get("STAT_CD").equals("6")){
						statNm = "반려";
					}else if(applMap.get("STAT_CD").equals("7")){
						statNm = "결제요청";
					}
					
					String host = Constants.MAIL_SERVER_IP; // smtp서버
					String to = memMap.get("MAIL").toString(); // 수신EMAIL
					String toName = memMap.get("USER_NAME").toString(); // 수신인
					String subject = "[저작권찾기] 법정허락 이용신청 "+statNm;
					
					String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
					String fromName = Constants.SYSTEM_NAME;
					
					if (memMap.get("MAIL_RECE_YSNO").equals("Y") && to != null && to != "") {
						
						// ------------------------- mail 정보
						// ----------------------------------//
						MailInfo info = new MailInfo();
						info.setFrom(from);
						info.setFromName(fromName);
						info.setHost(host);
						info.setEmail(to);
						info.setEmailName(toName);
						info.setSubject(subject);

						StringBuffer sb = new StringBuffer();
						
						sb.append("				<tr>");
						sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
						sb.append("					<p style=\"font-weight:bold; background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 법정허락 이용신청 "+statNm+" 정보</p>");
						sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
						sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자 이름 : "+toName+" </li>");	
						sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청번호 : "+ applMap.get("APPLY_NO") + " </li>");
						sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : " +applMap.get("APPLY_WORKS_TITL")+" </li>");				
						sb.append("					</ul>");
						sb.append("					</td>");
						sb.append("				</tr>");
						
					    info.setMessage(sb.toString());
						info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
						
/*
						
						
						sb.append("<div class=\"mail_title\">" + toName + "님, 법정허락 이용신청이 "+statNm+" 되었습니다. </div>");
						sb.append("<div class=\"mail_contents\"> ");
						sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
						sb.append("		<tr> ");
						sb.append("			<td> ");
						sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
						// 변경부분
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">신청번호</td> ");
						sb.append("						<td class=\"tdData\">" + applMap.get("APPLY_NO") + "</td> ");
						sb.append("					</tr> ");
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">제호(제목)</td> ");
						sb.append("						<td class=\"tdData\">" + applMap.get("APPLY_WORKS_TITL") + "</td> ");
						sb.append("					</tr> ");
						sb.append("				</table> ");
						sb.append("			</td> ");
						sb.append("		</tr> ");
						sb.append("	</table> ");
						sb.append("</div> ");
						
						info.setMessage(new String(MailMessage.getChangeInfoMailText(sb
								.toString())));*/
						MailManager manager = MailManager.getInstance();

						info = manager.sendMail(info);
						if (info.isSuccess()) {
							System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());
						} else {
							System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
						}
						
					}
					
					// sms 보내기
					String smsTo = memMap.get("MOBL_PHON").toString();
					String smsFrom = Constants.SYSTEM_TELEPHONE;
					
					String smsMessage = "[저작권찾기] 법정허락 이용신청이 "+statNm+" 되었습니다.";
					
					if (memMap.get("SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {

						SMSManager smsManager = new SMSManager();

						System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
					}
					
				} // end email, sms
				
			}
			
		}
		
	}
	
	// adminStatPrpsWorksChange 조회
	public void adminStatPrpsWorksChange() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map conditionMap = getMap(ds_condition, 0);
		Dataset ds_stat_works2 = getDataset("ds_stat_works2");
		
		String  row_status = null;
		
		for( int i=0; i<ds_stat_works2.getRowCount();i++ ){
			
			row_status = ds_stat_works2.getRowStatus(i);
			Map map = getMap(ds_stat_works2, i );
			
			if( row_status.equals("insert") == true )
			{
				int attachSeqn = commonDao.getNewAttcSeqn();
				String fileName = ds_stat_works2.getColumnAsString(i, "FILE_NAME");
				
				if(fileName != null && !fileName.equals(""))
				{
					map.put("ATTC_SEQN", attachSeqn);
					
					String filePath = "";
					String realFileNm = "";
					String fileSize = "";
					byte[] file = null;
					Map upload = null;
					
					if(ds_stat_works2.getColumn(i, "ATTACH_FILE_CONTENT0") == null || ds_stat_works2.getColumn(i, "ATTACH_FILE_CONTENT0").toString().equals(""))
					{
						filePath = ds_stat_works2.getColumnAsString(i, "FILE_PATH");
						realFileNm = ds_stat_works2.getColumnAsString(i, "REAL_FILE_NAME");
						fileSize = ds_stat_works2.getColumnAsString(i, "FILE_SIZE");
					}else{
						file = ds_stat_works2.getColumn(i, "ATTACH_FILE_CONTENT0").getBinary();
						upload = FileUtil.uploadMiFile(fileName, file);
						filePath = upload.get("FILE_PATH").toString();
						realFileNm = upload.get("REAL_FILE_NAME").toString();
						fileSize = ds_stat_works2.getColumn(i, "ATTACH_FILE_SIZE0").toString();
					}
					
					Map fileMap = new HashMap();
					fileMap.put("ATTC_SEQN", attachSeqn);
					fileMap.put("FILE_ATTC_CD", "ST");
					fileMap.put("FILE_NAME", fileName);
					fileMap.put("FILE_PATH", filePath);
					fileMap.put("FILE_SIZE", fileSize);
					fileMap.put("REAL_FILE_NAME", realFileNm);
					fileMap.put("RGST_IDNT", map.get("USER_ID"));
					fileMap.put("MODI_IDNT", map.get("USER_ID"));
					fileMap.put("APPLY_WRITE_YMD", conditionMap.get("APPLY_WRITE_YMD"));
					fileMap.put("APPLY_WRITE_SEQ", conditionMap.get("APPLY_WRITE_SEQ"));
					
					// 첨부파일 등록
					adminStatMgntDao.adminFileInsert(fileMap);
					adminStatMgntDao.adminStatAttcFileInsert(fileMap);
				}
				
				// 이용승인신청 명세서 수정
				adminStatMgntDao.adminStatApplyWorksUpdate(map);
				// 이용승인신청 명세서 상태변경내역 등록
				adminStatMgntDao.adminStatApplyWorksShisInsert(map);
				
				// 승인, 거절 시 메일,sms 보내기
				if(map.get("STAT_RSLT_CD").equals("2") || map.get("STAT_RSLT_CD").equals("3"))
				{
					// 메일 보내기
					Map memMap = new HashMap();
					memMap.put("USER_IDNT", conditionMap.get("RGST_IDNT"));
					List memList = adminCommonDao.selectMemberInfo(memMap);
					memMap = (Map)memList.get(0);
					
					String statNm = "";
					if(map.get("STAT_RSLT_CD").equals("2")){
						statNm = "승인";
					}else if(map.get("STAT_RSLT_CD").equals("3")){
						statNm = "거절";
					}
					
					String host = Constants.MAIL_SERVER_IP; // smtp서버
					String to = memMap.get("MAIL").toString(); // 수신EMAIL
					String toName = memMap.get("USER_NAME").toString(); // 수신인
					String subject = "[저작권찾기] 법정허락 이용신청 명세서 "+statNm;
					
					String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
					String fromName = Constants.SYSTEM_NAME;
					
					if (memMap.get("MAIL_RECE_YSNO").equals("Y") && to != null && to != "") {
						
						// ------------------------- mail 정보
						// ----------------------------------//
						MailInfo info = new MailInfo();
						info.setFrom(from);
						info.setFromName(fromName);
						info.setHost(host);
						info.setEmail(to);
						info.setEmailName(toName);
						info.setSubject(subject);

						StringBuffer sb = new StringBuffer();
						
						
						sb.append("				<tr>");
						sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
						sb.append("					<p style=\"font-weight:bold; background:url("+kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 법정허락 이용신청 명세서 "+statNm+" 정보</p>");
						sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
						sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자 이름 : "+toName+" </li>");	
						sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청번호 : "+  map.get("APPLY_WRITE_YMD") + "-"+map.get("APPLY_WRITE_SEQ") + " </li>");
						sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : " +map.get("WORKS_TITL")+" </li>");				
						sb.append("					</ul>");
						sb.append("					</td>");
						sb.append("				</tr>");
						
					    info.setMessage(sb.toString());
						info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
						
					/*	
						
						sb.append("<div class=\"mail_title\">" + toName + "님, 법정허락 이용신청 명세서가 "+statNm+" 되었습니다. </div>");
						sb.append("<div class=\"mail_contents\"> ");
						sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
						sb.append("		<tr> ");
						sb.append("			<td> ");
						sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
						// 변경부분
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">신청번호</td> ");
						sb.append("						<td class=\"tdData\">" + map.get("APPLY_WRITE_YMD") + "-"+map.get("APPLY_WRITE_SEQ")+"</td> ");
						sb.append("					</tr> ");
						sb.append("					<tr> ");
						sb.append("						<td class=\"tdLabel\">제호(제목)</td> ");
						sb.append("						<td class=\"tdData\">" + map.get("WORKS_TITL") + "</td> ");
						sb.append("					</tr> ");
						sb.append("				</table> ");
						sb.append("			</td> ");
						sb.append("		</tr> ");
						sb.append("	</table> ");
						sb.append("</div> ");
						
						info.setMessage(new String(MailMessage.getChangeInfoMailText(sb
								.toString())));*/
						MailManager manager = MailManager.getInstance();

						info = manager.sendMail(info);
						if (info.isSuccess()) {
							System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());
						} else {
							System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
						}
						
					}
					
					// sms 보내기
					String smsTo = memMap.get("MOBL_PHON").toString();
					String smsFrom = Constants.SYSTEM_TELEPHONE;
					
					String smsMessage = "[저작권찾기] 법정허락 이용신청 명세서"+map.get("WORKS_TITL")+"가 "+statNm+" 되었습니다.";
					
					if(memMap.get("SMS_RECE_YSNO") != null){
						if (memMap.get("SMS_RECE_YSNO").equals("Y") && smsTo != null && smsTo != "") {

							SMSManager smsManager = new SMSManager();

							System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
						}
					}
				} // end email, sms
			}
		}
	}
	
	
	// adminStatPrpsRegi 법정허락 관리자 등록 20120829 정병호
	public void adminStatPrpsRegi() throws Exception{
	    Dataset ds_stat_appl = getDataset("ds_stat_appl");
	    Dataset ds_ml_file = getDataset("ds_ml_file");
	    Dataset ds_stat_works = getDataset("ds_stat_works");
	    
	    Map applMap = getMap(ds_stat_appl, 0);
	    
	    DecimalFormat df = new DecimalFormat("00");
	    Calendar currentCal = Calendar.getInstance();
	    currentCal.add(currentCal.DATE, 0);
	    
	    String  applyWriteYmd = Integer.toString(currentCal.get(Calendar.YEAR))
	    + df.format(currentCal.get(Calendar.MONTH) + 1)
	    + df.format(currentCal.get(Calendar.DAY_OF_MONTH));
	    
	    int applyWriteSeq = myStatDao.newApplyWriteSeq();
	    
	    applMap.put("APPLY_WRITE_YMD", applyWriteYmd);
	    applMap.put("APPLY_WRITE_SEQ", applyWriteSeq);
	    
	    /*resd암호화 str 20121106 정병호*/
	    
	    xCrypto.RegisterEx("normal", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "normal");
	    xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
	    
	    String sOutput7 = null;
	    String sString = (String)applMap.get("APPLR_RESD_CORP_NUMB");
	    sOutput7    = xCrypto.Encrypt("pattern7",sString );

	    String sOutput7_1 = null;
	    String sString_1 = (String)applMap.get("APPLY_PROXY_RESD_CORP_NUMB");
	    sOutput7_1    = xCrypto.Encrypt("pattern7",sString_1);
	    
	    
	    applMap.put("APPLR_RESD_CORP_NUMB", sOutput7);
	    
	    applMap.put("APPLY_PROXY_RESD_CORP_NUMB", sOutput7_1);
	    
	    /*resd암호화 str 20121106 정병호*/
	    
	    
	    adminStatMgntDao.adminStatPrpsRegi(applMap);
	    adminStatMgntDao.adminStatPrpsShisRegi(applMap);
	    for(int i=0; i<ds_ml_file.getRowCount(); i++){
			Map dsFileMap = getMap(ds_ml_file, i);
			
			int attachSeqn = commonDao.getNewAttcSeqn();
			String fileName = ds_ml_file.getColumnAsString(i, "FILE_NAME");
			
			if(fileName != null && !fileName.equals("")){
                String filePath = "";
                String realFileNm = "";
                String fileSize = "";
                byte[] file = null;
                Map upload = null;
                    
                if(ds_ml_file.getColumn(i, "ATTACH_FILE_CONTENT"+i) == null || ds_ml_file.getColumn(i, "ATTACH_FILE_CONTENT"+i).toString().equals("")){
			
					filePath = ds_ml_file.getColumnAsString(i, "FILE_PATH");
					realFileNm = ds_ml_file.getColumnAsString(i, "REAL_FILE_NAME");
					fileSize = ds_ml_file.getColumnAsString(i, "FILE_SIZE");
                } else {
    		    	file = ds_ml_file.getColumn(i, "ATTACH_FILE_CONTENT"+i).getBinary();
    		    	upload = FileUtil.uploadMiFile(fileName, file);
    		    	filePath = upload.get("FILE_PATH").toString();
    		    	realFileNm = upload.get("REAL_FILE_NAME").toString();
    		    	fileSize = ds_ml_file.getColumn(i, "ATTACH_FILE_SIZE"+i).toString();
                }
                	Map fileMap = new HashMap();
                	fileMap.put("ATTC_SEQN", attachSeqn);
				fileMap.put("FILE_ATTC_CD", dsFileMap.get("FILE_ATTC_CD"));
				fileMap.put("FILE_NAME_CD", dsFileMap.get("FILE_NAME_CD"));
				fileMap.put("FILE_NAME", fileName);
				fileMap.put("FILE_PATH", filePath);
				fileMap.put("FILE_SIZE", fileSize);
				fileMap.put("REAL_FILE_NAME", realFileNm);
				fileMap.put("RGST_IDNT", dsFileMap.get("RGST_IDNT"));
			
				Map statAttcFileMap = new HashMap();
			
				statAttcFileMap.put("APPLY_WRITE_YMD", applyWriteYmd);
				statAttcFileMap.put("APPLY_WRITE_SEQ", applyWriteSeq);
				statAttcFileMap.put("ATTC_SEQN", attachSeqn);
				statAttcFileMap.put("MODI_IDNT", dsFileMap.get("RGST_IDNT"));
				adminStatMgntDao.adminFileInsert(fileMap);
				adminStatMgntDao.adminStatAttcFileInsert(statAttcFileMap);
			}
	    }
	    
	    for(int i=0; i<ds_stat_works.getRowCount(); i++){
			Map dsWorksMap = getMap(ds_stat_works,i);
			dsWorksMap.put("APPLY_WRITE_YMD", applyWriteYmd);
			dsWorksMap.put("APPLY_WRITE_SEQ", applyWriteSeq);
			adminStatMgntDao.adminStatWorksInsert(dsWorksMap);
	    }
	    
	    
	}
	
	//오프라인 이용신청 수정 2014-11-06 이병원
	public void adminStatPrpsUpte() throws Exception {
		
		Dataset ds_stat_appl = getDataset("ds_stat_appl");
		Dataset ds_ml_file = getDataset("ds_ml_file");
		Dataset ds_stat_works = getDataset("ds_stat_works");
		Dataset ds_works_tmp = getDataset("ds_works_tmp");
		Dataset ds_file_tmp = getDataset("ds_file_tmp");
		
		Map applMap = getMap(ds_stat_appl,0);
		 
		//--주민번호 암호화 시작
		xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
		 
	    String sOutput7 = null;
	    String sString = (String)applMap.get("APPLR_RESD_CORP_NUMB");
	    sOutput7    = xCrypto.Encrypt("pattern7",sString );
		 
	    String sOutput7_1 = null;
	    String sString_1 = (String)applMap.get("APPLY_PROXY_RESD_CORP_NUMB");
	    sOutput7_1    = xCrypto.Encrypt("pattern7",sString_1);
	    
	    applMap.put("APPLR_RESD_CORP_NUMB", sOutput7);
	    applMap.put("APPLY_PROXY_RESD_CORP_NUMB", sOutput7_1);
	    //--주민번호 암호화 끝
	    
	    Map statAttcFileMap = null;
	    Map dsFileMap = null;
	    Map fileMap = null;
	    
	    //명세서 삭제
	    for(int i=0; i<ds_works_tmp.getRowCount(); i++){
			Map dsWorksMap = getMap(ds_works_tmp,i);
			
			adminStatMgntDao.adminStatWorksDel(dsWorksMap);
	    }
	    
	    //첨부파일 삭제
	    for(int i=0; i<ds_file_tmp.getRowCount(); i++){	
			
			String attc_seqn = ds_file_tmp.getColumnAsString(i, "ATTC_SEQN");
			
			if(attc_seqn != null && !attc_seqn.equals("")){
                
				dsFileMap = getMap(ds_file_tmp, i);
				statAttcFileMap = new HashMap();
				
				statAttcFileMap.put("ATTC_SEQN", dsFileMap.get("ATTC_SEQN"));
				statAttcFileMap.put("APPLY_WRITE_YMD", dsFileMap.get("APPLY_WRITE_YMD"));
				statAttcFileMap.put("APPLY_WRITE_SEQ", dsFileMap.get("APPLY_WRITE_SEQ"));
				
				//첨부 서류삭제
				adminStatMgntDao.adminStatAttcFileDel(statAttcFileMap);
				
				//첨부 파일삭제
				adminStatMgntDao.adminFileDel(statAttcFileMap);

			}
	    }
	    //이용승인신청서 수정
	    adminStatMgntDao.adminStatPrpsUpte(applMap);
	    // 이용승인신청서 상태변경
		adminStatMgntDao.adminStatPrpsShisRegi2(applMap);
	    
	    //첨부파일 재등록
	    for(int j=0; j<ds_ml_file.getRowCount(); j++){
			
			dsFileMap = getMap(ds_ml_file, j);
			statAttcFileMap = new HashMap();
			fileMap = new HashMap();
			
			int attachSeqn = commonDao.getNewAttcSeqn();
			String fileName = ds_ml_file.getColumnAsString(j, "FILE_NAME");
			if(fileName != null && !fileName.equals("")){
			
				String filePath = "";
	            String realFileNm = "";
	            String fileSize = "";
	            byte[] file = null;
	            Map upload = null;
	            
	            String write_ymd = (String)applMap.get("APPLY_WRITE_YMD");
				String write_seq = (String)applMap.get("APPLY_WRITE_SEQ");
            
	            if(ds_ml_file.getColumn(j, "ATTACH_FILE_CONTENT"+j) == null || ds_ml_file.getColumn(j, "ATTACH_FILE_CONTENT"+j).toString().equals("")){
	            
	            	filePath = ds_ml_file.getColumnAsString(j, "FILE_PATH");
					realFileNm = ds_ml_file.getColumnAsString(j, "REAL_FILE_NAME");
					fileSize = ds_ml_file.getColumnAsString(j, "FILE_SIZE");
	            }else{
			
	            	file = ds_ml_file.getColumn(j, "ATTACH_FILE_CONTENT"+j).getBinary();
			    	upload = FileUtil.uploadMiFile(fileName, file);
			    	filePath = upload.get("FILE_PATH").toString();
			    	realFileNm = upload.get("REAL_FILE_NAME").toString();
			    	fileSize = ds_ml_file.getColumn(j, "ATTACH_FILE_SIZE"+j).toString();
	            }
         	
	        	fileMap.put("ATTC_SEQN", attachSeqn);          	
				fileMap.put("FILE_ATTC_CD", dsFileMap.get("FILE_ATTC_CD"));
				fileMap.put("FILE_NAME_CD", dsFileMap.get("FILE_NAME_CD"));
				fileMap.put("FILE_NAME", fileName);
				fileMap.put("FILE_PATH", filePath);
				fileMap.put("FILE_SIZE", fileSize);
				fileMap.put("REAL_FILE_NAME", realFileNm);
				fileMap.put("RGST_IDNT", dsFileMap.get("RGST_IDNT"));		
	
				//첨부파일 등록
				adminStatMgntDao.adminFileInsert(fileMap);
				
				statAttcFileMap.put("ATTC_SEQN", attachSeqn);
				statAttcFileMap.put("APPLY_WRITE_YMD", write_ymd);
				statAttcFileMap.put("APPLY_WRITE_SEQ", write_seq);
				statAttcFileMap.put("MODI_IDNT", applMap.get("RGST_IDNT"));
					
				//첨부서류 등록
				adminStatMgntDao.adminStatAttcFileInsert(statAttcFileMap);
			
			}
	    }
		
	    
	   
	    
	    //명세서 재등록
	    for(int j=0; j<ds_stat_works.getRowCount(); j++){
	    	Map dsWorksMap = getMap(ds_stat_works,j);
	    	
	    	int works_seqn = adminStatMgntDao.adminStatWorksSeqn(applMap);
	    	
	    	dsWorksMap.put("APPLY_WRITE_YMD", applMap.get("APPLY_WRITE_YMD"));
	    	dsWorksMap.put("APPLY_WRITE_SEQ", applMap.get("APPLY_WRITE_SEQ"));
	    	dsWorksMap.put("WORKS_SEQN", works_seqn);
	    	
	    	adminStatMgntDao.adminStatWorksInsert(dsWorksMap);
	    }
	    


	}
	
	//	오프라인 이용신청 일괄등록 20141020 이병원
	public void adminStatPrpsRegiAll() throws Exception {
		
		Dataset ds_stat_appl = getDataset("ds_stat_appl");
		Dataset ds_recno = getDataset("ds_recno");
		Dataset ds_stat_works = getDataset("ds_stat_works");
		
		ArrayList li = new ArrayList();
		Map map1 = getMap(ds_stat_appl,0);
		String rgst_idnt = (String)map1.get("RGST_IDNT");
		int applyWriteSeq = 0;
		String tmpYmd = "";
		
		for(int i=0; i<ds_stat_appl.getRowCount(); i++){
			
			Map appMap = getMap(ds_stat_appl,i);
		
			if(ds_stat_appl.getColumnAsString(i, "APPLY_WRITE_YMD")!=null){
				
				tmpYmd = (String)appMap.get("APPLY_WRITE_YMD");
				applyWriteSeq = myStatDao.newApplyWriteSeq2(tmpYmd);
				
				appMap.put("APPLY_WRITE_YMD", tmpYmd);
				appMap.put("APPLY_WRITE_SEQ", applyWriteSeq);
				appMap.put("RGST_IDNT", rgst_idnt);
				
				xCrypto.RegisterEx("normal", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "normal");
				xCrypto.RegisterEx("pattern7", 2,  new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
				 
				String sOutput7 = null;
			    String sString = (String)appMap.get("APPLR_RESD_CORP_NUMB");
			    sOutput7    = xCrypto.Encrypt("pattern7",sString );

			    String sOutput7_1 = null;
			    String sString_1 = (String)appMap.get("APPLY_PROXY_RESD_CORP_NUMB");
			    sOutput7_1    = xCrypto.Encrypt("pattern7",sString_1);
			    
			    appMap.put("APPLR_RESD_CORP_NUMB", sOutput7);
			    appMap.put("APPLY_PROXY_RESD_CORP_NUMB", sOutput7_1);
				
			    adminStatMgntDao.adminStatPrpsRegiAll(appMap);
			    adminStatMgntDao.adminStatPrpsShisRegiAll(appMap);
			    adminStatMgntDao.adminStatWorksInsertAll(appMap);			
	
			}else{
				
				appMap.put("APPLY_WRITE_YMD", tmpYmd);
				appMap.put("APPLY_WRITE_SEQ", applyWriteSeq);
				appMap.put("RGST_IDNT", rgst_idnt);
				
			    adminStatMgntDao.adminStatWorksInsertAll(appMap);
				
			}
		}
		
	}
	//데이터 중복 확인
	public void adminStatPrpsSearch() throws Exception{
		
		Dataset ds_apply = getDataset("ds_apply");
		ArrayList li = new ArrayList();
		for(int i=0; i<ds_apply.getRowCount(); i++){
			Map map = getMap(ds_apply,i);
			Map map2 = new HashMap();
			
			int apply_ymd= adminStatMgntDao.adminStatPrpsRegiSelect(map);
			
			if(apply_ymd !=0){
				map2.put("APPLY_WRITE_YMD", map.get("APPLY_WRITE_YMD"));
				map2.put("APPLY_NO", map.get("APPLY_NO"));
				
				li.add(map2);
			}
		}
		addList("ds_apply", li);
	}
	

	public void cPayList() throws Exception {
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List) adminStatMgntDao.cPayList(map);
		
		addList("ds_list", list);
		
	}
}
