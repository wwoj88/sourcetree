package kr.or.copyright.mls.ajaxTest.service;

import java.util.List;

public interface UserListService {
	
	public List<String> getNameListForSuggest(String namePrefix);

}
