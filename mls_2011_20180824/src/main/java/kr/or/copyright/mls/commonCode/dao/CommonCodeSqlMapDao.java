package kr.or.copyright.mls.commonCode.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class CommonCodeSqlMapDao extends SqlMapClientDaoSupport implements CommonCodeDao {

	public List commonCodeList(Map map) {
		return getSqlMapClientTemplate().queryForList("CommonCode.commonCodeList",map);
	}
	
}
