package kr.or.copyright.mls.console.rcept.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd23Service{

	/**
	 * 미분배보상금 도서관 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd23List1( Map<String, Object> commandMap ) throws Exception;

}
