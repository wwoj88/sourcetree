package kr.or.copyright.mls.console.effort;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.effort.inter.FdcrAd0301Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAd0301Dao" )
public class FdcrAd0301DaoImpl extends EgovComAbstractDAO implements FdcrAd0301Dao{

	// 저작물 보고 관리자 리스트 조회
	public List selectMailMgntList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminEvnSet.selectMailMgntList", map );
	}

	// 저작물 보고 관리자 이메일 삭제
	public void deleteMailMgntList( Map map ){
		getSqlMapClientTemplate().delete( "AdminEvnSet.deleteMailMgntList", map );
	}

	// 저작물 보고 관리자 이메일 등록
	public void insertMailMgntList( Map map ){
		getSqlMapClientTemplate().insert( "AdminEvnSet.insertMailMgntList", map );

	}

	// 관리자 이메일 등록 중복 확인
	public int isCheckMailMgntList( Map map ){
		int iUserCnt = (Integer) getSqlMapClientTemplate().queryForObject( "AdminEvnSet.isCheckMailMgntList", map );
		return iUserCnt;
	}

	// 신청기능구분 코드값
	public List SelectMailMgntCodeList( Map map ){
		// TODO Auto-generated method stub
		return getSqlMapClientTemplate().queryForList( "AdminEvnSet.SelectMailMgntCodeList", map );
	}

	// 상당한노력
	// -----------------------------------------------------------------------------

	// 상당한노력 자동화정보 조회
	public List statProcSetList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminEvnSet.statProcSetList", map );
	}

	// 상당한노력 자동화 셋팅
	public void statProcSetUpdateYn( Map map ){
		getSqlMapClientTemplate().update( "AdminEvnSet.statProcSetUpdateYn", map );
	}

	// --------------------------------------------------------------------------------------

	// 저작물보고 관리자 메일발송 위탁관리저작물 정보
	public List selectMailCommWorksInfo(){
		return getSqlMapClientTemplate().queryForList( "AdminEvnSet.selectMailCommWorksInfo" );
	}

	public int isCheckMailCommWorksInfo(){
		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminEvnSet.isCheckMailCommWorksInfo" );
	}

	// 저작물보고 관리자 메일발송 미분배보상금 정보
	public List selectMailInmtWorksInfo(){
		return getSqlMapClientTemplate().queryForList( "AdminEvnSet.selectMailInmtWorksInfo" );
	}

	public int isCheckMailInmtWorksInfo(){
		return (Integer) getSqlMapClientTemplate().queryForObject( "AdminEvnSet.isCheckMailInmtWorksInfo" );
	}

}
