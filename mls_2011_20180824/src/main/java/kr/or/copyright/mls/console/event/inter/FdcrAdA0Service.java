package kr.or.copyright.mls.console.event.inter;

import java.util.ArrayList;
import java.util.Map;

public interface FdcrAdA0Service{

	/**
	 * 이벤트 등록
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA0Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

}
