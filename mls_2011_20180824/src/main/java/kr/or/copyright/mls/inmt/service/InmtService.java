package kr.or.copyright.mls.inmt.service;

public interface InmtService {

	public void inmtList() throws Exception;
	
	public void inmtSave() throws Exception;

	public void inmtSelect() throws Exception;
	
}
