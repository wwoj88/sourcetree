package kr.or.copyright.mls.adminMenu.service;


public interface AdminMenuService {
	
	// 메뉴 목록조회
	public void selectMenuList() throws Exception;
	
	// 메뉴 저장처리 
	public void menuUpdate() throws Exception;
	
	// 그룹 목록 조회
	public void selectGroupList() throws Exception;
	
	// 그룹 메뉴 조회하기
	public void groupMenuInfo() throws Exception;
	
	// 그룹  등록하기
	public void groupInsert() throws Exception;
	
	// 그룹, 그룹메뉴 삭제하기
	public void groupDelete() throws Exception;
	
	// 그룹, 그룹메뉴 수정하기
	public void groupMenuUpdate() throws Exception;
	
}
