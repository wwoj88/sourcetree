package kr.or.copyright.mls.rsltInqr.controller;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.common.service.CodeListService;
import kr.or.copyright.mls.inmtPrps.model.InmtPrps;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;
import kr.or.copyright.mls.rsltInqr.service.RsltInqrService;
import kr.or.copyright.mls.support.util.SessionUtil;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class RsltInqrController extends MultiActionController{
//	private Log logger = LogFactory.getLog(getClass());

	private CodeListService codeListService;
	private RsltInqrService rsltInqrService;
	
	public void setCodeListService(CodeListService codeListService){
		this.codeListService = codeListService;
	}
	
	public void setRsltInqrService(RsltInqrService rsltInqrService) {
		this.rsltInqrService = rsltInqrService;
	}

	public ModelAndView list(HttpServletRequest request, HttpServletResponse respone) throws Exception {

		User user = SessionUtil.getSession(request);
		String sessUserIdnt = user.getUserIdnt();
		
		if(sessUserIdnt == null){
			return new ModelAndView("redirect:/user/user.do?method=goLogin");
		}
				
		Date date=new Date();
		SimpleDateFormat yearForm = new SimpleDateFormat("yyyy");
		SimpleDateFormat dayForm = new SimpleDateFormat("yyyyMMdd");
		String thisMonth = yearForm.format(date)+"0101";
		String thisDay = dayForm.format(date);
		
		String DIVS = ServletRequestUtils.getStringParameter(request, "DIVS","10");

//        System.out.println("#######################");
//        System.out.println("list");
//        System.out.println("#######################");
		RghtPrps rghtPrps = new RghtPrps();

		rghtPrps.setUSER_IDNT(sessUserIdnt);  // 로그인한 회원 아이디 
		rghtPrps.setDIVS(DIVS);
		rghtPrps.setSrchPrpsDivs(ServletRequestUtils.getStringParameter(request, "srchPrpsDivs"));
		rghtPrps.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate",thisMonth));
		rghtPrps.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate",thisDay));
		rghtPrps.setSrchTitle(ServletRequestUtils.getStringParameter(request, "srchTitle"));
		
		rghtPrps.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));
		
		int from = Constants.DEFAULT_ROW_PER_PAGE * (rghtPrps.getNowPage()-1) + 1;
        int to = Constants.DEFAULT_ROW_PER_PAGE * rghtPrps.getNowPage();
        
        rghtPrps.setStartRow(from);
        rghtPrps.setEndRow(to);
        
        int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
        
        ModelAndView mv = null;
        
        if(DIVS.equals("10")){
        	rghtPrps.setSrchPrpsRghtCode(ServletRequestUtils.getStringParameter(request, "srchPrpsRghtCode"));
        	
        	mv = new ModelAndView("rsltInqr/rghtRsltInqrList", "rsltList", rsltInqrService.rsltInqrList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
        	
        	mv.addObject("srchPrpsRghtCode", ServletRequestUtils.getStringParameter(request, "srchPrpsRghtCode"));  // 신청목적
        }else if(DIVS.equals("20")){
        	mv = new ModelAndView("rsltInqr/inmtRsltInqrList", "rsltList", rsltInqrService.rsltInqrList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
        }
		
		
		mv.addObject("DIVS", ServletRequestUtils.getStringParameter(request, "DIVS"));  // 10:권리찾기 , 20:보상금
		mv.addObject("srchTitle", ServletRequestUtils.getStringParameter(request, "srchTitle"));  // 검색어
		mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate",thisMonth));  // 신청일자
		mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate",thisDay));  // 신청일자
		mv.addObject("srchPrpsDivs", ServletRequestUtils.getStringParameter(request, "srchPrpsDivs"));  // 구분
		mv.addObject("nowPage", pageNo);
		
		mv.addObject("thisMonth", thisMonth);
		mv.addObject("thisDay", thisDay);
		
		return mv;
	}
	
	// 권리찾기신청 조회(음악/어문/방송대본/이미지/영화/방송/기타)
	public ModelAndView rghtPrpsDetl(HttpServletRequest request, HttpServletResponse respone) throws Exception{
		
		//조회조건
		RghtPrps srchRghtPrps = new RghtPrps();

		srchRghtPrps.setSrchPrpsDivs(ServletRequestUtils.getStringParameter(request, "srchPrpsDivs", ""));
		srchRghtPrps.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate", ""));
		srchRghtPrps.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate", ""));
		srchRghtPrps.setSrchTitle(ServletRequestUtils.getStringParameter(request, "srchTitle", ""));
		
		RghtPrps rghtPrps = new RghtPrps();
		
		String mode			= ServletRequestUtils.getStringParameter(request, "mode","");			// R : 레포팅화면
		String report			= ServletRequestUtils.getStringParameter(request, "report","");			// 레포팅 파일
		String modal 			= ServletRequestUtils.getStringParameter(request, "modal","0");			// 레포팅 파일 모달구분
		
		String DIVS = ServletRequestUtils.getStringParameter(request, "DIVS","M");
		String PRPS_MAST_KEY = ServletRequestUtils.getStringParameter(request, "PRPS_MAST_KEY");
		
		rghtPrps.setDIVS(DIVS);
		rghtPrps.setPRPS_MAST_KEY(PRPS_MAST_KEY);
		
		ModelAndView mv = null;
		
		// 레포팅
		if(mode.equals("R")) 	mv = new ModelAndView(report);
		
		// M: 음악, O: 어문, C:방송대본, I:이미지, V: 영화, R:방송 , X: 기타, N: 뉴스
		if(DIVS.equals("M")){
			if(!mode.equals("R")) mv = new ModelAndView("rsltInqr/rghtPrpsDetl_musc");	// 레포팅이 아닌경우에는 setting
			mv.addObject( "rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("O")){
        	if(!mode.equals("R")) mv = new ModelAndView("rsltInqr/rghtPrpsDetl_book");
        	mv.addObject("rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("N")){
        	if(!mode.equals("R")) mv = new ModelAndView("rsltInqr/rghtPrpsDetl_news");
        	mv.addObject("rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("C")){
        	if(!mode.equals("R")) mv = new ModelAndView("rsltInqr/rghtPrpsDetl_script");
        	mv.addObject("rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("I")){
        	if(!mode.equals("R")) mv = new ModelAndView("rsltInqr/rghtPrpsDetl_image");
        	mv.addObject("rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("V")){
        	if(!mode.equals("R")) mv = new ModelAndView("rsltInqr/rghtPrpsDetl_mvie");
        	mv.addObject("rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("R")){
        	if(!mode.equals("R")) mv = new ModelAndView("rsltInqr/rghtPrpsDetl_broadcast");
        	mv.addObject("rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("X")){
        	if(!mode.equals("R")) mv = new ModelAndView("rsltInqr/rghtPrpsDetl_etc");
        	mv.addObject("rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }
		
		rghtPrps.setDIVS(DIVS);
		
		mv.addObject("DIVS", DIVS);
		mv.addObject("fileList", rsltInqrService.rghtFileList(rghtPrps));				// 파일목록
		mv.addObject("workList", rsltInqrService.rghtPrpsWorkList(rghtPrps));	// 신청저작물
		mv.addObject("rsltList", rsltInqrService.rsltTrstWorksList(rghtPrps));		// 신청단체별 저작물별 처리목록
		mv.addObject("srchParam",srchRghtPrps);
		
		// 레포팅
		if(mode.equals("R")) {
			mv.addObject("workListCount", rsltInqrService.rghtPrpsWorkList(rghtPrps).size());
			mv.addObject("modal", modal);
		}
		
		return mv;
	}
	
	// 권리찾기신청 수정화면이동(음악/도서/방송대본/이미지/영화/방송/기타)
	public ModelAndView rghtPrpsModi(HttpServletRequest request, HttpServletResponse respone) throws Exception{
		
		RghtPrps rghtPrps = new RghtPrps();
		
		String DIVS = ServletRequestUtils.getStringParameter(request, "DIVS","M");
		String PRPS_MAST_KEY = ServletRequestUtils.getStringParameter(request, "PRPS_MAST_KEY");
		
		rghtPrps.setDIVS(DIVS);
		rghtPrps.setPRPS_MAST_KEY(PRPS_MAST_KEY);
		
		ModelAndView mv = null;
		
		// M: 음악, O: 어문, C:방송대본, I:이미지, V:영화, R:방송 , X : 기타
		if(DIVS.equals("M")){
				mv = new ModelAndView("rsltInqr/rghtPrpsModi_musc", "rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("O")){
        	mv = new ModelAndView("rsltInqr/rghtPrpsModi_book", "rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("N")){
        	mv = new ModelAndView("rsltInqr/rghtPrpsModi_news", "rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("C")){
        	mv = new ModelAndView("rsltInqr/rghtPrpsModi_script", "rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("I")){
        	mv = new ModelAndView("rsltInqr/rghtPrpsModi_image", "rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("V")){
        	mv = new ModelAndView("rsltInqr/rghtPrpsModi_mvie", "rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("R")){
        	mv = new ModelAndView("rsltInqr/rghtPrpsModi_broadcast", "rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }else if(DIVS.equals("X")){
        	mv = new ModelAndView("rsltInqr/rghtPrpsModi_etc", "rghtPrps", rsltInqrService.rghtPrpsDetl(rghtPrps)); 
        }
		
		CodeList codeList = new CodeList();
		List genreList = new ArrayList();
		List broadList = null;
		if(DIVS.equals("C")){
			codeList.setBigCode("13"); // 방송대본 장르
			genreList = codeListService.commonCodeList(codeList);
			mv.addObject("genreList", genreList);
			
			codeList.setBigCode("16");  // 방송사
			broadList = codeListService.commonCodeList(codeList);
			mv.addObject("broadList", broadList);
		}
		
		List mediList = null;
		List chnlList = null;
		if(DIVS.equals("R")){
			codeList.setBigCode("15"); // 방송 매체
			mediList = codeListService.commonCodeList(codeList);
			mv.addObject("mediList", mediList);
			
			codeList.setBigCode("17");  // 채널
			chnlList = codeListService.commonCodeList(codeList);
			mv.addObject("chnlList", chnlList);
		}
		
		rghtPrps.setDIVS(DIVS);
		
		List IROWIDX = rsltInqrService.rghtPrpsWorkList(rghtPrps);
		
		mv.addObject("DIVS", DIVS);
		mv.addObject("fileList", rsltInqrService.rghtFileList(rghtPrps));				// 파일목록
		mv.addObject("workList", rsltInqrService.rghtPrpsWorkList(rghtPrps));	// 신청저작물
		mv.addObject("IROWIDX", IROWIDX.size());	// 신청저작물
		//mv.addObject("rsltList", rsltInqrService.rsltTrstWorksList(rghtPrps));		// 신청단체별 저작물별 처리목록
		
		return mv;
	}
	
	// 권리찾기신청 처리결과 조회
	public ModelAndView rghtPrpsRsltDetl(HttpServletRequest request, HttpServletResponse respone) throws Exception{
		
		RghtPrps rghtPrps = new RghtPrps();
		
		bind(request, rghtPrps);
		
		ModelAndView mv = null;
		
        mv = new ModelAndView("rsltInqr/PrpsRsltDetlDesc", "rghtPrps", rsltInqrService.rsltTrstWorksDesc(rghtPrps)); 
		
		mv.addObject("DIVS", rghtPrps.getDIVS());
		
		return mv;
	}
	
	// 보상금신청내역 조회()
	// 20100824 김소라 report 추가.
	public ModelAndView inmtPrpsDetl(HttpServletRequest request, HttpServletResponse respone) throws Exception{

//		System.out.println("##############################");
//		System.out.println("inmtPrpsDetl");
//		System.out.println("##############################");
//		System.out.println("DIVS			="+ServletRequestUtils.getStringParameter(request, "DIVS"));
//		System.out.println("srchPrpsDivs	="+ServletRequestUtils.getStringParameter(request, "srchPrpsDivs"));
//		System.out.println("srchStartDate	="+ServletRequestUtils.getStringParameter(request, "srchStartDate"));
//		System.out.println("srchEndDate		="+ServletRequestUtils.getStringParameter(request, "srchEndDate"));
//		System.out.println("srchTitle		="+ServletRequestUtils.getStringParameter(request, "srchTitle"));
//		System.out.println("##############################");

		RghtPrps dto = new RghtPrps();
		
		String DIVS 		= ServletRequestUtils.getStringParameter(request, "DIVS","B");
		String PRPS_MAST_KEY= ServletRequestUtils.getStringParameter(request, "PRPS_MAST_KEY");
		String PRPS_SEQN 	= ServletRequestUtils.getStringParameter(request, "PRPS_SEQN");
		String mode			= ServletRequestUtils.getStringParameter(request, "mode","");			// U : 수정화면, R : 레포팅화면
		String report			= ServletRequestUtils.getStringParameter(request, "report","");			// 레포팅 파일

		dto.setSrchPrpsDivs(ServletRequestUtils.getStringParameter(request, "srchPrpsDivs"));
		dto.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate"));
		dto.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate"));
		dto.setSrchTitle(ServletRequestUtils.getStringParameter(request, "srchTitle"));
		dto.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));

		dto.setDIVS(DIVS);
		dto.setPRPS_MAST_KEY(PRPS_MAST_KEY);
		dto.setPRPS_SEQN(PRPS_SEQN);
		
		ModelAndView mv = null;
		
		// B:방송음악, S:교과용, L:도서관  A:수업목적
		if(DIVS.equals("B")){
        	mv = new ModelAndView("rsltInqr/inmtPrpsDetl_musc");
        	if(mode.equals("U"))	mv = new ModelAndView("rsltInqr/inmtPrpsModi_musc");
        }else if(DIVS.equals("S")){
        	mv = new ModelAndView("rsltInqr/inmtPrpsDetl_subj");
        	if(mode.equals("U"))	mv = new ModelAndView("rsltInqr/inmtPrpsModi_subj"); 
        }else if(DIVS.equals("L")){
        	mv = new ModelAndView("rsltInqr/inmtPrpsDetl_libr"); 
        	if(mode.equals("U"))	mv = new ModelAndView("rsltInqr/inmtPrpsModi_libr");
        }else if(DIVS.equals("A")){
        	mv = new ModelAndView("rsltInqr/inmtPrpsDetl_supp"); 
        	if(mode.equals("U"))	mv = new ModelAndView("rsltInqr/inmtPrpsModi_supp");
        }
		
		// 레포팅
		if(mode.equals("R"))	mv = new ModelAndView(report);
		
		InmtPrps clientInfo = rsltInqrService.inmtPrpsDetl(dto);	//신청자정보
		List rsltList = rsltInqrService.inmtPrpsRsltList(dto);		//보상금신청 처리결과
		List tempList = rsltInqrService.inmtTempRsltList(dto);
		List fileList = rsltInqrService.inmtFileList(dto);
		
		List kappList	= null;	//음제협
		List fokapoList	= null;	//음실연
		List krtraList	= null;	//복전협(교과)
		List krtraList_2= null;	//복전협(도서)
		List trstList		 = new ArrayList();	
		
		String totDealStat = rsltInqrService.inmtTotalDealStat(dto);	//전체처리상태 값

		String attFileYn = "";
		String offxListRecp = "";
		int iTrst = 0; int i202=0;  int i203=0;  int i205=0;  int i205_2=0; 
		
		//분류체크
		String trst_203 = "0";
		String trst_202 = "0";
		String trst_205 = "0";
		String trst_205_2 = "0";
		
		String pemrRlnm = "";	//본명
		String pemrStnm = "";	//예명
		String grupName = "";	//그룹명
		String resdNumb = "";	//주민번호
		String resdNumbStr = "";//주민번호
		
		for(int i = 0; i < rsltList.size(); i++){
			RghtPrps adto = (RghtPrps)rsltList.get(i);
			
			if( adto.getTRST_ORGN_CODE().equals("203") ) {
				trst_203 = "1";
				kappList = rsltInqrService.inmtKappRsltList(dto);
				
				if(kappList.size()>0) {
					attFileYn = (String) ((InmtPrps)kappList.get(0)).getATTC_YSNO();
					offxListRecp = (String) ((InmtPrps)kappList.get(0)).getOFFX_LINE_RECP();
				}
			}else if( adto.getTRST_ORGN_CODE().equals("202") ) {
				trst_202 = "1";
				fokapoList = rsltInqrService.inmtFokapoRsltList(dto);
				
				if(fokapoList.size()>0) {
					pemrRlnm = (String) ((InmtPrps)fokapoList.get(0)).getPEMR_RLNM();	//본명
					pemrStnm = (String) ((InmtPrps)fokapoList.get(0)).getPEMR_STNM();	//예명
					grupName = (String) ((InmtPrps)fokapoList.get(0)).getGRUP_NAME();	//그룹명
					resdNumb = (String) ((InmtPrps)fokapoList.get(0)).getRESD_NUMB();	//주민번호
					resdNumbStr = (String) ((InmtPrps)fokapoList.get(0)).getRESD_NUMB_STR();	//주민번호
					
					attFileYn = (String) ((InmtPrps)fokapoList.get(0)).getATTC_YSNO();
					offxListRecp = (String) ((InmtPrps)fokapoList.get(0)).getOFFX_LINE_RECP();
					
				//	mv.addObject("resdNumb_1",	resdNumb.substring(0,6));
				//	mv.addObject("resdNumb_2",	resdNumb.substring(6));
				}
			}else if( adto.getTRST_ORGN_CODE().equals("205") && adto.getPRPS_DIVS().equals("S") ) {
				trst_205 = "1";
				dto.setTRST_ORGN_CODE(adto.getTRST_ORGN_CODE());
				krtraList = rsltInqrService.inmtKrtraRsltList(dto);
				
				if(krtraList.size()>0) {
					pemrRlnm = (String) ((InmtPrps)krtraList.get(0)).getRTPS_RLNM();	//본명
					pemrStnm = (String) ((InmtPrps)krtraList.get(0)).getRTPS_STNM();	//필명
					grupName = (String) ((InmtPrps)krtraList.get(0)).getRTPS_CRNM();	//예명
					resdNumb = (String) ((InmtPrps)krtraList.get(0)).getRESD_NUMB();	//주민번호
					resdNumbStr = (String) ((InmtPrps)krtraList.get(0)).getRESD_NUMB_STR();	//주민번호
					attFileYn = (String) ((InmtPrps)krtraList.get(0)).getATTC_YSNO();
					offxListRecp = (String) ((InmtPrps)krtraList.get(0)).getOFFX_LINE_RECP();
					
					mv.addObject("resdNumb_1",	resdNumb.substring(0,6));
					mv.addObject("resdNumb_2",	resdNumb.substring(6));
				}
			}else if( adto.getTRST_ORGN_CODE().equals("205") && adto.getPRPS_DIVS().equals("L") ) {
				trst_205_2 = "1";
				dto.setTRST_ORGN_CODE(adto.getTRST_ORGN_CODE());
				krtraList_2 = rsltInqrService.inmtKrtraRsltList(dto);
				
				if(krtraList_2.size()>0) {
					pemrRlnm = (String) ((InmtPrps)krtraList_2.get(0)).getRTPS_RLNM();	//본명
					pemrStnm = (String) ((InmtPrps)krtraList_2.get(0)).getRTPS_CRNM();	//필명
					grupName = (String) ((InmtPrps)krtraList_2.get(0)).getRTPS_STNM();	//예명
					resdNumb = (String) ((InmtPrps)krtraList_2.get(0)).getRESD_NUMB();	//주민번호
					resdNumbStr = (String) ((InmtPrps)krtraList_2.get(0)).getRESD_NUMB_STR();	//주민번호
					attFileYn = (String) ((InmtPrps)krtraList_2.get(0)).getATTC_YSNO();
					offxListRecp = (String) ((InmtPrps)krtraList_2.get(0)).getOFFX_LINE_RECP();
					
					mv.addObject("resdNumb_1",	resdNumb.substring(0,6));
					mv.addObject("resdNumb_2",	resdNumb.substring(6));
				}
			}
			
			if( adto.getTRST_ORGN_CODE().equals("203") && (++i203)==1 
					|| adto.getTRST_ORGN_CODE().equals("202") && (++i202)==1 
					|| adto.getTRST_ORGN_CODE().equals("205") && adto.getPRPS_DIVS().equals("S")  && (++i205)==1 
					|| adto.getTRST_ORGN_CODE().equals("205") && adto.getPRPS_DIVS().equals("L")  && (++i205_2)==1 ) {
				
				HashMap trMap = new HashMap();
				
				if( adto.getTRST_ORGN_CODE().equals("205") && adto.getPRPS_DIVS().equals("L"))
					trMap.put("TRST_ORGN_CODE", "205_2");	
				else
					trMap.put("TRST_ORGN_CODE", adto.getTRST_ORGN_CODE());			// 분류코드값 : 신탁기관
				
				trMap.put("DEAL_STAT", adto.getDEAL_STAT());								// 상태 값
				trMap.put("RSLT_DESC", adto.getRSLT_DESC());								// 처리결과
				trMap.put("TRST_ATTCHFILE_YN", attFileYn);										// 파일첨부 YN 값
				trMap.put("OFFX_LINE_RECP", offxListRecp);										// 오프라인접수 YN 값
	
				trstList.add(iTrst, trMap);		
				
				iTrst++;
			}
		}
		
		clientInfo.setTRST_203(trst_203);
		clientInfo.setTRST_202(trst_202);
		clientInfo.setTRST_205(trst_205);
		clientInfo.setTRST_205_2(trst_205_2);

		mv.addObject("srchParam", dto);
		
		mv.addObject("clientInfo",	clientInfo);
		mv.addObject("tempList",	tempList);
		mv.addObject("tempListCnt",	tempList.size());

		mv.addObject("fileList",	fileList);
		
		mv.addObject("trstList",	trstList);
		mv.addObject("totDealStat",	totDealStat);
		
		mv.addObject("kappList",	kappList);
		mv.addObject("fokapoList",	fokapoList);
		mv.addObject("krtraList",	krtraList);
		mv.addObject("krtraList_2",	krtraList_2);
		
		mv.addObject("pemrRlnm",	pemrRlnm);
		mv.addObject("pemrStnm",	pemrStnm);
		mv.addObject("grupName",	grupName);
		//
		mv.addObject("resdNumbStr",	resdNumbStr);

		
		
		return mv;
	}

	public ModelAndView isValidDealStat(HttpServletRequest request, HttpServletResponse respone) throws Exception{
		
		String[] actFlagYnArray = ServletRequestUtils.getStringParameters(request,"actFlagYn");
		String[] prpsMastKeyArray = ServletRequestUtils.getStringParameters(request,"prpsMastKey");
		
		String maxDealStat = null;
		PrintWriter out = respone.getWriter();
		
		for(int i = 0; i < actFlagYnArray.length; i++){
			if(actFlagYnArray[i].equals("Y")){
				if(!prpsMastKeyArray[i].equals("")){
						
					RghtPrps rghtPrps = new RghtPrps();
					
					rghtPrps.setPRPS_MAST_KEY(prpsMastKeyArray[i]);
					
					maxDealStat = rsltInqrService.isValidDealStat(rghtPrps);
					
					if(!maxDealStat.equals("1")){
						out.print("false");
					}else{
						out.print("true");
					}
				}
			}
		}
		out.close();
		return null;
	}
	
	// 신청현황 삭제 
	public ModelAndView deleteRsltInqr(HttpServletRequest request, HttpServletResponse respone) throws Exception{
		
		String[] actFlagYnArray = ServletRequestUtils.getStringParameters(request,"actFlagYn");
		String[] prpsMastKeyArray = ServletRequestUtils.getStringParameters(request,"prpsMastKey");
		
		for(int i = 0; i < actFlagYnArray.length; i++){
			if(actFlagYnArray[i].equals("Y")){
				if(!prpsMastKeyArray[i].equals("")){
						
					RghtPrps rghtPrps = new RghtPrps();
					
					rghtPrps.setPRPS_MAST_KEY(prpsMastKeyArray[i]);
	
					rsltInqrService.deleteRsltInqr(rghtPrps);
				}
			}
		}
		
		return this.list(request, respone);
		
	}
	
	// 단건삭제
	public ModelAndView deleteRslt(HttpServletRequest request, HttpServletResponse respone) throws Exception{
		
		String prpsMastKey = ServletRequestUtils.getStringParameter(request,"PRPS_MAST_KEY");
		
		RghtPrps rghtPrps = new RghtPrps();
		
		rghtPrps.setPRPS_MAST_KEY(prpsMastKey);

		rsltInqrService.deleteRsltInqr(rghtPrps);
				
		
		return this.list(request, respone);
		
	}
	
}
