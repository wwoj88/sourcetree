package kr.or.copyright.mls.support.util;

import java.io.UnsupportedEncodingException;

public class CommonUtil2 {
    
	private CommonUtil2() 
	{

	}
	
	  /**
	   * 문자형을 돈을 표현하는 형식으로 바꾼다.
	   * @param sCount
	   * @return
	  */
	  public static String subDot(String sCount){
	  
	  String sNum = sCount;
	  int  iLen = sNum.length();
	  String sTemp = "";
	  int  co  = 3;
	        
	  if(iLen < 4) return sNum;
	  	while (iLen>0)
	  	{
	  		iLen = iLen - co;
	        if(iLen<0)
	        {
	        	co = iLen + co;
	        	iLen = 0;
	        }
	        if(iLen != 0){
	        	sTemp=","+sNum.substring(iLen,co+iLen)+sTemp;
	        }
	        else
	        {
	        	sTemp=","+sNum.substring(iLen,co)+sTemp;
	        }
	     }
	  return sTemp.substring(1);
	 } 
	  
/**
   * 원하는 자릿수만큼 문자열 앞에 0을 채워주는 함수
   *
   * @param       changeIntValue 바꾸고자 하는 숫자 
   * @param       반환되어질 전체 자릿수
   * @return      0 이 채워진 숫자
  */
	public static String addZeroFirst(String changeIntValue, int strNum) 
	{
		if (strNum < 0) 
			return ("Parameter two is wrong");
		
		int intValueLength = changeIntValue.trim().length();
		
		if (intValueLength >= strNum) 
		{
			return (changeIntValue);
		} 
		else 
		{
			String returnValue = changeIntValue;
			
			for (int i = 0 ; i < (strNum - intValueLength) ; i++) 
			{
				returnValue = "0" + returnValue;
			}
			return (returnValue);
		}
	}
	
	/**
   * 문자열의 바이트 길이를 주어진 최소, 최대 길이와 비교한다.
   *
   * @param   str 검사 하고자 하는 문자열
   * @param   min 검사 하고자 하는 최소 길이
   * @param   max 검사 하고자 하는 최대 길이
   * @return  문자열의 바이트 길이가 유효하면 'true' or 'false'
   */
  public static boolean compareByteLength(String str, int min, int max) {
      return compareByteLength(str, min, max, "8859_1");
  }

  /**
   * 문자열의 바이트 길이를 주어진 최소, 최대 길이와 비교한다.
   *
   * @param   str 검사 하고자 하는 문자열
   * @param   min 검사 하고자 하는 최소 길이
   * @param   max 검사 하고자 하는 최대 길이
   * @param   enc 문자 인코딩
   * @return  문자열의 바이트 길이가 유효하면 'true' or 'false'
   */
  public static boolean compareByteLength(
          String str,
          int min,
          int max,
          String enc) {
      str = str.trim();
      try {
          int l = str.getBytes(enc).length;
          if (l >= min && l <= max)
              return true;
      }
      catch (UnsupportedEncodingException _ex) {
      }
      return false;
  }
  
  public static boolean compareByteLength2(String str, int min, int max)
  {
  	int len = str.length();
  	
  	int cnt = 0; int index = 0 ;
  	
  	while(index < len && cnt <max)
  	{
  		if(str.charAt(index++)<256)
  			cnt++;
  		else
  			cnt += 2;
  	}
  	
  	min = index;
  	
  	if(len>=min && max <= cnt)
  		return true;
  	else
  		return false;
  }

  /**
   * 문자열의 길이를 주어진 길이와 비교한다.
   *
   * @param   str 검사 하고자 하는 문자열
   * @param   len 검사 하고자 하는 길이
   * @return  문자열의 길이가 주어진 길이와 같으면 'true' or 'false'
   */
  
  public static boolean compareLength(String str, int len) {
      str = str.trim();
      int l = str.length();
      if (l == len)
          return true;
      return false;
  }

  
  /**
   * 문자 하나가 숫자인지 검사
   *
   * @param   str 검사 하고자 하는 문자
   * @return  숫자인지의 여부에 따라 'true' or 'false'
   */
  public static boolean isNumber(char c) {
      if (c < '0' || c > '9')
          return false;
      return true;
  }


  /**
   * 문자열이 숫자인지 검사
   *
   * @param   str 검사 하고자 하는 문자열
   * @return  숫자인지의 여부에 따라 'true' or 'false'
   */
  public static boolean isNumber(String str) {
      if (str == null || str.equals("")) return false;

      str = str.trim();
      int len = str.length();
      if (len == 0)
          return false;

      for (int i = 0; i < len; i++) {
          if (!isNumber(str.charAt(i)))
              return false;
      }
      return true;
  }
  
	/**
	 * 실제 존재하는 날짜인지 체크한다. "yyyyMMdd" 형식
	 * @param 날짜 String ex)"20030202"
	 * @return boolean true 날짜 형식이 맞고, 존재하는 날짜일 때
	 *                 false 날짜 형식이 맞지 않거나, 존재하지 않는 날짜일 때
	 */
	public static boolean isValid(String s) throws Exception {
		return isValid(s, "yyyyMMdd");
	}

	/**
	 * 사용자가 입력한 날짜가 실제 존재하는지 체크한다.
	 * @param s date string you want to check.
	 * @param format string representation of the date format. For example, "yyyy-MM-dd".
	 * @return boolean true 날짜 형식이 맞고, 존재하는 날짜일 때
	 *                 false 날짜 형식이 맞지 않거나, 존재하지 않는 날짜일 때
	 */
	public static boolean isValid(String s, String format) {
		
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat (format, java.util.Locale.KOREA);
		
		java.util.Date date = null;
		
		try 
		{
			//int temp = Integer.parseInt(s);
			
			date = formatter.parse(s);
		}
		catch(java.text.ParseException e) 
		{
            return false;
		}
		catch(NumberFormatException ex){
			return false;
		}

		if ( ! formatter.format(date).equals(s) )
            return false;

        return true;
	}
	
	/**
	 * 시작일이 종료일보다 큰지 체크
	 * @param sStrDate
	 * @param sEndDate
	 * @param iDateSize
	 * @return
	 */
   public static boolean chkDate(String sStrDate, String sEndDate, int iDateSize){
	   boolean bFlag = true;
	   
	   if( sStrDate !=null && sStrDate.length() == iDateSize && sEndDate !=null && sEndDate.length()==iDateSize ){
		   if(Integer.parseInt(sStrDate) > Integer.parseInt(sEndDate) ){
			   bFlag =  false;
		   } else {
			   bFlag =  true;
		   }
	   }
	   return bFlag;
   }
}

