package kr.or.copyright.mls.console.mber.inter;

import java.util.Map;

public interface FdcrAd83Service{

	/**
	 * 회원정보 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd83List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 회원정보 상세조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd83View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 담당자, 회원 비밀번호 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd83Update1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 담당자, 회원 업체명 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd83CommNameUpdate( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 담당자, 업체 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd83CompanyDelete( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 비밀번호 틀린횟수 초기화
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fncUpdateFailCnt( Map<String, Object> commandMap ) throws Exception;
}
