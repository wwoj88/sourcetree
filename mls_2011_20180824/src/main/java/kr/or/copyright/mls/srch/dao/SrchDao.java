package kr.or.copyright.mls.srch.dao;

import java.util.List;
import java.util.Map;

public interface SrchDao {

	public List codeList(String srchDivs);
	//법정허락
	public List findStatWorks(Map params); 	//ml_stat_works
	public int countStatWorks(Map params);	//검색된 게시물 전체 갯수
	public List findStatDetl(Map params);	//각 테이블별 상세내용
	public List getCoptHodr(Map params);	//ml_non_idnt_works_copthodr
	//위탁관리 저작물
	public List findCommWorks(Map params);	//ml_comm_works
	public List findCommDetl(Map params);	//각 테이블별 상세내용
	public int countCommWorks(Map params);	//검색된 게시물 전체 갯수
	//법정허락등록부
	public List findCrosWorks(Map params);	//법정허락 등록부 리스트
	public int countCrosWorks(Map params);	//갯수
	
	public List findCertWorks(Map params);
	public int countFindCertWorks(Map params);
	public void insertSrch(String worksTitle);
	public List getSearchWordList() throws Exception;
}
