package kr.or.copyright.mls.intg.service;

import java.util.List;

import kr.or.copyright.mls.intg.dao.IntgDao;
import kr.or.copyright.mls.intg.model.Intg;
import kr.or.copyright.mls.common.common.service.BaseService;

public class IntgServiceImpl extends BaseService implements IntgService {

	//private Log logger = LogFactory.getLog(getClass());
	
	public IntgDao intgDao;
//	private DaoManager daoMg r;
	
	public void setIntgDao(IntgDao intgDao) {
//		daoMgr = DaoConfig.getDaoManager();
		this.intgDao = intgDao;
	}
	
	//이미지 검색 CNT
	public String srchImgCnt(Intg IntgDTO) {
		
		String cnt = intgDao.srchImgCnt(IntgDTO);
		
		return cnt;
	}
	// 이미지 검색
	public List srchImg(Intg IntgDTO) {
		
		List list = intgDao.srchImg(IntgDTO);
		
		return list;
	}
	
	// 이미지 상세
	public List detailImg(Intg IntgDTO) {

		List list = intgDao.detailImg(IntgDTO);
		
		return list;
	}
	
	
	// 방송음악 검색 CNT
	public String srchBrctCnt(Intg IntgDTO) {
		
		String cnt = intgDao.srchBrctCnt(IntgDTO);
		
		return cnt;
	}
	// 방송음악 검색
	public List srchBrct(Intg IntgDTO) {
		
		List list = intgDao.srchBrct(IntgDTO);
		
		return list;
	}
	
	// 방송음악 상세 
	public List detailBrct(Intg IntgDTO) {

		List list = intgDao.detailBrct(IntgDTO);
		
		return list;
	}
	
	
	// 교과용 검색 CNT
	public String srchSubjCnt(Intg IntgDTO) {
		
		String cnt = intgDao.srchSubjCnt(IntgDTO);
		
		return cnt;
	}
	// 교과용 검색
	public List srchSubj(Intg IntgDTO) {
		
		List list = intgDao.srchSubj(IntgDTO);
		
		return list;
	}
	
	// 방송음악 상세 
	public List detailSubj(Intg IntgDTO) {

		List list = intgDao.detailSubj(IntgDTO);
		
		return list;
	}
	
	
	//	도서관 검색 CNT
	public String srchLibrCnt(Intg IntgDTO) {
		
		String cnt = intgDao.srchLibrCnt(IntgDTO);
		
		return cnt;
	}
	
	// 도서관 검색
	public List srchLibr(Intg IntgDTO) {
		
		List list = intgDao.srchLibr(IntgDTO);
		
		return list;
	}
	
	//	도서관 상세 
	public List detailLibr(Intg IntgDTO) {

		List list = intgDao.detailLibr(IntgDTO);
		
		return list;
	}	
}
