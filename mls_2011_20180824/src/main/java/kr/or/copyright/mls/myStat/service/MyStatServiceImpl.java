package kr.or.copyright.mls.myStat.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.common.common.utils.CommonUtil2;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.myStat.dao.MyStatDao;
import kr.or.copyright.mls.myStat.model.ApplySign;
import kr.or.copyright.mls.myStat.model.File;
import kr.or.copyright.mls.myStat.model.StatApplication;
import kr.or.copyright.mls.myStat.model.StatApplicationShis;
import kr.or.copyright.mls.myStat.model.StatApplyWorks;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Service("myStatService")
public class MyStatServiceImpl extends BaseService implements MyStatService {
	
	
	@javax.annotation.Resource(name="myStatDao")
	private MyStatDao myStatDao;

	public void setMyStatDao(MyStatDao myStatDao) {
		this.myStatDao = myStatDao;
	}

	private String realUploadPath = FileUtil.uploadPath();
	
	
	//이용승인 신청 현황 목록 조회
	public List statRsltInqrList(StatApplication params) {
		
		List list = myStatDao.statRsltInqrList(params);
		
		return list;
	}
	//이용승인 신청 현황 목록 조회
	public int totalRowStatRsltInqrList(StatApplication params) {
		
		int totalRow = myStatDao.statRsltInqrListCount(params);
		
		
		return totalRow;
	}
	
	
	//신청서 신규 차수 조회 
	public int newApplyWriteSeq(String sDiv) {
		
		int newApplyWriteSeq = 0;
		
		if(sDiv.equals("TMP")) {
			newApplyWriteSeq = myStatDao.newTmpApplyWriteSeq();
		}else{
			newApplyWriteSeq = myStatDao.newApplyWriteSeq();
		}
		
		return newApplyWriteSeq;
		
	}
	
	
    // step1 임시저장
    public void insertTmpStatPrps(HttpServletRequest request,
	    StatApplication params) {
	try {
	    // 이용승인 신청서 TMP 등록
	    myStatDao.insertStatApplicationTmp(params);
	    
	    List paramList = new ArrayList();
	    
	    for (int i = 0; i < params.getApplyWorksCnt(); i++) {
		StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
		
		statApplyWorksInfo.setApplyWriteYmd(params.getApplyWriteYmd());//신청서작성YMD
		statApplyWorksInfo.setApplyWriteSeq(params.getApplyWriteSeq());//신청서작성SEQ
		statApplyWorksInfo.setWorksSeqn(i + 1);//명세서순번
		statApplyWorksInfo.setWorksId(params.getChkWorksId()[i]);//저작물ID
		
		//신청구분 setting
		statApplyWorksInfo.setApplyType01(params.getApplyType01());
		statApplyWorksInfo.setApplyType02(params.getApplyType02());
		statApplyWorksInfo.setApplyType03(params.getApplyType03());
		statApplyWorksInfo.setApplyType04(params.getApplyType04());
		statApplyWorksInfo.setApplyType05(params.getApplyType05());
		
		statApplyWorksInfo.setWorksKind(params.getApplyWorksKind());//종류
		statApplyWorksInfo.setWorksForm(params.getApplyWorksForm());//형태
		
		paramList.add(statApplyWorksInfo);
	    }
	    
	    
	    StatApplyWorks param = (StatApplyWorks)paramList.get(0);

	    myStatDao.deleteStatApplyWorksTmp(param);
	    
	    myStatDao.insertStatApplyWorksTmp(paramList);
	    
	    

	    List fileList = params.getFileList();

	    // 첨부파일 및 맵핑정보 등록
	    if (fileList.size() > 0) {
		myStatDao.insertFile(fileList);
		myStatDao.insertStatAttcFileTmp(fileList);
	    }

	    // 명세서 테이블 등록(1단계 임시저장 저작물아이디등록)

	} catch (Exception e) {
	    e.printStackTrace();
	    TransactionAspectSupport.currentTransactionStatus()
		    .setRollbackOnly();
	}
    }

    public Map<String, Object> selectExcelStatApplyWorksInfoTmp(StatApplication params) throws Exception {
	List fileList = params.getFileList();
	File paramFile = (File) fileList.get(0);
	paramFile.setApplyWriteYmd(params.getApplyWriteYmd());
	paramFile.setApplyWriteSeq(params.getApplyWriteSeq());
	Map<String, Object> map;

	if (paramFile.getFileNameCd().equals("1")) {
	    File file = myStatDao.excelStatAttcFileTmp(paramFile);
	    map = gexResult(file, params);
	    map.put("fileInfo", file);
	    return map;
	}
	return null;
    }
    
    
    public Map<String, Object> selectExcelStatApplyWorksInfo(StatApplication params) throws Exception {
	List fileList = params.getFileList();
	File paramFile = (File) fileList.get(0);
	paramFile.setApplyWriteYmd(params.getApplyWriteYmd());
	paramFile.setApplyWriteSeq(params.getApplyWriteSeq());
	Map<String, Object> map;
	
	if (paramFile.getFileNameCd().equals("1")) {
	    File file = myStatDao.excelStatAttcFile(paramFile);
	    map = gexResult(file, params);
	    map.put("fileInfo", file);
	    return map;
	}
	return null;
    }
    
	
    // 법정허락 이용승인 엑셀명세서 파일 저장(저장:신규,개인저작물) 20120820 /수정/ 정병호
    public void insertExcelStatApplyWorksInfo(StatApplication params)
	    throws Exception {
	List fileList = params.getFileList();
	File paramFile = (File) fileList.get(0);
	paramFile.setApplyWriteYmd(params.getApplyWriteYmd());
	paramFile.setApplyWriteSeq(params.getApplyWriteSeq());
	Map<String, Object> map = null;
	
	if (paramFile.getFileNameCd().equals("1")) {
	    map = gexResult(paramFile, params);
	}
	
	List<StatApplyWorks> resultList = (List<StatApplyWorks>) map.get("result");
	
	
/*	 StatApplyWorks param = (StatApplyWorks)paramList.get(0);

	    myStatDao.deleteStatApplyWorksTmp(param);
	    
	    myStatDao.insertStatApplyWorksTmp(paramList);*/
	
	
	if(resultList.size() >0){
	    
	    for(int i=0; i<resultList.size(); i++){
		StatApplyWorks param = (StatApplyWorks)resultList.get(i);
		param.setWorksSeqn(i+1);
	    }
	    
	    StatApplyWorks param = (StatApplyWorks)resultList.get(0);

	    myStatDao.deleteStatApplyWorks(param);
	    
	    myStatDao.insertStatApplyWorks(resultList);
	}
	

    }
    
    // 법정허락 이용승인 임시 엑셀명세서 파일 저장(임시저장:신규,개인저작물) 20120820 /수정/ 정병호
    public void insertExcelStatApplyWorksInfoTmp(StatApplication params)
	    throws Exception {
	List fileList = params.getFileList();
	File paramFile = (File) fileList.get(0);
	paramFile.setApplyWriteYmd(params.getApplyWriteYmd());
	paramFile.setApplyWriteSeq(params.getApplyWriteSeq());
	Map<String, Object> map = null;
	
	if (paramFile.getFileNameCd().equals("1")) {
	    map = gexResult(paramFile, params);
	}
	
	List<StatApplyWorks> resultList = (List<StatApplyWorks>) map.get("result");
	
	
/*	 StatApplyWorks param = (StatApplyWorks)paramList.get(0);

	    myStatDao.deleteStatApplyWorksTmp(param);
	    
	    myStatDao.insertStatApplyWorksTmp(paramList);*/
	
	if(resultList.size() >0){
	    
	    for(int i=0; i<resultList.size(); i++){
		StatApplyWorks param = (StatApplyWorks)resultList.get(i);
		param.setWorksSeqn(i+1);
		
	    }
	    
	    StatApplyWorks param = (StatApplyWorks)resultList.get(0);

	    myStatDao.deleteStatApplyWorksTmp(param);
	    
	    myStatDao.insertStatApplyWorksTmp_P(resultList);
	}
	

    }
    
    // 법정허락 이용승인 신청서tmp 저장(임시저장:신규,개인저작물) 20120816 /수정/ 정병호
    public void insertTmpStatPrps_priv(HttpServletRequest request,
	    StatApplication params,String existYn) {
	try {
	    // 이용승인 신청서 TMP 등록
	    myStatDao.insertStatApplicationTmp(params);
	    
	    List paramList = new ArrayList();
	    //if(!existYn.equals("Y")){
		    
	    for (int i = 0; i < params.getApplyWorksCnt(); i++) {
		StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
		statApplyWorksInfo.setApplyWriteYmd(params.getApplyWriteYmd());
		statApplyWorksInfo.setApplyWriteSeq(params.getApplyWriteSeq());
		statApplyWorksInfo.setWorksSeqn(i + 1);

		paramList.add(statApplyWorksInfo);
	    }
	    StatApplyWorks param = (StatApplyWorks) paramList.get(0);

	    myStatDao.deleteStatApplyWorksTmp(param);

	    myStatDao.insertStatApplyWorksTmp_P(paramList);
	    //}
	    
	    List fileList = params.getFileList();

	    // 첨부파일 및 맵핑정보 등록
	    if (fileList.size() > 0) {
		myStatDao.insertFile(fileList);
		myStatDao.insertStatAttcFileTmp(fileList);
	    }
	    // 명세서 테이블 등록(1단계 임시저장 저작물아이디등록)

	} catch (Exception e) {
	    e.printStackTrace();
	    TransactionAspectSupport.currentTransactionStatus()
		    .setRollbackOnly();
	}
    }
    
    public Map<String, Object> gexResult(File fileObj,StatApplication params) throws Exception{
Map<String, Object> map = new HashMap<String, Object>();
	
	List<StatApplyWorks> result = new ArrayList<StatApplyWorks>();

	int totWriteRow = 0; //작성한 로우의 합
	int totErrRow = 0; //에러 로우의 합
	
	    
	    int rowCnt = 0;
	    
	    String fileType = "";
	    fileType = fileObj.getRealFileName().substring(fileObj.getRealFileName().length()-1, fileObj.getRealFileName().length());
	    
	    if(fileType.toUpperCase().equals("X")){
		XSSFWorkbook wBook = new XSSFWorkbook(fileObj.getFilePath()+fileObj.getRealFileName());
		XSSFSheet sheet = wBook.getSheetAt(0); // 첫번째 시트
		rowCnt = sheet.getPhysicalNumberOfRows();	// row 수
		/*System.out.println("################################################################################");
		System.out.println("####################################sheet.getRows() ::" + rowCnt+"############################################");
		System.out.println("################################################################################");*/
		
		if(rowCnt>1004){
		    result = null;
		    map.put("writeRow", 0);//작성 건수가 0건(다시 일괄작성)
		    map.put("errRow", 0);//오류 건수
		    map.put("isAllEmpty", false);
		    map.put("result", result);
		    return map;
		}
		int emptyRow =0;
		int totalRow =0;
		//4번째 row 부터
		for(int k = 3; rowCnt<1004 && k<rowCnt; k++){
		    totalRow++;
		    XSSFRow row = sheet.getRow(k);
		    
		    if(row != null){
			StatApplyWorks applyWorksVO = new StatApplyWorks();
			applyWorksVO.setApplyWriteYmd(params.getApplyWriteYmd());
			applyWorksVO.setApplyWriteSeq(params.getApplyWriteSeq());
			
			int cellCnt = row.getLastCellNum();
			
			String sFlag = "";
			
			int idx = 0;
			int emptyCnt = 0;
			
			for(int c = 0; c<cellCnt; c++){
			    idx = 0;
			    
			    XSSFCell cell = row.getCell(c);
			    
			    if(cell != null){
				
				String value = null;
				
				switch(cell.getCellType()){
					case XSSFCell.CELL_TYPE_FORMULA:
					    value=cell.getCellFormula().trim();
					    break;
					case XSSFCell.CELL_TYPE_NUMERIC:
					    value=""+(int)cell.getNumericCellValue();
					    break;
					case XSSFCell.CELL_TYPE_STRING:
					    value=""+cell.getStringCellValue().trim();
					    break;
					case XSSFCell.CELL_TYPE_BLANK:
					    value="";
					    break;
					case XSSFCell.CELL_TYPE_ERROR:
					    value=""+cell.getErrorCellValue();
					    break;
					default:
				}
				
				if(value.length() == 0){
				    emptyCnt++;
				}
				
				//0. 순번
				if(c == idx++){
				    
				}
				
				// 1.신청구분[필수]
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 1 || !CommonUtil2.isNumber(value)){
					sFlag += "A"+c+"@";
				    }else{
        				    switch (Integer.parseInt(value)) {
        				    case 1:
        					applyWorksVO.setApplyType01("1");
        					applyWorksVO.setApplyType(value);
        					break;
        				    case 2:
        					applyWorksVO.setApplyType02("1");
        					applyWorksVO.setApplyType(value);
        					break;
        				    case 3:
        					applyWorksVO.setApplyType03("1");
        					applyWorksVO.setApplyType(value);
        					break;
        				    case 4:
        					applyWorksVO.setApplyType04("1");
        					applyWorksVO.setApplyType(value);
        					break;
        				    case 5:
        					applyWorksVO.setApplyType05("1");
        					applyWorksVO.setApplyType(value);
        					break;
        				    default:
        					sFlag += "A"+c+"@";
        				    }
				    }
				}
				
				// 2.제호/제목[필수]
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 150){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setWorksTitl(value);
				    }
				}
				
				// 3.종류[필수]
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 60){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setWorksKind(value);
				    }
				}
				
				// 4.형태및수량[필수]
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 60){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setWorksForm(value);
				    }
				}
				
				// 5.신청물의내용[필수]
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 1300){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setWorksDesc(value);
				    }
				}
				
				// 6.공표연월일[필수]
				if(c == idx++){
				    if(!CommonUtil2.isNumber(value) || value.length() == 0 || value.length() > 8){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setPublYmd(value);
				    }
				}
				
				// 7.공표국가[필수]
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 30){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setPublNatn(value);
				    }
				}
				
				// 8.공표방법CD[필수]
				//코드 범위 정규식 만들어야 함 1-6, 99 가 true 가 되도록 
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 2 || !CommonUtil2.isNumber(value) || !CommonUtil2.isPubCod(value)){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setPublMediCd(Integer.parseInt(value));
				    }
				}
				
				// 9.공표방법기타
				if(c == idx++){
				    if(value.length()>60){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setPublMediEtc(value);
				    }
				}
				
				// 10.공표매체정보[필수]
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 60){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setPublMedi(value);
				    }
				}
				
				// 11.권리자성명/법인명[필수]
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 60){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setCoptHodrName(value);
				    }
				}
				
				// 12.권리전화번호[필수]
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 13 || !CommonUtil2.isPhone(value)){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setCoptHodrTelxNumb(value);
				    }
				}
				
				// 13.권리자주소[필수]
				if(c == idx++){
				    if(value.length() == 0 || value.length() > 60){
					sFlag += "A"+c+"@";
				    }else{
					applyWorksVO.setCoptHodrAddr(value);
				    }
				}
				
				/*System.out.println("Row:Cell = "+k+":"+c+" = "+value.trim()+"::"+idx);*/
			    }
			   
			} // // .. end Cell_FOR
			applyWorksVO.setErrMsg(sFlag);
			
			if(sFlag.length() > 0){
			    applyWorksVO.setIsUse("N");
			}else{
			    applyWorksVO.setIsUse("Y");
			}
			
			/*System.out.println("START====================="+k);*/
			if(emptyCnt != 14){
			   /* System.out.println(applyWorksVO.toString());*/
			    if(applyWorksVO.getIsUse().equals("N")){
				totErrRow++;
			    }
			    result.add(applyWorksVO);
			}else if(emptyCnt == 14){
			    //작성한 명세서중 빈 로우 값을 증가
			    emptyRow++;
			}
			/*System.out.println("END======================="+k);*/
		    }
		}// // .. end Row_FOR
		System.out.println("#####totalRow#####"+totalRow+"###########");
		System.out.println("#####emptyRow#####"+emptyRow+"###########");
		System.out.println("#####writeROW#####"+(totalRow-emptyRow)+"###########");
		System.out.println("#####totErrRow#####"+totErrRow+"###########");
		totWriteRow = (totalRow-emptyRow);
	    }
	if(totWriteRow == 0){
	    result = null;
	    map.put("writeRow", 0);//작성 건수가 0건(다시 일괄작성)
	    map.put("errRow", 0);//오류 건수
	    map.put("isAllEmpty", true);
	    map.put("result", result);
	    
	    return map;
	}
	
	map.put("writeRow", totWriteRow);
	map.put("errRow", totErrRow);//오류 건수
	map.put("isAllEmpty", false);
	map.put("result", result);
	
	return map;
    }
	
	
	//이용승인 신청서tmp 조회
	public StatApplication statApplicationTmp(StatApplication params) {
		
		StatApplication VO = myStatDao.statApplicationTmp(params);
		
		return VO;
	}
	
	
	//이용승인 신청서 조회
	public StatApplication statApplication(StatApplication params) {
		
		StatApplication VO = myStatDao.statApplication(params);
		
		return VO;
	}
	
	
	//이용승인 신청서 상태변경내역 조회
	public List statApplicationShisList(StatApplicationShis params) {
		
		List list = myStatDao.statApplicationShisList(params);
		
		return list;
	}
	
	
	//이용승인 신청서tmp 첨부파일 조회
	public List statAttcFileTmp(StatApplication params) {
		
		List fileList = myStatDao.statAttcFileTmp(params);
		
		return fileList;
	}
	
	
	//이용승인 신청서 첨부파일 조회
	public List statAttcFile(StatApplication params) {
		
		List fileList = myStatDao.statAttcFile(params);
		
		return fileList;
	}
	
	
    // step1 수정(임시저장)
    public void updateTmpStatPrps(HttpServletRequest request,
	    StatApplication params) {

	try {
	    // 이용승인 신청서TMP 수정
	    myStatDao.updateStatApplicationTmp(params);

	    List paramList = new ArrayList();

	    for (int i = 0; i < params.getApplyWorksCnt(); i++) {
		StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
		statApplyWorksInfo.setApplyWriteYmd(params.getApplyWriteYmd());
		statApplyWorksInfo.setApplyWriteSeq(params.getApplyWriteSeq());
		statApplyWorksInfo.setWorksSeqn(i + 1);
		statApplyWorksInfo.setWorksId(params.getChkWorksId()[i]);
		
		//신청구분 setting
		statApplyWorksInfo.setApplyType01(params
			.getApplyType01());
		statApplyWorksInfo.setApplyType02(params
			.getApplyType02());
		statApplyWorksInfo.setApplyType03(params
			.getApplyType03());
		statApplyWorksInfo.setApplyType04(params
			.getApplyType04());
		statApplyWorksInfo.setApplyType05(params
			.getApplyType05());
		
		statApplyWorksInfo.setWorksKind(params.getApplyWorksKind());//종류
		statApplyWorksInfo.setWorksForm(params.getApplyWorksForm());//형태

		paramList.add(statApplyWorksInfo);
	    }

	    StatApplyWorks param = (StatApplyWorks) paramList.get(0);

	    myStatDao.deleteStatApplyWorksTmp(param);

	    myStatDao.insertStatApplyWorksTmp(paramList);

	    List fileList = params.getFileList();
	    List delFileList = params.getDelFileList();

	    // 첨부파일 및 맵핑정보, 삭제
	    if (delFileList.size() > 0) {
		myStatDao.deleteStatAttcFileTmp(delFileList);
		myStatDao.deleteFile(delFileList);
	    }

	    // 첨부파일 및 맵핑정보, 등록 및 수정
	    if (fileList.size() > 0) {
		myStatDao.insertFile(fileList);
		myStatDao.insertStatAttcFileTmp(fileList);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    TransactionAspectSupport.currentTransactionStatus()
		    .setRollbackOnly();
	}
    }
    
 // step1 수정(임시저장)
    public void updateTmpStatPrps_priv(HttpServletRequest request,
	    StatApplication params, boolean isExcel, String existYn) {

	try {
	    // 이용승인 신청서TMP 수정
	    if(!isExcel){
		myStatDao.updateStatApplicationTmp(params);
	    }
	    
	    List paramList = new ArrayList();
	    //if(!existYn.equals("Y")){
		
		int curCnt = params.getApplyWorksCnt();
		int prevCnt = params.getPrevCnt();
		
		
		int statApplyWorksCount = myStatDao.getStatApplyWorksCount(params);
		
		
		//명세서 테이블에 건수가 없다면(이전에 파일 첨부했음) 입력받은 건수대로 테이블을 insert하여 셋팅을 하라!
		if(statApplyWorksCount == 0){
		    List paramList1 = new ArrayList();
		    
		    for (int i = 0; i < params.getApplyWorksCnt(); i++) {
			StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
			statApplyWorksInfo.setApplyWriteYmd(params
				.getApplyWriteYmd());
			statApplyWorksInfo.setApplyWriteSeq(params
				.getApplyWriteSeq());
			statApplyWorksInfo.setWorksSeqn(i + 1);

			paramList1.add(statApplyWorksInfo);
		    }
		    StatApplyWorks param1 = (StatApplyWorks)paramList1.get(0);
		    
		    myStatDao.deleteStatApplyWorksTmp(param1);
			
		    myStatDao.insertStatApplyWorksTmp_P(paramList1);
		    
		}else{
		    if(curCnt > prevCnt){//현재건수가 이전건수보다 더 크다면 이전건수+1 인덱스로 추가를 해줘야함
			for (int i = prevCnt; i < curCnt; i++) {
			    StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
			    statApplyWorksInfo.setApplyWriteYmd(params
				    .getApplyWriteYmd());
			    statApplyWorksInfo.setApplyWriteSeq(params
				    .getApplyWriteSeq());
			    statApplyWorksInfo.setWorksSeqn(i + 1);
			    paramList.add(statApplyWorksInfo);
			}
			myStatDao.insertStatApplyWorksTmp_P(paramList);
		    } else if (curCnt < prevCnt) {// 현재건수가 이전건수보다 더 작다면 현재건수+1
						  // 인덱스를 삭제 해줘야함
			for (int i = curCnt; i < prevCnt; i++) {
			    StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
			    statApplyWorksInfo.setApplyWriteYmd(params
				    .getApplyWriteYmd());
			    statApplyWorksInfo.setApplyWriteSeq(params
				    .getApplyWriteSeq());
			    statApplyWorksInfo.setWorksSeqn(i + 1);
			    paramList.add(statApplyWorksInfo);
			}
			myStatDao.deleteWorksSeqnStatApplyWorksTmp(paramList);
		    } else {
			System.out
				.println("##################################################################################################");
			System.out
				.println("##################################################################################################");
			System.out.println("변동없음");
			System.out
				.println("##################################################################################################");
			System.out
				.println("##################################################################################################");
		    }
		}
	    //}else{
		//엑셀파일 첨부를 하였다면 기존의 명세서는 다 날려 버린다. 
		
		//직접입력 5건 넣고 이전화면 가서 엑셀파일로 7건을 넣으면 명세서 테이블은 그대로 5건이 된다.
		//엑셀 파일은 1단계에서 2단계 올때 명세서를 참고 하지 않고 파일을 참고하여 테이블을 생성하기때문에
		//다시 엑셀파일에서 직접입력으로 변경을 하고 3건을 넣으면 이전 건수 7건을 기준으로 하여 
		//테이블을 변경을 한다. 이경우 이전에는 7건 현재는 3건이기 때문에 4,5,6,7 로우를 삭제 하게 되는데
		//사실 현재 로우는 5개 밖에 없어서 6,7번은 없는 항목이다.
		
		//반대로 직접입력에서 5건을 넣고 이전화면에 가서 엑셀파일로 3건을 입력하면 명세서는 그대로 5건이 된다. 
		//엑셀 파일은 1단계에서 2단계 올때 명세서를 참고하지 않고 파일을 참고하여 테이블을 생성하기 때문에
		//다시 엑셀파일에서 직접입력으로 변경을 하고 4건을 넣으면 이전 건수 3건을 기준으로 하여
		//테이블을 변경한다. 이경우 이전에는 이전 값 3건 현재는 4건이기때문에 4번째로우를 더 더하게 되는데
		//사실테이블은 현재 5건이 있기 때문에 변동이 없다 4번째 값을 더할 의미가 없다. 
		
		
		
		//그런데 엑셀파일을 첨부 할때 마다 기존의 명세서를 리셋을 하게 되면 
		//직접입력으로 2단계 저장을 한 데이터가 사라진다. 
		
		
		
		//결론적으로 해결 방안은
		//엑셀 existYN 일때도 명세서 테이블에 신청 건수 만큼 로우를 INSERT를 해주고
		//수정을 했을때 이전 값 현재 값 비교를 해서 있는 값은 그대로 두고 없는 값은 추가를 해준다. 
		
		//StatApplyWorks param = new StatApplyWorks();
		//param.setApplyWriteYmd(params.getApplyWriteYmd());
		//param.setApplyWriteSeq(params.getApplyWriteSeq());
		
		//myStatDao.deleteStatApplyWorksTmp(param);
		
	    //}
	    List fileList = params.getFileList();
	    List delFileList = params.getDelFileList();

	    // 첨부파일 및 맵핑정보, 삭제
	    if (delFileList.size() > 0) {
		myStatDao.deleteStatAttcFileTmp(delFileList);
		myStatDao.deleteFile(delFileList);
	    }

	    // 첨부파일 및 맵핑정보, 등록 및 수정
	    if (fileList.size() > 0) {
		myStatDao.insertFile(fileList);
		myStatDao.insertStatAttcFileTmp(fileList);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    TransactionAspectSupport.currentTransactionStatus()
		    .setRollbackOnly();
	}
    }
    
    // step1 수정(보완저장)
    public void updateStatPrps_priv(HttpServletRequest request,
	    StatApplication params, boolean isExcel, String existYn) {

	try {
	    // 이용승인 신청서TMP 수정
	    if(!isExcel){
		myStatDao.updateStatApplication(params);
	    }
	    
	    List paramList = new ArrayList();
	    //if(!existYn.equals("Y")){
		
		int curCnt = params.getApplyWorksCnt();
		int prevCnt = params.getPrevCnt();
		
		
		if(curCnt > prevCnt){//현재건수가 이전건수보다 더 크다면 이전건수+1 인덱스로 추가를 해줘야함
		    for(int i=prevCnt; i<curCnt; i++){
			StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
			statApplyWorksInfo.setApplyWriteYmd(params.getApplyWriteYmd());
			statApplyWorksInfo.setApplyWriteSeq(params.getApplyWriteSeq());
			statApplyWorksInfo.setWorksSeqn(i + 1);
/*			statApplyWorksInfo.setApplyType01(params
				.getApplyType01());
			statApplyWorksInfo.setApplyType02(params
				.getApplyType02());
			statApplyWorksInfo.setApplyType03(params
				.getApplyType03());
			statApplyWorksInfo.setApplyType04(params
				.getApplyType04());
			statApplyWorksInfo.setApplyType05(params
				.getApplyType05());*/			
			paramList.add(statApplyWorksInfo);
		    }
		    myStatDao.insertStatApplyWorks(paramList);
		}else if(curCnt < prevCnt){//현재건수가 이전건수보다 더 작다면 현재건수+1 인덱스를 삭제 해줘야함
		    for(int i=curCnt; i<prevCnt; i++){
			StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
			statApplyWorksInfo.setApplyWriteYmd(params.getApplyWriteYmd());
			statApplyWorksInfo.setApplyWriteSeq(params.getApplyWriteSeq());
			statApplyWorksInfo.setWorksSeqn(i + 1);
			paramList.add(statApplyWorksInfo);
		    }
		    myStatDao.deleteWorksSeqnStatApplyWorks(paramList);
		}else{
		    //System.out.println("변동없음");
		}
	   // }
	    

	    List fileList = params.getFileList();
	    List delFileList = params.getDelFileList();

	    // 첨부파일 및 맵핑정보, 삭제
	    if (delFileList.size() > 0) {
		myStatDao.deleteStatAttcFile(delFileList);
		myStatDao.deleteFile(delFileList);
	    }

	    // 첨부파일 및 맵핑정보, 등록 및 수정
	    if (fileList.size() > 0) {
		myStatDao.insertFile(fileList);
		myStatDao.insertStatAttcFile(fileList);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    TransactionAspectSupport.currentTransactionStatus()
		    .setRollbackOnly();
	}
    }
	
	//이용승인 명세서tmp 삭제
	public void deleteStatApplyWorksTmp(StatApplyWorks param) {

		try {
			myStatDao.deleteStatApplyWorksTmp(param);
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	
	//step1 수정(보완임시저장)
	public void updateStatPrps(HttpServletRequest request, StatApplication params) {
		
		try {
			//이용승인 신청서TMP 수정
			myStatDao.updateStatApplication(params);
			
			//이용승인 신청 상태변경내역 등록
			params.setNewApplyWriteYmd(params.getApplyWriteYmd());
			params.setNewApplyWriteSeq(params.getApplyWriteSeq());
			myStatDao.insertStatApplicationShis(params);
			
			//이용승인 신청 명세서 삭제 및 등록
			List paramList = new ArrayList();
			for (int i = 0; i < params.getApplyWorksCnt(); i++) {
			    StatApplyWorks statApplyWorksInfo = new StatApplyWorks();
			    statApplyWorksInfo.setApplyWriteYmd(params.getApplyWriteYmd());
			    statApplyWorksInfo.setApplyWriteSeq(params.getApplyWriteSeq());
			    statApplyWorksInfo.setWorksSeqn(i + 1);
			    statApplyWorksInfo.setWorksId(params.getChkWorksId()[i]);
			    paramList.add(statApplyWorksInfo);
			    
			  //신청구분 setting
			    statApplyWorksInfo.setApplyType01(params
					.getApplyType01());
			    statApplyWorksInfo.setApplyType02(params
					.getApplyType02());
			    statApplyWorksInfo.setApplyType03(params
					.getApplyType03());
			    statApplyWorksInfo.setApplyType04(params
					.getApplyType04());
			    statApplyWorksInfo.setApplyType05(params
					.getApplyType05());
				
			    statApplyWorksInfo.setWorksKind(params.getApplyWorksKind());//종류
			    statApplyWorksInfo.setWorksForm(params.getApplyWorksForm());//형태
			}
			StatApplyWorks param = (StatApplyWorks) paramList.get(0);
                
			//myStatDao.deleteStatApplyWorks(param);
                
                	//myStatDao.insertStatApplyWorks(paramList);
			
			myStatDao.updateStatApplyWorks(param);

			List fileList = params.getFileList();
			List delFileList = params.getDelFileList();
			
			
			//첨부파일 및 맵핑정보, 삭제
			if(delFileList.size() > 0) {	
				myStatDao.deleteStatAttcFile(delFileList);
				myStatDao.deleteFile(delFileList);
			}
			
			//첨부파일 및 맵핑정보, 등록 및 수정
			if(fileList.size() > 0) {	
				myStatDao.insertFile(fileList);
				myStatDao.insertStatAttcFile(fileList);
			}
			
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	//이용승인 명세서tmp 삭제
	public void deleteStatApplyWorks(StatApplyWorks param) {

		try {
			myStatDao.deleteStatApplyWorks(param);
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	
	//step2 임시저장
	public void insertTmpStatApplyWorksList(List list) {
		
		try{
			StatApplyWorks param = (StatApplyWorks) list.get(0);
			
			myStatDao.deleteStatApplyWorksTmp(param);
			myStatDao.insertStatApplyWorksTmp(list);
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	
	//step2 보완임시저장
	public void insertStatApplyWorksList(List list) {
		
		try{
			StatApplyWorks param = (StatApplyWorks) list.get(0);
			
			myStatDao.deleteStatApplyWorks(param);
			myStatDao.insertStatApplyWorks(list);
			
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	
	//step2 보완임시저장
	public void insertStatApplyWorksList_tmp(List list, String chk) {
		
		try{
			StatApplyWorks param = (StatApplyWorks) list.get(0);
			
			myStatDao.deleteStatApplyWorksTmp(param);
			
			if(chk.equals("P")){
			    myStatDao.insertStatApplyWorksTmp_P(list);
			}else{
			    myStatDao.insertStatApplyWorksTmp(list);
			}
			
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	
	//step3 신청 실행
	public void insertStatPrps(StatApplication param) {
		
		try{
			ApplySign applySign = param.getApplySign();
			
			myStatDao.insertStatApplication(param);
			myStatDao.insertStatApplicationShis(param);
			myStatDao.insertStatAttcFile(param);
			myStatDao.insertStatApplyWorks(param);
			myStatDao.insertApplySign(applySign);
			myStatDao.deleteStatPrpsTmp(param);
			
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	
	//step3 신청 보완실행
	public void updateStatPrps(StatApplication param) {
		
		try{
			myStatDao.updateStatApplicationStat(param);
			myStatDao.insertStatApplicationShis(param);
			
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	
	//이용승인 신청 명세서 tmp 상세조회
	public List statApplyWorksListTmp(StatApplyWorks params) {
		
		List list = myStatDao.statApplyWorksListTmp(params);
		
		return list;
	}
	
	//이용승인 신청 명세서 상세조회
	public List statApplyWorksList(StatApplyWorks params) {
		
		List list = myStatDao.statApplyWorksList(params);
		
		return list;
	}

	//법정허락 이용승인 신청 임시저장 내역 삭제
	public void deleteStatPrps(List params) {
		
		try{
			for(int i = 0; i < params.size(); i++) {
				StatApplication param = (StatApplication)params.get(i);
				myStatDao.deleteStatPrpsTmp(param);
			}
			
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	// 법정허락 결제정보 등록
	public void insertStatPayStatus(StatApplication params){
		try{
			myStatDao.insertStatPayStatus(params);
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	// 법정허락 결제정보 수정
	public void updateStatPayStatus(StatApplication params){
		try{
			myStatDao.updateStatPayStatus(params);
		} catch(Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	  public void insertTmpStatPrps_new(List fileList) {
			
	    	try {
		
		  
	    		 if (fileList.size() > 0) {
	    			 myStatDao.insertFile(fileList);
	    			 myStatDao.insertStatAttcFileTmp(fileList);
	    			//myStatDao.insertStatAttcFile(fileList);
		    }

		

		} catch (Exception e) {
		    e.printStackTrace();
		    TransactionAspectSupport.currentTransactionStatus()
			    .setRollbackOnly();
		}
	    }
	
}