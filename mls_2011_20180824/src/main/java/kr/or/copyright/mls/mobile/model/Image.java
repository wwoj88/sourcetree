package kr.or.copyright.mls.mobile.model;

public class Image {
	private int 
		imgeSeqn
	;
	
	private String
		workName
		, coptHodr
		, lishComp
		, usexYear
		, imageUrl
		, divs
		, wterDivs
	;

	public int getImgeSeqn() {
		return imgeSeqn;
	}

	public void setImgeSeqn(int imgeSeqn) {
		this.imgeSeqn = imgeSeqn;
	}

	public String getWorkName() {
		return workName;
	}

	public void setWorkName(String workName) {
		this.workName = workName;
	}

	public String getCoptHodr() {
		return coptHodr;
	}

	public void setCoptHodr(String coptHodr) {
		this.coptHodr = coptHodr;
	}

	public String getLishComp() {
		return lishComp;
	}

	public void setLishComp(String lishComp) {
		this.lishComp = lishComp;
	}

	public String getUsexYear() {
		return usexYear;
	}

	public void setUsexYear(String usexYear) {
		this.usexYear = usexYear;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDivs() {
		return divs;
	}

	public void setDivs(String divs) {
		this.divs = divs;
	}

	public String getWterDivs() {
		return wterDivs;
	}

	public void setWterDivs(String wterDivs) {
		this.wterDivs = wterDivs;
	}
	
	
}
