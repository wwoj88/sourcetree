package kr.or.copyright.mls.mobile.board.service;

import java.util.Map;

//import kr.or.copyright.mls.mobile.board.model.MBoard;
import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.mobile.board.model.MBoard;

public interface MBoardService {

	public ListResult findBoardList(int pageNo, int rowPerPage, Map params);

	public MBoard boardView(MBoard board);
}
