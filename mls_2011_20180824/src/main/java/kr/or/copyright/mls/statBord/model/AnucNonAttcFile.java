package kr.or.copyright.mls.statBord.model;

public class AnucNonAttcFile {
	private int attcSeqn;
	private long worksId;
	private String rgstIdnt;
	private String rgstDttm;
	
	@Override
	public String toString() {
		return "AnucNonAttcFile [attcSeqn=" + attcSeqn + ", worksId=" + worksId
				+ ", rgstIdnt=" + rgstIdnt + ", rgstDttm=" + rgstDttm + "]";
	}
	public int getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(int attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	
	public long getWorksId() {
		return worksId;
	}
	public void setWorksId(long worksId) {
		this.worksId = worksId;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	
	
	
}
