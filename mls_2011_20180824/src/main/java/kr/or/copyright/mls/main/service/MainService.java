package kr.or.copyright.mls.main.service;

import java.util.List;

import kr.or.copyright.mls.main.model.Main;

public interface MainService {

	public Main mainView(Main main);
	
	public void insertUserCount();
	public List anucBord01();
	public List anucBord06();
	public List anucBord03();
	public List anucBord04();
	public List anucBord05();
	public List QnABord();
	public List statSrch();
	public String alltInmt();
	public List notiList();

	public List anucBord07();
}
