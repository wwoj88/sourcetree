package kr.or.copyright.mls.scheduling;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd20Dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

@Controller
public class updateMuscStatYn {
	

	@javax.annotation.Resource(name = "fdcrAd20Dao")
	private FdcrAd20Dao fdcrAd20Dao;
	protected Log log = LogFactory.getLog(this.getClass());

	public void execute() throws Exception{
		Map map = new HashMap();
		long time = System.currentTimeMillis();         
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		log.debug( "updateLibrStatYn trigger statProcBatch (59 second): current time = " + sdf.format(time) );
		System.out.println("updateLibrStatYn trigger statProcBatch (59 second): current time = " + sdf.format(time));    
		// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트
		fdcrAd20Dao.updateLibrStatYn( map );
		fdcrAd20Dao.updateSubjStatYn( map );
		fdcrAd20Dao.updateLssnStatYn( map );
	}

}
