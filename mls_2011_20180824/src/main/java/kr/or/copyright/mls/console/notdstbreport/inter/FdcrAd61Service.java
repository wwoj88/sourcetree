package kr.or.copyright.mls.console.notdstbreport.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd61Service{

	/**
	 * 등록현황 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd61List1( Map<String, Object> commandMap ) throws Exception;

}
