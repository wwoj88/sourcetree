package kr.or.copyright.mls.console.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 이벤트 등록 및 관리 > 이벤트 운영관리
 * 
 * @author ljh
 */
@Controller
public class FdcrAdA1Controller extends DefaultController{

	@Resource( name = "fdcrAdA1Service" )
	private FdcrAdA1ServiceImpl fdcrAdA1Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA1Controller.class );

	/**
	 * 이벤트 목록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1List1.page" )
	public String fdcrAdA1List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA1Service.fdcrAdA1List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		return "/event/fdcrAdA1List1.tiles";
	}

	/**
	 * 이벤트 상세
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1View2.page" )
	public String fdcrAdA1View2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA1Service.fdcrAdA1View2( commandMap );

		model.addAttribute( "info", commandMap.get( "info" ) );
		model.addAttribute( "rs_agree", commandMap.get( "rs_agree" ) );
		System.out.println( "이벤트 상세 조회" );
		return "/event/fdcrAdA1View2.tiles";
	}

	/**
	 * 이벤트 재등록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1Insert1.page" )
	public void fdcrAdA1Insert1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "USER_ID", "" );
		commandMap.put( "GUBUN", "" );

		String[] AGREEYNS = request.getParameterValues( "AGREEYN" );
		String[] ITEM_DESCS = request.getParameterValues( "ITEM_DESC" );
		String[] ITEM_TITLES = request.getParameterValues( "ITEM_TITLE" );
		String[] AGREE_ITEM_CDS = request.getParameterValues( "AGREE_ITEM_CD" );
		commandMap.put( "AGREEYN", AGREEYNS );
		commandMap.put( "ITEM_DESC", ITEM_DESCS );
		commandMap.put( "ITEM_TITLE", ITEM_TITLES );
		commandMap.put( "AGREE_ITEM_CD", AGREE_ITEM_CDS );

		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAdA1Service.fdcrAdA1Insert1( commandMap, fileinfo );
		returnAjaxString( response, isSuccess );
		System.out.println( "이벤트 재등록" );
	}

	/**
	 * 이벤트 수정
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1Update1.page" )
	public String fdcrAdA1Update1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String EVENT_ID = EgovWebUtil.getString( commandMap, "EVENT_ID" );
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		commandMap.put( "OPER_YN", EgovWebUtil.getString( commandMap, "OPER_YN" ) );
		commandMap.put( "OPEN_YN_PROP", EgovWebUtil.getString( commandMap, "OPEN_YN_PROP" ) );
		commandMap.put( "OPEN_YN_DTTM_PART", EgovWebUtil.getString( commandMap, "OPEN_YN_DTTM_PART" ) );
		commandMap.put( "OPEN_YN_WIN_ANUC_DATE", EgovWebUtil.getString( commandMap, "OPEN_YN_WIN_ANUC_DATE" ) );
		commandMap.put( "OPEN_YN_WIN_ANUC_DESC", EgovWebUtil.getString( commandMap, "OPEN_YN_WIN_ANUC_DESC" ) );
		commandMap.put( "OPEN_YN_WIN_CNT", EgovWebUtil.getString( commandMap, "OPEN_YN_WIN_CNT" ) );
		commandMap.put( "OPEN_YN_IMAGE", EgovWebUtil.getString( commandMap, "OPEN_YN_IMAGE" ) );
		commandMap.put( "PART_DUPL_YN", EgovWebUtil.getString( commandMap, "PART_DUPL_YN" ) );
		
		
		ArrayList<Map<String,Object>> agreeList = new ArrayList<Map<String,Object>>();
		String AGREEYN_1 = EgovWebUtil.getString( commandMap, "AGREE_ITEM_CD_1" );
		String ITEM_DESC_1 = EgovWebUtil.getString( commandMap, "ITEM_DESC_1" );
		agreeList.add( getAgreeMap("1", AGREEYN_1, ITEM_DESC_1) );

		String AGREEYN_2 = EgovWebUtil.getString( commandMap, "AGREE_ITEM_CD_2" );
		String ITEM_DESC_2 = EgovWebUtil.getString( commandMap, "ITEM_DESC_2" );
		agreeList.add( getAgreeMap("2", AGREEYN_2, ITEM_DESC_2) );
		
		String AGREEYN_99 = EgovWebUtil.getString( commandMap, "AGREE_ITEM_CD_99" );
		String ITEM_DESC_99 = EgovWebUtil.getString( commandMap, "ITEM_DESC_99" );
		agreeList.add( getAgreeMap("99", AGREEYN_99, ITEM_DESC_99) );
		
		commandMap.put( "agreeList", agreeList );

		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAdA1Service.fdcrAdA1Update1( commandMap, fileinfo );
		if( isSuccess ){
			return returnUrl(
				model,
				"수정했습니다.",
				"/console/event/fdcrAdA1View2.page?EVENT_ID="+EVENT_ID);
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/event/fdcrAdA1View2.page?EVENT_ID="+EVENT_ID);
		}
	}
	
	public Map<String,Object> getAgreeMap(String AGREE_ITEM_CD, String AGREEYN, String ITEM_DESC){
		String ITEM_TITLE = "";
		if(null!=AGREE_ITEM_CD){
			if("1".equals(AGREE_ITEM_CD)){
				ITEM_TITLE = "개인정보 수집·이용에 대한 동의";
			}else if("2".equals(AGREE_ITEM_CD)){
				ITEM_TITLE = "개인정보 취급위탁에 대한 동의";
			}else if("99".equals(AGREE_ITEM_CD)){
				ITEM_TITLE = "기타";
			}
		}
		Map<String,Object> agreeMap = new HashMap<String,Object>();
		agreeMap.put( "AGREE_ITEM_CD", AGREE_ITEM_CD );
		agreeMap.put( "AGREEYN", AGREEYN );
		agreeMap.put( "ITEM_TITLE", ITEM_TITLE );
		agreeMap.put( "ITEM_DESC", ITEM_DESC );
		return agreeMap;
	}

	/**
	 * 이벤트 선택삭제
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1Delete1.page" )
	public String fdcrAdA1Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String[] EVENT_IDS = request.getParameterValues( "EVENT_ID" );
		
		commandMap.put( "EVENT_ID", EVENT_IDS );
		boolean isSuccess = fdcrAdA1Service.fdcrAdA1Delete1( commandMap );
		if( isSuccess ){
			return returnUrl(
				model,
				"삭제했습니다.",
				"/console/event/fdcrAdA1List1.page");
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/event/fdcrAdA1List1.page");
		}
	}

	/**
	 * 이벤트 설문형
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1Popup1.page" )
	public String fdcrAdA1Popup1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "user_id", ConsoleLoginUser.getUserId() );
		model.addAttribute( "commandMap", commandMap );
		return "/event/fdcrAdA1Popup1.popup";
	}
	
	@RequestMapping( value = "/console/event/fdcrAdA1Popup2.page" )
	public String fdcrAdA1Popup2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "user_id", ConsoleLoginUser.getUserId() );
		
		fdcrAdA1Service.fdcrAdA1View1( commandMap );

		model.addAttribute( "ds_data", commandMap.get( "ds_data" ) );
		model.addAttribute( "commandMap", commandMap );
		return "/event/fdcrAdA1Popup2.popup";
	}
	

	/**
	 * 댓글형 이벤트 상세페이지 댓글 등록/수정
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1Insert2.page" )
	public void fdcrAdA1Insert2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );

		boolean isSuccess = fdcrAdA1Service.fdcrAdA1Insert2( commandMap );
		returnAjaxString( response, isSuccess );
	}

	/**
	 * 참여 및 응답현황 목록 팝업
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1Popup3.page" )
	public String fdcrAdA1Popup3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA1Service.fdcrAdA1Pop1( commandMap );
		model.addAttribute( "ds_event", commandMap.get( "ds_event" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_list_rslt", commandMap.get( "ds_list_rslt" ) );
		model.addAttribute( "headerList", commandMap.get( "headerList" ) );
		model.addAttribute( "colCnt", commandMap.get( "colCnt" ) );
		return "/event/fdcrAdA1Popup3.popup";
	}

	/**
	 * 참여 및 응답현황 목록 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1ExcelDown1.page" )
	public String fdcrAdA1ExcelDown1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		// 파라미터 셋팅
		commandMap.put( "EVENT_ID", "" );
		commandMap.put( "STATNO", "" );

		fdcrAdA1Service.fdcrAdA1ExcelDown1( commandMap );

		model.addAttribute( "ds_event", commandMap.get( "ds_event" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_list_rslt", commandMap.get( "ds_list_rslt" ) );

		System.out.println( "참여 및 응답현황 목록 엑셀다운로드" );
		return "test";
	}

	/**
	 * 참여 및 응답현황/당첨자 자동선정 당첨자 등록
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1Insert3.page" )
	public String fdcrAdA1Insert3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		String target = EgovWebUtil.getString( commandMap, "target" );
		String EVENT_ID = EgovWebUtil.getString( commandMap, "EVENT_ID" );
		String[] CHK = request.getParameterValues( "CHK" );
		if(null!=CHK && CHK.length > 0){
			int size = CHK.length;
			String[] EVENT_IDS = new String[size];
			String[] USER_IDNTS = new String[size];
			String[] PART_CNTS = new String[size];
			
			for(int i=0;i<size;i++){
				String c = CHK[i];
				String[] arr = c.split( "\\|" );
				EVENT_IDS[i] = arr[0];
				USER_IDNTS[i] = arr[1];
				PART_CNTS[i] = arr[2];
			}
			
			commandMap.put( "EVENT_ID", EVENT_IDS );
			commandMap.put( "USER_IDNT", USER_IDNTS );
			commandMap.put( "PART_CNT", PART_CNTS );
		}else{
			commandMap.put( "EVENT_ID", null );
			commandMap.put( "USER_IDNT", null );
			commandMap.put( "PART_CNT", null );
		}

		boolean isSuccess = fdcrAdA1Service.fdcrAdA1Insert3( commandMap );
		if( isSuccess ){
			return returnUrl(
				model,
				"당첨 처리했습니다.",
				"/console/event/fdcrAdA1Popup3.page?EVENT_ID="+EVENT_ID,
				target);
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/event/fdcrAdA1Popup3.page?EVENT_ID="+EVENT_ID,
				target);
		}
	}

	/**
	 * 참여 및 응답현황 당첨자 취소
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1Insert4.page" )
	public String fdcrAdA1Insert4( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		
		String EVENT_ID = EgovWebUtil.getString( commandMap, "EVENT_ID" );
		String[] CHK = request.getParameterValues( "CHK" );
		if(null!=CHK && CHK.length > 0){
			int size = CHK.length;
			String[] EVENT_IDS = new String[size];
			String[] USER_IDNTS = new String[size];
			String[] PART_CNTS = new String[size];
			
			for(int i=0;i<size;i++){
				String c = CHK[i];
				String[] arr = c.split( "\\|" );
				EVENT_IDS[i] = arr[0];
				USER_IDNTS[i] = arr[1];
				PART_CNTS[i] = arr[2];
			}
			
			commandMap.put( "EVENT_ID", EVENT_IDS );
			commandMap.put( "USER_IDNT", USER_IDNTS );
			commandMap.put( "PART_CNT", PART_CNTS );
		}else{
			commandMap.put( "EVENT_ID", null );
			commandMap.put( "USER_IDNT", null );
			commandMap.put( "PART_CNT", null );
		}
		boolean isSuccess = fdcrAdA1Service.fdcrAdA1Insert4( commandMap );
		if( isSuccess ){
			return returnUrl(
				model,
				"취소 처리했습니다.",
				"/console/event/fdcrAdA1Popup3.page?EVENT_ID="+EVENT_ID,
				"self");
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/event/fdcrAdA1Popup3.page?EVENT_ID="+EVENT_ID,
				"self");
		}
	}

	/**
	 * 참여 및 응답현황 당첨자 자동선정 목록 팝업
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1Popup4.page" )
	public String fdcrAdA1Popup4( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA1Service.fdcrAdA1Pop2( commandMap );

		model.addAttribute( "ds_event", commandMap.get( "ds_event" ) );
		return "/event/fdcrAdA1Popup4.popup";
	}

	/**
	 * 당첨자 자동선정 선정하기
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/event/fdcrAdA1Popup4Sub1.page" )
	public String fdcrAdA1Popup4Sub1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA1Service.fdcrAdA1List2( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		return "/event/fdcrAdA1Popup4Sub1";
	}

}
