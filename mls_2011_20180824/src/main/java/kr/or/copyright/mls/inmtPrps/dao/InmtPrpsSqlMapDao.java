package kr.or.copyright.mls.inmtPrps.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.inmtPrps.model.InmtPrps;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class InmtPrpsSqlMapDao extends SqlMapClientDaoSupport implements InmtPrpsDao {

	//private Log logger = LogFactory.getLog(getClass());
	
	public String maxYear(InmtPrps params) {
		return getSqlMapClientTemplate().queryForObject("InmtPrps.maxYear", params).toString();
	}
	
	public int inmtCount(InmtPrps params) {
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("InmtPrps.inmtCount", params));
	}
	
	public List inmtList(InmtPrps params) {
		return getSqlMapClientTemplate().queryForList("InmtPrps.getInmtList", params);		
	}
	
	public List chkInmtList(InmtPrps params) {
		return getSqlMapClientTemplate().queryForList("InmtPrps.getChkInmtList", params);		
	}
	
	public int prpsMastKey() {
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("InmtPrps.prpsMastKey"));
	}
	
	public int prpsSeqnMax() {
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("InmtPrps.prpsSeqnMax"));
	}
	
	public void inmtPspsInsert(Map params) {
		getSqlMapClientTemplate().insert("InmtPrps.inmtPspsInsert", params);
	}
	
	public void inmtKappInsert(Map params) {
		getSqlMapClientTemplate().insert("InmtPrps.inmtKappInsert", params);
	}
	
	public void inmtFokapoInsert(Map params) {
		getSqlMapClientTemplate().insert("InmtPrps.inmtFokapoInsert", params);
	}
	
	public void inmtKrtraInsert(Map params) {
		getSqlMapClientTemplate().insert("InmtPrps.inmtKrtraInsert", params);
	}
	
	public void inmtPrpsRsltInsert(Map params) {
		getSqlMapClientTemplate().insert("InmtPrps.inmtPrpsRsltInsert", params);
	}
	
	public void insertConnInfo(Map params) {
		getSqlMapClientTemplate().insert("InmtPrps.insertConnInfo", params);
	}
	
	public String getMaxDealStat(RghtPrps params) {
		return getSqlMapClientTemplate().queryForObject("InmtPrps.getMaxDealStat", params).toString();
	}
	
	public void inmtKappDelete(RghtPrps params) {
		getSqlMapClientTemplate().delete("InmtPrps.inmtKappDelete", params);
	}
	
	public void inmtFokapoDelete(RghtPrps params) {
		getSqlMapClientTemplate().delete("InmtPrps.inmtFokapoDelete", params);
	}
	
	public void inmtKrtraDelete(RghtPrps params) {
		getSqlMapClientTemplate().delete("InmtPrps.inmtKrtraDelete", params);
	}

	public void inmtPrpsRsltDelete(RghtPrps params) {
		getSqlMapClientTemplate().delete("InmtPrps.inmtPrpsRsltDelete", params);
	}

	public void inmtPrpsDelete(RghtPrps params) {
		getSqlMapClientTemplate().delete("InmtPrps.inmtPrpsDelete", params);
	}
	
	public void inmtFileDeleteOne(InmtPrps params){
		getSqlMapClientTemplate().delete("InmtPrps.inmtFileDeleteOne", params);
	}
	
	public List inmtPrpsHistList(RghtPrps params) {
		return getSqlMapClientTemplate().queryForList("InmtPrps.inmtPrpsHistList", params);
	}
	
	// 추가등록 저작물 get BrctInmtSeqn max+1
	public int getBrctInmtSeqn() {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("InmtPrps.getBrctInmtSeqn",null);
		return max.intValue(); 
	}
	
	// 등록 방송음악 insert
	public void brctImtInsert(RghtPrps params){
		getSqlMapClientTemplate().insert("InmtPrps.brctImtInsert", params);
	}
	
	// 추가등록 저작물 get LibrInmtSeqn max+1
	public int getLibrInmtSeqn() {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("InmtPrps.getLibrInmtSeqn",null);
		return max.intValue(); 
	}
	
	// 등록 도서관 insert
	public void librImtInsert(RghtPrps params){
		getSqlMapClientTemplate().insert("InmtPrps.librImtInsert", params);
	}
	
	// 추가등록 저작물 get SubjInmtSeqn max+1
	public int getSubjInmtSeqn() {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("InmtPrps.getSubjInmtSeqn",null);
		return max.intValue(); 
	}
	
	// 등록 도서관 insert
	public void subjImtInsert(RghtPrps params){
		getSqlMapClientTemplate().insert("InmtPrps.subjImtInsert", params);
	}

	//신규 미분배보상금 방송음악 CNT조회 20121002 정병호
	public int muscInmtCount(InmtPrps params){
	    return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("InmtPrps.muscInmtCount", params));
	}
	
	//신규 미분배보상금 방송음악 최대금액 조회
	public int muscMaxAmnt(){
	    return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("InmtPrps.muscMaxAmnt"));
	}
	
	//신규 미분배보상금 교과용 CNT조회 20121002 정병호
	public int subjInmtCount(InmtPrps params){
	    return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("InmtPrps.subjInmtCount", params));
	}	
	
	//신규 미분배보상금 도서관 CNT조회 20121002 정병호
	public int librInmtCount(InmtPrps params){
	    return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("InmtPrps.librInmtCount", params));
	}
	
	//신규 미분배보상금 수업목적 CNT조회 20121002 정병호
	public int lssnInmtCount(InmtPrps params){
	    return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("InmtPrps.lssnInmtCount", params));
	}
	
	//신규 미분배보상금 방송음악 조회 20121002 정병호
	public List muscInmtList(InmtPrps params){
	    return getSqlMapClientTemplate().queryForList("InmtPrps.muscInmtList", params);		
	}
	
	//신규 미분배보상금 교과용 조회 20121002 정병호
	public List subjInmtList(InmtPrps params){
	    return getSqlMapClientTemplate().queryForList("InmtPrps.subjInmtList", params);		
	}
	
	//신규 미분배보상금 도서관 조회 20121002 정병호
	public List librInmtList(InmtPrps params){
	    return getSqlMapClientTemplate().queryForList("InmtPrps.librInmtList", params);		
	}
	
	//신규 미분배보상금 수업목적 조회 20121002 정병호
	public List lssnInmtList(InmtPrps params){
	    return getSqlMapClientTemplate().queryForList("InmtPrps.lssnInmtList", params);		
	}
	
	//신규 미분배보상금 상위 리스트 조회 
	public List inmtPrpsTopList(Map params){
	    return getSqlMapClientTemplate().queryForList("InmtPrps.inmtPrpsTopList", params);		
	}

	public List inmtPrpsAmntList( Map params ){
		return getSqlMapClientTemplate().queryForList("InmtPrps.inmtPrpsAmntList", params);
	}

}