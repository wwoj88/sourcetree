package kr.or.copyright.mls.console.stats;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.stats.inter.FdcrAd96Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 통계관리 > 법정허락 신청 통계(기존)
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd96Controller extends DefaultController{

	@Resource( name = "fdcrAd96Service" )
	private FdcrAd96Service fdcrAd96Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd96Controller.class );

	/**
	 * 연도별 신청건수, 승인건수
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd96List1.page" )
	public String fdcrAd96List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd96List1 Start" );
		// 파라미터 셋팅
		commandMap.put( "YEAR_MONTH", "" );

		fdcrAd96Service.fdcrAd96List1( commandMap );
		model.addAttribute( "ds_list1", commandMap.get( "ds_list1" ) );
		logger.debug( "fdcrAd96List1 End" );
		return "test";
	}
	
	/**
	 * 연도별 저작물, 실연, 음반, 방송, 데이터베이스별 건수
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd96List2.page" )
	public String fdcrAd96List2( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd96List2 Start" );
		// 파라미터 셋팅
		commandMap.put( "YEAR_MONTH", "" );

		fdcrAd96Service.fdcrAd96List2( commandMap );
		model.addAttribute( "ds_list2", commandMap.get( "ds_list2" ) );
		logger.debug( "fdcrAd96List2 End" );
		return "test";
	}
	
	/**
	 * 연도별 보상금액별
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd96List3.page" )
	public String fdcrAd96List3( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd96List3 Start" );
		// 파라미터 셋팅
		commandMap.put( "YEAR_MONTH", "" );

		fdcrAd96Service.fdcrAd96List3( commandMap );
		model.addAttribute( "ds_list3", commandMap.get( "ds_list3" ) );
		logger.debug( "fdcrAd96List3 End" );
		return "test";
	}
}
