package kr.or.copyright.mls.console.email.inter;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface FdcrAdA10Dao{
	
	/**
	 * 그룹별 메일폼목록
	 * 
	 * @return
	 */
	public List selectFormGroupList(Map map);
	
	// 메뉴 조회
	public Map<String, Object> selectFormInfo(Map map);

	/**
	  * <PRE>
	  * 간략 : 
	  * 상세 : 
	  * <PRE>
	  * @param commandMap 
	  */
	public void formUpdate( Map map );

	/**
	  * <PRE>
	  * 간략 : 
	  * 상세 : 
	  * <PRE>
	  * @param commandMap 
	  */
	public void formInsert( Map map );

	/**
	  * <PRE>
	  * 간략 : 
	  * 상세 : 
	  * <PRE>
	  * @param commandMap 
	  */
	public void formDelete( Map map );
	
	
}
