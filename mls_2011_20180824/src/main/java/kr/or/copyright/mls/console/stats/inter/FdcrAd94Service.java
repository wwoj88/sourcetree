package kr.or.copyright.mls.console.stats.inter;

import java.util.Map;

public interface FdcrAd94Service{

	/**
	 * 저작권찾기신청 및 처리통계(기간별)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd94List1( Map<String, Object> commandMap ) throws Exception;
}
