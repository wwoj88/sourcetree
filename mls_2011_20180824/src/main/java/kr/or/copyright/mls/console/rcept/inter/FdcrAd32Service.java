package kr.or.copyright.mls.console.rcept.inter;

import java.util.Map;

public interface FdcrAd32Service{

	/**
	 * 상당한노력 진행현황 조회
	 * 
	 * @throws Exception
	 */
	public void fdcrAd32List1( Map<String, Object> commandMap ) throws Exception;
}
