package kr.or.copyright.mls.console.stats;

import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.stats.inter.FdcrAd95Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 통계관리 > 법정허락 신청 통계
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd95Controller extends DefaultController {

     @Resource(name = "fdcrAd95Service")
     private FdcrAd95Service fdcrAd95Service;

     private static final Logger logger = LoggerFactory.getLogger(FdcrAd95Controller.class);

     /**
      * 조회
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95List1.page")
     public String fdcrAd95List1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95List1 Start");
          // 파라미터 셋팅
          commandMap.put("YYYYMMDD", EgovWebUtil.getDate("yyyyMMdd"));
          fdcrAd95Service.fdcrAd95List1(commandMap);
          model.addAttribute("commandMap", commandMap);
          model.addAttribute("ds_list1", commandMap.get("ds_list1"));
          model.addAttribute("ds_list2", commandMap.get("ds_list2"));
          model.addAttribute("ds_list3", commandMap.get("ds_list3"));
          model.addAttribute("ds_list4", commandMap.get("ds_list4"));
          model.addAttribute("ds_list5", commandMap.get("ds_list5"));
          logger.debug("fdcrAd95List1 End");
          return "/stats/fdcrAd95List1.tiles";
     }

     /**
      * 법정허락 이용승인 건수 통계
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95List2.page")
     public String fdcrAd95List2(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95List2 Start");
          // 파라미터 셋팅
          commandMap.put("YEAR_MONTH", "");

          fdcrAd95Service.fdcrAd95List2(commandMap);
          model.addAttribute("ds_list1", commandMap.get("ds_list1"));
          logger.debug("fdcrAd95List2 End");
          return "test";
     }

     /**
      * 법정허락 대상 저작물 현황
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95List3.page")
     public String fdcrAd95List3(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95List3 Start");
          // 파라미터 셋팅
          fdcrAd95Service.fdcrAd95List3(commandMap);
          model.addAttribute("ds_list2", commandMap.get("ds_list2"));
          logger.debug("fdcrAd95List3 End");
          return "/stats/fdcrAd95List3";
     }

     /**
      * 보상금 공탁서 공고 건수 통계
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95List4.page")
     public String fdcrAd95List4(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95List4 Start");
          // 파라미터 셋팅
          commandMap.put("YEAR_MONTH", "");

          fdcrAd95Service.fdcrAd95List4(commandMap);
          model.addAttribute("ds_list3", commandMap.get("ds_list3"));
          logger.debug("fdcrAd95List4 End");
          return "test";
     }

     /**
      * 상당한노력 신청 건수 통계
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95List5.page")
     public String fdcrAd95List5(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95List5 Start");
          // 파라미터 셋팅
          commandMap.put("YEAR_MONTH", "");

          fdcrAd95Service.fdcrAd95List5(commandMap);
          model.addAttribute("ds_list4", commandMap.get("ds_list4"));
          logger.debug("fdcrAd95List5 End");
          return "test";
     }

     /**
      * 상당한노력 신청 유형별 통계
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95List6.page")
     public String fdcrAd95List6(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95List6 Start");
          // 파라미터 셋팅
          commandMap.put("YEAR_MONTH", "");

          fdcrAd95Service.fdcrAd95List6(commandMap);
          model.addAttribute("ds_list5", commandMap.get("ds_list5"));
          logger.debug("fdcrAd95List6 End");
          return "test";
     }

     /**
      * 법정허락 이용승인 명세서 목록(팝업)
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95List7.page")
     public String fdcrAd95List7(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95List7 Start");
          // 파라미터 셋팅
          fdcrAd95Service.fdcrAd95List7(commandMap);
          model.addAttribute("ds_list", commandMap.get("ds_list"));
          logger.debug("fdcrAd95List7 End");
          return "/stats/fdcrAd95List7.popup";
     }


     /**
      * 법정허락 이용승인 건수 통계 엑셀다운로드
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95Down1.page")
     public String fdcrAd95Down1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95Down1 Start");
          // 파라미터 셋팅
          commandMap.put("YEAR_MONTH", "");

          fdcrAd95Service.fdcrAd95List2(commandMap);
          model.addAttribute("ds_list1", commandMap.get("ds_list1"));
         // System.out.println(commandMap.get("ds_list1").toString());
          logger.debug("fdcrAd95Down1 End");
          return "/stats/fdcrAd95Down1";
     }

     /**
      * 법정허락 대상 저작물 현황 엑셀다운로드
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95Down2.page")
     public String fdcrAd95Down2(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95Down2 Start");
          // 파라미터 셋팅
          commandMap.put("YEAR_MONTH", "");

          fdcrAd95Service.fdcrAd95List3(commandMap);
          
          model.addAttribute("ds_list2", commandMap.get("ds_list2"));
    
          logger.debug("fdcrAd95Down2 End");
          return "/stats/fdcrAd95Down2";

     }

     /**
      * 보상금 공탁서 공고 건수 통계 엑셀다운로드
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95Down3.page")
     public String fdcrAd95Down3(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95Down3 Start");
          // 파라미터 셋팅
          commandMap.put("YEAR_MONTH", "");

          fdcrAd95Service.fdcrAd95List4(commandMap);
          model.addAttribute("ds_list3", commandMap.get("ds_list3"));
          logger.debug("fdcrAd95Down3 End");
          return "/stats/fdcrAd95Down3";

     }

     /**
      * 상당한노력 신청 건수 통계 엑셀다운로드
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95Down4.page")
     public String fdcrAd95Down4(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95Down4 Start");
          // 파라미터 셋팅
          commandMap.put("YEAR_MONTH", "");

          fdcrAd95Service.fdcrAd95List5(commandMap);
          model.addAttribute("ds_list4", commandMap.get("ds_list4"));
          logger.debug("fdcrAd95Down4 End");
          return "test";
     }

     /**
      * 상당한노력 신청 건수 통계 엑셀다운로드
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/stats/fdcrAd95Down5.page")
     public String fdcrAd95Down5(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd95Down5 Start");
          // 파라미터 셋팅
          commandMap.put("YEAR_MONTH", "");

          fdcrAd95Service.fdcrAd95List6(commandMap);
          model.addAttribute("ds_list5", commandMap.get("ds_list5"));
          logger.debug("fdcrAd95Down5 End");
          return "test";
     }

}
