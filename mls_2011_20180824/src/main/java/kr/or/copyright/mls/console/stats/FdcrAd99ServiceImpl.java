package kr.or.copyright.mls.console.stats;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd15Dao;
import kr.or.copyright.mls.console.stats.inter.FdcrAd99Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service( "fdcrAd99Service" )
public class FdcrAd99ServiceImpl extends CommandService implements FdcrAd99Service{

	// old AdminWorksMgntDao
	@Autowired
	private FdcrAd15Dao fdcrAd15Dao;

	/**
	 * 등록부 연계 등록현황 - 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd99List1( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd15Dao.crosWorksMgntReptView( commandMap );

		int TOTAL = 0;
		for(int i=0;i<list.size();i++){
			Map<String,Object> map = (Map<String,Object>)list.get( i );
			int WORKS_CONT = EgovWebUtil.getToInt( map, "WORKS_CONT" );
			TOTAL += WORKS_CONT;
		}
		commandMap.put( "TOTAL", TOTAL );
		commandMap.put( "ds_list", list );
	}
}
