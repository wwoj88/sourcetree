package kr.or.copyright.mls.console.mber;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import kr.or.copyright.mls.adminCommon.dao.AdminCommonDao;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.dreamsecurity.jcaos.asn1.re;
import com.dreamsecurity.jcaos.asn1.x509.h;

/**
 * 회원관리 > 회원정보 목록
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd83Controller extends DefaultController {

     @Resource(name = "fdcrAd83Service")
     private FdcrAd83Service fdcrAd83Service;

     @Resource(name = "adminCommonDao")
     private AdminCommonDao adminCommonDao;
     private static final Logger logger = LoggerFactory.getLogger(FdcrAd83Controller.class);

     /**
      * 회원정보 목록
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd83List1.page")
     public String fdcrAd83List1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd83List1 Start");
          // 파라미터 셋팅
          String USER_NAME = EgovWebUtil.getString(commandMap, "USER_NAME");
          if (!"".equals(USER_NAME)) {
               USER_NAME = URLDecoder.decode(USER_NAME, "UTF-8");
               commandMap.put("USER_NAME", USER_NAME);
          }
          HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
          String ip = req.getHeader("X-FORWARDED-FOR");
          if (ip == null) {
               ip = req.getRemoteAddr();
          }
          Map logMap = new HashMap();
          logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
          logMap.put("PROC_STATUS", "회원 목록" );
          logMap.put("PROC_ID",  "회원목록/전체");
          logMap.put("MENU_URL", request.getRequestURI());
          logMap.put("IP_ADDRESS", ip);

          adminCommonDao.insertAdminLogDo(logMap);
          
          
          
          
          fdcrAd83Service.fdcrAd83List1(commandMap);
          model.addAttribute("ds_list", commandMap.get("ds_list"));
          model.addAttribute("commandMap", commandMap);
          model.addAttribute("paginationInfo", commandMap.get("paginationInfo"));
          logger.debug("fdcrAd83List1 End");
          return "/mber/fdcrAd83List1.tiles";
     }

     /**
      * 회원정보 상세
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @return
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd83View1.page")
     public String fdcrAd83View1(ModelMap model,

               Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd83View1 Start");

          // 파라미터 셋팅
          fdcrAd83Service.fdcrAd83View1(commandMap);
          model.addAttribute("info", commandMap.get("ds_list"));
          logger.debug("fdcrAd83View1 End");

          String USER_IDNT = request.getParameter("USER_IDNT");
          HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
          String ip = req.getHeader("X-FORWARDED-FOR");
          if (ip == null) {
               ip = req.getRemoteAddr();
          }
          Map logMap = new HashMap();
          System.out.println(request.getRequestURI());

          logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
          logMap.put("PROC_STATUS", "회원 목록" );
          logMap.put("PROC_ID",  USER_IDNT + "/ 정보검색");
          logMap.put("MENU_URL", request.getRequestURI());
          logMap.put("IP_ADDRESS", ip);

          adminCommonDao.insertAdminLogDo(logMap);
          
          return "/mber/fdcrAd83View1.tiles";
     }

     /**
      * 비밀번호 변경
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd83Update1.page")
     public String fdcrAd83Update1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          String USER_IDNT = request.getParameter("USER_IDNT");
          logger.debug("fdcrAd83Update1 Start");
          boolean isSuccess = fdcrAd83Service.fdcrAd83Update1(commandMap);
          logger.debug("fdcrAd83Update1 End");
          
          if (isSuccess) {

               HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
               String ip = req.getHeader("X-FORWARDED-FOR");
               if (ip == null) {
                    ip = req.getRemoteAddr();
               }
               Map logMap = new HashMap();


               logMap.put("MANAGER_ID", ConsoleLoginUser.getUserId());
               logMap.put("PROC_STATUS", "회원 목록" );
               logMap.put("PROC_ID",  USER_IDNT + "/ 정보수정");
               logMap.put("MENU_URL", request.getRequestURI());
               logMap.put("IP_ADDRESS", ip);

               adminCommonDao.insertAdminLogDo(logMap);


               return returnUrl(model, "변경했습니다.", "/console/mber/fdcrAd83List1.page");

          } else {
               return returnUrl(model, "실패했습니다.", "/console/mber/fdcrAd83List1.page");
          }

     }

     @RequestMapping(value = "/console/mber/fncUpdateFailCnt.page")
     public String fncUpdateFailCnt(ModelMap model,

               Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fncUpdateFailCnt Start");
          // boolean isSuccess = fdcrAd83Service.fdcrAd83Update1( commandMap );
          fdcrAd83Service.fncUpdateFailCnt(commandMap);
          logger.debug("fncUpdateFailCnt End");
          /*
           * if( isSuccess ){ return returnUrl( model, "변경했습니다.",
           * "/console/mber/fdcrAd83View1.page?USER_IDNT=" + EgovWebUtil.getString( commandMap, "USER_IDNT"
           * )); }else{ return returnUrl( model, "실패했습니다.", "/console/mber/fdcrAd83View1.page?USER_IDNT=" +
           * EgovWebUtil.getString( commandMap, "USER_IDNT" )); }
           */
          return "forward:/console/mber/fdcrAd83View1.page";
     }
}
