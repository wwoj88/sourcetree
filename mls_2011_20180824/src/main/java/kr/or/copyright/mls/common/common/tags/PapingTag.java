package kr.or.copyright.mls.common.common.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PapingTag extends TagSupport {

	final static private Log logger = LogFactory.getLog(PapingTag.class);

	private int totalRow; // 전체 게시물수
	private int pageNo; // 현재 페이지
	private int rowPerPage; // 한 화면에 보여줄 게시물수
	private int pageSize; // 페이지 리스트 사이즈
	private String link;
	private String function; 

	public int getTotalRow() {
		return totalRow;
	}

	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getRowPerPage() {
		return rowPerPage;
	}

	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public int doStartTag() throws JspException {

		try {

			StringBuilder sb = new StringBuilder();

			int totalPage = ((totalRow - 1) / rowPerPage) + 1;

			int temp = ((pageNo - 1) / pageSize) * pageSize + 1;
			/*
			if (temp > 1) {
				sb.append("<a href='" + link + "&pageNo" + (temp - pageSize)
						+ "'>" + "[이전" + pageSize + "개" + "</a>&nbsp;&nbsp;");
			}
			*/
			if (temp > 1) {
				sb.append("<a href=\"javascript:")
				  .append(function)
				  .append("('")
				  .append(temp - pageSize)
				  .append("')\">")
				  .append("[ 이전  ]")
				  .append("</a>");
			}

			int i = 1;
			while (i < pageSize && temp <= totalPage) {
				if (temp == pageNo) {
					sb.append(" <b>" + temp + "</b> ");
				} else {
					sb.append(" <a href=\"javascript:")
					  .append(function)
					  .append("('")
					  .append(temp)
					  .append("')\">")
					  .append(temp)
					  .append("</a> ");
					
//					sb.append("<a href=list.jsp?pageNo=" + temp + ">" + temp
//							+ "</a>&nbsp;&nbsp;");
				}
				i++;
				temp++;
			}

			if(temp > totalPage){
				sb.append("<a href=\"javascript:")
				  .append(function)
				  .append("('")
				  .append(temp)
				  .append("')\">")
				  .append("[ 다음  ]")
				  .append("</a>");
			}
			
			JspWriter out = pageContext.getOut();

			out.print(sb.toString());
		} catch (IOException e) {
			logger.error(e.toString());
			throw new JspException(e.getMessage());
		}

		return SKIP_BODY;
	}
}
