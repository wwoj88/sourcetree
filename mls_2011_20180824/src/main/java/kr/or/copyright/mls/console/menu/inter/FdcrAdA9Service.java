package kr.or.copyright.mls.console.menu.inter;

import java.util.HashMap;
import java.util.Map;

public interface FdcrAdA9Service{

	/**
	 * 메뉴 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA9List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 메뉴 등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdA9Write1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 메뉴 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdA9Update1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 메뉴 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdA9Delete1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 메뉴 저장처리
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void menuUpdate( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 그룹 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void selectGroupList( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 그룹 메뉴 조회하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void groupMenuInfo( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 그룹 등록하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean groupInsert( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 그룹 메뉴 삭제하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean groupDelete( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 그룹 메뉴 수정하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean groupMenuUpdate( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 메뉴 조회
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectMenuInfo( Map<String, Object> commandMap ) throws Exception;
}
