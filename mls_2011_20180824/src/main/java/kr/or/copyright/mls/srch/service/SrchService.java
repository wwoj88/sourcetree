package kr.or.copyright.mls.srch.service;

import java.util.List;
import java.util.Map;



public interface SrchService {
	public List codeList(String srchDivs);
	//법정허락
	public List findStatWorks(Map params);
	public int countStatWorks(Map params);
	public List findStatDetl(Map params);
	public List getCoptHodr(Map params);
	//위탁관리저작물
	public List findCommWorks(Map params);
	public List findCommDetl(Map params);
	public int countCommWorks(Map params);
	//법정허락등록부
	public List findCrosWorks(Map params);
	public int countCrosWorks(Map params);
	
	public List findCertWorks(Map params);
	public int countFindCertWorks(Map params);
	public void insertSrch(String worksTitle) throws Exception;
	public List getSearchWordList() throws Exception;
}	

