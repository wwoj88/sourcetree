package kr.or.copyright.mls.image.dao;


import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import kr.or.copyright.mls.image.model.ImageDTO;

public class ImageDaoImpl extends SqlMapClientDaoSupport implements ImageDao{

	public ImageDTO getImageByWorkId( int worksid ){
		return (ImageDTO) getSqlMapClientTemplate().queryForObject("Image.getImageByWorkId",worksid);
	}

	public ImageDTO getPhotoByWorkId( int worksid ){
		return (ImageDTO) getSqlMapClientTemplate().queryForObject("Image.getPhotoByWorkId",worksid);
	}
}
