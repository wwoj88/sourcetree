package kr.or.copyright.mls.adminEvent.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class AdminEventSqlMapDao extends SqlMapClientDaoSupport implements AdminEventDao{
	
	// 설문참가 정보 List
	public List campPartList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminEvent.campPartList", map);
	}
	
	// 이벤트 List
	public List eventMgntList(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminEvent.eventMgntList", map);
	}
	
	// 이벤트 Detail
	public List eventMgntDetail(Map map) {
		return getSqlMapClientTemplate().queryForList("AdminEvent.eventMgntDetail", map);
	}
	
	// 이벤트 Delete
	public void eventMgntDelete(Map map) {
		getSqlMapClientTemplate().delete("AdminEvent.eventMgntDelete", map);
	}
	
	// 이벤트 insert
	public void eventMgntInsert(Map map) {
		getSqlMapClientTemplate().insert("AdminEvent.eventMgntInsert", map);
	}
	
	// 이벤트 update
	public void eventMgntUpdate(Map map) {
		getSqlMapClientTemplate().insert("AdminEvent.eventMgntUpdate", map);
	}
}
