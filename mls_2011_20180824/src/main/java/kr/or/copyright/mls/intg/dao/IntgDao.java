package kr.or.copyright.mls.intg.dao;

import java.util.List;

import kr.or.copyright.mls.intg.model.Intg;

public interface IntgDao {
	
	public String srchImgCnt(Intg IntgDTO);
	
	public List srchImg(Intg IntgDTO);
	
	public List detailImg(Intg IntgDTO);
	
	
	public String srchBrctCnt(Intg IntgDTO);
	
	public List srchBrct(Intg IntgDTO);
	
	public List detailBrct(Intg IntgDTO);
	
	
	public String srchSubjCnt(Intg IntgDTO);
	
	public List srchSubj(Intg IntgDTO);
	
	public List detailSubj(Intg IntgDTO);
	
	
	public String srchLibrCnt(Intg IntgDTO);
	
	public List srchLibr(Intg IntgDTO);
	
	public List detailLibr(Intg IntgDTO);
	
}
 