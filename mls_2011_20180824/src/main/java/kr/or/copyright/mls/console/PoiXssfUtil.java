package kr.or.copyright.mls.console;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PoiXssfUtil {

     private static final Logger LOGGER = LoggerFactory.getLogger(PoiXssfUtil.class);

     /**
      * xlsx 파일 전체 읽기
      * 
      * @param filename 파일경로
      */
     public static void read(String filename) {

          FileInputStream fin = null;
          XSSFWorkbook workbook = null;
          try {
               fin = new FileInputStream(filename);
               workbook = new XSSFWorkbook(fin);
               int sheetNum = workbook.getNumberOfSheets();
               for (int k = 0; k < sheetNum; k++) {
                    LOGGER.debug("Sheet Number : " + k);
                    LOGGER.debug("Sheet Name : " + workbook.getSheetName(k));
                    XSSFSheet sheet = workbook.getSheetAt(k);

                    int rows = sheet.getPhysicalNumberOfRows();
                    for (int r = 0; r < rows; r++) {
                         XSSFRow row = sheet.getRow(r);
                         LOGGER.debug("Row : " + row.getRowNum());

                         int cells = row.getPhysicalNumberOfCells();
                         for (int c = 0; c < cells; c++) {
                              getColumnValue(row, c, ExcelUtil.RETURN_TYPE_STRING);
                         }
                    }
               }
          } catch (FileNotFoundException e) {
               e.printStackTrace();
          } catch (IOException e) {
               e.printStackTrace();
          } finally {
               try {
                    if (workbook != null) {
                         workbook = null;
                    }

                    if (fin != null) {
                         fin.close();
                    }
               } catch (IOException e) {
                    e.printStackTrace();
               }
          }
     }

     /**
      * xlsx 파일 특정 sheet읽기
      * 
      * @param filename 파일경로
      * @param sheetIndex sheet인덱스
      */
     public static void read(String filename, int sheetIndex) {

          FileInputStream fin = null;
          XSSFWorkbook workbook = null;
          try {
               fin = new FileInputStream(filename);
               workbook = new XSSFWorkbook(fin);
               int sheetNum = workbook.getNumberOfSheets();
               if (sheetIndex < sheetNum) {
                    LOGGER.debug("Sheet Number : " + sheetIndex);
                    LOGGER.debug("Sheet Name : " + workbook.getSheetName(sheetIndex));
                    XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
                    int rows = sheet.getPhysicalNumberOfRows();
                    for (int r = 0; r < rows; r++) {
                         XSSFRow row = sheet.getRow(r);
                         LOGGER.debug("Row : " + row.getRowNum());

                         int cells = row.getPhysicalNumberOfCells();
                         for (int c = 0; c < cells; c++) {
                              getColumnValue(row, c, ExcelUtil.RETURN_TYPE_STRING);
                         }
                    }
               }
          } catch (FileNotFoundException e) {
               e.printStackTrace();
          } catch (IOException e) {
               e.printStackTrace();
          } finally {
               try {
                    if (workbook != null) {
                         workbook = null;
                    }

                    if (fin != null) {
                         fin.close();
                    }
               } catch (IOException e) {
                    e.printStackTrace();
               }
          }
     }

     /**
      * xlsx 파일 특정 sheet, 특정 row 읽기
      * 
      * @param filename 파일경로
      * @param sheetIndex sheet인덱스
      * @param rowIndex row인덱스
      */
     public static void read(String filename, int sheetIndex, int rowIndex) {

          FileInputStream fin = null;
          XSSFWorkbook workbook = null;
          try {
               fin = new FileInputStream(filename);
               workbook = new XSSFWorkbook(fin);
               LOGGER.debug("Sheet Number : " + sheetIndex);
               LOGGER.debug("Sheet Name : " + workbook.getSheetName(sheetIndex));
               XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
               if (sheet != null) {
                    int rows = sheet.getPhysicalNumberOfRows();
                    if (rowIndex < rows) {
                         XSSFRow row = sheet.getRow(rowIndex);
                         LOGGER.debug("Row : " + rowIndex);

                         int cells = row.getPhysicalNumberOfCells();
                         for (int c = 0; c < cells; c++) {
                              LOGGER.debug("Col : " + c);
                              getColumnValue(row, c, ExcelUtil.RETURN_TYPE_STRING);
                         }
                    }
               }
          } catch (FileNotFoundException e) {
               e.printStackTrace();
          } catch (IOException e) {
               e.printStackTrace();
          } finally {
               try {
                    if (workbook != null) {
                         workbook = null;
                    }

                    if (fin != null) {
                         fin.close();
                    }
               } catch (IOException e) {
                    e.printStackTrace();
               }
          }
     }

     /**
      * xlsx 파일 특정 sheet, 특정 row , 특정 컬럼 읽기
      * 
      * @param filename 파일경로
      * @param sheetIndex sheet인덱스
      * @param rowIndex row인덱스
      * @param colIndex col인덱스
      */
     public static void read(String filename, int sheetIndex, int rowIndex, int colIndex) {

          FileInputStream fin = null;
          XSSFWorkbook workbook = null;
          try {
               fin = new FileInputStream(filename);
               workbook = new XSSFWorkbook(fin);
               LOGGER.debug("Sheet Number : " + sheetIndex);
               LOGGER.debug("Sheet Name : " + workbook.getSheetName(sheetIndex));
               XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
               if (sheet != null) {
                    int rows = sheet.getPhysicalNumberOfRows();
                    if (rowIndex < rows) {
                         XSSFRow row = sheet.getRow(rowIndex);
                         LOGGER.debug("Row : " + rowIndex);

                         int cells = row.getPhysicalNumberOfCells();
                         if (colIndex < cells) {
                              LOGGER.debug("Col : " + colIndex);
                              getColumnValue(row, colIndex, ExcelUtil.RETURN_TYPE_STRING);
                         }
                    }
               }
          } catch (FileNotFoundException e) {
               e.printStackTrace();
          } catch (IOException e) {
               e.printStackTrace();
          } finally {
               try {
                    if (workbook != null) {
                         workbook = null;
                    }

                    if (fin != null) {
                         fin.close();
                    }
               } catch (IOException e) {
                    e.printStackTrace();
               }
          }
     }

     /**
      * xlsx 칼럼값 데이터형태에 맞추어 가져오기
      * 
      * @param row
      * @param colIndex
      * @return
      */
     public static Object getColumnValue(XSSFRow row, int colIndex, int returnType) {

          Object obj = null;
          String value = null;
          XSSFCell cell = row.getCell(colIndex);
          if (cell != null) {
               switch (cell.getCellType()) {
                    case XSSFCell.CELL_TYPE_FORMULA:
                         value = cell.getCellFormula();
                         obj = cell.getCellFormula();
                         break;
                    case XSSFCell.CELL_TYPE_NUMERIC:

                         value = String.valueOf((int) cell.getNumericCellValue());
                         obj = String.valueOf((int) cell.getNumericCellValue());

                         break;
                    case XSSFCell.CELL_TYPE_STRING:
                         value = cell.getStringCellValue(); // String
                         obj = cell.getStringCellValue();
                         break;
                    case XSSFCell.CELL_TYPE_BLANK:
                         value = "";
                         obj = "";
                         break;
                    case XSSFCell.CELL_TYPE_BOOLEAN:
                         value = Boolean.toString(cell.getBooleanCellValue()); // boolean
                         obj = cell.getBooleanCellValue();
                         break;
                    case XSSFCell.CELL_TYPE_ERROR:
                         value = "ERROR";// cell.getErrorCellValue(); // byte
                         obj = "ERROR";
                         break;
                    default:
               }
               LOGGER.debug("CELL col=" + cell.getColumnIndex() + " VALUE=" + value);
          }
          if (returnType == ExcelUtil.RETURN_TYPE_STRING) {
               return value;
          } else if (returnType == ExcelUtil.RETURN_TYPE_OBJECT) {
               return obj;
          } else {
               return obj;
          }
     }

     /**
      * 엑셀 데이터 ArrayList, Map 객체로 가져오기
      * 
      * @param filename 파일경로
      * @param returnType 엑셀데이터 리턴타입 (1:String, 2:Object)
      * @return ArrayList<ArrayList<Map<String,Object>>>
      */
     public static ArrayList<ArrayList<Map<String, Object>>> getData(String filename, int returnType) {

          FileInputStream fin = null;
          XSSFWorkbook workbook = null;
          ArrayList<ArrayList<Map<String, Object>>> excelDataList = new ArrayList<ArrayList<Map<String, Object>>>();
          try {
               fin = new FileInputStream(filename);
               workbook = new XSSFWorkbook(fin);
               int sheetNum = workbook.getNumberOfSheets();
               for (int k = 0; k < sheetNum; k++) {
                    // LOGGER.debug("Sheet Number : " + k);
                    // LOGGER.debug("Sheet Name : " + workbook.getSheetName(k));
                    ArrayList<Map<String, Object>> sheetDataList = new ArrayList<Map<String, Object>>();
                    XSSFSheet sheet = workbook.getSheetAt(k);

                    int rows = sheet.getPhysicalNumberOfRows();
                    for (int r = 0; r < rows; r++) {
                         Map<String, Object> rowData = new HashMap<String, Object>();
                         XSSFRow row = sheet.getRow(r);
                         if (row == null) {
                              break;
                         }
                         // LOGGER.debug("Row : " + row.getRowNum());

                         int cells = row.getPhysicalNumberOfCells();
                         for (int c = 0; c < cells; c++) {
                              Object value = getColumnValue(row, c, returnType);
                              rowData.put(k + "_" + r + "_" + c, value);
                         }
                         sheetDataList.add(rowData);
                    }
                    excelDataList.add(sheetDataList);
               }
          } catch (FileNotFoundException e) {
               e.printStackTrace();
          } catch (IOException e) {
               e.printStackTrace();
          } finally {
               try {
                    if (workbook != null) {
                         workbook = null;
                    }

                    if (fin != null) {
                         fin.close();
                    }
               } catch (IOException e) {
                    e.printStackTrace();
               }
          }
          return excelDataList;
     }

     /**
      * 특정 sheet의 엑셀데이터 가져오기
      * 
      * @param filename 파일경로
      * @param sheetIndex
      * @param returnType 엑셀데이터 리턴타입 (1:String, 2:Object)
      * @return ArrayList<Map<String,Object>>
      */
     public static ArrayList<Map<String, Object>> getData(String filename, int sheetIndex, int returnType) {

          ArrayList<Map<String, Object>> sheetDataList = new ArrayList<Map<String, Object>>();
          FileInputStream fin = null;
          XSSFWorkbook workbook = null;
          try {
               fin = new FileInputStream(filename);
               workbook = new XSSFWorkbook(fin);
               int sheetNum = workbook.getNumberOfSheets();
               if (sheetIndex < sheetNum) {
                    LOGGER.debug("Sheet Number : " + sheetIndex);
                    LOGGER.debug("Sheet Name : " + workbook.getSheetName(sheetIndex));
                    XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
                    int rows = sheet.getPhysicalNumberOfRows();
                    for (int r = 0; r < rows; r++) {
                         Map<String, Object> rowData = new HashMap<String, Object>();
                         XSSFRow row = sheet.getRow(r);
                         LOGGER.debug("Row : " + row.getRowNum());

                         int cells = row.getPhysicalNumberOfCells();
                         for (int c = 0; c < cells; c++) {
                              Object value = getColumnValue(row, c, returnType);
                              rowData.put(sheetIndex + "_" + r + "_" + c, value);
                         }
                         sheetDataList.add(rowData);
                    }
               }
          } catch (FileNotFoundException e) {
               e.printStackTrace();
          } catch (IOException e) {
               e.printStackTrace();
          } finally {
               try {
                    if (workbook != null) {
                         workbook = null;
                    }

                    if (fin != null) {
                         fin.close();
                    }
               } catch (IOException e) {
                    e.printStackTrace();
               }
          }
          return sheetDataList;
     }

     /**
      * 특정 행의 엑셀데이터를 Map객체로 가져오기
      * 
      * @param filename 파일경로
      * @param sheetIndex
      * @param rowIndex
      * @return Map<String,Object>
      */
     public static Map<String, Object> getData(String filename, int sheetIndex, int rowIndex, int returnType) {

          Map<String, Object> rowData = new HashMap<String, Object>();
          FileInputStream fin = null;
          XSSFWorkbook workbook = null;
          try {
               fin = new FileInputStream(filename);
               workbook = new XSSFWorkbook(fin);
               LOGGER.debug("Sheet Number : " + sheetIndex);
               LOGGER.debug("Sheet Name : " + workbook.getSheetName(sheetIndex));
               XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
               if (sheet != null) {
                    int rows = sheet.getPhysicalNumberOfRows();
                    if (rowIndex < rows) {
                         XSSFRow row = sheet.getRow(rowIndex);
                         LOGGER.debug("Row : " + rowIndex);

                         int cells = row.getPhysicalNumberOfCells();
                         for (int c = 0; c < cells; c++) {
                              LOGGER.debug("Col : " + c);
                              Object value = getColumnValue(row, c, returnType);
                              rowData.put(sheetIndex + "_" + rowIndex + "_" + c, value);
                         }
                    }
               }
          } catch (FileNotFoundException e) {
               e.printStackTrace();
          } catch (IOException e) {
               e.printStackTrace();
          } finally {
               try {
                    if (workbook != null) {
                         workbook = null;
                    }

                    if (fin != null) {
                         fin.close();
                    }
               } catch (IOException e) {
                    e.printStackTrace();
               }
          }
          return rowData;
     }
}
