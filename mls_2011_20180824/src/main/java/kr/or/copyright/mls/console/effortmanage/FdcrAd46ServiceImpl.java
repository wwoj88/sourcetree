package kr.or.copyright.mls.console.effortmanage;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;
import kr.or.copyright.mls.console.effortmanage.inter.FdcrAd46Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd46Service" )
public class FdcrAd46ServiceImpl extends CommandService implements FdcrAd46Service{

	// OldDao adminStatProcDao
	@Resource( name = "fdcrAd03Dao" )
	private FdcrAd03Dao fdcrAd03Dao;

	/**
	 * ��ȸ
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd46List1( Map<String, Object> commandMap ) throws Exception{
		List list = (List) fdcrAd03Dao.nonWorksReptView( commandMap );

		commandMap.put( "ds_List", list );

	}
}
