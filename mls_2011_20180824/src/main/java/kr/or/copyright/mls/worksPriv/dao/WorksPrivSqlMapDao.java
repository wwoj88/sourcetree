package kr.or.copyright.mls.worksPriv.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.board.model.Board;
import kr.or.copyright.mls.board.model.BoardFile;
import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.worksPriv.model.WorksPriv;
import kr.or.copyright.mls.worksPriv.model.WorksPrivFile;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class WorksPrivSqlMapDao extends SqlMapClientDaoSupport implements WorksPrivDao {
//	LIST 총 갯수 마이페이지
	public int findWorksPrivCount(Map params){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("WorksPriv.getWorksPrivCount", params));
	}
//	LIST 내용 마이페이지
	public List findWorksPrivList(Map params){
		return getSqlMapClientTemplate().queryForList("WorksPriv.getWorksPrivList", params);
	}
	
//	LIST 총 갯수 참여마당
	public int findWorksPrivCountO(Map params){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("WorksPriv.getWorksPrivCountO", params));
	}
//	LIST 내용 참여마당
	public List findWorksPrivListO(Map params){
		return getSqlMapClientTemplate().queryForList("WorksPriv.getWorksPrivListO", params);
	}	
	
	
	
	public List<CodeList> findCodeList(CodeList codeList){
		return getSqlMapClientTemplate().queryForList("WorksPriv.getCodeList", codeList);
	}
	
//	개인저작물 등록  최신 번호 가져오기
	public int worksPrivMax(WorksPriv worksPriv){
		Integer cnt = (Integer)getSqlMapClientTemplate().queryForObject("WorksPriv.worksPrivMax",worksPriv);
		return cnt.intValue();
	}
	
//	개인저작물 등록 하기F
	public void goInsert(WorksPriv worksPriv){
		getSqlMapClientTemplate().insert("WorksPriv.goInsert", worksPriv);
		getSqlMapClientTemplate().insert("WorksPriv.goInsertCCL", worksPriv);
		
	}
	
//  개인저작물 파일 등록하기
	public void goInsertFile(WorksPrivFile worksPrivFile){
		getSqlMapClientTemplate().insert("WorksPriv.goInsertFile", worksPrivFile);
	}
	
//	개인저작물 상세보기
	public WorksPriv goView(WorksPriv worksPriv){
		return (WorksPriv)getSqlMapClientTemplate().queryForObject("WorksPriv.goView",worksPriv);
	}
	
//	개인저작물 파일 리스트
	public List worksPrivFileList(WorksPriv worksPriv){
		return (List)getSqlMapClientTemplate().queryForList("WorksPriv.worksPrivFileList", worksPriv);
	}
	
//	파일 삭제
	public void fileDelete(WorksPrivFile worksPrivFile){
		getSqlMapClientTemplate().delete("WorksPriv.deleteFileWorksPriv", worksPrivFile);
	}
	
//	개인저작물 수정
	public void goUpdate(WorksPriv worksPriv){
		getSqlMapClientTemplate().update("WorksPriv.goUpdate", worksPriv);
		getSqlMapClientTemplate().update("WorksPriv.goUpdateCCL", worksPriv);
	}
//  개인저작물 삭제
	public void goDelete(WorksPriv worksPriv){
		getSqlMapClientTemplate().delete("WorksPriv.goDelete", worksPriv);
		getSqlMapClientTemplate().delete("WorksPriv.goDeleteCCL", worksPriv);
		getSqlMapClientTemplate().delete("WorksPriv.goDeleteFILE", worksPriv);
	}
}
