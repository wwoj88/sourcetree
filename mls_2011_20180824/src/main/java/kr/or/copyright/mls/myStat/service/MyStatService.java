package kr.or.copyright.mls.myStat.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.myStat.model.StatApplication;
import kr.or.copyright.mls.myStat.model.StatApplicationShis;
import kr.or.copyright.mls.myStat.model.StatApplyWorks;


public interface MyStatService {
	
	//법정허락 신청현황 목록 조회
	public List statRsltInqrList(StatApplication params);
	public int totalRowStatRsltInqrList(StatApplication params);
	
	//법청허락 신규 신청서SEQ 조회
	public int newApplyWriteSeq(String param);
	
	// step1 수정(보완저장)
	public void updateStatPrps_priv(HttpServletRequest request,StatApplication params, boolean isExcel, String existYn);
	
	//법정허락 이용승인 신청서tmp 저장(임시저장:신규)
	public void insertTmpStatPrps(HttpServletRequest request, StatApplication param);
	
	//법정허락 이용승인 신청서tmp 저장(파일저장:신규)
	public void insertTmpStatPrps_new(List FileList);
	
	//법정허락 이용승인 신청서tmp 저장(임시저장:신규,개인저작물) 20120816 /수정/ 정병호
	public void insertTmpStatPrps_priv(HttpServletRequest request, StatApplication param, String existYn);
	
	//법정허락 이용승인 임시 엑셀명세서 파일 조회(임시저장:신규,개인저작물) 20120816 /수정/ 정병호
	public Map<String, Object> selectExcelStatApplyWorksInfoTmp(StatApplication params) throws Exception;
	
	//법정허락 이용승인 엑셀명세서 파일 조회(임시저장:신규,개인저작물) 20120816 /수정/ 정병호
	public Map<String, Object> selectExcelStatApplyWorksInfo(StatApplication params) throws Exception;
	
	//법정허락 이용승인 엑셀명세서 파일 저장(저장:신규,개인저작물) 20120820 /수정/ 정병호
	public void insertExcelStatApplyWorksInfo(StatApplication params) throws Exception;
	
	 // 법정허락 이용승인 임시 엑셀명세서 파일 저장(임시저장:신규,개인저작물) 20120820 /수정/ 정병호
	public void insertExcelStatApplyWorksInfoTmp(StatApplication params) throws Exception;
	
	//법정허락 이용승인 명세서 저장(임시저장:신규,개인저작물) 20120820 /수정/ 정병호
	public void insertStatApplyWorksList_tmp(List list, String chk);
	
	//법정허락 이용승인 명세서tmp 삭제
	public void deleteStatApplyWorksTmp(StatApplyWorks param);
	//법
	//법정허락 이용승인 신청서tmp 조회
	public StatApplication statApplicationTmp(StatApplication param);
	
	//법정허락 이용승인 신청서 조회
	public StatApplication statApplication(StatApplication param);
	
	//법정허락 이용승인 신청 상태변경낵역 조회
	public List statApplicationShisList(StatApplicationShis param);
	
	//법정허락 이용승인 신청서tmp 첨부파일 조회
	public List statAttcFileTmp(StatApplication param);
	
	//법정허락 이용승인 신청서 첨부파일 조회
	public List statAttcFile(StatApplication param);
	
	//법정허락 이용승인 신청서tmp 수정(임시저장:수정)
	public void updateTmpStatPrps(HttpServletRequest request, StatApplication param);
	
	//법정허락 이용승인 신청서tmp 수정(임시저장:수정)
	public void updateTmpStatPrps_priv(HttpServletRequest request, StatApplication param, boolean isExcel, String existYn);
	
	//법정허락 이용승인 신청서 수정(보완임시저장)
	public void updateStatPrps(HttpServletRequest request, StatApplication param);
	
	//법정허락 이용승인 명세서tmp 기존 삭제 및 신규 저장(임시저장)
	public void insertTmpStatApplyWorksList(List list);
	
	//법정허락 이용승인 명세서 기존 삭제 및 신규 저장(보완임시저장)
	public void insertStatApplyWorksList(List list);
	
	//법정허락 이용승인 신청 저장
	public void insertStatPrps(StatApplication param);
	
	//법정허락 이용승인 신청 보완
	public void updateStatPrps(StatApplication param);
	
	//법정허락 이용승인 명세서tmp 삭제
	public void deleteStatApplyWorks(StatApplyWorks param);
	
	//법정허락 이용승인 신청 명세서tmp 조회

	public List statApplyWorksListTmp(StatApplyWorks param);
	//법정허락 이용승인 신청 명세서 조회
	public List statApplyWorksList(StatApplyWorks param);
	
	//법정허락 이용승인 신청 임시저장 내역 삭제
	public void deleteStatPrps(List param);
	
	// 법정허락 결제정보 등록
	public void insertStatPayStatus(StatApplication params);
	
	// 법정허락 결제정보 수정
	public void updateStatPayStatus(StatApplication params);
	
}