package kr.or.copyright.mls.board.service;

import static org.mockito.Matchers.booleanThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import kr.or.copyright.mls.board.dao.BoardDao;
import kr.or.copyright.mls.board.model.Board;
import kr.or.copyright.mls.board.model.BoardFile;
import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.support.util.FileUploadUtil;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

//@TransactionConfiguration(defaultRollback = true)
public class BoardServiceImpl extends BaseService implements BoardService {

	//private Log logger = LogFactory.getLog(getClass());
	
	private BoardDao boardDao;
	//private DaoManager daoMgr;

	public void setBoardDao(BoardDao boardDao) {
//		daoMgr = DaoConfig.getDaoManager();
		this.boardDao = boardDao;
	}
	
	@Transactional(readOnly = false)
	public int insertQust(Board board) throws Exception{
		
		try {
			int threaded = boardDao.boardMax(board);
	
//			daoMgr.startTransaction();
			
			board.setBordSeqn(threaded);
			board.setThreaded(threaded);
			System.out.println( board.toString() );
			System.out.println( threaded );
			
			boardDao.insertQust(board);
			
	//		파일등록
			BoardFile boardFile = new BoardFile();
			boardFile.setFileList(board.getFileList());
			List multFileList = (List)boardFile.getFileList();
			for(int i=0; i < board.getFileList().size(); i++) {
				
				MultipartFile multFile = (MultipartFile) multFileList.get( i );
				boardFile.setFileName( multFile.getOriginalFilename() );
				boardFile.setRealFileName( multFile.getOriginalFilename() );
				boardFile.setFileSize( ""+multFile.getSize() );
				boardFile.setFilePath( board.getFilePath() );
				boardFile.setBordSeqn(""+threaded);
				boardFile.setThreaded(""+threaded);
				boardFile.setMenuSeqn(board.getMenuSeqn());	
					
				boardDao.insertBoardFile(boardFile);
			}
			
//			daoMgr.commitTransaction();
		} catch(Exception e){
			e.printStackTrace();
			
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return -1;
			
		} 
		
		return 1;
	}
	
	public Board isValidPswd(Board board) {
		
		board = boardDao.isValidPswd(board);
		return board;
	}

	public void updateInqrCont(Board board) {
		boardDao.updateInqrCont(board);
	}
	
	public void updateStatInqrCont(Board board) {
		boardDao.updateStatInqrCont(board);
	}
	
	public void updateQust(Board board) {
		boardDao.updateQust(board);
			
/*//		파일등록
		BoardFile boardFile = new BoardFile();
			
		for(int i=0; i < board.getFileList().size(); i++) {

			boardFile = (BoardFile) board.getFileList().get(i);
			boardFile.setBordSeqn(""+board.getBordSeqn());
			boardFile.setThreaded(""+board.getThreaded());
			boardFile.setMenuSeqn(board.getMenuSeqn());	
				
			boardDao.insertBoardFile(boardFile);
		}*/
		//		파일등록
		BoardFile boardFile = new BoardFile();
		boardFile.setFileList(board.getFileList());
		List multFileList = (List)boardFile.getFileList();
		for(int i=0; i < board.getFileList().size(); i++) {
			
			MultipartFile multFile = (MultipartFile) multFileList.get( i );
			boardFile.setFileName( multFile.getOriginalFilename() );
			boardFile.setRealFileName( multFile.getOriginalFilename() );
			boardFile.setFileSize( ""+multFile.getSize() );
			boardFile.setFilePath( board.getFilePath() );
			boardFile.setBordSeqn(""+board.getThreaded());
			boardFile.setThreaded(""+board.getThreaded());
			boardFile.setMenuSeqn(board.getMenuSeqn());	
				
			boardDao.insertBoardFile(boardFile);
		}
	}
	public void fileMultDelete( ArrayList<BoardFile> chk ){
		// TODO Auto-generated method stub
		String strSavePath = "D:\\Git\\mls_2011\\web\\upload\\test\\"; // 실제 웹 서버에 저장되는 디렉터리를 지정한다.
		System.out.println( chk );
		for(BoardFile deleteFile : chk)
		{
			//System.out.println( "deleteFile.getRealFileName() : " +deleteFile.getRealFileName());
			//System.out.println( "deleteFile.getAttcSeqn() : " +deleteFile.getAttcSeqn());
			FileUploadUtil.deleteFormFiles(deleteFile.getRealFileName(), strSavePath); // 로컬용
			boardDao.fileDelete(deleteFile);
		}
		
	}
	public void fileDelete(String[] chk) {
		BoardFile boardFile = new BoardFile();
		String[] fname = new String[2];
		
		for(int i = 0; i < chk.length; i++){
			StringTokenizer st = new StringTokenizer(chk[i],",");
			
			while (st.hasMoreTokens()) {
		        fname[0] = st.nextToken();    // attcSeqn
		        fname[1] = st.nextToken();    // fileName
			}
		
			//FileUploadUtil.deleteFormFiles(fname[1], strSavePath); // 로컬용
			//FileUploadUtil.deleteFormFiles(fname[1], "/home/tmax/mls/web/upload/"); // 실서버용
			
			FileUploadUtil.deleteFormFiles(fname[1], "/home/tmax_dev/mls/web/upload/"); // 개발서버용
			//FileUploadUtil.deleteFormFiles(fname[1], new String(kr.or.copyright.mls.support.constant.Constants.getProperty("file_realUpload_path"))); // 개발서버용
			
			//boardFile.setAttcSeqn( Long.parseLong(fname[0]));
			boardDao.fileDelete(boardFile);
		}
	}

	public ListResult findBoardList(int pageNo, int rowPerPage, Map params) {
		
        int from = rowPerPage * (pageNo-1) + 1;
        int to = rowPerPage * pageNo;
        
        params.put("FROM", ""+from);
        params.put("TO", ""+to);
        int totalRow = 0;
        
//		List list = boardDao.findBoardList(params);
		/* 양재석 추가 start */
		List list = null;
		
		String menuSeqn	= (String)params.get("menuSeqn");
		
        if(menuSeqn.equals("8")){
        	totalRow = boardDao.findStatBoardCount(params); //법정허락저작물
        }else if(menuSeqn.equals("9")){
        	totalRow = boardDao.findMyQustCount(params);
        }else{
        	totalRow = boardDao.findBoardCount(params);
        }
		
		if(menuSeqn.equals("4")){ 
			list = boardDao.findNotiBoardList(params);
		}else if(menuSeqn.equals("8")){ 
			list = boardDao.findStatBoardList(params); //법정허락저작물
		}else if(menuSeqn.equals("9")){ 
			//list = boardDao. //마이페이지 묻고답하기
			list = boardDao.findMyQustList(params);
		}else{
			list = boardDao.findBoardList(params);	
		}
		/* 양재석 추가 end */
		
		ListResult listResult = new ListResult(totalRow, pageNo, rowPerPage, list);
	
		return listResult;
	}
	
	public Board boardView(Board board) {
		
		//board = boardDao.boardView(board);
		/* 양재석 추가 start */
		String menuSeqn	= String.valueOf((int)board.getMenuSeqn());
		
		if(menuSeqn.equals("3")){
			board = boardDao.notiBoardView(board);
		}else if(menuSeqn.equals("8")){
			board = boardDao.statBoardView(board);
		}else if(menuSeqn.equals("9")){
			board = boardDao.myQustBoardView(board);
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>22"+board.getSrchText());
		}else{
			board = boardDao.boardView(board);
		}
		/* 양재석 추가 end */
		if(menuSeqn.equals("8")){
			board.setFileList(boardDao.statBoardFileList(board));
		}else{
			board.setFileList(boardDao.boardFileList(board));
		}

		return board;
	}

	public void deleteQust(Board board) {
		if(board.getDeth() == 0) {
			long prevThreaded = (board.getThreaded()-1)/1000 * 1000;
			board.setPrevThreaded(prevThreaded);

			boardDao.deleteAll(board);
			boardDao.deleteFileAll(board);

		} else {
			boardDao.delete(board);
		}
	}
	
	// 커뮤니티 게시판 추가(최남식)
	public void insertComm(Board board) {
		try {
			int threaded = boardDao.boardMax(board);
	
//			daoMgr.startTransaction();
			
			board.setBordSeqn(threaded);
			board.setThreaded(threaded);
			
			boardDao.insertComm(board);
			
	//		파일등록
			BoardFile boardFile = new BoardFile();
				
			for(int i=0; i < board.getFileList().size(); i++) {
	
				boardFile = (BoardFile) board.getFileList().get(i);
				boardFile.setBordSeqn(""+threaded);
				boardFile.setThreaded(""+threaded);
				boardFile.setMenuSeqn(board.getMenuSeqn());	
					
				boardDao.insertBoardFile(boardFile);
			}
//			daoMgr.commitTransaction();
		} finally {
//			daoMgr.endTransaction();
		}
	}
}
