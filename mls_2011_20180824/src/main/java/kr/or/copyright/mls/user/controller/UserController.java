package kr.or.copyright.mls.user.controller;

import static org.mockito.Matchers.booleanThat;

import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.common.userLogin.service.UserLoginService;
import kr.or.copyright.mls.adminStatProc.dao.AdminStatProcDao;
import kr.or.copyright.mls.ajaxTest.service.CodeListService;
import kr.or.copyright.mls.common.common.utils.CommonUtil2;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.SMSManager;
import kr.or.copyright.mls.support.util.SessionUtil;
import kr.or.copyright.mls.user.service.UserService;
import org.json.simple.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import KISINFO.VNO.VNOInterop;

import com.softforum.xdbe.xCrypto;

public class UserController extends MultiActionController {

	//private Log logger = LogFactory.getLog(getClass());
    
	private CodeListService codeListService;
	
	public void setCodeListService(CodeListService codeListService){
	    this.codeListService = codeListService;
	}

	private UserService userService;

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	private UserLoginService userLoginService;

	public void setUserLoginService(UserLoginService userLoginService) {
		this.userLoginService = userLoginService;
	}

	public ModelAndView goPage(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		return new ModelAndView("user/userRegiCheck");
	}
	
	public ModelAndView userRegiCheck(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		Map params = new HashMap();
		
		ModelAndView mv = null;
		
		Map userMap = new HashMap();

		/*params.put("userDivs", ServletRequestUtils.getStringParameter(request, "userDivs"));
		params.put("userName", ServletRequestUtils.getStringParameter(request, "userName"));
		params.put("corpNumb", ServletRequestUtils.getStringParameter(request, "corpNumb"));
		params.put("dupInfo", ServletRequestUtils.getStringParameter(request, "dupInfo"));*/			
		//2014-08-01 안심본인인증
		//회원구분
		params.put("userDivs", ServletRequestUtils.getStringParameter(request, "userDivs"));
		//이름
		params.put("userName", ServletRequestUtils.getStringParameter(request, "sName"));
		//중복가입확인값
		params.put("dupInfo", ServletRequestUtils.getStringParameter(request, "dupInfo"));
		//법인번호
		params.put("resdCorpNumb", ServletRequestUtils.getStringParameter(request, "resdCorpNumb"));
		//사업자번호
		params.put("corpNumb", ServletRequestUtils.getStringParameter(request, "corpNumb"));
		userMap = userService.findUserList(params);
		
		mv = new ModelAndView("ident/idntSrchRlst");
		
		mv.addObject("userMap", userMap);
		
		
		if(userMap != null){
		    mv.addObject("idntSrch", "Y");//사용자가 있다면 Y
		}else{
		    mv.addObject("idntSrch", "");//사용자가 없다면
		}

		return mv;
	}
	
	public ModelAndView goUserAgree(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		return new ModelAndView("user/userAgree");
	}
	
	public ModelAndView goUserRegi(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		return new ModelAndView("user/userRegi");
	}
	
	public ModelAndView goIdntConf(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		return new ModelAndView("common/idntConf");
	}
	
	public ModelAndView goSsnNoConf(HttpServletRequest request,
		HttpServletResponse respone) throws Exception {
	    return new ModelAndView("common/goSsnNoConf");
	}
	
	public ModelAndView goPostNumbSrch(HttpServletRequest request,
		HttpServletResponse respone) throws Exception {
	    	ModelAndView mv = new ModelAndView("common/zipxNumb","initZip", userService.getInitRoadCodeList());
		return mv;
	}
	
	public ModelAndView goSgInstall(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		return new ModelAndView("common/sg_install");
	}
	
	public ModelAndView goLogin(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		return new ModelAndView("userLogin/userLogn");
	}
	public ModelAndView goLogin2(HttpServletRequest request,
		HttpServletResponse respone) throws Exception {

	return new ModelAndView("userLogin/userLogn2");
}

	
	public ModelAndView goLoginSso(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		return new ModelAndView("userLogin/userLognSso");
	}
	
	public ModelAndView goIdntPswdSrch(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		return new ModelAndView("ident/idntPswdSrch");
	}
	
	public ModelAndView goIdntSrch(HttpServletRequest request,
		HttpServletResponse respone) throws Exception {

	return new ModelAndView("ident/idntSrch");
	}	
	public ModelAndView goPswdSrch(HttpServletRequest request,
		HttpServletResponse respone) throws Exception {

	return new ModelAndView("ident/pswdSrch");
	}
	
	public ModelAndView userIdntPswdSrch(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		Map params = new HashMap();
		Map userMap = new HashMap();
		ModelAndView mv = null;
		
		String pswdSrch = ServletRequestUtils.getStringParameter(request, "pswdSrch");

		if ("Y".equals(pswdSrch)) {
			
			params.put("userDivs", ServletRequestUtils.getStringParameter(request, "userDivs_pwd"));
			params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt_pwd"));
			params.put("userName", ServletRequestUtils.getStringParameter(request, "userName_pwd"));
			params.put("resdCorpNumb", ServletRequestUtils.getStringParameter(request, "resdCorpNumb_pwd"));
			params.put("corpNumb", ServletRequestUtils.getStringParameter(request, "corpNumb_pwd"));
			params.put("pswdSrch", ServletRequestUtils.getStringParameter(request, "pswdSrch_pwd"));
			params.put("mail", ServletRequestUtils.getStringParameter(request, "userEmail_pwd"));
			params.put("mgnbYsno", ServletRequestUtils.getStringParameter(request, "mgnbYsno_pwd"));
			params.put("dupInfo", ServletRequestUtils.getStringParameter(request, "dupInfo"));
			
			//return new ModelAndView("ident/pswdSrchRlst", "userMap", userService.userIdntPswdSrch(params));
			
			//return new ModelAndView("ident/idntSrchRlst", "userMap", userService.userIdntPswdSrch(params));
			
			userMap = userService.userPswdSrch(params);
		
			String userInfoYn = "N"; //디폴트 회원정보 없음..
			
			if(userMap != null){
	
				userInfoYn ="Y"; //회원정보 있음..
				String exPswd = CommonUtil2.asciiout(10); // 비밀번호 랜덤 생성
				
				if(userMap.get("CLMS_AGR_YN").equals("Y")){//CLMS가입 회원인 경우
					userMap.put("pswd", exPswd);
					userMap.put("clmsUserIdnt", userMap.get("CLMS_USER_IDNT"));
					
					userService.updateClmsUserInfo(userMap); //CLMS DB 비밀번호 update
				}
				
				String pswd = exPswd.toUpperCase();
				String sOutput_H        = null;
				sOutput_H	= xCrypto.Hash(6, pswd);//단방향 암호화(sOutput)
				
				userMap.put("pswd", sOutput_H);
				userMap.put("userIdnt", userMap.get("USER_IDNT"));
				
				userService.updateUserPswd(userMap); //저작권찾기 DB 비밀번호 update
				
				//----------------------------- mail send------------------------------//
			    String host = Constants.MAIL_SERVER_IP;	//smtp서버
			    String to     =  (String) userMap.get("MAIL");	// 수신인
			    String toName =  (String) userMap.get("USER_NAME");
			    String subject = "[저작권찾기] 임시 비밀번호가 발급되었습니다.";
			      
			    String from = Constants.SYSTEM_MAIL_ADDRESS;  //발신인 주소 - 시스템 대표 메일
			    String fromName = Constants.SYSTEM_NAME;
			        
			    String memberName = (String) userMap.get("USER_NAME");
			    String memberId   = (String) userMap.get("USER_IDNT");
			    String exPswdUpper = exPswd.toUpperCase();
			      
			    if(to != null && to != "")  {
				    //------------------------- mail 정보 ----------------------------------//
				    MailInfo info = new MailInfo();
				    info.setFrom(from);
				    info.setFromName(fromName);
				    info.setHost(host);
				    info.setEmail(to);
				    info.setEmailName(toName);
				    info.setSubject(subject);
				      
				    
				    
				    StringBuffer sb = new StringBuffer();   
					sb.append("				<tr>");
					sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
					sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">저작권찾기 사이트 임시 비밀번호 정보</p>");
					sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
					sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 회원님의 이름 : "+memberName+"</li>");
					sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 회원님의 아이디 : "+memberId+"</li>");
					sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 임시 비밀번호 : "+exPswdUpper+"</li>");				
					sb.append("					</ul>");
					sb.append("					</td>");
					sb.append("				</tr>");
		
				    info.setMessage(sb.toString());
				    info.setDetlYn("N");
					info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
				    
				    
				    
/*				    
				    sb.append("<div class=\"mail_title\">"+toName+"님의 저작권찾기 사이트 임시 비밀번호입니다. </div>");
				    sb.append("<div class=\"mail_contents\"> ");
				    sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
				    sb.append("		<tr> ");
				    sb.append("			<td> ");
				    sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
		// 변경부분
				    sb.append("					<tr> ");
				    sb.append("						<td class=\"tdLabel\">회원님의 아이디</td> ");
				    sb.append("						<td class=\"tdData\">"+ memberId+"</td> ");
				    sb.append("					</tr> ");
		// 변경부분
				    sb.append("					<tr> ");
				    sb.append("						<td class=\"tdLabel\">임시 비밀번호</td> ");
				    sb.append("						<td class=\"tdData\">"+exPswdUpper+"</td> ");
				    sb.append("					</tr> ");

				    sb.append("				</table> ");
				    sb.append("			</td> ");
				    sb.append("		</tr> ");
				    sb.append("	</table> ");
				    sb.append("</div> ");
					
				    info.setMessage(new String(MailMessage.getChangeInfoMailText(sb.toString())));*/
		    	  /*  MailManager manager = MailManager.getInstance();
			    	
		    	    info = manager.sendMail(info);
			    	if(info.isSuccess()){
			    	  System.out.println(">>>>>>>>>>>> message success :"+info.getEmail() );
			    	}else{
			    	  System.out.println(">>>>>>>>>>>> message false   :"+info.getEmail() );
			    	}    	*/
			    			    
			    }
			}
			
			
				mv = new ModelAndView("ident/idntPswdSrch", "userMap", userMap);
				mv.addObject("userInfoYn", userInfoYn); 
				mv.addObject("pswdSrch", pswdSrch); 
			
		} else {
			
			params.put("userDivs", ServletRequestUtils.getStringParameter(request, "userDivs"));
			params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt"));
			params.put("userName", ServletRequestUtils.getStringParameter(request, "userName"));
			params.put("resdCorpNumb", ServletRequestUtils.getStringParameter(request, "resdCorpNumb"));
			params.put("corpNumb", ServletRequestUtils.getStringParameter(request, "corpNumb"));
			params.put("pswdSrch", ServletRequestUtils.getStringParameter(request, "pswdSrch"));
			params.put("mgnbYsno", ServletRequestUtils.getStringParameter(request, "mgnbYsno"));
			params.put("commName", ServletRequestUtils.getStringParameter(request, "commName"));
			params.put("mail", ServletRequestUtils.getStringParameter(request, "mail"));
			params.put("dupInfo", ServletRequestUtils.getStringParameter(request, "dupInfo"));
			
			
			//return new ModelAndView("ident/idntSrchRlst", "userMap", userService.userIdntPswdSrch(params));
			mv = new ModelAndView("ident/idntSrchRlst", "userMap", userService.userIdntPswdSrch(params));
			mv.addObject("pswdSrch", pswdSrch);  
			mv.addObject("idntSrch", "Y");  
		}
		return mv;
	}
	
	public ModelAndView postNumbSrch(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		Map params = new HashMap();
		ModelAndView mv = new ModelAndView("common/zipxNumb");
		params.put("law_dong_nm", ServletRequestUtils.getStringParameter(request, "law_dong_nm"));
		params.put("law_li_nm", ServletRequestUtils.getStringParameter(request, "law_li_nm"));
		params.put("jibun_orig_no", ServletRequestUtils.getStringParameter(request, "jibun_orig_no"));
		params.put("jibun_sub_no", ServletRequestUtils.getStringParameter(request, "jibun_sub_no"));
		params.put("road_nm", ServletRequestUtils.getStringParameter(request, "road_nm"));
		params.put("buld_orig_no", ServletRequestUtils.getStringParameter(request, "buld_orig_no"));
		params.put("buld_sub_no", ServletRequestUtils.getStringParameter(request, "buld_sub_no"));
		params.put("buld_nm", ServletRequestUtils.getStringParameter(request, "buld_nm"));
		params.put("search", ServletRequestUtils.getStringParameter(request, "search"));
		params.put("sido_nm", ServletRequestUtils.getStringParameter(request, "sido_nm"));
		params.put("gugun_nm", ServletRequestUtils.getStringParameter(request, "gugun_nm"));
		
		mv.addObject("postNumbList",userService.findPostRoadNumb(params));
		mv.addObject("initZip", userService.getInitRoadCodeList());
	
		return mv;
		
		//return new ModelAndView("common/zipxNumb", "postNumbList", userService.findPostRoadNumb(params));
	}
	
	public ModelAndView getIdntConf(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		Map params = new HashMap();

		params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt"));
		
		Map userIdnt = new HashMap();
		
		userIdnt = userService.findUserIdnt(params);
		
		// CLMS 통합로그인 동의여부 START
		if(userIdnt==null)
			userIdnt = userService.findClmsUserIdnt1(params);
		
		if(userIdnt==null)
			userIdnt = userService.findClmsUserIdnt2(params);
		// CLMS 통합로그인 동의여부 END
		
		return new ModelAndView("common/idntConf", "userIdntMap", userIdnt);
	}
	
	
	public ModelAndView regiSsnNo(HttpServletRequest request,
		HttpServletResponse respone) throws Exception {
	    
	    Map params = new HashMap();
	    
	    params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt"));
	    params.put("resdCorpNumb", ServletRequestUtils.getStringParameter(request, "resdCorpNumb"));
	    
	    userService.updateRegiSsnNo(params);
	    
	    Map userInfo = new HashMap();
	    
	    userInfo = userService.selectUserInfo(params);
	    
	    String ssnNo = (String) userInfo.get("RESD_CORP_NUMB");

	    ModelAndView mv = new ModelAndView("common/goSsnNoConf");
	    
	    if(ssnNo.equals("ipin")){
		mv.addObject("regiChk", "");
	    }else{
		mv.addObject("regiChk", "Y");
		HttpSession session = request.getSession(true);
		session.setAttribute("sessSsbNo",ssnNo);
	    }
	    
	    return mv;
	}
	
	
	public ModelAndView goUserInsert(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		Map params = new HashMap();
		
		params.put("userDivs", ServletRequestUtils.getStringParameter(request, "userDivs"));
		params.put("userName", ServletRequestUtils.getStringParameter(request, "userName"));
		params.put("resdCorpNumb", ServletRequestUtils.getStringParameter(request, "resdCorpNumb1", "")+ServletRequestUtils.getStringParameter(request, "resdCorpNumb2", ""));
		params.put("corpNumb", ServletRequestUtils.getStringParameter(request, "corpNumb"));
		params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt"));
		params.put("pswd", ServletRequestUtils.getStringParameter(request, "pswd"));
		params.put("mail", ServletRequestUtils.getStringParameter(request, "mail"));
		params.put("zipxNumb", ServletRequestUtils.getStringParameter(request, "zip"));
		params.put("addr", ServletRequestUtils.getStringParameter(request, "addr"));
		params.put("detlAddr", ServletRequestUtils.getStringParameter(request, "detlAddr"));
		params.put("telxNumb", ServletRequestUtils.getStringParameter(request, "telxNumb"));
		params.put("moblPhon", ServletRequestUtils.getStringParameter(request, "moblPhon"));
		params.put("mailReceYsno", ServletRequestUtils.getStringParameter(request, "mailReceYsno"));
		params.put("smsReceYsno", ServletRequestUtils.getStringParameter(request, "smsReceYsno"));
		params.put("dupInfo", ServletRequestUtils.getStringParameter(request, "dupInfo"));
		params.put("road_addr_yn", ServletRequestUtils.getStringParameter(request, "road_addr_yn"));
		
		String pswd = ServletRequestUtils.getStringParameter(request, "pswd");
		String resdCorpNumb = ServletRequestUtils.getStringParameter(request, "resdCorpNumb1", "")+ServletRequestUtils.getStringParameter(request, "resdCorpNumb2", "");
		
		
		//CLMS DI값을 생성
		String sSiteCode = "D747";			// IPIN 서비스 사이트 코드		(NICE신용평가정보에서 발급한 사이트코드)
		String sSitePw = "Yagins12";			// IPIN 서비스 사이트 패스워드	(NICE신용평가정보에서 발급한 사이트패스워드)
		
		
		// ============ CLMS 통합로그인 동의여부 START
		
		String clmsAgrYN = ServletRequestUtils.getStringParameter(request, "clmsAgrYn", "N"); // CLMS 통합로그인 동의여부  
		params.put("clmsAgrYn", clmsAgrYN );		
		params.put("rightAgrYn", clmsAgrYN);			// 권리찾기 통합로그인 동의여부
		String sJumin = resdCorpNumb;
		String sFlag = "JID";
		
		// 중복(가입)확인모듈 객체 생성
		VNOInterop vnoInterop = new VNOInterop();
		// Method 결과값(iRtn)에 따라, 프로세스 진행여부를 파악합니다.
		int iRtn = vnoInterop.fnRequest(sSiteCode, sSitePw, sJumin, sFlag);
		String sDupInfo ="";
		if (iRtn == 1) {
	    	// iRtn 값이 정상이므로, 귀사의 기획의도에 맞게 진행하시면 됩니다.
	    	// 아래와 같이 bstrDupInfo 함수를 통해 DI 값(64 Byte)을 추출할 수 있습니다.
		    sDupInfo = vnoInterop.getDupInfo();
		}else{
		    sDupInfo = "";
		}
		params.put("sDupInfo",sDupInfo);
		
		// Method 결과값(iRtn)에 따라, 프로세스 진행여부를 파악합니다.
		
		Map clmsMap = new HashMap();
		
		params.put("clmsUserPswd", "");
		
		Map clmsInfo = null;
		
		// CLMS 통합로그인 동의여부 : 'Y'
		if(clmsAgrYN.equals("Y")) {
		    clmsInfo = new HashMap();

System.out.println("***  CLMS 통합로그인 동의여부 : 'Y' " );

			// CLMS 에 동일 주민등록번호(사업자번호)로 가입정보 존재여부 확인.
			
                	/*
                	 * CLMS DUP_INFO값을 만들어서 주민번호 OR 해당 DI값으로 가입 유무를 확인 할 수 있음
                	 * DUP_INFO값을 만들기 위해서는 CLMS의 사이트코드값과 주민번호만 있으면 셋팅은 가능함
                	 */
			clmsMap = userService.selectClmsUserInfo(params);
			
			if(clmsMap != null) {
System.out.println("****  CLMS 에 동일 주민등록번호(사업자번호)로 가입정보 존재" );				
				// CLMS 에 존재하는경우
		    	params.put("clmsUserIdnt", clmsMap.get("USER_IDNT"));		// CLMS 아이디
		    	params.put("clmsUserPswd", clmsMap.get("PSWD"));			// CLMS 패스워드
		    	
		    	userService.updateClmsUserInfo(params);
		    	
			} else {
System.out.println("****  CLMS 에 동일 주민등록번호(사업자번호)로 가입정보 미존재" );				
				// CLMS 에 존지하지 않는경우
				params.put("fromRightYn", "Y");		// 출처 저작권 여부
				params.put("clmsUserIdnt", params.get("userIdnt"));		// CLMS 아이디
		    	params.put("clmsUserPswd", params.get("pswd"));			// CLMS 패스워드
		    	params.put("resdCorpNumb", resdCorpNumb);
		    	
		    	
		    	/*
		    	 * CLMS DUP_INFO값을 만들어서 셋팅을 해줘야 한다.
		    	 * DUP_INFO값을 만들기 위해서는 CLMS의 사이트코드값과 주민번호만 있으면 셋팅은 가능함
		    	 */
		    	
		    	
		    	//params.put("clmsUserIdnt", params.get("userIdnt").toString().toUpperCase());		// CLMS 아이디
		    	//params.put("clmsUserPswd", params.get("pswd").toString().toUpperCase());			// CLMS 패스워드
				
		    		userService.insertClmsUserInfo(params);
		    	
		    		clmsInfo = userService.selectClmsUserInfo(params);
		    		
		    		if(clmsInfo != null) {
			    	    params.put("clmsUserPswd", clmsInfo.get("PSWD"));	
			    	}else{
			    	    params.put("clmsUserPswd", "");	
			    	}
			}
			//
		}
		// ============ CLMS 통합로그인 동의여부 END
		
		params.put("pswd", pswd);
		params.put("resdCorpNumb", resdCorpNumb);
		userService.insertUser(params);
		
		//----------------------------- mail send------------------------------//
	    String host = Constants.MAIL_SERVER_IP;	//smtp서버
	    String to     = ServletRequestUtils.getStringParameter(request, "mail");	// 수신인
	    String toName = ServletRequestUtils.getStringParameter(request, "userName");
	    String subject = "[저작권찾기] 회원가입이 완료되었습니다.";
	      
	    String from = Constants.SYSTEM_MAIL_ADDRESS;  //발신인 주소 - 시스템 대표 메일
	    String fromName = Constants.SYSTEM_NAME;
	        
	    String memberName = ServletRequestUtils.getStringParameter(request, "userName");
	    String memberId   = ServletRequestUtils.getStringParameter(request, "userIdnt");
	      
	    if(to != null && to != "")  {
		    //------------------------- mail 정보 ----------------------------------//
		    MailInfo info = new MailInfo();
		    info.setFrom(from);
		    info.setFromName(fromName);
		    info.setHost(host);
		    info.setEmail(to);
		    info.setEmailName(toName);
		    info.setSubject(subject);
		      
		    StringBuffer sb = new StringBuffer();   
		    
			sb.append("				<tr>");
			sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">");
			sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">저작권찾기 사이트 회원 정보</p>");
			sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
			sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 회원님의 이름 : "+memberName+"</li>");
			sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 회원님의 아이디 : "+memberId+"</li>");
			sb.append("					</ul>");
			sb.append("					</td>");
			sb.append("				</tr>");

		    info.setMessage(sb.toString());
		    info.setDetlYn("N");
			info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
		    
/*		    
		    
		    sb.append("<div class=\"mail_title\">"+memberName+"님이 저작권찾기 사이트에 회원으로 가입되었습니다. </div>");
		    sb.append("<div class=\"mail_contents\"> ");
		    sb.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
		    sb.append("		<tr> ");
		    sb.append("			<td> ");
		    sb.append("				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
// 변경부분
		    sb.append("					<tr> ");
		    sb.append("						<td class=\"tdLabel\">회원님의 아이디</td> ");
		    sb.append("						<td class=\"tdData\">"+ memberId+"</td> ");
		    sb.append("					</tr> ");
// 변경부분
//		    sb.append("					<tr> ");
//		    sb.append("						<td class=\"tdLabel\">담당자 이메일 주소</td> ");
//		    sb.append("						<td class=\"tdData\"><a href=\"mailto:"+Constants.SYSTEM_MAIL_ADDRESS+"\" onFocus='this.blur()'>"+Constants.SYSTEM_MAIL_ADDRESS+"</a></td> ");
//		    sb.append("					</tr> ");
//		    sb.append("					<tr> ");
//		    sb.append("						<td class=\"tdLabel\">담당자 전화번호</td> ");
//		    sb.append("						<td class=\"tdData\">"+Constants.SYSTEM_TELEPHONE+"</td> ");
//		    sb.append("					</tr> ");
		    sb.append("				</table> ");
		    sb.append("			</td> ");
		    sb.append("		</tr> ");
		    sb.append("	</table> ");
		    sb.append("</div> ");*/
			
/*		    sb.append("      <tr>                                                                                                                  ");
		    sb.append("        <td height=\"25\" class=\"font_12px_basic_L\">                                                                      ");
		    sb.append("          <img src=\"http://book.clms.or.kr/user/images/common/icon_title_orange01.gif\" width=\"4\" height=\"12\" align=\"absmiddle\">                 ");
		    sb.append("          &nbsp;<span class=\"font_11px_orange_L\">"+memberName +"</span> 님이                                                      ");
		    sb.append("                <span class=\"font_11px_orange_L\">저작권찾기 시스템 </span> 회원으로 가입되었습니다.</td>                             ");
		    sb.append("      </tr>                                                                                                                 ");
		    sb.append("      <tr>                                                                                                                  ");
		    sb.append("        <td height=\"25\" class=\"font_12px_basic_L\">                                                                      ");
		    sb.append("         <img src=\"http://book.clms.or.kr/user/images/common/icon_title_orange01.gif\" width=\"4\" height=\"12\" align=\"absmiddle\">                  ");
		    sb.append("          &nbsp;회원님의 아이디 : <span class=\"font_11px_orange_L\">"+memberId+"</span></td>                                           ");
		    sb.append("      </tr>                                                                                                                 ");
			
		    info.setMessage(new String(MailMessage.getChangeInfoMailText(sb.toString())));*/
    	    MailManager manager = MailManager.getInstance();
	    	
    	    info = manager.sendMail(info);
	    	if(info.isSuccess()){
	    	  System.out.println(">>>>>>>>>>>> message success :"+info.getEmail() );
	    	}else{
	    	  System.out.println(">>>>>>>>>>>> message false   :"+info.getEmail() );
	    	}	    	
	    
	    }
	      
	    String smsTo = ServletRequestUtils.getStringParameter(request, "moblPhon");
	    String smsFrom = Constants.SYSTEM_TELEPHONE;
	    String smsMessage = "저작권찾기 회원가입 완료";

	    SMSManager smsManager = new SMSManager();
	  	
	  	System.out.println("return = " + smsManager.sendSMS(smsTo, smsFrom, smsMessage));
		
//		return new ModelAndView("user/userRegiRslt");
	  	return new ModelAndView("user/userSucc");
	}
	
	public ModelAndView selectUserInfo(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		
		User user = SessionUtil.getSession(request);

		String sessUserIdnt = user.getUserIdnt();
		
		if(sessUserIdnt == null){
			return new ModelAndView("redirect:/user/user.do?method=goLogin");
		}

		Map params = new HashMap();

		params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt"));

		String  divs = ServletRequestUtils.getStringParameter(request, "DIVS"); 
		
		if(divs.equals("U")){
			
			String newClmsChk = "N";
			
			Map userInfoMap = userService.selectUserInfo(params);
			
			// CLMS 회원통합 : '미동의' 인경우. (20110406)
			if( userInfoMap.get("CLMS_AGR_YN").toString().equals("N")) {
				
				// 사용자 아이디와 저작권찾기의 동일한아이디인 경우.
				Map clmsUserIdnt = userService.findClmsUserIdnt1(params);	// CLMS 아이디 조회
				
				if(clmsUserIdnt != null) {
					
					params.put("userDivs", userInfoMap.get("USER_DIVS"));
					params.put("corpNumb", userInfoMap.get("CORP_NUMB"));
					params.put("resdCorpNumb", userInfoMap.get("RESD_CORP_NUMB"));
					
					params.put("userDivs", userInfoMap.get("USER_DIVS"));
					// 본인정보로 대상시스템에 회원정보가 존재하는지  Check
					if(userService.selectClmsUserInfo(params) == null) {
						newClmsChk = "Y";
					}
				}
			}
			
			ModelAndView mv = new ModelAndView("my/myInfoModi");
			
			mv.addObject("userInfoMap",userInfoMap);
			mv.addObject("newClmsChk", newClmsChk);
			
			return mv;
			
		}else{
			return new ModelAndView("my/myInfoDelt", "userInfoMap", userService.selectUserInfo(params));
		}
	}
	
	public ModelAndView updateUserInfo(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		Map params = new HashMap();
		
		String  divs = ServletRequestUtils.getStringParameter(request, "DIVS"); 
		System.out.println( divs );
		// 회원정보 수정
		if(divs.equals("U")){

			params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "mlsUserIdnt"));
			params.put("pswd", ServletRequestUtils.getStringParameter(request, "pswd"));
			params.put("mail", ServletRequestUtils.getStringParameter(request, "mail"));
			params.put("zipxNumb", ServletRequestUtils.getStringParameter(request, "zip"));
			params.put("addr", ServletRequestUtils.getStringParameter(request, "addr"));
			params.put("detlAddr", ServletRequestUtils.getStringParameter(request, "detlAddr"));
			params.put("telxNumb", ServletRequestUtils.getStringParameter(request, "telxNumb"));
			params.put("moblPhon", ServletRequestUtils.getStringParameter(request, "moblPhon"));
			params.put("mailReceYsno", ServletRequestUtils.getStringParameter(request, "mailReceYsno"));
			params.put("smsReceYsno", ServletRequestUtils.getStringParameter(request, "smsReceYsno"));
			params.put("submitChek", ServletRequestUtils.getStringParameter(request, "submitChek"));
			params.put("road_addr_yn", ServletRequestUtils.getStringParameter(request, "road_addr_yn"));
			
			
			// 기본정보
			Map userInfo = userService.selectUserInfo(params);
			
			params.put("userDivs", userInfo.get("USER_DIVS"));
			params.put("userName", userInfo.get("USER_NAME"));
			params.put("resdCorpNumb", userInfo.get("RESD_CORP_NUMB"));
			params.put("corpNumb",  userInfo.get("CORP_NUMB"));

			
			String userIdnt = ServletRequestUtils.getStringParameter(request, "mlsUserIdnt", "");
			String orgnPswd = ServletRequestUtils.getStringParameter(request, "orgnPswd", "");		// 기존 Pwd
			String pswd = ServletRequestUtils.getStringParameter(request, "pswd", "");					// 수정 Pwd
			
			String clmsAgrYn = ServletRequestUtils.getStringParameter(request, "clmsAgrYn", "");				// 기존동의여부
			String newClmsAgrYn = ServletRequestUtils.getStringParameter(request, "newClmsAgrYn", "");// 신규 동의여부
			String newClmsChk = ServletRequestUtils.getStringParameter(request, "newClmsChk", "");		// 대상시스템 해당아이디 사용유무
			String newClmsIdnt = ServletRequestUtils.getStringParameter(request, "userIdnt", "");			// 신규 clms ID
			String newClmsPswd = ServletRequestUtils.getStringParameter(request, "clmsPswd", "");			// 신규 clms Pwd
			
			String clmsUserIdnt = ServletRequestUtils.getStringParameter(request, "clmsUserIdnt", "");		// 기존 clms ID
			
			
			// ============ CLMS 통합로그인 동의여부 START 
			Map clmsInfo = null;
			
			//CLMS DI값을 생성
			String sSiteCode = "D747";			// IPIN 서비스 사이트 코드		(NICE신용평가정보에서 발급한 사이트코드)
			String sSitePw = "Yagins12";			// IPIN 서비스 사이트 패스워드	(NICE신용평가정보에서 발급한 사이트패스워드)
			
			String sJumin = (String) userInfo.get("RESD_CORP_NUMB");
			String sFlag = "JID";
			
			// 중복(가입)확인모듈 객체 생성
			VNOInterop vnoInterop = new VNOInterop();
			// Method 결과값(iRtn)에 따라, 프로세스 진행여부를 파악합니다.
			int iRtn = vnoInterop.fnRequest(sSiteCode, sSitePw, sJumin, sFlag);
			String sDupInfo ="";
			if (iRtn == 1) {
		    	// iRtn 값이 정상이므로, 귀사의 기획의도에 맞게 진행하시면 됩니다.
		    	// 아래와 같이 bstrDupInfo 함수를 통해 DI 값(64 Byte)을 추출할 수 있습니다.
			    sDupInfo = vnoInterop.getDupInfo();
			}else{
			    sDupInfo = "";
			}
			params.put("sDupInfo",sDupInfo);
			
			
			// (기존 동의회원의 경우)
			if( clmsAgrYn.equals("Y") && clmsUserIdnt.length()>0 && !orgnPswd.equals(pswd) ) {
			        clmsInfo = new HashMap();
				params.put("clmsAgrYn", clmsAgrYn);
				
				clmsInfo.put("rightAgrYn", clmsAgrYn);
				clmsInfo.put("userIdnt", userIdnt);
				clmsInfo.put("pswd", pswd);
				clmsInfo.put("clmsUserIdnt", clmsUserIdnt);
				
				// CLMS 회원정보.저작권찾기PSWD 수정
				userService.updateClmsUserInfo(clmsInfo);
				clmsInfo = userService.selectClmsUserInfo(params);
				params.put("clmsUserPswd", clmsInfo.get("PSWD"));
				
			}
			
			// (신규 동의회원의 경우)
			if( newClmsAgrYn.equals("Y")) {
			        clmsInfo = new HashMap();
				System.out.println("");
				System.out.println();
				System.out.println("***  CLMS 통합로그인 동의여부(신규) : 'Y' " );
				
				params.put("clmsAgrYn", newClmsAgrYn);
				params.put("rightAgrYn", newClmsAgrYn);
				
				// CLMS에 동일한 아이디가 사용되고 있어 새로운 아이디를 받는 이용자
				if(newClmsChk.equals("Y")) {
					
					params.put("fromRightYn", "Y");		// 출처 저작권 여부
					params.put("clmsUserIdnt", newClmsIdnt);		// CLMS 아이디
			    	params.put("clmsUserPswd", newClmsPswd);			// CLMS 패스워드
			    	
			    	userService.insertClmsUserInfo2(params);
			    	clmsInfo = userService.selectClmsUserInfo(params);
				}else { 

					// CLMS 에 동일 주민등록번호(사업자번호)로 가입정보 존재여부 확인.
					clmsInfo = userService.selectClmsUserInfo(params);
					
					if(clmsInfo != null) {
						System.out.println("");
						System.out.println("****  CLMS 에 동일 주민등록번호(사업자번호)로 가입정보 존재" );				
					
						// CLMS 에 존재하는경우
				    	params.put("clmsUserIdnt", clmsInfo.get("USER_IDNT"));		// CLMS 아이디
				    	params.put("clmsUserPswd", clmsInfo.get("PSWD"));			// CLMS 패스워드
				    	
				    	userService.updateClmsUserInfo(params);
				    	
					} else {
						System.out.println("");
						System.out.println("****  CLMS 에 동일 주민등록번호(사업자번호)로 가입정보 미존재" );				
					
						// CLMS 에 존지하지 않는경우
						params.put("fromRightYn", "Y");		// 출처 저작권 여부
						params.put("clmsUserIdnt", params.get("userIdnt"));		// CLMS 아이디
				    	params.put("clmsUserPswd", params.get("pswd"));			// CLMS 패스워드
				    	params.put("resdCorpNumb", userInfo.get("RESD_CORP_NUMB")); //주민번호 
				    	//params.put("clmsUserIdnt", params.get("userIdnt").toString().toUpperCase());		// CLMS 아이디
				    	//params.put("clmsUserPswd", params.get("pswd").toString().toUpperCase());			// CLMS 패스워드
						
				    	userService.insertClmsUserInfo(params);
				    	clmsInfo = userService.selectClmsUserInfo(params);
				    	
        				    	if(clmsInfo != null) {
        					    params.put("clmsUserPswd", clmsInfo.get("PSWD"));	
        					}else{
        					    params.put("clmsUserPswd", "");
        					}
					}
				}
				
			} // .. end 신규 동의
			// ============ CLMS 통합로그인 동의여부 END
			
			if(clmsInfo == null) {
			    params.put("clmsUserPswd", "");
			}
			
			params.put("resdCorpNumb", userInfo.get("RESD_CORP_NUMB"));
			
			params.put("pswd", pswd);
			userService.updateUserInfo(params);
			
			//userInfo = userService.selectUserInfo(params);
			
			User user = new User();
			user.setUserIdnt(userIdnt);
			user.setPswd(pswd);

			user = userLoginService.login(user);
			
			// 세션 set
			SessionUtil.setUserSession(request, user);
			
			// SSO 적용
			SessionUtil.setSSO(request, user);
			
		
			return new ModelAndView("redirect:/user/user.do?method=selectUserInfo&DIVS=U&userIdnt="+userIdnt);
			
		}else{
			
			String pswd = ServletRequestUtils.getStringParameter(request, "pswd").toUpperCase();
			String tmpPswd = ServletRequestUtils.getStringParameter(request, "tmpPswd");
			String sOutput_H        = null;
			sOutput_H	= xCrypto.Hash(6, pswd);//단방향 암호화(sOutput)
		
			if(sOutput_H.equals(tmpPswd)){
			
				Map params2 = new HashMap();
				params2.put("userIdnt", ServletRequestUtils.getStringParameter(request, "mlsUserIdnt"));
				params2.put("scssDesc", ServletRequestUtils.getStringParameter(request, "scssDesc"));
			
				HttpSession session = request.getSession();
//				session.removeAttribute("loginMember");
//				session.invalidate();
				session.removeAttribute("sessUserIdnt");
				session.removeAttribute("sessUserName");
				session.removeAttribute("sessSsoYn");
				session.removeAttribute("sessMail");
				session.removeAttribute("sessMoblPhon");
				session.removeAttribute("sessUserDivs");
		
				session.removeAttribute("sessClmsAgrYn");
				session.removeAttribute("sessClmsUserIdnt");
				session.removeAttribute("sessClmsPswd");
			
				// 20091027 로그분석기적용에 대한 쿠키적용

				Cookie killCookie = new Cookie("UID", null);
				killCookie.setPath("/");
				killCookie.setMaxAge(0);
					
				respone.addCookie(killCookie);
			
				userService.detlUserInfo(params2);
			
				ModelAndView mv = new ModelAndView("redirect:/main/main.do");
				return mv;
			}
			else {
				String pswdYsno = "N";
				params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt"));
				
				ModelAndView mv = new ModelAndView("my/myInfoDelt");
				mv.addObject("pswdYsno",pswdYsno);
				mv.addObject("submitChek2","N");
				mv.addObject("userInfoMap", userService.selectUserInfo(params));
				return mv;
			}
		}
	}
	
	public ModelAndView userPswdSrch(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		Map params = new HashMap();
		Map userMap = new HashMap();
		ModelAndView mv = null;
		
		String pswdSrch = ServletRequestUtils.getStringParameter(request, "pswdSrch");

		if ("Y".equals(pswdSrch)) {
			
			params.put("userDivs", ServletRequestUtils.getStringParameter(request, "userDivs_pwd"));
			params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt_pwd"));
			params.put("userName", ServletRequestUtils.getStringParameter(request, "userName_pwd"));
			params.put("resdCorpNumb", ServletRequestUtils.getStringParameter(request, "resdCorpNumb_pwd"));
			params.put("corpNumb", ServletRequestUtils.getStringParameter(request, "corpNumb_pwd"));
			params.put("pswdSrch", ServletRequestUtils.getStringParameter(request, "pswdSrch_pwd"));
			//params.put("mail", ServletRequestUtils.getStringParameter(request, "userEmail_pwd"));
			params.put("mgnbYsno", ServletRequestUtils.getStringParameter(request, "mgnbYsno_pwd"));
			params.put("dupInfo", ServletRequestUtils.getStringParameter(request, "dupInfo"));
			
			return new ModelAndView("ident/pswdRlst", "userMap", userService.userPswdSrch(params));
			
		}else {
			params.put("userDivs", ServletRequestUtils.getStringParameter(request, "userDivs"));
			params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt"));
			params.put("userName", ServletRequestUtils.getStringParameter(request, "userName"));
			params.put("resdCorpNumb", ServletRequestUtils.getStringParameter(request, "resdCorpNumb"));
			params.put("corpNumb", ServletRequestUtils.getStringParameter(request, "corpNumb"));
			params.put("pswdSrch", ServletRequestUtils.getStringParameter(request, "pswdSrch"));
			params.put("mgnbYsno", ServletRequestUtils.getStringParameter(request, "mgnbYsno"));
			params.put("commName", ServletRequestUtils.getStringParameter(request, "commName"));
			//params.put("mail", ServletRequestUtils.getStringParameter(request, "mail"));
			params.put("dupInfo", ServletRequestUtils.getStringParameter(request, "dupInfo"));
			
			
			//return new ModelAndView("ident/idntSrchRlst", "userMap", userService.userIdntPswdSrch(params));
			mv = new ModelAndView("ident/idntSrchRlst", "userMap", userService.userIdntPswdSrch(params));
			mv.addObject("pswdSrch", pswdSrch);  
			mv.addObject("idntSrch", "Y");  
		}
		
		return mv;
	}
	
	//개인회원 비밀번호 찾기(직접수정)
	public ModelAndView userPswdUpd(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		Map params = new HashMap();
		Map userMap = new HashMap();
		ModelAndView mv = null;
		
		
		String pswd = ServletRequestUtils.getStringParameter(request, "pswd", "");					// 수정 Pwd
		String userIdnt = ServletRequestUtils.getStringParameter(request, "userIdnt", "");
		
		userMap.put("pswd", pswd);
		userMap.put("userIdnt", userIdnt);
		userService.updateUserPswdInfo(userMap);
			
			return new ModelAndView("redirect:/user/user.do?method=goLogin");
			
	}
	
	public ModelAndView userPswdSrch2(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		Map params = new HashMap();
		Map userMap = new HashMap();
		ModelAndView mv = null;
		
		String pswdSrch = ServletRequestUtils.getStringParameter(request, "pswdSrch");

		if ("Y".equals(pswdSrch)) {
			
			params.put("userDivs", ServletRequestUtils.getStringParameter(request, "userDivs_pwd"));
			params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt_pwd"));
			params.put("userName", ServletRequestUtils.getStringParameter(request, "userName_pwd"));
			params.put("resdCorpNumb", ServletRequestUtils.getStringParameter(request, "resdCorpNumb_pwd"));
			params.put("corpNumb", ServletRequestUtils.getStringParameter(request, "corpNumb_pwd"));
			params.put("pswdSrch", ServletRequestUtils.getStringParameter(request, "pswdSrch_pwd"));
			//params.put("mail", ServletRequestUtils.getStringParameter(request, "userEmail_pwd"));
			params.put("mgnbYsno", ServletRequestUtils.getStringParameter(request, "mgnbYsno_pwd"));
			params.put("dupInfo", ServletRequestUtils.getStringParameter(request, "dupInfo"));
			
			String userIdnt = ServletRequestUtils.getStringParameter(request, "userIdnt_pwd");
			userMap.put("userIdnt", userIdnt);
			
			//if (userIdnt == "" || userIdnt == null){
			//	return new ModelAndView("redirect:/user/user.do?method=goLogin");
			//}else {
				return new ModelAndView("ident/pswdRlst2", "userMap", userService.userPswdSrch(userMap));
			//}
			
		}else {
			params.put("userDivs", ServletRequestUtils.getStringParameter(request, "userDivs"));
			params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt"));
			params.put("userName", ServletRequestUtils.getStringParameter(request, "userName"));
			params.put("resdCorpNumb", ServletRequestUtils.getStringParameter(request, "resdCorpNumb"));
			params.put("corpNumb", ServletRequestUtils.getStringParameter(request, "corpNumb"));
			params.put("pswdSrch", ServletRequestUtils.getStringParameter(request, "pswdSrch"));
			params.put("mgnbYsno", ServletRequestUtils.getStringParameter(request, "mgnbYsno"));
			params.put("commName", ServletRequestUtils.getStringParameter(request, "commName"));
			//params.put("mail", ServletRequestUtils.getStringParameter(request, "mail"));
			params.put("dupInfo", ServletRequestUtils.getStringParameter(request, "dupInfo"));
			
			
			//return new ModelAndView("ident/idntSrchRlst", "userMap", userService.userIdntPswdSrch(params));
			mv = new ModelAndView("ident/idntSrchRlst", "userMap", userService.userIdntPswdSrch(params));
			mv.addObject("pswdSrch", pswdSrch);  
			mv.addObject("idntSrch", "Y");  
		}
		
		return mv;
	}
	
	/**
	 * 문화예술협단체 관리자 회원가입 팝업
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public ModelAndView adminGoPop(HttpServletRequest request,
		HttpServletResponse respone) throws Exception {
	    return new ModelAndView("common/adminGoPop");
	}
	
		
		/**
		 * 관리자 아이디 중복체크
		 * 
		 * @param commandMap
		 * @throws Exception
		 */
		public void fncCheckId( HttpServletRequest request, HttpServletResponse response ) throws Exception{
			logger.debug( "fdcrAd84Update2 Start" );
			// 파라미터 셋팅
			Map params = new HashMap();
			Map userIdnt = new HashMap();
			
			//String USER_IDNT = EgovWebUtil.getString( params, "USER_IDNT" );
			String USER_IDNT = ServletRequestUtils.getStringParameter(request, "USER_IDNT");
			params.put( "userIdnt", USER_IDNT );
			
			int result = userService.chkDupLoginId(params);
			
			//userService.findUserIdnt(params);
			
			//System.out.println("USER_IDNT::"+USER_IDNT);
			response.setContentType( "application/json");
			
			PrintWriter out = response.getWriter();
			JSONObject obj = new JSONObject();

			obj.put("result",  result);
			out.print(obj);
			out.flush();
			out.close();
		}
		
		/**
		 * 기관명 검색 팝업
		 * 
		 * @param commandMap
		 * @throws Exception
		 */
		/*
		public ModelAndView fncSchCommName( HttpServletRequest request,
			HttpServletResponse respone ) throws Exception{
			// 파라미터 셋팅
			Map params = new HashMap();
			Map gubun = new HashMap();
			
			String GUBUN = ServletRequestUtils.getStringParameter(request, "GUBUN");
			params.put( "gubun", GUBUN );
			List list = userService.findSchCommName(params);
			System.out.println("GUBUN::"+GUBUN);
			
			return new ModelAndView("common/commNamePop", "gubunMap", gubun);
		}
		*/
		
		// 권리찾기 신청목적 선택 팝업
		public ModelAndView fncSchCommName(HttpServletRequest request,
		HttpServletResponse respone) throws Exception {
		
			Map params = new HashMap();
			Map gubun = new HashMap();
			
			String GUBUN = ServletRequestUtils.getStringParameter(request, "GUBUN");
			String COMM_NAME = ServletRequestUtils.getStringParameter(request, "COMM_NAME");
			params.put( "gubun", GUBUN );
			params.put( "comm_name", COMM_NAME );
			System.out.println("구분:::::::::::::::::::::::::::::::::::::::::"+GUBUN);
			System.out.println("업체명:::::::::::::::::::::::::::::::::::::::::"+GUBUN);
			
			List list = userService.findSchCommName(params);
			
			ModelAndView mv = null;
			
			mv = new ModelAndView("common/commNamePop", "", params);
			mv.addObject("gubun", gubun);
			mv.addObject("list", list);
			
			return mv;
		}
		
		
		/**
		 * 관리자 등록
		 * 
		 * @param commandMap
		 * @throws Exception
		 */
		public void insertAdmin( HttpServletRequest request, HttpServletResponse response ) throws Exception{
			// 파라미터 셋팅
			Map params = new HashMap();
			
			//String USER_IDNT = EgovWebUtil.getString( params, "USER_IDNT" );
			String COMM_NAME = ServletRequestUtils.getStringParameter(request, "COMM_NAME");
			if(COMM_NAME != "" && COMM_NAME != null){COMM_NAME = URLDecoder.decode( COMM_NAME, "UTF-8" );}
			String TRST_CEOX_NAME = ServletRequestUtils.getStringParameter(request, "TRST_CEOX_NAME");
			if(TRST_CEOX_NAME != "" && TRST_CEOX_NAME != null){TRST_CEOX_NAME = URLDecoder.decode( TRST_CEOX_NAME, "UTF-8" );}
			String TRST_TEXL_NUMB = ServletRequestUtils.getStringParameter(request, "TRST_TEXL_NUMB");
			String USER_NAME = URLDecoder.decode(ServletRequestUtils.getStringParameter(request, "USER_NAME"), "UTF-8");
			String USER_IDNT = ServletRequestUtils.getStringParameter(request, "USER_IDNT");
			String PSWD = ServletRequestUtils.getStringParameter(request, "PSWD");
			String MAIL = ServletRequestUtils.getStringParameter(request, "MAIL");
			//String MAIL2 = EgovWebUtil.getString( MAIL, "MAIL" );
			String MOBL_PHON = ServletRequestUtils.getStringParameter(request, "MOBL_PHON");
			String TRST_ORGN_DIVS_CODE = ServletRequestUtils.getStringParameter(request, "TRST_ORGN_DIVS_CODE");
			String TRST_ORGN_CODE = ServletRequestUtils.getStringParameter(request, "TRST_ORGN_CODE");
			
			
			System.out.println("USER_NAME  : " + USER_NAME);
			System.out.println( "COMM_NAME : " + COMM_NAME );
			
			MAIL =  MAIL.replace( "%40", "@" );
			
			System.out.println( "MAIL : " + MAIL );
			//System.out.println( "MAIL2 : " + MAIL2 );
			
			
			
			params.put("COMM_NAME", COMM_NAME);
			params.put("TRST_CEOX_NAME", TRST_CEOX_NAME);
			params.put("TRST_TEXL_NUMB", TRST_TEXL_NUMB);
			params.put("USER_NAME", USER_NAME);
			params.put("USER_IDNT", USER_IDNT);
			params.put("PSWD", PSWD);
			params.put("MAIL", MAIL);
			params.put("MOBL_PHON", MOBL_PHON);
			params.put("TRST_ORGN_DIVS_CODE", TRST_ORGN_DIVS_CODE);
			params.put("TRST_ORGN_CODE", TRST_ORGN_CODE);
			
			
			boolean result = userService.insertAdmin( params );

			if(result){
				response.setContentType( "application/json");
			
				PrintWriter out = response.getWriter();
				JSONObject obj = new JSONObject();

				obj.put("result",  "Y");
				out.print(obj);
				out.flush();
				out.close();
			} else {
				response.setContentType( "application/json");
				
				PrintWriter out = response.getWriter();
				JSONObject obj = new JSONObject();

				obj.put("result",  "N");
				out.print(obj);
				out.flush();
				out.close();
			}
		}
		
		public ModelAndView insertChildUser(HttpServletRequest request, HttpServletResponse response ) throws Exception {
			//System.out.println( "call inserChildUser " );
			return new ModelAndView("user/insertChildPage");
		}
		
		public ModelAndView identificationUser(HttpServletRequest request, HttpServletResponse response ) throws Exception {
			//System.out.println( "call identificationUser " );
			return new ModelAndView("user/identificationUser");
		}
		
		public ModelAndView identificationUser2(HttpServletRequest request, HttpServletResponse response) throws Exception {
			System.out.println( "call identificationUser " );
			// 파라미터 셋팅
			Map params = new HashMap();
			
			//String USER_IDNT = EgovWebUtil.getString( params, "USER_IDNT" );
			String userName = ServletRequestUtils.getStringParameter(request, "userName");
			System.out.println( "userName : " + userName);
			if(userName != "" && userName != null){userName = URLDecoder.decode( userName, "UTF-8" );}
			
			String dupInfo = ServletRequestUtils.getStringParameter(request, "dupInfo");
			String connInfo = ServletRequestUtils.getStringParameter(request, "connInfo");
			String birthDate = ServletRequestUtils.getStringParameter(request, "birthDate");
			
			
			String requestNumber = ServletRequestUtils.getStringParameter(request, "requestNumber");
			String responseNumber = ServletRequestUtils.getStringParameter(request, "responseNumber");
			String userName_pwd = ServletRequestUtils.getStringParameter(request, "userName_pwd");
			String userIdnt_pwd = ServletRequestUtils.getStringParameter(request, "userIdnt_pwd");
			String userEmail_pwd = ServletRequestUtils.getStringParameter(request, "userEmail_pwd");
			
			params.put("userName",userName);
			params.put("dupInfo",dupInfo);
			params.put("connInfo",connInfo);
			params.put("birthDate",birthDate);
			
		/*	System.out.println( "userName : " + userName);
			System.out.println( "dupInfo : " + dupInfo);
			System.out.println( "connInfo : " + connInfo);
			System.out.println( "birthDate : " + birthDate);
			System.out.println( "requestNumber : " + requestNumber);
			System.out.println( "responseNumber : " + responseNumber);
			System.out.println( "userName_pwd : " + userName_pwd);
			System.out.println( "userIdnt_pwd : " + userIdnt_pwd);
			System.out.println( "userEmail_pwd : " + userEmail_pwd);*/
			request.getSession().setAttribute( "userData",  params);
			
			return new ModelAndView("user/identificationUser2");
		}
		
		public ModelAndView goUserRegiChild(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
			System.out.println( "call goUserRegiChild" );
			
			Map params = new HashMap();
			
			//String USER_IDNT = EgovWebUtil.getString( params, "USER_IDNT" );
			/*String userName = ServletRequestUtils.getStringParameter(request, "userName");
			System.out.println( "userName : " + userName);
			if(userName != "" && userName != null){userName = URLDecoder.decode( userName, "UTF-8" );}
			
			String dupInfo = ServletRequestUtils.getStringParameter(request, "dupInfo");
			String connInfo = ServletRequestUtils.getStringParameter(request, "connInfo");
			String birthDate = ServletRequestUtils.getStringParameter(request, "birthDate");
			
			
			String requestNumber = ServletRequestUtils.getStringParameter(request, "requestNumber");
			String responseNumber = ServletRequestUtils.getStringParameter(request, "responseNumber");
			String userName_pwd = ServletRequestUtils.getStringParameter(request, "userName_pwd");
			String userIdnt_pwd = ServletRequestUtils.getStringParameter(request, "userIdnt_pwd");
			String userEmail_pwd = ServletRequestUtils.getStringParameter(request, "userEmail_pwd");
			
			params.put("userName",userName);
			params.put("dupInfo",dupInfo);
			params.put("connInfo",connInfo);
			params.put("birthDate",birthDate);*/
			
			
		return new ModelAndView("user/userRegiChild");
	    }
		
		public void parnetSetSession(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
			//request.getCharacterEncoding();
			//Map params = new HashMap();
			//String USER_IDNT = EgovWebUtil.getString( params, "parentName" );
			//System.out.println( "USER_IDNT : " + USER_IDNT );
			System.out.println( "call goUserRegiChild" );
			System.out.println( request.getAttribute( "parentName" )  );
			String parentName = ServletRequestUtils.getStringParameter(request, "parentName");
			String parentNumberFull = ServletRequestUtils.getStringParameter(request, "parentNumberFull");
			String decodeResult = URLDecoder.decode(parentName,"UTF-8");
			
			
			request.getSession().setAttribute( "decodeResult", decodeResult );
			request.getSession().setAttribute( "parentNumberFull", parentNumberFull );
			System.out.println( "decodeResult : " + decodeResult );
			System.out.println( "parentName : "+ parentName);
			System.out.println( "parentNumberFull : "+ parentNumberFull);
			boolean result = true;
			if(result){
				response.setContentType( "application/json");
			
				PrintWriter out = response.getWriter();
				JSONObject obj = new JSONObject();

				obj.put("result",  "Y");
				out.print(obj);
				out.flush();
				out.close();
			} else {
				response.setContentType( "application/json");
				
				PrintWriter out = response.getWriter();
				JSONObject obj = new JSONObject();

				obj.put("result",  "N");
				out.print(obj);
				out.flush();
				out.close();
			}
		//return "setSesstionOK";
	    }
		
		public ModelAndView userRegiCheckIndividual(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
			
			return new ModelAndView("user/userRegiCheckIndividual");
		}
}
