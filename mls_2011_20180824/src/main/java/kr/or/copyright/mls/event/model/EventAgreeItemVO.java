package kr.or.copyright.mls.event.model;

public class EventAgreeItemVO {

	
	private String 
	agree_item_cd
	,item_title
	,item_desc
	,event_id;

	public String getAgree_item_cd() {
		return agree_item_cd;
	}

	public void setAgree_item_cd(String agree_item_cd) {
		this.agree_item_cd = agree_item_cd;
	}

	public String getItem_title() {
		return item_title;
	}

	public void setItem_title(String item_title) {
		this.item_title = item_title;
	}

	public String getItem_desc() {
		return item_desc;
	}

	public void setItem_desc(String item_desc) {
		this.item_desc = item_desc;
	}

	public String getEvent_id() {
		return event_id;
	}

	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}

	@Override
	public String toString() {
		return "EventAgreeItemVO [agree_item_cd=" + agree_item_cd
				+ ", item_title=" + item_title + ", item_desc=" + item_desc
				+ ", event_id=" + event_id + "]";
	}
	
}
