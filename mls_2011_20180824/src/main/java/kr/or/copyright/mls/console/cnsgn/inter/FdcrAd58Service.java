package kr.or.copyright.mls.console.cnsgn.inter;

import java.util.ArrayList;
import java.util.Map;

public interface FdcrAd58Service {
	
	/**
	 * 저작물보고 등록 - 보고대상목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd58List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 저작물 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public void fdcrAd58List2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 일괄등록
	 */
	public boolean fdcrAd58Insert1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 일괄수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd58Update1( Map<String, Object> commandMap ) throws Exception;
}
