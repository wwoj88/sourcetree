package kr.or.copyright.mls.console.legal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.ExcelUtil;
import kr.or.copyright.mls.console.legal.inter.FdcrAd12Service;
import kr.or.copyright.mls.console.legal.inter.FdcrAd13Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 법정허락 > 법정허락 대상저작물 관리(일괄삭제)
 * 
 * @author ljh
 */
@Controller
public class FdcrAd13Controller extends DefaultController{

	@Resource( name = "fdcrAd13Service" )
	private FdcrAd13Service fdcrAd13Service;
	
	@Resource( name = "fdcrAd12Service" )
	private FdcrAd12Service fdcrAd12Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd13Controller.class );
	public static final int RETURN_TYPE_STRING = 1;
	
	/**
	 * 법정허락 대상저작물 관리(일괄삭제) 목록
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd13List1.page" )
	public String fdcrAd12List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		/*
		commandMap.put( "FROM_NO", "" );
		commandMap.put( "TO_NO", "" );
		commandMap.put( "BORD_CD", "" );
		commandMap.put( "TAB_INDEX", "" );
		*/
		//commandMap.put( "SCH_WORKS_DIVS_CD", "" );
		//commandMap.put( "SCH_STAT_OBJC_CD", "" );
		//commandMap.put( "SCH_WORKS_TITLE", "" );
		//commandMap.put( "SCH_LYRI_WRTR", "" );

		fdcrAd12Service.fdcrAd12List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		//model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		//model.addAttribute( "ds_count", commandMap.get( "ds_count" ) );
		System.out.println( "법정허락 대상저작물 관리(일괄삭제) 목록" );
		return "legal/fdcrAd13List1.tiles";
	}

	/**
	 * 법정허락 대상저작물 관리 목록(일괄삭제) 엑셀양식 첨부파일 다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd13Download1.page" )
	public void fdcrAd13Download1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		// 파라미터 셋팅
		commandMap.put( "FILE_URL", "" );
		String filePath = "";
		String fileName = "";

		fileName = "법정허락삭제항목업로드샘플.xlsx";
		filePath = "/home/right4me/web/upload/form/법정허락삭제항목업로드샘플.xlsx";

		if( null != fileName && !"".equals( fileName ) ){
			download( request, response, filePath, fileName );
		}
	}

	/**
	 * 법정허락 대상저작물 관리 목록(일괄삭제)엑셀양식 업로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd13Upload1.page" )
	public String fdcrAd13Upload1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		
		//담을 arrayList
		ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
		
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );
		
		String file1 = EgovWebUtil.getString( commandMap, "file1" );
		
		ArrayList<ArrayList<Map<String,Object>>> excelDataList = ExcelUtil.get(file1, RETURN_TYPE_STRING);
		ArrayList<Map<String,Object>> excelDataList1 = excelDataList.get(0);
		
		/* 반복문 시작 */
		for(int i=3; i<excelDataList1.size(); i++ ){
		Map<String,Object> excelDataList2 = excelDataList1.get(i);
		System.out.println("excelDataList의 행:"+excelDataList1.size());
		System.out.println("excelDataList의 열:"+excelDataList2.size());
		
		Set key = excelDataList2.keySet();
		  for (Iterator iterator = key.iterator(); iterator.hasNext();) {
               String keyName = (String) iterator.next();
               String valueName = (String) excelDataList2.get(keyName);
               System.out.println(keyName +" = " +valueName);
               commandMap.put( "valueName"+i, valueName );
		  }
          uploadList.add(excelDataList2);
		}
		model.addAttribute( "uploadList", uploadList );
		return "legal/fdcrAd13UploadForm1.tiles";
	}

	/**
	 * 법정허락 대상저작물 관리 목록(일괄삭제)일괄 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/legal/fdcrAd13Delete1.page" )
	public String fdcrAd13Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
		String WORKS_DIVS_CD = EgovWebUtil.getString( commandMap, "WORKS_DIVS_CD" );

		boolean isSuccess = fdcrAd13Service.fdcrAd13Delete1( commandMap );
		returnAjaxString( response, isSuccess );
		System.out.println( "법정허락 대상저작물 관리 목록(일괄삭제)일괄 삭제" );
		
		if( isSuccess ){
			return returnUrl(
				model,
				"저장했습니다.",
				"/console/legal/fdcrAd13List1.page");
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/legal/fdcrAd13List1.page");
		}
	}
}
