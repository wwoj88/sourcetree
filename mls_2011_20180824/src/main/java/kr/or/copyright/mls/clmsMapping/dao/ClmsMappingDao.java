package kr.or.copyright.mls.clmsMapping.dao;

import java.util.List;
import java.util.Map;

public interface ClmsMappingDao {
	
	public List kappListTotalRow(Map map);
	
	public List kappList(Map map);
	
	public List komcaList(Map map);
	
	public void icnUpdate(Map map);
	
	public List icnListTotalRow(Map map);
	
	public List icnList(Map map);
	
	public void icnDelete(Map map);
}
