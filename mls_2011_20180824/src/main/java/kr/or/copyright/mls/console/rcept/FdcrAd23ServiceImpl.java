package kr.or.copyright.mls.console.rcept;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd20Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd23Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd23Service" )
public class FdcrAd23ServiceImpl extends CommandService implements FdcrAd23Service{

	@Resource( name = "fdcrAd20Dao" )
	private FdcrAd20Dao fdcrAd20Dao;

	/**
	 * 미분배보상금 도서관 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd23List1( Map<String, Object> commandMap ) throws Exception{
		List pageList = (List) fdcrAd20Dao.totalRowinmtMgntLibrList( commandMap ); // 페이징카운트
		int totCnt = ((BigDecimal)((Map)pageList.get( 0 )).get("COUNT")).intValue();
		pagination( commandMap, totCnt, 0 );
		commandMap.put( "totCnt", totCnt );

		// DAO호출
		List list = (List) fdcrAd20Dao.inmtMgntLibrList( commandMap );
		//List pageList = (List) fdcrAd20Dao.totalRowinmtMgntLibrList( commandMap );

		//2017-11-02 스케줄러로 전환 미분배리스트 업데이트
		// 분배공고일자 기준 3년 지난 저작물 법정허락 업데이트
		//fdcrAd20Dao.updateLibrStatYn( commandMap );
		commandMap.put( "ds_List", list );
		//commandMap.put( "ds_page", pageList );

	}
}
