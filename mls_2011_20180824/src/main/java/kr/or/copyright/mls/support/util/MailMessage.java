package kr.or.copyright.mls.support.util;

import java.io.UnsupportedEncodingException;

import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;

public class MailMessage {

	// 기존(~2012) 메일템플릿
	public static StringBuffer getChangeInfoMailText(String info) throws UnsupportedEncodingException{
		StringBuffer sb = new StringBuffer();
		sb.append("<html>                                                                                                                      ");
		sb.append("<head>                                                                                                                      ");
		sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=euc-kr\" />                                                  ");
		sb.append("<title>"+Constants.FINDCOPYRIGHT_NAME +"사이트 메일링서비스</title>                                                                        ");
		sb.append("<link href=\""+Constants.DOMAIN_HOME+"/css/mail.css\" rel=\"stylesheet\" type=\"text/css\" />                                                        ");
		sb.append("</head>                                                                                                                     ");
		sb.append("<body>                                                                                                                      ");
		sb.append("<div id=\"mls\"> ");
		sb.append("<div id=\"logo\">저작권찾기</div> ");
		sb.append("<div id=\"url\"> <a href=\""+Constants.DOMAIN_HOME+"\" target=\"_blank\">"+Constants.DOMAIN_HOME+"</a><br />저작권도 찾고! 보상도 받고!</div> ");
		sb.append("<div class=\"mail_body\"> ");
		sb.append(info);	// 메일 정보
		sb.append("	<div id=\"button_default\"> <a href=\""+Constants.DOMAIN_HOME+"\" target=\"_blank\">"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("findCopyRight_name").getBytes("ISO-8859-1"), "EUC-KR")+" 사이트 바로가기</a></div> ");
		sb.append("</div>");
		sb.append("<div id=\"mail_bottom\">copyright 2009 "+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("findCopyRight_name").getBytes("ISO-8859-1"), "EUC-KR")+". All rights reserved.</div> ");
		sb.append("</div>");
		sb.append("</body>");
		sb.append("</html>");
		

		
		return sb;
	}
	
	// 기존(~2012) 메일템플릿
	public static StringBuffer getChangeOrgnInfoMailText(MailInfo info ){
		
		StringBuffer sb = new StringBuffer();
		sb.append("<html>                                                                                                                      ");
		sb.append("<head>                                                                                                                      ");
		sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=euc-kr\" />                                                  ");
		sb.append("<title>저작권찾기 사이트 메일링서비스</title>                                                                        ");
		sb.append("<link href=\""+Constants.DOMAIN_HOME+"/css/mail.css\" rel=\"stylesheet\" type=\"text/css\" />                                                        ");
		sb.append("</head>                                                                                                                     ");
		sb.append("<body>                                                                                                                      ");
		sb.append("<div id=\"mls\"> ");
		sb.append("<div id=\"logo\">저작권찾기</div> ");
		sb.append("<div id=\"url\"> <a href=\""+Constants.DOMAIN_HOME+"\" target=\"_blank\">"+Constants.DOMAIN_HOME+"</a><br />저작권도 찾고! 보상도 받고!</div> ");
		sb.append("<div class=\"mail_body\"> ");
		
		//sb.append(info);	// 메일 정보
		
	    sb.append("	<div class=\"mail_title\"> 저작권찾기신청이 완료되었습니다. </div>");
	    sb.append("	<div class=\"mail_contents\"> ");
	    sb.append("		<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
	    sb.append("			<tr> ");
	    sb.append("				<td> ");
	    sb.append("					<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");
// 변경부분
	    sb.append("						<tr> ");
	    sb.append("							<td class=\"tdLabel\">신청 저작물명</td> ");
	    sb.append("							<td class=\"tdData\">["+info.getPrpsDivsName()+"]"+ info.getPrpsTitle()+"</td> ");
	    sb.append("						</tr> ");
	    
	    sb.append("						<tr> ");
	    sb.append("							<td class=\"tdLabel\">신청자 정보</td> ");
	    sb.append("							<td class=\"tdData\">이름 : "+info.getPrpsName()+"  (email: "+info.getPrpsEmail()+", mobile: "+info.getPrpsSms()+")</td> ");
	    sb.append("						</tr> ");
	    
	    sb.append("					</table> ");
// 변경부분
	    sb.append("					</table> ");
	    sb.append("				</td> ");
	    sb.append("			</tr> ");
	    sb.append("		</table> ");
	    sb.append("	</div> ");
	    
		sb.append("	<div id=\"button_default\"> <a href=\""+Constants.DOMAIN_HOME+"\" target=\"_blank\">저작권찾기 사이트 바로가기</a></div> ");
		sb.append("</div>");
		sb.append("<div id=\"mail_bottom\">copyright 2009 저작권찾기. All rights reserved.</div> ");
		sb.append("</div>");
		sb.append("</body>");
		sb.append("</html>");
		

		
		return sb;
	}
	
	// 2013년 메일템플릿
	public static StringBuffer getChangeInfoMailText2(MailInfo info ){
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">    \n");
		sb.append("<html lang=\"ko\">    \n");
		sb.append("<head>    \n");
		sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=euc-kr\">    \n");
		sb.append("<title>저작권찾기 사이트 메일링서비스</title>     \n");
		sb.append("<link href=\"http://fonts.googleapis.com/earlyaccess/nanumgothic.css\" rel=\"stylesheet\" type=\"text/css\">    \n");
		sb.append("<style type=\"text/css\">    \n");
		sb.append("@font-face{font-family:NG;src:url(NanumGothic.eot);src:local(※),url(NanumGothic.woff) format('woff')}    \n");
		sb.append("@font-face{font-family:NGB;src:url(NanumGothicBold.eot);src:local(※),url(NanumGothicBold.woff) format('woff')}    \n");
		sb.append("@font-face{font-family:NGEB;src:url(NanumGothicExtraBold.eot);src:local(※),url(NanumGothicExtraBold.woff) format('woff')}    \n");
		sb.append("</style>    \n");
		sb.append("</head>    \n");
		sb.append("<body style=\"padding: 0; margin: 0; font-size: 12px; font-family: dotum; color: #333; line-height:1.8; padding-bottom:40px;\" >    \n");
		sb.append("		<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" summary=\"메일템플릿\" width=\"660\" align=\"center\"     \n");
		sb.append("		style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/top.gif) no-repeat 0 0; margin-top:40px; border:1px solid #ccc;\">    \n");
		sb.append("			<tbody>    \n");
		sb.append("				<tr>    \n");
		sb.append("					<td height=\"33\" width=\"517\" style=\"padding-left:18px;\"><img src=\""+Constants.DOMAIN_HOME+"/images/2012/mail/top_logo.png\" alt=\"저작권찾기 사이트\"/></td>    \n");
		sb.append("					<td><img src=\""+Constants.DOMAIN_HOME+"/images/2012/mail/top_url.png\" alt=\"www.findcopyright.or.kr\"/></td>    \n");
		sb.append("				</tr>    \n");
		sb.append("				<tr>    \n");
		sb.append("					<td colspan=\"2\" height=\"200\" style=\"vertical-align:top;\">    \n");
		sb.append("						<p style=\"margin:0; padding:30px 0 0 32px\"><img src=\""+Constants.DOMAIN_HOME+"/images/2012/mail/top_title.gif\" alt=\"저작권찾기 사이트\"/></p>    \n");
		sb.append("						<p style=\"font-family:'나눔고딕', NanumGothic, sans-serif; font-size:36px; font-weight:bold; color:#0062bd; margin:0; padding:10px 0 0 45px; line-height:1.3;\">저작권찾기 사이트<br>    \n");
		sb.append("							<span style=\"font-size:32px; color:#333;\">안내메일</span>    \n");
		sb.append("						</p>    \n");					
		sb.append("					</td>    \n");			
		sb.append("				</tr>    \n");
		sb.append("				<tr>    \n");
		sb.append("					<td colspan=\"2\" height=\"127\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/ico_mail.gif) no-repeat 32px 3px;\">    \n");
		sb.append("						<p style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dotted.gif) repeat-x 0 100%; margin:0; padding:43px 0 43px 173px;\">    \n");
		sb.append("						<span style=\"font-size:16px; font-weight:bold;\">"+info.getSubject()+"</span><br/>    \n");
		if("N".equals(info.getDetlYn())) {
			sb.append("						<span style=\"font-size:12px;\">회원가입 정보는 <span style=\"font-weight:bold; color:#008dbc;\">저작권찾기 사이트</span>에서 조회 가능합니다.</span>     \n");
		}
		else	
			sb.append("						<span style=\"font-size:12px;\">상세정보는 <span style=\"font-weight:bold; color:#008dbc;\">저작권찾기 사이트</span>에서 가능합니다.</span>     \n");
		sb.append("						<a href=\""+Constants.DOMAIN_HOME+"\" style=\"vertical-align:middle;\"><img src=\""+Constants.DOMAIN_HOME+"/images/2012/mail/btn_go.gif\" alt=\"저작권찾기 사이트 바로가기\" style=\"border:none;\"/></a>    \n");
		sb.append("					</p>    \n");
		sb.append("					</td>    \n");
		sb.append("				</tr>    \n");
		// content
		sb.append(info.getMessage());
		// Footer //
		sb.append("				<tr>    \n");
		sb.append("					<td colspan=\"2\" height=\"128\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; background:#eaf1f7;\">    \n");
		sb.append("						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"font-size:11px; color:#777; line-height:1.6;\">    \n");
		sb.append("							<tbody>    \n");
		sb.append("								<tr>    \n");
		sb.append("									<td rowspan=\"2\" width=\"169\"><img src=\""+Constants.DOMAIN_HOME+"/images/2012/mail/bot_logo.gif\" alt=\"한국저작권위원회\" style=\"padding-left:25px;\"/></td>    \n");
		sb.append("									<td style=\"padding:0 0 10px 5px;\">    \n");
		sb.append("										본 메일은 발신전용으로 회신되지 않습니다.    \n");
		
		// 수신거부 setting
		if(info.getMailRejcCd()!=null && info.getMailRejcCd().length()>0 ){
			sb.append("									<br/>메일 수신을 원하지 않을 경우 <a href=\""+Constants.DOMAIN_HOME+"/mail/insertRejcForm.do?rejc="+info.getMailRejcCd()+"&addr="+info.getEmail()+"\" style=\"color:#008dbc;\">수신거부</a>를 클릭하시기 바랍니다.    \n");
			/*sb.append("									<br/>메일 수신을 원하지 않을 경우 <a href=\"http://new.right4me.or.kr/mail/insertRejcForm.do?rejc="+info.getMailRejcCd()+"&addr="+info.getEmail()+"\" style=\"color:#008dbc;\">수신거부</a>를 클릭하시기 바랍니다.    \n");*/	}
		
		sb.append("									</td>    \n");
		sb.append("								</tr>    \n");
		sb.append("								<tr>    \n");
		//sb.append("									<td style=\"padding-right:15px;\"><p style=\"margin:0; border-top:1px solid #fff; padding:10px 0 0 5px;\"><img src=\""+Constants.DOMAIN_HOME+"/images/2012/mail/address.gif\" " +	"alt=\"52852 경상남도 진주시 충의로 19(LH공사 1,3,5층) 한국저작권 위원회 대표전화 :  055-792-0129\"/></p>    \n");		
		sb.append("									<td style=\"padding-right:15px;\"><p style=\"margin:0; border-top:1px solid #fff; padding:10px 0 0 5px;\">52852 경상남도 진주시 충의로 19(LH공사 1,3,5층) 한국저작권 위원회 대표전화 :  055-792-0125</p>    \n");		
		sb.append("									"+info.getMailReadHtml());
		sb.append("									</td>    \n");
		sb.append("								</tr>    \n");
		sb.append("							</tbody>    \n");
		sb.append("						</table>    \n");
		sb.append("					</td>    \n");
		sb.append("				</tr>    \n");
		//Footer END
		sb.append("			</tbody>    \n");
		sb.append("		</table>    \n");
		sb.append("	</body>    \n");
		sb.append("</html>    \n");
		
		return sb;
	}
}
