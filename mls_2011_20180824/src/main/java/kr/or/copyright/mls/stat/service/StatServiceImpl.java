package kr.or.copyright.mls.stat.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.or.copyright.mls.stat.dao.StatDAO;
import kr.or.copyright.mls.stat.model.MlStatWorksDefaultVO;
import kr.or.copyright.mls.stat.model.MlStatWorksVO;

@Service(value="StatService")
public class StatServiceImpl implements StatService{
    
    @Resource(name="StatDAO")
    private StatDAO statDAO;

    public List<MlStatWorksVO> selectMlStatWorksList(
	    MlStatWorksDefaultVO searchVO) throws Exception {
	return statDAO.selectMlStatWorksList(searchVO);
    }

    public List<MlStatWorksVO> checkSelectMlStatWorksList(
	    MlStatWorksDefaultVO searchVO) throws Exception {
	return statDAO.checkSelectMlStatWorksList(searchVO);
    }

    public int selectMlStatWorksListTotCnt(MlStatWorksDefaultVO searchVO) {
	return statDAO.selectMlStatWorksListTotCnt(searchVO);
    }

}
