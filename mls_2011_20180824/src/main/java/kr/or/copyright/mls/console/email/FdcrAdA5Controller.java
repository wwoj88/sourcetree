package kr.or.copyright.mls.console.email;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 안내메일관리 > 메시지관리
 * 
 * @author ljh
 */
@Controller
public class FdcrAdA5Controller extends DefaultController{

	@Resource( name = "fdcrAdA5Service" )
	private FdcrAdA5ServiceImpl fdcrAdA5Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA5Controller.class );

	/**
	 * 메시지 관리 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA5List1.page" )
	public String fdcrAdA5List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "MSG_STOR_CD", "1" );

		fdcrAdA5Service.fdcrAdA5List1( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "메시지 관리 목록 조회" );
		return "email/fdcrAdA5List1.tiles";
	}

	/**
	 * 메시지 삭제
	 * 
	 * @param request
	 * @param commandMap
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA5Delete1.page" )
	public String fdcrAdA5Delete1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String[] MSG_IDS = request.getParameterValues( "MSG_ID" );
		commandMap.put( "MSG_ID", MSG_IDS );

		boolean isSuccess = fdcrAdA5Service.fdcrAdA5Delete1( commandMap );
		
		logger.debug( "메시지 삭제" );
		if( isSuccess ){
			return returnUrl(
				model,
				"삭제했습니다.",
				"/console/email/fdcrAdA5List1.page");
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/email/fdcrAdA5List1.page");
		}
	}

	/**
	 * 메시지 관리 상세 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA5View1.page" )
	public String fdcrAdA5View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "MSG_STOR_CD", "" );

		fdcrAdA5Service.fdcrAdA5View1( commandMap );

		model.addAttribute( "ds_mail", commandMap.get( "ds_mail" ) );
		model.addAttribute( "ds_send2", commandMap.get( "ds_send2" ) );
		model.addAttribute( "ds_detail", commandMap.get( "ds_detail" ) );

		logger.debug( "메시지 관리 상세 조회" );
		return "email/fdcrAdA5View1.tiles";
	}

	/**
	 * 메시지 수정
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA5Update1.page" )
	public void fdcrAdA5Update1( ModelMap model,
		Map<String, Object> commandMap,
		MultipartHttpServletRequest mreq,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		String[] RECV_CDS = request.getParameterValues( "RECV_CD" );
		String[] RECV_SEQNS = request.getParameterValues( "RECV_SEQN" );
		String[] RECV_NAMES = request.getParameterValues( "RECV_NAME" );
		String[] RECV_PERS_IDS = request.getParameterValues( "RECV_PERS_ID" );
		String[] RECV_MAIL_ADDRS = request.getParameterValues( "RECV_MAIL_ADDR" );
		String[] RECV_GROUP_IDS = request.getParameterValues( "RECV_GROUP_ID" );
		String[] RECV_GROUP_NAMES = request.getParameterValues( "RECV_GROUP_NAME" );
		String[] FILE_NAMES = request.getParameterValues( "FILE_NAME" );
		String[] FILE_PATHS = request.getParameterValues( "FILE_PATH" );
		String[] FILE_SIZES = request.getParameterValues( "FILE_SIZE" );
		String[] REAL_FILE_NAMES = request.getParameterValues( "REAL_FILE_NAME" );
		String[] ATTC_SEQNS = request.getParameterValues( "ATTC_SEQN" );
		String[] RGST_IDNTS = request.getParameterValues( "RGST_IDNT" );

		commandMap.put( "MSG_ID", "" );
		commandMap.put( "GUBUN", "" );
		commandMap.put( "MSG_STOR_CD", "" );
		commandMap.put( "TITLE", "" );
		commandMap.put( "CONTENT", "" );
		commandMap.put( "RGST_IDNT", "" );

		commandMap.put( "RECV_CD", RECV_CDS );
		commandMap.put( "RECV_SEQN", RECV_SEQNS );
		commandMap.put( "RECV_NAME", RECV_NAMES );
		commandMap.put( "RECV_PERS_ID", RECV_PERS_IDS );
		commandMap.put( "RECV_MAIL_ADDR", RECV_MAIL_ADDRS );
		commandMap.put( "RECV_GROUP_ID", RECV_GROUP_IDS );
		commandMap.put( "RECV_GROUP_NAME", RECV_GROUP_NAMES );
		commandMap.put( "FILE_NAME", FILE_NAMES );
		commandMap.put( "FILE_PATH", FILE_PATHS );
		commandMap.put( "FILE_SIZE", FILE_SIZES );
		commandMap.put( "REAL_FILE_NAME", REAL_FILE_NAMES );
		commandMap.put( "ATTC_SEQN", ATTC_SEQNS );
		commandMap.put( "RGST_IDNT", RGST_IDNTS );

		// 파일 업로드
		String uploadPath =
			new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		ArrayList<Map<String, Object>> fileinfo = fileUpload( mreq, uploadPath, null, true );

		boolean isSuccess = fdcrAdA5Service.fdcrAdA5Update1( commandMap, fileinfo );
		returnAjaxString( response, isSuccess );
		logger.debug( "메시지 수정하기" );
	}

}
