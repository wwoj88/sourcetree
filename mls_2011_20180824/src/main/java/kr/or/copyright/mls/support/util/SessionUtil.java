package kr.or.copyright.mls.support.util;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;import org.aspectj.weaver.JoinPointSignature;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.web.servlet.ModelAndView;

import com.bandi.oauth.BandiSSOAgent;
import com.softforum.xdbe.xCrypto;

import SafeSignOn.SSO;
import kr.or.copyright.common.userLogin.controller.BanSSO;
import kr.or.copyright.common.userLogin.controller.TestSSOValid;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.common.model.TransUserDTO;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
public class SessionUtil {

	public static void setUserSession(HttpServletRequest request, User user) {

		//TODO 암호화주석제거
		//    String sDecoded7 = user.getSsnNo();
		String sDecoded7 = null;



		HttpSession session = request.getSession(true);

		session.setAttribute("sessUserIdnt", user.getUserIdnt());
		session.setAttribute("sessPswd", user.getPswd());
		session.setAttribute("sessUserName", user.getUserName());
		session.setAttribute("sessUserDivs", user.getUserDivs());
		session.setAttribute("sessMail", user.getMail());
		session.setAttribute("sessSsoYn", user.getSso_loginyn());
		session.setAttribute("sessMoblPhon", user.getMoblPhon());

		session.setAttribute("sessClmsAgrYn", user.getClmsAgrYn());
		session.setAttribute("sessClmsUserIdnt", user.getClmsUserIdnt());
		session.setAttribute("sessClmsPswd", user.getClmsPswd());

		session.setAttribute("sessCrnNo", user.getCrnNo());

		if (sDecoded7 != null && !sDecoded7.equals("")) {
			session.setAttribute("sessSsbNo", sDecoded7.trim());
		}
		session.setAttribute("sessUserAddr", user.getUserAddr());
		request.setAttribute("userInfoDTO", user);
	}


	//임시세션 생성 setUserSession_cert
	// 2012-11-26
	public static void setUserSession_cert(HttpServletRequest request, User user) {


		HttpSession session = request.getSession(true);

		session.setAttribute("m_user_id", user.getUserIdnt());
		session.setAttribute("m_user_name", user.getUserName());
	}

	public static User getSession(HttpServletRequest request) {

		HttpSession session = request.getSession(false);
		User user = new User();
		if (session == null) {
			return null;
		}
		String access_token = (String)request.getSession(true).getAttribute("access_token");
		
		if(access_token==null) {


			user.setUserIdnt((String) session.getAttribute("sessUserIdnt"));
			user.setPswd((String) session.getAttribute("sessPswd"));
			user.setUserName((String) session.getAttribute("sessUserName"));
			user.setMgnbYsno((String) session.getAttribute("sessMgnbYsno"));
			user.setMail((String) session.getAttribute("sessMail"));
			user.setMoblPhon((String) session.getAttribute("sessMoblPhon"));
			user.setUserDivs((String) session.getAttribute("sessUserDivs"));

			user.setClmsAgrYn((String) session.getAttribute("sessClmsAgrYn"));
			user.setClmsUserIdnt((String) session.getAttribute("sessClmsUserIdnt"));
			user.setClmsPswd((String) session.getAttribute("sessClmsPswd"));

			user.setCrnNo((String) session.getAttribute("sessCrnNo"));
			user.setSsnNo((String) session.getAttribute("sessSsbNo"));

			user.setUserAddr((String) session.getAttribute("sessUserAddr"));

			return user;
		}else {
			try {

				TestSSOValid.main( request );
					
				user.setUserIdnt((String) session.getAttribute("sessUserIdnt"));
				user.setPswd((String) session.getAttribute("sessPswd"));
				user.setUserName((String) session.getAttribute("sessUserName"));
				user.setSso_loginyn( (String) session.getAttribute("sessSsoYn"));
				user.setMail((String) session.getAttribute("sessMail"));
				user.setMoblPhon((String) session.getAttribute("sessMoblPhon"));
				user.setUserDivs((String) session.getAttribute("sessUserDivs"));
				user.setSso_loginyn( (String) session.getAttribute("sessSsoYn"));
				user.setClmsAgrYn((String) session.getAttribute("sessClmsAgrYn"));
				user.setClmsUserIdnt((String) session.getAttribute("sessClmsUserIdnt"));
				user.setClmsPswd((String) session.getAttribute("sessClmsPswd"));

				user.setCrnNo((String) session.getAttribute("sessCrnNo"));
				user.setSsnNo((String) session.getAttribute("sessSsbNo"));

				user.setUserAddr((String) session.getAttribute("sessUserAddr"));
				
				user.setAccessToken( (String) session.getAttribute("access_token") );
				user.setRefreshToken((String) session.getAttribute("refresh_token")  );
				
			}catch(Exception e){

				return null;
			}

		}

		return user;
	}

	public static User getSSO2(HttpServletRequest request) throws Exception{

		HttpSession session = request.getSession(true);
		String type = "1";
		String access_token = (String)request.getSession(true).getAttribute("access_token");
		String ssoUri = "https://sso.copyright.or.kr/oauth2/token.do";  //개발 
	  //String ssoUri = "https://devsso.copyright.or.kr/oauth2/token.do";
		String clientId = "fa48f99639b2418c956c1642d8e8e02e";
		String clientSecret = "8joqm4lgs11otg0ah8gz7nydp";
		String scope = "http://sso.copyright.or.kr";						// 고정



		User user = new User();
		
		BandiSSOAgent agent = new BandiSSOAgent(ssoUri, clientId, clientSecret, scope);
		//BanSSO agent = new BanSSO(ssoUri, clientId, clientSecret, scope);
		String client_ip =  agent.getLocalServerIp(request);
		try {

			TestSSOValid.main( request );

		}catch(Exception e){
			 e.printStackTrace();
			return user;
		}

		//String client_ip =agent.getLocalServerIp(request);
		//System.out.println("GET SSO2 TOKEN ::"+access_token  );
		try{
			HashMap<String, String> userInfo = agent.userInfo(access_token, client_ip, type);

			Object[] keys = (Object[]) userInfo.keySet().toArray();
				for(int i=0;i<userInfo.size();i++) {
					//System.out.println("["+keys[i]+"] "+userInfo.get(keys[i]));
					user.setUserIdnt( userInfo.get("siteMembId") );
				  
					if(userInfo.get("siteMembId")==null || userInfo.get("siteMembId").equals( "" )) {
						
						user.setUserIdnt( userInfo.get("membId") );
						user.setUserName( userInfo.get( "membName" ) );
						user.setMail( userInfo.get( "membEmail" ) );
						
						
					}
			
			}
		
				HashMap<String, String> result = new HashMap<String, String>();
				Object object=null;
				JSONArray arrayObj=null;
				JSONParser jsonParser=new JSONParser();
				if(userInfo.get("corpUserInfo") !=null) {
				object=jsonParser.parse(userInfo.get("corpUserInfo"));
		
				arrayObj=(JSONArray) object;

				
				for (Object key : ((HashMap) arrayObj.get(0)).keySet()) {
             result.put(key.toString(), ((HashMap) arrayObj.get(0)).get(key).toString());
        }
				}
			  // 필요하면 끌어다씀 법인정보 
				
		}
		catch( Exception e ){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		return user;

	}




	public static void setSSO(HttpServletRequest request, User user) {

		if (user.getClmsUserIdnt() != null && user.getClmsPswd() != null) {

			int Result = -1;
			String SSOAPIKEY = "368B184727Y89AB69FAZ";
			SSO sso = new SSO(SSOAPIKEY);
			sso.setPortNumber(7000);

			int SSOTokenVersion = 3;
			String UID = user.getUserIdnt();
			String SSOAgentID = "CLMS_SSO";
			String UCIP = request.getRemoteAddr();

			sso.putValue("user_id2", user.getClmsUserIdnt());
			sso.putValue("user_pwd2", user.getClmsPswd());

			String sToken = sso.makeSimpleToken(SSOTokenVersion, UID,
				SSOAgentID, UCIP);
			System.out.println("[1] :: "+sToken);

			sso.verifyToken(sToken);
			String sToken1 = sso.getToken();
			System.out.println("[2] :: "+sToken1);

			sso.verifyToken(sToken);
			Result = sso.getLastError();


			if(Result < 0){ System.out.println("[FAIL] : "+Result); }else{
				System.out.println("[SUCCESS] : "+Result);
				System.out.println("[SUCCESS] : "+sso.getValue("user_id2"));
				System.out.println("[SUCCESS] : "+sso.getValue("user_pwd2")); }


			if (sToken == null || sToken.equals("") || sToken.length() < 0) {
				System.out.println("[Fail] : "+Result);
			} else {
				request.getSession(true).setAttribute("sToken", sToken);
			}
		}
	}
}
