package kr.or.copyright.mls.statBord.model;

public class AnucBordAttcFile {
	private int attcSeqn;				//첨부파일 순번
	private long bordCd;					//게시판 구분CD
	private long bordSeqn;				//게시판 순번
	private String rgstIdnt;			//등록자 id
	private String rgstDttm;			//등록일자
	@Override
	public String toString() {
		return "AnucBordAttcFile [attcSeqn=" + attcSeqn + ", bordCd=" + bordCd
				+ ", bordSeqn=" + bordSeqn + ", rgstIdnt=" + rgstIdnt
				+ ", rgstDttm=" + rgstDttm + "]";
	}
	public int getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(int attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public long getBordCd() {
		return bordCd;
	}
	public void setBordCd(long bordCd) {
		this.bordCd = bordCd;
	}
	public long getBordSeqn() {
		return bordSeqn;
	}
	public void setBordSeqn(long bordSeqn) {
		this.bordSeqn = bordSeqn;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
	
	
}
