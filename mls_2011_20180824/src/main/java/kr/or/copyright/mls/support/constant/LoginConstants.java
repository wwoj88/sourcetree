package kr.or.copyright.mls.support.constant;

/**
 * 로그인 관련 상수 정의
 * 
 * @author 이상유
 * @version $Id: LoginConstants.java,v 1.1 2012/07/02 01:05:48 yagin14 Exp $
 */
public class LoginConstants {
	
	public static final String LOGIN_SESSION_ATTRIBUTE = "User";	// Login Session 속성 정보명
	public static final String LOGIN_PAGE_ATTRIBUTE = "loginPage";	// Login Page 속성 정보명
	
	public static final String USER_PAGE = "user";	// USER 페이지
//	public static final String ADMIN_PAGE = "admin";	// 관리자 페이지
//	public static final String MUSIC_PAGE = "music"; 	// 음악 페이지
//	public static final String LITER_PAGE = "liter";	// 어문 페이지
	
	public static final String USER_LOGIN_MAIN_PAGE = "redirect:/main/main.do";	// USER 메인 페이지
//	public static final String ADMIN_LOGIN_MAIN_PAGE = "redirect:/admin/main.do";	// 관리자 메인 페이지
//	public static final String MUSIC_LOGIN_MAIN_PAGE = "redirect:/user/music/main_music.do";	// 음악 메인 페이지
//	public static final String LITER_LOGIN_MAIN_PAGE = "redirect:/user/liter/main_liter.do";	// 어문 메인 페이지
//	public static final String LOGIN_SUCCESS_REDIRCT_PAGE = "redirect:/user/userLogInSuccess.do";	// 로그인 성공 페이지
	
	public static final String USER_MAIN_PAGE = "/main/main.do";	// USER 메인 페이지
//	public static final String MUSIC_MAIN_PAGE = "/user/music/main_music.do";	// 음악 메인 페이지
//	public static final String LITER_MAIN_PAGE = "/user/liter/main_liter.do";	// 어문 메인 페이지
//	public static final String ADMIN_MAIN_PAGE = "/admin/main.do";	// 관리자 메인 페이지
	
//	public static final String LOGIN_SUCCESS_PAGE = "/user/userLogInSuccess.do";	// 로그인 성공 페이지
	public static final String LOGIN_SUCCESS_PAGE = "/main/main.do";	// 로그인 성공 페이지
	
	public static final String CERT_LOGIN_CD_SSN = "ssn";	// 공인인증서 로그인 구분 : 개인(ssn)
	public static final String CERT_LOGIN_CD_CRN = "crn";	// 공인인증서 로그인 구분 : 사업자(crn)
	
	public static final String MEMBER_TYPE_1 = "1";	// 사용자 유형(MEMBER_TYPE) : 개인 (1)
	public static final String MEMBER_TYPE_2 = "2";	// 사용자 유형(MEMBER_TYPE) : 법인사업자 (2)
	public static final String MEMBER_TYPE_3 = "3";	// 사용자 유형(MEMBER_TYPE) : 개인사업자 (3)
	public static final String MEMBER_TYPE_4 = "4";	// 사용자 유형(MEMBER_TYPE) : 임의단체(학회등) (4)
	public static final String MEMBER_TYPE_5 = "5";	// 사용자 유형(MEMBER_TYPE) : 외국인 (5)
	
}
