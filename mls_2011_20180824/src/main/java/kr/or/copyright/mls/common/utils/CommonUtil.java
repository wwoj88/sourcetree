/* ********************************************************************************
 * 1.클래스   명 : StringUtil.java
 * 2.클래스 개요 :
 * 3.관련 테이블 :
 * 4.관련 클래스 :
 * 5.관련 페이지 :
 * 6.작   성  자 : 강명표
 * 7.작 성 일 자 : 2004-03-30
 * 8.수 정 일 자 :
 * *****************************************************************************
 */

package kr.or.copyright.mls.common.utils;

//import org.tipa.yap.code.CodeManager;
import java.util.StringTokenizer;

public class CommonUtil 
{
	private CommonUtil() 
	{

	}

	public static String setNewImage(int dayCnt, int isNew, String name)
	{
		String result = "";
		
		if(dayCnt < isNew)
		{
			result = "<img src='/images/new_icon_" + name + ".gif'>";
		}
		
		return result;
	}
	
	public static String getFileName(String fileName, String oldFileName, String delFileFlag)
	{
		String result = "";
		
		if(delFileFlag.equals(""))
		{
			if(fileName.equals(""))
				result = oldFileName;
			else
				result = fileName;
		}	
		
		return result;
	}
	
	public static boolean isDeleteFile(String fileName, String oldFileName, String delFileFlag)
	{
		boolean result = false;
		
		if((!fileName.equals("") || !delFileFlag.equals("")) && !oldFileName.equals(""))
			result = true;
		
		return result;
	}
	

	
    /**
     * 문자열 널 체크 함수
     * 
     * @param str
     * @return
     */
    public static boolean isNull(String str) {
        if (str == null) return true;
        str = str.trim();
        if (str == null || str.length() <= 0) return true;
        return false;
    }
    
    /**
     * 
     * @param cont
     * @param isHtml
     * @return
     */
    public static String replaceBr(String cont,boolean isHtml) { //엔터를 <BR>로변환, 각종태그사용유무
        if (isNull(cont)) return "";
        StringBuffer buffer = new StringBuffer();
        char c,oc=0;
        int n=0;
        int MAX_BUF = 1024;
        char[] buf= new char[MAX_BUF+128];
        
        for (int i=0; i<cont.length(); ++i) {
            c = cont.charAt(i);
            if(isHtml) { // 일반 text에서 입력한 것을 html 처럼 보이게하기
                switch(c) {
                    case '\n' : 
                        if (oc=='\r') {
                            buf[n]='<' ; n++; buf[n]='B' ; n++; buf[n]='R' ; n++;  
                            buf[n]='>' ; n++; buf[n]='\n'; n++;
                        } else {
                            buf[n]=c; n++;
                        }
                        break;
                    case ' ' : 
                        if (oc==' ') {
                            buf[n]='&' ; n++; buf[n]='n' ; n++; buf[n]='b' ; n++;
                            buf[n]='s' ; n++; buf[n]='p'; n++; buf[n]=';'; n++;
                        } else {
                            buf[n]=c; n++;
                        }
                        break;
                    case '\t' :
                        for(int j=0;j<4;j++) {
                            buf[n]='&' ; n++; buf[n]='n' ; n++; buf[n]='b' ; n++;
                            buf[n]='s' ; n++; buf[n]='p'; n++; buf[n]=';'; n++;
                        }
                        break;
                    default   : 
                        buf[n]=c;
                        n++;
                        break;
                } // 
            } else {   
                switch(c) {
                    case '<'  : 
                        buf[n]='&'; n++;
                        buf[n]='l'; n++;
                        buf[n]='t'; n++;
                        break;
                    case '>'  : 
                        buf[n]='&'; n++;
                        buf[n]='g'; n++;
                        buf[n]='t'; n++;
                        break;
                    case '\n' : 
                        if (oc=='\r') {
                            buf[n]='<' ; n++; buf[n]='B' ; n++; buf[n]='R' ; n++;
                            buf[n]='>' ; n++; buf[n]='\n'; n++;
                        } else {
                            buf[n]=c; n++;
                        }
                        break;
                    case ' ' : 
                        if (oc==' ') {
                            buf[n]='&' ; n++;  buf[n]='n' ; n++; buf[n]='b' ; n++;
                            buf[n]='s' ; n++;  buf[n]='p'; n++; buf[n]=';'; n++;
                        } else {
                            buf[n]=c; n++;
                        }
                        break;
                    case '\t' :
                        for(int j=0;j<4;j++) {
                            buf[n]='&' ; n++; buf[n]='n' ; n++; buf[n]='b' ; n++;
                            buf[n]='s' ; n++; buf[n]='p'; n++; buf[n]=';'; n++;
                        }
                        break;
                    default   : 
                        buf[n]=c;
                        n++;
                        break;
                }
            }
            oc=c;
            
            if (n>MAX_BUF) {
                buffer.append(buf,0,n);
                n=0;
            }
        }
        
        if (n!=0) buffer.append(buf,0,n);
        return buffer.toString();
    }   
    
    public static String getImagePath(String file_name)	//첨부파일 종류에 따라 이미지가 결정되는 
    {
    	if(file_name!=null && !"".equals(file_name))
		{
			String ext=file_name.substring(file_name.length()-4);
			String imgPath="";
			if(ext.equals(".doc"))
			{
				imgPath="/images/doc.gif";
			}
			else if(ext.equals(".hwp"))
			{
				imgPath="/images/hwp.gif";
			}
			else if(ext.equals(".gif")||ext.equals(".jpg")||ext.equals("jpeg")||ext.equals(".png")||ext.equals(".bmp")||ext.equals(".pcx"))
			{
				imgPath="/images/img.gif";
			}
			else if(ext.equals(".pdf"))
			{
				imgPath="/images/pdf.gif";
			}
			else if(ext.equals(".PDF"))
			{
				imgPath="/images/pdf.gif";
			}
			else if(ext.equals(".ppt"))
			{
				imgPath="/images/ppt.gif";
			}
			else if(ext.equals(".txt"))
			{
				imgPath="/images/txt.gif";
			}
			else if(ext.equals(".xls"))
			{
				imgPath="/images/xls.gif";
			}
			else if(ext.equals(".zip"))
			{
				imgPath="/images/zip.gif";
			}
			else if(ext.equals(".htm")||ext.equals("html"))
			{
				imgPath="/images/htm.gif";
			}
			else
			{
				imgPath="/images/etc.gif";
			}
			return imgPath;
		}
    	else
    	{
    		return "";
    	}
    }
    
    public static String cutFileName(String file)		//파일명과 확장자만 출력하기 
    {
    	String result="";
    	boolean chk=false;
    	String temp="";
	    StringTokenizer st = new StringTokenizer(file, "_");
	    temp=st.nextToken();
    	
    	if(file.length() < 14 || temp.length() != 13)
	    {
	    	result = file;
	    }
	    else
	    {
	    	for(int i=0;i<temp.length();i++)
	    	{
	    		 String delims = "1234567890";
	    		 for(int j=0;j < delims.length();j++)
	    		 {

	    			 if(temp.charAt(i)==delims.charAt(j))
	    			 {
	    				 chk=true;
	    				 break;
	    			 }
	    			 else
	    			 {
	    			 	chk=false;
	    			 }
	    		 }
	    		 if(!chk)
	    		 	break;
	    	}
	    	if(!chk)
	    	{
	    		result = file;
	    	}
	    	else
	    	{
	    		result = file.substring(14);
	    	}
	    }
    	return result;
    }
}