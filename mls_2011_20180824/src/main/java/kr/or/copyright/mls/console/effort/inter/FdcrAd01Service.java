package kr.or.copyright.mls.console.effort.inter;

import java.util.ArrayList;
import java.util.Map;

public interface FdcrAd01Service {
	/**
	 * 저작권자 조회 공고 게시판 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd01List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한노력 저작권자 조회 공고 게시판 상세조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd01View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁 공고 게시판 - 공고승인
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd01Update1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 상세보기(보완처리용)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd01View2( Map<String, Object> commandMap ) throws Exception;
	/**
	 * 저작권자 조회공고, 보상금 공탁공고 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd01Delete1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 저작권자 조회공고, 보상금 공탁공고 처리상태 수정(신청,보완,신청승인,반려)
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd01Update2( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 공고 항목 코드
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Map<String,Object>> selectCdList(Map<String,Object> commandMap) throws Exception;
}
