package kr.or.copyright.mls.image.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kr.or.copyright.mls.image.controller.ImageController;
import kr.or.copyright.mls.image.dao.ImageDao;
import kr.or.copyright.mls.image.model.ImageDTO;

public class ImageServiceImpl implements ImageService{

	private static final Logger logger = LoggerFactory.getLogger( ImageServiceImpl.class );
	
	private ImageDao imageDao;
	private ImageDTO imageDTO;
	public ImageDTO getImageByWorkId( int works_id , int genreCode){
		if(genreCode==7)//�̼�
		{
			imageDTO = imageDao.getImageByWorkId(works_id);
		}else if(genreCode==8){//����
			imageDTO = imageDao.getPhotoByWorkId( works_id );
		}
		return imageDTO;
	}
	
	public void setImageDao( ImageDao imageDao ){
		this.imageDao = imageDao;
	}
	
	public void setImageDTO( ImageDTO imageDTO ){
		this.imageDTO = imageDTO;
	}
	
}
