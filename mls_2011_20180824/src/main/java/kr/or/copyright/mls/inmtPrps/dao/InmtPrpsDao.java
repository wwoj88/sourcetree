package kr.or.copyright.mls.inmtPrps.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.inmtPrps.model.InmtPrps;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

public interface InmtPrpsDao {
	
	public String maxYear(InmtPrps params);
	
	public int inmtCount(InmtPrps params);
	
	public List inmtList(InmtPrps params);
	
	//신규 미분배보상금 방송음악 CNT조회 20121002 정병호
	public int muscInmtCount(InmtPrps params);

	//신규 미분배보상금 방송음악 최대금액 조회
	public int muscMaxAmnt();
	
	//신규 미분배보상금 교과용 CNT조회 20121002 정병호
	public int subjInmtCount(InmtPrps params);
	
	//신규 미분배보상금 도서관 CNT조회 20121002 정병호
	public int librInmtCount(InmtPrps params);
	
	//신규 미분배보상금 수업목적 CNT조회 20121002 정병호
	public int lssnInmtCount(InmtPrps params);
	
	//신규 미분배보상금 방송음악 조회 20121002 정병호
	public List muscInmtList(InmtPrps params);
	
	//신규 미분배보상금 교과용 조회 20121002 정병호
	public List subjInmtList(InmtPrps params);
	
	//신규 미분배보상금 도서관 조회 20121002 정병호
	public List librInmtList(InmtPrps params);
	
	//신규 미분배보상금 수업목적 조회 20121002 정병호
	public List lssnInmtList(InmtPrps params);
	
	
	public List chkInmtList(InmtPrps params);
	
	public int prpsMastKey();
	
	public int prpsSeqnMax();
	
	public void inmtPspsInsert(Map params);
	
	public void inmtKappInsert(Map params);
	
	public void inmtFokapoInsert(Map params);
	
	public void inmtKrtraInsert(Map params);
	
	public void inmtPrpsRsltInsert(Map params);
	
	public void insertConnInfo(Map params);
	
	public String getMaxDealStat(RghtPrps params);

	public void inmtKappDelete(RghtPrps params);

	public void inmtFokapoDelete(RghtPrps params);

	public void inmtKrtraDelete(RghtPrps params);

	public void inmtPrpsRsltDelete(RghtPrps params);

	public void inmtPrpsDelete(RghtPrps params);
	
	public void inmtFileDeleteOne(InmtPrps params);
	
	public List inmtPrpsHistList(RghtPrps params);
	
	public int getBrctInmtSeqn();
	public void brctImtInsert(RghtPrps params);
	
	public int getLibrInmtSeqn();
	public void librImtInsert(RghtPrps params);
	
	public int getSubjInmtSeqn();
	public void subjImtInsert(RghtPrps params);
	
	public List inmtPrpsTopList(Map params);

	public List inmtPrpsAmntList( Map params );
	
}