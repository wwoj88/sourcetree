package kr.or.copyright.mls.adminEvnSet.service;

import java.util.List;
import java.util.Map;

public interface AdminEvnSetService {
	
	//  저작물보고 관리자 이메일 등록, 삭제
	public void selectMailMgntList() throws Exception;
	
	public void isCheckMailMgntList() throws Exception;
	
	public void SelectMailMgntCodeList() throws Exception;
	
	public void deleteMailMgntList() throws Exception;
	
	public void insertMailMgntList() throws Exception;
	
	
	// 상당한노력 자동화설정
	public void statProcSetList() throws Exception;
	
	public void statProcSetUpdate() throws Exception;
	
	// 저작물보고 관리자 메일발송
	public void WorksMgntMailScheduling() throws Exception;
}
