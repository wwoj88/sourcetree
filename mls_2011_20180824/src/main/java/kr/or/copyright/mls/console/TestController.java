package kr.or.copyright.mls.console;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.service.NewTestService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController{

	@Resource( name = "newTestService" )
	private NewTestService newTestService;

	private static final Logger logger = LoggerFactory.getLogger( TestController.class );

	@RequestMapping( value = "/console/test.page" )
	public String test(
		ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		System.out.println( "test" );
		newTestService.statBord06List( commandMap );
		List ds_list = (List) commandMap.get( "ds_list" );
		model.addAttribute( "ds_list", ds_list );
		return "test";
	}

}
