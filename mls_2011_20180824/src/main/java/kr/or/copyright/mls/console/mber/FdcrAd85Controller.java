package kr.or.copyright.mls.console.mber;

import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Service;
import kr.or.copyright.mls.console.mber.inter.FdcrAd84Service;
import kr.or.copyright.mls.console.mber.inter.FdcrAd85Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 회원관리 > 내정보 수정
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd85Controller extends DefaultController {

     @Resource(name = "fdcrAd84Service")
     private FdcrAd84Service fdcrAd84Service;

     @Resource(name = "fdcrAd83Service")
     private FdcrAd83Service fdcrAd83Service;

     private static final Logger logger = LoggerFactory.getLogger(FdcrAd85Controller.class);

     /**
      * 조회
      * 
      * @param commandMap
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd85UpdateForm1.page")
     public String fdcrAd85UpdateForm1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd85UpdateForm1 Start");
          // 파라미터 셋팅
          String USER_IDNT = ConsoleLoginUser.getUserId();
          commandMap.put("USER_IDNT", USER_IDNT);
          String gubun = EgovWebUtil.getString(commandMap, "GUBUN");
          if (gubun.equals("edit")) {
               fdcrAd84Service.fdcrAd84UpdateForm1(commandMap);
          }
          model.addAttribute("info", commandMap.get("ds_list"));
          logger.debug("fdcrAd85UpdateForm1 End");
          return "/mber/fdcrAd85UpdateForm1.tiles";
     }

     /**
      * 수정
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd85Update1.page")
     public String fdcrAd85Update1(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd85Update1 Start");
          String USER_IDNT = ConsoleLoginUser.getUserId();
          commandMap.put("USER_IDNT", USER_IDNT);
          // 파라미터 셋팅
          boolean isSuccess = fdcrAd84Service.fdcrAd84Update1(commandMap);
          returnAjaxString(response, isSuccess);
          logger.debug("fdcrAd85Update1 End");
          if (isSuccess) {
               return returnUrl(model, "저장했습니다.", "/console/mber/fdcrAd85UpdateForm1.page?GUBUN=edit&USER_IDNT=" + EgovWebUtil.getString(commandMap, "USER_IDNT"));
          } else {
               return returnUrl(model, "실패했습니다.", "/console/mber/fdcrAd85UpdateForm1.page?GUBUN=edit&USER_IDNT=" + EgovWebUtil.getString(commandMap, "USER_IDNT"));
          }
     }

     /**
      * 비밀번호 변경
      * 
      * @param model
      * @param commandMap
      * @param request
      * @param response
      * @throws Exception
      */
     @RequestMapping(value = "/console/mber/fdcrAd85Update2.page")
     public String fdcrAd85Update2(ModelMap model, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

          logger.debug("fdcrAd85Update2 Start");
          String USER_IDNT = ConsoleLoginUser.getUserId();
          commandMap.put("USER_IDNT", USER_IDNT);
          // 파라미터 셋팅
          boolean isSuccess = fdcrAd83Service.fdcrAd83Update1(commandMap);
          logger.debug("fdcrAd85Update2 End");
          if (isSuccess) {
               return returnUrl(model, "변경했습니다.", "/console/mber/fdcrAd85UpdateForm1.page?GUBUN=edit&USER_IDNT=" + USER_IDNT);
          } else {
               return returnUrl(model, "실패했습니다.", "/console/mber/fdcrAd85UpdateForm1.page?GUBUN=edit&USER_IDNT=" + USER_IDNT);
          }
     }

}
