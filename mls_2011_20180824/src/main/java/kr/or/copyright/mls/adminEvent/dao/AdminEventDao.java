package kr.or.copyright.mls.adminEvent.dao;

import java.util.List;
import java.util.Map;

public interface AdminEventDao {

	public List campPartList(Map map);
	
	public List eventMgntList(Map map);
	
	public List eventMgntDetail(Map map);
	
	public void eventMgntDelete(Map map);
	
	public void eventMgntUpdate(Map map);
	
	public void eventMgntInsert(Map map);
}
