package kr.or.copyright.mls.console.rcept;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd20Dao;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.tobesoft.platform.data.Dataset;

@Repository( "fdcrAd20Dao" )
public class FdcrAd20DaoImpl extends EgovComAbstractDAO implements FdcrAd20Dao{

	// 방송음악 List
	public List brctInmtList( Map map ){

		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.brctInmtList", map );
	}

	// 도서관 List
	public List librInmtList( Map map ){

		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.librInmtList", map );
	}

	// 교과용 List
	public List subjInmtList( Map map ){

		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.subjInmtList", map );
	}

	// 방송음악 insert
	public boolean brctInmtInsert( Map map ) throws SQLException{

		boolean bFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminInmtMgnt.brctInmtInsert", map );

			bFlag = true;

		}
		catch( Exception e ){
			// e.printStackTrace();
			bFlag = false;
		}
		finally{
			return bFlag;
		}
	}

	// 방송음악 insert
	public List brctInmtInsert( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception{

		List returnDsList = new ArrayList();
		boolean bFlag = true;

		int cnt = 0;
		
		String MGNT_INMT_SEQN = EgovWebUtil.getString( commandMap, "MGNT_INMT_SEQN" );
		String SDSR_NAME = EgovWebUtil.getString( commandMap, "SDSR_NAME" );
		String ALBM_NAME = EgovWebUtil.getString( commandMap, "ALBM_NAME" );
		String MUCI_NAME = EgovWebUtil.getString( commandMap, "MUCI_NAME" );
		String PERF_NAME = EgovWebUtil.getString( commandMap, "PERF_NAME" );
		String ALBM_PROD = EgovWebUtil.getString( commandMap, "ALBM_PROD" );
		String YYMM = EgovWebUtil.getString( commandMap, "YYMM" );
		String ALLT_YSNO = EgovWebUtil.getString( commandMap, "ALLT_YSNO" );
		String ALLT_DATE = EgovWebUtil.getString( commandMap, "ALLT_DATE" );
		String ALLT_AMNT = EgovWebUtil.getString( commandMap, "ALLT_AMNT" );

		// 0번째 row는 컬럼정보이므로 제외
		for( int i = 0; i < excelList.size(); i++ ){

			Map map = null;

			try{

				map = excelList.get( i );

				// 법정허락저작물 저작물ID(WORKS_ID) MAX값 구해서 값 셋팅

				int isExist =
					Integer.parseInt( ""
						+ getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.getExistStatBrctInmt", map ) );

				if( isExist == 0 ){
					int worksId = getNewWorksId();
					// 법정허락저작물 등록
					Map statWorksMap = new HashMap();
					statWorksMap.put( "WORKS_ID", worksId );
					// statWorksMap.put("GENRE_CD", "");
					statWorksMap.put( "WORKS_TITLE", map.get( "SDSR_NAME" ) );
					statWorksMap.put( "WORKS_DIVS_CD", 1 );
					statWorksMap.put( "STAT_WORKS_YN", "N" );
					statWorksMap.put( "SYST_EFFORT_STAT_CD", 1 );
					// statWorksMap.put("SYST_EFFORT_STAT_DTTM", );
					statWorksMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );
					statWorksMap.put( "GENRE_CD", 2 );

					// 법정허락저작물 등록
					statWorksInsert( statWorksMap );

					map.put( "WORKS_ID", worksId );
					map.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

					String trstOrgnCode = (String) commandMap.get( "TRST_ORGN_CODE" );

					if( trstOrgnCode.equals( "202" ) ){
						map.put( "FOKAPO_MGNT_YSNO", "Y" );
						map.put( "KAPP_MGNT_YSNO", "N" );
					}else if( trstOrgnCode.equals( "203" ) ){
						map.put( "FOKAPO_MGNT_YSNO", "N" );
						map.put( "KAPP_MGNT_YSNO", "Y" );
					}else{
						map.put( "FOKAPO_MGNT_YSNO", "N" );
						map.put( "KAPP_MGNT_YSNO", "N" );
					}
					map.put( "KAPP_YSNO", "Y" );
					map.put( "FOKAPO_YSNO", "Y" );

					bFlag = brctInmtInsert( map );
				}else{
					bFlag = false;
				}

				if( !bFlag ){
					returnDsList.add( map );
				}else{
					cnt++;
				}
			}
			catch( Exception e ){
				// TODO Auto-generated catch block
				e.printStackTrace();

				returnDsList.add( map );
			}
		}

		Map reportMap = new HashMap();

		reportMap.put( "TRST_ORGN_CODE", commandMap.get( "TRST_ORGN_CODE" ) );
		reportMap.put( "INMT_CD", 1 );
		reportMap.put( "REPT_WORKS_CONT", cnt );
		reportMap.put( "MODI_WORKS_CONT", 0 );
		reportMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

		inmtWorksReportInsert( reportMap );

		return returnDsList;
	}

	public boolean subjInmtInsert( Map map ) throws SQLException{

		boolean bFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminInmtMgnt.subjInmtInsert", map );

			bFlag = true;

		}
		catch( Exception e ){
			e.printStackTrace();
			bFlag = false;
		}
		finally{
			return bFlag;
		}

	}

	// 교과용 insert
	public List subjInmtInsert( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception{

		List returnDsList = new ArrayList();
		boolean bFlag = true;

		int cnt = 0;

		String MGNT_INMT_SEQN = EgovWebUtil.getString( commandMap, "MGNT_INMT_SEQN" );
		String WORK_NAME = EgovWebUtil.getString( commandMap, "WORK_NAME" );
		String COPT_HODR = EgovWebUtil.getString( commandMap, "COPT_HODR" );
		String SCHL = EgovWebUtil.getString( commandMap, "SCHL" );
		String BOOK_DIVS = EgovWebUtil.getString( commandMap, "BOOK_DIVS" );
		String LISH_COMP = EgovWebUtil.getString( commandMap, "LISH_COMP" );
		String SUBJ_NAME = EgovWebUtil.getString( commandMap, "SUBJ_NAME" );
		String BOOK_SIZE_DIVS = EgovWebUtil.getString( commandMap, "BOOK_SIZE_DIVS" );
		String SCHL_YEAR_DIVS = EgovWebUtil.getString( commandMap, "SCHL_YEAR_DIVS" );
		String PUBC_YEAR = EgovWebUtil.getString( commandMap, "PUBC_YEAR" );
		String WORK_KIND = EgovWebUtil.getString( commandMap, "WORK_KIND" );
		String USEX_PAGE = EgovWebUtil.getString( commandMap, "USEX_PAGE" );
		String YYMM = EgovWebUtil.getString( commandMap, "YYMM" );
		String ALLT_YSNO = EgovWebUtil.getString( commandMap, "ALLT_YSNO" );
		String ALLT_DATE = EgovWebUtil.getString( commandMap, "ALLT_DATE" );
		String ALLT_AMNT = EgovWebUtil.getString( commandMap, "ALLT_AMNT" );
		
		// 0번째 row는 컬럼정보이므로 제외
		for( int i = 0; i < excelList.size(); i++ ){

			Map map = null;

			try{

				map = excelList.get( i );
				int isExist =
					Integer.parseInt( ""
						+ getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.getExistStatSubjInmt", map ) );

				if( isExist == 0 ){
					// 법정허락저작물 저작물ID(WORKS_ID) MAX값 구해서 값 셋팅 하기
					int worksId = getNewWorksId();

					// 법정허락저작물 GENRE_CD?값 셋팅하기 = null 처리

					// 법정허락저작물 테이블 INSERT만들기(공통값)

					// 상당한노력 진행상태 변경내역 테이블 INSERT만들기?(공통값)=트리거 만들 예정

					map.put( "WORKS_ID", worksId );
					map.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

					// 법정허락저작물 데이터 생성
					Map statWorksMap = new HashMap();
					statWorksMap.put( "WORKS_ID", worksId );
					// statWorksMap.put("GENRE_CD", "");
					statWorksMap.put( "WORKS_TITLE", map.get( "WORK_NAME" ) );
					statWorksMap.put( "WORKS_KIND", map.get( "WORK_KIND" ) );
					statWorksMap.put( "WORKS_DIVS_CD", 2 );
					statWorksMap.put( "STAT_WORKS_YN", "N" );
					statWorksMap.put( "SYST_EFFORT_STAT_CD", 1 );
					// statWorksMap.put("SYST_EFFORT_STAT_DTTM", );
					statWorksMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );
					System.out.println("WORK_KIND : "+ map.get( "WORK_KIND" ) );
					// 법정허락저작물 등록
					statWorksInsert( statWorksMap );

					// 교과용보상금 등록
					
					bFlag = subjInmtInsert( map );
				}else{
					bFlag = false;
				}

				if( !bFlag ){
					returnDsList.add( map );
				}else{
					cnt++;
				}

			}
			catch( Exception e ){
				// TODO Auto-generated catch block
				e.printStackTrace();

				returnDsList.add( map );
			}

		}

		Map reportMap = new HashMap();

		reportMap.put( "TRST_ORGN_CODE", commandMap.get( "TRST_ORGN_CODE" ) );
		reportMap.put( "INMT_CD", 2 );
		reportMap.put( "REPT_WORKS_CONT", cnt );
		reportMap.put( "MODI_WORKS_CONT", 0 );
		reportMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

		inmtWorksReportInsert( reportMap );

		return returnDsList;
	}

	public boolean librInmtInsert( Map map ) throws SQLException{

		boolean bFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminInmtMgnt.librInmtInsert", map );

			bFlag = true;

		}
		catch( Exception e ){
			e.printStackTrace();
			bFlag = false;
		}
		finally{
			return bFlag;
		}

	}

	// 도서관 insert
	public List librInmtInsert( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception{

		List returnDsList = new ArrayList();
		boolean bFlag = true;

		int cnt = 0;
		
		String RGST_ORGN_CODE = EgovWebUtil.getString( commandMap, "RGST_ORGN_CODE" );
		String MGNT_INMT_SEQN = EgovWebUtil.getString( commandMap, "MGNT_INMT_SEQN" );
		String WORK_NAME = EgovWebUtil.getString( commandMap, "WORK_NAME" );
		String COPT_HODR = EgovWebUtil.getString( commandMap, "COPT_HODR" );
		String USEX_LIBR = EgovWebUtil.getString( commandMap, "USEX_LIBR" );
		String LISH_COMP = EgovWebUtil.getString( commandMap, "LISH_COMP" );
		String PUBC_YEAR = EgovWebUtil.getString( commandMap, "PUBC_YEAR" );
		String YYMM = EgovWebUtil.getString( commandMap, "YYMM" );
		String ALLT_YSNO = EgovWebUtil.getString( commandMap, "ALLT_YSNO" );
		String ALLT_DATE = EgovWebUtil.getString( commandMap, "ALLT_DATE" );
		String ALLT_AMNT = EgovWebUtil.getString( commandMap, "ALLT_AMNT" );

		// 0번째 row는 컬럼정보이므로 제외
		for( int i = 0; i < excelList.size(); i++ ){

			Map map = null;

			try{

				map = excelList.get( i );

				int isExist =Integer.parseInt( ""+ getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.getExistStatLibrInmt", map ) );

				if( isExist == 0 ){
					// 법정허락저작물 저작물ID(WORKS_ID) MAX값 구해서 값 셋팅
					int worksId = getNewWorksId();

					// 법정허락저작물 등록
					Map statWorksMap = new HashMap();
					statWorksMap.put( "WORKS_ID", worksId );
					// statWorksMap.put("GENRE_CD", "");
					statWorksMap.put( "WORKS_TITLE", map.get( "WORK_NAME" ) );
					statWorksMap.put( "WORKS_DIVS_CD", 3 );
					statWorksMap.put( "STAT_WORKS_YN", "N" );
					statWorksMap.put( "SYST_EFFORT_STAT_CD", 1 );
					statWorksMap.put( "GENRE_CD", 1 );
					// statWorksMap.put("SYST_EFFORT_STAT_DTTM", );
					statWorksMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

					// 법정허락저작물 등록
					statWorksInsert( statWorksMap );
					map.put( "WORKS_ID", worksId );
					map.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );
					bFlag = librInmtInsert( map );
				}else{
					bFlag = false;
				}
				if( !bFlag ){
					returnDsList.add( map );
				}else{
					cnt++;
				}

			}
			catch( Exception e ){
				// TODO Auto-generated catch block
				e.printStackTrace();

				returnDsList.add( map );
			}

		}

		Map reportMap = new HashMap();

		reportMap.put( "TRST_ORGN_CODE", commandMap.get( "TRST_ORGN_CODE" ) );
		reportMap.put( "INMT_CD", 3 );
		reportMap.put( "REPT_WORKS_CONT", cnt );
		reportMap.put( "MODI_WORKS_CONT", 0 );
		reportMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

		inmtWorksReportInsert( reportMap );

		return returnDsList;
	}

	// 수업목적 insert
	public List lssnInmtInsert( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception{

		List returnDsList = new ArrayList();
		boolean bFlag = true;

		int cnt = 0;

		String MGNT_INMT_SEQN = EgovWebUtil.getString( commandMap, "MGNT_INMT_SEQN" );
		String WORK_NAME = EgovWebUtil.getString( commandMap, "WORK_NAME" );
		String WRITER_NAME = EgovWebUtil.getString( commandMap, "WRITER_NAME" );
		String USEX_SITE = EgovWebUtil.getString( commandMap, "USEX_SITE" );
		String PUBC_YEAR = EgovWebUtil.getString( commandMap, "PUBC_YEAR" );
		String WORK_KIND = EgovWebUtil.getString( commandMap, "WORK_KIND" );
		String ALLT_YSNO = EgovWebUtil.getString( commandMap, "ALLT_YSNO" );
		String ALLT_DATE = EgovWebUtil.getString( commandMap, "ALLT_DATE" );
		// 0번째 row는 컬럼정보이므로 제외
		for( int i = 0; i < excelList.size(); i++ ){

			Map map = null;

			try{

				map = excelList.get( i );

				int isExist =
					Integer.parseInt( ""
						+ getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.getExistStatLssnInmt", map ) );

				if( isExist == 0 ){
					// 법정허락저작물 저작물ID(WORKS_ID) MAX값 구해서 값 셋팅
					int worksId = getNewWorksId();

					// 법정허락저작물 등록
					Map statWorksMap = new HashMap();
					statWorksMap.put( "WORKS_ID", worksId );
					// statWorksMap.put("GENRE_CD", "");
					statWorksMap.put( "WORKS_TITLE", map.get( "WORK_NAME" ) );
					statWorksMap.put( "WORKS_DIVS_CD", 7 );
					statWorksMap.put( "STAT_WORKS_YN", "N" );
					statWorksMap.put( "SYST_EFFORT_STAT_CD", 1 );
					statWorksMap.put( "GENRE_CD", 1 );
					// statWorksMap.put("SYST_EFFORT_STAT_DTTM", );
					statWorksMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

					// 법정허락저작물 등록
					statWorksInsert( statWorksMap );
					map.put( "WORKS_ID", worksId );
					map.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );
					bFlag = lssnInmtInsert( map );
				}else{
					bFlag = false;
				}
				if( !bFlag ){
					returnDsList.add( map );
				}else{
					cnt++;
				}

			}
			catch( Exception e ){
				// TODO Auto-generated catch block
				e.printStackTrace();

				returnDsList.add( map );
			}

		}

		Map reportMap = new HashMap();

		reportMap.put( "TRST_ORGN_CODE", commandMap.get( "TRST_ORGN_CODE" ) );
		reportMap.put( "INMT_CD", 4 );
		reportMap.put( "REPT_WORKS_CONT", cnt );
		reportMap.put( "MODI_WORKS_CONT", 0 );
		reportMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

		inmtWorksReportInsert( reportMap );

		return returnDsList;
	}

	private boolean lssnInmtInsert( Map map ){
		boolean bFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminInmtMgnt.lssnInmtInsert", map );

			bFlag = true;

		}
		catch( Exception e ){
			e.printStackTrace();
			bFlag = false;
		}
		finally{
			return bFlag;
		}
	}

	@Transactional( readOnly = false )
	public boolean subjInmtUpdate( Map map ) throws SQLException{

		boolean bFlag = true;

		try{

			// 보상금정보 update
			getSqlMapClientTemplate().update( "AdminInmtMgnt.subjInmWorksInfoUpdate", map );
			getSqlMapClientTemplate().update( "AdminInmtMgnt.subjInmAlltInfoUpdate", map );

			// 이미지저작물정보 update
			getSqlMapClientTemplate().update( "AdminInmtMgnt.imgeRhgtWorksInfoUpdate", map );

			bFlag = true;

		}
		catch( Exception e ){

			e.printStackTrace();
			bFlag = false;

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		finally{
			return bFlag;
		}

	}

	// 교과용보상금 테이블 수정 20120904 정병호
	@Transactional( readOnly = false )
	public boolean subjInmtUpdateUpload( Map map ) throws SQLException{

		boolean bFlag = true;

		try{

			// 교과용보상금 update
			getSqlMapClientTemplate().update( "AdminInmtMgnt.subjInmtUpdateUpload", map );
			bFlag = true;

		}
		catch( Exception e ){

			e.printStackTrace();
			bFlag = false;

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		finally{
			return bFlag;
		}

	}

	// 도서관보상금 테이블 수정 20120904 정병호
	@Transactional( readOnly = false )
	public boolean librInmtUpdateUpload( Map map ) throws SQLException{

		boolean bFlag = true;

		try{

			// 교과용보상금 update
			getSqlMapClientTemplate().update( "AdminInmtMgnt.librInmtUpdateUpload", map );
			bFlag = true;

		}
		catch( Exception e ){

			e.printStackTrace();
			bFlag = false;

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		finally{
			return bFlag;
		}

	}

	// 도서관보상금 테이블 수정 20120904 정병호
	@Transactional( readOnly = false )
	public boolean brctInmtUpdateUpload( Map map ) throws SQLException{

		boolean bFlag = true;

		try{

			// 도서관보상금 update
			getSqlMapClientTemplate().update( "AdminInmtMgnt.brctInmtUpdateUpload", map );
			bFlag = true;

		}
		catch( Exception e ){

			e.printStackTrace();
			bFlag = false;

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		finally{
			return bFlag;
		}
	}

	// 수업목적보상금 테이블 수정 20120904 정병호
	@Transactional( readOnly = false )
	public boolean lssnInmtUpdateUpload( Map map ) throws SQLException{

		boolean bFlag = true;

		try{

			// 교과용보상금 update
			getSqlMapClientTemplate().update( "AdminInmtMgnt.lssnInmtUpdateUpload", map );
			bFlag = true;

		}
		catch( Exception e ){

			e.printStackTrace();
			bFlag = false;

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		finally{
			return bFlag;
		}

	}

	// 교과용 update
	public List subjInmtUpdate( Dataset ds ){

		List returnDsList = new ArrayList();
		boolean bFlag = true;

		for( int i = 0; i < ds.getRowCount(); i++ ){

			Map map = null;

			try{
				map = getMap( ds, i );

				bFlag = subjInmtUpdate( map );

				if( !bFlag ){
					returnDsList.add( map );
				}

			}
			catch( Exception e ){
				// TODO Auto-generated catch block
				e.printStackTrace();

				returnDsList.add( map );
			}

		}

		return returnDsList;
	}

	// 교과용 NEW Update 20120904 정병호
	public List subjInmtUpdateUpload( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception{

		List returnDsList = new ArrayList();
		boolean bFlg = true;

		int cnt = 0;
		
		String MGNT_INMT_SEQN = EgovWebUtil.getString( commandMap, "MGNT_INMT_SEQN" );
		String WORK_NAME = EgovWebUtil.getString( commandMap, "WORK_NAME" );
		String COPT_HODR = EgovWebUtil.getString( commandMap, "COPT_HODR" );
		String SCHL = EgovWebUtil.getString( commandMap, "SCHL" );
		String BOOK_DIVS = EgovWebUtil.getString( commandMap, "BOOK_DIVS" );
		String LISH_COMP = EgovWebUtil.getString( commandMap, "LISH_COMP" );
		String SUBJ_NAME = EgovWebUtil.getString( commandMap, "SUBJ_NAME" );
		String BOOK_SIZE_DIVS = EgovWebUtil.getString( commandMap, "BOOK_SIZE_DIVS" );
		String SCHL_YEAR_DIVS = EgovWebUtil.getString( commandMap, "SCHL_YEAR_DIVS" );
		String PUBC_YEAR = EgovWebUtil.getString( commandMap, "PUBC_YEAR" );
		String WORK_KIND = EgovWebUtil.getString( commandMap, "WORK_KIND" );
		String USEX_PAGE = EgovWebUtil.getString( commandMap, "USEX_PAGE" );
		String YYMM = EgovWebUtil.getString( commandMap, "YYMM" );
		String ALLT_YSNO = EgovWebUtil.getString( commandMap, "ALLT_YSNO" );
		String ALLT_DATE = EgovWebUtil.getString( commandMap, "ALLT_DATE" );
		String ALLT_AMNT = EgovWebUtil.getString( commandMap, "ALLT_AMNT" );

		for( int i = 0; i < excelList.size(); i++ ){

			Map map = null;

			try{

				map = excelList.get( i );

				// ds에 담긴 복전협내부관리아이디와, 복전협저작물코드 값으로 업데이트 처리를 한다.
				map.put( "MODI_IDNT", commandMap.get( "USER_IDNT" ) );
				bFlg = subjInmtUpdateUpload( map );

				if( !bFlg ){
					returnDsList.add( map );
				}else{
					// true이면 works_id값을 가져와서
					int worksId = getSubjInmtWorksId( map );

					Map statWorksMap = new HashMap();
					statWorksMap.put( "WORKS_TITLE", map.get( "WORK_NAME" ) );
					statWorksMap.put( "MODI_IDNT", commandMap.get( "USER_IDNT" ) );
					statWorksMap.put( "WORKS_ID", worksId );

					statWorksUpdate( statWorksMap );

					cnt++;

				}
			}
			catch( Exception e ){
				e.printStackTrace();
				returnDsList.add( map );
			}
		}

		Map reportMap = new HashMap();

		reportMap.put( "TRST_ORGN_CODE", commandMap.get( "TRST_ORGN_CODE" ) );
		reportMap.put( "INMT_CD", 2 );
		reportMap.put( "REPT_WORKS_CONT", 0 );
		reportMap.put( "MODI_WORKS_CONT", cnt );
		reportMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

		inmtWorksReportInsert( reportMap );

		return returnDsList;
	}

	// 도서관 NEW Update 20120905 정병호
	public List librInmtUpdateUpload( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception{

		List returnDsList = new ArrayList();
		boolean bFlg = true;

		int cnt = 0;
		
		String MGNT_INMT_SEQN = EgovWebUtil.getString( commandMap, "MGNT_INMT_SEQN" );
		String WORK_NAME = EgovWebUtil.getString( commandMap, "WORK_NAME" );
		String COPT_HODR = EgovWebUtil.getString( commandMap, "COPT_HODR" );
		String USEX_LIBR = EgovWebUtil.getString( commandMap, "USEX_LIBR" );
		String LISH_COMP = EgovWebUtil.getString( commandMap, "LISH_COMP" );
		String PUBC_YEAR = EgovWebUtil.getString( commandMap, "PUBC_YEAR" );
		String YYMM = EgovWebUtil.getString( commandMap, "YYMM" );
		String ALLT_YSNO = EgovWebUtil.getString( commandMap, "ALLT_YSNO" );
		String ALLT_DATE = EgovWebUtil.getString( commandMap, "ALLT_DATE" );
		String ALLT_AMNT = EgovWebUtil.getString( commandMap, "ALLT_AMNT" );

		for( int i = 0; i < excelList.size(); i++ ){

			Map map = null;

			try{

				map = excelList.get( i );

				// ds에 담긴 복전협내부관리아이디와, 복전협저작물코드 값으로 업데이트 처리를 한다.

				map.put( "MODI_IDNT", commandMap.get( "USER_IDNT" ) );
				bFlg = librInmtUpdateUpload( map );

				if( !bFlg ){
					returnDsList.add( map );
				}else{
					// true이면 works_id값을 가져와서
					int worksId = getLibrInmtWorksId( map );

					Map statWorksMap = new HashMap();
					statWorksMap.put( "WORKS_TITLE", map.get( "WORK_NAME" ) );
					statWorksMap.put( "MODI_IDNT", commandMap.get( "USER_IDNT" ) );
					statWorksMap.put( "WORKS_ID", worksId );
					statWorksUpdate( statWorksMap );
					cnt++;
				}
			}
			catch( Exception e ){
				e.printStackTrace();
				returnDsList.add( map );
			}
		}

		Map reportMap = new HashMap();

		reportMap.put( "TRST_ORGN_CODE", commandMap.get( "TRST_ORGN_CODE" ) );
		reportMap.put( "INMT_CD", 3 );
		reportMap.put( "REPT_WORKS_CONT", 0 );
		reportMap.put( "MODI_WORKS_CONT", cnt );
		reportMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

		inmtWorksReportInsert( reportMap );

		return returnDsList;
	}

	// 방송음악 NEW Update 20120905 정병호
	public List brctInmtUpdateUpload( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception{

		List returnDsList = new ArrayList();
		boolean bFlg = true;

		int cnt = 0;
		
		String MGNT_INMT_SEQN = EgovWebUtil.getString( commandMap, "MGNT_INMT_SEQN" );
		String SDSR_NAME = EgovWebUtil.getString( commandMap, "SDSR_NAME" );
		String ALBM_NAME = EgovWebUtil.getString( commandMap, "ALBM_NAME" );
		String MUCI_NAME = EgovWebUtil.getString( commandMap, "MUCI_NAME" );
		String PERF_NAME = EgovWebUtil.getString( commandMap, "PERF_NAME" );
		String ALBM_PROD = EgovWebUtil.getString( commandMap, "ALBM_PROD" );
		String YYMM = EgovWebUtil.getString( commandMap, "YYMM" );
		String ALLT_YSNO = EgovWebUtil.getString( commandMap, "ALLT_YSNO" );
		String ALLT_DATE = EgovWebUtil.getString( commandMap, "ALLT_DATE" );
		String ALLT_AMNT = EgovWebUtil.getString( commandMap, "ALLT_AMNT" );
		
		for( int i = 0; i < excelList.size(); i++ ){
			Map map = null;

			try{
				map = excelList.get( i );

				// ds에 담긴 복전협내부관리아이디와, 복전협저작물코드 값으로 업데이트 처리를 한다.
				map.put( "MODI_IDNT", commandMap.get( "USER_IDNT" ) );
				bFlg = brctInmtUpdateUpload( map );

				if( !bFlg ){
					returnDsList.add( map );
				}else{
					// true이면 works_id값을 가져와서
					int worksId = getBrctInmtWorksId( map );

					Map statWorksMap = new HashMap();
					statWorksMap.put( "WORKS_TITLE", map.get( "SDSR_NAME" ) );
					statWorksMap.put( "MODI_IDNT", commandMap.get( "USER_IDNT" ) );
					statWorksMap.put( "WORKS_ID", worksId );

					statWorksUpdate( statWorksMap );
					cnt++;

				}
			}
			catch( Exception e ){
				e.printStackTrace();
				returnDsList.add( map );
			}
		}
		Map reportMap = new HashMap();

		reportMap.put( "TRST_ORGN_CODE", commandMap.get( "TRST_ORGN_CODE" ) );
		reportMap.put( "INMT_CD", 1 );
		reportMap.put( "REPT_WORKS_CONT", 0 );
		reportMap.put( "MODI_WORKS_CONT", cnt );
		reportMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

		inmtWorksReportInsert( reportMap );

		return returnDsList;
	}

	// 수업목적 NEW Update 20120905 정병호
	public List lssnInmtUpdateUpload( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception{

		List returnDsList = new ArrayList();
		boolean bFlg = true;

		int cnt = 0;

		String MGNT_INMT_SEQN = EgovWebUtil.getString( commandMap, "MGNT_INMT_SEQN" );
		String WORK_NAME = EgovWebUtil.getString( commandMap, "WORK_NAME" );
		String WRITER_NAME = EgovWebUtil.getString( commandMap, "WRITER_NAME" );
		String USEX_SITE = EgovWebUtil.getString( commandMap, "USEX_SITE" );
		String PUBC_YEAR = EgovWebUtil.getString( commandMap, "PUBC_YEAR" );
		String WORK_KIND = EgovWebUtil.getString( commandMap, "WORK_KIND" );
		String ALLT_YSNO = EgovWebUtil.getString( commandMap, "ALLT_YSNO" );
		String ALLT_DATE = EgovWebUtil.getString( commandMap, "ALLT_DATE" );
		
		for( int i = 0; i < excelList.size(); i++ ){

			Map map = null;

			try{

				map = excelList.get( i );

				// ds에 담긴 복전협내부관리아이디와, 복전협저작물코드 값으로 업데이트 처리를 한다.

				map.put( "MODI_IDNT", commandMap.get( "USER_IDNT" ) );
				bFlg = lssnInmtUpdateUpload( map );

				if( !bFlg ){
					returnDsList.add( map );
				}else{
					// true이면 works_id값을 가져와서
					int worksId = getLssnInmtWorksId( map );

					Map statWorksMap = new HashMap();
					statWorksMap.put( "WORKS_TITLE", map.get( "WORK_NAME" ) );
					statWorksMap.put( "MODI_IDNT", commandMap.get( "USER_IDNT" ) );
					statWorksMap.put( "WORKS_ID", worksId );
					statWorksUpdate( statWorksMap );
					cnt++;
				}
			}
			catch( Exception e ){
				e.printStackTrace();
				returnDsList.add( map );
			}
		}

		Map reportMap = new HashMap();

		reportMap.put( "TRST_ORGN_CODE", commandMap.get( "TRST_ORGN_CODE" ) );
		reportMap.put( "INMT_CD", 4 );
		reportMap.put( "REPT_WORKS_CONT", 0 );
		reportMap.put( "MODI_WORKS_CONT", cnt );
		reportMap.put( "RGST_IDNT", commandMap.get( "USER_IDNT" ) );

		inmtWorksReportInsert( reportMap );

		return returnDsList;
	}

	public List inmtMgntMuscList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.inmtMgntMuscList", map );
	}

	public List totalRowinmtMgntMuscList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.totalRowinmtMgntMuscList", map );
	}

	public List inmtMgntLibrList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.inmtMgntLibrList", map );
	}

	public List totalRowinmtMgntLibrList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.totalRowinmtMgntLibrList", map );
	}

	public List inmtMgntSubjList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.inmtMgntSubjList", map );
	}

	public List totalRowinmtMgntSubjList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.totalRowinmtMgntSubjList", map );
	}

	public List inmtMgntLssnList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.inmtMgntLssnList", map );
	}

	public List totalRowinmtMgntLssnList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.totalRowinmtMgntLssnList", map );
	}

	public List inmtMgntMovieList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.inmtMgntMovieList", map );
	}

	public List totalRowinmtMgntMovieList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.totalRowinmtMgntMovieList", map );
	}

	public List inmtMgntImageList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.inmtMgntImageList", map );
	}

	public List totalRowinmtMgntImageList( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.totalRowinmtMgntImageList", map );
	}

	/**
	 * Dataset의 특정 Row를 HashMap Type으로 반환한다.
	 * 
	 * @param ds
	 *            Dataset Object
	 * @param rnum
	 *            Dataset Row Index
	 * @return HashMap Object
	 */
	public HashMap getMap( Dataset ds,
		int rnum ) throws Exception{

		if( ds == null || ds.getRowCount() == 0 ){

			return null;
		}

		HashMap result = new HashMap();

		for( int i = 0; i < ds.getColumnCount(); i++ ){

			String columnName = ds.getColumnId( i );

			result.put( columnName, ds.getColumnAsObject( rnum, columnName ) );
		}

		return result;
	}

	// 이미지 insert
	public List imgeInsert( Dataset ds ){

		List returnDsList = new ArrayList();
		boolean bFlag = true;

		RghtPrps rghtPrps = new RghtPrps();

		// 0번째 row는 컬럼정보이므로 제외
		for( int i = 1; i < ds.getRowCount(); i++ ){

			Map map = null;

			try{

				map = getMap( ds, i );

				rghtPrps = imgeCount( map );

				if( rghtPrps.getPRPS_CNT().equals( "0" ) ){
					bFlag = imgeInsert( map );
					if( !bFlag ){
						returnDsList.add( map );
					}
				}else{
					returnDsList.add( map );
				}
				// bFlag = imgeInsert(map);
				//
				// if( !bFlag ) {
				// returnDsList.add(map);
				// }

			}
			catch( Exception e ){
				// TODO Auto-generated catch block
				e.printStackTrace();

				returnDsList.add( map );
			}

		}

		return returnDsList;
	}

	public boolean imgeInsert( Map map ) throws SQLException{

		boolean bFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminInmtMgnt.imgeInsert", map );

			bFlag = true;

		}
		catch( Exception e ){
			e.printStackTrace();
			bFlag = false;
		}
		finally{
			return bFlag;
		}

	}

	public RghtPrps imgeCount( Map map ){
		return (RghtPrps) getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.imgeCount", map );
	}

	// 영화 insert
	public List movieInsert( Dataset ds ){

		List returnDsList = new ArrayList();
		boolean worksFlag = true;
		boolean movieFlag = true;
		boolean mediaFlag = true;

		RghtPrps movie = new RghtPrps();
		RghtPrps crId = new RghtPrps();

		// 0번째 row는 컬럼정보이므로 제외
		for( int i = 1; i < ds.getRowCount(); i++ ){

			Map map = null;

			try{

				map = getMap( ds, i );

				movie = worksCount( map );

				if( movie.getPRPS_CNT().equals( "0" ) ){

					crId = maxCrId( map );

					map.put( "CR_ID", crId.getPRPS_CNT() );

					worksFlag = worksInsert( map );
					movieFlag = movieInsert( map );
					mediaFlag = mediaInsert( map );
					if( !worksFlag || !movieFlag || !mediaFlag ){
						returnDsList.add( map );
					}
				}else{

					crId = selectCrId( map );

					map.put( "CR_ID", crId.getPRPS_CNT() );

					mediaFlag = mediaInsert( map );
					if( !mediaFlag ){
						returnDsList.add( map );
					}
				}
			}
			catch( Exception e ){
				// TODO Auto-generated catch block
				e.printStackTrace();

				returnDsList.add( map );
			}

		}

		return returnDsList;
	}

	public boolean worksInsert( Map map ) throws SQLException{

		boolean worksFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminInmtMgnt.worksInsert", map );

			worksFlag = true;

		}
		catch( Exception e ){
			e.printStackTrace();
			worksFlag = false;
		}
		finally{
			return worksFlag;
		}
	}

	public boolean movieInsert( Map map ) throws SQLException{

		boolean movieFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminInmtMgnt.movieInsert", map );

			movieFlag = true;

		}
		catch( Exception e ){
			e.printStackTrace();
			movieFlag = false;
		}
		finally{
			return movieFlag;
		}
	}

	public boolean mediaInsert( Map map ) throws SQLException{

		boolean mediaFlag = true;

		try{
			getSqlMapClientTemplate().insert( "AdminInmtMgnt.mediaInsert", map );

			mediaFlag = true;

		}
		catch( Exception e ){
			e.printStackTrace();
			mediaFlag = false;
		}
		finally{
			return mediaFlag;
		}
	}

	public RghtPrps worksCount( Map map ){
		return (RghtPrps) getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.worksCount", map );
	}

	public RghtPrps maxCrId( Map map ){
		return (RghtPrps) getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.maxCrId", map );
	}

	public RghtPrps selectCrId( Map map ){
		return (RghtPrps) getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.selectCrId", map );
	}

	// 법정허락 저작물 신규 아이디 조회 20120904 정병호
	public int getNewWorksId(){
		return Integer.parseInt( "" + getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.getNewWorksId" ) );
	}

	// 법정허락 저작물 등록 20120904 정병호
	public void statWorksInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminInmtMgnt.statWorksInsert", map );
	}

	// 교과용보상금 works_id 가져오기 20120904 정병호
	public int getSubjInmtWorksId( Map map ){
		return Integer.parseInt( ""
			+ getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.getSubjInmtWorksId", map ) );
	}

	// 도서관보상금 works_id 가져오기 20120905 정병호
	public int getLibrInmtWorksId( Map map ){
		return Integer.parseInt( ""
			+ getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.getLibrInmtWorksId", map ) );
	}

	// 방송음악보상금 works_id 가져오기 20120905 정병호
	public int getBrctInmtWorksId( Map map ){
		return Integer.parseInt( ""
			+ getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.getBrctInmtWorksId", map ) );
	}

	// 수업목적보상금 works_id 가져오기 20120905 정병호
	public int getLssnInmtWorksId( Map map ){
		return Integer.parseInt( ""
			+ getSqlMapClientTemplate().queryForObject( "AdminInmtMgnt.getLssnInmtWorksId", map ) );
	}

	// 법정허락 저작물 수정 20120904 정병호
	public void statWorksUpdate( Map map ){
		getSqlMapClientTemplate().update( "AdminInmtMgnt.statWorksUpdate", map );
	}

	// 법정허락 미분배보상금 저작물 등록정보 등록 20120905 정병호
	public void inmtWorksReportInsert( Map map ){
		getSqlMapClientTemplate().insert( "AdminInmtMgnt.inmtWorksReportInsert", map );
	}

	public List inmtMgntReptView( Map map ){
		return getSqlMapClientTemplate().queryForList( "AdminInmtMgnt.inmtMgntReptView", map );
	}

	// 미분배 방송보상금 법정허락 자동전환 여부 업데이트
	public void updateMuscStatYn( Map map ){
		getSqlMapClientTemplate().update( "AdminInmtMgnt.updateMuscStatYn", map );
	}

	// 미분배 도서관보상금 법정허락 자동전환 여부 업데이트
	public void updateLibrStatYn( Map map ){
		getSqlMapClientTemplate().update( "AdminInmtMgnt.updateLibrStatYn", map );
	}

	// 미분배 교과용보상금 법정허락 자동전환 여부 업데이트
	public void updateSubjStatYn( Map map ){
		getSqlMapClientTemplate().update( "AdminInmtMgnt.updateSubjStatYn", map );
	}

	// 미분배 수업목적보상금 법정허락 자동전환 여부 업데이트
	public void updateLssnStatYn( Map map ){
		getSqlMapClientTemplate().update( "AdminInmtMgnt.updateLssnStatYn", map );
	}

	public List imgeInsert( ArrayList<Map<String, Object>> excelList ){
		// TODO Auto-generated method stub
		return null;
	}

	public List movieInsert( ArrayList<Map<String, Object>> excelList ){
		// TODO Auto-generated method stub
		return null;
	}

}
