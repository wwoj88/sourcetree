package kr.or.copyright.mls.console.stats;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.stats.inter.FdcrAd98Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 통계관리 > 검색어 순위 조회
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd98Controller extends DefaultController{

	@Resource( name = "fdcrAd98Service" )
	private FdcrAd98Service fdcrAd98Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd98Controller.class );

	/**
	 * 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/stats/fdcrAd98List1.page" )
	public String fdcrAd98List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd98List1 Start" );
		// 파라미터 셋팅
		fdcrAd98Service.fdcrAd98List1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		logger.debug( "fdcrAd98List1 End" );
		return "/stats/fdcrAd98List1.tiles";
	}
}
