package kr.or.copyright.mls.adminBoard.service;


public interface AdminBoardService {

	public void faqList() throws Exception;

	public void faqDetailList() throws Exception;	
	
	public void faqSave() throws Exception;
	
	public void qnaList() throws Exception;
	
	public void qnaDetailList() throws Exception;	
	
	public void qnaSave() throws Exception;
	
//	public void adminBoardSave() throws Exception;

	/* 양재석 추가 start */
	//관리자 공지사항
	public void sysmNotiList() throws Exception;
	
	public void sysmNotiDetailList() throws Exception;	
	
	public void sysmNotiSave() throws Exception;
	
	//공지사항
	public void notiList() throws Exception;
	
	public void notiDetailList() throws Exception;	
	
	public void notiSave() throws Exception;
	
	//홍보관
	public void promList() throws Exception;
	
	public void promDetailList() throws Exception;	
	
	public void promSave() throws Exception;
	
	//법정허락 저작물 
	public void statList() throws Exception;
	
	public void statSave() throws Exception;
	
	public void statDetailList() throws Exception;
	
	public void clmsWorksList() throws Exception;
	
	public void admSysmNotiList() throws Exception;
	/* 양재석 추가 end */
}
