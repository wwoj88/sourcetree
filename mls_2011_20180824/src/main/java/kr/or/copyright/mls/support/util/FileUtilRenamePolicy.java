package kr.or.copyright.mls.support.util;

import java.io.File;

import com.oreilly.servlet.multipart.*;
import kr.or.copyright.mls.support.util.DateUtil;

public class FileUtilRenamePolicy  implements FileRenamePolicy{
	public File rename(File f) {

		String name = f.getName();
		String body = null;
		String ext = null;
		
		int dot = name.lastIndexOf(".");
		if (dot != -1) {
			body = name.substring(0, dot);
		  	ext = name.substring(dot);  // includes "."
		} else {
		  	body = name;
		  	ext = "";
		}
		
		if (!f.exists()) {
			
			String newName = DateUtil.getCurrDateNumber() + DateUtil.getCurrTimeNumber() + "0000g" + ext;
			f = new File(f.getParent(), newName);
			
			return f;
		} else {
			
			int count = 0;
			String newName = "";
			while (f.exists()) {
				count++;
		  		if (count < 10) {
		  			newName = DateUtil.getCurrDateNumber() + DateUtil.getCurrTimeNumber() + "000g" + count + ext;
		  		} else if (count < 100) {
		  			newName = DateUtil.getCurrDateNumber() + DateUtil.getCurrTimeNumber() + "00g" + count + ext;
		  		} else if (count < 1000) {
					newName = DateUtil.getCurrDateNumber() + DateUtil.getCurrTimeNumber() + "0g" + count + ext;
		  		}
		  		f = new File(f.getParent(), newName);
			}
		}
	return f;
	
	}
}
