package kr.or.copyright.mls.console.email;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.email.inter.FdcrAdA10Dao;

import org.springframework.stereotype.Repository;

@Repository( "fdcrAdA10Dao" )
public class FdcrAdA10DaoImpl extends EgovComAbstractDAO implements FdcrAdA10Dao{
	
	/**
	 * 그룹별 메일폼목록
	 * 
	 * @return
	 */
	public List selectFormGroupList(Map map){
		return getSqlMapClientTemplate().queryForList( "mailForm.selectFormGroupList", map );
	}
	
	// 메뉴 조회
	public Map<String, Object> selectFormInfo(Map map){
		return (Map<String,Object>)getSqlMapClientTemplate().queryForObject( "mailForm.selectFormInfo", map );
	}
	
	public void formUpdate( Map map ){
		getSqlMapClientTemplate().update( "mailForm.formUpdate", map );
	}
	
	/**
	  * <PRE>
	  * 간략 : 
	  * 상세 : 
	  * <PRE>
	  * @param commandMap 
	  */
	public void formInsert( Map map ){
		getSqlMapClientTemplate().insert( "mailForm.formInsert", map );
	}

	/**
	  * <PRE>
	  * 간략 : 
	  * 상세 : 
	  * <PRE>
	  * @param commandMap 
	  */
	public void formDelete( Map map ){
		getSqlMapClientTemplate().delete( "mailForm.formDelete", map );
	}
}
