package kr.or.copyright.mls.console.setup;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 설정 > 보고저작물관리
 * 
 * @author wizksy
 */
@Controller
public class FdcrAdC0Controller extends DefaultController{

	@Resource( name = "fdcrAdC0Service" )
	private FdcrAdC0ServiceImpl fdcrAdC0Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdC0Controller.class );

	/**
	 * 상당한노력 자동화 설정 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/setup/fdcrAdC0List1.page" )
	public String fdcrAdC0List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdC0List1 Start" );
		fdcrAdC0Service.fdcrAdC0List1( commandMap );
		model.addAttribute( "ds_list_stat", commandMap.get( "ds_list_stat" ) );
		return "setup/fdcrAdC0List1.tiles";
	}

	/**
	 * 상당한노력 자동화 수정
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/setup/fdcrAdC0Update1.page" )
	public String fdcrAdC0Update1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdC0Update1 Start" );
		boolean isSuccess = fdcrAdC0Service.fdcrAdC0Update1( commandMap );
		if( isSuccess ){
			return returnUrl( model, "저장했습니다.", "/console/setup/fdcrAdC0List1.page" );
		}else{
			return returnUrl( model, "실패했습니다.", "/console/setup/fdcrAdC0List1.page" );
		}
	}
}
