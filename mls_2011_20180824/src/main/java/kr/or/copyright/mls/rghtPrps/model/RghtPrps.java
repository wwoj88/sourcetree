package kr.or.copyright.mls.rghtPrps.model;

import java.util.List;

import kr.or.copyright.mls.common.BaseObject;
import kr.or.copyright.mls.support.util.HtmlUtility;

public class RghtPrps extends BaseObject{

	//	페이징
	private int nowPage;  
	private int startRow; 
	private int endRow; 
	private int totalRow;
	private String sTotalRow;
	private String ROW_NUM;


	// 권리찾기신청
	private String divs;
	private String[] keyId;
	
	private String PRPS_MAST_KEY;
	private String ORDER_PRPS_MAST_KEY;
	private String PRPS_DIVS;
	private String PRPS_DIVS_NAME;
	private String PRPS_SEQN;
	private String USER_IDNT;
	private String SEQN;
	
	private String PRPS_IDNT_CODE;
	private String PRPS_IDNT_CODE_VALUE;
	private String PRPS_IDNT;
	private String PRPS_IDNT_NR;
	private String PRPS_IDNT_ME;
	
	private String PRPS_DOBL_CODE;
	private String PRPS_DOBL_KEY;
	private String RGHT_PRPS_MAST_KEY;
	private String INMT_PRPS_MAST_KEY;
	private String PRPS_RGHT_CODE;
	private String PRPS_RGHT_CODE_VALUE;
	private String RESD_CORP_NUMB;
	private String HOME_TELX_NUMB;
	private String BUSI_TELX_NUMB;
	private String TELX_NUMB;
	private String MOBL_PHON;
	private String FAXX_NUMB;
	private String MAIL;
	private String HOME_ADDR;
	private String BUSI_ADDR;
	private String PRPS_DESC;
	private String OFFX_LINE_RECP;
	private String COPT_REGI_YSNO;
	private String ATTC_YSNO;
	private String FILE_INFO;			// 신청시 파일정보
	private String DELT_FILE_INFO[] ;	// 삭제파일정보
	private int iResult = 1;			// 결과값
	
	// 저작권자 미상
	private String TRUST_CNT;
	
	private String SINGER_YN;
	private String PLAYER_YN;
	private String CONDUCTOR_YN;
	private String PRODUCER_YN;
	private String LYRICIST_YN;
	private String COMPOSER_YN;
	private String ARRANGER_YN;
	private String TRANSLATOR_YN;
	
	private String WRITER_YN;
	private String COPT_HODR_YN;
	private String DISTRIBUTOR_YN;
	private String INVESTOR_YN;
	private String MAKER_YN;
	
	
	// 신청저작물 배열(음악)
	private String MUSIC_TITLE_ARR[];
	private String ALBUM_TITLE_ARR[];
	private String LYRICIST_ARR[];
	private String COMPOSER_ARR[];
	private String ARRANGER_ARR[];
	private String LYRICIST_ORGN_ARR[];
	private String COMPOSER_ORGN_ARR[];
	private String ARRANGER_ORGN_ARR[];
	private String SINGER_ARR[];
	private String PLAYER_ARR[];
	private String CONDUCTOR_ARR[];
	private String PRODUCER_ARR[];
	private String SINGER_ORGN_ARR[];
	private String PLAYER_ORGN_ARR[];
	private String CONDUCTOR_ORGN_ARR[];
	private String PRODUCER_ORGN_ARR[];
	private String PERF_TIME_ARR[];
	private String ISSUED_DATE_ARR[];
	private String LYRICS_ARR[];
	
	// 도서 
	private String TITLE_ARR[];
	private String BOOK_TITLE_ARR[];
	private String FIRST_EDITION_YEAR_ARR[];
	private String PUBLISHER_ARR[];
	private String MATERIAL_TYPE_ARR[];
	private String PUBLISH_TYPE_ARR[];
	private String TRANSLATOR_ARR[];
	private String TRANSLATOR_ORGN_ARR[];
	private String LICENSOR_NAME_KOR_ARR[];
	private String LICENSOR_NAME_ORGN_KOR_ARR[];
	
	// 뉴스
	private String ARTICL_PUBC_SDATE_ARR[];
	
	// 방송대본
	private String GENRE_KIND_ARR[];
	private String GENRE_KIND_NAME_ARR[];
	private String BROAD_MEDI_ARR[];
	private String BROAD_MEDI_NAME_ARR[];
	private String BROAD_STAT_ARR[];
	private String BROAD_STAT_NAME_ARR[];
	private String BROAD_ORD_ARR[];
	private String BROAD_DATE_ARR[];
	private String DIRECT_ARR[];
	private String MAKER_ARR[];
	private String WRITER_ARR[];
	private String WRITER_ORGN_ARR[];
	
	// 이미지 
	private String WORK_NAME_ARR[];
	private String LISH_COMP_ARR[];
	private String WTER_DIVS_ARR[];
	private String USEX_YEAR_ARR[];
	private String IMAGE_DIVS_ARR[];
	private String COPT_HODR_ARR[];
	private String COPT_HODR_ORGN_ARR[];
	private String OPEN_MEDI_CODE_ARR[]; // 공표매체코드
	private String OPEN_MEDI_TEXT_ARR[]; // 공표매체텍스트  
	private String OPEN_DATE_ARR[]; // 공표일자
	private String SUBJ_INMT_SEQN_ARR[];	// 교과용 보상금 정보
	
	// 영화 
	private String DIRECTOR_ARR[];
	private String LEADING_ACTOR_ARR[];
	private String PRODUCE_DATE_ARR[];
	private String MEDIA_CODE_VALUE_ARR[];
	private String INVESTOR_ARR[];
	private String INVESTOR_ORGN_ARR[];
	private String DISTRIBUTOR_ARR[];
	private String DISTRIBUTOR_ORGN_ARR[];
	
	// 방송
	private String PROG_NAME_ARR[];
	private String MEDI_CODE_ARR[];
	private String CHNL_CODE_ARR[];
	private String MEDI_CODE_NAME_ARR[];
	private String CHNL_CODE_NAME_ARR[];
	private String PROG_ORDSEQ_ARR[];
	private String PROG_GRAD_ARR[];
	private String MAKER_ORGN_ARR[];
	
	private String CHK_1;
	private String CHK_200;
	private String CHK_201;
	private String CHK_202;
	private String CHK_203;
	private String CHK_204;
	private String CHK_205;
	private String CHK_206;
	private String CHK_211;
	private String CHK_212;
	private String CHK_213;
	private String CHK_214;
	private String CHK_215;
	private String CHK_216;
	
	// 첨부파일
	private String ATTC_SEQN;
	private String FILE_NAME;
	private String FILE_PATH;
	private String FILE_SIZE;
	private String REAL_FILE_NAME;
	
	// 신청처리결과
	private String TRST_ORGN_CODE;
	private String TRST_ORGN_CODE_VALUE;
	private String TRST_ORGN_CODE_ARR[];
	private String RSLT_DESC;
	private String DEAL_STAT;
	private String DEAL_STAT_VALUE;
	private String MYML_YSNO;
	private String TRST_YSNO;
	private String DEAL_STAT_FLAG;
	private String RGST_IDNT;
	private String MODI_IDNT;
	private String MODI_IDNT_NAME;
	private String MODI_DTTM;
	
	// 저작권리
	private String RGHT_ROLE_CODE;
	private String HOLD_NAME;
	private String HOLD_NAME_ORGN;
	
	private String PRPS_CNT; //권리찾기 신청건수
	
	// 음악저작물
	private String CHK;
	private String CR_ID;
	private String NR_ID;
	private String ALBUM_ID;
	private String GENRE;
	private String ISSUED_DATE;
	private String TITLE;
	private String LYRICIST;
	private String COMPOSER;
	private String ARRANGER;
	private String TRANSLATOR; // 역자 
	private String LYRICIST_ORGN;
	private String COMPOSER_ORGN;
	private String ARRANGER_ORGN;
	private String TRANSLATOR_ORGN;
	private String ALBUM_TITLE;
	private String MUSIC_TITLE;
	private String SINGER_ORGN;
	private String PLAYER_ORGN;
	private String CONDUCTOR_ORGN;
	private String FEATURING_ORGN;
	private String PRODUCER_ORGN;
	private String SINGER;
	private String PLAYER;
	private String CONDUCTOR;
	private String FEATURING;
	private String PRODUCER;
	private String NONPERF;
	private String ICN_NUMB;
	private String CA201ID;
	private String CA202ID;
	private String CA203ID;
	private String ALBUM_PRODUCED_CRH;
	private String INSERTDATE;
	private String TRUST_YN;
	private String PAK_TRUST_YN;
	private String KAPP_TRUST_YN;
	private String DISK_NO;
	private String TRACK_NO;
	private String INSERT_DATE;
	private String EDITION;
	private String ALBUM_TYPE;
	private String ALBUM_MEDIA_TYPE;
	private String PERFORMANCE_TIME;
	private String ALBUM_LABLE_NO;
	private String ALBUM_LABLE_NAME;
	private String MUSIC_TYPE;
	private String PERF_TIME;
	private String LYRICS;
	private String TOTAL_TRACK;
	private String recFile;
	
	//	어문저작물
	private String BOOK_NR_ID;
	private String LICENSOR_NAME_KOR;
	private String LICENSOR_NAME_KOR_ORGN;
	private String SUBTITLE;  // 도서부제 
	private String FIRST_EDITION_YEAR;
	private String PUBLISH_TYPE;
	private String PUBLISH_TYPE_NAME;
	private String RETRIEVE_TYPE;
	private String RETRIEVE_TYPE_NAME;
	private String PUBLISHER;
	private String WORKS_TITLE;  // 어문저작물명 
	private String WORKS_SUBTITLE;  // 어문저작물부제 
	private String MATERIAL_TYPE;  // 장르구분
	private String STAGE_NAME;  // 예필명
	private String CA_NAME;  // 권리기관명 
	private String ISBN_ISSN;
	private String BOOK_TITLE;
	private String KRTRA_TRUST_YN;  // 복전협 신탁여부 
	
	// 방송대본 저작물
	private String WRITER;
	private String WRITER_ORGN;
	private String GENRE_KIND;  // 장르분류
	private String GENRE_KIND_NAME;  
	private String SUBJ_KIND;  // 소재분류
	private String SUBJ_KIND_NAME; 
	private String DIRECT; // 연출
	private String CRHID_OF_CA; // 방작협 관리번호
	private String CAID;  // 방작협 신탁기관 코드
	private String ORIG_WORK;  // 원작명
	private String ORIG_WRITER;  // 원작자
	private String PLAYERS;  // 주요출연진
	private String SYNNOPSIS;  // 줄거리
	private String BROAD_ORD;  // 방송회차
	private String BROAD_DATE;  // 방송일시
	private String BROAD_MEDI; // 방송매체코드
	private String BROAD_MEDI_NAME;  // 방송매체
	private String BROAD_STAT;  // 방송사코드
	private String BROAD_STAT_NAME;  // 방송사
	private String TELE_TIME;  // 방송시간(분)
	private String MAKER;  // 제작사
	private String MC;  // 진행자
	private String BROAD_DATE2;  // 방송일시
	
	// 이미지저작물
	private String IMGE_SEQN;
	private String WORK_NAME;
	private String COPT_HODR;
	private String COPT_HODR_ORGN;
	private String LISH_COMP;
	private String WTER_DIVS;
	private String USEX_YEAR;
	private String IMAGE_DIVS;
	private String WORK_FILE_NAME;
	private String OPEN_MEDI_CODE; // 공표매체코드
	private String OPEN_MEDI_TEXT; // 공표매체텍스트  
	private String OPEN_DATE; // 공표일자
	private String SUBJ_INMT_SEQN; // 교과용 보상금 정보
	private String SCHL;
	private String BOOK_DIVS;
	private String SUBJ_NAME;
	private String BOOK_SIZE_DIVS;
	private String SCHL_YEAR_DIVS;
	private String AUTR_DIVS;
	private String WORK_DIVS;
	
	// 영화저작물
	private String MEDIA_ID;
	private String TITLE_ORG;			// 원문제목
	private String PROD_HOLDER;	// 제작자
	private String CIRC_HOLDER;	// 유통사
	private String PUBL_HOLDER;	// 발매사
	private String DIST_HOLDER;	// 배급사
	private String INVESTOR;			// 투자사
	private String MV_PRODUCER;	// 영화제작사
	private String MV_WRITER;		//  영화작가
	private String DISTRIBUTOR;	// 배급사
	private String INVESTOR_ORGN;			// 투자사
	private String DISTRIBUTOR_ORGN;	// 배급사
	private String PRODUCE_YEAR;	// 제작년도
	private String DIRECTOR;			// 감독_연출자
	private String DIRECTOR_VIEW;
	private String LEADING_ACTOR;	// 출연진
	private String LEADING_ACTOR_VIEW;
	private String GENRE_VALUE;		// 영화장르명
	private String MOVIE_TYPE;			// 영화형태
	private String MOVIE_TYPE_VALUE;	
	private String VIEW_GRADE;			// 관람등급
	private String VIEW_GRADE_VALUE;
	private String RUN_TIME;				// 상영시간
	private String RELEASE_DATE;		// 개봉일자
	private String CRH_ID_OF_CA;
	private String KMVA_CODE;			// 영산협 id
	private String MOVIE_MGNT_SEQN;
	private String stdCrhId;			// icn
	private String CANAME;				// 권리기관명
	private String MEDIA_CODE;		// 매체코드
	private String MEDIA_CODE_VALUE;	
	private String PRODUCE_DATE;			// 제작일
	private String DISCLOSURE_DATE;	// 공표일
	private String MEDIA_VIEW_GRADE_VIEW;	// 매체관람등급
	private String KOFIC_TRUST_YN;  // 영진위 신탁여부 
	private String POST_URS; // 포스트 URL
	
	// 방송저작물
	private String PROG_CODE; // 프로그램코드
	private String PROG_NAME; // 프로그램명
	private String ORGI_NAME; // 원제
	private String MEDI_CODE; // 매체코드
	private String MEDI_CODE_NAME; // 매체코드명
	private String CHNL_CODE; // 채널코드
	private String CHNL_CODE_NAME; // 채널코드명
	private String BROAD_GENR; // 방송위장르
	private String PROG_ID; // 프로그램id
	private String PROG_ORDSEQ; // 프로그램회차
	private String SUBPR_NAME; // 부제
	private String SMPRG_NAME; // 소제
	private String PART_NAME; // 부순명칭
	private String MAIN_STORY; // 주요내용
	private String PROD_QULTY; // 제작품질
	private String PTABL_TITLE; // 편성표제목
	private String PROG_GRAD; // 프로그램등급
	private String MAKER_ORGN; 
	
	// 뉴스저작물
	private String REPORTER_NM;
	private String PROVIDER_NM_ORGN;
	private String PROVIDER_NM;
	private String ARTICL_PUBC_SDATE;
	private String ARTICL_DT;
	private String PROVIDER_CD;
	private String COI_CODE;
	private String LINK_PAGE_URL;
	private String PROVIDER_YN;
	
	// 조회조건
	private String DIVS;
	private String srchTitle;
	private String srchTitleLen;
	private String srchProducer;
	private String srchAlbumTitle;
	private String srchSinger;
	private String srchStartDate;
	private String srchEndDate;
	private String srchLicensor;
	private String srchNonPerf;			// 미확인실연자
	private String srchNoneName;		// 저작권미상 저작자명
	private String srchNoneRole;			// 저작권미상 항목
	private String srchNoneRole_arr[];// 저작권미상 항목 배열
	private String srchSdsrName;
	private String srchMuciName;
	private String srchYymm;
	private String srchWterDivs;			// 집필진
	private String srchPublisher;  // 출판사 
	private String srchBookTitle; // 도서명 
	private String srchLicensorNm;  //저자명 
	private String srchPrpsDivs;  // 신청내역 구분 
	private String srchYear;	// 제작년도
	private String srchDirector; // 감독명
	private String srchViewGrade;	// 관람등급
	private String srchActor;	// 출연진
	private String srchDistributor; // 배급사
	private String srchWorkName; // 이미지
	private String srchLishComp; // 출판사 
	private String srchCoptHodr; // 저자명 
	private String srchWriter;  // 작가명
	private String srchBroadStatName;  // 방송사
	private String srchProgName;  // 프로그램명
	private String srchMaker;  // 제작자 
	private String srchDirect; //연출가
	private String srchPlayers; //주요출연진
	private String srchPrpsRghtCode; //신청목적
	private String srchProviderVcd;
	/* 정병호 뉴스 검색 조건 추가 str */
	private String srchProviderNm;
	
	
	private String CNT;
	private String MAX_DEAL_STAT;
	private String PRPS_TITE;
	private String PRPS_TITE_VIEW;
	private String RGST_DTTM;
	private String SDSR_NAME;
	private String TOTAL_STAT;
	private String USER_NAME;
	private String ALBM_NAME;
	private String MUCI_NAME;
	private String LYRI_WRTR;
	private String COMS_WRTR;
	private String ARRG_WRTR;
	private String INMT_SEQN;
	private String PUBC_YEAR;
	
	//회원
	private String USER_DIVS;
	private String RESD_CORP_NUMB_VIEW;
	private String CORP_NUMB;
	
	//session 정보
	private String SESS_U_MAIL;
	private String SESS_U_MAIL_RECE_YSNO;
	private String SESS_U_MOBL_PHON;
	private String SESS_U_SMS_RECE_YSNO;
	
	// 기타
	private String PRPS_SIDE_GENRE; // 신청저작물장르
	private String PRPS_SIDE_R_GENRE; 
	private String PRPS_SIDE_R_GENRE_ARR[];
	private String PRPS_RGHT_DESC; //신청저작물설명
	private String DIVS_CODE;
	private String BIG_CODE;
	private String ETC_TRST_ORGN_CODE;
	private String SIDE_COPT_HODR;
	
	private List fileList;

	private String ISSUED_DATE2;
	
	
	
	//====================================================
	
	// 음악
	public String getMUSIC_TITLE_TRNS() {
		return HtmlUtility.translateS(MUSIC_TITLE);
	}
	public String getALBUM_TITLE_TRNS() {
		return HtmlUtility.translateS(ALBUM_TITLE);
	}
	public String getLYRICIST_TRNS() {
		return HtmlUtility.translateS(LYRICIST);
	}
	public String getLYRICIST_ORGN_TRNS() {
		return HtmlUtility.translateS(LYRICIST_ORGN);
	}
	public String getCOMPOSER_TRNS() {
		return HtmlUtility.translateS(COMPOSER);
	}
	public String getCOMPOSER_ORGN_TRNS() {
		return HtmlUtility.translateS(COMPOSER_ORGN);
	}
	public String getSINGER_TRNS() {
		return HtmlUtility.translateS(SINGER);
	}
	public String getSINGER_ORGN_TRNS() {
		return HtmlUtility.translateS(SINGER_ORGN);
	}
	public String getPRODUCER_TRNS() {
		return HtmlUtility.translateS(PRODUCER);
	}
	public String getPRODUCER_ORGN_TRNS() {
		return HtmlUtility.translateS(PRODUCER_ORGN);
	}
	public String getARRANGER_TRNS() {
		return HtmlUtility.translateS(ARRANGER);
	}
	public String getARRANGER_ORGN_TRNS() {
		return HtmlUtility.translateS(ARRANGER_ORGN);
	}
	public String getTRANSLATOR_TRNS() {
		return HtmlUtility.translateS(TRANSLATOR);
	}
	public String getTRANSLATOR_ORGN_TRNS() {
		return HtmlUtility.translateS(TRANSLATOR_ORGN);
	}
	public String getPLAYER_TRNS() {
		return HtmlUtility.translateS(PLAYER);
	}
	public String getPLAYER_ORGN_TRNS() {
		return HtmlUtility.translateS(PLAYER_ORGN);
	}
	public String getCONDUCTOR_TRNS() {
		return HtmlUtility.translateS(CONDUCTOR);
	}
	public String getCONDUCTOR_ORGN_TRNS() {
		return HtmlUtility.translateS(CONDUCTOR_ORGN);
	}
	public String getFEATURING_TRNS() {
		return HtmlUtility.translateS(FEATURING);
	}
	public String getFEATURING_ORGN_TRNS() {
		return HtmlUtility.translateS(FEATURING_ORGN);
	}
	public String getALBUM_PRODUCED_CRH_TRNS() {
		return HtmlUtility.translateS(ALBUM_PRODUCED_CRH);
	}
	public String getNONPERF_TRNS() {
		return HtmlUtility.translateS(NONPERF);
	}
	
	private String MUSIC_TITLE_ARR_TRNS[] ;
	private String ALBUM_TITLE_ARR_TRNS[] ;
	
	private String LYRICIST_ARR_TRNS[] ;
	private String COMPOSER_ARR_TRNS[] ;
	private String ARRANGER_ARR_TRNS[] ;
	private String SINGER_ARR_TRNS[] ;
	private String PLAYER_ARR_TRNS[] ;
	private String CONDUCTOR_ARR_TRNS[] ;
	private String PRODUCER_ARR_TRNS[] ;
	
	private String LYRICIST_ORGN_ARR_TRNS[] ;
	private String COMPOSER_ORGN_ARR_TRNS[] ;
	private String ARRANGER_ORGN_ARR_TRNS[] ;
	private String SINGER_ORGN_ARR_TRNS[] ;
	private String PLAYER_ORGN_ARR_TRNS[] ;
	private String CONDUCTOR_ORGN_ARR_TRNS[] ;
	private String PRODUCER_ORGN_ARR_TRNS[] ;
	
	public String[] getMUSIC_TITLE_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [MUSIC_TITLE_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(MUSIC_TITLE_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getALBUM_TITLE_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [ALBUM_TITLE_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(ALBUM_TITLE_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getLYRICIST_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [LYRICIST_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(LYRICIST_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getCOMPOSER_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [COMPOSER_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(COMPOSER_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getARRANGER_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [ARRANGER_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(ARRANGER_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getSINGER_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [SINGER_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(SINGER_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getPLAYER_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [PLAYER_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(PLAYER_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}	
	
	public String[] getCONDUCTOR_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [CONDUCTOR_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(CONDUCTOR_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}	
	
	public String[] getPRODUCER_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [PRODUCER_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(PRODUCER_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}	
	
	public String[] getLYRICIST_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [LYRICIST_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(LYRICIST_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getCOMPOSER_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [COMPOSER_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(COMPOSER_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getARRANGER_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [ARRANGER_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(ARRANGER_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getSINGER_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [SINGER_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(SINGER_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getPLAYER_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [PLAYER_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(PLAYER_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}	
	
	public String[] getCONDUCTOR_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [CONDUCTOR_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(CONDUCTOR_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}	
	
	public String[] getPRODUCER_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [PRODUCER_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(PRODUCER_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}	
	
	// 어문
	public String getTITLE_TRNS() {
		return HtmlUtility.translateS(TITLE);
	}
	public String getBOOK_TITLE_TRNS() {
		return HtmlUtility.translateS(BOOK_TITLE);
	}
	public String getSUBTITLE_TRNS() {
		return HtmlUtility.translateS(SUBTITLE);
	}	
	public String getLICENSOR_NAME_KOR_TRNS() {
		return HtmlUtility.translateS(LICENSOR_NAME_KOR);
	}
	public String getLICENSOR_NAME_KOR_ORGN_TRNS() {
		return HtmlUtility.translateS(LICENSOR_NAME_KOR_ORGN);
	}
	public String getPUBLISHER_TRNS() {
		return HtmlUtility.translateS(PUBLISHER);
	}
	
	private String ARTICL_PUBC_SDATE_ARR_TRNS[];
	
	public String[] getARTICL_PUBC_SDATE_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [ARTICL_PUBC_SDATE_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(ARTICL_PUBC_SDATE_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	private String TITLE_ARR_TRNS[] ;
	private String BOOK_TITLE_ARR_TRNS[] ;
	private String SUBTITLE_ARR_TRNS[] ;
	private String PUBLISHER_ARR_TRNS[] ;
	private String LICENSOR_NAME_KOR_ARR_TRNS[] ;
	private String TRANSLATOR_ARR_TRNS[] ;
	private String LICENSOR_NAME_KOR_ORGN_ARR_TRNS[] ;
	private String TRANSLATOR_ORGN_ARR_TRNS[] ;
	
	public String[] getTITLE_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [TITLE_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(TITLE_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getBOOK_TITLE_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [BOOK_TITLE_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(BOOK_TITLE_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getSUBTITLE_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [SUBTITLE_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(SUBTITLE_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getPUBLISHER_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [PUBLISHER_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(PUBLISHER_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getLICENSOR_NAME_KOR_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [LICENSOR_NAME_KOR_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(LICENSOR_NAME_KOR_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getTRANSLATOR_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [TRANSLATOR_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(TRANSLATOR_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getLICENSOR_NAME_KOR_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [LICENSOR_NAME_KOR_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(LICENSOR_NAME_KOR_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getTRANSLATOR_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [TRANSLATOR_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(TRANSLATOR_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	

	// 방송대본
	public String getWRITER_TRNS() {
		return HtmlUtility.translateS(WRITER);
	}
	public String getWRITER_ORGN_TRNS() {
		return HtmlUtility.translateS(WRITER_ORGN);
	}
	public String getDIRECT_TRNS() {
		return HtmlUtility.translateS(DIRECT);
	}
	public String getBROAD_MEDI_NAME_TRNS() {
		return HtmlUtility.translateS(BROAD_MEDI_NAME);
	}
	public String getBROAD_STAT_NAME_TRNS() {
		return HtmlUtility.translateS(BROAD_STAT_NAME);
	}
	public String getMAKER_TRNS() {
		return HtmlUtility.translateS(MAKER);
	}
	public String getMAKER_ORGN_TRNS() {
		return HtmlUtility.translateS(MAKER_ORGN);
	}
	public String getPLAYERS_TRNS() {
		return HtmlUtility.translateS(PLAYERS);
	}
	
	private String WRITER_ARR_TRNS[] ;
	private String WRITER_ORGN_ARR_TRNS[] ;
	private String DIRECT_ARR_TRNS[] ;
	private String BROAD_MEDI_NAME_ARR_TRNS[] ;
	private String BROAD_STAT_NAME_ARR_TRNS[] ;
	private String MAKER_ARR_TRNS[] ;
	
	public String[] getWRITER_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [WRITER_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(WRITER_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getWRITER_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [WRITER_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(WRITER_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getDIRECT_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [DIRECT_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(DIRECT_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getBROAD_MEDI_NAME_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [BROAD_MEDI_NAME_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(BROAD_MEDI_NAME_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getBROAD_STAT_NAME_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [BROAD_STAT_NAME_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(BROAD_STAT_NAME_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getMAKER_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [MAKER_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(MAKER_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	
	// 이미지
	public String getWORK_NAME_TRNS() {
		return HtmlUtility.translateS(WORK_NAME);
	}
	public String getCOPT_HODR_TRNS() {
		return HtmlUtility.translateS(COPT_HODR);
	}
	public String getCOPT_HODR_ORGN_TRNS() {
		return HtmlUtility.translateS(COPT_HODR_ORGN);
	}
	public String getLISH_COMP_TRNS() {
		return HtmlUtility.translateS(LISH_COMP);
	}
	public String getWTER_DIVS_TRNS() {
		return HtmlUtility.translateS(WTER_DIVS);
	}
	public String getIMAGE_DIVS_TRNS() {
		return HtmlUtility.translateS(IMAGE_DIVS);
	}
	public String getOPEN_MEDI_TEXT_TRNS(){
		return HtmlUtility.translateS(OPEN_MEDI_TEXT);
	}
	
	private String WORK_NAME_ARR_TRNS[] ;
	private String IMAGE_DIVS_ARR_TRNS[] ;
	private String OPEN_MEDI_TEXT_ARR_TRNS[] ;
	private String COPT_HODR_ARR_TRNS[];
	private String COPT_HODR_ORGN_ARR_TRNS[];
	
	public String[] getWORK_NAME_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [WORK_NAME_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(WORK_NAME_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getIMAGE_DIVS_ARR_TRNS() {
			
		String TMP_TRNS[] = new String [IMAGE_DIVS_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(IMAGE_DIVS_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}

	public String[] getOPEN_MEDI_TEXT_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [OPEN_MEDI_TEXT_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(OPEN_MEDI_TEXT_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getCOPT_HODR_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [COPT_HODR_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(COPT_HODR_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getCOPT_HODR_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [COPT_HODR_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(COPT_HODR_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}

	// 영화
	public String getDIRECTOR_TRNS() {
		return HtmlUtility.translateS(DIRECTOR);
	}
	public String getLEADING_ACTOR_TRNS() {
		return HtmlUtility.translateS(LEADING_ACTOR);
	}
	public String getVIEW_GRADE_VALUE_TRNS() {
		return HtmlUtility.translateS(VIEW_GRADE_VALUE);
	}
	public String getDISTRIBUTOR_TRNS() {
		return HtmlUtility.translateS(DISTRIBUTOR);
	}
	public String getDISTRIBUTOR_ORGN_TRNS() {
		return HtmlUtility.translateS(DISTRIBUTOR_ORGN);
	}
	public String getINVESTOR_TRNS() {
		return HtmlUtility.translateS(INVESTOR);
	}
	public String getINVESTOR_ORGN_TRNS() {
		return HtmlUtility.translateS(INVESTOR_ORGN);
	}
	
	private String DIRECTOR_ARR_TRNS[] ;
	private String LEADING_ACTOR_ARR_TRNS[] ;
	private String INVESTOR_ARR_TRNS[] ;
	private String DISTRIBUTOR_ARR_TRNS[] ;
	private String INVESTOR_ORGN_ARR_TRNS[] ;
	private String DISTRIBUTOR_ORGN_ARR_TRNS[] ;

	public String[] getDIRECTOR_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [DIRECTOR_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(DIRECTOR_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getLEADING_ACTOR_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [LEADING_ACTOR_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(LEADING_ACTOR_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getINVESTOR_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [INVESTOR_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(INVESTOR_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getDISTRIBUTOR_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [DISTRIBUTOR_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(DISTRIBUTOR_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getINVESTOR_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [INVESTOR_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(INVESTOR_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getDISTRIBUTOR_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [DISTRIBUTOR_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(DISTRIBUTOR_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	
	//방송
	public String getPROG_NAME_TRNS() {
		return HtmlUtility.translateS(PROG_NAME);
	}
	public String getPROG_ORDSEQ_TRNS() {
		return HtmlUtility.translateS(PROG_ORDSEQ);
	}
	public String getMEDI_CODE_NAME_TRNS() {
		return HtmlUtility.translateS(MEDI_CODE_NAME);
	}
	public String getCHNL_CODE_NAME_TRNS() {
		return HtmlUtility.translateS(CHNL_CODE_NAME);
	}
	
	private String PROG_NAME_ARR_TRNS[] ;
	private String MEDI_CODE_NAME_ARR_TRNS[] ;
	private String CHNL_CODE_NAME_ARR_TRNS[] ;
	private String PROG_ORDSEQ_ARR_TRNS[] ;
	private String MAKER_ORGN_ARR_TRNS[] ;
	
	public String[] getPROG_NAME_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [PROG_NAME_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(PROG_NAME_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}	
	
	public String[] getMEDI_CODE_NAME_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [MEDI_CODE_NAME_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(MEDI_CODE_NAME_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getCHNL_CODE_NAME_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [CHNL_CODE_NAME_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(CHNL_CODE_NAME_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getPROG_ORDSEQ_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [PROG_ORDSEQ_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(PROG_ORDSEQ_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	
	public String[] getMAKER_ORGN_ARR_TRNS() {
		
		String TMP_TRNS[] = new String [MAKER_ORGN_ARR_TRNS.length];
		
		for(int i=0; i<TMP_TRNS.length; i++) {
			TMP_TRNS[i] = HtmlUtility.translateS(MAKER_ORGN_ARR_TRNS[i]);
		}
		return TMP_TRNS;
	}
	//====================================================
	
	public String getBROAD_GENR() {
		return BROAD_GENR;
	}

	public void setBROAD_GENR(String broad_genr) {
		BROAD_GENR = broad_genr;
	}

	public String getCHNL_CODE() {
		return CHNL_CODE;
	}

	public void setCHNL_CODE(String chnl_code) {
		CHNL_CODE = chnl_code;
	}

	public String getCHNL_CODE_NAME() {
		return CHNL_CODE_NAME;
	}

	public void setCHNL_CODE_NAME(String chnl_code_name) {
		CHNL_CODE_NAME = chnl_code_name;
	}

	public String getMAIN_STORY() {
		return MAIN_STORY;
	}

	public void setMAIN_STORY(String main_story) {
		MAIN_STORY = main_story;
	}

	public String getMEDI_CODE() {
		return MEDI_CODE;
	}

	public void setMEDI_CODE(String medi_code) {
		MEDI_CODE = medi_code;
	}

	public String getMEDI_CODE_NAME() {
		return MEDI_CODE_NAME;
	}

	public void setMEDI_CODE_NAME(String medi_code_name) {
		MEDI_CODE_NAME = medi_code_name;
	}

	public String getORGI_NAME() {
		return ORGI_NAME;
	}

	public void setORGI_NAME(String orgi_name) {
		ORGI_NAME = orgi_name;
	}

	public String getPART_NAME() {
		return PART_NAME;
	}

	public void setPART_NAME(String part_name) {
		PART_NAME = part_name;
	}

	public String getPROD_QULTY() {
		return PROD_QULTY;
	}

	public void setPROD_QULTY(String prod_qulty) {
		PROD_QULTY = prod_qulty;
	}

	public String getPROG_CODE() {
		return PROG_CODE;
	}

	public void setPROG_CODE(String prog_code) {
		PROG_CODE = prog_code;
	}

	public String getPROG_GRAD() {
		return PROG_GRAD;
	}

	public void setPROG_GRAD(String prog_grad) {
		PROG_GRAD = prog_grad;
	}

	public String getMAKER_ORGN() {
		return MAKER_ORGN;
	}

	public void setMAKER_ORGN(String maker_orgn) {
		MAKER_ORGN = maker_orgn;
	}

	public String getPROG_ID() {
		return PROG_ID;
	}

	public void setPROG_ID(String prog_id) {
		PROG_ID = prog_id;
	}

	public String getPROG_NAME() {
		return PROG_NAME;
	}

	public void setPROG_NAME(String prog_name) {
		PROG_NAME = prog_name;
	}

	public String getPROG_ORDSEQ() {
		return PROG_ORDSEQ;
	}

	public void setPROG_ORDSEQ(String prog_ordseq) {
		PROG_ORDSEQ = prog_ordseq;
	}

	public String getPTABL_TITLE() {
		return PTABL_TITLE;
	}

	public void setPTABL_TITLE(String ptabl_title) {
		PTABL_TITLE = ptabl_title;
	}

	public String getSMPRG_NAME() {
		return SMPRG_NAME;
	}

	public void setSMPRG_NAME(String smprg_name) {
		SMPRG_NAME = smprg_name;
	}

	public String getSrchMaker() {
		return srchMaker;
	}

	public void setSrchMaker(String srchMaker) {
		this.srchMaker = srchMaker;
	}

	public String getSrchProgName() {
		return srchProgName;
	}

	public void setSrchProgName(String srchProgName) {
		this.srchProgName = srchProgName;
	}

	public String getSUBPR_NAME() {
		return SUBPR_NAME;
	}

	public void setSUBPR_NAME(String subpr_name) {
		SUBPR_NAME = subpr_name;
	}

	public String getBROAD_DATE() {
		return BROAD_DATE;
	}

	public void setBROAD_DATE(String broad_date) {
		BROAD_DATE = broad_date;
	}

	public String getBROAD_MEDI() {
		return BROAD_MEDI;
	}

	public void setBROAD_MEDI(String broad_medi) {
		BROAD_MEDI = broad_medi;
	}

	public String getBROAD_MEDI_NAME() {
		return BROAD_MEDI_NAME;
	}

	public void setBROAD_MEDI_NAME(String broad_medi_name) {
		BROAD_MEDI_NAME = broad_medi_name;
	}

	public String getBROAD_ORD() {
		return BROAD_ORD;
	}

	public void setBROAD_ORD(String broad_ord) {
		BROAD_ORD = broad_ord;
	}

	public String getBROAD_STAT() {
		return BROAD_STAT;
	}

	public void setBROAD_STAT(String broad_stat) {
		BROAD_STAT = broad_stat;
	}

	public String getBROAD_STAT_NAME() {
		return BROAD_STAT_NAME;
	}

	public void setBROAD_STAT_NAME(String broad_stat_name) {
		BROAD_STAT_NAME = broad_stat_name;
	}

	public String getCAID() {
		return CAID;
	}

	public void setCAID(String caid) {
		CAID = caid;
	}

	public String getCHK_206() {
		return CHK_206;
	}

	public void setCHK_206(String chk_206) {
		CHK_206 = chk_206;
	}

	public String getCRHID_OF_CA() {
		return CRHID_OF_CA;
	}

	public void setCRHID_OF_CA(String crhid_of_ca) {
		CRHID_OF_CA = crhid_of_ca;
	}

	public String getDIRECT() {
		return DIRECT;
	}

	public void setDIRECT(String direct) {
		DIRECT = direct;
	}

	public String getGENRE_KIND() {
		return GENRE_KIND;
	}

	public void setGENRE_KIND(String genre_kind) {
		GENRE_KIND = genre_kind;
	}

	public String getGENRE_KIND_NAME() {
		return GENRE_KIND_NAME;
	}

	public void setGENRE_KIND_NAME(String genre_kind_name) {
		GENRE_KIND_NAME = genre_kind_name;
	}

	public String getMAKER() {
		return MAKER;
	}

	public void setMAKER(String maker) {
		MAKER = maker;
	}

	public String getMC() {
		return MC;
	}

	public void setMC(String mc) {
		MC = mc;
	}

	public String getORIG_WORK() {
		return ORIG_WORK;
	}

	public void setORIG_WORK(String orig_work) {
		ORIG_WORK = orig_work;
	}

	public String getORIG_WRITER() {
		return ORIG_WRITER;
	}

	public void setORIG_WRITER(String orig_writer) {
		ORIG_WRITER = orig_writer;
	}

	public String getPLAYERS() {
		return PLAYERS;
	}

	public void setPLAYERS(String players) {
		PLAYERS = players;
	}

	public String getSrchBroadStatName() {
		return srchBroadStatName;
	}

	public void setSrchBroadStatName(String srchBroadStatName) {
		this.srchBroadStatName = srchBroadStatName;
	}

	public String getSrchWriter() {
		return srchWriter;
	}

	public void setSrchWriter(String srchWriter) {
		this.srchWriter = srchWriter;
	}

	public String getSUBJ_KIND() {
		return SUBJ_KIND;
	}

	public void setSUBJ_KIND(String subj_kind) {
		SUBJ_KIND = subj_kind;
	}

	public String getSUBJ_KIND_NAME() {
		return SUBJ_KIND_NAME;
	}

	public void setSUBJ_KIND_NAME(String subj_kind_name) {
		SUBJ_KIND_NAME = subj_kind_name;
	}

	public String getSYNNOPSIS() {
		return SYNNOPSIS;
	}

	public void setSYNNOPSIS(String synnopsis) {
		SYNNOPSIS = synnopsis;
	}

	public String getTELE_TIME() {
		return TELE_TIME;
	}

	public void setTELE_TIME(String tele_time) {
		TELE_TIME = tele_time;
	}

	public String getWRITER() {
		return WRITER;
	}

	public String getWRITER_ORGN() {
		return WRITER_ORGN;
	}

	public void setWRITER_ORGN(String writer_orgn) {
		WRITER_ORGN = writer_orgn;
	}

	public void setWRITER(String writer) {
		WRITER = writer;
	}

	public String getCOPT_HODR_ORGN() {
		return COPT_HODR_ORGN;
	}

	public void setCOPT_HODR_ORGN(String copt_hodr_orgn) {
		COPT_HODR_ORGN = copt_hodr_orgn;
	}

	public String[] getCOPT_HODR_ORGN_ARR() {
		return COPT_HODR_ORGN_ARR;
	}

	public void setCOPT_HODR_ORGN_ARR(String[] copt_hodr_orgn_arr) {
		COPT_HODR_ORGN_ARR = copt_hodr_orgn_arr;
		COPT_HODR_ORGN_ARR_TRNS = copt_hodr_orgn_arr;
	}

	public String getOPEN_DATE() {
		return OPEN_DATE;
	}

	public void setOPEN_DATE(String open_date) {
		OPEN_DATE = open_date;
	}

	public String[] getOPEN_DATE_ARR() {
		return OPEN_DATE_ARR;
	}

	public void setOPEN_DATE_ARR(String[] open_date_arr) {
		OPEN_DATE_ARR = open_date_arr;
	}

	public String getOPEN_MEDI_CODE() {
		return OPEN_MEDI_CODE;
	}

	public void setOPEN_MEDI_CODE(String open_medi_code) {
		OPEN_MEDI_CODE = open_medi_code;
	}

	public String[] getOPEN_MEDI_CODE_ARR() {
		return OPEN_MEDI_CODE_ARR;
	}

	public void setOPEN_MEDI_CODE_ARR(String[] open_medi_code_arr) {
		OPEN_MEDI_CODE_ARR = open_medi_code_arr;
	}

	public String getOPEN_MEDI_TEXT() {
		return OPEN_MEDI_TEXT;
	}

	public void setOPEN_MEDI_TEXT(String open_medi_text) {
		OPEN_MEDI_TEXT = open_medi_text;
	}

	public String[] getOPEN_MEDI_TEXT_ARR() {
		return OPEN_MEDI_TEXT_ARR;
	}

	public void setOPEN_MEDI_TEXT_ARR(String[] open_medi_text_arr) {
		OPEN_MEDI_TEXT_ARR = open_medi_text_arr;
		OPEN_MEDI_TEXT_ARR_TRNS = open_medi_text_arr;
	}

	public String getMV_PRODUCER() {
		return MV_PRODUCER;
	}

	public void setMV_PRODUCER(String mv_producer) {
		MV_PRODUCER = mv_producer;
	}
	
	public int getNowPage() {
		return nowPage;
	}

	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public int getTotalRow() {
		return totalRow;
	}

	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}

	public String getDivs() {
		return divs;
	}

	public void setDivs(String divs) {
		this.divs = divs;
	}

	public String[] getKeyId() {
		return keyId;
	}

	public void setKeyId(String[] keyId) {
		this.keyId = keyId;
	}

	public String getPRPS_MAST_KEY() {
		return PRPS_MAST_KEY;
	}

	public void setPRPS_MAST_KEY(String prps_mast_key) {
		PRPS_MAST_KEY = prps_mast_key;
	}

	public String getORDER_PRPS_MAST_KEY() {
		return ORDER_PRPS_MAST_KEY;
	}

	public void setORDER_PRPS_MAST_KEY(String order_prps_mast_key) {
		ORDER_PRPS_MAST_KEY = order_prps_mast_key;
	}

	public String getPRPS_DIVS() {
		return PRPS_DIVS;
	}

	public void setPRPS_DIVS(String prps_divs) {
		PRPS_DIVS = prps_divs;
	}

	public String getPRPS_DIVS_NAME() {
		return PRPS_DIVS_NAME;
	}

	public void setPRPS_DIVS_NAME(String prps_divs_name) {
		PRPS_DIVS_NAME = prps_divs_name;
	}

	public String getPRPS_SEQN() {
		return PRPS_SEQN;
	}

	public void setPRPS_SEQN(String prps_seqn) {
		PRPS_SEQN = prps_seqn;
	}

	public String getUSER_IDNT() {
		return USER_IDNT;
	}

	public void setUSER_IDNT(String user_idnt) {
		USER_IDNT = user_idnt;
	}

	public String getSEQN() {
		return SEQN;
	}

	public void setSEQN(String seqn) {
		SEQN = seqn;
	}

	public String getPRPS_IDNT_CODE() {
		return PRPS_IDNT_CODE;
	}

	public void setPRPS_IDNT_CODE(String prps_idnt_code) {
		PRPS_IDNT_CODE = prps_idnt_code;
	}

	public String getPRPS_IDNT_CODE_VALUE() {
		return PRPS_IDNT_CODE_VALUE;
	}

	public void setPRPS_IDNT_CODE_VALUE(String prps_idnt_code_value) {
		PRPS_IDNT_CODE_VALUE = prps_idnt_code_value;
	}

	public String getPRPS_IDNT() {
		return PRPS_IDNT;
	}

	public void setPRPS_IDNT(String prps_idnt) {
		PRPS_IDNT = prps_idnt;
	}

	public String getPRPS_IDNT_NR() {
		return PRPS_IDNT_NR;
	}

	public void setPRPS_IDNT_NR(String prps_idnt_nr) {
		PRPS_IDNT_NR = prps_idnt_nr;
	}

	public String getPRPS_IDNT_ME() {
		return PRPS_IDNT_ME;
	}

	public void setPRPS_IDNT_ME(String prps_idnt_me) {
		PRPS_IDNT_ME = prps_idnt_me;
	}

	public String getPRPS_RGHT_CODE() {
		return PRPS_RGHT_CODE;
	}

	public void setPRPS_RGHT_CODE(String prps_rght_code) {
		PRPS_RGHT_CODE = prps_rght_code;
	}

	public String getPRPS_RGHT_CODE_VALUE() {
		return PRPS_RGHT_CODE_VALUE;
	}

	public void setPRPS_RGHT_CODE_VALUE(String prps_rght_code_value) {
		PRPS_RGHT_CODE_VALUE = prps_rght_code_value;
	}

	public String getRESD_CORP_NUMB() {
		return RESD_CORP_NUMB;
	}

	public void setRESD_CORP_NUMB(String resd_corp_numb) {
		RESD_CORP_NUMB = resd_corp_numb;
	}

	public String getHOME_TELX_NUMB() {
		return HOME_TELX_NUMB;
	}

	public void setHOME_TELX_NUMB(String home_telx_numb) {
		HOME_TELX_NUMB = home_telx_numb;
	}

	public String getBUSI_TELX_NUMB() {
		return BUSI_TELX_NUMB;
	}

	public void setBUSI_TELX_NUMB(String busi_telx_numb) {
		BUSI_TELX_NUMB = busi_telx_numb;
	}

	public String getTELX_NUMB() {
		return TELX_NUMB;
	}

	public void setTELX_NUMB(String telx_numb) {
		TELX_NUMB = telx_numb;
	}

	public String getMOBL_PHON() {
		return MOBL_PHON;
	}

	public void setMOBL_PHON(String mobl_phon) {
		MOBL_PHON = mobl_phon;
	}

	public String getFAXX_NUMB() {
		return FAXX_NUMB;
	}

	public void setFAXX_NUMB(String faxx_numb) {
		FAXX_NUMB = faxx_numb;
	}

	public String getMAIL() {
		return MAIL;
	}

	public void setMAIL(String mail) {
		MAIL = mail;
	}

	public String getHOME_ADDR() {
		return HOME_ADDR;
	}

	public void setHOME_ADDR(String home_addr) {
		HOME_ADDR = home_addr;
	}

	public String getBUSI_ADDR() {
		return BUSI_ADDR;
	}

	public void setBUSI_ADDR(String busi_addr) {
		BUSI_ADDR = busi_addr;
	}

	public String getPRPS_DESC() {
		return PRPS_DESC;
	}

	public void setPRPS_DESC(String prps_desc) {
		PRPS_DESC = prps_desc;
	}

	public String getOFFX_LINE_RECP() {
		return OFFX_LINE_RECP;
	}

	public void setOFFX_LINE_RECP(String offx_line_recp) {
		OFFX_LINE_RECP = offx_line_recp;
	}

	public String getCOPT_REGI_YSNO() {
		return COPT_REGI_YSNO;
	}

	public void setCOPT_REGI_YSNO(String copt_regi_ysno) {
		COPT_REGI_YSNO = copt_regi_ysno;
	}

	public String getATTC_YSNO() {
		return ATTC_YSNO;
	}

	public void setATTC_YSNO(String attc_ysno) {
		ATTC_YSNO = attc_ysno;
	}

	public String[] getMUSIC_TITLE_ARR() {
		return MUSIC_TITLE_ARR;
	}

	public void setMUSIC_TITLE_ARR(String[] music_title_arr) {
		MUSIC_TITLE_ARR = music_title_arr;
		MUSIC_TITLE_ARR_TRNS = music_title_arr;
	}

	public String[] getALBUM_TITLE_ARR() {
		return ALBUM_TITLE_ARR;
	}

	public void setALBUM_TITLE_ARR(String[] album_title_arr) {
		ALBUM_TITLE_ARR = album_title_arr;
		ALBUM_TITLE_ARR_TRNS = album_title_arr;
	}

	public String[] getLYRICIST_ARR() {
		return LYRICIST_ARR;
	}

	public void setLYRICIST_ARR(String[] lyricist_arr) {
		LYRICIST_ARR = lyricist_arr;
		LYRICIST_ARR_TRNS = lyricist_arr;
	}

	public String[] getCOMPOSER_ARR() {
		return COMPOSER_ARR;
	}

	public void setCOMPOSER_ARR(String[] composer_arr) {
		COMPOSER_ARR = composer_arr;
		COMPOSER_ARR_TRNS = composer_arr;
	}

	public String[] getARRANGER_ARR() {
		return ARRANGER_ARR;
	}

	public void setARRANGER_ARR(String[] arranger_arr) {
		ARRANGER_ARR = arranger_arr;
		ARRANGER_ARR_TRNS = arranger_arr;
	}

	public String[] getLYRICIST_ORGN_ARR() {
		return LYRICIST_ORGN_ARR;
	}

	public void setLYRICIST_ORGN_ARR(String[] lyricist_orgn_arr) {
		LYRICIST_ORGN_ARR = lyricist_orgn_arr;
		LYRICIST_ORGN_ARR_TRNS = lyricist_orgn_arr;
	}

	public String[] getCOMPOSER_ORGN_ARR() {
		return COMPOSER_ORGN_ARR;
	}

	public void setCOMPOSER_ORGN_ARR(String[] composer_orgn_arr) {
		COMPOSER_ORGN_ARR = composer_orgn_arr;
		COMPOSER_ORGN_ARR_TRNS = composer_orgn_arr;
	}

	public String[] getARRANGER_ORGN_ARR() {
		return ARRANGER_ORGN_ARR;
	}

	public void setARRANGER_ORGN_ARR(String[] arranger_orgn_arr) {
		ARRANGER_ORGN_ARR = arranger_orgn_arr;
		ARRANGER_ORGN_ARR_TRNS = arranger_orgn_arr;
	}

	public String[] getSINGER_ARR() {
		return SINGER_ARR;
	}

	public void setSINGER_ARR(String[] singer_arr) {
		SINGER_ARR = singer_arr;
		SINGER_ARR_TRNS = singer_arr;
	}

	public String[] getPLAYER_ARR() {
		return PLAYER_ARR;
	}

	public void setPLAYER_ARR(String[] player_arr) {
		PLAYER_ARR = player_arr;
		PLAYER_ARR_TRNS = player_arr;
	}

	public String[] getCONDUCTOR_ARR() {
		return CONDUCTOR_ARR;
	}

	public void setCONDUCTOR_ARR(String[] conductor_arr) {
		CONDUCTOR_ARR = conductor_arr;
		CONDUCTOR_ARR_TRNS = conductor_arr;
	}

	public String[] getPRODUCER_ARR() {
		return PRODUCER_ARR;
	}

	public void setPRODUCER_ARR(String[] producer_arr) {
		PRODUCER_ARR = producer_arr;
		PRODUCER_ARR_TRNS = producer_arr;
	}

	public String[] getSINGER_ORGN_ARR() {
		return SINGER_ORGN_ARR;
	}

	public void setSINGER_ORGN_ARR(String[] singer_orgn_arr) {
		SINGER_ORGN_ARR = singer_orgn_arr;
		SINGER_ORGN_ARR_TRNS = singer_orgn_arr;
	}

	public String[] getPLAYER_ORGN_ARR() {
		return PLAYER_ORGN_ARR;
	}

	public void setPLAYER_ORGN_ARR(String[] player_orgn_arr) {
		PLAYER_ORGN_ARR = player_orgn_arr;
		PLAYER_ORGN_ARR_TRNS = player_orgn_arr;
	}

	public String[] getCONDUCTOR_ORGN_ARR() {
		return CONDUCTOR_ORGN_ARR;
	}

	public void setCONDUCTOR_ORGN_ARR(String[] conductor_orgn_arr) {
		CONDUCTOR_ORGN_ARR = conductor_orgn_arr;
		CONDUCTOR_ORGN_ARR_TRNS = conductor_orgn_arr;
	}

	public String[] getPRODUCER_ORGN_ARR() {
		return PRODUCER_ORGN_ARR;
	}

	public void setPRODUCER_ORGN_ARR(String[] producer_orgn_arr) {
		PRODUCER_ORGN_ARR = producer_orgn_arr;
		PRODUCER_ORGN_ARR_TRNS = producer_orgn_arr;
	}

	public String[] getPERF_TIME_ARR() {
		return PERF_TIME_ARR;
	}

	public void setPERF_TIME_ARR(String[] perf_time_arr) {
		PERF_TIME_ARR = perf_time_arr;
	}

	public String[] getISSUED_DATE_ARR() {
		return ISSUED_DATE_ARR;
	}

	public void setISSUED_DATE_ARR(String[] issued_date_arr) {
		ISSUED_DATE_ARR = issued_date_arr;
	}

	public String[] getLYRICS_ARR() {
		return LYRICS_ARR;
	}

	public void setLYRICS_ARR(String[] lyrics_arr) {
		LYRICS_ARR = lyrics_arr;
	}

	public String[] getTITLE_ARR() {
		return TITLE_ARR;
	}

	public void setTITLE_ARR(String[] title_arr) {
		TITLE_ARR = title_arr;
		TITLE_ARR_TRNS = title_arr;
	}

	public String[] getBOOK_TITLE_ARR() {
		return BOOK_TITLE_ARR;
	}

	public void setBOOK_TITLE_ARR(String[] book_title_arr) {
		BOOK_TITLE_ARR = book_title_arr;
		BOOK_TITLE_ARR_TRNS = book_title_arr;
	}

	public String[] getFIRST_EDITION_YEAR_ARR() {
		return FIRST_EDITION_YEAR_ARR;
	}

	public void setFIRST_EDITION_YEAR_ARR(String[] first_edition_year_arr) {
		FIRST_EDITION_YEAR_ARR = first_edition_year_arr;
	}

	public String[] getPUBLISHER_ARR() {
		return PUBLISHER_ARR;
	}

	public void setPUBLISHER_ARR(String[] publisher_arr) {
		PUBLISHER_ARR = publisher_arr;
		PUBLISHER_ARR_TRNS = publisher_arr;
	}

	public String[] getMATERIAL_TYPE_ARR() {
		return MATERIAL_TYPE_ARR;
	}

	public void setMATERIAL_TYPE_ARR(String[] material_type_arr) {
		MATERIAL_TYPE_ARR = material_type_arr;
	}

	public String[] getPUBLISH_TYPE_ARR() {
		return PUBLISH_TYPE_ARR;
	}

	public void setPUBLISH_TYPE_ARR(String[] publish_type_arr) {
		PUBLISH_TYPE_ARR = publish_type_arr;
	}

	public String[] getTRANSLATOR_ARR() {
		return TRANSLATOR_ARR;
	}

	public void setTRANSLATOR_ARR(String[] translator_arr) {
		TRANSLATOR_ARR = translator_arr;
		TRANSLATOR_ARR_TRNS = translator_arr;
	}

	public String[] getTRANSLATOR_ORGN_ARR() {
		return TRANSLATOR_ORGN_ARR;
	}

	public void setTRANSLATOR_ORGN_ARR(String[] translator_orgn_arr) {
		TRANSLATOR_ORGN_ARR = translator_orgn_arr;
		TRANSLATOR_ORGN_ARR_TRNS = translator_orgn_arr;
	}

	public String[] getLICENSOR_NAME_KOR_ARR() {
		return LICENSOR_NAME_KOR_ARR;
	}

	public void setLICENSOR_NAME_KOR_ARR(String[] licensor_name_kor_arr) {
		LICENSOR_NAME_KOR_ARR = licensor_name_kor_arr;
		LICENSOR_NAME_KOR_ARR_TRNS = licensor_name_kor_arr;
	}

	public String[] getLICENSOR_NAME_ORGN_KOR_ARR() {
		return LICENSOR_NAME_ORGN_KOR_ARR;
	}

	public void setLICENSOR_NAME_ORGN_KOR_ARR(String[] licensor_name_orgn_kor_arr) {
		LICENSOR_NAME_ORGN_KOR_ARR = licensor_name_orgn_kor_arr;
		LICENSOR_NAME_KOR_ORGN_ARR_TRNS = licensor_name_orgn_kor_arr;
	}

	public String[] getDIRECTOR_ARR() {
		return DIRECTOR_ARR;
	}

	public void setDIRECTOR_ARR(String[] director_arr) {
		DIRECTOR_ARR = director_arr;
		DIRECTOR_ARR_TRNS = director_arr;
	}

	public String[] getLEADING_ACTOR_ARR() {
		return LEADING_ACTOR_ARR;
	}

	public void setLEADING_ACTOR_ARR(String[] leading_actor_arr) {
		LEADING_ACTOR_ARR = leading_actor_arr;
		LEADING_ACTOR_ARR_TRNS = leading_actor_arr;
	}

	public String[] getPRODUCE_DATE_ARR() {
		return PRODUCE_DATE_ARR;
	}

	public void setPRODUCE_DATE_ARR(String[] produce_date_arr) {
		PRODUCE_DATE_ARR = produce_date_arr;
	}

	public String[] getMEDIA_CODE_VALUE_ARR() {
		return MEDIA_CODE_VALUE_ARR;
	}

	public void setMEDIA_CODE_VALUE_ARR(String[] media_code_value_arr) {
		MEDIA_CODE_VALUE_ARR = media_code_value_arr;
	}

	public String[] getINVESTOR_ARR() {
		return INVESTOR_ARR;
	}

	public void setINVESTOR_ARR(String[] investor_arr) {
		INVESTOR_ARR = investor_arr;
		INVESTOR_ARR_TRNS = investor_arr;
	}

	public String[] getINVESTOR_ORGN_ARR() {
		return INVESTOR_ORGN_ARR;
	}

	public void setINVESTOR_ORGN_ARR(String[] investor_orgn_arr) {
		INVESTOR_ORGN_ARR = investor_orgn_arr;
		INVESTOR_ORGN_ARR_TRNS = investor_orgn_arr;
	}

	public String[] getDISTRIBUTOR_ARR() {
		return DISTRIBUTOR_ARR;
	}

	public void setDISTRIBUTOR_ARR(String[] distributor_arr) {
		DISTRIBUTOR_ARR = distributor_arr;
		DISTRIBUTOR_ARR_TRNS = distributor_arr;
	}

	public String[] getDISTRIBUTOR_ORGN_ARR() {
		return DISTRIBUTOR_ORGN_ARR;
	}

	public String[] getCHNL_CODE_ARR() {
		return CHNL_CODE_ARR;
	}

	public void setCHNL_CODE_ARR(String[] chnl_code_arr) {
		CHNL_CODE_ARR = chnl_code_arr;
	}

	public String[] getCHNL_CODE_NAME_ARR() {
		return CHNL_CODE_NAME_ARR;
	}

	public void setCHNL_CODE_NAME_ARR(String[] chnl_code_name_arr) {
		CHNL_CODE_NAME_ARR = chnl_code_name_arr;
		CHNL_CODE_NAME_ARR_TRNS = chnl_code_name_arr;
	}

	public String[] getMAKER_ORGN_ARR() {
		return MAKER_ORGN_ARR;
	}

	public void setMAKER_ORGN_ARR(String[] maker_orgn_arr) {
		MAKER_ORGN_ARR = maker_orgn_arr;
		MAKER_ORGN_ARR_TRNS = maker_orgn_arr;
	}

	public String[] getMEDI_CODE_ARR() {
		return MEDI_CODE_ARR;
	}

	public void setMEDI_CODE_ARR(String[] medi_code_arr) {
		MEDI_CODE_ARR = medi_code_arr;
	}

	public String[] getMEDI_CODE_NAME_ARR() {
		return MEDI_CODE_NAME_ARR;
	}

	public void setMEDI_CODE_NAME_ARR(String[] medi_code_name_arr) {
		MEDI_CODE_NAME_ARR = medi_code_name_arr;
		MEDI_CODE_NAME_ARR_TRNS = medi_code_name_arr;
	}

	public String[] getPROG_GRAD_ARR() {
		return PROG_GRAD_ARR;
	}

	public void setPROG_GRAD_ARR(String[] prog_grad_arr) {
		PROG_GRAD_ARR = prog_grad_arr;
	}

	public String[] getPROG_NAME_ARR() {
		return PROG_NAME_ARR;
	}

	public void setPROG_NAME_ARR(String[] prog_name_arr) {
		PROG_NAME_ARR = prog_name_arr;
		PROG_NAME_ARR_TRNS = prog_name_arr;
	}

	public String[] getPROG_ORDSEQ_ARR() {
		return PROG_ORDSEQ_ARR;
	}

	public void setPROG_ORDSEQ_ARR(String[] prog_ordseq_arr) {
		PROG_ORDSEQ_ARR = prog_ordseq_arr;
		PROG_ORDSEQ_ARR_TRNS = prog_ordseq_arr;
	}

	public void setDISTRIBUTOR_ORGN_ARR(String[] distributor_orgn_arr) {
		DISTRIBUTOR_ORGN_ARR = distributor_orgn_arr;
		DISTRIBUTOR_ORGN_ARR_TRNS = distributor_orgn_arr;
	}
	
	public void setARTICL_PUBC_SDATE_ARR(String[] articl_pubc_sdate_arr) {
		ARTICL_PUBC_SDATE_ARR = articl_pubc_sdate_arr;
		ARTICL_PUBC_SDATE_ARR_TRNS = articl_pubc_sdate_arr;
	}

	public String getCHK_1() {
		return CHK_1;
	}

	public void setCHK_1(String chk_1) {
		CHK_1 = chk_1;
	}

	public String getCHK_201() {
		return CHK_201;
	}

	public void setCHK_201(String chk_201) {
		CHK_201 = chk_201;
	}

	public String getCHK_202() {
		return CHK_202;
	}

	public void setCHK_202(String chk_202) {
		CHK_202 = chk_202;
	}

	public String getCHK_203() {
		return CHK_203;
	}

	public void setCHK_203(String chk_203) {
		CHK_203 = chk_203;
	}

	public String getCHK_204() {
		return CHK_204;
	}

	public void setCHK_204(String chk_204) {
		CHK_204 = chk_204;
	}

	public String getCHK_205() {
		return CHK_205;
	}

	public void setCHK_205(String chk_205) {
		CHK_205 = chk_205;
	}

	public String getATTC_SEQN() {
		return ATTC_SEQN;
	}

	public void setATTC_SEQN(String attc_seqn) {
		ATTC_SEQN = attc_seqn;
	}

	public String getFILE_NAME() {
		return FILE_NAME;
	}

	public void setFILE_NAME(String file_name) {
		FILE_NAME = file_name;
	}

	public String getFILE_PATH() {
		return FILE_PATH;
	}

	public void setFILE_PATH(String file_path) {
		FILE_PATH = file_path;
	}

	public String getFILE_SIZE() {
		return FILE_SIZE;
	}

	public void setFILE_SIZE(String file_size) {
		FILE_SIZE = file_size;
	}

	public String getREAL_FILE_NAME() {
		return REAL_FILE_NAME;
	}

	public void setREAL_FILE_NAME(String real_file_name) {
		REAL_FILE_NAME = real_file_name;
	}

	public String getTRST_ORGN_CODE() {
		return TRST_ORGN_CODE;
	}

	public void setTRST_ORGN_CODE(String trst_orgn_code) {
		TRST_ORGN_CODE = trst_orgn_code;
	}

	public String getTRST_ORGN_CODE_VALUE() {
		return TRST_ORGN_CODE_VALUE;
	}

	public void setTRST_ORGN_CODE_VALUE(String trst_orgn_code_value) {
		TRST_ORGN_CODE_VALUE = trst_orgn_code_value;
	}

	public String[] getTRST_ORGN_CODE_ARR() {
		return TRST_ORGN_CODE_ARR;
	}

	public void setTRST_ORGN_CODE_ARR(String[] trst_orgn_code_arr) {
		TRST_ORGN_CODE_ARR = trst_orgn_code_arr;
	}

	public String getRSLT_DESC() {
		return RSLT_DESC;
	}

	public void setRSLT_DESC(String rslt_desc) {
		RSLT_DESC = rslt_desc;
	}

	public String getDEAL_STAT() {
		return DEAL_STAT;
	}

	public void setDEAL_STAT(String deal_stat) {
		DEAL_STAT = deal_stat;
	}

	public String getDEAL_STAT_VALUE() {
		return DEAL_STAT_VALUE;
	}

	public void setDEAL_STAT_VALUE(String deal_stat_value) {
		DEAL_STAT_VALUE = deal_stat_value;
	}

	public String getMYML_YSNO() {
		return MYML_YSNO;
	}

	public void setMYML_YSNO(String myml_ysno) {
		MYML_YSNO = myml_ysno;
	}

	public String getTRST_YSNO() {
		return TRST_YSNO;
	}

	public void setTRST_YSNO(String trst_ysno) {
		TRST_YSNO = trst_ysno;
	}

	public String getDEAL_STAT_FLAG() {
		return DEAL_STAT_FLAG;
	}

	public void setDEAL_STAT_FLAG(String deal_stat_flag) {
		DEAL_STAT_FLAG = deal_stat_flag;
	}

	public String getRGST_IDNT() {
		return RGST_IDNT;
	}

	public void setRGST_IDNT(String rgst_idnt) {
		RGST_IDNT = rgst_idnt;
	}

	public String getMODI_IDNT() {
		return MODI_IDNT;
	}

	public void setMODI_IDNT(String modi_idnt) {
		MODI_IDNT = modi_idnt;
	}

	public String getMODI_IDNT_NAME() {
		return MODI_IDNT_NAME;
	}

	public void setMODI_IDNT_NAME(String modi_idnt_name) {
		MODI_IDNT_NAME = modi_idnt_name;
	}

	public String getMODI_DTTM() {
		return MODI_DTTM;
	}

	public void setMODI_DTTM(String modi_dttm) {
		MODI_DTTM = modi_dttm;
	}

	public String getRGHT_ROLE_CODE() {
		return RGHT_ROLE_CODE;
	}

	public void setRGHT_ROLE_CODE(String rght_role_code) {
		RGHT_ROLE_CODE = rght_role_code;
	}

	public String getHOLD_NAME() {
		return HOLD_NAME;
	}

	public void setHOLD_NAME(String hold_name) {
		HOLD_NAME = hold_name;
	}

	public String getHOLD_NAME_ORGN() {
		return HOLD_NAME_ORGN;
	}

	public void setHOLD_NAME_ORGN(String hold_name_orgn) {
		HOLD_NAME_ORGN = hold_name_orgn;
	}

	public String getCHK() {
		return CHK;
	}

	public void setCHK(String chk) {
		CHK = chk;
	}

	public String getCR_ID() {
		return CR_ID;
	}

	public void setCR_ID(String cr_id) {
		CR_ID = cr_id;
	}

	public String getNR_ID() {
		return NR_ID;
	}

	public void setNR_ID(String nr_id) {
		NR_ID = nr_id;
	}

	public String getALBUM_ID() {
		return ALBUM_ID;
	}

	public void setALBUM_ID(String album_id) {
		ALBUM_ID = album_id;
	}

	public String getGENRE() {
		return GENRE;
	}

	public void setGENRE(String genre) {
		GENRE = genre;
	}

	public String getISSUED_DATE() {
		return ISSUED_DATE;
	}

	public void setISSUED_DATE(String issued_date) {
		ISSUED_DATE = issued_date;
	}

	public String getTITLE() {
		return TITLE;
	}

	public void setTITLE(String title) {
		TITLE = title;
	}

	public String getLYRICIST() {
		return LYRICIST;
	}

	public void setLYRICIST(String lyricist) {
		LYRICIST = lyricist;
	}

	public String getCOMPOSER() {
		return COMPOSER;
	}

	public void setCOMPOSER(String composer) {
		COMPOSER = composer;
	}

	public String getARRANGER() {
		return ARRANGER;
	}

	public void setARRANGER(String arranger) {
		ARRANGER = arranger;
	}

	public String getTRANSLATOR() {
		return TRANSLATOR;
	}

	public void setTRANSLATOR(String translator) {
		TRANSLATOR = translator;
	}

	public String getLYRICIST_ORGN() {
		return LYRICIST_ORGN;
	}

	public void setLYRICIST_ORGN(String lyricist_orgn) {
		LYRICIST_ORGN = lyricist_orgn;
	}

	public String getCOMPOSER_ORGN() {
		return COMPOSER_ORGN;
	}

	public void setCOMPOSER_ORGN(String composer_orgn) {
		COMPOSER_ORGN = composer_orgn;
	}

	public String getARRANGER_ORGN() {
		return ARRANGER_ORGN;
	}

	public void setARRANGER_ORGN(String arranger_orgn) {
		ARRANGER_ORGN = arranger_orgn;
	}

	public String getTRANSLATOR_ORGN() {
		return TRANSLATOR_ORGN;
	}

	public void setTRANSLATOR_ORGN(String translator_orgn) {
		TRANSLATOR_ORGN = translator_orgn;
	}

	public String getALBUM_TITLE() {
		return ALBUM_TITLE;
	}

	public void setALBUM_TITLE(String album_title) {
		ALBUM_TITLE = album_title;
	}

	public String getMUSIC_TITLE() {
		return MUSIC_TITLE;
	}

	public void setMUSIC_TITLE(String music_title) {
		MUSIC_TITLE = music_title;
	}

	public String getSINGER_ORGN() {
		return SINGER_ORGN;
	}

	public void setSINGER_ORGN(String singer_orgn) {
		SINGER_ORGN = singer_orgn;
	}

	public String getPLAYER_ORGN() {
		return PLAYER_ORGN;
	}

	public void setPLAYER_ORGN(String player_orgn) {
		PLAYER_ORGN = player_orgn;
	}

	public String getCONDUCTOR_ORGN() {
		return CONDUCTOR_ORGN;
	}

	public void setCONDUCTOR_ORGN(String conductor_orgn) {
		CONDUCTOR_ORGN = conductor_orgn;
	}

	public String getFEATURING_ORGN() {
		return FEATURING_ORGN;
	}

	public void setFEATURING_ORGN(String featuring_orgn) {
		FEATURING_ORGN = featuring_orgn;
	}

	public String getPRODUCER_ORGN() {
		return PRODUCER_ORGN;
	}

	public void setPRODUCER_ORGN(String producer_orgn) {
		PRODUCER_ORGN = producer_orgn;
	}

	public String getSINGER() {
		return SINGER;
	}

	public void setSINGER(String singer) {
		SINGER = singer;
	}

	public String getPLAYER() {
		return PLAYER;
	}

	public void setPLAYER(String player) {
		PLAYER = player;
	}

	public String getCONDUCTOR() {
		return CONDUCTOR;
	}

	public void setCONDUCTOR(String conductor) {
		CONDUCTOR = conductor;
	}

	public String getFEATURING() {
		return FEATURING;
	}

	public void setFEATURING(String featuring) {
		FEATURING = featuring;
	}

	public String getPRODUCER() {
		return PRODUCER;
	}

	public void setPRODUCER(String producer) {
		PRODUCER = producer;
	}

	public String getICN_NUMB() {
		return ICN_NUMB;
	}

	public void setICN_NUMB(String icn_numb) {
		ICN_NUMB = icn_numb;
	}

	public String getCA201ID() {
		return CA201ID;
	}

	public void setCA201ID(String ca201id) {
		CA201ID = ca201id;
	}

	public String getCA202ID() {
		return CA202ID;
	}

	public void setCA202ID(String ca202id) {
		CA202ID = ca202id;
	}

	public String getCA203ID() {
		return CA203ID;
	}

	public void setCA203ID(String ca203id) {
		CA203ID = ca203id;
	}

	public String getALBUM_PRODUCED_CRH() {
		return ALBUM_PRODUCED_CRH;
	}

	public void setALBUM_PRODUCED_CRH(String album_produced_crh) {
		ALBUM_PRODUCED_CRH = album_produced_crh;
	}

	public String getINSERTDATE() {
		return INSERTDATE;
	}

	public void setINSERTDATE(String insertdate) {
		INSERTDATE = insertdate;
	}

	public String getTRUST_YN() {
		return TRUST_YN;
	}

	public void setTRUST_YN(String trust_yn) {
		TRUST_YN = trust_yn;
	}

	public String getPAK_TRUST_YN() {
		return PAK_TRUST_YN;
	}

	public void setPAK_TRUST_YN(String pak_trust_yn) {
		PAK_TRUST_YN = pak_trust_yn;
	}

	public String getKAPP_TRUST_YN() {
		return KAPP_TRUST_YN;
	}

	public void setKAPP_TRUST_YN(String kapp_trust_yn) {
		KAPP_TRUST_YN = kapp_trust_yn;
	}

	public String getDISK_NO() {
		return DISK_NO;
	}

	public void setDISK_NO(String disk_no) {
		DISK_NO = disk_no;
	}

	public String getTRACK_NO() {
		return TRACK_NO;
	}

	public void setTRACK_NO(String track_no) {
		TRACK_NO = track_no;
	}

	public String getINSERT_DATE() {
		return INSERT_DATE;
	}

	public void setINSERT_DATE(String insert_date) {
		INSERT_DATE = insert_date;
	}

	public String getEDITION() {
		return EDITION;
	}

	public void setEDITION(String edition) {
		EDITION = edition;
	}

	public String getALBUM_TYPE() {
		return ALBUM_TYPE;
	}

	public void setALBUM_TYPE(String album_type) {
		ALBUM_TYPE = album_type;
	}

	public String getALBUM_MEDIA_TYPE() {
		return ALBUM_MEDIA_TYPE;
	}

	public void setALBUM_MEDIA_TYPE(String album_media_type) {
		ALBUM_MEDIA_TYPE = album_media_type;
	}

	public String getPERFORMANCE_TIME() {
		return PERFORMANCE_TIME;
	}

	public void setPERFORMANCE_TIME(String performance_time) {
		PERFORMANCE_TIME = performance_time;
	}

	public String getALBUM_LABLE_NO() {
		return ALBUM_LABLE_NO;
	}

	public void setALBUM_LABLE_NO(String album_lable_no) {
		ALBUM_LABLE_NO = album_lable_no;
	}

	public String getALBUM_LABLE_NAME() {
		return ALBUM_LABLE_NAME;
	}

	public void setALBUM_LABLE_NAME(String album_lable_name) {
		ALBUM_LABLE_NAME = album_lable_name;
	}

	public String getMUSIC_TYPE() {
		return MUSIC_TYPE;
	}

	public void setMUSIC_TYPE(String music_type) {
		MUSIC_TYPE = music_type;
	}

	public String getPERF_TIME() {
		return PERF_TIME;
	}

	public void setPERF_TIME(String perf_time) {
		PERF_TIME = perf_time;
	}

	public String getLYRICS() {
		return LYRICS;
	}

	public void setLYRICS(String lyrics) {
		LYRICS = lyrics;
	}

	public String getTOTAL_TRACK() {
		return TOTAL_TRACK;
	}

	public void setTOTAL_TRACK(String total_track) {
		TOTAL_TRACK = total_track;
	}

	public String getBOOK_NR_ID() {
		return BOOK_NR_ID;
	}

	public void setBOOK_NR_ID(String book_nr_id) {
		BOOK_NR_ID = book_nr_id;
	}

	public String getLICENSOR_NAME_KOR() {
		return LICENSOR_NAME_KOR;
	}

	public void setLICENSOR_NAME_KOR(String licensor_name_kor) {
		LICENSOR_NAME_KOR = licensor_name_kor;
	}

	public String getLICENSOR_NAME_KOR_ORGN() {
		return LICENSOR_NAME_KOR_ORGN;
	}

	public void setLICENSOR_NAME_KOR_ORGN(String licensor_name_kor_orgn) {
		LICENSOR_NAME_KOR_ORGN = licensor_name_kor_orgn;
	}

	public String getSUBTITLE() {
		return SUBTITLE;
	}

	public void setSUBTITLE(String subtitle) {
		SUBTITLE = subtitle;
	}

	public String getFIRST_EDITION_YEAR() {
		return FIRST_EDITION_YEAR;
	}

	public void setFIRST_EDITION_YEAR(String first_edition_year) {
		FIRST_EDITION_YEAR = first_edition_year;
	}

	public String getPUBLISH_TYPE() {
		return PUBLISH_TYPE;
	}

	public void setPUBLISH_TYPE(String publish_type) {
		PUBLISH_TYPE = publish_type;
	}

	public String getPUBLISH_TYPE_NAME() {
		return PUBLISH_TYPE_NAME;
	}

	public void setPUBLISH_TYPE_NAME(String publish_type_name) {
		PUBLISH_TYPE_NAME = publish_type_name;
	}

	public String getRETRIEVE_TYPE() {
		return RETRIEVE_TYPE;
	}

	public void setRETRIEVE_TYPE(String retrieve_type) {
		RETRIEVE_TYPE = retrieve_type;
	}

	public String getRETRIEVE_TYPE_NAME() {
		return RETRIEVE_TYPE_NAME;
	}

	public void setRETRIEVE_TYPE_NAME(String retrieve_type_name) {
		RETRIEVE_TYPE_NAME = retrieve_type_name;
	}

	public String getPUBLISHER() {
		return PUBLISHER;
	}

	public void setPUBLISHER(String publisher) {
		PUBLISHER = publisher;
	}

	public String getWORKS_TITLE() {
		return WORKS_TITLE;
	}

	public void setWORKS_TITLE(String works_title) {
		WORKS_TITLE = works_title;
	}

	public String getWORKS_SUBTITLE() {
		return WORKS_SUBTITLE;
	}

	public void setWORKS_SUBTITLE(String works_subtitle) {
		WORKS_SUBTITLE = works_subtitle;
	}

	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	public void setMATERIAL_TYPE(String material_type) {
		MATERIAL_TYPE = material_type;
	}

	public String getSTAGE_NAME() {
		return STAGE_NAME;
	}

	public void setSTAGE_NAME(String stage_name) {
		STAGE_NAME = stage_name;
	}

	public String getCA_NAME() {
		return CA_NAME;
	}

	public void setCA_NAME(String ca_name) {
		CA_NAME = ca_name;
	}

	public String getISBN_ISSN() {
		return ISBN_ISSN;
	}

	public void setISBN_ISSN(String isbn_issn) {
		ISBN_ISSN = isbn_issn;
	}

	public String getBOOK_TITLE() {
		return BOOK_TITLE;
	}

	public void setBOOK_TITLE(String book_title) {
		BOOK_TITLE = book_title;
	}

	public String getMEDIA_ID() {
		return MEDIA_ID;
	}

	public void setMEDIA_ID(String media_id) {
		MEDIA_ID = media_id;
	}

	public String getTITLE_ORG() {
		return TITLE_ORG;
	}

	public void setTITLE_ORG(String title_org) {
		TITLE_ORG = title_org;
	}

	public String getPROD_HOLDER() {
		return PROD_HOLDER;
	}

	public void setPROD_HOLDER(String prod_holder) {
		PROD_HOLDER = prod_holder;
	}

	public String getCIRC_HOLDER() {
		return CIRC_HOLDER;
	}

	public void setCIRC_HOLDER(String circ_holder) {
		CIRC_HOLDER = circ_holder;
	}

	public String getPUBL_HOLDER() {
		return PUBL_HOLDER;
	}

	public void setPUBL_HOLDER(String publ_holder) {
		PUBL_HOLDER = publ_holder;
	}

	public String getDIST_HOLDER() {
		return DIST_HOLDER;
	}

	public void setDIST_HOLDER(String dist_holder) {
		DIST_HOLDER = dist_holder;
	}

	public String getINVESTOR() {
		return INVESTOR;
	}

	public void setINVESTOR(String investor) {
		INVESTOR = investor;
	}

	public String getDISTRIBUTOR() {
		return DISTRIBUTOR;
	}

	public void setDISTRIBUTOR(String distributor) {
		DISTRIBUTOR = distributor;
	}

	public String getPRODUCE_YEAR() {
		return PRODUCE_YEAR;
	}

	public void setPRODUCE_YEAR(String produce_year) {
		PRODUCE_YEAR = produce_year;
	}

	public String getDIRECTOR() {
		return DIRECTOR;
	}

	public void setDIRECTOR(String director) {
		DIRECTOR = director;
	}

	public String getLEADING_ACTOR() {
		return LEADING_ACTOR;
	}

	public void setLEADING_ACTOR(String leading_actor) {
		LEADING_ACTOR = leading_actor;
	}

	public String getGENRE_VALUE() {
		return GENRE_VALUE;
	}

	public void setGENRE_VALUE(String genre_value) {
		GENRE_VALUE = genre_value;
	}

	public String getMOVIE_TYPE() {
		return MOVIE_TYPE;
	}

	public void setMOVIE_TYPE(String movie_type) {
		MOVIE_TYPE = movie_type;
	}

	public String getMOVIE_TYPE_VALUE() {
		return MOVIE_TYPE_VALUE;
	}

	public void setMOVIE_TYPE_VALUE(String movie_type_value) {
		MOVIE_TYPE_VALUE = movie_type_value;
	}

	public String getVIEW_GRADE() {
		return VIEW_GRADE;
	}

	public void setVIEW_GRADE(String view_grade) {
		VIEW_GRADE = view_grade;
	}

	public String getVIEW_GRADE_VALUE() {
		return VIEW_GRADE_VALUE;
	}

	public void setVIEW_GRADE_VALUE(String view_grade_value) {
		VIEW_GRADE_VALUE = view_grade_value;
	}

	public String getRUN_TIME() {
		return RUN_TIME;
	}

	public void setRUN_TIME(String run_time) {
		RUN_TIME = run_time;
	}

	public String getRELEASE_DATE() {
		return RELEASE_DATE;
	}

	public void setRELEASE_DATE(String release_date) {
		RELEASE_DATE = release_date;
	}

	public String getCRH_ID_OF_CA() {
		return CRH_ID_OF_CA;
	}

	public void setCRH_ID_OF_CA(String crh_id_of_ca) {
		CRH_ID_OF_CA = crh_id_of_ca;
	}

	public String getKMVA_CODE() {
		return KMVA_CODE;
	}

	public void setKMVA_CODE(String kmva_code) {
		KMVA_CODE = kmva_code;
	}

	public String getMOVIE_MGNT_SEQN() {
		return MOVIE_MGNT_SEQN;
	}

	public void setMOVIE_MGNT_SEQN(String movie_mgnt_seqn) {
		MOVIE_MGNT_SEQN = movie_mgnt_seqn;
	}

	public String getStdCrhId() {
		return stdCrhId;
	}

	public void setStdCrhId(String stdCrhId) {
		this.stdCrhId = stdCrhId;
	}

	public String getCANAME() {
		return CANAME;
	}

	public void setCANAME(String caname) {
		CANAME = caname;
	}

	public String getMEDIA_CODE() {
		return MEDIA_CODE;
	}

	public void setMEDIA_CODE(String media_code) {
		MEDIA_CODE = media_code;
	}

	public String getMEDIA_CODE_VALUE() {
		return MEDIA_CODE_VALUE;
	}

	public void setMEDIA_CODE_VALUE(String media_code_value) {
		MEDIA_CODE_VALUE = media_code_value;
	}

	public String getPRODUCE_DATE() {
		return PRODUCE_DATE;
	}

	public void setPRODUCE_DATE(String produce_date) {
		PRODUCE_DATE = produce_date;
	}

	public String getDISCLOSURE_DATE() {
		return DISCLOSURE_DATE;
	}

	public void setDISCLOSURE_DATE(String disclosure_date) {
		DISCLOSURE_DATE = disclosure_date;
	}

	public String getMEDIA_VIEW_GRADE_VIEW() {
		return MEDIA_VIEW_GRADE_VIEW;
	}

	public void setMEDIA_VIEW_GRADE_VIEW(String media_view_grade_view) {
		MEDIA_VIEW_GRADE_VIEW = media_view_grade_view;
	}

	public String getDIVS() {
		return DIVS;
	}

	public void setDIVS(String divs) {
		DIVS = divs;
	}

	public String getSrchTitle() {
		return srchTitle;
	}

	public void setSrchTitle(String srchTitle) {
		this.srchTitle = srchTitle;
	}
	
	public String getSrchTitleLen() {
		return srchTitleLen;
	}

	public void setSrchTitleLen(String srchTitleLen) {
		this.srchTitleLen = srchTitleLen;
	}

	public String getSrchProducer() {
		return srchProducer;
	}

	public void setSrchProducer(String srchProducer) {
		this.srchProducer = srchProducer;
	}

	public String getSrchAlbumTitle() {
		return srchAlbumTitle;
	}

	public void setSrchAlbumTitle(String srchAlbumTitle) {
		this.srchAlbumTitle = srchAlbumTitle;
	}

	public String getSrchSinger() {
		return srchSinger;
	}

	public void setSrchSinger(String srchSinger) {
		this.srchSinger = srchSinger;
	}

	public String getSrchStartDate() {
		return srchStartDate;
	}

	public void setSrchStartDate(String srchStartDate) {
		this.srchStartDate = srchStartDate;
	}

	public String getSrchEndDate() {
		return srchEndDate;
	}

	public void setSrchEndDate(String srchEndDate) {
		this.srchEndDate = srchEndDate;
	}

	public String getSrchLicensor() {
		return srchLicensor;
	}

	public void setSrchLicensor(String srchLicensor) {
		this.srchLicensor = srchLicensor;
	}

	public String getSrchSdsrName() {
		return srchSdsrName;
	}

	public void setSrchSdsrName(String srchSdsrName) {
		this.srchSdsrName = srchSdsrName;
	}

	public String getSrchMuciName() {
		return srchMuciName;
	}

	public void setSrchMuciName(String srchMuciName) {
		this.srchMuciName = srchMuciName;
	}

	public String getSrchYymm() {
		return srchYymm;
	}

	public void setSrchYymm(String srchYymm) {
		this.srchYymm = srchYymm;
	}

	public String getSrchPublisher() {
		return srchPublisher;
	}

	public void setSrchPublisher(String srchPublisher) {
		this.srchPublisher = srchPublisher;
	}

	public String getSrchBookTitle() {
		return srchBookTitle;
	}

	public void setSrchBookTitle(String srchBookTitle) {
		this.srchBookTitle = srchBookTitle;
	}

	public String getSrchLicensorNm() {
		return srchLicensorNm;
	}

	public void setSrchLicensorNm(String srchLicensorNm) {
		this.srchLicensorNm = srchLicensorNm;
	}

	public String getSrchPrpsDivs() {
		return srchPrpsDivs;
	}

	public void setSrchPrpsDivs(String srchPrpsDivs) {
		this.srchPrpsDivs = srchPrpsDivs;
	}

	public String getSrchYear() {
		return srchYear;
	}

	public void setSrchYear(String srchYear) {
		this.srchYear = srchYear;
	}

	public String getSrchDirector() {
		return srchDirector;
	}

	public void setSrchDirector(String srchDirector) {
		this.srchDirector = srchDirector;
	}

	public String getCNT() {
		return CNT;
	}

	public void setCNT(String cnt) {
		CNT = cnt;
	}

	public String getMAX_DEAL_STAT() {
		return MAX_DEAL_STAT;
	}

	public void setMAX_DEAL_STAT(String max_deal_stat) {
		MAX_DEAL_STAT = max_deal_stat;
	}

	public String getPRPS_TITE() {
		return PRPS_TITE;
	}

	public void setPRPS_TITE(String prps_tite) {
		PRPS_TITE = prps_tite;
	}

	public String getPRPS_TITE_VIEW() {
		return PRPS_TITE_VIEW;
	}

	public void setPRPS_TITE_VIEW(String prps_tite_view) {
		PRPS_TITE_VIEW = prps_tite_view;
	}

	public String getRGST_DTTM() {
		return RGST_DTTM;
	}

	public void setRGST_DTTM(String rgst_dttm) {
		RGST_DTTM = rgst_dttm;
	}

	public String getSDSR_NAME() {
		return SDSR_NAME;
	}

	public void setSDSR_NAME(String sdsr_name) {
		SDSR_NAME = sdsr_name;
	}

	public String getTOTAL_STAT() {
		return TOTAL_STAT;
	}

	public void setTOTAL_STAT(String total_stat) {
		TOTAL_STAT = total_stat;
	}

	public String getUSER_NAME() {
		return USER_NAME;
	}

	public void setUSER_NAME(String user_name) {
		USER_NAME = user_name;
	}

	public String getUSER_DIVS() {
		return USER_DIVS;
	}

	public void setUSER_DIVS(String user_divs) {
		USER_DIVS = user_divs;
	}

	public String getRESD_CORP_NUMB_VIEW() {
		return RESD_CORP_NUMB_VIEW;
	}

	public void setRESD_CORP_NUMB_VIEW(String resd_corp_numb_view) {
		RESD_CORP_NUMB_VIEW = resd_corp_numb_view;
	}

	public String getCORP_NUMB() {
		return CORP_NUMB;
	}

	public void setCORP_NUMB(String corp_numb) {
		CORP_NUMB = corp_numb;
	}

	public List getFileList() {
		return fileList;
	}

	public void setFileList(List fileList) {
		this.fileList = fileList;
	}

	public String getCHK_211() {
		return CHK_211;
	}

	public void setCHK_211(String chk_211) {
		CHK_211 = chk_211;
	}

	public String getDISTRIBUTOR_ORGN() {
		return DISTRIBUTOR_ORGN;
	}

	public void setDISTRIBUTOR_ORGN(String distributor_orgn) {
		DISTRIBUTOR_ORGN = distributor_orgn;
	}

	public String getINVESTOR_ORGN() {
		return INVESTOR_ORGN;
	}

	public void setINVESTOR_ORGN(String investor_orgn) {
		INVESTOR_ORGN = investor_orgn;
	}

	public String getSrchActor() {
		return srchActor;
	}

	public void setSrchActor(String srchActor) {
		this.srchActor = srchActor;
	}

	public String getSrchViewGrade() {
		return srchViewGrade;
	}

	public void setSrchViewGrade(String srchViewGrade) {
		this.srchViewGrade = srchViewGrade;
	}

	public String getFILE_INFO() {
		return FILE_INFO;
	}

	public void setFILE_INFO(String file_info) {
		FILE_INFO = file_info;
	}

	public String getRecFile() {
		return recFile;
	}

	public void setRecFile(String recFile) {
		this.recFile = recFile;
	}

	public String getKRTRA_TRUST_YN() {
		return KRTRA_TRUST_YN;
	}

	public void setKRTRA_TRUST_YN(String krtra_trust_yn) {
		KRTRA_TRUST_YN = krtra_trust_yn;
	}

	public String getKOFIC_TRUST_YN() {
		return KOFIC_TRUST_YN;
	}

	public void setKOFIC_TRUST_YN(String kofic_trust_yn) {
		KOFIC_TRUST_YN = kofic_trust_yn;
	}

	public String getSrchDistributor() {
		return srchDistributor;
	}

	public void setSrchDistributor(String srchDistributor) {
		this.srchDistributor = srchDistributor;
	}

	public String getDIRECTOR_VIEW() {
		return DIRECTOR_VIEW;
	}

	public void setDIRECTOR_VIEW(String director_view) {
		DIRECTOR_VIEW = director_view;
	}

	public String getLEADING_ACTOR_VIEW() {
		return LEADING_ACTOR_VIEW;
	}

	public void setLEADING_ACTOR_VIEW(String leading_actor_view) {
		LEADING_ACTOR_VIEW = leading_actor_view;
	}

	public String[] getDELT_FILE_INFO() {
		return DELT_FILE_INFO;
	}

	public void setDELT_FILE_INFO(String[] delt_file_info) {
		DELT_FILE_INFO = delt_file_info;
	}

	public String getSESS_U_MAIL() {
		return SESS_U_MAIL;
	}

	public void setSESS_U_MAIL(String sESSUMAIL) {
		SESS_U_MAIL = sESSUMAIL;
	}

	public String getSESS_U_MAIL_RECE_YSNO() {
		return SESS_U_MAIL_RECE_YSNO;
	}

	public void setSESS_U_MAIL_RECE_YSNO(String sESSUMAILRECEYSNO) {
		SESS_U_MAIL_RECE_YSNO = sESSUMAILRECEYSNO;
	}

	public String getSESS_U_MOBL_PHON() {
		return SESS_U_MOBL_PHON;
	}

	public void setSESS_U_MOBL_PHON(String sESSUMOBLPHON) {
		SESS_U_MOBL_PHON = sESSUMOBLPHON;
	}

	public String getSESS_U_SMS_RECE_YSNO() {
		return SESS_U_SMS_RECE_YSNO;
	}

	public void setSESS_U_SMS_RECE_YSNO(String sESSUSMSRECEYSNO) {
		SESS_U_SMS_RECE_YSNO = sESSUSMSRECEYSNO;
	}

	public String getSrchCoptHodr() {
		return srchCoptHodr;
	}

	public void setSrchCoptHodr(String srchCoptHodr) {
		this.srchCoptHodr = srchCoptHodr;
	}

	public String getSrchLishComp() {
		return srchLishComp;
	}

	public void setSrchLishComp(String srchLishComp) {
		this.srchLishComp = srchLishComp;
	}

	public String getSrchWorkName() {
		return srchWorkName;
	}

	public void setSrchWorkName(String srchWorkName) {
		this.srchWorkName = srchWorkName;
	}

	public String getCOPT_HODR() {
		return COPT_HODR;
	}

	public void setCOPT_HODR(String copt_hodr) {
		COPT_HODR = copt_hodr;
	}

	public String getIMAGE_DIVS() {
		return IMAGE_DIVS;
	}

	public void setIMAGE_DIVS(String image_divs) {
		IMAGE_DIVS = image_divs;
	}

	public String getIMGE_SEQN() {
		return IMGE_SEQN;
	}

	public void setIMGE_SEQN(String imge_seqn) {
		IMGE_SEQN = imge_seqn;
	}

	public String getLISH_COMP() {
		return LISH_COMP;
	}

	public void setLISH_COMP(String lish_comp) {
		LISH_COMP = lish_comp;
	}

	public String getUSEX_YEAR() {
		return USEX_YEAR;
	}

	public void setUSEX_YEAR(String usex_year) {
		USEX_YEAR = usex_year;
	}

	public String getWORK_FILE_NAME() {
		return WORK_FILE_NAME;
	}

	public void setWORK_FILE_NAME(String work_file_name) {
		WORK_FILE_NAME = work_file_name;
	}

	public String getWORK_NAME() {
		return WORK_NAME;
	}

	public void setWORK_NAME(String work_name) {
		WORK_NAME = work_name;
	}

	public String getWTER_DIVS() {
		return WTER_DIVS;
	}

	public void setWTER_DIVS(String wter_divs) {
		WTER_DIVS = wter_divs;
	}

	public String[] getCOPT_HODR_ARR() {
		return COPT_HODR_ARR;
	}

	public void setCOPT_HODR_ARR(String[] copt_hodr_arr) {
		COPT_HODR_ARR = copt_hodr_arr;
		COPT_HODR_ARR_TRNS = copt_hodr_arr;
	}

	public String[] getIMAGE_DIVS_ARR() {
		return IMAGE_DIVS_ARR;
	}

	public void setIMAGE_DIVS_ARR(String[] image_divs_arr) {
		IMAGE_DIVS_ARR = image_divs_arr;
		IMAGE_DIVS_ARR_TRNS = image_divs_arr;
	}

	public String[] getLISH_COMP_ARR() {
		return LISH_COMP_ARR;
	}

	public void setLISH_COMP_ARR(String[] lish_comp_arr) {
		LISH_COMP_ARR = lish_comp_arr;
	}

	public String[] getUSEX_YEAR_ARR() {
		return USEX_YEAR_ARR;
	}

	public void setUSEX_YEAR_ARR(String[] usex_year_arr) {
		USEX_YEAR_ARR = usex_year_arr;
	}

	public String[] getBROAD_DATE_ARR() {
		return BROAD_DATE_ARR;
	}

	public void setBROAD_DATE_ARR(String[] broad_date_arr) {
		BROAD_DATE_ARR = broad_date_arr;
	}

	public String[] getBROAD_MEDI_NAME_ARR() {
		return BROAD_MEDI_NAME_ARR;
	}

	public void setBROAD_MEDI_NAME_ARR(String[] broad_medi_name_arr) {
		BROAD_MEDI_NAME_ARR = broad_medi_name_arr;
		BROAD_MEDI_NAME_ARR_TRNS = broad_medi_name_arr;
	}

	public String[] getBROAD_ORD_ARR() {
		return BROAD_ORD_ARR;
	}

	public void setBROAD_ORD_ARR(String[] broad_ord_arr) {
		BROAD_ORD_ARR = broad_ord_arr;
	}

	public String[] getBROAD_STAT_NAME_ARR() {
		return BROAD_STAT_NAME_ARR;
	}

	public void setBROAD_STAT_NAME_ARR(String[] broad_stat_name_arr) {
		BROAD_STAT_NAME_ARR = broad_stat_name_arr;
		BROAD_STAT_NAME_ARR_TRNS = broad_stat_name_arr;
	}

	public String[] getBROAD_MEDI_ARR() {
		return BROAD_MEDI_ARR;
	}

	public void setBROAD_MEDI_ARR(String[] broad_medi_arr) {
		BROAD_MEDI_ARR = broad_medi_arr;
	}

	public String[] getBROAD_STAT_ARR() {
		return BROAD_STAT_ARR;
	}

	public void setBROAD_STAT_ARR(String[] broad_stat_arr) {
		BROAD_STAT_ARR = broad_stat_arr;
	}

	public String[] getGENRE_KIND_ARR() {
		return GENRE_KIND_ARR;
	}

	public void setGENRE_KIND_ARR(String[] genre_kind_arr) {
		GENRE_KIND_ARR = genre_kind_arr;
	}

	public String[] getGENRE_KIND_NAME_ARR() {
		return GENRE_KIND_NAME_ARR;
	}

	public void setGENRE_KIND_NAME_ARR(String[] genre_kind_name_arr) {
		GENRE_KIND_NAME_ARR = genre_kind_name_arr;
	}

	public String[] getMAKER_ARR() {
		return MAKER_ARR;
	}

	public void setMAKER_ARR(String[] maker_arr) {
		MAKER_ARR = maker_arr;
		MAKER_ARR_TRNS = maker_arr;
	}

	public String[] getDIRECT_ARR() {
		return DIRECT_ARR;
	}

	public void setDIRECT_ARR(String[] direct_arr) {
		DIRECT_ARR = direct_arr;
		DIRECT_ARR_TRNS =  direct_arr;
	}

	public String[] getWRITER_ARR() {
		return WRITER_ARR;
	}

	public void setWRITER_ARR(String[] writer_arr) {
		WRITER_ARR = writer_arr;
		WRITER_ARR_TRNS = writer_arr;
	}

	public String[] getWRITER_ORGN_ARR() {
		return WRITER_ORGN_ARR;
	}

	public void setWRITER_ORGN_ARR(String[] writer_orgn_arr) {
		WRITER_ORGN_ARR = writer_orgn_arr;
		WRITER_ORGN_ARR_TRNS = writer_orgn_arr;
	}

	public String[] getWORK_NAME_ARR() {
		return WORK_NAME_ARR;
	}

	public void setWORK_NAME_ARR(String[] work_name_arr) {
		WORK_NAME_ARR = work_name_arr;
		WORK_NAME_ARR_TRNS = work_name_arr;
	}

	public String[] getWTER_DIVS_ARR() {
		return WTER_DIVS_ARR;
	}

	public void setWTER_DIVS_ARR(String[] wter_divs_arr) {
		WTER_DIVS_ARR = wter_divs_arr;
	}

	public String getARRANGER_YN() {
		return ARRANGER_YN;
	}

	public void setARRANGER_YN(String arranger_yn) {
		ARRANGER_YN = arranger_yn;
	}

	public String getCOMPOSER_YN() {
		return COMPOSER_YN;
	}

	public void setCOMPOSER_YN(String composer_yn) {
		COMPOSER_YN = composer_yn;
	}

	public String getCONDUCTOR_YN() {
		return CONDUCTOR_YN;
	}

	public void setCONDUCTOR_YN(String conductor_yn) {
		CONDUCTOR_YN = conductor_yn;
	}

	public String getLYRICIST_YN() {
		return LYRICIST_YN;
	}

	public void setLYRICIST_YN(String lyricist_yn) {
		LYRICIST_YN = lyricist_yn;
	}

	public String getPLAYER_YN() {
		return PLAYER_YN;
	}

	public void setPLAYER_YN(String player_yn) {
		PLAYER_YN = player_yn;
	}

	public String getPRODUCER_YN() {
		return PRODUCER_YN;
	}

	public void setPRODUCER_YN(String producer_yn) {
		PRODUCER_YN = producer_yn;
	}

	public String getSINGER_YN() {
		return SINGER_YN;
	}

	public void setSINGER_YN(String singer_yn) {
		SINGER_YN = singer_yn;
	}

	public String getTRANSLATOR_YN() {
		return TRANSLATOR_YN;
	}

	public void setTRANSLATOR_YN(String translator_yn) {
		TRANSLATOR_YN = translator_yn;
	}

	public String getTRUST_CNT() {
		return TRUST_CNT;
	}

	public void setTRUST_CNT(String trust_cnt) {
		TRUST_CNT = trust_cnt;
	}

	public String getSrchNoneName() {
		return srchNoneName;
	}

	public void setSrchNoneName(String srchNoneName) {
		this.srchNoneName = srchNoneName;
	}

	public String getSrchNoneRole() {
		return srchNoneRole;
	}

	public void setSrchNoneRole(String srchNoneRole) {
		this.srchNoneRole = srchNoneRole;
	}

	public String[] getSrchNoneRole_arr() {
		return srchNoneRole_arr;
	}

	public void setSrchNoneRole_arr(String[] srchNoneRole_arr) {
		this.srchNoneRole_arr = srchNoneRole_arr;
	}

	public String getCOPT_HODR_YN() {
		return COPT_HODR_YN;
	}

	public void setCOPT_HODR_YN(String copt_hodr_yn) {
		COPT_HODR_YN = copt_hodr_yn;
	}

	public String getSrchWterDivs() {
		return srchWterDivs;
	}

	public void setSrchWterDivs(String srchWterDivs) {
		this.srchWterDivs = srchWterDivs;
	}

	public String getWRITER_YN() {
		return WRITER_YN;
	}

	public void setWRITER_YN(String writer_yn) {
		WRITER_YN = writer_yn;
	}

	public String getDISTRIBUTOR_YN() {
		return DISTRIBUTOR_YN;
	}

	public void setDISTRIBUTOR_YN(String distributor_yn) {
		DISTRIBUTOR_YN = distributor_yn;
	}

	public String getINVESTOR_YN() {
		return INVESTOR_YN;
	}

	public void setINVESTOR_YN(String investor_yn) {
		INVESTOR_YN = investor_yn;
	}

	public String getPRPS_SIDE_GENRE() {
		return PRPS_SIDE_GENRE;
	}

	public void setPRPS_SIDE_GENRE(String prps_side_genre) {
		PRPS_SIDE_GENRE = prps_side_genre;
	}

	public String getPRPS_SIDE_R_GENRE() {
		return PRPS_SIDE_R_GENRE;
	}

	public void setPRPS_SIDE_R_GENRE(String prps_side_r_genre) {
		PRPS_SIDE_R_GENRE = prps_side_r_genre;
	}

	public String getPRPS_RGHT_DESC() {
		return PRPS_RGHT_DESC;
	}

	public void setPRPS_RGHT_DESC(String prps_rght_desc) {
		PRPS_RGHT_DESC = prps_rght_desc;
	}

	public String[] getPRPS_SIDE_R_GENRE_ARR() {
		return PRPS_SIDE_R_GENRE_ARR;
	}

	public void setPRPS_SIDE_R_GENRE_ARR(String[] prps_side_r_genre_arr) {
		PRPS_SIDE_R_GENRE_ARR = prps_side_r_genre_arr;
	}

	public String getCHK_200() {
		return CHK_200;
	}

	public void setCHK_200(String chk_200) {
		CHK_200 = chk_200;
	}

	public String getDIVS_CODE() {
		return DIVS_CODE;
	}

	public void setDIVS_CODE(String divs_code) {
		DIVS_CODE = divs_code;
	}
	
	public String getPRPS_CNT() {
		return PRPS_CNT;
	}

	public void setPRPS_CNT(String prps_cnt) {
		PRPS_CNT = prps_cnt;
	}

	public String getMAKER_YN() {
		return MAKER_YN;
	}

	public void setMAKER_YN(String maker_yn) {
		MAKER_YN = maker_yn;
	}

	public String getBIG_CODE() {
		return BIG_CODE;
	}

	public void setBIG_CODE(String big_code) {
		BIG_CODE = big_code;
	}

	public String getCHK_212() {
		return CHK_212;
	}

	public void setCHK_212(String chk_212) {
		CHK_212 = chk_212;
	}

	public String getCHK_213() {
		return CHK_213;
	}

	public void setCHK_213(String chk_213) {
		CHK_213 = chk_213;
	}

	public String getCHK_214() {
		return CHK_214;
	}

	public void setCHK_214(String chk_214) {
		CHK_214 = chk_214;
	}

	public String getCHK_215() {
		return CHK_215;
	}

	public void setCHK_215(String chk_215) {
		CHK_215 = chk_215;
	}

	public String getCHK_216() {
		return CHK_216;
	}

	public void setCHK_216(String chk_216) {
		CHK_216 = chk_216;
	}

	public String getETC_TRST_ORGN_CODE() {
		return ETC_TRST_ORGN_CODE;
	}

	public void setETC_TRST_ORGN_CODE(String etc_trst_orgn_code) {
		ETC_TRST_ORGN_CODE = etc_trst_orgn_code;
	}

	public String getSIDE_COPT_HODR() {
		return SIDE_COPT_HODR;
	}

	public void setSIDE_COPT_HODR(String side_copt_hodr) {
		SIDE_COPT_HODR = side_copt_hodr;
	}

	public String getPOST_URS() {
		return POST_URS;
	}

	public void setPOST_URS(String post_urs) {
		POST_URS = post_urs;
	}

	public String getMV_WRITER() {
		return MV_WRITER;
	}

	public void setMV_WRITER(String mv_writer) {
		MV_WRITER = mv_writer;
	}

	public String getPRPS_DOBL_CODE() {
		return PRPS_DOBL_CODE;
	}

	public void setPRPS_DOBL_CODE(String prps_dobl_code) {
		PRPS_DOBL_CODE = prps_dobl_code;
	}

	public String getPRPS_DOBL_KEY() {
		return PRPS_DOBL_KEY;
	}

	public void setPRPS_DOBL_KEY(String prps_dobl_key) {
		PRPS_DOBL_KEY = prps_dobl_key;
	}

	public int getIResult() {
		return iResult;
	}

	public void setIResult(int result) {
		iResult = result;
	}

	public String getMUCI_NAME() {
		return MUCI_NAME;
	}

	public void setMUCI_NAME(String muci_name) {
		MUCI_NAME = muci_name;
	}

	public String getALBM_NAME() {
		return ALBM_NAME;
	}

	public void setALBM_NAME(String albm_name) {
		ALBM_NAME = albm_name;
	}

	public String getARRG_WRTR() {
		return ARRG_WRTR;
	}

	public void setARRG_WRTR(String arrg_wrtr) {
		ARRG_WRTR = arrg_wrtr;
	}

	public String getCOMS_WRTR() {
		return COMS_WRTR;
	}

	public void setCOMS_WRTR(String coms_wrtr) {
		COMS_WRTR = coms_wrtr;
	}

	public String getLYRI_WRTR() {
		return LYRI_WRTR;
	}

	public void setLYRI_WRTR(String lyri_wrtr) {
		LYRI_WRTR = lyri_wrtr;
	}

	public String getINMT_SEQN() {
		return INMT_SEQN;
	}

	public void setINMT_SEQN(String inmt_seqn) {
		INMT_SEQN = inmt_seqn;
	}

	public String getINMT_PRPS_MAST_KEY() {
		return INMT_PRPS_MAST_KEY;
	}

	public void setINMT_PRPS_MAST_KEY(String inmt_prps_mast_key) {
		INMT_PRPS_MAST_KEY = inmt_prps_mast_key;
	}

	public String getRGHT_PRPS_MAST_KEY() {
		return RGHT_PRPS_MAST_KEY;
	}

	public void setRGHT_PRPS_MAST_KEY(String rght_prps_mast_key) {
		RGHT_PRPS_MAST_KEY = rght_prps_mast_key;
	}

	public String getPUBC_YEAR() {
		return PUBC_YEAR;
	}

	public void setPUBC_YEAR(String pubc_year) {
		PUBC_YEAR = pubc_year;
	}

	public String getSUBJ_INMT_SEQN() {
		return SUBJ_INMT_SEQN;
	}

	public void setSUBJ_INMT_SEQN(String subj_inmt_seqn) {
		SUBJ_INMT_SEQN = subj_inmt_seqn;
	}

	public String[] getSUBJ_INMT_SEQN_ARR() {
		return SUBJ_INMT_SEQN_ARR;
	}

	public void setSUBJ_INMT_SEQN_ARR(String[] subj_inmt_seqn_arr) {
		SUBJ_INMT_SEQN_ARR = subj_inmt_seqn_arr;
	}

	public String getAUTR_DIVS() {
		return AUTR_DIVS;
	}

	public void setAUTR_DIVS(String autr_divs) {
		AUTR_DIVS = autr_divs;
	}

	public String getBOOK_DIVS() {
		return BOOK_DIVS;
	}

	public void setBOOK_DIVS(String book_divs) {
		BOOK_DIVS = book_divs;
	}

	public String getBOOK_SIZE_DIVS() {
		return BOOK_SIZE_DIVS;
	}

	public void setBOOK_SIZE_DIVS(String book_size_divs) {
		BOOK_SIZE_DIVS = book_size_divs;
	}

	public String getSCHL() {
		return SCHL;
	}

	public void setSCHL(String schl) {
		SCHL = schl;
	}

	public String getSCHL_YEAR_DIVS() {
		return SCHL_YEAR_DIVS;
	}

	public void setSCHL_YEAR_DIVS(String schl_year_divs) {
		SCHL_YEAR_DIVS = schl_year_divs;
	}

	public String getSUBJ_NAME() {
		return SUBJ_NAME;
	}

	public void setSUBJ_NAME(String subj_name) {
		SUBJ_NAME = subj_name;
	}

	public String getWORK_DIVS() {
		return WORK_DIVS;
	}

	public void setWORK_DIVS(String work_divs) {
		WORK_DIVS = work_divs;
	}

	public String getISSUED_DATE2() {
		return ISSUED_DATE2;
	}

	public void setISSUED_DATE2(String issued_date2) {
		ISSUED_DATE2 = issued_date2;
	}

	public String getSrchDirect() {
		return srchDirect;
	}

	public void setSrchDirect(String srchDirect) {
		this.srchDirect = srchDirect;
	}

	public String getSrchPlayers() {
		return srchPlayers;
	}

	public void setSrchPlayers(String srchPlayers) {
		this.srchPlayers = srchPlayers;
	}

	public String getBROAD_DATE2() {
		return BROAD_DATE2;
	}

	public void setBROAD_DATE2(String broad_date2) {
		BROAD_DATE2 = broad_date2;
	}

	public String getSrchPrpsRghtCode() {
		return srchPrpsRghtCode;
	}

	public void setSrchPrpsRghtCode(String srchPrpsRghtCode) {
		this.srchPrpsRghtCode = srchPrpsRghtCode;
	}

	public String getNONPERF() {
		return NONPERF;
	}

	public void setNONPERF(String nonperf) {
		NONPERF = nonperf;
	}

	public String getSrchNonPerf() {
		return srchNonPerf;
	}

	public void setSrchNonPerf(String srchNonPerf) {
		this.srchNonPerf = srchNonPerf;
	}
	public String getARTICL_DT() {
		return ARTICL_DT;
	}
	public void setARTICL_DT(String articl_dt) {
		ARTICL_DT = articl_dt;
	}
	public String getARTICL_PUBC_SDATE() {
		return ARTICL_PUBC_SDATE;
	}
	public void setARTICL_PUBC_SDATE(String articl_pubc_sdate) {
		ARTICL_PUBC_SDATE = articl_pubc_sdate;
	}
	public String getPROVIDER_CD() {
		return PROVIDER_CD;
	}
	public void setPROVIDER_CD(String provider_cd) {
		PROVIDER_CD = provider_cd;
	}
	public String getPROVIDER_NM() {
		return PROVIDER_NM;
	}
	public void setPROVIDER_NM(String provider_nm) {
		PROVIDER_NM = provider_nm;
	}
	public String getREPORTER_NM() {
		return REPORTER_NM;
	}
	public void setREPORTER_NM(String reporter_nm) {
		REPORTER_NM = reporter_nm;
	}
	public String getCOI_CODE() {
		return COI_CODE;
	}
	public void setCOI_CODE(String coi_code) {
		COI_CODE = coi_code;
	}
	public String getLINK_PAGE_URL() {
		return LINK_PAGE_URL;
	}
	public void setLINK_PAGE_URL(String link_page_url) {
		LINK_PAGE_URL = link_page_url;
	}
	public String getSrchProviderVcd() {
		return srchProviderVcd;
	}
	public void setSrchProviderVcd(String srchProviderVcd) {
		this.srchProviderVcd = srchProviderVcd;
	}
	public String getPROVIDER_YN() {
		return PROVIDER_YN;
	}
	public void setPROVIDER_YN(String provider_yn) {
		PROVIDER_YN = provider_yn;
	}
	public String getSrchProviderNm() {
		return srchProviderNm;
	}
	public void setSrchProviderNm(String srchProviderNm) {
		this.srchProviderNm = srchProviderNm;
	}
	public String getPROVIDER_NM_ORGN() {
		return PROVIDER_NM_ORGN;
	}
	public void setPROVIDER_NM_ORGN(String provider_nm_orgn) {
		PROVIDER_NM_ORGN = provider_nm_orgn;
	}
	public String getSTotalRow() {
		return sTotalRow;
	}
	public void setSTotalRow(String totalRow) {
		sTotalRow = totalRow;
	}
	
	public String getROW_NUM() {
		return ROW_NUM;
	}
	public void setROW_NUM(String rOW_NUM) {
		ROW_NUM = rOW_NUM;
	}
}