package kr.or.copyright.mls.console.email;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.email.inter.FdcrAdA4Dao;
import kr.or.copyright.mls.console.email.inter.FdcrAdA5Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAdA5Service" )
public class FdcrAdA5ServiceImpl extends CommandService implements FdcrAdA5Service{

	@Resource( name = "fdcrAdA4Dao" )
	private FdcrAdA4Dao fdcrAdA4Dao;

	/**
	 * 메시지 관리 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA5List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA4Dao.selectEmailMsgList( commandMap );

		commandMap.put( "ds_list", list );

	}

	/**
	 * 메시지 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA5Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			String[] MSG_IDS = (String[]) commandMap.get( "MSG_ID" );
			for( int i = 0; i < MSG_IDS.length; i++ ){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "MSG_ID", MSG_IDS[i] );
				fdcrAdA4Dao.deleteEmailMsg( map );
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 메시지 관리 상세 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA5View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA4Dao.selectEmailMsg( commandMap );
		List list2 = (List) fdcrAdA4Dao.selectEmailAddrListByMsgId( commandMap );
		List fileList = (List) fdcrAdA4Dao.selectMailAttcList( commandMap );

		commandMap.put( "ds_mail", list );
		commandMap.put( "ds_send2", list2 );
		commandMap.put( "ds_detail", fileList );

	}

	/**
	 * 메시지 수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA5Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String MSG_ID = (String) commandMap.get( "MSG_ID" );
			String GUBUN = (String) commandMap.get( "GUBUN" );
			String MSG_STOR_CD = (String) commandMap.get( "MSG_STOR_CD" );
			String TITLE = (String) commandMap.get( "TITLE" );
			String CONTENT = (String) commandMap.get( "CONTENT" );
			String RGST_IDNT = (String) commandMap.get( "RGST_IDNT" );

			String[] RECV_CDS = (String[]) commandMap.get( "RECV_CD" );
			String[] RECV_SEQNS = (String[]) commandMap.get( "RECV_SEQN" );
			String[] RECV_NAMES = (String[]) commandMap.get( "RECV_NAME" );
			String[] RECV_PERS_IDS = (String[]) commandMap.get( "RECV_PERS_ID" );
			String[] RECV_MAIL_ADDRS = (String[]) commandMap.get( "RECV_MAIL_ADDR" );
			String[] RECV_GROUP_IDS = (String[]) commandMap.get( "RECV_GROUP_ID" );
			String[] RECV_GROUP_NAMES = (String[]) commandMap.get( "RECV_GROUP_NAME" );
			String[] FILE_NAMES = (String[]) commandMap.get( "FILE_NAME" );
			String[] FILE_PATHS = (String[]) commandMap.get( "FILE_PATH" );
			String[] FILE_SIZES = (String[]) commandMap.get( "FILE_SIZE" );
			String[] ATTC_SEQNS = (String[]) commandMap.get( "ATTC_SEQN" );

			String row_status;
			try{
				boolean flag = false;
				Map<String, Object> tempMap = new HashMap<String, Object>();
				tempMap.put( "MSG_ID", MSG_ID );
				tempMap.put( "GUBUN", GUBUN );
				tempMap.put( "MSG_STOR_CD", MSG_STOR_CD );

				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "TITLE", TITLE );
				map.put( "CONTENT", CONTENT );
				map.put( "RGST_IDNT", RGST_IDNT );

				int msgIdSeq =
					Integer.parseInt( (String) ( tempMap.get( "MSG_ID" ) == null ? "0" : tempMap.get( "MSG_ID" ) ) );
				map.put( "MSG_ID", msgIdSeq );

				fdcrAdA4Dao.updateEmailMsg( map );

				// 첨부파일 delete 처리
				for( int i = 0; MSG_ID != null && i < ATTC_SEQNS.length; i++ ){
					Map<String, Object> deleteMap = new HashMap<String, Object>();
					deleteMap.put( "MSG_ID", MSG_ID );
					deleteMap.put( "ATTC_SEQN", ATTC_SEQNS[i] );
					fdcrAdA4Dao.deleteMailAttc( deleteMap );

					String fileName = commandMap.get( "REAL_FILE_NAME" ).toString();

					try{
						File file = new File( (String) deleteMap.get( "FILE_PATH" ), fileName );
						if( file.exists() ){
							file.delete();
						}
					}
					catch( Exception e ){
						e.printStackTrace();
					}
				}

				// detail INSERT, UPDATE처리
				for( int i = 0; i < ATTC_SEQNS.length; i++ ){

					// row_status = ds_detail.getRowStatus(i);
					row_status = "";

					Map<String, Object> fileMap = new HashMap<String, Object>();

					if( row_status.equals( "insert" ) == true ){
						byte[] file = (byte[]) commandMap.get( "CLIENT_FILE" );

						String fileName = commandMap.get( "REAL_FILE_NAME" ).toString();

						Map upload = FileUtil.uploadMiFile( fileName, file, "mail/" );

						// // 파일저장명 put
						fileMap.put( "MSG_ID", msgIdSeq );
						fileMap.put( "REAL_FILE_NAME", upload.get( "REAL_FILE_NAME" ) );
						fileMap.put( "FILE_PATH", upload.get( "FILE_PATH" ) );
						fileMap.put( "FILE_NAME", fileName );

						fdcrAdA4Dao.insertMailAttc( fileMap );
					}
				}

				flag = fdcrAdA4Dao.deleteEmailRecvInfo( tempMap );
				if( flag ){
					for( int i = 0; i < RECV_PERS_IDS.length; i++ ){
						Map<String, Object> map2 = new HashMap<String, Object>();
						map2.put( "MSG_ID", msgIdSeq );
						map2.put( "RECV_SEQN", i + 1 );
						map2.put( "RECV_CD", RECV_CDS[i] );
						map2.put( "RECV_NAME", RECV_NAMES[i] );
						map2.put( "RECV_PERS_ID", RECV_PERS_IDS[i] );
						map2.put( "RECV_MAIL_ADDR", RECV_MAIL_ADDRS[i] );
						map2.put( "RECV_GROUP_ID", RECV_GROUP_IDS[i] );
						map2.put( "RECV_GROUP_NAME", RECV_GROUP_NAMES[i] );
						fdcrAdA4Dao.insertEmailRecvInfo( map2 );
					}
					for( int j = 0; j < RECV_CDS.length; j++ ){
						Map<String, Object> map3 = new HashMap<String, Object>();
						map3.put( "MSG_ID", msgIdSeq );
						map3.put( "RECV_SEQN", j + 1 );
						map3.put( "RECV_CD", RECV_CDS[j] );
						map3.put( "RECV_NAME", RECV_NAMES[j] );
						map3.put( "RECV_PERS_ID", RECV_PERS_IDS[j] );
						map3.put( "RECV_MAIL_ADDR", RECV_MAIL_ADDRS[j] );
						map3.put( "RECV_GROUP_ID", RECV_GROUP_IDS[j] );
						map3.put( "RECV_GROUP_NAME", RECV_GROUP_NAMES[j] );
						fdcrAdA4Dao.insertEmailRecvInfo( map3 );
					}

				}

			}
			catch( Exception e ){
				e.printStackTrace();
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
