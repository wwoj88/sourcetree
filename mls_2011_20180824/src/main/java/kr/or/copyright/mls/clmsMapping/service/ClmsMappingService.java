package kr.or.copyright.mls.clmsMapping.service;

public interface ClmsMappingService {
	
	public void mappingListForm() throws Exception;
	
	public void kappList() throws Exception;
	
	public void komcaList() throws Exception;
	
	public void icnUpdate() throws Exception;
	
	public void icnList() throws Exception;
	
	public void icnDelete() throws Exception;
}
