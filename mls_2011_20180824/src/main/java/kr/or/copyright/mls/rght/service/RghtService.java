package kr.or.copyright.mls.rght.service;

public interface RghtService {
	// 내권리찾기(음악조회)
	public void muscRghtList() throws Exception;

	// 내권리찾기(음악상세조회)	  
	public void muscRghtDetail() throws Exception; 
	
	// 내권리찾기(어문 조회)	
	public void bookRghtList() throws Exception;
	
	// 내권리찾기(어문상세조회)	  
	public void bookRghtDetail() throws Exception;
	
	// 내권리찾기(이미지 조회)	
	public void imgeRghtList() throws Exception;

	// 내권리찾기(저작권자 음악 조회)	
	public void muscList() throws Exception;
	
	// 내권리찾기(저작권자 어문 조회)	
	public void bookList() throws Exception;	
	
	// 내권리찾기(저작권자 어문 조회)	
	public void rghtDetlList() throws Exception;	
	
	// 내권리찾기 신청
	public void rghtSave() throws Exception;	
	
	// 내권리찾기 수정
	public void rghtModi() throws Exception;
	
	// 내권리찾기 수정
	public void rghtDelete() throws Exception;	

	// 내권리찾기 처리결과조회
	public void rghtDetlDescList() throws Exception;		
}
