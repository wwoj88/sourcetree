package kr.or.copyright.mls.main.service;

import java.util.List;

import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.main.dao.MainDao;
import kr.or.copyright.mls.main.model.Main;

public class MainServiceImpl extends BaseService implements MainService {

	//private Log logger = LogFactory.getLog(getClass());
	
	private MainDao mainDao;

	public void setMainDao(MainDao mainDao) {
		this.mainDao = mainDao;
	}

	public Main mainView(Main main) {
		
//		main.setQnaList(mainDao.qustList(main)); // faq 
		main.setInmtList(mainDao.inmtList(main)); // 보상금 저작물
		main.setNotiList(mainDao.notiList(main)); // 공지사항
		main.setWorkList(mainDao.workList(main)); // 신규등록저작물
		main.setLawList(mainDao.lawList(main)); // 법정허락저작물
//		main.setWorkNoneList(mainDao.workNoneList(main));
		main.setPromPotoList(mainDao.promPotoList(main));
//		main.setInmt(mainDao.alltInmt(main));
		//main.setInmt202(mainDao.alltInmt202(main));
		//main.setInmt203(mainDao.alltInmt203(main));
		//main.setInmtL(mainDao.alltInmtL(main));
		//main.setInmtS(mainDao.alltInmtS(main));
//		main.setPromMovieList(mainDao.promMovieList(main));
		
		return main;
	}
	
	public void insertUserCount() {
		mainDao.insertUserCount();
	}
	public List anucBord01(){
		return mainDao.anucBord01();
	}
	public List anucBord06(){
		return mainDao.anucBord06();
	}	
	public List anucBord03(){
		return mainDao.anucBord03();
	}
	public List anucBord04(){
		return mainDao.anucBord04();
	}
	public List anucBord05(){
		return mainDao.anucBord05();
	}
	public List anucBord07(){
		return mainDao.anucBord07();
	}
	public List QnABord(){
		return mainDao.QnABord();
	}
	public List statSrch(){
		return mainDao.statSrch();
	}
	public String alltInmt(){
		return mainDao.alltInmt();
	}
	public List notiList(){
		return mainDao.notiList();
	}
}
