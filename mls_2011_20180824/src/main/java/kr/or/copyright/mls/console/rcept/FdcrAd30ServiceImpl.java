package kr.or.copyright.mls.console.rcept;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd20Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd30Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd30Service" )
public class FdcrAd30ServiceImpl extends CommandService implements FdcrAd30Service{

	@Resource( name = "fdcrAd03Dao" )
	private FdcrAd03Dao fdcrAd03Dao;

	@Resource( name = "fdcrAd20Dao" )
	private FdcrAd20Dao fdcrAd20Dao;

	/**
	 * 미분배보상금 교과용 저작물 보고일 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd30List1( Map<String, Object> commandMap ) throws Exception{

		// DAO호출
		List list = (List) fdcrAd03Dao.reptMgntList( commandMap );

		commandMap.put( "ds_list_0", list );

	}

}
