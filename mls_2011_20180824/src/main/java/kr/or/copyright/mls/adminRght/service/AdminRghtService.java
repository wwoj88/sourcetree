package kr.or.copyright.mls.adminRght.service;


public interface AdminRghtService {

	// 내권리찾기 조회
	public void rghtList() throws Exception;
 
	// 내권리찾기 상세조회
	public void rghtDetlList() throws Exception;	
	
	// 신청저작물 상세조회
	public void selectWorksInfo() throws Exception;
	
	// 내권리찾기 저장
	public void rghtSave() throws Exception;	
	
	// 내권리찾기 완료처리
	public void rghtEndUpdate() throws Exception;	
	
	// 내권리찾기 삭제처리
	public void rghtDelete() throws Exception;		
	

	// 통합저작권자ID신
	public void getIcnNumb() throws Exception;	
	
	
}
