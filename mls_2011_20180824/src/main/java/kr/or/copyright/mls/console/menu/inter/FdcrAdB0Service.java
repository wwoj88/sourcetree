package kr.or.copyright.mls.console.menu.inter;

import java.util.Map;

public interface FdcrAdB0Service{
	
	/**
	 * 그룹 목록 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB0List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 그룹 조회하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB0View1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 그룹 메뉴 조회하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdB0List2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 그룹 등록하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean groupInsert( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 그룹 메뉴 삭제하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean groupDelete( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 그룹 메뉴 수정하기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAdB0Update1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	  * <PRE>
	  * 간략 : 그룹 삭제
	  * 상세 : 그룹 삭제
	  * <PRE>
	  * @param commandMap
	  * @return
	  * @throws Exception 
	  */
	public boolean fdcrAdB0Delete1( Map<String, Object> commandMap ) throws Exception;
	
}
