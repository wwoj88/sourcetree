package kr.or.copyright.mls.stat.model;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @Class Name : MlStatBordDefaultVO.java
 * @Description : MlStatBord Default VO class
 * @Modification Information
 *
 * @author 정병호
 * @since 2012.08.06
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public class MlStatWorksDefaultVO implements Serializable {
	
    private String searchCondition = "";//장르
    
    private String searchKeyword1 = "";//제호(제목)
    
    private String searchKeyword2 = "";//저작권자
    
    private String searchWorksId ="";//저작물ID
    
    private String rnum;
    
    private String pageNum =""; //현재 페이지 번호
    
    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    /** COPT_HODR_NAME */
//    private String coptHodrName;
    
    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }
    
    public String getSearchWorksId() {
        return searchWorksId;
    }

    public void setSearchWorksId(String searchWorksId) {
        this.searchWorksId = searchWorksId;
    }

    private String searchUseYn = "";
    
    private int pageIndex = 1;
    
    private int pageUnit = 10;
    
    private int pageSize = 10;

    public String getSearchKeyword1() {
        return searchKeyword1;
    }

    public void setSearchKeyword1(String searchKeyword1) {
        this.searchKeyword1 = searchKeyword1;
    }

    public String getSearchKeyword2() {
        return searchKeyword2;
    }

    public void setSearchKeyword2(String searchKeyword2) {
        this.searchKeyword2 = searchKeyword2;
    }

    /** firstIndex */
    private int firstIndex = 1;

    /** lastIndex */
    private int lastIndex = 1;

    /** recordCountPerPage */
    private int recordCountPerPage = 10;
    
        
	public int getFirstIndex() {
		return firstIndex;
	}

	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}

	public int getLastIndex() {
		return lastIndex;
	}

	public void setLastIndex(int lastIndex) {
		this.lastIndex = lastIndex;
	}

	public int getRecordCountPerPage() {
		return recordCountPerPage;
	}

	public void setRecordCountPerPage(int recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}

	public String getSearchCondition() {
        return searchCondition;
    }

    public void setSearchCondition(String searchCondition) {
        this.searchCondition = searchCondition;
    }

    public String getSearchUseYn() {
        return searchUseYn;
    }

    public void setSearchUseYn(String searchUseYn) {
        this.searchUseYn = searchUseYn;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageUnit() {
        return pageUnit;
    }

    public void setPageUnit(int pageUnit) {
        this.pageUnit = pageUnit;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
