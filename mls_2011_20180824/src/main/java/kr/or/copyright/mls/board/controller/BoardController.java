package kr.or.copyright.mls.board.controller;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.common.userLogin.controller.LoginFormController;
import kr.or.copyright.mls.board.model.Board;
import kr.or.copyright.mls.board.model.BoardFile;
import kr.or.copyright.mls.board.service.BoardService;
import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.support.util.FileUploadUtil;
import kr.or.copyright.mls.support.util.StringUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.record.formula.functions.Replace;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.tagfree.util.MimeUtil;


public class BoardController extends MultiActionController {

	//private Log logger = LogFactory.getLog(getClass());

	private BoardService boardService;

	//private String realUploadPath = "D:/server/mls/mls_2010/upload/";  // 로컬용
	//private String realUploadPath = "C:/home/tmax/mls/web/upload/";  // 로컬용
	//private String realUploadPath = "/home/tmax/mls/web/upload/";  // 실서버용
	//private String realUploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버용
	private String realUploadPath = FileUtil.uploadPath();
	private String DownLoadPath;
		
	public void setDownLoadPath(String downLoadPath) {
		this.DownLoadPath = downLoadPath;
	}
	
	public void setBoardService(BoardService boardService) {
		this.boardService = boardService;
	}

	public ModelAndView list(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
				
		Map params = new HashMap();
		params.put("srchDivs", ServletRequestUtils.getStringParameter(request, "srchDivs"));
		params.put("srchText", ServletRequestUtils.getStringParameter(request, "srchText"));
		params.put("PAGE_NO", ServletRequestUtils.getIntParameter(request, "page_no", 1));
		params.put("menuSeqn", ServletRequestUtils.getStringParameter(request, "menuSeqn"));
		params.put("rgstIdnt", ServletRequestUtils.getStringParameter(request, "rgstIdnt"));
		params.put("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));
		params.put("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));
		
		//법정허락공고 
		params.put("gubun", ServletRequestUtils.getStringParameter(request, "gubun", ""));		// 메인에서 넘어오는 경우 사용. totalsearch
		params.put("srchGubun", ServletRequestUtils.getStringParameter(request, "srchGubun"));
		params.put("srchTitle", ServletRequestUtils.getStringParameter(request, "srchTitle"));
		params.put("srchApprDttm", ServletRequestUtils.getStringParameter(request, "srchApprDttm"));
		params.put("srchClmsTite", ServletRequestUtils.getStringParameter(request, "srchClmsTite"));
		params.put("srchGenre", ServletRequestUtils.getStringParameter(request, "srchGenre"));
		
		String srchFILD1 = ServletRequestUtils.getStringParameter(request, "FILD1");
		String srchFILD2 = ServletRequestUtils.getStringParameter(request, "FILD2");
		String srchFILD3 = ServletRequestUtils.getStringParameter(request, "FILD3");
		String srchFILD4 = ServletRequestUtils.getStringParameter(request, "FILD4");
		String srchFILD5 = ServletRequestUtils.getStringParameter(request, "FILD5");
		String srchFILD6 = ServletRequestUtils.getStringParameter(request, "FILD6");
		String srchFILD7 = ServletRequestUtils.getStringParameter(request, "FILD7");
		String srchFILD8 = ServletRequestUtils.getStringParameter(request, "FILD8");
		String srchFILD9 = ServletRequestUtils.getStringParameter(request, "FILD9");
		String srchFILD10 = ServletRequestUtils.getStringParameter(request, "FILD10");
		String srchFILD11 = ServletRequestUtils.getStringParameter(request, "FILD11");
		String srchFILD99 = ServletRequestUtils.getStringParameter(request, "FILD99");
		
		//params.put("srchFILD", ServletRequestUtils.getStringParameter(request, "srchFILD"));
		params.put("FILD1", srchFILD1);
		params.put("FILD2", srchFILD2);
		params.put("FILD3", srchFILD2);
		params.put("FILD4", srchFILD4);
		params.put("FILD5", srchFILD5);
		params.put("FILD6", srchFILD6);
		params.put("FILD7", srchFILD7);
		params.put("FILD8", srchFILD8);    
		params.put("FILD9", srchFILD9);
		params.put("FILD10", srchFILD10);
		params.put("FILD11", srchFILD11);
		params.put("FILD99", srchFILD99);
		params.put("FILD", ServletRequestUtils.getStringParameter(request, "FILD"));
		
		int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
		String menuSeqn = ServletRequestUtils.getStringParameter(request, "menuSeqn");
 
		if ("2".equals(menuSeqn)) { // Q&A
			return new ModelAndView("qust/qustAnswList", "boardList", boardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		} else if ("4".equals(menuSeqn)) {	// 공지사항
			return new ModelAndView("noti/notiList", "boardList", boardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		} else if ("5".equals(menuSeqn)) {	// 홍보관
//			params.put("rgstKindCode", ServletRequestUtils.getStringParameter(request, "leftsub"));
			return new ModelAndView("prom/promList", "boardList", boardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		} else if ("6".equals(menuSeqn)) {	// 커뮤니티 
			return new ModelAndView("comm/commList", "boardList", boardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		} else if ("8".equals(menuSeqn)) {	// 법정허락 저작물
			return new ModelAndView("stat/statList", "boardList", boardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		} else if ("9".equals(menuSeqn)) { //나의 질문과 답 추가 (20110914,정병호)
			//params.put("menuSeqn", "2");
			return new ModelAndView("my/myQustList", "boardList", boardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		} else if("7".equals(menuSeqn)) {  // 이벤트
			params.put("menuSeqn", "6"); 
			return new ModelAndView("board/eventList", "boardList", boardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		} else {
			return new ModelAndView("ftaq/ftaqList", "boardList", boardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		}
		
	} 

	public ModelAndView boardView(HttpServletRequest request, HttpServletResponse respone) throws Exception {
		
		Map params = new HashMap();
		params.put("srchDivs", ServletRequestUtils.getStringParameter(request, "srchDivs"));
		params.put("srchText", ServletRequestUtils.getStringParameter(request, "srchText"));
		params.put("PAGE_NO", ServletRequestUtils.getIntParameter(request, "page_no", 1));
		params.put("menuSeqn", ServletRequestUtils.getStringParameter(request, "menuSeqn"));
		params.put("rgstIdnt", ServletRequestUtils.getStringParameter(request, "rgstIdnt"));
		params.put("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));
		params.put("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));
		
		//법정허락공고 
		params.put("gubun", ServletRequestUtils.getStringParameter(request, "gubun", ""));		// 메인에서 넘어오는 경우 사용. totalsearch
		params.put("srchGubun", ServletRequestUtils.getStringParameter(request, "srchGubun"));
		params.put("srchTitle", ServletRequestUtils.getStringParameter(request, "srchTitle"));
		params.put("srchApprDttm", ServletRequestUtils.getStringParameter(request, "srchApprDttm"));
		params.put("srchClmsTite", ServletRequestUtils.getStringParameter(request, "srchClmsTite"));
		params.put("srchGenre", ServletRequestUtils.getStringParameter(request, "srchGenre"));
		
		String srchFILD1 = ServletRequestUtils.getStringParameter(request, "FILD1");
		String srchFILD2 = ServletRequestUtils.getStringParameter(request, "FILD2");
		String srchFILD3 = ServletRequestUtils.getStringParameter(request, "FILD3");
		String srchFILD4 = ServletRequestUtils.getStringParameter(request, "FILD4");
		String srchFILD5 = ServletRequestUtils.getStringParameter(request, "FILD5");
		String srchFILD6 = ServletRequestUtils.getStringParameter(request, "FILD6");
		String srchFILD7 = ServletRequestUtils.getStringParameter(request, "FILD7");
		String srchFILD8 = ServletRequestUtils.getStringParameter(request, "FILD8");
		String srchFILD9 = ServletRequestUtils.getStringParameter(request, "FILD9");
		String srchFILD10 = ServletRequestUtils.getStringParameter(request, "FILD10");
		String srchFILD11 = ServletRequestUtils.getStringParameter(request, "FILD11");
		String srchFILD99 = ServletRequestUtils.getStringParameter(request, "FILD99");
		
		//params.put("srchFILD", ServletRequestUtils.getStringParameter(request, "srchFILD"));
		params.put("FILD1", srchFILD1);
		params.put("FILD2", srchFILD2);
		params.put("FILD3", srchFILD2);
		params.put("FILD4", srchFILD4);
		params.put("FILD5", srchFILD5);
		params.put("FILD6", srchFILD6);
		params.put("FILD7", srchFILD7);
		params.put("FILD8", srchFILD8);    
		params.put("FILD9", srchFILD9);
		params.put("FILD10", srchFILD10);
		params.put("FILD11", srchFILD11);
		params.put("FILD99", srchFILD99);
		params.put("FILD", ServletRequestUtils.getStringParameter(request, "FILD"));
		
		int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
		
		
		
		
		
		
		Board board = new Board();
		
		board.setBordSeqn(ServletRequestUtils.getRequiredLongParameter(request, "bordSeqn"));
		board.setMenuSeqn(ServletRequestUtils.getRequiredLongParameter(request, "menuSeqn"));
		board.setSrchDivs(ServletRequestUtils.getStringParameter(request, "srchDivs"));
		board.setSrchText(ServletRequestUtils.getStringParameter(request, "srchText"));

		board.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate"));
		board.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate"));
		
		
		board.setPage_no(ServletRequestUtils.getStringParameter(request, "page_no"));
		
		
		String menuSeqn = ServletRequestUtils.getStringParameter(request, "menuSeqn");
		String submitType = ServletRequestUtils.getStringParameter(request, "submitType");
		board.setRgstKindCode(ServletRequestUtils.getStringParameter(request, "leftsub"));
		
		if ("8".equals(menuSeqn)) {
			boardService.updateInqrCont(board);
		}else if("7".equals(menuSeqn)){
			board.setMenuSeqn(6);
			board.setThreaded(ServletRequestUtils.getRequiredLongParameter(request, "threaded"));
			boardService.updateInqrCont(board);
		}else{
			board.setThreaded(ServletRequestUtils.getRequiredLongParameter(request, "threaded"));
			boardService.updateInqrCont(board);
		}
		
		if ("2".equals(menuSeqn)) {
			if ("update".equals(submitType)) {
				System.out.println( "update call" );
				//System.out.println("boardService.boardView(board)"+ boardService.boardView(board) );
				/*System.out.println( request.getParameter( "srchDivs" ) +" : "+ request.getParameter( "srchText" ) +" : "+ request.getParameter( "page_no" ));*/
				request.setAttribute( "submitType", "update" );
				return new ModelAndView("qust/NewqustAnswQutnModi", "board", boardService.boardView(board));
			} else {
				return new ModelAndView("qust/qustAnswDetl", "board", boardService.boardView(board));
				/*return new ModelAndView("qust/__qustAnswDetl", "board", boardService.boardView(board));*/
			}
		/* 양재석 추가 start */
		} else if ("3".equals(menuSeqn) || "4".equals(menuSeqn)) {
			return new ModelAndView("noti/notiDetl", "board", boardService.boardView(board));
		/* 양재석 추가 end */
		} else if ("6".equals(menuSeqn)) {
			if ("update".equals(submitType)) {
				return new ModelAndView("comm/commModi", "board", boardService.boardView(board));
			} else {
				return new ModelAndView("comm/commDetl", "board", boardService.boardView(board));
			}
		} else if ("5".equals(menuSeqn)) {
			return new ModelAndView("prom/promDetl", "board", boardService.boardView(board));
		}else if ("8".equals(menuSeqn)) {
			return new ModelAndView("stat/statDetl", "board", boardService.boardView(board));
		}else if ("9".equals(menuSeqn)) {
			return new ModelAndView("my/myQustDetl", "board", boardService.boardView(board));
		}else if("7".equals(menuSeqn)) {
			params.put("menuSeqn", "6");
			return new ModelAndView("board/eventList", "boardList", boardService.findBoardList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
		}else {
			return new ModelAndView("ftaq/ftaqDetl", "board", boardService.boardView(board));			
		}
	}
	
	public ModelAndView insertQust(HttpServletRequest request, HttpServletResponse response, BoardFile boardDto  ) throws Exception {

		Board board = new Board();
		
		String strSavePath = "D:\\Git\\mls_2011\\web\\upload\\test\\"; // 실제 웹 서버에 저장되는 디렉터리를 지정한다.
		String strSaveUrl = "http://localhost:8080/upload/test/"; // 실제 웹 서버에 저장되는 디렉터리의 웹 URL 경로를 지정한다.
		//String strSaveUrl = "http://www.right4me.or.kr:8080/upload/editor"; // 실제 웹 서버에 저장되는 디렉터리의 웹 URL 경로를 지정한다.
		
		//String strSavePath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("strSavePath"));// 실제 웹 서버에 저장되는 디렉터리를 지정한다.
		//String strSaveUrl = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("strSaveUrl"));// 실제 웹 서버에 저장되는 디렉터리의 웹 URL 경로를 지정한다.
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request); 
		System.out.println( "insertQust call" );

		List<MultipartFile> MultiFile = boardDto.getFileList(); 
		
		List<String> nameList = new ArrayList<String>();
		System.out.println( MultiFile );
		for(MultipartFile file : MultiFile)
		{
			nameList.add( file.getOriginalFilename() );
			File copyFile = new File(strSavePath+file.getOriginalFilename());
			 file.transferTo(copyFile);//원래 업로드한 파일이 해당 경로로 이동
		}
		
		/*board.setBordSeqn( StringUtil.nullToZero( multipartRequest.getParameter("bordSeqn")) );*/
		/*board.setThreaded( StringUtil.nullToZero(multipartRequest.getParameter("threaded")) );*/
		//파일 패스 추가
		board.setFilePath( strSaveUrl );
		
		board.setMenuSeqn( StringUtil.nullToZero(multipartRequest.getParameter("menuSeqn")) );
		board.setModiIdnt(multipartRequest.getParameter("modiIdnt"));
		board.setTite(multipartRequest.getParameter("tite"));
		board.setRgstIdnt(multipartRequest.getParameter("rgstIdnt"));
		board.setRgstName(multipartRequest.getParameter("rgstName"));
		board.setPswd(multipartRequest.getParameter("pswd"));
		board.setBordDesc(multipartRequest.getParameter("bordDesc"));	
		board.setBordDescTag(multipartRequest.getParameter("bordDescTag"));	
		board.setInqrCont(0);
		board.setMail(multipartRequest.getParameter("mail"));
		board.setDeth(0);
		board.setHtmlYsno(StringUtil.nullToEmptyChk(multipartRequest.getParameter("htmlYsno")) );
		board.setFileList( MultiFile );
	/*	board.setFileName(multipartRequest.getParameter("fileName"));
		board.setFilePath(multipartRequest.getParameter("filePath"));
		board.setFileSize(multipartRequest.getParameter("fileSize"));	*/
		board.setSrchDivs(multipartRequest.getParameter("srchDivs"));
		board.setSrchText(StringUtil.nullToEmpty(multipartRequest.getParameter("srchText")) );
		board.setPage_no(multipartRequest.getParameter("page_no") == null ? "1" : multipartRequest.getParameter("page_no") );
		System.out.println( MultiFile );
	
		System.out.println( "MimeUtil call" );
		/*MimeUtil util = new MimeUtil(); // com.tagfree.util.MimeUtil 생성
		
		util.setMimeValue(multipartRequest.getParameter("mime_contents")); // 작성된 본문 + 포함된 이진 파일의 MIME 값 지정
		System.out.println( util.getCharsetInMime() );  
		util.setSavePath(strSavePath); // 저장 디렉터리 지정
		System.out.println( strSavePath );
		util.setSaveUrl(strSaveUrl); // URL 경로 지정
		System.out.println( strSaveUrl );
		util.setSaveFilePattern("yyyyMMddHHmmss");
		System.out.println( "MimeUtil setValue4" );
		util.setRename(true); // 파일을 저장 시에 새로운 이름을 생성할 것인지를 설정
		System.out.println( "MimeUtil setValue5" );
		util.processDecoding(); // MIME 값의 디코딩 -> 이 때 포함된 파일은 모두 웹 서버에 저장된다.
		System.out.println( "MimeUtil setValue6" );
		board.setBordDescTag(util.getDecodedHtml(false)); //디코딩
		System.out.println( "MimeUtil setValue7" );*/
		/*
		Enumeration e = util.getDecodedFileList(); // 디코딩되어 저장된 파일 이름 리스트
 		
		while (e.hasMoreElements()) 
 		{
 			String s = (String)e.nextElement();
 			out.println(new String(s.getBytes("iso8859-1"), "euc-kr"));
 		}
 		*/

//		 File Upload
		/*Iterator fileNameIterator = multipartRequest.getFileNames();
		List fileList = new ArrayList();
		
		while(fileNameIterator.hasNext()) {
			MultipartFile multiFile= multipartRequest.getFile((String)fileNameIterator.next());
			if(multiFile.getSize()>0) {
				BoardFile boardFile = FileUploadUtil.uploadFormFiles(multiFile, realUploadPath);
				fileList.add(boardFile);
			}
		}*/
		
		
		     
		      
		     
		/*board.setFileList(fileList);*/
		
		int iResult = boardService.insertQust(board);
		
		//System.out.print("iResult>>>>>>>>>>>>>>>>>>>>"+iResult);
		//if(iResult > 0)
		//	return new ModelAndView("qust/qustRegiSucc");
		//else
		ModelAndView mv = new ModelAndView("qust/qustRegiSucc", "iResult", iResult);
		
		
		
		mv.addObject("title", board.getTite());
		
		return mv;
	}
	
    public ModelAndView isValidPwd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Board board = new Board();
		board.setBordSeqn( StringUtil.nullToZero( request.getParameter("bordSeqn")) );
		board.setMenuSeqn( StringUtil.nullToZero( request.getParameter("menuSeqn")) );
		board.setPswd(StringUtil.nullToEmpty(request.getParameter("pswd")));
		board.setThreaded( StringUtil.nullToZero(request.getParameter("threaded")) );
		
		board = boardService.isValidPswd(board);
		
		PrintWriter out = response.getWriter();
		if (board == null) {
			out.print("false");
		} else {
			out.print("true");
		}
		out.close();
		return null;
		
	}
    
	public ModelAndView updateQust(HttpServletRequest request, HttpServletResponse response, BoardFile boardDto) throws Exception {

		Board board = new Board();
		System.out.println( "updateQust call" );
		String strSavePath = "D:\\Git\\mls_2011\\web\\upload\\test\\"; // 실제 웹 서버에 저장되는 디렉터리를 지정한다.
		String strSaveUrl = "http://localhost:8080/upload/test/"; // 실제 웹 서버에 저장되는 디렉터리의 웹 URL 경로를 지정한다.
		/*CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request); 
		System.out.println( multipartRequest.getParameter( "bordSeqn" ) );*/
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request); 
		
		
		List<MultipartFile> MultiFile = boardDto.getFileList(); 
		
		List<String> nameList = new ArrayList<String>();
		System.out.println( MultiFile );
		for(MultipartFile file : MultiFile)
		{
			nameList.add( file.getOriginalFilename() );
			File copyFile = new File(strSavePath+file.getOriginalFilename());
			 file.transferTo(copyFile);//원래 업로드한 파일이 해당 경로로 이동
		}
		
		/*board.setBordSeqn( StringUtil.nullToZero( multipartRequest.getParameter("bordSeqn")) );*/
		/*board.setThreaded( StringUtil.nullToZero(multipartRequest.getParameter("threaded")) );*/
		//파일 패스 추가
		board.setFilePath( strSaveUrl );
		
		
		
		board.setBordSeqn( StringUtil.nullToZero( multipartRequest.getParameter("bordSeqn")) );
		board.setThreaded( StringUtil.nullToZero(multipartRequest.getParameter("threaded")) );
		board.setMenuSeqn( StringUtil.nullToZero(multipartRequest.getParameter("menuSeqn")) );
		board.setModiIdnt(multipartRequest.getParameter("rgstIdnt"));
		board.setTite(multipartRequest.getParameter("tite"));
		board.setRgstIdnt(multipartRequest.getParameter("rgstIdnt"));
		board.setBordDesc(multipartRequest.getParameter("bordDesc"));	
		board.setMail(multipartRequest.getParameter("mail"));
		board.setHtmlYsno(StringUtil.nullToEmptyChk(multipartRequest.getParameter("htmlYsno")) );
		/*board.setFileName(multipartRequest.getParameter("fileName"));
		board.setFilePath(multipartRequest.getParameter("filePath"));
		board.setFileSize(multipartRequest.getParameter("fileSize"));	*/
		board.setFileList( MultiFile );//추가
		board.setSrchDivs(multipartRequest.getParameter("srchDivs"));
		board.setSrchText(StringUtil.nullToEmpty(multipartRequest.getParameter("srchText")) );
		board.setPage_no(multipartRequest.getParameter("page_no") == null ? "1" : multipartRequest.getParameter("page_no") );
		System.out.println( multipartRequest.getParameter("fileName") );
		
		board.setPage_no(multipartRequest.getParameter("page_no") == null ? "1" : multipartRequest.getParameter("page_no") );
		
		//String strSavePath = "D:/01.project/04.mls_new/web/upload/editor"; // 실제 웹 서버에 저장되는 디렉터리를 지정한다.
		//String strSaveUrl = "http://www.right4me.or.kr:8080/upload/editor"; // 실제 웹 서버에 저장되는 디렉터리의 웹 URL 경로를 지정한다.
		
		/*String strSavePath = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("strSavePath"));// 실제 웹 서버에 저장되는 디렉터리를 지정한다.
		String strSaveUrl = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("strSaveUrl"));// 실제 웹 서버에 저장되는 디렉터리의 웹 URL 경로를 지정한다.
*/		

		/*MimeUtil util = new MimeUtil(); // com.tagfree.util.MimeUtil 생성
		 
		util.setMimeValue(multipartRequest.getParameter("mime_contents")); // 작성된 본문 + 포함된 이진 파일의 MIME 값 지정
		 
		util.setSavePath(strSavePath); // 저장 디렉터리 지정
		util.setSaveUrl(strSaveUrl); // URL 경로 지정
		util.setSaveFilePattern("yyyyMMddHHmmss");
		util.setRename(true); // 파일을 저장 시에 새로운 이름을 생성할 것인지를 설정
		util.processDecoding(); // MIME 값의 디코딩 -> 이 때 포함된 파일은 모두 웹 서버에 저장된다. 
		
		board.setBordDescTag(util.getDecodedHtml(false)); //디코딩
*/		
		//추가 다중파일 삭제
		Board returnboard = boardService.boardView(board);
		List deleteFileList = returnboard.getFileList();
		String[] chk;
		ArrayList<BoardFile> deleteBoardFile = new ArrayList<BoardFile>();
		
		for(Object deleteBoardFileList : deleteFileList)
		{
			BoardFile boardFile = new BoardFile();
			Board delFile = (Board)deleteBoardFileList;
			boardFile.setFileName( delFile.getFileName() );
			boardFile.setRealFileName( delFile.getRealFileName() );
			boardFile.setAttcSeqn( delFile.getAttcSeqn() );
			
			deleteBoardFile.add( boardFile );
		}
		
		
        if(deleteBoardFile != null){
			boardService.fileMultDelete( deleteBoardFile);
		}
		

        //File Upload //이전 소스
		/*Iterator fileNameIterator = multipartRequest.getFileNames();
		List fileList = new ArrayList();
		
		while(fileNameIterator.hasNext()) {
			MultipartFile multiFile= multipartRequest.getFile((String)fileNameIterator.next());
			if(multiFile.getSize()>0) {
				BoardFile boardFile = FileUploadUtil.uploadFormFiles(multiFile, realUploadPath);
				fileList.add(boardFile);
			}
		}*/
		
		/*board.setFileList(fileList);*/
		boardService.updateQust(board);
		
		return new ModelAndView("qust/qustModiSucc");
	}
	private Log logger = LogFactory.getLog(getClass());
	
	public ModelAndView fileDownLoad(HttpServletRequest request,  HttpServletResponse respone) throws Exception {
	   String filePath= request.getParameter( "filePath" );
		 String fileName= request.getParameter( "fileName" );
		 String realFileName = request.getParameter( "realFileName" );
		 System.out.println( "filePath :" +filePath);
		 System.out.println( "fileName :"+ fileName);
		 System.out.println( "realFileName :"+ realFileName);
			Map map = new HashMap();

			
	
	      System.out.println( "DownLoadPath1  : "+ DownLoadPath );
	      map.put("path", DownLoadPath);
	      map.put( "fileName" , fileName );
	      map.put("filePath",filePath);
	      map.put("realFileName",realFileName);
	      return new ModelAndView("downloadView",map);
	}
	
	public ModelAndView fileDownLoad2(HttpServletRequest request,  HttpServletResponse respone) throws Exception {
	     String filePath= request.getParameter( "filePath" );
		 String fileName= request.getParameter( "fileName" );
		 String realFileName = request.getParameter( "realFileName" );
		 System.out.println( "filePath :" +filePath);
		 System.out.println( "fileName :"+ fileName);
		 System.out.println( "realFileName :"+ realFileName);
			Map map = new HashMap();
	      
	      System.out.println( "DownLoadPath  : "+ DownLoadPath );
	      map.put("path", filePath);
	      map.put("fileName", fileName);
	      map.put("realFileName", realFileName);
	      return new ModelAndView("downloadView",map);
	}
	
	public ModelAndView goQust(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		/*return new ModelAndView("qust/qustAnswQutnRegi");*/
		return new ModelAndView("qust/NewqustAnswQutnModi");
	}
	
	public ModelAndView goAnswer(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		return new ModelAndView("qust/qustAnswRegi");
	}

	public ModelAndView deleteQust(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		Board board = new Board();
		
		board.setBordSeqn(ServletRequestUtils.getRequiredLongParameter(request, "bordSeqn"));
		board.setMenuSeqn(ServletRequestUtils.getRequiredLongParameter(request, "menuSeqn"));
		board.setThreaded(ServletRequestUtils.getRequiredLongParameter(request, "threaded"));
		board.setDeth(ServletRequestUtils.getRequiredIntParameter(request, "deth"));
		
		boardService.deleteQust(board);
		
		return new ModelAndView("qust/qustDeltSucc");
	}
	
	// 커뮤니티 게시판 추가(최남식)
	public ModelAndView goComm(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		return new ModelAndView("comm/commRegi");
	}
	
	public ModelAndView insertComm(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Board board = new Board();
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request); 
		
		board.setBordSeqn( StringUtil.nullToZero( multipartRequest.getParameter("bordSeqn")) );
		board.setThreaded( StringUtil.nullToZero(multipartRequest.getParameter("threaded")) );
		board.setMenuSeqn( StringUtil.nullToZero(multipartRequest.getParameter("menuSeqn")) );
		board.setModiIdnt(multipartRequest.getParameter("modiIdnt"));
		board.setTite(multipartRequest.getParameter("tite"));
		board.setRgstIdnt(multipartRequest.getParameter("rgstIdnt"));
		board.setPswd(multipartRequest.getParameter("pswd"));
		board.setBordDesc(multipartRequest.getParameter("bordDesc"));	
		board.setInqrCont(0);
		board.setMail(multipartRequest.getParameter("mail"));
		board.setDeth(0);
		board.setHtmlYsno(StringUtil.nullToEmptyChk(multipartRequest.getParameter("htmlYsno")) );
		board.setFileName(multipartRequest.getParameter("fileName"));
		board.setFilePath(multipartRequest.getParameter("filePath"));
		board.setFileSize(multipartRequest.getParameter("fileSize"));	
		board.setSrchDivs(multipartRequest.getParameter("srchDivs"));
		board.setSrchText(StringUtil.nullToEmpty(multipartRequest.getParameter("srchText")) );
		board.setPage_no(multipartRequest.getParameter("page_no") == null ? "1" : multipartRequest.getParameter("page_no") );

//		 File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List fileList = new ArrayList();
		
		while(fileNameIterator.hasNext()) {
			MultipartFile multiFile= multipartRequest.getFile((String)fileNameIterator.next());
			if(multiFile.getSize()>0) {
				BoardFile boardFile = FileUploadUtil.uploadFormFiles(multiFile, realUploadPath);
				fileList.add(boardFile);
			}
		}
		
		board.setFileList(fileList);
		
		boardService.insertQust(board);
		
		return new ModelAndView("comm/commRegiSucc");
	}
	
	public ModelAndView updateComm(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Board board = new Board();
		  
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request); 
		
		board.setBordSeqn( StringUtil.nullToZero( multipartRequest.getParameter("bordSeqn")) );
		board.setThreaded( StringUtil.nullToZero(multipartRequest.getParameter("threaded")) );
		board.setMenuSeqn( StringUtil.nullToZero(multipartRequest.getParameter("menuSeqn")) );
		board.setModiIdnt(multipartRequest.getParameter("rgstIdnt"));
		board.setTite(multipartRequest.getParameter("tite"));
		board.setRgstIdnt(multipartRequest.getParameter("rgstIdnt"));
		board.setBordDesc(multipartRequest.getParameter("bordDesc"));	
		board.setMail(multipartRequest.getParameter("mail"));
		board.setHtmlYsno(StringUtil.nullToEmptyChk(multipartRequest.getParameter("htmlYsno")) );
		board.setFileName(multipartRequest.getParameter("fileName"));
		board.setFilePath(multipartRequest.getParameter("filePath"));
		board.setFileSize(multipartRequest.getParameter("fileSize"));	
		board.setSrchDivs(multipartRequest.getParameter("srchDivs"));
		board.setSrchText(StringUtil.nullToEmpty(multipartRequest.getParameter("srchText")) );
		board.setPage_no(multipartRequest.getParameter("page_no") == null ? "1" : multipartRequest.getParameter("page_no") );
		 
		String[] chk;
		chk = multipartRequest.getParameterValues("chk"); 
        
		if(chk != null){
			boardService.fileDelete(chk); 
		}

//		 File Upload
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List fileList = new ArrayList();
		
		while(fileNameIterator.hasNext()) {
			MultipartFile multiFile= multipartRequest.getFile((String)fileNameIterator.next());
			if(multiFile.getSize()>0) {
				BoardFile boardFile = FileUploadUtil.uploadFormFiles(multiFile, realUploadPath);
				fileList.add(boardFile);
			}
		}
		
		board.setFileList(fileList);
		boardService.updateQust(board);
		
		return new ModelAndView("comm/commModiSucc");
	}
	
	public ModelAndView deleteComm(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		Board board = new Board();
		
		board.setBordSeqn(ServletRequestUtils.getRequiredLongParameter(request, "bordSeqn"));
		board.setMenuSeqn(ServletRequestUtils.getRequiredLongParameter(request, "menuSeqn"));
		board.setThreaded(ServletRequestUtils.getRequiredLongParameter(request, "threaded"));
		board.setDeth(ServletRequestUtils.getRequiredIntParameter(request, "deth"));
		
		boardService.deleteQust(board);
		
		return new ModelAndView("comm/commDeltSucc");
	}
	
	public ModelAndView vocList(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
	
		return new ModelAndView("voc/vocList");
	}
}
