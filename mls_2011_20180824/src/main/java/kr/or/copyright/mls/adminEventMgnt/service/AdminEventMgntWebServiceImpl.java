package kr.or.copyright.mls.adminEventMgnt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminEventMgnt.dao.AdminEventMgntWebDao;
import kr.or.copyright.mls.adminEventMgnt.model.MlEventItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdminEventMgntWebServiceImpl implements AdminEventMgntWebService{

	@Autowired
	private AdminEventMgntWebDao adminEventMgntWebDao;
	
	public Map getMultiQustItemMgntSeq(Map map) {
		return adminEventMgntWebDao.getMultiQustItemMgntSeq(map);
	}

	public void addMultiQustItemMgnt(Map ml_event_item) {
		adminEventMgntWebDao.addMultiQustItemMgnt(ml_event_item);
	}

	@Transactional
	public void addMultiQustItemMgntChoice(ArrayList list) {
		for (int i = 0; i < list.size(); i++) {
			Map map = (Map) list.get(i);
			adminEventMgntWebDao.addMultiQustItemMgntChoice(map);
		}
	}

	public List getEventItemList(MlEventItem vo) {
		ArrayList<MlEventItem> list = (ArrayList<MlEventItem>) adminEventMgntWebDao.getEventItemList(vo);//문항
		for (int i = 0; i < list.size(); i++) {
			list.get(i).getItem_type_cd();
			if ("3".equals(list.get(i).getItem_type_cd())) {//객관식인것만
				list.get(i).setListSub(adminEventMgntWebDao.getEventItemChoiceList(list.get(i)));//항목
			}
		}
		return list;
	}

	@Transactional
	public void addEventCorans(MlEventItem vo) throws Exception {
		adminEventMgntWebDao.delEventCorans(vo);//답변삭제
		//텍스트 답변 인서트
		if (vo.getAction1() != null) {
			for (int i = 0; i < vo.getAction1().size(); i++) {
				HashMap ma1 = (HashMap)vo.getAction1().get(i);
					adminEventMgntWebDao.addEventCorans((Map)vo.getAction1().get(i));
			}
		}
		
		adminEventMgntWebDao.uptCoransSetN(vo);
		//항목 답변 업데이트
		if (vo.getAction2() != null) {
			for (int i = 0; i < vo.getAction2().size(); i++) {
				Map m2 =(Map)vo.getAction2().get(i);
					adminEventMgntWebDao.uptCoransSetY((Map)vo.getAction2().get(i));
			}
		}
	}

	@Transactional
	public void delMultiQustItemMgnt(MlEventItem vo) {
		adminEventMgntWebDao.delMlEventCorans(vo);
		adminEventMgntWebDao.delMlEventItemChoice(vo);
		adminEventMgntWebDao.delMlEventItem(vo);
	}

	
	/* 항목순서셋팅
	 * @see kr.or.copyright.mls.adminEventMgnt.service.AdminEventMgntWebService#uptEventItemSeqn(kr.or.copyright.mls.adminEventMgnt.model.MlEventItem)
	 */
	@Transactional
	public void uptEventItemSeqn(MlEventItem vo) {
		for (int i = 0; i < vo.getItem_id_arr().length; i++) {
			MlEventItem vv = new MlEventItem();
			vv.setEvent_id(vo.getEvent_id());
			vv.setItem_id(vo.getItem_id_arr()[i]);
			vv.setItem_seqn((i+1)+"");
			adminEventMgntWebDao.uptEventItemSeqn(vv);
		}
	}

	public MlEventItem uptMultiQustItemMgntView(MlEventItem vo) {
		MlEventItem eventDetlVo = adminEventMgntWebDao.getEventItemDetl(vo);
		eventDetlVo.setListSub(adminEventMgntWebDao.getEventItemChoiceList(eventDetlVo));
		return eventDetlVo;
	}

	@Transactional
	public void uptMultiQustItemMgnt(MlEventItem vo) {
		adminEventMgntWebDao.delMlEventCorans(vo);
		adminEventMgntWebDao.delMlEventItemChoice(vo);
		adminEventMgntWebDao.uptMultiQustItemMgnt(vo);
		
		
		if(vo.getItem_choice_desc_arr() != null){
			for (int i = 0; i < vo.getItem_choice_desc_arr().length; i++) {
				HashMap<String, String> hm = new HashMap<String, String>();
				hm.put("EVENT_ID", vo.getEvent_id());
				hm.put("ITEM_ID", vo.getItem_id());
				hm.put("ITEM_CHOICE_DESC", vo.getItem_choice_desc_arr()[i]);
				hm.put("ETC_YN", vo.getItem_choice_etc_yn_arr()[i]);
				adminEventMgntWebDao.addMultiQustItemMgntChoice(hm);
			}
		}
		
	}

	public String getEventTitle(MlEventItem vo) {
		return adminEventMgntWebDao.getEventTitle(vo);
	}
}
