package kr.or.copyright.mls.event.model;

import java.util.Arrays;
import java.util.List;

import kr.or.copyright.mls.common.common.utils.Constants;

public class EventMgntVO {

	private String
	rnum,
	event_title,
	item_id,
	item_seqn,
	item_type_cd,
	item_desc,
	max_choice_cnt,
	rgst_idnt,
	rgst_dttm,
	event_id,
	item_choice_id,
	item_choice_seqn,
	item_choice_desc,
	etc_yn,
	user_id,
	corans_yn,
	rslt_desc,
	opening_yn
	,oper_yn
	,start_dttm_oper
	,end_dttm_oper
	,start_dttm_win_anuc
	,end_dttm_win_anuc
	,prop_desc
	,start_dttm_part
	,end_dttm_part
	,win_anuc_date
	,win_anuc_desc
	,win_cnt
	,image_file_name
	,image_file_path
	,event_type_cd
	,part_dupl_yn
	,modi_idnt
	,modi_dttm
	,delt_yn
	,open_yn_prop
	,open_yn_dttm_part
	,open_yn_win_anuc_date
	,open_yn_win_anuc_desc
	,open_yn_win_cnt
	,open_yn_image
	,image_file_name_win
	,image_file_path_win
	,name
	,user_idnt
	,part_cnt
	,sex
	,birth_dtae
	,mobl_phon
	,mail
	,part_dttm
	,win_yn
	,win_dttm
	,rslt_seq
	,event_item_choice_id
	,rslt_file_name
	,rslt_file_path
	,opening_win_yn
	,type
	;
	
	List listSub, action1, action2, list;
	
	String[] item_id_arr,item_type_cd_arr,item_choice_desc_arr,item_choice_etc_yn_arr;
	
	//	����¡
	private int nowPage = 1;  
	private int startRow = 1; 
	private int endRow = 10; 
	private int totalRow = 0;
	private int rowNo;


	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOpening_win_yn() {
		return opening_win_yn;
	}

	public void setOpening_win_yn(String opening_win_yn) {
		this.opening_win_yn = opening_win_yn;
	}

	public String getRslt_seq() {
		return rslt_seq;
	}

	public void setRslt_seq(String rslt_seq) {
		this.rslt_seq = rslt_seq;
	}

	public String getEvent_item_choice_id() {
		return event_item_choice_id;
	}

	public void setEvent_item_choice_id(String event_item_choice_id) {
		this.event_item_choice_id = event_item_choice_id;
	}

	public String getRslt_file_name() {
		return rslt_file_name;
	}

	public void setRslt_file_name(String rslt_file_name) {
		this.rslt_file_name = rslt_file_name;
	}

	public String getRslt_file_path() {
		return rslt_file_path;
	}

	public void setRslt_file_path(String rslt_file_path) {
		this.rslt_file_path = rslt_file_path;
	}

	public String getPart_cnt() {
		return part_cnt;
	}

	public void setPart_cnt(String part_cnt) {
		this.part_cnt = part_cnt;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirth_dtae() {
		return birth_dtae;
	}

	public void setBirth_dtae(String birth_dtae) {
		this.birth_dtae = birth_dtae;
	}

	public String getMobl_phon() {
		return mobl_phon;
	}

	public void setMobl_phon(String mobl_phon) {
		this.mobl_phon = mobl_phon;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPart_dttm() {
		return part_dttm;
	}

	public void setPart_dttm(String part_dttm) {
		this.part_dttm = part_dttm;
	}

	public String getWin_yn() {
		return win_yn;
	}

	public void setWin_yn(String win_yn) {
		this.win_yn = win_yn;
	}

	public String getWin_dttm() {
		return win_dttm;
	}

	public void setWin_dttm(String win_dttm) {
		this.win_dttm = win_dttm;
	}

	public String getUser_idnt() {
		return user_idnt;
	}

	public void setUser_idnt(String user_idnt) {
		this.user_idnt = user_idnt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRnum() {
		return rnum;
	}

	public void setRnum(String rnum) {
		this.rnum = rnum;
	}

	public String getOper_yn() {
		return oper_yn;
	}

	public void setOper_yn(String oper_yn) {
		this.oper_yn = oper_yn;
	}

	public String getStart_dttm_oper() {
		return start_dttm_oper;
	}

	public void setStart_dttm_oper(String start_dttm_oper) {
		this.start_dttm_oper = start_dttm_oper;
	}

	public String getEnd_dttm_oper() {
		return end_dttm_oper;
	}

	public void setEnd_dttm_oper(String end_dttm_oper) {
		this.end_dttm_oper = end_dttm_oper;
	}

	public String getStart_dttm_win_anuc() {
		return start_dttm_win_anuc;
	}

	public void setStart_dttm_win_anuc(String start_dttm_win_anuc) {
		this.start_dttm_win_anuc = start_dttm_win_anuc;
	}

	public String getEnd_dttm_win_anuc() {
		return end_dttm_win_anuc;
	}

	public void setEnd_dttm_win_anuc(String end_dttm_win_anuc) {
		this.end_dttm_win_anuc = end_dttm_win_anuc;
	}

	public String getProp_desc() {
		return prop_desc;
	}

	public void setProp_desc(String prop_desc) {
		this.prop_desc = prop_desc;
	}

	public String getStart_dttm_part() {
		return start_dttm_part;
	}

	public void setStart_dttm_part(String start_dttm_part) {
		this.start_dttm_part = start_dttm_part;
	}

	public String getEnd_dttm_part() {
		return end_dttm_part;
	}

	public void setEnd_dttm_part(String end_dttm_part) {
		this.end_dttm_part = end_dttm_part;
	}

	public String getWin_anuc_date() {
		return win_anuc_date;
	}

	public void setWin_anuc_date(String win_anuc_date) {
		this.win_anuc_date = win_anuc_date;
	}

	public String getWin_anuc_desc() {
		return win_anuc_desc;
	}

	public void setWin_anuc_desc(String win_anuc_desc) {
		this.win_anuc_desc = win_anuc_desc;
	}

	public String getWin_cnt() {
		return win_cnt;
	}

	public void setWin_cnt(String win_cnt) {
		this.win_cnt = win_cnt;
	}

	public String getImage_file_name() {
		return image_file_name;
	}

	public void setImage_file_name(String image_file_name) {
		this.image_file_name = image_file_name;
	}

	public String getImage_file_path() {
		return image_file_path;
	}

	public void setImage_file_path(String image_file_path) {
		this.image_file_path = image_file_path;
	}

	public String getEvent_type_cd() {
		return event_type_cd;
	}

	public void setEvent_type_cd(String event_type_cd) {
		this.event_type_cd = event_type_cd;
	}

	public String getPart_dupl_yn() {
		return part_dupl_yn;
	}

	public void setPart_dupl_yn(String part_dupl_yn) {
		this.part_dupl_yn = part_dupl_yn;
	}

	public String getModi_idnt() {
		return modi_idnt;
	}

	public void setModi_idnt(String modi_idnt) {
		this.modi_idnt = modi_idnt;
	}

	public String getModi_dttm() {
		return modi_dttm;
	}

	public void setModi_dttm(String modi_dttm) {
		this.modi_dttm = modi_dttm;
	}

	public String getDelt_yn() {
		return delt_yn;
	}

	public void setDelt_yn(String delt_yn) {
		this.delt_yn = delt_yn;
	}

	public String getOpen_yn_prop() {
		return open_yn_prop;
	}

	public void setOpen_yn_prop(String open_yn_prop) {
		this.open_yn_prop = open_yn_prop;
	}

	public String getOpen_yn_dttm_part() {
		return open_yn_dttm_part;
	}

	public void setOpen_yn_dttm_part(String open_yn_dttm_part) {
		this.open_yn_dttm_part = open_yn_dttm_part;
	}

	public String getOpen_yn_win_anuc_date() {
		return open_yn_win_anuc_date;
	}

	public void setOpen_yn_win_anuc_date(String open_yn_win_anuc_date) {
		this.open_yn_win_anuc_date = open_yn_win_anuc_date;
	}

	public String getOpen_yn_win_anuc_desc() {
		return open_yn_win_anuc_desc;
	}

	public void setOpen_yn_win_anuc_desc(String open_yn_win_anuc_desc) {
		this.open_yn_win_anuc_desc = open_yn_win_anuc_desc;
	}

	public String getOpen_yn_win_cnt() {
		return open_yn_win_cnt;
	}

	public void setOpen_yn_win_cnt(String open_yn_win_cnt) {
		this.open_yn_win_cnt = open_yn_win_cnt;
	}

	public String getOpen_yn_image() {
		return open_yn_image;
	}

	public void setOpen_yn_image(String open_yn_image) {
		this.open_yn_image = open_yn_image;
	}

	public String getImage_file_name_win() {
		return image_file_name_win;
	}

	public void setImage_file_name_win(String image_file_name_win) {
		this.image_file_name_win = image_file_name_win;
	}

	public String getImage_file_path_win() {
		return image_file_path_win;
	}

	public void setImage_file_path_win(String image_file_path_win) {
		this.image_file_path_win = image_file_path_win;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}

	public String getOpening_yn() {
		return opening_yn;
	}

	public void setOpening_yn(String opening_yn) {
		this.opening_yn = opening_yn;
	}

	public String getEvent_title() {
		return event_title;
	}

	public void setEvent_title(String event_title) {
		this.event_title = event_title;
	}

	public String getItem_id() {
		return item_id;
	}

	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}

	public String getItem_seqn() {
		return item_seqn;
	}

	public void setItem_seqn(String item_seqn) {
		this.item_seqn = item_seqn;
	}

	public String getItem_type_cd() {
		return item_type_cd;
	}

	public void setItem_type_cd(String item_type_cd) {
		this.item_type_cd = item_type_cd;
	}

	public String getItem_desc() {
		return item_desc;
	}

	public void setItem_desc(String item_desc) {
		this.item_desc = item_desc;
	}

	public String getMax_choice_cnt() {
		return max_choice_cnt;
	}

	public void setMax_choice_cnt(String max_choice_cnt) {
		this.max_choice_cnt = max_choice_cnt;
	}

	public String getRgst_idnt() {
		return rgst_idnt;
	}

	public void setRgst_idnt(String rgst_idnt) {
		this.rgst_idnt = rgst_idnt;
	}

	public String getRgst_dttm() {
		return rgst_dttm;
	}

	public void setRgst_dttm(String rgst_dttm) {
		this.rgst_dttm = rgst_dttm;
	}

	public String getEvent_id() {
		return event_id;
	}

	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}

	public String getItem_choice_id() {
		return item_choice_id;
	}

	public void setItem_choice_id(String item_choice_id) {
		this.item_choice_id = item_choice_id;
	}

	public String getItem_choice_seqn() {
		return item_choice_seqn;
	}

	public void setItem_choice_seqn(String item_choice_seqn) {
		this.item_choice_seqn = item_choice_seqn;
	}

	public String getItem_choice_desc() {
		return item_choice_desc;
	}

	public void setItem_choice_desc(String item_choice_desc) {
		this.item_choice_desc = item_choice_desc;
	}

	public String getEtc_yn() {
		return etc_yn;
	}

	public void setEtc_yn(String etc_yn) {
		this.etc_yn = etc_yn;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getCorans_yn() {
		return corans_yn;
	}

	public void setCorans_yn(String corans_yn) {
		this.corans_yn = corans_yn;
	}

	public String getRslt_desc() {
		return rslt_desc;
	}

	public void setRslt_desc(String rslt_desc) {
		this.rslt_desc = rslt_desc;
	}

	public List getListSub() {
		return listSub;
	}

	public void setListSub(List listSub) {
		this.listSub = listSub;
	}

	public List getAction1() {
		return action1;
	}

	public void setAction1(List action1) {
		this.action1 = action1;
	}

	public List getAction2() {
		return action2;
	}

	public void setAction2(List action2) {
		this.action2 = action2;
	}

	public String[] getItem_id_arr() {
		return item_id_arr;
	}

	public void setItem_id_arr(String[] item_id_arr) {
		this.item_id_arr = item_id_arr;
	}

	public String[] getItem_type_cd_arr() {
		return item_type_cd_arr;
	}

	public void setItem_type_cd_arr(String[] item_type_cd_arr) {
		this.item_type_cd_arr = item_type_cd_arr;
	}

	public String[] getItem_choice_desc_arr() {
		return item_choice_desc_arr;
	}

	public void setItem_choice_desc_arr(String[] item_choice_desc_arr) {
		this.item_choice_desc_arr = item_choice_desc_arr;
	}

	public String[] getItem_choice_etc_yn_arr() {
		return item_choice_etc_yn_arr;
	}

	public void setItem_choice_etc_yn_arr(String[] item_choice_etc_yn_arr) {
		this.item_choice_etc_yn_arr = item_choice_etc_yn_arr;
	}

	public int getNowPage() {
		return nowPage;
	}

	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
		this.startRow = Constants.DEFAULT_ROW_PER_PAGE * (nowPage - 1) + 1;
		this.endRow = Constants.DEFAULT_ROW_PER_PAGE * nowPage;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public int getTotalRow() {
		return totalRow;
	}

	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}

	public int getRowNo() {
		return rowNo;
	}

	public void setRowNo(int rowNo) {
		this.rowNo = rowNo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EventMgntVO [rnum=");
		builder.append(rnum);
		builder.append(", event_title=");
		builder.append(event_title);
		builder.append(", item_id=");
		builder.append(item_id);
		builder.append(", item_seqn=");
		builder.append(item_seqn);
		builder.append(", item_type_cd=");
		builder.append(item_type_cd);
		builder.append(", item_desc=");
		builder.append(item_desc);
		builder.append(", max_choice_cnt=");
		builder.append(max_choice_cnt);
		builder.append(", rgst_idnt=");
		builder.append(rgst_idnt);
		builder.append(", rgst_dttm=");
		builder.append(rgst_dttm);
		builder.append(", event_id=");
		builder.append(event_id);
		builder.append(", item_choice_id=");
		builder.append(item_choice_id);
		builder.append(", item_choice_seqn=");
		builder.append(item_choice_seqn);
		builder.append(", item_choice_desc=");
		builder.append(item_choice_desc);
		builder.append(", etc_yn=");
		builder.append(etc_yn);
		builder.append(", user_id=");
		builder.append(user_id);
		builder.append(", corans_yn=");
		builder.append(corans_yn);
		builder.append(", rslt_desc=");
		builder.append(rslt_desc);
		builder.append(", opening_yn=");
		builder.append(opening_yn);
		builder.append(", oper_yn=");
		builder.append(oper_yn);
		builder.append(", start_dttm_oper=");
		builder.append(start_dttm_oper);
		builder.append(", end_dttm_oper=");
		builder.append(end_dttm_oper);
		builder.append(", start_dttm_win_anuc=");
		builder.append(start_dttm_win_anuc);
		builder.append(", end_dttm_win_anuc=");
		builder.append(end_dttm_win_anuc);
		builder.append(", prop_desc=");
		builder.append(prop_desc);
		builder.append(", start_dttm_part=");
		builder.append(start_dttm_part);
		builder.append(", end_dttm_part=");
		builder.append(end_dttm_part);
		builder.append(", win_anuc_date=");
		builder.append(win_anuc_date);
		builder.append(", win_anuc_desc=");
		builder.append(win_anuc_desc);
		builder.append(", win_cnt=");
		builder.append(win_cnt);
		builder.append(", image_file_name=");
		builder.append(image_file_name);
		builder.append(", image_file_path=");
		builder.append(image_file_path);
		builder.append(", event_type_cd=");
		builder.append(event_type_cd);
		builder.append(", part_dupl_yn=");
		builder.append(part_dupl_yn);
		builder.append(", modi_idnt=");
		builder.append(modi_idnt);
		builder.append(", modi_dttm=");
		builder.append(modi_dttm);
		builder.append(", delt_yn=");
		builder.append(delt_yn);
		builder.append(", open_yn_prop=");
		builder.append(open_yn_prop);
		builder.append(", open_yn_dttm_part=");
		builder.append(open_yn_dttm_part);
		builder.append(", open_yn_win_anuc_date=");
		builder.append(open_yn_win_anuc_date);
		builder.append(", open_yn_win_anuc_desc=");
		builder.append(open_yn_win_anuc_desc);
		builder.append(", open_yn_win_cnt=");
		builder.append(open_yn_win_cnt);
		builder.append(", open_yn_image=");
		builder.append(open_yn_image);
		builder.append(", image_file_name_win=");
		builder.append(image_file_name_win);
		builder.append(", image_file_path_win=");
		builder.append(image_file_path_win);
		builder.append(", name=");
		builder.append(name);
		builder.append(", user_idnt=");
		builder.append(user_idnt);
		builder.append(", part_cnt=");
		builder.append(part_cnt);
		builder.append(", sex=");
		builder.append(sex);
		builder.append(", birth_dtae=");
		builder.append(birth_dtae);
		builder.append(", mobl_phon=");
		builder.append(mobl_phon);
		builder.append(", mail=");
		builder.append(mail);
		builder.append(", part_dttm=");
		builder.append(part_dttm);
		builder.append(", win_yn=");
		builder.append(win_yn);
		builder.append(", win_dttm=");
		builder.append(win_dttm);
		builder.append(", rslt_seq=");
		builder.append(rslt_seq);
		builder.append(", event_item_choice_id=");
		builder.append(event_item_choice_id);
		builder.append(", rslt_file_name=");
		builder.append(rslt_file_name);
		builder.append(", rslt_file_path=");
		builder.append(rslt_file_path);
		builder.append(", opening_win_yn=");
		builder.append(opening_win_yn);
		builder.append(", listSub=");
		builder.append(listSub);
		builder.append(", action1=");
		builder.append(action1);
		builder.append(", action2=");
		builder.append(action2);
		builder.append(", list=");
		builder.append(list);
		builder.append(", item_id_arr=");
		builder.append(Arrays.toString(item_id_arr));
		builder.append(", item_type_cd_arr=");
		builder.append(Arrays.toString(item_type_cd_arr));
		builder.append(", item_choice_desc_arr=");
		builder.append(Arrays.toString(item_choice_desc_arr));
		builder.append(", item_choice_etc_yn_arr=");
		builder.append(Arrays.toString(item_choice_etc_yn_arr));
		builder.append(", nowPage=");
		builder.append(nowPage);
		builder.append(", startRow=");
		builder.append(startRow);
		builder.append(", endRow=");
		builder.append(endRow);
		builder.append(", totalRow=");
		builder.append(totalRow);
		builder.append(", rowNo=");
		builder.append(rowNo);
		builder.append("]");
		return builder.toString();
	}
}
