package kr.or.copyright.mls.console.rwmnyreqst.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd36Service{

	/**
	 * 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd36List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상세 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd36View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 신청인 정보 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd36View2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 처리결과 상세정보 팝업
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd36Pop1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 신청내역 삭제
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd36Delete1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 처리결과 및 신청정보 저장
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd36Insert1( Map<String, Object> commandMap ) throws Exception;

}
