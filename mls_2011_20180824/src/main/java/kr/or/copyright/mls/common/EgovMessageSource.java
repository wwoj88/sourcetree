package kr.or.copyright.mls.common;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * λ©μμ§? λ¦¬μ?€ ?¬?©? ?? MessageSource ?Έ?°??΄?€ λ°? ReloadableResourceBundleMessageSource ?΄??€? κ΅¬νμ²?
 * @author κ³΅ν΅?λΉμ€ κ°λ°??? ?΄λ¬Έμ??
 * @since 2009.06.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << κ°μ ?΄? ₯(Modification Information) >>
 *   
 *   ?? ?Ό      ?? ?           ?? ?΄?©
 *  -------    --------    ---------------------------
 *   2009.03.11  ?΄λ¬Έμ??          μ΅μ΄ ??±
 *
 * </pre>
 */

public class EgovMessageSource extends ReloadableResourceBundleMessageSource implements MessageSource {

	private ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource;

	/**
	 * getReloadableResourceBundleMessageSource() 
	 * @param reloadableResourceBundleMessageSource - resource MessageSource
	 * @return ReloadableResourceBundleMessageSource
	 */	
	public void setReloadableResourceBundleMessageSource(ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource) {
		this.reloadableResourceBundleMessageSource = reloadableResourceBundleMessageSource;
	}
	
	/**
	 * getReloadableResourceBundleMessageSource() 
	 * @return ReloadableResourceBundleMessageSource
	 */	
	public ReloadableResourceBundleMessageSource getReloadableResourceBundleMessageSource() {
		return reloadableResourceBundleMessageSource;
	}
	
	/**
	 * ? ?? λ©μΈμ§? μ‘°ν
	 * @param code - λ©μΈμ§? μ½λ
	 * @return String
	 */	
	public String getMessage(String code) {
		return getReloadableResourceBundleMessageSource().getMessage(code, null, Locale.getDefault());
	}

}
