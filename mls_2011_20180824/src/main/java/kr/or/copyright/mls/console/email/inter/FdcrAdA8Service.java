package kr.or.copyright.mls.console.email.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAdA8Service{

	/**
	 * 월별 자동메일 발송 관리 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA8List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 발신내역 수신확인 팝업
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA8Pop1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 발송 관리 저장
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAdA8Insert1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 메일 주소록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA8List2( Map<String, Object> commandMap ) throws Exception;

}
