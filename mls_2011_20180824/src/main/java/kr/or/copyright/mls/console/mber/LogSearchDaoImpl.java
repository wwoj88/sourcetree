package kr.or.copyright.mls.console.mber;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import kr.or.copyright.mls.console.EgovComAbstractDAO;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Dao;
import kr.or.copyright.mls.console.mber.inter.LogSearchDao;
@Repository( "logSearchDao" )
public class LogSearchDaoImpl extends EgovComAbstractDAO implements LogSearchDao {

	// 회원정보 목록조회
		public List logList(Map map) {
			return getSqlMapClientTemplate().queryForList("AdminUser.logList",map);
		}
		
		/**
		 * 회원정보 카운트
		 * @return
		 * @throws SQLException
		 */
		public int logListCount( Map<String, Object> commandMap ) throws SQLException{
			return (Integer) selectByPk( "AdminUser.logListCount", commandMap );
		}

	

}
