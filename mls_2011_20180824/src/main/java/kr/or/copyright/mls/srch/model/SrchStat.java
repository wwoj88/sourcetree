package kr.or.copyright.mls.srch.model;

import java.util.List;

public class SrchStat {
	private int midCode;		//ML_CODE 
	private String codeName;	//ML_CODE
	private int worksId;
	private int worksDivsCd;
	private String worksTitle;
	private int pageNo;
	private int startNo;
	private int endNo;
	
	private String liShComp;	//출판사(교과용, 도서관)
	private String coptHodr;	//작가, 저자 (교과용, 도서관, 거소불명)
	private String workName;	//도서관보상금 제목
	//---------방송보상금-------------
	private String albmName;	//앨범명
	private String albmProd;	//음반제작자
	private String muciName;	//가수명
	private String perfName;	//연주자
	private String sdsrName;	//제목
	//---------교과용보상금-------------
	private String bookDivs;	//교과목
	private String subjName;	//제목
	//---------거소불명-------------
	private String anucItem6;	//발행자
	private String anucItem7;	//공표일자
	private String genreCdName; //장르 이름
	//---------기승인 저작물-------
	private String worksForm;
	private String coptHodrName;
	private String confDate;
	
	private List findList;		//검색된 값을 담는 리스트
	private List coptHodrList;	//거소불명 coptHodr 담는 리스트
	public int getMidCode() {
		return midCode;
	}
	public void setMidCode(int midCode) {
		this.midCode = midCode;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public int getWorksId() {
		return worksId;
	}
	public void setWorksId(int worksId) {
		this.worksId = worksId;
	}
	public int getWorksDivsCd() {
		return worksDivsCd;
	}
	public void setWorksDivsCd(int worksDivsCd) {
		this.worksDivsCd = worksDivsCd;
	}
	public String getWorksTitle() {
		return worksTitle;
	}
	public void setWorksTitle(String worksTitle) {
		this.worksTitle = worksTitle;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getStartNo() {
		return startNo;
	}
	public void setStartNo(int startNo) {
		this.startNo = startNo;
	}
	public int getEndNo() {
		return endNo;
	}
	public void setEndNo(int endNo) {
		this.endNo = endNo;
	}
	public String getLiShComp() {
		return liShComp;
	}
	public void setLiShComp(String liShComp) {
		this.liShComp = liShComp;
	}
	public String getCoptHodr() {
		return coptHodr;
	}
	public void setCoptHodr(String coptHodr) {
		this.coptHodr = coptHodr;
	}
	public String getWorkName() {
		return workName;
	}
	public void setWorkName(String workName) {
		this.workName = workName;
	}
	public String getAlbmName() {
		return albmName;
	}
	public void setAlbmName(String albmName) {
		this.albmName = albmName;
	}
	public String getAlbmProd() {
		return albmProd;
	}
	public void setAlbmProd(String albmProd) {
		this.albmProd = albmProd;
	}
	public String getMuciName() {
		return muciName;
	}
	public void setMuciName(String muciName) {
		this.muciName = muciName;
	}
	public String getPerfName() {
		return perfName;
	}
	public void setPerfName(String perfName) {
		this.perfName = perfName;
	}
	public String getSdsrName() {
		return sdsrName;
	}
	public void setSdsrName(String sdsrName) {
		this.sdsrName = sdsrName;
	}
	public String getBookDivs() {
		return bookDivs;
	}
	public void setBookDivs(String bookDivs) {
		this.bookDivs = bookDivs;
	}
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public String getAnucItem6() {
		return anucItem6;
	}
	public void setAnucItem6(String anucItem6) {
		this.anucItem6 = anucItem6;
	}
	public String getAnucItem7() {
		return anucItem7;
	}
	public void setAnucItem7(String anucItem7) {
		this.anucItem7 = anucItem7;
	}
	public String getGenreCdName() {
		return genreCdName;
	}
	public void setGenreCdName(String genreCdName) {
		this.genreCdName = genreCdName;
	}
	public String getWorksForm() {
		return worksForm;
	}
	public void setWorksForm(String worksForm) {
		this.worksForm = worksForm;
	}
	public String getCoptHodrName() {
		return coptHodrName;
	}
	public void setCoptHodrName(String coptHodrName) {
		this.coptHodrName = coptHodrName;
	}
	public String getConfDate() {
		return confDate;
	}
	public void setConfDate(String confDate) {
		this.confDate = confDate;
	}
	public List getFindList() {
		return findList;
	}
	public void setFindList(List findList) {
		this.findList = findList;
	}
	public List getCoptHodrList() {
		return coptHodrList;
	}
	public void setCoptHodrList(List coptHodrList) {
		this.coptHodrList = coptHodrList;
	}
	
	}
