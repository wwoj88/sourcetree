package kr.or.copyright.mls.adminWorksMgnt.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminWorksMgnt.dao.AdminWorksMgntDao;
import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.NumberUtil;

import com.tobesoft.platform.data.Dataset;

public class AdminWorksMgntServiceImpl extends BaseService implements AdminWorksMgntService {

    private AdminWorksMgntDao adminWorksMgntDao;
    
    public void setAdminWorksMgntDao(AdminWorksMgntDao adminWorksMgntDao) {
        this.adminWorksMgntDao = adminWorksMgntDao;
    }
    

/*    public void worksMgntInsert() throws Exception {
    	
		Dataset ds_report = getDataset("ds_report");
		Dataset ds_condition = getDataset("ds_condition");
		
		List returnList = null;
		
		Map map = getMap(ds_condition, 0);
		
		if(map.get("GENRE_CODE").equals("1")){//음악
		    Dataset ds_excel = getDataset("ds_excel_music");
		    returnList =  adminWorksMgntDao.adminCommMusicInsert(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_music", returnList);
		}else if(map.get("GENRE_CODE").equals("2")){//어문
		    Dataset ds_excel = getDataset("ds_excel_book");
		    returnList =  adminWorksMgntDao.adminCommBookInsert(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_book", returnList);
		}else if(map.get("GENRE_CODE").equals("3")){//방송대본
		    Dataset ds_excel = getDataset("ds_excel_script");
		    returnList =  adminWorksMgntDao.adminCommScriptInsert(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_script", returnList);
		}else if(map.get("GENRE_CODE").equals("4")){//영화
		    Dataset ds_excel = getDataset("ds_excel_movie");
		    returnList =  adminWorksMgntDao.adminCommMovieInsert(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_movie", returnList);
		}else if(map.get("GENRE_CODE").equals("5")){//방송
		    Dataset ds_excel = getDataset("ds_excel_broadcast");
		    returnList =  adminWorksMgntDao.adminCommBroadcastInsert(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_broadcast", returnList);
		}else if(map.get("GENRE_CODE").equals("6")){//뉴스
		    Dataset ds_excel = getDataset("ds_excel_news");
		    returnList =  adminWorksMgntDao.adminCommNewsInsert(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_news", returnList);
		}else if(map.get("GENRE_CODE").equals("7")){//미술
		    Dataset ds_excel = getDataset("ds_excel_art");
		    returnList =  adminWorksMgntDao.adminCommArtInsert(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_art", returnList);
		}else if(map.get("GENRE_CODE").equals("99")){//기타
		    Dataset ds_excel = getDataset("ds_excel_side");
		    returnList =  adminWorksMgntDao.adminCommSideInsert(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_side", returnList);
		}
		
    }*/
    
    public void worksMgntInsert() throws Exception {
    	
		Dataset ds_report = getDataset("ds_report");
		Dataset ds_condition = getDataset("ds_condition");
		Map reportMap = getMap(ds_report, 0);
		Map conditionMap = getMap(ds_condition,0);
		
		List resultList = new ArrayList();
		List errorList = new ArrayList();
		
		int genreCode= Integer.parseInt((conditionMap.get("GENRE_CODE") == null ? "0" : (String)conditionMap.get("GENRE_CODE")));
		
		if (genreCode == 0) {
			throw new Exception("장르코드 NULL : worksMgntInsert");
		}
		
		Dataset ds_excel=null;
		switch (genreCode) {
		case 1: ds_excel = getDataset("ds_excel_music");break;//음악
		case 2: ds_excel = getDataset("ds_excel_book");break;//어문
		case 3: ds_excel = getDataset("ds_excel_script");break;//방송대본
		case 4: ds_excel = getDataset("ds_excel_movie");break;//영화
		case 5: ds_excel = getDataset("ds_excel_broadcast");break;//방송
		case 6: ds_excel = getDataset("ds_excel_news");break;//뉴스
		case 7: ds_excel = getDataset("ds_excel_art");break;//미술
		case 8: ds_excel = getDataset("ds_excel_image");break;//이미지
		case 99: ds_excel = getDataset("ds_excel_side");break;//기타
		default: throw new Exception("장르코드 Error OR Null : worksMgntInsert");
		}
    	
		int rFlag = 0;//리포트FLAG
		boolean cFlag = true;//저작물FLAG
		boolean gFlag = true;//장르별저작물FLAG
		
		
		int cnt = 0;
		
		GregorianCalendar d  = new GregorianCalendar();
		
		//보고연월
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) 
	        + String.valueOf(((d.get(Calendar.MONTH)+1) < 10) ?
	        "0" + String.valueOf(d.get(Calendar.MONTH)+1) :
	        String.valueOf(d.get(Calendar.MONTH)+1));
		
		String reptYmd = reptYmdTmp.replaceAll(" ", ""); 
		
		//장르코드
		String genreCd = (String) conditionMap.get("GENRE_CODE");
		
		//등록자아이디
		String rgstIdnt = (String) conditionMap.get("USER_IDNT");
		
		//위탁관리기관ID
		String trstOrgnCode = (String) conditionMap.get("TRST_ORGN_CODE");
		
		reportMap.put("REPT_YMD", reptYmd);//보고연월
		reportMap.put("GENRE_CD", genreCd);//장르코드
		reportMap.put("RGST_IDNT", rgstIdnt);//등록자아이디
		rFlag = adminWorksMgntDao.adminCommWorksReportInsert(reportMap);
		
		if(rFlag == 0){
		    for(int i=0; i<ds_excel.getRowCount(); i++){
				Map map = null;
				map = getMap(ds_excel, i);
				resultList.add(map);
		    }
		}else{
			ArrayList inList = new ArrayList();
			for(int i=0; i<ds_excel.getRowCount(); i++){
			
				Map map = null;
				try {
				    map = getMap(ds_excel, i);
				    //seq를 사용하여 worksId 생성
				    map.put("TRST_ORGN_CODE", trstOrgnCode);//위탁관리기관아이디
				    map.put("GENRE_CD", genreCd);//장르코드	
				    if(adminWorksMgntDao.getExistCommWorks(map) == 0){
				    	map.put("REPT_YMD", reptYmd);//보고년월
				    	map.put("GENRE_CD", genreCd);//장르코드
				    	map.put("RGST_IDNT", rgstIdnt);//등록자아이디
				    	map.put("WORKS_ID", adminWorksMgntDao.getCommWorksId());//저작물아이디
						inList.add(map);
						cnt++;
				    }else{
				    	resultList.add(map);
				    }
				    
					if ((cnt%100 == 0 || i == ds_excel.getRowCount()-1) && inList.size() > 0) {
						if(adminWorksMgntDao.commWorksInsertBacth(inList)== null){
							try {
								switch (genreCode) {
								case 1: adminWorksMgntDao.commMusicInsertBacth(inList); break;//음악
								case 2: adminWorksMgntDao.commBookInsertBacth(inList); break;//어문
								case 3: adminWorksMgntDao.commScriptInsertBacth(inList); break;//방송대본
								case 4: adminWorksMgntDao.commMovieInsertBacth(inList); break;//영화
								case 5: adminWorksMgntDao.commBroadcastInsertBacth(inList); break;//방송
								case 6: adminWorksMgntDao.commNewsInsertBacth(inList); break;//뉴스
								case 7: adminWorksMgntDao.commArtInsertBacth(inList); break;//미술
								case 8: adminWorksMgntDao.commImageInsertBacth(inList); break;//이미지
								case 99: adminWorksMgntDao.commSideInsertBacth(inList); break;//기타
								default:	break;
								}
							} catch (Exception e) {
								for (int j = 0; j < inList.size(); j++) {
									adminWorksMgntDao.adminCommWorksDelete((Map) inList.get(j));
								}
								errorList.addAll(inList);
								e.printStackTrace();
							}
						}else{
							for (int j = 0; j < inList.size(); j++) {
								adminWorksMgntDao.adminCommWorksDelete((Map) inList.get(j));
							}
							errorList.addAll(inList);
						}
						inList.clear();
					}
				}catch (Exception e) {
					errorList.addAll(inList);
					inList.clear();
				    e.printStackTrace();
				}
		    }
			inList = null;
		}
		
		if(errorList.size() > 0){
			Map map2 = null;
			try {
				for (int j = 0; j < errorList.size(); j++) {
					map2 = (Map) errorList.get(j);
					if(adminWorksMgntDao.adminCommWorksInsert(map2)){
						try {
							boolean bool = false;	
							switch (genreCode) {
							case 1: bool=adminWorksMgntDao.adminCommMusicInsert(map2); break;//음악
							case 2: bool=adminWorksMgntDao.adminCommBookInsert(map2); break;//어문
							case 3: bool=adminWorksMgntDao.adminCommScriptInsert(map2); break;//방송대본
							case 4: bool=adminWorksMgntDao.adminCommMovieInsert(map2); break;//영화
							case 5: bool=adminWorksMgntDao.adminCommBroadcastInsert(map2); break;//방송
							case 6: bool=adminWorksMgntDao.adminCommNewsInsert(map2); break;//뉴스
							case 7: bool=adminWorksMgntDao.adminCommArtInsert(map2); break;//미술
							case 8: bool=adminWorksMgntDao.adminCommImageInsert(map2); break;//이미지
							case 99: bool=adminWorksMgntDao.adminCommSideInsert(map2); break;//기타
							default:	break;
							}
							if (bool) {
								cnt++;
							}else {
								adminWorksMgntDao.adminCommWorksDelete(map2);
								resultList.add(map2);
							}
						}catch (Exception e) {
							adminWorksMgntDao.adminCommWorksDelete(map2);
							resultList.add(map2);
							e.printStackTrace();
						}
					}else{
						resultList.add(map2);
					}
					map2=null;
				}
			} catch (Exception e) {
				if(map2 != null){
					resultList.add(map2);
				}
				System.out.println("등록에러2 : worksMgntInsert()");
				e.printStackTrace();
			}
		}
		
		reportMap.put("REPT_WORKS_CONT", (cnt - errorList.size())) ;
		reportMap.put("REPT_SEQN", rFlag);
		
		if(rFlag != 0){
			adminWorksMgntDao.adminCommWorksReportUpdate(reportMap);
		}
		/*
			메일보내기
		*/
		
		String host = Constants.MAIL_SERVER_IP;	//smtp서버
		String from = Constants.SYSTEM_MAIL_ADDRESS;  //발신인 주소 - 시스템 대표 메일
		String fromName = Constants.SYSTEM_NAME;
		String to = (String) reportMap.get("REPT_CHRR_MAIL");
		String toName = (String) reportMap.get("REPT_CHRR_NAME");
		
		
		String subject = "";	//메일 제목
		
		MailInfo info = new MailInfo();
		
	    info.setFrom(from);
	    info.setFromName(fromName);
	    info.setHost(host);
	    info.setEmail(to);
	    info.setEmailName(toName);

	    
		if(to.length() > 0) {
			//완료시간	  	
			Calendar cal = Calendar.getInstance( );
		    SimpleDateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd aa hh:mm:ss");
		    String rgstDttm = sdf.format(cal.getTime()); 

			
		    
		    // 기관명
			String commName = (String) reportMap.get("COMM_NAME");
			
			//사용자 정보 셋팅 
/*			String userIdnt = (String) userMap.get("USER_IDNT");
			String userName = (String) userMap.get("USER_NAME");
			String userEmail = (String) userMap.get("U_MAIL");
			String userSms = (String) userMap.get("MOBL_PHON");
*/
			//총 등록(또는 수정)건 수
			String reptWorksCont = NumberUtil.getCommaNumber((cnt - errorList.size()));
			String mailTitlDivs = "등록";
			String mailTitl = "[저작권찾기] 위탁관리 저작물 보고";
			
			subject = mailTitl+" "+mailTitlDivs+"이 완료되었습니다.";
		    
		    StringBuffer sb = new StringBuffer();   
			sb.append("				<tr>");
			sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">");
			sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 보고 정보</p>");
			sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
			sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 보고기관 : "+commName+"</li>");
			sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">"+mailTitlDivs+" 완료 일자 : "+rgstDttm+"</li>");
			sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">"+mailTitlDivs+" 보고 건수 : "+reptWorksCont+" 건 </li>");				
			//sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 담당자 정보 : "+userName+" (email:"+ userEmail +", mobile: "+userSms+")</li>");		
			sb.append("					</ul>");
			sb.append("					</td>");
			sb.append("				</tr>");

		    info.setMessage(sb.toString());
		    info.setSubject(subject);
			info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));				    
		}
		MailManager manager = MailManager.getInstance();
		
	    info = manager.sendMail(info);
	    	
	    if (info.isSuccess()) {
		System.out.println(">>>>>>>>>>>> 1. message success :"
			+ info.getEmail());
	    } else {
		System.out.println(">>>>>>>>>>>> 1. message false   :"
			+ info.getEmail());
	    }
		
		switch (genreCode) {
		case 1: addList("ds_excel_music", resultList);break;//음악
		case 2: addList("ds_excel_book", resultList);break;//어문
		case 3: addList("ds_excel_script", resultList);break;//방송대본
		case 4: addList("ds_excel_movie", resultList);break;//영화
		case 5: addList("ds_excel_broadcast", resultList);break;//방송
		case 6: addList("ds_excel_news", resultList);break;//뉴스
		case 7: addList("ds_excel_art", resultList);break;//미술
		case 8: addList("ds_excel_image", resultList);break;//이미지
		case 99: addList("ds_excel_side", resultList);break;//기타
		default: throw new Exception("장르코드 Error OR Null : worksMgntInsert");
		}
    	
		resultList = null;	
    }

    public void worksMgntUpdate() throws Exception {
		Dataset ds_report = getDataset("ds_report");
		Dataset ds_condition = getDataset("ds_condition");
		
		List returnList = null;
		
		Map map = getMap(ds_condition, 0);
		
		int cnt = 0;
		if(map.get("GENRE_CODE").equals("1")){//어문 OR 도서
		    Dataset ds_excel = getDataset("ds_excel_music");
		    cnt=ds_excel.getRowCount();
		    returnList = adminWorksMgntDao.adminCommMusicUpdate(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_music", returnList);		    
		}else if(map.get("GENRE_CODE").equals("2")){//어문
		    Dataset ds_excel = getDataset("ds_excel_book");
		    cnt=ds_excel.getRowCount();
		    returnList =  adminWorksMgntDao.adminCommBookUpdate(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_book", returnList);
		}else if(map.get("GENRE_CODE").equals("3")){//방송대본
		    Dataset ds_excel = getDataset("ds_excel_script");
		    cnt=ds_excel.getRowCount();
		    returnList =  adminWorksMgntDao.adminCommScriptUpdate(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_script", returnList);
		}else if(map.get("GENRE_CODE").equals("4")){//영화
		    Dataset ds_excel = getDataset("ds_excel_movie");
		    cnt=ds_excel.getRowCount();
		    returnList =  adminWorksMgntDao.adminCommMovieUpdate(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_movie", returnList);
		}else if(map.get("GENRE_CODE").equals("5")){//방송
		    Dataset ds_excel = getDataset("ds_excel_broadcast");
		    cnt=ds_excel.getRowCount();
		    returnList =  adminWorksMgntDao.adminCommBroadcastUpdate(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_broadcast", returnList);
		}else if(map.get("GENRE_CODE").equals("6")){//뉴스
		    Dataset ds_excel = getDataset("ds_excel_news");
		    cnt=ds_excel.getRowCount();
		    returnList =  adminWorksMgntDao.adminCommNewsUpdate(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_news", returnList);
		}else if(map.get("GENRE_CODE").equals("7")){//미술
		    Dataset ds_excel = getDataset("ds_excel_art");
		    cnt=ds_excel.getRowCount();
		    returnList =  adminWorksMgntDao.adminCommArtUpdate(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_art", returnList);
		}else if(map.get("GENRE_CODE").equals("8")){//이미지
		    Dataset ds_excel = getDataset("ds_excel_image");
		    cnt=ds_excel.getRowCount();
		    returnList =  adminWorksMgntDao.adminCommImageUpdate(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_image", returnList);
		}else if(map.get("GENRE_CODE").equals("99")){//기타
		    Dataset ds_excel = getDataset("ds_excel_side");
		    cnt=ds_excel.getRowCount();
		    returnList =  adminWorksMgntDao.adminCommSideUpdate(ds_condition, ds_report, ds_excel);
		    addList("ds_excel_side", returnList);
		}
		
		/*
		메일보내기
		 */
		Map reportMap=getMap(ds_report, 0);
		
		String host = Constants.MAIL_SERVER_IP;	//smtp서버
		String from = Constants.SYSTEM_MAIL_ADDRESS;  //발신인 주소 - 시스템 대표 메일
		String fromName = Constants.SYSTEM_NAME;
		String to = (String) reportMap.get("REPT_CHRR_MAIL");
		String toName = (String) reportMap.get("REPT_CHRR_NAME");
		
		
		String subject = "";	//메일 제목
		
		MailInfo info = new MailInfo();
		
	    info.setFrom(from);
	    info.setFromName(fromName);
	    info.setHost(host);
	    info.setEmail(to);
	    info.setEmailName(toName);
	
	    
		if(to.length() > 0) {
			//완료시간	  	
			Calendar cal = Calendar.getInstance( );
		    SimpleDateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd aa hh:mm:ss");
		    String rgstDttm = sdf.format(cal.getTime()); 
	
			
		    
		    // 기관명
			String commName = (String) reportMap.get("COMM_NAME");
			
			//사용자 정보 셋팅 
	/*			String userIdnt = (String) userMap.get("USER_IDNT");
			String userName = (String) userMap.get("USER_NAME");
			String userEmail = (String) userMap.get("U_MAIL");
			String userSms = (String) userMap.get("MOBL_PHON");
	*/
			//총 등록(또는 수정)건 수
			String reptWorksCont = NumberUtil.getCommaNumber((cnt - returnList.size()));
			String mailTitlDivs = "수정";
			String mailTitl = "[저작권찾기] 위탁관리 저작물 보고";
			
			subject = mailTitl+" "+mailTitlDivs+"이 완료되었습니다.";
		    
		    StringBuffer sb = new StringBuffer();   
			sb.append("				<tr>");
			sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">");
			sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 보고 정보</p>");
			sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
			sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 보고기관 : "+commName+"</li>");
			sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">"+mailTitlDivs+" 완료 일자 : "+rgstDttm+"</li>");
			sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">"+mailTitlDivs+" 보고 건수 : "+reptWorksCont+" 건 </li>");				
			//sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 담당자 정보 : "+userName+" (email:"+ userEmail +", mobile: "+userSms+")</li>");		
			sb.append("					</ul>");
			sb.append("					</td>");
			sb.append("				</tr>");
	
		    info.setMessage(sb.toString());
		    info.setSubject(subject);
			info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));				    
		}
		MailManager manager = MailManager.getInstance();
		
	    info = manager.sendMail(info);
	    	
	    if (info.isSuccess()) {
		System.out.println(">>>>>>>>>>>> 1. message success :"
			+ info.getEmail());
	    } else {
		System.out.println(">>>>>>>>>>>> 1. message false   :"
			+ info.getEmail());
	    }
		
		
		
		
		
		
    }

    public void worksMgntList() throws Exception {
    	
		Dataset ds_condition = getDataset("ds_condition");
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		List list = (List)adminWorksMgntDao.worksMgntList(map);
		List pageList = (List)adminWorksMgntDao.totalRowWorksMgntList(map);
		
		addList("ds_list", list);
		addList("ds_page", pageList);
		
    }
    
    //2014-11-26 이병원 보고저작물 조회
    public void worksMgntList_01() throws Exception {
    	
    	Dataset ds_condition = getDataset("ds_condition");
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		int gubun = Integer.parseInt((String)map.get("GENRE_CD"));
		List orgnName = null;
		List list = null;
		List pageList = null;
		
		int gubun2 = gubun;
		
		switch (gubun) {
		
			case 0: map.put("GENRE_CD", "");
				list = (List)adminWorksMgntDao.worksMgntList_01(map);
				break;
			
			case 1: map.put("GENRE_CD", "음악");
				list = (List)adminWorksMgntDao.worksMgntList_01_music(map);
				break;
			
			case 2: map.put("GENRE_CD", "어문");
				list = (List)adminWorksMgntDao.worksMgntList_01_book(map);
				break;
			
			case 3: map.put("GENRE_CD", "방송대본");
				list = (List)adminWorksMgntDao.worksMgntList_01_script(map);
				break;
			
			case 4: map.put("GENRE_CD", "영화");
				list = (List)adminWorksMgntDao.worksMgntList_01_movie(map);
				break;
			
			case 5: map.put("GENRE_CD", "방송");
				list = (List)adminWorksMgntDao.worksMgntList_01_broadcast(map);
				break;
			
			case 6: map.put("GENRE_CD", "뉴스");
				list = (List)adminWorksMgntDao.worksMgntList_01_news(map);
				break;
			
			case 7: map.put("GENRE_CD", "미술");
				list = (List)adminWorksMgntDao.worksMgntList_01_art(map);
				break;
			
			case 8: map.put("GENRE_CD", "이미지");
				list = (List)adminWorksMgntDao.worksMgntList_01_image(map);
				break;
			
			case 9: map.put("GENRE_CD", "기타");
				list = (List)adminWorksMgntDao.worksMgntList_01_side(map);
				break;
			
			default: throw new Exception("장르코드 Error OR Null");
			
		}
		
		pageList = (List)adminWorksMgntDao.totalRowWorksMgntList_01(map);
		orgnName = (List)adminWorksMgntDao.searchOrgnName(map);

		switch (gubun2) {
		
			case 0: addList("ds_list", list);break;
			case 1: addList("ds_list_music", list);break;
			case 2: addList("ds_list_book", list);break;
			case 3: addList("ds_list_script", list);break;
			case 4: addList("ds_list_movie", list);break;
			case 5: addList("ds_list_broadcast", list);break;
			case 6: addList("ds_list_news", list);break;
			case 7: addList("ds_list_art", list);break;
			case 8: addList("ds_list_image", list);break;
			case 9: addList("ds_list_side", list);break;
			default: throw new Exception("장르코드 Error OR Null");
			
		}
		addList("ds_page", pageList);
		addList("ds_orgnname",orgnName);
    }

    public void worksMgntMnthList() throws Exception {
    	
		Dataset ds_condition = getDataset("ds_condition");
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		//월별 보고정보
		List dsReport = (List)adminWorksMgntDao.worksMgntReptInfo(map);
		
		//월별,장르별 통계 정보
		List dsCount = (List)adminWorksMgntDao.worksMgntReptView(map);
		
		//월별 저작물 리스트
		
		addList("ds_count", dsCount);
		addList("ds_report", dsReport);
	    }
	
	public void worksMgntNoDataInsert() throws Exception {
		
		Dataset ds_report = getDataset("ds_report");
		Dataset ds_condition = getDataset("ds_condition");
		
		Map map = getMap(ds_condition, 0);
		
		adminWorksMgntDao.adminWorksMgntNoDataInsert(ds_condition, ds_report);
	
    }

    public void worksMgntReptMonthView() throws Exception {
    	
		Dataset ds_condition = getDataset("ds_condition");
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		List list = (List)adminWorksMgntDao.worksMgntReptMonthView(map);
		List list_0 = (List)adminWorksMgntDao.worksMgntReptMonthCoCnt(map);
		
		addList("ds_list", list);
		addList("ds_list_0", list_0);
		
    }
    
    public void worksMgntReptView() throws Exception {
    	
    	Dataset ds_condition = getDataset("ds_condition");
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		List list = (List)adminWorksMgntDao.worksMgntReptView(map);
		List list_0 = (List)adminWorksMgntDao.worksMgntReptViewCoCnt(map);
		
		addList("ds_list", list);
		addList("ds_list_0", list_0);
		
	}
	    
	public void worksMgntReptYearView() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		List list = (List)adminWorksMgntDao.worksMgntReptYearView(map);
		
		addList("ds_list", list);
	
	
    }

    public void worksMgntAppDate() throws Exception {
    	
		Dataset ds_condition = getDataset("ds_app_date");
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		List list = (List)adminWorksMgntDao.worksMgntAppDate(map);
		
		addList("ds_app_date", list);
	
    }
    
    
    public void crosWorksMgntReptView() throws Exception {
    	
    	Dataset ds_condition = getDataset("ds_condition");
    	
    	ds_condition.printDataset();
    	
    	Map map = getMap(ds_condition, 0);
    	
    	List list = (List)adminWorksMgntDao.crosWorksMgntReptView(map);
    	//List list_0 = (List)adminWorksMgntDao.worksMgntReptViewCoCnt(map);
    	
    	addList("ds_list", list);
    	//addList("ds_list_0", list_0);
    }
    
    
    //위탁관리 최신 기관 정보(기관코드, ID)
    public void worksMgntReptInfo2() throws Exception{
    	
    	Dataset ds_condition = getDataset("ds_condition");
    	
    	ds_condition.printDataset();
    	
    	Map map = getMap(ds_condition, 0);    	
    	
    	List list = (List)adminWorksMgntDao.worksMgntReptInfo2(map);    	
    	
    	addList("ds_report2", list);
    	
    }
    
    //위탁관리 정보 등록여부 확인
    public void worksMgntChkCnt() throws Exception {
    	
    	Dataset ds_condition = getDataset("ds_condition");
    	Map map = getMap(ds_condition, 0);   	
    	
    	if(map.get("GENRE_CODE").equals("1")){//음악
    		Dataset ds_excel = getDataset("ds_excel_music");
    		List list = (List)adminWorksMgntDao.worksMgntChkCnt(ds_condition, ds_excel);
    		addList("ds_count",list);
		}else if(map.get("GENRE_CODE").equals("2")){//어문
			Dataset ds_excel = getDataset("ds_excel_book");
			List list = (List)adminWorksMgntDao.worksMgntChkCnt(ds_condition, ds_excel);
			addList("ds_count",list);
		}else if(map.get("GENRE_CODE").equals("3")){//방송대본
			Dataset ds_excel = getDataset("ds_excel_script");
			List list = (List)adminWorksMgntDao.worksMgntChkCnt(ds_condition, ds_excel);
			addList("ds_count",list);
		}else if(map.get("GENRE_CODE").equals("4")){//영화
			Dataset ds_excel = getDataset("ds_excel_movie");
			List list = (List)adminWorksMgntDao.worksMgntChkCnt(ds_condition, ds_excel);
			addList("ds_count",list);
		}else if(map.get("GENRE_CODE").equals("5")){//방송
			Dataset ds_excel = getDataset("ds_excel_broadcast");
			List list = (List)adminWorksMgntDao.worksMgntChkCnt(ds_condition, ds_excel);
			addList("ds_count",list);
		}else if(map.get("GENRE_CODE").equals("6")){//뉴스
			Dataset ds_excel = getDataset("ds_excel_news");
			List list = (List)adminWorksMgntDao.worksMgntChkCnt(ds_condition, ds_excel);
			addList("ds_count",list);
		}else if(map.get("GENRE_CODE").equals("7")){//미술
			Dataset ds_excel = getDataset("ds_excel_art");
			List list = (List)adminWorksMgntDao.worksMgntChkCnt(ds_condition, ds_excel);
			addList("ds_count",list);
		}else if(map.get("GENRE_CODE").equals("99")){//기타
			Dataset ds_excel = getDataset("ds_excel_side");
			List list = (List)adminWorksMgntDao.worksMgntChkCnt(ds_condition, ds_excel);
			addList("ds_count",list);
		}
    	
    
    }
    
    // 위탁관리저작물 보고 목록 엑셀 다운로드
    public void worksMgntExcelDown() throws Exception {
    	
		Dataset ds_condition = getDataset("ds_condition");
		
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition, 0);
		
		List list = (List)adminWorksMgntDao.worksMgntExcelDown(map);
		//List pageList = (List)adminWorksMgntDao.totalRowWorksMgntList(map);
		
		addList("ds_exceldown", list);
		//addList("ds_page", pageList);
		
    }
}
