package kr.or.copyright.mls.console.rcept.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd24Service{

	/**
	 * 미분배보상금 도서관 저작물 보고일 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd24List1( Map<String, Object> commandMap ) throws Exception;

}
