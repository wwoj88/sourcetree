package kr.or.copyright.mls.rslt.dao;

import java.util.List;
import java.util.Map;

public interface RsltDao {

//	public int updateBoard(Board board);
	
	public List prpsRsltList(Map map);
	
	public List prpsRsltListCount(Map map);	
	
	public List inmtPrpsRsltList(Map map);

	public void inmtPrpsDelete(Map map);
	
	public void inmtPrpsRsltDelete(Map map);
	
	public void inmtKappDelete(Map map);
	
	public void inmtFokapoDelete(Map map);
	
	public void inmtKrtraDelete(Map map);
	
	
	public List prpsFileList(Map map);
	
	public void prpsFileDelete(Map map);
	
	public String getMaxDealStat(Map map);
	
	// 강명표 추가
	public void insertConnInfo(Map map);
}
