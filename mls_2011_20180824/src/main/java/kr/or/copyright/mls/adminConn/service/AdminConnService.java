package kr.or.copyright.mls.adminConn.service;


public interface AdminConnService {
	
	// 회원정보 목록조회
	public void monthList() throws Exception;

	public void dayList() throws Exception;	
	
	public void connStatPeriList() throws Exception;
	
	public void connStatPeriMonthList() throws Exception;
	
	public void statYearApplyList() throws Exception;

	public void statYearSpplyList() throws Exception;	
	
	public void statYearLgmtAmntList() throws Exception;
}
