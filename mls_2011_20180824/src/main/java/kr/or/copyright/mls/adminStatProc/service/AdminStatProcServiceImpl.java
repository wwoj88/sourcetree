package kr.or.copyright.mls.adminStatProc.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;


import com.tobesoft.platform.data.Dataset;

import kr.or.copyright.mls.adminEvnSet.dao.AdminEvnSetDao;
import kr.or.copyright.mls.adminStatProc.dao.AdminStatProcDao;
import kr.or.copyright.mls.common.dao.CommonDao;
import kr.or.copyright.mls.common.mail.SendMail;
import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.DateUtil;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;

@Service("AdminStatProcService")
public class AdminStatProcServiceImpl extends BaseService implements AdminStatProcService{
	
	
	private AdminStatProcDao adminStatProcDao;
	private AdminEvnSetDao adminEvnSetDao;
	private CommonDao commonDao;
	
	
	public void setAdminStatProcDao(AdminStatProcDao adminStatProcDao){
		this.adminStatProcDao = adminStatProcDao;
	}
	
	public void setAdminEvnSetDao(AdminEvnSetDao adminEvnSetDao){
		this.adminEvnSetDao = adminEvnSetDao;
	}
	
	public void setCommonDao(CommonDao commonDao){
		this.commonDao = commonDao; 
	}
	  
	// 스케쥴링 (기존)
	public void schedulingV10()throws Exception{
		
		System.out.println("=====================================================================");
		System.out.println("1 >> "+DateUtil.getCurrDateNumber()+" "+DateUtil.getCurrTimeNumber());
		System.out.println("=====================================================================");
		
		try {
			
			// 1. 상당한 노력 대상 저작물 수집
			//adminStatProcDao.callProcGetStatProcOrd();
			
			HashMap map = new HashMap();
			map.put("SCHEDULE_SRCH_STAT_CD", "2");
		
			// 2. 실행대상 저작물 수집
			ArrayList list = (ArrayList) adminStatProcDao.selectStatProcAllLogList(map);
			
			// 3. 실행
			System.out.println("     >>>>> 상당한노력 START ");
			if(list.size()>0) {
				
				for( int iTot=0; iTot<list.size(); iTot++) {
					
					int iBase = 10; // 프로시저 실행 구분 건수
					int iMod = 0;
					int iTargCnt = 0;
					int iRoop = 0;
					
					HashMap statMap = (HashMap)list.get(iTot);
					
					iTargCnt = Integer.parseInt(String.valueOf(statMap.get("TARG_WORKS_CONT")));
					//iTargCnt=(Integer)statMap.get("TARG_WORKS_CONT");
					
					// 단위 건수 셋팅
					iRoop = (iTargCnt/iBase);
					iMod = (iTargCnt%iBase);
					
					System.out.println("=iTargCnt : "+iTargCnt);
					System.out.println("=iRoop : "+iRoop);
					System.out.println("=iMod : "+iMod);
					if(iMod > 0 ) iRoop++;
					System.out.println("=iRoop : "+iRoop);
					
					int iFrom = 0;
					int iTo = 0;
					
					// 전체로그 : start
					for(int i=0; i<iRoop; i++) {
						
						iFrom = (i*iBase)+1;
						iTo = (i*iBase)+iBase;
						if(iTo>iTargCnt) iTo = iTargCnt;
						
						statMap.put("FROM", iFrom);
						statMap.put("TO", iTo);
						
						System.out.println("*** roop test >> "+i+" :: "+iFrom+", "+iTo);
						// call 프로시저
						adminStatProcDao.callStatProc(statMap);
						
					}
					// 전체로그 : end
					System.out.println("     >>>>> 상당한노력 END ");
				}
				
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			System.out.println("END=====================================================================");
			System.out.println(" ");
		}
	}
	
	// 스케쥴링 사전체크 (1. 자동화설정/)
	private boolean preCheckSchedule(String setCd){
		
		boolean bFlag = false;
		HashMap map = new HashMap();
		
		map.put("SET_CD", setCd); 
		
		// 0. 자동화 설정여부 확인
		List set_list = (List) adminEvnSetDao.statProcSetList(map);
		HashMap setMap = (HashMap)set_list.get(0);
		
		if(setMap.get("USE_YN").equals("1")) bFlag = true;
		
		return bFlag;
	}
	
	// 스케쥴링 (분기. 20121109) + 자동화설정추가(20131018)
	public void statProcScheduling()throws Exception{
		
		boolean bFlag = false;	// 스케쥴링 실행 플래그
		String sMailMent = "상당한노력 수행 대상저작물 전환대상이 없습니다. <BR/> 예외처리 대상이 존재하는 경우 [완료]처리해주세요.";
		List resultList = new ArrayList();
		
	    
		try {
			
			HashMap map = new HashMap();
		
			// 0. 자동화 설정여부 확인
			bFlag = preCheckSchedule("10"); // 10 : 상당한노력 대상저작물 자동수행, 20 : 예외처리직접수행  ,30 : 법정허락전환 자동수행
			
			if(bFlag) {
				
				// 1. 상당한 노력 대상 저작물 수집
				map.put("P_SCH_YN", "Y"); // 자동화 "Y"
				map.put("P_TRST_ORGN_CODE", "199");
				adminStatProcDao.callProcGetStatProcOrd(map);
				map.put("P_TRST_ORGN_CODE", "200");
				adminStatProcDao.callProcGetStatProcOrd(map);
				
				map.put("P_TRST_ORGN_CODE", "");
				map.put("SCHEDULE_SRCH", "Y");
				
				// 2. 실행대상 저작물 수집
				ArrayList list = (ArrayList) adminStatProcDao.selectStatProcAllLogList(map);
				
				// 3. 실행 -- 수집완료인건만; (STAT_CD") == "2")
				System.out.println("     >>>>> 상당한노력 START ");
				
				if(list.size()>0) {
					
					for( int iTot=0; iTot<list.size() ; iTot++) {
						
						HashMap statMap = (HashMap)list.get(iTot);
						
						// 수집완료인건만..
						if(Integer.parseInt(String.valueOf(statMap.get("STAT_CD")))==2) {
							
							// 결과조회용 key 값 수집
							resultList.add( statMap.get("ORD")+"|"+statMap.get("WORKS_DIVS_CD"));
							
							int iBase = 10; // 프로시저 실행 구분 건수
							int iMod = 0;
							int iTargCnt = 0;
							int iRoop = 0;
							
							iTargCnt = Integer.parseInt(String.valueOf(statMap.get("TARG_WORKS_CONT")));
							
							// 단위 건수 셋팅
							iRoop = (iTargCnt/iBase);
							iMod = (iTargCnt%iBase);
							
							System.out.println("=iTargCnt : "+iTargCnt);
							System.out.println("=iRoop : "+iRoop);
							System.out.println("=iMod : "+iMod);
							if(iMod > 0 ) iRoop++;
							System.out.println("=iRoop : "+iRoop);
							
							int iFrom = 0;
							int iTo = 0;
												
							// 01. 상당한노력 공고
							for(int i=0; i<iRoop; i++) {
								
								iFrom = (i*iBase)+1;
								iTo = (i*iBase)+iBase;
								if(iTo>iTargCnt) iTo = iTargCnt;
								
								statMap.put("FROM", iFrom);
								statMap.put("TO", iTo);
								
								System.out.println("*** 공고 roop >> "+i+" :: "+iFrom+", "+iTo);
								
								// call 프로시저 
								adminStatProcDao.callStatProc01(statMap);
								
							} // ..end 01. 상당한노력 공고
							
							// 02. 매칭
							for(int i=0; i<iRoop; i++) {
								
								iFrom = (i*iBase)+1;
								iTo = (i*iBase)+iBase;
								if(iTo>iTargCnt) iTo = iTargCnt;
								
								statMap.put("FROM", iFrom);
								statMap.put("TO", iTo);
								//statMap.put("P_SCH_YN", "Y");	// 자동화 "Y"
								
								System.out.println("*** 매칭 roop >> "+i+" :: "+iFrom+", "+iTo);
								System.out.println( "adminStatProcDao.callStatProc01(statMap) : " + statMap );
								// call 프로시저 
								adminStatProcDao.callStatProc02(statMap);
								
							} // ..end 02.매칭
							
							
						} // .. end if
						
						// 메일링 (거소불명 대상조회 +send) + 예외처리 아닌경우
						System.out.println( "===================이하 메일======================= : ");
						if(String.valueOf(statMap.get("WORKS_DIVS_CD")).equals("2") && !preCheckSchedule("20")){ // 거소불명 
							
							statMap.put("STAT_PROC_YN", "N");	// 수행대상 제외
							statMap.put("STAT_CHNG_YN", "N");
							statMap.put("WORKS_DIVS_CD", "4");
							
							// 메일공통정보 set
							statMap.put("MAIL_TITL", "저작권자 찾기 위한 상당한노력 수행 완료");
							//map.put("MAIL_MENT", "");
							statMap.put("MAIL_RESULT", "법정허락 전환이 불가능한 저작물 입니다.");
							System.out.println( "sendResultMail(statMap) : " + statMap );
							this.sendResultMail(statMap);
						}
						
					} // ..end for
					
				}
				
				/* *** 메일링 설정 START ***********/
				HashMap mailMap = new HashMap();
				mailMap.put("result", resultList);
				
				int excpTargCnt = 0; // 예외처리 대상여부
				
				if(list.size() > 0) {
					
					ArrayList mailList = (ArrayList) adminStatProcDao.selectStatProcAllLogList(mailMap);
					
					// 메일전달정보
					int mailListSize = mailList.size();
					
					int ord[] = new int[mailListSize];
					String yyyymm[] = new String[mailListSize];
					String works_divs_name[] = new String[mailListSize];
					int rslt_cont_ys[] = new int[mailListSize];
					int rslt_cont_no[] = new int[mailListSize];
					
					for(int i = 0; i<mailListSize; i++){
						HashMap subMap = (HashMap) mailList.get(i);
						
						if(String.valueOf(subMap.get("EXCP_STAT_CD")).equals("20"))
							excpTargCnt++;
						
						ord[i] = Integer.parseInt(String.valueOf( subMap.get("ORD")));
						yyyymm[i] = (String) subMap.get("YYYYMM");
						works_divs_name[i] = (String) subMap.get("WORKS_DIVS_NAME");
						
						rslt_cont_ys[i] = Integer.parseInt(String.valueOf(subMap.get("RSLT_CONT_1ST_YS")));
						rslt_cont_ys[i] += Integer.parseInt(String.valueOf( subMap.get("RSLT_CONT_2ND_YS")));
						rslt_cont_ys[i] += Integer.parseInt(String.valueOf( subMap.get("RSLT_CONT_3RD_YS")));
						
						rslt_cont_no[i] = Integer.parseInt(String.valueOf( subMap.get("RSLT_CONT_1ST_NO")));
					}
					
					mailMap.put("ORD", ord);
					mailMap.put("YYYYMM", yyyymm);
					mailMap.put("WORKS_DIVS_NAME", works_divs_name);
					mailMap.put("RSLT_CONT_YS", rslt_cont_ys);
					mailMap.put("RSLT_CONT_NO", rslt_cont_no);
					
					// 1)예외처리 직접수행 여부에 따라 안내메일 구성 다름
					//if( preCheckSchedule("20") ){	
					if( preCheckSchedule("20") && excpTargCnt>0 ){	
						//1-1) 직접수행 : 완료안내 및 예외처리 요청 mailing
						sMailMent = "상당한노력 예외처리 직접수행을 진행하여 [완료]처리해주세요.";
					}else{
						//1-2) none : 완료안내 및 법정허락전환 자동수행 안내 mailing
						sMailMent = "법정허락 대상저작물 전환을 직접수행해주세요.";
						
						if(preCheckSchedule("30")){
							sMailMent = "자동화설정에 따라 법정허락 대상저작물 전환이 자동수행됩니다.";
							// 법정허락 자동수행 호출
						}
					}
				}
				
				mailMap.put("MAIL_MENT", sMailMent);
				mailMap.put("MAIL_TITL", "상당한노력 자동화 수행");
				mailMap.put("MGNT_DIVS", "STAT_PROC");
				
				List reclist = commonDao.getMgntMail(mailMap);
				
				int listSize = reclist.size();
				
				String to[] = new String[listSize];
				String toName[] = new String[listSize];
					
				for(int i = 0; i<listSize; i++){
					HashMap subMap = (HashMap) reclist.get(i);
					
					toName[i] = (String) subMap.get("USER_NAME");
					to[i] = (String) subMap.get("MAIL");
				}
				
				mailMap.put("TO", to);
				mailMap.put("TO_NAME", toName);
				
				SendMail.send(mailMap);	
				
				/* *** 메일링 설정 END ***********/
				if((!preCheckSchedule("20")|| (preCheckSchedule("20") && excpTargCnt==0) )&& preCheckSchedule("30")){
					statWorksScheduling();  // 법정허락 자동수행 호출
				}
				
			} // .. end if
			System.out.println("     >>>>> 상당한노력 END ");
			
					
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
		}
	}
	
	// 법정허락 전환 자동화 호출 (예외처리 완료시 사용한다.)
	public void callStatWorksScheduling() throws Exception{
		
		boolean bFlag = false;
		HashMap map = new HashMap();
		
		map.put("P_EXCP_STAT_CD", "20");	// (20:예외처리대기)
		
		ArrayList list = (ArrayList) adminStatProcDao.selectStatProcAllLogList(map);
		
		// 모든예외처리 완료 시
		if(list.size() == 0) {
			
			bFlag = preCheckSchedule("30");
			
			HashMap mailMap = new HashMap();
			// 1) 예외처리 완료 메일링
			mailMap.put("MAIL_MENT", "법정허락 대상저작물 전환을 직접수행해주세요.");
			
			if(bFlag){
				mailMap.put("MAIL_MENT", "자동화설정에 따라 법정허락 대상저작물 전환이 자동수행됩니다.");
				// 법정허락 자동수행 호출
			}
			
			mailMap.put("MAIL_TITL", "상당한노력 예외처리 직접수행");
			mailMap.put("MGNT_DIVS", "STAT_PROC_EXCP");
			
			List reclist = commonDao.getMgntMail(mailMap);
			
			int listSize = reclist.size();

			String to[] = new String[listSize];
			String toName[] = new String[listSize];
				
			for(int i = 0; i<listSize; i++){
				HashMap subMap = (HashMap) reclist.get(i);
				
				toName[i] = (String) subMap.get("USER_NAME");
				to[i] = (String) subMap.get("MAIL");
			}
			
			mailMap.put("TO", to);
			mailMap.put("TO_NAME", toName);
			
			SendMail.send(mailMap);		
			/* *** 메일링 설정 END ***********/
			
			// 2) 법정허락 전환 자동화 설정여부에 따라 자동화 호출
			if(bFlag){
				statWorksScheduling();  // 법정허락 자동수행 호출
			}
		}
		
	}
	
	
	// 법정허락 전환 자동화
	public void statWorksScheduling()throws Exception{
		
		boolean bFlag = false;	// 스케쥴링 실행 플래그
		String sMailMent = "법정허락 대상저작물 전환대상이 없습니다.";
		List resultList = new ArrayList();
		
		try {
			
			HashMap map = new HashMap();
			
			// 0. 자동화 설정여부 확인
			bFlag = preCheckSchedule("30"); // 10 : 상당한노력 대상저작물 자동수행, 20 : 예외처리직접수행  ,30 : 법정허락전환 자동수행
			
			if(bFlag) {
				
				// 1. 법정허락 전환 대상 저작물 수집
				map.put("P_SCH_YN", "Y"); // 자동화 "Y"
				map.put("SCHEDULE_SRCH", "Y");
				
				adminStatProcDao.callGetStatWorksord(map);
				
				// 2. 실행대상 저작물 수집
				ArrayList list = (ArrayList) adminStatProcDao.selectStatWorksAllLogList(map);
				
				// 3. 실행 -- 수집완료인건만; (STAT_CD") == "2")
				System.out.println("     >>>>> 법정허락전환 START ");
				
				if(list.size()>0) {
					
					for( int iTot=0; iTot<list.size() ; iTot++) {
						
						HashMap statMap = (HashMap)list.get(iTot);
						
						// 수집완료인건만..
						if(Integer.parseInt(String.valueOf(statMap.get("STAT_CD")))==2) {
							
//							 결과조회용 key 값 수집
							resultList.add( statMap.get("STAT_ORD")+"|"+statMap.get("YYYYMM"));
							
							int iBase = 10; // 프로시저 실행 구분 건수
							int iMod = 0;
							int iTargCnt = 0;
							int iRoop = 0;
							
							iTargCnt = Integer.parseInt(String.valueOf(statMap.get("TARG_WORKS_CONT")));
							
							// 단위 건수 셋팅
							iRoop = (iTargCnt/iBase);
							iMod = (iTargCnt%iBase);
							
							System.out.println("=iTargCnt : "+iTargCnt);
							System.out.println("=iRoop : "+iRoop);
							System.out.println("=iMod : "+iMod);
							if(iMod > 0 ) iRoop++;
							System.out.println("=iRoop : "+iRoop);
							
							int iFrom = 0;
							int iTo = 0;
							
							// 전체로그 : start
							for(int i=0; i<iRoop; i++) {
								
								iFrom = (i*iBase)+1;
								iTo = (i*iBase)+iBase;
								if(iTo>iTargCnt) iTo = iTargCnt;
								
								statMap.put("FROM", iFrom);
								statMap.put("TO", iTo);
								
								System.out.println("*** 전환 roop >> "+i+" :: "+iFrom+", "+iTo);
								// call 프로시저
								adminStatProcDao.callChangeStatWorks(statMap);
								
							} // .. end for		
							
							// 메일링 (거소불명 대상조회 +send)
							if(Integer.parseInt(String.valueOf(statMap.get("WORKS_DIVS_CONT_02")))>0){ // 거소불명 카운트가 0보다 클때
								statMap.put("STAT_CHNG_YN", "Y");	// 전환대상
								statMap.put("STAT_PROC_YN", "Y");
								statMap.put("WORKS_DIVS_CD", "4");
								
								// 메일공통정보 set
								statMap.put("MAIL_TITL", "저작권자 찾기 위한 상당한노력 수행 완료");
								//map.put("MAIL_MENT", "");
								statMap.put("MAIL_RESULT", "법정허락 대상 저작물로 전환이 완료되었습니다. 법정허락 이용승인신청이 가능합니다.");
											
								this.sendResultMail(statMap);
							}

							
						} // .. end if
						
					} // ..end for
					
					
				}
				
				/* *** 메일링 설정 START ***********/
				HashMap mailMap = new HashMap();
				mailMap.put("result", resultList);
				
				if(list.size()>0) {
					
					sMailMent = "";
					
					ArrayList mailList = (ArrayList) adminStatProcDao.selectStatWorksAllLogList(mailMap);
					
					// 메일전달정보
					int mailListSize = mailList.size();
					
					int ord[] = new int[mailListSize];
					String yyyymm[] = new String[mailListSize];
					int targ_works_cont[] = new int [mailListSize];
					int rslt_cont[] = new int[mailListSize];
					
					for(int i = 0; i<mailListSize; i++){
						HashMap subMap = (HashMap) mailList.get(i);
						
						ord[i] = Integer.parseInt(String.valueOf( subMap.get("STAT_ORD")));
						yyyymm[i] = (String) subMap.get("YYYYMM");
						targ_works_cont[i] = Integer.parseInt(String.valueOf(  subMap.get("TARG_WORKS_CONT")));
						rslt_cont[i] = Integer.parseInt(String.valueOf(subMap.get("RSLT_CONT")));
										}
					
					mailMap.put("ORD", ord);
					mailMap.put("YYYYMM", yyyymm);
					mailMap.put("TARG_WORKS_CONT", targ_works_cont);
					mailMap.put("RSLT_CONT", rslt_cont);
				}
				
				//법정허락 전환  수행완료 메일링 
				mailMap.put("MAIL_MENT", sMailMent);
				mailMap.put("MAIL_TITL", "법정허락 대상저작물 전환 자동화 수행");
				mailMap.put("MGNT_DIVS", "STATWORK_PROC");
				
				List reclist = commonDao.getMgntMail(mailMap);
				
				int listSize = reclist.size();

				String to[] = new String[listSize];
				String toName[] = new String[listSize];
					
				for(int i = 0; i<listSize; i++){
					HashMap subMap = (HashMap) reclist.get(i);
					
					toName[i] = (String) subMap.get("USER_NAME");
					to[i] = (String) subMap.get("MAIL");
				}
				
				mailMap.put("TO", to);
				mailMap.put("TO_NAME", toName);
				
				SendMail.send(mailMap);	
				/* *** 메일링 설정 END ***********/
				
			} // .. end if
			System.out.println("     >>>>> 법정허락전환  END ");
			
			// 법정허락대상 전환완료  mailing 추가
		
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
		}
	}
	
	
	// 상당한노력 진행현황 조회
	public void statProcList() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List) adminStatProcDao.selectStatProcAllLogList(map);
		List works_list = (List) adminStatProcDao.selectStatWorksAllLogList(map);
		List set_list = (List) adminEvnSetDao.statProcSetList(map);
		
		addList("ds_works_list", works_list);
		addList("ds_list", list);
		addList("ds_set_list", set_list);
	}
	
	// 법정허락 대상물 전환 진행현황 조회
	public void statWorksList() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List) adminStatProcDao.selectStatWorksAllLogList(map);
		
		addList("ds_works_list", list);
	}
	
	// 대상저작물 수집-프로시져 호출
	public void getStatProcOrd() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		//ds_condition.printDataset();
		
		// DAO 호출
		adminStatProcDao.callProcGetStatProcOrd(map); 
		List list = (List) adminStatProcDao.selectStatProcAllLogList(map);
		
		addList("ds_list", list);
	}
	
	// 법정허락 대상저작물 수집-프로시져 호출
	public void getStatWorksOrd() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		adminStatProcDao.callGetStatWorksord(map);
		List list = (List) adminStatProcDao.selectStatWorksAllLogList(map);
		
		addList("ds_works_list", list);
	}
	
	// 상당한 노력 진행(기존)
	public void goStatProcV10() throws Exception{
		
		Dataset ds_list_proc = getDataset("ds_list_proc");
		
		//ds_list_proc.printDataset();
		
		Map map = getMap(ds_list_proc, 0);
		
		map.get("ORD");
		map.get("WORKS_DIVS_CD");
		
		int iBase = 10000; // 프로시저 실행 구분 건수
		int iMod = 0;
		int iTargCnt = 0; 
		int iRoop = 0;
		
		iTargCnt = ds_list_proc.getColumnAsInteger(0,"TARG_WORKS_CONT");//(Integer) map.get("TARG_WORKS_CONT");
		
		/*
		if(ds_list_proc.getColumnAsString(0, "WORKS_DIVS_CD") == "1") // 미분배
			map.put("WORKS_DIVS_CD", "1,2,3");
		else 
			map.put("WORKS_DIVS_CD", "4");	// 거소불명
		*/
		
		// 단위 건수 셋팅
		iRoop = (iTargCnt/iBase);
		iMod = (iTargCnt%iBase);
		
		System.out.println("=iTargCnt : "+iTargCnt);
		System.out.println("=iRoop : "+iRoop);
		System.out.println("=iMod : "+iMod);
		if(iMod > 0 ) iRoop++;
		System.out.println("=iRoop : "+iRoop);
		
		int iFrom = 0;
		int iTo = 0;
		
		// 전체로그 : start
		for(int i=0; i<iRoop; i++) {
			
			iFrom = (i*iBase)+1;
			iTo = (i*iBase)+iBase;
			if(iTo>iTargCnt) iTo = iTargCnt;
			
			map.put("FROM", iFrom);
			map.put("TO", iTo);
			
			System.out.println("*** roop test >> "+i+" :: "+iFrom+", "+iTo);
			// call 프로시저
			adminStatProcDao.callStatProc(map);
			
		}
		// 전체로그 : end
	}
	
	// 상당한 노력 진행(프로세스 분기 20121108)
	public void goStatProc() throws Exception{
		
		Dataset ds_list_proc = getDataset("ds_list_proc");
		
		ds_list_proc.printDataset();
		
		Map map = getMap(ds_list_proc, 0);
		
		//map.get("ORD");
		//map.get("WORKS_DIVS_CD");
		
		int iBase = 500; // 프로시저 실행 구분 건수
		int iMod = 0;
		int iTargCnt = 0; 
		int iRoop = 0;
		
		iTargCnt = ds_list_proc.getColumnAsInteger(0,"TARG_WORKS_CONT");
		
		// 단위 건수 셋팅
		iRoop = (iTargCnt/iBase);
		iMod = (iTargCnt%iBase);
		
		System.out.println("=iTargCnt : "+iTargCnt);
		System.out.println("=iRoop : "+iRoop);
		System.out.println("=iMod : "+iMod);
		if(iMod > 0 ) iRoop++;
		System.out.println("=iRoop : "+iRoop);
		
		int iFrom = 0;
		int iTo = 0;
		
		// 01. 상당한노력 공고
		for(int i=0; i<iRoop; i++) {
			
			iFrom = (i*iBase)+1;
			iTo = (i*iBase)+iBase;
			if(iTo>iTargCnt) iTo = iTargCnt;
			
			map.put("FROM", iFrom);
			map.put("TO", iTo);
			
			System.out.println("*** 공고 roop >> "+i+" :: "+iFrom+", "+iTo);
			
			// call 프로시저 
			adminStatProcDao.callStatProc01(map);
			
		}
		
		// 02. 매칭
		for(int i=0; i<iRoop; i++) {
			
			iFrom = (i*iBase)+1;
			iTo = (i*iBase)+iBase;
			if(iTo>iTargCnt) iTo = iTargCnt;
			
			map.put("FROM", iFrom);
			map.put("TO", iTo);
			
			System.out.println("*** 매칭 roop >> "+i+" :: "+iFrom+", "+iTo);
			
			// call 프로시저 
			adminStatProcDao.callStatProc02(map);
			
		}
		
		// 메일링 (거소불명 대상조회 +send) + 예외처리 아닌경우
		if(map.get("WORKS_DIVS_CD").equals("2") && !preCheckSchedule("20")){ // 거소불명 
			
			map.put("STAT_PROC_YN", "N");	// 수행대상 제외
			map.put("STAT_CHNG_YN", "N");
			map.put("WORKS_DIVS_CD", "4");
			
			// 메일공통정보 set
			map.put("MAIL_TITL", "저작권자 찾기 위한 상당한노력 수행 완료");
			//map.put("MAIL_MENT", "");
			map.put("MAIL_RESULT", "법정허락 전환이 불가능한 저작물 입니다.");
			
			this.sendResultMail(map);
		}
		
	}
	
	// 상당한노력 결과 메일링 (거소불명 신청건만 사용)
	public void sendResultMail(Map map) throws Exception{
		
		List mailList = (List)adminStatProcDao.statProcTargWorksMailList(map);
		
		for(int i=0; i<mailList.size(); i++) {
			
			Map mailMap = (HashMap)mailList.get(i);
			
			mailMap.put("MAIL_TITL", map.get("MAIL_TITL"));
			mailMap.put("MAIL_RESULT", map.get("MAIL_RESULT"));
			mailMap.put("MAIL_WORKS_INFO", mailMap.get("WORKS_TITLE")+" ("+mailMap.get("RGST_DTTM")+")");
			
			
			this.sendMail(mailMap);
		}
	}
		
	// 법정허락 대상 전환
	public void goChangeStatWorks() throws Exception {
		
		Dataset ds_list_proc = getDataset("ds_works_list_proc");
		
		ds_list_proc.printDataset();
		
		Map map = getMap(ds_list_proc, 0);
		
		map.get("STAT_ORD");
		
		int iBase = 10; // 프로시저 실행 구분 건수
		int iMod = 0;
		int iTargCnt = 0; 
		int iRoop = 0;
		
		iTargCnt = ds_list_proc.getColumnAsInteger(0,"TARG_WORKS_CONT");//(Integer) map.get("TARG_WORKS_CONT");
		
		/*
		if(ds_list_proc.getColumnAsString(0, "WORKS_DIVS_CD") == "1") // 미분배
			map.put("WORKS_DIVS_CD", "1,2,3");
		else 
			map.put("WORKS_DIVS_CD", "4");	// 거소불명
		*/
		
		// 단위 건수 셋팅
		iRoop = (iTargCnt/iBase);
		iMod = (iTargCnt%iBase);
		
		System.out.println("=iTargCnt : "+iTargCnt);
		System.out.println("=iRoop : "+iRoop);
		System.out.println("=iMod : "+iMod);
		if(iMod > 0 ) iRoop++;
		System.out.println("=iRoop : "+iRoop);
		
		int iFrom = 0;
		int iTo = 0;
		
		// 전체로그 : start
		for(int i=0; i<iRoop; i++) {
			
			iFrom = (i*iBase)+1;
			iTo = (i*iBase)+iBase;
			if(iTo>iTargCnt) iTo = iTargCnt;
			
			map.put("FROM", iFrom);
			map.put("TO", iTo);
			
			System.out.println("*** roop test >> "+i+" :: "+iFrom+", "+iTo);
			// call 프로시저
			adminStatProcDao.callChangeStatWorks(map);
			
		}
		// 전체로그 : end
		
		// 메일링 (거소불명 대상조회 +send)
		if(ds_list_proc.getColumnAsInteger(0,"WORKS_DIVS_CONT_02")>0){ // 거소불명 카운트가 0보다 클때
			map.put("STAT_CHNG_YN", "Y");	// 전환대상
			map.put("STAT_PROC_YN", "Y");
			map.put("WORKS_DIVS_CD", "4");
			
			// 메일공통정보 set
			map.put("MAIL_TITL", "저작권자 찾기 위한 상당한노력 수행 완료");
			//map.put("MAIL_MENT", "");
			map.put("MAIL_RESULT", "법정허락 대상 저작물로 전환이 완료되었습니다. 법정허락 이용승인신청이 가능합니다.");
						
			this.sendResultMail(map);
		}
	}
	
	// 상당한 노력 예외처리완료
	public void goStatExcpDone() throws Exception{
		Dataset ds_list_proc = getDataset("ds_list_proc");
		
		//ds_list_proc.printDataset();
		
		Map map = getMap(ds_list_proc, 0);
		
		map.get("ORD");
		map.get("WORKS_DIVS_CD");

		adminStatProcDao.statProcAllLogUpdate(map);
		
		
        // 메일링 (거소불명 대상조회 +send) + 예외처리 경우
		if(map.get("WORKS_DIVS_CD").equals("2") && preCheckSchedule("20")){ // 거소불명 
			
			map.put("STAT_PROC_YN", "N");	// 수행대상 제외
			map.put("STAT_CHNG_YN", "N");
			map.put("WORKS_DIVS_CD", "4");
			
			// 메일공통정보 set
			map.put("MAIL_TITL", "저작권자 찾기 위한 상당한노력 수행 완료");
			//map.put("MAIL_MENT", "");
			map.put("MAIL_RESULT", "법정허락 전환이 불가능한 저작물 입니다.");
			
			this.sendResultMail(map);
		}
	}
	
	// 대상저작물전환
	public void chgStatWorksUpdateYn() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
	
		ds_condition.printDataset();
		
		Map map = getMap(ds_condition,0);
		
		adminStatProcDao.chgStatWorksUpdateYn(map);
		
	}
	
	public void inmtReptView() throws Exception {
	    Dataset ds_condition = getDataset("ds_condition");
	    
	    Map map = getMap(ds_condition,0);
	    
	    List list = (List)adminStatProcDao.inmtReptView(map);
	    
	    addList("ds_List", list);
	    
	}

	public void nonWorksReptView() throws Exception {
	    Dataset ds_condition = getDataset("ds_condition");
	    
	    Map map = getMap(ds_condition,0);
	    
	    List list = (List)adminStatProcDao.nonWorksReptView(map);
	    
	    addList("ds_List", list);
	    
	}
	
	public void worksEntReptView() throws Exception {
	    
	    Dataset ds_condition = getDataset("ds_condition");
	    
	    Map map = getMap(ds_condition,0);
	    
	    List list = (List)adminStatProcDao.worksEntReptView(map);
	    
	    addList("ds_List", list);
	    
	    
	}
	
	// 대상 저작물 목록
	public void statProcTargPopup() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		String worksDivsCd = map.get("WORKS_DIVS_CD").toString();
		
		List excpList = new ArrayList();
		List excpPageCount = new ArrayList();
		
		if(worksDivsCd.equals("1")) {
			excpList = (List) adminStatProcDao.statProcTargExcpList(map);
			excpPageCount = (List) adminStatProcDao.statProcPopListCount(map);
		}
		
		// DAO 호출
		List list = (List) adminStatProcDao.statProcTargPopList(map);
		List pageCount = (List) adminStatProcDao.statProcTargPopListCount(map);
		
		addList("ds_list", list);
		addList("ds_page", pageCount);
		addList("ds_excpList", excpList);
		addList("ds_page_excp", excpPageCount);
	}

	
	// 상당한노력 현황 조회
	public void statProcPopup() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List) adminStatProcDao.statProcPopup(map);
		List pageCount = (List) adminStatProcDao.statProcPopListCount(map);
		
		addList("ds_list", list);
		addList("ds_page", pageCount);
		
	}

	public void statProcWorksPopup() throws Exception {
		
		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List) adminStatProcDao.statProcWorksPopup(map);
		List pageCount = (List) adminStatProcDao.statProcWorksPopListCount(map);
		
		addList("ds_works_list", list);
		addList("ds_page", pageCount);
		
	}
	
	// 보고저작물 등록기간 관리
	public void reptMgntList() throws Exception {
	    
	    Dataset ds_condition = getDataset("ds_condition");
	    Map map = getMap(ds_condition, 0);
	    
	    List list = (List) adminStatProcDao.reptMgntList(map);
	    
	    addList("ds_list_0", list);
	    
	}
	
	// 보고저작물 등록기간 수정
	public void reptMgntUpdate() throws Exception{
	    
	    Dataset ds_condition = getDataset("ds_condition");
	    Map map = getMap(ds_condition, 0);
	    int idx = Integer.parseInt((String) map.get("CD"))-1;
	    
	    Dataset ds_list = getDataset("ds_list_0");
	    Map map1 = getMap(ds_list,idx);
	    
	    if(map1.get("YSNO").equals("Y")){
		adminStatProcDao.reptMgntUpdate(map1);
	    }else{
		adminStatProcDao.reptMgntUpdateYn(map1);
	    }
	    
	}
	
	public void statProcInfo() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");
		Map map = getMap(ds_condition, 0);
		
		// DAO 호출
		List list = (List) adminStatProcDao.statProcInfo(map);
		List pageCount = (List) adminStatProcDao.statProcInfoCount(map);
		
		addList("ds_list", list);
		addList("ds_page", pageCount);
		
	}
	
//	 상당한노력 결과 메일링 
	public void sendMail(Map mailMap) throws Exception{
		
		if(mailMap.get("MAIL_ADDR")!= null) {
			
			System.out.println("==================================================");
			System.out.println(">> " + mailMap.get("MAIL_ADDR").toString());
			System.out.println("==================================================");
			
			String subject = "";
			
			String host = Constants.MAIL_SERVER_IP; // smtp서버
			String to = mailMap.get("MAIL_ADDR").toString(); // 수신EMAIL
			String toName = mailMap.get("USER_NAME").toString(); // 수신인
			
			String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
			String fromName = Constants.SYSTEM_NAME;
			
			String mailTitl = (String) mailMap.get("MAIL_TITL");
			String mailWorksInfo = (String) mailMap.get("MAIL_WORKS_INFO");	// 저작물정보
			String mailResult = (String) mailMap.get("MAIL_RESULT");	// 수행결과
			
			
			// ------------------------- mail 정보
			// ----------------------------------//
			MailInfo info = new MailInfo();
			
			info.setFrom(from);
			info.setFromName(fromName);
			info.setHost(host);
			info.setEmail(to);
			info.setEmailName(toName);
			info.setSubject(subject);
			
			subject = mailTitl+"이 완료되었습니다.";
			
			StringBuffer sb = new StringBuffer();				
			sb.append("				<tr>    \n");
			sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">    \n");
			sb.append("						<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">"+mailTitl+" 정보</p>    \n");
			
			sb.append("						<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">    \n");
			
			sb.append("							<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">신청정보 : "+mailWorksInfo+"</li>    \n");
			sb.append("							<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">수행결과 : "+mailResult+"</li>    \n");
			sb.append("						</ul>    \n");
			
			sb.append("					</td>    \n");
			sb.append("				</tr>    \n");

		    info.setMessage(sb.toString());
		    info.setSubject(subject);
			info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
			
			MailManager manager = MailManager.getInstance();

////////////////////////////////////////////////////////////////////////////////
			/*
			HashMap insertMap = new HashMap();
			
			insertMap.put("EMAIL_TITLE", info.getSubject());
			insertMap.put("EMAIL_DESC", info.getMessage());
			insertMap.put("EMAIL_ADDR", info.getEmail());
			
			adminStatProcDao.insertMailList(insertMap);
			*/
//////////////////////////////////////////////////////////////////////////////		//
			
			
			
			info = manager.sendMail(info);
			
			if (info.isSuccess()) {
				System.out.println(">>>>>>>>>>>> message success :" + info.getEmail());
			} else {
				System.out.println(">>>>>>>>>>>> message false   :" + info.getEmail());
			}
			
			
		}
	}
	
	/*
	//신청 담당자 메일수신관리
	public void selectMailMgntList() throws Exception{
		// DAO 호출
		List list = (List) adminStatProcDao.selectMailMgntList();
		
		addList("ds_list", list);
	}
	
	// 그룹, 그룹메뉴 삭제하기
	public void deleteMailMgntList() throws Exception{
		
	    	Dataset ds_list = getDataset("ds_list");
	    	
	    	for(int i=0; i<ds_list.getRowCount(); i++){
	    	    Map dsMap = getMap(ds_list, i);
	    	    
	    	    if(dsMap.get("CHK").equals("1")){
	    		adminStatProcDao.deleteMailMgntList(dsMap);
	    	    }
	    	}
	    	
	    	//List list = (List) adminStatProcDao.selectMailMgntList();
		
		//addList("ds_list", list);
	}
	
	public void insertMailMgntList() throws Exception{
	    Dataset ds_regi = getDataset("ds_regi");
	    
	    Map dsMap = getMap(ds_regi, 0);
	    
	    adminStatProcDao.insertMailMgntList(dsMap);
	}
	
	public void isCheckMailMgntList() throws Exception{
		
		Dataset ds_list = getDataset("ds_regi");     
		Map map = getMap(ds_list, 0);
		
		// 관리자ID체크
		int iUserCnt = adminStatProcDao.isCheckMailMgntList(map); 
		HashMap h = new HashMap();
		h.put("ID_CHECK_CNT", iUserCnt);
		
		// hmap를 dataset로 변환한다.
		addList("ds_condition", h);
		
	}
	*/
	
	
/*
	public void send(Map userMap) throws Exception{
		
		String type = (String) userMap.get("MGNT_DIVS");
		
		String host = Constants.MAIL_SERVER_IP;	//smtp서버
		String from = Constants.SYSTEM_MAIL_ADDRESS;  //발신인 주소 - 시스템 대표 메일
		String fromName = Constants.SYSTEM_NAME;
		String to[] = (String[]) userMap.get("TO");
		String toName[] = (String[]) userMap.get("TO_NAME");
		
		//관리자 정보를 map으로 담고 해당 정보로 셋팅을 해서 보낸다.
	    //String to     =  (String) userMap.get("MAIL");	// 수신인은 관리자 이므로 복수가 나올수 있음 map으로 담음
		//String toName =  (String) userMap.get("USER_NAME"); //수신인 이름 관리자 복수가 나올수 있음 map으로 담음
		
		
		String subject = "";	//메일 제목
		
		MailInfo info = new MailInfo();
		
	    info.setFrom(from);
	    info.setFromName(fromName);
	    info.setHost(host);
	    info.setArrEmail(to);
	    info.setArrEmailName(toName);
	    
		if(to.length > 0) {
		
			if(type.substring(0,2).equals("BO")){//저작권자 조회공고
			    	
				 
				//신청제목
				String worksTitl = (String) userMap.get("WORKS_TITL");
				
				//사용자 정보 셋팅 
				String userIdnt = (String) userMap.get("USER_IDNT");
				String userName = (String) userMap.get("USER_NAME");
				String userEmail = (String) userMap.get("U_MAIL");
				String userSms = (String) userMap.get("MOBL_PHON");
				String mailTitlDivs = (String) userMap.get("MAIL_TITL_DIVS");
				String mailTitl = (String) userMap.get("MAIL_TITL");
				
				subject = mailTitl+mailTitlDivs+"이 완료되었습니다.";
			    
			    StringBuffer sb = new StringBuffer();   
				sb.append("				<tr>");
				sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">");
				sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">"+mailTitlDivs+" 정보</p>");
				sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목) : "+worksTitl+"</li>");
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">"+mailTitlDivs+"자 정보 : "+userName+" (email:"+ userEmail +", mobile: "+userSms+")</li>");				
				//sb.append("						<li style=\"background:url(/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">신청일자 : 0000.00.00</li>");
				sb.append("					</ul>");
				sb.append("					</td>");
				sb.append("				</tr>");
	
			    info.setMessage(sb.toString());
			    info.setSubject(subject);
				info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
			    
				    
			}else if(type.equals("ST")){//법정허락신청
		
				//신청제목
				String worksTitl = (String) userMap.get("WORKS_TITL");
				
				//사용자 정보 셋팅 
				String userIdnt = (String) userMap.get("USER_IDNT");
				String userName = (String) userMap.get("USER_NAME");
				String userEmail = (String) userMap.get("U_MAIL");
				String userSms = (String) userMap.get("MOBL_PHON");
				String mailTitlDivs = (String) userMap.get("MAIL_TITL_DIVS");
				String mailTitl = (String) userMap.get("MAIL_TITL");
				String applyNo = (String) userMap.get("APPLY_NO");
				
				subject = mailTitl;
							
			    StringBuffer sb = new StringBuffer();   
				sb.append("				<tr>");
				sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">");
				sb.append("					<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">신청 정보</p>");
				sb.append("					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">");
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청번호 : "+applyNo+"</li>");
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 제호(제목): "+worksTitl+"</li>");				
				sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 신청자자 정보 : "+userName+" (email:"+ userEmail +", mobile: "+userSms+")</li>");				
				sb.append("					</ul>");
				sb.append("					</td>");
				sb.append("				</tr>");
	
			    info.setMessage(sb.toString());
			    info.setSubject(subject);
				info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
			
			}
			else if(type.equals("STAT_PROC") || type.equals("STATWORK_PROC") || type.equals("STAT_PROC_EXCP")){//상당한노력 자동화 수행결과 안내관련
				
				String mailTitl = (String) userMap.get("MAIL_TITL");
				String mailMent = (String) userMap.get("MAIL_MENT");
				
				int ord[] = (int[]) userMap.get("ORD");
				String yyyymm[] = (String[]) userMap.get("YYYYMM");
				String works_divs_name[] = (String[]) userMap.get("WORKS_DIVS_NAME");
				int rslt_cont_ys[] = (int[]) userMap.get("RSLT_CONT_YS");
				int rslt_cont_no[] = (int[]) userMap.get("RSLT_CONT_NO");
				int targ_works_cont[] = (int[]) userMap.get("TARG_WORKS_CONT");
				int rslt_cont[] = (int[]) userMap.get("RSLT_CONT");
				
				subject = mailTitl+"이 완료되었습니다.";
				
				StringBuffer subSb = new StringBuffer();
				
				if(type.equals("STAT_PROC") && yyyymm!=null){
					subSb.append("						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">    \n");
					subSb.append("							<thead>    \n");
					subSb.append("								<tr>    \n");
					subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"15%\">기준년월</th>    \n");
					subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"10%\">차수</th>    \n");	
					subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">대상저작물</th>    \n");
					subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">상당한노력 수행결과(매칭/미매칭)</th>    \n");
					subSb.append("								</tr>    \n");
					subSb.append("							</thead>    \n");
					subSb.append("							<tbody>    \n");
					
					for(int i=0;i<ord.length;i++){	
						subSb.append("							<tr>    \n");		
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+yyyymm[i]+"</td>    \n");
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+ord[i]+"</td>    \n");
						subSb.append("								<td style=\"padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+works_divs_name[i]+"</td>    \n");
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+rslt_cont_ys[i]+"/"+rslt_cont_no[i]+"</td>    \n");
						subSb.append("							</tr>    \n");
					}
					
					subSb.append("						</tbody>    \n");
					subSb.append("					</table>    \n");
				}else if(type.equals("STATWORK_PROC") && yyyymm!=null){
					subSb.append("						<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:1px solid #d1d1d1; border-collapse:collapse; margin-top:10px; color:#666;\">    \n");
					subSb.append("							<thead>    \n");
					subSb.append("								<tr>    \n");
					subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"15%\">기준년월</th>    \n");
					subSb.append("									<th scope=\"row\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\" width=\"10%\">차수</th>    \n");	
					subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">대상저작물</th>    \n");
					subSb.append("									<th scope=\"col\" style=\"background:#63adf1; color:#fff; padding:5px; border-right:1px solid #c0e1ff;\">전환저작물</th>    \n");
					subSb.append("								</tr>    \n");
					subSb.append("							</thead>    \n");
					subSb.append("							<tbody>    \n");
					
					for(int i=0;i<ord.length;i++){	
						subSb.append("							<tr>    \n");		
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+yyyymm[i]+"</td>    \n");
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+ord[i]+"</td>    \n");
						subSb.append("								<td style=\"padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+targ_works_cont[i]+"</td>    \n");
						subSb.append("								<td style=\"text-align:center; padding:5px; border-right:1px solid #d0d0d0; border-bottom:1px solid #d0d0d0;\">"+rslt_cont[i]+"</td>    \n");
						subSb.append("							</tr>    \n");
					}
					
					subSb.append("						</tbody>    \n");
					subSb.append("					</table>    \n");
				}
				
			    StringBuffer sb = new StringBuffer();   
			    
				sb.append("				<tr>    \n");
				sb.append("					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">    \n");
				sb.append("						<p style=\"font-weight:bold; background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">"+mailTitl+" 정보</p>    \n");
				
				if(mailMent.length()>0) {
					sb.append("						<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">    \n");
					sb.append("							<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">"+mailMent+"</li>    \n");
					sb.append("						</ul>    \n");
				}
				
				sb.append( subSb.toString() );
				
				sb.append("					</td>    \n");
				sb.append("				</tr>    \n");
	
			    info.setMessage(sb.toString());
			    info.setSubject(subject);
				info.setMessage(new String(MailMessage.getChangeInfoMailText2(info)));
			}
			
////////////////////////////////////////////////////////////////////////////////
	
	HashMap insertMap = new HashMap();
	
	insertMap.put("EMAIL_TITLE", info.getSubject());
	insertMap.put("EMAIL_DESC", info.getMessage());
	insertMap.put("EMAIL_ADDR", to[0]);
	adminStatProcDao.insertMailList(insertMap);
	
////////////////////////////////////////////////////////////////////////////////
			MailManager manager = MailManager.getInstance();
			
		    info = manager.sendMailAll(info);
		    	
		    if (info.isSuccess()) {
			System.out.println(">>>>>>>>>>>> 1. message success :"
				+ info.getEmail());
		    } else {
			System.out.println(">>>>>>>>>>>> 1. message false   :"
				+ info.getEmail());
		    }
		}
	}
*/
}

