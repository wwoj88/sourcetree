package kr.or.copyright.mls.adminEventMgnt.model;

import java.util.Arrays;
import java.util.List;

public class MlEventItem {
	
	private String
	event_title,
	item_id,
	item_seqn,
	item_type_cd,
	item_desc,
	max_choice_cnt,
	rgst_idnt,
	rgst_dttm,
	event_id,
	item_choice_id,
	item_choice_seqn,
	item_choice_desc,
	etc_yn,
	user_id,
	corans_yn,
	rslt_desc
	;
	
	List listSub, action1, action2;
	
	String[] item_id_arr,item_type_cd_arr,item_choice_desc_arr,item_choice_etc_yn_arr;

	
	
	public String getEvent_title() {
		return event_title;
	}

	public void setEvent_title(String event_title) {
		this.event_title = event_title;
	}

	public String[] getItem_choice_desc_arr() {
		return item_choice_desc_arr;
	}

	public void setItem_choice_desc_arr(String[] item_choice_desc_arr) {
		this.item_choice_desc_arr = item_choice_desc_arr;
	}

	public String[] getItem_choice_etc_yn_arr() {
		return item_choice_etc_yn_arr;
	}

	public void setItem_choice_etc_yn_arr(String[] item_choice_etc_yn_arr) {
		this.item_choice_etc_yn_arr = item_choice_etc_yn_arr;
	}

	public String getRslt_desc() {
		return rslt_desc;
	}

	public void setRslt_desc(String rslt_desc) {
		this.rslt_desc = rslt_desc;
	}

	public List getAction1() {
		return action1;
	}

	public void setAction1(List action1) {
		this.action1 = action1;
	}

	public List getAction2() {
		return action2;
	}

	public void setAction2(List action2) {
		this.action2 = action2;
	}

	public String getCorans_yn() {
		return corans_yn;
	}

	public void setCorans_yn(String corans_yn) {
		this.corans_yn = corans_yn;
	}

	public String[] getItem_id_arr() {
		return item_id_arr;
	}

	public void setItem_id_arr(String[] item_id_arr) {
		this.item_id_arr = item_id_arr;
	}

	public String[] getItem_type_cd_arr() {
		return item_type_cd_arr;
	}

	public void setItem_type_cd_arr(String[] item_type_cd_arr) {
		this.item_type_cd_arr = item_type_cd_arr;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public List getListSub() {
		return listSub;
	}

	public void setListSub(List listSub) {
		this.listSub = listSub;
	}

	public String getItem_choice_id() {
		return item_choice_id;
	}

	public void setItem_choice_id(String item_choice_id) {
		this.item_choice_id = item_choice_id;
	}

	public String getItem_choice_seqn() {
		return item_choice_seqn;
	}

	public void setItem_choice_seqn(String item_choice_seqn) {
		this.item_choice_seqn = item_choice_seqn;
	}

	public String getItem_choice_desc() {
		return item_choice_desc;
	}

	public void setItem_choice_desc(String item_choice_desc) {
		this.item_choice_desc = item_choice_desc;
	}

	public String getEtc_yn() {
		return etc_yn;
	}

	public void setEtc_yn(String etc_yn) {
		this.etc_yn = etc_yn;
	}

	public String getItem_id() {
		return item_id;
	}

	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}

	public String getItem_seqn() {
		return item_seqn;
	}

	public void setItem_seqn(String item_seqn) {
		this.item_seqn = item_seqn;
	}

	public String getItem_type_cd() {
		return item_type_cd;
	}

	public void setItem_type_cd(String item_type_cd) {
		this.item_type_cd = item_type_cd;
	}

	public String getItem_desc() {
		return item_desc;
	}

	public void setItem_desc(String item_desc) {
		this.item_desc = item_desc;
	}

	public String getMax_choice_cnt() {
		return max_choice_cnt;
	}

	public void setMax_choice_cnt(String max_choice_cnt) {
		this.max_choice_cnt = max_choice_cnt;
	}

	public String getRgst_idnt() {
		return rgst_idnt;
	}

	public void setRgst_idnt(String rgst_idnt) {
		this.rgst_idnt = rgst_idnt;
	}

	public String getRgst_dttm() {
		return rgst_dttm;
	}

	public void setRgst_dttm(String rgst_dttm) {
		this.rgst_dttm = rgst_dttm;
	}

	public String getEvent_id() {
		return event_id;
	}

	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}

	@Override
	public String toString() {
		return "MlEventItem [item_id=" + item_id + ", item_seqn=" + item_seqn
				+ ", item_type_cd=" + item_type_cd + ", item_desc=" + item_desc
				+ ", max_choice_cnt=" + max_choice_cnt + ", rgst_idnt="
				+ rgst_idnt + ", rgst_dttm=" + rgst_dttm + ", event_id="
				+ event_id + ", item_choice_id=" + item_choice_id
				+ ", item_choice_seqn=" + item_choice_seqn
				+ ", item_choice_desc=" + item_choice_desc + ", etc_yn="
				+ etc_yn + ", user_id=" + user_id + ", corans_yn=" + corans_yn
				+ ", rslt_desc=" + rslt_desc + ", listSub=" + listSub
				+ ", action1=" + action1 + ", action2=" + action2
				+ ", item_id_arr=" + Arrays.toString(item_id_arr)
				+ ", item_type_cd_arr=" + Arrays.toString(item_type_cd_arr)
				+ ", item_choice_desc_arr="
				+ Arrays.toString(item_choice_desc_arr)
				+ ", item_choice_etc_yn_arr="
				+ Arrays.toString(item_choice_etc_yn_arr) + "]";
	}

}
