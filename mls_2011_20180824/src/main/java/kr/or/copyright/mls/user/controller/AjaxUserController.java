package kr.or.copyright.mls.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import kr.or.copyright.mls.ajaxTest.model.AjaxTest;
import kr.or.copyright.mls.ajaxTest.model.Code;
import kr.or.copyright.mls.ajaxTest.service.CodeListService;
import kr.or.copyright.mls.ajaxTest.service.UserListService;
import kr.or.copyright.mls.common.utils.AjaxXmlView;
import kr.or.copyright.mls.user.model.ZipCodeRoad;
import kr.or.copyright.mls.user.service.UserService;
import net.sourceforge.ajaxtags.xml.AjaxXmlBuilder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AjaxUserController{
	
	private static final Log LOG = LogFactory.getLog(AjaxUserController.class);

	@Resource(name="userService")
	private UserService userService;
	
	@RequestMapping("/ajaxGugun/zip.do")
	protected ModelAndView getJsonZip(HttpServletRequest request) throws Exception{
		ModelAndView modelAndView = new ModelAndView("jsonReport");
		Map<String, String> params = new HashMap<String, String>();
		String sidoNm = ServletRequestUtils.getStringParameter(request, "sidoNm");
		params.put("sidoNm", sidoNm);
		List<ZipCodeRoad> codeList = userService.getRoadCodeList(params);
		modelAndView.addObject("codeList",codeList);
		return modelAndView;
	}
}