package kr.or.copyright.mls.event.model;

import kr.or.copyright.mls.common.common.utils.Constants;

public class QnABoardVO {
	
	private int rnum;
	
	private String
	bord_seqn
	,menu_seqn
	,tite
	,pswd
	,bord_desc
	,inqr_cont
	,mail
	,deth
	,delt_ysno
	,html_ysno
	,rgst_idnt
	,rgst_dttm
	,modi_idnt
	,modi_dttm
	,threaded
	,rgst_kind_code
	,rgst_name
	,modi_name
	,url
	,open_ymd
	,end_ymd
	,bord_desc_tag
	,answ_desc
	,answ_desc_tag
	,open_ysno
	,answ_rgst_idnt
	,answ_rgst_dttm;
	
	//	����¡
	private int nowPage = 1;  
	private int startRow = 1; 
	private int endRow = 10; 
	private int totalRow = 0;
	private int rowNo;

	
	public int getRnum() {
		return rnum;
	}
	public void setRnum(int rnum) {
		this.rnum = rnum;
	}
	public String getBord_seqn() {
		return bord_seqn;
	}
	public void setBord_seqn(String bord_seqn) {
		this.bord_seqn = bord_seqn;
	}
	public String getMenu_seqn() {
		return menu_seqn;
	}
	public void setMenu_seqn(String menu_seqn) {
		this.menu_seqn = menu_seqn;
	}
	public String getTite() {
		return tite;
	}
	public void setTite(String tite) {
		this.tite = tite;
	}
	public String getPswd() {
		return pswd;
	}
	public void setPswd(String pswd) {
		this.pswd = pswd;
	}
	public String getBord_desc() {
		return bord_desc;
	}
	public void setBord_desc(String bord_desc) {
		this.bord_desc = bord_desc;
	}
	public String getInqr_cont() {
		return inqr_cont;
	}
	public void setInqr_cont(String inqr_cont) {
		this.inqr_cont = inqr_cont;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getDeth() {
		return deth;
	}
	public void setDeth(String deth) {
		this.deth = deth;
	}
	public String getDelt_ysno() {
		return delt_ysno;
	}
	public void setDelt_ysno(String delt_ysno) {
		this.delt_ysno = delt_ysno;
	}
	public String getHtml_ysno() {
		return html_ysno;
	}
	public void setHtml_ysno(String html_ysno) {
		this.html_ysno = html_ysno;
	}
	public String getRgst_idnt() {
		return rgst_idnt;
	}
	public void setRgst_idnt(String rgst_idnt) {
		this.rgst_idnt = rgst_idnt;
	}
	public String getRgst_dttm() {
		return rgst_dttm;
	}
	public void setRgst_dttm(String rgst_dttm) {
		this.rgst_dttm = rgst_dttm;
	}
	public String getModi_idnt() {
		return modi_idnt;
	}
	public void setModi_idnt(String modi_idnt) {
		this.modi_idnt = modi_idnt;
	}
	public String getModi_dttm() {
		return modi_dttm;
	}
	public void setModi_dttm(String modi_dttm) {
		this.modi_dttm = modi_dttm;
	}
	public String getThreaded() {
		return threaded;
	}
	public void setThreaded(String threaded) {
		this.threaded = threaded;
	}
	public String getRgst_kind_code() {
		return rgst_kind_code;
	}
	public void setRgst_kind_code(String rgst_kind_code) {
		this.rgst_kind_code = rgst_kind_code;
	}
	public String getRgst_name() {
		return rgst_name;
	}
	public void setRgst_name(String rgst_name) {
		this.rgst_name = rgst_name;
	}
	public String getModi_name() {
		return modi_name;
	}
	public void setModi_name(String modi_name) {
		this.modi_name = modi_name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getOpen_ymd() {
		return open_ymd;
	}
	public void setOpen_ymd(String open_ymd) {
		this.open_ymd = open_ymd;
	}
	public String getEnd_ymd() {
		return end_ymd;
	}
	public void setEnd_ymd(String end_ymd) {
		this.end_ymd = end_ymd;
	}
	public String getBord_desc_tag() {
		return bord_desc_tag;
	}
	public void setBord_desc_tag(String bord_desc_tag) {
		this.bord_desc_tag = bord_desc_tag;
	}
	public String getAnsw_desc() {
		return answ_desc;
	}
	public void setAnsw_desc(String answ_desc) {
		this.answ_desc = answ_desc;
	}
	public String getAnsw_desc_tag() {
		return answ_desc_tag;
	}
	public void setAnsw_desc_tag(String answ_desc_tag) {
		this.answ_desc_tag = answ_desc_tag;
	}
	public String getOpen_ysno() {
		return open_ysno;
	}
	public void setOpen_ysno(String open_ysno) {
		this.open_ysno = open_ysno;
	}
	public String getAnsw_rgst_idnt() {
		return answ_rgst_idnt;
	}
	public void setAnsw_rgst_idnt(String answ_rgst_idnt) {
		this.answ_rgst_idnt = answ_rgst_idnt;
	}
	public String getAnsw_rgst_dttm() {
		return answ_rgst_dttm;
	}
	public void setAnsw_rgst_dttm(String answ_rgst_dttm) {
		this.answ_rgst_dttm = answ_rgst_dttm;
	}
	public int getNowPage() {
		return nowPage;
	}
	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
		this.startRow = Constants.DEFAULT_ROW_PER_PAGE * (nowPage - 1) + 1;
		this.endRow = Constants.DEFAULT_ROW_PER_PAGE * nowPage;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
	public int getTotalRow() {
		return totalRow;
	}
	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}
	public int getRowNo() {
		return rowNo;
	}
	public void setRowNo(int rowNo) {
		this.rowNo = rowNo;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("QnABoardVO [rnum=");
		builder.append(rnum);
		builder.append(", bord_seqn=");
		builder.append(bord_seqn);
		builder.append(", menu_seqn=");
		builder.append(menu_seqn);
		builder.append(", tite=");
		builder.append(tite);
		builder.append(", pswd=");
		builder.append(pswd);
		builder.append(", bord_desc=");
		builder.append(bord_desc);
		builder.append(", inqr_cont=");
		builder.append(inqr_cont);
		builder.append(", mail=");
		builder.append(mail);
		builder.append(", deth=");
		builder.append(deth);
		builder.append(", delt_ysno=");
		builder.append(delt_ysno);
		builder.append(", html_ysno=");
		builder.append(html_ysno);
		builder.append(", rgst_idnt=");
		builder.append(rgst_idnt);
		builder.append(", rgst_dttm=");
		builder.append(rgst_dttm);
		builder.append(", modi_idnt=");
		builder.append(modi_idnt);
		builder.append(", modi_dttm=");
		builder.append(modi_dttm);
		builder.append(", threaded=");
		builder.append(threaded);
		builder.append(", rgst_kind_code=");
		builder.append(rgst_kind_code);
		builder.append(", rgst_name=");
		builder.append(rgst_name);
		builder.append(", modi_name=");
		builder.append(modi_name);
		builder.append(", url=");
		builder.append(url);
		builder.append(", open_ymd=");
		builder.append(open_ymd);
		builder.append(", end_ymd=");
		builder.append(end_ymd);
		builder.append(", bord_desc_tag=");
		builder.append(bord_desc_tag);
		builder.append(", answ_desc=");
		builder.append(answ_desc);
		builder.append(", answ_desc_tag=");
		builder.append(answ_desc_tag);
		builder.append(", open_ysno=");
		builder.append(open_ysno);
		builder.append(", answ_rgst_idnt=");
		builder.append(answ_rgst_idnt);
		builder.append(", answ_rgst_dttm=");
		builder.append(answ_rgst_dttm);
		builder.append(", nowPage=");
		builder.append(nowPage);
		builder.append(", startRow=");
		builder.append(startRow);
		builder.append(", endRow=");
		builder.append(endRow);
		builder.append(", totalRow=");
		builder.append(totalRow);
		builder.append(", rowNo=");
		builder.append(rowNo);
		builder.append("]");
		return builder.toString();
	}
}
