package kr.or.copyright.mls.console.notdstbmanage.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd74Service{

	/**
	 * 미분배 보상금 관리 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd74List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 방송음악 보상금 등록현황 등록
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd74Insert1( Map<String, Object> commandMap ) throws Exception;

}
