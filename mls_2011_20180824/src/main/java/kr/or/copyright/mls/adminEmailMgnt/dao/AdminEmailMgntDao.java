package kr.or.copyright.mls.adminEmailMgnt.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.tobesoft.platform.data.Dataset;

public interface AdminEmailMgntDao {
	
	public List selectEmailAddrGroupList(Map map);
	
	public List selectEmailAddrList(Map map);
	
	public int selectMaxMsgIdSeq();
	
	public void insertEmailMsg(Map map);
	
	public void insertEmailRecvInfo(Map map);
	
	public List selectEmailMsgList (Map map);
	
	public List selectEmailMsg (Map map);
	
	public List selectEmailAddrListByMsgId (Map map);
	
	public void updateEmailMsg(Map map);
	
	public boolean deleteEmailRecvInfo(Map map) throws SQLException;
	
	public void deleteEmailMsg(Map map);
	
	public void insertSendList(Map map);
	
	public List selectSendEmailList();
	
	public List selectSendEmailView(Map map);
	
	public void insertSuplList(Map map);

	public List selectScMailingList(Map map);
	
	public List selectScMailingMsgId(Map map);
	
	public void insertScMailing(Map map);
	
	public int selectScMailingCount(Map map);
	
	public void updateScMailng(Map map);
	
	public List selectScMailing(Map map);
	
	public void updateSendScMailng(Map map);
	
	public void insertMailAttc(Map map);
	
	public List selectMailAttcList(Map map);
	
	public void deleteMailAttc(Map map);
	
	public void updateEndDttmMsg(Map map);
	
}
