package kr.or.copyright.mls.rghtPrps.dao;

import java.util.List;

import kr.or.copyright.mls.rghtPrps.model.PrpsAttc;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class RghtPrpsSqlMapDao extends SqlMapClientDaoSupport implements RghtPrpsDao{
	
	//private Log logger = LogFactory.getLog(getClass());
	
	// get 미리듣기파일
	public String getMuscRecFile(RghtPrps rghtPrps) {
		return (String)getSqlMapClientTemplate().queryForObject("RghtPrps.getMuscRecFile", rghtPrps);
	}
	
	// 음악 권리찾기신청 저작물목록
	public List muscPrpsList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.muscPrpsList", rghtPrps);
	}
	
	// 음악목록 건수
	public int muscRghtCount(RghtPrps rghtPrps) {
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("RghtPrps.muscRghtCount", rghtPrps));
	}
	
	// 음악목록
	public List muscRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.muscRghtList", rghtPrps);
	}
	
	// 음악상세
	public RghtPrps muscRghtDetail(RghtPrps rghtPrps) {
		return (RghtPrps)getSqlMapClientTemplate().queryForObject("RghtPrps.muscRghtDetail", rghtPrps);
	}
	
	// 어문 권리찾기신청 저작물목록
	public List bookPrpsList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.bookPrpsList", rghtPrps);
	}
		
	// 어문목록 건수
	public int bookRghtCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("RghtPrps.bookRghtCount", rghtPrps));
	}
	
	// 어문 목록
	public List bookRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.bookRghtList", rghtPrps);
	}
	
	// 어문상세
	public RghtPrps bookRghtDetail(RghtPrps rghtPrps) {
		return (RghtPrps)getSqlMapClientTemplate().queryForObject("RghtPrps.bookRghtDetail", rghtPrps);
	}
	
	// 방송대본목록
	public List scriptRghtList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RghtPrps.scriptRghtList", rghtPrps);
	}
	
	// 방송대본목록 건수
	public int scriptRghtCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("RghtPrps.scriptRghtCount", rghtPrps));
	}
	
	// 방송대본 상세 
	public RghtPrps scriptRghtDetail(RghtPrps rghtPrps){
		return (RghtPrps)getSqlMapClientTemplate().queryForObject("RghtPrps.scriptRghtDetail", rghtPrps);
	}
	
	// 방송대본 권리찾기신청 저작물 목록
	public List scriptPrpsList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RghtPrps.scriptPrpsList", rghtPrps);
	}
	
	// 이미지 권리찾기신청 저작물목록
	public List imagePrpsList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.imagePrpsList", rghtPrps);
	}
	
	// 이미지목록 건수
	public int imageRghtCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("RghtPrps.imageRghtCount", rghtPrps));
	}
	
	// 이미지 목록
	public List imageRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.imageRghtList", rghtPrps);
	}
	
	// 영화 권리찾기신청 저작물목록
	public List mviePrpsList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.mviePrpsList", rghtPrps);
	}
	
	// 영화목록 건수
	public int mvieRghtCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("RghtPrps.mvieRghtCount", rghtPrps));
	}
	
	// 영화 목록
	public List mvieRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.mvieRghtList", rghtPrps);
	}
	
	// 영화상세
	public RghtPrps mvieRghtDetail(RghtPrps rghtPrps) {
		return (RghtPrps)getSqlMapClientTemplate().queryForObject("RghtPrps.mvieRghtDetail", rghtPrps);
	}
	
	// 방송목록 건수
	public int broadcastRghtCount(RghtPrps rghtPrps){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("RghtPrps.broadcastRghtCount", rghtPrps));
	}
	
	// 방송목록
	public List broadcastRghtList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RghtPrps.broadcastRghtList", rghtPrps);
	}
	
	// 방송상세
	public RghtPrps broadcastRghtDetail(RghtPrps rghtPrps) {
		return (RghtPrps)getSqlMapClientTemplate().queryForObject("RghtPrps.broadcastRghtDetail", rghtPrps);
	}
	
	// 저작물별 권리찾기신청내역 List
	public List rghtPrpsHistList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.rghtPrpsHistList", rghtPrps);
	}
	
	// 방송 권리찾기신청 저작물목록
	public List broadcastPrpsList(RghtPrps rghtPrps){
		return getSqlMapClientTemplate().queryForList("RghtPrps.broadcastPrpsList", rghtPrps);
	}
	
	
//	 뉴스 권리찾기신청 저작물목록
	public List newsPrpsList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.newsPrpsList", rghtPrps);
	}
	
	//  뉴스목록 건수
	public int newsRghtCount(RghtPrps rghtPrps) {
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("RghtPrps.newsRghtCount", rghtPrps));
	}
	
	//  뉴스목록
	public List newsRghtList(RghtPrps rghtPrps) {
		return getSqlMapClientTemplate().queryForList("RghtPrps.newsRghtList", rghtPrps);
	}
	
	//  뉴스상세
	public RghtPrps newsRghtDetail(RghtPrps rghtPrps) {
		return (RghtPrps)getSqlMapClientTemplate().queryForObject("RghtPrps.newsRghtDetail", rghtPrps);
	}
	
	// get 권리찾기신청 masterkey max+1
	public int prpsMastKey() {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("RghtPrps.prpsMastKey",null);
		return max.intValue(); 
	}
	
	// 권리찾기신청 마스터테이블 insert
	public void prpsMasterInsert(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().insert("RghtPrps.prpsMasterInsert",rghtPrps);
	}	
	
	// 권리찾기신청 마스터테이블 update
	public void prpsMasterUpdate(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().insert("RghtPrps.prpsMasterUpdate",rghtPrps);
	}
	
	// 권리찾기신청 기관별처리결과 기본 insert
	public void prpsRsltInsert(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().insert("RghtPrps.prpsRsltInsert",rghtPrps);
	}	
	
	// 권리찾기신청 기관별 관리권리 insert
	public void prpsRsltRghtInsert(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().insert("RghtPrps.prpsRsltRghtInsert",rghtPrps);
	}
	
	// 추가등록 저작물 get CR_ID max+1
	public int getCrId() {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("RghtPrps.getCrId",null);
		return max.intValue(); 
	}
	
	// 추가등록 저작물 get NR_ID max+1
	public int getNrId(RghtPrps rghtPrps) {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("RghtPrps.getNrId", rghtPrps);
		return max.intValue(); 
	}
	
	// 추가등록 저작물 get ALBUM_ID max+1
	public int getAlbumId() {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("RghtPrps.getAlbumId",null);
		return max.intValue(); 
	}
	
	// 추가등록 저작물 get BOOK_NR_ID max+1
	public int getBookNrId(RghtPrps rghtPrps) {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("RghtPrps.getBookNrId", rghtPrps);
		return max.intValue(); 
	}
	
	// 추가등록 저작물 get MEDIA_ID max+1
	public int getMovieMediaId(RghtPrps rghtPrps) {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("RghtPrps.getMovieMediaId", rghtPrps);
		return max.intValue(); 
	}
	
	// 저작물 추가등록
	public void worksInsert(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().insert("RghtPrps.worksInsert",rghtPrps);
	}
	
	// 음원저작물 추가등록
	public void musicInsert(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().insert("RghtPrps.musicInsert",rghtPrps);
	}
	
	// 앨범저작물 추가등록
	public void albumInsert(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().insert("RghtPrps.albumInsert",rghtPrps);
	}
	
	// 도서저작물 추가등록
	public void bookInsert(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().insert("RghtPrps.bookInsert",rghtPrps);
	}
	
	// 방송대본저작물 추가등록
	public void scriptInsert(RghtPrps rghtPrps){
		getSqlMapClientTemplate().insert("RghtPrps.scriptInsert",rghtPrps);
	}
	
	// 이미지저작물 추가등록
	public void imageInsert(RghtPrps rghtPrps){
		getSqlMapClientTemplate().insert("RghtPrps.imageInsert",rghtPrps);
	}
	
	// 영화저작물 추가등록
	public void movieInsert(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().insert("RghtPrps.movieInsert",rghtPrps);
	}
	
	// 영화매체저작물 추가등록
	public void movieMediaInsert(RghtPrps rghtPrps) {
		getSqlMapClientTemplate().insert("RghtPrps.movieMediaInsert",rghtPrps);
	}
	
	// 방송저작물 추가등록
	public void broadcastInsert(RghtPrps rghtPrps){
		getSqlMapClientTemplate().insert("RghtPrps.broadcastInsert",rghtPrps);
	}
	
	public void insertPrpsAttc(PrpsAttc prpsAttc) {
		getSqlMapClientTemplate().insert("RghtPrps.insertPrpsAttc", prpsAttc);
	}
	
	// 기타저작물 추가등록
	public void sideInsert(RghtPrps rghtPrps){
		getSqlMapClientTemplate().insert("RghtPrps.sideInsert",rghtPrps);
	}
	
	// 뉴스저작물 추가등록
	public void newsInsert(RghtPrps rghtPrps){
		getSqlMapClientTemplate().insert("RghtPrps.newsInsert",rghtPrps);
	}
}
