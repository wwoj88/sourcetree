package kr.or.copyright.mls.rslt.service;

public interface RsltService {

	public void prpsRsltList() throws Exception;
	
	public void prpsRsltDelete() throws Exception;
	
}
