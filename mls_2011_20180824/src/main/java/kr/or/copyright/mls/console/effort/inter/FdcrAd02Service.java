package kr.or.copyright.mls.console.effort.inter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface FdcrAd02Service{

	/**
	 * 상당한 노력 신청 접수 - 목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd02List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한 노력 신청 접수 - 상세조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd02UpdateForm1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한 노력 신청 접수 - 저작권자 조회 공고내용 보완
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd02UpdateForm2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 상당한노력신청 - 저작권자 조회 공고내용 수정 및 보완이력 등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd02Update2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 진행상태 내역
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd02List2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 진행상태 저장
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd02Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

	/**
	 * 상당한노력신청 - 저작권자 조회공고 삭제
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd02Delete1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 첨부파일 조회
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public List fdcrAd02List3( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 상세조회
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public List fdcrAd02List4( Map<String, Object> commandMap ) throws Exception;
}
