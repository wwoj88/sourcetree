package kr.or.copyright.mls.srch.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.extend.RightDAOImpl;

import org.springframework.stereotype.Repository;


@Repository("SrchDao")
public class SrchSqlMapDao extends RightDAOImpl implements SrchDao{
	
	public List codeList(String srchDivs){
		return (List) getSqlMapClientTemplate().queryForList("Srch.codeList", srchDivs);
	}
	//법정허락
	public List findStatWorks(Map params){
		return (List) getSqlMapClientTemplate().queryForList("Srch.findStatWorks", params);
	}
	public int countStatWorks(Map params){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("Srch.countStatWorks", params));
	}
	public List findStatDetl(Map params){
		return (List) getSqlMapClientTemplate().queryForList("Srch.findStatDetl", params);
	}
	public List getCoptHodr(Map params){
		return (List) getSqlMapClientTemplate().queryForList("Srch.getCoptHodr", params);
	}
	//위탁관리 저작물
	public List findCommWorks(Map params){
		return (List) getSqlMapClientTemplate().queryForList("Srch.findCommWorks", params);
	}
	public List findCommDetl(Map params){
		return (List) getSqlMapClientTemplate().queryForList("Srch.findCommDetl", params);
	}
	public int countCommWorks(Map params){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("Srch.countCommWorks", params));
	}
	//법정허락등록부
	public List findCrosWorks(Map params){
		return (List) getSqlMapClientTemplate().queryForList("Srch.findCrosWorks", params);
	}
	public int countCrosWorks(Map params){
		return Integer.parseInt(""+getSqlMapClientTemplate().queryForObject("Srch.countCrosWorks", params));
	}
	//권리인증
	public List findCertWorks(Map params){
		return (List) getSqlMapClientTemplate().queryForList("Srch.findCertWorks", params);
	}
	public int countFindCertWorks(Map params){
		return (Integer) getSqlMapClientTemplate().queryForObject("Srch.countFindCertWorks", params);
	}
	
	public void insertSrch(String worksTitle) {
		getSqlMapClientTemplate().insert("Srch.insertSrch", worksTitle);
	}
	public List getSearchWordList() throws Exception{
		return getSqlMapClientTemplate().queryForList("Srch.getSearchWordList");
	}
}
