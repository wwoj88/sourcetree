package kr.or.copyright.mls.console.email;

import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.email.inter.FdcrAdA4Dao;
import kr.or.copyright.mls.console.email.inter.FdcrAdA4Service;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.StringUtil;

import org.springframework.stereotype.Service;

import com.tobesoft.platform.data.Dataset;

@Service( "fdcrAdA4Service" )
public class FdcrAdA4ServiceImpl extends CommandService implements FdcrAdA4Service{

	@Resource( name = "fdcrAdA4Dao" )
	private FdcrAdA4Dao fdcrAdA4Dao;

	/**
	 * 주소록 불러오기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA4List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list2 = (List) fdcrAdA4Dao.selectEmailAddrGroupList( commandMap );
		List list = (List) fdcrAdA4Dao.selectEmailAddrList( commandMap );

		commandMap.put( "ds_list", list );
		commandMap.put( "ds_menu", list2 );

	}

	/**
	 * 전체주소록 불러오기
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA4List2( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		String[] RECV_GROUP_IDS = (String[]) commandMap.get( "RECV_GROUP_ID" );
		String[] RECV_REJC_YNS = (String[]) commandMap.get( "RECV_REJC_YN" );
		List list = new ArrayList();

		for( int i = 0; i < RECV_GROUP_IDS.length; i++ ){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put( "RECV_GROUP_ID", RECV_GROUP_IDS[i] );
			map.put( "RECV_REJC_YN", RECV_REJC_YNS[i] );
			List templist = (List) fdcrAdA4Dao.selectEmailAddrList( map );
			list.addAll( templist );
		}
		commandMap.put( "ds_all_send", list );
	}

	/**
	 * 메일 저장하기
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA4Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			
			/*String[] MSG_IDS = (String[]) commandMap.get( "MSG_ID" );
			String[] RECV_CDS = (String[]) commandMap.get( "RECV_CD" );
			String[] RECV_SEQNS = (String[]) commandMap.get( "RECV_SEQN" );
			String[] RECV_NAMES = (String[]) commandMap.get( "RECV_NAME" );
			String[] RECV_PERS_IDS = (String[]) commandMap.get( "RECV_PERS_ID" );
			String[] RECV_MAIL_ADDRS = (String[]) commandMap.get( "RECV_MAIL_ADDR" );
			String[] RECV_GROUP_IDS = (String[]) commandMap.get( "RECV_GROUP_ID" );
			String[] RECV_GROUP_NAMES = (String[]) commandMap.get( "RECV_GROUP_NAME" );
			String[] FILE_NAMES = (String[]) commandMap.get( "FILE_NAME" );
			String[] FILE_PATHS = (String[]) commandMap.get( "FILE_PATH" );
			String[] FILE_SIZES = (String[]) commandMap.get( "FILE_SIZE" );
			String[] RGST_IDNTS = (String[]) commandMap.get( "RGST_IDNT" );*/

			String row_status;
			try{

				int msgIdSeq = fdcrAdA4Dao.selectMaxMsgIdSeq();
				commandMap.put( "MSG_ID", msgIdSeq );
				fdcrAdA4Dao.insertEmailMsg( commandMap );
				
				String[] SEND_MAIL = null;
				String[] REF_MAIL = ((String)commandMap.get( "REF_MAIL" )).split( "," ); 
					
				if(commandMap.get( "SEND_MAIL" ) instanceof String[]){
					SEND_MAIL = (String[]) commandMap.get( "SEND_MAIL" );
				} else {
					SEND_MAIL = new String[1];
					SEND_MAIL[0] = (String) commandMap.get( "SEND_MAIL" );
				}
				
				// detail INSERT, UPDATE처리
				/*for( int i = 0; i < SEND_MAIL.length; i++ ){
					Map<String, Object> fileMap = new HashMap<String, Object>();

					byte[] file = (byte[]) commandMap.get( "CLIENT_FILE" );
					String fileName = (String) commandMap.get( "REAL_FILE_NAME" );

					Map upload = FileUtil.uploadMiFile( fileName, file, "mail/" );

					fileMap.put( "MSG_ID", msgIdSeq );
					fileMap.put( "REAL_FILE_NAME", upload.get( "REAL_FILE_NAME" ) );
					fileMap.put( "FILE_PATH", upload.get( "FILE_PATH" ) );
					fileMap.put( "FILE_NAME", fileName );

					fdcrAdA4Dao.insertMailAttc( fileMap );
				}*/

				String[] _send = null;
				for( int i = 0; i < SEND_MAIL.length; i++ ){
					
					_send = SEND_MAIL[i].split( "/" );
					
					Map<String, Object> map2 = new HashMap<String, Object>();
					map2.put( "MSG_ID", msgIdSeq );
					map2.put( "RECV_SEQN", i + 1 );
					map2.put( "RECV_CD", 1 );
					
					map2.put( "RECV_NAME", _send[2] );
					map2.put( "RECV_PERS_ID", _send[3] );
					map2.put( "RECV_MAIL_ADDR", _send[5] );
					map2.put( "RECV_GROUP_ID", _send[4] );
					map2.put( "RECV_GROUP_NAME", _send[6] );
					fdcrAdA4Dao.insertEmailRecvInfo( map2 );
				}
				
				for( int j = 0; j < REF_MAIL.length; j++ ){
					Map<String, Object> map3 = new HashMap<String, Object>();
					map3.put( "MSG_ID", msgIdSeq );
					map3.put( "RECV_SEQN", j + 1 );
					map3.put( "RECV_CD", 2 );
					map3.put( "RECV_MAIL_ADDR", REF_MAIL[j] );
					fdcrAdA4Dao.insertEmailRecvInfo( map3 );
				}

			}
			catch( Exception e ){
				e.printStackTrace();
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 메일 보내기
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAdA4Insert2( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String[] MSG_IDS = (String[]) commandMap.get( "MSG_ID" );
			String[] RECV_CDS = (String[]) commandMap.get( "RECV_CD" );
			String[] RECV_SEQNS = (String[]) commandMap.get( "RECV_SEQN" );
			String[] RECV_NAMES = (String[]) commandMap.get( "RECV_NAME" );
			String[] RECV_PERS_IDS = (String[]) commandMap.get( "RECV_PERS_ID" );
			String[] RECV_MAIL_ADDRS = (String[]) commandMap.get( "RECV_MAIL_ADDR" );
			String[] RECV_GROUP_IDS = (String[]) commandMap.get( "RECV_GROUP_ID" );
			String[] RECV_GROUP_NAMES = (String[]) commandMap.get( "RECV_GROUP_NAME" );
			String[] FILE_NAMES = (String[]) commandMap.get( "FILE_NAME" );
			String[] FILE_PATHS = (String[]) commandMap.get( "FILE_PATH" );
			String[] FILE_SIZES = (String[]) commandMap.get( "FILE_SIZE" );
			String[] RGST_IDNTS = (String[]) commandMap.get( "RGST_IDNT" );

			String[] RECV_COMM_NAMES = (String[]) commandMap.get( "RECV_COMM_NAME" );
			String[] RECV_USER_NAMES = (String[]) commandMap.get( "RECV_USER_NAME" );
			String[] RECV_REJC_YNS = (String[]) commandMap.get( "RECV_REJC_YN" );
			String[] FROM_MLS_YNS = (String[]) commandMap.get( "FROM_MLS_YN" );
			String[] GROUP_ID_PARENTS = (String[]) commandMap.get( "GROUP_ID_PARENT" );
			String[] EXPR_EMAILS = (String[]) commandMap.get( "EXPR_EMAIL" );

			Map<String, Object> mailMap = new HashMap<String, Object>();

			List<Map> sendList = new ArrayList();
			// List<Map> fileList = new ArrayList();
			String row_status;
			// 첨부파일 insert
			for( int i = 0; i < FILE_NAMES.length; i++ ){

				// row_status = ds_detail.getRowStatus( i );
				row_status = "";
				Map<String, Object> fileMap = new HashMap<String, Object>();
				if( row_status.equals( "insert" ) == true ){
					// 메일보내기 폼에서 바로 파일첨부를 해서 전송하는 경우
					byte[] file = (byte[]) commandMap.get( "CLIENT_FILE" );

					String fileName = commandMap.get( "REAL_FILE_NAME" ).toString();

					Map upload = FileUtil.uploadMiFile( fileName, file, "mail/" );

					fileMap.put( "REAL_FILE_NAME", upload.get( "REAL_FILE_NAME" ) );
					fileMap.put( "FILE_PATH", upload.get( "FILE_PATH" ) );
					fileMap.put( "FILE_NAME", fileName );

					fileList.add( fileMap );
				}else if( row_status.equals( "normal" ) == true ){
					// 메일 저장함에 있던 파일을 목록으로 불러와서 전송하는 경우, 해당 파일을 복사해서 다시 넣어준다.
					String fileFullPath = (String) fileMap.get( "FILE_PATH" ) + (String) fileMap.get( "REAL_FILE_NAME" );
					FileInputStream in = new FileInputStream( fileFullPath );
					int iSize = (int) Double.parseDouble( String.valueOf( fileMap.get( "FILE_SIZE" ) ) );
					byte[] file = new byte[iSize];
					int count = 0;
					while( ( count = in.read( file ) ) != -1 ){

					}

					String fileName = commandMap.get( "REAL_FILE_NAME" ).toString();

					Map upload = FileUtil.uploadMiFile( fileName, file, "mail/" );

					fileMap.put( "REAL_FILE_NAME", upload.get( "REAL_FILE_NAME" ) );
					fileMap.put( "FILE_PATH", upload.get( "FILE_PATH" ) );

					fileList.add( fileMap );
				}

			}
			if( fileList.size() != 0 ){
				mailMap.put( "fileList", fileList );
			}

			StringBuffer sb = new StringBuffer();
			sb.append( "				<tr> \n" );
			sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\"> \n" );
			sb.append( "					<p style=\"margin:10px 0 0 0;\">"
				+ StringUtil.appendHtmlBr( (String) mailMap.get( "CONTENT" ) ) + "</p>	\n" );
			sb.append( "					</td>	\n" );
			sb.append( "				</tr>	\n" );

			// 첨부파일 다운로드 태그 생성
			if( fileList.size() != 0 ){
				sb.append( "				<tr>	\n" );
				sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px 26px 35px;\">	\n" );
				sb.append( "					<p style=\"font-weight:bold; background:url("
					+ Constants.DOMAIN_HOME
					+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\">첨부파일</p>	\n" );
				sb.append( "					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">" );

				for( int i = 0; i < fileList.size(); i++ ){
					sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME
						+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> " + "<a href=\""
						+ Constants.DOMAIN_HOME + "/board/board.page?method=fileDownLoad&filePath="
						+ fileList.get( i ).get( "FILE_PATH" ) + "&fileName=" + fileList.get( i ).get( "FILE_NAME" )
						+ "&realFileName=" + fileList.get( i ).get( "REAL_FILE_NAME" ) + "\">"
						+ fileList.get( i ).get( "FILE_NAME" ) + "</a></li>	\n" );
				}
				sb.append( "					</ul>	\n" );
				sb.append( "					</td>	\n" );
				sb.append( "				</tr>	\n" );

			}

			String contentHtml = new String( sb );

			mailMap.put( "CONTENT_HTML", contentHtml );
			mailMap.put( "MAIL_REJC_CD", "1" );// 수신거부코드

			for( int i = 0; i < RECV_CDS.length; i++ ){
				Map<String, Object> sendMap = new HashMap<String, Object>();
				sendMap.put( "RECV_COMM_NAME", RECV_COMM_NAMES[i] );
				sendMap.put( "RECV_USER_NAME", RECV_USER_NAMES[i] );
				sendMap.put( "RECV_PERS_ID", RECV_PERS_IDS[i] );
				sendMap.put( "RECV_MAIL_ADDR", RECV_MAIL_ADDRS[i] );
				sendMap.put( "RECV_GROUP_ID", RECV_GROUP_IDS[i] );
				sendMap.put( "RECV_GROUP_NAME", RECV_GROUP_NAMES[i] );
				sendMap.put( "RECV_REJC_YN", RECV_REJC_YNS[i] );
				sendMap.put( "FROM_MLS_YN", FROM_MLS_YNS[i] );
				sendMap.put( "RECV_NAME", RECV_NAMES[i] );
				sendMap.put( "RECV_CD", RECV_CDS[i] );
				sendMap.put( "GROUP_ID_PARENT", GROUP_ID_PARENTS[i] );
				sendMap.put( "EXPR_EMAIL", EXPR_EMAILS[i] );

				sendList.add( sendMap );
			}

			for( int j = 0; j < RECV_CDS.length; j++ ){
				Map<String, Object> map3 = new HashMap<String, Object>();
				map3.put( "RECV_CD", RECV_CDS[j] );
				map3.put( "RECV_MAIL_ADDR", RECV_MAIL_ADDRS[j] );
				sendList.add( map3 );
			}
			sendEmail( sendList, mailMap );
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/*
	 * mailMap 필요 컬럼 MSG_STOR_CD - 메세지함 구분코드 TITLE - 메일제목 CONTENT - 메일내용 txt
	 * CONTENT_HTML - 메일내용 HTML RGST_IDNT - 발신자 아이디 SEND_MAIL_ADDR - 발신자 메일주소
	 * SUPL_ID - 보완내역 발신시에만 필요 sendList Map 필요 컬럼 RECV_CD - 수신구분코드
	 * RECV_MAIL_ADDR - 수신자 메일 주소 RECV_NAME - 수신자 이름 SEND_MAIL_ADDR - 발신자
	 * 메일주소(mailMap에서 얻음)
	 */
	public int sendEmail( List<Map> sendList,
		Map mailMap ) throws Exception{

		String host = Constants.MAIL_SERVER_IP; // smtp서버
		// String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
		String from = StringUtil.nullToEmpty( (String) mailMap.get( "SEND_MAIL_ADDR" ) );
		if( from.equals( "" ) ){
			from = Constants.SYSTEM_MAIL_ADDRESS;
		}
		String fromName = Constants.SYSTEM_NAME;
		String subject = "[저작권찾기] " + (String) mailMap.get( "TITLE" );
		String contentHtml = (String) mailMap.get( "CONTENT_HTML" );
		String mailRejcCd = (String) mailMap.get( "MAIL_REJC_CD" );// 수신거부코드

		int iResult = 0;

		int msgIdSeq = fdcrAdA4Dao.selectMaxMsgIdSeq(); // 메일함 seq
		Date startDate = new Date();
		Date endDate = null;

		// 발신함 메일 저장
		mailMap.put( "MSG_ID", msgIdSeq );
		mailMap.put( "START_DTTM_TRNS", startDate );
		fdcrAdA4Dao.insertEmailMsg( mailMap );

		// 첨부파일 저장

		List<Map> fileList = new ArrayList<Map>();

		if( mailMap.get( "fileList" ) != null ){
			fileList = (List<Map>) mailMap.get( "fileList" );
		}

		for( int i = 0; i < fileList.size(); i++ ){
			Map fileMap = fileList.get( i );
			fileMap.put( "MSG_ID", msgIdSeq );
			fdcrAdA4Dao.insertMailAttc( fileMap );
		}

		try{

			for( int i = 0; i < sendList.size(); i++ ){
				String to = StringUtil.nullToEmpty( (String) sendList.get( i ).get( "RECV_MAIL_ADDR" ) ); // 수신EMAIL
				String toName = StringUtil.nullToEmpty( (String) sendList.get( i ).get( "RECV_NAME" ) ); // 수신인

				MailInfo info = new MailInfo();
				info.setFrom( from );
				info.setFromName( fromName );
				info.setHost( host );
				info.setEmail( to );
				info.setEmailName( toName );
				info.setSubject( subject );
				info.setMessage( contentHtml );
				info.setMailRejcCd( mailRejcCd );

				// 메일발신내역 ID 셋팅 = 메세지함ID+메일주소앞부분+일련번호
				String id[] = to.split( "@" );
				String encodeId = "" + msgIdSeq + id[0] + i;

				/*
				 * if(mailMap.get("MSG_STOR_CD").equals("2")){//발신함 저장 메세지만
				 * 메일수신확인소스 삽입 StringBuffer sb = new StringBuffer(); sb.append(
				 * "<img src=\"http://new.right4me.or.kr/mail/updateEmailRecv.page?send_msg_id="
				 * +encodeId+"\" width=\"0\" height=\"0\">");
				 * info.setMailReadHtml(new String(sb)); }
				 */

				StringBuffer sb = new StringBuffer();
				sb.append( "<img src=\"" + Constants.DOMAIN_HOME + "/mail/updateEmailRecv.page?send_msg_id=" + encodeId
					+ "\" width=\"0\" height=\"0\">" );

				info.setMailReadHtml( new String( sb ) );

				sendList.get( i ).put( "MSG_ID", msgIdSeq );
				sendList.get( i ).put( "SEND_MAIL_ADDR", from );
				sendList.get( i ).put( "SEND_MSG_ID", encodeId );
				sendList.get( i ).put( "RECV_SEQN", i + 1 );// 수신자정보 수신자 순번

				info.setMessage( new String( MailMessage.getChangeInfoMailText2( info ) ) );
				MailManager manager = MailManager.getInstance();
				if( i % 100 == 0 && i != 0 ){
					System.out.println( i + "번째 메일입니다. 잠시 후 발송됩니다.\n" );
					try{
						Thread.sleep( 5 * 1000 * 60 );
						info = manager.sendMail( info );
					}
					catch( Exception e ){
						e.printStackTrace();
					}

				}else{
					info = manager.sendMail( info );
				}

				if( info.isSuccess() ){
					System.out.println( ">>>>>>>>>>>> message success :" + info.getEmail() );
					sendList.get( i ).put( "SEND_DTTM", info.getSendDttm() );
				}else{
					System.out.println( ">>>>>>>>>>>> message false   :" + info.getEmail() );
					// 운영에서는 주석처리하세요!
					// sendList.get(i).put("SEND_DTTM", info.getSendDttm());
				}
				// HTML 확인을 위한 임시테이블에 메일전송 정보 넣기(운영,개발서버에서는 주석처리)
				/*
				 * HashMap insertMap = new HashMap();
				 * insertMap.put("EMAIL_TITLE", info.getSubject());
				 * insertMap.put("EMAIL_DESC", info.getMessage());
				 * insertMap.put("EMAIL_ADDR", to);
				 * adminStatProcDao.insertMailList(insertMap);
				 */

			}
			// 발신함 발신완료시간 업데이트
			endDate = new Date();
			mailMap.put( "END_DTTM_TRNS", endDate );
			fdcrAdA4Dao.updateEndDttmMsg( mailMap );

			for( int i = 0; i < sendList.size(); i++ ){
				fdcrAdA4Dao.insertEmailRecvInfo( sendList.get( i ) );// 수신자 정보
																		// 저장
				fdcrAdA4Dao.insertSendList( sendList.get( i ) );// 메일 발신내역 저장
			}

			HashMap insertMap = new HashMap();

			// 보완내역 메일발신내역 등록
			String msgStorCd = StringUtil.nullToEmpty( (String) mailMap.get( "MSG_STOR_CD" ) );
			if( "4".equals( msgStorCd ) || "5".equals( msgStorCd ) || "6".equals( msgStorCd ) ){// 보완내역
																								// 발신함
																								// 코드
				List<Map> suplList = new ArrayList();

				for( int j = 0; j < sendList.size(); j++ ){
					Map suplMap = new HashMap();
					suplMap.put( "SUPL_ID", mailMap.get( "SUPL_ID" ) );
					suplMap.put( "MSG_STOR_CD", msgStorCd );
					suplMap.put( "SEND_MSG_ID", sendList.get( j ).get( "SEND_MSG_ID" ) );
					suplList.add( suplMap );
				}

				for( int k = 0; k < suplList.size(); k++ ){
					fdcrAdA4Dao.insertSuplList( suplList.get( k ) );
				}

			}
			String malingMgntCd = StringUtil.nullToEmpty( (String) mailMap.get( "MAILING_MGNT_CD" ) );
			// 자동메일링 관리 메일발신내역 등록
			if( "1".equals( malingMgntCd ) || "2".equals( malingMgntCd ) ){
				Map map = new HashMap();
				long time = System.currentTimeMillis();
				SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMM" );

				map.put( "MAILING_MGNT_CD", malingMgntCd );
				map.put( "YYYYMM", sdf.format( time ) );
				map.put( "MSG_ID", msgIdSeq );
				map.put( "START_DTTM", startDate );
				map.put( "END_DTTM", endDate );
				fdcrAdA4Dao.updateSendScMailng( map );
			}

			iResult = 1;

		}
		catch( Exception e ){
			iResult = -1;
		}
		finally{
			return iResult;
		}

	}
	
	/**
	  * <PRE>
	  * 간략 : 메일 메세지 상세보기
	  * 상세 : 메일 메세지 상세보기
	  * <PRE>
	  * @param commandMap
	  * @throws Exception 
	  */
	public void fdcrAdA4View1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAdA4Dao.selectEmailMsg(commandMap);
		List list2= (List) fdcrAdA4Dao.selectEmailAddrListByMsgId(commandMap);
		List fileList= (List) fdcrAdA4Dao.selectMailAttcList(commandMap);

		commandMap.put("ds_mail", list);
		commandMap.put("ds_send2", list2);
		commandMap.put("ds_detail", fileList);

	}

	/** 
	  * <PRE>
	  * 간략 :메일 수정하기
	  * 상세 :메일 수정하기
	  * <PRE>
	  * @see kr.or.copyright.mls.console.email.inter.FdcrAdA4Service#fdcrAdA4Update1(java.util.Map, java.util.ArrayList)
	  */
	public boolean fdcrAdA4Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			String MSG_ID = (String) commandMap.get( "MSG_ID" );
			String GUBUN = (String) commandMap.get( "GUBUN" );
			String MSG_STOR_CD = (String) commandMap.get( "MSG_STOR_CD" );
			String TITLE = (String) commandMap.get( "TITLE" );
			String CONTENT = (String) commandMap.get( "CONTENT" );
			String RGST_IDNT = (String) commandMap.get( "RGST_IDNT" );

			String[] RECV_CDS = (String[]) commandMap.get( "RECV_CD" );
			String[] RECV_SEQNS = (String[]) commandMap.get( "RECV_SEQN" );
			String[] RECV_NAMES = (String[]) commandMap.get( "RECV_NAME" );
			String[] RECV_PERS_IDS = (String[]) commandMap.get( "RECV_PERS_ID" );
			String[] RECV_MAIL_ADDRS = (String[]) commandMap.get( "RECV_MAIL_ADDR" );
			String[] RECV_GROUP_IDS = (String[]) commandMap.get( "RECV_GROUP_ID" );
			String[] RECV_GROUP_NAMES = (String[]) commandMap.get( "RECV_GROUP_NAME" );
			String[] FILE_NAMES = (String[]) commandMap.get( "FILE_NAME" );
			String[] FILE_PATHS = (String[]) commandMap.get( "FILE_PATH" );
			String[] FILE_SIZES = (String[]) commandMap.get( "FILE_SIZE" );
			String[] ATTC_SEQNS = (String[]) commandMap.get( "ATTC_SEQN" );

			String row_status;
			try{
				boolean flag = false;
				Map<String, Object> tempMap = new HashMap<String, Object>();
				tempMap.put( "MSG_ID", MSG_ID );
				tempMap.put( "GUBUN", GUBUN );
				tempMap.put( "MSG_STOR_CD", MSG_STOR_CD );

				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "TITLE", TITLE );
				map.put( "CONTENT", CONTENT );
				map.put( "RGST_IDNT", RGST_IDNT );

				int msgIdSeq =
					Integer.parseInt( (String) ( tempMap.get( "MSG_ID" ) == null ? "0" : tempMap.get( "MSG_ID" ) ) );
				map.put( "MSG_ID", msgIdSeq );

				fdcrAdA4Dao.updateEmailMsg( map );

				// 첨부파일 delete 처리
				for( int i = 0; MSG_ID != null && i < ATTC_SEQNS.length; i++ ){
					Map<String, Object> deleteMap = new HashMap<String, Object>();
					deleteMap.put( "MSG_ID", MSG_ID );
					deleteMap.put( "ATTC_SEQN", ATTC_SEQNS[i] );
					fdcrAdA4Dao.deleteMailAttc( deleteMap );

					String fileName = commandMap.get( "REAL_FILE_NAME" ).toString();

					try{
						File file = new File( (String) deleteMap.get( "FILE_PATH" ), fileName );
						if( file.exists() ){
							file.delete();
						}
					}
					catch( Exception e ){
						e.printStackTrace();
					}
				}

				// detail INSERT, UPDATE처리
				for( int i = 0; i < ATTC_SEQNS.length; i++ ){

					// row_status = ds_detail.getRowStatus(i);
					row_status = "";

					Map<String, Object> fileMap = new HashMap<String, Object>();

					if( row_status.equals( "insert" ) == true ){
						byte[] file = (byte[]) commandMap.get( "CLIENT_FILE" );

						String fileName = commandMap.get( "REAL_FILE_NAME" ).toString();

						Map upload = FileUtil.uploadMiFile( fileName, file, "mail/" );

						// // 파일저장명 put
						fileMap.put( "MSG_ID", msgIdSeq );
						fileMap.put( "REAL_FILE_NAME", upload.get( "REAL_FILE_NAME" ) );
						fileMap.put( "FILE_PATH", upload.get( "FILE_PATH" ) );
						fileMap.put( "FILE_NAME", fileName );

						fdcrAdA4Dao.insertMailAttc( fileMap );
					}
				}

				flag = fdcrAdA4Dao.deleteEmailRecvInfo( tempMap );
				if( flag ){
					for( int i = 0; i < RECV_PERS_IDS.length; i++ ){
						Map<String, Object> map2 = new HashMap<String, Object>();
						map2.put( "MSG_ID", msgIdSeq );
						map2.put( "RECV_SEQN", i + 1 );
						map2.put( "RECV_CD", RECV_CDS[i] );
						map2.put( "RECV_NAME", RECV_NAMES[i] );
						map2.put( "RECV_PERS_ID", RECV_PERS_IDS[i] );
						map2.put( "RECV_MAIL_ADDR", RECV_MAIL_ADDRS[i] );
						map2.put( "RECV_GROUP_ID", RECV_GROUP_IDS[i] );
						map2.put( "RECV_GROUP_NAME", RECV_GROUP_NAMES[i] );
						fdcrAdA4Dao.insertEmailRecvInfo( map2 );
					}
					for( int j = 0; j < RECV_CDS.length; j++ ){
						Map<String, Object> map3 = new HashMap<String, Object>();
						map3.put( "MSG_ID", msgIdSeq );
						map3.put( "RECV_SEQN", j + 1 );
						map3.put( "RECV_CD", RECV_CDS[j] );
						map3.put( "RECV_NAME", RECV_NAMES[j] );
						map3.put( "RECV_PERS_ID", RECV_PERS_IDS[j] );
						map3.put( "RECV_MAIL_ADDR", RECV_MAIL_ADDRS[j] );
						map3.put( "RECV_GROUP_ID", RECV_GROUP_IDS[j] );
						map3.put( "RECV_GROUP_NAME", RECV_GROUP_NAMES[j] );
						fdcrAdA4Dao.insertEmailRecvInfo( map3 );
					}

				}

			}
			catch( Exception e ){
				e.printStackTrace();
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
