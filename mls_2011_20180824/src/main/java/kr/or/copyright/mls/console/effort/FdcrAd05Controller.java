package kr.or.copyright.mls.console.effort;

import java.net.URLDecoder;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd05Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 상당한노력(신청 및 조회공고) > 통계 > 상당한 노력 매칭정보
 * 
 * @author wizksy
 */
@Controller
public class FdcrAd05Controller extends DefaultController{

	@Resource( name = "fdcrAd05Service" )
	private FdcrAd05Service fdcrAd05Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAd05Controller.class );

	/**
	 * 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd05List1.page" )
	public String fdcrAd05List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd05List1 Start" );
		// 파라미터 셋팅
		String works_title = EgovWebUtil.getString( commandMap, "works_title" );
		if( !"".equals( works_title ) ){
			works_title = URLDecoder.decode( works_title, "UTF-8" );
			commandMap.put( "works_title", works_title );
		}
		fdcrAd05Service.fdcrAd05List1( commandMap );
		model.addAttribute( "commandMap", commandMap );
		model.addAttribute( "paginationInfo", commandMap.get( "paginationInfo" ) );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ds_page", commandMap.get( "ds_page" ) );
		logger.debug( "fdcrAd05List1 End" );
		return "effort/fdcrAd05List1.tiles";
	}

	/**
	 * 엑셀다운로드
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/effort/fdcrAd05Down1.page" )
	public String fdcrAd05Down1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAd05Down1 Start" );
		// 파라미터 셋팅
		String works_title = EgovWebUtil.getString( commandMap, "works_title" );
		if( !"".equals( works_title ) ){
			works_title = URLDecoder.decode( works_title, "UTF-8" );
			commandMap.put( "works_title", works_title );
		}
		fdcrAd05Service.fdcrAd05Down1( commandMap );
		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		model.addAttribute( "ExcelDown", "Y" );
		logger.debug( "fdcrAd05Down1 End" );
		return "effort/fdcrAd05Down1";
	}
}
