package kr.or.copyright.mls.console.cnsgn;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.cnsgn.inter.FdcrAd58Dao;
import kr.or.copyright.mls.console.cnsgn.inter.FdcrAd58Service;
import kr.or.copyright.mls.support.constant.Constants;
import kr.or.copyright.mls.support.dto.MailInfo;
import kr.or.copyright.mls.support.util.MailManager;
import kr.or.copyright.mls.support.util.MailMessage;
import kr.or.copyright.mls.support.util.NumberUtil;

import org.springframework.stereotype.Service;

@Service( "fdcrAd58Service" )
public class FdcrAd58ServiceImpl extends CommandService implements FdcrAd58Service{

	// old AdminWorksRegistrationDao
	@Resource( name = "fdcrAd58Dao" )
	private FdcrAd58Dao fdcrAd58Dao;

	/**
	 * 저작물보고 등록 - 보고대상목록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd58List1( Map<String, Object> commandMap ) throws Exception{

		List report_List = new ArrayList();
		List report_List_total = new ArrayList();
		List report_List_total2 = new ArrayList();
		List date = new ArrayList();

		String ca_id = (String) commandMap.get( "SCH_CA_ID" );
		String genre = (String) commandMap.get( "GENRE" );
		
		if(ca_id != null ){

			if( genre.equals( "1" ) ){
				if( ca_id.equals( "201" ) || ca_id.equals( "202" ) || ca_id.equals( "203" ) || ca_id.equals( "220" ) ){
					report_List_total = (List) fdcrAd58Dao.reportListTotalRowMusic( commandMap ); // 총건수
					int totCnt = ((BigDecimal)((Map)report_List_total.get( 0 )).get("COUNT")).intValue();
					pagination( commandMap, totCnt, 0 );
					report_List_total2 = (List) fdcrAd58Dao.reportListTotalRowMusic2( commandMap ); // 보고건수
					report_List = (List) fdcrAd58Dao.reportListMusic( commandMap ); // TREAT_YN
																					// =
																					// 'N'
				}
			}else if( genre.equals( "2" ) ){
				if( ca_id.equals( "204" ) || ca_id.equals( "205" ) || ca_id.equals( "206" ) || ca_id.equals( "215" ) ){
					// mls 보고 리스트
					//report_List_total = (List) fdcrAd58Dao.reportListTotalRowBook( commandMap ); // 총건수
					//int totCnt = ((BigDecimal)((Map)report_List_total.get( 0 )).get("COUNT")).intValue();
					//pagination( commandMap, totCnt, 0 );
					//report_List_total2 = (List) fdcrAd58Dao.reportListTotalRowBook2( commandMap ); // 보고건수
					//report_List = (List) fdcrAd58Dao.reportListBook( commandMap ); // TREAT_YN
																					// =
																					// 'N'
				}
			}else{
				if( ca_id.equals( "757" ) || ca_id.equals( "525" ) || ca_id.equals( "836" ) || ca_id.equals( "837" )
					|| ca_id.equals( "838" ) ){ // 대리중계업체일 경우 추후 ca_id 별로 구분
					report_List_total = (List) fdcrAd58Dao.reportListAgentTotalRow( commandMap ); // 총건수
					int totCnt = ((BigDecimal)((Map)report_List_total.get( 0 )).get("COUNT")).intValue();
					pagination( commandMap, totCnt, 0 );
					report_List_total2 = (List) fdcrAd58Dao.reportListAgentTotalRow2( commandMap ); // 보고건수
					report_List = (List) fdcrAd58Dao.reportListAgent( commandMap ); // TREAT_YN
																					// =
																					// 'N'
				}
			}
			
			// 현재 년월 가져오기
			date = (List) fdcrAd58Dao.getYearMonth();
		}

		// hmap를 dataset로 변환한다.
		commandMap.put( "ds_page", report_List_total );
		commandMap.put( "ds_page2", report_List_total2 );
		commandMap.put( "ds_report_List", report_List );
		commandMap.put( "ds_date", date );
	}

	/**
	 * 저작물 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	public void fdcrAd58List2( Map<String, Object> commandMap ) throws Exception{
		String genre = (String) commandMap.get( "GENRE" );

		if( genre.equals( "1" ) ){
			// 초기화
			fdcrAd58Dao.targetDeleteMusic( commandMap );
			// 임시저장
			fdcrAd58Dao.targetInsertMusic( commandMap );
			// 중복제거(COMM_WORKS_ID)
			// fdcrAd58Dao.commWorksIdDelete(map);

			List targetMusic = null;
			targetMusic = (List) fdcrAd58Dao.targetMusicList( commandMap );

			commandMap.put( "ds_target_music", targetMusic );
		}else if( genre.equals( "2" ) ){
			// 초기화
			fdcrAd58Dao.targetDeleteBook( commandMap );
			// 임시저장
			fdcrAd58Dao.targetInsertBook( commandMap );
			// 중복제거(COMM_WORKS_ID)
			// fdcrAd58Dao.commWorksIdDelete(map);

			List targetBook = null;
			targetBook = (List) fdcrAd58Dao.targetBookList( commandMap );

			commandMap.put( "ds_target_book", targetBook );
		}
	}

	/**
	 * 일괄등록
	 */
	public boolean fdcrAd58Insert1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			List resultList = new ArrayList();
			List errorList = new ArrayList();

			int genreCode =
				Integer.parseInt( ( commandMap.get( "GENRE" ) == null ? "0" : (String) commandMap.get( "GENRE" ) ) );

			if( genreCode == 0 ){
				throw new Exception( "장르코드 NULL : worksRegistrationInsert" );
			}

			ArrayList<Map<String, Object>> ds_target = null;
			switch( genreCode ){
				case 1:
					ds_target = (ArrayList<Map<String, Object>>) commandMap.get( "ds_target_music" );
					break;// 음악
				case 2:
					ds_target = (ArrayList<Map<String, Object>>) commandMap.get( "ds_target_book" );
					break;// 어문
				default:
					throw new Exception( "장르코드 Error OR Null : worksMgntInsert" );
			}

			int rFlag = 0;// 리포트FLAG
			boolean cFlag = true;// 저작물FLAG
			boolean gFlag = true;// 장르별저작물FLAG

			int cnt = 0;

			GregorianCalendar d = new GregorianCalendar();

			// 보고연월
			String reptYmdTmp =
				String.valueOf( d.get( Calendar.YEAR ) )
					+ String.valueOf( ( ( d.get( Calendar.MONTH ) + 1 ) < 10 ) ? "0"
						+ String.valueOf( d.get( Calendar.MONTH ) + 1 ) : String.valueOf( d.get( Calendar.MONTH ) + 1 ) );

			String reptYmd = reptYmdTmp.replaceAll( " ", "" );

			// 장르코드
			String genreCd = (String) commandMap.get( "GENRE" );

			// 등록자아이디
			String rgstIdnt = (String) commandMap.get( "USER_ID" );

			// 위탁관리기관ID
			String trstOrgnCode = (String) commandMap.get( "SCH_CA_ID" );

			commandMap.put( "REPT_YMD", reptYmd );// 보고연월
			commandMap.put( "GENRE_CD", genreCd );// 장르코드
			commandMap.put( "RGST_IDNT", rgstIdnt );// 등록자아이디
			rFlag = fdcrAd58Dao.adminCommWorksReportInsert( commandMap );

			if( rFlag == 0 ){
				for( int i = 0; i < ds_target.size(); i++ ){
					Map map = null;
					map = ds_target.get( i );
					resultList.add( map );
				}
			}else{
				ArrayList inList = new ArrayList();
				for( int i = 0; i < ds_target.size(); i++ ){

					Map map = null;
					try{
						map = ds_target.get( i );
						// seq를 사용하여 worksId 생성
						map.put( "SCH_CA_ID", trstOrgnCode );// 위탁관리기관아이디
						map.put( "GENRE", genreCd );// 장르코드
						if( fdcrAd58Dao.getExistCommWorks( map ) == 0 ){
							map.put( "REPT_YMD", reptYmd );// 보고년월
							map.put( "GENRE", genreCd );// 장르코드
							map.put( "RGST_IDNT", rgstIdnt );// 등록자아이디
							map.put( "WORKS_ID", fdcrAd58Dao.getCommWorksId() );// 저작물아이디
							inList.add( map );
							cnt++;
						}else{
							resultList.add( map );
						}

						if( ( cnt % 100 == 0 || i == ds_target.size() - 1 ) && inList.size() > 0 ){
							if( fdcrAd58Dao.commWorksInsertBacth( inList ) == null ){
								try{
									switch( genreCode ){
										case 1:
											fdcrAd58Dao.commMusicInsertBacth( inList );
											break;// 음악
										case 2:
											fdcrAd58Dao.commBookInsertBacth( inList );
											break;// 어문
										default:
											break;
									}
								}
								catch( Exception e ){
									for( int j = 0; j < inList.size(); j++ ){
										fdcrAd58Dao.adminCommWorksDelete( (Map) inList.get( j ) );
									}
									errorList.addAll( inList );
									e.printStackTrace();
								}
							}else{
								for( int j = 0; j < inList.size(); j++ ){
									fdcrAd58Dao.adminCommWorksDelete( (Map) inList.get( j ) );
								}
								errorList.addAll( inList );
							}
							inList.clear();
						}
					}
					catch( Exception e ){
						errorList.addAll( inList );
						inList.clear();
						e.printStackTrace();
					}
				}
				inList = null;
			}

			if( errorList.size() > 0 ){
				Map map2 = null;
				try{
					for( int j = 0; j < errorList.size(); j++ ){
						map2 = (Map) errorList.get( j );
						if( fdcrAd58Dao.adminCommWorksInsert( map2 ) ){
							try{
								boolean bool = false;
								switch( genreCode ){
									case 1:
										bool = fdcrAd58Dao.adminCommMusicInsert( map2 );
										break;// 음악
									case 2:
										bool = fdcrAd58Dao.adminCommBookInsert( map2 );
										break;// 어문
									default:
										break;
								}
								if( bool ){
									cnt++;
								}else{
									fdcrAd58Dao.adminCommWorksDelete( map2 );
									resultList.add( map2 );
								}
							}
							catch( Exception e ){
								fdcrAd58Dao.adminCommWorksDelete( map2 );
								resultList.add( map2 );
								e.printStackTrace();
							}
						}else{
							resultList.add( map2 );
						}
						map2 = null;
					}
				}
				catch( Exception e ){
					if( map2 != null ){
						resultList.add( map2 );
					}
					System.out.println( "등록에러2 : worksMgntInsert()" );
					e.printStackTrace();
				}
			}

			commandMap.put( "REPT_WORKS_CONT", ( cnt - errorList.size() ) );
			commandMap.put( "REPT_SEQN", rFlag );

			if( rFlag != 0 ){
				fdcrAd58Dao.adminCommWorksReportUpdate( commandMap );
			}

			try{

				/*
				 * 메일보내기
				 */

				String host = Constants.MAIL_SERVER_IP; // smtp서버
				String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표
																// 메일
				String fromName = Constants.SYSTEM_NAME;
				String to = (String) commandMap.get( "REPT_CHRR_MAIL" );
				String toName = (String) commandMap.get( "REPT_CHRR_NAME" );

				String subject = ""; // 메일 제목

				MailInfo info = new MailInfo();

				info.setFrom( from );
				info.setFromName( fromName );
				info.setHost( host );
				info.setEmail( to );
				info.setEmailName( toName );

				if( to.length() > 0 ){
					// 완료시간
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd aa hh:mm:ss" );
					String rgstDttm = sdf.format( cal.getTime() );

					// 기관명
					String commName = (String) commandMap.get( "COMM_NAME" );

					// 사용자 정보 셋팅
					/*
					 * String userIdnt = (String) userMap.get("USER_IDNT");
					 * String userName = (String) userMap.get("USER_NAME");
					 * String userEmail = (String) userMap.get("U_MAIL"); String
					 * userSms = (String) userMap.get("MOBL_PHON");
					 */
					// 총 등록(또는 수정)건 수
					String reptWorksCont = NumberUtil.getCommaNumber( ( cnt - errorList.size() ) );
					String mailTitlDivs = "등록";
					String mailTitl = "[저작권찾기] 위탁관리 저작물 보고";

					subject = mailTitl + " " + mailTitlDivs + "이 완료되었습니다.";

					StringBuffer sb = new StringBuffer();
					sb.append( "				<tr>" );
					sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">" );
					sb.append( "					<p style=\"font-weight:bold; background:url("
						+ Constants.DOMAIN_HOME
						+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 보고 정보</p>" );
					sb.append( "					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">" );
					sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME
						+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 보고기관 : " + commName
						+ "</li>" );
					sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME
						+ "/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">" + mailTitlDivs
						+ " 완료 일자 : " + rgstDttm + "</li>" );
					sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME
						+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">" + mailTitlDivs
						+ " 보고 건수 : " + reptWorksCont + " 건 </li>" );
					// sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 담당자 정보 : "+userName+" (email:"+
					// userEmail +", mobile: "+userSms+")</li>");
					sb.append( "					</ul>" );
					sb.append( "					</td>" );
					sb.append( "				</tr>" );

					info.setMessage( sb.toString() );
					info.setSubject( subject );
					info.setMessage( new String( MailMessage.getChangeInfoMailText2( info ) ) );
				}
				MailManager manager = MailManager.getInstance();
				info = manager.sendMail( info );

				if( info.isSuccess() ){
					System.out.println( ">>>>>>>>>>>> 1. message success :" + info.getEmail() );
				}else{
					System.out.println( ">>>>>>>>>>>> 1. message false   :" + info.getEmail() );
				}

			}
			catch( Exception e ){
				e.printStackTrace();
			}

			switch( genreCode ){
				case 1:
					commandMap.put( "ds_target_music", resultList );
					break;// 음악
				case 2:
					commandMap.put( "ds_target_book", resultList );
					break;// 어문
				default:
					throw new Exception( "장르코드 Error OR Null : worksMgntInsert" );

			}
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 일괄수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd58Update1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			List returnList = null;
			int cnt = 0;
			if( commandMap.get( "GENRE_CODE" ).equals( "1" ) ){ // 음악
				ArrayList<Map<String, Object>> ds_target =
					(ArrayList<Map<String, Object>>) commandMap.get( "ds_target_music" );
				cnt = ds_target.size();
				returnList = fdcrAd58Dao.adminCommMusicUpdate( commandMap, ds_target );
				commandMap.put( "ds_target_music", returnList );
			}else if( commandMap.get( "GENRE_CODE" ).equals( "2" ) ){ // 어문
				ArrayList<Map<String, Object>> ds_target =
					(ArrayList<Map<String, Object>>) commandMap.get( "ds_target_book" );
				cnt = ds_target.size();
				returnList = fdcrAd58Dao.adminCommBookUpdate( commandMap, ds_target );
				commandMap.put( "ds_target_book", returnList );
			}

			/*
			 * 메일보내기
			 */
			String host = Constants.MAIL_SERVER_IP; // smtp서버
			String from = Constants.SYSTEM_MAIL_ADDRESS; // 발신인 주소 - 시스템 대표 메일
			String fromName = Constants.SYSTEM_NAME;
			String to = (String) commandMap.get( "REPT_CHRR_MAIL" );
			String toName = (String) commandMap.get( "REPT_CHRR_NAME" );

			String subject = ""; // 메일 제목

			MailInfo info = new MailInfo();

			info.setFrom( from );
			info.setFromName( fromName );
			info.setHost( host );
			info.setEmail( to );
			info.setEmailName( toName );

			if( to.length() > 0 ){
				// 완료시간
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd aa hh:mm:ss" );
				String rgstDttm = sdf.format( cal.getTime() );

				// 기관명
				String commName = (String) commandMap.get( "COMM_NAME" );

				// 사용자 정보 셋팅
				/*
				 * String userIdnt = (String) userMap.get("USER_IDNT"); String
				 * userName = (String) userMap.get("USER_NAME"); String
				 * userEmail = (String) userMap.get("U_MAIL"); String userSms =
				 * (String) userMap.get("MOBL_PHON");
				 */
				// 총 등록(또는 수정)건 수
				String reptWorksCont = NumberUtil.getCommaNumber( ( cnt - returnList.size() ) );
				String mailTitlDivs = "수정";
				String mailTitl = "[저작권찾기] 위탁관리 저작물 보고";

				subject = mailTitl + " " + mailTitlDivs + "이 완료되었습니다.";

				StringBuffer sb = new StringBuffer();
				sb.append( "				<tr>" );
				sb.append( "					<td colspan=\"2\" style=\"border-left:10px solid #eaf1f7; border-right:10px solid #eaf1f7; padding:0 35px;\">" );
				sb.append( "					<p style=\"font-weight:bold; background:url("
					+ Constants.DOMAIN_HOME
					+ "/images/2012/mail/bullet.gif) no-repeat 0 4px; padding:0 0 0 20px; margin:20px 0 0 0;\"> 보고 정보</p>" );
				sb.append( "					<ul style=\"list-style:none; padding:0 0 0 18px; margin:10px 0 0 0;\">" );
				sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME
					+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 보고기관 : " + commName + "</li>" );
				sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME
					+ "/images/2012/mail/dot.gif) no-repeat 0 9px; padding-left:12px;\">" + mailTitlDivs + " 완료 일자 : "
					+ rgstDttm + "</li>" );
				sb.append( "						<li style=\"background:url(" + Constants.DOMAIN_HOME
					+ "/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\">" + mailTitlDivs + " 보고 건수 : "
					+ reptWorksCont + " 건 </li>" );
				// sb.append("						<li style=\"background:url("+Constants.DOMAIN_HOME+"/images/2012/mail/dot.gif) no-repeat 0 8px; padding-left:12px;\"> 담당자 정보 : "+userName+" (email:"+
				// userEmail +", mobile: "+userSms+")</li>");
				sb.append( "					</ul>" );
				sb.append( "					</td>" );
				sb.append( "				</tr>" );

				info.setMessage( sb.toString() );
				info.setSubject( subject );
				info.setMessage( new String( MailMessage.getChangeInfoMailText2( info ) ) );
			}
			MailManager manager = MailManager.getInstance();

			info = manager.sendMail( info );

			if( info.isSuccess() ){
				System.out.println( ">>>>>>>>>>>> 1. message success :" + info.getEmail() );
			}else{
				System.out.println( ">>>>>>>>>>>> 1. message false   :" + info.getEmail() );
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
