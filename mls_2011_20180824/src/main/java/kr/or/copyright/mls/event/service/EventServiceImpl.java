package kr.or.copyright.mls.event.service;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.tobesoft.platform.data.Dataset;

import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.event.dao.EventDao;
import kr.or.copyright.mls.event.model.Event;

public class EventServiceImpl extends BaseService implements EventService {
	
	private EventDao eventDao;
	
	public void setEventDao(EventDao eventDao) {
		
		this.eventDao = eventDao;
	}
	
	public int checkCampPart(Event event) {
		
		return eventDao.checkCampPart(event);
	}
	
	
	@Transactional(readOnly = false)
	public int insertCampPartInfo(Event event) throws Exception{
		
		int iFlag = -200;
		
		try {
			
			eventDao.insertCampPartInfo(event);
			
			iFlag = 200;
			
		} catch(Exception e) {
			
			e.printStackTrace();

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return iFlag;
		}
		
		return iFlag;
	}
	
	@Transactional(readOnly = false)
	public int insertCampPartRslt(Event event) throws Exception{
		
		int iFlag = -300;
		
		try {
			
			eventDao.insertCampPartRslt(event);
			
			iFlag = 300;
			
		} catch(Exception e) {
			
			e.printStackTrace();

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return iFlag;
		}
		
		return iFlag;
	}	
	
	
	// 관리자 화면
	public void campPartList() throws Exception{
		
		Dataset ds_condition = getDataset("ds_condition");
		
		ds_condition.printDataset();
		Map map = getMap(ds_condition, 0);
		

		// DAO호출
		List list = (List)eventDao.campPartList(map);
		
		// DataSet return
		addList("ds_List", list);
	}
}
