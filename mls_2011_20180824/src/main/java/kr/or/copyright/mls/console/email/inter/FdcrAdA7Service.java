package kr.or.copyright.mls.console.email.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAdA7Service{

	/**
	 * 신청정보 보완안내 메일 발신내역 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA7List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 공고정보 보완내역 팝업
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA7Pop1( Map<String, Object> commandMap ) throws Exception;

}
