package kr.or.copyright.mls.console.email;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.email.inter.FdcrAdA7Service;
import kr.or.copyright.mls.support.util.StringUtil;

import org.springframework.stereotype.Service;

@Service( "fdcrAdA7Service" )
public class FdcrAdA7ServiceImpl extends CommandService implements FdcrAdA7Service{

	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	/**
	 * 신청정보 보완안내 메일 발신내역 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA7List1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = (List) fdcrAd01Dao.selectBordSuplSendList( commandMap );

		commandMap.put( "ds_list", list );

	}

	/**
	 * 공고정보 보완내역 팝업
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAdA7Pop1( Map<String, Object> commandMap ) throws Exception{

		// DAO 호출
		List list = new ArrayList();
		String msgStorCd = StringUtil.nullToEmpty( (String) commandMap.get( "MSG_STOR_CD" ) );
		if( "6".equals( msgStorCd ) ){ // 상당한 노력
			list = (List) fdcrAd01Dao.selectWorksSuplItemList( commandMap );
		}else if( "5".equals( msgStorCd ) ){// 보상금 공탁공고(5)
			commandMap.put( "BIG_CODE", "89" );// 보상금 공탁공고 보완항목 코드값
			list = (List) fdcrAd01Dao.selectBordSuplItemList( commandMap );
		}else if( "4".equals( msgStorCd ) ){// 저작권자 조회공고(4),
			commandMap.put( "BIG_CODE", "88" );// 저작권자 조회공고 보완항목 코드값
			list = (List) fdcrAd01Dao.selectBordSuplItemList( commandMap );
		}
		commandMap.put( "ds_supl_list", list );
	}

}
