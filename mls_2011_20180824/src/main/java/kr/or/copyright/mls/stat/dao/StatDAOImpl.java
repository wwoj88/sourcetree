package kr.or.copyright.mls.stat.dao;

import java.util.List;

import kr.or.copyright.mls.stat.model.MlStatWorksDefaultVO;
import kr.or.copyright.mls.stat.model.MlStatWorksVO;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * @Class Name : MlStatBordDAO.java
 * @Description : MlStatBord DAO Class
 * @Modification Information
 *
 * @author ����ȣ
 * @since 2012.08.06
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("StatDAO")
public class StatDAOImpl extends EgovAbstractDAO implements StatDAO {

    /**
     * Insert
     * @param vo
     * @return
     * @throws Exception
     */
    /*
    public String insertMlStatBord(MlStatBordVO vo) throws Exception {
        return (String)insert("mlStatBordDAO.insertMlStatBord_S", vo);
    }*/

    /**
     * Update
     * @param vo
     * @throws Exception
     */
    /*
    public void updateMlStatBord(MlStatBordVO vo) throws Exception {
        update("mlStatBordDAO.updateMlStatBord_S", vo);
    }*/

    /**
     * Delete
     * @param vo
     * @throws Exception
     */
    /*
    public void deleteMlStatBord(MlStatBordVO vo) throws Exception {
        delete("mlStatBordDAO.deleteMlStatBord_S", vo);
    }*/

    /**
     * Select
     * @param vo
     * @return
     * @throws Exception
     */
    /*
    public MlStatBordVO selectMlStatBord(MlStatBordVO vo) throws Exception {
        return (MlStatBordVO) selectByPk("mlStatBordDAO.selectMlStatBord_S", vo);
    }*/

    /**
     * SelectList
     * @param searchVO
     * @return
     * @throws Exception
     */
    public List<MlStatWorksVO> selectMlStatWorksList(MlStatWorksDefaultVO searchVO) throws Exception {
        return list("Stat.selectMlStatWorksList", searchVO);
    }

    /**
     * TotCntList
     * @param searchVO
     * @return
     */
    public int selectMlStatWorksListTotCnt(MlStatWorksDefaultVO searchVO) {
        return (Integer)getSqlMapClientTemplate().queryForObject("Stat.selectMlStatWorksListTotCnt", searchVO);
    }

    /**
     * CheckSelectList
     * 
     * @param searchVO
     * @return
     */
    public List<MlStatWorksVO> checkSelectMlStatWorksList(
	    MlStatWorksDefaultVO searchVO) throws Exception {
	return list("Stat.checkSelectMlStatWorksList",searchVO);
    }

}
