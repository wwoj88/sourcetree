package kr.or.copyright.mls.console.rcept.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd26Service{

	/**
	 * 미분배보상금 수업목적 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd26List1( Map<String, Object> commandMap ) throws Exception;

}
