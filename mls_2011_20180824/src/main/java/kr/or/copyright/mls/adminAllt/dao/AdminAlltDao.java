package kr.or.copyright.mls.adminAllt.dao;

import java.util.List;
import java.util.Map;

public interface AdminAlltDao {

	public List alltInmtDetl(Map map);
	
	public void alltInmtUpdate(Map map);
	
	public List alltInmtCount(Map map);
	
	public void alltInmtSave(Map map);
	
	public List alltInmtList(Map map);
	
	/**
	 * @param map
	 * @author ����ȣ
	 * @since 2012.11.20
	 * @return
	 */
	public List alltInmtMgntList(Map map);
	
	public void alltInmtMgntUpdate(Map map);
	
	public void alltInmtMgntInsert(Map map);
	
	public void alltInmtYsNoUpdate(Map map);
	
	public void alltInmtAllUpdate(Map map);
	
	public String alltInmtMgntMaxYear();
}
