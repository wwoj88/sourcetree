package kr.or.copyright.mls.console.legal.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd10Service{

	/**
	 * 보상금 공탁공고 게시판 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁공고 게시판 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁공고 게시판 등록자 정보 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10Pop1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁공고 게시판 승인 공고
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd10Update1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁공고 게시판 승인 처리상태 수정
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd10Update2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁공고 게시판 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd10Delete1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁공고 게시판 공고내용 보완 폼
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10Pop2( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 보상금 공탁공고 게시판 공고내용 불러오기
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> fdcrAd10Detail( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁공고 게시판 공고내용 수정 및 보완이력 등록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd10SuplRegi1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁공고 게시판 보완내역 메일 발신함 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10SuplSendList1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 보상금 공탁공고 게시판 공고정보 보완내역 팝업
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd10SuplListPop1( Map<String, Object> commandMap ) throws Exception;

}
