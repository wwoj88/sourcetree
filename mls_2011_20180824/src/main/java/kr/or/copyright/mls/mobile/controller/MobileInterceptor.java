package kr.or.copyright.mls.mobile.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class MobileInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String userAgent = request.getHeader("USER-AGENT").toLowerCase();
		if(userAgent.matches(".*iphone.*")||
				userAgent.matches(".*ipad.*")||
				userAgent.matches(".*ipod.*")||
				userAgent.matches(".*android.*")) {
			return true;
		} else {
			throw new ModelAndViewDefiningException(new ModelAndView("mobile/redirect"));
		}
	}
}
