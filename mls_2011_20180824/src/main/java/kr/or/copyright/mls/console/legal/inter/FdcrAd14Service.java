package kr.or.copyright.mls.console.legal.inter;

import java.sql.SQLException;
import java.util.Map;

public interface FdcrAd14Service{

	/**
	 * 결제집결표 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd14List1( Map<String, Object> commandMap ) throws Exception;

}
