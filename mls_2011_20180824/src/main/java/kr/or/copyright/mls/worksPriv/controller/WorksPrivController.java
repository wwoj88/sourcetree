package kr.or.copyright.mls.worksPriv.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;
import kr.or.copyright.mls.support.util.FileUploadUtil;
import kr.or.copyright.mls.support.util.SessionUtil;
import kr.or.copyright.mls.support.util.StringUtil;
import kr.or.copyright.mls.worksPriv.model.WorksPriv;
import kr.or.copyright.mls.worksPriv.model.WorksPrivFile;
import kr.or.copyright.mls.worksPriv.service.WorksPrivService;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class WorksPrivController extends MultiActionController {

	//private Log logger = LogFactory.getLog(getClass());

	
	private WorksPrivService worksPrivService;

	//private String realUploadPath = "D:/server/mls/mls_2010/upload/";  // 로컬용
	//private String realUploadPath = "C:/home/tmax/mls/web/upload/";  // 로컬용
	//private String realUploadPath = "/home/tmax/mls/web/upload/";  // 실서버용
	//private String realUploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버용
	private String realUploadPath = FileUtil.uploadPath();
	
	

	public void setWorksPrivService(WorksPrivService worksPrivService) {
		this.worksPrivService = worksPrivService;
	}

	public ModelAndView list(HttpServletRequest request,HttpServletResponse respone) throws Exception {
		//>>params values
		Map params = new HashMap();
		params.put("srchGenreDivs", ServletRequestUtils.getStringParameter(request, "srchGenreDivs"));
		params.put("srchStatDivs", ServletRequestUtils.getStringParameter(request, "srchStatDivs"));
		params.put("srchText", ServletRequestUtils.getStringParameter(request, "srchText"));
		params.put("PAGE_NO", ServletRequestUtils.getIntParameter(request, "page_no", 1));
		params.put("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));
		params.put("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));
		params.put("menuSeqn", ServletRequestUtils.getStringParameter(request, "menuSeqn",""));
		params.put("srchCommId", ServletRequestUtils.getStringParameter(request, "srchCommId"));
		params.put("srchTitle", ServletRequestUtils.getStringParameter(request, "srchTitle"));
		params.put("srchRgstName", ServletRequestUtils.getStringParameter(request, "srchRgstName"));
		
		String srchFILD1 = ServletRequestUtils.getStringParameter(request, "FILD1");
		String srchFILD2 = ServletRequestUtils.getStringParameter(request, "FILD2");
		String srchFILD3 = ServletRequestUtils.getStringParameter(request, "FILD3");
		String srchFILD4 = ServletRequestUtils.getStringParameter(request, "FILD4");
		String srchFILD5 = ServletRequestUtils.getStringParameter(request, "FILD5");
		String srchFILD6 = ServletRequestUtils.getStringParameter(request, "FILD6");
		String srchFILD7 = ServletRequestUtils.getStringParameter(request, "FILD7");
		String srchFILD8 = ServletRequestUtils.getStringParameter(request, "FILD8");
		String srchFILD9 = ServletRequestUtils.getStringParameter(request, "FILD9");
		String srchFILD10 = ServletRequestUtils.getStringParameter(request, "FILD10");
		String srchFILD11 = ServletRequestUtils.getStringParameter(request, "FILD11");
		String srchFILD99 = ServletRequestUtils.getStringParameter(request, "FILD99");
		
		params.put("FILD1", srchFILD1);
		params.put("FILD2", srchFILD2);
		params.put("FILD3", srchFILD2);
		params.put("FILD4", srchFILD4);
		params.put("FILD5", srchFILD5);
		params.put("FILD6", srchFILD6);
		params.put("FILD7", srchFILD7);
		params.put("FILD8", srchFILD8);
		params.put("FILD9", srchFILD9);
		params.put("FILD10", srchFILD10);
		params.put("FILD11", srchFILD11);
		params.put("FILD99", srchFILD99);
		params.put("FILD", ServletRequestUtils.getStringParameter(request, "FILD"));
		
		int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);
		String menuSeqn = ServletRequestUtils.getStringParameter(request, "menuSeqn");
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>String menuSeqn:"+menuSeqn);
		
		//>>code values
		//분야코드
		CodeList codeList = new CodeList();
		codeList.setBigCode("35"); 
		List genreCodeList = worksPrivService.findCodeList(codeList);
		//발급상태코드
		codeList.setBigCode("48");
		List statCodeList = worksPrivService.findCodeList(codeList);
		
		//ModelAndView mv = new ModelAndView("my/myWorksList", "codeListArr", codeListArr);
		if ("1".equals(menuSeqn)) { // Q&A
			ModelAndView mv = new ModelAndView("privWork/privWorksList", "worksPrivList", worksPrivService.findWorksPrivList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, params));
			mv.addObject("genreCodeList",genreCodeList);
			return mv;
		}else{
			User user = SessionUtil.getSession(request);
			String sessUserIdnt = user.getUserIdnt(); 
			params.put("rgstIdnt", sessUserIdnt);
			ModelAndView mv = new ModelAndView("my/myWorksList","worksPrivList", worksPrivService.findWorksPrivList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, params));
			mv.addObject("genreCodeList",genreCodeList);
			mv.addObject("statCodeList",statCodeList);
			return mv;
		}
		//mv.addObject("title", board.getTite());

		
		//return new ModelAndView("my/myWorksList", "worksPrivList", worksPrivService.findWorksPrivList(pageNo, Constants.DEFAULT_ROW_PER_PAGE,  params));
	}
	
	public ModelAndView goRegiSmpl(HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		return new ModelAndView("my/myWorksRegi_smpl");
	}
	
	public ModelAndView goRegi(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		String rgstIdnt = ServletRequestUtils.getStringParameter(request, "rgstIdnt");
		int genreCode = ServletRequestUtils.getIntParameter(request, "genreCode",0);
//		>>code values
		CodeList codeList = new CodeList();
		
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+rgstIdnt);
		
		//분야코드
		codeList.setBigCode("35");
		List genreCodeList = worksPrivService.findCodeList(codeList);
		
		
		//공표매체
		codeList.setBigCode("11");
		List publCodeList = worksPrivService.findCodeList(codeList);
		
		//종류코드
		switch(genreCode){
		case 1:
			codeList.setBigCode("36");
			break;
		case 2:
			codeList.setBigCode("37");
			break;
		case 3:
			codeList.setBigCode("38");
			break;
		case 4:
			codeList.setBigCode("39");
			break;
		case 5:
			codeList.setBigCode("40");
			break;
		case 6:
			codeList.setBigCode("41");
			break;
		case 7:
			codeList.setBigCode("42");
			break;
		case 8:
			codeList.setBigCode("43");
			break;
		case 9:
			codeList.setBigCode("44");
			break;
		case 10:
			codeList.setBigCode("45");
			break;
		case 11:
			codeList.setBigCode("46");
			break;	
		}
		List kindCodeList = new ArrayList();
		if(genreCode != 0){
			kindCodeList = worksPrivService.findCodeList(codeList);
		}
		
		ModelAndView mv = new ModelAndView("my/myWorksRegi");
		mv.addObject("genreCodeList", genreCodeList);
		if(genreCode != 0){
			mv.addObject("kindCodeList", kindCodeList);
		}
		mv.addObject("publCodeList", publCodeList);
		mv.addObject("genreCode", genreCode);
		mv.addObject("rgstIdnt", rgstIdnt);
		
		return mv;

	}
	
	
	public ModelAndView fileDownLoad(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		return new ModelAndView("common/FileDownload");
	}
	
	public ModelAndView goInsert(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		WorksPriv worksPriv = new WorksPriv();
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request);
		
		worksPriv.setCrId(StringUtil.nullToZero( multipartRequest.getParameter("crId")) );
		//worksPriv.setThreaded( StringUtil.nullToZero(multipartRequest.getParameter("threaded")) );
		//worksPriv.setMenuSeqn( StringUtil.nullToZero(multipartRequest.getParameter("menuSeqn")) );
		
		//ServletRequestUtils.getRequiredIntParameter(request, "deth")
		worksPriv.setGenreCode(multipartRequest.getParameter("genreCode"));
		worksPriv.setKindCode(multipartRequest.getParameter("kindCode"));
		worksPriv.setCreaYear(multipartRequest.getParameter("creaYear"));
		worksPriv.setTitle(multipartRequest.getParameter("titleName"));
		worksPriv.setPublCode(multipartRequest.getParameter("publCode"));
		worksPriv.setPublName(multipartRequest.getParameter("publName"));
		worksPriv.setPublYmd(multipartRequest.getParameter("publYmd"));
		worksPriv.setWorksDesc(multipartRequest.getParameter("worksDesc"));
		worksPriv.setWorksUrl(multipartRequest.getParameter("worksUrl"));
		worksPriv.setCclCode(multipartRequest.getParameter("cclCode"));
		worksPriv.setCoptHodr(multipartRequest.getParameter("coptHodr"));
		worksPriv.setCoptHodrDesc(multipartRequest.getParameter("coptHodrDesc"));
		
		worksPriv.setRgstIdnt(multipartRequest.getParameter("rgstIdnt"));
		worksPriv.setModiIdnt(multipartRequest.getParameter("modiIdnt"));
		worksPriv.setFileName(multipartRequest.getParameter("fileName"));
		worksPriv.setFilePath(multipartRequest.getParameter("filePath"));
		worksPriv.setFileSize(multipartRequest.getParameter("fileSize"));	
		worksPriv.setSrchDivs(multipartRequest.getParameter("srchDivs"));
		worksPriv.setSrchText(StringUtil.nullToEmpty(multipartRequest.getParameter("srchText")) );
		worksPriv.setPage_no(multipartRequest.getParameter("page_no") == null ? "1" : multipartRequest.getParameter("page_no") );
		
		 //File Upload
			Iterator fileNameIterator = multipartRequest.getFileNames();
			List fileList = new ArrayList();
			
			while(fileNameIterator.hasNext()) {
				MultipartFile multiFile= multipartRequest.getFile((String)fileNameIterator.next());
				if(multiFile.getSize()>0) {
					WorksPrivFile worksPrivFile = FileUploadUtil.uploadFormFilesW(multiFile, realUploadPath);
					fileList.add(worksPrivFile);
				}
			}
			
			worksPriv.setFileList(fileList);
			
			int iResult = worksPrivService.goInsert(worksPriv);
			
			//System.out.print("iResult>>>>>>>>>>>>>>>>>>>>"+iResult);
			//if(iResult > 0)
			//	return new ModelAndView("qust/qustRegiSucc");
			//else
			ModelAndView mv = new ModelAndView("my/myWorksRegiSucc", "iResult", iResult);
			
			mv.addObject("title", worksPriv.getTitle());
			
			return mv;
	}
	
	public ModelAndView goView(HttpServletRequest request, HttpServletResponse response) throws Exception{
		int genreCode = ServletRequestUtils.getIntParameter(request, "genreCode",0);
		String menuSeqn = ServletRequestUtils.getStringParameter(request, "menuSeqn");
//		>>code values
		CodeList codeList = new CodeList();
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+genreCode);
		
		//분야코드
		codeList.setBigCode("35");
		List genreCodeList = worksPrivService.findCodeList(codeList);
		
		
		//공표매체
		codeList.setBigCode("11");
		List publCodeList = worksPrivService.findCodeList(codeList);
		
		//종류코드
		switch(genreCode){
		case 1:
			codeList.setBigCode("36");
			break;
		case 2:
			codeList.setBigCode("37");
			break;
		case 3:
			codeList.setBigCode("38");
			break;
		case 4:
			codeList.setBigCode("39");
			break;
		case 5:
			codeList.setBigCode("40");
			break;
		case 6:
			codeList.setBigCode("41");
			break;
		case 7:
			codeList.setBigCode("42");
			break;
		case 8:
			codeList.setBigCode("43");
			break;
		case 9:
			codeList.setBigCode("44");
			break;
		case 10:
			codeList.setBigCode("45");
			break;
		case 11:
			codeList.setBigCode("46");
			break;	
		}
		List kindCodeList = new ArrayList();
		if(genreCode != 0){
			kindCodeList = worksPrivService.findCodeList(codeList);
		}
		
		
		
		WorksPriv worksPriv = new WorksPriv();
		
		worksPriv.setCrId(ServletRequestUtils.getRequiredLongParameter(request, "crId"));
		worksPriv.setPage_no(ServletRequestUtils.getStringParameter(request, "page_no"));
		
		String submitType = ServletRequestUtils.getStringParameter(request, "submitType");
		
		if("update".equals(submitType)){
			ModelAndView mv = new ModelAndView("my/myWorksModi", "worksPriv", worksPrivService.goView(worksPriv));
			mv.addObject("genreCodeList", genreCodeList);
			if(genreCode != 0){
				mv.addObject("kindCodeList", kindCodeList);
			}
			mv.addObject("publCodeList", publCodeList);
			mv.addObject("genreCode", genreCode);
			return mv;
		}else{
			if("1".equals(menuSeqn)){
				return new ModelAndView("privWork/privWorksDetl","worksPriv", worksPrivService.goView(worksPriv));
			}else{
				return new ModelAndView("my/myWorksDetl", "worksPriv", worksPrivService.goView(worksPriv));
			}
		}
	}
	
	public ModelAndView goDelete(HttpServletRequest request, HttpServletResponse respone) throws Exception{
		String menuSeqn = ServletRequestUtils.getStringParameter(request, "menuSeqn");
		
		WorksPriv worksPriv = new WorksPriv();
		
		long crId = ServletRequestUtils.getRequiredLongParameter(request, "crId");
		
		worksPriv.setCrId(crId);
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>:::::::crId: "+crId);
		
		worksPrivService.goDelete(worksPriv);
		
		return this.list(request, respone);
		
	}
	
	public ModelAndView goUpdate(HttpServletRequest request, HttpServletResponse response)throws Exception{
		WorksPriv worksPriv = new WorksPriv();
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(request); 
		
		worksPriv.setCrId(StringUtil.nullToZero( multipartRequest.getParameter("crId")) );
		//worksPriv.setThreaded( StringUtil.nullToZero(multipartRequest.getParameter("threaded")) );
		//worksPriv.setMenuSeqn( StringUtil.nullToZero(multipartRequest.getParameter("menuSeqn")) );
		
		//ServletRequestUtils.getRequiredIntParameter(request, "deth")
		worksPriv.setModiIdnt(multipartRequest.getParameter("modiIdnt"));
		worksPriv.setGenreCode(multipartRequest.getParameter("genreCode"));
		worksPriv.setKindCode(multipartRequest.getParameter("kindCode"));
		worksPriv.setCreaYear(multipartRequest.getParameter("creaYear"));
		worksPriv.setTitle(multipartRequest.getParameter("titleName"));
		worksPriv.setPublCode(multipartRequest.getParameter("publCode"));
		worksPriv.setPublName(multipartRequest.getParameter("publName"));
		worksPriv.setPublYmd(multipartRequest.getParameter("publYmd"));
		worksPriv.setWorksDesc(multipartRequest.getParameter("worksDesc"));
		worksPriv.setWorksUrl(multipartRequest.getParameter("worksUrl"));
		worksPriv.setCclCode(multipartRequest.getParameter("cclCode"));
		worksPriv.setCoptHodr(multipartRequest.getParameter("coptHodr"));
		worksPriv.setCoptHodrDesc(multipartRequest.getParameter("coptHodrDesc"));
		
		worksPriv.setFileName(multipartRequest.getParameter("fileName"));
		worksPriv.setFilePath(multipartRequest.getParameter("filePath"));
		worksPriv.setFileSize(multipartRequest.getParameter("fileSize"));	
		worksPriv.setSrchDivs(multipartRequest.getParameter("srchDivs"));
		worksPriv.setSrchText(StringUtil.nullToEmpty(multipartRequest.getParameter("srchText")) );
		worksPriv.setPage_no(multipartRequest.getParameter("page_no") == null ? "1" : multipartRequest.getParameter("page_no") );
		
		String[] chk;
		chk = multipartRequest.getParameterValues("chk");
		
		if(chk != null){
			worksPrivService.fileDelete(chk);
		}
		
		Iterator fileNameIterator = multipartRequest.getFileNames();
		List fileList = new ArrayList();
		
		while(fileNameIterator.hasNext()) {
			MultipartFile multiFile= multipartRequest.getFile((String)fileNameIterator.next());
			if(multiFile.getSize()>0) {
				WorksPrivFile worksPrivFile = FileUploadUtil.uploadFormFilesW(multiFile, realUploadPath);
				fileList.add(worksPrivFile);
			}
		}
		
		worksPriv.setFileList(fileList);
		worksPrivService.goUpdate(worksPriv);
		
		return new ModelAndView("my/myWorksModiSucc");
	}
	
	
}
