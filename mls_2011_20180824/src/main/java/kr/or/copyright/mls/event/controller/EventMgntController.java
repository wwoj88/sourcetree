package kr.or.copyright.mls.event.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.adminEventMgnt.model.MlEventItem;
import kr.or.copyright.mls.adminEventMgnt.service.AdminEventMgntWebService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.event.model.EventMgntVO;
import kr.or.copyright.mls.event.model.QnABoardVO;
import kr.or.copyright.mls.event.service.EventMgntService;
import kr.or.copyright.mls.rghtPrps.model.PrpsAttc;
import kr.or.copyright.mls.support.util.FileUploadUtil;
import kr.or.copyright.mls.support.util.SessionUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EventMgntController {
	
	@Autowired
	EventMgntService eventMgntService;
	
	@Autowired
	AdminEventMgntWebService adminEventMgntWebService;

	@RequestMapping("/eventMgnt/eventList")
	public ModelAndView eventUserList(HttpServletRequest req, HttpServletResponse res, EventMgntVO vo){
		vo.setList(eventMgntService.getEventUserList(vo));
		vo.setTotalRow(eventMgntService.getEventUserListTotal(vo));
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("vo",vo);
		mv.setViewName("/eventMgnt/eventList");
		return mv;
	}
	
	
	@RequestMapping("/eventMgnt/eventDetail")
	public ModelAndView getEventUserDetail(HttpServletRequest req, HttpServletResponse res, EventMgntVO vo) {
		
		User user = SessionUtil.getSession(req);
		ModelAndView mv = new ModelAndView();
		if (null != user) {
			vo.setUser_idnt(user.getUserIdnt());
			mv.addObject("partCnt",eventMgntService.getEventPartCnt(vo));
		}
		
		mv.addObject("vo",eventMgntService.getEventUserDetail(vo));
		mv.setViewName("/eventMgnt/eventDetail");
		return mv;
	}
	
	@RequestMapping("/eventMgnt/eventDetailView")
	public ModelAndView getEventUserDetailView(HttpServletRequest req, HttpServletResponse res, EventMgntVO vo) {
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("vo",eventMgntService.getEventUserDetail(vo));
		mv.setViewName("/eventMgnt/eventDetailView");
		return mv;
	}
	
	
	@RequestMapping("/eventMgnt/eventView02")
	public ModelAndView getEventView02(HttpServletRequest req, HttpServletResponse res, EventMgntVO vo) {
		
		User user = SessionUtil.getSession(req);
		vo.setUser_idnt(user.getUserIdnt());
		
		vo.setList(eventMgntService.getEventView02List(vo));
		vo.setTotalRow(eventMgntService.getEventView02Total(vo));
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("listvo", vo);
		mv.addObject("vo",eventMgntService.getEventUserDetail(vo));
		mv.addObject("agree",eventMgntService.getEventAgreeItem(vo));//동의항목 내용
		mv.addObject("partCnt",eventMgntService.getEventPartCnt(vo));
		mv.setViewName("/eventMgnt/eventView02");
		return mv;
	}
	
	@RequestMapping("/eventMgnt/addEventView02")
	public ModelAndView addEventView02(HttpServletRequest req, HttpServletResponse res, EventMgntVO vo) throws Exception {
		
		//아이디 셋팅
		User user = SessionUtil.getSession(req);
		if (null == user || null == user.getUserIdnt() || "".equals(user.getUserIdnt())) {
			ModelAndView mv = new ModelAndView("redirect:/user/user.do?method=goLogin");
			return mv;
		}
		String sessUserIdnt = user.getUserIdnt();
		if (sessUserIdnt.equals(vo.getUser_idnt())) {
			eventMgntService.addEventView02(vo);
		}else{
			throw new Exception("Not Login 로그인 체크 오류 ex)로그인 하지 않은 사용자거나 잘못된 데이터");
		}
		
		return new ModelAndView("redirect:/eventMgnt/eventJoinSucc.do?type=2&event_id="+vo.getEvent_id()+"&name="+URLEncoder.encode(vo.getName(),"EUC-KR")+"&win_anuc_date="+eventMgntService.getEventUserDetail(vo).getWin_anuc_date());
	}
	
	@RequestMapping("/eventMgnt/eventJoinSucc.do")
	public ModelAndView getEventJoinSucc(HttpServletRequest req, HttpServletResponse res, EventMgntVO vo) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("vo", vo);
		mv.setViewName("/eventMgnt/eventJoinSucc");
		return mv;
	}
	
	@RequestMapping("/eventMgnt/eventView01")
	public ModelAndView getEventView01(HttpServletRequest req, HttpServletResponse res, MlEventItem vo, EventMgntVO vvo) {
		
		User user = SessionUtil.getSession(req);
		if(!"view".equals(vvo.getType())){
			if (null == user || null == user.getUserIdnt() || "".equals(user.getUserIdnt())) {
				ModelAndView mv = new ModelAndView("redirect:/user/user.do?method=goLogin");
				return mv;
			}else{
				vvo.setUser_idnt(user.getUserIdnt());
			}
		}
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/eventMgnt/eventView01");
		mv.addObject("list",adminEventMgntWebService.getEventItemList(vo));
		mv.addObject("eventTitle",adminEventMgntWebService.getEventTitle(vo));
		
		mv.addObject("listvo", vvo);
		mv.addObject("vo",eventMgntService.getEventUserDetail(vvo));
		mv.addObject("agree",eventMgntService.getEventAgreeItem(vvo));//동의항목 내용
		mv.addObject("partCnt",eventMgntService.getEventPartCnt(vvo));
		
		return mv;
	}
	
	@RequestMapping("/eventMgnt/addEventView01")
	public ModelAndView addEventView01(HttpServletRequest req, HttpServletResponse res, EventMgntVO vo) throws Exception {
		
		User user = SessionUtil.getSession(req);
		if (null == user || null == user.getUserIdnt() || "".equals(user.getUserIdnt())) {
			ModelAndView mv = new ModelAndView("redirect:/user/user.do?method=goLogin");
			return mv;
		}
		vo.setUser_idnt(user.getUserIdnt());
		
		
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartRequest; 
		multipartRequest = multipartResolver.resolveMultipart(req);
		Map<String, MultipartFile> files = multipartRequest.getFileMap();
		
		String event_id = multipartRequest.getParameter("event_id");
		String user_id = user.getUserIdnt();
		String item_id_arr[] = multipartRequest.getParameterValues("item_id");
		String item_type_cd_arr[] = multipartRequest.getParameterValues("item_type_cd");
		
		vo.setName(multipartRequest.getParameter("name"));
		vo.setSex(multipartRequest.getParameter("sex"));
		vo.setBirth_dtae(multipartRequest.getParameter("birth_dtae"));
		vo.setMobl_phon(multipartRequest.getParameter("mobl_phon"));
		vo.setMail(multipartRequest.getParameter("mail"));
		vo.setEvent_id(event_id);
		vo.setUser_id(user_id);
		
		ArrayList<EventMgntVO> action1 = new ArrayList<EventMgntVO>();//텍스트계열용
		for (int i = 0; i < item_id_arr.length; i++) {
			String itemId = item_id_arr[i];
			switch (Integer.parseInt(item_type_cd_arr[i])) {
				case 1:
					if (multipartRequest.getParameter("obj_"+itemId) != null && !"".equals(multipartRequest.getParameter("obj_"+itemId))) {
						EventMgntVO mm = new EventMgntVO();
						mm.setEvent_id(event_id);
						mm.setItem_id(itemId);
						mm.setRslt_desc(multipartRequest.getParameter("obj_"+itemId));
						mm.setRgst_idnt(user_id);
						action1.add(mm);
					}
				break;
				case 2:
					if (multipartRequest.getParameter("obj_"+itemId) != null && !"".equals(multipartRequest.getParameter("obj_"+itemId))) {
						EventMgntVO m2 = new EventMgntVO();
						m2.setEvent_id(event_id);
						m2.setItem_id(itemId);
						m2.setRslt_desc(multipartRequest.getParameter("obj_"+itemId));
						m2.setRgst_idnt(user_id);
						action1.add(m2);
					}
				break;
				case 3:
					if (multipartRequest.getParameterValues("obj_"+itemId) != null) {
						String[] itemArr = multipartRequest.getParameterValues("obj_"+itemId);
						for (int j = 0; j < itemArr.length; j++) {
							EventMgntVO m3 = new EventMgntVO();
							m3.setEvent_id(event_id);
							m3.setItem_id(itemId);
							m3.setRgst_idnt(user_id);
							m3.setEvent_item_choice_id(itemArr[j]);
							m3.setRslt_desc(multipartRequest.getParameter("obj_txt_"+itemId+"_"+itemArr[j]));
							action1.add(m3);
						}
					}
				break;
				case 4: 
					EventMgntVO m4 = new EventMgntVO();
					PrpsAttc prpsAttc = FileUploadUtil.uploadPrpsFiles(files.get("obj_"+itemId),FileUtil.uploadPath()+"event/");
					m4.setEvent_id(event_id);
					m4.setItem_id(itemId);
					m4.setRgst_idnt(user_id);
					m4.setRslt_file_name(prpsAttc.getFILE_NAME());
					m4.setRslt_file_path(prpsAttc.getFILE_PATH()+prpsAttc.getREAL_FILE_NAME());
					action1.add(m4);
				break;
				default:	break;
			}
		}
		
		//vo셋팅
		vo.setAction1(action1);
		
		eventMgntService.addEventView01(vo);
		
		return new ModelAndView("redirect:/eventMgnt/eventJoinSucc.do?type=1&event_id="+vo.getEvent_id()+"&name="+URLEncoder.encode(vo.getName(),"EUC-KR")+"&win_anuc_date="+eventMgntService.getEventUserDetail(vo).getWin_anuc_date());
	}
	
	@RequestMapping("/eventMgnt/winningList")
	public ModelAndView getWinningList(HttpServletRequest req, HttpServletResponse res, EventMgntVO vo){
		vo.setList(eventMgntService.getWinningList(vo));
		vo.setTotalRow(eventMgntService.getWinningListTotal(vo));
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("vo",vo);
		mv.setViewName("/eventMgnt/winningList");
		return mv;
	}
	
	@RequestMapping("/eventMgnt/winningDetail")
	public ModelAndView getwinningDetail(HttpServletRequest req, HttpServletResponse res, EventMgntVO vo) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("vo",eventMgntService.getEventUserDetail(vo));
		mv.setViewName("/eventMgnt/winningDetail");
		return mv;
	}
	
	@RequestMapping("/eventMgnt/topMenu")
	public ModelAndView getTopMenu(HttpServletRequest req, HttpServletResponse res, EventMgntVO vo) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("qnayn",eventMgntService.getTopMenu(9));
		mv.setViewName("/eventMgnt/topMenu");
		return mv;
	}
	
	@RequestMapping("/eventMgnt/qnaList")
	public ModelAndView getQnaList(HttpServletRequest req, HttpServletResponse res, QnABoardVO vo) throws Exception {
		vo.setMenu_seqn("9");
		vo.setTotalRow(eventMgntService.getQnaListCnt(vo));
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("vo",vo);
		mv.addObject("list",eventMgntService.getQnaList(vo));
		mv.setViewName("/eventMgnt/qnaList");
		return mv;
	}
	
	@RequestMapping("/eventMgnt/qnaRegi")
	public ModelAndView getQnaRegi(HttpServletRequest req, HttpServletResponse res, QnABoardVO vo) throws Exception {
		User user = SessionUtil.getSession(req);
		if (null == user || null == user.getUserIdnt() || "".equals(user.getUserIdnt())) {
			ModelAndView mv = new ModelAndView("redirect:/user/user.do?method=goLogin");
			return mv;
		}
		vo.setRgst_idnt(user.getUserIdnt());
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("vo", vo);
		mv.setViewName("/eventMgnt/qnaRegi");
		return mv;
	}
	
	@RequestMapping("/eventMgnt/addQnaRegi")
	public ModelAndView addQnaRegi(HttpServletRequest req, HttpServletResponse res, QnABoardVO vo) throws Exception {
		User user = SessionUtil.getSession(req);
		if (null == user || null == user.getUserIdnt() || "".equals(user.getUserIdnt())) {
			ModelAndView mv = new ModelAndView("redirect:/user/user.do?method=goLogin");
			return mv;
		}
		vo.setRgst_name(user.getUserName());
		vo.setRgst_idnt(user.getUserIdnt());
		
		eventMgntService.addQnaRegi(vo);
		return new ModelAndView("redirect:/eventMgnt/qnaList.do?nowPage="+vo.getNowPage());
	}
	
	
	@RequestMapping("/eventMgnt/qnaDetail")
	public ModelAndView getQnaDetail(HttpServletRequest req, HttpServletResponse res, QnABoardVO vo) throws Exception {
		
		QnABoardVO detl =  eventMgntService.getQnaDetail(vo);
		
		User user = SessionUtil.getSession(req);
		if ("N".equals(detl.getOpen_ysno())) {
			if(!detl.getRgst_idnt().equals(user.getUserIdnt())){
				ModelAndView mv = new ModelAndView("redirect:/eventMgnt/qnaList.do?comp=N");
				return mv;
			}
		}
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/eventMgnt/qnaDetail");
		mv.addObject("vo", detl);
		return mv;
	}
	
	
	@RequestMapping("/eventMgnt/qnaDelete")
	public ModelAndView delQnaDelete(HttpServletRequest req, HttpServletResponse res, QnABoardVO vo) throws Exception {
		eventMgntService.delQnaDelete(vo);
		ModelAndView mv = new ModelAndView("redirect:/eventMgnt/qnaList.do");
		return mv;
	}
	
	@RequestMapping("/eventMgnt/qnaModi")
	public ModelAndView qnaModi(HttpServletRequest req, HttpServletResponse res, QnABoardVO vo) throws Exception {
		QnABoardVO detl =  eventMgntService.getQnaDetail(vo);
		
		User user = SessionUtil.getSession(req);
		if(!detl.getRgst_idnt().equals(user.getUserIdnt())){
			ModelAndView mv = new ModelAndView("redirect:/eventMgnt/qnaList.do?comp=KIN");
			return mv;
		}
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/eventMgnt/qnaModi");
		mv.addObject("vo", detl);
		return mv;
	}
	
	@RequestMapping("/eventMgnt/uptQnaModi")
	public ModelAndView uptQnaModi(HttpServletRequest req, HttpServletResponse res, QnABoardVO vo) throws Exception {
		
		User user = SessionUtil.getSession(req);
		if (null == user || null == user.getUserIdnt() || "".equals(user.getUserIdnt())) {
			ModelAndView mv = new ModelAndView("redirect:/user/user.do?method=goLogin");
			return mv;
		}
		vo.setModi_idnt(user.getUserIdnt());
		vo.setModi_name(user.getUserName());
		eventMgntService.uptQnaModi(vo);
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/eventMgnt/qnaDetail.do?menu_seqn=9&bord_seqn="+vo.getBord_seqn()+"&threaded="+vo.getThreaded()+"&nowPage="+vo.getNowPage());
		return mv;
	}
}