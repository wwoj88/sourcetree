package kr.or.copyright.mls.adminEventMgnt.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.adminEventMgnt.model.MlEventItem;

public interface AdminEventMgntWebService {
	
	public Map getMultiQustItemMgntSeq(Map map);
	public void addMultiQustItemMgnt(Map ml_event_item);
	public void addMultiQustItemMgntChoice(ArrayList list);
	public List getEventItemList(MlEventItem vo);
	public void addEventCorans(MlEventItem vo) throws Exception;
	public void delMultiQustItemMgnt(MlEventItem vo);
	public void uptEventItemSeqn(MlEventItem vo);
	public MlEventItem uptMultiQustItemMgntView(MlEventItem vo);
	public void uptMultiQustItemMgnt(MlEventItem vo);
	public String getEventTitle(MlEventItem vo);
}
