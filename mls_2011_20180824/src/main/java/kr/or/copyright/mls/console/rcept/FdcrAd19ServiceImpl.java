package kr.or.copyright.mls.console.rcept;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd19Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd19Service" )
public class FdcrAd19ServiceImpl extends CommandService implements FdcrAd19Service{

	@Resource( name = "fdcrAd03Dao" )
	private FdcrAd03Dao fdcrAd03Dao;

	/**
	 * 미분배보상금저작물 보고현황 조회
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public void fdcrAd19List1( Map<String, Object> commandMap ) throws Exception{

		List list = (List) fdcrAd03Dao.inmtReptView( commandMap );
		commandMap.put( "ds_list", list );

	}
}
