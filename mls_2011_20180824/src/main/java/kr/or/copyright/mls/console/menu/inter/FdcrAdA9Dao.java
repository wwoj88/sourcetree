package kr.or.copyright.mls.console.menu.inter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface FdcrAdA9Dao {

	// 메뉴 목록조회
	public List selectMenuList();
	
	// 메뉴 삭제
	public void menuDelete(Map map);
	
	// 메뉴 등록
	public int menuInsert(Map map);
	
	// 메뉴 수정
	public void menuUpdate(Map map);
	
	// 그룹 목록 조회
	public List selectGroupList();
	
	// 그룹 상세 조회
	public Map groupInfo(Map map);
	
	// 그룹 메뉴 조회
	public List groupMenuInfo(Map map);
	
	// 그룹 아이디 값 세팅
	int groupIdMax(Map map);
	
	// 그룹 등록
	public void groupInsert(Map map);
	
	// 그룹 메뉴 등록
	public void groupMenuInsert(Map map);
	
	// 그룹 삭제
	public void groupDelete(Map map);
	
	// 그룹 메뉴 삭제
	public void groupMenuDelete(Map map);
	
	// 그룹 수정
	public void groupUpdate(Map map);
	
	
	public Map<String, Object> selectMenuInfo(Map map);
}
