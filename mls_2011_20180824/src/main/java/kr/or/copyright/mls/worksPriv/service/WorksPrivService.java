package kr.or.copyright.mls.worksPriv.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.board.model.Board;
import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.worksPriv.model.WorksPriv;

public interface WorksPrivService {

	
	public ListResult findWorksPrivList(int pageNo, int rowPerPage, Map params);
	
	public List<CodeList> findCodeList(CodeList codeList);
	
	int goInsert(WorksPriv worksPriv) throws Exception;
	
	public WorksPriv goView(WorksPriv worksPriv);
	
	public void goUpdate(WorksPriv worksPriv);

	public void goDelete(WorksPriv worksPriv);
	
	public void fileDelete(String[] chk);
}
