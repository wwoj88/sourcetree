package kr.or.copyright.mls.intg.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.intg.model.Intg;
import kr.or.copyright.mls.intg.service.IntgService;
import kr.or.copyright.mls.support.util.StringUtil;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class IntgController extends MultiActionController {

	//private Log logger = LogFactory.getLog(getClass());

	public IntgService intgService;

	//private String realUploadPath = "C:/home/tmax/mls/web/upload/";  // 실서버용
//	private String realUploadPath = "/home/tmax_dev/mls/web/upload/";  // 개발서버용
	
	public void setIntgService(IntgService intgService) {
		this.intgService = intgService;
	}

	public ModelAndView result(HttpServletRequest request,	HttpServletResponse respone) throws Exception {
		
		Intg IntgDTO = new Intg();
		
	//	ArrayList aList = new ArrayList();
	//	return new ModelAndView("intg/intgSrch", "srchResult", intgService.srchImg(aList));
		
		String title = ServletRequestUtils.getStringParameter(request, "srchWord", "");
		String more = ServletRequestUtils.getStringParameter(request, "more", "");
		String gubun = "0";
		int cate = 0;
		int currentPage = StringUtil.nullToZeroInt(request.getParameter("currentPage") == null ? "1" : request.getParameter("currentPage"));

		
		if( ServletRequestUtils.getStringParameter(request, "cate", "0") !=null && ServletRequestUtils.getStringParameter(request, "cate", "0").length()>0) {
			cate = Integer.parseInt(ServletRequestUtils.getStringParameter(request, "cate", "0"));
		}
		if( ServletRequestUtils.getStringParameter(request, "gubun", "0") !=null && ServletRequestUtils.getStringParameter(request, "gubun", "0").length()>0) {
			gubun = ServletRequestUtils.getStringParameter(request, "gubun", "0");
		}
		
		IntgDTO.setTitle(title);
		IntgDTO.setMore(more);


		// selectBox
		String selectItem[] = {"통합검색","음악저작물","앨범","어문저작물","도서","음악저작권자","어문저작권자","이미지","방송음악보상금","교과용보상금","도서관보상금"};
		String selectBox = "";
		for(int i=0; i<selectItem.length; i++){
			if( i==cate) {
				selectBox += "<option value='"+i+"' selected>"+selectItem[i]+"</option>";
			} else {
				selectBox += "<option value='"+i+"'>"+selectItem[i]+"</option>";
			}
		}
		
		// 결과내검색
		StringTokenizer st = null;
		
		String resultWord = (String)request.getParameter("resultWord");
		ArrayList arrList = new ArrayList();
		
		if( resultWord !=null && resultWord.length()>0) {
			st = new StringTokenizer(resultWord, "★");
	
			while(st.hasMoreTokens()){
				arrList.add(st.nextToken()); 
			}
			
			IntgDTO.setResultWordList(arrList);
		}
		
		List totalImgWorks = null;			// 이미지
		String totalRowImgWorks = "";
		
		List totalBrctWorks = null;			// 방송음악
		String totalRowBrctWorks = "";
		
		List totalSubjWorks = null;			// 교과용
		String totalRowSubjWorks = "";
		
		List totalLibrWorks = null;			// 도서관
		String totalRowLibrWorks = "";
		
		ModelAndView mv = new ModelAndView("intg/intgSrch","Intg", IntgDTO);
	
	
		if(IntgDTO.getTitle() !=null && IntgDTO.getTitle().length()>0) {
			
			// 이미지
			if( gubun.equals("0") || gubun.equals("7")){
				
				totalRowImgWorks = intgService.srchImgCnt(IntgDTO);
				
				
				if(gubun.equals("7") ) {
					IntgDTO.setFrom((currentPage - 1) * 10);	//시작 ROW
					IntgDTO.setTo(currentPage * 10);	// 마지막 ROW	
					
					mv.addObject("totalRow", totalRowImgWorks);
				}
				
				if( Integer.parseInt(totalRowImgWorks) >0){
					totalImgWorks = intgService.srchImg(IntgDTO);
				}
				
			}
			
			// 방송음악
			if( gubun.equals("0") || gubun.equals("8")){
				
				totalRowBrctWorks = intgService.srchBrctCnt(IntgDTO);
				
				
				if(gubun.equals("8") ) {
					IntgDTO.setFrom((currentPage - 1) * 10);	//시작 ROW
					IntgDTO.setTo(currentPage * 10);	// 마지막 ROW	
					
					mv.addObject("totalRow", totalRowBrctWorks);
				}
				
				if( Integer.parseInt(totalRowBrctWorks) >0){
					totalBrctWorks = intgService.srchBrct(IntgDTO);
				}
				
			}
			
			// 교과용
			if( gubun.equals("0") || gubun.equals("9")){
				
				totalRowSubjWorks = intgService.srchSubjCnt(IntgDTO);
				
				
				if(gubun.equals("9") ) {
					IntgDTO.setFrom((currentPage - 1) * 10);	//시작 ROW
					IntgDTO.setTo(currentPage * 10);	// 마지막 ROW	
					
					mv.addObject("totalRow", totalRowSubjWorks);
				}
				
				if( Integer.parseInt(totalRowSubjWorks) >0){
					totalSubjWorks = intgService.srchSubj(IntgDTO);
				}
				
			}
			
			// 도서관
			if( gubun.equals("0") || gubun.equals("10")){
				
				totalRowLibrWorks = intgService.srchLibrCnt(IntgDTO);
				
				
				if(gubun.equals("10") ) {
					IntgDTO.setFrom((currentPage - 1) * 10);	//시작 ROW
					IntgDTO.setTo(currentPage * 10);	// 마지막 ROW	
					
					mv.addObject("totalRow", totalRowLibrWorks);
				}
				
				if( Integer.parseInt(totalRowLibrWorks) >0){
					totalLibrWorks = intgService.srchLibr(IntgDTO);
				}
				
			}

			
		}
		
		mv.addObject("resultWord", resultWord);		// 결과내검색
		mv.addObject("currentPage", currentPage);
		mv.addObject("cate",cate);
		mv.addObject("gubun", gubun);
		mv.addObject("selectBox",selectBox);
		mv.addObject("srchWord",title);
		
		mv.addObject("totalRowImgWorks", totalRowImgWorks);
		mv.addObject("totalImgWorks", totalImgWorks);
		mv.addObject("totalRowBrctWorks", totalRowBrctWorks);
		mv.addObject("totalBrctWorks", totalBrctWorks);
		mv.addObject("totalRowSubjWorks", totalRowSubjWorks);
		mv.addObject("totalSubjWorks", totalSubjWorks);
		mv.addObject("totalRowLibrWorks", totalRowLibrWorks);
		mv.addObject("totalLibrWorks", totalLibrWorks);
		
		return mv;
	}

	public ModelAndView goImgDetail(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {

		String imgeSeq = ServletRequestUtils.getStringParameter(request, "imgeSeq", "");
		
		Intg IntgDTO = new Intg();
		
		IntgDTO.setImgeSeq(imgeSeq);
		
		List ImgDetail = null;			// 이미지
		
		ImgDetail = intgService.detailImg(IntgDTO);
		
		ModelAndView mv = new ModelAndView("intg/intgImgDetl");
		mv.addObject("ImgDetail", ImgDetail);	
		
		return mv;
	}
	
	public ModelAndView goBrctDetail(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		String inmtSeqn = ServletRequestUtils.getStringParameter(request, "inmtSeqn", "");
		
		Intg IntgDTO = new Intg();
		
		IntgDTO.setInmtSeqn(inmtSeqn);
		
		List BrctDetail = null;			// 방송음악 보상금
		
		BrctDetail = intgService.detailBrct(IntgDTO);
		
		ModelAndView mv = new ModelAndView("intg/intgBrctDetl");
		mv.addObject("BrctDetail", BrctDetail);		
		
		return mv;
	}
	
	public ModelAndView goSubjDetail(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		String inmtSeqn = ServletRequestUtils.getStringParameter(request, "inmtSeqn", "");
		
		Intg IntgDTO = new Intg();
		
		IntgDTO.setInmtSeqn(inmtSeqn);
		
		List SubjDetail = null;			// 교과용 보상금
		
		SubjDetail = intgService.detailSubj(IntgDTO);
		
		ModelAndView mv = new ModelAndView("intg/intgSubjDetl");
		mv.addObject("SubjDetail", SubjDetail);		
		
		return mv;
	}
	
	public ModelAndView goLibrDetail(HttpServletRequest request,
			HttpServletResponse respone) throws Exception {
		
		String inmtSeqn = ServletRequestUtils.getStringParameter(request, "inmtSeqn", "");
		
		Intg IntgDTO = new Intg();
		
		IntgDTO.setInmtSeqn(inmtSeqn);
		
		List LibrDetail = null;			// 도서관 보상금
		
		LibrDetail = intgService.detailLibr(IntgDTO);
		
		ModelAndView mv = new ModelAndView("intg/intgLibrDetl");
		mv.addObject("LibrDetail", LibrDetail);		
		
		return mv;
	}
}
