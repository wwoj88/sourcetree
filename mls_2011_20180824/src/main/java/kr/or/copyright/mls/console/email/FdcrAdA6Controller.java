package kr.or.copyright.mls.console.email;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.ntcn.FdcrAd75ServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 안내메일관리 > 발신함
 * 
 * @author ljh
 */
@Controller
public class FdcrAdA6Controller extends DefaultController{

	@Resource( name = "fdcrAdA6Service" )
	private FdcrAdA6ServiceImpl fdcrAdA6Service;

	@Resource( name = "fdcrAd75Service" )
	private FdcrAd75ServiceImpl fdcrAd75Service;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdA6Controller.class );

	/**
	 * 발신함 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA6List1.page" )
	public String fdcrAdA6List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		fdcrAdA6Service.fdcrAdA6List1( commandMap );

		model.addAttribute( "ds_list", commandMap.get( "ds_list" ) );
		System.out.println( "발신함 목록 조회" );
		return "email/fdcrAdA6List1.tiles";
	}

	/**
	 * 발신함 상세 조회(송수신내역/메일의내용)
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA6View1.page" )
	public String fdcrAdA6View1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{

		commandMap.put( "MSG_ID", request.getParameter( "MSG_ID" ) );
		commandMap.put( "GUBUN", request.getParameter( "GUBUN" ) );

		fdcrAdA6Service.fdcrAdA6View1( commandMap );

		model.addAttribute( "commandMap", commandMap);
		model.addAttribute( "ds_view", commandMap.get( "ds_view" ) );
		model.addAttribute( "ds_detail", commandMap.get( "ds_detail" ) );

		System.out.println( "발신함 상세 조회" );
		return "email/fdcrAdA6View1";
	}

	/**
	 * 발신함 첨부 파일 다운로드
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/email/fdcrAdA6Down1.page" )
	public void fdcrAdA6Down1( HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{

		String savePath = new String( kr.or.copyright.mls.support.constant.Constants.getProperty( "file_upload_path" ) );
		Map<String, Object> fileInfo = fdcrAd75Service.selectAttachFileInfo( commandMap );

		String fileRealNm = EgovWebUtil.getString( fileInfo, "FILE_REALNAME" );
		String BRDID = EgovWebUtil.getString( fileInfo, "BRDID" );
		String fileNm = EgovWebUtil.getString( fileInfo, "FILE_NAME" );
		String downPath = savePath + fileNm;
		if( !"".equals( fileRealNm ) ){
			download( request, response, downPath, fileRealNm );
		}
	}

}
