package kr.or.copyright.mls.console.menu;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.console.ConsoleCommonService;
import kr.or.copyright.mls.console.ConsoleLoginUser;
import kr.or.copyright.mls.console.DefaultController;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.menu.inter.FdcrAdB0Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 메뉴관리 > 그룹메뉴관리
 * 
 * @author wizksy
 */
@Controller
public class FdcrAdB0Controller extends DefaultController{

	@Resource( name = "fdcrAdB0Service" )
	private FdcrAdB0Service fdcrAdB0Service;

	@Resource( name = "consoleCommonService" )
	private ConsoleCommonService consoleCommonService;

	private static final Logger logger = LoggerFactory.getLogger( FdcrAdB0Controller.class );

	/**
	 * 그룹 목록 조회
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdB0List1.page" )
	public String fdcrAdA9List1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdB0List1 Start" );
		fdcrAdB0Service.fdcrAdB0List1( commandMap );
		model.addAttribute( "ds_group_info", commandMap.get( "ds_group_info" ) );
		return "menu/fdcrAdB0List1.tiles";
	}

	/**
	 * 그룹 메뉴 수정폼
	 * 
	 * @param model
	 * @param commandMap
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdB0UpdateForm1.page" )
	public String fdcrAdB0UpdateForm1( ModelMap model,
		Map<String, Object> commandMap,
		HttpServletRequest request,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdB0UpdateForm1 Start" );
		fdcrAdB0Service.fdcrAdB0View1( commandMap );
		model.addAttribute( "ds_group_info", commandMap.get( "ds_group_info" ) );
		return "menu/fdcrAdB0UpdateForm1.tiles";
	}

	/**
	 * 메뉴 트리
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdB0UpdateForm1Sub1.page" )
	public void fdcrAdB0UpdateForm1Sub1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdB0UpdateForm1Sub1 Start" );
		ArrayList<Map<String, Object>> treeList = new ArrayList<Map<String, Object>>();
		Map<String, Object> rootTree = new HashMap<String, Object>();
		rootTree.put( "id", "menuId_0" );
		rootTree.put( "text", "전체보기" );
		rootTree.put( "icon", "folder" );
		Map<String, Object> rootTreeState = new HashMap<String, Object>();
		rootTreeState.put( "opened", true );
		rootTree.put( "state", rootTreeState );
		treeList.add( rootTree );

		try{
			fdcrAdB0Service.fdcrAdB0List2( commandMap );
			ArrayList<Map<String, Object>> groupMenuList = (ArrayList<Map<String, Object>>) commandMap.get( "ds_group_menu" );
			ArrayList<Map<String, Object>> menuList = (ArrayList<Map<String, Object>>) consoleCommonService.selectMenuGroupList( new HashMap<String,Object>() );
			for( int i = 0; i < menuList.size(); i++ ){
				Map<String, Object> map = menuList.get( i );
				int menuId1 = ( (BigDecimal) map.get( "MENU_ID" ) ).intValue();
				String menuId = ( (BigDecimal) map.get( "MENU_ID" ) ).intValue() + "";
				String upperMenuId = ( (BigDecimal) map.get( "PARENT_MENU_ID" ) ).intValue() + "";
				String menuNm = EgovWebUtil.getString( map, "MENU_TITLE" );
				String menuDesc = EgovWebUtil.getString( map, "MENU_DESC" );
				int childCnt = EgovWebUtil.getToInt( map, "CHILD_CNT" );
				String html = "";
				html += "<li style=\"width:300px;float:right\" title=\""+menuDesc+"\">"+menuNm+"</li>";
				Map<String, Object> tree = new HashMap<String, Object>();
				tree.put( "id", "menuId_" + menuId );
				tree.put( "text", html );
				Map<String, Object> treeState = new HashMap<String, Object>();
				treeState.put( "opened", true );
				for(Map<String,Object> selMenu :  groupMenuList){
					int selMenuId = ( (BigDecimal) selMenu.get( "MENU_ID" ) ).intValue();
					if(selMenuId==menuId1 && childCnt == 0){
						treeState.put( "selected", true );
						break;
					}
				}
				if( childCnt > 0 ){
					tree.put( "icon", "folder" );
				}else{
					tree.put( "icon", "file" );
				}
				// if("11001".equals( menuId )){
				// treeState.put( "selected", true );
				// }
				tree.put( "state", treeState );
				tree.put( "parent", "menuId_" + upperMenuId );
				setChildNode( treeList, tree );
			}
		}
		catch( RuntimeException e ){
			// e.printStackTrace();
			System.out.println( "실행중 에러발생 " );
		}
		returnAjaxJsonArray( response, treeList );
	}

	public void setChildNode( ArrayList<Map<String, Object>> list,
		Map<String, Object> node ){
		String parent = (String) node.get( "parent" );

		for( int i = 0; i < list.size(); i++ ){
			Map<String, Object> compareMap = list.get( i );
			String compMenuId = (String) compareMap.get( "id" );
			ArrayList<Map<String, Object>> compChildList = (ArrayList<Map<String, Object>>) compareMap.get( "children" );

			if( compMenuId.equals( parent ) ){
				ArrayList<Map<String, Object>> childList = (ArrayList<Map<String, Object>>) compareMap.get( "children" );
				if( null != childList && childList.size() > 0 ){
					childList.add( node );
				}else{
					childList = new ArrayList<Map<String, Object>>();
					childList.add( node );
				}
				compareMap.put( "children", childList );
			}else{
				if( null != compChildList ){
					setChildNode( compChildList, node );
				}
			}
		}
	}

	/**
	 * 그룹 메뉴 수정
	 * 
	 * @param model
	 * @param request
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping( value = "/console/menu/fdcrAdB0Update1.page" )
	public String fdcrAdB0Update1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		logger.debug( "fdcrAdB0Update1 Start" );
		String MENU_IDS = request.getParameter( "MENU_ID" );
		String[] MENU_ID = MENU_IDS.split( "," );
		commandMap.put( "MODI_IDNT", ConsoleLoginUser.getUserId() );
		commandMap.put( "RGST_IDNT", ConsoleLoginUser.getUserId() );
		commandMap.put( "MENU_ID", MENU_ID );
		boolean isSuccess = fdcrAdB0Service.fdcrAdB0Update1( commandMap );
		if( isSuccess ){
			return returnUrl(
				model,
				"저장했습니다.",
				"/console/menu/fdcrAdB0UpdateForm1.page?GROUP_YMD=" + EgovWebUtil.getString( commandMap, "GROUP_YMD" )
					+ "&GROUP_SEQ=" + EgovWebUtil.getString( commandMap, "GROUP_SEQ" ) );
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/menu/fdcrAdB0UpdateForm1.page?GROUP_YMD=" + EgovWebUtil.getString( commandMap, "GROUP_YMD" )
					+ "&GROUP_SEQ=" + EgovWebUtil.getString( commandMap, "GROUP_SEQ" ) );
		}
	}
	
	
	/**
	  * <PRE>
	  * 간략 : 그룹 메뉴 삭제
	  * 상세 : 
	  * <PRE>
	  * @param model
	  * @param request
	  * @param commandMap
	  * @param response
	  * @return
	  * @throws Exception 
	  */
	@RequestMapping( value = "/console/menu/fdcrAdB0Delete1.page" )
	public String fdcrAdB0Delete1( ModelMap model,
		HttpServletRequest request,
		Map<String, Object> commandMap,
		HttpServletResponse response ) throws Exception{
		
		logger.debug( "fdcrAdB0Delete1 Start" );
		boolean isSuccess = fdcrAdB0Service.fdcrAdB0Delete1( commandMap );
		if( isSuccess ){
			return returnUrl(
				model,
				"삭제했습니다.",
				"/console/menu/fdcrAdB0List1.page");
		}else{
			return returnUrl(
				model,
				"실패했습니다.",
				"/console/menu/fdcrAdB0List1.page");
		}
	}

}
