package kr.or.copyright.mls.main.dao;

import java.util.List;

import kr.or.copyright.mls.main.model.Main;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class MainSqlMapDao extends SqlMapClientDaoSupport implements MainDao {

	//private Log logger = LogFactory.getLog(getClass());
	
	public List qustList(Main main) {

		return (List)getSqlMapClientTemplate().queryForList("Main.qustList", main);
	}
	
	public List inmtList(Main main) {

		return (List)getSqlMapClientTemplate().queryForList("Main.inmtList", main);
	}
	
	public List workList(Main main) {

		return (List)getSqlMapClientTemplate().queryForList("Main.workList", main);
	}
	
	public void insertUserCount() {
		getSqlMapClientTemplate().insert("Main.insertUserCount",null);
	}
	
	public List notiList(Main main) {

		return (List)getSqlMapClientTemplate().queryForList("Main.notiList", main);
	}
	
	public List promPotoList(Main main) {

		return (List)getSqlMapClientTemplate().queryForList("Main.promPotoList", main);
	}
	
	public List promMovieList(Main main) {

		return (List)getSqlMapClientTemplate().queryForList("Main.promMovieList", main);
	}
	
	public List lawList(Main main) {

		return (List)getSqlMapClientTemplate().queryForList("Main.lawList", main);
	}
	
	public List workNoneList(Main main) {

		return (List)getSqlMapClientTemplate().queryForList("Main.workNoneList", main);
	}
	
	public String alltInmt() {
		return ""+getSqlMapClientTemplate().queryForObject("Main.alltInmt");
	}
	
	public String alltInmt202(Main main) {
		return ""+getSqlMapClientTemplate().queryForObject("Main.alltInmt202", main);
	}
	
	public String alltInmt203(Main main) {
		return ""+getSqlMapClientTemplate().queryForObject("Main.alltInmt203", main);
	}
	
	public String alltInmtL(Main main) {
		return ""+getSqlMapClientTemplate().queryForObject("Main.alltInmtL", main);
	}
	
	public String alltInmtS(Main main) {
		return ""+getSqlMapClientTemplate().queryForObject("Main.alltInmtS", main);
	}
	
	public List anucBord01(){
		return (List) getSqlMapClientTemplate().queryForList("Main.anucBord01");
	}
	public List anucBord06(){
		return (List) getSqlMapClientTemplate().queryForList("Main.anucBord06");
	}
	public List anucBord03(){
		return (List) getSqlMapClientTemplate().queryForList("Main.anucBord03");
	}
	public List anucBord04(){
		return (List) getSqlMapClientTemplate().queryForList("Main.anucBord04");
	}
	public List anucBord05(){
		return (List) getSqlMapClientTemplate().queryForList("Main.anucBord05");
	}
	public List anucBord07(){
		return (List) getSqlMapClientTemplate().queryForList("Main.anucBord07");
	}
	public List QnABord(){
		return (List) getSqlMapClientTemplate().queryForList("Main.QnABord");
	}
	public List statSrch(){
		return (List) getSqlMapClientTemplate().queryForList("Main.statSrch");
	}
	public List notiList(){
		return (List) getSqlMapClientTemplate().queryForList("Main.notiList");
	}
}

