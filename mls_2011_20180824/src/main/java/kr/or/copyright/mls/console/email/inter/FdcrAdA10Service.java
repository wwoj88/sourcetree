package kr.or.copyright.mls.console.email.inter;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface FdcrAdA10Service{

	/**
	 * 메일폼관리 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAdA10List1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 그룹별 메일폼목록
	 * 
	 * @return
	 */
	public List selectFormGroupList(Map<String, Object> commandMap) throws Exception;
	
	// 메뉴 조회
	public Map<String, Object> selectFormInfo(Map<String, Object> commandMap) throws Exception;
	
	public boolean formUpdate( Map<String, Object> commandMap ) throws Exception;
	
	public boolean formInsert( Map<String, Object> commandMap ) throws Exception;

	public boolean formDelete( Map<String, Object> commandMap ) throws Exception;
}
