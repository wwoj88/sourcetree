package kr.or.copyright.mls.ajaxTest.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.ajaxTest.model.Code;
import kr.or.copyright.mls.common.extend.RightDAOImpl;
import kr.or.copyright.mls.user.model.ZipCodeRoad;

import org.springframework.stereotype.Repository;

@Repository("CodeListDAO")
public class CodeListSqlMapDao extends RightDAOImpl implements CodeListDAO {

	private static final String NAMESPACE = "AjaxTest";
	
	@SuppressWarnings("unchecked")
	public List<Code> getInitCodeList() {
		return (List<Code>)getSqlMapClientTemplate().queryForList(NAMESPACE+".getCodeList");
	}

	@SuppressWarnings("unchecked")
	public List<Code> getCodeList(Map params){
		return (List<Code>)getSqlMapClientTemplate().queryForList(NAMESPACE+".getCodeList1",params);
	}

	@SuppressWarnings("unchecked")
	public List<ZipCodeRoad> getRoadCodeList(Map params) {
	    return (List<ZipCodeRoad>)getSqlMapClientTemplate().queryForList(NAMESPACE+".getRoadCodeList",params);
	}

	@SuppressWarnings("unchecked")
	public List<ZipCodeRoad> getInitRoadCodeList() {
	    return (List<ZipCodeRoad>)getSqlMapClientTemplate().queryForList(NAMESPACE+".getInitRoadCodeList");
	}
}
