package kr.or.copyright.mls.console.rcept;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.effort.inter.FdcrAd03Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd20Dao;
import kr.or.copyright.mls.console.rcept.inter.FdcrAd22Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd22Service" )
public class FdcrAd22ServiceImpl extends CommandService implements FdcrAd22Service{

	@Resource( name = "fdcrAd03Dao" )
	private FdcrAd03Dao fdcrAd03Dao;

	@Resource( name = "fdcrAd20Dao" )
	private FdcrAd20Dao fdcrAd20Dao;

	/**
	 * 일괄수정
	 * 
	 * @param commandMap
	 * @param excelList
	 * @return
	 * @throws Exception
	 */
	public boolean fdcrAd22Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> excelList ) throws Exception{
		boolean result = false;
		try{

			List returnList = new ArrayList();

			if( commandMap.get( "GENRE" ).equals( "M" ) ){ // 방송음악
				returnList = fdcrAd20Dao.brctInmtUpdateUpload( commandMap, excelList );
			}else if( commandMap.get( "GENRE" ).equals( "S" ) ){ // 교과용
				returnList = fdcrAd20Dao.subjInmtUpdateUpload( commandMap, excelList );
			}else if( commandMap.get( "GENRE" ).equals( "L" ) ){ // 도서관
				returnList = fdcrAd20Dao.librInmtUpdateUpload( commandMap, excelList );
			}else if( commandMap.get( "GENRE" ).equals( "A" ) ){ // 수업목적
				returnList = fdcrAd20Dao.lssnInmtUpdateUpload( commandMap, excelList );
			}
			commandMap.put( "ds_excel", returnList );

			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}
}
