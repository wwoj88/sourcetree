package kr.or.copyright.mls.console.cnsgn.inter;

public class AdminWorksRegistation {
	
	int no;
	
	String ca_id;
	
	String manager_id;
	
	String genre;
	
	String name;
	
	String duty;
	
	String e_mail;
	
	String phone_num;
	
	String dn;
	
	String serial_number;
	
	String sign_cert;
	
	String original_message;
	
	String sign_value;
	
	String insert_id;
	
	String insert_date;
	
	String modify_id;
	
	String modify_date;
	
	String rept_works_cont;
	
	String modi_works_cont;
	
	String startDate;
	
	String endDate;
	
	String reportDate;
	
	String key;

	String mls_genre_cd;
	
	String mls_rept_seqn;
	//사설인증서 정보
	String cc_dn;				
	
	String cc_sn;				
	
	String cc_get_r;				
	
	String cc_signed_data;				
	
	String cr_id;

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getCa_id() {
		return ca_id;
	}

	public void setCa_id(String ca_id) {
		this.ca_id = ca_id;
	}

	public String getManager_id() {
		return manager_id;
	}

	public void setManager_id(String manager_id) {
		this.manager_id = manager_id;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDuty() {
		return duty;
	}

	public void setDuty(String duty) {
		this.duty = duty;
	}

	public String getE_mail() {
		return e_mail;
	}

	public void setE_mail(String e_mail) {
		this.e_mail = e_mail;
	}

	public String getPhone_num() {
		return phone_num;
	}

	public void setPhone_num(String phone_num) {
		this.phone_num = phone_num;
	}

	public String getDn() {
		return dn;
	}

	public void setDn(String dn) {
		this.dn = dn;
	}

	public String getSerial_number() {
		return serial_number;
	}

	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}

	public String getSign_cert() {
		return sign_cert;
	}

	public void setSign_cert(String sign_cert) {
		this.sign_cert = sign_cert;
	}

	public String getOriginal_message() {
		return original_message;
	}

	public void setOriginal_message(String original_message) {
		this.original_message = original_message;
	}

	public String getSign_value() {
		return sign_value;
	}

	public void setSign_value(String sign_value) {
		this.sign_value = sign_value;
	}

	public String getInsert_id() {
		return insert_id;
	}

	public void setInsert_id(String insert_id) {
		this.insert_id = insert_id;
	}

	public String getInsert_date() {
		return insert_date;
	}

	public void setInsert_date(String insert_date) {
		this.insert_date = insert_date;
	}

	public String getModify_id() {
		return modify_id;
	}

	public void setModify_id(String modify_id) {
		this.modify_id = modify_id;
	}

	public String getModify_date() {
		return modify_date;
	}

	public void setModify_date(String modify_date) {
		this.modify_date = modify_date;
	}

	public String getRept_works_cont() {
		return rept_works_cont;
	}

	public void setRept_works_cont(String rept_works_cont) {
		this.rept_works_cont = rept_works_cont;
	}

	public String getModi_works_cont() {
		return modi_works_cont;
	}

	public void setModi_works_cont(String modi_works_cont) {
		this.modi_works_cont = modi_works_cont;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMls_genre_cd() {
		return mls_genre_cd;
	}

	public void setMls_genre_cd(String mls_genre_cd) {
		this.mls_genre_cd = mls_genre_cd;
	}

	public String getMls_rept_seqn() {
		return mls_rept_seqn;
	}

	public void setMls_rept_seqn(String mls_rept_seqn) {
		this.mls_rept_seqn = mls_rept_seqn;
	}

	public String getCc_dn() {
		return cc_dn;
	}

	public void setCc_dn(String cc_dn) {
		this.cc_dn = cc_dn;
	}

	public String getCc_sn() {
		return cc_sn;
	}

	public void setCc_sn(String cc_sn) {
		this.cc_sn = cc_sn;
	}

	public String getCc_get_r() {
		return cc_get_r;
	}

	public void setCc_get_r(String cc_get_r) {
		this.cc_get_r = cc_get_r;
	}

	public String getCc_signed_data() {
		return cc_signed_data;
	}

	public void setCc_signed_data(String cc_signed_data) {
		this.cc_signed_data = cc_signed_data;
	}

	public String getCr_id() {
		return cr_id;
	}

	public void setCr_id(String cr_id) {
		this.cr_id = cr_id;
	}
	
	
}
