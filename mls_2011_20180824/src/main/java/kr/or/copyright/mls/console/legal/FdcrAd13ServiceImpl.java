package kr.or.copyright.mls.console.legal;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.ConsoleCommonDao;
import kr.or.copyright.mls.console.EgovWebUtil;
import kr.or.copyright.mls.console.effort.inter.FdcrAd01Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd06Dao;
import kr.or.copyright.mls.console.legal.inter.FdcrAd13Service;

import org.springframework.stereotype.Service;

@Service( "fdcrAd13Service" )
public class FdcrAd13ServiceImpl extends CommandService implements FdcrAd13Service{

	// AdminStatMgntSqlMapDao
	@Resource( name = "fdcrAd06Dao" )
	private FdcrAd06Dao fdcrAd06Dao;

	// AdminStatBoardSqlMapDao
	@Resource( name = "fdcrAd01Dao" )
	private FdcrAd01Dao fdcrAd01Dao;

	// AdminCommonSqlMapDao
	@Resource( name = "consoleCommonDao" )
	private ConsoleCommonDao consoleCommonDao;

	/**
	 * 이용승인신청 일괄등록 파일 업로드
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd13Upload1( ArrayList<Map<String, Object>> fileList ) throws Exception{
		boolean result = false;
		try{
			int attachSeqn = consoleCommonDao.getNewAttcSeqn();

			Map<String, Object> fileInfo = new HashMap<String, Object>();
			if( fileList != null && fileList.size() > 0 ){

				fileInfo = fileList.get( 0 );

				Map fileMap = new HashMap();
				fileMap.put( "ATTC_SEQN", attachSeqn );
				fileMap.put( "FILE_ATTC_CD", "BO" );
				fileMap.put( "FILE_NAME_CD", 99 );
				fileMap.put( "FILE_NAME", fileInfo.get( "F_fileName" ) );
				fileMap.put( "FILE_PATH", fileInfo.get( "F_saveFilePath" ) );
				fileMap.put( "FILE_SIZE", fileInfo.get( "F_filesize" ) );
				fileMap.put( "REAL_FILE_NAME", fileInfo.get( "F_orgFileName" ) );

				Map statAttcFileMap = new HashMap();

				statAttcFileMap.put( "ATTC_SEQN", attachSeqn );
				statAttcFileMap.put( "MODI_IDNT", fileMap.get( "RGST_IDNT" ) );

				// 첨부파일 등록
				fdcrAd06Dao.adminFileInsert( fileMap );
				fdcrAd01Dao.statBord06FileInsert( statAttcFileMap );
			}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 법정허락 대상저작물 일괄 삭제
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public boolean fdcrAd13Delete1( Map<String, Object> commandMap ) throws Exception{
		boolean result = false;
		try{
			String WORKS_TITLE = EgovWebUtil.getString( commandMap, "WORKS_TITLE" );
			String WORKS_DIVS_CD = EgovWebUtil.getString( commandMap, "WORKS_DIVS_CD" );

			//for( int i = 0; i < WORKS_TITLES.length; i++ ){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put( "WORKS_TITLE", WORKS_TITLE );
				param.put( "WORKS_DIVS_CD", WORKS_DIVS_CD );
				fdcrAd01Dao.statBord07UpdateAll( param );
			//}
			result = true;
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return result;
	}

}
