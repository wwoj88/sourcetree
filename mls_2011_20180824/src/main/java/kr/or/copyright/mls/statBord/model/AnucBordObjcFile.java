package kr.or.copyright.mls.statBord.model;

public class AnucBordObjcFile {
	private int attcSeqn;
	private long statObjcId;
	private String rgstIdnt;
	private String rgstDttm;
	@Override
	public String toString() {
		return "AnucBordObjcFile [attcSeqn=" + attcSeqn + ", statObjcId="
				+ statObjcId + ", rgstIdnt=" + rgstIdnt + ", rgstDttm="
				+ rgstDttm + "]";
	}
	public int getAttcSeqn() {
		return attcSeqn;
	}
	public void setAttcSeqn(int attcSeqn) {
		this.attcSeqn = attcSeqn;
	}
	public long getStatObjcId() {
		return statObjcId;
	}
	public void setStatObjcId(long statObjcId) {
		this.statObjcId = statObjcId;
	}
	public String getRgstIdnt() {
		return rgstIdnt;
	}
	public void setRgstIdnt(String rgstIdnt) {
		this.rgstIdnt = rgstIdnt;
	}
	public String getRgstDttm() {
		return rgstDttm;
	}
	public void setRgstDttm(String rgstDttm) {
		this.rgstDttm = rgstDttm;
	}
}
