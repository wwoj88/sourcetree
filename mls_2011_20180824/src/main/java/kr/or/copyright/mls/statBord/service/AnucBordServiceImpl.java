package kr.or.copyright.mls.statBord.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import kr.or.copyright.mls.statBord.dao.AnucBordDao;
import kr.or.copyright.mls.statBord.model.AnucBord;
import kr.or.copyright.mls.statBord.model.AnucBordAttcFile;
import kr.or.copyright.mls.statBord.model.AnucBordFile;
import kr.or.copyright.mls.statBord.model.AnucBordObjc;
import kr.or.copyright.mls.statBord.model.AnucBordObjcFile;
import kr.or.copyright.mls.statBord.model.AnucBordSupl;
import kr.or.copyright.mls.statBord.model.AnucNonAttcFile;
import kr.or.copyright.mls.statBord.model.Code;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;


@Service("AnucBordService")
public class AnucBordServiceImpl implements AnucBordService {
	private Logger log = Logger.getLogger(this.getClass());
	@Resource(name="AnucBordDao")
	private AnucBordDao dao;

	public List<AnucBord> SelTest(){
		return dao.SelTest();
	}
	public int insertAnuc(AnucBord anucBord) {
		try{
			dao.insertAnuc(anucBord);
		if(anucBord.getFileList().size() != 0){
			AnucBordFile anucBordFile = new AnucBordFile();
			for(int i=0; i < anucBord.getFileList().size(); i++) {
			anucBordFile = (AnucBordFile) anucBord.getFileList().get(i);
			anucBordFile.setRgstIdnt(anucBord.getRgstIdnt());
			dao.fileInsertAnuc1(anucBordFile);
			
			AnucBordAttcFile anucBordAttcFile = new AnucBordAttcFile();
			anucBordAttcFile.setBordCd(anucBord.getBordCd());
			anucBordAttcFile.setBordSeqn(anucBord.getBordSeqn());
			log.info("bordSeeqn=" +anucBordAttcFile.getBordSeqn());
			anucBordAttcFile.setRgstIdnt(anucBord.getRgstIdnt());
			dao.fileInsertAnuc2(anucBordAttcFile);
			}
		}
		}catch(Exception e){
				e.printStackTrace();
			}
		return 1;
	}
	public int getBordSeqn(){
		return dao.getBordSeqn();
	}
	public void deleteAnucBord(int bordSeqn){
		 dao.deleteAnucBord(bordSeqn);
	}
	public List<AnucBord> selectAnuc(AnucBord anucBord){
		return 	dao.selectAnuc(anucBord);
	}
	public List<AnucBordFile> fileSelectAnuc(int bordSeqn){
		return dao.fileSelectAnuc(bordSeqn);
	}
	public List<AnucBordFile> fileSelectObjc(int statObjcId){
		return dao.fileSelectObjc(statObjcId);
	}
	public List<AnucBord> detailAnuc(int bordSeqn){
		try{
			dao.updateInqrContAnuc(bordSeqn);
		}catch(Exception e){
			e.printStackTrace();
		}
		return dao.detailAnuc(bordSeqn);
	}
	public List<AnucBord> findAnuc(AnucBord anucBord){
		return dao.findAnuc(anucBord);
	}
	public int countAnuc(AnucBord anucBord){
		return dao.countAnuc(anucBord);
	}
	public int findCount(AnucBord anucBord){
		return dao.findCount(anucBord);
	}
	public int insertAnucObjc(AnucBordObjc anucBordObjc){
			try{
				dao.insertAnucObjc(anucBordObjc);
				int bordSeqn = anucBordObjc.getBordSeqn();
				dao.updateAnucBordObjcYn(bordSeqn);
			if(anucBordObjc.getFileList().size() != 0){
				AnucBordFile anucBordFile = new AnucBordFile();
				for(int i=0; i < anucBordObjc.getFileList().size(); i++) {
					anucBordFile = (AnucBordFile) anucBordObjc.getFileList().get(i);
					anucBordFile.setRgstIdnt(anucBordObjc.getRgstIdnt());
					dao.fileInsertAnuc1(anucBordFile);
					
					AnucBordObjcFile anucBordObjcFile = new AnucBordObjcFile();
					anucBordObjcFile.setRgstIdnt(anucBordFile.getRgstIdnt());
					anucBordObjcFile.setStatObjcId(anucBordObjc.getStatObjcId());
					dao.fileInsertObjc(anucBordObjcFile);
					anucBordObjc.setAttcSeqn(anucBordFile.getAttcSeqn());
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				}
			return 1;
		}
	public List<AnucBordObjc> selectAnucObjc(int bordSeqn,int bordCd){
		return dao.selectAnucObjc(bordSeqn, bordCd);
	}
	public List<AnucBordObjc> selectAnucObjcShis(int statObjcId){
		return dao.selectAnucObjcShis(statObjcId);
	}
	public int countAnucObjc(int bordSeqn){
		return dao.countAnucObjc(bordSeqn);
	}
	public List getObjcFileAttc(int statObjcId){
		return dao.getObjcFileAttc(statObjcId);
	}
	public int deleteAnucBordObjc(int statObjcId){
		int attcSeqn = 0;
		List<AnucBordFile> list = dao.getObjcFileAttc(statObjcId);
		if(list.size() != 0){
		 for(int i =0; i<list.size(); i++){
			attcSeqn = list.get(i).getAttcSeqn();
			dao.deleteObjcAttcFile(attcSeqn);			
			dao.deleteObjcFile(attcSeqn);	
		 }
		 	dao.deleteAnucBordObjcNon(statObjcId);
		 	dao.deleteAnucBordObjc(statObjcId);
		 	dao.deleteAnucBordObjcStat(statObjcId);
		}else{
			dao.deleteAnucBordObjcNon(statObjcId);
			dao.deleteAnucBordObjc(statObjcId);
			dao.deleteAnucBordObjcStat(statObjcId);
		}
		return 1;
	}
	public int deleteObjcFile(int attcSeqn){
			dao.deleteObjcAttcFile(attcSeqn);			
			dao.deleteObjcFile(attcSeqn);	
		return 1;
	}
	public int updateAnucObjc(AnucBordObjc anucBordObjc){
		try{
			dao.updateAnucObjc(anucBordObjc);
			
			if(anucBordObjc.getFileList().size() != 0){
				AnucBordFile anucBordFile = new AnucBordFile();
				for(int i=0; i < anucBordObjc.getFileList().size(); i++) {
					anucBordFile = (AnucBordFile) anucBordObjc.getFileList().get(i);
					anucBordFile.setRgstIdnt(anucBordObjc.getRgstIdnt());
					dao.fileInsertAnuc1(anucBordFile);
					
					AnucBordObjcFile anucBordObjcFile = new AnucBordObjcFile();
					anucBordObjcFile.setRgstIdnt(anucBordFile.getRgstIdnt());
					anucBordObjcFile.setStatObjcId(anucBordObjc.getStatObjcId());
					dao.fileInsertObjc(anucBordObjcFile);
					anucBordObjc.setAttcSeqn(anucBordFile.getAttcSeqn());
					}
				}
		}catch(Exception e){
			e.printStackTrace();
		}
			
		return 1;
	}
	public void updateAnucBord(AnucBord anucBord){
		try{
			dao.updateAnucBord(anucBord);
			if(anucBord.getFileList().size() != 0){
				AnucBordFile anucBordFile = new AnucBordFile();
				for(int i=0; i < anucBord.getFileList().size(); i++) {
				anucBordFile = (AnucBordFile) anucBord.getFileList().get(i);
				anucBordFile.setRgstIdnt(anucBord.getModiIdnt());
				dao.fileInsertAnuc1(anucBordFile);
				AnucBordAttcFile anucBordAttcFile = new AnucBordAttcFile();
				anucBordAttcFile.setBordCd(anucBord.getBordCd());
				anucBordAttcFile.setBordSeqn(anucBord.getBordSeqn());
				anucBordAttcFile.setRgstIdnt(anucBord.getModiIdnt());
				dao.fileInsertAnuc2(anucBordAttcFile);
				}
			}
			}catch(Exception e){
				e.printStackTrace();
		}
	}
	public void deleteAttcFile(int attcSeqn){
		dao.deleteAttcFile(attcSeqn);
		dao.deleteObjcFile(attcSeqn);
	}
	//------------------거소불명-------------------
	public int getWorksId(){
		return dao.getWorksId();
	}
	public List<AnucBord> selectNonAnuc(AnucBord anucBord){
		return dao.selectNonAnuc(anucBord);
	}
	public int insertNonAnuc(AnucBord anucBord){
		try{
			dao.insertNonAnuc(anucBord);
			dao.insertNonAnuc2(anucBord);
			dao.insertNonAnuc3(anucBord);
			log.info("service Size = " +anucBord.getFileList().size());
		if(anucBord.getFileList().size() != 0){
				AnucBordFile anucBordFile = new AnucBordFile();
				for(int i=0; i < anucBord.getFileList().size(); i++) {
				anucBordFile = (AnucBordFile) anucBord.getFileList().get(i);
				anucBordFile.setRgstIdnt(anucBord.getRgstIdnt());
				dao.fileInsertNon(anucBordFile);
				
				AnucNonAttcFile anucNonAttcFile = new AnucNonAttcFile();
				anucNonAttcFile.setWorksId(anucBord.getWorksId());
				anucNonAttcFile.setRgstIdnt(anucBord.getRgstIdnt());
				dao.fileInsertNonAnuc(anucNonAttcFile);
			}
		}
		}catch(Exception e){
				e.printStackTrace();
			}
		return 1;
	}
	
	public int insertCopthodr(AnucBord anucBord){
		dao.insertCopthodr(anucBord);
		return 1;
	}

	public List<Code> getCodeList(Map params){
		return dao.getCodeList(params);
	}
	
	public List<AnucBord> detailNonAnuc(int worksId){
		return dao.detailNonAnuc(worksId);
	}
	public List<AnucBord> detailGetMaker(int worksId){
		return dao.detailGetMaker(worksId);
	}
	public List<AnucBord> detailNonCopthodr(AnucBord anucBord){
		return dao.detailNonCopthodr(anucBord);
	}
	public List<AnucBord> NonResult(int worksId){
		return dao.NonResult(worksId);
	}
	public List<AnucBordFile> fileSelectNonAnuc(int worksId){
		return dao.fileSelectNonAnuc(worksId);
	}
	public void updateNonCoptHodr(AnucBord anucBord){
		dao.updateNonCoptHodr(anucBord);
	}
	public void updateNonAnuc(AnucBord anucBord){
		try{
			dao.updateNonAnuc(anucBord);
			dao.updateStatWorks(anucBord);
		if(anucBord.getFileList().size() != 0){
			AnucBordFile anucBordFile = new AnucBordFile();
			for(int i=0; i < anucBord.getFileList().size(); i++) {
			anucBordFile = (AnucBordFile) anucBord.getFileList().get(i);
			anucBordFile.setRgstIdnt(anucBord.getRgstIdnt());
			dao.fileInsertAnuc1(anucBordFile);
			
			AnucNonAttcFile anucNonAttcFile = new AnucNonAttcFile();
			anucNonAttcFile.setWorksId(anucBord.getWorksId());
			anucNonAttcFile.setRgstIdnt(anucBord.getRgstIdnt());
			dao.fileInsertNonAnuc(anucNonAttcFile);
			}
		}
		}catch(Exception e){
				e.printStackTrace();
			}
	}
	public void deleteNonAnuc(int worksId){
		dao.deleteNonAnuc(worksId);
	}
	public int countNonAnuc(String rgstIdnt){
		return dao.countNonAnuc(rgstIdnt);
	}
	public int deleteNonFile(int attcSeqn){
		dao.deleteNonAttcFile(attcSeqn);
		dao.deleteObjcFile(attcSeqn);
		return 1;
	}
	public int deletecoptHodr(int worksId){
		return dao.deletecoptHodr(worksId);
	}
	
	public List<AnucBord> findNonAnuc(Map param){
		return dao.findNonAnuc(param);
	}
	public int countFindNonAnuc(Map param){
		return dao.countFindNonAnuc(param);
	}
//-------------------법정허락 이용승인신청-----------------------
	public void insertStatObjc(AnucBordObjc anucBordObjc){
		try{
		dao.insertStatObjc(anucBordObjc);
		
		int worksId = anucBordObjc.getWorksId();
		dao.updateStatWorksObjcYn(worksId);
		if(anucBordObjc.getFileList().size() != 0){
			AnucBordFile anucBordFile = new AnucBordFile();
			for(int i=0; i < anucBordObjc.getFileList().size(); i++) {
			anucBordFile = (AnucBordFile) anucBordObjc.getFileList().get(i);
			anucBordFile.setRgstIdnt(anucBordObjc.getRgstIdnt());
			dao.fileInsertAnuc1(anucBordFile);
			
			AnucBordObjcFile anucBordObjcFile = new AnucBordObjcFile();
			anucBordObjcFile.setRgstIdnt(anucBordFile.getRgstIdnt());
			anucBordObjcFile.setStatObjcId(anucBordObjc.getStatObjcId());
			dao.fileInsertObjc(anucBordObjcFile);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public int countStatObjc(int worksId){
		return dao.countStatObjc(worksId);
	}
	public List<AnucBordObjc> selectStatObjc(int worksId){
		return dao.selectStatObjc(worksId);
	}
	public int deleteStatObjc(int statObjcId){
		int attcSeqn = 0;
		List<AnucBordFile> list = dao.getObjcFileAttc(statObjcId);
		if(list.size() != 0){
		 for(int i =0; i<list.size(); i++){
			attcSeqn = list.get(i).getAttcSeqn();
			dao.deleteObjcAttcFile(attcSeqn);			
			dao.deleteObjcFile(attcSeqn);	
		 }
		 	dao.deleteStatObjc(statObjcId);
		 	dao.deleteAnucBordObjcStat(statObjcId);
		}else{
			dao.deleteStatObjc(statObjcId);
			dao.deleteAnucBordObjcStat(statObjcId);
		}
		
		return 1;
	}
	
	public void updateAnucBordObjcYnDelete(int bordSeqn){
		dao.updateAnucBordObjcYnDelete(bordSeqn);
	}
	public void updateStatWorksObjcYnDelete(int worksId){
		dao.updateStatWorksObjcYnDelete(worksId);
	}
	
	public List StatObjcPop(AnucBord anucBord){
		return dao.StatObjcPop(anucBord);
	}
	
	
	// 저작권자 조회공고, 보상금 공탁공고 보완내역 불러오기	
	public List<AnucBordSupl> selectBordSuplItemList(int bordSeqn, int bordCd){
		return dao.selectBordSuplItemList(bordSeqn, bordCd);
	}
	// 상당한노력신청 - 저작권자 조회공고 보완목록 불러오기
	public List<AnucBordSupl> selectWorksSuplItemList(int worksId){
		return dao.selectWorksSuplItemList(worksId);
	}
	
	
	public int statBo07TotalCount(Map dataMap){
		return dao.statBo07TotalCount(dataMap);
	}
	
	//기승인 법정허락 저작물 리스트
	public List statBo07List(Map dataMap){
		return dao.statBo07List(dataMap);
	}
}
