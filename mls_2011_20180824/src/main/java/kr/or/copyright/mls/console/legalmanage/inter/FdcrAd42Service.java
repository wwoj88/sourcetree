package kr.or.copyright.mls.console.legalmanage.inter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface FdcrAd42Service{

	/**
	 * 목록 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd42List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 저작물 상세 및 수정 폼
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd42UpdateForm1( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 저작물 상세
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> statDetailList( Map<String, Object> commandMap ) throws Exception;
	
	/**
	 * 저작물 상세 첨부파일 목록
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Map<String, Object>> statFileList( Map<String, Object> commandMap ) throws SQLException;

	/**
	 * CLMS 저작물 목록 조회 팝업
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd42Popup1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * CLMS 저작물 상세 조회
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd42View1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * CLMS 저작물 상세 조회(음악,어문)
	 * 
	 * @param commandMap
	 * @return
	 * @throws SQLException
	 */
	public void fdcrAd42View2( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 저작물 수정
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd42Update1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;
	
	/**
	 * 저작물 등록
	 * 
	 * @param commandMap
	 * @throws Exception
	 */
	public boolean fdcrAd42Insert1( Map<String, Object> commandMap,
		ArrayList<Map<String, Object>> fileList ) throws Exception;

}
