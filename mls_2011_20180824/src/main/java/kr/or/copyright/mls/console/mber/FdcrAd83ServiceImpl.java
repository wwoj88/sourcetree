package kr.or.copyright.mls.console.mber;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import kr.or.copyright.mls.console.CommandService;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Dao;
import kr.or.copyright.mls.console.mber.inter.FdcrAd83Service;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import com.softforum.xdbe.xCrypto;

@Service("fdcrAd83Service")
public class FdcrAd83ServiceImpl extends CommandService implements FdcrAd83Service {

     // old AdminUserDao
     @Resource(name = "fdcrAd83Dao")
     private FdcrAd83Dao fdcrAd83Dao;

     /**
      * 회원정보 목록 조회
      * 
      * @param commandMap
      * @throws Exception
      */
     public void fdcrAd83List1(Map<String, Object> commandMap) throws Exception {

          // DAO호출
          int totCnt = fdcrAd83Dao.userListCount(commandMap);
          pagination(commandMap, totCnt, 0);
          List list = (List) fdcrAd83Dao.userList(commandMap);
          commandMap.put("ds_list", list); // 로그인 사용자 정보
     }

     /**
      * 회원정보 상세조회
      * 
      * @param commandMap
      * @throws Exception
      */
     private Log logger = LogFactory.getLog(getClass());

     public void fdcrAd83View1(Map<String, Object> commandMap) throws Exception {

          // DAO호출
          List list2 = (List) fdcrAd83Dao.userDetlList(commandMap);

          /* resd복호화 str 20121108 정병호 */
          // TODO 암호화 주석제거
         // xCrypto.RegisterEx("pattern7", 2, new String(kr.or.copyright.mls.support.constant.Constants.getProperty("xcry")), "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");

          /*
           * xCrypto.RegisterEx( "pattern7", 2, "/home/jeus/xecuredb/conf/xdsp_pool.properties", "pool1",
           * "mcst_db", "mcst_owner", "mcst_table", "pattern7" );
           */

          Map list = new HashMap();
          list = (Map) list2.get(0);

          String RESD_CORP_NUMB = "";

          /*// 주민번호가 notnull 인 경우만. old
          if (list.get("RESD_CORP_NUMB") != null && list.get("RESD_CORP_NUMB").toString().replaceAll(" ", "").length() > 0) {
               // TODO 암호화주석
               logger.debug("fdcrAd83View1 call2");
               logger.debug("fdcrAd83View1 list.get( \"RESD_CORP_NUMB\" )" + list.get("RESD_CORP_NUMB"));
               // System.out.println( "" );
               RESD_CORP_NUMB = xCrypto.Decrypt("pattern7", (String) list.get("RESD_CORP_NUMB")).replaceAll(" ", "");// (원본
               // 주민번호
               // 복호화)
               RESD_CORP_NUMB = (String) list.get("RESD_CORP_NUMB");
               logger.debug("fdcrAd83View1 RESD_CORP_NUMB : " + RESD_CORP_NUMB);
               logger.debug("fdcrAd83View1 end");
               String RESD_CORP_NUMB1 = RESD_CORP_NUMB.substring(0, 6);
               String RESD_CORP_NUMB2 = ""; // RESD_CORP_NUMB.substring(6);

               list.put("RESD_CORP_NUMB", RESD_CORP_NUMB1 + "-" + RESD_CORP_NUMB2);
          }
*/
          // 주민번호가 notnull 인 경우만. new
          /*
           * if( list.get( "RESD_CORP_NUMB" ) != null && list.get( "RESD_CORP_NUMB" ).toString().replaceAll(
           * " ", "" ).length() > 0 ){ //TODO 암호화주석 logger.debug( "fdcrAd83View1 call2" ); logger.debug(
           * "fdcrAd83View1 list.get( \"RESD_CORP_NUMB\" )" + list.get( "RESD_CORP_NUMB" ) );
           * //System.out.println( "" ); String RESD_CORP_NUMB_RESULT=(String)list.get( "RESD_CORP_NUMB" );
           * logger.debug( RESD_CORP_NUMB_RESULT.substring( 7 ) ); String RESD_CORP_NUMB2 = xCrypto.Decrypt(
           * "pattern7", RESD_CORP_NUMB_RESULT.substring( 6 ) ).replaceAll( " ", "" );// (원본 //String
           * RESD_CORP_NUMB2 =""; RESD_CORP_NUMB = (String) list.get( "RESD_CORP_NUMB" );
           * logger.debug("fdcrAd83View1 RESD_CORP_NUMB : "+ RESD_CORP_NUMB );
           * logger.debug("fdcrAd83View1 end"); String RESD_CORP_NUMB1 = RESD_CORP_NUMB.substring( 0, 6 );
           * list.put( "RESD_CORP_NUMB", RESD_CORP_NUMB1 + "-" + RESD_CORP_NUMB2 ); }
           */

          commandMap.put("ds_list", list); // 로그인 사용자 정보
     }

     /**
      * 담당자, 회원 비밀번호 수정
      * 
      * @param commandMap
      * @throws Exception
      */
     public boolean fdcrAd83Update1(Map<String, Object> commandMap) throws Exception {

          boolean result = false;
          try {
               /* pwsd암호화 str 20121108 성혜진 */
               String sOutput_H = null;
               String passWord = ((String) commandMap.get("passWord")).toUpperCase();
               sOutput_H = xCrypto.Hash(6, passWord);// 단방향 암호화(sOutput)
               commandMap.put("passWord", sOutput_H);
               /* pwsd암호화 end 20121108 성혜진 */
               fdcrAd83Dao.userMgnbPswdUpdate(commandMap);

               // if(!"".equals( commandMap.get( "clmsAgrYn" ) ) || commandMap.get( "clmsAgrYn" ) != null){
               if (commandMap.get("clmsAgrYn") != null) {
                    if (!commandMap.get("clmsAgrYn").equals("")) {
                         if (commandMap.get("clmsAgrYn").equals("Y")) {

                              // Clms비밀번호 대문자처리 (주석처리(아래두줄))
                              // String passWord2 = passWord.toUpperCase();
                              // map.put("cprPswd", passWord2);

                              /***********
                               * Clms비밀번호 암호화 주석처리****************
                               **********************************************/
                              commandMap.put("cprPswd", sOutput_H);

                              commandMap.put("clmsUserIdnt", commandMap.get("CLMS_USER_IDNT"));
                              fdcrAd83Dao.updateClmsUserInfo(commandMap);
                         }
                    }
               }
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     /**
      * 담당자, 회원 비밀번호 수정
      * 
      * @param commandMap
      * @throws Exception
      */
     public boolean fdcrAd83CommNameUpdate(Map<String, Object> commandMap) throws Exception {

          boolean result = false;

          try {
               Map consoleUser = (Map) commandMap.get("CONSOLE_USER");
               Map historyInsertMap;
               Map map = null;

               List resultList = fdcrAd83Dao.fdcrAd83MlCodeSelectList(commandMap);
               System.out.println("resultList : " + resultList);
               System.out.println("resultList.size() : " + resultList.size());
               resultList = fdcrAd83Dao.fdcrAd83CommNameSelectList(commandMap);
               System.out.println("resultList : " + resultList);
               if (resultList.size() != 0) {
                    System.out.println("resultList.size() != 0");
                    for (int i = 0; i < resultList.size(); i++) {
                         map = (HashMap) resultList.get(i);
                         if (commandMap.get("COMM_NAME_OLD").equals(map.get("USER_NAME"))) {
                              map.put("COMM_NAME", commandMap.get("COMM_NAME"));
                              fdcrAd83Dao.fdcrAd83UserNameUpdate(map);
                              addInsertHistory("ML_USER_INFO_USER_NAME", commandMap, map, consoleUser);
                         } ;
                    }

                    fdcrAd83Dao.fdcrAd83UserInfoCommNameUpdate(commandMap);
                    System.out.println("update ok");
                    addInsertHistory("ML_USER_INFO_COMM_NAME", commandMap, map, consoleUser);
                    System.out.println("add userinfo ok");
                    fdcrAd83Dao.fdcrAd83CommWorksReportCommNameUpdate(commandMap);
                    System.out.println("report update ok");
                    addInsertHistory("ML_COMM_WORKS_REPORT", commandMap, map, consoleUser);
                    System.out.println("add history ok");
                    // ML_COMM_WORKS 수정 안 하기로 승권 부장님과 합의
                    /*
                     * List reList = fdcrAd83Dao.fdcrAd83commNameSelectListByCommWorks( commandMap );
                     * System.out.println( "reList : " + reList ); if(reList.size()!=0) {
                     * fdcrAd83Dao.fdcrAd83MlCommWorksCommNameUpdate( commandMap );
                     * addInsertHistory("ML_COMM_WORKS",commandMap,map,consoleUser); }
                     */
               }

               if (!commandMap.get("clmsAgrYn").equals("") || commandMap.get("clmsAgrYn") != null) {
                    if (commandMap.get("clmsAgrYn") != null) {
                         if (!commandMap.get("clmsAgrYn").equals("")) {
                              if (commandMap.get("clmsAgrYn").equals("Y")) {
                                   commandMap.put("clmsUserIdnt", commandMap.get("CLMS_USER_IDNT"));
                                   addInsertHistory("CLMS_TEST.TA_MEMBER", commandMap, map, consoleUser);
                                   fdcrAd83Dao.updateClmsUserUpdate(commandMap);
                              }
                         }
                    }
               }
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     public boolean fdcrAd83CompanyDelete(Map<String, Object> commandMap) throws Exception {

          boolean result = false;

          String[] tableName = {"ML_COMM_ART", "ML_COMM_MOVIE", "ML_COMM_NEWS", "ML_COMM_SCRIPT", "ML_COMM_BOOK", "ML_COMM_BROADCAST", "ML_COMM_IMAGE", "ML_COMM_SIDE", "ML_COMM_MUSIC"};
          String[] backupTableName = {"ML_COMM_ART_DELETE_HISTORY", "ML_COMM_MOVIE_DELETE_HISTORY", "ML_COMM_NEWS_DELETE_HISTORY", "ML_COMM_SCRIPT_DELETE_HISTORY", "ML_COMM_BOOK_DELETE_HISTORY",
                    "ML_COMM_BROADCAST_DEL_HIS", "ML_COMM_IMAGE_DELETE_HISTORY", "ML_COMM_SIDE_DELETE_HISTORY", "ML_COMM_MUSIC_DELETE_HISTORY"};


          try {
               Map consoleUser = (Map) commandMap.get("CONSOLE_USER");
               Map historyInsertMap;
               Map map = null;

               List resultList = fdcrAd83Dao.fdcrAd83MlCodeSelectList(commandMap);
               System.out.println(resultList);
               if (resultList.size() == 0) {
                    return false;
               } else {

                    resultList = fdcrAd83Dao.fdcrAd83CommNameSelectList(commandMap);
                    // 관리자가 삭제 시 해당업체의 모든 사용자 삭제와 업체의 모든 위탁관리저작물 목록 모두 삭제인것인지..
                    if (resultList.size() != 0) {
                         fdcrAd83Dao.fdcrAd83UserBackup(commandMap);
                         fdcrAd83Dao.fdcrAd83CompanyDelete(commandMap);
                         for (int i = 0; i < resultList.size(); i++) {
                              map = (HashMap) resultList.get(i);
                              addInsertHistory("ML_USER_INFO_DELETE", commandMap, map, consoleUser);
                         }
                    }
                    fdcrAd83Dao.fdcrAd83CommWorksReportCommNameBackup(commandMap);
                    fdcrAd83Dao.fdcrAd83CommWorksReportCommNameDelete(commandMap);
                    addInsertHistory("ML_COMM_WORKS_REPORT_DELETE", commandMap, map, consoleUser);

                    for (int i = 0; i < tableName.length; i++) {
                         commandMap.put("TABLE_NAME", tableName[i]);
                         commandMap.put("BACKUP_TABLE_NAME", backupTableName[i]);
                         List tableReturnList = fdcrAd83Dao.fdcrAd83CommWorksTablesCommNameSelect(commandMap);
                         if (tableReturnList.size() != 0) {
                              fdcrAd83Dao.fdcrAd83CommWorksTablesCommNameBackup(commandMap);
                              addInsertHistory("ML_COMM_WORKS_REPORT_DELETE", commandMap, map, consoleUser);
                              fdcrAd83Dao.fdcrAd83CommWorksTablesCommNameDelete(commandMap);
                         }
                    }

                    List reList = fdcrAd83Dao.fdcrAd83commNameSelectListByCommWorks(commandMap);
                   // System.out.println("reList : " + reList);
                    if (reList.size() != 0) {
                         fdcrAd83Dao.fdcrAd83MlCommWorksCommNameBackup(commandMap);
                         fdcrAd83Dao.fdcrAd83MlCommWorksCommNameDelete(commandMap);
                         addInsertHistory("ML_COMM_WORKS_DELETE", commandMap, map, consoleUser);
                    }
               }

               /*
                * if(!"".equals( commandMap.get( "clmsAgrYn" ) ) || commandMap.get( "clmsAgrYn" ) != null){
                * if(commandMap.get( "clmsAgrYn" ) != null){ if(!commandMap.get( "clmsAgrYn" ).equals("")){ if(
                * commandMap.get( "clmsAgrYn" ).equals( "Y" ) ){ commandMap.put( "clmsUserIdnt", commandMap.get(
                * "CLMS_USER_IDNT" ) ); //addInsertHistory("CLMS_TEST.TA_MEMBER",commandMap,map,consoleUser);
                * //fdcrAd83Dao.updateClmsUserUpdate( commandMap ); } } } }
                */
               result = true;
          } catch (Exception e) {
               e.printStackTrace();
          }
          return result;
     }

     public void addInsertHistory(String TABLE_NAME, Map commandMap, Map map, Map consoleUser) {

          HashMap<String, String> tempMap = new HashMap<String, String>();
          tempMap.put("TABLE_NAME", TABLE_NAME);
          tempMap.put("OLD_COMM_NAME", (String) commandMap.get("COMM_NAME_OLD"));
          tempMap.put("NEW_COMM_NAME", (String) commandMap.get("COMM_NAME"));
          tempMap.put("USER_IDNT", (String) commandMap.get("USER_IDNT"));
          tempMap.put("TRST_ORGN_CODE", (String) map.get("TRST_ORGN_CODE"));
          tempMap.put("RGST_IDNT", (String) consoleUser.get("USER_ID"));
          fdcrAd83Dao.insertCommNameUpdateHistory(tempMap);
     }

     public void fncUpdateFailCnt(Map<String, Object> commandMap) throws Exception {

          fdcrAd83Dao.fncUpdateFailCnt(commandMap);
     }
}


