package kr.or.copyright.mls.rghtPrps.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.or.copyright.mls.common.common.model.ListResult;
import kr.or.copyright.mls.common.common.utils.Constants;
import kr.or.copyright.mls.common.common.utils.XssUtil;
import kr.or.copyright.mls.common.model.CodeList;
import kr.or.copyright.mls.common.service.CodeListService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.rghtPrps.model.PrpsAttc;
import kr.or.copyright.mls.rghtPrps.model.RghtPrps;
import kr.or.copyright.mls.rghtPrps.service.RghtPrpsService;
import kr.or.copyright.mls.support.util.FileUploadUtil;
import kr.or.copyright.mls.user.service.UserService;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.validation.Validator;

public class RghtPrpsController extends MultiActionController {
     // private Log logger = LogFactory.getLog(getClass());

     private CodeListService codeListService;
     private RghtPrpsService rghtPrpsService;
     private UserService userService;

     /** List of Validators to apply to commands */
     private Validator[] validators;

     // private String realUploadPath = "D:/server/mls/mls_2010/upload/"; // 로컬용
     // private String realUploadPath = "C:/home/tmax/mls/web/upload/"; // 로컬용
     // private String realUploadPath = "/home/tmax/mls/web/upload/"; // 실서버용
     // private String realUploadPath = "/home/tmax_dev/mls/web/upload/"; // 개발서버용
     private String realUploadPath = FileUtil.uploadPath();

     public void setCodeListService(CodeListService codeListService) {

          this.codeListService = codeListService;
     }

     public void setRghtPrpsService(RghtPrpsService rghtPrpsService) {

          this.rghtPrpsService = rghtPrpsService;
     }

     public void setUserService(UserService userService) {

          this.userService = userService;
     }

     // 음악저작물 미리듣기 url을 구한다.
     public String getMuscRecFile(String CRH_ID_OF_CA) {

          String recFile = "";

          RghtPrps rghtPrps = new RghtPrps();
          rghtPrps.setCRH_ID_OF_CA(CRH_ID_OF_CA);

          recFile = rghtPrpsService.getMuscRecFile(rghtPrps);
          return recFile;
     }

     public ModelAndView list(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          String DIVS = ServletRequestUtils.getStringParameter(request, "DIVS", "M");
          String gubun = ServletRequestUtils.getStringParameter(request, "gubun", ""); // 통합검색에서 넘어온경우 : totalSearch 기본 검색어 set 하지 않는다.

          Date date = new Date();
          SimpleDateFormat yearForm = new SimpleDateFormat("yyyy");
          SimpleDateFormat dayForm = new SimpleDateFormat("yyyyMMdd");
          String thisYear = yearForm.format(date);
          String thisMonth = yearForm.format(date) + "0101";
          String thisDay = dayForm.format(date);

          /* 20120220 검색조건 화면 수정 //정병호 */
          // 공통
          String srchTitle_c = ServletRequestUtils.getStringParameter(request, "srchTitle", "");
          String srchStartDate_c = ServletRequestUtils.getStringParameter(request, "srchStartDate", "");
          String srchEndDate_c = ServletRequestUtils.getStringParameter(request, "srchEndDate", "");

          // 뉴스
          String srchProviderNm_c = ServletRequestUtils.getStringParameter(request, "srchProviderNm", "");


          ModelAndView mv = null;
          mv = new ModelAndView("rghtPrps/rghtMain");
          mv.addObject("DIVS", DIVS); // M:음악 , B:도서, C:방송대본, I:이미지, V:영화, R:방송
          String srchTitle = ServletRequestUtils.getStringParameter(request, "srchTitle", "").replaceAll("\"", "&quot;");
          mv.addObject("srchTitle", srchTitle); // 곡명, 작품명, 영화명
          mv.addObject("srchProducer", ServletRequestUtils.getStringParameter(request, "srchProducer")); // 제작사
          mv.addObject("srchAlbumTitle", ServletRequestUtils.getStringParameter(request, "srchAlbumTitle")); // 앨범명
          mv.addObject("srchSinger", ServletRequestUtils.getStringParameter(request, "srchSinger")); // 가수명

          if (DIVS.equals("M") && !gubun.equals("totalSearch")) { // 음악
               // mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request,
               // "srchStartDate",thisMonth)); // 발매일
               // mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request,
               // "srchEndDate",thisDay)); // 발매일
               mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate")); // 발매일
               mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate")); // 발매일
          } else if (DIVS.equals("B") && !gubun.equals("totalSearch")) { // 도서
               // mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request,
               // "srchStartDate",thisYear));
               // mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request,
               // "srchEndDate",thisYear));
               mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));
               mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));
          } else if (DIVS.equals("I")) { // 이미지
               mv.addObject("srchWorkName", ServletRequestUtils.getStringParameter(request, "srchWorkName")); // 이미지명
               mv.addObject("srchWterDivs", ServletRequestUtils.getStringParameter(request, "srchWterDivs")); // 작가명
          } else if (DIVS.equals("V")) { // 영화
               mv.addObject("srchViewGrade", ServletRequestUtils.getStringParameter(request, "srchViewGrade")); // 관람등급
               mv.addObject("srchDirector", ServletRequestUtils.getStringParameter(request, "srchDirector")); // 감독
               mv.addObject("srchActor", ServletRequestUtils.getStringParameter(request, "srchActor")); // 출연진
               mv.addObject("srchDistributor", ServletRequestUtils.getStringParameter(request, "srchDistributor")); // 제작사/배급사/투자사
               mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));
               mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));
          } else if (DIVS.equals("C")) { // 방송대본
               // mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request,
               // "srchStartDate",thisYear));
               // mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request,
               // "srchEndDate",thisYear));
               mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));
               mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));
          } else if (DIVS.equals("R")) { // 방송
               // mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request,
               // "srchStartDate",thisYear));
               // mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request,
               // "srchEndDate",thisYear));
               mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));
               mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));
          } else if (DIVS.equals("N")) { // 뉴스
               mv.addObject("srchStartDate", ServletRequestUtils.getStringParameter(request, "srchStartDate"));
               mv.addObject("srchEndDate", ServletRequestUtils.getStringParameter(request, "srchEndDate"));
               mv.addObject("srchProviderNm", ServletRequestUtils.getStringParameter(request, "srchProviderNm"));

               String srchStartDate = ServletRequestUtils.getStringParameter(request, "srchStartDate", "");
               String srchEndDate = ServletRequestUtils.getStringParameter(request, "srchEndDate");

               /* 20120220 검색조건 화면 수정 //정병호 */
               mv.addObject("srchStartDate", srchStartDate);
               mv.addObject("srchEndDate", srchEndDate);
          }

          String srchLicensor = ServletRequestUtils.getStringParameter(request, "srchLicensor", "").replaceAll("\"", "&quot;");
          mv.addObject("srchLicensor", srchLicensor); // 저작자명

          // 도서
          mv.addObject("srchPublisher", ServletRequestUtils.getStringParameter(request, "srchPublisher")); // 출판사
          mv.addObject("srchBookTitle", ServletRequestUtils.getStringParameter(request, "srchBookTitle")); // 도서명
          mv.addObject("srchLicensorNm", ServletRequestUtils.getStringParameter(request, "srchLicensorNm")); // 저자명

          // 방송대본
          mv.addObject("srchWriter", ServletRequestUtils.getStringParameter(request, "srchWriter")); // 작가명
          mv.addObject("srchBroadStatName", ServletRequestUtils.getStringParameter(request, "srchBroadStatName")); // 방송사
          mv.addObject("srchDirect", ServletRequestUtils.getStringParameter(request, "srchDirect")); // 연출가
          mv.addObject("srchPlayers", ServletRequestUtils.getStringParameter(request, "srchPlayers")); // 주요출연진

          // 방송
          mv.addObject("srchProgName", ServletRequestUtils.getStringParameter(request, "srchProgName")); // 프로그램명
          mv.addObject("srchMaker", ServletRequestUtils.getStringParameter(request, "srchMaker")); // 제작자

          mv.addObject("gubun", gubun); // 통합검색인 경우.

          return mv;
     }


     // 저작물 목록>>>>>>>>>신청화면에 포함된 서브리스트 목록
     public ModelAndView subListForm(HttpServletRequest request, HttpServletResponse respone) throws Exception {
          // System.out.println("################");
          // System.out.println("subListForm");
          // System.out.println("################");


          String DIVS = ServletRequestUtils.getStringParameter(request, "DIVS", "M"); // 구분값. 값이 없을 시 음악으로 세팅
          String dtlYn = ServletRequestUtils.getStringParameter(request, "dtlYn", ""); // 구분값. 목록화면인지 권리찾기 신청화면인지 구분

          RghtPrps rghtPrps = new RghtPrps();


          rghtPrps.setSrchTitle(ServletRequestUtils.getStringParameter(request, "srchTitle")); // 곡명
          rghtPrps.setSrchProducer(ServletRequestUtils.getStringParameter(request, "srchProducer")); // 제작사
          rghtPrps.setSrchAlbumTitle(ServletRequestUtils.getStringParameter(request, "srchAlbumTitle")); // 앨범명
          rghtPrps.setSrchSinger(ServletRequestUtils.getStringParameter(request, "srchSinger")); // 가수명
          rghtPrps.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate")); // 발매일
          // rghtPrps.setSrchStartDate("20110501");
          rghtPrps.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate")); // 발매일
          rghtPrps.setSrchLicensor(ServletRequestUtils.getStringParameter(request, "srchLicensor")); // 저작자명

          // 도서
          rghtPrps.setSrchPublisher(ServletRequestUtils.getStringParameter(request, "srchPublisher")); // 출판사
          rghtPrps.setSrchBookTitle(ServletRequestUtils.getStringParameter(request, "srchBookTitle")); // 도서명
          rghtPrps.setSrchLicensorNm(ServletRequestUtils.getStringParameter(request, "srchLicensorNm")); // 저자명

          // 방송대본
          rghtPrps.setSrchWriter(ServletRequestUtils.getStringParameter(request, "srchWriter")); // 작가명
          rghtPrps.setSrchBroadStatName(ServletRequestUtils.getStringParameter(request, "srchBroadStatName")); // 방송사

          // 이미지
          rghtPrps.setSrchWorkName(ServletRequestUtils.getStringParameter(request, "srchWorkName")); // 이미지명
          rghtPrps.setSrchLishComp(ServletRequestUtils.getStringParameter(request, "srchLishComp")); // 출판사
          rghtPrps.setSrchCoptHodr(ServletRequestUtils.getStringParameter(request, "srchCoptHodr")); // 출판년도

          // 영화
          rghtPrps.setSrchViewGrade(ServletRequestUtils.getStringParameter(request, "srchViewGrade")); // 도서부제
          rghtPrps.setSrchDirector(ServletRequestUtils.getStringParameter(request, "srchDirector")); // 감독명
          rghtPrps.setSrchActor(ServletRequestUtils.getStringParameter(request, "srchActor")); // 출연진
          rghtPrps.setSrchDistributor(ServletRequestUtils.getStringParameter(request, "srchDistributor")); // 배급사

          // 방송
          rghtPrps.setSrchProgName(ServletRequestUtils.getStringParameter(request, "srchProgName")); // 프로그램명
          rghtPrps.setSrchMaker(ServletRequestUtils.getStringParameter(request, "srchMaker")); // 제작자

          // 뉴스
          rghtPrps.setSrchProviderNm(ServletRequestUtils.getStringParameter(request, "srchProviderNm")); // 언론사

          rghtPrps.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));

          int from = Constants.DEFAULT_ROW_PER_PAGE * (rghtPrps.getNowPage() - 1) + 1;
          int to = Constants.DEFAULT_ROW_PER_PAGE * rghtPrps.getNowPage();

          rghtPrps.setStartRow(from);
          rghtPrps.setEndRow(to);

          int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);

          ModelAndView mv = null;

          // rghtPrpsService.muscRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps);
          if (DIVS.equals("M")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlMuscRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("muscList", listResult);
               } else {
                    mv = new ModelAndView("rghtPrps/muscRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("muscList", listResult);
               }
          } else if (DIVS.equals("B")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlBookRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("bookList", listResult);
               } else {
                    mv = new ModelAndView("rghtPrps/bookRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("bookList", listResult);
               }
          } else if (DIVS.equals("C")) { // 방송대본
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlScriptRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("scriptList", listResult);
               } else {
                    mv = new ModelAndView("rghtPrps/scriptRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("scriptList", listResult);
               }
          } else if (DIVS.equals("I")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlImageRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("imageList", listResult);
               } else {
                    mv = new ModelAndView("rghtPrps/imageRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("imageList", listResult);
               }
          } else if (DIVS.equals("V")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlMvieRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("mvieList", listResult);
               } else {
                    mv = new ModelAndView("rghtPrps/mvieRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("mvieList", listResult);
               }
          } else if (DIVS.equals("R")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlBroadcastRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("broadcastList", listResult);
               } else {
                    mv = new ModelAndView("rghtPrps/broadcastRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("broadcastList", listResult);
               }
          } else if (DIVS.equals("N")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlNewsRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("newsList", listResult);
               } else {
                    mv = new ModelAndView("rghtPrps/newsRghtSrch");
                    ListResult listResult = new ListResult(0, pageNo, Constants.DEFAULT_ROW_PER_PAGE, null);
                    mv.addObject("newsList", listResult);
               }
          }

          mv.addObject("srchParam", rghtPrps);
          mv.addObject("parentGubun", ServletRequestUtils.getStringParameter(request, "parentGubun", "N"));

          return mv;
     }


     // 저작물 목록>>>>rghtMain에 포함된 서브리스트 목록
     public ModelAndView subList(HttpServletRequest request, HttpServletResponse respone) throws Exception {
          // System.out.println("################");
          // System.out.println("subList");
          // System.out.println("################");


          Date date = new Date();
          SimpleDateFormat yearForm = new SimpleDateFormat("yyyy");
          SimpleDateFormat dayForm = new SimpleDateFormat("yyyyMMdd");
          String thisYear = yearForm.format(date);
          String thisMonth = yearForm.format(date) + "0101";
          String thisDay = dayForm.format(date);

          String DIVS = ServletRequestUtils.getStringParameter(request, "DIVS", "M"); // 구분값. 값이 없을 시 음악으로 세팅
          String dtlYn = ServletRequestUtils.getStringParameter(request, "dtlYn", ""); // 구분값. 목록화면인지 권리찾기 신청화면인지 구분

          RghtPrps rghtPrps = new RghtPrps();



          /* 20120220 검색조건 화면 수정 //정병호 */

          // 공통
          String srchLicensor = ServletRequestUtils.getStringParameter(request, "srchLicensor");
          String srchTitle = ServletRequestUtils.getStringParameter(request, "srchTitle");
          String srchStartDate = ServletRequestUtils.getStringParameter(request, "srchStartDate");
          String srchEndDate = ServletRequestUtils.getStringParameter(request, "srchEndDate");

          // 음악
          String srchSinger = ServletRequestUtils.getStringParameter(request, "srchSinger");
          String srchAlbumTitle = ServletRequestUtils.getStringParameter(request, "srchAlbumTitle");
          String srchProducer = ServletRequestUtils.getStringParameter(request, "srchProducer");
          // srchTitle,srchLicensor,srchStartDate srchEndDate 공통

          // 도서
          String srchLicensorNm = ServletRequestUtils.getStringParameter(request, "srchLicensorNm");
          String srchBookTitle = ServletRequestUtils.getStringParameter(request, "srchBookTitle");
          String srchPublisher = ServletRequestUtils.getStringParameter(request, "srchPublisher");
          // srchTitle,srchLicensor,srchStartDate srchEndDate 공통

          // 뉴스
          String srchProviderNm = ServletRequestUtils.getStringParameter(request, "srchProviderNm");
          // srchTitle,srchStartDate srchEndDate 공통

          // 방송대본
          String srchWriter = ServletRequestUtils.getStringParameter(request, "srchWriter");
          String srchDirect = ServletRequestUtils.getStringParameter(request, "srchDirect");
          String srchBroadStatName = ServletRequestUtils.getStringParameter(request, "srchBroadStatName");
          String srchPlayers = ServletRequestUtils.getStringParameter(request, "srchPlayers");
          // srchTitle,srchStartDate srchEndDate 공통

          // 이미지
          String srchWterDivs = ServletRequestUtils.getStringParameter(request, "srchWterDivs");
          String srchWorkName = ServletRequestUtils.getStringParameter(request, "srchWorkName");

          // 영화
          String srchDirector = ServletRequestUtils.getStringParameter(request, "srchDirector");
          String srchDistributor = ServletRequestUtils.getStringParameter(request, "srchDistributor");
          String srchActor = ServletRequestUtils.getStringParameter(request, "srchActor");
          // srchTitle,srchStartDate srchEndDate 공통

          // 방송
          String srchMaker = ServletRequestUtils.getStringParameter(request, "srchMaker");
          String srchProgName = ServletRequestUtils.getStringParameter(request, "srchProgName");
          // srchTitle,srchStartDate srchEndDate 공통

          /* END //정병호 */


          rghtPrps.setSrchTitle(ServletRequestUtils.getStringParameter(request, "srchTitle", "")); // 곡명, 작품명, 영화명
          rghtPrps.setSrchTitleLen(rghtPrps.getSrchTitle().length() + "");
          rghtPrps.setSrchProducer(ServletRequestUtils.getStringParameter(request, "srchProducer")); // 제작사
          rghtPrps.setSrchAlbumTitle(ServletRequestUtils.getStringParameter(request, "srchAlbumTitle")); // 앨범명
          rghtPrps.setSrchSinger(ServletRequestUtils.getStringParameter(request, "srchSinger")); // 가수명
          rghtPrps.setSrchLicensor(ServletRequestUtils.getStringParameter(request, "srchLicensor", "")); // 저작자명
          rghtPrps.setSrchNonPerf(ServletRequestUtils.getStringParameter(request, "srchNonPerf", "")); // 미확인실연자)

          // 도서
          rghtPrps.setSrchPublisher(ServletRequestUtils.getStringParameter(request, "srchPublisher")); // 출판사
          rghtPrps.setSrchBookTitle(ServletRequestUtils.getStringParameter(request, "srchBookTitle")); // 도서명

          rghtPrps.setSrchLicensorNm(ServletRequestUtils.getStringParameter(request, "srchLicensorNm")); // 저자명
          rghtPrps.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate")); // 발매일
          rghtPrps.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate")); // 발매일

          // 뉴스
          rghtPrps.setSrchProviderNm(ServletRequestUtils.getStringParameter(request, "srchProviderNm")); // 언론사

          if (DIVS.equals("B")) {
               if (rghtPrps.getSrchPublisher().trim().equals("") && rghtPrps.getSrchBookTitle().trim().equals("") && rghtPrps.getSrchLicensorNm().trim().equals("")
                         && rghtPrps.getSrchLicensor().trim().equals("") && rghtPrps.getSrchTitle().trim().equals("") && rghtPrps.getSrchStartDate().trim().equals("")
                         && rghtPrps.getSrchEndDate().trim().equals("")) {

                    rghtPrps.setSrchStartDate("2000"); // 발매일
                    rghtPrps.setSrchEndDate(thisYear); // 발매일
               }

               if (!rghtPrps.getSrchStartDate().trim().equals("")) {
                    rghtPrps.setSrchStartDate(rghtPrps.getSrchStartDate().substring(0, 4));
               }
               if (!rghtPrps.getSrchEndDate().trim().equals("")) {
                    rghtPrps.setSrchEndDate(rghtPrps.getSrchEndDate().substring(0, 4));
               }

          } else if (DIVS.equals("V")) {
               if (!rghtPrps.getSrchStartDate().trim().equals("")) {
                    rghtPrps.setSrchStartDate(rghtPrps.getSrchStartDate().substring(0, 4));
               }
               if (!rghtPrps.getSrchEndDate().trim().equals("")) {
                    rghtPrps.setSrchEndDate(rghtPrps.getSrchEndDate().substring(0, 4));
               }
          } else {
               rghtPrps.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate")); // 발매일
               rghtPrps.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate")); // 발매일
          }



          // 방송대본
          rghtPrps.setSrchWriter(ServletRequestUtils.getStringParameter(request, "srchWriter")); // 작가명
          rghtPrps.setSrchBroadStatName(ServletRequestUtils.getStringParameter(request, "srchBroadStatName")); // 방송사
          rghtPrps.setSrchDirect(ServletRequestUtils.getStringParameter(request, "srchDirect")); // 연출가
          rghtPrps.setSrchPlayers(ServletRequestUtils.getStringParameter(request, "srchPlayers")); // 주요출연진

          // 이미지
          // rghtPrps.setSrchWorkName(ServletRequestUtils.getStringParameter(request, "srchWorkName")); //
          // 이미지명

          if (DIVS.equals("I")) {
               rghtPrps.setSrchWorkName(ServletRequestUtils.getStringParameter(request, "srchWorkName", "").replace(" ", ""));
          } else {
               rghtPrps.setSrchWorkName(ServletRequestUtils.getStringParameter(request, "srchWorkName")); // 이미지명
          }
          rghtPrps.setSrchLishComp(ServletRequestUtils.getStringParameter(request, "srchLishComp")); // 출판사
          rghtPrps.setSrchCoptHodr(ServletRequestUtils.getStringParameter(request, "srchCoptHodr")); // 출판년도
          rghtPrps.setSrchWterDivs(ServletRequestUtils.getStringParameter(request, "srchWterDivs")); // 작가명

          // 영화
          rghtPrps.setSrchViewGrade(ServletRequestUtils.getStringParameter(request, "srchViewGrade")); // 도서부제
          rghtPrps.setSrchDirector(ServletRequestUtils.getStringParameter(request, "srchDirector")); // 감독명
          rghtPrps.setSrchActor(ServletRequestUtils.getStringParameter(request, "srchActor")); // 출연진
          rghtPrps.setSrchDistributor(ServletRequestUtils.getStringParameter(request, "srchDistributor")); // 배급사

          // 방송
          rghtPrps.setSrchProgName(ServletRequestUtils.getStringParameter(request, "srchProgName")); // 프로그램명
          rghtPrps.setSrchMaker(ServletRequestUtils.getStringParameter(request, "srchMaker")); // 제작자

          rghtPrps.setSTotalRow(ServletRequestUtils.getStringParameter(request, "sTotalRow", ""));

          // System.out.println("=============================================================");
          // System.out.println("sTotalRow >> "+rghtPrps.getSTotalRow());
          // System.out.println("=============================================================");

          rghtPrps.setNowPage(ServletRequestUtils.getIntParameter(request, "page_no", 1));

          int from = Constants.DEFAULT_ROW_PER_PAGE * (rghtPrps.getNowPage() - 1) + 1;
          int to = Constants.DEFAULT_ROW_PER_PAGE * rghtPrps.getNowPage();

          rghtPrps.setStartRow(from);
          rghtPrps.setEndRow(to);

          int pageNo = ServletRequestUtils.getIntParameter(request, "page_no", 1);

          ModelAndView mv = null;

          // rghtPrpsService.muscRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps);
          if (DIVS.equals("M")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlMuscRghtSrch", "muscList", rghtPrpsService.muscRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
               } else {
                    /* 검색조건이 없는 화면 //20120220 정병호 */
                    if (srchLicensor.equals("") && srchTitle.equals("") && srchSinger.equals("") && srchAlbumTitle.equals("") && srchProducer.equals("") && srchStartDate.equals("")
                              && srchEndDate.equals("")) {
                         mv = new ModelAndView("rghtPrps/muscRghtSrch");
                         mv.addObject("emptyYn", "Y");
                    } else {
                         mv = new ModelAndView("rghtPrps/muscRghtSrch", "muscList", rghtPrpsService.muscRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
                    }
               }
          } else if (DIVS.equals("B")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlBookRghtSrch", "bookList", rghtPrpsService.bookRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
               } else {
                    /* 검색조건이 없는 화면 //20120220 정병호 */
                    if (srchLicensorNm.equals("") && srchTitle.equals("") && srchLicensor.equals("") && srchBookTitle.equals("") && srchPublisher.equals("") && srchStartDate.equals("")
                              && srchEndDate.equals("")) {
                         mv = new ModelAndView("rghtPrps/bookRghtSrch");
                         mv.addObject("emptyYn", "Y");
                    } else {
                         mv = new ModelAndView("rghtPrps/bookRghtSrch", "bookList", rghtPrpsService.bookRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
                    }
               }
          } else if (DIVS.equals("C")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlScriptRghtSrch", "scriptList", rghtPrpsService.scriptRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
               } else {
                    /* 검색조건이 없는 화면 //20120220 정병호 */
                    if (srchWriter.equals("") && srchTitle.equals("") && srchDirect.equals("") && srchBroadStatName.equals("") && srchPlayers.equals("") && srchStartDate.equals("")
                              && srchEndDate.equals("")) {
                         mv = new ModelAndView("rghtPrps/scriptRghtSrch");
                         mv.addObject("emptyYn", "Y");
                    } else {
                         mv = new ModelAndView("rghtPrps/scriptRghtSrch", "scriptList", rghtPrpsService.scriptRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
                    }
               }
          } else if (DIVS.equals("I")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlImageRghtSrch", "imageList", rghtPrpsService.imageRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
               } else {
                    /* 검색조건이 없는 화면 //20120220 정병호 */
                    if (srchWterDivs.equals("") && srchWorkName.equals("")) {
                         mv = new ModelAndView("rghtPrps/imageRghtSrch");
                         mv.addObject("emptyYn", "Y");
                    } else {
                         mv = new ModelAndView("rghtPrps/imageRghtSrch", "imageList", rghtPrpsService.imageRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
                    }
               }
          } else if (DIVS.equals("V")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlMvieRghtSrch", "mvieList", rghtPrpsService.mvieRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
               } else {
                    /* 검색조건이 없는 화면 //20120220 정병호 */
                    if (srchDirector.equals("") && srchTitle.equals("") && srchDistributor.equals("") && srchActor.equals("") && srchStartDate.equals("") && srchEndDate.equals("")) {
                         mv = new ModelAndView("rghtPrps/mvieRghtSrch");
                         mv.addObject("emptyYn", "Y");
                    } else {
                         mv = new ModelAndView("rghtPrps/mvieRghtSrch", "mvieList", rghtPrpsService.mvieRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
                    }
               }
          } else if (DIVS.equals("R")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlBroadcastRghtSrch", "broadcastList", rghtPrpsService.broadcastRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
               } else {
                    /* 검색조건이 없는 화면 //20120220 정병호 */
                    if (srchMaker.equals("") && srchProgName.equals("") && srchTitle.equals("") && srchStartDate.equals("") && srchEndDate.equals("")) {
                         mv = new ModelAndView("rghtPrps/broadcastRghtSrch");
                         mv.addObject("emptyYn", "Y");
                    } else {
                         mv = new ModelAndView("rghtPrps/broadcastRghtSrch", "broadcastList", rghtPrpsService.broadcastRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
                    }
               }
          } else if (DIVS.equals("N")) {
               if (dtlYn.equals("Y")) {
                    mv = new ModelAndView("rghtPrps/dtlNewsRghtSrch", "newsList", rghtPrpsService.newsRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
               } else {
                    /* 검색조건이 없는 화면 //20120220 정병호 */
                    if (srchProviderNm.equals("") && srchTitle.equals("") && srchStartDate.equals("") && srchEndDate.equals("")) {
                         mv = new ModelAndView("rghtPrps/newsRghtSrch");
                         mv.addObject("emptyYn", "Y");
                    } else {
                         mv = new ModelAndView("rghtPrps/newsRghtSrch", "newsList", rghtPrpsService.newsRghtList(pageNo, Constants.DEFAULT_ROW_PER_PAGE, rghtPrps));
                    }
               }
          }

          // 쌍따옴표 처리
          rghtPrps.setSrchTitle(rghtPrps.getSrchTitle().replaceAll("\"", "&quot;"));
          rghtPrps.setSrchLicensor(rghtPrps.getSrchLicensor().replaceAll("\"", "&quot;"));

          mv.addObject("srchParam", rghtPrps);
          mv.addObject("parentGubun", ServletRequestUtils.getStringParameter(request, "parentGubun", "N"));

          return mv;

     }

     // 권리찾기 신청목적 선택 팝업
     public ModelAndView rghtPrpsSelc(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          RghtPrps rghtPrps = new RghtPrps();

          bind(request, rghtPrps);

          String gubun = ServletRequestUtils.getStringParameter(request, "gubun");

          ModelAndView mv = null;

          mv = new ModelAndView("rghtPrps/rghtPrpsSelc_pop", "rghtPrps", rghtPrps);
          mv.addObject("gubun", gubun);

          return mv;
     }

     // 저작물상세
     public ModelAndView detail(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          RghtPrps rghtPrps = new RghtPrps();

          bind(request, rghtPrps);

          String divs = ServletRequestUtils.getStringParameter(request, "DIVS");

          ModelAndView mv = null;

          if (divs.equals("M")) {
               mv = new ModelAndView("rghtPrps/muscRghtWorkDetl", "music", rghtPrpsService.muscRghtDetail(rghtPrps));
          } else if (divs.equals("B")) {
               mv = new ModelAndView("rghtPrps/bookRghtWorkDetl", "book", rghtPrpsService.bookRghtDetail(rghtPrps));
          } else if (divs.equals("C")) {
               mv = new ModelAndView("rghtPrps/scriptRghtWorkDetl", "script", rghtPrpsService.scriptRghtDetail(rghtPrps));
          } else if (divs.equals("I")) {
               String imgFileName = ServletRequestUtils.getStringParameter(request, "WORK_FILE_NAME");
               String workName = ServletRequestUtils.getStringParameter(request, "WORK_NAME");
               String filePath = kr.or.copyright.mls.support.constant.Constants.DOMAIN_HOME + "/upload/thumb/image/" + imgFileName;
               mv = new ModelAndView("rghtPrps/imageRghtWorkDetl");
               mv.addObject("filePath", filePath);
               mv.addObject("workName", workName);
          } else if (divs.equals("V")) {
               mv = new ModelAndView("rghtPrps/mvieRghtWorkDetl", "movie", rghtPrpsService.mvieRghtDetail(rghtPrps));
          } else if (divs.equals("R")) {
               mv = new ModelAndView("rghtPrps/broadcastRghtWorkDetl", "broadcast", rghtPrpsService.broadcastRghtDetail(rghtPrps));
          } else if (divs.equals("N")) {
               mv = new ModelAndView("rghtPrps/newsRghtWorkDetl", "news", rghtPrpsService.newsRghtDetail(rghtPrps));
          }

          return mv;
     }

     // 권리찾기신청화면 이동
     public ModelAndView rghtPrps(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          return null;
     }

     /* 권리찾기신청화면 이동 주석처리(이동안함) */
     /*
      * public ModelAndView rghtPrps(HttpServletRequest request, HttpServletResponse respone) throws
      * Exception {
      * 
      * // System.out.println("################"); // System.out.println("rghtPrps"); //
      * System.out.println("################"); String divs =
      * ServletRequestUtils.getStringParameter(request, "DIVS"); String ifrmInput =
      * ServletRequestUtils.getStringParameter(request, "ifrmInput");
      * 
      * RghtPrps srchRghtPrps = new RghtPrps();
      * 
      * srchRghtPrps.setSrchTitle(ServletRequestUtils.getStringParameter(request, "srchTitle")); // 곡명,
      * 작품명, 영화명 srchRghtPrps.setSrchProducer(ServletRequestUtils.getStringParameter(request,
      * "srchProducer")); // 제작사
      * srchRghtPrps.setSrchAlbumTitle(ServletRequestUtils.getStringParameter(request,
      * "srchAlbumTitle")); // 앨범명
      * srchRghtPrps.setSrchSinger(ServletRequestUtils.getStringParameter(request, "srchSinger")); // 가수명
      * srchRghtPrps.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate"));
      * // 발매일 srchRghtPrps.setSrchEndDate(ServletRequestUtils.getStringParameter(request,
      * "srchEndDate")); // 발매일
      * srchRghtPrps.setSrchLicensor(ServletRequestUtils.getStringParameter(request, "srchLicensor",""));
      * // 저작자명 srchRghtPrps.setSrchNoneName(ServletRequestUtils.getStringParameter(request,
      * "srchNoneName","")); // 저작(권)자명
      * 
      * // 도서 srchRghtPrps.setSrchPublisher(ServletRequestUtils.getStringParameter(request,
      * "srchPublisher")); // 출판사
      * srchRghtPrps.setSrchBookTitle(ServletRequestUtils.getStringParameter(request, "srchBookTitle"));
      * // 도서명 srchRghtPrps.setSrchLicensorNm(ServletRequestUtils.getStringParameter(request,
      * "srchLicensorNm")); // 저자명
      * 
      * // 방송대본 srchRghtPrps.setSrchWriter(ServletRequestUtils.getStringParameter(request,
      * "srchWriter")); // 작가명
      * srchRghtPrps.setSrchBroadStatName(ServletRequestUtils.getStringParameter(request,
      * "srchBroadStatName")); // 방송사
      * srchRghtPrps.setSrchDirect(ServletRequestUtils.getStringParameter(request, "srchDirect")); // 연출가
      * srchRghtPrps.setSrchPlayers(ServletRequestUtils.getStringParameter(request, "srchPlayers")); //
      * 주요출연진
      * 
      * // 이미지 srchRghtPrps.setSrchWorkName(ServletRequestUtils.getStringParameter(request,
      * "srchWorkName")); // 이미지명
      * srchRghtPrps.setSrchLishComp(ServletRequestUtils.getStringParameter(request, "srchLishComp")); //
      * 출판사명 srchRghtPrps.setSrchCoptHodr(ServletRequestUtils.getStringParameter(request,
      * "srchCoptHodr")); // 저자명
      * srchRghtPrps.setSrchWterDivs(ServletRequestUtils.getStringParameter(request, "srchWterDivs")); //
      * 저자명
      * 
      * // 영화 srchRghtPrps.setSrchDistributor(ServletRequestUtils.getStringParameter(request,
      * "srchDistributor")); // 제작사/배급사/투자사
      * srchRghtPrps.setSrchDirector(ServletRequestUtils.getStringParameter(request, "srchDirector")); //
      * 감독/연출 srchRghtPrps.setSrchViewGrade(ServletRequestUtils.getStringParameter(request,
      * "srchViewGrade")); // 관람등급
      * srchRghtPrps.setSrchActor(ServletRequestUtils.getStringParameter(request, "srchActor")); // 출연진
      * 
      * // 방송 srchRghtPrps.setSrchProgName(ServletRequestUtils.getStringParameter(request,
      * "srchProgName")); // 프로그램명
      * srchRghtPrps.setSrchMaker(ServletRequestUtils.getStringParameter(request, "srchMaker")); // 제작자
      * 
      * //뉴스 srchRghtPrps.setSrchProviderNm(ServletRequestUtils.getStringParameter(request,
      * "srchProviderNm")); // 제작자
      * 
      * 
      * // 체크박스 값 배열생성 String keyId[] = (ifrmInput.replaceAll("%7C", "|").replaceAll("N",
      * "")).split("&iChk="); // .replaceAll("N", "") 추가
      * 
      * RghtPrps rghtPrps = new RghtPrps(); rghtPrps.setDivs(divs); rghtPrps.setKeyId(keyId);
      * 
      * ModelAndView mv = null;
      * 
      * // 저작물 목록 List prpsRghtList = new ArrayList();
      * 
      * if(ifrmInput.length()>0) prpsRghtList = rghtPrpsService.prpsList(rghtPrps);
      * 
      * // DIV 값에 따라 저작물 종류 구분
      * 
      * if(divs.equals("M")) mv = new ModelAndView("rghtPrps/rghtPrps_musc", "prpsList", prpsRghtList);
      * else if(divs.equals("B")) mv = new ModelAndView("rghtPrps/rghtPrps_book", "prpsList",
      * prpsRghtList); else if(divs.equals("C")) mv = new ModelAndView("rghtPrps/rghtPrps_script",
      * "prpsList", prpsRghtList); else if(divs.equals("I")) mv = new
      * ModelAndView("rghtPrps/rghtPrps_image", "prpsList", prpsRghtList); else if(divs.equals("V")) mv =
      * new ModelAndView("rghtPrps/rghtPrps_mvie", "prpsList", prpsRghtList); else if(divs.equals("R"))
      * mv = new ModelAndView("rghtPrps/rghtPrps_broadcast", "prpsList", prpsRghtList); else
      * if(divs.equals("N")) mv = new ModelAndView("rghtPrps/rghtPrps_news", "prpsList", prpsRghtList);
      * else if(divs.equals("X")) mv = new ModelAndView("rghtPrps/rghtPrps_etc", "prpsList",
      * prpsRghtList);
      * 
      * // 공통코드 조회 시작 CodeList code = new CodeList();
      * 
      * // 신청저작물장르 공통 코드 조회 code.setBigCode("19"); List prpsSideGenreList =
      * codeListService.commonCodeList(code);
      * 
      * //공통코드 조회 종료
      * 
      * Map params = new HashMap(); params.put("userIdnt",
      * ServletRequestUtils.getStringParameter(request, "userIdnt"));
      * 
      * rghtPrps.setDIVS("NOBACK");
      * rghtPrps.setPRPS_RGHT_CODE(ServletRequestUtils.getStringParameter(request, "PRPS_RGHT_CODE",""));
      * // 신청목적
      * 
      * CodeList codeList = new CodeList(); List genreList = null; List broadList = null;
      * if(divs.equals("C")){ codeList.setBigCode("13"); // 방송대본 장르 genreList =
      * codeListService.commonCodeList(codeList); mv.addObject("genreList", genreList);
      * 
      * codeList.setBigCode("16"); // 방송사 broadList = codeListService.commonCodeList(codeList);
      * mv.addObject("broadList", broadList); }
      * 
      * List mediList = null; List chnlList = null; if(divs.equals("R")){ codeList.setBigCode("15"); //
      * 방송 매체 mediList = codeListService.commonCodeList(codeList); mv.addObject("mediList", mediList);
      * 
      * codeList.setBigCode("17"); // 채널 chnlList = codeListService.commonCodeList(codeList);
      * mv.addObject("chnlList", chnlList); }
      * 
      * mv.addObject("userInfo", userService.selectUserInfo(params)); mv.addObject("DIVS", divs);
      * mv.addObject("back", rghtPrps); mv.addObject("rghtPrps", rghtPrps); mv.addObject("listDivs",
      * ServletRequestUtils.getStringParameter(request, "listDivs","")); // 접근 목록 구분 : 권리미상에서 접근한 경우>
      * noneRghtList
      * 
      * // 쌍따옴표 처리 srchRghtPrps.setSrchTitle(srchRghtPrps.getSrchTitle().replaceAll("\"", "&quot;"));
      * srchRghtPrps.setSrchLicensor(srchRghtPrps.getSrchLicensor().replaceAll("\"", "&quot;"));
      * 
      * mv.addObject("srchParam",srchRghtPrps); mv.addObject("PRPS_SIDE_GENRE",prpsSideGenreList); return
      * mv;//rghtPrps_musc.jsp }
      */

     // 권리찾기신청 처리
     public ModelAndView rghtPrpsProc(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          // String USER_IDNT = ServletRequestUtils.getRequiredStringParameter(request, "USER_IDNT");
          int iResult = 1;

          RghtPrps rghtPrps = new RghtPrps();

          bind(request, rghtPrps);

          String divs = ServletRequestUtils.getStringParameter(request, "DIVS");

          String keyId[] = ServletRequestUtils.getStringParameters(request, "iChkVal");

          String PRPS_DOBL_CODE = ServletRequestUtils.getStringParameter(request, "PRPS_DOBL_CODE", ""); // 보상금동시신청 코드

          String PRPS_RGHT_CODE = ServletRequestUtils.getRequiredStringParameter(request, "PRPS_RGHT_CODE");
          String TRST_ORGN_CODE_ARR[] = ServletRequestUtils.getRequiredStringParameters(request, "TRST_ORGN_CODE");

          // 음악
          String MUSIC_TITLE_ARR[] = ServletRequestUtils.getStringParameters(request, "MUSIC_TITLE");
          String ALBUM_TITLE_ARR[] = ServletRequestUtils.getStringParameters(request, "ALBUM_TITLE");

          String LYRICIST_ARR[] = ServletRequestUtils.getStringParameters(request, "LYRICIST");
          String COMPOSER_ARR[] = ServletRequestUtils.getStringParameters(request, "COMPOSER");
          String ARRANGER_ARR[] = ServletRequestUtils.getStringParameters(request, "ARRANGER");

          String SINGER_ARR[] = ServletRequestUtils.getStringParameters(request, "SINGER");
          String PLAYER_ARR[] = ServletRequestUtils.getStringParameters(request, "PLAYER");
          String CONDUCTOR_ARR[] = ServletRequestUtils.getStringParameters(request, "CONDUCTOR");
          String PRODUCER_ARR[] = ServletRequestUtils.getStringParameters(request, "PRODUCER");

          String LYRICIST_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "LYRICIST_ORGN");
          String COMPOSER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "COMPOSER_ORGN");
          String ARRANGER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "ARRANGER_ORGN");

          String SINGER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "SINGER_ORGN");
          String PLAYER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "PLAYER_ORGN");
          String CONDUCTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "CONDUCTOR_ORGN");
          String PRODUCER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "PRODUCER_ORGN");

          String PERF_TIME_ARR[] = ServletRequestUtils.getStringParameters(request, "PERF_TIME");
          // String ISSUED_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "ISSUED_DATE");
          // String LYRICS_ARR[] = ServletRequestUtils.getStringParameters(request, "LYRICS");

          // 도서
          String TITLE_ARR[] = ServletRequestUtils.getStringParameters(request, "TITLE");
          String BOOK_TITLE_ARR[] = ServletRequestUtils.getStringParameters(request, "BOOK_TITLE");
          String PUBLISHER_ARR[] = ServletRequestUtils.getStringParameters(request, "PUBLISHER");
          String FIRST_EDITION_YEAR_ARR[] = ServletRequestUtils.getStringParameters(request, "FIRST_EDITION_YEAR");
          String PUBLISH_TYPE_ARR[] = ServletRequestUtils.getStringParameters(request, "PUBLISH_TYPE");

          String LICENSOR_NAME_KOR_ARR[] = ServletRequestUtils.getStringParameters(request, "LICENSOR_NAME_KOR");
          String LICENSOR_NAME_KOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "LICENSOR_NAME_KOR_ORGN");
          String TRANSLATOR_ARR[] = ServletRequestUtils.getStringParameters(request, "TRANSLATOR");
          String TRANSLATOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "TRANSLATOR_ORGN");

          // 방송대본
          String GENRE_KIND_ARR[] = ServletRequestUtils.getStringParameters(request, "GENRE_KIND");
          String GENRE_KIND_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "GENRE_KIND_NAME");
          String BROAD_MEDI_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_MEDI");
          String BROAD_MEDI_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_MEDI_NAME");
          String BROAD_STAT_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_STAT");
          String BROAD_STAT_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_STAT_NAME");
          String BROAD_ORD_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_ORD");
          // String BROAD_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_DATE");
          String DIRECT_ARR[] = ServletRequestUtils.getStringParameters(request, "DIRECT");
          String MAKER_ARR[] = ServletRequestUtils.getStringParameters(request, "MAKER");
          String WRITER_ARR[] = ServletRequestUtils.getStringParameters(request, "WRITER");
          String WRITER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "WRITER_ORGN");

          // 이미지
          String WORK_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "WORK_NAME");
          String IMAGE_DIVS_ARR[] = ServletRequestUtils.getStringParameters(request, "IMAGE_DIVS");
          String OPEN_MEDI_CODE_ARR[] = ServletRequestUtils.getStringParameters(request, "OPEN_MEDI_CODE");
          String OPEN_MEDI_TEXT_ARR[] = ServletRequestUtils.getStringParameters(request, "OPEN_MEDI_TEXT");
          // String OPEN_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "OPEN_DATE");
          String COPT_HODR_ARR[] = ServletRequestUtils.getStringParameters(request, "COPT_HODR"); // 저작권자
          String COPT_HODR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "COPT_HODR_ORGN");
          String SUBJ_INMT_SEQN_ARR[] = ServletRequestUtils.getStringParameters(request, "SUBJ_INMT_SEQN");

          // 기타
          String PRPS_SIDE_R_GENRE_ARR[] = ServletRequestUtils.getStringParameters(request, "PRPS_SIDE_R_GENRE"); // 분야

          // 영화
          String LEADING_ACTOR_ARR[] = ServletRequestUtils.getStringParameters(request, "LEADING_ACTOR");
          // String PRODUCE_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "PRODUCE_DATE");
          String MEDIA_CODE_VALUE_ARR[] = ServletRequestUtils.getStringParameters(request, "MEDIA_CODE_VALUE");
          String DIRECTOR_ARR[] = ServletRequestUtils.getStringParameters(request, "DIRECTOR");
          String INVESTOR_ARR[] = ServletRequestUtils.getStringParameters(request, "INVESTOR");
          String DISTRIBUTOR_ARR[] = ServletRequestUtils.getStringParameters(request, "DISTRIBUTOR");
          String INVESTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "INVESTOR_ORGN");
          String DISTRIBUTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "DISTRIBUTOR_ORGN");

          // 방송
          String PROG_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "PROG_NAME");
          String MEDI_CODE_ARR[] = ServletRequestUtils.getStringParameters(request, "MEDI_CODE");
          String CHNL_CODE_ARR[] = ServletRequestUtils.getStringParameters(request, "CHNL_CODE");
          String MEDI_CODE_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "MEDI_CODE_NAME");
          String CHNL_CODE_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "CHNL_CODE_NAME");
          String PROG_ORDSEQ_ARR[] = ServletRequestUtils.getStringParameters(request, "PROG_ORDSEQ");
          String PROG_GRAD_ARR[] = ServletRequestUtils.getStringParameters(request, "PROG_GRAD");
          String MAKER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "MAKER_ORGN");

          // ----첨부파일 관련정보 start
          String[] getFileOrgnCode = ServletRequestUtils.getStringParameters(request, "hddnGetFileOrgnCode");
          String[] getRealFileName = ServletRequestUtils.getStringParameters(request, "hddnGetRealFileName");
          String[] getFilePath = ServletRequestUtils.getStringParameters(request, "hddnGetFilePath");
          String[] getFileName = ServletRequestUtils.getStringParameters(request, "hddnGetFileName");
          String[] getFileSize = ServletRequestUtils.getStringParameters(request, "hddnGetFileSize");
          // ----첨부파일 관련정보 end

          // 음원 Set
          rghtPrps.setDivs(divs);
          rghtPrps.setKeyId(keyId);
          rghtPrps.setPRPS_DOBL_CODE(PRPS_DOBL_CODE.replace("Y", "1")); // 1: 보상금 동시신청
          rghtPrps.setPRPS_RGHT_CODE(PRPS_RGHT_CODE);
          rghtPrps.setTRST_ORGN_CODE_ARR(TRST_ORGN_CODE_ARR);
          rghtPrps.setMUSIC_TITLE_ARR(MUSIC_TITLE_ARR);
          rghtPrps.setALBUM_TITLE_ARR(ALBUM_TITLE_ARR);

          rghtPrps.setLYRICIST_ARR(LYRICIST_ARR);
          rghtPrps.setCOMPOSER_ARR(COMPOSER_ARR);
          rghtPrps.setARRANGER_ARR(ARRANGER_ARR);
          rghtPrps.setSINGER_ARR(SINGER_ARR);
          rghtPrps.setPLAYER_ARR(PLAYER_ARR);
          rghtPrps.setCONDUCTOR_ARR(CONDUCTOR_ARR);
          rghtPrps.setPRODUCER_ARR(PRODUCER_ARR);

          rghtPrps.setLYRICIST_ORGN_ARR(LYRICIST_ORGN_ARR);
          rghtPrps.setCOMPOSER_ORGN_ARR(COMPOSER_ORGN_ARR);
          rghtPrps.setARRANGER_ORGN_ARR(ARRANGER_ORGN_ARR);
          rghtPrps.setSINGER_ORGN_ARR(SINGER_ORGN_ARR);
          rghtPrps.setPLAYER_ORGN_ARR(PLAYER_ORGN_ARR);
          rghtPrps.setCONDUCTOR_ORGN_ARR(CONDUCTOR_ORGN_ARR);
          rghtPrps.setPRODUCER_ORGN_ARR(PRODUCER_ORGN_ARR);

          rghtPrps.setPERF_TIME_ARR(PERF_TIME_ARR);
          // rghtPrps.setISSUED_DATE_ARR(ISSUED_DATE_ARR);
          // rghtPrps.setLYRICS_ARR(LYRICS_ARR);

          // 도서 Set
          rghtPrps.setTITLE_ARR(TITLE_ARR);
          rghtPrps.setBOOK_TITLE_ARR(BOOK_TITLE_ARR);
          rghtPrps.setPUBLISHER_ARR(PUBLISHER_ARR);
          rghtPrps.setFIRST_EDITION_YEAR_ARR(FIRST_EDITION_YEAR_ARR);
          rghtPrps.setPUBLISH_TYPE_ARR(PUBLISH_TYPE_ARR);
          rghtPrps.setLICENSOR_NAME_KOR_ARR(LICENSOR_NAME_KOR_ARR);
          rghtPrps.setLICENSOR_NAME_ORGN_KOR_ARR(LICENSOR_NAME_KOR_ORGN_ARR);
          rghtPrps.setTRANSLATOR_ARR(TRANSLATOR_ARR);
          rghtPrps.setTRANSLATOR_ORGN_ARR(TRANSLATOR_ORGN_ARR);

          // 방송대본 Set
          rghtPrps.setGENRE_KIND_ARR(GENRE_KIND_ARR);
          rghtPrps.setGENRE_KIND_NAME_ARR(GENRE_KIND_NAME_ARR);
          rghtPrps.setBROAD_MEDI_ARR(BROAD_MEDI_ARR);
          rghtPrps.setBROAD_MEDI_NAME_ARR(BROAD_MEDI_NAME_ARR);
          rghtPrps.setBROAD_STAT_ARR(BROAD_STAT_ARR);
          rghtPrps.setBROAD_STAT_NAME_ARR(BROAD_STAT_NAME_ARR);
          rghtPrps.setBROAD_ORD_ARR(BROAD_ORD_ARR);
          // rghtPrps.setBROAD_DATE_ARR(BROAD_DATE_ARR);
          rghtPrps.setDIRECT_ARR(DIRECT_ARR);
          rghtPrps.setMAKER_ARR(MAKER_ARR);
          rghtPrps.setWRITER_ARR(WRITER_ARR);
          rghtPrps.setWRITER_ORGN_ARR(WRITER_ORGN_ARR);

          // 이미지 Set
          rghtPrps.setWORK_NAME_ARR(WORK_NAME_ARR);
          rghtPrps.setIMAGE_DIVS_ARR(IMAGE_DIVS_ARR);
          rghtPrps.setOPEN_MEDI_CODE_ARR(OPEN_MEDI_CODE_ARR);
          rghtPrps.setOPEN_MEDI_TEXT_ARR(OPEN_MEDI_TEXT_ARR);
          // rghtPrps.setOPEN_DATE_ARR(OPEN_DATE_ARR);
          rghtPrps.setCOPT_HODR_ARR(COPT_HODR_ARR);
          rghtPrps.setCOPT_HODR_ORGN_ARR(COPT_HODR_ORGN_ARR);
          rghtPrps.setSUBJ_INMT_SEQN_ARR(SUBJ_INMT_SEQN_ARR);

          // 기타
          String PRPS_SIDE_GENRE = ServletRequestUtils.getStringParameter(request, "PRPS_SIDE_GENRE");
          String PRPS_RGHT_DESC = ServletRequestUtils.getStringParameter(request, "PRPS_RGHT_DESC");
          rghtPrps.setPRPS_SIDE_R_GENRE_ARR(PRPS_SIDE_R_GENRE_ARR);
          rghtPrps.setPRPS_SIDE_GENRE(PRPS_SIDE_GENRE);
          rghtPrps.setPRPS_RGHT_DESC(PRPS_RGHT_DESC);

          // 영화 Set
          rghtPrps.setDIRECTOR_ARR(DIRECTOR_ARR);
          rghtPrps.setLEADING_ACTOR_ARR(LEADING_ACTOR_ARR);
          // rghtPrps.setPRODUCE_DATE_ARR(PRODUCE_DATE_ARR);
          rghtPrps.setMEDIA_CODE_VALUE_ARR(MEDIA_CODE_VALUE_ARR);
          rghtPrps.setPRODUCER_ARR(PRODUCER_ARR);
          rghtPrps.setPRODUCER_ORGN_ARR(PRODUCER_ORGN_ARR);
          rghtPrps.setINVESTOR_ARR(INVESTOR_ARR);
          rghtPrps.setINVESTOR_ORGN_ARR(INVESTOR_ORGN_ARR);
          rghtPrps.setDISTRIBUTOR_ARR(DISTRIBUTOR_ARR);
          rghtPrps.setDISTRIBUTOR_ORGN_ARR(DISTRIBUTOR_ORGN_ARR);

          // 방송 Set
          rghtPrps.setPROG_NAME_ARR(PROG_NAME_ARR);
          rghtPrps.setMEDI_CODE_ARR(MEDI_CODE_ARR);
          rghtPrps.setCHNL_CODE_ARR(CHNL_CODE_ARR);
          rghtPrps.setMEDI_CODE_NAME_ARR(MEDI_CODE_NAME_ARR);
          rghtPrps.setCHNL_CODE_NAME_ARR(CHNL_CODE_NAME_ARR);
          rghtPrps.setPROG_ORDSEQ_ARR(PROG_ORDSEQ_ARR);
          rghtPrps.setPROG_GRAD_ARR(PROG_GRAD_ARR);
          rghtPrps.setMAKER_ORGN_ARR(MAKER_ORGN_ARR);

          // -----------------------달력이미지 추가 수정 Start-----------------------//
          // 음악 발매일
          String ISSUED_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "ISSUED_DATE");
          String ISSUED_DATE_ARR2[] = new String[ISSUED_DATE_ARR.length];
          String issued = "";

          for (int f = 1; f <= ISSUED_DATE_ARR.length; f++) {

               issued = ServletRequestUtils.getStringParameter(request, "ISSUED_DATE_" + f);
               ISSUED_DATE_ARR2[f - 1] = issued;
          }
          rghtPrps.setISSUED_DATE_ARR(ISSUED_DATE_ARR2);


          // 방송대본 (방송일시)
          String BROAD_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_DATE");
          String BROAD_DATE_ARR2[] = new String[BROAD_DATE_ARR.length];
          String broad = "";

          for (int f = 1; f <= BROAD_DATE_ARR.length; f++) {

               broad = ServletRequestUtils.getStringParameter(request, "BROAD_DATE_" + f);
               BROAD_DATE_ARR2[f - 1] = broad;
          }
          rghtPrps.setBROAD_DATE_ARR(BROAD_DATE_ARR2);

          // 이미지 (공표일자)
          String OPEN_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "OPEN_DATE");
          String OPEN_DATE_ARR2[] = new String[OPEN_DATE_ARR.length];

          String open = "";
          for (int f = 1; f <= OPEN_DATE_ARR.length; f++) {

               open = ServletRequestUtils.getStringParameter(request, "OPEN_DATE_" + f);
               OPEN_DATE_ARR2[f - 1] = open;
          }
          rghtPrps.setOPEN_DATE_ARR(OPEN_DATE_ARR2);

          // 영화 (제작일자)
          String PRODUCE_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "PRODUCE_DATE");
          String PRODUCE_DATE_ARR2[] = new String[PRODUCE_DATE_ARR.length];

          String produce = "";
          for (int f = 1; f <= PRODUCE_DATE_ARR.length; f++) {

               produce = ServletRequestUtils.getStringParameter(request, "PRODUCE_DATE_" + f);
               PRODUCE_DATE_ARR2[f - 1] = produce;
          }
          rghtPrps.setPRODUCE_DATE_ARR(PRODUCE_DATE_ARR2);
          // -----------------------달력이미지 추가 수정 End-----------------------//

          // File Upload
          List fileList = new ArrayList();

          for (int f = 0; f < getFileName.length; f++) {
               PrpsAttc prpsAttc = new PrpsAttc();

               prpsAttc.setREAL_FILE_NAME(getRealFileName[f]);
               prpsAttc.setTRST_ORGN_CODE(getFileOrgnCode[f]);
               prpsAttc.setFILE_PATH(getFilePath[f]);
               prpsAttc.setFILE_NAME(getFileName[f]);
               prpsAttc.setFILE_SIZE(getFileSize[f]);

               fileList.add(prpsAttc);
          }

          rghtPrps.setFileList(fileList);

          Map params = new HashMap();
          params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "USER_IDNT"));
          Map userInfo = userService.selectUserInfo(params); // 사용자 조회

          rghtPrps.setSESS_U_MAIL((String) userInfo.get("U_MAIL"));
          rghtPrps.setSESS_U_MAIL_RECE_YSNO((String) userInfo.get("U_MAIL_RECE_YSNO"));
          rghtPrps.setSESS_U_MOBL_PHON((String) userInfo.get("U_MOBL_PHON"));
          rghtPrps.setSESS_U_SMS_RECE_YSNO((String) userInfo.get("U_SMS_RECE_YSNO"));

          // int iResult = rghtPrpsService.rghtPrpsSave(rghtPrps);
          RghtPrps reRghtPrps = rghtPrpsService.rghtPrpsSave(rghtPrps);
          iResult = reRghtPrps.getIResult();

          ModelAndView mv = null;

          if (PRPS_DOBL_CODE.equals("Y") && iResult == 1) {
               // 동시신청 + 신청성공

               // div에 따른 보상금 신청화면으로 이동
               String srchDIVS = "1";
               if (divs.equals("M"))
                    srchDIVS = "1"; // 음악 -> 방송음악
               else if (divs.equals("I"))
                    srchDIVS = "2"; // 이미지 -> 교과용
               else if (divs.equals("B"))
                    srchDIVS = "3"; // 도서 -> 도서관
               else if (divs.equals("A"))
                    srchDIVS = "4"; // 도서 -> 도서관

               mv = new ModelAndView("redirect:/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + srchDIVS);

               mv.addObject("ifrmInput", reRghtPrps.getINMT_SEQN()); // 보상금key
               mv.addObject("rghtPrpsMastKey", reRghtPrps.getPRPS_MAST_KEY()); // // 권리찾기 mastKey
               mv.addObject("prpsDoblCode", "1"); // 보상금 동시신청 코드값

          } else {
               mv = new ModelAndView("rghtPrps/muscRghtSucc", "DIVS", divs);

               mv.addObject("iResult", iResult);
               mv.addObject("listDivs", ServletRequestUtils.getStringParameter(request, "listDivs", "")); // 접근 목록 구분 : 권리미상에서 접근한 경우> noneRghtList
          }

          return mv;
     }


     // 권리찾기신청 수정처리
     public ModelAndView rghtPrpsModiProc(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          // String USER_IDNT = ServletRequestUtils.getRequiredStringParameter(request, "USER_IDNT");
          CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
          MultipartHttpServletRequest multipartRequest;
          multipartRequest = multipartResolver.resolveMultipart(request);

          RghtPrps rghtPrps = new RghtPrps();

          bind(multipartRequest, rghtPrps);

          String mail = rghtPrps.getMAIL();
          String home_addr = rghtPrps.getHOME_ADDR();
          String busi_addr = rghtPrps.getBUSI_ADDR();
          String prps_desc = rghtPrps.getPRPS_DESC();


          rghtPrps.setMAIL(XssUtil.unscript(mail));
          rghtPrps.setHOME_ADDR(XssUtil.unscript(home_addr));
          rghtPrps.setBUSI_ADDR(XssUtil.unscript(busi_addr));
          rghtPrps.setPRPS_DESC(XssUtil.unscript(prps_desc));



          String divs = ServletRequestUtils.getStringParameter(multipartRequest, "DIVS");

          String keyId[] = ServletRequestUtils.getStringParameters(multipartRequest, "iChkVal");

          String PRPS_RGHT_CODE = ServletRequestUtils.getRequiredStringParameter(multipartRequest, "PRPS_RGHT_CODE");
          String TRST_ORGN_CODE_ARR[] = ServletRequestUtils.getRequiredStringParameters(multipartRequest, "TRST_ORGN_CODE");

          // 음악
          String MUSIC_TITLE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MUSIC_TITLE");
          String ALBUM_TITLE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "ALBUM_TITLE");

          String LYRICIST_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LYRICIST");
          String COMPOSER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "COMPOSER");
          String ARRANGER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "ARRANGER");

          String SINGER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "SINGER");
          String PLAYER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PLAYER");
          String CONDUCTOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "CONDUCTOR");
          String PRODUCER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PRODUCER");

          String LYRICIST_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LYRICIST_ORGN");
          String COMPOSER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "COMPOSER_ORGN");
          String ARRANGER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "ARRANGER_ORGN");

          String SINGER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "SINGER_ORGN");
          String PLAYER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PLAYER_ORGN");
          String CONDUCTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "CONDUCTOR_ORGN");
          String PRODUCER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PRODUCER_ORGN");

          String PERF_TIME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PERF_TIME");
          // String ISSUED_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest,
          // "ISSUED_DATE");
          // String LYRICS_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LYRICS");

          // 도서
          String TITLE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "TITLE");
          String BOOK_TITLE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BOOK_TITLE");
          String PUBLISHER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PUBLISHER");
          String FIRST_EDITION_YEAR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "FIRST_EDITION_YEAR");
          String PUBLISH_TYPE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PUBLISH_TYPE");

          String LICENSOR_NAME_KOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LICENSOR_NAME_KOR");
          String LICENSOR_NAME_KOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LICENSOR_NAME_KOR_ORGN");
          String TRANSLATOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "TRANSLATOR");
          String TRANSLATOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "TRANSLATOR_ORGN");

          // 방송대본
          String GENRE_KIND_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "GENRE_KIND");
          String BROAD_MEDI_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BROAD_MEDI");
          String BROAD_STAT_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BROAD_STAT");
          String BROAD_ORD_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BROAD_ORD");
          // String BROAD_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest,
          // "BROAD_DATE");
          String DIRECT_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "DIRECT");
          String MAKER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MAKER");
          String WRITER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "WRITER");
          String WRITER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "WRITER_ORGN");

          // 이미지
          String WORK_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "WORK_NAME");
          String IMAGE_DIVS_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "IMAGE_DIVS");
          String OPEN_MEDI_CODE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "OPEN_MEDI_CODE");
          String OPEN_MEDI_TEXT_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "OPEN_MEDI_TEXT");
          // String OPEN_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "OPEN_DATE");
          String COPT_HODR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "COPT_HODR"); // 저작권자
          String COPT_HODR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "COPT_HODR_ORGN");

          // 기타
          String PRPS_SIDE_R_GENRE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PRPS_SIDE_R_GENRE"); // 분야

          // 영화
          String LEADING_ACTOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LEADING_ACTOR");
          // String PRODUCE_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest,
          // "PRODUCE_DATE");
          String MEDIA_CODE_VALUE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MEDIA_CODE_VALUE");
          String DIRECTOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "DIRECTOR");
          String INVESTOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "INVESTOR");
          String DISTRIBUTOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "DISTRIBUTOR");
          String DISTRIBUTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "DISTRIBUTOR_ORGN");
          String INVESTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "INVESTOR_ORGN");

          // 방송
          String PROG_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PROG_NAME");
          String MEDI_CODE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MEDI_CODE");
          String CHNL_CODE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "CHNL_CODE");
          String MEDI_CODE_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MEDI_CODE_NAME");
          String CHNL_CODE_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "CHNL_CODE_NAME");
          String PROG_ORDSEQ_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PROG_ORDSEQ");
          String PROG_GRAD_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PROG_GRAD");
          String MAKER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MAKER_ORGN");

          String offxLineRecp = ServletRequestUtils.getStringParameter(multipartRequest, "OFFX_LINE_RECP");

          // 음원 Set
          rghtPrps.setDivs(divs);
          rghtPrps.setKeyId(keyId);
          rghtPrps.setPRPS_RGHT_CODE(PRPS_RGHT_CODE);
          rghtPrps.setTRST_ORGN_CODE_ARR(TRST_ORGN_CODE_ARR);
          rghtPrps.setMUSIC_TITLE_ARR(MUSIC_TITLE_ARR);
          rghtPrps.setALBUM_TITLE_ARR(ALBUM_TITLE_ARR);


          rghtPrps.setLYRICIST_ARR(XssUtil.unscriptArr(LYRICIST_ARR));
          rghtPrps.setCOMPOSER_ARR(XssUtil.unscriptArr(COMPOSER_ARR));
          rghtPrps.setARRANGER_ARR(XssUtil.unscriptArr(ARRANGER_ARR));
          rghtPrps.setSINGER_ARR(XssUtil.unscriptArr(SINGER_ARR));
          rghtPrps.setPLAYER_ARR(XssUtil.unscriptArr(PLAYER_ARR));
          rghtPrps.setCONDUCTOR_ARR(XssUtil.unscriptArr(CONDUCTOR_ARR));
          rghtPrps.setPRODUCER_ARR(XssUtil.unscriptArr(PRODUCER_ARR));

          rghtPrps.setLYRICIST_ORGN_ARR(XssUtil.unscriptArr(LYRICIST_ORGN_ARR));
          rghtPrps.setCOMPOSER_ORGN_ARR(XssUtil.unscriptArr(COMPOSER_ORGN_ARR));
          rghtPrps.setARRANGER_ORGN_ARR(XssUtil.unscriptArr(ARRANGER_ORGN_ARR));
          rghtPrps.setSINGER_ORGN_ARR(XssUtil.unscriptArr(SINGER_ORGN_ARR));
          rghtPrps.setPLAYER_ORGN_ARR(XssUtil.unscriptArr(PLAYER_ORGN_ARR));
          rghtPrps.setCONDUCTOR_ORGN_ARR(XssUtil.unscriptArr(CONDUCTOR_ORGN_ARR));
          rghtPrps.setPRODUCER_ORGN_ARR(XssUtil.unscriptArr(PRODUCER_ORGN_ARR));

          rghtPrps.setPERF_TIME_ARR(PERF_TIME_ARR);
          // rghtPrps.setISSUED_DATE_ARR(ISSUED_DATE_ARR);
          // rghtPrps.setLYRICS_ARR(LYRICS_ARR);

          // 도서 Set
          rghtPrps.setTITLE_ARR(TITLE_ARR);
          rghtPrps.setBOOK_TITLE_ARR(BOOK_TITLE_ARR);
          rghtPrps.setPUBLISHER_ARR(PUBLISHER_ARR);
          rghtPrps.setFIRST_EDITION_YEAR_ARR(FIRST_EDITION_YEAR_ARR);
          rghtPrps.setPUBLISH_TYPE_ARR(PUBLISH_TYPE_ARR);
          rghtPrps.setLICENSOR_NAME_KOR_ARR(XssUtil.unscriptArr(LICENSOR_NAME_KOR_ARR));
          rghtPrps.setLICENSOR_NAME_ORGN_KOR_ARR(XssUtil.unscriptArr(LICENSOR_NAME_KOR_ORGN_ARR));
          rghtPrps.setTRANSLATOR_ARR(XssUtil.unscriptArr(TRANSLATOR_ARR));
          rghtPrps.setTRANSLATOR_ORGN_ARR(XssUtil.unscriptArr(TRANSLATOR_ORGN_ARR));

          // 방송대본 Set
          rghtPrps.setGENRE_KIND_ARR(GENRE_KIND_ARR);
          rghtPrps.setBROAD_MEDI_ARR(BROAD_MEDI_ARR);
          rghtPrps.setBROAD_STAT_ARR(BROAD_STAT_ARR);
          rghtPrps.setBROAD_ORD_ARR(BROAD_ORD_ARR);
          // rghtPrps.setBROAD_DATE_ARR(BROAD_DATE_ARR);
          rghtPrps.setDIRECT_ARR(DIRECT_ARR);
          rghtPrps.setMAKER_ARR(XssUtil.unscriptArr(MAKER_ARR));
          rghtPrps.setWRITER_ARR(XssUtil.unscriptArr(WRITER_ARR));
          rghtPrps.setWRITER_ORGN_ARR(XssUtil.unscriptArr(WRITER_ORGN_ARR));

          // 이미지 Set
          rghtPrps.setWORK_NAME_ARR(XssUtil.unscriptArr(WORK_NAME_ARR));
          rghtPrps.setIMAGE_DIVS_ARR(IMAGE_DIVS_ARR);
          rghtPrps.setOPEN_MEDI_CODE_ARR(OPEN_MEDI_CODE_ARR);
          rghtPrps.setOPEN_MEDI_TEXT_ARR(XssUtil.unscriptArr(OPEN_MEDI_TEXT_ARR));
          // rghtPrps.setOPEN_DATE_ARR(OPEN_DATE_ARR);

          if (divs.equals("X")) {
               if (rghtPrps.getPRPS_RGHT_CODE().equals("1") || rghtPrps.getPRPS_RGHT_CODE().equals("2")) {
                    rghtPrps.setCOPT_HODR_ARR(XssUtil.unscriptArr(COPT_HODR_ARR));
                    rghtPrps.setCOPT_HODR_ORGN_ARR(XssUtil.unscriptArr(COPT_HODR_ORGN_ARR));
               }
          } else {
               rghtPrps.setCOPT_HODR_ARR(XssUtil.unscriptArr(COPT_HODR_ARR));
               rghtPrps.setCOPT_HODR_ORGN_ARR(XssUtil.unscriptArr(COPT_HODR_ORGN_ARR));
          }

          // 기타 Set
          String PRPS_SIDE_GENRE = ServletRequestUtils.getStringParameter(multipartRequest, "PRPS_SIDE_GENRE");
          String PRPS_RGHT_DESC = ServletRequestUtils.getStringParameter(multipartRequest, "PRPS_RGHT_DESC");
          rghtPrps.setPRPS_SIDE_R_GENRE_ARR(PRPS_SIDE_R_GENRE_ARR);
          rghtPrps.setPRPS_SIDE_GENRE(PRPS_SIDE_GENRE);
          rghtPrps.setPRPS_RGHT_DESC(XssUtil.unscript(PRPS_RGHT_DESC));

          // 영화 Set
          rghtPrps.setDIRECTOR_ARR(DIRECTOR_ARR);
          rghtPrps.setLEADING_ACTOR_ARR(LEADING_ACTOR_ARR);
          // rghtPrps.setPRODUCE_DATE_ARR(PRODUCE_DATE_ARR);
          rghtPrps.setMEDIA_CODE_VALUE_ARR(MEDIA_CODE_VALUE_ARR);
          rghtPrps.setPRODUCER_ARR(XssUtil.unscriptArr(PRODUCER_ARR));
          rghtPrps.setPRODUCER_ORGN_ARR(XssUtil.unscriptArr(PRODUCER_ORGN_ARR));
          rghtPrps.setINVESTOR_ARR(XssUtil.unscriptArr(INVESTOR_ARR));
          rghtPrps.setINVESTOR_ORGN_ARR(XssUtil.unscriptArr(INVESTOR_ORGN_ARR));
          rghtPrps.setDISTRIBUTOR_ARR(XssUtil.unscriptArr(DISTRIBUTOR_ARR));
          rghtPrps.setDISTRIBUTOR_ORGN_ARR(XssUtil.unscriptArr(DISTRIBUTOR_ORGN_ARR));

          // 방송 Set
          rghtPrps.setPROG_NAME_ARR(PROG_NAME_ARR);
          rghtPrps.setMEDI_CODE_ARR(MEDI_CODE_ARR);
          rghtPrps.setCHNL_CODE_ARR(CHNL_CODE_ARR);
          rghtPrps.setMEDI_CODE_NAME_ARR(MEDI_CODE_NAME_ARR);
          rghtPrps.setCHNL_CODE_NAME_ARR(CHNL_CODE_NAME_ARR);
          rghtPrps.setPROG_ORDSEQ_ARR(PROG_ORDSEQ_ARR);
          rghtPrps.setPROG_GRAD_ARR(PROG_GRAD_ARR);
          rghtPrps.setMAKER_ORGN_ARR(XssUtil.unscriptArr(MAKER_ORGN_ARR));

          // -----------------------달력이미지 추가 수정 Start-----------------------//
          // 음악 발매일
          if (rghtPrps.getPRPS_RGHT_CODE().equals("1") || rghtPrps.getPRPS_RGHT_CODE().equals("2")) {
               String ISSUED_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "ISSUED_DATE");
               String ISSUED_DATE_ARR2[] = new String[ISSUED_DATE_ARR.length];

               String issued = "";
               String seq = "";
               for (int f = 1; f <= ISSUED_DATE_ARR.length; f++) {
                    seq = ISSUED_DATE_ARR[f - 1];

                    issued = ServletRequestUtils.getStringParameter(multipartRequest, "ISSUED_DATE_" + seq);
                    ISSUED_DATE_ARR2[f - 1] = issued;
               }
               rghtPrps.setISSUED_DATE_ARR(ISSUED_DATE_ARR2);
          } else if (rghtPrps.getPRPS_RGHT_CODE().equals("3")) {
               String ISSUED_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "VAL_ISSUED_DATE");
               String ISSUED_DATE_ARR2[] = new String[ISSUED_DATE_ARR.length];

               String issued = "";
               String seq = "";
               for (int f = 1; f <= ISSUED_DATE_ARR.length; f++) {
                    seq = ISSUED_DATE_ARR[f - 1];

                    issued = ServletRequestUtils.getStringParameter(multipartRequest, "VAL_ISSUED_DATE_" + seq);
                    ISSUED_DATE_ARR2[f - 1] = issued;
               }
               rghtPrps.setISSUED_DATE_ARR(ISSUED_DATE_ARR2);
          }

          // 방송대본 (방송일시), 방송 (방송일자)
          if (rghtPrps.getPRPS_RGHT_CODE().equals("1") || rghtPrps.getPRPS_RGHT_CODE().equals("2")) {
               String BROAD_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BROAD_DATE");
               String BROAD_DATE_ARR2[] = new String[BROAD_DATE_ARR.length];

               String broad = "";
               String broadSeq = "";
               for (int f = 1; f <= BROAD_DATE_ARR.length; f++) {
                    broadSeq = BROAD_DATE_ARR[f - 1];

                    broad = ServletRequestUtils.getStringParameter(multipartRequest, "BROAD_DATE_" + broadSeq);
                    BROAD_DATE_ARR2[f - 1] = broad;
               }
               rghtPrps.setBROAD_DATE_ARR(BROAD_DATE_ARR2);
          } else if (rghtPrps.getPRPS_RGHT_CODE().equals("3")) {
               String BROAD_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "VAL_BROAD_DATE");
               String BROAD_DATE_ARR2[] = new String[BROAD_DATE_ARR.length];

               String broad = "";
               String broadSeq = "";
               for (int f = 1; f <= BROAD_DATE_ARR.length; f++) {
                    broadSeq = BROAD_DATE_ARR[f - 1];

                    broad = ServletRequestUtils.getStringParameter(multipartRequest, "VAL_BROAD_DATE_" + broadSeq);
                    BROAD_DATE_ARR2[f - 1] = broad;
               }
               rghtPrps.setBROAD_DATE_ARR(BROAD_DATE_ARR2);
          }

          // 이미지 (공표일자)
          if (rghtPrps.getPRPS_RGHT_CODE().equals("1") || rghtPrps.getPRPS_RGHT_CODE().equals("2")) {
               String OPEN_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "OPEN_DATE");
               String OPEN_DATE_ARR2[] = new String[OPEN_DATE_ARR.length];

               String open = "";
               String openSeq = "";
               for (int f = 1; f <= OPEN_DATE_ARR.length; f++) {
                    openSeq = OPEN_DATE_ARR[f - 1];

                    open = ServletRequestUtils.getStringParameter(multipartRequest, "OPEN_DATE_" + openSeq);
                    OPEN_DATE_ARR2[f - 1] = open;
               }
               rghtPrps.setOPEN_DATE_ARR(OPEN_DATE_ARR2);
          } else if (rghtPrps.getPRPS_RGHT_CODE().equals("3")) {
               String OPEN_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "VAL_OPEN_DATE");
               String OPEN_DATE_ARR2[] = new String[OPEN_DATE_ARR.length];

               String open = "";
               String openSeq = "";
               for (int f = 1; f <= OPEN_DATE_ARR.length; f++) {
                    openSeq = OPEN_DATE_ARR[f - 1];

                    open = ServletRequestUtils.getStringParameter(multipartRequest, "VAL_OPEN_DATE_" + openSeq);
                    OPEN_DATE_ARR2[f - 1] = open;
               }
               rghtPrps.setOPEN_DATE_ARR(OPEN_DATE_ARR2);
          }

          // 영화 (제작일자)
          if (rghtPrps.getPRPS_RGHT_CODE().equals("1") || rghtPrps.getPRPS_RGHT_CODE().equals("2")) {

               String PRODUCE_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PRODUCE_DATE");

               String PRODUCE_DATE_ARR2[] = new String[PRODUCE_DATE_ARR.length];
               String produce = "";
               String oproduceSeq = "";
               for (int f = 1; f <= PRODUCE_DATE_ARR.length; f++) {
                    oproduceSeq = PRODUCE_DATE_ARR[f - 1];

                    produce = ServletRequestUtils.getStringParameter(multipartRequest, "PRODUCE_DATE_" + oproduceSeq);
                    PRODUCE_DATE_ARR2[f - 1] = produce;
               }
               rghtPrps.setPRODUCE_DATE_ARR(PRODUCE_DATE_ARR2);
          } else if (rghtPrps.getPRPS_RGHT_CODE().equals("3")) {
               String PRODUCE_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "VAL_PRODUCE_DATE");

               String PRODUCE_DATE_ARR2[] = new String[PRODUCE_DATE_ARR.length];
               String produce = "";
               String oproduceSeq = "";
               for (int f = 1; f <= PRODUCE_DATE_ARR.length; f++) {
                    oproduceSeq = PRODUCE_DATE_ARR[f - 1];

                    produce = ServletRequestUtils.getStringParameter(multipartRequest, "VAL_PRODUCE_DATE_" + oproduceSeq);
                    PRODUCE_DATE_ARR2[f - 1] = produce;
               }
               rghtPrps.setPRODUCE_DATE_ARR(PRODUCE_DATE_ARR2);
          }

          // ----첨부파일 관련정보 start
          String[] getFileOrgnCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileOrgnCode");
          String[] getRealFileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetRealFileName");
          String[] getFilePath = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFilePath");
          String[] getFileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileName");
          String[] getFileSize = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileSize");

          String[] fileOrgnCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFileOrgnCode");
          String[] fileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFileName");

          String hddnDelFileInfo = ServletRequestUtils.getStringParameter(multipartRequest, "hddnDelFileInfo", "");
          rghtPrps.setDELT_FILE_INFO(hddnDelFileInfo.split(","));

          // ----첨부파일 관련정보 end

          // ----첨부Upload start
          List fileList = new ArrayList();

          // 기존파일
          for (int f = 0; f < getFileName.length; f++) {
               PrpsAttc prpsAttc = new PrpsAttc();

               prpsAttc.setREAL_FILE_NAME(getRealFileName[f]);
               prpsAttc.setTRST_ORGN_CODE(getFileOrgnCode[f]);
               prpsAttc.setFILE_PATH(getFilePath[f]);
               prpsAttc.setFILE_NAME(getFileName[f]);
               prpsAttc.setFILE_SIZE(getFileSize[f]);

               fileList.add(prpsAttc);
          }

          // ----new 첨부파일등록 start
          // File Upload
          Iterator fileNameIterator = multipartRequest.getFileNames();

          while (fileNameIterator.hasNext()) {
               MultipartFile multiFile = multipartRequest.getFile((String) fileNameIterator.next());
               if (multiFile.getSize() > 0) {
                    PrpsAttc prpsAttc = FileUploadUtil.uploadPrpsFiles(multiFile, realUploadPath);

                    for (int fm = 0; fm < fileName.length; fm++) {
                         if (fileName[fm].toUpperCase().equals(prpsAttc.getFILE_NAME().toUpperCase())) {
                              // if(fileName[fm].equals(prpsAttc.getFILE_NAME())){
                              prpsAttc.setFILE_NAME(fileName[fm]);
                              prpsAttc.setTRST_ORGN_CODE(fileOrgnCode[fm]);
                         }
                    }

                    fileList.add(prpsAttc);
               }
          }

          rghtPrps.setFileList(fileList);

          int iResult = rghtPrpsService.rghtPrpsModi(rghtPrps);

          ModelAndView mv = new ModelAndView("rsltInqr/muscRghtModiSucc", "DIVS", divs);

          mv.addObject("iResult", iResult);

          return mv;
     }

     // 권리찾기신청 확인화면
     public ModelAndView rghtPrpsProcDetl(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          // String USER_IDNT = ServletRequestUtils.getRequiredStringParameter(request, "USER_IDNT");
          CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
          MultipartHttpServletRequest multipartRequest;
          multipartRequest = multipartResolver.resolveMultipart(request);

          RghtPrps rghtPrps = new RghtPrps();

          bind(multipartRequest, rghtPrps);

          String prpsDesc = ServletRequestUtils.getStringParameter(multipartRequest, "PRPS_DESC");

          String divs = ServletRequestUtils.getStringParameter(multipartRequest, "DIVS");

          String keyId[] = ServletRequestUtils.getStringParameters(multipartRequest, "iChkVal");

          String PRPS_DOBL_CODE = ServletRequestUtils.getStringParameter(multipartRequest, "PRPS_DOBL_CODE"); // 보상금동시신청코드
          String PRPS_RGHT_CODE = ServletRequestUtils.getRequiredStringParameter(multipartRequest, "PRPS_RGHT_CODE");
          String TRST_ORGN_CODE_ARR[] = ServletRequestUtils.getRequiredStringParameters(multipartRequest, "TRST_ORGN_CODE");

          // 음악
          String MUSIC_TITLE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MUSIC_TITLE");
          String ALBUM_TITLE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "ALBUM_TITLE");

          String LYRICIST_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LYRICIST");
          String COMPOSER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "COMPOSER");
          String ARRANGER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "ARRANGER");

          String SINGER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "SINGER");
          String PLAYER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PLAYER");
          String CONDUCTOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "CONDUCTOR");
          String PRODUCER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PRODUCER");

          String LYRICIST_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LYRICIST_ORGN");
          String COMPOSER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "COMPOSER_ORGN");
          String ARRANGER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "ARRANGER_ORGN");

          String SINGER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "SINGER_ORGN");
          String PLAYER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PLAYER_ORGN");
          String CONDUCTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "CONDUCTOR_ORGN");
          String PRODUCER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PRODUCER_ORGN");

          String PERF_TIME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PERF_TIME");

          // String ISSUED_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest,
          // "ISSUED_DATE");
          // String LYRICS_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LYRICS");

          // 도서
          String TITLE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "TITLE");
          String BOOK_TITLE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BOOK_TITLE");
          String PUBLISHER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PUBLISHER");
          String FIRST_EDITION_YEAR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "FIRST_EDITION_YEAR");
          String PUBLISH_TYPE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PUBLISH_TYPE");

          String LICENSOR_NAME_KOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LICENSOR_NAME_KOR");
          String LICENSOR_NAME_KOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LICENSOR_NAME_KOR_ORGN");
          String TRANSLATOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "TRANSLATOR");
          String TRANSLATOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "TRANSLATOR_ORGN");

          // 방송대본
          String GENRE_KIND_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "GENRE_KIND");
          String GENRE_KIND_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "GENRE_KIND_NAME");
          String BROAD_MEDI_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BROAD_MEDI");
          String BROAD_MEDI_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BROAD_MEDI_NAME");
          String BROAD_STAT_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BROAD_STAT");
          String BROAD_STAT_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BROAD_STAT_NAME");
          String BROAD_ORD_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BROAD_ORD");
          // String BROAD_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest,
          // "BROAD_DATE");
          String DIRECT_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "DIRECT");
          String MAKER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MAKER");
          String WRITER_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "WRITER");
          String WRITER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "WRITER_ORGN");

          // 이미지
          String WORK_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "WORK_NAME"); // 이미지명
          String IMAGE_DIVS_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "IMAGE_DIVS"); // 분야
          String OPEN_MEDI_CODE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "OPEN_MEDI_CODE"); // 공표매체코드
          String OPEN_MEDI_TEXT_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "OPEN_MEDI_TEXT"); // 공표매체텍스트
          // String OPEN_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "OPEN_DATE");
          // // 공표일자
          String COPT_HODR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "COPT_HODR"); // 저작권자
          String COPT_HODR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "COPT_HODR_ORGN"); // 저작권자
          String SUBJ_INMT_SEQN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "SUBJ_INMT_SEQN"); // 교과용 보상금 정보

          // 기타
          String PRPS_SIDE_R_GENRE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PRPS_SIDE_R_GENRE"); // 분야

          // 영화
          String LEADING_ACTOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "LEADING_ACTOR");
          // String PRODUCE_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest,
          // "PRODUCE_DATE");
          String MEDIA_CODE_VALUE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MEDIA_CODE_VALUE");
          String DIRECTOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "DIRECTOR");
          String INVESTOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "INVESTOR");
          String DISTRIBUTOR_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "DISTRIBUTOR");
          String DISTRIBUTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "DISTRIBUTOR_ORGN");
          String INVESTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "INVESTOR_ORGN");

          // 방송
          String PROG_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PROG_NAME");
          String MEDI_CODE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MEDI_CODE");
          String CHNL_CODE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "CHNL_CODE");
          String MEDI_CODE_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MEDI_CODE_NAME");
          String CHNL_CODE_NAME_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "CHNL_CODE_NAME");
          String PROG_ORDSEQ_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PROG_ORDSEQ");
          String PROG_GRAD_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PROG_GRAD");
          String MAKER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "MAKER_ORGN");

          // 음원
          rghtPrps.setDivs(divs);
          rghtPrps.setKeyId(keyId);
          rghtPrps.setPRPS_DOBL_CODE(PRPS_DOBL_CODE);
          rghtPrps.setPRPS_RGHT_CODE(PRPS_RGHT_CODE);
          rghtPrps.setTRST_ORGN_CODE_ARR(TRST_ORGN_CODE_ARR);
          rghtPrps.setMUSIC_TITLE_ARR(MUSIC_TITLE_ARR);
          rghtPrps.setALBUM_TITLE_ARR(ALBUM_TITLE_ARR);

          rghtPrps.setLYRICIST_ARR(XssUtil.unscriptArr(LYRICIST_ARR));
          rghtPrps.setCOMPOSER_ARR(XssUtil.unscriptArr(COMPOSER_ARR));
          rghtPrps.setARRANGER_ARR(XssUtil.unscriptArr(ARRANGER_ARR));
          rghtPrps.setSINGER_ARR(XssUtil.unscriptArr(SINGER_ARR));
          rghtPrps.setPLAYER_ARR(XssUtil.unscriptArr(PLAYER_ARR));
          rghtPrps.setCONDUCTOR_ARR(XssUtil.unscriptArr(CONDUCTOR_ARR));
          rghtPrps.setPRODUCER_ARR(XssUtil.unscriptArr(PRODUCER_ARR));

          rghtPrps.setLYRICIST_ORGN_ARR(XssUtil.unscriptArr(LYRICIST_ORGN_ARR));
          rghtPrps.setCOMPOSER_ORGN_ARR(XssUtil.unscriptArr(COMPOSER_ORGN_ARR));
          rghtPrps.setARRANGER_ORGN_ARR(XssUtil.unscriptArr(ARRANGER_ORGN_ARR));
          rghtPrps.setSINGER_ORGN_ARR(XssUtil.unscriptArr(SINGER_ORGN_ARR));
          rghtPrps.setPLAYER_ORGN_ARR(XssUtil.unscriptArr(PLAYER_ORGN_ARR));
          rghtPrps.setCONDUCTOR_ORGN_ARR(XssUtil.unscriptArr(CONDUCTOR_ORGN_ARR));
          rghtPrps.setPRODUCER_ORGN_ARR(XssUtil.unscriptArr(PRODUCER_ORGN_ARR));

          rghtPrps.setPERF_TIME_ARR(PERF_TIME_ARR);
          // rghtPrps.setISSUED_DATE_ARR(ISSUED_DATE_ARR);
          // rghtPrps.setLYRICS_ARR(LYRICS_ARR);

          // 도서
          rghtPrps.setTITLE_ARR(TITLE_ARR);
          rghtPrps.setBOOK_TITLE_ARR(BOOK_TITLE_ARR);
          rghtPrps.setPUBLISHER_ARR(PUBLISHER_ARR);
          rghtPrps.setFIRST_EDITION_YEAR_ARR(FIRST_EDITION_YEAR_ARR);
          rghtPrps.setPUBLISH_TYPE_ARR(PUBLISH_TYPE_ARR);
          rghtPrps.setLICENSOR_NAME_KOR_ARR(XssUtil.unscriptArr(LICENSOR_NAME_KOR_ARR));
          rghtPrps.setLICENSOR_NAME_ORGN_KOR_ARR(XssUtil.unscriptArr(LICENSOR_NAME_KOR_ORGN_ARR));
          rghtPrps.setTRANSLATOR_ARR(XssUtil.unscriptArr(TRANSLATOR_ARR));
          rghtPrps.setTRANSLATOR_ORGN_ARR(XssUtil.unscriptArr(TRANSLATOR_ORGN_ARR));

          // 방송대본
          rghtPrps.setGENRE_KIND_ARR(GENRE_KIND_ARR);
          rghtPrps.setGENRE_KIND_NAME_ARR(GENRE_KIND_NAME_ARR);
          rghtPrps.setBROAD_MEDI_ARR(BROAD_MEDI_ARR);
          rghtPrps.setBROAD_MEDI_NAME_ARR(BROAD_MEDI_NAME_ARR);
          rghtPrps.setBROAD_STAT_ARR(BROAD_STAT_ARR);
          rghtPrps.setBROAD_STAT_NAME_ARR(BROAD_STAT_NAME_ARR);
          rghtPrps.setBROAD_ORD_ARR(BROAD_ORD_ARR);
          // rghtPrps.setBROAD_DATE_ARR(BROAD_DATE_ARR);
          rghtPrps.setDIRECT_ARR(DIRECT_ARR);
          rghtPrps.setMAKER_ARR(XssUtil.unscriptArr(MAKER_ARR));
          rghtPrps.setWRITER_ARR(XssUtil.unscriptArr(WRITER_ARR));
          rghtPrps.setWRITER_ORGN_ARR(XssUtil.unscriptArr(WRITER_ORGN_ARR));

          // 이미지
          rghtPrps.setWORK_NAME_ARR(XssUtil.unscriptArr(WORK_NAME_ARR));
          rghtPrps.setOPEN_MEDI_CODE_ARR(OPEN_MEDI_CODE_ARR);
          rghtPrps.setOPEN_MEDI_TEXT_ARR(XssUtil.unscriptArr(OPEN_MEDI_TEXT_ARR));
          // rghtPrps.setOPEN_DATE_ARR(OPEN_DATE_ARR);
          rghtPrps.setIMAGE_DIVS_ARR(IMAGE_DIVS_ARR);
          rghtPrps.setCOPT_HODR_ARR(XssUtil.unscriptArr(COPT_HODR_ARR));
          rghtPrps.setCOPT_HODR_ORGN_ARR(XssUtil.unscriptArr(COPT_HODR_ORGN_ARR));
          rghtPrps.setSUBJ_INMT_SEQN_ARR(SUBJ_INMT_SEQN_ARR);

          // 기타
          String PRPS_SIDE_GENRE = ServletRequestUtils.getStringParameter(multipartRequest, "PRPS_SIDE_GENRE");
          String PRPS_RGHT_DESC = ServletRequestUtils.getStringParameter(multipartRequest, "PRPS_RGHT_DESC");
          rghtPrps.setPRPS_SIDE_R_GENRE_ARR(PRPS_SIDE_R_GENRE_ARR);
          rghtPrps.setPRPS_SIDE_GENRE(PRPS_SIDE_GENRE);
          rghtPrps.setPRPS_RGHT_DESC(XssUtil.unscript(PRPS_RGHT_DESC));
          // String ETC_TRST_ORGN_CODE = ServletRequestUtils.getStringParameter(multipartRequest,
          // "ETC_TRST_ORGN_CODE");
          // rghtPrps.setETC_TRST_ORGN_CODE(ETC_TRST_ORGN_CODE);
          //
          // 영화 Set
          rghtPrps.setDIRECTOR_ARR(DIRECTOR_ARR);
          rghtPrps.setLEADING_ACTOR_ARR(LEADING_ACTOR_ARR);
          // rghtPrps.setPRODUCE_DATE_ARR(PRODUCE_DATE_ARR);
          rghtPrps.setMEDIA_CODE_VALUE_ARR(MEDIA_CODE_VALUE_ARR);
          rghtPrps.setPRODUCER_ARR(XssUtil.unscriptArr(PRODUCER_ARR));
          rghtPrps.setPRODUCER_ORGN_ARR(XssUtil.unscriptArr(PRODUCER_ORGN_ARR));
          rghtPrps.setINVESTOR_ARR(XssUtil.unscriptArr(INVESTOR_ARR));
          rghtPrps.setINVESTOR_ORGN_ARR(XssUtil.unscriptArr(INVESTOR_ORGN_ARR));
          rghtPrps.setDISTRIBUTOR_ARR(XssUtil.unscriptArr(DISTRIBUTOR_ARR));
          rghtPrps.setDISTRIBUTOR_ORGN_ARR(XssUtil.unscriptArr(DISTRIBUTOR_ORGN_ARR));

          // 방송 Set
          rghtPrps.setPROG_NAME_ARR(PROG_NAME_ARR);
          rghtPrps.setMEDI_CODE_ARR(MEDI_CODE_ARR);
          rghtPrps.setCHNL_CODE_ARR(CHNL_CODE_ARR);
          rghtPrps.setMEDI_CODE_NAME_ARR(MEDI_CODE_NAME_ARR);
          rghtPrps.setCHNL_CODE_NAME_ARR(CHNL_CODE_NAME_ARR);
          rghtPrps.setPROG_ORDSEQ_ARR(PROG_ORDSEQ_ARR);
          rghtPrps.setPROG_GRAD_ARR(PROG_GRAD_ARR);
          rghtPrps.setMAKER_ORGN_ARR(XssUtil.unscriptArr(MAKER_ORGN_ARR));

          rghtPrps.setPRPS_DESC(prpsDesc);

          String offxLineRecp = ServletRequestUtils.getStringParameter(multipartRequest, "OFFX_LINE_RECP");
          rghtPrps.setOFFX_LINE_RECP(offxLineRecp);


          // -----------------------달력이미지 추가 수정 Start-----------------------//
          // 음악 (발매일)
          if (rghtPrps.getPRPS_RGHT_CODE().equals("1") || rghtPrps.getPRPS_RGHT_CODE().equals("2")) {
               String ISSUED_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "ISSUED_DATE");
               String ISSUED_DATE_ARR2[] = new String[ISSUED_DATE_ARR.length];

               String issued = "";
               String seq = "";
               for (int f = 1; f <= ISSUED_DATE_ARR.length; f++) {
                    seq = ISSUED_DATE_ARR[f - 1];

                    issued = ServletRequestUtils.getStringParameter(multipartRequest, "ISSUED_DATE_" + seq);
                    ISSUED_DATE_ARR2[f - 1] = issued;
               }
               rghtPrps.setISSUED_DATE_ARR(ISSUED_DATE_ARR2);
          } else if (rghtPrps.getPRPS_RGHT_CODE().equals("3")) {
               String ISSUED_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "VAL_ISSUED_DATE");
               String ISSUED_DATE_ARR2[] = new String[ISSUED_DATE_ARR.length];

               String issued = "";
               String seq = "";
               for (int f = 1; f <= ISSUED_DATE_ARR.length; f++) {
                    seq = ISSUED_DATE_ARR[f - 1];

                    issued = ServletRequestUtils.getStringParameter(multipartRequest, "VAL_ISSUED_DATE_" + seq);
                    ISSUED_DATE_ARR2[f - 1] = issued;
               }
               rghtPrps.setISSUED_DATE_ARR(ISSUED_DATE_ARR2);
          }

          // 뉴스 (기사일자)
          if (rghtPrps.getPRPS_RGHT_CODE().equals("1") || rghtPrps.getPRPS_RGHT_CODE().equals("2")) {
               String ARTICL_PUBC_SDATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "ARTICL_PUBC_SDATE");
               String ARTICL_PUBC_SDATE_ARR2[] = new String[ARTICL_PUBC_SDATE_ARR.length];

               String open = "";
               String openSeq = "";
               for (int f = 1; f <= ARTICL_PUBC_SDATE_ARR.length; f++) {
                    openSeq = ARTICL_PUBC_SDATE_ARR[f - 1];

                    open = ServletRequestUtils.getStringParameter(multipartRequest, "ARTICL_PUBC_SDATE_" + openSeq);
                    ARTICL_PUBC_SDATE_ARR2[f - 1] = open;
               }
               rghtPrps.setARTICL_PUBC_SDATE_ARR(ARTICL_PUBC_SDATE_ARR2);

          } else if (rghtPrps.getPRPS_RGHT_CODE().equals("3")) {
               String ARTICL_PUBC_SDATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "VAL_ARTICL_PUBC_SDATE");
               String ARTICL_PUBC_SDATE_ARR2[] = new String[ARTICL_PUBC_SDATE_ARR.length];

               String open = "";
               String openSeq = "";
               for (int f = 1; f <= ARTICL_PUBC_SDATE_ARR.length; f++) {
                    openSeq = ARTICL_PUBC_SDATE_ARR[f - 1];

                    open = ServletRequestUtils.getStringParameter(multipartRequest, "VAL_ARTICL_PUBC_SDATE_" + openSeq);
                    ARTICL_PUBC_SDATE_ARR2[f - 1] = open;
               }
               rghtPrps.setARTICL_PUBC_SDATE_ARR(ARTICL_PUBC_SDATE_ARR2);
          }

          // 방송대본 (방송일시), 방송 (방송일자)
          if (rghtPrps.getPRPS_RGHT_CODE().equals("1") || rghtPrps.getPRPS_RGHT_CODE().equals("2")) {
               String BROAD_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "BROAD_DATE");
               String BROAD_DATE_ARR2[] = new String[BROAD_DATE_ARR.length];

               String broad = "";
               String broadSeq = "";
               for (int f = 1; f <= BROAD_DATE_ARR.length; f++) {
                    broadSeq = BROAD_DATE_ARR[f - 1];

                    broad = ServletRequestUtils.getStringParameter(multipartRequest, "BROAD_DATE_" + broadSeq);
                    BROAD_DATE_ARR2[f - 1] = broad;
               }
               rghtPrps.setBROAD_DATE_ARR(BROAD_DATE_ARR2);
          } else if (rghtPrps.getPRPS_RGHT_CODE().equals("3")) {
               String BROAD_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "VAL_BROAD_DATE");
               String BROAD_DATE_ARR2[] = new String[BROAD_DATE_ARR.length];

               String broad = "";
               String broadSeq = "";
               for (int f = 1; f <= BROAD_DATE_ARR.length; f++) {
                    broadSeq = BROAD_DATE_ARR[f - 1];

                    broad = ServletRequestUtils.getStringParameter(multipartRequest, "VAL_BROAD_DATE_" + broadSeq);
                    BROAD_DATE_ARR2[f - 1] = broad;
               }
               rghtPrps.setBROAD_DATE_ARR(BROAD_DATE_ARR2);
          }

          // 이미지 (공표일자)
          if (rghtPrps.getPRPS_RGHT_CODE().equals("1") || rghtPrps.getPRPS_RGHT_CODE().equals("2")) {
               String OPEN_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "OPEN_DATE");
               String OPEN_DATE_ARR2[] = new String[OPEN_DATE_ARR.length];

               String open = "";
               String openSeq = "";
               for (int f = 1; f <= OPEN_DATE_ARR.length; f++) {
                    openSeq = OPEN_DATE_ARR[f - 1];

                    open = ServletRequestUtils.getStringParameter(multipartRequest, "OPEN_DATE_" + openSeq);
                    OPEN_DATE_ARR2[f - 1] = open;
               }
               rghtPrps.setOPEN_DATE_ARR(OPEN_DATE_ARR2);
          } else if (rghtPrps.getPRPS_RGHT_CODE().equals("3")) {
               String OPEN_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "VAL_OPEN_DATE");
               String OPEN_DATE_ARR2[] = new String[OPEN_DATE_ARR.length];

               String open = "";
               String openSeq = "";
               for (int f = 1; f <= OPEN_DATE_ARR.length; f++) {
                    openSeq = OPEN_DATE_ARR[f - 1];

                    open = ServletRequestUtils.getStringParameter(multipartRequest, "VAL_OPEN_DATE_" + openSeq);
                    OPEN_DATE_ARR2[f - 1] = open;
               }
               rghtPrps.setOPEN_DATE_ARR(OPEN_DATE_ARR2);
          }

          // 영화 (제작일자)
          if (rghtPrps.getPRPS_RGHT_CODE().equals("1") || rghtPrps.getPRPS_RGHT_CODE().equals("2")) {
               String PRODUCE_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "PRODUCE_DATE");
               String PRODUCE_DATE_ARR2[] = new String[PRODUCE_DATE_ARR.length];

               String produce = "";
               String oproduceSeq = "";
               for (int f = 1; f <= PRODUCE_DATE_ARR.length; f++) {
                    oproduceSeq = PRODUCE_DATE_ARR[f - 1];

                    produce = ServletRequestUtils.getStringParameter(multipartRequest, "PRODUCE_DATE_" + oproduceSeq);
                    PRODUCE_DATE_ARR2[f - 1] = produce;
               }
               rghtPrps.setPRODUCE_DATE_ARR(PRODUCE_DATE_ARR2);
          } else if (rghtPrps.getPRPS_RGHT_CODE().equals("3")) {
               String PRODUCE_DATE_ARR[] = ServletRequestUtils.getStringParameters(multipartRequest, "VAL_PRODUCE_DATE");
               String PRODUCE_DATE_ARR2[] = new String[PRODUCE_DATE_ARR.length];

               String produce = "";
               String oproduceSeq = "";
               for (int f = 1; f <= PRODUCE_DATE_ARR.length; f++) {
                    oproduceSeq = PRODUCE_DATE_ARR[f - 1];

                    produce = ServletRequestUtils.getStringParameter(multipartRequest, "VAL_PRODUCE_DATE_" + oproduceSeq);
                    PRODUCE_DATE_ARR2[f - 1] = produce;
               }
               rghtPrps.setPRODUCE_DATE_ARR(PRODUCE_DATE_ARR2);
          }


          // 회원
          rghtPrps.setUSER_DIVS(ServletRequestUtils.getStringParameter(multipartRequest, "USER_DIVS"));
          rghtPrps.setUSER_NAME(ServletRequestUtils.getStringParameter(multipartRequest, "USER_NAME"));
          rghtPrps.setRESD_CORP_NUMB_VIEW(ServletRequestUtils.getStringParameter(multipartRequest, "RESD_CORP_NUMB_VIEW"));
          rghtPrps.setCORP_NUMB(ServletRequestUtils.getStringParameter(multipartRequest, "CORP_NUMB"));
          rghtPrps.setHOME_TELX_NUMB(ServletRequestUtils.getStringParameter(multipartRequest, "HOME_TELX_NUMB"));
          rghtPrps.setBUSI_TELX_NUMB(ServletRequestUtils.getStringParameter(multipartRequest, "BUSI_TELX_NUMB"));
          rghtPrps.setMOBL_PHON(ServletRequestUtils.getStringParameter(multipartRequest, "MOBL_PHON"));
          rghtPrps.setFAXX_NUMB(ServletRequestUtils.getStringParameter(multipartRequest, "FAXX_NUMB"));
          rghtPrps.setMAIL(XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "MAIL")));
          rghtPrps.setHOME_ADDR(XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "HOME_ADDR")));
          rghtPrps.setBUSI_ADDR(XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "BUSI_ADDR")));
          rghtPrps.setPRPS_DESC(XssUtil.unscript(ServletRequestUtils.getStringParameter(multipartRequest, "PRPS_DESC")));

          // ----첨부파일 관련정보 start
          String[] fileOrgnCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFileOrgnCode");
          String[] fileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnFileName");

          String[] getFileOrgnCode = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileOrgnCode");
          String[] getRealFileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetRealFileName");
          String[] getFilePath = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFilePath");
          String[] getFileName = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileName");
          String[] getFileSize = ServletRequestUtils.getStringParameters(multipartRequest, "hddnGetFileSize");
          // ----첨부파일 관련정보 end

          // ----첨부Upload start
          List fileList = new ArrayList();

          // File Upload
          for (int f = 0; f < getFileName.length; f++) {
               PrpsAttc prpsAttc = new PrpsAttc();

               prpsAttc.setREAL_FILE_NAME(getRealFileName[f]);
               prpsAttc.setFILE_PATH(getFilePath[f]);
               prpsAttc.setFILE_NAME(getFileName[f]);
               prpsAttc.setFILE_SIZE(getFileSize[f]);
               prpsAttc.setTRST_ORGN_CODE(getFileOrgnCode[f]);

               fileList.add(prpsAttc);
          }

          // ----첨부파일등록 start
          // File Upload
          Iterator fileNameIterator = multipartRequest.getFileNames();

          while (fileNameIterator.hasNext()) {
               MultipartFile multiFile = multipartRequest.getFile((String) fileNameIterator.next());
               if (multiFile.getSize() > 0) {
                    PrpsAttc prpsAttc = FileUploadUtil.uploadPrpsFiles(multiFile, realUploadPath);

                    for (int fm = 0; fm < fileName.length; fm++) {

                         if (fileName[fm].toUpperCase().equals(prpsAttc.getFILE_NAME().toUpperCase())) {
                              // if(fileName[fm].equals(prpsAttc.getFILE_NAME())){
                              prpsAttc.setFILE_NAME(fileName[fm]);
                              prpsAttc.setTRST_ORGN_CODE(fileOrgnCode[fm]);
                         }
                    }

                    fileList.add(prpsAttc);
               }
          }

          rghtPrps.setFileList(fileList);


          int trstCnt = rghtPrps.getTRST_ORGN_CODE_ARR().length;
          for (int k = 0; k < trstCnt; k++) {

               rghtPrps.setTRST_ORGN_CODE(rghtPrps.getTRST_ORGN_CODE_ARR()[k]);

               if (rghtPrps.getTRST_ORGN_CODE().equals("1")) {
                    rghtPrps.setCHK_1("1");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("201")) {
                    rghtPrps.setCHK_201("201");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("202")) {
                    rghtPrps.setCHK_202("202");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("203")) {
                    rghtPrps.setCHK_203("203");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("204")) {
                    rghtPrps.setCHK_204("204");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("205")) {
                    rghtPrps.setCHK_205("205");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("206")) {
                    rghtPrps.setCHK_206("206");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("211")) {
                    rghtPrps.setCHK_211("211");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("212")) {
                    rghtPrps.setCHK_212("212");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("213")) {
                    rghtPrps.setCHK_213("213");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("214")) {
                    rghtPrps.setCHK_214("214");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("215")) {
                    rghtPrps.setCHK_215("215");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("216")) {
                    rghtPrps.setCHK_216("216");
               }
          }

          ModelAndView mv = null;

          // DIV 값에 따라 저작물 종류 구분
          if (divs.equals("M"))
               mv = new ModelAndView("rghtPrps/rghtPrps_musc_detl", "DIVS", divs);
          else if (divs.equals("B"))
               mv = new ModelAndView("rghtPrps/rghtPrps_book_detl", "DIVS", divs);
          else if (divs.equals("N"))
               mv = new ModelAndView("rghtPrps/rghtPrps_news_detl", "DIVS", divs);
          else if (divs.equals("C"))
               mv = new ModelAndView("rghtPrps/rghtPrps_script_detl", "DIVS", divs);
          else if (divs.equals("I"))
               mv = new ModelAndView("rghtPrps/rghtPrps_image_detl", "DIVS", divs);
          else if (divs.equals("V"))
               mv = new ModelAndView("rghtPrps/rghtPrps_mvie_detl", "DIVS", divs);
          else if (divs.equals("R"))
               mv = new ModelAndView("rghtPrps/rghtPrps_broadcast_detl", "DIVS", divs);
          else if (divs.equals("X"))
               mv = new ModelAndView("rghtPrps/rghtPrps_etc_detl", "DIVS", divs);

          // mv = new ModelAndView("rghtPrps/rghtPrps_musc_detl", "DIVS", divs);
          // Map params = new HashMap();
          // params.put("userIdnt", ServletRequestUtils.getStringParameter(multipartRequest, "USER_IDNT"));

          CodeList codeList = new CodeList();
          List genreList = new ArrayList();
          List broadList = new ArrayList();
          if (divs.equals("C")) {
               codeList.setBigCode("13"); // 방송대본 장르
               genreList = codeListService.commonCodeList(codeList);
               mv.addObject("genreList", genreList);

               codeList.setBigCode("16"); // 방송사
               broadList = codeListService.commonCodeList(codeList);
               mv.addObject("broadList", broadList);
          }

          List mediList = null;
          List chnlList = null;
          if (divs.equals("R")) {
               codeList.setBigCode("15"); // 방송 매체
               mediList = codeListService.commonCodeList(codeList);
               mv.addObject("mediList", mediList);

               codeList.setBigCode("17"); // 채널
               chnlList = codeListService.commonCodeList(codeList);
               mv.addObject("chnlList", chnlList);
          }

          mv.addObject("userInfo", rghtPrps);
          mv.addObject("rghtPrps", rghtPrps);
          mv.addObject("fileList", fileList);
          mv.addObject("listDivs", ServletRequestUtils.getStringParameter(multipartRequest, "listDivs", "")); // 접근 목록 구분 : 권리미상에서 접근한 경우> noneRghtList

          return mv;
     }

     // 권리찾기신청화면 이동(이전화면)
     public ModelAndView rghtPrpsBack(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          // String divs = ServletRequestUtils.getStringParameter(request, "DIVS");
          // String ifrmInput = ServletRequestUtils.getStringParameter(request, "ifrmInput");

          RghtPrps srchRghtPrps = new RghtPrps();

          srchRghtPrps.setSrchTitle(ServletRequestUtils.getStringParameter(request, "srchTitle")); // 곡명
          srchRghtPrps.setSrchProducer(ServletRequestUtils.getStringParameter(request, "srchProducer")); // 제작사
          srchRghtPrps.setSrchAlbumTitle(ServletRequestUtils.getStringParameter(request, "srchAlbumTitle")); // 앨범명
          srchRghtPrps.setSrchSinger(ServletRequestUtils.getStringParameter(request, "srchSinger")); // 가수명
          srchRghtPrps.setSrchStartDate(ServletRequestUtils.getStringParameter(request, "srchStartDate")); // 발매일
          srchRghtPrps.setSrchEndDate(ServletRequestUtils.getStringParameter(request, "srchEndDate")); // 발매일
          srchRghtPrps.setSrchLicensor(ServletRequestUtils.getStringParameter(request, "srchLicensor")); // 저작자명

          // 도서
          srchRghtPrps.setSrchPublisher(ServletRequestUtils.getStringParameter(request, "srchPublisher")); // 출판사
          srchRghtPrps.setSrchBookTitle(ServletRequestUtils.getStringParameter(request, "srchBookTitle")); // 도서명
          srchRghtPrps.setSrchLicensorNm(ServletRequestUtils.getStringParameter(request, "srchLicensorNm")); // 저자명


          RghtPrps rghtPrps = new RghtPrps();

          bind(request, rghtPrps);

          String prpsDesc = ServletRequestUtils.getStringParameter(request, "PRPS_DESC");

          String divs = ServletRequestUtils.getStringParameter(request, "DIVS");

          String keyId[] = ServletRequestUtils.getStringParameters(request, "iChkVal");

          String PRPS_RGHT_CODE = ServletRequestUtils.getRequiredStringParameter(request, "PRPS_RGHT_CODE");
          String TRST_ORGN_CODE_ARR[] = ServletRequestUtils.getRequiredStringParameters(request, "TRST_ORGN_CODE");

          // 음악
          String MUSIC_TITLE_ARR[] = ServletRequestUtils.getStringParameters(request, "MUSIC_TITLE");
          String ALBUM_TITLE_ARR[] = ServletRequestUtils.getStringParameters(request, "ALBUM_TITLE");

          String LYRICIST_ARR[] = ServletRequestUtils.getStringParameters(request, "LYRICIST");
          String COMPOSER_ARR[] = ServletRequestUtils.getStringParameters(request, "COMPOSER");
          String ARRANGER_ARR[] = ServletRequestUtils.getStringParameters(request, "ARRANGER");

          String SINGER_ARR[] = ServletRequestUtils.getStringParameters(request, "SINGER");
          String PLAYER_ARR[] = ServletRequestUtils.getStringParameters(request, "PLAYER");
          String CONDUCTOR_ARR[] = ServletRequestUtils.getStringParameters(request, "CONDUCTOR");
          String PRODUCER_ARR[] = ServletRequestUtils.getStringParameters(request, "PRODUCER");

          String LYRICIST_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "LYRICIST_ORGN");
          String COMPOSER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "COMPOSER_ORGN");
          String ARRANGER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "ARRANGER_ORGN");

          String SINGER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "SINGER_ORGN");
          String PLAYER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "PLAYER_ORGN");
          String CONDUCTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "CONDUCTOR_ORGN");
          String PRODUCER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "PRODUCER_ORGN");

          String PERF_TIME_ARR[] = ServletRequestUtils.getStringParameters(request, "PERF_TIME");
          // String ISSUED_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "ISSUED_DATE");
          // String LYRICS_ARR[] = ServletRequestUtils.getStringParameters(request, "LYRICS");

          // 도서
          String TITLE_ARR[] = ServletRequestUtils.getStringParameters(request, "TITLE");
          String BOOK_TITLE_ARR[] = ServletRequestUtils.getStringParameters(request, "BOOK_TITLE");
          String PUBLISHER_ARR[] = ServletRequestUtils.getStringParameters(request, "PUBLISHER");
          String FIRST_EDITION_YEAR_ARR[] = ServletRequestUtils.getStringParameters(request, "FIRST_EDITION_YEAR");
          String PUBLISH_TYPE_ARR[] = ServletRequestUtils.getStringParameters(request, "PUBLISH_TYPE");

          String LICENSOR_NAME_KOR_ARR[] = ServletRequestUtils.getStringParameters(request, "LICENSOR_NAME_KOR");
          String LICENSOR_NAME_KOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "LICENSOR_NAME_KOR_ORGN");
          String TRANSLATOR_ARR[] = ServletRequestUtils.getStringParameters(request, "TRANSLATOR");
          String TRANSLATOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "TRANSLATOR_ORGN");

          // 방송대본
          String GENRE_KIND_ARR[] = ServletRequestUtils.getStringParameters(request, "GENRE_KIND");
          String GENRE_KIND_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "GENRE_KIND_NAME");
          String BROAD_MEDI_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_MEDI");
          String BROAD_MEDI_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_MEDI_NAME");
          String BROAD_STAT_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_STAT");
          String BROAD_STAT_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_STAT_NAME");
          String BROAD_ORD_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_ORD");
          // String BROAD_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_DATE");
          String DIRECT_ARR[] = ServletRequestUtils.getStringParameters(request, "DIRECT");
          String MAKER_ARR[] = ServletRequestUtils.getStringParameters(request, "MAKER");
          String WRITER_ARR[] = ServletRequestUtils.getStringParameters(request, "WRITER");
          String WRITER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "WRITER_ORGN");

          // 이미지
          String WORK_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "WORK_NAME");
          String IMAGE_DIVS_ARR[] = ServletRequestUtils.getStringParameters(request, "IMAGE_DIVS");
          String OPEN_MEDI_CODE_ARR[] = ServletRequestUtils.getStringParameters(request, "OPEN_MEDI_CODE");
          String OPEN_MEDI_TEXT_ARR[] = ServletRequestUtils.getStringParameters(request, "OPEN_MEDI_TEXT");
          // String OPEN_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "OPEN_DATE");
          String COPT_HODR_ARR[] = ServletRequestUtils.getStringParameters(request, "COPT_HODR");
          String COPT_HODR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "COPT_HODR_ORGN");
          String SUBJ_INMT_SEQN_ARR[] = ServletRequestUtils.getStringParameters(request, "SUBJ_INMT_SEQN");

          // 기타
          String PRPS_SIDE_R_GENRE_ARR[] = ServletRequestUtils.getStringParameters(request, "PRPS_SIDE_R_GENRE"); // 분야

          // 영화
          String LEADING_ACTOR_ARR[] = ServletRequestUtils.getStringParameters(request, "LEADING_ACTOR");
          // String PRODUCE_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "PRODUCE_DATE");
          String MEDIA_CODE_VALUE_ARR[] = ServletRequestUtils.getStringParameters(request, "MEDIA_CODE_VALUE");
          String DIRECTOR_ARR[] = ServletRequestUtils.getStringParameters(request, "DIRECTOR");
          String INVESTOR_ARR[] = ServletRequestUtils.getStringParameters(request, "INVESTOR");
          String DISTRIBUTOR_ARR[] = ServletRequestUtils.getStringParameters(request, "DISTRIBUTOR");
          String DISTRIBUTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "DISTRIBUTOR_ORGN");
          String INVESTOR_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "INVESTOR_ORGN");

          // 방송
          String PROG_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "PROG_NAME");
          String MEDI_CODE_ARR[] = ServletRequestUtils.getStringParameters(request, "MEDI_CODE");
          String CHNL_CODE_ARR[] = ServletRequestUtils.getStringParameters(request, "CHNL_CODE");
          String MEDI_CODE_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "MEDI_CODE_NAME");
          String CHNL_CODE_NAME_ARR[] = ServletRequestUtils.getStringParameters(request, "CHNL_CODE_NAME");
          String PROG_ORDSEQ_ARR[] = ServletRequestUtils.getStringParameters(request, "PROG_ORDSEQ");
          String PROG_GRAD_ARR[] = ServletRequestUtils.getStringParameters(request, "PROG_GRAD");
          String MAKER_ORGN_ARR[] = ServletRequestUtils.getStringParameters(request, "MAKER_ORGN");

          // 음원
          rghtPrps.setDivs(divs);
          rghtPrps.setKeyId(keyId);
          rghtPrps.setPRPS_RGHT_CODE(PRPS_RGHT_CODE);
          rghtPrps.setTRST_ORGN_CODE_ARR(TRST_ORGN_CODE_ARR);
          rghtPrps.setMUSIC_TITLE_ARR(MUSIC_TITLE_ARR);
          rghtPrps.setALBUM_TITLE_ARR(ALBUM_TITLE_ARR);

          rghtPrps.setLYRICIST_ARR(LYRICIST_ARR);
          rghtPrps.setCOMPOSER_ARR(COMPOSER_ARR);
          rghtPrps.setARRANGER_ARR(ARRANGER_ARR);
          rghtPrps.setSINGER_ARR(SINGER_ARR);
          rghtPrps.setPLAYER_ARR(PLAYER_ARR);
          rghtPrps.setCONDUCTOR_ARR(CONDUCTOR_ARR);
          rghtPrps.setPRODUCER_ARR(PRODUCER_ARR);

          rghtPrps.setLYRICIST_ORGN_ARR(LYRICIST_ORGN_ARR);
          rghtPrps.setCOMPOSER_ORGN_ARR(COMPOSER_ORGN_ARR);
          rghtPrps.setARRANGER_ORGN_ARR(ARRANGER_ORGN_ARR);
          rghtPrps.setSINGER_ORGN_ARR(SINGER_ORGN_ARR);
          rghtPrps.setPLAYER_ORGN_ARR(PLAYER_ORGN_ARR);
          rghtPrps.setCONDUCTOR_ORGN_ARR(CONDUCTOR_ORGN_ARR);
          rghtPrps.setPRODUCER_ORGN_ARR(PRODUCER_ORGN_ARR);

          rghtPrps.setPERF_TIME_ARR(PERF_TIME_ARR);
          // rghtPrps.setISSUED_DATE_ARR(ISSUED_DATE_ARR);
          // rghtPrps.setLYRICS_ARR(LYRICS_ARR);

          // 도서
          rghtPrps.setTITLE_ARR(TITLE_ARR);
          rghtPrps.setBOOK_TITLE_ARR(BOOK_TITLE_ARR);
          rghtPrps.setPUBLISHER_ARR(PUBLISHER_ARR);
          rghtPrps.setFIRST_EDITION_YEAR_ARR(FIRST_EDITION_YEAR_ARR);
          rghtPrps.setPUBLISH_TYPE_ARR(PUBLISH_TYPE_ARR);
          rghtPrps.setLICENSOR_NAME_KOR_ARR(LICENSOR_NAME_KOR_ARR);
          rghtPrps.setLICENSOR_NAME_ORGN_KOR_ARR(LICENSOR_NAME_KOR_ORGN_ARR);
          rghtPrps.setTRANSLATOR_ARR(TRANSLATOR_ARR);
          rghtPrps.setTRANSLATOR_ORGN_ARR(TRANSLATOR_ORGN_ARR);

          // 방송대본
          rghtPrps.setGENRE_KIND_ARR(GENRE_KIND_ARR);
          rghtPrps.setBROAD_MEDI_ARR(BROAD_MEDI_ARR);
          rghtPrps.setBROAD_STAT_ARR(BROAD_STAT_ARR);
          rghtPrps.setGENRE_KIND_NAME_ARR(GENRE_KIND_NAME_ARR);
          rghtPrps.setBROAD_MEDI_NAME_ARR(BROAD_MEDI_NAME_ARR);
          rghtPrps.setBROAD_STAT_NAME_ARR(BROAD_STAT_NAME_ARR);
          rghtPrps.setBROAD_ORD_ARR(BROAD_ORD_ARR);
          // rghtPrps.setBROAD_DATE_ARR(BROAD_DATE_ARR);
          rghtPrps.setDIRECT_ARR(DIRECT_ARR);
          rghtPrps.setMAKER_ARR(MAKER_ARR);
          rghtPrps.setWRITER_ARR(WRITER_ARR);
          rghtPrps.setWRITER_ORGN_ARR(WRITER_ORGN_ARR);

          // 이미지
          rghtPrps.setWORK_NAME_ARR(WORK_NAME_ARR);
          rghtPrps.setIMAGE_DIVS_ARR(IMAGE_DIVS_ARR);
          rghtPrps.setOPEN_MEDI_CODE_ARR(OPEN_MEDI_CODE_ARR);
          rghtPrps.setOPEN_MEDI_TEXT_ARR(OPEN_MEDI_TEXT_ARR);
          // rghtPrps.setOPEN_DATE_ARR(OPEN_DATE_ARR);
          rghtPrps.setCOPT_HODR_ARR(COPT_HODR_ARR);
          rghtPrps.setCOPT_HODR_ORGN_ARR(COPT_HODR_ORGN_ARR);
          rghtPrps.setSUBJ_INMT_SEQN_ARR(SUBJ_INMT_SEQN_ARR);

          // 기타
          String PRPS_SIDE_GENRE = ServletRequestUtils.getStringParameter(request, "PRPS_SIDE_GENRE");
          String PRPS_RGHT_DESC = ServletRequestUtils.getStringParameter(request, "PRPS_RGHT_DESC");
          rghtPrps.setPRPS_SIDE_R_GENRE_ARR(PRPS_SIDE_R_GENRE_ARR);
          rghtPrps.setPRPS_SIDE_GENRE(PRPS_SIDE_GENRE);
          rghtPrps.setPRPS_RGHT_DESC(PRPS_RGHT_DESC);

          // 영화 Set
          rghtPrps.setDIRECTOR_ARR(DIRECTOR_ARR);
          rghtPrps.setLEADING_ACTOR_ARR(LEADING_ACTOR_ARR);
          // rghtPrps.setPRODUCE_DATE_ARR(PRODUCE_DATE_ARR);
          rghtPrps.setMEDIA_CODE_VALUE_ARR(MEDIA_CODE_VALUE_ARR);
          rghtPrps.setPRODUCER_ARR(PRODUCER_ARR);
          rghtPrps.setPRODUCER_ORGN_ARR(PRODUCER_ORGN_ARR);
          rghtPrps.setINVESTOR_ARR(INVESTOR_ARR);
          rghtPrps.setINVESTOR_ORGN_ARR(INVESTOR_ORGN_ARR);
          rghtPrps.setDISTRIBUTOR_ARR(DISTRIBUTOR_ARR);
          rghtPrps.setDISTRIBUTOR_ORGN_ARR(DISTRIBUTOR_ORGN_ARR);

          // 방송 Set
          rghtPrps.setPROG_NAME_ARR(PROG_NAME_ARR);
          rghtPrps.setMEDI_CODE_ARR(MEDI_CODE_ARR);
          rghtPrps.setCHNL_CODE_ARR(CHNL_CODE_ARR);
          rghtPrps.setMEDI_CODE_NAME_ARR(MEDI_CODE_NAME_ARR);
          rghtPrps.setCHNL_CODE_NAME_ARR(CHNL_CODE_NAME_ARR);
          rghtPrps.setPROG_ORDSEQ_ARR(PROG_ORDSEQ_ARR);
          rghtPrps.setPROG_GRAD_ARR(PROG_GRAD_ARR);
          rghtPrps.setMAKER_ORGN_ARR(MAKER_ORGN_ARR);

          rghtPrps.setPRPS_DESC(prpsDesc);

          // 회원
          rghtPrps.setUSER_DIVS(ServletRequestUtils.getStringParameter(request, "USER_DIVS"));
          rghtPrps.setUSER_NAME(ServletRequestUtils.getStringParameter(request, "USER_NAME"));
          rghtPrps.setRESD_CORP_NUMB_VIEW(ServletRequestUtils.getStringParameter(request, "RESD_CORP_NUMB_VIEW"));
          rghtPrps.setCORP_NUMB(ServletRequestUtils.getStringParameter(request, "CORP_NUMB"));
          rghtPrps.setTELX_NUMB(ServletRequestUtils.getStringParameter(request, "HOME_TELX_NUMB"));
          rghtPrps.setBUSI_TELX_NUMB(ServletRequestUtils.getStringParameter(request, "BUSI_TELX_NUMB"));
          rghtPrps.setMOBL_PHON(ServletRequestUtils.getStringParameter(request, "MOBL_PHON"));
          rghtPrps.setFAXX_NUMB(ServletRequestUtils.getStringParameter(request, "FAXX_NUMB"));
          rghtPrps.setMAIL(ServletRequestUtils.getStringParameter(request, "MAIL"));
          rghtPrps.setHOME_ADDR(ServletRequestUtils.getStringParameter(request, "HOME_ADDR"));
          rghtPrps.setBUSI_ADDR(ServletRequestUtils.getStringParameter(request, "BUSI_ADDR"));
          rghtPrps.setPRPS_DESC(ServletRequestUtils.getStringParameter(request, "PRPS_DESC"));



          // -----------------------달력이미지 추가 수정 Start-----------------------//
          // 음악 발매일
          String ISSUED_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "ISSUED_DATE");
          String ISSUED_DATE_ARR2[] = new String[ISSUED_DATE_ARR.length];
          String issued = "";

          for (int f = 1; f <= ISSUED_DATE_ARR.length; f++) {

               issued = ServletRequestUtils.getStringParameter(request, "ISSUED_DATE_" + f);
               ISSUED_DATE_ARR2[f - 1] = issued;
          }
          rghtPrps.setISSUED_DATE_ARR(ISSUED_DATE_ARR2);

          // 음악 발매일
          String ARTICL_PUBC_SDATE_ARR[] = ServletRequestUtils.getStringParameters(request, "ARTICL_PUBC_SDATE");
          String ARTICL_PUBC_SDATE_ARR2[] = new String[ISSUED_DATE_ARR.length];
          String news = "";

          for (int f = 1; f <= ARTICL_PUBC_SDATE_ARR.length; f++) {

               news = ServletRequestUtils.getStringParameter(request, "ARTICL_PUBC_SDATE_" + f);
               ARTICL_PUBC_SDATE_ARR2[f - 1] = news;
          }
          rghtPrps.setARTICL_PUBC_SDATE_ARR(ARTICL_PUBC_SDATE_ARR2);


          // 방송대본 (방송일시)
          String BROAD_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "BROAD_DATE");
          String BROAD_DATE_ARR2[] = new String[BROAD_DATE_ARR.length];
          String broad = "";

          for (int f = 1; f <= BROAD_DATE_ARR.length; f++) {

               broad = ServletRequestUtils.getStringParameter(request, "BROAD_DATE_" + f);
               BROAD_DATE_ARR2[f - 1] = broad;
          }
          rghtPrps.setBROAD_DATE_ARR(BROAD_DATE_ARR2);

          // 이미지 (공표일자)
          String OPEN_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "OPEN_DATE");
          String OPEN_DATE_ARR2[] = new String[OPEN_DATE_ARR.length];

          String open = "";
          for (int f = 1; f <= OPEN_DATE_ARR.length; f++) {

               open = ServletRequestUtils.getStringParameter(request, "OPEN_DATE_" + f);
               OPEN_DATE_ARR2[f - 1] = open;
          }
          rghtPrps.setOPEN_DATE_ARR(OPEN_DATE_ARR2);

          // 영화 (제작일자)
          String PRODUCE_DATE_ARR[] = ServletRequestUtils.getStringParameters(request, "PRODUCE_DATE");
          String PRODUCE_DATE_ARR2[] = new String[PRODUCE_DATE_ARR.length];

          String produce = "";
          for (int f = 1; f <= PRODUCE_DATE_ARR.length; f++) {

               produce = ServletRequestUtils.getStringParameter(request, "PRODUCE_DATE_" + f);
               PRODUCE_DATE_ARR2[f - 1] = produce;
          }
          rghtPrps.setPRODUCE_DATE_ARR(PRODUCE_DATE_ARR2);


          rghtPrps.setDIVS("BACK");
          String offxLineRecp = ServletRequestUtils.getStringParameter(request, "OFFX_LINE_RECP");
          rghtPrps.setOFFX_LINE_RECP(offxLineRecp);

          int trstCnt = rghtPrps.getTRST_ORGN_CODE_ARR().length;
          for (int k = 0; k < trstCnt; k++) {

               rghtPrps.setTRST_ORGN_CODE(rghtPrps.getTRST_ORGN_CODE_ARR()[k]);

               if (rghtPrps.getTRST_ORGN_CODE().equals("1")) {
                    rghtPrps.setCHK_1("1");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("200")) {
                    rghtPrps.setCHK_200("200");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("201")) {
                    rghtPrps.setCHK_201("201");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("202")) {
                    rghtPrps.setCHK_202("202");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("203")) {
                    rghtPrps.setCHK_203("203");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("204")) {
                    rghtPrps.setCHK_204("204");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("205")) {
                    rghtPrps.setCHK_205("205");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("206")) {
                    rghtPrps.setCHK_206("206");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("211")) {
                    rghtPrps.setCHK_211("211");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("212")) {
                    rghtPrps.setCHK_212("212");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("213")) {
                    rghtPrps.setCHK_213("213");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("214")) {
                    rghtPrps.setCHK_214("214");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("215")) {
                    rghtPrps.setCHK_215("215");
               }
               if (rghtPrps.getTRST_ORGN_CODE().equals("216")) {
                    rghtPrps.setCHK_216("216");
               }
          }

          // ----첨부파일 관련정보 start
          String[] getFileOrgnCode = ServletRequestUtils.getStringParameters(request, "hddnGetFileOrgnCode");
          String[] getRealFileName = ServletRequestUtils.getStringParameters(request, "hddnGetRealFileName");
          String[] getFilePath = ServletRequestUtils.getStringParameters(request, "hddnGetFilePath");
          String[] getFileName = ServletRequestUtils.getStringParameters(request, "hddnGetFileName");
          String[] getFileSize = ServletRequestUtils.getStringParameters(request, "hddnGetFileSize");

          // ----첨부파일 관련정보 end

          ArrayList fileList = new ArrayList();

          for (int f = 0; f < getRealFileName.length; f++) {

               PrpsAttc prpsAttc = new PrpsAttc();

               prpsAttc.setREAL_FILE_NAME(getRealFileName[f]);
               prpsAttc.setFILE_PATH(getFilePath[f]);
               prpsAttc.setFILE_NAME(getFileName[f]);
               prpsAttc.setFILE_SIZE(getFileSize[f]);
               prpsAttc.setTRST_ORGN_CODE(getFileOrgnCode[f]);

               fileList.add(prpsAttc);
          }

          ModelAndView mv = null;

          // DIV 값에 따라 저작물 종류 구분
          if (divs.equals("M"))
               mv = new ModelAndView("rghtPrps/rghtPrps_musc", "DIVS", divs);
          else if (divs.equals("B"))
               mv = new ModelAndView("rghtPrps/rghtPrps_book", "DIVS", divs);
          else if (divs.equals("N"))
               mv = new ModelAndView("rghtPrps/rghtPrps_news", "DIVS", divs);
          else if (divs.equals("C"))
               mv = new ModelAndView("rghtPrps/rghtPrps_script", "DIVS", divs);
          else if (divs.equals("I"))
               mv = new ModelAndView("rghtPrps/rghtPrps_image", "DIVS", divs);
          else if (divs.equals("V"))
               mv = new ModelAndView("rghtPrps/rghtPrps_mvie", "DIVS", divs);
          else if (divs.equals("X"))
               mv = new ModelAndView("rghtPrps/rghtPrps_etc", "DIVS", divs);
          else if (divs.equals("R"))
               mv = new ModelAndView("rghtPrps/rghtPrps_broadcast", "DIVS", divs);

          CodeList codeList = new CodeList();
          List genreList = new ArrayList();
          List broadList = new ArrayList();
          if (divs.equals("C")) {
               codeList.setBigCode("13"); // 방송대본 장르
               genreList = codeListService.commonCodeList(codeList);
               mv.addObject("genreList", genreList);

               codeList.setBigCode("16"); // 방송사
               broadList = codeListService.commonCodeList(codeList);
               mv.addObject("broadList", broadList);
          }

          List mediList = null;
          List chnlList = null;
          if (divs.equals("R")) {
               codeList.setBigCode("15"); // 방송 매체
               mediList = codeListService.commonCodeList(codeList);
               mv.addObject("mediList", mediList);

               codeList.setBigCode("17"); // 채널
               chnlList = codeListService.commonCodeList(codeList);
               mv.addObject("chnlList", chnlList);
          }

          Map params = new HashMap();
          params.put("userIdnt", ServletRequestUtils.getStringParameter(request, "userIdnt"));


          int aa = rghtPrps.getPRPS_SIDE_R_GENRE_ARR().length;

          mv.addObject("FILE_INFO", ServletRequestUtils.getStringParameter(request, "FILE_INFO"));
          mv.addObject("prpsList", rghtPrps);
          mv.addObject("rghtPrps", rghtPrps);
          mv.addObject("back", rghtPrps);
          mv.addObject("length", aa);
          mv.addObject("userInfo", rghtPrps);
          mv.addObject("srchParam", srchRghtPrps);
          mv.addObject("fileList", fileList);
          mv.addObject("listDivs", ServletRequestUtils.getStringParameter(request, "listDivs", "")); // 접근 목록 구분 : 권리미상에서 접근한 경우> noneRghtList
          return mv;// rghtPrps_musc.jsp
     }

     // 권리찾기신청 확인화면
     public ModelAndView rghtPrpsHistList(HttpServletRequest request, HttpServletResponse respone) throws Exception {

          RghtPrps rghtPrps = new RghtPrps();

          bind(request, rghtPrps);

          List list = rghtPrpsService.rghtPrpsHistList(rghtPrps);

          ModelAndView mv = new ModelAndView("rghtPrps/rghtPrpsHist", "rghtPrpsHist", list);

          mv.addObject("totalrow", list.size());
          mv.addObject("DIVS", rghtPrps.getDIVS());

          return mv;
     }
}
