package kr.or.copyright.mls.user.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.user.model.ZipCodeRoad;

public interface UserDao {

	public Map findUserList(Map params);
	
	public List findPostNumb(Map params);
	
	/*20120620추가*/
	public List findPostRoadNumb(Map params);
	
	public Map findUserIdnt(Map params);
	
	public Map findClmsUserIdnt1(Map params);
	
	public Map findClmsUserIdnt2(Map params);
	
	public void insertUser(Map params);
	
	public Map selectUserInfo(Map params);
	
	public void updateUserInfo(Map params);
	/* 양재석 추가 start */
	public Map getInmtUserInfo(Map params);
	/* 양재석 추가 end */
	public Map userIdntPswdSrch(Map params);
	
	public Map userPswdSrch(Map params);
	
	public void detlUserInfo(Map params);
	
	public Map selectClmsUserInfo(Map params);
	
	public void insertClmsUserInfo(Map params);
	
	public void insertClmsUserInfo2(Map params);
	
	public void updateClmsUserInfo(Map params);
	
	public void updateUserPswd(Map params);
	
	public void updateRegiSsnNo(Map params);
	
	public List<ZipCodeRoad> getInitRoadCodeList();
	
	public List<ZipCodeRoad> getRoadCodeList(Map params);

	public void updateUserPswdInfo(Map params);

	public List findSchCommName( Map params );
	
	public int chkDupLoginId( Map params );
	
	public void insertAdmin( Map params );
	
	public int getNewTrstOrgnCd();
}
