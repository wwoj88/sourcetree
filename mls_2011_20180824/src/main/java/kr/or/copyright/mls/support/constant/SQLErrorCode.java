package kr.or.copyright.mls.support.constant;

public class SQLErrorCode {
  public static final int DUPLICATE =  1 ;
  public static final int RI_PARENT =  2291 ;
  public static final int RI_CHILD =  2292 ;
  public static final int UPDATE_FAIL =  -1 ;
  public static final int DELETE_FAIL =  -2 ;
}
