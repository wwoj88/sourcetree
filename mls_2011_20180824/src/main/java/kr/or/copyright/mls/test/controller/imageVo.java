package kr.or.copyright.mls.test.controller;


public class imageVo{
	private int works_id;
	private String image_desc;
	private String keyword;
	private String writer;
	private String producer;
	private String image_url;
	private String image_open_yn;
	private String rgst_idnt;
	private String rgst_dttn;
	private String modi_idnt;
	private String modi_dttm;
	
	public int getWorks_id(){
		return works_id;
	}
	
	public void setWorks_id( int works_id ){
		this.works_id = works_id;
	}
	
	public String getImage_desc(){
		return image_desc;
	}
	
	public void setImage_desc( String image_desc ){
		this.image_desc = image_desc;
	}
	
	public String getKeyword(){
		return keyword;
	}
	
	public void setKeyword( String keyword ){
		this.keyword = keyword;
	}
	
	public String getWriter(){
		return writer;
	}
	
	public void setWriter( String writer ){
		this.writer = writer;
	}
	
	public String getProducer(){
		return producer;
	}
	
	public void setProducer( String producer ){
		this.producer = producer;
	}
	
	public String getImage_url(){
		return image_url;
	}
	
	public void setImage_url( String image_url ){
		this.image_url = image_url;
	}
	
	public String getImage_open_yn(){
		return image_open_yn;
	}
	
	public void setImage_open_yn( String image_open_yn ){
		this.image_open_yn = image_open_yn;
	}
	
	public String getRgst_idnt(){
		return rgst_idnt;
	}
	
	public void setRgst_idnt( String rgst_idnt ){
		this.rgst_idnt = rgst_idnt;
	}
	
	public String getRgst_dttn(){
		return rgst_dttn;
	}
	
	public void setRgst_dttn( String rgst_dttn ){
		this.rgst_dttn = rgst_dttn;
	}
	
	public String getModi_idnt(){
		return modi_idnt;
	}
	
	public void setModi_idnt( String modi_idnt ){
		this.modi_idnt = modi_idnt;
	}
	
	public String getModi_dttm(){
		return modi_dttm;
	}
	
	public void setModi_dttm( String modi_dttm ){
		this.modi_dttm = modi_dttm;
	}

	@Override
	public String toString(){
		return "imageVo [works_id=" + works_id + ", image_desc=" + image_desc + ", keyword=" + keyword + ", writer="
			+ writer + ", producer=" + producer + ", image_url=" + image_url + ", image_open_yn=" + image_open_yn
			+ ", rgst_idnt=" + rgst_idnt + ", rgst_dttn=" + rgst_dttn + ", modi_idnt=" + modi_idnt + ", modi_dttm="
			+ modi_dttm + "]";
	}
	
	
}
