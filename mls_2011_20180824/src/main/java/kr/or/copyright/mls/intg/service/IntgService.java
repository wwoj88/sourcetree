package kr.or.copyright.mls.intg.service;

import java.util.List;

import kr.or.copyright.mls.intg.model.Intg;

public interface IntgService {
	
	// 이미지 CNT
	public String srchImgCnt(Intg IntgDTO);
	
	// 이미지
	public List srchImg(Intg IntgDTO);
	
	// 이미지 상세
	public List detailImg(Intg IntgDTO);
	
	
	// 방송음악 CNT
	public String srchBrctCnt(Intg IntgDTO);
	
	// 방송음악
	public List srchBrct(Intg IntgDTO);
	
	// 방송음악 상세
	public List detailBrct(Intg IntgDTO);
	
	
	// 교과용 CNT
	public String srchSubjCnt(Intg IntgDTO);
	
	// 교과용
	public List srchSubj(Intg IntgDTO);
	
	// 교과용 상세
	public List detailSubj(Intg IntgDTO);
	
	
	// 도서관 CNT
	public String srchLibrCnt(Intg IntgDTO);
	
	// 도서관
	public List srchLibr(Intg IntgDTO);
	
	//	도서관 상세
	public List detailLibr(Intg IntgDTO);
	
}
