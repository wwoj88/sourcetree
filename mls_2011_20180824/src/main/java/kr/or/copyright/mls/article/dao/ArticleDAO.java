package kr.or.copyright.mls.article.dao;

import java.util.List;

import kr.or.copyright.mls.article.model.Article;

public interface ArticleDAO {
    public int insertArticle(Article article);
	
    public void deleteArticle(int articleId);

    public List<Article> selectArticleList(int communityId);
    
    public Article selectArticleInfo(int articleId); 

}
