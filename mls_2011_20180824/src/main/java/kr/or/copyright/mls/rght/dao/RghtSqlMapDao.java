package kr.or.copyright.mls.rght.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
 
public class RghtSqlMapDao extends SqlMapClientDaoSupport implements RghtDao {
	
	// 내권리찾기 음악리스트 조회 
	public List muscRghtList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.muscRghtList",map);
	}	
	
	// 내권리찾기 음악리스트 조회 (count)
	public List muscRghtListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.muscRghtListCount",map);
	}
    
	// 내권리찾기(음악상세조회)	
	public List muscRghtDetail(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.muscRghtDetail",map);
	}	
	
	// 내권리찾기 어문리스트 조회 
	public List bookRghtList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.bookRghtList",map);
	}	
	// 내권리찾기 어문리스트 조회  count
	public List bookRghtListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.bookRghtListCount",map);
	}
	
	// 내권리찾기 어문상세 조회  count
	public List bookRghtDetail(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.bookRghtDetail",map);
	}
	
	
	// 내권리찾기 이미지리스트 조회 
	public List imgeRghtList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.imgeRghtList",map);
	}	
	
	// 내권리찾기 이미지리스트 조회  count	
	public List imgeRghtListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.imgeRghtListCount",map);
	}	

	public List coptRghtList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.coptRghtList",map);
	}	
	
	public List muscList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.muscList",map);
	}	

	public List muscListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.muscListCount",map);
	}	
	
	
	public List bookList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.bookList",map);
	}	
	
	public List bookListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.bookListCount",map);
	}		
	
	public List rghtTotalDealStat(Map map){
		return getSqlMapClientTemplate().queryForList("Rght.rghtTotalDealStat", map);
	}
	
	
	public List rghtTempList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.rghtTempList",map);
	}		
	
	public List rghtDetlList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.rghtDetlList",map);
	}		

	public int prpsMastKey() {
		Integer max = (Integer)getSqlMapClientTemplate().queryForObject("Rght.prpsMastKey",null);
		return max.intValue(); 
	}
	
	public void prpsMasterInsert(Map map) {
		getSqlMapClientTemplate().insert("Rght.prpsMasterInsert",map);
	}		
	
	public void prpsMasterUpdate(Map map) {
		getSqlMapClientTemplate().update("Rght.prpsMasterUpdate",map);
	}		
	

	public void prpsMasterDelete(Map map) {
		getSqlMapClientTemplate().delete("Rght.prpsMasterDelete",map);
	}	
	
	public void prpsDetailDelete(Map map) {
		getSqlMapClientTemplate().delete("Rght.prpsDetailDelete",map);
	}		
	
	
	public void prpsDetail201Insert(Map map) {
		getSqlMapClientTemplate().insert("Rght.prpsDetail201Insert",map);
	}	
	
	public void prpsDetail202Insert(Map map) {
		getSqlMapClientTemplate().insert("Rght.prpsDetail202Insert",map);
	}	
	
	public void prpsDetail203Insert(Map map) {
		getSqlMapClientTemplate().insert("Rght.prpsDetail203Insert",map);
	}
	
	public void prpsDetail204Insert(Map map) {
		getSqlMapClientTemplate().insert("Rght.prpsDetail204Insert",map);
	}
	
	public void prpsDetail205Insert(Map map) {
		getSqlMapClientTemplate().insert("Rght.prpsDetail205Insert",map);
	}	
	
	public List rghtDetlDescList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.rghtDetlDescList",map);
	}	
	
	public List rghtFileList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.rghtFileList", map);
	}
	
	public void rghtFileInsert(Map map) {
		getSqlMapClientTemplate().insert("Rght.rghtFileInsert", map);
	}
	
	public void rghtFileDelete(Map map) {
		getSqlMapClientTemplate().delete("Rght.rghtFileDelete", map);
	}
	
	public List getMaxDealStat(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.getMaxDealStat", map);
	}
	
//	 강명표 추가
	public void insertConnInfo(Map map) {
		getSqlMapClientTemplate().insert("Rght.insertConnInfo", map);
	}
}
