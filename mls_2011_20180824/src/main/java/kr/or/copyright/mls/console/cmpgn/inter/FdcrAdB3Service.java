package kr.or.copyright.mls.console.cmpgn.inter;

import java.util.Map;

public interface FdcrAdB3Service{

	/**
	 * 설문 참여 목록
	 */
	public void fdcrAdB3List1( Map<String, Object> commandMap ) throws Exception;

	/**
	 * 설문 참여 조회
	 */
	public Map fdcrAdB3View1( Map<String, Object> commandMap ) throws Exception;
}
