package kr.or.copyright.common.userLogin.service;




import kr.or.copyright.common.userLogin.PasswordMismatchException;
import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.model.User;

public interface UserLoginService {

	public User login(User user) throws UserNotFoundException, PasswordMismatchException;
	
	public User certLogin(User user) throws UserNotFoundException, PasswordMismatchException;
	
	public User loginYn(User user) throws UserNotFoundException, PasswordMismatchException;
	
	public User certLoginYn(User user) throws UserNotFoundException, PasswordMismatchException;
	
	public User certAdminLogin(User user) throws UserNotFoundException ;
	
	public void updateCertInfo(User user);

	public void failCountInit( User user );

	public User checkUser( User user ) throws Exception;

	public User transData( User user ) throws Exception;

	public void updateSso( User user )throws Exception;

	public User userlogin2( User user )throws UserNotFoundException, PasswordMismatchException;

	public void insertSsoHistory( User user ) throws Exception;

}
