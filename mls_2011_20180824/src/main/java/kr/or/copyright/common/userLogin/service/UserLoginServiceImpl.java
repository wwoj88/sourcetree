package kr.or.copyright.common.userLogin.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.ObjectUtils;

import com.softforum.xdbe.xCrypto;


import kr.or.copyright.common.userLogin.PasswordMismatchException;
import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.dao.UserLoginDao;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.common.common.service.BaseService;
import kr.or.copyright.mls.support.util.IPUtil;

public class UserLoginServiceImpl extends BaseService implements UserLoginService {

	//private Log logger = LogFactory.getLog(getClass());
	
	private UserLoginDao userLoginDao;
	
	public void setUserLoginDao(UserLoginDao userLoginDao) {
		this.userLoginDao = userLoginDao;
	}

	public Map findUser(Map params) throws UserNotFoundException{ 
		Map userList = new HashMap();

		if (params.get("ssnNo") != null || params.get("crnNo") != null) {
			userList = userLoginDao.certFindUser(params);	
		} else {
			userList = userLoginDao.findUser(params);			
		}
		
		if(userList == null){
			throw new UserNotFoundException("입력하신 정보에 해당되는 사용자가 없습니다.", null);
		}
		
		return userList;
	}
	
	public Map findUserPswd(Map params, User user) throws PasswordMismatchException, UserNotFoundException{ 
		
		
		Map userList = userLoginDao.findUserPswd(params);
		
		if(userList == null){
			userLoginDao.failCountUpdate(user);
			User returnUser = userLoginDao.failCountSelect(user);
			throw new PasswordMismatchException("입력하신 비밀번호가 잘못 됐습니다.", null);
		}
		
		return userList;
	}
	
	public User login(User user) throws UserNotFoundException, PasswordMismatchException {
		
		Map params = new HashMap();
		
		params.put("userIdnt", user.getUserIdnt());
		
		String pswd = user.getPswd();
		
		if(pswd.length() != 44){

			/*pwsd암호화 str 20121105 정병호*/
			String sOutput_H        = null;
			String sString = user.getPswd().toUpperCase();
			
			//TODO 암호화주석제거
			//sOutput_H	= xCrypto.Hash(6, sString);//단방향 암호화(sOutput)
		    sOutput_H ="qXBbaGoorPMSsaRoHOZgJr+JdetYu/uYROWMpIm85Ns=";//local
			params.put("pswd", sOutput_H);
			user.setPswd(sOutput_H);
			/*pwsd암호화 end 20121105 정병호*/
		}else{
			params.put("pswd", pswd);
			user.setPswd(pswd);
		}
		
		System.out.println( "parmas::"+params.get( "pswd" ) );
		System.out.println( "parmas::"+params.get( "userIdnt" ) );
		
		findUser(params);
		
		findUserPswd(params,user);
		//System.out.println(" user data : " + user );
		/*IPUtil ipUtil = new IPUtil();
		user.setConnectIp( ipUtil.ipCheck() );*/
		user.setActionProcess( "Login" );
		userLoginDao.userConnectLog(user);
		return userLoginDao.login(user);
    }
	
	public User checkUser(User user) throws Exception{
		
		Map params = new HashMap();
		String sOutput_H        = null;
		String sString = user.getPswd().toUpperCase();
		
		//TODO 암호화주석제거
		//user.setPswd( xCrypto.Hash(6, sString) );
		//sOutput_H	= xCrypto.Hash(6, sString);//단방향 암호화(sOutput)
	     //user.setPswd( "qXBbaGoorPMSsaRoHOZgJr+JdetYu/uYROWMpIm85Ns=" );	
	     sOutput_H ="qXBbaGoorPMSsaRoHOZgJr+JdetYu/uYROWMpIm85Ns=";//local
		//sOutput_H = "PY51dg24W+6KGjCSPrb5LoTD6TIFLnXr31Yi7I7DfuA=";
		user.setPswd( sOutput_H );
	//	params.put("userIdnt", user.getUserIdnt());
		//params.put("pswd", sOutput_H);
		/*params.put("pswd", sOutput_H);*/
		
	    user =userLoginDao.checkUser(user);
	    if(user==null) {
	    	 User user2 = new User();
	    	user2.setUserIdnt( "FAIL" );
			user2.setCeti_usex_ysno( "FAIL" );
			return user2;
	    }

	return user;
		
	}
	
	
	public User transData(User user) throws Exception{

		
	    user =userLoginDao.transData(user);
	    if(user==null) {
	    	 User user2 = new User();
	    	user2.setUserIdnt( "FAIL" );
			user2.setSso_loginyn( "FAIL" );
			return user2;
	    }

	return user;
		
	}
	
	public User certLogin(User user) throws UserNotFoundException, PasswordMismatchException {
		
		Map params = new HashMap();
		
		params.put("ssnNo", user.getSsnNo());
		params.put("crnNo", user.getCrnNo());
		
		findUser(params);
		
		return userLoginDao.certLogin(user);
    }
	
	public User loginYn(User user) throws UserNotFoundException, PasswordMismatchException {
		
		Map params = new HashMap();
		
		params.put("userIdnt", user.getUserIdnt());
		String pswd = user.getPswd();
		
		if(pswd.length() != 44){
			/*pwsd암호화 str 20121105 정병호*/
			String sOutput_H        = null;
			String sString = user.getPswd().toUpperCase();
	
			//sOutput_H	= xCrypto.Hash(6, sString);//단방향 암호화(sOutput)
			sOutput_H ="qXBbaGoorPMSsaRoHOZgJr+JdetYu/uYROWMpIm85Ns=";//local
			//sOutput_H = "PY51dg24W+6KGjCSPrb5LoTD6TIFLnXr31Yi7I7DfuA=";
			params.put("pswd", sOutput_H);
			/*System.out.println("###############################");
			System.out.println(sOutput_H);
			System.out.println("###############################");*/
			user.setPswd(sOutput_H);
			/*pwsd암호화 end 20121105 정병호*/
		}else {
			params.put("pswd", pswd);
			user.setPswd(pswd);

		}
		findUser(params);
		
		findUserPswd(params,user);
		
		return userLoginDao.loginYn(user);
    }
	
	public User certLoginYn(User user) throws UserNotFoundException, PasswordMismatchException {
		
		Map params = new HashMap();
		
		params.put("ssnNo", user.getSsnNo());
		params.put("crnNo", user.getCrnNo());
		
		findUser(params);
		
		return userLoginDao.certLoginYn(user);
    }
	
	
	public void updateSso(User user){
		try{
			userLoginDao.updateSso(user);
		}
		catch( Exception e ){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void insertSsoHistory(User user){
		try{
			userLoginDao.insertSsoHistory(user);
		}
		catch( Exception e ){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	/*관리자 공인인증서 로그인*/
	public User certAdminLogin(User user) throws UserNotFoundException {
		
		return userLoginDao.certAdminLogin(user);
    }
	
	public void updateCertInfo(User user){
		userLoginDao.updateCertInfoAll(user);
		userLoginDao.updateCertInfoOwn(user);
	}

	public void failCountInit( User user ){
		System.out.println( "failCountInit service" );
		userLoginDao.failCountInit(user);
	}

	public User userlogin2( User user ) throws UserNotFoundException,PasswordMismatchException{
		try{
			user = userLoginDao.userlogin2(user);
		}
		catch( Exception e ){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return user;
	}
	
}
