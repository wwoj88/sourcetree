package kr.or.copyright.common.userLogin.controller;


import java.net.URLEncoder;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.oauth.BandiSSOAgent;
import kr.or.copyright.common.userLogin.controller.BanSSO;
import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.support.util.SessionUtil;

public class TestSSOLogin {

	// agent 관련설정
  private static String ssoUri = "https://sso.copyright.or.kr/oauth2/token.do";	// 실서버 URL
	//private static String ssoUri = "https://devsso.copyright.or.kr/oauth2/token.do";	// 개발서버 URL
	private static String clientId = "fa48f99639b2418c956c1642d8e8e02e";
	private static String clientSecret = "8joqm4lgs11otg0ah8gz7nydp";
	private static String scope = "http://sso.copyright.or.kr";						// 고정
	
	// 1 : id/pw, 2 : hp/pw , 3 : seq/ci 
	private static String type = "1";

	public static  void testLog(HttpServletRequest request, User user) throws Exception {
		// SSO 로그인 연게 SSOAGENT 생성
	     
	     System.out.println("URI :: "+ssoUri);
	     System.out.println(user.toString());
	     try {
	          
		BandiSSOAgent agent = new BandiSSOAgent(ssoUri, clientId, clientSecret, scope);
		//BanSSO agent = new BanSSO(ssoUri, clientId, clientSecret, scope);
		// 사용자 접속아이디
		// agent.getLocalServerIp(request);
		//String client_ip = "127.0.0.1";
		String client_ip =  agent.getLocalServerIp(request);
		//System.out.println( user.getUserIdnt() );
		//System.out.println( user.getPswd() );
		//System.out.println( client_ip );

		
		String user_id = user.getUserIdnt() ;
		String user_pwd =  user.getPswd();
				
		// SSO accessToken함수 요청 및 accessToken 함수결과 참조 
		HashMap<String, String> login = agent.accessToken(user_id, user_pwd, client_ip, type);
		
		if(login.get("error").equals("0000") || login.get("error").equals("VL-3130")) {
		
			String access_token = login.get("access_token");
			String refresh_token = login.get("refresh_token");
			// 
			
			
			//System.out.println( "LOGIN SSO TOKEN :: "+access_token );
			// SSO 로그인 연계 "userInfo 함수 요청" 및  "userInfo 함수 결과" 참조
			
			HashMap<String, String> userInfo = agent.userInfo(access_token, client_ip, type);

			if(userInfo.get("error").equals("0000")) {
		
				//"userInfo 함수 결과"의  필드를 기준으로 맵핑
				Object[] keys = (Object[]) userInfo.keySet().toArray();
				for(int i=0;i<userInfo.size();i++) {
					System.out.println("["+keys[i]+"] "+userInfo.get(keys[i]));
					user.setUserIdnt( userInfo.get( "siteMembId" ) );
				}
				HttpSession session = request.getSession();
				session.setAttribute("access_token", access_token );
				session.setAttribute("refresh_token", refresh_token);
				// 세션에 access_token, refresh_token 저장
				// 통합회원 연동시 및 유효성 검증을 위해 필요
				
				// **중요(필요시)
				// 연동 완료시 저장한 개별사이트의 통함회원 아이디와 SSO로그인시 취득한 통합회원 아이디가 다른경우에는 개별사이트의 통합회원 아이디를 SSO로그인시 취득한 통합외원 아이디로 갱신 필요
				
			} else {
				// 사용자 정보 조회 시 오류
				
				System.out.println("[getUserInfo error] "+userInfo.get("error"));
				System.out.println("[getUserInfo error_message] "+userInfo.get("error_message"));
				throw new UserNotFoundException("입력하신 정보에 해당되는 사용자가 없습니다.", null);
			}
		} else {
			// 로그인 실패
			// 사용자 정보 조회 시 오류
			System.out.println("[login error] "+login.get("error"));
			System.out.println("[login error_message] "+login.get("error_message"));
			throw new UserNotFoundException("입력하신 정보에 해당되는 사용자가 없습니다.", null);
		} 
		}catch (Exception e){
         e.printStackTrace();
    }
	
	}
	
}
