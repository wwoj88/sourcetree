package kr.or.copyright.common.userLogin.controller;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bandi.oauth.BandiSSOAgent;
import com.tagfree.util.URLEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.or.copyright.mls.support.util.IPUtil;
import kr.or.copyright.mls.support.util.SessionUtil;
import kr.or.copyright.common.userLogin.PasswordMismatchException;
import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.common.userLogin.service.UserDormancyService;
import kr.or.copyright.common.userLogin.service.UserLoginService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import SafeSignOn.SSO;

public class LoginFormController extends SimpleFormController {

//	private final Log logger = LogFactory.getLog(getClass());
	
//	private final Log logger = LogFactory.getLog(getClass());
	private static final Log LOG = LogFactory.getLog(LoginFormController.class);
	
	private UserLoginService userLoginService;
	private UserDormancyService userDormancyService;
	
	public void setUserDormancyService( UserDormancyService userDormancyService ){
		this.userDormancyService = userDormancyService;
	}

	public void setUserLoginService(UserLoginService userLoginService) {
		this.userLoginService = userLoginService;
	}

	public LoginFormController() {

	}

	protected ModelAndView processFormSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException ex)
			throws Exception {
		
		return super.processFormSubmission(request, response, command, ex);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException ex)
			throws Exception {
		
		System.out.println("############################################");
		System.out.println("############# result_getFormView(): "+getFormView());
		System.out.println("############# request.getParameter('cert_reg'): "+request.getParameter("cert_reg"));
		System.out.println("############# request.getParameter('USER_IDNT'): "+request.getParameter("USER_IDNT"));
		System.out.println("############# request.getParameter('method'): "+request.getParameter("method"));
		System.out.println("############################################");
		
		if(request.getParameter("method") != null && request.getParameter("method").equals( "dormancyInit" )) {
			//System.out.println( "request.getParameter(\"method\")" + request.getParameter("method") );
			String userIdnt = request.getParameter("USER_IDNT");
			userDormancyService.dormancyInit(userIdnt);
			return new ModelAndView("/common/dormancyInitSucc");
		}
		
	      HttpServletRequest req = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
	      String ip = req.getHeader("X-FORWARDED-FOR");
	      if (ip == null)
	        ip = req.getRemoteAddr(); 
	      System.out.println("CONNECT IP : " + ip);

	      
		System.out.println( ("request.getParameter(\"login\")" + request.getParameter("login")));

		if(request.getParameter("login") != null && request.getParameter("login").length()>0){
//			Authenticate auth = (Authenticate)command;
			
			
			User user = new User();
			User userYn = new User();
			System.out.println("request.getParameter(\"loginDivs\") : " + request.getParameter("loginDivs"));
			if (request.getParameter("loginDivs") != null && "Y".equals(request.getParameter("loginDivs"))) {
				user.setSsnNo(request.getParameter("ssnNo"));						// 주민등록번호
				user.setCrnNo(request.getParameter("crnNo"));						// 사업자등록번호
/*				user.setSsnNo(request.getParameter("certId"));						// 인증서 추적을 위한 정보
				user.setSsnNo(request.getParameter("userSignCert"));				// 공인인증서 정보
				user.setSsnNo(request.getParameter("userSignValue"));				// 전자서명 값 
				user.setSsnNo(request.getParameter("encryptedSessionKey"));			// 비밀키 정보
				user.setSsnNo(request.getParameter("encryptedUserRandomNumber"));	// 인증서 소유자 확인을 위한 정보
				user.setSsnNo(request.getParameter("encryptedUserSSN"));			// 주민등록번호 암호화 정보
*/				
				try {
					LOG.debug("request.getParameter(\"crnNo\") : "+ request.getParameter("crnNo")+" , request.getParameter(\"ssnNo\") :"+request.getParameter("ssnNo"));
					if((!request.getParameter("crnNo").equals("") || !request.getParameter("ssnNo").equals("")) && !request.getParameter("encryptedSessionKey").equals("")){
						
						userYn = userLoginService.certLoginYn(user);
						if(userYn != null){
							if(userYn.getScssYsno().equals("Y")){
								request.setAttribute("errorMessage", "이미 탈퇴한 회원 입니다.");
								return new ModelAndView(getFormView());
							}
						}
					
						user = userLoginService.certLogin(user);
						//System.out.println( "user : " + user );
						SessionUtil.setUserSession(request, user);
						
						// SSO 적용(20110223)
						SessionUtil.setSSO(request, user);
						
						// 20091027 로그분석기적용에 대한 쿠키적용
						
						Cookie cookie = new Cookie("UID", user.getUserIdnt());
						cookie.setPath("/");
						response.addCookie(cookie);
						
						// 끝
					
						return new ModelAndView(getSuccessView());
					} else {
						return new ModelAndView(getFormView());
					}
				} catch(UserNotFoundException e){
					request.setAttribute("errorMessage", e.getMessage());
					return new ModelAndView(getFormView());
				}
				
			} 
			
			 
			
			else {
				try{
					user.setUserIdnt(ServletRequestUtils.getStringParameter(request, "userIdnt"));
					user.setPswd(ServletRequestUtils.getStringParameter(request, "pswd"));
					userYn.setUserIdnt(ServletRequestUtils.getStringParameter(request, "userIdnt"));
					userYn.setPswd(ServletRequestUtils.getStringParameter(request, "pswd"));
					
					String user_idnt = ServletRequestUtils.getStringParameter(request, "userIdnt");
					String user_pwd  = ServletRequestUtils.getStringParameter(request, "pswd");
					
					Map returnUser = userDormancyService.dormancyUserCheck(user);
					
					if(returnUser != null) {
						request.setAttribute( "returnUser", returnUser );
						return new ModelAndView("/common/dormancyInit");
					}
				
					user = userLoginService.login(user);
					//System.out.println( "user : " + user );
					userYn = userLoginService.loginYn(userYn);
					//System.out.println( "userYn : " + userYn );
					if(userYn != null){
						if(userYn.getScssYsno().equals("Y")){
							request.setAttribute("errorMessage", "이미 탈퇴한 회원 입니다.");
							return new ModelAndView(getFormView());
						}
					}
					if(user.getLoginFailCnt()==null){
						
						user.setLoginFailCnt("0");
						
					}
					if(Integer.parseInt( user.getLoginFailCnt() ) <= 4)	{
						
						//System.out.println( "SESSION SET" );
						
						SessionUtil.setUserSession(request, user);
						
						userLoginService.failCountInit(user);
					}else{
						
						request.setAttribute("errorMessage","비밀번호를 5회 이상 틀리셨습니다. 담당자에게 연락부탁드립니다.");
						
						return new ModelAndView(getFormView());
					}
					
					
					

					
					// 끝
					
					// 관리자 + 이용자 리턴화면 분기
					if(user.getMgnbYsno().equals("Y")){
						//임시 세션 생성
						SessionUtil.setUserSession_cert(request, user);
						//return new ModelAndView();
						//return new ModelAndView("redirect:/main/main.do");
						
						return new ModelAndView("redirect:/admin/main/index.jsp?initCode=1");
						
					} 
						
						//System.out.println( "YN : "+user.getSso_loginyn()  );
						
						//System.out.println( "USERDIV"+user.getUserDivs() );
						
						if((user.getSso_loginyn() == null || user.getSso_loginyn().equals( "N" ))) {
							if(user.getUserDivs().equals("01") || user.getUserDivs()=="01") {
      							SimpleDateFormat format1 = new SimpleDateFormat ("yyyyMMdd");
      
      							String format_time1 = format1.format (System.currentTimeMillis());
      							String home = URLEncoder.encode( "https://www.findcopyright.or.kr/","UTF-8" );
      							String rec = URLEncoder.encode( "https://www.findcopyright.or.kr/recTransUser.do","UTF-8" );
      							String sucreturn = URLEncoder.encode( "https://www.findcopyright.or.kr/sendTransUser.do?userIdnt="+user.getUserIdnt()+"&date="+format_time1+"2","UTF-8" );
      						
      							request.setAttribute("returnSiteCode", "SITE006");
      							request.setAttribute("returnSiteUrl", home);
      							request.setAttribute("returnResultUrl", rec);
      							request.setAttribute("transDataUrl",sucreturn);
      							
      							request.setAttribute("errorMessage","통합회원 시스템변경으로인해 통합회원전환을 해주시기 바랍니다.");
							
							     return new ModelAndView("/userLogin/transData");
							}else {
			              SimpleDateFormat format1 = new SimpleDateFormat ("yyyyMMdd");

			              String format_time1 = format1.format (System.currentTimeMillis());
			              String home = URLEncoder.encode( "https://www.findcopyright.or.kr/","UTF-8" );
			              String rec = URLEncoder.encode( "https://www.findcopyright.or.kr/recTransUser.do","UTF-8" );
			              String sucreturn = URLEncoder.encode( "https://www.findcopyright.or.kr/sendTransCorpUser.do?userIdnt="+user.getUserIdnt()+"&date="+format_time1+"2","UTF-8" );
			            
			              request.setAttribute("returnSiteCode", "SITE006");
			              request.setAttribute("returnSiteUrl", home);
			              request.setAttribute("returnResultUrl", rec);
			              request.setAttribute("transDataUrl",sucreturn);
			              
			              request.setAttribute("errorMessage","통합회원 시스템변경으로인해 통합회원전환을 해주시기 바랍니다.");
			              
			              return new ModelAndView("/userLogin/corpTransData");
							}
							
						}else if(user.getSso_loginyn().equals( "Y" )) {
							
							HttpSession session = request.getSession();
							session.removeAttribute("sessUserIdnt");
							session.removeAttribute("sessUserName");
							session.removeAttribute("sessSsoYn");
							session.removeAttribute("sessMail");
							session.removeAttribute("sessMoblPhon");
							session.removeAttribute("sessUserDivs");
							
							session.removeAttribute("sessClmsAgrYn");
							session.removeAttribute("sessClmsUserIdnt");
							session.removeAttribute("sessClmsPswd");
							session.removeAttribute("access_token" );
							session.removeAttribute("refresh_token");
							// 20091027 로그분석기적용에 대한 쿠키적용
							request.setAttribute("errorMessage","통합전환 된 아이디입니다. 통합로그인 아이디로 로그인해주세요.");
							return new ModelAndView("/userLogin/userLogn");
							
						}
							
					return new ModelAndView(getSuccessView());
				}catch(UserNotFoundException e){
					request.setAttribute("errorMessage", e.getMessage());
					return new ModelAndView(getFormView());
				}catch(PasswordMismatchException e){
					request.setAttribute("errorMessage", e.getMessage());
					return new ModelAndView(getFormView());				
				}
			}
			
		} 
		
		// 관리자 로그인 처리 추가 (20121127)
		else if (request.getParameter("adminLogin") != null && request.getParameter("adminLogin").length()>0) {
			
			String adminType = ServletRequestUtils.getStringParameter(request, "adminType",""); //1.adminCheck : 인증서 로그인, 2.adminCheck : 인증서 등록 시 임시로그인, 3.certRegi : 인증서등록, 4.idLogin : 담당자 로그인
			
			// 1.인증서 로그인
			if(adminType.equals("certLogin")){
				
				User user = new User();
				
				System.out.println("=====================================================");
				System.out.println("=====================================================");
				System.out.println(request.getParameter("certDN"));
				System.out.println("=====================================================");
				System.out.println("=====================================================");
				
				user.setCertDN(request.getParameter("certDN"));	
				
				// 1. DN 존재유무
				user = userLoginService.certAdminLogin(user);
				
					// 1.1 존재 (로그인 O)
					if(user != null){
						// 세션 추가.
						user = userLoginService.login(user);
						
						SessionUtil.setUserSession(request, user);
						
						return new ModelAndView("redirect:/admin/main/index.jsp");
					} 
					// 1.2 미존재 (로그인 X)
					else {
						return new ModelAndView("redirect:/admin/login/login.jsp?errorCode=001");
					}
						
			}
			// 2.인증서 등록시 - 관리자권한 확인
			else if(adminType.equals("adminCheck")){
				try{
					
					User user = new User();
					User userYn = new User();
					
					user.setUserIdnt(ServletRequestUtils.getStringParameter(request, "userIdnt"));
					user.setPswd(ServletRequestUtils.getStringParameter(request, "pswd"));
					
					userYn.setUserIdnt(ServletRequestUtils.getStringParameter(request, "userIdnt"));
					userYn.setPswd(ServletRequestUtils.getStringParameter(request, "pswd"));
					
					//System.out.println("userIdnt>>>>"+user.getUserIdnt());
					
					user = userLoginService.login(user);
					
					userYn = userLoginService.loginYn(userYn);
					
					if(userYn != null){
						if(userYn.getScssYsno().equals("Y")){
							request.setAttribute("errorMessage", "이미 탈퇴한 회원 입니다.");
							return new ModelAndView("redirect:/admin/login/login_reg.jsp?errorCode=201");
						}
					}
					
					
					// 관리자의 경우에만..
					if(user.getMgnbYsno().equals("Y") && user.getUserDivs().equals("200")) {
						
						//임시 세션 생성
						SessionUtil.setUserSession_cert(request, user);
						
						return new ModelAndView("redirect:/admin/login/login_reg_conf.jsp");
					
					}else {
						return new ModelAndView("redirect:/admin/login/login_reg.jsp?errorCode=204");
					}
						
					
					
				}catch(UserNotFoundException e){
					request.setAttribute("errorMessage", e.getMessage());
					return new ModelAndView("redirect:/admin/login/login_reg.jsp?errorCode=202");
				}catch(PasswordMismatchException e){
					request.setAttribute("errorMessage", e.getMessage());
					return new ModelAndView("redirect:/admin/login/login_reg.jsp?errorCode=203");			
				}
			}
			// 3. 공인인증서 등록
			else if(adminType.equals("certRegi")){
				
				System.out.println("=====================================================");
				System.out.println("=====================================================");
				System.out.println(request.getParameter("certDN"));
				System.out.println("=====================================================");
				System.out.println("=====================================================");
				
				User user = new User();
				user.setCertDN(request.getParameter("certDN"));	
				user.setUserIdnt(request.getParameter("userIdnt"));
				
				userLoginService.updateCertInfo(user);
				
				// 등록된 인증서가 없습니다. 메시지 처리
				return new ModelAndView("redirect:/admin/login/login.jsp?errorCode=002");
			}
			// 3. 담당자 로그인
			else if(adminType.equals("idLogin")){
				try{
					
					User user = new User();
					User userYn = new User();
					
					user.setUserIdnt(ServletRequestUtils.getStringParameter(request, "userIdnt"));
					user.setPswd(ServletRequestUtils.getStringParameter(request, "pswd"));
					
					userYn.setUserIdnt(ServletRequestUtils.getStringParameter(request, "userIdnt"));
					userYn.setPswd(ServletRequestUtils.getStringParameter(request, "pswd"));
					
					//System.out.println("userIdnt>>>>"+user.getUserIdnt());
					
					user = userLoginService.login(user);
					
					userYn = userLoginService.loginYn(userYn);
					
					if(userYn != null){
						if(userYn.getScssYsno().equals("Y")){
							request.setAttribute("errorMessage", "이미 탈퇴한 회원 입니다.");
							return new ModelAndView("redirect:/admin/login/login.jsp?errorCode=201");
						}
					}
					
					
					// 담당자의 경우에만..
					if(user.getMgnbYsno().equals("Y") && !user.getUserDivs().equals("200")) {
						
						SessionUtil.setUserSession(request, user);
						
						return new ModelAndView("redirect:/admin/main/index.jsp");
					
					}else {
						return new ModelAndView("redirect:/admin/login/login.jsp?errorCode=205");
					}
						
					
					
				}catch(UserNotFoundException e){
					request.setAttribute("errorMessage", e.getMessage());
					return new ModelAndView("redirect:/admin/login/login.jsp?errorCode=202");
				}catch(PasswordMismatchException e){
					request.setAttribute("errorMessage", e.getMessage());
					return new ModelAndView("redirect:/admin/login/login.jsp?errorCode=203");			
				}
			}
			
			return null;
			
		}
		else{
			HttpSession session = request.getSession();
			
			
			
			if (session.getAttribute("sessUserIdnt") != null) {
//				session.invalidate();
				System.out.println( "LOGOUT" );
				 	if(session.getAttribute( "sessSsoYn" ) !=null) {
				 		System.out.println( "LOGOUT SSO" );
				 		String ssoUri = "https://sso.copyright.or.kr/oauth2/token.do";	// 실서버URL
				 		//String ssoUri = "https://devsso.copyright.or.kr/oauth2/token.do";	// 개발서버 URL
						String clientId = "fa48f99639b2418c956c1642d8e8e02e";
						String clientSecret = "8joqm4lgs11otg0ah8gz7nydp";
						String scope = "http://sso.copyright.or.kr";						// 고정
						
						BandiSSOAgent agent = new BandiSSOAgent(ssoUri, clientId, clientSecret, scope);
						String client_ip =agent.getLocalServerIp( request );
						
						String access_token = (String) session.getAttribute( "access_token" );
						agent.logout(access_token, client_ip);
						session.removeAttribute("sessUserIdnt");
						session.removeAttribute("sessUserName");
						session.removeAttribute("sessSsoYn");
						session.removeAttribute("sessMail");
						session.removeAttribute("sessMoblPhon");
						session.removeAttribute("sessUserDivs");
						
						session.removeAttribute("sessClmsAgrYn");
						session.removeAttribute("sessClmsUserIdnt");
						session.removeAttribute("sessClmsPswd");
						session.removeAttribute("access_token" );
						session.removeAttribute("refresh_token");
				 	}else {
				session.removeAttribute("sessUserIdnt");
				session.removeAttribute("sessPswd");
				session.removeAttribute("sessUserName");
				session.removeAttribute("sessSsoYn");
				session.removeAttribute("sessMail");
				session.removeAttribute("sessMoblPhon");
				session.removeAttribute("sessUserDivs");
				
				session.removeAttribute("sessSsbNo");
				session.removeAttribute("access_token" );
				session.removeAttribute("refresh_token");
				session.removeAttribute("sessClmsAgrYn");
				session.removeAttribute("sessClmsUserIdnt");
				session.removeAttribute("sessClmsPswd");
				 	}
				// 20091027 로그분석기적용에 대한 쿠키적용
				 		
				Cookie killCookie = new Cookie("UID", null);
				killCookie.setPath("/");
				killCookie.setMaxAge(0);
						
				response.addCookie(killCookie);

				// 끝
				
				return new ModelAndView(getSuccessView());
			} else {
				return new ModelAndView("/userLogin/userLogn");
			}
		}
	}

	protected ModelAndView showForm(HttpServletRequest request,	HttpServletResponse response, Object command, BindException ex)	throws Exception {
		System.out.println( "LOG OUT" );
		
		if(request.getParameter("logout") != null){
			HttpSession session = request.getSession();
			if(session.getAttribute( "sessSsoYn" )!=null) {
				String ssoUri = "https://sso.copyright.or.kr/oauth2/token.do";	
				//String ssoUri = "https://devsso.copyright.or.kr/oauth2/token.do";  // 개발서버 URL
				String clientId = "fa48f99639b2418c956c1642d8e8e02e";
				String clientSecret = "8joqm4lgs11otg0ah8gz7nydp";
				String scope = "http://sso.copyright.or.kr";						// 고정
				
				BandiSSOAgent agent = new BandiSSOAgent(ssoUri, clientId, clientSecret, scope);
				String client_ip =agent.getLocalServerIp( request );
				
				String access_token = (String) session.getAttribute( "access_token" );
				agent.logout(access_token, client_ip);
				session.removeAttribute("sessUserIdnt");
				session.removeAttribute("sessUserName");
				session.removeAttribute("sessSsoYn");
				session.removeAttribute("sessMail");
				session.removeAttribute("sessMoblPhon");
				session.removeAttribute("sessUserDivs");
				
				session.removeAttribute("sessClmsAgrYn");
				session.removeAttribute("sessClmsUserIdnt");
				session.removeAttribute("sessClmsPswd");
				session.removeAttribute("access_token" );
				session.removeAttribute("refresh_token");
			}
//			session.removeAttribute("loginMember");
//			session.invalidate();
			session.removeAttribute("sessUserIdnt");
			session.removeAttribute("sessUserName");
			session.removeAttribute("sessSsoYn");
			session.removeAttribute("sessMail");
			session.removeAttribute("sessMoblPhon");
			session.removeAttribute("sessUserDivs");
			
			session.removeAttribute("sessClmsAgrYn");
			session.removeAttribute("sessClmsUserIdnt");
			session.removeAttribute("sessClmsPswd");
			session.removeAttribute("access_token" );
			session.removeAttribute("refresh_token");
			// 20091027 로그분석기적용에 대한 쿠키적용

			Cookie killCookie = new Cookie("UID", null);
			killCookie.setPath("/");
			killCookie.setMaxAge(0);
					
			response.addCookie(killCookie);

			// 끝
			
			return new ModelAndView(getSuccessView());
		}else{
			HttpSession session = request.getSession();
			
			if(session.getAttribute("loginMember") != null){
				
				return new ModelAndView("/logout");
			}
		}

		return super.showForm(request, response, ex);
	}

	
}
