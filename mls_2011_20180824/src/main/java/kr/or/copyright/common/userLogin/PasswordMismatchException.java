package kr.or.copyright.common.userLogin;

import org.springframework.core.ErrorCoded;

public class PasswordMismatchException extends Exception implements ErrorCoded {

	public PasswordMismatchException() {
		super();
	}

	public PasswordMismatchException(String message, Throwable cause) {
		super(message, cause);
	}

	public PasswordMismatchException(Throwable cause) {
		super(cause);
	}

	public String getErrorCode() {
		return "password.mismatch.exception";
	}

}
