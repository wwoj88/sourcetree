package kr.or.copyright.common.userLogin.service;

import java.util.List;
import java.util.Map;

import kr.or.copyright.common.userLogin.PasswordMismatchException;
import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.model.User;

public interface UserDormancyService {
	public List<Map> userDormancyCheck();

	public List<Map> userDormancyList();

	public void dormancyMove( List<Map> dormancyList );

	public Map dormancyUserCheck( User user );

	public void dormancyInit( String userIdnt );
}
