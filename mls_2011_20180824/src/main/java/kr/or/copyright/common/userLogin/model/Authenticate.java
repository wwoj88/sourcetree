package kr.or.copyright.common.userLogin.model;

import kr.or.copyright.mls.common.BaseObject;

public class Authenticate extends BaseObject {

	private String userIdnt;
	private String pswd;
	
	public String getUserIdnt() {
		return userIdnt;
	}
	public void setUserIdnt(String userIdnt) {
		this.userIdnt = userIdnt;
	}
	public String getPswd() {
		return pswd;
	}
	public void setPswd(String pswd) {
		this.pswd = pswd;
	}
}
