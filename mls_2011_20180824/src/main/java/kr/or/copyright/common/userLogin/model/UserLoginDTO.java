package kr.or.copyright.common.userLogin.model;

import java.lang.reflect.Field;

public class UserLoginDTO {

	public String toString(){
		Class Cls = this.getClass();
		Field[] fie = Cls.getDeclaredFields();      //PUBLIC인 변수를 받는다.

		StringBuffer sb = new StringBuffer();
		sb.append(" @@@ "); sb.append(Cls.getName()); sb.append(" info @@@ ");

		try {
			for( int j = 0; j < fie.length; j++ ){
				/* 특정 필드의 접근제어자가 private 나 protected 로 접근이 불가능 하면
				* 임시로 접근 가능하게 수정 */
				if( !fie[j].isAccessible() ) {
					fie[j].setAccessible( true );
				}
	
				String key = fie[j].getName();      //변수명
				//String value =  (String)Cls.getField(key).get(this);
				Object value = fie[j].get(this);
	
				if( j!=0 ){
					sb.append(" , "); 
				}
				sb.append(key); sb.append(" = "); sb.append(value);
			}
		}catch (Exception e) {
			System.out.println(e);
		}
		return sb.toString();
	}

	private String userIdnt;
	private String pswd;
	private String userName;
	private String moblPhon;
	private String userDivs;
	private String mgnbYsno;
	private String mail;
	private String userAddr;					// 엄동규 추가 유저 주소
	
	private String CertLoginCd;					// 인증서 로그인 구분(crn:사업자, ssn:개인)
	private String SsnNo;						// 전자서명용 주민등록번호
	private String CrnNo;						// 전자서명용 사업자등록번호
	private String CertId;						// 전자서명용 CertID
	private String UserSignCert;				// 공인인증서 정보
	private String UserSignValue;				// 전자서명 값
	private String EncryptedSessionKey;			// 비밀키를 암호화한 정보
	private String EncryptedUserRandomNumber;	// 공인인증서 확인을 위한 정보
	private String EncryptedUserSSN;			// 공인인증서를 통하여 암호화 된 주민등록번호

	
	public String getUserAddr() {
		return userAddr;
	}
	public void setUserAddr(String userAddr) {
		this.userAddr = userAddr;
	}
	public String getCertId() {
		return CertId;
	}
	public void setCertId(String certId) {
		CertId = certId;
	}
	public String getCertLoginCd() {
		return CertLoginCd;
	}
	public void setCertLoginCd(String certLoginCd) {
		CertLoginCd = certLoginCd;
	}
	public String getCrnNo() {
		return CrnNo;
	}
	public void setCrnNo(String crnNo) {
		CrnNo = crnNo;
	}
	public String getEncryptedSessionKey() {
		return EncryptedSessionKey;
	}
	public void setEncryptedSessionKey(String encryptedSessionKey) {
		EncryptedSessionKey = encryptedSessionKey;
	}
	public String getEncryptedUserRandomNumber() {
		return EncryptedUserRandomNumber;
	}
	public void setEncryptedUserRandomNumber(String encryptedUserRandomNumber) {
		EncryptedUserRandomNumber = encryptedUserRandomNumber;
	}
	public String getEncryptedUserSSN() {
		return EncryptedUserSSN;
	}
	public void setEncryptedUserSSN(String encryptedUserSSN) {
		EncryptedUserSSN = encryptedUserSSN;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getMgnbYsno() {
		return mgnbYsno;
	}
	public void setMgnbYsno(String mgnbYsno) {
		this.mgnbYsno = mgnbYsno;
	}
	public String getMoblPhon() {
		return moblPhon;
	}
	public void setMoblPhon(String moblPhon) {
		this.moblPhon = moblPhon;
	}
	public String getPswd() {
		return pswd;
	}
	public void setPswd(String pswd) {
		this.pswd = pswd;
	}
	public String getSsnNo() {
		return SsnNo;
	}
	public void setSsnNo(String ssnNo) {
		SsnNo = ssnNo;
	}
	public String getUserDivs() {
		return userDivs;
	}
	public void setUserDivs(String userDivs) {
		this.userDivs = userDivs;
	}
	public String getUserIdnt() {
		return userIdnt;
	}
	public void setUserIdnt(String userIdnt) {
		this.userIdnt = userIdnt;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserSignCert() {
		return UserSignCert;
	}
	public void setUserSignCert(String userSignCert) {
		UserSignCert = userSignCert;
	}
	public String getUserSignValue() {
		return UserSignValue;
	}
	public void setUserSignValue(String userSignValue) {
		UserSignValue = userSignValue;
	}


}
