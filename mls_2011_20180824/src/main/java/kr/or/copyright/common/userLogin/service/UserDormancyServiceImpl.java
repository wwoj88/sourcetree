package kr.or.copyright.common.userLogin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.softforum.xdbe.xCrypto;

import kr.or.copyright.common.userLogin.PasswordMismatchException;
import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.dao.UserDormancyDao;
import kr.or.copyright.common.userLogin.dao.UserLoginDao;
import kr.or.copyright.common.userLogin.model.User;
import kr.or.copyright.mls.common.common.service.BaseService;

public class UserDormancyServiceImpl extends BaseService implements UserDormancyService {

	//private Log logger = LogFactory.getLog(getClass());
	
	private UserDormancyDao userDormancyDao;
	
	public void setUserDormancyDao( UserDormancyDao userDormancyDao ){
		this.userDormancyDao = userDormancyDao;
	}

	public List<Map> userDormancyCheck(){
		return userDormancyDao.UserDormancyCheck();
	}

	public List<Map> userDormancyList(){
		return userDormancyDao.userDormancyList();
	}

	public void dormancyMove( List<Map> dormancyList ){
		
		for(int i = 0 ; i < dormancyList.size() ; i++)
		{
			userDormancyDao.dormancyMove(dormancyList.get( i ));
		}
	}

	public Map dormancyUserCheck( User user ){
		return userDormancyDao.dormancyUserCheck(user);
	}

	public void dormancyInit( String userIdnt ){
		userDormancyDao.dormancyInit(userIdnt);
	}
}
