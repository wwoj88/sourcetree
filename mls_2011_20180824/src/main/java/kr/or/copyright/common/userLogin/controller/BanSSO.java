package kr.or.copyright.common.userLogin.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;



public class BanSSO {


     private String SSO_URL = "";
     private String CLIENT_ID = "";
     private String CLIENT_SECRET = "";
     private String SCOPE = "";

     public BanSSO(String ssoUri, String clientId, String clientSecret, String scope) {

          this.SSO_URL = ssoUri;
          this.CLIENT_ID = clientId;
          this.CLIENT_SECRET = clientSecret;
          this.SCOPE = scope;
     }

     public HashMap<String, String> accessToken(String user_id, String user_pwd, String client_ip, String type) throws Exception {

          HashMap<String, String> result = new HashMap<String, String>();

          user_id = URLEncoder.encode(user_id);
          user_pwd = URLEncoder.encode(user_pwd);

          StringBuffer sb = new StringBuffer();
          sb.append("grant_type=").append("owner_password").append("&");
          sb.append("client_id=").append(this.CLIENT_ID).append("&");
          sb.append("client_secret=").append(this.CLIENT_SECRET).append("&");
          sb.append("user_id=").append(user_id).append("&");
          sb.append("user_pwd=").append(user_pwd).append("&");
          sb.append("scope=").append(this.SCOPE).append("&");
          sb.append("client_ip=").append(client_ip).append("&");
          sb.append("type=").append(type);
          System.out.println(sb.toString());
          String responseStr = send(sb.toString());

          return parsingData(responseStr);
     }



     public HashMap<String, String> tokenValid(String access_token, String client_ip) throws Exception {

          HashMap<String, String> result = new HashMap<String, String>();

          StringBuffer sb = new StringBuffer();
          sb.append("grant_type=").append("token_valid").append("&");
          sb.append("client_id=").append(this.CLIENT_ID).append("&");
          sb.append("client_secret=").append(this.CLIENT_SECRET).append("&");
          sb.append("access_token=").append(access_token).append("&");
          sb.append("scope=").append(this.SCOPE).append("&");
          sb.append("client_ip=").append(client_ip);

          String responseStr = send(sb.toString());

          return parsingData(responseStr);
     }



     public HashMap<String, String> refreshToken(String refresh_token, String client_ip) throws Exception {

          HashMap<String, String> result = new HashMap<String, String>();

          StringBuffer sb = new StringBuffer();
          sb.append("grant_type=").append("refresh_token").append("&");
          sb.append("client_id=").append(this.CLIENT_ID).append("&");
          sb.append("client_secret=").append(this.CLIENT_SECRET).append("&");
          sb.append("refresh_token=").append(refresh_token).append("&");
          sb.append("scope=").append(this.SCOPE).append("&");
          sb.append("client_ip=").append(client_ip);

          String responseStr = send(sb.toString());

          return parsingData(responseStr);
     }



     public HashMap<String, String> userInfo(String access_token, String client_ip, String type) throws Exception {

          HashMap<String, String> result = new HashMap<String, String>();

          StringBuffer sb = new StringBuffer();
          sb.append("grant_type=").append("access_token_identify").append("&");
          sb.append("client_id=").append(this.CLIENT_ID).append("&");
          sb.append("client_secret=").append(this.CLIENT_SECRET).append("&");
          sb.append("access_token=").append(access_token).append("&");
          sb.append("scope=").append(this.SCOPE).append("&");
          sb.append("client_ip=").append(client_ip).append("&");
          sb.append("type=").append(type);

          Thread.sleep(40L);

          String responseStr = send(sb.toString());

          return parsingData(responseStr);
     }



     public HashMap<String, String> logout(String access_token, String client_ip) throws Exception {

          HashMap<String, String> result = new HashMap<String, String>();

          StringBuffer sb = new StringBuffer();
          sb.append("grant_type=").append("logout").append("&");
          sb.append("client_id=").append(this.CLIENT_ID).append("&");
          sb.append("client_secret=").append(this.CLIENT_SECRET).append("&");
          sb.append("access_token=").append(access_token).append("&");
          sb.append("scope=").append(this.SCOPE).append("&");
          sb.append("client_ip=").append(client_ip);

          String responseStr = send(sb.toString());

          return parsingData(responseStr);
     }



     public HashMap<String, String> getNewSession(String access_token, String client_ip) throws Exception {

          HashMap<String, String> result = new HashMap<String, String>();

          StringBuffer sb = new StringBuffer();
          sb.append("grant_type=").append("access_token_new").append("&");
          sb.append("client_id=").append(this.CLIENT_ID).append("&");
          sb.append("client_secret=").append(this.CLIENT_SECRET).append("&");
          sb.append("access_token=").append(access_token).append("&");
          sb.append("scope=").append(this.SCOPE).append("&");
          sb.append("client_ip=").append(client_ip);

          String responseStr = send(sb.toString());

          return parsingData(responseStr);
     }



     public HashMap<String, String> parsingData(String jsonStr) {

          HashMap<String, String> result = new HashMap<String, String>();

          try {
               JSONParser parser = new JSONParser();
               JSONObject object = (JSONObject) parser.parse(jsonStr);

               for (Object key : object.keySet()) {
                    result.put(key.toString(), object.get(key).toString());
               }
          } catch (Exception e) {
               e.printStackTrace();
               result = null;
          }

          return result;
     }

     public String send(String jsonData) throws Exception {



          TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {

               public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                    return null;
               }

               public void checkClientTrusted(X509Certificate[] certs, String authType) {}

               public void checkServerTrusted(X509Certificate[] certs, String authType) {}
          }};

          // Install the all-trusting trust manager
          SSLContext sc = SSLContext.getInstance("SSL");
          sc.init(null, trustAllCerts, new java.security.SecureRandom());
          HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

          // Create all-trusting host name verifier
          HostnameVerifier allHostsValid = new HostnameVerifier() {

               public boolean verify(String hostname, SSLSession session) {

                    return true;
               }
          };
          HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

          String result = null;
          System.out.println("[SSO Agent SEND Parameters] " + jsonData);

          try {
               URL url = new URL(this.SSO_URL);
               if (url.getProtocol().equals("http")) {
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setUseCaches(false);

                    PrintWriter out = new PrintWriter(connection.getOutputStream());

                    out.print(jsonData);
                    out.flush();
                    out.close();

                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
                    String inputLine = "";

                    while ((inputLine = in.readLine()) != null) {
                         result = inputLine;
                    }

                    in.close();
               } else {



                    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setUseCaches(false);

                    PrintWriter out = new PrintWriter(connection.getOutputStream());

                    out.print(jsonData);
                    out.flush();
                    out.close();

                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
                    String inputLine = "";

                    while ((inputLine = in.readLine()) != null) {
                         result = inputLine;
                    }

                    in.close();
               }
          } catch (Exception e) {
               e.printStackTrace();
               JSONObject object = new JSONObject();
               object.put("error", "9999");
               object.put("error_message", "SSO ");
               result = object.toJSONString();
          }

          return result;
     }


     public String getLocalServerIp(HttpServletRequest req) {

          String clientIp = req.getHeader("X-Forwarded-For");

          if (isEmpty(clientIp)) {
               clientIp = req.getHeader("Proxy-Client-IP");
          }

          if (isEmpty(clientIp)) {
               clientIp = req.getHeader("WL-Proxy-Client-IP");
          }

          if (isEmpty(clientIp)) {
               clientIp = req.getHeader("HTTP_CLIENT_IP");
          }

          if (isEmpty(clientIp)) {
               clientIp = req.getHeader("HTTP_X_FORWARDED_FOR");
          }

          if (isEmpty(clientIp)) {
               clientIp = req.getHeader("X-Real-IP");
          }

          if (isEmpty(clientIp)) {
               clientIp = req.getHeader("X-RealIP");
          }

          if (isEmpty(clientIp)) {
               clientIp = req.getHeader("REMOTE_ADDR");
          }

          if (isEmpty(clientIp)) {
               clientIp = req.getRemoteAddr();
          }

          return clientIp;
     }



     public static boolean isEmpty(String source) {

          if ("".equals(source) || source == null || "null".equals(source)) {
               return true;
          }
          return false;
     }



}
