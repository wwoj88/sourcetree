package kr.or.copyright.common.userLogin.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.model.User;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class UserLoginSqlMapDao extends SqlMapClientDaoSupport implements UserLoginDao {

	//private Log logger = LogFactory.getLog(getClass());
	
	public User login(User user) {
		return (User) getSqlMapClientTemplate().queryForObject("UserLogin.userLogin", user);
	}
	
	public User certLogin(User user) {
		return (User) getSqlMapClientTemplate().queryForObject("UserLogin.certUserLogin", user);
	}
	
	public Map findUser(Map params) {

		return (Map) getSqlMapClientTemplate().queryForObject("UserLogin.getUser", params);
	}
	
	public Map certFindUser(Map params) {

		return (Map) getSqlMapClientTemplate().queryForObject("UserLogin.certFindUser", params);
	}
	
	public Map findUserPswd(Map params) {

		return (Map) getSqlMapClientTemplate().queryForObject("UserLogin.getUserPswd", params);
	}
	
	public User loginYn(User user) {
		return (User) getSqlMapClientTemplate().queryForObject("UserLogin.userLoginYn", user);
	}
	
	public User certLoginYn(User user) {
		return (User) getSqlMapClientTemplate().queryForObject("UserLogin.certUserLoginYn", user);
	}
	
	public User certAdminLogin(User user) {
		return (User) getSqlMapClientTemplate().queryForObject("UserLogin.certAdminLogin", user);
	}
	
	public void updateCertInfoAll(User user) {
		getSqlMapClientTemplate().update("UserLogin.updateCertInfoAll", user);
	}
	
	public void updateCertInfoOwn(User user){
		getSqlMapClientTemplate().update("UserLogin.updateCertInfoOwn", user);
	}

	public void failCountUpdate( User user ){
		getSqlMapClientTemplate().update("UserLogin.failCountUpdate", user);
	}

	public User failCountSelect( User user ){
		return (User) getSqlMapClientTemplate().queryForObject("UserLogin.failCountSelect", user);
	}

	public void userConnectLog( User user ){
		getSqlMapClientTemplate().insert( "UserLogin.userConnectLog", user);
	}

	public void failCountInit( User user ){
		getSqlMapClientTemplate().update("UserLogin.failCountInit", user);
	}
	
	public User checkUser(User user) {
		return (User) getSqlMapClientTemplate().queryForObject( "UserLogin.checkUser", user);
	}

	public User transData( User user ) throws Exception{
		return (User) getSqlMapClientTemplate().queryForObject( "UserLogin.transData", user);
	}
	public void updateSso( User user ){
		getSqlMapClientTemplate().update( "UserLogin.updateSso", user);
	}
	public void insertSsoHistory( User user ){
		getSqlMapClientTemplate().update( "UserLogin.insertSsoHistory", user);
	}
	public User userlogin2(User user) {
		return (User) getSqlMapClientTemplate().queryForObject("UserLogin.userlogin2", user);
	}
	
}
