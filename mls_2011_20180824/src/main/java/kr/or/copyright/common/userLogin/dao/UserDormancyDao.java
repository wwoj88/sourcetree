package kr.or.copyright.common.userLogin.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.common.userLogin.PasswordMismatchException;
import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.model.User;

public interface UserDormancyDao {

	public List<Map> UserDormancyCheck();

	public List<Map> userDormancyList();

	public void dormancyMove(Map map);

	public Map dormancyUserCheck( User user );

	public void dormancyInit( String userIdnt );

}
