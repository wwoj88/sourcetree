package kr.or.copyright.common.userLogin.dao;

import java.util.Map;

import kr.or.copyright.common.userLogin.PasswordMismatchException;
import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.model.User;

public interface UserLoginDao {

	public User login(User user) throws UserNotFoundException, PasswordMismatchException;
	
	public User certLogin(User user) throws UserNotFoundException, PasswordMismatchException;
	
	public Map findUser(Map params) throws UserNotFoundException;
	
	public Map certFindUser(Map params) throws UserNotFoundException;
	
	public Map findUserPswd(Map params) throws PasswordMismatchException;
	
	public User loginYn(User user) throws UserNotFoundException, PasswordMismatchException;
	
	public User certLoginYn(User user) throws UserNotFoundException, PasswordMismatchException;
	
	public User certAdminLogin(User user);
	
	public void updateCertInfoAll(User user);
	
	public void updateCertInfoOwn(User user);

	public void failCountUpdate( User user );

	public User failCountSelect( User user );

	public void userConnectLog( User user );

	public void failCountInit( User user );

	public User checkUser(User user)throws Exception;

	public User transData( User user ) throws Exception;

	public void updateSso( User user )throws Exception;

	public User userlogin2( User user )throws Exception;

	public void insertSsoHistory( User user )throws Exception;
}
