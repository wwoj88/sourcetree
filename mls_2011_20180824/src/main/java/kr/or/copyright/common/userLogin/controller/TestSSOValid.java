package kr.or.copyright.common.userLogin.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.bandi.oauth.BandiSSOAgent;
import com.dreamsecurity.jcaos.protocol.HTTP;

import kr.or.copyright.common.userLogin.UserNotFoundException;


public class TestSSOValid {

	// agent 관련 설정
	private static String ssoUri = "https://sso.copyright.or.kr/oauth2/token.do";	// 실서버 URL
	//private static String ssoUri = "https://devsso.copyright.or.kr/oauth2/token.do";	// 개발서버 URL
	private static String clientId = "fa48f99639b2418c956c1642d8e8e02e";
	private static String clientSecret = "8joqm4lgs11otg0ah8gz7nydp";
	private static String scope = "http://sso.copyright.or.kr";						// 고정
	
	public static void main(HttpServletRequest request) throws Exception {
		// SSO 로그인 연계 "BandiSSOAgent 생성" 참조
	   
		BandiSSOAgent agent = new BandiSSOAgent(ssoUri, clientId, clientSecret, scope);
		//BanSSO agent = new BanSSO(ssoUri, clientId, clientSecret, scope);
		String access_token = request.getParameter( "accessToken" );
		String refresh_token = request.getParameter( "refreshToken" );
		
		// 사용자 접속 ip
		//String client_ip = "127.0.0.1";
		String client_ip =agent.getLocalServerIp( request );
	
		if(access_token == null) {
			
			HttpSession session = request.getSession();
			access_token = (String) session.getAttribute("access_token" );
			refresh_token = (String) session.getAttribute("refresh_token" );
			
		}
		// 세션에서 취득한 제어 토큰
	

		//System.out.println("Valid Access_Token In Session  :: " +access_token  );
	//	System.out.println("Valid refresh_token In Session  :: " +refresh_token  );
		
		//==================================
		// SSO 유효성 검증 (Intercepter에서 매번 실행)
		//==================================
		// SSO 로그인 연계 "tokenValid 함수 요청" 및 "tokenValid 함수 결과" 참조
		
		HashMap<String, String> tokenValid = agent.tokenValid(access_token, client_ip);
		
		System.out.println("tokenValid.get(\"error\") ::" +tokenValid.get("error")  );
		
		if(tokenValid.get("error").equals("0000") == false) {
			// 토큰이 유효하지 않는 경우
			// 세션 로그아웃 처리
			// SSO 로그인 연계 "logout 함수요청" 참조\
			
			HttpSession session = request.getSession();
			session.removeAttribute("access_token" );
			session.removeAttribute("refresh_token");
			session.removeAttribute("sessUserIdnt");
			session.removeAttribute("sessPswd");
			session.removeAttribute("sessUserName");
			session.removeAttribute("sessSsoYn");
			session.removeAttribute("sessMail");
			session.removeAttribute("sessMoblPhon");
			session.removeAttribute("sessUserDivs");
			
			session.removeAttribute("sessSsbNo");
			session.removeAttribute("access_token" );
			session.removeAttribute("refresh_token");
			session.removeAttribute("sessClmsAgrYn");
			session.removeAttribute("sessClmsUserIdnt");
			session.removeAttribute("sessClmsPswd");
			
		    System.out.println( "ERROR :::: "+tokenValid.get("error") );
		    System.setProperty("https.cipherSuites", "SSL_RSA_WITH_3DES_EDE_CBC_SHA");
			agent.logout(access_token, client_ip);
			
			throw new UserNotFoundException("입력하신 정보에 해당되는 사용자가 없습니다.", null);
		}

		
		//==================================
		// SSO 로그 아웃
		//==================================
		// SSO 로그인 연계 "logout 함수요청" 참조
		//agent.logout(access_token, client_ip);
		// 세션 로그아웃 
	}

}
