package kr.or.copyright.common.userLogin.model;

import java.lang.reflect.Field;

public class User {

	private String userIdnt;
	private String pswd;
	private String userName;
	private String moblPhon;
	private String userDivs;
	private String mgnbYsno;
	private String mail;
	private String orgnMgnbYsno;
	private String scssYsno;
	private String clmsAgrYn;
	private String clmsUserIdnt;
	private String clmsPswd;
	private String userAddr;					// 엄동규 추가 유저 주소
	private String loginFailCnt;				// 20171110 추가 로그인 실패 횟수
	private String actionProcess;				// 20171110 추가 로그인 실패 횟수
	private String connectIp;
	private String ceti_usex_ysno;
	private String sso_loginyn;
	private String sso_login_date;
	private String sso_login_id;
	private String sso_login_seq;
	private String accessToken;
	private String refreshToken;
	


	
	
	
	public String getAccessToken(){
		return accessToken;
	}



	
	public void setAccessToken( String accessToken ){
		this.accessToken = accessToken;
	}



	
	public String getRefreshToken(){
		return refreshToken;
	}



	
	public void setRefreshToken( String refreshToken ){
		this.refreshToken = refreshToken;
	}



	public String getSso_login_id(){
		return sso_login_id;
	}


	
	public void setSso_login_id( String sso_login_id ){
		this.sso_login_id = sso_login_id;
	}


	
	public String getSso_login_seq(){
		return sso_login_seq;
	}


	
	public void setSso_login_seq( String sso_login_seq ){
		this.sso_login_seq = sso_login_seq;
	}


	public String getSso_loginyn(){
		return sso_loginyn;
	}

	
	public void setSso_loginyn( String sso_loginyn ){
		this.sso_loginyn = sso_loginyn;
	}

	
	public String getSso_login_date(){
		return sso_login_date;
	}

	
	public void setSso_login_date( String sso_login_date ){
		this.sso_login_date = sso_login_date;
	}

	public String getConnectIp(){
		return connectIp;
	}
	
	public void setConnectIp( String connectIp ){
		this.connectIp = connectIp;
	}
	private String CertLoginCd;					// 인증서 로그인 구분(crn:사업자, ssn:개인)
	private String SsnNo;						// 전자서명용 주민등록번호
	private String CrnNo;						// 전자서명용 사업자등록번호
	private String CertId;						// 전자서명용 CertID
	private String UserSignCert;				// 공인인증서 정보
	private String UserSignValue;				// 전자서명 값
	private String EncryptedSessionKey;			// 비밀키를 암호화한 정보
	private String EncryptedUserRandomNumber;	// 공인인증서 확인을 위한 정보
	private String EncryptedUserSSN;			// 공인인증서를 통하여 암호화 된 주민등록번호
	private String certDN;
	
	@Override
	public String toString(){
		return "User [userIdnt=" + userIdnt + ", pswd=" + pswd + ", userName=" + userName + ", moblPhon=" + moblPhon
			+ ", userDivs=" + userDivs + ", mgnbYsno=" + mgnbYsno + ", mail=" + mail + ", orgnMgnbYsno=" + orgnMgnbYsno
			+ ", scssYsno=" + scssYsno + ", clmsAgrYn=" + clmsAgrYn + ", clmsUserIdnt=" + clmsUserIdnt + ", clmsPswd="
			+ clmsPswd + ", userAddr=" + userAddr + ", loginFailCnt=" + loginFailCnt + ", actionProcess="
			+ actionProcess + ", CertLoginCd=" + CertLoginCd + ", SsnNo=" + SsnNo + ", CrnNo=" + CrnNo + ", CertId="
			+ CertId + ", UserSignCert=" + UserSignCert + ", UserSignValue=" + UserSignValue + ", EncryptedSessionKey="
			+ EncryptedSessionKey + ", EncryptedUserRandomNumber=" + EncryptedUserRandomNumber + ", EncryptedUserSSN="
			+ EncryptedUserSSN + ", certDN=" + certDN + "]";
	}
	public String getActionProcess(){
		return actionProcess;
	}
	public void setActionProcess( String actionProcess ){
		this.actionProcess = actionProcess;
	}
	public String getLoginFailCnt(){
		return loginFailCnt;
	}
	public void setLoginFailCnt( String loginFailCnt ){
		this.loginFailCnt = loginFailCnt;
	}
	public String getCertDN() {
		return certDN;
	}
	public void setCertDN(String certDN) {
		this.certDN = certDN;
	}
	public String getUserIdnt() {
		return userIdnt;
	}
	public void setUserIdnt(String userIdnt) {
		this.userIdnt = userIdnt;
	}
	public String getPswd() {
		return pswd;
	}
	public void setPswd(String pswd) {
		this.pswd = pswd;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMoblPhon() {
		return moblPhon;
	}
	public void setMoblPhon(String moblPhon) {
		this.moblPhon = moblPhon;
	}
	public String getUserDivs() {
		return userDivs;
	}
	public void setUserDivs(String userDivs) {
		this.userDivs = userDivs;
	}
	public String getMgnbYsno() {
		return mgnbYsno;
	}
	public void setMgnbYsno(String mgnbYsno) {
		this.mgnbYsno = mgnbYsno;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getOrgnMgnbYsno() {
		return orgnMgnbYsno;
	}
	public void setOrgnMgnbYsno(String orgnMgnbYsno) {
		this.orgnMgnbYsno = orgnMgnbYsno;
	}
	public String getScssYsno() {
		return scssYsno;
	}
	public void setScssYsno(String scssYsno) {
		this.scssYsno = scssYsno;
	}
	public String getUserAddr() {
		return userAddr;
	}
	public void setUserAddr(String userAddr) {
		this.userAddr = userAddr;
	}
	public String getClmsAgrYn() {
		return clmsAgrYn;
	}
	public void setClmsAgrYn(String clmsAgrYn) {
		this.clmsAgrYn = clmsAgrYn;
	}
	public String getClmsUserIdnt() {
		return clmsUserIdnt;
	}
	public void setClmsUserIdnt(String clmsUserIdnt) {
		this.clmsUserIdnt = clmsUserIdnt;
	}
	public String getClmsPswd() {
		return clmsPswd;
	}
	public void setClmsPswd(String clmsPswd) {
		this.clmsPswd = clmsPswd;
	}
	public String getCertLoginCd() {
		return CertLoginCd;
	}
	public void setCertLoginCd(String certLoginCd) {
		CertLoginCd = certLoginCd;
	}
	public String getSsnNo() {
		return SsnNo;
	}
	public void setSsnNo(String ssnNo) {
		SsnNo = ssnNo;
	}
	public String getCrnNo() {
		return CrnNo;
	}
	public void setCrnNo(String crnNo) {
		CrnNo = crnNo;
	}
	public String getCertId() {
		return CertId;
	}
	public void setCertId(String certId) {
		CertId = certId;
	}
	public String getUserSignCert() {
		return UserSignCert;
	}
	public void setUserSignCert(String userSignCert) {
		UserSignCert = userSignCert;
	}
	public String getUserSignValue() {
		return UserSignValue;
	}
	public void setUserSignValue(String userSignValue) {
		UserSignValue = userSignValue;
	}
	public String getEncryptedSessionKey() {
		return EncryptedSessionKey;
	}
	public void setEncryptedSessionKey(String encryptedSessionKey) {
		EncryptedSessionKey = encryptedSessionKey;
	}
	public String getEncryptedUserRandomNumber() {
		return EncryptedUserRandomNumber;
	}
	public void setEncryptedUserRandomNumber(String encryptedUserRandomNumber) {
		EncryptedUserRandomNumber = encryptedUserRandomNumber;
	}
	public String getEncryptedUserSSN() {
		return EncryptedUserSSN;
	}
	public void setEncryptedUserSSN(String encryptedUserSSN) {
		EncryptedUserSSN = encryptedUserSSN;
	}

	public String getCeti_usex_ysno(){
		return ceti_usex_ysno;
	}

	public void setCeti_usex_ysno( String ceti_usex_ysno ){
		this.ceti_usex_ysno = ceti_usex_ysno;
	}
	
	
	
	
	

}
