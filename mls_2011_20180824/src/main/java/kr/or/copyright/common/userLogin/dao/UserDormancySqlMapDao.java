package kr.or.copyright.common.userLogin.dao;

import java.util.List;
import java.util.Map;

import kr.or.copyright.common.userLogin.UserNotFoundException;
import kr.or.copyright.common.userLogin.model.User;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class UserDormancySqlMapDao extends SqlMapClientDaoSupport implements UserDormancyDao {

	public List<Map> UserDormancyCheck(){
		return (List) getSqlMapClientTemplate().queryForList( "UserDormancy.userDormancyCheck");
	}

	public List<Map> userDormancyList(){
		return (List) getSqlMapClientTemplate().queryForList( "UserDormancy.userDormancyList");
	}

	public void dormancyMove( Map map ){
		getSqlMapClientTemplate().update( "UserDormancy.dormancyMove",map);
		//getSqlMapClientTemplate().insert( "UserDormancy.userDelete",map);
	}

	public Map dormancyUserCheck( User user ){
		return (Map) getSqlMapClientTemplate().queryForObject("UserDormancy.dormancyUserCheck",user);
	}

	public void dormancyInit( String userIdnt ){
		getSqlMapClientTemplate().update( "UserDormancy.dormancyInit",userIdnt);
		//getSqlMapClientTemplate().insert( "UserDormancy.dormancyDelete",userIdnt);
	}
	

}
