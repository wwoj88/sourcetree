<%@ page language="java" contentType="text/html;charset=euc-kr" %>

<%
//현재 URL 가져오기
String c_url = javax.servlet.http.HttpUtils.getRequestURL(request).toString(); 

//가져온 URL 에서 도메인부분만 가져오기
String temp = c_url.substring(0, c_url.indexOf("/", 8));

    NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();
    
    String sSiteCode = "G6840";				// NICE로부터 부여받은 사이트 코드
    String sSitePassword = "91F92CJ77YO7";		// NICE로부터 부여받은 사이트 패스워드
    
    String sRequestNumber = "REQ0000000001";        	// 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로 
                                                    	// 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.
    sRequestNumber = niceCheck.getRequestNO(sSiteCode);
  	session.setAttribute("REQ_SEQ" , sRequestNumber);	// 해킹등의 방지를 위하여 세션을 쓴다면, 세션에 요청번호를 넣는다.
  	
   	String sAuthType = "";      	// 없으면 기본 선택화면, M: 핸드폰, C: 신용카드, X: 공인인증서
   	
   	String popgubun 	= "N";		//Y : 취소버튼 있음 / N : 취소버튼 없음
		String customize 	= "";			//없으면 기본 웹페이지 / Mobile : 모바일페이지
		
		 // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
	    String sReturnUrl = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("checkSuccess"));
	    //String sReturnUrl = "http://dev.findcopyright.or.kr/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL
		String sErrorUrl = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("checkFail"));
	    //String sErrorUrl = "http://dev.findcopyright.or.kr/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL
	    
	    // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
    //String sReturnUrl1 = "https://www.kdce.or.kr:444/user/checkplus_success_sa.do";      // 성공시 이동될 URL(운영)
    //String sErrorUrl1 = "https://www.kdce.or.kr:444/user/checkplus_fail.do";          // 실패시 이동될 URL(운영)
      if(temp.equalsIgnoreCase("http://dev.copyright.or.kr")){
    	  sReturnUrl = "http://dev.copyright.or.kr/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(로컬)
    	  sErrorUrl = "http://dev.copyright.or.kr/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(로컬)
   }else if(temp.equalsIgnoreCase("http://www.copyright.or.kr:8880")){
	   sReturnUrl = "http://www.copyright.or.kr:8880/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(개발)
	   sErrorUrl = "http://www.copyright.or.kr:8880/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(개발)
   }else if(temp.equalsIgnoreCase("http://localhost:8080")){
	   sReturnUrl = "http://localhost:8080/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(개발)
	   sErrorUrl = "http://localhost:8080/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(개발)
   }

    // 입력될 plain 데이타를 만든다.
    String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
                        "8:SITECODE" + sSiteCode.getBytes().length + ":" + sSiteCode +
                        "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
                        "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
                        "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
                        "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
                        "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize;
    
    String sMessage = "";
    String sEncData1 = "";
    
    int iReturn = niceCheck.fnEncode(sSiteCode, sSitePassword, sPlainData);
    if( iReturn == 0 )
    {
        sEncData1 = niceCheck.getCipherData();
    }
    else if( iReturn == -1)
    {
        sMessage = "암호화 시스템 에러입니다.";
    }    
    else if( iReturn == -2)
    {
        sMessage = "암호화 처리오류입니다.";
    }    
    else if( iReturn == -3)
    {
        sMessage = "암호화 데이터 오류입니다.";
    }    
    else if( iReturn == -9)
    {
        sMessage = "입력 데이터 오류입니다.";
    }    
    else
    {
        sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
    }
%>

<html>
<head>
	<title>NICE신용평가정보 - CheckPlus 안심본인인증 테스트</title>
	
	<script language='javascript'>
	window.name ="Parent_window";
	
	function fnPopup(){
		window.open('', 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
		document.form_chk.target = "popupChk";
		document.form_chk.submit();
	}
	</script>
</head>
<body onload="fnPopup()" >
	<%= sMessage %><br><br>
	업체정보 암호화 데이타 : [<%= sEncData1 %>]<br><br>

	<!-- 본인인증 서비스 팝업을 호출하기 위해서는 다음과 같은 form이 필요합니다. -->
	<form name="form_chk" method="post">
		<input type="hidden" name="m" value="checkplusSerivce">						<!-- 필수 데이타로, 누락하시면 안됩니다. -->
		<input type="hidden" name="EncodeData" value="<%= sEncData1 %>">		<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
	    
	    <!-- 업체에서 응답받기 원하는 데이타를 설정하기 위해 사용할 수 있으며, 인증결과 응답시 해당 값을 그대로 송신합니다.
	    	 해당 파라미터는 추가하실 수 없습니다. -->
		<input type="hidden" name="param_r1" value="">
		<input type="hidden" name="param_r2" value="">
		<input type="hidden" name="param_r3" value="">
	    
		<a href="javascript:fnPopup();"> CheckPlus 안심본인인증 Click</a>
	</form>
</body>
</html>