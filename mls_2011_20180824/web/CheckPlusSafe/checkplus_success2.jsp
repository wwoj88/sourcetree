<%@ page language="java" contentType="text/html;charset=euc-kr" %>

<%
    NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();

    String sEncodeData = requestReplace(request.getParameter("EncodeData"), "encodeData");
    String sReserved1  = request.getParameter("param_r1");
    String sReserved2  = request.getParameter("param_r2");
    String sReserved3  = request.getParameter("param_r3");

    String sSiteCode = "G6840";				   // NICE로부터 부여받은 사이트 코드
    String sSitePassword = "91F92CJ77YO7";			 // NICE로부터 부여받은 사이트 패스워드

    String sCipherTime = "";				 // 복호화한 시간
    String sRequestNumber = "";			 // 요청 번호
    String sResponseNumber = "";		 // 인증 고유번호
    String sAuthType = "";				   // 인증 수단
    String sName = "";							 // 성명
    String sDupInfo = "";						 // 중복가입 확인값 (DI_64 byte)
    String sConnInfo = "";					 // 연계정보 확인값 (CI_88 byte)
    String sBirthDate = "";					 // 생일
    String sGender = "";						 // 성별
    String sNationalInfo = "";       // 내/외국인정보 (개발가이드 참조)
    String sMessage = "";
    String sPlainData = "";
    
    int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);

    if( iReturn == 0 )
    {
        sPlainData = niceCheck.getPlainData();
        sCipherTime = niceCheck.getCipherDateTime();
        
        // 데이타를 추출합니다.
        java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);
        
        sRequestNumber  = (String)mapresult.get("REQ_SEQ");
        sResponseNumber = (String)mapresult.get("RES_SEQ");
        sAuthType 			= (String)mapresult.get("AUTH_TYPE");
        sName 					= (String)mapresult.get("NAME");
        sBirthDate 			= (String)mapresult.get("BIRTHDATE");
        sGender 				= (String)mapresult.get("GENDER");
        sNationalInfo  	= (String)mapresult.get("NATIONALINFO");
        sDupInfo 				= (String)mapresult.get("DI");
        sConnInfo 			= (String)mapresult.get("CI");
	    // 휴대폰 번호 : MOBILE_NO => (String)mapresult.get("MOBILE_NO");
		// 이통사 정보 : MOBILE_CO => (String)mapresult.get("MOBILE_CO");
		// checkplus_success 페이지에서 결과값 null 일 경우, 관련 문의는 관리담당자에게 하시기 바랍니다.        
        String session_sRequestNumber = (String)session.getAttribute("REQ_SEQ");
        if(!sRequestNumber.equals(session_sRequestNumber))
        {
            sMessage = "세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.";
            sResponseNumber = "";
            sAuthType = "";
        }
    }
    else if( iReturn == -1)
    {
        sMessage = "복호화 시스템 에러입니다.";
    }    
    else if( iReturn == -4)
    {
        sMessage = "복호화 처리오류입니다.";
    }    
    else if( iReturn == -5)
    {
        sMessage = "복호화 해쉬 오류입니다.";
    }    
    else if( iReturn == -6)
    {
        sMessage = "복호화 데이터 오류입니다.";
    }    
    else if( iReturn == -9)
    {
        sMessage = "입력 데이터 오류입니다.";
    }    
    else if( iReturn == -12)
    {
        sMessage = "사이트 패스워드 오류입니다.";
    }    
    else
    {
        sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
    }

%>
<%!
public static String requestReplace (String paramValue, String gubun) {
        String result = "";
        
        if (paramValue != null) {
        	
        	paramValue = paramValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

        	paramValue = paramValue.replaceAll("\\*", "");
        	paramValue = paramValue.replaceAll("\\?", "");
        	paramValue = paramValue.replaceAll("\\[", "");
        	paramValue = paramValue.replaceAll("\\{", "");
        	paramValue = paramValue.replaceAll("\\(", "");
        	paramValue = paramValue.replaceAll("\\)", "");
        	paramValue = paramValue.replaceAll("\\^", "");
        	paramValue = paramValue.replaceAll("\\$", "");
        	paramValue = paramValue.replaceAll("'", "");
        	paramValue = paramValue.replaceAll("@", "");
        	paramValue = paramValue.replaceAll("%", "");
        	paramValue = paramValue.replaceAll(";", "");
        	paramValue = paramValue.replaceAll(":", "");
        	paramValue = paramValue.replaceAll("-", "");
        	paramValue = paramValue.replaceAll("#", "");
        	paramValue = paramValue.replaceAll("--", "");
        	paramValue = paramValue.replaceAll("-", "");
        	paramValue = paramValue.replaceAll(",", "");
        	
        	if(gubun != "encodeData"){
        		paramValue = paramValue.replaceAll("\\+", "");
        		paramValue = paramValue.replaceAll("/", "");
            paramValue = paramValue.replaceAll("=", "");
        	}
        	
        	result = paramValue;
            
        }
        return result;
  }
%>

<html>
<head>
    <title>NICE신용평가정보 - CheckPlus 안심본인인증 테스트</title>
    <script type="text/javascript">
    	function fnLoad(){		
    		console.log('<%=sReserved1%>');
    		
    		var sBirthDate = '<%=sBirthDate%>';
    		
    		var year= sBirthDate.substr(0,4);
    		var month= sBirthDate.substr(4,2);
    		var day= sBirthDate.substr(6,2);
    		var baseYear = new Date().getFullYear()-15; 
    		var baseMonth = new Date().getMonth() + 1;
    		var baseDay = new Date().getDate();
    		/* console.log(year+" : "+baseYear);
    		console.log(month+" : "+baseMonth);
    		console.log(day+" : "+baseDay);
    		console.log(year<baseYear)
    		console.log(year==baseYear&&month<baseMonth) */
    		/* if(year<baseYear||year==baseYear&&month>=baseMonth&&day>=baseDay){
    			alert("만 14세 초과입니다.");
    			parent.opener.parent.document.frm2.action = "/user/user.do?method=goPage";
    			parent.opener.parent.document.frm2.submit();
    			self.close();
    			return false;
    		} */

    	
			alert("본인인증에 성공하셨습니다.");
			console.log("본인인증완료");
<%			
			if(sReserved1.equals("idntSrch")){
				
%>    			console.log("if1");
				parent.opener.parent.document.frm1.userName.value = "<%= sName %>";
	    		parent.opener.parent.document.frm1.dupInfo.value = "<%= sDupInfo %>";
	    		parent.opener.parent.document.frm1.connInfo.value = "<%= sConnInfo %>";
	    		parent.opener.parent.document.frm1.birthDate.value = "<%= sBirthDate %>";
    			parent.opener.parent.document.frm1.action = "/user/user.do?method=identificationUser2";
				parent.opener.parent.document.frm1.submit();
		  		self.close();
		  		
<%
			} else if (sReserved1.equals("pswdSrch")){
%>				console.log("if2");
				parent.opener.parent.document.frm2.requestNumber.value = "<%= sRequestNumber %>";
	    		parent.opener.parent.document.frm2.responseNumber.value = "<%= sResponseNumber %>";
	    		parent.opener.parent.document.frm2.userName.value = "<%= sName %>";
	    		parent.opener.parent.document.frm2.userName_pwd.value = "<%= sName %>";
	    		parent.opener.parent.document.frm2.dupInfo.value = "<%= sDupInfo %>";
	    		parent.opener.parent.document.frm2.connInfo.value = "<%= sConnInfo %>";
	    		parent.opener.parent.document.frm2.birthDate.value = "<%= sBirthDate %>";
	    		parent.opener.parent.document.frm2.userIdnt_pwd.value = "<%= sReserved2 %>";
	    		parent.opener.parent.document.frm2.userEmail_pwd.value = "<%= sReserved3 %>";
    			parent.opener.parent.document.frm2.action = "/user/user.do?method=identificationUser2";
				parent.opener.parent.document.frm2.submit();
		  		self.close();
<%
			}else{
%>    		
				console.log("if3");
				
	    		parent.opener.parent.document.frm2.userName.value = "<%= sName %>";
	    		console.log(parent.opener.parent.document.frm2.userName.value);
	    		parent.opener.parent.document.frm2.dupInfo.value = "<%= sDupInfo %>";
	    		parent.opener.parent.document.frm2.connInfo.value = "<%= sConnInfo %>";
	    		parent.opener.parent.document.frm2.birthDate.value = "<%= sBirthDate %>";
    			parent.opener.parent.document.frm2.action = "/user/user.do?method=identificationUser2";
				parent.opener.parent.document.frm2.submit();
				self.close();
		 <% }%>
	}
    	
    </script>
    
</head>
<body onload="fnLoad()" >
    <center>

    
	</center>
</body>
</html>