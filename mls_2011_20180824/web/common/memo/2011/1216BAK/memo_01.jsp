<%@ page contentType="text/html;charset=euc-kr" %>
<%
String DIVS = request.getParameter("DIVS");
%>
<!-- 
<div class="contentsRoundBox" >
	<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
	<div class="infoImg2">
		<p><strong>음악저작물 권리찾기 신청에 관란 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
		<ul>
			<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
			<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
			<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
		</ul>
	</div>
</div>
 -->
 
 <!-- 
<fieldset class="sch  mt0">
	<div class="contentsSch  mt0">
		<span class="round lb"></span><span class="round rb"></span><span class="round lt"></span><span class="round rt"></span>
		
		<div class="infoImg2">
			<p><strong>음악저작물 권리찾기 신청에 관란 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
			<ul>
				<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
				<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
				<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
			</ul>
		</div>	
	</div>
</fieldset>
 -->

<%
if(DIVS!=null || DIVS.length()>0){
	
	 //out.println("[ "+DIVS+" ]");
	
	if(DIVS.equals("M") || DIVS.equals("X")
		|| DIVS.equals("1") || DIVS.equals("B")
		|| DIVS.equals("2") || DIVS.equals("S")
		|| DIVS.equals("3") || DIVS.equals("L")
		|| DIVS.equals("C") || DIVS.equals("I")
		|| DIVS.equals("V") || DIVS.equals("R") || DIVS.equals("N")){
%> 
<%
		// 음악
		if(DIVS.equals("M")){
%>		
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2011/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">음악저작물 저작권찾기<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국음악저작권협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">황혜림</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-2660-0594</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">tnlover07@hotmail.com</span>
							</li>
							<li>
								<em class="w20">한국음원제작자협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">최소정</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-3270-5933</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">csj@kapp.or.kr</span>
							</li>
							<li>
								<em class="w20">한국음악실연자연합회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">민부영(대리)</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-745-8286(6112)</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">prettyowl@fkmp.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}
		// 도서
		else if( DIVS.equals("B")){
%>	
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2011/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">도서저작물 저작권찾기<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국문예학술저작권협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">김은정</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-508-0440</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">kej0512@copyrightkorea.org</span>
							</li>
							<li>
								<em class="w20">한국복사전송권협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">김경채 대리</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-2608-2036</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">abcchae@krtra.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}
// 뉴스(N)
		else if( DIVS.equals("N")){
%>		
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2011/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">뉴스저작물 저작권찾기<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국언론진흥재단</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">미래기술팀</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-2001-7782</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">dnc@kpf.com</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}
		// 방송대본
		else if( DIVS.equals("C")){
%>	
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2011/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">방송대본저작물 저작권찾기<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국방송작가협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">윤선미</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-782-1696</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">office@ktrwa.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}
		// 이미지
		else if( DIVS.equals("I")){
%>	
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2011/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">이미지저작물 저작권찾기<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국문예학술저작권협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">김은정</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-508-0440</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">kej0512@copyrightkorea.org</span>
							</li>
							<li>
								<em class="w20">한국복사전송권협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">김경채 대리</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-2608-2036</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">abcchae@krtra.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}
		// 영화
		else if( DIVS.equals("V")){
%>	
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2011/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">영화저작물 저작권찾기<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국영화제작가협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">정재훈</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-2267-9983</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">kfpa2@kfpa.net</span>
							</li>
							<li>
								<em class="w20">한국시나리오작가협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">정지영 총무</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-2275-0566</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">scenario11@hanmail.net</span>
							</li>
							<li>
								<em class="w20">한국영상산업협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">김의수 팀장</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-3452-1008</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">master@kmva.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}
		// 방송
		else if( DIVS.equals("R")){
%>	
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2011/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">방송저작물 저작권찾기<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국방송실연자협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">육세영 선임</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-784-7802</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">kbpa7802@chol.com</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}
		// 기타
		else if( DIVS.equals("X")){
%>	
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2011/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">기타저작물 저작권찾기<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국음악저작권협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">황혜림</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-2660-0594</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">tnlover07@hotmail.com</span>
							</li>
							<li>
								<em class="w20">한국음악실연자연합회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">민부영 대리</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-745-8286</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">prettyowl@fkmp.kr</span>
							</li>
							<li>
								<em class="w20">한국음원제작자협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">최소정</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-3270-5933</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">csj@kapp.or.kr</span>
							</li>
							<li>
								<em class="w20">한국문예학술저작권협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">김은정</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-508-0440</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">kej0512@copyrightkorea.org</span>
							</li>
							<li>
								<em class="w20">한국복사전송권협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">김경채 대리</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-2608-2036</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">abcchae@krtra.or.kr</span>
							</li>
							<li>
								<em class="w20">한국방송작가협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">윤선미</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-782-1696</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">office@ktrwa.or.kr</span>
							</li>
							<li>
								<em class="w20">한국영상산업협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">박혜영</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-3452-1008</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">master@kmva.or.kr</span>
							</li>
							<li>
								<em class="w20">한국시나리오작가협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">정지영 총무</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-2275-0566</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">scenario11@hanmail.net</span>
							</li>
							<li>
								<em class="w20">한국영화제작가협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">정재훈</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-2267-9983</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">kfpa2@kfpa.net</span>
							</li>
							<li>
								<em class="w20">한국방송실연자협회</em>
								<span class="w20"><img alt="" src="/images/2011/common/ic_nm.gif">육세영 선임</span>
								<span class="w25"><img alt="" src="/images/2011/common/ic_tel.gif">02-784-7802</span>
								<span class="w30"><img alt="" src="/images/2011/common/ic_mail.gif">kbpa7802@chol.com</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>				
<%
		}
		// 방송음악
		else if( DIVS.equals("1") || DIVS.equals("B")){
%>		
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img src="/images/2011/content/box_img1.gif" alt="" /></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">방송음악보상금<strong class="blue2">신청에 관한 문의는 신청분류에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국음악실연자연합회</em>
								<span class="w20"><img src="/images/2011/common/ic_nm.gif" alt="" />권기태(팀장)</span>
								<span class="w25"><img src="/images/2011/common/ic_tel.gif" alt="" />02-745-8286(6220)</span>
								<span class="w30"><img src="/images/2011/common/ic_mail.gif" alt="" />kjery@fkmp.kr</span>
							</li>
							<li>
								<em class="w20">한국음원제작자협회</em>
								<span class="w20"><img src="/images/2011/common/ic_nm.gif" alt="" />최소정</span>
								<span class="w25"><img src="/images/2011/common/ic_tel.gif" alt="" />02-3270-5933</span>
								<span class="w30"><img src="/images/2011/common/ic_mail.gif" alt="" />csj@kapp.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}
		// 교과용(2,S)
		else if( DIVS.equals("2") || DIVS.equals("S")){
%>		
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img src="/images/2011/content/box_img1.gif" alt="" /></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">교과용보상금<strong class="blue2">신청에 관한 문의는 신청분류에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국복사전송권협회</em>
								<span class="w20"><img src="/images/2011/common/ic_nm.gif" alt="" />김나영</span>
								<span class="w25"><img src="/images/2011/common/ic_tel.gif" alt="" />02-2608-2036(대표)</span>
								<span class="w30"><img src="/images/2011/common/ic_mail.gif" alt="" />nayoung@krtra.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}
		// 도서관(3,L)
		else if( DIVS.equals("3") || DIVS.equals("L")){
%>		
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img src="/images/2011/content/box_img1.gif" alt="" /></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">도서관보상금 <strong class="blue2">신청에 관한 문의는 신청분류에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국복사전송권협회</em>
								<span class="w20"><img src="/images/2011/common/ic_nm.gif" alt="" />황경환</span>
								<span class="w25"><img src="/images/2011/common/ic_tel.gif" alt="" />02-2608-2036(대표)</span>
								<span class="w30"><img src="/images/2011/common/ic_mail.gif" alt="" />hkh@krtra.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}

%>
<%
	}
}
%>