<%@ page contentType="text/html;charset=euc-kr" %>
<%
String DIVS = request.getParameter("DIVS");
%>
<!-- 
<div class="contentsRoundBox" >
	<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
	<div class="infoImg2">
		<p><strong>음악저작물 권리찾기 신청에 관란 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
		<ul>
			<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
			<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
			<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
		</ul>
	</div>
</div>
 -->
 
 <!-- 
<fieldset class="sch  mt0">
	<div class="contentsSch  mt0">
		<span class="round lb"></span><span class="round rb"></span><span class="round lt"></span><span class="round rt"></span>
		
		<div class="infoImg2">
			<p><strong>음악저작물 권리찾기 신청에 관란 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
			<ul>
				<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
				<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
				<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
			</ul>
		</div>	
	</div>
</fieldset>
 -->

<%
if(DIVS!=null || DIVS.length()>0){
	
	 //out.println("[ "+DIVS+" ]");
	
	if(DIVS.equals("M") || DIVS.equals("X")
		|| DIVS.equals("1") || DIVS.equals("B")
		|| DIVS.equals("2") || DIVS.equals("S")
		|| DIVS.equals("3") || DIVS.equals("L")
		|| DIVS.equals("4") || DIVS.equals("A")
		|| DIVS.equals("C") || DIVS.equals("I")
		|| DIVS.equals("V") || DIVS.equals("R") || DIVS.equals("N") || DIVS.equals("MS")){
%> 
<%
		// 음악
		if(DIVS.equals("M")){
%>		
			<!-- 
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2012/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">음악저작물 저작권정보 변경<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국음악저작권협회 (작사,작곡,편곡)</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>전종훈</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2660-0585</span>
								<span class="w40"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>bigshow0411@komca.or.kr</span>
							</li>
							<li>
								<em class="w20">함께하는음악저작인협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>이용수</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-333-8766</span>
								<span class="w40"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>yongsoo.lee@koscap.or.kr</span>
							</li>
							<li>
								<em class="w20">한국음악실연자연합회 (가창,연주,지휘)</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>권기태 팀장</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2659-7048</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>kjery@fkmp.kr</span>
							</li>
							<li>
								<em class="w20">한국음반산업협회 (음반제작사)</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>김제경</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-3270-5933</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>kjk@riak.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			 -->
					 <!-- Tab str -->
                          <ul class="sub_menu1 w114 mar_tp30">
                           <!--    <li class="first"><a href="/mlsInfo/rghtInfo01.jsp">소개</a></li>
                              <li><a href="/mlsInfo/rghtInfo02.jsp">이용방법</a></li> -->
                              <li class="on"><a href="/rghtPrps/rghtSrch.do?DIVS=M" class="on">음악</a></li>
							  <li><a href="/rghtPrps/rghtSrch.do?DIVS=B">어문</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=N">뉴스</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송</a></li>
                      		</ul>
                      <!-- //Tab -->
                      <p class="clear"></p>
			 <div class="sub01_con_bg4_tp mar_tp30"></div>
			 <div class="sub01_con_bg4">
					<div class="font15"><span class="color_2c65aa">음악저작물 저작권 정보 변경</span>신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</div>
					<h3 class="mar_tp10">한국음악저작권협회 (작사,작곡,편곡)</h3>
					<div class="tel_and_email mar_tp10">
						<span class="word_dian_bg">전종훈</span>
						<span class="word_dian_bg"style="margin-left: 80px;">Tel : 02-2660-0585</span>
						<span class="word_dian_bg"style="margin-left: 70px;">E-mail : bigshow0411@komca.or.kr</span>
					</div>
					<h3 class="mar_tp10">함께하는음악저작인협회</h3>
					<div class="tel_and_email mar_tp10">
						<span class="word_dian_bg">이용수</span>
						<span class="word_dian_bg"style="margin-left: 80px;">Tel : 02-333-8766</span>
						<span class="word_dian_bg"style="margin-left: 78px;">E-mail : yongsoo.lee@koscap.or.kr</span>
					</div>
					<h3 class="mar_tp10">한국음악실연자연합회 (가창,연주,지휘)</h3>
					<div class="tel_and_email mar_tp10">
						<span class="word_dian_bg">권기태 팀장</span>
						<span class="word_dian_bg"style="margin-left: 50px;">Tel : 02-2659-7048</span>
						<span class="word_dian_bg"style="margin-left: 68px;">E-mail : kjery@fkmp.kr</span>
					</div>
					<h3 class="mar_tp10">한국음반산업협회 (음반제작사)</h3>
					<div class="tel_and_email mar_tp10">
						<span class="word_dian_bg">김제경</span>
						<span class="word_dian_bg"style="margin-left: 80px;">Tel : 02-3270-5933</span>
						<span class="word_dian_bg"style="margin-left: 68px;">E-mail : kjk@riak.or.kr</span>
					</div>
				</div>
<%
		}
		// 도서
		else if( DIVS.equals("B")){
%>	
			<!-- 
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2012/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">어문저작물 저작권정보 변경<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국문예학술저작권협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>박남규</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-508-0440</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>png@ekosa.org</span>
							</li>
							<li>
								<em class="w20">한국복제전송저작권협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>심명관</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>070-4265-2536</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>smk@korra.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			 -->
					 <!-- Tab str -->
                          <ul class="sub_menu1 w114 mar_tp30">
                           <!--    <li class="first"><a href="/mlsInfo/rghtInfo01.jsp">소개</a></li>
                              <li><a href="/mlsInfo/rghtInfo02.jsp">이용방법</a></li> -->
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악</a></li>
							  <li class="on"><a href="/rghtPrps/rghtSrch.do?DIVS=B" class="on">어문</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=N">뉴스</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송</a></li>
                      		</ul>
                      <!-- //Tab -->
                      <p class="clear"></p>
			 <div class="sub01_con_bg4_tp mar_tp30"></div>
			 <div class="sub01_con_bg4">
					<div class="font15"><span class="color_2c65aa">어문저작물 저작권 정보 변경</span>신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</div>
					<h3 class="mar_tp10">한국문예학술저작권협회</h3>
					<div class="tel_and_email mar_tp10">
						<span class="word_dian_bg">박남규</span>
						<span class="word_dian_bg"style="margin-left: 80px;">Tel : 02-508-0440</span>
						<span class="word_dian_bg"style="margin-left: 80px;">E-mail : png@ekosa.org</span>
					</div>
					<h3 class="mar_tp10">한국복제전송저작권협회</h3>
					<div class="tel_and_email mar_tp10">
						<span class="word_dian_bg">심명관</span>
						<span class="word_dian_bg"style="margin-left: 80px;">Tel : 070-4265-2536</span>
						<span class="word_dian_bg"style="margin-left: 63px;">E-mail : smk@korra.kr</span>
					</div>
				</div>
<%
		}
// 뉴스(N)
		else if( DIVS.equals("N")){
%>		
			<!-- 
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2012/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">뉴스저작물 저작권정보 변경<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국언론진흥재단</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>뉴스저작권팀</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2001-7791(~8)</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>news@kpf.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			 -->
					 <!-- Tab str -->
                          <ul class="sub_menu1 w114 mar_tp30">
                           <!--    <li class="first"><a href="/mlsInfo/rghtInfo01.jsp">소개</a></li>
                              <li><a href="/mlsInfo/rghtInfo02.jsp">이용방법</a></li> -->
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악</a></li>
							  <li><a href="/rghtPrps/rghtSrch.do?DIVS=B">어문</a></li>
                              <li class="on"><a href="/rghtPrps/rghtSrch.do?DIVS=N" class="on">뉴스</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송</a></li>
                      		</ul>
                      <!-- //Tab -->
                      <p class="clear"></p>
			 <div class="sub01_con_bg4_tp mar_tp30"></div>
			 <div class="sub01_con_bg4">
				<div class="font15"><span class="color_2c65aa">뉴스저작물 저작권 정보 변경</span>신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</div>
				<h3 class="mar_tp10">한국언론진흥재단</h3>
				<div class="tel_and_email mar_tp10">
					<span class="word_dian_bg">뉴스저작권팀</span>
					<span class="word_dian_bg"style="margin-left: 40px;">Tel : 02-2001-7791(~8)</span>
					<span class="word_dian_bg"style="margin-left: 45px;">E-mail : news@kpf.or.kr</span>
				</div>
			</div>
<%
		}
		// 방송대본
		else if( DIVS.equals("C")){
%>	
			<!-- 
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2012/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">방송대본저작물 저작권정보 변경<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국방송작가협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>김지숙 부장</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-782-1696</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>copyright@ktwa.or.kr</span>
							</li>
							<li>
								<em class="w20">한국시나리오작가협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>정지영 부장</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2275-0566(대표)</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>scenario11@hanmail.net</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			 -->
			 		<!-- Tab str -->
                          <ul class="sub_menu1 w114 mar_tp30">
                           <!--    <li class="first"><a href="/mlsInfo/rghtInfo01.jsp">소개</a></li>
                              <li><a href="/mlsInfo/rghtInfo02.jsp">이용방법</a></li> -->
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악</a></li>
							  <li><a href="/rghtPrps/rghtSrch.do?DIVS=B">어문</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=N">뉴스</a></li>
                              <li class="on"><a href="/rghtPrps/rghtSrch.do?DIVS=C" class="on">방송대본</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송</a></li>
                      		</ul>
                      <!-- //Tab -->
                      <p class="clear"></p>
			 <div class="sub01_con_bg4_tp mar_tp30"></div>
			 <div class="sub01_con_bg4">
				<div class="font15"><span class="color_2c65aa">방송대본저작물 저작권 정보 변경</span>신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</div>
				<h3 class="mar_tp10">한국방송작가협회</h3>
				<div class="tel_and_email mar_tp10">
					<span class="word_dian_bg">김지숙 부장</span>
					<span class="word_dian_bg"style="margin-left: 50px;">Tel : 02-782-1696</span>
					<span class="word_dian_bg"style="margin-left: 81px;">E-mail : copyright@ktwa.or.kr</span>
				</div>
				<h3 class="mar_tp10">한국시나리오작가협회</h3>
				<div class="tel_and_email mar_tp10">
					<span class="word_dian_bg">정지영 부장</span>
					<span class="word_dian_bg"style="margin-left: 50px;">Tel : 02-2275-0566(대표)</span>
					<span class="word_dian_bg"style="margin-left: 37px;">E-mail : scenario11@hanmail.net</span>
				</div>
			</div>
<%
		}
		// 이미지
		else if( DIVS.equals("I")){
%>	
			<!-- 
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2012/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">이미지저작물 저작권정보 변경<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국문예학술저작권협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>박남규</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-508-0440</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>png@ekosa.org</span>
							</li>
							<li>
								<em class="w20">한국복제전송저작권협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>심명관</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>070-4265-2536</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>smk@korra.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			 -->
			 		 <!-- Tab str -->
                          <ul class="sub_menu1 w114 mar_tp30">
                           <!--    <li class="first"><a href="/mlsInfo/rghtInfo01.jsp">소개</a></li>
                              <li><a href="/mlsInfo/rghtInfo02.jsp">이용방법</a></li> -->
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악</a></li>
							  <li><a href="/rghtPrps/rghtSrch.do?DIVS=B">어문</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=N">뉴스</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본</a></li>
                              <li class="on"><a href="/rghtPrps/rghtSrch.do?DIVS=I" class="on">이미지</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송</a></li>
                      		</ul>
                      <!-- //Tab -->
                      <p class="clear"></p>
			 <div class="sub01_con_bg4_tp mar_tp30"></div>
			 <div class="sub01_con_bg4">
				<div class="font15"><span class="color_2c65aa">이미지저작물 저작권 정보 변경</span>신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</div>
				<h3 class="mar_tp10">한국문예학술저작권협회</h3>
				<div class="tel_and_email mar_tp10">
					<span class="word_dian_bg">박남규</span>
					<span class="word_dian_bg"style="margin-left: 80px;">Tel : 02-508-0440</span>
					<span class="word_dian_bg"style="margin-left: 80px;">E-mail : png@ekosa.org</span>
				</div>
				<h3 class="mar_tp10">한국복제전송저작권협회</h3>
				<div class="tel_and_email mar_tp10">
					<span class="word_dian_bg">심명관</span>
					<span class="word_dian_bg"style="margin-left: 80px;">Tel : 070-4265-2536</span>
					<span class="word_dian_bg"style="margin-left: 63px;">E-mail : smk@korra.kr</span>
				</div>
			</div>
<%
		}
		// 영화
		else if( DIVS.equals("V")){
%>	
			<!-- 
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2012/content/box_img1.gif"/></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">영화저작물 저작권정보 변경<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국영화제작가협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>권명화</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2267-9984</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>kfpa3@kfpa.net</span>
							</li>
							<li>
								<em class="w20">한국시나리오작가협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>정지영</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2275-0566(대표)</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>scenario11@hanmail.net</span>
							</li>
							<li>
								<em class="w20">한국영화배급협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif">김의수 부장</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif">02-3452-1001</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif">webmaster@mdak.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			 -->
			 		 <!-- Tab str -->
                          <ul class="sub_menu1 w114 mar_tp30">
                           <!--    <li class="first"><a href="/mlsInfo/rghtInfo01.jsp">소개</a></li>
                              <li><a href="/mlsInfo/rghtInfo02.jsp">이용방법</a></li> -->
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악</a></li>
							  <li><a href="/rghtPrps/rghtSrch.do?DIVS=B">어문</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=N">뉴스</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지</a></li>
                              <li class="on"><a href="/rghtPrps/rghtSrch.do?DIVS=V" class="on">영화</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송</a></li>
                      		</ul>
                      <!-- //Tab -->
                      <p class="clear"></p>
			 <div class="sub01_con_bg4_tp mar_tp30"></div>
			 <div class="sub01_con_bg4">
				<div class="font15"><span class="color_2c65aa">영화저작물 저작권 정보 변경</span>신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</div>
				<h3 class="mar_tp10">한국영화제작가협회</h3>
				<div class="tel_and_email mar_tp10">
					<span class="word_dian_bg">권명화</span>
					<span class="word_dian_bg"style="margin-left: 80px;">Tel : 02-2267-9984</span>
					<span class="word_dian_bg"style="margin-left: 72px;">E-mail : kfpa3@kfpa.net</span>
				</div>
				<h3 class="mar_tp10">한국시나리오작가협회</h3>
				<div class="tel_and_email mar_tp10">
					<span class="word_dian_bg">정지영</span>
					<span class="word_dian_bg"style="margin-left: 80px;">Tel : 02-2275-0566</span>
					<span class="word_dian_bg"style="margin-left: 72px;">E-mail : scenario11@hanmail.net</span>
				</div>
				<h3 class="mar_tp10">한국영화배급협회</h3>
				<div class="tel_and_email mar_tp10">
					<span class="word_dian_bg">김의수 부장</span>
					<span class="word_dian_bg"style="margin-left: 50px;">Tel : 02-3452-1001</span>
					<span class="word_dian_bg"style="margin-left: 72px;">E-mail : webmaster@mdak.or.kr</span>
				</div>
			</div>
<%
		}
		// 방송
		else if( DIVS.equals("R")){
%>	
			<!-- 
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2012/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">방송저작물 저작권정보 변경<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국방송실연자협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>김주호 사무차장</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-784-7802</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>kbpa7802@chol.com</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			 -->
			 		 <!-- Tab str -->
                          <ul class="sub_menu1 w114 mar_tp30">
                           <!--    <li class="first"><a href="/mlsInfo/rghtInfo01.jsp">소개</a></li>
                              <li><a href="/mlsInfo/rghtInfo02.jsp">이용방법</a></li> -->
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악</a></li>
							  <li><a href="/rghtPrps/rghtSrch.do?DIVS=B">어문</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=N">뉴스</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화</a></li>
                              <li class="on"><a href="/rghtPrps/rghtSrch.do?DIVS=R" class="on">방송</a></li>
                      		</ul>
                      <!-- //Tab -->
                      <p class="clear"></p>
			 <div class="sub01_con_bg4_tp mar_tp30"></div>
			 <div class="sub01_con_bg4">
				<div class="font15"><span class="color_2c65aa">방송저작물 저작권 정보 변경</span>신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</div>
				<h3 class="mar_tp10">한국방송실연자협회</h3>
				<div class="tel_and_email mar_tp10">
					<span class="word_dian_bg">김주호 사무차장</span>
					<span class="word_dian_bg"style="margin-left: 23px;">Tel : 02-784-7802</span>
					<span class="word_dian_bg"style="margin-left: 80px;">E-mail : kbpa7802@chol.com</span>
				</div>
			</div>
<%
		}
		// 기타
		else if( DIVS.equals("X")){
%>	
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2012/content/box_img1.gif"/></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">기타저작물 저작권 정보 변경<strong class="blue2">신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국음악저작권협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>전종훈</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2660-0585</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>bigshow0411@komca.or.kr</span>
							</li>
							<li>
								<em class="w20">한국음악실연자연합회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>권기태 팀장</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2659-7048</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>kjery@fkmp.kr</span>
							</li>
							<li>
								<em class="w20">한국음반산업협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>김제경</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-3270-5933</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>kjk@riak.or.kr</span>
							</li>
							<li>
								<em class="w20">한국문예학술저작권협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>박남규</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-508-0440(대표)</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>png@ekosa.org</span>
							</li>
							<li>
								<em class="w20">한국복사전송권협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>김경채 대리</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2608-2036</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>abcchae@krtra.or.kr</span>
							</li>
							<li>
								<em class="w20">한국방송작가협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>김지숙 부장</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-782-1696</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>copyright@ktwa.or.kr</span>
							</li>
							<li>
								<em class="w20">한국영화배급협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>박혜영</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-3452-1008</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>master@mdak.or.kr</span>
							</li>
							<li>
								<em class="w20">한국시나리오작가협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>정지영 부장</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2275-0566(대표)</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>scenario11@hanmail.net</span>
							</li>
							<li>
								<em class="w20">한국영화제작가협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>권명화</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2267-9984</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>kfpa3@kfpa.net</span>
							</li>
							<li>
								<em class="w20">한국방송실연자협회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>김주호 사무차장</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-784-7802</span>
								<span class="w30"><img alt="e-Mail" src="/images/2012/common/ic_mail.gif"/>kbpa7802@chol.com</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
<%
		}		// 법정허락
		else if(DIVS.equals("MS")){
%>		
			<div class="gray_box">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img alt="" src="/images/2012/content/box_img1.gif"></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">법정허락 <strong class="blue2">신청에 관한 문의는 아래의 담당자에게 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국저작권위원회</em>
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>심의조사팀</span>
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>02-2660-0104</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>							
<%
		}
		// 방송음악
		else if( DIVS.equals("1") || DIVS.equals("B")){
%>		
			<!-- 
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img src="/images/2012/content/box_img1.gif" alt="" /></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">방송음악 미분배보상금<strong class="blue2">신청에 관한 문의는 제공 및 확인 기관에 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국음악실연자연합회</em>
								<span class="w20"><img src="/images/2012/common/ic_nm.gif" alt="이름" />권기태 팀장 </span>
								<span class="w25"><img src="/images/2012/common/ic_tel.gif" alt="Tel" />02-2659-7048</span>
								<span class="w30"><img src="/images/2012/common/ic_mail.gif" alt="e-Mail" />kjery@fkmp.kr</span>
							</li>
							<li>
								<em class="w20">한국음반산업협회</em>
								<span class="w20"><img src="/images/2012/common/ic_nm.gif" alt="이름" />김제경</span>
								<span class="w25"><img src="/images/2012/common/ic_tel.gif" alt="Tel" />02-3270-5933</span>
								<span class="w30"><img src="/images/2012/common/ic_mail.gif" alt="e-Mail" />kjk@riak.or.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			 -->
                      <!-- Tab str -->
                          <ul class="sub_menu1 w114 mar_tp30">
                              <li class="on"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1" class="on">음악</a></li>
							  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용</a></li>
                              <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관</a></li>
                              <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=4">수업목적</a></li>
                      		</ul>
                      <!-- //Tab -->
                      <p class="clear"></p>
                      
                       <div class="sub01_con_bg4_tp mar_tp30"></div>
						 <div class="sub01_con_bg4">
							<!-- <div class="font15"><span class="color_2c65aa">방송음악 미분배 보상금</span>신청에 관한 문의는 제공 및 확인 기관에 해당하는 단체로 연락바랍니다.</div> -->
							<div class="font15"><span class="color_2c65aa">방송음악 미분배 보상금</span>신청에 관한 문의는 제공 기관에 해당하는 단체로 연락바랍니다.</div>
							<!-- <h3 class="mar_tp10">한국음악실연자연합회(음실련)</h3> -->
							<h3 class="mar_tp10">한국음악실연자연합회(음실련)</h3>
							<div class="tel_and_email mar_tp10">
								<!-- <span class="word_dian_bg">권기태 팀장</span>
								<span class="word_dian_bg"style="margin-left: 50px;">Tel : 02-2659-7048</span>
								<span class="word_dian_bg"style="margin-left: 81px;">E-mail : kjery@fkmp.kr</span> -->
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>&nbsp;회원관리팀 </span>&nbsp;&nbsp;&nbsp;
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>&nbsp;02-2659-7215</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>&nbsp;전산분배팀</span>&nbsp;&nbsp;&nbsp;
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>&nbsp;02-2659-7067 </span><br>
							</div>
							<!-- <h3 class="mar_tp10">한국음반산업협회(음산협)</h3> -->
							<h3 class="mar_tp10">한국음반산업협회(음산협)</h3>
							<div class="tel_and_email mar_tp10">
								<!-- <span class="word_dian_bg">김제경</span>
								<span class="word_dian_bg"style="margin-left: 80px;">Tel : 02-3270-5933</span>
								<span class="word_dian_bg"style="margin-left: 80px;">E-mail : kjk@riak.or.kr</span> -->
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>&nbsp;콘텐츠팀</span>&nbsp;&nbsp;&nbsp;
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>&nbsp;02-3270-5933</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="w20"><img alt="이름" src="/images/2012/common/ic_nm.gif"/>&nbsp;정산분배팀</span>&nbsp;&nbsp;&nbsp;
								<span class="w25"><img alt="Tel" src="/images/2012/common/ic_tel.gif"/>&nbsp;02-3270-5961</span><br>
							</div>
						 </div>
<%
		}
		// 교과용(2,S)
		else if( DIVS.equals("2") || DIVS.equals("S")){
%>		
		<!-- 
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img src="/images/2012/content/box_img1.gif" alt="" /></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">교과용 미분배보상금<strong class="blue2">신청에 관한 문의는 아래의 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국복제전송저작권협회</em>
								<span class="w20"><img src="/images/2012/common/ic_nm.gif" alt="이름" />김나영</span>
								<span class="w25"><img src="/images/2012/common/ic_tel.gif" alt="Tel" />070-4265-2525</span>
								<span class="w30"><img src="/images/2012/common/ic_mail.gif" alt="e-Mail" />nayoung@korra.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		 -->
			<!-- Tab str -->
                  <ul class="sub_menu1 w114 mar_tp30">
                  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악</a></li>
				  <li class="on"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2" class="on">교과용</a></li>
                  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관</a></li>
                  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=4">수업목적</a></li>
                  </ul>
            <!-- //Tab -->
                  <p class="clear"></p>
                  
                  <div class="sub01_con_bg4_tp mar_tp30"></div>
						 <div class="sub01_con_bg4">
							<div class="font15"><span class="color_2c65aa">교과용 미분배 보상금</span>신청에 관한 문의는 제공 및 확인 기관에 해당하는 단체로 연락바랍니다.</div>
							<h3 class="mar_tp10">한국복제전송저작권협회</h3>
							<div class="tel_and_email mar_tp10">
								<span class="word_dian_bg">김나영</span>
								<span class="word_dian_bg"style="margin-left: 80px;">Tel : 070-4265-2525</span>
								<span class="word_dian_bg"style="margin-left: 73px;">E-mail : nayoung@korra.kr</span>
							</div>
						 </div> 
			
<%
		}
		// 도서관(3,L)
		else if( DIVS.equals("3") || DIVS.equals("L")){
%>
			<!-- 	
			<div class="gray_box mt5">
				<div class="box4">
					<div class="box4_con floatDiv">
						<p class="fl ml10"><img src="/images/2012/content/box_img1.gif" alt="" /></p>
						<div class="fl ml20 w85">
							<p class="strong mt5 black">도서관 미분배보상금 <strong class="blue2">신청에 관한 문의는 아래의 해당하는 단체로 연락바랍니다.</strong></p>
							<ul class="mt15 list2">
							<li>
								<em class="w20">한국복제전송저작권협회</em>
								<span class="w20"><img src="/images/2012/common/ic_nm.gif" alt="이름" />김광성 과장</span>
								<span class="w25"><img src="/images/2012/common/ic_tel.gif" alt="Tel" />070-4265-2522</span>
								<span class="w30"><img src="/images/2012/common/ic_mail.gif" alt="e-Mail" />kskim@korra.kr</span>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			 -->
			<!-- Tab str -->
                  <ul class="sub_menu1 w114 mar_tp30">
                  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악</a></li>
				  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용</a></li>
                  <li class="on"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3" class="on">도서관</a></li>
                  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=4">수업목적</a></li>
                  </ul>
            <!-- //Tab -->
                  <p class="clear"></p>
                  <div class="sub01_con_bg4_tp mar_tp30"></div>
						 <div class="sub01_con_bg4">
							<div class="font15"><span class="color_2c65aa">도서관 미분배 보상금</span>신청에 관한 문의는 제공 및 확인 기관에 해당하는 단체로 연락바랍니다.</div>
							<h3 class="mar_tp10">한국복제전송저작권협회</h3>
							<div class="tel_and_email mar_tp10">
								<span class="word_dian_bg">김광성 과장</span>
								<span class="word_dian_bg"style="margin-left: 50px;">Tel : 070-4265-2522</span>
								<span class="word_dian_bg"style="margin-left: 73px;">E-mail : kskim@korra.kr</span>
							</div>
						 </div>
<%
		}
		// 수업목적(4,A)
		else if( DIVS.equals("4") || DIVS.equals("A")){

%>
	<!-- Tab str -->
                  <ul class="sub_menu1 w114 mar_tp30">
                  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악</a></li>
				  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용</a></li>
                  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관</a></li>
                  <li class="on"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=4" class="on">수업목적</a></li>
                  </ul>
            <!-- //Tab -->
                  <p class="clear"></p>
                  <div class="sub01_con_bg4_tp mar_tp30"></div>
						 <div class="sub01_con_bg4">
							<div class="font15"><span class="color_2c65aa">수업목적 미분배 보상금</span>신청에 관한 문의는 제공 및 확인 기관에 해당하는 단체로 연락바랍니다.</div>
							<h3 class="mar_tp10">한국복제전송저작권협회</h3>
							<div class="tel_and_email mar_tp10">
								<span class="word_dian_bg">김광성 과장</span>
								<span class="word_dian_bg"style="margin-left: 50px;">Tel : 070-4265-2522</span>
								<span class="word_dian_bg"style="margin-left: 73px;">E-mail : kskim@korra.kr</span>
							</div>
						 </div>
<%
		}

%>
<%
	}
}
%>