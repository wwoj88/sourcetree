<%@ page contentType="text/html;charset=euc-kr" %>
<%
String DIVS = request.getParameter("DIVS");
%>
<!-- 
<div class="contentsRoundBox" >
	<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
	<div class="infoImg2">
		<p><strong>음악저작물 권리찾기 신청에 관란 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
		<ul>
			<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
			<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
			<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
		</ul>
	</div>
</div>
 -->
 
 <!-- 
<fieldset class="sch  mt0">
	<div class="contentsSch  mt0">
		<span class="round lb"></span><span class="round rb"></span><span class="round lt"></span><span class="round rt"></span>
		
		<div class="infoImg2">
			<p><strong>음악저작물 권리찾기 신청에 관란 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
			<ul>
				<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
				<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
				<li>한국음악저작권협회  <span class="blue">김담당 주임</span> <span class="blue fontSmall"> (02)344-4444</span>
			</ul>
		</div>	
	</div>
</fieldset>
 -->

<%
if(DIVS!=null || DIVS.length()>0){
	
	 //out.println("[ "+DIVS+" ]");
	
	if(DIVS.equals("M") || DIVS.equals("X")
		|| DIVS.equals("1") || DIVS.equals("B")
		|| DIVS.equals("2") || DIVS.equals("S")
		|| DIVS.equals("3") || DIVS.equals("L")
		|| DIVS.equals("C") || DIVS.equals("I")
		|| DIVS.equals("V") || DIVS.equals("R")){
%> 
 <fieldset class="sch mt5">
 	<legend>안내문의</legend>
	<div class="contentsSch">
		<span class="round lb"></span><span class="round rb"></span><span class="round lt"></span><span class="round rt"></span>
		
		<div class="infoImg2 pb20 pt15">

<%
		// 음악
		if(DIVS.equals("M")){
%>		
			<p><strong>음악저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
			<table summary="음악저작물 저작권찾기 신청문의 안내">
				<colgroup>
					<col width="29%">
					<col width="15%">
					<col width="27%">
					<col width="*%">
				</colgroup>
				<tbody>
					<tr>
						<th>한국음악저작권협회</th>
						<td>전종훈</td>
						<td><span class="gray strong">☏</span> <span class="">02-2660-0585</span></td>
						<td><span class="gray strong">Email</span> <span class="">bigshow0411@komca.or.kr</span></td>
					</tr>
					<tr>
						<th>한국음반산업협회</th>
						<td>최소정</td>
						<td><span class="gray strong">☏</span> <span class="">02-3270-5933</span></td>
						<td><span class="gray strong">Email</span> <span class="">csj@kapp.or.kr</span></td>
					</tr>
					<tr>
						<th>한국음악실연자연합회</th>
						<td>노현주</td>
						<td><span class="gray strong">☏</span> <span class="">02-745-8286(내선번호:1)</span></td>
						<td><span class="gray strong">Email</span> <span class="">hcfliegen@fkmp.kr</span></td>
					</tr>
				</tbody>
			</table>
<%
		}
		// 도서
		else if( DIVS.equals("B")){
%>	
			<p><strong>도서저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
			<table summary="도서저작물 저작권찾기 신청문의 안내">
				<colgroup>
					<col width="31%">
					<col width="15%">
					<col width="20%">
					<col width="*%">
				</colgroup>
				<tbody>
					<tr>
						<th>한국문예학술저작권협회</th>
						<td>김은정</td>
						<td><span class="gray strong">☏</span> <span class="">02-508-0440</span></td>
						<td><span class="gray strong">Email</span> <span class="">kej0512@copyrightkorea.org </span></td>
					</tr>
					<tr>
						<th>한국복사전송권협회</th>
						<td>김경채 대리</td>
						<td><span class="gray strong">☏</span> <span class="">02-2608-2036</span></td>
						<td><span class="gray strong">Email</span> <span class="">abcchae@krtra.or.kr</span></td>
					</tr>
				</tbody>
			</table>
<%
		}
		// 방송대본
		else if( DIVS.equals("C")){
%>	
			<p><strong>방송대본저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
			<table summary="방송대본저작물 저작권찾기 신청문의 안내">
				<colgroup>
					<col width="30%">
					<col width="18%">
					<col width="23%">
					<col width="*%">
				</colgroup>
				<tbody>
					<tr>
						<th>한국방송작가협회</th>
						<td>윤선미</td>
						<td><span class="gray strong">☏</span> <span class="">02-782-1696</span></td>
						<td><span class="gray strong">Email</span> <span class="">office@ktrwa.or.kr </span></td>
					</tr>
					<tr>
						<th>한국시나리오작가협회</th>
						<td>이윤성</td>
						<td><span class="gray strong">☏</span> <span class="">02-2275-0566</span></td>
						<td><span class="gray strong">Email</span> <span class="">scenario11@hanmail.net</span></td>
					</tr>
				</tbody>
			</table>
<%
		}
		// 이미지
		else if( DIVS.equals("I")){
%>	
			<p><strong>이미지저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
			<table summary="이미지저작물 저작권찾기 신청문의 안내">
				<colgroup>
					<col width="32%">
					<col width="13%">
					<col width="20%">
					<col width="*%">
				</colgroup>
				<tbody>
					<tr>
						<th>한국문예학술저작권협회</th>
						<td>김은정</td>
						<td><span class="gray strong">☏</span> <span class="">02-508-0440</span></td>
						<td><span class="gray strong">Email</span> <span class="">kej0512@copyrightkorea.org </span></td>
					</tr>
					<tr>
						<th>한국복사전송권협회</th>
						<td>김경채 대리</td>
						<td><span class="gray strong">☏</span> <span class="">02-2608-2036</span></td>
						<td><span class="gray strong">Email</span> <span class="">abcchae@krtra.or.kr</span></td>
					</tr>
				</tbody>
			</table>	
<%
		}
		// 영화
		else if( DIVS.equals("V")){
%>	
			<p><strong>영화저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
			<table summary="영화저작물 저작권찾기 신청문의 안내">
				<colgroup>
					<col width="30%">
					<col width="19%">
					<col width="20%">
					<col width="*%">
				</colgroup>
				<tbody>
					<tr>
						<th>한국영화제작가협회</th>
						<td>정재훈</td>
						<td><span class="gray strong">☏</span> <span class="">02-2267-9983</span></td>
						<td><span class="gray strong">Email</span> <span class="">kfpa2@kfpa.net</span></td>
					</tr>
					<tr>
						<th>한국시나리오작가협회</th>
						<td>이윤성</td>
						<td><span class="gray strong">☏</span> <span class="">02-2275-0566</span></td>
						<td><span class="gray strong">Email</span> <span class="">scenario11@hanmail.net</span></td>
					</tr>
					<tr>
						<th>한국영화배급협회</th>
						<td>김대용 대리</td>
						<td><span class="gray strong">☏</span> <span class="">02-3452-1003</span></td>
						<td><span class="gray strong">Email</span> <span class="">webmaster@mdak.or.kr</span></td>
					</tr>
				</tbody>
			</table>	
<%
		}
		// 방송
		else if( DIVS.equals("R")){
%>	
			<p><strong>방송저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
			<table summary="방송저작물 저작권찾기 신청문의 안내">
				<colgroup>
					<col width="33%">
					<col width="18%">
					<col width="22%">
					<col width="*%">
				</colgroup>
				<tbody>
					<tr>
						<th>한국방송실연자협회</th>
						<td>육세영 선임</td>
						<td><span class="gray strong">☏</span> <span class="">02-784-7802</span></td>
						<td><span class="gray strong">Email</span> <span class="">kbpa7802@chol.com</span></td>
					</tr>
				</tbody>
			</table>	
<%
		}
		// 기타
		else if( DIVS.equals("X")){
%>	
			<p><strong>기타저작물 저작권찾기 신청에 관한 문의는 신청 권리구분에 해당하는 단체로 연락바랍니다.</strong></p>
			<table summary="기타저작물 저작권찾기 신청문의 안내">
				<colgroup>
					<col width="29%">
					<col width="17%">
					<col width="20%">
					<col width="*%">
				</colgroup>
				<tbody>
					<tr>
						<th>한국음악저작권협회</th>
						<td>황혜림</td>
						<td><span class="gray strong">☏</span> <span class="">02-2660-0584</span></td>
						<td><span class="gray strong">Email</span> <span class="">tnlover07@hotmail.com</span></td>
					</tr>
					<tr>
						<th>한국음악실연자연합회</th>
						<td>민부영 대리</td>
						<td><span class="gray strong">☏</span> <span class="">02-745-8286</span></td>
						<td><span class="gray strong">Email</span> <span class="">prettyowl@fkmp.kr </span></td>
					</tr>
					<tr>
						<th>한국음반산업협회</th>
						<td> 김선영 </td>
						<td><span class="gray strong">☏</span> <span class="">02-3270-5933</span></td>
						<td><span class="gray strong">Email</span> <span class="">ksy@riak.or.kr</span></td>
					</tr>
					<tr>
						<th>한국문예학술저작권협회</th>
						<td>김은정</td>
						<td><span class="gray strong">☏</span> <span class="">02-508-0440</span></td>
						<td><span class="gray strong">Email</span> <span class="">kej0512@copyrightkorea.org </span></td>
					</tr>
					<tr>
						<th>한국복사전송권협회</th>
						<td>김경채 대리</td>
						<td><span class="gray strong">☏</span> <span class="">02-2608-2036</span></td>
						<td><span class="gray strong">Email</span> <span class="">abcchae@krtra.or.kr</span></td>
					</tr>
					<tr>
						<th>한국방송작가협회</th>
						<td>윤선미</td>
						<td><span class="gray strong">☏</span> <span class="">02-782-1696</span></td>
						<td><span class="gray strong">Email</span> <span class="">office@ktrwa.or.kr </span></td>
					</tr>
					<tr>
						<th>한국영화배급협회</th>
						<td>박혜영</td>
						<td><span class="gray strong">☏</span> <span class="">02-3452-1008</span></td>
						<td><span class="gray strong">Email</span> <span class="">master@mdak.or.kr</span></td>
					</tr>
					<tr>
						<th>한국시나리오작가협회</th>
						<td>정지영 총무</td>
						<td><span class="gray strong">☏</span> <span class="">02-2275-0566</span></td>
						<td><span class="gray strong">Email</span> <span class="">scenario11@hanmail.net</span></td>
					</tr>
					<tr>
						<th>한국영화제작가협회</th>
						<td>정재훈</td>
						<td><span class="gray strong">☏</span> <span class="">02-2267-9983</span></td>
						<td><span class="gray strong">Email</span> <span class="">kfpa2@kfpa.net</span></td>
					</tr>
					<tr>
						<th>한국방송실연자협회</th>
						<td>육세영 선임</td>
						<td><span class="gray strong">☏</span> <span class="">02-784-7802</span></td>
						<td><span class="gray strong">Email</span> <span class="">kbpa7802@chol.com</span></td>
					</tr>
					<tr>
						<th>한국언론진흥재단</th>
						<td>미래기술팀</td>
						<td><span class="gray strong">☏</span> <span class="">02-2001-7782</span></td>
						<td><span class="gray strong">Email</span> <span class="">dnc@kpf.or.kr</span></td>
					</tr>
				</tbody>
			</table>							
<%
		}
		// 방송음악
		else if( DIVS.equals("1") || DIVS.equals("B")){
%>		
			<p><strong>방송음악보상금 신청에 관한 문의는 신청분류에 해당하는 단체로 연락바랍니다.</strong></p>
			<table summary="방송음악 보상금 신청문의 안내">
				<colgroup>
					<col width="30%">
					<col width="14%">
					<col width="28%">
					<col width="*%">
				</colgroup>
				<tbody>
					<tr>
						<th>한국음악실연자연합회</th>
						<td>권기태 팀장</td>
						<td><span class="gray strong">☏</span> <span class="">02-745-8286(6220)</span></td>
						<td><span class="gray strong">Email</span> <span class="">kjery@fkmp.kr</span></td>
					</tr>
					<tr>
						<th>한국음원제작자협회</th>
						<td> 최소정 </td>
						<td><span class="gray strong">☏</span> <span class="">02-3270-5933</span></td>
						<td><span class="gray strong">Email</span> <span class="">csj@kapp.or.kr</span></td>
					</tr>
				</tbody>
			</table>
<%
		}
		// 교과용(2,S)
		else if( DIVS.equals("2") || DIVS.equals("S")){
%>		
			<p><strong>교과용보상금 신청에 관한 문의는 신청분류에 해당하는 단체로 연락바랍니다.</strong></p>
			<table summary="교과용 보상금 신청문의 안내">
				<colgroup>
					<col width="32%">
					<col width="14%">
					<col width="26%">
					<col width="*%">
				</colgroup>
				<tbody>
					<tr>
						<th>한국복제전송저작권협회</th>
						<td>김나영 대리</td>
						<td><span class="gray strong">☏</span> <span class="">02-2608-2800(대표)</span></td>
						<td><span class="gray strong">Email</span> <span class="">nayoung@korra.kr</span></td>
					</tr>
				</tbody>
			</table>
<%
		}
		// 도서관(3,L)
		else if( DIVS.equals("3") || DIVS.equals("L")){
%>		
			<p><strong>도서관보상금 신청에 관한 문의는 신청분류에 해당하는 단체로 연락바랍니다.</strong></p>
			<table summary="도서관 보상금 신청문의 안내">
				<colgroup>
					<col width="32%">
					<col width="14%">
					<col width="30%">
					<col width="*%">
				</colgroup>
				<tbody>
					<tr>
						<th>한국복제전송저작권협회</th>
						<td>김경채 과장</td>
						<td><span class="gray strong">☏</span> <span class="">02-2608-2800(대표)</span></td>
						<td><span class="gray strong">Email</span> <span class="">abcchae@korra.kr</span></td>
					</tr>
				</tbody>
			</table>
<%
		}
%>
			
		</div>	
	</div>
</fieldset>

<%
	}
}
%>