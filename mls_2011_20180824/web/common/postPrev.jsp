<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String title = request.getParameter("title");
	String postUrl = request.getParameter("postUrl");
%>

<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>영화포스터 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/popup.css">
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop">
	
		<!-- Header str -->
		<div id="popHeader">
			<h1>이미지 미리보기</h1>
		</div>
		<!-- //Header end -->
		
		<!-- Container str -->
		<div id="popContainer">
			
			<div class="popContent" >
				<div class="tabelRound">	
					<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<colgroup>
						<col width="30%">
						<col width="70%">
						</colgroup>
						<tbody>
							<tr>
								<th class="bgbr lft">영화명</th>
								<td><%=title%></td>
							</tr>
							<tr>
								<td colspan="2"><img src="<%=postUrl%>" alt="<%=title%>" /></td>
							</tr>
						</tbody>		
					</table>
				</div>
				
			</div>
			
		</div>
		<!-- //Container end -->
		
		<!-- Footer str -->
		<div id="popFooter">
			<p class="copyright" style="font-size:11px;">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		</div>
		<!-- Footer end -->
		
		<p class="close"><a href="#1" onclick="javascript:window.close()" ><img src="/images/2010/pop/close.gif" alt="" title="이 창을 닫습니다."/></a></p>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
