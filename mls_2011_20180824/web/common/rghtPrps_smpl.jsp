<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권 찾기 신청 예시화면 보기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/deScript.js"></script>
<!--

//-->
</script>
</head>
<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
		<c:choose>
		  <c:when test="${param.DVI == 'APPLY'}">
			<h1>개인정보 동의서</h1>
		  </c:when>
		  <otherwise>
			<h1>예시화면 보기</h1>
    		</otherwise>
		 </c:choose>
		</div>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<div class="section">
				<!-- 음악 -->
				<c:if test="${param.DVI == 'MR'}" >
					<img src="/images/2011/smpl/m_01.jpg" alt="" />
				</c:if>
				<c:if test="${param.DVI == 'MU'}" >
					<img src="/images/2011/smpl/m_02.jpg" alt="" />
				</c:if>
				<!-- 도서 -->
				<c:if test="${param.DVI == 'BR'}" >
					<img src="/images/2011/smpl/b_01.jpg" alt="" />
				</c:if>
				<c:if test="${param.DVI == 'BU'}" >
					<img src="/images/2011/smpl/b_02.jpg" alt="" />
				</c:if>
				<!-- 방송대본 -->
				<c:if test="${param.DVI == 'CR'}" >
					<img src="/images/2011/smpl/c_01.jpg" alt="" />
				</c:if>
				<c:if test="${param.DVI == 'CU'}" >
					<img src="/images/2011/smpl/c_02.jpg" alt="" />
				</c:if>
				<!-- 이미지 -->
				<c:if test="${param.DVI == 'IR'}" >
					<img src="/images/2011/smpl/i_01.jpg" alt="" />
				</c:if>
				<c:if test="${param.DVI == 'IU'}" >
					<img src="/images/2011/smpl/i_02.jpg" alt="" />
				</c:if>
				<!-- 영화 -->
				<c:if test="${param.DVI == 'VR'}" >
					<img src="/images/2011/smpl/v_01.jpg" alt="" />
				</c:if>
				<c:if test="${param.DVI == 'VU'}" >
					<img src="/images/2011/smpl/v_02.jpg" alt="" />
				</c:if>
				<!-- 방송 -->
				<c:if test="${param.DVI == 'RR'}" >
					<img src="/images/2011/smpl/r_01.jpg" alt="" />
				</c:if>
				<c:if test="${param.DVI == 'RU'}" >
					<img src="/images/2011/smpl/r_02.jpg" alt="" />
				</c:if>
				<!-- 방송 -->
				<c:if test="${param.DVI == 'XR'}" >
					<img src="/images/2011/smpl/x_01.jpg" alt="" />
				</c:if>
				<c:if test="${param.DVI == 'XU'}" >
					<img src="/images/2011/smpl/x_02.jpg" alt="" />
				</c:if>
				<!-- 뉴스 -->
				<c:if test="${param.DVI == 'NR'}" >
					<img src="/images/2011/smpl/n_01.jpg" alt="" />
				</c:if>
				<c:if test="${param.DVI == 'NU'}" >
					<img src="/images/2011/smpl/n_02.jpg" alt="" />
				</c:if>
				
				<!-- 보상금 방송음악 -->
				<c:if test="${param.DVI == 'INMT01'}" >
					<img src="/images/2011/smpl/inmt01.jpg" alt="" />
				</c:if>
				<!-- 보상금 교과 -->
				<c:if test="${param.DVI == 'INMT02'}" >
					<img src="/images/2011/smpl/inmt02.jpg" alt="" />
				</c:if>
				<!-- 보상금 도서관 -->
				<c:if test="${param.DVI == 'INMT03'}" >
					<img src="/images/2011/smpl/inmt03.jpg" alt="" />
				</c:if>
				
				<!-- 법정허락 -->
				<c:if test="${param.DVI == 'MS01'}" >
					<img src="/images/2011/smpl/ms01_01.jpg" alt="" />
				</c:if>
				<!-- 법정허락 -->
				<c:if test="${param.DVI == 'MS02'}" >
					<img src="/images/2011/smpl/ms01_02.jpg" alt="" />
				</c:if>
				<!-- 법정허락 -->
				<c:if test="${param.DVI == 'MS11'}" >
					<img src="/images/2011/smpl/ms02.jpg" alt="" />
				</c:if>
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close()" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="" /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
