<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ page contentType="text/html;charset=euc-kr"%>
<%
	
	String sform = request.getParameter("sform");
	String sfName = request.getParameter("fname");
	String sfFullName = request.getParameter("ffullname");
%>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>권리찾기사이트</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/JavaScript" src="/js/rollover.js"></script>
<script type="text/javascript" src="/js/common/File.js"></script>
<script type="text/javascript" src="/js/common/Function.js"></script>
<script type="text/javascript">
	function fnCheck(){
		var df = document.forms[0];
		var iFlag = 0;

		if(df.file0.value!=""){
			iFlag = 1;
		}

		if (iFlag!=1) {
			alert("업로드할 파일을 선택하세요");
			df.file0.focus();
			return;
		}

		df.action = "common_file_check2.jsp";
		df.submit();

	}

	// 팝업창 사이즈 조절
	function getfocus(){
			//window.resizeTo(440, 276);			// 한개 파일 업로드 할때 사이즈
	}

</script>

</head>

<body onload="javascript:getfocus();">
<form name="frm" enctype="multipart/form-data" method="post">
<input type="submit" style="display:none;">
<table width="440" height="276" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="3" height="74" class="pop_top_bg" valign="bottom">파일첨부</td>
		</tr>
		<tr>
			<td width="24"><img src="/images/popup/left.gif" alt=""></td>
			<td width="391" align="center" valign="top" class="T_spc15">
				<table width="350" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td height="1" class="pop_dot_bg"></td>
					</tr>
					<tr>
						<td height="40" class="pop_td_bg" align="center"><input class="input" title="" style="width:90%;" name="file0" type="file"></td>
					</tr>
					<tr>
						<td height="1" class="pop_dot_bg"></td>
					</tr>
					<tr>
						<td height="45" valign="bottom" align="center"><a href="#1" onclick="javascript:fnCheck();" ><img src="/images/popup/btn_ok.gif" alt=""></a><a href="#1" onclick="javascript:self.close();"><img src="/images/popup/btn_cancel2.gif" alt=""></a></td>
					</tr>
				</table>
			</td>
			<td width="25"><img src="/images/popup/right.gif" alt=""></td>
		</tr>
		<tr>
			<td colspan="3" height="41" align="right" valign="bottom" class="pop_bottom_bg"><a href="#1" onclick="javascript:window.close();" ><img src="/images/popup/close.gif" alt=""></a></td>
		</tr>
	</table>

<!-- HIDDEN STR -->
<input type="hidden" name="fileupcount" value="1">
<input type="hidden" name="sform" value="<%=sform%>">
<input type="hidden" name="fname" value="<%=sfName%>">
<input type="hidden" name="ffullname" value="<%=sfFullName%>">
<!-- HIDDEN END -->

</form>
</body>
</html>
