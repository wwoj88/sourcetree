<%@ page contentType="text/html; charset=euc-kr" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.net.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%!
public String getFileExist(String strUrl) {
	 URL url = null;
	 URLConnection con = null;
	 String strImgPath = "";
	 String sResult = "0";
	try {
		url = new URL(strUrl);
		con = url.openConnection();
		java.net.HttpURLConnection http = (java.net.HttpURLConnection)con;
		// 결과값 파일 있을때 200 리턴
		// 결과값 파일 없을때 404 리턴

		if(http.getResponseCode() == 404){
    		strImgPath = "";
    		sResult = "1";
   		}else if(http.getResponseCode() == 200){
    		strImgPath = strUrl;
    		sResult = "0";
   		}
	}catch(Exception e){
	}
	return sResult;
}
%>

<%
	String mp3Url = request.getParameter("mp3Url");
	if( getFileExist(mp3Url).equals("1")){
%>
		<script type="text/JavaScript">
			alert("파일이 존재하지 않습니다.");
			window.close();
		</script>
<%
	}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<title>음원샘플 미리듣기 | 내권리찾기</title>
<script type="text/javascript">

	function onload(){
		document.getElementById("mp3").innerHTML = '<embed src="<%=mp3Url%>" width="290" height="40" autostart="ture"></embed>';
		goClose()
	}
	
	function goClose(){	  
	 	setTimeout('self.close()', 31000); 	  
	}
</script>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/deScript.js"></script>
</head>
<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>음원샘플 미리듣기</h1>
		</div>
		<!-- //HEADER end -->
		<!-- CONTAINER str-->
		<div id="popContents">
			
			<div class="section ce">
				<div id = "mp3" ></div>
			</div>
			
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" class="pop_close"><img src="/images/2011/button/pop_close.gif" alt="" /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>

