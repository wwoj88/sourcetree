<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.File" %>
<%@ page import="com.oreilly.servlet.MultipartRequest" %>
<%@ page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>
<%@ page import="kr.or.copyright.mls.support.util.FileUtilRenamePolicy"%>
<%@ page import="kr.or.copyright.mls.support.util.FileUploadUtil" %>
<%@ page import="org.apache.log4j.Logger"%> <!-- LOG사용설정 -->
<%! private Logger log = Logger.getLogger(this.getClass()); %>

<%@ page contentType="text/html; charset=euc-kr"%>
<%
	
	int iCount = 0;
	if (application.getAttribute("fileupcount")==null)
		application.setAttribute("fileupcount", "1");
	else {
		iCount = Integer.parseInt((String)application.getAttribute("fileupcount"));
		iCount++;
		application.setAttribute("fileupcount", ""+(iCount));
	}

	String sform = "";
	String sFileName = "";

	String sfName = "";
	String sfFullName  = "";
	
	String gubun="";
	String chghistoryseqno="";
	int chgfileseqno= 0;
	
	File UpFile;
	String savePath = FileUploadUtil.getAbsoluteFilePath("");				// 저장경로에 폴더 생성
	String relationPath = FileUploadUtil.getRelactionFilePath();
	String sSaveFileName = "";
	String sOriFileName = "";
	String sFileExt = "";
	long FileSize;
	int iFileSeqno = 0;
	int gSizeLimit = 52428800	;
	String file_jobgubun ="0";
%>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>권리찾기사이트</title>
<script language="JavaScript" type="text/JavaScript">

function goClose(){
 	setTimeout('self.close()', 1500);
}
</script>
</head>

<body>
<form name="frm" method="post">
<input type="submit" style="display:none;">
<table width="440" height="276" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="3" height="74" class="pop_top_bg" valign="bottom">파일첨부</td>
		</tr>
		<tr>
			<td width="24"><img src="/images/popup/left.gif" alt=""></td>
			<td width="391" align="center" valign="top" class="T_spc15">
				<table width="350" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td height="1" class="pop_dot_bg"></td>
					</tr>
					<tr>
						<td height="40" class="pop_td_bg" align="center">

<%
		if(savePath==null){
		out.print("<span class='tit_01'>파일이 업로드 되지 않았습니다.(파일저장 위치 생성실패)</span>");

	} else {		//savePath 위치에 파일 저장하기
		try{
	MultipartRequest multi=new MultipartRequest(request, savePath, gSizeLimit, "euc-kr", new FileUtilRenamePolicy());
	sform = multi.getParameter("sform");
	sfName = multi.getParameter("fname");
	sfFullName = multi.getParameter("ffullname");
	
	for (int i = 0; i < Integer.parseInt(multi.getParameter("fileupcount")); i++) {
		if (multi.getFilesystemName("file"+i) == null) continue;
		sFileName = multi.getFilesystemName("file"+i);
		sOriFileName = multi.getOriginalFileName("file"+i);
		UpFile = multi.getFile("file"+i);
		FileSize = UpFile.length();


		if (sFileName.lastIndexOf(".")>=0)	{ // 첨부파일 확장자 확장자
	sFileExt = sFileName.substring(sFileName.lastIndexOf(".")+1, sFileName.length());
	sFileExt = sFileExt.toLowerCase();
		}

		if(sFileName == null) {
	out.print("<span class='tit_01'>파일업로드 실패</span>");

		} else {

	boolean result	=false;

	try {
		sSaveFileName = FileUploadUtil.makeFileName(sOriFileName, "");
		result = UpFile.renameTo(new File(savePath+"/"+sSaveFileName));
	} catch(Exception e){
		out.println(e);
		System.out.println(e);
		log.error(e);
	}

	if (result) {
%>
						<script>

  						i = opener.document.<%=sform%>.<%=sfName%>.length;
  						opener.document.<%=sform%>.<%=sfName%>.length++;
					// opener.document.<%//=sform%>.<%//=sfName%>.options[i].value= '<%//=sOriFileName+"^"+savePath+relationPath+"/"+sSaveFileName+"^"+FileSize%>';
  						opener.document.<%=sform%>.<%=sfName%>.options[i].value= '<%=sOriFileName+"^"+savePath+"/"+"^"+sSaveFileName+"^"+FileSize%>';
  						opener.document.<%=sform%>.<%=sfName%>.options[i].text= '<%=sOriFileName%>';
					
						goClose();
						</script>
					<%
						out.println("<span class='tit_01'>파일첨부 성공</span>");
					} else {
						out.println("<span class='tit_01'>파일첨부 실패</span>");
					}
				}
			}
		} catch(IOException e){
			out.println(e.getMessage());
			log.error(e);
		} catch(Exception e) {
			out.println(e.getMessage());
			log.error(e);
		}
	}
%>
				</td>
					</tr>
					<tr>
						<td height="1" class="pop_dot_bg"></td>
					</tr>
					<tr>
						<td height="45" valign="bottom" align="center"><a href="#1" onclick="javascript:window.close();"><img src="/images/popup/btn_ok.gif" alt=""></a></td>
					</tr>
				</table>
			</td>
			<td width="25"><img src="/images/popup/right.gif" alt=""></td>
		</tr>
		<tr>
			<td colspan="3" height="41" align="right" valign="bottom" class="pop_bottom_bg"><a href="#1" onclick="javascript:window.close();"><img src="/images/popup/close.gif" alt=""></a></td>
		</tr>
	</table>

</form>

</body>
</html>
