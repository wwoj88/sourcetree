<%@ page contentType="text/html; charset=euc-kr" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.net.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%!
public String getFileExist(String strUrl) {
	 URL url = null;
	 URLConnection con = null;
	 String strImgPath = "";
	 String sResult = "0";
	try {
		url = new URL(strUrl);
		con = url.openConnection();
		java.net.HttpURLConnection http = (java.net.HttpURLConnection)con;
		// 결과값 파일 있을때 200 리턴
		// 결과값 파일 없을때 404 리턴

		if(http.getResponseCode() == 404){
    		strImgPath = "";
    		sResult = "1";
   		}else if(http.getResponseCode() == 200){
    		strImgPath = strUrl;
    		sResult = "0";
   		}
	}catch(Exception e){
	}
	return sResult;
}
%>

<%
	String mp3Url = request.getParameter("mp3Url");
	if( getFileExist(mp3Url).equals("1")){
%>
		<script type="text/JavaScript">
			alert("파일이 존재하지 않습니다.");
			window.close();
		</script>
<%
	}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<link type="text/css" rel="stylesheet" href="/css/2010/popup.css"/>
<title>저작권 찾기</title>
<script type="text/javascript">

	function onload(){
		document.getElementById("mp3").innerHTML = '<embed src="<%=mp3Url%>" width="290" height="40" autostart="ture"></embed>';
		goClose()
	}
	
	function goClose(){	  
	 	setTimeout('self.close()', 31000); 	  
	}
</script>

</head>
<body onload="javascript:onload()">

<!-- 전체를 감싸는 DIVISION -->
	<div id="pop">
	
		<!-- Header str -->
		<div id="popHeader">
			<h1>샘플듣기</h1>
		</div>
		<!-- //Header end -->
		
		<!-- Container str -->
		<div id="popContainer">
			
			<div class="popContent" >
		
				<div id = "mp3" ></div>
		
			</div>
		</div>	
		<!-- //Container end -->
		
		<!-- Footer str -->
		<div id="popFooter">
			<p class="copyright" style="font-size:11px;">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		</div>
		<!-- Footer end -->
		
		<p class="close"><a href="javascript:window.close()"><img src="/images/2010/pop/close.gif" alt="" title="이 창을 닫습니다."/></a></p>
	
	</div>
</body>
</html>

