/*------------------------------------------------------
 * 도움말 팝업
 * @param file : file name
-------------------------------------------------------*/
function openhelp(file)
{
   window.open(file, "popup", "menubar=no, resize=no, scrollbars=no, width=400, height=450");
}

/*------------------------------------------------------
 * 결과내재검색
 * @param
-------------------------------------------------------*/
function resrch_chk()
{
   var fnm = document.search;          
   if(fnm.resrch_check.checked) {            
    	fnm.resrch.value = "yes";
    }else{
	    fnm.resrch.value = "";
	}	
}

/*------------------------------------------------------
 * 검색
-------------------------------------------------------*/
function goSearch(){
	
    var fnm = document.search;
    var query=fnm.query.value;
    query=query.replace(/^\s+/,""); 

    if (query == "" || query == null) {
        alert("Search for is required.");
        return;
    }
		/*
    setTimeout(loadSearch,10);		//프로그레스 호출
    
    setLatestQuery("MYQUERY",fnm.query.value);	// Cookie 저장(내가찾은 검색어)
    
    rangeValue="";
    for(i=0; i<fnm.range.length; i++){    	
    	if(fnm.range[i].checked){    		
    		if(rangeValue!="") rangeValue = rangeValue+",";
    		rangeValue = rangeValue+fnm.range[i].value;    		    		
    	}
    }
    
    fnm.arr_range.value = rangeValue;    
    */
    fnm.submit();
    return false;
}

/*------------------------------------------------------
 * 인기검색어,내가 찾은 검색어
-------------------------------------------------------*/
function favoriteMyQuery(query){
	var fnm = document.search;
	fnm.query.value=query;
	fnm.startDate.value="";
	fnm.endDate.value="";
	setLatestQuery("MYQUERY",fnm.query.value);	// Cookie 저장
	fnm.submit();
	return false;
}

/*------------------------------------------------------
 * 더 많은 검색결과 보기
 * @param page : page number
 * @param target : target name
-------------------------------------------------------*/
function moreResult(page,target,resrch) {

	//setTimeout(loadSearch,10);		//프로그레스 호출	
	
	  var fnm = document.search;
	    if (document.search ) {
	    	fnm.page.value = page;
	    	fnm.target.value = target;
	    	fnm.resrch.value = resrch;
	    	
	    	if( resrch == "yes") fnm.resrch.checked=true;
	    	fnm.mode.value = "paging";
	    	
	    	fnm.submit();    	
	      return false;
	    }
}

/*------------------------------------------------------
 * 페이징 
 * @param page : page number
-------------------------------------------------------*/
function changePage(page,resrch) {
	
	//setTimeout(loadSearch,10);		//프로그레스 호출
	
	  var fnm = document.search;
	    
	 if( resrch == "yes") fnm.resrch.checked=true;
	 fnm.mode.value = "paging";
	 fnm.page.value = page;

	 fnm.submit();
}	

/*------------------------------------------------------
 * 검색 프로그레스 호출 및 숨기기 
 * @param page : page number
-------------------------------------------------------*/
function loadSearch() 
{
  if (document.getElementById) 
  {  
    document.getElementById('progress').style.left = document.body.clientWidth/2 - 150;
    document.getElementById('progress').style.top = document.body.scrollTop + (document.body.clientHeight/2)/2;
    document.getElementById('progress').style.visibility = 'visible';
  } 
  else 
  {
    if (document.layers) 
    {  
      document.progress.left = document.body.clientWidth/2 - 150;
      document.progress.visibility = 'visible';
    } 
    else 
    {  
      document.all.progress.style.left = document.body.clientWidth/2 - 150;
      document.all.progress.style.top = document.body.scrollTop + (document.body.clientHeight/2)/2;
      document.all.progress.style.visibility = 'visible';
    }
  }
}

/*------------------------------------------------------
 * input 창에서 엔터키로 검색
 * @param page :keyCode 
-------------------------------------------------------*/
 function CheckEnt(obj)
 {
 	if (event.keyCode == 13)
 	{
 		goSearch();
 	}
 }
 
 /*------------------------------------------------------
 * 달력에서 사용하는 메소드
 * @param page : date
-------------------------------------------------------*/
 function padZero(s)
{
		return (""+s).length<2?"0"+s:s;
}

function show(object) {
	if (document.layers && document.layers[object] != null) document.layers[object].visibility = 'visible';
	else if (document.all) document.all[object].style.visibility = 'visible';
}

function hide(object) {
	if (document.layers && document.layers[object] != null) document.layers[object].visibility = 'hidden';
	else if (document.all) document.all[object].style.visibility = 'hidden';
}

/*------------------------------------------------------
 * 체크박스 그룹별 선택, 해제
 * @param num
-------------------------------------------------------*/
function check(num) {	
	for (var i = 0; i < document.search.range.length; i++) {		
		var anchor = document.search.range[i];
		var relAttribute = String(anchor.getAttribute('rel'));		
		if(relAttribute!=num) {
			anchor.checked=false;
		}
	}
}

/*------------------------------------------------------
 * POPUP Sample
-------------------------------------------------------*/
function sample_POPUP(key){  
	var lsParam = '';
	lsParam ='http://www.naver.com/index.html?query='+key;
	var win=window.open(lsParam,'sample','top=0,left=0,width=900,height=800,location=0,toolbar=0,status=0,fullscreen=0,menubar=0,scrollbars=yes,resizable=yes');
  if(win) win.focus();
}