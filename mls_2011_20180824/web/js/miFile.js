/*******************************************************************************
' Function Name : gfn_flieUp
' Discription       : 첨부파일 설정
' Parameters     :  targetDs(정보를 담을 Dataset), 
' 					targetRow(정보를 담을 Dataset의 row),
' 					seq(첨부순번)
' Return value  : None
' remark          : java에서 필요한 내용 설정
' Writer        : 양재석
' Update Date   : 2009/11/12
*******************************************************************************/
function gfn_fileUp(targetDs, targetRow, targetColId, seq)
{
	//component	생성
	var isExist1 = Find("fdg_fileDialog000");
	var isExist2 = Find("fil_file000");
	if(isExist1 == null)		Create("FileDialog", "fdg_fileDialog000");
	if(isExist2 == null)		Create("File", "fil_file000");
	
	fdg_fileDialog000.Type = "OPEN";
	if (!fdg_fileDialog000.Open())		return;
	
	var fileName = fdg_fileDialog000.FileName;
	var filePath = fdg_fileDialog000.FilePath;
	var fileSize = fil_file000.getLength(fdg_fileDialog000.FileName);
	
	fil_file000.FileName = filePath + "\\" + fileName;
	fil_file000.Open("rb"); 
	var blobbuffer = fil_file000.ReadBinary();

	if(blobbuffer == "NULL")		alert("파일첨부를 실패하였습니다.\n 다시 선택해 주시기바랍니다.");
	
	fil_file000.Close();

	//컬럼생성
	var isExistCol0 = targetDs.GetColIndex("ATTACH_FILE_CNT");
	if(isExistCol0 < 0){
		targetDs.AddColumn("ATTACH_FILE_CNT");
		targetDs.SetColumn(0, "ATTACH_FILE_CNT", 0);
	}
	var isExistCol1 = targetDs.GetColIndex("ATTACH_FILE_NM"+seq);
	var isExistCol2 = targetDs.GetColIndex("ATTACH_FILE_PATH"+seq);
	var isExistCol3 = targetDs.GetColIndex("ATTACH_FILE_SIZE"+seq);
	var isExistCol4 = targetDs.GetColIndex("ATTACH_FILE_CONTENT"+seq);
	if(isExistCol1 < 0){
		targetDs.AddColumn("ATTACH_FILE_NM"+seq);
	}
	if(isExistCol2 < 0)		targetDs.AddColumn("ATTACH_FILE_PATH"+seq);
	if(isExistCol3 < 0)		targetDs.AddColumn("ATTACH_FILE_SIZE"+seq);
	if(isExistCol4 < 0)		targetDs.AddColumn("ATTACH_FILE_CONTENT"+seq, "Blob");
	
	targetDs.SetColumn(targetRow, "ATTACH_FILE_NM"+seq, fileName);
	targetDs.SetColumn(targetRow, "ATTACH_FILE_PATH"+seq, filePath);
	targetDs.SetColumn(targetRow, "ATTACH_FILE_SIZE"+seq, fileSize);
	targetDs.SetColumn(targetRow, "ATTACH_FILE_CONTENT"+seq, blobbuffer);
	
	
	//해당 데이터셋의 컬럼에 파일명 입력
	targetDs.SetColumn(targetRow, targetColId, fileName);
	return filePath + "\\" + fileName;
}





function gfn_fileUp1(targetDs, targetRow, targetColId, seq)
{
	//component	생성
	var isExist1 = Find("fdg_fileDialog000");
	var isExist2 = Find("fil_file000");
	if(isExist1 == null)		Create("FileDialog", "fdg_fileDialog000");
	if(isExist2 == null)		Create("File", "fil_file000");
	
	fdg_fileDialog000.Type = "OPEN";
	if (!fdg_fileDialog000.Open())		return;
	
	var fileName = fdg_fileDialog000.FileName;
	var filePath = fdg_fileDialog000.FilePath;
	var fileSize = fil_file000.getLength(fdg_fileDialog000.FileName);
	
	fil_file000.FileName = filePath + "\\" + fileName;
	fil_file000.Open("rb"); 
	var blobbuffer = fil_file000.ReadBinary();

	if(blobbuffer == "NULL")		alert("파일첨부를 실패하였습니다.\n 다시 선택해 주시기바랍니다.");
	
	fil_file000.Close();

	//컬럼생성
	var isExistCol0 = targetDs.GetColIndex("ATTACH_FILE_CNT");
	if(isExistCol0 < 0){
		targetDs.AddColumn("ATTACH_FILE_CNT");
		targetDs.SetColumn(0, "ATTACH_FILE_CNT", 0);
	}
	var isExistCol1 = targetDs.GetColIndex("ATTACH_FILE_NM"+seq);
	var isExistCol2 = targetDs.GetColIndex("ATTACH_FILE_PATH"+seq);
	var isExistCol3 = targetDs.GetColIndex("ATTACH_FILE_SIZE"+seq);
	var isExistCol4 = targetDs.GetColIndex("ATTACH_FILE_CONTENT"+seq);
	if(isExistCol1 < 0){
		targetDs.AddColumn("ATTACH_FILE_NM"+seq);
	}
	if(isExistCol2 < 0)		targetDs.AddColumn("ATTACH_FILE_PATH"+seq);
	if(isExistCol3 < 0)		targetDs.AddColumn("ATTACH_FILE_SIZE"+seq);
	if(isExistCol4 < 0)		targetDs.AddColumn("ATTACH_FILE_CONTENT"+seq, "Blob");
	
	targetDs.SetColumn(targetRow, "ATTACH_FILE_NM"+seq, fileName);
	targetDs.SetColumn(targetRow, "ATTACH_FILE_PATH"+seq, filePath);
	targetDs.SetColumn(targetRow, "ATTACH_FILE_SIZE"+seq, fileSize);
	targetDs.SetColumn(targetRow, "ATTACH_FILE_CONTENT"+seq, blobbuffer);
	
	
	//해당 데이터셋의 컬럼에 파일명 입력
	targetDs.SetColumn(targetRow, targetColId, fileName);
	return filePath + "\\" + fileName;
}
/*******************************************************************************
' Function Name : gfn_flieUp
' Discription       : 이미지 첨부파일 설정
' Parameters     :  targetDs(정보를 담을 Dataset), 
' 					targetRow(정보를 담을 Dataset의 row),
' 					seq(첨부순번)
' Return value  : None
' remark          : java에서 필요한 내용 설정
' Writer        : 오성실
' Update Date   : 2009/11/12
*******************************************************************************/
function gfn_imageFileUp(targetDs, targetRow, targetColId, seq)
{
	//component	생성
	var isExist1 = Find("fdg_fileDialog000");
	var isExist2 = Find("fil_file000");
	if(isExist1 == null)		Create("FileDialog", "fdg_fileDialog000");
	if(isExist2 == null)		Create("File", "fil_file000");
	
	//fdg_fileDialog000.Filter = "All(*.*)|*.*|Text File(*.txt)|*.txt|";
	fdg_fileDialog000.Filter = "JPEG(*.jpg)|*.jpg|GIF(*.gif)|*.gif|";
	fdg_fileDialog000.AppendExtDefault = true;	
	
	fdg_fileDialog000.Type = "OPEN";
	
	if (!fdg_fileDialog000.Open())		return;
	
	var fileName = fdg_fileDialog000.FileName;
	var filePath = fdg_fileDialog000.FilePath;
	var fileSize = fil_file000.getLength(fdg_fileDialog000.FileName);
	
	fil_file000.FileName = filePath + "\\" + fileName;
	fil_file000.Open("rb"); 
	var blobbuffer = fil_file000.ReadBinary();

	if(blobbuffer == "NULL")		alert("파일첨부를 실패하였습니다.\n 다시 선택해 주시기바랍니다.");
	
	fil_file000.Close();

	//컬럼생성
	var isExistCol0 = targetDs.GetColIndex("ATTACH_FILE_CNT");
	if(isExistCol0 < 0){
		targetDs.AddColumn("ATTACH_FILE_CNT");
		targetDs.SetColumn(0, "ATTACH_FILE_CNT", 0);
	}
	var isExistCol1 = targetDs.GetColIndex("ATTACH_FILE_NM"+seq);
	var isExistCol2 = targetDs.GetColIndex("ATTACH_FILE_PATH"+seq);
	var isExistCol3 = targetDs.GetColIndex("ATTACH_FILE_SIZE"+seq);
	var isExistCol4 = targetDs.GetColIndex("ATTACH_FILE_CONTENT"+seq);
	if(isExistCol1 < 0){
		targetDs.AddColumn("ATTACH_FILE_NM"+seq);
		targetDs.SetColumn(targetRow, "ATTACH_FILE_CNT", tointeger(targetDs.GetColumn(0, "ATTACH_FILE_CNT")) + 1);
	}
	if(isExistCol2 < 0)		targetDs.AddColumn("ATTACH_FILE_PATH"+seq);
	if(isExistCol3 < 0)		targetDs.AddColumn("ATTACH_FILE_SIZE"+seq);
	if(isExistCol4 < 0)		targetDs.AddColumn("ATTACH_FILE_CONTENT"+seq, "Blob");
	
	targetDs.SetColumn(targetRow, "ATTACH_FILE_NM"+seq, fileName);
	targetDs.SetColumn(targetRow, "ATTACH_FILE_PATH"+seq, filePath);
	targetDs.SetColumn(targetRow, "ATTACH_FILE_SIZE"+seq, fileSize);
	targetDs.SetColumn(targetRow, "ATTACH_FILE_CONTENT"+seq, blobbuffer);
	
	//해당 데이터셋의 컬럼에 파일명 입력
	targetDs.SetColumn(targetRow, targetColId, fileName);
	return filePath + "\\" + fileName;
}
/*******************************************************************************
' Function Name : gfn_fileUp2
' Discription       : 첨부파일 설정( 첨부파일 구분 추가 )
' Parameters     :  targetDs(정보를 담을 Dataset), 
' 					targetRow(정보를 담을 Dataset의 row),
' 					seq(첨부순번),
'					targetGubun(파일을 구분하는 값 추가)
' Return value  : None
' remark          : java에서 필요한 내용 설정
' Writer        : 오성실
' Update Date   : 2010/01/04
*******************************************************************************/
function gfn_fileUp2(targetDs, targetRow, targetColId, seq, targetGubun) 
{
	targetDs.AddColumn("FILE_GUBUN"+seq);
	targetDs.SetColumn(targetRow, "FILE_GUBUN"+seq, targetGubun);
	
	return gfn_imageFileUp(targetDs, targetRow, targetColId, seq);
}

/*******************************************************************************
' Function Name : gfn_flieDown
' Discription       : 첨부파일 다운로드
' Parameters     :  fileName(화면에 보이는 명), realName(서버에 저장된 명)
' Return value  : None
' remark          : 
' Writer        : 양재석
' Update Date   : 2009/11/12
*******************************************************************************/
function gfn_fileDown(fileName, realName)
{
	//component	생성
	var isExist = Find("WebBrowser_fileDownload000");
	if(isExist == null)		Create("WebBrowser", "WebBrowser_fileDownload000");
	
	WebBrowser_fileDownload000.Visible = true;
	WebBrowser_fileDownload000.PageUrl= gfn_getServerIP("/miXml_ci_main_Win32.xml")
						+"/miXml/fileDown.jsp?fileName="
						+fileName
						+"&realName="+realName;
	WebBrowser_fileDownload000.Run();
}