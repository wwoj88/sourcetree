/*******************************************************************************
* 기능   : Common 정보 셋팅
* 인수   : ds - 해당프로그램 ID
           objCombo - inputdataset
           strCodeGubn - outputdataset
           strCodeFlag - 전체, 선택
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_getYear( dsObj )
{

	dsObj.ClearData();
	
	dsObj.AddRow();
	
	dsObj.SetColumn(dsObj.rowposition, "CODE",  "" );	
	dsObj.SetColumn(dsObj.rowposition, "DATA",  "-전체-" );	
			
	var strYear = SubStr(GetDate(),0,4);
    var k = 0;
    
	for(i=2006 ; i<=SubStr(GetDate(),0,4); i++)
	{
	
		dsObj.AddRow();
		
		dsObj.SetColumn(dsObj.rowposition, "CODE",  toNumber(strYear)-k );	
		dsObj.SetColumn(dsObj.rowposition, "DATA",  toNumber(strYear)-k+""+"년" );	
		k++;
	}		

}


/*******************************************************************************
* 기능   : Common 정보 셋팅
* 인수   : ds - 해당프로그램 ID
           objCombo - inputdataset
           strCodeGubn - outputdataset
           strCodeFlag - 전체, 선택
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_getYear1( dsObj )
{

	dsObj.ClearData();
	
	var strYear = SubStr(GetDate(),0,4);
    var k = 0;
    
	for(i=2006 ; i<=SubStr(GetDate(),0,4); i++)
	{
	
		dsObj.AddRow();
		
		dsObj.SetColumn(dsObj.rowposition, "CODE",  toNumber(strYear)-k );	
		dsObj.SetColumn(dsObj.rowposition, "DATA",  toNumber(strYear)-k+""+"년" );	
		k++;
	}		

	
}

/*******************************************************************************
* 기능   : Common 정보 셋팅
* 인수   : ds - 해당프로그램 ID
           objCombo - inputdataset
           strCodeGubn - outputdataset
           strCodeFlag - 전체, 선택
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_getYear3( dsObj, strYear, endYear )
{

	dsObj.ClearData();
	
	dsObj.AddRow();
	
	dsObj.SetColumn(dsObj.rowposition, "CODE",  "" );	
	dsObj.SetColumn(dsObj.rowposition, "DATA",  "-전체-" );	
			
	//var strYear = SubStr(GetDate(),0,4);
    var k = 0;
    
	for(i=strYear ; i<=endYear; i++)
	{
	
		dsObj.AddRow();
		
		dsObj.SetColumn(dsObj.rowposition, "CODE",  toNumber(endYear)-k );	
		dsObj.SetColumn(dsObj.rowposition, "DATA",  toNumber(endYear)-k+""+"년" );	
		k++;
	}		

}

function gfn_getYear4( dsObj, strYear, endYear )
{

	dsObj.ClearData();
	
	//dsObj.AddRow();
	
	//dsObj.SetColumn(dsObj.rowposition, "CODE",  "" );	
	//dsObj.SetColumn(dsObj.rowposition, "DATA",  "-전체-" );	
			
	//var strYear = SubStr(GetDate(),0,4);
    var k = 0;
    
	for(i=strYear ; i<=endYear; i++)
	{
	
		dsObj.AddRow();
		
		dsObj.SetColumn(dsObj.rowposition, "CODE",  toNumber(endYear)-k );	
		dsObj.SetColumn(dsObj.rowposition, "DATA",  toNumber(endYear)-k+""+"년" );	
		k++;
	}		

}


function gfn_getYear5(dsObj, strYear, strMonth)
{
	dsObj.ClearData();
	
	dsObj.AddRow();
	
	dsObj.SetColumn(dsObj.rowposition, "CODE",  "" );	
	dsObj.SetColumn(dsObj.rowposition, "DATA",  "-전체-" );	
			
	//var strYear = SubStr(GetDate(),0,4);
    var k = 0;
    var g = 0;
    
    var curYear = ToNumber(SubStr(GetDate(),0,4)); //현재 년
    var curMonth = ToNumber(SubStr(GetDate(),4,2)); //현재 월

    
	for(i=strYear; i<=curYear; i++)
	{
		g = 0;
		if(k == 0 && strYear == curYear){//2012 , strYear:2012, endYear:2012 시작월에서 부터 현재 월까지 구해준다.
			for(j=strMonth; j<=curMonth; j++){
				dsObj.AddRow();
				//alert(toString(toNumber(endYear)-k)+""+toString(toNumber(endMonth)-g));
				//alert(toNumber(endMonth)-g);
				
				var strMonth0 = toNumber(curMonth)-g;
				
				if(strMonth0 < 10){
					strMonth0 = toString("0"+strMonth0);
				}
				
				dsObj.SetColumn(dsObj.rowposition, "CODE",  toString(toNumber(curYear)-k)+""+strMonth0);
				dsObj.SetColumn(dsObj.rowposition, "DATA",  toString(toNumber(curYear)-k)+""+strMonth0);
				g++;
			}
		}else if(k == 0 && strYear != curYear){
			for(j=1; j<=curMonth; j++){
				dsObj.AddRow();
				
				var strMonth0 = toNumber(curMonth)-g;
				
				if(strMonth0 < 10){
					strMonth0 = toString("0"+strMonth0);
				}
				dsObj.SetColumn(dsObj.rowposition, "CODE",  toString(toNumber(curYear)-k)+""+strMonth0);
				dsObj.SetColumn(dsObj.rowposition, "DATA",  toString(toNumber(curYear)-k)+""+strMonth0);
				g++;
			}
		}else if(i != curYear){
			var fullMonth = 12;
			for(j=1; j<=fullMonth; j++){
				dsObj.AddRow();
				
				var strMonth0 = fullMonth-g;
				
				if(strMonth0 < 10){
					strMonth0 = toString("0"+strMonth0);
				}
				dsObj.SetColumn(dsObj.rowposition, "CODE",  toString(toNumber(curYear)-k)+""+strMonth0);
				dsObj.SetColumn(dsObj.rowposition, "DATA",  toString(toNumber(curYear)-k)+""+strMonth0);
				g++;
			}
		}else if(i == curYear){
			var fullMonth = 12;
			for(j=12; j>=strMonth; j--){
				dsObj.AddRow();
				
				var strMonth0 = fullMonth-g;
				
				if(strMonth0 < 10){
					strMonth0 = toString("0"+strMonth0);
				}
				dsObj.SetColumn(dsObj.rowposition, "CODE",  toString(toNumber(curYear)-k)+""+strMonth0);
				dsObj.SetColumn(dsObj.rowposition, "DATA",  toString(toNumber(curYear)-k)+""+strMonth0);
				g++;
			}
		}
		k++;
	}		

}

/*******************************************************************************
* 기능   : Common 정보 셋팅
* 인수   : ds - 해당프로그램 ID
           objCombo - inputdataset
           strCodeGubn - outputdataset
           strCodeFlag - 전체, 선택
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_getMonth( dsObj )
{
	return   SubStr(GetDate(),4,2);
}




/*******************************************************************************
* 기능   : Common 정보 셋팅
* 인수   : ds - 해당프로그램 ID
           objCombo - inputdataset
           strCodeGubn - outputdataset
           strCodeFlag - 전체, 선택
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_addCombo(ds,  objCombo , strCodeGubn,  strCodeFlag )
{
	
	objCombo.InnerDataset = ds;
	
	gds_CommonCode.AddRow();
	gds_CommonCode.SetColumn(gds_CommonCode.rowposition, "codeGubun", strCodeGubn );
	gds_CommonCode.SetColumn(gds_CommonCode.rowposition, "codeFlag",  strCodeFlag );	
	gds_CommonCode.SetColumn(gds_CommonCode.rowposition, "outDs",     ds );	

}



/*******************************************************************************
* 기능   : CommonCode 조회 셋팅
* remark : gds_CommonCode
*******************************************************************************/
function gfn_comboSearch()
{


	inds  = "gds_CommonCode=gds_CommonCode ";

	var  outds = "";

	
	for( var i=0; gds_CommonCode.count > i ; i++ ) 
	{   
	    ds    = gds_CommonCode.GetColumn(i,"outDs");
		outds = outds + ds+"="+ds+ " ";
	}

    var strService = "commonCodeService";
	var strMethod  = "commonCodeList";
	var strArgs    = "";
		
	strArgs  = "service=" + quote(strService);
	strArgs += " method=" + quote(strMethod);

	gfn_Transaction( "", 
					 "comboInit",
					 inds,
					 outds,
					 strArgs );	
			 
}



/*******************************************************************************
* 기능   : Common Transcation 처리 
* 인수   : formid - 해당프로그램 ID
           inds - inputdataset
           outds - outputdataset
           strarg - Argument
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_Transaction(formid, svcid, inds, outds, strarg)
{	

	//inds  = "gds_common=gds_common_tmp " + inds;
	inds  = "gds_common=gds_common " + inds;
	//inds  = inds;
	outds = outds;
	
	//http.TimeOut = 30;  // 디폴트
	//http.timeout = 360; // 6분
	//http.timeout = 1800; // 30분
	//http.timeout = 3600; // 60분
	//http.timeout = 7200; // 120분
	http.timeout = 7200; // 
	
	Transaction(svcid, 
				"DEFAULT_SERVER::", 
				inds, 
				outds, 
				strarg, 
				"Tr_Callback");
}

 

//------------------------------------------------------------------                                           
// 업무메뉴 클릭 WINDOW OPEN
// ex : gfn_user_NewForm("sample","SAM_WebEditor")
//      gfn_user_NewForm("sample","SAM_WebEditor","PARM=" + quote("ABC"))  
//------------------------------------------------------------------ 
function gfn_user_NewForm(Fprefix,Formid, param,  formName ) 
{ 

	var objForms = AllWindows(Formid);
    
    if( gds_windows.count == 10 )
    {
		alert("10개 이상의 화면은 열수 없습니다.");
		return;
    }
    
  // 로그 저장
	gfn_insertAdminLog(Fprefix + "::" + Formid, "조회");
    
    
	// newform 이 없는경우                                                                                      
	if(objForms[0] == null) 
	{
		
		G_XMLNAME = Fprefix + "::" + Formid + ".xml";
		G_FORMID  = Formid;
	    
	    // form 정보를 저장
	    /*
		var row = gds_windows.addRow();
		gds_windows.SetColumn(row,"formid",Formid);
		gds_windows.SetColumn(row,"lev","0");
		gds_windows.SetColumn(row,"formName",formName);					
		*/					
		NewWindow(G_FORMID, G_XMLNAME, param, , , "OpenStyle=max resize=true",-1 , -1);
		
		//DockBar_Menu.mnb_mdi.MenuDataset = "gds_windows";
		
	} 
	else 
	{
		
		objForms[0].close();
		
		G_XMLNAME = Fprefix + "::" + Formid + ".xml";
		G_FORMID  = Formid;
		NewWindow(G_FORMID, G_XMLNAME, param, , , "OpenStyle=max resize=true",-1 , -1);
			
		//objForms[0].SetFocus();
		//objForms[0].
		
	}

	
} 

//------------------------------------------------------------------                                           
// 업무메뉴 클릭 WINDOW OPEN (CLOSE 버튼 비활성화)
// ex : gfn_user_NewForm("sample","SAM_WebEditor")
//      gfn_user_NewForm("sample","SAM_WebEditor","PARM=" + quote("ABC"))  
//------------------------------------------------------------------ 

function gfn_user_NewForm2(Fprefix,Formid, param,  formName ) 
{ 

	var objForms = AllWindows(Formid);
    
    if( gds_windows.count == 10 )
    {
		alert("10개 이상의 화면은 열수 없습니다.");
		return;
    }
    
    
	// newform 이 없는경우                                                                                      
	if(objForms[0] == null) 
	{
		
		G_XMLNAME = Fprefix + "::" + Formid + ".xml";
		G_FORMID  = Formid;
	    
	    // form 정보를 저장
	    /*
		var row = gds_windows.addRow();
		gds_windows.SetColumn(row,"formid",Formid);
		gds_windows.SetColumn(row,"lev","0");
		gds_windows.SetColumn(row,"formName",formName);					
		*/					
		NewWindow(G_FORMID, G_XMLNAME, param, , , "OpenStyle=max CloseFlag=false",-1 , -1);
		
		//DockBar_Menu.mnb_mdi.MenuDataset = "gds_windows";
		
	} 
	else 
	{
			
		objForms[0].SetFocus();
		
	}

	
} 

//------------------------------------------------------------------                                           
// 업무메뉴 클릭 WINDOW OPEN
// ex : gfn_user_NewForm("sample","SAM_WebEditor")
//      gfn_user_NewForm("sample","SAM_WebEditor","PARM=" + quote("ABC"))  
//------------------------------------------------------------------ 
function gfn_open(Fprefix,Formid, param,  formName ) 
{ 

	var objForms = AllWindows(Formid);
	
    if( gds_windows.count == 10 )
    {
		alert("10개 이상의 화면은 열수 없습니다.");
		return;
    }
    
	// newform 이 없는경우                                                                                      
	if( length(objForms[0]) == 0) 
	{
		
		G_XMLNAME = Fprefix + "::" + Formid + ".xml";
		G_FORMID  = Formid;
	    
	    // form 정보를 저장
	    /*
		var row = gds_windows.addRow();
		gds_windows.SetColumn(row,"formid",Formid);
		gds_windows.SetColumn(row,"lev","0");
		gds_windows.SetColumn(row,"formName",formName);					
		*/					
		NewWindow(G_FORMID, G_XMLNAME, param, , , "OpenStyle=max resize=true",-1 , -1);
		
		//DockBar_Menu.mnb_mdi.MenuDataset = "gds_windows";
		
	} 
	else 
	{

		G_XMLNAME = Fprefix + "::" + Formid + ".xml";
		G_FORMID  = Formid;

	    // form 정보를 저장
		var row = gds_windows.addRow();
		gds_windows.SetColumn(row,"formid",Formid);
		gds_windows.SetColumn(row,"lev","0");
		gds_windows.SetColumn(row,"formName",formName);					
		
	
		NewWindow(G_FORMID, G_XMLNAME, param, , , "OpenStyle=max resize=true",-1 , -1);

		DockBar_Menu.mnb_mdi.MenuDataset = "gds_windows";

	}

	
} 

/********************************
처리중입니다. 프레스바 열기
********************************/
function gfn_openProgress()
{
	// 팝업함수 호출
	/*
	Open("baseInfo::progressBar.xml"
			, ""			
			, 440, 100
			, "scroll=true Autosize=false");
	*/
	if ( find("progressBar") == null ) {
		Create("Div", "progressBar", "left='100' border='none' top='200' width='440,' height='100' url='baseInfo::progressBar.xml' syncContents='true'"); 
	} 
	
}

function gfn_openProgress2(topNum)
{
	// 팝업함수 호출
	/*
	Open("baseInfo::progressBar.xml"
			, ""			
			, 440, 100
			, "scroll=true Autosize=false");
	*/	
	if ( find("progressBar") == null) {
		Create("Div", "progressBar", "left='100' border='none' top='"+topNum+"' width='440,' height='100' url='baseInfo::progressBar.xml' syncContents='false'"); 
	} 	
}

/********************************
* 처리중입니다. 프레스바 닫기
********************************/
function gfn_closeProgress()
{
	// 자식창이 떠있는지 확인 Allwindows(" 자식창Id")
	//if( Allwindows("progressBar").count > 0 ){
	//	progressBar.close();
	//}
	
	if( find("progressBar") != null ) {
		Destroy("progressBar");
	}
}


//------------------------------------------------------------------                                           
// form Event에서 정의를 한다.
//------------------------------------------------------------------ 
function gfn_OnKeyDown(obj,objSenderObj,nChar,bShift,bControl,bAlt,nLLParam,nHLParam)
{
		if(nChar=="13")  lfn_PreSearch();
}


function  gfn_AlertMessage( strCode,  strMsg )
{
	if(strCode!="")
	{
		alert("ERROR CODE ("+strCode+") : " +  strMsg );
	}
	else
	{
		alert( strMsg );
	}
}


function  gf_AlertMessage( strCode,  strMsg )
{
	if(strCode!="")
	{
		alert("ERROR CODE ("+strCode+") : " +  strMsg );
	}
	else
	{
		alert( strMsg );
	}
}

//------------------------------------------------------------------                                           
// 관리자화면이 닫힐때 처리
//------------------------------------------------------------------ 
function gfn_close()
{

	aRow = gds_windows.FindRow("formid",  this.id );
	gds_windows.DeleteRow(aRow);
	DockBar_Menu.mnb_mdi.MenuDataset = "gds_windows";	
	close();
}


//------------------------------------------------------------------                                           
// from,  to date 조회
// from   현제년의 1월1일
// to     현제년월
//------------------------------------------------------------------ 
function gfn_fromToDate(objFrom,  objTo )
{
	objFrom.Value = SubStr( GetDate() , 0, 4)+"01"+"01";
	objTo.Value = SubStr( GetDate() , 0, 8);
}



//------------------------------------------------------------------                                           
// 그리드 헤더의 체크를 선택했을때 처리
// objGrid : 그리드명
// objDs   : 데이타셋 명
//------------------------------------------------------------------ 
function  gfn_gridAllChk( objGrid,  objDs )
{
	
	if(objGrid.GetCellText("Head",1,0)=="1"){
	
		objGrid.SetCellProp("Head",0,"text","0");
		
		for( i=0; i <= objDs.count; i++)
		{
			objDs.SetColumn(i, "CHK", 0);
		}

	}else{
	
		objGrid.SetCellProp("Head",0,"text","1");
		
		for( i=0; i <= objDs.count; i++)
		{
			objDs.SetColumn(i, "CHK", 1);
		}

	}

}



//------------------------------------------------------------------                                           
// 로그인여부를 체크
//------------------------------------------------------------------ 
function gfn_userLoginChk()
{

	if( S_USER_ID=="null" ||  S_USER_ID = "")
	{
		UserNotify("100","로그인이 필요한 화면입니다.");  
		return;
	}

}



//------------------------------------------------------------------                                           
// 이메일 형식 체크. 
//------------------------------------------------------------------ 
function gfn_IsEmail(sValue, focusObj)
{
	var sReturnValue = false;
	var sTmp = "";
	var sRegExp = "[a-z0-9]+[a-z0-9\-\_.,]+@[a-z0-9]+[a-z0-9.,]+\\.[a-z0-9]+";
	
	var regexp = CreateRegExp(sRegExp,"ig");
	sTmp = regexp.Exec(sValue);
	
	if ( sTmp == null )
		sReturnValue = false;
	else
	{
		if ( ( sTmp.index == 0 ) && (sTmp.length == sValue.length ) )
			sReturnValue = true;
		else
			sReturnValue = false;
	}
	
	if( sReturnValue == false)
	{	
		alert("정확한 E-Mail을 입력해주세요.");
		focusObj.SetFocus();
	}
	
	
	return sReturnValue;
} 

//------------------------------------------------------------------                                           
// 각 정규식에대한 함수 호출.
//------------------------------------------------------------------ 
function gfn_isFormatCheck(gubun, sValue, focusObj, sMsg)
{
	//alert(gubun);
	
	var sRegExp = "";
	var alertMsg = "정확한 " +sMsg+ "을(를) 입력해주세요.  \n숫자와 '-'만 입력가능합니다.";
	
	if( gubun.ToUpperCase == 'PHONE')
		sRegExp = "^([0-9]{2,4}-)?[0-9]{3,4}-[0-9]{4}$";		//  01-000-0000, 0000-0000, 000-0000-0000

	else if( gubun.ToUpperCase == 'ACCOUNT')
		sRegExp = "^([0-9]{1,}-)*[0-9]{1,}-[0-9]{1,}$";
		
	else if( gubun.ToUpperCase == 'EMAIL')
	{
		sRegExp = "[a-z0-9]+[a-z0-9.,]+@[a-z0-9]+[a-z0-9.,]+\\.[a-z0-9]+";
		alertMsg = "정확한 " +sMsg+ "을(를) 입력해주세요";
	}
	
	return gfn_isFormat(sRegExp, sValue, focusObj, alertMsg);
}

//------------------------------------------------------------------                                           
// 넘버포맷형식 체크. 
//------------------------------------------------------------------ 
function gfn_isFormat(sRegExp, sValue, focusObj, alertMsg)
{	
	//alert(sRegExp);
	
	var sReturnValue = false;
	var sTmp = "";
	
	var regexp = CreateRegExp(sRegExp,"ig");
	sTmp = regexp.Exec(sValue);
	
	if ( sTmp == null )
		sReturnValue = false;
	else
	{
		if ( ( sTmp.index == 0 ) && (sTmp.length == sValue.length ) )
			sReturnValue = true;
		else
			sReturnValue = false;
	}
	
	if( sReturnValue == false)
	{	
		alert(alertMsg);
		focusObj.SetFocus();
	}
	
	return sReturnValue;
} 


/********************************
edit  text 널체크 글로벌 함수.
********************************/
function gfn_formCheck(checkType, objValue, focusObj, msg)
{	
	// 널체크
	if(checkType == 'NULL')
	{
		if( length(trim( objValue) ) == 0 )  
		{
			alert(msg+"(을)를 입력해 주세요.");
			
			focusObj.SetFocus();
			
			return false;
			break;
		}
		else
		{	
			return true;
		}
	}
}

/********************************
edit  value 널체크 글로벌 함수.
********************************/

function gfn_formCheck2(checkType, objID, msg)
{	
	// 널체크
	if(checkType == 'NULL')
	{
		if( length(trim( objID.value) ) == 0 )  
		{
			alert(msg+"(을)를 입력해 주세요.");
			
			objID.SetFocus();
			
			return false;
			break;
		}
		else
		{	
			return true;
		}
	}
}




/********************************
ds 널체크 글로벌 함수.
예] gfn_dsCheck(ds_list_kapp, "DISK_NAME^CD_CODE", "음반명^시디코드", div1.grd_List, "1^2");
********************************/
function gfn_dsCheck(objDs, chkColNames, msgs, objGrid, grdColIds, focusObj)
{

	var arrayColNames    = chkColNames.split("^");
	var arrayMsgs 	  	 = msgs.split("^");
	var arrayGrdColIds 	 = grdColIds.split("^");
	
	var cnt=0;
	for( i=0; i< objDs.GetRowCount(); i++ )
	{	
		for( j =0 ; j<arrayColNames.length; j++) 
		{	
			//alert(objDs.GetColumn(i,arrayColNames[j]));
			if( length(trim(objDs.GetColumn(i,arrayColNames[j]))) == 0)
			{	
			
				//alert(objDs.GetRowPosition());
				//ds_list.GetRowPosition()
				
				alert(arrayMsgs[j]+"(을)를 입력해 주세요.");		
				
				// 포커스이동
				if(focusObj != null)
				{
					focusObj.SetFocus();
				}
				
				cnt++;
				return false;
				break;
			}
			
			if(cnt>0)	break;
		}
		if(cnt>0)	break;
	}
	
	if(cnt>0)	return false;
	else return true;
	
}


/********************************
ds 널체크 글로벌 함수.
예] if( !gfn_dsRowCheck(ds_list_kapp, "내용(음원저작자협회)", div1.chk_file)) return false;
********************************/
function gfn_dsRowCheck(objDs, msg, focusObj)
{
	//ds_list_kapp.GetRowCount();
	
	if(objDs.GetRowCount() == 0)
	{
		alert(msg+"정보를 입력해주세요.");
		focusObj.SetFocus();	
		
		return false;
	}
	
	return true;
}


 
/********************************
한행 ds 널체크 글로벌 함수.
예]
********************************/
function gfn_dsCheck1(writeObjDs, chkColName1, chkFocusObjId, returnMsg){
	if(length(trim(writeObjDs.GetColumn(0,chkColName1))) == 0){
		var objType = substr(chkFocusObjId.id,0,3);
		if(objType == "chk"){
			alert(returnMsg+"(을)를 선택해주세요.");
		}else{
			alert(returnMsg+"(을)를 입력해주세요.");
		}
		chkFocusObjId.SetFocus();
		return false;
	}
	
	return true;
}

/********************************
여러행 ds 널체크 글로벌 함수.
예]
********************************/
function gfn_dsMultiCheck1(writeObjDs, chkColName1, chkFocusObjId, returnMsg, idx){
	if(length(trim(writeObjDs.GetColumn(idx,chkColName1))) == 0){
		var objType = substr(chkFocusObjId.id,0,3);
		if(objType == "chk"){
			writeObjDs.row = idx;
			alert((idx+1)+"번째 이용 승인신청 명세서의 "+returnMsg+"(을)를 선택해주세요.");
		}else{
			writeObjDs.row = idx;
			alert((idx+1)+"번째 이용 승인신청 명세서의 "+returnMsg+"(을)를 입력해주세요.");
		}
		chkFocusObjId.SetFocus();
		return false;
	}
	
	return true;
}


/********************************
파일다운로드URL RETURN
********************************/

function gfn_getFilePath()
{
	//return "D://temp//";
	//return "/home/tmax/mls/web/upload/";  // 실서버
	return "/home/tmax_dev/mls/web/upload/";  // 개발서버
	
}


/********************************
현재 서버위치 RETURN
********************************/
function gfn_getServerIP(strDelimiter)
{
	//현재 서버위치를 가지고 온다.
	var aSplitResult = split(global.startxml,strDelimiter, "webstyle");
	var sHrmLocation = aSplitResult[0];
	var sOtherLocation = "";

	//alert("aSplitResult.length"+aSplitResult.length);

	//현재 로컬에서 작업중인 경우 개발계로 지정
	if ( aSplitResult.length == 1){
		
		//sHrmLocation = "http://new.right4me.or.kr";
		//sHrmLocation = "http://www.right4me.or.kr:8080";
		sHrmLocation = "http://127.0.0.1";
	}
	
	return sHrmLocation;
}

function gfn_fileDownUrl( argFilePath, argFileName, argRealFileName, strDelimiter)
{
	var ServerUrl 		= gfn_getServerIP(strDelimiter)+"/include/FileDownload.jsp";

// 개발 or 운영에 올릴 시 "http://"+ -> 이부분 삭제해야함.ServerUrl에 http://이 포함되어 있음.
	var strPageUrl      = ServerUrl
	                    + "?filePath="+argFilePath
	                    + "&fileName="+argFileName
	                    + "&realFileName="+argRealFileName;     
	
	return strPageUrl;
	
	
}

function lfn_main(obj)
{	
	
	// 로그인시 공지사항 목록 디폴트 오픈.
	//prefix = "board";
	//Formid = "sysmNotiList";
	prefix = "main";
	Formid = "mainList";
	param  = "open=default";
	FormName = "관리자메인화면";
	
	if(Formid != null){
		
		gfn_user_NewForm(prefix,Formid,param,FormName);
		
		mainList.lfn_Start();
		
		// 자신종료
		this.close();
	}
	
}



function lfn_main2(obj)
{	
	
	// 로그인시 공지사항 목록 디폴트 오픈.
	//prefix = "board";
	//Formid = "sysmNotiList";
	prefix = "main";
	Formid = "mainList";
	param  = "open=default";
	FormName = "관리자메인화면";
	
	if(Formid != null){
		
		gfn_user_NewForm(prefix,Formid,param,FormName);
		
		mainList.lfn_Start();
	}
	
}
//URL 정규식
function lfn_isURL(sValue){
	var sReturnValue = false;
	var sTmp = "";
	var sRegExp = "(((http(s?))\:\/\/)?)([0-9a-zA-Z\-]+\.)+[a-zA-Z]{2,6}((\:[0-9]+)?(\/\S*)?)+([0-9a-zA-Z\-]+\.(jpg|jpeg|gif|png|bmp|tif))$";
	
	var regexp = CreateRegExp(sRegExp,"ig");
	sTmp = regexp.Exec(sValue);
	
	
	if ( sTmp == null )
		sReturnValue = false;
	else
	{
		if ( ( sTmp.index == 0 ) && (sTmp.length == sValue.length ) )
			sReturnValue = true;
		else
			sReturnValue = false;
	}

	return sReturnValue;
	
}

//년월일 벨리데이션 체크
function lfn_isValidDate(dateStr) {

	var y = ToInteger(substr(dateStr,0,4)); 
	var m = ToInteger(substr(dateStr,4,2));
	var d = ToInteger(substr(dateStr,6,2));

 
	var er = true; // 에러 변수
	var daa = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	
	if (y%1000 != 0 && y%4 == 0){
		daa[1] = 29; // 윤년
	}
	
	if (d > daa[m-1] || d < 1){
		er = false; // 날짜 체크
	}
	
	if (m < 1 || m > 12){
		er = false; // 월 체크
	}
	
	if (m%1 != 0 || y%1 != 0 || d%1 != 0){
		er = false; // 정수 체크
	}

	return er;
}

//알파벳 체크
function lfn_isValAlpha(cha){
	var regex = "[a-zA-z]";
	var tmp = "";
	var returnVal = false;
	
	var regexp = CreateRegExp(regex,"ig");
	tmp = regexp.Exec(cha);
	
	if( tmp == null){
		returnVal = false;
	}else{
		if((tmp.index==0) && (tmp.length==cha.length)){
			returnVal = true;
		}else{
			returnVal = false;
		}
	}
	
	return returnVal;
}

/*******************************************************************************
' Function Name : gfn_insertAdminLog
' Discription     : 관리자 로그 저장
' Parameters     :  
' Return value  : None
' remark          : gds_adminLog
' Writer        : 김소라
' Update Date   : 2014/11/24
*******************************************************************************/
function gfn_insertAdminLog(menuId, procStatus)
{

	gds_admin_log.ClearData();
    gds_admin_log.AddRow();

	gds_admin_log.SetColumn( 0, "MANAGER_ID", gds_common.GetColumn(0, "USER_ID"));  // 관리자 아이디
	gds_admin_log.SetColumn( 0, "MENU_URL", menuId); // 메뉴
	gds_admin_log.SetColumn( 0, "PROC_STATUS", procStatus);   // 처리내역
	gds_admin_log.SetColumn( 0, "IP_ADDRESS", gds_common.GetColumn(0, "IP"));   // IP
	
    var strService = "adminCommonService";
	var strMethod  = "insertAdminLogDo";
	var strArgs    = "";
		
	strArgs  = "service=" + quote(strService);
	strArgs += " method=" + quote(strMethod);

	gfn_Transaction( "", 
					 "",
					 "gds_admin_log=gds_admin_log ",
					 "",
					 strArgs );	
			 
}