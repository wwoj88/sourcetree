function gfn_MakePage()
{
	//iLeft, iWidth로 페이지 번호의 크기와 간격을 지정합니다.
	//한자리 : width = 26, left 간격 : 25
	//두자리 : width = 30, left 간격 : 30
	//세자리 : width = 35, left 간격 : 35
	var iLeft       = 10;
	var iWidth      = 16;
	
	var iMyFont		= "";
	var iMyColor	= "";

	iTotalCount = ds_page.GetColumn(0,"COUNT");
	
	if(iTotalCount==0)
	{
		div_page.Contents 	= "";
		return;
	}
	
	var sb = "<Contents>\n";
	
	var iTotalPage;	//
	if( floor(iTotalCount%iVolumnPerPage) > 0 )	
		iTotalPage = floor(iTotalCount/iVolumnPerPage) +1;
	else
		iTotalPage = floor(iTotalCount/iVolumnPerPage);

	var iUint = floor((toInteger(iNowPage)-1) / toInteger(iPageScale));
	iUint = (iUint * iPageScale)+1;
	
	var iNextUnit = toInteger(iUint+iPageScale);

	// 첫페이지(<<)
	if(iUint > iPageScale){ 
		sb = sb +"<button	Cursor='HAND' ImageID='first'  Id='first'   Left='0'	Top='2' Width='13' Height='17' Font='굴림,9'   Color='Black' Appearance='Falt' OnClick='getList' OnMouseOver='f_over_color' OnMouseOut='f_out_color' OnFocus='f_focus_color' OnKillFocus='f_kill_color' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}else{
		sb = sb +"<button	ImageID='first_off'  Id='first'   Left='0'	Top='2' Width='13' Height='17' Font='굴림,9'   Color='Black' Appearance='Falt'  BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}
	iLeft = iLeft + 10;

	// 앞으로(<)
	if(iUint > iPageScale){
		sb = sb +"<button	Cursor='HAND' Id='before'  ImageID='before'   userData='"+(iUint-1)+"'	Left='"+iLeft+"'	Top='2'	Width='30'	Height='17'	 Font='굴림,9'  Text='이전' Color='Black'  Appearance='Falt' OnClick='getList' OnMouseOver='f_over_color' OnMouseOut='f_out_color' OnFocus='f_focus_color' OnKillFocus='f_kill_color' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}else{
		sb = sb +"<button	Id='before'  ImageID='before_off'   userData='"+(iUint-1)+"'	Left='"+iLeft+"'	Top='2'	Width='30'	Height='17'	 Font='굴림,9'  Text='이전' Color='Black'  Appearance='Falt'  BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}
	iLeft = iLeft +15;
	
	cnt = 0;

	// 페이징[1 2 3 4 5 6 7 8 9 10]
	for(var i = iUint ; i < iNextUnit ; i++ ){
		
		vPer =  ToString(i).length;
	
		if( vPer == 1 || vPer == 0 )
		{
          // iLeft   = iLeft + 10;
	       iWidth  = 16;
		}
		else
		{
			if(i == 10 )
			{
				iWidth  = 16;			
			}
			else 
			{
				vper = 5 * ToNumber(vPer);
				//iLeft   = iLeft + ( vper * 10 );
				iWidth  = 16 + (ToNumber(vper)*1) ;			
			}
		}
		
		cnt = cnt ++;

		if(i > iTotalPage)
			break;
		if(i == iNowPage){
			sb = sb +"<button	Cursor='HAND'  Id='"+i+"' Left='"+iLeft+"' Top='0' Width='"+iWidth+"' Height='17' Font='굴림,9,bold' Text='"+i+"' Color='#F56A15' Appearance='Falt' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
		}else{
			sb = sb +"<button	Cursor='HAND'  Id='"+i+"'	Left='"+iLeft+"'	Top='0'	Width='"+iWidth+"'	Height='17'	 Font='굴림,9'  Text='"+i+"' Color='#8F8F8F'  Appearance='Falt' OnClick='getList' OnMouseOver='f_over_color' OnMouseOut='f_out_color' OnFocus='f_focus_color' OnKillFocus='f_kill_color' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
		}
		
		iLeft = iLeft + iWidth;		
		
	}
	iLeft = iLeft +5;
	
	// 뒤로(>)
	if(iNextUnit <= iTotalPage){
		sb = sb +"<button	Cursor='HAND' Id='next' ImageID='next' userData='"+iNextUnit+"'	Left='"+iLeft+"'	Top='2'	Width='30'	Height='17'	 Font='굴림,9'   Color='Black'  Appearance='Falt' OnClick='getList' OnMouseOver='f_over_color' OnMouseOut='f_out_color' OnFocus='f_focus_color' OnKillFocus='f_kill_color' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}else{
		sb = sb +"<button	 Id='next' ImageID='next_off' userData='"+iNextUnit+"'	Left='"+iLeft+"'	Top='2'	Width='30'	Height='17'	 Font='굴림,9'   Color='Black'  Appearance='Falt'  BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}
	iLeft = iLeft + 15;
	
	// 마지막페이지(>>)
	if(iNextUnit < iTotalPage){
		sb = sb +"<button	Cursor='HAND' Id='end'  ImageID='end'  userData='"+iTotalPage+"'	Left='"+iLeft+"'	Top='2'	Width='13'	Height='17'	 Font='굴림,9'  Color='Black'  Appearance='Falt' OnClick='getList' OnMouseOver='f_over_color' OnMouseOut='f_out_color' OnFocus='f_focus_color' OnKillFocus='f_kill_color' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}else{
		sb = sb +"<button	Cursor='HAND' Id='end'  ImageID='end_off'  userData='"+iTotalPage+"'	Left='"+iLeft+"'	Top='2'	Width='13'	Height='17'	 Font='굴림,9'  Color='Black'  Appearance='Falt'  BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}
	iLeft = iLeft + 13;  

	sb 			= sb +"</Contents>\n";
	div_page.Contents 	= sb;
	
	//trace(">>>>>>>>>>>>>>>> [Pasing String]\n\n"+sb);
}



function isNumber(obj) { 
	var str = obj.value;
	if(str.length == 0)
		return false;
	
	for(var i=0; i < str.length; i++) {
		if(!('0' <= str.charAt(i) && str.charAt(i) <= '9'))
			return false;
		}
	return true;
}


//	OnMouseOver
function f_over_color(){
	
}
//	OnMouseOut
function f_out_color(){
	
}
//	OnFocus
function f_focus_color(){
	
}
//	OnKillFocus
function f_kill_color(){
	
}


function getList(obj)
{
    if(obj.Id =='first' )
    {
		iNowPage = 1;
    }
	else if( obj.Id=='before' || obj.Id=='next' || obj.Id=='end' )
	{
		iNowPage = obj.userData;
	}
	else if( isNumber(obj))
	{
		iNowPage = obj.Id;
	}
	
	ds_condition.SetColumn( 0, "FROM_NO",   (tonumber(iNowPage)*iVolumnPerPage) - (iVolumnPerPage-1) );		
	ds_condition.SetColumn( 0, "TO_NO",     tonumber(iNowPage)*iVolumnPerPage );		
	
    lfn_Search();
	
}


////////////////// 두번째 페이징 ////////////////////

function gfn_MakePage2()
{

	//iLeft, iWidth로 페이지 번호의 크기와 간격을 지정합니다.
	//한자리 : width = 26, left 간격 : 25
	//두자리 : width = 30, left 간격 : 30
	//세자리 : width = 35, left 간격 : 35
	var iLeft       = 10;
	var iWidth      = 16;
	
	var iMyFont		= "";
	var iMyColor	= "";

	iTotalCount2 = ds_page2.GetColumn(0,"COUNT");
	
	if(iTotalCount2==0)
	{
		div_page2.Contents 	= "";
		return;
	}
	
	var sb = "<Contents>\n";
	
	var iTotalPage;	//
	if( floor(iTotalCount2%iVolumnPerPage2) > 0 )	
		iTotalPage = floor(iTotalCount2/iVolumnPerPage2) +1;
	else
		iTotalPage = floor(iTotalCount2/iVolumnPerPage2);

	var iUint = floor((toInteger(iNowPage2)-1) / toInteger(iPageScale2));
	iUint = (iUint * iPageScale2)+1;

	var iNextUnit = toInteger(iUint+iPageScale2);

	// 첫페이지(<<)
	if(iUint > iPageScale2){ 
		sb = sb +"<button	Cursor='HAND' ImageID='first'  Id='first'   Left='0'	Top='2' Width='13' Height='17' Font='굴림,9'   Color='Black' Appearance='Falt' OnClick='getList2' OnMouseOver='f_over_color' OnMouseOut='f_out_color' OnFocus='f_focus_color' OnKillFocus='f_kill_color' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}else{
		sb = sb +"<button	ImageID='first_off'  Id='first'   Left='0'	Top='2' Width='13' Height='17' Font='굴림,9'   Color='Black' Appearance='Falt'  BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}
	iLeft = iLeft + 10;

	// 앞으로(<)
	if(iUint > iPageScale2){
		sb = sb +"<button	Cursor='HAND' Id='before'  ImageID='before'   userData='"+(iUint-1)+"'	Left='"+iLeft+"'	Top='2'	Width='30'	Height='17'	 Font='굴림,9'  Text='이전' Color='Black'  Appearance='Falt' OnClick='getList2' OnMouseOver='f_over_color' OnMouseOut='f_out_color' OnFocus='f_focus_color' OnKillFocus='f_kill_color' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}else{
		sb = sb +"<button	Id='before'  ImageID='before_off'   userData='"+(iUint-1)+"'	Left='"+iLeft+"'	Top='2'	Width='30'	Height='17'	 Font='굴림,9'  Text='이전' Color='Black'  Appearance='Falt'  BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}
	iLeft = iLeft +15;
	
	cnt = 0;

	// 페이징[1 2 3 4 5 6 7 8 9 10]
	for(var i = iUint ; i < iNextUnit ; i++ ){
		
		vPer =  ToString(i).length;
	
		if( vPer == 1 || vPer == 0 )
		{
          // iLeft   = iLeft + 10;
	       iWidth  = 16;
		}
		else
		{
			if(i == 10 )
			{
				iWidth  = 16;			
			}
			else 
			{
				vper = 5 * ToNumber(vPer);
				//iLeft   = iLeft + ( vper * 10 );
				iWidth  = 16 + (ToNumber(vper)*1) ;			
			}
		}
		
		cnt = cnt ++;

		if(i > iTotalPage)
			break;
		if(i == iNowPage2){
			sb = sb +"<button	Cursor='HAND'  Id='"+i+"' Left='"+iLeft+"' Top='0' Width='"+iWidth+"' Height='17' Font='굴림,9,bold' Text='"+i+"' Color='#F56A15' Appearance='Falt' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
		}else{
			sb = sb +"<button	Cursor='HAND'  Id='"+i+"'	Left='"+iLeft+"'	Top='0'	Width='"+iWidth+"'	Height='17'	 Font='굴림,9'  Text='"+i+"' Color='#8F8F8F'  Appearance='Falt' OnClick='getList2' OnMouseOver='f_over_color' OnMouseOut='f_out_color' OnFocus='f_focus_color' OnKillFocus='f_kill_color' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
		}
		
		iLeft = iLeft + iWidth;		
		
	}
	iLeft = iLeft +5;
	
	// 뒤로(>)
	if(iNextUnit <= iTotalPage){
		sb = sb +"<button	Cursor='HAND' Id='next' ImageID='next' userData='"+iNextUnit+"'	Left='"+iLeft+"'	Top='2'	Width='30'	Height='17'	 Font='굴림,9'   Color='Black'  Appearance='Falt' OnClick='getList2' OnMouseOver='f_over_color' OnMouseOut='f_out_color' OnFocus='f_focus_color' OnKillFocus='f_kill_color' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}else{
		sb = sb +"<button	 Id='next' ImageID='next_off' userData='"+iNextUnit+"'	Left='"+iLeft+"'	Top='2'	Width='30'	Height='17'	 Font='굴림,9'   Color='Black'  Appearance='Falt'  BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}
	iLeft = iLeft + 15;
	
	// 마지막페이지(>>)
	if(iNextUnit < iTotalPage){
		sb = sb +"<button	Cursor='HAND' Id='end'  ImageID='end'  userData='"+iTotalPage+"'	Left='"+iLeft+"'	Top='2'	Width='13'	Height='17'	 Font='굴림,9'  Color='Black'  Appearance='Falt' OnClick='getList2' OnMouseOver='f_over_color' OnMouseOut='f_out_color' OnFocus='f_focus_color' OnKillFocus='f_kill_color' BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}else{
		sb = sb +"<button	Cursor='HAND' Id='end'  ImageID='end_off'  userData='"+iTotalPage+"'	Left='"+iLeft+"'	Top='2'	Width='13'	Height='17'	 Font='굴림,9'  Color='Black'  Appearance='Falt'  BKColor = '#FFFFFF' BorderColor = '#FFFFFF'></button>\n" ;
	}
	iLeft = iLeft + 13;  

	sb 			= sb +"</Contents>\n";
	div_page2.Contents 	= sb;
	
	//trace(">>>>>>>>>>>>>>>> [Pasing String]\n\n"+sb);
}

function getList2(obj)
{
    if(obj.Id =='first' )
    {
		iNowPage2 = 1;
    }
	else if( obj.Id=='before' || obj.Id=='next' || obj.Id=='end' )
	{
		iNowPage2 = obj.userData;
	}
	else if( isNumber(obj))
	{
		iNowPage2 = obj.Id;
	}
	
	ds_condition.SetColumn( 0, "FROM_NO2",   (tonumber(iNowPage2)*iVolumnPerPage2) - (iVolumnPerPage2-1) );		
	ds_condition.SetColumn( 0, "TO_NO2",     tonumber(iNowPage2)*iVolumnPerPage2 );		
	
    lfn_Search2();
	
}






