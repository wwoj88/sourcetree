<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html;charset=euc-kr" %>
<%// 기존 Install화면을 사이즈 작게수정 : 20091113일 작성%>

<HTML>
<HEAD>
<base target="_self"/>
<TITLE>::저작권찾기::</TITLE>
<meta http-equiv="pragma" content="no-cache">
<link rel="stylesheet" href="/css/installStyle.css" type="text/css">
<link href="/css/popup.css" rel="stylesheet" type="text/css"/>
<SCRIPT LANGUAGE="JavaScript" src="/install/js/MiInstaller321.js"></SCRIPT>
<SCRIPT>

var returnVal = '';

function modalclose(sValue){
/*	
	window.returnValue = sValue;
	
	
*/
	returnVal = sValue;
	opener.fn_clsInstall(sValue);
	self.close();
}

function modalExit(){
	
	if(returnVal=='Y')
		return;
		
	opener.fn_clsInstall('N');
	self.close();
}

function callSetup(url){
	window.dialogArguments.location.href = url;
	self.close();
}


function popInstallMemo(){
	//alert("popInstallMemo");
	window.name = "dialogPopup";
	window.target = "dialogPopup";
	
	var sFeatures = 'width=710px; height=368px; help=0; status=0;';
	
	var returnVal = window.open('/install/popMemo.jsp', '',  sFeatures);
	
	if (returnVal == null ) {
		alert('팝업이 차단되었습니다. 팝업차단 사용안함으로 설정해 주세요.');
	}
}
</SCRIPT>
<script language="vbscript">
	//---------------------------------------------------------------------------------
	// ActiveX Setup Check
	//---------------------------------------------------------------------------------	
	Function  Check_Module
	  
	  	On Error Resume Next
	  	
	   	Err.number = 0
	   	chkMsg= document.MiInstaller.Version
	  	if  Err.number = 438 Then	
			Check_Module = "false"
	  	Else
	        Check_Module = "true"
	  	End If
	End Function 
</script>

<SCRIPT LANGUAGE="JavaScript">
	var GcurIndx = 0;
	var Gtotcnt = 0;
	
	var TotalVersionFileCnt;
	var TotalItemCnt;
	var progressColor = "red";	// set to progress bar color
	var pg_cell_At = 0,pg1_cell_At = 0; 
	var IsError = false;
	var proMsg, procMsg;

	var proMsg, procMsg;;
	var TotalVersionFileCnt;
	var TotalItemCnt;
	var flagDISTRIBUTE;
	var cur_version_file;
	var Server_Path = window.location.href;
	Server_Path = Server_Path.substring(0, Server_Path.lastIndexOf("/")) + "/";	
	var processWidth = 590;
	
	var MiInstallPath = 'http://'+location.host+'/install/info/';
//	var MiStartXMLPath = 'http://'+location.host+'/MYKEY.xml';	
//	var MiStartXMLPath = 'http://'+location.host+'/mls_project_ci_main_Win32.xml';
	var MiStartXMLPath =	'http://'+location.host+'/make_mls_project_ci_main_Win32.jsp?hostIp='+location.host;
	//---------------------------------------------------------------------------------
	// Load시 Version Check 및 Install
	//---------------------------------------------------------------------------------
	function fn_OnLoad()
	{
		
		if ( Check_Module() == "true" )
		{
			
			var MiInstaller = document.getElementById("MiInstaller");
			
			MiInstaller.Key = "mls";
			MiInstaller.Launch = true;	
			MiInstaller.Width  = 1024;
			MiInstaller.Height	= 768;
			MiInstaller.Retry = 0;
			MiInstaller.Timeout = 300;
			MiInstaller.GlobalVal = "";
			MiInstaller.OnlyOne = true;
			MiInstaller.ComponentPath = "%UserApp%TobeSoft\\mls\\component";
			MiInstaller.StartXML  = MiStartXMLPath;
			//MiInstaller.Resource = "%component%resource.xml";	
			//MiInstaller.StartImage  = "%component%next_start.gif";

			//MiInstaller.NotLaunchForErr = "true";
			var UpdateUrl = "";
	  	var mode = checkOS();
			if ( mode == "VISTA" )
			{
				UpdateUrl = "Info_Vista.xml";
			} else {
				UpdateUrl = "Info_XP.xml";
			}
			
			MiInstaller.UpdateURL = MiInstallPath + UpdateUrl;
			var Bcnt = MiInstaller.ExistVersionUpCnt(); 
			if ( Bcnt )
			{
				MiInstaller.StartDownLoad();
			} else {
				fn_run();
			}

			} 
		else 
		{
			//---------------------------------------------------------------------------------
			// ActiveX 설치 되지 않은 경우
			//---------------------------------------------------------------------------------
			IsError = true;

			err_msg.innerHTML += '<font color="red">Unable to Install ActiveX Control from Web Sites !!!<br><br>' + 
					'<img src="/images/install/bu_comment02.gif" width="14" height="11">&nbsp;&nbsp;<font color="black">수동설치시 <a href="install/MiPlatform_SetupDeploy320U_200901.exe"><b>여기</b></a> 를 클릭하시기 바랍니다.</font>';

			t_err.className = "show";
			reinstall.className = "show";
		}

	}

	function page_link()
	{
	
		// 단축 아이콘 만들기
		// UBKImage : 250 * 300
		
		//BYTE가 256을 넘으면 안됨..
		// 256을 넘을 경우 MiInstaller의 property를 이용할것

		var strCommand = '-V 3.2 -D Win32U -R FALSE -K KORAIL -L TRUE -LE TRUE -BI "%component%next_upd.gif" ';
		var MiInstaller = document.getElementById("MiInstaller");
		MiInstaller.MakeShortCut("%system%MiUpdater321.exe",strCommand,"제주관광공사","%Component%icon_next.ico","");
		
		// MakeShortCut 바로가기를 만드는 함수
		// strExeName: 바로가기를 만들 실행 파일 이름
		// strCommand: 바로가기를 만들 때 필요한 Command 정보
		// strShortcutName: 바로가기 파일 이름
		// strIConPath: 변경하려는 Icon 경로를 %alias%형태로 입력할 수 있습니다.
		// strPos: Startmenu-시작 / Desktop-바탕화면(Default)
	
		// Alias 참고
		// %MiPlatform%
		// %Component%
		// %system%
		// %Window%
		// %ProgramFiles%
	
	} 

	function fn_run()
	{
		if ( IsError ) return ;
		
		//단축 아이콘 생성
		//page_link();
	
    		//location.href = "/main/main.do";
		modalclose('Y');	
	}
 

	function progress_clear(obj) {
		obj.width = 0;
		if ( obj.id == "progress" ) pg_cell_At = 0;
		else pg1_cell_At = 0;		
	}
	
	function progress_update(obj,cur_cnt) {
		
		if ( obj.id == "progress" ) obj.width = cur_cnt;
		else obj.width = cur_cnt;
	}
	
	
</SCRIPT>

<SCRIPT language=JavaScript for=MiInstaller event=OnConfirm(ItemName)>
{
	
	var a;
	a  = "OnConfirm=>Item=";
	a += ItemName;	
	//alert(a);
}
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=MiInstaller EVENT=OnProgress(ItemName,prgress,progressMax,StatusText)>
{
			
	var Progress = document.getElementById("progress");
	if(ItemName != "")	{
		if( progressMax > 0 && prgress > 0 )	{
		        var before_At = pg_cell_At;

		        pg_cell_At = parseInt( ( ( ( GcurIndx - 1)/Gtotcnt ) * processWidth ) +  ( ( ( 1/Gtotcnt ) * processWidth ) * (prgress/progressMax) ) );
		        if ( pg_cell_At > processWidth ) pg_cell_At = processWidth;
						progress.width = pg_cell_At/1.5;
		}
	}
}
</SCRIPT>

<SCRIPT language=JavaScript for=MiInstaller event=OnStartDownLoad(VersionFileName,DownFileName,Type,TotalCnt,CurIndex)>
{
		var Progress = document.getElementById("progress");
		var Distproc = document.getElementById("Distproc");
		var tot_item = document.getElementById("tot_item");
		var item_nm = document.getElementById("item_nm");	
		var prc_msg = document.getElementById("prc_msg");	

		if ( Type == 1 ) //EVENTCONFIG
		{
			//progress_clear(progress);
			//progress_clear(progress1);
		}
		else if ( Type == 2 ) 
		{
				progress.width = 0;
				Distproc.width = 0;
				tot_item.innerHTML = "&nbsp;" + CurIndex + "/" + TotalCnt;		
				//tot_ditem.innerHTML = "&nbsp;";
		
		} 
		else if ( Type == 3 ) //EVENTDOWNLOAD
		{
			GcurIndx = CurIndex;
			Gtotcnt = TotalCnt;
			tot_item.innerHTML = "&nbsp;" + CurIndex + "/" + TotalCnt;		
			
			var tpos = DownFileName.lastIndexOf("/") + 1;
			var Fname = DownFileName.substr(tpos,DownFileName.length - tpos);

			item_nm.innerHTML =  "["+Fname+"]";
		
			if ( prc_msg != "" && prc_msg != null && prc_msg != "undefined" ) prc_msg.innerHTML = "&nbsp;파일 다운로드 중....";
		}
		else if ( Type == 4 ) //EVENTDISTRIBUTE
		{
			//tot_ditem.innerHTML = "&nbsp;" + CurIndex + "/" + TotalCnt;		
			
			var tpos = DownFileName.lastIndexOf("/") + 1;
			var Fname = DownFileName.substr(tpos,DownFileName.length - tpos);

			item_nm.innerHTML =  "["+Fname+"]";
		
			if ( prc_msg != "" && prc_msg != null && prc_msg != "undefined" ) prc_msg.innerHTML = "&nbsp;실제 경로 배포 중....";
		}	
		

}


</SCRIPT>
 
<!--ItemSize는 미지정 -->
<SCRIPT language=JavaScript for=MiInstaller event=OnEndDownLoad(VersionFileName,DownFileName,Type,TotalCnt,CurIndex)>
{
		var Progress = document.getElementById("progress");
		var Distproc = document.getElementById("Distproc");
		var t_err = document.getElementById("t_err");
		var reinstall = document.getElementById("reinstall");	
		var prc_msg = document.getElementById("prc_msg");	
			
		if ( Type == 1 ) //EVENTCONFIG
		{
			//alert(VersionFileName + "," + DownFileName);
			
			if ( !IsError ) {

				TotalVersionFileCnt = TotalCnt;
				progress_update(progress,processWidth/1.5);
				progress_update(Distproc,processWidth/1.5);
				if ( prc_msg != "" && prc_msg != null && prc_msg != "undefined" ) prc_msg.innerHTML = "&nbsp;설치 완료";
		
				fn_run();
				//history.back();
			}
			
		}
		else if ( Type == 3 )//EVENTDOWNLOAD
		{
		    var before_At = pg_cell_At;
					pg_cell_At = parseInt( ( (CurIndex)/TotalCnt ) * processWidth );
					progress_update(progress,pg_cell_At/1.5);
			
			if ( IsError ) {
					t_err.className = "show";
					reinstall.className = "show";
			}		
		
			if ( prc_msg != "" && prc_msg != null && prc_msg != "undefined" ) prc_msg.innerHTML = "&nbsp;파일 다운로드 완료....";
		}
		else if ( Type == 4 )//EVENTDISTRIBUTE
		{
		    var before_At = pg1_cell_At;
			  	pg1_cell_At = parseInt( ( (CurIndex)/TotalCnt ) * processWidth );
					progress_update(Distproc,pg1_cell_At/1.5);
					
			if ( IsError ) {
					t_err.className = "show";
					reinstall.className = "show";
			}		
			
			if ( prc_msg != "" && prc_msg != null && prc_msg != "undefined" ) prc_msg.innerHTML = "&nbsp;실제 경로 배포 완료";
		}
}
</SCRIPT>

<SCRIPT language=JavaScript for=MiInstaller event=OnError(ItemName,ErrorCode,ErrorMsg)>
{
		var err_msg = document.getElementById("err_msg");
		var err_msg_title = document.getElementById("err_msg_title");
		var t_err = document.getElementById("t_err");
		var reinstall = document.getElementById("reinstall");	
		var prc_msg = document.getElementById("prc_msg")+" " ;	
			
		IsError = true;
		err_msg.innerHTML += '<font class="f2" color="red">' + ItemName + '&nbsp;다운&nbsp;실패&nbsp; -- ErrorCode:' + ErrorCode + ' ' + ErrorMsg + "<br>" +
				'<font color="black"><br>▣ 수동설치시 <a href="javascript:popInstallMemo();" style="font-weight:bold; text-decoration: underline; color:f28a25">여기</a> 를 클릭하시기 바랍니다.</font><br>';
		err_msg_title.innerHTML += '▣ 설치시 에러가 발생한 항목';
		t_err.className = "show";
		reinstall.className = "show";
		var MiInstaller = document.getElementById("MiInstaller");
		MiInstaller.stop();
}

</SCRIPT>

</HEAD>

<BODY leftmargin="0" topmargin="0" onload="fn_OnLoad()" onunload="modalExit()" style="background:#ffffff;margin:0px; " >
	
<SCRIPT LANGUAGE="JavaScript">
	CreateMiInstlr("MiInstaller","Win32","3.2","mls");	
</SCRIPT>
<div id="wrap" style="background:#ffffff; margin:0px; padding:0px;" align="center" valign="middle">
<!-- 
<div id="wrap2" style="width=710px; height=500px;margin:0px; " >
<div id="wrap3" style="background:#ffffff; margin:0px; overflow:scroll; overflow-x:hidden; width=710px; height=500px;">
 -->
<div id="wrap2" style="width=460px; height=265px;margin:0px; " >
<div id="wrap3" style="background:#ffffff; margin:0px; overflow:scroll; overflow-x:hidden; width=460px; height=265px;">

	<div class="popupContents">
		<div class="component2"><span class="hide">저작권찾기 홈페이지에 필요한 컴포넌트를 설치 중입니다. 잠시만 기다려 주십시오.</span></div>
		
		<!-- div class="progress"><span class="hide">상태바</span></div -->

			
		<table width="400" align="center" cellpadding="0" cellspacing="5">
			<tr height="5">
				<td width="200"  style="font-size:11px;"  colspan="2">▣ 파일설치 진행 상황</td>
				<td id="tot_item"  style="font-size:11px;"  align="right"></td>
			</tr>			
			<tr>
				<td colspan="3">
					<div  valign="middle" style="width:393px;font-size:3px;padding:1px;border:1px GROOVE silver;">
						<IMG ID="progress" SRC='/images/install/installbar.jpg' WIDTH=0% HEIGHT=10 >
					</div>
				</td>
			</tr>
			<tr height="1">
				<td colspan="3"></td>
			</tr>
			<tr height="5">
				<td  width="60" style="font-size:11px;" >▣ 대상파일</td>
				<td id="item_nm" style="font-size:11px;" align=left class="f2" NOWRAP></td>
				<td id="prc_msg"  width="200"  style="font-size:11px;"  align="right"></td>
			</tr>
		
			<tr>
				<td colspan="3">
					<div  style="width:393px; font-size:2px;padding:1px;border:1px GROOVE silver;">
						<IMG ID="Distproc" SRC='/images/install/installbar.jpg' WIDTH=0% HEIGHT=10 >
					</div>
				</td>
			</tr>
			
			<tr>
				<td height="3" id=err_msg_title colspan="3" style="font-size:11px;" ></td>
			</tr>
			<tr>
				<td height="3" id=err_msg colspan="3" style="font-size:11px;" >&nbsp;</td>
			</tr>
		</table>

		
		<table width="400" border=0 align="center"  cellpadding="0" cellspacing="5">
			<tr>
				<td  style="font-size:11px;">컴퓨터 및 네트웍의 상황에 따라 많은 시간이 소요되는 경우가 있습니다.
					<br />잠시 기다려 주십시오...(정보인증 보안모듈 설치중)
					<br />
					<br /><span class="comment" style="font-size:11px;">설치도중 '보안경고' 창이 뜨면 "예" 눌러서 S/W를 설치하세요.</span>
					<br />만약 오랜 시간이 지나도 설치되지 않으면 저작권찾기 홈페이지에 필요한 컴포넌트를 수동으로 설치한 후에 다시 접속해 주십시오. 
					수동설치시 <a href="javascript:popInstallMemo();" style="font-weight:bold; text-decoration: underline; color:f28a25">여기</a> 를 클릭하시기 바랍니다.
				</td>
			</tr>
		</table>
	</div>
	<!-- div class="popupBottom"><img src="/images/button/close_btn.gif" alt="닫기" width="76" height="23" /></div -->
</div>
</div>
</div>
</body>
</html>
