<%
	Dim jsFile
	//여기서 MDI가 아닌 SDI인 경우에는 MiPlatform320_SX.js를 include하세요.
	if request("Device") = "U" then
		jsFile = "Miplatform320_MXU.js"  //MiPlatform320_SXU.js
	else
		jsFile = "Miplatform320_MX.js"   //MiPlatform320_SX.js
	end if
%>
<HTML>
	<HEAD>
		<title>Next</title>
<style>
	.f2 { font-size:12 }
</style>		
	<SCRIPT LANGUAGE="javascript">
		function fn_load()
		{	
			MiPlatformCtrl_320MX.Key = "TobeNext_U";
			MiPlatformCtrl_320MX.autosize = false;
      			MiPlatformCtrl_320MX.startxml = "http://www.miplatform.com/next/UI/Next_320.xml";
			MiPlatformCtrl_320MX.enableScroll = false;
			MiPlatformCtrl_320MX.WantAllKeys = "True";
      
//      alert(" Before DoRun():: ");
      
			MiPlatformCtrl_320MX.DoRun();						

		}
		
  function fn_logclear()
  {
  	prc_msg.innerHTML = "";
  }
		
	</SCRIPT>	
	
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="FormSize(strSendFormID, lwidth, lheight)">
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="ShowStatusBar(strSendFormID, bShow)">
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="NewWindow(strSendFormID, strID, strUrl, strArguement, strOpenStyle, lleft, ltop, lwidth, lheight, pbHandleFlag)">
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="Close(strSendFormID)">
	</SCRIPT>	
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="AddLogView(strSendFormID, strMsg)">
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="Communication(strSendFormID, bStart)">
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="JobCompleted(strSendFormID,strReqID,strServiceID,strURL)">
//		alert("JobCompleted====>" + strReqID);
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="ShowTitleBar(strSendFormID,bShow)">
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="Initialize(strSendFormID, plhHostwnd)">
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="LoadMenu(pICyDom, strStartXML, strURL)">
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="Activate(strSendFormID)">
	</SCRIPT>	
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="BeforeExitApplication(bExitFlag)">
	</SCRIPT>
    	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="ExitApplication()">
    		window.close();
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="PlatformError(errorcode, errormsg)">
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="BeforeNavigate(disp,strReqID,strServiceID,strUrl)">
//		alert("BeforeNavigate====>" + strReqID);
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="NavigateComplete(disp,strReqID,strServiceID,strUrl)">
//		alert("NavigateComplete====>" + strReqID);
	</SCRIPT>
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="LoadCompleted(strSendFormID, strReqID, strServiceID, strURL)">
//		alert("LoadCompleted====>" + strReqID);
	</SCRIPT>
	
	<SCRIPT LANGUAGE=javascript FOR=MiPlatformCtrl_320MX EVENT="UserNotify(nNotifyID,strMsg)">
		if ( nNotifyID == 100 ) window.close();
	</SCRIPT>
	
	</HEAD>
	<body topMargin="2" onload="fn_load()" scroll="auto">
    <script language="javascript" src="<%=jsFile%>" ></script>
	</body>
</HTML>
