<%
	Dim Mode
	
	Mode = Request("mode")
	
%>
<?xml version='1.0' encoding="euc-kr" ?>
<root>
	<params>
		<param id='DEVICE' type='string'>win32</param>
		<param id='SOURCE' type='string'>http://www.miplatform.com/Next/miplatform320U/basic_files</param>
		<param id='TARGET' type='string'>%Engine%</param>
		<param id='FILENAME' type='string'></param>
		<param id='VERSION' type='string'>system::8,0,50727,762</param>
		<param id='ACTION' type='string'>DOWN</param>
		<param id='STATUS' type='string'>0</param>
	</params>
	<dataset id="win32">
		<colinfo id='DEVICE' type='string' size='255'/>
		<colinfo id='SOURCE' type='string' size='255'/>
		<colinfo id='TARGET' type='string' size='255'/>
		<colinfo id='FILENAME' type='string' size='255'/>
		<colinfo id='VERSION' type='string' size='255'/>
		<colinfo id='ACTION' type='string' size='255'/>
		<colinfo id='STATUS' type='string' size='2'/>
<%
	if Mode = "VISTA" then
%>		
		<record><FILENAME>MiPlatformSvc3U.cab</FILENAME>
			<TARGET>{4000E52B-7283-4a5e-90FF-08323F262BD7}</TARGET>
			<VERSION>system::2007,1,28,1</VERSION>
		</record>

<%
	elseif Mode = "XP" then
%>		
		<record>
			<FILENAME>vcredist_x86.exe</FILENAME>
			<TARGET>x86_Microsoft.VC80.CRT_1fc8b3b9a1e18e3b\msvcr80.dll;x86_Microsoft.VC80.MFC_1fc8b3b9a1e18e3b\mfc80u.dll</TARGET>
			<VERSION>8,0,50727,762;8,0,50727,762</VERSION>
			<ACTION>WinSxS</ACTION>
			<ARGUMENT>/Q</ARGUMENT>
		</record>
<%
	else 
%>		
		<record><FILENAME>mfc80u.dll</FILENAME><VERSION>system::8,0,50727,762</VERSION></record>
		<record><FILENAME>msvcr80.dll</FILENAME><VERSION>system::8,0,50727,762</VERSION></record>
<%
	end if
%>		
	</dataset>
</root>