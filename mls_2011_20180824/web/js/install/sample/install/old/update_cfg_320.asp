<%
	Dim Mode
	
	Mode = Request("mode")
	
%>
<?xml version='1.0' encoding="euc-kr" ?>
<root>
	<params>
	<param id='VERSION' type='string'>2007,3,9,1</param>
	<param id='Config' type='string'>true</param>
	</params>
	
	<dataset id='win32'>
		<colinfo id='SOURCE' type='string' size='256'/>
		<colinfo id='DEVICE' type='string' size='256'/>
		<colinfo id='VERSION' type='string' size='256'/>
		<colinfo id='STATUS' type='string' size='2'/>

<%
	if Mode = "VISTA" then
%>		
		<record>
			<DEVICE>Win32</DEVICE>
			<SOURCE>update_msi_vista.xml</SOURCE>
		</record>
		<record>
			<DEVICE>Win32</DEVICE>
			<SOURCE>update_etc_files_vista.xml</SOURCE>
		</record>

<%
	else 
%>		
	<record>
		<DEVICE>Win32</DEVICE>
		<SOURCE>update_msi.xml</SOURCE>
	</record>
	<record>
		<DEVICE>Win32</DEVICE>
		<SOURCE>update_engine_files.xml</SOURCE>
	</record>
	<record>
		<DEVICE>Win32</DEVICE>
		<SOURCE>update_etc_files.xml</SOURCE>
	</record>
<%
	end if
%>		
	<record>
		<DEVICE>Win32</DEVICE>
		<SOURCE>update_component_files.xml</SOURCE>
	</record>

	</dataset>
</root>
