<%
	Dim uacMode 
	
	uacMode = request("UAC")
%>
<?xml version='1.0' encoding='euc-kr'?>
<root>
<params> <!--디폴트값설정-->
	<param id='DEVICE' type='string'>Win32</param>
	<param id='SOURCE' type='string'>http://www.miplatform.co.kr/Next/install/miplatform320/files_msi</param>
	<param id='TARGET' type='string'>C:\Temp\</param>
	<param id='VERSION' type='string'>2006,3,16,1</param>
	<param id='ACTION' type='string'>DOWNCHECK</param>
	<param id='STATUS' type='string'>0</param>
</params>
<dataset id='output'>
	<colinfo id='DEVICE' type="string" size='255'/>
	<colinfo id='SOURCE' type="string" size='255'/>
	<colinfo id='TARGET' type="string" size='255'/>
	<colinfo id='FILENAME' type="string" size='255'/>
	<colinfo id='VERSION' type="string" size='255'/>
	<colinfo id='ACTION' type="string" size='255'/>
	<colinfo id='ARGUMENT' type="string" size='255'/>
	<colinfo id='STATUS' type="string" size='2'/>
	<record>
		<DEVICE>Win32</DEVICE>
		<ACTION>MSI</ACTION>
		<ARGUMENT>/qb</ARGUMENT> 
		<TARGET>{3964575C-D828-4587-AED1-E538EAAFC083}</TARGET>
		<FILENAME>MiPlatform_InstallBase320.msi</FILENAME>
		<VERSION>System::3.20.10</VERSION>
	</record>	
	<record>
		<DEVICE>Win32</DEVICE>
		<ACTION>MSI</ACTION>
		<ARGUMENT>/qb</ARGUMENT> 
		<TARGET>{25DD76DB-7288-4EC4-9592-0E6BF5F32E58}</TARGET>
		<FILENAME>MiPlatform_InstallEngine320A.msi</FILENAME>
		<PATCHFILENAME>MiPlatform_InstallEngine320.msp</PATCHFILENAME>
		<OLDVERSION>System::3.20.60</OLDVERSION>
		<VERSION>System::3.20.70</VERSION>
	</record>
<% if 	uacMode = "N" then %>

	<record>
		<DEVICE>Win32</DEVICE>
		<ACTION>MSI</ACTION>
		<ARGUMENT>/qb</ARGUMENT> 
		<TARGET>{7AD1C170-64DF-4253-8A10-595290DAC3E7}</TARGET>
		<FILENAME>MiPlatform_FixUACProblem320A.msi</FILENAME>
		<VERSION>System::3.20.60</VERSION>
	</record>	
<% end if %>
	<record>
		<DEVICE>Win32</DEVICE>
		<ACTION>MSI</ACTION>
		<ARGUMENT>/qb</ARGUMENT> 
		<TARGET>{3CD02841-0CF2-46D2-8D8A-DA6C6C8C9483}</TARGET>
		<FILENAME>VsReport.msi</FILENAME>
		<VERSION>System::3.2.50</VERSION>
	</record>  
<!--
	<record>
		<DEVICE>Win32</DEVICE>
		<ACTION>MSI_</ACTION>
		<ARGUMENT>/qb</ARGUMENT> 
		<TARGET>{5A5AB07F-2E03-405D-8DAF-1DB38D1DE14A}</TARGET>
		<FILENAME>MiPlatform_Updater320.msi</FILENAME>
		<VERSION>System::3.20.70</VERSION>
		<OLDVERSION>System::3.20.60</OLDVERSION>
	</record>	
-->
</dataset>
</root>
