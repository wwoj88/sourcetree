<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html;charset=euc-kr" %>
<HTML>
<HEAD>
<TITLE>::저작권찾기::</TITLE>
<meta http-equiv="pragma" content="no-cache">
<link rel="stylesheet" href="/css/installStyle.css" type="text/css">
<link href="/css/popup.css" rel="stylesheet" type="text/css"/>
</HEAD>
<BODY leftmargin="0" topmargin="0">
	<div id="wrap">
		<table width="604" border="0" align="center" cellpadding="0" cellspacing="5">
			<tr>
				<td>
					<ul>
						<li><a href="/install/MiPlatform_SetupDeploy320A.exe" style="font-weight:bold; text-decoration: underline; color:f28a25">여기</a>를 눌러 설치 프로그램을 다운로드하며, 파일로 저장하십시오.</li>
						<li>실행중인 웹브라우저 모두 종료하십시오.</li>
						<li>파일의 압축을 풀고 설치 프로그램을 실행하십시오.</li>
						<li>자세한 설치사항은 ReadMe.txt를 참고하십시오.</li>
						<li>장애시 문의전화: 02-538-6000</li>
					</ul>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
					<ul>
						<li style="font-weight:bold; color:f28a25">[참고]WindowsXP SP2인 경우 웹브라우져의 '도구>인터넷옵셥>보안>사용자지정수준'에서 아래 항목을 모두 '사용'으로 설정해주시기 바랍니다.</li>
						<li class="none">
							<ul>
								<li>1."ActiveX컨트롤에 대해 자동으로 확인"(WindowsXP인 경우)</li>
								<li>2."ActiveX컨트롤 및 플로그 인 실행"</li>
								<li>3."안전한 것으로 표시된 "ActiveX컨트롤 스크립트"</li>
								<li>4."서명된 "ActiveX컨트롤 다운로드"</li>
								<li>5."바이너리 및 스크립트 동작"</li>
							</ul>
						</li>
					</ul>
				</td>
			</tr>
		</table>
	</div>
	<div class="popupBottom"><img src="/images/button/close_btn.gif" alt="닫기" width="76" height="23" /></div>
</BODY>
</HTML>