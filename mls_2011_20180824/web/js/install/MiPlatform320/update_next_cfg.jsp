<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html;charset=euc-kr" %>
<%
  String  mode = request.getParameter("mode");
%>
<?xml version='1.0' encoding="euc-kr" ?>
<root>
	<params>
	<param id='VERSION' type='string'>2007,3,9,1</param>
	<param id='Config' type='string'>true</param>
	</params>
	
	<dataset id='win32'>
		<colinfo id='SOURCE' type='string' size='256'/>
		<colinfo id='DEVICE' type='string' size='256'/>
		<colinfo id='VERSION' type='string' size='256'/>
		<colinfo id='STATUS' type='string' size='2'/>
<%  if( mode.equals("VISTA")  )  {  %>
	<record>
		<DEVICE>Win32</DEVICE>
		<SOURCE>update_msi.xml</SOURCE>
	</record>
<% }else{ %>
	<record>
		<DEVICE>Win32</DEVICE>
		<SOURCE>update_xp.xml</SOURCE>
	</record>
	<record>
		<DEVICE>Win32</DEVICE>
		<SOURCE>update_engine.xml</SOURCE>
	</record>
	<record>
		<DEVICE>Win32</DEVICE>
		<SOURCE>update_ocx_files.xml</SOURCE>
	</record>
<% } %>
	<record>
		<DEVICE>Win32</DEVICE>
		<SOURCE>update_etc_files.xml</SOURCE>
	</record> 
	<record> 
		<DEVICE>Win32</DEVICE>
		<SOURCE>update_component_files.xml</SOURCE>
	</record>

	</dataset>
</root>
