<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html;charset=euc-kr" %>
<HTML>
<HEAD>
<TITLE></TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link rel="stylesheet" href="/css/installStyle.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" src="/install/js/MiInstaller321.js"></SCRIPT>
<script language="vbscript">
	//---------------------------------------------------------------------------------
	// ActiveX Setup Check
	//---------------------------------------------------------------------------------	
	Function  Check_Module
	  
	  	On Error Resume Next
	  	
	   	Err.number = 0
	   	chkMsg= document.MiInstaller.Version
	  	if  Err.number = 438 Then	
			Check_Module = "false"
	  	Else
	        Check_Module = "true"
	  	End If
	End Function 
</script>

<SCRIPT LANGUAGE="JavaScript">
	var GcurIndx = 0;
	var Gtotcnt = 0;
	
	var TotalVersionFileCnt;
	var TotalItemCnt;
	var progressColor = "red";	// set to progress bar color
	var pg_cell_At = 0,pg1_cell_At = 0; 
	var IsError = false;
	var proMsg, procMsg;

	var proMsg, procMsg;;
	var TotalVersionFileCnt;
	var TotalItemCnt;
	var flagDISTRIBUTE;
	var cur_version_file;
	var Server_Path = window.location.href;
	Server_Path = Server_Path.substring(0, Server_Path.lastIndexOf("/")) + "/";	
	var processWidth = 590;
	
	var MiInstallPath = 'http://'+location.host+'/install/info/';
	var MiStartXMLPath = 'http://'+location.host+'/MYKEY.xml';	
//	var MiStartXMLPath = 'http://localhost/mls_admin_ci_main_Win32.xml';
		
	//---------------------------------------------------------------------------------
	// Load시 Version Check 및 Install
	//---------------------------------------------------------------------------------
	function fn_OnLoad()
	{
		
		if ( Check_Module() == "true" )
		{
			
			var MiInstaller = document.getElementById("MiInstaller");
			
			MiInstaller.Key = "mls";
			MiInstaller.Launch = true;	
			MiInstaller.Width  = 1024;
			MiInstaller.Height	= 768;
			MiInstaller.Retry = 0;
			MiInstaller.Timeout = 300;
			MiInstaller.GlobalVal = "";
			MiInstaller.OnlyOne = true;
			MiInstaller.ComponentPath = "%UserApp%TobeSoft\\mls\\component";
			MiInstaller.StartXML  = MiStartXMLPath;
			//MiInstaller.Resource = "%component%resource.xml";	
			//MiInstaller.StartImage  = "%component%next_start.gif";

			//MiInstaller.NotLaunchForErr = "true";
			var UpdateUrl = "";
	  	var mode = checkOS();
			if ( mode == "VISTA" )
			{
				UpdateUrl = "Info_Vista.xml";
			} else {
				UpdateUrl = "Info_XP.xml";
			}
			
			MiInstaller.UpdateURL = MiInstallPath + UpdateUrl;
			var Bcnt = MiInstaller.ExistVersionUpCnt(); 
			if ( Bcnt )
			{
				MiInstaller.StartDownLoad();
			} else {
				fn_run();
			}

			} 
		else 
		{
			//---------------------------------------------------------------------------------
			// ActiveX 설치 되지 않은 경우
			//---------------------------------------------------------------------------------
			IsError = true;

			err_msg.innerHTML += '<font color="red">Unable to Install ActiveX Control from Web Sites !!!<br><br>' + 
					'<img src="/images/install/bu_comment02.gif" width="14" height="11">&nbsp;&nbsp;<font color="black">수동설치시 <a href="install/MiPlatform_SetupDeploy320U_200901.exe"><b>여기</b></a> 를 클릭하시기 바랍니다.</font>';

			t_err.className = "show";
			reinstall.className = "show";
		}

	}

	function page_link()
	{
	
		// 단축 아이콘 만들기
		// UBKImage : 250 * 300
		
		//BYTE가 256을 넘으면 안됨..
		// 256을 넘을 경우 MiInstaller의 property를 이용할것

		var strCommand = '-V 3.2 -D Win32U -R FALSE -K KORAIL -L TRUE -LE TRUE -BI "%component%next_upd.gif" ';
		var MiInstaller = document.getElementById("MiInstaller");
		MiInstaller.MakeShortCut("%system%MiUpdater321.exe",strCommand,"제주관광공사","%Component%icon_next.ico","");
		
		// MakeShortCut 바로가기를 만드는 함수
		// strExeName: 바로가기를 만들 실행 파일 이름
		// strCommand: 바로가기를 만들 때 필요한 Command 정보
		// strShortcutName: 바로가기 파일 이름
		// strIConPath: 변경하려는 Icon 경로를 %alias%형태로 입력할 수 있습니다.
		// strPos: Startmenu-시작 / Desktop-바탕화면(Default)
	
		// Alias 참고
		// %MiPlatform%
		// %Component%
		// %system%
		// %Window%
		// %ProgramFiles%
	
	} 

	function fn_run()
	{
		if ( IsError ) return ;
		
		//단축 아이콘 생성
		//page_link();

    //location.href = "/main/index.jsp";
		
	}


	function progress_clear(obj) {
		obj.width = 0;
		if ( obj.id == "progress" ) pg_cell_At = 0;
		else pg1_cell_At = 0;		
	}
	
	function progress_update(obj,cur_cnt) {
		
		if ( obj.id == "progress" ) obj.width = cur_cnt;
		else obj.width = cur_cnt;
	}
	
	
</SCRIPT>

<SCRIPT language=JavaScript for=MiInstaller event=OnConfirm(ItemName)>
{
	
	var a;
	a  = "OnConfirm=>Item=";
	a += ItemName;	
	//alert(a);
}
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=MiInstaller EVENT=OnProgress(ItemName,prgress,progressMax,StatusText)>
{
			
	var Progress = document.getElementById("progress");
	if(ItemName != "")	{
		if( progressMax > 0 && prgress > 0 )	{
		        var before_At = pg_cell_At;

		        pg_cell_At = parseInt( ( ( ( GcurIndx - 1)/Gtotcnt ) * processWidth ) +  ( ( ( 1/Gtotcnt ) * processWidth ) * (prgress/progressMax) ) );
		        if ( pg_cell_At > processWidth ) pg_cell_At = processWidth;
						progress.width = pg_cell_At;
		}
	}
}
</SCRIPT>

<SCRIPT language=JavaScript for=MiInstaller event=OnStartDownLoad(VersionFileName,DownFileName,Type,TotalCnt,CurIndex)>
{
		var Progress = document.getElementById("progress");
		var Distproc = document.getElementById("Distproc");
		var tot_item = document.getElementById("tot_item");
		var item_nm = document.getElementById("item_nm");	
		var prc_msg = document.getElementById("prc_msg");	

		if ( Type == 1 ) //EVENTCONFIG
		{
			//progress_clear(progress);
			//progress_clear(progress1);
		}
		else if ( Type == 2 ) 
		{
				progress.width = 0;
				Distproc.width = 0;
				tot_item.innerHTML = "&nbsp;" + CurIndex + "/" + TotalCnt;		
				//tot_ditem.innerHTML = "&nbsp;";
		
		} 
		else if ( Type == 3 ) //EVENTDOWNLOAD
		{
			GcurIndx = CurIndex;
			Gtotcnt = TotalCnt;
			tot_item.innerHTML = "&nbsp;" + CurIndex + "/" + TotalCnt;		
			
			var tpos = DownFileName.lastIndexOf("/") + 1;
			var Fname = DownFileName.substr(tpos,DownFileName.length - tpos);

			item_nm.innerHTML = "&nbsp;" + Fname;
		
			if ( prc_msg != "" && prc_msg != null && prc_msg != "undefined" ) prc_msg.innerHTML = "&nbsp;파일 다운로드 중....";
		}
		else if ( Type == 4 ) //EVENTDISTRIBUTE
		{
			//tot_ditem.innerHTML = "&nbsp;" + CurIndex + "/" + TotalCnt;		
			
			var tpos = DownFileName.lastIndexOf("/") + 1;
			var Fname = DownFileName.substr(tpos,DownFileName.length - tpos);

			item_nm.innerHTML = "&nbsp;" + Fname;
		
			if ( prc_msg != "" && prc_msg != null && prc_msg != "undefined" ) prc_msg.innerHTML = "&nbsp;실제 경로 배포 중....";
		}	
		

}


</SCRIPT>
 
<!--ItemSize는 미지정 -->
<SCRIPT language=JavaScript for=MiInstaller event=OnEndDownLoad(VersionFileName,DownFileName,Type,TotalCnt,CurIndex)>
{
		var Progress = document.getElementById("progress");
		var Distproc = document.getElementById("Distproc");
		var t_err = document.getElementById("t_err");
		var reinstall = document.getElementById("reinstall");	
		var prc_msg = document.getElementById("prc_msg");	
			
		if ( Type == 1 ) //EVENTCONFIG
		{
			//alert(VersionFileName + "," + DownFileName);
			
			if ( !IsError ) {

				TotalVersionFileCnt = TotalCnt;
				progress_update(progress,processWidth);
				progress_update(Distproc,processWidth);
				if ( prc_msg != "" && prc_msg != null && prc_msg != "undefined" ) prc_msg.innerHTML = "&nbsp;설치 완료";
		
				fn_run();
				//history.back();
			}
			
		}
		else if ( Type == 3 )//EVENTDOWNLOAD
		{
		    var before_At = pg_cell_At;
					pg_cell_At = parseInt( ( (CurIndex)/TotalCnt ) * processWidth );
					progress_update(progress,pg_cell_At);
			
			if ( IsError ) {
					t_err.className = "show";
					reinstall.className = "show";
			}		
		
			if ( prc_msg != "" && prc_msg != null && prc_msg != "undefined" ) prc_msg.innerHTML = "&nbsp;파일 다운로드 완료....";
		}
		else if ( Type == 4 )//EVENTDISTRIBUTE
		{
		    var before_At = pg1_cell_At;
			  	pg1_cell_At = parseInt( ( (CurIndex)/TotalCnt ) * processWidth );
					progress_update(Distproc,pg1_cell_At);
					
			if ( IsError ) {
					t_err.className = "show";
					reinstall.className = "show";
			}		
			
			if ( prc_msg != "" && prc_msg != null && prc_msg != "undefined" ) prc_msg.innerHTML = "&nbsp;실제 경로 배포 완료";
		}
}
</SCRIPT>

<SCRIPT language=JavaScript for=MiInstaller event=OnError(ItemName,ErrorCode,ErrorMsg)>
{
		var err_msg = document.getElementById("err_msg");
		var t_err = document.getElementById("t_err");
		var reinstall = document.getElementById("reinstall");	
		var prc_msg = document.getElementById("prc_msg");	
			
		IsError = true;
		err_msg.innerHTML += '<font class="f2" color="red">' + ItemName + '&nbsp;다운&nbsp;실패&nbsp; -- ErrorCode:' + ErrorCode + ' ' + ErrorMsg + "<br>" +
				'<img src="/images/install/bu_comment02.gif" width="14" height="11">&nbsp;&nbsp;<font color="black">수동설치시 <a href="install/MiPlatform_SetupDeploy320U_200901.exe"><b>여기</b></a> 를 클릭하시기 바랍니다.</font>';
		
		t_err.className = "show";
		reinstall.className = "show";
		var MiInstaller = document.getElementById("MiInstaller");
		MiInstaller.stop();
}

</SCRIPT>

</HEAD>
<BODY leftmargin="0" topmargin="0" onload="fn_OnLoad()">
	
<SCRIPT LANGUAGE="JavaScript">
	CreateMiInstlr("MiInstaller","Win32","3.2","mls");	
</SCRIPT>
	
<br><br>
<center>
<table id="ins_tbl" align="center" border=0 width="600">
  <tr>
    <td><table width="637" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr> 
          <td height="10"></td>
        </tr>
        <tr> 
          <td><img src="/images/install/insu_down_listbg1.gif" width="637" height="11"></td>
        </tr>
        <tr> 
          <td height="37" bgcolor="#EEEEEE"> <table width="607" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td height="15"></td>
              </tr>
              <tr> 
                <td><table width="590" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr> 
                      <td width="10" rowspan="3"><img src="/images/install/insu_down_img.gif" width="150" height="90"></td>
                      <td width="590" height="35"><img src="/images/install/insu_down_title.gif" width="204" height="27"></td>
                    </tr>
                    <tr> 
                      <td height="45"><img src="/images/install/insu_down_title2.gif" width="329" height="32"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><table width="607" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="12" height="12"><img src="/images/install/insu_down_listbg_sub1.gif" width="12" height="12"></td>
                      <td width="583" height="12" bgcolor="#FFFFFF"></td>
                      <td width="12" height="12"><img src="/images/install/insu_down_listbg_sub2.gif" width="12" height="12"></td>
                    </tr>
                    <tr> 
                      <td rowspan="5" bgcolor="#FFFFFF">&nbsp;</td>
                      <td bgcolor="#FFFFFF" height="5"></td>
                      <td rowspan="5" bgcolor="#FFFFFF">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td bgcolor="#FFFFFF" height="5">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td bgcolor="#FFFFFF">
                        <table width="570" border="0" align="center" cellpadding="0" cellspacing="0">
                          <tr> 
                            <td width="10" valign="top"><img src="/images/install/insu_down_list_icon1.gif" width="8" height="11"></td>
                            <td width="570">윈도우 XP 서비스팩2 사용자께서는 주소 표시줄 아래 경고문구 
                              “ <img src="/images/install/insu_down_list_icon2.gif" width="15" height="15" align="absmiddle">이 
                              사이트에서...여기를 클릭하십<br>
                              시오” 를 선택하시어, Active X컨트롤을 설치하시기 바랍니다.</td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr> 
                      <td bgcolor="#FFFFFF">&nbsp;</td>
                    </tr>
                    <tr> 
                    <td bgcolor="#FFFFFF">
                    <table width="570" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr> 
                            <td width="10" valign="top"><img src="/images/install/insu_down_list_icon1.gif" width="8" height="11"></td>
                            <td width="570">프로그램 설치가 정상적이지 않을 경우에는 <strong><font color="#FF7200">수동설치</font></strong>를 
                              통해 수동설치하시거나 <strong><font color="#FF7200">문의하기</font></strong>를 
                              통해 질문<br>
                              하시면 신속히 답변을 드리겠습니다.</td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr> 
                      <td width="12" height="12"><img src="/images/install/insu_down_listbg_sub3.gif" width="12" height="12"></td>
                      <td bgcolor="#FFFFFF"></td>
                      <td width="12" height="12"><img src="/images/install/insu_down_listbg_sub4.gif" width="12" height="12"></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td><img src="/images/install/insu_down_listbg2.gif" width="637" height="11"></td>
        </tr>
        <tr>
          <td height="5"></td>
        </tr>
      </table></td>
  </tr>
</table>
</center>
<table  id="ins_tbl" align="center" border=0 width="600" >
<tr>
	<td>
	<table >
		<tr>
			<td class="f2"> 파일다운로드 진행 상황</td><td id="comp_cnt" class="f2" align=left NOWRAP>&nbsp;</td>
		</tr>
	</table>
	</td>
</tr>
<tr>
<td colspan=2 valign="middle" WIDTH=90%>
	<div  valign="middle" style="font-size:3px;padding:1px;border:1px GROOVE silver;">
	<IMG ID="progress" SRC='/images/install/installbar.jpg' WIDTH=0% HEIGHT=10 >
	</div>
</td>
</tr>
<tr>
	<td>
	<table>
		<tr>
		<td class="f2" >파일설치 진행 상황</td><td id="tot_item" align=left class="f2" NOWRAP>&nbsp;</td>
		</tr>
	</table>
	</td>		
</tr>
<tr>
	<td colspan=2 valign="middle" WIDTH=90%>
	<div  style="font-size:3px;padding:1px;border:1px GROOVE silver;">
	<IMG ID="Distproc" SRC='/images/install/installbar.jpg' WIDTH=0% HEIGHT=10 >
	</div>
	</td>
</tr>
<tr>
	<td>
	<table>
		<tr>
			<td class="f2">대상파일</td><td id="item_nm" class="f2" align=left NOWRAP>&nbsp;</td>
		</tr>
	</table>
	</td>		
</tr>
<tr>
	<td id=prc_msg class="f2" >&nbsp;<td>
</tr>	
</table>
<table class="hide" id=t_err align=center>
	<tr>
		<td class="f2">설치시 에러가 발생한 항목</td>
	</tr>
	<tr>
		<td id=err_msg class="f2">&nbsp;</td>
	</tr>
</table>

</BODY>
</HTML>
