<%@ page contentType="text/html; charset=euc-kr" language="java" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>저작권라이선스관리시스템 개편 안내</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<style type="text/css">
body {margin:0; padding: 0; font-size:0.75em; }
.open_pop {background: url(http://clms.or.kr/images2/popup/openpop_bg.gif) repeat-y left top;margin: 0; width:586px;}
.open_pop .intro_txt {background: #f6f5f1 url(http://clms.or.kr/images2/popup/open_pop.gif) no-repeat left top; margin:0; margin-top: 3px; color: #333; font-size:100%; word-wrap: break-word; padding: 190px 20px 30px 138px; }
.open_pop p {line-height: 1.5;}
.open_pop p em {color: #006184; font-style: normal; font-weight: bold; letter-spacing: -0.05em;  line-height:2;}
strong.point {color: #d72d2c;}
.open_pop .pop_btn {margin: 0; position: absolute; top: 287px; height: 35px; width: 482px; text-align: center;}
.open_pop .pop_btn img {margin-right: 3px;}
.open_pop .control {margin: 0;background: url(http://clms.or.kr/images2/popup/control_bg.gif) no-repeat right top; color: #FFF; font-size: 95%; padding: 5px 5px 10px; text-align:right;}
.open_pop .control p {margin: 0; padding: 0;}
.open_pop .control p a {color: #FFF; font-weight: bold; text-decoration: none;}
.open_pop .control input {margin: 0;}
.open_pop .control span a {position: absolute; right:0; padding: 5px 20px; color: #FFF; font-weight: bold;}
</style>

<script language="JavaScript">
<!-- 
    function getCookie() {
		var rtn = "";
		var search = "clmsmessage=";
		if (document.cookie.length>0){
			offset = document.cookie.indexOf(search);
			if (offset != -1){
				offset += search.length;
				end = document.cookie.indexOf(";", offset);
				if (end == -1)
					end = document.cookie.length;
				rtn = unescape(document.cookie.substring(offset, end));
			}
		}

		if(rtn == "done1") {
			window.close();
		}
    }    

	function setCookie(name, value, expiredays) {
		var todayDate = new Date();
		todayDate.setDate( todayDate.getDate() + expiredays );
		document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
	}

	function onClose() {
		if(document.event.first.checked == true) {
			setCookie("clmsmessage", "done1", 1);
		}
		window.close();
	}
//-->
</script>
</head>

<body >
<center>
<br>
<div class="open_pop" title="저작권라이선스관리시스템 오픈 안내">
<form name="event">
	<div class="intro_txt">
		<p><em>1) 서비스 중지 : 2009년 8월 27일 (목) 18:30 ~ 2009년 8월 30일(일) 24:00</em><br>
<em>2) 개편 오픈 : 2009년 8월 31일(월)</em></p>
		<p>CLMS 사이트는 기존 URL주소(http://www.clms.or.kr) 와 동일합니다.</p>
		<p>(구)저작권찾기(http://www.musiclicense.or.kr) 사이트는 폐쇄되오니<br>
		개편된 저작권찾기 사이트(<strong class="point">http://www.right4me.or.kr</strong>)로 접속하거나 CLMS의 저작권찾기 배너를 클릭하시기 바랍니다.</p>
		<p>※ 개편될 시스템의 이용 매뉴얼은 공지사항 및 자료실을 통해 확인 가능하시며, 추후 설명회 및 교육 일정은 별도 공지할 예정입니다.</p>
	</div>
	<!--
	<div class="control">
		<p class="btn">
		<label for="date_close">오늘 하루 열지 않음</label>
		<input type="checkbox" id="first" name="first"> 
		<a href="#" onclick="onClose();">CLOSE</a></p>
	</div>
	  -->
</form>	
<div>
</center>
</body>
</html>
