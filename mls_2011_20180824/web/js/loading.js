function loading(obj, target){		
// 페이지가 로딩될 때 'Loading 이미지'를 숨긴다.
		$(obj).hide();
	
		// ajax 실행 및 완료시 'Loading 이미지'의 동작을 컨트롤하자.
		$(obj).ajaxStart(function()
		{
			// 로딩이미지의 위치 및 크기조절	
			$(obj).css('position', 'absolute');
			/*
			$('#viewLoading').css('left', $('#content').offset().left);
			$('#viewLoading').css('top', $('#content').offset().top);
			$('#viewLoading').css('width', $('#content').css('width'));
			$('#viewLoading').css('height', $('#content').css('height'));
			*/
			$(obj).css('left', 200);
			$(obj).css('top', 150);
			$(obj).css('width', $(target).css('width'));
			$(obj).css('height', $(target).css('height'));
			//$(this).show();
			$(obj).fadeIn(500);
		})
		.ajaxStop(function()
		{
			//$(this).hide();
			$(obj).fadeOut(500);
		});
}