//*************************************************************************************
//	파일명		: sg_encrypt.js
//	작성자		: 고영주
//	최초 작성일	: 2003년 8월 6일
//	최종 수정일	: 2004년 2월 4일
//*************************************************************************************

function encryptSessionKey( strKeyID, strKmCert )
{
	if ( strKeyID == null || strKeyID == "" )
	{
		setErrorCode( "NO_USER_ID" );
		setErrorMessage( "" );
		setErrorFunctionName( "encryptSessionKey()" );
		return "";
	}

	if ( strKmCert == null || strKmCert == "")
	{
		setErrorCode("NO_DATA_VALUE");
		setErrorMessage("");
		setErrorFunctionName( "encryptSessionKey()" );
		return "";
	}

	var bReturn = GetSymmetricKey( strKeyID );
	if ( !bReturn )
	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "encryptSessionKey()" );
		return "";
	}

	var strEncryptedSessionKey = EncryptSymKey(strKmCert);
 	if ( strEncryptedSessionKey == "" )
   	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "encryptSessionKey()" );
		return "";
  	}

        return removeCRLF( strEncryptedSessionKey );
}

function decryptSessionKey( strKeyID, strEncryptedSessionKey )
{
	if ( strKeyID == null || strKeyID == "" )
	{
		setErrorCode( "NO_USER_ID" );
		setErrorMessage( "" );
		setErrorFunctionName( "decryptSessionKey()" );
		return false;
	}

	if ( strEncryptedSessionKey == null || strEncryptedSessionKey == "" )
	{
		setErrorCode("NO_DATA_VALUE");
		setErrorMessage( "" );
		setErrorFunctionName( "decryptSessionKey()" );
		return false;
	}

	var bReturn = selectCertificate( strKeyID );
	if ( !bReturn )
	{
		setErrorFunctionName( "decryptSessionKey()" );
		return false;
	}

	bReturn = DecryptSymKey( strEncryptedSessionKey );
	if( !bReturn )
	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "decryptSessionKey()" );
        	return false;
        }

	bReturn = SetSymmetricKey( strKeyID );
	if ( !bReturn )
	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "decryptSessionKey()" );
        	return false;
	}

	return true;
}

function encryptDataString( strKeyID, strMessage )
{
	if ( strKeyID == null || strKeyID == "" )
	{
		setErrorCode( "NO_USER_ID" );
		setErrorMessage( "" );
		setErrorFunctionName( "encryptDataString()" );
		return "";
	}

	if ( strMessage == null || strMessage == "" )
	{
		setErrorCode("NO_DATA_VALUE");
		setErrorMessage("");
		setErrorFunctionName( "encryptDataString()" );
		return "";
	}

	var bReturn = GetSymmetricKey( strKeyID );
	if ( !bReturn )
	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "encryptDataString()" );
		return "";
	}

	var strEncryptedData = EncryptData( strMessage );
	if ( strEncryptedData == "" )
	{                                                                    
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "encryptDataString()" );
		return "";
	}

	return removeCRLF( strEncryptedData );
}

function decryptDataString( strKeyID, strEncryptedMessage )
{
	if ( strEncryptedMessage == null || strEncryptedMessage == "" )
	{
		setErrorCode("NO_DATA_VALUE");
		setErrorMessage("");
		setErrorFunctionName( "decryptDataString()" );
		return "";
	}

	if ( strKeyID == null || strKeyID == "" )
	{
		setErrorCode("NO_USER_ID");
		setErrorMessage( "" );
		setErrorFunctionName( "decryptDataString()" );
		return "";
	}

	var bReturn = GetSymmetricKey( strKeyID );
	if ( !bReturn )
	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "decryptDataString()" );
		return "";
	}

	var strDecryptedData = DecryptData( strEncryptedMessage )
	if ( strDecryptedData == "" )
	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "decryptDataString()" );
		return "";
	}

	return strDecryptedData;
}

function encryptDataFile( strKeyID, strInputFilePath, strOutputFilePath )
{
	if ( strInputFilePath == null || strInputFilePath == ""
		|| strInputFilePath == null || strInputFilePath == "" )
	{
		setErrorCode("NO_DATA_VALUE");
		setErrorMessage("");
		setErrorFunctionName( "encryptDataFile()" );
		return false;
	}

	if ( strKeyID == null || strKeyID == "" )
	{
		setErrorCode("NO_USER_ID");
		setErrorMessage( "" );
		setErrorFunctionName( "encryptDataFile()" );
		return false;
	}

	var bReturn = GetSymmetricKey( strKeyID );
	if ( !bReturn )
	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "encryptDataFile()" );
		return false;
	}

        bReturn = EncryptFile( strInputFilePath, strOutputFilePath );
        if( !bReturn )
	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "encryptDataFile()" );
		return false;
	}

	return bReturn;
}

function decryptDataFile( strKeyID, strInputFilePath, strOutputFilePath )
{
	if ( strInputFilePath == null || strInputFilePath == ""
		|| strInputFilePath == null || strInputFilePath == "" )
	{
		setErrorCode("NO_DATA_VALUE");
		setErrorMessage("");
		setErrorFunctionName( "decryptDataFile()" );
		return false;
	}

	if ( strKeyID == null || strKeyID == "" )
	{
		setErrorCode("NO_USER_ID");
		setErrorMessage( "" );
		setErrorFunctionName( "decryptDataFile()" );
		return false;
	}

	var bReturn = GetSymmetricKey( strKeyID );
	if ( !bReturn )
	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "decryptDataFile()" );
		return false;
	}

	var bReturn = DecryptFile( strInputFilePath, strOutputFilePath );
        if( !bReturn )
	{
		setErrorCode( "" );
		setErrorMessage( GetLastErrMsg() );
		setErrorFunctionName( "decryptDataFile()" );
		return false;
	}

	return bReturn;
}
