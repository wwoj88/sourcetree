/*******************************************************************************
* 기능   : Common 정보 셋팅
* 인수   : ds - 해당프로그램 ID
           objCombo - inputdataset
           strCodeGubn - outputdataset
           strCodeFlag - 전체, 선택
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_getYear( dsObj )
{

	dsObj.ClearData();
	
	dsObj.AddRow();
	
	dsObj.SetColumn(dsObj.rowposition, "CODE",  "" );	
	dsObj.SetColumn(dsObj.rowposition, "DATA",  "-전체-" );	
			
	var strYear = SubStr(GetDate(),0,4);
    var k = 0;
    
	for(i=2006 ; i<=SubStr(GetDate(),0,4); i++)
	{
	
		dsObj.AddRow();
		
		dsObj.SetColumn(dsObj.rowposition, "CODE",  toNumber(strYear)-k );	
		dsObj.SetColumn(dsObj.rowposition, "DATA",  toNumber(strYear)-k+""+"년" );	
		k++;
	}		

}


/*******************************************************************************
* 기능   : Common 정보 셋팅
* 인수   : ds - 해당프로그램 ID
           objCombo - inputdataset
           strCodeGubn - outputdataset
           strCodeFlag - 전체, 선택
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_getYear1( dsObj )
{

	dsObj.ClearData();
	
	var strYear = SubStr(GetDate(),0,4);
    var k = 0;
    
	for(i=2006 ; i<=SubStr(GetDate(),0,4); i++)
	{
	
		dsObj.AddRow();
		
		dsObj.SetColumn(dsObj.rowposition, "CODE",  toNumber(strYear)-k );	
		dsObj.SetColumn(dsObj.rowposition, "DATA",  toNumber(strYear)-k+""+"년" );	
		k++;
	}		

	
}


/*******************************************************************************
* 기능   : Common 정보 셋팅
* 인수   : ds - 해당프로그램 ID
           objCombo - inputdataset
           strCodeGubn - outputdataset
           strCodeFlag - 전체, 선택
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_getMonth( dsObj )
{
	return   SubStr(GetDate(),4,2);
}




/*******************************************************************************
* 기능   : Common 정보 셋팅
* 인수   : ds - 해당프로그램 ID
           objCombo - inputdataset
           strCodeGubn - outputdataset
           strCodeFlag - 전체, 선택
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_addCombo(ds,  objCombo , strCodeGubn,  strCodeFlag )
{
	
	objCombo.InnerDataset = ds;
	
	gds_CommonCode.AddRow();
	gds_CommonCode.SetColumn(gds_CommonCode.rowposition, "codeGubun", strCodeGubn );
	gds_CommonCode.SetColumn(gds_CommonCode.rowposition, "codeFlag",  strCodeFlag );	
	gds_CommonCode.SetColumn(gds_CommonCode.rowposition, "outDs",     ds );	

}



/*******************************************************************************
* 기능   : CommonCode 조회 셋팅
* remark : gds_CommonCode
*******************************************************************************/
function gfn_comboSearch()
{


	inds  = "gds_CommonCode=gds_CommonCode ";

	var  outds = "";

	
	for( var i=0; gds_CommonCode.count > i ; i++ ) 
	{   
	    ds    = gds_CommonCode.GetColumn(i,"outDs");
		outds = outds + ds+"="+ds+ " ";
	}

    var strService = "commonCodeService";
	var strMethod  = "commonCodeList";
	var strArgs    = "";
		
	strArgs  = "service=" + quote(strService);
	strArgs += " method=" + quote(strMethod);

	gfn_Transaction( "", 
					 "comboInit",
					 inds,
					 outds,
					 strArgs );	
			 
}



/*******************************************************************************
* 기능   : Common Transcation 처리 
* 인수   : formid - 해당프로그램 ID
           inds - inputdataset
           outds - outputdataset
           strarg - Argument
* 리턴값 : None
* remark : OpenPage Dataset에 해당프로그램ID의 data가 있어야 함. 
           해당페이지 오픈시 Window에 추가 및 삭제 처리가 선행되어야 함.
*******************************************************************************/
function gfn_Transaction(formid, svcid, inds, outds, strarg)
{	

	inds  = "gds_common=gds_common " + inds;

	outds = outds;
	
	//http.TimeOut = 90;  // 디폴트
	//http.timeout = 360; // 6분
	http.timeout = 1800; // 30분
	
	
	Transaction(svcid, 
				"DEFAULT_SERVER::", 
				inds, 
				outds, 
				strarg, 
				"Tr_Callback");						
}

 

//------------------------------------------------------------------                                           
// 업무메뉴 클릭 WINDOW OPEN
// ex : gfn_user_NewForm("sample","SAM_WebEditor")
//      gfn_user_NewForm("sample","SAM_WebEditor","PARM=" + quote("ABC"))  
//------------------------------------------------------------------ 
function gfn_user_NewForm(Fprefix,Formid, param,  formName ) 
{ 

	var objForms = AllWindows(Formid);
    
    if( gds_windows.count == 10 )
    {
		alert("10개 이상의 화면은 열수 없습니다.");
		return;
    }
    
    
	// newform 이 없는경우                                                                                      
	if(objForms[0] == null) 
	{
		
		G_XMLNAME = Fprefix + "::" + Formid + ".xml";
		G_FORMID  = Formid;
	    
	    // form 정보를 저장
	    /*
		var row = gds_windows.addRow();
		gds_windows.SetColumn(row,"formid",Formid);
		gds_windows.SetColumn(row,"lev","0");
		gds_windows.SetColumn(row,"formName",formName);					
		*/					
		NewWindow(G_FORMID, G_XMLNAME, param, , , "OpenStyle=max resize=true",-1 , -1);
		
		//DockBar_Menu.mnb_mdi.MenuDataset = "gds_windows";
		
	} 
	else 
	{
			
		objForms[0].SetFocus();
		
	}

	
} 




//------------------------------------------------------------------                                           
// 업무메뉴 클릭 WINDOW OPEN
// ex : gfn_user_NewForm("sample","SAM_WebEditor")
//      gfn_user_NewForm("sample","SAM_WebEditor","PARM=" + quote("ABC"))  
//------------------------------------------------------------------ 
function gfn_open(Fprefix,Formid, param,  formName ) 
{ 

	var objForms = AllWindows(Formid);
	
    if( gds_windows.count == 10 )
    {
		alert("10개 이상의 화면은 열수 없습니다.");
		return;
    }
    
	// newform 이 없는경우                                                                                      
	if( length(objForms[0]) == 0) 
	{
		
		G_XMLNAME = Fprefix + "::" + Formid + ".xml";
		G_FORMID  = Formid;
	    
	    // form 정보를 저장
	    /*
		var row = gds_windows.addRow();
		gds_windows.SetColumn(row,"formid",Formid);
		gds_windows.SetColumn(row,"lev","0");
		gds_windows.SetColumn(row,"formName",formName);					
		*/					
		NewWindow(G_FORMID, G_XMLNAME, param, , , "OpenStyle=max resize=true",-1 , -1);
		
		//DockBar_Menu.mnb_mdi.MenuDataset = "gds_windows";
		
	} 
	else 
	{

		G_XMLNAME = Fprefix + "::" + Formid + ".xml";
		G_FORMID  = Formid;

	    // form 정보를 저장
		var row = gds_windows.addRow();
		gds_windows.SetColumn(row,"formid",Formid);
		gds_windows.SetColumn(row,"lev","0");
		gds_windows.SetColumn(row,"formName",formName);					
		
	
		NewWindow(G_FORMID, G_XMLNAME, param, , , "OpenStyle=max resize=true",-1 , -1);

		DockBar_Menu.mnb_mdi.MenuDataset = "gds_windows";

	}

	
} 




//------------------------------------------------------------------                                           
// form Event에서 정의를 한다.
//------------------------------------------------------------------ 
function gfn_OnKeyDown(obj,objSenderObj,nChar,bShift,bControl,bAlt,nLLParam,nHLParam)
{
		if(nChar=="13")  lfn_PreSearch();
}


function  gfn_AlertMessage( strCode,  strMsg )
{
	if(strCode!="")
	{
		alert("ERROR CODE ("+strCode+") : " +  strMsg );
	}
	else
	{
		alert( strMsg );
	}
}


//------------------------------------------------------------------                                           
// 관리자화면이 닫힐때 처리
//------------------------------------------------------------------ 
function gfn_close()
{

	aRow = gds_windows.FindRow("formid",  this.id );
	gds_windows.DeleteRow(aRow);
	DockBar_Menu.mnb_mdi.MenuDataset = "gds_windows";	
	close();
}


//------------------------------------------------------------------                                           
// from,  to date 조회
// from   현제년의 1월1일
// to     현제년월
//------------------------------------------------------------------ 
function gfn_fromToDate(objFrom,  objTo )
{
	objFrom.Value = SubStr( GetDate() , 0, 4)+"01"+"01";
	objTo.Value = SubStr( GetDate() , 0, 8);
}



//------------------------------------------------------------------                                           
// 그리드 헤더의 체크를 선택했을때 처리
// objGrid : 그리드명
// objDs   : 데이타셋 명
//------------------------------------------------------------------ 
function  gfn_gridAllChk( objGrid,  objDs )
{
	
	if(objGrid.GetCellText("Head",1,0)=="1"){
	
		objGrid.SetCellProp("Head",0,"text","0");
		
		for( i=0; i <= objDs.count; i++)
		{
			objDs.SetColumn(i, "CHK", 0);
		}

	}else{
	
		objGrid.SetCellProp("Head",0,"text","1");
		
		for( i=0; i <= objDs.count; i++)
		{
			objDs.SetColumn(i, "CHK", 1);
		}

	}

}



//------------------------------------------------------------------                                           
// 로그인여부를 체크
//------------------------------------------------------------------ 
function gfn_userLoginChk()
{

	if( S_USER_ID=="null" ||  S_USER_ID = "")
	{
		UserNotify("100","로그인이 필요한 화면입니다.");  
		return;
	}

}



//------------------------------------------------------------------                                           
// 이메일 형식 체크. 
//------------------------------------------------------------------ 
function gfn_IsEmail(sValue, focusObj)
{
	var sReturnValue = false;
	var sTmp = "";
	var sRegExp = "[a-z0-9]+[a-z0-9.,]+@[a-z0-9]+[a-z0-9.,]+\\.[a-z0-9]+";
	
	var regexp = CreateRegExp(sRegExp,"ig");
	sTmp = regexp.Exec(sValue);
	
	if ( sTmp == null )
		sReturnValue = false;
	else
	{
		if ( ( sTmp.index == 0 ) && (sTmp.length == sValue.length ) )
			sReturnValue = true;
		else
			sReturnValue = false;
	}
	
	if( sReturnValue == false)
	{	
		alert("정확한 E-Mail을 입력해주세요.");
		focusObj.SetFocus();
	}
	
	
	return sReturnValue;
} 

//------------------------------------------------------------------                                           
// 각 정규식에대한 함수 호출.
//------------------------------------------------------------------ 
function gfn_isFormatCheck(gubun, sValue, focusObj, sMsg)
{
	//alert(gubun);
	
	var sRegExp = "";
	var alertMsg = "정확한 " +sMsg+ "을(를) 입력해주세요.  \n숫자와 '-'만 입력가능합니다.";
	
	if( gubun.ToUpperCase == 'PHONE')
		sRegExp = "^([0-9]{2,4}-)?[0-9]{3,4}-[0-9]{4}$";		//  01-000-0000, 0000-0000, 000-0000-0000

	else if( gubun.ToUpperCase == 'ACCOUNT')
		sRegExp = "^([0-9]{1,}-)*[0-9]{1,}-[0-9]{1,}$";
		
	else if( gubun.ToUpperCase == 'EMAIL')
	{
		sRegExp = "[a-z0-9]+[a-z0-9.,]+@[a-z0-9]+[a-z0-9.,]+\\.[a-z0-9]+";
		alertMsg = "정확한 " +sMsg+ "을(를) 입력해주세요";
	}
	
	return gfn_isFormat(sRegExp, sValue, focusObj, alertMsg);
}

//------------------------------------------------------------------                                           
// 넘버포맷형식 체크. 
//------------------------------------------------------------------ 
function gfn_isFormat(sRegExp, sValue, focusObj, alertMsg)
{	
	//alert(sRegExp);
	
	var sReturnValue = false;
	var sTmp = "";
	
	var regexp = CreateRegExp(sRegExp,"ig");
	sTmp = regexp.Exec(sValue);
	
	if ( sTmp == null )
		sReturnValue = false;
	else
	{
		if ( ( sTmp.index == 0 ) && (sTmp.length == sValue.length ) )
			sReturnValue = true;
		else
			sReturnValue = false;
	}
	
	if( sReturnValue == false)
	{	
		alert(alertMsg);
		focusObj.SetFocus();
	}
	
	return sReturnValue;
} 


/********************************
edit  text 널체크 글로벌 함수.
********************************/
function gfn_formCheck(checkType, objValue, focusObj, msg)
{	
	// 널체크
	if(checkType == 'NULL')
	{
		if( length(trim( objValue) ) == 0 )  
		{
			alert(msg+"(을)를 입력해 주세요.");
			
			focusObj.SetFocus();
			
			return false;
			break;
		}
		else
		{	
			return true;
		}
	}
}

/********************************
edit  value 널체크 글로벌 함수.
********************************/

function gfn_formCheck2(checkType, objID, msg)
{	
	// 널체크
	if(checkType == 'NULL')
	{
		if( length(trim( objID.value) ) == 0 )  
		{
			alert(msg+"(을)를 입력해 주세요.");
			
			objID.SetFocus();
			
			return false;
			break;
		}
		else
		{	
			return true;
		}
	}
}


/********************************
ds 널체크 글로벌 함수.
예] gfn_dsCheck(ds_list_kapp, "DISK_NAME^CD_CODE", "음반명^시디코드", div1.grd_List, "1^2");
********************************/
function gfn_dsCheck(objDs, chkColNames, msgs, objGrid, grdColIds, focusObj)
{

	var arrayColNames    = chkColNames.split("^");
	var arrayMsgs 	  	 = msgs.split("^");
	var arrayGrdColIds 	 = grdColIds.split("^");
	
	var cnt=0;
	for( i=0; i< objDs.GetRowCount(); i++ )
	{	
		for( j =0 ; j<arrayColNames.length; j++) 
		{	
			//alert(objDs.GetColumn(i,arrayColNames[j]));
			if( length(trim(objDs.GetColumn(i,arrayColNames[j]))) == 0)
			{	
			
				//alert(objDs.GetRowPosition());
				//ds_list.GetRowPosition()
				
				alert(arrayMsgs[j]+"(을)를 입력해 주세요.");		
				
				// 포커스이동
				if(focusObj != null)
				{
					focusObj.SetFocus();
				}
				
				// 해당 셀 선택
				objDs.row = i;
				objGrid.SetCellPos(arrayGrdColIds[j]);
				objGrid.ShowEditor(true);
				
				cnt++;
				return false;
				break;
			}
			
			if(cnt>0)	break;
		}
		if(cnt>0)	break;
	}
	
	if(cnt>0)	return false;
	else return true;
	
}


/********************************
ds 널체크 글로벌 함수.
예] if( !gfn_dsRowCheck(ds_list_kapp, "내용(음원저작자협회)", div1.chk_file)) return false;
********************************/
function gfn_dsRowCheck(objDs, msg, focusObj)
{
	//ds_list_kapp.GetRowCount();
	
	if(objDs.GetRowCount() == 0)
	{
		alert(msg+"정보를 입력해주세요.");
		focusObj.SetFocus();	
		
		return false;
	}
	
	return true;
}


/********************************
파일다운로드URL RETURN
********************************/

function gfn_getFilePath()
{
	//return "D://temp//";
	//return "/home/tmax/mls/web/upload/";  // 실서버
	//return "/home/tmax_dev/mls/web/upload/";  // 개발서버
	
	return "/home/tmax/mls/web/upload/";
}

/********************************
현재 서버위치 RETURN
********************************/
function gfn_getServerIP(strDelimiter)
{
	//현재 서버위치를 가지고 온다.
	var aSplitResult = split(global.startxml,strDelimiter, "webstyle");
	var sHrmLocation = aSplitResult[0];
	var sOtherLocation = "";
	
	//alert("aSplitResult.length"+aSplitResult.length);
	
	//현재 로컬에서 작업중인 경우 개발계로 지정
	if ( aSplitResult.length == 1){
		
		sHrmLocation = "localhost";
	}
	
	return sHrmLocation;
}

function gfn_fileDownUrl( argFilePath, argFileName, argRealFileName, strDelimiter)
{	
	var ServerUrl 		= gfn_getServerIP(strDelimiter)+"/include/FileDownload.jsp";
	
	var strPageUrl      = ServerUrl
	                    + "?filePath="+argFilePath
	                    + "&fileName="+argFileName
	                    + "&realFileName="+argRealFileName;
	return strPageUrl;
}