<%@ Page Language="C#" Debug="false" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Sockets" %>
<%@ Import Namespace="System.Text" %>
<%
    string wfcontextRoot = "";
	string wfServerAddress = "xxx.xxx.xxx.xxx";

	IPAddress host = IPAddress.Parse(wfServerAddress);
	IPEndPoint hostep = new IPEndPoint(host, 80);
	Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 500);
    socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 500);

    try
    {
        socket.Connect(hostep);
        socket.Send(Encoding.ASCII.GetBytes("GET ALIVE HTTP/1.1\r\n\r\n"));
        Response.Write("<script type='text/javascript' src='" + wfcontextRoot + "/webfilter/js/webfilter.js'></script>");
    }
    catch (SocketException)
    {
		Response.Write("<iframe id='webfilterSmsFrame' name='webfilterSmsFrame' width='0' src='" + wfcontextRoot + "/webfilter/html/webfilterBypass.html' height='0' frameborder='0' scrolling='no' noresize></iframe>");
    }
    finally
    {
        socket.Close();
    }
%>
<iframe id="webfilterTargetFrame" name="webfilterTargetFrame" width="0" height="0" frameborder="0" scrolling="no" noresize></iframe>
<!--
<%@ Page Language="VB" Debug="false" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Sockets" %>
<%@ Import Namespace="System.Text" %>
<%
    Dim WfSocket As Sockets.Socket
    Dim Encrp As IPEndPoint
    Dim wfcontextRoot As String
    Dim wfServerAddress As String
    wfcontextRoot = ""
    wfServerAddress = "xxx.xxx.xxx.xxx"
    WfSocket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
    WfSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 500)
    WfSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 500)
    Encrp = New IPEndPoint(IPAddress.Parse(wfServerAddress), 80)
    
    Try 
        WfSocket.Connect(Encrp)
        WfSocket.Send(Encoding.ASCII.GetBytes("GET ALIVE HTTP/1.1\r\n\r\n"))
        Response.Write("<script type='text/javascript' src='" & wfcontextRoot & "/webfilter/js/webfilter.js'></script>")
    Catch ex As SocketException
        Response.Write("<iframe id='webfilterSmsFrame' name='webfilterSmsFrame' width='0' src='" & wfcontextRoot & "/webfilter/html/webfilterBypass.html' height='0' frameborder='0' scrolling='no' noresize></iframe>")
    Finally
        WfSocket.Close()
    End Try 
%>
-->