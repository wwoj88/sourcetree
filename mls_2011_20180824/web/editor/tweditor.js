//#tip create editor
if(true)
{
	var clsid =  "2B9CC783-2A5D-4189-8B97-F5D28E02F6F2";
	var version = "3,5,1,2025";
	var cab = "/editor/tweditor.cab";
	var env = "/editor/env.xml";
	var id = "twe";
	var applyinitdata = 1;//apply:1
	var editmode = 0;//edit:0
	//var key = "MDVGNDhGQThBQzc5MDlDMjFEMTVEQUNEQzBFMjBFMEI2MDhCMjczQTZCMkVDOTk0RTdEM0MxNDZDQzkzQUNFMzFEN0Y5MjUzRjFDQzZCMjk0MkI5MzhENDQwMDAxMjkyMjIwMzMxMUM0QjdGOEQzODFGQjI2Mjc0NkI0QzBGRjc1M0FCQzU0REVBRUJBNjgwMTdEM0FBQ0I3MjFDMTgxQTRCQjg4NEE5NTNFMEUyNTE2RTczNUUwMkQwQThBMjUwMDJDQjhGQzhENjQ2MzhFMzdGREQ5MUU2OTcwMEI1MEI2RjExQUQxNjEwRjNDMTNCMEIwRTBDNDJGNEZFNzZFQTNFQjM1NDZFQURDQzhEQzJBRUJFQ0FENjRGQkQ0MTVCNDdDRTk4NjBFRUNFQURCMUNFRTJCMzJDRUEzQzc2RkNBQjg4NzBCMzg3NzFDMTM0AA==";
	//var key = 'NzY3Q0Y1NjRCQzVDOUY0M0QwMDc1QzQxMzQyOTk2NzI0RTJEMjExQjE4MjFCNDA1MTQ4MDlGQjM3QkJEODI0RDgzNTMzQzc1MDA1OEM1OUU4MkU3RDk3NjJGQ0M5QzA2ODdCQzE4NzQ2NUY1M0RDMjAwQzNFMzkxNTgxMzBFRTYwMkE4NkQwREREMEEwMjA3OTgwRTI0NTMzOEJCNjdDNUE1ODhFN0RGNjcwMzBBMjQxMTk4QzE4OTM1RDQxOEExMEU5M0Y1OUJFMDYxNTA5N0M4NTY0MEFGMjQ3RjMxREI4QzNBNzBCMTBCRDFEQjA2MzA3NDE1QjhCQUM4ODgwRTUyREU3NEI2NDVCRTRBNEQ2OTk2OUIyMzE2ODYwMzAwNUE0MjY4MEU2NzNDRjBEQzEzODk4MTMyQ0E1OTA4NzRBQjg4NzBCMzg3NzFDMTM0AA==';
	// (~12/01)
	// var key = "Uju40Kn2tMYAIOsa+TyVlAj39Rur0DiJeJpICAtRLV92r1aksszLEfzx0Gj2gcbJbbPHUSWeUPScws18rp9DkybtKTfhT4TP3zWTRwNvPSg1PmgtG0HTUTJnN2j5Hb0VS592RhZzFCEHxfmF3jyjJpVqWPWuYMrcJIkr4tUKX0g=";
	
	//(~12/31)
	var key = "/xXWFU6Xqb4uQXwt70h59sDlxKZzN8XYkAyRk4Q6HMwp9F/MbwU1QJayo9QdrumaHf8pL1Y7vH63Eds+ntdEsivWLJuxbeojAV0EGZ9laGUJnohy8PAuaKYJi4SRqD+l39qIgJkOtoMkLbG1zouswQKX5Hw9s0bUicPI3XR3DOk=";
	
	document.write('<object ID="'+id+'" width="100%" height="100%" CLASSID="CLSID:'+clsid+'" CODEBASE="'+cab+'#version='+version+'">');
	document.write('<PARAM name="InitFile" value="'+env+'"/>');
	document.write('<PARAM name="ApplyInitData" VALUE="'+applyinitdata+'"/>');
	document.write('<PARAM name="Mode" VALUE="'+editmode+'"/>');
	document.write('<PARAM name="LicenseKey" value="'+key+'">');
	document.write('</object>');
}

//#tip p->br, br->p
if(false)
{
	document.write('<script language="JScript" FOR="'+id+'" EVENT="OnKeyDown(event)">');
	document.write('	if (!event.shiftKey && event.keyCode == 13)');
	document.write('	{');
	document.write('		document.'+id+'.InsertHtml("<br>");');
	document.write('		event.returnValue = true;');
	document.write('	}');
	document.write('	if (event.shiftKey && event.keyCode == 13)');
	document.write('	{');
	document.write('		document.'+id+'.InsertHtml("<p>");');
	document.write('		event.returnValue = true;');
	document.write('	}');
	document.write('</script>');
}

//#tip protected backspace
if(false)
{
	document.write('<script language="JavaScript">');
	document.write('document.onkeydown = twekeyhandler;');
	document.write('function twekeyhandler() {');
	document.write('    if (window.event && window.event.keyCode == 8) {');
	document.write('        alert("protected backspace key");');
	document.write('        return false;');
	document.write('    }');
	document.write('}');
	document.write('</script>');
}

//#tip activex redesign ref)http://www.tagfree.com/ver2/Support_Center/faq/faq_view.asp?supno=11
if(false)
{
	if(editmode != 1)
	{
		document.write('<script language="JScript" FOR="'+id+'" EVENT="BeforeTabChange">');
		document.write('	var view = document.'+id+'.ActiveTab;');
		document.write('	if(view == 1 || view == 3)');
		document.write('	{')
		document.write('		document.'+id+'.ObjectToScript("object", "object.asp","tag","tweobject");');
		document.write('		document.'+id+'.ObjectToScript("embed", "object.asp","tag","tweobject");');
		document.write('	}')
		document.write('</script>');
		document.write('<script language="JScript" FOR="'+id+'" EVENT="OnLoadComplete">');
 		document.write('	var view = document.'+id+'.ActiveTab;');
 		document.write('	if(view == 1 || view == 3) document.'+id+'.ScriptToObject("tag","tweobject");');
		document.write('</script>');
	}
}

//#tip custom popup menu
if(false)
{
	document.write('<OBJECT id="ContextMenu" width="0" height="0" classid="CLSID:7823A620-9DD9-11CF-A662-00AA00C066D2" CODEBASE="http://activex.microsoft.com/controls/iexplorer/x86/iemenu.cab#version=4"></OBJECT>');
	document.write('<script language="JScript" FOR="twe" EVENT="OnContextMenu(event)">');
	document.write('	ContextMenu.Clear();');
	document.write('	ContextMenu.AddItem ("1", 1);');
	document.write('	ContextMenu.AddItem ("2", 2);');
	document.write('	ContextMenu.PopUp();');
	document.write('	event.returnValue = true;');
	document.write('</script>');
	document.write('<SCRIPT LANGUAGE="VBscript">');
	document.write('Sub ContextMenu_Click(ByVal x)');
	document.write('	alert(x)');
	document.write('End Sub');
	document.write('</SCRIPT>');
}