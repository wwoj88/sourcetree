<%@ page language="java" contentType="text/html; charset=EUC-KR"
  pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
  //페이지 이동
  function fncGoPage(page) {
  /*  var datas = [];
    //datas[0] = 'MENU_SEQN';
    var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd16List1.page"/>',datas, page);
    location.href = url; */
    var datas = [];
    var YYYYMM = $('#schYYYYMM').val();
    var YYYYMM2 = $('#schYYYYMM2').val();
    var GUBUN = $('#GUBUN').val();
    var TITLE = $('#TITLE').val();
    //datas[0] = 'MENU_SEQN';
    var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd16List1.page"/>',datas, page);
    TITLE = encodeURI(encodeURIComponent(TITLE));
    url += '&YYYYMM='+YYYYMM+'&YYYYMM2='+YYYYMM2+'&GUBUN='+GUBUN+'&TITLE='+TITLE;
    
    location.href = url;
  }

  //검색
  function fncGoSearch() {
    /* var YYYYMM = $('#schYYYYMM').val();
    var GUBUN = $('#GUBUN').val();
    var TITLE = $('#TITLE').val();
    
    var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd16List1.page"/>')+'&GUBUN='+GUBUN+'&TITLE='+TITLE+'&YYYYMM='+YYYYMM;
    location.href = url; */
    var datas = [];
    var YYYYMM = $('#schYYYYMM').val();
    var YYYYMM2 = $('#schYYYYMM2').val();
    var GUBUN = $('#GUBUN').val();
    var TITLE = $('#TITLE').val();
    if(YYYYMM == ''){
      alert('보고년월을 선택해 주십시오.'); 
      return false;
    }
    if(YYYYMM2 == ''){
      YYYYMM2 = YYYYMM;
    }
    if(YYYYMM>YYYYMM2){
      alert("시작월이 더 큽니다.");
      return;
    }
    TITLE = encodeURI(encodeURIComponent(TITLE));
    
    var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd16List1.page"/>',datas,1)+'&GUBUN='+GUBUN+'&TITLE='+TITLE+'&YYYYMM='+YYYYMM+'&YYYYMM2='+YYYYMM2;
    location.href = url;
  }
  
  function fncExcelDown1() {
    /* var datas = [];
    datas[0] = 'schYYYYMM';
    datas[1] = 'GUBUN';
    datas[2] = 'TITLE';
    var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd16Down1.page"/>',datas, 1);
    var YYYYMM = $('#schYYYYMM').val();
    var GUBUN = $('#GUBUN').val();
    var TITLE = $('#TITLE').val();
    
    if(YYYYMM == ''){
      alert('보고년월을 선택해 주십시오.'); 
      return false;
    }
    
    if(YYYYMM != '' || GUBUN != '' || TITLE != ''){
      TITLE = encodeURI(encodeURIComponent(TITLE));
      url += '&YYYYMM='+YYYYMM+'&GUBUN='+GUBUN+'&TITLE='+TITLE;
    }
    location.href = url; */
    //datas[0] = 'GUBUN';
    //datas[1] = 'TITLE';
    var datas = [];
    var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd16Down1.page"/>',datas, 1);
    var YYYYMM = $('#schYYYYMM').val();
    var YYYYMM2 = $('#schYYYYMM2').val();
    var GUBUN = $('#GUBUN').val();
    var TITLE = $('#TITLE').val();
    
    if(YYYYMM == ''){
      alert('보고년월을 선택해 주십시오.'); 
      return false;
    }
    if(YYYYMM2 == ''){
      YYYYMM2 = YYYYMM;
    }
    if(YYYYMM>YYYYMM2){
      alert("시작월이 더 큽니다.");
      return;
    }
  
    if(YYYYMM != '' || GUBUN != '' || TITLE != ''){
      if(YYYYMM != '' ){
        if(YYYYMM == YYYYMM2){
          TITLE = encodeURI(encodeURIComponent(TITLE));
          url += '&YYYYMM='+YYYYMM+'&YYYYMM2='+YYYYMM2+'&GUBUN='+GUBUN+'&TITLE='+TITLE;   
        }else{
          TITLE = encodeURI(encodeURIComponent(TITLE));
          url += '&YYYYMM='+YYYYMM+'&YYYYMM2='+YYYYMM2+'&GUBUN='+GUBUN+'&TITLE='+TITLE; 
        }
        
      }else{
        TITLE = encodeURI(encodeURIComponent(TITLE));
        url += '&YYYYMM='+YYYYMM+'&YYYYMM2='+YYYYMM2+'&GUBUN='+GUBUN+'&TITLE='+TITLE; 
      }
      
    }
    
     location.href = url;
  }
  
  $(function(){
    init();
  })
  
  function init()
  {
    $('#checkBoxAllCheck').click(checkBoxAllChecked);
    $('#deleteBtn').click(deleteBtnClick);
  }
  
  function deleteBtnClick(event){
    console.log('deleteBtnClick')
    var worksIdList= getWorkId();
    $('#deleteList').val(worksIdList);
    console.log($('#deleteList').val());
    $('#deleteListForm')[0].submit();
  }
  
  function checkBoxAllChecked(event){
    if($('#checkBoxAllCheck').is(':checked')){
      checkBoxOnChage(true)
    }else{
      checkBoxOnChage(false)
    }
  }
  
  function getWorkId(){
    var checkBoxLength = $("input:checkbox[name=deleteCheckBox]").length;
    var worksIdList = [];
    
    for(var i = 1 ; i <= checkBoxLength ; i++){
      if($('#check'+i).prop('checked')){
        worksIdList.push($('#check'+i).val());
      }
    }
    
    return worksIdList;
  }
  
  function checkBoxOnChage(flag)
  {

    var checkBoxLength = $("input:checkbox[name=deleteCheckBox]").length;
    
    for(var i = 1 ; i <= checkBoxLength ; i++){
      $('#check'+i).prop('checked',flag);
    } 
  }
</script>
<style>
.fixed-table-body {
  overflow-x: auto;
}
</style>
<form name="boardForm" id="boardForm" method="post">
  <input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<form name="deleteListForm" id="deleteListForm" method="post" action="/console/rcept/fdcrAd16Delete.page">
  <input type="hidden" id="deleteList" name="deleteList" />
</form>


<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">검색</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse">
        <i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
        <%-- 
          <div class="input-group">
            <span class="input-group-addon" style="width:100px;"> <label>보고년월</label></span>
            <span class="input-group-addon" style="width:150px;"> 
              <select id="schYYYYMM" name="schYYYYMM" class="form-control">
                 <c:forEach var="time" items="${timeList}" varStatus="listStatus">
                 <option value="${time.REPT_YMD}" ${time.REPT_YMD eq commandMap.YYYYMM ?'selected' :''}><c:out value="${time.REPT_YMD}"/></option>
                </c:forEach>
              </select>
            </span>
            <span class="input-group-addon" style="width:100px;"> <label>구분</label></span>
            <span class="input-group-addon" style="width:150px;"> 
              <c:forEach var="info" items="${ds_list}" varStatus="listStatus">
              <c:set var="schGubun" value="${commandMap.GUBUN }"></c:set>
              </c:forEach>
              <select class="form-control" id="GUBUN" name="GUBUN">
                <option selected="selected" value="" >-전체-</option>
                <option value="10" ${schGubun eq '10' ?'selected' :''}>저작물종류</option>
                <option value="20" ${schGubun eq '20' ?'selected' :''}>저작물명</option>
                <option value="30" ${schGubun eq '30' ?'selected' :''}>저작물부제</option>
                <option value="40" ${schGubun eq '40' ?'selected' :''}>창작년도</option>
                <option value="50" ${schGubun eq '50' ?'selected' :''}>작성기관명</option>
              </select>
            </span>
            <span class="input-group-addon" style="width:250px;"><input type="text" class="form-control" id="TITLE" name="TITLE" value="${commandMap.TITLE }"></span>
            <div class="btn-group" style="float: right; margin: 10px 20px 0 0;"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
          </div>
         --%> 
          <form name="searchForm" id="searchForm" method="post">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  
                  <th class="text-center search-th">보고년월</th>
                  <%-- <td>
                    <select id="schYYYYMM" name="schYYYYMM" class="form-control">
                       <option value="" >-전체-</option>
                       <c:forEach var="time" items="${timeList}" varStatus="listStatus">
                       <option value="${time.REPT_YMD}" ${time.REPT_YMD eq commandMap.YYYYMM ?'selected' :''}><c:out value="${time.REPT_YMD}"/></option>
                       </c:forEach>
                    </select>
                  </td> --%>
                  <td>
                    <select id="schYYYYMM" name="schYYYYMM" class="form-control">
                       <option value="" >-전체-</option>
                       <c:forEach var="time" items="${timeList}" varStatus="listStatus">
                       <option value="${time.REPT_YMD}" ${time.REPT_YMD eq commandMap.YYYYMM ?'selected' :''}><c:out value="${time.REPT_YMD}"/></option>
                       </c:forEach>
                    </select>
                  </td>
                  <td>
                    <select id="schYYYYMM2" name="schYYYYMM2" class="form-control">
                       <option value="" >-전체-</option>
                       <c:forEach var="time" items="${timeList}" varStatus="listStatus">
                       <option value="${time.REPT_YMD}" ${time.REPT_YMD eq commandMap.YYYYMM2 ?'selected' :''}><c:out value="${time.REPT_YMD}"/></option>
                       </c:forEach>
                    </select>
                  </td>
                  <th class="text-center search-th">구분</th>
                  <td>
                    <c:forEach var="info" items="${ds_list}" varStatus="listStatus">
                      <c:set var="schGubun" value="${commandMap.GUBUN }"></c:set>
                    </c:forEach>
                    <select class="form-control" id="GUBUN" name="GUBUN">
                      <option selected="selected" value="" >-전체-</option>
                      <option value="10" ${schGubun eq '10' ?'selected' :''}>저작물종류</option>
                      <option value="20" ${schGubun eq '20' ?'selected' :''}>저작물명</option>
                      <option value="30" ${schGubun eq '30' ?'selected' :''}>저작물부제</option>
                      <option value="40" ${schGubun eq '40' ?'selected' :''}>창작년도</option>
                      <option value="50" ${schGubun eq '50' ?'selected' :''}>작성기관명</option>
                    </select>
                  </td>
                  <td>
                    <input type="text" class="form-control" id="TITLE" name="TITLE" value="${commandMap.TITLE }">
                  </td>
                </tr>
              </tbody>
            </table>
          </form>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <div class="box-footer" style="text-align:right">
      <div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
    </div>
    <!-- /.box-body -->
  </div>
</div>


<div class="box" style="margin-top: 30px;">
  <!-- /.box-header -->
  <div class="box-body">
  <div class="fixed-table-body">
  <c:set var="totCnt" value="${ds_count}" />
    <span><b>&nbsp;&nbsp;총 <fmt:formatNumber value="${totCnt}" pattern="###,###,###,##0" groupingUsed="true" />건</b></span>
    <table id="fdcrAd20List1" class="table table-bordered" style="margin-top: 5px;">
      <thead>
        <tr>
          <th><input type="checkbox" id="checkBoxAllCheck"></th>
          <th class="text-center" style="width: 7%;">보고년월</font></th>
          <th class="text-center" style="width: 10%;">저작물관리번호</font></th>
          <th class="text-center" style="width: 7%;">국내외구분</font></th>
          <th class="text-center" style="width: 7%;">저작물분류</th>
          <th class="text-center">저작물명</font></th>
          <th class="text-center" style="width: 13%;">저작물부제</font></th>
          <th class="text-center" style="width: 13%;">창작년도(발행연월일)</font></th>
          <th class="text-center" style="width: 7%;">위탁관리구분</font></th>
          <th class="text-center" style="width: 15%;">작성기관명</font></th>
        </tr>
      </thead>
      <tbody>
        <c:choose>
          <c:when test="${empty ds_list}">
            <tr>
              <td colspan="9" class="text-center">게시물이 없습니다.</td>
            </tr>
          </c:when>
          <c:otherwise>
            <c:set var="diskcount" value="0" />
            
            <c:forEach var="info" items="${ds_list}" varStatus="listStatus">
              <c:set var='diskcount' value='${(diskcount+1)}'/>
              <tr>
                <td><input type="checkbox" id="check${diskcount}" name="deleteCheckBox" value="${info.WORKS_ID}"></td>
                <td><c:out value="${info.REPT_YMD}"/></td>
                <td><c:out value="${info.COMM_WORKS_ID}"/></td>
                <td>
                <c:if test="${info.NATION_CD == '1' }">국내</c:if>
                <c:if test="${info.NATION_CD == '2' }">국외</c:if>
                </td>
                <td>
                <c:if test="${info.GENRE_CD == '1' }">음악</c:if>
                <c:if test="${info.GENRE_CD == '2' }">어문</c:if>
                <c:if test="${info.GENRE_CD == '3' }">방송대본</c:if>
                <c:if test="${info.GENRE_CD == '4' }">영화</c:if>
                <c:if test="${info.GENRE_CD == '5' }">방송</c:if>
                <c:if test="${info.GENRE_CD == '6' }">뉴스</c:if>
                <c:if test="${info.GENRE_CD == '7' }">미술</c:if>
                <c:if test="${info.GENRE_CD == '8' }">이미지</c:if>
                <c:if test="${info.GENRE_CD == '99' }">기타</c:if>
                </td>
                <td><c:out value="${info.WORKS_TITLE}"/></td>
                <td><c:out value="${info.WORKS_SUB_TITLE}"/></td>
                <td><c:out value="${info.CRT_YEAR}"/></td>
                <td>
                <c:if test="${info.COMM_MGNT_CD == '0' }">없음</c:if>
                <c:if test="${info.COMM_MGNT_CD != '0' }"><c:out value="${info.COMM_MGNT_CD}"/></c:if>
                </td>
                <td><c:out value="${info.RGST_ORGN_NAME}"/></td>
              </tr>
            </c:forEach>
          </c:otherwise>
        </c:choose>
      </tbody>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix text-center">
    <ul>
      <!-- <button id="deleteBtn" type="button" class="btn btn-primary" style="float: right; text-align: right; margin: 5px 0 0 0;">위탁 저작물 삭제</button> -->
      <button type="submit" class="btn btn-primary" onclick="fncExcelDown1();return false;" style="float: right; text-align: right; margin: 5px 0 0 0;">Excel Down</button>
    </ul>
    <ul class="pagination pagination-sm no-margin">
      <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
    </ul>
  </div>
</div>