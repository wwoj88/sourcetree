<%@ page language="java" contentType="application/vnd.ms-excel;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%
	String reptYmd = (String)request.getParameter("REPT_YMD");
	String filename = reptYmd+" 월별 위탁관리저작물 보고현황.xls";
	
	response.setHeader( "Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "utf-8") + "" );
	response.setHeader( "Content-Description", "JSP Generated Data" );

	//response.setContentType( "application/vnd.ms-excel" );   

	//DataSet divList = (DataSet) box.getObject("EmasDivList");
	//HashMap ageMap = (HashMap) box.getObject("LicenseAgeClass");
%>
	
	<table border="1" cellpadding="0" cellspacing="0" style="table-layout:fixed;text-align:center" >
		<thead>
			<tr>
				<th style="background-color:#eeeeee;width: 16%">작성기관명</th>
				<th style="background-color:#eeeeee;width: 7%">보고일자</th>
				<th style="background-color:#eeeeee;width: 7%">음악</th>
				<th style="background-color:#eeeeee;width: 7%">어문</th>
				<th style="background-color:#eeeeee;width: 7%">방송대본</th>
				<th style="background-color:#eeeeee;width: 7%">영화</th>
				<th style="background-color:#eeeeee;width: 7%">방송</th>
				<th style="background-color:#eeeeee;width: 7%">뉴스</th>
				<th style="background-color:#eeeeee;width: 7%">미술</th>
				<th style="background-color:#eeeeee;width: 7%">이미지</th>
				<th style="background-color:#eeeeee;width: 7%">기타</th>
				<th style="background-color:#eeeeee;width: 7%">저작물<br/>없음</th>
				<th style="background-color:#eeeeee;width: 7%">계</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty ds_list}">
				</c:when>
				<c:otherwise>
				<c:set var="sum1" value="0" />
				<c:set var="sum2" value="0" />
				<c:set var="sum3" value="0" />
				<c:set var="sum4" value="0" />
				<c:set var="sum5" value="0" />
				<c:set var="sum6" value="0" />
				<c:set var="sum7" value="0" />
				<c:set var="sum8" value="0" />
				<c:set var="sum9" value="0" />
				<c:set var="sum10" value="0" />
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<tr>
						<td style='mso-number-format:"\@";'><c:out value="${info.CONAME }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.RGST_DTTM }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_1 }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_2 }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_3 }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_4 }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_5 }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_6 }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_7 }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_8 }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_9 }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_10 }"/></td>
						<td style='mso-number-format:"\@";'><c:out value="${info.CNT_TOT }"/></td>
						<c:set var="sum1" value="${ sum1 + info.CNT1_ }" />
						<c:set var="sum2" value="${ sum2 + info.CNT2_ }" />
						<c:set var="sum3" value="${ sum3 + info.CNT3_ }" />
						<c:set var="sum4" value="${ sum4 + info.CNT4_ }" />
						<c:set var="sum5" value="${ sum5 + info.CNT5_ }" />
						<c:set var="sum6" value="${ sum6 + info.CNT6_ }" />
						<c:set var="sum7" value="${ sum7 + info.CNT7_ }" />
						<c:set var="sum8" value="${ sum8 + info.CNT8_ }" />
						<c:set var="sum9" value="${ sum9 + info.CNT9_ }" />
						<c:set var="sum10" value="${ sum10 + info.CNT10_ }" />
					</tr>
					</c:forEach>
					<tr>
						<td colspan="2">합계</td>
						<td style='mso-number-format:"\@";'><fmt:formatNumber value="${sum1}" pattern="###,###,###,##0" groupingUsed="true" /></td>
						<td style='mso-number-format:"\@";'><fmt:formatNumber value="${sum2}" pattern="###,###,###,##0" groupingUsed="true" /></td>
						<td style='mso-number-format:"\@";'><fmt:formatNumber value="${sum3}" pattern="###,###,###,##0" groupingUsed="true" /></td>
						<td style='mso-number-format:"\@";'><fmt:formatNumber value="${sum4}" pattern="###,###,###,##0" groupingUsed="true" /></td>
						<td style='mso-number-format:"\@";'><fmt:formatNumber value="${sum5}" pattern="###,###,###,##0" groupingUsed="true" /></td>
						<td style='mso-number-format:"\@";'><fmt:formatNumber value="${sum6}" pattern="###,###,###,##0" groupingUsed="true" /></td>
						<td style='mso-number-format:"\@";'><fmt:formatNumber value="${sum7}" pattern="###,###,###,##0" groupingUsed="true" /></td>
						<td style='mso-number-format:"\@";'><fmt:formatNumber value="${sum8}" pattern="###,###,###,##0" groupingUsed="true" /></td>
						<td style='mso-number-format:"\@";'><fmt:formatNumber value="${sum9}" pattern="###,###,###,##0" groupingUsed="true" /></td>
						<td style='mso-number-format:"\@";'>-</td>
						<td style='mso-number-format:"\@";'><fmt:formatNumber value="${sum1+sum2+sum3+sum4+sum5+sum6+sum7+sum8+sum9}" pattern="###,###,###,##0" groupingUsed="true" /></td>
					</tr>
					<tr>
						<c:forEach var="info_0" items="${ds_list_0}" varStatus="listStatus">
							<c:set var="info_1" value="${info_0.CNT1 }"/>
							<c:set var="info_2" value="${info_0.CNT2 }"/>
							<c:set var="info_3" value="${info_0.CNT3 }"/>
							<c:set var="info_4" value="${info_0.CNT4 }"/>
							<c:set var="info_5" value="${info_0.CNT5 }"/>
							<c:set var="info_6" value="${info_0.CNT6 }"/>
							<c:set var="info_7" value="${info_0.CNT7 }"/>
							<c:set var="info_8" value="${info_0.CNT8 }"/>
							<c:set var="info_9" value="${info_0.CNT9 }"/>
							<c:set var="info_10" value="${info_0.CNT10 }"/>
						</c:forEach>
						<td colspan="2">보고기관수</td>
						<td><c:out value="${info_1 }"/></td>
						<td><c:out value="${info_2 }"/></td>
						<td><c:out value="${info_3 }"/></td>
						<td><c:out value="${info_4 }"/></td>
						<td><c:out value="${info_5 }"/></td>
						<td><c:out value="${info_6 }"/></td>
						<td><c:out value="${info_7 }"/></td>
						<td><c:out value="${info_8 }"/></td>
						<td><c:out value="${info_9 }"/></td>
						<td><c:out value="${info_10 }"/></td>
						<td></td>
					</tr>
				</c:otherwise>
				</c:choose>
		</tbody>
	</table>
