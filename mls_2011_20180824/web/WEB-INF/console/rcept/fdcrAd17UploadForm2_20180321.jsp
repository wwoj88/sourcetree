<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<% request.setCharacterEncoding("euc-kr"); %>
<% response.setContentType("text/html; charset=euc-kr"); %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>

	$(document).ready(function() {
		
		var failCntSize =  $('#uploadFailListSize').val(); 
		alert("오류대상 저작물 건수: "+failCntSize+"건\n수정 후 다시 등록하세요.");
		var GENRE_CODE = $('#GENRE_CODE').val();
		if(GENRE_CODE == '1'){
			fdcrAd21List1_1.style.display="inline-table";
		}else if(GENRE_CODE == '2'){
			fdcrAd21List1_2.style.display="inline-table";
		}else if(GENRE_CODE == '3'){
			fdcrAd21List1_3.style.display="inline-table";
		}else if(GENRE_CODE == '4'){
			fdcrAd21List1_4.style.display="inline-table";
		}else if(GENRE_CODE == '5'){
			fdcrAd21List1_5.style.display="inline-table";
		}else if(GENRE_CODE == '6'){
			fdcrAd21List1_6.style.display="inline-table";
		}else if(GENRE_CODE == '7'){
			fdcrAd21List1_7.style.display="inline-table";
		}else if(GENRE_CODE == '8'){
			fdcrAd21List1_8.style.display="inline-table";
		}else if(GENRE_CODE == '99'){
			fdcrAd21List1_99.style.display="inline-table";
		}
	});

	//업로드 파일양식 다운로드
	function fncDown(){
		var GENRE_CODE = $('#GENRE_CODE').val();
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd17Down1.page"/>')+'&GENRE_CODE='+GENRE_CODE;
		location.href = url;
	}

	function fncDelete(){
		var $obj = $("input[name='chk01']");
	    var checkCount = $obj.size();
	    var f = document.chkForm;
	    for(var i=0; i < checkCount; i++ ){
			if( $obj.eq(i).is(":checked")==true ){
				alert("보고저작물 없음 체크를 해제하세요.");
			}else{
				alert("삭제할 항목을 체크해 주세요.");
				return false;
			}
		}
	}
	
	function form_change(excelForm){
		if(excelForm == "1"){
			fdcrAd21List1_1.style.display="inline-table";
			fdcrAd21List1_2.style.display="none";
			fdcrAd21List1_3.style.display="none";
			fdcrAd21List1_4.style.display="none";
			fdcrAd21List1_5.style.display="none";
			fdcrAd21List1_6.style.display="none";
			fdcrAd21List1_7.style.display="none";
			fdcrAd21List1_8.style.display="none";
			fdcrAd21List1_99.style.display="none";
		}else if(excelForm == "2"){
			fdcrAd21List1_1.style.display="none";
			fdcrAd21List1_2.style.display="inline-table";
			fdcrAd21List1_3.style.display="none";
			fdcrAd21List1_4.style.display="none";
			fdcrAd21List1_5.style.display="none";
			fdcrAd21List1_6.style.display="none";
			fdcrAd21List1_7.style.display="none";
			fdcrAd21List1_8.style.display="none";
			fdcrAd21List1_99.style.display="none";
		}else if(excelForm == "3"){
			fdcrAd21List1_1.style.display="none";
			fdcrAd21List1_2.style.display="none";
			fdcrAd21List1_3.style.display="inline-table";
			fdcrAd21List1_4.style.display="none";
			fdcrAd21List1_5.style.display="none";
			fdcrAd21List1_6.style.display="none";
			fdcrAd21List1_7.style.display="none";
			fdcrAd21List1_8.style.display="none";
			fdcrAd21List1_99.style.display="none";
		}else if(excelForm == "4"){
			fdcrAd21List1_1.style.display="none";
			fdcrAd21List1_2.style.display="none";
			fdcrAd21List1_3.style.display="none";
			fdcrAd21List1_4.style.display="inline-table";
			fdcrAd21List1_5.style.display="none";
			fdcrAd21List1_6.style.display="none";
			fdcrAd21List1_7.style.display="none";
			fdcrAd21List1_8.style.display="none";
			fdcrAd21List1_99.style.display="none";
		}else if(excelForm == "5"){
			fdcrAd21List1_1.style.display="none";
			fdcrAd21List1_2.style.display="none";
			fdcrAd21List1_3.style.display="none";
			fdcrAd21List1_4.style.display="none";
			fdcrAd21List1_5.style.display="inline-table";
			fdcrAd21List1_6.style.display="none";
			fdcrAd21List1_7.style.display="none";
			fdcrAd21List1_8.style.display="none";
			fdcrAd21List1_99.style.display="none";
		}else if(excelForm == "6"){
			fdcrAd21List1_1.style.display="none";
			fdcrAd21List1_2.style.display="none";
			fdcrAd21List1_3.style.display="none";
			fdcrAd21List1_4.style.display="none";
			fdcrAd21List1_5.style.display="none";
			fdcrAd21List1_6.style.display="inline-table";
			fdcrAd21List1_7.style.display="none";
			fdcrAd21List1_8.style.display="none";
			fdcrAd21List1_99.style.display="none";
		}else if(excelForm == "7"){
			fdcrAd21List1_1.style.display="none";
			fdcrAd21List1_2.style.display="none";
			fdcrAd21List1_3.style.display="none";
			fdcrAd21List1_4.style.display="none";
			fdcrAd21List1_5.style.display="none";
			fdcrAd21List1_6.style.display="none";
			fdcrAd21List1_7.style.display="inline-table";
			fdcrAd21List1_8.style.display="none";
			fdcrAd21List1_99.style.display="none";
		}else if(excelForm == "8"){
			fdcrAd21List1_1.style.display="none";
			fdcrAd21List1_2.style.display="none";
			fdcrAd21List1_3.style.display="none";
			fdcrAd21List1_4.style.display="none";
			fdcrAd21List1_5.style.display="none";
			fdcrAd21List1_6.style.display="none";
			fdcrAd21List1_7.style.display="none";
			fdcrAd21List1_8.style.display="inline-table";
			fdcrAd21List1_99.style.display="none";
		}else {
			fdcrAd21List1_1.style.display="none";
			fdcrAd21List1_2.style.display="none";
			fdcrAd21List1_3.style.display="none";
			fdcrAd21List1_4.style.display="none";
			fdcrAd21List1_5.style.display="none";
			fdcrAd21List1_6.style.display="none";
			fdcrAd21List1_7.style.display="none";
			fdcrAd21List1_8.style.display="none";
			fdcrAd21List1_99.style.display="inline-table";
		}
	}
	
	function chk01_change(chkVal){
	    var $obj = $("input[name='chk01']");
	    var checkCount = $obj.size();
	    var f = document.chkForm;
	    for(var i=0; i < checkCount; i++ ){
			if( $obj.eq(i).is(":checked")==true ){
	            f.checkButton.style.display="inline";
	            f.GENRE_CODE.disabled = "true";
	            f.downBtn.disabled="true";
	            f.file1.disabled="true";
			}else{
	            f.checkButton.style.display="none";
	            f.GENRE_CODE.disabled = "false";
	            f.downBtn.disabled="false";
	            f.file1.disabled="false";
			}
	    }
	}
	
	function noDataInsert(){
		 var f = document.chkForm;
		 f.action = '<c:url value="/console/rcept/fdcrAd17InsertNodata.page"/>';
		 f.submit();
	}
	
	function popCommOpen(){
		var f = document.chkForm;
		var TRST_ORGN_DIVS_CODE = $('#TRST_ORGN_DIVS_CODE').val();
		var popUrl = '<c:url value="/console/rcept/fdcrAd17Pop1.page"/>?TRST_ORGN_DIVS_CODE='
				+ TRST_ORGN_DIVS_CODE;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=800, height=600, resizable=no, scrollbars=yes, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function SetValue(returnValue) {
	  alert(returnValue);
	  var COMM_NAME = document.getElementById("COMM_NAME");
	  COMM_NAME.value = returnValue;
	}
	
	function file_change(file){
		var f = document.formUpload;
		/* 
		var file1 = $('#file1').val();
		if(confirm("파일을 업로드 하시겠습니까?"))
		{
			f.action = '<c:url value="/console/rcept/fdcrAd17Upload1.page"/>?file1='
				+ file1;
			f.submit();
		}
		 */
		if(confirm("파일을 업로드 하시겠습니까?")){
			f.action = '<c:url value="/console/rcept/fdcrAd17Upload2.page"/>';
			f.submit();
		}
	}
	
	//선택삭제
	function rowCheDel(){
	  var $obj = $("input[name='chk']");
	  var checkCount = $obj.size();
	  for (var i=0; i<checkCount; i++){
	   if($obj.eq(i).is(":checked")){
	   $obj.eq(i).parent().parent().remove();
	   }
	  }
	 }

	function returnArrayList(){
		var returnList = new Array();
		for(var i=0; i<arguments.length; i++) {
			returnList.push(arguments[i]);
        }
		returnList.push("0");
		returnList.push("없음");
		//console.log("returnList : " + returnList)
		return returnList;
	}
	
	function checkColumn(checkValue , list ){
		var flag = true;
		var listSize = list.length;
		for(var i = 0 ; i < listSize ; i++){
			if(list[i] == checkValue){
				flag = false;
				break;	
			}
		}
		return flag;
	}
 
	function fnValidate(tot_cnt){
		
		$('#chkValYN').val('N'); // 유효성체크확인
		
		var chkValYN2 = $('#chkValYN').val();
		var curRowErrCount = 0;
		var isRowError = false;
		var errorRowStr = '';
		var allRowErrCount = 0;
		var f = document.chkForm;
		var genreCd = $('#GENRE_CODE').val();
		
	if(genreCd == '0'){
		alert("엑셀양식을 선택하세요.");
	} else if(genreCd == '1'){
		for(var a = 1; a <= tot_cnt; a++){
				
				var target_1 = document.getElementById("1cell_"+a+"_0").value;
				var target_2 = document.getElementById("1cell_"+a+"_1").value;
				var target_3 = document.getElementById("1cell_"+a+"_2").value;
				var target_4 = document.getElementById("1cell_"+a+"_3").value;
				var target_5 = document.getElementById("1cell_"+a+"_4").value;
				var target_7 = document.getElementById("1cell_"+a+"_6").value;
				var target_8 = document.getElementById("1cell_"+a+"_7").value;
				var target_9 = document.getElementById("1cell_"+a+"_8").value;
				var target_10 = document.getElementById("1cell_"+a+"_9").value;
				var target_11 = document.getElementById("1cell_"+a+"_10").value;
				var target_12 = document.getElementById("1cell_"+a+"_11").value;
				var target_13 = document.getElementById("1cell_"+a+"_12").value;
				var target_14 = document.getElementById("1cell_"+a+"_13").value;
				var target_15 = document.getElementById("1cell_"+a+"_14").value;
				var target_16 = document.getElementById("1cell_"+a+"_15").value;
				var target_17 = document.getElementById("1cell_"+a+"_16").value;
				var target_18 = document.getElementById("1cell_"+a+"_17").value;
				var target_19 = document.getElementById("1cell_"+a+"_18").value;
				var target_20 = document.getElementById("1cell_"+a+"_19").value;
				var target_21 = document.getElementById("1cell_"+a+"_20").value;
				var target_22 = document.getElementById("1cell_"+a+"_21").value;
				
					//Validation Check
					if(target_1=='' || target_1 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_0").g;
					     t.style.backgroundColor = "#fc0"; 
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_1.length > 65){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_0");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					 
					if(target_2=='' || target_2 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_1");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_2.length > 100){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_1");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_3=='' || target_3 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_2");
					     t.style.backgroundColor = "#fc0"; 
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_3.length > 333){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_2");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_4.length > 1 ){
						target_4 = '0';
					}
					if(target_4=='' || target_4 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_3");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_4.length > 1 || target_4.length == 0 || target_4.length > 2){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_3");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_5=='' || target_5 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_4");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_5.length > 165 || target_5.length == 0){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_4");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_7 != '없음'){	
						if(target_7=='' || target_7 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_6");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_7.length > 20 || target_7.length == 0 || target_7 == '0'){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("1td_"+a+"_6");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
					}
					
					if(target_8 != '없음'){
						if(target_8=='' || target_8 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_7");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_8.length == 0){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("1td_"+a+"_7");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
					}
					
					if(target_9.length > 8 ){
						target_9 = '0';
					}
					if(target_9 != '없음'){
						if(target_9=='' || target_9 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_8");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_9.length == 0 || target_9.length > 8){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("1td_"+a+"_8");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
					}
					
					if(target_10=='' || target_10 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_9");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_10.length > 333 || target_10.length == 0){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_9");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_11=='' || target_11 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_10");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_11.length > 333 || target_11.length == 0){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_10");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_12=='' || target_12 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_11");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_12.length > 333 || target_12.length == 0){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_11");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_13=='' || target_13 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_12");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_13.length > 333 || target_13.length == 0){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_12");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_14=='' || target_14 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_13");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_14.length > 333 || target_14.length == 0){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_13");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_15=='' || target_15 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_14");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_15.length > 1300 || target_15.length == 0){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_14");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_16=='' || target_16 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_15");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_16.length > 333 || target_16.length == 0){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_15");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_17.length > 1 ){
						target_17 = '0';
					}
					if(target_17=='' || target_17 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_16");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_17.length == 0 || target_17.length > 1 || target_17 > 5){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_16");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_18.length > 1 ){
						target_18 = '0';
					}
					if(target_18=='' || target_18 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_17");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_18.length == 0 || target_18.length > 1 || target_17 > 3){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_17");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_19.length > 1 ){
						target_19 = '0';
					}
					if(target_19=='' || target_19 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_18");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_19.length == 0 || target_19.length > 1 || target_19 > 3){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_18");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_20.length > 1 ){
						target_20 = '0';
					}
					if(target_20=='' || target_20 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("1td_"+a+"_19");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_20.length == 0 || target_20.length > 1 || target_20 > 3){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_19");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
							}
						}
					
					if(target_21 != '없음'){
						if(target_21=='' || target_21 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_20");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_21.length == 0 || target_21.length > 200){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("1td_"+a+"_20");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						 }
					  }
					
					if(target_22 != '없음'){
						if(target_22 =='' || target_22 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("1td_"+a+"_21");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_22.length == 0 || target_22.length > 200){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("1td_"+a+"_21");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						 }
					  }
		 		 }
		alert("유효성 확인이 완료되었습니다.\n전체건수 : " + tot_cnt + "건(오류대상항목: " + curRowErrCount + "건)");
		$('#curRowErrCount').val(curRowErrCount);
		chkValYN2 = 'Y';
		$('#chkValYN').val('Y');
	  	}else if(genreCd == '2'){
			for(var a = 1; a <= tot_cnt; a++){
					var target_1 = document.getElementById("2cell_"+a+"_0").value;
					var target_2 = document.getElementById("2cell_"+a+"_1").value;
					var target_3 = document.getElementById("2cell_"+a+"_2").value;
					var target_4 = document.getElementById("2cell_"+a+"_3").value;
					var target_5 = document.getElementById("2cell_"+a+"_4").value;
					var target_6 = document.getElementById("2cell_"+a+"_5").value;
					var target_7 = document.getElementById("2cell_"+a+"_6").value;
					var target_8 = document.getElementById("2cell_"+a+"_7").value;
					var target_9 = document.getElementById("2cell_"+a+"_8").value;
					var target_10 = document.getElementById("2cell_"+a+"_9").value;
					
						//Validation Check
					if(target_1=='' || target_1 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("2td_"+a+"_0");
					     t.style.backgroundColor = "#fc0"; 
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_1.length > 65){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("2td_"+a+"_0");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					 
					if(target_2=='' || target_2 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("2td_"+a+"_1");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_2.length > 100){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("2td_"+a+"_1");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_3=='' || target_3 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("2td_"+a+"_2");
					     t.style.backgroundColor = "#fc0"; 
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_3.length > 333){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("2td_"+a+"_2");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_4=='' || target_4 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("2td_"+a+"_3");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_4.length > 160){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("2td_"+a+"_3");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_5.length > 4 ){
						target_5 = '0';
					}
					if(target_5 != '없음'){
						if(target_5=='' || target_5 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("2td_"+a+"_4");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_5.length > 4 || target_5.length == 0){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("2td_"+a+"_4");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
					}
					
					if(target_6=='' || target_6 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("2td_"+a+"_5");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_6.length > 333){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("2td_"+a+"_5");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_7=='' || target_7 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("2td_"+a+"_6");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_7.length > 333){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("2td_"+a+"_6");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_8.length > 4 ){
						target_8 = '0';
					}
					if(target_8 != '없음'){
						if(target_8=='' || target_8 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("2td_"+a+"_7");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_8.length > 4){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("2td_"+a+"_7");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
					}
					
					if(target_9=='' || target_9 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("2td_"+a+"_8");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_9.length > 65){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("2td_"+a+"_8");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
					
					if(target_10.length > 1 ){
						target_10 = '0';
					}
					if(target_10=='' || target_10 == null){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("2td_"+a+"_9");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}else{
						if(target_10.length > 1 || target_10.length == 0 || target_10 > 5){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("2td_"+a+"_9");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
					}
				}
			alert("유효성 확인이 완료되었습니다.\n전체건수 : " + tot_cnt + "건(오류대상항목: " + curRowErrCount + "건)");
			$('#curRowErrCount').val(curRowErrCount);
			chkValYN2 = 'Y';
			$('#chkValYN').val('Y');
		  }else if(genreCd == '3'){
				for(var a = 1; a <= tot_cnt; a++){
						
						var target_1 = document.getElementById("3cell_"+a+"_0").value;
						var target_2 = document.getElementById("3cell_"+a+"_1").value;
						var target_3 = document.getElementById("3cell_"+a+"_2").value;
						var target_4 = document.getElementById("3cell_"+a+"_3").value;
						var target_5 = document.getElementById("3cell_"+a+"_4").value;
						var target_6 = document.getElementById("3cell_"+a+"_5").value;
						var target_7 = document.getElementById("3cell_"+a+"_6").value;
						var target_8 = document.getElementById("3cell_"+a+"_7").value;
						var target_9 = document.getElementById("3cell_"+a+"_8").value;
						var target_10 = document.getElementById("3cell_"+a+"_9").value;
						var target_11 = document.getElementById("3cell_"+a+"_10").value;
						var target_12 = document.getElementById("3cell_"+a+"_11").value;
						var target_13 = document.getElementById("3cell_"+a+"_12").value;
						var target_14 = document.getElementById("3cell_"+a+"_13").value;
						
							//Validation Check
						if(target_1=='' || target_1 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("3td_"+a+"_0");
						     t.style.backgroundColor = "#fc0"; 
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_1.length > 65){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_0");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
						 
						if(target_2=='' || target_2 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("3td_"+a+"_1");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_2.length > 100){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_1");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
						
						if(target_3=='' || target_3 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("3td_"+a+"_2");
						     t.style.backgroundColor = "#fc0"; 
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_3.length > 160){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_2");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
						
						if(target_4=='' || target_4 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("3td_"+a+"_3");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_4.length > 165){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_3");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
						
						if(target_5 != '없음'){
							if(target_5=='' || target_5 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_4");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_5.length > 6 || target_5.length == 0){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("3td_"+a+"_4");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
						}
						
						if(target_6 != '없음'){
							if(target_6=='' || target_6 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_5");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_6.length > 200 || target_6.length == 0){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("3td_"+a+"_5");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
						}
						
						if(target_7=='' || target_7 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("3td_"+a+"_6");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_7.length > 65){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_6");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
						
						if(target_8=='' || target_8 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("3td_"+a+"_7");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_8.length > 130){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_7");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
						
						if(target_9 != '없음'){
							if(target_9=='' || target_9 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_8");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_9.length > 10){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("3td_"+a+"_8");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
						}
						
						if(target_10 != '없음'){
							if(target_10=='' || target_10 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_9");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_10.length > 8 || target_10.length == 0 ){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("3td_"+a+"_9");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
						}
						
						if(target_11.length > 65){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("3td_"+a+"_10");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}
						
						if(target_12=='' || target_12 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("3td_"+a+"_11");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_12.length > 65){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_11");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
						
						if(target_13=='' || target_13 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("3td_"+a+"_12");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_13.length > 65){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_12");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
						
						if(target_14.length > 1 ){
							target_14 = '0';
						}
						if(target_14=='' || target_14 == null){
							//오류 시 컬럼 td 색상 변경
							var t = document.getElementById("3td_"+a+"_13");
						     t.style.backgroundColor = "#fc0";
						     isRowError = true;
							 curRowErrCount++;
						}else{
							if(target_14.length > 1 || target_14.length == 0 || target_14 > 5 ){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("3td_"+a+"_13");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}
						}
					}
				alert("유효성 확인이 완료되었습니다.\n전체건수 : " + tot_cnt + "건(오류대상항목: " + curRowErrCount + "건)");
				$('#curRowErrCount').val(curRowErrCount);
				chkValYN2 = 'Y';
				$('#chkValYN').val('Y');
			  }else if(genreCd == '4'){
					for(var a = 1; a <= tot_cnt; a++){
							var target_1 = document.getElementById("4cell_"+a+"_0").value;
							var target_2 = document.getElementById("4cell_"+a+"_1").value;
							var target_3 = document.getElementById("4cell_"+a+"_2").value;
							var target_4 = document.getElementById("4cell_"+a+"_3").value;
							var target_5 = document.getElementById("4cell_"+a+"_4").value;
							var target_6 = document.getElementById("4cell_"+a+"_5").value;
							var target_7 = document.getElementById("4cell_"+a+"_6").value;
							var target_8 = document.getElementById("4cell_"+a+"_7").value;
							var target_9 = document.getElementById("4cell_"+a+"_8").value;
							var target_10 = document.getElementById("4cell_"+a+"_9").value;
							var target_11 = document.getElementById("4cell_"+a+"_10").value;
							var target_12 = document.getElementById("4cell_"+a+"_11").value;
							
								//Validation Check
							if(target_1=='' || target_1 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_0");
							     t.style.backgroundColor = "#fc0"; 
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_1.length > 65){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_0");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
								
							if(target_2=='' || target_2 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_1");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_2.length > 100){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_1");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
							
							if(target_3=='' || target_3 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_2");
							     t.style.backgroundColor = "#fc0"; 
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_3.length > 160){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_2");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
							
							if(target_4 != '없음'){
								if(target_4=='' || target_4 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_3");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_4.length > 4 || target_4.length == 0){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("4td_"+a+"_3");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
							}
							
							if(target_5=='' || target_5 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_4");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_5.length > 333 || target_5.length == 0){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_4");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
							
							if(target_6=='' || target_6 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_5");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_6.length > 65 || target_6.length == 0){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_5");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
							
							if(target_7=='' || target_7 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_6");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_7.length > 65 || target_7.length == 0){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_6");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
							
							if(target_8=='' || target_8 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_7");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_8.length > 130 || target_8.length == 0){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_7");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
							
							if(target_9=='' || target_9 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_8");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_9.length > 65 || target_9.length == 0){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_8");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
							
							if(target_10=='' || target_10 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_9");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_10.length > 65 || target_10.length == 0){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_9");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
							
							if(target_11=='' || target_11 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_10");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_11.length > 65 || target_11.length == 0){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_10");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
							
							if(target_12.length > 1 ){
								target_12 = '0';
							}
							if(target_12=='' || target_12 == null){
								//오류 시 컬럼 td 색상 변경
								var t = document.getElementById("4td_"+a+"_11");
							     t.style.backgroundColor = "#fc0";
							     isRowError = true;
								 curRowErrCount++;
							}else{
								if(target_12.length > 1 || target_12.length == 0 || target_12 > 5){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("4td_"+a+"_11");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
							}
						}
					alert("유효성 확인이 완료되었습니다.\n전체건수 : " + tot_cnt + "건(오류대상항목: " + curRowErrCount + "건)");
					$('#curRowErrCount').val(curRowErrCount);
					chkValYN2 = 'Y';
					$('#chkValYN').val('Y');
				  }else if(genreCd == '5'){
						for(var a = 1; a <= tot_cnt; a++){
								
								var target_1 = document.getElementById("5cell_"+a+"_0").value;
								var target_2 = document.getElementById("5cell_"+a+"_1").value;
								var target_3 = document.getElementById("5cell_"+a+"_2").value;
								var target_4 = document.getElementById("5cell_"+a+"_3").value;
								var target_5 = document.getElementById("5cell_"+a+"_4").value;
								var target_6 = document.getElementById("5cell_"+a+"_5").value;
								var target_7 = document.getElementById("5cell_"+a+"_6").value;
								var target_8 = document.getElementById("5cell_"+a+"_7").value;
								var target_9 = document.getElementById("5cell_"+a+"_8").value;
								var target_10 = document.getElementById("5cell_"+a+"_9").value;
								var target_11 = document.getElementById("5cell_"+a+"_10").value;
								
									//Validation Check
								if(target_1=='' || target_1 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("5td_"+a+"_0");
								     t.style.backgroundColor = "#fc0"; 
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_1.length > 65){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_0");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								 
								if(target_2=='' || target_2 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("5td_"+a+"_1");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_2.length > 100){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_1");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_3=='' || target_3 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("5td_"+a+"_2");
								     t.style.backgroundColor = "#fc0"; 
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_3.length > 160){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_2");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_4 != '99' && target_4.length > 1 ){
									target_4 = '0';
								}
								if(target_4=='' || target_4 == null){
									if(target_4 != '99'){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_3");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
										}
								}else{
									if(target_4.length > 6 || target_4.length == 0){
										if(target_4 != '99'){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_3");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
										}
									}
								}
								
								if(target_5 != '없음'){
									if(target_5=='' || target_5 == null){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_4");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}else{
										if(target_5.length > 4 || target_5.length == 0){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("5td_"+a+"_4");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
								}
								
								if(target_6 != '없음'){
									if(target_6=='' || target_6 == null){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_5");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}else{
										if(target_6.length > 10 || target_6.length == 0){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("5td_"+a+"_5");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
								}
								
								if(target_7=='' || target_7 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("5td_"+a+"_6");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_7.length > 65 || target_7.length == 0){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_6");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_8=='' || target_8 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("5td_"+a+"_7");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_8.length > 65 || target_8.length == 0){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_7");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_9=='' || target_9 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("5td_"+a+"_8");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_9.length > 130 || target_9.length == 0){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_8");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_10=='' || target_10 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("5td_"+a+"_9");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_10.length > 65 || target_10.length == 0){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_9");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_11.length > 1 ){
									target_11 = '0';
								}
								if(target_11=='' || target_11 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("5td_"+a+"_10");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_11.length > 1 || target_11.length == 0 || target_11 > 5){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("5td_"+a+"_10");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
							}
						alert("유효성 확인이 완료되었습니다.\n전체건수 : " + tot_cnt + "건(오류대상항목: " + curRowErrCount + "건)");
						$('#curRowErrCount').val(curRowErrCount);
						chkValYN2 = 'Y';
						$('#chkValYN').val('Y');
				  }else if(genreCd == '6'){
						for(var a = 1; a <= tot_cnt; a++){
								var target_1 = document.getElementById("6cell_"+a+"_0").value;
								var target_2 = document.getElementById("6cell_"+a+"_1").value;
								var target_3 = document.getElementById("6cell_"+a+"_2").value;
								var target_4 = document.getElementById("6cell_"+a+"_3").value;
								var target_5 = document.getElementById("6cell_"+a+"_4").value;
								var target_6 = document.getElementById("6cell_"+a+"_5").value;
								var target_7 = document.getElementById("6cell_"+a+"_6").value;
								var target_8 = document.getElementById("6cell_"+a+"_7").value;
								var target_9 = document.getElementById("6cell_"+a+"_8").value;
								var target_10 = document.getElementById("6cell_"+a+"_9").value;
								var target_11 = document.getElementById("6cell_"+a+"_10").value;
								var target_12 = document.getElementById("6cell_"+a+"_11").value;
									//Validation Check
								if(target_1=='' || target_1 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_0");
								     t.style.backgroundColor = "#fc0"; 
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_1.length > 65){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("6td_"+a+"_0");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
									
								if(target_2=='' || target_2 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_1");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_2.length > 100){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("6td_"+a+"_1");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_3=='' || target_3 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_2");
								     t.style.backgroundColor = "#fc0"; 
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_3.length > 160){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("6td_"+a+"_2");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_4=='' || target_4 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_3");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_4.length > 10 || target_4.length == 0){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_3");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
									}
								}
								
								if(target_5=='' || target_5 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_4");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_5.length > 33){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("6td_"+a+"_4");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_6=='' || target_6 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_5");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_6.length > 65){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("6td_"+a+"_5");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_7=='' || target_7 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_6");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_7.length > 65){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("6td_"+a+"_6");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_8=='' || target_8 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_7");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_8.length > 6){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("6td_"+a+"_7");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_9 != '없음'){
									if(target_9=='' || target_9 == null){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("6td_"+a+"_8");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}else{
										if(target_9.length > 8 || target_9.length == 0){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("6td_"+a+"_8");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
								}
								
								if(target_10=='' || target_10 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_9");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}
								
								if(target_11=='' || target_11 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_10");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_11.length > 65){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("6td_"+a+"_10");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
								
								if(target_12.length > 1 ){
									target_12 = '0';
								}
								if(target_12=='' || target_12 == null){
									//오류 시 컬럼 td 색상 변경
									var t = document.getElementById("6td_"+a+"_11");
								     t.style.backgroundColor = "#fc0";
								     isRowError = true;
									 curRowErrCount++;
								}else{
									if(target_12.length > 1 || target_12.length == 0 || target_12 > 5){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("6td_"+a+"_11");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}
								}
							}
						alert("유효성 확인이 완료되었습니다.\n전체건수 : " + tot_cnt + "건(오류대상항목: " + curRowErrCount + "건)");
						$('#curRowErrCount').val(curRowErrCount);
						chkValYN2 = 'Y';
						$('#chkValYN').val('Y');
					  }else if(genreCd == '7'){
							for(var a = 1; a <= tot_cnt; a++){
									
									var target_1 = document.getElementById("7cell_"+a+"_0").value;
									var target_2 = document.getElementById("7cell_"+a+"_1").value;
									var target_3 = document.getElementById("7cell_"+a+"_2").value;
									var target_4 = document.getElementById("7cell_"+a+"_3").value;
									var target_5 = document.getElementById("7cell_"+a+"_4").value;
									var target_6 = document.getElementById("7cell_"+a+"_5").value;
									var target_7 = document.getElementById("7cell_"+a+"_6").value;
									var target_8 = document.getElementById("7cell_"+a+"_7").value;
									var target_9 = document.getElementById("7cell_"+a+"_8").value;
									var target_10 = document.getElementById("7cell_"+a+"_9").value;
									var target_11 = document.getElementById("7cell_"+a+"_10").value;
									var target_12 = document.getElementById("7cell_"+a+"_11").value;
									var target_13 = document.getElementById("7cell_"+a+"_12").value;
									var target_14 = document.getElementById("7cell_"+a+"_13").value;
									var target_15 = document.getElementById("7cell_"+a+"_14").value;
									var target_16 = document.getElementById("7cell_"+a+"_15").value;
									var target_17 = document.getElementById("7cell_"+a+"_16").value;
										//Validation Check
									if(target_1=='' || target_1 == null){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("7td_"+a+"_0");
									     t.style.backgroundColor = "#fc0"; 
									     isRowError = true;
										 curRowErrCount++;
									}else{
										if(target_1.length > 65){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_0");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
										
									if(target_2=='' || target_2 == null){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("7td_"+a+"_1");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}else{
										if(target_2.length > 100){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_1");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
									
									if(target_3=='' || target_3 == null){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("7td_"+a+"_2");
									     t.style.backgroundColor = "#fc0"; 
									     isRowError = true;
										 curRowErrCount++;
									}else{
										if(target_3.length > 160){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_2");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
									
									if(target_4 != undefined){
										if(target_4 != '' || target_4 != null ){
											if(target_4.length > 160){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("7td_"+a+"_3");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
									}
									
									if(target_5=='' || target_5 == null){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("7td_"+a+"_4");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}else{
										if(target_5.length > 33){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_4");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
									
									if(target_6 != '없음'){
										if(target_6=='' || target_6 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_5");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_6.length > 8 || target_6.length == 0){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("7td_"+a+"_5");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
									}
									
									if(target_7 != undefined){
										if(target_7.length > 100){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("7td_"+a+"_6");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
										}
									}
									
									if(target_8 != undefined){
										if(target_8.length > 165){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("7td_"+a+"_7");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
										}
									}
									
									if(target_9 != undefined){
										if(target_9.length > 100){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_8");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
									
									if(target_10 != undefined){
										if(target_10.length > 165){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_9");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
									
									if(target_11 != undefined){
										if(target_11.length > 333){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_10");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
									
									if(target_12 != '없음'){
										if(target_12=='' || target_12 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_11");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_12.length > 1000){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("7td_"+a+"_11");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
									}
									
									if(target_13 != '없음'){
										if(target_13=='' || target_13 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_12");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_13.length > 333){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("7td_"+a+"_12");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
									}
									
									if(target_14 != undefined){
										if(target_14.length > 333){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_13");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
									
									if(target_15 != '없음'){
										if(target_15=='' || target_15 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_14");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_15.length > 8 || target_15.length == 0){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("7td_"+a+"_14");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
									}
									
									if(target_16 != undefined){
										if(target_16.length > 65){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_15");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
									
									if(target_17.length > 1){
										target_17 = 0;
									}
									if(target_17=='' || target_17 == null){
										//오류 시 컬럼 td 색상 변경
										var t = document.getElementById("7td_"+a+"_16");
									     t.style.backgroundColor = "#fc0";
									     isRowError = true;
										 curRowErrCount++;
									}else{
										if(target_17.length > 1 || target_17.length == 0 || target_17 > 5){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("7td_"+a+"_16");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}
									}
								}
							alert("유효성 확인이 완료되었습니다.\n전체건수 : " + tot_cnt + "건(오류대상항목: " + curRowErrCount + "건)");
							$('#curRowErrCount').val(curRowErrCount);
							chkValYN2 = 'Y';
							$('#chkValYN').val('Y');
						  }else if(genreCd == '8'){
								for(var a = 1; a <= tot_cnt; a++){
										
										var target_1 = document.getElementById("8cell_"+a+"_0").value;
										var target_2 = document.getElementById("8cell_"+a+"_1").value;
										var target_3 = document.getElementById("8cell_"+a+"_2").value;
										var target_4 = document.getElementById("8cell_"+a+"_3").value;
										var target_5 = document.getElementById("8cell_"+a+"_4").value;
										var target_6 = document.getElementById("8cell_"+a+"_5").value;
										var target_7 = document.getElementById("8cell_"+a+"_6").value;
										var target_8 = document.getElementById("8cell_"+a+"_7").value;
										var target_9 = document.getElementById("8cell_"+a+"_8").value;
										var target_10 = document.getElementById("8cell_"+a+"_9").value;
										var target_11 = document.getElementById("8cell_"+a+"_10").value;
										var target_12 = document.getElementById("8cell_"+a+"_11").value;
										
											//Validation Check
										if(target_1=='' || target_1 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("8td_"+a+"_0");
										     t.style.backgroundColor = "#fc0"; 
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_1.length > 65){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_0");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
											
										if(target_2=='' || target_2 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("8td_"+a+"_1");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}else{
											//if(target_2.nodeValue != 'Y' || target_2.length > 100){
											if(target_2.length > 100){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_1");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
										
										if(target_3=='' || target_3 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("8td_"+a+"_2");
										     t.style.backgroundColor = "#fc0"; 
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_3.length > 160){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_2");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
										
										if(target_4 != '없음'){
											if(target_4=='' || target_4 == null){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_3");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}else{
												if(target_4.length > 2500 || target_4.length == 0){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_3");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
												}
											}
										}
										
										if(target_5 != '없음'){
											if(target_5=='' || target_5 == null){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_4");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}else{
												if(target_5.length > 650 || target_5.length == 0){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("8td_"+a+"_4");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}
											}
										}
										
										if(target_6=='' || target_6 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("8td_"+a+"_5");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_6.length > 8 || target_6.length == 0){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_5");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
										
										if(target_7=='' || target_7 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("8td_"+a+"_6");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_7.length > 300 || target_7.length == 0){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_6");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
										
										if(target_8=='' || target_8 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("8td_"+a+"_7");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_8.length > 180 || target_8.length == 0){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_7");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
										
										if(target_9 != '없음'){
											if(target_9=='' || target_9 == null){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_8");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}else{
												if(target_9.length > 180 || target_9.length == 0){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("8td_"+a+"_8");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}
											}
										}
										
										if(target_10=='' || target_10 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("8td_"+a+"_9");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_10.length > 1 || target_10.length == 0 || target_10 != 'Y' && target_10 != 'N'){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_9");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
										
										if(target_11 != undefined){
											if(target_11.length > 37){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_10");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
										
										if(target_12.length > 1){
											target_12 = 0;
										}
										if(target_12=='' || target_12 == null){
											//오류 시 컬럼 td 색상 변경
											var t = document.getElementById("8td_"+a+"_11");
										     t.style.backgroundColor = "#fc0";
										     isRowError = true;
											 curRowErrCount++;
										}else{
											if(target_12.length > 1 || target_12.length == 0 || target_12 > 5){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("8td_"+a+"_11");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}
										}
									}
								alert("유효성 확인이 완료되었습니다.\n전체건수 : " + tot_cnt + "건(오류대상항목: " + curRowErrCount + "건)");
								$('#curRowErrCount').val(curRowErrCount);
								chkValYN2 = 'Y';
								$('#chkValYN').val('Y');
							  }else if(genreCd == '99'){
									for(var a = 1; a <= tot_cnt; a++){
											
											var target_1 = document.getElementById("99cell_"+a+"_0").value;
											var target_2 = document.getElementById("99cell_"+a+"_1").value;
											var target_3 = document.getElementById("99cell_"+a+"_2").value;
											var target_4 = document.getElementById("99cell_"+a+"_3").value;
											var target_5 = document.getElementById("99cell_"+a+"_4").value;
											var target_6 = document.getElementById("99cell_"+a+"_5").value;
											var target_7 = document.getElementById("99cell_"+a+"_6").value;
											var target_8 = document.getElementById("99cell_"+a+"_7").value;
											var target_9 = document.getElementById("99cell_"+a+"_8").value;
											
											//Validation Check
											if(target_1=='' || target_1 == null){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("99td_"+a+"_0");
											     t.style.backgroundColor = "#fc0"; 
											     isRowError = true;
												 curRowErrCount++;
											}else{
												if(target_1.length > 65){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("99td_"+a+"_0");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}
											}
											
											if(target_2=='' || target_2 == null){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("99td_"+a+"_1");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}else{
												if(target_2.length > 100){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("99td_"+a+"_1");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}
											}
											
											if(target_3=='' || target_3 == null){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("99td_"+a+"_2");
											     t.style.backgroundColor = "#fc0"; 
											     isRowError = true;
												 curRowErrCount++;
											}else{
												if(target_3.length > 6 || target_3 == 0 || target_3 > 6){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("99td_"+a+"_2");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}
											}
											
											if(target_4 != '없음'){
												if(target_4=='' || target_4 == null){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("99td_"+a+"_3");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}else{
													if(target_4.length > 160){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("99td_"+a+"_3");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
													}
												}
											}
											
											if(target_5=='' || target_5 == null){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("99td_"+a+"_4");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}else{
												if(target_5.length > 65){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("99td_"+a+"_4");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}
											}
											
											if(target_6 != '없음'){
												if(target_6=='' || target_6 == null){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("99td_"+a+"_5");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}else{
													if(target_6.length > 4 || target_6 == 0){
														//오류 시 컬럼 td 색상 변경
														var t = document.getElementById("99td_"+a+"_5");
													     t.style.backgroundColor = "#fc0";
													     isRowError = true;
														 curRowErrCount++;
													}
												}
											}
											
											if(target_7=='' || target_7 == null){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("99td_"+a+"_6");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}else{
												if(target_7.length > 65){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("99td_"+a+"_6");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}
											}
											
											if(target_8 != '없음'){
												if(target_8=='' || target_8 == null){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("99td_"+a+"_7");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}else{
													if(target_8.length > 8 || target_8 == 0){
														//오류 시 컬럼 td 색상 변경
														var t = document.getElementById("99td_"+a+"_7");
													     t.style.backgroundColor = "#fc0";
													     isRowError = true;
														 curRowErrCount++;
													}
												}
											}
											
											if(target_9.length > 1){
												target_9 = 0;
											}
											if(target_9=='' || target_9 == null){
												//오류 시 컬럼 td 색상 변경
												var t = document.getElementById("99td_"+a+"_8");
											     t.style.backgroundColor = "#fc0";
											     isRowError = true;
												 curRowErrCount++;
											}else{
												if(target_9.length > 1 || target_9.length == 0 || target_9 == 0 || target_9 > 5){
													//오류 시 컬럼 td 색상 변경
													var t = document.getElementById("99td_"+a+"_8");
												     t.style.backgroundColor = "#fc0";
												     isRowError = true;
													 curRowErrCount++;
												}
											}
										}
									alert("유효성 확인이 완료되었습니다.\n전체건수 : " + tot_cnt + "건(오류대상항목: " + curRowErrCount + "건)");
									$('#curRowErrCount').val(curRowErrCount);
									chkValYN2 = 'Y';
									$('#chkValYN').val('Y');
								  }
								}

			//일괄수정
			function fncSave(tot_cnt) {
				var f = document.chkForm;
		 		var chkValYN2 = $('#chkValYN').val();
		 		var genreCd = $('#GENRE_CODE').val();
				 
		 		if(chkValYN2 == 'N' || chkValYN2 == null || chkValYN2 == ''){
		 			alert("유효성 검사를 해주세요.")
		 		}else if($('#curRowErrCount').val() != 0){
		 			alert("오류대상 항목이 존재한 경우에는 등록하실 수 없습니다.");
		 		}else{
		 			if(genreCd == '0'){
						alert("엑셀양식을 선택하세요.");
					} else if(genreCd == '1'){	
				 		for(var a = 1; a <= tot_cnt; a++){
				 			var target_1 = document.getElementById("1cell_"+a+"_0").value;
							var target_2 = document.getElementById("1cell_"+a+"_1").value;
							var target_3 = document.getElementById("1cell_"+a+"_2").value;
							var target_4 = document.getElementById("1cell_"+a+"_3").value;
							var target_5 = document.getElementById("1cell_"+a+"_4").value;
							var target_6 = document.getElementById("1cell_"+a+"_5").value;
							var target_7 = document.getElementById("1cell_"+a+"_6").value;
							var target_8 = document.getElementById("1cell_"+a+"_7").value;
							var target_9 = document.getElementById("1cell_"+a+"_8").value;
							var target_10 = document.getElementById("1cell_"+a+"_9").value;
							var target_11 = document.getElementById("1cell_"+a+"_10").value;
							var target_12 = document.getElementById("1cell_"+a+"_11").value;
							var target_13 = document.getElementById("1cell_"+a+"_12").value;
							var target_14 = document.getElementById("1cell_"+a+"_13").value;
							var target_15 = document.getElementById("1cell_"+a+"_14").value;
							var target_16 = document.getElementById("1cell_"+a+"_15").value;
							var target_17 = document.getElementById("1cell_"+a+"_16").value;
							var target_18 = document.getElementById("1cell_"+a+"_17").value;
							var target_19 = document.getElementById("1cell_"+a+"_18").value;
							var target_20 = document.getElementById("1cell_"+a+"_19").value;
							var target_21 = document.getElementById("1cell_"+a+"_20").value;
							var target_22 = document.getElementById("1cell_"+a+"_21").value;
							
							var target_23 = document.getElementById("reptChrrName").value;
							var target_24 = document.getElementById("reptChrrPosi").value;
							var target_25 = document.getElementById("reptChrrTelx").value;
							var target_26 = document.getElementById("reptChrrMail").value;
							var target_27 = document.getElementById("commName").value;
							
							var var1 = encodeURI(encodeURIComponent(target_1));
							var var2 = encodeURI(encodeURIComponent(target_2));
							var var3 = encodeURI(encodeURIComponent(target_3));
							var var4 = encodeURI(encodeURIComponent(target_4));
							var var5 = encodeURI(encodeURIComponent(target_5));
							var var6 = encodeURI(encodeURIComponent(target_6));
							var var7 = encodeURI(encodeURIComponent(target_7));
							var var8 = encodeURI(encodeURIComponent(target_8));
							var var9 = encodeURI(encodeURIComponent(target_9));
							var var10 = encodeURI(encodeURIComponent(target_10));
							var var11 = encodeURI(encodeURIComponent(target_11));
							var var12 = encodeURI(encodeURIComponent(target_12));
							var var13 = encodeURI(encodeURIComponent(target_13));
							var var14 = encodeURI(encodeURIComponent(target_14));
							var var15 = encodeURI(encodeURIComponent(target_15));
							var var16 = encodeURI(encodeURIComponent(target_16));
							var var17 = encodeURI(encodeURIComponent(target_17));
							var var18 = encodeURI(encodeURIComponent(target_18));
							var var19 = encodeURI(encodeURIComponent(target_19));
							var var20 = encodeURI(encodeURIComponent(target_20));
							var var21 = encodeURI(encodeURIComponent(target_21));
							var var22 = encodeURI(encodeURIComponent(target_22));
							
							var var23 = encodeURI(encodeURIComponent(target_23));
							var var24 = encodeURI(encodeURIComponent(target_24));
							var var25 = encodeURI(encodeURIComponent(target_25));
							var var26 = encodeURI(encodeURIComponent(target_26));
							var var27 = encodeURI(encodeURIComponent(target_27));
							
								f.action = '<c:url value="/console/rcept/fdcrAd17MusicUpdate.page"/>?RGST_ORGN_NAME='
									+ var1+'&COMM_WORKS_ID='+var2+'&WORKS_TITLE='+var3+'&NATION_CD='
									+var4+'&ALBUM_TITLE='+var5+'&ALBUM_LABLE='+var6+'&DISK_SIDE='+var7+'&TRACK_NO='
									+var8+'&ALBUM_ISSU_YEAR='+var9+'&LYRC='+var10+'&COMP='+var11+'&ARRA='+var12
									+'&TRAN='+var13+'&SING='+var14+'&PERF='+var15+'&PROD='+var16+'&COMM_MGNT_CD='+var17
									+'&LICE_MGNT_CD='+var18+'&PERF_MGNT_CD='+var19+'&PROD_MGNT_CD='+var20+'&UCI='+var21+'&RATIO='+var22
									+'&tot_cnt='+tot_cnt
									+'&reptChrrName='+var23
									+'&reptChrrPosi='+var24
									+'&reptChrrTelx='+var25
									+'&reptChrrMail='+var26
									+'&commName='+var27;
								f.submit();
				 		}
			 		}else if(genreCd == '2'){	
			 			for(var a = 1; a <= tot_cnt; a++){
			 				
				 			var target_1 = document.getElementById("2cell_"+a+"_0").value;
							var target_2 = document.getElementById("2cell_"+a+"_1").value;
							var target_3 = document.getElementById("2cell_"+a+"_2").value;
							var target_4 = document.getElementById("2cell_"+a+"_3").value;
							var target_5 = document.getElementById("2cell_"+a+"_4").value;
							var target_6 = document.getElementById("2cell_"+a+"_5").value;
							var target_7 = document.getElementById("2cell_"+a+"_6").value;
							var target_8 = document.getElementById("2cell_"+a+"_7").value;
							var target_9 = document.getElementById("2cell_"+a+"_8").value;
							var target_10 = document.getElementById("2cell_"+a+"_9").value;
							
							var target_11 = document.getElementById("reptChrrName").value;
							var target_12 = document.getElementById("reptChrrPosi").value;
							var target_13 = document.getElementById("reptChrrTelx").value;
							var target_14 = document.getElementById("reptChrrMail").value;
							var target_15 = document.getElementById("commName").value;
							
							var var1 = encodeURI(encodeURIComponent(target_1));
							var var2 = encodeURI(encodeURIComponent(target_2));
							var var3 = encodeURI(encodeURIComponent(target_3));
							var var4 = encodeURI(encodeURIComponent(target_4));
							var var5 = encodeURI(encodeURIComponent(target_5));
							var var6 = encodeURI(encodeURIComponent(target_6));
							var var7 = encodeURI(encodeURIComponent(target_7));
							var var8 = encodeURI(encodeURIComponent(target_8));
							var var9 = encodeURI(encodeURIComponent(target_9));
							var var10 = encodeURI(encodeURIComponent(target_10));
							
							var var11 = encodeURI(encodeURIComponent(target_11));
							var var12 = encodeURI(encodeURIComponent(target_12));
							var var13 = encodeURI(encodeURIComponent(target_13));
							var var14 = encodeURI(encodeURIComponent(target_14));
							var var15 = encodeURI(encodeURIComponent(target_15));
							
							f.action = '<c:url value="/console/rcept/fdcrAd17BookUpdate.page"/>?RGST_ORGN_NAME='
								+ var1+'&COMM_WORKS_ID='+var2+'&WORKS_TITLE='+var3+'&WORKS_SUB_TITLE='
								+var4+'&CRT_YEAR='+var5+'&BOOK_TITLE='+var6+'&BOOK_PUBLISHER='+var7+'&BOOK_ISSU_YEAR='
								+var8+'&COPT_NAME='+var9+'&COMM_MGNT_CD='+var10+'&tot_cnt='+tot_cnt
								+'&reptChrrName='+var11
								+'&reptChrrPosi='+var12
								+'&reptChrrTelx='+var13
								+'&reptChrrMail='+var14
								+'&commName='+var15;
							f.submit();
				 		}
			 		}else if(genreCd == '3'){	
				 		for(var a = 1; a <= tot_cnt; a++){
				 			
							var target_1 = document.getElementById("3cell_"+a+"_0").value;
							var target_2 = document.getElementById("3cell_"+a+"_1").value;
							var target_3 = document.getElementById("3cell_"+a+"_2").value;
							var target_4 = document.getElementById("3cell_"+a+"_3").value;
							var target_5 = document.getElementById("3cell_"+a+"_4").value;
							var target_6 = document.getElementById("3cell_"+a+"_5").value;
							var target_7 = document.getElementById("3cell_"+a+"_6").value;
							var target_8 = document.getElementById("3cell_"+a+"_7").value;
							var target_9 = document.getElementById("3cell_"+a+"_8").value;
							var target_10 = document.getElementById("3cell_"+a+"_9").value;
							var target_11 = document.getElementById("3cell_"+a+"_10").value;
							var target_12 = document.getElementById("3cell_"+a+"_11").value;
							var target_13 = document.getElementById("3cell_"+a+"_12").value;
							var target_14 = document.getElementById("3cell_"+a+"_13").value;
							
							var target_15 = document.getElementById("reptChrrName").value;
							var target_16 = document.getElementById("reptChrrPosi").value;
							var target_17 = document.getElementById("reptChrrTelx").value;
							var target_18 = document.getElementById("reptChrrMail").value;
							var target_19 = document.getElementById("commName").value;
							
							var var1 = encodeURI(encodeURIComponent(target_1));
							var var2 = encodeURI(encodeURIComponent(target_2));
							var var3 = encodeURI(encodeURIComponent(target_3));
							var var4 = encodeURI(encodeURIComponent(target_4));
							var var5 = encodeURI(encodeURIComponent(target_5));
							var var6 = encodeURI(encodeURIComponent(target_6));
							var var7 = encodeURI(encodeURIComponent(target_7));
							var var8 = encodeURI(encodeURIComponent(target_8));
							var var9 = encodeURI(encodeURIComponent(target_9));
							var var10 = encodeURI(encodeURIComponent(target_10));
							var var11 = encodeURI(encodeURIComponent(target_11));
							var var12 = encodeURI(encodeURIComponent(target_12));
							var var13 = encodeURI(encodeURIComponent(target_13));
							var var14 = encodeURI(encodeURIComponent(target_14));
							
							var var15 = encodeURI(encodeURIComponent(target_15));
							var var16 = encodeURI(encodeURIComponent(target_16));
							var var17 = encodeURI(encodeURIComponent(target_17));
							var var18 = encodeURI(encodeURIComponent(target_18));
							var var19 = encodeURI(encodeURIComponent(target_19));
							
							f.action = '<c:url value="/console/rcept/fdcrAd17ScriptUpdate.page"/>?RGST_ORGN_NAME='
								+ var1+'&COMM_WORKS_ID='+var2+'&WORKS_TITLE='+var3+'&WORKS_ORIG_TITLE='
								+var4+'&SCRT_GENRE_CD='+var5+'&SCRP_SUBJ_CD='+var6+'&ORIG_WRITER='+var7+'&PLAYER='
								+var8+'&BROD_ORD_SEQ='+var9+'&BORD_DATE='+var10+'&MAKE_CPY='+var11
								+'&WRITER='+var12+'&DIRECTOR='+var13+'&COMM_MGNT_CD='+var14
								+'&tot_cnt='+tot_cnt
								+'&reptChrrName='+var15
								+'&reptChrrPosi='+var16
								+'&reptChrrTelx='+var17
								+'&reptChrrMail='+var18
								+'&commName='+var19;
							f.submit();
				 		}
			 		}else if(genreCd == '4'){	
				 		for(var a = 1; a <= tot_cnt; a++){
				 			var target_1 = document.getElementById("4cell_"+a+"_0").value;
							var target_2 = document.getElementById("4cell_"+a+"_1").value;
							var target_3 = document.getElementById("4cell_"+a+"_2").value;
							var target_4 = document.getElementById("4cell_"+a+"_3").value;
							var target_5 = document.getElementById("4cell_"+a+"_4").value;
							var target_6 = document.getElementById("4cell_"+a+"_5").value;
							var target_7 = document.getElementById("4cell_"+a+"_6").value;
							var target_8 = document.getElementById("4cell_"+a+"_7").value;
							var target_9 = document.getElementById("4cell_"+a+"_8").value;
							var target_10 = document.getElementById("4cell_"+a+"_9").value;
							var target_11 = document.getElementById("4cell_"+a+"_10").value;
							var target_12 = document.getElementById("4cell_"+a+"_11").value;
							
							var target_13 = document.getElementById("reptChrrName").value;
							var target_14 = document.getElementById("reptChrrPosi").value;
							var target_15 = document.getElementById("reptChrrTelx").value;
							var target_16 = document.getElementById("reptChrrMail").value;
							var target_17 = document.getElementById("commName").value;
							
							var var1 = encodeURI(encodeURIComponent(target_1));
							var var2 = encodeURI(encodeURIComponent(target_2));
							var var3 = encodeURI(encodeURIComponent(target_3));
							var var4 = encodeURI(encodeURIComponent(target_4));
							var var5 = encodeURI(encodeURIComponent(target_5));
							var var6 = encodeURI(encodeURIComponent(target_6));
							var var7 = encodeURI(encodeURIComponent(target_7));
							var var8 = encodeURI(encodeURIComponent(target_8));
							var var9 = encodeURI(encodeURIComponent(target_9));
							var var10 = encodeURI(encodeURIComponent(target_10));
							var var11 = encodeURI(encodeURIComponent(target_11));
							var var12 = encodeURI(encodeURIComponent(target_12));
							
							var var13 = encodeURI(encodeURIComponent(target_13));
							var var14 = encodeURI(encodeURIComponent(target_14));
							var var15 = encodeURI(encodeURIComponent(target_15));
							var var16 = encodeURI(encodeURIComponent(target_16));
							var var17 = encodeURI(encodeURIComponent(target_17));
							
							f.action = '<c:url value="/console/rcept/fdcrAd17MovieUpdate.page"/>?RGST_ORGN_NAME='
								+ var1+'&COMM_WORKS_ID='+var2+'&WORKS_TITLE='+var3+'&CRT_YEAR='
								+var4+'&PLAYER='+var5+'&DIRECT='+var6+'&WRITER='+var7+'&DIRECTOR='
								+var8+'&PRODUCER='+var9+'&DISTRIBUTOR='+var10+'&INVESTOR='+var11
								+'&COMM_MGNT_CD='+var12
								+'&tot_cnt='+tot_cnt
								+'&reptChrrName='+var13
								+'&reptChrrPosi='+var14
								+'&reptChrrTelx='+var15
								+'&reptChrrMail='+var16
								+'&commName='+var17;
							f.submit();
				 		}
			 		}else if(genreCd == '5'){	
				 		for(var a = 1; a <= tot_cnt; a++){
				 			
							var target_1 = document.getElementById("5cell_"+a+"_0").value;
							var target_2 = document.getElementById("5cell_"+a+"_1").value;
							var target_3 = document.getElementById("5cell_"+a+"_2").value;
							var target_4 = document.getElementById("5cell_"+a+"_3").value;
							var target_5 = document.getElementById("5cell_"+a+"_4").value;
							var target_6 = document.getElementById("5cell_"+a+"_5").value;
							var target_7 = document.getElementById("5cell_"+a+"_6").value;
							var target_8 = document.getElementById("5cell_"+a+"_7").value;
							var target_9 = document.getElementById("5cell_"+a+"_8").value;
							var target_10 = document.getElementById("5cell_"+a+"_9").value;
							var target_11 = document.getElementById("5cell_"+a+"_10").value;
							
							var target_12 = document.getElementById("reptChrrName").value;
							var target_13 = document.getElementById("reptChrrPosi").value;
							var target_14 = document.getElementById("reptChrrTelx").value;
							var target_15 = document.getElementById("reptChrrMail").value;
							var target_16 = document.getElementById("commName").value;
							
							var var1 = encodeURI(encodeURIComponent(target_1));
							var var2 = encodeURI(encodeURIComponent(target_2));
							var var3 = encodeURI(encodeURIComponent(target_3));
							var var4 = encodeURI(encodeURIComponent(target_4));
							var var5 = encodeURI(encodeURIComponent(target_5));
							var var6 = encodeURI(encodeURIComponent(target_6));
							var var7 = encodeURI(encodeURIComponent(target_7));
							var var8 = encodeURI(encodeURIComponent(target_8));
							var var9 = encodeURI(encodeURIComponent(target_9));
							var var10 = encodeURI(encodeURIComponent(target_10));
							var var11 = encodeURI(encodeURIComponent(target_11));
							
							var var12 = encodeURI(encodeURIComponent(target_12));
							var var13 = encodeURI(encodeURIComponent(target_13));
							var var14 = encodeURI(encodeURIComponent(target_14));
							var var15 = encodeURI(encodeURIComponent(target_15));
							var var16 = encodeURI(encodeURIComponent(target_16));
							
							f.action = '<c:url value="/console/rcept/fdcrAd17BroadcastUpdate.page"/>?RGST_ORGN_NAME='
								+ var1+'&COMM_WORKS_ID='+var2+'&WORKS_TITLE='+var3+'&BORD_GENRE_CD='
								+var4+'&MAKE_YEAR='+var5+'&BROD_ORD_SEQ='+var6+'&DIRECTOR='+var7+'&MAKE_CPY='
								+var8+'&PLAYER='+var9+'&WRITER='+var10+'&COMM_MGNT_CD='+var11
								+'&tot_cnt='+tot_cnt
								+'&reptChrrName='+var12
								+'&reptChrrPosi='+var13
								+'&reptChrrTelx='+var14
								+'&reptChrrMail='+var15
								+'&commName='+var16;
							f.submit();
				 		}
			 		}else if(genreCd == '6'){	
				 		for(var a = 1; a <= tot_cnt; a++){
				 			
							var target_1 = document.getElementById("6cell_"+a+"_0").value;
							var target_2 = document.getElementById("6cell_"+a+"_1").value;
							var target_3 = document.getElementById("6cell_"+a+"_2").value;
							var target_4 = document.getElementById("6cell_"+a+"_3").value;
							var target_5 = document.getElementById("6cell_"+a+"_4").value;
							var target_6 = document.getElementById("6cell_"+a+"_5").value;
							var target_7 = document.getElementById("6cell_"+a+"_6").value;
							var target_8 = document.getElementById("6cell_"+a+"_7").value;
							var target_9 = document.getElementById("6cell_"+a+"_8").value;
							var target_10 = document.getElementById("6cell_"+a+"_9").value;
							var target_11 = document.getElementById("6cell_"+a+"_10").value;
							var target_12 = document.getElementById("6cell_"+a+"_11").value;
							
							var target_13 = document.getElementById("reptChrrName").value;
							var target_14 = document.getElementById("reptChrrPosi").value;
							var target_15 = document.getElementById("reptChrrTelx").value;
							var target_16 = document.getElementById("reptChrrMail").value;
							var target_17 = document.getElementById("commName").value;
							
							var var1 = encodeURI(encodeURIComponent(target_1));
							var var2 = encodeURI(encodeURIComponent(target_2));
							var var3 = encodeURI(encodeURIComponent(target_3));
							var var4 = encodeURI(encodeURIComponent(target_4));
							var var5 = encodeURI(encodeURIComponent(target_5));
							var var6 = encodeURI(encodeURIComponent(target_6));
							var var7 = encodeURI(encodeURIComponent(target_7));
							var var8 = encodeURI(encodeURIComponent(target_8));
							var var9 = encodeURI(encodeURIComponent(target_9));
							var var10 = encodeURI(encodeURIComponent(target_10));
							var var11 = encodeURI(encodeURIComponent(target_11));
							var var12 = encodeURI(encodeURIComponent(target_12));
							
							var var13 = encodeURI(encodeURIComponent(target_13));
							var var14 = encodeURI(encodeURIComponent(target_14));
							var var15 = encodeURI(encodeURIComponent(target_15));
							var var16 = encodeURI(encodeURIComponent(target_16));
							var var17 = encodeURI(encodeURIComponent(target_17));
							
							f.action = '<c:url value="/console/rcept/fdcrAd17NewsUpdate.page"/>?RGST_ORGN_NAME='
								+ var1+'&COMM_WORKS_ID='+var2+'&WORKS_TITLE='+var3+'&PAPE_NO='
								+var4+'&PAPE_KIND='+var5+'&CONTRIBUTOR='+var6+'&REPOTER='+var7+'&REVISION_ID='
								+var8+'&ARTICL_DATE='+var9+'&ARTICL_ISSU_DTTM='+var10+'&PROVIDER='+var11
								+'&COMM_MGNT_CD='+var12
								+'&tot_cnt='+tot_cnt
								+'&reptChrrName='+var13
								+'&reptChrrPosi='+var14
								+'&reptChrrTelx='+var15
								+'&reptChrrMail='+var16
								+'&commName='+var17;
							f.submit();
				 		}
			 		}else if(genreCd == '7'){	
				 		for(var a = 1; a <= tot_cnt; a++){
							
							var target_1 = document.getElementById("7cell_"+a+"_0").value;
							var target_2 = document.getElementById("7cell_"+a+"_1").value;
							var target_3 = document.getElementById("7cell_"+a+"_2").value;
							var target_4 = document.getElementById("7cell_"+a+"_3").value;
							var target_5 = document.getElementById("7cell_"+a+"_4").value;
							var target_6 = document.getElementById("7cell_"+a+"_5").value;
							var target_7 = document.getElementById("7cell_"+a+"_6").value;
							var target_8 = document.getElementById("7cell_"+a+"_7").value;
							var target_9 = document.getElementById("7cell_"+a+"_8").value;
							var target_10 = document.getElementById("7cell_"+a+"_9").value;
							var target_11 = document.getElementById("7cell_"+a+"_10").value;
							var target_12 = document.getElementById("7cell_"+a+"_11").value;
							var target_13 = document.getElementById("7cell_"+a+"_12").value;
							var target_14 = document.getElementById("7cell_"+a+"_13").value;
							var target_15 = document.getElementById("7cell_"+a+"_14").value;
							var target_16 = document.getElementById("7cell_"+a+"_15").value;
							var target_17 = document.getElementById("7cell_"+a+"_16").value;
							
							var target_18 = document.getElementById("reptChrrName").value;
							var target_19 = document.getElementById("reptChrrPosi").value;
							var target_20 = document.getElementById("reptChrrTelx").value;
							var target_21 = document.getElementById("reptChrrMail").value;
							var target_22 = document.getElementById("commName").value;
							
							var var1 = encodeURI(encodeURIComponent(target_1));
							var var2 = encodeURI(encodeURIComponent(target_2));
							var var3 = encodeURI(encodeURIComponent(target_3));
							var var4 = encodeURI(encodeURIComponent(target_4));
							var var5 = encodeURI(encodeURIComponent(target_5));
							var var6 = encodeURI(encodeURIComponent(target_6));
							var var7 = encodeURI(encodeURIComponent(target_7));
							var var8 = encodeURI(encodeURIComponent(target_8));
							var var9 = encodeURI(encodeURIComponent(target_9));
							var var10 = encodeURI(encodeURIComponent(target_10));
							var var11 = encodeURI(encodeURIComponent(target_11));
							var var12 = encodeURI(encodeURIComponent(target_12));
							var var13 = encodeURI(encodeURIComponent(target_13));
							var var14 = encodeURI(encodeURIComponent(target_14));
							var var15 = encodeURI(encodeURIComponent(target_15));
							var var16 = encodeURI(encodeURIComponent(target_16));
							var var17 = encodeURI(encodeURIComponent(target_17));
							
							var var18 = encodeURI(encodeURIComponent(target_18));
							var var19 = encodeURI(encodeURIComponent(target_19));
							var var20 = encodeURI(encodeURIComponent(target_20));
							var var21 = encodeURI(encodeURIComponent(target_21));
							var var22 = encodeURI(encodeURIComponent(target_22));
							
							f.action = '<c:url value="/console/rcept/fdcrAd17ArtUpdate.page"/>?RGST_ORGN_NAME='
								+ var1+'&COMM_WORKS_ID='+var2+'&WORKS_TITLE='+var3+'&WORKS_SUB_TITLE='
								+var4+'&KIND='+var5+'&MAKE_DATE='+var6+'&SOURCE_INFO='+var7+'&SIZE_INFO='
								+var8+'&RESOLUTION='+var9+'&MAIN_MTRL='+var10+'&TXTR='+var11
								+'&IMG_URL='+var12+'&DETL_URL='+var13+'&WRITER='+var14
								+'&POSS_DATE='+var15+'&POSS_ORGN_NAME='+var16+'&COMM_MGNT_CD='+var17
								+'&tot_cnt='+tot_cnt
								+'&reptChrrName='+var18
								+'&reptChrrPosi='+var19
								+'&reptChrrTelx='+var20
								+'&reptChrrMail='+var21
								+'&commName='+var22;
							f.submit();
				 		}
			 		}else if(genreCd == '8'){	
				 		for(var a = 1; a <= tot_cnt; a++){
				 			var target_1 = document.getElementById("8cell_"+a+"_0").value;
							var target_2 = document.getElementById("8cell_"+a+"_1").value;
							var target_3 = document.getElementById("8cell_"+a+"_2").value;
							var target_4 = document.getElementById("8cell_"+a+"_3").value;
							var target_5 = document.getElementById("8cell_"+a+"_4").value;
							var target_6 = document.getElementById("8cell_"+a+"_5").value;
							var target_7 = document.getElementById("8cell_"+a+"_6").value;
							var target_8 = document.getElementById("8cell_"+a+"_7").value;
							var target_9 = document.getElementById("8cell_"+a+"_8").value;
							var target_10 = document.getElementById("8cell_"+a+"_9").value;
							var target_11 = document.getElementById("8cell_"+a+"_10").value;
							var target_12 = document.getElementById("8cell_"+a+"_11").value;
							
							var target_13 = document.getElementById("reptChrrName").value;
							var target_14 = document.getElementById("reptChrrPosi").value;
							var target_15 = document.getElementById("reptChrrTelx").value;
							var target_16 = document.getElementById("reptChrrMail").value;
							var target_17 = document.getElementById("commName").value;
							
							var var1 = encodeURI(encodeURIComponent(target_1));
							var var2 = encodeURI(encodeURIComponent(target_2));
							var var3 = encodeURI(encodeURIComponent(target_3));
							var var4 = encodeURI(encodeURIComponent(target_4));
							var var5 = encodeURI(encodeURIComponent(target_5));
							var var6 = encodeURI(encodeURIComponent(target_6));
							var var7 = encodeURI(encodeURIComponent(target_7));
							var var8 = encodeURI(encodeURIComponent(target_8));
							var var9 = encodeURI(encodeURIComponent(target_9));
							var var10 = encodeURI(encodeURIComponent(target_10));
							var var11 = encodeURI(encodeURIComponent(target_11));
							var var12 = encodeURI(encodeURIComponent(target_12));
							
							var var13 = encodeURI(encodeURIComponent(target_13));
							var var14 = encodeURI(encodeURIComponent(target_14));
							var var15 = encodeURI(encodeURIComponent(target_15));
							var var16 = encodeURI(encodeURIComponent(target_16));
							var var17 = encodeURI(encodeURIComponent(target_17));
							
							f.action = '<c:url value="/console/rcept/fdcrAd17ImageUpdate.page"/>?RGST_ORGN_NAME='
								+ var1+'&COMM_WORKS_ID='+var2+'&WORKS_TITLE='+var3+'&IMAGE_DESC='
								+var4+'&KEYWORD='+var5+'&CRT_YEAR='+var6+'&WRITER='+var7+'&PRODUCER='
								+var8+'&IMAGE_URL='+var9+'&IMAGE_OPEN_YN='+var10+'&ICNX_NUMB='+var11
								+'&COMM_MGNT_CD='+var12
								+'&tot_cnt='+tot_cnt
								+'&reptChrrName='+var13
								+'&reptChrrPosi='+var14
								+'&reptChrrTelx='+var15
								+'&reptChrrMail='+var16
								+'&commName='+var17;
							f.submit();
				 		}
			 		}else if(genreCd == '99'){	
				 		for(var a = 1; a <= tot_cnt; a++){
				 			var target_1 = document.getElementById("99cell_"+a+"_0").value;
							var target_2 = document.getElementById("99cell_"+a+"_1").value;
							var target_3 = document.getElementById("99cell_"+a+"_2").value;
							var target_4 = document.getElementById("99cell_"+a+"_3").value;
							var target_5 = document.getElementById("99cell_"+a+"_4").value;
							var target_6 = document.getElementById("99cell_"+a+"_5").value;
							var target_7 = document.getElementById("99cell_"+a+"_6").value;
							var target_8 = document.getElementById("99cell_"+a+"_7").value;
							var target_9 = document.getElementById("99cell_"+a+"_8").value;
							
							var target_10 = document.getElementById("reptChrrName").value;
							var target_11 = document.getElementById("reptChrrPosi").value;
							var target_12 = document.getElementById("reptChrrTelx").value;
							var target_13 = document.getElementById("reptChrrMail").value;
							var target_14 = document.getElementById("commName").value;
							
							var var1 = encodeURI(encodeURIComponent(target_1));
							var var2 = encodeURI(encodeURIComponent(target_2));
							var var3 = encodeURI(encodeURIComponent(target_3));
							var var4 = encodeURI(encodeURIComponent(target_4));
							var var5 = encodeURI(encodeURIComponent(target_5));
							var var6 = encodeURI(encodeURIComponent(target_6));
							var var7 = encodeURI(encodeURIComponent(target_7));
							var var8 = encodeURI(encodeURIComponent(target_8));
							var var9 = encodeURI(encodeURIComponent(target_9));
							
							var var10 = encodeURI(encodeURIComponent(target_10));
							var var11 = encodeURI(encodeURIComponent(target_11));
							var var12 = encodeURI(encodeURIComponent(target_12));
							var var13 = encodeURI(encodeURIComponent(target_13));
							var var14 = encodeURI(encodeURIComponent(target_14));
							
							f.action = '<c:url value="/console/rcept/fdcrAd17SideUpdate.page"/>?RGST_ORGN_NAME='
								+ var1+'&COMM_WORKS_ID='+var2+'&SIDE_GENRE_CD='+var3+'&WORKS_TITLE='
								+var4+'&COPT_NAME='+var5+'&CRT_YEAR='+var6+'&PUBL_MEDI='+var7+'&PUBL_DATE='
								+var8+'&COMM_MGNT_CD='+var9
								+'&tot_cnt='+tot_cnt
								+'&reptChrrName='+var10
								+'&reptChrrPosi='+var11
								+'&reptChrrTelx='+var12
								+'&reptChrrMail='+var13
								+'&commName='+var14;
							f.submit();
				 		}
			 		}
				}
			}
			
			function fncInsert(){
				var f = document.chkForm;
				alert("등록 하시겠습니까?");
				f.action = '<c:url value="/console/rcept/fdcrAd17Insert.page"/>';
				f.submit();
			}
			
			function fncBack(){
				var f = document.chkForm;
				alert("초기화 하시겠습니까?");
				f.action = '<c:url value="/console/rcept/fdcrAd17List1.page"/>';
				f.submit();
			}
	
	
	
</script>
<style>
.fixed-table-body {
	overflow-x: auto;
}
</style>
<form name="chkForm" id="chkForm" method="post">
	<input type="hidden" id="chkValYN" name="chkValYN" value="" /> 
	<input type="hidden" id="reptChrrName" name="reptChrrName" value="${reptChrrName}"/> 
	<input type="hidden" id="reptChrrPosi" name="reptChrrPosi" value="${reptChrrPosi}"/> 
	<input type="hidden" id="reptChrrTelx" name="reptChrrTelx" value="${reptChrrTelx}"/> 
	<input type="hidden" id="reptChrrMail" name="reptChrrMail" value="${reptChrrMail}"/>
	<input type="hidden" id="commName" name="commName" value="${commName}"/> 
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
	<input type="hidden" id="uploadFailList" name="uploadFailList" value="${uploadFailList}"/>
	<input type="hidden" id="uploadFailListSize" name="uploadFailListSize" value="${fn:length(uploadFailList)}"/>
	<input type="hidden" id="curRowErrCount">
	<div>
		<b>${reptYYYY}년&nbsp;&nbsp;&nbsp;&nbsp; ${reptMM}월
			&nbsp;&nbsp;&nbsp;&nbsp;위탁관리 저작물 보고</b>
	</div>
	<table class="table table-bordered text-center">
		<tbody>
			<tr>
				<th class="text-center">보고기관</th>
				<td class="text-center"><input type="text" id="COMM_NAME" name="COMM_NAME" style="border: 0px; background-color: transparent; width: 100%;" value="${commName}" readonly="readonly"/></td>
				<th class="text-center">보고기관 검색</th>
				<td class="text-center"><select class="form-control"
					id="TRST_ORGN_DIVS_CODE" name="TRST_ORGN_DIVS_CODE"
					style="width: 300px; display: inline-block;">
						<option value="1" selected="selected">대리중개업체</option>
						<option value="2">신탁관리단체</option>
				</select> <input type="button" id="popComm" name="popComm" value="검색"
					style="margin-left: 10px;" onclick="popCommOpen();return false;">
				</td>
			</tr>
			<tr>
				<th class="text-center">담당자</th>
				<td class="text-center"><input type="text" id="REPT_CHRR_NAME"
					name="REPT_CHRR_NAME" value="${reptChrrName}" /></td>
				<th class="text-center">담당자 직위</th>
				<td class="text-center"><input type="text" id="REPT_CHRR_POSI"
					name="REPT_CHRR_POSI" value="${reptChrrPosi}" /></td>
			</tr>
			<tr>
				<th class="text-center">담당자 전화번호</th>
				<td class="text-center"><input type="text" id="REPT_CHRR_TELX"
					name="REPT_CHRR_TELX" value="${reptChrrTelx}" /></td>
				<th class="text-center">담당자 이메일</th>
				<td class="text-center"><input type="text" id="REPT_CHRR_MAIL"
					name="REPT_CHRR_MAIL" value="${reptChrrMail}" /></td>
			</tr>
			<tr>
				<th class="text-center">보고저작물 없음</th>
				<td class="text-center" colspan="3"><input type="checkbox"
					id="chk01" name="chk01"
					onchange="javascript:chk01_change(this.value);" /> <input
					type="button" id="checkButton" name="checkButton" value="등록"
					style="float: right; display: none;" onclick="noDataInsert();">
				</td>
			</tr>
		</tbody>
	</table>

	<div class="box box-default">
		<!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<form name="searchForm" id="searchForm" method="post">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th class="text-center search-th">엑셀양식</th>
										<td><select class="form-control" id="GENRE_CODE" name="GENRE_CODE" onchange="javascript:form_change(this.value);">
												<option value="0">-선택-</option>
												<option value="1"
													<c:if test="${genreCode eq '1'}">selected="selected"</c:if>>음악</option>
												<option value="2"
													<c:if test="${genreCode eq '2'}">selected="selected"</c:if>>어문</option>
												<option value="3"
													<c:if test="${genreCode eq '3'}">selected="selected"</c:if>>방송대본</option>
												<option value="4"
													<c:if test="${genreCode eq '4'}">selected="selected"</c:if>>영화</option>
												<option value="5"
													<c:if test="${genreCode eq '5'}">selected="selected"</c:if>>방송</option>
												<option value="6"
													<c:if test="${genreCode eq '6'}">selected="selected"</c:if>>뉴스</option>
												<option value="7"
													<c:if test="${genreCode eq '7'}">selected="selected"</c:if>>미술</option>
												<option value="8"
													<c:if test="${genreCode eq '8'}">selected="selected"</c:if>>이미지</option>
												<option value="99"
													<c:if test="${genreCode eq '99'}">selected="selected"</c:if>>기타</option>
										</select></td>
										<td><a href="#" onclick="fncDown();return false;"><input type="button" value="다운로드" id="downBtn" name="downBtn"></a></td>
									</tr>
									<tr>
										<th class="text-center search-th">엑셀파일 업로드</th>
										<td colspan="3">
											<div>
											<c:if test="${fileName ne ''}"><a href="#">${fileName}</a></c:if>
											</div>
											<!-- 
											<div id="DivIdFile1" style="display: inline-block;">
												<input type="file" id="file1" name="file1" value=""
													size="50" style="float: left"
													onchange="javascript:file_change(this.value);"> <input
													type="button" id="file1del" value="파일삭제"
													onclick="fncFileMinus(1);">&nbsp;&nbsp;<font
													color="red">( * 60,000건 이하만 가능 )</font>
											</div>
											 -->
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<!-- /.row -->
				</div>
				<!-- /.box-body -->
			</div>
		</div>
	</div>

	<div style="overflow: scroll; height: 520px; width: 100%;">
		<div class="box" style="margin-top: 30px;">
			<!-- /.box-header -->
			<div class="box-body">
				<div class="fixed-table-body">
					<div style="display: inline-block;">
						<c:forEach var="info" items="${uploadFailList}" varStatus="listStatus">
							<c:set var="tot_cnt" value="${listStatus.count}" />
						</c:forEach>
						<a href="#" onclick="javascript:fnValidate(${tot_cnt});return false;"><input type="button" value="유효성확인" id="valChk" name="valChk"></a> 
						<a href="#" onclick="rowCheDel();return false;"><input type="button" value="선택삭제" id="deleteChk" name="deleteChk"></a>
						<a href="#" onclick="fncSave(${tot_cnt});return false;"><input type="button" value="일괄수정" id="insertAll" name="insertAll"></a>
					</div>
					<table id="fdcrAd21List1_1" class="table table-bordered" style="margin-top: 15px; display: none; width: 1500px; table-layout: fixed; overflow-x: scroll;">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">선택</th>
								<th class="text-center" style="width: 50px;">순번</th>
								<th class="text-center" style="width: 150px;">작성기관명</th>
								<th class="text-center" style="width: 100px;">기관내부<br />관리ID</th>
								<th class="text-center" style="width: 50px;">오류<br />건수</th>
								<th class="text-center" style="width: 300px;">곡명</th>
								<th class="text-center" style="width: 100px;">국내외구분</th>
								<th class="text-center" style="width: 300px;">앨범명</th>
								<th class="text-center" style="width: 300px;">앨범제작사</th>
								<th class="text-center" style="width: 100px;">디스크면</th>
								<th class="text-center" style="width: 100px;">트랙번호</th>
								<th class="text-center" style="width: 150px;">앨범발매년도</th>
								<th class="text-center" style="width: 150px;">작사가성명</th>
								<th class="text-center" style="width: 150px;">작곡가성명</th>
								<th class="text-center" style="width: 150px;">편곡가성명</th>
								<th class="text-center" style="width: 150px;">역사가성명</th>
								<th class="text-center" style="width: 150px;">가수성명</th>
								<th class="text-center" style="width: 150px;">연주자성명</th>
								<th class="text-center" style="width: 150px;">음반제작자성명</th>
								<th class="text-center" style="width: 150px;">위탁관리구분</th>
								<th class="text-center" style="width: 150px;">저작권자<br />권리처리구분</th>
								<th class="text-center" style="width: 150px;">실연자권리<br />처리구분</th>
								<th class="text-center" style="width: 150px;">음반제작자<br />권리처리구분</th>
								<th class="text-center" style="width:300px;">UCI</th>
								<th class="text-center" style="width:150px;">지분율</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="info" items="${uploadFailList}" varStatus="listStatus">
							<tr id="tr_${listStatus.count}">
								<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
								<td class="text-center" style="width: 50px;">${listStatus.count}</td>
									<c:forEach var="i" begin="0" end="21" step="1">
										<c:if test="${i == '2' }">
											<td class="text-center" style="width: 100px;"></td>
										</c:if>
										<c:if test="${i == '0' || i == '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="1td_${listStatus.count}_${i}"><input type="text" id="1cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
										<c:if test="${i != '0' && i != '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="1td_${listStatus.count}_${i}"><input type="text" id="1cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
									</c:forEach>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<table id="fdcrAd21List1_2" class="table table-bordered" style="margin-top: 15px; display: none; width: 1500px; table-layout: fixed; overflow-x: scroll;">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">선택</th>
								<th class="text-center" style="width: 50px;">순번</th>
								<th class="text-center" style="width: 150px;">작성기관명</th>
								<th class="text-center" style="width: 100px;">기관내부<br />관리ID</th>
								<th class="text-center" style="width: 50px;">오류<br />건수</th>
								<th class="text-center" style="width: 300px;">저작물명</th>
								<th class="text-center" style="width: 300px;">저작물부제</th>
								<th class="text-center" style="width: 150px;">창작년도</th>
								<th class="text-center" style="width: 300px;">도서명</th>
								<th class="text-center" style="width: 150px;">출판사</th>
								<th class="text-center" style="width: 100px;">발행년도</th>
								<th class="text-center" style="width: 150px;">저작자명</th>
								<th class="text-center" style="width: 150px;">위탁관리구분</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="info" items="${uploadFailList}" varStatus="listStatus">
							<tr id="tr_${listStatus.count}">
								<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
								<td class="text-center" style="width: 50px;">${listStatus.count}</td>
									<c:forEach var="i" begin="0" end="9" step="1">
										<c:if test="${i == '2' }">
											<td class="text-center" style="width: 100px;"></td>
										</c:if>
										<c:if test="${i == '0' || i == '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="2td_${listStatus.count}_${i}"><input type="text" id="2cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
										<c:if test="${i != '0' && i != '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="2td_${listStatus.count}_${i}"><input type="text" id="2cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
									</c:forEach>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<table id="fdcrAd21List1_3" class="table table-bordered" style="margin-top: 15px; display: none; width: 1500px; table-layout: fixed; overflow-x: scroll;">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">선택</th>
								<th class="text-center" style="width: 50px;">순번</th>
								<th class="text-center" style="width: 150px;">작성기관명</th>
								<th class="text-center" style="width: 150px;">기관내부<br />관리ID</th>
								<th class="text-center" style="width: 50px;">오류<br />건수</th>
								<th class="text-center" style="width: 300px;">작품명(대제목)</th>
								<th class="text-center" style="width: 300px;">원작명</th>
								<th class="text-center" style="width: 150px;">장르분류</th>
								<th class="text-center" style="width: 150px;">소재분류</th>
								<th class="text-center" style="width: 150px;">원작작가명</th>
								<th class="text-center" style="width: 150px;">주요출연진</th>
								<th class="text-center" style="width: 150px;">방송횟차</th>
								<th class="text-center" style="width: 150px;">방송일자</th>
								<th class="text-center" style="width: 150px;">제작사</th>
								<th class="text-center" style="width: 150px;">작가(저자)</th>
								<th class="text-center" style="width: 150px;">연출자명</th>
								<th class="text-center" style="width: 150px;">위탁관리구분</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="info" items="${uploadFailList}" varStatus="listStatus">
							<tr id="tr_${listStatus.count}">
								<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
								<td class="text-center" style="width: 50px;">${listStatus.count}</td>
									<c:forEach var="i" begin="0" end="13" step="1">
										<c:if test="${i == '2' }">
											<td class="text-center" style="width: 100px;"></td>
										</c:if>
										<c:if test="${i == '0' || i == '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="3td_${listStatus.count}_${i}"><input type="text" id="3cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
										<c:if test="${i != '0' && i != '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="3td_${listStatus.count}_${i}"><input type="text" id="3cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
									</c:forEach>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<table id="fdcrAd21List1_4" class="table table-bordered" style="margin-top: 15px; display: none; width: 1500px; table-layout: fixed; overflow-x: scroll;">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">선택</th>
								<th class="text-center" style="width: 50px;">순번</th>
								<th class="text-center" style="width: 150px;">작성기관명</th>
								<th class="text-center" style="width: 100px;">기관내부<br />관리ID</th>
								<th class="text-center" style="width: 50px;">오류<br />건수</th>
								<th class="text-center" style="width: 300px;">영화제목(국문)</th>
								<th class="text-center" style="width: 100px;">제작년도</th>
								<th class="text-center" style="width: 300px;">출연자명</th>
								<th class="text-center" style="width: 150px;">감독</th>
								<th class="text-center" style="width: 150px;">작가(저자)</th>
								<th class="text-center" style="width: 150px;">연출자</th>
								<th class="text-center" style="width: 150px;">제작자</th>
								<th class="text-center" style="width: 150px;">배급사</th>
								<th class="text-center" style="width: 150px;">투자사</th>
								<th class="text-center" style="width: 150px;">위탁관리구분</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="info" items="${uploadFailList}" varStatus="listStatus">
							<tr id="tr_${listStatus.count}">
								<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
								<td class="text-center" style="width: 50px;">${listStatus.count}</td>
									<c:forEach var="i" begin="0" end="11" step="1">
										<c:if test="${i == '2' }">
											<td class="text-center" style="width: 100px;"></td>
										</c:if>
										<c:if test="${i == '0' || i == '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="4td_${listStatus.count}_${i}"><input type="text" id="4cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
										<c:if test="${i != '0' && i != '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="4td_${listStatus.count}_${i}"><input type="text" id="4cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
									</c:forEach>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<table id="fdcrAd21List1_5" class="table table-bordered" style="margin-top: 15px; display: none; width: 1500px; table-layout: fixed; overflow-x: scroll;">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">선택</th>
								<th class="text-center" style="width: 50px;">순번</th>
								<th class="text-center" style="width: 150px;">작성기관명</th>
								<th class="text-center" style="width: 100px;">기관내부<br />관리ID</th>
								<th class="text-center" style="width: 50px;">오류<br />건수</th>
								<th class="text-center" style="width: 300px;">제목</th>
								<th class="text-center" style="width: 100px;">장르구분</th>
								<th class="text-center" style="width: 100px;">제작년도</th>
								<th class="text-center" style="width: 100px;">회차</th>
								<th class="text-center" style="width: 150px;">연출자명</th>
								<th class="text-center" style="width: 150px;">제작사명</th>
								<th class="text-center" style="width: 150px;">출연자명</th>
								<th class="text-center" style="width: 150px;">작가명</th>
								<th class="text-center" style="width: 150px;">위탁관리구분</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="info" items="${uploadFailList}" varStatus="listStatus">
							<tr id="tr_${listStatus.count}">
								<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
								<td class="text-center" style="width: 50px;">${listStatus.count}</td>
									<c:forEach var="i" begin="0" end="10" step="1">
										<c:if test="${i == '2' }">
											<td class="text-center" style="width: 100px;"></td>
										</c:if>
										<c:if test="${i == '0' || i == '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="5td_${listStatus.count}_${i}"><input type="text" id="5cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
										<c:if test="${i != '0' && i != '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="5td_${listStatus.count}_${i}"><input type="text" id="5cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
									</c:forEach>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<table id="fdcrAd21List1_6" class="table table-bordered" style="margin-top: 15px; display: none; width: 1500px; table-layout: fixed; overflow-x: scroll;">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">선택</th>
								<th class="text-center" style="width: 50px;">순번</th>
								<th class="text-center" style="width: 150px;">작성기관명</th>
								<th class="text-center" style="width: 300px;">기관내부<br />관리ID</th>
								<th class="text-center" style="width: 50px;">오류<br />건수</th>
								<th class="text-center" style="width: 500px;">기사제목</th>
								<th class="text-center" style="width: 100px;">지면번호</th>
								<th class="text-center" style="width: 100px;">지면면종</th>
								<th class="text-center" style="width: 150px;">기고자</th>
								<th class="text-center" style="width: 150px;">기자명</th>
								<th class="text-center" style="width: 100px;">변경버전</th>
								<th class="text-center" style="width: 150px;">기사일자</th>
								<th class="text-center" style="width: 150px;">기사발행일시</th>
								<th class="text-center" style="width: 150px;">언론사명</th>
								<th class="text-center" style="width: 150px;">위탁관리구분</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="info" items="${uploadFailList}" varStatus="listStatus">
							<tr id="tr_${listStatus.count}">
								<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
								<td class="text-center" style="width: 50px;">${listStatus.count}</td>
									<c:forEach var="i" begin="0" end="11" step="1">
										<c:if test="${i == '2' }">
											<td class="text-center" style="width: 100px;"></td>
										</c:if>
										<c:if test="${i == '0' || i == '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="6td_${listStatus.count}_${i}"><input type="text" id="6cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
										<c:if test="${i != '0' && i != '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="6td_${listStatus.count}_${i}"><input type="text" id="6cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
									</c:forEach>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<table id="fdcrAd21List1_7" class="table table-bordered" style="margin-top: 15px; display: none; width: 1500px; table-layout: fixed; overflow-x: scroll;">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">선택</th>
								<th class="text-center" style="width: 50px;">순번</th>
								<th class="text-center" style="width: 150px;">작성기관명</th>
								<th class="text-center" style="width: 200px;">기관내부<br />관리ID</th>
								<th class="text-center" style="width: 50px;">오류<br />건수</th>
								<th class="text-center" style="width: 300px;">제목</th>
								<th class="text-center" style="width: 100px;">부제목</th>
								<th class="text-center" style="width: 300px;">분류</th>
								<th class="text-center" style="width: 100px;">저작년월일</th>
								<th class="text-center" style="width: 150px;">출처</th>
								<th class="text-center" style="width: 100px;">크기</th>
								<th class="text-center" style="width: 150px;">해상도</th>
								<th class="text-center" style="width: 150px;">주재료</th>
								<th class="text-center" style="width: 150px;">구조및특징</th>
								<th class="text-center" style="width: 700px;">이미지URL</th>
								<th class="text-center" style="width: 300px;">상세페이지URL</th>
								<th class="text-center" style="width: 150px;">작가</th>
								<th class="text-center" style="width: 150px;">소장년월일</th>
								<th class="text-center" style="width: 150px;">소장기관명</th>
								<th class="text-center" style="width: 150px;">위탁관리구분</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="info" items="${uploadFailList}" varStatus="listStatus">
							<tr id="tr_${listStatus.count}">
								<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
								<td class="text-center" style="width: 50px;">${listStatus.count}</td>
									<c:forEach var="i" begin="0" end="16" step="1">
										<c:if test="${i == '2' }">
											<td class="text-center" style="width: 100px;"></td>
										</c:if>
										<c:if test="${i == '0' || i == '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="7td_${listStatus.count}_${i}"><input type="text" id="7cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
										<c:if test="${i != '0' && i != '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="7td_${listStatus.count}_${i}"><input type="text" id="7cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
									</c:forEach>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<table id="fdcrAd21List1_8" class="table table-bordered"
						style="margin-top: 15px; display: none; width: 1500px; table-layout: fixed; overflow-x: scroll;">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">선택</th>
								<th class="text-center" style="width: 50px;">순번</th>
								<th class="text-center" style="width: 150px;">작성기관명</th>
								<th class="text-center" style="width: 200px;">기관내부<br />관리ID</th>
								<th class="text-center" style="width: 50px;">오류<br />건수</th>
								<th class="text-center" style="width: 300px;">이미지명</th>
								<th class="text-center" style="width: 300px;">이미지설명</th>
								<th class="text-center" style="width: 150px;">이미지키워드</th>
								<th class="text-center" style="width: 150px;">창작년월일</th>
								<th class="text-center" style="width: 200px;">작가</th>
								<th class="text-center" style="width: 200px;">제작자</th>
								<th class="text-center" style="width: 300px;">이미지URL</th>
								<th class="text-center" style="width: 100px;">이미지공개여부</th>
								<th class="text-center" style="width: 300px;">ICN</th>
								<th class="text-center" style="width: 150px;">위탁관리구분</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="info" items="${uploadFailList}" varStatus="listStatus">
							<tr id="tr_${listStatus.count}">
								<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
								<td class="text-center" style="width: 50px;">${listStatus.count}</td>
									<c:forEach var="i" begin="0" end="11" step="1">
										<c:if test="${i == '2' }">
											<td class="text-center" style="width: 100px;"></td>
										</c:if>
										<c:if test="${i == '0' || i == '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="8td_${listStatus.count}_${i}"><input type="text" id="8cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
										<c:if test="${i != '0' && i != '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="8td_${listStatus.count}_${i}"><input type="text" id="8cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
									</c:forEach>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<table id="fdcrAd21List1_99" class="table table-bordered"
						style="margin-top: 15px; display: none; width: 1500px; table-layout: fixed; overflow-x: scroll;">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">선택</th>
								<th class="text-center" style="width: 50px;">순번</th>
								<th class="text-center" style="width: 150px;">작성기관명</th>
								<th class="text-center" style="width: 200px;">기관내부<br />관리ID</th>
								<th class="text-center" style="width: 50px;">오류<br />건수</th>
								<th class="text-center" style="width: 150px;">저작물종류</th>
								<th class="text-center" style="width: 300px;">저작물명 또는 키워드</th>
								<th class="text-center" style="width: 150px;">저작자명</th>
								<th class="text-center" style="width: 100px;">창작년도</th>
								<th class="text-center" style="width: 150px;">공표매체명</th>
								<th class="text-center" style="width: 150px;">공표일자</th>
								<th class="text-center" style="width: 150px;">위탁관리구분</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="info" items="${uploadFailList}" varStatus="listStatus">
							<tr id="tr_${listStatus.count}">
								<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
								<td class="text-center" style="width: 50px;">${listStatus.count}</td>
									<c:forEach var="i" begin="0" end="8" step="1">
										<c:if test="${i == '2' }">
											<td class="text-center" style="width: 100px;"></td>
										</c:if>
										<c:if test="${i == '0' || i == '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="99td_${listStatus.count}_${i}"><input type="text" id="99cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
										<c:if test="${i != '0' && i != '1' }">
											<c:set var="map1" value="0_${info.idx}_${i}"></c:set>
											<td class="text-center" style="width: 100px;" id="99td_${listStatus.count}_${i}"><input type="text" id="99cell_${listStatus.count}_${i}" value="${info[map1]}" style="border: 0px; width: 95%;"/></td>
										</c:if>
									</c:forEach>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
</form>