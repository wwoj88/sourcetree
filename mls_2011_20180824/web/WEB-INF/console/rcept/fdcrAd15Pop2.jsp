<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
	
	function reptMonthView(TRST_ORGN_CODE,YYYY,YYYYMM){
		var popUrl = '<c:url value="/console/rcept/fdcrAd15List4.page"/>?TRST_ORGN_CODE='
				+ TRST_ORGN_CODE+'&YYYY='+YYYY+'&YYYYMM='+YYYYMM;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=1200, height=800, resizable=no, scrollbars=yes, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function fncExcelDown1(REPT_YMD) {
		var url = '<c:url value="/console/rcept/fdcrAd15Down2.page"/>?REPT_YMD='+REPT_YMD;
		location.href = url;
	}
	
	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	$(function(){
		
		for(var i = 2 ; i <= 12 ; i++ ){
			$('#sum'+i+'Total').find('b').html(numberWithCommas($('#sum'+i+'Total').find('b').html()));
		}
	});
</script>
	<c:forEach var="info_reptYmd" items="${ds_list}" varStatus="listStatus">
		<c:set var="reptYmd" value="${info_reptYmd.RGST_DTTM }"/>
	</c:forEach>
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶${fn:substring(reptYmd,0,4) }년 ${fn:substring(reptYmd,5,7) }월 월별 위탁관리저작물 보고현황</b></h4>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" style="width: 16%">작성기관명</th>
						<th class="text-center" style="width: 7%">보고일자</th>
						<th class="text-center" style="width: 7%">음악</th>
						<th class="text-center" style="width: 7%">어문</th>
						<th class="text-center" style="width: 7%">방손대본</th>
						<th class="text-center" style="width: 7%">영화</th>
						<th class="text-center" style="width: 7%">방송</th>
						<th class="text-center" style="width: 7%">뉴스</th>
						<th class="text-center" style="width: 7%">미술</th>
						<th class="text-center" style="width: 7%">이미지</th>
						<th class="text-center" style="width: 7%">사진</th>
						<th class="text-center" style="width: 7%">기타</th>
						<th class="text-center" style="width: 7%">저작물<br/>없음</th>
						<th class="text-center" style="width: 7%">계</th>
					</tr>
				</thead>
				<tbody>
				<c:choose>
				<c:when test="${empty ds_list}">
				</c:when>
				<c:otherwise>
				<c:set var="sum1" value="0" />
				<c:set var="sum2" value="0" />
				<c:set var="sum3" value="0" />
				<c:set var="sum4" value="0" />
				<c:set var="sum5" value="0" />
				<c:set var="sum6" value="0" />
				<c:set var="sum7" value="0" />
				<c:set var="sum8" value="0" />
				<c:set var="sum12" value="0" />
				<c:set var="sum9" value="0" />
				<c:set var="sum10" value="0" />
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<c:set var="yyyy" value="${fn:substring(info.RGST_DTTM ,0,4) }" />
					<c:set var="yyyymm" value="${fn:substring(info.RGST_DTTM,0,4) }${fn:substring(info.RGST_DTTM ,5,7) }" />
					<c:set var="reptYmd" value="${fn:substring(info.RGST_DTTM ,0,4) }${fn:substring(info.RGST_DTTM ,5,7) }" />
					<tr>
						<td class="text-center"><a href="#" onclick="reptMonthView('<c:out value="${info.CUR_TRST_ORGN_CODE}"/>','${yyyy}','${yyyymm}');return false;"><font size="1pt"><c:out value="${info.CONAME }"/></font></a></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.RGST_DTTM }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_1 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_2 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_3 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_4 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_5 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_6 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_7 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_8 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_12 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_9 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_10 }"/></font></td>
						<td class="text-center"><font size="2pt"><c:out value="${info.CNT_TOT }"/></font></td>
						<c:set var="sum1" value="${ sum1 + info.CNT1_ }" />
						<c:set var="sum2" value="${ sum2 + info.CNT2_ }" />
						<c:set var="sum3" value="${ sum3 + info.CNT3_ }" />
						<c:set var="sum4" value="${ sum4 + info.CNT4_ }" />
						<c:set var="sum5" value="${ sum5 + info.CNT5_ }" />
						<c:set var="sum6" value="${ sum6 + info.CNT6_ }" />
						<c:set var="sum7" value="${ sum7 + info.CNT7_ }" />
						<c:set var="sum8" value="${ sum8 + info.CNT8_ }" />
						<c:set var="sum12" value="${ sum12 + info.CNT12_ }" />
						<c:set var="sum9" value="${ sum9 + info.CNT9_ }" />
						<c:set var="sum10" value="${ sum10 + info.CNT10_ }" />
						
					</tr>
					</c:forEach>
					<tr>
						<td id="sum1Total" class="text-center" colspan="2"><b>합계</b></td>
						<td id="sum2Total" class="text-center"><b>${sum1 }</b></td>
						<td id="sum3Total" class="text-center"><b>${sum2 }</b></td>
						<td id="sum4Total" class="text-center"><b>${sum3 }</b></td>
						<td id="sum5Total" class="text-center"><b>${sum4 }</b></td>
						<td id="sum6Total" class="text-center"><b>${sum5 }</b></td>
						<td id="sum7Total" class="text-center"><b>${sum6 }</b></td>
						<td id="sum8Total" class="text-center"><b>${sum7 }</b></td>
						<td id="sum9Total" class="text-center"><b>${sum8 }</b></td>
						<td id="sum9Total" class="text-center"><b>${sum12 }</b></td>
						<td id="sum10Total" class="text-center"><b>${sum9 }</b></td>
						<td id="sum11Total" class="text-center"><b>-</b></td>
						<td id="sum12Total" class="text-center"><b>${sum1+sum2+sum3+sum4+sum5+sum6+sum7+sum8+sum9+sum12}</b></td>
					</tr>
					<tr>
						<c:forEach var="info_0" items="${ds_list_0}" varStatus="listStatus">
							<c:set var="info_1" value="${info_0.CNT1 }"/>
							<c:set var="info_2" value="${info_0.CNT2 }"/>
							<c:set var="info_3" value="${info_0.CNT3 }"/>
							<c:set var="info_4" value="${info_0.CNT4 }"/>
							<c:set var="info_5" value="${info_0.CNT5 }"/>
							<c:set var="info_6" value="${info_0.CNT6 }"/>
							<c:set var="info_7" value="${info_0.CNT7 }"/>
							<c:set var="info_8" value="${info_0.CNT8 }"/>
							<c:set var="info_12" value="${info_0.CNT12 }"/>
							<c:set var="info_9" value="${info_0.CNT9 }"/>
							<c:set var="info_10" value="${info_0.CNT10 }"/>
						</c:forEach>
						<td class="text-center" colspan="2"><b>보고기관수</b></td>
						<td class="text-center"><b><c:out value="${info_1 }"/></b></td>
						<td class="text-center"><b><c:out value="${info_2 }"/></b></td>
						<td class="text-center"><b><c:out value="${info_3 }"/></b></td>
						<td class="text-center"><b><c:out value="${info_4 }"/></b></td>
						<td class="text-center"><b><c:out value="${info_5 }"/></b></td>
						<td class="text-center"><b><c:out value="${info_6 }"/></b></td>
						<td class="text-center"><b><c:out value="${info_7 }"/></b></td>
						<td class="text-center"><b><c:out value="${info_8 }"/></b></td>
						<td class="text-center"><b><c:out value="${info_12 }"/></b></td>
						<td class="text-center"><b><c:out value="${info_9 }"/></b></td>
						<td class="text-center"><b><c:out value="${info_10 }"/></b></td>
						<td class="text-center"></b></td>
					</tr>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
			<div style="float: left; display: inline-block; margin-top: 10px;"><font size="1pt">최초보고업체 *구분<br/>보고일자: 보고업체 월별 최종 보고일자<br/>단위 건, 보고저작물건수<br/>보고기관수: 기관수(신규기관수/기존기관수)</font></div>
			<div class="btn-group" style="float: right; margin-top: 20px;">
				<button type="submit" class="btn btn-primary" onclick="fncExcelDown1(${REPT_YMD});return false;" style="float: right; text-align: right; margin: 5px 0 0 0;">Excel Down</button>
			</div>
		</div>
