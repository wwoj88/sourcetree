<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	// 년도 검색
	function reptYyyyChng(selOptVal) {
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd19List1.page"/>')+'&REPT_YYYY='+selOptVal;
		location.href = url;
	}
	
	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	$(function(){
		var trLength = $('#fdcrAd06List1 > tbody tr').length;
		var tdLength = $('#fdcrAd06List1 > tbody > tr > td').length;

		for(var i = 0 ; i < trLength ; i++)
		{
			for(var j = 1 ; j < $('#fdcrAd06List1 > tbody > tr').eq(i).find('td').length ; j++){
				var bTagCheck = $('#fdcrAd06List1 > tbody tr').eq(i).find('td').eq(j).find('b').html();
				if(bTagCheck!= undefined){
					$('#fdcrAd06List1 > tbody tr').eq(i).find('td').eq(j).find('b').html(numberWithCommas($('#fdcrAd06List1 > tbody tr').eq(i).find('td').eq(j).find('b').html()));
				}else{
					$('#fdcrAd06List1 > tbody tr').eq(i).find('td').eq(j).html(numberWithCommas($('#fdcrAd06List1 > tbody tr').eq(i).find('td').eq(j).html()));
				} 
			}
		}
	});
	
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group" style="float: right;">
						<span><b>년도:</b></span>&nbsp;&nbsp;&nbsp;&nbsp;
						<span style="width:150px; float: right; display: inline-block;"> 
							<select class="form-control" id="REPT_YYYY" name="REPT_YYYY" onchange="reptYyyyChng(this.value);">
								<option selected="selected" value="" >--전체--</option>
								<option value="2019">2019년</option>
								<option value="2018">2018년</option>
								<option value="2017">2017년</option>
								<option value="2016">2016년</option>
								<option value="2015">2015년</option>
								<option value="2014">2014년</option>
								<option value="2013">2013년</option>
								<option value="2012">2012년</option>
							</select>
						</span>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd06List1" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 10%; background-color: teal;" rowspan="2"><font color="white">년도</font></th>
					<th class="text-center" style="width: 48%; background-color: teal;" colspan="2"><font color="white">방송음악</font></th>
					<th class="text-center" style="width: 14%; background-color: teal;" rowspan="2"><font color="white">도서관</font></th>
					<th class="text-center" style="width: 14%; background-color: teal;;" rowspan="2"><font color="white">교과용</font></th>
					<th class="text-center" style="width: 14%; background-color: teal;" rowspan="2"><font color="white">계</font></th>
				</tr>
				<tr>
					<th class="text-center" style="width: 24%; background-color: teal;"><font color="white">음산협</font></th>
					<th class="text-center" style="width: 24%; background-color: teal;"><font color="white">음실련</font></th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="6" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:set var="sum0" value="0" />
						<c:set var="sum1" value="0" />
						<c:set var="sum2" value="0" />
						<c:set var="sum3" value="0" />
						<c:set var="sum4" value="0" />
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.REPT_YYYY}"/></td>
								<td style="text-align: right;"><c:out value="${info.BRCT203_CONT}"/></td>
								<td style="text-align: right;"><c:out value="${info.BRCT202_CONT}"/></td>
								<td style="text-align: right;"><c:out value="${info.LIBR_CONT}"/></td>
								<td style="text-align: right;"><c:out value="${info.SUBJ_CONT}"/></td>
								<td style="text-align: right;"><c:out value="${info.TOT_CNT}"/></td>
								<c:set var="sum0" value="${ sum0 + info.BRCT203_CONT }" />
								<c:set var="sum1" value="${ sum1 + info.BRCT202_CONT }" />
								<c:set var="sum2" value="${ sum2 + info.LIBR_CONT }" />
								<c:set var="sum3" value="${ sum3 + info.SUBJ_CONT }" />
								<c:set var="sum4" value="${ sum4 + info.TOT_CNT }" />
							</tr>
						</c:forEach>
							<tr>
								<td class="text-center"><b>합계</b></td>
								<td style="text-align: right;"><b><c:out value="${sum0}"/></b></td>
								<td style="text-align: right;"><b><c:out value="${sum1}"/></b></td>
								<td style="text-align: right;"><b><c:out value="${sum2}"/></b></td>
								<td style="text-align: right;"><b><c:out value="${sum3}"/></b></td>
								<td style="text-align: right;"><b><c:out value="${sum4}"/></b></td>
							</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
</div>