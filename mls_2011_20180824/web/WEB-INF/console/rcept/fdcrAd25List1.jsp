<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//업로드 파일양식 다운로드
	function fncDown(){
		location.href = '<c:url value="/console/rcept/fdcrAd25Down1.page"/>';
	}
	
	function file_change(file){
		var f = document.formUpload;
		/* 
		var file1 = $('#file1').val();
		if(confirm("파일을 업로드 하시겠습니까?"))
		{
			f.action = '<c:url value="/console/rcept/fdcrAd25Upload1.page"/>?file1='
				+ file1;
			f.submit();
		}
		 */
		 if(confirm("파일을 업로드 하시겠습니까?")){
			f.action = '<c:url value="/console/rcept/fdcrAd25Upload1.page"/>';
			f.submit();
		}
	}
	
	function fncValChk(){
		alert(".xls,.xlsx 파일을 업로드 해주세요.");
		return false;
	}
	
	function fncDelete(){
		alert("삭제할 항목을 체크해 주세요.");
		return false;
	}
</script>
<style>
.fixed-table-body {
  overflow-x: auto;
}
</style>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<h4>&nbsp;&nbsp;● 도서관보상금 일괄수정</h4>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"> <label>엑셀양식</label></span>
						<span class="input-group-addon" style="width:150px;"><a href="#" onclick="fncDown();return false;"><input type="button" value="다운로드"></a></span>
						<span class="input-group-addon"> <label>엑셀파일 업로드</label></span>
						<span class="input-group-addon" style="width:150px;">
							<form name="formUpload" id="formUpload" method="post" enctype="multipart/form-data">
								<div id="DivIdFile1" style="display: inline-block;">
									<input type="file" id="file1" name="file1" value="" size="50" style="float:left" onchange="javascript:file_change(this.value);">
									<input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);">&nbsp;&nbsp;<font color="red">( * 50000건 이하만 가능 )</font>
								</div>
							</form>
						</span>
					</div>
				</div>
			<!-- /.row -->
			</div>
		<!-- /.box-body -->
		</div>
		<span><font color="red">&nbsp;&nbsp;* 미분배 보상금액의 분배내역과 보상금 발생내역의 시기가 일치하지 않으므로 데이터 업데이트 주기는 1년입니다.</font></span>
	</div>
</div>

<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body" style="overflow:auto;">
	<div class="fixed-table-body">
		<div style="display: inline-block;">
			<a href="#" onclick="fncValChk();return false;"><input type="button" value="유효성확인"></a>
			<a href="#" onclick="fncDelete();return false;"><input type="button" value="선택삭제"></a>
			<a href="#" onclick="fncUpdate();return false;"><input type="button" value="일괄등록"></a>
		</div>
		<table id="fdcrAd24List1" class="table table-bordered table-hover" style="margin-top: 15px;">
			<thead>
				<tr>
					<th class="text-center" style="width: 100px;">선택</th>
					<th class="text-center" style="width: 100px;">번호</th>
					<th class="text-center" style="width: 100px;">작성기관코드</th>
					<th class="text-center" style="width: 100px;">기관 내부관리ID</th>
					<th class="text-center" style="width: 100px;">오류건수</th>
					<th class="text-center" style="width: 100px;">저작물명</th>
					<th class="text-center" style="width: 100px;">저자성명</th>
					<th class="text-center" style="width: 100px;">이용도서관</th>
					<th class="text-center" style="width: 100px;">출판사</th>
					<th class="text-center" style="width: 100px;">발행연도</th>
					<th class="text-center" style="width: 100px;">분배공고년도</th>
					<th class="text-center" style="width: 100px;">분배여부</th>
					<th class="text-center" style="width: 100px;">분배공고일자</th>
					<th class="text-center" style="width: 100px;">보상금액</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
	<!-- /.box-body -->
</div>