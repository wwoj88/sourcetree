<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>

	//업로드 파일양식 다운로드
	function fncDown(){
		location.href = '<c:url value="/console/rcept/fdcrAd28Down1.page"/>';
	}
	
	function fnValidate(tot_cnt){
		
		$('#chkValYN').val('N'); // 유효성체크확인
		
		var chkValYN2 = $('#chkValYN').val();
		var curRowErrCount = 0;
		var isRowError = false;
		var errorRowStr = '';
		var allRowErrCount = 0;
		var f = document.chkForm;
		
		for(var a = 1; a <= tot_cnt; a++){
			var target_1 = document.getElementById("td_"+a+"_0").childNodes[0];
			var target_2 = document.getElementById("td_"+a+"_1").childNodes[0];
			var target_3 = document.getElementById("td_"+a+"_2").childNodes[0];
			var target_4 = document.getElementById("td_"+a+"_3").childNodes[0];
			var target_5 = document.getElementById("td_"+a+"_4").childNodes[0];
			var target_6 = document.getElementById("td_"+a+"_5").childNodes[0];
			var target_7 = document.getElementById("td_"+a+"_6").childNodes[0];
			var target_8 = document.getElementById("td_"+a+"_7").childNodes[0];
			var target_9 = document.getElementById("td_"+a+"_8").childNodes[0];
			var target_10 = document.getElementById("td_"+a+"_9").childNodes[0];
			
				//Validation Check
				if(target_1=='' || target_1 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_0");
				     t.style.backgroundColor = "#fc0"; 
				     isRowError = true;
					 curRowErrCount++;
				}
				 
				if(target_2=='' || target_2 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_1");
				     t.style.backgroundColor = "#fc0";
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_2.length > 50){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_1");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
				if(target_3=='' || target_3 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_2");
				     t.style.backgroundColor = "#fc0"; 
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_3.length > 333){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_2");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
				if(target_4=='' || target_4 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_3");
				     t.style.backgroundColor = "#fc0";
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_4.length > 333){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_3");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
				if(target_5.length > 333){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_4");
				     t.style.backgroundColor = "#fc0";
				     isRowError = true;
					 curRowErrCount++;
				}
				
				if(target_6=='' || target_6 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_5");
				     t.style.backgroundColor = "#fc0";
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_6.length > 333){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_5");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
				if(target_7=='' || target_7 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_6");
				     t.style.backgroundColor = "#fc0";
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_7.length > 33){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_6");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
				if(target_8=='' || target_8 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_7");
				     t.style.backgroundColor = "#fc0";
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_8.length > 4){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_7");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
				if(target_9=='' || target_9 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_8");
				     t.style.backgroundColor = "#fc0";
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_9.length > 33){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_8");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
				if(target_10=='' || target_10 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_9");
				     t.style.backgroundColor = "#fc0";
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_10.length > 50){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_9");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
		}
		alert("유효성 확인이 완료되었습니다.\n전체건수 : " + tot_cnt + "건(오류대상항목: " + curRowErrCount + "건)");
		$('#curRowErrCount').val(curRowErrCount);
		chkValYN2 = 'Y';
		$('#chkValYN').val('Y');
	} 
	
	function file_change(file){
		var f = document.formUpload;
		/* 
		var file1 = $('#file1').val();
		if(confirm("파일을 업로드 하시겠습니까?"))
		{
			f.action = '<c:url value="/console/rcept/fdcrAd28Upload1.page"/>?file1='
				+ file1;
			f.submit();
		}
		 */
		if(confirm("파일을 업로드 하시겠습니까?")){
			f.action = '<c:url value="/console/rcept/fdcrAd28Upload1.page"/>';
			f.submit();
		} 
	}
	
	//선택삭제
	function rowCheDel(){
	  var $obj = $("input[name='chk']");
	  var checkCount = $obj.size();
	  for (var i=0; i<checkCount; i++){
	   if($obj.eq(i).is(":checked")){
	   $obj.eq(i).parent().parent().remove();
	   }
	  }
	 }
	
	//일괄등록
	function fncSave(tot_cnt) {
		var f = document.chkForm;
 		var chkValYN2 = $('#chkValYN').val();
 		if(chkValYN2 == 'N' || chkValYN2 == null || chkValYN2 == ''){
 			alert("유효성 검사를 해주세요.")
 		}else if($('#curRowErrCount').val() != 0){
 			alert("오류대상 항목이 존재한 경우에는 등록하실 수 없습니다.");
 		}else{
	 		for(var a = 1; a <= tot_cnt; a++){
	 			var target_1 = document.getElementById("td_"+a+"_0").childNodes[0];
				var target_2 = document.getElementById("td_"+a+"_1").childNodes[0];
				var target_3 = document.getElementById("td_"+a+"_2").childNodes[0];
				var target_4 = document.getElementById("td_"+a+"_3").childNodes[0];
				var target_5 = document.getElementById("td_"+a+"_4").childNodes[0];
				var target_6 = document.getElementById("td_"+a+"_5").childNodes[0];
				var target_7 = document.getElementById("td_"+a+"_6").childNodes[0];
				var target_8 = document.getElementById("td_"+a+"_7").childNodes[0];
				var target_9 = document.getElementById("td_"+a+"_8").childNodes[0];
				var target_10 = document.getElementById("td_"+a+"_9").childNodes[0];
				
				var var1 = encodeURI(encodeURIComponent(target_1.nodeValue));
				var var2 = encodeURI(encodeURIComponent(target_2.nodeValue));
				var var3 = encodeURI(encodeURIComponent(target_3.nodeValue));
				var var4 = encodeURI(encodeURIComponent(target_4.nodeValue));
				var var5 = encodeURI(encodeURIComponent(target_5.nodeValue));
				var var6 = encodeURI(encodeURIComponent(target_6.nodeValue));
				var var7 = encodeURI(encodeURIComponent(target_7.nodeValue));
				var var8 = encodeURI(encodeURIComponent(target_8.nodeValue));
				var var9 = encodeURI(encodeURIComponent(target_9.nodeValue));
				var var10 = encodeURI(encodeURIComponent(target_10.nodeValue));
				
				f.action = '<c:url value="/console/rcept/fdcrAd28Insert1.page"/>?RGST_ORGN_CODE='
					+ var1+'&MGNT_INMT_SEQN='+var2+'&WORK_NAME='+var3+'&WRITER_NAME='
					+var4+'&USEX_SITE='+var5+'&PUBC_YEAR='+var6+'&WORK_KIND='+var7+'&ALLT_YSNO='
					+var8+'&ALLT_DATE='+var9+'&ALLT_AMNT='+var10;

				f.submit();
	 		}
 		}
 		
	}
	

</script>
<style>
.fixed-table-body {
  overflow-x: auto;
}
</style>
<div class="box box-default">
	<h4>&nbsp;&nbsp;수업목적보상금 일괄수정</h4>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"> <label>엑셀양식</label></span>
						<span class="input-group-addon" style="width:150px;"><a href="#" onclick="fncDown();return false;"><input type="button" value="다운로드"></a></span>
						<span class="input-group-addon"> <label>엑셀파일 업로드</label></span>
						<span class="input-group-addon" style="width:150px;">
							<form name="formUpload" id="formUpload" method="post" enctype="multipart/form-data">
								<div id="DivIdFile1" style="display: inline-block;">
									<input type="file" id="file1" name="file1" value="" size="50" style="float:left" onchange="javascript:file_change(this.value);">
									<input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);">&nbsp;&nbsp;<font color="red">( * 50000건 이하만 가능 )</font>
								</div>
						</form>
						</span>
					</div>
				</div>
			<!-- /.row -->
			</div>
		<!-- /.box-body -->
		</div>
		<span><font color="red">&nbsp;&nbsp;* 미분배 보상금액의 분배내역과 보상금 발생내역의 시기가 일치하지 않으므로 데이터 업데이트 주기는 1년입니다.</font></span>
	</div>
</div>

<form name="chkForm" id="chkForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
	<input type="hidden" id="chkValYN" name="chkValYN" value="" />
	<input type="hidden" id="curRowErrCount">
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
	<div class="fixed-table-body">
		<div style="display: inline-block;">
		<c:forEach var="info" items="${uploadList}" varStatus="listStatus">
		<c:set var="tot_cnt" value="${listStatus.count}"/>
		</c:forEach>
			<a href="#" onclick="javascript:fnValidate(${tot_cnt});return false;"><input type="button" value="유효성확인"></a>
			<a href="#" onclick="rowCheDel();return false;"><input type="button" value="선택삭제"></a>
			<a href="#" onclick="fncSave(${tot_cnt});return false;"><input type="button" value="일괄등록"></a>
		</div>
		<table id="fdcrAd28List1" class="table table-bordered" style="margin-top: 15px;">
			<thead>
				<tr>
					<th class="text-center" style="width: 50px;">선택</th>
					<th class="text-center" style="width: 50px;">순번</th>
					<th class="text-center" style="width: 50px;">작성기관코드</th>
					<th class="text-center" style="width: 100px;">기관 내부관리ID</th>
					<th class="text-center" style="width: 50px;">오류건수</th>
					<th class="text-center" style="width: 200px;">저작물명</th>
					<th class="text-center" style="width: 100px;">저작자범명</th>
					<th class="text-center" style="width: 100px;">출처</th> 
					<th class="text-center" style="width: 100px;">발행연도</th>
					<th class="text-center" style="width: 150px;">저작물종류</th>
					<th class="text-center" style="width: 100px;">분배여부</th>
					<th class="text-center" style="width: 100px;">분배공고일자</th>
					<th class="text-center" style="width: 100px;">보상금액</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="info" items="${uploadList}" varStatus="listStatus">
				<tr id="tr_${listStatus.count}">
					<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
					<td class="text-center" style="width: 50px;">${listStatus.count}</td>
					<c:forEach var="i" begin="0" end="10" step="1">
						<c:if test="${i == '2' }">
							<td class="text-center" style="width: 100px;"></td>
						</c:if>
						<c:if test="${i == '0' || i == '1' }">
						<c:set var="map1" value="0_${listStatus.index+3}_${i}"></c:set>
							<td class="text-center" id="td_${listStatus.count}_${i}" style="width: 100px;">${info[map1]}</td>
						</c:if>
						<c:if test="${i != '0' && i != '1' && i != '2' }">
						<c:set var="map1" value="0_${listStatus.index+3}_${i-1}"></c:set>
							<td class="text-center" id="td_${listStatus.count}_${i-1}" style="width: 100px;">${info[map1]}</td>
						</c:if>
					</c:forEach>
				
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
	</div>
	<!-- /.box-body -->
</div>
</form>