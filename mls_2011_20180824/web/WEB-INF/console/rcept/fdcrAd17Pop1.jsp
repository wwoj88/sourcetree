<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd17Pop1.page"/>',datas, page);
		location.href = url;
	}
	
	function schComaneList(orgCd){
		var SCH_CONAME = $('#SCH_CONAME').val();
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd17Pop1.page"/>')+'&SCH_CONAME='+SCH_CONAME+'&TRST_ORGN_DIVS_CODE='+orgCd;
		location.href = url;
	}
	
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
	
	function doClose(commNm){
		var commNm = commNm;
		opener.SetValue(commNm);
		window.close();
	}
</script>
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶ 기관/단체 찾기</b></h4>
			<table class="table table-bordered">
			<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
				<c:set var="orgCd" value="${info.ORGCD }"/>
			</c:forEach>
					<tr>
						<td class="text-center" style="width: 20%">기관/단체명/대표자</td>
						<td class="text-center" style="width: 60%"><input type="text" id="SCH_CONAME" name="SCH_CONAME"/></td>
						<td class="text-center" style="width: 20%"><input type="button" id="schComane" name="schComane" value="검색" style="float:right;" onclick="schComaneList('<c:out value="${orgCd}"/>');"></td>
					</tr>
			</table>				
			<table class="table table-bordered">
			<thead>
				<tr style="height: 30px;">
					<th class="text-center" style="width: 10%">순번</th>
					<th class="text-center">기관/단체명</th>
					<th class="text-center" style="width: 15%">대표자</th>
					<th class="text-center" style="width: 15%">신고번호</th>
					<th class="text-center" style="width: 20%">사업자등록번호</th>
				</tr>
			</thead>
			<tbody style="overflow-y: scroll;">
				<c:choose>
				<c:when test="${empty ds_list}">
					<tr>
						<td class="text-center" style="width: 100%" colspan="5">검색어를 입력해주세요.</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<tr style="height: 30px;">
						<td class="text-center"><c:out value="${info.ROWNUM }"/></td>
						<td class="text-center"><a href="#" onclick="doClose('<c:out value="${info.CONAME }"/>');return false;"><c:out value="${info.CONAME }"/></a></td>
						<td class="text-center"><c:out value="${info.CEONAME }"/></td>
						<td class="text-center"><c:out value="${info.APPNO }"/></td>
						<td class="text-center"><c:out value="${info.CONO }"/></td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix text-center">
		  <ul class="pagination pagination-sm no-margin">
		    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
		  </ul>
		</div>
