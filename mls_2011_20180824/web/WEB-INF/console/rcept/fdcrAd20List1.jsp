<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd20List1.page"/>',datas, page);
		location.href = url;
	}

	//검색
	function fncGoSearch() {
		var GUBUN = $('#GUBUN').val();
		var TITLE = $('#TITLE').val();
		
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd20List1.page"/>')+'&GUBUN='+GUBUN+'&TITLE='+TITLE;
		location.href = url;
	}
</script>
<style>
.fixed-table-body {
  overflow-x: auto;
}
</style>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" style="width:100px;"> <label>구분</label></span>
						<span class="input-group-addon" style="width:150px;"> 
							<select
							class="form-control" id="GUBUN" name="GUBUN">
								<option selected="selected" value="" >전체</option>
								<option value="10">곡명</option>
								<option value="20">가수성명</option>
								<option value="30">분배공고년도</option>
							</select>
						</span>
						<span class="input-group-addon" style="width:250px;"><input type="text" class="form-control" id="TITLE" name="TITLE"></span>
						<div class="btn-group" style="float: right; margin: 10px 20px 0 0;"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
		
		<!-- /.box-body -->
	</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
	<c:set var="totCnt" value="${ds_count}" />
	<span><b>&nbsp;&nbsp;총 <fmt:formatNumber value="${totCnt}" pattern="###,###,###,##0" groupingUsed="true" />건</b></span>
	<div class="fixed-table-body" style="margin-top: 5px;">
		<table id="fdcrAd20List1" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 100px;">순번</font></th>
					<th class="text-center" style="width: 100px;">작성기관코드</font></th>
					<th class="text-center" style="width: 150px;">기관내부관리ID</font></th>
					<th class="text-center" style="width: 300px;">곡명</th>
					<th class="text-center" style="width: 150px;">앨범명</font></th>
					<th class="text-center" style="width: 150px;">가수성명</font></th>
					<th class="text-center" style="width: 150px;">연주자성명</font></th>
					<th class="text-center" style="width: 150px;">음반제작자 성명</font></th>
					<th class="text-center" style="width: 100px;">분배공고년도</font></th>
					<th class="text-center" style="width: 100px;">분배여부</font></th>
					<th class="text-center" style="width: 100px;">분배공고일자</font></th>
					<th class="text-center" style="width: 100px;">보상금액</font></th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="11" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.ROW_NUM}"/></td>
								<td><c:out value="${info.RGST_ORGN_CODE}"/></td>
								<td><c:out value="${info.MGNT_INMT_SEQN}"/></td>
								<td><c:out value="${info.SDSR_NAME}"/></td>
								<td><c:out value="${info.ALBM_NAME}"/></td>
								<td><c:out value="${info.MUCI_NAME}"/></td>
								<td><c:out value="${info.PERF_NAME}"/></td>
								<td><c:out value="${info.ALBM_PROD}"/></td>
								<td><c:out value="${info.YYMM}"/></td>
								<td><c:out value="${info.ALLT_YSNO}"/></td>
								<td><c:out value="${info.ALLT_DATE}"/></td>
								<td>
								<c:set var="priceData" value="${info.ALLT_AMNT}" />
								<fmt:formatNumber value="${priceData}" pattern="###,###,###,##0" groupingUsed="true" />
								</td>
							</tr>
							
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	</div>
	<!-- /.box-body -->
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
</div>