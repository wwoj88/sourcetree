<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd16ImageList.page"/>',datas, page);
		var GUBUN = $("#submitGUBUN").val();
		//console.log(GUBUN)
		var TITLE = $('#submitTITLE').val();
		var selectType = $("#submitselectType").val();
		location.href = url+'&GUBUN='+GUBUN+'&TITLE='+TITLE+'&selectType='+selectType;
	}
		
	//검색
	function fncGoSearch() {
		$("#submitGUBUN").val($(":input:radio[name=GUBUN]:checked").val())
		$("#submitTITLE").val($('#TITLE').val())
		$("#submitselectType").val($("#selectType").val())
		var GUBUN = $("#submitGUBUN").val();
		var TITLE = $("#submitTITLE").val();
		var selectType = $("#submitselectType").val();
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd16ImageList.page"/>')+'&GUBUN='+GUBUN+'&TITLE='+TITLE+'&selectType='+selectType;
		location.href = url;
	}
	
	function fncExcelDown1() {
		var datas = [];
		datas[0] = 'schYYYYMM';
		datas[1] = 'GUBUN';
		datas[2] = 'TITLE';
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd16Down1.page"/>',datas, 1);
		var YYYYMM = $('#schYYYYMM').val();
		var GUBUN = $('#GUBUN').val();
		var TITLE = $('#TITLE').val();
		
		if(YYYYMM == ''){
			alert('보고년월을 선택해 주십시오.');	
			return false;
		}
		
		if(YYYYMM != '' || GUBUN != '' || TITLE != ''){
			TITLE = encodeURI(encodeURIComponent(TITLE));
			url += '&YYYYMM='+YYYYMM+'&GUBUN='+GUBUN+'&TITLE='+TITLE;
		}
		location.href = url;
	}
	
	$(function(){
		init();
	})
	
	function init(){
		documentSet();
	}
	
	function showContent(id){


			var url = $('#'+id).find('img').attr('src').substr(0,$('#'+id).find('img').attr('src').lastIndexOf("/")+1)+$('#'+id).find("input[id='imgName']").val();
			
			var title = $('#'+id).find('ul').find('li').eq(0).html().substr($('#'+id).find('ul').find('li').eq(0).html().indexOf(":")+1);
			var writer =$('#'+id).find('ul').find('li').eq(1).html().substr($('#'+id).find('ul').find('li').eq(1).html().indexOf(":")+1);
			var crt_year =$('#'+id).find('ul').find('li').eq(2).html().substr($('#'+id).find('ul').find('li').eq(2).html().indexOf(":")+1);
			var keyword =$('#'+id).find('ul').find('li').eq(3).html().substr($('#'+id).find('ul').find('li').eq(3).html().indexOf(":")+1);
			
			
			//var image_desc = $('#'+id).find('ul').find('li').eq(1).html().substr($('#'+id).find('ul').find('li').eq(1).html().indexOf(":")+1);
		/* 	$('#targetImg').load(function(){
				
				var containerSize = $('.inner').css('height').substr(0,$('.inner').css('height').lastIndexOf("p"));
				//console.log($('.inner').css('height').substr(0))
				//console.log($('.inner').css('height').lastIndexOf("p"))
				var titleSize = $('.tit').css('height').substr(0,$('.tit').css('height').lastIndexOf("p"));
				var imageSize= $('.img').css('height').substr(0,$('.img').css('height').lastIndexOf("p"));
				//console.log(containerSize)
				if(Number(containerSize)<(Number(titleSize)+Number(imageSize))){
					
					$('.inner').css('height',(Number(titleSize)+Number(imageSize)+30)+'px')
					console.log(Number(containerSize))
					console.log((Number(titleSize)+Number(imageSize)))
					console.log($('.inner').css('height'))
				}
			}) */
			$('#targetImg').attr('src',url);
			
			$('.subject').html(title)
			$('#keyworkd').html(keyword);
			$('#crt_year').html(crt_year);
			$('#writer').html(writer);
			
			$('.pop_thumb').show();
	}
	
	function documentSet(){
		var GUBUN = '<c:out value="${commandMap.GUBUN}"/>';
		var selectType = '<c:out value="${commandMap.selectType}"/>';

		if(GUBUN==10){
			$("#GUBUN1").prop("checked", true);	
		}else if(GUBUN==20){
			$("#GUBUN2").prop("checked", true);
		}else{
			$("#GUBUN1").prop("checked", true);
		}
		
		if(selectType=='works_title'){
			$("#selectType option:eq(1)").attr("selected", "selected");
		}else if(selectType=='writer'){
			$("#selectType option:eq(2)").attr("selected", "selected");
		}
		
		$(".pop_thumb .btn_close").click(function(){
			$('.pop_thumb').hide();
		});
	}
	


</script>
<style>
.fixed-table-body {
  overflow-x: auto;
}
</style>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>
<style>
<!--
.pop_thumb {
	display:none;
	position:fixed;
	width:100%;
	height:100%;
	left:0;
	top:0;
	z-index:99999;
	background:rgba(0,0,0,0.55);
}
.pop_thumb .inner {
	background:#fff;
	width:687px;
	height:581px;
	border:1px solid #000;
	margin:10% auto;
}
.pop_thumb .inner .tit {
	background:#4e4943;
	padding:0 18px;
	color:#fff;
	font-size:15px;
	line-height:45px;
	position:relative;
	height:45px;
}
.pop_thumb .inner .tit .btn_close {
	position:absolute;
	right:17px;
}
.pop_thumb .inner .cont {
	padding:23px 20px;
	overflow:hidden;
	display:inline; 
}
.pop_thumb .inner .cont .img {
	width:310px;
	float:left;
	
}
.pop_thumb .inner .cont .img img {
	width:100%;
	position:relative;
}
.pop_thumb .inner .cont .ment {
	margin-left:36px;
	position:relative;
	float:left;
	width:301px;
}
.pop_thumb .inner .cont .ment .subject {
	padding:8px 5px 15px;
	border-bottom:1px solid #e8e8e8;
	font-size:16px;
	font-weight:600;
}
.pop_thumb .inner .cont .ment li {
	background:url(https://www.findcopyright.or.kr/images/2012/common/dot2.gif) no-repeat 0 9px;
	padding:0px 0px 21px 10px;
	line-height:20px;
	color:#545454;
	font-size:14px;
}
.pop_thumb .inner .cont .ment ul {
	padding:22px 15px 0 9px;
}
-->
</style>
<form id="hiddenForm" name="hiddenForm">
	<input id="submitGUBUN" type="hidden" value="${commandMap.GUBUN }">
	<input id="submitTITLE" type="hidden" value="${commandMap.TITLE }">
	<input id="submitselectType" type="hidden" value="${commandMap.selectType }">
</form>
<!-- 썸네일 팝업 시작 -->
<div class="pop_thumb">
	<div class="inner">
		<h2 class="tit">이미지 정보<a href="javascript:;" class="btn_close"><img src="/images/2017/new/btn_close_pop.png" alt="팝업닫기"></a></h2>
		<div class="cont">
			<div class="img">
				<img id="targetImg" style="margin: 10px 10px 10px 10px;" src="/images/2017/new/img_pop.png" alt="">
			</div>
			<div class="ment">
				<p class="subject">
					제목부분
				</p>
				<ul>
					<li><strong>작가명 : </strong><spen id="writer"></spen></li>
					<li><strong>키워드 : </strong><spen id="keyworkd"></spen></li>
					<li><strong>창작년월일 : </strong><spen id="crt_year"></spen></li>
					<!-- <li><strong>권리 관리 기관명 : </strong>두영디지텍</li> -->
					<li><strong>이미지 설명 : </strong><spen id="image_desc"></spen></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- 썸네일 팝업 끝 -->
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">구분</th>
									<td>
										<input type="radio" id="GUBUN1" name="GUBUN" value="10">미술
										<input type="radio" id="GUBUN2" name="GUBUN" value="20">사진
									</td>
									<td>
										<select class="form-control" id="selectType" name="selectType">
											<option selected="selected" value="" >-전체-</option>
											<option value="works_title" >제목</option>
											<option value="writer" >작가명</option>
										</select>
									</td>
									<td>
										<input type="text" class="form-control" id="TITLE" name="TITLE" value="${commandMap.TITLE }">
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
		<!-- /.box-body -->
	</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
	<div class="fixed-table-body">
	<c:set var="totCnt" value="${ds_count}" />
		<span><b>&nbsp;&nbsp;총 <fmt:formatNumber value="${totCnt}" pattern="###,###,###,##0" groupingUsed="true" />건</b></span>
		<table id="fdcrAd20List1" class="table table-bordered" style="margin-top: 5px;">
		<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="9" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:set var="diskcount" value="0" />
						
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<c:set var='diskcount' value='${(diskcount+1)}'/>
							<c:if test="${diskcount / 5 ==0}"><tr></c:if>
							
								<td class="thumb" onclick="showContent('content${diskcount}')" id="content${diskcount}" style="width:291px ; height : 142px;">
									<table>
										<tr>
											<td >
												<img width="83" height="125" src="<c:out value="${info.IMAGE_URL}"/><c:out value="${info.IMAGE_THUMBNAIL_NAME}"/>"  onError="javascript:this.src='/images/noimg/noimg.jpg'" >
												<input type="hidden" id="imgName" value ="${info.IMAGE_NAME}">
												<input type="hidden" id="image_desc" value ="${info.IMAGE_DESC}">
											<td>
											<td>
												<ul>
												<li>제목 : <c:out value="${info.WORKS_TITLE}"/></li>
												<li>작가명 : <c:out value="${info.WRITER}"/></li>
												<li>창작년도 : <c:out value="${info.CRT_YEAR}"/></li>
												<li>이미지 키워드 : <c:out value="${info.KEYWORD}"/></li>
												</ul>
											</td>
										<tr>
									
									</table>
								</td>
							<c:if test="${diskcount  % 5 ==0}"></tr></c:if>
						
						</c:forEach>
					</c:otherwise>
				</c:choose>
		
		</table>
	</div>
	</div>
	<!-- /.box-body -->
	<div class="box-footer clearfix text-center">
	  <ul>
	   <!--  <button id="deleteBtn" type="button" class="btn btn-primary" style="float: right; text-align: right; margin: 5px 0 0 0;">위탁 저작물 삭제</button> -->
	  	<!-- <button type="submit" class="btn btn-primary" onclick="fncExcelDown1();return false;" style="float: right; text-align: right; margin: 5px 0 0 0;">Excel Down</button> -->
	  </ul>
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
</div>