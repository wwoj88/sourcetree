<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶ 업체별 보고현황</b></h4>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" style="width: 20%">작성기관명</th>
						<th class="text-center" style="width: 20%">계</th>
					</tr>
				</thead>
				<tbody>
				<c:choose>
				<c:when test="${empty ds_list}">
				</c:when>
				<c:otherwise>
				<c:set var = "sum" value = "0" />
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<tr>
						<td class="text-center" style="width: 80%"><c:out value="${info.CONAME }"/></td>
						<td class="text-center" style="width: 80%" id=""><c:out value="${info.CNT }"/></td>
						<c:set var= "sum" value="${sum + info.CNT}"/>
						<%-- <td><c:out value="${sum_cnt}"/></td> --%>
					</tr>
					</c:forEach>
					<tr>
						<th class="text-center" style="width: 20%">합계</th>
						<th class="text-center" style="width: 20%"><c:out value="${sum}"/></th>
					</tr>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
			<!-- 
			<div class="btn-group" style="float: right; margin-top: 20px;">
				<button type="submit" class="btn btn-primary" style="float: right;"
					onclick="fncExcelDown();">Excel Down</button>
			</div>
			 -->
		</div>
