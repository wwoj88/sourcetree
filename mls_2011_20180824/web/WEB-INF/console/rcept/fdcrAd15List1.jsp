<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>

$(function(){
	
	init();
})
	
function init(){
	selectBoxSet();
	$('#srchBtn').click(srchBtnClick);
}	

function srchBtnClick(event){
	console.log('srchBtnClick call');
	goSrch();
}


function selectBoxSet(){
	 var reptYYYY = '<c:out value="${REPT_YYYY}"/>';
	 var REPT_firstMM = '<c:out value="${REPT_firstMM}"/>';
	 var REPT_lastMM = '<c:out value="${REPT_lastMM}"/>';
	if(reptYYYY!=""){
		 $('#YYYY > option[value="'+reptYYYY+'"]').attr('selected','true');
	}
	
	if(REPT_firstMM!=""){
		 $('#firstMM > option[value="'+REPT_firstMM+'"]').attr('selected','true');
	}
	
	if(REPT_lastMM!=""){
		 $('#lastMM > option[value="'+REPT_lastMM+'"]').attr('selected','true');
	}
}


	// 년도 검색
	/* function yyyyChng(selOptVal) {
		//console.log(selOptVal);
		
		var YYYY = $('#YYYY').val();
		var firstMM = $('#firstMM').val();
		var lastMM = $('#lastMM').val();
		console.log(YYYY);
		console.log(firstMM);
		console.log(lastMM);
		
		if(firstMM>lastMM){
			alert("시작월이 더 큽니다.");
			return;
		}
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd15List1.page"/>')+'&YYYY='+selOptVal;
		
		
		
		//location.href = url;
	} */
	
	// 년도 검색
	function goSrch() {
		var YYYY = $('#YYYY').val();
		var firstMM = $('#firstMM').val();
		var lastMM = $('#lastMM').val();
		console.log(YYYY);
		console.log(firstMM);
		console.log(lastMM);
		
		if(firstMM>lastMM){
			alert("시작월이 더 큽니다.");
			return;
		}
		
		
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd15List1.page"/>')+'&YYYY='+YYYY;
		url += "&firstMM="+firstMM+"&lastMM="+lastMM;
		
		
		location.href = url;
	}
	
	function fncGoSave(){
		var f = document.writeForm;
		f.action = '<c:url value="/console/notdstbmanage/fdcrAd74Insert1.page"/>';
		f.submit();
	}
	
	function reptGenreView(GENRE_CD,YYYY){
		/* 
		alert("GENRE_CD::::::"+GENRE_CD);
		alert("YYYY::::::"+YYYY);
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd15List2.page"/>')+'&GENRE_CD='+GENRE_CD+'&YYYY='+YYYY;
		location.href = url;
		 */
		var popUrl = '<c:url value="/console/rcept/fdcrAd15List2.page"/>?GENRE_CD='
				+ GENRE_CD +'&YYYY='+YYYY;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=450, height=700, resizable=no, scrollbars=yes, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function reptYearView(REPT_YMD){
		var popUrl = '<c:url value="/console/rcept/fdcrAd15List3.page"/>?REPT_YMD='
				+ REPT_YMD;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=1200, height=500, resizable=no, scrollbars=yes, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function fncExcelDown1() {
		var datas = [];
		datas[0] = 'YYYY';
		var url = fncGetBoardParam('<c:url value="/console/rcept/fdcrAd15Down1.page"/>',datas, 1);
		var YYYY = $('#YYYY').val();
		if(YYYY != ''){
			url += '&YYYY='+YYYY;
		}
		location.href = url;
	}
	
</script>
<form name="writeForm" id="writeForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />

<div class="box box-default">
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group" style="width:100%;">
						<span style="float: right; display: inline-block;">
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
						<c:set var="reptYmd" value="${info.REPT_YMD }"></c:set>
						<c:set var="schReptYear" value="${fn:substring(reptYmd,0,4) }"></c:set>
						<c:set var="reptYYYY" value="${REPT_YYYY }"></c:set>
						</c:forEach>
						
							<span ><b>년도: </b></span>
							<select id="YYYY" name="YYYY">
								<option value="" selected="selected">--전체--</option>
								<option value="2020">2020년</option>
								<option value="2019">2019년</option>
								<option value="2018">2018년</option>
								<option value="2017">2017년</option>
								<option value="2016">2016년</option>
								<option value="2015">2015년</option>
								<option value="2014">2014년</option>
								<option value="2013">2013년</option>
								<option value="2012">2012년</option>
							</select>
							
							<span><b>시작월 : </b></span>
							<select id="firstMM" name="firstMM">
								<option value="" selected="selected">--전체--</option>
								<option value="01">1월</option>
								<option value="02">2월</option>
								<option value="03">3월</option>
								<option value="04">4월</option>
								<option value="05">5월</option>
								<option value="06">6월</option>
								<option value="07">7월</option>
								<option value="08">8월</option>
								<option value="09">9월</option>
								<option value="10">10월</option>
								<option value="11">11월</option>
								<option value="12">12월</option>
							</select>
							
							<span><b>마지막월 : </b></span>
							<select id="lastMM" name="lastMM">
								<option value="" selected="selected">--전체--</option>
								<option value="01">1월</option>
								<option value="02">2월</option>
								<option value="03">3월</option>
								<option value="04">4월</option>
								<option value="05">5월</option>
								<option value="06">6월</option>
								<option value="07">7월</option>
								<option value="08">8월</option>
								<option value="09">9월</option>
								<option value="10">10월</option>
								<option value="11">11월</option>
								<option value="12">12월</option>
							</select>
							
							<span><input id="srchBtn" type="button" value="검색"></span>
						</span>
					
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd74List1" class="table table-bordered text-center">
			<thead>
				<tr>
					<th style="width: 8%;">년월</th>
					<th style="width: 8%;">음악</th>
					<th style="width: 8%;">어문</th>
					<th style="width: 8%;">방송대본</th>
					<th style="width: 8%;;">영화</th>
					<th style="width: 8%;">방송</th>
					<th style="width: 8%;">뉴스</th>
					<th style="width: 8%;">미술</th>
					<th style="width: 8%;">이미지</th>
					<th style="width: 8%;">사진</th>
					<th style="width: 8%;">기타</th>
					<th style="width: 8%;">저작물 없음</th>
					<th style="width: 12%;">계</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="12" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
					<c:set var="sum1" value="0" />
					<c:set var="sum2" value="0" />
					<c:set var="sum3" value="0" />
					<c:set var="sum4" value="0" />
					<c:set var="sum5" value="0" />
					<c:set var="sum6" value="0" />
					<c:set var="sum7" value="0" />
					<c:set var="sum8" value="0" />
					<c:set var="sum9" value="0" />
					<c:set var="sum10" value="0" />
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
						<c:set var="reptYmd2" value="${info.REPT_YMD }"></c:set>
							<tr>
								<td class="text-center"><a href="#" onclick="reptYearView('<c:out value="${reptYmd2}"/>');return false;"><b><u><c:out value="${info.REPT_YMD }"/></u></b></a></td>
								<td class="text-center"><c:out value="${info.CNT_1 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_2 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_3 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_4 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_5 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_6 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_7 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_8 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_12 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_9 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_10 }"/></td>
								<td class="text-center"><c:out value="${info.CNT_TOT }"/></td>
								<c:set var="sum1" value="${ sum1 + info.CNT1_ }" />
								<c:set var="sum2" value="${ sum2 + info.CNT2_ }" />
								<c:set var="sum3" value="${ sum3 + info.CNT3_ }" />
								<c:set var="sum4" value="${ sum4 + info.CNT4_ }" />
								<c:set var="sum5" value="${ sum5 + info.CNT5_ }" />
								<c:set var="sum6" value="${ sum6 + info.CNT6_ }" />
								<c:set var="sum7" value="${ sum7 + info.CNT7_ }" />
								<c:set var="sum8" value="${ sum8 + info.CNT8_ }" />
								<c:set var="sum12" value="${ sum12 + info.CNT12_ }" />
								<c:set var="sum9" value="${ sum9 + info.CNT9_ }" />
								<c:set var="sum10" value="${ sum10 + info.CNT10_ }" />
							</tr>
						</c:forEach>
					<tr>
						<td class="text-center"><b>합계</b></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('1','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum1}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('2','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum2}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('3','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum3}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('4','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum4}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('5','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum5}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('6','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum6}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('7','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum7}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('8','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum8}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('9','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum12}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('99','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum9}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('0','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum10}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('','<c:out value="${reptYYYY}"/>');return false;"><b><u><fmt:formatNumber value="${sum1+sum2+sum3+sum4+sum5+sum6+sum7+sum8+sum9+sum10+sum12}" pattern="###,###,###,##0" groupingUsed="true" /></u></b></a></td>
					</tr>
					</c:otherwise>
				</c:choose>
			<c:choose>
			<c:when test="${empty ds_list}">
			</c:when>
			<c:otherwise>
				<c:forEach var="info_0" items="${ds_list_0}" varStatus="listStatus">
					<tr>
						<td class="text-center"><b>보고기관수</b></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('1','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT1 }"/></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('2','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT2 }"/></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('3','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT3 }"/></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('4','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT4 }"/></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('5','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT5 }"/></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('6','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT6 }"/></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('7','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT7 }"/></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('8','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT8 }"/></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('9','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT12 }"/></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('99','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT9 }"/></u></b></a></td>
						<td class="text-center"><a href="#" onclick="reptGenreView('0','<c:out value="${reptYYYY}"/>');return false;"><b><u><c:out value="${info_0.CNT10 }"/></u></b></a></td>
						<td class="text-center"><b><u></td>
					</tr>
				</c:forEach>
			</c:otherwise>
			</c:choose>
			</tbody>
		</table>
		
		<span style="color: black; margin:10px 0 0 10px; display: inline-block;">(* 단위 건, 보고저작물건수/수정저작물건수)</span>
		<button type="submit" class="btn btn-primary" onclick="fncExcelDown1();return false;" style="float: right; text-align: right; margin: 5px 0 0 0;">Excel Down</button>
	</div>
	
	<!-- /.box-body -->
</div>
</form>