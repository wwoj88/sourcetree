<%@ page language="java" contentType="application/vnd.ms-excel;charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%
	String filename = "위탁관리 저작물 목록.xls";
    
	response.setHeader( "Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "utf-8") + "" );
	response.setHeader( "Content-Description", "JSP Generated Data" );

	//response.setContentType( "application/vnd.ms-excel" );   

	//DataSet divList = (DataSet) box.getObject("EmasDivList");
	//HashMap ageMap = (HashMap) box.getObject("LicenseAgeClass");
%>
	<table border="1" cellpadding="0" cellspacing="0" style="table-layout:fixed;text-align:center" >
		<thead>
			<tr>
				<th style="background-color:#eeeeee;width: 7%">보고년월</th>
				<th style="background-color:#eeeeee;width: 10%">저작물관리번호</th>
				<th style="background-color:#eeeeee;width: 7%">국내외구분</th>
				<th style="background-color:#eeeeee;width: 7%">저작물분류</th>
				<th style="background-color:#eeeeee;width: 21%">저작물명</th>
				<th style="background-color:#eeeeee;width: 13%">저작물부제</th>
				<th style="background-color:#eeeeee;width: 13%">창작년도(발행연월일)</th>
				<th style="background-color:#eeeeee;width: 7%">위탁관리구분</th>
				<th style="background-color:#eeeeee;width: 15%">작성기관명</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="9" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td style='mso-number-format:"\@";'><c:out value="${info.REPT_YMD}"/></td>
								<td style='mso-number-format:"\@";'><c:out value="${info.COMM_WORKS_ID}"/></td>
								<td style='mso-number-format:"\@";'>
								<c:if test="${info.NATION_CD == '1' }">국내</c:if>
								<c:if test="${info.NATION_CD == '2' }">국외</c:if>
								</td style='mso-number-format:"\@";'>
								<td style='mso-number-format:"\@";'>
								<c:if test="${info.GENRE_CD == '1' }">음악</c:if>
								<c:if test="${info.GENRE_CD == '2' }">어문</c:if>
								<c:if test="${info.GENRE_CD == '3' }">방송대본</c:if>
								<c:if test="${info.GENRE_CD == '4' }">영화</c:if>
								<c:if test="${info.GENRE_CD == '5' }">방송</c:if>
								<c:if test="${info.GENRE_CD == '6' }">뉴스</c:if>
								<c:if test="${info.GENRE_CD == '7' }">미술</c:if>
								<c:if test="${info.GENRE_CD == '8' }">이미지</c:if>
								<c:if test="${info.GENRE_CD == '99' }">기타</c:if>
								</td>
								<td style='mso-number-format:"\@";'><c:out value="${info.WORKS_TITLE}"/></td>
								<td style='mso-number-format:"\@";'><c:out value="${info.WORKS_SUB_TITLE}"/></td>
								<td style='mso-number-format:"\@";'><c:out value="${info.CRT_YEAR}"/></td>
								<td style='mso-number-format:"\@";'>
								<c:if test="${info.COMM_MGNT_CD == '0' }">없음</c:if>
								<c:if test="${info.COMM_MGNT_CD != '0' }"><c:out value="${info.COMM_MGNT_CD}"/></c:if>
								</td>
								<td style='mso-number-format:"\@";'><c:out value="${info.RGST_ORGN_NAME}"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
		</tbody>
	</table>
