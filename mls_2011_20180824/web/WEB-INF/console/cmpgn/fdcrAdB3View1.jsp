<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
//첨부파일 다운로드
function fncDown(CAMP_PART_SEQN){
	location.href = '<c:url value="/console/cmpgn/fdcrAdB3Down2.page"/>?CAMP_PART_SEQN='+CAMP_PART_SEQN;
}

function fncGoList(){
	location.href = '<c:url value="/console/cmpgn/fdcrAdB3List1.page"/>';
}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${info.MENU_SEQN}"/>" /> 
	<input type="hidden" id="BORD_SEQN" name="BORD_SEQN" value="<c:out value="${info.BORD_SEQN}"/>" />
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
</form>

<div class="box" style="margin-top: 30px;">
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoList();return false;">목록</button></div>
	</div>
	<div class="box-header">
		<h3 class="box-title">참여자 정보</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th style="width: 20%">번호</th>
					<td style="width: 30%"><c:out value="${info.CAMP_PART_SEQN}"/></td>
					<th style="width: 20%">참여일자</th>
					<td style="width: 30%"><c:out value="${info.RGST_DTTM}"/></td>
				</tr>
				<tr>
					<th>성명</th>
					<td><c:out value="${info.CAMP_PART_NAME}"/></td>
					<th>연락처</th>
					<td><c:out value="${info.CAMP_PART_TELX}"/></td>
				</tr>
				<tr>
					<th>주소</th>
					<td colspan="3"><c:out value="${info.CAMP_PART_ADDR_VIEW}"/></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="box" style="margin-top: 30px;">
	<div class="box-header">
		<h3 class="box-title">설문 참여 정보</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th style="width: 20%">설문1</th>
					<td style="width: 30%"><c:out value="${info.ITEM_RSLT_01}"/></td>
					<th style="width: 20%">설문7</th>
					<td style="width: 30%"><c:out value="${info.ITEM_RSLT_07}"/></td>
				</tr>
				<tr>
					<th>설문2</th>
					<td><c:out value="${info.ITEM_RSLT_02}"/></td>
					<th>설문8-1</th>
					<td><c:out value="${info.ITEM_RSLT_08_1}"/></td>
				</tr>
				<tr>
					<th>설문3</th>
					<td><c:out value="${info.ITEM_RSLT_03}"/></td>
					<th>설문8-2</th>
					<td><c:out value="${info.ITEM_RSLT_08_2}"/></td>
				</tr>
				<tr>
					<th>설문4</th>
					<td><c:out value="${info.ITEM_RSLT_04}"/></td>
					<th>설문8-3</th>
					<td><c:out value="${info.ITEM_RSLT_08_3}"/></td>
				</tr>
				<tr>
					<th>설문5</th>
					<td><c:out value="${info.ITEM_RSLT_05}"/></td>
					<th>설문8-4</th>
					<td><c:out value="${info.ITEM_RSLT_08_4}"/></td>
				</tr>
				<tr>
					<th>설문6</th>
					<td colspan="3"><c:out value="${info.ITEM_RSLT_06}"/></td>
				</tr>
				<tr>
					<th>설문9</th>
					<td colspan="3"><c:out value="${info.ITEM_RSLT_09}"/></td>
				</tr>
				<tr>
					<th>설문10</th>
					<td colspan="3"><c:out value="${info.ITEM_RSLT_10}"/></td>
				</tr>
				<c:if test="${not empty info.CAMP_FILE_NAME}">
				<tr>
					<th>첨부파일</th>
					<td colspan="3"><a href="javascript:fncDown('<c:out value="${info.CAMP_PART_SEQN}"/>');"><c:out value="${info.CAMP_FILE_NAME}"/></a></td>
				</tr>
				</c:if>
			</tbody>
		</table>
	</div>
</div>