<%@ page language="java" contentType="application/vnd.ms-excel;charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%
	String TO = (String)request.getAttribute("TO");
	if(null==TO){
		TO = "";
	}
	response.setCharacterEncoding("EUC-KR");
	String filename = new String( ("설문참여현황("+TO+").xls").getBytes( "EUC-KR" ), "latin1" );
	response.setContentType( "application/vnd.ms-excel" );   
	response.setHeader( "Content-Disposition", "attachment; filename=" + filename+";");
	response.setHeader( "Content-Description", "JSP Generated Data" );
	response.setHeader( "Content-Transfer-Encoding", "binary;" );
	response.setHeader( "Pragma", "no-cache;" );
	response.setHeader( "Expires", "-1;" );	
%>
<html>
<head>
<meta http-equiv=Content-Type content='text/html; charset=euc-kr'>
</head>
<body>	
		<table style="table-layout:fixed" border="1">
			<thead>
				<tr>
					<th class="text-center" style="bgcolor:gray;width:80px">번호</th>
					<th class="text-center" style="bgcolor:gray;width:120px">참여일자</th>
					<th class="text-center" style="bgcolor:gray;width:100px">성명</th>
					<th class="text-center" style="bgcolor:gray;width:150px">연락처</th>
					<th class="text-center" style="bgcolor:gray;width:300px">주소</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문1</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문2</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문3</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문4</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문5</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문6</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문7</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문8-1</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문8-2</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문8-3</th>
					<th class="text-center" style="bgcolor:gray;width:100px">설문8-4</th>
					<th class="text-center" style="bgcolor:gray;width:1000px">설문9</th>
					<th class="text-center" style="bgcolor:gray;width:1000px">설문10</th>
					<th class="text-center" style="bgcolor:gray;width:100px">첨부파일</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_List}">
						<tr>
							<td colspan="18">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_List}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.CAMP_PART_SEQN}" /></td>
								<td class="text-center"><c:out value="${info.RGST_DTTM}" /></td>
								<td class="text-center"><c:out value="${info.CAMP_PART_NAME}" /></td>
								<td class="text-center"><c:out value="${info.CAMP_PART_TELX}" /></td>
								<td class="text-center"><c:out value="${info.CAMP_PART_ADDR_VIEW}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_01}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_02}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_03}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_04}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_05}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_06}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_07}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_08_1}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_08_2}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_08_3}" /></td>
								<td class="text-center"><c:out value="${info.ITEM_RSLT_08_4}" /></td>
								<td><c:out value="${info.ITEM_RSLT_09}" /></td>
								<td><c:out value="${info.ITEM_RSLT_10}" /></td>
								<td class="text-center"><c:out value="${info.CAMP_FILE_NAME}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</body>
</html>