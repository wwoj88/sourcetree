<%@ page language="java" contentType="text/html; charset=EUC-KR"
  pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
  //其捞瘤 捞悼
  function fncGoPage(page) {
    var datas = [];

    var url = '<c:url value="/console/mber/log.page"/>?A=A='+'&pageIndex='+page;
    
    location.href = url;
  }
  function fncGoView(USER_IDNT) {
    var USER_IDNT=USER_IDNT.toUpperCase();
    location.href = '<c:url value="/console/mber/fdcrAd84List1.page"/>?USER_IDNT='+ USER_IDNT;
  }
</script>


<div class="box box-default"></div>
<div class="box" style="margin-top: 30px;">
  <!-- /.box-header -->
  <div class="box-body">
    <table id="fdcrAd75List1"
      class="table table-bordered table-hover text-center table-list ">
      <thead>
        <tr>
          <th style="width: 6%">鉴锅</th>
          <th style="width: 16%">酒捞叼</th>
          <th style="width: 16%">贸府郴开</th>
          <th style="width: 16%">立辟/贸府 ID</th>
          <th style="width: 16%">贸府老矫</th>
          <th style="width: 16%">立加 IP</th>

        </tr>
      </thead>
      <tbody>
        <c:choose>
          <c:when test="${empty ds_list}">
            <tr>
              <td colspan="6"></td>
            </tr>
          </c:when>
          <c:otherwise>
            <c:forEach var="info" items="${ds_list}" varStatus="listStatus">
              <tr>
                <td><c:out value="${paginationInfo.totalRecordCount-(listStatus.count)-((paginationInfo.currentPageNo-1)*paginationInfo.recordCountPerPage)+1}" /></td>
                <td><a href="#" onclick="fncGoView('<c:out value="${info.MANAGER_ID}" />');return false;"><c:out value="${info.MANAGER_ID}" /></a></td>
                <td><c:out value="${info.PROC_STATUS}" /></td>
                <td><c:out value="${info.PROC_ID}" /></td>
                <td><c:out value="${info.PROC_DATE}" /></td>
                <td><c:out value="${info.IP_ADDRESS}" /></td>

              </tr>
            </c:forEach>
          </c:otherwise>
        </c:choose>
      </tbody>
    </table>
  </div>
  <div class="box-footer clearfix text-center">
    <ul class="pagination pagination-sm no-margin">
      <ui:pagination paginationInfo="${paginationInfo}" type="image"
        jsFunction="fncGoPage" />
    </ul>
  </div>
</div>