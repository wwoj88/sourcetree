<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

<%
//System.out.println( "CONSOLE_USER :::::" + request.getSession().getAttribute("CONSOLE_USER") );
//Map CONSOLE_USER = (Map) request.getSession().getAttribute("CONSOLE_USER");
//String CONSOLE_USER_ID = (String) CONSOLE_USER.get( "USER_ID" );
%>

<script>
  var gubun = '<c:out value="${param.GUBUN}"/>';
  $(function() {
    fncSet();
    if (gubun == 'edit') {
      fncTreeInit();
      $('#DivTree').show();
      $('#TrIdPwEdit').show();
      $('#TrIdORGN_MGNB_YSNO').show();
      $('#TrIdTRST_ORGN_CODE').show();
      $('#TrIdSCSS_YSNO').show();
    } else {
      $('#TrIdPwWrite').show();
      $('#DivIdCheckBtn').show();
    }
  });

  /* tree setting */
  function fncTreeInit() {
    var datas = [];
    datas[0] = 'GROUP_YMD';
    datas[1] = 'GROUP_SEQ';
    var url = fncGetBoardParam('<c:url value="/console/mber/fdcrAd84UpdateForm1Sub1.page"/>', datas);
    $('#tree1').jstree({
      'core' : {
      'data' : {
      "url" : url,
      "dataType" : "json"
      },
      "themes" : {
      "theme" : "classic",
      "icon" : false
      }
      }
    }).bind("select_node.jstree", function(event, data) {
      var id = data.node.id;
      id = id.substring(id.indexOf("_") + 1);
    });
  }

  function fncGoList() {
    var url = '<c:url value="/console/mber/fdcrAd84List1.page"/>';
    location.href = url;
  }

  function fncUpdatePwd() {
    if (confirm('비밀번호를 수정하시겠습니까?') == true) {
      var f = document.boardForm;

      if (f.passWord.value == '') {
        alert("비밀번호를 입력하세요");
        f.passWord.focus();
        return false;
      }

      f.action = "/console/mber/fdcrAd84Update4.page";
      f.submit();
    } else {
      return;
    }

  }

  function fncSet() {
    $("#USER_IDNT").keyup(function() {
      $(this).val($(this).val().replace(/[^\!-z]/g, ""));
    });
    //fncSetReadOnly("TRST_CEOX_NAME");
    //fncSetReadOnly("TRST_TEXL_NUMB");
    if (gubun == 'edit') {
      fncSetDisable("TRST_ORGN_DIVS_CODE");
    }
  }

  function fncSetDisable(id) {
    $('#' + id).attr('disabled', true);
    $('#' + id).css("background-color", "#eeeeee");
  }

  function fncSetEnable(id) {
    $('#' + id).attr('disabled', false);
    $('#' + id).css("background-color", "#ffffff");
  }

  function fncSetReadOnly(id) {
    $('#' + id).attr('readonly', true);
    $('#' + id).css("background-color", "#eeeeee");
  }

  function fncSetNotReadOnly(id) {
    $('#' + id).attr('readonly', false);
    $('#' + id).css("background-color", "#ffffff");
  }

  function fncSave() {
    var f = document.boardForm;
    if (gubun == 'edit') {
      if (f.COMM_NAME.value == '') {
        alert("단체 및 기관명을 입력하세요");
        f.COMM_NAME.focus();
        return false;
      }

      if (f.TRST_CEOX_NAME.value == '') {
        alert("대표자명을 입력하세요");
        f.TRST_CEOX_NAME.focus();
        return false;
      }

      if (f.TRST_TEXL_NUMB.value == '') {
        alert("연락처를 입력하세요");
        f.TRST_TEXL_NUMB.focus();
        return false;
      }

      fncCheckOrgnMgnb();
    }

    if (f.USER_NAME.value == '') {
      alert("이름을 입력하세요");
      f.USER_NAME.focus();
      return false;
    }
    if (f.USER_NAME.value == '') {
      alert("이름을 입력하세요");
      f.USER_NAME.focus();
      return false;
    }
    if (f.USER_DEPARTMENT.value == '') {
      alert("부서명을 입력하세요");
      f.USER_DEPARTMENT.focus();
      return false;
    }  
    if (f.USER_POSITION.value == '') {
      alert("직급 또는 직책을 입력하세요");
      f.USER_POSITION.focus();
      return false;
    }  
    if (f.USER_IDNT.value == '') {
      alert("아이디를 입력하세요");
      return false;
    }
    if (gubun == 'write' && f.userIdChk.value != 'Y') {
      alert("아이디 중복확인을 해주십시요");
      return false;
    }

    if (f.MAIL.value == '') {
      alert("E-Mail을 입력하세요");
      f.MAIL.focus();
      return false;
    }

    if (f.MOBL_PHON.value == '') {
      alert("핸드폰번호를 입력하세요");
      f.MOBL_PHON.focus();
      return false;
    }

    if (gubun == 'edit') {
      f.action = "/console/mber/fdcrAd84Update1.page";
    } else if (gubun == 'write') {
      f.action = "/console/mber/fdcrAd84Insert1.page";
    }
    f.submit();
  }

  /**
   * 아이디 중복
   */
  function fncCheckId() {
    var userId = $('#USER_IDNT').val();
    if (userId != '') {
      var data = {};
      data.USER_IDNT = userId;
      fncPost('<c:out value="/console/mber/fdcrAd84Update2.page"/>', data, function(returnData) {
        if (returnData == 'Y') {
          alert('사용 중인 아이디 입니다.');
          $('#USER_IDNT').val('');
          $('#USER_IDNT').focus();
        } else {
          alert('사용 가능한 아이디 입니다.');
          $('#userIdChk').val('Y');
        }
      });
    } else {
      alert('아이디를 입력해주세요.');
      $('#USER_IDNT').focus();
    }
  }

  /**
   * 대표 담당자 존재 여부
   */
  function fncCheckOrgnMgnb() {
    var data = {};
    data.TRST_ORGN_CODE = document.getElementById("TRST_ORGN_CODE");
    fncPost('<c:out value="/console/mber/fdcrAd84Update3.page"/>', data, function(returnData) {
      if (returnData == 'Y') {
        $('#ORGN_MGNB_YSNO').val('Y');
        alert('해당기관으로 등록된 대표담당자정보가 존재합니다. \n대표담당자여부는 로그인 후 회원정보에서 수정이 가능합니다.');
      } else {
        $('#ORGN_MGNB_YSNO').val('N');
      }
    })
  }

  function fncChangeTrst(TRST_ORGN_DIVS_CODE) {
    $('#COMM_NAME').val('');
    $('#TRST_CEOX_NAME').val('');
    $('#TRST_TEXL_NUMB').val('');
    if (TRST_ORGN_DIVS_CODE == 1 || TRST_ORGN_DIVS_CODE == 2) {
      fncSetEnable("TRST_CEOX_NAME");
      fncSetEnable("TRST_TEXL_NUMB");
      $('#DivIdCommNameSearchBtn').show();
    } else if (TRST_ORGN_DIVS_CODE == 5) {
      fncSetEnable("COMM_NAME");
      fncSetEnable("TRST_CEOX_NAME");
      fncSetEnable("TRST_TEXL_NUMB");
      $('#DivIdCommNameSearchBtn').hide();
    } else {
      fncSetDisable("TRST_CEOX_NAME");
      fncSetDisable("TRST_TEXL_NUMB");
      $('#DivIdCommNameSearchBtn').hide();
    }
  }

  //기관/단체 조회 팝업창
  function fncSearchCompanyPopup() {
    var TRST_ORGN_DIVS_CODE = $('#TRST_ORGN_DIVS_CODE').val();
    var popUrl = '<c:url value="/console/mber/fdcrAd84Pop1.page"/>?TRST_ORGN_DIVS_CODE=' + TRST_ORGN_DIVS_CODE;
    var popOption = "width=800, height=600, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
    window.open(popUrl, "", popOption);
  }

  function fncCommNameUpdate() {
    if (confirm('단체 및 기관명을 수정하시겠습니까?') == true) {
      var f = document.boardForm;
      if (f.COMM_NAME.value == '') {
        alert("단체명을 입력하세요");
        f.COMM_NAME.focus();
        return false;
      }

      f.action = "/console/mber/fdcrAd84Update5.page";
      f.submit();
    } else {
      return;
    }
  }
  function fncBack2() {
      if (confirm('초기화하시겠습니까? ') == true) {
    var f = document.boardForm;
    $("#boardForm")[0].reset();
  } else {
    return;
  } 
  }
  function fncDelete() {
    if (confirm('단체 및 기관을 삭제하시겠습니까?\n삭제시 등록 되었던 모든 데이터가 삭제 됩니다.') == true) {
      var f = document.boardForm;

      f.action = "/console/mber/fdcrAd84CompanyDelete.page";
      f.submit();
    } else {
      return;
    }
  
    
  }
</script>
<form name="boardForm" id="boardForm" method="post">
	
	<input type="hidden" id="userId" name="userId" value="<c:out value="${info.USER_IDNT}"/>" /> 
	<input type="hidden" id="clmsAgrYn" name="clmsAgrYn" value="<c:out value="${info.CLMS_AGR_YN}"/>" />
	<input type="hidden" id="userIdChk" name="userIdChk" value="N" />
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" /> 
	<input type="hidden" id="GROUP_YMD" name="GROUP_YMD" value="<c:out value="${info.GROUP_YMD}"/>" /> 
	<input type="hidden" id="GROUP_SEQ" name="GROUP_SEQ" value="<c:out value="${info.GROUP_SEQ}"/>" /> 
	<input type="hidden" id="MAIL_RECE_YSNO" name="MAIL_RECE_YSNO" value="<c:out value="${info.MAIL_RECE_YSNO}"/>" /> 
	<input type="hidden" id="TELX_NUMB" name="TELX_NUMB" value="<c:out value="${info.TELX_NUMB}"/>" /> 
	<input type="hidden" id="SMS_RECE_YSNO" name="SMS_RECE_YSNO" value="<c:out value="${info.SMS_RECE_YSNO}"/>" />
	<input type="hidden" id="COMM_NAME_OLD" name="COMM_NAME_OLD" value="<c:out value="${info.COMM_NAME}"/>" />
	<input type="hidden" id="COMM_TRST_ORGN_CODE" name="COMM_TRST_ORGN_CODE" value="<c:out value="${info.TRST_ORGN_CODE }"/>" />


	<div class="box" style="margin-top: 30px;">
		<div class="box-header with-border">
			<h3 class="box-title">담당자 수정</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th style="width: 20%">구분 *</th>
						<td style="width: 80%">
							<select id="TRST_ORGN_DIVS_CODE" name="TRST_ORGN_DIVS_CODE" onchange="fncChangeTrst(this.value);">
								<option value="3" <console:fn func="isSelected" value="3" value1="${info.TRST_ORGN_DIVS_CODE }" />>시스템관리자</option>
								<option value="4" <console:fn func="isSelected" value="4" value1="${info.TRST_ORGN_DIVS_CODE }" />>심의조정팀</option>
								<option value="2" <console:fn func="isSelected" value="2" value1="${info.TRST_ORGN_DIVS_CODE }" />>신탁단체</option>
								<option value="1" <console:fn func="isSelected" value="1" value1="${info.TRST_ORGN_DIVS_CODE }" />>대리중개</option>
								<option value="5" <console:fn func="isSelected" value="5" value1="${info.TRST_ORGN_DIVS_CODE }" />>문화예술협단체</option>
							</select>
						</td>
					</tr>
					<tr>
						<th style="width: 20%">단체 및 기관 명 *</th>
						<c:if test="${CONSOLE_USER.USER_ID == 'CPMADMIN' }">
							<td style="width: 80%">
								<input type="text" name="COMM_NAME" id="COMM_NAME" value="<c:out value="${info.COMM_NAME}"/>" size="50">
								<div class="btn-group">
									<!--  <button type="submit" class="btn btn-block btn-primary btn-xs" onclick="fncCommNameUpdate()">변경</button> -->
								</div>
							</td>
						</c:if>
						<c:if test="${CONSOLE_USER.USER_ID != 'CPMADMIN' }">
							<td style="width: 80%">
								<input type="text" disabled="disabled" id="COMM_NAME" name="COMM_NAME" value="<c:out value="${info.COMM_NAME}"/>" />
							</td>
						</c:if>
					</tr>
					<tr>
						<th style="width: 20%">대표자 명 *</th>
						<td style="width: 80%">
							<input type="text" name="TRST_CEOX_NAME" id="TRST_CEOX_NAME" value="<c:out value="${info.TRST_CEOX_NAME}"/>" size="50" />
						</td>
					</tr>
					<tr>
						<th style="width: 20%">연락처 *</th>
						<td style="width: 80%">
							<input type="text" name="TRST_TEXL_NUMB" id="TRST_TEXL_NUMB" value="<c:out value="${info.TRST_TEXL_NUMB}"/>" size="50" />
						</td>
					</tr>
					<tr>
						<th style="width: 20%">이름 *</th>
						<td style="width: 80%">
							<input type="text" name="USER_NAME" id="USER_NAME" value="<c:out value="${info.USER_NAME}"/>" size="50" />
						</td>
					</tr>
					<tr>
						<th style="width: 20%">부서명 *</th>
						<td style="width: 80%">
							<input type="text" name="USER_DEPARTMENT" id="USER_DEPARTMENT" value="<c:out value="${info.USER_DEPARTMENT}"/>" size="50" />
						</td>
					</tr>
					<tr>
						<th style="width: 20%">직급 또는 직책 *</th>
						<td style="width: 80%">
							<input type="text" name="USER_POSITION" id="USER_POSITION" value="<c:out value="${info.USER_POSITION}"/>" size="50" />
						</td>
					</tr>
					<tr>
						<th style="width: 20%">아이디 *</th>
						<td style="width: 80%">
							<c:choose>

								<c:when test="${info.USER_IDNT !=null }">

									<input type="text" name="USER_IDNT" id="USER_IDNT" style="ime-mode: inactive" readonly value="<c:out value="${info.USER_IDNT}"/>" size="50" />

									<div class="btn-group" id="DivIdCheckBtn" style="display: none">
										<button type="submit" class="btn btn-block btn-primary btn-xs" onclick="fncCheckId();return false;">중복확인</button>
									</div>

								</c:when>

								<c:when test="${info.USER_IDNT == null}">

									<input type="text" name="USER_IDNT" id="USER_IDNT" style="ime-mode: inactive" value="<c:out value="${info.USER_IDNT}"/>" size="50" />

									<div class="btn-group" id="DivIdCheckBtn" style="display: none">
										<button type="submit" class="btn btn-block btn-primary btn-xs" onclick="fncCheckId();return false;">중복확인</button>
									</div>

								</c:when>



							</c:choose>

							<%--     <input type="text" name="USER_IDNT" id="USER_IDNT" style="ime-mode: inactive" value="<c:out value="${info.USER_IDNT}"/>" size="50" />
                
                  <div class="btn-group" id="DivIdCheckBtn" style="display: none">
                    <button type="submit" class="btn btn-block btn-primary btn-xs" onclick="fncCheckId();return false;">중복확인</button>
                  </div> --%>


						</td>
					</tr>
					<tr id="TrIdPwEdit" style="display: none">
						<th style="width: 20%">비밀번호 *</th>
						<td style="width: 80%">
							<input type="password" id="passWord" name="passWord" />
							<div class="btn-group">
								<button type="submit" class="btn btn-block btn-primary btn-xs" onclick="fncUpdatePwd();">수정</button>
							</div>
						</td>
					</tr>
					<tr id="TrIdPwWrite" style="display: none">
						<th style="width: 20%">비밀번호 *</th>
						<td style="width: 80%">
							<input type="password" id="PSWD" name="PSWD" size="50" />
						</td>
					</tr>
					<tr>
						<th style="width: 20%">E-Mail *</th>
						<td style="width: 80%">
							<input type="text" name="MAIL" id="MAIL" value="<c:out value="${info.MAIL}"/>" size="50" />
						</td>
					</tr>
					<tr>
						<th style="width: 20%">핸드폰번호 *</th>
						<td style="width: 80%">
							<input type="text" name="MOBL_PHON" id="MOBL_PHON" value="<c:out value="${info.MOBL_PHON}"/>" size="50" />
						</td>
					</tr>
					<tr id="TrIdORGN_MGNB_YSNO" style="display: none">
						<th style="width: 20%">대표담당자 여부</th>
						<td style="width: 80%">
							<select id="ORGN_MGNB_YSNO" name="ORGN_MGNB_YSNO">
								<option value="Y" <console:fn func="isSelected" value="Y" value1="${info.ORGN_MGNB_YSNO }" />>Yes</option>
								<option value="N" <console:fn func="isSelected" value="N" value1="${info.ORGN_MGNB_YSNO }" />>No</option>
							</select>
						</td>
					</tr>
					<tr id="TrIdTRST_ORGN_CODE" style="display: none">
						<th style="width: 20%">관리자구분</th>
						<td style="width: 80%">
							<select id="TRST_ORGN_CODE" name="TRST_ORGN_CODE">
								<option value="200" <console:fn func="isSelected" value="200" value1="${info.TRST_ORGN_CODE }" />>시스템관리자</option>
								<option value="201" <console:fn func="isSelected" value="201" value1="${info.TRST_ORGN_CODE }" />>한국음악저작권협회(콤카)</option>
								<option value="202" <console:fn func="isSelected" value="202" value1="${info.TRST_ORGN_CODE }" />>한국음악실연자연합회(음실련)</option>
								<option value="203" <console:fn func="isSelected" value="203" value1="${info.TRST_ORGN_CODE }" />>한국음원제작자협회(음제협)</option>
								<option value="204" <console:fn func="isSelected" value="204" value1="${info.TRST_ORGN_CODE }" />>한국문예학술저작권협회(문예협)</option>
								<option value="205" <console:fn func="isSelected" value="205" value1="${info.TRST_ORGN_CODE }" />>한국복사전송권협회(복전협)</option>
								<option value="206" <console:fn func="isSelected" value="206" value1="${info.TRST_ORGN_CODE }" />>한국방송작가협회(방작협)</option>
								<option value="207" <console:fn func="isSelected" value="207" value1="${info.TRST_ORGN_CODE }" />>한국방송공사(KBS)</option>
								<option value="208" <console:fn func="isSelected" value="208" value1="${info.TRST_ORGN_CODE }" />>(주)문화방송(MBC)</option>
								<option value="210" <console:fn func="isSelected" value="210" value1="${info.TRST_ORGN_CODE }" />>영화진흥위원회(영진위)</option>
								<option value="211" <console:fn func="isSelected" value="211" value1="${info.TRST_ORGN_CODE }" />>한국영상산업협회(영산협)</option>
								<option value="212" <console:fn func="isSelected" value="212" value1="${info.TRST_ORGN_CODE }" />>한국시나리오작가협회</option>
								<option value="213" <console:fn func="isSelected" value="213" value1="${info.TRST_ORGN_CODE }" />>한국영화제작자협회</option>
								<option value="214" <console:fn func="isSelected" value="214" value1="${info.TRST_ORGN_CODE }" />>한국방송실연자연합회</option>
								<option value="215" <console:fn func="isSelected" value="215" value1="${info.TRST_ORGN_CODE }" />>한국언론재단</option>
								<option value="216" <console:fn func="isSelected" value="216" value1="${info.TRST_ORGN_CODE }" />>한국문화콘텐츠</option>

							</select>
						</td>
					</tr>
					<tr id="TrIdSCSS_YSNO" style="display: none">
						<th style="width: 20%">삭제여부</th>
						<td style="width: 80%">
							<select id="SCSS_YSNO" name="SCSS_YSNO">
								<option value="Y" <console:fn func="isSelected" value="Y" value1="${info.SCSS_YSNO }" />>Yes</option>
								<option value="N" <console:fn func="isSelected" value="N" value1="${info.SCSS_YSNO }" />>No</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-footer" style="text-align: right">
		<c:if test="${param.GUBUN ne 'edit'}">
  		<div class="btn-group">
        <button  class="btn btn-primary" onclick="fncBack2();return false;">초기화</button>
      </div>
      </c:if>
			<div class="btn-group">
				<button type="submit" class="btn btn-primary" onclick="fncSave();return false;">저장</button>
			</div>
			<div class="btn-group">
				<button type="submit" class="btn btn-primary" onclick="fncGoList();return false;">목록</button>
			</div>
			<div class="btn-group">
				<button type="submit" class="btn btn-primary" onclick="fncDelete();return false;">삭제</button>
			</div>
		</div>
		<div class="box" style="margin-top: 30px; display: none" id="DivTree">
			<div class="box-body">
				<div id="tree" style="width: 900px; height: 590px; border: 1px solid #ddd; float: left; margin-bottom: 10px; padding: 10px; overflow-y: scroll;">
					<div id="omTreeDiv">
						<div id="tree1"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>