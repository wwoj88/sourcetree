<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
function fncGoList(){
	var url = '<c:url value="/console/mber/fdcrAd83List1.page"/>';
	location.href = url;
}

function fncUpdatePwd(){
	var f = document.boardForm;
	
	if( f.passWord.value == '')
	{
		alert("비밀번호를 입력하세요");
		f.passWord.focus();
		return false;
	}	
	
	f.action = "/console/mber/fdcrAd83Update1.page";
	f.submit();
}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="USER_IDNT" name="USER_IDNT" value="<c:out value="${info.USER_IDNT}"/>" />
	<input type="hidden" id="userId" name="userId" value="<c:out value="${info.USER_IDNT}"/>" />
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
	<input type="hidden" id="CLMS_AGR_YN" name="CLMS_AGR_YN" value="<c:out value="${info.CLMS_AGR_YN}"/>" />
	<input type="hidden" id="CLMS_USER_IDNT" name="CLMS_USER_IDNT" value="<c:out value="${info.CLMS_USER_IDNT}"/>" />
	
<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border">
		<h3 class="box-title" >회원정보 상세정보</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th style="width: 20%">회원구분</th>
					<td style="width: 80%"><c:out value="${info.USER_DIVS}"/></td>
				</tr>
				<tr>
					<th style="width: 20%">신탁회원확인</th>
					<td style="width: 80%"><c:out value="${info.TRST_ORGN_NAME}"/></td>
				</tr>
				<tr>
					<th style="width: 20%">이름/사업자명</th>
					<td style="width: 80%"><c:out value="${info.USER_NAME}"/></td>
				</tr>
				<tr>
					<th style="width: 20%">주민(법인)등록번호</th>
					<td style="width: 80%"><c:out value="${info.RESD_CORP_NUMB}"/></td>
				</tr>
				<tr>
					<th style="width: 20%">아이디</th>
					<td style="width: 80%"><c:out value="${info.USER_IDNT}"/></td>
				</tr>
				<tr>
					<th style="width: 20%">비밀번호</th>
					<td style="width: 80%"><input type="text" id="passWord" name="passWord"/> <div class="btn-group"><button type="submit" class="btn btn-block btn-primary btn-xs" onclick="fncUpdatePwd();">변경</button></div></td>
				</tr>
				<tr>
					<th style="width: 20%">E-Mail</th>
					<td style="width: 80%"><c:out value="${info.MAIL}"/></td>
				</tr>
				<tr>
					<th style="width: 20%">우편번호</th>
					<td style="width: 80%"><c:out value="${info.ZIPX_CODE}"/></td>
				</tr>
				<tr>
					<th style="width: 20%">주소</th>
					<td style="width: 80%"><c:out value="${info.ADDR}"/></td>
				</tr>
				<tr>
					<th style="width: 20%">상세주소</th>
					<td style="width: 80%"><c:out value="${info.DETL_ADDR}"/></td>
				</tr>
				<tr>
					<th style="width: 20%">핸드폰번호</th>
					<td style="width: 80%"><c:out value="${info.MOBL_PHON}"/></td>
				</tr>
				<tr>
					<th style="width: 20%">수신방법</th>
					<td style="width: 80%">이메일 : <c:out value="${info.MAIL_RECE_YSNO}"/> SMS: <c:out value="${info.SMS_RECE_YSNO}"/></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoList();return false;">목록</button></div>
	</div>
</div>
</form>