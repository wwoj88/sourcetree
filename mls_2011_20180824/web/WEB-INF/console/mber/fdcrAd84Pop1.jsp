<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncGoSearch(){
		var TRST_ORGN_DIVS_CODE = $('#TRST_ORGN_DIVS_CODE').val();
		var url = '<c:url value="/console/mber/fdcrAd84Pop1.page"/>?TRST_ORGN_DIVS_CODE='+TRST_ORGN_DIVS_CODE;
		var SCH_CONAME = $('#SCH_CONAME').val();
		if(SCH_CONAME != ''){
			SCH_CONAME = encodeURI(encodeURIComponent(SCH_CONAME));
			url += '&SCH_CONAME='+SCH_CONAME;
		}
		location.href = url;
	}
	
	function fncSelectCompany(CONAME,CEONAME,TRUSTTEL){
		$("#COMM_NAME",opener.document).val(CONAME);
		$("#TRST_CEOX_NAME",opener.document).val(CEONAME);
		$("#TRST_TEXL_NUMB",opener.document).val(TRUSTTEL);
		self.close();
	}
</script>
<div class="box-body" style="padding:10px">
<form name="updateForm" id="updateForm" method="post" onsubmit="return false;">
<input type="hidden" id="TRST_ORGN_DIVS_CODE" name="TRST_ORGN_DIVS_CODE" value="<c:out value="${param.TRST_ORGN_DIVS_CODE}"/>" />
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">기관/단체/ 찾기</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">기관/단체명/대표자</th>
									<td>
										<input type="text" id="SCH_CONAME" name="SCH_CONAME" value="<c:out value="${commandMap.SCH_CONAME}"/>" onkeyPress="if (event.keyCode==13){return false;}"/>
									</td>
								</tr>
							</tbody>
						</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>
<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border">
		* 아래 목록에서 해당 기관/단체를 선택하세요
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 10%">순번</th>
					<th>기관/단체명</th>
					<th style="width: 10%">대표자</th>
					<th style="width: 15%">신고번호</th>
					<th style="width: 17%">사업자등록번호</th>
					<th style="width: 10%">선택</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty list}">
						<tr>
							<td colspan="10">검색된 결과가 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${listStatus.count}" /></td>
								<td><c:out value="${info.CONAME}" /></td>
								<td><c:out value="${info.CEONAME}" /></td>
								<td><c:out value="${info.APPNO}" /></td>
								<td><c:out value="${info.CONO}" /></td>
								<td><div class="btn-group"><button type="submit" class="btn btn-block btn-primary btn-xs" onclick="fncSelectCompany('<c:out value="${info.CONAME}" />','<c:out value="${info.CEONAME}" />','<c:out value="${info.TRUSTTEL}" />');return false;">선택</button></div></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>
</form>
</div>