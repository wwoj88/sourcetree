<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//검색
	function fncGoSearch() {
		var datas = [];
		datas[0] = 'USER_IDNT';
		datas[1] = 'USER_DIVS';
	
		var url = fncGetBoardParam('<c:url value="/console/mber/fdcrAd83List1.page"/>',datas, 1);
		var USER_NAME = $('#USER_NAME').val();
		if(USER_NAME != ''){
			USER_NAME = encodeURI(encodeURIComponent(USER_NAME));
			url += '&USER_NAME='+USER_NAME;
		}
		
		var pattern = /[^(0-9)]/gi;
		var MOBL_PHON = $('#MOBL_PHON').val().replace(/\s/gi, "").replace(pattern,"");
		if(MOBL_PHON != ''){
			url += '&MOBL_PHON='+MOBL_PHON;
		}
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(USER_IDNT){
		location.href = '<c:url value="/console/mber/fdcrAd83View1.page"/>?USER_IDNT='+USER_IDNT;
	}
	
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		datas[0] = 'USER_IDNT';
		datas[1] = 'USER_DIVS';
		var url = fncGetBoardParam('<c:url value="/console/mber/fdcrAd83List1.page"/>',datas, page);
		var USER_NAME = $('#USER_NAME').val();
		if(USER_NAME != ''){
			USER_NAME = encodeURI(encodeURIComponent(USER_NAME));
			url += '&USER_NAME='+USER_NAME;
		}
		var pattern = /[^(0-9)]/gi;
		var MOBL_PHON = $('#MOBL_PHON').val().replace(/\s/gi, "").replace(pattern,"");
		if(MOBL_PHON != ''){
			url += '&MOBL_PHON='+MOBL_PHON;
		}
		location.href = url;
	}
	
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${param.MENU_SEQN}"/>" /> 
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">구분</th>
									<td>
									<select id="USER_DIVS" name="USER_DIVS">
											<option value="" <console:fn func="isSelected" value="" value1="${commandMap.USER_DIVS }" />>-전체-</option>
											<option value="01" <console:fn func="isSelected" value="01" value1="${commandMap.USER_DIVS }" />>개인회원</option>
											<option value="02" <console:fn func="isSelected" value="02" value1="${commandMap.USER_DIVS }" />>법인회원</option>
											<option value="03" <console:fn func="isSelected" value="03" value1="${commandMap.USER_DIVS }" />>개인사업자</option>
									</select>
									</td>
									<th class="text-center search-th">아이디</th>
									<td>
										<input type="text" id="USER_IDNT" name="USER_IDNT" value="<c:out value="${commandMap.USER_IDNT}"/>"/>
									</td>
									<th class="text-center search-th">신청자명</th>
									<td>
										<input type="text" id="USER_NAME" name="USER_NAME" value="<c:out value="${commandMap.USER_NAME}"/>"/>
									</td>
									<th class="text-center search-th">휴대폰번호</th>
									<td>
										<input type="text" id="MOBL_PHON" name="MOBL_PHON" value="<c:out value="${commandMap.MOBL_PHON}"/>"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>

<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 5%">순번</th>
					<th style="width: 14%">신청자명(아이디)</th>
					<th style="width: 12%">회원구분</th>
					<th style="width: 14%">핸드폰번호</th>
					<th style="width: 12%">가입일자</th>
					<th style="width: 12%">탈퇴여부</th>
					<th style="width: 14%">저작권찾기 신청건수</th>
					<th style="width: 14%">보상금 신청건수</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="6"></td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${paginationInfo.totalRecordCount-(listStatus.count)-((paginationInfo.currentPageNo-1)*paginationInfo.recordCountPerPage)+1}"/></td>
								<td><a href="#" onclick="fncGoView('<c:out value="${info.USER_IDNT}" />');return false;"><c:out value="${info.USER_NAME}" /></a></td>
								<td><c:out value="${info.USER_DIVS}" /></td>
								<td><c:out value="${info.MOBL_PHON}" /></td>
								<td><console:fn func="getDate" value="${info.RGST_DTTM}" value1="yyyyMMdd" value2="yyyy-MM-dd"/></td>
								<td><c:out value="${info.SCSS_YSNO}" /></td>
								<td><c:out value="${info.PRPS_CNT}" /></td>
								<td><c:out value="${info.INMT_CNT}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
</div>