<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd06List1.page"/>',datas, page);
		location.href = url;
	}
	
	//검색
	function fncGoSearch() {
		var GUBUN1 = $('#GUBUN1').val();
		var SCH_NO = $('#SCH_NO').val();
		var SCH_APPLY_TYPE = $('#SCH_APPLY_TYPE').val();
		var SCH_APPLY_WORKS_TITL = $('#SCH_APPLY_WORKS_TITL').val();
		var SCH_STAT_CD = $('#SCH_STAT_CD').val();
		
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06List1.page"/>')+'&GUBUN1='+GUBUN1+'&SCH_NO='+SCH_NO+'&SCH_APPLY_TYPE='+SCH_APPLY_TYPE+'&SCH_APPLY_WORKS_TITL='+SCH_APPLY_WORKS_TITL+'&SCH_STAT_CD='+SCH_STAT_CD;
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(APPLY_WRITE_YMD,APPLY_WRITE_SEQ){
		//var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06View1.page"/>')+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ;
		location.href = url;
	}
	
	//등록폼 이동
	function fncGoWriteForm(){
		var datas = [];
		datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd06WriteForm1.page"/>',datas);
		location.href = url;
	} 
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				
					<div class="input-group" style="width:100%">
						<span class="input-group-addon" style="width:20%;"> <label>구분</label></span>
						<span class="input-group-addon" style="width:30%;"> 
							<select
							class="form-control" style="width:100px" id="GUBUN1" name="GUBUN1">
								<option selected="selected" value="ALL" >--전체--</option>
								<option value="M">음악</option>
								<option value="O">도서</option>
								<option value="N">뉴스</option>
								<option value="C">방송대본</option>
								<option value="I">이미지</option>
								<option value="V">영화</option>
								<option value="R">방송</option>
								<option value="X">기타</option>
							</select>
						</span> 
						<span class="input-group-addon" style="width:20%;"> <label>신청목적</label></span>
						<span class="input-group-addon" style="width:30%;"> 
							<select
							class="form-control" style="width:100px" id="SCH_OPEN_YN" name="SCH_OPEN_YN">
								<option value="" selected="selected">--전체--</option>
								<option value="1">권리자의 저작찾기</option>
								<option value="3">이용자의 저작권조회</option>
							</select>
						</span> 
					</div>
					
					<div class="input-group" style="width:100%">
						<span class="input-group-addon" style="width:20%;"> <label>처리상태</label></span>
						<span class="input-group-addon" style="width:30%;"> 
							<select
							class="form-control" style="width:100px" id="GUBUN1" name="GUBUN1">
								<option selected="selected" value="ALL" >--전체--</option>
								<option value="1">신청</option>
								<option value="2">접수</option>
								<option value="3">처리중</option>
								<option value="4">완료</option>
							</select>
						</span> 
						<span class="input-group-addon" style="width:20%;"> <label>신청일자</label></span>
						<span class="input-group-addon" style="width:30%;"> 
						</span> 
					</div>
					
					<div class="input-group" style="width:100%">
						<span class="input-group-addon" style="width:20%;">
							<select
							class="form-control" style="width:100px" id="GUBUN1" name="GUBUN1">
								<option selected="selected" value="ALL" >신청자</option>
								<option value="1">신청자ID</option>
								<option value="2">신청자명</option>
							</select>
						</span>
						<span class="input-group-addon" style="width:30%;"> 
							<input type="text" id="" name="" value="" />
						</span> 
						<span class="input-group-addon" style="width:20%;"> 
							<select
							class="form-control" style="width:100px" id="GUBUN1" name="GUBUN1">
								<option selected="selected" value="ALL" >--전체--</option>
								<option value="1">저작물명</option>
								<option value="2">내용</option>
							</select>
						</span>
						<span class="input-group-addon" style="width:30%;"> 
							<input type="text" id="" name="" value="" />
						</span> 
					</div>
					<!-- 
					<div class="input-group">
						<span class="input-group-addon" style="width:150px;"> 
							<select
							class="form-control" id="GUBUN1" name="GUBUN1">
								<option selected="selected" value="" >--전체--</option>
								<option value="M">음악</option>
								<option value="O">도서</option>
								<option value="N">뉴스</option>
								<option value="C">방송대본</option>
								<option value="I">이미지</option>
								<option value="V">영화</option>
								<option value="R">방송</option>
								<option value="X">기타</option>
							</select>
						</span>
						<span class="input-group-addon" style="width:200px;"><input type="text" class="form-control" id="SCH_NO" name="SCH_NO"></span>
						<span class="input-group-addon" style="width:100px;"> <label>신청서구분</label></span>
						<span class="input-group-addon" style="width:150px;"> 
							<select
							class="form-control" id="SCH_APPLY_TYPE" name="SCH_APPLY_TYPE">
								<option selected="selected" value="">전체</option>
								<option value="1">저작물</option>
								<option value="2">실연</option>
								<option value="3">음반</option>
								<option value="4">방송</option>
								<option value="5">데이터베이스</option>
							</select>
						</span>
						<span class="input-group-addon" style="width:100px;"> <label>제호(제목)</label></span>
						<span class="input-group-addon" style="width:250px;"><input type="text" class="form-control" id="SCH_APPLY_WORKS_TITL" name="SCH_APPLY_WORKS_TITL"></span>
						<span class="input-group-addon" style="width:100px;"> <label>진행상태</label></span>
						<span class="input-group-addon" style="width:150px;"> 
							<select
							class="form-control" id="SCH_STAT_CD" name="SCH_STAT_CD">
								<option selected="selected" value="">전체</option>
								<option value="1">임시저장</option>
								<option value="2">신청완료</option>
								<option value="3">보완요청</option>
								<option value="4">보완임시저장</option>
								<option value="5">보완완료</option>
								<option value="6">반려</option>
								<option value="7">결제요청</option>
								<option value="8">접수완료</option>
								<option value="9">심의중</option>
								<option value="10">심의완료</option>
							</select>
						</span>
						<div class="btn-group" style="float: right; margin: 10px 20px 0 0;"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
					</div>
					 -->
				</div>
			</div>
			<!-- /.row -->
		</div>
		
		<!-- /.box-body -->
	</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd35List1" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center" style="width: 11%">구분</th>
					<th class="text-center" style="width: 20%">신청목적</th>
					<th class="text-center">저작물명</th>
					<th class="text-center" style="width: 11%">신청자</th>
					<th class="text-center" style="width: 11%">신청일자</th>
					<th class="text-center" style="width: 11%">처리상태</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="7" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.RNO}" /></td>
								<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>');"><c:out value="${info.APPLY_NO}" /></a></td>
								<td class="text-center"><c:out value="${info.RECEIPT_NO}" /></td>
								<td class="text-center">
								<c:if test="${info.APPLY_TYPE == '1'}">저작물</c:if>
								<c:if test="${info.APPLY_TYPE == '2'}">실연</c:if>
								<c:if test="${info.APPLY_TYPE == '3'}">음반</c:if>
								<c:if test="${info.APPLY_TYPE == '4'}">방송</c:if>
								<c:if test="${info.APPLY_TYPE == '5'}">데이터베이스</c:if>
								</td>
								<td class="text-center"><c:out value="${info.APPLY_WORKS_TITL}" /></td>
								<td class="text-center">
								<c:if test="${info.STAT_CD == '1'}">임시저장</c:if>
								<c:if test="${info.STAT_CD == '2'}"><font color="red">신청완료</font></c:if>
								<c:if test="${info.STAT_CD == '3'}">보완요청</c:if>
								<c:if test="${info.STAT_CD == '4'}">보완임시저장</c:if>
								<c:if test="${info.STAT_CD == '5'}">보완완료</c:if>
								<c:if test="${info.STAT_CD == '6'}">반려</c:if>
								<c:if test="${info.STAT_CD == '7'}">결제요청</c:if>
								<c:if test="${info.STAT_CD == '8'}">접수완료</c:if>
								<c:if test="${info.STAT_CD == '9'}">심의중</c:if>
								<c:if test="${info.STAT_CD == '10'}">심의완료</c:if>
								</td>
								<td class="text-center"><c:out value="${info.APPLR_NAME}" /></td>
								<td class="text-center"><c:out value="${info.WORKS_CNT}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
	</div>
</div>