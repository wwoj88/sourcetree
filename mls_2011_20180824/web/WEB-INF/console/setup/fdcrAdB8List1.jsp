<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>

	function fncCheck(div){
		if($('#YSNO_'+div).prop("checked")){
			$('#START_MM_'+div).attr("disabled",false);
			$('#START_DD_'+div).attr("disabled",false);
			$('#END_MM_'+div).attr("disabled",false);
			$('#END_DD_'+div).attr("disabled",false);
		}else{
			$('#START_MM_'+div).attr("disabled",true);
			$('#START_DD_'+div).attr("disabled",true);
			$('#END_MM_'+div).attr("disabled",true);
			$('#END_DD_'+div).attr("disabled",true);
		}	
	}
	
	//저장
	function fncUpdate1(div){
		var f = document.bodyForm;
		f.START_MM.value = $('#START_MM_'+div).val();
		f.START_DD.value = $('#START_DD_'+div).val();
		f.END_MM.value = $('#END_MM_'+div).val();
		f.END_DD.value = $('#END_DD_'+div).val();
		f.YSNO.value = $('#YSNO_'+div).val();
		f.DIV.value = div;
		f.action = '<c:url value="/console/setup/fdcrAdB8Update1.page"/>';
		f.submit();
	} 
</script>
<form name="bodyForm" id="bodyForm" method="post">
	<input type="hidden" id="START_MM" name="START_MM" value="" />
	<input type="hidden" id="START_DD" name="START_DD" value="" /> 
	<input type="hidden" id="END_MM" name="END_MM" value="" />
	<input type="hidden" id="END_DD" name="END_DD" value="" /> 
	<input type="hidden" id="YSNO" name="YSNO" value="" /> 
	<input type="hidden" id="DIV" name="DIV" value="" /> 
</form>

<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAdB8List1" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="text-center">&nbsp;</th>
					<th class="text-center" style="width: 15%">시작일</th>
					<th class="text-center" style="width: 15%">종료일</th>
					<th class="text-center" style="width: 10%">사용여부</th>
					<th class="text-center" style="width: 10%">저장</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list_0}">
						<tr>
							<td colspan="5">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list_0}" varStatus="listStatus">
							<tr>
								<td class="text-center">
									<c:if test="${info.DIV == 1}">미분배보상금 저작물보고 등록</c:if>
									<c:if test="${info.DIV == 2}">미분배보상금 저작물보고 수정</c:if>
									<c:if test="${info.DIV == 3}">위탁관리 저작물보고 등록</c:if>
								</td>
								<td class="text-center">
									<c:if test="${info.DIV == 1}">
										<console:fn func="getMonthSelectBox" value="${info.START_MM }" value1="START_MM_${info.DIV}" value2="${info.YSNO}"/>월
									</c:if>
									<console:fn func="getDaySelectBox" value="${info.START_DD }" value1="START_DD_${info.DIV}" value2="${info.YSNO}"/>일
								</td>
								<td class="text-center">
									<c:if test="${info.DIV == 1}">
										<console:fn func="getMonthSelectBox" value="${info.END_MM }" value1="END_MM_${info.DIV}" value2="${info.YSNO}"/>월
									</c:if>
									<console:fn func="getDaySelectBox" value="${info.END_DD }" value1="END_DD_${info.DIV}" value2="${info.YSNO}"/>일
								</td>
								<td class="text-center"><input type="checkbox" name="YSNO" id="YSNO_${info.DIV}" value="Y" <console:fn func="isChecked" value="Y" value1="${info.YSNO}"/> onclick="fncCheck('<c:out value="${info.DIV}"/>');"></td>
								<td class="text-center"><button onclick="fncUpdate1('<c:out value="${info.DIV}"/>');return false;">저장</button></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncDeleteGroup();return false;">삭제</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
	</div>
</div>