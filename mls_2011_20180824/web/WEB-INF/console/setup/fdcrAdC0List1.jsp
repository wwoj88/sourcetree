<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//저장
	function fncUpdate1(setCd){
		var f = document.bodyForm;
		f.USE_YN.value = $('#USE_YN_'+setCd).val();
		f.SET_CD.value = setCd;
		f.action = '<c:url value="/console/setup/fdcrAdC0Update1.page"/>';
		f.submit();
	} 
</script>
<form name="bodyForm" id="bodyForm" method="post">
	<input type="hidden" id="USE_YN" name="USE_YN" value="" /> 
	<input type="hidden" id="SET_CD" name="SET_CD" value="" /> 
</form>

<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAdC0List1" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="text-center" style="width: 40%">&nbsp;</th>
					<th class="text-center" style="width: 40%">설정정보</th>
					<th class="text-center" style="width: 10%">사용여부</th>
					<th class="text-center" style="width: 10%">저장</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list_stat}">
						<tr>
							<td colspan="4">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list_stat}" varStatus="listStatus">
							<tr>
								<td class="text-center">
									<c:if test="${info.SET_CD == 10}">1.상당한노력 대상저작물 수집 및 수행</c:if>
									<c:if test="${info.SET_CD == 20}">2.상당한노력 수행결과 예외처리 직접수행여부</c:if>
									<c:if test="${info.SET_CD == 30}">3.법정허락 전환 대상저작물 수집 및 수행</c:if>
								</td>
								<td class="text-center">
									<c:if test="${info.SET_CD == 10}">
									1. 수행시기 : 매월 마지막 토요일 0시 수행<br/>
									2. 완료안내 : 등록된 이메일주소로 수행결과를 메일발송
									</c:if>
									<c:if test="${info.SET_CD == 20}">
									상당한 수행결과에 대한 예외처리를 <br/>
									담당자가 직접수행하는것으로 [예외처리 완료]처리 시 <br/>
									“3.법정허락 전환 대상저작물 수집 및 수행”에 대한 <br/>
									자동화 수행이 진행됨
									</c:if>
									<c:if test="${info.SET_CD == 30}">
									1. 수행시기
									&nbsp;&nbsp;&nbsp;1-1. 예외처리 직접수행 시<br/>
									&nbsp;&nbsp;&nbsp;    1-2. 상당한노력 수행완료 시<br/>
									2. 완료안내 : 등록된 이메일주소로 수행결과를 메일발송
									</c:if>
								</td>
								<td class="text-center"><input type="checkbox" name="USE_YN" id="USE_YN_${info.SET_CD}" value="1" <console:fn func="isChecked" value="1" value1="${info.USE_YN}"/>></td>
								<td class="text-center"><button onclick="fncUpdate1('<c:out value="${info.SET_CD}"/>');return false;">저장</button></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>