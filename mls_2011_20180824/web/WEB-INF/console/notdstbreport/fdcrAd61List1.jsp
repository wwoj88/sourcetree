<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	// 년도 검색
	function reptYyyyChng(selOptVal) {
		var url = fncGetBoardParam('<c:url value="/console/notdstbreport/fdcrAd61List1.page"/>')+'&REPT_YYYY='+selOptVal;
		location.href = url;
	}
</script>
<form name="writeForm" id="writeForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />

<div class="box box-default">
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group" style="float: left;">
						<span style="width:150px; float: left; display: inline-block;">
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<c:set var="schReptYyyy" value="${info.REPT_YYYY }"></c:set>
						</c:forEach>
							<select class="form-control" id="REPT_YYYY" name="REPT_YYYY" onchange="reptYyyyChng(this.value);">
								<option value="2016" <c:if test="${schReptYyyy eq '2016'}">selected="selected"</c:if>>2016년</option>
								<option value="2015" <c:if test="${schReptYyyy eq '2015'}">selected="selected"</c:if>>2015년</option>
								<option value="2014" <c:if test="${schReptYyyy eq '2014'}">selected="selected"</c:if>>2014년</option>
								<option value="2013" <c:if test="${schReptYyyy eq '2013'}">selected="selected"</c:if>>2013년</option>
							</select>
						</span>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd71List1" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 10%; background-color: teal;" rowspan="3"><font color="white">년도</font></th>
					<th class="text-center" style="width: 90%; background-color: teal;" colspan="3"><font color="white">미분배보상금 저작물 등록현황</font></th>
				</tr>
				<tr>
					<th class="text-center" style="width: 30%; background-color: teal;"><font color="white">방송음악</font></th>
					<th class="text-center" style="width: 30%; background-color: teal;"><font color="white">교과용</font></th>
					<th class="text-center" style="width: 30%; background-color: teal;"><font color="white">도서관</font></th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="4" class="text-center">등록 현황이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.REPT_YYYY}"/></td>
								<td class="text-center"><c:out value="${info.BRCT_CONT}"/></td>
								<td class="text-center"><c:out value="${info.SUBJ_CONT}"/></td>
								<td class="text-center"><c:out value="${info.LIBR_CONT}"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
</div>
</form>