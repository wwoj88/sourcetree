<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	// 년도 검색
	function reptYyyyChng(selOptVal) {
		var url = fncGetBoardParam('<c:url value="/console/notdstbmanage/fdcrAd73List1.page"/>')+'&INMT_YEAR='+selOptVal;
		location.href = url;
	}
	
	function fncGoSave(){
		var f = document.writeForm;
		var inmtYear = $('#inmtYear').val()
		var quarter = $('#quarter').val()
		var alltInmt = $('#alltInmt').val()
		
		f.action = '<c:url value="/console/notdstbmanage/fdcrAd73Insert1.page"/>';
		f.submit();
	}
</script>
<form name="writeForm" id="writeForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />

<div class="box box-default">
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group" style="float: left;">
						<span style="width:150px; float: left; display: inline-block;">
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<c:set var="schInmtYear" value="${info.INMT_YEAR }"></c:set>
						</c:forEach>
							<select class="form-control" id="INMT_YEAR" name="INMT_YEAR" onchange="reptYyyyChng(this.value);">
								<option value="2016" <c:if test="${schInmtYear eq '2016'}">selected="selected"</c:if>>2016년</option>
								<option value="2015" <c:if test="${schInmtYear eq '2015'}">selected="selected"</c:if>>2015년</option>
								<option value="2014" <c:if test="${schInmtYear eq '2014'}">selected="selected"</c:if>>2014년</option>
								<option value="2013" <c:if test="${schInmtYear eq '2013'}">selected="selected"</c:if>>2013년</option>
							</select>
						</span>
					</div>
						<div class="box-header with-border pull-right">
							<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSave();return false;">등록</button></div>
						</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd73List1" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 15%; background-color: teal;"><font color="white">년도</font></th>
					<th class="text-center" style="width: 25%; background-color: teal;"><font color="white">분기</font></th>
					<th class="text-center" style="width: 60%; background-color: teal;"><font color="white">미분배 보상금</font></th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="3" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><input type="text" id="inmtYear" name="inmtYear" readonly="readonly" value="${info.INMT_YEAR}" style="width: 100%;background-color:transparent;border:0px;text-align: center;"/></td>
								<td class="text-center"><input type="text" id="quarter" name="quarter" readonly="readonly" value="${info.QUARTER}" style="width: 100%;background-color:transparent;border:0px;text-align:center;"/></td>
								<td style="text-align: right;"><input type="text" id="alltInmt" name="alltInmt" value="${info.ALLT_INMT}" style="width: 100%;background-color:transparent;border:0px;text-align: right;"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		<span style="color: orange; margin-top: 10px;"><b>* 분기별 미분배 보상금액을 입력하면 위원회에서 최종승인 후 저작권찾기 사이트에 공개됩니다.</b></span>
	</div>
	<!-- /.box-body -->
</div>
</form>