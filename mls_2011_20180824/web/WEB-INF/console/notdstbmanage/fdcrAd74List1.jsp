<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	
	// 년도 검색
	function reptYyyyChng(selOptVal) {
		var url = fncGetBoardParam('<c:url value="/console/notdstbmanage/fdcrAd74List1.page"/>')+'&inmtYear='+selOptVal;
		location.href = url;
	}
	
	function fncGoSave(){
		 	var $obj = $("input[name='openYsno']");
		    var countSize = $obj.size();
		    //var checkCount = $("input[name='openYsno']:checked").length;
		    var f = document.writeForm;
		    for(var i=0; i < countSize; i++ ){
				//if( $obj.eq(i).is(":checked")==true ){
					var target_1 = document.getElementById("inmtYear_"+i).value;
					var target_2 = document.getElementById("half_"+i).value;
					var target_3 = document.getElementById("a1_"+i).value;
					var target_4 = document.getElementById("a2_"+i).value;
					var target_5 = document.getElementById("a3_"+i).value;
					var target_6 = document.getElementById("b1_"+i).value;
					var target_7 = document.getElementById("b2_"+i).value;
					var target_8 = document.getElementById("b3_"+i).value;
					var target_9 = document.getElementById("c1_"+i).value;
					var target_10 = document.getElementById("c2_"+i).value;
					var target_11 = document.getElementById("totAlltInmt_"+i).value;
					if( $obj.eq(i).is(":checked")==true ){
						var target_12 = "1";
					}else{
						var target_12 = "0";
					}
					//var target_12 = document.getElementById("openYsno_"+i).value;
					
					f.action = '<c:url value="/console/notdstbmanage/fdcrAd74Insert1.page"/>?INMT_YEAR='
						+ target_1+'&HALF='+target_2+'&A1='+target_3+'&A2='
						+target_4+'&A3='+target_5+'&B1='+target_6+'&B2='+target_7+'&B3='
						+target_8+'&C1='+target_9+'&C2='+target_10+'&TOT_ALLT_INMT='+target_11+'&OPEN_YSNO='+target_12;
					f.submit();
				//}
		    }
		    
		//var f = document.writeForm;
		//	f.action = '<c:url value="/console/notdstbmanage/fdcrAd74Insert1.page"/>';
		//	f.submit();
	}
</script>
<form name="writeForm" id="writeForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
	<input type="hidden" id="strOpenYsno" name="strOpenYsno"/>

<div class="box box-default">
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group" style="width:100%;">
						<span style="float: left; display: inline-block;"> 
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<c:set var="schInmtYear" value="${info.INMT_YEAR }"></c:set>
						</c:forEach>
							<select class="form-control" id="inmtYear" name="inmtYear" onchange="reptYyyyChng(this.value);">
								<option selected="selected" value="" >--전체--</option>
								<option value="2020" <c:if test="${schInmtYear eq '2020'}">selected="selected"</c:if>>2020년</option>
								<option value="2019" <c:if test="${schInmtYear eq '2019'}">selected="selected"</c:if>>2019년</option>
								<option value="2018" <c:if test="${schInmtYear eq '2018'}">selected="selected"</c:if>>2018년</option>
								<option value="2017" <c:if test="${schInmtYear eq '2017'}">selected="selected"</c:if>>2017년</option>
								<option value="2016" <c:if test="${schInmtYear eq '2016'}">selected="selected"</c:if>>2016년</option>
								<option value="2015" <c:if test="${schInmtYear eq '2015'}">selected="selected"</c:if>>2015년</option>
								<option value="2014" <c:if test="${schInmtYear eq '2014'}">selected="selected"</c:if>>2014년</option>
								<option value="2013" <c:if test="${schInmtYear eq '2013'}">selected="selected"</c:if>>2013년</option>
								<%-- 
								<option value="2012" <c:if test="${schInmtYear eq '2012'}">selected="selected"</c:if>>2012년</option>
								<option value="2011" <c:if test="${schInmtYear eq '2011'}">selected="selected"</c:if>>2011년</option>
								<option value="2010" <c:if test="${schInmtYear eq '2010'}">selected="selected"</c:if>>2010년</option>
								<option value="2009" <c:if test="${schInmtYear eq '2009'}">selected="selected"</c:if>>2009년</option>
								 --%>
							</select>
						</span>
						<span style="float: right;"> 
						<font color="red" style="text-align: right; float: right;">(단위 : 천원)</font>
						</span>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd74List1" class="table table-bordered text-center">
			<thead>
				<tr>
					<th style="width: 8%;" rowspan="3">년도</th>
					<th style="width: 6%;" rowspan="3">반기</th>
					<th style="width: 68%;" colspan="8">분야별미분배보상금</th>
					<th style="width: 10%;" rowspan="3">계</th>
					<th style="width: 8%;;" rowspan="3">공개여부</th>
				</tr>
				<tr>
					<th style="width: 24%;" colspan="3">음반제작자</th>
					<th style="width: 24%;" colspan="3">실연자</th>
					<th style="width: 16%;" colspan="2">저작권자</th>
				</tr>
				<tr>
					<th style="width: 8%;">판매용<br/>음반 방송</th>
					<th style="width: 8%;">디지털<br/>음성 송신</th>
					<th style="width: 8%;">판매용<br/>음반 공연</th>
					<th style="width: 8%;">판매용<br/>음반 방송</th>
					<th style="width: 8%;">디지털<br/>음성 송신</th>
					<th style="width: 8%;">판매용<br/>음반 공연</th>
					<th style="width: 8%;">교과용<br/>도서</th>
					<th style="width: 8%;">도서관</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="12" class="text-center">게시물이 없습니다.(년도 검색을 해주세요.)</td>
						</tr>
					</c:when>
					<c:otherwise>
						<%-- <c:forEach var="info" items="${ds_list}" varStatus="listStatus" begin="1" end="12" step="1"> --%>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr id="tr_${listStatus.index}">
								<td class="text-center" id="td_${listStatus.index}_1"><input type="text" id="inmtYear_${listStatus.index}" name="inmtYear" readonly="readonly" value="${info.INMT_YEAR}" style="width: 100%;background-color:transparent;border:0px;text-align: center;"/></td>
								<td class="text-center" id="td_${listStatus.index}_2"><input type="text" id="half_${listStatus.index}" name="half" readonly="readonly" value="${info.HALF}" style="width: 100%;background-color:transparent;border:0px;text-align: center;"/></td>
								<td class="text-center" id="td_${listStatus.index}_3"><input type="text" id="a1_${listStatus.index}" name="a1" value="${info.A1}" style="width: 100%;background-color:transparent;border:0px;text-align: right;"/></td>
								<td class="text-center" id="td_${listStatus.index}_4"><input type="text" id="a2_${listStatus.index}" name="a2" value="${info.A2}" style="width: 100%;background-color:transparent;border:0px;text-align: right;"/></td>
								<td class="text-center" id="td_${listStatus.index}_5"><input type="text" id="a3_${listStatus.index}" name="a3" value="${info.A3}" style="width: 100%;background-color:transparent;border:0px;text-align: right;"/></td>
								<td class="text-center" id="td_${listStatus.index}_6"><input type="text" id="b1_${listStatus.index}" name="b1" value="${info.B1}" style="width: 100%;background-color:transparent;border:0px;text-align: right;"/></td>
								<td class="text-center" id="td_${listStatus.index}_7"><input type="text" id="b2_${listStatus.index}" name="b2" value="${info.B2}" style="width: 100%;background-color:transparent;border:0px;text-align: right;"/></td>
								<td class="text-center" id="td_${listStatus.index}_8"><input type="text" id="b3_${listStatus.index}" name="b3" value="${info.B3}" style="width: 100%;background-color:transparent;border:0px;text-align: right;"/></td>
								<td class="text-center" id="td_${listStatus.index}_9"><input type="text" id="c1_${listStatus.index}" name="c1" value="${info.C1}" style="width: 100%;background-color:transparent;border:0px;text-align: right;"/></td>
								<td class="text-center" id="td_${listStatus.index}_10"><input type="text" id="c2_${listStatus.index}" name="c2" value="${info.C2}" style="width: 100%;background-color:transparent;border:0px;text-align: right;"/></td>
								<td class="text-center" id="td_${listStatus.index}_11"><input type="text" id="totAlltInmt_${listStatus.index}" name="totAlltInmt" readonly="readonly" value="${info.TOT_ALLT_INMT}" style="width: 100%;background-color:transparent;border:0px;text-align: center;"/></td>
								<td class="text-center" id="td_${listStatus.index}_12">
								<input type="checkbox" name="openYsno" id="openYsno_${listStatus.index}" <console:fn func="isChecked" value="1" value1="${info.OPEN_YSNO}"/>>
								</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		<span style="color: red; margin:10px 0 0 10px; display: inline-block;">*[공개여부] 를 체크하면 저작권 찾기 사이트에서 공개됩니다.</span>
		<button type="submit" class="btn btn-primary" onclick="fncGoSave();return false;" style="float: right; text-align: right; margin: 5px 0 0 0;">저장</button>
	</div>
	<!-- /.box-body -->
</div>
</form>