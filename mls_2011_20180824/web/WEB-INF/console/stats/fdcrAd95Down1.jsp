<%@ page language="java" contentType="application/vnd.ms-excel;charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%
	String filename = "법정허락 이용승인 건수 통계.xls";
    
	response.setHeader( "Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "utf-8") + "" );
	response.setHeader( "Content-Description", "JSP Generated Data" );

	//response.setContentType( "application/vnd.ms-excel" );   

	//DataSet divList = (DataSet) box.getObject("EmasDivList");
	//HashMap ageMap = (HashMap) box.getObject("LicenseAgeClass");
%>
	<table border="1" cellpadding="0" cellspacing="0" style="table-layout:fixed;text-align:center" >
		<thead>
			<tr>
				<th style="background-color:#eeeeee;width: 7%">연도</th>
				<th colspan="1" style="background-color:#eeeeee;width: 10%">신청</th>
				<th colspan="2" style="background-color:#eeeeee;width: 7%">진행</th>
				<th colspan="3" style="background-color:#eeeeee;width: 7%">완료</th>

			</tr>
			<tr>
			    <th></th>
			    <th></th>
          <th style="background-color:#eeeeee;">접수</th>
          <th style="background-color:#eeeeee;">심의</th>
          <th style="background-color:#eeeeee;">승인</th>
          <th style="background-color:#eeeeee;">기각</th>
          <th style="background-color:#eeeeee;">기타</th>
        </tr>
		</thead>
		<tbody>
			<c:choose>
					<c:when test="${empty ds_list1}">
						<tr>
							<td colspan="9" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						  <c:forEach var="info" items="${ds_list1}" varStatus="listStatus">
              <tr>
                <td><c:out value="${info.YEAR}" /></td>
                <td><c:out value="${info.CNT1}" /></td>
                <td><c:out value="${info.CNT2}" /></td>
                <td><c:out value="${info.CNT3}" /></td>
                <td><c:out value="${info.CNT4}" /></td>
                <td><c:out value="${info.CNT5}" /></td>
                <td><c:out value="${info.CNT6}" /></td>
              </tr>
            </c:forEach>
					</c:otherwise>
				</c:choose>
		</tbody>
	</table>
