<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncGoSearch(){
		
		var ds_year = $('#ds_year').val();
		var ds_month = $('#ds_month').val();
		
		var YEAR_MONTH = '';
		if(ds_year != '' && ds_month != ''){
			YEAR_MONTH = ds_year+ds_month;
		}else if(ds_year != '' && ds_month == '') {
			YEAR_MONTH = ds_year;
		}else if(ds_year == '' && ds_month == '') {
		}
		
		var f = document.boardForm;
		f.YEAR_MONTH.value=YEAR_MONTH;
		f.action='/console/stats/fdcrAd87List1.page';
		f.submit();
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="YEAR_MONTH" name="YEAR_MONTH" value="<c:out value="${param.YEAR_MONTH}"/>" /> 

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">날짜</th>
									<td colspan="3">
										<console:fn func="getYearSelectBox0" value="${param.ds_year}" value1="ds_year" />
										<console:fn func="getMonthSelectBox" value="${param.ds_month}" value1="ds_month" />
									</td>
								</tr>
							</tbody>
						</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>

<div class="box" style="margin-top: 30px;">
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 10%">날짜</th>
					<th colspan="2">방문자수(그래프)</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.YEARMONTH}" /></td>
								<td style="width: 15%"><c:out value="${info.COUNT_PCNT}" /></td>
								<td><div class="progress xs"><div class="progress-bar progress-bar-green" style="width: <c:out value="${info.USERPCNT}" />%;"></div></div></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>
</form>