<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	$(function () {
		fncGoSearch();
	});
	//검색
	function fncGoSearch() {
		var datas = [];
		datas[0] = 'TRST_ORGN_CODE';
		datas[1] = 'YYYYMM';
		datas[2] = 'GUBUN';
		var url = fncGetBoardParam('<c:url value="/console/stats/fdcrAd97List3.page"/>',datas, 1);
		var TITLE = $('#TITLE').val();
		alert(TITLE);
		if(TITLE != ''){
			TITLE = encodeURI(encodeURIComponent(TITLE));
			url += '&TITLE='+TITLE;
		}
		fncLoad('#TableIdFdcrAd97List3',url,null, function(){
			
		});
	}
	
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		datas[0] = 'TRST_ORGN_CODE';
		datas[1] = 'YYYYMM';
		datas[2] = 'GUBUN';
		var url = fncGetBoardParam('<c:url value="/console/stats/fdcrAd97List3.page"/>',datas, page);
		var TITLE = $('#TITLE').val();
		if(TITLE != ''){
			TITLE = encodeURI(encodeURIComponent(TITLE));
			url += '&TITLE='+TITLE;
		}
		fncLoad('#TableIdFdcrAd97List3',url,null, function(){
			
		});
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="FROM" name="FROM" value="<c:out value="${param.FROM}"/>" /> 
	<input type="hidden" id="TO" name="TO" value="<c:out value="${param.TO}"/>" /> 

<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border">
		<h3 class="box-title">월별 보고현황 조회</h3>
	</div>
	<div class="box-body">
		<table class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 25%"><c:out value="${commandMap.YYYY}"/>년 <c:out value="${commandMap.MM}"/>월</th>
					<th style="width: 25%">위탁관리 저작물 보고</th>
					<th style="width: 25%">보고건수:<c:out value="${ds_report.REPT_WORKS_CONT}"/></th>
					<th style="width: 25%">보고일자:<c:out value="${ds_report.RGST_DTTM}"/></th>								
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>보고기관</th>
					<td colspan="3"><c:out value="${ds_report.COMM_NAME}" /></td>
				</tr>
				<tr>
					<th>담당자</th>
					<td><c:out value="${ds_report.REPT_CHRR_NAME}" /></td>
					<th>담당자 직위</th>
					<td><c:out value="${ds_report.REPT_CHRR_POSI}" /></td>
				</tr>
				<tr>
					<th>담당자 전화번호</th>
					<td><c:out value="${ds_report.REPT_CHRR_TELX}" /></td>
					<th>담당자 이메일</th>
					<td><c:out value="${ds_report.REPT_CHRR_MAIL}" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-body">
		<table class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th>보고건수</th>
					<th>음악</th>
					<th>어문</th>
					<th>방송대본</th>
					<th>영화</th>
					<th>방송</th>
					<th>뉴스</th>
					<th>미술</th>
					<th>이미지</th>
					<th>기타</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th><c:out value="${ds_count.CNT_TOT}"/></th>
					<td><c:out value="${ds_count.CNT_1}"/></td>
					<td><c:out value="${ds_count.CNT_2}"/></td>
					<td><c:out value="${ds_count.CNT_3}"/></td>
					<td><c:out value="${ds_count.CNT_4}"/></td>
					<td><c:out value="${ds_count.CNT_5}"/></td>
					<td><c:out value="${ds_count.CNT_6}"/></td>
					<td><c:out value="${ds_count.CNT_7}"/></td>
					<td><c:out value="${ds_count.CNT_8}"/></td>
					<td><c:out value="${ds_count.CNT_9}"/></td>
					<td><c:out value="${ds_count.CNT_10}"/></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
</form>


<div>
<form name="searchForm" id="searchForm" method="post">
<div class="box box-default">
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
						<input type="hidden" id="TRST_ORGN_CODE" name="TRST_ORGN_CODE" value="${commandMap.TRST_ORGN_CODE}"/>
						<input type="hidden" id="YYYYMM" name="YYYYMM" value="${commandMap.YYYYMM}"/>
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">구분</th>
									<td colspan="2" >
										<select style="width:100px" id="GUBUN" name="GUBUN">
											<option value="00" <console:fn func="isSelected" value="00" value1="${commandMap.GUBUN }" />>전체</option>
											<option value="10" <console:fn func="isSelected" value="10" value1="${commandMap.GUBUN }" />>저작물종류</option>
											<option value="20" <console:fn func="isSelected" value="20" value1="${commandMap.GUBUN }" />>저작물명</option>
											<option value="30" <console:fn func="isSelected" value="30" value1="${commandMap.GUBUN }" />>저작물부제</option>
											<option value="40" <console:fn func="isSelected" value="40" value1="${commandMap.GUBUN }" />>창작년도</option>
										</select>
										<input type="text" id="TITLE" name="TITLE" value="<c:out value="${commandMap.TITLE}"/>"/>
									</td>
									<td style="width:10%;text-align:center">
										<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
									</td>
								</tr>
							</tbody>
						</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</div>
</div>
<div class="box" style="margin-top: 30px;" id="TableIdFdcrAd97List3">
	
</div>
</form>
</div>