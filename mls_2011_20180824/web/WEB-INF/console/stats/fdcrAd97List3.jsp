<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
	<div class="box-header with-border">
		총 <c:out value="${paginationInfo.totalRecordCount}"/>건
	</div>
	<div class="box-body">
		<!-- /.box-header -->
		<table class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th>보고년월</th>
					<th>저작물관리번호</th>
					<th>국내외구분</th>
					<th>저작물분류</th>
					<th>저작물명</th>
					<th>저작물부재</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="6">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.REPT_YMD}" /></td>
								<td><c:out value="${info.COMM_WORKS_ID}" /></td>
								<td><c:out value="${info.NATION_CD}" /></td>
								<td><c:out value="${info.GENRE_CD}" /></td>
								<td><c:out value="${info.WORKS_TITLE}" /></td>
								<td class="text-left"><c:out value="${info.WORKS_SUB_TITLE}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
