<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	$(function () {
	});
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="FROM" name="FROM" value="<c:out value="${param.FROM}"/>" /> 
	<input type="hidden" id="TO" name="TO" value="<c:out value="${param.TO}"/>" /> 
	<input type="hidden" id="GUBUN" name="GUBUN" value="<c:out value="${param.GUBUN}"/>" /> 
	<input type="hidden" id="YEARMONTH" name="YEARMONTH" value="<c:out value="${param.YEARMONTH}"/>" /> 

<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border">
		<h3 class="box-title">법정허락 이용승인 명세서 목록</h3>
	</div>
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 25%">순번</th>
					<th style="width: 25%">신청자구분</th>
					<th style="width: 25%">제호(제목)</th>
					<th style="width: 25%">진행상태</th>								
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td style="width: 15%"><c:out value="${info.RNUM}" /></td>
								<td style="width: 15%"><c:out value="${info.APPLY_TYPE_NM}" /></td>
								<td style="width: 15%"><c:out value="${info.WORKS_TITL}" /></td>
								<td style="width: 15%"><c:out value="${info.STAT_RSLT_CD_NM}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>
</form>