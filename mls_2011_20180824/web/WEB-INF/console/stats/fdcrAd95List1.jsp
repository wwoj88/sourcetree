<%@ page language="java" contentType="text/html; charset=EUC-KR"
  pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
  $(function(){
    $('#datepi').datepicker({
      format: 'yyyymmdd', 
      language:'kr',
      autoclose: true
    }); 
  })

    function fncExcelDown1() {

    var datas = [];
    var url = fncGetBoardParam('<c:url value="/console/stats/fdcrAd95Down1.page"/>',datas, 1);
   
     location.href = url;
  }
  function fncExcelDown2() {

    var datas = [];
    var url = fncGetBoardParam('<c:url value="/console/stats/fdcrAd95Down2.page"/>',datas, 1);
    url += '&YYYYMMDD='+$('#YYYYMMDD').val();
     location.href = url;
  }
  function fncExcelDown3() {

    var datas = [];
    var url = fncGetBoardParam('<c:url value="/console/stats/fdcrAd95Down3.page"/>',datas, 1);
   
     location.href = url;
  }
  //법정허락 이용 승인 명세서 팝업
  function fncStatPrpsApplyList(YEAR, STAT_CD, STAT_RSLT_CD) {
    var popUrl = '<c:url value="/console/stats/fdcrAd95List7.page"/>?YEAR='+YEAR+"&STAT_CD="+STAT_CD+"&STAT_RSLT_CD="+STAT_RSLT_CD;
    var popOption = "width=600, height=600, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
    window.open(popUrl, "fncStatPrpsApplyList", popOption);
  }
  
  function fncLoadFdcrAd95List3(){
    var WORKS_DIVS_CD = $('#WORKS_DIVS_CD').val();
    var YYYYMMDD = $('#YYYYMMDD').val();
    var data = {};
    data.WORKS_DIVS_CD = WORKS_DIVS_CD;
    data.YYYYMMDD = YYYYMMDD;
    fncLoad('#TableIdFdcrAd95List2','<c:out value="/console/stats/fdcrAd95List3.page"/>',data,function(){
      
    });
  }
</script>
<form name="boardForm" id="boardForm" method="post">
<div class="box" style="margin-top: 30px;height:300px;overflow-y:auto;">
  <div class="box-header with-border">
    <h3 class="box-title">법정허락 이용승인 건수 통계</h3>
    <div class="btn-group"><button  class="btn btn-primary btn-xs" onclick="fncExcelDown1();return false;">Excel Down</button></div>
  </div>
  <div class="box-body">
    <table class="table table-bordered table-hover text-center table-list ">
      <thead>
        <tr>
          <th rowspan="2" style="width: 10%">연도</th>
          <th rowspan="2">신청</th>
          <th colspan="2">진행</th>
          <th colspan="3">완료</th>
        </tr>
        <tr>
          <th>접수</th>
          <th>심의</th>
          <th>승인</th>
          <th>기각</th>
          <th>기타</th>
        </tr>
      </thead>
      <tbody>
        <c:choose>
          <c:when test="${empty ds_list1}">
          </c:when>
          <c:otherwise>
            <c:forEach var="info" items="${ds_list1}" varStatus="listStatus">
              <tr>
                <td><c:out value="${info.YEAR}" /></td>
                <td><a href="#" onclick="fncStatPrpsApplyList('<c:out value="${info.YEAR}" />', '0', '');return false;"><c:out value="${info.CNT1}" /></a></td>
                <td><a href="#" onclick="fncStatPrpsApplyList('<c:out value="${info.YEAR}" />', '8', '');return false;"><c:out value="${info.CNT2}" /></a></td>
                <td><a href="#" onclick="fncStatPrpsApplyList('<c:out value="${info.YEAR}" />', '9', '');return false;"><c:out value="${info.CNT3}" /></a></td>
                <td><a href="#" onclick="fncStatPrpsApplyList('<c:out value="${info.YEAR}" />', '10', '2');return false;"><c:out value="${info.CNT4}" /></a></td>
                <td><a href="#" onclick="fncStatPrpsApplyList('<c:out value="${info.YEAR}" />', '10', '3');return false;"><c:out value="${info.CNT5}" /></a></td>
                <td><a href="#" onclick="fncStatPrpsApplyList('<c:out value="${info.YEAR}" />', '10', '4');return false;"><c:out value="${info.CNT6}" /></a></td>
              </tr>
            </c:forEach>
          </c:otherwise>
        </c:choose>
      </tbody>
    </table>
    <div>(* 단위 건, 이용승인 명세서 건수)</div>
  </div>
</div>



<div>
  <div class="box" style="margin-top: 30px;width:50%;height:400px;overflow-y:auto;float:left;padding:10px">
    <div class="box-header with-border">
      <h3 class="box-title">법정허락 대상 저작물 현황</h3>
      <div class="btn-group"><button type="submit" class="btn btn-primary btn-xs" onclick="fncExcelDown2();return false;">Excel Down</button></div>
    </div>
    <div class="box-body">
      <table class="table table-bordered">
        <tbody>
          <tr>
            <td class="text-center search-th">
              <select name="WORKS_DIVS_CD" id="WORKS_DIVS_CD" onchange="fncLoadFdcrAd95List3();return false;">
                <option value="">-전체-</option>
                <option value="1" <console:fn func="isSelected" value="1" value1="${param.ds_divs}"/>>미분배보상금 대상 저작물</option>
                <option value="4" <console:fn func="isSelected" value="4" value1="${param.ds_divs}"/>>거소불명 저작물</option>
              </select>
            </td>
            <td>
              <div style="float:left;height:34px;line-height:34px;">기준 : </div><div class="input-group date" id="datepi" style="width:150px;">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right"  id="YYYYMMDD" name="YYYYMMDD" value="${commandMap.YYYYMMDD }"/>
                      </div>
            </td>
          </tr>
        </tbody>
      </table>
      <table id="TableIdFdcrAd95List2" class="table table-bordered table-hover text-center table-list ">
        <thead>
          <tr>
            <th>장르</th>
            <th>출처</th>
            <th>법정허락 대상 저작물</th>
          </tr>
        </thead>
        <tbody>
          <c:choose>
            <c:when test="${empty ds_list2}">
            </c:when>
            <c:otherwise>
              <c:forEach var="info" items="${ds_list2}" varStatus="listStatus">
                <tr>
                  <td><c:out value="${info.GENRE}" /></td>
                  <td><c:out value="${info.INMT}" /></td>
                  <td><console:fn func="formatNumber" value="${info.CNT}"/></td>
                </tr>
              </c:forEach>
            </c:otherwise>
          </c:choose>
        </tbody>
      </table>
      <div>(* 단위 건, 저작물 건수)</div>
    </div>
  </div>
  <div class="box" style="margin-top: 30px;width:50%;height:400px;overflow-y:auto;float:right;padding:10px">
    <div class="box-header with-border">
      <h3 class="box-title">보상금 공탁서 공고 건수 통계</h3>
      <div class="btn-group"><button type="submit" class="btn btn-primary btn-xs" onclick="fncExcelDown3();return false;">Excel Down</button></div>
    </div>
    <div class="box-body">
      <table class="table table-bordered table-hover text-center table-list ">
        <thead>
          <tr>
            <th>연도</th>
            <th>보상금공탁서</th>
          </tr>
        </thead>
        <tbody>
          <c:choose>
            <c:when test="${empty ds_list3}">
            </c:when>
            <c:otherwise>
              <c:forEach var="info" items="${ds_list3}" varStatus="listStatus">
                <tr>
                  <td><c:out value="${info.YEAR}" /></td>
                  <td><c:out value="${info.CNT}" /></td>
                </tr>
              </c:forEach>
            </c:otherwise>
          </c:choose>
        </tbody>
      </table>
      <div>(* 단위 건, 보상금 공탁 건수)</div>
    </div>
  </div>
</div>




<%-- 현재 대기중 상당한노력
 <div class="box" style="margin-top: 30px;height:300px;overflow-y:auto;">
  <div class="box-header with-border">
    <h3 class="box-title">상당한노력 신청 건수 통계</h3>
    <div class="btn-group"><button type="submit" class="btn btn-primary btn-xs" onclick="fncGoWriteForm();return false;">Excel Down</button></div>
  </div>
  <div class="box-body">
    <table class="table table-bordered table-hover text-center table-list ">
      <thead>
        <tr>
          <th>연도</th>
          <th>상단항노력 신청공고</th>
          <th>1분기</th>
          <th>2분기</th>
          <th>3분기</th>
          <th>4분기</th>
          <th>계</th>
        </tr>
      </thead>
      <tbody>
        <c:choose>
          <c:when test="${empty ds_list4}">
          </c:when>
          <c:otherwise>
            <c:forEach var="info" items="${ds_list4}" varStatus="listStatus">
              <tr <c:if test="${listStatus.count%2==0 }">style="background:#eeeeee"</c:if>>
                <c:if test="${!empty info.ROWS}">
                <td rowspan="<c:out value="${info.ROWS}"/>" style="background:#ffffff"><c:out value="${info.YEAR}"/></td>
                </c:if>
                <td><c:out value="${info.DIVS_NAME}" /></td>
                <td><c:out value="${info.CNT1}" /></td>
                <td><c:out value="${info.CNT2}" /></td>
                <td><c:out value="${info.CNT3}" /></td>
                <td><c:out value="${info.CNT4}" /></td>
                <td><c:out value="${info.SUM_CNT}" /></td>
              </tr>
            </c:forEach>
          </c:otherwise>
        </c:choose>
      </tbody>
    </table>
    <div>(* 단위 건, 신청 건수)</div>
  </div>
</div>


<div class="box" style="margin-top: 30px;height:300px;overflow-y:auto;">
  <div class="box-header with-border">
    <h3 class="box-title">상당한노력 신청 유형별 통계</h3>
    <div class="btn-group"><button type="submit" class="btn btn-primary btn-xs" onclick="fncGoWriteForm();return false;">Excel Down</button></div>
  </div>
  <div class="box-body">
    <table class="table table-bordered table-hover text-center table-list ">
      <thead>
        <tr>
          <th>연도</th>
          <th>상단항노력 신청공고</th>
          <th>어문</th>
          <th>음악</th>
          <th>연극</th>
          <th>미술</th>
          <th>건축</th>
          <th>사진</th>
          <th>영상</th>
          <th>도형</th>
          <th>컴퓨터프로그램</th>
          <th>편집</th>
          <th>2차적저작물</th>
          <th>기타</th>
          <th>계</th>
        </tr>
      </thead>
      <tbody>
        <c:choose>
          <c:when test="${empty ds_list5}">
          </c:when>
          <c:otherwise>
            <c:forEach var="info" items="${ds_list5}" varStatus="listStatus">
              <tr <c:if test="${listStatus.count%2==0 }">style="background:#eeeeee"</c:if>>
                <c:if test="${!empty info.ROWS}">
                <td rowspan="<c:out value="${info.ROWS}"/>" style="background:#ffffff"><c:out value="${info.YEAR}" /></td>
                </c:if>
                <td><c:out value="${info.DIVS_NAME}" /></td>
                <td><c:out value="${info.CNT1}" /></td>
                <td><c:out value="${info.CNT2}" /></td>
                <td><c:out value="${info.CNT3}" /></td>
                <td><c:out value="${info.CNT4}" /></td>
                <td><c:out value="${info.CNT5}" /></td>
                <td><c:out value="${info.CNT6}" /></td>
                <td><c:out value="${info.CNT7}" /></td>
                <td><c:out value="${info.CNT8}" /></td>
                <td><c:out value="${info.CNT9}" /></td>
                <td><c:out value="${info.CNT10}" /></td>
                <td><c:out value="${info.CNT11}" /></td>
                <td><c:out value="${info.CNT12}" /></td>
                <td><c:out value="${info.SUM_CNT}" /></td>
              
            </c:forEach>
          </c:otherwise>
        </c:choose>
      </tbody>
    </table>
    <div>(* 단위 건, 신청 건수)</div>
  </div> --%>
</div>
</form>