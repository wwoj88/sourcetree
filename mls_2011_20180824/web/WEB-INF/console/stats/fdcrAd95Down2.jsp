<%@ page language="java" contentType="application/vnd.ms-excel;charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%
	String filename = "법정허락_대상_저작물_현황.xls";
    
	response.setHeader( "Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "utf-8") + "" );
	response.setHeader( "Content-Description", "JSP Generated Data" );

	//response.setContentType( "application/vnd.ms-excel" );   

	//DataSet divList = (DataSet) box.getObject("EmasDivList");
	//HashMap ageMap = (HashMap) box.getObject("LicenseAgeClass");
%>
	<table border="1" cellpadding="0" cellspacing="0" style="table-layout:fixed;text-align:center" >
		<thead>
			<tr>
				<th colspan="1" style="background-color:#eeeeee;width: 10%">장르</th>
				<th colspan="3" style="background-color:#eeeeee;width: 7%">출처</th>
				<th colspan="5" style="background-color:#eeeeee;width: 7%">법정허락 대상 저작물</th>

			</tr>
			 
        <tbody>
          <c:choose>
            <c:when test="${empty ds_list2}">
                <tr>
              <td colspan="9" class="text-center">게시물이 없습니다.</td>
            </tr>
            </c:when>
            <c:otherwise>
              <c:forEach var="info" items="${ds_list2}" varStatus="listStatus">
                <tr>
                  <td colspan="1"><c:out value="${info.GENRE}" /></td>
                  <td colspan="3"><c:out value="${info.INMT}" /></td>
                  <td colspan="5"><fmt:formatNumber value="${info.CNT}" pattern="###,###,###,##0" groupingUsed="true" /></td>
                </tr>
              </c:forEach>
            </c:otherwise>
          </c:choose>
		</tbody>
	</table>
