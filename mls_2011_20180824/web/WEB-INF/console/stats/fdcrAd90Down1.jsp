<%@ page language="java" contentType="application/vnd.ms-excel;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%
	String from = (String)request.getParameter("FROM");
	String to = (String)request.getParameter("TO");
	String gubun = (String)request.getParameter("GUBUN");
	String gubunNm = "";
	if(null!=gubun && !"".equals(gubun)){
		if("0".equals(gubun)){
			gubunNm = "사이트";
		}else if("1".equals(gubun)){
			gubunNm = "모바일(저작권찾기)";
		}else if("2".equals(gubun)){
			gubunNm = "모바일(거래소)";
		}
	}
	String filename = "기간별 접속 통계_"+gubunNm+"("+from+"~"+to+").xls";
    
	response.setHeader( "Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "utf-8") + "" );
	response.setHeader( "Content-Description", "JSP Generated Data" );

	//response.setContentType( "application/vnd.ms-excel" );   

	//DataSet divList = (DataSet) box.getObject("EmasDivList");
	//HashMap ageMap = (HashMap) box.getObject("LicenseAgeClass");
%>
		<table border="1" cellpadding="0" cellspacing="0" style="table-layout:fixed;text-align:center" >
			<thead>
				<tr>
					<th style="background-color:#eeeeee;width: 10%">년도</th>
					<th style="background-color:#eeeeee;width: 10%">월</th>
					<th style="background-color:#eeeeee;width: 10%">접속자수</th>								
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td style="width: 15%"><c:out value="${info.YEAR}" /></td>
								<td style="width: 15%"><c:out value="${info.YEARMONTH}" /></td>
								<td style="width: 15%">
								<c:choose>
									<c:when test="${!empty info.YEARMONTH_PARAM && info.YEARMONTH_PARAM != '합계' }">
									<a href="javascript:fncPopupMonthStat('<c:out value="${info.YEARMONTH_PARAM}" />');"><c:out value="${info.COUNT_PCNT}"/></a>
									</c:when>
									<c:otherwise>
									<c:out value="${info.COUNT_PCNT}"/>
									</c:otherwise>
								</c:choose>
								</td>
							</tr>
						</c:forEach>
							<tr>
								<td style="background-color:#eeeeee;width: 15%" colspan="2"><b>총 합계</b></td>
								<td style="background-color:#eeeeee;width: 15%"><b><c:out value="${TOTAL}" />명</b></td>
							</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>