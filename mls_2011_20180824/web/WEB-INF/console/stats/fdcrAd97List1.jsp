<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	$(function () {
		$('#YYYY').change(function() {
			var YYYY = $('#YYYY').val();
			location.href='<c:out value="/console/stats/fdcrAd97List1.page"/>?YYYY='+YYYY;
		});
	});
	
	//대상저작물 팝업
	function fncPopupFdcrList2(TRST_ORGN_CODE, YYYYMM) {
		var popUrl = '<c:url value="/console/stats/fdcrAd97List2.page"/>?TRST_ORGN_CODE='+TRST_ORGN_CODE+"&YYYYMM="+YYYYMM;
		var popOption = "width=900, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "fncPopupMonthStat", popOption);
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="FROM" name="FROM" value="<c:out value="${param.FROM}"/>" /> 
	<input type="hidden" id="TO" name="TO" value="<c:out value="${param.TO}"/>" /> 
	<input type="hidden" id="PARAM_FROM" name="PARAM_FROM" value="<c:out value="${param.PARAM_FROM}"/>" /> 
	<input type="hidden" id="PARAM_TO" name="PARAM_TO" value="<c:out value="${param.PARAM_TO}"/>" /> 
<div class="box" style="margin-top:30px;">
	<div class="box-body">
		<div style="height:30px;line-height:30px;">
			<div style="float:left;height:25px;">* 저작물 건수를 더블클릭하면 보고현황정보가 조회됩니다.</div> 
			<div style="float:right;height:25px;">년도 : <console:fn func="getYearSelectBox" value="${commandMap.YYYY}" value1="YYYY" /></div>
		</div>
		<div style="height:600px;overflow-y:auto;">
		<table style="width:1500px;" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 120px" rowspan="2">신고번호</th>
					<th style="width: 250px" rowspan="2">업체명</th>
					<th style="width: 1130px;" colspan="12"><c:out value="${commandMap.YYYY}"/></th>								
				</tr>
				<tr>
					<th>1</th>								
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th>6</th>
					<th>7</th>
					<th>8</th>
					<th>9</th>
					<th>10</th>
					<th>11</th>
					<th>12</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.REPT_NO}" /></td>
								<td><c:out value="${info.CONAME}" /></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>01');"><c:out value="${info.M01}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>02');"><c:out value="${info.M02}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>03');"><c:out value="${info.M03}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>04');"><c:out value="${info.M04}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>05');"><c:out value="${info.M05}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>06');"><c:out value="${info.M06}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>07');"><c:out value="${info.M07}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>08');"><c:out value="${info.M08}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>09');"><c:out value="${info.M09}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>10');"><c:out value="${info.M10}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>11');"><c:out value="${info.M11}" /></a></td>
								<td><a href="#" onclick="fncPopupFdcrList2('<c:out value="${info.CA_ID}"/>','<c:out value="${commandMap.YYYY}"/>12');"><c:out value="${info.M12}" /></a></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		</div>
	</div>
	<div style="float:clear;height:40px;line-height:40px;">
		<div style="float:left;">(* 단위 건, 신청건수/수정건수)</div>
		<div style="float:right;">
			<div class="btn-group"><button type="button" class="btn btn-block btn-primary btn-xs" style="width:150px;" onclick="fncLoadfdcrAd03List1Proc();return false;">담당자 email복사하기</button></div>
			<div class="btn-group"><button type="button" class="btn btn-block btn-primary btn-xs" style="width:150px;" onclick="fncLoadfdcrAd03List1Proc();return false;">Excel Down</button></div>
		</div>
	</div>
</div>
</form>