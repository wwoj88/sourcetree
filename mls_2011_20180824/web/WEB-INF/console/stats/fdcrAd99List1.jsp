<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncGoSearch(){
		var f = document.boardForm;
		f.action='/console/stats/fdcrAd99List1.page';
		f.submit();
	}
</script>
<form name="boardForm" id="boardForm" method="post">

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">날짜</th>
									<td colspan="3">
										<console:fn func="getYearSelectBox" value="${param.YYYY}" value1="YYYY" value2="Y" />
									</td>
								</tr>
							</tbody>
						</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>

<div class="box" style="margin-top: 30px;">
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 10%">년월</th>
					<th>등록건수</th>
					<th>최종등록일자</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.YYYYMM}" /></td>
								<td><console:fn func="formatNumber" value="${info.WORKS_CONT}"/></td>
								<td><c:out value="${info.LST_RGST_DTTM}" /></td>
							</tr>
						</c:forEach>
							<tr>
								<th>합계</th>
								<th><console:fn func="formatNumber" value="${TOTAL}"/></th>
								<th></th>
							</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		<div>(* 단위 건, 등록저작물건수)</div>
	</div>
</div>
</form>