<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	$(function () {
	});
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="FROM" name="FROM" value="<c:out value="${param.FROM}"/>" /> 
	<input type="hidden" id="TO" name="TO" value="<c:out value="${param.TO}"/>" /> 
	<input type="hidden" id="GUBUN" name="GUBUN" value="<c:out value="${param.GUBUN}"/>" /> 
	<input type="hidden" id="YEARMONTH" name="YEARMONTH" value="<c:out value="${param.YEARMONTH}"/>" /> 

<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border">
		<h3 class="box-title">월별 접속자수 목록</h3>
	</div>
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="return false;">엑셀다운로드</button></div>
	</div>
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 25%">년도</th>
					<th style="width: 25%">월</th>
					<th style="width: 25%">일</th>
					<th style="width: 25%">접속자수</th>								
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td style="width: 15%"><c:out value="${info.YEAR}" /></td>
								<td style="width: 15%"><c:out value="${info.MONTH}" /></td>
								<td style="width: 15%"><c:out value="${info.DAY}" /></td>
								<td style="width: 15%"><c:out value="${info.COUNT_PCNT}" /></td>
							</tr>
						</c:forEach>
							<tr>
								<td style="width: 15%" colspan="3"><b>총 합계</b></td>
								<td style="width: 15%"><b><c:out value="${TOTAL}" />명</b></td>
							</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>
</form>