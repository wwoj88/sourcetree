<%@ page language="java" contentType="application/vnd.ms-excel;charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%
	String filename = "보상금_공탁서_공고_건수.xls";
    
	response.setHeader( "Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "utf-8") + "" );
	response.setHeader( "Content-Description", "JSP Generated Data" );

%>
	<table border="1" cellpadding="0" cellspacing="0" style="table-layout:fixed;text-align:center" >
		<thead>
			<tr>
				<th colspan="3" style="background-color:#eeeeee;width: 10%">연도</th>
				<th colspan="6" style="background-color:#eeeeee;width: 7%">보상금 공탁서</th>
		

			</tr>
			 
        <tbody>
         
          <c:choose>
            <c:when test="${empty ds_list3}">
            </c:when>
            <c:otherwise>
              <c:forEach var="info" items="${ds_list3}" varStatus="listStatus">
                <tr>
                  <td colspan="3"><c:out value="${info.YEAR}" /></td>
                  <td colspan="6"><c:out value="${info.CNT}" /></td>
                </tr>
              </c:forEach>
            </c:otherwise>
          </c:choose>
		</tbody>
	</table>
