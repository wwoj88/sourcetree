<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
				<thead>
					<tr>
						<th>장르</th>
						<th>출처</th>
						<th>법정허락 대상 저작물</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty ds_list2}">
						</c:when>
						<c:otherwise>
							<c:forEach var="info" items="${ds_list2}" varStatus="listStatus">
								<tr>
									<td><c:out value="${info.GENRE}" /></td>
									<td><c:out value="${info.INMT}" /></td>
									<td><console:fn func="formatNumber" value="${info.CNT}"/></td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>