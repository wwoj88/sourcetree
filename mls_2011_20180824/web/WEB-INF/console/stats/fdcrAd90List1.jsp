<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	$(function () {
		var start = moment().subtract(29, 'days');
	    var end = moment();
	    
	    var paramStart = "<c:out value="${param.PARAM_FROM}"/>";
	    var paramEnd = "<c:out value="${param.PARAM_TO}"/>";
	    if(paramStart!=''){
	    	start = paramStart;
	    }
	    
	    if(paramEnd!=''){
	    	end = paramEnd;
	    }
	    
		$('#term').daterangepicker(
		{
			format: 'YYYY.MM.DD', language:'kr',
			startDate: start,
		    endDate: end
		}, 
		function(start, end, label) {
			$('#PARAM_FROM').val(start.format('YYYY.MM.DD'));
			$('#PARAM_TO').val(end.format('YYYY.MM.DD'));
			
			$('#FROM').val(start.format('YYYYMMDD'));
			$('#TO').val(end.format('YYYYMMDD'));
		});
	});
	
	function fncGoSearch(){
		var f = document.boardForm;
		f.DATE.value=YEAR_MONTH;
		f.action='/console/stats/fdcrAd88List1.page';
		f.submit();
	}
	
	//대상저작물 팝업
	function fncPopupMonthStat(YEARMONTH) {
		var FROM = $('#FROM').val();
		var TO = $('#TO').val();
		var GUBUN = $('#GUBUN').val();
		 // 검색구분 (0:전체, 1:예외처리대상)
		var popUrl = '<c:url value="/console/stats/fdcrAd90List2.page"/>?FROM='+FROM+"&TO="+TO+"&GUBUN="+GUBUN+"&YEARMONTH="+YEARMONTH;
		var popOption = "width=600, height=500, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "fncPopupMonthStat", popOption);
	}
	
	
	function fncExcelDown1(){
		var FROM = $('#FROM').val();
		var TO = $('#TO').val();
		var GUBUN = $('#GUBUN').val();
		location.href='<c:url value="/console/stats/fdcrAd90Down1.page"/>?FROM='+FROM+"&TO="+TO+"&GUBUN="+GUBUN;
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="FROM" name="FROM" value="<c:out value="${param.FROM}"/>" /> 
	<input type="hidden" id="TO" name="TO" value="<c:out value="${param.TO}"/>" /> 
	<input type="hidden" id="PARAM_FROM" name="PARAM_FROM" value="<c:out value="${param.PARAM_FROM}"/>" /> 
	<input type="hidden" id="PARAM_TO" name="PARAM_TO" value="<c:out value="${param.PARAM_TO}"/>" /> 
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">구분</th>
									<td>
										<select id="GUBUN" name="GUBUN">
											<option value="0" <console:fn func="isSelected" value="0" value1="${param.GUBUN}"/>>사이트</option>
											<%-- <option value="1" <console:fn func="isSelected" value="1" value1="${param.GUBUN}"/>>모바일(저작권찾기)</option>
											<option value="2" <console:fn func="isSelected" value="2" value1="${param.GUBUN}"/>>모바일(거래소)</option> --%>
										</select>
									</td>
									<th class="text-center search-th">기간</th>
									<td>
										<div class="input-group">
						                  <div class="input-group-addon">
						                    <i class="fa fa-calendar"></i>
						                  </div>
						                  <input type="text" class="form-control pull-right" id="term" value="">
						                </div>
									</td>
								</tr>
							</tbody>
						</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>

<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
		<!-- <div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncExcelDown1();return false;">Excel Down</button></div> -->
	</div>
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 10%">년도</th>
					<th style="width: 10%">월</th>
					<th style="width: 10%">접속자수</th>								
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td style="width: 15%"><c:out value="${info.YEAR}" /></td>
								<td style="width: 15%"><c:out value="${info.YEARMONTH}" /></td>
								<td style="width: 15%">
								<c:choose>
									<c:when test="${!empty info.YEARMONTH_PARAM && info.YEARMONTH_PARAM != '합계' }">
									<a href="javascript:fncPopupMonthStat('<c:out value="${info.YEARMONTH_PARAM}" />');"><c:out value="${info.COUNT_PCNT}"/></a>
									</c:when>
									<c:otherwise>
									<c:out value="${info.COUNT_PCNT}"/>
									</c:otherwise>
								</c:choose>
								</td>
							</tr>
						</c:forEach>
							<tr>
								<td style="width: 15%" colspan="2"><b>총 합계</b></td>
								<td style="width: 15%"><b><c:out value="${TOTAL}" />명</b></td>
							</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>
</form>