<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	
	//검색
	function fncGoSearch() {
		var GUBUN = $('#GUBUN').val();
		var TITLE = $('#TITLE').val();
		
		/* 
		if(GUBUN == "1"){
			document.getElementById('fdcrAd42Pop1').style.display  = 'block';
		}else if(GUBUN == "2"){
			document.getElementById('fdcrAd42Pop1').style.display  = 'none';
			document.getElementById('fdcrAd42Pop2').style.display  = 'block';
		}else if(GUBUN == "3"){
			document.getElementById('fdcrAd42Pop1').style.display  = 'none';
			document.getElementById('fdcrAd42Pop3').style.display  = 'block';
		}else if(GUBUN == "4"){
			document.getElementById('fdcrAd42Pop1').style.display  = 'none';
			document.getElementById('fdcrAd42Pop4').style.display  = 'block';
		}else if(GUBUN == "5"){
			document.getElementById('fdcrAd42Pop1').style.display  = 'none';
			document.getElementById('fdcrAd42Pop5').style.display  = 'block';
		}else{
			document.getElementById('fdcrAd42Pop1').style.display  = 'none';
			document.getElementById('fdcrAd42Pop6').style.display  = 'block';
		}
		 */
		var url = fncGetBoardParam('<c:url value="/console/legalmanage/fdcrAd42List2.page"/>')+'&GUBUN='+GUBUN+'&TITLE='+TITLE;
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(CR_ID,TITLE,ICN,GENRE){
		/* 
		var returnValue = {
		    key1: CR_ID,
		    key2: TITLE,
		    key3: ICN
		  };
		  window.opener.getReturnValue(JSON.stringify(returnValue));
		  window.close();
		  */
		 var key1 = CR_ID;
		 var key2 = TITLE;
		 var key3 = ICN;
		 var key4 = GENRE;
		 opener.SetValue(key1,key2,key3,key4);
		 window.close();
		  
	}
	
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
<div style="margin: 7px 0 15px 5px;"><font size="3">● CLMS 저작물 정보 목록</font></div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group" style="width:100%">
						<span class="input-group-addon" style="width:100px;"> <label>구분</label></span>
						<span class="input-group-addon" style="width:100px;"> 
							<select
							class="form-control" style="width:150px" id="GUBUN" name="GUBUN">
								<%-- 
								<option value="1" <c:if test="${GUBUN eq '1'}">selected="selected"</c:if>>음악</option>
								<option value="2" <c:if test="${GUBUN eq '2'}">selected="selected"</c:if>>도서</option>
								<option value="3" <c:if test="${GUBUN eq '3'}">selected="selected"</c:if>>방송대본</option>
								<option value="4" <c:if test="${GUBUN eq '4'}">selected="selected"</c:if>>방송</option>
								<option value="5" <c:if test="${GUBUN eq '5'}">selected="selected"</c:if>>영화</option>
								<option value="6" <c:if test="${GUBUN eq '6'}">selected="selected"</c:if>>이미지</option>
								 --%>
								<option value="1" selected="selected">음악</option>
								<option value="2">도서</option>
								<option value="3">방송대본</option>
								<option value="4">방송</option>
								<option value="5">영화</option>
								<option value="6">이미지</option>
							</select>
						</span> 
						<span class="input-group-addon" style="width:20%;"> <label>제목</label></span>
						<span class="input-group-addon" style="width:30%;"> 
							<input type="text" class="form-control" id="TITLE" name="TITLE"/>
						</span> 
					</div>
					<button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;" style="float: right; margin-top: 10px;">검색</button>
				</div>
			</div>
			<!-- /.row -->
		</div>
		
		<!-- /.box-body -->
	</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
			<table id="fdcrAd42Pop1" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">제목</th>
					<th class="text-center" style="width: 20%">앨범명</th>
					<th class="text-center" style="width: 15%">ICN</th>
				</tr>
			</thead>
			<tbody>
			<c:choose>
			<c:when test="${empty ds_list}">
				<tr>
					<td colspan="9" class="text-center">게시물이 없습니다.</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
				<tr>
					<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
					<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
					<td class="text-center"><c:out value="${info.GUBUN}"/></td>
					<td class="text-center"><c:out value="${info.ICN }"/></td>
				</tr>
				</c:forEach>
			</c:otherwise>
			</c:choose>
			</tbody>
			<%-- 
			</c:if>
			<c:if test="${strGubun eq '2' }">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">제목</th>
					<th class="text-center" style="width: 20%">수록도서명</th>
					<th class="text-center" style="width: 15%">ICN</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
					<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
					<td class="text-center"><c:out value="${info.GUBUN}"/></td>
					<td class="text-center"><c:out value="${info.ICN }"/></td>
				</tr>
			</tbody>
			</c:if>
			<c:if test="${strGubun eq '3' }">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">제목</th>
					<th class="text-center" style="width: 20%">부제목</th>
					<th class="text-center" style="width: 15%">ICN</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
					<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
					<td class="text-center"><c:out value="${info.GUBUN}"/></td>
					<td class="text-center"><c:out value="${info.ICN }"/></td>
				</tr>
			</tbody>
			</c:if>
			<c:if test="${strGubun eq '4' }">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">제목</th>
					<th class="text-center" style="width: 20%">앨범명</th>
					<th class="text-center" style="width: 15%">ICN</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
					<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
					<td class="text-center"><c:out value="${info.GUBUN}"/></td>
					<td class="text-center"><c:out value="${info.ICN }"/></td>
				</tr>
			</tbody>
			</c:if>
			<c:if test="${strGubun eq '5' }">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">제목</th>
					<th class="text-center" style="width: 20%">앨범명</th>
					<th class="text-center" style="width: 15%">ICN</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
					<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
					<td class="text-center"><c:out value="${info.GUBUN}"/></td>
					<td class="text-center"><c:out value="${info.ICN }"/></td>
				</tr>
			</tbody>
			</c:if>
			<c:if test="${strGubun eq '6' }">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">제목</th>
					<th class="text-center" style="width: 20%">앨범명</th>
					<th class="text-center" style="width: 15%">ICN</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
					<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
					<td class="text-center"><c:out value="${info.GUBUN}"/></td>
					<td class="text-center"><c:out value="${info.ICN }"/></td>
				</tr>
			</tbody>
			</c:if>
			 --%>
		</table>
		<%-- 
		<table id="fdcrAd42Pop2" class="table table-bordered table-hover" style="display: none;">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">제목</th>
					<th class="text-center" style="width: 20%">수록도서명</th>
					<th class="text-center" style="width: 15%">ICN</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="4" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
								<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
								<td class="text-center"><c:out value="${info.GUBUN}"/></td>
								<td class="text-center"><c:out value="${info.ICN }"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<table id="fdcrAd42Pop3" class="table table-bordered table-hover" style="display: none;">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">제목</th>
					<th class="text-center" style="width: 20%">부제목</th>
					<th class="text-center" style="width: 15%">ICN</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="4" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
								<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
								<td class="text-center"><c:out value="${info.GUBUN}"/></td>
								<td class="text-center"><c:out value="${info.ICN }"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<table id="fdcrAd42Pop4" class="table table-bordered table-hover" style="display: none;">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">프로그램명</th>
					<th class="text-center" style="width: 20%">방송일자</th>
					<th class="text-center" style="width: 15%">ICN</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="4" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
								<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
								<td class="text-center"><c:out value="${info.GUBUN}"/></td>
								<td class="text-center"><c:out value="${info.ICN }"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<table id="fdcrAd42Pop5" class="table table-bordered table-hover" style="display: none;">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">저작물명</th>
					<th class="text-center" style="width: 10%">제작사</th>
					<th class="text-center" style="width: 10%">투자사</th>
					<th class="text-center" style="width: 10%">배급사</th>
					<th class="text-center" style="width: 10%">매체</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="6" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
								<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
								<td class="text-center"><c:out value="${info.GUBUN}"/></td>
								<td class="text-center"><c:out value="${info.ICN }"/></td>
								<td class="text-center"><c:out value="${info.ICN }"/></td>
								<td class="text-center"><c:out value="${info.ICN }"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<table id="fdcrAd42Pop6" class="table table-bordered table-hover" style="display: none;">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center">저작물명</th>
					<th class="text-center" style="width: 20%">출판사명</th>
					<th class="text-center" style="width: 15%">분류</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="4" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
								<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.CR_ID}"/>','<c:out value="${info.TITLE}"/>','<c:out value="${info.ICN}"/>','<c:out value="${info.GENRE}"/>');return false;"><c:out value="${info.TITLE }"/></a></td>
								<td class="text-center"><c:out value="${info.GUBUN}"/></td>
								<td class="text-center"><c:out value="${info.ICN }"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		 --%>
		
	</div>
	<!-- /.box-body -->
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
	</div>
</div>