<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/legalmanage/fdcrAd42List1.page"/>',datas, page);
		location.href = url;
	}
	//검색
	function fncGoSearch() {
	
		var GUBUN = $('#GUBUN').val();
		var TITLE = $('#TITLE').val();
		var CLMS_TITLE = $('#CLMS_TITLE').val();
		var FILD_1 = $('#FILD_1').val();
		var FILD_2 = $('#FILD_2').val();
		var FILD_3 = $('#FILD_3').val();
		var FILD_4 = $('#FILD_4').val();
		var FILD_5 = $('#FILD_5').val();
		var FILD_6 = $('#FILD_6').val();
		var FILD_7 = $('#FILD_7').val();
		var FILD_8 = $('#FILD_8').val();
		var FILD_9 = $('#FILD_9').val();
		var FILD_10 = $('#FILD_10').val();
		var FILD_11 = $('#FILD_11').val();
		var FILD_99 = $('#FILD_99').val();
		var FILD = "0";
		
		var test1Result = $("#FILD_1").prop("checked") ;
		var test2Result = $("#FILD_2").prop("checked") ;
		var test3Result = $("#FILD_3").prop("checked") ;
		var test4Result = $("#FILD_4").prop("checked") ;
		var test5Result = $("#FILD_5").prop("checked") ;
		var test6Result = $("#FILD_6").prop("checked") ;
		var test7Result = $("#FILD_7").prop("checked") ;
		var test8Result = $("#FILD_8").prop("checked") ;
		var test9Result = $("#FILD_9").prop("checked") ;
		var test10Result = $("#FILD_10").prop("checked") ;
		var test11Result = $("#FILD_11").prop("checked") ;
		var test99Result = $("#FILD_99").prop("checked") ;
		
		alert(test1Result);
		if(test1Result == true){
			FILD = "1";
			FILD_1 = "1";
		}
		if(test2Result == true){
			FILD = "1";
			FILD_2 = "1";
		}
		if(test3Result == true){
			FILD = "1";
			FILD_3 = "1";
		}
		if(test4Result == true){
			FILD = "1";
			FILD_4 = "1";
		}
		if(test5Result == true){
			FILD = "1";
			FILD_5 = "1";
		}
		if(test6Result == true){
			FILD = "1";
			FILD_6 = "1";
		}
		if(test7Result == true){
			FILD = "1";
			FILD_7 = "1";
		}
		if(test8Result == true){
			FILD = "1";
			FILD_8 = "1";
		}
		if(test9Result == true){
			FILD = "1";
			FILD_9 = "1";
		}
		if(test10Result == true){
			FILD = "1";
			FILD_10 = "1";
		}
		if(test11Result == true){
			FILD = "1";
			FILD_11 = "1";
		}
		if(test99Result == true){
			FILD = "1";
			FILD_99 = "1";
		}
		
		var url = fncGetBoardParam('<c:url value="/console/legalmanage/fdcrAd42List1.page"/>')+'&GUBUN='+GUBUN+'&TITLE='+TITLE+'&CLMS_TITLE='+CLMS_TITLE
		+'&FILD='+FILD+'&FILD_1='+FILD_1+'&FILD_2='+FILD_2+'&FILD_3='+FILD_3+'&FILD_4='+FILD_4+'&FILD_5='+FILD_5+'&FILD_6='+FILD_6
		+'&FILD_7='+FILD_7+'&FILD_8='+FILD_8+'&FILD_9='+FILD_9+'&FILD_10='+FILD_10+'&FILD_11='+FILD_11+'&FILD_99='+FILD_99;
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(BORD_SEQN,MENU_SEQN){
		//var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/legalmanage/fdcrAd42UpdateForm1.page"/>')+'&BORD_SEQN='+BORD_SEQN+'&MENU_SEQN='+MENU_SEQN;
		location.href = url;
	}
	
	//등록폼 이동
	function fncGoWriteForm(){
		var url = '<c:url value="/console/legalmanage/fdcrAd42WriteForm1.page"/>';
		location.href = url;
	} 
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group" style="width:100%">
						<span class="input-group-addon" style="width:20%;"> <label>구분</label></span>
						<span class="input-group-addon" style="width:30%;"> 
							<select
							class="form-control" style="width:100px" id="GUBUN" name="GUBUN">
								<option value="" selected="selected">-전체-</option>
								<option value="1">승인</option>
								<option value="2">공고</option>
							</select>
						</span> 
						<span class="input-group-addon" style="width:20%;"> <label>승인일자</label></span>
						<span class="input-group-addon" style="width:30%;"> 
						</span> 
					</div>
					<div class="input-group" style="width:100%">
						<span class="input-group-addon" style="width:20%;"> <label>제목</label></span>
						<span class="input-group-addon" style="width:30%;"> 
							<input type="text" class="form-control" id="TITLE" name="TITLE"/>
						</span> 
						<span class="input-group-addon" style="width:20%;"> <label>저작물명</label></span>
						<span class="input-group-addon" style="width:30%;"> 
							<input type="text" class="form-control" id="CLMS_TITLE" name="CLMS_TITLE"/>
						</span> 
					</div>
					<div class="input-group" style="width:100%;">
						<span class="input-group-addon" style="width:20%;"> <label>분야</label></span>
						<span class="input-group-addon" style="width:80%; text-align: left;"> 
							<input type="checkbox" id="FILD_1" name="FILD_1" value="0">어문&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="FILD_2" name="FILD_2" value="0">음악&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="FILD_3" name="FILD_3" value="0">연극&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="FILD_4" name="FILD_4" value="0">미술&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="FILD_5" name="FILD_5" value="0">건축&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="FILD_6" name="FILD_6" value="0">사진&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="FILD_7" name="FILD_7" value="0">영상<br/><br/>
							<input type="checkbox" id="FILD_8" name="FILD_8" value="0">도형&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="FILD_9" name="FILD_9" value="0">컴퓨터프로그램&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="FILD_10" name="FILD_10" value="0">2차저작물&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="FILD_11" name="FILD_11" value="0">편집저작물&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="FILD_99" name="FILD_99" value="0">기타
							<button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;" style="float: right;">검색</button>
						</span>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
		
		<!-- /.box-body -->
	</div>
</div>


<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd42List1" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center" style="width: 7%">구분</th>
					<th class="text-center">제목</th>
					<th class="text-center" style="width: 7%">분야</th>
					<th class="text-center" style="width: 15%">저작(인접)권자</th>
					<th class="text-center" style="width: 11%">승인(공고)일자</th>
					<th class="text-center" style="width: 7%">ICN</th>
					<th class="text-center" style="width: 10%">첨부파일</th>
					<th class="text-center" style="width: 10%">조회수</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="9" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
								<td class="text-center"><c:out value="${info.GUBUN }"/></td>
								<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.BORD_SEQN}"/>','<c:out value="${info.MENU_SEQN}"/>');"><c:out value="${info.TITE }"/></a></td>
								<td class="text-center"><c:out value="${info.FILD_NAME }"/></td>
								<td class="text-center"><c:out value="${info.LICENSOR_NAME }"/></td>
								<td class="text-center"><c:out value="${info.APPR_DTTM }"/></td>
								<td class="text-center"><c:out value="${info.ICN }"/></td>
								<td class="text-center"><c:out value="${info.ATTC_COUNT }"/></td>
								<td class="text-center"><c:out value="${info.INQR_CONT }"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
</div>