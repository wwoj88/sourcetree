<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
//첨부파일 다운로드
function fncDown(BORD_SEQN,MENU_SEQN){
	location.href = '<c:url value="/console/legalmanage/fdcrAd42Download1.page"/>?BORD_SEQN='+BORD_SEQN+'&MENU_SEQN='+MENU_SEQN;
}


//취소 시 상세화면 이동
function fncGoView(APPLY_WRITE_YMD,APPLY_WRITE_SEQ){
	//var datas = [];
	//datas[0] = 'MENU_SEQN';
	var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06View1.page"/>')+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ;
	location.href = url;
}

//진행상태 수정
function fncGoHistChng(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,RECEIPT_NO){
	var STAT_CD = $('#STAT_CD').val();
	var STAT_MEMO = $('#STAT_MEMO').val();
	alert(STAT_CD + STAT_MEMO);
	var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06StatChange1.page"/>')+'&STAT_CD='+STAT_CD+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&RECEIPT_NO='+RECEIPT_NO+'&STAT_MEMO='+STAT_MEMO;
	location.href = url;
}

//심의결과 변경상태 수정
function fncGoHistChng2(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,WORKS_SEQN){
	var STAT_RSLT_CD = $('#STAT_RSLT_CD').val();
	var STAT_RSLT_MEMO = $('#STAT_RSLT_MEMO').val();
	var LGMT_PERI = $('#LGMT_PERI').val();
	var LGMT_AMNT = $('#LGMT_AMNT').val();
	var LGMT_PLAC_NAME = $('#LGMT_PLAC_NAME').val();
	var LGMT_GRAN = $('#LGMT_GRAN').val();
	alert(STAT_RSLT_CD + STAT_RSLT_MEMO);
	var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06StatWorksChange.page"/>')+'&STAT_RSLT_CD='+STAT_RSLT_CD+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&WORKS_SEQN='+WORKS_SEQN+'&STAT_RSLT_MEMO='+STAT_RSLT_MEMO
	+'&LGMT_PERI='+LGMT_PERI+'&LGMT_AMNT='+LGMT_AMNT+'&LGMT_PLAC_NAME='+LGMT_PLAC_NAME+'&LGMT_GRAN='+LGMT_GRAN;
	location.href = url;
}

//진행상태 내역조회 팝업
function fncGoHistory(APPLY_WRITE_YMD,APPLY_WRITE_SEQ){
	
	var popUrl = '<c:url value="/console/legal/fdcrAd06History1.page"/>?APPLY_WRITE_YMD='
		+ APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ; //팝업창에 출력될 페이지 URL
	var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
	window.open(popUrl, "", popOption);
}

//심의결과 내역조회 팝업
function fncGoHistory2(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,WORKS_SEQN){
	
	var popUrl = '<c:url value="/console/legal/fdcrAd06History2.page"/>?APPLY_WRITE_YMD='
		+ APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&WORKS_SEQN='+WORKS_SEQN; //팝업창에 출력될 페이지 URL
	var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
	window.open(popUrl, "", popOption);
}

function fncGoUpdate(BORD_SEQN,MENU_SEQN){
	
	var f = document.updateForm;
	f.action = '<c:url value="/console/legalmanage/fdcrAd42Update1.page"/>?BORD_SEQN='+BORD_SEQN+'&MENU_SEQN='+MENU_SEQN;
	f.submit();
	
}

//삭제
function fncGoDelete(BORD_SEQN,MENU_SEQN){
	if(confirm("삭제하시겠습니까?")){
		var f = document.updateForm;
		f.action = '<c:url value="/console/legalmanage/fdcrAd42Delete1.page"/>?BORD_SEQN='+BORD_SEQN+'&MENU_SEQN='+MENU_SEQN;
		f.submit();
		//var url = fncGetBoardParam('<c:url value="/console/legalmanage/fdcrAd42Delete1.page"/>')+'&BORD_SEQN='+BORD_SEQN+'&MENU_SEQN='+MENU_SEQN;
		//location.href = url;
	}
}

//목록
function fncGoList(){
	location.href = '<c:url value="/console/legalmanage/fdcrAd42List1.page"/>';
}

//취소
function fncGoCancel(){
	location.href = '<c:url value="/console/legalmanage/fdcrAd42List1.page"/>';
}

//clms 저작물 조회 팝업
function fncGoClmsSearch(){
	var popUrl = '<c:url value="/console/legalmanage/fdcrAd42Popup1.page"/>'; //팝업창에 출력될 페이지 URL
	var popOption = "width=1200, height=1200, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
	window.open(popUrl, "", popOption);
}

/* 
function getReturnValue(returnValue) {
	  //alert(returnValue);
	  var icnVal = $('#ICN').val();
	  icnVal = returnValue[2];
	  alert(icnVal);
	}
 */
 
 function SetValue(key1,key2,key3,key4){
	 var strTitle = $('#CLMS_TITE').val();
	 var strIcn = $('#ICN').val();
	 strTitle = key2;
	 strIcn = key3;
	 
	 alert(key4);
	 if(key4 == "1"){
		document.getElementById("PRPS_DIVS").value = "M";
	 }else if(key4 == "2"){
		document.getElementById("PRPS_DIVS").value = "O"; 
	 }else if(key4 == "3"){
		document.getElementById("PRPS_DIVS").value = "C";
	 }else if(key4 == "4"){
		document.getElementById("PRPS_DIVS").value = "R"; 
	 }else if(key4 == "5"){
		document.getElementById("PRPS_DIVS").value = "V"; 
	 }else if(key4 == "6"){
		alert("준비중"); 
	 }else {
		slert("선택된 장르가 없습니다.");
	 }
	 document.getElementById("CLMS_TITE").value = strTitle;
	 document.getElementById("ICN").value = strIcn;
	 document.getElementById("CR_ID").value = key1;
 }
 
 function fncIcnView(){
	 var ICN = $('#ICN').val();
	 var CR_ID = $('#CR_ID').val();
	 var PRPS_DIVS = $('#PRPS_DIVS').val();
	 
	 if(PRPS_DIVS == "C" || PRPS_DIVS == "R" || PRPS_DIVS == "V" ){
	 	 var popUrl = '<c:url value="/console/legalmanage/fdcrAd42View1.page"/>?CR_ID='+CR_ID+'&PRPS_DIVS='+PRPS_DIVS; //팝업창에 출력될 페이지 URL
		 var popOption = "width=1200, height=1200, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		 window.open(popUrl, "", popOption);
	 }else{
		 var popUrl = '<c:url value="/console/legalmanage/fdcrAd42View2.page"/>?CR_ID='+CR_ID+'&PRPS_DIVS='+PRPS_DIVS; //팝업창에 출력될 페이지 URL
		 var popOption = "width=1200, height=1200, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		 window.open(popUrl, "", popOption);
	 }
 }
</script>
<form name="updateForm" id="updateForm" method="post" enctype="multipart/form-data">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
	<input type="hidden" id="rowCount" name="rowCount" value="1">
	<input type="hidden" id="CR_ID" name="CR_ID" value="">
	<input type="hidden" id="PRPS_DIVS" name="PRPS_DIVS" value="">
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
			<div style="margin: 7px 0 15px 5px;"><font size="3">● 법정허락 저작물 수정</font></div>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th class="text-center" style="width: 20%">이 름</th>
						<td style="width: 30%"><input class="form-control" type="text" name="RGST_IDNT" id="RGST_IDNT" value="<c:out value="${info.RGST_IDNT}"/>"/></td>
						<th class="text-center" style="width: 20%">저작(인접)권자</th>
						<td style="width: 30%"><input class="form-control" type="text" name="LICENSOR_NAME" id="LICENSOR_NAME" value="<c:out value="${info.LICENSOR_NAME}"/>"/></td>
					</tr>
					 
					<tr>
						<th class="text-center">제목</th>
						<td colspan="3">
						<input class="form-control" type="text" name="TITE" id="TITE" value="<c:out value="${info.TITE}"/>" />
						</td>
					</tr>
					<tr>
						<th class="text-center">저작물명</th>
						<td colspan="2">
						<input class="form-control" type="text" name="CLMS_TITE" id="CLMS_TITE" value="<c:out value="${info.CLMS_TITE}"/>" />
						</td>
						<td>
						<button type="submit" class="btn btn-primary" onclick="fncGoClmsSearch();return false;">조회</button>
						</td>
					</tr>
					<tr>
						<th class="text-center">ICN번호</th>
						<td colspan="3">
						<%-- <a href="#"><input class="form-control" type="text" name="ICN" id="ICN" value="${strIcn}" onclick="fncIcnView();return false;"/></a> --%>
						<a href="#"><input class="form-control" type="text" name="ICN" id="ICN" value="<c:out value="${info.ICN}"/>" onclick="fncIcnView();return false;"/></a>
						</td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">공고승인구분</th>
						<td style="width: 30%">
							<select class="form-control" id="GUBUN" name="GUBUN">
								<option value="1" <c:if test="${info.GUBUN eq '1'}">selected="selected"</c:if>>승인</option>
								<option value="2" <c:if test="${info.GUBUN eq '2'}">selected="selected"</c:if>>공고</option>
							</select>
						</td>
						<th class="text-center" style="width: 20%">승인일자</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPR_DTTM" id="APPR_DTTM" value="<c:out value="${info.APPR_DTTM}"/>"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">분야</th>
						<td colspan="3">
							<input type="checkbox" name="FILD_1" id="FILD_1" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_1}"/>> 도서
							<input type="checkbox" name="FILD_2" id="FILD_2" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_2}"/>> 음악
							<input type="checkbox" name="FILD_3" id="FILD_3" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_3}"/>> 연극
							<input type="checkbox" name="FILD_4" id="FILD_4" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_4}"/>> 미술
							<input type="checkbox" name="FILD_5" id="FILD_5" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_5}"/>> 건축
							<input type="checkbox" name="FILD_6" id="FILD_6" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_6}"/>> 사진
							<input type="checkbox" name="FILD_7" id="FILD_7" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_7}"/>> 영상<br/>
							<input type="checkbox" name="FILD_8" id="FILD_8" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_8}"/>> 도형
							<input type="checkbox" name="FILD_9" id="FILD_9" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_9}"/>> 컴퓨터프로그램
							<input type="checkbox" name="FILD_10" id="FILD_10" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_10}"/>> 2차저작물
							<input type="checkbox" name="FILD_11" id="FILD_11" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_11}"/>> 편집저작물
							<input type="checkbox" name="FILD_12" id="FILD_12" value="1" <console:fn func="isChecked" value="1" value1="${info.FILD_99}"/>> 기타
						</td>
					</tr>
					<tr>
						<th class="text-center">내   용</th>
						<td colspan="3">
						<textarea class="form-control" cols="5" id="BORD_DESC" name="BORD_DESC"><c:out value="${info.BORD_DESC}"/></textarea>
						</td>
					</tr>
					<tr>
						<th class="text-center">첨부서류</th>
						<td colspan="3" id="TdIdFile">
						<c:choose>
						<c:when test="${empty ds_file}">
							<span>등록된 첨부서류가 없습니다.</span>
						</c:when>
						<c:otherwise>
							<c:forEach var="attach" items="${ds_file}"
								varStatus="listStatus">
							<p class="file">
								<console:fn func="getFileIcon" value="${attach.REAL_FILE_NAME}"/>
								<a href="#" onclick="fncDown('<c:out value="${info.BORD_SEQN}"/>','<c:out value="${info.MENU_SEQN}"/>');return false;"><c:out value="${attach.FILE_NAME}"/></a>
								<span>(파일크기: <console:fn func="strFileSize" value="${attach.FILE_SIZE}"/>)</span>
							</p>
							</c:forEach>
						</c:otherwise>
						</c:choose>
						</td>
					</tr>
			</table>
		</div>
	<div class="box-footer" style="text-align:right;">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoUpdate('<c:out value="${info.BORD_SEQN}"/>','<c:out value="${info.MENU_SEQN}"/>');return false;">저장</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoDelete('<c:out value="${info.BORD_SEQN}"/>','<c:out value="${info.MENU_SEQN}"/>');return false;">삭제</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoCancel('<c:out value="${info.BORD_SEQN}"/>','<c:out value="${info.MENU_SEQN}"/>');return false;">취소</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoList('<c:out value="${info.BORD_SEQN}"/>','<c:out value="${info.MENU_SEQN}"/>');return false;">목록</button></div>
	</div>
</div>
</form>