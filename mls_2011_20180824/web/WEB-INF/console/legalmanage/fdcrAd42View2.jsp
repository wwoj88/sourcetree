<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncClosePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
<div style="margin: 7px 0 15px 5px;"><font size="3">법정허락 저작물</font></div>
	<!-- /.box-header -->
</div>

<div style="margin: 7px 0 15px 5px;"><font size="3">● CLMS저작물 상세조회</font></div>
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
	<div style="margin: 7px 0 5px 5px;"><font size="3">◎ 저작물정보</font></div>
		<table id="fdcrAd42View2" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">저작물제목</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th>저작물부제목</th>
						<td class="text-center"></td>
					</tr>
			</tbody>
		</table>
		
	<div style="margin: 30px 0 5px 5px;"><font size="3">◎ 작품정보</font></div>
		<table id="fdcrAd42View2" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">프로그램코드</th>
						<td class="text-center"></td>
						<th style="width: 20%">프로그램명</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">원제</th>
						<td class="text-center"></td>
						<th style="width: 20%">매체코드</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">채널코드</th>
						<td class="text-center"></td>
						<th style="width: 20%">방송위장르</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">프로그램ID</th>
						<td class="text-center"></td>
						<th style="width: 20%">프로그램회차</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">부제</th>
						<td class="text-center"></td>
						<th style="width: 20%">소제</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">방송일자</th>
						<td class="text-center"></td>
						<th style="width: 20%">부순명칭</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th>주요내용</th>
						<td class="text-center" colspan="3"></td>
					</tr>
					<tr>
						<th style="width: 20%">제작품질</th>
						<td class="text-center"></td>
						<th style="width: 20%">프로그램등급</th>
						<td class="text-center"></td>
					</tr>
			</tbody>
		</table>
		
	<div style="margin: 30px 0 5px 5px;"><font size="3">◎ 권리정보</font></div>
		<table id="fdcrAd42View2" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">제작자</th>
						<td class="text-center"></td>
						<th style="width: 20%">한국방송콘텐츠협회</th>
						<td class="text-center"></td>
					</tr>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncClosePop();return false;">닫기</button></div>
	</div>
</div>