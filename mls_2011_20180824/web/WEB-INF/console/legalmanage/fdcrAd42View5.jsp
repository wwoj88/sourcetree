<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncClosePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
<div style="margin: 7px 0 15px 5px;"><font size="3">법정허락 저작물</font></div>
	<!-- /.box-header -->
</div>

<div style="margin: 7px 0 15px 5px;"><font size="3">● CLMS저작물 상세조회</font></div>
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
	<div style="margin: 7px 0 5px 5px;"><font size="3">◎ 앨범정보</font></div>
		<table id="fdcrAd42View5" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">앨범명</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">앨범유형</th>
						<td class="text-center"></td>
						<th style="width: 20%">앨범레이블번호</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">앨범제작저작권자</th>
						<td class="text-center"></td>
						<th style="width: 20%">에디션번호</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">발매일</th>
						<td class="text-center"></td>
						<th style="width: 20%">제작회사</th>
						<td class="text-center"></td>
					</tr>
			</tbody>
		</table>
		
	<div style="margin: 30px 0 5px 5px;"><font size="3">◎ 곡정보</font></div>
		<table id="fdcrAd42View5" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">곡명</th>
						<td class="text-center"></td>
						<th style="width: 20%">디스크번호</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">트랙번호</th>
						<td class="text-center"></td>
						<th style="width: 20%">곡구분(일반/클래식)</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">프로듀서</th>
						<td class="text-center"></td>
						<th style="width: 20%">런닝타임</th>
						<td class="text-center"></td>
					</tr>
			</tbody>
		</table>
		
	<div style="margin: 30px 0 5px 5px;"><font size="3">◎ 권리정보</font></div>
		<table id="fdcrAd42View5" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">작사</th>
						<td class="text-center"></td>
						<th style="width: 20%">작곡</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">편곡</th>
						<td class="text-center"></td>
						<th style="width: 20%">한국음악저작권협회</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">가창</th>
						<td class="text-center"></td>
						<th style="width: 20%">연주</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">지휘</th>
						<td class="text-center"></td>
						<th style="width: 20%">한국음악실연자연합회</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">음반 제작자</th>
						<td class="text-center"></td>
						<th style="width: 20%">한국음반산업협회</th>
						<td class="text-center"></td>
					</tr>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncClosePop();return false;">닫기</button></div>
	</div>
</div>