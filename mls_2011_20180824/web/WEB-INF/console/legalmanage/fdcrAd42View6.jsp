<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncClosePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
<div style="margin: 7px 0 15px 5px;"><font size="3">법정허락 저작물</font></div>
	<!-- /.box-header -->
</div>

<div style="margin: 7px 0 15px 5px;"><font size="3">● CLMS저작물 상세조회</font></div>
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
	<div style="margin: 7px 0 5px 5px;"><font size="3">◎ 도서저작물정보</font></div>
		<table id="fdcrAd42View6" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">저작물제목</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th>저작물부제목</th>
						<td class="text-center"></td>
					</tr>
			</tbody>
		</table>
		
	<div style="margin: 30px 0 5px 5px;"><font size="3">◎ 도서정보</font></div>
		<table id="fdcrAd42View6" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">도서명</th>
						<td class="text-center"></td>
						<th style="width: 20%">도서부제</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">출판사</th>
						<td class="text-center"></td>
						<th style="width: 20%">자료유형</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">검색분류</th>
						<td class="text-center"></td>
						<th style="width: 20%">장르구분</th>
						<td class="text-center"></td>
					</tr>
			</tbody>
		</table>
		
	<div style="margin: 30px 0 5px 5px;"><font size="3">◎ 권리정보</font></div>
		<table id="fdcrAd42View6" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">저자</th>
						<td class="text-center"></td>
						<th style="width: 20%">예필명</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th>역자</th>
						<td class="text-center"></td>
					</tr>
					<tr>
						<th style="width: 20%">한국문예학술저작권협회</th>
						<td class="text-center"></td>
						<th style="width: 20%">한국복사전송권협회</th>
						<td class="text-center"></td>
					</tr>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncClosePop();return false;">닫기</button></div>
	</div>
</div>