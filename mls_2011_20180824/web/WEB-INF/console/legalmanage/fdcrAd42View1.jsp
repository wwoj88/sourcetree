<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncClosePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
<div style="margin: 7px 0 15px 5px;"><font size="3">법정허락 저작물</font></div>
	<!-- /.box-header -->
</div>

<div style="margin: 7px 0 15px 5px;"><font size="3">● CLMS저작물 상세조회</font></div>
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
	<c:forEach var="info" items="${ds_works_info}" varStatus="listStatus">
	<div style="margin: 7px 0 5px 5px;"><font size="3">◎ 저작물정보</font></div>
		<table id="fdcrAd42View1" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">저작물제목</th>
						<td class="text-center"><c:out value="${info.TITLE }"/></td>
					</tr>
					<tr>
						<th>저작물부제목</th>
						<td class="text-center"><c:out value="${info.SUBTITLE }"/></td>
					</tr>
			</tbody>
		</table>
		
	<div style="margin: 30px 0 5px 5px;"><font size="3">◎ 작품정보</font></div>
		<table id="fdcrAd42View1" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">장르분류</th>
						<td class="text-center"><c:out value="${info.GENRE_KIND_NAME }"/></td>
						<th style="width: 20%">소재분류</th>
						<td class="text-center"><c:out value="${info.SUBJ_KIND_NAME }"/></td>
					</tr>
					<tr>
						<th>연출</th>
						<td class="text-center" colspan="3"><c:out value="${info.DIRECT }"/></td>
					</tr>
					<tr>
						<th>원작명</th>
						<td class="text-center" colspan="3"><c:out value="${info.ORIG_WORK }"/></td>
					</tr>
					<tr>
						<th>주요출연진</th>
						<td class="text-center" colspan="3"><c:out value="${info.PLAYERS }"/></td>
					</tr>
					<tr>
						<th>줄거리</th>
						<td class="text-center" colspan="3"><c:out value="${info.SYNNOPSIS }"/></td>
					</tr>
					<tr>
						<th style="width: 20%">방송회차</th>
						<td class="text-center"><c:out value="${info.BROAD_ORD }"/></td>
						<th style="width: 20%">방송일시</th>
						<td class="text-center"><c:out value="${info.BROAD_DATE }"/></td>
					</tr>
					<tr>
						<th style="width: 20%">방송시간/분</th>
						<td class="text-center"><c:out value="${info.TELE_TIME }"/></td>
						<th style="width: 20%">제작사</th>
						<td class="text-center"><c:out value="${info.MAKER }"/></td>
					</tr>
					<tr>
						<th>진행자</th>
						<td class="text-center" colspan="3"><c:out value="${info.MC }"/></td>
					</tr>
			</tbody>
		</table>
		
	<div style="margin: 30px 0 5px 5px;"><font size="3">◎ 권리정보</font></div>
		<table id="fdcrAd42View1" class="table table-bordered text-center">
			<tbody>
					<tr>
						<th style="width: 20%">작가</th>
						<td class="text-center"><c:out value="${info.WRITER }"/></td>
						<th style="width: 20%">한국방송작가협회</th>
						<td class="text-center">신탁</td>
					</tr>
			</tbody>
		</table>
	</c:forEach>
	</div>
	<!-- /.box-body -->
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncClosePop();return false;">닫기</button></div>
	</div>
</div>