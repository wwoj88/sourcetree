<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
		<table class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 100px" rowspan="2">기준년월</th>
					<th style="width: 50px" rowspan="2">진행<br>차수</th>
					<th style="width: 300px" colspan="3">대상저작물</th>
					<th style="width: 120px" colspan="2" rowspan="2">진행상태<br>자동화수행(*)표시</th>
					<th style="width: 150px" rowspan="2">법정허락 대상저작물 전환</th>
				</tr>
				<tr>
					<th style="width: 100px">전체</th>
					<th style="width: 100px">(미분배보상금)</th>
					<th style="width: 100px">(개인신청)</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_works_list}">
						<tr>
							<td colspan="18">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_works_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.YYYYMM}" /></td>
								<td><c:out value="${info.STAT_ORD}" /></td>
								<td><console:fn func="formatNumber" value="${info.TARG_WORKS_CONT}"/></td>
								<td><console:fn func="formatNumber" value="${info.WORKS_DIVS_CONT_01}" /></td>
								<td><console:fn func="formatNumber" value="${info.WORKS_DIVS_CONT_02}" /></td>
								<td><c:out value="${info.STAT_NAME}" /></td>
								<td>
									<c:if test="${info.STAT_CD == 2}"><a href="#" onclick="fncExeFdcrAd03_2('<c:out value="${info.STAT_ORD}" />', '<c:out value="${info.YYYYMM}" />', '<c:out value="${info.TARG_WORKS_CONT}" />', '<c:out value="${info.P_SCH_YN}" />');return false;">실행</a></c:if>
									<c:if test="${info.STAT_CD == 3}">진행중</c:if>
									<c:if test="${info.STAT_CD == 4}"><a href="#" onclick="fncPopupResultWorks('<c:out value="${info.STAT_ORD}" />', '<c:out value="${info.YYYYMM}" />');return false;">결과보기</a></c:if>
								</td>
								<td><console:fn func="formatNumber" value="${info.RSLT_CONT}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
