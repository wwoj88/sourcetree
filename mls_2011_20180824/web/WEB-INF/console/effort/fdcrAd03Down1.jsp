<%@ page language="java" contentType="application/vnd.ms-excel;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%
	String filename = "상당한노력 진행현황.xls";
    
	response.setHeader( "Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "utf-8") + "" );
	response.setHeader( "Content-Description", "JSP Generated Data" );

	//response.setContentType( "application/vnd.ms-excel" );   

	//DataSet divList = (DataSet) box.getObject("EmasDivList");
	//HashMap ageMap = (HashMap) box.getObject("LicenseAgeClass");
%>
<table border="1" cellpadding="0" cellspacing="0" style="table-layout:fixed;text-align:center" >
	<thead>
		<tr>
			<th style="background-color:#eeeeee;width: 100px" rowspan="2">기준년월</th>
			<th style="background-color:#eeeeee;width: 50px" rowspan="2">진행<br>차수</th>
			<th style="background-color:#eeeeee;width: 200px" rowspan="2" colspan="2">대상저작물</th>
			<th style="background-color:#eeeeee;width: 100px" colspan="2">(1차)위탁관리저작물매칭</th>
			<th style="background-color:#eeeeee;width: 100px" colspan="2">(2차)위탁관리저작물매칭</th>
			<th style="background-color:#eeeeee;width: 100px" colspan="2">(3차)위탁관리저작물매칭</th>
			<th style="background-color:#eeeeee;width: 120px" colspan="2" rowspan="2">진행상태<br>자동화수행(*)표시</th>
			<th style="background-color:#eeeeee;width: 140px" colspan="2">진행일자</th>
			<th style="background-color:#eeeeee;width: 50px" rowspan="2">상당한노력공고</th>
			<th style="background-color:#eeeeee;width: 140px" colspan="3">상당한노력 예외 직접수행</th>
		</tr>
		<tr>
			<th style="background-color:#eeeeee;width: 50px">매칭</th>
			<th style="background-color:#eeeeee;width: 50px">비매칭</th>
			<th style="background-color:#eeeeee;width: 50px">매칭</th>
			<th style="background-color:#eeeeee;width: 50px">비매칭</th>
			<th style="background-color:#eeeeee;width: 50px">매칭</th>
			<th style="background-color:#eeeeee;width: 50px">비매칭</th>
			<th style="background-color:#eeeeee;width: 70px">시작일자</th>
			<th style="background-color:#eeeeee;width: 70px">종료일자</th>
			<th style="background-color:#eeeeee;width: 70px" colspan="2">진행상태</th>
			<th style="background-color:#eeeeee;width: 70px">완료일자</th>
		</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${empty ds_list}">
			</c:when>
			<c:otherwise>
				<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<tr>
						<td><c:out value="${info.YYYYMM}" /></td>
						<td><c:out value="${info.ORD}" /></td>
						<td><c:out value="${info.WORKS_DIVS_NAME}" /></td>
						<td><c:out value="${info.TARG_WORKS_CONT}" /></td>
						<td><c:out value="${info.RSLT_CONT_1ST_YS}" /></td>
						<td><c:out value="${info.RSLT_CONT_1ST_NO}" /></td>
						<td><c:out value="${info.RSLT_CONT_2ND_YS}" /></td>
						<td><c:out value="${info.RSLT_CONT_2ND_NO}" /></td>
						<td><c:out value="${info.RSLT_CONT_3RD_YS}" /></td>
						<td><c:out value="${info.RSLT_CONT_3RD_NO}" /></td>
						<td><c:out value="${info.STAT_NAME}" /></td>
						<td>
							<c:if test="${info.STAT_CD == 2}">실행</c:if>
							<c:if test="${info.STAT_CD == 3}">진행중</c:if>
							<c:if test="${info.STAT_CD == 4}">결과보기</c:if>
						</td>
						<td><c:if test="${!empty info.START_DTTM}"><console:fn func="getDate" value="${info.START_DTTM}" value1="yyyyMMdd" value2="yyyy.MM.dd"/></c:if></td>
						<td><c:if test="${!empty info.END_DTTM}"><console:fn func="getDate" value="${info.END_DTTM}" value1="yyyyMMdd" value2="yyyy.MM.dd"/></c:if></td>
						<td><c:out value="${info.ANUC_CONT}" /></td>
						<td>
							<c:out value="${info.EXCP_STAT_NAME}" />
						</td>
						<td>
							<c:if test="${info.EXCP_STAT_CD == 20}"><a href="#" onclick="fncPopupResult('<c:out value="${info.ORD}" />', '<c:out value="${info.YYYYMM}" />', '<c:out value="${info.WORKS_DIVS_CD}" />', 1);return false;"><c:out value="${info.EXCP_CONT}" /></a></c:if>
							<c:if test="${info.EXCP_STAT_CD != 20}"><c:out value="${info.EXCP_CONT}" /></c:if>
						</td>
						<td><c:if test="${!empty info.EXCP_END_DTTM}"><console:fn func="getDate" value="${info.EXCP_END_DTTM}" value1="yyyyMMdd" value2="yyyy.MM.dd"/></c:if></td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
