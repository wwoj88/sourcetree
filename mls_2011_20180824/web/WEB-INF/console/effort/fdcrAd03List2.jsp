<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
		<table class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 100px" rowspan="2">기준년월</th>
					<th style="width: 50px" rowspan="2">진행<br>차수</th>
					<th style="width: 200px" rowspan="2" colspan="2">대상저작물</th>
					<th style="width: 100px" colspan="2">(1차)위탁관리저작물매칭</th>
					<th style="width: 100px" colspan="2">(2차)위탁관리저작물매칭</th>
					<th style="width: 100px" colspan="2">(3차)위탁관리저작물매칭</th>
					<th style="width: 120px" colspan="2" rowspan="2">진행상태<br>자동화수행(*)표시</th>
					<th style="width: 140px" colspan="2">진행일자</th>
					<th style="width: 50px" rowspan="2">상당한노력공고</th>
					<th style="width: 140px" colspan="3">상당한노력 예외 직접수행</th>
				</tr>
				<tr>
					<th style="width: 50px">매칭</th>
					<th style="width: 50px">비매칭</th>
					<th style="width: 50px">매칭</th>
					<th style="width: 50px">비매칭</th>
					<th style="width: 50px">매칭</th>
					<th style="width: 50px">비매칭</th>
					<th style="width: 70px">시작일자</th>
					<th style="width: 70px">종료일자</th>
					<th style="width: 70px" colspan="2">진행상태</th>
					<th style="width: 70px">완료일자</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="18">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.YYYYMM}" /></td>
								<td><c:out value="${info.ORD}" /></td>
								<td><c:out value="${info.WORKS_DIVS_NAME}" /></td>
								<td><a href="#" onclick="fncPopupTarget('<c:out value="${info.ORD}" />', '<c:out value="${info.YYYYMM}" />', '<c:out value="${info.WORKS_DIVS_CD}" />', '<c:out value="${info.STAT_CD}" />');return false;"><c:out value="${info.TARG_WORKS_CONT}" /></a></td>
								<td><c:out value="${info.RSLT_CONT_1ST_YS}" /></td>
								<td><c:out value="${info.RSLT_CONT_1ST_NO}" /></td>
								<td><c:out value="${info.RSLT_CONT_2ND_YS}" /></td>
								<td><c:out value="${info.RSLT_CONT_2ND_NO}" /></td>
								<td><c:out value="${info.RSLT_CONT_3RD_YS}" /></td>
								<td><c:out value="${info.RSLT_CONT_3RD_NO}" /></td>
								<td><c:out value="${info.STAT_NAME}" /></td>
								<td>
									<c:if test="${info.STAT_CD == 2}"><a href="#" onclick="fncExeFdcrAd03_1('<c:out value="${info.ORD}" />', '<c:out value="${info.YYYYMM}" />', '<c:out value="${info.WORKS_DIVS_CD}" />', '<c:out value="${info.TARG_WORKS_CONT}" />');return false;">실행</a></c:if>
									<c:if test="${info.STAT_CD == 3}">진행중</c:if>
									<c:if test="${info.STAT_CD == 4}"><a href="#" onclick="fncPopupResult('<c:out value="${info.ORD}" />', '<c:out value="${info.YYYYMM}" />', '<c:out value="${info.WORKS_DIVS_CD}" />', 0);return false;">결과보기</a></c:if>
								</td>
								<td><c:if test="${!empty info.START_DTTM}"><console:fn func="getDate" value="${info.START_DTTM}" value1="yyyyMMdd" value2="yyyy.MM.dd"/></c:if></td>
								<td><c:if test="${!empty info.END_DTTM}"><console:fn func="getDate" value="${info.END_DTTM}" value1="yyyyMMdd" value2="yyyy.MM.dd"/></c:if></td>
								<td><c:out value="${info.ANUC_CONT}" /></td>
								<td>
									<c:out value="${info.EXCP_STAT_NAME}" />
								</td>
								<td>
									<c:if test="${info.EXCP_STAT_CD == 20}"><a href="#" onclick="fncPopupResult('<c:out value="${info.ORD}" />', '<c:out value="${info.YYYYMM}" />', '<c:out value="${info.WORKS_DIVS_CD}" />', 1);return false;"><c:out value="${info.EXCP_CONT}" /></a></c:if>
									<c:if test="${info.EXCP_STAT_CD != 20}"><c:out value="${info.EXCP_CONT}" /></c:if>
								</td>
								<td><c:if test="${!empty info.EXCP_END_DTTM}"><console:fn func="getDate" value="${info.EXCP_END_DTTM}" value1="yyyyMMdd" value2="yyyy.MM.dd"/></c:if></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>