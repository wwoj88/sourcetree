<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncGoPage(page) {
		var datas = [];
		datas[0] = 'ORD';
		datas[1] = 'YYYYMM';
		datas[2] = 'WORKS_DIVS_CD';
		datas[3] = 'SEARCH_CD';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd03List5.page"/>',datas, page);
		location.href = url;
	}
	
	function fncGoSearch(){
		var datas = [];
		datas[0] = 'ORD';
		datas[1] = 'YYYYMM';
		datas[2] = 'WORKS_DIVS_CD';
		datas[3] = 'SEARCH_CD';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd03List5.page"/>',datas);
		var WORKS_TITLE = $('#WORKS_TITLE').val();
		if(WORKS_TITLE != ''){
			WORKS_TITLE = encodeURI(encodeURIComponent(WORKS_TITLE));
			url += '&WORKS_TITLE='+WORKS_TITLE;
		}
		location.href = url;
	}
	
	function fncUpdateYn(STAT_PROC_EXCP_YN,ORD,YYYYMM,WORKS_DIVS_CD,WORKS_ID){
		if(STAT_PROC_EXCP_YN=='Y'){
			if(confirm("해당저작물의 상당한노력 예외처리를 삭제합니다.")){
				var data = {};
				data.ORD = ORD;
				data.YYYYMM = YYYYMM;
				data.WORKS_DIVS_CD = WORKS_DIVS_CD;
				data.WORKS_ID = WORKS_ID;
				data.SYST_EFFORT_STAT_CD = "4";
				data.STAT_WORKS_YN = "N";
				
				fncPost('<c:out value="/console/effort/fdcrAd03Update1.page"/>',data,function(returnData){
					if(returnData=='Y'){
						alert('예외처리완료');
						location.reload();
					}else{
						alert('실패했습니다.');
					}
				});
			}
		}else{
			if(confirm("해당저작물을 상당한노력 예외처리합니다.")){
				var data = {};
				data.ORD = ORD;
				data.YYYYMM = YYYYMM;
				data.WORKS_DIVS_CD = WORKS_DIVS_CD;
				data.WORKS_ID = WORKS_ID;
				data.SYST_EFFORT_STAT_CD = "3";
				data.STAT_WORKS_YN = "Y";
				
				fncPost('<c:out value="/console/effort/fdcrAd03Update1.page"/>',data,function(returnData){
					if(returnData=='Y'){
						alert('예외처리완료');
						location.reload();
					}else{
						alert('실패했습니다.');
					}
				});
			}
		}
		
	}
	
	function fncExcelDown2(){
		var datas = [];
		datas[0] = 'ORD';
		datas[1] = 'YYYYMM';
		datas[2] = 'WORKS_DIVS_CD';
		datas[3] = 'SEARCH_CD';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd03Down2.page"/>',datas);
		var WORKS_TITLE = $('#WORKS_TITLE').val();
		if(WORKS_TITLE != ''){
			WORKS_TITLE = encodeURI(encodeURIComponent(WORKS_TITLE));
			url += '&WORKS_TITLE='+WORKS_TITLE;
		}
		location.href = url;
	}
</script>
<div class="box-body" style="padding:10px">
<form name="updateForm" id="updateForm" method="post">
<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
<input type="hidden" id="ORD" name="ORD" value="<c:out value="${commandMap.ORD}"/>" />
<input type="hidden" id="YYYYMM" name="YYYYMM" value="<c:out value="${commandMap.YYYYMM}"/>" />
<input type="hidden" id="WORKS_DIVS_CD" name="WORKS_DIVS_CD" value="<c:out value="${commandMap.WORKS_DIVS_CD}"/>" />
<input type="hidden" id="SEARCH_CD" name="SEARCH_CD" value="<c:out value="${commandMap.SEARCH_CD}"/>" />
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">상당한 노력 진행현황</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">저작물명</th>
									<td>
										<input type="text" id="WORKS_TITLE" name="WORKS_TITLE" value="<c:out value="${commandMap.WORKS_TITLE}"/>"/>
									</td>
								</tr>
							</tbody>
						</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>
<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncExcelDown2();return false;">Excel Down</button></div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 7%">진행차수</th>
					<th style="width: 7%">기준년월</th>
					<th style="width: 7%">수행일자</th>
					<th style="width: 10%">대상저작물</th>
					<th>저작물명</th>
					<th style="width: 9%">1차매칭여부</th>
					<th style="width: 9%">2차매칭여부</th>
					<th style="width: 9%">3차매칭여부</th>
					<th style="width: 9%">공고등록여부</th>
					<th style="width: 8%">공고일자</th>
					<th style="width: 8%">상당한노력 예외처리</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="11">내용이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.ORD}" /></td>
								<td><c:out value="${info.YYYYMM}" /></td>
								<td><c:out value="${info.PROC_DTTM}" /></td>
								<td><c:out value="${info.WORKS_DIVS_NAME}" /></td>
								<td><c:out value="${info.WORKS_TITLE}" /></td>
								<td><c:out value="${info.RSLT_1ST_YN}" /></td>
								<td><c:out value="${info.RSLT_2ND_YN}" /></td>
								<td><c:out value="${info.RSLT_3RD_YN}" /></td>
								<td><c:out value="${info.ANUC_REGI_YN}" /></td>
								<td><c:if test="${!empty info.ANUC_REGI_DTTM}"><console:fn func="getDate" value="${info.ANUC_REGI_DTTM}" value1="yyyyMMdd" value2="yyyy-MM-dd"/></c:if></td>
								<td>
									<a href="#" onclick="fncUpdateYn('<c:out value="${info.STAT_PROC_EXCP_YN}" />','<c:out value="${info.ORD}" />','<c:out value="${info.YYYYMM}" />','<c:out value="${info.WORKS_DIVS_CD}" />','<c:out value="${info.WORKS_ID}" />');">
										<c:if test="${info.STAT_PROC_EXCP_YN=='Y'}">예외처리완료(<c:out value="${info.STAT_PROC_EXCP_DTTM}" />)</c:if>
										<c:if test="${info.STAT_PROC_EXCP_YN!='Y'}">대상저작물 전환 및 공고등록</c:if>
									</a>
								</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
</div>
</form>
</div>