<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	$(function () {
	});
	
	function fncGoPage(page) {
		var datas = [];
		datas[0] = 'works_divs_cd';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd05List1.page"/>',datas, page);
		var works_title = $('#works_title').val();
		if(works_title != ''){
			works_title = encodeURI(encodeURIComponent(works_title));
			url += '&works_title='+works_title;
		}
		location.href = url;
	}
	
	function fncGoSearch() {
		var datas = [];
		datas[0] = 'works_divs_cd';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd05List1.page"/>',datas, 1);
		var works_title = $('#works_title').val();
		if(works_title != ''){
			works_title = encodeURI(encodeURIComponent(works_title));
			url += '&works_title='+works_title;
		}
		location.href = url;
	}
	
	function fncExcelDown1() {
		var datas = [];
		datas[0] = 'works_divs_cd';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd05Down1.page"/>',datas, 1);
		var works_title = $('#works_title').val();
		if(works_title != ''){
			works_title = encodeURI(encodeURIComponent(works_title));
			url += '&works_title='+works_title;
		}
		location.href = url;
	}
</script>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
					<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">구분</th>
									<td colspan="3">
										<select style="width:100px" id="works_divs_cd" name="works_divs_cd">
											<option value="" <console:fn func="isSelected" value="" value1="${commandMap.works_divs_cd}"/>>-전체-</option>
											<option value="1" <console:fn func="isSelected" value="1" value1="${commandMap.works_divs_cd}"/>>미분배보상금(방송음악)</option>
											<option value="2" <console:fn func="isSelected" value="2" value1="${commandMap.works_divs_cd}"/>>미분배보상금(교과용)</option>
											<option value="3" <console:fn func="isSelected" value="3" value1="${commandMap.works_divs_cd}"/>>미분배보상금(도서관)</option>
											<option value="4" <console:fn func="isSelected" value="4" value1="${commandMap.works_divs_cd}"/>>거소불명저작물</option>
											<option value="6" <console:fn func="isSelected" value="6" value1="${commandMap.works_divs_cd}"/>>기승인저작물</option>
										</select>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">제호</th>
									<td colspan="3">
										<input type="text" id="works_title" name="works_title" value="<c:out value="${commandMap.works_title}"/>"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>
<div class="box" style="margin-top: 30px;">
<!-- /.box-header -->
<div class="box-header" style="text-align:right">
	총 <c:out value="${paginationInfo.totalRecordCount}"/>건 
	<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncExcelDown1();return false;">Excel Down</button></div>
</div>
<div class="box-body">
	<table id="fdcrAd02List1Sub1" class="table table-bordered table-hover text-center table-list">
		<thead>
			<tr>
				<th style="width: 50%" colspan="5">대상 저작물 정보</th>
				<th style="width: 50%" colspan="3">매칭 저작물 정보</th>
			</tr>
			<tr>
				<th style="width: 5%">번호</th>
				<th style="width: 10%">구분</th>
				<th style="width: 5%">장르</th>
				<th style="width: 15%">제호</th>
				<th style="width: 15%">저작물정보</th>
				<th style="width: 5%">순번</th>
				<th style="width: 10%">구분</th>
				<th style="width: 35%">저작물정보</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty ds_list}">
					<tr>
						<td colspan="6" class="text-center">검색 결과가 없습니다.</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
						<tr>
							<td><c:out value="${paginationInfo.totalRecordCount-(listStatus.count)-((paginationInfo.currentPageNo-1)*paginationInfo.recordCountPerPage)+1}"/></td>
							<td><c:out value="${info.WORKS_DIVS_CD_NM}" /></td>
							<td><c:out value="${info.GENRE_CD_NM}" /></td>
							<td><c:out value="${info.WORKS_TITLE}" /></td>
							<td><c:out value="${info.WORKS_INFO}" /></td>
							<td><c:out value="${info.WORKS_SEQN}" /></td>
							<td><c:out value="${info.MAPP_WORKS_DIVS_CD_NAME}" /></td>
							<td><c:out value="${info.MAPP_WORKS_INFO}" /></td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>
<div class="box-footer clearfix text-center">
  <ul class="pagination pagination-sm no-margin">
    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
</div>