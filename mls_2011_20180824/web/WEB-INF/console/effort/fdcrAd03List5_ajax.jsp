<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%@ page buffer="1kb" autoFlush="true" %>
<script>

	function fncGoPage(page) {
		var datas = [];
		datas[0] = 'ORD';
		datas[1] = 'YYYYMM';
		datas[2] = 'WORKS_DIVS_CD';
		datas[3] = 'SEARCH_CD';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd03List5.page"/>',datas, page);
		var SEARCH_WORKS_TITLE = $('#SEARCH_WORKS_TITLE').val();
		if(SEARCH_WORKS_TITLE != ''){
			SEARCH_WORKS_TITLE = encodeURI(encodeURIComponent(SEARCH_WORKS_TITLE));
			url += '&WORKS_TITLE='+SEARCH_WORKS_TITLE;
		}
		location.href = url;
	}
	
	
	function fncGoSearch(){
		var datas = [];
		datas[0] = 'ORD';
		datas[1] = 'YYYYMM';
		datas[2] = 'WORKS_DIVS_CD';
		datas[3] = 'SEARCH_CD';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd03List5.page"/>',datas);
		var WORKS_TITLE = $('#WORKS_TITLE').val();
		if(WORKS_TITLE != ''){
			WORKS_TITLE = encodeURI(encodeURIComponent(WORKS_TITLE));
			url += '&WORKS_TITLE='+WORKS_TITLE;
		}
		location.href = url;
	}
	
	function fncExcelDown2(){
		var datas = [];
		datas[0] = 'ORD';
		datas[1] = 'YYYYMM';
		datas[2] = 'WORKS_DIVS_CD';
		datas[3] = 'SEARCH_CD';
		console.log(datas)
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd03Down2.page"/>',datas);
		var WORKS_TITLE = $('#WORKS_TITLE').val();
		if(WORKS_TITLE != ''){
			WORKS_TITLE = encodeURI(encodeURIComponent(WORKS_TITLE));
			url += '&WORKS_TITLE='+WORKS_TITLE;
		}
		location.href = url;
	}
	
	$(function(){
		init();
	})
	
	function init(){
		modalSet();
		pageMove(1);
		loadDataSet();
		//console.log("dataSet()")
	}
	
	function loadDataSet(){
		var ord = "${ORD}";
		var yyyymm = "${YYYYMM}";
		var worksDivsCd = "${WORKS_DIVS_CD}";
		var searchCd = '${SEARCH_CD}';
		
	//	console.log(ord)
	//	console.log(yyyymm)
	//	console.log(worksDivsCd)
	//	console.log(searchCd)
		
		$('#ORD').val(ord);
		$('#YYYYMM').val(yyyymm);
		$('#WORKS_DIVS_CD').val(worksDivsCd);
		$('#SEARCH_CD').val(searchCd);
	//	console.log($('#ORD').val(ord))
	//	console.log($('#YYYYMM').val(yyyymm))
	//	console.log($('#WORKS_DIVS_CD').val(worksDivsCd))
	//	console.log($('#SEARCH_CD').val(searchCd))
		
	}
	
	function pageMove(pageNum){
		$('.numBtn').attr('disabled',true);
		modalOnOff(true);
		var currentPageNum = pageNum;
		var ord = "${ORD}";
		var yyyymm = "${YYYYMM}";
		var worksDivsCd = "${WORKS_DIVS_CD}";
		var searchCd = '${SEARCH_CD}';
		var worksTitle = $('#WORKS_TITLE').val();
		var datas = {ORD : ord , YYYYMM : yyyymm, WORKS_DIVS_CD : worksDivsCd , SEARCH_CD : searchCd, pageIndex : currentPageNum};
		console.log("worksTitle : ");
		console.log(worksTitle!='');
		console.log(worksTitle);
		if(worksTitle != ''){
			worksTitle = encodeURI(encodeURIComponent(worksTitle));
			var datas = {ORD : ord , YYYYMM : yyyymm, WORKS_DIVS_CD : worksDivsCd , SEARCH_CD : searchCd, pageIndex : currentPageNum,WORKS_TITLE : worksTitle};
		}
		
		var url = "/console/effort/fdcrAd03List5Ajax.page";
		
		//비동기 통신을 하여 json타입으로 호출한다. 
		$.ajax({ 
			type: "POST",
			url: url , 
			data : datas, 
			dataType : 'json' , 
			success: function(data) {
				dataSet(data);
				
			} 
		}); 
	}
	
	function dataSet(data){
	
		var dsList = data.commandMap.ds_list;
		var stringList = [];
		
		
		$('.content').remove('')
		
		for(var i = 0 ; i < dsList.length ; i ++){
			var stringBuffer ="<tr class='content' style ='min-height:0px;text-align: center; border: 1px solid #ccc;' >";
			stringBuffer +="<td  style ='text-align: center; border: 1px solid #ccc;'>"+dsList[i].ORD+"</td>";
			stringBuffer +="<td  style ='text-align: center; border: 1px solid #ccc;'>"+dsList[i].YYYYMM+"</td>";
			stringBuffer +="<td  style ='text-align: center; border: 1px solid #ccc;'>"+dsList[i].PROC_DTTM+"</td>";
			stringBuffer +="<td  style ='text-align: center; border: 1px solid #ccc;'>"+dsList[i].WORKS_DIVS_NAME+"</td>";
			stringBuffer +="<td  style ='text-align: center; border: 1px solid #ccc;'>"+dsList[i].WORKS_TITLE+"</td>";
			stringBuffer +="<td  style ='text-align: center; border: 1px solid #ccc;'>"+dsList[i].RSLT_1ST_YN+"</td>";
			stringBuffer +="<td  style ='text-align: center; border: 1px solid #ccc;'>"+dsList[i].RSLT_2ND_YN+"</td>";
			stringBuffer +="<td  style ='text-align: center; border: 1px solid #ccc;'>"+dsList[i].RSLT_3RD_YN+"</td>";
			stringBuffer +="<td  style ='text-align: center; border: 1px solid #ccc;'>"+dsList[i].ANUC_REGI_YN+"</td>";
			stringBuffer +="<td  style ='text-align: center; border: 1px solid #ccc;'>"+dsList[i].ANUC_REGI_DTTM+"</td>";
			stringBuffer +="</tr>";
			$('#fdcrAd75List1').append(stringBuffer)
		}
		
		
		var totalCntNum = data.commandMap.ds_page[0].COUNT;
		var pageCnt = Math.floor(totalCntNum/10)+1;
		$('#paginationUl').html('')
		
		for(var i = 1 ; i <= pageCnt ; i++ ){
			var bottomString = "<li ><a class='numBtn' onclick='pageMove("+i+")'; return false;> " + i + "</a></li>";
			$('#paginationUl').append(bottomString);
		}
		$('.numBtn').attr('disabled',false);
		modalOnOff(false);
	}
	
	function modalSet(){
		var windowWidth = $( window ).width();
		var windowHeight =$( window ).height(); 
		
		/* var documentWidth = $( document ).width();
		var documentHeight = $( document ).height(); */
		
		var barWidth = $('#loadingBarContainer').outerWidth(true);
		var barHeight = $('#loadingBarContainer').outerHeight(true);
		
		//console.log(((windowWidth/2) - (barWidth/2)))
		//console.log(((windowHeight/2) - (barHeight/2)))
		
		$('#loadingBarContainer').css('z-index',99999);
		$('#loadingBarContainer').offset({left: ((windowWidth/2) - (barWidth/2))});
		$('#loadingBarContainer').offset({top: ((windowHeight/2) - (barHeight/2))});
		
		$('#modalBack').css('z-index',9999);
		$('#modalBack').css('width',windowWidth);
		$('#modalBack').css('height', screen.availHeight);
		$('#modalBack').css('opacity',0.3);
		$('#modalBack').offset({left: 0});
		$('#modalBack').offset({top: 0});
		
		modalOnOff(false);
	}
	
	function modalOnOff( setBoolean ){
		//console.log(setBoolean);
		if(setBoolean){
			$('#loadingBarContainer').css('display','block');
			$('#modalBack').css('display','block');
		}else{
			$('#loadingBarContainer').css('display','none');
			$('#modalBack').css('display','none');
		}
		
	};
</script>

<div class="box-body" style="padding:10px">

<!-- 로딩바 구현 -->
 	<div id="loadingBarContainer" style="position: absolute; background-color: white; width: 300px; height: 70px;text-align: center;padding-top: 10px;"> 
		<font style="">데이터를 불러오는 중입니다.</font>
		<img alt="" src="/images/loadingBar.gif">
	</div>
	<div id="modalBack" style="position: absolute; background-color:black;"></div>
<!--  -->

<form name="updateForm" id="updateForm" method="post">
<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
<input type="hidden" id="ORD" name="ORD" value="<c:out value="${commandMap.ORD}"/>" />
<input type="hidden" id="YYYYMM" name="YYYYMM" value="<c:out value="${commandMap.YYYYMM}"/>" />
<input type="hidden" id="WORKS_DIVS_CD" name="WORKS_DIVS_CD" value="<c:out value="${commandMap.WORKS_DIVS_CD}"/>" />
<input type="hidden" id="SEARCH_CD" name="SEARCH_CD" value="<c:out value="${commandMap.SEARCH_CD}"/>" />
<input type="hidden" id="SEARCH_WORKS_TITLE" name="SEARCH_WORKS_TITLE" value="<c:out value="${commandMap.WORKS_TITLE}"/>" />

<%-- <jsp:include page="./fdcrAd03List5_loading.jsp" flush="true" /> --%>



<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">상당한 노력 진행현황</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">저작물명</th>
									<td>
										<input type="text" id="WORKS_TITLE" name="WORKS_TITLE" value="<c:out value="${commandMap.WORKS_TITLE}"/>"/>
									</td>
								</tr>
							</tbody>
						</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<!-- <div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div> -->
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="pageMove();">검색</button></div>
		</div>
	</div>
</div>
<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncExcelDown2();return false;">Excel Down</button></div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 7%">진행차수</th>
					<th style="width: 7%">기준년월</th>
					<th style="width: 7%">수행일자</th>
					<th style="width: 10%">대상저작물</th>
					<th>저작물명</th>
					<th style="width: 10%">1차매칭여부</th>
					<th style="width: 10%">2차매칭여부</th>
					<th style="width: 10%">3차매칭여부</th>
					<th style="width: 10%">공고등록여부</th>
					<th style="width: 8%">공고일자</th>
				</tr>
			</thead>
			<%-- <tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="10">내용이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.ORD}" /></td>
								<td><c:out value="${info.YYYYMM}" /></td>
								<td><c:out value="${info.PROC_DTTM}" /></td>
								<td><c:out value="${info.WORKS_DIVS_NAME}" /></td>
								<td><c:out value="${info.WORKS_TITLE}" /></td>
								<td><c:out value="${info.RSLT_1ST_YN}" /></td>
								<td><c:out value="${info.RSLT_2ND_YN}" /></td>
								<td><c:out value="${info.RSLT_3RD_YN}" /></td>
								<td><c:out value="${info.ANUC_REGI_YN}" /></td>
								<td><c:out value="${info.ANUC_REGI_DTTM}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody> --%>
		</table>
	</div>
	<div class="box-footer clearfix text-center">
		<ul id="paginationUl" class="pagination pagination-sm no-margin">
		
		</ul>
	</div>
	
	<%-- <div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div> --%>
</div>
</form>
</div>