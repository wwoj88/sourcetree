<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncGoSave(BORD_CD,BORD_SEQN){
		var f = document.updateForm;
		f.action = '<c:url value="/console/effort/fdcrAd01Update3.page"/>';
		f.submit();
	}
	
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
<form name="updateForm" id="updateForm" method="post">
	<input type="hidden" id="BORD_CD" name="BORD_CD" value="<c:out value="${param.BORD_CD}"/>" /> 
	<input type="hidden" id="BORD_SEQN" name="BORD_SEQN" value="<c:out value="${param.BORD_SEQN}"/>" /> 
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶ 보상금 공탁공고 공고내용 보완</b></h4>
			<table class="table table-bordered" style="width: 100%">
				<tbody>
				<c:choose>
				<c:when test="${empty ds_list}">
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<input type="hidden" id="TITE" name="TITE" value="<c:out value="${info.TITE}"/>" />
					<tr>
						<th class="text-center" rowspan="9" style="width: 10%">내용</th>
					</tr>
					<tr>
						<td>
							<b>1. 저작권자를 찾는다는 취지 :</b><br>
							<textarea class="form-control" cols="50" rows="3" name="BORD_DESC" id="BORD_DESC" ><c:out value="${info.BORD_DESC }"/></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<b>2. 저작재산권자의 성명 또는 명칭,주소 또는 거소 등 :</b>
							<dl  style="padding-left:25px;">
								<dd>- 성명  : <input type="text" class="form-control" name="ANUC_ITEM_1" id="ANUC_ITEM_1" value="<c:out value="${info.ANUC_ITEM_1 }"/>" style="width: 300px; display: inline-block;"/></dd>
								<dd>- 주소 : <input type="text" class="form-control" name="ANUC_ITEM_2" id="ANUC_ITEM_2" value="<c:out value="${info.ANUC_ITEM_2 }"/>" style="width: 300px; display: inline-block;"/></dd>
								<dd>- 연락처 : <input type="text" class="form-control" name="ANUC_ITEM_3" id="ANUC_ITEM_3" value="<c:out value="${info.ANUC_ITEM_3 }"/>" style="width: 300px; display: inline-block;"/></dd>
							</dl>
						</td>
					</tr>
					<tr>
						<td>
							<b>3. 저작물의 제호 :</b>
							<dl  style="padding-left:25px;">
								<dd>- 장르 : 
									<select name="GENRE_CD" id="GENRE_CD">
									<c:forEach var="genreInfo" items="${ds_genre}" varStatus="listStatus">
										<option value="<c:out value="${genreInfo.MID_CODE }"/>" <console:fn func="isSelected" value="${genreInfo.MID_CDE }" value1="${info.GENRE_CD}"/>><c:out value="${genreInfo.CODE_NAME }"/></option>
									</c:forEach>
									</select>
								<dd>- 제호 : <input type="text" class="form-control" name="ANUC_ITEM_4" id="ANUC_ITEM_4" value="<c:out value="${info.ANUC_ITEM_4}" />" style="width: 300px; display: inline-block;"/></dd>
							</dl>
						</td>
					</tr>
					<tr>
						<td>
							<b>4. 공표시 표시된 저작재산권자의 성명(실명 또는 이명):</b><br/>
							<input type="text" class="form-control" name="ANUC_ITEM_5" id="ANUC_ITEM_5" value="<c:out value="${info.ANUC_ITEM_5 }"/>" style="width: 300px; display: inline-block;"/>
						</td>
					</tr>
					<tr>
						<td>
							<b>5. 공탁소의 명칭 및 소재지 :</b>
							<dl  style="padding-left:25px;">
								<dd>- 저작물 발행 : <input type="text" class="form-control" name="ANUC_ITEM_6" id="ANUC_ITEM_6" value="<c:out value="${info.ANUC_ITEM_6}" />" style="width: 300px; display: inline-block;"/></dd>
								<dd>- 공표 연월일 : <input type="text" class="form-control" name="ANUC_ITEM_7" id="ANUC_ITEM_7" value="<c:out value="${info.ANUC_ITEM_7}" />" style="width: 300px; display: inline-block;"/></dd>
							</dl>
						</td>
					</tr>
					<tr>
						<td>
							<b>6. 저작물의 이용 목적 :</b>
							<textarea class="form-control" cols="20" rows="3" name="ANUC_ITEM_8" id="ANUC_ITEM_8"><c:out value="${info.ANUC_ITEM_8 }"/></textarea></td>
					</tr>
					<tr>
						<td>
							<b>7. 복제물의 표지사진 등의 자료 :</b>
							<c:forEach var="file" items="${fileList}" varStatus="listStatus">
							<input type="text" class="form-control" name="FILE_NAME" id="FILE_NAME" value="<c:out value="${file.FILE_NAME}" />" style="width: 300px; display: inline-block;" disbaled/>
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>
							<b>8. 공고자 및 연락처 :</b>
							<dl  style="padding-left:25px;">
								<dd>- 공고자 : <input type="text" class="form-control" name="ANUC_ITEM_9" id="ANUC_ITEM_9" value="<c:out value="${info.ANUC_ITEM_9}" />" style="width: 300px; display: inline-block;"/></dd>
								<dd>- 주소 : <input type="text" class="form-control" name="ANUC_ITEM_10" id="ANUC_ITEM_10" value="<c:out value="${info.ANUC_ITEM_10}" />" style="width: 300px; display: inline-block;"/></dd>
								<dd>- 연락처 : <input type="text" class="form-control" name="ANUC_ITEM_11" id="ANUC_ITEM_11" value="<c:out value="${info.ANUC_ITEM_11}" />" style="width: 300px; display: inline-block;"/></dd>
								<dd>- 담당자 : <input type="text" class="form-control" name="ANUC_ITEM_12" id="ANUC_ITEM_12" value="<c:out value="${info.ANUC_ITEM_12}" />" style="width: 300px; display: inline-block;"/></dd>
							</dl>
						</td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table><br/></br>
			<span style="display: inline-block;"><b>(*) 항목은 담당자에 의해 보완된 항목입니다.</b></span>
			<div class="btn-group" style="float: right;">
				<button type="submit" class="btn btn-primary" style="float: right;"
					onclick="closePop();">닫기</button>
			</div>
			<div class="btn-group" style="display: inline-block; float: right; margin-right: 15px;">
				<button type="submit" class="btn btn-primary" style="float: right;"
					onclick="fncGoSave('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');">보완완료</button>
			</div>
		</div>
</form>
