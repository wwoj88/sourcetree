<%@ page language="java" contentType="application/vnd.ms-excel;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%
	String filename = "법정허락 대상저작물 진행현황.xls";
    
	response.setHeader( "Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "utf-8") + "" );
	response.setHeader( "Content-Description", "JSP Generated Data" );

	//response.setContentType( "application/vnd.ms-excel" );   

	//DataSet divList = (DataSet) box.getObject("EmasDivList");
	//HashMap ageMap = (HashMap) box.getObject("LicenseAgeClass");
%>
	<table border="1" cellpadding="0" cellspacing="0" style="table-layout:fixed;text-align:center" >
		<thead>
			<tr>
				<th style="background-color:#eeeeee;width: 50%" colspan="5">대상 저작물 정보</th>
				<th style="background-color:#eeeeee;width: 50%" colspan="3">매칭 저작물 정보</th>
			</tr>
			<tr>
				<th style="background-color:#eeeeee;width: 5%">번호</th>
				<th style="background-color:#eeeeee;width: 10%">구분</th>
				<th style="background-color:#eeeeee;width: 5%">장르</th>
				<th style="background-color:#eeeeee;width: 15%">제호</th>
				<th style="background-color:#eeeeee;width: 15%">저작물정보</th>
				<th style="background-color:#eeeeee;width: 5%">순번</th>
				<th style="background-color:#eeeeee;width: 10%">구분</th>
				<th style="background-color:#eeeeee;width: 35%">저작물정보</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty ds_list}">
					<tr>
						<td colspan="6" class="text-center">검색 결과가 없습니다.</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
						<tr>
							<td><c:out value="${paginationInfo.totalRecordCount-(listStatus.count)-((paginationInfo.currentPageNo-1)*paginationInfo.recordCountPerPage)+1}"/></td>
							<td><c:out value="${info.WORKS_DIVS_CD_NM}" /></td>
							<td><c:out value="${info.GENRE_CD_NM}" /></td>
							<td><c:out value="${info.WORKS_TITLE}" /></td>
							<td><c:out value="${info.WORKS_INFO}" /></td>
							<td><c:out value="${info.WORKS_SEQN}" /></td>
							<td><c:out value="${info.MAPP_WORKS_DIVS_CD_NAME}" /></td>
							<td><c:out value="${info.MAPP_WORKS_INFO}" /></td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
