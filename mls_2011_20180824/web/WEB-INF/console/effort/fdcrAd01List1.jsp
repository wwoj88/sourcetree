<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	$(function () {
		var tabIndex = '<c:out value="${commandMap.TAB_INDEX}"/>';
		
		fncLoadList(tabIndex);
	});
	
	
	function fncLoadList(tabIndex){
	  var page = '<c:out value="${page}"/>';
    
	  
		$('#TAB_INDEX').val(tabIndex);
		fncLoad('#tab'+tabIndex, '<c:url value="/console/effort/fdcrAd01List2.page"/>', {
			"TAB_INDEX" : tabIndex
		}, function(data) {
		});
		
		$("[id^=LiIdTab]").each(function(){
			$(this).removeClass("active");
		});
		$('#LiIdTab'+tabIndex).addClass("active");
		
		$("[id^=tab]").each(function(){
			$(this).removeClass("active");
		});
		$('#tab'+tabIndex).addClass("active");
		if(page){
		  fncGoPage(page);  
    }
	}
	
	function fncLoadTabCount(count1, count2, count3, count4){
		$('#tabText_0').text("승인공고("+count1+")");
		$('#tabText_1').text("이의신청현황("+count2+")");
		$('#tabText_2').text("신청[미공고]("+count3+")");
		$('#tabText_3').text("반려("+count4+")");
	}
	
	function fncGoSearch() {
		var tabIndex = $('#TAB_INDEX').val();
		var formData = $("form[name=searchForm]").serialize().replace(/%/g, '%25');
		fncLoad('#tab'+tabIndex, '<c:url value="/console/effort/fdcrAd01List2.page"/>', formData, function(data) {});
	}
	
	function fncGoPage(page) {
	   //alert(page);
	   var datas = [];
	   
	   var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd01List2.page"/>',datas, page);
	   //alert(url);
	   var formData = $("form[name=searchForm]").serialize().replace(/%/g, '%25');
	   var tabIndex = $('#TAB_INDEX').val();

	   fncLoad('#tab'+tabIndex, url, formData, function(data) {});
	 }
</script>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>
  
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>

	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
					<%-- <input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" /> --%>
					<input type="hidden" id="TAB_INDEX" name="TAB_INDEX" value="<c:out value="${commandMap.TAB_INDEX}"/>" />
					
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">구분</th>
									<td>
										<select id="SCH_WORKS_DIVS_CD" name="SCH_WORKS_DIVS_CD">
											<option value="" selected="selected">전체</option>
											<option value="2">거소불명</option>
											<option value="1">미분배</option>
											<option value="3">개인</option>
										</select>
									</td>
									<th class="text-center search-th">제목</th>
									<td>
										<input type="text" id="SCH_TITL" name="SCH_TITL"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">공고여부</th>
									<td>
										<select id="SCH_OPEN_YN" name="SCH_OPEN_YN">
											<option value="" selected="selected">전체</option>
											<option value="Y">공고</option>
											<option value="N">미공고</option>
										</select>
									</td>
									<th class="text-center search-th">이의제기</th>
									<td>
										<select id="SCH_STAT_OBJC_CD" name="SCH_STAT_OBJC_CD">
											<option value="" selected="selected">전체</option>
											<option value="1">신청</option>
											<option value="2">접수</option>
											<option value="3">처리완료</option>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>

<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
	  <li id="LiIdTab0"><a href="#tab0" data-toggle="tab" aria-expanded="true" onclick="fncLoadList('0');" id="tabText_0">승인공고</a></li>
	  <li id="LiIdTab1"><a href="#tab1" data-toggle="tab" aria-expanded="false" onclick="fncLoadList('1');" id="tabText_1">이의신청 현황</a></li>
	  <li id="LiIdTab2"><a href="#tab2" data-toggle="tab" aria-expanded="false" onclick="fncLoadList('2');" id="tabText_2">신청[미공고]</a></li>
	  <li id="LiIdTab3"><a href="#tab3" data-toggle="tab" aria-expanded="false" onclick="fncLoadList('3');" id="tabText_3">반려</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane" id="tab0">
		</div>
		<div class="tab-pane" id="tab1">
		</div>
		<div class="tab-pane" id="tab2">
		</div>
		<div class="tab-pane" id="tab3">
		</div>
	</div>
</div>