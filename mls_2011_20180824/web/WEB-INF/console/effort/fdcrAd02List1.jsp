<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	$(function () {
		var tabIndex = '<c:out value="${commandMap.TAB_INDEX}"/>';
		fncLoadList(tabIndex);
	});
	
	
	function fncLoadList(tabIndex){
		
		$('#TAB_INDEX').val(tabIndex);
		fncLoad('#tab'+tabIndex, '<c:url value="/console/effort/fdcrAd02List1Sub1.page"/>', {
			"TAB_INDEX" : tabIndex
		}, function(data) {
		});
		
		$("[id^=LiIdTab]").each(function(){
			$(this).removeClass("active");
		});
		$('#LiIdTab'+tabIndex).addClass("active");
		
		$("[id^=tab]").each(function(){
			$(this).removeClass("active");
		});
		$('#tab'+tabIndex).addClass("active");
		
	}
	
	function fncLoadTabCount(count1, count2, count3, count4){
		$('#tabText_0').text("신청 ("+count1+")");
		$('#tabText_1').text("접수("+count2+")");
		$('#tabText_2').text("처리완료("+count3+")");
	}
	
	function fncGoSearch() {
		var tabIndex = $('#TAB_INDEX').val();
		var formData = $("form[name=searchForm]").serialize().replace(/%/g, '%25');
		fncLoad('#tab'+tabIndex, '<c:url value="/console/effort/fdcrAd02List1Sub1.page"/>', formData, function(data) {});
	}
</script>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
					<input type="hidden" id="TAB_INDEX" name="TAB_INDEX" value="<c:out value="${commandMap.TAB_INDEX}"/>" />
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">장르</th>
									<td colspan="3">
										<select style="width:100px" id="SCH_GENRE_CD" name="SCH_GENRE_CD">
											<option value="" selected="selected">전체</option>
											<option value="1">어문</option>
											<option value="2">음악</option>
											<option value="3">연극</option>
											<option value="4">미술</option>
											<option value="5">건축</option>
											<option value="6">사진</option>
											<option value="7">영상</option>
											<option value="8">도형</option>
											<option value="9">컴퓨터프로그램</option>
											<option value="10">편집</option>
											<option value="11">2차적저작물</option>
											<option value="99">기타</option>
										</select>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">제호</th>
									<td colspan="3">
										<input type="text" id="SCH_WORKS_TITLE" name="SCH_WORKS_TITLE"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">신청자</th>
									<td colspan="3">
										<input type="text" id="SCH_RGST_NAME" name="SCH_RGST_NAME"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>

<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
	  <li id="LiIdTab0"><a href="#tab0" data-toggle="tab" aria-expanded="true" onclick="fncLoadList('0');" id="tabText_0">신청</a></li>
	  <li id="LiIdTab1"><a href="#tab1" data-toggle="tab" aria-expanded="false" onclick="fncLoadList('1');" id="tabText_1">접수</a></li>
	  <li id="LiIdTab2"><a href="#tab2" data-toggle="tab" aria-expanded="false" onclick="fncLoadList('2');" id="tabText_2">처리완료</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane" id="tab0">
		</div>
		<div class="tab-pane" id="tab1">
		</div>
		<div class="tab-pane" id="tab2">
		</div>
	</div>
</div>