<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
$(function () {
	fncLoadTabCount('<c:out value="${ds_count0.COUNT}"/>', '<c:out value="${ds_count1.COUNT}"/>', '<c:out value="${ds_count2.COUNT}"/>');
});

//상세 이동
function fncGoView(WORKS_ID){
	var url = '<c:url value="/console/effort/fdcrAd02UpdateForm1.page"/>?WORKS_ID='+WORKS_ID;
	location.href = url;
}

//등록폼 이동
function fncGoWriteForm(){
	var datas = [];
	datas[0] = 'MENU_SEQN';
	var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd75WriteForm1.page"/>',datas);
	location.href = url;
} 

//페이징
function fncGoPage(page) {
	var datas = [];
	var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd02List1Sub1.page"/>',datas, page);
	var formData = $("form[name=searchForm]").serialize().replace(/%/g, '%25');
	var tabIndex = $('#TAB_INDEX').val();
	fncLoad('#tab'+tabIndex, url, formData, function(data) {});
}
</script>
<form id="listForm" name="listForm">
<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>
			<div class="box" style="margin-top: 30px;">
				<!-- /.box-header -->
				<!-- 
				<div class="box-header" style="text-align:right">
					<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
				</div>
				 -->
				<div class="box-body">
					<table id="fdcrAd02List1Sub1" class="table table-bordered table-hover text-center table-list">
						<thead>
							<tr>
								<th style="width: 8%">순번</th>
								<th style="width: 10%">장르</th>
								<th>제호</th>
								<th style="width: 12%">등청자</th>
								<th style="width: 10%">등록일자</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty ds_list}">
									<tr>
										<td colspan="6" class="text-center">상당한노력 신청접수된 내용이 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
										<tr>
											<td><c:out value="${paginationInfo.totalRecordCount-(listStatus.count)-((paginationInfo.currentPageNo-1)*paginationInfo.recordCountPerPage)+1}"/></td>
											<td><c:out value="${info.GENRE_CD_NM}" /></td>
											<td class="text-left"><a href="#" onclick="fncGoView('<c:out value="${info.WORKS_ID}"/>');"><c:out value="${info.WORKS_TITLE}" /></a></td>
											<td><c:out value="${info.RGST_NAME}" /></td>
											<td><c:out value="${info.RGST_DTTM}" /></td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				<div class="box-footer clearfix text-center">
				  <ul class="pagination pagination-sm no-margin">
				    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
				  </ul>
				</div>
			</div>
