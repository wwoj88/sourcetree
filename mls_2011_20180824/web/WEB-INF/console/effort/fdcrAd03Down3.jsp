<%@ page language="java" contentType="application/vnd.ms-excel;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%
	String filename = "법정허락 대상저작물 진행현황.xls";
    
	response.setHeader( "Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "utf-8") + "" );
	response.setHeader( "Content-Description", "JSP Generated Data" );

	//response.setContentType( "application/vnd.ms-excel" );   

	//DataSet divList = (DataSet) box.getObject("EmasDivList");
	//HashMap ageMap = (HashMap) box.getObject("LicenseAgeClass");
%>
		<table border="1" cellpadding="0" cellspacing="0" style="table-layout:fixed;text-align:center" >
			<thead>
				<tr>
					<th style="background-color:#eeeeee;width: 10%">기준년월</th>
					<th style="background-color:#eeeeee;width: 12%">진행차수</th>
					<th style="background-color:#eeeeee;width: 10%">매칭일자</th>
					<th style="background-color:#eeeeee;width: 10%">대상저작물</th>
					<th style="background-color:#eeeeee;">저작물명</th>
					<th style="background-color:#eeeeee;width: 17%">법정허락대상물<br>전환여부</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="6">내용이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.YYYYMM}"/></td>
								<td><c:out value="${info.STAT_ORD}" /></td>
								<td><c:out value="${info.PROC_DTTM}" /></td>
								<td><c:out value="${info.WORKS_DIVS_NAME}" /></td>
								<td><c:out value="${info.WORKS_TITLE}" /></td>
								<td><c:out value="${info.STAT_CHNG_YN}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
