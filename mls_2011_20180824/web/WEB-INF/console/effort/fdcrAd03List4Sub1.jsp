<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncGoPage(page) {
		var datas = [];
		datas[0] = 'ORD';
		datas[1] = 'YYYYMM';
		datas[2] = 'WORKS_DIVS_CD';
		datas[3] = 'STAT_CD';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd03List4.page"/>',datas, page);
		location.href = url;
	}
	
	
</script>
<div class="box-body" style="padding:10px">
<form name="updateForm" id="updateForm" method="post">
<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
<input type="hidden" id="ORD" name="ORD" value="<c:out value="${commandMap.ORD}"/>" />
<input type="hidden" id="YYYYMM" name="YYYYMM" value="<c:out value="${commandMap.YYYYMM}"/>" />
<input type="hidden" id="WORKS_DIVS_CD" name="WORKS_DIVS_CD" value="<c:out value="${commandMap.WORKS_DIVS_CD}"/>" />
<input type="hidden" id="STAT_CD" name="STAT_CD" value="<c:out value="${commandMap.STAT_CD}"/>" />
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">상당한 노력 수행 대상저작물</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
</div>
<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="return false;">Excel Down</button></div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 7%">순번</th>
					<th style="width: 12%">대상저작물</th>
					<th style="width: 10%">장르</th>
					<th>저작물명</th>
					<th style="width: 10%">분배일자</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="5">내용이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${paginationInfo.totalRecordCount-(listStatus.count)-((paginationInfo.currentPageNo-1)*paginationInfo.recordCountPerPage)+1}"/></td>
								<td><c:out value="${info.WORKS_DIVS_CD_NM}" /></td>
								<td><c:out value="${info.GENRE_CD_NM}" /></td>
								<td><c:out value="${info.WORKS_TITLE}" /></td>
								<td><c:out value="${info.ALLT_DTTM}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
</div>
</form>
</div>