<%@ page language="java" contentType="text/html; charset=EUC-KR"
  pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
  function fileDownLoad(filePath, fileName, realFileName) {
    //alert(filePath+'+'+fileName+'+'+realFileName);
    var frm = document.getElementById("form1");
    frm.filePath.value     = filePath;
    frm.fileName.value     = fileName;
    frm.realFileName.value = realFileName;
  
    frm.target="boardView";
    frm.action = "/board/board.do?method=fileDownLoad";
    frm.submit();
  }
  //첨부파일 다운로드
  function fncDown(BORD_CD, BORD_SEQN) {
    location.href = '<c:url value="/console/legal/fdcrAd08Download1.page"/>?BORD_CD='
        + BORD_CD + '&BORD_SEQN=' + BORD_SEQN;
  }

  //회원정보 팝업
  function popupOpen(USER_IDNT) {
    var popUrl = '<c:url value="/console/legal/fdcrAd10Pop1.page"/>?USER_IDNT='
        + USER_IDNT;
    ; //팝업창에 출력될 페이지 URL
    var popOption = "width=800, height=550, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
    window.open(popUrl, "", popOption);
  }
  
  //보완 팝업
  function popupOpen2(BORD_CD, BORD_SEQN) {
    var popUrl = '<c:url value="/console/effort/fdcrAd01Pop1.page"/>?BORD_CD='+ BORD_CD + '&BORD_SEQN=' + BORD_SEQN;
    ; //팝업창에 출력될 페이지 URL
    var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
    window.open(popUrl, "", popOption);
  }

  //공고 승인 상태 수정
  function fncGoUpdate(BORD_CD, BORD_SEQN) {
    var f = document.anucStatCdForm;
    var ANUC_STAT_CD = f.ANUC_STAT_CD.value;
    
    if(ANUC_STAT_CD==3){
      if (confirm("공고승인 하시겠습니까?")) {
        f.action = '<c:url value="/console/effort/fdcrAd01Update1.page"/>';
        f.submit();
      }
    }else{
      if (confirm("상태를 저장하시겠습니까?")) {
        f.action = '<c:url value="/console/effort/fdcrAd01Update2.page"/>';
        f.submit();
      }
    }
    
  }
  
  //게시글 삭제
  function fncGoDelete(BORD_CD, BORD_SEQN) {
    if (confirm("삭제하시겠습니까?")) {
      location.href = '<c:url value="/console/effort/fdcrAd01Delete1.page"/>?BORD_CD='+ BORD_CD + '&BORD_SEQN=' + BORD_SEQN;
    }
  }

  function fncGoList(OPEN_YN, ANUC_STAT_CD, SUPLCOUNT) {
    var page= $('#pageIndex').val();
    if (OPEN_YN == 'Y' && ANUC_STAT_CD == '3') {
      location.href = '<c:url value="/console/effort/fdcrAd01List1.page"/>?TAB_INDEX=0';
    } else if (OPEN_YN == 'N' && ANUC_STAT_CD == '4') {
      location.href = '<c:url value="/console/effort/fdcrAd01List1.page"/>?TAB_INDEX=3';
    } else {
      location.href = '<c:url value="/console/effort/fdcrAd01List1.page"/>?TAB_INDEX=2&PAGE='+page;
    }
  }
  
  //메일발신함 조회
  function fncGoSendList(SUPL_ID) {
    var popUrl = '<c:url value="/console/legal/fdcrAd10SuplSendList1.page"/>?SUPL_ID='
      + SUPL_ID;
    ; //팝업창에 출력될 페이지 URL
    var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
    window.open(popUrl, "", popOption);
  }
</script>
<form name="boardForm" id="boardForm" method="post">
  <input type="hidden" id="pageIndex" name="pageIndex"
    value="<c:out value="${page}"/>" />
</form>

<div class="box" style="margin-top: 30px;">
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table table-bordered text-center">
      <tbody>
        <c:choose>
          <c:when test="${empty detailList}">
          </c:when>
          <c:otherwise>
            <c:forEach var="info" items="${detailList}" varStatus="listStatus">
              <tr>
                <th>접수번호</th>
                <td colspan="3"><c:out value="${info.RECEIPT_NO}" /></td>
              </tr>
              <tr>
                <th>제목</th>
                <td colspan="3"><c:out value="${info.TITE}" escapeXml="false"/></td>
              </tr>
              <tr>
                <th>공고일자</th>
                <td style="width: 30%"><c:if
                    test="${info.OPEN_DTTM == null}">미공고</c:if> <c:if
                    test="${info.OPEN_DTTM != null}">${info.OPEN_DTTM}</c:if></td>
                <th class="text-center" style="width: 20%">등록자</th>
                <td style="width: 30%"><a href="#"
                  onclick="popupOpen('<c:out value="${info.RGST_IDNT}"/>');return false;"><u><c:out
                        value="${info.USER_NAME}" /></u></a></td>
              </tr>
              <tr>
              <form name="form1" id="form1" method="post">
                <input type="hidden" name="filePath">
              <input type="hidden" name="fileName">
              <input type="hidden" name="realFileName">
                <th>첨부서류</th>
                <td colspan="3" id="TdIdFile"><c:choose>
                    <c:when test="${empty fileList}">
                      <span>등록된 첨부서류가 없습니다.</span>
                    </c:when>
                    <c:otherwise>
                      <c:forEach var="attach" items="${fileList}"
                        varStatus="listStatus">
                      
                        <p class="file">
                          <console:fn func="getFileIcon"
                            value="${attach.REAL_FILE_NAME}" />
                            <a href="#" onclick="fileDownLoad('${attach.FILE_PATH}','${attach.FILE_NAME}','${attach.REAL_FILE_NAME}');" 
                                            onkeypress="fileDownLoad('${attach.FILE_PATH}','${attach.FILE_NAME}','${attach.REAL_FILE_NAME}')">
                                            ${attach.FILE_NAME }
                                          
                                          </a>
                        <%--  <a href="#"
                            onclick="fncDown('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;"><c:out
                              value="${attach.FILE_NAME}" /></a> --%> <span>(파일크기: <console:fn
                              func="strFileSize" value="${attach.FILE_SIZE}" />)
                          </span>
                        </p>
                      </c:forEach>
                    </c:otherwise>
                  </c:choose></td>
                  </form>
              </tr>
              <tr>
                <th>내용
                <c:if test="${info.ANUC_STAT_CD eq '2'}"><br/>
                <button type="submit" class="btn btn-primary" onclick="popupOpen2('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">보완</button>
                </c:if>
                </th>
                <td colspan="3">
                  1. 저작권자를 찾는다는 취지 : <c:out value="${info.BORD_DESC}" escapeXml="false"/> 
                  <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="10" value2="(*)"/>
                  <br />
                  <br />
                  2. 저작재산권자의 성명 또는 명칭,주소 또는 거소 등
                    <dl  style="padding-left:25px;">
                      <dd>- 성명  : <c:out value="${info.ANUC_ITEM_1}"  escapeXml="false" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="21" value2="(*)"/></dd>
                      <dd>- 주소 : <c:out value="${info.ANUC_ITEM_2}"  escapeXml="false" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="22" value2="(*)"/></dd>
                      <dd>- 연락처 : <c:out value="${info.ANUC_ITEM_3}"  escapeXml="false" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="23" value2="(*)"/></dd>
                    </dl>
                  3. 저작물의 제호
                    <dl  style="padding-left:25px;">
                      <dd>- 장르 : <c:out value="${info.GENRE_CD_NM}" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="31" value2="(*)"/></dd>
                      <dd>- 제호 : <c:out value="${info.ANUC_ITEM_4}" escapeXml="false" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="32" value2="(*)"/></dd>
                    </dl>
                  4. 공표시 표시된 저작재산권자의 성명(실명 또는 이명) : <c:out value="${info.ANUC_ITEM_5}"  escapeXml="false"  />
                  <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="40" value2="(*)"/>
                  <br />
                  <br />
                  5. 저작물을 발행 또는 공표한자
                    <dl  style="padding-left:25px;">
                      <dd>- 저작물 발행 : <c:out value="${info.ANUC_ITEM_6}" escapeXml="false" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="51" value2="(*)"/></dd>
                      <dd>- 공표 연월일 : <c:out value="${info.ANUC_ITEM_7}" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="52" value2="(*)"/></dd>
                    </dl>
                  6. 저작물의 이용 목적 : <c:out value="${info.ANUC_ITEM_8}" escapeXml="false"/>
                  <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="60" value2="(*)"/>
                  <br />
                  <br />
                  7. 복제물의 표지사진 등의 자료 :
                  <c:forEach var="file" items="${fileList}" varStatus="listStatus">
                   <c:out value="${file.FILE_NAME}" />
                   </c:forEach>
                  <br />
                  <br />
                  8. 공고자 및 연락처
                    <dl  style="padding-left:25px;">
                      <dd>- 공고자 : <c:out value="${info.ANUC_ITEM_9}"  escapeXml="false" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="81" value2="(*)"/></dd>
                      <dd>- 주소 : <c:out value="${info.ANUC_ITEM_10}"  escapeXml="false" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="82" value2="(*)"/></dd>
                      <dd>- 연락처 : <c:out value="${info.ANUC_ITEM_11}"  escapeXml="false" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="83" value2="(*)"/></dd>
                      <dd>- 담당자 : <c:out value="${info.ANUC_ITEM_12}"  escapeXml="false" /> <console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="84" value2="(*)"/></dd>
                    </dl>
                </td>
              </tr>
              <tr>
                <th>처리상태</th>
                <td colspan="3">
                  <form name="anucStatCdForm" id="anucStatCdForm" method="post">
                  <input type="hidden" id="BORD_CD" name="BORD_CD" value="<c:out value="${commandMap.BORD_CD}"/>" /> 
                  <input type="hidden" id="BORD_SEQN" name="BORD_SEQN" value="<c:out value="${commandMap.BORD_SEQN}"/>" />
                    <select id="ANUC_STAT_CD" name="ANUC_STAT_CD">
                      <option value="1"
                        <c:if test="${info.ANUC_STAT_CD eq '1'}">selected="selected"</c:if>>신청</option>
                      <option value="2"
                        <c:if test="${info.ANUC_STAT_CD eq '2'}">selected="selected"</c:if>>보완</option>
                      <option value="3"
                        <c:if test="${info.ANUC_STAT_CD eq '3'}">selected="selected"</c:if>>공고승인</option>
                      <option value="4"
                        <c:if test="${info.ANUC_STAT_CD eq '4'}">selected="selected"</c:if>>신청반려</option>
                    </select>
                  </form>
                </td>
            </c:forEach>
          </c:otherwise>
        </c:choose>
      </tbody>
    </table>
    <b>(*) 항목은 담당자에 의해 보완된 항목입니다.</b>
  </div>
  
  <c:forEach var="info" items="${detailList}" varStatus="listStatus">
    <div class="box-footer" style="display: inline-block; width: 100%">
      <div class="btn-group">
        <button type="submit" class="btn btn-primary"
          onclick="fncGoDelete('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">삭제</button>
      </div>
      <div class="btn-group">
        <button type="submit" class="btn btn-primary"
          onclick="fncGoList('<c:out value="${info.OPEN_YN}"/>','<c:out value="${info.ANUC_STAT_CD}"/>',<c:out value="${suplListCount}"/>);">목록</button>
      </div>
      <%-- 
      <div class="btn-group" style="float: right;">
        <button type="submit" class="btn btn-primary"
          onclick="fncGoPrint('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">출력</button>
      </div>
       --%>
      <div style="display: inline-block; float: right;">
      <c:if test="${info.OPEN_YN != 'Y' && info.ANUC_STAT_CD!=4}">
        <div class="btn-group" style="float: right; margin-right: 3px;">
          <button type="submit" class="btn btn-primary"
            onclick="fncGoUpdate('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">저장</button>
        </div>
      </c:if>
      </div>
    </div>
  </c:forEach>
  <br/><br/>
  <div class="box-body">
    <table class="table table-bordered">
    <h4><b>▶ 공고정보 보완내역</b></h4>
      <thead>
        <tr>
          <th class="text-center" style="width: 5%" rowspan="2">번호</th>
          <th class="text-center" style="width: 60%" colspan="2">보완사항</th>
          <th class="text-center" style="width: 10%" rowspan="2">보완일자</th>
          <th class="text-center" style="width: 10%" rowspan="2">담당자</th>
          <th class="text-center" style="width: 15%" rowspan="2">메일수신확인</th>
        </tr>
        <tr>
          <th class="text-center" style="width: 25%">공고항목</th>
          <th class="text-center" style="width: 35%">보완 세부내역</th>
        </tr>
      </thead>
      <tbody>
        <c:choose>
          <c:when test="${empty suplList}">
            <tr>
              <td colspan="6" class="text-center">등록된 공고정보 보완내역이 없습니다.</td>
            </tr>
          </c:when>
          <c:otherwise>
            <c:forEach var="info" items="${suplList}" varStatus="listStatus">
              <tr>
                <td class="text-center"><c:out value="${info.SUPL_SEQ}" /></td>
                <td class="text-center"><c:out value="${info.SUPL_ITEM}" /></td>
                <td class="text-center"><c:out value="${info.MODI_ITEM}" /></td>
                <td class="text-center"><c:out value="${info.RGST_DTTM}" /></td>
                <td class="text-center"><c:out value="${info.RGST_NAME}" /></td>
                <td class="text-center"><a href="#" onclick="fncGoSendList('<c:out value="${info.SUPL_ID}" />');return false;"><font color="light-blue;"><b>[보기]</b></font></a></td>
              </tr>
            </c:forEach>
          </c:otherwise>
        </c:choose>
      </tbody>
    </table>
  </div>
    
</div>