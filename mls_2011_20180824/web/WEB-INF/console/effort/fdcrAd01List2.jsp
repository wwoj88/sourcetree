<%@ page language="java" contentType="text/html; charset=EUC-KR"
  pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
$(function () {
  fncLoadTabCount('<c:out value="${ds_count0.COUNT}"/>', '<c:out value="${ds_count1.COUNT}"/>', '<c:out value="${ds_count2.COUNT}"/>', '<c:out value="${ds_count3.COUNT}"/>');
});

//상세 이동
function fncGoView(BORD_SEQN){
  var page = $('#pageIndex').val();
  var url = '<c:url value="/console/effort/fdcrAd01View1.page"/>?BORD_SEQN='+BORD_SEQN+'&PAGE='+page;
  location.href = url;
}

//등록폼 이동
function fncGoWriteForm(){
  var datas = [];
  datas[0] = 'MENU_SEQN';
  var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd75WriteForm1.page"/>',datas);
  location.href = url;
} 
function fncGoPage(page) {
   //alert(page);
   var datas = [];
   
   var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd01List2.page"/>',datas, page);
   //alert(url);
   var formData = $("form[name=searchForm]").serialize().replace(/%/g, '%25');
   var tabIndex = $('#TAB_INDEX').val();

   fncLoad('#tab'+tabIndex, url, formData, function(data) {});
 }
</script>
<form id="listForm" name="listForm">
<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>
      <div class="box" style="margin-top: 30px;">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="fdcrAd01List2" class="table table-bordered table-list">
            <thead>
              <tr>
                <th style="width: 8%">순번</th>
                <th style="width: 10%">장르</th>
                <th>제호</th>
                <th style="width: 15%">등청자</th>
                <th style="width: 10%">등록일자</th>
              </tr>
            </thead>
            <tbody>
              <c:choose>
                <c:when test="${empty ds_list}">
                  <tr>
                    <td colspan="6">게시물이 없습니다.</td>
                  </tr>
                </c:when>
                <c:otherwise>
                  <c:forEach var="info" items="${ds_list}" varStatus="listStatus">
                    <tr>
                      <td><c:out value="${paginationInfo.totalRecordCount-(listStatus.count)-((paginationInfo.currentPageNo-1)*paginationInfo.recordCountPerPage)+1}"/></td>
                      <td><c:out value="${info.GENRE_CD_NM}" escapeXml="false"  /></td>
                      <td class="text-left"><a href="#" onclick="fncGoView('<c:out value="${info.BORD_SEQN}"/>');"><c:out value="${info.TITE}" escapeXml="false"  /></a></td>
                      <td><c:out value="${info.USER_NAME}" /></td>
                      <td><c:out value="${info.RGST_DTTM}" /></td>
                    </tr>
                  </c:forEach>
                </c:otherwise>
              </c:choose>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <!-- 
        <div class="box-footer" style="text-align:right">
          <div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
        </div>
         -->
         <div class="box-footer clearfix text-center">
          <ul class="pagination pagination-sm no-margin">
            <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
          </ul>
            <input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
        </div>
      </div>
