<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncGoPage(page) {
		var datas = [];
		datas[0] = 'ORD';
		datas[1] = 'YYYYMM';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd03List7.page"/>',datas, page);
		location.href = url;
	}
	
	function fncExcelDown3(){
		var datas = [];
		datas[0] = 'ORD';
		datas[1] = 'YYYYMM';
		var url = fncGetBoardParam('<c:url value="/console/effort/fdcrAd03Down3.page"/>',datas, 1);
		location.href = url;
	}
</script>
<div class="box-body" style="padding:10px">
<form name="updateForm" id="updateForm" method="post">
<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
<input type="hidden" id="ORD" name="ORD" value="<c:out value="${commandMap.ORD}"/>" />
<input type="hidden" id="YYYYMM" name="YYYYMM" value="<c:out value="${commandMap.YYYYMM}"/>" />
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">법정허락 대상저작물 진행현황</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
</div>
<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncExcelDown3();return false;">Excel Down</button></div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 10%">기준년월</th>
					<th style="width: 12%">진행차수</th>
					<th style="width: 10%">매칭일자</th>
					<th style="width: 10%">대상저작물</th>
					<th>저작물명</th>
					<th style="width: 17%">법정허락대상물<br>전환여부</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="6">내용이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.YYYYMM}"/></td>
								<td><c:out value="${info.STAT_ORD}" /></td>
								<td><c:out value="${info.PROC_DTTM}" /></td>
								<td><c:out value="${info.WORKS_DIVS_NAME}" /></td>
								<td><c:out value="${info.WORKS_TITLE}" /></td>
								<td><c:out value="${info.STAT_CHNG_YN}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
</div>
</form>
</div>