<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script type="text/javascript">
	$(function() {
		fncTreeInit();
	});

	/* tree setting */
	function fncTreeInit() {
		var datas = [];
		datas[0] = 'GROUP_YMD';
		datas[1] = 'GROUP_SEQ';
		var url = fncGetBoardParam('<c:url value="/console/menu/fdcrAdB0UpdateForm1Sub1.page"/>',datas);
		$('#tree1').jstree({
			"checkbox" : {
			      "keep_selected_style" : false
			    },
			'plugins':["checkbox"],
			'core' : {
				'data' : {
					"url" : url,
					"dataType" : "json"
				},
				"themes" :
				{
					"theme" : "classic",
					"icon" : false
				}
			}
		}).bind("select_node.jstree", function(event, data) {
			var id = data.node.id;
			id = id.substring(id.indexOf("_") + 1);
		});
	}

	//수정
	function fncUpdateGroup(){
		rules = {
				GROUP_TITLE : "required",
				GROUP_DESC : "required"
		};

		messages = {
				GROUP_TITLE : "그룹명을 입력해주세요",
				GROUP_DESC : "그룹설명을 입력해주세요"
		};
		
		if(!fncValidate(rules,messages)){
			return false;
		}
		
		var checked_list = $('#tree1').jstree("get_checked",true);
		var checked_ids = [];
		$.each(checked_list, function() {
			
			if($.inArray(this.id, checked_ids) === -1 && this.id != '#' && this.id != 'menuId_0' )checked_ids.push(this.id);
			
			$.each(this.parents,function(i, key){
				if(key != null && key != '' && key != 'undefined' && key != '#' && key != 'menuId_0'){
					if(key != 0 && $.inArray(key, checked_ids) === -1)checked_ids.push(key);
				}	
			});
			
		});
		document.getElementById('MENU_ID').value = checked_ids.join(",");
		
		var f = document.bodyForm;
		f.action = '<c:url value="/console/menu/fdcrAdB0Update1.page"/>';
		f.submit();
	}
	
	//취소
	function fncCancel(){
		var datas = [];
		datas[0] = 'GROUP_YMD';
		datas[1] = 'GROUP_SEQ';
		var url = fncGetBoardParam('<c:url value="/console/menu/fdcrAdB0List1.page"/>',datas);
		location.href = url;
	}
</script>
<form name="bodyForm" id="bodyForm" method="post" onsubmit="return false;">
	<input type="hidden" id="GROUP_YMD" name="GROUP_YMD" value="<c:out value="${ds_group_info.GROUP_YMD}"/>" /> 
	<input type="hidden" id="GROUP_SEQ" name="GROUP_SEQ" value="<c:out value="${ds_group_info.GROUP_SEQ}"/>" /> 
	<input type="hidden" id="MENU_ID" name="MENU_ID" value="" />

<div class="box">
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th style="width: 20%">그룹명</th>
					<td style="width: 80%"><input class="form-control" type="text" name="GROUP_TITLE" id="GROUP_TITLE" value="<c:out value="${ds_group_info.GROUP_TITLE}"/>"/></td>
				</tr>
				<tr>
					<th style="width: 20%">그룹설명</th>
					<td style="width: 80%"><input class="form-control" type="text" name="GROUP_DESC" id="GROUP_DESC" value="<c:out value="${ds_group_info.GROUP_DESC}"/>"/></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="box" style="margin-top: 30px;">
	<div class="box-body">
		<div id="tree"
			style="width: 900px; height: 590px; border:1px solid #ddd;float: left; margin-bottom: 10px; padding:10px; overflow-y:scroll;">
			<div id="omTreeDiv" >
				<div id="tree1"></div>
			</div>
		</div>
	</div>
</div>
<div class="box-footer" style="text-align:right">
	<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncUpdateGroup();return false;">저장</button></div>
	<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncCancel();return false;">취소</button></div>
</div>
</form>