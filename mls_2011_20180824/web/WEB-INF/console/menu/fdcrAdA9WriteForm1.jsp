<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<form name="regForm" id="regForm" method="post">
<input type="hidden" name="THREADED" value="<c:out value="${menu.MENU_ID}"/>"/>
<input type="hidden" name="MENU_DEPTH" value="<c:out value="${menu.MENU_DEPTH+1}"/>"/>
<input type="hidden" name="MENU_KIND" value=""/>

					<div style="text-align:right;height:30px">
						<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncWrite();return false;">등록</button></div>
					</div>
					<table class="table table-bordered">
						<caption></caption>
						<colgroup>
							<col width="30%">
							<col width="70%">
						</colgroup>
						<tbody>
						<tr>
							<th>메뉴이름</th>
							<td><input class="form-control" type="text" name="MENU_TITLE" id="MENU_TITLE" value=""/></td>
						</tr>
						<tr>
							<th>경로</th>
							<td><input class="form-control" type="text" name="MENU_URL" id="MENU_URL" value=""/></td>
						</tr>
						<tr>
							<th>설명</th>
							<td><input class="form-control" type="text" name="MENU_DESC" id="MENU_DESC" value=""/></td>
						</tr>
						</tbody>
					</table>
</form>		