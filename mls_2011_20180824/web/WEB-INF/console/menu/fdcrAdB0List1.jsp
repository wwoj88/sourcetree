<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//상세 이동
	function fncGoUpdateForm(GROUP_YMD,GROUP_SEQ){
		var url = '<c:url value="/console/menu/fdcrAdB0UpdateForm1.page"/>?GROUP_YMD='+GROUP_YMD+'&GROUP_SEQ='+GROUP_SEQ;
		location.href = url;
	}
	// 등록
	function fncGoWriteForm(){
		var url = '<c:url value="/console/menu/fdcrAdB0UpdateForm1.page"/>';
		location.href = url;
	}
	
	//그룹 삭제
	function fncDeleteGroup(){
		var f = document.bodyForm;
		f.action = '<c:url value="/console/menu/fdcrAdB0Delete1.page"/>';
		f.submit();
	} 
</script>
<form name="bodyForm" id="bodyForm" method="post">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN"
		value="<c:out value="${param.MENU_SEQN}"/>" /> <input type="hidden"
		id="pageIndex" name="pageIndex"
		value="<c:out value="${commandMap.pageIndex}"/>" />

<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAdB0List1" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="text-center" style="width: 8%">선택</th>
					<th class="text-center" style="width: 8%">순번</th>
					<th class="text-center" style="width: 10%">그룹코드</th>
					<th class="text-center" style="width: 25%">그룹명</th>
					<th class="text-center">그룹설명</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_group_info}">
						<tr>
							<td colspan="5">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_group_info}" varStatus="listStatus">
						
							<tr>
								<td class="text-center"><input type="checkbox" name="GROUP_VALUE" id="CHK" value="<c:out value="${info.GROUP_YMD}_${info.GROUP_SEQ}"/>"/></td>
								<td class="text-center"><c:out value="${listStatus.count}" /></td>
								<td class="text-center"><c:out value="${info.GROUP_ID}" /></td>
								<td class="text-center"><a href="#"  onclick="fncGoUpdateForm('<c:out value="${info.GROUP_YMD}" />','<c:out value="${info.GROUP_SEQ}" />');return false;"><c:out value="${info.GROUP_TITLE}" /></a></td>
								<td class="text-center"><c:out value="${info.GROUP_DESC}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncDeleteGroup();return false;">삭제</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
	</div>
</div>
</form>