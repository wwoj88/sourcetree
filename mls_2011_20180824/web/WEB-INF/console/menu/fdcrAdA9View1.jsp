<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<form name="regForm" id="regForm" method="post">
<input type="hidden" name="MENU_ID" value="<c:out value="${menu.MENU_ID}"/>"/>
<input type="hidden" name="THREADED" value="<c:out value="${menu.THREADED}"/>"/>
<input type="hidden" name="PARENT" value="<c:out value="${menu.PARENT}"/>"/>
<input type="hidden" name="MENU_DEPTH" value="<c:out value="${menu.MENU_DEPTH}"/>"/>
<input type="hidden" name="MENU_KIND" value="<c:out value="${menu.MENU_KIND}"/>"/>
<input type="hidden" name="MENU_ORDER" value="<c:out value="${menu.MENU_ORDER}"/>"/>

<div style="text-align:right;height:30px">
	<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncAddMenu('<c:out value="${menu.MENU_ID}"/>');return false;">추가</button></div>
	<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncUpdate('<c:out value="${menu.MENU_ID}"/>');return false;">수정</button></div>
	<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncDelete('<c:out value="${menu.MENU_ID}"/>');return false;">삭제</button></div>
</div>
<table class="table table-bordered">
	<caption></caption>
	<colgroup>
		<col width="30%">
		<col width="70%">
	</colgroup>
	<tbody>
	<tr>
		<th>메뉴이름</th>
		<td><input class="form-control" type="text" name="MENU_TITLE" id="MENU_TITLE" value="<c:out value="${menu.MENU_TITLE }"/>"/></td>
	</tr>
	<tr>
		<th>경로</th>
		<td><input class="form-control" type="text" name="MENU_URL" id="MENU_URL" value="<c:out value="${menu.MENU_URL }"/>"/></td>
	</tr>
	<tr>
		<th>설명</th>
		<td><input class="form-control" type="text" name="MENU_DESC" id="MENU_DESC" value="<c:out value="${menu.MENU_DESC }"/>"/></td>
	</tr>
	</tbody>
</table>
</form>		