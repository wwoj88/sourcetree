<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script type="text/javascript">
	$(function() {
		init();
	});

	/* tree setting */
	function fncTreeInit() {
		$('#tree1').jstree({
			'core' : {
				'data' : {
					"url" : "<c:url value='/console/menu/fdcrAdA9List1Sub1.page'/>",
					"dataType" : "json"
				},
				"themes" :
				{
					"theme" : "classic",
					"icon" : false
				}
			}
		}).bind("select_node.jstree", function(event, data) {
			var id = data.node.id;
			id = id.substring(id.indexOf("_") + 1);
			fncLoad('#menuFormDiv', '<c:url value="/console/menu/fdcrAdA9View1.page"/>', {
				"MENU_ID" : id
			}, function(data) {
			});
		});
	}

	/* init */
	function init(menuId) {
		if (menuId) {
			fncLoad('#menuFormDiv', '<c:url value="/console/menu/fdcrAdA9View1.page"/>', {
				"MENU_ID" : menuId
			}, function(data) {
			});
		} else {
			fncLoad('#menuFormDiv', '<c:url value="/console/menu/fdcrAdA9View1.page"/>',
					{}, function(data) {
						fncTreeInit();
						fncLoad('#menuFormDiv', '<c:url value="/console/menu/fdcrAdA9View1.page"/>', {
							"MENU_ID" : '126'
						}, function(data) {
						});
					});
		}

	}

	/* 메뉴추가 페이지 */
	function fncAddMenu(MENU_ID) {
		fncLoad('#menuFormDiv',
				'<c:url value="/console/menu/fdcrAdA9WriteForm1.page"/>', {"MENU_ID":MENU_ID},
				function(data) {
				});
	}
	
	/* 메뉴 추가 */
	function fncWrite() {
		var formData = $("form[name=regForm]").serialize().replace(/%/g, '%25');
		fncPost('<c:url value="/console/menu/fdcrAdA9Write1.page"/>',formData,function(data){
			if(data == 'N'){
				alert("실패하였습니다.");
			}else{
				$('#tree1').jstree('refresh');
				init(data);
			}
		});
	}

	/* 메뉴 수정 */
	function fncUpdate(MENU_ID) {
		if(MENU_ID){
			var formData = $("form[name=regForm]").serialize().replace(/%/g, '%25');
			fncPost('<c:url value="/console/menu/fdcrAdA9Update1.page"/>',formData,function(data){
				if(data == 'N'){
					alert("실패하였습니다.");
				}else{
					alert("메뉴가 수정되었습니다.");
					$('#tree1').jstree('refresh');
					init(data);
				}
			});
		}
	}
	
	/* 메뉴 삭제  */
	function fncDelete(MENU_ID){
		if(MENU_ID){
			fncPost('<c:url value="/console/menu/fdcrAdA9Delete1.page"/>',{"MENU_ID":MENU_ID},function(data){
				if(data == 'Y'){
					alert("메뉴가 삭제되었습니다.");
					$('#tree1').jstree('refresh');
					init();
				}else{
					alert("실패하였습니다.")
				}
			});
		}
	}
</script>

<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN"
		value="<c:out value="${param.MENU_SEQN}"/>" /> <input type="hidden"
		id="pageIndex" name="pageIndex"
		value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box" style="margin-top: 30px;">
	<div class="box-body">
		<div id="tree"
			style="width: 340px; height: 590px; border:1px solid #ddd;float: left; margin-bottom: 10px; padding:10px; overflow-y:scroll;">
			<div id="omTreeDiv" >
				<div id="tree1"></div>
			</div>
		</div>
		<div class="menu_right" id="menuFormDiv" style="float:left;width:530px;height: 590px; border:1px solid #ddd;padding:10px;margin-left:20px;">
		</div>
	</div>
</div>