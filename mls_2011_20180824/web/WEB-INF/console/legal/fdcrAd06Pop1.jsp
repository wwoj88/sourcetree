<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶ 이용 승인신청 진행상태내역</b></h4>
			<table class="table table-bordered">
				<thead>
					<tr>
					<th class="text-center" style="width: 20%">일시</th>
					<th class="text-center" style="width: 15%">상태</th>
					<th class="text-center">비고</th>
					</tr>
				</thead>
				<tbody>
				<c:choose>
				<c:when test="${empty ds_list}">
					<tr>
						<td class="text-center" colspan="3">등록된 진행상태 내역이 없습니다.</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<tr>
						<td class="text-center"><c:out value="${info.CHNG_DTTM }"/></td>
						<td class="text-center">
						<c:if test="${info.STAT_CD eq '1'}">임시저장</c:if>
						<c:if test="${info.STAT_CD eq '2'}">신청완료</c:if>
						<c:if test="${info.STAT_CD eq '3'}">보완요청</c:if>
						<c:if test="${info.STAT_CD eq '4'}">보완임시저장</c:if>
						<c:if test="${info.STAT_CD eq '5'}">보완완료</c:if>
						<c:if test="${info.STAT_CD eq '6'}">반려</c:if>
						<c:if test="${info.STAT_CD eq '7'}">결제요청</c:if>
						<c:if test="${info.STAT_CD eq '8'}">접수완료</c:if>
						<c:if test="${info.STAT_CD eq '9'}">심의중</c:if>
						<c:if test="${info.STAT_CD eq '10'}">심의완료</c:if>
						</td>
						<td class="text-center"><c:out value="${info.STAT_MEMO }"/></td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
			<div class="btn-group" style="float: right; margin-top: 20px;">
				<button type="submit" class="btn btn-primary" style="float: right;"
					onclick="closePop();">닫기</button>
			</div>
		</div>
