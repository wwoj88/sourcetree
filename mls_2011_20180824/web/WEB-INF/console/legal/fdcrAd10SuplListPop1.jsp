<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//검색
	function fncGoSearch() {
		var KEYWORD = $('#KEYWORD').val();
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd10SuplSendList1.page"/>')+'&KEYWORD='+KEYWORD;
		location.href = url;
	}
	
	//보완내역 상세팝업
	function fncGoDetail(SUPL_ID) {
		var popUrl = '<c:url value="/console/legal/fdcrAd10SuplListPop1.page"/>?SUPL_ID='
			+ SUPL_ID;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
	<!-- /.box-header -->
	<h4><b>▶ 공고정보 보완내역</b></h4>
	<!-- /.box-header -->
	<div class="box-body">
			<table class="table table-bordered" style="width: 100%">
				<thead>
					<tr>
						<th class="text-center" style="width: 8%" rowspan="2">번호</th>
						<th class="text-center" style="width: 60%" colspan="2">보완사항</th>
						<th class="text-center" style="width: 10%" rowspan="2">보완일자</th>
						<th class="text-center" style="width: 10%" rowspan="2">담당자</th>
					</tr>
					<tr>
						<th class="text-center" style="width: 25%">공고항목</th>
						<th class="text-center" style="width: 35%">보완 세부내역</th>
					</tr>
				</thead>
				<tbody>
				<c:choose>
				<c:when test="${empty ds_list}">
					<tr>
						<td colspan="5" class="text-center">보완내역 정보가 없습니다.</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
						<tr>
							<td class="text-center"><c:out value="${info.SUPL_SEQ}" /></td>
							<td class="text-center">
							<c:if test="${info.SUPL_ITEM_CD == '10'}">1. 저작물의 제호</c:if>
							<c:if test="${info.SUPL_ITEM_CD == '20'}">2. 저작자 및 저작재산권자의 성명</c:if>
							<c:if test="${info.SUPL_ITEM_CD == '30'}">3. 저작물 이용의 내용</c:if>
							<c:if test="${info.SUPL_ITEM_CD == '40'}">4. 공탁금액</c:if>
							<c:if test="${info.SUPL_ITEM_CD == '50'}">5. 공탁소의 명칭 및 소재지</c:if>
							<c:if test="${info.SUPL_ITEM_CD == '60'}">6. 공탁근거</c:if>
							<c:if test="${info.SUPL_ITEM_CD == '71'}">7. 저작물이용자의 주소</c:if>
							<c:if test="${info.SUPL_ITEM_CD == '72'}">8. 저작물이용자의 성명</c:if>
							</td>
							<td class="text-center"><c:out value="${info.MODI_ITEM}" /></td>
							<td class="text-center"><c:out value="${info.RGST_DTTM}" /></td>
							<td class="text-center"><c:out value="${info.RGST_NAME}" /></td>
						</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		</div>
