<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
//첨부파일 다운로드
function fncDown(BORD_CD,BORD_SEQN){
	location.href = '<c:url value="/console/legal/fdcrAd08Download1.page"/>?BORD_CD='+BORD_CD+'&BORD_SEQN='+BORD_SEQN;
}

//수정 폼으로 이동
function fncGoUpdateForm(BORD_CD,BORD_SEQN){
	/* 
	var datas = [];
	datas[0] = 'BORD_CD';
	datas[1] = 'BORD_SEQN';
	 */
	 location.href = '<c:url value="/console/legal/fdcrAd08UpdateForm1.page"/>?BORD_CD='+BORD_CD+'&BORD_SEQN='+BORD_SEQN;
} 

//게시글 삭제
function fncGoDelete(BORD_CD,BORD_SEQN){
	if(confirm("삭제하시겠습니까?")){
	 location.href = '<c:url value="/console/legal/fdcrAd08Delete1.page"/>?BORD_CD='+BORD_CD+'&BORD_SEQN='+BORD_SEQN;
	}
}

function fncGoList(){
	location.href = '<c:url value="/console/legal/fdcrAd08List1.page"/>';
}

</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
</form>
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
			<table class="table table-bordered text-center">
				<tbody>
				<c:choose>
				<c:when test="${empty detailList}">
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${detailList}" varStatus="listStatus">
					<tr>
						<th style="width: 20%">접수번호</th>
						<td style="width: 30%"><c:out value="${info.RECEIPT_NO}"/></td>
						<th style="width: 20%">구분</th>
						<td style="width: 30%"><c:out value="${info.DIVS_CD_NM}"/></td>
					</tr>
					 
					<tr>
						<th>제목</th>
						<td colspan="3"><c:out value="${info.TITE}"/></td>
					</tr>
					
					<tr>
						<th style="width: 20%">공고일자</th>
						<td style="width: 30%"><c:out value="${info.OPEN_DTTM}"/></td>
						<th style="width: 20%">공고자</th>
						<td style="width: 30%"><c:out value="${info.RGST_NAME}"/></td>
					</tr>
					
					<tr>
						<th>첨부서류</th>
						<td colspan="3" id="TdIdFile">
						<c:choose>
						<c:when test="${empty fileList}">
							<span>등록된 첨부서류가 없습니다.</span>
						</c:when>
						<c:otherwise>
							<c:forEach var="attach" items="${fileList}"
								varStatus="listStatus">
							<p class="file">
								<console:fn func="getFileIcon" value="${attach.REAL_FILE_NAME}"/>
								<a href="#" onclick="fncDown('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;"><c:out value="${attach.REAL_FILE_NAME}"/></a>
								<span>(파일크기: <console:fn func="strFileSize" value="${attach.FILE_SIZE}"/>)</span>
							</p>
							</c:forEach>
						</c:otherwise>
						</c:choose>
						</td>
					</tr>
					
					<tr>
						<th>내용</th>
						<td colspan="3">
						문화체육관광부 공고 제 &nbsp;&nbsp;<c:out value="${info.ANUC_ITEM_1}"/>호:&nbsp;&nbsp; 
						<c:out value="${info.ANUC_ITEM_2}"/><br/><br/>
						저작물 이용의 법정허락 신청 내용 공고<br/>
						1. 신청인: <c:out value="${info.ANUC_ITEM_3}"/><br/><br/>
						2. 대상저작물<br/>
						&nbsp;&nbsp;&nbsp;&nbsp;가. 저작물 종류 : <c:out value="${info.ANUC_ITEM_4}"/><br/>
						&nbsp;&nbsp;&nbsp;&nbsp;나. 저작물 제호(명칭) : <c:out value="${info.ANUC_ITEM_5}"/><br/>
						&nbsp;&nbsp;&nbsp;&nbsp;다. 공표시 표시된 저작자 성명 : <c:out value="${info.ANUC_ITEM_6}"/><br/>
						&nbsp;&nbsp;&nbsp;&nbsp;라. 공표 매체 : <c:out value="${info.ANUC_ITEM_7}"/><br/>
						&nbsp;&nbsp;&nbsp;&nbsp;마. 공표연월일 : <c:out value="${info.ANUC_ITEM_8}"/><br/><br/>
						3. 신청목적 : <c:out value="${info.ANUC_ITEM_9}"/><br/><br/>
						4. 법적근거 : <c:out value="${info.ANUC_ITEM_10}"/><br/><br/>
						5. 신청경위 : <c:out value="${info.ANUC_ITEM_11}"/><br/><br/>
						6. 이의신청 : 동 저작물의 이용승인신청에 대하여 이의를 제기하려는 저작재산권자는 승인이전에 다음 사항이 기재된 서류 등을 한국저작권위원회 심의조사팀(055-792-0083 , article50@copyright.or.kr)으로 제출 하시기 바랍니다.<br/>
									  &nbsp;&nbsp;&nbsp;&nbsp; 가. 법정허락 신청 내용에 대한 이의 내용 및 그 이유<br/>
									  &nbsp;&nbsp;&nbsp;&nbsp; 나. 성명(단체인 경우 단체명과 그 대표자)/주소/전화번호<br/>
									   &nbsp;&nbsp;&nbsp;&nbsp;다. 자신이 그 권리자로 표시된 저작권 등의 등록증 사본 또는 그에 상당하는 자료<br/>
									   &nbsp;&nbsp;&nbsp;&nbsp;라. 자신의 성명이나 또는 예명, 아호/약칭 등으로서 널리 알려진 것이 표시되어 있는 그 저작물의 사본 또는 그에 상당하는 자료  
						</td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		</div>
	<c:forEach var="info" items="${detailList}" varStatus="listStatus">
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoUpdateForm('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">수정</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoDelete('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">삭제</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoList();">목록</button></div>
	</div>
	</c:forEach>
</div>