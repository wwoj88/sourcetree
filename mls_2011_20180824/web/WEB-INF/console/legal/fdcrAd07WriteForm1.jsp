<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<style>
.table > tbody > tr > th {
	text-align: center;
}
</style>
<script>
$(document).ready(function(){
    $("#APPLY_TYPE_01").change(function(){
        if($("#APPLY_TYPE_01").is(":checked")){
        	$('input:checkbox[id="APPLY_TYPE_01_1"]').attr("checked", true); //단일건
        	$('input:checkbox[id="APPLY_TYPE_01_2"]').attr("checked", true); //단일건
        }else{
        	$('input:checkbox[id="APPLY_TYPE_01_1"]').attr("checked", false); //단일건
        	$('input:checkbox[id="APPLY_TYPE_01_2"]').attr("checked", false); //단일건
        }
    });
    $("#APPLY_TYPE_02").change(function(){
        if($("#APPLY_TYPE_02").is(":checked")){
        	$('input:checkbox[id="APPLY_TYPE_02_1"]').attr("checked", true); //단일건
        	$('input:checkbox[id="APPLY_TYPE_02_2"]').attr("checked", true); //단일건
        }else{
        	$('input:checkbox[id="APPLY_TYPE_02_1"]').attr("checked", false); //단일건
        	$('input:checkbox[id="APPLY_TYPE_02_2"]').attr("checked", false); //단일건
        }
    });
    $("#APPLY_TYPE_03").change(function(){
        if($("#APPLY_TYPE_03").is(":checked")){
        	$('input:checkbox[id="APPLY_TYPE_03_1"]').attr("checked", true); //단일건
        	$('input:checkbox[id="APPLY_TYPE_03_2"]').attr("checked", true); //단일건
        }else{
        	$('input:checkbox[id="APPLY_TYPE_03_1"]').attr("checked", false); //단일건
        	$('input:checkbox[id="APPLY_TYPE_03_2"]').attr("checked", false); //단일건
        }
    });
    $("#APPLY_TYPE_04").change(function(){
        if($("#APPLY_TYPE_04").is(":checked")){
        	$('input:checkbox[id="APPLY_TYPE_04_1"]').attr("checked", true); //단일건
        	$('input:checkbox[id="APPLY_TYPE_04_2"]').attr("checked", true); //단일건
        }else{
        	$('input:checkbox[id="APPLY_TYPE_04_1"]').attr("checked", false); //단일건
        	$('input:checkbox[id="APPLY_TYPE_04_2"]').attr("checked", false); //단일건
        }
    });
    $("#APPLY_TYPE_05").change(function(){
        if($("#APPLY_TYPE_05").is(":checked")){
        	$('input:checkbox[id="APPLY_TYPE_05_1"]').attr("checked", true); //단일건
        	$('input:checkbox[id="APPLY_TYPE_05_2"]').attr("checked", true); //단일건
        }else{
        	$('input:checkbox[id="APPLY_TYPE_05_1"]').attr("checked", false); //단일건
        	$('input:checkbox[id="APPLY_TYPE_05_2"]').attr("checked", false); //단일건
        }
    });
});

	//등록
	function fncWrite() {
		/* 
		rules = {
			APPLY_NO : "required",
			RECEIPT_NO : "required",
			APPLY_WORKS_TITL : "required"
		};

		messages = {
			RGST_IDNT : "신청번호를 입력해주세요",
			RECEIPT_NO : "접수번호를 입력해주세요",
			APPLY_WORKS_TITL : "제호를 입력해주세요"
		};

		if (!fncValidate(rules, messages)) {
			return false;
		}
		 */

		var f = document.writeForm;
		f.action = '<c:url value="/console/legal/fdcrAd07Regi1.page"/>';
		f.submit();
	}

	//취소
	function fncCancel() {
		var datas = [];
		datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam(
				'<c:url value="/console/ntcn/fdcrAd75List1.page"/>', datas);
		location.href = url;
	}
	
	//이용승인신청 명세서 정보 추가
	function fncAddRow(){
		var table1 = document.getElementById('table1');
		var row = table1.insertRow(table1.rows.length);
		
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		var cell3 = row.insertCell(2);
		var cell4 = row.insertCell(3);
		var cell5 = row.insertCell(4);
		var cell6 = row.insertCell(5);
		
		var rowCount = table1.rows.length;
		$("#WORKS_SEQN").val(rowCount-1);
		   
		/* cell1.innerHTML = "<td><input class='form-control' style='border:0px;' type='text' id='WORKS_SEQN' name='WORKS_SEQN' value='" +rowCount - 1+"'></td>"; */
		//cell1.innerHTML = rowCount-1;
		cell1.innerHTML = "<td><input class='form-control' style='border:0px;' tpye='text' name='WORKS_SEQN2' id='WORKS_SEQN2' value='" +document.getElementById('WORKS_SEQN').value+"'></td>";
		cell2.innerHTML = "<td><input class='form-control' style='border:0px;' tpye='text' name='APPLY_WORKS_TITL2' id='APPLY_WORKS_TITL2' value='" +document.getElementById('APPLY_WORKS_TITL').value+"'></td>";
		cell3.innerHTML = "<td><input class='form-control' style='border:0px;' tpye='text' name='APPLY_WORKS_KIND2' id='APPLY_WORKS_KIND2' value='" +document.getElementById('APPLY_WORKS_KIND').value+"'></td>";
		cell4.innerHTML = "<td><input class='form-control' style='border:0px;' tpye='text' name='APPLY_WORKS_FORM2' id='APPLY_WORKS_FORM2' value='" +document.getElementById('APPLY_WORKS_FORM').value+"'></td>";
		cell5.innerHTML = "<td><input class='form-control' style='border:0px;' tpye='text' name='PUBL_YMD2' id='PUBL_YMD2' value=''></td>";
		cell6.innerHTML = "<td><input class='form-control' style='border:0px;' tpye='text' name='COPT_HODR_NAME2' id='COPT_HODR_NAME2' value=''></td>";
		
		var strTitle = $('#APPLY_WORKS_TITL').val();
		var strKind = $('#APPLY_WORKS_KIND').val();
		var strForm = $('#APPLY_WORKS_FORM').val();
		
		$("#APPLY_WORKS_TITL3").val(strTitle);
		$("#APPLY_WORKS_KIND3").val(strKind);
		$("#APPLY_WORKS_FORM3").val(strForm);
	}
	
	function fncDeleteRow() {
	    var table1 = document.getElementById('table1');
	    if (table1.rows.length < 1) return;
	    table1.deleteRow( table1.rows.length-1 ); // 하단부터 삭제
	  }
	
	function fncPublYmd(){
		var a=document.getElementById("PUBL_YMD");
		var b=document.getElementById("PUBL_YMD2");
		
		b.value = a.value;
	}
	
	function fncCoptHodrName(){
		var a=document.getElementById("COPT_HODR_NAME");
		var b=document.getElementById("COPT_HODR_NAME2");
		
		b.value = a.value;
	}
</script>
<form name="writeForm" id="writeForm" method="post" enctype="multipart/form-data">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${param.MENU_SEQN}"/>" /> 
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
	<input type="hidden" id="WORKS_SEQN" name="WORKS_SEQN"/> 
	<div class="box" style="margin-top: 30px;">
		<!-- /.box-header -->
		<div class="box-body">
			<div style="margin: 7px 0 15px 5px;"><font size="3">● 이용 승인신청 정보</font></div>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th style="width: 20%">신청번호</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLY_NO" id="APPLY_NO"/></td>
						<th style="width: 20%">접수번호</th>
						<td style="width: 30%"><input class="form-control" type="text" name="RECEIPT_NO" id="RECEIPT_NO"/></td>
					</tr>
					 
					<tr>
						<td rowspan="2" colspan="3" style="padding-left: 322px;">
							<input type="checkbox" name="APPLY_TYPE_01" id="APPLY_TYPE_01" value="Y"> 저작물<br/>
							<input type="checkbox" name="APPLY_TYPE_02" id="APPLY_TYPE_02" value="Y"> 실연
							<input type="checkbox" name="APPLY_TYPE_03" id="APPLY_TYPE_03" value="Y"> 음반
							<input type="checkbox" name="APPLY_TYPE_04" id="APPLY_TYPE_04" value="Y"> 방송
							<font size="4" style="padding-left: 150px;">이용 승인신청서</font><br/> 
							<input type="checkbox" name="APPLY_TYPE_05" id="APPLY_TYPE_05" value="Y"> 데이터베이스
						</td>
						<td>처리기간</td>
					</tr>
					<tr>
						<td>40일</td>
					</tr>
					
					<tr>
						<th>제호</th>
						<td colspan="3">
						<input type="checkbox" name="HTML_YSNO" id="HTML_YSNO" value="Y" <console:fn func="isChecked" value="Y" value1="${info.HTML_YSNO}"/>> 여러건 신청 : 총 <input style="width: 10%; display: inline;" class="form-control" type="text" name="APPLY_WORKS_CNT" id="APPLY_WORKS_CNT"/>건<br/>
						<input class="form-control" type="text" name="APPLY_WORKS_TITL" id="APPLY_WORKS_TITL"/>
						</td>
					</tr>
					<tr>
						<th style="width: 20%">종류</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLY_WORKS_KIND" id="APPLY_WORKS_KIND"/></td>
						<th style="width: 20%">형태 및 수량</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLY_WORKS_FORM" id="APPLY_WORKS_FORM"/></td>
					</tr>
					<tr>
						<th>이용의 내용</th>
						<td colspan="3">
							<textarea class="form-control" cols="5" id="USEX_DESC" name="USEX_DESC"></textarea>
						</td>
					</tr>
					<tr>
						<th>승인신청사유</th>
						<td colspan="3">
							<textarea class="form-control" cols="5" id="APPLY_REAS" name="APPLY_REAS"></textarea>
						</td>
					</tr>
					<tr>
						<th>보상금액</th>
						<td colspan="3"><input style="width: 50%" class="form-control" type="text" name="CPST_AMNT" id="CPST_AMNT"/></td>
					</tr>
					<tr> 
						<th>신청인 성명<br/>(법인명)</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLR_NAME" id="APPLR_NAME"/></td>
						<th>신청인 주민등록번호<br/>(법인등록번호)</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLR_RESD_CORP_NUMB" id="APPLR_RESD_CORP_NUMB"/></td>
					</tr>
					<tr> 
						<th>신청인 주소</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLR_ADDR" id="APPLR_ADDR"/></td>
						<th>신청인 전화번호</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLR_TELX" id="APPLR_TELX"/></td>
					</tr>
					<tr> 
						<th>대리인 성명<br/>(법인명)</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLY_PROXY_NAME" id="APPLY_PROXY_NAME"/></td>
						<th>대리인 주민등록번호<br/>(법인등록번호)</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLY_PROXY_RESD_CORP_NUMB" id="APPLY_PROXY_RESD_CORP_NUMB"/></td>
					</tr>
					<tr> 
						<th>대리인 주소</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLY_PROXY_ADDR" id="APPLY_PROXY_ADDR"/></td>
						<th>대리인 전화번호</th>
						<td style="width: 30%"><input class="form-control" type="text" name="APPLY_PROXY_TELX" id="APPLY_PROXY_TELX"/></td>
					</tr>
					<tr>
						<td colspan="4" style="padding-left: 322px;">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="APPLY_RAW_CD" id="APPLY_RAW_CD" value="50"> 제50조<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="APPLY_RAW_CD" id="APPLY_RAW_CD" value="51"> 제51조&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="APPLY_TYPE_01_1" id="APPLY_TYPE_01_1" value="Y"/> 저작물<br/>
							저작권법 &nbsp;&nbsp;&nbsp;<input type="checkbox" name="APPLY_RAW_CD" id="APPLY_RAW_CD" value="52"> 제52조에 따라 위와 같이&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" name="APPLY_TYPE_02_1" id="APPLY_TYPE_02_1" value="Y"/> 실연
							<input type="checkbox" name="APPLY_TYPE_03_1" id="APPLY_TYPE_03_1" value="Y"/> 음반
							<input type="checkbox" name="APPLY_TYPE_04_1" id="APPLY_TYPE_04_1" value="Y"/> 방송
							이용의 승인을 신청합니다.<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="APPLY_RAW_CD" id="APPLY_RAW_CD" value="89"> 제89조&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="APPLY_TYPE_05_1" id="APPLY_TYPE_05_1" value="Y"/> 데이터베이스<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="APPLY_RAW_CD" id="APPLY_RAW_CD" value="97"> 제97조<br/>
						</td>
					</tr>
					<tr>
						<th>첨부서류</th>
						<td colspan="3" id="TdIdFile">
						<div style="height:50px;" onclick="fncFileAdd();return false;"><i class="fa fa-fw fa-plus"></i><input type="button" value="파일추가" style="height:30px;"></div>
						<div id="DivIdFile1" ><input type="file" id="file1" name="file1" size="50" style="float:left"><input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);"></div>
						</td>
					</tr>
				</tbody>
			</table>
				<div class="btn-group" style="float: right; margin-top: 5px;">
					<button type="submit" class="btn btn-primary" onclick="fncAddRow();return false;" style="margin-right: 10px;">추가</button>
					<button type="submit" class="btn btn-primary" onclick="fncDeleteRow();return false;">삭제</button>
				</div>
			
			<div style="margin: 100px 0 15px 5px;"><font size="3">● 이용 승인신청 명세서 정보</font></div>
				<table class="table table-bordered" id="table1">
					<thead>
						<th style="width: 5%; text-align: center;">순번</th>
						<th style="width: 35%; text-align: center;">제호(제목)</th>
						<th style="width: 15%; text-align: center;">종류</th>
						<th style="width: 15%; text-align: center;">형태 및 수량</th>
						<th style="width: 15%; text-align: center;">공표연월일</th>
						<th style="width: 15%; text-align: center;">권리자 성명</th>
					</thead>
					<tbody id="tbody1">
					</tbody>
				</table>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<td rowspan="2" colspan="1" style="width: 20%">
							<input type="checkbox" name="APPLY_TYPE_01_2" id="APPLY_TYPE_01_2" value="Y"> 저작물<br/>
							<input type="checkbox" name="APPLY_TYPE_02_2" id="APPLY_TYPE_02_2" value="Y"> 실연
							<input type="checkbox" name="APPLY_TYPE_03_2" id="APPLY_TYPE_03_2" value="Y"> 음반
							<input type="checkbox" name="APPLY_TYPE_04_2" id="APPLY_TYPE_04_2" value="Y"> 방송<br/>
							<input type="checkbox" name="APPLY_TYPE_05_2" id="APPLY_TYPE_05_2" value="Y"> 데이터베이스
							 <%-- <console:fn func="isChecked" value="Y" value1="${info.HTML_YSNO}"/> --%>
						</td>
						<th colspan="1" style="width: 20%">제호</th>
						<td colspan="3" style="width: 60%">
							<input class="form-control" type="text" name="APPLY_WORKS_TITL3" id="APPLY_WORKS_TITL3"/>
						</td>
					</tr>
					<tr>
						<th colspan="1" style="width: 20%">종류</th>
						<td colspan="1" style="width: 20%">
							<input class="form-control" type="text" name="APPLY_WORKS_KIND3"id="APPLY_WORKS_KIND3"/>
						</td>
						<th colspan="1" style="width: 20%">형태 및 수량</th>
						<td colspan="1" style="width: 20%">
							<input class="form-control" type="text" name="APPLY_WORKS_FORM3"id="APPLY_WORKS_FORM3"/>
						</td>
					</tr>
					
					<tr>
						<th rowspan="2" colspan="1" style="width: 20%">
							공표<br/>
							(실연/음반발매/방송 등 포함)
						</th>
						<th colspan="1" style="width: 20%">공표연월일(8자리)</th>
						<td colspan="1" style="width: 20%">
							<input class="form-control" type="text" name="PUBL_YMD"id="PUBL_YMD" onkeyup="fncPublYmd();"/>
						</td>
						<th colspan="1" style="width: 20%">공표국가</th>
						<td colspan="1" style="width: 20%">
							<input class="form-control" type="text" name="PUBL_NATN"id="PUBL_NATN"/>
						</td>
					</tr>
					<tr>
						<th colspan="1" style="width: 20%">공표방법</th>
						<td colspan="1" style="width: 20%">
							<input type="checkbox" name="PUBL_MEDI_CD" id="PUBL_MEDI_CD" value="1"> 출판
							<input type="checkbox" name="PUBL_MEDI_CD" id="PUBL_MEDI_CD" value="2"> 인터넷<br/>
							<input type="checkbox" name="PUBL_MEDI_CD" id="PUBL_MEDI_CD" value="3"> 복제·배포(판매)<br/>
							<input type="checkbox" name="PUBL_MEDI_CD" id="PUBL_MEDI_CD" value="4"> 공연
							<input type="checkbox" name="PUBL_MEDI_CD" id="PUBL_MEDI_CD" value="5"> 전시<br/>
							<input type="checkbox" name="PUBL_MEDI_CD" id="PUBL_MEDI_CD" value="6"> 방송<br/>
							<input type="checkbox" name="PUBL_MEDI_CD" id="PUBL_MEDI_CD" value="7"> 기타
							<%-- <console:fn func="isChecked" value="7" value1="${info.HTML_YSNO}"/> --%>
						</td>
						<th colspan="1" style="width: 20%">공표매체정보</th>
						<td colspan="1" style="width: 20%">
							<input class="form-control" type="text" name="PUBL_MEDI"id="PUBL_MEDI"/>
						</td>
					</tr>
					
					<tr>
						<th rowspan="2" colspan="1" style="width: 20%">
							권리자
						</th>
						<th colspan="1" style="width: 20%">성명<br/>(법인명)</th>
						<td colspan="1" style="width: 20%">
							<input class="form-control" type="text" name="COPT_HODR_NAME"id="COPT_HODR_NAME" onkeyup="fncCoptHodrName();"/>
						</td>
						<th colspan="1" style="width: 20%">전화번호</th>
						<td colspan="1" style="width: 20%">
							<input class="form-control" type="text" name="COPT_HODR_TELX_NUMB"id="COPT_HODR_TELX_NUMB"/>
						</td>
					</tr>
					<tr>
						<th colspan="1" style="width: 20%">주소</th>
						<td colspan="3" style="width: 60%">
						<input class="form-control" type="text" name="COPT_HODR_ADDR" id="COPT_HODR_ADDR"/>
						</td>
					</tr>
					
					<tr>
						<th rowspan="2" colspan="1" style="width: 20%">
							신청물의 내용
						</th>
						<td colspan="4" style="width: 20%">
							<textarea class="form-control" cols="6" id="WORKS_DESC" name="WORKS_DESC"></textarea>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-footer" style="text-align: right">
			<div class="btn-group">
				<button type="submit" class="btn btn-primary"
					onclick="fncWrite();return false;">등록</button>
			</div>
			<div class="btn-group">
				<button type="submit" class="btn btn-primary"
					onclick="fncCancel();return false;">취소</button>
			</div>
		</div>
	</div>
</form>