<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>

	//업로드 파일양식 다운로드
	function fncDown(){
		location.href = '<c:url value="/console/legal/fdcrAd13Download1.page"/>';
	}

	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd12List1.page"/>',datas, page);
		location.href = url;
	}
	
	// 파일업로드 후 폼 이동
	function file_change(file){
		var f = document.formUpload;
		var file1 = $('#file1').val();
		alert("파일이 들어옴!"+file1);
		/* if(f.file.value == ""){
			alert("파일을 업로드해주세요.");
			return ;
		} */
		if(confirm("파일을 업로드 하시겠습니까?"))
		{
			f.action = '<c:url value="/console/legal/fdcrAd13Upload1.page"/>?file1='
				+ file1;
			f.submit();
		}
		
	}
	
	//선택삭제
	function rowCheDel(){
	  var sum = 0;
	  var $obj = $("input[name='chk']");
	  var checkCount = $obj.size();
	  
	  for(var i=0; i < checkCount; i++ ){
       if( $obj.eq(i).is(":checked")==true ){
    	   
    	var var1 = $obj[i].value;
    	
    	var f = document.delForm;
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd12Delete1.page"/>')+'&WORKS_ID='+var1;
		location.href = url;
		
	    sum += 1;
			}
	  	}
	  	if ( sum < 1 ) {
	      alert("체크 개수가 1개 이하입니다.");
	      return;
	    }
	  alert("선택되어진 체크박스의 갯수는 " + sum + "개입니다." );
	 }
	
	function fncValChk(){
		alert(".xls,.xlsx 파일을 업로드 해주세요.");
		return false;
	}
	
	function fncDelete(){
		alert("삭제할 항목을 체크해 주세요.");
		return false;
	}
	
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<h4>&nbsp;&nbsp;● 법정허락 대상저작물 관리(일괄삭제)</h4>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"> <label>엑셀양식</label></span>
						<span class="input-group-addon" style="width:150px;"><a href="#" onclick="fncDown();return false;"><input type="button" value="다운로드"></a></span>
						<!-- action="/console/rcept/fdcrAd21List1.page" -->
						<span class="input-group-addon"> <label>엑셀파일 업로드</label></span>
						<span class="input-group-addon" style="width:150px;">
						<form name="formUpload" id="formUpload" method="post" enctype="multipart/form-data">
							<div id="DivIdFile1" style="display: inline-block;">
								<input type="file" id="file1" name="file1" value="" size="50" style="float:left" onchange="javascript:file_change(this.value);">
								<input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);">&nbsp;&nbsp;<font color="red">( * 50000건 이하만 가능 )</font>
							</div>
						</form>
						</span>
					</div>
				</div>
			<!-- /.row -->
			</div>
		<!-- /.box-body -->
		</div>
	</div>
</div>
<form name="delForm" id="delForm" method="post">
<div style="height: 520px;">
<div class="box" style="margin-top: 30px;">
	<h4>&nbsp;&nbsp;● 목록확인</h4>
		<div class="box-header with-border pull-left" style="display: inline-block;">
			<div class="btn-group">
				<button type="submit" class="btn btn-primary" onclick="fncValChk();return false;">유효성확인</button>
				<button type="submit" class="btn btn-primary" onclick="fncDelete();return false;" style="margin-left: 10px;">선택삭제</button>
			</div>
		</div>
		<div class="box-header with-border pull-right">
			<div class="btn-group">
				<button type="submit" class="btn btn-primary" onclick="fncUpdate();return false;">일괄삭제</button>
			</div>
		</div>
		<!-- /.box-header -->
				<div class="box-body">
					<table id="fdcrAd13List1" class="table table-bordered" style="margin-top: 15px;">
						<thead>
							<tr>
								<th class="text-center" style="width: 100px;">선택</th>
								<th class="text-center" style="width: 100px;">순번</th>
								<th class="text-center" style="width: 100px;">제호</th>
								<th class="text-center" style="width: 100px;">저작권자</th>
								<th class="text-center" style="width: 100px;">오류 건수</th>
								<th class="text-center" style="width: 100px;">공표일자</th>
								<th class="text-center" style="width: 100px;">구분</th>
								<th class="text-center" style="width: 100px;">공고일자</th> 
								<th class="text-center" style="width: 100px;">저작물구분</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
	</div>
</form>
<%-- 
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
 --%>