<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<style>
.table > tbody > tr > th {
	text-align: center;
}
</style>
<script>
	//등록
	function fncSave(BORD_CD,BORD_SEQN) {
		rules = {
			RECEIPT_NO : "required",
			DIVS_CD_NM : "required",
			TITE : "required"
		};

		messages = {
			RECEIPT_NO : "접수번호는 필수항목입니다",
			DIVS_CD_NM : "구분은 필수항목입니다",
			TITE : "제목은 필수항목입니다"
		};

		if (!fncValidate(rules, messages)) {
			return false;
		}
	  if (!confirm("수정하시겠습니까?")) {
	   
	    return false;
	   
	   
	    
	  }else{
	    var f = document.updateForm;
      f.action = '<c:url value="/console/legal/fdcrAd08Update1.page"/>';
      f.submit();
	  }
	  
		/* var RECEIPT_NO = $('#RECEIPT_NO').val();
		var DIVS_CD = $('#DIVS_CD').val();
		var WORKS_ID = $('#WORKS_ID').val();
		var TITE = $('#TITE').val();
		var ANUC_ITEM_1 = $('#ANUC_ITEM_1').val();
		var ANUC_ITEM_2 = $('#ANUC_ITEM_2').val();
		var ANUC_ITEM_3 = $('#ANUC_ITEM_3').val();
		var ANUC_ITEM_4 = $('#ANUC_ITEM_4').val();
		var ANUC_ITEM_5 = $('#ANUC_ITEM_5').val();
		var ANUC_ITEM_6 = $('#ANUC_ITEM_6').val();
		var ANUC_ITEM_7 = $('#ANUC_ITEM_7').val();
		var ANUC_ITEM_8 = $('#ANUC_ITEM_8').val();
		var ANUC_ITEM_9 = $('#ANUC_ITEM_9').val();
		var ANUC_ITEM_10 = $('#ANUC_ITEM_10').val();
		var ANUC_ITEM_11 = $('#ANUC_ITEM_11').val();
		var file1 = $('#file1').val();

		var f = document.updateForm;
		if (confirm("수정하시겠습니까?")) {
		f.action = '<c:url value="/console/legal/fdcrAd08Update1.page"/>?BORD_CD='
			+ BORD_CD+'&BORD_SEQN='+BORD_SEQN+'&RECEIPT_NO='+RECEIPT_NO+'&DIVS_CD='
			+DIVS_CD+'&WORKS_ID='+WORKS_ID+'&TITE='+TITE+'&ANUC_ITEM_1='
			+ANUC_ITEM_1+'&ANUC_ITEM_2='+ANUC_ITEM_2+'&ANUC_ITEM_3='+ANUC_ITEM_3+'&ANUC_ITEM_4='
			+ANUC_ITEM_4+'&ANUC_ITEM_5='+ANUC_ITEM_5+'&ANUC_ITEM_6='+ANUC_ITEM_6+'&ANUC_ITEM_7='
			+ANUC_ITEM_7+'&ANUC_ITEM_8='+ANUC_ITEM_8+'&ANUC_ITEM_9='+ANUC_ITEM_9+'&ANUC_ITEM_10='
			+ANUC_ITEM_10+'&ANUC_ITEM_11='+ANUC_ITEM_11+'&file1='+file1;
		f.submit();
		} */
	}

	//취소
	function fncCancel() {
		var datas = [];
		datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam(
				'<c:url value="/console/ntcn/fdcrAd75List1.page"/>', datas);
		location.href = url;
	}
	
	function fncGoList(){
		location.href = '<c:url value="/console/legal/fdcrAd08List1.page"/>';
	}
</script>
<form name="updateForm" id="updateForm" method="post" enctype="multipart/form-data">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${param.MENU_SEQN}"/>" /> 
	<input type="hidden" id="BORD_CD" name="BORD_CD" value="<c:out value="${param.BORD_CD}"/>" /> 
	<input type="hidden" id="BORD_SEQN" name="BORD_SEQN" value="<c:out value="${param.BORD_SEQN}"/>" /> 
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
	<div class="box" style="margin-top: 30px;">
		<!-- /.box-header -->
		<div class="box-body">
			<table class="table table-bordered">
				<tbody>
				<c:choose>
					<c:when test="${empty detailList}">
					</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${detailList}" varStatus="listStatus">
					<tr>
						<th style="width: 20%">접수번호</th>
						<td style="width: 30%"><input class="form-control" type="text" name="RECEIPT_NO"
							id="RECEIPT_NO" value="<c:out value="${info.RECEIPT_NO}"/>" /></td>
						<th style="width: 20%">구분</th>
						<td style="width: 30%">
						<%-- <input class="form-control" type="text" name="DIVS_CD_NM" id="DIVS_CD_NM" value="<c:out value="${info.DIVS_CD_NM}"/>" /> --%>
							<select class="form-control" id="DIVS_CD" name="DIVS_CD">
								<option value="1" <c:if test="${info.DIVS_CD eq '1'}">selected="selected"</c:if>>미분배</option>
								<option value="2" <c:if test="${info.DIVS_CD eq '2'}">selected="selected"</c:if>></option>
								<option value="3" <c:if test="${info.DIVS_CD eq '3'}">selected="selected"</c:if>></option>
								<option value="4" <c:if test="${info.DIVS_CD eq '4'}">selected="selected"</c:if>>거소불명 저작물</option>
							</select>
						</td>
					</tr>
					 
					<tr>
						<th style="width: 20%">제목</th>
						<td colspan="3"><input class="form-control" type="text" name="TITE" id="TITE" value="<c:out value="${info.TITE}"/>" /></td>
					</tr>
					<tr>
						<th style="width: 20%">공고일자</th>
						<td style="width: 30%"><c:out value="${info.OPEN_DTTM}"/></td>
						<th style="width: 20%">공고자</th>
						<td style="width: 30%"><c:out value="${info.RGST_NAME}"/></td>
					</tr>
					
					<tr>
					<th style="width: 20%">첨부파일</th>
					<td><font color="light-blue;">* 공고문 스캔파일 등록시<br/>jpg 파일형식으로 업로드 하시기 바랍니다.</font></td>
					<td colspan="2" id="TdIdFile">
						<c:choose>
						<c:when test="${empty fileList}">
						</c:when>
						<c:otherwise>
							<c:forEach var="attach" items="${fileList}"
								varStatus="listStatus">
							<p class="file">
								<input type="checkbox" name="orgFileDel" value="<c:out value="${attach.ATTC_SEQN}"/>"/> <console:fn func="getFileIcon" value="${attach.REAL_FILE_NAME}"/>
								<a href="#" onclick="fncDown('<c:out value="${attach.MENU_SEQN}"/>','<c:out value="${attach.BORD_SEQN}"/>','<c:out value="${attach.ATTC_SEQN}"/>');return false;"><c:out value="${attach.FILE_NAME}"/></a>
								<span>(파일크기: <console:fn func="strFileSize" value="${attach.FILE_SIZE}"/>)</span>
							</p>
							</c:forEach>
						</c:otherwise>
						</c:choose>
						<div style="height:50px;" onclick="fncFileAdd();return false;"><i class="fa fa-fw fa-plus"></i><input type="button" value="파일추가" style="height:30px;"></div>
						<div id="DivIdFile1" ><input type="file" id="file1" name="file1" value="" size="50" style="float:left"><input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);"></div>
					</td>
					</tr>
					<tr>
						<th style="width: 20%">내용</th>
						<td colspan="3">
						<b>문화체육관광부 공고 제 &nbsp;&nbsp;<input type="text" class="form-control" name="ANUC_ITEM_1" id="ANUC_ITEM_1" value="<c:out value="${info.ANUC_ITEM_1}"/>" style="width: 50px;height: 30px; display: inline;"/>&nbsp;&nbsp;호</b><br/>
						<input type="text" class="form-control" id="ANUC_ITEM_2" name="ANUC_ITEM_2" value="<c:out value="${info.ANUC_ITEM_2}"/>" style="width: 100%;height: 60px;"/><br/><br/>
						<b>저작물 이용의 법정허락 신청 내용 공고</b><br/><br/>
						<b>1. 신청인</b><input type="text" class="form-control" name="ANUC_ITEM_3" id="ANUC_ITEM_3" value="<c:out value="${info.ANUC_ITEM_3}"/>" style="width: 200px; margin-left: 100px;display: inline;"/><br/>
						<b>2. 대상저작물</b><br/>
						&nbsp;&nbsp;&nbsp;&nbsp;<b>가. 저작물 종류</b><input type="text" class="form-control" name="ANUC_ITEM_4" id="ANUC_ITEM_4" value="<c:out value="${info.ANUC_ITEM_4}"/>" style="width: 200px; margin-left: 166px; display: inline;"/><br/>
						&nbsp;&nbsp;&nbsp;&nbsp;<b>나. 저작물 제호(명칭)</b><input type="text" class="form-control" name="ANUC_ITEM_5" id="ANUC_ITEM_5" value="<c:out value="${info.ANUC_ITEM_5}"/>" style="width: 200px; margin-left: 129px; display: inline;"/><br/>
						&nbsp;&nbsp;&nbsp;&nbsp;<b>다. 공표시 표시된 저작자 성명</b><input type="text" class="form-control" name="ANUC_ITEM_6" id="ANUC_ITEM_6" value="<c:out value="${info.ANUC_ITEM_6}"/>" style="width: 200px; margin-left: 77px; display: inline;"/><br/>
						&nbsp;&nbsp;&nbsp;&nbsp;<b>라. 공표 매체</b><input type="text" class="form-control" name="ANUC_ITEM_7" id="ANUC_ITEM_7" value="<c:out value="${info.ANUC_ITEM_7}"/>" style="width: 200px; margin-left: 180px; display: inline;"/><br/>
						&nbsp;&nbsp;&nbsp;&nbsp;<b>마. 공표연월일</b><input type="text" class="form-control" name="ANUC_ITEM_8" id="ANUC_ITEM_8" value="<c:out value="${info.ANUC_ITEM_8}"/>" style="width: 200px; margin-left: 169px; display: inline;"/><br/><br/>
						<b>3. 신청목적</b><input type="text" class="form-control" name="ANUC_ITEM_9" id="ANUC_ITEM_9" value="<c:out value="${info.ANUC_ITEM_9}"/>" style="width: 80%; height: 100px; margin-left: 50px; display: inline;"/><br/><br/>
						<b>4. 법적근거</b><input type="text" class="form-control" name="ANUC_ITEM_10" id="ANUC_ITEM_10" value="<c:out value="${info.ANUC_ITEM_10}"/>" style="width: 300px; margin-left: 50px; display: inline;"/><br/><br/>
						<b>5. 신청경위</b><input type="text" class="form-control" name="ANUC_ITEM_11" id="ANUC_ITEM_11" value="<c:out value="${info.ANUC_ITEM_11}"/>" style="width: 80%; height: 100px; margin-left: 50px; display: inline;"/><br/><br/>
						<b>6. 이의신청</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;동 저작물의 이용승인신청에 대하여 이의를 제기하려는 저작재산권자는 승인이전에 다음 사항이 기재된 서류 등을 <br/>
									   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;한국저작권위원회 심의조정팀(02-2660-0102)으로 제출 하시기 바랍니다.<br/><br/>
									  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 가. 법정허락 신청 내용에 대한 이의 내용 및 그 이유<br/>
									  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 나. 성명(단체인 경우 단체명과 그 대표자)/주소/전화번호<br/>
									   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;다. 자신이 그 권리자로 표시된 저작권 등의 등록증 사본 또는 그에 상당하는 자료<br/>
									   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;라. 자신의 성명이나 또는 예명, 아호/약칭 등으로서 널리 알려진 것이 표시되어 있는 그 저작물의 사본 또는 그에 상당하는 자료  
						</td>
					</tr>
					</c:forEach>
					</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		</div>
		<div class="box-footer" style="text-align: right">
			<div class="btn-group">
			<c:choose>
					<c:when test="${empty detailList}">
					</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${detailList}" varStatus="listStatus">
					<button class="btn btn-primary" onclick="fncSave('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');">저장</button>
					</c:forEach>
				</c:otherwise>
			</c:choose>
			</div>
			<div class="btn-group"><button class="btn btn-primary" onclick="fncGoList();return false;">목록</button></div>
		</div>
	</div>
</form>