<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
//첨부파일 다운로드
function fncDown(BORD_CD,BORD_SEQN){
	location.href = '<c:url value="/console/legal/fdcrAd09Download1.page"/>?BORD_CD='+BORD_CD+'&BORD_SEQN='+BORD_SEQN;
}

//수정 폼으로 이동
function fncGoUpdateForm(BORD_CD,BORD_SEQN){
	/* 
	var datas = [];
	datas[0] = 'BORD_CD';
	datas[1] = 'BORD_SEQN';
	 */
	 location.href = '<c:url value="/console/legal/fdcrAd09UpdateForm1.page"/>?BORD_CD='+BORD_CD+'&BORD_SEQN='+BORD_SEQN;
} 

//게시글 삭제
function fncGoDelete(BORD_CD,BORD_SEQN){
	if(confirm("삭제하시겠습니까?")){
	 location.href = '<c:url value="/console/legal/fdcrAd09Delete1.page"/>?BORD_CD='+BORD_CD+'&BORD_SEQN='+BORD_SEQN;
	}
}

function fncGoList(){
	location.href = '<c:url value="/console/legal/fdcrAd09List1.page"/>';
}

</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
</form>
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
			<table class="table table-bordered text-center">
				<tbody>
				<c:choose>
				<c:when test="${empty ds_list}">
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<tr>
						<th style="width: 20%">접수번호</th>
						<td style="width: 30%"><c:out value="${info.RECEIPT_NO}"/></td>
						<th style="width: 20%">구분</th>
						<td style="width: 30%"><c:out value="${info.DIVS_CD_NM}"/></td>
					</tr>
					 
					<tr>
						<th>제목</th>
						<td colspan="3"><c:out value="${info.TITE}"/></td>
					</tr>
					
					<tr>
						<th style="width: 20%">공고일자</th>
						<td style="width: 30%"><c:out value="${info.OPEN_DTTM}"/></td>
						<th style="width: 20%">공고자</th>
						<td style="width: 30%"><c:out value="${info.RGST_NAME}"/></td>
					</tr>
					
					<tr>
						<th>첨부서류</th>
						<td colspan="3" id="TdIdFile">
						<c:choose>
						<c:when test="${empty fileList}">
							<span>등록된 첨부서류가 없습니다.</span>
						</c:when>
						<c:otherwise>
							<c:forEach var="attach" items="${fileList}"			varStatus="listStatus">
							<p class="file">
								<console:fn func="getFileIcon" value="${attach.REAL_FILE_NAME}"/>
								<a href="#" onclick="fncDown('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;"><c:out value="${attach.REAL_FILE_NAME}"/></a>
								<span>(파일크기: <console:fn func="strFileSize" value="${attach.FILE_SIZE}"/>)</span>
							</p>
							</c:forEach>
						</c:otherwise>
						</c:choose>
						</td>
					</tr>
					
					<tr>
						<th>내용</th>
						<td colspan="3">
						1. 저작물의 제호 및 공표연월일 : <c:out value="${info.ANUC_ITEM_1}"/><br/><br/>
						2. 저작자 또는 저작재간권자 성명 : <c:out value="${info.ANUC_ITEM_2}"/><br/><br/>
						3. 이용승인을 받은 자의 성명 : <c:out value="${info.ANUC_ITEM_3}"/><br/><br/>
						4. 저작물의 이용승인 조건(이용허락 기간 및 보상금)<br/>
						&nbsp;&nbsp;&nbsp;&nbsp; - 이용허락 기간 : <c:out value="${info.ANUC_ITEM_4}"/><br/>
						&nbsp;&nbsp;&nbsp;&nbsp; - 보상금 : <c:out value="${info.ANUC_ITEM_5}"/><br/><br/>
						5. 저작물의 이용방법 및 형태 : <c:out value="${info.ANUC_ITEM_6}"/><br/><br/>
						6. 승인 일자 : <c:out value="${info.ANUC_ITEM_7}"/>
						</td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		</div>
	<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoUpdateForm('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">수정</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoDelete('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">삭제</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoList();">목록</button></div>
	</div>
	</c:forEach>
</div>