<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncGoSave(BORD_CD,BORD_SEQN){
		var f = document.updateForm;
		f.action = '<c:url value="/console/legal/fdcrAd10SuplRegi1.page"/>';
		f.submit();
	}
	
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
<form name="updateForm" id="updateForm" method="post">
	<input type="hidden" id="BORD_CD" name="BORD_CD" value="<c:out value="${param.BORD_CD}"/>" /> 
	<input type="hidden" id="BORD_SEQN" name="BORD_SEQN" value="<c:out value="${param.BORD_SEQN}"/>" /> 
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶ 보상금 공탁공고 공고내용 보완</b></h4>
			<table class="table table-bordered" style="width: 100%">
				<tbody>
				<c:choose>
				<c:when test="${empty ds_list}">
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<tr>
						<th class="text-center" rowspan="9" style="width: 10%">내용</th>
					</tr>
					<tr>
						<td><b>1. 저작물의 제호 :</b>&nbsp;&nbsp;<input type="text" class="form-control" name="ANUC_ITEM_1" id="ANUC_ITEM_1" value="<c:out value="${info.ANUC_ITEM_1 }"/>" style="width: 300px; display: inline-block;"/></td>
					</tr>
					<tr>
						<td><b>2. 저작자 및 지적재산권자의 성명 :</b>&nbsp;&nbsp;<input type="text" class="form-control" name="ANUC_ITEM_2" id="ANUC_ITEM_2" value="<c:out value="${info.ANUC_ITEM_2 }"/>" style="width: 300px; display: inline-block;"/></td>
					</tr>
					<tr>
						<td><b>3. 저작물 이용의 내용 :</b>&nbsp;&nbsp;<textarea class="form-control" cols="50" rows="3" name="ANUC_ITEM_3" id="ANUC_ITEM_3" ><c:out value="${info.ANUC_ITEM_3 }"/></textarea></td>
					</tr>
					<tr>
						<td><b>4. 공탁금액 :</b>&nbsp;&nbsp;<input type="text" class="form-control" name="ANUC_ITEM_4" id="ANUC_ITEM_4" value="<c:out value="${info.ANUC_ITEM_4 }"/>" style="width: 300px; display: inline-block;"/></td>
					</tr>
					<tr>
						<td><b>5. 공탁소의 명칭 및 소재지 :</b>&nbsp;&nbsp;<input type="text" class="form-control" name="ANUC_ITEM_5" id="ANUC_ITEM_5" value="<c:out value="${info.ANUC_ITEM_5 }"/>" style="width: 300px; display: inline-block;"/></td>
					</tr>
					<tr>
						<td><b>6. 공탁근거 :</b>&nbsp;&nbsp;<textarea class="form-control" cols="20" rows="3" name="ANUC_ITEM_6" id="ANUC_ITEM_6"><c:out value="${info.ANUC_ITEM_6 }"/></textarea></td>
					</tr>
					<tr>
						<td><b>7.저작물 이용자의 주소/성명</b><br/>
						&nbsp;&nbsp;&nbsp;<b>- 주소 :</b>&nbsp;&nbsp;<input type="text" class="form-control" name="ANUC_ITEM_7" id="ANUC_ITEM_7" value="<c:out value="${info.ANUC_ITEM_7 }"/>" style="width: 300px; display: inline-block;"/><br/>
						&nbsp;&nbsp;&nbsp;<b>- 성명 :</b>&nbsp;&nbsp;<input type="text" class="form-control" name="ANUC_ITEM_8" id="ANUC_ITEM_8" value="<c:out value="${info.ANUC_ITEM_8 }"/>" style="width: 300px; display: inline-block;"/></td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table><br/></br>
			<span style="display: inline-block;"><b>(*) 항목은 담당자에 의해 보완된 항목입니다.</b></span>
			<div class="btn-group" style="float: right;">
				<button type="submit" class="btn btn-primary" style="float: right;"
					onclick="closePop();">닫기</button>
			</div>
			<div class="btn-group" style="display: inline-block; float: right; margin-right: 15px;">
				<button type="submit" class="btn btn-primary" style="float: right;"
					onclick="fncGoSave('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');">보완완료</button>
			</div>
		</div>
</form>
