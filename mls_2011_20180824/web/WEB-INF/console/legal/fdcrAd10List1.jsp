<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		//datas[0] = 'MENU_SEQN';
	
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd10List1.page"/>',datas, page);
		location.href = url;
	}
	
	//검색
	function fncGoSearch() {
		var SCH_RECEIPT_NO = $('#SCH_RECEIPT_NO').val();
		var SCH_TITL = $('#SCH_TITL').val();
		
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd10List1.page"/>')+'&SCH_RECEIPT_NO='+SCH_RECEIPT_NO+'&SCH_TITL='+SCH_TITL;
		location.href = url;
	}
	
	 /* 
	function fncGoSearch() {
		//var tabIndex = $('#TAB_INDEX').val();
		var formData = $("form[name=searchForm]").serialize().replace(/%/g, '%25');
		fncLoad('', '<c:url value="/console/legal/fdcrAd10List1.page"/>', formData, function(data) {});
	}
	  */
	
	//상세 이동
	function fncGoView(BORD_CD,BORD_SEQN){
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd10View1.page"/>')+'&BORD_CD='+BORD_CD+'&BORD_SEQN='+BORD_SEQN;
		location.href = url;
	}
	
	//등록폼 이동
	function fncGoWriteForm(){
		//var datas = [];
		//datas[0] = 'MENU_SEQN';
		//var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd08WriteForm1.page"/>',datas);
		//location.href = url;
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd08WriteForm1.page"/>');
		location.href = url;
	} 
	
	//보상금 공탁 공고 목록(tab_index:0)
	function fncGoView0(TAB_INDEX){
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd10List1.page"/>')+'&TAB_INDEX='+TAB_INDEX;
		location.href = url;
	}
	
	//미공고 목록(tab_index:1)
	function fncGoView1(TAB_INDEX){
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd10List2.page"/>')+'&TAB_INDEX='+TAB_INDEX;
		location.href = url;
	}
	
	//반려 목록(tab_index:2)
	function fncGoView2(TAB_INDEX){
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd10List3.page"/>')+'&TAB_INDEX='+TAB_INDEX;
		location.href = url;
	}
	
	$(function(){
		for(var i = 0 ; i < 10 ; i ++){
			//console.log($('#anucItem'+i).html())
			if($('#anucItem'+i).html()!=undefined){
				$('#anucItem'+i).html(wonAttach($('#anucItem'+i).html()));
			}
		}
	})	

	function wonAttach(text){
		console.log(text)
			if(text.substr(text.indexOf(-1))!="원"){
				text+="원";
			}
			return text;
	}	
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
					<input type="hidden" id="TAB_INDEX" name="TAB_INDEX" value="<c:out value="${commandMap.TAB_INDEX}"/>" />
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">접수번호</th>
									<td colspan="3">
										<input type="text" id="SCH_RECEIPT_NO" name="SCH_RECEIPT_NO">
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">제목</th>
									<td colspan="3">
										<input type="text" id="SCH_TITL" name="SCH_TITL"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>
<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
	  <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="true" onclick="fncGoView0('0');">보상금공탁 공고 목록(${ds_count1.COUNT})</a></li>
	  <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false" onclick="fncGoView1('1');">미공고 목록(${ds_count0.COUNT})</a></li>
	  <li class=""><a href="#tab3" data-toggle="tab" aria-expanded="false" onclick="fncGoView2('2');">반려 목록(${ds_count2.COUNT})</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div class="box" style="margin-top: 30px;">
			
				<div class="box-header with-border pull-right">
					<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
				</div>
				
		<!-- /.box-header -->
				<div class="box-body">
					<table id="fdcrAd10List1" class="table table-bordered">
						<thead>
							<tr>
								<th class="text-center" style="width: 7%">순번</th>
								<th class="text-center">제호(제목)</th>
								<th class="text-center" style="width: 7%">공탁금액</th>
								<th class="text-center" style="width: 11%">공고일자</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty ds_list}">
									<tr>
										<td colspan="3" class="text-center">게시물이 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
										<tr>
											<td class="text-center"><c:out value="${info.ROW_NUM }" /></td>
											<td><a href="#" onclick="fncGoView('<c:out value="${info.BORD_CD }"/>','<c:out value="${info.BORD_SEQN }"/>');"><c:out value="${info.TITE }" escapeXml="false" /></a></td>
											<td id="anucItem${listStatus.index}" style="text-align: right;"><c:out value="${info.ANUC_ITEM_4 }" /></td>
											<td class="text-center"><c:out value="${info.OPEN_DTTM }" /></td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
				<div class="box-footer clearfix text-center">
				  <ul class="pagination pagination-sm no-margin">
				    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
				  </ul>
				</div>
			</div>
		</div>
	</div>
</div>
