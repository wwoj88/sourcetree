<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>

	//업로드 파일양식 다운로드
	function fncDown(){
		location.href = '<c:url value="/console/legal/fdcrAd13Download1.page"/>';
	}
	
	//유효성 확인
	function aaa()
	{
		$('#chkValYN').val('N'); // 유효성체크확인
		var chkValYN2 = $('#chkValYN').val();
		var isRowError = false;
		var errorRowStr = '';
		var curRowErrCount = 0;
		var allRowErrCount = 0;
		
		var f = document.delForm;
		var table = document.getElementById("fdcrAd13List1");
 		var rowsCount = table.rows.length-1;
		
		for(var a = 1; a <= rowsCount; a++){
			var target_1 = document.getElementById("td_"+a+"_0").childNodes[0];
			var target_2 = document.getElementById("td_"+a+"_1").childNodes[0];
			var target_3 = document.getElementById("td_"+a+"_2").childNodes[0];
			var target_4 = document.getElementById("td_"+a+"_3").childNodes[0];
			var target_5 = document.getElementById("td_"+a+"_4").childNodes[0];
			var target_6 = document.getElementById("td_"+a+"_5").childNodes[0];
				//Validation Check
				if(target_1=='' || target_1 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_0");
				     t.style.backgroundColor = "#fc0"; 
				     isRowError = true;
					 curRowErrCount++;
				}
				 
				if(target_2=='' || target_2 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_1");
				     t.style.backgroundColor = "#fc0"; 
				     isRowError = true;
					 curRowErrCount++;
				}
				
				if(target_3=='' || target_3 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_2");
				     t.style.backgroundColor = "#fc0"; 
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_3.length > 150){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_2");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
				if(target_4=='' || target_4 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_3");
				     t.style.backgroundColor = "#fc0";
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_4.length == 0){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_3");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
				if(target_5=='' || target_5 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_4");
				     t.style.backgroundColor = "#fc0"; 
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_5.length > 60){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_4");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
				
				if(target_6=='' || target_6 == null){
					//오류 시 컬럼 td 색상 변경
					var t = document.getElementById("td_"+a+"_5");
				     t.style.backgroundColor = "#fc0";
				     isRowError = true;
					 curRowErrCount++;
				}else{
					if(target_6.length > 60){
						//오류 시 컬럼 td 색상 변경
						var t = document.getElementById("td_"+a+"_5");
					     t.style.backgroundColor = "#fc0";
					     isRowError = true;
						 curRowErrCount++;
					}
				}
			}
		alert("유효성 확인이 완료되었습니다.\n전체건수 : " + rowsCount + "건(오류대상항목: " + curRowErrCount + "건)");
		chkValYN2 = 'Y';
		$('#chkValYN').val('Y');
	} 
	
	//파일 첨부 시
	function file_change(file){
		var f = document.formUpload;
		var file1 = $('#file1').val();
		alert("파일이 들어옴!"+file1);
		/* if(f.file.value == ""){
			alert("파일을 업로드해주세요.");
			return ;
		} */
		if(confirm("파일을 업로드 하시겠습니까?"))
		{
			f.action = '<c:url value="/console/legal/fdcrAd13Upload1.page"/>?file1='
				+ file1;
			f.submit();
		}
		
	}
	
	//선택삭제
	function rowCheDel(){
      var sum = 0;
	  var $obj = $("input[name='chk']");
	  var checkCount = $obj.size();
		  for(var i=0; i < checkCount; i++ ){
	       	if( $obj.eq(i).is(":checked")==true ){
		    sum += 1;
			}
		  }
	  	if ( sum < 1 ) {
	      alert("체크 개수가 1개 이하입니다.");
	      return;
	    }
	  for (var i=0; i<checkCount; i++){
	   if($obj.eq(i).is(":checked")){
	   $obj.eq(i).parent().parent().remove();
	   }
	  }
	 }
	
	//일괄삭제
	function fncDelete() {

		var f = document.delForm;
		var table = document.getElementById("fdcrAd13List1");
 		var rowsCount = table.rows.length-1;
 		
 		var chkValYN2 = $('#chkValYN').val();
 		alert("chkValYN2:"+chkValYN2);
 		
 		if(chkValYN2 == 'N' || chkValYN2 == null || chkValYN2 == ''){
 			alert("유효성 검사를 해주세요.")
 		}
 		else{
	 		for(var a = 1; a <= rowsCount; a++){
	 			var target_1 = document.getElementById("td_"+a+"_0").childNodes[0];
				var target_2 = document.getElementById("td_"+a+"_1").childNodes[0];
				var target_3 = document.getElementById("td_"+a+"_2").childNodes[0];
				var target_4 = document.getElementById("td_"+a+"_3").childNodes[0];
				var target_5 = document.getElementById("td_"+a+"_4").childNodes[0];
				var target_6 = document.getElementById("td_"+a+"_5").childNodes[0];
				
				var var1 = target_1.nodeValue;
				var var2 = target_2.nodeValue;
				var var3 = target_3.nodeValue;
				var var4 = target_4.nodeValue;
				var var5 = target_5.nodeValue;
				var var6 = target_6.nodeValue;
				
				f.action = '<c:url value="/console/legal/fdcrAd13Delete1.page"/>?WORKS_TITLE='
					+ var1+'&COPT_HODR='+var2+'&MEDI_OPEN_DATE='+var3+'&WORKS_DIVS_CD_NM='
					+var4+'&OPEN_DTTM='+var5+'&WORKS_DIVS_CD='+var6;
				f.submit();
	 		}
 		}
	}
	
1
</script>
<style>
.fixed-table-body {
  overflow-x: auto;
}
</style>
<div class="box box-default">
	<h4>&nbsp;&nbsp;● 법정허락 대상저작물 관리(일괄삭제)</h4>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"> <label>엑셀양식</label></span>
						<span class="input-group-addon" style="width:150px;"><a href="#" onclick="fncDown();return false;"><input type="button" value="다운로드"></a></span>
						<!-- action="/console/rcept/fdcrAd21List1.page" -->
						<span class="input-group-addon"> <label>엑셀파일 업로드</label></span>
						<span class="input-group-addon" style="width:150px;">
						<form name="formUpload" id="formUpload" method="post" enctype="multipart/form-data">
							<div id="DivIdFile1" style="display: inline-block;">
								<input type="file" id="file1" name="file1" value="" size="50" style="float:left" onchange="javascript:file_change(this.value);">
								<input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);">&nbsp;&nbsp;<font color="red">( * 50000건 이하만 가능 )</font>
							</div>
						</form>
						</span>
					</div>
				</div>
			<!-- /.row -->
			</div>
		<!-- /.box-body -->
		</div>
	</div>
</div>

<form name="delForm" id="delForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
	<input type="hidden" id="chkValYN" name="chkValYN" value="" />
<div style="height: 520px;">
<div class="box" style="margin-top: 30px;">
	<h4>&nbsp;&nbsp;● 목록확인</h4>
		<c:forEach var="info" items="${uploadList}" varStatus="listStatus">
		<c:set var="tot_cnt" value="${listStatus.count}"/>
		</c:forEach>
		<div class="box-header with-border pull-left" style="display: inline-block;">
			<div class="btn-group">
				<button type="submit" class="btn btn-primary" onclick="aaa();return false;">유효성확인</button>
				<button type="submit" class="btn btn-primary" onclick="rowCheDel();return false;" style="margin-left: 10px;">선택삭제</button>
			</div>
		</div>
		<div class="box-header with-border pull-right">
			<div class="btn-group">
				<button type="submit" class="btn btn-primary" onclick="fncDelete(${tot_cnt});return false;">일괄삭제</button>
			</div>
		</div>
	
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd13List1" class="table table-bordered" style="margin-top: 15px;">
			<thead>
				<tr>
					<th class="text-center" style="width: 50px;">선택</th>
					<th class="text-center" style="width: 50px;">순번</th>
					<th class="text-center" style="width: 250px;">제호</th>
					<th class="text-center" style="width: 100px;">저작권자</th>
					<th class="text-center" style="width: 50px;">오류 건수</th>
					<th class="text-center" style="width: 100px;">공표일자</th>
					<th class="text-center" style="width: 100px;">구분</th>
					<th class="text-center" style="width: 100px;">공고일자</th> 
					<th class="text-center" style="width: 100px;">저작물구분</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="info" items="${uploadList}" varStatus="listStatus">
				<tr id="tr_${listStatus.count}">
					<td class="text-center" style="width: 50px;"><input type="checkbox" name="chk" id="chk" value="1"></td>
					<td class="text-center" style="width: 50px;">${listStatus.count}</td>
					<c:forEach var="i" begin="0" end="6" step="1">
						<c:if test="${i == '2' }">
							<td class="text-center" style="width: 100px;"></td>
						</c:if>
						<c:if test="${i == '0' || i == '1' }">
						<c:set var="map1" value="0_${listStatus.index+3}_${i}"></c:set>
							<td class="text-center" id="td_${listStatus.count}_${i}" style="width: 100px;">${info[map1]}</td>
						</c:if>
						<c:if test="${i != '0' && i != '1' && i != '2' }">
						<c:set var="map1" value="0_${listStatus.index+3}_${i-1}"></c:set>
							<td class="text-center" id="td_${listStatus.count}_${i-1}" style="width: 100px;">${info[map1]}</td>
						</c:if>
					</c:forEach>
				
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
</div>
</div>
</form>