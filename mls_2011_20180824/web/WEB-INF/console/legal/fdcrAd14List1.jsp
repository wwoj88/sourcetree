<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//검색
	function fncGoSearch() {
		var SCH_RECEIPT_NO = $('#SCH_RECEIPT_NO').val();
		//var SCH_WORKS_DIVS_CD = $('#SCH_WORKS_DIVS_CD').val();
		//var SCH_TITL = $('#SCH_TITL').val();
		
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd14List1.page"/>')+'&SCH_RECEIPT_NO='+SCH_RECEIPT_NO;
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(BORD_CD,BORD_SEQN){
		//var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd09View1.page"/>')+'&BORD_CD='+BORD_CD+'&BORD_SEQN='+BORD_SEQN;
		location.href = url;
	}
	
	//등록폼 이동
	function fncGoWriteForm(){
		//var datas = [];
		//datas[0] = 'MENU_SEQN';
		//var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd09WriteForm1.page"/>',datas);
		//location.href = url;
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd09WriteForm1.page"/>');
		location.href = url;
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">신청번호</th>
									<td colspan="3">
										<input type="text" id="SCH_RECEIPT_NO" name="SCH_RECEIPT_NO">
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">결제일자</th>
									<td colspan="3">
										<input type="text" id="SCH_RECEIPT_NO" name="SCH_RECEIPT_NO"> ~ <input type="text" id="SCH_RECEIPT_NO" name="SCH_RECEIPT_NO">
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>
<span><font color="blue;">&nbsp;&nbsp;* 이의제기가 신청된 정보는 저작권찾기 사이트의 관리게시판에서 제외됩니다.</font></span>

<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd14List1" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 25%">신청번호</th>
					<th class="text-center" style="width: 25%">결제금액</th>
					<th class="text-center" style="width: 25%">결제자</th>
					<th class="text-center" style="width: 25%">결제일자</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="4" class="text-center">검색결과가 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.LGD_OID}" /></td>
								<td class="text-center"><c:out value="${info.LGD_AMOUNT}" /></td>
								<td class="text-center"><c:out value="${info.LGD_BUYER}" /></td>
								<td class="text-center"><c:out value="${info.APPLY_WRITE_YMD}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
</div>