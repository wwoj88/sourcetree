<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06List1.page"/>',datas, page);
		location.href = url;
	}
	
	//검색
	function fncGoSearch() {
		var GUBUN1 = $('#GUBUN1').val();
		var SCH_NO = $('#SCH_NO').val();
		var SCH_APPLY_TYPE = $('#SCH_APPLY_TYPE').val();
		var SCH_APPLY_WORKS_TITL = $('#SCH_APPLY_WORKS_TITL').val();
		var SCH_STAT_CD = $('#SCH_STAT_CD').val();
		
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06List1.page"/>')+'&GUBUN1='+GUBUN1+'&SCH_NO='+SCH_NO+'&SCH_APPLY_TYPE='+SCH_APPLY_TYPE+'&SCH_APPLY_WORKS_TITL='+SCH_APPLY_WORKS_TITL+'&SCH_STAT_CD='+SCH_STAT_CD;
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(APPLY_WRITE_YMD,APPLY_WRITE_SEQ){
		//var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06View1.page"/>')+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ;
		location.href = url;
	}
	
	//등록폼 이동
	function fncGoWriteForm(){
		var datas = [];
		datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd06WriteForm1.page"/>',datas);
		location.href = url;
	} 
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<td>
										<select style="width:100px" id="GUBUN1" name="GUBUN1">
											<option selected="selected" value="1" >신청번호</option>
											<option value="2">접수번호</option>
										</select>
									</td>
									<td colspan="3">
										<input type="text" id="SCH_NO" name="SCH_NO">
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">신청서구분</th>
									<td colspan="3">
										<select id="SCH_APPLY_TYPE" name="SCH_APPLY_TYPE">
											<option selected="selected" value="">전체</option>
											<option value="1">저작물</option>
											<option value="2">실연</option>
											<option value="3">음반</option>
											<option value="4">방송</option>
											<option value="5">데이터베이스</option>
										</select>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">제호(제목)</th>
									<td colspan="3">
										<input type="text" id="SCH_APPLY_WORKS_TITL" name="SCH_APPLY_WORKS_TITL">
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">진행상태</th>
									<td colspan="3">
										<select id="SCH_STAT_CD" name="SCH_STAT_CD">
											<option selected="selected" value="">전체</option>
											<option value="1">임시저장</option>
											<option value="2">신청완료</option>
											<option value="3">보완요청</option>
											<option value="4">보완임시저장</option>
											<option value="5">보완완료</option>
											<option value="6">반려</option>
											<option value="7">결제요청</option>
											<option value="8">접수완료</option>
											<option value="9">심의중</option>
											<option value="10">심의완료</option>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>

<div class="box" style="margin-top: 30px;">
	<!-- 
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
	</div>
	 -->
	<!-- /.box-header -->
	<div class="box-body">
		<span><b>&nbsp;&nbsp;총&nbsp;&nbsp;${ds_count}건</b></span>
		<table id="fdcrAd06List1" class="table table-bordered" style="margin-top: 5px;">
			<thead>
				<tr>
					<th class="text-center" style="width: 7%">순번</th>
					<th class="text-center" style="width: 12%">신청번호</th>
					<th class="text-center" style="width: 12%">작성번호</th>
					<th class="text-center" style="width: 11%">신청서구분</th>
					<th class="text-center">제호(제목)</th>
					<th class="text-center" style="width: 11%">진행상태</th>
					<th class="text-center" style="width: 11%">신청자</th>
					<th class="text-center" style="width: 11%">명세서건수</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty list}">
						<tr>
							<td colspan="8" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.RNO}" /></td>
								<td class="text-center"><a href="#" onclick="fncGoView('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>');"><c:out value="${info.APPLY_NO}" /></a></td>
								<td class="text-center"><c:out value="${info.RECEIPT_NO}" /></td>
								<td class="text-center">
								<c:if test="${info.APPLY_TYPE == '1'}">저작물</c:if>
								<c:if test="${info.APPLY_TYPE == '2'}">실연</c:if>
								<c:if test="${info.APPLY_TYPE == '3'}">음반</c:if>
								<c:if test="${info.APPLY_TYPE == '4'}">방송</c:if>
								<c:if test="${info.APPLY_TYPE == '5'}">데이터베이스</c:if>
								</td>
								<td class="text-center"><c:out value="${info.APPLY_WORKS_TITL}"  escapeXml="false"/></td>
								<td class="text-center">
								<c:if test="${info.STAT_CD == '1'}">임시저장</c:if>
								<c:if test="${info.STAT_CD == '2'}"><font color="red">신청완료</font></c:if>
								<c:if test="${info.STAT_CD == '3'}">보완요청</c:if>
								<c:if test="${info.STAT_CD == '4'}">보완임시저장</c:if>
								<c:if test="${info.STAT_CD == '5'}">보완완료</c:if>
								<c:if test="${info.STAT_CD == '6'}">반려</c:if>
								<c:if test="${info.STAT_CD == '7'}">결제요청</c:if>
								<c:if test="${info.STAT_CD == '8'}">접수완료</c:if>
								<c:if test="${info.STAT_CD == '9'}">심의중</c:if>
								<c:if test="${info.STAT_CD == '10'}">심의완료</c:if>
								</td>
								<td class="text-center"><c:out value="${info.APPLR_NAME}" /></td>
								<td class="text-center"><c:out value="${info.WORKS_CNT}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
	
</div>