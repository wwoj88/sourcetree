<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶ 회원정보 상세정보</b></h4>
			<table class="table table-bordered">
				<tbody>
				<c:choose>
				<c:when test="${empty ds_list}">
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<tr>
						<th class="text-center" style="width: 20%">회원구분</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.USER_DIVS }"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">신탁회원확인</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.TRST_ORGN_NAME }"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">이름/사업자명</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.USER_NAME }"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">주민(법인)등록번호</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.RESD_CORP_NUMB }"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">아이디</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.USER_IDNT }"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">E-Mail</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.MAIL }"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">우편번호</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.ZIPX_CODE }"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">주소</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.ADDR }"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">상세주소</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.DETL_ADDR }"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">자택전화번호</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.TELX_NUMB }"/></td>
					</tr>
					<tr>
						<th class="text-center" style="width: 20%">핸드폰번호</th>
						<td class="text-center" style="width: 80%"><c:out value="${info.MOBL_PHON }"/></td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
			<div class="btn-group" style="float: right; margin-top: 20px;">
				<button type="submit" class="btn btn-primary" style="float: right;"
					onclick="closePop();">닫기</button>
			</div>
		</div>
