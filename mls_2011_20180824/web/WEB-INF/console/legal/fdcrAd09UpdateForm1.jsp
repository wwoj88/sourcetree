<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<style>
.table>tbody>tr>th {
	text-align: center;
}
</style>
<script>
  //등록
  function fncSave(BORD_CD, BORD_SEQN) {

    rules = {
    RECEIPT_NO : "required",
    DIVS_CD_NM : "required",
    TITE : "required"
    };

    messages = {
    RECEIPT_NO : "접수번호는 필수항목입니다",
    DIVS_CD_NM : "구분은 필수항목입니다",
    TITE : "제목은 필수항목입니다"
    };

    if (!fncValidate(rules, messages)) {
      return false;
    }

    var f = document.updateForm;
    f.action = '<c:url value="/console/legal/fdcrAd09Update1.page"/>';
    f.submit();
  }

  function fncGoList() {
    location.href = '<c:url value="/console/legal/fdcrAd09List1.page"/>';
  }
</script>
<form name="updateForm" id="updateForm" method="post" enctype="multipart/form-data">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${param.MENU_SEQN}"/>" /> <input type="hidden" id="BORD_CD" name="BORD_CD" value="<c:out value="${param.BORD_CD}"/>" /> <input type="hidden" id="BORD_SEQN" name="BORD_SEQN" value="<c:out value="${param.BORD_SEQN}"/>" /> <input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
	<div class="box" style="margin-top: 30px;">
		<!-- /.box-header -->
		<div class="box-body">
			<table class="table table-bordered">
				<tbody>
					<c:choose>
						<c:when test="${empty ds_list}">
						</c:when>
						<c:otherwise>
							<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
								<tr>
									<th style="width: 20%">접수번호</th>
									<td style="width: 30%">
										<input class="form-control" type="text" name="RECEIPT_NO" id="RECEIPT_NO" value="<c:out value="${info.RECEIPT_NO}"/>" />
									</td>
									<th style="width: 20%">구분</th>
									<td style="width: 30%">
										<%-- <input class="form-control" type="text" name="DIVS_CD_NM" id="DIVS_CD_NM" value="<c:out value="${info.DIVS_CD_NM}"/>" /> --%>
										<select class="form-control" id="DIVS_CD" name="DIVS_CD">
											<option value="1" <c:if test="${info.DIVS_CD eq '1'}">selected="selected"</c:if>>미분배</option>
											<option value="2" <c:if test="${info.DIVS_CD eq '2'}">selected="selected"</c:if>></option>
											<option value="3" <c:if test="${info.DIVS_CD eq '3'}">selected="selected"</c:if>></option>
											<option value="4" <c:if test="${info.DIVS_CD eq '4'}">selected="selected"</c:if>>거소불명저작물</option>
											<option value="5" <c:if test="${info.DIVS_CD eq '5'}">selected="selected"</c:if>>개인</option>
										</select>
									</td>
								</tr>

								<tr>
									<th style="width: 20%">제목</th>
									<td colspan="3">
										<input class="form-control" type="text" name="TITE" id="TITE" value="<c:out value="${info.TITE}"/>" />
									</td>
								</tr>
								<tr>
									<th style="width: 20%">공고일자</th>
									<td style="width: 30%">
										<c:out value="${info.OPEN_DTTM}" />
									</td>
									<th style="width: 20%">공고자</th>
									<td style="width: 30%">
										<c:out value="${info.RGST_NAME}" />
									</td>
								</tr>

								<tr>
									<th style="width: 20%">첨부파일</th>
									<td>
										<font color="light-blue;">* 공고문 스캔파일 등록시<br />jpg 파일형식으로 업로드 하시기 바랍니다.
										</font>
									</td>
									<td colspan="2" id="TdIdFile">
										<c:choose>
											<c:when test="${empty fileList}">
											</c:when>
											<c:otherwise>
												<c:forEach var="attach" items="${fileList}" varStatus="listStatus">
													<p class="file">
														<input type="checkbox" name="orgFileDel" value="<c:out value="${attach.ATTC_SEQN}"/>" />
														<console:fn func="getFileIcon" value="${attach.REAL_FILE_NAME}" />
														<a href="#" onclick="fncDown('<c:out value="${attach.MENU_SEQN}"/>','<c:out value="${attach.BORD_SEQN}"/>','<c:out value="${attach.ATTC_SEQN}"/>');return false;"><c:out value="${attach.FILE_NAME}" /></a> <span>(파일크기: <console:fn func="strFileSize" value="${attach.FILE_SIZE}" />)
														</span>
													</p>
												</c:forEach>
											</c:otherwise>
										</c:choose>
										<div style="height: 50px;" onclick="fncFileAdd();return false;">
											<i class="fa fa-fw fa-plus"></i><input type="button" value="파일추가" style="height: 30px;">
										</div>
										<div id="DivIdFile1">
											<input type="file" id="file1" name="file1" value="" size="50" style="float: left"><input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);">
										</div>
									</td>
								</tr>
								<tr>
									<th style="width: 20%">내용</th>
									<td colspan="3">
										<b>1. 저작물의 제호 및 공표연월일</b><br /> <input type="text" class="form-control" name="ANUC_ITEM_1" id="ANUC_ITEM_1" value="<c:out value="${info.ANUC_ITEM_1}"/>" style="width: 100%; display: inline;" /><br /> <br /> <b>2. 저작자 또는 저작재간권자 성명</b><br /> <input type="text" class="form-control" name="ANUC_ITEM_2" id="ANUC_ITEM_2" value="<c:out value="${info.ANUC_ITEM_2}"/>" style="width: 100%; display: inline;" /><br /> <br /> <b>3. 이용승인을 받은 자의 성명</b> <input type="text" class="form-control" name="ANUC_ITEM_3" id="ANUC_ITEM_3" value="<c:out value="${info.ANUC_ITEM_3}"/>" style="width: 100%; display: inline;" /><br /> <br /> <b>4. 저작물의 이용승인 조건(이용허락 기간 및 보상금)</b><br /> &nbsp;&nbsp;&nbsp;&nbsp; <b>- 이용허락 기간 </b> <input type="text" class="form-control" name="ANUC_ITEM_4" id="ANUC_ITEM_4" value="<c:out value="${info.ANUC_ITEM_4}"/>" style="width: 300px; margin-left: 50px; display: inline;" /><br /> &nbsp;&nbsp;&nbsp;&nbsp; <b>- 보상금 </b><input type="text" class="form-control"
											name="ANUC_ITEM_5" id="ANUC_ITEM_5" value="<c:out value="${info.ANUC_ITEM_5}"/>" style="width: 300px; margin-left: 95px; display: inline;"
										/><br /> <br /> <b>5. 저작물의 이용방법 및 형태 </b><br /> <input type="text" class="form-control" name="ANUC_ITEM_6" id="ANUC_ITEM_6" value="<c:out value="${info.ANUC_ITEM_6}"/>" style="width: 100%; height: 100px; display: inline;" /><br /> <br /> <b>6. 승인 일자 </b> <input type="text" class="form-control" name="ANUC_ITEM_7" id="ANUC_ITEM_7" value="<c:out value="${info.ANUC_ITEM_7}"/>" style="width: 300px; margin-left: 85px; display: inline;" />
									</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		<div class="box-footer" style="text-align: right">
			<div class="btn-group">
				<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<button type="submit" class="btn btn-primary" onclick="fncSave('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');">저장</button>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="btn-group">
				<button type="submit" class="btn btn-primary" onclick="fncGoList();">목록</button>
			</div>
		</div>
	</div>
</form>