<%@ page language="java" contentType="text/html; charset=EUC-KR"
  pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
//첨부파일 다운로드
function fncDown(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,ATTC_SEQN){
  location.href = '<c:url value="/console/legal/fdcrAd06Down1.page"/>?APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&ATTC_SEQN='+ATTC_SEQN;
}

//수정 폼으로 이동
function fncGoUpdateForm(APPLY_WRITE_YMD,APPLY_WRITE_SEQ){
  //var datas = [];
  //datas[0] = 'MENU_SEQN';
  var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06UpdateForm1.page"/>')+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ;
  location.href = url;
}

//신청서 삭제
function fncGoDelete(APPLY_WRITE_YMD,APPLY_WRITE_SEQ){
  //var datas = [];
  //datas[0] = 'MENU_SEQN';
  if(confirm("삭제하시겠습니까?")){
    var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06Delete1.page"/>')+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ;
    location.href = url;
  }
}

function fncGoList(){
  location.href = '<c:url value="/console/legal/fdcrAd06List1.page"/>';
}

//진행상태 수정
/* 
function fncGoHistChng(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,RECEIPT_NO){
  var STAT_CD = $('#STAT_CD').val();
  var STAT_MEMO = $('#STAT_MEMO').val();
  var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06StatChange1.page"/>')+'&STAT_CD='+STAT_CD+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&RECEIPT_NO='+RECEIPT_NO+'&STAT_MEMO='+STAT_MEMO;
  location.href = url;
}
 */
function fncGoHistChng(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,RECEIPT_NO,RGST_IDNT,APPLY_WORKS_TITL,APPLY_NO){
  var STAT_CD = $('#STAT_CD').val();
  var STAT_MEMO = $('#STAT_MEMO').val();
  
  var f = document.statForm;
  f.action = '<c:url value="/console/legal/fdcrAd06StatChange1.page"/>?STAT_CD='
    + STAT_CD+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&RECEIPT_NO='
    +RECEIPT_NO+'&STAT_MEMO='+STAT_MEMO+'&RGST_IDNT='+RGST_IDNT+'&APPLY_WORKS_TITL='+APPLY_WORKS_TITL+'&APPLY_NO='+APPLY_NO;
  f.submit();
}

//심의결과 변경상태 수정
function fncGoHistChng2(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,WORKS_SEQN){
  var STAT_RSLT_CD = $('#STAT_RSLT_CD').val();
  var STAT_RSLT_MEMO = $('#STAT_RSLT_MEMO').val();
  var LGMT_PERI = $('#LGMT_PERI').val();
  var LGMT_AMNT = $('#LGMT_AMNT').val();
  var LGMT_PLAC_NAME = $('#LGMT_PLAC_NAME').val();
  var LGMT_GRAN = $('#LGMT_GRAN').val();
  var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06StatWorksChange.page"/>')+'&STAT_RSLT_CD='+STAT_RSLT_CD+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&WORKS_SEQN='+WORKS_SEQN+'&STAT_RSLT_MEMO='+STAT_RSLT_MEMO
  +'&LGMT_PERI='+LGMT_PERI+'&LGMT_AMNT='+LGMT_AMNT+'&LGMT_PLAC_NAME='+LGMT_PLAC_NAME+'&LGMT_GRAN='+LGMT_GRAN;
  location.href = url;
}

//진행상태 내역조회 팝업
function fncGoHistory(APPLY_WRITE_YMD,APPLY_WRITE_SEQ){
  
  var popUrl = '<c:url value="/console/legal/fdcrAd06History1.page"/>?APPLY_WRITE_YMD='
    + APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ; //팝업창에 출력될 페이지 URL
  var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
  window.open(popUrl, "", popOption);
}

//심의결과 내역조회 팝업
function fncGoHistory2(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,WORKS_SEQN){
  
  var popUrl = '<c:url value="/console/legal/fdcrAd06History2.page"/>?APPLY_WRITE_YMD='
    + APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&WORKS_SEQN='+WORKS_SEQN; //팝업창에 출력될 페이지 URL
  var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
  window.open(popUrl, "", popOption);
}

//이용승인신청공고 등록화면 팝업
function fncGoPop3(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,WORKS_SEQN){
  
  /* 
  var APPLR_NAME = $('#APPLR_NAME').val();
  var WORKS_KIND = $('#WORKS_KIND').val();
  var WORKS_TITL = $('#WORKS_TITL').val();
  var COPT_HODR_NAME = $('#COPT_HODR_NAME').val();
  var PUBL_MEDI = $('#PUBL_MEDI').val();
  var PUBL_YMD = $('#PUBL_YMD').val();
  var RECEIPT_NO = $('#RECEIPT_NO').val();
   */
  
  var popUrl = '<c:url value="/console/legal/fdcrAd06Pop3.page"/>?APPLY_WRITE_YMD='
    + APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&WORKS_SEQN='+WORKS_SEQN; //팝업창에 출력될 페이지 URL
  var popOption = "width=796, height=784, resizable=no, scrollbars=yes, status=no;"; //팝업창 옵션(optoin)
  window.open(popUrl, "", popOption);
}

//이용승인공고 등록화면 팝업
function fncGoPop4(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,WORKS_SEQN){
  
  var popUrl = '<c:url value="/console/legal/fdcrAd06Pop4.page"/>?APPLY_WRITE_YMD='
    + APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&WORKS_SEQN='+WORKS_SEQN; //팝업창에 출력될 페이지 URL
  var popOption = "width=796, height=784, resizable=no, scrollbars=yes, status=no;"; //팝업창 옵션(optoin)
  window.open(popUrl, "", popOption);
}

//상세 보기
function fncGoApplyView(APPLY_WRITE_YMD,APPLY_WRITE_SEQ,WORKS_SEQN){
  var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd06View1.page"/>')+'&APPLY_WRITE_YMD='+APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&WORKS_SEQN='+WORKS_SEQN;
  location.href = url;
}

//이용승인신청서 출력
function fncGoReport1(APPLY_WRITE_YMD,APPLY_WRITE_SEQ){
  
  var report = "report/statPrps_report_main";
  var mode = "R";
  var url = '/myStat/statPrpsView.do?APPLY_WRITE_YMD='
    + APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&mode='+mode+'&report='+report+'&modal=1';
  
  var ServerUrl = gfn_getServerIP("/make_mls_admin_ci_main_Win32.jsp");
  location.href = url;
}

//이용승인신청명세서 출력
function fncGoReport2(APPLY_WRITE_YMD,APPLY_WRITE_SEQ){
  
  var report = "report/statPrps_report_list";
  var mode = "R";
  var url = '/myStat/statPrpsView.do?APPLY_WRITE_YMD='
    + APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ+'&mode='+mode+'&report='+report+'&modal=1';
  
  var ServerUrl = gfn_getServerIP("/make_mls_admin_ci_main_Win32.jsp");
  location.href = url;
}

</script>
<form name="boardForm" id="boardForm" method="post">
  <input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
</form>
<div class="box" style="margin-top: 30px;">
  <!-- /.box-header -->
  <div class="box-body">
      <div style="margin: 7px 0 15px 5px;"><font size="3">● 이용 승인신청 정보</font>
        <button type="submit" class="btn btn-primary" style="text-align: right; float: right;" onclick="fncGoReport1('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>');return false;">이용승인신청서</button>
      </div>
      <table class="table table-bordered text-center">
        <tbody>
          <tr>
            <th style="width: 20%">신청번호</th>
            <td style="width: 30%"><c:out value="${info.APPLY_NO}"/></td>
            <th style="width: 20%">접수번호</th>
            <td style="width: 30%"><c:out value="${info.RECEIPT_NO}"/></td>
          </tr>
           
          <tr>
            <td rowspan="2" colspan="3" style="padding-left: 322px;">
<input type="checkbox" name="APPLY_TYPE_01" id="APPLY_TYPE_01" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_01}"/>>저작물<br/>
<input type="checkbox" name="APPLY_TYPE_02" id="APPLY_TYPE_02" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_02}"/>>실연
<input type="checkbox" name="APPLY_TYPE_03" id="APPLY_TYPE_03" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_03}"/>>음반
<input type="checkbox" name="APPLY_TYPE_04" id="APPLY_TYPE_04" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_04}"/>>방송
<font size="4" style="padding-left: 150px;">이용 승인신청서</font><br/> 
<input type="checkbox" name="APPLY_TYPE_05" id="APPLY_TYPE_05" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_05}"/>>데이터베이스
            </td>
            <td>처리기간</td>
          </tr>
          <tr>
            <td>40일</td>
          </tr>
          
          <tr>
            <th >제호</th>
            <td colspan="3">
            <c:if test="${info.APPLY_WORKS_CNT > 1}"><input type="checkbox" checked="checked"></c:if>
            <c:if test="${info.APPLY_WORKS_CNT <= 1}"><input type="checkbox"></c:if>
            여러건 신청 : 총 <input style="width: 10%; display: inline;" class="form-control" type="text" name="APPLY_WORKS_CNT" id="APPLY_WORKS_CNT" value="<c:out value="${info.APPLY_WORKS_CNT}"/>" />건<br/>
            <input class="form-control" type="text" name="APPLY_WORKS_TITL" id="APPLY_WORKS_TITL" value="<c:out value="${info.APPLY_WORKS_TITL}"/>" />
            </td>
          </tr>
          <tr>
            <th style="width: 20%">종류</th>
            <td style="width: 30%"><c:out value="${info.APPLY_WORKS_KIND}"/></td>
            <th style="width: 20%">형태 및 수량</th>
            <td style="width: 30%"><c:out value="${info.APPLY_WORKS_FORM}"/></td>
          </tr>
          <tr>
            <th>이용의 내용</th>
            <td colspan="3">
            <textarea class="form-control" cols="5"><c:out value="${info.USEX_DESC}"/></textarea>
            </td>
          </tr>
          <tr>
            <th>승인신청사유</th>
            <td colspan="3">
            <textarea class="form-control" cols="5"><c:out value="${info.APPLY_REAS}"/></textarea>
            </td>
          </tr>
          <tr>
            <th>보상금액</th>
            <td colspan="3"><c:out value="${info.CPST_AMNT}"/></td>
          </tr>
          <tr> 
            <th>신청인 성명<br/>(법인명)</th>
            <td style="width: 30%"><c:out value="${info.APPLR_NAME}"/></td>
            <th>신청인 주민등록번호<br/>(법인등록번호)</th>
            <td style="width: 30%"><c:out value="${info.APPLR_RESD_CORP_NUMB}"/></td>
          </tr>
          <tr> 
            <th>신청인 주소</th>
            <td style="width: 30%"><c:out value="${info.APPLR_ADDR}"/></td>
            <th>신청인 전화번호</th>
            <td style="width: 30%"><c:out value="${info.APPLR_TELX}"/></td>
          </tr>
          <tr> 
            <th>대리인 성명<br/>(법인명)</th>
            <td style="width: 30%"><c:out value="${info.APPLY_PROXY_NAME}"/></td>
            <th>대리인 주민등록번호<br/>(법인등록번호)</th>
            <td style="width: 30%"><c:out value="${info.APPLY_PROXY_RESD_CORP_NUMB}"/></td>
          </tr>
          <tr> 
            <th>대리인 주소</th>
            <td style="width: 30%"><c:out value="${info.APPLY_PROXY_ADDR}"/></td>
            <th>대리인 전화번호</th>
            <td style="width: 30%"><c:out value="${info.APPLY_PROXY_TELX}"/></td>
          </tr>
          <tr>
            <td colspan="4" style="padding-left: 322px;">
            <%--
            APPLY_RAW_CD:50 
            APPLY_RAW_CD:51 
            APPLY_RAW_CD:52 
            APPLY_RAW_CD:89 
            APPLY_RAW_CD:97 
            APPLY_TYPE_01:저작물
            APPLY_TYPE_02:실연
            APPLY_TYPE_03:음반
            APPLY_TYPE_04:방송
            APPLY_TYPE_05:데이터베이스
            --%>
<input type="checkbox" value="50" <console:fn func="isChecked" value="50" value1="${info.APPLY_RAW_CD}"/>> 제50조<br/>
<input type="checkbox" value="51" <console:fn func="isChecked" value="51" value1="${info.APPLY_RAW_CD}"/>> 제51조&nbsp;&nbsp;&nbsp;
<input type="checkbox" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_01}"/>> 저작물<br/>
저작권법 &nbsp;&nbsp;&nbsp;<input type="checkbox" value="52" <console:fn func="isChecked" value="52" value1="${info.APPLY_RAW_CD}"/>> 제52조에 따라 위와 같이&nbsp;&nbsp;&nbsp;
<input type="checkbox" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_02}"/>> 실연
<input type="checkbox" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_03}"/>> 음반
<input type="checkbox" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_04}"/>> 방송
이용의 승인을 신청합니다.<br/>
<input type="checkbox" value="89" <console:fn func="isChecked" value="89" value1="${info.APPLY_RAW_CD}"/>> 제89조&nbsp;&nbsp;&nbsp;
<input type="checkbox" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_05}"/>> 데이터베이스<br/>
<input type="checkbox" value="97" <console:fn func="isChecked" value="97" value1="${info.APPLY_RAW_CD}"/>> 제97조<br/>
            </td>
          </tr>
          <tr>
            <th>첨부서류</th>
            <td colspan="3" id="TdIdFile">
            <c:choose>
            <c:when test="${empty attachList}">
              <span>등록된 첨부서류가 없습니다.</span>
            </c:when>
            <c:otherwise>
              <c:forEach var="attach" items="${attachList}"
                varStatus="listStatus">
              <p class="file">
                <console:fn func="getFileIcon" value="${attach.REAL_FILE_NAME}"/>
                <a href="#" onclick="fncDown('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>','<c:out value="${attach.ATTC_SEQN}"/>');return false;"><c:out value="${attach.FILE_NAME}"/></a>
                <span>(파일크기: <console:fn func="strFileSize" value="${attach.FILE_SIZE}"/>)</span>
              </p>
              </c:forEach>
            </c:otherwise>
            </c:choose>
            </td>
          </tr>
          <form name="statForm" id="statForm" method="post" enctype="multipart/form-data">
          <tr>
            <th>진행상태<br/>
              <div class="btn-group" style="display: inline-block;">
                <button type="submit" class="btn btn-primary" onclick="fncGoHistory('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>');return false;">내역조회</button>
              </div>
            </th>
            <td colspan="3">
            <span style="width:30%; display: inline;"><b>진행상태</b></span>
              <select class="form-control" id="STAT_CD" name="STAT_CD" style="width: 20%;display: inline; margin-left: 50px;" >
                <option value="1" <c:if test="${info.STAT_CD eq '1'}">selected="selected"</c:if>>임시저장</option>
                <option value="2" <c:if test="${info.STAT_CD eq '2'}">selected="selected"</c:if>>신청완료</option>
                <option value="3" <c:if test="${info.STAT_CD eq '3'}">selected="selected"</c:if>>보완요청</option>
                <option value="4" <c:if test="${info.STAT_CD eq '4'}">selected="selected"</c:if>>보완임시저장</option>
                <option value="5" <c:if test="${info.STAT_CD eq '5'}">selected="selected"</c:if>>보완완료</option>
                <option value="6" <c:if test="${info.STAT_CD eq '6'}">selected="selected"</c:if>>반려</option>
                <option value="7" <c:if test="${info.STAT_CD eq '7'}">selected="selected"</c:if>>결제요청</option>
                <option value="8" <c:if test="${info.STAT_CD eq '8'}">selected="selected"</c:if>>접수완료</option>
                <option value="9" <c:if test="${info.STAT_CD eq '9'}">selected="selected"</c:if>>심의중</option>
                <option value="10" <c:if test="${info.STAT_CD eq '10'}">selected="selected"</c:if>>심의완료</option>
              </select>
              
              <button type="submit" class="btn btn-primary" onclick="fncGoHistChng('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>','<c:out value="${info.RECEIPT_NO}"/>','<c:out value="${info.RGST_IDNT}"/>','<c:out value="${info.APPLY_WORKS_TITL}"/>','<c:out value="${info.APPLY_NO}"/>');return false;">저장</button>
              <br/>
            <span style="width:30%; display: inline;"><b>진행상태 비고</b></span>
              <textarea class="form-control" cols="5" id="STAT_MEMO" name="STAT_MEMO" style="width: 50%;display: inline; margin-top: 10px; margin-left: 20px;"><c:out value="${info.STAT_MEMO}"/></textarea>
              <br/>
            <span style="width:30%; display: inline;"><b>첨부파일</b></span>
              <input type="file" id="file1" name="file1" value="" size="50" style="width: 50%;display: inline; margin-top: 5px; margin-left: 50px;"><input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);">
            </td>
          </tr>
          </form>
      </table>
      <br/><br/>
      
      <div style="margin: 7px 0 15px 5px;"><font size="3">● 이용 승인신청 명세서 정보</font>
        <button type="submit" class="btn btn-primary" style="text-align: right; float: right;" onclick="fncGoReport2('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>');return false;">이용승인신청명세서</button>
      </div>
      <table class="table table-bordered text-center">
        <thead>
        <tr>
          <th style="width: 8%">순번</th>
          <th>제목</th>
          <th style="width: 10%">종류</th>
          <th style="width: 10%">형태 및 수량</th>
          <th style="width: 12%">공표연월일</th>
          <th style="width: 10%">관리자 성명</th>
        </tr>
        </thead>
        <tbody>
          <c:choose>
            <c:when test="${empty applyWorksList}">
              <span>등록된 명세서가 없습니다.</span>
            </c:when>
            <c:otherwise>
              <c:forEach var="applyWorks" items="${applyWorksList}" varStatus="listStatus">
              <tr>
              <td class="text-center"><c:out value="${applyWorks.WORKS_SEQN }"/></td>
              <td class="text-center"><a href="#" onclick="fncGoApplyView('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>','<c:out value="${applyWorks.WORKS_SEQN}"/>');"><c:out value="${applyWorks.WORKS_TITL }"/></a></td>
              <td class="text-center"><c:out value="${applyWorks.WORKS_KIND }"/></td>
              <td class="text-center"><c:out value="${applyWorks.WORKS_FORM }"/></td>
              <td class="text-center"><c:out value="${applyWorks.PUBL_YMD }"/></td>
              <td class="text-center"><c:out value="${applyWorks.COPT_HODR_NAME }"/></td>
              </tr>
              </c:forEach>
            </c:otherwise>
          </c:choose>
        </tbody>
      </table>
      <br/>
      <table class="table table-bordered text-center">
        <tbody>
        <c:choose>
        <c:when test="${empty applyWorksInfo}">
          <span>등록된 명세서가 없습니다.</span>
        </c:when>
        <c:otherwise>
          <c:forEach var="applyWorksInfo" items="${applyWorksInfo}" varStatus="listStatus">
          <tr>
          <td rowspan="2" style="width: 20%"><b>
<input type="checkbox" name="APPLY_TYPE_01" id="APPLY_TYPE_01" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_01}"/>>저작물<br/>
<input type="checkbox" name="APPLY_TYPE_02" id="APPLY_TYPE_02" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_02}"/>>실연
<input type="checkbox" name="APPLY_TYPE_03" id="APPLY_TYPE_03" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_03}"/>>음반
<input type="checkbox" name="APPLY_TYPE_04" id="APPLY_TYPE_04" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_04}"/>>방송<br/>
<input type="checkbox" name="APPLY_TYPE_05" id="APPLY_TYPE_05" value="1" <console:fn func="isChecked" value="1" value1="${info.APPLY_TYPE_05}"/>>데이터베이스
          </b></td>
          <td class="text-center" style="width: 20%"><b>제호(제목)</b></td>
          <td colspan="3" style="width: 60%"><c:out value="${applyWorksInfo.WORKS_TITL }"/></td>
          </tr>
          <tr>
          <td class="text-center" style="width: 20%"><b>종류</b></td>
          <td><c:out value="${applyWorksInfo.WORKS_KIND }"/></td>
          <td class="text-center" style="width: 20%"><b>형태 및 수량</b></td>
          <td><c:out value="${applyWorksInfo.WORKS_FORM }"/></td>
          </tr>
          
          <tr>
          <td class="text-center" rowspan="2" style="width: 20%"><b>공표</b><br/>(실연/음반발매/방송<br/>등 포함)</td>
          <td class="text-center" style="width: 20%"><b>공표연월일</b></td>
          <td><c:out value="${applyWorksInfo.PUBL_YMD }" /></td>
          <td class="text-center" style="width: 20%"><b>공표국가</b></td>
          <td><c:out value="${applyWorksInfo.PUBL_NATN }" /></td>
          </tr>
          <tr>
          <td class="text-center" style="width: 20%"><b>공표방법</b></td>
          <td><b>
<input type="checkbox" value="1" <console:fn func="isChecked" value="1" value1="${applyWorksInfo.PUBL_MEDI_CD}"/>>출판
<input type="checkbox" value="2" <console:fn func="isChecked" value="2" value1="${applyWorksInfo.PUBL_MEDI_CD}"/>>인터넷<br/>
<input type="checkbox" value="3" <console:fn func="isChecked" value="3" value1="${applyWorksInfo.PUBL_MEDI_CD}"/>>복제·배포(판매)<br/>
<input type="checkbox" value="4" <console:fn func="isChecked" value="4" value1="${applyWorksInfo.PUBL_MEDI_CD}"/>>공연
<input type="checkbox" value="5" <console:fn func="isChecked" value="5" value1="${applyWorksInfo.PUBL_MEDI_CD}"/>>전시<br/>
<input type="checkbox" value="6" <console:fn func="isChecked" value="6" value1="${applyWorksInfo.PUBL_MEDI_CD}"/>>방송<br/>
<input type="checkbox" value="7" <console:fn func="isChecked" value="7" value1="${applyWorksInfo.PUBL_MEDI_CD}"/>>기타<br/>
          </b></td>
          <td class="text-center" style="width: 20%"><b>공표매체정보</b></td>
          <td><c:out value="${applyWorksInfo.PUBL_MEDI }"/></td>
          </tr>
          
          <tr>
          <td class="text-center" rowspan="2" style="width: 20%;"><b><br/>관리자</b></td>
          <td class="text-center" style="width: 20%"><b>성명<br/>(법인명)</b></td>
          <td><c:out value="${applyWorksInfo.COPT_HODR_NAME }"/></td>
          <td class="text-center" style="width: 20%"><b>전화번호</b></td>
          <td><c:out value="${applyWorksInfo.COPT_HODR_TELX_NUMB }"/></td>
          </tr>
          <tr>
          <td class="text-center" style="width: 20%"><b>주소</b></td>
          <td colspan="3"><c:out value="${applyWorksInfo.COPT_HODR_ADDR }"/></td>
          </tr>
          
          <tr>
          <td class="text-center" style="width: 20%;"><b><br/>산출물의 내용</b></td>
          <td colspan="4">
            <textarea class="form-control" cols="5"><c:out value="${applyWorksInfo.WORKS_DESC}"/></textarea>
          </td>
          </tr>
          
          <tr>
          <td class="text-center" rowspan="7" style="width: 20%;"><b><br/>심의결과</b><br/>
              <div class="btn-group" style="display: inline-block;">
                <button type="submit" class="btn btn-primary" onclick="fncGoHistory2('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>','<c:out value="${applyWorksInfo.WORKS_SEQN}"/>');return false;">내역조회</button>
              </div></td>
          <td class="text-center" colspan="4"><b>심의결과 수정/저장</b></td>
          </tr>
          <tr>
          <td colspan="4">
            <span style="width:30%; display: inline;"><b>심의결과</b></span>
              <select class="form-control" id="STAT_RSLT_CD" name="STAT_RSLT_CD" style="width: 20%;display: inline; margin-left: 50px;" >
                <option value="0" <c:if test="${applyWorksInfo.STAT_RSLT_CD eq '0'}">selected="selected"</c:if>>선택</option>
                <option value="1" <c:if test="${applyWorksInfo.STAT_RSLT_CD eq '1'}">selected="selected"</c:if>>공고</option>
                <option value="2" <c:if test="${applyWorksInfo.STAT_RSLT_CD eq '2'}">selected="selected"</c:if>>승인</option>
                <option value="3" <c:if test="${applyWorksInfo.STAT_RSLT_CD eq '3'}">selected="selected"</c:if>>거절</option>
                <option value="4" <c:if test="${applyWorksInfo.STAT_RSLT_CD eq '4'}">selected="selected"</c:if>>기타</option>
              </select>
              <button type="submit" class="btn btn-primary" onclick="fncGoHistChng2('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>','<c:out value="${applyWorksInfo.WORKS_SEQN}"/>');return false;">저장</button>
              <button type="submit" class="btn btn-primary" style="text-align: right; float: right;" onclick="fncGoPop4('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>','<c:out value="${applyWorksInfo.WORKS_SEQN}"/>');return false;">이용승인공고</button>
              <button type="submit" class="btn btn-primary" style="text-align: right; float: right; margin-right: 15px;" onclick="fncGoPop3('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>','<c:out value="${applyWorksInfo.WORKS_SEQN}"/>');return false;">이용승인신청공고</button>
              <br/>
            <span style="width:30%; display: inline;"><b>심의결과 비고</b></span>
              <textarea class="form-control" cols="5" style="width: 50%;display: inline; margin-top: 10px; margin-left: 20px;" id="STAT_RSLT_MEMO" name="STAT_RSLT_MEMO"><c:out value="${applyWorksInfo.STAT_RSLT_MEMO}"/></textarea>
              <br/>
            <span style="width:30%; display: inline;"><b>첨부파일</b></span>
              <input type="file" id="file2" name="file2" value="" size="50" style="width: 50%;display: inline; margin-top: 5px; margin-left: 50px;"><input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);">
          </td>
          </tr>
          <tr>
          <td class="text-center" colspan="4"><b>이용승인 조건</b></td>
          </tr>
          <tr>
          <td><b>이용허락 기간</b></td>
          <td colspan="3"><input type="text" class="form-control" id="LGMT_PERI" name="LGMT_PERI" value="${applyWorksInfo.LGMT_PERI}"/></td>
          </tr>
          <tr>
          <td><b>공탁금액</b></td>
          <td colspan="3"><input type="text" class="form-control" id="LGMT_AMNT" name="LGMT_AMNT" value="${applyWorksInfo.LGMT_AMNT}"/></td>
          </tr>
          <tr>
          <td><b>공탁소의 명칭 및 소재지</b></td>
          <td colspan="3"><input type="text" class="form-control" id="LGMT_PLAC_NAME" name="LGMT_PLAC_NAME" value="${applyWorksInfo.LGMT_PLAC_NAME}"/></td>
          </tr>
          <tr>
          <td><b>공탁근거</b></td>
          <td colspan="3"><input type="text" class="form-control" id="LGMT_GRAN" name="LGMT_GRAN" value="${applyWorksInfo.LGMT_GRAN}"/></td>
          </tr>
          </c:forEach>
        </c:otherwise>
        </c:choose>
        </tbody>
      </table>
    </div>
  <div class="box-footer" style="text-align:right">
  <c:if test="${info.STAT_CD eq '8'}">
    <div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoUpdateForm('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>');return false;">수정</button></div>
  </c:if>
    <div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoDelete('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>');return false;">삭제</button></div>
    <div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoList();">목록</button></div>
  </div>
</div>