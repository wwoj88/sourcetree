<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd11List1.page"/>',datas, page);
		location.href = url;
	}
	//검색
	function fncGoSearch() {
		var SCH_LYRI_WRTR = $('#SCH_LYRI_WRTR').val();
		var SCH_WORKS_DIVS_CD = $('#SCH_WORKS_DIVS_CD').val();
		var SCH_WORKS_TITLE = $('#SCH_WORKS_TITLE').val();
		var SCH_STAT_OBJC_CD = $('#SCH_STAT_OBJC_CD').val();
		
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd11List1.page"/>')+'&SCH_LYRI_WRTR='+SCH_LYRI_WRTR+'&SCH_WORKS_DIVS_CD='+SCH_WORKS_DIVS_CD+'&SCH_WORKS_TITLE='+SCH_WORKS_TITLE+'&SCH_STAT_OBJC_CD='+SCH_STAT_OBJC_CD;
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(WORKS_ID){
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd11View1.page"/>')+'&WORKS_ID='+WORKS_ID;
		location.href = url;
	}
	
	//법정허락 대상물 공고 목록 탭 클릭
	function fncGoView0(TAB_INDEX){
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd11List1.page"/>')+'&TAB_INDEX='+TAB_INDEX;
		location.href = url;
	}
	
	//이의제기 목록 탭 클릭
	function fncGoView1(TAB_INDEX){
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd11List2.page"/>')+'&TAB_INDEX='+TAB_INDEX;
		location.href = url;
	}
	
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">구분</th>
									<td colspan="3">
										<select id="SCH_WORKS_DIVS_CD" name="SCH_WORKS_DIVS_CD">
											<option selected="selected" value="" >전체</option>
											<option value="1">미분배(방송음악)</option>
											<option value="2">미분배(교과용)</option>
											<option value="3">미분배(도서관)</option>
											<option value="7">미분배(수업목적)</option>
											<option value="4">거소불명</option>
											<option value="6">기승인</option>
										</select>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">저작권자</th>
									<td colspan="3">
										<input type="text" id="SCH_LYRI_WRTR" name="SCH_LYRI_WRTR"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">제호</th>
									<td colspan="3">
										<input type="text" id="SCH_WORKS_TITLE" name="SCH_WORKS_TITLE"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">이의제기</th>
									<td colspan="3">
										<select id="SCH_STAT_OBJC_CD" name="SCH_STAT_OBJC_CD">
											<option selected="selected" value="">전체</option>
											<option value="1">신청</option>
											<option value="2">접수</option>
											<option value="3">처리완료</option>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>
<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
	  <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="true" onclick="fncGoView0('0');">법정허락 대상물 공고 목록</a></li>
	  <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false" onclick="fncGoView1('1');">이의제기 목록</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div class="box" style="margin-top: 30px;">
		<!-- /.box-header -->
				<div class="box-body">
					<table id="fdcrAd11List1" class="table table-bordered">
						<thead>
							<tr>
								<th class="text-center" style="width: 7%">순번</th>
								<th class="text-center">제호</th>
								<th class="text-center" style="width: 12%">저작권자</th>
								<th class="text-center" style="width: 10%">공표일자</th>
								<th class="text-center" style="width: 12%">구분</th>
								<th class="text-center" style="width: 10%">공고일자</th>
								<th class="text-center" style="width: 10%">이의제기</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty ds_list}">
									<tr>
										<td colspan="6" class="text-center">게시물이 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
										<tr>
											<td class="text-center"><c:out value="${info.ROW_NUM}" /></td>
											<td><a href="#" onclick="fncGoView('<c:out value="${info.WORKS_ID}"/>')"><c:out value="${info.WORKS_TITLE}" /></a></td>
											<td class="text-center"><c:out value="${info.COPT_HODR}" /></td>
											<td class="text-center"><c:out value="${info.MEDI_OPEN_DATE}" /></td>
											<td class="text-center">
											<c:if test="${info.WORKS_DIVS_CD == '1'}">미분배(방송음악)</c:if>
											<c:if test="${info.WORKS_DIVS_CD == '2'}">미분배(교과용)</c:if>
											<c:if test="${info.WORKS_DIVS_CD == '3'}">미분배(도서관)</c:if>
											<c:if test="${info.WORKS_DIVS_CD == '4'}">거소불명</c:if>
											<c:if test="${info.WORKS_DIVS_CD == '6'}">기승인</c:if>
											<c:if test="${info.WORKS_DIVS_CD == '7'}">미분배(수업목적)</c:if>
											</td>
											<td class="text-center"><c:out value="${info.OPEN_DTTM}" /></td>
											<td class="text-center"><c:out value="${info.STAT_OBJC_CD}" /></td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
				<div class="box-footer clearfix text-center">
				  <ul class="pagination pagination-sm no-margin">
				    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
				  </ul>
				</div>
			</div>
		</div>
	</div>
</div>
