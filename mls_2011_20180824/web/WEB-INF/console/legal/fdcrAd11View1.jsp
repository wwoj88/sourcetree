<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>

	$(document).ready(function() {
		var STAT_OBJC_CD = $('#STAT_OBJC_CD').val();
		if(STAT_OBJC_CD == '3'){
			document.getElementById("STAT_OBJC_RSLT_CD").style.display = "inline-block";
		}else { 
			document.getElementById("STAT_OBJC_RSLT_CD").style.display = "none";
		}
	});

	//첨부파일 다운로드
	function fncDown(WORKS_ID) {
		location.href = '<c:url value="/console/legal/fdcrAd11Download1.page"/>?WORKS_ID='
				+ WORKS_ID;
	}

	//회원정보 팝업
	function popupOpen(OBJC_RGST_ID) {
		var popUrl = '<c:url value="/console/legal/fdcrAd11Pop1.page"/>?OBJC_RGST_ID='
				+ OBJC_RGST_ID;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=800, height=550, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	//보완 팝업
	function popupOpen2(BORD_CD, BORD_SEQN) {
		var popUrl = '<c:url value="/console/legal/fdcrAd10Pop2.page"/>?BORD_CD='
			+ BORD_CD + '&BORD_SEQN=' + BORD_SEQN;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}

	//공고 승인 상태 수정
	function fncGoUpdate(BORD_CD, BORD_SEQN) {
		var f = document.anucStatCdForm;
		if (confirm("상태를 저장하시겠습니까?")) {
			f.action = '<c:url value="/console/legal/fdcrAd10Update2.page"/>';
			f.submit();
		}
	}

	//게시글 삭제
	function fncGoDelete(BORD_CD, BORD_SEQN) {
		if (confirm("삭제하시겠습니까?")) {
			location.href = '<c:url value="/console/legal/fdcrAd10Delete1.page"/>?BORD_CD='
					+ BORD_CD + '&BORD_SEQN=' + BORD_SEQN;
		}
	}
 
	function fncGoList(OBJC_YN) {
		if (OBJC_YN == 'Y') {
			location.href = '<c:url value="/console/legal/fdcrAd11List2.page"/>?OBJC_YN='
				+ OBJC_YN;
		} else {
			location.href = '<c:url value="/console/legal/fdcrAd11List1.page"/>?OBJC_YN='
				+ OBJC_YN;
		}
	}
	
	// 진행상태 내역조회 팝업
	function fncGoHistory(STAT_OBJC_ID){
		
		var popUrl = '<c:url value="/console/legal/fdcrAd11Pop2.page"/>?STAT_OBJC_ID='
			+ STAT_OBJC_ID;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	//메일발신함 조회
	function fncGoSendList(SUPL_ID) {
		var popUrl = '<c:url value="/console/legal/fdcrAd10SuplSendList1.page"/>?SUPL_ID='
			+ SUPL_ID;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function fncGoModify(STAT_OBJC_ID,WORKS_ID){
		var STAT_OBJC_CD = $('#STAT_OBJC_CD').val();
		var STAT_OBJC_RSLT_CD = $('#STAT_OBJC_RSLT_CD').val();
		alert("STAT_OBJC_ID"+STAT_OBJC_ID);
		alert("WORKS_ID"+WORKS_ID);
		alert("STAT_OBJC_CD"+STAT_OBJC_CD);
		alert("STAT_OBJC_RSLT_CD"+STAT_OBJC_RSLT_CD);
		if (confirm("수정하시겠습니까?")) {
			location.href = '<c:url value="/console/legal/fdcrAd11Update1.page"/>?STAT_OBJC_ID='
					+ STAT_OBJC_ID + 'WORKS_ID=' + WORKS_ID + 'STAT_OBJC_CD=' + STAT_OBJC_CD + 'STAT_OBJC_RSLT_CD=' + STAT_OBJC_RSLT_CD;
		}
		//var f = document.boardForm;
		//f.action = '<c:url value="/console/legal/fdcrAd11Update1.page"/>?STAT_OBJC_ID='
		//+ STAT_OBJC_ID;
		//f.submit();
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex"value="<c:out value="${param.pageIndex}"/>" />
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered text-center">
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td>등록된 정보가 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<th style="width: 20%">제호</th>
								<td style="width: 30%"><c:out value="${info.WORKS_TITLE}" /></td>
								<th>구분</th>
								<td style="width: 30%"><c:out value="${info.WORKS_DIVS_CD_NM}" /></td>
							</tr>
							<tr>
								<th style="width: 20%">공표정보</th>
								<td style="width: 30%"><c:out value="${info.PUBL_MEDI}" /></td>
								<th style="width: 20%">공표일자</th>
								<td style="width: 30%"><c:out value="${info.MEDI_OPEN_DATE}" /></td>
							</tr>
							<tr>
								<th style="width: 20%">저작권자</th>
								<td style="width: 30%"><c:out value="${info.COPT_HODR}" /></td>
								<th style="width: 20%">공고일</th>
								<td style="width: 30%"><c:out value="${info.OPEN_DTTM}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	<%-- 
	<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
		<div class="box-footer" style="width: 100%;">
			<div class="btn-group" style="float: right;">
				<button type="submit" class="btn btn-primary"
					onclick="fncGoList('<c:out value="${info.OBJC_YN}"/>');return false;">목록</button>
			</div>
		</div>
	</c:forEach>
	 --%>
	<br/><br/><br/><br/>
	 
	<c:choose>
	<c:when test="${empty ds_object}">
		<tr>
			<!-- <td colspan="6" class="text-center">등록된 공고정보 보완내역이 없습니다.</td> -->
		</tr>
	</c:when>
	<c:otherwise>
	<div class="box-body">
		<table class="table table-bordered text-center">
		<h4><b>▶ 이의제기 ${fn:length(ds_object)} 건</b></h4>
			<tbody>
				<c:forEach var="info" items="${ds_object}" varStatus="listStatus">
					<tr>
						<th rowspan="3" style="width: 20%">이의제기<br/>신청정보</th>
						<th style="width: 10%">신청인</th>
						<td style="width: 30%"><a href="#" onclick="popupOpen('<c:out value="${info.OBJC_RGST_ID}"/>');return false;"><u><c:out value="${info.OBJC_RGST_NAME }"/></u></a></td>
						<th style="width: 10%">신청일자</th>
						<td style="width: 30%"><c:out value="${info.OBJC_RGST_DTTM }"/></td>
					</tr>
					<tr>
						<th>내용</th>
						<td colspan="3"><c:out value="${info.OBJC_DESC }"/></td>
					</tr>
					<tr>
						<th>첨부파일</th>
						<%-- <td colspan="3"><c:out value="${info.OBJC_RGST_DTTM }"/></td> --%>
						<td colspan="3" id="TdIdFile">
							<c:choose>
							<c:when test="${empty ds_object_file}">
								<span>등록된 첨부서류가 없습니다.</span>
							</c:when>
							<c:otherwise>
								<c:forEach var="attach" items="${ds_object_file}"
									varStatus="listStatus">
								<p class="file">
									<console:fn func="getFileIcon" value="${attach.REAL_FILE_NAME}"/>
									<a href="#" onclick="fncDown('<c:out value="${info.WORKS_ID}"/>');return false;"><c:out value="${attach.FILE_NAME}"/></a>
									<span>(파일크기: <console:fn func="strFileSize" value="${attach.FILE_SIZE}"/>)</span>
								</p>
								</c:forEach>
							</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th rowspan="3">이의제기<br/>진행상태<br/>
							<div class="btn-group" style="display: inline-block;">
								<button type="submit" class="btn btn-primary" onclick="fncGoHistory('<c:out value="${info.STAT_OBJC_ID}"/>');return false;">내역조회</button>
							</div>
						</th>
						<th>진행상태</th>
						<td>
							<select class="form-control" id="STAT_OBJC_CD" name="STAT_OBJC_CD" style="width: 200px;">
								<option value="1" <c:if test="${info.STAT_OBJC_CD eq '1'}">selected="selected"</c:if>>신청</option>
								<option value="2" <c:if test="${info.STAT_OBJC_CD eq '2'}">selected="selected"</c:if>>접수</option>
								<option value="3" <c:if test="${info.STAT_OBJC_CD eq '3'}">selected="selected"</c:if>>처리완료</option>
							</select>
						</td>
						<th>처리결과</th>
						<td>
							<select class="form-control" id="STAT_OBJC_RSLT_CD" name="STAT_OBJC_RSLT_CD" style="width: 200px; display: inline-block;">
								<option value="0" <c:if test="${info.STAT_OBJC_RSLT_CD eq '0'}">selected="selected"</c:if>>선택</option>
								<option value="1" <c:if test="${info.STAT_OBJC_RSLT_CD eq '1'}">selected="selected"</c:if>>승인</option>
								<option value="2" <c:if test="${info.STAT_OBJC_RSLT_CD eq '2'}">selected="selected"</c:if>>기각</option>
							</select>
							<div class="btn-group" style="float: right; display: inline-block;">
								<button type="submit" class="btn btn-primary"
									onclick="fncGoModify('<c:out value="${info.STAT_OBJC_ID}"/>','<c:out value="${info.WORKS_ID}"/>');return false;">수정</button>
							</div>
						</td>
					</tr>
					<tr>
						<th>진행상태 비고</th>
						<td colspan="3"><c:out value="${info.STAT_OBJC_MEMO }"/></td>
					</tr>
					<tr>
						<th>첨부파일</th>
						<%-- <td colspan="3"><c:out value="${info.OBJC_RGST_DTTM }"/></td> --%>
						<td colspan="3" id="TdIdFile">
							<c:choose>
							<c:when test="${empty ds_object_file}">
								<span>등록된 첨부서류가 없습니다.</span>
							</c:when>
							<c:otherwise>
								<c:forEach var="attach" items="${ds_object_file}"
									varStatus="listStatus">
								<p class="file">
									<console:fn func="getFileIcon" value="${attach.REAL_FILE_NAME}"/>
									<a href="#" onclick="fncDown('<c:out value="${info.WORKS_ID}"/>');return false;"><c:out value="${attach.FILE_NAME}"/></a>
									<span>(파일크기: <console:fn func="strFileSize" value="${attach.FILE_SIZE}"/>)</span>
								</p>
								</c:forEach>
							</c:otherwise>
							</c:choose>
							<div style="height:50px;" onclick="fncFileAdd();return false;"><i class="fa fa-fw fa-plus"></i><input type="button" value="파일추가" style="height:30px;"></div>
							<div id="DivIdFile1" ><input type="file" id="file1" name="file1" value="" size="50" style="float:left"><input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);"></div>
						</td>
					</tr>
				 </c:forEach>
			</tbody>
		</table>
	</div>
	</c:otherwise>
	</c:choose>
	
	<c:if test="${empty ds_object}">
	 	<div class="box-footer" style="width: 100%;">
			<div class="btn-group" style="float: right;">
				<button type="submit" class="btn btn-primary"
					onclick="fncGoList('N');return false;">목록</button>
			</div>
		</div>
	 </c:if>
	 <c:if test="${!empty ds_object}">
	 	<div class="box-footer" style="width: 100%;">
			<div class="btn-group" style="float: right;">
				<button type="submit" class="btn btn-primary"
					onclick="fncGoList('Y');return false;">목록</button>
			</div>
		</div>
	 </c:if>	
	 
</div>
</div>
</form>