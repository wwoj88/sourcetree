<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
	
	function fncGoSave(APPLY_WRITE_YMD,APPLY_WRITE_SEQ){
		var f = document.insertForm;
		f.action = '<c:url value="/console/legal/fdcrAd06Regi2.page"/>?APPLY_WRITE_YMD='
			+ APPLY_WRITE_YMD+'&APPLY_WRITE_SEQ='+APPLY_WRITE_SEQ;
		f.submit();
	}
</script>
<form name="insertForm" id="insertForm" method="post">
	<input type="hidden" id="BORD_CD" name="BORD_CD" value="<c:out value="${param.BORD_CD}"/>" /> 
	<input type="hidden" id="BORD_SEQN" name="BORD_SEQN" value="<c:out value="${param.BORD_SEQN}"/>" /> 
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶ 이용승인공고 게시판 등록</b></h4>
			<table class="table table-bordered">
				<tbody>
				<c:choose>
				<c:when test="${empty applyWorksInfo}">
					<span>등록된 정보가 없습니다.</span>
				</c:when>
				<c:otherwise>
					<c:forEach var="applyWorksInfo" items="${applyWorksInfo}" varStatus="listStatus">
					<tr>
						<th style="width: 20%">접수번호</th>
						<td style="width: 30%"><input class="form-control" type="text" name="RECEIPT_NO"
							id="RECEIPT_NO" value="<c:out value="${info.RECEIPT_NO}"/>" /></td>
						<th style="width: 20%">구분</th>
						<td style="width: 30%">
						<!-- <input class="form-control" type="text" name="DIVS_CD_NM" id="DIVS_CD_NM" value="" /> -->
							<select class="form-control" id="DIVS_CD" name="DIVS_CD">
								<option value="">--선택--</option>
								<option value="4">거소불명 저작물</option>
								<option value="1">미분배</option>
								<option value="5">개인</option>
							</select>
						</td>
					</tr>
					 
					<tr>
						<th style="width: 20%">제목</th>
						<td colspan="3"><input class="form-control" type="text" name="TITE" id="TITE" value="" /></td>
					</tr>
					<tr>
						<th style="width: 20%">공고일자</th>
						<td style="width: 30%"><input class="form-control" type="text" name="OPEN_DTTM" id="OPEN_DTTM" value="" /></td>
						<th style="width: 20%">공고자</th>
						<td style="width: 30%"><input class="form-control" type="text" name="RGST_IDNT" id="RGST_IDNT" value="" /></td>
					</tr>
					
					<tr>
					<th style="width: 20%">첨부파일</th>
					<td><font color="light-blue;">* 공고문 스캔파일 등록시<br/>jpg 파일형식으로 업로드 하시기 바랍니다.</font></td>
					<td colspan="2" id="TdIdFile">
						<div style="height:50px;" onclick="fncFileAdd();return false;"><i class="fa fa-fw fa-plus"></i><input type="button" value="파일추가" style="height:30px;"></div>
						<div id="DivIdFile1" ><input type="file" id="file1" name="file1" value="" size="50" style="float:left"><input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);"></div>
					</td>
					</tr>
					<tr>
						<th style="width: 20%">내용</th>
						<td colspan="3">
						<b>1. 저작물의 제호 및 공표연월일</b><br/>
						<input type="text" class="form-control" name="ANUC_ITEM_1" id="ANUC_ITEM_1" value="<c:out value="${applyWorksInfo.WORKS_TITL}"/>" style="width: 100%; display: inline;"/><br/><br/>
						<b>2. 저작자 또는 저작재간권자 성명</b><br/>
						<input type="text" class="form-control" name="ANUC_ITEM_2" id="ANUC_ITEM_2" value="<c:out value="${applyWorksInfo.COPT_HODR_NAME }"/>" style="width: 100%; display: inline;"/><br/><br/>
						<b>3. 이용승인을 받은 자의 성명</b>
						<input type="text" class="form-control" name="ANUC_ITEM_3" id="ANUC_ITEM_3" value="<c:out value="${info.APPLR_NAME}"/>" style="width: 100%; display: inline;"/><br/><br/>
						<b>4. 저작물의 이용승인 조건(이용허락 기간 및 보상금)</b><br/>
						&nbsp;&nbsp;&nbsp;&nbsp; <b>- 이용허락 기간 </b> <input type="text" class="form-control" name="ANUC_ITEM_4" id="ANUC_ITEM_4" value="" style="width: 300px; margin-left: 50px; display: inline;"/><br/>
						&nbsp;&nbsp;&nbsp;&nbsp; <b>- 보상금 </b><input type="text" class="form-control" name="ANUC_ITEM_5" id="ANUC_ITEM_5" value="" style="width: 300px; margin-left: 95px; display: inline;"/><br/><br/>
						<b>5. 저작물의 이용방법 및 형태 </b><br/>
						<input type="text" class="form-control" name="ANUC_ITEM_6" id="ANUC_ITEM_6" value="" style="width: 100%; height: 100px; display: inline;"/><br/><br/>
						<b>6. 승인 일자 </b> <input type="text" class="form-control" name="ANUC_ITEM_7" id="ANUC_ITEM_7" value="" style="width: 300px; margin-left: 85px; display: inline;"/>
						</td>
					</tr>
					</c:forEach>
					</c:otherwise>
				</c:choose>
				</tbody>
			</table>
			<div class="btn-group" style="float: right; margin-top: 20px;">
				<button type="submit" class="btn btn-primary" style="float: right;"
					onclick="closePop();">닫기</button>
					<button type="submit" class="btn btn-primary" style="float: right; margin-right: 10px;"
					onclick="fncGoSave('<c:out value="${info.APPLY_WRITE_YMD}"/>','<c:out value="${info.APPLY_WRITE_SEQ}"/>');return false;">저장</button>
			</div>
		</div>
</form>
