<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		//datas[0] = 'MENU_SEQN';

		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd08List1.page"/>',datas, page);
		
		location.href = url;
	}
	
	//검색
	function fncGoSearch() {
		var SCH_RECEIPT_NO = $('#SCH_RECEIPT_NO').val();
		var SCH_WORKS_DIVS_CD = $('#SCH_WORKS_DIVS_CD').val();
		var SCH_TITL = $('#SCH_TITL').val();
		var SCH_STAT_OBJC_CD = $('#SCH_STAT_OBJC_CD').val();
		
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd08List1.page"/>')+'&SCH_RECEIPT_NO='+SCH_RECEIPT_NO+'&SCH_WORKS_DIVS_CD='+SCH_WORKS_DIVS_CD+'&SCH_TITL='+SCH_TITL+'&SCH_STAT_OBJC_CD='+SCH_STAT_OBJC_CD;
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(BORD_CD,BORD_SEQN){
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd08View1.page"/>')+'&BORD_CD='+BORD_CD+'&BORD_SEQN='+BORD_SEQN;
		location.href = url;
	}
	
	//등록폼 이동
	function fncGoWriteForm(){
		//var datas = [];
		//datas[0] = 'MENU_SEQN';
		//var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd08WriteForm1.page"/>',datas);
		//location.href = url;
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd08WriteForm1.page"/>');
		location.href = url;
	} 
	
	//이용승인신청 탭 클릭
	function fncGoStatView(){
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd08List1.page"/>');
		location.href = url;
	}
	
	//이의제기 탭 클릭
	function fncGoObjcView(OBJC_YN){
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd08List2.page"/>')+'&OBJC_YN='+OBJC_YN;
		location.href = url;
	}
	
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">접수번호</th>
									<td colspan="3">
										<input type="text" id="SCH_RECEIPT_NO" name="SCH_RECEIPT_NO">
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">구분</th>
									<td colspan="3">
										<select id="SCH_WORKS_DIVS_CD" name="SCH_WORKS_DIVS_CD">
											<option selected="selected" value="" >전체</option>
											<option value="2">거소불명</option>
											<option value="1">미분배</option>
											<option value="3">개인</option>
										</select>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">제목</th>
									<td colspan="3">
										<input type="text" id="SCH_TITL" name="SCH_TITL">
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">이의제기</th>
									<td colspan="3">
										<select id="SCH_STAT_OBJC_CD" name="SCH_STAT_OBJC_CD">
												<option selected="selected" value="">전체</option>
												<option value="1">신청</option>
												<option value="2">접수</option>
												<option value="3">처리완료</option>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
			<!-- /.row -->
		</div>
		
		<!-- /.box-body -->
	</div>
</div>
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
	</div>
</div>
</div>
<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
	  <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="true" onclick="fncGoStatView();">이용승인 신청 공고  목록</a></li>
	  <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false" onclick="fncGoObjcView('Y');">이의제기 목록</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div class="box" style="margin-top: 30px;">
				<div class="box-header with-border pull-right">
					<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
				</div>
		<!-- /.box-header -->
				<div class="box-body">
					<table id="fdcrAd08List1" class="table table-bordered">
						<thead>
							<tr>
								<th class="text-center" style="width: 7%">순번</th>
								<th class="text-center" style="width: 12%">접수번호</th>
								<th class="text-center">제호(제목)</th>
								<th class="text-center" style="width: 11%">구분</th>
								<th class="text-center" style="width: 11%">공고일자</th>
								<th class="text-center" style="width: 11%">이의제기</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty list}">
									<tr>
										<td colspan="6" class="text-center">게시물이 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="info" items="${list}" varStatus="listStatus">
										<tr>
											<td class="text-center"><c:out value="${info.ROW_NUM}" /></td>
											<td class="text-center"><c:out value="${info.RECEIPT_NO}" /></td>
											<td><a href="#" onclick="fncGoView('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');"><c:out value="${info.TITE}" escapeXml="false" /></a></td>
											<td class="text-center"><c:out value="${info.DIVS_CD_NM}" /></td>
											<td class="text-center"><c:out value="${info.OPEN_DTTM}" /></td>
											<td class="text-center">
											<c:if test="${info.STAT_OBJC_CD == '1'}">신청</c:if>
											<c:if test="${info.STAT_OBJC_CD == '2'}">접수</c:if>
											<c:if test="${info.STAT_OBJC_CD == '3'}">처리완료</c:if>
											</td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
				<div class="box-footer clearfix text-center">
				  <ul class="pagination pagination-sm no-margin">
				    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
				  </ul>
				</div>
			</div>
		</div>
	</div>
</div>
