<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//검색
	function fncGoSearch() {
		var KEYWORD = $('#KEYWORD').val();
		var url = fncGetBoardParam('<c:url value="/console/legal/fdcrAd10SuplSendList1.page"/>')+'&KEYWORD='+KEYWORD;
		location.href = url;
	}
	
	//보완내역 상세팝업
	function fncGoDetail(SUPL_ID) {
		var popUrl = '<c:url value="/console/legal/fdcrAd10SuplListPop1.page"/>?SUPL_ID='
			+ SUPL_ID;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=700, height=500, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	MSG_STOR_CD
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
	<!-- /.box-header -->
	<h4><b>▶ 신청정보 보완안내 메일 발신내역</b></h4>
	<div class="box-body" style="width: 100%">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon" style="width:100px;"> <label>수신자</label></span>
						<span class="input-group-addon" style="width:80%;"><input type="text" class="form-control" id="KEYWORD" name="KEYWORD"></span>
						<div class="btn-group" style="float: right; margin: 10px 20px 0 20px;"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
			<table class="table table-bordered" style="width: 100%">
				<thead>
				 <tr>
					<th class="text-center" style="width: 8%">순번</th>
					<th class="text-center">구분</th>
					<th class="text-center" style="width: 20%">수신자</th>
					<th class="text-center" style="width: 15%">발신일자</th>
					<th class="text-center" style="width: 15%">수신일자</th>
					<th class="text-center" style="width: 12%">보완내역조회</th>
				 </tr>
				</thead>
				<tbody>
				<c:choose>
				<c:when test="${empty ds_list}">
					<tr>
						<td colspan="6" class="text-center">발신된 보완안내 메일 정보가 없습니다.</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<tr>
						<td class="text-center"><c:out value="${info.RNUM }"/></td>
						<td class="text-center"><c:out value="${info.GUBUN }"/></td>
						<td class="text-center"><c:out value="${info.RECV_NAME }"/><br/>(<c:out value="${info.RECV_MAIL_ADDR }"/>)</td>
						<td class="text-center"><c:out value="${info.SEND_DTTM }"/></td>
						<td class="text-center">
						<c:if test="${info.READ_DTTM == null }">읽지않음</c:if>
						<c:if test="${info.READ_DTTM != null }"><c:out value="${info.READ_DTTM }"/></c:if>
						</td>
						<td class="text-center"><a href="#" onclick="fncGoDetail('<c:out value="${info.SUPL_ID}" />');return false;"><b>[조회]</b></a></td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		</div>
