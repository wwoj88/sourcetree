<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//첨부파일 다운로드
	function fncDown(BORD_CD, BORD_SEQN) {
		location.href = '<c:url value="/console/legal/fdcrAd08Download1.page"/>?BORD_CD='
				+ BORD_CD + '&BORD_SEQN=' + BORD_SEQN;
	}

	//회원정보 팝업
	function popupOpen(USER_IDNT) {
		var popUrl = '<c:url value="/console/legal/fdcrAd10Pop1.page"/>?USER_IDNT='
				+ USER_IDNT;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=800, height=550, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	//보완 팝업
	function popupOpen2(BORD_CD, BORD_SEQN) {
		var popUrl = '<c:url value="/console/legal/fdcrAd10Pop2.page"/>?BORD_CD='
			+ BORD_CD + '&BORD_SEQN=' + BORD_SEQN;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}

	//공고 승인 상태 수정
	function fncGoUpdate(BORD_CD, BORD_SEQN) {
		var f = document.anucStatCdForm;
		if (confirm("상태를 저장하시겠습니까?")) {
			f.action = '<c:url value="/console/legal/fdcrAd10Update2.page"/>';
			f.submit();
		}
	}

	//게시글 삭제
	function fncGoDelete(BORD_CD, BORD_SEQN) {
		if (confirm("삭제하시겠습니까?")) {
			location.href = '<c:url value="/console/legal/fdcrAd10Delete1.page"/>?BORD_CD='
					+ BORD_CD + '&BORD_SEQN=' + BORD_SEQN;
		}
	}

	function fncGoList(OPEN_YN, ANUC_STAT_CD) {
		if (OPEN_YN == 'Y' && ANUC_STAT_CD == '3') {
			location.href = '<c:url value="/console/legal/fdcrAd10List1.page"/>';
		} else if (OPEN_YN == 'N' && ANUC_STAT_CD == '4') {
			location.href = '<c:url value="/console/legal/fdcrAd10List3.page"/>';
		} else {
			location.href = '<c:url value="/console/legal/fdcrAd10List2.page"/>';
		}
	}
	
	//메일발신함 조회
	function fncGoSendList(SUPL_ID) {
		var popUrl = '<c:url value="/console/legal/fdcrAd10SuplSendList1.page"/>?SUPL_ID='
			+ SUPL_ID;
		; //팝업창에 출력될 페이지 URL
		var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex"
		value="<c:out value="${param.pageIndex}"/>" />
</form>
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered text-center">
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<th style="width: 20%">접수번호</th>
								<td colspan="3"><c:out value="${info.RECEIPT_NO}" /></td>
							</tr>

							<tr>
								<th>제목</th>
								<td colspan="3"><c:out value="${info.TITE}" /></td>
							</tr>

							<tr>
								<th style="width: 20%">공고일자</th>
								<td style="width: 30%"><c:if
										test="${info.OPEN_DTTM == null}">미공고</c:if> <c:if
										test="${info.OPEN_DTTM != null}">${info.OPEN_DTTM}</c:if></td>
								<th style="width: 20%">등록자</th>
								<td style="width: 30%"><a href="#"
									onclick="popupOpen('<c:out value="${info.RGST_IDNT}"/>');return false;"><u><c:out
												value="${info.USER_NAME}" /></u></a></td>
							</tr>

							<tr>
								<th>첨부서류</th>
								<td colspan="3" id="TdIdFile"><c:choose>
										<c:when test="${empty ds_file}">
											<span>등록된 첨부서류가 없습니다.</span>
										</c:when>
										<c:otherwise>
											<c:forEach var="attach" items="${ds_file}"
												varStatus="listStatus">
												<p class="file">
													<console:fn func="getFileIcon"
														value="${attach.REAL_FILE_NAME}" />
													<a href="#"
														onclick="fncDown('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;"><c:out
															value="${attach.FILE_NAME}" /></a> <span>(파일크기: <console:fn
															func="strFileSize" value="${attach.FILE_SIZE}" />)
													</span>
												</p>
											</c:forEach>
										</c:otherwise>
									</c:choose></td>
							</tr>

							<tr>
								<th>내용
								<c:if test="${info.ANUC_STAT_CD eq '2'}"><br/>
								<button type="submit" class="btn btn-primary" onclick="popupOpen2('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">보완</button>
								</c:if>
								</th>
								<td colspan="3">1. 저작물의 제호 : &nbsp;&nbsp;<c:out
										value="${info.ANUC_ITEM_1}" />
										<console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="10" value2="(*)"/>
								<br />
								<br /> 2. 저작자 및 저작재산권자의 성명: &nbsp;&nbsp;<c:out
										value="${info.ANUC_ITEM_2}" />
								<console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="20" value2="(*)"/>
								<br />
								<br /> 3. 저작물 이용의 내용: &nbsp;&nbsp;<c:out
										value="${info.ANUC_ITEM_3}" />
								<console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="30" value2="(*)"/>		
								<br />
								<br /> 4. 공탁금액: &nbsp;&nbsp;<c:out value="${info.ANUC_ITEM_4}" />
								<console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="40" value2="(*)"/>
								<br />
								<br /> 5. 공탁소의 명칭 및 소재지: &nbsp;&nbsp;<c:out
										value="${info.ANUC_ITEM_5}" />
								<console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="50" value2="(*)"/>
								<br />
								<br /> 6. 공탁근거: &nbsp;&nbsp;<c:out value="${info.ANUC_ITEM_6}" />
								<console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="60" value2="(*)"/>
								<br />
								<br /> 7. 저작물 이용자의 주소·성명<br /> &nbsp;&nbsp;&nbsp;&nbsp;- 주소 :
									&nbsp;&nbsp;<c:out value="${info.ANUC_ITEM_7}" />
								<console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="71" value2="(*)"/>	
								<br />&nbsp;&nbsp;&nbsp;&nbsp;- 성명 : &nbsp;&nbsp;<c:out value="${info.ANUC_ITEM_8}" />
								<console:fn func="findRow" list="${suplList}" value="SUPL_ITEM_CD" value1="72" value2="(*)"/>
								</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		<b>(*) 항목은 담당자에 의해 보완된 항목입니다.</b>
	</div>
	
	<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
		<div class="box-footer" style="display: inline-block; width: 100%">
			<%-- 
			<div class="btn-group">
				<button type="submit" class="btn btn-primary"
					onclick="fncGoDelete('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">삭제</button>
			</div>
			 --%>
			<div class="btn-group" style="float: right;">
				<button type="submit" class="btn btn-primary"
					onclick="fncGoList('<c:out value="${info.OPEN_YN}"/>','<c:out value="${info.ANUC_STAT_CD}"/>');">목록</button>
			</div>
			<%-- 
			<div class="btn-group" style="float: right;">
				<button type="submit" class="btn btn-primary"
					onclick="fncGoPrint('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">출력</button>
			</div>
			 --%>
			<div style="display: inline-block; float: right;">
			<form name="anucStatCdForm" id="anucStatCdForm" method="post" enctype="multipart/form-data">
			<input type="hidden" id="BORD_CD" name="BORD_CD" value="<c:out value="${param.BORD_CD}"/>" /> 
			<input type="hidden" id="BORD_SEQN" name="BORD_SEQN" value="<c:out value="${param.BORD_SEQN}"/>" />
				<c:if test="${info.ANUC_STAT_CD eq '1'}">
				<div class="btn-group" style="float: right; margin-right: 3px;">
					<button type="submit" class="btn btn-primary"
						onclick="fncGoUpdate('<c:out value="${info.BORD_CD}"/>','<c:out value="${info.BORD_SEQN}"/>');return false;">저장</button>
				</div>
				<div style="float: right;">
					<select id="ANUC_STAT_CD" name="ANUC_STAT_CD">
						<option value="1"
							<c:if test="${info.ANUC_STAT_CD eq '1'}">selected="selected"</c:if>>신청</option>
						<option value="2"
							<c:if test="${info.ANUC_STAT_CD eq '2'}">selected="selected"</c:if>>보완</option>
						<option value="3"
							<c:if test="${info.ANUC_STAT_CD eq '3'}">selected="selected"</c:if>>공고승인</option>
						<option value="4"
							<c:if test="${info.ANUC_STAT_CD eq '4'}">selected="selected"</c:if>>신청반려</option>
					</select>
				</div>
				</c:if>
			</form>
			</div>
		</div>
	</c:forEach>
	<br/><br/>
	<div class="box-body">
		<table class="table table-bordered text-center">
		<h4><b>▶ 공고정보 보완내역</b></h4>
			<thead>
				<tr>
					<th style="width: 5%" rowspan="2">번호</th>
					<th style="width: 60%" colspan="2">보완사항</th>
					<th style="width: 10%" rowspan="2">보완일자</th>
					<th style="width: 10%" rowspan="2">담당자</th>
					<th style="width: 15%" rowspan="2">메일수신확인</th>
				</tr>
				<tr>
					<th style="width: 25%">공고항목</th>
					<th style="width: 35%">보완 세부내역</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_supl_list}">
						<tr>
							<td colspan="6" class="text-center">등록된 공고정보 보완내역이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_supl_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.SUPL_SEQ}" /></td>
								<td class="text-center"><c:out value="${info.SUPL_ITEM}" /></td>
								<td class="text-center"><c:out value="${info.MODI_ITEM}" /></td>
								<td class="text-center"><c:out value="${info.RGST_DTTM}" /></td>
								<td class="text-center"><c:out value="${info.RGST_NAME}" /></td>
								<td class="text-center"><a href="#" onclick="fncGoSendList('<c:out value="${info.SUPL_ID}" />');return false;"><font color="light-blue;"><b>[보기]</b></font></a></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
		
</div>