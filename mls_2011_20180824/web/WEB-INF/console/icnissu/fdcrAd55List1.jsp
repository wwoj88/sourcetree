<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//검색
	function fncGoSearch() {
		var srch_icnStatCd = $('#srch_icnStatCd').val();
		var srch_icnxNumb = $('#srch_icnxNumb').val();
		var srch_title = $('#srch_title').val();
		var srch_genre = $('#srch_genre').val();
		var srch_rgstStartDate = $('#srch_rgstStartDate').val();
		var srch_rgstEndDate = $('#srch_rgstEndDate').val();
		var srch_rgstName = $('#srch_rgstName').val();
		
		var url = fncGetBoardParam('<c:url value="/console/icnissu/fdcrAd55List1.page"/>')+'&srch_icnStatCd='+srch_icnStatCd+'&srch_icnxNumb='+srch_icnxNumb+'&srch_title='+srch_title
		+'&srch_genre='+srch_genre+'&srch_rgstStartDate='+srch_rgstStartDate+'&srch_rgstEndDate='+srch_rgstEndDate+'&srch_rgstName='+srch_rgstName;
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(BORD_SEQN,MENU_SEQN){
		//var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/icnissu/fdcrAd55View1.page"/>')+'&BORD_SEQN='+BORD_SEQN+'&MENU_SEQN='+MENU_SEQN;
		location.href = url;
	}
	
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">개인저작물</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">ICN 발급상태</th>
									<td>
										<select style="width:100px" id="srch_icnStatCd" name="srch_icnStatCd">
											<option value="" selected="selected">-전체-</option>
											<option value="1">신청</option>
											<option value="2">접수</option>
											<option value="3">발급완료</option>
											<option value="4">발급거절</option>
										</select>
									</td>
									<th class="text-center search-th">ICN 번호</th>
									<td colspan="3">
										<input type="text" id="srch_icnxNumb" name="srch_icnxNumb"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">분야</th>
									<td>
										<select style="width:100px" id="srch_genre" name="srch_genre">
											<option value="" selected="selected">-전체-</option>
											<option value="1">어문</option>
											<option value="2">음악</option>
											<option value="3">연극</option>
											<option value="4">미술</option>
											<option value="5">건축</option>
											<option value="6">사진</option>
											<option value="7">영상</option>
											<option value="8">도형</option>
											<option value="9">컴퓨터프로그램</option>
											<option value="10">편집저작물</option>
											<option value="11">2차적저작물</option>
										</select>
									</td>
									<th class="text-center search-th">등록일</th>
									<td>
										<input type="text" id="srch_rgstStartDate" name="srch_rgstStartDate"/>
										<input type="text" id="srch_rgstEndDate" name="srch_rgstEndDate"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">저작물명</th>
									<td>
										<input type="text" id="srch_title" name="srch_title"/>
									</td>
									<th class="text-center search-th">저작권등록자</th>
									<td>
										<input type="text" id="srch_rgstName" name="srch_rgstName"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
			<!-- /.row -->
		</div>
		
		<!-- /.box-body -->
	</div>
</div>
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
	</div>
</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd55List1" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 5%" rowspan="2">순번</th>
					<th class="text-center" rowspan="2">저작물명</th>
					<th class="text-center" style="width: 10%" rowspan="2">분야</th>
					<th class="text-center" style="width: 15%" rowspan="2">저작권등록자</th>
					<th class="text-center" style="width: 25%" colspan="2">ICN 발급정보</th>
					<th class="text-center" style="width: 10%" rowspan="2">등록일</th>
				</tr>
				<tr>
					<th class="text-center">발급상태</th>
					<th class="text-center">ICN번호</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="7" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.ROW_NUM }"/></td>
								<td class="text-center"><c:out value="${info.TITLE }"/></td>
								<td class="text-center"><c:out value="${info.GENRE_NAME }"/></td>
								<td class="text-center"><c:out value="${info.RGST_NAME }"/></td>
								<td class="text-center"><c:out value="${info.ICN_STAT_NAME }"/></td>
								<td class="text-center"><c:out value="${info.ICNX_NUMB }"/></td>
								<td class="text-center"><c:out value="${info.RGST_DTTM }"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
</div>