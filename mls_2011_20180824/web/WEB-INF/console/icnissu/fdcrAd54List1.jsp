<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//검색
	function fncGoSearch() {
		var srch_icnxStatCode = $('#srch_icnxStatCode').val();
		var srch_icnxNumb = $('#srch_icnxNumb').val();
		var srch_title = $('#srch_title').val();
		var srch_AlbumTitle = $('#srch_bookTitle').val();
		var srch_lyricist = $('#srch_publisher').val();
		var srch_composer = $('#srch_firstEditionYear').val();
		var srch_licensor = $('#srch_licensor').val();
		var srch_translator = $('#srch_translator').val();
		
		var url = fncGetBoardParam('<c:url value="/console/icnissu/fdcrAd54List1.page"/>')+'&srch_icnxStatCode='+srch_icnxStatCode+'&srch_icnxNumb='+srch_icnxNumb+'&srch_title='+srch_title
		+'&srch_bookTitle='+srch_bookTitle+'&srch_publisher='+srch_publisher+'&srch_firstEditionYear='+srch_firstEditionYear+'&srch_licensor='+srch_licensor+'&srch_translator='+srch_translator;
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(BORD_SEQN,MENU_SEQN){
		//var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/icnissu/fdcrAd53View1.page"/>')+'&BORD_SEQN='+BORD_SEQN+'&MENU_SEQN='+MENU_SEQN;
		location.href = url;
	}
	
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">도서저작물 목록</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">CLMS전송상태</th>
									<td>
										<select style="width:100px" id="srch_icnxStatCode" name="srch_icnxStatCode">
											<option value="" selected="selected">-전체-</option>
											<option value="2">전송대기</option>
											<option value="4">전송완료</option>
											<option value="99">전송오류</option>
										</select>
									</td>
									<th class="text-center search-th">복전협관리번호</th>
									<td colspan="3">
										<input type="text" id="srch_icnxNumb" name="srch_icnxNumb"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">저작물명</th>
									<td>
										<input type="text" id="srch_title" name="srch_title"/>
									</td>
									<th class="text-center search-th">도서명</th>
									<td colspan="3">
										<input type="text" id="srch_bookTitle" name="srch_bookTitle"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">출판사</th>
									<td>
										<input type="text" id="srch_publisher" name="srch_publisher"/>
									</td>
									<th class="text-center search-th">발행년도</th>
									<td>
										<input type="text" id="srch_firstEditionYear" name="srch_firstEditionYear"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">저자</th>
									<td>
										<input type="text" id="srch_licensor" name="srch_licensor"/>
									</td>
									<th class="text-center search-th">역자</th>
									<td>
										<input type="text" id="srch_translator" name="srch_translator"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
			<!-- /.row -->
				</div>
		
		<!-- /.box-body -->
			</div>
	</div>
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
	</div>
</div>
</div>


<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd54List1" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 5%" rowspan="2"><input type="checkbox" name="CHK" id="CHK" value=""/></th>
					<th class="text-center" rowspan="2">저작물명</th>
					<th class="text-center" style="width: 15%" rowspan="2">도서명</th>
					<th class="text-center" style="width: 7%" rowspan="2">출판사</th>
					<th class="text-center" style="width: 7%" rowspan="2">발행년도</th>
					<th class="text-center" style="width: 7%" rowspan="2">자료유형</th>
					<th class="text-center" style="width: 10%">저자</th>
					<th class="text-center" style="width: 10%">역자</th>
					<th class="text-center" style="width: 10%" rowspan="2">복전협 관리번호</th>
					<th class="text-center" style="width: 10%" rowspan="2">CLMS 전송여부</th>
				</tr>
				<tr>
					<th class="text-center" style="width: 10%">통합저작권자ID</th>
					<th class="text-center" style="width: 10%">통합저작권자ID</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="8" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><input type="checkbox" name="CHK" id="CHK" value="<c:out value="${info.CHK}" />"/></td>
								<td class="text-center"><c:out value="${info.TITLE }"/></td>
								<td class="text-center"><c:out value="${info.BOOK_TITLE }"/></td>
								<td class="text-center"><c:out value="${info.PUBLISHER }"/></td>
								<td class="text-center"><c:out value="${info.FIRST_EDITION_YEAR }"/></td>
								<td class="text-center"><c:out value="${info.PUBLISH_TYPE }"/></td>
								<td class="text-center"><c:out value="${info.LICENSOR }"/></td>
								<td class="text-center"><c:out value="${info.TRANSLATOR }"/></td>
								<td class="text-center"><c:out value="${info.ICNX_NUMB }"/></td>
								<td class="text-center"><c:out value="${info.ICN_STAT }"/></td>
							</tr>
							<tr>
								<td class="text-center"><c:out value="${info.LICENSOR_ID }"/></td>
								<td class="text-center"><c:out value="${info.TRANSLATOR_ID }"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">CLMS 전송</button></div>
	</div>
</div>