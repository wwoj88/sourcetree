<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//검색
	function fncGoSearch() {
		var srch_icnxStatCode = $('#srch_icnxStatCode').val();
		var srch_icnxNumb = $('#srch_icnxNumb').val();
		var srch_title = $('#srch_title').val();
		var srch_AlbumTitle = $('#srch_AlbumTitle').val();
		var srch_lyricist = $('#srch_lyricist').val();
		var srch_composer = $('#srch_composer').val();
		var srch_singer = $('#srch_singer').val();
		
		var url = fncGetBoardParam('<c:url value="/console/icnissu/fdcrAd53List1.page"/>')+'&srch_icnxStatCode='+srch_icnxStatCode+'&srch_icnxNumb='+srch_icnxNumb+'&srch_title='+srch_title
		+'&srch_AlbumTitle='+srch_AlbumTitle+'&srch_lyricist='+srch_lyricist+'&srch_composer='+srch_composer+'&srch_singer='+srch_singer;
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(BORD_SEQN,MENU_SEQN){
		//var datas = [];
		//datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/icnissu/fdcrAd53View1.page"/>')+'&BORD_SEQN='+BORD_SEQN+'&MENU_SEQN='+MENU_SEQN;
		location.href = url;
	}
	
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">음악저작물 목록</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">ICN 발급상태</th>
									<td>
										<select style="width:100px" id="srch_icnxStatCode" name="srch_icnxStatCode">
											<option value="" selected="selected">-전체-</option>
											<option value="2">발급대기</option>
											<option value="3">발급요청</option>
											<option value="4">발급완료</option>
											<option value="99">발급오류</option>
										</select>
									</td>
									<th class="text-center search-th">ICN 번호</th>
									<td colspan="3">
										<input type="text" id="srch_icnxNumb" name="srch_icnxNumb"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">곡명</th>
									<td colspan="3">
										<input type="text" id="srch_title" name="srch_title"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">앨범명</th>
									<td>
										<input type="text" id="srch_AlbumTitle" name="srch_AlbumTitle"/>
									</td>
									<th class="text-center search-th">작사</th>
									<td>
										<input type="text" id="srch_lyricist" name="srch_lyricist"/>
									</td>
								</tr>
								<tr>
									<th class="text-center search-th">작곡</th>
									<td>
										<input type="text" id="srch_composer" name="srch_composer"/>
									</td>
									<th class="text-center search-th">가창</th>
									<td>
										<input type="text" id="srch_singer" name="srch_composer"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
			<!-- /.row -->
		</div>
		
		<!-- /.box-body -->
	</div>
</div>
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
	</div>
</div>
</div>

<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd53List1" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 5%" rowspan="2"><input type="checkbox" name="CHK" id="CHK" value=""/></th>
					<th class="text-center" rowspan="2">곡명</th>
					<th class="text-center" style="width: 15%" rowspan="2">앨범명</th>
					<th class="text-center" style="width: 7%" rowspan="2">발매일</th>
					<th class="text-center" style="width: 7%" rowspan="2">작사</th>
					<th class="text-center" style="width: 7%" rowspan="2">작곡</th>
					<th class="text-center" style="width: 7%" rowspan="2">가창</th>
					<th class="text-center" style="width: 15%" colspan="2">ICN 발급정보</th>
				</tr>
				<tr>
					<th class="text-center">발급상태</th>
					<th class="text-center">ICN번호</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="8" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><input type="checkbox" name="CHK" id="CHK" value="<c:out value="${info.CHK}" />"/></td>
								<td class="text-center"><c:out value="${info.TITLE }"/></td>
								<td class="text-center"><c:out value="${info.ALBUM_TITLE }"/></td>
								<td class="text-center"><c:out value="${info.ALBUM_ISSUE_DATE }"/></td>
								<td class="text-center"><c:out value="${info.LYRICS }"/></td>
								<td class="text-center"><c:out value="${info.COMPOSER }"/></td>
								<td class="text-center"><c:out value="${info.SINGER }"/></td>
								<td></td>
							</tr>
							<tr>
								<td class="text-center"><c:out value="${info.ICN_STAT }"/></td>
								<td class="text-center"><c:out value="${info.ICNX_NUMB }"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<!-- /.box-body -->
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">ICN 발급</button></div>
	</div>
</div>