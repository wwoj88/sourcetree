<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<table id="fdcrAdA7List1" class="table table-bordered table-hover text-center table-list">
<colgroup>
	<col width="6%"/> 
	<col width="*"/>
	<col width="24%"/>
	<col width="16%"/>
	<col width="16%"/>
	<col width="12%"/>
</colgroup>
<thead>
	<tr>
		<th>순번</th>
		<th>구분</th>
		<th>수신자</th>
		<th>발신일자</th>
		<th>수신일자</th>
		<th>보완내역 조회</th>
	</tr>
</thead>
<tbody>
	<c:choose>
	<c:when test="${empty ds_list}">
	<tr>
		<td colspan="6"></td>
	</tr>
	</c:when>
	<c:otherwise>
	<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
		<tr>
			<td><c:out value="${info.RNUM}"/></td>
			<td><c:out value="${info.GUBUN}" /></td>
			<td><c:out value="${info.RECV_NAME}(${info.RECV_MAIL_ADDR})" /></td>
			<td><c:out value="${info.SEND_DTTM}"/></td>
			<td>${empty info.READ_DTTM ? '읽지않음' : info.READ_DTTM}</td>
			<td><a href="#" onclick="javascript:fncPopup('${info.SUPL_ID}','${info.MSG_STOR_CD}');">조회</a></td>
		</tr>
	</c:forEach>
	</c:otherwise>
	</c:choose>
	</tbody>
</table>