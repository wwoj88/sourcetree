<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<form name="regForm" id="regForm" method="post">
	<input type="hidden" name="FORM_ID" value="<c:out value="${form.FORM_ID}"/>"/>
	<input type="hidden" name="THREADED" value="<c:out value="${form.THREADED}"/>"/>
	<input type="hidden" name="PARENT" value="<c:out value="${form.PARENT}"/>"/>
	<input type="hidden" name="FORM_DEPTH" value="<c:out value="${form.FORM_DEPTH}"/>"/>
	<input type="hidden" name="FORM_ORDER" value="<c:out value="${form.FORM_ORDER}"/>"/>
	
	<div style="text-align:right;height:30px">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncAddMenu('<c:out value="${form.FORM_ID}"/>');return false;">추가</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncUpdate('<c:out value="${form.FORM_ID}"/>');return false;">수정</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncDelete('<c:out value="${form.FORM_ID}"/>');return false;">삭제</button></div>
	</div>
	<table class="table table-bordered">
		<caption></caption>
		<colgroup>
			<col width="15%">
			<col width="85%">
		</colgroup>
		<tbody>
			<tr>
				<th>메일폼 이름</th>
				<td><input class="form-control" type="text" name="FORM_TITLE" id="FORM_TITLE" value="<c:out value="${form.FORM_TITLE }"/>"/></td>
			</tr>
			<tr>
				<th>메일폼 설명</th>
				<td><input class="form-control" type="text" name="FORM_DESC" id="FORM_DESC" value="<c:out value="${form.FORM_DESC }"/>"/></td>
			</tr>
			<tr>
				<th>메일폼 내용</th>
				<td>
					<textarea class="form-control" id="FORM_CONTENT" name="FORM_CONTENT" style="height: 300px"><c:out value="${form.FORM_CONTENT }"/></textarea>
				</td>
			</tr>
		</tbody>
	</table>
</form>
	

