<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:forEach items="${ds_msg_Id}" var="info">
<input type="hidden" id="MSG_ID" name="MSG_ID" value="${info.MSG_ID}"/>
</c:forEach>
<input type="hidden" id="MAILING_MGNT_CD" name="MAILING_MGNT_CD" value="${msgStorCd eq 11 ? '1': (msgStorCd eq 12 ? '2': '')}"/>
<table id="fdcrAd8List1" class="table table-bordered table-hover text-center table-list ">
<colgroup>
	<col width="11%"/> 
	<col width="12%"/>
	<col width="*"/>
	<col width="14%"/>
	<col width="14%"/>
	<col width="14%"/>
</colgroup>
<thead>
	<tr>
		<th colspan="2">설정</th>
		<th colspan="4">발신내역</th>
	</tr>	
	<tr>
		<th>사용여부</th>
		<th>년월</th>
		<th>제목</th>
		<th>완료여부</th>
		<th>완료일자</th>
		<th>수신확인</th>
	</tr>
</thead>
<tbody>
<c:choose>
	<c:when test="${empty ds_list}">
		<tr>
			<td colspan="6"></td>
		</tr>
	</c:when>
	<c:otherwise>
	<%-- ${not empty info.STAT_CD ? 'checked' : '' } --%>
	<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
		<tr>
			<td><input type="checkbox" name="CHECKED_VALUE"${not empty info.STAT_CD ? 'checked' : '' } value="${info.USE_YSNO}_${info.YEAR_MONTH}_${info.STAT_CD}_${info.START_DTTM}_${info.END_DTTM}"></td>
			<td><c:out value="${info.YEAR_MONTH}"/></td>
			<td><c:out value="${info.TITLE}" /></td>
			<td><c:out value="${empty info.STAT_CD ? '' : info.STAT_CD eq 0 ? '대기' : '완료' }" /></td>
			<td><c:out value="${info.START_DTTM}" /></td>
			<td><c:if test="${not empty info.STAT_CD }"><a href="#" onclick="javascript:fncPopup2();return false;"><c:out value="[수신확인]" /></a></c:if></td>
		</tr>
	</c:forEach>
	</c:otherwise>
</c:choose>
	</tbody>
</table>