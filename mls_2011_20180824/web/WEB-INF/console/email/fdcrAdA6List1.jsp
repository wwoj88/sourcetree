<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<style>
.table > tbody > tr > th {
	text-align: center;
}
</style>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="/console/plugins/multifile-master/jquery.MultiFile.js" type="text/javascript" ></script>

<script>
	
	$(function() {
		fncView('select2','${ds_list[0].MSG_ID}');
	});
	
	function fncView(type,MSG_ID){
		$("#view").empty();
		$("MSG_ID").val(MSG_ID);
		$("GUBUN").val(type);
		fncLoad('#view', '<c:url value="/console/email/fdcrAdA6View1.page"/>', {
			"MSG_ID" : MSG_ID, "GUBUN" :type
		}, function(data) {
			CKEDITOR.replace('compose-textarea'); 
		});
	}
	
</script>
<form name="form" id="form" method="post" enctype="multipart/form-data">
	<input type="hidden" id="GUBUN" name="GUBUN"/> 
	<input type="hidden" id="MSG_ID" name="MSG_ID"/>
	<div class="box" style="margin-top: 30px;">
		<!-- /.box-header -->
		<div class="box-body" style="height: 300px; overflow-y:auto;">
			<table class="table table-bordered table-hover text-center table-list">
				<colgroup>
					<col width="20%"/>
					<col width="*"/>
					<col width="20%"/>
				</colgroup>
				<thead>
					<tr>
						<th>받는사람</th>
						<th>제목</th>
						<th>발신일자</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${ds_list}" var="info">
					<tr>
						<td><a href="#" onclick="javascript:fncView('select1','${info.MSG_ID}');"><c:out value="${info.EXPR_ADDR}"/></a></td>
						<td><a href="#" onclick="javascript:fncView('select2','${info.MSG_ID}');"><c:out value="${info.TITLE}"/></a></td>
						<td><c:out value="${info.START_DTTM_TRNS}"/> </td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<p style="margin-left: 10px; margin-top: 10px;"><strong>* 받는 사람 주소를 누르면 메일 송,수신 내역이 조회됩니다.</strong></p>
		<p style="margin-left: 10px;"><strong>* 제목을 누르면 해당 메일의 내용이 조회됩니다.</strong></p>
		<div class="box-body" id="view" style="height: 600px; overflow-y:auto; ">
		
		</div>
	</div>
</form>


