<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<style>
.table > tbody > tr > th {
	text-align: center;
}
</style>
<script>
	//삭제
	function fncDelete(){
		var isChecked = false;
		$("input[name=MSG_ID]:checked").each(function() {
			var eventId = $(this).val();
			isChecked = true;
		});
		
		if(!isChecked){
			alert("삭제할 메세지를 선택해주세요.");
			return false;
		}
		
		var f = document.form;
		f.action = "/console/email/fdcrAdA5Delete1.page";
		f.submit();
	}
	
	function fncPopup(MSG_ID) {
		var gubun= MSG_ID != 0 ? "edit" : "";
		var popUrl = '<c:url value="/console/email/fdcrAdA4Pop2.page"/>?&TYPE=msgList&MSG_ID='+MSG_ID+'&GUBUN='+gubun;
		var popOption = "width=950, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}

</script>
<form id="form" name="form" method="post" action="#">
<div class="box box-default">
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncDelete();return false;">삭제</button></div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<colgroup>
				<col width="6%" />
				<col width="*" />
				<col width="16%" />
			</colgroup>
			<thead>
				<tr>
					<th>선택</th>
					<th>제목</th>
					<th>저장일</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="3">저장된&#32;메세지&#32;내역이&#32;없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><input type="checkbox" name="MSG_ID" id="MSG_ID" value="${info.MSG_ID}"/></td>
								<td><a href="#" onclick="javascript:fncPopup('${info.MSG_ID}');return false;"><c:out value="${info.TITLE}"/></a></td>
								<td><c:out value="${info.RGST_DTTM}"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix text-center">
	  <%-- <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul> --%>
	</div>
</div>
</form>