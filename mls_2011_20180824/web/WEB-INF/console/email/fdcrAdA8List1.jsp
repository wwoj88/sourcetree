<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<style>
.table > tbody > tr > th {
	text-align: center;
}
</style>
<script>
	$(function(){
		fncLoadList('11','위탁관리저작물 보고 안내');
		
		$("select[name='YEAR']").on("change",function(){
			fncLoadList($("#MSG_STOR_CD").val(),$("#title").html())
		})
	})
	
	//팝업
	function fncPopup() {
		var msg_id = $("#MSG_ID").val();
		var msg_stor_cd = $("#MSG_STOR_CD").val();
		var gubun= msg_id != 0 ? "edit" : "";
		var popUrl = '<c:url value="/console/email/fdcrAdA4Pop2.page"/>?MSG_ID='+msg_id+'&GUBUN='+gubun+'&TYPE=monthList&MSG_STOR_CD='+msg_stor_cd;
		var popOption = "width=920, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function fncPopup2() {
		var msg_id = $("#MSG_ID").val();
		var popUrl = '<c:url value="/console/email/fdcrAdA8Pop1.page"/>?MSG_ID='+msg_id;
		var popOption = "width=920, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}

	//등록
	function fncSave() {

		if($("#MSG_ID").val() == 0){
			alert("먼저 [메일관리] 에서 자동메일 내용을 등록해주세요.");
			return false;
		}
		
		var f = document.form;
		//var CHECKED_VALUE = $("#CHECKED_VALUE").val();
		//alert(CHECKED_VALUE);
		var MAILING_MGNT_CD = $("#MAILING_MGNT_CD").val();
		
		var $obj = $("input[name='CHECKED_VALUE']");
	    var checkCount = $obj.size();
	    //alert($("input:checkbox[name='CHECKED_VALUE']:checked").length+"개");
	    //var checkLen = $("input:checkbox[name='CHECKED_VALUE']:checked").length;
	    for(var i=1; i < checkCount; i++ ){
			if( $obj.eq(i-1).is(":checked")==true ){
				f.action = '<c:url value="/console/email/fdcrAdA8Insert1.page"/>';
				f.submit();
			}
	    }
	}

	function fncLoadList(value,title){
		
		var year = $("select[name='YEAR'] option:selected").val();
		$("#MSG_STOR_CD").val(value);
		$("#title").html(title);
		
		fncLoad('#fdcrAd8List1', '<c:url value="/console/email/fdcrAdA8List1Sub1.page"/>', {
			"MSG_STOR_CD" : value, "YEAR":year
		}, function(data) {
		});
		
		$("[id^=list]").each(function(){
			$(this).removeClass("active");
		});
		
		$('#list'+value).addClass("active");
		
	}
	
</script>

<form id="form" name="form" method="post" action="#">
<input type="hidden" id="MSG_STOR_CD" name="MSG_STOR_CD" value='<c:out value="${commandMap.MSG_STOR_CD}"/>'/>
<%-- <input type="hidden" id="MAILING_MGNT_CD" name="MAILING_MGNT_CD" value="${commandMap.MSG_STOR_CD eq 11 ? '1': (commandMap.MSG_STOR_CD eq 12 ? '2': '')}"/> --%>
<%-- <input type="hidden" id="MAILING_MGNT_CD" name="MAILING_MGNT_CD" value="${msgStorCd}"/> --%>
<div class="row">
	<div class="col-md-3">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">목록</h3>
			</div>
			<div class="box-body no-padding" style="height: 670px;">
				<ul class="nav nav-pills nav-stacked">
					<li ${commandMap.MSG_STOR_CD eq 11 ? 'class="active"': ''} id="list11"><a href="#" onclick="javascript:fncLoadList('11','위탁관리저작물 보고 안내');">위탁관리저작물 보고 안내</a></li>
			        <li ${commandMap.MSG_STOR_CD eq 12 ? 'class="active"': ''} id="list12"><a href="#" onclick="javascript:fncLoadList('12','미분배보상금대상 저작물보고 안내');">미분배보상금대상 저작물보고 안내</a></li>
			    </ul>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
	<!-- /.col -->
	<div class="col-md-9">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title" id="title">${commandMap.MSG_STOR_CD eq 11 ? '위탁관리저작물 보고 안내': (commandMap.MSG_STOR_CD eq 12 ? '미분배보상금대상 저작물보고 안내': '')}</h3>
			</div>
			<div class="mailbox-controls">
	        <!-- Check all button -->
	        	<div class="form-group col-xs-2">
	        		<console:fn func="getYearSelectBox" value="${param.ds_year}" value1="YEAR" value3="form-control"/>
	        	</div>
	            <div class="pull-right">
	            	<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncSave();return false;">저장</button></div>
            	</div>
           	</div>
           	<!-- /.box-header -->
	        <div class="box-body" id="fdcrAd8List1">
	        	
			</div>
			<div class="box-footer ">
				<p style="color: red;">(*)월별 자동메일은 매월 1일 오전 1시에 발송됩니다.</p>
				<button type="submit" onclick="javascript:fncPopup(); return false;" class="btn btn-primary pull-right">메일관리</button>
			</div>
		</div>
		<!-- /. box -->
	</div>
	<!-- /.col -->
</div>
</form>