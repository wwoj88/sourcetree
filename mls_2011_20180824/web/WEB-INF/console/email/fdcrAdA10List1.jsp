<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

<!-- CK Editor --> 
<script src="/console/plugins/ckeditor/ckeditor.js"></script>

<script type="text/javascript">
	var editor = null;

	$(function() {
		init();
	});
	
	
	
	
	/* tree setting */
	function fncTreeInit() {
		$('#tree1').jstree({
			'core' : {
				'data' : {
					"url" : "<c:url value='/console/email/fdcrAdA10List1Sub1.page'/>",
					"dataType" : "json"
				},
				"themes" :
				{
					"theme" : "classic",
					"icon" : false
				}
			}
		}).bind("select_node.jstree", function(event, data) {
			var id = data.node.id;
			id = id.substring(id.indexOf("_") + 1);
			fncLoad('#menuFormDiv', '<c:url value="/console/email/fdcrAdA10View1.page"/>', {
				"FORM_ID" : id
			}, function(data) {
				editor = CKEDITOR.replace('FORM_CONTENT');
			});
			
			
			
		});
	}

	/* init */
	function init(formId) {
		if (formId) {
			fncLoad('#menuFormDiv', '<c:url value="/console/email/fdcrAdA10View1.page"/>', {
				"FORM_ID" : formId
			}, function(data) {
				editor = CKEDITOR.replace('FORM_CONTENT'); 
			});
		} else {
			fncLoad('#menuFormDiv', '<c:url value="/console/email/fdcrAdA10View1.page"/>',
					{}, function(data) {
						fncTreeInit();
						fncLoad('#menuFormDiv', '<c:url value="/console/email/fdcrAdA10View1.page"/>', {
							"FORM_ID" : '-1'
						}, function(data) {
							editor = CKEDITOR.replace('FORM_CONTENT'); 
						});
					});
		}
		
		

	}

	/* 메일폼추가 페이지 */
	function fncAddMenu(FORM_ID) {
		fncLoad('#menuFormDiv',
				'<c:url value="/console/email/fdcrAdA10WriteForm1.page"/>', {"FORM_ID":FORM_ID},
				function(data) {
					editor = CKEDITOR.replace('FORM_CONTENT'); 
				});
		
		
	}
	
	/* 메일폼 추가 */
	function fncWrite() {
		for (instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
		
		var formData = $("form[name=regForm]").serialize().replace(/%/g, '%25');
		
		fncPost('<c:url value="/console/email/fdcrAdA10Write1.page"/>',formData,function(data){
			if(data == 'N'){
				alert("실패하였습니다.");
			}else{
				$('#tree1').jstree('refresh');
				init(data);
			}
		});
		
		
	}

	/* 메일폼 수정 */
	function fncUpdate(FORM_ID) {
		if(FORM_ID){
			
			for (instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
			
			var formData = $("form[name=regForm]").serialize().replace(/%/g, '%25');
			
			fncPost('<c:url value="/console/email/fdcrAdA10Update1.page"/>',formData,function(data){
				if(data == 'N'){
					alert("실패하였습니다.");
				}else{
					alert("메일폼이 수정되었습니다.");
					$('#tree1').jstree('refresh');
					init(data);
				}
			});
		}
		
		
	}
	
	/* 메일폼 삭제  */
	function fncDelete(FORM_ID){
		if(FORM_ID){
			fncPost('<c:url value="/console/email/fdcrAdA10Delete1.page"/>',{"FORM_ID":FORM_ID},function(data){
				if(data == 'Y'){
					alert("메일폼이 삭제되었습니다.");
					$('#tree1').jstree('refresh');
					init();
				}else{
					alert("실패하였습니다.");
				}
			});
		}
	}
</script>

<div class="box" style="margin-top: 30px;">
	<div class="box-body">
		<div id="tree"
			style="width: 340px; height: 590px; border:1px solid #ddd;float: left; margin-bottom: 10px; padding:10px; overflow-y:scroll;">
			<div id="omTreeDiv" >
				<div id="tree1"></div>
			</div>
		</div>
		<div class="menu_right" id="menuFormDiv" style="float:left;width:900px;height: 590px; border:1px solid #ddd;padding:10px;margin-left:20px;">
		</div>
	</div>
</div>


