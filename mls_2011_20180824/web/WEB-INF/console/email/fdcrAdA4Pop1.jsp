<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<!-- tree -->
<link rel="stylesheet" href='/console/custom/js/jstree/themes/default/style.css'/>
<script src="/console/custom/js/jstree/jstree.js"></script>
<script src="/console/custom/js/jstree/jstree.checkbox.js"></script>
<style>
.lead {font-size: 15px;}
</style>
<script>
	$(function() {
		fncTreeInit();
		
		$(document).ready(function() {
			
			$('#mailAdd').on("click", function(){
				//$('#treeView').find("input[checkbox]")
				
				$('#treeView').find("input[type=checkbox]:checked").each(function() {
					
					$(this).prop("checked", false);
					
					var _element = $(this).parent().parent().clone();
					//var _element = $(this).parent().parent();
					
					$('#emailList').append(_element);
					
				});
				
			});
			
			$('#mailDel').on("click", function(){
				
				$('#emailList').find("input[type=checkbox]:checked").each(function() {
					$(this).parent().parent().remove();					
				});
				
			});
			
		});
		
	});
	
	/* tree setting */
	function fncTreeInit() {
		$('#tree1').jstree({
			'core' : {
				'data' : {
					"url" : "<c:url value='/console/email/fdcrAdA4Sub1.page'/>",
					"dataType" : "json"
				},
				"themes" :
				{
					"theme" : "classic",
					"icon" : false
				}
			}

		}).bind("select_node.jstree", function(event, data) {
			var id = data.node.id;
			id = id.substring(id.indexOf("_") + 1);
			
			var recv_group_id = null;
			var recv_rejc_yn = null;
			
			if(id == '10' || id == '20'){
				recv_group_id = id.substring(0, 1);
			} else if(id == '11' || id == '21'){
				recv_group_id = id.substring(0, 1);
				recv_rejc_yn = 'N';
			}
			
			fncLoad('#treeView', '<c:url value="/console/email/fdcrAdA4Pop1View1.page"/>', {
				"GROUP_ID" : id, "RECV_GROUP_ID" : recv_group_id, "RECV_REJC_YN" : recv_rejc_yn
			}, function(data) {
			});
		});
		
	}
	
	function selectEmail(){
		$('#emailList').find("input[type=checkbox]").each(function() {
			opener.fncSelectEmail($(this).val());					
		});
		
		closePop();
	}
	
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            주소록
          </h2>
        </div>
        <!-- /.col -->
      </div>

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-4">
          <p class="lead">* 그룹정보</p>
          <div id="tree1" class="text-muted well well-sm no-shadow" style="font-size:12px; height:350px;margin-top: -10px; background-color: white; overflow: auto;">
          </div>
          <p class="lead"><strong>* 코콤스 등록 업체 기준으로 해당 시스템에 등록된 정보가 있는 경우 담당자 email로 함</strong></p>
        </div>
        <div class="col-xs-4">
          <p class="lead">* 그룹별 목록 조회</p>
          <div id="treeView" class="text-muted well well-sm no-shadow" style="height:350px; margin-top: -10px; background-color: white;">
	            <div class="text-muted well well-sm no-shadow" style="font-size:12px;height:50px; margin-top: 5px; background-color: white;">
	          		조회된 데이타가 없습니다.
	            </div>
	          	<div class="text-muted well well-sm no-shadow" style="font-size:12px;height:130px; margin-top: -10px; background-color: white; overflow: auto;">
	            	조회된 데이타가 없습니다.
	          	</div>
	          	<p style="margin-top: -10px;">수신거부</p>
	          	<div class="text-muted well well-sm no-shadow" style="font-size:12px;height:100px; margin-top: -10px; background-color: white; overflow: auto;">
	            	조회된 데이타가 없습니다.
	          	</div>
          </div>
          <p class="lead"><strong>(*)로 표시된 정보는 해당 시스템에 담당자로 등록된 기관임</strong></p>
        </div>
        
        <div class="text-muted well-sm no-shadow" style="display: table;height:350px;padding: 0px;padding-left: 15px;background-color: white;float: left;">
        	<div style="display: table-cell; vertical-align: middle;">
        		<div class="row" style="margin-bottom: 20px;"><span id="mailAdd" class="glyphicon glyphicon-plus"></span></div>
        		<div class="row"><span id="mailDel" class="glyphicon glyphicon-minus"></span></div>
        	</div>
        </div>
        
        <div style="width: 31%;float: left;padding:0px 15px;">
          <p class="lead">*받는 사람</p>
          <div id="emailList" class="text-muted well well-sm no-shadow" style="font-size:12px; height:350px;margin-top: -10px; background-color: white; overflow: auto;">
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary" onclick="closePop();">닫기</button>
          <button type="button" class="btn btn-primary pull-right" onclick="selectEmail();">확인</button>
        </div>
      </div>
      </section>