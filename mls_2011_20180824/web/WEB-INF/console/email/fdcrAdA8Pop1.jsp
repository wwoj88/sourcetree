<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
<form name="updateForm" id="updateForm" method="post">
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶ 수신확인</b></h4>
			<table class="table table-bordered" style="width: 100%">
				<colgroup>
					<col width="22%">
					<col width="*">
					<col width="10%">
					<col width="10%">
				</colgroup>
				<thead>
					<tr>
						<th class="text-center">받는사람</th>
						<th class="text-center">제목</th>
						<th class="text-center">발신일자</th>
						<th class="text-center">수신일자</th>
					</tr>
				</thead>
				<tbody>
				<c:choose>
				<c:when test="${empty ds_view}">
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_view}" varStatus="listStatus">
					<tr>
						<td class="text-center">${info.EXPR_ADDR}</td>
						<td class="text-center">${info.TITLE}</td>
						<td class="text-center">${info.SEND_DTTM}</td>
						<td class="text-center">${empty info.READ_DTTM ? '읽지않음' : info.READ_DTTM }</td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		</div>
		<div class="box-footer ">
			<button type="submit" class="btn btn-primary pull-right" onclick="closePop();">닫기</button>
		</div>
	    <!-- /. box -->

</form>
