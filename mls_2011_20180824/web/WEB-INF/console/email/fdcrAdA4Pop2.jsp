<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<style>
.table > tbody > tr > th {
	text-align: center;
}
</style>

<script src="/console/plugins/multifile-master/jquery.MultiFile.js" type="text/javascript" ></script>

<script>
	//등록
	function fncSave() {
		rules = {
			TITE : "required"
		};

		messages = {
			TITE : "제목은 필수항목입니다"
		};

		if (!fncValidate(rules, messages)) {
			return false;
		}
		
		$("#mailList option").prop("selected", true);
		var url  = "${commandMap.GUBUN}" == 'edit' ? '<c:url value="/console/email/fdcrAdA4Update1.page"/>' : '<c:url value="/console/email/fdcrAdA4Insert1.page"/>'
		var f = document.writeForm;
		f.action = url;
		f.submit();
	}

	function fncGoList(){
		location.href = '<c:url value="/console/legal/fdcrAd09List1.page"/>';
	}
	
	//팝업
	function fncPopup() {
		var popUrl = '<c:url value="/console/email/fdcrAdA4Pop1.page"/>';
		var popOption = "width=1000, height=650, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function fncSelectEmail(data) {
		var _datas = data.split("/");
		if(_datas[0] == 'group'){
			$("#mailList").append("<option value='"+data+"'>@ "+_datas[6]+"</option>");	
		} else {
			$("#mailList").append("<option value='"+data+"'>"+_datas[2]+" "+_datas[5]+"</option>");
		}
			
	}
</script>
<form name="writeForm" id="writeForm" method="post"
	enctype="multipart/form-data">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${param.MENU_SEQN}"/>" /> 
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
	<input type="hidden" id="MSG_STOR_CD" name="MSG_STOR_CD" value="<c:out value="${commandMap.MSG_STOR_CD}"/>" />
	<input type="hidden" id="MSG_ID" name="MSG_ID" value="${commandMap.MSG_ID}" />
	<input type="hidden" id="GUBUN" name="GUBUN" value="<c:out value="${commandMap.GUBUN}"/>" />
	<input type="hidden" id="TYPE" name="TYPE" value="<c:out value="${commandMap.TYPE}"/>" />
	<div class="box" style="margin-top: 30px;">
		<!-- /.box-header -->
		<div class="box-body">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th style="width: 20%">받는사람</th>
						<td style="width: 80%">
							<div style="width: 90%; float: left;">
								<select multiple="multiple" name="SEND_MAIL" class="form-control" id="mailList">
									<c:forEach items="${ds_send2}" var="send">
									<c:if test="${send.RECV_CD eq 1}"><option>${send.EXPR_EMAIL}</option></c:if>
									</c:forEach>
								</select>
			                </div>
			                <div style="width: 5%; float: left; padding: 0px 5px;">
			                	<div class="btn-group" >
									<button type="button" class="btn btn-primary" onclick="javascript:fncPopup();">조회</button>
								</div>
			                </div>
						</td>
					</tr>
					<tr>
						<th style="width: 20%">참조</th>
						<c:set var="recv_mail_addr" value=""/>
						<c:set var="expr_email" value=""/>
						
						<c:forEach items="${ds_send2}" var="send" varStatus="index">
							<c:if test="${send.RECV_CD eq 2}">
								<c:set var="recv_mail_addr" value="${recv_mail_addr}${recv_mail_addr eq '' ? '' : ',' }${send.RECV_MAIL_ADDR}" />
							</c:if>
						</c:forEach>
						<td style="width: 80%"><input class="form-control" placeholder="참조" name="REF_MAIL" value="${recv_mail_addr}"></td>
					</tr>
					<tr>
						<th style="width: 20%">제목</th>
						<td style="width: 80%"><input class="form-control" id="TITLE" name="TITLE" placeholder="제목" value="${ds_mail[0].TITLE}"></td>
					</tr>
					
					<tr>
						<th style="width: 20%">내용</th>
						<td style="width: 80%">
							<textarea id="compose-textarea" class="form-control" name="CONTENT" style="height: 300px">${ds_mail[0].CONTENT}</textarea>
						</td>
					</tr>
					
					
					<tr>
						<th style="width: 20%">첨부파일</th>
						<td style="width: 80%"><input type="file" class="multi" name="mail_attach"/></td>
					</tr>
					
				</tbody>
			</table>
		</div>
		<div class="box-footer" style="text-align: right">
			<c:if test="${commandMap.TYPE ne 'monthList'}">
				<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="">보내기</button></div>
			</c:if>
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="javascript:fncSave()">저장하기</button></div>
		</div>
	</div>
</form>

<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
	$(function () {
		
		CKEDITOR.replace('compose-textarea');  
		
		//Add text editor
		//$("#compose-textarea").wysihtml5();
	});
</script>
