<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<style>
.table > tbody > tr > th {
	text-align: center;
}
</style>

<script src="/console/plugins/multifile-master/jquery.MultiFile.js" type="text/javascript" ></script>

<script>
	//등록
	function fncSave() {
		rules = {
			TITE : "required"
		};

		messages = {
			TITE : "제목은 필수항목입니다"
		};

		if (!fncValidate(rules, messages)) {
			return false;
		}
		
		$("#mailList option").prop("selected", true);
		$("#MSG_STOR_CD").val("1"); // 발신함 1 - 대기, 2 - 발송 ??? 확인필요
		
		var f = document.writeForm;
		f.action = '<c:url value="/console/email/fdcrAdA4Insert1.page"/>';
		f.submit();
	}

	function fncGoList(){
		location.href = '<c:url value="/console/legal/fdcrAd09List1.page"/>';
	}
	
	//팝업
	function fncPopup() {
		var popUrl = '<c:url value="/console/email/fdcrAdA4Pop1.page"/>';
		var popOption = "width=1000, height=650, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function fncSelectEmail(data) {
		var _datas = data.split("/");
		if(_datas[0] == 'group'){
			$("#mailList").append("<option value='"+data+"'>@ "+_datas[6]+"</option>");	
		} else {
			$("#mailList").append("<option value='"+data+"'>"+_datas[2]+" "+_datas[5]+"</option>");
		}
			
	}
	
</script>

<form name="writeForm" id="writeForm" method="post"
	enctype="multipart/form-data">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${param.MENU_SEQN}"/>" /> 
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
	<input type="hidden" id="MSG_STOR_CD" name="MSG_STOR_CD" value="" />
	
	<div class="box" style="margin-top: 30px;">
		<!-- /.box-header -->
		<div class="box-body">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th style="width: 20%">받는사람</th>
						<td style="width: 80%">
							<div style="width: 90%; float: left;">
								<select multiple="multiple" name="SEND_MAIL" class="form-control" id="mailList"></select>
			                </div>
			                <div style="width: 5%; float: left; padding: 0px 5px; ">
			                	<div class="btn-group">
									<button type="button" class="btn btn-primary" onclick="javascript:fncPopup()">조회</button>
								</div>
			                </div>
						</td>
					</tr>
					<tr>
						<th style="width: 20%">참조</th>
						<td style="width: 80%"><input class="form-control" name="REF_MAIL" placeholder="참조"></td>
					</tr>
					<tr>
						<th style="width: 20%">제목</th>
						<td style="width: 80%"><input class="form-control" name="TITLE" placeholder="제목"></td>
					</tr>
					
					<tr>
						<th style="width: 20%">내용</th>
						<td style="width: 80%">
							<textarea id="compose-textarea" class="form-control" name="CONTENT" style="height: 500px"></textarea>
						</td>
					</tr>
					
					
					<tr>
						<th style="width: 20%">첨부파일</th>
						<td style="width: 80%"><input type="file" class="multi" name="mail_attach"/></td>
					</tr>
					
				</tbody>
			</table>
		</div>
		<div class="box-footer" style="text-align: right">
			<div class="btn-group">
				<button type="submit" class="btn btn-primary" onclick="fncSend()">보내기</button>
			</div>
			<div class="btn-group"><button type="button" class="btn btn-primary" onclick="fncSave()">저장하기</button></div>
		</div>
	</div>
</form>

<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
	$(function () {
		
		CKEDITOR.replace('compose-textarea');  
		
		//Add text editor
		//$("#compose-textarea").wysihtml5();
	});
</script>
