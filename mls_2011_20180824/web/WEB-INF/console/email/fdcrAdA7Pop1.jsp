<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	
	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
</script>
<form name="updateForm" id="updateForm" method="post">
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶ 보상금 공탁공고 공고내용 보완</b></h4>
			<table class="table table-bordered" style="width: 100%">
				<colgroup>
					<col width="6%">
					<col width="22%">
					<col width="*">
					<col width="10%">
					<col width="10%">
				</colgroup>
				<thead>
					<tr>
						<th class="text-center" rowspan="2">번호</th>
						<th class="text-center" colspan="2">보완사항</th>
						<th class="text-center" rowspan="2">보완일자</th>
						<th class="text-center" rowspan="2">담당자</th>
					</tr>
					<tr>
						<th class="text-center">공고항목</th>
						<th class="text-center">보완 세부내역</th>
					</tr>
				</thead>
				<tbody>
				<c:choose>
				<c:when test="${empty ds_supl_list}">
				</c:when>
				<c:otherwise>
					<c:forEach var="info" items="${ds_supl_list}" varStatus="listStatus">
					<tr>
						<td class="text-center">${info.SUPL_SEQ}</td>
						<td class="text-center">${info.SUPL_ITEM}</td>
						<td class="text-center">${info.MODI_ITEM}</td>
						<td class="text-center">${info.RGST_DTTM}</td>
						<td class="text-center">${info.RGST_NAME}</td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		</div>
		<div class="box-footer ">
			<button type="submit" class="btn btn-primary pull-right" onclick="closePop();">닫기</button>
		</div>
	    <!-- /. box -->

</form>
