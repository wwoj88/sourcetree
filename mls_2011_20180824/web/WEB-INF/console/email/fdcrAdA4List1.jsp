<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

<style>
.table > tbody > tr > th {
	text-align: center;
}
</style>
<script>
	//등록
	function fncSave() {
		rules = {
			RECEIPT_NO : "required",
			DIVS_CD : "required",
			TITE : "required"
		};

		messages = {
			RECEIPT_NO : "접수번호는 필수항목입니다",
			DIVS_CD : "구분은 필수항목입니다",
			TITE : "제목은 필수항목입니다"
		};

		if (!fncValidate(rules, messages)) {
			return false;
		}

		var f = document.writeForm;
		f.action = '<c:url value="/console/legal/fdcrAd09Regi1.page"/>';
		f.submit();
	}

	function fncGoList(){
		location.href = '<c:url value="/console/legal/fdcrAd09List1.page"/>';
	}
</script>
<form name="writeForm" id="writeForm" method="post"
	enctype="multipart/form-data">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${param.MENU_SEQN}"/>" /> 
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
	<div class="box" style="margin-top: 30px;">
		<!-- /.box-header -->
		<div class="box-body">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th style="width: 20%">받는사람</th>
						<td style="width: 80%"></td>
					</tr>
					<tr>
						<th style="width: 20%">참조</th>
						<td style="width: 80%"></td>
					</tr>
					<tr>
						<th style="width: 20%">제목</th>
						<td style="width: 80%"></td>
					</tr>
					<tr>
						<th style="width: 20%">첨부파일</th>
						<td style="width: 80%"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-footer" style="text-align: right">
			<div class="btn-group">
				<button type="submit" class="btn btn-primary" onclick="">보내기</button>
			</div>
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="">저장하기</button></div>
		</div>
	</div>
</form>