<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

			<c:if test="${commandMap.GUBUN eq 'select1'}">
			<table class="table table-bordered table-hover text-center table-list">
				<colgroup>
					<col width="20%"/>
					<col width="*"/>
					<col width="15%"/>
					<col width="15%"/>
				</colgroup>
				<thead>
					<tr>
						<th>받는사람</th>
						<th>제목</th>
						<th>발신일자</th>
						<th>수신일자</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${ds_view}" var="info">
					<tr>
						<td><c:out value="${info.EXPR_ADDR}"/></td>
						<td><c:out value="${info.TITLE}"/></td>
						<td><c:out value="${info.START_DTTM_TRNS}"/> </td>
						<td><c:out value="${info.START_DTTM_TRNS}"/> </td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			</c:if>
			<c:if test="${commandMap.GUBUN eq 'select2'}">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th style="width: 20%">받는사람</th>
						<td style="width: 80%">
							<div style="width: 90%; float: left;">
								<select multiple class="form-control">
									<option>한국언론진흥재단 한국언론진흥재단 &lt;news@kpf.or.kr&gt;</option>
				                    <option>한국음악실연자협회 박정현 &lt;bjh@fkmp.kr&gt;</option>
				                </select>
			                </div>
			            </td>
					</tr>
					<tr>
						<th style="width: 20%">참조</th>
						<td style="width: 80%"><input class="form-control" placeholder="참조"></td>
					</tr>
					<tr>
						<th style="width: 20%">제목</th>
						<td style="width: 80%"><input class="form-control" placeholder="제목"></td>
					</tr>
					
					<tr>
						<th style="width: 20%">내용</th>
						<td style="width: 80%">
							<textarea id="compose-textarea" class="form-control" style="height: 300px"></textarea>
						</td>
					</tr>
					
					<tr>
						<th style="width: 20%">첨부파일</th>
						<td style="width: 80%"><input type="file" class="multi" name="mail_attach"/></td>
					</tr>
					
				</tbody>
			</table>
			</c:if>
		
