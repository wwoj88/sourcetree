<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

<script>
	
	function fncLoadList(value, title){
		
		
		$("#KEYWORD").val('');
		$("#MSG_STOR_CD").val(value);
		$("#title").html(title);
		fncLoad('#fdcrAdA7List1', '<c:url value="/console/email/fdcrAdA7List1Sub1.page"/>', {
			"MSG_STOR_CD" : value
		}, function(data) {
		});
		
		$("[id^=list]").each(function(){
			$(this).removeClass("active");
		});
		
		$('#list'+value).addClass("active");
		
	}
	
	function fncSearch(){
		
		var MSG_STOR_CD = $("#MSG_STOR_CD").val();
		var url = fncGetBoardParam('<c:url value="/console/email/fdcrAdA7List1.page"/>')+'&MSG_STOR_CD='+MSG_STOR_CD;
		
		var KEYWORD = $('#KEYWORD').val();
		if(KEYWORD != ''){
			KEYWORD = encodeURI(encodeURIComponent(KEYWORD));
			url += '&KEYWORD='+KEYWORD;
		}
		
		location.href = url;	
	}
		
	//팝업
	function fncPopup(SUPL_ID,MSG_STOR_CD) {
		var popUrl = '<c:url value="/console/email/fdcrAdA7Pop1.page"/>?SUPL_ID='+SUPL_ID+'&MSG_STOR_CD='+MSG_STOR_CD;
		var popOption = "width=920, height=500, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
</script>


	<div class="row">
		<form id="form" name="form" method="post" action="#">
		<input type="hidden" id="MSG_STOR_CD" name="MSG_STOR_CD" value="${commandMap.MSG_STOR_CD}"/>
		<div class="col-md-3">
			
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">목록</h3>
			    </div>
			    <div class="box-body no-padding" style="height: 670px;">
			      <ul class="nav nav-pills nav-stacked">
			        <li ${commandMap.MSG_STOR_CD eq 4 ? 'class="active"': ''} id="list4"><a href="#" onclick="javascript:fncLoadList('4','저작권자 조회공고 신청정보 보완');">저작권자 조회공고 신청정보 보완</a></li>
			        <li ${commandMap.MSG_STOR_CD eq 5 ? 'class="active"': ''} id="list5"><a href="#" onclick="javascript:fncLoadList('5','보상금 공탁공고 신청정보 보완');">보상금 공탁공고 신청정보 보완</a></li>
			        <li ${commandMap.MSG_STOR_CD eq 6 ? 'class="active"': ''} id="list6"><a href="#" onclick="javascript:fncLoadList('6','상당한 노력 신청정보 보완');">상당한 노력 신청정보 보완</a></li>
			      </ul>
			    </div>
		    </div>
		</div>
	
		<div class="col-md-9">
		  <div class="box box-default">
		    <div class="box-header with-border">
		      <h3 class="box-title" id="title">${commandMap.MSG_STOR_CD eq 4 ? '저작권자 조회공고 신청정보 보완': (commandMap.MSG_STOR_CD eq 5 ? '보상금 공탁공고 신청정보 보완': (commandMap.MSG_STOR_CD eq 6 ? '상당한 노력 신청정보 보완': ''))}</h3>
		    </div>
		    <div class="mailbox-controls">
		    	<!-- Check all button -->
		        <div class="form-group">
	                  <label for="inputEmail3" class="col-sm-1 control-label">수신자</label>
	                  <div class="col-sm-5">
	                    <input type="text" class="form-control" id="KEYWORD" name="KEYWORD" placeholder="수신자" value="${commandMap.KEYWORD}">
	                  </div>
	                  <div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncSearch();return false;">검색</button></div>
	            </div>
	        </div>
		    <!-- /.box-header -->
		    <div class="box-body" style="height:610px;overflow-y:auto">
				<table id="fdcrAdA7List1" class="table table-bordered table-hover text-center table-list">
					<colgroup>
						<col width="6%"/> 
						<col width="*"/>
						<col width="24%"/>
						<col width="16%"/>
						<col width="16%"/>
						<col width="12%"/>
					</colgroup>
					<thead>
						<tr>
							<th>순번</th>
							<th>구분</th>
							<th>수신자</th>
							<th>발신일자</th>
							<th>수신일자</th>
							<th>보완내역 조회</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
						<c:when test="${empty ds_list}">
						<tr>
							<td colspan="6"></td>
						</tr>
						</c:when>
						<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${info.RNUM}"/></td>
								<td><c:out value="${info.GUBUN}" /></td>
								<td><c:out value="${info.RECV_NAME}(${info.RECV_MAIL_ADDR})" /></td>
								<td><c:out value="${info.SEND_DTTM}"/></td>
								<td>${empty info.READ_DTTM ? '읽지않음' : info.READ_DTTM}</td>
								<td><a href="#" onclick="javascript:fncPopup('${info.SUPL_ID}','${info.MSG_STOR_CD}');">조회</a></td>
							</tr>
						</c:forEach>
						</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
		</div>
      </div>
      </form>
    </div>