<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div class="text-muted well well-sm no-shadow" style="font-size:12px;height:50px; margin-top: 5px; background-color: white;">
	<c:forEach var="group" items="${ds_group}" >
	<div class="checkbox" style="white-space: nowrap;">
		<label><input type="checkbox" value="group//${group.GROUP_NAME}//${group.GROUP_ID}//${group.GROUP_NAME}">${group.GROUP_NAME}</label>
	</div>
	</c:forEach>
</div>
<div class="text-muted well well-sm no-shadow" style="font-size:12px; height:130px; margin-top: -10px; background-color: white; overflow-x:scroll; overflow-y: auto;">
<c:choose> 	
 	<c:when test="${empty ds_list}">
 		조회된 데이타가 없습니다.
 	</c:when>
	<c:otherwise>
 		<c:forEach var="list" items="${ds_list}" >
		<div class="checkbox" style="white-space: nowrap;">
			<label>
			<input type="checkbox" value="person/${list.RECV_CD}/${list.RECV_NAME}/${list.RECV_PERS_ID}/${list.RECV_GROUP_ID}/${list.RECV_MAIL_ADDR}/${list.RECV_GROUP_NAME}">
			<c:if test="${list.FROM_MLS_YN eq 'Y'}">(*)&nbsp;</c:if>${list.RECV_COMM_NAME}&nbsp;/&nbsp;${list.RECV_USER_NAME}&nbsp;/&nbsp;${list.RECV_MAIL_ADDR}
			</label>
		</div>
		</c:forEach>
 	</c:otherwise>
</c:choose>
</div>
<p style="margin-top: -10px;">수신거부</p>
<div class="text-muted well well-sm no-shadow" style="font-size:12px;height:100px; margin-top: -10px; background-color: white; overflow: auto;">
 	조회된 데이타가 없습니다.
</div>