<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncSave(){
		var formData = $("form[name=updateForm]").serialize().replace(/%/g, '%25');
		fncPost('<c:out value="/console/event/fdcrAdA1Insert2.page"/>',formData,function(returnData){
			if(returnData=='Y'){
				alert('등록했습니다.');
			}else{
				alert('실패했습니다.');
			}
		});
	}
	
	function fncCancel(){
		self.close();
	}
</script>
<div class="box-body" style="padding:10px">
<form name="updateForm" id="updateForm" method="post">
<input type="hidden" id="ITEM_TYPE_CD" name="ITEM_TYPE_CD" value="2" />
<input type="hidden" id="ITEM_ID" name="ITEM_ID" value="1" />
<input type="hidden" id="EVENT_ID" name="EVENT_ID" value="<c:out value="${ds_data.EVENT_ID}"/>" />
<input type="hidden" id="MAX_CHOICE_CNT" name="MAX_CHOICE_CNT" value="" />
<input type="hidden" id="mode" name="mode" value="<c:out value="${commandMap.mode}"/>" />
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">문항관리</h3><br><br>
		<b>*댓글형 이벤트 안내</b><br>
		문구를 입력하시면 댓글 이벤트에 표시가 됩니다.
	</div>
	<div class="box-header with-border">
		<h4>문구입력</h4>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>
									<textarea style="width:100%;height:100%" id="ITEM_DESC" name="ITEM_DESC"><c:out value="${ds_data.ITEM_DESC}"/></textarea>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:center">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncCancel();return false;">취소</button></div>
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncSave();return false;">등록</button></div>
		</div>
	</div>
</div>
</form>
</div>