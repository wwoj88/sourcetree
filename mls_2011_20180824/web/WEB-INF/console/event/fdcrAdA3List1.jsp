<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
</script>
<form name="boardForm" id="boardForm" method="post">
<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">새로고침</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncDeleteEvent();return false;">삭제</button></div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width:20%">이벤트명</th>
					<th style="width:15%">참여자수</th>
					<th colspan="3" style="width:50%">남녀비율</th>
					<th style="width:15%">남녀수</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td rowspan="2"><c:out value="${info.EVENT_TITLE}" /></td>
								<td rowspan="2"><c:out value="${info.TOTAL_CNT}" /></td>
								<td style="width:10%">남</td>
								<td style="width:20%"><div class="progress xs"><div class="progress-bar progress-bar-green" style="width: <c:out value="${info.MPCT}" />%;"></div></div></td>
								<td style="width:10%"><c:out value="${info.MPCT}" />%</td>
								<td style="width:10%"><c:out value="${info.MCNT}" /></td>
							</tr>
							<tr>
								<td>여</td>
								<td><div class="progress xs"><div class="progress-bar progress-bar-green" style="width: <c:out value="${info.FPCT}" />%;"></div></div></td>
								<td><c:out value="${info.FPCT}" />%</td>
								<td><c:out value="${info.FCNT}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>
</form>