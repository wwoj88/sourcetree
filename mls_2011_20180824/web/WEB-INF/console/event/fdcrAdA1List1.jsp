<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//상세 이동
	function fncGoView(EVENT_ID){
		location.href = '<c:url value="/console/event/fdcrAdA1View2.page"/>?EVENT_ID='+EVENT_ID;
	}
	
	function fncDeleteEvent(){
		var isChecked = false;
		$("input[name=EVENT_ID]:checked").each(function() {
			var eventId = $(this).val();
			isChecked = true;
		});
		
		if(!isChecked){
			alert("삭제할 이벤트를 선택해주세요.");
			return false;
		}
		
		var f = document.boardForm;
		f.action = "/console/event/fdcrAdA1Delete1.page";
		f.submit();
	}
	
	//문항등록 설문형
	function fncPopup1(EVENT_ID, EVENT_TYPE_CD) {
		if(EVENT_TYPE_CD=='1'){
			var popUrl = '<c:url value="/console/event/fdcrAdA1Popup1.page"/>?EVENT_ID='+EVENT_ID;
			var popOption = "width=720, height=650, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
			window.open(popUrl, "", popOption);
		}else if(EVENT_TYPE_CD=='2'){
			var popUrl = '<c:url value="/console/event/fdcrAdA1Popup2.page"/>?EVENT_ID='+EVENT_ID;
			var popOption = "width=720, height=650, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
			window.open(popUrl, "", popOption);
		}
	}
	
	//참여 및 응답현황 팝업
	function fncPopup3(EVENT_ID) {
		var popUrl = '<c:url value="/console/event/fdcrAdA1Popup3.page"/>?EVENT_ID='+EVENT_ID;
		var popOption = "width=920, height=900, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
</script>
<form name="boardForm" id="boardForm" method="post">
<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">새로고침</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncDeleteEvent();return false;">삭제</button></div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 8%">선택</th>
					<th>이벤트</th>
					<th>미리보기</th>
					<th>운영여부</th>
					<th>유형</th>
					<th>운영기간</th>
					<th>응모기간</th>
					<th>당첨자발표</th>
					<th>당첨자수</th>
					<th>참여인원</th>
					<th>참여현황 및 당첨자</th>
					<th>당첨자 발표운영기간</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="12">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><input type="checkbox" id="EVENT_ID" name="EVENT_ID" value="<c:out value="${info.EVENT_ID}"/>"/></td>
								<td><a href="#" onclick="fncGoView('<c:out value="${info.EVENT_ID}"/>');return false;"><c:out value="${info.EVENT_TITLE}" /></a></td>
								<td>미리보기</td>ㄴ
								<td><c:out value="${info.OPER_YN}" /></td>
								<td><a href="#" onclick="fncPopup1('<c:out value="${info.EVENT_ID}"/>', '<c:out value="${info.EVENT_TYPE_CD}"/>');"><c:out value="${info.EVENT_TYPE_CD_NAME}" /></a></td>
								<td><c:out value="${info.START_DTTM_OPER}" /></td>
								<td><c:out value="${info.START_DTTM_PART}" /></td>
								<td><c:out value="${info.WIN_ANUC_DATE}" /></td>
								<td><c:out value="${info.WIN_CNT}" /></td>
								<td><c:out value="${info.PART_CNT}" /></td>
								<td><a href="#" onclick="fncPopup3('<c:out value="${info.EVENT_ID}"/>');">[조회]</a></td>
								<td><c:out value="${info.START_DTTM_WIN_ANUC}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>
</form>