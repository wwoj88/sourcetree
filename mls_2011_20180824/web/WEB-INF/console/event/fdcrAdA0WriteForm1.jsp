<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
$(function(){
	$('#DTTM_OPER').daterangepicker(
		{
			format: 'YYYY-MM-DD', language:'kr'
		}, 
		function(start, end, label) {
			$('#START_DTTM_OPER').val(start.format('YYYYMMDD'));
			$('#END_DTTM_OPER').val(end.format('YYYYMMDD'));
		}
	);	
	
	$('#DTTM_WIN_ANUC').daterangepicker(
		{
			format: 'YYYY-MM-DD', language:'kr'
		}, 
		function(start, end, label) {
			$('#START_DTTM_WIN_ANUC').val(start.format('YYYYMMDD'));
			$('#END_DTTM_WIN_ANUC').val(end.format('YYYYMMDD'));
		}
	);	
	
	$('#DTTM_PART').daterangepicker(
		{
			format: 'YYYY-MM-DD', language:'kr'
		}, 
		function(start, end, label) {
			$('#START_DTTM_PART').val(start.format('YYYYMMDD'));
			$('#END_DTTM_PART').val(end.format('YYYYMMDD'));
		}
	);	
	$('#WIN_ANUC_DATE').datepicker({
		format: 'yyyymmdd', language:'kr',
		autoClose: true
	});	
	
	
})
//등록
function fncWrite(){
	rules = {
			DTTM_OPER : "required",
			DTTM_WIN_ANUC : "required",
			DTTM_PART : "required",
			WIN_ANUC_DATE : "required"
	};

	messages = {
			DTTM_OPER : "이벤트 운영기간을 선택해주세요",
			DTTM_WIN_ANUC : "당첨 발표기간을 선택해주세요요",
			DTTM_PART : "응모기간을 선택해주세요",
			WIN_ANUC_DATE : "당첨자 발표을 선택해주세요"
	};
	
	if(!fncValidate(rules,messages)){
		return false;
	}
	
	
	var f = document.writeForm;
	f.action = '<c:url value="/console/event/fdcrAdA0Insert1.page"/>';
	f.submit();
}

//취소
function fncCancel(){
	location.href="/console/event/fdcrAdA0WriteForm1.page";
}
</script>
<form name="writeForm" id="writeForm" method="post" enctype="multipart/form-data">
	<input type="hidden" id="START_DTTM_OPER" name="START_DTTM_OPER" value=""/>
	<input type="hidden" id="END_DTTM_OPER" name="END_DTTM_OPER" value=""/>
	<input type="hidden" id="START_DTTM_WIN_ANUC" name="START_DTTM_WIN_ANUC" value=""/>
	<input type="hidden" id="END_DTTM_WIN_ANUC" name="END_DTTM_WIN_ANUC" value=""/>
	<input type="hidden" id="START_DTTM_PART" name="START_DTTM_PART" value=""/>
	<input type="hidden" id="END_DTTM_PART" name="END_DTTM_PART" value=""/>
	
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered text-center">
			<tbody>
				<tr>
					<th style="width: 20%" colspan="2">이벤트명</th>
					<td style="width: 80%"><input class="form-control" type="text" name="EVENT_TITLE" id="EVENT_TITLE" value=""/></td>
				</tr>
				<tr>
					<th style="width: 20%" colspan="2">이벤트 운영기간</th>
					<td style="width: 80%"><input class="form-control" type="text" name="DTTM_OPER" id="DTTM_OPER" value="" readonly/></td>
				</tr>
				<tr>
					<th style="width: 20%" colspan="2">당첨자 발표기간</th>
					<td style="width: 80%"><input class="form-control" type="text" name="DTTM_WIN_ANUC" id="DTTM_WIN_ANUC" value="" readonly/></td>
				</tr>
				<tr>
					<th style="width: 20%" colspan="2">운영여부</th>
					<td style="width: 80%"><input type="checkbox" id="OPER_YN" name="OPER_YN" value="Y"/></td>
				</tr>
				<tr>
					<th style="width: 10%">공개여부</th>
					<th style="width: 10%">항목</th>
					<th>내용</th>
				</tr>
				<tr>
					<th style="width: 10%"><input type="checkbox" id="OPEN_YN_PROP" name="OPEN_YN_PROP" value="Y"/></th>
					<th style="width: 10%">목적</th>
					<td><textarea rows="3" cols="50" id="PROP_DESC" name="PROP_DESC"></textarea></td>
				</tr>
				<tr>
					<th style="width: 10%"><input type="checkbox" id="OPEN_YN_DTTM_PART" name="OPEN_YN_DTTM_PART" value="Y"/></th>
					<th style="width: 10%">응모기간</th>
					<td><input class="form-control" type="text" name="DTTM_PART" id="DTTM_PART" value="" readonly/></td>
				</tr>
				<tr>
					<th style="width: 10%"><input type="checkbox" id="OPEN_YN_WIN_ANUC_DESC" name="OPEN_YN_WIN_ANUC_DESC" value="Y"/></th>
					<th style="width: 10%">참여안내</th>
					<td><textarea rows="3" cols="50" name="WIN_ANUC_DESC" id="WIN_ANUC_DESC"></textarea></td>
				</tr>
				<tr>
					<th style="width: 10%"><input type="checkbox" id="OPEN_YN_WIN_ANUC_DATE" name="OPEN_YN_WIN_ANUC_DATE" value="Y"/></th>
					<th style="width: 10%">당첨자발표</th>
					<td><input class="form-control" type="text" name="WIN_ANUC_DATE" id="WIN_ANUC_DATE" value="" readonly/></td>
				</tr>
				<tr>
					<th style="width: 10%"><input type="checkbox" id="OPEN_YN_WIN_CNT" name="OPEN_YN_WIN_CNT" value="Y"/></th>
					<th style="width: 10%">당첨자 수</th>
					<td><input class="form-control" type="text" name="WIN_CNT" id="WIN_CNT" value=""/></td>
				</tr>
				<tr>
					<th style="width: 10%"><input type="checkbox" id="OPEN_YN_IMAGE" name="OPEN_YN_IMAGE" value="Y"/></th>
					<th style="width: 10%">안내이미지</th>
					<td><div id="DivIdFile1" ><input type="file" id="file1" name="file1" value="" size="50" style="float:left"><input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);"></div></td>
				</tr>
				<tr>
					<th style="width: 20%" colspan="2">동의항목 관리</th>
					<td style="width: 80%">
						<input type="checkbox" id="AGREE_ITEM_CD_1" name="AGREE_ITEM_CD_1" value="Y"/> 개인정보 수집/이용에 대한 동의<br/>
						<textarea rows="3" cols="50" name="ITEM_DESC_1" id="ITEM_DESC_1"></textarea>
						<br/>
						<input type="checkbox" id="AGREE_ITEM_CD_2" name="AGREE_ITEM_CD_2" value="Y"/> 개인정보 취급위탁에 대한 동의<br/>
						<textarea rows="3" cols="50" name="ITEM_DESC_2" id="ITEM_DESC_2"></textarea>
						<br/>
						<input type="checkbox" id="AGREE_ITEM_CD_99" name="AGREE_ITEM_CD_99" value="Y"/> 기타<br/>
						<textarea rows="3" cols="50" name="ITEM_DESC_99" id="ITEM_DESC_99"></textarea>
					</td>
				</tr>
				<tr>
					<th style="width: 20%" colspan="2">이벤트 유형</th>
					<td style="width: 80%">
						<input type="radio" id="EVENT_TYPE_CD" name="EVENT_TYPE_CD" value="1" checked/> 설문형
						<input type="radio" id="EVENT_TYPE_CD" name="EVENT_TYPE_CD" value="2"/> 댓글달기
					</td>
				</tr>
				<tr>
					<th style="width: 20%" colspan="2">참여 유형</th>
					<td style="width: 80%">
						<input type="checkbox" id="PART_DUPL_YN" name="PART_DUPL_YN" value="Y"/> 복수음답
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncWrite();return false;">저장</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncCancel();return false;">취소</button></div>
	</div>
</div>
</form>