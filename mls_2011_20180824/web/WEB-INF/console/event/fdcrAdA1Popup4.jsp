<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
function fncResultYes(){
	var isChecked = false;
	$("input[name=CHK]:checked").each(function() {
		isChecked = true;
	});
	
	if(!isChecked){
		alert("당첨자를 선택해주세요.");
		return false;
	}
	
	var f = document.updateForm;
	f.action = "/console/event/fdcrAdA1Insert3.page";
	f.submit();
}

function fncSelect(){
	var data = {};
	data.EVENT_ID = $('#EVENT_ID').val();
	data.WIN_RANDOM_CNT = $('#WIN_RANDOM_CNT').val();
	fncLoad('#DivIdFdcrAdA1Popup4Sub1','<c:out value="/console/event/fdcrAdA1Popup4Sub1.page"/>',data,function(){
	});
}
</script>
<div class="box-body" style="padding:10px">
<form name="updateForm" id="updateForm" method="post">
<input type="hidden" id="EVENT_ID" name="EVENT_ID" value="<c:out value="${ds_event.EVENT_ID}"/>" />

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">당첨자 자동선정</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th class="text-center search-th">이벤트명</th>
								<td colspan="3"><c:out value="${ds_event.EVENT_TITLE }"/></td>
							</tr>
							<tr>
								<th class="text-center search-th">기준</th>
								<td>
									<select id="STATNO" name="STATNO">
										<option value="1">전체</option>
										<option value="2">오답자제외</option>
										<option value="3">복수응답제외</option>
										<option value="4">당첨자</option>
									</select>
								</td>
								<th class="text-center search-th">선정인원</th>
								<td>
									<select id="WIN_RANDOM_CNT" name="WIN_RANDOM_CNT">
									<c:forEach begin="1" end="${ds_event.WIN_CNT}" var="i" varStatus="s">
										<option value="${ds_event.WIN_CNT-i+1}">${ds_event.WIN_CNT-i+1}</option>
									</c:forEach>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncSelect();return false;">선정하기</button></div>
		</div>
	</div>
</div>

<div class="box box-default">
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group" id="DivIdFdcrAdA1Popup4Sub1">
					<table class="table table-bordered text-center table-list ">
						<thead>
							<tr>
								<th>선택</th>
								<th>당첨여부</th>
								<th>이름</th>
								<th>아이디</th>
								<th>성별</th>
								<th>생년월일</th>
								<th>휴대전화</th>
								<th>이메일</th>
								<th>참여일자</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</div>
</div>
<div class="box-footer clearfix text-center">
	<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncResultYes();return false;">선택 당첨자 등록</button></div>
</div>
</form>
</div>