<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
					<table class="table table-bordered text-center table-list ">
						<thead>
							<tr>
								<th>선택</th>
								<th>당첨여부</th>
								<th>이름</th>
								<th>아이디</th>
								<th>성별</th>
								<th>생년월일</th>
								<th>휴대전화</th>
								<th>이메일</th>
								<th>참여일자</th>
							</tr>
						</thead>
						<tbody>
					<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><input type="checkbox" id="CHK" name="CHK" value="<c:out value="${info.EVENT_ID}"/>|<c:out value="${info.USER_IDNT}"/>|<c:out value="${info.PART_CNT}"/>"/></td>
								<td><c:out value="${info.WIN_NAME}"/></td>
								<td><c:out value="${info.NAME}"/></td>
								<td><c:out value="${info.USER_IDNT}"/></td>
								<td><c:out value="${info.SEX}"/></td>
								<td><c:out value="${info.BIRTH_DTAE}"/></td>
								<td><c:out value="${info.MOBL_PHON}"/></td>
								<td><c:out value="${info.MAIL}"/></td>
								<td><c:out value="${info.PART_DTTM}"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
					</c:choose>
						</tbody>
					</table>
