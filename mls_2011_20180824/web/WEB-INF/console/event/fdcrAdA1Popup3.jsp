<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
function fncResultYes(){
	var isChecked = false;
	$("input[name=CHK]:checked").each(function() {
		isChecked = true;
	});
	
	if(!isChecked){
		alert("당첨자를 선택해주세요.");
		return false;
	}
	
	var f = document.updateForm;
	f.action = "/console/event/fdcrAdA1Insert3.page";
	f.submit();
}

function fncResultNo(){
	var isChecked = false;
	$("input[name=CHK]:checked").each(function() {
		isChecked = true;
	});
	
	if(!isChecked){
		alert("당첨 취소자를 선택해주세요.");
		return false;
	}
	
	var f = document.updateForm;
	f.action = "/console/event/fdcrAdA1Insert4.page";
	f.submit();
}

function fncSelectAuto(){
	var EVENT_ID = $('#EVENT_ID').val();
	var popUrl = '<c:url value="/console/event/fdcrAdA1Popup4.page"/>?EVENT_ID='+EVENT_ID;
	var popOption = "width=920, height=900, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
	window.open(popUrl, "fncSelectAuto", popOption);
}
</script>
<div class="box-body" style="padding:10px">
<form name="updateForm" id="updateForm" method="post">
<input type="hidden" id="EVENT_ID" name="EVENT_ID" value="<c:out value="${ds_event.EVENT_ID}"/>" />
<input type="hidden" id="target" name="target" value="self" />
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">참여 및 응답현황</h3><br><br>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table class="table table-bordered text-center table-list ">
						<tbody>
							<tr>
								<th>이벤트</th>
								<th>유형</th>
								<th>참여인원</th>
								<th>당첨자 선정</th>
							</tr>
							<tr>
								<td><c:out value="${ds_event.EVENT_TITLE}"/></td>
								<td><c:out value="${ds_event.EVENT_TYPE_NAME}"/></td>
								<td><c:out value="${ds_event.PART_CNT}"/></td>
								<td><c:out value="${ds_event.WIN_TOTAL_CNT}"/></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</div>
</div>

<div class="box box-default" style="width:900px;height:500px;overflow:auto">
	<div class="box-header with-border">
		<div style="float:left">
			<select id="STATNO" name="STATNO">
				<option value="1">전체</option>
				<option value="2">오답자제외</option>
				<option value="3">복수응답제외</option>
				<option value="4">당첨자</option>
			</select>
		</div>
		<div style="float:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncCancel();return false;">Excel Down</button></div>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body" style="width:1200px;">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table class="table table-bordered text-center table-list ">
						<thead>
							<tr>
								<th rowspan="2">선택</th>
								<th colspan="8">참여자 정보</th>
								<th rowspan="2">답변일치항목</th>
								<c:if test="${!empty headerList}">
								<th colspan="<c:out value="${colCnt}"/>">응답현황</th>
								</c:if>
							</tr>
							<tr>
								<th>당첨여부</th>
								<th>이름</th>
								<th>아이디</th>
								<th>성별</th>
								<th>생년월일</th>
								<th>휴대전화</th>
								<th>이메일</th>
								<th>참여일자</th>
								<c:forEach var="info" items="${headerList}" varStatus="listStatus">
								<th><c:out value="${info.ITEM_DESC}"/></th>
								</c:forEach>
							</tr>
						</thead>
						<tbody>
					<c:choose>
					<c:when test="${empty ds_list}">
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td><input type="checkbox" id="CHK" name="CHK" value="<c:out value="${info.EVENT_ID}"/>|<c:out value="${info.USER_IDNT}"/>|<c:out value="${info.PART_CNT}"/>"/></td>
								<td><c:out value="${info.WIN_NAME}"/></td>
								<td><c:out value="${info.NAME}"/></td>
								<td><c:out value="${info.USER_IDNT}"/></td>
								<td><c:out value="${info.SEX}"/></td>
								<td><c:out value="${info.BIRTH_DTAE}"/></td>
								<td><c:out value="${info.MOBL_PHON}"/></td>
								<td><c:out value="${info.MAIL}"/></td>
								<td><c:out value="${info.PART_DTTM}"/></td>
								<td><c:out value="${info.ANS_EQ}"/></td>
								<c:forEach begin="1" end="${colCnt}" var="i" varStatus="s">
								<c:set var="tempName">COL${i}</c:set>
								<td width="100px"><c:out value="${info[tempName]}"/></td>
								</c:forEach>
							</tr>
						</c:forEach>
					</c:otherwise>
					</c:choose>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</div>
</div>
<div class="box-footer clearfix text-center">
	<div style="float:left">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncResultYes();return false;">선택참여자 당첨자 등록</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncSelectAuto();return false;">당첨자 자동선정</button></div>
	</div>
	<div style="float:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncResultNo();return false;">선택 당첨취소</button></div>
	</div>
</div>
</form>
</div>