<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	function fncGoSearch(){
		var SCH_CA_ID = $('#SCH_CA_ID').val();
		var GENRE = $('#GENRE').val();
		var url = fncGetBoardParam('<c:url value="/console/cnsgn/fdcrAd58List1.page"/>')+'&SCH_CA_ID='+SCH_CA_ID+'&GENRE='+GENRE;
		location.href = url;
	}
	
	function form_change(excelForm){
		if(excelForm == "1"){
			fdcrAd21List1_1.style.display="inline-table";
			fdcrAd21List1_2.style.display="none";
		}else if(excelForm == "2"){
			fdcrAd21List1_1.style.display="none";
			fdcrAd21List1_2.style.display="inline-table";
		}
	}
</script>
<style>
.fixed-table-body {
  overflow-x: auto;
}
</style>
<form name="reportForm" id="reportForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
	<table class="table table-bordered text-center">
			<tbody>
				<tr>
					<th class="text-center">제출근거 및 목적</th>
				</tr>
				<tr>
					<td class="text-center">저작권법 시행령 제52조제3항에 의거 위탁관리업자는 관리 저작물을 매월 10일까지 보고하도록 함<br/><br/>
					동법시행령 제18조제2항2호에 근거하여 문화체육관광부 장관은 위탁과리업자가 보고한 사항을 근거로 해당 저작물의<br/><br/>
					저작재산권자나 그의 거소를 조회하는 등의 저작권자 찾기 위한 상당한 노력을 수행하기 위함<br/><br/><br/></td>
				</tr>
			</tbody>
		</table>
		<table class="table table-bordered text-center">
			<tbody>
				<tr>
					<th class="text-center">보고기관</th>
					<td class="text-center">
						<c:forEach var="dsReportList" items="${ds_report_List}" varStatus="listStatus">
						<c:set var="repCrId" value="${dsReportList.CR_ID }"></c:set>
						<c:set var="repGenre" value="${dsReportList.GENRE }"></c:set>
						</c:forEach>
						<select class="form-control" id="SCH_CA_ID" name="SCH_CA_ID">
							<option selected="selected" value="" >선택하세요</option>
							<option value="201" <c:if test="${repCrId eq '201'}">selected="selected"</c:if>>한국음악저작권협회</option>
							<option value="202" <c:if test="${repCrId eq '202'}">selected="selected"</c:if>>한국음악실연자연합회</option>
							<option value="203" <c:if test="${repCrId eq '203'}">selected="selected"</c:if>>한국음반산업협회</option>
							<option value="220" <c:if test="${repCrId eq '220'}">selected="selected"</c:if>>함께하는음악저작인협회</option>
							<option value="204" <c:if test="${repCrId eq '204'}">selected="selected"</c:if>>한국문예학술저작권협회</option>
						</select>
					</td>
					<th class="text-center">보고건수(조회 건수)</th>
					<td class="text-center"></td>
				</tr>
				<tr>
					<th class="text-center">담당자</th>
					<td class="text-center"><input type="text" id="" name=""/></td>
					<th class="text-center">담당자 직위</th>
					<td class="text-center"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<th class="text-center">담당자 전화번호</th>
					<td class="text-center"><input type="text" id="" name=""/></td>
					<th class="text-center">담당자 이메일</th>
					<td class="text-center"><input type="text" id="" name=""/></td>
				</tr>
			</tbody>
		</table>
			
	<div style="overflow: scroll; height: 520px; width: 100%;">
		<div class="box" style="margin-top: 30px;">
		<!-- /.box-header -->
		<div class="box-body">
		<div class="fixed-table-body">
			<div><b>관리저작물등록 확인</b></div>
			<div style="display: inline-block; margin-top: 15px; width: 100%">
				<select class="form-control" id="GENRE" name="GENRE" onchange="javascript:form_change(this.value);" style="width: 100px; display: inline-block;">
					<option value="1" <c:if test="${repGenre eq '1'}">selected="selected"</c:if>>음악</option>
					<option value="2" <c:if test="${repGenre eq '2'}">selected="selected"</c:if>>어문</option>
				</select>
				<a href="#" onclick="fncGoSearch();return false;"><input type="button" value="저작물조회"></a>
				<a href="#" onclick="fncValChk();return false;"><input type="button" value="유효성확인" id="valChk" name="valChk"></a>
				<a href="#" onclick="fncDelete();return false;"><input type="button" value="선택삭제" id="deleteChk" name="deleteChk"></a>
				<a href="#" onclick="fncSave();return false;"><input type="button" value="일괄등록" id="insertAll" name="insertAll"></a>
			</div>
			<table id="fdcrAd21List1_1" class="table table-bordered" style="margin-top: 15px; width:1500px; table-layout:fixed; overflow-x:scroll; ">
			<thead>
				<tr>
					<th class="text-center" style="width:50px;">선택</th>
					<th class="text-center" style="width:50px;">순번</th>
					<th class="text-center" style="width:150px;">작성기관명</th>
					<th class="text-center" style="width:100px;">기관내부<br/>관리ID</th>
					<th class="text-center" style="width:50px;">오류<br/>건수</th>
					<th class="text-center" style="width:300px;">곡명</th>
					<th class="text-center" style="width:100px;">국내외구분</th>
					<th class="text-center" style="width:300px;">앨범명</th>
					<th class="text-center" style="width:300px;">앨범제작사</th>
					<th class="text-center" style="width:100px;">디스크면</th>
					<th class="text-center" style="width:100px;">트랙번호</th>
					<th class="text-center" style="width:150px;">앨범발매년도</th>
					<th class="text-center" style="width:150px;">작사가성명</th>
					<th class="text-center" style="width:150px;">작곡가성명</th>
					<th class="text-center" style="width:150px;">편곡가성명</th>
					<th class="text-center" style="width:150px;">역사가성명</th>
					<th class="text-center" style="width:150px;">가수성명</th>
					<th class="text-center" style="width:150px;">연주자성명</th>
					<th class="text-center" style="width:150px;">음반제작자성명</th>
					<th class="text-center" style="width:150px;">위탁관리구분</th>
					<th class="text-center" style="width:150px;">저작권자<br/>권리처리구분</th>
					<th class="text-center" style="width:150px;">실연자권리<br/>처리구분</th>
					<th class="text-center" style="width:150px;">음반제작자<br/>권리처리구분</th>
				</tr>
			</thead>
				<tbody>
				</tbody>
			</table>
			
			<table id="fdcrAd21List1_2" class="table table-bordered" style="margin-top: 15px; display: none; width:1500px; table-layout:fixed; overflow-x:scroll;">
			<thead>
				<tr>
					<th class="text-center" style="width:50px;">선택</th>
					<th class="text-center" style="width:50px;">순번</th>
					<th class="text-center" style="width:150px;">작성기관명</th>
					<th class="text-center" style="width:100px;">기관내부<br/>관리ID</th>
					<th class="text-center" style="width:50px;">오류<br/>건수</th>
					<th class="text-center" style="width:300px;">저작물명</th>
					<th class="text-center" style="width:300px;">저작물부제</th>
					<th class="text-center" style="width:150px;">창작년도</th>
					<th class="text-center" style="width:300px;">도서명</th>
					<th class="text-center" style="width:150px;">출판사</th>
					<th class="text-center" style="width:100px;">발행년도</th>
					<th class="text-center" style="width:150px;">저작자명</th>
					<th class="text-center" style="width:150px;">위탁관리구분</th>
				</tr>
			</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		</div>
		<!-- /.box-body -->
	</div>
	</div>
</form>