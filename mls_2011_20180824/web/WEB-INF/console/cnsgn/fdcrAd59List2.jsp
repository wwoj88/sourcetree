<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>

	function closePop(){
		parent.close();
		window.close();
		self.close();
	}
	//페이지 이동
	function fncGoPage(page) {

	
		var datas = [];
		//datas[0] = 'YYYYMM';
		//datas[0] = 'MENU_SEQN';
		var YYYYMM = $('#schYYYYMM').val();
		/* alert(YYYYMM); */
	   var url = fncGetBoardParam('<c:url value="/console/cnsgn/fdcrAd59List2.page"/>',datas, page)+ '&YYYYMM='+YYYYMM;
		location.href = url;
	}
	
	//검색
	function fncGoSearch(YYYYMM) {
		var SCH_GUBUN = $('#SCH_GUBUN').val();
		var SCH_TITLE = $('#SCH_TITLE').val();
		
		var url = fncGetBoardParam('<c:url value="/console/cnsgn/fdcrAd59List2.page"/>')+'&SCH_GUBUN='+SCH_GUBUN+'&SCH_TITLE='+SCH_TITLE+'&YYYYMM='+YYYYMM;
		location.href = url;
	}
	
	function fncExcelDown1() {
		//alert($('#schYYYYMM').val());
		var datas = [];
		datas[0] = 'schYYYYMM';
		datas[1] = 'GUBUN';
		datas[2] = 'TITLE';
		var url = fncGetBoardParam('<c:url value="/console/cnsgn/fdcrAd59Down1.page"/>',datas, 1);
		var YYYYMM = $('#schYYYYMM').val();
		
		var GUBUN = $('#GUBUN').val();
		var TITLE = $('#TITLE').val();
		 if(YYYYMM != '' || GUBUN != '' || TITLE != ''){
			TITLE = encodeURI(encodeURIComponent(TITLE));
			url += '&YYYYMM='+YYYYMM+'&GUBUN='+GUBUN+'&TITLE='+TITLE;
		} 
		//alert(url) 
		location.href = url;
	}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
	<input type="hidden" id="TITLE" name="TITLE" value="위탁저장물보고/월별" />
	<input type="hidden" id="schYYYYMM" name="schYYYYMM" value="${param.YYYYMM}" />
		<!-- <input="type="hidden" id="schYYYYMM" name="schYYYYMM" value="test"/> -->
	
</form>
	<c:forEach var="rept_cnt" items="${ds_count}" varStatus="listStatus">
		<c:set var="reptYmd" value="${rept_cnt.REPT_YMD }"/>
		<c:set var="cnttot_" value="${rept_cnt.CNTTOT_ }"/>
		<c:set var="reptdate" value="${rept_cnt.REPTDATE }"/>
	</c:forEach>
	<!-- /.box-header -->
	<div class="box-body">
	<h4><b>▶월별 보고현황 조회</b></h4>
	
			<div>
				<b>${fn:substring(reptYmd,0,4) }년&nbsp;&nbsp;&nbsp;&nbsp; ${fn:substring(reptYmd,4,6) }월 &nbsp;&nbsp;&nbsp;&nbsp;위탁관리 저작물 보고
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;보고건수: <c:out value="${cnttot_}"/><%-- <fmt:formatNumber value="${cnttot_}" pattern="###,###,###,##0" groupingUsed="true" /> --%>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;보고일자: ${reptdate}</b> 
			</div>
			<table class="table table-bordered text-center">
				<tbody>
				<c:choose>
				<c:when test="${empty ds_report}">
				</c:when>
				<c:otherwise>
					<c:forEach var="report" items="${ds_report}" varStatus="listStatus">
					<tr>
						<th>보고기관</th>
						<td class="text-center" colspan="3"><c:out value="${report.COMM_NAME }"/></td>
					</tr>
					<tr>
						<th>담당자</th>
						<td class="text-center"><c:out value="${report.REPT_CHRR_NAME }" escapeXml="false"/></td>
						<th class="text-center">담당자 직위</th>
						<td class="text-center"><c:out value="${report.REPT_CHRR_POSI }"/></td>
					</tr>
					<tr>
						<th>담당자 전화번호</th>
						<td class="text-center"><c:out value="${report.REPT_CHRR_TELX }"/></td>
						<th class="text-center">담당자 이메일</th>
						<td class="text-center"><c:out value="${report.REPT_CHRR_MAIL }"/></td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
			<br/>
			<table class="table table-bordered text-center">
				<thead>
					<tr>
						<th class="text-center" style="width: 28%">보고건수</th>
						<th class="text-center" style="width: 8%">음악</th>
						<th class="text-center" style="width: 8%">어문</th>
						<th class="text-center" style="width: 8%">방송대본</th>
						<th class="text-center" style="width: 8%">영화</th>
						<th class="text-center" style="width: 8%">방송</th>
						<th class="text-center" style="width: 8%">뉴스</th>
						<th class="text-center" style="width: 8%">미술</th>
						<th class="text-center" style="width: 8%">이미지</th>
						<th class="text-center" style="width: 8%">기타</th>
					</tr>
				</thead>
				<tbody>
				<c:choose>
				<c:when test="${empty ds_count}">
				</c:when>
				<c:otherwise>
					<c:forEach var="count" items="${ds_count}" varStatus="listStatus">
					<tr>
						<th><c:out value="${count.CNT_TOT }"/></th>
						<td class="text-center"><c:out value="${count.CNT_1 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_2 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_3 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_4 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_5 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_6 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_7 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_8 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_9 }"/></td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		</div>
		<br/>
		<div class="box box-default">
		<!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="input-group" style="width:100%;">
							<div style="float: left; display: inline-block; margin: 5px 0 0 40px;">* 구분</div>
							<span style="float: left; display: inline-block;margin: 0 0 0 20px;">
								<select class="form-control" id="SCH_GUBUN" name="SCH_GUBUN">
									<option selected="selected" value="" >--전체--</option>
									<option value="10">저작물종류</option>
									<option value="20">저작물명</option>
									<option value="30">저작물부제</option>
									<option value="40">창작년도</option>
								</select>
							</span>
							<div style="display: inline-block; margin: 5px 0 0 20px; width: 60%">
							<input type="text" id="SCH_TITLE" name="SCH_TITLE" style="width: 750px;">
							</div>
							<div class="btn-group" style="margin-left: 70px;"><button type="submit" class="btn btn-primary" onclick="fncGoSearch('${reptYmd}');return false;">검색</button></div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>
	<br/>
	<c:forEach var="page" items="${ds_page}" varStatus="listStatus">
	<c:set var="tot_cnt" value="${page.COUNT }" />
	</c:forEach>
	<b>총&nbsp;&nbsp;<fmt:formatNumber value="${tot_cnt}" pattern="###,###,###,##0" groupingUsed="true" />&nbsp;건</b>
	<table id="fdcrAd15Pop3" class="table table-bordered" style="margin-top: 10px;">
			<thead>
				<tr>
					<th class="text-center" style="width: 10%">보고년월</th>
					<th class="text-center" style="width: 15%">저작물관리번호</th>
					<th class="text-center" style="width: 10%">국내외구분</th>
					<th class="text-center" style="width: 10%">저작물분류</th>
					<th class="text-center">저작물명</th>
					<th class="text-center" style="width: 15%">저작물부제</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty ds_list}">
						<tr>
							<td colspan="6" class="text-center">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
							<tr>
								<td class="text-center"><c:out value="${info.REPT_YMD }"/></td>
								<td class="text-center"><c:out value="${info.COMM_WORKS_ID }"/></td>
								<td class="text-center">
								<c:if test="${info.NATION_CD == '1' }">국내</c:if>
								<c:if test="${info.NATION_CD == '2' }">국외</c:if>
								</td>
								<td class="text-center">
								<c:if test="${info.GENRE_CD == '1' }">음악</c:if>
								<c:if test="${info.GENRE_CD == '2' }">어문</c:if>
								<c:if test="${info.GENRE_CD == '3' }">방송대본</c:if>
								<c:if test="${info.GENRE_CD == '4' }">영화</c:if>
								<c:if test="${info.GENRE_CD == '5' }">방송</c:if>
								<c:if test="${info.GENRE_CD == '6' }">뉴스</c:if>
								<c:if test="${info.GENRE_CD == '7' }">미술</c:if>
								<c:if test="${info.GENRE_CD == '8' }">이미지</c:if>
								<c:if test="${info.GENRE_CD == '99' }">기타</c:if>
								</td>
								<td class="text-center"><c:out value="${info.WORKS_TITLE }"/></td>
								<td class="text-center"><c:out value="${info.WORKS_SUB_TITLE }"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	  <!-- <ul>
	  	<button type="submit" class="btn btn-primary" onclick="fncExcelDown1();return false;" style="float: right; text-align: right; margin: 5px 0 0 0;">Excel Down</button>
	  </ul> -->
	</div>
	