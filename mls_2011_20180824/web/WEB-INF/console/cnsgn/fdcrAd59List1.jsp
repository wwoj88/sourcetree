<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
  // 년도 검색
  function yyyyChng(selOptVal) {
    var url = fncGetBoardParam('<c:url value="/console/cnsgn/fdcrAd59List1.page"/>') + '&YYYY=' + selOptVal;
    location.href = url;
  }

  function reptYearView(YYYYMM) {
    var url = fncGetBoardParam('<c:url value="/console/cnsgn/fdcrAd59List2.page"/>') + '&YYYYMM=' + YYYYMM + '&YYYYMM2=' + YYYYMM;
    location.href = url;
  }
</script>
<form name="writeForm" id="writeForm" method="post">
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />

	<div>
		<h4>&nbsp;&nbsp;● 관리저작물 월별 보고현황</h4>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="input-group" style="width: 100%;">
							<span style="float: right; display: inline-block;"> <c:forEach var="info" items="${ds_list}" varStatus="listStatus">
									<c:set var="reptYmd" value="${info.REPT_YMD }"></c:set>
									<c:set var="schReptYear" value="${fn:substring(reptYmd,0,4) }"></c:set>
									<c:set var="reptYYYY" value="${REPT_YYYY }"></c:set>
								</c:forEach> <select class="form-control" id="YYYY" name="YYYY" onchange="yyyyChng(this.value);">
									<option value="" selected="selected" <c:if test="${reptYYYY eq ''}">selected="selected"</c:if>>--전체--</option>

									<option value="2020" <c:if test="${reptYYYY eq '2020'}">selected="selected"</c:if>>2020년</option>
									<option value="2019" <c:if test="${reptYYYY eq '2019'}">selected="selected"</c:if>>2019년</option>
									<option value="2018" <c:if test="${reptYYYY eq '2018'}">selected="selected"</c:if>>2018년</option>
									<option value="2017" <c:if test="${reptYYYY eq '2017'}">selected="selected"</c:if>>2017년</option>
									<option value="2016" <c:if test="${reptYYYY eq '2016'}">selected="selected"</c:if>>2016년</option>
									<option value="2015" <c:if test="${reptYYYY eq '2015'}">selected="selected"</c:if>>2015년</option>
									<option value="2014" <c:if test="${reptYYYY eq '2014'}">selected="selected"</c:if>>2014년</option>
									<option value="2013" <c:if test="${reptYYYY eq '2013'}">selected="selected"</c:if>>2013년</option>
									<option value="2012" <c:if test="${reptYYYY eq '2012'}">selected="selected"</c:if>>2012년</option>
								</select>
							</span> <span style="float: right; display: inline-block;"><b>년도: </b></span>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	</div>


	<div class="box">
		<!-- /.box-header -->
		<div class="box-body" style="overflow: auto; overflow-x: hidden; width: 100%; height: 500px">
			<table id="fdcrAd74List1" class="table table-bordered text-center" cellPadding="0" border="0" width="100%" style="table-layout: fixed;">
				<thead>
					<tr>
						<th style="width: 7%;">보고년월</th>
						<th style="width: 7%;">보고일자</th>
						<th style="width: 7%;">음악</th>
						<th style="width: 7%;">어문</th>
						<th style="width: 7%;">방송대본</th>
						<th style="width: 7%;">영화</th>
						<th style="width: 7%;">방송</th>
						<th style="width: 7%;">뉴스</th>
						<th style="width: 7%;">미술</th>
						<th style="width: 7%;">이미지</th>
						<th style="width: 7%;">사진</th>
						<th style="width: 7%;">기타</th>
						<th style="width: 12%;">계</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty ds_list}">
							<tr>
								<td colspan="12" class="text-center">게시물이 없습니다.(년도 검색을 해주세요.)</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:set var="sum1" value="0" />
							<c:set var="sum2" value="0" />
							<c:set var="sum3" value="0" />
							<c:set var="sum4" value="0" />
							<c:set var="sum5" value="0" />
							<c:set var="sum6" value="0" />
							<c:set var="sum7" value="0" />
							<c:set var="sum8" value="0" />
							<c:set var="sum12" value="0" />
							<c:set var="sum9" value="0" />
							<c:set var="sum10" value="0" />
							<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
								<c:set var="reptYmd2" value="${info.REPT_YMD }"></c:set>
								<tr>
									<td class="text-center">
										<a href="#" onclick="reptYearView('<c:out value="${reptYmd2}"/>');return false;"><b><u><c:out value="${info.REPT_YMD }" /></u></b></a>
									</td>
									<td class="text-center">
										<c:out value="${info.REPTDATE }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_1 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_2 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_3 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_4 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_5 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_6 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_7 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_8 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_12 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_9 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_TOT }" />
									</td>
									<c:set var="sum1" value="${ sum1 + info.CNT1_ }" />
									<c:set var="sum2" value="${ sum2 + info.CNT2_ }" />
									<c:set var="sum3" value="${ sum3 + info.CNT3_ }" />
									<c:set var="sum4" value="${ sum4 + info.CNT4_ }" />
									<c:set var="sum5" value="${ sum5 + info.CNT5_ }" />
									<c:set var="sum6" value="${ sum6 + info.CNT6_ }" />
									<c:set var="sum7" value="${ sum7 + info.CNT7_ }" />
									<c:set var="sum8" value="${ sum8 + info.CNT8_ }" />
									<c:set var="sum12" value="${ sum12 + info.CNT12_ }" />
									<c:set var="sum9" value="${ sum9 + info.CNT9_ }" />
									<c:set var="sum10" value="${ sum10 + info.CNT10_ }" />
								</tr>
							</c:forEach>
							<tr>
								<td class="text-center">
									<b>합계</b>
								</td>
								<td></td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum1}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum2}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum3}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum4}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum5}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum6}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum7}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum8}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum12}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum9}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum1+sum2+sum3+sum4+sum5+sum6+sum7+sum8+sum9+sum10}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
		<span style="color: red; margin: 20px 0 0 10px; display: inline-block;">* 관리저작물 보고 기간은 매월 1일부터 10일까지 입니다.</span>
	</div>
</form>