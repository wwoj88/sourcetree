<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
	<!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/console/custom/img/sample.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><c:out value="${loginInfo.USER_NAME }"/></p>
          <c:if test="${loginInfo.USER_NAME != null}"><a href="/console/common/logout.page">로그아웃</a></c:if>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
<!--         <li class="header">관리자 시스템</li> -->
<!--         <li class="active treeview"><a href="#"><span>상당한노력(신청 및 조회공고)</span> <i class="fa fa-angle-left pull-right"></i></a> -->
<!-- 		<ul class="treeview-menu"> -->
<!-- 			<li><a href="#"><i class="fa fa-circle-o"></i>저작권자 조회공고(직접수행)</a></li> -->
<!-- 			<li><a href="#"><i class="fa fa-circle-o"></i>상당한노력 신청접수</a></li> -->
<!-- 			<li><a href="#"><i class="fa fa-circle-o"></i>상당한노력 수행</a></li> -->
<!-- 			<li><a href="#"><i class="fa fa-circle-o"></i>이의신청</a></li> -->
<!-- 			<li><a href="#"><i class="fa fa-circle-o"></i>통계</a> -->
<!-- 				<ul class="treeview-menu"> -->
<!-- 					<li><a href="#"><i class="fa fa-circle-o"></i>상당한노력 매칭정보</a></li> -->
<!-- 				</ul> -->
<!-- 			</li> -->
<!-- 		</ul> -->
<!-- 		</li> -->
        <c:out value="${leftMenu}" escapeXml="false"/>
      </ul>
    </section>
    <!-- /.sidebar -->