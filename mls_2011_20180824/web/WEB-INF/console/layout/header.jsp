<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
    <!-- Logo -->
    <a href="/console/main/main.page?menuId=0" class="logo">
      <span class="logo-lg" style="font-size:18px">권리자 찾기 정보시스템</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
<!--               <img src="/console/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
              <span class="hidden-xs"><c:out value="${loginInfo.USER_NAME }"/></span>
            </a>
<!--             <ul class="dropdown-menu"> -->
<!--               User image -->
<!--               <li class="user-header"> -->
<!--                 <img src="/console/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->

<!--                 <p> -->
<!--                   Alexander Pierce - Web Developer -->
<!--                   <small>Member since Nov. 2012</small> -->
<!--                 </p> -->
<!--               </li> -->
<!--               Menu Body -->
<!--               <li class="user-body"> -->
<!--                 <div class="row"> -->
<!--                   <div class="col-xs-4 text-center"> -->
<!--                     <a href="#">Followers</a> -->
<!--                   </div> -->
<!--                   <div class="col-xs-4 text-center"> -->
<!--                     <a href="#">Sales</a> -->
<!--                   </div> -->
<!--                   <div class="col-xs-4 text-center"> -->
<!--                     <a href="#">Friends</a> -->
<!--                   </div> -->
<!--                 </div> -->
<!--                 /.row -->
<!--               </li> -->
<!--               Menu Footer -->
<!--               <li class="user-footer"> -->
<!--                 <div class="pull-left"> -->
<!--                   <a href="#" class="btn btn-default btn-flat">Profile</a> -->
<!--                 </div> -->
<!--                 <div class="pull-right"> -->
<!--                   <a href="#" class="btn btn-default btn-flat">Sign out</a> -->
<!--                 </div> -->
<!--               </li> -->
<!--             </ul> -->
          </li>
          <!-- Control Sidebar Toggle Button -->
<!--           <li> -->
<!--             <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
<!--           </li> -->
        </ul>
      </div>
    </nav>