<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> -->
<!-- <script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
 -->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
body{
	background-color: #ecf0f5;
}
.insertForm{
	margin: 25px;
	border-top:1px solid #9C9C9C; border-bottom:1px solid #F6F6F6;

}

.title{
	font-size : 20px;
	font-weight : bold;
	background-color: #3c8dbc;
	color:white;
} 

.insertTable{
	width : 100%;
	background-color: white;
}
.insertTable th{
   padding: 5px;
   border: 1px solid gray;
   background-color: #eee;
}
.insertTable td{
   padding: 5px;
   border: 1px solid gray;
} 

.srchList{
	width : 100%;
	background-color: white;
}
.srchList th{
   padding: 5px;
   border: 1px solid gray;
   background-color: #eee;
}
.srchList td{
   padding: 5px;
   border: 1px solid gray;
} 



</style>
 <script type="text/javascript"> 
$(function(){
	init();
	
});

function init()
{
	btnSet();
}

function btnSet(){
	$('#srchBtn').click(srchBtnClick);
}

function srchBtnClick(event){
	$('#companySrchForm')[0].submit();
}

function nameClick(id,code,name,commName)
{
	var parent = window.opener;
	parent.document.getElementById('serviceRequesterCompany').value = commName;
	parent.document.getElementById('serviceRequester').value = name;
	parent.document.getElementById('serviceRequesterCompanyHidden').value = code;
	parent.document.getElementById('serviceRequesterHidden').value = id;
	parent.document.getElementById('serviceRequesterCompanyHiddenName').value = commName;
	parent.document.getElementById('serviceRequesterHiddenName').value = name;
	self.close();
}

</script>

</head>

<body>
<span style="font-size:20px; padding-left: 20px; padding-top: 15px"> <b>저작권찾기</b> 관리시스템 </span>
<form class="insertForm" id="companySrchForm" name="companySrchForm" action="/console/civilservice/personSrch.page" method="post">	
	
			
			<table class="insertTable">
			<tr>
				<td colspan="4" class ="title">민원 요청자 검색</td>
			</tr>
			<tr>
			<th><label>민원자명</label></th>
			<td><input type="text" name="companyName"><input type="submit" value="검색" id ="srchBtn"></td>
			</tr>
			</table>
			<br>
			<table class="srchList">
					<c:choose>
			<c:when test="${empty returnList}">
				<tr>
					<td colspan="10">데이터가 없습니다.</td>
				</tr>
			</c:when>
			<c:otherwise>
				<tr>
						<th>민원요청자명(아이디)</th>
						<th>핸드폰번호</th>
						<th>E-Mail</th>
						<th>단체 및 기관명</th>
				</tr>
				<c:forEach var="info" items="${returnList}" varStatus="listStatus">
						<tr>
							<td><a href="#" onClick="nameClick('${info.USER_IDNT}','${info.TRST_ORGN_CODE}','${info.USER_NAME}','${info.COMM_NAME}')"><c:out value="${info.USER_NAME}" /></a></td>
							<td><c:out value="${info.MOBL_PHON}" /></td>
							<td><c:out value="${info.MAIL}" /></td>
							<td><c:out value="${info.COMM_NAME}" /></td>
						</tr>
					</c:forEach>			
				</c:otherwise>
			</c:choose>		
			</table>	
</form>	

</body>
</html>