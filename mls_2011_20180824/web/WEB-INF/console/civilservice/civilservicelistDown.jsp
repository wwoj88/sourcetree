<%@ page language="java" contentType="application/vnd.ms-excel;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

<%
	String filename = "민원접수 처리현황.xls";
    
	response.setHeader( "Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "utf-8") + "" );
	response.setHeader( "Content-Description", "JSP Generated Data" );

	//response.setContentType( "application/vnd.ms-excel" );   

	//DataSet divList = (DataSet) box.getObject("EmasDivList");
	//HashMap ageMap = (HashMap) box.getObject("LicenseAgeClass");
%>
<table border="1" cellpadding="0" cellspacing="0" style="table-layout:fixed;text-align:center" >
	<thead>
		<tr>
			<th style="background-color:#eeeeee;width: 40px" >순번</th>
			<th style="background-color:#eeeeee;width: 200px" >접수일자</th>
			<th style="background-color:#eeeeee;width: 200px" >처리일자</th>
			<th style="background-color:#eeeeee;width: 200px" >제목</th>
			<th style="background-color:#eeeeee;width: 200px" >내용</th>
			<th style="background-color:#eeeeee;width: 100px" >구분</th>
			<th style="background-color:#eeeeee;width: 100px" >유형</th>
			<th style="background-color:#eeeeee;width: 100px" >상담자</th>
			<th style="background-color:#eeeeee;width: 120px" >실연자</th>
			<th style="background-color:#eeeeee;width: 140px" >처리상황</th>
			<th style="background-color:#eeeeee;width: 140px" >집 전화번호</th>
			<th style="background-color:#eeeeee;width: 140px" >핸드폰 번호</th>
		</tr>
	</thead>
	<tbody>
	
		<c:choose>
			<c:when test="${empty ds_list}">
			</c:when>
			<c:otherwise>
				<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
					<tr>
						<td><c:out value="${info.ROW_NUM}" /></td>
						<td><c:out value="${info.RGST_DTTM}" /></td>
						<td><c:out value="${info.ANSW_RGST_DTTM}" /></td>
						<td><c:out value="${info.CIVIL_TITLE}" /></td>
						<td><c:out value="${info.BOARD_CONTENT}" /></td>
						<td>
						<c:if test="${info.CIVIL_CATEGORY eq 1}">민원</c:if>
						<c:if test="${info.CIVIL_CATEGORY eq 2}">오류</c:if>
						<c:if test="${info.CIVIL_CATEGORY eq 3}">요청</c:if>
						<c:if test="${info.CIVIL_CATEGORY eq 4}">기타</c:if>
						</td>
						<%-- <td>
						<c:if test="${info.CIVIL_KIND eq 1}">개인정보</c:if>
						<c:if test="${info.CIVIL_KIND eq 2}">신탁관련</c:if>
						<c:if test="${info.CIVIL_KIND eq 3}">기타</c:if>
						</td> --%>
						<td><c:out value="${info.USER_NAME}" /></td>
						<td><c:out value="${info.PERFORMERS_IDNT}" /></td>
						<td>
						<c:if test="${info.CIVIL_STATE eq 1}">대기</c:if>
						<c:if test="${info.CIVIL_STATE eq 2}">보류</c:if>
						<c:if test="${info.CIVIL_STATE eq 3}">완료</c:if>
						</td>
						<td><c:out value="${info.HOME_NUMBER}" /></td>
						<td><c:out value="${info.PHONE_NUMBER}" /></td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
