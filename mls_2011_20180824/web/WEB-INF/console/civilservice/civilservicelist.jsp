<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <link rel="stylesheet" href="/resources/demos/style.css">
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$( function() {
	    init();
	});
	
	function init()
	{
		datePickerSet();
		srchCheck();
		checkBoxSet();
	}
	
	function checkBoxSet(){
		var checkBoxLength = $("input[type=checkbox][name=CIVIL_STATE]").length;
	
		for(var i = 0 ;  i < checkBoxLength ; i ++)
		{
			$('#CIVIL_STATE'+i).change(checkBoxChecked);
		}
		
	}
		
	
	function civilServiceDeleteClick(id){
		location.href = "/console/civilservice/civilserviceDelete.page?civilSeqn="+id;
	}
	
	function viewPageBtnClick(id){
		location.href = "/console/civilservice/civilserviceView.page?civilSeqn="+id;
	}
	
	
	function checkBoxChecked(event)
	{
		$("input[type=checkbox][name=CIVIL_STATE]").attr('checked',false);
		event.target.checked = true;
	}

	function srchCheck(){
		var civil_category = "<c:out value="${commandMap.CIVIL_CATEGORY}" />";
		var srch_title = "<c:out value="${commandMap.SRCH_TITLE}" />";
		var civil_kind = "<c:out value="${commandMap.CIVIL_KIND}" />";
		var civil_state = "<c:out value="${commandMap.CIVIL_STATE}" />";
		var civil_start_date = "<c:out value="${commandMap.START_DATE}" />";
		var civil_end_date = "<c:out value="${commandMap.END_DATE}" />";
		var civil_cubun = "<c:out value="${commandMap.GUBUN}" />"
		if(srch_title!=''){
			$("#SRCH_TITLE").val(srch_title);
		}
		if(civil_category!=''){
			$("#CIVIL_CATEGORY option:eq("+civil_category+")").attr("selected", "selected");
		}
		if(civil_state!=''){
			checkBoxSet();
			$('#CIVIL_STATE'+civil_state).prop('checked', true) ;
		}else{
			//civil_state=0;
			$('#CIVIL_STATE0').prop('checked', true) ;
		}
		if(srch_title!=''){}
		if(civil_kind!=''){
			$("#CIVIL_KIND option:eq("+civil_kind+")").attr("selected", "selected");
		}
		if(civil_start_date!=''){
			$('#datepicker').val(civil_start_date);
		}
		if(civil_end_date!=''){
			$('#datepicker2').val(civil_end_date);
		}
		if(civil_cubun!=''){
			$("#GUBUN option:eq("+civil_cubun+")").attr("selected", "selected");
		}
	}

	function datePickerSet(){
		 $.datepicker.setDefaults({
	        dateFormat: 'yy-mm-dd',
	        //한글 달력 주석
	        prevText: '이전 달',
	        nextText: '다음 달',
	        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
	        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
	        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
	        showMonthAfterYear: true,
	        yearSuffix: 'Year'
	    }); 
	    $( "#datepicker" ).datepicker();
	    $( "#datepicker2" ).datepicker();
	}
	
	function fncGoWriteForm()
	{
		var popUrl = '<c:url value="/console/civilservice/civilservicePopup.page"/>';
		var popOption = "width=700, height=900, resizable=no, status=no,resizable=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function fncGoPage(page) {
		$('#searchForm').attr('action','/console/civilservice/civilservice.page?pageIndex='+page);
		$('#searchForm')[0].submit();
	}

	//검색
	function fncGoSearch() {
		
		//$( "#datepicker2" ).val();
		var startDate = $( "#datepicker" ).val();
		var endDate = $( "#datepicker2" ).val();
		var startDateArr = startDate.split('-');
		var endDateArr = endDate.split('-');
		var startDateCompare = new Date(startDateArr[0], parseInt(startDateArr[1])-1, startDateArr[2]);
	    var endDateCompare = new Date(endDateArr[0], parseInt(endDateArr[1])-1, endDateArr[2]);
	    //console.log(startDateCompare.getTime()+" : "+ endDateCompare.getTime())
	    if(startDateCompare.getTime() > endDateCompare.getTime()) {
            alert("시작날짜와 종료날짜를 확인해 주세요.");
            return;
        }
		//console.log(startDate+" : "+ endDate)
		
		$('#searchForm').attr('action','/console/civilservice/civilservice.page');
		$('#searchForm')[0].submit();
	}
	
	function fncExcelDown1() {
		var datas = [];
		datas[0] = 'START_DATE';
		datas[1] = 'END_DATE';
		
		var url = fncGetBoardParam('<c:url value="/console/civilservice/civilserviceExcelDown.page"/>',datas, 1);
		var START_DATE = $('#START_DATE').val();
		var END_DATE = $('#END_DATE').val();
		
		if(START_DATE != '' || END_DATE != ''){
			url += '&START_DATE='+START_DATE+'&END_DATE='+END_DATE;
		}
		location.href = url;
	} 
</script>

<div>
	<form id="insertSubmitFrom" name="civilServiceForm" action="/console/civilservice/civilservice.page" method="post" enctype="multipart/form-data"  >
	
	</form>
</div>

<div class="box box-default">
	<div class="box-header with-border">
<!-- 		<h3 class="box-title">검색</h3>
 -->
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<table class="table table-bordered">
						<tr>
							<th class="text-center search-th">작주 처리건수</th>
							<td>완료 :<c:out value=" ${selectCivilStats[0].WEEK_CNT6}" /></td>
							<td>보류 :<c:out value="${selectCivilStats[0].WEEK_CNT5}" /></td>
							<td>대기 :<c:out value="${selectCivilStats[0].WEEK_CNT4}" /></td>
							<th class="text-center search-th">작월 처리건수</th>
							<td>완료 :${selectCivilStats[0].MONTH_CNT6}</td>
							<td>보류 :${selectCivilStats[0].MONTH_CNT5}</td>
							<td>대기 :${selectCivilStats[0].MONTH_CNT4}</td>
						</tr>
						<tr>
							<th class="text-center search-th">금주 처리건수</th>
							<td>완료 :<c:out value=" ${selectCivilStats[0].WEEK_CNT3}" /></td>
							<td>보류 :<c:out value="${selectCivilStats[0].WEEK_CNT2}" /></td>
							<td>대기 :<c:out value="${selectCivilStats[0].WEEK_CNT1}" /></td>
							<th class="text-center search-th">금월 처리건수</th>
							<td>완료 :${selectCivilStats[0].MONTH_CNT3}</td>
							<td>보류 :${selectCivilStats[0].MONTH_CNT2}</td>
							<td>대기 :${selectCivilStats[0].MONTH_CNT1}</td>
						</tr>
					</table>
					<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">처리상황</th>
									<td>
										<div>
										<input id="CIVIL_STATE0" name="CIVIL_STATE" type="checkbox" value="">전체&nbsp;&nbsp;&nbsp;<input id="CIVIL_STATE3" name="CIVIL_STATE" type="checkbox" value="3">완료&nbsp;&nbsp;&nbsp;
										<input id="CIVIL_STATE1" name="CIVIL_STATE" type="checkbox" value="1">대기&nbsp;&nbsp;&nbsp;<input id="CIVIL_STATE2" name="CIVIL_STATE" type="checkbox" value="2">보류&nbsp;&nbsp;&nbsp;
										</div>
									</td>
									<th class="text-center search-th">구분</th>
									<td colspan="3">
										<select id="CIVIL_CATEGORY" name="CIVIL_CATEGORY">
											<option value="">-전체-</option>
											<option value="1">민원</option>
											<option value="2">오류</option>
											<option value="3">요청</option>
											<option value="4">기타</option>
										</select>
									</td>
								</tr>
						
								<tr>
									<th class="text-center search-th">기간</th>
									<td>
										<input type="text" id="datepicker" name="START_DATE" style="width:45%;"> ~ 
										<input type="text" id="datepicker2" name="END_DATE" style="width:45%;">
									</td>
									<th class="text-center search-th">검색</th>
									<td>
										<select id="GUBUN" name="GUBUN">
										 	<option value="" >-전체-</option>
											<option value="1" >상담자</option>
											<option value="2" >제목</option>
											<option value="3" >내용</option>
										</select>
									</td>
									<td colspan="2">
										<input type="text" id="SRCH_TITLE" name="SRCH_TITLE" value="" style="width:70%;"/>
										<div class="btn-group"><button type="submit" class="btn btn-block btn-primary btn-xs" onclick="fncGoSearch();return false;">검색</button></div>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">접수</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncExcelDown1();return false;">엑셀 다운로드</button></div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
	
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
		<thead>
					<tr>
						<th>순번</th>
						<th>접수일자</th>
						<th>처리일자</th>
						<th>구분</th>
						<th>상담자</th>
						<th>민원요청자</th>
						<th>처리상황</th>
						<th>상세</th>
						<c:if test="${CONSOLE_USER.TRST_ORGN_CODE eq 200}"><th>삭제</th></c:if>
					</tr>
		</thead>
		<c:choose>
			<c:when test="${empty ds_list}">
				<tr>
					<td colspan="10">데이터가 없습니다.</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="info" items="${ds_list}" varStatus="listStatus">
				
				<tbody>
					<tr>
								<td><c:out value="${info.ROW_NUM}" /></td>
								<td><c:out value="${info.RGST_DTTM}" /></td>
								<td><c:out value="${info.ANSW_RGST_DTTM}" /></td>

								<td>
									<c:if test="${info.CIVIL_CATEGORY eq 1}">민원</c:if>
									<c:if test="${info.CIVIL_CATEGORY eq 2}">오류</c:if>
									<c:if test="${info.CIVIL_CATEGORY eq 3}">요청</c:if>
									<c:if test="${info.CIVIL_CATEGORY eq 4}">기타</c:if>
								</td>

								<td><c:out value="${info.USER_NAME}" /></td>
								
								<c:choose>
								<c:when test="${empty info.REQUESTER_NAME}">
									<td>민원 요청자 미제출</td>
								</c:when>
								<c:otherwise>
									<td><c:out value="${info.REQUESTER_NAME}" /></td>
								</c:otherwise>
								</c:choose>
	
								<td>
									<c:if test="${info.CIVIL_STATE eq 1}">대기</c:if>
									<c:if test="${info.CIVIL_STATE eq 2}">보류</c:if>
									<c:if test="${info.CIVIL_STATE eq 3}">완료</c:if>
								</td>
							
								<td><button id="viewPageBtn" type="button" class="btn btn-block btn-primary btn-xs" onClick="viewPageBtnClick('${info.CIVIL_SEQN}')">보기</button></td>
							
								<c:if test="${CONSOLE_USER.TRST_ORGN_CODE eq 200}">
									<td><button id="deleteCivilServiceBtn" type="button" class="btn btn-block btn-primary btn-xs" onclick="civilServiceDeleteClick('${info.CIVIL_SEQN}')">삭제</button></td>
								</c:if>
							</tr>
				</c:forEach>			
			</c:otherwise>
		</c:choose>			
				
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	 
	 <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div> 
</div>