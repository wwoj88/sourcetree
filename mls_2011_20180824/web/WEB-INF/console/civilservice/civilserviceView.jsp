<%@ page language="java" contentType="text/html; charset=EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <link rel="stylesheet" href="/resources/demos/style.css">
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
 <style>
 	table.contentBox{
 	width: 100%;
 	border-collapse: collapse;
    text-align: left;
    line-height: 1.5;
    margin : 20px 10px;
 	}
 	table.contentBox th{
	width:10%; 
	text-align: center;
	background-color : #ecf0f5;
	padding: 10px;
    font-weight: bold;
    vertical-align: top;
    border: 1px solid #ccc;
	}
	table.contentBox td{
	width:10%; 
	text-align: center;
	padding: 10px;
    font-weight: bold;
    vertical-align: top;
    border: 1px solid #ccc;
	}
	
	table.commentBox{
	 width: 100%;
 	 text-align: left;
 	 margin : 20px 10px;
	}
	
	table.commentBox th{
	width:10%; 
	text-align: center;
	background-color : #ecf0f5;
	padding: 10px;
    font-weight: bold;
    vertical-align: top;
    border: 1px solid #ccc;
	}
	
	table.commentBox td{
	text-align: center;
    font-weight: bold;
    vertical-align: top;
    border: 1px solid #ccc;
	}
	
	
 </style>
<script>
	$(function(){
		init();
		
	});
	
	function init()
	{
		$('input[type="checkbox"][ name="civilState"]').click(civilStateChange);
		$('#serviceListBtn').click(serviceListBtnClick);
		civilStateSet();
	}
	
	function serviceListBtnClick(event){
		location.href="/console/civilservice/civilservice.page";
	}
	
	function civilStateSet(){
		var civilState = "<c:out value="${returnList[0].civilState}"/>";
		$('input[type="checkbox"][ name="civilState"]').eq((Number(civilState)-1)).prop('checked', true);
	}
	
	function civilStateChange(event){
		$('input[type="checkbox"][ name="civilState"]').prop('checked', false);
		event.target.checked = true;
	}
	
	function fncWrite(){
		
		$('#commentForm').submit();
	}
	
	function makeForm( actionURL ) {
	    var f = document.createElement("form");
	    f.name = "frmHandle";
	    f.action = actionURL;
	    f.method = "post";
	    document.body.appendChild(f);
	    return f;
	}
	
	function addData(name, value) {
		 
	    var elem = document.createElement("input");
	 
	    elem.setAttribute("type", "hidden");
	    elem.setAttribute("name", name);
	    elem.setAttribute("value", value);
	 
	    return elem;
	}
	
	function fileDown(fileNum){
		
		var submitForm = makeForm("/console/civilservice/civilserviceFileDown.page");
		submitForm.appendChild(addData("fileNum",fileNum));
		console.log(submitForm)
		submitForm.submit();
		
	}
	
	function fncDelete()
	{
		$('#commentForm').attr("action","/console/civilservice/deleteCivilserviceComment.page");
		$('#commentForm').submit();
	}
</script>

<div class="box box-default">
	<div class="box-header with-border">

<form name="writeForm" id="writeForm" method="post">
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table class="contentBox">
			<tbody>
				<tr>
					<th >제목</th>
					<td colspan="3">${returnList[0].civilTitle}</td>
					<th >상담자</th>
					<td >${returnList[0].userName}</td>
					<th >실연자</th>
					<td >${returnList[0].performersIdnt}</td>
				</tr>
				<tr>
					<th>처리상황</th>
					<td>
						<c:if test="${returnList[0].civilState eq 1}">대기</c:if>
						<c:if test="${returnList[0].civilState eq 2}">보류</c:if>
						<c:if test="${returnList[0].civilState eq 3}">완료</c:if>
					</td>
					<th>접수일</th>
					<td >${returnList[0].rgstDttm}</td>
					<th>처리일</th>
					<td>${returnList[0].answRgstDttm}</td>
					<th>답변자</th>
					<td>${returnList[0].answRgstIdnt}</td>
				</tr>
				<tr>
					<th>구분</th>
					<td>
						<c:if test="${returnList[0].civilCategory eq 1}">민원</c:if>
						<c:if test="${returnList[0].civilCategory eq 2}">오류</c:if>
						<c:if test="${returnList[0].civilCategory eq 3}">요청</c:if>
						<c:if test="${returnList[0].civilCategory eq 4}">기타</c:if>
					</td>
					<th>유형</th>
					<td>
					<c:if test="${returnList[0].civilKind eq 1}">개인정보</c:if>
					<c:if test="${returnList[0].civilKind eq 2}">신탁관련</c:if>
					<c:if test="${returnList[0].civilKind eq 3}">기타</c:if>
					</select>
					</td>
					<th>연락처(집)</th>
					<td>
						${returnList[0].homeNumber} 
					</td>
					<th>연락처(핸드폰)</th>
					<td>
						${returnList[0].phoneNumber}
					</td>
				</tr>
				<tr>
					<th>운영사 전달일자</th>
					<td>
						${returnList[0].processingDate}
					</td>
					<th>민원 요청자</th>
					<td>
					<c:choose>
					<c:when test="${empty returnPerson[0].USER_NAME}">
						${returnList[0].serviceRequester}
					</c:when>
					<c:otherwise>
						${returnPerson[0].USER_NAME}
					</c:otherwise>
					</c:choose>
					</td>
					<th>민원 요청 업체</th>
					<td>
					<c:choose>
					<c:when test="${empty returnPerson[0].COMM_NAME}">
						${returnList[0].serviceRequesterCompany}
					</c:when>
					<c:otherwise>
						${returnPerson[0].COMM_NAME}
					</c:otherwise>
					</c:choose> 
					</td>
					<th></th>
					<td></td>
				</tr>
				<tr>
					<th>내용</th>
					<td colspan="7"><textarea name = "boardContent" rows="10" style="resize:none; width: 100%;" readonly="readonly">${returnList[0].boardContent}</textarea></td>
				</tr>
				<tr>
					<th>파일</th>
					<td colspan="7">
						
							<c:forEach var="info" items="${returnFileList}" varStatus="listStatus">
									<div><a href="#"  onclick="fileDown('${info.civilFileSeqn}')">${info.orgFileName}</a></div>
							</c:forEach>
						
					</td>
				</tr>
				
			</tbody>
		</table>
	</div>
</div>
</form>
	</div> 
</div>

<div class="box box-default">
	<div class="box-header with-border">
	<form id="commentForm" name="commentForm" method="post" action="/console/civilservice/commentCivilServiceInsert.page">
		
		<input type="hidden" name="civilSeqn" value="${returnList[0].civilSeqn}">
		<table class="commentBox">
			<c:if test="${CONSOLE_USER.TRST_ORGN_CODE eq 200}">
			<tr>
			
				<th>처리상황</th>
				<td>
					<input type="checkbox" name="civilState" id="civilState" value="1">대기
					<input type="checkbox" name="civilState" id="civilState" value="2">보류
					<input type="checkbox" name="civilState" id="civilState" value="3">완료
				</td>
				
			</tr>
			</c:if>
			<tr>
				<th>처리내용</th>
				<td colspan="7"><textarea id="boardContent" name = "boardContent" rows="10" style="resize:none; width: 100%;">${commentList[0].boardContent}</textarea></td>
			</tr>
		</table>
	<c:if test="${CONSOLE_USER.TRST_ORGN_CODE eq 200}">
		<div class="box-footer" style="text-align:right">
				<div class="btn-group"><button id="serviceListBtn" type="button" class="btn btn-primary" >목록</button></div>
				<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncWrite();return false;">저장</button></div>
				<c:if test="${commentList[0] ne null}">
					<div class="btn-group"><button type="button" class="btn btn-primary" onclick="fncDelete();return false;">삭제</button></div>
				</c:if>
	</div>
	</c:if>
	</form>
	</div>
</div>