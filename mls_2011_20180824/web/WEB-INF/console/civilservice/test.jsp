<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> -->
<!-- <script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
 -->
 
<style>
body{
	background-color: #ecf0f5;
}
.insertForm{
	margin: 25px;
	border-top:1px solid #9C9C9C; border-bottom:1px solid #F6F6F6;

}

.title{
	font-size : 20px;
	font-weight : bold;
	background-color: #3c8dbc;
	color:white;
}
/* 
.title_bottom{
	margin-bottom: 10px;
	border-top:1px solid #9C9C9C; border-bottom:1px solid #F6F6F6;
} */

div{
	margin: 5px;
	padding : 5px;
	border: 1px solid gray;
}
</style>
 <script type="text/javascript"> 
$(function(){
	init();
	
});

function init()
{
	if (window.console){
		// Add console commands here.
		}
	setNowDate();	
	checkboxOn();
	btnOn();
	fileEvent();
}

function fileEvent(){
	$('#fileAddBtn').click(fileAddEvent);
	$('#fileRemoveBtn').click(fileRemoveEvent);
}

function fileAddEvent(event){
	var text = '<input type="file" id="fileList" name="fileList" size="80" class="inputFile" id="inputFile"><label for="inputFile"></label>';
	$('#fileDiv').append(text);
	
}

function fileRemoveEvent(event){
	if($('#fileDiv').find('input').length != 1){
		$('#fileDiv').find('input').eq($('#fileDiv').find('input').length-1).remove();
		$('#fileDiv').find('label').eq($('#fileDiv').find('input').length-1).remove();	
	}
}

function showKeyCode(event) {
	event = event || window.event;
	
	var keyID = (event.which) ? event.which : event.keyCode;
	console.log(keyID==229)
	if(keyID!=9&&keyID!=8){
		
	 	if(keyID==229){
	 		
	 		event.preventDefault();
	 		
	 	}else if(( keyID >=48 && keyID <= 57 ) || ( keyID >=96 && keyID <= 105 )){
	 		
	 		if(event.target.name=="homeNumber2"){
				if(event.target.value.length>=3){
					event.target.value = event.target.value.substr(0,2)
				}
			}else {
				if(event.target.value.length>=4){
					event.target.value = event.target.value.substr(0,3)
				}
			}

	 	}else{
	 		event.preventDefault();
	 	}
	}
	

}


function btnOn()
{
	$('#performersIdntScrhBtn').click(performersIdntScrhBtnClick);
	$('#insertBtn').click(insertBtnClick);	
	$('#closeBtn').click(windowCloseClick);
}

function windowCloseClick(event){
	window.close();
}

function checkboxOn()
{
	$('input[type="checkbox"][ name="civilCategory"]').click(civilCategoryChange);
	$('input[type="checkbox"][ name="civilState"]').click(civilStateChange);
}

function setNowDate()
{
	var Now = new Date();
	var NowTime = Now.getFullYear();
	NowTime += '-' + (Now.getMonth() + 1) ;
	NowTime += '-' + Now.getDate();
	NowTime += ' ' + Now.getHours();
	NowTime += ':' + Now.getMinutes();

	$('#nowDate').val(NowTime);
}


function insertBtnClick(event)
{
	console.log($('#civilServiceForm'));
	console.log($("#boardContent").val());
	if($('#pswd').val()==""){

		$('#pswd').focus();
		return false;
	}else if($('input[type="checkbox"][ name="civilCategory"]:checked').val()==undefined){

		$('#civilCategory').focus();
		return false;
	}else if($("select[name=civilKind]").val()==undefined){

		$('#civilKind').focus();
		return false;
	}else if($("#civilTitle").val()==""){
	
		$('#civilTitle').focus();
		return false;
	}else if($("select[name=phoneNumber1]").val()==""){
	
		$('#phoneNumber1').focus();
		return false;
	}else if($("#phoneNumber2").val()==""){
	
		$('#phoneNumber2').focus();
		return false;
	}else if($("#phoneNumber3").val()==""){
	
		$('#phoneNumber3').focus();
		return false;
	}else if($("#boardContent").val()==""||$("#boardContent").val()==undefined){
	
		$('#boardContent').focus();
		return false;
	} 
	
	window.opener.name = "parentPage"; 
    document.getElementById('civilServiceForm').target = "parentPage";
    document.getElementById('civilServiceForm').submit();
    self.close();
}

function performersIdntScrhBtnClick(event){
		var url="/console/civilservice/srchPerformersIdnt.page";  
		//console.log($('#performersIdnt').val())
	    var params="performersIdnt="+encodeURIComponent($('#performersIdnt').val());  
	    //var params="performersIdnt="+$('#performersIdnt').val();
	    $.ajax({      
	        type:"GET",  
	        url:url,  
	        
	        data:params,   
	        success:function(data){   
	        	/* alert(data)
	        	console.log(data)
	            console.log(escape(data))
	            console.log(unescape(data))
	            console.log(encodeURI(data))
	            console.log(decodeURI(data))
	            console.log(encodeURIComponent(data))
	            console.log(decodeURIComponent(data)) */
	        },   
	        error:function(e){  
	            alert(e.responseText);  
	        }  
	    });
}

function civilStateChange(event){
	$('input[type="checkbox"][ name="civilState"]').prop('checked', false);
	event.target.checked = true;
}

function civilCategoryChange(event){
	$('input[type="checkbox"][ name="civilCategory"]').prop('checked', false);
	event.target.checked = true;
}


</script>
<style>

</style>
</head>

<body>
<span style="font-size:px;"> <b></b>  </span>
<form class="insertForm" id="civilServiceForm" name="civilServiceForm" action="/console/civilservice/civilserviceInsert.page" method="post" enctype="multipart/form-data" >	
	<div class ="title">????</div>
			<input type="hidden" name="civilDeth" value="0">
			<div style="background-color: white;">
			<label>???????</label>&nbsp;&nbsp;&nbsp;<input id="nowDate" name="nowDate" type="text" readonly="readonly" value="">&nbsp;&nbsp;&nbsp;
			<label>????????</label>&nbsp;&nbsp;&nbsp;<input type="text" readonly="readonly" value="${CONSOLE_USER.USER_NAME}">
			</div>
			<div style="background-color: white;">
			<input name = "rgstIdnt"type="hidden" readonly="readonly" value="${CONSOLE_USER.USER_ID}">
			<label>??????�????</label>&nbsp;&nbsp;&nbsp;<input id="pswd" name="pswd" type="password" >
			</div>
			<div style="background-color: white;">
			<label>??????</label>&nbsp;&nbsp;&nbsp;<input name="civilCategory" type="checkbox" value="1">??????<input name="civilCategory" type="checkbox" value="2">???????????????<br>
			</div>
			<div style="background-color: white;">
			<label>?????????????</label>&nbsp;&nbsp;&nbsp;<input id="performersIdnt" name="performersIdnt" type="text"><!-- <input id="performersIdntScrhBtn" type="button" value="??????????????"> --><br>
			</div>
			<div style="background-color: white;">
			<label>????</label>&nbsp;&nbsp;&nbsp; 
				<select name="civilKind">
					<option value="1" >????????????</option>
					<option value="2" >?????????</option>
					<option value="3" >�????</option>
				</select><br>
			</div>
			<div style="background-color: white;">
			<label>??????? ??????</label>&nbsp;&nbsp;&nbsp;<input id="civilTitle" name="civilTitle" type="text">
			</div>
			<div style="background-color: white;">
			<label>?????????</label>&nbsp;&nbsp;&nbsp;
			<select name="homeNumber1">
					<option value="02">????(02)</option>
					<option value="051">?????(051)</option>
					<option value="053">?????(053)</option>
					<option value="032">?????(032)</option>
					<option value="062">???????(062)</option>
					<option value="042">????(042)</option>
					<option value="052">?????(052)</option>
					<option value="044">?????(044)</option>
					<option value="031">??????(031)</option>
					<option value="033">??????(033)</option>
					<option value="043">??????(043)</option>
					<option value="041">????????(041)</option>
					<option value="063">?????(063)</option>
					<option value="061">????(061)</option>
					<option value="054">??????(054)</option>
					<option value="055">??????(055)</option>
					<option value="064">??????(064)</option>
			</select>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="homeNumber2" name="homeNumber2" type="text" onkeydown="return showKeyCode(event)">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="homeNumber3" name="homeNumber3" type="text" onkeydown="return showKeyCode(event)">
			</div>
			<div style="background-color: white;">
			<label>????????</label> &nbsp;&nbsp;&nbsp;
					<select name="phoneNumber1">
						<option value="010">010</option>
						<option value="011">011</option>
						<option value="016">016</option>
						<option value="017">017</option>
						<option value="018">018</option>
						<option value="019">019</option>
					</select>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="phoneNumber2" name="phoneNumber2" type="text" onkeydown="return showKeyCode(event)">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="phoneNumber3" name="phoneNumber3" type="text" onkeydown="return showKeyCode(event)"><br>
					<input name="civilState" type="hidden" value="1">
			</div>
				<input name="civilState" type="checkbox" value="3">???????????	<br> -->
			<div style="background-color: white;">
			<label>???????????</label> 
				<textarea id="boardContent" name = "boardContent" rows="10" style="resize:none; width: 100%;"></textarea>
			</div>	
			<div style="background-color: white;">
			<input id="fileAddBtn" type="button" value="????????�???? ???????">&nbsp;&nbsp;&nbsp;<input id="fileRemoveBtn" type="button" value="????????�???? ????">
			</div style="background-color: white;">
			<div id="fileDiv" style="background-color: white;">	
			<input type="file" id="fileList" name="fileList" size="80" class="inputFile" id="inputFile"><label for="inputFile">�????????</label>
			</div>
				
	<div style="background-color: white;"><input id="insertBtn" type="submit" value="?????">&nbsp;&nbsp;&nbsp;<input id="closeBtn" type="button" value="�ݱ�"></div>
	<!-- <div><input id="insertBtn" type="button" value="??????????"><input type="button" value="?????????"></div> -->
</form>	

</body>
</html><%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> -->
<!-- <script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
 -->
 
<style>
body{
	background-color: #ecf0f5;
}
.insertForm{
	margin: 25px;
	border-top:1px solid #9C9C9C; border-bottom:1px solid #F6F6F6;

}

.title{
	font-size : 20px;
	font-weight : bold;
	background-color: #3c8dbc;
	color:white;
}
/* 
.title_bottom{
	margin-bottom: 10px;
	border-top:1px solid #9C9C9C; border-bottom:1px solid #F6F6F6;
} */

div{
	margin: 5px;
	padding : 5px;
	border: 1px solid gray;
}
</style>
 <script type="text/javascript"> 
$(function(){
	init();
	
});

function init()
{
	if (window.console){
		// Add console commands here.
		}
	setNowDate();	
	checkboxOn();
	btnOn();
	fileEvent();
}

function fileEvent(){
	$('#fileAddBtn').click(fileAddEvent);
	$('#fileRemoveBtn').click(fileRemoveEvent);
}

function fileAddEvent(event){
	var text = '<input type="file" id="fileList" name="fileList" size="80" class="inputFile" id="inputFile"><label for="inputFile"></label>';
	$('#fileDiv').append(text);
	
}

function fileRemoveEvent(event){
	if($('#fileDiv').find('input').length != 1){
		$('#fileDiv').find('input').eq($('#fileDiv').find('input').length-1).remove();
		$('#fileDiv').find('label').eq($('#fileDiv').find('input').length-1).remove();	
	}
}

function showKeyCode(event) {
	event = event || window.event;
	
	var keyID = (event.which) ? event.which : event.keyCode;
	console.log(keyID==229)
	if(keyID!=9&&keyID!=8){
		
	 	if(keyID==229){
	 		
	 		event.preventDefault();
	 		
	 	}else if(( keyID >=48 && keyID <= 57 ) || ( keyID >=96 && keyID <= 105 )){
	 		
	 		if(event.target.name=="homeNumber2"){
				if(event.target.value.length>=3){
					event.target.value = event.target.value.substr(0,2)
				}
			}else {
				if(event.target.value.length>=4){
					event.target.value = event.target.value.substr(0,3)
				}
			}

	 	}else{
	 		event.preventDefault();
	 	}
	}
	

}


function btnOn()
{
	$('#performersIdntScrhBtn').click(performersIdntScrhBtnClick);
	$('#insertBtn').click(insertBtnClick);	
	$('#closeBtn').click(windowCloseClick);
}

function windowCloseClick(event){
	window.close();
}

function checkboxOn()
{
	$('input[type="checkbox"][ name="civilCategory"]').click(civilCategoryChange);
	$('input[type="checkbox"][ name="civilState"]').click(civilStateChange);
}

function setNowDate()
{
	var Now = new Date();
	var NowTime = Now.getFullYear();
	NowTime += '-' + (Now.getMonth() + 1) ;
	NowTime += '-' + Now.getDate();
	NowTime += ' ' + Now.getHours();
	NowTime += ':' + Now.getMinutes();

	$('#nowDate').val(NowTime);
}


function insertBtnClick(event)
{
	console.log($('#civilServiceForm'));
	console.log($("#boardContent").val());
	if($('#pswd').val()==""){

		$('#pswd').focus();
		return false;
	}else if($('input[type="checkbox"][ name="civilCategory"]:checked').val()==undefined){

		$('#civilCategory').focus();
		return false;
	}else if($("select[name=civilKind]").val()==undefined){

		$('#civilKind').focus();
		return false;
	}else if($("#civilTitle").val()==""){
	
		$('#civilTitle').focus();
		return false;
	}else if($("select[name=phoneNumber1]").val()==""){
	
		$('#phoneNumber1').focus();
		return false;
	}else if($("#phoneNumber2").val()==""){
	
		$('#phoneNumber2').focus();
		return false;
	}else if($("#phoneNumber3").val()==""){
	
		$('#phoneNumber3').focus();
		return false;
	}else if($("#boardContent").val()==""||$("#boardContent").val()==undefined){
	
		$('#boardContent').focus();
		return false;
	} 
	
	window.opener.name = "parentPage"; 
    document.getElementById('civilServiceForm').target = "parentPage";
    document.getElementById('civilServiceForm').submit();
    self.close();
}

function performersIdntScrhBtnClick(event){
		var url="/console/civilservice/srchPerformersIdnt.page";  
		//console.log($('#performersIdnt').val())
	    var params="performersIdnt="+encodeURIComponent($('#performersIdnt').val());  
	    //var params="performersIdnt="+$('#performersIdnt').val();
	    $.ajax({      
	        type:"GET",  
	        url:url,  
	        
	        data:params,   
	        success:function(data){   
	        	/* alert(data)
	        	console.log(data)
	            console.log(escape(data))
	            console.log(unescape(data))
	            console.log(encodeURI(data))
	            console.log(decodeURI(data))
	            console.log(encodeURIComponent(data))
	            console.log(decodeURIComponent(data)) */
	        },   
	        error:function(e){  
	            alert(e.responseText);  
	        }  
	    });
}

function civilStateChange(event){
	$('input[type="checkbox"][ name="civilState"]').prop('checked', false);
	event.target.checked = true;
}

function civilCategoryChange(event){
	$('input[type="checkbox"][ name="civilCategory"]').prop('checked', false);
	event.target.checked = true;
}


</script>
<style>

</style>
</head>

<body>
<span style="font-size:px;"> <b></b>  </span>
<form class="insertForm" id="civilServiceForm" name="civilServiceForm" action="/console/civilservice/civilserviceInsert.page" method="post" enctype="multipart/form-data" >	
	<div class ="title"></div>
			<input type="hidden" name="civilDeth" value="0">
			<div style="background-color: white;">
			<label></label>&nbsp;&nbsp;&nbsp;<input id="nowDate" name="nowDate" type="text" readonly="readonly" value="">&nbsp;&nbsp;&nbsp;
			<label></label>&nbsp;&nbsp;&nbsp;<input type="text" readonly="readonly" value="${CONSOLE_USER.USER_NAME}">
			</div>
			<div style="background-color: white;">
			<input name = "rgstIdnt"type="hidden" readonly="readonly" value="${CONSOLE_USER.USER_ID}">
			<label></label>&nbsp;&nbsp;&nbsp;<input id="pswd" name="pswd" type="password" >
			</div>
			<div style="background-color: white;">
			<label></label>&nbsp;&nbsp;&nbsp;<input name="civilCategory" type="checkbox" value="1"><input name="civilCategory" type="checkbox" value="2"><br>
			</div>
			<div style="background-color: white;">
			<label></label>&nbsp;&nbsp;&nbsp;<input id="performersIdnt" name="performersIdnt" type="text"><br>
			</div>
			<div style="background-color: white;">
			<label></label>&nbsp;&nbsp;&nbsp; 
				<select name="civilKind">
					<option value="1" ></option>
					<option value="2" ></option>
					<option value="3" ></option>
				</select><br>
			</div>
			<div style="background-color: white;">
			<label></label>&nbsp;&nbsp;&nbsp;<input id="civilTitle" name="civilTitle" type="text">
			</div>
			<div style="background-color: white;">
			<label></label>&nbsp;&nbsp;&nbsp;
			<select name="homeNumber1">
					<option value="02">(02)</option>
					<option value="051">(051)</option>
					<option value="053">(053)</option>
					<option value="032">(032)</option>
					<option value="062">(062)</option>
					<option value="042">(042)</option>
					<option value="052">(052)</option>
					<option value="044">(044)</option>
					<option value="031">(031)</option>
					<option value="033">(033)</option>
					<option value="043">(043)</option>
					<option value="041">(041)</option>
					<option value="063">(063)</option>
					<option value="061">(061)</option>
					<option value="054">(054)</option>
					<option value="055">(055)</option>
					<option value="064">(064)</option>
			</select>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="homeNumber2" name="homeNumber2" type="text" onkeydown="return showKeyCode(event)">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="homeNumber3" name="homeNumber3" type="text" onkeydown="return showKeyCode(event)">
			</div>
			<div style="background-color: white;">
			<label></label> &nbsp;&nbsp;&nbsp;
					<select name="phoneNumber1">
						<option value="010">010</option>
						<option value="011">011</option>
						<option value="016">016</option>
						<option value="017">017</option>
						<option value="018">018</option>
						<option value="019">019</option>
					</select>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="phoneNumber2" name="phoneNumber2" type="text" onkeydown="return showKeyCode(event)">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="phoneNumber3" name="phoneNumber3" type="text" onkeydown="return showKeyCode(event)"><br>
					<input name="civilState" type="hidden" value="1">
			</div>
				<input name="civilState" type="checkbox" value="3">	<br> -->
			<div style="background-color: white;">
			<label></label> 
				<textarea id="boardContent" name = "boardContent" rows="10" style="resize:none; width: 100%;"></textarea>
			</div>	
			<div style="background-color: white;">
			<input id="fileAddBtn" type="button" value="">&nbsp;&nbsp;&nbsp;<input id="fileRemoveBtn" type="button" value="">
			</div style="background-color: white;">
			<div id="fileDiv" style="background-color: white;">	
			<input type="file" id="fileList" name="fileList" size="80" class="inputFile" id="inputFile"><label for="inputFile"></label>
			</div>
				
	<div style="background-color: white;"><input id="insertBtn" type="submit" value="">&nbsp;&nbsp;&nbsp;<input id="closeBtn" type="button" value="�ݱ�"></div>
</form>	

</body>
</html>