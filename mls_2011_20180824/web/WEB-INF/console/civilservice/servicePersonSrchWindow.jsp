<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>

<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> -->
<!-- <script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
 -->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
body{
	background-color: #ecf0f5;
}
.insertForm{
	margin: 25px;
	border-top:1px solid #9C9C9C; border-bottom:1px solid #F6F6F6;

}

.title{
	font-size : 20px;
	font-weight : bold;
	background-color: #3c8dbc;
	color:white;
}

.insertTable{
	width : 100%;
	background-color: white;
}
.insertTable th{
   padding: 5px;
   border: 1px solid gray;
   background-color: #eee;
}
.insertTable td{
   padding: 5px;
   border: 1px solid gray;
} 


</style>
 <script type="text/javascript"> 
$(function(){
	init();
	
});

function init()
{
	var colseFlag ='<c:out value='${colseFlag}'/>';
	console.log(colseFlag);
	 if(colseFlag=="colseFlag")
	{
		opener.location.href="/console/civilservice/civilservice.page";
		self.close();
	} 
	if (window.console){
		// Add console commands here.
		}
	setNowDate();	
	setDatepicker();
	checkboxOn();
	btnOn();
	fileEvent();
}

function setDatepicker()
{
	$.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        showMonthAfterYear: true,
        yearSuffix: 'Year'
    });
    $( "#datepicker" ).datepicker();	
}

function fileEvent(){
	$('#fileAddBtn').click(fileAddEvent);
	$('#fileRemoveBtn').click(fileRemoveEvent);
}

function fileAddEvent(event){
	var text = '<input type="file" id="fileList" name="fileList" size="65" class="inputFile" id="inputFile"><label for="inputFile">첨부파일</label>';
	$('#fileDiv').append(text);
	
}

function fileRemoveEvent(event){
	if($('#fileDiv').find('input').length != 1){
		$('#fileDiv').find('input').eq($('#fileDiv').find('input').length-1).remove();
		$('#fileDiv').find('label').eq($('#fileDiv').find('input').length-1).remove();	
	}
}

function showKeyCode(event) {
	event = event || window.event;
	
	var keyID = (event.which) ? event.which : event.keyCode;
	console.log(keyID==229)
	if(keyID!=9&&keyID!=8){
		
	 	if(keyID==229){
	 		event.returnValue = false;
	 		event.preventDefault();
	 		
	 	}else if(( keyID >=48 && keyID <= 57 ) || ( keyID >=96 && keyID <= 105 )){
	 		
	 		if(event.target.name=="homeNumber2"){
				if(event.target.value.length>=3){
					event.target.value = event.target.value.substr(0,2)
				}
			}else {
				if(event.target.value.length>=4){
					event.target.value = event.target.value.substr(0,3)
				}
			}

	 	}else{
	 		event.preventDefault();
	 	}
	}
	

}


function btnOn()
{
	$('#performersIdntScrhBtn').click(performersIdntScrhBtnClick);
	$('#insertBtn').click(insertBtnClick);	
	$('#closeBtn').click(windowCloseClick);
	$('#companySrch').click(companySrchClick);
	$('#servicePersonSrch').click(servicePersonSrchClick);
}

function companySrchClick(event){
	alert("companySrchClick");
	var popUrl = '<c:url value="/console/civilservice/companySrchPopup.page"/>';
	var popOption = "width=700, height=900, resizable=no, status=no,resizable=no;"; //팝업창 옵션(optoin)
	window.open(popUrl, "", popOption);
}

function servicePersonSrchClick(event){
	alert("servicePersonSrchClick");
}

function windowCloseClick(event){
	window.close();
}

function checkboxOn()
{
	$('input[type="checkbox"][ name="civilCategory"]').click(civilCategoryChange);
	$('input[type="checkbox"][ name="civilState"]').click(civilStateChange);
}

function setNowDate()
{
	var Now = new Date();
	var NowTime = Now.getFullYear();
	NowTime += '-' + (Now.getMonth() + 1) ;
	NowTime += '-' + Now.getDate();
	NowTime += ' ' + Now.getHours();
	NowTime += ':' + Now.getMinutes();

	$('#nowDate').val(NowTime);
}


function insertBtnClick(event)
{
	if($('#pswd').val()==""){
		alert("비밀번호를 입력해주십시오.");
		$('#pswd').focus();
		return false;
	}else if($('input[type="checkbox"][ name="civilCategory"]:checked').val()==undefined){
		alert("구분을 입력해주십시오.");
		$('#civilCategory').focus();
		return false;
	}else if($("#civilTitle").val()==""){
		alert("제목을 입력해주십시오.");
		$('#civilTitle').focus();
		return false;
	}else if($("select[name=phoneNumber1]").val()==""){
		alert("연락처를 입력해주십시오.");
		$('#phoneNumber1').focus();
		return false;
	}else if($("#phoneNumber2").val()==""){
		alert("연락처를 입력해주십시오.");
		$('#phoneNumber2').focus();
		return false;
	}else if($("#phoneNumber3").val()==""){
		alert("연락처를 입력해주십시오.");
		$('#phoneNumber3').focus();
		return false;
	}else if($("#boardContent").val()==""||$("#boardContent").val()==undefined){
		alert("내용을 입력해주십시오.");
		$('#boardContent').focus();
		return false;
	} 

 	document.getElementById('civilServiceForm').submit();
}

function performersIdntScrhBtnClick(event){
		var url="/console/civilservice/srchPerformersIdnt.page";  
	    var params="performersIdnt="+encodeURIComponent($('#performersIdnt').val());  

	    $.ajax({      
	        type:"GET",  
	        url:url,  
	        
	        data:params,   
	        success:function(data){   
	        	/* alert(data)
	        	console.log(data)
	            console.log(escape(data))
	            console.log(unescape(data))
	            console.log(encodeURI(data))
	            console.log(decodeURI(data))
	            console.log(encodeURIComponent(data))
	            console.log(decodeURIComponent(data)) */
	        },   
	        error:function(e){  
	            alert(e.responseText);  
	        }  
	    });
}

function civilStateChange(event){
	$('input[type="checkbox"][ name="civilState"]').prop('checked', false);
	event.target.checked = true;
}

function civilCategoryChange(event){
	$('input[type="checkbox"][ name="civilCategory"]').prop('checked', false);
	event.target.checked = true;
}


</script>

</head>

<body>
<span style="font-size:20px; padding-left: 20px; padding-top: 15px"> <b>저작권찾기</b> 관리시스템 </span>
<form class="insertForm" id="civilServiceForm" name="civilServiceForm" action="/console/civilservice/civilserviceInsert.page" method="post" enctype="multipart/form-data" >	
	
			<input type="hidden" name="civilDeth" value="0">
			<input type="hidden" name="civilKind" value="1">
			<input name="civilState" type="hidden" value="1">
			<input name = "rgstIdnt"type="hidden" readonly="readonly" value="${CONSOLE_USER.USER_ID}">
			<input id="pswd" name="pswd" type="hidden" value="1234">
			
			<table class="insertTable">
			<tr>
				<td colspan="4" class ="title">접수</td>
			</tr>
			<tr>
			<th><label>일자  </label></th>
				<td><input id="nowDate" name="nowDate" type="text" readonly="readonly" value=""></td>
				<th><label> 상담자  </label></th><td><input type="text" readonly="readonly" value="${CONSOLE_USER.USER_NAME}"></td>
			</tr>

			<tr>
				<th><label>구분</label></th>
				<td colspan="3"><input name="civilCategory" type="checkbox" value="1">민원&nbsp;&nbsp;&nbsp;
				<input name="civilCategory" type="checkbox" value="2">오류&nbsp;&nbsp;&nbsp;
				<input name="civilCategory" type="checkbox" value="3">요청&nbsp;&nbsp;&nbsp;
				<input name="civilCategory" type="checkbox" value="4">기타<br></td>
			</tr>
			<tr>
				<th><label>민원 요청 업체</label></th>
				<td><input id="serviceRequesterCompany" name="SERVICE_REQUESTER_COMPANY" type="text" style="width:60%;"><input type="button" style="" value="검색" id="companySrch"></td>
				<th><label>민원 요청자</label></th>
				<td><input id="serviceRequester" name="SERVICE_REQUESTER" type="text" style="width:60%;"><input type="button" value="검색" id="servicePersonSrch"></td>
			</tr>
			
			<tr>
			<th>
			<label>민원 제목</label></th><td colspan="3"><input id="civilTitle" name="civilTitle" type="text" style="width: 100%;"></td>
			</tr>
			<tr>
			<th><label>연락처</label></th>
			<td colspan="3">
			<select name="homeNumber1">
					<option value="02">서울(02)</option>
					<option value="051">부산(051)</option>
					<option value="053">대구(053)</option>
					<option value="032">인천(032)</option>
					<option value="062">광주(062)</option>
					<option value="042">대전(042)</option>
					<option value="052">울산(052)</option>
					<option value="044">세종(044)</option>
					<option value="031">경기(031)</option>
					<option value="033">강원(033)</option>
					<option value="043">충북(043)</option>
					<option value="041">축남(041)</option>
					<option value="063">전북(063)</option>
					<option value="061">전남(061)</option>
					<option value="054">경북(054)</option>
					<option value="055">경남(055)</option>
					<option value="064">제주(064)</option>
			</select>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="homeNumber2" name="homeNumber2" type="text" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" onkeydown="return showKeyCode(event)">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="homeNumber3" name="homeNumber3" type="text" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" onkeydown="return showKeyCode(event)">
			</td>
			</tr>
			
			<tr>
			<th><label>휴대폰</label></th>
			<td colspan="3">
					<select name="phoneNumber1">
						<option value="010">010</option>
						<option value="011">011</option>
						<option value="016">016</option>
						<option value="017">017</option>
						<option value="018">018</option>
						<option value="019">019</option>
					</select>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="phoneNumber2" name="phoneNumber2" type="text" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" onkeydown="return showKeyCode(event)">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<input id="phoneNumber3" name="phoneNumber3" type="text" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" onkeydown="return showKeyCode(event)"><br>
					<!-- <input name="civilState" type="hidden" value="1"> -->
			</td>
			</tr>
				
			<tr>
			<th><label>민원내용</label></th>
			<td colspan="3">
			 
				<textarea id="boardContent" name = "boardContent" rows="10" style="resize:none; width: 100%;"></textarea>
			</td>	
			
			<tr>
			<td colspan="4">
			<input id="fileAddBtn" type="button" value="파일업로더 추가">&nbsp;&nbsp;&nbsp;<input id="fileRemoveBtn" type="button" value="파일업로더 삭제">
			</td>
			
			<tr>
			<td colspan="4">
			<div id="fileDiv" style="background-color: white;">	
			<input type="file" id="fileList" name="fileList" size="65" class="inputFile" id="inputFile"><label for="inputFile">첨부파일</label>
			</div>
			</td>
			</tr>
			
			<tr>
				<th><label>운영사 전달일시</label></th>
				<td colspan="3"><input type="text" id="datepicker" name="PROCESSING_DATE" style="width:45%;"></td>
			</tr>
			
			<tr>
			<td colspan="4">
			<input id="insertBtn" type="submit" value="등록">&nbsp;&nbsp;&nbsp;<input id="closeBtn" type="button" value="닫기">
			</td>
			</tr>
			</table>	
</form>	

</body>
</html>