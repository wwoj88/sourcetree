<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
	//페이지 이동
	function fncGoPage(page) {
		var datas = [];
		datas[0] = 'MENU_SEQN';
		datas[1] = 'GUBUN';
		var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd75List1.page"/>',datas, page);
		var TITLE = $('#TITLE').val();
		if(TITLE != ''){
			TITLE = encodeURI(encodeURIComponent(TITLE));
			url += '&TITLE='+TITLE;
		}
		location.href = url;
	}
	
	//검색
	function fncGoSearch() {
		var datas = [];
		datas[0] = 'MENU_SEQN';
		datas[1] = 'GUBUN';
		var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd75List1.page"/>',datas, 1);
		var TITLE = $('#TITLE').val();
		if(TITLE != ''){
			TITLE = encodeURI(encodeURIComponent(TITLE));
			url += '&TITLE='+TITLE;
		}
		location.href = url;
	}
	
	//상세 이동
	function fncGoView(BORD_SEQN){
		var datas = [];
		datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd75View1.page"/>',datas)+'&BORD_SEQN='+BORD_SEQN;
		location.href = url;
	}
	
	//등록폼 이동
	function fncGoWriteForm(){
		var datas = [];
		datas[0] = 'MENU_SEQN';
		var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd75WriteForm1.page"/>',datas);
		location.href = url;
	} 
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${param.MENU_SEQN}"/>" /> 
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${commandMap.pageIndex}"/>" />
</form>

<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">검색</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<form name="searchForm" id="searchForm" method="post">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th class="text-center search-th">구분</th>
									<td colspan="3">
										<select style="width:100px" id="GUBUN" name="GUBUN">
											<option value="00" <console:fn func="isSelected" value="00" value1="${commandMap.GUBUN }" />>전체</option>
											<option value="10" <console:fn func="isSelected" value="10" value1="${commandMap.GUBUN }" />>제목</option>
											<option value="20" <console:fn func="isSelected" value="20" value1="${commandMap.GUBUN }" />>내용</option>
										</select>
										<input type="text" id="TITLE" name="TITLE" value="<c:out value="${commandMap.TITLE}"/>"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer" style="text-align:right">
			<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoSearch();return false;">검색</button></div>
		</div>
	</div>
</div>

<div class="box" style="margin-top: 30px;">
	<div class="box-header with-border pull-right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoWriteForm();return false;">등록</button></div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list ">
			<thead>
				<tr>
					<th style="width: 8%">순번</th>
					<th>제목</th>
					<th style="width: 10%">작성자</th>
					<th style="width: 10%">작성일</th>
					<th style="width: 12%">첨부파일</th>
					<th style="width: 10%">조회수</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty list}">
						<tr>
							<td colspan="6">게시물이 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="info" items="${list}" varStatus="listStatus">
							<tr>
								<td><c:out value="${paginationInfo.totalRecordCount-(listStatus.count)-((paginationInfo.currentPageNo-1)*paginationInfo.recordCountPerPage)+1}"/></td>
								<td class="text-left"><a href="#" onclick="fncGoView('<c:out value="${info.BORD_SEQN}"/>');"><c:out value="${info.TITE}" /></a></td>
								<td><c:out value="${info.RGST_NAME}" /></td>
								<td><c:out value="${info.WDAY}" /></td>
								<td><c:if test="${info.ATTC_COUNT > 0}">다운로드</c:if></td>
								<td><c:out value="${info.INQR_CONT}" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix text-center">
	  <ul class="pagination pagination-sm no-margin">
	    <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fncGoPage"/>
	  </ul>
	</div>
</div>