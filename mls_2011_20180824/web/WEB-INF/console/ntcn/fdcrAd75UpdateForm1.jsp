<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
//첨부파일 다운로드
function fncDown(MENU_SEQN,BORD_SEQN,ATTC_SEQN){
	location.href = '<c:url value="/console/ntcn/fdcrAd75Down1.page"/>?MENU_SEQN='+MENU_SEQN+'&BORD_SEQN='+BORD_SEQN+'&ATTC_SEQN='+ATTC_SEQN;
}

//수정
function fncUpdate(){
	rules = {
			RGST_IDNT : "required",
			TITE : "required",
			BORD_DESC : "required"
	};

	messages = {
			RGST_IDNT : "이름를 입력해주세요",
			TITE : "제목을 입력해주세요",
			BORD_DESC : "내용을 입력해주세요"
	};
	
	if(!fncValidate(rules,messages)){
		return false;
	}
	
	var f = document.updateForm;
	f.action = '<c:url value="/console/ntcn/fdcrAd75Update1.page"/>';
	f.submit();
}

//취소
function fncCancel(){
	var datas = [];
	datas[0] = 'MENU_SEQN';
	datas[1] = 'BORD_SEQN';
	var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd75View1.page"/>',datas);
	location.href = url;
}
</script>
<form name="updateForm" id="updateForm" method="post" enctype="multipart/form-data">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${info.MENU_SEQN}"/>" /> 
	<input type="hidden" id="BORD_SEQN" name="BORD_SEQN" value="<c:out value="${info.BORD_SEQN}"/>" />
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th style="width: 20%">이름</th>
					<td style="width: 80%"><input class="form-control" type="text" name="RGST_IDNT" id="RGST_IDNT" value="<c:out value="${info.RGST_IDNT}"/>"/></td>
				</tr>
				<tr>
					<th style="width: 20%">HTML 여부</th>
					<td style="width: 80%"><input type="checkbox" name="HTML_YSNO" id="HTML_YSNO" value="Y" <console:fn func="isChecked" value="Y" value1="${info.HTML_YSNO}"/>></td>
				</tr>
				<tr>
					<th style="width: 20%">제목</th>
					<td style="width: 80%"><input class="form-control" type="text" name="TITE" id="TITE" value="<c:out value="${info.TITE}"/>"/></td>
				</tr>
				<tr>
					<th style="width: 20%">내용</th>
					<td style="width: 80%"><textarea name="BORD_DESC" id="BORD_DESC" class="form-control" rows="35" placeholder="Enter ..."><c:out value="${info.BORD_DESC}"/></textarea></td>
				</tr>
				<tr>
					<th style="width: 20%">첨부파일</th>
					<td style="width: 80%" id="TdIdFile">
						<p>* 체크박스에 체크한 항목은 삭제처리됩니다.</p>
						<c:choose>
						<c:when test="${empty attachList}">
						</c:when>
						<c:otherwise>
							<c:forEach var="attach" items="${attachList}"
								varStatus="listStatus">
							<p class="file">
								<input type="checkbox" name="orgFileDel" value="<c:out value="${attach.ATTC_SEQN}"/>"/> <console:fn func="getFileIcon" value="${attach.REAL_FILE_NAME}"/>
								<a href="#" onclick="fncDown('<c:out value="${attach.MENU_SEQN}"/>','<c:out value="${attach.BORD_SEQN}"/>','<c:out value="${attach.ATTC_SEQN}"/>');return false;"><c:out value="${attach.FILE_NAME}"/></a>
								<span>(파일크기: <console:fn func="strFileSize" value="${attach.FILE_SIZE}"/>)</span>
							</p>
							</c:forEach>
						</c:otherwise>
						</c:choose>
						<div style="height:50px;" onclick="fncFileAdd();return false;"><i class="fa fa-fw fa-plus"></i><input type="button" value="파일추가" style="height:30px;"></div>
						<div id="DivIdFile1" ><input type="file" id="file1" name="file1" value="" size="50" style="float:left"><input type="button" id="file1del" value="파일삭제" onclick="fncFileMinus(1);"></div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncUpdate();return false;">수정</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncCancel();return false;">취소</button></div>
	</div>
</div>
</form>