<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<script>
//첨부파일 다운로드
function fncDown(MENU_SEQN,BORD_SEQN,ATTC_SEQN){
	location.href = '<c:url value="/console/ntcn/fdcrAd75Down1.page"/>?MENU_SEQN='+MENU_SEQN+'&BORD_SEQN='+BORD_SEQN+'&ATTC_SEQN='+ATTC_SEQN;
}

//수정 폼으로 이동
function fncGoUpdateForm(){
	var datas = [];
	datas[0] = 'MENU_SEQN';
	datas[1] = 'BORD_SEQN';
	
	var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd75UpdateForm1.page"/>',datas);
	location.href = url;
} 

//삭제..
function fncGoDelete(){
	var datas = [];
	datas[0] = 'MENU_SEQN';
	datas[1] = 'BORD_SEQN';
	
	var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd75Delete1.page"/>',datas);
	location.href = url;
} 

function fncGoList(){
	var datas = [];
	datas[0] = 'MENU_SEQN';
	var url = fncGetBoardParam('<c:url value="/console/ntcn/fdcrAd75List1.page"/>',datas);
	location.href = url;
}
</script>
<form name="boardForm" id="boardForm" method="post">
	<input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${info.MENU_SEQN}"/>" /> 
	<input type="hidden" id="BORD_SEQN" name="BORD_SEQN" value="<c:out value="${info.BORD_SEQN}"/>" />
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
</form>
<div class="box" style="margin-top: 30px;">
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th style="width: 20%">이름</th>
					<td style="width: 80%"><input class="form-control" type="text" name="RGST_IDNT" id="RGST_IDNT" value="<c:out value="${info.RGST_IDNT}"/>" readonly="readonly"/></td>
				</tr>
				<tr>
					<th style="width: 20%">제목</th>
					<td style="width: 80%"><input class="form-control" type="text" name="TITE" id="TITE" value="<c:out value="${info.TITE}"/>" readonly="readonly"/></td>
				</tr>
				<tr>
					<th style="width: 20%">내용</th>
					<td style="width: 80%"><textarea name="BORD_DESC" id="BORD_DESC" class="form-control" rows="35" placeholder="Enter ..." readonly="readonly"><c:out value="${info.BORD_DESC}"/></textarea></td>
				</tr>
				<tr>
					<th style="width: 20%">첨부파일</th>
					<td style="width: 80%">
						<c:choose>
						<c:when test="${empty attachList}">
						</c:when>
						<c:otherwise>
							<c:forEach var="attach" items="${attachList}"
								varStatus="listStatus">
							<p class="file">
								<console:fn func="getFileIcon" value="${attach.REAL_FILE_NAME}"/>
								<a href="#" onclick="fncDown('<c:out value="${attach.MENU_SEQN}"/>','<c:out value="${attach.BORD_SEQN}"/>','<c:out value="${attach.ATTC_SEQN}"/>');return false;"><c:out value="${attach.FILE_NAME}"/></a>
								<span>(파일크기: <console:fn func="strFileSize" value="${attach.FILE_SIZE}"/>)</span>
							</p>
							</c:forEach>
						</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-footer" style="text-align:right">
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoUpdateForm();return false;">수정</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoDelete();return false;">삭제</button></div>
		<div class="btn-group"><button type="submit" class="btn btn-primary" onclick="fncGoList();">목록</button></div>
	</div>
</div>