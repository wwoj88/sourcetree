<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="java.util.List"%>
<%
  	String submitChek = request.getParameter("submitChek") == null ? "" : request.getParameter("submitChek");
	User user = SessionUtil.getSession(request);
	String menuSeqn = request.getParameter("menuSeqn");
	String sessUserIdnt = user.getUserIdnt();
	String srchGenreDivs = request.getParameter("srchGenreDivs") == null ? "" : request.getParameter("srchGenreDivs");
	String srchStatDivs = request.getParameter("srchStatDivs") == null ? "" : request.getParameter("srchStatDivs");
	String srchCommId = request.getParameter("srchCommId") == null ? "" : request.getParameter("srchCommId");
	String srchTitle = request.getParameter("srchTitle") == null ? "" : request.getParameter("srchTitle");
	String srchRgstName = request.getParameter("srchRgstName") == null ? "" : request.getParameter("srchRgstName");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
	String rgstIdnt  = request.getParameter("rgstIdnt") == null ? "" : request.getParameter("rgstIdnt");
	
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>참여마당(개인저작물) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--


function fn_goRegi() {
	
	// 로그인확인
	var userIdnt = "<%=(String) session.getAttribute("sessUserIdnt")%>";
	
	if(userIdnt=='null' ||userIdnt=='')
	{
		alert('로그인이 필요한 화면입니다.');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}

	var frm = document.form1;
	frm.page_no.value = 1;
	frm.method = "post";
	frm.action = "/worksPriv/worksPriv.do?method=goRegi";
	frm.submit();
}


function worksPriv_search() {
	var frm = document.form1;
	frm.page_no.value = 1;
	frm.submit();
}

function worksPrivDetail(crId){
	var frm = document.form1;
	frm.crId.value = crId;
	frm.page_no.value = '<%=page_no%>';
	frm.method = "post";
	frm.action = "/worksPriv/worksPriv.do?method=goView";
	frm.submit();
}
  
function goPage(pageNo){
	var frm = document.form1;
	frm.page_no.value = pageNo;
	frm.submit();
}


function fn_enterCheck(obj){
  if (event.keyCode == 13) {
	  board_search();
  }
}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(5);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_5.gif" alt="참여마당" title="참여마당" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/worksPriv/worksPriv.do?menuSeqn=1"><img src="/images/2011/content/sub_lnb0501_off.gif" title="개인저작물" alt="개인저작물" /></a></li>
					<li id="lnb2"><a href="/board/board.do?menuSeqn=7&amp;page_no=1"><img src="/images/2011/content/sub_lnb0502_off.gif" title="이벤트" alt="이벤트" /></a></li>
					<li id="lnb3"><a href="/main/main.do?method=goSiteList"><img src="/images/2011/content/sub_lnb0503_off.gif" title="관련사이트 안내" alt="관련사이트 안내" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb1");</script>
				</div>
				<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>참여마당</span><em>개인저작물</em></p>
					<h1><img src="/images/2011/title/content_h1_0501.gif" alt="개인저작물" title="개인저작물" /></h1>
					
					<div class="section">
						<!-- 검색 -->
						<form name="form1" action="#">
						<input type="hidden" name="page_no" />
					    <input type="hidden" name="crId" />
					    <input type="hidden" name="menuSeqn" value="<%=menuSeqn%>" />
							<fieldset class="w100">
							<legend></legend>
								<div class="boxStyle">
									<div class="box1 floatDiv">
										<p class="fl mt5 w15"><img src="/images/2011/content/sch_txt.gif" alt="Search" title="Search"></p>
										<table class="fl schBoxGrid w70" summary="">
											<caption></caption>
											<colgroup><col width="10%"><col width="35%"><col width="15%"><col width="*"></colgroup>
											<tbody>
												<tr>
													<th scope="row"><label for="app1">분야</label></th>
													<td>
														<select id="app1" name="srchGenreDivs">
															<c:set var="genreCode" value="<%=srchGenreDivs%>" />
															<option value="">- 전체 -</option>
															<c:forEach items="${genreCodeList}" var="genreCodeList">
																	<option value="${genreCodeList.code}"  <c:if test="${genreCode == genreCodeList.code}">selected='selected'</c:if> >${genreCodeList.codeName}</option>
															</c:forEach>
														</select>
														</td>
													<th scope="row"><label for="sch1">ICN 번호</label></th>
													<td><input type="text"  id="sch1" name="srchCommId" value="<%=srchCommId%>" size="35" onkeyup="fn_enterCheck(this)" title="" class="inputData w75" /></td>
												</tr>
												<tr>
													<th scope="row"><label for="sch2">저작물명</label></th>
													<td><input type="text" id="sch2" name="srchTitle" value="<%=srchTitle%>" size="35" onkeyup="fn_enterCheck(this)" title="" class="inputData w70"></td>
													<th scope="row"><label for="sch3">저작권등록자</label></th>
													<td><input type="text" id="sch3" name="srchRgstName" value="<%=srchRgstName%>" size="35" onkeyup="fn_enterCheck(this)" title="" class="inputData w75"></td>
												</tr>
											</tbody>
										</table>
										<p class="fl btn_area"><input type="image" src="/images/2011/button/sch.gif" onclick = "javascript:worksPriv_search();" alt="검색" title="검색">
									</p></div>
									<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
								</div>
							</fieldset>
						</form>
						<!-- //검색 -->
						
						<!-- 테이블 리스트 Set -->
						<div class="section mt20">
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="8%">
								<col width="*">
								<col width="10%">
								<col width="15%">
								<col width="20%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">저작물명</th>
										<th scope="col">분야</th>
										<th scope="col">저작권등록자</th>
										<th scope="col">ICN 번호</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${worksPrivList.totalRow == 0}">
										<tr>
											<td class="ce" colspan="5"> 등록된 게시물이 없습니다. </td>
										</tr>
									</c:if>
									<c:if test="${worksPrivList.totalRow > 0}">
									<c:forEach items="${worksPrivList.resultList}" var="worksPriv">
								    <c:set var="NO" value="${worksPriv.TOTAL_CNT}"/>
							        <c:set var="i" value="${i+1}"/>
									<tr>
											<td class="ce"><c:out value="${NO - i}"/></td>
											<td><a href="#1" onclick="javascript:worksPrivDetail('${worksPriv.CR_ID}')">${worksPriv.TITLE}</a></td>
											<td class="ce">${worksPriv.GENRE_NAME}</td>
											<td class="ce">${worksPriv.RGST_NAME}</td>
											<td class="ce">${worksPriv.COMM_ID}</td>
										</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<!-- 페이징 -->
							<div class="pagination">
							<ul>
								<li>
						 		<%-- 페이징 리스트 --%>
								  <jsp:include page="../common/PageList_2011.jsp" flush="true">
									  <jsp:param name="totalItemCount" value="${worksPrivList.totalRow}" />
										<jsp:param name="nowPage"        value="${param.page_no}" />
										<jsp:param name="functionName"   value="goPage" />
										<jsp:param name="listScale"      value="" />
										<jsp:param name="pageScale"      value="" />
										<jsp:param name="flag"           value="M01_FRONT" />
										<jsp:param name="extend"         value="no" />
									</jsp:include>
								</li>	
							</ul>
						</div>
							<!-- //페이징 -->
							<div class="btnArea">
								<p class="rgt"><span class="button medium"><a href="#1" onclick="javascript:fn_goRegi();">개인저작물 등록</a></span></p>
							</div>
						</div>
						<!-- //테이블 리스트 Set -->
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
