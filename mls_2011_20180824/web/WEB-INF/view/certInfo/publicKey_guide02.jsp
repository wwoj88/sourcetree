<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="pragma" content="no-cache"/>
<title>저작권찾기 공인인증서 발급 안내</title>
<link href="/css/popup.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="wrap">
	<div class="popupTitle">
		<h2>사업자용 공인인증서 발급</h2>
	</div>
	<div class="popupContents">
		<div class="box"><img src="/images/sub/public_02img.gif" alt="" class="floatR"/>공인인증서는 정부가 믿고 찾는 한국정보인증에서 발급 받을 수 있습니다!<br /><br />우리나라 제1호 공인인증기관인 한국정보인증은 저작권위원회의 저작권 관련 서비스를 이용하시는 고객에게 공인잉증서를 발급하고 있습니다.</div>
		<div>
			<h3>편리한 찾아가는 서비스(신청후 발급까지 3~4일이 소요됩니다)</h3>
			<p style="padding:15px;"><img src="/images/sub/public_process01.gif" alt="절차" /></p>
			
		</div>
		<div>
			<h3>제출서류안내</h3>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="grid">
				<thead>
					<tr>
						<th class="tdLabel">대표자 보인 신청시(인감증명서와 동일한 인감 날인)</th>
						<th class="thTitle">대리인 신청시(인감증명서와 동일한 인감 날인)</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="tdData"><ul>
							<li>1.공인인증서비스 신청서 1부</li>
						<li>2.사업자등록증 사본 1부</li>
						<li>3.대표자 신분증 앞/뒤 사본 1부(원본지참)</li>
						</ul>
						</td>
						<td class="tdData"><ul>
							<li>1.공인인증서비스 신청서 1부</li>
						<li>2.사업자등록증 사본 1부</li>
						<li>3.개인/법인 인감증명서 원본 1부</li>
						<li>4.대리인 신분증 앞/뒤 사본 1부(원본지참)</li>
						</ul></td>
					</tr>
				</tbody>
			</table>
			
			<a href="#1"><img src="/images/button/btn_publicApp.gif" alt="공인인증서 신청하기" width="166" height="31" /></a>
			<p>※ 고객께서 직접 방문하여 공인인증서를 신청하시기를 원하시면 한국정보인증 홈페이지<br /><a href="http://www.signgate.com" target="_blank">http://www.signgate.com</a>를 통해 안내를 받으실 수 있습니다.<br /></p>
		</div>
		<div class="popMsg"><span class="floatL">공인인증서 발급문의:1577-8787</span> <span class="floatR">Copyright 저작권위원회. All rights reserved.</span></div>
	</div>
	<div class="popupBottom"><a href="#1" onclick="javascript:window.close();"><img src="/images/button/close_btn.gif" alt="닫기" width="76" height="23" /></a></div>
</div>
</body>
</html>
