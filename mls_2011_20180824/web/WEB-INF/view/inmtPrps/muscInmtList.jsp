<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Map"%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금신청 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 8px 0;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	width: 700px;
	margin-left: 0px;
}
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
<script type="text/JavaScript">
//<!--


var emptyYn = '${emptyYn}'; // 검색조건이 없을 경우 //20120220 정병호

//페이징
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
		},
		onSuccess: function(req) {     
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.target = "_self";
	frm.method = "post";
	frm.action = "/inmtPrps/inmtPrps.do?method=muscInmtList";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	var totCnt = cfInsertComma('${inmtList.totalRow}');
	
	if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
		window.parent.document.getElementById("spn_totalRow").innerHTML = totCnt + '건';
	}
}

// 보상금 history오픈 : 부모창에서 오픈
function openInmtPrps(crId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	
	var url = '/inmtPrps/inmtPrps.do?method=inmtPrpsHistList&DIVS=B'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch, width=500, height=500';
	
	parent.frames.openDetail(url, name, openInfo);
}

//table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	var the_height = document.getElementById(name).offsetHeight;
	document.getElementById(targetName).style.height = the_height+13;
}

function initParameter(){
	resizeDiv("tab_scroll", "div_scroll");
	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifMuscInmtList"); 
	
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	parent.frames.scrollTo(0,0);
	
	/* console.log("test");
	console.log(document.getElementsByClassName('ALLT_AMNT'))
	console.log($("[name='ALLT_AMNT']"));
	console.log($("#test_id")); */
	
	var len = document.getElementsByName('ALLT_AMNT').length;
	console.log("len : " + len)
	
	for(var i = 0 ;  i < len ; i++){
		//console.log("comma(document.getElementsByName('ALLT_AMNT')[i].innerHTML) : " + comma(document.getElementsByName('ALLT_AMNT')[i].innerHTML.trim()))
		document.getElementsByName('ALLT_AMNT')[i].innerHTML = comma(document.getElementsByName('ALLT_AMNT')[i].innerHTML.trim()) + "원"
	}

}

if(window.attachEvent){
	window.attachEvent("onload", initParameter);
	window.attachEvent("onload", fn_totalCNT);
}else if(window.addEventListener) {
	window.addEventListener("load", initParameter, false);
	window.addEventListener("load", fn_totalCNT, false);
}else{ 
	window.onload = initParameter;
} 


function comma(num){
    var len, point, str; 
       
    num = num + ""; 
    point = num.length % 3 ;
    len = num.length; 
   
    str = num.substring(0, point); 
    while (point < len) { 
        if (str != "") str += ","; 
        str += num.substring(point, point + 3); 
        point += 3; 
    } 
     
    return str;
 
}


//-->
</script>
</head>
<body>
		
		
			
		<form name="ifFrm" action="#">
			<input type="hidden" name="page_no"/>
			<input type="hidden" name="srchDIVS" value="${srchParam.srchDIVS }"/>
			<input type="hidden" name="srchSdsrName" value="${srchParam.srchSdsrName }"/>
			<input type="hidden" name="srchMuciName" value="${srchParam.srchMuciName }"/>
			<input type="hidden" name="srchYymm" value="${srchParam.srchYymm }"/>
			<input type="hidden" name="alltAmnt" value="${srchParam.ALLT_AMNT }"/>
			<input type="hidden" name="latterAmnt" value="${srchParam.LATTER_AMNT }"/>
			<input type="hidden" name="sort" value="${srchParam.SORT }"/>
			<input type="hidden" name="trstOrgnCode" value="${trstOrgnCode }"/>
			<input type="submit" style="display:none;">
			<!-- 테이블 영역입니다 -->
			<div id="div_scroll"  class="tabelRound" style="width:100%;height:auto;padding:0px;">
				<table id="tab_scroll" class="sub_tab3 mar_tp40" cellspacing="0" cellpadding="0" width="100%" summary="방송음악보상금 발생 저작물 목록입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
					<colgroup>
						<col width="5%"/>
						<col width="15%"/>
						<col width="10%"/>
						<col width="10%"/>
						<col width="10%"/>
						<col width="*"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col">순번</th>
							<th scope="col">곡명</th>
							<th scope="col">가수</th>
							<th scope="col">앨범명</th>
							<th scope="col">제공 및<br> 확인기관</th>
							<!--<th scope="col">미분배<br>보상금</th>-->
							<th scope="col">분배<br>공고년도</th>
							<th scope="col">주실연자</th>
							<th scope="col">부실연자</th>
							<th scope="col">지휘자</th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${emptyYn == 'Y'}"><!-- 검색조건이 없을 경우 //20120220 정병호 -->
						<tr>
							<td class="ce" colspan="5">검색조건을 입력해 주세요.</td>
						</tr>
					</c:if>
					<c:if test="${empty inmtList.resultList && emptyYn != 'Y'}">
						<tr>
							<td class="ce"colspan="5">검색된 목록이 없습니다.</td>
						</tr>
					</c:if>
					<c:if test="${!empty inmtList.resultList}">
						<c:forEach items="${inmtList.resultList}" var="inmtPrps">
							<c:set var="NO" value="${inmtPrps.totalRow}"/>
							<c:set var="i" value="${i+1}"/>
						<tr>
							<c:choose>
							 	<c:when test="${inmtPrps.ROW_NUM % 2 == 0}">
							 		<td class="ce ev_num">${inmtPrps.ROW_NUM}</td>
							 	</c:when>
							 	<c:otherwise>
							 		<td class="ce">${inmtPrps.ROW_NUM}</td>
							 	</c:otherwise>
							</c:choose>	
							<c:choose>
							 	<c:when test="${inmtPrps.ROW_NUM % 2 == 0}">
							 		<td class="ev_num">
										<!-- hidden value start -->
										<input type="hidden" name="inmtSeqn" value="${inmtPrps.INMT_SEQN}"/>
										<input type="hidden" name="sdsrName" value="${inmtPrps.SDSR_NAME}"/>
										<input type="hidden" name="muciName" value="${inmtPrps.MUCI_NAME}"/>
										<input type="hidden" name="yymm" value="${inmtPrps.YYMM}"/>
										<input type="hidden" name="divs" value="${inmtPrps.DIVS}"/>
										<input type="hidden" name="prpsDivs" value="${inmtPrps.PRPS_DIVS}"/>
										<input type="hidden" name="kapp" value="${inmtPrps.KAPP}"/>
										<input type="hidden" name="fokapo" value="${inmtPrps.FOKAPO}"/>
										<input type="hidden" name="krtra" value="${inmtPrps.KRTRA}"/>
										<input type="hidden" name="oferEtpr" value="${inmtPrps.OFER_ETPR}"/>
										<input type="hidden" name="brctCont" value="${inmtPrps.BRCT_CONT}"/>
										<input type="hidden" name="lyriWrtr" value="${inmtPrps.LYRI_WRTR}"/>
										<input type="hidden" name="comsWrtr" value="${inmtPrps.COMS_WRTR}"/>
										<input type="hidden" name="arrgWrtr" value="${inmtPrps.ARRG_WRTR}"/>
										<input type="hidden" name="albmName" value="${inmtPrps.ALBM_NAME}"/>
										<input type="hidden" name="duesCode" value="${inmtPrps.DUES_CODE}"/>
										<input type="hidden" name="dataType" value="${inmtPrps.DATA_TYPE}"/>
										<input type="hidden" name="usexType" value="${inmtPrps.USEX_TYPE}"/>
										<input type="hidden" name="selgYsno" value="${inmtPrps.SELG_YSNO}"/>
										<input type="hidden" name="usexLibr" value="${inmtPrps.USEX_LIBR}"/>
										<input type="hidden" name="ouptPage" value="${inmtPrps.OUPT_PAGE}"/>
										<input type="hidden" name="ctrlNumb" value="${inmtPrps.CTRL_NUMB}"/>
										<input type="hidden" name="lishComp" value="${inmtPrps.LISH_COMP}"/>
										<input type="hidden" name="workCode" value="${inmtPrps.WORK_CODE}"/>
										<input type="hidden" name="caryDivs" value="${inmtPrps.CARY_DIVS}"/>
										<input type="hidden" name="schl" value="${inmtPrps.SCHL}"/>
										<input type="hidden" name="bookDivs" value="${inmtPrps.BOOK_DIVS}"/>
										<input type="hidden" name="bookSizeDivs" value="${inmtPrps.BOOK_SIZE_DIVS}"/>
										<input type="hidden" name="schlYearDivs" value="${inmtPrps.SCHL_YEAR_DIVS}"/>
										<input type="hidden" name="pubcCont" value="${inmtPrps.PUBC_CONT}"/>
										<input type="hidden" name="autrDivs" value="${inmtPrps.AUTR_DIVS}"/>
										<input type="hidden" name="workKind" value="${inmtPrps.WORK_KIND}"/>
										<input type="hidden" name="usexPage" value="${inmtPrps.USEX_PAGE}"/>
										<input type="hidden" name="workDivs" value="${inmtPrps.WORK_DIVS}"/>
										<input type="hidden" name="subjName" value="${inmtPrps.SUBJ_NAME}"/>
										<!-- hidden value end -->
										${inmtPrps.SDSR_SNAME }
									</td>
							 	</c:when>
							 	<c:otherwise>
							 		<td>
								<!-- hidden value start -->
										<input type="hidden" name="inmtSeqn" value="${inmtPrps.INMT_SEQN}"/>
										<input type="hidden" name="sdsrName" value="${inmtPrps.SDSR_NAME}"/>
										<input type="hidden" name="muciName" value="${inmtPrps.MUCI_NAME}"/>
										<input type="hidden" name="yymm" value="${inmtPrps.YYMM}"/>
										<input type="hidden" name="divs" value="${inmtPrps.DIVS}"/>
										<input type="hidden" name="prpsDivs" value="${inmtPrps.PRPS_DIVS}"/>
										<input type="hidden" name="kapp" value="${inmtPrps.KAPP}"/>
										<input type="hidden" name="fokapo" value="${inmtPrps.FOKAPO}"/>
										<input type="hidden" name="krtra" value="${inmtPrps.KRTRA}"/>
										<input type="hidden" name="oferEtpr" value="${inmtPrps.OFER_ETPR}"/>
										<input type="hidden" name="brctCont" value="${inmtPrps.BRCT_CONT}"/>
										<input type="hidden" name="lyriWrtr" value="${inmtPrps.LYRI_WRTR}"/>
										<input type="hidden" name="comsWrtr" value="${inmtPrps.COMS_WRTR}"/>
										<input type="hidden" name="arrgWrtr" value="${inmtPrps.ARRG_WRTR}"/>
										<input type="hidden" name="albmName" value="${inmtPrps.ALBM_NAME}"/>
										<input type="hidden" name="duesCode" value="${inmtPrps.DUES_CODE}"/>
										<input type="hidden" name="dataType" value="${inmtPrps.DATA_TYPE}"/>
										<input type="hidden" name="usexType" value="${inmtPrps.USEX_TYPE}"/>
										<input type="hidden" name="selgYsno" value="${inmtPrps.SELG_YSNO}"/>
										<input type="hidden" name="usexLibr" value="${inmtPrps.USEX_LIBR}"/>
										<input type="hidden" name="ouptPage" value="${inmtPrps.OUPT_PAGE}"/>
										<input type="hidden" name="ctrlNumb" value="${inmtPrps.CTRL_NUMB}"/>
										<input type="hidden" name="lishComp" value="${inmtPrps.LISH_COMP}"/>
										<input type="hidden" name="workCode" value="${inmtPrps.WORK_CODE}"/>
										<input type="hidden" name="caryDivs" value="${inmtPrps.CARY_DIVS}"/>
										<input type="hidden" name="schl" value="${inmtPrps.SCHL}"/>
										<input type="hidden" name="bookDivs" value="${inmtPrps.BOOK_DIVS}"/>
										<input type="hidden" name="bookSizeDivs" value="${inmtPrps.BOOK_SIZE_DIVS}"/>
										<input type="hidden" name="schlYearDivs" value="${inmtPrps.SCHL_YEAR_DIVS}"/>
										<input type="hidden" name="pubcCont" value="${inmtPrps.PUBC_CONT}"/>
										<input type="hidden" name="autrDivs" value="${inmtPrps.AUTR_DIVS}"/>
										<input type="hidden" name="workKind" value="${inmtPrps.WORK_KIND}"/>
										<input type="hidden" name="usexPage" value="${inmtPrps.USEX_PAGE}"/>
										<input type="hidden" name="workDivs" value="${inmtPrps.WORK_DIVS}"/>
										<input type="hidden" name="subjName" value="${inmtPrps.SUBJ_NAME}"/>
										<!-- hidden value end -->
										${inmtPrps.SDSR_SNAME }
									</td>
							 	</c:otherwise>
							</c:choose>	
							<c:choose>
							 	<c:when test="${inmtPrps.ROW_NUM % 2 == 0}">
							 		<td class="ce ev_num">${inmtPrps.MUCI_NAME }</td>
							 	</c:when>
							 	<c:otherwise>
							 		<td class="ce">${inmtPrps.MUCI_NAME }</td>
							 	</c:otherwise>
							</c:choose>	
							<c:choose>
							 	<c:when test="${inmtPrps.ROW_NUM % 2 == 0}">
							 		<td class="ce ev_num">${inmtPrps.ALBM_NAME }</td>
							 	</c:when>
							 	<c:otherwise>
							 		<td class="ce">${inmtPrps.ALBM_NAME }</td>
							 	</c:otherwise>
							</c:choose>	
							<c:choose>
							 	<c:when test="${inmtPrps.ROW_NUM % 2 == 0}">
							 		<td class="ce ev_num">
										<c:if test="${inmtPrps.TRST_ORGN_CODE == '203'}">
										<!-- 한국음반산업협회 -->음산협
										</c:if>
										<c:if test="${inmtPrps.TRST_ORGN_CODE == '202'}">
										<!-- 한국음악실연자연합회 -->음실련
										</c:if>
									</td>
							 	</c:when>
							 	<c:otherwise>
							 		<td class="ce">
										<c:if test="${inmtPrps.TRST_ORGN_CODE == '203'}">
										<!-- 한국음반산업협회 -->음산협
										</c:if>
										<c:if test="${inmtPrps.TRST_ORGN_CODE == '202'}">
										<!-- 한국음악실연자연합회 -->음실련
										</c:if>
									</td>
							 	</c:otherwise>
							</c:choose>	
							<!--<c:choose>
							 	<c:when test="${inmtPrps.ROW_NUM % 2 == 0}">
							 		<td id="test_id" name="ALLT_AMNT" class="ce ev_num">
										<c:if test="${inmtPrps.ALLT_AMNT == '' || inmtPrps.ALLT_AMNT ne null }">
											${inmtPrps.ALLT_AMNT}
										</c:if>								
									</td>
							 	</c:when>
							 	<c:otherwise>
							 		<td id="ALLT_AMNT" name="ALLT_AMNT" class="ce">
										<c:if test="${inmtPrps.ALLT_AMNT == '' || inmtPrps.ALLT_AMNT ne null }">
											${inmtPrps.ALLT_AMNT}
										</c:if>
									</td>
									
							 	</c:otherwise>
							</c:choose>	 -->
							<c:choose>
							 	<c:when test="${inmtPrps.ROW_NUM % 2 == 0}">
							 		<td class="ce ev_num">
										<c:if test="${inmtPrps.YYMM != '' || inmtPrps.YYMM ne null }">
											${inmtPrps.YYMM}
										</c:if>								
									</td>
							 	</c:when>
							 	<c:otherwise>
							 		<td class="ce">
											<c:if test="${inmtPrps.YYMM != '' || inmtPrps.YYMM ne null }">
											${inmtPrps.YYMM}
										</c:if>
									</td>
							 	</c:otherwise>
							</c:choose>
							<c:choose>
							 	<c:when test="${inmtPrps.ROW_NUM % 2 == 0}">
							 		<td class="ce ev_num">
										<c:if test="${inmtPrps.MAIN_PERFORMERS_STATE != '' || inmtPrps.MAIN_PERFORMERS_STATE ne null }">
											${inmtPrps.MAIN_PERFORMERS_STATE}
										</c:if>	
										<c:if test="${inmtPrps.MAIN_PERFORMERS_STATE == '' || inmtPrps.MAIN_PERFORMERS_STATE eq null }">
											-
										</c:if>								
									</td>
							 	</c:when>
							 	<c:otherwise>
							 		<td class="ce">
											<c:if test="${inmtPrps.MAIN_PERFORMERS_STATE != '' || inmtPrps.MAIN_PERFORMERS_STATE ne null }">
											${inmtPrps.MAIN_PERFORMERS_STATE}
										</c:if>
											<c:if test="${inmtPrps.MAIN_PERFORMERS_STATE == '' || inmtPrps.MAIN_PERFORMERS_STATE eq null }">
											-
										</c:if>
									</td>
							 	</c:otherwise>
							</c:choose>		
							<c:choose>
							 	<c:when test="${inmtPrps.ROW_NUM % 2 == 0}">
							 		<td class="ce ev_num">
										<c:if test="${inmtPrps.SUB_PERFORMERS_STATE != '' || inmtPrps.SUB_PERFORMERS_STATE ne null }">
											${inmtPrps.SUB_PERFORMERS_STATE}
										</c:if>
										<c:if test="${inmtPrps.SUB_PERFORMERS_STATE == '' || inmtPrps.SUB_PERFORMERS_STATE eq null }">
											-
										</c:if>																
									</td>
							 	</c:when>
							 	<c:otherwise>
							 		<td class="ce">
											<c:if test="${inmtPrps.SUB_PERFORMERS_STATE != '' || inmtPrps.SUB_PERFORMERS_STATE ne null }">
											${inmtPrps.SUB_PERFORMERS_STATE}
										</c:if>
										<c:if test="${inmtPrps.SUB_PERFORMERS_STATE == '' || inmtPrps.SUB_PERFORMERS_STATE eq null }">
											-
										</c:if>			
									</td>
							 	</c:otherwise>
							</c:choose>	
							<c:choose>
							 	<c:when test="${inmtPrps.ROW_NUM % 2 == 0}">
							 		<td class="ce ev_num">
										<c:if test="${inmtPrps.CONDUCTOR_STATE != '' || inmtPrps.CONDUCTOR_STATE ne null }">
											${inmtPrps.CONDUCTOR_STATE}
										</c:if>		
										<c:if test="${inmtPrps.CONDUCTOR_STATE == '' || inmtPrps.CONDUCTOR_STATE eq null }">
											-
										</c:if>								
									</td>
							 	</c:when>
							 	<c:otherwise>
							 		<td class="ce">
											<c:if test="${inmtPrps.CONDUCTOR_STATE != '' || inmtPrps.CONDUCTOR_STATE ne null }">
											${inmtPrps.CONDUCTOR_STATE}
										</c:if>
											<c:if test="${inmtPrps.CONDUCTOR_STATE == '' || inmtPrps.CONDUCTOR_STATE eq null }">
											-
										</c:if>
									</td>
							 	</c:otherwise>
							</c:choose>	
						</tr>
						</c:forEach>
					</c:if>
					</tbody>
				</table>
				<%-- <div>${srchParam.trstOrgnCode }</div> --%>
				</div>
				<p class="clear"></p>
				<br/>
				<!--paging start-->
				<div class="pagination" style="text-align: center;">
					<jsp:include page="../common/PageList_2011.jsp" flush="true">
						<jsp:param name="totalItemCount" value="${inmtList.totalRow}" />
						<jsp:param name="nowPage"        value="${param.page_no}" />
						<jsp:param name="functionName"   value="goPage" />
						<jsp:param name="listScale"      value="" />
						<jsp:param name="pageScale"      value="" />
						<jsp:param name="flag"           value="M01_FRONT" />
						<jsp:param name="extend"         value="no" />
					</jsp:include>
				</div>
				<!--paging end-->
			<!--contents end-->
		</form>
</body>
</html>

