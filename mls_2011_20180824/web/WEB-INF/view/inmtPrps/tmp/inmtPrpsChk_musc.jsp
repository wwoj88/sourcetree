<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld"%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<style type="text/css">
<!--
.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript">
<!--
	//목록으로 이동
	function fn_list(){
		var frm = document.frm;
	
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=list&amp;gubun=edit&amp;srchDIVS=${srchParam.srchDIVS }";
		frm.submit();
	}
	
	//뒤로가기
	function fn_prePage(){
		var frm = document.frm;
	
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps";
		frm.submit();
	}
	
	
	//보상금신청
	function fn_chkSubmit(){
		var frm = document.frm;
		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrpsSave";
		frm.submit();
	}
	
	
	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.frm;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;
	
		frm.method = "post";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
	}

	// table name 사이즈에 대한 div targetName리사이즈
	//function resizeDiv(name) {
	//	var the_height = document.getElementById(name).offsetHeight;
	//	document.getElementById(name).style.height = the_height + 13  + "px" ;
	//}
	
	//table name 사이즈에 대한 div targetName리사이즈
	function resizeDiv(name, targetName) {
		var the_height = document.getElementById(name).offsetHeight;
		document.getElementById(targetName).style.height = the_height+13+ "px" ;
	}
	
//-->
</script>
</head>

<body>
<!-- 전체를 감싸는 DIVISION -->
<div id="wrap"><!-- HEADER str--> <jsp:include
	page="/include/2010/header.jsp" /> <script type="text/javascript">initNavigation(-1);</script>
<!-- GNB setOn 각페이지에 넣어야합니다. --> <!-- HEADER end --> <!-- CONTAINER str-->
<div id="container">
<div class="content">
<div class="left">
<h2><img src="/images/2010/common/left_h2_2.png" alt="보상금신청"
	title="보상금신청" class="png24" /></h2>
<!-- left menu str -->
<div class="sMenu">
<ul>
	<li class="active"><a
		href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악<span>&nbsp;</span></a></li>
	<li><a
		href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용<span>&nbsp;</span></a></li>
	<li><a
		href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관<span>&nbsp;</span></a></li>
</ul>
</div>
<!-- left menu end --></div>
<div class="contentBody" id="contentBody">
<p class="path"><span class="skip">현재위치 : </span><span><img
	src="/images/2010/common/path_h.gif" alt="Home" class="vmid" />&nbsp;</span>&gt;<span>보상금신청</span>&gt;<strong>방송음악</strong></p>
<div class="subVis">
<p class="rgt"><img src="/images/2010/common/top_visual.png"
	class="png24" alt="탑이미지" /></p>
<h3>보상금 신청 <span><img src="/images/2010/common/typoBg.gif"
	class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." />&nbsp;</span></h3>
</div>

<form name="frm" action="" class="sch"><input type="hidden"
	name="srchDIVS" value="${srchParam.srchDIVS }" /> <input type="hidden"
	name="srchSdsrName" value="${srchParam.srchSdsrName }" /> <input
	type="hidden" name="srchMuciName" value="${srchParam.srchMuciName }" />
<input type="hidden" name="srchYymm" value="${srchParam.srchYymm }" />
<input type="hidden" name="gubun" value="back" /> <input type="hidden"
	name="nowPage" value="${srchParam.nowPage }" /> <input type="hidden"
	name="filePath"> <input type="hidden" name="fileName">
<input type="hidden" name="realFileName">

<!-- memo 삽입 -->
<jsp:include page="/common/memo/memo_01.jsp">
	<jsp:param name="DIVS" value="${srchParam.srchDIVS }"/>
</jsp:include>
<!-- // memo 삽입 -->
					
<div class="bbsSection">
<h4>방송음악보상금 신청</h4>
<fieldset><legend>보상금신청 신청자 정보</legend> <!-- 보상금신청(기본정보) 테이블 영역입니다 -->
<div class="tabelRound"><span class="round lt">&nbsp;</span><span
	class="round rt">&nbsp;</span><!-- 라운딩효과 -->
<table cellspacing="0" cellpadding="0" border="1" class="grid topLine"
	summary="보상금 신청정보 입니다.">
	<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
	<colgroup>
		<col width="20%">
		<col width="*">
	</colgroup>
	<tbody>
		<tr>
			<th scope="row" class="bgbr lft">보상금종류</th>
			<td>방송음악</td>
		</tr>
		<tr>
			<th scope="row" class="bgbr lft">신청자</th>
			<td>
			<table cellspacing="0" cellpadding="0" border="1" class="grid"
				summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등 정보입니다.">
				<colgroup>
					<col width="15%">
					<col width="35%">
					<col width="15%">
					<col width="35%">
				</colgroup>
				<tbody>
					<tr>
						<th class="bgbr3 lft">성명</th>
						<td>${clientInfo.PRPS_NAME }</td>
						<th class="bgbr3 lft">주민번호/사업자번호</th>
						<td>${clientInfo.RESD_CORP_NUMB_VIEW}</td>
					</tr>
					<tr>
						<th class="bgbr3 lft">전화</th>
						<td><label class="labelBlock"><em class="w45">자택</em>${clientInfo.HOME_TELX_NUMB
						}</label> <label class="labelBlock"><em class="w45">사무실</em>${clientInfo.BUSI_TELX_NUMB
						}</label> <label class="labelBlock"><em class="w45">휴대폰</em>${clientInfo.MOBL_PHON
						}</label></td>
						<th class="bgbr3 lft">Fax</th>
						<td>${clientInfo.FAXX_NUMB }</td>
					</tr>
					<tr>
						<th class="bgbr3 lft">이메일주소</th>
						<td>${clientInfo.MAIL }</td>
						<th class="bgbr3 lft">입금처</th>
						<td><label class="labelBlock"><em class="w50">은행명</em>${clientInfo.BANK_NAME}</label>
						<label class="labelBlock"><em class="w50">계좌번호</em>${clientInfo.ACCT_NUMB
						}</label> <label class="labelBlock"><em class="w50">예금주</em>${clientInfo.DPTR
						}</label></td>
					</tr>
					<tr>
						<th class="bgbr3 lft">주소</th>
						<td colspan="3" class="vtop"><label class="labelBlock"><em
							class="w45">자택</em>${clientInfo.HOME_ADDR }</label> <label
							class="labelBlock"><em class="w45">사무실</em>${clientInfo.BUSI_ADDR
						}</label></td>
					</tr>
				</tbody>
			</table>

			<!-- 전화면에서 넘어온 값듯 --> 
			<input type="hidden" name="hddnInsertKapp" 	value="${clientInfo.KAPP}" /> 
			<input type="hidden"	name="hddnInsertFokapo" value="${clientInfo.FOKAPO}" /> 
			<input type="hidden" name="hddnUserIdnt" value="${clientInfo.USER_IDNT }" />
			<input type="hidden" name="txtPrpsName"	value="${clientInfo.PRPS_NAME }" /> 
			<input type="hidden"	name="txtPrsdCorpNumbView" value="${clientInfo.RESD_CORP_NUMB }" />
			<input type="hidden" name="txtHomeTelxNumb"	value="${clientInfo.HOME_TELX_NUMB }" /> 
			<input type="hidden"	name="txtBusiTelxNumb" value="${clientInfo.BUSI_TELX_NUMB }" /> 
			<input type="hidden" name="txtMoblPhon" value="${clientInfo.MOBL_PHON }" />
			<input type="hidden" name="txtFaxxNumb" 	value="${clientInfo.FAXX_NUMB }" /> 
			<input type="hidden"	name="txtHomeAddr" value="${clientInfo.HOME_ADDR }" /> 
			<input type="hidden" name="txtBusiAddr" value="${clientInfo.BUSI_ADDR }" />
			<input type="hidden" name="txtBankName" 	value="${clientInfo.BANK_NAME }" /> 
			<input type="hidden"	name="txtAcctNumb" value="${clientInfo.ACCT_NUMB }" /> 
			<input type="hidden" name="txtDptr" value="${clientInfo.DPTR }" /> 
			<input type="hidden" name="txtMail" value="${clientInfo.MAIL }" /> 
			<input type="hidden" name="hddnTrst203" value="${clientInfo.TRST_203 }" />
			<input type="hidden" name="hddnTrst202"	value="${clientInfo.TRST_202 }" /> 
			<input type="hidden"	name="hddnTrst205" value="${clientInfo.TRST_205 }" /> 
			<input type="hidden" name="hddnTrst205_2" value="${clientInfo.TRST_205_2 }" />
			<input type="hidden" name="txtareaPrpsDesc" value="${clientInfo.PRPS_DESC }" />
			<input type="hidden" name="prpsDoblCode"	value="${prpsDoblCode }" />
			<input type="hidden" name="rghtPrpsMastKey"	value="${rghtPrpsMastKey }" />	
			
			</td>
		</tr>
		<tr>
			<th scope="row" class="bgbr lft">신청분류</th>
			<td><label> <input type="checkbox"
				<c:if test="${clientInfo.KAPP == 'Y'}">checked</c:if> disabled
				class="inputRChk" title="저작인접권(앨범제작)" />저작인접권(앨범제작) </label> <label> <input
				type="checkbox"
				<c:if test="${clientInfo.FOKAPO == 'Y'}">checked</c:if>
				class="inputRChk ml10" disabled title="저작인접권(가창,연주,지휘 등)"  />저작인접권(가창, 연주, 지휘 등) </label></td>
		</tr>
		<tr>
			<th scope="row" class="bgbr lft">신청저작물</th>
			<td><!-- 테이블 영역입니다 -->
			<div class="tabelRound overflow_y h100">
			<table cellspacing="0" cellpadding="0" border="1" class="grid w95"
				summary="보상금신청 저작물 정보 입니다.">
				<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
					<col width="8%">
					<col width="52%">
					<col width="25%">
					<col width="15%">
				</colgroup>
				<thead>
					<tr>
						<th scope="col" class="headH">순번</th>
						<th scope="col" class="headH">곡명</th>
						<th scope="col" class="headH">가수</th>
						<th scope="col" class="headH">방송년도</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${!empty tempList}">
						<c:forEach items="${tempList}" var="tempList">
							<c:set var="NO" value="${tempListCnt + 1}" />
							<c:set var="i" value="${i+1}" />
							<tr>
								<td class="ce">${NO - i } <!-- 전화면에서 넘어온 값듯 --> <input
									type="hidden" name="hddnSelectInmtSeqn"
									value="${tempList.INMT_SEQN }" /> 
									<input type="hidden" name="hddnSelectPrpsIdntCode" value="${tempList.PRPS_IDNT_CODE }" />
									<input type="hidden"	name="hddnSelectPrpsDivs" value="${tempList.PRPS_DIVS }" /> 
									<input type="hidden" name="hddnSelectKapp" value="${tempList.KAPP }" />
									<input type="hidden" name="hddnSelectFokapo"	value="${tempList.FOKAPO }" /> 
									<input type="hidden"	name="hddnSelectKrtra" value="${tempList.KRTRA }" /> 
									<input type="hidden" name="hddnSelectSdsrName" value="${tempList.SDSR_NAME }" /> 
									<input type="hidden"	name="hddnSelectMuciName" value="${tempList.MUCI_NAME }" /> 
									<input type="hidden" name="hddnSelectYymm" value="${tempList.YYMM }" />
									<input type="hidden" name="hddnSelectLishComp"	value="${tempList.LISH_COMP }" /> 
									<input type="hidden"	name="hddnSelectUsexType" value="${tempList.USEX_TYPE }" />
								</td>
								<td>${tempList.SDSR_NAME }</td>
								<td class="ce">${tempList.MUCI_NAME }</td>
								<td class="ce">${tempList.YYMM }</td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
			</div>
			<!-- //테이블 영역입니다 --></td>
		</tr>
	</tbody>
</table>
</div>
<!-- //보상금신청(기본정보) 테이블 영역입니다 --></fieldset>
</div>

<c:if test="${clientInfo.KAPP =='Y'}">
	<c:if test="${!empty trstList}">
		<c:forEach items="${trstList}" var="trstList">
			<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
				<!-- 음제협 관련 영역 -->
				<div class="bbsSection">
				<div class="floatDiv">
				<h4 class="fl">내용(한국음원제작자협회) <span class="labelFont"> <label><input
					type="checkbox" class="inputChk ml10"
					<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if>
					disabled="disabled" title="오프라인접수(첨부서류)" />오프라인접수(첨부서류)</label> <input type="hidden"
					name="hddnKappOff" value="${trstList.OFFX_LINE_RECP }" /> </span></h4>
				</div>

				<!-- 보상금신청 항목 테이블 영역입니다 -->
				<div id="divKappItemList" class="tabelRound div_scroll"
					style="width: 745px; padding: 0 0 0 0;">
				<table id="tabKappItemList" cellspacing="0" cellpadding="0" border="1"
					class="grid topLine" summary="(한국음원제작자협회)보상금신청 저작물 정보 입니다."  style="width: 1000px;">
					<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
					<colgroup>
						<col width="25px">
						<col width="150px">
						<col width="100px">
						<col width="145px">
						<col width="80px">
						<col width="80px">
						<col width="80px">
						<col width="100px">
						<col width="80px">
						<col width="80px">
						<col width="80px">
					</colgroup>
					<thead>
						<tr>
							<th scope="col" class="headH">번호</th>
							<th scope="col" class="headH">음반명</th>
							<th scope="col" class="headH">CD코드</th>
							<th scope="col" class="headH">곡명</th>
							<th scope="col" class="headH">가수명</th>
							<th scope="col" class="headH">발매일</th>
							<th scope="col" class="headH">국가명</th>
							<th scope="col" class="headH">권리근거</th>
							<th scope="col" class="headH">작사가</th>
							<th scope="col" class="headH">작곡가</th>
							<th scope="col" class="headH">음악장르<BR>
							(테마코드)</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${!empty kappList}">
							<c:forEach items="${kappList}" var="kappList">
								<c:set var="kappNo" value="${kappNo+1}" />
								<tr>
									<td class="ce">${kappNo } <!-- 전화면에서 넘어온 값듯 --> <input
										type="hidden" name="hddnKappPrpsIdnt"
										value="${kappList.PRPS_IDNT }" /> <input
										type="hidden" name="hddnKappPrpsIdntCode"
										value="${kappList.PRPS_IDNT_CODE }" /> <input type="hidden"
										name="txtKappDiskName" value="${kappList.DISK_NAME }" /> <input
										type="hidden" name="txtKappCdCode"
										value="${kappList.CD_CODE }" /> <input type="hidden"
										name="hddnKappSongName" value="${kappList.SONG_NAME }" /> <input
										type="hidden" name="txtKappSnerName"
										value="${kappList.SNER_NAME }" /> <input type="hidden"
										name="txtKappDateIssu" value="${kappList.DATE_ISSU }" /> <input
										type="hidden" name="txtKappNatuName"
										value="${kappList.NATN_NAME }" /> <input type="hidden"
										name="txtKappRghtGrnd" value="${kappList.RGHT_GRND }" /> <input
										type="hidden" name="txtKappLyriWrtr"
										value="${kappList.LYRI_WRTR }" /> <input type="hidden"
										name="txtKappComsWrtr" value="${kappList.COMS_WRTR }" /> <input
										type="hidden" name="selKappMuscGnre"
										value="${kappList.MUSC_GNRE }" /></td>
									<td>${kappList.DISK_NAME }</td>
									<td>${kappList.CD_CODE }</td>
									<td>${kappList.SONG_NAME }</td>
									<td>${kappList.SNER_NAME }</td>
									<td>${kappList.DATE_ISSU }</td>
									<td>${kappList.NATN_NAME }</td>
									<td>${kappList.RGHT_GRND }</td>
									<td>${kappList.LYRI_WRTR }</td>
									<td>${kappList.COMS_WRTR }</td>
									<td><c:choose>
										<c:when test="${kappList.MUSC_GNRE=='2' }">일반가요</c:when>
										<c:when test="${kappList.MUSC_GNRE=='14' }">성인가요</c:when>
										<c:when test="${kappList.MUSC_GNRE=='3' }">댄스</c:when>
										<c:when test="${kappList.MUSC_GNRE=='4' }">시낭송</c:when>
										<c:when test="${kappList.MUSC_GNRE=='5' }">MR</c:when>
										
										<c:when test="${kappList.MUSC_GNRE=='6' }">아동음악</c:when>
										<c:when test="${kappList.MUSC_GNRE=='7' }">발라드</c:when>
										<c:when test="${kappList.MUSC_GNRE=='8' }">국악</c:when>
										<c:when test="${kappList.MUSC_GNRE=='9' }">캐롤</c:when>
										<c:when test="${kappList.MUSC_GNRE=='10' }">기독교</c:when>
										
										<c:when test="${kappList.MUSC_GNRE=='11' }">불교</c:when>
										<c:when test="${kappList.MUSC_GNRE=='12' }">연주곡</c:when>
										<c:when test="${kappList.MUSC_GNRE=='13' }">가곡</c:when>
										<c:when test="${kappList.MUSC_GNRE=='1' }">OST</c:when>
										<c:when test="${kappList.MUSC_GNRE=='15' }">락</c:when>
										
										<c:when test="${kappList.MUSC_GNRE=='16' }">일렉트로닉</c:when>
										<c:when test="${kappList.MUSC_GNRE=='17' }">클래식</c:when>
										<c:when test="${kappList.MUSC_GNRE=='18' }">힙합</c:when>
										<c:when test="${kappList.MUSC_GNRE=='19' }">재즈</c:when>
										<c:when test="${kappList.MUSC_GNRE=='20' }">알앤비/소울</c:when>
										
										<c:when test="${kappList.MUSC_GNRE=='21' }">팝</c:when>
										<c:when test="${kappList.MUSC_GNRE=='22' }">월드뮤직</c:when>
										<c:when test="${kappList.MUSC_GNRE=='23' }">크로스오버</c:when>
										<c:when test="${kappList.MUSC_GNRE=='24' }">포크</c:when>
										<c:when test="${kappList.MUSC_GNRE=='25' }">뉴에이지</c:when>
										
										<c:when test="${kappList.MUSC_GNRE=='26' }">로고송</c:when>
										<c:when test="${kappList.MUSC_GNRE=='99' }">기타</c:when>
										
										
										
									</c:choose></td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
				</div>
				<!-- //보상금신청 항목 테이블 영역입니다 --> <!-- 보상금신청 첨부파일 테이블 영역입니다 -->
				<div id="divKappFileList" class="tabelRound mb5"
					style="display:<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>;">
				<table cellspacing="0" cellpadding="0" border="1"
					class="grid topLine" summary="(한국음원제작자협회)보상금신청 첨부파일 정보입니다." > 
					<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
					<colgroup>
						<col width="20%">
						<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="col" class="headH white p12">첨부파일</th>
							<td>
							<dl class="fl categori w100">
								<dt><strong>파일이름</strong></dt>
								<dd>
								<table class="w100">
									<colgroup>
										<col>
									</colgroup>
									<tbody>
										<c:if test="${!empty fileList}">
											<c:set var="fileCnt" value="0" />
											<c:forEach items="${fileList}" var="fileList">
												<c:if test="${fileList.TRST_ORGN_CODE=='203' }">
													<c:set var="fileCnt" value="${fileCnt+1}" />
													<tr>
														<td style="padding: 0px;">${fileList.FILE_NAME} <input
															type="hidden" name="hddnGetRealFileName"
															value="${fileList.REAL_FILE_NAME }" /> <input
															type="hidden" name="hddnGetFilePath"
															value="${fileList.FILE_PATH }" /> <input type="hidden"
															name="hddnGetFileName" value="${fileList.FILE_NAME }" />
														<input type="hidden" name="hddnGetFileSize"
															value="${fileList.FILE_SIZE }" /> <input type="hidden"
															name="hddnGetFileOrgnCode"
															value="${fileList.TRST_ORGN_CODE }" /></td>
													</tr>
												</c:if>
											</c:forEach>
											<c:if test="${fileCnt < 1}">
												<tr>
													<td class="ce" style="padding: 0px;">파일이 없습니다.</td>
												</tr>
											</c:if>
										</c:if>
										<c:if test="${empty fileList}">
											<tr>
												<td class="ce" style="padding: 0px;">파일이 없습니다.</td>
											</tr>
										</c:if>
									</tbody>
								</table>
								</dd>
							</dl>
							</td>
						</tr>
					</tbody>
				</table>
				</div>
				<!-- //보상금신청 첨부파일 테이블 영역입니다 --> <!-- 보상금신청 오프라인 관련 영역 -->
				<div id="divKappOffline1" class="contentsRoundBox"
					style="display:<c:if test="${trstList.OFFX_LINE_RECP!='Y' }">none</c:if>;">
				<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span
					class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
				<div class="infoImg1">
				<p><strong>한국음원제작자협회 오프라인접수 선택한 경우 해당협회에 관련서류를
				제출하여야합니다.</strong></p>
				<ul>
					<li>1. 방송보상금 신청서 <br />
					&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금
					신청현황조회 상세화면에서 방송보상금 신청서 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span></li>
					<li class="mt3"><!-- 2. 음원명세서 및 해당 음반  -->2. 음원등록명세서와 저작인접물(음반,음원 등) <br />
					&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금
					신청현황조회 상세화면에서 음원명세서 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span></li>
					<li class="mt3">3. 주민등록등본/법인등기부등본</li>
					<li class="mt3">4. 사업자등록증(사본) 1부</li>
					<li class="mt3">5. 인감증명서 1부</li>
					<li class="mt3">6. 통장사본 1부</li>
				</ul>
				</div>
				</div>
				<!-- //보상금신청 오프라인 관련 영역 --></div>
				<!-- //음제협 관련 영역 -->
			</c:if>
		</c:forEach>
	</c:if>
</c:if> <c:if test="${clientInfo.FOKAPO =='Y'}">
	<c:if test="${!empty trstList}">
		<c:forEach items="${trstList}" var="trstList">
			<c:if test="${trstList.TRST_ORGN_CODE=='202' }">
				<!-- 실연자 관련 영역 -->
				<div class="bbsSection">
				<div class="floatDiv">
				<h4 class="fl">내용(한국음악실연자연합회) <span class="labelFont"> <label><input
					type="checkbox" class="inputChk ml10"
					<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if>
					disabled="disabled" title="오프라인접수(첨부서류)" />오프라인접수(첨부서류)</label> <input type="hidden"
					name="hddnFokapoOff" value="${trstList.OFFX_LINE_RECP }" /> </span></h4>
				</div>

				<!-- 보상금신청 실연자정보 테이블 영역입니다 -->
				<div>
				<div class="tabelRound">
				<table cellspacing="0" cellpadding="0" border="1"
					class="grid topLine" summary="(한국음악실연자연합회)보상금신청 실연자정보 입니다.">
					<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
					<colgroup>
						<col width="20%">
						<col width="30%">
						<col width="20%">
						<col width="30%">
					</colgroup>
					<tbody>
						<tr>
							<th class="bgbr">실연자</th>
							<td><label class="labelBlock"><em class="w45">본명</em>${pemrRlnm
							}</label> <label class="labelBlock"><em class="w45">예명</em>${pemrStnm
							}</label> <label class="labelBlock"><em class="w45">그룹명</em>${grupName
							}</label></td>
							<th class="bgbr">주민번호</th>
							<td>
								<!-- ${resdNumbStr }   -->
								<input type="text" name="txtFokapoResdNumb_1" value="${resdNumbStr_1 }" title="실연자 주민번호(1번째)" class="whiteR w20" />
						    	-
						    	<input type="text" name="txtFokapoResdNumb_2" value="${resdNumbStr_2 }" title="실연자 주민번호(2번째)" class="whiteL w20" />
							<!-- 전화면에서 넘어온 값듯 --> <input
								type="hidden" name="txtFokapoPemrRlnm" value="${pemrRlnm }" /> <input
								type="hidden" name="txtFokapoPemrStnm" value="${pemrStnm }" /> <input
								type="hidden" name="txtFokapoGrupName" value="${grupName }" /> <input
								type="hidden" name="txtFokapoResdNumb" value="${resdNumbStr }" />
							</td>
						</tr>
					</tbody>
				</table>
				</div>
				<!-- //보상금신청 실연자정보 테이블 영역입니다 --> <!-- 보상금신청 항목 테이블 영역입니다 -->
				<div id="divFokapoItemList" class="tabelRound">
				<table id="tabFokapoItemList" cellspacing="0" cellpadding="0" border="1"
					class="clear grid topLine mt5" summary="(한국음악실연자연합회)보상금신청 저작물 정보입니다.">
					<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
					<colgroup>
						<col width="25px">
						<col width="120px">
						<col width="80px">
						<col width="110px">
						<col width="80px">
						<col width="80px">
						<col width="110px">
						<col width="80px">
					</colgroup>
					<thead>
						<tr>
							<th scope="col" class="headH">번호</th>
							<th scope="col" class="headH">작품명</th>
							<th scope="col" class="headH">가수명</th>
							<th scope="col" class="headH">앨범명</th>
							<th scope="col" class="headH">앨범발매일</th>
							<th scope="col" class="headH">실연종류</th>
							<th scope="col" class="headH">실연내용</th>
							<th scope="col" class="headH">기여율(%)</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${!empty fokapoList}">
							<c:forEach items="${fokapoList}" var="fokapoList">
								<c:set var="fokapoNo" value="${fokapoNo+1}" />
								<tr>
									<td class="ce">${fokapoNo } <!-- 전화면에서 넘어온 값듯 --> <input
										type="hidden" name="hddnFokapoPrpsIdnt"
										value="${fokapoList.PRPS_IDNT }" /> <input
										type="hidden" name="hddnFokapoPrpsIdntCode"
										value="${fokapoList.PRPS_IDNT_CODE }" /> <input type="hidden"
										name="hddnFokapoPdtnName" value="${fokapoList.PDTN_NAME }" />
									<input type="hidden" name="txtFokapoSnerName"
										value="${fokapoList.SNER_NAME }" /> <input type="hidden"
										name="txtFokapoAbumName" value="${fokapoList.ABUM_NAME }" />
									<input type="hidden" name="txtFokapoAbumDateIssu"
										value="${fokapoList.ABUM_DATE_ISSU }" /> <input type="hidden"
										name="txtFokapoPemsKind" value="${fokapoList.PEMS_KIND }" /> <input
										type="hidden" name="txtFokapoPemsDesc"
										value="${fokapoList.PEMS_DESC }" /> <input type="hidden"
										name="txtFokapoCtbtRate" value="${fokapoList.CTBT_RATE }" /></td>
									<td>${fokapoList.PDTN_NAME }</td>
									<td class="ce">${fokapoList.SNER_NAME }</td>
									<td>${fokapoList.ABUM_NAME }</td>
									<td class="ce">${fokapoList.ABUM_DATE_ISSU }</td>
									<td class="ce">${fokapoList.PEMS_KIND }</td>
									<td class="ce">${fokapoList.PEMS_DESC }</td>
									<td class="rgt">${fokapoList.CTBT_RATE }</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
				</div>
				<!-- //보상금신청 항목 테이블 영역입니다 --> <!-- 보상금신청 첨부파일 테이블 영역입니다 -->
				<div id="divFokapoFileList" class="tabelRound mb5"
					style="display:<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>;">
				<table cellspacing="0" cellpadding="0" border="1"
					class="grid topLine" summary="(한국음악실연자연합회)보상금신청 첨부파일 정보 입니다.">
					<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
					<colgroup>
						<col width="20%">
						<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="col" class="headH white p12">첨부파일</th>
							<td>
							<dl class="fl categori w100">
								<dt><strong>파일이름</strong></dt>
								<dd>
								<table class="w100">
									<colgroup>
										<col>
									</colgroup>
									<tbody>
										<c:if test="${!empty fileList}">
											<c:set var="fileCnt" value="0" />
											<c:forEach items="${fileList}" var="fileList">
												<c:if test="${fileList.TRST_ORGN_CODE=='202' }">
													<c:set var="fileCnt" value="${fileCnt+1}" />
													<tr>
														<td style="padding: 0px;">${fileList.FILE_NAME} <input
															type="hidden" name="hddnGetRealFileName"
															value="${fileList.REAL_FILE_NAME }" /> <input
															type="hidden" name="hddnGetFilePath"
															value="${fileList.FILE_PATH }" /> <input type="hidden"
															name="hddnGetFileName" value="${fileList.FILE_NAME }" />
														<input type="hidden" name="hddnGetFileSize"
															value="${fileList.FILE_SIZE }" /> <input type="hidden"
															name="hddnGetFileOrgnCode"
															value="${fileList.TRST_ORGN_CODE }" /></td>
													</tr>
												</c:if>
											</c:forEach>
											<c:if test="${fileCnt < 1}">
												<tr>
													<td class="ce" style="padding: 0px;">파일이 없습니다.</td>
												</tr>
											</c:if>
										</c:if>
										<c:if test="${empty fileList}">
											<tr>
												<td class="ce" style="padding: 0px;">파일이 없습니다.</td>
											</tr>
										</c:if>
									</tbody>
								</table>
								</dd>
							</dl>
							</td>
						</tr>
					</tbody>
				</table>
				</div>
				<!-- //보상금신청 첨부파일 테이블 영역입니다 --> <!-- 보상금신청 오프라인 관련 영역 -->
				<div id="divFokapoOffline1" class="contentsRoundBox"
					style="display:<c:if test="${trstList.OFFX_LINE_RECP!='Y' }">none</c:if>;">
				<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span
					class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
				<div class="infoImg1">
				<p><strong>한국음악실연자연합회 오프라인접수 선택한 경우 해당연합회에 관련서류를
				제출하여야합니다.</strong></p>
				<ul>
					<li>1. 방송보상금 신청서 <br />
					&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금
					신청현황조회 상세화면에서 방송보상금 신청서 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span></li>
					<li class="mt3">2. 실연자임을 증명할 수 있는 서류<br />
					&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(앨범자켓,
					저작권등록증, 저작권인증서, 실연사실확인서의 사본 등)</span></li>
					<li class="mt3">3. 상속, 양도, 승계 등의 사실을 증명할 수 있는 서류</li>
					<li class="mt3">4. 주민등록등본</li>
					<li class="mt3">5. 통장사본 1부</li>
				</ul>
				</div>
				</div>
				<!-- //보상금신청 오프라인 관련 영역 --></div>
				<!-- 실연자 관련 영역 --></div>
			</c:if>
		</c:forEach>
	</c:if>
</c:if> <!-- 보상금신청(내용) 테이블 영역입니다 -->
<div class="bbsSection">
<div id="divContent" class="tabelRound mb5">
<table cellspacing="0" cellpadding="0" border="1"
	class="clear grid topLine mt5" summary="보상금신청 내용입력 정보입니다.">
	<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
	<colgroup>
		<col width="15%">
		<col width="*">
	</colgroup>
	<tbody>
		<tr>
			<th class="bgbr">내용</th>
			<td>
			<p class="overflow_y h100">${clientInfo.PRPS_DESC }</p>
			</td>
		</tr>
	</tbody>
</table>
</div>
</div>
<!-- //보상금신청(내용) 테이블 영역입니다 --></form>

<div class="floatDiv mt10">
<p class="fl"><span class="button medium icon"><span
	class="list">&nbsp;</span><a href="#" onclick="javascript:fn_list();">목록</a></span>
</p>
<p class="fl" style="margin-left: 10px;"><span
	class="button medium icon"><span class="list">&nbsp;</span><a
	href="#" onclick="javascript:fn_prePage();">이전화면</a></span></p>
<p class="rgt"><span class="button medium icon"><span
	class="default">&nbsp;</span><a href="#"
	onclick="javascript:fn_chkSubmit();">보상금 신청</a></span></p>
</div>

</div>
</div>
</div>
<!-- CONTAINER end --> <!-- FOOTER str--> <jsp:include
	page="/include/2010/footer.jsp" /> <!-- FOOTER end --></div>
<!-- //전체를 감싸는 DIVISION -->



<c:if test="${clientInfo.KAPP =='Y'}">
<script type="text/javascript">
<!--
	window.onload	= function(){	
									resizeDiv('tabKappItemList', 'divKappItemList');
								}
//-->
</script>
</c:if>

<c:if test="${clientInfo.FOKAPO =='Y'}">
<script type="text/javascript">
<!--
	window.onload	= function(){	
									resizeDiv('tabFokapoItemList', 'divFokapoItemList');
								}
//-->
</script>
</c:if>

</body>
</html>
