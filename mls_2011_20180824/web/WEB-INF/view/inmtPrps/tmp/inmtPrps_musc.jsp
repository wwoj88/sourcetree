<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금발생저작물 조회 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<style type="text/css">
<!--
.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript">
<!--
	//체크박스에 따른 화면 설정(첨부파일,오프라인)
	function fn_chgInsertDiv(sOrgn, obj){
		var frm = document.frm;

		if(obj.checked){
			if(sOrgn == 'kapp'){
				frm.hddnInsertKapp.value = 'Y';
			}else{
				frm.hddnInsertFokapo.value = 'Y';
			}
			fn_chgDiv(sOrgn, 'Y');
			resizeDiv('divKappItemList' , 'div_scroll');
			
		}else{
			if(sOrgn == 'kapp'){
				if(confirm('저작인적권(앨범제작) 선택해제 시 관련 선택저작물이 신청이 되지않습니다.\r\n선택해제 하시겠습니까?')){
					frm.hddnInsertKapp.value = 'N';
				}else{
					obj.checked	= true;
					return;
				}
			}else{
				if(confirm('저작인적권(가창, 연주, 지휘 등) 선택해제 시 관련 선택저작물이 신청이 되지않습니다.\r\n선택해제 하시겠습니까?')){
					frm.hddnInsertFokapo.value = 'N';
				}else{
					obj.checked	= true;
					return;
				}
			}
			fn_chgDiv(sOrgn, 'N');
		}
	}

	//체크박스에 따른 화면 설정(첨부파일,오프라인)
	function fn_chgDiv(sOrgn,sDiv){

		var oFldKapp = document.getElementById("fldKapp");
		var oFldFokapo = document.getElementById("fldFokapo"); 
			
		var oInmtInfo, oItemList, oFileList, oOffLine;	//div
		var chkOff;	//checkbox
		var oBtn;
	
		if(sOrgn == 'kapp'){			//음제협
			oItemList	= document.getElementById("divKappItemList");
			oFileList	= document.getElementById("divKappFileList");
			oOffLine	= document.getElementById("divKappOffline1");
			oBtn		= document.getElementById("spanKappBtn");

			chkOff	= document.getElementById("chkKappOff");

		}else if(sOrgn == 'fokapo'){	//실연자
			oInmtInfo	= document.getElementById("divFokapoInmtInfo");
			oItemList	= document.getElementById("divFokapoItemList");
			oFileList	= document.getElementById("divFokapoFileList");
			oOffLine	= document.getElementById("divFokapoOffline1");
			oBtn		= document.getElementById("spanFokapoBtn");

			chkOff	= document.getElementById("chkFokapoOff");
		}

		if(sDiv == 'OFF'){
			if(chkOff.checked){
				if(sOrgn == 'fokapo')	oInmtInfo.style.display = '';
				oItemList.style.display	= '';
				oFileList.style.display = 'none';
				oOffLine.style.display	= '';
				oBtn.style.display		= '';
			}else{
				if(sOrgn == 'fokapo')	oInmtInfo.style.display = '';
				oItemList.style.display	= '';
				oFileList.style.display = '';
				oBtn.style.display		= '';
				oOffLine.style.display	= '';
			}
		}else if(sDiv == 'Y'){
			if(sOrgn == 'kapp'){
				oFldKapp.style.display = '';
			}else{
				oFldFokapo.style.display = '';
			}
		}else if(sDiv == 'N'){
			if(sOrgn == 'kapp'){
				oFldKapp.style.display = 'none';
			}else{
				oFldFokapo.style.display = 'none';
			}
		}
	}

	// 선택된 저작물 삭제
	function fn_delete(sStr, rowDisName){
		var frm = document.frm;
		
		//선택 목록
		var chkObjs = document.getElementsByName(sStr);
		var sCheck = 0;
		
		for(i=0; i<chkObjs.length;i++){
			if(chkObjs[i].checked){
				sCheck = 1;
			}
		}
		
		if(sCheck == 0 ){
			alert('선택된 저작물이 없습니다.');
			return;
		}
		
		for(var i=chkObjs.length-1; i >= 0; i--){
			var chkObj = chkObjs[i];
			
			if(chkObj.checked){
			
				// 선택된 저작물을 삭제한다.
				var oTR = findParentTag(chkObj, "TR");
				
				if(eval(oTR) && rowDisName=='kapp_displaySeq' ){ 	
					oTR.parentNode.removeChild(oTR);
					var the_height = document.getElementById('divKappItemList').offsetHeight;
					document.getElementById('divKappItemList').style.height = the_height - 33  + "px" ;
				}
				
				if(eval(oTR) && rowDisName=='fokapo_displaySeq') 	oTR.parentNode.removeChild(oTR);
				
			}
		}
		
		// 순번재지정
		fn_resetSeq(rowDisName);
	}

	//순번 재지정
	function fn_resetSeq(disName){
	    var oSeq = document.getElementsByName(disName);
	    for(i=0; i<oSeq.length; i++){
	        oSeq[i].value = i+1;
	    }
	}
	//보상금신청
	function fn_chkSubmit(){
		var frm = document.frm;

		//신청자 정보 필수체크
		var oFldInmt	= document.getElementById("fldInmtInfo");
		var oInmt	= oFldInmt.getElementsByTagName("input");


		var txtHomeAddr	= ""; var txtBusiAddr	= "";
		var txtHomeTelxNumb	= "";  var txtBusiTelxNumb	= "";  var txtMoblPhon	= "";  var txtFaxxNumb	= ""; 

/*
		txtHomeAddr	= document.getElementById("txtHomeAddr").value;
		txtBusiAddr	= document.getElementById("txtBusiAddr").value;

		txtHomeTelxNumb	= document.getElementById("txtHomeTelxNumb").value;
		txtBusiTelxNumb	= document.getElementById("txtBusiTelxNumb").value;
		txtMoblPhon	= document.getElementById("txtMoblPhon").value;
		txtFaxxNumb	= document.getElementById("txtFaxxNumb").value;
*/	

		txtHomeAddr	= document.getElementsByName("txtHomeAddr").value;
		txtBusiAddr	= document.getElementsByName("txtBusiAddr").value;

		txtHomeTelxNumb	= document.getElementsByName("txtHomeTelxNumb").value;
		txtBusiTelxNumb	= document.getElementsByName("txtBusiTelxNumb").value;
		txtMoblPhon	= document.getElementsByName("txtMoblPhon").value;
		txtFaxxNumb	= document.getElementsByName("txtFaxxNumb").value;
			
		for(i = 0; i < oInmt.length; i++){
			/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
			if(checkField(oInmt[i]) == false){
				return;
			}
			*/
			
			// 전화번호
			if(oInmt[i].name == 'txtHomeTelxNumb'
				|| oInmt[i].name == 'txtBusiTelxNumb'
				|| oInmt[i].name == 'txtMoblPhon'
				|| oInmt[i].name == 'txtFaxxNumb'){
				
				if(txtHomeTelxNumb == ''){
					if(txtBusiTelxNumb == ''){
						if(txtMoblPhon == ''){
							if(txtFaxxNumb == ''){
								if(!nullCheck(oInmt[i]))	return;
							}	
						}
					}
				}
				
				if(!character(oInmt[i],  'EK'))	return;
			}
			
			// 자택 사무실 주소 nullCheck
			if(oInmt[i].name == 'txtHomeAddr'
				|| oInmt[i].name == 'txtBusiAddr'){
				if(txtHomeAddr == ''){
					if(txtBusiAddr == ''){
						if(!nullCheck(oInmt[i]))	return;
					}	
				}
			}
			
			// 입금처
			if(oInmt[i].name == 'txtBankName'
				|| oInmt[i].name == 'txtAcctNumb'
				|| oInmt[i].name == 'txtDptr'){
				
				if(!nullCheck(oInmt[i]))	return;
			}

		}
		

		//체크박스 값들
		var vChkKappOff		= document.getElementById("chkKappOff").checked;
		var vChkFokapoOff	= document.getElementById("chkFokapoOff").checked;
		if(vChkKappOff)		frm.hddnKappOff.value = "Y";
		if(vChkFokapoOff)	frm.hddnFokapoOff.value = "Y";
		
		var oChk203 = document.getElementById("chkTrts203");
		var oChk202 = document.getElementById("chkTrts202");
		
		//음제협 정보 필수체크
		if(oChk203.checked){
			var oFldKapp	= document.getElementById("fldKapp");
			var oKapp	= oFldKapp.getElementsByTagName("input");//+oFldKapp.getElementsByTagName("select");
			var oKappSel	= oFldKapp.getElementsByTagName("select");
			
			
			for(i = 0; i < oKapp.length; i++){
				/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
				if(checkField(oKapp[i]) == false){
					return;
				}
				*/
				//nullCheck
				
				//alert(oKapp[i].id);
				if(oKapp[i].name == 'txtKappDiskName'
					||oKapp[i].name == 'hddnKappSongName'
					||oKapp[i].name == 'txtKappSnerName'
					||oKapp[i].name == 'txtKappDateIssu'
					||oKapp[i].name == 'selKappMuscGnre'){

					if(!nullCheck(oKapp[i]))	return;
				}

				// 날짜 형식 체크
				if(oKapp[i].name == 'txtKappDateIssu'){
				   	
				   	// 자동입력				
					if((oKapp[i].value).length == 4) {
						oKapp[i].value = oKapp[i].value+'0101';
					}
					
					if(!dateCheck(oKapp[i]))	return;
				}
			}
			
			
			// 장르 select 박스
			for(i = 0; i < oKappSel.length; i++){
				if(oKappSel[i].name == 'selKappMuscGnre'){
					if(!nullCheck(oKappSel[i]))	return;
				}
			}
			

			//신청저작물 체크
			var oKappPrpsIdnt = document.getElementsByName("hddnKappPrpsIdnt");
			for(i = 0; i < oKappPrpsIdnt.length; i++){
				if(oKappPrpsIdnt[i].value == null || (oKappPrpsIdnt[i].value).length < 1){
					alert('내용(한국음원제작자협회) 정보를 입력해주세요.');
					return;
				}
			}
		}
		
		//음실연 정보 필수체크
		if(oChk202.checked){				
			var oFldFokapo	= document.getElementById("fldFokapo");
			var oFokapo	= oFldFokapo.getElementsByTagName("input");
			for(i = 0; i < oFokapo.length; i++){
				/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
				if(checkField(oFokapo[i]) == false){
					return;
				}
				*/
				
				// nullCheck
				if(oFokapo[i].name == 'txtFokapoPemrRlnm'
					||oFokapo[i].name == 'txtFokapoResdNumb_1'
					||oFokapo[i].name == 'txtFokapoResdNumb_2'
					||oFokapo[i].name == 'txtFokapoSnerName'
					//|| oFokapo[i].name == 'hddnFokapoPdtnName'
					){
					
					if(!nullCheck(oFokapo[i]))	return;
				}

				// 글자길이 범위 체크
				if(oFokapo[i].name == 'txtFokapoResdNumb_1'){
					if(!rangeSize(oFokapo[i], '6'))	return;
				}
				
				if(oFokapo[i].name == 'txtFokapoResdNumb_2'){
					if(!rangeSize(oFokapo[i], '7'))	return;
				}
				
				// 날짜 형식 체크
				if(oFokapo[i].name == 'txtFokapoAbumDateIssu'){

					if(!dateCheck(oFokapo[i]))	return;
				}
				
				// 금지할 문자 종류 체크 (E : 영문, K : 한글, N : 숫자)
				if(oFokapo[i].name == 'txtFokapoResdNumb_1'
					|| oFokapo[i].name == 'txtFokapoResdNumb_2'
					|| oFokapo[i].name == 'txtFokapoCtbtRate'){
					
					if(!character(oFokapo[i],  'EKS'))	return;
				}
			}

			//신청저작물 체크
			var oFokapoPrpsIdnt = document.getElementsByName("hddnFokapoPrpsIdnt");
			for(i = 0; i < oFokapoPrpsIdnt.length; i++){
				if(oFokapoPrpsIdnt[i].value == null || (oFokapoPrpsIdnt[i].value).length < 1){
					alert('내용(한국음악실연자연합회) 정보를 입력해주세요.');
					return;
				}
			}
		}
		
		//내용 필수 체크
		var oContent	= document.getElementById("txtareaPrpsDesc");
		/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
		if(checkField(oContent) == false){
			return;
		}
		*/
		// nullCheck
		if(!nullCheck(oContent))	return;

		if(onkeylengthMax(oContent, 4000, 'txtareaPrpsDesc') == false){
			return;
		}
		
		if(oChk203.checked == false && oChk202.checked == false){
			alert('분류(을)를 선택 해 주세요.');
			return;
		}

		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrpsChk";
		frm.submit();
	}
	
	
	//목록으로 이동
	function fn_list(){
		var frm = document.srchForm;
		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=list&amp;gubun=edit&amp;srchDIVS=1";
		frm.submit();
	}

	// table name 사이즈에 대한 div targetName리사이즈
	function resizeDiv(divName, tabName) {
		var the_height = document.getElementById(tabName).offsetHeight;
		document.getElementById(divName).style.height = the_height + 13  + "px" ;
	}


    //maxlength 체크
	function onkeylengthMax(formobj, maxlength, objname) {  
	    var li_byte     = 0;  
	    var li_len      = 0;  
	    for(var i=0; i< formobj.value.length; i++){  
	    	
	        if (escape(formobj.value.charAt(i)).length > 4){  
	            li_byte += 3;  
	        } else {  
	            li_byte++;  
	        }  
	        if(li_byte <= maxlength) {  
	            li_len = i + 1;  
	        }  
	    }

	    if(li_byte > maxlength){  
	        alert('최대 글자 입력수를 초과 하였습니다. \n최대 글자 입력수를 초과된 내용은 자동으로 삭제됩니다.');  
	        formobj.value = formobj.value.substr(0, li_len);
	        return false;  
		    formobj.focus();  
	    }
	    return true;  
	}
	
	function fn_confirm() {
		var prpsDoblCode = '${prpsDoblCode }'
		var firstYN = '${firstYN }';
		
		if(prpsDoblCode == '1' && firstYN == 'Y'){
			
			var conVal = confirm('저작권찾기가 신청되었습니다. \n보상금발생 저작물내역을 먼저 확인해주세요. 지금 확인 하시겠습니까?');
			
			if(conVal){
			
				// 목록으로 이동한다.
				fn_list();
			}
		}
	}

	//숫자만 입력
	function only_arabic(t){
		var key = (window.netscape) ? t.which :  event.keyCode;

		if (key < 45 || key > 57) {
			if(window.netscape){  // 파이어폭스
				t.preventDefault();
			}else{
				event.returnValue = false;
			}
		} else {
			//alert('숫자만 입력 가능합니다.');
			if(window.netscape){   // 파이어폭스
				return true;
			}else{
				event.returnValue = true;
			}
		}	
	}
	
	
	//헬프메시지 레이어를 화면에 뿌려준다
	function viewMessage(pageName)
	{
		var layer = document.getElementById('helpMessage');
		
		if(typeof(layer) != 'undefined')
		{
			var forms = document.helpForm;
			
			forms.pageName.value = pageName;
			forms.action = '/common/help_message.jsp';
			
			var result = trim(synchRequest(getQueryString(forms)));
			
			layer.innerHTML = result;
			
			//layer.clientHeight의 값을 정확히 구하기 위해 미리 보이지 않는 사각에 layer을 미리 화면에 띄어준다.
			layer.style.top = -1000;
			layer.style.display = '';
			
			//var top = window.event.clientY + document.body.scrollTop - (layer.clientHeight + 50);
			
			//if(top < 0)
			//	top = 0;
				
			//if(top < document.body.scrollTop)
			//	top = top + (document.body.scrollTop - top);
				
			//layer.style.top = top;
			
			//layer.style.left=document.body.scrollLeft + window.event.clientX;
	    	//layer.style.top=document.body.scrollTop + window.event.clientY;
			
			layer.style.left=window.event.clientX+(document.documentElement.scrollLeft || 
	            document.body.scrollLeft) - 
	            document.documentElement.clientLeft;
	            
			layer.style.top=window.event.clientY +(document.documentElement.scrollTop || 
	            document.body.scrollTop) - 
	            document.documentElement.clientTop;
			
			layer.style.display = '';
		}
	}

	//헬프메시지 레이어를 닫아줌

	function closeMessage()
	{
		var layer = document.getElementById('helpMessage');
		
		if(typeof(layer) != 'undefined')
			layer.style.display = 'none';
	}
	
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_2.png" alt="보상금발생저작물 조회" title="보상금발생저작물 조회" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li class="active"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악<span>&nbsp;</span></a></li>
							<li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용<span>&nbsp;</span></a></li>
							<li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관<span>&nbsp;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>보상금발생저작물 조회</span>&gt;<strong>보상금신청</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>보상금발생저작물 조회
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<!-- hidden form str : 헬프레이어 -->
					<form name="helpForm" method="post" action="">
						<input type="hidden" name="pageName">
					</form>
					
					<form name="frm" action="" enctype="multipart/form-data" class="sch">
						<input type="hidden" name="srchDIVS"		value="${srchParam.srchDIVS }" />
						<input type="hidden" name="srchSdsrName"	value="${srchParam.srchSdsrName }" />
						<input type="hidden" name="srchMuciName"	value="${srchParam.srchMuciName }" />
						<input type="hidden" name="srchYymm"		value="${srchParam.srchYymm }" />
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }" />
						
						<input type="hidden" name="hddnTrst203"	value="${clientInfo.TRST_203 }" />
						<input type="hidden" name="hddnTrst202"	value="${clientInfo.TRST_202 }" />
						<input type="hidden" name="hddnTrst205"	value="${clientInfo.TRST_205 }" />
						<input type="hidden" name="hddnTrst205_2"	value="${clientInfo.TRST_205_2 }" />
					
					
						<input type="hidden" name="prpsDoblCode"	value="${prpsDoblCode }" />
						<input type="hidden" name="rghtPrpsMastKey"	value="${rghtPrpsMastKey }" />
					<!-- memo 삽입 -->
						<jsp:include page="/common/memo/memo_01.jsp">
							<jsp:param name="DIVS" value="${srchDIVS}"/>
						</jsp:include>
					<!-- // memo 삽입 -->
					
					<div class="bbsSection">
						<h4>방송음악 보상금 신청</h4>
							<fieldset id="fldInmtInfo">
								<legend>보상금신청 신청자 정보</legend>
								
								<!-- 보상금신청(기본정보) 테이블 영역입니다 -->
								<div class="tabelRound">
									<span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span><!-- 라운딩효과 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금 신청정보 입력 폼입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row" class="bgbr lft">보상금 종류</th>
												<td><c:if test="${srchDIVS == '1'}">방송음악</c:if><c:if test="${srchDIVS == '2'}">교과용</c:if><c:if test="${srchDIVS == '3'}">도서관</c:if></td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청자</th>
												<td>
													<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
													<colgroup>
														<col width="15%">
														<col width="35%">
														<col width="15%">
														<col width="35%">
														</colgroup>
														<tbody>
															<tr>
																<th class="bgbr3 lft"><label class="necessary">성명</label></th>
																<td>
																	<input type="hidden" name="hddnUserIdnt" value="${clientInfo.USER_IDNT }" />
																	<input type="text" name="txtPrpsName" value="${clientInfo.PRPS_NAME }" title="신청자 성" readonly class="inputData w60" />
																</td>
																<th class="bgbr3 lft"><label class="necessary">주민번호/</label>사업자번호</th>
																<td><input type="text" name="txtPrsdCorpNumbView" value="${clientInfo.RESD_CORP_NUMB_VIEW }" title="주민번호/사업자번호" readonly class="inputData w60" /></td>
															</tr>
															<tr>
																<th class="bgbr3 lft"><label class="necessary">전화</label></th>
																<td>
																	<label class="labelBlock"><em class="w55">자택</em><input type="text" name="txtHomeTelxNumb" value="${clientInfo.HOME_TELX_NUMB }" maxlength="20" title="신청자 전화(자택)" class="inputData w60" /></label>
																	<label class="labelBlock"><em class="w55">사무실</em><input type="text" name="txtBusiTelxNumb" value="${clientInfo.BUSI_TELX_NUMB }" maxlength="20" title="신청자 전화(사무실)" class="inputData w60" /></label>
																	<label class="labelBlock"><em class="w55">휴대폰</em><input type="text" name="txtMoblPhon" value="${clientInfo.MOBL_PHON }" maxlength="20" title="신청자 전화(휴대폰)" class="inputData w60" /></label>
																</td>
																<th class="bgbr3 lft">Fax</th>
																<td><input name="txtFaxxNumb" value="${clientInfo.FAXX_NUMB }" maxlength="15" title="신청자 전화(FAX)" class="inputData" /></td>
															</tr>
															<tr>
																<th class="bgbr3 lft"><label class="necessary">주소</label></th>
																<td colspan="3">
																	<label class="labelBlock"><em class="w55">자택</em><input type="text" name="txtHomeAddr" id="txtHomeAddr" value="${clientInfo.HOME_ADDR }" maxlength="33" title="신청자 주소(자택)" class="inputData w80" /></label>
																	<label class="labelBlock"><em class="w55">사무실</em><input type="text" name="txtBusiAddr" id="txtBusiAddr" value="${clientInfo.BUSI_ADDR }" maxlength="33" title="신청자 주소(사무실)" class="inputData w80" /></label>
																</td>
															</tr>
															<tr>
																<th class="bgbr3 lft"><label class="necessary">입금처</label></th>
																<td>
																	<label class="labelBlock"><em class="w55">은행명</em><input type="text" name="txtBankName" value="${clientInfo.BANK_NAME }" title="입금 은행명" maxlength="50" class="inputData w60" /></label>
																	<label class="labelBlock"><em class="w55">계좌번호</em><input type="text" name="txtAcctNumb" value="${clientInfo.ACCT_NUMB }" title="입금 계좌번호" maxlength="50" class="inputData w60" /></label>
																	<label class="labelBlock"><em class="w55">예금주</em><input type="text" name="txtDptr" value="${clientInfo.DPTR }" title="입금 예금주" maxlength="33" class="inputData w60" /></label>
																</td>
																<th class="bgbr3 lft">E-Mail</th>
																<td><input type="text" name="txtMail" value="${clientInfo.MAIL }" maxlength="50" title="신청자 EMAIL" class="inputData" /></td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청 분류</th>
												<td>
													<label>
														<input type="checkbox" name="chkTrts203" id="chkTrts203" 
															onclick="javascript:fn_chgInsertDiv('kapp',this);" 
															<c:choose>
																<c:when test="${clientInfo.KAPP != 'Y'}"><c:if test="${clientInfo.TRST_203 != '1'}">disabled</c:if></c:when>
																<c:otherwise><c:if test="${clientInfo.TRST_203 == '1'}">checked</c:if></c:otherwise> 
															</c:choose>
															 class="inputRChk" title="저작인접권(앨범제작)" />저작인접권(앨범제작)
													</label>
													<label class="ml10">
														<input type="checkbox" name="chkTrst202" id="chkTrts202" 
															onclick="javascript:fn_chgInsertDiv('fokapo',this);" 
															<c:choose>
																<c:when test="${clientInfo.FOKAPO != 'Y'}"><c:if test="${clientInfo.TRST_202 != '1'}">disabled</c:if></c:when>
																<c:otherwise><c:if test="${clientInfo.TRST_202 == '1'}">checked</c:if></c:otherwise> 
															</c:choose> 
															class="inputRChk" title="저작인접권(가창, 연주, 지휘 등)" />저작인접권(가창, 연주, 지휘 등)</label>
													<input type="hidden" name="hddnInsertKapp"		value="<c:if test="${clientInfo.TRST_203 == '1'}">Y</c:if>" />
													<input type="hidden" name="hddnInsertFokapo"	value="<c:if test="${clientInfo.TRST_202 == '1'}">Y</c:if>" /> 
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청저작물</th>
												<td>	
												
													<!-- 테이블 영역입니다 -->
													<div class="tabelRound">
														<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="보상금신청 저작물 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
															<col width="8%">
															<col width="52%">
															<col width="25%">
															<col width="15%">
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH">곡명</th>
																	<th scope="col" class="headH">가수</th>
																	<th scope="col" class="headH">방송년도</th>
																</tr>
															</thead>
															<tbody>
															<c:if test="${!empty selectList}">
																<c:forEach items="${selectList}" var="selectList">	
																	<c:set var="NO" value="${selectListCnt + 1}"/>
																	<c:set var="i" value="${i+1}"/>
																	
																<tr>
																	<td class="ce">
																		<c:out value="${NO - i}"/>
																		<!--<c:out value="${i}"/>-->
																		<input type="hidden" name="hddnSelectInmtSeqn" value="${selectList.INMT_SEQN }" />
																		<input type="hidden" name="hddnSelectPrpsDivs" value="${selectList.PRPS_DIVS }" />
																		<input type="hidden" name="hddnSelectPrpsIdntCode" value="${selectList.PRPS_IDNT_CODE }" />
																		<input type="hidden" name="hddnSelectKapp" value="${selectList.KAPP }" />
																		<input type="hidden" name="hddnSelectFokapo" value="${selectList.FOKAPO }" />
																		<input type="hidden" name="hddnSelectKrtra" value="${selectList.KRTRA }" />
																		
																		<input type="hidden" name="hddnSelectSdsrName"	value="${selectList.SDSR_NAME }" />
																		<input type="hidden" name="hddnSelectMuciName"	value="${selectList.MUCI_NAME }" />
																		<input type="hidden" name="hddnSelectYymm"		value="${selectList.YYMM }" />
																		<input type="hidden" name="hddnSelectLishComp"	value="${selectList.LISH_COMP }" />
																		<input type="hidden" name="hddnSelectUsexType"	value="${selectList.USEX_TYPE }" />
																		
																	</td>
																	<td>${selectList.SDSR_NAME }</td>
																	<td class="ce">${selectList.MUCI_NAME }</td>
																	<td class="ce">${selectList.YYMM }</td>
																</tr>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
													</div>
													<!-- //테이블 영역입니다 -->
													
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청(기본정보) 테이블 영역입니다 -->
								
							</fieldset> 
							
							<!-- 음제협 관련 영역 -->
							<fieldset id="fldKapp" style="display:
								<c:choose>
									<c:when test="${clientInfo.TRST_203 != '1'}">none</c:when>
									<c:when test="${clientInfo.KAPP != 'Y'}">none</c:when> 
								</c:choose>;">
								<legend>한국음원제작자협회 보상금 신청 내용</legend>
								
								<div id="divKappTitle" class="floatDiv mt10">
									<h5 class="fl">내용(한국음원제작자협회)
										<span class="fontNormal ml10">
											<label>
												<input type="checkbox" id="chkKappOff" 
													onclick="javascript:fn_chgDiv('kapp','OFF');" class="inputChk"
													<c:if test="${!empty trstList}">
														<c:forEach items="${trstList}" var="trstList">
															<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
																<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if>
															</c:if>
														</c:forEach>
													</c:if> title="오프라인접수(첨부서류)" />오프라인접수(첨부서류)</label>
											<input type="hidden" name="hddnKappOff" /> 
										</span>
									</h5>
									<p class="fr"><span id="spanKappBtn" class="button small type3"><a href="#" onclick="javascript:fn_delete('chkKapp', 'kapp_displaySeq');return false;">행삭제</a></span></p>
								</div>
								
								<!-- 보상금신청 항목 테이블 영역입니다 -->
								<div id="divKappItemList" class="tabelRound div_scroll" style="width:745px; padding:0 0 0 0;">
									<table id="div_scroll" cellspacing="0" cellpadding="0" border="1" class="grid" summary="(한국음원제작자협회)보상금신청 저작물 정보 입력폼입니다." style="width:1025px;"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="25px">
										<col width="25px">
										<col width="150px">
										<col width="100px">
										<col width="145px">
										<col width="80px">
										<col width="80px">
										<col width="80px">
										<col width="100px">
										<col width="80px">
										<col width="80px">
										<col width="80px">
										</colgroup>
										<thead>
											<tr>
												<th scope="col" class="headH"><input type="checkbox" onclick="javascript:checkBoxToggle('frm','chkKapp',this);" class="vmid" title="전체선택" /></th>
												<th scope="col" class="headH">번호</label></th>
												<th scope="col" class="headH"><label class="necessary">음반명</label> <img src ="/images/2010/common/ic_find_id.gif" class="vtop" onclick="viewMessage('02_01_01');return false;" alt="음반명 입력안내 보기" style="cursor:hand" /> </th>
												<th scope="col" class="headH">CD코드</th>
												<th scope="col" class="headH">곡명</th>
												<th scope="col" class="headH"><label class="necessary">가수명</label></th>
												<th scope="col" class="headH"><label class="necessary">발매일</label> <img src ="/images/2010/common/ic_find_id.gif" class="vtop" onclick="viewMessage('02_01_02');return false;" alt="발매일 입력안내 보기" style="cursor:hand" /> </th>
												<th scope="col" class="headH">국가명</th>
												<th scope="col" class="headH">권리근거</th>
												<th scope="col" class="headH">작사가</th>
												<th scope="col" class="headH">작곡가</th>
												<th scope="col" class="headH"><label class="necessary">음악장르</label><BR>(테마코드)</th>
											</tr>
										</thead>
										<tbody>
										
										<c:if test="${empty kappList}">
											<c:if test="${!empty selectList}">
												<c:forEach items="${selectList}" var="selectList">	
													<c:if test="${selectList.KAPP == 'Y'}">
													<c:set var="k" value="${k+1}"/>
												<tr>
													<td class="ce"><input type="checkbox" name="chkKapp" class="vmid" title="선택" /></td>
													<td class="ce"><input name="kapp_displaySeq" id="kapp_displaySeq_${k}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${k}"/></td>
													<td class="ce"><input type="text" name="txtKappDiskName" value="" maxlength="65" title="음반명" class="inputData w90 ce" /></td>
													<td class="ce"><input type="text" name="txtKappCdCode" value="" maxlength="50" title="CD코드" class="inputData w90 ce" /></td>
													<td>
														${selectList.SDSR_NAME }
														<input type="hidden" name="hddnKappSongName" value="${selectList.SDSR_NAME }" title="곡명"/>
														<input type="hidden" name="hddnKappPrpsIdnt" value="${selectList.INMT_SEQN }"/>
														<input type="hidden" name="hddnKappPrpsIdntCode" value="${selectList.PRPS_IDNT_CODE }"/>
													</td>
													<td class="ce"><input type="text" name="txtKappSnerName" value="" maxlength="16" title="가수명" class="inputData w90 ce" /></td>
													<td class="ce"><input type="text" id="txtKappDateIssu" name="txtKappDateIssu" value="" title="발매일" size="8" maxlength="8" onkeypress="javascript:only_arabic(event);" class="inputData w90 ce" />
													</td>
													<td class="ce"><input type="text" name="txtKappNatuName" value="" maxlength="16" title="국가명" class="inputData w90 ce" /></td>
													<td class="ce"><input type="text" name="txtKappRghtGrnd" value="" maxlength="33" title="권리근거"  class="inputData w90 ce" /></td>
													<td class="ce"><input type="text" name="txtKappLyriWrtr" value="" maxlength="16" title="작사"  class="inputData w90 ce" /></td>
													<td class="ce"><input type="text" name="txtKappComsWrtr" value="" maxlength="16" title="작곡"  class="inputData w90 ce" /></td>
													<td class="ce">
														<select id="selKappMuscGnre" name="selKappMuscGnre" title="음악장르">
															<option value="">선택</option>
															
															<option value="2">일반가요</option>
															<option value="14">성인가요</option>
															<option value="3">댄스</option>
															<option value="4">시낭송</option>
															<option value="5">MR</option>
															
															<option value="6">아동음악</option>
															<option value="7">발라드</option>
															<option value="8">국악</option>
															<option value="9">캐롤</option>
															<option value="10">기독교</option>
															
															<option value="11">불교</option>
															<option value="12">연주곡</option>
															<option value="13">가곡</option>
															<option value="1">OST</option>
															<option value="15">락</option>
															
															<option value="16">일렉트로닉</option>
															<option value="17">클래식</option>
															<option value="18">힙합</option>
															<option value="19">재즈</option>
															<option value="20">알앤비/소울</option>
															
															<option value="21">팝</option>
															<option value="22">월드뮤직</option>
															<option value="23">크로스오버</option>
															<option value="24">포크</option>
															<option value="25">뉴에이지</option>
															
															<option value="26">로고송</option>
															<option value="99">기타</option>
														</select>
													</td>
												</tr>
													</c:if>
												</c:forEach>
											</c:if>
										</c:if>
										
										<c:if test="${!empty kappList}">
											<c:forEach items="${kappList}" var="kappList">	
											<c:set var="k" value="${k+1}"/>
											<tr>
												<td class="ce"><input type="checkbox" name="chkKapp" class="vmid" title="선택" /></td>
												<td class="ce"><input name="kapp_displaySeq" id="kapp_displaySeq_${k}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${k}"/></td>
												<td class="ce"><input type="text" name="txtKappDiskName" value="${kappList.DISK_NAME }" maxlength="65" title="음반명" class="inputData w90 ce" /></td>
												<td class="ce"><input type="text" name="txtKappCdCode" value="${kappList.CD_CODE }" maxlength="50" title="CD코드" class="inputData w90 ce" /></td>
												<td>
													${kappList.SONG_NAME }
													<input type="hidden" name="hddnKappSongName" value="${kappList.SONG_NAME }" title="곡명"/>
													<input type="hidden" name="hddnKappPrpsIdnt" value="${kappList.PRPS_IDNT }"/>
													<input type="hidden" name="hddnKappPrpsIdntCode" value="${kappList.PRPS_IDNT_CODE }"/>
												</td>
												<td class="ce"><input type="text" name="txtKappSnerName" value="${kappList.SNER_NAME }" maxlength="16" title="가수명" class="inputData w90 ce" /></td>
												<td class="ce"><input type="text" name="txtKappDateIssu" id="txtKappDateIssu_${k}" value="${kappList.DATE_ISSU }"  title="발매일" class="inputData w90 ce" />
												</td>
												<td class="ce"><input type="text" name="txtKappNatuName" value="${kappList.NATN_NAME }" maxlength="16" title="국가명" class="inputData w90 ce" /></td>
												<td class="ce"><input type="text" name="txtKappRghtGrnd" value="${kappList.RGHT_GRND }" maxlength="33" title="권리근거"  class="inputData w90 ce" /></td>
												<td class="ce"><input type="text" name="txtKappLyriWrtr" value="${kappList.LYRI_WRTR }" maxlength="16" title="작사"  class="inputData w90 ce" /></td>
												<td class="ce"><input type="text" name="txtKappComsWrtr" value="${kappList.COMS_WRTR }" maxlength="16" title="작곡"  class="inputData w90 ce" /></td>
												<td class="ce">
													<select name="selKappMuscGnre" title="음악장르">
														<option value="">선택</option>

														<option value="2" <c:if test="${kappList.MUSC_GNRE=='2' }">selected</c:if>>일반가요</option>
														<option value="14" <c:if test="${kappList.MUSC_GNRE=='14' }">selected</c:if>>성인가요</option>
														<option value="3" <c:if test="${kappList.MUSC_GNRE=='3' }">selected</c:if>>댄스</option>
														<option value="4" <c:if test="${kappList.MUSC_GNRE=='4' }">selected</c:if>>시낭송</option>
														<option value="5" <c:if test="${kappList.MUSC_GNRE=='5' }">selected</c:if>>MR</option>
														
														<option value="6" <c:if test="${kappList.MUSC_GNRE=='6' }">selected</c:if>>아동음악</option>
														<option value="7" <c:if test="${kappList.MUSC_GNRE=='7' }">selected</c:if>>발라드</option>
														<option value="8" <c:if test="${kappList.MUSC_GNRE=='8' }">selected</c:if>>국악</option>
														<option value="9" <c:if test="${kappList.MUSC_GNRE=='9' }">selected</c:if>>캐롤</option>
														<option value="10" <c:if test="${kappList.MUSC_GNRE=='10' }">selected</c:if>>기독교</option>
														
														<option value="11" <c:if test="${kappList.MUSC_GNRE=='11' }">selected</c:if>>불교</option>
														<option value="12" <c:if test="${kappList.MUSC_GNRE=='12' }">selected</c:if>">연주곡</option>
														<option value="13" <c:if test="${kappList.MUSC_GNRE=='13' }">selected</c:if>>가곡</option>
														<option value="1" <c:if test="${kappList.MUSC_GNRE=='1' }">selected</c:if>>OST</option>
														<option value="15" <c:if test="${kappList.MUSC_GNRE=='15' }">selected</c:if>>락</option>
														
														<option value="16" <c:if test="${kappList.MUSC_GNRE=='16' }">selected</c:if>>일렉트로닉</option>
														<option value="17" <c:if test="${kappList.MUSC_GNRE=='17' }">selected</c:if>>클래식</option>
														<option value="18" <c:if test="${kappList.MUSC_GNRE=='18' }">selected</c:if>>힙합</option>
														<option value="19" <c:if test="${kappList.MUSC_GNRE=='19' }">selected</c:if>>재즈</option>
														<option value="20" <c:if test="${kappList.MUSC_GNRE=='20' }">selected</c:if>>알앤비/소울</option>
														
														<option value="21" <c:if test="${kappList.MUSC_GNRE=='21' }">selected</c:if>>팝</option>
														<option value="22" <c:if test="${kappList.MUSC_GNRE=='22' }">selected</c:if>>월드뮤직</option>
														<option value="23" <c:if test="${kappList.MUSC_GNRE=='23' }">selected</c:if>>크로스오버</option>
														<option value="24" <c:if test="${kappList.MUSC_GNRE=='24' }">selected</c:if>>포크</option>
														<option value="25" <c:if test="${kappList.MUSC_GNRE=='25' }">selected</c:if>>뉴에이지</option>
														
														<option value="26" <c:if test="${kappList.MUSC_GNRE=='26' }">selected</c:if>>로고송</option>
														<option value="99" <c:if test="${kappList.MUSC_GNRE=='99' }">selected</c:if>>기타</option>
													</select>
												</td>
											</tr>
											</c:forEach>
										</c:if>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 항목 테이블 영역입니다 -->
								
								<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
								<div id="divKappFileList" class="tabelRound mb5" 
									style="display:<c:if test="${!empty trstList}">
														<c:forEach items="${trstList}" var="trstList">
															<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
																<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
															</c:if>
														</c:forEach>
													</c:if>;">

								</div>
								<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
								
								<!-- 보상금신청 오프라인 관련 영역 -->
								<div id="divKappOffline1" class="contentsRoundBox" >
									<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
									<div class="infoImg1">
										<p><strong>한국음원제작자협회 오프라인접수 선택한 경우 해당협회에 관련서류를 제출하여야합니다.</strong></p>
										<ul>
											<li>1. 방송보상금 신청서<br/> 
												&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금 신청현황조회 상세화면에서 방송보상금 신청서 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span>
											</li>
											<li class="mt3"><!-- 2. 음원명세서 및 해당 음반  -->2. 음원등록명세서와 저작인접물(음반,음원 등)<br/>
												&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금 신청현황조회 상세화면에서 음원명세서 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span>
											</li>
											<li class="mt3">3. 주민등록등본/법인등기부등본</li>
											<li class="mt3">4. 사업자등록증(사본) 1부</li>
											<li class="mt3">5. 인감증명서 1부</li>
											<li class="mt3">6. 통장사본 1부</li>
										</ul>
									</div>
								</div>
								<!-- //보상금신청 오프라인 관련 영역 -->
							</fieldset>
							<!-- //음제협 관련 영역 -->
						
							<!-- 실연자 관련 영역 -->
							<fieldset id="fldFokapo" style="display:
								<c:choose>
									<c:when test="${clientInfo.TRST_202 != '1'}">none</c:when>
									<c:when test="${clientInfo.FOKAPO != 'Y'}">none</c:when> 
								</c:choose>;">
								<legend>한국음악실연자협회 보상금신청 내용</legend>
								
								<div class="floatDiv mt10">
									<h5 class="fl">내용(한국음악실연자연합회)
										<span class="fontNormal ml10">
											<label>
												<input type="checkbox" id="chkFokapoOff" 
													onclick="javascript:fn_chgDiv('fokapo','OFF');" class="inputChk" 
													<c:if test="${!empty trstList}">
														<c:forEach items="${trstList}" var="trstList">
															<c:if test="${trstList.TRST_ORGN_CODE=='202' }">
																<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if>
															</c:if>
														</c:forEach>
													</c:if> title="오프라인접수(첨부서류)" />오프라인접수(첨부서류)</label>
											<input type="hidden" name="hddnFokapoOff" /> 
										</span>
									</h5>
									<p class="fr"><span id="spanFokapoBtn" class="button small type3"><a href="#" onclick="javascript:fn_delete('chkFokapo', 'fokapo_displaySeq'); return false;">행삭제</a></span></p>
								</div>
								
								<!-- 보상금신청 실연자정보 테이블 영역입니다 -->
								<div id="divFokapoInmtInfo" class="tabelRound mb5">
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="(한국음악실연자연합회)보상금신청 실연자정보 입력 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="30%">
										<col width="20%">
										<col width="30%">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col" class="headH white p12"><label class="necessary">실연자</label></th>
												<td>
													<label class="labelBlock"><em class="w45">본명</em><input type="text" name="txtFokapoPemrRlnm" value="${PRPS_NAME }" maxlength="16" title="실연자(본명)" class="inputData w60" /></label>
													<label class="labelBlock"><em class="w45">예명</em><input type="text" name="txtFokapoPemrStnm" value="${pemrStnm }" maxlength="16" title="실연자(예명)" class="inputData w60" /></label>
													<label class="labelBlock"><em class="w45">그룹명</em><input type="text" name="txtFokapoGrupName" value="${grupName }" maxlength="65" title="실연자(그룹명)" class="inputData w60" /></label>
												</td>
												<th scope="col" class="headH white p12"><label class="necessary">주민번호</label></th>
												<td>
													<!-- 
													<input type="text" name="txtFokapoResdNumb" value="${resdNumbStr }" title="실연자(주민번호)" class="inputData w80" />
													 -->
													 <input type="text" name="txtFokapoResdNumb_1" maxlength="6" Size="6" onkeypress="javascript:only_arabic(event);" value="${RESD_CORP_NUMB1 }" title="실연자(주민번호)" class="inputData w40" />
													 -
													 <input type="password" autocomplete="off" name="txtFokapoResdNumb_2" maxlength="7" Size="7" onkeypress="javascript:only_arabic(event);" value="${RESD_CORP_NUMB2 }" title="실연자(주민번호)" class="inputData w40" />
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 실연자정보 테이블 영역입니다 -->
								
								<!-- 보상금신청 항목 테이블 영역입니다 -->
								<div id="divFokapoItemList" class="tabelRound">
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="(한국음악실연자연합회)보상금신청 저작물 정보 입력폼입니다." style="table-layout:fixed;"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20px">
										<col width="20px">
										<col width="120px">
										<col width="80px">
										<col width="110px">
										<col width="80px">
										<col width="80px">
										<col width="110px">
										<col width="60px">
										</colgroup>
										<thead>
											<tr>
												<th scope="col" class="headH"><input type="checkbox" onclick="javascript:checkBoxToggle('frm','chkFokapo',this);" class="vmid" title="전체선택" /></th>
												<th scope="col" class="headH">번호</th>
												<th scope="col" class="headH"><label class="necessary">작품명</label></th>
												<th scope="col" class="headH"><label class="necessary">가수명</label></th>
												<th scope="col" class="headH">앨범명</th>
												<th scope="col" class="headH">앨범발매일</th>
												<th scope="col" class="headH">실연종류</th>
												<th scope="col" class="headH">실연내용</th>
												<th scope="col" class="headH">기여율(%)</th>
											</tr>
										</thead>
										<tbody>
										<c:if test="${empty fokapoList}">
											<c:if test="${!empty selectList}">
												<c:forEach items="${selectList}" var="selectList">	
													<c:if test="${selectList.FOKAPO == 'Y'}">
													<c:set var="kk" value="${kk+1}"/>
												<tr>
													<td class="ce"><input type="checkbox" name="chkFokapo" class="vmid" title="선택" /></td>
													<td class="ce"><input name="fokapo_displaySeq" id="fokapo_displaySeq_${kk}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${kk}"/></td>
													<td>
														${selectList.SDSR_NAME }
														<input type="hidden" name="hddnFokapoPdtnName" value="${selectList.SDSR_NAME }" title="작품명" />
														<input type="hidden" name="hddnFokapoPrpsIdnt" value="${selectList.INMT_SEQN }" />
														<input type="hidden" name="hddnFokapoPrpsIdntCode" value="${selectList.PRPS_IDNT_CODE }" />
													</td>
													<td class="ce"><input type="text" name="txtFokapoSnerName" value="" maxlength="16" title="가수명" class="inputData w90 ce" /></td>
													<td class="ce"><input type="text" name="txtFokapoAbumName" value="" maxlength="65" title="앨범명" class="inputData w90 ce" /></td>
													<td class="ce"><input type="text" name="txtFokapoAbumDateIssu" value="" title="앨범발매일" size="8" maxlength="8" onkeypress="javascript:only_arabic(event);" class="inputData w90 ce" /></td>
													<td class="ce"><input type="text" name="txtFokapoPemsKind" value="" maxlength="65" title="실연종류" class="inputData w90 ce" /></td>
													<td class="ce"><input type="text" name="txtFokapoPemsDesc" value="" maxlength="65" title="실연내용" class="inputData w90 ce" /></td>
													<td class="ce"><input type="text" name="txtFokapoCtbtRate" value="" title="기여율" class="inputData w90 ce" /></td>
												</tr>
													</c:if>
												</c:forEach>
											</c:if>
										</c:if>
										
										<c:if test="${!empty fokapoList}">
											<c:forEach items="${fokapoList}" var="fokapoList">	
											<c:set var="kk" value="${kk+1}"/>
											<tr>
												<td class="ce"><input type="checkbox" name="chkFokapo" class="vmid" title="선택"/></td>
													<td class="ce"><input name="fokapo_displaySeq" id="fokapo_displaySeq_${kk}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${kk}"/></td>
												<td>
													${fokapoList.PDTN_NAME }
													<input type="hidden" name="hddnFokapoPdtnName" value="${fokapoList.PDTN_NAME }" title="작품명" />
													<input type="hidden" name="hddnFokapoPrpsIdnt" value="${fokapoList.PRPS_IDNT }" />
													<input type="hidden" name="hddnFokapoPrpsIdntCode" value="${fokapoList.PRPS_IDNT_CODE }" />
												</td>
												<td class="ce"><input type="text" name="txtFokapoSnerName" value="${fokapoList.SNER_NAME }" maxlength="16" title="가수명" class="inputData w90 ce" /></td>
												<td class="ce"><input type="text" name="txtFokapoAbumName" value="${fokapoList.ABUM_NAME }" maxlength="65" title="앨범명" class="inputData w90 ce" /></td>
												<td class="ce"><input type="text" name="txtFokapoAbumDateIssu" value="${fokapoList.ABUM_DATE_ISSU }" title="앨범발매일" class="inputData w90 ce" /></td>
												<td class="ce"><input type="text" name="txtFokapoPemsKind" value="${fokapoList.PEMS_KIND }" maxlength="65"  title="실연종류" class="inputData w90 ce" /></td>
												<td class="ce"><input type="text" name="txtFokapoPemsDesc" value="${fokapoList.PEMS_DESC }" maxlength="65" title="실연내용"  class="inputData w90 ce" /></td>
												<td class="ce"><input type="text" name="txtFokapoCtbtRate" value="${fokapoList.CTBT_RATE }" title="기여율" class="inputData w90 ce" /></td>
											</tr>
											</c:forEach>
										</c:if>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 항목 테이블 영역입니다 -->
								
								<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
								<div id="divFokapoFileList" class="tabelRound mb5" 
									style="display:<c:if test="${!empty trstList}">
														<c:forEach items="${trstList}" var="trstList">
															<c:if test="${trstList.TRST_ORGN_CODE=='202' }">
																<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
															</c:if>
														</c:forEach>
													</c:if>;">

								</div>
								<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
							
								<!-- 보상금신청 오프라인 관련 영역 -->
								<div id="divFokapoOffline1" class="contentsRoundBox" >
									<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
									<div class="infoImg1">
										<p><strong>한국음악실연자연합회 오프라인접수 선택한 경우 해당연합회에 관련서류를 제출하여야합니다.</strong></p>
										<ul>
											<li>1. 방송보상금 신청서 <br/>
												&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금 신청현황조회 상세화면에서 방송보상금 신청서 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span>
											</li>
											<li class="mt3">2. 실연자임을 증명할 수 있는 서류<br/>
												&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(앨범자켓, 저작권등록증, 저작권인증서, 실연사실확인서의 사본 등)</span>
											</li>
											<li class="mt3">3. 상속, 양도, 승계 등의 사실을 증명할 수 있는 서류</li>
											<li class="mt3">4. 주민등록등본</li>
											<li class="mt3">5. 통장사본 1부</li>
										</ul>
									</div>
								</div>
								<!-- //보상금신청 오프라인 관련 영역 -->
							</fieldset>
							<!-- 실연자 관련 영역 -->
						
							<fieldset id="fldOffline" class="mt10">
								<legend>보상금신청 내용</legend>
								
								<!-- 보상금신청(내용) 테이블 영역입니다 -->
								<div id="divContent"  class="tabelRound mb5">
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금신청 내용입력 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col" class="headH white p12"><label for="txtareaPrpsDesc" class="necessary">내용</label></th>
												<td>
													<textarea cols="10" rows="10" name="txtareaPrpsDesc" id="txtareaPrpsDesc" title="내용" class="h100">${clientInfo.PRPS_DESC }</textarea>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청(내용) 테이블 영역입니다 -->
								
							</fieldset>
						
					</div>
					
					</form>
					
					<div class="floatDiv mt10">
						<p class="fl"><span class="button medium icon"><span class="list">&nbsp;</span><a href="#" onclick="javascript:fn_list()">목록</a></span></p>
						<p class="fr"><span class="button medium icon"><span class="default">&nbsp;</span><a href="#" onclick="javascript:fn_chkSubmit();">보상금 신청</a></span></p>
					</div>
						
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	<SPAN id="helpMessage" style="position:absolute;top:0px;left:0px;height:0px;width:0px;background-color:#ffffff;display:none;z-index:5;border : 1 solid #aaaaaa;">
	
<form name="srchForm" action="">
<input type="hidden" name="srchDIVS"		value="${srchParam.srchDIVS }" />
<input type="hidden" name="srchSdsrName"	value="${srchParam.srchSdsrName }" />
<input type="hidden" name="srchMuciName"	value="${srchParam.srchMuciName }" />
<input type="hidden" name="srchYymm"		value="${srchParam.srchYymm }" />
<input type="hidden" name="nowPage"			value="${srchParam.nowPage }" />
</form>

<script type="text/javascript" src="/js/2010/file.js"></script>
<script type="text/javascript">
<!--
	function fn_setFileInfo(){
		var listCnt = '${fn:length(fileList)}';
		<c:if test="${!empty fileList}">
			<c:forEach items="${fileList}" var="fileList">
				<c:choose>
					<c:when test="${fileList.TRST_ORGN_CODE=='203' }">
						fn_addGetFile(	'203',
										'${fileList.PRPS_MAST_KEY}',
										'${fileList.PRPS_SEQN}',
										'${fileList.ATTC_SEQN}',
										'${fileList.FILE_PATH}',
										'${fileList.REAL_FILE_NAME}',
										'${fileList.FILE_NAME}',
										'${fileList.TRST_ORGN_CODE}',
										'${fileList.FILE_SIZE}'
										);
					</c:when>
					<c:when test="${fileList.TRST_ORGN_CODE=='202' }">	
						fn_addGetFile(	'202',
										'${fileList.PRPS_MAST_KEY}',
										'${fileList.PRPS_SEQN}',
										'${fileList.ATTC_SEQN}',
										'${fileList.FILE_PATH}',
										'${fileList.REAL_FILE_NAME}',
										'${fileList.FILE_NAME}',
										'${fileList.TRST_ORGN_CODE}',
										'${fileList.FILE_SIZE}'
										);
					</c:when>
				</c:choose>
			</c:forEach>
		</c:if>		
	}

	window.onload	= function(){	fn_createTable("divKappFileList", "203");
									fn_createTable("divFokapoFileList", "202");	
									fn_setFileInfo();
									resizeDiv('divKappItemList' , 'div_scroll');
									
									fn_confirm();
								}
//-->
</script>
</body>
</html>
