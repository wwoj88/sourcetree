<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript">
<!--
	//목록으로 이동
	function fn_list(){
		var frm = document.frm;
	
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=list&amp;gubun=edit&amp;srchDIVS=${srchParam.srchDIVS }";
		frm.submit();
	}
	
	//뒤로가기
	function fn_prePage(){
		var frm = document.frm;
	
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&amp;gubun=&amp;srchDIVS=${srchParam.srchDIVS }";
		frm.submit();
	}
	
	
	//보상금신청
	function fn_chkSubmit(){
		var frm = document.frm;
		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrpsSave";
		frm.submit();
	}
	
	
	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.frm;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;
	
		frm.method = "post";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
	}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_2.png" alt="보상금신청" title="보상금신청" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악<span>&nbsp;</span></a></li>
							<li class="active"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용<span>&nbsp;</span></a></li>
							<li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관<span>&nbsp;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>보상금신청</span>&gt;<strong>교과용보상금</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>보상금 신청
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
								
					<form name="frm" action="" class="sch">
						<input type="hidden" name="srchDIVS"		value="${srchParam.srchDIVS }"/>
						<input type="hidden" name="srchSdsrName"	value="${srchParam.srchSdsrName }" />
						<input type="hidden" name="srchMuciName"	value="${srchParam.srchMuciName }" />
						<input type="hidden" name="srchYymm"		value="${srchParam.srchYymm }" />
						<input type="hidden" name="gubun"			value="back" />
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }" />
						
						<input type="hidden" name="filePath">
						<input type="hidden" name="fileName">
						<input type="hidden" name="realFileName">
					
<!-- memo 삽입 -->
<jsp:include page="/common/memo/memo_01.jsp">
	<jsp:param name="DIVS" value="${srchParam.srchDIVS }"/>
</jsp:include>
<!-- // memo 삽입 -->

					<div class="bbsSection">
						<h4>교과용 보상금 신청</h4>
							<fieldset>
								<legend>신청자 정보</legend>
								
								<!-- 보상금신청(기본정보) 테이블 영역입니다 -->
								<div class="tabelRound">
									<span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span><!-- 라운딩효과 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금 신청정보 입력 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
											<col width="20%">
											<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row" class="bgbr lft">보상금종류</th>
												<td>교과용</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청자</th>
												<td>
												
													<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력정보 입니다.">
														<colgroup>
															<col width="15%">
															<col width="*">
															<col width="15%">
															<col width="35%">
														</colgroup>
														<tbody>
															<tr>
																<th class="bgbr3 lft">성명</th>
																<td>${clientInfo.PRPS_NAME }</td>
																<th class="bgbr3 lft">주민번호/사업자번호</th>
																<td>${clientInfo.RESD_CORP_NUMB }</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">전화</th>
																<td>
																	<label class="labelBlock"><em class="w45">자택</em>${clientInfo.HOME_TELX_NUMB }</label>
																	<label class="labelBlock"><em class="w45">사무실</em>${clientInfo.BUSI_TELX_NUMB }</label>
																	<label class="labelBlock"><em class="w45">휴대폰</em>${clientInfo.MOBL_PHON }</label>
																</td>
																<th class="bgbr3 lft">Fax</th>
																<td>${clientInfo.FAXX_NUMB }</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">이메일주소</th>
																<td>${clientInfo.MAIL }</td>
																<th class="bgbr3 lft">입금처</th>
																<td>
																	<label class="labelBlock"><em class="w50">은행명</em>${clientInfo.DPTR }</label>
																	<label class="labelBlock"><em class="w50">계좌번호</em>${clientInfo.ACCT_NUMB }</label>
																	<label class="labelBlock"><em class="w50">예금주</em>${clientInfo.DPTR }</label>
																</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">주소</th>
																<td colspan="3" class="vtop">
																	<label class="labelBlock"><em class="w45">자택</em>${clientInfo.HOME_ADDR }</label>
																	<label class="labelBlock"><em class="w45">사무실</em>${clientInfo.BUSI_ADDR }</label>
																</td>
															</tr>
														</tbody>
													</table>
													
													<!-- 전화면에서 넘어온 값듯 -->
													<input type="hidden" name="hddnInsertKrtra"		value="<c:if test="${clientInfo.TRST_205 == '1'}">Y</c:if>" />
													<input type="hidden" name="hddnUserIdnt"		value="${clientInfo.USER_IDNT }"/>
													<input type="hidden" name="txtPrpsName"			value="${clientInfo.PRPS_NAME }"/>
													<input type="hidden" name="txtPrsdCorpNumbView"	value="${clientInfo.RESD_CORP_NUMB }"/>
													<input type="hidden" name="txtHomeTelxNumb"		value="${clientInfo.HOME_TELX_NUMB }"/>
													<input type="hidden" name="txtBusiTelxNumb"		value="${clientInfo.BUSI_TELX_NUMB }"/>
													<input type="hidden" name="txtMoblPhon"			value="${clientInfo.MOBL_PHON }"/>
													<input type="hidden" name="txtFaxxNumb"			value="${clientInfo.FAXX_NUMB }"/>
													<input type="hidden" name="txtHomeAddr"			value="${clientInfo.HOME_ADDR }"/>
													<input type="hidden" name="txtBusiAddr"			value="${clientInfo.BUSI_ADDR }"/>
													<input type="hidden" name="txtBankName"			value="${clientInfo.BANK_NAME }"/>
													<input type="hidden" name="txtAcctNumb"			value="${clientInfo.ACCT_NUMB }"/>
													<input type="hidden" name="txtDptr"				value="${clientInfo.DPTR }"/>
													<input type="hidden" name="txtMail"				value="${clientInfo.MAIL }"/>
													<input type="hidden" name="hddnTrst203"			value="${clientInfo.TRST_203 }"/>
													<input type="hidden" name="hddnTrst202"			value="${clientInfo.TRST_202 }"/>
													<input type="hidden" name="hddnTrst205"			value="${clientInfo.TRST_205 }"/>
													<input type="hidden" name="hddnTrst205_2"		value="${clientInfo.TRST_205_2 }"/>
													<input type="hidden" name="txtareaPrpsDesc"		value="${clientInfo.PRPS_DESC }"/>
												   	<input type="hidden" name="prpsDoblCode"	value="${prpsDoblCode }" />
													<input type="hidden" name="rghtPrpsMastKey"	value="${rghtPrpsMastKey }" />	
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청분류</th>
												<td>
												    <label><input type="checkbox" <c:if test="${clientInfo.TRST_205 == '1'}">checked</c:if> disabled class="inputRChk" title="교과용 보상금"/>교과용 보상금</label>
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청저작물</th>
												<td>
												
													<!-- 테이블 영역입니다 -->
													<div class="tabelRound overflow_y h100">
														<table cellspacing="0" cellpadding="0" border="1" class="grid w95" summary="보상금신청 저작물 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
																<col width="8%">
																<col width="52%">
																<col width="25%">
																<col width="15%">
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH">도서명</th>
																	<th scope="col" class="headH">저자</th>
																	<th scope="col" class="headH">사용년도</th>
																</tr>
															</thead>
															<tbody>
															<c:if test="${!empty tempList}">
																<c:forEach items="${tempList}" var="tempList">	
																	<c:set var="NO" value="${tempListCnt + 1}"/>
																	<c:set var="i" value="${i+1}"/>
																<tr>
																	<td class="ce">
																		${NO - i }
																		<!-- 전화면에서 넘어온 값듯 -->
																		<input type="hidden" name="hddnSelectInmtSeqn"	value="${tempList.INMT_SEQN }"/>
																		<input type="hidden" name="hddnSelectPrpsIdntCode" value="${tempList.PRPS_IDNT_CODE }" />
																		<input type="hidden" name="hddnSelectPrpsDivs"	value="${tempList.PRPS_DIVS }"/>
																		<input type="hidden" name="hddnSelectKapp"		value="${tempList.KAPP }"/>
																		<input type="hidden" name="hddnSelectFokapo"	value="${tempList.FOKAPO }"/>
																		<input type="hidden" name="hddnSelectKrtra"		value="${tempList.KRTRA }"/>
																		<input type="hidden" name="hddnSelectSdsrName"	value="${tempList.SDSR_NAME }"/>
																		<input type="hidden" name="hddnSelectMuciName"	value="${tempList.MUCI_NAME }"/>
																		<input type="hidden" name="hddnSelectYymm"		value="${tempList.YYMM }"/>
																		<input type="hidden" name="hddnSelectLishComp"	value="${tempList.LISH_COMP }"/>
																		<input type="hidden" name="hddnSelectUsexType"	value="${tempList.USEX_TYPE }"/>
																	</td>
																	<td>${tempList.SDSR_NAME }</td>
																	<td class="ce">${tempList.MUCI_NAME }</td>
																	<td class="ce">${tempList.YYMM }</td>
																</tr>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
													</div>
													<!-- //테이블 영역입니다 -->
													
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청(기본정보) 테이블 영역입니다 -->
								
							</fieldset>
					</div>
										
		<c:if test="${!empty trstList}">
			<c:forEach items="${trstList}" var="trstList">
				<c:if test="${trstList.TRST_ORGN_CODE=='205' }">
                    <!-- 복전협 관련 영역 -->
					<div class="bbsSection">
						<div class="floatDiv">
							<h4 class="fl">내용(한국복사전송권협회) 교과용 보상금
							    <span class="labelFont">
							        <label><input type="checkbox" class="inputChk ml10" <c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if> disabled title="오프라인접수(첨부서류)" />오프라인접수(첨부서류)</label>
									<input type="hidden" name="hddnKrtraOff" value="${trstList.OFFX_LINE_RECP }"/> 
							    </span>
							</h4>
						</div>
						
						<div>
							<!-- 보상금신청 권리자정보 테이블 영역입니다 -->
							<div class="tabelRound">
    							<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금신청 권리자정보 입력 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
    								<colgroup>
    								<col width="15%">
    								<col width="40%">
    								<col width="15%">
    								<col width="*">
    								</colgroup>
    								<tbody>
    									<tr>
    										<th class="bgbr">권리자</th>
    										<td>
    											<label class="labelBlock"><em class="w45">본명</em>${pemrRlnm }</label>
    											<label class="labelBlock"><em class="w45">필명</em>${grupName }</label>
    											<label class="labelBlock"><em class="w45">예명</em>${pemrStnm }</label>
    										</td>
										    <th class="bgbr">주민번호</th>
										    <td>
										    	<!-- ${resdNumbStr } -->
										    	<input type="text" name="txtKrtraResdNumb_1" value="${resdNumbStr_1 }" class="whiteR w20" />
										    	-
										    	<input type="text" name="txtKrtraResdNumb_2" value="${resdNumbStr_2 }" class="whiteL w20" />
										    	
										    	<!-- 전화면에서 넘어온 값듯 -->
										    	<input type="hidden" name="txtKrtraPemrRlnm"	value="${pemrRlnm }"/>
										    	<input type="hidden" name="txtKrtraPemrStnm"	value="${pemrStnm }"/>
										    	<input type="hidden" name="txtKrtraGrupName"	value="${grupName }"/>
										    	<input type="hidden" name="txtKrtraResdNumb"	value="${resdNumbStr }"/>
										    </td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //보상금신청 권리자정보 테이블 영역입니다 -->
														
							<!-- 보상금신청 항목 테이블 영역입니다 -->
							<div id="divFokapoItemList" class="tabelRound">
    							<table cellspacing="0" cellpadding="0" border="1" class="clear grid topLine mt5" summary="보상금신청 저작물 정보 입력정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
    								<colgroup>
										<col width="6%">
										<col width="11%">
										<col width="11%">
										<col width="11%">
										<col width="12%">
										<col width="13%">
										<col width="8%">
										<col width="8%">
										<col width="10%">
    								</colgroup>
    								<thead>
    									<tr>
    										<th scope="col" rowspan="2" class="headH">번호</th>
    										<th scope="col" rowspan="2" class="headH">저작물종류</th>
    										<th scope="col" rowspan="2" class="headH">저작물명</th>
    										<th scope="col" rowspan="2" class="headH">저작자명</th>
    										<th scope="col" colspan="5" class="headH">이용현황</th>
    									</tr>
    									<tr>
    										<th scope="col" class="headH">학교급 및 학교</th>
    										<th scope="col" class="headH">발행년도 및 학기</th>
    										<th scope="col" class="headH">교과목</th>
    										<th scope="col" class="headH">출판사</th>
    										<th scope="col" class="headH">이용페이지</th>
    									</tr>
    								</thead>
    								<tbody>
									<c:if test="${!empty krtraList}">
										<c:forEach items="${krtraList}" var="krtraList">	
											<c:set var="krtraNo" value="${krtraNo+1}"/>
										<tr>
    										<td class="ce">
    											${krtraNo }
												<!-- 전화면에서 넘어온 값듯 -->
												<input type="hidden" name="hddnKrtraPrpsIdnt" 	value="${krtraList.INMT_SEQN }" />
												<input type="hidden" name="hddnKrtraPrpsDivs" 	value="${krtraList.PRPS_DIVS }" />
												<input type="hidden" name="hddnKrtraPrpsIdntCode" 	value="${krtraList.PRPS_IDNT_CODE }" />
											
												<input type="hidden" name="hddnKrtraCoprKind"	value="${krtraList.COPT_KIND }" />
												<input type="hidden" name="hddnKrtraWorkName"	value="${krtraList.WORK_NAME }" />
												<input type="hidden" name="hddnKrtraWrtrName"	value="${krtraList.WRTR_NAME }" />
												<input type="hidden" name="hddnKrtraScyr"		value="${krtraList.SCYR }" />
												<input type="hidden" name="hddnKrtraSctr"		value="${krtraList.SCTR }" />
												<input type="hidden" name="hddnKrtraSjet"		value="${krtraList.SJET }" />
												<input type="hidden" name="hddnKrtraBookCncn"	value="${krtraList.BOOK_CNCN }" />
												<input type="hidden" name="hddnKrtraWorkDivs"	value="${krtraList.WORK_DIVS }" />
											</td>
    										<td class="ce">${krtraList.COPT_KIND }</td>
    										<td>${krtraList.WORK_NAME }</td>
    										<td>${krtraList.WRTR_NAME }</td>
    										<td class="ce">${krtraList.SCYR }</td>
    										<td class="ce">${krtraList.SCTR }</td>
    										<td class="ce">${krtraList.SJET }</td>
    										<td class="rgt">${krtraList.BOOK_CNCN }</td>
    										<td class="rgt">${krtraList.WORK_DIVS }</td>
    									</tr>
										</c:forEach>
									</c:if>
    								</tbody>
    							</table>
							</div>
							<!-- //보상금신청 항목 테이블 영역입니다 -->
							
							<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
							<div id="divFokapoFileList" class="tabelRound mb5" style="display:<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>;">
								<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금신청 첨부파일입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
									<colgroup>
									<col width="20%">
									<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="col" class="headH white p12">첨부서류</th>
											<td>
												<dl class="fl categori w100">
													<dt><strong>파일이름</strong></dt>
													<dd>
														<table class="w100">
															<colgroup>
																<col>
															</colgroup>
															<tbody>
																<c:if test="${!empty fileList}">
																	<c:forEach items="${fileList}" var="fileList">	
																<tr>
																	<td style="padding:0px;">${fileList.FILE_NAME}
																		<input type="hidden" name="hddnGetRealFileName"	value="${fileList.REAL_FILE_NAME }"/>
																		<input type="hidden" name="hddnGetFilePath"		value="${fileList.FILE_PATH }"/>
																		<input type="hidden" name="hddnGetFileName"		value="${fileList.FILE_NAME }"/>
																		<input type="hidden" name="hddnGetFileSize"		value="${fileList.FILE_SIZE }"/>
																		<input type="hidden" name="hddnGetFileOrgnCode"	value="${fileList.TRST_ORGN_CODE }"/>
																	</td>
																</tr>
																	</c:forEach>
																</c:if>
																<c:if test="${empty fileList}">
																	<tr>
																		<td class="ce" style="padding: 0px;">파일이 없습니다.</td>
																	</tr>
																</c:if>
															</tbody>
														</table>
													</dd>
												</dl>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
								
							<!-- 보상금신청 오프라인 관련 영역 -->
							<div id="divKrtraOffline1" class="contentsRoundBox" style="display:<c:if test="${trstList.OFFX_LINE_RECP!='Y' }">none</c:if>;">
								<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
								<div class="infoImg1">
									<p><strong>한국복사전송권협회(교과용보상금) 오프라인접수 선택한 경우 해당협회에 관련서류를 제출하여야합니다.</strong></p>
									<ul>
										<li>1. 교과용보상금 신청서<br/>
											&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금 신청현황조회 상세화면에서 교과용보상금 신청서 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span>
										</li>
										<li class="mt3">2. 권리자임을 증명할 수 있는 서류(저작권등록증 등)</li>
										<li class="mt3">3. 상속, 양도, 승계 등의 사실을 증명할 수 있는 서류</li>
										<li class="mt3">4. 주민등록등본</li>
										<li class="mt3">5. 통장사본</li>
									</ul>
								</div>
							</div>
							<!-- //보상금신청 오프라인 관련 영역 -->
							
						</div>
					</div>
					<!-- 복전협 관련 영역 -->
				</c:if>
			</c:forEach>
		</c:if>
		
					<!-- 보상금신청(내용) 테이블 영역입니다 -->
					<div class="bbsSection">
						<div id="divContent"  class="tabelRound mb5">
							<table cellspacing="0" cellpadding="0" border="1" class="clear grid topLine mt5" summary="보상금신청 내용입력 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
									<col width="15%">
									<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th class="bgbr">내용</th>
										<td><p class="overflow_y h100">${clientInfo.PRPS_DESC }</p></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- //보상금신청(내용) 테이블 영역입니다 -->
					</form>
					
					<div class="floatDiv mt10">
						<p class="fl">
							<span class="button medium icon"><span class="list">&nbsp;</span><a href="#" onclick="javascript:fn_list();">목록</a></span>
						</p>
						<p class="fl" style = "margin-left:10px;">
							<span class="button medium icon"><span class="list">&nbsp;</span><a href="#" onclick="javascript:fn_prePage();">이전화면</a></span>
						</p>
						<p class="rgt">
							<span class="button medium icon"><span class="default">&nbsp;</span><a href="#" onclick="javascript:fn_chkSubmit();">보상금 신청</a></span>
						</p>
					</div>
						
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
