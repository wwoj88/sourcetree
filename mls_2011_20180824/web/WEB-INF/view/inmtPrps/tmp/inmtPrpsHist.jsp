<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금 신청내역 조회 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/popup.css">
<style type="text/css">

body {
	overflow-x: auto;
	margin: 0 0 0 0 ; 
}

.div_scroll {
	overflow-x: hidden;
	overflow-y: auto;
	margin-left: 0px;
}
-->
</style>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop">
	
		<!-- Header str -->
		<div id="popHeader">
			<h1>보상금 신청내역 조회</h1>
		</div>
		<!-- //Header end -->
		
		<!-- Container str -->
		<div id="popContainer">
			<div class="popContent">
				
				<h2>저작물 정보</h2>
				
				<!-- 테이블 영역입니다 -->
				<div class="tabelRound mt10">
					<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금 신청내역 저작물명, 보상금 신청횟수 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<colgroup>
						<col width="20%">
						<col width="30%">
						<col width="20%">
						<col width="*">
						</colgroup>
						
						<tbody>
							<tr>
								<th class="ce bgbr">저작물명</th>
								<td colspan="3"><b>${inmtPrpsHist[0].TITLE}</b></td>
							</tr>
							<tr>
								<th class="ce bgbr">보상금 신청 횟수</th>
								<td colspan="3">${totalrow} 회</td>
							</tr>
						</tbody>	
					</table>
					
					<h3>&nbsp;</h3>
					<h2>보상금 신청내역</h2>
					
					<div class="tabelRound div_scroll" style="width:100%;height:190px;padding:0 0 0 0;">
					<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금 신청내역 신청순번, 신청일자, 진행상태 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<colgroup>
						<col width="20%">
						<col width="40%">
						<col width="*">
						</colgroup>
						<tbody>
							<tr>
								<th class="ce bgbr">신청순번</th>
								<th class="ce bgbr">신청일자</th>
								<th class="ce bgbr">진행상태</th>
							</tr>
						
								<c:forEach items="${inmtPrpsHist}" var="inmtPrpsHist">
								<tr>
									<td class="ce">${totalrow - i}<c:set var="i" value="${i+1}"/></td>
									<td class="ce">${inmtPrpsHist.RGST_DTTM}</td>
									<td class="ce">${inmtPrpsHist.TOTAL_STAT}</td>
								</tr>
								</c:forEach>

						</tbody>
					</table>
					</div>
					
				</div>
				
				<!-- //테이블 영역입니다 -->
			</div>
		</div>
		<!-- //Container end -->
		
		<!-- Footer str -->
		<div id="popFooter">
			<p class="copyright">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		</div>
		<!-- Footer end -->
		
		<p class="close"><a href="javascript:window.close()"><img src="/images/2010/pop/close.gif" alt="X" title="이 창을 닫습니다."/></a></p>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>