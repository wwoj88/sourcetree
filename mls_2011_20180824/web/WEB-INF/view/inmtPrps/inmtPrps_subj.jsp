<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금발생저작물 조회 | 저작권찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<style type="text/css">
<!--
.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
.w55{width:55%;}
.necessary{ background: url(/images/2012/common/necessary.gif) no-repeat 98% 2px !important; padding-right: 10px !important;}/* 필수 */
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>


<script type="text/JavaScript">
<!--
	
//체크박스에 따른 화면 설정(첨부파일,오프라인)
	function fn_chgInsertDiv(sOrgn, obj){
		var frm = document.frm;
	
		if(obj.checked){
			if(sOrgn == 'krtra'){
				frm.hddnInsertKrtra.value = 'Y';
			}
			fn_chgDiv(sOrgn, 'Y');
		}else{
			if(sOrgn == 'krtra'){
				if(confirm('교과용 보상금 선택해제 시 관련 선택저작물이 신청이 되지않습니다.\r\n선택해제 하시겠습니까?')){
					frm.hddnInsertKrtra.value = 'N';
				}else{
					obj.checked	= true;
					return;
				}
			}
			fn_chgDiv(sOrgn, 'N');
		}
	}
	
	function openSmplDetail(div) {

		var param = '';
		
		param = 'DVI='+div;
		
		var url = '/common/rghtPrps_smpl.jsp?'+param
		var name = '';
		var openInfo = 'target=rghtPrps_mvie, width=705, height=570, scrollbars=yes';
		
		window.open(url, name, openInfo);
	}

	//체크박스에 따른 화면 설정(첨부파일,오프라인)
	function fn_chgDiv(sOrgn,sDiv){

		var oFldKrtra = document.getElementById("fldKrtra");
		
		var oInmtInfo, oItemList, oFileList, oOffLine;	//div
		var chkOff;	//checkbox
		var oBtn;

		oInmtInfo	= document.getElementById("divKrtraInmtInfo");
		oItemList	= document.getElementById("divKrtraItemList");
		oFileList	= document.getElementById("divKrtraFileList");
		oOffLine	= document.getElementById("divKrtraOffline1");
		oBtn		= document.getElementById("spanKrtraBtn");

		chkOff	= document.getElementById("chkKrtraOff");

		if(sDiv == 'OFF'){
			if(chkOff.checked){
				oInmtInfo.style.display = '';
				oItemList.style.display	= '';
				oFileList.style.display = 'none';
				oOffLine.style.display	= '';
				oBtn.style.display		= '';
			}else{
				oInmtInfo.style.display = '';
				oItemList.style.display	= '';
				oFileList.style.display = '';
				oBtn.style.display		= '';
				oOffLine.style.display	= '';
			}
		}else if(sDiv == 'Y'){
			oFldKrtra.style.display = '';
		}else if(sDiv == 'N'){
			oFldKrtra.style.display = 'none';
		}
	}

	// 선택된 저작물 삭제
	function fn_delete(sStr){
		var frm = document.frm;
		
		//선택 목록
		var chkObjs = document.getElementsByName(sStr);
		var sCheck = 0;
		
		for(i=0; i<chkObjs.length;i++){
			if(chkObjs[i].checked){
				sCheck = 1;
			}
		}
		
		if(sCheck == 0 ){
			alert('선택된 저작물이 없습니다.');
			return;
		}
		
		for(var i=chkObjs.length-1; i >= 0; i--){
			var chkObj = chkObjs[i];
			
			if(chkObj.checked){
				// 선택된 저작물을 삭제한다.
				var oTR = findParentTag(chkObj, "TR");
				if(eval(oTR)) 	oTR.parentNode.removeChild(oTR);
			}
		}
	}

	//보상금신청
	function fn_chkSubmit(){
		var frm = document.frm;

		//신청자 정보 필수체크
		var oFldInmt	= document.getElementById("fldInmtInfo");
		var oInmt	= oFldInmt.getElementsByTagName("input");

		var txtHomeAddr	= ""; var txtBusiAddr	= "";
		var txtHomeTelxNumb	= "";  var txtBusiTelxNumb	= "";  var txtMoblPhon	= "";  var txtFaxxNumb	= "";  var txtMail = "";

		/*
		txtHomeAddr	= document.getElementById("txtHomeAddr").value;
		txtBusiAddr	= document.getElementById("txtBusiAddr").value;
		
		txtHomeTelxNumb	= document.getElementById("txtHomeTelxNumb").value;
		txtBusiTelxNumb	= document.getElementById("txtBusiTelxNumb").value;
		txtMoblPhon	= document.getElementById("txtMoblPhon").value;
		txtFaxxNumb	= document.getElementById("txtFaxxNumb").value;
*/
		txtHomeAddr	= document.getElementsByName("txtHomeAddr")[0].value;
		txtBusiAddr	= document.getElementsByName("txtBusiAddr")[0].value;
		
		txtHomeTelxNumb	= document.getElementsByName("txtHomeTelxNumb")[0].value;
		txtBusiTelxNumb	= document.getElementsByName("txtBusiTelxNumb")[0].value;
		txtMoblPhon	= document.getElementsByName("txtMoblPhon")[0].value;
		txtFaxxNumb	= document.getElementsByName("txtFaxxNumb")[0].value;
		txtMail 	= document.getElementsByName("txtMail")[0].value;
		
		for(i = 0; i < oInmt.length; i++){
			/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
			if(checkField(oInmt[i]) == false){
				return;
			}
			*/
			
			// 전화번호
			if(oInmt[i].name == 'txtHomeTelxNumb'
				|| oInmt[i].name == 'txtBusiTelxNumb'
				|| oInmt[i].name == 'txtMoblPhon'
				|| oInmt[i].name == 'txtFaxxNumb'){
				
				if(txtHomeTelxNumb == ''){
					if(txtBusiTelxNumb == ''){
						if(txtMoblPhon == ''){
							if(!nullCheck(oInmt[i]))	return;
						}
					}
				}
				
				if(!character(oInmt[i],  'EK'))	return;
			}
			
			
			// 자택 사무실 주소 nullCheck
			if(oInmt[i].name == 'txtHomeAddr'
				|| oInmt[i].name == 'txtBusiAddr'){
				if(txtHomeAddr == ''){
					if(txtBusiAddr == ''){
						if(!nullCheck(oInmt[i]))	return;
					}	
				}
			}
			
			// 입금처
			if(oInmt[i].name == 'txtBankName'
				|| oInmt[i].name == 'txtAcctNumb'
				|| oInmt[i].name == 'txtDptr'){
				
				if(!nullCheck(oInmt[i]))	return;
			}
			
			// E-mail
			if(oInmt[i].name == 'txtMail'){
				if(!character(oInmt[i],  'K'))	return;
			}

		}
		

		//체크박스 값들
		var vChkKrtraOff	= document.getElementById("chkKrtraOff").checked;
		if(vChkKrtraOff)	frm.hddnKrtraOff.value = "Y";

		var oChk205 = document.getElementById("chkTrts205");
		
		//복전협 정보 필수체크
		if(oChk205.checked){
			var oFldKrtra	= document.getElementById("fldKrtra");
			var oKrtra	= oFldKrtra.getElementsByTagName("input");
			for(i = 0; i < oKrtra.length; i++){
				/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
				if(checkField(oKrtra[i]) == false){
					return;
				}
				*/
				// nullCheck
				if(oKrtra[i].name == 'txtKrtraPemrRlnm'
					||oKrtra[i].name == 'txtKrtraResdNumb_1'
					||oKrtra[i].name == 'txtKrtraResdNumb_2'){
					
					if(!nullCheck(oKrtra[i]))	return;
				}

				// 글자길이 범위 체크
				if(oKrtra[i].name == 'txtKrtraResdNumb_1'){
					if(!rangeSize(oKrtra[i], '6'))	return;
				}
				
				if(oKrtra[i].name == 'txtKrtraResdNumb_2'){
					if(!rangeSize(oKrtra[i], '7'))	return;
				}
				
				// 금지할 문자 종류 체크 (E : 영문, K : 한글, N : 숫자)
				if(oKrtra[i].name == 'txtKrtraResdNumb_1'
					||oKrtra[i].name == 'txtKrtraResdNumb_2'){
					
					if(!character(oKrtra[i],  'EKS'))	return;
				}
			}
			
			//신청저작물 체크
			var oKrtraPrpsIdnt = document.getElementsByName("hddnKrtraPrpsIdnt");
			for(i = 0; i < oKrtraPrpsIdnt.length; i++){
				if(oKrtraPrpsIdnt[i].value == null || (oKrtraPrpsIdnt[i].value).length < 1){
					alert('내용(복사전송권협회) 정보를 입력해주세요.');
					return;
				}
			}
		}

		//내용 필수 체크
		var oContent	= document.getElementById("txtareaPrpsDesc");
		/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
		if(checkField(oContent) == false){
			return;
		}
		*/
		// nullCheck
		if(!nullCheck(oContent))	return;
		if(onkeylengthMax(oContent, 4000, 'txtareaPrpsDesc') == false){
			return;
		}

		if(oChk205.checked == false){
			alert('분류(을)를 선택 해 주세요.');
			return;
		}
		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrpsChk";
		frm.submit();
	}
	
	
	//목록으로 이동
	function fn_list(){
		var frm = document.srchForm;

		//frm.srchSdsrName.value	= '';
		//frm.srchMuciName.value	= '';
		//frm.srchYymm.value		= '';
		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=list&amp;gubun=edit&amp;srchDIVS=2";
		frm.submit();
	}

    //maxlength 체크
	function onkeylengthMax(formobj, maxlength, objname) {  
	    var li_byte     = 0;  
	    var li_len      = 0;  
	    for(var i=0; i< formobj.value.length; i++){  
	    	
	        if (escape(formobj.value.charAt(i)).length > 4){  
	            li_byte += 3;  
	        } else {  
	            li_byte++;  
	        }  
	        if(li_byte <= maxlength) {  
	            li_len = i + 1;  
	        }  
	    }

	    if(li_byte > maxlength){  
	        alert('최대 글자 입력수를 초과 하였습니다. \n최대 글자 입력수를 초과된 내용은 자동으로 삭제됩니다.');  
	        formobj.value = formobj.value.substr(0, li_len);
	        return false;  
		    formobj.focus();  
	    }
	    return true;  
	}
	
	function fn_confirm() {
		var prpsDoblCode = '${prpsDoblCode }'
		var firstYN = '${firstYN }';
		
		if(prpsDoblCode == '1' && firstYN == 'Y'){
			
			var conVal = confirm('저작권찾기가 신청되었습니다. \n보상금발생 저작물내역을 먼저 확인해주세요. 지금 확인 하시겠습니까?');
			
			if(conVal){
			
				// 목록으로 이동한다.
				fn_list();
			}
		}
	}

	//숫자만 입력
	function only_arabic(t){
		var key = (window.netscape) ? t.which :  event.keyCode;

		if (key < 45 || key > 57) {
			if(window.netscape){  // 파이어폭스
				t.preventDefault();
			}else{
				event.returnValue = false;
			}
		} else {
			//alert('숫자만 입력 가능합니다.');
			if(window.netscape){   // 파이어폭스
				return true;
			}else{
				event.returnValue = true;
			}
		}	
	}
		
//-->
</script>
</head>
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(2);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_2.gif" alt="내권리찾기" title="내권리찾기" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<!-- CONTENT str-->
			<div class="content">
			<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu02.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb2");</script>
			<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>내권리찾기</span><em>미분배보상금 신청</em></p>
					<h1><img src="/images/2012/title/content_h1_0202.gif" alt="미분배보상금 신청 " title="미분배보상금 신청" /></h1>
					<form name="frm" enctype="multipart/form-data" class="sch" action="#">
						<input type="hidden" name="srchDIVS"		value="${srchParam.srchDIVS }" />
						<input type="hidden" name="srchSdsrName"	value="${srchParam.srchSdsrName }" />
						<input type="hidden" name="srchMuciName"	value="${srchParam.srchMuciName }" />
						<input type="hidden" name="srchYymm"		value="${srchParam.srchYymm }" />
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }" />
						
						<input type="hidden" name="hddnTrst203"		value="${clientInfo.TRST_203 }" />
						<input type="hidden" name="hddnTrst202"		value="${clientInfo.TRST_202 }" />
						<input type="hidden" name="hddnTrst205"		value="${clientInfo.TRST_205 }" />
						<input type="hidden" name="hddnTrst205_2"	value="${clientInfo.TRST_205_2 }" />

						<input type="hidden" name="prpsDoblCode"	value="${prpsDoblCode }" />
						<input type="hidden" name="rghtPrpsMastKey"	value="${rghtPrpsMastKey }" />
						<input type="submit" style="display:none;">
					<div class="section">
						<!-- Tab str -->
                          <ul id="tab11" class="tab_menuBg">
                              <li class="first"><a href="#">소개</a></li>
                              <li><a href="#">이용방법</a></li>
                              <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악</a></li>
							  <li class="on"><strong><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용</a></strong></li>
                              <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관</a></li>
                      		</ul>
                    	<!-- //Tab -->
						
						<!-- memo 삽입 -->
						<jsp:include page="/common/memo/2011/memo_01.jsp">
							<jsp:param name="DIVS" value="${srchDIVS}"/>
						</jsp:include>
						<!-- // memo 삽입 -->
						<!-- article str -->
						<div class="article mt20">
							<div class="floatDiv">
								<h2 class="fl">교과용 미분배보상금 신청</h2>
								<p class="fr"><span class="button small icon"><a href="#1" onclick="javascript:openSmplDetail('INMT02')" >예시화면 보기</a><span class="help"></span></span></p>
							</div>
							<div id="fldInmtInfo"><!-- fieldset 보상금신청 신청자 정보 --> 
								<span class="topLine"></span>
								<!-- 그리드스타일 -->
								<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
									<colgroup>
									<col width="20%">
									<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">보상금종류</th>
											<td>교과용</td>
										</tr>
										<tr>
											<th scope="row">신청인정보</th>
											<td>
												<span class="topLine2"></span>
												<!-- 그리드스타일 -->
												<table cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
													<colgroup>
													<col width="14%">
													<col width="33%">
													<col width="29%">
													<col width="*">
													</colgroup>
													<tbody>
														<tr>
															<th scope="row"><label class="necessary">성명</label></th>
															<td>
																<input type="hidden" name="hddnUserIdnt" value="${clientInfo.USER_IDNT }" />
																<input type="text" name="txtPrpsName" value="${clientInfo.PRPS_NAME }" title="신청자 성" readonly/>
															</td>
															<th scope="row"><label class="necessary">주민등록번호/</label>사업자번호</th>
															<td>
																<input type="text" class="w90" name="txtPrsdCorpNumbView" value="${clientInfo.RESD_CORP_NUMB_VIEW }" title="주민번호/사업자번호" readonly />
															</td>
														</tr>
														<tr>
															<th scope="row"><label class="necessary">전화번호</label></th>
															<td>
																<ul class="list1">
																	<li class="p11"><label for="regi1" class="inBlock w25">자택</label> : <input type="text" id="regi1" name="txtHomeTelxNumb" value="${clientInfo.HOME_TELX_NUMB }" maxlength="14" title="신청자 전화(자택)" class="inputDataN w55" /></li>
																	<li class="p11"><label for="regi2" class="inBlock w25">사무실</label> : <input type="text" id="regi2" name="txtBusiTelxNumb" value="${clientInfo.BUSI_TELX_NUMB }" maxlength="14" title="신청자 전화(사무실)" class="inputDataN w55" /></li>
																	<li class="p11"><label for="regi3" class="inBlock w25">휴대폰</label> : <input type="text" id="regi3" name="txtMoblPhon" value="${clientInfo.MOBL_PHON }" maxlength="14" title="신청자 전화(휴대폰)" class="inputDataN w55" /></li>
																</ul>
															</td>
															<th scope="row">팩스번호</th>
															<td>
																<input name="txtFaxxNumb" value="${clientInfo.FAXX_NUMB }" maxlength="15" title="신청자 전화(FAX)" class="inputDataN w90" />
															</td>
														</tr>
														<tr>
															<th scope="row"><label class="necessary">주소</label></th>
															<td colspan="3">
																<ul class="list1">
																<li class="p11"><label for="txtHomeAddr" class="inBlock w10">자택</label> : <input type="text" name="txtHomeAddr" id="txtHomeAddr" value="${clientInfo.HOME_ADDR }" maxlength="50" title="신청자 주소(자택)" class="inputDataN w85" /></li>
																<li class="p11"><label for="txtBusiAddr" class="inBlock w10">사무실</label> : <input type="text" name="txtBusiAddr" id="txtBusiAddr" value="${clientInfo.BUSI_ADDR }" maxlength="50" title="신청자 주소(사무실)" class="inputDataN w85" /></li>
																</ul>
															</td>
														</tr>
														<tr>
															<th scope="row"><label class="necessary">입금처</label></th>
															<td>
																<ul class="list1">
																<li class="p11"><label for="txtBankName" class="inBlock w30">은행명</label> : <input type="text" id="txtBankName"  name="txtBankName" value="${clientInfo.BANK_NAME }" title="입금 은행명" maxlength="18" class="inputDataN w55" /></li>
																<li class="p11"><label for="txtAcctNumb" class="inBlock w30">계좌번호</label> : <input type="text" id="txtAcctNumb" name="txtAcctNumb" value="${clientInfo.ACCT_NUMB }" title="입금 계좌번호" maxlength="18" class="inputDataN w55" /></li>
																<li class="p11"><label for="txtDptr" class="inBlock w30">예금주</label> : <input type="text" id="txtDptr" name="txtDptr" value="${clientInfo.DPTR }" title="입금 예금주" maxlength="33" class="inputDataN w55" /></li>
																</ul>
															</td>
															<th scope="row">E-Mail</th>
															<td>
																<input type="text" name="txtMail" value="${clientInfo.MAIL }" maxlength="50" title="신청자 EMAIL" class="inputDataN w85" />
															</td>
														</tr>
													</tbody>
												</table>
												<!-- //그리드스타일 -->
											</td>
										</tr>
										<tr>
											<th scope="row">신청구분</th>
											<td>
												<ul class="line22">
													<li>
														<input type="checkbox" name="chkTrts205" id="chkTrts205" onclick="javascript:fn_chgInsertDiv('krtra',this);"
														<c:choose>
															<c:when test="${clientInfo.KRTRA != 'Y'}">
																<c:if test="${clientInfo.TRST_205 != '1'}">
																	disabled
																</c:if>
															</c:when>
															<c:otherwise>
																<c:if test="${clientInfo.TRST_205 == '1'}">
																	checked
																</c:if>
															</c:otherwise> 
														</c:choose>
														 class="inputRChk" title="교과용 보상금"/>
														 <label for="chkTrts205" class="p12">교과용 보상금</label>
														 <!-- hidden Value Setting -->
														 <c:choose>
															<c:when test="${clientInfo.KRTRA != 'Y'}">
																<c:if test="${clientInfo.TRST_205 != '1'}">
																	<input type="hidden" name="hddnInsertKrtra" value="N" />
																</c:if>
															</c:when>
															<c:otherwise>
																<c:if test="${clientInfo.TRST_205 == '1'}">
																	<input type="hidden" name="hddnInsertKrtra" value="Y" />
																</c:if>
															</c:otherwise> 
														</c:choose>
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<th scope="row">신청저작물정보</th>
											<td>
												<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="보상금신청 저작물 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="10%">
													<col width="*">
													<col width="20%">
													<col width="20%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">순번</th>
															<th scope="col">도서명</th>
															<th scope="col">저자</th>
															<th scope="col">사용년도</th>
														</tr>
													</thead>
													<tbody>
													<c:if test="${!empty selectList}">
														<c:forEach items="${selectList}" var="selectList">	
															<c:set var="NO" value="${selectListCnt + 1}"/>
															<c:set var="i" value="${i+1}"/>
														<tr>
															<td class="ce">
																<c:out value="${NO - i}"/>
																<input type="hidden" name="hddnSelectInmtSeqn" value="${selectList.INMT_SEQN }" />
																<input type="hidden" name="hddnSelectPrpsDivs" value="${selectList.PRPS_DIVS }" />
																<input type="hidden" name="hddnSelectPrpsIdntCode" value="${selectList.PRPS_IDNT_CODE }" />
																<input type="hidden" name="hddnSelectKapp" value="${selectList.KAPP }" />
																<input type="hidden" name="hddnSelectFokapo" value="${selectList.FOKAPO }" />
																<input type="hidden" name="hddnSelectKrtra" value="${selectList.KRTRA }" />
																
																<input type="hidden" name="hddnSelectSdsrName"	value="${selectList.SDSR_NAME }" />
																<input type="hidden" name="hddnSelectMuciName"	value="${selectList.MUCI_NAME }" />
																<input type="hidden" name="hddnSelectYymm"		value="${selectList.YYMM }" />
																<input type="hidden" name="hddnSelectLishComp"	value="${selectList.LISH_COMP }" />
																<input type="hidden" name="hddnSelectUsexType"	value="${selectList.USEX_TYPE }" />
															</td>
															<td>${selectList.SDSR_NAME }</td>
															<td class="ce">${selectList.MUCI_NAME }</td>
															<td class="ce">${selectList.YYMM }</td>
														</tr>
														</c:forEach>
													</c:if>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</div> <!-- //fieldset -->
													
							<!-- 복전협 관련 영역 -->
							<div id="fldKrtra" style="display:
								<c:choose>
									<c:when test="${clientInfo.TRST_205 != '1'}">none</c:when>
								</c:choose>;"><!-- fieldSet str -->
								<div class="floatDiv mt20">
									<h3 class="fl">내용(한국복사전송권협회) 교과용보상금
										<span class="ml10">
										<input type="checkbox" id="chkKrtraOff" onclick="javascript:fn_chgDiv('krtra','OFF');" class="inputChk"
										<c:if test="${!empty trstList}">
										<c:forEach items="${trstList}" var="trstList">
											<c:if test="${trstList.TRST_ORGN_CODE=='205' }">
												<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if>
											</c:if>
											</c:forEach>
											</c:if> title="오프라인접수(첨부서류)" />
											<label for="chkKrtraOff" class="orange thin">오프라인접수(첨부서류)</label>
										<input type="hidden" name="hddnKrtraOff" /> 
										</span>
									</h3>
									<p class="fr">
										<span id="spanKrtraBtn"><a href="#1" onclick="javascript:fn_delete('chkKrtra');"><img src="/images/2012/button/delete.gif" alt="삭제" /></a></span>
									</p>
								</div>
								
								<!-- 보상금신청 실연자정보 테이블 영역입니다 -->
								<div id="divKrtraInmtInfo" class="mb5">
									<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="보상금신청 권리자정보 입력 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="30%">
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><label class="necessary">권리자</label></th>
												<td>
													<ul class="list1">
														<li class="p11"><label for="regi4" class="inBlock w25">본명</label> : <input type="text" id="regi4" name="txtKrtraPemrRlnm" value="${PRPS_NAME}" title="실연자(본명)" class="inputDataN w60" /></li>
														<li class="p11"><label for="regi5" class="inBlock w25">필명</label> : <input type="text" id="regi5" name="txtKrtraGrupName" value="${grupName}" title="권리자(필명)" maxlength="16" class="inputDataN w60" /></li>
														<li class="p11"><label for="regi6" class="inBlock w25">예명</label> : <input type="text" id="regi6" name="txtKrtraPemrStnm" value="${pemrStnm}" title="권리자(예명)" maxlength="20"class="inputDataN w60" /></li>
													</ul>
												</td>
												<th scope="col"><label class="necessary">주민등록번호</label></th>
												<td>
												 	<input type="text" name="txtKrtraResdNumb_1" maxlength="6" Size="6" onkeypress="javascript:only_arabic(event);" value="${RESD_CORP_NUMB1 }" title="권리자 주민번호(1번째)" class="inputDataN w40" />
													 -
													<input type="password"  autocomplete="off" name="txtKrtraResdNumb_2" maxlength="7" Size="7" onkeypress="javascript:only_arabic(event);" value="${RESD_CORP_NUMB2 }" title="권리자 주민번호(2번째)" class="inputDataN w40" />
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 권리자정보 테이블 영역입니다 -->
								<!-- 보상금신청 항목 테이블 영역입니다 -->
								<div id="divKrtraItemList" class="div_scroll" style="width:726px; padding:0 0 0 0;">
									<table id="div_scroll" cellspacing="0" cellpadding="0" border="1" class="grid mt5 tableFixed" summary="보상금신청 저작물 정보 입력폼입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="4%">
										<col width="8%">
										<col width="*">
										<col width="13%">
										<col width="8%">
										<col width="10%">
										<col width="10%">
										<col width="10%">
										<col width="12%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col" rowspan="2"><input type="checkbox" onclick="javascript:checkBoxToggle('frm','chkKrtra',this);" class="vmid" title="전체선택" /></th>
												<th scope="col" rowspan="2">저작물<br>종류</th>
												<th scope="col" rowspan="2">저작물명</th>
												<th scope="col" rowspan="2">저작자명</th>
												<th scope="col" colspan="5">이용현황</th>
											</tr>
											<tr>
												<th scope="col">학교급<br>및 학교</th>
												<th scope="col">발행년도<br>및 학기</th>
												<th scope="col">교과목</th>
												<th scope="col">출판사</th>
												<th scope="col">이용페이지</th>
											</tr>
										</thead>
										<tbody>
										<c:if test="${empty krtraList}">
											<c:if test="${!empty selectList}">
												<c:forEach items="${selectList}" var="selectList">	
													<c:if test="${selectList.KRTRA == 'Y'}">
													
														<!-- 일반 보상금신청 -->
														<c:if test="${prpsDoblCode != '1' }">
															<tr>	
																<td class="ce">
																	<input type="checkbox" name="chkKrtra" id="chkKrtra" class="vmid" title="선택"/>
																	<input type="hidden" name="hddnKrtraPrpsIdnt" value="${selectList.INMT_SEQN }" />
																	<input type="hidden" name="hddnKrtraPrpsDivs" value="${selectList.PRPS_DIVS }" />
																	<input type="hidden" name="hddnKrtraPrpsIdntCode" value="${selectList.PRPS_IDNT_CODE }" />
																			
																	<input type="hidden" name="hddnKrtraCoprKind"	value="${selectList.WORK_KIND }" />
																	<input type="hidden" name="hddnKrtraWorkName"	value="${selectList.SDSR_NAME }" />
																	<input type="hidden" name="hddnKrtraWrtrName"	value="${selectList.MUCI_NAME }" />
																	<input type="hidden" name="hddnKrtraScyr"		value="${selectList.SCHL }" />
																	<input type="hidden" name="hddnKrtraSctr"		value="${selectList.YYMM }" />
																	<input type="hidden" name="hddnKrtraSjet"		value="${selectList.SUBJ_NAME }" />
																	<input type="hidden" name="hddnKrtraBookCncn"	value="${selectList.LISH_COMP }" />
																	<input type="hidden" name="hddnKrtraWorkDivs"	value="${selectList.WORK_DIVS }" />
																</td>
																<td class="ce">${selectList.WORK_KIND }</td>
																<td class="ce">${selectList.SDSR_NAME }</td>
																<td class="ce">${selectList.MUCI_NAME }</td>
																<td class="ce">${selectList.SCHL }</td>
																<td class="ce">${selectList.YYMM }</td>
																<td class="ce">${selectList.SUBJ_NAME }</td>
																<td class="ce">${selectList.LISH_COMP }</td>
																<td class="ce">${selectList.WORK_DIVS }</td>
															</tr>
														</c:if>
														<!-- 동시 보상금신청 -->
														<c:if test="${prpsDoblCode == '1' }">
															
															<!-- 기존 보상금 -->
															<c:if test="${selectList.PRPS_IDNT_CODE != '2' }">
																<tr>	
																	<td class="ce">
																		<input type="checkbox" name="chkKrtra" id="chkKrtra" class="vmid" title="선택" />
																		<input type="hidden" name="hddnKrtraPrpsIdnt" value="${selectList.INMT_SEQN }" />
																		<input type="hidden" name="hddnKrtraPrpsDivs" value="${selectList.PRPS_DIVS }" />
																		<input type="hidden" name="hddnKrtraPrpsIdntCode" value="${selectList.PRPS_IDNT_CODE }" />
																				
																		<input type="hidden" name="hddnKrtraCoprKind"	value="${selectList.WORK_KIND }" />
																		<input type="hidden" name="hddnKrtraWorkName"	value="${selectList.SDSR_NAME }" />
																		<input type="hidden" name="hddnKrtraWrtrName"	value="${selectList.MUCI_NAME }" />
																		<input type="hidden" name="hddnKrtraScyr"		value="${selectList.SCHL }" />
																		<input type="hidden" name="hddnKrtraSctr"		value="${selectList.YYMM }" />
																		<input type="hidden" name="hddnKrtraSjet"		value="${selectList.SUBJ_NAME }" />
																		<input type="hidden" name="hddnKrtraBookCncn"	value="${selectList.LISH_COMP }" />
																		<input type="hidden" name="hddnKrtraWorkDivs"	value="${selectList.WORK_DIVS }" />
																	</td>
																	<td class="ce">${selectList.WORK_KIND }</td>
																	<td class="ce">${selectList.SDSR_NAME }</td>
																	<td class="ce">${selectList.MUCI_NAME }</td>
																	<td class="ce">${selectList.SCHL }</td>
																	<td class="ce">${selectList.YYMM }</td>
																	<td class="ce">${selectList.SUBJ_NAME }</td>
																	<td class="ce">${selectList.LISH_COMP }</td>
																	<td class="ce">${selectList.WORK_DIVS }</td>
																</tr>
															</c:if>
															
															<!-- 등록 보상금 -->
															<c:if test="${selectList.PRPS_IDNT_CODE == '2' }">
																<tr>	
																	<td class="ce">
																		<input type="checkbox" name="chkKrtra" id="chkKrtra" class="vmid" title="선택" />
																		<input type="hidden" name="hddnKrtraPrpsIdnt" value="${selectList.INMT_SEQN }" />
																		<input type="hidden" name="hddnKrtraPrpsDivs" value="${selectList.PRPS_DIVS }" />
																		<input type="hidden" name="hddnKrtraPrpsIdntCode" value="${selectList.PRPS_IDNT_CODE }" />
																				
																		<input type="hidden" name="hddnKrtraWorkName"	value="${selectList.SDSR_NAME }" />
																	</td>
																	<td class="ce"><input type="text" name="hddnKrtraCoprKind" value="${selectList.WORK_KIND }" maxlength="30" title="저작물종류" class="inputData w90 ce" /> </td>
																	<td class="ce">${selectList.SDSR_NAME }</td>
																	<td class="ce"><input type="text" name="hddnKrtraWrtrName" value="${selectList.MUCI_NAME }" maxlength="30" title="저작자명" class="inputData w90 ce" /></td>
																	<td class="ce"><input type="text" name="hddnKrtraScyr" value="${selectList.SCHL }" maxlength="30" title="학교급 및 학교" class="inputData w90 ce" /></td>
																	<td class="ce"><input type="text" name="hddnKrtraSctr" value="${selectList.YYMM }" maxlength="30" title="발행년도 및 학기" class="inputData w90 ce" /></td>
																	<td class="ce"><input type="text" name="hddnKrtraSjet" value="${selectList.SUBJ_NAME }" maxlength="30" title="교과목" class="inputData w90 ce" /></td>
																	<td class="ce"><input type="text" name="hddnKrtraBookCncn" value="${selectList.LISH_COMP }" maxlength="30" title="출판사" class="inputData w90 ce" /></td>
																	<td class="ce"><input type="text" name="hddnKrtraWorkDivs" value="${selectList.WORK_DIVS }" maxlength="30" title="이용페이지" class="inputData w90 ce" /></td>
																</tr>
															</c:if>
															
														</c:if>
													</c:if>
												</c:forEach>
											</c:if>
										</c:if>
										
										<c:if test="${!empty krtraList}">
											<c:forEach items="${krtraList}" var="krtraList">
											
												<!-- 일반 보상금신청 -->
												<c:if test="${prpsDoblCode != '1' }">
														
												<tr>
													<td class="ce">
														<input type="checkbox" name="chkKrtra" id="chkKrtra" class="vmid" title="선택" />
														<input type="hidden" name="hddnKrtraPrpsIdnt" value="${krtraList.INMT_SEQN }" />
														<input type="hidden" name="hddnKrtraPrpsDivs" value="${krtraList.PRPS_DIVS }" />
														<input type="hidden" name="hddnKrtraPrpsIdntCode" value="${krtraList.PRPS_IDNT_CODE }" />
														
														<input type="hidden" name="hddnKrtraCoprKind"	value="${krtraList.COPT_KIND }" />
														<input type="hidden" name="hddnKrtraWorkName"	value="${krtraList.WORK_NAME }" />
														<input type="hidden" name="hddnKrtraWrtrName"	value="${krtraList.WRTR_NAME }" />
														<input type="hidden" name="hddnKrtraScyr"		value="${krtraList.SCYR }" />
														<input type="hidden" name="hddnKrtraSctr"		value="${krtraList.SCTR }" />
														<input type="hidden" name="hddnKrtraSjet"		value="${krtraList.SJET }" />
														<input type="hidden" name="hddnKrtraBookCncn"	value="${krtraList.BOOK_CNCN }" />
														<input type="hidden" name="hddnKrtraWorkDivs"	value="${krtraList.WORK_DIVS }" />
													</td>
													<td class="ce">${krtraList.COPT_KIND }</td>
													<td class="ce">${krtraList.WORK_NAME }</td>
													<td class="ce">${krtraList.WRTR_NAME }</td>
													<td class="ce">${krtraList.SCYR }</td>
													<td class="ce">${krtraList.SCTR }</td>
													<td class="ce">${krtraList.SJET }</td>
													<td class="ce">${krtraList.BOOK_CNCN }</td>
													<td class="ce">${krtraList.WORK_DIVS }</td>
												</tr>
											</c:if>
											
											<!-- 동시 보상금신청 -->
											<c:if test="${prpsDoblCode == '1' }">
												
												<!-- 기존 보상금 -->
												<c:if test="${krtraList.PRPS_IDNT_CODE != '2' }">
													<tr>
														<td class="ce">
															<input type="checkbox" name="chkKrtra" id="chkKrtra" class="vmid" title="선택" />
															<input type="hidden" name="hddnKrtraPrpsIdnt" value="${krtraList.INMT_SEQN }" />
															<input type="hidden" name="hddnKrtraPrpsDivs" value="${krtraList.PRPS_DIVS }" />
															<input type="hidden" name="hddnKrtraPrpsIdntCode" value="${krtraList.PRPS_IDNT_CODE }" />
															
															<input type="hidden" name="hddnKrtraCoprKind"	value="${krtraList.COPT_KIND }" />
															<input type="hidden" name="hddnKrtraWorkName"	value="${krtraList.WORK_NAME }" />
															<input type="hidden" name="hddnKrtraWrtrName"	value="${krtraList.WRTR_NAME }" />
															<input type="hidden" name="hddnKrtraScyr"		value="${krtraList.SCYR }" />
															<input type="hidden" name="hddnKrtraSctr"		value="${krtraList.SCTR }" />
															<input type="hidden" name="hddnKrtraSjet"		value="${krtraList.SJET }" />
															<input type="hidden" name="hddnKrtraBookCncn"	value="${krtraList.BOOK_CNCN }" />
															<input type="hidden" name="hddnKrtraWorkDivs"	value="${krtraList.WORK_DIVS }" />
														</td>
														<td class="ce">${krtraList.COPT_KIND }</td>
														<td class="ce">${krtraList.WORK_NAME }</td>
														<td class="ce">${krtraList.WRTR_NAME }</td>
														<td class="ce">${krtraList.SCYR }</td>
														<td class="ce">${krtraList.SCTR }</td>
														<td class="ce">${krtraList.SJET }</td>
														<td class="ce">${krtraList.BOOK_CNCN }</td>
														<td class="ce">${krtraList.WORK_DIVS }</td>
													</tr>
												</c:if>	
												
												<!-- 등록 보상금 -->
													<c:if test="${krtraList.PRPS_IDNT_CODE == '2' }">
														<tr>	
															<td class="ce">
																<input type="checkbox" name="chkKrtra" id="chkKrtra" class="vmid" title="선택" />
																<input type="hidden" name="hddnKrtraPrpsIdnt" value="${krtraList.INMT_SEQN }" />
																<input type="hidden" name="hddnKrtraPrpsDivs" value="${krtraList.PRPS_DIVS }" />
																<input type="hidden" name="hddnKrtraPrpsIdntCode" value="${krtraList.PRPS_IDNT_CODE }" />
																
																<input type="hidden" name="hddnKrtraWorkName"	value="${krtraList.WORK_NAME }" />
															</td>
															<td class="ce"><input type="text" name="hddnKrtraCoprKind" value="${krtraList.COPT_KIND }" maxlength="30" title="저작물종류" class="inputData w90 ce" /> </td>
															<td class="ce">${krtraList.SDSR_NAME }</td>
															<td class="ce"><input type="text" name="hddnKrtraWrtrName" value="${krtraList.WRTR_NAME }" maxlength="30" title="저작자명" class="inputData w90 ce" /></td>
															<td class="ce"><input type="text" name="hddnKrtraScyr" value="${krtraList.SCYR }" maxlength="30" title="학교급 및 학교" class="inputData w90 ce" /></td>
															<td class="ce"><input type="text" name="hddnKrtraSctr" value="${krtraList.SCTR }" maxlength="30" title="발행년도 및 학기" class="inputData w90 ce" /></td>
															<td class="ce"><input type="text" name="hddnKrtraSjet" value="${krtraList.SJET }" maxlength="30" title="교과목" class="inputData w90 ce" /></td>
															<td class="ce"><input type="text" name="hddnKrtraBookCncn" value="${krtraList.BOOK_CNCN }" maxlength="30" title="출판사" class="inputData w90 ce" /></td>
															<td class="ce"><input type="text" name="hddnKrtraWorkDivs" value="${krtraList.WORK_DIVS }" maxlength="30" title="이용페이지" class="inputData w90 ce" /></td>
														</tr>
													</c:if>
												</c:if>
											</c:forEach>
										</c:if>
										</tbody>
									</table>
								</div>
								<!-- 
								//보상금신청 항목 테이블 영역입니다 --><!--
								<div id="divKappFileList" class="tabelRound mb5" 
									style="display:<c:if test="${!empty trstList}">
									<c:forEach items="${trstList}" var="trstList">
										<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
											<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
										</c:if>
									</c:forEach>
									</c:if>;">
								</div>
								-->
								<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
								<div>
									<table cellspacing="0" cellpadding="0" border="1" class="grid mt10" style="margin-top:1px;" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><br>첨부서류<br><br></th>
												<td>
													<div id="divKrtraFileList" class="mb5"
													style="display:<c:if test="${!empty trstList}">
													<c:forEach items="${trstList}" var="trstList">
														<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
															<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
														</c:if>
													</c:forEach>
													</c:if>;">
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
								
								
								<!-- 보상금신청 오프라인 관련 영역 -->
								<div id="divKrtraOffline1" class="white_box">
									<div class="box5">
										<div class="box5_con floatDiv" style="width:713px;">
											<p class="fl mt5"><img src="/images/2012/content/box_img4.gif" alt="" /></p>
											<div class="fl ml30 mt5">
												<h4><strong>한국복사전송권협회(교과용보상금) 오프라인접수 선택한 경우 해당협회에 관련서류를<br> 제출하여야합니다.</strong></h4>
												<ul class="list1 mt10">
													<li>교과용보상금 신청서
													<br><span class="blue2 fontSmall">(보상금 신청현황조회 상세화면에서 교과용보상금 신청서 출력 가능. [위치] <u>마이페이지>신청현황>미분배보상금 신청</u>)</span>
													</li>
													<li>권리자임을 증명할 수 있는 서류(저작권등록증 등))</li>
													<li>상속, 양도, 승계 등의 사실을 증명할 수 있는 서류</li>
													<li>주민등록등본</li>
													<li>통장사본</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<!-- //보상금신청 오프라인 관련 영역 -->
							</div> <!-- //fieldset -->
							<div class="mt20"><!-- fieldeset보상금신청 내용-->
								<!-- 보상금신청(내용) 테이블 영역입니다 -->
								<div id="divContent" class="tabelRound mb5" style="width:726px;">
									<span class="topLine"></span>
									<table cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="보상금신청 내용입력 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><label for="txtareaPrpsDesc" class="necessary">내용</label></th>
												<td scope="col">
													<textarea rows="10" name="txtareaPrpsDesc" id="txtareaPrpsDesc" title="내용" class="h100 w100">${clientInfo.PRPS_DESC }</textarea>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div><!-- //fieldset -->
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_list()">목록</a></span></p>
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_chkSubmit();">보상금 신청 확인</a></span>
							</div>
						</div>
						<!-- //article end -->
					</div>
				</form>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>	
			<!-- CONTENT end-->
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<form name="srchForm" action="#">
<input type="hidden" name="srchDIVS"		value="${srchParam.srchDIVS }" />
<input type="hidden" name="srchSdsrName"	value="${srchParam.srchSdsrName }" />
<input type="hidden" name="srchMuciName"	value="${srchParam.srchMuciName }" />
<input type="hidden" name="srchYymm"		value="${srchParam.srchYymm }" />
<input type="hidden" name="nowPage"			value="${srchParam.nowPage }" />
<input type="submit" style="display:none;">
</form>

<script type="text/javascript" src="/js/2011/file.js"></script>
<script type="text/javascript">
<!--
	function fn_setFileInfo(){
		var listCnt = '${fn:length(fileList)}';
		<c:if test="${!empty fileList}">
			<c:forEach items="${fileList}" var="fileList">
			fn_addGetFile(	'205',
							'${fileList.PRPS_MAST_KEY}',
							'${fileList.PRPS_SEQN}',
							'${fileList.ATTC_SEQN}',
							'${fileList.FILE_PATH}',
							'${fileList.REAL_FILE_NAME}',
							'${fileList.FILE_NAME}',
							'${fileList.TRST_ORGN_CODE}',
							'${fileList.FILE_SIZE}'
							);
			</c:forEach>
		</c:if>		
	}
	
	window.onload	= function(){	fn_createTable2("divKrtraFileList", "205");
									fn_setFileInfo();
									fn_confirm();
								}
//-->
</script>	
</body>
</html>