<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금발생저작물 조회 | 저작권찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<style type="text/css">
<!--
.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
.w55{width:55%;}
.necessary{ background: url(/images/2012/common/necessary.gif) no-repeat 98% 2px !important; padding-right: 10px !important;}/* 필수 */
-->
</style>
<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>


<script type="text/JavaScript">
<!--
	//체크박스에 따른 화면 설정(첨부파일,오프라인)
	function fn_chgInsertDiv(sOrgn, obj){
		var frm = document.frm;
		if(obj.checked){
			if(sOrgn == 'kapp'){
				frm.hddnInsertKapp.value = 'Y';
			}else{
				frm.hddnInsertFokapo.value = 'Y';
			}
			fn_chgDiv(sOrgn, 'Y');
			resizeDiv('divKappItemList' , 'div_scroll');
		}else{
			if(sOrgn == 'kapp'){
				if(confirm('저작인적권(앨범제작) 선택해제 시 관련 선택저작물이 신청이 되지않습니다.\r\n선택해제 하시겠습니까?')){
					frm.hddnInsertKapp.value = 'N';
				}else{
					obj.checked	= true;
					return;
				}
			}else{
				if(confirm('저작인적권(가창, 연주, 지휘 등) 선택해제 시 관련 선택저작물이 신청이 되지않습니다.\r\n선택해제 하시겠습니까?')){
					frm.hddnInsertFokapo.value = 'N';
				}else{
					obj.checked	= true;
					return;
				}
			}
			fn_chgDiv(sOrgn, 'N');
		}
	}

	//체크박스에 따른 화면 설정(첨부파일,오프라인)
	function fn_chgDiv(sOrgn,sDiv){

		var oFldKapp = document.getElementById("fldKapp");
		var oFldFokapo = document.getElementById("fldFokapo"); 
			
		var oInmtInfo, oItemList, oFileList, oOffLine;	//div
		var chkOff;	//checkbox
		var oBtn;
	
		if(sOrgn == 'kapp'){			//음제협
			oItemList	= document.getElementById("divKappItemList");
			oFileList	= document.getElementById("divKappFileList");
			oOffLine	= document.getElementById("divKappOffline1");
			oBtn		= document.getElementById("spanKappBtn");
			chkOff		= document.getElementById("chkKappOff");
		}else if(sOrgn == 'fokapo'){	//실연자
			oInmtInfo	= document.getElementById("divFokapoInmtInfo");
			oItemList	= document.getElementById("divFokapoItemList");
			oFileList	= document.getElementById("divFokapoFileList");
			oOffLine	= document.getElementById("divFokapoOffline1");
			oBtn		= document.getElementById("spanFokapoBtn");
			chkOff	= document.getElementById("chkFokapoOff");
		}

		if(sDiv == 'OFF'){
			if(chkOff.checked){
				if(sOrgn == 'fokapo')	oInmtInfo.style.display = '';
				oItemList.style.display	= '';
				oFileList.style.display = 'none';
				oOffLine.style.display	= '';
				oBtn.style.display		= '';
			}else{
				if(sOrgn == 'fokapo')	oInmtInfo.style.display = '';
				oItemList.style.display	= '';
				oFileList.style.display = '';
				oBtn.style.display		= '';
				oOffLine.style.display	= '';
			}
		}else if(sDiv == 'Y'){
			if(sOrgn == 'kapp'){
				oFldKapp.style.display = '';
			}else{
				oFldFokapo.style.display = '';
			}
		}else if(sDiv == 'N'){
			if(sOrgn == 'kapp'){
				oFldKapp.style.display = 'none';
			}else{
				oFldFokapo.style.display = 'none';
			}
		}
	}

	// 선택된 저작물 삭제
	function fn_delete(sStr, sStr2, rowDisName){
		var frm = document.frm;
		
		//선택 목록
		var chkObjs = document.getElementsByName(sStr);
		var inputObjs = document.getElementsByName(sStr2); 
		var sCheck = 0;
		
		for(i=0; i<chkObjs.length;i++){
			if(chkObjs[i].checked){
				sCheck = 1;
			}
		}
		
		if(sCheck == 0 ){
			alert('선택된 저작물이 없습니다.');
			return;
		}
		
		for(var i=chkObjs.length-1; i >= 0; i--){
			var chkObj = chkObjs[i];
			var inputObj = inputObjs[i];
			
			if(chkObj.checked){
			
				// 선택된 저작물을 삭제한다.
				var oTR = findParentTag(chkObj, "TR");
				if(inputObjs.length > 0){
					var oTR2 = findParentTag(inputObj, "TR");
				}
				
				if(eval(oTR) && rowDisName=='kapp_displaySeq' ){ 	
					if(inputObjs.length > 0){
						oTR2.parentNode.removeChild(oTR2);
					}
					oTR.parentNode.removeChild(oTR);

					var the_height = document.getElementById('divKappItemList').offsetHeight;
					document.getElementById('divKappItemList').style.height = the_height - 33  + "px" ;
				}
				
				if(eval(oTR) && rowDisName=='fokapo_displaySeq') 	oTR.parentNode.removeChild(oTR);
				
			}
		}
		
		// 순번재지정
		fn_resetSeq(rowDisName);
	}

	//순번 재지정
	function fn_resetSeq(disName){
	    var oSeq = document.getElementsByName(disName);
	    for(i=0; i<oSeq.length; i++){
	        oSeq[i].value = i+1;
	    }
	}
	//보상금신청
	function fn_chkSubmit(){
		var frm = document.frm;

		//신청자 정보 필수체크
		var oFldInmt	= document.getElementById("fldInmtInfo");
		var oInmt	= oFldInmt.getElementsByTagName("input");


		var txtHomeAddr	= ""; var txtBusiAddr	= "";
		var txtHomeTelxNumb	= "";  var txtBusiTelxNumb	= "";  var txtMoblPhon	= "";  var txtFaxxNumb	= "";  var txtMail = "";

/*
		txtHomeAddr	= document.getElementById("txtHomeAddr").value;
		txtBusiAddr	= document.getElementById("txtBusiAddr").value;

		txtHomeTelxNumb	= document.getElementById("txtHomeTelxNumb").value;
		txtBusiTelxNumb	= document.getElementById("txtBusiTelxNumb").value;
		txtMoblPhon	= document.getElementById("txtMoblPhon").value;
		txtFaxxNumb	= document.getElementById("txtFaxxNumb").value;
*/	

		txtHomeAddr	= document.getElementsByName("txtHomeAddr")[0].value;
		txtBusiAddr	= document.getElementsByName("txtBusiAddr")[0].value;
		

		txtHomeTelxNumb	= document.getElementsByName("txtHomeTelxNumb")[0].value;
		txtBusiTelxNumb	= document.getElementsByName("txtBusiTelxNumb")[0].value;
		txtMoblPhon	= document.getElementsByName("txtMoblPhon")[0].value;
		txtFaxxNumb	= document.getElementsByName("txtFaxxNumb")[0].value;
		txtMail = document.getElementsByName("txtMail")[0].value;
		
		for(i = 0; i < oInmt.length; i++){
			/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
			if(checkField(oInmt[i]) == false){
				return;
			}
			*/
			
			// 전화번호
			if(oInmt[i].name == 'txtHomeTelxNumb'
				|| oInmt[i].name == 'txtBusiTelxNumb'
				|| oInmt[i].name == 'txtMoblPhon'
				|| oInmt[i].name == 'txtFaxxNumb'){
				
				if(txtHomeTelxNumb == ''){
					if(txtBusiTelxNumb == ''){
						if(txtMoblPhon == ''){
							if(!nullCheck(oInmt[i]))	return;
						}
					}
				}
				
				if(!character(oInmt[i],  'EK'))	return;
			}
			
			// 자택 사무실 주소 nullCheck
			if(oInmt[i].name == 'txtHomeAddr'
				|| oInmt[i].name == 'txtBusiAddr'){
				if(txtHomeAddr == ''){
					if(txtBusiAddr == ''){
						if(!nullCheck(oInmt[i]))	return;
					}	
				}
			}
			
			// 입금처
			if(oInmt[i].name == 'txtBankName'
				|| oInmt[i].name == 'txtAcctNumb'
				|| oInmt[i].name == 'txtDptr'){
				
				if(!nullCheck(oInmt[i]))	return;
			}
			
			// E-mail
			if(oInmt[i].name == 'txtMail'){
				if(!character(oInmt[i],  'K'))	return;
			}

		}
		

		//체크박스 값들
		var vChkKappOff		= document.getElementById("chkKappOff").checked;
		var vChkFokapoOff	= document.getElementById("chkFokapoOff").checked;
		if(vChkKappOff)		frm.hddnKappOff.value = "Y";
		if(vChkFokapoOff)	frm.hddnFokapoOff.value = "Y";
		
		var oChk203 = document.getElementById("chkTrts203");
		var oChk202 = document.getElementById("chkTrts202");
		
		//음제협 정보 필수체크
		if(oChk203.checked){
			var oFldKapp	= document.getElementById("fldKapp");
			var oKapp	= oFldKapp.getElementsByTagName("input");//+oFldKapp.getElementsByTagName("select");
			var oKappSel	= oFldKapp.getElementsByTagName("select");
			
			
			for(i = 0; i < oKapp.length; i++){
				/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
				if(checkField(oKapp[i]) == false){
					return;
				}
				*/
				//nullCheck
				
				//alert(oKapp[i].id);
				if(oKapp[i].name == 'txtKappDiskName'
					||oKapp[i].name == 'hddnKappSongName'
					||oKapp[i].name == 'txtKappSnerName'
					||oKapp[i].name == 'txtKappDateIssu'
					||oKapp[i].name == 'selKappMuscGnre'){

					if(!nullCheck(oKapp[i]))	return;
				}
				
				// cd-code
				if(oKapp[i].name == 'txtKappCdCode'){
					if(!character(oKapp[i],  'K'))	return;
				}
				
				

				// 날짜 형식 체크
				if(oKapp[i].name == 'txtKappDateIssu'){
				   	
				   	// 자동입력				
					if((oKapp[i].value).length == 4) {
						oKapp[i].value = oKapp[i].value+'0101';
					}
					
					if(!dateCheck(oKapp[i]))	return;
				}
			}
			
			
			// 장르 select 박스
			for(i = 0; i < oKappSel.length; i++){
				if(oKappSel[i].name == 'selKappMuscGnre'){
					if(!nullCheck(oKappSel[i]))	return;
				}
			}
			

			//신청저작물 체크
			var oKappPrpsIdnt = document.getElementsByName("hddnKappPrpsIdnt");
			for(i = 0; i < oKappPrpsIdnt.length; i++){
				if(oKappPrpsIdnt[i].value == null || (oKappPrpsIdnt[i].value).length < 1){
					alert('내용(한국음원제작자협회) 정보를 입력해주세요.');
					return;
				}
			}
		}
		
		//음실연 정보 필수체크
		if(oChk202.checked){				
			var oFldFokapo	= document.getElementById("fldFokapo");
			var oFokapo	= oFldFokapo.getElementsByTagName("input");
			for(i = 0; i < oFokapo.length; i++){
				/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
				if(checkField(oFokapo[i]) == false){
					return;
				}
				*/
				
				// nullCheck
				if(oFokapo[i].name == 'txtFokapoPemrRlnm'
					||oFokapo[i].name == 'txtFokapoResdNumb_1'
					||oFokapo[i].name == 'txtFokapoResdNumb_2'
					||oFokapo[i].name == 'txtFokapoSnerName'
					//|| oFokapo[i].name == 'hddnFokapoPdtnName'
					){
					
					if(!nullCheck(oFokapo[i]))	return;
				}

				// 글자길이 범위 체크
				if(oFokapo[i].name == 'txtFokapoResdNumb_1'){
					if(!rangeSize(oFokapo[i], '6'))	return;
				}
				
				if(oFokapo[i].name == 'txtFokapoResdNumb_2'){
					if(!rangeSize(oFokapo[i], '7'))	return;
				}
				
				// 날짜 형식 체크
				if(oFokapo[i].name == 'txtFokapoAbumDateIssu'){

					if(!dateCheck(oFokapo[i]))	return;
				}
				
				// 금지할 문자 종류 체크 (E : 영문, K : 한글, N : 숫자)
				if(oFokapo[i].name == 'txtFokapoResdNumb_1'
					|| oFokapo[i].name == 'txtFokapoResdNumb_2'
					|| oFokapo[i].name == 'txtFokapoCtbtRate'){
					
					if(!character(oFokapo[i],  'EKS'))	return;
				}
			}

			//신청저작물 체크
			var oFokapoPrpsIdnt = document.getElementsByName("hddnFokapoPrpsIdnt");
			for(i = 0; i < oFokapoPrpsIdnt.length; i++){
				if(oFokapoPrpsIdnt[i].value == null || (oFokapoPrpsIdnt[i].value).length < 1){
					alert('내용(한국음악실연자연합회) 정보를 입력해주세요.');
					return;
				}
			}
		}
		
		//내용 필수 체크
		var oContent	= document.getElementById("txtareaPrpsDesc");
		/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
		if(checkField(oContent) == false){
			return;
		}
		*/
		// nullCheck
		if(!nullCheck(oContent))	return;

		if(onkeylengthMax(oContent, 4000, 'txtareaPrpsDesc') == false){
			return;
		}
		
		if(oChk203.checked == false && oChk202.checked == false){
			alert('분류(을)를 선택 해 주세요.');
			return;
		}

		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrpsChk";
		frm.submit();
	}
	
	
	//목록으로 이동
	function fn_list(){
		var frm = document.srchForm;
		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=list&amp;gubun=edit&amp;srchDIVS=1";
		frm.submit();
	}

	// table name 사이즈에 대한 div targetName리사이즈
	function resizeDiv(divName, tabName) {
		var the_height = document.getElementById(tabName).offsetHeight;
		document.getElementById(divName).style.height = the_height + 13  + "px" ;
	}


    //maxlength 체크
	function onkeylengthMax(formobj, maxlength, objname) {  
	    var li_byte     = 0;  
	    var li_len      = 0;  
	    for(var i=0; i< formobj.value.length; i++){  
	    	
	        if (escape(formobj.value.charAt(i)).length > 4){  
	            li_byte += 3;  
	        } else {  
	            li_byte++;  
	        }  
	        if(li_byte <= maxlength) {  
	            li_len = i + 1;  
	        }  
	    }

	    if(li_byte > maxlength){  
	        alert('최대 글자 입력수를 초과 하였습니다. \n최대 글자 입력수를 초과된 내용은 자동으로 삭제됩니다.');  
	        formobj.value = formobj.value.substr(0, li_len);
	        return false;  
		    formobj.focus();  
	    }
	    return true;  
	}
	
	function fn_confirm() {
		var prpsDoblCode = '${prpsDoblCode }'
		var firstYN = '${firstYN }';
		
		if(prpsDoblCode == '1' && firstYN == 'Y'){
			
			var conVal = confirm('저작권찾기가 신청되었습니다. \n보상금발생 저작물내역을 먼저 확인해주세요. 지금 확인 하시겠습니까?');
			
			if(conVal){
			
				// 목록으로 이동한다.
				fn_list();
			}
		}
	}
	
	function openSmplDetail(div) {

		var param = '';
		
		param = 'DVI='+div;
		
		var url = '/common/rghtPrps_smpl.jsp?'+param
		var name = '';
		var openInfo = 'target=rghtPrps_mvie, width=705, height=570, scrollbars=yes';
		
		window.open(url, name, openInfo);
	}
		

	//숫자만 입력
	function only_arabic(t){
		var key = (window.netscape) ? t.which :  event.keyCode;

		if (key < 45 || key > 57) {
			if(window.netscape){  // 파이어폭스
				t.preventDefault();
			}else{
				event.returnValue = false;
			}
		} else {
			//alert('숫자만 입력 가능합니다.');
			if(window.netscape){   // 파이어폭스
				return true;
			}else{
				event.returnValue = true;
			}
		}	
	}
	
	
	//헬프메시지 레이어를 화면에 뿌려준다
	function viewMessage(pageName)
	{
		var layer = document.getElementById('helpMessage');
		
		if(typeof(layer) != 'undefined')
		{
			var forms = document.helpForm;
			
			forms.pageName.value = pageName;
			forms.action = '/common/help_message.jsp';
			
			var result = trim(synchRequest(getQueryString(forms)));
			
			layer.innerHTML = result;
			
			//layer.clientHeight의 값을 정확히 구하기 위해 미리 보이지 않는 사각에 layer을 미리 화면에 띄어준다.
			layer.style.top = -1000;
			layer.style.display = '';
			
			//var top = window.event.clientY + document.body.scrollTop - (layer.clientHeight + 50);
			
			//if(top < 0)
			//	top = 0;
				
			//if(top < document.body.scrollTop)
			//	top = top + (document.body.scrollTop - top);
				
			//layer.style.top = top;
			
			//layer.style.left=document.body.scrollLeft + window.event.clientX;
	    	//layer.style.top=document.body.scrollTop + window.event.clientY;
			
			layer.style.left=window.event.clientX+(document.documentElement.scrollLeft || 
	            document.body.scrollLeft) - 
	            document.documentElement.clientLeft;
	            
			layer.style.top=window.event.clientY +(document.documentElement.scrollTop || 
	            document.body.scrollTop) - 
	            document.documentElement.clientTop;
			
			layer.style.display = '';
		}
	}

	//헬프메시지 레이어를 닫아줌

	function closeMessage()
	{
		var layer = document.getElementById('helpMessage');
		
		if(typeof(layer) != 'undefined')
			layer.style.display = 'none';
	}
	
//-->
</script>
</head>
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(2);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_2.gif" alt="내권리찾기" title="내권리찾기" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<!-- CONTENT str-->
			<div class="content">
				<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu02.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb2");</script>
			<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>내권리찾기</span><em>미분배보상금 신청</em></p>
					<h1><img src="/images/2012/title/content_h1_0202.gif" alt="미분배보상금 신청" title="미분배보상금 신청" /></h1>
					<!-- hidden form str : 헬프레이어 -->
					<form name="helpForm" method="post" action="#">
						<input type="hidden" name="pageName">
						<input type="submit" style="display:none;">
					</form>
					
					<form name="frm" action="#" enctype="multipart/form-data" class="sch">
						<input type="hidden" name="srchDIVS"		value="${srchParam.srchDIVS }" />
						<input type="hidden" name="srchSdsrName"	value="${srchParam.srchSdsrName }" />
						<input type="hidden" name="srchMuciName"	value="${srchParam.srchMuciName }" />
						<input type="hidden" name="srchYymm"		value="${srchParam.srchYymm }" />
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }" />
						
						<input type="hidden" name="hddnTrst203"	value="${clientInfo.TRST_203 }" />
						<input type="hidden" name="hddnTrst202"	value="${clientInfo.TRST_202 }" />
						<input type="hidden" name="hddnTrst205"	value="${clientInfo.TRST_205 }" />
						<input type="hidden" name="hddnTrst205_2"	value="${clientInfo.TRST_205_2 }" />
					
					
						<input type="hidden" name="prpsDoblCode"	value="${prpsDoblCode }" />
						<input type="hidden" name="rghtPrpsMastKey"	value="${rghtPrpsMastKey }" />
						<input type="submit" style="display:none;">
					<div class="section">
					
						<!-- Tab str -->
                          <ul id="tab11" class="tab_menuBg">
                              <li class="first"><a href="#">소개</a></li>
                              <li><a href="#">이용방법</a></li>
                              <li class="on"><strong><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악</a></strong></li>
							  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용</a></li>
                              <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관</a></li>
                      		</ul>
                    	<!-- //Tab -->
                    	
						<!-- memo 삽입 -->
						<jsp:include page="/common/memo/2011/memo_01.jsp">
							<jsp:param name="DIVS" value="${srchDIVS}"/>
						</jsp:include>
						<!-- // memo 삽입 -->
						<!-- article str -->
						<div class="article mt20">
							<div class="floatDiv">
								<h2 class="fl">방송음악 미분배보상금 신청</h2>
								<p class="fr"><span class="button small icon"><a href="#1" onclick="javascript:openSmplDetail('INMT01')">예시화면 보기</a><span class="help"></span></span></p>
							</div>
							<div id="fldInmtInfo"><!-- fieldset 보상금신청 신청자 정보 --> 
								<span class="topLine"></span>
								<!-- 그리드스타일 -->
								<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
									<colgroup>
									<col width="20%">
									<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">보상금종류</th>
											<td>방송음악</td>
										</tr>
										<tr>
											<th scope="row">신청인정보</th>
											<td>
												<span class="topLine2"></span>
												<!-- 그리드스타일 -->
												<table cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
													<colgroup>
													<col width="14%">
													<col width="33%">
													<col width="29%">
													<col width="*">
													</colgroup>
													<tbody>
														<tr>
															<th scope="row"><label class="necessary">성명</label></th>
															<td>
																<input type="hidden" name="hddnUserIdnt" value="${clientInfo.USER_IDNT }" />
																<input type="text" name="txtPrpsName" value="${clientInfo.PRPS_NAME }" title="신청자 성" readonly/>
															</td>
															<th scope="row"><label class="necessary">주민등록번호/</label>사업자번호</th>
															<td>
																<input type="text" class="w90" name="txtPrsdCorpNumbView" value="${clientInfo.RESD_CORP_NUMB_VIEW }" title="주민번호/사업자번호" readonly />
															</td>
														</tr>
														<tr>
															<th scope="row"><label class="necessary">전화번호</label></th>
															<td>
																<ul class="list1">
																	<li class="p11"><label for="regi1" class="inBlock w25">자택</label> : <input type="text" id="regi1" name="txtHomeTelxNumb" value="${clientInfo.HOME_TELX_NUMB }" maxlength="14" title="신청자 전화(자택)" class="inputDataN w55" /></li>
																	<li class="p11"><label for="regi2" class="inBlock w25">사무실</label> : <input type="text" id="regi2" name="txtBusiTelxNumb" value="${clientInfo.BUSI_TELX_NUMB }" maxlength="14" title="신청자 전화(사무실)" class="inputDataN w55" /></li>
																	<li class="p11"><label for="regi3" class="inBlock w25">휴대폰</label> : <input type="text" id="regi3" name="txtMoblPhon" value="${clientInfo.MOBL_PHON }" maxlength="14" title="신청자 전화(휴대폰)" class="inputDataN w55" /></li>
																</ul>
															</td>
															<th scope="row">팩스번호</th>
															<td>
																<input name="txtFaxxNumb" value="${clientInfo.FAXX_NUMB }" maxlength="15" title="신청자 전화(FAX)" class="inputDataN w90" />
															</td>
														</tr>
														<tr>
															<th scope="row"><label class="necessary">주소</label></th>
															<td colspan="3">
																<ul class="list1">
																<li class="p11"><label for="txtHomeAddr" class="inBlock w10">자택</label> : <input type="text" name="txtHomeAddr" id="txtHomeAddr" value="${clientInfo.HOME_ADDR }" maxlength="50" title="신청자 주소(자택)" class="inputDataN w85" /></li>
																<li class="p11"><label for="txtBusiAddr" class="inBlock w10">사무실</label> : <input type="text" name="txtBusiAddr" id="txtBusiAddr" value="${clientInfo.BUSI_ADDR }" maxlength="50" title="신청자 주소(사무실)" class="inputDataN w85" /></li>
																</ul>
															</td>
														</tr>
														<tr>
															<th scope="row"><label class="necessary">입금처</label></th>
															<td>
																<ul class="list1">
																<li class="p11"><label for="txtBankName" class="inBlock w30">은행명</label> : <input type="text" id="txtBankName"  name="txtBankName" value="${clientInfo.BANK_NAME }" title="입금 은행명" maxlength="18" class="inputDataN w55" /></li>
																<li class="p11"><label for="txtAcctNumb" class="inBlock w30">계좌번호</label> : <input type="text" id="txtAcctNumb" name="txtAcctNumb" value="${clientInfo.ACCT_NUMB }" title="입금 계좌번호" maxlength="18" class="inputDataN w55" /></li>
																<li class="p11"><label for="txtDptr" class="inBlock w30">예금주</label> : <input type="text" id="txtDptr" name="txtDptr" value="${clientInfo.DPTR }" title="입금 예금주" maxlength="33" class="inputDataN w55" /></li>
																</ul>
															</td>
															<th scope="row">E-Mail</th>
															<td>
																<input type="text" name="txtMail" value="${clientInfo.MAIL }" maxlength="25" title="신청자 EMAIL" class="inputDataN w85" />
															</td>
														</tr>
													</tbody>
												</table>
												<!-- //그리드스타일 -->
											</td>
										</tr>
										<tr>
											<th scope="row">신청구분</th>
											<td>
												<ul class="line22">
													<li>
														<input type="checkbox" name="chkTrts203" title="저작인접권(앨범제작)" id="chkTrts203" onclick="javascript:fn_chgInsertDiv('kapp',this);"
														<c:choose>
															<c:when test="${clientInfo.KAPP != 'Y'}">
																<c:if test="${clientInfo.TRST_203 != '1'}">
																	disabled
																</c:if>
															</c:when>
															<c:otherwise>
																<c:if test="${clientInfo.TRST_203 == '1'}">
																	checked
																</c:if>
															</c:otherwise> 
														</c:choose>
														 class="inputRChk"/>
														 <label for="chkTrts203" class="p12">저작권인접권(앨범제작)</label>
														 <!-- hidden Value Setting -->
														 <c:choose>
															<c:when test="${clientInfo.KAPP != 'Y'}">
																<c:if test="${clientInfo.TRST_203 != '1'}">
																	<input type="hidden" name="hddnInsertKapp" value="N" />
																</c:if>
															</c:when>
															<c:otherwise>
																<c:if test="${clientInfo.TRST_203 == '1'}">
																	<input type="hidden" name="hddnInsertKapp" value="Y" />
																</c:if>
															</c:otherwise> 
														</c:choose>
													</li>
													<li>
														<input type="checkbox" name="chkTrst202" title="저작인접권(가창, 연주, 지휘 등)" id="chkTrts202" onclick="javascript:fn_chgInsertDiv('fokapo',this);" 
														<c:choose>
															<c:when test="${clientInfo.FOKAPO != 'Y'}">
																<c:if test="${clientInfo.TRST_202 != '1'}">
																	disabled
																</c:if>
															</c:when>
															<c:otherwise>
																<c:if test="${clientInfo.TRST_202 == '1'}">
																	checked
																</c:if>
															</c:otherwise> 
														</c:choose> 
														class="inputRChk" />
														<label for="chkTrts202" class="p12">저작권인접권(가창, 연주, 지휘 등)</label>
														<!-- hidden Value Setting -->
														<c:choose>
															<c:when test="${clientInfo.FOKAPO != 'Y'}">
																<c:if test="${clientInfo.TRST_202 != '1'}">
																	<input type="hidden" name="hddnInsertFokapo" value="N" /> 
																</c:if>
															</c:when>
															<c:otherwise>
																<c:if test="${clientInfo.TRST_202 == '1'}">
																	<input type="hidden" name="hddnInsertFokapo" value="Y" /> 
																</c:if>
															</c:otherwise> 
														</c:choose> 
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<th scope="row">신청저작물정보</th>
											<td>
												<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="보상금신청 저작물 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="10%">
													<col width="*">
													<col width="20%">
													<col width="20%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">순번</th>
															<th scope="col">곡명</th>
															<th scope="col">가수</th>
															<th scope="col">방송년도</th>
														</tr>
													</thead>
													<tbody>
													<c:if test="${!empty selectList}">
													<c:forEach items="${selectList}" var="selectList">	
														<c:set var="NO" value="${selectListCnt + 1}"/>
														<c:set var="i" value="${i+1}"/>
														<tr>
															<td class="ce">
																<c:out value="${NO - i}"/>
																<!--<c:out value="${i}"/>-->
																<input type="hidden" name="hddnSelectInmtSeqn" value="${selectList.INMT_SEQN }" />
																<input type="hidden" name="hddnSelectPrpsDivs" value="${selectList.PRPS_DIVS }" />
																<input type="hidden" name="hddnSelectPrpsIdntCode" value="${selectList.PRPS_IDNT_CODE }" />
																<input type="hidden" name="hddnSelectKapp" value="${selectList.KAPP }" />
																<input type="hidden" name="hddnSelectFokapo" value="${selectList.FOKAPO }" />
																<input type="hidden" name="hddnSelectKrtra" value="${selectList.KRTRA }" />
																
																<input type="hidden" name="hddnSelectSdsrName"	value="${selectList.SDSR_NAME }" />
																<input type="hidden" name="hddnSelectMuciName"	value="${selectList.MUCI_NAME }" />
																<input type="hidden" name="hddnSelectYymm"		value="${selectList.YYMM }" />
																<input type="hidden" name="hddnSelectLishComp"	value="${selectList.LISH_COMP }" />
																<input type="hidden" name="hddnSelectUsexType"	value="${selectList.USEX_TYPE }" />
															</td>
															<td>${selectList.SDSR_NAME }</td>
															<td class="ce">${selectList.MUCI_NAME }</td>
															<td class="ce">${selectList.YYMM }</td>
														</tr>
														</c:forEach>
													</c:if>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</div> <!-- //fieldset -->						
							<!-- 음제협 관련 영역 -->
							<div id="fldKapp" style="display:
								<c:choose>
									<c:when test="${clientInfo.TRST_203 != '1'}">none</c:when>
									<c:when test="${clientInfo.KAPP != 'Y'}">none</c:when> 
								</c:choose>;"><!-- fieldSet str -->
								<div class="floatDiv mt20">
									<h3 class="fl">내용(한국음원제작자협회)
										<span class="ml10">
										<input type="checkbox" id="chkKappOff" onclick="javascript:fn_chgDiv('kapp','OFF');" class="inputChk"
											<c:if test="${!empty trstList}">
											<c:forEach items="${trstList}" var="trstList">
												<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
													<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if>
												</c:if>
											</c:forEach>
											</c:if> title="오프라인접수(첨부서류)" />
											<label for="chkKappOff" class="orange thin">오프라인접수(첨부서류)</label>
										<input type="hidden" name="hddnKappOff" /> 
										</span>
									</h3>
									<p class="fr">
										<span id="spanKappBtn"><a href="#1" onclick="javascript:fn_delete('chkKapp','txtKappRghtGrnd','kapp_displaySeq');return false;"><img src="/images/2012/button/delete.gif" alt="삭제" /></a></span>
									</p>
								</div>
								<!-- 보상금신청 항목 테이블 영역입니다 -->
								<div id="divKappItemList" class="div_scroll" style="width:726px; padding:0 0 0 0;">
									<table id="div_scroll" cellspacing="0" cellpadding="0" border="1" class="grid mt5 tableFixed" summary="(한국음원제작자협회)보상금신청 저작물 정보 입력폼입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="4%">
										<col width="6%">
										<col width="*">
										<col width="12%">
										<col width="14%">
										<col width="12%">
										<col width="10%">
										<col width="10%">
										<col width="14%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col" rowspan="2"><input type="checkbox" onclick="javascript:checkBoxToggle('frm','chkKapp','txtKappDateIssu',this);" class="vmid" title="전체선택" /></th>
												<th scope="col" rowspan="2">번<br>호</th>
												<th scope="col" rowspan="2"><label class="necessary white">음반명</label><a href="#1" class="ml10 underline black2" onclick="javascript:toggleLayer('help_pop1');" id="toggleBtn"><img src ="/images/2012/common/ic_find_id.gif" alt="" class="vtop"></a></th>
												<!-- 도움말 레이어 -->
												<div class="layer_pop w80 mt40 ml20" id="help_pop1">
													<h1>음반명 입력안내</h1>
													<div class="layer_con">
														<ul class="list1">
														<c:if test="${srchParam.srchDIVS == '1'}">
															<li class="p11">클래식의 경우 [ 작곡가 : 작품명 / 연주자 ]의 형식으로 입력한다. </li>
															<li class="p11">국내/팝음반의 경우, [ 타이틀(*집) ]의 형식으로 입력한다. </li>
															<li class="p11">스파인에 적혀있는 타이틀(레이블과 레이블 번호를 제외한 모든내용)을 특수 기호("",/ 등)는 제외하고 그대로 적는 것을 원칙으로 한다.</li>
															<li class="p11">단, 의미가 있는 특수기호와 구분(쉼표, 등)을 위한 특수기호는 사용할 수 있다. 예) # -"삽"의 경우</li>
															<li class="p11">FIRST ALBUM이라고 표기되어 있는 경우, 1집이라고 표기한다.</li>
															<li class="p11">로마자는 아라비아 숫자로 대치한다.</li>
															<li class="p11">한글과 외국어 병기시에는 국내는 한글을, 팝은 외국어를 우선하여 표기한다. 외국어가 한자로 표기된 경우 한글로 변환가능한 경우 한글로 기입하고, 본래 표기된 한자명은 부제명에 추가기입한다.</li>
															<li class="p11">단, 음반명이 하나만(한글/외국어) 표기되어 있을 경우, 그 하나를 적고, 필요에 따라(검색어 사용 가능성) 다른 언어를 병기한다.</li>
															<li class="p11">국내 음반명의 경우, 영어로만 표기되어 있을 경우 발음에 따라(해설지에 적힌대로) 한글로 병기한다. 표기형식은 [영문명(한글명/ ○집)으로 한다. 예) BOMBOM(봄봄/ 3집)]</li>
															<li class="p11">[시리즈]에 해당하는 경우, 부제명에 시리즈명을 입력한다.</li>
														</c:if>
														<c:if test="${srchParam.srchDIVS == '2'}">
															<li class="p11"><strong>출판년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 출판년도와 관계없이 검색합니다.</li>
															<li class="p11"><strong>저작물명</strong>: 저작물명은 대소문자 구분없이 검색 합니다.</li>
															<li class="p11"><strong>저작자명</strong>: 저작권자에 해당되는 데이터를 검색 합니다.</li>
														</c:if>
														<c:if test="${srchParam.srchDIVS == '3'}">
															<li class="p11"><strong>발행년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 발행년도와 관계없이 검색합니다.</li>
															<li class="p11"><strong>저작물명</strong>: 저작물명은 대소문자 구분없이 검색 합니다.</li>
															<li class="p11"><strong>저자</strong>: 저자에 해당되는 데이터를 검색 합니다.</li>
														</c:if>
														</ul>
													</div>
													<a href="#1" onclick="javascript:toggleLayer('help_pop1');" class="layer_close"  id="toggleClose"><img src="/images/2012/button/layer_close.gif" alt="닫기" /></a>
												</div>
												<!-- //도움말 레이어 -->
												<th scope="col" rowspan="2">CD코드</th>
												<th scope="col" rowspan="2">곡명</th>
												<th scope="col"><label class="necessary white">가수명</label></th>
												<th scope="col">국가명</th>
												<th scope="col">작사가</th>
												<th scope="col" rowspan="2"><label class="necessary white">음악장르</label><br />(테마코드)</th>
											</tr>
											<tr>
												<th scope="col"><label class="necessary white">발매일</label><a href="#1" onclick="javascript:toggleLayer('help_pop2');" class="ml10 underline black2"><img src ="/images/2012/common/ic_find_id.gif" alt="" class="vtop"></a></th>
												<!-- 도움말 레이어 -->
												<div class="layer_pop w80 mt50 ml20" id="help_pop2">
													<h1>발매일 입력안내</h1>
													<div class="layer_con">
														<ul class="list1">
														<c:if test="${srchParam.srchDIVS == '1'}">
															<li class="p11">yyyymmdd형식으로 입력한다. 년도만 입력하는 경우 월일은 '0101'으로 자동 입력됨 </li>
														</c:if>
														<c:if test="${srchParam.srchDIVS == '2'}">
															<li class="p11"><strong>출판년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 출판년도와 관계없이 검색합니다.</li>
															<li class="p11"><strong>저작물명</strong>: 저작물명은 대소문자 구분없이 검색 합니다.</li>
															<li class="p11"><strong>저작자명</strong>: 저작권자에 해당되는 데이터를 검색 합니다.</li>
														</c:if>
														<c:if test="${srchParam.srchDIVS == '3'}">
															<li class="p11"><strong>발행년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 발행년도와 관계없이 검색합니다.</li>
															<li class="p11"><strong>저작물명</strong>: 저작물명은 대소문자 구분없이 검색 합니다.</li>
															<li class="p11"><strong>저자</strong>: 저자에 해당되는 데이터를 검색 합니다.</li>
														</c:if>
														</ul>
													</div>
													<a href="#1" onclick="javascript:toggleLayer('help_pop2');" class="layer_close"><img src="/images/2012/button/layer_close.gif" alt="" /></a>
												</div>
												<!-- //도움말 레이어 -->
												<th scope="col">권리근거</th>
												<th scope="col">작곡가</th>
											</tr>
										</thead>
										<tbody>
										<c:if test="${empty kappList}">
											<c:if test="${!empty selectList}">
											<c:forEach items="${selectList}" var="selectList">	
											<c:if test="${selectList.KAPP == 'Y'}">
											<c:set var="k" value="${k+1}"/>
												<tr>
													<td rowspan="2" class="pd5"><input type="checkbox" name="chkKapp" class="vmid" title="선택" /></td>
													<td rowspan="2" class="pd5"><input name="kapp_displaySeq" id="kapp_displaySeq_${k}" title="순번" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${k}"/></td>
													<td rowspan="2" class="pd5"><input type="text" name="txtKappDiskName" value="" maxlength="65" title="음반명" class="inputDataN w95 ce" /></td>
													<td rowspan="2" class="pd5"><input type="text" name="txtKappCdCode" value="" maxlength="50" title="CD코드" class="inputDataN w95 ce" /></td>
													<td rowspan="2" class="pd5 ce">
														${selectList.SDSR_NAME }
														<input type="hidden" name="hddnKappSongName" value="${selectList.SDSR_NAME }" title="곡명"/>
														<input type="hidden" name="hddnKappPrpsIdnt" value="${selectList.INMT_SEQN }"/>
														<input type="hidden" name="hddnKappPrpsIdntCode" value="${selectList.PRPS_IDNT_CODE }"/>
													</td>
													<td class="ce pd5"><input type="text" name="txtKappSnerName" value="" maxlength="16" title="가수명" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtKappNatuName" value="" maxlength="16" title="국가명" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtKappLyriWrtr" value="" maxlength="16" title="작사"  class="inputDataN w95 ce" /></td>
													<td class="ce pd5" rowspan="2">
														<select id="selKappMuscGnre" name="selKappMuscGnre" title="음악장르">
															<option value="">선택</option>
															
															<option value="2">일반가요</option>
															<option value="14">성인가요</option>
															<option value="3">댄스</option>
															<option value="4">시낭송</option>
															<option value="5">MR</option>
															
															<option value="6">아동음악</option>
															<option value="7">발라드</option>
															<option value="8">국악</option>
															<option value="9">캐롤</option>
															<option value="10">기독교</option>
															
															<option value="11">불교</option>
															<option value="12">연주곡</option>
															<option value="13">가곡</option>
															<option value="1">OST</option>
															<option value="15">락</option>
															
															<option value="16">일렉트로닉</option>
															<option value="17">클래식</option>
															<option value="18">힙합</option>
															<option value="19">재즈</option>
															<option value="20">알앤비/소울</option>
															
															<option value="21">팝</option>
															<option value="22">월드뮤직</option>
															<option value="23">크로스오버</option>
															<option value="24">포크</option>
															<option value="25">뉴에이지</option>
															
															<option value="26">로고송</option>
															<option value="99">기타</option>
														</select>
													</td>
												</tr>
												<tr>
													<td class="ce pd5"><input type="text" id="txtKappDateIssu" name="txtKappDateIssu" value="" title="발매일" size="8" maxlength="8" onkeypress="javascript:only_arabic(event);" class="inputDataN w95 ce" />
													<td class="ce pd5"><input type="text" name="txtKappRghtGrnd" value="" maxlength="33" title="권리근거"  class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtKappComsWrtr" value="" maxlength="16" title="작곡"  class="inputDataN w95 ce" /></td>
												</tr>
											</c:if>
											</c:forEach>
											</c:if>
										</c:if>
										<c:if test="${!empty kappList}">
											<c:forEach items="${kappList}" var="kappList">	
											<c:set var="k" value="${k+1}"/>
												<tr>
													<td class="ce pd5" rowspan="2"><input type="checkbox" name="chkKapp" class="vmid" title="선택" /></td>
													<td class="ce pd5" rowspan="2"><input name="kapp_displaySeq" id="kapp_displaySeq_${k}" title="순번" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${k}"/></td>
													<td class="ce pd5" rowspan="2"><input type="text" name="txtKappDiskName" value="${kappList.DISK_NAME }" maxlength="65" title="음반명" class="inputDataN w95 ce" /></td>
													<td class="ce pd5" rowspan="2"><input type="text" name="txtKappCdCode" value="${kappList.CD_CODE }" maxlength="50" title="CD코드" class="inputDataN w95 ce" /></td>
													<td rowspan="2">
														${kappList.SONG_NAME }
														<input type="hidden" name="hddnKappSongName" value="${kappList.SONG_NAME }" title="곡명"/>
														<input type="hidden" name="hddnKappPrpsIdnt" value="${kappList.PRPS_IDNT }"/>
														<input type="hidden" name="hddnKappPrpsIdntCode" value="${kappList.PRPS_IDNT_CODE }"/>
													</td>
													<td class="ce pd5"><input type="text" name="txtKappSnerName" value="${kappList.SNER_NAME }" maxlength="16" title="가수명" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtKappNatuName" value="${kappList.NATN_NAME }" maxlength="16" title="국가명" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtKappLyriWrtr" value="${kappList.LYRI_WRTR }" maxlength="16" title="작사"  class="inputDataN w95 ce" /></td>
													<td class="ce pd5" rowspan="2">
														<select name="selKappMuscGnre" title="음악장르">
															<option value="">선택</option>
	
															<option value="2" <c:if test="${kappList.MUSC_GNRE=='2' }">selected</c:if>>일반가요</option>
															<option value="14" <c:if test="${kappList.MUSC_GNRE=='14' }">selected</c:if>>성인가요</option>
															<option value="3" <c:if test="${kappList.MUSC_GNRE=='3' }">selected</c:if>>댄스</option>
															<option value="4" <c:if test="${kappList.MUSC_GNRE=='4' }">selected</c:if>>시낭송</option>
															<option value="5" <c:if test="${kappList.MUSC_GNRE=='5' }">selected</c:if>>MR</option>
															
															<option value="6" <c:if test="${kappList.MUSC_GNRE=='6' }">selected</c:if>>아동음악</option>
															<option value="7" <c:if test="${kappList.MUSC_GNRE=='7' }">selected</c:if>>발라드</option>
															<option value="8" <c:if test="${kappList.MUSC_GNRE=='8' }">selected</c:if>>국악</option>
															<option value="9" <c:if test="${kappList.MUSC_GNRE=='9' }">selected</c:if>>캐롤</option>
															<option value="10" <c:if test="${kappList.MUSC_GNRE=='10' }">selected</c:if>>기독교</option>
															
															<option value="11" <c:if test="${kappList.MUSC_GNRE=='11' }">selected</c:if>>불교</option>
															<option value="12" <c:if test="${kappList.MUSC_GNRE=='12' }">selected</c:if>">연주곡</option>
															<option value="13" <c:if test="${kappList.MUSC_GNRE=='13' }">selected</c:if>>가곡</option>
															<option value="1" <c:if test="${kappList.MUSC_GNRE=='1' }">selected</c:if>>OST</option>
															<option value="15" <c:if test="${kappList.MUSC_GNRE=='15' }">selected</c:if>>락</option>
															
															<option value="16" <c:if test="${kappList.MUSC_GNRE=='16' }">selected</c:if>>일렉트로닉</option>
															<option value="17" <c:if test="${kappList.MUSC_GNRE=='17' }">selected</c:if>>클래식</option>
															<option value="18" <c:if test="${kappList.MUSC_GNRE=='18' }">selected</c:if>>힙합</option>
															<option value="19" <c:if test="${kappList.MUSC_GNRE=='19' }">selected</c:if>>재즈</option>
															<option value="20" <c:if test="${kappList.MUSC_GNRE=='20' }">selected</c:if>>알앤비/소울</option>
															
															<option value="21" <c:if test="${kappList.MUSC_GNRE=='21' }">selected</c:if>>팝</option>
															<option value="22" <c:if test="${kappList.MUSC_GNRE=='22' }">selected</c:if>>월드뮤직</option>
															<option value="23" <c:if test="${kappList.MUSC_GNRE=='23' }">selected</c:if>>크로스오버</option>
															<option value="24" <c:if test="${kappList.MUSC_GNRE=='24' }">selected</c:if>>포크</option>
															<option value="25" <c:if test="${kappList.MUSC_GNRE=='25' }">selected</c:if>>뉴에이지</option>
															
															<option value="26" <c:if test="${kappList.MUSC_GNRE=='26' }">selected</c:if>>로고송</option>
															<option value="99" <c:if test="${kappList.MUSC_GNRE=='99' }">selected</c:if>>기타</option>
														</select>
													</td>
												</tr>
												<tr>
													<td class="ce pd5"><input type="text" name="txtKappDateIssu" id="txtKappDateIssu_${k}" value="${kappList.DATE_ISSU }"  title="발매일" class="inputDataN w95 ce" />
													<td class="ce pd5"><input type="text" name="txtKappRghtGrnd" value="${kappList.RGHT_GRND }" maxlength="33" title="권리근거"  class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtKappComsWrtr" value="${kappList.COMS_WRTR }" maxlength="16" title="작곡"  class="inputDataN w95 ce" /></td>
												</tr>
											</c:forEach>
										</c:if>
										</tbody>
									</table>
								</div>
								<!-- 
								//보상금신청 항목 테이블 영역입니다 --><!--
								<div id="divKappFileList" class="tabelRound mb5" 
									style="display:<c:if test="${!empty trstList}">
									<c:forEach items="${trstList}" var="trstList">
										<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
											<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
										</c:if>
									</c:forEach>
									</c:if>;">
								</div>
								-->
								<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
								<div>
									<table cellspacing="0" cellpadding="0" border="1" class="grid" style="margin-top:1px;" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col">첨부서류</th>
												<td>
													<div id="divKappFileList" class="mb5"
													style="display:<c:if test="${!empty trstList}">
													<c:forEach items="${trstList}" var="trstList">
														<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
															<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
														</c:if>
													</c:forEach>
													</c:if>;">
													</div>
													<span id="addFile" style="display:none;">오프라인접수로 선택한 경우 첨부파일을 등록할 수 없습니다.</span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
								
								
								<!-- 보상금신청 오프라인 관련 영역 -->
								<div id="divKappOffline1" class="white_box">
									<div class="box5">
										<div class="box5_con floatDiv" style="width:713px;">
											<p class="fl mt5"><img src="/images/2012/content/box_img4.gif" alt="" /></p>
											<div class="fl ml30 mt5">
												<h4><strong>한국음원제작자협회 오프라인접수 선택한 경우 해당협회에 관련서류를 제출하여야합니다.</strong></h4>
												<ul class="list1 mt10">
												<li>방송보상금 신청서
												<br><span class="blue2 thin">(보상금 신청현황조회 상세화면에서 방송보상금 신청서 출력 가능. [위치] <u>마이페이지>신청현황>미분배보상금 신청</u>)</span> 
												</li>
												<li>음원등록명세서와 저작인접물(음반,음원 등)<br/>
												<span class="blue2 thin">(보상금 신청현황조회 상세화면에서 음원명세서 출력 가능. [위치] <u>마이페이지>신청현황>미분배보상금 신청</u>)</span>
												</li>
												<li>주민등록등본/법인등기부등본</li>
												<li>사업자등록증(사본) 1부</li>
												<li>인감증명서 1부</li>
												<li>통장사본 1부</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<!-- //보상금신청 오프라인 관련 영역 -->
								
							</div> <!-- //fieldset -->
							
							<!-- 실연자 관련 영역 -->
							<div id="fldFokapo" style="display:
								<c:choose>
									<c:when test="${clientInfo.TRST_202 != '1'}">none</c:when>
									<c:when test="${clientInfo.FOKAPO != 'Y'}">none</c:when> 
								</c:choose>;" ><!-- fieldset 한국음악실연자협회 보상금신청 내용 -->
								<div class="floatDiv mt20">
									<h3 class="fl">내용(한국음악실연자연합회)
										<span class="ml10">
											<input type="checkbox" id="chkFokapoOff" 
												onclick="javascript:fn_chgDiv('fokapo','OFF');" class="inputChk" 
												<c:if test="${!empty trstList}">
													<c:forEach items="${trstList}" var="trstList">
														<c:if test="${trstList.TRST_ORGN_CODE=='202' }">
															<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if>
														</c:if>
													</c:forEach>
												</c:if> 
												title="오프라인접수(첨부서류)" /><label for="chkFokapoOff" class="orange thin">오프라인접수(첨부서류)</label>
											<input type="hidden" name="hddnFokapoOff" /> 
										</span>
									</h3>
									<p class="fr">
										<span id="spanFokapoBtn"><a href="#1" onclick="javascript:fn_delete('chkFokapo','','fokapo_displaySeq'); return false;"><img src="/images/2012/button/delete.gif" alt="삭제"/></a></span>
									</p>
								</div>
								<!-- 보상금신청 실연자정보 테이블 영역입니다 -->
								<div id="divFokapoInmtInfo" class="mb5">
									<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="(한국음악실연자연합회)보상금신청 실연자정보 입력 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="30%">
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><label class="necessary">실연자</label></th>
												<td>
													<ul class="list1">
														<li class="p11"><label for="regi1" class="inBlock w25">본명</label> : <input type="text" id="regi1" name="txtFokapoPemrRlnm" value="${PRPS_NAME}" maxlength="16" title="실연자(본명)" class="inputDataN w60" /></li>
														<li class="p11"><label for="regi2" class="inBlock w25">예명</label> : <input type="text" id="regi2" name="txtFokapoPemrStnm" value="${pemrStnm}" maxlength="16" title="실연자(예명)" class="inputDataN w60" /></li>
														<li class="p11"><label for="regi3" class="inBlock w25">그룹명</label> : <input type="text" id="regi3" name="txtFokapoGrupName" value="${grupName}" maxlength="20" title="실연자(그룹명)" class="inputDataN w60" /></li>
													</ul>
												</td>
												<th scope="col"><label class="necessary">주민등록번호</label></th>
												<td>
												 	<input type="text" name="txtFokapoResdNumb_1" maxlength="6" Size="6" onkeypress="javascript:only_arabic(event);" value="${RESD_CORP_NUMB1 }" title="실연자(주민번호)" class="inputDataN w40" />
													 -
													<input type="password" autocomplete="off" name="txtFokapoResdNumb_2" maxlength="7" Size="7" onkeypress="javascript:only_arabic(event);" value="${RESD_CORP_NUMB2 }" title="실연자(주민번호)" class="inputDataN w40" />
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 실연자정보 테이블 영역입니다 -->
								
								<!-- 보상금신청 항목 테이블 영역입니다 -->
								<div id="divFokapoItemList" class="div_scroll" style="width:726px;">
									<table cellspacing="0" cellpadding="0" border="1" class="grid mt5 tableFixed" summary="(한국음악실연자연합회)보상금신청 저작물 정보 입력폼입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="4%">
										<col width="5%">
										<col width="*">
										<col width="11%">
										<col width="13%">
										<col width="11%">
										<col width="10%">
										<col width="12%">
										<col width="10%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col" class="headH"><input type="checkbox" onclick="javascript:checkBoxToggle('frm','chkFokapo',this);" class="vmid" title="전체선택" /></th>
												<th scope="col">번호</th>
												<th scope="col"><label class="necessary">작품명</label></th>
												<th scope="col"><label class="necessary">가수명</label></th>
												<th scope="col">앨범명</th>
												<th scope="col">앨범발매일</th>
												<th scope="col">실연종류</th>
												<th scope="col">실연내용</th>
												<th scope="col">기여율(%)</th>
											</tr>
										</thead>
										<tbody>
										<c:if test="${empty fokapoList}">
											<c:if test="${!empty selectList}">
											<c:forEach items="${selectList}" var="selectList">	
											<c:if test="${selectList.FOKAPO == 'Y'}">
											<c:set var="kk" value="${kk+1}"/>
												<tr>
													<td class="ce pd5"><input type="checkbox" name="chkFokapo" class="vmid" title="선택" /></td>
													<td class="ce pd5"><input name="fokapo_displaySeq" id="fokapo_displaySeq_${kk}" type="text" title="순번" class="w100 ce" style="border:0px;" readonly="readonly" value="${kk}"/></td>
													<td class="pd5">
														${selectList.SDSR_NAME }
														<input type="hidden" name="hddnFokapoPdtnName" value="${selectList.SDSR_NAME }" title="작품명" />
														<input type="hidden" name="hddnFokapoPrpsIdnt" value="${selectList.INMT_SEQN }" />
														<input type="hidden" name="hddnFokapoPrpsIdntCode" value="${selectList.PRPS_IDNT_CODE }" />
													</td>
													<td class="ce pd5"><input type="text" name="txtFokapoSnerName" value="" maxlength="16" title="가수명" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtFokapoAbumName" value="" maxlength="65" title="앨범명" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtFokapoAbumDateIssu" value="" title="앨범발매일" size="8" maxlength="8" onkeypress="javascript:only_arabic(event);" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtFokapoPemsKind" value="" maxlength="65" title="실연종류" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtFokapoPemsDesc" value="" maxlength="65" title="실연내용" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtFokapoCtbtRate" value="" title="기여율" class="inputDataN w95 ce" /></td>
												</tr>
											</c:if>
											</c:forEach>
											</c:if>
										</c:if>
										<c:if test="${!empty fokapoList}">
											<c:forEach items="${fokapoList}" var="fokapoList">	
											<c:set var="kk" value="${kk+1}"/>
												<tr>
													<td class="ce pd5"><input type="checkbox" name="chkFokapo" class="vmid" title="선택"/></td>
													<td class="ce pd5"><input name="fokapo_displaySeq" id="fokapo_displaySeq_${kk}" title="순번" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${kk}"/></td>
													<td class="pd5">
														${fokapoList.PDTN_NAME }
														<input type="hidden" name="hddnFokapoPdtnName" value="${fokapoList.PDTN_NAME }" title="작품명" />
														<input type="hidden" name="hddnFokapoPrpsIdnt" value="${fokapoList.PRPS_IDNT }" />
														<input type="hidden" name="hddnFokapoPrpsIdntCode" value="${fokapoList.PRPS_IDNT_CODE }" />
													</td>
													<td class="ce pd5"><input type="text" name="txtFokapoSnerName" value="${fokapoList.SNER_NAME }" maxlength="16" title="가수명" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtFokapoAbumName" value="${fokapoList.ABUM_NAME }" maxlength="65" title="앨범명" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtFokapoAbumDateIssu" value="${fokapoList.ABUM_DATE_ISSU }" title="앨범발매일" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtFokapoPemsKind" value="${fokapoList.PEMS_KIND }" maxlength="65"  title="실연종류" class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtFokapoPemsDesc" value="${fokapoList.PEMS_DESC }" maxlength="65" title="실연내용"  class="inputDataN w95 ce" /></td>
													<td class="ce pd5"><input type="text" name="txtFokapoCtbtRate" value="${fokapoList.CTBT_RATE }" title="기여율" class="inputDataN w95 ce" /></td>
												</tr>
											</c:forEach>
										</c:if>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 항목 테이블 영역입니다 -->
								
								<!-- 보상금신청 첨부파일 테이블 영역입니다 
								<div id="divFokapoFileList" class="tabelRound mb5 mt10" style="display:
								<c:if test="${!empty trstList}">
									<c:forEach items="${trstList}" var="trstList">
										<c:if test="${trstList.TRST_ORGN_CODE=='202' }">
											<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
										</c:if>
									</c:forEach>
								</c:if>;">
								</div>
								 //보상금신청 첨부파일 테이블 영역입니다 
								-->
								
								<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
								<div class="mt10">
									<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><label class="necessary">첨부서류</label></th>
												<td>
													<div id=divFokapoFileList class="tabelRound mb5" style="display:
														<c:if test="${!empty trstList}">
															<c:forEach items="${trstList}" var="trstList">
																<c:if test="${trstList.TRST_ORGN_CODE=='202' }">
																	<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
																</c:if>
															</c:forEach>
														</c:if>;">
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
								
								<!-- 보상금신청 오프라인 관련 영역 -->
								<div id="divFokapoOffline1" class="white_box">
									<div class="box5">
										<div class="box5_con floatDiv" style="width:713px;">
											<p class="fl mt5"><img src="/images/2012/content/box_img4.gif" alt="" /></p>
											<div class="fl ml30 mt5">
												<h4>한국음악실연자연합회 오프라인접수 선택한 경우 해당연합회에 관련서류를 제출하여야합니다.</h4>
												<ul class="list1 mt10">
												<li>방송보상금 신청서
												<br><span class="blue2 thin">(보상금 신청현황조회 상세화면에서 방송보상금 신청서 출력 가능. [위치] <u>마이페이지>신청현황>미분배보상금 신청</u>)</span> 
												</li>
												<li>실연자임을 증명할 수 있는 서류
												<br><span class="blue2 thin">(앨범자켓, 저작권등록증, 저작권인증서, 실연사실확인서의 사본 등)</span>
												</li>
												<li>상속, 양도, 승계 등의 사실을 증명할 수 있는 서류</li>
												<li>주민등록등본</li>
												<li>통장사본 1부</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<!-- //보상금신청 오프라인 관련 영역 -->
							</div><!-- //fieldset -->
							<!-- 실연자 관련 영역 -->
								
							<div id="fldOffline" class="mt20"><!-- fieldeset보상금신청 내용-->
								<!-- 보상금신청(내용) 테이블 영역입니다 -->
								<div id="divContent" class="tabelRound mb5" style="width:726px;">
									<span class="topLine"></span>
									<table cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="보상금신청 내용입력 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><label for="txtareaPrpsDesc" class="necessary">내용</label></th>
												<td scope="col">
													<textarea rows="10" name="txtareaPrpsDesc" id="txtareaPrpsDesc" title="내용" class="h100 w100">${clientInfo.PRPS_DESC }</textarea>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div><!-- //fieldset -->
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_list()">목록</a></span></p>
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_chkSubmit();">보상금 신청 확인</a></span>
							</div>
						</div>
						<!-- //article end -->
					</div>
				</form>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>	
			<!-- CONTENT end-->
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	<SPAN id="helpMessage" style="position:absolute;top:0px;left:0px;height:0px;width:0px;background-color:#ffffff;display:none;z-index:5;border : 1 solid #aaaaaa;">
	</SPAN>
	
	<form name="srchForm" action="#">
	<input type="hidden" name="srchDIVS"		value="${srchParam.srchDIVS }" />
	<input type="hidden" name="srchSdsrName"	value="${srchParam.srchSdsrName }" />
	<input type="hidden" name="srchMuciName"	value="${srchParam.srchMuciName }" />
	<input type="hidden" name="srchYymm"		value="${srchParam.srchYymm }" />
	<input type="hidden" name="nowPage"			value="${srchParam.nowPage }" />
	<input type="submit" style="display:none;">
	</form>

<script type="text/javascript" src="/js/2011/file.js"></script>
<script type="text/javascript">
<!--
	function fn_setFileInfo(){
		var listCnt = '${fn:length(fileList)}';
		<c:if test="${!empty fileList}">
			<c:forEach items="${fileList}" var="fileList">
				<c:choose>
					<c:when test="${fileList.TRST_ORGN_CODE=='203' }">
						fn_addGetFile(	'203',
										'${fileList.PRPS_MAST_KEY}',
										'${fileList.PRPS_SEQN}',
										'${fileList.ATTC_SEQN}',
										'${fileList.FILE_PATH}',
										'${fileList.REAL_FILE_NAME}',
										'${fileList.FILE_NAME}',
										'${fileList.TRST_ORGN_CODE}',
										'${fileList.FILE_SIZE}'
										);
					</c:when>
					<c:when test="${fileList.TRST_ORGN_CODE=='202' }">	
						fn_addGetFile(	'202',
										'${fileList.PRPS_MAST_KEY}',
										'${fileList.PRPS_SEQN}',
										'${fileList.ATTC_SEQN}',
										'${fileList.FILE_PATH}',
										'${fileList.REAL_FILE_NAME}',
										'${fileList.FILE_NAME}',
										'${fileList.TRST_ORGN_CODE}',
										'${fileList.FILE_SIZE}'
										);
					</c:when>
				</c:choose>
			</c:forEach>
		</c:if>		
	}
	window.onload = function(){	
		fn_createTable2("divKappFileList", "203");
		fn_createTable2("divFokapoFileList", "202");	
		fn_setFileInfo();
		resizeDiv('divKappItemList' , 'div_scroll');
		
		fn_confirm();
	}
//-->
</script>
</body>
</html>