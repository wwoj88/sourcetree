<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>미분배 보상금 대상 저작물 확인 서비스 이용 | 내권리찾기 | 권리자찾기</title>

<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css">
<link rel="stylesheet" type="text/css" href="/css/table.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<style type="text/css">
<!--
.divM_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	width: 700px;
	margin-left: 0px;
}

.box {
	background-color:black;
	position:absolute;
	z-index:99999999;
	display:none;
}
-->
</style>

<script type="text/javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script> -->

<script type="text/javascript">

</script>


<script type="text/javascript">

<!--
jQuery(function(){
	//alert('call')
	fn_InitframeList();
	var div = '${srchDIVS}';
	var titleName = "";
	if(div=='1'){
		jQuery("#tab11").children().eq(0).addClass("on").html("<strong>음악<\/strong>").css({color:'#2a2a2a'});
		titleName = "음악";
	}
	else if(div=='2'){
		jQuery("#tab11").children().eq(1).addClass("on").html("<strong>교과용<\/strong>").css({color:'#2a2a2a'});
		titleName = "교과용";
	}
	else if(div=='3'){
		jQuery("#tab11").children().eq(2).addClass("on").html("<strong>도서관<\/strong>").css({color:'#2a2a2a'});
		titleName = "도서관";
	}
	else if(div=='4'){
		jQuery("#tab11").children().eq(3).addClass("on").html("<strong>수업목적<\/strong>").css({color:'#2a2a2a'});
		titleName = "수업목적";
	}
	
	this.title = "미분배 보상금 대상 저작물확인 - "+titleName+" | 미분배 보상금 저작물 찾기 | 권리자찾기";
});

//str
jQuery(window).resize(function(e){
	var docuWidth = jQuery(document).width()-16;
	var docuHeight = jQuery(document).height();
	
	jQuery("#modalBox").css({width:docuWidth,height:docuHeight});
	
	 var yp=document.body.scrollTop;
	   var xp=document.body.scrollLeft;

	   var ws=document.body.clientWidth;
	   var hs=document.body.clientHeight;
	   
	   var ajaxBox=$('ajaxBox');

});
//end


// 로딩 이미지 박스
function showAjaxBox(ment){
	//항상 화면 중앙에 나타나도록...
   var yp=document.body.scrollTop;
   var xp=document.body.scrollLeft;
   

   var ws=document.body.clientWidth;
   var hs=document.body.clientHeight;
   
   
   if(!ment) ment="잠시만 기다려주세요..";
   
   var ajaxBox=$('ajaxBox');

	//str
	var docuWidth = jQuery('#wrap').width();
	var docuHeight = jQuery('#wrap').height();
	
   //back_blackBox
	 jQuery("<div>").addClass("box").attr("id","modalBox").
		css({width:docuWidth,height:docuHeight,left:0,top:0}).appendTo("body"); 
	 	jQuery("#modalBox").fadeTo(100, 0.3);
		jQuery("#modalBox").click(function(){
		jQuery("#modalBox").remove();
	})
	//end

  Element.show(ajaxBox);    
}

// 로딩 이미지 박스 감추기
function hideAjaxBox(){
	Element.hide('ajaxBox');	
	//$('ajaxBox').hide();
	//str
	jQuery("#modalBox").remove();
	//end
	
// 	통합검색에서 넘어온경우 알림메시지
	alram();
}

//검색 조건이 없는 첫화면 조회 //20120220 정병호
function fn_InitframeList()
{
	var div = '${srchParam.srchDIVS }';
	var frm = document.frm;

	frm.target = "ifMuscInmtList";
	if(div == '2'){
		frm.target = "ifSubjInmtList";
	}else if(div == '3'){
		frm.target = "ifLibrInmtList";
	}else if(div == '4'){
		frm.target = "ifLssnInmtList";
	}
	
	//frm.method = "post";
	//frm.action = "/inmtPrps/inmtPrps.do?method=muscInmtList&page_no=1&srchDIVS="+'${srchParam.srchDIVS }';
	//frm.submit();
}

function chk(event){
	ev = window.event || event;

	if((ev.keyCode>=48 && ev.keyCode<=57) || ev.keyCode == "8" || ev.keyCode == "13" ) return true;
	else return false;
}


//iframe 검색
function fn_frameList()
{
	var div = '${srchParam.srchDIVS }';
	var frm = document.frm;

	frm.target = "ifMuscInmtList";
	if(div == '2'){
		frm.target = "ifSubjInmtList";
	}else if(div == '3'){
		frm.target = "ifLibrInmtList";
	}else if(div == '4'){
		frm.target = "ifLssnInmtList";
	}
	
	//showAjaxBox();		// 로딩 이미지 박스 보이게..
	new Ajax.Request("/inmtPrps/inmtPrps.do?method=muscInmtList&page_no=1&srchDIVS="+div ,{   
		method:'POST',
		onLoading: function() {     
			showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {
		//	showAjaxBox();
			hideAjaxBox();
		} 
	});
	frm.method = "post";
	frm.action = "/inmtPrps/inmtPrps.do?method=muscInmtList&page_no=1&srchDIVS="+div;
	frm.submit();
}
//로딩 이미지 박스
function showAjaxBox(ment){
	//항상 화면 중앙에 나타나도록...
   var yp=document.body.scrollTop;
   var xp=document.body.scrollLeft;
   

   var ws=document.body.clientWidth;
   var hs=document.body.clientHeight;
   
   
   if(!ment) ment="잠시만 기다려주세요..";
   
   var ajaxBox=$('ajaxBox');
  // $('ajaxBoxMent').innerHTML=ment;


	//str
	var docuWidth = jQuery('#wrap').width();
	var docuHeight = jQuery('#wrap').height();
	
   //back_blackBox
	 jQuery("<div>").addClass("box").attr("id","modalBox").
		css({width:docuWidth,height:docuHeight,left:0,top:0}).appendTo("body"); 
	 jQuery("#modalBox").fadeTo(100, 0.3);
	jQuery("#modalBox").click(function(){
		jQuery("#modalBox").remove();
	})
	//end

  Element.show(ajaxBox);    
}



// 상세팝업
function openDetail(url, name, openInfo)
{
	window.open(url, name, openInfo);
}

//리사이즈
function resizeIFrame(name) {
	console.log(name)
	var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
	document.getElementById(name).height= the_height+80;

}


//보상금신청
function fn_chkSubmit(sDiv){
	var frm = document.frm;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goLogin";
		//location.href = "/user/user.do?method=goSgInstall";
		return;
	}else{
		var div = '${srchParam.srchDIVS }';
		var isExist = 'N';
		var ifForm = '';
		if(sDiv == '1'){
			var hddnFrm = document.hidForm;
			if(div == '1'){
				ifForm = document.getElementById('ifMuscInmtList').contentWindow.document.ifFrm;
				hddnFrm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}else if(div == '2'){
				ifForm = document.getElementById('ifSubjInmtList').contentWindow.document.ifFrm;
				hddnFrm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}else if(div == '3'){
				ifForm = document.getElementById('ifLibrInmtList').contentWindow.document.ifFrm;
				hddnFrm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}else if(div == '4'){
				ifForm = document.getElementById('ifLssnInmtList').contentWindow.document.ifFrm;
				hddnFrm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}
			
			if(inspectCheckBoxField(ifForm.iChk)){
				//iFrame 항목
			    var ifrmInputVal = (getCheckStr(ifForm, 'Y'));

				hddnFrm.srchSdsrName.value	= frm.srchSdsrName.value;
				hddnFrm.srchYymm.value		= frm.srchYymm.value;
				hddnFrm.srchMuciName.value	= frm.srchMuciName.value;
			    
				hddnFrm.ifrmInput.value = ifrmInputVal;
			    
				hddnFrm.target = "_self";
				hddnFrm.method = "post";
				hddnFrm.submit();
			}else{
				alert("신청대상을 선택해 주세요.");
				return;
			}
		}else{
			//선택 목록 항목
			var chkObjs = document.getElementsByName("chk");

			for(var i = 0; i < chkObjs.length; i++) {
				if(chkObjs[i].checked == true){
					isExist = 'Y';
				}
			}
	
			if(isExist == 'N'){
				alert("신청대상을 선택해 주세요.");
				return;
			}
			
			frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			if(div == '2'){
				frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}else if(div == '3'){
				frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}else if(div == '4'){
				frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}
			frm.target = "_self";
			frm.method = "post";
			frm.submit();
		}
	}
}

//높이 조절
function resizeSelTbl(){
	//var chkObjs = document.getElementsByName("chk");
	//document.getElementById("div_inmtPrps").style.height = (31.5 * (chkObjs.length + 1)) + 13 + "px" ;
	
	resizeDiv("tbl_inmtPrps", "div_inmtPrps");
}

//table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

    var the_height = document.getElementById(name).offsetHeight; //해당 Div의 높이
   var chkId = "chk"//체크박스 네임
   var chkCnt = document.getElementsByName(chkId).length;//현재건수
   var isLong = false;//현재 건수가 15는 넘었는지
   var default_height = 493;
   
   if(chkCnt > 15){
   	document.getElementById(targetName).style.height = default_height+"px";
   	document.getElementById(targetName).style.overflowY = "auto";
   }else{
   	document.getElementById(targetName).style.height = "auto";
   	document.getElementById(targetName).style.overflowY = "hidden";
   }
   //console.log(chkCnt+"개");
   //console.log("the_height[현재의높이는]: "+the_height);
   //console.log("default_height[현재의높이는]: "+default_height);
   
   //document.getElementById(targetName).style.height = the_height+13;
  
}

var gubun = "${gubun}";

function alram(){

	var mesg = '';
	
	// 통합검색에서 넘어온경우 알림메시지 
	//if("${gubun}" == "totalSearch" ){
	if(gubun == "totalSearch" ){
		
		gubun= '';
		
		if("${srchParam.srchSdsrName}"!=''){
			mesg = "[${srchParam.srchSdsrName}] (으)로 ";
		
			if("${srchParam.srchDIVS}" == '1')	mesg+= "곡명";
			if("${srchParam.srchDIVS}" == '2')	mesg+= "저작물명";
			if("${srchParam.srchDIVS}" == '3')	mesg+= "저작물명";
			if("${srchParam.srchDIVS}" == '4')	mesg+= "저작물명";
		}
		
		if("${srchParam.srchMuciName}"!=''){
			mesg = "[${srchParam.srchMuciName}] (으)로 ";
		
			if("${srchParam.srchDIVS}" == '1')	mesg+= "가수";
			if("${srchParam.srchDIVS}" == '2')	mesg+= "저작자명";
			if("${srchParam.srchDIVS}" == '3')	mesg+= "저자";
			if("${srchParam.srchDIVS}" == '4')	mesg+= "저자";
		}
		
		mesg += "(이)가 검색됩니다.\n검색항목 입력으로 상세검색이 가능합니다. ";
	
		alert( mesg);
	}
	
}
//검색 조건이 없는 첫화면 조회 //20120220 정병호
window.onload =	function(){
	fn_InitframeList();
	amntComma();
	//console.log("document.getElementById('chBox1') : " + document.getElementById('chBox1'))
	//console.log("document.getElementById('chBox2') : " + document.getElementById('chBox2'))
	var el = document.getElementById('chBox1');
	var el2 = document.getElementById('chBox2');
	
	if(el){
		el.addEventListener('click', chBoxCheck);
	};
	if(el2){
		el2.addEventListener('click', chBoxCheck);
	};
}

function chBoxCheck(event){
	var chBox1 = document.getElementById('chBox1');
	var chBox2 = document.getElementById('chBox2');
	//console.log(event.target.checked)
	if(event.target.checked){
		chBox1.checked = false;
		chBox2.checked = false;
		event.target.checked = true;
	}
}

function amntComma(){
	var latterAmnt = document.getElementById("latterAmnt");
	var alltAmnt = document.getElementById("alltAmnt");
	
	if(latterAmnt){
		for(var i = 1 ; i < latterAmnt.length ; i++ ){
			latterAmnt.options[i].text = comma(latterAmnt.options[i].text);
		}
		
		for(var j = 1 ; j < (latterAmnt.length-1) ; j++ ){
			alltAmnt.options[j].text = comma(alltAmnt.options[j].text);
		}
		
	}
}

function comma(num){
    var len, point, str; 
       
    num = num + ""; 
    point = num.length % 3 ;
    len = num.length; 
   
    str = num.substring(0, point); 
    while (point < len) { 
        if (str != "") str += ","; 
        str += num.substring(point, point + 3); 
        point += 3; 
    } 
     
    return str;
 
}

//-->
</script>
</head>
<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader2.jsp" /> --%>
		<!-- <script type="text/javascript">initNavigation(2);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			<div class="container_vis">
			</div>

			
			<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu02_2.jsp" />				
			<!-- //래프 -->
				<!--  ajaxBox str -->
				
				<div id="ajaxBox" style="position:absolute; z-index:99999999999; 
										background: url(/images/2012/common/loadingBg.gif) no-repeat 0 0; position: absolute; 
										width: 500px; height: 500px; left: 50%; top: 50%; margin-left: -250px; margin-top: -250px; text-indent: -9999px; font-size:0px;/* border: #000 solid 1px; */">
										저작물을 조회하는 중입니다. 잠시만 기다려 주시기 바랍니다.
										
				</div>
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					<div class="con_rt_head">
					<img src="../images/sub_img/sub_home.png" alt="홈 페이지" />
					&gt;
					미분배 보상금 저작물 찾기
					&gt;
					<span class="bold">미분배 보상금 대상 저작물확인</span>
					</div>
					<h1><div class="con_rt_hd_title">미분배 보상금 대상 저작물확인</div></h1>
					 <div class="sub_contents_con"> 
					<!-- Tab str -->
	
                    <!-- //Tab -->
					<p class="clear"></p>
					<!-- contentForm STR -->
					<!-- hidden form str : 아이프레임데이타 가져가는 용도-->		
					<form name="hidForm" action="#" class="sch">
						<input type="hidden" name="srchDIVS"	value="${srchParam.srchDIVS }">
						<input type="hidden" name="srchSdsrName" />
						<input type="hidden" name="srchYymm" />
						<input type="hidden" name="srchMuciName" />
						<input type="hidden" name="srchAlbmName" />
						<input type="hidden" name="ifrmInput" />
						<input type="submit" style="display:none;">
					</form>
					<form name="frm" action="#" class="sch">
					<input type="submit" style="display:none;">
					<!--  SECTION STR  -->
					<div class="section relative">
						<!-- 검색 -->
						
							<!-- memo 삽입 -->
							<jsp:include page="/common/memo/2011/memo_01.jsp" >
									<jsp:param name="DIVS" value="${srchDIVS}"/>
							</jsp:include>
							
										<table class="sub_tab2 mar_tp20" cellspacing="0" cellpadding="0" width="100%" summary="저작물 조회 검색조건 표로 단체, 곡명, 가수, 앨범명, 보상금액 정렬과 조회 버튼으로 구성되어 있습니다.">
											<caption>저작물 조회 검색조건</caption>
											<colgroup>
												<col width="14%">
												<col width="21%">
												<col width="13%">
												<col width="20%">
												<col width="*">
											</colgroup>
											<tbody>
												<c:if test="${srchParam.srchDIVS == '1'}">
													<tr>
														<th scope="row" id="tb1"><label for="sch3">단체</label></th>
														<td headers="tb1" style="width:21%;">
															<select id="sch3" style="width:83%; height:26px; padding:3px;" name="trstOrgnCode">
																<option value="" selected>통합</option>
																<option value="203">한국음반산업협회</option>
																<option value="202">한국음악실연자연합회</option>
																<!-- <option value="203">음산협</option>
																<option value="202">음실련</option> -->
															</select>
														</td>
														
														<th scope="row"><label for="sch2">곡명</label></th>
														<td colspan="3">
															<input type="text" style="width:80%;padding:3px;" id="sch2" name="srchSdsrName" value="${srchParam.srchSdsrName}"/>
														</td>
														
													</tr>
													<tr>
														<th scope="row"><label for="sch4">가수</label></th>
														<td><input type="text" style="width:80%;padding:3px;" id="sch4" name="srchMuciName" value="${srchParam.srchMuciName}" /></td>
														<th scope="row" ><label for="sch5">앨범명</label></th>
														<td><input type="text" style="width:80%;padding:3px;" id="sch5" name="srchAlbmName" value="${srchParam.srchAlbmName}" /></td>
													</tr>
													<!--  <tr>
														<th scope="col"><label for="sch6">미분배보상금액</label></th>
														<td>
														
														
														<select id="alltAmnt" name="alltAmnt" style="width:90%;height:26px;padding:3px;">
														<%--  <c:forEach var="i" begin="0" end="${maxAmnt}" step="100000"> <!-- scope 생략으로 페이지 영역에 저장 step생략으로 1씩 증가 -->
														  <option value="${i}">${i}</option>
														 </c:forEach> --%>
														 <option value="0      ">0      </option>
														 <option value="150000 ">150000 </option>
														 <option value="300000 ">300000 </option>
														 <option value="450000 ">450000 </option>
														 <option value="600000 ">600000 </option>
														 <option value="750000 ">750000 </option>
														 <option value="900000 ">900000 </option>
														 <option value="1050000">1050000</option>
														 <option value="1200000">1200000</option>
														 <option value="1350000">1350000</option>
														 <option value="1500000">1500000</option>
														 <option value="1650000">1650000</option>
														 <option value="1800000">1800000</option>
														 <option value="1950000">1950000</option>
														 <option value="2100000">2100000</option>
														 <option value="2250000">2250000</option>
														 <option value="2400000">2400000</option>
														 <option value="2550000">2550000</option>
														 <option value="2700000">2700000</option>
														 <option value="2850000">2850000</option>
														 <option value="3000000">3000000</option>
														 <option value="3150000">3150000</option>
														 <option value="3300000">3300000</option>
														 <option value="3450000">3450000</option>
														 <option value="3600000">3600000</option>
														 <option value="3750000">3750000</option>
														 <option value="3900000">3900000</option>
														 <option value="4050000">4050000</option>
														 <option value="4200000">4200000</option>
														 <option value="4350000">4350000</option>
														 <option value="4500000">4500000</option>
														 <option value="4650000">4650000</option>
														 <option value="4800000">4800000</option>
														 <option value="4950000">4950000</option>
														 <option value="5100000">5100000</option>
														 <option value="5250000">5250000</option>
														 <option value="5400000">5400000</option>
														</select>원
														</td>
														<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;~</td>
														<td>
											
														<select id="latterAmnt" name="latterAmnt" style="width:90%;height:26px;padding:3px;">
														<option value="${maxAmnt}">전체</option>
													<%-- 	 <c:forEach var="j" begin="0" end="${maxAmnt}" step="100000"> <!-- scope 생략으로 페이지 영역에 저장 step생략으로 1씩 증가 -->
														  <option value="${j}">${j}</option>
														 </c:forEach> --%>
												    	 <option value="0      ">0      </option>
														 <option value="150000 ">150000 </option>
														 <option value="300000 ">300000 </option>
														 <option value="450000 ">450000 </option>
														 <option value="600000 ">600000 </option>
														 <option value="750000 ">750000 </option>
														 <option value="900000 ">900000 </option>
														 <option value="1050000">1050000</option>
														 <option value="1200000">1200000</option>
														 <option value="1350000">1350000</option>
														 <option value="1500000">1500000</option>
														 <option value="1650000">1650000</option>
														 <option value="1800000">1800000</option>
														 <option value="1950000">1950000</option>
														 <option value="2100000">2100000</option>
														 <option value="2250000">2250000</option>
														 <option value="2400000">2400000</option>
														 <option value="2550000">2550000</option>
														 <option value="2700000">2700000</option>
														 <option value="2850000">2850000</option>
														 <option value="3000000">3000000</option>
														 <option value="3150000">3150000</option>
														 <option value="3300000">3300000</option>
														 <option value="3450000">3450000</option>
														 <option value="3600000">3600000</option>
														 <option value="3750000">3750000</option>
														 <option value="3900000">3900000</option>
														 <option value="4050000">4050000</option>
														 <option value="4200000">4200000</option>
														 <option value="4350000">4350000</option>
														 <option value="4500000">4500000</option>
														 <option value="4650000">4650000</option>
														 <option value="4800000">4800000</option>
														 <option value="4950000">4950000</option>
														 <option value="5100000">5100000</option>
														 <option value="5250000">5250000</option>
														 <option value="5400000">5400000</option>
														</select>원
														</td>
														<%-- <th scope="col"><label for="sch5">앨범명</label></th>
														<td><input type="text" style="width:95%;padding:3px;" id="sch5" name="srchAlbmName" value="${srchParam.srchAlbmName}" /></td> --%>
													</tr>-->
													<tr>
														<th scope="row" id="tb2">보상금액 정렬</th>
														<td style="width:19%;" headers="tb2">
															<label><input id="chBox1" name="sort" value="DESC" title="내림차순정렬" type="checkbox">내림차순</label>
															<label><input id="chBox2" name="sort" value="ASC" title="오름차순정렬" type="checkbox">오름차순</label>
														</td><td></td>
														<td align="right"><a href="#fn_frameList" onclick="javascript:fn_frameList();" onkeypress="javascript:fn_frameList();" class="music_search" style="margin-right: 40px;"><img src="/images/sub_img/sub_25.gif" alt="조회" /></a></td>
													</tr>
													
												</c:if>
												<c:if test="${srchParam.srchDIVS == '2'}">
													<tr>
														<th scope="row"><label for="sch3">저작물명</label></th>
														<td colspan="3">
														<input type="text" style="width:100%;padding:3px;" id="sch3" name="srchSdsrName" value="${srchParam.srchSdsrName}" />	
														</td>
													</tr>
													<tr>
														<th scope="row"><label for="sch2">저작자명</label></th>
														<td><input type="text" style="width:100%;padding:3px;" id="sch2" name="srchMuciName" value="${srchParam.srchMuciName}" /></td>
														<th scope="row"><label for="sch4">출판년도</label></th>
														<td><input type="text" style="width:100%; padding:3px; ime-mode:disabled; character:KE;" id="sch4" name="srchYymm" onkeypress="return chk()" value="${srchParam.srchYymm}" size="16" maxlength="4"/></td>
														<td rowspan="3" class="bro175_posi">
															<a href="#fn_frameList" onclick="javascript:fn_frameList();" onkeypress="javascript:fn_frameList();" class="music_search" style="margin-top: -40px;"><img src="/images/sub_img/sub_25.gif" alt="조회" /></a>
															<div class="bro130"></div>
														</td>
													</tr>
												</c:if>
												<c:if test="${srchParam.srchDIVS == '3'}">
													<tr>
														<th scope="row"><label for="sch3">저작물명</label></th>
														<td colspan="3">
															<input type="text" style="width:100%;padding:3px;" id="sch3" name="srchSdsrName" value="${srchParam.srchSdsrName}" maxlength="50"/>
														</td>
													</tr>
													<tr>
														<th scope="row"><label for="sch2">저자</label></th>
														<td><input type="text" style="width:100%;padding:3px;" id="sch2" name="srchMuciName" value="${srchParam.srchMuciName}" maxlength="50"/></td>
														<th scope="row"><label for="sch4">발행년도</label></th>
														<td><input type="text" id="sch4" name="srchYymm" style="width:100%;padding:3px;ime-mode:disabled;character=KE;" onkeypress='return chk()' value="${srchParam.srchYymm}" maxlength="4"/></td>
														<td rowspan="3" class="bro175_posi">
															<a href="#fn_frameList" onclick="javascript:fn_frameList();" onkeypress="javascript:fn_frameList();" class="music_search" style="margin-top: -40px;"><img src="/images/sub_img/sub_25.gif" alt="조회" /></a>
															<div class="bro130"></div>
														</td>
														
													</tr>
												</c:if>
												<c:if test="${srchParam.srchDIVS == '4'}">
													<tr>
														<th scope="row"><label for="sch3">저작물명</label></th>
														<td colspan="3">
														<input type="text" style="width:100%;padding:3px;" id="sch3" name="srchSdsrName" value="${srchParam.srchSdsrName}" maxlength="50"/>
														</td>
													</tr>
													<tr>
														<th scope="row"><label for="sch2">저작자</label></th>
														<td><input type="text" style="width:100%;padding:3px;" id="sch2" name="srchMuciName" value="${srchParam.srchMuciName}" maxlength="50"/></td>														
														<th scope="row"><label for="sch4">발행년도</label></th>
														<td><input type="text" style="width:100%;padding:3px;ime-mode:disabled;character=KE;" id="sch4" name="srchYymm" onkeypress='return chk()' value="${srchParam.srchYymm}" maxlength="4"/></td>
														<td rowspan="3" class="bro175_posi">
															<a href="#fn_frameList" onclick="javascript:fn_frameList();" onkeypress="javascript:fn_frameList();" class="music_search" style="margin-top: -40px;"><img src="/images/sub_img/sub_25.gif" alt="조회" /></a>
															<div class="bro130"></div>
														</td>
													</tr>
												</c:if>
											</tbody>
										</table>
										<div>
											<div class="bg_fafafa"><!--* <strong>상기의 미분배보상금 금액은 음산협,음실련의 분배시점에 따라 금액이 상이함으로 차이가 있을 수 있음을 알려드립니다.</strong><br><br>-->* 검색은 원하는 항목에 찾고자 하는 검색어를 기입한 후 오른쪽 ‘조회’ 버튼을 클릭하세요.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#1" onclick="javascript:toggleLayer('help_pop1');" id="toggleBtn">도움말 <img src="/images/sub_img/sub_27_1.gif" alt="" /></a></div>
											<!-- <p class="gray_box_line">&lowast; 검색은 원하는 항목에 찾고자 하는 검색어를 기입한 후 오른쪽 ‘조회’ 버튼을 클릭하세요.<a href="#1" onclick="javascript:toggleLayer('help_pop1');" class="ml10 underline black2">도움말<img src="/images/2012/common/ic_help.gif" class="vmid ml5" alt="" /></a></p> -->
											
											<!-- 도움말 레이어 -->
											<div class="layer_pop w80" id="help_pop1">
												<h1>검색도움말</h1>
												<div class="layer_con">
													<ul class="list1">
													<c:if test="${srchParam.srchDIVS == '1'}">
														<!-- <li class="p11"><strong>방송년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 방송년도와 관계없이 검색합니다.</li> -->
														<li class="p11"><strong>곡명</strong>: 곡명에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11" style="margin-top: 5px;"><strong>가수</strong>: 가수명에 데이터를 검색 합니다.</li>
														<li class="p11" style="margin-top: 5px;"><strong>앨범명</strong>: 앨범명에 해당되는 데이터를 검색 합니다.</li>
													</c:if>
													<c:if test="${srchParam.srchDIVS == '2'}">
														<!-- <li class="p11"><strong>출판년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 출판년도와 관계없이 검색합니다.</li> -->
														<li class="p11"><strong>출판년도</strong>: '2011'과 같이 출판년도를 입력하여 해당되는 데이터를 검색 합니다.</li>
														<li class="p11" style="margin-top: 5px;"><strong>저작물명</strong>: 저작물명에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11" style="margin-top: 5px;"><strong>저작자명</strong>: 저작권자에 해당되는 데이터를 검색 합니다.</li>
													</c:if>
													<c:if test="${srchParam.srchDIVS == '3'}">
														<!-- <li class="p11"><strong>발행년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 발행년도와 관계없이 검색합니다.</li> -->
														<li class="p11"><strong>발행년도</strong>: '2011'과 같이 발행년도를 입력하여 해당되는 데이터를 검색 합니다.</li>
														<li class="p11" style="margin-top: 5px;"><strong>저작물명</strong>: 저작물명에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11" style="margin-top: 5px;"><strong>저자</strong>: 저자에 해당되는 데이터를 검색 합니다.</li>
													</c:if>
													<c:if test="${srchParam.srchDIVS == '4'}">
														<!-- <li class="p11"><strong>발행년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 발행년도와 관계없이 검색합니다.</li> -->
														<li class="p11"><strong>발행년도</strong>: '2011'과 같이 발행년도를 입력하여 해당되는 데이터를 검색 합니다.</li>
														<li class="p11" style="margin-top: 5px;"><strong>저작물명</strong>: 저작물명에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11" style="margin-top: 5px;"><strong>저자</strong>: 저자에 해당되는 데이터를 검색 합니다.</li>
													</c:if>
													</ul>
												</div>
												<a href="#1" onclick="javascript:toggleLayer('help_pop1');" class="layer_close" id="toggleClose"><img src="/images/2012/button/layer_close.gif" alt="닫기" /></a>
											</div>
											<!-- //도움말 레이어 -->
										</div>
							
								<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
						<!-- //검색 -->
				
						<div class="floatDiv mt20">
						<!-- 아이프레임 str -->
						<c:if test="${srchParam.srchDIVS == '1'}">
							<iframe id="ifMuscInmtList" title="저작물조회(방송음악)" name="ifMuscInmtList" 
							width="100%" height="440" marginwidth="0" marginheight="0" frameborder="0" 
							scrolling="no" style="overflow-y:hidden;" ></iframe>
						</c:if>
						<c:if test="${srchParam.srchDIVS == '2'}">
							<iframe id="ifSubjInmtList" title="저작물조회(교과용)" name="ifSubjInmtList" 
							width="100%" height="440" marginwidth="0" marginheight="0" frameborder="0" 
							scrolling="no" style="overflow-y:hidden;" ></iframe>
						</c:if>
						<c:if test="${srchParam.srchDIVS == '3'}">
							<iframe id="ifLibrInmtList" title="저작물조회(도서관)" name="ifLibrInmtList" 
							width="100%" height="440" marginwidth="0" marginheight="0" frameborder="0" 
							scrolling="no" style="overflow-y:hidden;" ></iframe>
						</c:if>
						<c:if test="${srchParam.srchDIVS == '4'}">
							<iframe id="ifLssnInmtList" title="저작물조회(수업목적)" name="ifLssnInmtList" 
							width="100%" height="440" marginwidth="0" marginheight="0" frameborder="0" 
							scrolling="no" style="overflow-y:hidden;" ></iframe>
						</c:if>
						</div>
				
						<!-- //그리드스타일 -->
					</div>
					<!--  SECTION end  -->
					</form>
					<!-- contentForm end -->
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
			<p class="clear"></p>
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
<script type="text/javascript">
Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
</body>
</html>
