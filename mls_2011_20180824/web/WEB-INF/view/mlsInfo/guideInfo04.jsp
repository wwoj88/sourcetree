<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(6);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_6.gif" alt="이용안내" title="이용안내" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/main/main.do?method=info01"><img src="/images/2011/content/sub_lnb0601_off.gif" title="저작권찾기 의미" alt="저작권찾기 의미" /></a></li>
					<li id="lnb2"><a href="/main/main.do?method=info02"><img src="/images/2011/content/sub_lnb0602_off.gif" title="권리자 저작권찾기 신청" alt="권리자 저작권찾기 신청" /></a></li>
					<li id="lnb3"><a href="/main/main.do?method=info03"><img src="/images/2011/content/sub_lnb0603_off.gif" title="이용자 저작권찾기 신청" alt="이용자 저작권찾기 신청" /></a></li>
					<li id="lnb4"><a href="/main/main.do?method=info04"><img src="/images/2011/content/sub_lnb0604_off.gif" title="보상금 신청" alt="보상금 신청" /></a></li>
					<li id="lnb5"><a href="/main/main.do?method=info05"><img src="/images/2011/content/sub_lnb0605_off.gif" title="권리자미확인저작물 이용" alt="권리자미확인저작물 이용" /></a></li>
                    <li id="lnb6"><a href="/main/main.do?method=goContactUs"><img src="/images/2011/content/sub_lnb0606_off.gif" title="이용문의" alt="이용문의" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb4");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>이용안내</span><em>보상금 신청</em></p>
					<h1><img src="/images/2011/title/content_h1_0604.gif" alt="보상금 신청1" title="보상금 신청1" /></h1>
					
					<div class="section">
						<div class="w100">
                    	<img src="/images/2011/content/guideInfo04_subimg01.gif" alt="보상금 신청 이미지" class="fl" />
                        	<div class="fr w70 mt10"><img src="/images/2011/content/guideInfo04_subimg02.gif" alt="보상금이란?" />
                            <p class="w70 mt10">저작권법에서는 저작물의 이용 촉진 활성화를 위하여 일정 사유에 대해 일일이 저작권자의 허락과 협의 절차를 거치지 않고 이용자가 저작물을 이용할 수 있도록 하고 있습니다.</p>
							<p class="mt30"><img src="/images/2011/content/guideInfo04_subimg03.gif" alt="보상금제도란?" /></p>                      
                            <p class="w70 mt10">이러한 저작물 사용에 대한 대가를 권리자에게 제공하는 것입니다.<br> 방송보상금은 방송에 사용된 음악 작품의 실연자, 최초 음반 제작자에게 보상되며 교과용보상금은 교과용 도서에 공표된 저작물의 권리자, 도서관보상금은 도서관 내에서 자료출력, 전송된 저작물의 권리자 등에게 보상됩니다.</p>
                            <p class="mt30"><img src="/images/2011/content/guideInfo04_subimg04.gif" alt="권리자가 보상금 발생 저작물을 조회한 결과 저작물이 사용되었으나 보상금을 분배받지 못한 경우에 신청합니다." /></p>
                            <p class="mt20">
                            	<a href="/inmtPrps/inmtPrps.do?mNum=4&sNum=0&leftsub=0&srchDIVS=1"><img title="보상금 신청" alt="보상금신청" src="/images/2011/button/btn_app4.gif"></a>
                            	<a href="http://www.copycle.or.kr/jsp/comm/NormalCtrl.jsp?L=2&M=4&S=2" target="_blank"><img title="교과용보상금 분배절차 바로가기" alt="교과용보상금 분배절차 바로가기" src="/images/2011/button/btn_app4_02.gif"></a>
                            	<a href="http://www.copycle.or.kr/jsp/comm/NormalCtrl.jsp?L=4&M=4&S=2" target="_blank"><img title="도서관보상금 분배절차 바로가기" alt="도서관보상금 분배절차 바로가기" src="/images/2011/button/btn_app4_03.gif"></a>
                            </p>
							<h2 class="mt30">신청절차</h2>
                            <p><img src="/images/2011/content/guideInfo04_subimg05.gif" alt="" /></p>
                            </div>
                        </div>		
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>