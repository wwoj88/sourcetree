<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(6);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_6.gif" alt="이용안내" title="이용안내" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/main/main.do?method=info01"><img src="/images/2011/content/sub_lnb0601_off.gif" title="저작권찾기 의미" alt="저작권찾기 의미" /></a></li>
					<li id="lnb2"><a href="/main/main.do?method=info02"><img src="/images/2011/content/sub_lnb0602_off.gif" title="권리자 저작권찾기 신청" alt="권리자 저작권찾기 신청" /></a></li>
					<li id="lnb3"><a href="/main/main.do?method=info03"><img src="/images/2011/content/sub_lnb0603_off.gif" title="이용자 저작권찾기 신청" alt="이용자 저작권찾기 신청" /></a></li>
					<li id="lnb4"><a href="/main/main.do?method=info04"><img src="/images/2011/content/sub_lnb0604_off.gif" title="보상금 신청" alt="보상금 신청" /></a></li>
					<li id="lnb5"><a href="/main/main.do?method=info05"><img src="/images/2011/content/sub_lnb0605_off.gif" title="권리자미확인저작물 이용" alt="권리자미확인저작물 이용" /></a></li>
                    <li id="lnb6"><a href="/main/main.do?method=goContactUs"><img src="/images/2011/content/sub_lnb0606_off.gif" title="이용문의" alt="이용문의" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb2");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>이용안내</span><em>권리자 저작권찾기 신청</em></p>
					<h1><img src="/images/2011/title/content_h1_0602.gif" alt="권리자 저작권찾기 신청" title="권리자 저작권찾기 신청" /></h1>
					
					<div class="section">
                    	<div class="w100">
                    	<img src="/images/2011/content/guideInfo02_subimg01.gif" alt="권리자 저작권찾기 신청 이미지" class="fl" />
                        	<div class="fr w70 mt35 info02Bg">
                            <p class="strong black w70">권리자가 자신이 창작한 저작물의 권리관계정보를 조회한 결과 저작물 정보가 누락되는 등 오류가 있을 경우 신청합니다.</p>
                            <p class="mt25 w70">이때, 저작권신탁단체의 신탁회원 또는 보상금 분배회원이라면 해당 신탁단체에서 신청된 내용을 확인하여 처리하게 됩니다.<br>
신탁단체의 신탁회원 또는 보상금 분배회원이 아니라면 한국저작권위원회의 저작권등록을 함으로써 처리할 수 있습니다.</p>
                            <p class="mt20 h110">
                            	<a href="/rghtPrps/rghtSrch.do?DIVS=M">
									<img title="권리자 저작권찾기 신청" alt="권리자 저작권찾기 신청" src="/images/2011/button/btn_app2.gif">
								</a>
							</p>
                            </div>
                            <div class="fr">
                            <h2>신청절차</h2>
                            <img src="/images/2011/content/guideInfo02_subimg03.gif" alt="" class="fl" /></div>
                        </div>	
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>