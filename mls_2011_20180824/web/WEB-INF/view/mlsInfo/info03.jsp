<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_6.png" alt="이용안내" title="이용안내" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/main/main.do?method=info01">저작권찾기란?<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info02">저작권찾기의 필요성<span>&lt;</span></a></li>
							<li class="active"><a href="/main/main.do?method=info03">저작권 미확인 저작물<br>이란?<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info04">참여방법<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=goContactUs">이용문의<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>이용안내</span>&gt;<strong>저작권 미확인 저작물이란?</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>저작권 미확인 저작물이란?
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<div class="section">
						<p><img src="/images/2010/contents/infoImg3.gif" alt="Welcome 저작권찾기에 오신 것을 환영합니다." title="Welcome 저작권찾기에 오신 것을 환영합니다." /></p>
						<p class="ml40 gray2 mt10"><strong>저작권찾기</strong> 이전에 저작물이 무엇인지 알아야 합니다.</p>
						<ul class="list_type1 ml30 mt10">
							<li><strong>저작물이란?</strong>
								<p class="gray2">저작물이란 인간의 사상 또는 감정을 표현한 창작물을 말합니다.</p>
								<p>따라서 모든 문화적 결과물은 저작물이라 할 수 있습니다.<br />
그리고 저작권이란 이러한 저작물을 창작한 사람, 즉 저작자가 가지는 권리를 말합니다. <br />

또한, 저작권과 유사한 권리로 저작인접권이 있습니다. 저작물을 일반인들이 향유할 수 있도록 매개해주는 배우나 가수,<br /> 
연주자 등의 실연자들과 음반제작자 및 방송 사업자들에게 주어지는 권리입니다.<br />
저작권은 크게 보아 지적재산권의 한 부분이지만, 특허나 상표 등의 산업재산권과는 달리 별도의 등록 등의 절차 없이 <br />
(이를 ‘무방식주의’라고 합니다) 저작물을 창작하는 순간 권리가 발생합니다.</p>
							</li>
							<li class="mt20"><strong>저작권미확인 저작물이란?</strong>
							<p class="gray2">저작물의 권리관계가 명확하게 밝혀지지 않은 저작물을 말합니다.</p>
								<ul>
									<li class="bgNone pl0 mt10">- 저작권미확인 저작물 범위
										<ul>
											<li class="bgNone">① 저작자 정보가 없는 경우입니다. </li>
											<li class="bgNone">② 저작자 정보는 있지만 저작권자 정보를 확인할 수 없는 경우입니다.</li>
											<li class="bgNone">③ 공저자인 경우 위 ①, ② 항목을 하나라도 포함하는 경우입니다.</li>
										</ul>
									</li>
									<li class="bgNone pl0 mt10">- 저작권미확인 표시 및 표시 제거되는 기준
										<ul>
											<li class="bgNone">① 저작권미확인 저작물 범위에 속한 저작물의 경우 ‘저작권미확인’으로 표시합니다.</li>
											<li class="bgNone">② 저작권미확인 저작물의 저작권자 정보가 확인되는 경우 ‘저작권미확인’ 표시를 제거합니다.</li>
										</ul>
									</li>
								</ul>
							</li>
							<li class="mt20"><strong>저작물을 올바르게 이용하려면...</strong>
								<ul>
									<li class="bgNone">① 어떤 저작물을 이용할 것인지 결정합니다.</li>
									<li class="bgNone">② 그 저작물이 보호받는 것인지 확인합니다.</li>
									<li class="bgNone">③ 저작물 이용 방식이 저작권법상 허용되는 방식인지 확인합니다. </li>
									<li class="bgNone">④ 저작권자에게 저작물 제목과 이용하려는 방법 등을 자세히 알리고 이용에 대한 허락을 받습니다.</li>
									<li class="bgNone">⑤ 허락받은 범위 내에서만 이용하며, 저작권자의 의사에 따라 저작자 표시, 출처 표시 등을 명확히 하고 씁니다. </li>
								</ul>
							</li>
						</ul>
					</div>	
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>