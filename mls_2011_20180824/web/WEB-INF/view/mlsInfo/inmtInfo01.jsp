<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>미분배 보상금 대상 저작물 확인 소개 및 이용방법 - 소개 | 내권리찾기 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css">
<link rel="stylesheet" type="text/css" href="/css/table.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js"></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-2', 'auto');
  ga('send', 'pageview');
//-->
</script>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader2.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(1);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title">미분배 보상금 <br/> 저작물 찾기 </div>
			<ul class="sub_lf_menu">
				<li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">미분배 보상금 대상 저작물 확인</a></li>
				<li><a href="/mlsInfo/inmtInfo02.jsp" class="on">소개 및 이용방법</a></li>
				<!-- <li><a href="/mlsInfo/rghtInfo01.jsp">저작권 정보 확인</a>
					<ul class="sub_lf_menu2 disnone">
						<li><a href="/mlsInfo/rghtInfo01.jsp">소개 및 이용방법</a></li>
						<li><a href="/rghtPrps/rghtSrch.do?DIVS=M">서비스 이용</a></li>
					</ul>
				</li> -->
				<!-- <li><a href="/mlsInfo/inmtInfo02.jsp" class="on">미분배 보상금 대상 저작물 확인</a>
					<ul class="sub_lf_menu2">
						<li><a href="/mlsInfo/inmtInfo02.jsp" class="on">소개 및 이용방법</a></li>
						<li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">서비스 이용</a></li>
					</ul>
				</li> -->
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				내 권리 찾기
				&gt;
				<span class="bold">소개 및 이용방법</span>
			</div>
			<div class="con_rt_hd_title">소개 및 이용방법</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<h1 class="sub_con_h2 float_lf">미분배 보상금 대상 저작물 조회 방법</h1>
				<div class="float_rt"><a href="/mlsInfo/inmtInfo02.jsp" class="method_bg_25869b">서비스 소개 바로가기</a></div>
				<p class="clear"></p>
				<p class="mar_tp5">1. 조회하려는 저작물의 분야를 선택하고 검색 키워드를 입력하신 후 조회 버튼을 클릭합니다.</p>
				<p class="mar_tp5">(선택하신 분야에 따라 검색 항목은 다를 수 있습니다.)</p>
				<ul class="sub_menu1 w200 mar_tp40">
					<li class="on"><a href="#none" class="on">방송음악</a></li>
					<li><a href="#none">교과용</a></li>
					<li><a href="#none">도서관</a></li>
					<li><a href="#none" class="last_rt_bor">수업목적</a></li>
				</ul>
				<p class="clear"></p>
				<div class="mar_tp20"><img src="/images/sub_img/sub_35.jpg" alt="그림" /></div>
				<p class="mar_tp20">2. 검색 결과를 확인하고 저작물의 제목(제호)을 클릭하여 상세정보 팝업에서 저작물 및 저작권 정보를 확인합니다.</p>
				<div class="mar_tp20"><img src="/images/sub_img/sub_36.jpg" alt="그림" /></div>
				<p class="mar_tp20">3. 보상금신청은 검색화면 상단에 안내된 단체로 관련사항을 문의합니다.</p>
				<div class="mar_tp20"><img src="/images/sub_img/sub_37.jpg" alt="그림" /></div>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
	  	<!-- //CONTAINER --> 
	  	
	  	<!-- FOOTER str-->
	  	<!-- 2017변경 -->
			<jsp:include page="/include/2017/footer.jsp" />
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	    	
</body>
</html>