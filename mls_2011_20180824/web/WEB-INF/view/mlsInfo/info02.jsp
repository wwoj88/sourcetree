<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_6.png" alt="이용안내" title="이용안내" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/main/main.do?method=info01">저작권찾기란?<span>&lt;</span></a></li>
							<li class="active"><a href="/main/main.do?method=info02">저작권찾기의 필요성<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info03">저작권 미확인 저작물<br>이란?<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info04">참여방법<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=goContactUs">이용문의<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>이용안내</span>&gt;<strong>저작권찾기의 필요성</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>저작권찾기의 필요성
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<div class="section">
						<p><img src="/images/2010/contents/infoImg2.gif" alt="Welcome 저작권찾기에 오신 것을 환영합니다." title="Welcome 저작권찾기에 오신 것을 환영합니다." /></p>
						<ul class="list_type1 ml30">
							<li><strong> 저작물에 대한 권리정보를 확보하는 것입니다.</strong>
								<p class="ml5">이를 통하여 권리자에 대한 정당한 보상 및 이용자의 저작물 이용에 대해서 원활한 권리처리가 이루어지도록 저작물의 권리관계를 
확인하는 것이라고 할 수 있습니다.</p>
							</li>
							<li class="mt20"><strong>저작물 이용에 대한 정당한 보상을 받을 수 있습니다.</strong>
								<ul>
									<li class="bgNone pl0 mt10">- 저작권찾기를 통해 정당한 저작권를 확인받으면서 저작물 이용에 대한 보상금을 받을 수 있습니다.</li>
									<li class="bgNone pl0 mt10">- 보상금제도
										<p class="ml10 gray2 fontSmall">저작권법에서는 저작물의 이용 촉진 활성화를 위하여 일정한 이용 사유에 대하여 저작권자의 허락이나 협의 절차를
  거치지 않고 이용자가 저작물을 이용할 수 있도록 하고 있습니다. 
  보상금제도란 이러한 저작물 사용에 대한 대가를 권리자에게 제공하는 것입니다.</p>
									</li>
									<li class="bgNone pl0 mt10">- 보상금의 종류는?
										<p class="ml10 gray2 fontSmall">방송보상금은 방송에 사용된 음악 작품의 실연자와 최초 음반 제작자에게 보상되며, 교과용보상금은 교과용 도서에 공표된
저작물의 권리자, 도서관보상금은 도서관 내에서 자료출력, 전송된 저작물의 권리자 등에게 보상됩니다.<br />
현재 찾아가지 않은 방송보상금은 한국음악실연자연합회와 한국음반산업협회에서, 교과용 보상금과 도서관 보상금은 
한국복사전송권협회에서 보관 중입니다.<br />
보상금 분배 공고를 한 날부터 3년이 경과한 미분배 보상금은 저작권법 규정에 따라 공익목적을 위하여 사용할 수 있습니다. <br />

따라서 되도록 빨리 자신에게 주어진 보상금을 모두 신청해야만 합니다.</p>
									</li>
								</ul>
							</li>
						</ul>
					</div>	
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>