<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(6);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_6.gif" alt="이용안내" title="이용안내" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/main/main.do?method=info01"><img src="/images/2011/content/sub_lnb0601_off.gif" title="저작권찾기 의미" alt="저작권찾기 의미" /></a></li>
					<li id="lnb2"><a href="/main/main.do?method=info02"><img src="/images/2011/content/sub_lnb0602_off.gif" title="권리자 저작권찾기 신청" alt="권리자 저작권찾기 신청" /></a></li>
					<li id="lnb3"><a href="/main/main.do?method=info03"><img src="/images/2011/content/sub_lnb0603_off.gif" title="이용자 저작권찾기 신청" alt="이용자 저작권찾기 신청" /></a></li>
					<li id="lnb4"><a href="/main/main.do?method=info04"><img src="/images/2011/content/sub_lnb0604_off.gif" title="보상금 신청" alt="보상금 신청" /></a></li>
					<li id="lnb5"><a href="/main/main.do?method=info05"><img src="/images/2011/content/sub_lnb0605_off.gif" title="권리자미확인저작물 이용" alt="권리자미확인저작물 이용" /></a></li>
                    <li id="lnb6"><a href="/main/main.do?method=goContactUs"><img src="/images/2011/content/sub_lnb0606_off.gif" title="이용문의" alt="이용문의" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb6");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>이용안내</span><em>이용문의</em></p>
					<h1><img src="/images/2011/title/content_h1_0606.gif" alt="이용문의" title="이용문의" /></h1>
					
					<div class="section">
						<h2>보상금관리 신탁단체</h2>
                    <!-- 테이블 리스트 Set -->
					  <div class="section mt20">
                      
                      <h3 class="fl">한국음악실연자연헙회</h3><p class="floatDiv"><span class="fr">홈페이지:www.fkmp.kr</span></p>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국음악실연자연헙회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="40%">
								<col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">보상금 신청관리</td>
										<td class="ce">권기태 팀장</td>
										<td class="ce">02-745-8286(6220)</td>
										<td class="ce">kjery@fkmp.kr</td>
									</tr>
									<tr>
										<td class="ce">저작권찾기 신청관리</td>
										<td class="ce">민부영 대리</td>
										<td class="ce">02-745-8286(6112)</td>
										<td class="ce">prettyowl@fkmp.kr</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">

							<h3 class="fl">한국음악제작협회</h3><p class="floatDiv"><span class="fr">홈페이지:www.kapp.or.kr</span></p>
                            <!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국음악제작협회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="40%">
								<col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">보상금 신청관리</td>
										<td class="ce">최소정</td>
										<td class="ce">02-3270-5933</td>
										<td class="ce">csj@kapp.or.kr</td>
									</tr>
									<tr>
										<td class="ce">저작권찾기 신청관리</td>
										<td class="ce">최소정</td>
										<td class="ce">02-3270-5933</td>
										<td class="ce">csj@kapp.or.kr</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">

							<h3 class="fl">한국복사전송권협회</h3><p class="floatDiv"><span class="fr">홈페이지:www.krtra.or.kr</span></p>
                            <!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국복사전송권협회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col width="20%">
                                <col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th colspan="2" scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="2" class="ce">보상금 신청관리</td>
										<td class="ce">교과용 보상금</td>
										<td class="ce">김나영</td>
										<td class="ce">02-2608-2036(대표)</td>
										<td class="ce">nayoung@krtra.or.kr</td>
									</tr>
									<tr>
										<td class="ce">도서관 보상금</td>
										<td class="ce">황경환</td>
										<td class="ce">02-2608-2036(대표)</td>
										<td class="ce">hkh@kapp.or.kr</td>
									</tr>
                                    <tr>
										<td colspan="2" align="center" class="ce">저작권찾기 신청관리</td>
										<td class="ce">김경채 대리</td>
										<td class="ce">02-2608-2036(대표)</td>
										<td class="ce">abcchae@kapp.or.kr</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							
						</div>
						<!-- //테이블 리스트 Set -->
					</div>
					
					<br/><br/>
					
					<div class="section">
						<h2>신탁단체</h2>
                    <!-- 테이블 리스트 Set -->
					  <div class="section mt20">
                      
                      <h3 class="fl">한국음악저작권협회</h3><p class="floatDiv"><span class="fr">홈페이지:www.komca.or.kr</span></p>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국음악저작권협회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="40%">
								<col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">저작권찾기 신청관리</td>
										<td class="ce">황혜림</td>
										<td class="ce">02-2660-0584</td>
										<td class="ce">tnlover07@hotmail.com</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">

							<h3 class="fl">한국문예학술저작권협회</h3><p class="floatDiv"><span class="fr">홈페이지:www.copyrightkorea.or.kr</span></p>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국문예학술저작권협회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="40%">
								<col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">저작권찾기 신청관리</td>
										<td class="ce">김은정</td>
										<td class="ce">02-508-0440(대표)</td>
										<td class="ce">kej0512@copyrightkorea.org</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">
							
							<h3 class="fl">한국영화배급협회</h3><p class="floatDiv"><span class="fr">홈페이지:www.mdak.or.kr</span></p>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국영화배급협회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="40%">
								<col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">저작권찾기 신청관리</td>
										<td class="ce">김의수 팀장</td>
										<td class="ce">02-3452-1008(대표)</td>
										<td class="ce">master@mdak.or.kr</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">
                            
                            <h3 class="fl">한국영화제작가협회</h3><p class="floatDiv"><span class="fr">홈페이지:www.kfpa.net</span></p>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국영화제작가협회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="40%">
								<col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">저작권찾기 신청관리</td>
										<td class="ce">정재훈</td>
										<td class="ce">02-2267-9983(대표)</td>
										<td class="ce">kfpa2@kfpa.net</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">
                            
                            <h3 class="fl">한국방송작가협회</h3><p class="floatDiv"><span class="fr">홈페이지:www.ktrwa.or.kr</span></p>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국방송작가협회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="40%">
								<col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">저작권찾기 신청관리</td>
										<td class="ce">윤선미</td>
										<td class="ce">02-782-1696(대표)</td>
										<td class="ce">office@ktrwa.or.kr</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">
                            
                            <h3 class="fl">한국시나리오작가협회</h3><p class="floatDiv"><span class="fr">홈페이지:www.scenario.or.kr</span></p>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국시나리오작가협회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="40%">
								<col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">저작권찾기 신청관리</td>
										<td class="ce">정지영 총무</td>
										<td class="ce">02-2275-0566(대표)</td>
										<td class="ce">scenario11@hanmail.net</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">
                            
                            <h3 class="fl">한국방송실연자협회</h3><p class="floatDiv"><span class="fr">홈페이지:www.kbpa.kr</span></p>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국방송실연자협회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="40%">
								<col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">저작권찾기 신청관리</td>
										<td class="ce">육세영 선임</td>
										<td class="ce">02-784-7802(대표)</td>
										<td class="ce">kbpa7802@chol.com</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">
                            
                            <h3 class="fl">한국언론진흥재단</h3><p class="floatDiv"><span class="fr">홈페이지:www.kpf.or.kr</span></p>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국방송실연자협회" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="40%">
								<col width="15%">
								<col width="*">
								<col width="25%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">저작권찾기 신청관리</td>
										<td class="ce">미래기술팀</td>
										<td class="ce">02-2001-7782(대표)</td>
										<td class="ce">dnc@kpf.or.kr</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">
                            
						</div>
						<!-- //테이블 리스트 Set -->
                    
                    	
						
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
