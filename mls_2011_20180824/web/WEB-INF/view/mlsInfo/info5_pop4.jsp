<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/popup.css">
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop">
	
		<!-- Header str -->
		<div id="popHeader">
			<h1>보상금 신청 이용 안내 </h1>
		</div>
		<!-- //Header end -->
		
		<!-- Container str -->
		<div id="popContainer">
			<div class="popContent">
				<h2>보상금 신청 접수 </h2>
				
				<p class="mt10"><img src="/images/2010/contents/info05_01_4.gif" alt="" class="border_type" /></p>
			</div>
		</div>
		<!-- //Container end -->
		
		<!-- Footer str -->
		<div id="popFooter">
			<p class="copyright">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		</div>
		<!-- Footer end -->
		
		<p class="close"><a href="#1" onclick="javascript:self.close();"><img src="/images/2010/pop/close.gif" alt="" title="이 창을 닫습니다." /></a></p>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
