<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_6.png" alt="이용안내" title="이용안내" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li class="active"><a href="/main/main.do?method=info01">저작권찾기란?<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info02">저작권찾기의 필요성<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info03">저작권 미확인 저작물<br>이란?<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info04">참여방법<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=goContactUs">이용문의<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>이용안내</span>&gt;<strong>저작권찾기란?</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>저작권찾기란?
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<div class="section">
						<p><img src="/images/2010/contents/infoImg1.gif" alt="Welcome 저작권찾기에 오신 것을 환영합니다." title="Welcome 저작권찾기에 오신 것을 환영합니다." /></p>
						<p class="gray ml30 mt20">우리가 이용하는 저작물 중 상당수는 아직까지 권리관계가 명확하게 밝혀지지 않았습니다. <br />
누가 작사&sdot;작곡&sdot;노래&sdot;연주&sdot;음반 제작을 하였는지, 시&sdot;소설&sdot;수필 등 어문저작물의 권리자는 누구인지, <br />또한 사진&sdot;그림 등의 이미지 권리자는 
누구인지, 그 권리관계가 확실히 밝혀지지 않아 <br />저작물 유통이나 권리자에 대한 보상이 원활히 이루어지지 않고 있습니다. <br /> <br />
							<strong>저작권 찾기</strong> 사이트는 권리자로 하여금 본인이 참여한 저작물의 권리관계를 확인하여 <br />권리자로서 정당한 보상을 받도록 하고, 이용자들에게 
저작권미확인저작물의 이용이 가능토록  해주는 <br />저작물 이용 활성화를 지원하는 웹사이트입니다.<br />
<!--<span class="blue">(저작권정보 조회 -> 저작권찾기 신청(권리자, 이용자) -> 권리관계 확인 -> 저작권정보 (미)수정, 보상금(미)지급)</span>-->
						</p>
						
						<ul class="w80 ml30 mt25 relative">
							<li class="bordBtm m10 relative">
								<dl class="infoProcess ml10 relative mb20">
									<dt class="green">저작물 정보조회</dt>
									<dd class="img"><img src="/images/2010/contents/infoProcess1.gif" alt="" /></dd>
									<dd>음악, 도서, 이미지, 영화, 방송대본에 대한 저작물 및 저작권 정보 확인</dd>
								</dl>
							</li>
							<li class="bordBtm m10 relative">
								<dl class="infoProcess ml10 relative">
									<dt class="blue">신청</dt>
									<dd class="img"><img src="/images/2010/contents/infoProcess2.gif" alt="" /></dd>
									<dd>권리자/이용자의 저작권찾기 신청 <br />미분배된 방송&sdot;교과용&sdot;도서관 보상금 신청</dd>
								</dl>
							</li>
							<li class="bordBtm m10 relative">
								<dl class="infoProcess ml10 relative mb20">
									<dt class="red">권리관계 확인</dt>
									<dd class="img"><img src="/images/2010/contents/infoProcess3.gif" alt="" /></dd>
									<dd>온오프라인으로 제출된 증빙서류를 통한 저작권 확인</dd>
								</dl>
							</li>
							<li class="m10 relative">
								<dl class="infoProcess ml10 relative mb20">
									<dt class="orange">처리완료</dt>
									<dd class="img"><img src="/images/2010/contents/infoProcess4.gif" alt="" /></dd>
									<dd>신청건에 대한 처리완료</dd>
								</dl>
							</li>
						</ul>
					</div>	
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->

</body>
</html>
