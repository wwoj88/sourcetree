<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<title>이용안내 | 저작권찾기</title>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }
 
//법정허락신청
function fn_goSetp1(sDiv) {
	var frm = document.form1;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/myStat/myStat.do?method=statPrps";
		frm.action_div.value = sDiv;
		frm.submit();
	}
}
//-->
</script>	
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(6);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<form name="form1" method="post" action="#">
			<input type="hidden" name="filePath">
			<input type="hidden" name="fileName">
			<input type="hidden" name="realFileName">
			<input type="hidden" name="action_div">
			<input type="submit" style="display:none;">
		</form>
		
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_6.gif" alt="이용안내" title="이용안내" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/main/main.do?method=info01"><img src="/images/2011/content/sub_lnb0601_off.gif" title="저작권찾기 의미" alt="저작권찾기 의미" /></a></li>
					<li id="lnb2"><a href="/main/main.do?method=info02"><img src="/images/2011/content/sub_lnb0602_off.gif" title="권리자 저작권찾기 신청" alt="권리자 저작권찾기 신청" /></a></li>
					<li id="lnb3"><a href="/main/main.do?method=info03"><img src="/images/2011/content/sub_lnb0603_off.gif" title="이용자 저작권찾기 신청" alt="이용자 저작권찾기 신청" /></a></li>
					<li id="lnb4"><a href="/main/main.do?method=info04"><img src="/images/2011/content/sub_lnb0604_off.gif" title="보상금 신청" alt="보상금 신청" /></a></li>
					<li id="lnb5"><a href="/main/main.do?method=info05"><img src="/images/2011/content/sub_lnb0605_off.gif" title="권리자미확인저작물 이용" alt="권리자미확인저작물 이용" /></a></li>
                    <li id="lnb6"><a href="/main/main.do?method=goContactUs"><img src="/images/2011/content/sub_lnb0606_off.gif" title="이용문의" alt="이용문의" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb5");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>이용안내</span><em>권리자미확인저작물 이용</em></p>
					<h1><img src="/images/2011/title/content_h1_0605.gif" alt="권리자미확인저작물 이용" title="권리자미확인저작물 이용" /></h1>
					
					<div class="section">
						<div class="w100">
                    	<img src="/images/2011/content/guideInfo05_subimg01.gif" alt="권리자미확인저작물 이용 이미지" class="fl" />
                        	<div class="fr w70 mt10"><img src="/images/2011/content/guideInfo05_subimg02.gif" alt="저작권찾기 사이트에서 권리자 미확인 저작물이란 저작물의 권리관계가 명확하게 밝혀지지 않는 저작물을 말합니다." />
                            <p class="mt30"><img src="/images/2011/content/guideInfo05_subimg03.gif" alt="권리자 미확인 저작물 범위" /></p>
                            <p class="mt10">1. 저작자 정보가 없는 경우입니다.<br>
2. 저작자 정보는 있지만 저작권자 정보를 확인할 수 없는 경우입니다.<br>
3. 공저자인 경우 위 1, 2 항목을 하나라도 포함하는 경우입니다.</p>
							<p class="mt30"><img src="/images/2011/content/guideInfo05_subimg04.gif" alt="권리자 미확인 저작물을 이용하고자 할 경우 법정허락 제도를 이용 가능합니다." /></p>
                            <p class="mt30"><img src="/images/2011/content/guideInfo05_subimg05.gif" alt="법정허락이란?" /></p>                       
                            <p class="w70 mt10">저작물의 이용자가 상당한 노력을 기울였어도 공표된 저작물의 저작재산권자를 알지 못하거나 저작물의 이용승인을 얻은 후 문화체육관광부장관이 정하는 기준에 의한 보상금을 공탁하고 이를 이용하도록 허락하는 제도를 말한다.</p>
                            <p class="mt30"><img src="/images/2011/content/guideInfo05_subimg06.gif" alt="법정허락 신청방법" /></p> 
                            <p class="mt10">한국저작권위원회 상담 후 저작권찾기 사이트에서 신청 또는 방문 신청<br>
											문의전화 :  02)2660-0042~0043
											<br>
											<a href="#1" onclick="javascript:fn_goSetp1('new');" onclick="fn_goSetp1('new');" onkeypress="fn_goSetp1('new');" title="법정허락 신청화면으로 이동합니다." ><img title="법정허락  신청" alt="법정허락 신청" src="/images/2011/button/btn_app4_05.gif"></a>
							</p>
							
							<p class="floatDiv mt30"><img src="/images/2011/button/btn_app7_01.gif" alt="이용 승인 신청서" /><span class="f1"><a href="#1" onclick="javascript:fn_fileDownLoad('G:/Server/mls/mls_2011/web/upload/form/','이용_승인신청서.hwp','이용_승인신청서.hwp')"><img src="/images/2011/button/btn_app7_02.gif" alt="양식다운로드" /></a></span></p>
                            <p class="floatDiv mt5"><img src="/images/2011/button/btn_app8_01.gif" alt="이용 승인 신청명세서" /><span class="f1"><a href="#1" onclick="javascript:fn_fileDownLoad('G:/Server/mls/mls_2011/web/upload/form/','이용+승인신청명세서.hwp','이용+승인신청명세서.hwp')"><img src="/images/2011/button/btn_app7_02.gif" alt="양식다운로드 " /></a></span></p>

                            </div>
                        </div>					
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>