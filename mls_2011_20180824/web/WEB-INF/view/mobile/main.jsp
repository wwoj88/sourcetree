<%@ page contentType="text/html; charset=euc-kr" pageEncoding="euc-kr"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script type="text/javascript">
window.onload = function() {
	window.scrollTo(0, 0);
}
</script>
<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>
<body id="main">
	<!-- 상단 -->
	<header>
		<h1><img src="/images/2011/m/main_h1.png" alt="저작권찾기" title="저작권찾기"></h1>
	</header>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<ul>
		<li class="m1"><a href="/m/main.do?method=guide">이용안내</a></li>
		<li class="m2"><a href="/m/main.do?method=worksInfo">저작권정보</a></li>
		<li class="m3"><a href="/m/main.do?method=statList">법적허락저작물</a></li>
		<li class="m4"><a href="/m/main.do?method=ftaqList">자주하는 질문</a></li>
		</ul>
	</section>
	<hr>
	<!-- 푸터 -->
	<!--
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
	-->
	<footer>
<div class="foot_lk"><a href="/m/main.do">전체메뉴</a>
<a href="http://www.findcopyright.or.kr/main/main.do" class="pc">PC버젼</a>
<a href="javascript:window.scrollTo(0,1);">맨위로</a></div>COPYRIGHT&copy; <time datetime="2011-09-29">2011</time>
<strong>한국저작권위원회</strong>. All right reserved.
</footer>
</body>
</html>