<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
%>
<html lang="ko">
<head>
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>자주묻는질문 | 저작권찾기</title>
<script type="text/JavaScript">
<!--

function goPage(pageNo){

	var frm = document.form;
	frm.page_no.value = pageNo;
	frm.submit();
}

function boardDetail(bordSeqn,menuSeqn,threaded){

	var frm = document.form;
	frm.bordSeqn.value = bordSeqn;
	frm.menuSeqn.value = menuSeqn;
	frm.threaded.value = threaded;
	frm.page_no.value = '${params.PAGE_NO}';
	
	frm.method = "post";
	frm.action = "/m/board/faq.do?method=view";
	
	frm.submit();
}

//-->
</script>
</head>

<body>
	
	<a href="http://m.findcopyright.or.kr"> === 모바일 저작권찾기 Home === </a>
	<br/><br/>
	<a href="/m/faq.do">자주묻는질문</a>
	
	<br/>
	
	<!-- 테이블 리스트 Set -->
	<div class="section mt20">
		<!-- 검색 -->
		<form name="form" action="#" class="sch mt20">
			<input type="hidden" name="page_no" />
			<input type="hidden" name="menuSeqn" value="${params.menuSeqn}" />
			<input type="hidden" name="bordSeqn" />
			<input type="hidden" name="threaded" />
			<input type="submit" style="display:none;">
		</form>
				   
		<!-- 그리드스타일 -->
		<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
			<colgroup>
			<col width="100%">
			</colgroup>
			
			<tbody>
			<c:if test="${list.totalRow == 0}">
				<tr>
					<td class="ce">등록된 게시물이 없습니다.</td>
				</tr>
			</c:if>
			<c:if test="${list.totalRow > 0}">
				<c:forEach items="${list.resultList}" var="board">
		        <c:set var="NO" value="${board.TOTAL_CNT}"/>
	        	<c:set var="i" value="${i+1}"/>
				<tr>
					<td scope="col"><a href="#1" onclick="javascript:boardDetail('${board.BORD_SEQN}','${board.MENU_SEQN}','${board.THREADED}')">${board.TITE}</a></td>
				</tr>
				</c:forEach>
 					</c:if>
			</tbody>
		</table>
		<!-- //그리드스타일 -->
		
		<!-- 페이징 -->
		 		<%-- 페이징 리스트 --%>
				  <jsp:include page="../../common/PageList_2011.jsp" flush="true">
					  <jsp:param name="totalItemCount" value="${list.totalRow}" />
						<jsp:param name="nowPage"        value="${params.PAGE_NO}" />
						<jsp:param name="functionName"   value="goPage" />
						<jsp:param name="listScale"      value="" />
						<jsp:param name="pageScale"      value="" />
						<jsp:param name="flag"           value="M01_FRONT" />
						<jsp:param name="extend"         value="no" />
					</jsp:include>
		<!-- //페이징 -->
		
	</div>
	<!-- //테이블 리스트 Set -->
						
</body>
