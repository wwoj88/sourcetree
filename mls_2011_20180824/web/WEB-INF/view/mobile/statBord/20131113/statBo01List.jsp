<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>
<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
<script>
// 	function musicDetl(_crId, _nrId, _albumId, _pageNo) {
// 		var frm = document.form;
// 		frm.crId.value = _crId;
// 		frm.nrId.value = _nrId;
// 		frm.albumId.value = _albumId;
// 		frm.pageNo.value = (_pageNo==null)?1:_pageNo;
// 		frm.method = "post";
// 		frm.action = "/m/info/list.do?method=musicDetl";
// 		frm.submit();
// 	}
	function goPage(pageNo) {
		location.href="/m/stat/stat.do?method=statBo01List&pageNo="+pageNo;

	}
	function search() {
		var key = document.getElementById("keyWord").value;
		alert(key);
		location.href = "/m/stat/stat.do?method=statBo01List&keyWord="+key;
	}
</script>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>법정허락</h1>
			<h2>저작권자조회공고</h2>
		</header>
		<form name="form" id="form" action="">
				<input type="hidden" id="pageNo" name="pageNo" value="${pageNo}"/>
				<input type="hidden" name="method" value="statBo01List"/>
				<article class="infoList">
				<b>검색결과 (총 <c:out value="${total}"/>개)</b> 
			<article class="btm_sch_area">
				<section class="btm_sch_box">
					<div><span><font color="#014789">저작물 명</font></span>
						<input type="search" name="keyWord" id="keyWord" title="검색어를 입력하세요" value="">
						<input onclick="search()" type="submit" value="검색"></div>
				</section>
			</article>
			</form>
			<br/>
			<ul class="list2">
			<c:if test="${total < 1}">
				<p>등록된 게시물이 없습니다.</p>
			</c:if>
			<c:if test="${total > 0}">
			<c:forEach var="mobileStat" items="${mobileStat}">
			<li>
				<span class="sh"></span>
				<a href="/m/stat/stat.do?method=statBo01Detl&bordSeqn=${mobileStat.bordSeqn}">&#91;${mobileStat.genreCdName}&#93;<c:out value="${mobileStat.tite}"/><span class="ic ln"></span>
				<span class="descript">
					<em>공고일 : <c:out value="${mobileStat.openDttm}"/></em>
					<em>구분 : <c:out value="${mobileStat.divsCdName}"/></em>
				</span>	
				</a>
			</li>
			</c:forEach>
			</c:if>
			</ul>
		
		<article class="btm_sch_area">
			<span class="sh"></span>
			<div class="pagination">
				<jsp:include page="../../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount"	 value="${total}" />
					<jsp:param name="nowPage"        value="${pageNo}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="5" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
			</div>
		</article>
		
		
	</section>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>