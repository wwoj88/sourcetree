<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>

<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>저작권정보</h1>
			<h2>영화</h2>
		</header>
		
		<article class="list_detl">
			<span class="sh"></span>
			<div>
				<header><h1><c:out value="${movieDetl.title}"/></h1></header>
				<ul class="detl">
				<li><strong>- 영화정보</strong>
					<ul class="news">
					<li><span class="tl">영화제목</span><span class="con"><c:out value="${movieDetl.title}"/></span></li>
   					<li><span class="tl">감독/연출</span><span class="con"><c:out value="${movieDetl.director}"/></span></li>
   					<li><span class="tl">장르</span><span class="con"><c:out value="${movieDetl.genreStr}"/></span></li>
   					<li><span class="tl">형태</span><span class="con"><c:out value="${movieDetl.movieType}"/></span></li>
   					<li><span class="tl">상영시간</span><span class="con"><c:out value="${movieDetl.runTime}"/>분</span></li>
   					<li><span class="tl">등급</span><span class="con"><c:out value="${movieDetl.viewGrade}"/></span></li>
   					<li><span class="tl">출연진 </span><span class="con"><c:out value="${movieDetl.leadingActor}"/></span></li>
   					<li><span class="tl">포스터보기 </span><span class="con"><a href="<c:out value='${movieDetl.postUrs}'/>" target="_blank">&spades; 포스터보러가기</a></span></li>
					</ul>
				</li>
				<li><strong>- 권리정보</strong>
					<ul>
					<li>제작사 : <c:out value='${movieDetl.producer}'/></li>
					<li>투자사 : <c:out value='${movieDetl.investor}'/></li>
					<li>배급사 : <c:out value='${movieDetl.distributor}'/></li>
   					<li>한국문예학술저작권협회 : <c:out value='${movieDetl.trustYnStr}'/></li>
					</ul>
				</li>
				</ul>
			</div>
			<div class="list_btn"><a href="javascript:history.back();">목록</a></div>
		</article>
		
	</section>
	<hr>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>