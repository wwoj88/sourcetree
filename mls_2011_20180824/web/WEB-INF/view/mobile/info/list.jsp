<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>
<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
<script>
	var totalPage = 10;
	function bookDetl(_crId) {
		var frm = document.form;
		frm.crId.value = _crId;
		frm.method = "post";
		frm.action = "/m/info/list.do?method=bookDetl";
		frm.submit();
	}
	function goPage(p) {
		var key = document.getElementById('keyWord').value;
		location.href="/m/info/list.do?method=bookList&pageNo="+p+"&keyWord="+key;
	}
	function search() {
		var key = document.getElementById("keyWord").value;
		location.href = "/m/info/list.do?method=bookList&keyWord="+key;
		
		$.ajax({
			url : "/m/info/list.do",
			data : ({method:'news', keyWord:'', pageNo:''}),
			dataType : 'html',
			success : function(html) {
				$('#contents').html(html);
			}
		});
		
	}
	
	window.onload = function() {
		
	}
</script>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<header>
		<h1><a href="main.html">저작권찾기</a></h1>
		<div><a href="javascript:history.back();"><strong>이전</strong></a></div>
	</header>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>저작권정보 검색</h1>
			<h2>음악</h2>
		</header>
		
		<article class="infoList">
			<header><h1>&bull; 최근등록저작물 Top 100</h1></header>
			<article class="btm_sch_area">
				<span class="sh"></span>
				<section class="btm_sch_box">
					<div>
						<input type="search" id="keyWord" title="검색어를 입력하세요" value="">
						<input onclick="search()" type="submit" value="검색">
					</div>
				</section>
			</article>
			<form name="form" action="">
				<input type="hidden" name="crId" />
			</form>
			<div id="contents"></div>
		</article>
		
		<article class="btm_sch_area">
			<span class="sh"></span>
			<div class="pagination">
				<jsp:include page="../../../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${viewObject.totalCnt}" />
					<jsp:param name="nowPage"        value="${viewObject.nowPage}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="5" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
			</div>
		</article>
		
		
	</section>
	<hr>
	<!-- 푸터 -->
	<footer>
		<div class="foot_lk"><a href="main.html">전체메뉴</a><a href="main/main.html" class="pc">PC버젼</a><a href="#main">맨위로</a></div>
		COPYRIGHT&copy; <time datetime="2011-09-29">2011</time><strong>한국저작권위원회</strong>. All right reserved.
	</footer>
</body>
</html>