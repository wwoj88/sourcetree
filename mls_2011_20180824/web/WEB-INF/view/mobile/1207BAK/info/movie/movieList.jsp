<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>
<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
<script>
	function movieDetl(_crId) {
		var frm = document.form;
		frm.crId.value = _crId;
		frm.method = "post";
		frm.action = "/m/info/list.do?method=movieDetl";
		frm.submit();
	}
	function goPage(p) {
		var key = document.getElementById('keyWord').value;
		location.href="/m/info/list.do?method=movieList&pageNo="+p+"&keyWord="+key;
	}
	function search() {
		var key = document.getElementById("keyWord").value;
		location.href = "/m/info/list.do?method=movieList&keyWord="+key;
	}
</script>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>저작권정보</h1>
			<h2>영화</h2>
		</header>
		
		<article class="infoList">
			<c:choose>
				<c:when test="${viewObject.keyWord!=null && viewObject.keyWord!=''}">
			<header><h1>&bull; 검색결과 (총 <c:out value="${viewObject.totalCnt}"/>개)</h1></header>
				</c:when>
				<c:otherwise>
			<header><h1>&bull; 최근등록저작물 Top 100</h1></header>
				</c:otherwise>	
			</c:choose>
			<article class="btm_sch_area">
				<span class="sh"></span>
				<section class="btm_sch_box">
					<div><span><font color="#014789">저작물 명</font></span>
						<input type="search" id="keyWord" title="검색어를 입력하세요" value="<c:out value='${viewObject.keyWord}'/>">
						<input onclick="search()" type="submit" value="검색"></div>
				</section>
			</article>
			<form name="form" action="">
				<input type="hidden" name="crId" />
			</form>
			<ul class="list2">
			<c:choose>
			<c:when test="${viewObject.totalCnt==0}">
			<li style="text-align: center; height: 50px;">
				<span class="sh"></span>
				<span class="descript"><em class="block">검색 결과가 존재하지 않습니다.</em></span>
			</li>		
			</c:when>
			<c:otherwise>
			<c:forEach var="movie" items="${movieList}" varStatus="status">
				<c:choose>
				    <c:when test="${status.index % 2 == 0}">
				       <li>
				    </c:when>
				    <c:otherwise>
				       <li class="jum">
				     </c:otherwise>
			   </c:choose>
				<span class="sh"></span>
				<a href="javascript:movieDetl('<c:out value="${movie.crId}"/>')">
				<div style="width: 90%"><c:out value="${movie.title}"/></div><span class="ic ln"></span>
				<span class="descript"><em class="block">감독/연출  : <c:out value="${movie.director}"/>/<c:out value="${movie.producer}"/></em>
				<em class="inBlock">출연진 : <c:out value="${movie.leadingActor}"/></em><em class="inBlock">제작년도 : <c:out value="${movie.produceYear}"/>년</em></span>
				</a>
			</li>
			</c:forEach>
			</c:otherwise>
			</c:choose>
			</ul>
		</article>
		
		<article class="btm_sch_area">
			<span class="sh"></span>
			<div class="pagination">
				<jsp:include page="../../../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${viewObject.totalCnt}" />
					<jsp:param name="nowPage"        value="${viewObject.nowPage}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="5" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
			</div>
		</article>
		
		
	</section>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>