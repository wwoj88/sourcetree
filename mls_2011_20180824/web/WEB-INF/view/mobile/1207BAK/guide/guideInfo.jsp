<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<meta name="format-detection" content="telephone=no">
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>
<script type="text/javascript">
function selectItem(id) {
	$('.list_detail').each(function(i,o) {
		if(id==this.id) {
			if($(this).css('display')=='block'){
				$(this).css('display', 'none');	
				var pos=$(this).offset().top;
				$("html, body").animate({scrollTop:pos},'slow');
			} else {
				$(this).css('display', 'block');
				var pos=$(this).position().top;
				$("html, body").animate({scrollTop:pos+46},'slow');
			}
			
		} else {
			$(this).css('display', 'none');
		}
	});
}
</script>
<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>이용안내</h1>
		</header>
		
		<ul class="list">
		<li><a href="javascript:selectItem('list1');">저작권찾기 의미<span class="ic d"></span></a>
			<div class="list_detail" id="list1">
				<article class="detail_view guide">
					<span class="list_ln"></span>
					<header>
						<h1><img src="/images/2011/m/tl1.png" alt="저작권찾기 의미"></h1>
					</header>
					
					<section class="guide_view">
						<p><strong>저작물은</strong> 창작자의 각고의 고통 속에서 오랜 시간과 많은 비용을 들여 탄생합니다. 그리고 이렇게 창작된 저작물은 권리자에겐 창작의 보람과 경제적 보상을, 이용자에겐 사업의 기회와 문화적 풍요로움을 선사합니다.</p>
						<p>그러나 안타깝게도 우리가 매일 이용하는 저작물 중 상당수는 그 권리관계가 명확하지 않은 저작물이 많습니다. 이로 인해, 이용자는 원하는 저작물의 권리자를 확인할 수 없어. 이용의 불편과 상당한 사회적 비용을 지출하고 있습니다. 또한 권리자는 자신의 정당한 권리를 행사하지 못하기도 하며 따라서 당연히 받아야 할 보상금의 혜택도 누리지 못하고 있습니다.</p>
						<p class="mt30"><img src="/images/2011/m/guide_txt1.png" alt="저작권찾기는 모두의 문화와 경제를 풍요롭게 하는 첫걸음입니다."></p>
						<ul>
						<li>이용자는 이용하려고 하는 저작물에 대한 권리정보를 손쉽게 확인할 수 있습니다.</li>
						<li>권리자는 자신의 저작물 권리관계에 대한 정확한 정보를 확인할 수 있습니다.</li>
						<li>또한, 미분배된 보상금을 확인하여 관련 신탁단체에 보상금 분배를 신청할 수 있습니다. </li>
						</ul>
					</section>
				</article>
			</div>
		</li>
		<li><a href="javascript:selectItem('list2');">권리자 저작권찾기 신청 안내<span class="ic d"></span></a>
			<div class="list_detail" id="list2">
				<article class="detail_view guide">
					<span class="list_ln"></span>
					<header>
						<h1><img src="/images/2011/m/tl2.png" alt="권리자 저작권찾기 신청 안내"></h1>
					</header>
					
					<section class="guide_view guide_view2">
						<p class="box"><strong>권리자가 자신이 창작한 저작물의 권리관계정보를 조회한 결과 저작물 정보가 누락되는 등 오류가 있을 경우 신청합니다.</strong></p>
						<p>이때, 저작권신탁단체의 신탁회원 또는 보상금 분배회원이라면 해당 신탁단체에서 신청된 내용을 확인하여 처리하게 됩니다.</p>
						<p>신탁단체의 신탁회원 또는 보상금 분배회원이 아니라면 한국저작권위원회의 저작권등록을 함으로써 처리할 수 있습니다.</p>
					</section>
				</article>
			</div>
		</li>
		<li><a href="javascript:selectItem('list3');">이용자 저작권찾기 신청 안내<span class="ic d"></span></a>
			<div class="list_detail" id="list3">
				<article class="detail_view guide">
					<span class="list_ln"></span>
					<header>
						<h1><img src="/images/2011/m/tl3.png" alt="이용자 저작권찾기 신청 "></h1>
					</header>
					
					<section class="guide_view guide_view2">
						<p class="box mb80"><strong>이용자가 이용하려고 하는 저작물의 권리정보를 조회한 결과, 권리자 미확인 저작물에 해당될 경우 신청합니다.</strong></p>
					</section>
				</article>
			</div>
		</li>
		<li><a href="javascript:selectItem('list4');">보상금 신청 안내<span class="ic d"></span></a>
			<div class="list_detail" id="list4">
				<article class="detail_view guide">
					<span class="list_ln"></span>
					<header>
						<h1><img src="/images/2011/m/tl4.png" alt="보상금 신청 안내"></h1>
					</header>
					
					<section class="guide_view guide_view3">
						<p class="box"><strong>보상금이란? </strong>저작권법에서는 저작물의 이용 촉진 활성화를 위하여 일정 사유에 대해 일일이 저작권자의 허락과 협의 절차를 거치지 않고 이용자가 저작물을 이용할 수 있도록 하고 있습니다.</p>
						<p class="mt30"><strong>보상금제도란? </strong><br>이러한 저작물 사용에 대한 대가를 권리자에게 제공하는 것입니다.<br> 방송보상금은 방송에 사용된 음악 작품의 실연자, 최초 음반 제작자에게 보상되며 교과용보상금은 교과용 도서에 공표된 저작물의 권리자, 도서관보상금은 도서관 내에서 자료출력, 전송된 저작물의 권리자 등에게 보상됩니다.</p>
						<p class="mt30 btm">권리자가 보상금 발생 저작물을 조회한 결과 저작물이 사용되었으나 보상금을 분배받지 못한 경우에 신청합니다.</p>
					</section>
				</article>
			</div>
		</li>
		<li><a href="javascript:selectItem('list5');">권리자미확인저작물 이용 안내<span class="ic d"></span></a>
			<div class="list_detail" id="list5">
				<article class="detail_view guide">
					<span class="list_ln"></span>
					<header>
						<h1><img src="/images/2011/m/tl5.png" alt="권리자미확인저작물 이용 안내"></h1>
					</header>
					
					<section class="guide_view guide_view4">
						<p class="box"><strong>저작권찾기 사이트에서 권리자 미확인 저작물이란</strong> 저작물의 권리관계가 명확하게 밝혀지지 않는 저작물을 말합니다.</p>
						<p class="mt30 mb5"><strong class="font_or">권리자 미확인 저작물 범위</strong></p>
						<ul class="num">
						<li>저작자 정보가 없는 경우입니다.</li>
						<li>저작자 정보는 있지만 저작권자 정보를 확인할 수 없는 경우입니다.</li>
						<li>공저자인 경우 위 1, 2 항목을 하나라도 포함하는 경우입니다.</li>
						</ul>
						<p class="mt30"><strong>권리자 미확인 저작물을 이용하고자 할 경우 법정허락 제도를 이용 가능합니다.</strong></p>
						
						<p class="mt30 mb5"><strong class="font_or">법정허락이란?</strong></p>
						<p class="mt0">저작물의 이용자가 상당한 노력을 기울였어도 공표된 저작물의 저작재산권자를 알지 못하거나 저작물의 이용승인을 얻은 후 문화체육관광부장관이 정하는 기준에 의한 보상금을 공탁하고 이를 이용하도록 허락하는 제도를 말한다.</p>
						
						<p class="mt30 mb5"><strong class="font_or">법정허락 신청방법</strong></p>
						<div class="guide_tel">한국저작권위원회 상담 후 내방 신청 및 저작권찾기 사이트를 통한 신청<strong>문의전화<br>02) 2660-0042~0043</strong></div>
					</section>
				</article>
			</div>
		</li>
		<li><a href="javascript:selectItem('list6');">이용문의<span class="ic d"></span></a>
			<div class="list_detail" id="list6">
				<article class="detail_view guide">
					<span class="list_ln"></span>
					<header>
						<h1><img src="/images/2011/m/tl6.png" alt="이용문의"></h1>
					</header>
					
					<section class="guide_view guide_view4">
						<p class="mt30 mb5"><strong class="font_or">한국저작권위원회</strong></p>
						<div class="guide_tel">유통인증팀<strong>강호기PM<br>02) 2669-0054<br>kanghogi@copyright.or.kr</strong></div>
					</section>
				</article>
			</div>
		</li>
		</ul>
	</section>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>