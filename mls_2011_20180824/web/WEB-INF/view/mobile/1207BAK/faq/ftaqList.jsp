<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<meta name="format-detection" content="telephone=no">
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>
<script type="text/javascript">
function selectItem(id) {
	$('.list_detail').each(function(i,o) {
		if(id==this.id) {
			if($(this).css('display')=='block'){
				$(this).css('display', 'none');	
				var pos=$(this).position().top;
				$("html, body").animate({scrollTop:pos},'slow');
			} else {
				$(this).css('display', 'block');
				var i = id.replace('st','');
				var pos=$(this).parent().offset().top;
				$("html, body").animate({scrollTop:pos},'slow');
			}
			
		} else {
			$(this).css('display', 'none');
		}
	});
}
function goPage(p) {
	location.href="/m/main.do?method=ftaqList&pageNo="+p;
}
</script>
<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>자주묻는 질문</h1>
		</header>
		<form name="form" action="">
			<input type="hidden" name="bordSeqn"/>
			<input type="hidden" name="threaded"/>
		</form>
		<ul class="list">
		<c:choose>
			<c:when test="${viewObject.totalCnt==0}">
			<li>
				<span class="sh"></span>
				<span class="descript"><em class="block">데이터가 존재하지 않습니다.</em></span>
			</li>			
			</c:when>
			<c:otherwise>
			<c:forEach var="faq" items="${faqList}" varStatus="status">
			<li><a href="javascript:selectItem('list${status.index}');">
				<em class="faq_q">Q</em><span class="faq_ques"><c:out value="${faq.tite}"/></span><span class="ic d"></span></a>
				<div class="list_detail" id="list${status.index}">
					<span class="sh"></span>
					<div class="detail_view">
						<em class="faq_a">A</em><span class="faq_ans">
						<c:out value="${faq.bordDesc}" escapeXml="false"/>
						</span>
					</div>
				</div>
			</li>
			</c:forEach>
			</c:otherwise>
			</c:choose>
		</ul>
		<article class="btm_sch_area">
			<span class="sh"></span>
			<div class="pagination">
				<jsp:include page="../../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${viewObject.totalCnt}" />
					<jsp:param name="nowPage"        value="${viewObject.nowPage}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="5" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
			</div>
		</article>
	</section>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>
