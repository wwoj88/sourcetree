<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>
<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
<script>
	function statView(seq) {
		var f = document.frm;
		f.bordSeqn.value = seq;
		f.method = "post";
		f.action = "/m/main.do?method=statDetl";
		f.submit();
	}
	function goPage(p) {
		var key = document.getElementById('keyWord').value;
		location.href="/m/main.do?method=statList&pageNo="+p+"&keyWord="+key;
	}
	function search() {
		var key = document.getElementById("keyWord").value;
		location.href = "/m/main.do?method=statList&keyWord="+key;
	}
</script>
</head>
<body id="m">
<form name="frm" action="">
	<input type="hidden" name="bordSeqn">
</form>
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>법정허락저작물</h1>
		</header>
		
		<article class="infoList">
			<header><h1></h1></header>
			<article class="btm_sch_area">
				<span class="sh"></span>
				<section class="btm_sch_box">
					<div>
						<input type="search" id="keyWord" title="검색어를 입력하세요" value="<c:out value='${viewObject.keyWord}'/>">
						<input onclick="search()" type="submit" value="검색">
					</div>
				</section>
			</article>
			<ul class="list2">
			<c:choose>
				<c:when test="${viewObject.totalCnt==0}">
				<li style="text-align: center; height: 50px;">
				<span class="sh"></span>
					<span class="descript"><em class="block">검색 결과가 존재하지 않습니다.</em></span>
				</li>				
				</c:when>
			<c:otherwise>
			<c:forEach var="rec" items="${statList}" varStatus="status">
				<c:choose>
				    <c:when test="${status.index % 2 == 0}">
				       <li>
				    </c:when>
				    <c:otherwise>
				       <li class="jum">
				     </c:otherwise>
			   </c:choose>
				<span class="sh"></span>
				<a href="<c:if test="${rec.gubun!='승인' }">javascript:statView('${rec.bordSeqn}');</c:if><c:if test="${rec.gubun!='공고' }">javascript:alert('승인된 항목은 상세내용을 확인 할 수 없습니다.');</c:if>">
					<strong class="f_orange">[<c:out value="${rec.gubun}"/>]</strong><c:out value="${rec.tite}"/>
					<c:if test="${rec.licensorName!=null }">
					<em class="stat"><c:out value="${rec.licensorName}"/></em>
					</c:if>
				<span class="ic ln"></span>
				<span class="descript"><em class="block">장르 : <c:out value="${rec.genre}"/></em><em class="inBlock"><c:out value="${rec.gubun}"/>일자 : <c:out value="${rec.apprDttm}"/></em></span>
				</a>
			</li>
			</c:forEach>
			</c:otherwise>
			</c:choose>
			</ul>
		</article>
		
		<article class="btm_sch_area">
			<span class="sh"></span>
			<div class="pagination">
				<jsp:include page="../../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${viewObject.totalCnt}" />
					<jsp:param name="nowPage"        value="${viewObject.nowPage}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="5" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
			</div>
		</article>
	</section>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>