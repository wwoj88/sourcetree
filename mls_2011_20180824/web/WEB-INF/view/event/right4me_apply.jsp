<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"%>
<%@ page contentType="text/html;charset=euc-kr"%>
<%@ page import = "kr.or.copyright.mls.support.util.DateUtil" %>

<%

	// 20110325 이후 접속은 당첨자 화면으로 이동한다.
	//if( Integer.parseInt(DateUtil.getCurrDateNumber()) > 20110320)	
	if( Integer.parseInt(DateUtil.getCurrDateNumber()) > 20110324)	
	{
		out.println("<script>");
		out.println("alert('이벤트가 종료되었습니다. 당첨자 확인화면으로 이동합니다.');");
		out.println("top.location='/event/renewal.do?method=prize'");
		out.println("</script>");
	}
%>

<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<title>▒저작권찾기-미분배 보상금을 찾아라!!▒</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link href="/css/2010/event.css" rel="stylesheet" type="text/css">
<script type="text/JavaScript" language="javascript"
	src="/js/2010/general.js"></script>
<script type="text/JavaScript">
<!--
	function img(){
	 
	 var fn = document.eventForm;
	 	
	 if(fn.item_rslt_07[0].checked==true){
	  document.all.aRow.style.display="";
	  document.all.bRow.style.display="none";
	  document.all.cRow.style.display="none";
	  document.all.dRow.style.display="none";  
	 }
	 if(fn.item_rslt_07[1].checked==true){
	  document.all.aRow.style.display="none";
	  document.all.bRow.style.display="";
	  document.all.cRow.style.display="none";
	  document.all.dRow.style.display="none";  
	 }
	 if(fn.item_rslt_07[2].checked==true){
	  document.all.aRow.style.display="none";
	  document.all.bRow.style.display="none";
	  document.all.cRow.style.display="";
	  document.all.dRow.style.display="none";  
	 }
	 if(fn.item_rslt_07[3].checked==true){
	  document.all.aRow.style.display="none";
	  document.all.bRow.style.display="none";
	  document.all.cRow.style.display="none";
	  document.all.dRow.style.display="";  
	 }
	}
	
	// 참가자 정보 중복확인
	function checkCampPart() {
	
		var frm = document.form1;		
	
		// validation Check
		if(checkForm(frm)) {
			
			frm.camp_part_telx.value = frm.camp_part_telx_01.value
			                                     +'-'+ frm.camp_part_telx_02.value
			                                     +'-'+ frm.camp_part_telx_03.value;
			                                     
			// 동의여부 Check
			if( !document.getElementById("chkAgree").checked){
				alert('개인정보동의 을(를) 입력하세요.');
				document.getElementById("chkAgree").focus();
				return;
			}
			
			frm.target  = "isRegiFrame";
	        frm.action = "/event/renewal.do?method=checkCampPart";
	        
	        frm.submit();
	     
	     }
	}
	
	// 설문조사 등록
	function goEvent() {
	
		var frm = document.eventForm;
		var addrFrm = document.form1;
		
		// 참가자 정보 중복확인여부
		/*
		if( frm.checkCampPart.value == '') {
			alert('먼저 설문 참가여부확인을 해주세요,');
			// 버튼 focus 필요
			
			return;
			
		}else 
		*/
		
		if( frm.checkCampPart.value == 'N') {
			alert('설문 참여는 1회만 가능합니다. 이미 참여한 정보입니다.');
			return;
			
		}else{

			// 참가자 정보
			frm.camp_part_name.value = addrFrm.camp_part_name.value;
			frm.camp_part_telx.value = addrFrm.camp_part_telx_01.value
			                                     +'-'+ addrFrm.camp_part_telx_02.value
			                                     +'-'+ addrFrm.camp_part_telx_03.value;
			
			frm.camp_part_zipx_code.value = addrFrm.zipxNumb.value;
			frm.camp_part_addr.value = addrFrm.addr.value;
			frm.camp_part_addr_detl.value = addrFrm.detlAddr.value;
			
			// 객관식 validation Check
			if(isNullfields(frm.item_rslt_01, '설문 1' )) return;
			if(isNullfields(frm.item_rslt_02, '설문 2' )) return;
			if(isNullfields(frm.item_rslt_03, '설문 3' )) return;
			if(isNullfields(frm.item_rslt_04, '설문 4' )) return;
			if(isNullfields(frm.item_rslt_05, '설문 5' )) return;
			if(isNullfields(frm.item_rslt_06, '설문 6' )) return;
			if(isNullfields(frm.item_rslt_07, '설문 7' )) return;
			if(radioVal(frm.item_rslt_07) == '1') if(isNullfields(frm.item_rslt_08_01, '설문 8-1' )) return;
			if(radioVal(frm.item_rslt_07) == '2') if(isNullfields(frm.item_rslt_08_02, '설문 8-2' )) return;
			if(radioVal(frm.item_rslt_07) == '3') if(isNullfields(frm.item_rslt_08_03, '설문 8-3' )) return;
			if(radioVal(frm.item_rslt_07) == '4') if(isNullfields(frm.item_rslt_08_04, '설문 8-4' )) return;
			
			// 주관식 validation Check
			if( checkForm(frm) ) {
			
				// 주관식 2nd Check
				if(frm.item_rslt_09.value == '글을 작성해 주세요.') {
					alert('설문 9을(를) 입력하세요.');
					frm.item_rslt_09.focus();
					return;
				}
				
				if(frm.item_rslt_10.value == '글을 작성해 주세요.') {
					alert('설문 10을(를) 입력하세요.');
					frm.item_rslt_10.focus();
					return;
				}
				
				// 첨부파일 Check
				if(frm.setFileSize.value > 5) {
					alert('첨부파일은 5MB 을 초과할 수 없습니다.\n첨부파일을 다시 등록해주세요.');
				
					frm.camp_file.focus();
					return;
				}
			    
				document.getElementsByName('item_rslt_arr')[0].value = radioVal(frm.item_rslt_01);
				document.getElementsByName('item_rslt_arr')[1].value = radioVal(frm.item_rslt_02);
				document.getElementsByName('item_rslt_arr')[2].value = radioVal(frm.item_rslt_03);
				document.getElementsByName('item_rslt_arr')[3].value = radioVal(frm.item_rslt_04);
				document.getElementsByName('item_rslt_arr')[4].value = radioVal(frm.item_rslt_05);
				
				document.getElementsByName('item_rslt_arr')[5].value = radioVal(frm.item_rslt_06);
				document.getElementsByName('item_rslt_arr')[6].value = radioVal(frm.item_rslt_07);
				
				if(radioVal(frm.item_rslt_07) == '1') {
					 document.getElementsByName('item_rslt_arr')[7].value = radioVal(frm.item_rslt_08_01);
					 document.getElementsByName('item_rslt_etc_arr')[8].value = ''; document.getElementsByName('item_rslt_etc_arr')[9].value = ''; document.getElementsByName('item_rslt_etc_arr')[10].value = '';
				}
				if(radioVal(frm.item_rslt_07) == '2') {
					document.getElementsByName('item_rslt_arr')[8].value = radioVal(frm.item_rslt_08_02);
					document.getElementsByName('item_rslt_etc_arr')[7].value = ''; document.getElementsByName('item_rslt_etc_arr')[9].value = ''; document.getElementsByName('item_rslt_etc_arr')[10].value = '';
				}
				if(radioVal(frm.item_rslt_07) == '3') {
					document.getElementsByName('item_rslt_arr')[9].value = radioVal(frm.item_rslt_08_03);
					document.getElementsByName('item_rslt_etc_arr')[7].value = ''; document.getElementsByName('item_rslt_etc_arr')[8].value = ''; document.getElementsByName('item_rslt_etc_arr')[10].value = '';
				}
				if(radioVal(frm.item_rslt_07) == '4') {
					document.getElementsByName('item_rslt_arr')[10].value = radioVal(frm.item_rslt_08_04);
					document.getElementsByName('item_rslt_etc_arr')[7].value = ''; document.getElementsByName('item_rslt_etc_arr')[8].value = ''; document.getElementsByName('item_rslt_etc_arr')[9].value = '';
				}
				
				document.getElementsByName('item_rslt_arr')[11].value = frm.item_rslt_09.value;
				document.getElementsByName('item_rslt_arr')[12].value = frm.item_rslt_10.value;
				
				frm.target  = "isRegiFrame";
		        frm.action = "/event/renewal.do?method=submitEvent";
		        
		        frm.submit();
		    }
	    }
        
	}
	
	// 파일업로드 사이즈 체크
	function checkFileSize() {
	
		var frm = document.eventForm;
		
		frm.target  = "isRegiFrame";
        frm.action = "/event/renewal.do?method=getfileSize";
        
        frm.submit();
	}
	
	// 처리 결과 값에 따른 처리.
	// -400 : 참가정보 중복
	// -100 : 파일 사이즈 오류
	// -200 : 참가자 정보 입력오류
	// -300 : 설문등록 정보 입력오류
	//  300 : 성공 
	function isSucc(flag) {
	
		//alert(flag);
		
		var frm = document.eventForm;
		
		if( flag == '-400') {
			alert('설문 참여는 1회만 가능합니다. 이미 참여한 정보입니다.');
			frm.checkCampPart.value='N';
			return;
		}else if( flag == '400') {
			//alert('설문 참여가 가능합니다.');
			
			frm.checkCampPart.value='Y';
			
			// 등록함수 호출;
			goEvent();
			
			//return;
		}
		
		else if( flag == '-100') {
			alert('첨부파일은 5MB 을 초과할 수 없습니다.');
			return;
		}
		
		else if( flag == '300' ) {
			alert('설문이 등록이 완료되었습니다. \n당첨여부는 입력하신 연락처로 개별통지됩니다.');
			
			//window.open("http://new.right4me.or.kr",'_blank');
				
			// (팝업인경우) 자기창 종료 proc 필요. 
			//window.close();
			frm.target  = "";
			frm.action = "/event/renewal.do?method=prize";
			frm.submit();
		}
		
		else {
			alert('설문 등록실패하였습니다.');
			return;
		}
	
	}
	
	// 기타항목 선택 시 널체크
	function checkEtc(checkVal, id) {
	
		var fieldProp = '';
		
		if( id == 'etc_02')
			fieldProp='name="item_rslt_etc_arr" id="item_rslt_02_etc" class="field" title="설문 2(기타항목)"  size="32" maxlength="50"';
		else if( id == 'etc_08_01')
			fieldProp='name="item_rslt_etc_arr" id="item_rslt_08_etc" class="field" title="설문 8-1(기타항목)" size="40" maxlength="50"';
		else if( id == 'etc_08_02')
			fieldProp='name="item_rslt_etc_arr" id="item_rslt_08_etc" class="field" title="설문 8-2(기타항목)" size="40" maxlength="50"';
		else if( id == 'etc_08_03')
			fieldProp='name="item_rslt_etc_arr" id="item_rslt_08_etc" class="field" title="설문 8-3(기타항목)" size="40" maxlength="50"';
		else if( id == 'etc_08_04')
			fieldProp='name="item_rslt_etc_arr" id="item_rslt_08_etc" class="field" title="설문 8-4(기타항목)" size="40" maxlength="50"';
			
		if(checkVal) {
			//document.getElementById('etc').innerHTML = '<input type="text" name="item_rslt_etc_arr" id="item_rslt_02_etc" class="field"  title="설문 2(기타항목)" nullCheck size="32" maxlength="50">';
			document.getElementById(id).innerHTML = '<input type="text"'+ fieldProp+' nullCheck />';
		} else {
			//document.getElementById(id).innerHTML = '<input type="text" name="item_rslt_etc_arr" id="item_rslt_02_etc" class="field"  title="설문 2(기타항목)" size="32" maxlength="50">';
			document.getElementById(id).innerHTML = '<input type="text"'+ fieldProp+' />';
		}
	}
	
	// 기타 외 항목 선택 시 널체크
	function unCheckEtc(checkVal, id) {
		
		var fieldProp = '';
		
		if( id == 'etc_02')
			fieldProp='name="item_rslt_etc_arr" id="item_rslt_02_etc" class="field" title="설문 2(기타항목)"  size="32" maxlength="50"';
		else if( id == 'etc_08_01')
			fieldProp='name="item_rslt_etc_arr" id="item_rslt_08_etc" class="field" title="설문 8-1(기타항목)" size="40" maxlength="50"';
		else if( id == 'etc_08_02')
			fieldProp='name="item_rslt_etc_arr" id="item_rslt_08_etc" class="field" title="설문 8-2(기타항목)" size="40" maxlength="50"';
		else if( id == 'etc_08_03')
			fieldProp='name="item_rslt_etc_arr" id="item_rslt_08_etc" class="field" title="설문 8-3(기타항목)" size="40" maxlength="50"';
		else if( id == 'etc_08_04')
			fieldProp='name="item_rslt_etc_arr" id="item_rslt_08_etc" class="field" title="설문 8-4(기타항목)" size="40" maxlength="50"';
			
		if(checkVal) {
			document.getElementById(id).innerHTML = '<input type="text"'+ fieldProp+' />';
		}
	
	}
	
	// setting file_size
	function setfileSize(size) {
		
		var mSize = new Number(size/(1024*1024));
		//      mSize = mSize.toFixed(2);
		
		document.eventForm.setFileSize.value = mSize.toFixed(2);
		
		document.getElementById('fileSize').innerHTML = mSize.toFixed(2);
		
		if(mSize>5) {
			alert('첨부파일은 5MB 을 초과할 수 없습니다.\n첨부파일을 다시 등록해주세요.');
			return;
		}
		
	}
	
	// radio 선택 값
	function radioVal(fields) {
		
		if(fields == 'undefined')
			return '';
			
		for( var k=0; k<fields.length; k++) {
			if(fields[k].checked) {
			
				if(fields[k].value == 'undefined')
					return '';
			
				return fields[k].value;
				break;
			}
			
		}
	}
	
	// radio 해당필드의 null 체크
	function isNullfields(fields, mesg)
	{
		var result = true;
		var kk = 0;
		
		
		for(i=0; i<fields.length; i++)
		{
			if(fields[i].checked)
			{
				result = false;
				return result;
				//break;
			}
		}
		
		if(result)
		{
			alert(mesg + "을(를) 선택하세요.");
			fields[0].focus();
		}
		
		return result;
	}
	
	// 숫자와 - 만 입력
	function chkNum(){
		
		//alert(event.keyCode);
		
		if(      ((event.keyCode<48) || (event.keyCode>57)) 
		    && ((event.keyCode<95) || (event.keyCode>106)) 
		    && event.keyCode !=8
		    && event.keyCode !=9){
			event.returnValue=false;
		}
	}
	
	// 입력 글자길이 체크
	function onkeylengthMax(formobj, maxlength, objname) {  
	
	    var li_byte     = 0;  
	    var li_len      = 0;  
	    
	    for(var i=0; i< formobj.value.length; i++){  
	        if (escape(formobj.value.charAt(i)).length > 4){  
	            li_byte += 3;  
	        } else {  
	            li_byte++;  
	        }  
	        if(li_byte <= maxlength) {  
	            li_len = i + 1;  
	        }  
	        
	        objname.innerHTML=li_byte;
	    }      
	    if(li_byte > maxlength){  
	       alert('최대 글자 입력수를 초과 하였습니다.');  
	        formobj.value = formobj.value.substr(0, li_len);  
	    }  
	    formobj.focus();  
	}

	// 우편번호 팝업
	function goPostNumbSrch() {
	  //window.open('/user/user.do?method=goPostNumbSrch','zipcode','toolbar=0, status=0, scrollbars=no, location=0, menubar=0, width=950, height=860');
		var pop = window.open("/jusoPopup.jsp?method=goPostNumbSrch","pop","width=570,height=420, scrollbars=yes, resizable=yes");
	}
	
	function jusoCallBack(roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn){
		// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
		document.form1.addr.value = roadAddrPart1;
		document.form1.detlAddr.value = addrDetail;
		document.form1.zipxNumb.value = zipNo;
	}
	
//-->
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- 상단  -->
<table width="100%" height="100%" border="0" cellpadding="0"
	cellspacing="0">
	<tr>
		<td align="center">
		<table width="1000" border="0" cellspacing="0" cellpadding="0">
			<tr>
	          <td><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="1000" height="163">
	            <param name="movie" value="/images/2010/event/top.swf">
	            <param name="quality" value="high">
	            <embed src="/images/2010/event/top.swf" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="1000" height="163"></embed>
	          </object>
	          </td>
			</tr>
			<tr>
				<td>
				<table width="1000" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><img src="/images/2010/event/right4me_event1_02.jpg"
							alt="" width="729" height="70"></td>
						<td><a href="/" target=""><img
							src="/images/2010/event/homepage.jpg" alt="" width="223" height="70"
							border="0"></a></td>
						<td><img src="/images/2010/event/right4me_event1_04.jpg"
							alt="" width="48" height="70"></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td><img src="/images/2010/event/right4me_event1_05.jpg" alt=""
					width="1000" height="46"></td>
			</tr>
			<tr>
				<td>
				<table width="1000" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><img src="/images/2010/event/right4me_event1_06.jpg" alt=""
							width="85" height="59"></td>
						<td><a href="/event/renewal.do"><img
							src="/images/2010/event/btn01.jpg" alt="" width="277" height="59"
							border="0"></a></td>
						<td><img src="/images/2010/event/btn02.jpg" alt="" width="279"
							height="59" border="0"></td>
						<td><a href="/event/renewal.do?method=prize"><img
							src="/images/2010/event/btn03.jpg" alt="" width="277" height="59"
							border="0"></a></td>
						<td><img src="/images/2010/event/right4me_event1_10.jpg" alt=""
							width="82" height="59"></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>

		<!-- 컨텐츠 -->
		<table width="1000" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><img src="/images/2010/event/right4me_event2_txt.jpg" alt=""width="1000" height="257"></td>
			</tr>
			<tr>
				<td>
				<table width="1000" border="0" cellspacing="0" cellpadding="0"
					background="/images/2010/event/cbg.jpg">
					<!-- 설문정보 -->
					<form name="eventForm" method="post" action="#" enctype="multipart/form-data">
						<input type="hidden" name="checkCampPart" value="" />
						<input type="hidden" name="setFileSize" value="" />
						
						<input type="hidden" name="camp_part_name" />
						<input type="hidden" name="camp_part_zipx_code" />
						<input type="hidden" name="camp_part_addr" />
						<input type="hidden" name="camp_part_addr_detl" />
						<input type="hidden" name="camp_part_telx" />
						 
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="hidden" name="item_rslt_arr" />
						<input type="submit" style="display:none;">
					
					<!-- 1번 -->	
					<tr>
						<td align="center">
						<table width="760" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><img src="/images/2010/event/q01_txt.jpg" alt="" width="517"
									height="23"></td>
							</tr>
							<tr>
								<td style="padding:9 0 9 28;">
									<!-- hidden start-->
									<input type="hidden" name="item_seqn_arr" value="1" />	<!-- DB 관리 문제항목 seqn -->
					  				<input type="hidden" name="item_view_seqn_arr" value="1" /> <!-- 문제항목 보여주는 seqn -->
					  				<input type="hidden" name="item_rslt_etc_arr" value="" /> <!-- 기타 -->
									<!-- hidden end-->
									<input type="radio" name="item_rslt_01" id="item_rslt_01_1" value="1" title="설문 1" nullCheck  />
									① 네&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_01" id="item_rslt_01_2" value="2" title="설문 1" nullCheck  />
									② 아니요
								</td>
							</tr>
							<tr>
								<td style="padding:4 0 4 28;"><img
									src="/images/2010/event/q01_stxt.jpg" alt="" width="543" height="14"></td>
							</tr>
							<tr>
								<td><img src="/images/2010/event/right4me_line.jpg" alt=""
									width="740" height="40"></td>
							</tr>
						</table>
						</td>
					</tr>
					
					<!-- 2번 -->
					<tr>
						<td align="center">
						<table width="760" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><img src="/images/2010/event/q02_txt.jpg" alt="" width="624" height="23"></td>
							</tr>
							<tr>
								<td style="padding:9 0 9 28;">
									<!-- hidden start-->
									<input type="hidden" name="item_seqn_arr" value="2" />	<!-- DB 관리 문제항목 seqn -->
					  				<input type="hidden" name="item_view_seqn_arr" value="2" /> <!-- 문제항목 보여주는 seqn -->
									<!-- hidden end-->
									<input type="radio" name="item_rslt_02" id="item_rslt_02_1" value="1" title="설문 2" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_02')"  />
									① 신문광고/리플렛&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_02" id="item_rslt_02_2" value="2" title="설문 2" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_02')" />
									② 인터넷&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_02" id="item_rslt_02_3" value="3" title="설문 2" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_02')" />
									③ 옥외광고(지하철광고 등)&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_02" id="item_rslt_02_4" value="4" title="설문 2"  onclick="javascript:checkEtc(this.checked, 'etc_02')" />
									④ 기타&nbsp;&nbsp; <span id="etc_02"><input type="text" name="item_rslt_etc_arr" id="item_rslt_02_etc" class="field"  title="설문 2(기타항목)"  size="32" maxlength="50"></span>
								</td>
							</tr>
							<tr>
								<td><img src="/images/2010/event/right4me_line.jpg" alt=""
									width="740" height="40"></td>
							</tr>
						</table>
						</td>
					</tr>
					
					<!-- 3번 -->
					<tr>
						<td align="center">
						<table width="760" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><img src="/images/2010/event/q03_txt.jpg" alt=""  width="730" height="39"></td>
							</tr>
							<tr>
								<td style="padding:9 0 9 28; line-height:180%;">
									<!-- hidden start-->
									<input type="hidden" name="item_seqn_arr" value="3" />	<!-- DB 관리 문제항목 seqn -->
					  				<input type="hidden" name="item_view_seqn_arr" value="3" /> <!-- 문제항목 보여주는 seqn -->
					  				<input type="hidden" name="item_rslt_etc_arr" value="" /> <!-- 기타 -->
									<!-- hidden end-->
									<input type="radio" name="item_rslt_03" id="item_rslt_03_1" value="1" title="설문 3" nullCheck  />
									① 이용할 저작물의 창작자(저작자) 정보(작가, 연주자, 가수, 제작자 등) 조회<br>
									<input type="radio" name="item_rslt_03" id="item_rslt_03_2" value="2" title="설문 3" nullCheck />
									② 이용할 저작물에 대한 정보가 없을 때, 저작물권리확인 신청<br>
									<input type="radio" name="item_rslt_03" id="item_rslt_03_3" value="3" title="설문 3" nullCheck />
									③ 내가 창작한 저작물 검색 및 저작물 정보등록 신청<br>
			                        <input type="radio" name="item_rslt_03" id="item_rslt_03_4" value="3" title="설문 3" nullCheck />
			                        ④ 창작자(권리자)로서, 사용된 저작물 이용대가(보상금)를 받을 수 있음&nbsp;&nbsp;&nbsp;
			                        <input type="radio" name="item_rslt_03" id="item_rslt_03_5" value="3" title="설문 3" nullCheck />
			                        ⑤ 모두 가능함
		                        </td>
							</tr>
							<tr>
								<td><img src="/images/2010/event/right4me_line.jpg" alt=""
									width="740" height="40"></td>
							</tr>
						</table>
						</td>
					</tr>
					
					<!-- 4번 -->
					<tr>
						<td align="center">
						<table width="760" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><img src="/images/2010/event/q04_txt.jpg" alt="" width="416" height="23"></td>
							</tr>
							<tr>
								<td style="padding:9 0 9 28;">
									<!-- hidden start-->
									<input type="hidden" name="item_seqn_arr" value="4" />	<!-- DB 관리 문제항목 seqn -->
					  				<input type="hidden" name="item_view_seqn_arr" value="4" /> <!-- 문제항목 보여주는 seqn -->
					  				<input type="hidden" name="item_rslt_etc_arr" value="" /> <!-- 기타 -->
									<!-- hidden end-->
									<input type="radio" name="item_rslt_04" id="item_rslt_04_1" value="1" title="설문 4" nullCheck  />
									① 저작물을 창작한 경험이 있다&nbsp;&nbsp;&nbsp;
                        			<input type="radio" name="item_rslt_04" id="item_rslt_04_2" value="2" title="설문 4" nullCheck /> 
									② 저작물을 이용한 경험이 있다&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_04" id="item_rslt_04_3" value="3" title="설문 4" nullCheck /> 
									③ 둘 다 경험이 있다&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_04" id="item_rslt_04_4" value="4" title="설문 4" nullCheck />
									④ 둘 다 아니다
								</td>
							</tr>
							<tr>
								<td><img src="/images/2010/event/right4me_line.jpg" alt=""
									width="740" height="40"></td>
							</tr>
						</table>
						</td>
					</tr>
					
					<!-- 5번 -->
					<tr>
						<td align="center">
						<table width="760" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><img src="/images/2010/event/q05_txt.jpg" alt="" width="275" height="23"></td>
							</tr>
							<tr> 
		                      <td style="padding:9 0 9 28;"><img src="/images/2010/event/q05_process.jpg" alt="" width="500" height="26"></td>
		                    </tr>
							<tr>
								<td style="padding:9 0 9 28;">
									<!-- hidden start-->
									<input type="hidden" name="item_seqn_arr" value="5" />	<!-- DB 관리 문제항목 seqn -->
					  				<input type="hidden" name="item_view_seqn_arr" value="5" /> <!-- 문제항목 보여주는 seqn -->
					  				<input type="hidden" name="item_rslt_etc_arr" value="" /> <!-- 기타 -->
									<!-- hidden end-->
									<input type="radio" name="item_rslt_05" id="item_rslt_05_1" value="1" title="설문 5" nullCheck  />
									① A → B → C → D&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_05" id="item_rslt_05_2" value="2" title="설문 5" nullCheck />
									② A → D → C→ B&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_05" id="item_rslt_05_3" value="3" title="설문 5" nullCheck />
									③ D → C → B → A&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_05" id="item_rslt_05_4" value="4" title="설문 5" nullCheck />
									④ B → A → C → D
								</td>
							</tr>
							<tr>
								<td><img src="/images/2010/event/q05-1_txt.jpg" alt="" width="740"
									height="13"></td>
							</tr>
							<tr>
								<td><img src="/images/2010/event/right4me_line.jpg" alt=""
									width="740" height="40"></td>
							</tr>
						</table>
						</td>
					</tr>
					
					<!-- 6번 -->
					<tr>
						<td align="center">
						<table width="760" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><img src="/images/2010/event/q06_txt.jpg" alt="" width="624" height="23"></td>
							</tr>
							<tr> 
		                      <td style="padding:9 0 9 28;"><img src="/images/2010/event/q06_process.jpg" alt="" width="489" height="26"></td>
		                    </tr>
							<tr>
								<td style="padding:9 0 9 28;">
									<!-- hidden start-->
									<input type="hidden" name="item_seqn_arr" value="6" />	<!-- DB 관리 문제항목 seqn -->
					  				<input type="hidden" name="item_view_seqn_arr" value="6" /> <!-- 문제항목 보여주는 seqn -->
					  				<input type="hidden" name="item_rslt_etc_arr" value="" /> <!-- 기타 -->
									<!-- hidden end-->
									<input type="radio" name="item_rslt_06" id="item_rslt_06_1" value="1" title="설문 6" nullCheck  />
									① A → C → D → B&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_06" id="item_rslt_06_2" value="2" title="설문 6" nullCheck />
									② A → D → C → B&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_06" id="item_rslt_06_3" value="3" title="설문 6" nullCheck />
									③ D → C → B → A&nbsp;&nbsp;&nbsp;
									<input type="radio" name="item_rslt_06" id="item_rslt_06_4" value="4" title="설문 6" nullCheck />
									④ B → A → C → D
								</td>
							</tr>
							<tr>
								<td style="padding:4 0 4 28;"><img
									src="/images/2010/event/q06_stxt.jpg" alt="" width="513" height="14"></td>
							</tr>
							<tr>
								<td><img src="/images/2010/event/right4me_line.jpg" alt=""
									width="740" height="40"></td>
							</tr>
						</table>
						</td>
					</tr>
					
					<!-- 7번 -->
					<tr> 
	                <td align="center"> 
					<table width="760" border="0" cellspacing="0" cellpadding="0">
	                    <tr> 
	                      <td><img src="/images/2010/event/q07_txt.jpg" alt="" width="730" height="39"></td>
	                    </tr>
	                    <tr> 
	                      <td style="padding:9 0 9 28; line-height:180%;">
	                      	  <!-- hidden start-->
							  <input type="hidden" name="item_seqn_arr" value="7" />	<!-- DB 관리 문제항목 seqn -->
			  				  <input type="hidden" name="item_view_seqn_arr" value="7" /> <!-- 문제항목 보여주는 seqn -->
			  				  <input type="hidden" name="item_rslt_etc_arr" value="" /> <!-- 기타 -->
							  <!-- hidden end-->
	                      	  <input type="radio" name="item_rslt_07" id="item_rslt_07_1" value="1" title="설문 7" nullCheck onClick="javascript:img();" >
	                          ① 저작권 정보 검색 기능(저작권정보 조회 및 보상금발생저작물 조회)<br>
	                          <input type="radio" name="item_rslt_07" id="item_rslt_07_2" value="2" title="설문 7" nullCheck onClick="javascript:img();">
	                          ② 저작권찾기 신청절차/보상금 신청 절차 기능(이용안내)<br>
	                          <input type="radio" name="item_rslt_07" id="item_rslt_07_3" value="3" title="설문 7" nullCheck onClick="javascript:img();">
	                          ③ ①번,②번 사용 후 결과정보를 확인하는 방법(로그인 후 신청현황조회)<br>
	                          <input type="radio" name="item_rslt_07" id="item_rslt_07_4" value="4" title="설문 7" nullCheck onClick="javascript:img();">
	                          ④ 사이트 이용 안내</td>
	                    </tr>
	                    
	                    <!-- 8번 -->
	                    <tr> 
	                      <td><img src="/images/2010/event/right4me_line.jpg" alt="" width="740" height="40">
	                      <!-- hidden start-->
					      <input type="hidden" name="item_seqn_arr" value="8" />	<!-- DB 관리 문제항목 seqn -->
	  				      <input type="hidden" name="item_view_seqn_arr" value="8-1" /> <!-- 문제항목 보여주는 seqn -->
	  				      
	  				      <input type="hidden" name="item_seqn_arr" value="9" />	<!-- DB 관리 문제항목 seqn -->
	  				      <input type="hidden" name="item_view_seqn_arr" value="8-2" /> <!-- 문제항목 보여주는 seqn -->
	  				      
	  				      <input type="hidden" name="item_seqn_arr" value="10" />	<!-- DB 관리 문제항목 seqn -->
	  				      <input type="hidden" name="item_view_seqn_arr" value="8-3" /> <!-- 문제항목 보여주는 seqn -->
	  				      
	  				      <input type="hidden" name="item_seqn_arr" value="11" />	<!-- DB 관리 문제항목 seqn -->
	  				      <input type="hidden" name="item_view_seqn_arr" value="8-4" /> <!-- 문제항목 보여주는 seqn -->
					      <!-- hidden end-->
	                      </td>
	                    </tr>
						
						<tr id="aRow" style="display:none"> 
	                      <td>
	                              <table width="760" border="0" cellspacing="0" cellpadding="0">
	                            <tr> 
	                              <td style="padding:0 0 3 28;"><img src="/images/2010/event/q08_1_txt.jpg" alt="" width="671" height="23"></td>
                          		</tr>
	                          <tr>
	                            <td style="padding:9 0 9 28; line-height:180%;;">
	                            	<input type="radio" name="item_rslt_08_01" id="item_rslt_08_1" value="1" title="설문 8-1" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_08_01')"  />
	                                ① 썸네일이미지, 음원 미리듣기 등을 확대 제공<br>
	                                <input type="radio" name="item_rslt_08_01" id="item_rslt_08_2" value="2" title="설문 8-1" nullCheck" onclick="javascript:unCheckEtc(this.checked, 'etc_08_01')" />
	                                ② ‘가나다’ 텍스트 기준으로 검색하는 방법<br>
	                                <input type="radio" name="item_rslt_08_01" id="item_rslt_08_3" value="3" title="설문 8-1" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_08_01')" />
	                                ③ 결과 내 재검색 방식을 이용하는 방법<br>
	                                <input type="radio" name="item_rslt_08_01" id="item_rslt_08_4" value="4" title="설문 8-1" nullCheck onclick="javascript:checkEtc(this.checked, 'etc_08_01')"/>
	                                ④ 기타&nbsp;&nbsp; 
	                                <span id="etc_08_01"><input name="item_rslt_etc_arr" id="item_rslt_08_etc" type="text" title="설문 8-1(기타항목)" class="field" size="40" maxlength="50"></span>
	                              </td>
	                          </tr>
	                        </table></td>
	                    </tr>
						
						
						<tr id="bRow" style="display:none"> 
	                      <td>
	                            <table width="760" border="0" cellspacing="0" cellpadding="0">
	                            <tr> 
	                              <td style="padding:0 0 3 28;"><img src="/images/2010/event/q08_2_txt.jpg" alt="" width="671" height="23"></td>
                          		</tr>
	                          <tr>
	                            <td style="padding:9 0 9 28; line-height:180%;">
	                            	<input type="radio" name="item_rslt_08_02" id="item_rslt_08_1" value="1" title="설문 8-2" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_08_02')"  />
	                                ① 화면구성을 변경하는 방법<br>
	                                <input type="radio" name="item_rslt_08_02" id="item_rslt_08_2" value="2" title="설문 8-2" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_08_02')" />
	                                ② 음악, 도서 등 장르 구별을 통합하는 방법<br>
	                                <input type="radio" name="item_rslt_08_02" id="item_rslt_08_3" value="3" title="설문 8-2" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_08_02')" />
	                                ③ 각 페이지마다 도움말 기능을 첨가하는 방법<br>
	                                <input type="radio" name="item_rslt_08_02" id="item_rslt_08_4" value="4" title="설문 8-2" nullCheck onclick="javascript:checkEtc(this.checked, 'etc_08_02')"/>
	                                ④ 기타&nbsp;&nbsp; 
	                                <span id="etc_08_02"><input name="item_rslt_etc_arr" type="text" class="field" id="item_rslt_08_etc" title="설문 8-2(기타항목)" size="40" maxlength="50"></span>
	                             </td>
	                          </tr>
	                        </table></td>
	                    </tr>
	
						
						<tr id="cRow" style="display:none"> 
	                      <td>
	                            <table width="760" border="0" cellspacing="0" cellpadding="0">
	                            <tr> 
	                              <td style="padding:0 0 3 28;"><img src="/images/2010/event/q08_3_txt.jpg" alt="" width="671" height="23"></td>
                          		</tr>
	                          <tr>
	                            <td style="padding:9 0 9 28; line-height:180%;">
	                            	<input type="radio" name="item_rslt_08_03" id="item_rslt_08_1" value="1" title="설문 8-3" nullCheck  onclick="javascript:unCheckEtc(this.checked, 'etc_08_03')"  />
	                                ① 문자메세지(SMS)를 통해 확인<br>
	                                <input type="radio" name="item_rslt_08_03" id="item_rslt_08_2" value="2" title="설문 8-3" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_08_03')" />
	                                ② 이메일을 통해 확인<br>
	                                <input type="radio" name="item_rslt_08_03" id="item_rslt_08_3" value="3" title="설문 8-3" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_08_03')" />
	                                ③ 마이페이지를 통해 확인<br>
	                                <input type="radio" name="item_rslt_08_03" id="item_rslt_08" value="4" title="설문 8-3" nullCheck onclick="javascript:checkEtc(this.checked, 'etc_08_03')"/>
	                                ④ 기타&nbsp;&nbsp; 
	                                <span id="etc_08_03"><input type="text" class="field" name="item_rslt_etc_arr" id="item_rslt_08_etc" title="설문 8-3(기타항목)" size="40" maxlength="50"></span>
	                             </td>
	                          </tr>
	                        </table></td>
	                    </tr>
						
						<tr id="dRow" style="display:none"> 
	                      <td>
	                            <table width="760" border="0" cellspacing="0" cellpadding="0">
	                            <tr> 
	                              <td style="padding:0 0 3 28;"><img src="/images/2010/event/q08_4_txt.jpg" alt="" width="671" height="23"></td>
                          </tr>
	                          <tr>
	                            <td style="padding:9 0 9 28; line-height:180%;">
	                            	<input type="radio" name="item_rslt_08_04" id="item_rslt_08_1" value="1" title="설문 8" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_08_04')" />
	                                ① 말풍선을 통해 안내하는 방법<br>
	                                <input type="radio" name="item_rslt_08_04" id="item_rslt_08_2" value="2" title="설문 8" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_08_04')" />
	                                ② 별도의 안내페이지를 운영하는 방법<br>
	                                <input type="radio" name="item_rslt_08_04" id="item_rslt_08_3" value="3" title="설문 8" nullCheck onclick="javascript:unCheckEtc(this.checked, 'etc_08_04')" />
	                                ③ 상담게시판을 통해 운영하는 방법<br>
	                                <input type="radio" name="item_rslt_08_04" id="item_rslt_08_4" value="4" title="설문 8" nullCheck onclick="javascript:checkEtc(this.checked, 'etc_08_04')"/>
	                                ④ 기타&nbsp;&nbsp; 
	                                <span id="etc_08_04"><input type="text" class="field" name="item_rslt_etc_arr" id="item_rslt_08_etc" title="설문 8-4(기타항목)" size="40" maxlength="50"></span>
	                              </td>
	                          </tr>
	                        </table></td>
	                    </tr>
						
						
	                  </table>
	                </td>
	              </tr>
					
					
					<!-- 9번-->
					<tr>
						<td align="center">
						<table width="760" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><img src="/images/2010/event/q09_txt.jpg" alt="" width="656" height="23"></td>
							</tr>
							<tr>
								<td style="padding:9 0 9 28; font-size: 11px;">
									<!-- hidden start-->
									<input type="hidden" name="item_seqn_arr" value="12" />	<!-- DB 관리 문제항목 seqn -->
					  				<input type="hidden" name="item_view_seqn_arr" value="9" /> <!-- 문제항목 보여주는 seqn -->
					  				<input type="hidden" name="item_rslt_etc_arr" value="" /> <!-- 기타 -->
									<!-- hidden end-->
									<textarea name="item_rslt_09" id="item_rslt_09"  title="설문 9" nullCheck rows="3" cols="100"  class="contents"
										style="border:1 solid #cdcdcd; font-size: 11px; line-height: 17px;"
										onClick="if(this.value=='글을 작성해 주세요.'){this.value=''}" onkeyup="onkeylengthMax(this, 600, byte09);">글을 작성해 주세요.</textarea>
									&nbsp;<font color="#FB6236"><strong><span id="byte09">0</span>byte</strong></font>/600byte
								</td>
							</tr>
							<tr>
								<td>
									<img src="/images/2010/event/right4me_line.jpg" alt="" width="740" height="40">
								</td>
							</tr>
						</table>
						</td>
					</tr>
					
					<!-- 10번 -->
					<tr>
						<td align="center">
						<table width="760" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><img src="/images/2010/event/q10_txt.jpg" alt="" width="730" height="39"></td>
							</tr>
							<tr>
								<td style="padding:9 0 9 28; font-size: 11px;">
									<input type="file" name="camp_file" size="60" title="" onchange="checkFileSize(this);"> &nbsp;<font color="#FB6236"><strong><span id="fileSize">0</span>MB</strong></font>/5MB
								</td>
							</tr>
							<tr>
								<td style="padding:3 0 5 28; font-size: 11px;">
									<!-- hidden start-->
									<input type="hidden" name="item_seqn_arr" value="13" />	<!-- DB 관리 문제항목 seqn -->
					  				<input type="hidden" name="item_view_seqn_arr" value="10" /> <!-- 문제항목 보여주는 seqn -->
					  				<input type="hidden" name="item_rslt_etc_arr" value="" /> <!-- 기타 -->
									<!-- hidden end-->
									<textarea name="item_rslt_10" id="item_rslt_10"  title="설문 10" nullCheck rows="3" cols="100"  class="contents"
										style="border:1 solid #cdcdcd; font-size: 11px; line-height: 17px;"
										onClick="if(this.value=='글을 작성해 주세요.'){this.value=''}" onkeyup="onkeylengthMax(this, 600, byte10);">글을 작성해 주세요.</textarea>
								&nbsp;<font color="#FB6236"><strong><span id="byte10">0</span>byte</strong></font>/600byte
								</td>
							</tr>
						</table>
						</td>
					</tr>
					
					</form>
					
					<tr>
						<td align="center"><img
							src="/images/2010/event/apply_top.jpg" alt="" width="1000" height="115"></td>
					</tr>
					<tr>
						<td align="center">
					  
						<table width="1000" border="0" cellspacing="0" cellpadding="0">
							<!-- 응모자 정보 start -->
							<form name="form1" method="post" action="#" enctype="multipart/form-data">
								<input type="submit" style="display:none;">
								<input type="hidden" name="camp_part_telx" />
								<input type="hidden" name="zip" />
								<input type="hidden" name="srchAddr" />
							<tr>
								<td><img src="/images/2010/event/apply_left.jpg" alt=""
									width="208" height="253"></td>
								<td width="666" valign="top"
									background="/images/2010/event/form_bg.jpg">
								<table width="666" border="0" cellspacing="0" cellpadding="0">
									<tr bgcolor="c8c8c8">
										<td height="1" colspan="4"></td>
									</tr>
									<tr>
										<td width="90" bgcolor="e1d9d6" style="padding:6 7 6 7;"><strong><font
											color="515151"><img
											src="/images/2010/event/apply_txt01.png" width="32"
											height="14"></font></strong></td>
										<td width="140" style="padding:6 7 6 7;"><input
											type="text" class="input" size="15" name="camp_part_name"
											id="camp_part_name" title="응모자 이름" rangeSize="~150" nullCheck>
										</td>
										<td width="90" bgcolor="e1d9d6" style="padding:6 7 6 7;"><strong><font
											color="515151"><img
											src="/images/2010/event/apply_txt02.png" width="42"
											height="14"></font></strong></td>
										<td style="padding:6 7 6 7;"><select
											name="camp_part_telx_01" id="camp_part_telx_01" class="field"
											title="응모자 연락처" nullCheck>
											<option value="">선택</option>
											<option value="010">010</option>
											<option value="011">011</option>
											<option value="016">016</option>
											<option value="017">017</option>
											<option value="018">018</option>
											<option value="019">019</option>
											<option value="02">02</option>
											<option value="031">031</option>
											<option value="032">032</option>
											<option value="033">033</option>
											<option value="041">041</option>
											<option value="042">042</option>
											<option value="043">043</option>
											<option value="051">051</option>
											<option value="052">052</option>
											<option value="053">053</option>
											<option value="054">054</option>
											<option value="061">061</option>
											<option value="062">062</option>
											<option value="063">063</option>
											<option value="064">064</option>
											<option value="0502">0502</option>
											<option value="0505">0505</option>
											<option value="0506">0506</option>
										</select> - <input type="text" name="camp_part_telx_02"
											id="camp_part_telx_02" class="field" title="응모자 연락처"
											character="KE" size="4" maxlength="4" nullCheck
											onkeydown="javascript:chkNum();" style='IME-MODE: disabled' />
										- <input type="text" name="camp_part_telx_03"
											id="camp_part_telx_03" class="field" title="응모자 연락처"
											character="KE" size="4" maxlength="4" nullCheck
											onkeydown="javascript:chkNum();" style='IME-MODE: disabled'>
										</td>
									</tr>
									<tr bgcolor="c8c8c8">
										<td height="1" colspan="4"></td>
									</tr>
									<tr>
										<td bgcolor="e1d9d6" style="padding:6 7 6 7;"><strong><font
											color="515151"><img
											src="/images/2010/event/apply_txt03.png" width="32"
											height="14"></font></strong></td>
										<td colspan="3" style="padding:6 7 6 7;"><input
											type="text" name="zipxNumb" id="zipxNumb" class="input"
											size="10" maxlength="8" title="응모자 주소(우편번호)" nullCheck readonly>
											<img src="/images/2010/event/post_btn.png" width="73" height="17" alt=""
											align="absmiddle" onclick="javascript:goPostNumbSrch();" style="cursor:hand"> &nbsp; <input type="text"
											name="addr" name="id" class="input" size="30" title="응모자 주소"
											rangeSize="~50" nullCheck readonly><input type="text"
											name="detlAddr" name="detlAddr" class="input" size="30"
											title="응모자 상세주소" rangeSize="~50" nullCheck></td>
									</tr>
									<tr bgcolor="c8c8c8">
										<td height="1" colspan="4"></td>
									</tr>
									<tr>
										<td bgcolor="e1d9d6" style="padding:6 7 6 7;"><font
											color="515151"><strong><img
											src="/images/2010/event/apply_txt04.png" width="66"
											height="14"></strong></font></td>
										<td colspan="3" style="padding:6 7 6 7;"><textarea
											name="textarea" cols="88" rows="10"
											style="font-family:Dotum; font-size:11px;"
											class="input_multy" readonly>■ 수집하는 개인정보항목
한국저작권위원회는 이름, 연락처 등을 아래와 같이 개인정보를 수집하고 있습니다.
- 수집항목: 이름, 연락처(전화번호), 주소
- 개인정보 수집방법: 프로모션 이벤트 웹 페이지 응모신청

■ 개인정보의 수집 및 이용목적
한국저작권위원회는 수집한 개인정보를 다음의 목적을 위해 활용합니다.
- 기타: 이벤트 데이터를 이용하여, '저작권찾기' 홈페이지 개선 및 사용자의 편의제공 자료로 쓰임

■ 개인정보의 보유 및 이용기간
한국저작권위원회는 개인정보 수집 및 이용목적이 달성한 후에는 예외 없이 해당 정보를 파기합니다.
                            </textarea></td>
									</tr>
									<tr bgcolor="c8c8c8">
										<td height="1" colspan="4"></td>
									</tr>
									<tr align="center">
										<td height="33" colspan="4"><input type="checkbox" name="chkAgree" id="chkAgree" title="개인정보동의" nullCheck
											 name="agree">&nbsp;위 내용에 '동의' 합니다.</td>
									</tr>
								</table>
								</td>
								<td><img src="/images/2010/event/apply_right.jpg" alt=""
									width="126" height="253"></td>
							</tr>
							</form>
							<!-- 응모자 정보 end -->
						</table>
						
						</td>
					</tr>
					<tr>
						<td align="center"><img
							src="/images/2010/event/apply_bottom.jpg" alt="" width="1000"
							height="46"></td>
					</tr>
					<tr>
						<td align="center">
						<table width="1000" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><img src="/images/2010/event/apply_bleft.jpg" alt=""
									width="390" height="70"></td>
								<td><a href="#1" onclick="javascript:checkCampPart();"><img
									src="/images/2010/event/event_btn.jpg" alt="" width="221" height="70"
									border="0"></a></td>
								<td><img src="/images/2010/event/apply_bright.jpg" alt=""
									width="389" height="70"></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td align="center"><img
							src="/images/2010/event/apply_bbottom.jpg" alt="" width="1000"
							height="19"></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>

		<!-- 하단 -->
		<table width="1000" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="10"></td>
			</tr>
			<tr>
				<td><img src="/images/2010/event/copyright.jpg" alt="" width="1000"
					height="65"></td>
			</tr>
		</table>

		</td>
	</tr>
</table>

<!-- 처리 iFrame -->
<iframe id="isRegiFrame" title="isRegiFrame" name="isRegiFrame" width="0" height="0"></iframe>
	
</body>
</html>
