<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%

%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>알림마당(캠페인) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/general.js"></script>		
<script type="text/JavaScript">
<!--
	
	// 참가자 정보 중복확인
	function checkCampPart() {
	
		var frm = document.form1;		
	
		// validation Check
		if(checkForm(frm)) {
			
			frm.target  = "isRegiFrame";
	        frm.action = "/event/renewal.do?method=checkCampPart";
	        
	        frm.submit();
	     
	     }
	}
	
	// 설문조사 등록
	function goEvent() {
	
		var frm = document.eventForm;
		var addrFrm = document.form1;
		
		// 참가자 정보 중복확인여부
		if( frm.checkCampPart.value == '') {
			alert('먼저 설문 참가여부확인을 해주세요,');
			// 버튼 focus 필요
			
			return;
			
		}else if( frm.checkCampPart.value == 'N') {
			alert('설문 참여는 1회만 가능합니다. 이미 참여한 정보입니다.');
			return;
			
		}else{

			// 참가자 정보
			frm.camp_part_name.value = addrFrm.camp_part_name.value;
			frm.camp_part_telx.value = addrFrm.camp_part_telx.value;
			
			frm.camp_part_zipx_code.value = addrFrm.zipxNumb.value;
			frm.camp_part_addr.value = addrFrm.addr.value;
			frm.camp_part_addr_detl.value = addrFrm.detlAddr.value;
			
			// 객관식 validation Check
			if(isNullfields(frm.item_rslt_01, '객관식 1번' )) return;
			if(isNullfields(frm.item_rslt_02, '객관식 2번' )) return;
			if(isNullfields(frm.item_rslt_03, '객관식 3번' )) return;
			
			// 주관식 validation Check
			if( checkForm(frm) ) {
			    
				document.getElementsByName('item_rslt_arr')[0].value = radioVal(frm.item_rslt_01);
				document.getElementsByName('item_rslt_arr')[1].value = radioVal(frm.item_rslt_02);
				document.getElementsByName('item_rslt_arr')[2].value = radioVal(frm.item_rslt_03);
				document.getElementsByName('item_rslt_arr')[3].value = frm.item_rslt_09.value;
				document.getElementsByName('item_rslt_arr')[4].value = frm.item_rslt_10.value;
				
				frm.target  = "isRegiFrame";
		        frm.action = "/event/renewal.do?method=submitEvent";
		        
		        frm.submit();
		    }
	    }
        
	}
	
	// 처리 결과 값에 따른 처리.
	// -400 : 참가정보 중복
	// -100 : 파일 사이즈 오류
	// -200 : 참가자 정보 입력오류
	// -300 : 설문등록 정보 입력오류
	//  300 : 성공 
	function isSucc(flag) {
	
		//alert(flag);
		
		var frm = document.eventForm;
		
		if( flag == '-400') {
			alert('설문 참여는 1회만 가능합니다. 이미 참여한 정보입니다.');
			frm.checkCampPart.value='N';
			return;
		}else if( flag == '400') {
			alert('설문 참여가 가능합니다.');
			frm.checkCampPart.value='Y';
			return;
		}
		
		else if( flag == '-100') {
			alert('첨부파일은 30MB 을 초과할 수 없습니다.');
			return;
		}
		
		else if( flag == '300' ) {
			alert('설문이 등록되었습니다. \n당첨여부는 입력하신 연락처로 개별통지됩니다.');
			
			// (팝업인경우) 자기창 종료 proc 필요. 
		}
		
		else {
			alert('설문 등록실패하였습니다.');
			return;
		}
		
	
	}
	
	// radio 선택 값
	function radioVal(fields) {
		
		for( var k=0; k<fields.length; k++) {
			if(fields[k].checked) {
				return fields[k].value;
				break;
			}
			
		}
	}
	
	// radio 해당필드의 null 체크
	function isNullfields(fields, mesg)
	{
		var result = true;
		var kk = 0;
		
		
		for(i=0; i<fields.length; i++)
		{
			if(fields[i].checked)
			{
				result = false;
				return result;
				//break;
			}
		}
		
		if(result)
		{
			alert(mesg + "을(를) 선택하세요.");
			fields[0].focus();
		}
		
		return result;
	}
	
	// 숫자와 - 만 입력
	function chkNum(){
		
		//alert(event.keyCode);
		
		if(      ((event.keyCode<48) || (event.keyCode>57)) 
		    && ((event.keyCode<95) || (event.keyCode>106)) 
		    && event.keyCode !=189
		    && event.keyCode !=109
		    && event.keyCode !=8){
			event.returnValue=false;
		}
	}

	
	
	function goPostNumbSrch() {
	  //window.open('/user/user.do?method=goPostNumbSrch','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=950, height=860');
		var pop = window.open("/jusoPopup.jsp?method=goPostNumbSrch","pop","width=570,height=420, scrollbars=yes, resizable=yes");
	}
	

	function jusoCallBack(roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn){
			// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
			document.form1.addr.value = roadAddrPart1;
			document.form1.detlAddr.value = addrDetail;
			document.form1.zipxNumb.value = zipNo;
	}
	
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(2);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_4.png" alt="알림마당" title="알림마당" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1">공지사항<span>&lt;</span></a></li>
							<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1">자주묻는질문<span>&lt;</span></a></li>
							<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=2&amp;page_no=1">묻고답하기<span>&lt;</span></a></li>
							<li><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=2&amp;menuSeqn=5&amp;page_no=1">홍보자료<span>&lt;</span></a></li>
							<li class="active"><a href="#1">캠페인<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>알림마당</span>&gt;<strong>캠페인</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>캠페인
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<form name="form1" method="post" action="#" enctype="multipart/form-data">
					  <input type="hidden" name="zip" />
					  <input type="hidden" name="srchAddr" />
					  <input type="submit" style="display:none;">
					  
					  <br/><br/>
					  
					  <b> [이벤트 참여자 정보]</b> <br/><br/>
					  
					  성명 : <input type="text" name="camp_part_name" id="camp_part_name"  title="참여자 성명" rangeSize="~150" nullCheck /><br/>
					  주소 : <input type="text" name="zipxNumb" id="zipxNumb" title="우편번호"  nullCheck /><a href="#1" onclick="javascript:goPostNumbSrch();"><u>[우편번호찾기]</u></a> <br/>
					 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input type="text" name="addr" id="addr" title="참여자 주소"  rangeSize="~50" nullCheck /> <input type="text" name="detlAddr" id="detlAddr"  title="참여자 상세주소"  rangeSize="~50" nullCheck /><br/>
					  연락처 : <input type="text" name="camp_part_telx" id="camp_part_telx" title="참여자 연락처" character="KE" rangeSize="~15" nullCheck onkeydown="javascript:chkNum();" style='IME-MODE: disabled'/><br/>
					  
					  <br/>
					  
					 <a href="#1" onclick="javascript:checkCampPart();"><u>설문 참여여부확인</u></a>
					 
					 <br/><br/><br/>
					</form>
					
					<form name="eventForm" method="post" action="#" enctype="multipart/form-data">
					  <input type="hidden" name="checkCampPart" value="" />
					  
					  <input type="hidden" name="camp_part_name" />
					  <input type="hidden" name="camp_part_zipx_code" />
					  <input type="hidden" name="camp_part_addr" />
					  <input type="hidden" name="camp_part_addr_detl" />
					  <input type="hidden" name="camp_part_telx" />
					  
					  <input type="hidden" name="item_rslt_arr" />
					  <input type="hidden" name="item_rslt_arr" />
					  <input type="hidden" name="item_rslt_arr" />
					  <input type="hidden" name="item_rslt_arr" />
					  <input type="hidden" name="item_rslt_arr" />
					  <input type="submit" style="display:none;">
					  
					 <b> [객관식]</b> <br/><br/>
					  
					  객관식 1번. 
					  <input type="hidden" name="item_seqn_arr" value="1" />	<!-- DB 관리 문제항목 seqn -->
					  <input type="hidden" name="item_view_seqn_arr" value="1" /> <!-- 문제항목 보여주는 seqn -->
					  
					  <input type="radio" name="item_rslt_01" id="item_rslt_01_1" value="1" title="객관식 1번" nullCheck />선택 1
					  <input type="radio" name="item_rslt_01" id="item_rslt_01_2" value="2" title="객관식 1번" nullCheck/>선택 2
					  <input type="radio" name="item_rslt_01" id="item_rslt_01_3" value="3" title="객관식 1번" nullCheck/>선택 3
					  <input type="radio" name="item_rslt_01" id="item_rslt_01_4" value="4" title="객관식 1번" nullCheck/>선택 4
					  
					   <br/>
					   
					  객관식 2번. 
					  <input type="hidden" name="item_seqn_arr" value="2" />	<!-- DB 관리 문제항목 seqn -->
					  <input type="hidden" name="item_view_seqn_arr" value="2" /> <!-- 문제항목 보여주는 seqn -->
					  
					  <input type="radio" name="item_rslt_02" id="item_rslt_02_1" value="1"/>선택 1
					  <input type="radio" name="item_rslt_02" id="item_rslt_02_2" value="2"/>선택 2
					  <input type="radio" name="item_rslt_02" id="item_rslt_02_3" value="3"/>선택 3
					  <input type="radio" name="item_rslt_02" id="item_rslt_02_4" value="4"/>선택 4
					  
					  <br/>
					   
					  객관식 3번. 
					  <input type="hidden" name="item_seqn_arr" value="3" />	<!-- DB 관리 문제항목 seqn -->
					  <input type="hidden" name="item_view_seqn_arr" value="3" /> <!-- 문제항목 보여주는 seqn -->
					  
					  <input type="radio" name="item_rslt_03" id="item_rslt_03_1" value="1"/>선택 1
					  <input type="radio" name="item_rslt_03" id="item_rslt_03_2" value="2"/>선택 2
					  <input type="radio" name="item_rslt_03" id="item_rslt_03_3" value="3"/>선택 3
					  <input type="radio" name="item_rslt_03" id="item_rslt_03_4" value="4"/>선택 4
					  
					  <br/><br/>
					  
					  <b> [주관식]</b>  <br/><br/>
					  
					  주관식 1번. 
					  <input type="hidden" name="item_seqn_arr" value="9" />	<!-- DB 관리 문제항목 seqn -->
					  <input type="hidden" name="item_view_seqn_arr" value="9" /> <!-- 문제항목 보여주는 seqn -->
					  
					  <input type="text" name="item_rslt_09" id="item_rslt_09"  title="주관식 1번" rangeSize="~600" nullCheck /> <br/>
					  
					  주관식 2번. 
					  <input type="hidden" name="item_seqn_arr" value="10" />	<!-- DB 관리 문제항목 seqn -->
					  <input type="hidden" name="item_view_seqn_arr" value="10" /> <!-- 문제항목 보여주는 seqn -->
					  
					  <input type="text" name="item_rslt_10" id="item_rslt_10"  title="주관식 2번" rangeSize="~600" nullCheck/> <br/>
					  
					  
					  <br/><br/>
					  
					  <b> [첨부파일]</b> <input type="file" name="camp_file" title="첨부파일" />
					  
					   <br/><br/>
					   
					   <a href="#1" onclick="javascript:goEvent();"><u>설문 참여하기</u></a>
					  
					</form>
						
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	
	<iframe id="isRegiFrame" title="isRegiFrame" name="isRegiFrame" width="0" height="0"></iframe>
	
</body>
</html>
