<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>법정허락 | 권리자찾기</title>

<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css"> 
<link type="text/css" rel="stylesheet" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/deScript.js"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>	
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/JavaScript">
<!--
/* calendar호출*/
function fn_calObj(frmName, objName, obj){
	var pObj = findParentTag(obj, "TR"); 
	var position = findTagByName(pObj, "input", objName); 
	//showCalendarObj(frmName, position); 
	showCalendar(frmName, objName);
} 

//취소
function fn_goList(){
	var frm = document.frm;

	if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/myStat/statRsltInqrList.do";
		frm.submit();
	}
}


//이전화면
function fn_goStep1() {
	var frm = document.frm;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		if(confirm("이전 단계로 가시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
			frm.stat_cd.value = '${statCd }';
			frm.target = "_self";
			frm.method = "post";
			frm.action = "/myStat/statPrpsModi.do";
			frm.submit();
		}
	}
}

//신청서구분
function fn_chkApplyType(oObj, sNo) {
	var oChkApplyType = document.getElementsByTagName("input");
	for(i = 0; i < oChkApplyType.length; i++) {
		if(oChkApplyType[i].type == "checkbox") {
			
			if( (oChkApplyType[i].id).indexOf("dummyApplyType0") > -1 && (oChkApplyType[i].id).indexOf("_"+sNo) > -1 && oChkApplyType[i].id != oObj.id) {
				oChkApplyType[i].checked = false;
			}
		}
	}
}



//공표방법
function fn_chkPublMediCd(oObj, sNo) {
	var oChkApplyType = document.getElementsByTagName("input");
	for(i = 0; i < oChkApplyType.length; i++) {
		if(oChkApplyType[i].type == "checkbox") {
			
			if( (oChkApplyType[i].id).indexOf("dummyPublMediCd") > -1 
					&& (oChkApplyType[i].id).indexOf("_"+sNo) > -1 
					&& oChkApplyType[i].id != oObj.id
					&& oChkApplyType[i].name == "dummyPublMediCd"+sNo) {
				oChkApplyType[i].checked = false;
			}
		}
	}
	
	if((oObj.id).indexOf('99') > -1 ) {
		if(oObj.checked == true) {
			document.getElementById("publMediEtc"+sNo).style.display = "";
		}else{
			document.getElementById("publMediEtc"+sNo).style.display = "none";
			document.getElementById("publMediEtc"+sNo).value = "";
		}
	}
}

//탭 선택에 따른 화면 변경
function fn_chgTabDisplay(idx) {

	var oA = document.getElementsByTagName("A");
	
	for(i = 0; i < oA.length; i++) {
		if( (oA[i].id).indexOf("tabA") > -1) {
			if(oA[i].id == "tabA"+idx) {
				oA[i].className = "active";
			} else {
				oA[i].className = "";
			}
		}
	}
	
    var oTbl = document.getElementById("tblTab");
	var oTr = oTbl.getElementsByTagName("TR");
	var oSpan = oTbl.getElementsByTagName("SPAN");	//탭별 삭제버튼 IE7에서 제대로 숨겨지지 않아 직접 숨김
	
	for(i = 0; i < oSpan.length; i++) {
		if( (oSpan[i].id).indexOf("sp") > -1) {
			if(oSpan[i].id == "sp"+idx) {
				oSpan[i].style.display = "";
			} else {
				oSpan[i].style.display = "none";
			}
		}
	}
	
	for(i = 0; i < oTr.length; i++) {
		if( (oTr[i].id).indexOf("trTab1") > -1) {
			if(oTr[i].id == "trTab1"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
		if( (oTr[i].id).indexOf("trTab2") > -1) {
			if(oTr[i].id == "trTab2"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
		if( (oTr[i].id).indexOf("trTab3") > -1) {
			if(oTr[i].id == "trTab3"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
		if( (oTr[i].id).indexOf("trTab4") > -1) {
			if(oTr[i].id == "trTab4"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
	}
}


var iTabIdx = 0;

//탭 추가
function fn_addTab() {

	var iApplyWorksCnt = '${applyWorksCnt}';

	var oUl = document.getElementsByTagName("UL");
	var tabCnt = 0;
	
	for(i = 0; i < oUl.length; i++) {
		if( (oUl[i].id).indexOf("tab") > -1) {
			tabCnt++;
		}
	}
	
	if( tabCnt == iApplyWorksCnt ) {
		alert('\'1단계 이용 승인 신청서\'에서 입력한 신청건수를 초과 할 수 없습니다. \n\r추가를 하실려면 이전단계로 가셔서 신청건수를 수정하셔야 합니다.');
		return;
	}
	
	var sTag = "";
	
	//탭버튼 생성 str
    var oDiv = document.getElementById("divTab");
    
    var oUl = document.createElement("UL");
    oUl.setAttribute("name", "tab");
    oUl.setAttribute("id", "tab"+iTabIdx);
    oUl.style.display = "block";
    oUl.style.height = "25px";
    sTag="	<li>";
    sTag+="		<a href=\"#1\" id=\"tabA"+iTabIdx+"\" name=\"tabA\" ";
	sTag+="			onclick=\"fn_chgTabDisplay(\'"+iTabIdx+"\');\" onkeypress=\"fn_chgTabDisplay(\'"+iTabIdx+"\');\" class=\"active\">";
	sTag+="			<strong id=\"tabNm"+iTabIdx+"\" style=\"width:80px;\">신규 명세서<\/strong>";
	sTag+="		<\/a>";
	sTag+="	<\/li>";
	oUl.innerHTML = sTag;
	oDiv.appendChild(oUl);
	//탭버튼 생성 end

	//화면 입력부 생성 str
    var oTbl = document.getElementById("tblTab");
    var oTbody = oTbl.getElementsByTagName("TBODY")[0];

	//저작물
    var oTr1 = document.createElement("TR");
    oTr1.setAttribute("name", "trTab1");
    oTr1.setAttribute("id", "trTab1"+iTabIdx);
	
	    var oTh = document.createElement("TH");
	    oTh.setAttribute("rowSpan", "4");
	    oTh.setAttribute("scope", "row");
	    oTh.className = "ce";
		sTag="	        <span id=\"sp"+iTabIdx+"\" class=\"button small black\">";
		sTag+="	            <a href=\"#1\" onclick=\"fn_delTab(\'"+iTabIdx+"\');\" onkeypress=\"fn_delTab(\'"+iTabIdx+"\');\">삭제<\/a>";
		sTag+="	        <\/span>";
		oTh.innerHTML = sTag;
		oTr1.appendChild(oTh);
		
		oTh = document.createElement("TH");
	    oTh.setAttribute("scope", "row");
	    sTag = "저작물";
		oTh.innerHTML = sTag;
		oTr1.appendChild(oTh)
		
		var oTd = document.createElement("TD");
		sTag="	    	<p class=\"mb5\">";
		    			<c:set var="dySeq" value="0"/>
		    			<c:forEach items="${applyTypeList}" var="applyTypeList">
		    				<c:set var="dySeq" value="${dySeq + 1}"/>
		sTag+="				<input type=\"checkbox\" id=\"dummyApplyType0${dySeq }_"+iTabIdx+"\" name=\"dummyApplyType0${dySeq }\" value=\"1\" ";
		sTag+="					onclick=\"fn_chkApplyType(this, "+iTabIdx+")\" onkeypress=\"fn_chkApplyType(this, "+iTabIdx+")\">";
		sTag+="				<label for=\"dummyApplyType0${dySeq }_"+iTabIdx+"\" class=\"p12\">${applyTypeList.codeName }<\/label>&nbsp;";
		    			</c:forEach>
		sTag+="		    <\/p>";
		sTag+="    		<span class=\"topLine2\"><\/span>";
		sTag+="    		<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" summary=\"\" class=\"grid\">";
		sTag+="    			<colgroup>";
		sTag+="        			<col width=\"20%\">";
		sTag+="        			<col width=\"30%\">";
		sTag+="        			<col width=\"20%\">";
		sTag+="        			<col width=\"*\">";
		sTag+="    			<\/colgroup>";
		sTag+="    			<tbody>";
		sTag+="    				<tr>";
		sTag+="    					<th scope=\"row\">";
		sTag+="    					    <label for=\"worksTitl"+iTabIdx+"\" class=\"necessary\">제호(제목)<\/label>";
		sTag+="    					<\/th>";
		sTag+="    					<td colspan=\"3\">";
		sTag+="    						<input type=\"text\" name=\"worksTitl\" id=\"worksTitl"+iTabIdx+"\" ";
		sTag+="    						    onchange=\"fn_titlSycn(\'"+iTabIdx+"\', this.value)\" class=\"w98\" title=\"제목(제호)\" rangeSize=\"0~500\" nullCheck>";
		sTag+="    					<\/td>";
		sTag+="    				<\/tr>";
		sTag+="    				<tr>";
		sTag+="    					<th scope=\"row\">";
		sTag+="    					    <label for=\"worksKind"+iTabIdx+"\" class=\"necessary\">종류</label>";
		sTag+="    					<\/th>";
		sTag+="    					<td>";
		sTag+="    					    <input type=\"text\" name=\"worksKind\" id=\"worksKind"+iTabIdx+"\" class=\"w98\" title=\"종류\" rangeSize=\"0~200\" nullCheck>";
		sTag+="    					<\/td>";
		sTag+="    					<th scope=\"row\">";
		sTag+="    					    <label for=\"worksForm"+iTabIdx+"\" class=\"necessary\">형태 및 수량</label>";
		sTag+="    					<\/th>";
		sTag+="    					<td>";
		sTag+="    					    <input type=\"text\" name=\"worksForm\" id=\"worksForm"+iTabIdx+"\" class=\"w98\" title=\"형태 및 수량\" rangeSize=\"0~200\" nullCheck>";
		sTag+="    					<\/td>";
		sTag+="    				<\/tr>";
		sTag+="    			<\/tbody>";
		sTag+="    		<\/table>";
		oTd.innerHTML = sTag;
	oTr1.appendChild(oTd)

	
	//공표
    var oTr2 = document.createElement("TR");
    oTr2.setAttribute("name", "trTab2");
    oTr2.setAttribute("id", "trTab2"+iTabIdx);
    
		oTh = document.createElement("TH");
	    oTh.setAttribute("scope", "row");
		sTag="	        공표<span class=\"block gray thin p11\">(*실연/음반발매/<br />방송 등 포함)</span>";
		oTh.innerHTML = sTag;
		oTr2.appendChild(oTh)
		
		oTd = document.createElement("Td");
		sTag="	    	<span class=\"topLine2\"></span>";
		sTag+="	    	<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" summary=\"\" class=\"grid\">";
		sTag+="	    		<colgroup>";
		sTag+="        			<col width=\"20%\">";
		sTag+="        			<col width=\"30%\">";
		sTag+="        			<col width=\"20%\">";
		sTag+="        			<col width=\"*\">";
		sTag+="	    		</colgroup>";
		sTag+="	    		<tbody>";
		sTag+="	    			<tr>";
		sTag+="	    				<th scope=\"row\">";
		sTag+="	    				    <label for=\"publYmd"+iTabIdx+"\" class=\"necessary\">공표연월일</label>";
		sTag+="	    				</th>";
		sTag+="	    				<td>";
		sTag+="	    				    <input type=\"text\" name=\"publYmd\" id=\"publYmd"+iTabIdx+"\" size=\"12\" title=\"공표연월일\" maxlength=\"8\" character=\"EK\" dateCheck nullCheck>";
		
		sTag+=" <img title=\"공표연월일을 선택하세요.\" alt=\"\" class=\"vmid\" align=\"middle\" style=\"cursor:pointer;\"";
		sTag+="		onclick=\"javascript:fn_calObj(\'frm\',\'publYmd\',this);\"";
		sTag+="		src=\"/images/2011/common/calendar.gif\">";
															
		sTag+="	    				</td>";
		sTag+="	    				<th scope=\"row\">";
		sTag+="	    				    <label for=\"publNatn"+iTabIdx+"\" class=\"necessary\">공표국가</label>";
		sTag+="	    				</th>";
		sTag+="	    				<td>";
		sTag+="	    				    <input type=\"text\" name=\"publNatn\" id=\"publNatn"+iTabIdx+"\" class=\"w98\" title=\"공표국가\" rangeSize=\"0~100\" nullCheck>";
		sTag+="	    				</td>";
		sTag+="	    			</tr>";
		sTag+="	    			<tr>";
		sTag+="	    				<th scope=\"row\">";
		sTag+="	    				    <label for=\"\" class=\"necessary\">공표방법</label>";
		sTag+="	    				</th>";
		sTag+="	    				<td>";
									<c:set var="dySeq" value="0"/>
							        <c:forEach items="${publMediList}" var="publMediList">
										<c:set var="dySeq" value="${dySeq + 1}"/>
		sTag+="							<input type=\"checkbox\" name=\"dummyPublMediCd"+iTabIdx+"\" id=\"dummyPublMediCd_${publMediList.code }_"+iTabIdx+"\" value=\"${publMediList.code }\" ";
		sTag+="								onclick=\"fn_chkPublMediCd(this, "+iTabIdx+")\" onkeypress=\"fn_chkPublMediCd(this, "+iTabIdx+")\"><label for=\"dummyPublMediCd_${publMediList.code }_"+iTabIdx+"\" class=\"necessary\">${publMediList.codeName }</label>&nbsp;<br/>";
								    </c:forEach>
		sTag+="	    					<input type=\"text\" name=\"publMediEtc\" id=\"publMediEtc"+iTabIdx+"\" size=\"12\" style=\"display:none;\" >";
		sTag+="	    				</td>";
		sTag+="	    				<th scope=\"row\">";
		sTag+="	    				    <label for=\"publMedi"+iTabIdx+"\" class=\"necessary\">공표매체정보</label>";
		sTag+="	    				</th>";
		sTag+="	    				<td>";
		sTag+="	    				    <input type=\"text\" name=\"publMedi\" id=\"publMedi"+iTabIdx+"\" class=\"w98\" title=\"공표매체정보\" rangeSize=\"0~200\" nullCheck>";
		sTag+="	    				</td>";
		sTag+="	    			</tr>";
		sTag+="	    		</tbody>";
		sTag+="	    	</table>";
		oTd.innerHTML = sTag;
	oTr2.appendChild(oTd)

	
	//권리자
    var oTr3 = document.createElement("TR");
    oTr3.setAttribute("name", "trTab3");
    oTr3.setAttribute("id", "trTab3"+iTabIdx);
	    
		oTh = document.createElement("TH");
	    oTh.setAttribute("scope", "row");
		sTag="권리자";
		oTh.innerHTML = sTag;
		oTr3.appendChild(oTh)
		
		oTd = document.createElement("Td");
		sTag="	    	<span class=\"topLine2\"></span>";
		sTag+="	    	<table cellspacing=\"0\" cellpadding=\"0\" border=\"1\" summary=\"\" class=\"grid\">";
		sTag+="	    		<colgroup>";
		sTag+="        			<col width=\"20%\">";
		sTag+="        			<col width=\"30%\">";
		sTag+="        			<col width=\"20%\">";
		sTag+="        			<col width=\"*\">";
		sTag+="	    		</colgroup>";
		sTag+="	    		<tbody>";
		sTag+="	    			<tr>";
		sTag+="	    				<th scope=\"row\">";
		sTag+="	    				    <label for=\"coptHodrName"+iTabIdx+"\" class=\"necessary\">성명(법인명)</label>";
		sTag+="	    				</th>";
		sTag+="	    				<td>";
		sTag+="	    				    <input type=\"text\" name=\"coptHodrName\" id=\"coptHodrName"+iTabIdx+"\" class=\"w98\" title=\"성명(법인명)\" rangeSize=\"0~200\" nullCheck>";
		sTag+="	    				</td>";
		sTag+="	    				<th scope=\"row\">";
		sTag+="	    				    <label for=\"coptHodrTelxNumb"+iTabIdx+"\" class=\"necessary\">전화번호</label>";
		sTag+="	    				</th>";
		sTag+="	    				<td>";
		sTag+="	    				    <input type=\"text\" name=\"coptHodrTelxNumb\" id=\"coptHodrTelxNumb"+iTabIdx+"\" class=\"w98\" title=\"전화번호\" rangeSize=\"0~20\" nullCheck>";
		sTag+="	    				</td>";
		sTag+="	    			</tr>";
		sTag+="	    			<tr>";
		sTag+="	    				<th scope=\"row\">";
		sTag+="	    				    <label for=\"cpotHodrAddr"+iTabIdx+"\" class=\"necessary\">주소</label>";
		sTag+="	    				</th>";
		sTag+="	    				<td colspan=\"3\">";
		sTag+="	    				    <input type=\"text\" name=\"coptHodrAddr\" id=\"coptHodrAddr"+iTabIdx+"\" class=\"w98\" title=\"주소\" rangeSize=\"0~200\" nullCheck>";
		sTag+="	    				</td>";
		sTag+="	    			</tr>";
		sTag+="	    		</tbody>";
		sTag+="	    	</table>";
		oTd.innerHTML = sTag;
	oTr3.appendChild(oTd)

	
	//신청물의 내용
    var oTr4 = document.createElement("TR");
    oTr4.setAttribute("name", "trTab4");
    oTr4.setAttribute("id", "trTab4"+iTabIdx);
    
		oTh = document.createElement("TH");
	    oTh.setAttribute("scope", "row");
		sTag="	        <label for=\"\" class=\"necessary\">신청물의 내용</label>";
		oTh.innerHTML = sTag;
		oTr4.appendChild(oTh)
		
		oTd = document.createElement("Td");
		sTag="	        <textarea cols=\"10\" name=\"worksDesc\" id=\"worksDesc"+iTabIdx+"\" class=\"inputData h100 w98\" rows=\"5\" title=\"신청물의 내용\" rangeSize=\"0~4000\" nullCheck></textarea>";
		oTd.innerHTML = sTag;
	oTr4.appendChild(oTd)
	
	oTbody.appendChild(oTr1);
	oTbody.appendChild(oTr2);
	oTbody.appendChild(oTr3);
	oTbody.appendChild(oTr4);
	//화면 입력부 생성 end
	
	fn_chgTabDisplay(iTabIdx);
	
	iTabIdx++;
}


//탭버튼에 제목 설정
function fn_titlSycn(idx, sTitl) {

	if(sTitl.length > 5) {
		sTitl = sTitl.substr(0, 5) + "...";
	}

	var oStrong = document.getElementsByTagName("STRONG");
	
	for(i = 0; i < oStrong.length; i++) {
		if( (oStrong[i].id).indexOf("tabNm") > -1) {
			if(oStrong[i].id == "tabNm"+idx) {
				oStrong[i].innerText = sTitl;
			}
		}
	}
}

//탭 삭제
function fn_delTab(idx) {

	//탭 갯수 및 삭제 대상 검사
    var oTbl = document.getElementById("tblTab");
    var oTbody = oTbl.getElementsByTagName("TBODY")[0];
    
	var oUl = document.getElementsByTagName("UL");
	var oTr = oTbl.getElementsByTagName("TR");
	
	var tgtIdx = -1;
	var tabCnt = 0;
	
	for(i = 0; i < oUl.length; i++) {
		
		if( (oUl[i].id).indexOf("tab") > -1) {
			tabCnt++;
			if(oUl[i].id == "tab"+idx) {
				tgtIdx = i;
			}
		}
	}

	//탭 삭제
	if(tabCnt > 1) {
		if(tgtIdx > -1) {
//			oUl[tgtIdx].removeNode(true);
			var oDiv = document.getElementById("divTab");
			oDiv.removeChild(oUl[tgtIdx]);

			//삭제 탭과 관련된 tr 삭제
			var oTr = oTbl.getElementsByTagName("TR");
			for(j = 0; j < oTr.length; j++) {
				if( (oTr[j].id).indexOf("trTab1") > -1) {
					if(oTr[j].id == "trTab1"+idx) {
						oTbody.removeChild(oTr[j]);
					}
				}
				if( (oTr[j].id).indexOf("trTab2") > -1) {
					if(oTr[j].id == "trTab2"+idx) {
						oTbody.removeChild(oTr[j]);
					}
				}
				if( (oTr[j].id).indexOf("trTab3") > -1) {
					if(oTr[j].id == "trTab3"+idx) {
						oTbody.removeChild(oTr[j]);
					}
				}
				if( (oTr[j].id).indexOf("trTab4") > -1) {
					if(oTr[j].id == "trTab4"+idx) {
						oTbody.removeChild(oTr[j]);
					}
				}
			}
		}
	} else {
		alert("승인신청  명세서는 최소 1건 이상이어야 합니다.");
		return;
	}
	

	//탭 삭제 후 제일 처음 탭 상태 active로 변경
	var oA = document.getElementsByTagName("A");
	var isActive = false;
	for(i = 0; i < oA.length; i++) {
		if(isActive == false) {
			if( (oA[i].id).indexOf("tabA") > -1) {
				oA[i].className = "active";
				isActive = true;
			}
		}
	}

	//tr 삭제 후 제일 처음 div 상태 display로 변경	
	isActive = false;
	oTr = document.getElementsByTagName("TR");
	for(i = 0; i < oTr.length; i++) {
		if(isActive == false) {
			if( (oTr[i].id).indexOf("trTab1") > -1) {
				oTr[i].style.display = "";
			}
			if( (oTr[i].id).indexOf("trTab2") > -1) {
				oTr[i].style.display = "";
			}
			if( (oTr[i].id).indexOf("trTab3") > -1) {
				oTr[i].style.display = "";
			}
			if( (oTr[i].id).indexOf("trTab4") > -1) {
				oTr[i].style.display = "";
				isActive = true;
			}
		}
	}
	
	isActive = false;
	var oSpan = oTbl.getElementsByTagName("SPAN");	//탭별 삭제버튼 IE7에서 제대로 숨겨지지 않아 직접 숨김
	for(i = 0; i < oSpan.length; i++) {
		if(isActive == false) {
			if( (oSpan[i].id).indexOf("sp") > -1) {
				oSpan[i].style.display = "";
				isActive = true;
			}
		}
	}
}

//table row 별 필수체크
function checkTr(oTr) {

	var result = ""; 
	var oInput = oTr.getElementsByTagName("input");
	
	for(z = 0; z<oInput.length; z++){
		
		if(checkField2(oInput[z]) == false) {
			result = oInput[z].id;
			break;
		}
	}
	
	return result;
}

//필수체크
function fn_chkValue() {

	var frm = document.frm;
	var tgtTabIdx = "";
	
	var oTr = document.getElementsByTagName("tr");
	
	for(i = 0; i < oTr.length; i++) {
		
		//저작물 관련
		if(oTr[i].id.indexOf("trTab1") > -1) {
			tgtTabIdx = (oTr[i].id).substr(6);
			
			//신청서 구분 체크여부 확인
			var chkCnt = 0;
			var focusTgt = '';
			
			var oInput = oTr[i].getElementsByTagName("input");
			for(j = 0; j < oInput.length; j++) {
				if(oInput[j].type != 'undefined' && oInput[j].type == "checkbox" && (oInput[j].id).indexOf("dummyApplyType0") > -1) {
					if(oInput[j].name == "dummyApplyType01") {
						chkCnt = 0;
						focusTgt = oInput[j].id;
					}
					
					if(oInput[j].checked == true) {
						chkCnt++;
					}
				}
			}
					
			if(chkCnt < 1) {
				alert("저작물 구분을(를) 선택하세요.");
				fn_chgTabDisplay(tgtTabIdx);
				document.getElementById(focusTgt).focus();
				return false;
			}
			
			tgtId = checkTr(oTr[i]);
			
			if(tgtId != "") {
				fn_chgTabDisplay(tgtTabIdx);
				document.getElementById(tgtId).focus();
				return false;
			}
		}
	//}

	//for(i = 0; i < oTr.length; i++) {	
		//공표 관련
		if(oTr[i].id.indexOf("trTab2") > -1) {
			var tgtObj = oTr[i];

			tgtTabIdx = (oTr[i].id).substr(6);
		
			tgtId = checkTr(oTr[i]);
			
			if(tgtId != "") {
				fn_chgTabDisplay(tgtTabIdx);
				document.getElementById(tgtId).focus();
				return false;
			}
		
			//공표방법 구분 체크여부 확인
			chkCnt = 0;
			focusTgt = '';
			var chkValue = '';

			var oInput = tgtObj.getElementsByTagName("input");
			for(j = 0; j < oInput.length; j++) {
				if(oInput[j].type != 'undefined' && oInput[j].type == "checkbox" && (oInput[j].id).indexOf("dummyPublMediCd") > -1) {
					if((oInput[j].id).indexOf("dummyPublMediCd_1") > -1) {
						chkCnt = 0;
						focusTgt = oInput[j].id;
					}
					
					if(oInput[j].checked == true) {
						chkCnt++;
						chkValue = oInput[j].value;
					}
				}
			}
					
			if(chkCnt < 1) {
				alert("공표방법을(를) 선택하세요.");
				fn_chgTabDisplay(tgtTabIdx);
				document.getElementById(focusTgt).focus();
				return false;
			}
			
			if(chkValue == '99' && document.getElementById("publMediEtc"+tgtTabIdx).value == '') {
				alert("공표방법 기타내용을 입력하세요.");
				fn_chgTabDisplay(tgtTabIdx);
				document.getElementById("publMediEtc"+tgtTabIdx).focus();
				return false;
			}
		}
	//}

	//for(i = 0; i < oTr.length; i++) {	
		//권리자 관련
		if(oTr[i].id.indexOf("trTab3") > -1) {
			tgtTabIdx = (oTr[i].id).substr(6);
			tgtId = checkTr(oTr[i]);
			
			if(tgtId != "") {
				fn_chgTabDisplay(tgtTabIdx);
				document.getElementById(tgtId).focus();
				return false;
			}
		}
	//}

	//for(i = 0; i < oTr.length; i++) {	
		//신청물의 내용 관련
		if(oTr[i].id.indexOf("trTab4") > -1) {
			tgtTabIdx = (oTr[i].id).substr(6);
			oInput = oTr[i].getElementsByTagName("textarea");
			if(checkField2(oInput[0]) == false) {
				tgtId = oInput[0].id;
			}
			
			if(tgtId != "") {
				fn_chgTabDisplay(tgtTabIdx);
				document.getElementById(tgtId).focus();
				return false;
			}
		}
	}
	
	<c:set var="dySeq" value="0"/>
	<c:forEach items="${applyTypeList}" var="applyTypeList">
		<c:set var="dySeq" value="${dySeq + 1}"/>
		var oChk = document.getElementsByName("dummyApplyType0${dySeq}");
		var sVal = "";
		for(i = 0; i < oChk.length; i++) {
			if(oChk[i].checked == true) {
				sVal = sVal+"1";
			}else{
				sVal = sVal+"0";
			}
		}
		document.getElementById("applyType0${dySeq}").value = sVal;
	</c:forEach>
	
	oInput = document.getElementsByTagName("input");
	sVal = "";
	for(i = 0; i < oInput.length; i++) {
		if(oInput[i].type != 'undefined' && oInput[i].type == 'checkbox' && (oInput[i].name).indexOf('dummyPublMediCd') > -1) {
			if(oInput[i].checked == true) {
				sVal = sVal+oInput[i].value + "!";
			}
		}
	}
	document.getElementById("publMediCd").value = sVal;

	return true;
}

//임시저장 및 화면이동
function fn_doSave(sDiv) {
	var frm = document.frm;
	var nowStatCd = '${statCd }';
	frm.action_div.value = sDiv;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		if(fn_chkValue()) {
		
			var iApplyWorksCnt = '${applyWorksCnt}';

			var oUl = document.getElementsByTagName("UL");
			var tabCnt = 0;
			
			for(i = 0; i < oUl.length; i++) {
				if( (oUl[i].id).indexOf("tab") > -1) {
					tabCnt++;
				}
			}
			
			if(iApplyWorksCnt > tabCnt){
				alert('현재 작성하신 \'이용승인명세서\'의 건수가 \n\r이전단계에서 신청하신 건수와 일치하지 않습니다.');
				return;
			}
		
			/*if(sDiv == 'goStep3' && frm.existYn.value != 'Y') {
				alert('현재 작성하신 이용승인신청에 대한\n\r\'이용승인명세서\' 관련 첨부파일이 첨부되지 않았습니다.\n\r이전단계 이동합니다.');
				frm.stat_cd.value = '${statCd }';
			}*/
		
			frm.target = "_self";
			frm.method = "post";
			frm.action = "/myStat/tmpStatPrps2SaveDo.do";
   			if(nowStatCd== '3' || nowStatCd== '4'){
				frm.action = "/myStat/statPrps2SaveDo.do";
   			}
			frm.submit();
		}
	}
}

	
function fn_goSample(){
	window.open("/images/2011/smpl/stat_step2.jpg", "", "width=770,height=850,scrollbars=yes");
}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2011/header.jsp" /> --%>
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- //HEADER end -->
		<div id="test"></div>
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2>
					<span>
						<img src="/images/2011/title/container_vis_h2_8.gif" alt="마이페이지" title="마이페이지" />
						<em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em>
					</span>
				</h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<jsp:include page="/include/2011/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1");
					subSlideMenu("sub_lnb","lnb13");
				</script>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>신청현황</span><em>법정허락</em></p>
					<h1><img src="/images/2011/title/content_h1_0807.gif" alt="법정허락" title="법정허락" /></h1>
					<jsp:include page="/common/memo/2011/memo_01.jsp">
						<jsp:param name="DIVS" value="MS" />
					</jsp:include>					
					<div class="section mt20">
						<div class="usr_process stat_process">
							<div class="process_box">
								<ul class="floatDiv">
								<li class="fl ml0"><img alt="1단계 약관동의" src="/images/2011/content/process21_off.gif"></li>
								<li class="fl on"><img alt="2단계 회원정보입력" src="/images/2011/content/process22_on.gif"></li>
								<li class="fl bgNone pr0"><img alt="3단계 가입완료" src="/images/2011/content/process23_off.gif"></li>
								</ul>
							</div>
						</div>
						
						<form name="frm" action="#">
							<input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
							<input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
							<input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
							<input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
							<input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
							<input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
							<input type="hidden" name="action_div" value=""/>
							
							<input type="hidden" name="applyWriteYmd" value="${srchParam.applyWriteYmd }">
							<input type="hidden" name="applyWriteSeq" value="${srchParam.applyWriteSeq }">
							<input type="hidden" name="apply_write_ymd" value="${srchParam.applyWriteYmd }">
							<input type="hidden" name="apply_write_seq" value="${srchParam.applyWriteSeq }">
							<input type="hidden" name="stat_cd"/>
					    	<input type="hidden" name="existYn" value="Y"/>
							<input type="submit" style="display:none;">
							
							<c:set var="dySeq" value="0"/>
							<c:forEach items="${applyTypeList}" var="applyTypeList">
								<c:set var="dySeq" value="${dySeq + 1}"/>
								<input type="hidden" id="applyType0${dySeq }" name="applyType0${dySeq }" value="">
							</c:forEach>
							<input type="hidden" id="publMediCd" name="publMediCd" value="">
						<!--이용 승인신청 명세서 정보 str -->
						<div class="floatDiv mt20">
							<h2 class="fl">
								이용 승인신청 명세서 정보
								<a href="#1" onclick="fn_addTab();" id="workAdd2">
								<img src="/images/2011/button/add.gif" alt="추가" /></a>
								<!-- 
								<span class="button small black">
									<a href="#1" onclick="fn_addTab();" onkeypress="fn_addTab();">추가</a>
								</span>
								<span class="button small icon thin" onclick="fn_addTab();">
									<!-- 
									<a href="#1" onclick="fn_addTab();">추가</a><span class="add"></span>
									 <a href="#1" >추가</a><span class="add"></span>
								</span>
								<span class="button small icon thin">
									<a href="#" onclick="fn_delTab();">삭제</a><span class="delete"></span>
								</span>
								-->
							</h2>
							<p class="fr"><span class="button small icon"><a href="#1" onclick="fn_goSample();" onkeypress="fn_goSample();">예시화면 보기</a><span class="help"></span></span></p>
						</div>
						<br/>

						<!-- 탭 str -->
						<div id="divTab" name="divTab" class="fl newStatus">
							<c:if test="${not empty statApplyWorksList}">
								<c:set var="idx" value="0"/>
								<c:set var="display" value=""/>
								<c:forEach items="${statApplyWorksList}" var="list">

									<c:if test="${fn:length(statApplyWorksList) == (idx+1)}">
										<c:set var="display" value="active"/>
									</c:if>
									<UL name="tab" id="tabD${idx }" style="display:block;height:25px">
										<li>
											<a href="#1" id="tabAD${idx }" name="tabA" onclick="fn_chgTabDisplay('D${idx }');" onkeypress="fn_chgTabDisplay('D${idx }');" class="${display }">
												<c:if test="${fn:length(list.WORKSTITL) > 5}">
													<strong id="tabNmD${idx }" style="width:80px;">${fn:substring(list.WORKSTITL, 0, 5)}</strong>
												</c:if>
												<c:if test="${fn:length(list.WORKSTITL) < 6}">
													<strong id="tabNmD${idx }" style="width:80px;">${list.WORKSTITL}</strong>
												</c:if>
											</a>
										</li>
									</UL>
									<c:set var="idx" value="${idx+1}"/>
								</c:forEach>
							</c:if>
						</div>
						<!-- 탭 end -->
						
						<table id="tblTab" cellspacing="0" cellpadding="0" border="1" class="grid " summary="">
							<colgroup>
							<col width="5%">
							<col width="20%">
							<col width="*">
							</colgroup>
							<tbody>
								<c:if test="${not empty statApplyWorksList}">
									<c:set var="idx" value="0"/>
									<c:set var="display" value="none"/>
									<c:forEach items="${statApplyWorksList}" var="list">
										<c:if test="${(fn:length(statApplyWorksList)) == (idx+1)}">
											<c:set var="display" value=""/>
										</c:if>
										<TR name="trTab1" id="trTab1D${idx }" style="display:${display };">
											<TH rowSpan="4" scope="row" class="ce">
												<span id="spD${idx }" class="button small black" style="display:${display };">
													<a href="#1" onclick="fn_delTab('D${idx }');" onkeypress="fn_delTab('D${idx }');">삭제</a>
												</span>
											</TH>
											<TH scope="row">저작물</TH>
											<TD>
												<p class="mb5">
													<c:set var="dySeq" value="0"/>
													<c:forEach items="${applyTypeList}" var="applyTypeList">
														<c:set var="dySeq" value="${dySeq + 1}"/>
														<input type="checkbox" id="dummyApplyType0${dySeq }_D${idx }" name="dummyApplyType0${dySeq }" value="1" 
															onclick="fn_chkApplyType(this, 'D${idx }');" onkeypress="fn_chkApplyType(this, 'D${idx }');" >
														<label for="dummyApplyType0${dySeq }_D${idx }" class="p12">${applyTypeList.codeName }</label>&nbsp;
													</c:forEach>
												</p>
												<span class="topLine2"></span>
												<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
													<colgroup>
														<col width="20%">
														<col width="30%">
														<col width="20%">
														<col width="*">
													</colgroup>
													<tbody>
														<tr>
															<th scope="row">
																<label for="worksTitlD${idx }" class="necessary">제호(제목)</label>
															</th>
															<td colspan="3">
																<input type="text" name="worksTitl" id="worksTitlD${idx }"
																	value="${list.WORKSTITL }" 
																	onchange="fn_titlSycn('D${idx }', this.value);" class="w98" title="제목(제호)" rangeSize="0~500" nullCheck>
															</td>
														</tr>
														<tr>
															<th scope="row">
																<label for="worksKindD${idx }" class="necessary">종류</label>
															</th>
															<td>
																<input type="text" name="worksKind" id="worksKindD${idx }" 
																	value="${list.WORKSKIND }" 
																	class="w98" title="종류" rangeSize="0~200" nullCheck>
															</td>
															<th scope="row">
																<label for="worksFormD${idx }" class="necessary">형태 및 수량</label>
															</th>
															<td>
																<input type="text" name="worksForm" id="worksFormD${idx }" 
																	value="${list.WORKSFORM }" 
																	class="w98" title="형태 및 수량" rangeSize="0~200" nullCheck>
															</td>
														</tr>
													</tbody>
												</table>
											</TD>
										</TR>
										<TR name="trTab2" id="trTab2D${idx }" style="display:${display };">
											<TH scope="row">공표<span class="block gray thin p11">(*실연/음반발매/<br />방송 등 포함)</span></TH>
											<Td>
												<span class="topLine2"></span>
												<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
													<colgroup>
														<col width="20%">
														<col width="30%">
														<col width="20%">
														<col width="*">
													</colgroup>
													<tbody>
														<tr>
															<th scope="row">
																<label for="publYmdD${idx }" class="necessary">공표연월일</label>
															</th>
															<td>
																<input type="text" name="publYmd" id="publYmdD${idx }" 
																	value="${list.PUBLYMD }" 
																	size="12" title="공표연월일" character="EK" maxlength="8" dateCheck nullCheck>
															</td>
															<th scope="row">
																<label for="publNatnD${idx }" class="necessary">공표국가</label>
															</th>
															<td>
																<input type="text" name="publNatn" id="publNatnD${idx }" 
																	value="${list.PUBLNATN }" 
																	class="w98" title="공표국가" rangeSize="0~100" nullCheck>
															</td>
														</tr>
														<tr>
															<th scope="row">
																<label for="" class="necessary">공표방법</label>
															</th>
															<td>
																<c:set var="dySeq" value="0"/>
																<c:forEach items="${publMediList}" var="publMediList">
																	<c:set var="dySeq" value="${dySeq + 1}"/>
																	<input type="checkbox" name="dummyPublMediCdD${idx }" id="dummyPublMediCd_${publMediList.code }_D${idx }" value="${publMediList.code }" 
																		<c:if test="${list.PUBLMEDICD == publMediList.code}">checked="checked"</c:if> 
																		onclick="fn_chkPublMediCd(this, 'D${idx }');" onkeypress="fn_chkPublMediCd(this, 'D${idx }');">
																	<label for="dummyPublMediCd_${publMediList.code }_D${idx }" class="necessary">${publMediList.codeName }</label>&nbsp;<br/>
																</c:forEach>
																<input type="text" name="publMediEtc" id="publMediEtcD${idx }" size="12" value="${list.PUBLMEDIETC}" 
																	style="display:<c:if test="${list.PUBLMEDICD != '99'}">none</c:if>;" >
															</td>
															<th scope="row">
																<label for="publMediD${idx }" class="necessary">공표매체정보</label>
															</th>
															<td>
																<input type="text" name="publMedi" id="publMediD${idx }" 
																	value="${list.PUBLMEDI }" 
																	class="w98" title="공표매체정보" rangeSize="0~200" nullCheck>
															</td>
														</tr>
													</tbody>
												</table>
											</Td>
										</TR>
										<TR name="trTab3" id="trTab3D${idx }" style="display:${display };">
											<TH scope="row">권리자</TH>
											<Td>
												<span class="topLine2"></span>
												<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
													<colgroup>
														<col width="20%">
														<col width="30%">
														<col width="20%">
														<col width="*">
													</colgroup>
													<tbody>
														<tr>
															<th scope="row">
																<label for="coptHodrNameD${idx }" class="necessary">성명(법인명)</label>
															</th>
															<td>
																<input type="text" name="coptHodrName" id="coptHodrNameD${idx }" 
																	value="${list.COPTHODRNAME }" 
																	class="w98" title="성명(법인명)" rangeSize="0~200" nullCheck>
															</td>
															<th scope="row">
																<label for="coptHodrTelxNumbD${idx }" class="necessary">전화번호</label>
															</th>
															<td>
																<input type="text" name="coptHodrTelxNumb" id="coptHodrTelxNumbD${idx }" 
																	value="${list.COPTHODRTELXNUMB }" 
																	class="w98" title="전화번호" rangeSize="0~20" nullCheck>
															</td>
														</tr>
														<tr>
															<th scope="row">
																<label for="cpotHodrAddrD${idx }" class="necessary">주소</label>
															</th>
															<td colspan="3">
																<input type="text" name="coptHodrAddr" id="coptHodrAddrD${idx }" 
																	value="${list.COPTHODRADDR }" 
																	class="w98" title="주소" rangeSize="0~200" nullCheck>
															</td>
														</tr>
													</tbody>
												</table>
											</Td>
										</TR>
										<TR name="trTab4" id="trTab4D${idx }" style="display:${display };">
											<TH scope="row"><label for="" class="necessary">신청물의 내용</label></TH>
												<Td>
													<textarea cols="10" name="worksDesc" id="worksDescD${idx }" class="w99" rows="5" title="신청물의 내용" rangeSize="0~4000" nullCheck>${list.WORKSDESC}</textarea>
												</Td>
											</TH>
										</TR>
										<c:set var="idx" value="${idx+1 }"/>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
						</form>
						<!-- 이용 승인신청 명세서 정보 end -->
						
						<!-- button area str -->
						<div class="btnArea">
							<p class="fl">
								<span class="button medium gray"><a href="#1" onclick="fn_goList();" onkeypress="fn_goList();">취소</a></span>
								<span class="button medium gray"><a href="#1" onclick="fn_goStep1();" onkeypress="fn_goStep1();">이전단계</a></span>
							</p>
							<p class="fr">
								<span class="button medium"><a href="#1" onclick="fn_doSave('goList');" onkeypress="fn_doSave('goList');">임시저장</a></span> 
								<span class="button medium"><a href="#1" onclick="fn_doSave('goStep3');" onkeypress="fn_doSave('goStep3');">작성내용 확인</a></span>
							</p>
						</div>
						<!-- button area end -->
					</div>
					
				</div>
				<!-- //주요컨텐츠 end -->
				
			</div>
			
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2011/footer.jsp" /> --%>
		<!-- //FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	
<script type="text/JavaScript">
<!--
	window.onload = function() {
						<c:if test="${not empty statApplyWorksList}">
							//신청서 구분에 값 맵핑
		    				<c:set var="dySeq" value="0"/>
			    			<c:forEach items="${statApplyWorksList}" var="statApplyWorksList">
			    				<c:if test="${statApplyWorksList.APPLYTYPE01 == '1'}">
			    					document.getElementById("dummyApplyType01_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE02 == '1'}">
			    					document.getElementById("dummyApplyType02_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE03 == '1'}">
			    					document.getElementById("dummyApplyType03_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE04 == '1'}">
			    					document.getElementById("dummyApplyType04_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE05 == '1'}">
			    					document.getElementById("dummyApplyType05_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:set var="dySeq" value="${dySeq + 1}"/>
			    			</c:forEach>
						</c:if>
						<c:if test="${empty statApplyWorksList}">
							fn_addTab();
						</c:if>
						
					}
-->
</script>
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>
