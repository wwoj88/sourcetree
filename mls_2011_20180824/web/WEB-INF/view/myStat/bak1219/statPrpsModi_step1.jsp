<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>법정허락 | 저작권찾기</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css"> 
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>	
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/JavaScript">
<!--
//취소
function fn_goList(){
	var frm = document.frm;

	if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.encoding = "application/x-www-form-urlencoded";
		frm.action = "/myStat/myStat.do?method=statRsltInqrList";
		frm.submit();
	}
}

function openSmplDetail(div) {

	var param = '';
	
	param = 'DVI='+div;
	
	var url = '/common/rghtPrps_smpl.jsp?'+param
	var name = '';
	var openInfo = 'target=rghtPrps_mvie width=705, height=570, scrollbars=yes';
	
	window.open(url, name, openInfo);
}
//신청자, 대리인 정보 맵핑
function fn_fillDate(oObj, sDiv) {
	var frm = document.frm;

	var dummyResdCorpNumb = '';
	var resdCorpNumb = '';
	<c:choose>
		<c:when test="${userInfo.USER_DIVS == '01' }">
			<c:if test="${userInfo.RESD_CORP_NUMB != null || userInfo.RESD_CORP_NUMB != ''}">
					dummyResdCorpNumb = '${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 0, 6) }-${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 6, 13) }';
					resdCorpNumb = '${userInfo.RESD_CORP_NUMB1 }-${userInfo.RESD_CORP_NUMB2 }';
			</c:if>
		</c:when>
		<c:otherwise>
			dummyResdCorpNumb = '${fn:substring(userInfo.CORP_NUMB, 0, 3)}-${fn:substring(userInfo.CORP_NUMB, 3, 5)}-${fn:substring(userInfo.CORP_NUMB, 5, 10)}';
			resdCorpNumb = '${fn:substring(userInfo.CORP_NUMB, 0, 3)}-${fn:substring(userInfo.CORP_NUMB, 3, 5)}-${fn:substring(userInfo.CORP_NUMB, 5, 10)}';
		</c:otherwise>
	</c:choose>
	
	if(oObj.checked) {
		if(sDiv == 'APPLR') {
			//신청자
			frm.applrName.value = '${userInfo.USER_NAME }';
			frm.dummyApplrResdCorpNumb.value = dummyResdCorpNumb;
			frm.applrResdCorpNumb.value = resdCorpNumb;
			frm.applrAddr.value = '${userInfo.HOME_ADDR }';
			frm.applrTelx.value = '${userInfo.TELX_NUMB }';
		
		} else {
			//대리인
			frm.applyProxyName.value = '${userInfo.USER_NAME }';
			frm.dummyApplyProxyResdCorpNumb.value = dummyResdCorpNumb;
			frm.applyProxyResdCorpNumb.value = resdCorpNumb;
			frm.applyProxyAddr.value = '${userInfo.HOME_ADDR }';
			frm.applyProxyTelx.value = '${userInfo.TELX_NUMB }';
		
		}
	} else {
		if(sDiv == 'APPLR') {
			//신청자
			frm.applrName.value = '';
			frm.dummyApplrResdCorpNumb.value = '';
			frm.applrResdCorpNumb.value = '';
			frm.applrAddr.value = '';
			frm.applrTelx.value = '';
		
		} else {
			//대리인
			frm.applyProxyName.value = '';
			frm.dummyApplyProxyResdCorpNumb.value = '';
			frm.applyProxyResdCorpNumb.value = '';
			frm.applyProxyAddr.value = '';
			frm.applyProxyTelx.value = '';
		
		}
	}
}

//신청서 구분(신청서명) 동기화
function fn_chkSycn(oObj, sNo) {
	var oChkApplyType = document.getElementsByTagName("input");
	for(i = 0; i < oChkApplyType.length; i++) {
		if(oChkApplyType[i].type == "checkbox") {
			
			if( ((oChkApplyType[i].id).toUpperCase()).indexOf("APPLYTYPE0") > -1 && oChkApplyType[i].id != oObj.id) {
				oChkApplyType[i].checked = false;
			}
		}
	}

	document.getElementById("dummyApplyType0"+sNo).checked = oObj.checked;

}

//저작권법 항목 
function fn_chkRawCd(sNo, sChk) {
	var oObjs = document.getElementsByName("dummyApplyRawCd");
	
	for(var i = 0; i < oObjs.length; i++) {
		if(oObjs[i].value == sNo &&sChk == true) {
			oObjs[i].checked = true;
		} else {
			oObjs[i].checked = false;
		}
	}
	/*
	document.getElementById("applyRawCd").value = sNo;
	*/
	document.getElementById("applyRawCd").value = "";
}

/* 첨부화일 관련 시작 */
//첨부화일 테이블 idx
var iRowIdx = 0;

//순번 재지정1
function fn_resetSeq1(){
    var oSeq = document.getElementsByName("fileNameCd");
    for(i=0; i<oSeq.length; i++){
        oSeq[i].value = i+1;
    }
}

//첨부화일 추가
function fn_addRow1(){
    var oTbl = document.getElementById("tblAttachFile");
    var oTbody = oTbl.getElementsByTagName("TBODY")[0];
    
    var oTr = document.createElement("TR");
    
	//순번(선택)
	var oTd = document.createElement("TD");
	var sTag = '<input name=\"chkDel1\" id=\"chkDel1_'+iRowIdx+'\" type=\"checkbox\" />';
	sTag += '<input name=\"fileNameCd\" id=\"fileNameCd_'+iRowIdx+'\" type=\"hidden\" value=\"99\" style=\"border:0px;\" readonly=\"readonly\"  />';
	oTd.innerHTML = sTag;
	oTd.style.width = '7%';
	oTd.className = 'ce';
	oTr.appendChild(oTd);
	
	//서류구분
	oTd = document.createElement("TD");
	oTd.innerHTML = '기타';
	oTd.style.width = '35%';
	oTr.appendChild(oTd);
	
	//첨부파일명
	oTd = document.createElement("TD");
	oTd.style.width = '18%';
	oTr.appendChild(oTd);
	  
	//첨부화일명
	oTd = document.createElement("TD");
	sTag = '<input type=\"hidden\" name=\"fileDelYn\" id=\"fileDelYn_'+iRowIdx+'\" />';
	sTag += '<span id=\"spfile'+iRowIdx+'\"><input type=\"file\" name=\"file'+iRowIdx+'\" id=\"file_'+iRowIdx+'\" class=\"inputData L w10\" style=\"width: 200px;\" onkeydown=\"return false;\" onchange=\"fn_chkFileType(this,'+iRowIdx+');\"/></span>';
	sTag += '<input name=\"hddnFile\" id=\"hddnFile_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
	sTag += '<input name=\"fileName\" id=\"fileName_'+iRowIdx+'\" type=\"text\" onkeyup=\"onkeylengthMax(this, 255, this.name);\" class=\"inputDate w95\" style=\"display:none;\" />';
	
	sTag += '<input name=\"realFileName\" id=\"realFileName_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
	sTag += '<input name=\"fileSize\" id=\"fileSize_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
	sTag += '<input name=\"filePath\" id=\"filePath_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
	oTd.innerHTML = sTag;
	oTr.appendChild(oTd);
	
	oTbody.appendChild(oTr);
	iRowIdx++;
	       
}

//첨부화일 삭제
function fn_delRow1(){
    var oTbl = document.getElementById("tblAttachFile");
    var oChkDel = document.getElementsByName("chkDel1");
    var iChkCnt = oChkDel.length;
    var iDelCnt = 0;
    
    if(iChkCnt == 1 && oChkDel[0].checked == true){
       	//수정시 기타 관련 파일의 tr에 ID가 존재함
       	//해당 체크 박스의 idx와 동일한 tr ID가 존재하면 해당 파일은 기존에 입력 되었던 파일
    	var oTr = document.getElementById("trAttcFile_D0");
       	if(oTr == null) {	//해당 ID를 가진 tr 이 없으면 현재 화면에서 새로 추가한 기타 파일
	        oTbl.deleteRow(6);
	        
        }else{	//해당 ID를 가진 tr이 있으면 기존에 입력 되었던 파일
        	oTr.style.display = "none";		//화면상에서 보이지만 않게 함
        	document.getElementById("fileDelYn_D0").value = "Y";	//파일 처리 구분을 삭제로 셋팅
        	oChkDel[0].checked = false;		//체크박스 체크 해제
        }
        iDelCnt++;
    }else if(iChkCnt > 1){
   	    for(i = iChkCnt-1; i >= 0; i--){
   	        if(oChkDel[i].checked == true){
   	        	//수정시 기타 관련 파일의 tr에 ID가 존재함
   	        	//해당 체크 박스의 idx와 동일한 tr ID가 존재하면 해당 파일은 기존에 입력 되었던 파일
   	        	var oTr = document.getElementById("trAttcFile_D"+(oChkDel[i].id).substr((oChkDel[i].id).indexOf('_')+1, (oChkDel[i].id).length));
   	        	if(oTr == null) {	//해당 ID를 가진 tr 이 없으면 현재 화면에서 새로 추가한 기타 파일
	   	            oTbl.deleteRow(i+6);
	   	            
   	            }else{	//해당 ID를 가진 tr이 있으면 기존에 입력 되었던 파일
		        	oTr.style.display = "none";		//화면상에서 보이지만 않게 함
		        	document.getElementById("fileDelYn_D"+(oChkDel[i].id).substr((oChkDel[i].id).indexOf('_')+1, (oChkDel[i].id).length)).value = "Y";	//파일 처리 구분을 삭제로 셋팅
		        	oChkDel[i].checked = false;		//체크박스 체크 해제
		        }
   	            iDelCnt++;
	        }
   	    }
   	}
   	
   	if(iDelCnt < 1){
   	    alert('삭제할 첨부화일을 선택하여 주십시요');
   	}
}

//첨부화일 확장자 제한
function fn_chkFileType(obj,iRowIdx){
	var refuseFile = ["HTM","HTML","PHP","PHP3","ASP","JSP","CGI","INI","PL"];
	var str = obj.value;
	var nPos = 0;
	var sepCnt = 0;
	var isAlert = false;
	var msg = "";
	
	//확장자 구하기
	if( str != null ){
		var splitLength = (str.split(".")).length;		
		str = str.split(".")[splitLength-1];
	}

	//확장자 구하기
	/*
	while(nPos >= 0){
		nPos = str.indexOf(".");
		if(nPos> 0){
			str = str.substring(nPos+1,str.length);
		}
		sepCnt++;
	}
	*/
	 
	for(i=0; i<refuseFile.length; i++){
		if(msg.length > 0){
			msg += ", ";
		}
		msg += refuseFile[i];
		
		if(str.toUpperCase() == refuseFile[i]){
			isAlert = true;
		}
	}
	
	if(sepCnt > 2 || isAlert == true){
		alert(msg + "확장자를 가진 화일이나 \n\r이중확장자(\"---.---.---\") 화일은 올리실 수 없습니다. ");
		
	 	var spObj = document.getElementById("spfile"+iRowIdx);
	  	spObj.outerHTML = '<span id=\"spfile'+iRowIdx+'\"><input type=\"file\" name=\"file\" id=\"file_'+iRowIdx+'\" class=\"inputData L w10\" style=\"width: 200px;\" onkeydown=\"return false;\" onchange=\"fn_chkFileType(this,'+iRowIdx+');\"/></span>';
	  	return false;
	}
	
	if(iRowIdx == 'D1') {
		frm.existYn.value = 'Y';
	}
	
	document.getElementById("fileDelYn_"+iRowIdx).value = "Y";
}
/* 첨부화일 관련 끝*/

//table row 별 필수체크
function checkTr(oTr) {
	var result = ""; 
	if(oTr.id == "applrInfo1"){
		//alert(oTr.id);
		var oInput = oTr.getElementsByTagName("input");
		for(z = 0; z<oInput.length; z++){
			if(checkField2(oInput[z]) == false){
				result = oInput[z].id;
				break;
			}else{
				
			}		
		}
	}
	if(oTr.id == "applrInfo2"){
		var oTextArea = oTr.getElementsByTagName("textarea");
		var oInput = oTr.getElementsByTagName("input");
		for(z = 0; z<oTextArea.length; z++){
			if(checkField2(oTextArea[z]) == false){
				result = oTextArea[z].id;
				break;
			}else{
				for(z = 0; z<oInput.length; z++){
					if(checkField2(oInput[z]) == false){
						result = oInput[z].id;
						break;
					}		
				}
			}		
		}
	}
	return result;
}

function checkForm3(forms) {
	var result = ""; 

	if(typeof(forms) != 'undefined') {
		for(y = 0; y<forms.length; y++) {
			if(checkField2(forms[y]) == false)  {
				result = forms[y].id;
				break;
			}
		}
	}
	else
		result = "";
		
	return result;
}


//필수체크
function fn_chkValue() {
	var frm = document.frm;
	
	//신청인정보 체크
	var oTr = document.getElementById("applrInfo1");
	tgtId = checkTr(oTr);
	
	if(tgtId != "") {
		document.getElementById(tgtId).focus();
		return false;
	}
	//신청인 주민/사업자번호 체크
	var sApplrResdCorpNumb = document.getElementById("applrResdCorpNumb").value;
	if(sApplrResdCorpNumb !=""){
		while(sApplrResdCorpNumb.indexOf('-') > -1){
			sApplrResdCorpNumb = sApplrResdCorpNumb.replace('-', '');
		}
		if(sApplrResdCorpNumb.length == 10){
			/*if(!check_busino(sApplrResdCorpNumb)){
				document.getElementById("dummyApplrResdCorpNumb").focus();
		  		alert("유효하지 않은 사업자등록번호입니다.");
		  		return;
			}*/
			if(!check_busino(sApplrResdCorpNumb)){
				document.getElementById("dummyApplrResdCorpNumb").focus();
		  		alert("유효하지 않은 법인등록번호입니다.");
		  		return;
			}
		} else if(sApplrResdCorpNumb.length == 13){
			var sStr1 = sApplrResdCorpNumb.substr(0,6);
			var sStr2 = sApplrResdCorpNumb.substr(6,7);
			if(ssnCheck3(sStr1, sStr2)){
				document.getElementById("dummyApplyProxyResdCorpNumb").focus();
				return;
			}
		} else {
			document.getElementById("dummyApplrResdCorpNumb").focus();
			alert("유효하지 않은 주민등록번호입니다.");
			return;
		}
	}
	
	var oTr = document.getElementById("applrInfo2");
	tgtId = checkTr(oTr);
	
	if(tgtId != "") {
		document.getElementById(tgtId).focus();
		return false;
	}
	
	
	
	//대리인 주민/사업자번호 체크
	var sApplyProxyResdCorpNumb = document.getElementById("applyProxyResdCorpNumb").value;
	if(sApplyProxyResdCorpNumb !=""){
		while(sApplyProxyResdCorpNumb.indexOf('-') > -1){
			sApplyProxyResdCorpNumb = sApplyProxyResdCorpNumb.replace('-', '');
		}
		if(sApplyProxyResdCorpNumb.length == 10){
			/*if(!check_busino(sApplyProxyResdCorpNumb)){
				document.getElementById("dummyApplyProxyResdCorpNumb").focus();
		  		alert("유효하지 않은 사업자등록번호입니다.");
		  		return;
			}*/
			if(!check_busino(sApplyProxyResdCorpNumb)){
				document.getElementById("dummyApplyProxyResdCorpNumb").focus();
		  		alert("유효하지 않은 법인등록번호입니다.");
		  		return;
			}
		} else if(sApplyProxyResdCorpNumb.length == 13){
			var sStr1 = sApplyProxyResdCorpNumb.substr(0,6);
			var sStr2 = sApplyProxyResdCorpNumb.substr(6,7);
			if(ssnCheck3(sStr1, sStr2)){
				document.getElementById("dummyApplyProxyResdCorpNumb").focus();
				return;
			}
		} else {
			document.getElementById("dummyApplyProxyResdCorpNumb").focus();
			alert("유효하지 않은 주민등록번호입니다.");
			return;
		}
	}
	

	var chkCnt = 0;
	var focusTgt = '';
	
	//신청서 구분 체크여부 확인
	<c:set var="dySeq" value="${0}"/>
	<c:forEach items="${applyTypeList}" var="applyTypeList">
		<c:set var="dySeq" value="${dySeq + 1}"/>
		if('${dySeq}' == 1) {
			focusTgt = 'applyType0${dySeq }';
		}
		if(document.getElementById("applyType0${dySeq }").checked) {
			chkCnt++;
		}
	</c:forEach>
	if(chkCnt == 0) {
		alert("신청서 구분을(를) 선택하세요.");
		document.getElementById(focusTgt).focus();
		return false;
	}
	
	//폼체크
	var tgtId = checkForm3(frm);
			
	if(tgtId != "") {
	
		document.getElementById(tgtId).focus();
		return false;
	}
	
	//저작권법 값 셋팅
	var oObjs = document.getElementsByName("dummyApplyRawCd");
	chkCnt = 0;
	for(var i = 0; i < oObjs.length; i++) {
		if(oObjs[i].checked == true) {
			document.getElementById("applyRawCd").value = oObjs[i].value;
			chkCnt++;
		}
	}
	if(chkCnt == 0) {
		alert("저작권법을(를) 선택하세요.");
		document.getElementById(oObjs[0].id).focus();
		return false;
	}
		
	//첨부화일
	var oInput = document.getElementsByTagName("input");
	var arrVal = new Array();
	var arrCnt = 0;
	for(i=0; i<oInput.length; i++){
		var tmpId = oInput[i].id;
		if(tmpId.substring(0,5) == "file_"){
			var tmpVal = oInput[i].value;
			var sep = 0;
			for(j=0; j<tmpVal.length; j++){
		        if(tmpVal.charCodeAt(j) == 92){	// 아스키 92 = '\'
		            sep = j;
		        }
		    }
			arrVal[arrCnt] = tmpVal.substring(sep,tmpVal.length);
			arrVal[arrCnt] = arrVal[arrCnt].replace("\\","");
			
			arrCnt++;
		}
	}
	arrVal.length = arrCnt;
	
	var oHddnFile = document.getElementsByName("hddnFile");
	for(i=0; i<oHddnFile.length; i++){
		/*
		if(arrVal[i].length < 1){
			alert('파일명을 입력하세요');
			return false;
		}
		*/
		oHddnFile[i].value = arrVal[i];
	}

	return true;
}

//임시저장 및 화면이동
function fn_doSave(sDiv) {
	var frm = document.frm;
	var nowStatCd = '${statApplication.statCd }';
	frm.action_div.value = sDiv;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		if(fn_chkValue()) {
		
			if(sDiv == 'goStep2' && frm.applyWorksCnt.value > 5 && frm.existYn.value != 'Y') {
				alert('신청건이 5건 이상일 경우,\n\r \'이용승인명세서\' 관련 첨부파일을 첨부하셔야 합니다.');
				return;
			}
		
			frm.target = "_self";
			frm.method = "post";
			frm.action = "/myStat/myStat.do?method=tmpStatPrps1SaveDo";
   			if(nowStatCd== '3' || nowStatCd== '4'){
    			frm.action = "/myStat/myStat.do?method=statPrps1SaveDo";
   			}
			
			frm.submit();
		}
	}
}

function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.fileFrm;

	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;
	
	frm.method = "post";
	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
}

function fn_matchResdCorpNumb(oObj) {
	var frm = document.frm;

	if(oObj.id == 'dummyApplrResdCorpNumb') {
		frm.applrResdCorpNumb.value = oObj.value;
	}
	if(oObj.id == 'dummyApplyProxyResdCorpNumb') {
		frm.applyProxyResdCorpNumb.value = oObj.value;
	}
}

	
function fn_goSample(){
	window.open("/images/2011/smpl/stat_step1.jpg", "", "width=770,height=850,scrollbars=yes");
}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2>
					<span>
						<img src="/images/2011/title/container_vis_h2_8.gif" alt="마이페이지" title="마이페이지" />
						<em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em>
					</span>
				</h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
				<iframe id="goLogin" title="법정허락 공인인증서 설치 아이프레임" 
					src="/include/sg_install.html" 
					frameborder="0" width="242" height="139" scrolling="no" 
					marginwidth="0" marginheight="0" style="display:none;"></iframe>
			
				<!-- 래프 -->
				<jsp:include page="/include/2011/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1");
					subSlideMenu("sub_lnb","lnb13");
				</script>
				<!-- //래프 -->
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>신청현황</span><em>법정허락</em></p>
					<h1><img src="/images/2011/title/content_h1_0807.gif" alt="법정허락" title="법정허락" /></h1>
					<jsp:include page="/common/memo/2011/memo_01.jsp">
						<jsp:param name="DIVS" value="MS" />
					</jsp:include>	
					<div class="section mt20">
						<div class="usr_process stat_process">
							<div class="process_box">
								<ul class="floatDiv">
								<li class="fl ml0 on"><img alt="1단계 약관동의" src="/images/2011/content/process21_on.gif"></li>
								<li class="fl"><img alt="2단계 회원정보입력" src="/images/2011/content/process22_off.gif"></li>
								<li class="fl bgNone pr0"><img alt="3단계 가입완료" src="/images/2011/content/process23_off.gif"></li>
								</ul>
							</div>
						</div>

						<form name="fileFrm" action="#">
							<input type="hidden" name="filePath">
							<input type="hidden" name="fileName">
							<input type="hidden" name="realFileName">
							<input type="submit" style="display:none;">
						</form>

						<form name="frm" action="#" enctype="multipart/form-data">
							<input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
							<input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
							<input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
							<input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
							<input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
							<input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
							<input type="hidden" name="action_div" value=""/>
							
							<input type="hidden" name="applyWriteYmd" value="${statApplication.applyWriteYmd }">
							<input type="hidden" name="applyWriteSeq" value="${statApplication.applyWriteSeq }">
							<input type="submit" style="display:none;">
							
						<div class="article mt20">

							<!-- 회원 정보 str -->
							<h2>회원 정보</h2>							
							<span class="topLine"></span>
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
								<colgroup>
								<col width="20%">
								<col width="30%">
								<col width="27%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">성명(법인명)</th>
										<td>${userInfo.USER_NAME }</td>
										<th scope="row">주민등록번호(법인등록번호)</th>
										<td>
											<c:choose>
												<c:when test="${userInfo.USER_DIVS == '01' }">
													<!-- 
													<c:if test="${userInfo.RESD_CORP_NUMB != null || userInfo.RESD_CORP_NUMB != ''}">
															${userInfo.RESD_CORP_NUMB1 }-${userInfo.RESD_CORP_NUMB2 }
													</c:if>
													-->
													<c:if test="${userInfo.RESD_CORP_NUMB != null || userInfo.RESD_CORP_NUMB != ''}">
														${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 0, 6) }-${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 6, 13) }
													</c:if>
												</c:when>
												<c:otherwise>
													${fn:substring(userInfo.CORP_NUMB, 0, 3)}-${fn:substring(userInfo.CORP_NUMB, 3, 5)}-${fn:substring(userInfo.CORP_NUMB, 5, 10)}
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<th scope="row">주소</th>
										<td>${userInfo.HOME_ADDR }</td>
										<th scope="row">전화번호</th>
										<td>
											${userInfo.TELX_NUMB }
											<input type="hidden" name="rgstIdnt" value="${userInfo.USER_IDNT }"/>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- 회원 정보 end -->
							
							<!-- 신청인 정보 str -->
							<h2 class="mt20">신청인 정보<span class="thin p11 ml10"> (<input type="checkbox" id="chkApplr" onclick="fn_fillDate(this, 'APPLR');" onkeypress="fn_fillDate(this, 'APPLR');"/><label for="chkApplr">회원정보와 동일</label>)</span></h2>
							<span class="topLine"></span>
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col width="30%">
								<col width="27%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr id="applrInfo1">
										<th scope="row"><label for="applrName" class="necessary">성명(법인명)</label></th>
										<td><input type="text" id="applrName" name="applrName" value="${statApplication.applrName }" rangeSize="0~200" title="성명(법인명)" nullCheck /></td>
										<th scope="row"><label for="dummyApplrResdCorpNumb" class="necessary">주민등록번호(법인등록번호)</label></th>
										<td>
											<input type="text" id="dummyApplrResdCorpNumb" name="dummyApplrResdCorpNumb" 
												value="${statApplication.dummyApplrResdCorpNumb }" 
												rangeSize="0~20" title="주민등록번호(법인등록번호)" nullCheck
												onchange="fn_matchResdCorpNumb(this)"/>
											<input type="hidden" id="applrResdCorpNumb" name="applrResdCorpNumb" 
												value="${statApplication.applrResdCorpNumb }" />
										</td>
									</tr>
									<tr id="applrInfo2">
										<th scope="row"><label for="applrAddr" class="necessary">주소</label></th>
										<td><textarea cols="10" class="inputData w90" style="height: 30px !important" rows="2" id="applrAddr" name="applrAddr" title="주소" rangeSize="0~200" nullCheck>${statApplication.applrAddr }</textarea></td>
										<!--<td><input type="text" id="applrAddr" name="applrAddr" class="w98" value="${statApplication.applrAddr }" rangeSize="0~200" title="주소" nullCheck /></td> -->
										<th scope="row"><label for="applrTelx" class="necessary">전화번호</label></th>
										<td><input type="text" id="applrTelx" name="applrTelx" value="${statApplication.applrTelx }" rangeSize="0~20" title="전화번호" nullCheck /></td>
									</tr>
								</tbody>
							</table>
							<!-- 신청인 정보 end -->
							
							<!-- 대리인 정보 str -->
							<h2 class="mt20">대리인 정보<span class="thin p11 ml10"> (<input type="checkbox" id="chkApplyProxy" onclick="fn_fillDate(this, 'APPLYPROXY');" onkeypress="fn_fillDate(this, 'APPLYPROXY');" /><label for="chkApplyProxy">회원정보와 동일</label>)</span></h2>
							<span class="topLine"></span>
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col width="30%">
								<col width="27%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="applyProxyName">성명(법인명)</label></th>
										<td><input type="text" id="applyProxyName" name="applyProxyName" value="${statApplication.applyProxyName }" rangeSize="0~200"/></td>
										<th scope="row"><label for="dummyApplyProxyResdCorpNumb">주민등록번호(법인등록번호)</label></th>
										<td>
											<input type="text" id="dummyApplyProxyResdCorpNumb" name="dummyApplyProxyResdCorpNumb" 
												value="${statApplication.dummyApplyProxyResdCorpNumb }" rangeSize="0~20"
												onchange="fn_matchResdCorpNumb(this)"/>
											<input type="hidden" id="applyProxyResdCorpNumb" name="applyProxyResdCorpNumb" 
												value="${statApplication.applyProxyResdCorpNumb }">
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="applyProxyAddr">주소</label></th>
										<td><textarea cols="10" class="inputData w90" style="height: 30px !important" rows="2" id="applyProxyAddr" name="applyProxyAddr" title="주소" rangeSize="0~200">${statApplication.applyProxyAddr }</textarea></td>
										<!--<td><input type="text" id="applyProxyAddr" name="applyProxyAddr" class="w98" value="${statApplication.applyProxyAddr }" rangeSize="0~200"/></td>-->
										<th scope="row"><label for="applyProxyTelx">전화번호</label></th>
										<td><input type="text" id="applyProxyTelx" name="applyProxyTelx" value="${statApplication.applyProxyTelx }" rangeSize="0~20"/></td>
									</tr>
								</tbody>
							</table>
							<!-- 대리인 정보 end -->
							
							<!-- 이용 승인신청 정보 str -->
							<div class="floatDiv mt20">
								<h2 class="fl">이용 승인신청 정보</h2>
								<p class="fr"><span class="button small icon"><a href="#1" onclick="openSmplDetail('MS');" onkeypress="openSmplDetail('MS');">예시화면 보기</a><span class="help"></span></span></p>
							</div>
							
							<span class="topLine"></span>
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid" style="table-layout:fixed;">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<td colspan="3" rowspan="2">
											<c:set var="dySeq" value="0"/>
											<c:forEach items="${applyTypeList}" var="applyTypeList">
												<c:set var="dySeq" value="${dySeq + 1}"/>
												<input type="checkbox" id="applyType0${dySeq }" title="선택" name="applyType0${dySeq }" value="1"
													onclick="fn_chkSycn(this, '${dySeq }');" onkeypress="fn_chkSycn(this, '${dySeq }');">
												<label for="applyType0${dySeq }" class="p12">${applyTypeList.codeName }</label>&nbsp;
											</c:forEach>
											<span style="color:#000;margin-left: 15px;font-size:16pt;color:#000;font-weight:bold;">이용 승인신청서</span>
										</td>
										<th scope="row" style="text-align: center;"><label for="">처리기간</label></th>
									</tr>
									<tr>
										<td style="text-align: center;">40일</td>
									</tr>
									<tr>
										<th scope="row"><label for="applyWorksTitl" class="necessary">제호(제목)</label></th>
										<td colspan="3">
											<p class="mb5">
												<input type="checkbox" title="선택" id="multiYn" name="multiYn" value="Y" <c:if test="${statApplication.applyWorksCnt > 0}">checked="checked"</c:if> />
												<label for="multiYn" class="p12">여러건 신청 : 총 <input type="text" title="총건수" id="applyWorksCnt" name="applyWorksCnt" size="5" value="${statApplication.applyWorksCnt }"  character="EK"/>건</label>
												<p class="p11 orange mb5">(* 신청건이 5건을 초과시 '2단계 이용 승인신청 명세서 작성'을 첨부파일로 대체합니다.)</p>
											</p>
											<textarea cols="10" class="inputData w98" rows="1" id="applyWorksTitl" name="applyWorksTitl" title="제목(제호)" rangeSize="0~500" nullCheck>${statApplication.applyWorksTitl }</textarea>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="applyWorksKind" class="necessary">종류</label></th>
										<td><input type="text" id="applyWorksKind" name="applyWorksKind" value="${statApplication.applyWorksKind }" title="종류" rangeSize="0~100" nullCheck/></td>
										<th scope="row"><label for="applyWorksForm" class="necessary">형태 및 수량</label></th>
										<td><input type="text" id="applyWorksForm" name="applyWorksForm" value="${statApplication.applyWorksForm }" title="형태 및 수량" rangeSize="0~100" nullCheck/></td>
									</tr>
									<tr>
										<th scope="row"><label for="usexDesc" class="necessary">이용의 내용</label></th>
										<td colspan="3">
											<p class="p11 orange mb5">(* 이용하고자 하는 목적, 방법 등을 상세히 기재)</p>
											<textarea cols="10" class="inputData w98" rows="5" style="height: 70px !important" id="usexDesc" name="usexDesc" title="이용의 내용" rangeSize="0~4000" nullCheck>${statApplication.usexDesc }</textarea>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="applyReas" class="necessary">승인신청사유</label></th>
										<td colspan="3">
											<p class="p11 orange mb5">(* ‘권리자 거소 확인 불가’ 등의 해당 사유를 기재)</p>
											<textarea cols="10" class="inputData w98" rows="5" style="height: 70px !important" id="applyReas" name="applyReas" title="승인신청사유" rangeSize="0~4000" nullCheck>${statApplication.applyReas }</textarea>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="cpstAmnt" class="necessary">보상금액 </label></th>
										<td colspan="3"><input type="text" id="cpstAmnt" name="cpstAmnt" value="${statApplication.cpstAmnt }" title="보상금액" nullCheck rangeSize="0~15"/></td>
									</tr>
									<tr>
										<td colspan="4">
											<div class="floatDiv ml100">
												<span class="fl inBlock mt30">저작권법</span> 
												<p class="fl ml10 black">
												<input type="checkbox" name="dummyApplyRawCd" id="dummyApplyRawCd50" value="50" 
													<c:if test="${statApplication.applyRawCd == '50' }"> checked="checked" </c:if>
													onclick="fn_chkRawCd(this.value, this.checked);" 
													onkeypress="fn_chkRawCd(this.value, this.checked);"><label class="p12" for="dummyApplyRawCd50"> 제 50조</label><br />
												<input type="checkbox" name="dummyApplyRawCd" id="dummyApplyRawCd51" value="51" 
													<c:if test="${statApplication.applyRawCd == '51' }"> checked="checked" </c:if>
													onclick="fn_chkRawCd(this.value, this.checked);" 
													onkeypress="fn_chkRawCd(this.value, this.checked);"><label class="p12" for="dummyApplyRawCd51"> 제 51조</label><br />
												<input type="checkbox" name="dummyApplyRawCd" id="dummyApplyRawCd52" value="52" 
													<c:if test="${statApplication.applyRawCd == '52' }"> checked="checked" </c:if>
													onclick="fn_chkRawCd(this.value, this.checked);" 
													onkeypress="fn_chkRawCd(this.value, this.checked);"><label class="p12" for="dummyApplyRawCd52"> 제 52조</label><br />
												<input type="checkbox" name="dummyApplyRawCd" id="dummyApplyRawCd89" value="89" 
													<c:if test="${statApplication.applyRawCd == '89' }"> checked="checked" </c:if>
													onclick="fn_chkRawCd(this.value, this.checked);" 
													onkeypress="fn_chkRawCd(this.value, this.checked);"><label class="p12" for="dummyApplyRawCd89"> 제 89조</label><br />
												<input type="checkbox" name="dummyApplyRawCd" id="dummyApplyRawCd97" value="97" 
													<c:if test="${statApplication.applyRawCd == '97' }"> checked="checked" </c:if>
													onclick="fn_chkRawCd(this.value, this.checked);" 
													onkeypress="fn_chkRawCd(this.value, this.checked);"><label class="p12" for="dummyApplyRawCd97"> 제 97조</label><br />
												<input type="hidden" name="applyRawCd" id="applyRawCd" value="${statApplication.applyRawCd }" title="저작권법"/>
												</p>
												<span class="fl inBlock mt30 ml10">에 따라 위와같이 </span> 
												<p class="fl ml10 black">
												<c:set var="dySeq" value="0"/>
												<c:forEach items="${applyTypeList}" var="applyTypeList">
													<c:set var="dySeq" value="${dySeq + 1}"/>
													<input type="checkbox" title="선택" id="dummyApplyType0${dySeq }" disabled="disabled">
													<label for="dummyApplyType0${dySeq }" class="p12">${applyTypeList.codeName }</label><br/>
												</c:forEach>
												</p>
												<span class="fl inBlock mt30 ml20">이용의 승인을 신청합니다. </span> 
											</div>
										</td>
									</tr>
								</tbody>
							</table>
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid" style="table-layout:fixed;">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="" class="necessary">첨부서류</label><!--<a href="#1" class="ml10 underline black2" onclick="javascript:toggleLayer('help_pop1');"><img src ="/images/2011/common/ic_find_id.gif" alt="" class="vtop"></a> --></th>
										<!-- 도움말 레이어 -->
										<div class="layer_pop w80" id="help_pop1" style="margin:515px 0 0 20px">
											<h1>첨부서류</h1>
											<div class="layer_con">
												<ul class="list1">
													<li class="p11">별지 제2호서식에 따른 이용승인신청명세서(저작물,실연,음반,방송,데이터베이스의 형태 및 내용이 명확하지 아니한 경우에는 그 견본,도면 또는 사진 등을 첨부하여야 합니다) 1부</li>
													<li class="p11">보상금액산정내역서 1부</li>
													<li class="p11">해당 저작물 등이 공표되었음을 밝힐 수 있는 서류 1부</li>
													<li class="p11">저작재산권자,저작인접권자 또는 데이터베이스제작자나 그의 거소를 알 수 없음을 밝힐 수 있는 서류(위 사유로 승인 신청하는 경우에 한정합니다) 1부</li>
													<li class="p11">협의에 관한 경과서류(협의가 성립되지 아니하여 승인 신청하는 경우에 한정합니다) 1부</li>
													<li class="p11">해당 음반이 우리나라에서 판매되어 3년이 경과하였음을 밝힐 수 있는 서류(법 제52조 및 법 제89조에 따라 승인 신청하는 경우에 한정합니다) 1부</li>
												</ul>
											</div>
											<a href="#1" onclick="javascript:toggleLayer('help_pop1');" class="layer_close"><img src="/images/2011/button/layer_close.gif" alt="" /></a>
										</div>
										<!-- //도움말 레이어 -->
										<td colspan="3">
											<!-- 첨부화일 시작 -->
					                        <div class="mReset">
												<div class="">
													<div class="fr mb5">
														<p>
															<span title="추가" class="button small orange"><button id="btnAdd" onkeypress="" onclick="javascript:fn_addRow1();" type="button">추가</button></span>
															<span title="삭제" class="button small orange"><button id="btnDel" onkeypress="" onclick="javascript:fn_delRow1();" type="button">삭제</button></span>
														</p>
													</div>
												</div>
												<div class="mb15">
					                                <table id="tblAttachFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부화일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
														<colgroup>
														    <col width="7%">
														    <col width="35%">
														    <col width="18%">
														    <col width="*%">
														</colgroup>
														<tbody>
														    <tr>
					    									    <th scope="row" class="ce">순번</th>
					    									    <th scope="row">서류구분</th>
					    									    <th scope="row" colspan="2" style="text-align: center;">첨부파일</th>
														    </tr>
														     <tr>
														    	<td class="ce">1</td>
														    	<td>이용승인신청명세서</td>
														    	<td>
														    		<c:set var="attcSeqn" value="0"/>
														    		<c:set var="existYn" value="N"/>
														    		<c:if test="${not empty statApplication.fileList}">
														    			<c:forEach items="${statApplication.fileList}" var="fileList">
															    			<c:if test="${fileList.fileNameCd == '1'}">
															    				<a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
															    					onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
																    				${fileList.fileName }
															    				</a>
														    					<c:set var="attcSeqn" value="${fileList.attcSeqn }"/>
														    					<c:set var="existYn" value="Y"/>
															    			</c:if>
														    			</c:forEach>
														    		</c:if>
												    				<input name="attcSeqn" id="attcSeqn_D1" value="${attcSeqn }" type="hidden" />
														    	</td>
														    	<td>
											    					<input name="fileNameCd" id="fileNameCd_D1" value="1" type="hidden" style="border:0px;" readonly="readonly"  />
															    	<input type="hidden" name="existYn" value="${existYn }"/>
															    	<input type="hidden" name="fileDelYn" id="fileDelYn_D1" />
																	<span id="spfileD1'">
																	    <input type="file" title="파일" name="fileD1" id="file_D1" 
																	        class="inputData L w10" style="width: 200px;" 
																	        onkeydown="return false;" onchange="fn_chkFileType(this,'D1');"/>
																	</span>
																	<input name="hddnFile" id="hddnFile_D1" type="hidden" style="display:none;" />
																	<input name="fileName" title="파일명" id="fileName_D1" type="text" 
																	    onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
																	<input name="realFileName" id="realFileName_D1" type="hidden" style="display:none;" />
																	<input name="fileSize" id="fileSize_D1" type="hidden" style="display:none;" />
																	<input name="filePath" id="filePath_D1" type="hidden" style="display:none;" />
														    	</td>
														    </tr>
														    <tr>
														    	<td class="ce">2</td>
														    	<td>보상금액산정내역서</td>
														    	<td>
														    		<c:set var="attcSeqn" value="0"/>
														    		<c:if test="${not empty statApplication.fileList}">
														    			<c:forEach items="${statApplication.fileList}" var="fileList">
															    			<c:if test="${fileList.fileNameCd == '2'}">
															    				<a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
															    					onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
																    				${fileList.fileName }
															    				</a>
														    					<c:set var="attcSeqn" value="${fileList.attcSeqn }"/>
															    			</c:if>
														    			</c:forEach>
														    		</c:if>
												    				<input name="attcSeqn" id="attcSeqn_D2" value="${attcSeqn }" type="hidden" />
														    	</td>
														    	<td>
															    	<input name="fileNameCd" id="fileNameCd_D2" value="2" type="hidden" style="border:0px;" readonly="readonly"  />
														    		<input type="hidden" name="fileDelYn" id="fileDelYn_D2" />
																	<span id="spfileD2'">
																	    <input type="file" title="첨부파일" name="fileD2" id="file_D2" 
																	        class="inputData L w10" style="width: 200px;" 
																	        onkeydown="return false;" onchange="fn_chkFileType(this,'D2');"/>
																	</span>
																	<input name="hddnFile" id="hddnFile_D2" type="hidden" style="display:none;" />
																	<input name="fileName" title="파일명" id="fileName_D2" type="text" 
																	    onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
																	<input name="realFileName" id="realFileName_D2" type="hidden" style="display:none;" />
																	<input name="fileSize" id="fileSize_D2" type="hidden" style="display:none;" />
																	<input name="filePath" id="filePath_D2" type="hidden" style="display:none;" />
														    	</td>
														    </tr>
														    <tr>
														    	<td class="ce">3</td>
														    	<td>저작물 공표여부 확인서류</td>
														    	<td>
														    		<c:set var="attcSeqn" value="0"/>
														    		<c:if test="${not empty statApplication.fileList}">
														    			<c:forEach items="${statApplication.fileList}" var="fileList">
															    			<c:if test="${fileList.fileNameCd == '3'}">
															    				<a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
															    					onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
																    				${fileList.fileName }
															    				</a>
														    					<c:set var="attcSeqn" value="${fileList.attcSeqn }"/>
															    			</c:if>
														    			</c:forEach>
														    		</c:if>
												    				<input name="attcSeqn" id="attcSeqn_D3" value="${attcSeqn }" type="hidden" />
														    	</td>
														    	<td>
															    	<input name="fileNameCd" id="fileNameCd_D3" value="3" type="hidden" style="border:0px;" readonly="readonly"  />
													    			<input type="hidden" name="fileDelYn" id="fileDelYn_D3" />
																	<span id="spfileD3'">
																	    <input type="file" title="첨부파일" name="fileD3" id="file_D3" 
																	        class="inputData L w10" style="width: 200px;" 
																	        onkeydown="return false;" onchange="fn_chkFileType(this,'D3');"/>
																	</span>
																	<input name="hddnFile" id="hddnFile_D3" type="hidden" style="display:none;" />
																	<input name="fileName" title="파일명" id="fileName_D3" type="text" 
																	    onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
																	<input name="realFileName" id="realFileName_D3" type="hidden" style="display:none;" />
																	<input name="fileSize" id="fileSize_D3" type="hidden" style="display:none;" />
																	<input name="filePath" id="filePath_D3" type="hidden" style="display:none;" />
														    	</td>
														    </tr>
														    <tr>
														    	<td class="ce">4</td>
														    	<td>거소 미확인여부 확인서류</td>
														    	<td>
														    		<c:set var="attcSeqn" value="0"/>
														    		<c:if test="${not empty statApplication.fileList}">
														    			<c:forEach items="${statApplication.fileList}" var="fileList">
															    			<c:if test="${fileList.fileNameCd == '4'}">
															    				<a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
															    					onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
																    				${fileList.fileName }
															    				</a>
														    					<c:set var="attcSeqn" value="${fileList.attcSeqn }"/>
															    			</c:if>
														    			</c:forEach>
														    		</c:if>
												    				<input name="attcSeqn" id="attcSeqn_D4" value="${attcSeqn }" type="hidden" />
														    	</td>
														    	<td>
															    	<input name="fileNameCd" id="fileNameCd_D4" value="4" type="hidden" style="border:0px;" readonly="readonly"  />
														    		<input type="hidden" name="fileDelYn" id="fileDelYn_D4" />
																	<span id="spfileD4'">
																	    <input type="file" title="첨부파일" name="fileD4" id="file_D4" 
																	        class="inputData L w10" style="width: 200px;" 
																	        onkeydown="return false;" onchange="fn_chkFileType(this,'D4');"/>
																	</span>
																	<input name="hddnFile" id="hddnFile_D4" type="hidden" style="display:none;" />
																	<input name="fileName" title="파일명" id="fileName_D4" type="text" 
																	    onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
																	<input name="realFileName" id="realFileName_D4" type="hidden" style="display:none;" />
																	<input name="fileSize" id="fileSize_D4" type="hidden" style="display:none;" />
																	<input name="filePath" id="filePath_D4" type="hidden" style="display:none;" />
														    	</td>
														    </tr>
														    <tr>
														    	<td class="ce">5</td>
														    	<td>협의경과서류</td>
														    	<td>
														    		<c:set var="attcSeqn" value="0"/>
														    		<c:if test="${not empty statApplication.fileList}">
														    			<c:forEach items="${statApplication.fileList}" var="fileList">
															    			<c:if test="${fileList.fileNameCd == '5'}">
															    				<a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
															    					onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
																    				${fileList.fileName }
															    				</a>
														    					<c:set var="attcSeqn" value="${fileList.attcSeqn }"/>
															    			</c:if>
														    			</c:forEach>
														    		</c:if>
												    				<input name="attcSeqn" id="attcSeqn_D5" value="${attcSeqn }" type="hidden" />
														    	</td>
														    	<td>
															    	<input name="fileNameCd" id="fileNameCd_D5" value="5" type="hidden" style="border:0px;" readonly="readonly"  />
														    		<input type="hidden" name="fileDelYn" id="fileDelYn_D5" />
																	<span id="spfileD5'">
																	    <input type="file" title="첨부파일" name="fileD5" id="file_D5" 
																	        class="inputData L w10" style="width: 200px;" 
																	        onkeydown="return false;" onchange="fn_chkFileType(this,'D5');"/>
																	</span>
																	<input name="hddnFile" id="hddnFile_D5" type="hidden" style="display:none;" />
																	<input name="fileName" title="파일명" id="fileName_D5" type="text" 
																	    onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
																	<input name="realFileName" id="realFileName_D5" type="hidden" style="display:none;" />
																	<input name="fileSize" id="fileSize_D5" type="hidden" style="display:none;" />
																	<input name="filePath" id="filePath_D5" type="hidden" style="display:none;" />
														    	</td>
														    </tr>
														    
														    <c:set var="fileIdx" value="6"/>
														    <c:if test="${not empty statApplication.fileList}">
												    			<c:forEach items="${statApplication.fileList}" var="fileList">
													    			<c:if test="${fileList.fileNameCd == '99'}">
														    <tr id="trAttcFile_D${fileIdx }">
														    	<td class="ce"><input name="chkDel1" title="선택"  id="chkDel1_${fileIdx }" type="checkbox" /></td>
														    	<td>기타</td>
														    	<td>
												    				<a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
												    					onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
													    				${fileList.fileName }
													    				<input name="attcSeqn" id="attcSeqn_D${fileIdx }" value="${fileList.attcSeqn }" type="hidden" />
												    				</a>
														    	</td>
														    	<td>
															    	<input name="fileNameCd" id="fileNameCd_D${fileIdx }" value="${fileList.fileNameCd }" type="hidden" style="border:0px;" readonly="readonly"  />
														    		<input type="hidden" name="fileDelYn" id="fileDelYn_D${fileIdx }" />
																	<span id="spfileD${fileIdx }'">
																	    <input type="file" title="첨부파일" name="fileD${fileIdx }" id="file_D${fileIdx }" 
																	        class="inputData L w10" style="width: 200px;" 
																	        onkeydown="return false;" onchange="fn_chkFileType(this,'D${fileIdx }');"/>
																	</span>
																	<input name="hddnFile" id="hddnFile_D${fileIdx }" type="hidden" style="display:none;" />
																	<input name="fileName" title="파일명" id="fileName_D${fileIdx }" type="text" 
																	    onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
																	<input name="realFileName" id="realFileName_D${fileIdx }" type="hidden" style="display:none;" />
																	<input name="fileSize" id="fileSize_D${fileIdx }" type="hidden" style="display:none;" />
																	<input name="filePath" id="filePath_D${fileIdx }" type="hidden" style="display:none;" />
														    	</td>
														    </tr>
														    		<c:set var="fileIdx" value="${fileIdx+1 }"/>
													    			</c:if>
												    			</c:forEach>
												    		</c:if>
														</tbody>
													</table>
												</div>
											</div>
											<!-- 첨부화일 끝 -->
										</td>
									</tr>
								</tbody>
							</table>		
							<div class="white_box">
								<div class="box5">
									<div class="box5_con floatDiv">
										<p class="fl mt5"><img src="/images/2011/content/box_img4.gif" alt="" /></p>
										<div class="fl ml30 mt5">
											<h4>첨부파일(증빙서류) 안내</h4>
											<ul class="list1 mt10">
											<li>별지 제2호서식에 따른 이용승인신청명세서(저작물,실연,음반,방송,데이터베이스의 형태 및 내용이 명확하지<br/> 아니한 경우에는 그 견본,도면 또는 사진 등을 첨부하여야 합니다) 1부</li>
											 <p class="floatDiv mb10"><img class="fl" src="/images/2011/button/btn_app8_01.gif" alt="이용 승인 신청명세서" /><span class="fl"><a href="#1" onclick="javascript:fn_fileDownLoad('C:/home/right4me_test/web/upload/form/','이용+승인신청명세서.hwp','이용+승인신청명세서.hwp')"><img src="/images/2011/button/btn_app7_02.gif" alt="양식다운로드 " /></a></span></p>
											<li>보상금액산정내역서 1부</li>
											<li>해당 저작물 등이 공표되었음을 밝힐 수 있는 서류 1부</li>
											<li>저작재산권자,저작인접권자 또는 데이터베이스제작자나 그의 거소를 알 수 없음을 밝힐 수 있는 서류<br/>(위 사유로 승인 신청하는 경우에 한정합니다) 1부</li>
											<li>협의에 관한 경과서류(협의가 성립되지 아니하여 승인 신청하는 경우에 한정합니다) 1부</li>
											<li>해당 음반이 우리나라에서 판매되어 3년이 경과하였음을 밝힐 수 있는 서류<br/>(법 제52조 및 법 제89조에 따라 승인 신청하는 경우에 한정합니다) 1부</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<!-- 이용 승인신청 정보 end -->
						
							<!-- button area str -->
							<div class="btnArea">
								<p class="fl">
									<span class="button medium gray"><a href="#1" onclick="fn_goList();" onkeypress="fn_goList();">취소</a></span>
								</p>
								<p class="fr">
									<span class="button medium"><a href="#1" onclick="fn_doSave('goList');" onkeypress="fn_doSave('goList');">임시저장</a></span> 
									<span class="button medium"><a href="#1" onclick="fn_doSave('goStep2');" onkeypress="fn_doSave('goStep2');">다음단계</a></span>
								</p>
							</div>
							<!-- button area end -->
						</div>
						</form>
						
					</div>
					
				</div>
				<!-- //주요컨텐츠 end -->
				
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- //FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	

<script type="text/JavaScript">
<!--
	window.onload = function(){
	
						//체크박스 셋팅
						var frm = document.frm;
						
						if('${statApplication.applyType01 }' == '1') {
							frm.applyType01.checked = true;
							frm.dummyApplyType01.checked = true;
						}
						if('${statApplication.applyType02 }' == '1') {
							frm.applyType02.checked = true;
							frm.dummyApplyType02.checked = true;
						}
						if('${statApplication.applyType03 }' == '1') {
							frm.applyType03.checked = true;
							frm.dummyApplyType03.checked = true;
						}
						if('${statApplication.applyType04 }' == '1') {
							frm.applyType04.checked = true;
							frm.dummyApplyType04.checked = true;
						}
						if('${statApplication.applyType05 }' == '1') {
							frm.applyType05.checked = true;
							frm.dummyApplyType05.checked = true;
						}
					
					}
//-->
</script>
</body>
</html>
