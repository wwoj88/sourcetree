<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("user_idnt", sessUserIdnt);
%>
<%
	pageContext.setAttribute("lf", "\n");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이벤트 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript">
	window.onload = function() {
		var partCnt = '${partCnt}';
		var partDuplYn = '${vo.part_dupl_yn}';

		if (partDuplYn == "N" && Number(partCnt) > 1) {
			alert("이미 참여하셨습니다.");
			self.close();
		}
		WindowReset();
	}

	function WindowReset() {
		var marginY = 67;
		if (navigator.userAgent.indexOf("Firefox") > 0) marginY = 65;
		else if(navigator.userAgent.indexOf("MSIE 8") > 0) marginY = 83;    // IE 8.x
		var hei = $("body").height();
		if (hei > 1000) {
			hei = 1000;
		}
		resizeTo(748, (hei + marginY));
	}

	//달력
	function fn_cal(frmName, objName) {
		showCalendar(frmName, objName);
	}

	function chkLegCk(objNo, obj) {
		var checkCnt = $("input[name='obj_" + objNo + "']:checked").length;
		var maxCount = $("input[name='max_choice_cnt_" + objNo + "']").val();
		if (Number(checkCnt) > Number(maxCount)) {
			alert("최대 선택 항목수는 " + maxCount + "개 입니다.");
			$(obj).attr("checked", false);
		}
	}

	function chDiv() {
		var checkPass = 'Y';
		var idck = '${user_idnt}';
		var partCnt = '${partCnt}';
		var partDuplYn = '${vo.part_dupl_yn}';
		if (idck == '') {
			alert("로그인하세요");
			return;
		}

		/*  	if(partDuplYn == 'N'){
		 if(partCnt = '' || Number(partCnt) > 1){
		 alert('이미 참여하셨습니다.');
		 return;
		 }
		 } */

		$("input.itemno").each(function() {
			var no = $(this).val();
			var typecd = $('#item_type_cd_' + no).val();
			if (typecd == '1' || typecd == '2') {
				if ($("#obj_" + no).val() == '') {
					alert('내용을 입력하세요');
					$("#obj_" + no).focus();
					checkPass = 'N';
					return false;
				}
			} else if (typecd == '3') {
				var cnt = $("input[name='obj_" + no + "']:checked").length;
				if (cnt <= 0) {
					alert("답을 선택해주세요.");
					checkPass = 'N';
					return false;
				}
			} else if (typecd == '4') {
				if ($("#obj_" + no).val() == '') {
					alert('파일을 입력하세요');
					$("#obj_" + no).focus();
					checkPass = 'N';
					return false;
				}
			}
		});
		if (checkPass == 'N')
			return;

		/*  	//1번 2번 타입 체크
		 $(".required").each(function(){
		 if($(this).val() == ''){
		 alert('내용을 입력하세요');
		 $(this).focus();
		 checkPass = 'N';
		 return false;
		 }
		 });
		 if(checkPass == 'N') return;
		
		 //3번타입 체크
		 $("input.itd").each(function(){
		 var vall = $(this).val();
		 var cnt = $("input[name='obj_"+vall+"']:checked").length;
		 if(cnt <= 0){
		 alert("답을 선택해주세요.");
		 $(this).focus();
		 checkPass = 'N';
		 return false;
		 }
		 });
		 if(checkPass == 'N') return; */

		//체크박스 기타 체크 3번타입
		$("input.ck").each(function() {
			if ($(this).is(':checked')) {
				var idname = $(this).attr("name").replace(/obj_/g, "obj_txt_") + '_' + $(this).val();
				if ($("#" + idname).val() == '') {
					alert('기타내용을 입력하세요');
					$("#" + idname).focus();
					checkPass = 'N';
					return false;
				}
			}
		});
		if (checkPass == 'N')
			return;

		/*  	//4번타입
		 $(".file").each(function(){
		 if($(this).val() == ''){
		 alert('파일을 입력하세요');
		 $(this).focus();
		 checkPass = 'N';
		 return false;
		 }
		 });
		 if(checkPass == 'N') return; */

		$('#pop_wrap1').attr("style", "display:none");
		$('#pop_wrap2').attr("style", "display:inline");
		WindowReset();
	}

	function chDiv2() {

		if ($('#name').val() == '') {
			alert("이름을 입력하세요.");
			$('#name').focus();
			return;
		}
		if ($('#birth_dtae').val() == '') {
			alert("생년월일을 입력하세요.");
			$('#birth_dtae').focus();
			return;
		}
		if ($('#mobl_phon').val() == '') {
			alert("휴대전화를 입력하세요.");
			$('#mobl_phon').focus();
			return;
		}

		var mob = $('#mobl_phon').val();
		if (mob.match(/^[\d]{3}-[\d]{3,4}-[\d]{4}$/) == null) {
			alert("휴대전화를 올바르게 입력하세요.");
			$('#mobl_phon').focus();
			return;
		}

		if ($('#mail').val() == '') {
			alert("이메일을 입력하세요.");
			$('#mail').focus();
			return;
		}

		var t = $('#mail').val();
		if (t.match(/^(\w+)@(\w+)[.](\w+)$/ig) == null && t.match(/^(\w+)@(\w+)[.](\w+)[.](\w+)$/ig) == null) {
			alert("이메일을 올바르게 입력하세요.");
			$('#mail').focus();
			return;
		}

		/* 	if($('#ckbox1').is(':checked') == false){
		 alert("동의항목을 체크해주세요.");
		 return;
		 } */

		var ehddml_pass = 'Y';
		$("input.ehddml").each(function() {
			if (!$(this).is(':checked')) {
				ehddml_pass = 'N';
				alert("동의항목을 체크해주세요.");
				return false;
			}
		});
		if (ehddml_pass == 'N')
			return;

		if (confirm("이벤트에 참여하시겠습니까?")) {
			$("#fbtn").attr("href","javascript:alert('잠시만 기다려주세요.');");
			formSubMit();
		}
	}

	function formSubMit() {
		$('#frm').attr('action', '/eventMgnt/addEventView01.do').submit();
	}

	function checkFile(aaa) {

	}
</script>
<style type="text/css">
.mt3 {
	margin-top: 3px;
}
</style>
</head>
<body class="popup_bg" id="body">
	<form action="#" name="frm" id="frm" method="post" enctype="multipart/form-data">
		<input type="hidden" name="event_id" value="${vo.event_id}" /> <input type="hidden" name="user_idnt" value="${user_idnt}" />
		<!-- 전체를 감싸는 DIVISION -->
		<div id="pop_wrap1">
			<!-- HEADER str-->
			<div id="popHeader">
				<h1>
					<c:choose>
						<c:when test="${vo.opening_yn eq 'S'}">
							<img class="vmid" src="/images/2012/common/ico_ing.gif" alt="진행예정" />
						</c:when>
						<c:when test="${vo.opening_yn eq 'Y'}">
							<img class="vmid" src="/images/2012/common/ico_ing.png" alt="진행중" />
						</c:when>
						<c:otherwise>
							<img class="vmid" src="/images/2012/common/ico_end.gif" alt="종료" />
						</c:otherwise>
					</c:choose>
					<span class="vmid ml5">${vo.event_title }</span> <span class="vmid ml5 thin p12">(기간 : ${vo.start_dttm_part}~${vo.end_dttm_part})</span>
				</h1>
			</div>
			<!-- //HEADER end -->
			<!-- CONTAINER str-->
			<div id="contentBody" style="padding: 0 15px 15px 15px;">
				<div class="section">
					<!-- 이벤트 설문조사 -->
					<div class="event">
						<!-- 이벤트이미지 -->
						<c:if test="${not empty vo.image_file_path && vo.open_yn_image eq 'Y'}">
							<div class="event_box">
								<p class="ce">
									<img src="/imageview.do?filePath=${vo.image_file_path}" alt="" class="event_img" />
								</p>
							</div>
						</c:if>
						<!-- //이벤트이미지 -->
						<!-- 설문조사 -->
						<span class="topLine mt20"></span>
						<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
							<tbody>
								<tr>
									<td>
										<ul class="event_poll">
											<c:forEach var="s" items="${list}" varStatus="seq">
												<li class="mt10"><c:choose>
														<c:when test="${s.item_type_cd eq '3'}">
															<input type="hidden" class="itd itemno" name="item_id" value="${s.item_id}">
														</c:when>
														<c:otherwise>
															<input type="hidden" class="itemno" name="item_id" value="${s.item_id}">
														</c:otherwise>
													</c:choose> <input type="hidden" name="item_type_cd" id="item_type_cd_${s.item_id}" value="${s.item_type_cd}"> <input type="hidden" name="item_seqn" value="${s.item_seqn}"> <input type="hidden" name="max_choice_cnt_${s.item_id}" value="${s.max_choice_cnt}">
													<p class="strong line22 blue">
														${seq.count}. ${s.item_desc}
														<c:if test="${s.max_choice_cnt > 1}">(최대 ${s.max_choice_cnt}개 선택)</c:if>
													</p> <c:choose>
														<c:when test="${s.item_type_cd eq '1'}">
															<input type="text" style="margin-left: 7px;" class="inputData w95 required" id="obj_${s.item_id}" name="obj_${s.item_id}" value="">
														</c:when>
														<c:when test="${s.item_type_cd eq '2'}">
															<textarea style="margin-left: 7px;" class="textarea w95 required" id="obj_${s.item_id}" name="obj_${s.item_id}" rows="3" cols="10"></textarea>
														</c:when>
														<c:when test="${s.item_type_cd eq '3'}">
															<c:forEach var="ss" items="${s.listSub}" varStatus="sseq">
																<c:choose>
																	<c:when test="${s.max_choice_cnt > 1}">
																		<c:choose>
																			<c:when test="${ss.etc_yn eq 'Y'}">
																				<input onclick="chkLegCk('${s.item_id}', this)" type="checkbox" class="mb5 mt5 ck" value="${ss.item_choice_id}" name="obj_${s.item_id}" title="">
																				<span class="mb5">${sseq.count}) 기타 ( <input type="text" class="inputData w70" name="obj_txt_${s.item_id}_${ss.item_choice_id}" id="obj_txt_${s.item_id}_${ss.item_choice_id}"> )
																				</span>
																				<br />
																			</c:when>
																			<c:otherwise>
																				<input onclick="chkLegCk('${s.item_id}', this)" type="checkbox" class="mb5 mt5" value="${ss.item_choice_id}" name="obj_${s.item_id}" title="">
																				<span class="mb5">${sseq.count}) ${ss.item_choice_desc}</span>
																				<br />
																			</c:otherwise>
																		</c:choose>
																	</c:when>
																	<c:otherwise>
																		<c:choose>
																			<c:when test="${ss.etc_yn eq 'Y'}">
																				<input type="radio" class="mb5 mt5 ck" value="${ss.item_choice_id}" id="rgstReas" name="obj_${s.item_id}" title="">
																				<span class="mb5">${sseq.count}) 기타 ( <input type="text" class="inputData w70" name="obj_txt_${s.item_id}_${ss.item_choice_id}" id="obj_txt_${s.item_id}_${ss.item_choice_id}"> )
																				</span>
																				<br />
																			</c:when>
																			<c:otherwise>
																				<input type="radio" class="mb5 mt5" value="${ss.item_choice_id}" id="rgstReas" name="obj_${s.item_id}" title="">
																				<span class="mb5">${sseq.count}) ${ss.item_choice_desc}</span>
																				<br />
																			</c:otherwise>
																		</c:choose>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</c:when>
														<c:when test="${s.item_type_cd eq '4'}">
															<input style="margin-left: 7px;" type="file" onchange="checkFile(this.id)" onkeydown="return false;" class="inputData L w95 file" title="첨부파일" id="obj_${s.item_id}" name="obj_${s.item_id}">
														</c:when>
													</c:choose> <span class="topLine mt10"></span></li>
											</c:forEach>
										</ul>
									</td>
								</tr>
							</tbody>
						</table>
						<!-- //설문조사 -->
						<!-- 버튼 str -->
						<div class="btnArea mt20">
							<c:choose>
								<c:when test="${param.type eq 'view'}">
									<a href="#"><img src="/images/2012/button/btn_parti.gif" alt="참여하기" /></a>
								</c:when>
								<c:otherwise>
									<a href="#" onclick="chDiv()"><img src="/images/2012/button/btn_parti.gif" alt="참여하기" /></a>
								</c:otherwise>
							</c:choose>
						</div>
						<!-- //버튼 -->
					</div>
					<!-- //이벤트 설문조사 -->
				</div>
			</div>
			<!-- //CONTAINER end -->

			<!-- FOOTER str-->
			<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights reserved.</p>
			<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="X" title="이 창을 닫습니다." /></a>
			<!-- //FOOTER end -->
		</div>
		<!-- //전체를 감싸는 DIVISION -->
		<!-- 전체를 감싸는 DIVISION -->
		<div id="pop_wrap2" style="display: none;">
			<!-- HEADER str-->
			<div id="popHeader">
				<h1>이벤트 참여정보 등록</h1>
			</div>
			<!-- //HEADER end -->
			<!-- CONTAINER str-->
			<div id="contentBody" style="padding: 0 15px 15px 15px;">
				<div class="section">
					<!-- 이벤트 설문조사 -->
					<h2 class="mt20">기본정보</h2>
					<span class="topLine"></span>
					<table cellpadding="0" cellspacing="0" border="1" class="grid">
						<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<tbody>
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="1" class="grid eventGrid">
										<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
											<col width="12%">
											<col width="38%">
											<col width="12%">
											<col width="38%">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><label for="name">이름</label></th>
												<td><input type="text" id="name" name="name" class="inputData w60"></td>
												<th scope="col" class="bgNone">&nbsp;</th>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<th scope="col">성별</th>
												<td><input type="radio" id="sex1" name="sex" checked="checked" value="M"> <label for="sex1" class="vmid">남</label><input type="radio" id="sex2" name="sex" class="ml20" value="F"> <label for="sex2" class="vmid">여</label></td>
												<th scope="col"><label for="birth_dtae">생년월일</label></th>
												<td><input type="text" id="birth_dtae" name="birth_dtae" class="inputData w60" readonly="readonly"><a href="#" class="ml5"><img src="/images/2012/common/calendar.gif" alt="달력" class="vmid" onclick="fn_cal('frm','birth_dtae')" /></a></td>
											</tr>
											<tr>
												<th scope="col"><label for="mobl_phon">휴대전화</label></th>
												<td colspan="3"><input type="text" id="mobl_phon" name="mobl_phon" class="inputData w30" />&nbsp;예)010-0000-0000</td>
											</tr>
											<tr>
												<th scope="col"><label for="mail">이메일주소</label></th>
												<td colspan="3"><input type="text" id="mail" name="mail" class="inputData w30"/>&nbsp;예)right@right4me.or.kr</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						<tbody>
					</table>
					<c:forEach items="${agree}" var="s">
						<h2 class="mt20">
							<label for="txt5">${s.item_title}</label>
						</h2>
						<textarea class="textarea w98" name="txt5" id="txt5" rows="3" cols="10" readonly="readonly">${s.item_desc}</textarea>
						<p class="mt5">
							<input type="checkbox" id="ckbox1" name="ckbox1" class="ehddml"> <span class="vmid">동의합니다.</span>
						</p>
					</c:forEach>
					<!-- //버튼Area -->
					<p class="btnArea">
						<a id="fbtn" href="javascript:chDiv2();"><img src="/images/2012/button/btn_complete.gif" alt="참여완료" /></a>
					</p>
					<!-- //버튼Area -->
					<!-- //이벤트 설문조사 -->
				</div>
			</div>
			<!-- //CONTAINER end -->

			<!-- FOOTER str-->
			<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights reserved.</p>
			<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="X" title="이 창을 닫습니다." /></a>
			<!-- //FOOTER end -->

		</div>
		<!-- //전체를 감싸는 DIVISION -->
	</form>
	<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>