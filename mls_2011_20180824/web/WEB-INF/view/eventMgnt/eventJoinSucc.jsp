<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이벤트참여 완료 안내 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="http://covo.jquery.com/jquery-latest.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript">
function windowExit(){
	opener.location.reload();
	self.close();
}
window.onload = function() {
	WindowReset();
}

function WindowReset() {
	var marginY = 67;
	if (navigator.userAgent.indexOf("Firefox") > 0) marginY = 65;
	else if(navigator.userAgent.indexOf("MSIE 8") > 0) marginY = 83;    // IE 8.x
	var hei = $("body").height();
	if (hei > 1000) {
		hei = 1000;
	}
	resizeTo(748, (hei + marginY));
}
</script>
<style type="text/css"> 
	.mt3{
		margin-top:3px;
	}
</style>
</head>
<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
			<!-- HEADER str-->
			<div id="popHeader">
				<h1>이벤트 참여완료 안내</h1>
			</div>	
			<!-- //HEADER end -->
			<!-- CONTAINER str-->
			<div id="contentBody" style="padding:0 15px 15px 15px;">
				<div class="section">
					<!-- 이벤트 설문조사 -->
					<div class="event">
						<div class="event_box">
							<img src="/images/2012/content/event_confirm.jpg" alt="" class="event_img"/>
							<div class="event_txt">
								<p class="name"><em>${vo.name}</em>님</p>
								<p class="strong mt10">성공적으로 참여완료 되었습니다. <br/>당첨여부는 ${fn:substring(vo.win_anuc_date,0,4)}년 ${fn:substring(vo.win_anuc_date,5,7)}월 ${fn:substring(vo.win_anuc_date,8,10)}일 부터 「저작권찾기 사이트」에서<br/> 확인 가능합니다. </p>
							</div>
						</div>
						<!-- 버튼Area -->	
						<c:choose>
							<c:when test="${vo.type eq '2'}">
								<p class="btnArea"><a href="/eventMgnt/eventView02.do?event_id=${vo.event_id}"><img src="/images/2012/button/btn_confirm.gif" alt="확인"/></a></p>
							</c:when>
							<c:otherwise>
								<p class="btnArea"><a href="javascript:windowExit();"><img src="/images/2012/button/btn_confirm.gif" alt="확인"/></a></p>
							</c:otherwise>
						</c:choose>
						<!-- //버튼Area -->	
					</div>										
					<!-- //이벤트 설문조사 -->										
				</div>				
			</div>
			<!-- //CONTAINER end -->
			
			<!-- FOOTER str-->
			<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights reserved.</p>
			<a href="#1" onclick="windowExit()" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="X" title="이 창을 닫습니다." /></a>
			<!-- //FOOTER end -->

		</div>	
		<!-- //전체를 감싸는 DIVISION -->
</body>
</html>

