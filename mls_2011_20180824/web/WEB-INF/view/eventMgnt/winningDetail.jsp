<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
%>
<% pageContext.setAttribute("lf", "\n"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이벤트 - 당첨자 발표 (${vo.event_title}) | 알림마당 | 권리자찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css">
<link rel="stylesheet" type="text/css" href="/css/table.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
</head>
<body>
	<form method="post" name="popfrm" id="popfrm" action="/eventMgnt/eventView01.do" target="popfrm">
		<input type="hidden" name="event_id" value="${vo.event_id}"/>
	</form>
 
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader1.jsp" /> --%>
		<!-- 2017 주석추가 -->
		<!-- <script type="text/javascript">initNavigation(1);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="contents">
			
				<!-- 래프 -->																														
				<div class="con_lf">
					<h2><div class="con_lf_big_title">알림마당</div></h2>
					<ul class="sub_lf_menu">
						<!-- <li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=4&page_no=1">공지사항</a></li>
						<li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=1&page_no=1">자주묻는 질문</a></li> -->
						<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1">공지사항</a></li>
						<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1">자주묻는 질문</a></li>
						<li><a href="/eventMgnt/eventList.do" class="on">이벤트</a></li>
						<li><a href="/mlsInfo/comeInfo.jsp">찾아오시는 길</a></li>
					</ul>
				</div>
				<!-- //래프 -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt">
					
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						알림마당
						&gt;
						<span class="bold">권리자 찾기 이벤트</span>
					</div>
					
					<h1><div class="con_rt_hd_title">당첨자 발표</div></h1>
					
					<!-- section -->
					<div class="section">
						<form name="form1" method="post" action = "#">
						<div class="mt20">
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" >
								<colgroup><col width="*"></colgroup>
								<tbody>
									<tr>
										<td style="border: 1px solid #e7e7e7;padding: 6px 10px;color: #444;background: #f3f4f5;font-size: 11px;text-align: left;font-weight: bold;line-height: 1.3;">
											<p class="fl">
												<c:choose>
													<c:when test="${vo.opening_win_yn eq 'S'}"><img src="/images/2012/common/ico_ing.gif" alt="" class="vmid" /></c:when>
													<c:when test="${vo.opening_win_yn eq 'Y'}"><img src="/images/2012/common/ico_ing.gif" alt="" class="vmid" /></c:when>
													<c:otherwise><img src="/images/2012/common/ico_end.gif" alt="종료" class="vmid" /></c:otherwise>
												</c:choose>
												<span class="vmid ml10">&lt;${vo.event_title}&gt; 당첨자발표 </span></p><p class="fr">기간 : ${vo.start_dttm_win_anuc}~${vo.end_dttm_win_anuc}
											</p>
										</td>
									</tr>
									<tr class="ce">
										<td><%-- <img src="/imageview.do?filePath=${vo.image_file_path_win}" alt="당첨자발표"> --%></td>
									</tr>
								</tbody>
							</table>							
							<!-- //그리드스타일 -->
							<!-- 버튼AREA -->
								<div class="btnArea">
									<p class="lft"><span class="button medium gray"><a href="/eventMgnt/winningList.do?nowPage=${param.nowPage}">목록</a></span></p>
								</div>
							<!--// 버튼AREA -->
						</div>
						<!-- //테이블 view Set -->
						</form>
																	
					</div>
					<!-- //section -->	
				</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
			</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>