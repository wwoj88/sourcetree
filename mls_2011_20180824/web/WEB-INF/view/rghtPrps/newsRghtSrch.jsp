<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2012/prototype.js"> </script>

<title>저작권찾기(뉴스) | 저작권찾기</title>
<script type="text/JavaScript">
<!--

var emptyYn = '${emptyYn}'; // 검색조건이 없을 경우 //20120220 정병호


window.name = "newsRghtSrch";
	
function goPage(pageNo){
	
	var frm = document.ifFrm;
	
	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
		},
		onSuccess: function(req) {     
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=N";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 뉴스저작물 상세 팝업오픈 : 부모창에서 오픈
function openNewsDetail( crId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=N'+param;
	var name = '';
	var openInfo = 'target=newsRghtSrch, width=705, height=400';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 영화저작물 포스트 팝업오픈 : 부모창에서 오픈
function openNews(postUrl) {
	var url = postUrl;
	var name = '';
	var openInfo = 'target=mvieRghtSrch, width=650, height=450, scrollbars=yes,toolbar=no,location=no,status=yes,menubar=no,resizable=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=N'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch, width=705, height=480';
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	
 	//parent.document.getElementById("totalRow").setfocus();
 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifNewsRghtSrch"); 
	
	if(emptyYn != 'Y'){// 검색조건이 없을 경우 //20120220 정병호
		var totalRow = ${newsList.totalRow}
		parent.document.getElementById("totalRow").value = totalRow //+"건"; //초기페이지 예외처리하기.
	}

	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	
	parent.frames.scrollTo(0,0);
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="sTotalRow" value="${newsList.totalRow}"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
	<input type="hidden" name="srchPublisher" value="${srchParam.srchPublisher }"/>
	<input type="hidden" name="srchBookTitle" value="${srchParam.srchBookTitle }"/>
	<input type="hidden" name="srchLicensorNm" value="${srchParam.srchLicensorNm }"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
	<input type="hidden" name="srchLicensor" value="${srchParam.srchLicensor }"/>
	<input type="hidden" name="srchProviderNm" value="${srchParam.srchProviderNm }"/>
	<input type="submit" style="display:none;"/>
	
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:auto;padding:0 0 0 0;margin-top:1px;">
			<table id="tab_scroll" class="sub_tab3 mar_tp40" cellspacing="0" cellpadding="0" width="100%" summary="뉴스 저작물의 뉴스기사제목, 언론사명, 기사일자, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
					<col width="5%"/>
					<col width="*" />
					<col width="25%" />
					<col width="16%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col">순번</th>
						<th scope="col">뉴스기사제목</th>
						<th scope="col">언론사명</th>
						<th scope="col">기사일자</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${emptyYn == 'Y'}"><!-- 검색조건이 없을 경우 //20120220 정병호 -->
					<tr>
						<td class="ce" colspan="4">검색조건을 입력해 주세요.</td>
					</tr>
				</c:if>
				<c:if test="${newsList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="4">검색된 뉴스저작물이 없습니다.</td>
					</tr>
				</c:if>
				<c:if test="${newsList.totalRow > 0}">
					<c:forEach items="${newsList.resultList}" var="newsList">
						<c:set var="NO" value="${newsList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
					<c:choose>
						 	<c:when test="${newsList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${newsList.ROW_NUM}</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${newsList.ROW_NUM}</td>
						 	</c:otherwise>
					</c:choose>
					<c:choose>
						 	<c:when test="${newsList.ROW_NUM % 2 == 0}">
						 		<td class="ev_num">
							<!-- hidden value start -->
							<input type="hidden" name="title" value="${newsList.TITLE}" />
							<input type="hidden" name="providerName" value="${newsList.PROVIDER_NM}" />
							<input type="hidden" name="articlPubcSdate" value="${newsList.ARTICL_PUBC_SDATE}" />
							<input type="hidden" name="crId" value="${newsList.CR_ID}" />
							<!-- hidden value end -->
							<a href="#1" onclick="javascript:openNewsDetail('${newsList.CR_ID }');" class="underline black2" title="새창 열림">${newsList.TITLE }</a>
							<c:if test="${newsList.LINK_PAGE_URL != '' && newsList.LINK_PAGE_URL != null}">
								<a href="#1" onclick="javascript:openNews('${newsList.LINK_PAGE_URL}');"><img src="/images/common/ic_preview.gif" class="vtop" alt="뉴스저작물 기사조회" /></a>
							</c:if>
						</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td>
							<!-- hidden value start -->
							<input type="hidden" name="title" value="${newsList.TITLE}" />
							<input type="hidden" name="providerName" value="${newsList.PROVIDER_NM}" />
							<input type="hidden" name="articlPubcSdate" value="${newsList.ARTICL_PUBC_SDATE}" />
							<input type="hidden" name="crId" value="${newsList.CR_ID}" />
							<!-- hidden value end -->
							<a href="#1" onclick="javascript:openNewsDetail('${newsList.CR_ID }');" class="underline black2" title="새창 열림">${newsList.TITLE }</a>
							<c:if test="${newsList.LINK_PAGE_URL != '' && newsList.LINK_PAGE_URL != null}">
								<a href="#1" onclick="javascript:openNews('${newsList.LINK_PAGE_URL}');"><img src="/images/common/ic_preview.gif" class="vtop" alt="뉴스저작물 기사조회" /></a>
							</c:if>
						</td>
						 	</c:otherwise>
					</c:choose>	
					<c:choose>
						 	<c:when test="${newsList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${newsList.PROVIDER_NM }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${newsList.PROVIDER_NM }</td>
						 	</c:otherwise>
					</c:choose>
					<c:choose>
						 	<c:when test="${newsList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${newsList.ARTICL_PUBC_SDATE }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${newsList.ARTICL_PUBC_SDATE }</td>
						 	</c:otherwise>
					</c:choose>	
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${newsList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
