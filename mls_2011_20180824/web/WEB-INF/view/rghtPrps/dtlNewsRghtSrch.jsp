<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2012/prototype.js"></script>
<title>권리찾기(뉴스) | 저작권찾기</title>
<script type="text/JavaScript">
<!--

window.name = "newsRghtSrch";


function fn_frameList(){
	var frm = document.ifFrm;

	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=N";
	
	new Ajax.Request('/test', {   
		onLoading: function() {
			//parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {
			parent.frames.showAjaxBox();
			//alert("완료");
		} 
	});
	
	frm.page_no.value = 1;
	frm.submit();
}

function goPage(pageNo){
	var frm = document.ifFrm;
	
	new Ajax.Request('/test', {   
		onLoading: function() {     
			//parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=N";
	frm.page_no.value = pageNo;
	frm.submit();
}


function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 뉴스저작물 상세 팝업오픈 : 부모창에서 오픈
function opennewsDetail( crId, nrId){

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=N'+param
	var name = '';
	var openInfo = 'target=newsRghtSrch, width=705, height=400';
	
	window.open(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifNewsRghtSrch"); 
	
	var totalRow = ${newsList.totalRow};
	totalRow = cfInsertComma(totalRow);
	parent.document.getElementById("totalRow").value = totalRow+"건";
	
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	
	// 부모창 알림- 추가저작물에 대한 중복확인검색 시 사용.
	if('${parentGubun}' == 'Y') {
	
		var srchTitleObj = document.ifFrm.srchTitle; //${srchParam.srchTitle};
		var srchTitle = srchTitleObj.value;
		
		if(totalRow=='0') {
			alert('저작물명 ['+srchTitle+'] 의 정보는 존재하지 않습니다. 계속진행 해주세요. \n' );
		}
		else {
			// 검색결과가 있는경우
			alert('저작물명 ['+srchTitle+'] 으로 '+totalRow+'건 검색되었습니다. \n추가등록을 원하는 저작물정보가 존재하는 경우 아래 검색결과에서 [선택 추가]해 주세요.' );
			srchTitleObj.focus();
		}
	}
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="dtlYn" value="Y"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
	
		<!-- search form str -->
		<fieldset class="w99">
			<legend></legend>
			<p class="box1">
				<label for="sch2" class="schDot1 pl10">
				<strong>뉴스기사제목</strong></label>
				<input type="text" id="sch2" name="srchTitle" value="${srchParam.srchTitle}" class="w20" />
				<label for="sch3" class="schDot1 ml5 pl10">
				<strong>언론사</strong></label>
				<input type="text"  id="sch3" name="srchProviderNm" value="${srchParam.srchProviderNm}" class="w10" />
				<span class="button small black"><input type="submit" value="검색" onclick="javascript:fn_frameList();"></input></span>
			</p>
		</fieldset>
		<div id="div_scroll" class="tabelRound div_scroll" style="width:569px;height:auto;padding:0 0 0 0;">
		<table id="tab_scroll" cellspacing="0" cellpadding="0" border="1" summary="뉴스저작물의 뉴스기사제목, 기사일자, 언론사사 정보입니다." class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
			<colgroup>
			<col width="2%" />
			<col width="*" />
			<col width="15%" />
			<col width="18%" />
			</colgroup>
			<thead>
				<tr>
					<th scope="col"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','ifrmChk',this);" style="cursor:pointer;" title="전체선택"/></th>
					<th scope="col">뉴스기사제목</th>
					<th scope="col">기사일자</th>
					<th scope="col">언론사</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${newsList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="5">검색된 뉴스저작물이 없습니다.</td>
					</tr>
				</c:if>
				
				<c:if test="${newsList.totalRow > 0}">
					<c:forEach items="${newsList.resultList}" var="newsList">
						<c:set var="NO" value="${newsList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
						<tr>
							<td class="ce">
								<input type="checkbox" name="ifrmChk" class="vmid" value="${newsList.CR_ID }|0|0" style="cursor:pointer;" title="선택" />
								<!-- hidden value start -->
								<input type="hidden" name="title" value="${newsList.TITLE}" />
								<input type="hidden" name="open_date" value="${newsList.ARTICL_PUBC_SDATE}" />
								<input type="hidden" name="coptHodr" value="${newsList.PROVIDER_NM}" />
								<input type="hidden" name="crId" value="${newsList.CR_ID}" />
								<!-- hidden value end -->	
	
							</td>
							<td><a href="#1" onclick="javascript:opennewsDetail('${newsList.CR_ID }');" title="새창 열림"><u>${newsList.TITLE }</u></a></td>
							<td class="ce">${newsList.ARTICL_PUBC_SDATE }</td>
							<td class="ce">${newsList.PROVIDER_NM }</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${newsList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
