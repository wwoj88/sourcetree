<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>저작권정보 조회(도서) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script type="text/javascript" src="/js/deScript.js"></script>

</head>
<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>도서저작물 상세조회</h1>
		</div>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<p class="red p11" ><strong>* 저작권정보 변경신청에 관한 문의는 검색화면의 단체로 연락바랍니다.</strong></p>
			<div class="section">
				<h2 class="mt15">도서저작물정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="도서저작물의 도서정보 입니다.">
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">작품명</th>
							<td colspan="3">${book.WORKS_TITLE}</td>
						</tr>
						<tr>
							<th scope="row">작품부제목</th>
							<td colspan="3">${book.WORKS_SUBTITLE}</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="section mt20">
				<h2>도서정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="도서저작물의 도서정보 입니다.">
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">도서명</th>
							<td colspan="3">${book.TITLE}</td>
						</tr>
						<tr>
							<th scope="row">도서부제</th>
							<td>${book.SUBTITLE}</td>
							<th scope="row">발행일자</th>
							<td>${book.FIRST_EDITION_YEAR}</td>
						</tr>
						<tr>
							<th scope="row">출판사</th>
							<td>${book.PUBLISHER}</td>
							<th scope="row">자료유형</th>
							<td>${book.PUBLISH_TYPE}</td>
						</tr>
						<tr>
							<th scope="row">검색분류</th>
							<td>${book.RETRIEVE_TYPE}</td>
							<th scope="row">장르구분</th>
							<td>${book.MATERIAL_TYPE}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="section mt20">
				<h2>권리정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="도서저작물의 권리정보 입니다.">
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">작가명</th>
							<td>${book.LICENSOR_NAME_KOR}</td>
							<th scope="row">예필명</th>
							<td>${book.STAGE_NAME}</td>
						</tr>
						<tr>
							<th scope="row">역자명</th>
							<td colspan="3">${book.TRANSLATOR}</td>
						</tr>
						<tr>
							<th scope="row">한국문예학술저작권협회</th>
							<td>
								<c:if test="${book.TRUST_YN == 'Y' }">신탁</c:if>
								<c:if test="${book.TRUST_YN == 'N' }">비신탁</c:if>
							</td>
							<th scope="row">한국복사전송권협회</th>
							<td>
								<c:if test="${book.KRTRA_TRUST_YN == 'Y' }">신탁</c:if>
								<c:if test="${book.KRTRA_TRUST_YN == 'N' }">비신탁</c:if>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		
			<div>
			<p class="fl mt20 p11" >* 디지털저작권거래소(kdce.or.kr)에서 저작권 이용계약 서비스를 이용할 수 있습니다. </p>
			<a class="fr mt15"  href="http://kdce.or.kr/user/main.do" target="_blank"><img src="/images/2012/button/btn_go_site.gif" alt="바로가기" title="바로가기" /></a><br/>
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="닫기" title="이 창을 닫습니다."/></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
