<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"></script>
<title>저작권찾기(음악) | 저작권찾기</title>
<script type="text/JavaScript">
<!--

window.name = "muscRghtSrch";

function fn_frameList(){
	var frm = document.ifFrm;

	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=M";
	
	new Ajax.Request('/test', {   
		onLoading: function() {
		},
		onSuccess: function(req) {    
		},
		onComplete: function() {
			parent.frames.showAjaxBox();
			//alert("완료");
		} 
	});
	
	frm.page_no.value = 1;
	frm.submit();
}

function goPage(pageNo){
	var frm = document.ifFrm;
	
	new Ajax.Request('/test', {   
		onLoading: function() {     
		},
		onSuccess: function(req) {     
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=M";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 음악저작물 상세 팝업오픈 : 부모창에서 오픈
function openMusicDetail( crId, nrId, meId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	param += '&NR_ID='+nrId;
	param += '&ALBUM_ID='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=M'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch, width=705, height=525, height=570, scrollbars=yes';
	
	window.open(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifMuscRghtSrch"); 
	
	var totalRow = ${muscList.totalRow};
	totalRow = cfInsertComma(totalRow);
	parent.document.getElementById("totalRow").value = totalRow+"건";
	
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	
	// 부모창 알림- 추가저작물에 대한 중복확인검색 시 사용.
	if('${parentGubun}' == 'Y') {
	
		var srchTitleObj = document.ifFrm.srchTitle; 
		var srchTitle = srchTitleObj.value;
		
		if(totalRow=='0') {
			alert('저작물명 ['+srchTitle+'] 의 정보는 존재하지 않습니다. 계속진행 해주세요. \n' );
		}
		else {
			// 검색결과가 있는경우
			alert('저작물명 ['+srchTitle+'] 으로 '+totalRow+'건 검색되었습니다. \n추가등록을 원하는 저작물정보가 존재하는 경우 아래 검색결과에서 [선택 추가]해 주세요.' );
			srchTitleObj.focus();
		}
	}
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="dtlYn" value="Y"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
	<input type="hidden" name="srchLicensor" value="${srchParam.srchLicensor }"/>
	
		<!-- search form str -->
		<fieldset class="w99">
			<legend></legend>
			<p class="box1">
				<label for="sch2" class="schDot1 pl10">
				<strong>저작물명</strong></label>
				<input type="text" id="sch2" name="srchTitle" value="${srchParam.srchTitle}" class="w10" />
				<label for="sch3" class="schDot1 ml pl10">
				<strong>앨범명</strong></label>
				<input type="text"  id="sch3" name="srchAlbumTitle" value="${srchParam.srchAlbumTitle}" class="w10" />
				<label for="sch1" class="schDot1 ml pl10">
				<strong>가수</strong></label>
				<input type="text" id="sch1" name="srchSinger" value="${srchParam.srchSinger}" class="w10" />
				<label for="sch4" class="schDot1 ml pl10">
				<strong>앨범제작</strong></label>
				<input type="text" id="sch4" name="srchProducer" value="${srchParam.srchProducer}" class="w10" />
				<span class="button small black"><input type="submit" value="검색" onclick="javascript:fn_frameList();"></input></span>
			</p>
		</fieldset>
		<div id="div_scroll" class="tabelRound div_scroll" style="width:569px;height:auto;padding:0 0 0 0;">
		<table id="tab_scroll" cellspacing="0" cellpadding="0" border="1" summary="음악저작물의 가수명, 저작물명, 앨범명, 발매일, 작사, 작곡, 가수, 제작사 정보입니다." class="grid mt5 tableFixed"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
			<colgroup>
			<col width="5%" />
			<col width="6%" />
			<col width="*" />
			<col width="13%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="12%" />
			</colgroup>
			<thead>
				<tr>
					<th rowspan="2" scope="col"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','ifrmChk',this);" style="cursor:pointer;" title="전체선택"/></th>
					<th rowspan="2" scope="col">순번</th>
					<th scope="col">저작물명</th>
					<th scope="col">실연시간</th>
					<th scope="col">작사</th>
					<th scope="col">작곡</th>
					<th scope="col">편곡</th>
					<th rowspan="2" scope="col">음반제작</th>
				</tr>
				<tr>
					<th scope="col">앨범명</th>
					<th scope="col">발매일</th>
					<th scope="col">가창</th>
					<th scope="col">연주</th>
					<th scope="col">지휘</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${muscList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="8">검색된 음악저작물이 없습니다.</td>
					</tr>
				</c:if>
				
				<c:if test="${muscList.totalRow > 0}">
					<c:forEach items="${muscList.resultList}" var="muscList">
						<c:set var="NO" value="${muscList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
						<tr>
							<td rowspan="2" class="ce">
								<input type="checkbox" name="ifrmChk" class="vmid" value="${muscList.CR_ID }|${muscList.NR_ID }|${muscList.ALBUM_ID }" style="cursor:pointer;" title="선택"/>
								<!-- hidden value start -->
								<input type="hidden" name="musicTitle" value="${muscList.MUSIC_TITLE_TRNS}" />
								<input type="hidden" name="albumTitle" value="${muscList.ALBUM_TITLE_TRNS}" />
								<input type="hidden" name="issuedDate" value="${muscList.ISSUED_DATE}" />
								<input type="hidden" name="lyricist" value="${muscList.LYRICIST_TRNS}" />
								<input type="hidden" name="composer" value="${muscList.COMPOSER_TRNS}" />
								<input type="hidden" name="singer" value="${muscList.SINGER_TRNS}" />
								<input type="hidden" name="producer" value="${muscList.PRODUCER_TRNS}" />
								<input type="hidden" name="crId" value="${muscList.CR_ID}" />
								<input type="hidden" name="nrId" value="${muscList.NR_ID}" />
								<input type="hidden" name="albumId" value="${muscList.ALBUM_ID}" />
								<input type="hidden" name="arranger" value="${muscList.ARRANGER_TRNS}" />
								<input type="hidden" name="translator" value="${muscList.TRANSLATOR_TRNS}" />
								<input type="hidden" name="player" value="${muscList.PLAYER_TRNS}" />
								<input type="hidden" name="conductor" value="${muscList.CONDUCTOR_TRNS}" />
								<input type="hidden" name="featuring" value="${muscList.FEATURING_TRNS}" />
								<input type="hidden" name="icnNumb" value="${muscList.ICN_NUMB}" />
								<input type="hidden" name="albumProducedCrh" value="${muscList.ALBUM_PRODUCED_CRH_TRNS}" />
								<input type="hidden" name="perfTime" value="${muscList.PERF_TIME}" />
								<!-- hidden value end -->
							</td>
							<td rowspan="2" class="ce"><c:out value="${i}"/></td>
							<td><a href="#1" title="새창 열림" class="underline black2" onclick="javascript:openMusicDetail('${muscList.CR_ID }','${muscList.NR_ID }','${muscList.ALBUM_ID }');">${muscList.MUSIC_TITLE }</a></td>
							<td class="ce">${muscList.PERF_TIME }</td>
							<td class="ce">${muscList.LYRICIST }</td>
							<td class="ce">${muscList.COMPOSER }</td>
							<td class="ce">${muscList.ARRANGER_TRNS}</td>
							<td rowspan="2" class="ce">${prpsList.PRODUCER_TRNS }</td>
						</tr>
						<tr>
							<td>${prpsList.ALBUM_TITLE}<c:if test="${prpsList.ALBUM_TITLE == null}">&nbsp;</c:if></td>
							<td class="ce">${prpsList.ISSUED_DATE }</td>
							<td class="ce">${prpsList.SINGER_TRNS }</td>
							<td class="ce">${prpsList.PLAYER_TRNS }</td>
							<td class="ce">${prpsList.CONDUCTOR_TRNS }</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${muscList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
