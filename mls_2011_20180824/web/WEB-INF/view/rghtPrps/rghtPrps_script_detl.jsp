<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.StringTokenizer"%>
<jsp:useBean id="rghtPrps" class= "kr.or.copyright.mls.rghtPrps.model.RghtPrps" scope="request" />
<jsp:setProperty name="rghtPrps" property="*" />
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권정보 조회(방송대본) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<style type="text/css">
<!--
body {
	overflow-x: auto;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>

<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>

<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/general.js"></script>
	
<script type="text/javascript">
<!--

window.name = "rghtPrps_script_detl";

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	
	
	var the_height = document.getElementById(name).offsetHeight;
	var chkObjs = document.getElementsByName("chk");
	  
	document.getElementById(targetName).style.height = the_height + 13 + "px" ;
	  

}

// 스크롤셋팅
function scrollSet(name, targetName, oRowName){
	
	var totalRow = document.getElementsByName(oRowName).length;
	
//	alert(totalRow);
		
	// 세로 기준건수 이상인 경우 세로스크롤 제어
	if( totalRow < 6) {
		resizeDiv(name, targetName);
		document.getElementById(targetName).style.overflowY = "hidden";
	}
	
	if( totalRow > 5 ) {
		document.getElementById(targetName).style.overflowY = "auto";
	}
}


//선택저작물 테이블 idx
var iRowIdx = 1;
var iRowIdx2 = 1;

// 오프라인 접수
function offLine_check(chk) {
	
	if(chk.checked) {
		document.getElementById("td_file_no").style.display = "";
		document.getElementById("td_file_yes").style.display = "none";
	} else{
		document.getElementById("td_file_no").style.display = "none";
		document.getElementById("td_file_yes").style.display = "";
	} 
}

// 권리찾기신청
function rghtPrps() {

	var frm = document.prpsForm;
	
	if(checkForm(frm)) {
		
		if( !document.getElementById("CHK_206").checked ){
			alert('신청 신탁관리단체을(를) 선택하세요.');
			document.getElementById("CHK_206").focus();
			return;
		}
		
		if( document.getElementsByName("iChkVal").length <1 ){
			alert('신청 저작물(를) 추가하세요.');
			return;
		}
		
		frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrpsProc&DIVS=C";
		
		frm.method="post";
		frm.submit();

	}
}

//권리찾기신청 입력폼
function rghtPrpsBack() {

		var frm = document.prpsForm;
	
		frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrpsBack&DIVS=C";
		
		frm.method="post";
		frm.submit();
}


// 목록 
function goList(){
	
	var listDivs = '${listDivs}';
	
	var frm = document.srchForm;
	
	// 접근한 목록으로 보낸다.
	if(listDivs == 'noneRghtList')
		frm.action = '/noneRght/rghtSrch.do?DIVS=C';
	else
		frm.action = '/rghtPrps/rghtSrch.do?DIVS=C';
		
	frm.method="post";
	frm.submit();
}

function showAttach(cnt) {
	var i=0;
	var content = "";
	var attach = document.getElementById("attach")
	attach.innerHTML = "";
	//document.all("attach").innerHTML = "";
	content += '<table>';
	for (i=0; i<cnt; i++) {
		content += '<tr>';
		content += '<td><input type="file" name="attachfile'+(i+1)+'" size="70;" class="inputFile"><\/td>';
		content += '<\/tr>';
	}
	content += '<\/table>';
	//document.all("attach").innerHTML = content;
	attach.innerHTML = content;
}

function applyAtch() {
var ansCnt = document.prpsForm.atchCnt.value;
showAttach(parseInt(ansCnt));
}

// 파일다운로드
function fn_fileDownLoad(filePath, fileName, realFileName) {
		
	var frm = document.prpsForm;
	
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.target="rghtPrps_script_detl";
	//frm.action = "/rsltInqr/rsltInqr.do?method=fileDownLoad";
	frm.action = "/board/board.do?method=fileDownLoad";
	frm.method="post";
	frm.submit();
 }
  
function initParameter(){
 	// 토탈건수
	scrollSet("listTab", "div_scroll_1", "iChk");
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
</head>
<body>
<!-- 전체를 감싸는 DIVISION -->
<div id="wrap"><!-- HEADER str--> <jsp:include
	page="/include/2012/header.jsp" /> <script type="text/javascript">initNavigation(2);</script>
<!-- GNB setOn 각페이지에 넣어야합니다. --> <!-- HEADER end -->
<!-- CONTAINER str-->
<div id="container" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
	<div class="container_vis" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
		<h2><span><img src="/images/2012/title/container_vis_h2_2.gif" alt="내권리찾기" title="내권리찾기" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
		<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
	</div>
	<div class="content">
	
			<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu02.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb1");</script>
			<!-- //래프 -->
		
		<!-- 주요컨텐츠 str -->
		<div class="contentBody" id="contentBody">
			<p class="path"><span>Home</span><span>내권리찾기</span><em>저작권정보 변경신청</em></p>
			<h1><img src="/images/2012/title/content_h1_0201.gif" alt="저작권정보 변경신청" title="저작권정보 변경신청" /></h1>
			
			<div class="section">
			
			 <!-- Tab str -->
                          <ul id="tab11" class="tab_menuBg">
                              <li class="first"><a href="#">소개</a></li>
                              <li><a href="#">이용방법</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악</a></li>
							  <li><a href="/rghtPrps/rghtSrch.do?DIVS=B">도서</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=N">뉴스</a></li>
                              <li class="on"><strong><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본</a></strong></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=X">기타</a></li>
                      		</ul>
                <!-- //Tab -->
				
			
				<!-- 연락처 박스  -->
				<jsp:include page="/common/memo/2011/memo_01.jsp">
					<jsp:param name="DIVS" value="${DIVS}" />
				</jsp:include>
				<!-- //연락처 박스 -->
				
				<div class="article mt20">
					<div class="floatDiv">
						<h2 class="fl">저작권정보 변경신청</h2>
					</div>
					<form name="prpsForm" action="#">
					<input type="hidden" name="USER_IDNT" value="${userInfo.USER_IDNT}"/>
					<input type="hidden" name="USER_DIVS" value="${userInfo.USER_DIVS }"/>
					<input type="hidden" name="USER_NAME" value="${userInfo.USER_NAME }"/>
					<input type="hidden" name="RESD_CORP_NUMB_VIEW" value="${userInfo.RESD_CORP_NUMB_VIEW }"/>
					<input type="hidden" name="CORP_NUMB" value="${userInfo.CORP_NUMB }"/>
					<input type="hidden" name="HOME_TELX_NUMB" value="${userInfo.HOME_TELX_NUMB }"/>
					<input type="hidden" name="BUSI_TELX_NUMB" value="${userInfo.BUSI_TELX_NUMB }"/>
					<input type="hidden" name="MOBL_PHON" value="${userInfo.MOBL_PHON }"/>
					<input type="hidden" name="FAXX_NUMB" value="${userInfo.FAXX_NUMB }"/>
					<input type="hidden" name="MAIL" value="${userInfo.MAIL }"/>
					<input type="hidden" name="HOME_ADDR" value="${userInfo.HOME_ADDR }"/>
					<input type="hidden" name="BUSI_ADDR" value="${userInfo.BUSI_ADDR }"/>
					<input type="hidden" name="OFFX_LINE_RECP" value="${rghtPrps.OFFX_LINE_RECP }"/>
					<input type="hidden" name="PRPS_RGHT_CODE" value="${rghtPrps.PRPS_RGHT_CODE }" />
					<input type="hidden" name="PRPS_DESC" value="${rghtPrps.PRPS_DESC}" />
					<!-- about files -->
					<input type="hidden" name="FILE_INFO" value="${rghtPrps.FILE_INFO}" />
					<input type="hidden" name="filePath" />
					<input type="hidden" name="fileName" />
					<input type="hidden" name="realFileName" />
					<!-- 접근목록 -->
					<input type="hidden" name="listDivs" value="${listDivs}"/>
					<input type="submit" style="display:none;">
					
					<span class="topLine"></span>
					<!-- 그리드스타일 -->
					<table cellspacing="0" cellpadding="0" border="1" summary="저작권찾기 신청정보 입력 폼입니다." class="grid tableFixed">
						<colgroup>
						<col width="8%">
						<col width="12%">
						<col width="*">
						</colgroup>
						<tbody>
							<tr>
								<th scope="row" rowspan="2" class="bgbr lft">신청<br>정보</th>
								<th scope="row"><label class="necessary">신청목적</label></th>
								<td><!-- <select name="PRPS_RGHT_CODE" id="PRPS_RGHT_CODE"  title="신청목적" nullCheck onchange="prps_check(this);">-->
									<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">권리자의 저작권찾기</c:if>
									<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">이용자의 저작권조회</c:if>
								</td>
							</tr>
							<tr>
								<th scope="row">신청인정보</th>
								<td>
									<span class="topLine2"></span>
									<!-- 그리드스타일 -->
									<table cellspacing="0" cellpadding="0" border="1" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다." class="grid">
										<colgroup>
										<col width="15%">
										<col width="35%">
										<col width="25%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row">성명</th>
												<td>${userInfo.USER_NAME}</td>
												<th scope="row">주민등록번호/사업자번호</th>
												<c:if test="${userInfo.USER_DIVS != '03'}">
												<td>
												${userInfo.RESD_CORP_NUMB_VIEW}
												</td>
												</c:if>
												<c:if test="${userInfo.USER_DIVS != '01'}">
												<td>
												${fn:substring(userInfo.CORP_NUMB,0,3)}-${fn:substring(userInfo.CORP_NUMB,3,5)}-${fn:substring(userInfo.CORP_NUMB,5,10)}
												</td>
												</c:if>
											</tr>
											<tr>
												<th scope="row">전화번호</th>
												<td>
													<ul class="list1">
													<li class="p11"><label class="inBlock w30">자택</label> : ${userInfo.HOME_TELX_NUMB}</li>
													<li class="p11"><label class="inBlock w30">사무실</label> : ${userInfo.BUSI_TELX_NUMB}</li>
													<li class="p11"><label class="inBlock w30">휴대폰</label> : ${userInfo.MOBL_PHON}</li>
													</ul>
												</td>
												<th scope="row">팩스번호</th>
												<td>${userInfo.FAXX_NUMB}</td>
											</tr>
											<tr>
												<th scope="row">이메일주소</th>
												<td colspan="3">${userInfo.MAIL}</td>
											</tr>
											<tr>
												<th scope="row">주소</th>
												<td colspan="3">
													<ul class="list1">
													<li class="p11"><label class="inBlock w10">자택</label> : ${userInfo.HOME_ADDR}</li>
													<li class="p11"><label class="inBlock w10">사무실</label> : ${userInfo.BUSI_ADDR}</li>
													</ul>
												</td>
											</tr>
										</tbody>
									</table>
									<!-- //그리드스타일 -->
								</td>
							</tr>
							<tr>
								<th scope="row" colspan="2"><label for="CHK_206" class="necessary">권리구분</label></th>
								<td>
									<ul class="line22">
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_206" value="206" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_206 == '206'}">checked="checked"</c:if>/>
										<label class="p12" for="CHK_206">작가 - 저작권자(한국방송작가협회)</label>
									</li>
									</ul>
								</td>
							</tr>
							<tr>
								<th scope="row" colspan="2"><label class="necessary">신청저작물정보</label></th>
								<td>
									<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1' || rghtPrps.PRPS_RGHT_CODE == '2'}">
									<!-- 권리자 저작권찾기 시작 -->
									<div id="div_1" class="tabelRound" style="width:572px; padding:0 0 0 0;">
										<div class="floatDiv mb5 mt10"><h3 class="fl mt5">권리자 저작권찾기<span id="totalRow"></span></h3></div>
										<div id="div_scroll_1" style="width:566px; padding:0 0 0 0;">
											<table id="listTab" cellspacing="0" cellpadding="0" border="1" summary="권리자 저작권찾기 신청저작물정보 폼입니다." class="grid ce tableFixed"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
												<colgroup>
												<col width="5%">
												<col width="*">
												<col width="14%">
												<col width="15%">
												<col width="16%">
												<col width="15%">
												<col width="12%">
												</colgroup>
												<thead>
													<tr>
														<th rowspan="2" scope="col">순번</th>
														<th rowspan="2" scope="col"><label class="necessary white">저작물명</label></th>
														<th rowspan="2" scope="col"><label class="necessary white">장르</label></th>
														<th scope="col">방송사</th>
														<th scope="col">방송매체</th>
														<th scope="col">연출</th>
														<th rowspan="2" scope="col">작가</th>
													</tr>
													<tr>
														<th scope="col">방송회차</th>
														<th scope="col"><label class="necessary white">방송일자</label></th>
														<th scope="col">제작사</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${rghtPrps.keyId}" var="prpsList">
														<c:set var="i" value="${i+1}"/>
														<tr>
															<td rowspan="2" class="ce">
																<input name="displaySeq" title="순번" id="displaySeq_${i}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}"/>
															</td>
															<td rowspan="2" class="lft">${rghtPrps.TITLE_ARR[i-1] }</td>
															<c:if test="${!empty rghtPrps.keyId[i-1]}">
															<td rowspan="2" class="ce">${rghtPrps.GENRE_KIND_NAME_ARR[i-1] }&nbsp;</td>
															<td class="ce">${rghtPrps.BROAD_STAT_NAME_ARR[i-1]}&nbsp;</td>
															<td class="ce">${rghtPrps.BROAD_MEDI_NAME_ARR[i-1]}&nbsp;</td>
															</c:if>
															<c:if test="${empty rghtPrps.keyId[i-1]}">
															<td rowspan="2" class="ce">
															<c:forEach items="${genreList}" var="genreList">
																<c:if test="${rghtPrps.GENRE_KIND_ARR[i-1] == genreList.code}">${genreList.codeName}</c:if>
															</c:forEach>
															&nbsp;</td>
															<td class="ce">
															<c:forEach items="${broadList}" var="broadList">
																<c:if test="${rghtPrps.BROAD_STAT_ARR[i-1] == broadList.code}">${broadList.codeName}</c:if>
															</c:forEach>
															&nbsp;</td>
															<td class="ce">
																<c:choose>
																	<c:when test="${rghtPrps.BROAD_MEDI_ARR[i-1] == 'T'}">TV</c:when>
																	<c:when test="${rghtPrps.BROAD_MEDI_ARR[i-1] == 'R'}">RADIO</c:when>
																	<c:when test="${rghtPrps.BROAD_MEDI_ARR[i-1] == 'C'}">CABLE</c:when>
																	<c:when test="${rghtPrps.BROAD_MEDI_ARR[i-1] == 'I'}">INTERNET</c:when>
																	<c:when test="${rghtPrps.BROAD_MEDI_ARR[i-1] == 'X'}">기타</c:when>
																</c:choose>
															</td>
															</c:if>
															<td class="ce">${rghtPrps.DIRECT_ARR[i-1] }&nbsp;</td>
															<td rowspan="2" class="ce">
																${rghtPrps.WRITER_ARR[i-1] }
																<!-- hidden Value -->
																<input type="hidden" name="WRITER" value="${rghtPrps.WRITER_ARR_TRNS[i-1] }" />
																
																<input type="hidden" name="iChkVal" value="${rghtPrps.keyId[i-1] }"/>
																<input type="hidden" name="TITLE"  value="${rghtPrps.TITLE_ARR_TRNS[i-1] }" />
																<input type="hidden" name="GENRE_KIND" value="${rghtPrps.GENRE_KIND_ARR[i-1] }" />
																<input type="hidden" name="GENRE_KIND_NAME" value="${rghtPrps.GENRE_KIND_NAME_ARR[i-1] }" />
																<input type="hidden" name="BROAD_MEDI" value="${rghtPrps.BROAD_MEDI_ARR[i-1] }" />
																<input type="hidden" name="BROAD_MEDI_NAME" value="${rghtPrps.BROAD_MEDI_NAME_ARR_TRNS[i-1] }" />
																<input type="hidden" name="BROAD_STAT" value="${rghtPrps.BROAD_STAT_ARR[i-1] }" />
																<input type="hidden" name="BROAD_STAT_NAME" value="${rghtPrps.BROAD_STAT_NAME_ARR_TRNS[i-1] }" />
																<input type="hidden" name="BROAD_ORD" value="${rghtPrps.BROAD_ORD_ARR[i-1] }" />
																<!--<input type="hidden" name="BROAD_DATE" value="${rghtPrps.BROAD_DATE_ARR[i-1] }" />-->
																<input type="hidden" name="BROAD_DATE_${i}" value="${rghtPrps.BROAD_DATE_ARR[i-1] }" />
																<input type="hidden" name="BROAD_DATE"/>
																<input type="hidden" name="DIRECT" value="${rghtPrps.DIRECT_ARR_TRNS[i-1] }"/>
																<input type="hidden" name="MAKER" value="${rghtPrps.MAKER_ARR_TRNS[i-1] }"/>
																<input type="hidden" name="WRITER_ORGN" value="${rghtPrps.WRITER_ORGN_ARR_TRNS[i-1] }" />
															</td>
														</tr>
														<tr>
															<td class="ce">${rghtPrps.BROAD_ORD_ARR[i-1] }&nbsp;</td>
															<td class="ce">${rghtPrps.BROAD_DATE_ARR[i-1] }&nbsp;</td>
															<td class="ce">${rghtPrps.MAKER_ARR[i-1] }&nbsp;</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
									</c:if>
									<!-- 권리자의 저작권 찾기 끝 -->
									
									<!-- 이용자의 저작권 찾기 시작 -->
									<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">	
									<div id="div_1" class="tabelRound mt10" style="width:572px; ">
									<div class="floatDiv mb5 mt10"><h3 class="fl mt5">이용자 저작권조회<span id="totalRow2"></span></h3></div>
										<div id="div_scroll_1" style="width:566px; padding:0 0 0 0;">
											<table id="listTab" cellspacing="0" cellpadding="0" border="1" summary="권리자 저작권찾기 신청저작물정보 폼입니다." class="grid ce"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
												<colgroup>
												<col width="5%">
												<col width="*">
												<col width="14%">
												<col width="15%">
												<col width="16%">
												<col width="15%">
												</colgroup>
												<thead>
													<tr>
														<th rowspan="2" scope="col">순번</th>
														<th rowspan="2" scope="col"><label class="necessary white">저작물명</label></th>
														<th rowspan="2" scope="col"><label class="necessary white">장르</label></th>
														<th scope="col">방송사</th>
														<th scope="col">방송매체</th>
														<th scope="col">연출</th>
													</tr>
													<tr>
														<th scope="col">방송회차</th>
														<th scope="col"><label class="necessary white">방송일자</label></th>
														<th scope="col">제작사</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${rghtPrps.keyId}" var="prpsList">
													<c:set var="ii" value="${ii+1}"/>
													<tr>
														<td rowspan="2" class="ce">
															<input name="displaySeq2" title="순번" id="displaySeq2_${ii}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${ii}"/>
														</td>
														<td rowspan="2" class="lft">${rghtPrps.TITLE_ARR[ii-1]}</td>
														<c:if test="${!empty rghtPrps.keyId[ii-1]}">
														<td rowspan="2" class="ce">${rghtPrps.GENRE_KIND_NAME_ARR[ii-1] }&nbsp;</td>
														<td class="ce">${rghtPrps.BROAD_STAT_NAME_ARR[ii-1]}&nbsp;</td>
														<td class="ce">${rghtPrps.BROAD_MEDI_NAME_ARR[ii-1]}&nbsp;</td>
														</c:if>
														<c:if test="${empty rghtPrps.keyId[ii-1]}">
														<td rowspan="2" class="ce">
														<c:forEach items="${genreList}" var="genreList">
															<c:if test="${rghtPrps.GENRE_KIND_ARR[ii-1] == genreList.code}">${genreList.codeName}</c:if>
														</c:forEach>&nbsp;</td>
														<td class="ce">
														<c:forEach items="${broadList}" var="broadList">
															<c:if test="${rghtPrps.BROAD_STAT_ARR[ii-1] == broadList.code}">${broadList.codeName}</c:if>
														</c:forEach>
														&nbsp;</td>
														<td class="ce"><c:choose>
															<c:when test="${rghtPrps.BROAD_MEDI_ARR[ii-1] == 'T'}">TV</c:when>
															<c:when test="${rghtPrps.BROAD_MEDI_ARR[ii-1] == 'R'}">RADIO</c:when>
															<c:when test="${rghtPrps.BROAD_MEDI_ARR[ii-1] == 'C'}">CABLE</c:when>
															<c:when test="${rghtPrps.BROAD_MEDI_ARR[ii-1] == 'I'}">INTERNET</c:when>
															<c:when test="${rghtPrps.BROAD_MEDI_ARR[ii-1] == 'X'}">기타</c:when>
														</c:choose>&nbsp;</td>
														</c:if>
														<td class="ce">${rghtPrps.DIRECT_ARR[ii-1] }&nbsp;</td>
													</tr>
													<tr>
														<td class="ce">${rghtPrps.BROAD_ORD_ARR[ii-1] }&nbsp;</td>
														<td class="ce">${rghtPrps.BROAD_DATE_ARR[ii-1] }&nbsp;</td>
														<td class="ce">${rghtPrps.MAKER_ARR[ii-1] }&nbsp;
														<!-- hidden Value -->
														<input type="hidden" name="iChkVal" value="${rghtPrps.keyId[ii-1] }"/>
														<input type="hidden" name="TITLE" value="${rghtPrps.TITLE_ARR_TRNS[ii-1] }" />
														<input type="hidden" name="GENRE_KIND" value="${rghtPrps.GENRE_KIND_ARR[ii-1] }" />
														<input type="hidden" name="BROAD_MEDI" value="${rghtPrps.BROAD_MEDI_ARR[ii-1] }" />
														<input type="hidden" name="BROAD_STAT" value="${rghtPrps.BROAD_STAT_ARR[ii-1] }" />
														<input type="hidden" name="GENRE_KIND_NAME" value="${rghtPrps.GENRE_KIND_NAME_ARR[ii-1] }" />
														<input type="hidden" name="BROAD_MEDI_NAME" value="${rghtPrps.BROAD_MEDI_NAME_ARR_TRNS[ii-1] }" />
														<input type="hidden" name="BROAD_STAT_NAME" value="${rghtPrps.BROAD_STAT_NAME_ARR_TRNS[ii-1] }" />
														<input type="hidden" name="BROAD_ORD" value="${rghtPrps.BROAD_ORD_ARR[ii-1] }" />
														<!--<input type="hidden" name="BROAD_DATE" value="${rghtPrps.BROAD_DATE_ARR[ii-1] }" />-->
														<input type="hidden" name="BROAD_DATE_${ii}" value="${rghtPrps.BROAD_DATE_ARR[ii-1] }" />
														<input type="hidden" name="BROAD_DATE"/>
														<input type="hidden" name="DIRECT" value="${rghtPrps.DIRECT_ARR_TRNS[ii-1] }"/>
														<input type="hidden" name="MAKER" value="${rghtPrps.MAKER_ARR_TRNS[ii-1] }"/>
														<input type="hidden" name="WRITER" />
														</td>
													</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
									</c:if>
									<!-- 이용자의 저작권 찾기 끝 -->
								</td>
							</tr>
							<tr>
								<th scope="row" colspan="2"><label for="PRPS_DESC" class="necessary">신청내용</label></th>
								<td>
									<%-- <textarea cols="10" rows="10" name="PRPS_DESC" id="PRPS_DESC" class="w99" title="신청내용" readonly="readonly" >${rghtPrps.PRPS_DESC}</textarea> --%>
									<% pageContext.setAttribute("line", "\n"); %>
 									${fn:replace(rghtPrps.PRPS_DESC, line, '<br/>')}
								</td>
							</tr>
							<tr>
								<th scope="row" colspan="2"><label for="file1">첨부파일<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">(증빙서류)</c:if> <br />
								<input type="checkbox" name="OFFX_LINE_RECP_VIEW" value="Y" id="file1" class="inputChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.OFFX_LINE_RECP == 'Y'}">checked="checked"</c:if> title="오프라인접수" />오프라인접수</label>
								</th>
								<td id="td_file_no" style="display:none">오프라인접수로 선택한 경우 첨부파일을 등록할 수 없습니다.</td>
								<td id="td_file_yes">
								<c:if test="${rghtPrps.OFFX_LINE_RECP != 'Y'}">
									<c:if test="${!empty fileList}">
										<c:forEach items="${fileList}" var="fileList">	
											${fileList.FILE_NAME}<!-- <a href="javascript:fn_fileDownLoad('${fileList.FILE_PATH}','${fileList.FILE_NAME}','${fileList.REAL_FILE_NAME}')">${fileList.FILE_NAME}</a> --><!-- history.back 에러 -->
												<input type="hidden" name="hddnGetRealFileName"	value="${fileList.REAL_FILE_NAME }"/>
												<input type="hidden" name="hddnGetFilePath"		value="${fileList.FILE_PATH }"/>
												<input type="hidden" name="hddnGetFileName"		value="${fileList.FILE_NAME }"/>
												<input type="hidden" name="hddnGetFileSize"		value="${fileList.FILE_SIZE }"/>
												<input type="hidden" name="hddnGetFileOrgnCode"	value="${fileList.TRST_ORGN_CODE }"/>
											<br/>
										</c:forEach>
									</c:if>
								</c:if>
								<c:if test="${rghtPrps.OFFX_LINE_RECP == 'Y'}">
									오프라인접수로 선택한 경우 첨부파일을 등록할 수 없습니다.
								</c:if>
								</td>
							</tr>
						</tbody>
					</table>
					
					<div class="btnArea">
						<p class="fl mr5"><span class="button medium gray"><a href="#1" onclick="javascript:goList();">목록</a></span></p>
						<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:rghtPrpsBack();">이전화면</a></span></p>
						<p class="fr">
							<c:if test="${rghtPrps.PRPS_DOBL_CODE == 'Y'}">
							<span class="blue2">* 저작권찾기 신청 처리 후 보상금신청화면으로 이동합니다.</span>
							</c:if>
							<span class="button medium"><a href="#1" onclick="javascript:rghtPrps();">저작권찾기 신청</a></span></p>
					</div>
				</form>
				</div>
			</div>
		</div>
		<!-- //주요컨텐츠 end -->
	</div>
</div>
<!-- //CONTAINER end -->
		
<!-- FOOTER str-->
<jsp:include page="/include/2012/footer.jsp" />
<!-- FOOTER end -->

</div>
<!-- //전체를 감싸는 DIVISION -->


<form name="srchForm" action="#">
<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
<input type="hidden" name="srchWriter" value="${srchParam.srchWriter }"/>
<input type="hidden" name="srchBroadStatName" value="${srchParam.srchBroadStatName }"/>
<input type="submit" style="display:none;">
</form>

<script type="text/javascript">
<!--
	<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1' || rghtPrps.PRPS_RGHT_CODE == '2'}">
		<c:forEach items="${rghtPrps.keyId}" var="prpsList">
			iRowIdx++;
		</c:forEach>
	</c:if>

	<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">	
		<c:forEach items="${rghtPrps.keyId}" var="prpsList">
			iRowIdx2++;
		</c:forEach>
	</c:if>
//-->
</script>
</body>
</html>
