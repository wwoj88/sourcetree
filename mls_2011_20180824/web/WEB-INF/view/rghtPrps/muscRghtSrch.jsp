<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"></script>
	
<title>저작권찾기(음악) | 저작권찾기</title>

<script type="text/JavaScript">
<!--

window.name = "muscRghtSrch";

var emptyYn = '${emptyYn}'; // 검색조건이 없을 경우 //20120220 정병호

function goPage(pageNo){
	var frm = document.ifFrm;
	
	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
		},
		onSuccess: function(req) {     
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=M";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 음악저작물 상세 팝업오픈 : 부모창에서 오픈
function openMusicDetail (crId, nrId, meId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	param += '&NR_ID='+nrId;
	param += '&ALBUM_ID='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=M'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch, width=705, height=570, scrollbars=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 음원 미리듣기 : 부모창에서 오픈
function openMp3 (url, name, openInfo) {
		parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=M'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch, width=705, height=480';
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifMuscRghtSrch"); 
	if(emptyYn != 'Y'){// 검색조건이 없을 경우 //20120220 정병호
		var totalRow = ${muscList.totalRow}
		parent.document.getElementById("totalRow").value = totalRow //+"건"; //초기페이지 예외처리하기.
	}
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	parent.frames.scrollTo(0,0); 

}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}

-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="sTotalRow" value="${muscList.totalRow}"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
	<input type="hidden" name="srchProducer" value="${srchParam.srchProducer }"/>
	<input type="hidden" name="srchAlbumTitle" value="${srchParam.srchAlbumTitle }"/>
	<input type="hidden" name="srchSinger" value="${srchParam.srchSinger }"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
	<input type="hidden" name="srchLicensor" value="${srchParam.srchLicensor}" />
	<input type="hidden" name="srchNonPerf" value="${srchParam.srchNonPerf}" />
	<input type="submit" style="display:none;"/>
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" style="width:100%;height:auto;padding:0 0 0 0;margin-top:1px;" class="tabelRound div_scroll">
		
			<table class="sub_tab3 mar_tp40" cellspacing="0" cellpadding="0" width="100%" summary="음악 저작물의 곡명, 앨범명, 발매일, 작사, 작곡, 가창, 음반제작사, 저작권찾기 정보입니다." id="tab_scroll"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="5%"/>
				<col width="*" />
				<col width="15%" />
				<col width="13%" />
				<col width="13%" />
				<col width="13%" />
				<col width="15%" />
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" scope="col">순번</th>
						<th scope="col" colspan="2">곡명</th>
						<th scope="col">작사</th>
						<th scope="col">작곡</th>
						<th scope="col">편곡</th>
						<th scope="col" rowspan="2">음반제작사</th>
					</tr>
					<tr>
						<th scope="col" rowspan="2">앨범명</th>
						<th scope="col">발매일자</th>
						<th scope="col">가창</th>
						<th scope="col">연주</th>
						<th scope="col">지휘</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${emptyYn == 'Y'}"><!-- 검색조건이 없을 경우 //20120220 정병호 -->
					<tr>
						<td class="ce" colspan="7">검색조건을 입력해 주세요.</td>
					</tr>
				</c:if>
				<c:if test="${muscList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="7">검색된 음악저작물이 없습니다.</td>
					</tr>
				</c:if>
				<c:if test="${muscList.totalRow > 0}">
					<c:forEach items="${muscList.resultList}" var="muscList">
						<c:set var="NO" value="${muscList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<!-- hidden value start -->
						<input type="hidden" name="musicTitle" value="${muscList.MUSIC_TITLE_TRNS}" />
						<input type="hidden" name="albumTitle" value="${muscList.ALBUM_TITLE_TRNS}" />
						<input type="hidden" name="issuedDate" value="${muscList.ISSUED_DATE}" />
						<input type="hidden" name="lyricist" value="${muscList.LYRICIST_TRNS}" />
						<input type="hidden" name="composer" value="${muscList.COMPOSER_TRNS}" />
						<input type="hidden" name="singer" value="${muscList.SINGER_TRNS}" />
						<input type="hidden" name="producer" value="${muscList.PRODUCER_TRNS}" />
						<input type="hidden" name="crId" value="${muscList.CR_ID}" />
						<input type="hidden" name="nrId" value="${muscList.NR_ID}" />
						<input type="hidden" name="albumId" value="${muscList.ALBUM_ID}" />
						<input type="hidden" name="arranger" value="${muscList.ARRANGER_TRNS}" />
						<input type="hidden" name="translator" value="${muscList.TRANSLATOR_TRNS}" />
						<input type="hidden" name="player" value="${muscList.PLAYER_TRNS}" />
						<input type="hidden" name="conductor" value="${muscList.CONDUCTOR_TRNS}" />
						<input type="hidden" name="featuring" value="${muscList.FEATURING_TRNS}" />
						<input type="hidden" name="icnNumb" value="${muscList.ICN_NUMB}" />
						<input type="hidden" name="albumProducedCrh" value="${muscList.ALBUM_PRODUCED_CRH_TRNS}" />
						<input type="hidden" name="nonPerf" value="${muscList.NONPERF_TRNS}" />
						<!-- hidden value end -->
						 <c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td rowspan="2" class="ev_num">${muscList.ROW_NUM}</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td rowspan="2">${muscList.ROW_NUM}</td>
						 	</c:otherwise>
						 </c:choose>
						<c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td class="ev_num" colspan="2">
									<a href="#1" class="underline black2" onclick="javascript:openMusicDetail('${muscList.CR_ID }','${muscList.NR_ID }','${muscList.ALBUM_ID }');" title="새창 열림">${muscList.MUSIC_TITLE }</a>
									<c:if test="${muscList.recFile != '' && muscList.recFile != null}">
									<a href="#1" onclick="javascript:openMp3('/common/mediaPlay.jsp?mp3Url=http://db.copyright.or.kr/cdccmp3/${muscList.recFile }','window2','location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=323,height=150,left=250, top=150, scrollbars=auto');"><img src="/images/common/ic_preview.gif" class="vtop" alt="음원 샘플듣기" /></a>
								</c:if>
								</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td colspan="2">
									<a href="#1" class="underline black2" onclick="javascript:openMusicDetail('${muscList.CR_ID }','${muscList.NR_ID }','${muscList.ALBUM_ID }');" title="새창 열림">${muscList.MUSIC_TITLE }</a>
									<c:if test="${muscList.recFile != '' && muscList.recFile != null}">
									<a href="#1" onclick="javascript:openMp3('/common/mediaPlay.jsp?mp3Url=http://db.copyright.or.kr/cdccmp3/${muscList.recFile }','window2','location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=323,height=150,left=250, top=150, scrollbars=auto');"><img src="/images/common/ic_preview.gif" class="vtop" alt="음원 샘플듣기" /></a>
									</c:if>
								</td>
						 	</c:otherwise>
						 </c:choose>
						  <c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${muscList.LYRICIST }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${muscList.LYRICIST }</td>
						 	</c:otherwise>
						 </c:choose>
						  <c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${muscList.COMPOSER }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${muscList.COMPOSER }</td>
						 	</c:otherwise>
						 </c:choose>
						 <c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${muscList.ARRANGER }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${muscList.ARRANGER }</td>
						 	</c:otherwise>
						 </c:choose>
						<c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num" rowspan="2">${muscList.PRODUCER }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce" rowspan="2">${muscList.PRODUCER }</td>
						 	</c:otherwise>
						 </c:choose>
					</tr>
					<tr>
					<c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td class="ev_num">${muscList.ALBUM_TITLE}<c:if test="${muscList.ALBUM_TITLE == null}">&nbsp;</c:if></td>
						 	</c:when>
						 	<c:otherwise>
						 		<td>${muscList.ALBUM_TITLE}<c:if test="${muscList.ALBUM_TITLE == null}">&nbsp;</c:if></td>
						 	</c:otherwise>
					</c:choose>
					<c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${muscList.ISSUED_DATE }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${muscList.ISSUED_DATE }</td>
						 	</c:otherwise>
					 </c:choose>
					<c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${muscList.SINGER }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${muscList.SINGER }</td>
						 	</c:otherwise>
					 </c:choose>
					 <c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${muscList.PLAYER }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${muscList.PLAYER }</td>
						 	</c:otherwise>
					 </c:choose>
					 <c:choose>
						 	<c:when test="${muscList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${muscList.CONDUCTOR }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${muscList.CONDUCTOR }</td>
						 	</c:otherwise>
					 </c:choose>	
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${muscList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->
		<!--contents end-->
</form>
</body>
</html>
