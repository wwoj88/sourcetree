<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>저작권정보 조회(뉴스) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script type="text/javascript" src="/js/deScript.js"></script>

</head>
<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>뉴스저작물 상세조회</h1>
		</div>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<p class="red p11" ><strong>* 저작권정보 변경신청에 관한 문의는 검색화면의 단체로 연락바랍니다.</strong></p>
			<div class="section">
				<h2 class="mt15">뉴스저작물정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="뉴스저작물정보 입니다.">
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody> 
						<tr>
							<th scope="row">기사제목</th>
							<td colspan="3">${news.TITLE}</td>
						</tr>
						<tr>
							<th scope="row">기사부제목</th>
							<td colspan="3">${news.SUBTITLE}</td>
						</tr>
						<tr>
							<th scope="row">기사발행시간</th>
							<td colspan="3">${news.ARTICL_PUBC_SDATE}</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="section mt20">
				<h2>권리정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="뉴스저작물의 권리정보 입니다.">
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">언론사명</th>
							<td>${news.PROVIDER_NM}</td>
							<th scope="row">한국언론진흥재단</th>
							<td>
								<c:if test="${news.TRUST_YN == 'Y' }">신탁</c:if>
								<c:if test="${news.TRUST_YN == 'N' }">비신탁</c:if>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div>
			<p class="fl mt20 p11" >* 디지털저작권거래소(kdce.or.kr)에서 저작권 이용계약 서비스를 이용할 수 있습니다. </p>
			<a class="fr mt15"  href="http://kdce.or.kr/user/main.do" target="_blank"><img src="/images/2012/button/btn_go_site.gif" alt="바로가기" title="바로가기" /></a><br/>
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close();" class="pop_close" ><img src="/images/2012/button/pop_close.gif" alt="닫기" title="이 창을 닫습니다." /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
