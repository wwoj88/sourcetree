<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2012/prototype.js"></script>
<title>권리찾기(이미지) | 저작권찾기</title>
<script type="text/JavaScript">
<!--

window.name = "imageRghtSrch";

function fn_frameList(){
	var frm = document.ifFrm;
	
	new Ajax.Request('/test', {   
		onLoading: function() {
			//parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {
			parent.frames.showAjaxBox();
			//alert("완료");
		} 
	});

	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=I";
	frm.page_no.value = 1;
	frm.submit();
}

function goPage(pageNo){
	var frm = document.ifFrm;
	
	new Ajax.Request('/test', {   
		onLoading: function() {     
			//parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});

	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=I";
	frm.page_no.value = pageNo;
	frm.submit();
}


function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 이미지저작물 상세 팝업오픈 : 부모창에서 오픈
function openImageDetail(workFileNm, workNm) {

	var param = '';
	
	param = '&WORK_FILE_NAME='+workFileNm;
	param += '&WORK_NAME='+workNm;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=I'+param
	var name = '';
	var openInfo = 'target=imageRghtSrch, width=320, height=310';
	
	window.open(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifImageRghtSrch"); 
	
	var totalRow = ${imageList.totalRow};
	totalRow = cfInsertComma(totalRow);
	parent.document.getElementById("totalRow").value = totalRow+"건";
	
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();

	// 부모창 알림- 추가저작물에 대한 중복확인검색 시 사용.
	if('${parentGubun}' == 'Y') {
	
		var srchTitleObj = document.ifFrm.srchWorkName; 
		var srchTitle = srchTitleObj.value;
		
		if(totalRow=='0') {
			alert('저작물명 ['+srchTitle+'] 의 정보는 존재하지 않습니다. 계속진행 해주세요. \n' );
		}
		else {
			// 검색결과가 있는경우
			alert('저작물명 ['+srchTitle+'] 으로 '+totalRow+'건 검색되었습니다. \n추가등록을 원하는 저작물정보가 존재하는 경우 아래 검색결과에서 [선택 추가]해 주세요.' );
			srchTitleObj.focus();
		}
	}
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="dtlYn" value="Y"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
	
		<!-- search form str -->
		<fieldset class="w99">
			<legend></legend>
			<p class="box1">
				<label for="sch2" class="schDot1 pl10">
				<strong>이미지명</strong></label>
				<input class="inputData" id="sch1" name="srchWorkName" value="${srchParam.srchWorkName}" size="11"/>
				<label for="sch2" class="schDot1 ml5 pl10">
				<strong>출판사</strong></label>
				<input class="inputData" id="sch2" name="srchLishComp" value="${srchParam.srchLishComp}" size="11"/>
				<label for="sch3" class="schDot1 ml5 pl10">
				<strong>저자명</strong></label>
				<input class="inputData" id="sch3" name="srchCoptHodr" value="${srchParam.srchCoptHodr}" size="11"/>
				<span class="button small black"><input type="submit" value="검색" onclick="javascript:fn_frameList();"></input></span>
			</p>
		</fieldset>
		<div id="div_scroll" class="tabelRound div_scroll" style="width:569px;height:auto;padding:0 0 0 0;">
		<table id="tab_scroll" cellspacing="0" cellpadding="0" border="1" summary="이미지저작물의 이미지명, 출판사, 집필진, 출판년도, 저작권자, 분류 정보입니다." class="grid mt5 tableFixed"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
			<colgroup>
			<col width="5%" />
			<col width="*" />
			<col width="13%" />
			<col width="14%" />
			<col width="12%" />
			<col width="13%" />
			<col width="13%" />
			</colgroup>
			<thead>
				<tr>
					<th scope="col"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','ifrmChk',this);" style="cursor:pointer;" title="전체선택" /></th>
					<th scope="col">이미지명</th>
					<th scope="col">출판사</th>
					<th scope="col">집필진</th>
					<th scope="col">출판년도</th>
					<th scope="col">저작권자</th>
					<th scope="col">분류</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${imageList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="7">검색된 이미지저작물이 없습니다.</td>
					</tr>
				</c:if>
				
				<c:if test="${imageList.totalRow > 0}">
					<c:forEach items="${imageList.resultList}" var="imageList">
						<c:set var="NO" value="${imageList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
						<tr>
							<td class="ce">
							<input type="checkbox" name="ifrmChk" class="vmid" value="${imageList.IMGE_SEQN }" style="cursor:pointer;" title="선택" />
							<!-- hidden value start -->
							<input type="hidden" name="imageSeqn" value='${imageList.IMGE_SEQN}' />
							<input type="hidden" name="crId" value="${imageList.CR_ID}" />
							<input type="hidden" name="workName" value='${imageList.WORK_NAME_TRNS}' />
							<input type="hidden" name="coptHodr" value='${imageList.COPT_HODR_TRNS}' />
							<input type="hidden" name="lishComp" value="${imageList.LISH_COMP_TRNS}" />
							<input type="hidden" name="wterDivs" value='${imageList.WTER_DIVS_TRNS}' />
							<input type="hidden" name="usexYear" value='${imageList.USEX_YEAR}' />
							<input type="hidden" name="imageDivs" value='${imageList.IMAGE_DIVS_TRNS}' />
							<input type="hidden" name="icnNumb" value="${imageList.ICN_NUMB}" />
							<input type="hidden" name="workFileName" value='${imageList.WORK_FILE_NAME}' />
							<input type="hidden" name="subjInmtSeqn" value='${imageList.SUBJ_INMT_SEQN}' />
							
							<!-- hidden value end -->
							</td>
							<td class="lft"><a class="underline black2" href="#1" onclick="javascript:openImageDetail('${imageList.WORK_FILE_NAME }','${imageList.WORK_NAME }');" title="새창 열림">${imageList.WORK_NAME }</a></td>
							<td>${imageList.LISH_COMP }</td>
							<td class="ce">${imageList.WTER_DIVS }</td>
							<td class="ce">${imageList.USEX_YEAR }</td>
							<td class="ce">${imageList.COPT_HODR }</td>
							<td class="ce">${imageList.IMAGE_DIVS }</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${imageList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
