<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2012/prototype.js"> </script>

<title>저작권찾기(도서) | 저작권찾기</title>
<script type="text/JavaScript">
<!--
var emptyYn = '${emptyYn}'; // 검색조건이 없을 경우 //20120220 정병호

window.name = "bookRghtSrch";
	
function goPage(pageNo){
	
	var frm = document.ifFrm;
	
	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
		},
		onSuccess: function(req) {     
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=B";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 어문저작물 상세 팝업오픈 : 부모창에서 오픈
function openBookDetail( crId, nrId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	param += '&NR_ID='+nrId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=B'+param
	var name = '';
	var openInfo = 'target=bookRghtSrch, width=705, height=500, scrollbars=yes';
	parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=O'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch, width=705, height=480';
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifBookRghtSrch"); 
	
	
	if(emptyYn != 'Y'){// 검색조건이 없을 경우 //20120220 정병호
		var totalRow = ${bookList.totalRow}
		parent.document.getElementById("totalRow").value = totalRow //+"건"; //초기페이지 예외처리하기.
	}
	
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	
	parent.frames.scrollTo(0,0);
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}

-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="sTotalRow" value="${bookList.totalRow}"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
	<input type="hidden" name="srchPublisher" value="${srchParam.srchPublisher }"/>
	<input type="hidden" name="srchBookTitle" value="${srchParam.srchBookTitle }"/>
	<input type="hidden" name="srchLicensorNm" value="${srchParam.srchLicensorNm }"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
	<input type="hidden" name="srchLicensor" value="${srchParam.srchLicensor }"/>
	<input type="submit" style="display:none;"/>
	
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:auto;padding:0 0 0 0;margin-top:1px;">
			<table class="sub_tab3 mar_tp40" cellspacing="0" cellpadding="0" width="100%" id="tab_scroll" summary="도서 저작물의 저작물명, 도서명, 발배년도, 저자, 역자, 출판사, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="5%"/>
				<col width="*" />
				<col width="25%" />
				<col width="13%" />
				<col width="13%" />
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" scope="col">순번</th>
						<th scope="col" rowspan="2">작품명</th>
						<th scope="col" colspan="2">도서명</th>
						<th scope="col">작가</th>
					</tr>
					<tr>
						<th scope="col">출판사</th>
						<th scope="col">발행일자</th>
						<th scope="col">역자</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${emptyYn == 'Y'}"><!-- 검색조건이 없을 경우 //20120220 정병호 -->
					<tr>
						<td class="ce" colspan="5">검색조건을 입력해 주세요.</td>
					</tr>
				</c:if>
				<c:if test="${bookList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="5">검색된 음악저작물이 없습니다.</td>
					</tr>
				</c:if>
				<c:if test="${bookList.totalRow > 0}">
					<c:forEach items="${bookList.resultList}" var="bookList">
						<c:set var="NO" value="${bookList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
					<c:choose>
						 	<c:when test="${bookList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num" rowspan="2" >${bookList.ROW_NUM}</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce" rowspan="2" >${bookList.ROW_NUM}</td>
						 	</c:otherwise>
					</c:choose>
					<c:choose>
						 	<c:when test="${bookList.ROW_NUM % 2 == 0}">
						 		<td rowspan="2" class="ev_num">
						<!-- hidden value start -->
							<input type="hidden" name="title" value="${bookList.TITLE_TRNS}" />
							<input type="hidden" name="bookTitle" value="${bookList.BOOK_TITLE_TRNS}" />
							<input type="hidden" name="subTitle" value="${bookList.SUBTITLE_TRNS}" />
							<input type="hidden" name="issuedDate" value="${bookList.FIRST_EDITION_YEAR}" />
							<input type="hidden" name="writer" value="${bookList.LICENSOR_NAME_KOR_TRNS}" />
							<input type="hidden" name="translator" value="${bookList.TRANSLATOR_TRNS}" />
							<input type="hidden" name="publisher" value="${bookList.PUBLISHER_TRNS}" />
							<input type="hidden" name="crId" value="${bookList.CR_ID}" />
							<input type="hidden" name="nrId" value="${bookList.BOOK_NR_ID}" />
							<input type="hidden" name="publishType" value="${bookList.PUBLISH_TYPE}" />
							<input type="hidden" name="retrieveType" value="${bookList.RETRIEVE_TYPE}" />
							<input type="hidden" name="icnNumb" value="${bookList.ICN_NUMB}" />
							<!-- hidden value end -->
							 <a class="underline black2" href="#1" onclick="javascript:openBookDetail('${bookList.CR_ID }','${bookList.BOOK_NR_ID }');" style="cursor:pointer;" title="새창 열림">${bookList.TITLE }</a>
						</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td rowspan="2">
						<!-- hidden value start -->
							<input type="hidden" name="title" value="${bookList.TITLE_TRNS}" />
							<input type="hidden" name="bookTitle" value="${bookList.BOOK_TITLE_TRNS}" />
							<input type="hidden" name="subTitle" value="${bookList.SUBTITLE_TRNS}" />
							<input type="hidden" name="issuedDate" value="${bookList.FIRST_EDITION_YEAR}" />
							<input type="hidden" name="writer" value="${bookList.LICENSOR_NAME_KOR_TRNS}" />
							<input type="hidden" name="translator" value="${bookList.TRANSLATOR_TRNS}" />
							<input type="hidden" name="publisher" value="${bookList.PUBLISHER_TRNS}" />
							<input type="hidden" name="crId" value="${bookList.CR_ID}" />
							<input type="hidden" name="nrId" value="${bookList.BOOK_NR_ID}" />
							<input type="hidden" name="publishType" value="${bookList.PUBLISH_TYPE}" />
							<input type="hidden" name="retrieveType" value="${bookList.RETRIEVE_TYPE}" />
							<input type="hidden" name="icnNumb" value="${bookList.ICN_NUMB}" />
							<!-- hidden value end -->
							 <a class="underline black2" href="#1" onclick="javascript:openBookDetail('${bookList.CR_ID }','${bookList.BOOK_NR_ID }');" style="cursor:pointer;" title="새창 열림">${bookList.TITLE }</a>
						</td>
						 	</c:otherwise>
					</c:choose>	
					<c:choose>
						 	<c:when test="${bookList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num" colspan="2">${bookList.BOOK_TITLE }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce" colspan="2">${bookList.BOOK_TITLE }</td>
						 	</c:otherwise>
					</c:choose>	
					<c:choose>
						 	<c:when test="${bookList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${bookList.LICENSOR_NAME_KOR }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${bookList.LICENSOR_NAME_KOR }</td>
						 	</c:otherwise>
					</c:choose>	
					</tr>
					<tr>
					<c:choose>
						 	<c:when test="${bookList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${bookList.PUBLISHER }<c:if test="${bookList.PUBLISHER == null}">&nbsp;</c:if></td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${bookList.PUBLISHER }<c:if test="${bookList.PUBLISHER == null}">&nbsp;</c:if></td>
						 	</c:otherwise>
					</c:choose>
					<c:choose>
						 	<c:when test="${bookList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${bookList.FIRST_EDITION_YEAR }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${bookList.FIRST_EDITION_YEAR }</td>
						 	</c:otherwise>
					</c:choose>
					<c:choose>
						 	<c:when test="${bookList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${bookList.TRANSLATOR }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${bookList.TRANSLATOR }</td>
						 	</c:otherwise>
					</c:choose>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${bookList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
