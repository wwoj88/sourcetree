<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
<title>저작권찾기(방송) | 저작권찾기</title>
<script type="text/JavaScript">
<!--

var emptyYn = '${emptyYn}'; // 검색조건이 없을 경우 //20120220 정병호

window.name = "broadcastRghtSrch";

	
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
		},
		onSuccess: function(req) {     
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=R";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 방송대본저작물 상세 팝업오픈 : 부모창에서 오픈
function openBroadcastDetail( crId ) {

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=R'+param
	var name = '';
	var openInfo = 'target=broadcastRghtSrch, width=705, height=500, scrollbars=yes ';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=R'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch, width=705, height=480';
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifBroadcastRghtSrch"); 
	
	if(emptyYn != 'Y'){// 검색조건이 없을 경우 //20120220 정병호
		var totalRow = ${broadcastList.totalRow}
		parent.document.getElementById("totalRow").value = totalRow //+"건"; //초기페이지 예외처리하기.
	}
	
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	
	parent.frames.scrollTo(0,0);
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}

-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="sTotalRow" value="${broadcastList.totalRow}"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchProgName" value="${srchParam.srchProgName }"/>
	<input type="hidden" name="srchMaker" value="${srchParam.srchMaker }"/>
	<input type="submit" style="display:none;"/>
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:auto;padding:0 0 0 0;margin-top:1px;">
			<table id="tab_scroll" class="sub_tab3 mar_tp40" cellspacing="0" cellpadding="0" width="100%" summary="방송 저작물의 저작물명, 프로그램명, 프로그램 회차, 방송일자, 매체, 채널, 프로그램등급, 제작자, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="5%"/>
				<col width="*" />
				<col width="17%" />
				<col width="15%" />
				<col width="15%" />
				<col width="15%" />
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" scope="col">순번</th>
						<th scope="col" rowspan="2">저작물명</th>
						<th scope="col" colspan="2">프로그램명</th>
						<th scope="col">방송일자</th>
						<th scope="col">제작자</th>
					</tr>
					<tr>
						<th scope="col">회차</th>
						<th scope="col">등급</th>
						<th scope="col">매체</th>
						<th scope="col">채널</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${emptyYn == 'Y'}"><!-- 검색조건이 없을 경우 //20120220 정병호 -->
					<tr>
						<td class="ce" colspan="6">검색조건을 입력해 주세요.</td>
					</tr>
				</c:if>
				<c:if test="${broadcastList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="6">검색된 방송저작물이 없습니다.</td>
					</tr>
				</c:if>
				<c:if test="${broadcastList.totalRow > 0}">
					<c:forEach items="${broadcastList.resultList}" var="broadcastList">
						<c:set var="NO" value="${broadcastList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<c:choose>
						 	<c:when test="${broadcastList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num" rowspan="2" >${broadcastList.ROW_NUM}</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce" rowspan="2" >${broadcastList.ROW_NUM}</td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${broadcastList.ROW_NUM % 2 == 0}">
						 		<td class="ev_num" style="cursor:pointer;" rowspan="2">
									<!-- hidden value start -->
									<input type="hidden" name="title" value="${broadcastList.TITLE_TRNS}" />
									<input type="hidden" name="progName" value="${broadcastList.PROG_NAME_TRNS}" />
									<input type="hidden" name="progOrdSeq" value="${broadcastList.PROG_ORDSEQ_TRNS}" /> <!-- 프로그램회차 -->
									<input type="hidden" name="broadDate" value="${broadcastList.BROAD_DATE}" />
									<input type="hidden" name="mediCode" value="${broadcastList.MEDI_CODE}" />
									<input type="hidden" name="chnlCode" value="${broadcastList.CHNL_CODE}" />
									<input type="hidden" name="mediCodeName" value="${broadcastList.MEDI_CODE_NAME_TRNS}" />
									<input type="hidden" name="chnlCodeName" value="${broadcastList.CHNL_CODE_NAME_TRNS}" />
									<input type="hidden" name="progGrad" value="${broadcastList.PROG_GRAD}" />
									<input type="hidden" name="crId" value="${broadcastList.CR_ID}" />
									<input type="hidden" name="maker" value="${broadcastList.MAKER_TRNS}" />
									<input type="hidden" name="icnNumb" />
									<!-- hidden value end -->
									<a href="#1" class="underline black2" onclick="javascript:openBroadcastDetail('${broadcastList.CR_ID }');" title="새창 열림">${broadcastList.TITLE }</a>
								</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td style="cursor:pointer;" rowspan="2">
									<!-- hidden value start -->
									<input type="hidden" name="title" value="${broadcastList.TITLE_TRNS}" />
									<input type="hidden" name="progName" value="${broadcastList.PROG_NAME_TRNS}" />
									<input type="hidden" name="progOrdSeq" value="${broadcastList.PROG_ORDSEQ_TRNS}" /> <!-- 프로그램회차 -->
									<input type="hidden" name="broadDate" value="${broadcastList.BROAD_DATE}" />
									<input type="hidden" name="mediCode" value="${broadcastList.MEDI_CODE}" />
									<input type="hidden" name="chnlCode" value="${broadcastList.CHNL_CODE}" />
									<input type="hidden" name="mediCodeName" value="${broadcastList.MEDI_CODE_NAME_TRNS}" />
									<input type="hidden" name="chnlCodeName" value="${broadcastList.CHNL_CODE_NAME_TRNS}" />
									<input type="hidden" name="progGrad" value="${broadcastList.PROG_GRAD}" />
									<input type="hidden" name="crId" value="${broadcastList.CR_ID}" />
									<input type="hidden" name="maker" value="${broadcastList.MAKER_TRNS}" />
									<input type="hidden" name="icnNumb" />
									<!-- hidden value end -->
									<a href="#1" class="underline black2" onclick="javascript:openBroadcastDetail('${broadcastList.CR_ID }');" title="새창 열림">${broadcastList.TITLE }</a>
								</td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${broadcastList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num" colspan="2">${broadcastList.PROG_NAME }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce" colspan="2">${broadcastList.PROG_NAME }</td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${broadcastList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${broadcastList.BROAD_DATE }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${broadcastList.BROAD_DATE }</td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${broadcastList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${broadcastList.MAKER }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${broadcastList.MAKER }</td>
						 	</c:otherwise>
						</c:choose>	
					</tr>
					<tr>
						<c:choose>
						 	<c:when test="${broadcastList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${broadcastList.PROG_ORDSEQ}<c:if test="${broadcastList.PROG_ORDSEQ==null}">&nbsp;</c:if></td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${broadcastList.PROG_ORDSEQ}<c:if test="${broadcastList.PROG_ORDSEQ==null}">&nbsp;</c:if></td>
						 	</c:otherwise>
						</c:choose>	
						<c:choose>
						 	<c:when test="${broadcastList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${broadcastList.PROG_GRAD }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${broadcastList.PROG_GRAD }</td>
						 	</c:otherwise>
						</c:choose>	
						<c:choose>
						 	<c:when test="${broadcastList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${broadcastList.MEDI_CODE_NAME }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${broadcastList.MEDI_CODE_NAME }</td>
						 	</c:otherwise>
						</c:choose>	
						<c:choose>
						 	<c:when test="${broadcastList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${broadcastList.CHNL_CODE_NAME }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${broadcastList.CHNL_CODE_NAME }</td>
						 	</c:otherwise>
						</c:choose>	
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${broadcastList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
