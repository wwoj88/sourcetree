<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>저작권정보 조회(방송) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>방송저작물 상세조회</h1>
		</div>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<p class="red p11" ><strong>* 저작권정보 변경신청에 관한 문의는 검색화면의 단체로 연락바랍니다.</strong></p>
			<div class="section">
				<h2 class="mt15">저작물정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="방송저작물 정보 입니다.">
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">저작물제목</th>
							<td colspan="3">${broadcast.TITLE}</td>
						</tr>
						<tr>
							<th scope="row">저작물부제목</th>
							<td colspan="3">${broadcast.SUBTITLE}</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="section mt20">
				<h2>프로그램정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="영화저작물의 매체정보 입니다.">
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">프로그램코드</th>
							<td>${broadcast.PROG_CODE}</td>
							<th scope="row">프로그램명</th>
							<td>${broadcast.PROG_NAME}</td>
						</tr>
						<tr>
							<th scope="row">원제</th>
							<td>${broadcast.ORGI_NAME}</td>
							<th scope="row">매체코드</th>
							<td>${broadcast.MEDI_CODE_NAME}</td>
						</tr>
						<tr>
							<th scope="row">채널코드</th>
							<td>${broadcast.CHNL_CODE_NAME}</td>
							<th scope="row">방송위장르</th>
							<td>${broadcast.BROAD_GENR}</td>
						</tr>
						<tr>
							<th scope="row">프로그램ID</th>
							<td>${broadcast.PROG_ID}</td>
							<th scope="row">프로그램회차</th>
							<td>${broadcast.PROG_ORDSEQ}</td>
						</tr>
						<tr>
							<th scope="row">부제</th>
							<td>${broadcast.SUBPR_NAME}</td>
							<th scope="row">소제</th>
							<td>${broadcast.SMPRG_NAME}</td>
						</tr>
						<tr>
							<th scope="row">방송일자</th>
							<td>${broadcast.BROAD_DATE}</td>
							<th scope="row">부순명칭</th>
							<td>${broadcast.PART_NAME}</td>
						</tr>
						<tr>
							<th scope="row">주요내용</th>
							<td colspan="3">${broadcast.MAIN_STORY}</td>
						</tr>
						<tr>
							<th scope="row">제작품질</th>
							<td>
								<input type="radio" name="radio" disabled="disabled" <c:if test="${broadcast.PROD_QULTY == '1'}">checked="checked"</c:if> />Y
								<input type="radio" name="radio" disabled="disabled" <c:if test="${broadcast.PROD_QULTY == '2'}">checked="checked"</c:if> />N
							</td>
							<th scope="row">프로그램등급</th>
							<td>${broadcast.PROG_GRAD}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="section mt20">
				<h2>권리정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="방송저작물의 권리정보 입니다.">
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">제작사</th>
							<td colspan="3">${broadcast.MAKER}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="닫기" title="이 창을 닫습니다." /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
