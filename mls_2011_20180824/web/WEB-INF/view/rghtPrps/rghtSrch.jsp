<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>권리찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/general.js"></script>	  
<script type="text/JavaScript">
<!--
//iframe 검색
function fn_frameList()
{
	var div = '${DIVS }';
	var frm = document.frm;
	
	if(div == 'M'){
		frm.target = "muscRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=muscList&page_no=1";
	}
	
	frm.method = "post";
	frm.submit();
}

// 상세팝업
function openDetail(url, name, openInfo)
{
	window.open(url, name, openInfo);
}

// 권리찾기신청
function goRghtPrps()
{
	// 아이프레임 
	var frm = muscRghtSrch.ifFrm;
		
		//alert(frm.iChk.length);
		
		if(inspectCheckBoxField(frm.iChk))
		{
			conValue = confirm('선택된 항목을 권리찾기신청합니다.');
			
			if(conValue) 
			{		
				    var ifrmInputVal = (getCheckStr(frm, 'Y'));
				    
				    frm = document.hidForm;
				    
				    frm.ifrmInput.value = ifrmInputVal;
				    frm.userIdnt.value="test";

					frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=M";
					frm.method = "post";
					
					frm.submit();
			}
			
		}
		else 
		{
			alert('항목을 선택 해 주세요.');
		}
	
}

// 리사이즈
function resizeIFrame(name) {

  var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
  document.getElementById(name).height= the_height+5;

}

// 선택된 저작물 추가
function fn_add(){
	
	//iFrame 항목
	var albumTitleObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumTitle");
	var issuedDateObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("issuedDate");
	var lyricistObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("lyricist");
	var composerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("composer");
	var singerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("singer");
	var producerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("producer");
	var chkObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("iChk");
	var crIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("crId");
	var nrIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("nrId");
	var albumIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumId");
	var arrangerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("arranger");
	var translatorObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("translator");
	var musicTitleObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("musicTitle");
	var playerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("player");
	var conductorObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("conductor");
	var featuringObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("featuring");
	var icnNumbObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("icnNumb");
	var albumProducedCrhObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumProducedCrh");
	
	// 선택 목록
	var selChkObjs = document.getElementsByName("chk");
	var selCrIdObjs = document.getElementsByName("crId");
	var selNrIdObjs = document.getElementsByName("nrId");
	var selAlbumIdObjs = document.getElementsByName("albumId");
	
	var params = "";
	var frm = document.frm;

	var isExistYn = "N";	//중복여부	
	var isAdd	= false;	//줄 추가여부
	
	var count = 0;
	
	for(var i = 0; i < chkObjs.length; i++) {

		if(chkObjs[i].checked == true && selChkObjs.length > 0) {
			//중복여부 검사
			for(var j=0; j<selChkObjs.length; j++) {
				if(crIdObjs[i].value == selCrIdObjs[j].value && nrIdObjs[i].value == selNrIdObjs[j].value && albumIdObjs[i].value == selAlbumIdObjs[j].value) {
					isExistYn = "Y";
				}
			}
			
			if(isExistYn == "N") {
				//줄 추가여부
				isAdd = true;
				count ++;
				
			}else{
				alert("선택된 저작물 ["+musicTitleObjs[i].value+"]는 이미 추가된 저작물입니다.");
				return;
			}
			
		} else if(chkObjs[i].checked == true && selChkObjs.length == 0) { //권리찾기 선택목록이 비웠을때 
			//기본 알림줄 삭제(선택된 목록이 없습니다.)
			var oDummyTr = document.getElementById("dummyTr")
			if(eval(oDummyTr)) 	oDummyTr.parentNode.removeChild(oDummyTr);

			//줄 추가여부		
			isAdd = true;
			count ++;
			
		}
		
		chkObjs[i].checked = false;  // 처리한 후 체크풀기 
		
		//줄 추가실행
		if(isAdd){
			var tbody = document.getElementById("tbl_rghtPrps").getElementsByTagName("TBODY")[0];
			
			var row = document.createElement("TR");
			//tr에 id 지정
			var nLastIdx	= 0;	//현재 대상 테이블 tr의 최대번호을 담음
			var nIdIdx;			//현재 대상 테이블에 있는 tr의 Id 번호를 담음
			var oTrInfo = document.getElementById("tbl_rghtPrps").getElementsByTagName("tr");
			for(h = 0; h < oTrInfo.length; h++){
				if(oTrInfo[h].id != "undefined" && oTrInfo[h].id != ""){
					nIdIdx = Number((oTrInfo[h].id).substr("tbl_rghtPrps".length, oTrInfo[h].id.length));
					if(nLastIdx < nIdIdx)	nLastIdx = nIdIdx;
				}
			}
			
			var sNewId = "tbl_rghtPrps" + (nLastIdx + 1);		//tr에 지정할 id
		    row.id = sNewId;
			
			var td0 = document.createElement("TD");
			var td1 = document.createElement("TD"); 
			var td2 = document.createElement("TD"); 
			var td3 = document.createElement("TD"); 
			var td4 = document.createElement("TD"); 
			var td5 = document.createElement("TD");
			var td6 = document.createElement("TD");
			var td7 = document.createElement("TD");
			
			var tdData0 = "";
			tdData0 += '<input type="checkbox" name="chk"           value="0">';
			tdData0 += '<input type="hidden" name="musicTitle"        value="'+ musicTitleObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="albumTitle"        value="'+ albumTitleObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="issuedDate"        value="'+ issuedDateObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="lyricist"            value="'+ lyricistObjs[i].value         +'">';
			tdData0 += '<input type="hidden" name="composer"            value="'+ composerObjs[i].value         +'">';
			tdData0 += '<input type="hidden" name="singer"        value="'+ singerObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="producer"            value="'+ producerObjs[i].value         +'">';
			tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
			tdData0 += '<input type="hidden" name="nrId"           value="'+ nrIdObjs[i].value        +'">';
			tdData0 += '<input type="hidden" name="albumId"        value="'+ albumIdObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="arranger"        value="'+ arrangerObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="translator"        value="'+ translatorObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="player"        value="'+ playerObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="conductor"        value="'+ conductorObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="featuring"        value="'+ featuringObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="icnNumb"        value="'+ icnNumbObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="albumProducedCrh"        value="'+ albumProducedCrhObjs[i].value     +'">';
			
			td0.innerHTML = tdData0;
			td0.className = 'ce';
			td1.innerHTML = musicTitleObjs[i].value;
			td2.innerHTML = albumTitleObjs[i].value;
			td3.innerHTML = issuedDateObjs[i].value;
			td3.className = 'ce';
			td4.innerHTML = lyricistObjs[i].value;
			td4.className = 'ce';
			td5.innerHTML = composerObjs[i].value;
			td5.className = 'ce';
			td6.innerHTML = singerObjs[i].value;
			td6.className = 'ce';
			td7.innerHTML = producerObjs[i].value;
			td7.className = 'ce';
			
			row.appendChild(td0); 
			row.appendChild(td1); 
			row.appendChild(td2); 
			row.appendChild(td3); 
			row.appendChild(td4); 
			row.appendChild(td5); 
			row.appendChild(td6); 
			row.appendChild(td7); 
			 
			tbody.appendChild(row); 
			isAdd = false;
		
		}	
	}	
	
	if(count == 0){
		alert('선택된 저작물이 없습니다.');
		return;
	}	
	
	//높이 조절
	resizeSelTbl();
	
}

// 선택된 저작물 삭제
function fn_delete(){
	
	//선택 목록
	var chkObjs = document.getElementsByName("chk");
	var sCheck = 0;
	
	for(i=0; i<chkObjs.length;i++){
		if(chkObjs[i].checked){
			sCheck = 1;
		}
	}
	
	if(sCheck == 0 ){
		alert('선택된 저작물이 없습니다.');
		return;
	}

	for(var i=chkObjs.length-1; i >= 0; i--){

		var chkObj = chkObjs[i];

		if(chkObj.checked){
			// 선택된 저작물을 삭제한다.
			var oTR = findParentTag(chkObj, "TR");	
			if(eval(oTR)) 	oTR.parentNode.removeChild(oTR);
		}
	}

	//높이 조절
	resizeSelTbl();
}

//높이 조절
function resizeSelTbl(){
	var chkObjs = document.getElementsByName("chk");

	document.getElementById("div_rghtPrps").style.height = (31.5 * (chkObjs.length + 1)) + 13 + "px" ;
	
}

//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(2);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2012/common/left_h2_1.png" alt="권리찾기신청" title="" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li class="active"><a href="#1">음악<span>&lt;</span></a></li>
							<li><a href="#1">어문<span>&lt;</span></a></li>
							<li><a href="#1">이미지<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2012/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>권리찾기신청</span>&gt;<strong>권리찾기신청 목록</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2012/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>음악저작물
							<span><img src="/images/2012/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<!-- hidden form str : 아이프레임데이타 가져가는 용도-->		
					<form name="hidForm" action="#" class="sch">
						<input type="hidden" name="userIdnt" />
						<input type="hidden" name="ifrmInput" />
						<input type="submit" style="display:none;">
					</form>
					
					<!-- search form str -->					
					<form name="frm" action="#" class="sch">
					<input type="submit" style="display:none;">
						<fieldset class="sch">
							<legend>음악저작물검색</legend>
							<div class="contentsSch">
								<span class="round lb"></span><span class="round rb"></span><span class="round lt"></span><span class="round rt"></span>
								<table border="1" summary="" class="w80"><!-- 스타일제거시 테이블형태 유지를 위해 보더를 준다 -->
									<caption></caption><!-- summary가 있을 경우 생략이 가능하다 -->
									<colgroup>
										<col width="20%">
										<col width="15%">
										<col width="30%">
										<col width="15%">
										<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<td rowspan="3"><img src="/images/2012/common/schBoxBg.gif" alt="Search" /></td>
											<th><label for="sch1">곡명</label></th><!-- 레이블과 id로 타이틀과 인풋을 연결시켜준다 -->
											<td><input class="inputData" id="sch1" name="srchTitle" /></td>
											<th><label for="sch2">제작사</label></th>
											<td><input class="inputData" id="sch2" name="srchProducer" /></td>
										</tr>
										<tr>
											<th><label for="sch3">앨범명</label></th>
											<td><input class="inputData" id="sch3" name="srchAlbumTitle" /></td>
											<th><label for="sch4">가수명</label></th>
											<td><input class="inputData" id="sch4" name="srchSinger" /></td>
										</tr>
										<tr>
											<th><label for="sch5">발매일</label></th>
											<td><input class="inputData" type="text" id="sch5" name="srchStartDate" size="6" /> ~ <input class="inputData" type="text" id="sch6" name="srchEndDate" size="6" />
												
											</td>
											<th><label for="sch6">저작자명</label></th>
											<td><input class="inputData" id="sch7" name="srchLicensor" /></td>
										</tr>
									</tbody>
								</table>
								<p class="btnArea"><input type="submit" class="imgSchBtn" onclick="javascript:fn_frameList();" /></p>
							</div>
						</fieldset>
					<!-- search form end -->
					
					<div class="bbsSection">
						<div class="floatDiv mb5">
							<p class="fl pt10 pl5"><input type="text" name="totalRow" title="건수" id="totalRow" style="border:0px;font-size:12px;font-weight:700;color:black;"/></p>
							<p class="fr"><span class="button medium icon"><span class="default"></span><a href="#1" onclick="javascript:goRghtPrps();">권리찾기 신청</a></span></p>
						</div>
						
						<!-- iframe str -->
							<iframe id="ifMuscRghtSrch" title="저작물조회(방송음악)" name="muscRghtSrch" 
								width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
								scrolling="no" style="overflow-y:hidden;" 
								src="/rghtPrps/rghtSrch.do?method=muscList&page_no=1">
							</iframe>


						<!-- iframe end -->
						
						
						<!-- //버튼영역 -->
						<p class="ce mt5">
							<span class="button small type4 icon"><span class="arrow"></span><a href="#1" onclick="javascript:fn_add();">추가</a></span> 
							<span class="button small type3"><a href="#1" onclick="javascript:fn_delete();">삭제</a></span>
						</p>
						<!-- //버튼영역 -->
						
						<div class="floatDiv mb5">
							<h4 class="fl">음악저작물 권리찾기 선택목록</h4>
							<p class="fr">
								<span class="button medium icon"><span class="default"></span><a href="#1">권리찾기 신청</a></span>
							</p>
						</div>
						
						<!-- 테이블 영역입니다 -->
						<div id="div_rghtPrps" class="tabelRound">
							<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
							<table id="tbl_rghtPrps" cellspacing="0" cellpadding="0" border="1" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="5%">
								<col width="*">
								<col width="17%">
								<col width="10%">
								<col width="13%">
								<col width="13%">
								<col width="13%">
								<col width="10%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col"><input type="checkbox" title="선택" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" /></th>
										<th scope="col">곡명</th>
										<th scope="col">앨범명</th>
										<th scope="col">발매일</th>
										<th scope="col">작사</th>
										<th scope="col">작곡</th>
										<th scope="col">가수</th>
										<th scope="col">제작사</th>
									</tr>
								</thead>
								<tbody>
									<tr id="dummyTr">
										<td colspan="8" class="ce">선택된 목록이 없습니다.</td>
									</tr>
								</tbody>
							</table>
						</div>
						</form>
						<!-- //테이블 영역입니다 -->
					</div>
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->

</body>
</html>
