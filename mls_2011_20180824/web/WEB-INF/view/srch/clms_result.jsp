<%@ page pageEncoding="euc-kr"%>
<div class="h2_box floatDiv mt10">
			<h2 class="fl"><%=targetname%>(<%=replaceComma(foundnum)%>건)</h2>
				<%if(target.compareTo("total")==0 && foundnum>5){%>
					<a href="javascript:moreResult('1','<%=temptarget%>','<%=resrch%>');" class="fr more">더보기</a>
				<%}%>
</div>
	<table border="1" cellspacing="0" cellpadding="0" summary="권리자찾기 신청정보입니다." class="grid">
								<colgroup>
								<col width="10%">
								<col>
								<col>
								</colgroup>
								<tbody>
<%
			for(int row=1; row<=resultnum; row++){
				collname=Mir.GetFieldVal(row, "collname");
			  if(collname.compareTo("music")==0){
			     category="음악";
				   albumId=Mir.GetFieldVal(row, "albumId");
				   crId=Mir.GetFieldVal(row, "crId");
				   title=Mir.GetFieldVal(row, "@title");
				   albumTitle=Mir.GetFieldVal(row, "@albumTitle");
				   musicTitle=Mir.GetFieldVal(row, "@musicTitle");
				   singer=Mir.GetFieldVal(row, "singer");
				   lyricist=Mir.GetFieldVal(row, "lyricist");
				   composer=Mir.GetFieldVal(row, "composer");
				   stdCrhId=Mir.GetFieldVal(row, "stdCrhId");
				   nrId=Mir.GetFieldVal(row, "nrId");				   
				   //Hrefurl="javascript:ViewMusic('"+albumId+"','"+crId+"','"+nrId+"')";

				}else if(collname.compareTo("album")==0){
					 category="앨범";
				   albumId=Mir.GetFieldVal(row, "albumId");
				   title=Mir.GetFieldVal(row, "@albumTitle");
				   producer=Mir.GetFieldVal(row, "producer");
				   issuedDate=Mir.GetFieldVal(row, "issuedDate");
				    //Hrefurl="javascript:ViewAlbum('"+albumId+"')";

				}else if(collname.compareTo("literature")==0){
					 category="어문";
				   crId=Mir.GetFieldVal(row, "crId");
				   bookId=Mir.GetFieldVal(row, "bookId");
				   title=Mir.GetFieldVal(row, "@title");
				   bookTitle=Mir.GetFieldVal(row, "@bookTitle");
				   publisher=Mir.GetFieldVal(row, "publisher");
				   stdCrhId=Mir.GetFieldVal(row, "stdCrhId");
				   writer=Mir.GetFieldVal(row, "writer");
				   //Hrefurl="javascript:ViewLiterature('"+crId+"','"+bookId+"')";

				}else if(collname.compareTo("book")==0){
					 category="도서";
				   crId=Mir.GetFieldVal(row, "crId");
				   bookId=Mir.GetFieldVal(row, "bookId");
				   bookTitle=Mir.GetFieldVal(row, "@bookTitle");
				   publisher=Mir.GetFieldVal(row, "publisher");
				   firstEditionYear=Mir.GetFieldVal(row, "firstEditionYear");
				   //Hrefurl="javascript:ViewBook('"+crId+"','"+bookId+"')";

				}else if(collname.compareTo("movie")==0){
					 category="영화";
				   crId=Mir.GetFieldVal(row, "crId");
				   title=Mir.GetFieldVal(row, "@title");
				   stdCrhId=Mir.GetFieldVal(row, "stdCrhId");
				   insertDate=Mir.GetFieldVal(row, "insertDate");
				   genreNM=Mir.GetFieldVal(row, "@genreNM");
				   movieTypeNM=Mir.GetFieldVal(row, "@movieTypeNM");
				   viewGradeNM=Mir.GetFieldVal(row, "@viewGradeNM");
				   runTime=Mir.GetFieldVal(row, "runTime");
				   produceYear=Mir.GetFieldVal(row, "produceYear");
				   releaseDate=Mir.GetFieldVal(row, "releaseDate");
				   leadingActor=Mir.GetFieldVal(row, "@leadingActor");
				   director=Mir.GetFieldVal(row, "@director");
				   producer=Mir.GetFieldVal(row, "@producer");
				   investor=Mir.GetFieldVal(row, "@investor");
				   formatCode=Mir.GetFieldVal(row, "formatCode");
				   mediaCode=Mir.GetFieldVal(row, "mediaCode");
				   produceDate=Mir.GetFieldVal(row, "produceDate");
				   distributor=Mir.GetFieldVal(row, "distributor");
				   //Hrefurl="javascript:ViewMovie('"+crId+"','"+mediaCode+"')";

				}else if(collname.compareTo("scenario")==0){
					 category="방송대본";
				   crId=Mir.GetFieldVal(row, "crId");
				   title=Mir.GetFieldVal(row, "@title");
				   ProdSubTitle=Mir.GetFieldVal(row, "ProdSubTitle");
				   broadOrd=Mir.GetFieldVal(row, "broadOrd");
				   broadDate=Mir.GetFieldVal(row, "broadDate");
				   writer=Mir.GetFieldVal(row, "writer");
				   direct=Mir.GetFieldVal(row, "direct");
				   genreKind=Mir.GetFieldVal(row, "genreKindNM");
				   origWork=Mir.GetFieldVal(row, "origWork");
				   broadMedi=Mir.GetFieldVal(row, "broadMediNM");
				   broadStat=Mir.GetFieldVal(row, "broadStatNM");
				   stdCrhId=Mir.GetFieldVal(row, "stdCrhId");
				   //Hrefurl="javascript:ViewBroad('"+crId+"')";

				}else if(collname.compareTo("content")==0){
					 category="방송콘텐츠";
				   crId=Mir.GetFieldVal(row, "crId");
				   title=Mir.GetFieldVal(row, "@title");
				   progName=Mir.GetFieldVal(row, "progName");
				   mediCode=Mir.GetFieldVal(row, "mediCodeNM");
  			   chnlCode=Mir.GetFieldVal(row, "chnlCodeNM");
				   progId=Mir.GetFieldVal(row, "progId");
				   progOrdseq =Mir.GetFieldVal(row, "progOrdseq");
				   broadDate=Mir.GetFieldVal(row, "broadDate");
				   prodQulty=Mir.GetFieldVal(row, "prodQultyNM");
				   ptablTitle=Mir.GetFieldVal(row, "ptablTitle");
				   progGrad=Mir.GetFieldVal(row, "progGrad");
				   stdCrhId=Mir.GetFieldVal(row, "stdCrhId");
				   //Hrefurl="javascript:ViewBroadContents('"+crId+"')";

				}else if(collname.compareTo("public")==0){
					 category="공공";
				   crId=Mir.GetFieldVal(row, "crId");
				   title=Mir.GetFieldVal(row, "@title");
				   typedivlnm=Mir.GetFieldVal(row, "@content1");
				   typedivmnm=Mir.GetFieldVal(row, "@content2");
				   meandivlnm=Mir.GetFieldVal(row, "@content3");
				   meandivmnm=Mir.GetFieldVal(row, "@content4");
				   uci=Mir.GetFieldVal(row, "@content5");
				   publcrid=Mir.GetFieldVal(row, "stdCrhId");
				   anncnatn=Mir.GetFieldVal(row, "@content6");
				   //Hrefurl="javascript:ViewPubl('"+crId+"')";
				   
				}else if(collname.compareTo("art")==0){
					 category="미술";
				   crId=Mir.GetFieldVal(row, "crId");
				   stdCrhId=Mir.GetFieldVal(row, "stdCrhId");
				   title=Mir.GetFieldVal(row, "@title");
				   subtitle=Mir.GetFieldVal(row, "@content1");
				   art_div_info=Mir.GetFieldVal(row, "@content2");
				   cltn_org_nm=Mir.GetFieldVal(row, "@content3");
				   size_info=Mir.GetFieldVal(row, "@content4");
				   main_mtrl=Mir.GetFieldVal(row, "@content5");
				   stru_ftre=Mir.GetFieldVal(row, "@content6");
				   //Hrefurl="javascript:ViewArt('"+crId+"')";
				   
				}else if(collname.compareTo("character")==0){
					 category="캐릭터";
				   crId=Mir.GetFieldVal(row, "crId");
				   stdCrhId=Mir.GetFieldVal(row, "stdCrhId");
				   title=Mir.GetFieldVal(row, "@title");
				   charDesc=Mir.GetFieldVal(row, "charDesc");
				   workNmKor=Mir.GetFieldVal(row, "workNmKor");
				   //Hrefurl="javascript:ViewChar('"+crId+"')";

				}else if(collname.compareTo("music_licensor")==0){
					 category="음악저작권자";
				   licensorYmd=Mir.GetFieldVal(row, "licensorYmd");
				   licensorSeq=Mir.GetFieldVal(row, "licensorSeq");
				   stageNameSeq=Mir.GetFieldVal(row, "stageNameSeq");
				   licensorKey=licensorYmd+licensorSeq+stageNameSeq;
				   licensorCode=Mir.GetFieldVal(row, "licensorCode");

				   if(licensorYmd.compareTo("99999999")==0) viewlicensorCode="통합저작권자 미확인";
				   else viewlicensorCode=licensorCode;

				   title=Mir.GetFieldVal(row, "@licensorNameKor");
				   licensorNameKor=Mir.GetFieldVal(row, "licensorNameKor");
				   prodName=Mir.GetFieldVal(row, "prodName");
				   prodCnt=Mir.GetFieldVal(row, "prodCnt");
				   
				   if(prodCnt.compareTo("0")!=0) {
				      tempCnt=Integer.parseInt(prodCnt)-1;
				      prodCnt=tempCnt+"";
				      if(prodCnt.compareTo("0")!=0) prodName=prodName+" 외 "+prodCnt+"건";
				   }

				   licensorRole =Mir.GetFieldVal(row, "licensorRole");
				   performName=Mir.GetFieldVal(row, "performName");
				   performCnt=Mir.GetFieldVal(row, "performCnt");

				   if(performCnt.compareTo("0")!=0) {
				      tempCnt=Integer.parseInt(performCnt)-1;
				      performCnt=tempCnt+"";
				      if(performCnt.compareTo("0")!=0) performName=performName+" 외 "+performCnt+"건";
				   }
				   //Hrefurl="javascript:ViewMLicensor('"+licensorNameKor+"','"+licensorCode+"','"+title+"','"+licensorCode+"')";
				   
				}else if(collname.compareTo("public_licensor")==0){
					 category="공공저작권자";
				   licensorYmd=Mir.GetFieldVal(row, "licensorYmd");
				   licensorSeq=Mir.GetFieldVal(row, "licensorSeq");
				   stageNameSeq=Mir.GetFieldVal(row, "stageNameSeq");
				   licensorKey=licensorYmd+licensorSeq+stageNameSeq;
				   licensorCode=Mir.GetFieldVal(row, "licensorCode");
				   if(licensorYmd.compareTo("99999999")==0) viewlicensorCode="통합저작권자 미확인";
				   else viewlicensorCode=licensorCode;
				   title=Mir.GetFieldVal(row, "@licensorNameKor");
				   licensorNameKor=Mir.GetFieldVal(row, "licensorNameKor");
				   prodName=Mir.GetFieldVal(row, "prodName");
				   prodCnt=Mir.GetFieldVal(row, "prodCnt");
				   
				   if(prodCnt.compareTo("0")!=0) {
				      tempCnt=Integer.parseInt(prodCnt)-1;
				      prodCnt=tempCnt+"";
				      if(prodCnt.compareTo("0")!=0) prodName=prodName+" 외 "+prodCnt+"건";
				   }
				   licensorRole =Mir.GetFieldVal(row, "licensorRole");
				   //Hrefurl="javascript:kkk('"+licensorNameKor+"','"+licensorCode+"','"+title+"','"+licensorCode+"')";
				   
				}else if(collname.compareTo("character_licensor")==0){
					 category="캐릭터저작권자";
				   licensorYmd=Mir.GetFieldVal(row, "licensorYmd");
				   licensorSeq=Mir.GetFieldVal(row, "licensorSeq");
				   stageNameSeq=Mir.GetFieldVal(row, "stageNameSeq");
				   licensorKey=licensorYmd+licensorSeq+stageNameSeq;
				   licensorCode=Mir.GetFieldVal(row, "licensorCode");
				   licenosrId=Mir.GetFieldVal(row, "licenosrId");
				   if(licensorYmd.compareTo("99999999")==0) viewlicensorCode="통합저작권자 미확인";
				   else viewlicensorCode=licensorCode;
				   title=Mir.GetFieldVal(row, "@licensorNameKor");
				   licensorNameKor=Mir.GetFieldVal(row, "licensorNameKor");
				   prodName=Mir.GetFieldVal(row, "prodName");
				   prodCnt=Mir.GetFieldVal(row, "prodCnt");
				   natiName=Mir.GetFieldVal(row, "natiName");
				   if(prodCnt.compareTo("0")!=0) {
				      tempCnt=Integer.parseInt(prodCnt)-1;
				      prodCnt=tempCnt+"";
				      if(prodCnt.compareTo("0")!=0) prodName=prodName+" 외 "+prodCnt+"건";
				   }
				   licensorRole =Mir.GetFieldVal(row, "licensorRole");
				   //Hrefurl="javascript:ViewCharLicensor('"+licenosrId+"','"+licensorNameKor+"')";
				   
				}else if(collname.compareTo("art_licensor")==0){
					 category="미술저작권자";
				   licensorYmd=Mir.GetFieldVal(row, "licensorYmd");
				   licensorSeq=Mir.GetFieldVal(row, "licensorSeq");
				   stageNameSeq=Mir.GetFieldVal(row, "stageNameSeq");
				   licensorKey=licensorYmd+licensorSeq+stageNameSeq;
				   licensorCode=Mir.GetFieldVal(row, "licensorCode");
				   if(licensorYmd.compareTo("99999999")==0) viewlicensorCode="통합저작권자 미확인";
				   else viewlicensorCode=licensorCode;
				   title=Mir.GetFieldVal(row, "@licensorNameKor");
				   licensorNameKor=Mir.GetFieldVal(row, "licensorNameKor");
				   prodName=Mir.GetFieldVal(row, "prodName");
				   prodCnt=Mir.GetFieldVal(row, "prodCnt");
				   if(prodCnt.compareTo("0")!=0) {
				      tempCnt=Integer.parseInt(prodCnt)-1;
				      prodCnt=tempCnt+"";
				      if(prodCnt.compareTo("0")!=0) prodName=prodName+" 외 "+prodCnt+"건";
				   }
				   licensorRole =Mir.GetFieldVal(row, "licensorRole");
				   //Hrefurl="javascript:kkk('"+licensorNameKor+"','"+licensorCode+"','"+title+"','"+licensorCode+"')";

				}else if(collname.compareTo("news")==0){
					category="뉴스";
					title=Mir.GetFieldVal(row, "@title");
				  title=title.replace("<br>","");
					title=title.replace("<BR>","");
					providerVcd=Mir.GetFieldVal(row, "providerVcd");
					articlDt=Mir.GetFieldVal(row, "articlDt");
					stdCrhId=Mir.GetFieldVal(row, "stdCrhId");
					caName=Mir.GetFieldVal(row, "caName");
					crId=Mir.GetFieldVal(row, "crId");
					//Hrefurl="javascript:ViewNews('"+crId+"')";

					if(providerVcd.equals("01100101")) prodname="경향신문";
				   else if(providerVcd.equals("01100201")) prodname="국민일보";
				   else if(providerVcd.equals("01100301")) prodname="내일신문";
				   else if(providerVcd.equals("01100601")) prodname="서울신문";
				   else if(providerVcd.equals("01100701")) prodname="세계일보";
				   else if(providerVcd.equals("01101001")) prodname="한겨레";
				   else if(providerVcd.equals("01200101")) prodname="경기일보";
				   else if(providerVcd.equals("01200201")) prodname="경인일보";
				   else if(providerVcd.equals("01200401")) prodname="인천일보";
				   else if(providerVcd.equals("01300101")) prodname="강원도민일보";
	  		   else if(providerVcd.equals("01300201")) prodname="강원일보";
				   else if(providerVcd.equals("01400201")) prodname="대전일보";
				   else if(providerVcd.equals("01400351")) prodname="중도일보";
				   else if(providerVcd.equals("01400401")) prodname="중부매일";
				   else if(providerVcd.equals("01400551")) prodname="충북일보";
				   else if(providerVcd.equals("01400701")) prodname="충청투데이";
				   else if(providerVcd.equals("01500051")) prodname="경남신문";
				   else if(providerVcd.equals("01500151")) prodname="경남도민일보";
				   else if(providerVcd.equals("01500301")) prodname="경상일보";
				   else if(providerVcd.equals("01500401")) prodname="국제신문";
				   else if(providerVcd.equals("01500501")) prodname="대구일보";
				   else if(providerVcd.equals("01500601")) prodname="매일신문";
				   else if(providerVcd.equals("01500701")) prodname="부산일보";
				   else if(providerVcd.equals("01500801")) prodname="영남일보";
				   else if(providerVcd.equals("01600301")) prodname="광주일보";
				   else if(providerVcd.equals("01600501")) prodname="무등일보";
				   else if(providerVcd.equals("01600601")) prodname="새전북신문";
				   else if(providerVcd.equals("01600801")) prodname="전남일보";
				   else if(providerVcd.equals("01601001")) prodname="전북도민일보";
				   else if(providerVcd.equals("01601101")) prodname="전북일보";
				   else if(providerVcd.equals("01700101")) prodname="제민일보";
				   else if(providerVcd.equals("01700201")) prodname="한라일보";
				   else if(providerVcd.equals("02100301")) prodname="서울경제신문";
				   else if(providerVcd.equals("02100501")) prodname="파이낸셜뉴스";
				   else if(providerVcd.equals("02100801")) prodname="아시아경제";
				   else if(providerVcd.equals("04100958")) prodname="EBN산업뉴스";
				   else if(providerVcd.equals("04100158")) prodname="데일리안";
				   else if(providerVcd.equals("04100608")) prodname="브레이크뉴스";
				   else if(providerVcd.equals("04101008")) prodname="이데일리";
				   else if(providerVcd.equals("04400108")) prodname="대덕넷";
				   else if(providerVcd.equals("05200752")) prodname="김포뉴스";
				   else if(providerVcd.equals("05202702")) prodname="평택문화신문";
				   else if(providerVcd.equals("05410052")) prodname="옥천신문";
				   else if(providerVcd.equals("05420702")) prodname="홍성신문";
				   else if(providerVcd.equals("05520352")) prodname="당진시대";
				   else if(providerVcd.equals("07100251")) prodname="미디어오늘";
				   else if(providerVcd.equals("07200502")) prodname="소년한국일보";
				   else if(providerVcd.equals("07100602")) prodname="PD저널";
				   else if(providerVcd.equals("07100702")) prodname="기자협회보";
				   else if(providerVcd.equals("10100101")) prodname="스포츠서울";
				   else if(providerVcd.equals("10100201")) prodname="스포츠칸";
				   else if(providerVcd.equals("10100301")) prodname="스포츠한국";
				   else if(providerVcd.equals("01600101")) prodname="광남일보";
				   else if(providerVcd.equals("04101508")) prodname="스투닷컴";
				   else if(providerVcd.equals("06101502")) prodname="이코노믹리뷰";
				   else if(providerVcd.equals("07100902")) prodname="어린이강원일보";
				   else if(providerVcd.equals("01101101")) prodname="한국일보";
				   else if(providerVcd.equals("04100058")) prodname="노컷뉴스";
				   else if(providerVcd.equals("02100701")) prodname="헤럴드경제";
				   else if(providerVcd.equals("03100101")) prodname="코리아헤럴드";
				   else if(providerVcd.equals("04100078")) prodname="뉴스핌";
				   else if(providerVcd.equals("02100601")) prodname="한국경제신문";
				   else if(providerVcd.equals("10100401")) prodname="스포츠월드";

				}else if(collname.compareTo("news_licensor")==0){
					category="뉴스저작권자";
					licensorRole=Mir.GetFieldVal(row, "licensorRole");
					licensorNameKor=Mir.GetFieldVal(row, "licensorNameKor");
				
				}else if(collname.compareTo("broadcast")==0){
					category="방송";
				program_name=Mir.GetFieldVal(row, "@content1");
				program_code=Mir.GetFieldVal(row, "program_code");
				origin_title=Mir.GetFieldVal(row, "@content2");
				gnre_code_name=Mir.GetFieldVal(row, "@content3");
				make_ymd=Mir.GetFieldVal(row, "@content4");
				running_time=Mir.GetFieldVal(row, "@content5");
				make_type_kor=Mir.GetFieldVal(row, "@content6");
				make_name=Mir.GetFieldVal(row, "@content7");
				make_total=Mir.GetFieldVal(row, "@content8");
				make_pd=Mir.GetFieldVal(row, "@content9");
				conner_no=Mir.GetFieldVal(row, "@content10");
				broad_num=Mir.GetFieldVal(row, "@content11");
				sub_title=Mir.GetFieldVal(row, "@content12");
				make_total=Mir.GetFieldVal(row, "make_total");
				crh_id_of_ca=Mir.GetFieldVal(row, "crh_id_of_ca");
				title=Mir.GetFieldVal(row, "@title");
				make_total=Mir.GetFieldVal(row, "make_total");
				producer=Mir.GetFieldVal(row, "producer");
				writer=Mir.GetFieldVal(row, "writer");
				crId=Mir.GetFieldVal(row, "crId");
				startday=Mir.GetFieldVal(row, "startday");
				stdCrhId=Mir.GetFieldVal(row, "stdCrhId");
				//Hrefurl="javascript:ViewBBroadContents("+crId+","+broad_num+")";

				}else if(collname.compareTo("broadcast_licensor")==0){
				category="방송저작권자";
				licensorId=Mir.GetFieldVal(row, "licensorId");
				licensorRole=Mir.GetFieldVal(row, "licensorRole");
				licensorNameKor=Mir.GetFieldVal(row, "licensorNameKor");
				licensorYmd=Mir.GetFieldVal(row, "licensorYmd");
			
				if(licensorYmd.compareTo("99999999")==0) viewlicensorCode="통합저작권자 미확인";
				else viewlicensorCode=licensorId;
					
				//Hrefurl="javascript:ViewBMLicensor('"+licensorNameKor+"','"+licensorId+"','<b>"+licensorNameKor+"</b>','"+licensorId+"')";
				
				}else if(collname.compareTo("literature_licensor")==0||collname.compareTo("scenario_licensor")==0||collname.compareTo("content_licensor")==0||collname.compareTo("movie_licensor")==0){
				   
				   if(collname.compareTo("literature_licensor")==0){
				        category="어문저작권자";
				   }else if(collname.compareTo("scenario_licensor")==0){
				        category="방송대본저작권자";
				   }else if(collname.compareTo("content_licensor")==0){
				        category="방송콘텐츠저작권자";
				   }else if(collname.compareTo("movie_licensor")==0){
				   			category="영화저작권자";
				   }
				   licensorYmd=Mir.GetFieldVal(row, "licensorYmd");
				   licensorSeq=Mir.GetFieldVal(row, "licensorSeq");
				   stageNameSeq=Mir.GetFieldVal(row, "stageNameSeq");
				   licensorKey=licensorYmd+licensorSeq+stageNameSeq;
				   licensorCode=Mir.GetFieldVal(row, "licensorCode");
				   if(licensorYmd.compareTo("99999999")==0) viewlicensorCode="통합저작권자 미확인";
				   else viewlicensorCode=licensorCode;
				   title=Mir.GetFieldVal(row, "@licensorNameKor");
				   licensorNameKor=Mir.GetFieldVal(row, "licensorNameKor");
				   prodName=Mir.GetFieldVal(row, "prodName");
				   prodCnt=Mir.GetFieldVal(row, "prodCnt");

				   if(prodCnt.compareTo("0")!=0) {
				      tempCnt=Integer.parseInt(prodCnt)-1;
				      prodCnt=tempCnt+"";
				      if(prodCnt.compareTo("0")!=0) prodName=prodName+" 외 "+prodCnt+"건";
				   }
				   licensorRole =Mir.GetFieldVal(row, "licensorRole");
	/*
				   if(collname.compareTo("literature_licensor")==0) Hrefurl="javascript:ViewLLicensor('"+licensorNameKor+"','"+licensorCode+"','"+title+"','"+licensorCode+"')";
				   else if(collname.compareTo("scenario_licensor")==0) Hrefurl="javascript:ViewBLicensor('"+licensorNameKor+"','"+licensorCode+"','"+title+"','"+licensorCode+"')";
				   else if(collname.compareTo("content_licensor")==0) Hrefurl="javascript:ViewCLicensor('"+licensorNameKor+"','"+licensorCode+"','"+title+"','"+licensorCode+"')";
				   else if(collname.compareTo("movie_licensor")==0) Hrefurl="javascript:ViewMOLicensor('"+licensorNameKor+"','"+licensorCode+"','"+title+"','"+licensorCode+"')";
*/
					}//if collname
					%>
	
					<%if(collname.compareTo("music")==0){%>
							<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span class="w60">수록앨범명 : <%=albumTitle%></span>
												<span>음원명 : <%=musicTitle%></span>
												<span class="w60">작사가 : <%=lyricist%></span>
												<span>작곡가 : <%=composer%></span>
												<span class="w60">가수 :  <%=singer%></span>
												<span>통합저작권관리번호 : <font style="color:#F96A0C;"><%=stdCrhId%></font></span>
											</p>
										</td>
							</tr>

				<%}else if(collname.compareTo("album")==0){%>
							<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span class="w60">앨범제작자 : <%=producer%></span>
												<span>앨범발매일 : <%=issuedDate%></span>
											</p>
										</td>
							</tr>

				<%}else if(collname.compareTo("literature")==0){%>
							<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span class="w60">수록도서명 : <%=bookTitle%></span>
												<span>작가 : <%=writer%></span>
												<span>통합저작권관리번호 : <font style="color:#F96A0C;"><%=stdCrhId%></font></span>
											</p>
										</td>
							</tr>

				<%}else if(collname.compareTo("book")==0){%>
							<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=bookTitle%></b>
											<p class="mt15">
												<span class="w60">발행자명 : <%=publisher%></span>
												<span>발행년도 : <%=firstEditionYear%></span>
											</p>
										</td>
							</tr>
	
				<%}else if(collname.compareTo("movie")==0){%>
								<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span class="w60">장르구분 : <%=genreNM%></span>
												<span>영화형태 : <%=movieTypeNM%></span>
												<span class="w60">관람등급 : <%=viewGradeNM%></span>
												<span>상영시간 : <%=runTime%></span>
												
												<span class="w60">제작년도 :  <%=produceYear%></span>
												<span>개봉일자 : <%=releaseDate%></span>
												<span class="w60">제작사 : <%=producer%></span>
												<span>투자사 : <%=investor%></span>
												<span class="w60">포맷정보 : <%=formatCode%></span>
							<%
							String[] mediaCodeArray;
							String[] produceDateArray;
							String[] distributorArray;
							mediaCodeArray=mediaCode.split(",");
							produceDateArray=produceDate.split(",");
							distributorArray=distributor.split(",");
							for(int movierow=0; movierow<mediaCodeArray.length; movierow++){
							%>

												<span>매체명 : <%=mediaCodeArray[movierow]%></span>
												<span class="w60">제작일자 : <%=produceDateArray[movierow]%></span>										
												<span>배급사 : <%=distributorArray[movierow]%></span>

							<%}%>
							
							   <span class="w60">출연진 : <%=leadingActor%></span>
								 <span>감독/연출자 : <%=director%></span>										
							   <span>통합저작권관리번호 : <font style="color:#F96A0C;"><%=stdCrhId%></font></span>
							   	</p>
										</td>
								</tr>

				<%}else if(collname.compareTo("scenario")==0){%>
								<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span>작품명소제목 : <%=ProdSubTitle%></span>
												<span class="w60">작가명 : <%=writer%></span>
												<span class="w60">원작명 : <%=origWork%></span>
												<span>방송회차 : <%=broadOrd%></span>
												<span class="w60">방송일시 :  <%=broadDate%></span>												
												<span>연출자 : <%=direct%></span>
												<span class="w60">장르분류 : <%=genreKind%></span>
												<span>방송매체 : <%=broadMedi%></span>
												<span class="w60">방송사 :  <%=broadStat%></span>
												<span>통합저작권관리번호 : <font style="color:#F96A0C;"><%=stdCrhId%></font></span>
											</p>
										</td>
								</tr>
									
				<%}else if(collname.compareTo("content")==0){%>
               <tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span>프로그램명 : <%=progName%></span>
												<span class="w60">매체코드 : <%=mediCode%></span>
												<span class="w60">채널코드 : <%=chnlCode%></span>
												<span>프로그램ID : <%=progId%></span>
												<span class="w60">회차 :  <%=progOrdseq%></span>												
												<span>방송일자 : <%=broadDate%></span>
												
												<span class="w60">제작품질 : <%=prodQulty%></span>
												<span>편성표제목 : <%=ptablTitle%></span>
												<span class="w60">프로그램등급 :  <%=progGrad%></span>
												<span>통합저작권관리번호 : <font style="color:#F96A0C;"><%=stdCrhId%></font></span>
											</p>
										</td>
              </tr>

				<%}else if(collname.compareTo("music_licensor")==0){%>
              <th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%>(<%=viewlicensorCode%>)</b>
											<p class="mt15">
												<span class="w60">저작물 : <%=prodName%></span>
												<span>저작권(역할) : <%=licensorRole%></span>
												<span class="w60">저작인접권 : <%=performName%></span>
											</p>
										</td>
							</tr>
	
				<%}else if(collname.compareTo("public_licensor")==0){%>
							<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%>(<%=viewlicensorCode%>)</b>
											<p class="mt15">
												<span class="w60">저작물 : <%=prodName%></span>
												<span>저작권(역할) : <%=licensorRole%></span>
											</p>
										</td>
							</tr>
							
				<%}else if(collname.compareTo("art_licensor")==0){%>
				      <th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%>(<%=viewlicensorCode%>)</b>
											<p class="mt15">
												<span class="w60">저작물 : <%=prodName%></span>
												<span>저작권(역할) : <%=licensorRole%></span>
											</p>
										</td>
							</tr>
							
				<%}else if(collname.compareTo("character_licensor")==0){%>
				      <th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%>(<%=viewlicensorCode%>)</b>
											<p class="mt15">
												<span class="w60">저작물 : <%=prodName%></span>
												<span>저작권(역할) : <%=licensorRole%></span>
												<span class="w60">국적 : <%=natiName%></span>
											</p>
										</td>
							</tr>

				<%}else if(collname.compareTo("news")==0){%>
								<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span class="w60">언론사명 : <%=prodname%></span>
												<span>기사일자 : <%=articlDt%></span>
												<span class="w60">관리단체 : <%=caName%></span>
												<span>통합저작권관리번호 : <font style="color:#F96A0C;"><%=stdCrhId%></font></span>
											</p>
										</td>
							</tr>
							
			    <%}else if(collname.compareTo("public")==0){%>
			    			<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span class="w60">형식분류 : <%=typedivlnm%> / <%=typedivmnm%></span>
												<span>의미분류 : <%=meandivlnm%> / <%=meandivmnm%></span>
												<span class="w60">공표국가 : <%=anncnatn%></span>
												<span class="w60">UCI : <%=uci%></span>
												<span>통합저작권관리번호 : <font style="color:#F96A0C;"><%=publcrid%></font></span>
											</p>
										</td>
							</tr>

				<%}else if(collname.compareTo("art")==0){%>
							<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span class="w60">부제 : <%=subtitle%></span>
												<span>분류정보 : <%=art_div_info%></span>
												<span class="w60">소장기관명 : <%=cltn_org_nm%></span>
												
												<span>크기정보 : <%=size_info%></span>
												<span class="w60">주재료 :  <%=main_mtrl%></span>
												<span>구조 및 특징 : <%=stru_ftre%></span>
												<span>통합저작권관리번호 : <font style="color:#F96A0C;"><%=stdCrhId%></font></span>
											</p>
										</td>
							</tr>
							
				<%}else if(collname.compareTo("character")==0){%>
							<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span class="w60">작품명 : <%=workNmKor%></span>
												<span>설명 : <%=charDesc%></span>
												<span class="w60">통합저작권관리번호 : <font style="color:#F96A0C;"><%=stdCrhId%></font></span>
											</p>
										</td>
							</tr>
							
				<%}else if(collname.compareTo("news_licensor")==0){%>
								<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=licensorNameKor%></b>
											<p class="mt15">
												<span class="w60">저작권(역할) : <%=licensorRole%></span>
											</p>
										</td>
								</tr>
	
				<%}else if(collname.compareTo("broadcast")==0){%>
								<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span class="w60">프로그램명 : <%=program_name%></span>
												<span>방송회차 : <%=broad_num%></span>
												<span class="w60">방송일자 : <%=startday%></span>
												<span>장르구분 : <%=gnre_code_name%></span>
												<span class="w60">제작타입 :  <%=make_type_kor%></span>
												<span>제작사 : <%=make_name%></span>
												
												<span class="w60">담당PD :  <%=make_pd%></span>
												<span>연출자 : <%=producer%></span>
												<span class="w60">대표작가 :  <%=writer%></span>
												<span>통합저작권관리번호 : <font style="color:#F96A0C;"><%=stdCrhId%></font></span>
											</p>
										</td>
							</tr>
	
				<%}else if(collname.compareTo("broadcast_licensor")==0){%>
							<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=licensorNameKor%>(<%=viewlicensorCode%>)</b>
											<p class="mt15">
												<span class="w60">저작권(역할) : <%=licensorRole%></span>
											</p>
										</td>
								</tr>
			
				<%}else if(collname.compareTo("literature_licensor")==0||collname.compareTo("scenario_licensor")==0||collname.compareTo("content_licensor")==0||collname.compareTo("movie_licensor")==0){%>
								<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%>(<%=viewlicensorCode%>)</b>
											<p class="mt15">
												<span class="w60">저작물 : <%=prodName%></span>
												<span class="w60">저작권(역할) : <%=licensorRole%></span>
											</p>
										</td>
									</tr>

				<%}%>
								
<%					
}//for
%>		
									
									
								</tbody>
	</table>