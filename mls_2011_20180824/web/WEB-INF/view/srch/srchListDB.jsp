<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	String sessSsnNo = user.getSsnNo();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 검색 및 상당한 노력 신청 서비스 이용 | 저작권자찾기 | 권리자찾기 </title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/menuhover.js"></script>
<script type="text/javascript"> 
<!--
jQuery.noConflict(); 

jQuery(window).load(function(){		
	var srch = document.frm.srch.value;
	if(srch == null || srch ==""){
		document.frm.srchDivs.value = "00";
	}else{
		document.frm.srchDivs.value = srch;		
 	}
	var reqUrl = window.location;
	var urlCd;	
	reqUrl=reqUrl.toString().substr(1,29);
	if(reqUrl=="http://www.findcopyright.or.kr"||reqUrl=="http://www.right4me.or.kr:8080"){
		urlCd="01";
	}
	else
		urlCd="02";
	
	document.frm.urlCd.value=urlCd;

	var moreVal = document.frm.more.value;

	if(moreVal == "SM"){
		jQuery("#statPagination").show();
		jQuery("#commPagination").hide();
		jQuery("#crosPagination").hide();
	}else if(moreVal == "CM"){
		jQuery("#statPagination").hide();
		jQuery("#commPagination").show();
		jQuery("#crosPagination").hide();
	}else if(moreVal == "CO"){
		jQuery("#statPagination").hide();
		jQuery("#commPagination").hide();
		jQuery("#crosPagination").show();
	}else{
		jQuery("#statPagination").hide();
		jQuery("#commPagination").hide();
		jQuery("#crosPagination").hide();
	}
	
});

function goPage(pageNo) {
	  var frm = document.frm;
	  frm.page_no.value = pageNo;
	  frm.target = "_self";  
// 	  if(trim(searchStr.value)=='' || searchStr.value.length==0)
	   frm.action = "srchList.do";
// 	  else
// 	   findForm.action = "/statBord/statBordFind.do";
	  frm.submit();
}
function statBoRegiPop(){
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
	}else{
	window.open("/statBord/statBo06Regi.do", "small", "width=750, height=650, scrollbars=yes, menubar=no, location=yes");
	}
}

function changeList(value){
	if(value != 00){
	var srchDivs = 0;
	if(value == 01){srchDivs = 35;}
	else if(value== 02){srchDivs = 79;}
	else if(value== 03){srchDivs = 72;}

	var options = {
			type		: "POST",
			url 		: '/srchList/codeList.do',
			data		: {"srchDivs":srchDivs},
			dataType    : "json",
			contentType : "application/x-www-form-urlencoded;charset=euc-kr",
			success		: function(data){
				var j = data.codeList;
				var options = '<option value="0">-- 전체 --<\/option>';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].midCode + '">' + j[i].codeName+ '<\/option>';
				}
				jQuery("#genreCd").html(options);
				jQuery('#genreCd option:first').attr('selected', 'selected');
				}
		};jQuery.ajax(options);
	}else{
		var options = '';
			options += '<option value="0">-- 전체 --<\/option>';
		jQuery("#genreCd").html(options);
		jQuery('#genreCd option:first').attr('selected', 'selected');
	}
}

function board_search(){
	var frm = document.frm;
	document.frm.more.value = "N";
	frm.action = 'srchList.do';
	frm.method = 'post';
	frm.submit();
}
function board_more(val){
	var frm = document.frm;
	if(val == "SM"){
		frm.srchDivs.value = "01";
	}else if(val == "CO"){
		frm.srchDivs.value="02";
	}else if(val == "CM"){
		frm.srchDivs.value="03";
	}
	document.frm.more.value=val;
	frm.action = 'srchList.do';
	frm.method = 'post';
	frm.submit();
	
}
//법정허락 신청
function fn_goStep(value){
	var userId = '<%=sessUserIdnt%>';
	
	var userName = '<%=sessUserName%>';
	
	if(document.hidForm.isSsnNo.value == ''){
		document.hidForm.isSsnNo.value = '<%=sessSsnNo%>';
	}
	
	var isSsnNo = document.hidForm.isSsnNo.value;
	
	
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}else{
		if(isSsnNo == 'null' || isSsnNo == ''){
			window.open('/user/user.do?method=goSsnNoConf&userIdnt='+userId+'&userName='+userName+'&sDiv=03&value='+value,'win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=375');
			return;
		}else{
			var hddnFrm = document.hidForm;
			var frm = document.frm;
			hddnFrm.action = "/stat/statPrpsMain.do";
			var worksId = value;
			hddnFrm.worksId.value = worksId;
			hddnFrm.target = "_self";
			hddnFrm.method = "post";
			hddnFrm.submit();
		}
	}
}
function crosPop(){
	window.open("http://www.cros.or.kr/report/search.cc", "small", "width=750, height=650, scrollbars=yes, menubar=no, location=yes");
}

function keyCheck(key){
	if(event.keyCode == 13){
		board_search();
	}
}

//-->
</script>
</head>
 
<body>
	<!-- 전체를 감싸는 DIVISION -->	 	
	<div id="wrap">
 
		<!-- HEADER str-->
		<jsp:include	page="/include/2012/header.jsp" /> 
		<script type="text/javascript">initNavigation(3);</script>
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container" class="container_vis3">
			<div class="container_vis">
				<h2><img src="images/2012/title/container_vis_h2_3.gif" alt="저작권자찾기" title="저작권자찾기" /></h2>
			</div>
 
			<div class="content">
			
				<!-- 래프 -->
				<jsp:include	page="/include/2012/leftMenu03.jsp" /> 
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1","lnb12"); 
 				</script>
				<!-- //래프 -->
				
<!-- 				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; "> -->
<!-- 					<p style="height: 38px; text-align: center; margin: 0;"> -->
<!-- 						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br /> -->
<!-- 						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span> -->
<!-- 					</p> -->
<!-- 				</div> -->
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					
					<p class="path"><span>Home</span><span>저작권자찾기</span><span>저작권자 검색 및 상당한 노력 신청</span><em>서비스 이용</em></p>
					<h1 title="저작물에 대한 주민의식이 필요합니다!"><img src="images/2012/title/content_h1_0302.gif" alt="서비스 이용" title="서비스 이용" /></h1>
					
					<div class="section relative">
						
					<!-- hidden form str : 아이프레임데이타 가져가는 용도-->		
					<form name="hidForm" action="#" class="sch">
						<input type="hidden" name="isSsnNo" />
						<input type="hidden" name="userIdnt" />
						<input type="hidden" name="ifrmInput" />
						<input type="hidden" name="PRPS_RGHT_CODE" />
						<input type="hidden" name="srchTitle" />
						<input type="hidden" name="srchProducer" />
						<input type="hidden" name="srchAlbumTitle" />
						<input type="hidden" name="srchSinger" />
						<input type="hidden" name="srchStartDate" />
						<input type="hidden" name="srchEndDate" />
						<!-- 음악 -->
						<input type="hidden" name="srchNonPerf" />
						<!-- 도서 -->
						<input type="hidden" name="srchLicensor" />
						<input type="hidden" name="srchPublisher" />
						<input type="hidden" name="srchBookTitle" />
						<input type="hidden" name="srchLicensorNm" />
						<!-- 방송대본 -->
						<input type="hidden" name="srchWriter" />
						<input type="hidden" name="srchBroadStatName" />
						<input type="hidden" name="srchDirect" />
						<input type="hidden" name="srchPlayers" />
						<!-- 이미지 -->
						<input type="hidden" name="srchWorkName" />
						<input type="hidden" name="srchLishComp" />
						<input type="hidden" name="srchCoptHodr" />
						<input type="hidden" name="srchWterDivs" />
						<!-- 영화 -->
						<input type="hidden" name="srchDistributor" />
						<input type="hidden" name="srchDirector" />
						<input type="hidden" name="srchViewGrade" />
						<input type="hidden" name="srchActor" />
						<!-- 방송 -->
						<input type="hidden" name="srchProgName" />
						<input type="hidden" name="srchMaker" />
						
						<!-- 뉴스 -->
						<input type="hidden" name="srchProviderNm" />
						<input type="hidden" name="worksId" />
						<input type="submit" style="display:none;">
					</form>
								
						<!-- 검색 -->
						<form name="frm" action="#">
							<fieldset class="w100">
							<legend>게시판검색</legend>
								<div class="boxStyle">
									<div class="box1 floatDiv">
										<p class="fl mt5 w15"><label for="srchDivs"><img title="Search" alt="Search" src="/images/2011/content/sch_txt.gif"></label></p>
										<p class="fl w90">
										<input type="hidden" id="urlCd" name="urlCd" value=""/>
										<input type="hidden" id="srch" value="${srchDivs}"/>
										<input type="hidden" name="page_no" id="page_no" value="1"/>
										<select class="w28" name="srchDivs" id="srchDivs" onchange="javascript:changeList(this.value)" title="통합검색">
											<option value="00" id="srcha">통합검색</option>
											<option value="01" id="srchb">법정허락 대상저작물</option>
											<option value="02" id="srchd">저작권 등록부</option>
											<option value="03" id="srche">위탁관리 저작물</option>
										</select>
										<select id="genreCd" name="genreCd" class="w28" title="상세검색">
													<option value="0">--전체--</option>
													<c:if test="${!empty genreCd}">
													<c:forEach items="${codeList}" var="codeList">
														<c:if test="${genreCd == codeList.midCode}">
															<option selected="selected" value="${codeList.midCode}">${codeList.codeName}</option>
														</c:if>
														<c:if test="${genreCd!= codeList.midCode}">
															<option value="${codeList.midCode}">${codeList.codeName}</option>
														</c:if>
													</c:forEach>
													</c:if>
													<c:if test="${empty genreCd}">
													<c:forEach items="${codeList}" var="codeList">
														<option value="${codeList.midCode}">${codeList.codeName}</option>
													</c:forEach>
													</c:if>
										</select>
										<input class="inputData w30" type="text" title="검색어" size="35" name="worksTitle" value="${worksTitle}" onkeydown="javascript:keyCheck(event)" />
										<input type="hidden" name="gubunVal" value="F"/>
										<input type="hidden" name="more" value="${more}" />
										
										<span class="button small black"><input type="submit" value="검색" onclick="javascript:board_search();"></span>
<!-- 										 <input type="checkbox" id="chk1" class="inputChk ml5" /><label for="chk1" class="p11 strong black">결과 내 검색</label>  -->
									</p>
									</div>
									<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
								</div>
							</fieldset>
 						<c:if test="${!empty gubunVal}">
							<div class="white_box">
								<div class="box5">
									<div class="box5_con floatDiv">
										<p class="fl"><img alt="" src="images/2012/content/box_img4.gif"></p>
										<div class="fl ml30 ">
											<ul class="list1 mt10">
											<li class="bgNone pl0"><strong class="inBlock w30 vtop"><sup class="sup black thin">1)</sup>법정허락 이용승인 신청</strong><span class="inBlock w70">이미 상당한노력이 이행되어 법정허락이용승인신청이 가능한 저작물로, 법정허락 이용승인 신청정보 작성화면으로 이동합니다.</span></li>
											<!-- 
											<li class="bgNone pl0 mt5"><strong class="inBlock w30 vtop"><sup class="sup black thin">2)</sup>이용승인 신청 안내</strong><span class="inBlock w70">위탁관리저작물로 이용승인 신청을 위한 해당관리기관 정보 안내화면을 보여줍니다.</span></li>
											 -->
											<li class="bgNone pl0 mt5"><strong class="inBlock w30 vtop"><sup class="sup black thin">2)</sup>저작권자 찾기위한 상당한<br/>&nbsp;&nbsp;노력 신청</strong><span class="inBlock w70">권리자찾기 정보시스템에서 한번의 신청으로 상당한노력 이행 신청과 함께 저작권자 조회공고 자동으로 처리됩니다.</span></li>
											<li class="bgNone pl0 mt5"><strong class="inBlock w30 vtop"><sup class="sup black thin">3)</sup>저작권 등록 정보 조회</strong><span class="inBlock w70">검색한 저작물의 정보 조회를 위해서 저작권등록 시스템 통합 검색으로 이동합니다. </span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="subcetcol">
								<p class="cetcol1">검색 결과에 찾으시는 저작물이 없는 경우 클릭하세요</p>
								<p class="cetcol2"><a href="javascript:;" onclick="statBoRegiPop()"><img src="/images/sub/btn_3.jpg" alt="검색" /></a></p>
								<p class="clear"></p>
							</div>
							<c:if test="${!empty srch}">
							<div class="h2_box floatDiv mt10" id="srchDiv">
								<h2 class="fl">법정허락 대상저작물(<c:if test="${!empty srchSize}">${srchSize}</c:if><c:if test="${empty srchSize}">0</c:if>건)</h2>
									<c:if test="${more == 'N'}">
								<a href="javascript:board_more('SM')" class="fr more">더보기</a>
									</c:if>
							</div>
							<table border="1" cellspacing="0" cellpadding="0" summary="법정허락 대상저작물 목록입니다." class="grid">
								<caption>법정허락 대상저작물</caption>
								<colgroup>
								<col width="10%"></col>
								<col></col>
								<col></col>
								</colgroup>
								<tbody>
								
									<c:forEach var="srch" items="${srch}">
									
									<c:if test="${srch.worksDivsCd == 4}">
									<tr>
										<th scope="row" class="vtop">${srch.genreCdName}</th>
										<td class="liceSrch">
											<B>${srch.worksTitle}</B><br/>
												<span class="w40">발행자 : ${srch.anucItem6}</span>
												<span>공표일자 : ${srch.anucItem7}</span>
												<span class="w95">작가 : 
												<c:if test="${!empty srch.coptHodr}">
												<input type="hidden" name="copt" id="copt" value="${srch.coptHodr}" />
												${srch.coptHodr}
												</c:if>		
												</span>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:fn_goStep(${srch.worksId})"><span class="sup1"></span>법정허락 이용승인 신청</a></span></td>
									</tr>
									</c:if>
									<c:if test="${srch.worksDivsCd == 1}">
									<tr>
										<th scope="row" class="vtop">${srch.genreCdName}</th>
										<td class="liceSrch">
											<B>${srch.worksTitle}</B><br/>
												<span class="w60">앨범명 : ${srch.albmName}</span>
												<span>음반제작자 : ${srch.albmProd}</span>
												<span class="w95">가수명 : ${srch.muciName}</span>
												<span>연주자 : ${srch.perfName}</span>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:fn_goStep(${srch.worksId})"><span class="sup1"></span>법정허락 이용승인 신청</a></span></td>
									</tr>
									</c:if>
									<c:if test="${srch.worksDivsCd == 2}">
									<tr>
										<th scope="row" class="vtop">${srch.genreCdName}</th>
										<td class="liceSrch">
											<B>${srch.worksTitle}</B><br/>
												<span class="w60">교과목 : ${srch.bookDivs}</span>
												<span>출판사 : ${srch.lishComp}</span>
												<span class="w95">저작자 : ${srch.coptHodr}</span>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:fn_goStep(${srch.worksId})"><span class="sup1"></span>법정허락 이용승인 신청</a></span></td>
									</tr>
									</c:if>
									<c:if test="${srch.worksDivsCd == 3}">
									<tr>
										<th scope="row" class="vtop">${srch.genreCdName}</th>
										<td class="liceSrch">
											<B>${srch.worksTitle}</B><br/>
												<span class="w60">출판사 : ${srch.lishComp}</span>
												<span class="w95">저자 : ${srch.coptHodr}</span>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:fn_goStep(${srch.worksId})"><span class="sup1"></span>법정허락 이용승인 신청</a></span></td>
									</tr>
									</c:if>
									<c:if test="${srch.worksDivsCd == 6}">
									<tr>
										<th scope="row" class="vtop">${srch.genreCdName}</th>
										<td class="liceSrch">
											<B>${srch.worksTitle}</B><br/>
												<span class="w60">형태 및 수량 : ${srch.worksForm}</span>
												<span class="w95">저작재산권자 : ${srch.coptHodrName}</span>
												<span class="w95">승인일 : ${srch.confDate}</span>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:fn_goStep(${srch.worksId})"><span class="sup1"></span>법정허락 이용승인 신청</a></span></td>
									</tr>
									</c:if>
									</c:forEach>
							
								</tbody>
							
							</table>
							
							</c:if>	
							
							<%-- 
							<c:if test="${srchDivs == '01' || srchDivs =='00'}">
							
							<div class="comment_box">
								<strong class="line15 black"><img alt="" class="vmid" src="images/2012/common/ic_tip.gif" /> 알려드립니다.</strong>
								<p class="comment block ml20 mt5">저작권 통합 검색을 통해서 검색목록에 대상이 없거나 검색결과가 없으면 [저작권자 찾기위한 상당한 노력 신청] 버튼을 클릭하여 진행하세요.</p>
							</div>
							<div class="btnArea" id ="btnArea">
							<p class="rgt"><span class="button medium icon"><a href="javascript:;" onclick="statBoRegiPop()"><span class="sup2"></span>저작권자 찾기위한 상당한 노력 신청</a></span></p>
							</div>
							</c:if>
							 --%>
							<!-- 페이징 -->
							<div id="statPagination"  class="pagination">
							<ul>
								<li>
						<!--페이징 리스트 -->
						 	<jsp:include page="../common/PageList_2011.jsp" flush="true">
							  	<jsp:param name="totalItemCount" value="${srchCount}" />
								<jsp:param name="nowPage"        value="${pageNo}" />
								<jsp:param name="functionName"   value="goPage" />
								<jsp:param name="listScale"      value="" />
								<jsp:param name="pageScale"      value="" />
								<jsp:param name="flag"           value="M01_FRONT" />
								<jsp:param name="extend"         value="no" />
							</jsp:include>
								</li>	
							</ul>
						</div>
						<!-- //페이징 -->
						
						<c:if test="${!empty cros}">
						<div class="h2_box floatDiv mt15" id="crosDiv">
								<h2 class="fl">저작권등록부(<c:if test="${!empty crosSize}">${crosSize}</c:if><c:if test="${empty crosSize}">0</c:if>건)</h2>
									<c:if test="${more == 'N'}">
								<a href="javascript:board_more('CO')" class="fr more">더보기</a>
									</c:if>
						</div>
							<table border="1" cellspacing="0" cellpadding="0" summary="저작권등록부 저작권자 검색 목록입니다." class="grid">
							<caption>저작권등록부</caption>
								<colgroup>
								<col width="10%"></col>
								<col></col>
								<col></col>
								</colgroup>
								<tbody>
									<c:forEach var="cros" items="${cros}">
									<tr>
										<th scope="row" class="vtop">${cros.contClassNameCd}</th>
										<td class="liceSrch">
											<B>${cros.contTitle}</B><br/>
												<span class="w95">등록번호 : ${cros.regId}</span>
												<span>등록일자 : ${cros.regDate}</span>
												<span class="w95">등록부문 : ${cros.regPart1Name}(${cros.regPart2Name})</span>
												<span>저작자 : ${cros.authorName}</span>
												<span class="w95">등록원인 : ${cros.regReason}</span>
												<span class="w95">등록권리자 : ${cros.regCoptHodrName}</span>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:;" onclick="crosPop()"><span class="sup3"></span>저작권 등록 정보 조회</a></span></td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
						<!-- 페이징 -->
						<div id="crosPagination" class="pagination">
						<ul>
							<li>
						<!--페이징 리스트 -->
						 	<jsp:include page="../common/PageList_2011.jsp" flush="true">
							  	<jsp:param name="totalItemCount" value="${crosCount}" />
								<jsp:param name="nowPage"        value="${pageNo}" />
								<jsp:param name="functionName"   value="goPage" />
								<jsp:param name="listScale"      value="" />
								<jsp:param name="pageScale"      value="" />
								<jsp:param name="flag"           value="M01_FRONT" />
								<jsp:param name="extend"         value="no" />
							</jsp:include>
								</li>	
							</ul>
						</div>
						
						<!-- 위탁관리 -->
							<c:if test="${!empty comm}">
							<div class="h2_box floatDiv mt15" id="commDiv">
								<h2 class="fl">위탁관리저작물(<c:if test="${!empty commSize}">${commSize}</c:if><c:if test="${empty commSize}">0</c:if>건)</h2>
									<c:if test="${more == 'N'}">
								<a href="javascript:board_more('CM')" class="fr more">더보기</a>
									</c:if>
							</div>
							
							<table border="1" cellspacing="0" cellpadding="0" summary="위탁관리저작물 검색 목록입니다." class="grid">
								<caption>위탁관리저작물</caption>
								<colgroup>
								<col width="10%"></col>
								<col></col>
								</colgroup>
								<tbody>
								
									<c:forEach var="comm" items="${comm}">
									<!-- 음악 -->
									<c:if test="${comm.genreCd == 1}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">앨범명 : ${comm.albumTitle}</span>
												<span>앨범발매년도 : ${comm.albumIssuYear}</span>
												<span class="w60">가수 : ${comm.sing}</span>
												<span>연주가 : ${comm.perf}</span>												
												<span class="w60">작사가 : ${comm.lyrc}</span>
												<span class="w60">작곡가 : ${comm.comp}</span>
												<span>편곡가 : ${comm.arra}</span>
												<span class="w60">역사가 : ${comm.tran}</span>
												<span class="w60">음반제작자 : ${comm.prod}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 어문 -->
									<c:if test="${comm.genreCd == 2}">									
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">부제 : ${comm.worksSubTitle}</span>
												<span>창작년도 : ${comm.crtYear}</span>
												<span class="w60">도서명 : ${comm.bookTitle}</span>
												<span>출판사 : ${comm.bookPublisher}출판</span>
												<span class="w60">발행년도 : ${comm.bookIssuYear}</span>
												<span class="w90">저작자 : ${comm.coptName}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 방송대본 -->
									<c:if test="${comm.genreCd == 3}">				
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">작품명(대제목) : ${comm.worksTitle}</span>
												<span>장르/소재 : ${comm.scrtGenreCd} / ${comm.scrpSubjCd},${comm.scrpSubjCd2}</span>
												<span class="w60">원작명 : ${comm.worksOrigTitle}</span>
												<span>원작작가 : ${comm.origWriter}</span>
												<span class="w60">방송일자/회차 : ${comm.bordDate} / ${comm.brodOrdSeq}</span>
												<span>주요출연진 : ${comm.player}</span>
												<span class="w60">제작사 : ${comm.makeCpy}제작</span>
												<span>작가(저자) : ${comm.writer}</span>
												<span class="w60">연출자 : ${comm.director}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 영화 -->
									<c:if test="${comm.genreCd == 4}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">제작년도 : ${comm.crtYear}</span>
												<span>출연자 : ${comm.player}</span>
												<span class="w60">감독 : ${comm.direct}</span>
												<span>작가 : ${comm.writer}</span>
												<span class="w60">연출자 : ${comm.director}</span>
												<span>제작자 : ${comm.producer}</span>
												<span class="w60">배급사 : ${comm.distributor}</span>
												<span class="w95">투자자 : ${comm.investor}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 방송 -->
									<c:if test="${comm.genreCd == 5}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">장르 : ${comm.bordGenreCd}</span>
												<span>제작년도 : ${comm.makeYear}</span>
												<span class="w95">회차 : ${comm.brodOrdSeq}</span>
												<span>연출자 : ${comm.director}</span>
												<span class="w60">작가 : ${comm.writer}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 뉴스 -->
									<c:if test="${comm.genreCd == 6}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">기사일자 : ${comm.articlDate}</span>
												<span>지면번호/지면면종 : ${comm.papeNo} / ${comm.papeKind}</span>
												<span class="w60">기고자 : ${comm.contributor}</span>
												<span>기자 : ${comm.repoter}</span>
												<span class="w60">언론사 : ${comm.provider}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 미술 -->
									<c:if test="${comm.genreCd == 7}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">부제 : ${comm.worksSubTitle}</span>
												<span>분류 : ${comm.kind}</span>
												<span class="w60">저작년월일 : ${comm.makeDate}</span>
												<span>출처 : ${comm.sourceInfo}</span>
												<span class="w60">주재료 : ${comm.mainMtrl}</span>
												<span>구조 및 특징 : ${comm.txtr}</span>
												<span class="w60">작가 : ${comm.writer}</span>
												<span >소장기관명 : ${comm.possOrgnName}</span>
												<span class="w60">소장년월일 : ${comm.possDate}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 기타 -->
									<c:if test="${comm.genreCd == 99}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
										<B>${comm.worksTitle}</B><br/>
												<span class="w60">저작물 종류 : ${comm.sideGenreCdName}</span>
												<span>창작년도 : ${comm.makeDate}</span>
												<span class="w60">공표매체 : ${comm.publMedi}</span>
												<span>공표일자 : ${comm.publDate}</span>
												<span class="w60">저작자 : ${comm.coptName}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									</c:forEach>
								</tbody>
							</table>
								</c:if>
							</c:if>

							<!-- 페이징 -->
							<div id="commPagination" class="pagination">
							<ul>
								<li>
						<!--페이징 리스트 -->
						 	<jsp:include page="../common/PageList_2011.jsp" flush="true">
							  	<jsp:param name="totalItemCount" value="${commCount}" />
								<jsp:param name="nowPage"        value="${pageNo}" />
								<jsp:param name="functionName"   value="goPage" />
								<jsp:param name="listScale"      value="" />
								<jsp:param name="pageScale"      value="" />
								<jsp:param name="flag"           value="M01_FRONT" />
								<jsp:param name="extend"         value="no" />
							</jsp:include>
								</li>	
							</ul>
						</div>
						<!-- //페이징 -->
						</form>
							<c:if test="${empty gubunVal}">
							<div class="comment_box">
							<strong class="line15 black"><img alt="" class="vmid" src="images/2012/common/ic_tip.gif" /> 찾고자 하는 저작물을 검색 하세요.</strong>
							</div>
							</c:if>
										
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	</div>

<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
<!-- 	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기 -->
</script>
 
</body>
</html>

