<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">   
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />     
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 찾기 검색 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">    
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript">
function crosPop(){
	window.open("https://cras.copyright.or.kr/front/right/comm/main_.do");
}

function certMoreResult(page,target,mode) {  // 더 많은 검색 결과보기
	parent.frames.certMoreResult(page, target, mode);
}

function goPages(pageNo) {
	  var certFrm = document.getElementById("certFrm");
	  certFrm.page_no.value=pageNo;
	  certFrm.action = "certList.do";
	  certFrm.submit();
}
</script>      
</head>
<body>
<form name="certFrm" id="certFrm">
<input type="hidden" name="worksTitle" value="${worksTitle }">
<input type="hidden" name="mode" value="${mode }">
<input type="hidden" name="genreCdName" value="${genreCdName }">
<input type="hidden" name="page_no" value="">
</form>
<c:if test="${not empty certList }">
	<div class="box5_con floatDiv">
		<div class="h2_box floatDiv mt10">
					<h2 class="fl">권리인증(${countCert}건)</h2>
					<c:if test="${countCert>5 && mode!='paging'}">
					<a href="javascript:certMoreResult('1','5','paging');" class="fr more">더보기</a>
					</c:if>
		</div>
			<table border="1" cellspacing="0" cellpadding="0" summary="저작권찾기 신청정보입니다." class="grid">
				<colgroup>
					<col width="10%">
					<col>
					<col>
				</colgroup>	
				<tbody>
					<c:set var="fontTag"><font color=#1ab1f6>${worksTitle}</font></c:set>		
					<c:forEach items="${certList}" var="certList">												
					<tr>			
						<th scope="row" class="vtop">${certList.genreCdName }</th>
						<td class="liceSrch">						
							<b>${fn:replace(certList.worksTitle,worksTitle,fontTag)}</b>
							<p class="mt15">
								<span class="w60">권리자정보 : ${certList.certName}</span>
								<span>인증일시 : ${certList.certDy}</span>												
							</p>
						</td>
						<td class="ce"><span class="button medium blue icon"><a href="javascript:crosPop();"><span class="sup4"></span>인증정보열람</a></span></td>
					</tr>
					</c:forEach>						
				</tbody>
			</table>
			<c:if test="${mode eq page }">
			  <!-- 페이징 시작 -->
				<div class="pagination">
					<ul>
						<li>
				<!--페이징 리스트 -->
				 	<jsp:include page="../common/PageList_2011.jsp" flush="true">
					  	<jsp:param name="totalItemCount" value="${countCert}" />
						<jsp:param name="nowPage"        value="${pageNo}" />
						<jsp:param name="functionName"   value="goPages" />
						<jsp:param name="listScale"      value="" />
						<jsp:param name="pageScale"      value="" />
						<jsp:param name="flag"           value="" />
						<jsp:param name="extend"         value="" />
					</jsp:include>
						</li>	
					</ul>
				</div>
			</c:if>
		</div>
</c:if>
</body>
</html>