<%@ page pageEncoding="euc-kr"%>
<!-- hidden form str : 아이프레임데이타 가져가는 용도-->	
<script type="text/javascript">
<!--
$(function(){
	$('#searchBtn').css('cursor','pointer');
	$('#searchBtn').click(searchBtnClick);
})

function searchBtnClick(event)
{
	//alert('click')
	$('#search').submit();
	//console.log($('#search').html())
}

//-->
</script>	
					<form name="hidForm" action="#" class="sch">
						<input type="hidden" name="worksTitle" value="">
						<input type="hidden" name="worksId" />
						<input type="hidden" name="isSsnNo" />
						<input type="hidden" name="userIdnt" />
						<input type="hidden" name="ifrmInput" />
						<input type="hidden" name="PRPS_RGHT_CODE" />
						<input type="hidden" name="srchTitle" />
						<input type="hidden" name="srchProducer" />
						<input type="hidden" name="srchAlbumTitle" />
						<input type="hidden" name="srchSinger" />
						<input type="hidden" name="srchStartDate" />
						<input type="hidden" name="srchEndDate" />
						<!-- 음악 -->
						<input type="hidden" name="srchNonPerf" />
						<!-- 도서 -->
						<input type="hidden" name="srchLicensor" />
						<input type="hidden" name="srchPublisher" />
						<input type="hidden" name="srchBookTitle" />
						<input type="hidden" name="srchLicensorNm" />
						<!-- 방송대본 -->
						<input type="hidden" name="srchWriter" />
						<input type="hidden" name="srchBroadStatName" />
						<input type="hidden" name="srchDirect" />
						<input type="hidden" name="srchPlayers" />
						<!-- 이미지 -->
						<input type="hidden" name="srchWorkName" />
						<input type="hidden" name="srchLishComp" />
						<input type="hidden" name="srchCoptHodr" />
						<input type="hidden" name="srchWterDivs" />
						<!-- 영화 -->
						<input type="hidden" name="srchDistributor" />
						<input type="hidden" name="srchDirector" />
						<input type="hidden" name="srchViewGrade" />
						<input type="hidden" name="srchActor" />
						<!-- 방송 -->
						<input type="hidden" name="srchProgName" />
						<input type="hidden" name="srchMaker" />
						
						<!-- 뉴스 -->
						<input type="hidden" name="srchProviderNm" />
						<input type="submit" style="display:none;">
					</form>
						<!-- 검색 -->
						<form id="search" name="search" action="srchList.do" method="post"><!--  onSubmit="goSearch(); return false" -->
									<div class="bg_f8f8f8">
									
									<!-- 2014.11.06 추가 -->
									<div>
											<div class="float_lf">
											<span class="sub_blue_point_bg">검색대상</span>&nbsp;&nbsp;&nbsp;
												  <input type="checkbox" value="all" name="range" id="chkall" onclick="check(1);" <%if(arr_range.indexOf("all")    != -1){ %>checked<%} %> /><label for="chkall" class="thin p12">전체</label>
												  <input type="checkbox" value="title" name="range" id="chktl" onclick="check(2);" <%if(arr_range.indexOf("title")    != -1){ %>checked<%} %> /><label for="chktl"  class="thin p12">저작물명</label>
												  <input type="checkbox" value="licer_detail" name="range" id="chktl2" onclick="check(3);" <%if(arr_range.indexOf("licer_detail")    != -1){ %>checked<%} %> /><label for="chktl2"  class="thin p12">저작권자</label>
											</div>
											<div class="float_rt">
											<span class="sub_blue_point_bg">정렬방식</span>&nbsp;&nbsp;&nbsp;
												<select name="sortfield" onchange="javascript:autosubmit(search,'<%=resrch%>')" id="line">
															<option value="score" <%if(sortfield.compareTo("score")==0){%>selected<%}%>>정확도순</option>
															<option value="title" <%if(sortfield.compareTo("title")==0){%>selected<%}%>>제목순</option>
												</select>
												<select name="sortorder" onchange="javascript:autosubmit(search,'<%=resrch%>')">
															<option value="desc" <%if(sortorder.compareTo("desc")==0){%>selected<%}%>>내림차순</option>
															<option value="asc" <%if(sortorder.compareTo("asc")==0){%>selected<%}%>>오름차순</option>
												</select>
											</div>
									<p class="clear"></p>
									</div>
									<!-- //2014.11.06 추가 -->
										<div class="mar_tp10">
										<select name="target" id="srchDivs" title="검색구분" style="padding-top: 7px; padding-bottom: 6px">
											<%for(int j=0; j<=targetcount; j++){%>
												<option value="<%=temptargetlist[j]%>" <%if(target.compareTo(temptargetlist[j])==0){%>selected<%}%>><%=targetnamelist[j]%></option>
											<%}%>
										</select>
										<input type="hidden" id="genreCd" name="genreCd" title="장르" value="" style="padding-top: 7px; padding-bottom: 6px">
										<!-- <input type="hidden" id="gubunVal" name="gubunVal" title="장르" value="F" style="padding-top: 7px; padding-bottom: 6px"> -->
										<!-- 20171212 수정 -->
										<%-- <select id="genreCd" name="genreCd" title="장르" style="padding-top: 7px; padding-bottom: 6px">
											<%for(int j=0; j<=genreCd_targetcount; j++){%>
												<option value="<%=genreCdValueList[j]%>" <%if(genreCd.compareTo(genreCdValueList[j])==0){%>selected<%}%>><%=genreCdNameList[j]%></option>
											<%}%>
										</select> --%>
										
										
										<%-- <%for(int j=0; j<=genreCd_targetcount; j++){%>
												<div><%=genreCd %> value= <%=genreCdValueList[j]%> :   seleted = <%=genreCd.compareTo(genreCdValueList[j])%>   ::   nameList = <%=genreCdNameList[j]%>
													: <%=genreCdValueList[j] %>
												</div>
										<%}%> --%>
										<input type="text" title="검색어" id="query" name="query" size="30" value="<%=query%>" style="IME-MODE: active;padding:3px;width:49%; height: 25px;"/>
										<img id="searchBtn" alt="검색" src="/images/sub_img/sub_25.gif">
										<!-- <input type="submit" value="검색" style="background-color: #f7b40a; color: white;"> -->&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chk1" name="resrch_check" class="checkbox" onclick="javascript:resrch_chk();"/><label for="chk1" class="p11 strong black">결과 내 검색</label>
										</div>
									</div>
							<input type="hidden" name="resultcount" value="10"/>
							<input type="hidden" name="page" value=""/>
							<input type="hidden" name="mode" value=""/>
							<input type="hidden" name="arr_range" value="<%=arr_range %>">
							<input type="hidden" name="resrch" value=""/>							
							<input type="hidden" id="urlCd" name="urlCd" value="01"/>
 						</form>
 						<div class="sub01_con_bg4_tp mar_tp30"></div>
 						<div class="sub01_con_bg4">
							<h3>법정허락 승인 신청</h3>
							<p class="word_dian_bg mar_tp5">이미 상당한 노력이 이행되어 법정허락 승인 신청이 가능한 저작물로, 법정허락 승인 신청정보 작성화면으로 이동합니다.</p>
							<h3 class="mar_tp10">저작권 등록 정보 조회</h3>
							<p class="word_dian_bg mar_tp5">검색한 저작물의 정보 조회를 위해서 저작권 등록 시스템 통합 검색으로 이동합니다.</p>
							<h3 class="mar_tp10">권리자 찾기위한  상당한노력 신청</h3>
							<p class="word_dian_bg mar_tp5">권리자 찾기 정보시스템에서 한 번의 신청으로 상당한 노력 이행 신청과 함께 저작권자 조회공고가 자동으로 처리됩니다.</p>
						</div>
						<div class="subcetcol">
							<p class="cetcol1">검색 결과에 찾으시는 저작물이 없는 경우 클릭하세요</p>
							<p class="cetcol2">
								<a href="#" onclick="statBoRegiPop();" class="btn_list_search btn01">저작권자 찾기 위한 상당한 노력 신청</a>
							</p>
							
							<!-- <p class="cetcol2"><a href="#" onclick="statBoRegiPop();"><img src="/images/sub/btn_3.jpg" alt="검색" /></a></p> -->
							<p class="clear"></p>
						</div>
<script type="text/javascript">						
$(function(){

<%
for(int j=0; j<=genreCd_targetcount; j++){
if(genreCd.compareTo(genreCdValueList[j])==0){%>
	var genreCd ='<%=genreCdValueList[j]%>';
	var num = <%=j%>;
<%}}%>
	var menuFlag = '${menuFlag}';
	/* console.log(menuFlag) */
	/* console.log(num);
	console.log(genreCd); */
	$('.sub_lf_menu').find('li').eq(num).find('a').addClass('on');
	if($('.con_rt_hd_title').html()!=genreCd){
		if(genreCd=='total'){
			if(menuFlag=='N'){
				$('.con_rt_hd_title').html('상당한 노력 신청 서비스');
			}else{
				$('.con_rt_hd_title').html('전체');
			}
			
		}else{
			$('.con_rt_hd_title').html(genreCd);
		}
	}
	//console.log($('.con_rt_hd_title').html())
	/* console.log($('.sub_lf_menu').find('li').eq(num).find('a').addClass('on'))
	console.log($('.sub_lf_menu').find('li').eq(1).html())
	console.log($('.sub_lf_menu').find('li').eq(2).html()) */
})						
</script>
				<%if(query.compareTo("")!=0){%>
					<!-- 2013.09.05 탭 추가 -->
					<ul class="tab_menuBg mt20">
                <%for(int j=0; j<=genreCd_targetcount; j++){%>
										<%if(j==0){%>
											<li <%if(genreCd.compareTo(genreCdValueList[j])==0){%>class="first on"<%}else{%>class="first"<%}%>><%if(genreCd.compareTo(genreCdValueList[j])==0){%><strong><%}%><a href="javascript:moreResult('1','<%=target%>','<%=genreCdValueList[j]%>','<%=resrch%>');"><%=genreCdNameList[j]%></a><%if(genreCd.compareTo(genreCdValueList[j])==0){%></strong><%}%></li>
										<%}else{%>
										  <li <%if(genreCd.compareTo(genreCdValueList[j])==0){%>class="on"<%}%>><%if(genreCd.compareTo(genreCdValueList[j])==0){%><strong><%}%><a href="javascript:moreResult('1','<%=target%>','<%=genreCdValueList[j]%>','<%=resrch%>');"><%=genreCdNameList[j]%></a><%if(genreCd.compareTo(genreCdValueList[j])==0){%></strong><%}%></li>
										<%}%>
								<%}%>
		          		</ul>
		          		<p class="clear"></p>
		          <!-- //2013.09.05 탭 추가 -->
		          <%}%>
