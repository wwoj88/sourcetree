<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%                                                     
	User user = SessionUtil.getSession(request);      
	String sessUserIdnt = user.getUserIdnt();
	
	String sessUserName = user.getUserName();
	String sessSsnNo = user.getSsnNo();	
	
	pageContext.setAttribute("UserName", sessUserIdnt);
	
	
	                                                  
%>                                      
<html lang="ko">   
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >     
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>권리자 검색 및 상당한 노력 신청 서비스 이용 | 저작권자찾기 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />  
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript" src="/js/menuhover.js"></script>
<script type="text/javascript"> 
 
function statBoRegiPop(){ 
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
	}else{
		window.open("/statBord/statBo06Regi.do", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes");
	}
}
          

function crosPop(content1){
	window.open("/search/pop_cros.jsp?reg_id="+content1, "_blank", "width=1000, height=841, scrollbars=yes, menubar=no, location=yes, resizable=yes ");
}
//법정허락 신청
function fn_goStep(value){
	var userId = '<%=sessUserIdnt%>';
	
	var userName = '<%=sessUserName%>';
	
	if(document.hidForm.isSsnNo.value == ''){
		document.hidForm.isSsnNo.value = '<%=sessSsnNo%>';
	}
	
	var isSsnNo = document.hidForm.isSsnNo.value;
	
	
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}else{
		if(isSsnNo == 'null' || isSsnNo == ''){
			window.open('/user/user.do?method=goSsnNoConf&userIdnt='+userId+'&userName='+userName+'&sDiv=03&value='+value,'win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=375');
			return;
		}else{
			var hddnFrm = document.hidForm;
			var frm = document.search;
			hddnFrm.worksTitle.value=frm.query.value;
			hddnFrm.action = "/stat/statPrpsMain.do";
			var worksId = value;
			hddnFrm.worksId.value = worksId;
			hddnFrm.target = "_self";
			hddnFrm.method = "post";
			hddnFrm.submit();
		}
	}
}
 

//-->                            
</script>
<script>
/* 	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69621660-2', 'auto');
	  ga('send', 'pageview');
 */
</script>     
</head>           
               
<body>

		<!-- HEADER str-->
		<jsp:include page="/include/2017/header.jsp" />
<script type="text/javascript">

$(function(){
	$('.sub_lf_menu > li').css('cursor','pointer');
})
</script>

		<%-- <jsp:include page="/include/2012/subHeader3.jsp" />
		<script type="text/javascript">initNavigation(3);</script> --%>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

				<!-- 썸네일 팝업 시작 -->
<div class="pop_thumb">
	<div class="inner">
		<h2 class="tit">이미지 정보<a href="javascript:;" class="btn_close"><img src="/images/2017/new/btn_close_pop.png" alt="팝업닫기"></a></h2>
		<div class="cont">
			<div class="img">
				<img id="image" src="/images/2017/new/img_pop.png" alt="">
			</div>
			<div class="ment">
				<!-- <p class="subject"> -->
					<font id="title">제목부분</font>
				<!-- </p> -->
				<ul id="dataList">
					<li><strong>이미지코드 : </strong><font id="imageCode">16270463</font></li>
					<li><strong>키워드 : </strong><font id="keyword">코사인</font></li>
					<li><strong>창작년월일 : </strong><font id="createDay">2017.05.10</font></li>
					<li><strong>권리 관리 기관명 : </strong><font id="companyName">두영디지텍</font></li>
					<li><strong>이미지 설명 : </strong><font id="imageDesc">이미지에 대한 설명을 작성해 주세요.</font></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- 썸네일 2팝업 끝 -->

		<!-- HEADER end -->
		                                      
		<!-- CONTAINER str-->    
		<div id="contents">                 
           
       <%--  ${menuFlag} --%>
          <c:if test="${menuFlag!='N'}">
            <div class="con_lf">
			<div class="con_lf_big_title">분야별 <br>권리자 찾기</div>
				<ul class="sub_lf_menu">
				<li><a href="" onclick="javascript:moreResult('1','total','total','no');">전체</a></li>
				<li><a href="" onclick="javascript:moreResult('1','total','음악','no');">음악</a></li>
				<li><a href="" onclick="javascript:moreResult('1','total','어문','no');">어문</a></li>
				<li><a href="" onclick="javascript:moreResult('1','total','방송대본','no');">방송대본</a></li>
				<li><a href="" onclick="javascript:moreResult('1','total','영화','no');">영화</a></li>
				<li><a href="" onclick="javascript:moreResult('1','total','방송','no');">방송</a></li>
				<li><a href="" onclick="javascript:moreResult('1','total','뉴스','no');">뉴스</a></li>
				<li><a href="" onclick="javascript:moreResult('1','total','미술','no');">미술</a></li>
				<li><a href="" onclick="javascript:moreResult('1','total','이미지','no');">이미지</a></li>
				<li><a href="" onclick="javascript:moreResult('1','total','사진','no');">사진</a></li>
				<li><a href="" onclick="javascript:moreResult('1','total','기타','no');">기타</a></li>
					<!-- <li><a href="/mlsInfo/liceSrchInfo01.jsp">소개 및 이용방법</a></li>
					<li><a href="/srchList.do" class="on">서비스 이용</a></li>
					<li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li>
					<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
					<li><a href="/mlsInfo/liceSrchInfo01.jsp" class="on">저작권자 찾기</a>
						<ul class="sub_lf_menu2">
							<li><a href="/mlsInfo/liceSrchInfo01.jsp">소개 및 이용방법</a></li>
							<li><a href="/srchList.do" class="on">서비스 이용</a></li>
							<li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li>
							<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
						</ul>
					</li>
					<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청</a>
						<ul class="sub_lf_menu2 disnone">
							<li><a href="/mlsInfo/statInfo01.jsp">소개 및 이용방법</a></li>
							<li><a href="/stat/statSrch.do">서비스 이용</a></li>
						</ul>
					</li> -->
				</ul>
			</div>
			</c:if>
			<c:if test="${menuFlag eq 'N'}">
			<div class="con_lf">
			<div class="con_lf_big_title">법정허락<br>승인 신청</div>
				<ul class="sub_lf_menu">
				<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
				<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
				<li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
				<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
				<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
				</ul>
			</div>
			</c:if>
			<div class="con_rt">                             
			   <div class="con_rt_head">
				
				<c:if test="${menuFlag!='N'}">
				<img src="../images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				분야별 권리자 찾기
				&gt;
				<c:if test="${genreCd!='total'}"><span class="bold">${genreCd}</span></c:if>
				<c:if test="${genreCd=='total'||genreCd==''}"><span class="bold">전체</span></c:if>
				
				</c:if>
				<c:if test="${menuFlag=='N'}">
				<img src="../images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				법정허락 승인 신청
				&gt;
				<span class="bold">상당한 노력 신청 서비스</span>
				</c:if>
			   </div>                                            
				           
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(images/2012/common/lodingBg.png) no-repeat 0 0; left:-1500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>                   
				</div>       
				
				 <!-- //Search str -->   
				<%@include file="mirsearch.jsp"%>                                                              
				 <!-- //Search end -->
				
			
			</div>  
		<!-- //CONTAINER end -->
		<p class="clear"></p>    
  </div>
  
  	<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
	<!-- FOOTER end -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

