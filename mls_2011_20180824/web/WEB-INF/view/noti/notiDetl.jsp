<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%
	String bordSeqn = request.getParameter("bordSeqn");
	String menuSeqn = request.getParameter("menuSeqn");
	String threaded = request.getParameter("threaded");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");

	Board boardDTO = (Board) request.getAttribute("board");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>공지사항 <%=boardDTO.getTite()%> | 알림마당 | 권리자찾기</title>
<<!-- title>공지사항 | 알림마당 | 권리자찾기</title> -->

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css">
<link rel="stylesheet" type="text/css" href="/css/table.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/JavaScript">
<!--
  function fn_notiList(){
		var frm = document.form1;
		frm.menuSeqn.value = '4';
		frm.action = "/board/board.do";
		frm.submit();
	}

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }
//-->
</script>
</head>

<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader6.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(6);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			
			<!-- 래프 -->
			<div class="con_lf">
			<h2><div class="con_lf_big_title">알림마당</div></h2>
					<ul class="sub_lf_menu">
						<!-- <li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=4&page_no=1" class="on">공지사항</a></li> -->
						<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1" class="on">공지사항</a></li>
						<li><a href="/mlsInfo/linkList01.jsp">자료실</a></li>
						<!-- <li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=1&page_no=1">자주묻는 질문</a></li> -->
						<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1">자주묻는 질문</a></li>
						<li><a href="/eventMgnt/eventList.do">이벤트</a></li>
						<li><a href="/mlsInfo/comeInfo.jsp">찾아오시는 길</a></li>
					</ul>
				</div>
			<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						알림마당
						&gt;
						<span class="bold">공지사항</span>
					</div>
					
					<h1><div class="con_rt_hd_title">공지사항</div></h1>
					
					<div class="section">
						<form name="form1" method="post" action = "#">
									<input type="hidden" name="bordSeqn" value="<%=bordSeqn%>">
									<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>">
									<input type="hidden" name="threaded" value="<%=threaded%>">
									<input type="hidden" name="srchDivs" value="<%=srchDivs%>">
									<input type="hidden" name="srchText" value="<%=srchText%>">
									<input type="hidden" name="page_no" value="<%=page_no%>">
									<input type="hidden" name="filePath">
									<input type="hidden" name="fileName">
									<input type="hidden" name="realFileName">
									<input type="submit" style="display:none;">
						<!-- 테이블 view Set -->
						<div class="article">
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="공지사항 - <%=boardDTO.getTite()%>의 세부내용 입니다.">
								<caption><%=boardDTO.getTite()%></caption>
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">제목</th>
										<td><%=boardDTO.getTite()%></td>
									</tr>
									<tr>
										<th scope="row">내용</th>
										<td><!-- 
											<p class="overflow_y h300">
											-->
											<%=CommonUtil.replaceBr(boardDTO.getBordDesc(),true)%>
											<!-- 
											</p>
											 --></td>
									</tr>
									<%
									          	List fileList = (List) boardDTO.getFileList();
									          	int listSize = fileList.size();
									
									          	if (listSize > 0) {
									%>
									<tr>
										<th scope="row" class="bgbr">첨부파일</th>
										<td>
									<%

									          	  for(int i=0; i<listSize; i++) {
									          	    Board fileDTO = (Board) fileList.get(i);
									%>	
										<a href="#1" onclick="javascript:fn_fileDownLoad('<%=fileDTO.getFilePath()%>','<%=fileDTO.getFileName()%>','<%=fileDTO.getRealFileName()%>')" class="orange underline" ><%=fileDTO.getFileName()%></a><br>
									<%
										            }
									%>	
										</td>
									</tr>
									<%
									          }
									%>
								</tbody>
							</table>
							
							<!-- //그리드스타일 -->
							
							<div class="btnArea">
								<p class="lft"><span class="button medium gray"><a href="#1" onclick="javascript:fn_notiList();">목록</a></span></p>
							</div>
							
						</div>
						</form>
						<!-- //테이블 view Set -->
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
			</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->


</body>
</html>
