<!--
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE> 넣으면 페이지 나오지 않음
-->
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

%>
<HTML>
<HEAD>
    <TITLE>ReportExpress</TITLE>
    <META http-equiv=Content-Type content="text/html; charset=ks_c_5601-1987"/>
    <meta http-equiv="Cache-Control" content="no-cache"/> 
    <meta http-equiv="Expires" content="0"/> 
    <meta http-equiv="Pragma" content="no-cache"/> 
    <LINK href="lib/common.css" type="text/css" rel="stylesheet"/>
	<style type="text/css">
	 td.txt {color:#615E5E; font-size:12px; font-family:돋움 , 굴림 ; letter-spacing:-10%; padding-left:10px;padding-bottom:15px; }
	</style>
</HEAD>
<SCRIPT LANGUAGE="javascript" src="/cdoc/rxpp/vista/rptrx_div.js"></SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=ErrorMsg(msg)>
<!--
    if (msg != "")  alert(msg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=PrintResult(flag)>
<!--
//        if (flag == true)
//        	alert("출력을 성공적으로 끝냈습니다.");
//        else
//        	alert("출력을 취소하셨거나 실패하였습니다.");
//-->
</SCRIPT>

<BODY bgColor=white leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>
<!-- 뷰어 설치 스크립트 호출 -->
<Table border=0 width=100% height=100%>
	<Tr>
	<Td width=100% height=100%>
		<div id="Inst" style="display:none"></div>
		<div id="objdiv" style="display:none"></div>
	</Td>
  </Tr>
</Table>
<form name=reportform action="#" method=post style="display:none">
<input type="hidden" name="rpx" value="법정허락_신청서.rpx">
<input type="hidden" name="fileurl" value="<%//=fileurl%>">
<input type="submit" style="display:none;">
<TEXTAREA name=xmldata style="visibility:hidden;height:0;width:0;" title="">
	
	<!-- 법정허락 신청서 -->
	
<root>
	<report>
	
		<c:set var="gbn1" value="0"/><c:set var="gbn2" value="0"/><c:set var="gbn3" value="0"/><c:set var="gbn4" value="0"/><c:set var="gbn5" value="0"/>
		<c:if test="${statApplication.applyType01 == '1'}">
			<c:set var="gbn1" value="1"/>
		</c:if>
		<c:if test="${statApplication.applyType02 == '1'}">
			<c:set var="gbn2" value="1"/>
		</c:if>
		<c:if test="${statApplication.applyType03 == '1'}">
			<c:set var="gbn3" value="1"/>
		</c:if>
		<c:if test="${statApplication.applyType04 == '1'}">
			<c:set var="gbn4" value="1"/>
		</c:if>
		<c:if test="${statApplication.applyType05 == '1'}">
			<c:set var="gbn5" value="1"/>
		</c:if>
		<제목 gbn1='${gbn1}' gbn2='${gbn2}' gbn3='${gbn3}' gbn4='${gbn4}' gbn5 = '${gbn5}'><![CDATA[이용 승인신청서]]></제목>
		<건수 chk='<c:if test="${statApplication.applyWorksCnt > 1}">1</c:if>'><![CDATA[${statApplication.applyWorksCnt}]]></건수>
		<제호><![CDATA[${statApplication.applyWorksTitl}]]></제호>
		<종류><![CDATA[${statApplication.applyWorksKind}]]></종류>
		<형태및수량><![CDATA[${statApplication.applyWorksForm}]]></형태및수량>
		<이용의내용><![CDATA[${statApplication.usexDesc}]]></이용의내용>
		<승인신청사유><![CDATA[${statApplication.applyReas}]]></승인신청사유>
		<보상금액><![CDATA[${statApplication.cpstAmnt}]]></보상금액>
		<신청인>
			<성명><![CDATA[${statApplication.applrName}]]></성명>
			<주민등록번호><![CDATA[${statApplication.applrResdCorpNumb}]]></주민등록번호>
			<주소><![CDATA[${statApplication.applrAddr}]]></주소>
			<전화번호><![CDATA[${statApplication.applrTelx}]]></전화번호>
		</신청인>
		<대리인>
			<성명><![CDATA[${statApplication.applyProxyName}]]></성명>
			<주민등록번호><![CDATA[${statApplication.applyProxyResdCorpNumb}]]></주민등록번호>
			<주소><![CDATA[${statApplication.applyProxyAddr}]]></주소>
			<전화번호><![CDATA[${statApplication.applyProxyTelx}]]></전화번호>
		</대리인>
		<c:set var="gbn6" value="0"/><c:set var="gbn7" value="0"/><c:set var="gbn8" value="0"/><c:set var="gbn9" value="0"/><c:set var="gbn10" value="0"/>
		<c:if test="${statApplication.applyRawCd == '50'}">
			<c:set var="gbn6" value="1"/>
		</c:if>
		<c:if test="${statApplication.applyRawCd == '51'}">
			<c:set var="gbn7" value="1"/>
		</c:if>
		<c:if test="${statApplication.applyRawCd == '52'}">
			<c:set var="gbn8" value="1"/>
		</c:if>
		<c:if test="${statApplication.applyRawCd == '89'}">
			<c:set var="gbn9" value="1"/>
		</c:if>
		<c:if test="${statApplication.applyRawCd == '97'}">
			<c:set var="gbn10" value="1"/>
		</c:if>
		<저작권법 gbn1='${gbn6}' gbn2='${gbn7}' gbn3='${gbn8}' gbn4='${gbn9}' gbn5 = '${gbn10}'></저작권법>
		<구분 gbn1='${gbn1}' gbn2='${gbn2}' gbn3='${gbn3}' gbn4='${gbn4}' gbn5 = '${gbn5}'></구분>
		<년월일>${statApplication.applyWriteYmd}</년월일>
		<신청자>${statApplication.rgstNm}</신청자>
		<수수료건수><![CDATA[${statApplication.applyWorksCnt}]]></수수료건수>
		<c:set var="money" value="10000"/>
		<수수료><![CDATA[${10000 * statApplication.applyWorksCnt}]]></수수료>
		
	</report>
</root>  
	
</TEXTAREA>
</form>
<script language="javascript" src="/cdoc/rxpp/vista/inst_page_div.js?modal=${modal}"></script>

</BODY>
</HTML>