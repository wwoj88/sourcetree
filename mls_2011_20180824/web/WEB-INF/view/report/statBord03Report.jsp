<!--
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE> 넣으면 페이지 나오지 않음
-->
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

%>
<HTML>
<HEAD>
    <TITLE>ReportExpress</TITLE>
    <META http-equiv=Content-Type content="text/html; charset=ks_c_5601-1987"/>
    <meta http-equiv="X-UA-Compatible" content="IE=5" />
    <meta http-equiv="Cache-Control" content="no-cache"/> 
    <meta http-equiv="Expires" content="0"/> 
    <meta http-equiv="Pragma" content="no-cache"/> 
    <LINK href="lib/common.css" type="text/css" rel="stylesheet"/>
	<style type="text/css">
	 td.txt {color:#615E5E; font-size:12px; font-family:돋움 , 굴림 ; letter-spacing:-10%; padding-left:10px;padding-bottom:15px; }
	</style>
</HEAD>
<SCRIPT LANGUAGE="javascript" src="/cdoc/rxpp/vista/rptrx_div.js"></SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=ErrorMsg(msg)>
<!--
    if (msg != "")  alert(msg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=PrintResult(flag)>
<!--
//        if (flag == true)
//        	alert("출력을 성공적으로 끝냈습니다.");
//        else
//        	alert("출력을 취소하셨거나 실패하였습니다.");
//-->
</SCRIPT>

<BODY bgColor=white leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>
<!-- 뷰어 설치 스크립트 호출 -->
<Table border=0 width=100% height=100%>
	<Tr>
	<Td width=100% height=100%>
		<div id="Inst" style="display:none"></div>
		<div id="objdiv" style="display:none"></div>
	</Td>
  </Tr>
</Table>
<form name=reportform action="#" method=post style="display:none">
<input type="hidden" name="rpx" value="보상금공탁공고신청.rpx">
<input type="hidden" name="fileurl" value="<%//=fileurl%>">
<input type="submit" style="display:none;">
<TEXTAREA name=xmldata style="visibility:hidden;height:0;width:0;" title="">
	
	<!--보상금 공탁 공고 신청서 -->
	

<root>	
	<신청자><![CDATA[${AnucBord.anucItem8}]]></신청자>
	<신청일자><![CDATA[${AnucBord.rgstDttm}]]></신청일자>
	<c:forEach items="${SuplList}" var="SuplList">	
		 <c:if test="${SuplList.suplItemCd=='10'}">
		 	<c:set var="anuc1" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='20'}">
		 	<c:set var="anuc2" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='30'}">
		 	<c:set var="anuc3" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='40'}">
		 	<c:set var="anuc4" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='50'}">
		 	<c:set var="anuc5" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='60'}">
		 	<c:set var="anuc6" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='71'}">
		 	<c:set var="anuc7" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='72'}">
		 	<c:set var="anuc8" value="Y"></c:set>
		 </c:if>		 
	</c:forEach>

	<보상금공탁공고정보>
		<제목><![CDATA[${AnucBord.tite}의 보상금 공탁 공고]]></제목>
		<공고자><![CDATA[${AnucBord.anucItem8}]]></공고자>
		<공고일자>
			<c:if test="${empty AnucBord.openDttm}"><![CDATA[미공고]]></c:if>
			<c:if test="${!empty AnucBord.openDttm}"><![CDATA[${AnucBord.openDttm}]]></c:if>
		</공고일자>
		<저작물의제호 보완='${anuc1 }'><![CDATA[${AnucBord.anucItem1}]]></저작물의제호>
		<저작자및저작재산권자의성명 보완='${anuc2 }'><![CDATA[${AnucBord.anucItem2}]]></저작자및저작재산권자의성명>
		<저작물의이용의내용 보완='${anuc3 }'><![CDATA[${AnucBord.anucItem3}]]></저작물의이용의내용>
		<공탁금액 보완='${anuc4 }'><![CDATA[${AnucBord.anucItem4}]]></공탁금액>
		<공탁소의명칭및소재지 보완='${anuc5 }'><![CDATA[${AnucBord.anucItem5}]]></공탁소의명칭및소재지>
		<공탁근거 보완='${anuc6 }'><![CDATA[${AnucBord.anucItem6}]]></공탁근거>
		<저작물이용자의주소및성명>
			<주소 보완='${anuc7 }'><![CDATA[${AnucBord.anucItem7}]]></주소>
			<성명 보완='${anuc8 }'><![CDATA[${AnucBord.anucItem8}]]></성명>
		</저작물이용자의주소및성명>
        <첨부>
        	<c:if test="${!empty fileList }">
				<c:forEach items="${fileList}" var="fileList">
					<파일><![CDATA[${fileList.fileName }]]></파일>
    			</c:forEach>
			</c:if>
			<c:if test="${empty fileList }">
				<파일><![CDATA[없음]]></파일>
			</c:if>
		</첨부>
	</보상금공탁공고정보>
	
	<보완여부 useYn='${AnucBord.suplYn}'> (*) 표시된 항목은 담당자에 의해 보완된 항목입니다.</보완여부>
</root>
	
</TEXTAREA>
</form>
<script language="javascript" src="/cdoc/rxpp/vista/inst_page_div.js?modal=${modal}"></script>

</BODY>
</HTML>