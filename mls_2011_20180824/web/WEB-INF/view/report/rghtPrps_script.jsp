<!--
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE> 넣으면 페이지 나오지 않음
-->
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

%>
<HTML>
<HEAD>
    <TITLE>ReportExpress</TITLE>
    <META http-equiv=Content-Type content="text/html; charset=ks_c_5601-1987"/>
    <meta http-equiv="Cache-Control" content="no-cache"/> 
    <meta http-equiv="Expires" content="0"/> 
    <meta http-equiv="Pragma" content="no-cache"/> 
    <LINK href="lib/common.css" type="text/css" rel="stylesheet"/>
	<style type="text/css">
	 td.txt {color:#615E5E; font-size:12px; font-family:돋움 , 굴림 ; letter-spacing:-10%; padding-left:10px;padding-bottom:15px; }
	</style>
</HEAD>
<SCRIPT LANGUAGE="javascript" src="/cdoc/rxpp/vista/rptrx_div.js"></SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=ErrorMsg(msg)>
<!--
    if (msg != "")  alert(msg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=PrintResult(flag)>
<!--
//        if (flag == true)
//        	alert("출력을 성공적으로 끝냈습니다.");
//        else
//        	alert("출력을 취소하셨거나 실패하였습니다.");
//-->
</SCRIPT>

<BODY bgColor=white leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>
<!-- 뷰어 설치 스크립트 호출 -->
<Table border=0 width=100% height=100%>
	<Tr>
	<Td width=100% height=100%>
		<div id="Inst" style="display:none"></div>
		<div id="objdiv" style="display:none"></div>
	</Td>
  </Tr>
</Table>
<form name=reportform action="#" method=post style="display:none">
<input type="hidden" name="rpx" value="권리찾기 신청서.rpx">
<input type="hidden" name="fileurl" value="<%//=fileurl%>">
<input type="submit" style="display:none;">
<TEXTAREA name=xmldata style="visibility:hidden;height:0;width:0;" title="">
	
	<!-- (방송대본)권리찾기 신청서 -->
	
	<root>
	<report>
		<제목><![CDATA[${workList[0].TITLE} 외 ${workListCount-1}건]]></제목>
		<신청인>
			<성명>
				<한글>${rghtPrps.USER_NAME}</한글>
				<한자></한자>
				<영문></영문>
			</성명>
			<주민번호>
<c:choose>
<c:when test='${fn:length(rghtPrps.RESD_CORP_NUMB)>6}'>${fn:substring(rghtPrps.RESD_CORP_NUMB,0,6)}-${fn:substring(rghtPrps.RESD_CORP_NUMB,6,13)}</c:when>
<c:otherwise>${rghtPrps.RESD_CORP_NUMB}</c:otherwise>
</c:choose>
			</주민번호>
			<자택주소><![CDATA[${rghtPrps.HOME_ADDR}]]></자택주소>
			<사무실주소><![CDATA[${rghtPrps.BUSI_ADDR}]]></사무실주소>
			<전화번호>
				<자택>${rghtPrps.HOME_TELX_NUMB}</자택>
				<사무실>${rghtPrps.BUSI_TELX_NUMB}</사무실>
				<휴대전화>${rghtPrps.MOBL_PHON}</휴대전화>
				<팩스>${rghtPrps.FAXX_NUMB}</팩스>
			</전화번호>
			<email><![CDATA[${rghtPrps.MAIL}]]></email>
		</신청인>
		<신청목적><c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'  || rghtPrps.PRPS_RGHT_CODE == '2'}">1</c:if><c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">2</c:if></신청목적>	<!-- 1 권리자의 권리찾기, 2 이용자의 권리조회 -->
		<신청구분>2</신청구분>	<!-- 방송대본 --><!-- 1 방송, 2 방송대본, 3 어문, 4 영화, 5 음악, 6 이미지, 7 기타 -->
		<권리구분>
<![CDATA[]]>		
<c:if test="${rghtPrps.CHK_206 == '1'}">
<![CDATA[작가 - 저작권자(한국방송작가협회)
]]></c:if>
		</권리구분>
		<저작물설명><![CDATA[]]></저작물설명>
		<신청내용>
<![CDATA[
${rghtPrps.PRPS_DESC}
]]>
		</신청내용>
		<신청년>${fn:substring(rghtPrps.RGST_DTTM,0,4)}</신청년>
		<신청월>${fn:substring(rghtPrps.RGST_DTTM,4,6)}</신청월>
		<신청일>${fn:substring(rghtPrps.RGST_DTTM,6,8)}</신청일>
		<신청인2>${rghtPrps.USER_NAME}</신청인2>
		
		<!-- 방송대본 명세서 시작 --> <!-- 신청서 구분에 따라 변경되는 부분 -->
		
		<명세표>		
			<작성년>${fn:substring(rghtPrps.RGST_DTTM,0,4)}</작성년>
			<작성월>${fn:substring(rghtPrps.RGST_DTTM,4,6)}</작성월>
			<작성일>${fn:substring(rghtPrps.RGST_DTTM,6,8)}</작성일>
			
<c:if test="${!empty workList}">

	<c:forEach items="${workList}" var="workList">
		<c:set var="NO" value="${workList.totalRow}"/>
		<c:set var="i" value="${i+1}"/>	
		
		<저작물>
			<순번>${i}</순번>
			<구분><![CDATA[${workList.PRPS_IDNT_CODE_VALUE }]]></구분>
			<저작물명><![CDATA[${workList.TITLE }]]></저작물명>
			<장르><![CDATA[${workList.GENRE_KIND_NAME }]]></장르>
			<방송매체>
<c:choose>
<c:when test="${workList.BROAD_MEDI == 'T'}">TV</c:when>
<c:when test="${workList.BROAD_MEDI == 'R'}">RADIO</c:when>
<c:when test="${workList.BROAD_MEDI == 'C'}">CABLE</c:when>
<c:when test="${workList.BROAD_MEDI == 'I'}">INTERNET</c:when>
<c:when test="${workList.BROAD_MEDI == 'X'}">기타</c:when>
</c:choose>
			</방송매체>
			<방송사><![CDATA[${workList.BROAD_STAT_NAME }]]></방송사>
			<방송회차><![CDATA[${workList.BROAD_ORD }]]></방송회차>
			<방송일시><![CDATA[${workList.BROAD_DATE }]]></방송일시>
			<연출><![CDATA[${workList.DIRECT }]]></연출>
			<제작사><![CDATA[${workList.MAKER }]]></제작사>
			
			<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'  || rghtPrps.PRPS_RGHT_CODE == '2'}">
				<작가><![CDATA[${workList.WRITER }]]></작가>
			</c:if>
		</저작물>
		
	</c:forEach>
</c:if>
		</명세표>		<!-- 신청서 구분에 따라 변경되는 부분 END -->
	</report>
</root>  
	
</TEXTAREA>
</form>
<script language="javascript" src="/cdoc/rxpp/vista/inst_page_div.js?modal=${modal}"></script>

</BODY>
</HTML>

