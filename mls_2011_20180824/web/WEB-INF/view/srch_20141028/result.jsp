<%@ page pageEncoding="euc-kr"%>
<div class="h2_box floatDiv mt10">
			<h2 class="fl"><%=targetname%>(<%=replaceComma(foundnum)%>건)</h2>
				<%if(target.compareTo("total")==0 && foundnum>5){%>
					<a href="javascript:moreResult('1','<%=temptarget%>','<%=resrch%>');" class="fr more">더보기</a>
				<%}%>
</div>
							
							<table border="1" cellspacing="0" cellpadding="0" summary="저작권찾기 신청정보입니다." class="grid">
								<colgroup>
								<col width="10%">
								<col>
								<col>
								</colgroup>
								<tbody>
									<%
												for(int row=1; row<=resultnum; row++){				
													title		    =	Mir.GetFieldVal(row, "@title");
													collname		=	Mir.GetFieldVal(row, "collname");
													category		=	Mir.GetFieldVal(row, "category");
													worksid		  =	Mir.GetFieldVal(row, "worksid");
													content1		=	Mir.GetFieldVal(row, "@content1");
													content2		=	Mir.GetFieldVal(row, "@content2");
													content3		=	Mir.GetFieldVal(row, "@content3");
													content4		=	Mir.GetFieldVal(row, "@content4");
													content5		=	Mir.GetFieldVal(row, "@content5");
													content6		=	Mir.GetFieldVal(row, "@content6");
													content7		=	Mir.GetFieldVal(row, "@content7");
													content8		=	Mir.GetFieldVal(row, "@content8");
													content9		=	Mir.GetFieldVal(row, "@content9");
													content10		=	Mir.GetFieldVal(row, "@content10");
													content11		=	Mir.GetFieldVal(row, "@content11");
													content12		=	Mir.GetFieldVal(row, "@content12");
													
									%>
									<%if(temptarget.compareTo("1")==0){%>
										<%if(collname.compareTo("law_broadcast")==0){%>
										<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">앨범명 : <%=content1%></span>
												<span class="w60">음반제작자 : <%=content2%></span>
												<span class="w60">가수명 : <%=content3%></span>
												<span class="w60">연주자 : <%=content4%></span>
											</p>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:fn_goStep(<%=worksid %>)"><span class="sup1"></span>법정허락 이용승인 신청</a></span></td>
									</tr>
									  <%}else if(collname.compareTo("law_subject")==0){%>
									  <tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">교과목 : <%=content1%></span>
												<span class="w60">출판사 : <%=content2%></span>
												<span class="w60">저작자 : <%=content3%></span>
											</p>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:fn_goStep(<%=worksid %>)"><span class="sup1"></span>법정허락 이용승인 신청</a></span></td>
									</tr>
									  <%}else if(collname.compareTo("law_library")==0){%>
									   <tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">출판사 : <%=content1%></span>
												<span class="w60">저자 : <%=content2%></span>
											</p>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:fn_goStep(<%=worksid %>)"><span class="sup1"></span>법정허락 이용승인 신청</a></span></td>
									</tr>
									  <%}else if(collname.compareTo("law_unknown")==0){%>
									  <tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">발행자 : <%=content1%></span>
												<span class="w60">공표일자 : <%=content2%></span>
												<span class="w60">작가 : <%=content3%></span>
											</p>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:fn_goStep(<%=worksid %>)"><span class="sup1"></span>법정허락 이용승인 신청</a></span></td>
									</tr>
									  <%}else if(collname.compareTo("law_gain")==0){%>
									<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">형태 및 수량 : <%=content1%></span>
												<span class="w60">저작재산권자 : <%=content2%></span>
												<span class="w60">승인일 : <%=content3%></span>
											</p>
										</td>
										<td class="ce"><span class="button medium blue icon"><a href="javascript:fn_goStep(<%=worksid %>)"><span class="sup1"></span>법정허락 이용승인 신청</a></span></td>
									</tr>
									<%}//collname(1)%>
									<%}else if(temptarget.compareTo("2")==0){%>
										<%if(collname.compareTo("lic_reg")==0){%>
									<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												 <span class="w60">등록번호 : <%=content1%></span>
												 <span class="w60">등록일자 : <%=content2%></span>
												 <span>등록부문 : <%=content3%> <%=content4%></span>
												 <span class="w60">저작자 : <%=content5%></span>
												 <span>등록원인 :  <%=content6%></span>
												 <span class="w60">등록권리자 :  <%=content7%></span>
											</p>
										</td>
									<td class="ce"><span class="button medium blue icon"><a href="javascript:;" onclick="crosPop('<%=content1.replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>","")
									%>')"><span class="sup3"></span>저작권 등록 정보 조회</a></span></td>
									</tr>
										<%}//collname%>
									<%}else if(temptarget.compareTo("3")==0){%>
										<%if(collname.compareTo("dep_music")==0){%>
										<%
												if(!worksid.equals("1366349")){ 
										%>
										<tr>
											<th scope="row" class="vtop"><%=category%></th>
											<td class="liceSrch">
												<b><%=title%></b>
												<p class="mt15">
													<span class="w60">앨범명 : <%=content1%></span>
													<span>앨범발매년도 : <%=content2%></span>
													<span class="w60">가수 : <%=content3%></span>
													<%
													 	if(worksid.equals("1260547")) {
													%>
													<span>연주가 : [코러스]김현아 [BASS GUITAR]김용한 [기타]이성렬</span>		
													<%
														} else{
													%>
													<span>연주가 : <%=content4%></span>		
													<%
														}
													%>											
													<span class="w60">작사가 : <%=content5%></span>
													<span class="w60">작곡가 : <%=content6%></span>
													<span>편곡가 : <%=content7%></span>
													<span class="w60">역사가 : <%=content8%></span>
													<span class="w60">음반제작자 : <%=content9%></span>
													
													<%
													 	if(worksid.equals("1260551") || worksid.equals("1260547")) {
													%>
													<span>권리관리기관(저작권) : 미확인 / 권리관리기관(실연) : 한국음악실연자연합회 / 권리관리기관(제작) : 미확인 </span>
													<%
														} else{
													%>
													<span>권리관리기관 : <%=content10%></span>
													<%
														}
													%>
													
												
												</p>
											</td>
										</tr>
										<%
												}
										%>
										<%}else if(collname.compareTo("dep_literature")==0){%>
										<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">부제 : <%=content1%></span>
												<span>창작년도 : <%=content2%></span>
												<span class="w60">도서명 : <%=content3%></span>
												<span>출판사 : <%=content4%></span>
												<span class="w60">발행년도 : <%=content5%></span>
												<span class="w90">저작자 :  <%=content6%></span>
												<span>권리관리기관 : <%=content7%></span>
											</p>
										</td>
									</tr>
										<%}else if(collname.compareTo("dep_scenario")==0){%>
										<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">작품명(대제목) : <%=title%></span>
												<span>장르/소재 : <%=content1%>/<%=content2%>,<%=content3%></span>
												<span class="w60">원작명  : <%=content4%></span>
												<span>원작작가 : <%=content5%></span>
												<span class="w60">방송일자/회차  : <%=content6%> / <%=content7%></span>
												<span class="w60">주요출연진 : <%=content8%></span>
												<span class="w90">제작사 :  <%=content9%></span>
												<span>작가(저자) : <%=content10%></span>
												<span class="w90">연출자 :  <%=content11%></span>
												<span class="w90">권리관리기관 :  <%=content12%></span>
											</p>
										</td>
									</tr>
									  <%}else if(collname.compareTo("dep_movie")==0){%>
									  <tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">제작년도 : <%=content1%></span>
												<span>출연자 : <%=content2%></span>
												<span class="w60">감독 : <%=content3%></span>
												<span>작가 : <%=content4%></span>
												<span class="w60">연출자 : <%=content5%></span>
												<span>제작자 :  <%=content6%></span>
												<span class="w60">배급사 :  <%=content7%></span>
												<span class="w95">투자자 :  <%=content8%></span>
												<span>권리관리기관 : <%=content9%></span>
											</p>
										</td>
									</tr>
									  <%}else if(collname.compareTo("dep_broadcast")==0){%>
									  <tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">장르  : <%=content1%></span>
												<span>제작년도 : <%=content2%></span>
												<span class="w60">회차 : <%=content3%></span>
												<span>연출자 : <%=content4%></span>
												<span class="w60">작가 : <%=content5%></span>
												<span>권리관리기관 :  <%=content6%></span>
											</p>
										</td>
									</tr>
									  <%}else if(collname.compareTo("dep_news")==0){%>
									  <tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">기사일자 : <%=content1%></span>
												<span>지면번호/지면면종 : <%=content2%>/<%=content3%></span>
												<span class="w60">기고자 : <%=content4%></span>
												<span>기자 : <%=content5%></span>
												<span class="w60">언론사 : <%=content6%></span>
												<span>권리관리기관 :  <%=content7%></span>
											</p>
										</td>
									</tr>
								  	<%}else if(collname.compareTo("dep_art")==0){%>
								  		<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
											<b><%=title%></b>
											<p class="mt15">
												<span class="w60">부제 : <%=content1%></span>
												<span>분류 : <%=content2%></span>
												<span class="w60">저작년월일 : <%=content3%> </span>
												<span>출처 : <%=content4%></span>
												<span class="w60">주재료 : <%=content5%></span>
												<span>구조 및 특징 : <%=content6%></span>
												<span class="w60">작가 :  <%=content7%></span>
												<span >소장기관명 : <%=content8%></span>
												<span class="w60">소장년월일 : <%=content9%></span>
												<span>권리관리기관 : <%=content10%></span>
											</p>
										</td>
									</tr>
						     		<%}else if(collname.compareTo("dep_etc")==0){%>
						     		<tr>
										<th scope="row" class="vtop"><%=category%></th>
										<td class="liceSrch">
										<b><%=title%></b>
											<p class="mt15">
												<span class="w60">저작물 종류 : <%=content1%></span>
												<span>창작년도 : <%=content2%></span>
												<span class="w60">공표매체 : <%=content3%></span>
												<span>공표일자 : <%=content4%></span>
												<span class="w60">저작자 :  <%=content5%></span>
												<span>권리관리기관 : <%=content6%></span>
											</p>
										</td>
									</tr>
						     		<%}%>
									<%}%>
								<%}//for%>
								
								</tbody>
							</table>
							
							<%if(temptarget.compareTo("1")==0){%>
							<div class="comment_box">
								<strong class="line15 black"><img alt="" class="vmid" src="images/2012/common/ic_tip.gif" /> 알려드립니다.</strong>
								<p class="comment block ml20 mt5">저작권 통합 검색을 통해서 검색목록에 대상이 없거나 검색결과가 없으면 [저작권자 찾기위한 상당한 노력 신청] 버튼을 클릭하여 진행하세요.</p>
							</div>
							<div class="btnArea" id ="btnArea">
							<p class="rgt"><span class="button medium icon"><a href="javascript:;" onclick="statBoRegiPop()"><span class="sup2"></span>저작권자 찾기위한 상당한 노력 신청</a></span></p>
							</div>
							<%}%>
	 