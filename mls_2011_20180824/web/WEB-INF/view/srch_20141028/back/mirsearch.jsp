<%@ page pageEncoding="euc-kr"%>
<%@ page language="java" contentType="text/html; charset=euc-kr"%>
<%@ page import="java.io.*" %>
<jsp:useBean id="Mir" scope="request" class="mirsearch280.MirSearchClient" />
              
<%@ include file = "./include/utils.jsp" %> <!-- 공통 함수 include -->
<%@ include file = "./include/global.jsp" %><!-- 공통 변수 include -->

<html>    
<head>
  <title>:: 통합검색 -</title>
  <meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
  <script type="text/javascript" src="/js/search/mirsearch.js"></script>							<!-- 검색관련 자바스크립트 -->
  
  <script type="text/javascript">
   function autosubmit(f,resrch){
		  f.resrch.value = resrch;
			f.mode.value="paging";
			f.submit();   
    }
    
    jQuery(window).load(function(){		
		changeList('<%=target%>','<%=genreCd%>');
	  });
	  
	  function changeList(value,value1){
			
			if(value == 1){
				var j =["어문","음악","연극","미술","건축","사진","영상","도형","컴퓨터프로그램","기타"];
				var t =["어문","음악","연극","미술","건축","사진","영상","도형","컴퓨터프로그램","기타"];
				
				var options = '';
				options += '<option value="total">-- 전체 --</option>';
				for (var i = 0; i < j.length; i++) {
					if(value1==t[i]){
						options += '<option value="' + t[i] + '" selected>' + j[i] + '</option>';
					}else{							  
					  options += '<option value="' + t[i] + '">' + j[i] + '</option>';
					}
				}
			}else if(value == 2){
				var j =["어문","음악","연극","미술","건축","사진","영상","도형","컴퓨터프로그램","편집","2차적저작물","음반","실연","기타"];
				var t =["어문","음악","연극","미술","건축","사진","영상","도형","컴퓨터프로그램","편집","2차적저작물","음반","실연","기타"];
				
				var options = '';
				options += '<option value="total">-- 전체 --</option>';
				for (var i = 0; i < j.length; i++) {
					if(value1==t[i]){
						options += '<option value="' + t[i] + '" selected>' + j[i] + '</option>';
					}else{							
					  options += '<option value="' + t[i] + '">' + j[i] + '</option>';
					}
				}
			}else if(value == 3){
				var j =["음악","어문","방송대본","영화","방송","뉴스","미술","기타"];
				var t =["음악","어문","방송대본","영화","방송","뉴스","미술","기타"];
				
				var options = '';
				options += '<option value="total">-- 전체 --</option>';
				for (var i = 0; i < j.length; i++) {
					if(value1==t[i]){
						options += '<option value="' + t[i] + '" selected>' + j[i] + '</option>';
					}else{							
					  options += '<option value="' + t[i] + '">' + j[i] + '</option>';
					}
				}
			}else if(value == 5){
				var j =["음악","연극","미술","건축","사진","영상","도형","컴퓨터프로그램","2차적저작물","편집저작물","실연","음반","방송","데이터베이스","기타"];
				var t =["음악","연극","미술","건축","사진","영상","도형","컴퓨터프로그램","2차적저작물","편집저작물","실연","음반","방송","데이터베이스","기타"];
				
				var options = '';
				options += '<option value="total">-- 전체 --</option>';
				for (var i = 0; i < j.length; i++) {
					if(value1==t[i]){
						options += '<option value="' + t[i] + '" selected>' + j[i] + '</option>';
					}else{							
					  options += '<option value="' + t[i] + '">' + j[i] + '</option>';
					}
				}
			}else{
				var options = '';
				options += '<option value="total">-- 전체 --</option>';
			}		
			/* else if(value == 4){
				var j =["음악","영화","앨범","도서","방송콘텐츠","방송콘텐츠저작권자","어문","어문저작권자","음악저작권자","방송대본","방송대본저작권자","방송","방송저작권자","영화저작권자","뉴스","뉴스저작권자","공공","공공저작권자","미술","미술저작권자","캐릭터","캐릭터저작권자"];
				var t =["music","movie","album","book","content","content_licensor","literature","literature_licensor","music_licensor","scenario","scenario_licensor","broadcast","broadcast_licensor","movie_licensor","news","news_licensor","public","public_licensor","art","art_licensor","character","character_licensor"];
				
				var options = '';
				options += '<option value="total">-- 전체 --</option>';
				for (var i = 0; i < j.length; i++) {
					if(value1==t[i]){
						options += '<option value="' + t[i] + '" selected>' + j[i] + '</option>';
					}else{							
					  options += '<option value="' + t[i] + '">' + j[i] + '</option>';
					}
				}
			} */
			jQuery("#genreCd").html(options);
}
  </script>             
</head>
<body>

<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					
					<p class="path"><span>Home</span><span>저작권자찾기</span><span>저작권자찾기검색</span><em>저작권 통합검색</em></p>
					<h1><img src="images/2012/title/content_h1_0302.gif" alt="저작권 통합검색" title="저작권 통합검색" /></h1>
					
					<div class="section relative">
						
					
						<%@include file="top.jsp"%>
<%		if(query.compareTo("")==0)  { %>
   					<%@include file="nullquery.jsp"%>    
<%		}else{%>
<%			int err=0, flag=0;

			// Session을 사용할 수 있도록 합니다.
			HttpSession SrchSession=request.getSession(true);
			String srchSession;

			if(target.compareTo("total")==0){ // 통합검색이면... %>
				<%@include file="total.jsp"%>
<%			} else { // 개별검색이면 %>
				<%@include file="each.jsp"%>
<%			}
		} %>

				</div>
				<!-- //주요컨텐츠 end -->
</body>