<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%                                                     
	User user = SessionUtil.getSession(request);      
	String sessUserIdnt = user.getUserIdnt();
	
	String sessUserName = user.getUserName();
	String sessSsnNo = user.getSsnNo();	
	
	pageContext.setAttribute("UserName", sessUserIdnt);
	
	
	                                                  
%>                                      
<html lang="ko">   
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >     
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 검색 및 상당한 노력 신청 서비스 이용 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">    
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--   
function statBoRegiPop(){ 
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
	}else{
		window.open("/statBord/statBo06Regi.do", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes");
	}
}
          
function crosPop(content1){
	window.open("http://www.cros.or.kr/report/search.cc?fl=*%2Cscore&sortField=score&q="+content1, "small", "width=750, height=650, scrollbars=yes, menubar=no, location=yes");
}

//법정허락 신청
function fn_goStep(value){
	var userId = '<%=sessUserIdnt%>';
	
	var userName = '<%=sessUserName%>';
	
	if(document.hidForm.isSsnNo.value == ''){
		document.hidForm.isSsnNo.value = '<%=sessSsnNo%>';
	}
	
	var isSsnNo = document.hidForm.isSsnNo.value;
	
	
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}else{
		if(isSsnNo == 'null' || isSsnNo == ''){
			window.open('/user/user.do?method=goSsnNoConf&userIdnt='+userId+'&userName='+userName+'&sDiv=03&value='+value,'win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=375');
			return;
		}else{
			var hddnFrm = document.hidForm;
			var frm = document.search;
			hddnFrm.worksTitle.value=frm.query.value;
			hddnFrm.action = "/stat/statPrpsMain.do";
			var worksId = value;
			hddnFrm.worksId.value = worksId;
			hddnFrm.target = "_self";
			hddnFrm.method = "post";
			hddnFrm.submit();
		}
	}
}
 

//-->                            
</script>      
</head>           
               
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
   
		<!-- HEADER str-->
		<jsp:include	page="/include/2012/header.jsp" /> 
		<script type="text/javascript">initNavigation(3);</script>
		<!-- HEADER end -->
		                                      
		<!-- CONTAINER str-->    
		<div id="container" class="container_vis3">
			<div class="container_vis">
				<h2><img src="images/2012/title/container_vis_h2_3.gif" alt="저작권자찾기" title="저작권자찾기" /></h2>
			</div>                  
                                                                 
			<div class="content">                             
			                                                
				<!-- 래프 -->        
				<jsp:include page="/include/2012/leftMenu03.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1","lnb12");    
 				</script>    
				<!-- //래프 -->      
				           
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>                   
				</div>       
				
				 <!-- //Search str -->   
				<%@include file="mirsearch.jsp"%>                                                                   
				 <!-- //Search end -->
				
			
		</div>  
		<!-- //CONTAINER end -->
		    
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	</div>
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

