<%@ page pageEncoding="euc-kr"%>
<%
	// 검색 초기값 설정
	//Mir.setDebug("D:\\WORKSPACE\\jsp\\WebContent\\log.txt");
	String   target			= interpret(request.getParameter("target"), "total");						// Category(tab, select box)
	String   genreCd			= interpret(request.getParameter("genreCd"), "total");						// Category(tab, select box)
	String   collection		= interpret(request.getParameter("collection"), "sample.col");				// Collection Name	
	String   query			= interpret(request.getParameter("query"), "");								// query

	String   sortfield		= interpret(request.getParameter("sortfield"), "score");					// sort field
	String   sortorder		= interpret(request.getParameter("sortorder"), "desc");						// sort order
	String   resrch			= interpret(request.getParameter("resrch"), "no");							// 결과내재검색
	String   mode			= interpret(request.getParameter("mode"), "");								// paging, tab
	String   arr_range		= interpret(request.getParameter("arr_range"), "all");						// zone(제목, 작성자....)
	int      curpage		= Integer.parseInt(interpret(request.getParameter("page"), "1"));			// 페이지 번호
	int      resultcount	= Integer.parseInt(interpret(request.getParameter("resultcount"), "5"));	// 화면에 출력될 건수
	//String   startDate		= interpret(request.getParameter("startDate"), "");							// 기간선택(시작일)	
	//String   endDate		= interpret(request.getParameter("endDate"), "");							// 기간선택(종료일)
	
	String serverhost="61.104.39.36";							// Mir-Search server
	int searchport=4004;		 							// Mir-Search port
	String starttag = "<font color=\"#1AB1F6\"><b>";		// 하이라이팅 시작 테그
  String endtag   = "</b></font>";						// 하이라이팅 종료 테그
  
	String queryparser="type_one";							// 쿼리파서 설정
	
	int pageview=10;										// 페이징시 페이지 번호 수 (|1|2|3|4|5|6|7|8|9|10|)
	int pagenum=0;											// 페이지 갯수 (건수가 27건이면 pagenum=3, 단 maxdoc가 500으로 셋팅되면 pagenum=50 )  
	int middle=1;											
	int maxdoc=500;											// 검색결과의 최대 건수(default:200)
	int fromdoc=((curpage-1)*resultcount)+1;				// 시작 문서번호
	int todoc=((curpage-1)*resultcount)+resultcount;		// 끝 문서 번호
	maxdoc=todoc;
	
	int i=0, foundnum=0, resultnum=0, validnum=0, totalnum=0;		// 검색된 결과 전체 건수 리턴,현재 결과 리스트 건수 리턴,유효한 결과 리스트 건수 리턴,검색된 결과의 전체 건수 리턴
		        
	String sortspec=sortfield+" "+sortorder;				// 검색결과의 정렬 방법(default:score desc)
	query=toHan(query);										// encodeing
	String querytext=MakeQuery(StringReplace(query.trim()), arr_range);				// 검색엔진 쿼리(특수문자 제거)

	//*********** [통합검색 -CLMS 제외 - 권리인증 추가] Start ***********//
	String targetnamelist[]={"통합검색","법정허락 대상저작물","저작권 등록부","위탁관리 저작물","권리인증"};
	String temptargetlist[]={"total","1","2","3","5"};					
	String tempColllist[]={"all","law.clm","lic_reg.col","dep.clm","cert.apply"};
	
	String fields="key,worksid,collname,@title,category,@content1,@content2,@content3,@content4,@content5,@content6,@content7,@content8,@content9,@content10,@content11,@content12";		// Collection fields 설정( 필드명 앞에 @일경우 하이라이팅  )
	String key="",worksid="",collname="",title="",category="",content1="",content2="",content3="",content4="",content5="",content6="",content7="",content8="",content9="",content10="",content11="",content12="";						// 변수
  //*********** [통합검색 -CLMS 제외 - 권리인증 추가] End ***********//

	//*********** [통합검색 -CLMS 포함] Start ***********//
	/*
	String targetnamelist[]={"통합검색","법정허락 대상저작물","저작권 등록부","위탁관리 저작물","통합저작권정보"};  
	String temptargetlist[]={"total","1","2","3","4"};					
	String tempColllist[]={"all","law.clm","lic_reg.col","dep.clm","clms.col"};

	String fields="key,worksid,collname,@title,category,@content1,@content2,@content3,@content4,@content5,@content6,@content7,@content8,@content9,@content10,@content11,@content12,crId,@albumTitle,@bookTitle,mediCodeNM,@genreNM,licensorSeq,publisher,chnlCodeNM,@movieTypeNM,stageNameSeq,stdCrhId,progId,@viewGradeNM,licensorCode,writer,progOrdseq,runTime,@licensorNameKor,firstEditionYear,prodQultyNM,produceYear,licensorNameKor,publ,charDesc,ptablTitle,releaseDate,prodName,workNmKor,progGrad,@leadingActor,prodCnt,ProdSubTitle,broadOrd,@director,licensorRole,broadDate,direct,genreKindNM,origWork,broadMediNM,broadStatNM,@producer,performName,@investor,performCnt,formatCode,licenosrId,mediaCode,natiName,crh_id_of_ca,stru_ftre,produceDate,providerVcd,producer,caName,startday,distributor,articlDt,program_code,@musicTitle,singer,lyricist,composer,stdCrhId,nrId,producer,issuedDate,progName,licensorId,licensorYmd";		
	String key="",worksid="",collname="",title="",category="",content1="",content2="",content3="",content4="",content5="",content6="",content7="",content8="",content9="",content10="",content11="",content12="";						// 변수
	*/
	//*********** [통합검색 -CLMS 포함] End ***********//


	int targetcount=targetnamelist.length-1;

  String targetname=targetnamelist[0]; 
	String temptarget=temptargetlist[0];
%>	
	
	<%@include file="clms_global.jsp"%>
