<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>


<%
	String domain = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("strSaveUrl2"));                      //URL을 변경해 주세요
	User user = SessionUtil.getSession(request);
	
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	pageContext.setAttribute("UserName", sessUserName); 
	pageContext.setAttribute("UserIdnt", sessUserIdnt); 

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 공고 - 보상금 지급사실 공고(${AnucBord.tite}) | 법정허락 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
function ObjcInsert(){  // 권리자 이의제기 insert
	var frm = document.getElementById("ObjcInsertForm");
	var bordSeqn = document.getElementById("bordSeqn").value;
	var bordCd = document.getElementById("bordCd").value;
 	frm.action = "/statBord/objcInsert.do?bordSeqn="+bordSeqn+"&bordCd="+bordCd;
 	frm.submit();
}
function updateOc(statObjcId){	//권리자 이의제기 update 열닫
	var i=statObjcId;
	var docu = document.getElementById("updatediv"+i).style.display;
	if(docu == "none"){
		document.getElementById("updatediv"+i).style.display = "block";
		document.getElementById("commentdiv"+i).style.display = "none";
	}else{
		document.getElementById("updatediv"+i).style.display = "none";
		document.getElementById("commentdiv"+i).style.display = "block";
	}
}
function ObjcInsertoc(){			//권리자 이의제기 insert 열닫
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
	}else if(userId != 'null' || userId!= ''){
		var i = document.getElementById("ObjcInsert").style.display;
		if(i == "none"){
			document.getElementById("ObjcInsert").style.display = "block";
		}else{
			document.getElementById("ObjcInsert").style.display = "none";
		}
	}
	
}
function updateObjc(){
	var upf = document.getElementById("updateObjcForm");
	upf.submit();
	
}
var filenum = 1;
function addfile(){			// 파일 input 추가
	filenum++;
	var fd= document.getElementById("p");
	if(filenum > 5){
		alert("첨부파일은 최대 5개까지 가능합니다.");
	}else
// 	fd.innerHTML += "<br/><input type='file' id='file" + filenum +"'name='file" + filenum +"'>&nbsp;&nbsp;&nbsp;<input type='button' name='"+ filenum +"' id='del" + filenum +"' value='삭제' onclick='javascript:delfile(this.name)'>";
	var inputFile = "<p id='fileP"+filenum+"'><input type='file' id='file" + filenum +"'name='file" + filenum +"'>&nbsp;&nbsp;&nbsp;<input type='button' name='"+ filenum +"' id='del" + filenum +"' value='삭제' onclick='javascript:delfile(this.name)'><\/p>";
	jQuery("#p").append(inputFile);
}
function delfile(id){		//파일 input 
	var file = document.getElementById("file").id;
   	var delb = document.getElementById("del").id;
  	var filen = file+id;	
  	var deln = delb+id;
  	document.getElementById(filen).outerHTML="";
  	document.getElementById(deln).outerHTML="";
  	filenum--;
}

function fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.getElementById("form1");
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.target="boardView";
	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
}

jQuery(document).ready(function() {
	
	var isScan = 'N';
	
	var fileName = '';
	
	var subFileName = '';
	var scanfileName = '';
	var orgFileName = '';
	
	var domain = '<%=domain%>';
	
	var idx = 0;
	
	var delIdx = 999;
	
	//alert(domain);
	
	<c:forEach items="${fileList}" var="fileList">
    	//alert('${fileList.realFileName}');
    	//alert('${fileList.fileName}');
    	idx = idx+1;
    	fileName = '${fileList.fileName}';
    	
    	subFileName = fileName.substr(0,12);
    	
    	//alert(subFileName);
    	
    	//alert('idx: '+idx);
    	
    	if(subFileName == "[공고문스캔파일등록]_"){
    		//alert("공고등록이당");
    		delIdx = idx;
    		isScan = 'Y';
   			orgFileName = '${fileList.realFileName}';
   			//alert('delIdx: '+delIdx);
   			//alert('isScan: '+isScan);
    	}else{
    		if(isScan == 'Y'){
	    		isScan = 'Y';
    		}else{
    			isScan = 'N';
    		}
    	}
	</c:forEach>
	
	if(isScan == 'Y'){
		//alert("text안보이게");
    	var img = jQuery("<img>").attr({"src":domain+orgFileName,"width":700});

    	jQuery("#img").prepend(img);
		jQuery("#img").css("display","block");
		jQuery("#text").css("display","none");
	}else{
    	jQuery("#text").css("display","block");
    	jQuery("#img").css("display","none");
	}
	
	if(idx > 0 && delIdx != 999 ){//첨부파일이 한개 이상이고, 해당 첨부파일에는 스캔파일이 존재를 한다. 
		//alert("삭제하자!");
		jQuery("#tblAttachFileScan>tbody:eq(0)>tr").eq(delIdx).remove();
		if(idx == 1){
			jQuery("#addFile").css("display","none");
		}
	}else if(idx == 0){
		jQuery("#addFile").css("display","none");
	}
	
	 if (jQuery("body").has("#now_loading").html() == null) {
	  var tmpHtml = "";
	  tmpHtml = "<div id='now_loading' style='position: absolute; top: 0; left: 0; display:none;z-index:9000'>";
	  tmpHtml += "<table width=200 height=100 border=0> ";
	  tmpHtml += "    <tr>";
	  tmpHtml += "        <td align=center><img id='ajax_load_type_img' src='/images/loading.gif'></td>";
	  tmpHtml += "    </tr>";
	  tmpHtml += "</table>";
	  tmpHtml += "</div>";

	  jQuery('#contentBody').append(tmpHtml).ajaxStart(function() {
		    var width = jQuery('#contentBody').width();
			var height = jQuery('#contentBody').height();
			
			//화면을 가리는 레이어의 사이즈 조정
			jQuery(".backLayer").width(width);
			jQuery(".backLayer").height(height);
			
			//화면을 가리는 레이어를 보여준다 (0.5초동안 30%의 농도의 투명도)
			jQuery(".backLayer").fadeTo(100, 0.0);
	   		showLoading();
	  }).ajaxStop(function() {
	    jQuery(".backLayer").fadeOut(1000);
	    hideLoading();
	  
	  });
	 }
});
jQuery(window).resize(function(){
	var width = jQuery('#contentBody').width();
	var height = jQuery('#contentBody').height();
	jQuery(".backLayer").width(width).height(height);
});

function showLoading() {
	jQuery(document).bind("mousemove" ,(function(e){
		var x = e.pageX;
		var y = e.pageY;
		jQuery('#now_loading').css("left", x-460);
	    jQuery('#now_loading').css("top", y-230);
	}));
	jQuery('#now_loading').show();
}




function loginCheck(){
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
	return;
	}else{
		location.href ="/statBord/statBo01Regi.do?bordCd=5";
	}
}

function hideLoading() {
	jQuery(document).unbind("mousemove" , function(){});
	jQuery('#now_loading').hide();
}



//-->
</script>
<script> 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-2', 'auto');
  ga('send', 'pageview');
</script>
</head>
 <style>
	div.backLayer {
		display:none;
		background-color:black; 
		position:absolute;
		left:0px;
		top:0px;
	}
  </style>
 
<body>
 		<!-- HEADER str-->
 		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader3.jsp" /> --%>
		<script type="text/javascript">initNavigation(3);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			
			<!-- 래프 -->
				<div class="con_lf">
						<h2><div class="con_lf_big_title">법정허락</div></h2>
					<ul class="sub_lf_menu">
					<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
					<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
					<!-- <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
					<li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
					<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
					<!-- <li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li> -->
					<!-- <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li> -->
					<li><a class="on" href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a>
						<ul class="sub_lf_menu2">
							<li><a href="/statBord/statBo03List.do?bordCd=3">승인 신청 공고</a>
							<li><a href="/statBord/statBo04List.do?bordCd=4">승인 공고</a></li>
							<li><a href="/statBord/statBo05List.do?bordCd=5">보상금 공탁 공고</a></li>
							<li><a class="on" href="/statBord/statBo08List.do?bordCd=8">보상금 지급사실 공고</a></li>
						</ul>
					</li>
					</ul>
					</div>
			<!-- 래프 -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/right4me/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						법정허락
						&gt;
						법정허락 공고
						&gt;
						<span class="bold">보상금 지급 사실 공고</span>
					</div>
					<h1><div class="con_rt_hd_title">법정허락 공고</div></h1>
					<div id="sub_contents_con">
                     <ul class="sub_menu1 w269 mar_tp40">
						<li class="first"><a href="/statBord/statBo03List.do?bordCd=3" style="width: 200px ">승인 신청 공고</a></li>
						<li><a href="/statBord/statBo04List.do?bordCd=4" style="width: 200px;">승인 공고</a></li>
						<li><a href="/statBord/statBo05List.do?bordCd=5" class="last_rt_bor" style="width: 200px;">보상금 공탁 공고</a></li>
						<li><a href="/statBord/statBo08List.do?bordCd=8" class="last_rt_bor on" style="width: 200px;">보상금 지급사실 공고</a></li>
					</ul>
                    <p class="clear"></p>
					<br/><br/>
						<!-- 그리드스타일 -->
						<form name="form1" id="form1" method="post" >
						<table border="1" cellspacing="0" cellpadding="0" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<tbody>
								<tr>
									<th scope="row" class="p12">${AnucBord.tite}</th>
								</tr>
								<tr>
									<td ><!--<span class="p11">공고자 : ${AnucBord.anucItem3}</span>&nbsp;&nbsp;&nbsp;&nbsp;--><span class="tl">공고일 : ${AnucBord.openDttm}</span>
									<div id="text" style="">
										<ul class="mt15">
											<li>
												<p class="blue2 line22"><br/>1. 저작물의 제호</p>
												<p class="mt5 line22">제호 : ${AnucBord.tite}</p><br/>
											</li>
											<li class="mt15">
												<p class="blue2 line22">2. 저작자 또는 저작재산권자의 성명</p>
												<p class="mt5 line22">공표 당시의 저작권자 : ${AnucBord.anucItem2}</p><br/>
											</li>
											<li class="mt15">
												<p class="blue2 line22">3. 저작물 이용 내용</p>
												<p class="mt5 line22">신청인 : ${AnucBord.anucItem3}</p><br/>
											</li>
											<li class="mt15">
												<p class="blue2 line22">4. 종류</p>
												<p class="mt5 line22">종류 : ${AnucBord.anucItem4}</p><br/>
											</li>
											<li class="mt15">
												<p class="blue2 line22">5. 보상금 금액</p>
												<p class="mt5 line22">${AnucBord.anucItem5}</p><br/>
											</li>
											<li class="mt15">
												<p class="blue2 line22">6. 보상금 지급근거</p>
												<p class="mt5 line22">${AnucBord.anucItem6}</p><br/>
											</li>
											<li class="mt15">
												<p class="blue2 line22">7. 저막물 이용자의 성명</p>
												<p class="mt5 line22">${AnucBord.anucItem7}</p><br/>
											</li>
										</ul>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						</form>
						<!-- //그리드스타일 -->
						
						<!-- 버튼 str -->
						<div class="btnArea">
							<p class="fl"><span class="button medium gray"><a href="/statBord/statBo04List.do?bordCd=4">목록</a></span></p>
							<!-- <p class="fr"><span class="button medium"><a href="javascript:loginCheck()">보상금 공탁 공고등록</a></span></p> -->
						</div>
						<!-- //버튼 -->
						
					</div>
						
				</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
			</div>
		<!-- //CONTAINER end -->		
		
		<!-- FOOTER str-->
			<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

