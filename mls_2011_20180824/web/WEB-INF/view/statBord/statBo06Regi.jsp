<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();

	String sessUserAddr = user.getUserAddr();
	String sessMoblPhon = user.getMoblPhon();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 찾기 검색 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
var number=1;
function nonWriteSub(){ //등록 submit
	var genreSel = document.getElementById("genre");
	var tite = document.getElementById("tite");
	var coptSel = document.getElementById("coptHodr1");
	var coptTex = document.getElementById("coptHodrNm1");
	var anucItem1 = document.getElementById("anucItem1");
	var anucItem2 = document.getElementById("anucItem2");
	var anucItem3 = document.getElementById("anucItem3");
	var anucItem4 = document.getElementById("anucItem4");
	var anucItem5 = document.getElementById("anucItem5");
	var anucItem6 = document.getElementById("anucItem6");
	var anucItem7 = document.getElementById("anucItem7");
	var anucItem8 = document.getElementById("anucItem8");
	var anucItem9 = document.getElementById("anucItem9");
	var chkAgr = document.getElementById("chkAgr");
	var radio = document.getElementsByName("rgstReas");
	var rgstReasCd = document.getElementById("rgstReasCd");
	var radioCount = 0;
	for(var i=0; i < radio.length; i++){
		if(radio[i].checked){
			var radioValue = radio[i].value;
			rgstReasCd.value = radioValue;
			radioCount++;
		}
	}
	if(genreSel.value == 0){
		alert("장르를 선택해 주세요");
		genreSel.focus();
	}
	
	if(tite.value == "" || tite.value.length ==0){
		alert("저작물의 제호를 입력해주세요");
		tite.focus();
	}else if(anucItem1.value=="" || anucItem1.value.length == 0){
		alert("저작재산권자를 찾는다는 취지를 입력해주세요");
		anucItem1.focus();
	}else if(anucItem2.value=="" || anucItem2.value.length == 0){
		alert("저작재산권자의 성명을 입력해주세요");
		anucItem2.focus();
	}else if(anucItem3.value=="" || anucItem3.value.length == 0){
		alert("저작재산권자의 주소를 입력해주세요");
		anucItem3.focus();
	}else if(anucItem4.value=="" || anucItem4.value.length == 0){
		alert("저작재산권자의 연락처를 입력해주세요");
		anucItem4.focus();
	}else if(anucItem5.value=="" || anucItem5.value.length == 0){
		alert("공표 시 표시된 저작재산권자의 성명을 입력해주세요");
		anucItem5.focus();
	}else if(anucItem6.value=="" || anucItem6.value.length == 0){
		alert("저작물발행을 입력해주세요");
		anucItem6.focus();
	}else if(anucItem7.value=="" || anucItem7.value.length == 0){
		alert("공표연월일을 입력해주세요");
		anucItem7.focus();
	}else if(anucItem7.value.length != 8 || anucItem7.value.indexOf('.') != -1 || anucItem7.value.indexOf('/') != -1 || anucItem7.value.indexOf('-') != -1){
		alert("공표연월일의 형식이 잘못되었습니다.");
		anucItem7.focus();
	}else if(anucItem8.value=="" || anucItem8.value.length == 0){
		alert("저작물의 이용목적을 입력해주세요");
		anucItem8.focus();
	}else if(radioCount == 0){
		 alert("저작권자 찾기위한 상당한 노력 신청 사유를 선택해주세요.");
		 document.getElementById("radioFocus").focus();
	}else if(coptSel.value==0){
		alert("저작권자 및 실연자 정보를 작성해주셔야 합니다.");
		coptSel.focus();
	}else if(!chkAgr.checked){
			alert("담당자 보완 항목에 동의하여주세요");
			chkAgr.focus();
	}else{
	var genreVal = genreSel.options[genreSel.selectedIndex].value;
	var fm  = document.nonWriteForm;
	var coptHodr; 
	var coptHodrNm;
	var nameGrpArray = new Array();
	if(number >= 1){
		for(var i=1; i<=fm.number.value; i++){
	 		coptHodrNm = document.getElementById("coptHodrNm"+i).value;
	 		if(document.getElementById("coptHodr"+i).value !=0 && coptHodrNm != '' && coptHodrNm.length != 0){
	  			nameGrpArray.push(coptHodrNm);
	  		}else if(document.getElementById("coptHodr"+i).value !=0 && coptHodrNm == '' && coptHodrNm.length == 0){
	  			//저작권자 및 실연자 정보의 셀렉트 박스를 선택하고 텍스트박스에 값이 없을때
	  			alert("저작권자 및 실연자 정보에 맞는 정보를 입력해 주세요.");
	  			document.getElementById("coptHodrNm"+i).focus();
	  			return;
	  		}
	 	}
	}
	var nameGrp = nameGrpArray.join();
	var rgstReasEtc = $('#rgstReas').val(); 
	var cdGrpCdArray  = new Array();			  //저작권자 및 실연자 정보의 
	if(number >= 1){
		for(var i = 1; i <= fm.number.value; i++){	  //값을 추가된 셀렉트의 갯수만큼 
			coptHodr = document.getElementById("coptHodr"+i).value;
			if(coptHodr == 0 && document.getElementById("coptHodrNm"+i).value != '' && document.getElementById("coptHodrNm"+i).value.length != 0){
				//저작권자 및 실연자 정보의 셀렉트 박스를 선택하지 않앗는데 텍스트박스에 값이 들어있는 경우
					alert("저작권자 및 실연자 정보에 대한 종류를 먼저 선택해 주세요.");
					document.getElementById("coptHodr"+i).focus();
					return;
			}else if(coptHodr != 0 && document.getElementById("coptHodrNm"+i).value != '' || document.getElementById("coptHodrNm"+i).value.length != 0){
				cdGrpCdArray.push(coptHodr);			  //배열에 담는다
			}
		}	
	}
	var cdGrpCdLength = cdGrpCdArray.length; 	  // 배열의 길이와
	var cdGrpCd  = cdGrpCdArray.join(); 	 	  // 배열의 값을 스트링값으로 변환하여 
		$('#coptHodrRoleCd').val(cdGrpCd);		  //히든에 담아서 서브밋
		$('#coptHodrName').val(nameGrp);
		$('#coptHodrsLength').val(cdGrpCdLength);
		$('#genreCd').val(genreVal);
		$('#rgstReasEtc').val(rgstReasEtc);
		
		alert("신청된 내용은 [마이페이지>신청현황>저작권자 찾기위한 상당한 노력신청]에서\n확인이 가능합니다.");
		fm.action="/statBord/statBo06Write.do";
		fm.submit();
	}
}
var iRowIdx = 1;
function fn_addRow1(){
	  iRowIdx++;
	  if(iRowIdx > 5){
		  alert("첨부파일은 최대5개까지 가능합니다.");
		  iRowIdx--;
		  return;
	  }
	  var oTbl = document.getElementById("tblAttachFile");
	  var oTbody = oTbl.getElementsByTagName("TBODY")[0];
	  
	  var oTr = document.createElement("TR");
	  oTr.id="fileTr"+iRowIdx;
		//순번(선택)
		var oTd = document.createElement("TD");
		var sTag = '<input name="chkDel1" id="chkDel1_'+iRowIdx+'" type="checkbox" value='+iRowIdx+' />';
		oTd.innerHTML = sTag;
		oTd.id = "fileTd"+iRowIdx;
		oTd.style.width = '7%';
		oTd.className = 'ce';
		oTr.appendChild(oTd);

		
		//첨부파일명
		oTd = document.createElement("TD");
		oTd.colSpan = 2;
		sTag = '<input type="hidden" name="fileDelYn" id="fileDelYn_'+iRowIdx+'" />';
		sTag += '<span id="spfile'+iRowIdx+'"><input type="file" name="file'+iRowIdx+'" id="file_'+iRowIdx+'" class="inputData L w100" onkeydown="return false;" 	onchange="checkFile(this.id)"/></span>';
				oTd.innerHTML = sTag;
		oTr.appendChild(oTd);
		
		oTbody.appendChild(oTr);

		       
	}

function fn_delRow1(){
var oTbl = document.getElementById("tblAttachFile");
var oChkDel = document.getElementsByName("chkDel1");
var count = 0;
var checkArray = new Array();

	for(var i=0; i<oChkDel.length; i++){
	if(oChkDel[i].checked == true){
		count++;
		var checkNum="";
		checkNum += oChkDel[i].value;
		checkArray.push(checkNum);			
		iRowIdx--;
	}
}
if(count == 0){
	alert("삭제하실 내용을 체크해주세요.");
}else{
	for(var i = 0 ; i < checkArray.length ; i ++){
			element = document.getElementById('fileTr'+checkArray[i]);
			element.parentNode.removeChild(element);
	}
}
}
function changeList(value){
	var ch = document.getElementsByName("checkCopt");
	var checkArray = new Array();
	var ment ='';
	var checkArray = new Array();
	for(var i=0; i<ch.length; i++){
			var checkNum="";
			checkNum += ch[i].value;
			checkArray.push(checkNum);			
			number--;
	}
	
	for(var i = 0 ; i < checkArray.length ; i ++){
		element = document.getElementById('selP'+checkArray[i]);
		element.parentNode.removeChild(element);
	}
	ment += '<p id=coptNull></p>';
	$("#addT").before(ment);
	addSelect();
	
}

function addSelect(){
	var value=document.getElementById("genre").value;
	var bigCode = 0;
	if(value == 1){bigCode = 62;}
	else if(value==2){bigCode = 63;}
	else if(value==3){bigCode = 64;}
	else if(value==4){bigCode = 65;}
	else if(value==5){bigCode = 66;}
	else if(value==6){bigCode = 67;}
	else if(value==7){bigCode = 68;}
	else if(value==8){bigCode = 69;}
	
	var options = {
			type		: "POST",
			url 		: '/statBord/codeList.do',
			data		: {"bigCode":bigCode},
			dataType    : "json",
			contentType : "application/x-www-form-urlencoded;charset=euc-kr",
			success		: function(data){
				var seleop = '';
				number++;
				document.getElementById("number").value = number;
				seleop  += '<p id="selP'+number+'" align="left"><input type="checkbox" name="checkCopt" value="'+number+'"/>&nbsp;';
				seleop  += '<select class="inputData" name="coptHodr'+number+'"id="coptHodr'+number+'"></select>&nbsp;';
				seleop  += '<input type="text" class="inputData" id="coptHodrNm'+number+'" name="coptHodrNm'+number+'"/></p>';
				var j = data.list;
				var options = '<option value="0">-- 선택 --</option>';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].midCode + '">' + j[i].codeName+ '</option>';
				}
				if(number == 1){
					element = document.getElementById('coptNull');
					element.parentNode.removeChild(element);
					$("#addT").after(seleop);
					$("#coptHodr"+number).append(options);
				}else{
					$("#selP"+(number-1)).after(seleop);
					$("#coptHodr"+number).append(options);
				}
			}
		};$.ajax(options);
		
}


function deleteSelect(){
	var ch = document.getElementsByName("checkCopt");
	var count = 0;
	var ment ='';
	
	var checkArray = new Array();
	for(var i=0; i<ch.length; i++){
		if(ch[i].checked == true){
			count++;
			var checkNum="";
			checkNum += ch[i].value;
			checkArray.push(checkNum);			
			number--;
		}
	}
	if(count == 0){
		alert("삭제하실 내용을 체크해주세요.");
	}else{
		for(var i = 0 ; i < checkArray.length ; i ++){
				element = document.getElementById('selP'+checkArray[i]);
				element.parentNode.removeChild(element);
		}
	}
	if(number < 1){
	   ment += '<p id=coptNull>저작권자 및 실연자 정보를 추가해주세요.</p>';
		$("#addT").before(ment);
	}
}
	
function titeSet(){
	var tex = document.getElementById("tite").value;
	document.getElementById("worksTitle").value = tex;
}
$(document).ready(function() {
	 if ($("body").has("#now_loading").html() == null) {
	  var tmpHtml = "";
	  tmpHtml = "<div id='now_loading' style='position: absolute; top: 0; left: 0; display:none;z-index:9000'>";
	  tmpHtml += "<table width=200 height=100 border=0> ";
	  tmpHtml += "    <tr>";
	  tmpHtml += "        <td align=center><img id='ajax_load_type_img' src='/images/loading.gif'></td>";
	  tmpHtml += "    </tr>";
	  tmpHtml += "</table>";
	  tmpHtml += "</div>";

	  $('#contentBody').append(tmpHtml).ajaxStart(function() {
		  var width = $('#contentBody').width();
			var height = $('#contentBody').height();
			
			//화면을 가리는 레이어의 사이즈 조정
			$(".backLayer").width(width);
			$(".backLayer").height(height);
			
			//화면을 가리는 레이어를 보여준다 (0.5초동안 30%의 농도의 투명도)
			$(".backLayer").fadeTo(500, 0.0);
	  		 showLoading();
		  }).ajaxStop(function() {
		    $(".backLayer").fadeOut(1000);
		    hideLoading();
	  
	  });
	 }

	});

	function showLoading() {
		$(document).bind("mousemove" ,(function(e){
			var x = e.pageX;
			var y = e.pageY;
			$('#now_loading').css("left", x-70);
		    $('#now_loading').css("top", y-20);
		}));
		$('#now_loading').show();
	}

	function hideLoading() {
		$(document).unbind("mousemove" , function(){});
		$('#now_loading').hide();
	 }

function checkFile(fileId){
	 var option = {
				 url 		: '/statBord/fileSizeCk.do',
				 type		: "post",
				 dataType   : "json",
				 data 		: jQuery('#nonWriteForm'),
				 success	: function(data){
					if(data.fileSize > 1024*1024*2){
						alert("2MB 이상 파일은 업로드 하실 수 없습니다.");
						if (jQuery.browser.msie) {
						    jQuery("#"+fileId).replaceWith( jQuery("#"+fileId).clone(true) );
						} else {
						    jQuery("#"+fileId).val("");
						}
					}
				}
	 };
		jQuery('#nonWriteForm').ajaxSubmit(option);
}

function statBo06RegiMultiPop(){
	/* window.open("/statBord/statBo06RegiMulti.do", "Multi", "width=1000, height=850, scrollbars=yes, menubar=no, location=yes");	

	self.close(); */
	
	location.href="/statBord/statBo06RegiMulti.do";	
		
}

//-->
</script>
</head>
<style>
	div.backLayer {
		display:none;
		background-color:black; 
		position:absolute;
		left:0px;
		top:0px;
	}

  </style>
<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
			<!-- HEADER str-->
			<div id="popHeader">
				<h1>저작권자 찾기위한 상당한 노력 신청</h1>
			</div>
			<div class="topArea">		
				<span class="p11 orange ml5" style="display: inline-block; padding-right: 200px;">※ 저작재산권자나 그의 거소가 명확하지 않은 저작물의 저작권자를 찾기위한 상당한 노력에 대한 이행신청을 합니다.</span>
				<a href="#" onclick="javascript:statBo06RegiMultiPop();" class="btn_new"><img src="/images/2012/button/btn_new_app.gif" alt="다건신청화면 바로가기" title="다건신청화면 바로가기" /></a>
				
			</div>

			<!-- //HEADER end -->
			<div id="contentBody" style="padding:0 15px 15px 15px;">
				<div class="section mt15">
<!-- 						<span class="button small icon"><a href="javascript:openSmplDetail('MR')">예시화면 보기</a><span class="help"></span></span></p> -->
						
						<h2>저작권자 찾기위한 상당한 노력 신청 사유</h2>
							&nbsp;<input type="radio" title="찾고자하는 저작물이 검색결과에 없는 경우" name="rgstReas" id="radioFocus" value="1" class="mb5 mt5"> <label for="radioFocus"><span class="mb5">찾고자하는 저작물이 검색결과에 없는 경우</span></label><br/>
							&nbsp;<input type="radio" title="검색결과 제호는 동일 하나 찾고자 하는 저작물이 아닌 경우" name="rgstReas" id="radioFocus02" value="2" class="mb5 mt5"> <label for="radioFocus02"><span class="mb5">검색결과 제호는 동일 하나 찾고자 하는 저작물이 아닌 경우</span></label><br/>
							&nbsp;<input type="radio" title="검색결과 저작물은 존재하나 저작권자 정보가 불일치 하는 경우" name="rgstReas" id="radioFocus03" value="3" class="mb5 mt5"> <label for="radioFocus03"><span class="mb5">검색결과 저작물은 존재하나 저작권자 정보가 불일치 하는 경우</span></label><br/>
							&nbsp;<input type="radio" title="찾고자하는 저작물은 존재하나 인접권자 정보가 불일치 하는 경우" name="rgstReas" id="radioFocus04" value="4" class="mb5 mt5"> <label for="radioFocus04"><span class="mb5">찾고자하는 저작물은 존재하나 인접권자 정보가 불일치 하는 경우</span></label><br/>
							&nbsp;<input type="radio" title="찾고자하는 저작물은 존재하나 관련기관 확인 후 찾고자 하는 저작물이 아닌경우" name="rgstReas" id="radioFocus05" value="5" class="mb5 mt5"> <label for="radioFocus05"><span class="mb5">찾고자하는 저작물은 존재하나 관련기관 확인 후 찾고자 하는 저작물이 아닌경우</span></label><br/>
							&nbsp;<input type="radio" title="기타" name="rgstReas" id="radioFocus06"value="6" class="mb5 mt5"> <label for="radioFocus06"><span class="mb5">기타</span></label><br/>&nbsp;&nbsp;&nbsp;
							<textarea cols="10" rows="3" name ="rgstReas" id="rgstReas" class="textarea w95" style="margin-left: 7px;" ></textarea>
							<br/>
							
						<br/>
						<h2>저작물 정보</h2>
						<form method="post" action="#" name="nonWriteForm" id="nonWriteForm" enctype="multipart/form-data">
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="18%">
							<col width="*">
							<col width="42%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col">장르</th>
									<th scope="col">제호</th>
									<th scope="col">저작권자 및 실연자 정보
									<!-- 20170906기능 미구현 확인 주석처리 -->
									<!-- <a href="javascript:addSelect()"><img src="/images/2012/button/add2.gif" alt="" title="" width="14px" /></a>
									<a href="javascript:deleteSelect()"><img src="/images/2012/button/delete2.gif" alt="" title="" width="14px"/></a> -->
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="ce pd5" style="vertical-align: top;"><select onchange="javascript:changeList(this.value)" id="genre"  name="genre" class="inputData w95">
											<option value="0">-- 선택 --</option>
												<c:forEach items="${codeList}" var="codeList">
													<option value="${codeList.midCode}">${codeList.codeName}</option>
												</c:forEach>
												</select>
									</td>
									<td class="ce pd5" style="vertical-align: top;"><input type="text" title="제목" class="inputData w95" id="tite" onblur="javascript:titeSet()"/></td>
									<td class="ce pd5">
									<p id="selP1" align="left">
									<input type="checkbox" name="checkCopt" value='1'/>
									<select id="coptHodr1" name="coptHodr1" class="inputData">
											<option value="0">-- 선택 --</option>
									</select>&nbsp;<input type="text" title="저작권자 및 실연자 정보" class="inputData" id="coptHodrNm1" name="coptHodrNm1"/>
									</p>
									<p id="addT"></p>
											<input type="hidden" id="coptHodrRoleCd" name="coptHodrRoleCd" value="" />
											<input type="hidden" id="coptHodrsLength" name="coptHodrsLength" value="0" />
											<input type="hidden" id="coptHodrName" name="coptHodrName" value="" />
											<input type="hidden" id="number" name="number" value="1"/>
											<input type="hidden" name="rgstReasCd" id="rgstReasCd" value=""/>
											<input type="hidden" name="rgstReasEtc" id="rgstReasEtc" value=""/>
											<input type=hidden id="mgntDivs" name="mgntDivs" value="BO06" />
										</td>
								</tr>
							</tbody>
						</table>
						
						<h2 class="mt20">저작권자 조회 공고</h2>
						
						<ul class="mt15">
					<li>
						<p class="strong line22">1. 저작재산권자를 찾는다는 취지</p>
						<textarea cols="10" rows="3" id="anucItem1" name="anucItem1" class="textarea w98" ></textarea>
					</li>
					<li class="mt15">
						<p class="strong line22">2. 저작재산권자의 성명 또는 명칭, 주소 또는 거소 등</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm">성명</label></th>
									<td><input type="text" id="anucItem2" title="성명" name="anucItem2" class="inputData" />
										<%-- <input type="hidden" name="worksId" id="worksId" value="${AnucBord.worksId}"/> --%>
									</td>
								</tr>
								<tr>
									<th scope="row"><label for="zip">주소</label></th>
									<td><input type="text" id="anucItem3" title="주소" name="anucItem3" class="inputData  w85" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="phone">연락처</label></th>
									<td><input type="text" id="anucItem4" title="연락처" name="anucItem4" class="inputData" /></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="strong line22">3. 저작물의 제호</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="tl">제호</label></th>
									<td>
									<input type="hidden"  id="genreCd" name="genreCd" value="" />
									<input type="text"  id="worksTitle" title="제호" name="worksTitle" value="" readonly="readonly" class="inputData w85" />
									</td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="strong line22">4. 공표 시 표시된 저작재산권자의 성명(실명 또는 이명)</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm2">성명</label></th>
									<td><input type="text" id="anucItem5" title="성명" name="anucItem5" class="inputData" /></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="strong line22">5. 저작물을 발행 또는 공표한자</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="date1">저작물발행</label></th>
									<td><input type="text" id="anucItem6" title="저작물발행" name="anucItem6" class="inputData" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="date2">공표연월일</label></th>
									<td><input type="text" id="anucItem7" title="공표연월일" name="anucItem7" maxlength="8" class="inputData" /> <span style="color:red">* ex ) 20120101(2012년 01월 01일)</span></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="strong line22">6. 저작물의 이용목적</p>
						<textarea cols="10" rows="3" name ="anucItem8" id="anucItem8" class="textarea w98" ></textarea>
					</li>
					<li class="mt15">
						<p class="strong line22">7.복제물의 표지사진 등의 자료</p>
						<div class="">
								<div class="fr mb5">
									<p>
										<span title="추가" class="button small"><button id="btnAdd" onkeypress="" onclick="javascript:fn_addRow1();" type="button">File 추가</button></span>
										<span title="삭제" class="button small"><button id="btnDel" onkeypress="" onclick="javascript:fn_delRow1();" type="button">File 삭제</button></span>
									</p>
								</div>
						</div>
						<div class="mb15">
						                                <table id="tblAttachFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부파일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
															<colgroup>
															    <col width="8%">
															    <col width="*%">
															</colgroup>
															<thead>
															    <tr>
						    									    <th scope="row" class="ce">순번</th>
						    									    <th scope="row">첨부파일</th>
															    </tr>
															</thead>
															<tbody>
															    <tr id="fileTr1">
															    	<td class="ce" id="fileTd1">
															    		<input name="chkDel1" id="chkDel1" type="checkbox" value="1" />
															    	</td>
															    	<td>
																    	<input type="hidden" name="fileDelYn" />
																		<span id="spfileD2'">
																		    <input type="file" name="file" id="file" title="첨부파일" 
																		        class="inputData L w100"
																		        onkeydown="return false;" onchange="checkFile(this.id)"/>
																		</span>
															    	</td>
															    </tr>
	                                           				</tbody>
														</table>
														<span class="p11 orange block mt5">※ 첨부파일은 최대5개까지 가능합니다.</span>
													</div>
					</li>
					<li class="mt15">
						<p class="strong line22">8. 공고자 및 연락처</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm3">공고자</label></th>
									<td>
									<input type="hidden" name="rgstIdnt" value="<%=sessUserIdnt%>"/>
									<input type="text"  name="anucItem9" title="공고자" class="inputData" value="<%=sessUserName%>"/></td>
								</tr>
								<tr>
									<th scope="row"><label for="zip3">주소</label></th>
									<td><input type="text"  name="anucItem10" title="주소" class="inputData w98" value="<%=sessUserAddr %>" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="phone3">연락처</label></th>
									<td><input type="text"  name="anucItem11" title="연락처" class="inputData" value="<%=sessMoblPhon%>" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="admin3">담당자</label></th>
									<td><input type="text" name="anucItem12" title="담당자" class="inputData" /></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt20">					
						<p class="strong line13">* 상당한 노력 신청 정보는 담당자에 의해 보완될 수 있습니다. (<input type='checkbox' id='chkAgr' class='mb3'/> 동의함)</p>
					</li>
					</ul>
					</form>
					<!-- 버튼 str -->	
						<div class="btnArea mt20">
							<p class="fl"><span class="button medium gray"><a href="javascript:self.close()">취소</a></span></p>
							
							<p class="fr"><span class="button medium"><a href="javascript:;" onclick="javascript:nonWriteSub()">신청</a></span></p>							
							
						</div>
	                              <!-- //버튼 -->
				</div>
				<!-- //주요컨텐츠 end -->
				
			</div>
	<!-- FOOTER str-->
			<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights reserved.</p>
			<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2011/button/pop_close.gif" alt="닫기" title="이 창을 닫습니다." /></a>
			<!-- //FOOTER end -->
</div>
			
	
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
<!-- 	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기 -->
 </script>
 
</body>
</html>

