<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 공고 - 보상금 공탁 공고 | 법정허락 | 권리자찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" >
<link rel="stylesheet" type="text/css" href="/css/table.css" >
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
function goPage(pageNo) {
	  var findForm = document.getElementById("findForm");
	  var searchStr = document.getElementById("title");
	  var bordCd = document.getElementById("bordCd");
	  findForm.page_no.value = pageNo;
	  findForm.target = "_self";  
	  if(trim(searchStr.value)=='' || searchStr.value.length==0)
	   findForm.action = "/statBord/statBo05List.do";
	  else
	   findForm.action = "/statBord/courtFind.do.do";
	  findForm.submit();
	 }
function fn_frameList(){
// 	var sel = document.getElementById("genre");		//장르 셀렉트박스
//  	var val = sel.options[sel.selectedIndex].value;	//셀렉트박스 값
 	var text = document.getElementById("title").value; //검색어 값
//  	document.getElementById("genreCd").value = val;
 	document.getElementById("tite").value = text;
 	document.getElementById("findForm").submit();
 	}
function keyCheck(key){
	if(event.keyCode == 13){
		fn_frameList();
	}
}	
function loginCheck(){
	var userId = '<%=sessUserIdnt%>';
	var bordCd = document.getElementById("bordCd").value;
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
	return;
	}else{
		window.open("/statBord/statBo05RegiPop.do", "small", "width=750, height=650, scrollbars=yes, menubar=no, location=yes");
	}
}



//-->
</script>
<script type="text/javascript"> 
/* (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-69621660-1', 'auto');
ga('send', 'pageview'); */
</script>
</head>
 
<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		
<script type="text/javascript"> 
$(function(){
	for(var i = 0 ; i < 10 ; i ++){
		//console.log($('#anucItem'+i).html())
		if($('#anucItem'+i).html()!=undefined){
			$('#anucItem'+i).html(wonAttach($('#anucItem'+i).html()));
		}
	}
})	

function wonAttach(text){
	//console.log(text)
		if(text.substr(text.indexOf(-1))!="원"){
			text+="원";
		}
		return text;
}
</script>
		<%-- <jsp:include page="/include/2012/subHeader3.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(3);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			
				<!-- 래프 -->
				<div class="con_lf">
					<h2><div class="con_lf_big_title">법정허락</div></h2>
					<ul class="sub_lf_menu">
						<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
						<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
						<!-- <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
						<li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
						<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
						<!-- <li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li> -->
						<!-- <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li> -->
						<li><a class="on" href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a>
							<ul class="sub_lf_menu2">
								<li><a href="/statBord/statBo03List.do?bordCd=3">승인 신청 공고</a>
								<li><a href="/statBord/statBo04List.do?bordCd=4">승인 공고</a></li>
								<li><a class="on" href="/statBord/statBo05List.do?bordCd=5">보상금 공탁 공고</a></li>
								<li><a href="/statBord/statBo08List.do?bordCd=8">보상금 지급사실 공고</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<!-- //래프 -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/right4me/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					
					<div class="con_rt_head">
						<img src="../images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						법정허락
						&gt;
						법정허락 공고
						&gt;
						<span class="bold">보상금 공탁 공고</span>
					</div>
					<h1><div class="con_rt_hd_title">법정허락 공고</div></h1>
					<div id="sub_contents_con" class="sub03_popup_posi">
						<ul class="sub_menu1 w269 mar_tp40">
							<li class="first"><a href="/statBord/statBo03List.do?bordCd=3" style="width: 200px;">승인 신청 공고</a></li>
							<li><a href="/statBord/statBo04List.do?bordCd=4" style="width: 200px;">승인 공고</a></li>
							<li class="on"><a href="/statBord/statBo05List.do?bordCd=5" class="last_rt_bor on" style="width: 200px;">보상금 공탁 공고</a></li>
							<li><a href="/statBord/statBo08List.do?bordCd=8" class="last_rt_bor" style="width: 200px;">보상금 지급사실 공고</a></li>
						</ul>
                   	  <p class="clear"></p>
                   	  
						<!-- 검색str -->
					<form method="get" action="/statBord/courtFind.do" id ="findForm">	
						<div class="mar_tp30 title_query">
							<div class="title_query_lf">
								<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;제목
									<input type="text" id="title" value="${title}" onkeydown="javascript:keyCheck(event)"style="margin-left:75px; width:70%"/>
								</label>
							</div>
							<div class="title_query_rt">
								<a href="#fn_frameList" onkeypress="javascript:fn_frameList();" onclick="javascript:fn_frameList();">
									<img src="/images/sub_img/sub_25.gif" alt="조회" />
								</a>
							</div>
							<p class="clear"></p>
						</div>
							<table class="fl schBoxGrid w70">
									
									<colgroup>
										<col width="25%">
										<col width="*">
									</colgroup>
									<tbody>
												<tr>     
<!-- 													<th scope="row"><label for="sch1">장르</label></th> -->
													<td scope="row"> 
													<input type="hidden" id = "page_no" name="page_no" value="1" />
													<input type="hidden" id = "tite" name = "tite" value="${title}" />
													<input type="hidden" id = "bordCd" name = "bordCd" value="${bordCd}" />

<!-- 														<select class="w50"  id="genre"> -->
<!-- 														<option value="0">전체</option> -->
<!-- 														<option value="1">어문</option> -->
<!-- 														<option value="2">음악</option> -->
<!-- 														<option value="3">연극</option> -->
<!-- 														<option value="4">미술</option> -->
<!-- 														<option value="5">건축</option> -->
<!-- 														<option value="6">사진</option> -->
<!-- 														<option value="7">영상</option> -->
<!-- 														<option value="8">도형</option> -->
<!-- 														<option value="9">컴퓨터프로그램</option> -->
<!-- 													</select> -->
													</td>
												</tr>
											</tbody>
								</table>
						</form>
						<!-- //검색 -->
						<br/>
						<!-- 그리드스타일 -->
						<table class="sub_tab3 mar_tp20" cellspacing="0" cellpadding="0" width="100%" summary="법정허락 보상금 공탁 공고 표로 번호, 제목, 공고일, 공탁금액으로 구성되어 있습니다.">
							<caption>법정허락 보상금 공탁 공고</caption>
							<colgroup>
							<col width="9%">
							<col width="*">
							<col width="15%">
							<col width="17%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col">번호</th>
<!-- 									<th scope="col">장르</th> -->
									<!-- <th scope="col" style="text-align: left">제목</th> -->
									<th scope="col">제목</th>
									<th scope="col">공고일</th>
									<th scope="col">공탁금액</th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${totalRow == 0}">
										<tr>
											<td class="ce" colspan="6">등록된 게시물이 없습니다.</td>
										</tr>
								</c:if>
								<c:if test="${totalRow > 0 }">
								<c:forEach var="AnucBord" items="${AnucBord}" varStatus="status">
									<tr>
										<td class="ce">${totalRow-AnucBord.rowNo+1}</td>
										<td style="text-align: left"><a href="/statBord/statBo01Detl.do?bordSeqn=${AnucBord.bordSeqn}&amp;bordCd=${AnucBord.bordCd}&amp;divsCd=1">${AnucBord.tite}</a></td>
										<td class="ce">${AnucBord.openDttm}</td>
										<td id="anucItem${status.index}" style="text-align: right;">${AnucBord.anucItem4}</td>
									</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
						<!-- //그리드스타일 -->
						
						<!-- 페이징 -->
							<div class="pagination">
							<ul>
								<li>
						<!--페이징 리스트 -->
						 	<jsp:include page="../common/PageList_2011.jsp" flush="true">
							  	<jsp:param name="totalItemCount" value="${totalRow}" />
								<jsp:param name="nowPage"        value="${AnucBordnum.nowPage}" />
								<jsp:param name="functionName"   value="goPage" />
								<jsp:param name="listScale"      value="" />
								<jsp:param name="pageScale"      value="" />
								<jsp:param name="flag"           value="M01_FRONT" />
								<jsp:param name="extend"         value="no" />
							</jsp:include>
								</li>	
							</ul>
						</div>
						<!-- //페이징 -->
						<div class="btnArea">
							<p class="rgt"><span class="button medium"><a href="javascript:;" onclick="javascript:loginCheck()">등록</a></span></p>
						</div>
										
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
			<p class="clear"></p>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
			<!-- 2017변경 -->
			<jsp:include page="/include/2017/footer.jsp" />
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	<!-- //전체를 감싸는 DIVISION -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

