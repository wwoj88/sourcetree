<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 찾기 검색 | 저작권자찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" >
<link rel="stylesheet" type="text/css" href="/css/table.css" >
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
$(window).load(function(){			//페이지의 로딩이 모두 끝난후 라디오박스 값셋팅
	var sucValue = document.getElementById("Suc").value;
	if(sucValue == 1){
		self.close();
		opener.parent.location.reload();
	}
	else if(sucValue == 2){ //다건신청 이후 창 닫기
		self.close();
	}
});
function goPages(pageNo) {
	  var findForm = document.getElementById("findForm");
	  var searchStr = document.getElementById("worksTitle");
	  findForm.page_no.value = pageNo;
	  
	  findForm.target = "_self";  
	  if(trim(searchStr.value)=='' || searchStr.value.length==0)
	   findForm.action = "/statBord/statBo06List.do";
	   else
	   findForm.action = "/statBord/statBo06Find.do";
	  findForm.submit();
	 }	 

//달력
function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}
//날짜체크 
function checkValDate(){
	var f = document.findForm;
	if(f.joinDay.value!='' && f.endDay.value!=''){
		if(parseInt(f.joinDay.value,10)>parseInt(f.endDay.value,10)){
			alert('만료일이 시작일보다 이전입니다.\n일자를 다시 확인하십시요');
			f.endDay.value='';
		}
	}
}
function statBoRegiPop(){
	window.open("/statBord/statBo06Regi.do", "small", "width=750, height=650, scrollbars=yes, menubar=no, location=yes");	
}

function findFormSubmit(){
	var f = document.findForm;
	if(f.joinDay.value =='' || f.endDay.value == ''){
		alert("신청일자를 입력해주세요.");
		f.action = "/statBord/statBo06List.do";
	}else if(f.joinDay.value!='' && f.endDay.value!=''){
		f.action = "/statBord/statBo06Find.do";
		f.submit();
	}
}
function keyCheck(key){
	if(event.keyCode == 13){
		findFormSubmit();
	}
}

//법정허락 신청
function fn_goStep(){
	var ch = document.getElementsByName("checkPrps");
	var checkArray = new Array();
	var count=0;
	for(var i=0; i<ch.length; i++){
		if(ch[i].checked == true){
			count++;
			var checkNum="";
			checkNum += ch[i].value;
			checkArray.push(checkNum);
		}
	}
	var hddnFrm = document.findForm;
	hddnFrm.action = "/stat/statPrpsMain.do";
		document.getElementById("worksId").value= checkArray.join();
		hddnFrm.target = "_self";
		hddnFrm.method = "post";
		hddnFrm.submit();
}


//-->
</script>
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-1', 'auto');
  ga('send', 'pageview');
</script>
</head>
 
<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="contents">
			
				<!-- 래프 -->																														
				<div class="con_lf" style="width: 22%">
					<div class="con_lf_big_title">마이페이지</div>
					<ul class="sub_lf_menu">
						<li><a href="/statBord/statBo06List.do?bordCd=6" class="on">신청현황</a>
							<ul class="sub_lf_menu2">
								<!-- <li><a href="/statBord/statBo06List.do?bordCd=6" class="on">저작권자 찾기위한 상당한 노력<br/>신청</a></li>-->
								<li><a href="/myStat/statRsltInqrList.do">법정허락 승인신청</a></li>
								<li><a href="/statBord/statBo01ListMy.do?bordCd=1">저작권자 조회 공고</a></li>
								<li><a href="/statBord/statBo05ListMy.do?bordCd=5">보상금 공탁 공고</a></li>
							</ul>
						</li>
						<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보</a>
							<ul class="sub_lf_menu2 disnone">
								<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보 수정</a></li>
								<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=D&amp;userIdnt=<%=sessUserIdnt%>">회원탈퇴</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<!-- //래프 -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/right4me/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						마이페이지
						&gt;
						신청현황
						&gt;
						<span class="bold">저작권자 찾기위한 상당한 노력 신청</span>
					</div>
					<div class="con_rt_hd_title">저작권자 찾기위한 상당한 노력 신청</div>
					<br/><br/><br/>
					<div class="section">
						
						<form action="#" id="findForm" name="findForm" method="get">
							<fieldset class="w100" style="width: 815px;">
							<legend></legend>
								<div class="boxStyle">
									<div class="box1 floatDiv">
										<p class="fl mt5 w15"><img title="Search" alt="Search" src="/images/content/sch_txt.gif"></p>
										<table summary="" class="fl schBoxGrid w70">
										<caption></caption>
											<colgroup><col width="15%"><col width="30%"><col width="15%"><col width="*"></colgroup>
											<tbody>
											<tr>
													<th scope="row"><label for="app1">장르</label></th>
													<td><select id="genreCd" name="genreCd" class="inputData" title="장르">
													<c:if test="${!empty genreCd}">
													<option value="0">--전체--</option>
													<c:forEach items="${codeList}" var="codeList">
														<c:if test="${genreCd == codeList.midCode}">
															<option selected="selected" value="${codeList.midCode}">${codeList.codeName}</option>
														</c:if>
														<c:if test="${genreCd!= codeList.midCode}">
															<option value="${codeList.midCode}">${codeList.codeName}</option>
														</c:if>
													</c:forEach>
													</c:if>
													<c:if test="${empty genreCd}">
													<option value="0">--전체--</option>
													<c:forEach items="${codeList}" var="codeList">
														<option value="${codeList.midCode}">${codeList.codeName}</option>
													</c:forEach>
													</c:if>
													</select>
													</td>
													<th scope="row"><label for="app2">신청일자</label></th>
													<td>
												
													<input type="text" id="joinDay" title="검색날짜" name="joinDay" style="width:50px" value="${defaultJoinDay}" onblur='javascript:checkValDate();'> 
													<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('findForm','joinDay');" class="vmid" alt="신청일자 시작일시 선택버튼" title="신청일자 시작일시를 선택하세요."/> 
													~
													<input type="text" id="endDay" title="검색날짜" name="endDay" value="${defaultEndDay}" onblur='javascript:checkValDate();' style="width:50px">
													<img src="/images/2012/common/calendar.gif" class="vmid" onclick="javascript:fn_cal('findForm','endDay');" alt="신청일자 종료일시 선택버튼" title="마지막 날짜를 선택하세요." /></td>
												</tr>
												<tr>
													<th scope="row"><label for="app3">진행상태</label></th>
													<td><select id="statCd" name="statCd" class="inputData w80" title="진행상태">
													<c:if test="${!empty statCd}">
													<option value="0">--전체--</option>
													<c:forEach items="${gubunList}" var="gubunList">
														<c:if test="${statCd == gubunList.midCode}">
															<option selected="selected" value="${gubunList.midCode}">${gubunList.codeName}</option>
														</c:if>
														<c:if test="${statCd != gubunList.midCode}">
															<option value="${gubunList.midCode}">${gubunList.codeName}</option>
														</c:if>
													</c:forEach>
													</c:if>
													<c:if test="${empty statCd}">
													<option value="0">--전체--</option>
													<c:forEach items="${gubunList}" var="gubunList">
														<option value="${gubunList.midCode}">${gubunList.codeName}</option>
													</c:forEach>
													</c:if>
													</select>
													</td>
													<th scope="row"><label for="app4">제목</label></th>
													<td><input type="text" title="제목" id="worksTitle" name="worksTitle" value="${title}" class="w80" onkeydown="javascript:keyCheck(event)">
													<input type="hidden" id="page_no" name="page_no" value="1" />
	
													<input type="hidden" id="bordCd" name="bordCd" value="6"/>
													<input type="hidden" id="Suc" name="Suc" value="${Success}"/>
													</td>
												</tr>
											</tbody>
										</table>
									
										<p class="fl btn_area pt25"><a href="javascript:;" onclick="javascript:findFormSubmit()"><input type="image" title="검색" alt="저작권자 찾기 위한 상당한 노력 신청 조회버튼" src="/images/2012/button/sch.gif"></a></p>
									</div>
									
									<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
								</div>
							</fieldset><br/><br/><br/>
					
						<p class="rgt mt20 mb5">
							<a href="javascript:;" onclick="statBoRegiPop()"><img title="상당한 노력신청(거소불명)" alt="상당한 노력신청(거소불명)" src="/images/2012/button/btn_app11.gif"></a>
							<a href="javascript:;" onclick="fn_goStep()"><img title="선택저작물  이용승인신청 " alt="선택저작물  이용승인신청 " src="/images/2012/button/btn_app13.gif"></a>
						</p>
						<!-- 그리드스타일 -->
						<table cellspacing="0" cellpadding="0" border="1" summary="저작권자를 찾기위한 상당한 노력 신청 리스트" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<caption>저작권자를 찾기위한 상당한 노력 신청 리스트</caption>
							<colgroup>
							<col width="6%">
							<col width="15%">
							<col width="*">
							<col width="15%">
							<col width="17%">
							<col width="5%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col">선택</th>
									<th scope="col">구분</th>
									<th scope="col">제목</th>
									<th scope="col">신청일자</th>
									<th scope="col">진행상태</th>
									<th scope="col">상당한 노력 진행상태</th>
									
								</tr>
							</thead>
							<c:if test="${totalRow == 0}">
										<tr>
											<td class="ce" colspan="6">등록된 게시물이 없습니다.</td>
										</tr>
								</c:if>
							<tbody>
						        <c:if test="${totalRow > 0 }">
							<c:forEach var="AnucBord" items="${AnucBord}">
							
									<tr>
									<!-- 법정허락 완료 -->
									<%-- 	<td>${AnucBord.systEffortStatCd}</td>
										<td>${AnucBord.statWorksYn}</td> --%>
										
										<c:if test="${AnucBord.systEffortStatCd == 4 && AnucBord.statWorksYn =='Y'}">
										 <td class="ce"><input title="Checkbox" type="checkbox" id="check${AnucBord.worksId}" name="checkPrps" value="${AnucBord.worksId}" />
										<input type="hidden" id="worksId" name="worksId" />
										</td>
										<%-- <td class="ce"><input title="Checkbox" type="checkbox" id="check${AnucBord.worksId}" name="checkPrps" value="${AnucBord.worksId}" />
										<input type="hidden" id="worksId" name="worksId" />
										</td>
									 --%>
									 <td class="ce"><input title="Checkbox" type="checkbox" id="check${AnucBord.worksId}" name="checkPrps" value="${AnucBord.worksId}" />
										<input type="hidden" id="worksId" name="worksId" />
										</td>
										</c:if>
										<c:if test="${!(AnucBord.systEffortStatCd == 4 && AnucBord.statWorksYn =='Y')}">
										<td class="ce"></td>
										</c:if>
										<td class="ce">${AnucBord.genreCdName}</td>
										<td align="left"><a href="/statBord/statBo06Detl.do?worksId=${AnucBord.worksId}">${AnucBord.worksTitle }</a></td>
										<td class="ce">${AnucBord.rgstDttm}</td>
										<td class="ce">${AnucBord.statCdName}<c:if test="${AnucBord.statCd==3}">(${AnucBord.statRsltCdName})</c:if></td>
										<td class="ce">
										<c:if test="${AnucBord.statRsltCd == 2}">상당한 노력 미진행</c:if><c:if test="${AnucBord.statRsltCd != 2}">${AnucBord.systEffortStatCdName}</c:if>
										</td>
									</tr>
									</c:forEach>
									</c:if>
							</tbody>
						</table>
						</form>
						<!-- //그리드스타일 -->
						
							  <!-- 페이징 시작 -->
						<div class="pagination">
							<ul>
								<li>
						<!--페이징 리스트 -->
						 	<jsp:include page="../common/PageList_2011.jsp" flush="true">
							  	<jsp:param name="totalItemCount" value="${totalRow}" />
								<jsp:param name="nowPage"        value="${AnucBordnum.nowPage}" />
								<jsp:param name="functionName"   value="goPages" />
								<jsp:param name="listScale"      value="" />
								<jsp:param name="pageScale"      value="" />
								<jsp:param name="flag"           value="M01_FRONT" />
								<jsp:param name="extend"         value="no" />
							</jsp:include>
								</li>	
							</ul>
						</div>
							<!-- //페이징 -->

					</div>
						
				</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
			</div>
		<!-- //CONTAINER end -->		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

