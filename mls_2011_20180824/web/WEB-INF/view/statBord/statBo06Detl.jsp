<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>


<%
	User user = SessionUtil.getSession(request);
	
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	pageContext.setAttribute("UserName", sessUserName); 

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 찾기 검색 | 저작권자찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
$(window).load(function(){			//페이지의 로딩이 모두 끝난후 라디오박스 값셋팅
	var radioValue = document.getElementById("rgstReasCd").value;
	var radio = document.getElementsByName("rgstReas");
	if(radioValue != 0){
		for(var i =0; i<=radio.length; i++){
			if(i == radioValue){
				radio[i-1].checked = true;
			}
		}
	}
	
	var anucItem8 = document.getElementById("anucItem8").value;
	var anuc8Cg = anucItem8.replace(/\r\n/g, "<br />");
	jQuery('#anuc8').append(anuc8Cg);
	var anucItem1 = document.getElementById("anucItem1").value;
	var anuc1Cg = anucItem1.replace(/\r\n/g, "<br />");
	jQuery('#anuc1').append(anuc1Cg);
	SuplCheck();
	tblSuplSort();
// alert(jQuery("#").value());

// 	var n= jQuery("input[type='text']").val();
// 	n.replace(/\r\n/g, "<br />");
// 	alert(n.replace(/\r\n/g, "<br />"));
});

function fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.getElementById("form1");
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.target="boardView";
	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
}

function go_steps(){
	var frm = document.form2;
	frm.submit();
}

function crosPop(){
	window.open("http://www.cros.or.kr/report/search.cc", "small", "width=750, height=650, scrollbars=yes, menubar=no, location=yes");
}


function tblSuplSort(){ // 보완목록에서 같은 차수일 경우 셀 합치기
	
	var tblSupl = document.getElementById("tblSupl");
	if(tblSupl==null){	return;
	}else{		
		startRow = 2; //검사 시작 row index
		var cNum = new Array(0, 3, 4);// cNum = 병합할 컬럼 번호
		
		var rows = tblSupl.rows;
		rowNum = rows.length; // 전체 줄 수
		tempVal = '';
		cnt = 0;		
		for( i = startRow; i < rowNum; i++ ) { 			
			var curVal = rows[i].cells[cNum[0]].innerHTML;		
			if( curVal == tempVal ) {
				if(cnt == 0) {
					cnt++;
					startRow = i - 1;
				}
				cnt++;
			}else if(cnt > 0) {				
				merge(tblSupl, startRow, cnt, cNum);				
				startRow = endRow = 0;
				cnt = 0;
			}else {
			}
				tempVal = curVal;		
		}
		
		if(cnt > 0) {			
		
			merge(tblSupl, startRow, cnt, cNum);
									
		}		
		
	}
		
}

function merge(tbl, startRow, cnt, cNum)
{
	rows = tbl.rows;
	row  = rows[startRow];
	
	for( i = startRow + 1; i < startRow + cnt; i++ ) {
		for( j = 0; j < cNum.size(); j++ ){
			rows[i].deleteCell(cNum[j]-j);
			//0번셀이 지워지면 뒤에있던 1번셀이 0번이 된다. 그래서 지워준 셀 수 만큼 인덱스 번호에서 빼준다.
		}
	}
	
	for( j = 0; j < cNum.size(); j++ ){
		row.cells[cNum[j]].rowSpan = cnt;	
	}
}

function SuplCheck(){ // 보완처리된 데이터에 * 표시 하기	

	<c:forEach items="${SuplList}" var="SuplList">		
		if(${SuplList.suplItemCd}=='10' && jQuery('#anuc1').text().substr(jQuery('#anuc1').text().length-3)!="(*)")	{			 
			jQuery('#anuc1').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='21' && jQuery('#anuc2').text().substr(jQuery('#anuc2').text().length-3)!="(*)")	{			 
			jQuery('#anuc2').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='22' && jQuery('#anuc3').text().substr(jQuery('#anuc3').text().length-3)!="(*)")		{			 
			jQuery('#anuc3').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='23' && jQuery('#anuc4').text().substr(jQuery('#anuc4').text().length-3)!="(*)")		{			 
			jQuery('#anuc4').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='31' && jQuery('#genreCdName').text().substr(jQuery('#genreCdName').text().length-3)!="(*)")		{			 
			jQuery('#genreCdName').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='32' && jQuery('#worksTitle').text().substr(jQuery('#worksTitle').text().length-3)!="(*)")		{			 
			jQuery('#worksTitle').append("\t\t (*)");
		}		
		else if(${SuplList.suplItemCd}=='40' && jQuery('#anuc5').text().substr(jQuery('#anuc5').text().length-3)!="(*)")		{			 
			jQuery('#anuc5').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='51' && jQuery('#anuc6').text().substr(jQuery('#anuc6').text().length-3)!="(*)")		{			 
			jQuery('#anuc6').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='52' && jQuery('#anuc7').text().substr(jQuery('#anuc7').text().length-3)!="(*)")		{			 
			jQuery('#anuc7').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='60' && jQuery('#anuc8').text().substr(jQuery('#anuc8').text().length-3)!="(*)")		{			 
			jQuery('#anuc8').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='81' && jQuery('#anuc9').text().substr(jQuery('#anuc9').text().length-3)!="(*)")		{			 
			jQuery('#anuc9').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='82' && jQuery('#anuc10').text().substr(jQuery('#anuc10').text().length-3)!="(*)")		{			 
			jQuery('#anuc10').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='83' && jQuery('#anuc11').text().substr(jQuery('#anuc11').text().length-3)!="(*)")		{			 
			jQuery('#anuc11').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='84' && jQuery('#anuc12').text().substr(jQuery('#anuc12').text().length-3)!="(*)")		{			 
			jQuery('#anuc12').append("\t\t (*)");
		}	
	</c:forEach>
}

//출력물 
var mywindow;
function fn_report(worksId, report){
	/* var frm = document.frm;
	var sUrl = "/statBord/statBo06Detl.do";

	var param = "?worksId="+worksId;
	     param += "&mode=R";
	     param += "&report="+report;
	 
	sUrl += param;
	window.open(sUrl,"PRINT","toolbar=no,menubar=no,resizable=no,status=yes"); */
	
	//alert(report);
	mywindow = window.open('', 'my div', 'height=400,width=600');
	mywindow.document.write('<html><head><title>저작권찾기 상당한노력 공고 정보</title>
');
	mywindow.document.write("<link type='text/css' rel='stylesheet' href=''/css/2012/common.css'><link type='text/css' rel='stylesheet' href='/css/2012/common_main.css'>");
	mywindow.document.write("<link type='text/css' rel='stylesheet' href='/css/2012/style.css'><link rel='stylesheet' type='text/css' href=''/css/sub.css' />");
	mywindow.document.write("<link rel='stylesheet' type='text/css' href='/css/table.css' /><link rel='stylesheet' type='text/css' href='/css/2012/calendar.css'>");
	mywindow.document.write('</head><body >');
	mywindow.document.write($('#printContainer').html());
	mywindow.document.write('</body></html>');
	mywindow.document.close(); // IE >= 10에 필요
	mywindow.focus(); // necessary for IE >= 10
	setTimeout("printCall()",1000);
}
function printCall(){
	mywindow.print()
	mywindow.close();
}


//-->
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-1', 'auto');
  ga('send', 'pageview');
</script>
</head>
 
<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			
				<!-- 래프 -->
				<div class="con_lf" style="width: 22%">
					<div class="con_lf_big_title">마이페이지</div>
					<ul class="sub_lf_menu">
						<li><a href="/statBord/statBo06List.do?bordCd=6" class="on">신청현황</a>
							<ul class="sub_lf_menu2">
								<li><a href="/statBord/statBo06List.do?bordCd=6" class="on">저작권자 찾기위한 상당한 노력<br/>신청</a></li>
								<li><a href="/myStat/statRsltInqrList.do">법정허락 승인신청</a></li>
								<li><a href="/statBord/statBo01ListMy.do?bordCd=1">저작권자 조회 공고</a></li>
								<li><a href="/statBord/statBo05ListMy.do?bordCd=5">보상금 공탁 공고</a></li>
							</ul>
						</li>
						<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보</a>
							<ul class="sub_lf_menu2 disnone">
								<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보 수정</a></li>
								<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=D&amp;userIdnt=<%=sessUserIdnt%>">회원탈퇴</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<!-- //래프 -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/right4me/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						마이페이지
						&gt;
						신청현황
						&gt;
						<span class="bold">저작권자 찾기위한 상당한 노력 신청</span>
					</div>
					<div class="con_rt_hd_title">저작권자 찾기위한 상당한 노력 신청</div>
					<br/>
				<div class="section">
					<div class="floatDiv mt20">
						<h2>저작권자 찾기위한 상당한 노력 신청 사유</h2>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청 사유" name="rgstReas" id="radioOne" disabled="disabled"	value="1"><span> 찾고자하는 저작물이 검색결과에 없는 경우</span><br/>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청 사유" name="rgstReas" id="radioTwe" disabled="disabled" value="2"><span>검색결과 제호는 동일 하나 찾고자 하는 저작물이 아닌 경우</span><br/>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청 사유" name="rgstReas" id="radioThree" disabled="disabled" value="3"> <span>검색결과 저작물은 존재하나 저작권자 정보가 불일치 하는 경우</span><br/>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청 사유" name="rgstReas" id="radioFore" disabled="disabled" value="4"> <span>찾고자하는 저작물은 존재하나 인접권자 정보가 불일치 하는 경우</span><br/>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청 사유" name="rgstReas" id="radioFive" disabled="disabled" value="5"> <span>찾고자하는 저작물은 존재하나 관련기관 확인 후 찾고자 하는 저작물이 아닌경우</span><br/>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청 사유" name="rgstReas" id="radioSix"  disabled="disabled" value="6"> <span>기타</span><br/>&nbsp;&nbsp;&nbsp;
							<c:if test="${AnucBord.rgstReasCd == 6}">
							<textarea cols="100" name="rgstReasEtc" id="rgstasEtc" disabled="disabled">${AnucBord.rgstReasEtc}</textarea><br/>
							</c:if>
							<input type="hidden" name="rgstReasCd" id="rgstReasCd" value="${AnucBord.rgstReasCd}"/>
							
							<br/>
							<h2 class="fl mt10">신청 저작물 정보</h2>
							<c:if test="${AnucBord.systEffortStatCd==3}">
							<p class="fr"><a href="#"><img title="저작물  이용승인신청 " alt="저작물  이용승인신청 " src="/images/2012/button/btn_app14.gif"></a></p>
							</c:if>
						</div>
						<!-- 그리드스타일 -->
						<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="10%">
							<col width="*">
							<col width="25%">
							<col width="15%">
							<col width="15%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col">구분</th>
									<th scope="col">제호</th>
									<th scope="col">저작자</th>
									<th scope="col">진행상태</th>
									<th scope="col">상당한노력 진행상태</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="ce">${AnucBord.genreCdName}</td>
									<td class="ce">${AnucBord.worksTitle}</td>
									
									<td style="word-break:break-all" align="center">
									<c:if test="${!empty coptHodr}">
									<c:forEach items="${coptHodr}" var="coptHodr">
										${coptHodr.coptHodrRoleCdName} : ${coptHodr.coptHodrName}<br/>
									</c:forEach>
									</c:if>
									<c:if test="${empty coptHodr}">
										<p class="blue2 line22">등록된 정보가 없습니다.</p>
									</c:if>
									</td>
									<td class="ce">${AnucBord.statCdName}<c:if test="${AnucBord.statCd==3}">(${AnucBord.statRsltCdName})</c:if></td>
									<td class="ce"><c:if test="${AnucBord.statRsltCd == 2}">상당한 노력 미진행</c:if><c:if test="${AnucBord.statRsltCd != 2}">${AnucBord.systEffortStatCdName}</c:if></td>
								</tr>
							</tbody>
						</table>
						<!-- //그리드스타일 -->
						<c:if test="${AnucBord.systEffortStatCd == 4 && AnucBord.statRsltCd != 2}">
							<h2 class="mt20">상당한 노력 진행 결과</h2>
						<c:if test="${AnucBord.statWorksYn == 'Y'}">
							<span>신청 저작물의 저작권자 정보의 조회결과가 없습니다. 법정허락 이용승인 신청이 가능합니다.</span><span style="float:right" class="button medium"><a href="javascript:;" onclick="go_steps()">이용승인 신청</a></span>
							<form action="/stat/statPrpsMain.do" method="post" name="form2">
							<input type="hidden" name="worksId" value="${AnucBord.worksId}"/>
							</form>
						</c:if>
						<c:if test="${AnucBord.statWorksYn == 'N'}">
						<span>신청 저작물에 대해 아래와 같이 저작권자 정보가 조회되었습니다. <br/>아래 저작물의 이용은 저작권등록부조회 또는 해당 권리관리기관에 문의하세요.</span>
							<!-- 위탁관리 -->
							<c:if test="${!empty comm}">
							<div class="h2_box floatDiv mt10" id="commDiv">
								<h2 class="fl">위탁관리저작물(<c:if test="${!empty commSize}">${commSize}</c:if><c:if test="${empty commSize}">0</c:if>건)</h2>
								<c:if test="${commSize>4}">
									<c:if test="${more == 'N'}">
								<a href="javascript:board_more('CM')" class="fr more">더보기</a>
									</c:if>
								</c:if>
							</div>
							
							<table border="1" cellspacing="0" cellpadding="0" summary="." class="grid">
								<colgroup>
								<col width="10%">
								<col>
								</colgroup>
								<tbody>
								
									<c:forEach var="comm" items="${comm}">
									<!-- 음악 -->
									<c:if test="${comm.genreCd == 1}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">앨범명 : ${comm.albumTitle}</span>
												<span>앨범발매년도 : ${comm.albumIssuYear}</span>
												<span class="w60">작사가 : ${comm.lyrc}</span>
												<span>작곡가 : ${comm.comp}</span>
												<span class="w60">편곡가 : ${comm.arra}</span>
												<span>역사가 : ${comm.tran}</span>
												<span class="w60">음반제작자 : ${comm.prod}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 어문 -->
									<c:if test="${comm.genreCd == 2}">									
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">부제 : ${comm.worksSubTitle}</span>
												<span>창작년도 : ${comm.crtYear}</span>
												<span class="w95">도서명 : ${comm.bookTitle}</span>
												<span>출판사 : ${comm.bookPublisher}출판</span>
												<span>발행년도 : ${comm.bookIssuYear}</span>
												<span class="w60">저작자 : ${comm.coptName}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 방송대본 -->
									<c:if test="${comm.genreCd == 3}">				
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">작품명(대제목) : ${comm.worksTitle}</span>
												<span>장르/소재 : ${comm.scrtGenreCd} / ${comm.scrpSubjCd},${comm.scrpSubjCd2}</span>
												<span class="w60">원작명 : ${comm.worksOrigTitle}</span>
												<span>원작작가 : ${comm.origWriter}</span>
												<span class="w60">방송일자/회차 : ${comm.bordDate} / ${comm.brodOrdSeq}</span>
												<span>주요출연진 : ${comm.player}</span>
												<span class="w60">제작사 : ${comm.makeCpy}제작</span>
												<span>작가(저자) : ${comm.writer}</span>
												<span class="w60">연출자 : ${comm.director}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 영화 -->
									<c:if test="${comm.genreCd == 4}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">제작년도 : ${comm.crtYear}</span>
												<span>출연자 : ${comm.player}</span>
												<span class="w95">감독 : ${comm.direct}</span>
												<span>작가 : ${comm.writer}</span>
												<span>연출자 : ${comm.director}</span>
												<span>제작자 : ${comm.producer}제작</span>
												<span>배급사 : ${comm.distributor}배급</span>
												<span class="w60">투자자 : ${comm.investor}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 방송 -->
									<c:if test="${comm.genreCd == 5}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">장르 : ${comm.bordGenreCd}</span>
												<span>제작년도 : ${comm.makeYear}</span>
												<span class="w95">회차 : ${comm.bordOrdSeq}</span>
												<span>연출자 : ${comm.director}</span>
												<span class="w60">작가 : ${comm.writer}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 뉴스 -->
									<c:if test="${comm.genreCd == 6}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">기사일자 : ${comm.articlDate}</span>
												<span>지면번호/지면면종 : ${comm.papeNo} / ${comm.papeKind}</span>
												<span class="w60">기고자 : ${comm.contributor}</span>
												<span>기자 : ${comm.repoter}</span>
												<span class="w60">언론사 : ${comm.provider}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 미술 -->
									<c:if test="${comm.genreCd == 7}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
											<B>${comm.worksTitle}</B><br/>
												<span class="w60">부제 : ${comm.worksSubTitle}</span>
												<span>분류 : ${comm.kind}</span>
												<span class="w60">저작년월일 : ${comm.makeDate}</span>
												<span>출처 : ${comm.sourceInfo}</span>
												<span class="w60">주재료 : ${comm.mainMtrl}</span>
												<span>구조 및 특징 : ${comm.txtr}</span>
												<span class="w60">작가 : ${comm.writer}</span>
												<span >소장기관명 : ${comm.possOrgnName}</span>
												<span class="w60">소장년월일 : ${comm.possDate}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									<!-- 기타 -->
									<c:if test="${comm.genreCd == 99}">
									<tr>
										<th scope="row" class="vtop">${comm.genreCdName}</th>
										<td class="liceSrch">
										<B>${comm.worksTitle}</B><br/>
												<span class="w60">저작물 종류 : ${comm.sideGenreCdName}</span>
												<span>창작년도 : ${comm.makeDate}</span>
												<span class="w60">공표매체 : ${comm.publMedi}</span>
												<span>공표일자 : ${comm.publDate}</span>
												<span class="w60">저작자 : ${comm.coptName}</span>
												<span>권리관리기관 : ${comm.trstOrgnCodeName}</span>
										</td>
									</tr>
									</c:if>
									</c:forEach>
								</tbody>
							</table>
								</c:if>
							</c:if>
						
						<c:if test="${!empty conf}">
							<div class="h2_box floatDiv mt10" id="crosDiv">
								<h2 class="fl">법정허락 대상저작물(<c:if test="${!empty confSize}">${confSize}</c:if><c:if test="${empty confSize}">0</c:if>건)</h2>
							</div>
							<table border="1" cellspacing="0" cellpadding="0" summary="." class="grid">
								<colgroup>
								<col width="10%">
								<col>
								<col>
								</colgroup>
								<tbody>
									<c:forEach var="conf" items="${conf}">
									<tr>
										<th scope="row" class="vtop">${conf.genreCdName}</th>
										<td class="liceSrch">
											<B>${conf.worksTitle}</B><br/>
											<span class="w60">형태 및 수량 : ${conf.worksForm}</span>
											<span class="w95">저작재산권자 : ${conf.coptHodrName}</span>
											<span class="w95">승인일 : ${conf.confDate}</span>
										</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
						
						<c:if test="${!empty cros}">
						<div class="h2_box floatDiv mt10" id="crosDiv">
								<h2 class="fl">저작권등록부(<c:if test="${!empty crosSize}">${crosSize}</c:if><c:if test="${empty crosSize}">0</c:if>건)</h2><a href="javascript:;" onclick="crosPop()" class="fr more">저작권 등록 정보 조회 바로가기</a>
								<c:if test="${crosSize>4}">
									<c:if test="${more == 'N'}">
								<a href="javascript:board_more('CO')" class="fr more">더보기</a>
									</c:if>
								</c:if>
						</div>
							<table border="1" cellspacing="0" cellpadding="0" summary="." class="grid">
								<colgroup>
								<col width="10%">
								<col>
								<col>
								</colgroup>
								<tbody>
									<c:forEach var="cros" items="${cros}">
									<tr>
										<th scope="row" class="vtop">${cros.contClassNameCd}</th>
										<td class="liceSrch">
											<B>${cros.contTitle}</B><br/>
												<span class="w95">등록번호 : ${cros.regId}</span>
												<span>등록일자 : ${cros.regDate}</span>
												<span class="w95">등록부문 : ${cros.regPart1Name}(${cros.regPart2Name})</span>
												<span>저작자 : ${cros.authorName}</span>
												<span class="w95">등록원인 : ${cros.regReason}</span>
												<span class="w95">등록권리자 : ${cros.regCoptHodrName}</span>
										</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
						</c:if>
						
						<br/>
						<div id="printContainer">
						<h2 class="mt20">권리자찾기 상당한노력 공고 정보</h2>
						<span class="topLine"></span>
						<form name="form1" id="form1" method="post">
							
							<input type="hidden" name="filePath">
							<input type="hidden" name="fileName">
							<input type="hidden" name="realFileName">
						
						<table border="1" cellspacing="0" cellpadding="0" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->							
							<tbody>
								<tr>
									<th scope="row" class="p12">&lt;${AnucBord.worksTitle}&gt;의 저작권자 조회 공고</th>
								</tr>

								<tr>
									<td>
										<p class="mt15">&lt;${AnucBord.worksTitle}&gt;의 저작권자 조회 공고 </p>
										<ul class="mt15">
									
											<li class="mt15">																							
												<p class="blue2 line22">1. 저작권자를 찾는다는 취지</p>
												<p class="mt5" id="anuc1">- <input type='hidden' id="anucItem1" value="${AnucBord.anucItem1}"/></p><br/>
												<p class="blue2 line22">2. 저작재산권자의 성명 또는 명칭, 주소 또는 거소 등</p>
												<p class="mt5" id="anuc2">- 성명 : ${AnucBord.anucItem2}</p><p id="anuc3"> - 주소 : ${AnucBord.anucItem3}</p><p id="anuc4">- 연락처 :${AnucBord.anucItem4}</p><br/>
												<p class="blue2 line22">3. 저작물의 제호</p>
												<p class="mt5" id="genreCdName">- 장르 : ${AnucBord.genreCdName}</p><p id="worksTitle"> - 제호 : ${AnucBord.worksTitle}</p><br/>
												<p class="blue2 line22">4. 공표 시 표시된 저작재산권자의 성명(실명 또는 이명)</p>
												<p class="mt5" id="anuc5">- 성명 : ${AnucBord.anucItem5}</p><br/>
												<p class="blue2 line22">5. 저작물을 발행 또는 공표한자</p>
												<p class="mt5" id="anuc6">- 저작물발행 : ${AnucBord.anucItem6}</p><p id="anuc7"> - 공표연월일 : ${AnucBord.anucItem7}</p><br/>
												<p class="blue2 line22">6. 저작물의 이용 목적</p>
												<p class="mt5" id="anuc8">- <input type='hidden' id="anucItem8" value="${AnucBord.anucItem8}"/></p><br/>
												<p class="blue2 line22">7. 복제물의 표지사진 등의 자료</p>
												<table id="tblAttachFile" border="1" width="200" cellspacing="0" summary="첨부파일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
															<colgroup>
															    <col width="33%">
															</colgroup>
															<tbody>
															    <tr>
						    									    <th scope="row" style="text-align:center;">첨부파일명</th>
															    </tr>
															    <c:if test="${!empty fileList }">
													    			<c:forEach items="${fileList}" var="fileList">
																	    <tr>
																	    	<td>
															    				<a href="#1" onclick="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
															    					onkeypress="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
																    				${fileList.fileName }
																    				<input name="attcSeqn" id="attcSeqn_D1" value="${fileList.attcSeqn }" type="hidden" />
															    				</a>
																	    	</td>
																	    </tr>
													    			</c:forEach>
													    		</c:if>
													    		<c:if test="${empty fileList }">
													    			<tr>
													    				<td>
													    				<p class="blue2 line22">등록된 파일이 없습니다.</p>
													    				</td>
													    			</tr>
													    		</c:if>
															</tbody>
														</table>	
												<p class="mt5 line22"> <br /></p>
												<p class="blue2 line22">8. 공고자 및 연락처</p>
												<p class="mt5" id="anuc9">- 공고자 : ${AnucBord.anucItem9}</p><p id="anuc10">- 주소 : ${AnucBord.anucItem10 } </p> <p id="anuc11">- 연락처 : ${AnucBord.anucItem11}</p><p id="anuc12">- 담당자 : ${AnucBord.anucItem12}</p>
												
											</li>
											
										</ul>
									</td>
								</tr>
							</tbody>
						</table>
						<!-- //그리드스타일 -->
						</form>
						</div>
						<!-- 버튼 str -->
						<div class="btnArea">
							<p class="fl"><span class="button medium gray"><a href="/statBord/statBo06List.do?bordCd=6">목록</a></span></p>
							<p class="fr">
							<c:if test="${AnucBord.statCd == 1}">
							<span class="button medium gray"><a href="/statBord/statBo06Update.do?worksId=${AnucBord.worksId}">수정</a></span> <span class="button medium"><a href="/statBord/statBo06Delete.do?worksId=${AnucBord.worksId}&bordCd=0&rgstIdnt=<%=sessUserIdnt%>" onclick="return confirm('확인을 누르면 삭제가 완료됩니다.')">삭제</a></span>
							</c:if>
							<span class="button medium"><a href="#" onclick="javascript:fn_report('${AnucBord.worksId}','report/statBord06Report');">출력</a></span>
							</p>
						</div>
						<!-- //버튼 -->
						<c:if test="${not empty SuplList}">
						<div class="section mt30">						
						<p class="strong p12 mb10">▶ 공고정보 보완내역 </p>
							<table id="tblSupl" border="1" cellspacing="0" cellpadding="0" class="grid_content" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<tr>
									<th rowspan="2" class="p11">번호</th>
									<th colspan="2" class="p11 w75">보완사항</th>																
									<th rowspan="2" class="p11 w10">보완일자</th>
									<th rowspan="2" class="p11 w10">담당자</th>								
								</tr>
								<tr>									
									<th class="p11 w35">공고항목</th>
									<th class="p11 w40">보완세부내역</th>															
								</tr>						
								<tbody>								
 								<c:if test="${empty SuplList}">
									<tr>
										<td colspan="5" class="ce">
											등록된 공고정보 보완내역이 없습니다.
										</td>
									</tr>									
								</c:if>
								<c:if test="${not empty SuplList}">													
 								<c:forEach items="${SuplList}" var="SuplList">		
 								<tr>
									<td align="center">${SuplList.suplSeq }</td>
									<td>${SuplList.suplItem }</td>
									<td>[${SuplList.preItem }] 에서 [${SuplList.postItem }] (으)로 보완 </td>
									<td align="center">${SuplList.rgstDttm }</td>
									<td align="center">${SuplList.rgstName }</td>									
								</tr>									
								</c:forEach>
								</c:if>
								</tbody>
							</table>
																					
							<p class="strong line11 mt15"> * 보완사항 관련 문의 : 심의조사팀 - 이상윤, TEL : 02-2660-0102</p>
						</div>						
						</c:if>	
					</div>
						
				</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
			</div>
		<!-- //CONTAINER end -->		
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- 	<jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<!-- <script type="text/JavaScript">  -->
<!-- 	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기 -->
<!-- </script> -->
 
</body>
</html>

