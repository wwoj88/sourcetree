<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>


<%
	User user = SessionUtil.getSession(request);

	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	pageContext.setAttribute("UserName", sessUserName); 
	pageContext.setAttribute("UserIdnt", sessUserIdnt); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 조회 공고 - ${AnucBord.tite} | 법정허락 | 권리자찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css">
<link rel="stylesheet" type="text/css" href="/css/table.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/Function.js"></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 

jQuery(window).load(function(){	
	var bordDesc = document.getElementById("bordDesc").value;
	var bordCg = bordDesc.replace(/\r\n/g, "<br />");
	jQuery('#bordDe').append(bordCg);
	var anucItem8 = document.getElementById("anucItem8").value;
	var anuc8Cg = anucItem8.replace(/\r\n/g, "<br />");
	jQuery('#anuc8').append(anuc8Cg);
});
function ObjcInsert(){  // 권리자 이의제기 insert
	var frm = document.getElementById("ObjcInsertForm");
	var bordSeqn = document.getElementById("bordSeqn").value;
	var bordCd = document.getElementById("bordCd").value;
 	frm.action = "/statBord/objcInsert.do?bordSeqn="+bordSeqn+"&bordCd="+bordCd;
 	frm.submit();
}
function updateOc(statObjcId){	//권리자 이의제기 update 열닫
	var i=statObjcId;
	var docu = document.getElementById("updatediv"+i).style.display;
	var objc = document.getElementById("objc"+i).value;
	var objcRepl = objc.replace("/<br />/g", "\r\n");
	document.getElementById("objcDesc"+i).value = objcRepl;
	if(docu == "none"){
		document.getElementById("updatediv"+i).style.display = "block";
		document.getElementById("commentdiv"+i).style.display = "none";
		document.getElementById("fileUp"+i).style.display = "block";
		document.getElementById("fileDetl"+i).style.display = "none";
		document.getElementById("insertBt").style.display = "none";
	}else{
		document.getElementById("updatediv"+i).style.display = "none";
		document.getElementById("commentdiv"+i).style.display = "block";
		document.getElementById("fileUp"+i).style.display = "none";
		document.getElementById("fileDetl"+i).style.display = "block";
		document.getElementById("insertBt").style.display = "block";
	}
}
function ObjcInsertoc(){			//권리자 이의제기 insert 열닫
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
	}else if(userId != 'null' || userId!= ''){
		var i = document.getElementById("ObjcInsert").style.display;
		if(i == "none"){
			document.getElementById("ObjcInsert").style.display = "block";
			document.getElementById("detlDiv").style.display = "none";
		}else{
			document.getElementById("ObjcInsert").style.display = "none";
			document.getElementById("detlDiv").style.display = "block";
		}
	}
	
}
function updateObjc(statObjcId){
	var upf = document.getElementById("updateObjcForm"+statObjcId);
	upf.submit();
}
var iRowIdx = 1;

function fn_addRow1(statObjcId, gubun){
	//gubun = N - 수정에서 불러올 파일이 있을때,  Y - 등록할때 ,  F - 수정에서 불러올 파일이 없을때
	//statObjcId = -1이면 등록 / 아니면 각 이의제기의 statObjcId를 받아옴
	
	  if(gubun == "N"){
		  if(document.getElementsByName("chkDelL"+statObjcId).length > 3){
			  alert("첨부파일은 최대5개까지 가능합니다.");
			  return;
		  }
		  iRowIdx++;
	  var oTbl = document.getElementById("tblAttachFile"+statObjcId);
	  var oTbody = oTbl.getElementsByTagName("TBODY")[0];
	  var oTr = document.createElement("TR");
	  oTr.id="fileTr"+iRowIdx;
		//순번(선택)
		var oTd = document.createElement("TD");
		var sTag = '<input name="chkDelL'+statObjcId+'" id="chkDel1_'+iRowIdx+'" title="체크박스" type="checkbox" value='+iRowIdx+' \/>';
		oTd.innerHTML = sTag;
		oTd.id = "fileTd"+iRowIdx;
		oTd.style.width = '7%';
		oTd.className = 'ce';
		oTr.appendChild(oTd);

		//첨부화일명
		oTd = document.createElement("TD");
		oTd.colSpan = 2;
		sTag = '<input type="hidden" name="fileDelYn" id="fileDelYn_'+iRowIdx+'" \/>';
		sTag += '<span id="spfile'+iRowIdx+'"><input type="file" name="file'+iRowIdx+'" id="file_'+iRowIdx+'" class="inputData L w100" title="파일" onkeydown="return false;" onchange="checkFile(this.id, "U")"\/><\/span>';
				oTd.innerHTML = sTag;
		oTr.appendChild(oTd);
		
		oTbody.appendChild(oTr);
	  }else if(gubun =="Y"){
		  iRowIdx++;
		  if(iRowIdx > 5){
			  alert("첨부파일은 최대5개까지 가능합니다.");
			  iRowIdx--;
			  return;
		  }
	  	 var oTbl = document.getElementById("tblAttachFileIn");
		 var oTbody = oTbl.getElementsByTagName("TBODY")[0];
		  
		  var oTr = document.createElement("TR");
		  oTr.id="fileTr"+iRowIdx;
			//순번(선택)
			var oTd = document.createElement("TD");
			var sTag = '<input name="chkDel1" id="chkDel1_'+iRowIdx+'" type="checkbox" value='+iRowIdx+' \/>';
			oTd.innerHTML = sTag;
			oTd.id = "fileTd"+iRowIdx;
			oTd.style.width = '7%';
			oTd.className = 'ce';
			oTr.appendChild(oTd);

			
			//첨부화일명
			oTd = document.createElement("TD");
			oTd.colSpan = 2;
			sTag = '<input type="hidden" name="fileDelYn" id="fileDelYn_'+iRowIdx+'" \/>';
			sTag += '<span id="spfile'+iRowIdx+'"><input type="file" name="file'+iRowIdx+'" id="file_'+iRowIdx+'" class="inputData L w100" onkeydown="return false;" onchange="checkFile(this.id, "I")"\/><\/span>';
			oTd.innerHTML = sTag;
			oTr.appendChild(oTd);
			
			oTbody.appendChild(oTr);
	  }else{
		  iRowIdx++;
		  if(iRowIdx > 5){
			  alert("첨부파일은 최대5개까지 가능합니다.");
			  iRowIdx--;
			  return;
		  }
			  var oTbl = document.getElementById("tblAttachFile"+statObjcId);
			  var oTbody = oTbl.getElementsByTagName("TBODY")[0];
			  var oTr = document.createElement("TR");
			  oTr.id="fileTrL"+iRowIdx;
				//순번(선택)
				var oTd = document.createElement("TD");
				var sTag = '<input name="chkDel'+statObjcId+'" id="chkDel1_'+statObjcId+'" type="checkbox" value='+iRowIdx+' \/>';
				oTd.innerHTML = sTag;
				oTd.id = "fileTd"+statObjcId;
				oTd.style.width = '7%';
				oTd.className = 'ce';
				oTr.appendChild(oTd);

				
				//첨부화일명
				oTd = document.createElement("TD");
				oTd.colSpan = 2;
				sTag = '<input type="hidden" name="fileDelYn" id="fileDelYn_'+statObjcId+'" \/>';
				sTag += '<span id="spfile'+statObjcId+'"><input type="file" name="file'+statObjcId+'" id="file_'+statObjcId+'" class="inputData L w100" onkeydown="return false;" onchange="checkFile(this.id, "U")"\/><\/span>';
						oTd.innerHTML = sTag;
				oTr.appendChild(oTd);
				
				oTbody.appendChild(oTr);
			  }
		}
var fileArray = new Array();
var fileLength = 0;
function fn_delRow1(statObjcId, gubun){
	//gubun = N - 수정에서 불러올 파일이 있을때,  Y - 등록할때 ,  F - 수정에서 불러올 파일이 없을때
	if(gubun == "N"){
		var oChkDel = document.getElementsByName("chkDelL"+statObjcId);
	}else if(gubun == "Y"){
		var oChkDel = document.getElementsByName("chkDel1");
	}else{
		var oChkDel = document.getElementsByName("chkDel"+statObjcId);
	}
	var count = 0;
	var checkArray = new Array();

	for(var i=0; i<oChkDel.length; i++){
	if(oChkDel[i].checked == true){
		count++;
		var checkNum="";
		checkNum += oChkDel[i].value;
		checkArray.push(checkNum);
		fileArray.push(checkNum);
		iRowIdx--;
		}
	}
	if(gubun == "N"){
	  var fileListDel = document.getElementsByName("chkDelL"+statObjcId); //기존에 불려와있던 파일의 체크박스
			if(fileListDel.checked == true){
				count++;
				var checkNum="";
				checkNum += fileListDel.value;
				fileArray.push(checkNum);
				}
	  var upFrm = document.getElementById("updateObjcForm"+statObjcId)
	  upFrm.fileCk.value=fileArray.join();
	  upFrm.fileLength.value=fileArray.length;
	}
	if(count == 0){
		alert("삭제하실 내용을 체크해주세요.");
	}else{
		if(gubun == "N" || gubun == "Y"){
			for(var i = 0 ; i < checkArray.length ; i ++){
					element = document.getElementById('fileTr'+checkArray[i]);
					element.parentNode.removeChild(element);
			}
		}else{
			for(var i = 0 ; i < checkArray.length ; i ++){
				element = document.getElementById('fileTrL'+checkArray[i]);
				element.parentNode.removeChild(element);
			}
		}
	}
}

function fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.getElementById("form1");
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.target="boardView";
	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
}
jQuery(document).ready(function() {
	 if (jQuery("body").has("#now_loading").html() == null) {
	  var tmpHtml = "";
	  tmpHtml = "<div id='now_loading' style='position: absolute; top: 0; left: 0; display:none;z-index:9000'>";
	  tmpHtml += "<table width=200 height=100 border=0> ";
	  tmpHtml += "    <tr>";
	  tmpHtml += "        <td align=center><img id='ajax_load_type_img' src='/images/loading.gif' alt=''><\/td>";
	  tmpHtml += "    <\/tr>";
	  tmpHtml += "<\/table>";
	  tmpHtml += "<\/div>";

	  jQuery('#contentBody').append(tmpHtml).ajaxStart(function() {
		    var width = jQuery('#contentBody').width();
			var height = jQuery('#contentBody').height();
			
			//화면을 가리는 레이어의 사이즈 조정
			jQuery(".backLayer").width(width);
			jQuery(".backLayer").height(height);
			
			//화면을 가리는 레이어를 보여준다 (0.5초동안 30%의 농도의 투명도)
			jQuery(".backLayer").fadeTo(100, 0.0);
	   		showLoading();
	  }).ajaxStop(function() {
	    jQuery(".backLayer").fadeOut(1000);
	    hideLoading();
	  
	  });
	 }


	// 타이틀 추가 2014.09.12 - STR
	var titleName = "";
	var div = '${divsCd}';
	
	if(div=='5'){
		titleName = "저작권자 조회공고";
	}else if(div=='4'){
		titleName = "상당한노력 공고(개인)";
	}else if(div=='6'){
		titleName = "상당한노력 공고(위원회)";
	}
	
	this.title = "저작권자 조회공고 (${AnucBord.tite}) | 법정허락 | 권리자찾기";
	// 타이틀 추가 2014.09.12 - END
	});
	jQuery(window).resize(function(){
		var width = jQuery('#contentBody').width();
		var height = jQuery('#contentBody').height();
		jQuery(".backLayer").width(width).height(height);
	});

	function showLoading() {
		jQuery(document).bind("mousemove" ,(function(e){
			var x = e.pageX;
			var y = e.pageY;
			jQuery('#now_loading').css("left", x-460);
		    jQuery('#now_loading').css("top", y-230);
		}));
		jQuery('#now_loading').show();
	}

	function hideLoading() {
		jQuery(document).unbind("mousemove" , function(){});
		jQuery('#now_loading').hide();
	 }
function checkFile(fileId, key){
	var option;
	if(key =='I'){
	 option = {
				 url 		: '/statBord/fileSizeCk.do',
				 type		: "post",
				 dataType   : "json",
				 data 		: jQuery('#ObjcInsertForm'),
				 success	: function(data){
					if(data.fileSize > 1024*1024*2){
						alert("2MB 이상 파일은 업로드 하실 수 없습니다.");
						if (jQuery.browser.msie) {
						    jQuery("#"+fileId).replaceWith( jQuery("#"+fileId).clone(true) );
						} else {
						    jQuery("#"+fileId).val("");
						}
					}
				}
	 };
	 jQuery('#ObjcInsertForm').ajaxSubmit(option);
	}else{
		var ID = jQuery('#statObjcId').val(); 
		option = {
				 url 		: '/statBord/fileSizeCk.do',
				 type		: "post",
				 dataType   : "json",
				 data 		: jQuery('#updateObjcForm'+ID),
				 success	: function(data){
					if(data.fileSize > 1024*1024*2){
						alert("2MB 이상 파일은 업로드 하실 수 없습니다.");
						if (jQuery.browser.msie) {
						    jQuery("#"+fileId).replaceWith( jQuery("#"+fileId).clone(true) );
						} else {
						    jQuery("#"+fileId).val("");
						}
					}
				}
	 };
	 jQuery('#updateObjcForm'+ID).ajaxSubmit(option);
	}
		
}
//-->


</script>
<script type="text/javascript">
	/* (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69621660-2', 'auto');
	  ga('send', 'pageview'); */
</script>

</head>
<body>
	<!-- HEADER str-->
	<!-- 2017변경 -->
	<jsp:include page="/include/2017/header.jsp" />
	<%-- <jsp:include page="/include/2012/subHeader3.jsp" /> --%>
	<!-- 2017 주석처리 -->
	<!-- <script type="text/javascript">initNavigation(3);</script> -->
	<!-- GNB setOn 각페이지에 넣어야합니다. -->

	<!-- HEADER end -->
	<!-- CONTAINER str-->
	<div id="contents">
		<div class="con_lf">
			<!-- 래프 -->
			<h2><div class="con_lf_big_title">
				법정허락
			</div></h2>
			<ul class="sub_lf_menu">
				<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
				<li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
				<li><a class="on" href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
				<!-- <li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
				<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li> -->
				<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>
			</ul>
			<!-- //래프 -->
		</div>
		<div id="ajaxBox" style="position: absolute; z-index: 1; background: url(images/2012/common/lodingBg.png) no-repeat 0 0; left: -500px; width: 306px; height: 38px; padding: 102px 0 0 0;">
			<p style="height: 38px; text-align: center; margin: 0;">
				<img src="/images/2012/common/loading.gif" alt="" style="margin-top: -4px; margin-bottom: 3px;" /><br /> <span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
			</p>
		</div>
		<!-- 주요컨텐츠 str -->
		<div class="con_rt" id="contentBody">
			<c:if test="${divsCd == 5}">
				<div class="con_rt_head">
					<img src="/images/sub_img/sub_home.png" alt="홈 페이지" /> &gt; 법정허락 &gt; <span class="bold">저작권자 조회 공고</span>
				</div>
			</c:if>
			<%-- 					<c:if test="${divsCd == 4}">
					<div class="con_rt_head">
					<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						법정허락 승인 신청
						&gt;
						<span class="bold">상당한노력 공고(개인)</span>
					</div>
					</c:if>
					<c:if test="${divsCd == 6}">
						<div class="con_rt_head">
					<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						법정허락 승인 신청
						&gt;
						<span class="bold">상당한노력 공고(위원회)</span>
					</div>
					</c:if> --%>
			<h1><div class="con_rt_hd_title">저작권자 조회 공고</div></h1>
			<div id="sub_contents_con" class="section">
				<div class="sub01_con_bg4_tp mar_tp30"></div>
				<div class="sub01_con_bg4">
					<div class="font15"><span class="color_2c65aa">법정허락의 상당한 노력중의 하나인 저작권자 조회 내용의 공고를 진행하는 게시판입니다.</span></div>
					<div class="font15 mar_tp10"><span class="color_2c65aa">공고는 10일 이상 진행해야 합니다.</span></div>
				</div>
				<!-- 탭 -->
				<c:if test="${divsCd == 5}">
					<ul class="sub_menu1 w269 mar_tp40">
						<li class="on"><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5" class="on">저작권자 조회공고</a></li>
						<!-- 		<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=4">상당한노력 공고(개인)</a></li>
						<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=6" class="last_rt_bor">상당한노력 공고(위원회)</a></li> -->
					</ul>
				</c:if>

				<c:if test="${divsCd == 4}">
					<ul class="sub_menu1 w269 mar_tp40">
						<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회공고</a></li>
						<!-- 	<li class="on"><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=4" class="on">상당한노력 공고(개인)</a></li>
						<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=6" class="last_rt_bor">상당한노력 공고(위원회)</a></li> -->
					</ul>
				</c:if>

				<!-- 				<c:if test="${divsCd == 3}">
                     <ul id="tab11"  class="tab_menuBg2">
						 <li class="first"><a href="/statBord/statBo01List.do?bordCd=1&divsCd=5">저작권자 조회공고</a></li>
                         <li><a href="/statBord/statBo01List.do?bordCd=1&divsCd=4">상당한노력 공고</a></li>
                         <li class="on"><strong><a href="/statBord/statBo01List.do?bordCd=1&divsCd=3">이의제기</a></strong></li>
             		</ul>
					</c:if> -->
				<c:if test="${divsCd == 6}">
					<ul class="sub_menu1 w269 mar_tp40">
						<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회공고</a></li>
						<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=4">상당한노력 공고(개인)</a></li>
						<li class="on"><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=6" class="on last_rt_bor">상당한노력 공고(위원회)</a></li>
					</ul>
				</c:if>

				<p class="clear"></p>

				<div class="section mt20">
					<span class="topLine"></span>
					<!-- 그리드스타일 -->
					<form name="form1" id="form1" method="post" action="">
						<input type="hidden" name="filePath" /> <input type="hidden" name="fileName" /> <input type="hidden" name="realFileName" />
						<table border="1" cellspacing="0" cellpadding="0" class="grid">
							<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<tbody>
								<tr>
									<td class="p11" style="border: 1px solid #e7e7e7;padding: 6px 10px;color: #444;background: #f3f4f5;font-size: 11px;text-align: left;font-weight: bold;line-height: 1.3;">
										<span>${AnucBord.tite}</span>
									</td>
								</tr>
								<tr>
									<td>
										<span class="p12">공고자 : ${AnucBord.anucItem9}</span><span class="p11 ml20">공고일 : ${AnucBord.openDttm} </span>
									</td>
								</tr>
								<tr>
									<td>
										<ul class="mt15">
											<li>
												<p class="blue2">1. 저작재산권자를 찾는다는 취지</p>
												<p class="mt5" id="bordDe">
													-<input type="hidden" id='bordDesc' value="${AnucBord.bordDesc}" />
												</p>
											</li>
											<li class="mt10">
												<p class="blue2 mt10">2. 저작재산권자의 성명 또는 명칭, 주소 또는 거소 등</p>
												<p class="mt5">
													&nbsp;&nbsp;- 성명 : ${AnucBord.anucItem1}<br /> &nbsp;&nbsp;- 주소 : ${AnucBord.anucItem2}<br /> &nbsp;&nbsp;- 연락처 :${AnucBord.anucItem3}<br />
												</p>
												<p class="blue2 mt10">3. 저작물의 제호</p>
												<p class="mt5">
													&nbsp;&nbsp;- 장르 : ${AnucBord.genreCdName}<br /> &nbsp;&nbsp;- 제호 : ${AnucBord.anucItem4} <br />
												</p>
												<p class="blue2 mt10">4. 공표 시 표시된 저작재산권자의 성명(실명 또는 이명)</p>
												<p class="mt5">
													&nbsp;&nbsp;- 성명 : ${AnucBord.anucItem5}<br />
												</p>
												<p class="blue2 mt10">5. 저작물을 발행 또는 공표한자</p>
												<p class="mt5">
													&nbsp;&nbsp;- 저작물발행 : ${AnucBord.anucItem6}<br />&nbsp;&nbsp;- 공표연월일 : ${AnucBord.anucItem7}<br />
												</p>
												<p class="blue2 mt10">6. 저작물의 이용 목적</p>
												<p class="mt5" id="anuc8">
													- <input type="hidden" id="anucItem8" value="${AnucBord.anucItem8}" />
												</p>
												<p class="blue2 mt10">7. 복제물의 표지사진 등의 자료</p>
												<p class="mt5">
													<c:if test="${!empty fileList }">
														<c:forEach items="${fileList}" var="fileList">
															&nbsp;&nbsp;- 첨부파일 : 
															<a href="#1" onclick="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" onkeypress="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')"> 
																${fileList.fileName } <input name="attcSeqn" id="attcSeqn_D1${fileList.attcSeqn }" value="${fileList.attcSeqn }" type="hidden" />
															</a><br />
														</c:forEach>
													</c:if>
													<c:if test="${empty fileList}">
														&nbsp;&nbsp;- 첨부파일 : 등록된 파일이 없습니다.
													</c:if>
												</p>
												<%-- <table id="tblFile" border="1" width="200" cellspacing="0" summary="첨부파일 표입니다.">
												<caption>첨부파일표</caption>
													<!-- summary는 표의 간략한 설명을 써주면 됨 -->
													<colgroup>
														<col width="33%">
													</colgroup>
													<tbody>
														<tr>
															<th scope="row" style="text-align: center;">첨부파일명</th>
														</tr>
														<c:if test="${!empty fileList }">
															<c:forEach items="${fileList}" var="fileList">
																<tr>
																	<td>
																		<a href="#1" onclick="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" onkeypress="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')"> ${fileList.fileName } <input name="attcSeqn" id="attcSeqn_D1${fileList.attcSeqn }" value="${fileList.attcSeqn }" type="hidden" />
																		</a>
																	</td>
																</tr>
															</c:forEach>
														</c:if>
														<c:if test="${empty fileList}">
															<tr>
																<td>
																	<p class="blue2">등록된 파일이 없습니다.</p>
																</td>
															</tr>
														</c:if>
													</tbody>
												</table> --%>
												
												<p class="mt10">
													<br />
												</p>
												<p class="blue2">8. 공고자 및 연락처</p>
												<p class="mt5">
													&nbsp;&nbsp;- 공고자 : ${AnucBord.anucItem9}<br />&nbsp;&nbsp;- 주소 : ${AnucBord.anucItem10} <br />&nbsp;&nbsp;- 연락처 : ${AnucBord.anucItem11}<br />&nbsp;&nbsp;- 담당자 : ${AnucBord.anucItem12}
												</p> <br />
											</li>
										</ul>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
					<!-- //그리드스타일 -->
				</div>
				<div class='backLayer' style=''></div>
				<div class="result_area floatDiv mt20">
					<!-- 이의제기 갯수 표시 -->
					<c:if test="${count != 0}">
						<p class="tab fl">
							<span class="tab2 p12">이의제기 <strong>${count}</strong>건
							</span>
						</p>
					</c:if>
					<p class="fr">
						<span class="p11 line22">이의제기 내용은 작성자 본인만 조회됩니다.</span>
					</p>
				</div>
				<div id="detlDiv" class="section">

					<c:forEach items="${AnucBordObjc}" var="AnucBordObjc">
						<!-- 이의제기 내용 뿌려주기 시작 -->
						<c:if test="<%=sessUserIdnt != null%>">
							<c:if test="${AnucBordObjc.rgstIdnt == UserIdnt}">
								<ul class="statBo">
									<li><span class="topLine"></span>
										<form method="post" id="updateObjcForm${AnucBordObjc.statObjcId}" action="/statBord/objcUpdate.do?bordCd=${AnucBordObjc.bordCd}&amp;bordSeqn=${AnucBordObjc.bordSeqn}&amp;statObjcId=${AnucBordObjc.statObjcId}" enctype="multipart/form-data">
											<table id="tblFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부화일 표입니다.">
											<caption>첨부화일 표입니다</caption>
												<!-- summary는 표의 간략한 설명을 써주면 됨 -->
												<tbody>
													<tr>
														<td colspan="2">
															<span class="p15">등록자 :<b> ${UserName} </b></span>&nbsp;&nbsp;<span class="p15">등록일 : ${AnucBordObjc.rgstDttm}</span>
															<c:if test="${AnucBordObjc.statObjcCd == 1}">
																<p class="fr">
																	<span class="button small"> <a href="javascript:;" onclick="javascript:updateOc(${AnucBordObjc.statObjcId})">수정</a></span> <span class="button small"><a href="/statBord/objcDelete.do?statObjcId=${AnucBordObjc.statObjcId}&amp;bordCd=${AnucBordObjc.bordCd}&amp;bordSeqn=${AnucBordObjc.bordSeqn}&amp;divsCd=${divsCd}" onclick="return confirm('확인버튼을 누르면 삭제가 완료됩니다.')">삭제</a> </span>
																</p>
															</c:if>
														</td>
													</tr>
												</tbody>


												<colgroup>
													<col width="15%">
													<col width="*%">
													<col width="10%">
												</colgroup>
												<tbody>
													<tr id="fileTr">
														<th style="text-align: center;"><label>내용</label></th>
														<td>
															<div id="commentdiv${AnucBordObjc.statObjcId}" style="dispaly: block">${AnucBordObjc.objcDesc}</div>
															<!-- 이의제기 수정폼 -->
															<div class="mt10" id="updatediv${AnucBordObjc.statObjcId}" style="display: none">
																<textarea class="inputData" id="objcDesc${AnucBordObjc.statObjcId}" name="objcDesc" style="height: 45px; width: 85%"></textarea>
																<input type="hidden" id="statObjcId" name="statObjcId" value="${AnucBordObjc.statObjcId}" /> <input type="hidden" id="objc${AnucBordObjc.statObjcId}" name="objc" value="${AnucBordObjc.objcDesc}" /> <input type="hidden" name="rgstIdnt" id="rgstIdnt" value="<%=sessUserIdnt%>" /> <input type="hidden" id="divsCdUp" name="divsCdUp" value="${divsCd}" /> <input type="hidden" id="fileCk" name="fileCk" value="" /> <input type="hidden" id="fileLength" name="fileLength" value="0" /> <span class="button large"><a href="javascript:;" onclick="javascript:updateObjc(${AnucBordObjc.statObjcId})">수정</a></span>
															</div>
														</td>
													</tr>

													<tr>
														<th style="text-align: center;">첨부파일명</th>
														<c:if test="${!empty AnucBordObjc.fileList}">
															<td id="fileDetl${AnucBordObjc.statObjcId}" style="display: block">
																<div>
																	<table>
																		<c:forEach items="${AnucBordObjc.fileList}" var="fileListObjc">
																			<tr>
																				<td>
																					<a href="#" onclick="javascript:fileDownLoad('${fileListObjc.filePath}','${fileListObjc.fileName}','${fileListObjc.realFileName}')">${fileListObjc.fileName}</a>
																				</td>
																			</tr>
																		</c:forEach>
																	</table>
																</div>
															</td>
														</c:if>
														<td id="fileUp${AnucBordObjc.statObjcId}" style="display: none">
															<div class="fr mb5">
																<p>
																	<c:if test="${!empty AnucBordObjc.fileList}">
																		<span title="추가" class="button small"><button id="btnAdd" onkeypress="" onclick="javascript:fn_addRow1(${AnucBordObjc.statObjcId}, 'N');" type="button">File 추가</button></span>
																		<span title="삭제" class="button small"><button id="btnDel" onkeypress="" onclick="javascript:fn_delRow1(${AnucBordObjc.statObjcId}, 'N');" type="button">File 삭제</button></span>
																	</c:if>
																	<c:if test="${empty AnucBordObjc.fileList}">
																		<span title="추가" class="button small"><button id="btnAdd" onkeypress="" onclick="javascript:fn_addRow1(${AnucBordObjc.statObjcId}, 'F');" type="button">File 추가</button></span>
																		<span title="삭제" class="button small"><button id="btnDel" onkeypress="" onclick="javascript:fn_delRow1(${AnucBordObjc.statObjcId}, 'F');" type="button">File 삭제</button></span>
																	</c:if>
																</p>
															</div>

															<div class="mb15">
																<table id="tblAttachFile${AnucBordObjc.statObjcId}" border="1" cellspacing="0" class="grid mb15 " summary="첨부화일 표입니다.">
																	<!-- summary는 표의 간략한 설명을 써주면 됨 -->
																	<caption>첨부화일 표</caption>
																	<colgroup>
																		<col width="8%">
																		<col width="*%">
																	</colgroup>
																	<thead>
																		<tr>
																			<th scope="row" class="ce">순번</th>
																			<th scope="row">첨부파일</th>
																		</tr>
																	</thead>
																	<tbody>

																		<c:if test="${!empty AnucBordObjc.fileList}">
																			<c:forEach items="${AnucBordObjc.fileList}" var="fileList">
																				<tr id="fileTr${fileList.attcSeqn}">
																					<td class="ce" id="fileLTd${fileList.attcSeqn}">
																						<input name="chkDelL${AnucBordObjc.statObjcId}" id="chkDel${AnucBordObjc.statObjcId}" type="checkbox" value="${fileList.attcSeqn}" /> <input type="hidden" id="attcSeqns" value="${fileList.attcSeqn}" />
																					</td>
																					<td>${fileList.fileName}</td>
																				</tr>

																			</c:forEach>
																			<tr id="fileTr1">
																				<td class="ce" id="fileTd${AnucBordObjc.statObjcId}">
																					<input name="chkDel" id="chkDel${AnucBordObjc.statObjcId}" type="checkbox" value="1" />
																				</td>
																				<td>
																					<input type="hidden" name="fileDelYn" /> <input type="file" name="file" id="file_up2" title="첨부파일" class="inputData L w100" onkeydown="return false;" onchange="checkFile(this.id, 'U')" />
																				</td>
																			</tr>
																		</c:if>
																		<c:if test="${empty AnucBordObjc.fileList}">
																			<tr id="fileTrL1">
																				<td class="ce" id="fileTd${AnucBordObjc.statObjcId}">

																					<input name="chkDel${AnucBordObjc.statObjcId}" id="chkDel${AnucBordObjc.statObjcId}" type="checkbox" value="1" />
																				</td>
																				<td>
																					<input type="hidden" name="fileDelYn" /> <input type="file" name="file" id="file_up1" title="첨부파일" class="inputData L w100" onkeydown="return false;" onchange="checkFile(this.id, 'U')" />

																				</td>
																			</tr>
																		</c:if>
																	</tbody>
																</table>
																<span class="p11 orange block mt5">※ 첨부파일은 최대5개까지 가능합니다. </span>
															</div>
														</td>
														<c:if test="${empty AnucBordObjc.fileList}">
															<td id="fileDetl${AnucBordObjc.statObjcId}" style="display: block">
																<div>
																	<p class="blue2 line22">등록된 파일이 없습니다.</p>
																</div>
															</td>
														</c:if>
													</tr>
													<c:if test="${!empty AnucBordObjc.shisList}">
														<tr>
															<th style="background-color: white; border-color: white;" colspan="2" class="state"><c:forEach var="shisList" items="${AnucBordObjc.shisList}">
																	<c:if test="${AnucBordObjc.shisList != null}">
																		<strong class="block blue">${shisList.statObjcCdName}<span class="p11 thin gray ml10">${shisList.chngDttm}</span></strong>
																		<strong>${shisList.statObjcMemo}</strong>
																	</c:if>
																</c:forEach></th>
														</tr>
													</c:if>

												</tbody>
											</table>
										</form></li>
								</ul>
							</c:if>
						</c:if>
					</c:forEach>
				</div>
				<!-- 이의제기 등록 폼 -->
				<c:if test="<%=sessUserIdnt != null%>">
					<form method="post" action="#" id="ObjcInsertForm" enctype="multipart/form-data">
						<div class="mt10" id="ObjcInsert" style="display: none">
							<input type="hidden" name="rgstIdnt" id="rgstIdnt" value="<%=sessUserIdnt%>" /> <input type="hidden" name="statObjcMemo" id="statObjcMemo" value="memo" /> <input type="hidden" name="bordCd" id="bordCd" value="${bordCd}" /> <input type="hidden" name="divsCd" id="divsCd" value="${divsCd}" /> <input type="hidden" name="bordSeqn" id="bordSeqn" value="${AnucBord.bordSeqn}" /> <input type=hidden id="mgntDivs" name="mgntDivs" value="BO02_03" /> <input type=hidden id="worksTitl" name="worksTitl" value="${AnucBord.anucItem4}" />

							<div class="mb15">
								<span class="topLine"></span>
								<table id="tblFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부화일 표입니다.">
									<!-- summary는 표의 간략한 설명을 써주면 됨 -->

									<colgroup>
										<col width="15%">
										<col width="*%">
									</colgroup>

									<tbody>
										<tr>
											<th scope="row"><label for="" class="necessary">내용</label></th>
											<td class="ce" align="center">
												<textarea name="objcDesc" style="height: 45px; width: 85%"></textarea>
												&nbsp;<span class="button large"><a href="javascript:;" onclick="javascript:ObjcInsert();">등록</a></span>
											</td>
										</tr>
										<tr>
											<th scope="row"><label for="" class="necessary">첨부서류</label></th>
											<td colspan="3">
												<!-- 첨부화일 시작 -->

												<div class="">
													<div class="fr mb5">
														<p>
															<span title="추가" class="button small"><button id="btnAdd" onkeypress="" onclick="javascript:fn_addRow1('-1', 'Y');" type="button">File 추가</button></span> <span title="삭제" class="button small"><button id="btnDel" onkeypress="" onclick="javascript:fn_delRow1('-1', 'Y');" type="button">File 삭제</button></span>
														</p>
													</div>
												</div>
												<div class="mb15">
													<table id="tblAttachFileIn" border="1" cellspacing="0" class="grid mb15 " summary="첨부화일 표입니다.">
														<!-- summary는 표의 간략한 설명을 써주면 됨 -->
														<colgroup>
															<col width="8%">
															<col width="*%">
														</colgroup>
														<thead>
															<tr>
																<th scope="row" class="ce">순번</th>
																<th scope="row">첨부파일</th>
															</tr>
														</thead>
														<tbody>
															<tr id="fileTr1">
																<td class="ce" id="fileTd1">
																	<input name="chkDel1" id="chkDel1" type="checkbox" value="1" />
																</td>
																<td>
																	<input type="hidden" name="fileDelYn" /> <span id="spfile1"> <input type="file" name="file" id="file_In" title="첨부파일" class="inputData L w100" onkeydown="return false;" onchange="checkFile(this.id, 'I')" />
																	</span>

																</td>
															</tr>
														</tbody>
													</table>
													<span class="p11 orange block mt5">※ 첨부파일은 최대5개까지 가능합니다. </span>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</form>
				</c:if>
				<div class="btnArea">
					<p class="fl">
						<span class="button medium gray"><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=${divsCd}">목록</a></span>
					</p>
					<p id="insertBt" style="display:block;" class="fr">
						<span class="button medium"><a href="javascript:;" onclick="javascript:ObjcInsertoc()" title="권리자 이의제기">권리자 이의제기</a></span>
					</p>
				</div>

			</div>
			<!-- //주요컨텐츠 end -->
		</div>
		<p class="clear"></p>
	</div>
	<!-- //CONTAINER end -->

	<!-- FOOTER str-->
	<!-- 2017변경 -->
	<jsp:include page="/include/2017/footer.jsp" />
	<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
	<!-- FOOTER end -->
	<!-- <script type="text/javascript" src="http://www.right4me.or.kr:8080/js/2010/calendarcode.js"></script> -->
	<!-- <script type="text/JavaScript">  -->
	<!-- 	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기 -->
	<!-- </script> -->

</body>
</html>

