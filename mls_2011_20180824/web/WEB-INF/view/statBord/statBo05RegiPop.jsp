<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	
	String sessUserAddr = user.getUserAddr();
	String sessMoblPhon = user.getMoblPhon();

	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 이용승인신청 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
var iRowIdx = 1;
function fn_addRow1(){
	  iRowIdx++;
	  if(iRowIdx > 5){
		  alert("첨부파일은 최대5개까지 가능합니다.");
		  iRowIdx--;
		  return;
	  }
	  var oTbl = document.getElementById("tblAttachFile");
	  var oTbody = oTbl.getElementsByTagName("TBODY")[0];
	  
	  var oTr = document.createElement("TR");
	  oTr.id="fileTr"+iRowIdx;
		//순번(선택)
		var oTd = document.createElement("TD");
		var sTag = '<input name="chkDel1" id="chkDel1_'+iRowIdx+'" type="checkbox" value='+iRowIdx+' />';
		oTd.innerHTML = sTag;
		oTd.id = "fileTd"+iRowIdx;
		oTd.style.width = '7%';
		oTd.className = 'ce';
		oTr.appendChild(oTd);

		
		//첨부파일명
		oTd = document.createElement("TD");
		oTd.colSpan = 2;
		sTag = '<input type="hidden" name="fileDelYn" id="fileDelYn_'+iRowIdx+'" />';
		sTag += '<span id="spfile'+iRowIdx+'"><input type="file" name="file'+iRowIdx+'" id="file_'+iRowIdx+'" class="inputData L w100" onkeydown="return false;" onchange="checkFile(this.id)"/></span>';
				oTd.innerHTML = sTag;
		oTr.appendChild(oTd);
		
		oTbody.appendChild(oTr);

		       
	}

function fn_delRow1(){
var oTbl = document.getElementById("tblAttachFile");
var oChkDel = document.getElementsByName("chkDel1");
var count = 0;
var checkArray = new Array();

	for(var i=0; i<oChkDel.length; i++){
	if(oChkDel[i].checked == true){
		count++;
		var checkNum="";
		checkNum += oChkDel[i].value;
		checkArray.push(checkNum);			
		iRowIdx--;
	}
}
if(count == 0){
	alert("삭제하실 내용을 체크해주세요.");
}else{
	for(var i = 0 ; i < checkArray.length ; i ++){
			element = document.getElementById('fileTr'+checkArray[i]);
			element.parentNode.removeChild(element);
	}
}
}

function submitCheck(){
	var tite = document.getElementById("tite");
	
	var chk1=document.getElementById("ApplyCheck").checked;
	var anucItem1 = document.getElementById("anucItem1");
	var anucItem2 = document.getElementById("anucItem2");
	var anucItem3 = document.getElementById("anucItem3");
	var anucItem4 = document.getElementById("anucItem4");
	var anucItem5 = document.getElementById("anucItem5");
	var anucItem6 = document.getElementById("anucItem6");
	var anucItem7 = document.getElementById("anucItem7");
	var anucItem8 = document.getElementById("anucItem8");
	var chkAgr = document.getElementById("chkAgr");
	   if(!chk1){
	    	alert("개인정보 수집에 동의해주세요.");
	    	return false;
	    }
	 if(anucItem1.value=="" || anucItem1.value.length == 0){
		alert("저작물의 제호를 입력해주세요");
		anucItem1.focus();
	}else if(anucItem2.value=="" || anucItem2.value.length == 0){
		alert("저작자 및 저작재산권자의 성명을 입력해주세요");
		anucItem2.focus();
	}else if(anucItem3.value=="" || anucItem3.value.length == 0){
		alert("저작물 이용의 내용을 입력해주세요");
		anucItem3.focus();
	}else if(anucItem4.value=="" || anucItem4.value.length == 0){
		alert("공탁금액을 입력해주세요");
		anucItem4.focus();
	}else if(anucItem5.value=="" || anucItem5.value.length == 0){
		alert("공탁소의 명칭 및 소재지를 입력해주세요");
		anucItem5.focus();
	}else if(anucItem6.value=="" || anucItem6.value.length == 0){
		alert("공탁근거를 입력해주세요");
		anucItem6.focus();
	}else if(anucItem7.value=="" || anucItem7.value.length == 0){
		alert("저작물이용자의 주소를 입력해주세요");
		anucItem7.focus();
	}else if(anucItem8.value=="" || anucItem8.value.length == 0){
		alert("저작물이용자의 성명을 입력해주세요");
		anucItem8.focus();
	}else if(!chkAgr.checked){
		alert("담당자 보완 항목에 동의하여주세요");
		chkAgr.focus();
	}else{
		
	var frm = document.statForm;
	tite.value = anucItem1.value;
	frm.action = "/statBord/statBo01WriteMy.do";
	frm.method="post";
	frm.submit();
	alert("등록된 내용은 관리자 승인 후 공고됩니다.\n [마이페이지>신청현황>보상금공탁공고]에서 \n등록내용 확인이 가능합니다.");
	}

}
$(document).ready(function() {
	 if ($("body").has("#now_loading").html() == null) {
	  var tmpHtml = "";
	  tmpHtml = "<div id='now_loading' style='position: absolute; top: 0; left: 0; display:none;z-index:9000'>";
	  tmpHtml += "<table width=200 height=100 border=0> ";
	  tmpHtml += "    <tr>";
	  tmpHtml += "        <td align=center><img id='ajax_load_type_img' src='/images/loading.gif'></td>";
	  tmpHtml += "    </tr>";
	  tmpHtml += "</table>";
	  tmpHtml += "</div>";

	  jQuery('#contentBody').append(tmpHtml).ajaxStart(function() {
		  var width = jQuery('#contentBody').width();
			var height = jQuery('#contentBody').height();
			
			//화면을 가리는 레이어의 사이즈 조정
			jQuery(".backLayer").width(width);
			jQuery(".backLayer").height(height);
			
			//화면을 가리는 레이어를 보여준다 (0.5초동안 30%의 농도의 투명도)
			jQuery(".backLayer").fadeTo(500, 0.0);
	  		showLoading();
	  }).ajaxStop(function() {
	    jQuery(".backLayer").fadeOut(1000);
		hideLoading();
	  });
	 }
	});

	function showLoading() {
		jQuery(document).bind("mousemove" ,(function(e){
			var x = e.pageX;
			var y = e.pageY;
			jQuery('#now_loading').css("left", x-70);
		    jQuery('#now_loading').css("top", y-20);
		}));
		jQuery('#now_loading').show();
	}

	function hideLoading() {
		jQuery(document).unbind("mousemove" , function(){});
		jQuery('#now_loading').hide();
	 }
	function checkFile(fileId){
	    var option = {
					 url 		: '/statBord/fileSizeCk.do',
					 type		: "post",
					 dataType   : "json",
					 data 		: jQuery('#statForm'),
					 success	: function(data){
						if(data.fileSize > 1024*1024*2){
							alert("2MB 이상 파일은 업로드 하실 수 없습니다.");
							if (jQuery.browser.msie) {
							    jQuery("#"+fileId).replaceWith( jQuery("#"+fileId).clone(true) );
							} else {
							    jQuery("#"+fileId).val("");
							}
						}
					}
		 };
			jQuery('#statForm').ajaxSubmit(option);
	}

//-->
</script>
</head>
 <style>
	div.backLayer {
		display:none;
		background-color:black; 
		position:absolute;
		left:0px;
		top:0px;
	}

  </style>
<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
			<!-- HEADER str-->
			<div id="popHeader">
				<h1>보상금 공탁공고 신청</h1>
			</div>  
			<!-- //HEADER end -->
	<!-- 전체를 감싸는 DIVISION -->
	<div id="contentBody" class="floatDiv mt20" style="padding:0 15px 15px 15px;">
	
			
				<form method="post" name="statForm" id="statForm" action="#"  enctype="multipart/form-data">
					<div class="section">
						
						<h2 class="mt20">보상금공탁공고</h2>
						<span class="p11 orange block mt5 ml5">※ 보상금공탁을 한 내용을 아래의 입력 항목에 맞게 기입을 하세요.</span>
						<!-- 작성리스트str -->
						<ul>
						<li>
							<p class="line22 mt20"><label for="name1"><strong>1. 저작물의 제호</strong></label></p>
							<input class="inputData w98" type="text" title="저작물의 제호"  name="anucItem1" id="anucItem1" style="height: 25px;">
							<input type="hidden" name="tite" id="tite" value=""/>
							<input type="hidden" name="bordCd" id="bordCd" value="5"/>
							<input type="hidden" name="rgstIdnt" value="<%=sessUserIdnt%>"/>
							<input type=hidden id="mgntDivs" name="mgntDivs" value="BO05" />
						</li>
						<li class="mt15">
							<p class="line22 mt10"><label for="name2"><strong>2. 저작자 및 저작재산권자의 성명</strong></label></p>
							<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col>
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="nm">성명</label></th>
										<td><input class="inputData" type="text" title="저작자 및 저작재산권자의 성명"  name="anucItem2" id="anucItem2" style="height: 20px;">
										</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li class="mt15">
							<p class="line22 mt10"><label for="name3"><strong>3.저작물 이용의 내용</strong></label></p>
							<textarea cols="10" rows="3" class="textarea w98" title="저작물 이용의 내용"  name="anucItem3" id="anucItem3"></textarea>
							
						</li>
						<li class="mt15">
							<p class="line22 mt10"><label for="mon"><strong>4. 공탁금액</strong></label></p>
							<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col>
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="nm">금액</label></th>
										<td>
										<input class="inputData" type="text"  title="공탁금액" name="anucItem4" id="anucItem4" style="height: 20px;">
										</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li class="mt15">
							<p class="line22 mt10"><label for="name4"><strong>5. 공탁소의 명칭 및 소재지</strong></label></p>
							<input class="inputData w98" type="text" title="공탁소의 명칭 및 소재지"  name="anucItem5" id="anucItem5" style="height: 25px;">
						</li>
						<li class="mt15">
							<p class="line22"><label for="name5"><strong>6. 공탁근거</strong></label></p>
							<textarea cols="10" rows="3" class="textarea w98" title="공탁근거"  name="anucItem6" id="anucItem6"></textarea>
						</li>
						<li class="mt15">
							<p class="line22 mt10"><label for="name6"><strong>7. 저작물이용자의 주소/성명</strong></label></p>
							<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col>
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="nm">주소</label></th>
										<td>
										 <input class="inputData w98" type="text" title="저작물이용자의 주소"  name="anucItem7" id="anucItem7" value="<%=sessUserAddr%>">
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="nm">성명</label></th>
										<td>
										<input class="inputData" type="text" title="저작물이용자의 성명"  readonly="readonly" name="anucItem8" id="anucItem8" value="<%=sessUserName%>">
										</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li class="mt15">
						<p class="line22 mt20"><label for="name6"><strong>8. 첨부파일</strong></label></p>
							<div class="">
								<div class="fr mb5">
									<p>
										<span title="추가" class="button small"><button id="btnAdd" onkeypress="" onclick="javascript:fn_addRow1();" type="button">File 추가</button></span>
										<span title="삭제" class="button small"><button id="btnDel" onkeypress="" onclick="javascript:fn_delRow1();" type="button">File 삭제</button></span>
									</p>
								</div>
						</div>
						<div class="mb15">
						                                <table id="tblAttachFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부파일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
															<colgroup>
															    <col width="8%">
															    <col width="*%">
															</colgroup>
															<thead>
															    <tr>
						    									    <th scope="row" class="ce">순번</th>
						    									    <th scope="row">첨부파일</th>
															    </tr>
															</thead>
															<tbody>
															    <tr id="fileTr1">
															    	<td class="ce" id="fileTd1">
															    		<input name="chkDel1" id="chkDel1" type="checkbox" value="1" />
															    	</td>
															    	<td>
																    	<input type="hidden" name="fileDelYn" />
																		<span id="spfileD2'">
																		    <input type="file" name="file" id="file" title="첨부파일" 
																		        class="inputData L w100"
																		        onkeydown="return false;" onchange="checkFile(this.id)"/>
																		</span>
															    	</td>
															    </tr>
	                                           				</tbody>
														</table>
														<span class="p11 orange block">※ 첨부파일은 최대5개까지 가능합니다.</span>
													</div>
						</li>
							<li class="mt20">
						<p class="strong line22">개인정보 동의</p>
						<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<tbody>
									<tr id ="applycheck">
										<th scope="row"><label for="applyProxyName">
										1. 개인 정보 수집의 목적 <br><br>
										   - 저작권 공고에 따른 업무 처리를 위해 아래와 같이 개인 정보를 수집하고 있습니다.  <br><br>
										
										2. 개인 정보 내용     <br><br>
										  - 지적 재산권자 정보 : 성명, 주소, 연락처<br> 
										  - 공고자의 정보: 성명, 주소, 연락처  <br><br>
										
										3. 개인정보의 보유 및 이용기간 <br><br>
										  - 이용자의 개인정보는 원칙적으로 개인정보의 수집 및 이용목적이 달성되면 지체 없이 파기합니다.<br> 
										  - 따라서 최종 로그인 후 2년이 경과하였거나 정보주체의 회원 탈퇴 신청 시 회원의 개인정보를 지체 없이 파기합니다. <br><br>
						
										4. 동의 거부 권리 사실 및 불이익 내용 <br><br>
										  -   이용자는 동의를 거부할 권리가 있습니다.<br> 
										  -  동의를 거부할 경우에는 서비스 이용에 제한됨을 알려드립니다. <br><br>
										  </label></th>
									</tr>
								</tbody>
							</table>
							<h5 style="text-align: center;">위 개인정보 수집,이용에 동의합니다.(필수<input class="ApplyCheck" type="checkbox" title="동의" id="ApplyCheck">)</h5>
						</li>
						<li class="mt20">					
							<p class="strong line13">* 보상금 공탁공고 정보는 담당자에 의해 보완될 수 있습니다. (<input type='checkbox' id='chkAgr' class='mb3'/> 동의함)</p>
						</li>
						</ul>
						<!-- //작성리스트 -->
						
						<!-- 버튼 str -->
						<div class="btnArea mt10 mb10">
							<p class="fl"><span class="button medium gray"><a href="javascript:self.close()">취소</a></span></p>
							<p class="fr"><span class="button medium"><input type="button" value="등록" onclick="submitCheck()"></span> </p>
						</div>
						<!-- //버튼 -->
					</div>
				</form>
				</div>
					
			<!-- FOOTER str-->
			<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights reserved.</p>
			<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2011/button/pop_close.gif" alt="X" title="이 창을 닫습니다." /></a>
			<!-- //FOOTER end -->				
		<!-- //전체를 감싸는 DIVISION -->
<!-- <script type="text/javascript" src="http://www.right4me.or.kr:8080/js/2010/calendarcode.js"></script> -->
<!-- <script type="text/JavaScript">  -->
<!-- 	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기 -->
<!-- </script> -->
 
</body>
</html>

