<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	User user = SessionUtil.getSession(request);
if(user==null){
	  user=  SessionUtil.getSSO2(request);
	}
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	
	String sessUserAddr = user.getUserAddr();
	String sessMoblPhon = user.getMoblPhon();

	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 이용승인신청 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/Function.js"></script>
<script type="text/javascript" language="javascript"
	src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery.printElement.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 

$(window).load(function(){			//페이지의 로딩이 모두 끝난후
	var listSize = document.getElementById("listSize").value;
	if(listSize >= 5){	//listSize의 값을 불러와서 
		document.getElementById("file").style.display = "none";
		document.getElementById("file_add_bt").style.display = "none";
	}
	var bordDesc = document.getElementById("bordDesc").value;
	var bordRepl = bordDesc.replace(/<br>/g, "\r\n");
	document.getElementById("bordDesc").value = bordRepl;
});


var iRowIdx = 1;
var fileval = new Array();
var fileLength = 0;
function fn_addRow1(){
	  iRowIdx++;
	  if(iRowIdx > 5){
		  alert("첨부파일은 최대5개까지 가능합니다.");
		  iRowIdx--;
		  return;
	  }
	  var oTbl = document.getElementById("tblAttachFile");
	  var oTbody = oTbl.getElementsByTagName("TBODY")[0];
	  
	  var oTr = document.createElement("TR");
	  oTr.id="fileTr"+iRowIdx;
		//순번(선택)
		var oTd = document.createElement("TD");
		var sTag = '<input name="chkDel1" id="chkDel1_'+iRowIdx+'" type="checkbox" value='+iRowIdx+' />';
		oTd.innerHTML = sTag;
		oTd.id = "fileTd"+iRowIdx;
		oTd.style.width = '7%';
		oTd.className = 'ce';
		oTr.appendChild(oTd);

		
		//첨부파일명
		oTd = document.createElement("TD");
		oTd.colSpan = 2;
		sTag = '<input type="hidden" name="fileDelYn" id="fileDelYn_'+iRowIdx+'" />';
		sTag += '<span id="spfile'+iRowIdx+'"><input type="file" name="file'+iRowIdx+'" id="file_'+iRowIdx+'" class="inputData L w150" /></span>';
				oTd.innerHTML = sTag;
		oTr.appendChild(oTd);
		
		oTbody.appendChild(oTr);
}
/* 
function fn_delRow1(){
    var oTbl = document.getElementById("tblAttachFile");
    var oChkDel = document.getElementsByName("chkDel1");
    var count = 0;
    var checkArray = new Array();

   	for(var i=0; i<oChkDel.length; i++){
		if(oChkDel[i].checked == true){
			count++;
			var checkNum="";
			checkNum += oChkDel[i].value;
			checkArray.push(checkNum);			
			iRowIdx--;
		}
	}
	if(count == 0){
		alert("삭제하실 내용을 체크해주세요.");
	}else{
		for(var i = 0 ; i < checkArray.length ; i ++){
				element = document.getElementById('fileTr'+checkArray[i]);
				element.parentNode.removeChild(element);
		}
	}
} */
 function fn_delRow1(){
var oTbl = document.getElementById("tblAttachFile");
var oChkDel = document.getElementsByName("chkDel1");
var oChkDelL = document.getElementsByName("chkDelL");
var count = 0;
var checkArray = new Array();

	for(var i=0; i<oChkDel.length; i++){
	if(oChkDel[i].checked == true){
		count++;
		var checkNum="";
		checkNum += oChkDel[i].value;
		checkArray.push(checkNum);			
		iRowIdx--;
		}
	}
	for(var i=0; i<oChkDelL.length; i++){
		if(oChkDelL[i].checked == true){
			count++;
			var checkNum="";
			checkNum += oChkDelL[i].value;
			fileval.push(checkNum);			
			iRowIdx--;
		}
}
if(count == 0){
	alert("삭제하실 내용을 체크해주세요.");
}else{
	for(var i = 0 ; i < checkArray.length ; i ++){
	
			element = document.getElementById('fileTr'+checkArray[i]);
			element.parentNode.removeChild(element);
	}
	for(var i = 0 ; i < fileval.length ; i ++){
	
		element = document.getElementById('fileLTr'+fileval[i]);
		
		element.parentNode.removeChild(element);
	  fileLength++;
}

	document.getElementById("fileLength").value=fileLength;

	}
} 

function updateSubmit(){ //등록 submit
	var tite = document.getElementById("tite");
	var boadDesc = document.getElementById("bordDesc");
	var genre = document.getElementById("genre");
	var anucItem1 = document.getElementById("anucItem1");
	var anucItem2 = document.getElementById("anucItem2");
	var anucItem3 = document.getElementById("anucItem3");
	var anucItem4 = document.getElementById("anucItem4");
	var anucItem5 = document.getElementById("anucItem5");
	var anucItem6 = document.getElementById("anucItem6");
	var anucItem7 = document.getElementById("anucItem7");
	var anucItem8 = document.getElementById("anucItem8");
	var anucItem9 = document.getElementById("anucItem9");
	var anucItem10 = document.getElementById("anucItem10");
	var anucItem11 = document.getElementById("anucItem11");
	var anucItem12 = document.getElementById("anucItem12");
	var chkAgr = document.getElementById("chkAgr");	
	if(tite.value == "" || tite.value.length ==0){
		alert("저작물의 제목을 입력해주세요");
		tite.focus();
// 	}else if(bordDesc.value=="" || bordDesc.value.length == 0){
// 		alert("저작재산권자를 찾는다는 취지를 입력해주세요");
// 		bordDesc.focus();
	}else if(anucItem1.value=="" || anucItem1.value.length == 0){
		alert("저작재산권자의 성명을 입력해주세요");
		anucItem1.focus();
	}else if(anucItem2.value=="" || anucItem2.value.length == 0){
		alert("저작재산권자의 주소를 입력해주세요");
		anucItem2.focus();
	}else if(anucItem3.value=="" || anucItem3.value.length == 0){
		alert("저작재산권자의 연락처를 입력해주세요");
		anucItem3.focus();
	}else if(genre.value == 0){ 
		alert("제호의 장르를 선택해주세요");
		genre.focus();
	}else if(anucItem4.value=="" || anucItem4.value.length == 0){
		alert("제호를 입력해주세요");
		anucItem4.focus();
	}else if(anucItem5.value=="" || anucItem5.value.length == 0){
		alert("공표 시 표시된 저작재산권자의 성명을 입력해주세요");
		anucItem5.focus();
	}else if(anucItem6.value=="" || anucItem6.value.length == 0){
		alert("저작물발행을 입력해주세요");
		anucItem6.focus();
	}else if(anucItem7.value=="" || anucItem7.value.length == 0){
		alert("공표연월일을 입력해주세요");
		anucItem7.focus();
	}else if(anucItem7.value.length != 8 || anucItem7.value.indexOf('.') != -1 || anucItem7.value.indexOf('/') != -1 || anucItem7.value.indexOf('-') != -1){
		alert("공표연월일의 입력값은 잘못된 날짜형식입니다.");
		anucItem7.focus();
// 	}else if(anucItem7.value.length == 8 || anucItem7.value.indexOf('.') == -1 || anucItem7.value.indexOf('/') == -1 || anucItem7.value.indexOf('-') == -1){
// 		var result = true;
// 		var yearStr		= new Number(anucItem7.value.substring(0, 4));
// 		var monthStr	= new Number(anucItem7.value.substring(4, 6));
// 		var dayStr		= new Number(anucItem7.value.substring(6, 8));
// 		if(monthStr > 12 || monthStr < 1)
// 				result = false;
// 			else if(dayStr > 31 || dayStr < 1)
// 				result = false;
// 			if(result == false)
// 			alert("공표연월일의 입력값은 잘못된 날짜형식입니다.");
// 			anucItem7.focus();
			
	}else if(anucItem8.value=="" || anucItem8.value.length == 0){
		alert("저작물의 이용목적을 입력해주세요");
		anucItem8.focus();
	} else if(!chkAgr.checked){
		alert("담당자 보완 항목에 동의하여주세요");
		chkAgr.focus();
	} else{
	var frm = document.statWriteForm;
//  	if(checkForm(frm)) {
	var fileGrp = fileval.join();

		//장르(select) 값 hidden genreCd value에 옮겨서 값 넘기기
	var sel = document.getElementById("genre");
	var val = sel.options[sel.selectedIndex].value;
			//document.getElementById("genreCd").value = val;
    		$('#genreCd1').val(val);
    	
		document.getElementById("fileCheck").value=fileGrp;
		frm.action = "/statBord/statBo01ModiProc.do";
		frm.submit();
	}
}
jQuery(document).ready(function() {
	 if (jQuery("body").has("#now_loading").html() == null) {
	  var tmpHtml = "";
	  tmpHtml = "<div id='now_loading' style='position: absolute; top: 0; left: 0; display:none;z-index:9000'>";
	  tmpHtml += "<table width=200 height=100 border=0> ";
	  tmpHtml += "    <tr>";
	  tmpHtml += "        <td align=center><img id='ajax_load_type_img' src='/images/loading.gif'></td>";
	  tmpHtml += "    </tr>";
	  tmpHtml += "</table>";
	  tmpHtml += "</div>";

	  jQuery('#contentBody').append(tmpHtml).ajaxStart(function() {
		  var width = jQuery('#contentBody').width();
		  var height = jQuery('#contentBody').height();
			
			//화면을 가리는 레이어의 사이즈 조정
			jQuery(".backLayer").width(width);
			jQuery(".backLayer").height(height);
			
			//화면을 가리는 레이어를 보여준다 (0.5초동안 30%의 농도의 투명도)
			jQuery(".backLayer").fadeTo(100, 0.0);
	   		showLoading();
	  }).ajaxStop(function() {
	    jQuery(".backLayer").fadeOut(1000);
	    hideLoading();
	  });
	 }
	});
	jQuery(document).resize(function(){
		var width = jQuery('#wrap').width();
		var height = jQuery('#wrap').height();
		jQuery(".backLayer").width(width).height(height);
	});

function showLoading() {
	jQuery(document).bind("mousemove" ,(function(e){
		var x = e.pageX;
		var y = e.pageY;
		var width = jQuery('#contentBody').width();
		var height = jQuery('#contentBody').height();
		
		jQuery('#now_loading').css("left", x-460);
	    jQuery('#now_loading').css("top", y-230);
	}));
	jQuery('#now_loading').show();
}

	function hideLoading() {
		jQuery(document).unbind("mousemove" , function(){});
		jQuery('#now_loading').hide();
	 }
function checkFile(fileId){
	 var option = {
				 url 		: '/statBord/fileSizeCk.do',
				 type		: "post",
				 dataType   : "json",
				 data 		: jQuery('#statWriteForm'),
				 success	: function(data){
					if(data.fileSize > 1024*1024*2){
						alert("2MB 이상 파일은 업로드 하실 수 없습니다.");
						if (jQuery.browser.msie) {
						    jQuery("#"+fileId).replaceWith( jQuery("#"+fileId).clone(true) );
						} else {
						    jQuery("#"+fileId).val("");
						}
					}
				}
	 };
		jQuery('#statWriteForm').ajaxSubmit(option);
}

</script>
</head>
<style>
div.backLayer {
	display: none;
	background-color: black;
	position: absolute;
	left: 0px;
	top: 0px;
}
</style>

<body>

	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">

		<!-- HEADER str-->
		<div id="header">
			<!-- 2017변경 -->
			<jsp:include page="/include/2017/header.jsp" />
			<%-- <jsp:include	page="/include/2012/header.jsp" />  --%>
			<script type="text/javascript">initNavigation(0);</script>
		</div>
		<!-- 		HEADER end -->

		<!-- CONTAINER str-->
		<div id="contents">
			<!-- 래프 -->
			<br>
			<div class="con_lf" style="width: 22%">
				<div class="con_lf_big_title">마이페이지</div>
				<ul class="sub_lf_menu">
					<li><a href="/myStat/statRsltInqrList.do" class="on">신청현황</a>
						<ul class="sub_lf_menu2">

							<li><a href="/myStat/statRsltInqrList.do">법정허락 승인신청</a></li>
							<li><a href="/statBord/statBo01ListMy.do?bordCd=1"
								class="on">저작권자 조회 공고</a></li>
							<li><a href="/statBord/statBo05ListMy.do?bordCd=5">보상금
									공탁 공고</a></li>
						</ul></li>
					<li><a
						href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보</a>
						<ul class="sub_lf_menu2 disnone">
							<li><a
								href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보
									수정</a></li>
							<li><a
								href="/user/user.do?method=selectUserInfo&amp;DIVS=D&amp;userIdnt=<%=sessUserIdnt%>">회원탈퇴</a></li>
						</ul></li>
				</ul>
			</div>
			<!-- 주요컨텐츠 str -->
			<div class="con_rt" id="contentBody">
			
				<div class="con_rt_head">
					<img src="/images/sub_img/sub_home.png" alt="홈 페이지" /> &gt; 마이페이지
					&gt; 신청현황 &gt; <span class="bold">저작권자 조회 공고</span>
				</div>
				<div class="con_rt_hd_title">저작권자 조회 공고 수정</div>
				<br />
				<div class="section">
					<!-- memo str -->
					<div class="white_box mt0">
						<div class="box5">
							<div class="box5_con floatDiv">
								<p class="fl mt5">
									<img alt="" src="/images/2012/content/box_img4.gif">
								</p>
								<div class="fl ml30 ">
									<ul class="list1 mt10">
										<li>법정허락의 상당한 노력으로 저작권자 조회 내용의 공고를 직접신청은 10일, 문화체육관광부 장관이
											대행하는 경우는 60일 이상 동안 진행을 한다.</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- //memo -->

					<!-- section -->

					<div class="section">

						
						<!-- 그리드 str -->
						<form method="post" action="#" name="statWriteForm" id="statWriteForm" enctype="multipart/form-data">
							<table border="1" cellspacing="0" cellpadding="0" summary=""
								class="grid">
								<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
									<col width="*">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">제목&nbsp;</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce pd5"><input type="text" title="제목"
											name="tite" id="tite" value="${AnucBord.tite}"
											class="inputData w95" /></td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드 -->

							<ul class="mt15">
								<li>
									<p class="strong line22">1. 저작권자를 찾는다는 취지&nbsp;</p> <textarea
										cols="10" rows="3" id="bordDesc" name="bordDesc" title="취지"
										class="textarea w98">${AnucBord.bordDesc}</textarea>
								</li>
								<li class="mt15">
									<p class="strong line22">2. 저작재산권자의 성명 또는 명칭, 주소 또는 거소 등</p>
									<table border="1" cellspacing="0" cellpadding="0" summary=""
										class="grid mt5">
										<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
											<col width="20%">
											<col>
										</colgroup>
										<tbody>
											<tr>
												<th scope="row"><label for="nm">성명</label></th>
												<td><input type="text" title="성명" name="anucItem1"
													id="anucItem1" value="${AnucBord.anucItem1}"
													class="inputData" /></td>
											</tr>
											<tr>
												<th scope="row"><label for="zip">주소</label></th>
												<td><input type="text" title="주소" name="anucItem2"
													id="anucItem2" value="${AnucBord.anucItem2}"
													class="inputData" /></td>
											</tr>
											<tr>
												<th scope="row"><label for="phone">연락처</label></th>
												<td><input type="text" title="연락처" name="anucItem3"
													id="anucItem3" value="${AnucBord.anucItem3}"
													class="inputData" /></td>
											</tr>
										</tbody>
									</table>
								</li>
								<li class="mt20">
						<p class="strong line22">3. 저작물의 제호</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="tl">제호</label></th>
									<td>
										<select id="genre" name="genre" class="inputData">
													<option value="0">--전체--</option>
													<c:forEach items="${codeList}" var="codeList">
														<c:if test="${AnucBord.genreCd == codeList.midCode}">
																<option selected="selected" value="${codeList.midCode}">${codeList.codeName}</option>
															</c:if>
															<c:if test="${AnucBord.genreCd != codeList.midCode}">
																<option value="${codeList.midCode}">${codeList.codeName}</option>
															</c:if>
													</c:forEach>
										</select>
									<input type=hidden id="genreCd1" name="genreCd" />
									<input type=hidden id="mgntDivs" name="mgntDivs" value="BO01" />
									<input type="text" title="제호" name="anucItem4" id="anucItem4"		value="${AnucBord.anucItem4}" class="inputData" />
									</td>
								</tr>
							</tbody>
						</table>
					</li>
								<li class="mt15">
									<p class="strong line22">4. 공표 시 표시된 저작재산권자의 성명(실명 또는 이명)</p>
									<table border="1" cellspacing="0" cellpadding="0" summary=""
										class="grid mt5">
										<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
											<col width="20%">
											<col>
										</colgroup>
										<tbody>
											<tr>
												<th scope="row"><label for="nm2">성명</label></th>
												<td><input type="text" name="anucItem5" title="성명"
													id="anucItem5" class="inputData"
													value="${AnucBord.anucItem5}" /></td>
											</tr>
										</tbody>
									</table>
								</li>
								<li class="mt15">
									<p class="strong line22">5. 저작물을 발행 또는 공표한자</p>
									<table border="1" cellspacing="0" cellpadding="0" summary=""
										class="grid mt5">
										<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
											<col width="20%">
											<col>
										</colgroup>
										<tbody>
											<tr>
												<th scope="row"><label for="date1">저작물발행</label></th>
												<td><input type="text" id="anucItem6" title="저작물발행"
													name="anucItem6" class="inputData"
													value="${AnucBord.anucItem6}" /></td>
											</tr>
											<tr>
												<th scope="row"><label for="date2">공표연월일</label></th>
												<td><input type="text" maxlength="8" title="공표연월일"
													id="anucItem7" name="anucItem7" class="inputData"
													value="${AnucBord.anucItem7}" /> <span style="color: red">*
														ex ) 20120101(2012년 01월 01일)</span></td>
											</tr>
										</tbody>
									</table>
								</li>
								<li class="mt15">
									<p class="strong line22">6. 저작물의 이용 목적</p> <textarea cols="10"
										rows="3" name="anucItem8" id="anucItem8" class="textarea w98">${AnucBord.anucItem8}</textarea>
								</li>
								<li class="mt15">
									<p class="strong line22">7.복제물의 표지사진 등의 자료</p>
									<div class="floatDiv" id="filediv">
										<div class="fr mb5">
											<p>
												<span title="추가" class="button small"><button
														id="btnAdd" onkeypress=""
														onclick="javascript:fn_addRow1();" type="button">File
														추가</button></span> <span title="삭제" class="button small"><button
														id="btnDel" onkeypress=""
														onclick="javascript:fn_delRow1();" type="button">File
														삭제</button></span>
											</p>
										</div>
										<table id="tblAttachFile" border="1" cellspacing="0"
											class="grid mb15 " summary="첨부파일 표입니다.">
										
											<colgroup>
												<col width="8%">
												<col width="*%">

											</colgroup>
											<thead>
												<tr>
													<th scope="row" class="ce">순번</th>
													<th scope="row">첨부파일</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${fileList}" var="fileList">
													<tr id="fileLTr${fileList.attcSeqn}">
														<td class="ce" id="fileLTd${fileList.attcSeqn}"><input
															name="chkDelL" id="chkDelL" type="checkbox"
															value="${fileList.attcSeqn}" /></td>
														<td>${fileList.fileName}</td>
													</tr>
												</c:forEach>
												<tr id="fileTr1">
													<td class="ce" id="fileTd1">
													<input name="chkDel1"	id="chkDel1" type="checkbox" value="1" /></td>
													<td>
													<input type="hidden" name="fileDelYn" /> 
													<span id="spfileD2'">
													 <input type="file" name="file"		id="file" title="첨부파일" class="inputData L w200"/>
															
													</span></td>
												</tr>
											</tbody>
										</table>
										<span class="p11 orange block mt5">※ 첨부파일은 최대5개까지
											가능합니다.</span> <input type="hidden" id="fileCheck" name="fileCheck"
											value="" /> <input type="hidden" id="fileLength"
											name="fileLength" value="" />
									</div>
								</li>
								<li class="mt15">
									<p class="strong line22">8. 공고자 및 연락처</p>
									<table border="1" cellspacing="0" cellpadding="0" summary=""
										class="grid mt5">
										<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
											<col width="20%">
											<col>
										</colgroup>
										<tbody>
											<tr>
												<th scope="row"><label for="nm3">공고자</label></th>
												<td><input type="text" title="공고자" name="anucItem9"
													id="anucItem9" class="inputData"
													value="${AnucBord.anucItem9}" /></td>
											</tr>
											<tr>
												<th scope="row"><label for="zip3">주소</label></th>
												<td><input type="text" title="주소" name="anucItem10"
													class="inputData w98" value="${AnucBord.anucItem10}" /></td>
											</tr>
											<tr>
												<th scope="row"><label for="phone3">연락처</label></th>
												<td><input type="text" title="연락처" name="anucItem11"
													class="inputData" value="${AnucBord.anucItem11}" /></td>
											</tr>
											<tr>
												<th scope="row"><label for="admin3">담당자</label></th>
												<td><input type="text" title="담당자" name="anucItem12"
													class="inputData" value="${AnucBord.anucItem12}" /> <input
													type="hidden" name="bordCd" id="bordCd"
													value="${AnucBord.bordCd}" /> <input type="hidden"
													name="bordSeqn" id="bordSeqn" value="${AnucBord.bordSeqn}" />
													<input type="hidden" name="divsCd" id="divsCd" value="5" />
													<input type="hidden" name="rgstIdnt" id="rgstIdnt"													value="<%=sessUserIdnt%>" /></td>
													<input type="hidden" name="modiIdnt" id="modiIdnt"             value="<%=sessUserIdnt%>" /></td>
											</tr>
										</tbody>
									</table>
								</li>
								<li class="mt20">
									<p class="strong line13">
										* 저작권자 조회공고 정보는 담당자에 의해 보완될 수 있습니다. (<input type='checkbox'
											id='chkAgr' class='mb3' /> 동의함)
									</p>
								</li>
							</ul>
						</form>
						<!-- 버튼 str -->
						<div class="btnArea">
							<p class="fl">
								<span class="button medium gray"><a
									href="javascript:history.back(-1)">취소</a></span>
							</p>
							<p class="fr">
								<span class="button medium"><input type="button"
									value="수정" onclick="updateSubmit()" /></span>
							</p>
						</div>
						<!-- //버튼 -->

					</div>
					<!-- //section -->
				</div>
				<!-- //주요컨텐츠 -->

			</div>
			<!-- //content -->

			<!-- FOOTER str-->
			<jsp:include page="/include/2017/footer.jsp" />
			<!-- FOOTER end -->

		</div>
		<!-- //CONTAINER -->


	</div>
	<!-- //전체를 감싸는 DIVISION -->

	<!-- <script type="text/javascript" src="http://www.right4me.or.kr:8080/js/2010/calendarcode.js"></script>  -->
	<!-- <script type="text/JavaScript">  -->
	<!-- Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기 -->
	<!-- </script> -->

</body>
</html>