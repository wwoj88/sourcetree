<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>


<%
	User user = SessionUtil.getSession(request);
	
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	pageContext.setAttribute("UserName", sessUserName); 

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금 공탁 공고 ${AnucBord.tite} | 신청현황 | 마이페이지 | 권리자찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript">  
<!--
$(window).load(function(){
	var anucItem3 = document.getElementById("anucItem3").value;
	var anuc3Cg = anucItem3.replace(/\r\n/g, "<br />");
	jQuery('#anuc3').append(anuc3Cg);
	var anucItem6 = document.getElementById("anucItem6").value;
	var anuc6Cg = anucItem6.replace(/\r\n/g, "<br />");
	jQuery('#anuc6').append(anuc6Cg);
	
	tblSuplSort();
	SuplCheck();

});
function fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.getElementById("form1");
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.target="boardView";
	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
}


function tblSuplSort(){ // 보완목록에서 같은 차수일 경우 셀 합치기
	
	var tblSupl = document.getElementById("tblSupl");
	if(tblSupl==null){	return;
	}else{		
		startRow = 2; //검사 시작 row index
		var cNum = new Array(0, 3, 4);// cNum = 병합할 컬럼 번호
		
		var rows = tblSupl.rows;
		rowNum = rows.length; // 전체 줄 수
		tempVal = '';
		cnt = 0;		
		for( i = startRow; i < rowNum; i++ ) { 			
			var curVal = rows[i].cells[cNum[0]].innerHTML;		
			if( curVal == tempVal ) {
				if(cnt == 0) {
					cnt++;
					startRow = i - 1;
				}
				cnt++;
			}else if(cnt > 0) {				
				merge(tblSupl, startRow, cnt, cNum);				
				startRow = endRow = 0;
				cnt = 0;
			}else {
			}
				tempVal = curVal;		
		}
		
		if(cnt > 0) {			
		
			merge(tblSupl, startRow, cnt, cNum);
									
		}		
		
	}
		
}

function merge(tbl, startRow, cnt, cNum)
{
	rows = tbl.rows;
	row  = rows[startRow];
	
	for( i = startRow + 1; i < startRow + cnt; i++ ) {
		for( j = 0; j < cNum.size(); j++ ){
			rows[i].deleteCell(cNum[j]-j);
			//0번셀이 지워지면 뒤에있던 1번셀이 0번이 된다. 그래서 지워준 셀 수 만큼 인덱스 번호에서 빼준다.
		}
	}
	
	for( j = 0; j < cNum.size(); j++ ){
		row.cells[cNum[j]].rowSpan = cnt;	
	}
}

function SuplCheck(){ // 보완처리된 데이터에 * 표시 하기
	
	var suplItemCd = new Array();
	var count = 0;
	
	<c:forEach items="${SuplList}" var="SuplList">		
		if(${SuplList.suplItemCd}=='10' && jQuery('#anuc1').text().substr(jQuery('#anuc1').text().length-3)!="(*)")	{			 
			jQuery('#anuc1').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='20' && jQuery('#anuc2').text().substr(jQuery('#anuc2').text().length-3)!="(*)")	{			 
			jQuery('#anuc2').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='30' && jQuery('#anuc3').text().substr(jQuery('#anuc3').text().length-3)!="(*)")		{			 
			jQuery('#anuc3').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='40' && jQuery('#anuc4').text().substr(jQuery('#anuc4').text().length-3)!="(*)")		{			 
			jQuery('#anuc4').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='50' && jQuery('#anuc5').text().substr(jQuery('#anuc5').text().length-3)!="(*)")		{			 
			jQuery('#anuc5').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='60' && jQuery('#anuc6').text().substr(jQuery('#anuc6').text().length-3)!="(*)")		{			 
			jQuery('#anuc6').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='71' && jQuery('#anuc7').text().substr(jQuery('#anuc7').text().length-3)!="(*)")		{			 
			jQuery('#anuc7').append("\t\t (*)");
		}
		else if(${SuplList.suplItemCd}=='72' && jQuery('#anuc8').text().substr(jQuery('#anuc8').text().length-3)!="(*)")		{			 
			jQuery('#anuc8').append("\t\t (*)");
		}		
	</c:forEach>	
}

//출력물 
function fn_report(bordCd, bordSeqn, report){

	var frm = document.frm;
	var sUrl = "/statBord/statBo01DetlMy.do";

	var param = "?bordCd="+bordCd;
	     param += "&bordSeqn="+bordSeqn;
	     param += "&mode=R";
	     param += "&report="+report;
	 
	sUrl += param;
	window.open(sUrl,"PRINT","toolbar=no,menubar=no,resizable=no,status=yes");
}


//-->
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-1', 'auto');
  ga('send', 'pageview');
</script>
</head>
 
<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			
				<!-- 래프 -->																														=
				<div class="con_lf" style="width: 22%">
					<h2><div class="con_lf_big_title">마이페이지</div></h2>
					<ul class="sub_lf_menu">
						<li><a href="/statBord/statBo01ListMy.do?bordCd=1" class="on">신청현황</a>
							<ul class="sub_lf_menu2">
								<!-- <li><a href="/statBord/statBo06List.do?bordCd=6">저작권자 찾기위한 상당한 노력<br/>신청</a></li>
								<li><a href="/myStat/statRsltInqrList.do">법정허락 승인신청</a></li> -->
								<li><a href="/statBord/statBo01ListMy.do?bordCd=1">저작권자 조회 공고</a></li>
								<li><a href="/statBord/statBo05ListMy.do?bordCd=5" class="on">보상금 공탁 공고</a></li>
							</ul>
						</li>
						<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보</a>
							<ul class="sub_lf_menu2 disnone">
								<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보 수정</a></li>
								<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=D&amp;userIdnt=<%=sessUserIdnt%>">회원탈퇴</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<!-- //래프 -->
				
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						마이페이지
						&gt;
						신청현황
						&gt;
						<span class="bold">보상금 공탁 공고</span>
					</div>
					<h1><div class="con_rt_hd_title">보상금 공탁 공고</div></h1>
					<br/>
					<div class="section">
						<span class="topLine"></span>
							<form name="form1" id="form1" method="post">
							
							<input type="hidden" name="filePath">
							<input type="hidden" name="fileName">
							<input type="hidden" name="realFileName">
						
						<table border="1" cellspacing="0" cellpadding="0" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							
							<tbody>
								<tr>
									<th scope="row" class="p12">&lt;${AnucBord.tite}&gt; 보상금 공탁 공고</th>
								</tr>
								<tr>
									<td><span class="p11">공고자 : ${AnucBord.anucItem8}</span>
									<span class="p11 ml20">공고일 : <c:if test="${empty AnucBord.openDttm}">미공고</c:if>
								          <c:if test="${!empty AnucBord.openDttm}">${AnucBord.openDttm}</c:if></span></td>
								</tr>
								<tr>
									<td>
										<ul class="mt15">
										<li>
											<p class="blue2 line22">1. 저작물의 제호</p>
											<p id="anuc1" class="mt5 ">- ${AnucBord.anucItem1}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">2. 저작자 및 저작재산권자의 성명</p>
											<p id="anuc2"  class="mt5 ">- ${AnucBord.anucItem2}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">3. 저작물 이용의 내용</p>
											<p class="mt5 " id="anuc3">- <input type="hidden" id="anucItem3" value="${AnucBord.anucItem3} "/></p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">4. 공탁금액</p>
											<p class="mt5 " id="anuc4" >- ${AnucBord.anucItem4}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">5. 공탁소의 명칭 및 소재지</p>
											<p class="mt5 " id="anuc5" >- ${AnucBord.anucItem5}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">6. 공탁근거</p>
											<p class="mt5" id="anuc6">- <input type="hidden" id="anucItem6" value="${AnucBord.anucItem6}"/></p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">7. 저작물이용자의 주소/성명</p>
											<p class="mt5" id="anuc7" >- 주소 : ${AnucBord.anucItem7}</p>
											<p class="mt5" id="anuc8" >- 성명 : ${AnucBord.anucItem8}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">8. 첨부</p>
											<table id="tblAttachFile" border="1" width="200" cellspacing="0" summary="첨부파일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
															<colgroup>
															    <col width="33%">
															</colgroup>
															<tbody>
															    <tr>
						    									    <th scope="row" style="text-align:center;">첨부파일명</th>
															    </tr>
															    <c:if test="${!empty fileList }">
													    			<c:forEach items="${fileList}" var="fileList">
																	    <tr>
																	    	<td>
															    				<a href="#1" onclick="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
															    					onkeypress="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
																    				${fileList.fileName }
																    				<input name="attcSeqn" id="attcSeqn_D1" value="${fileList.attcSeqn }" type="hidden" />
															    				</a>
																	    	</td>
																	    </tr>
													    			</c:forEach>
													    		</c:if>
													    		<c:if test="${empty fileList }">
													    		<tr>
													    			<td>
													    				<p class="blue2 line22">등록된 파일이 없습니다.</p>
													    			</td>
													    		</tr>
													    		</c:if>
															</tbody>
														</table>
										</li>
										</ul>
									</td>
								</tr>
							</tbody>
						</table>
						<!-- //그리드스타일 -->
						</form>
						<div class="mb20">				
						<ul class="mt20">
							<li class="mt20">	
								<p class="strong line13">(*) 항목은 담당자에 의해 보완된 항목입니다 </p>
							</li>				
						<c:if test="${AnucBord.anucStatCd==2 }">
							<li class="mt15">	
								<p class="strong line11"> *  담당자 보완중인 신청정보입니다.</p>
							</li>				
						</c:if>
						<c:if test="${AnucBord.anucStatCd==4 }">
							<li class="mt15">
								<p class="strong line11"> *  반려된 신청정보입니다.</p>
							</li>	
						</c:if>						
						</ul>
						</div>
						<div class="btnArea">						
						<!-- 버튼 str -->
						<p class="fl"><span class="button medium gray"><a href="/statBord/statBo05ListMy.do?bordCd=5">목록</a></span></p>
						<p class="fr">
						<c:if test="${empty AnucBord.openDttm}">
							<c:if test="${AnucBord.anucStatCd!=2 }">
								<span class="button medium"><a href="/statBord/statBo05Modi.do?bordSeqn=${AnucBord.bordSeqn}">수정</a>
								</span>&nbsp;&nbsp;<span class="button medium"><a href="/statBord/statBo01Delete.do?bordCd=${AnucBord.bordCd}&bordSeqn=${AnucBord.bordSeqn}"  onclick="return confirm('확인을 누르면 삭제가 완료됩니다.')">삭제</a></span>
							</c:if>
						</c:if>
						<span class="button medium"><a href="#" onclick="javascript:fn_report('${AnucBord.bordCd}','${AnucBord.bordSeqn }','report/statBord03Report');">출력</a></span></p>
						
						<!-- //버튼 -->
						</div>
						<c:if test="${not empty SuplList}">
						<div class="section mt30">									
						<p class="strong p12 mb10">▶ 공고정보 보완내역 </p>
							<table id="tblSupl" border="1" cellspacing="0" cellpadding="0" class="grid_content" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<tr>
									<th rowspan="2" class="p11">번호</th>
									<th colspan="2" class="p11 w75">보완사항</th>																
									<th rowspan="2" class="p11 w10">보완일자</th>
									<th rowspan="2" class="p11 w10">담당자</th>								
								</tr>
								<tr>									
									<th class="p11 w35">공고항목</th>
									<th class="p11 w40">보완세부내역</th>															
								</tr>						
								<tbody>								
 								<c:if test="${empty SuplList}">
									<tr>
										<td colspan="5" class="ce">
											등록된 공고정보 보완내역이 없습니다.
										</td>
									</tr>									
								</c:if>
								<c:if test="${not empty SuplList}">													
 								<c:forEach items="${SuplList}" var="SuplList">		
 								<tr>
									<td align="center">${SuplList.suplSeq }</td>
									<td>${SuplList.suplItem }</td>
									<td>[${SuplList.preItem }] 에서 [${SuplList.postItem }] (으)로 보완 </td>
									<td align="center">${SuplList.rgstDttm }</td>
									<td align="center">${SuplList.rgstName }</td>									
								</tr>									
								</c:forEach>
								</c:if>
								</tbody>
							</table>
																					
							<p class="strong line11 mt15"> * 보완사항 관련 문의 : 심의조사팀 - 이상윤, TEL : 02-2660-0102</p>
						</div>
						</c:if>									
					</div>			
				<!-- //주요컨텐츠 end -->
			</div>
			<p class="clear"></p>
		</div>
		<!-- //CONTAINER end -->		
		
		<!-- FOOTER str-->
			<!-- 2017변경 -->
			<jsp:include page="/include/2017/footer.jsp" />
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
			<!-- FOOTER end -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

