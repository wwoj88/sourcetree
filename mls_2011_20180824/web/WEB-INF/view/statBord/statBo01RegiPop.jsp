<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
User user = SessionUtil.getSession(request);
if(user==null){
  user=  SessionUtil.getSSO2(request);
}
String sessUserIdnt = user.getUserIdnt();
String sessUserName = user.getUserName();

String sessUserAddr = user.getUserAddr();
String sessMoblPhon = user.getMoblPhon();

	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 이용승인신청 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 

$(document).ready(function() {
	 if ($("body").has("#now_loading").html() == null) {
	  var tmpHtml = "";
	  tmpHtml = "<div id='now_loading' style='position: absolute; top: 0; left: 0; display:none;z-index:9000'>";
	  tmpHtml += "<table width=200 height=100 border=0> ";
	  tmpHtml += "    <tr>";
	  tmpHtml += "        <td align=center><img id='ajax_load_type_img' src='/images/loading.gif'></td>";
	  tmpHtml += "    </tr>";
	  tmpHtml += "</table>";
	  tmpHtml += "</div>";

	  $('#contentBody').append(tmpHtml).ajaxStart(function() {
		  var width = $('#contentBody').width();
			var height = $('#contentBody').height();
			
			//화면을 가리는 레이어의 사이즈 조정
			$(".backLayer").width(width);
			$(".backLayer").height(height);
			
			//화면을 가리는 레이어를 보여준다 (0.5초동안 30%의 농도의 투명도)
			$(".backLayer").fadeTo(100, 0.0);
	   		showLoading();
	  }).ajaxStop(function() {
	    $(".backLayer").fadeOut(1000);
	   	hideLoading();
	  
	  });
	 }

	});

	function showLoading() {
		$(document).bind("mousemove" ,(function(e){
			var x = e.pageX;
			var y = e.pageY;
			$('#now_loading').css("left", x-70);
		    $('#now_loading').css("top", y-20);
		}));
		$('#now_loading').show();
	}

	function hideLoading() {
		$(document).unbind("mousemove" , function(){});
		$('#now_loading').hide();
	 }
	function checkFile(fileId){
	
	    var option = {
					 url 		: '/statBord/fileSizeCk.do',
					 type		: "post",
					 dataType   : "json",
					 data 		: jQuery('#statWriteForm'),
					 success	: function(data){
						if(data.fileSize > 1024*1024*2){
							alert("2MB 이상 파일은 업로드 하실 수 없습니다.");
							if (jQuery.browser.msie) {
							    jQuery("#"+fileId).replaceWith( jQuery("#"+fileId).clone(true) );
							} else {
							    jQuery("#"+fileId).val("");
							}
						}
					}
		 };
			jQuery('#statWriteForm').ajaxSubmit(option);



	}
var iRowIdx = 1;
	function fn_addRow1(){
		  iRowIdx++;
		  if(iRowIdx > 5){
			  alert("첨부파일은 최대5개까지 가능합니다.");
			  iRowIdx--;
			  return;
		  }
		  var oTbl = document.getElementById("tblAttachFile");
		  var oTbody = oTbl.getElementsByTagName("TBODY")[0];
		  
		  var oTr = document.createElement("TR");
		  oTr.id="fileTr"+iRowIdx;
			//순번(선택)
			var oTd = document.createElement("TD");
			var sTag = '<input name="chkDel1" id="chkDel1_'+iRowIdx+'" type="checkbox" value='+iRowIdx+' />';
			oTd.innerHTML = sTag;
			oTd.id = "fileTd"+iRowIdx;
			oTd.style.width = '7%';
			oTd.className = 'ce';
			oTr.appendChild(oTd);

			
			//첨부화일명
			oTd = document.createElement("TD");
			oTd.colSpan = 2;
			sTag = '<input type="hidden" name="fileDelYn" id="fileDelYn_'+iRowIdx+'" />';
			sTag += '<span id="spfile'+iRowIdx+'"><input type="file" name="file'+iRowIdx+'" id="file_'+iRowIdx+'" class="inputData L w100" /></span>';
					oTd.innerHTML = sTag;
			oTr.appendChild(oTd);
			
			oTbody.appendChild(oTr);

			       
		}
	
function fn_delRow1(){
    var oTbl = document.getElementById("tblAttachFile");
    var oChkDel = document.getElementsByName("chkDel1");
    var count = 0;
    var checkArray = new Array();

   	for(var i=0; i<oChkDel.length; i++){
		if(oChkDel[i].checked == true){
			count++;
			var checkNum="";
			checkNum += oChkDel[i].value;
			checkArray.push(checkNum);			
			iRowIdx--;
		}
	}
	if(count == 0){
		alert("삭제하실 내용을 체크해주세요.");
	}else{
		for(var i = 0 ; i < checkArray.length ; i ++){
				element = document.getElementById('fileTr'+checkArray[i]);
				element.parentNode.removeChild(element);
		}
	}
}

function writeSubmit(){ //등록 submit
	var tite = document.getElementById("tite");
	var chk1=document.getElementById("ApplyCheck").checked;
	 
	var boadDesc = document.getElementById("bordDesc");
	var genre = document.getElementById("genre");
	var anucItem1 = document.getElementById("anucItem1");
	var anucItem2 = document.getElementById("anucItem2");
	var anucItem3 = document.getElementById("anucItem3");
	var anucItem4 = document.getElementById("anucItem4");
	var anucItem5 = document.getElementById("anucItem5");
	var anucItem6 = document.getElementById("anucItem6");
	var anucItem7 = document.getElementById("anucItem7");
	var anucItem8 = document.getElementById("anucItem8");
	var anucItem9 = document.getElementById("anucItem9");
	var anucItem10 = document.getElementById("anucItem10");
	var anucItem11 = document.getElementById("anucItem11");
	var anucItem12 = document.getElementById("anucItem12");
	var chkAgr = document.getElementById("chkAgr");
	   if(!chk1){
	    	alert("개인정보 수집에 동의해주세요.");
	    	return false;
	    }
	if(tite.value == "" || tite.value.length ==0){
		alert("저작물의 제목을 입력해주세요");
		tite.focus();	
// 	}else if(bordDesc.value=="" || bordDesc.value.length == 0){
// 		alert("저작재산권자를 찾는다는 취지를 입력해주세요");
// 		bordDesc.focus();
	}else if(anucItem1.value=="" || anucItem1.value.length == 0){
		alert("저작재산권자의 성명을 입력해주세요");
		anucItem1.focus();
	}else if(anucItem2.value=="" || anucItem2.value.length == 0){
		alert("저작재산권자의 주소를 입력해주세요");
		anucItem2.focus();
	}else if(anucItem3.value=="" || anucItem3.value.length == 0){
		alert("저작재산권자의 연락처를 입력해주세요");
		anucItem3.focus();
	}else if(genre.value == 0){ 
		alert("제호의 장르를 선택해주세요");
		genre.focus();
	}else if(anucItem4.value=="" || anucItem4.value.length == 0){
		alert("제호를 입력해주세요");
		anucItem4.focus();
	}else if(anucItem5.value=="" || anucItem5.value.length == 0){
		alert("공표 시 표시된 저작재산권자의 성명을 입력해주세요");
		anucItem5.focus();
	}else if(anucItem6.value=="" || anucItem6.value.length == 0){
		alert("저작물발행을 입력해주세요");
		anucItem6.focus();
	}else if(anucItem7.value=="" || anucItem7.value.length == 0){
		alert("공표연월일을 입력해주세요");
		anucItem7.focus();
	}else if(anucItem7.value.length != 8 || anucItem7.value.indexOf('.') != -1 || anucItem7.value.indexOf('/') != -1 || anucItem7.value.indexOf('-') != -1 || anucItem7.value.indexOf('<') != -1 || anucItem7.value.indexOf('>') != -1){
		alert("공표연월일의 입력값은 잘못된 날짜형식입니다.");
		anucItem7.focus();
// 	}else if(anucItem7.value.length == 8 || anucItem7.value.indexOf('.') == -1 || anucItem7.value.indexOf('/') == -1 || anucItem7.value.indexOf('-') == -1){
// 		var result = true;
// 		var yearStr		= new Number(anucItem7.value.substring(0, 4));
// 		var monthStr	= new Number(anucItem7.value.substring(4, 6));
// 		var dayStr		= new Number(anucItem7.value.substring(6, 8));
// 		if(monthStr > 12 || monthStr < 1)
// 				result = false;
// 			else if(dayStr > 31 || dayStr < 1)
// 				result = false;
// 			if(result == false)
// 			alert("공표연월일의 입력값은 잘못된 날짜형식입니다.");
// 			anucItem7.focus();
			
	}else if(anucItem8.value=="" || anucItem8.value.length == 0){
		alert("저작물의 이용목적을 입력해주세요");
		anucItem8.focus();
	} else if(!chkAgr.checked){
		alert("담당자 보완 항목에 동의하여주세요");
		chkAgr.focus();
	} else{
	var frm = document.statWriteForm;
//  	if(checkForm(frm)) {
		alert("등록된 내용은 관리자 승인 후 공고 됩니다.\n[마이페이지>신청현황>저작권자 조회 공고] 에서 등록내용\n확인이 가능합니다.");
		//장르(select) 값 hidden genreCd value에 옮겨서 값 넘기기
		var sel = document.getElementById("genre");
		var val = sel.options[sel.selectedIndex].value;
		document.getElementById("genreCd").value = val;
		frm.action = "/statBord/statBo01WriteMy.do";
		frm.submit();
	}
}

//
</script>
 
</head>
<style>
	div.backLayer {
		display:none;
		background-color:black; 
		position:absolute;
		left:0px;
		top:0px;
	}

  </style>

<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
			<!-- HEADER str-->
			<div id="popHeader">
				<h1>저작권자조회 공고 신청</h1>
			</div>
			<!-- //HEADER end -->	
			<div id="contentBody" class="floatDiv mt20" style="padding:0 15px 15px 15px;">
					
				<!-- 그리드 str -->
					<form method="post" action="#" name="statWriteForm" id="statWriteForm" enctype="multipart/form-data">
					<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid" style="margin-bottom:10px;"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<colgroup>
						<col width="*">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">제목&nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="ce pd5"><input type="text"  title="제목" name="tite" id="tite" class="inputData w95" /></td>
							</tr>
						</tbody>
					</table>
					<!-- //그리드 -->
					
					<ul class="mt20">
					<li>
						<p class="strong line22">1. 저작권자를 찾는다는 취지&nbsp;</p>
							<textarea cols="10" rows="3" id="bordDesc" name="bordDesc"  title="취지" class="textarea w98" ></textarea>
					</li>
					<li class="mt20">
						<p class="strong line22">2. 저작재산권자의 성명 또는 명칭, 주소 또는 거소 등</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm">성명</label></th>
									<td><input type="text" title="성명" name="anucItem1" id="anucItem1" class="inputData" />
									</td>
								</tr>
								<tr>
									<th scope="row"><label for="zip">주소</label></th>
									<td><input type="text" title="주소" name="anucItem2" id="anucItem2" class="inputData w95" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="phone">연락처</label></th>
									<td><input type="text" title="연락처" name="anucItem3" id="anucItem3" class="inputData" /></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt20">
						<p class="strong line22">3. 저작물의 제호</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="tl">제호</label></th>
									<td>
										<select id="genre" name="genre" class="inputData" title="제호 종류">
													<option value="0">--전체--</option>
													<c:forEach items="${codeList}" var="codeList">
															<option  value="${codeList.midCode}">${codeList.codeName}</option>
													</c:forEach>
										</select>
									<input type=hidden id="genreCd" name="genreCd" value="" />
									<input type=hidden id="mgntDivs" name="mgntDivs" value="BO01" />
									<input type="text" name="anucItem4" title="제호" id="anucItem4" class="inputData w75" />
									</td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt20">
						<p class="strong line22">4. 공표 시 표시된 저작재산권자의 성명(실명 또는 이명)</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm2">성명</label></th>
									<td><input type="text" name="anucItem5" title="성명" id="anucItem5" class="inputData" /></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt20">
						<p class="strong line22">5. 저작물을 발행 또는 공표한자</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="date1">저작물발행</label></th>
									<td><input type="text" id="anucItem6" title="저작물발행" name="anucItem6" class="inputData" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="date2">공표연월일</label></th>
									<td><input type="text" id="anucItem7"  maxlength="8" title="공표연월일" name="anucItem7" class="inputData" /> <span style="color:red">* ex ) 20120101(2012년 01월 01일)</span></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt20">
						<p class="strong line22">6. 저작물의 이용 목적</p>
						<textarea cols="10" rows="3" name ="anucItem8" id="anucItem8" title="저작물 이용 목적" class="textarea w98" ></textarea>
					</li>
					<li class="mt20">
						<p class="strong line22">7.복제물의 표지사진 등의 자료</p>
						<p class="">
								<div class="fr mb5">
									<p>
										<span title="추가" class="button small"><button id="btnAdd" onkeypress="" onclick="javascript:fn_addRow1();" type="button">File 추가</button></span>
										<span title="삭제" class="button small"><button id="btnDel" onkeypress="" onclick="javascript:fn_delRow1();" type="button">File 삭제</button></span>
									</p>
								</div>
						</p>
						<div class="mb15">
						                                <table id="tblAttachFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부화일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
															<colgroup>
															    <col width="8%">
															    <col width="*%">
															</colgroup>
															<thead>
															    <tr>
						    									    <th scope="row" class="ce">순번</th>
						    									    <th scope="row">첨부파일</th>
															    </tr>
															</thead>
															<tbody>
															    <tr id="fileTr1">
															    	<td class="ce" id="fileTd1">
															    		<input name="chkDel1" id="chkDel1" type="checkbox" title="첨부파일 체크" value="1" />
															    	</td>
															    	<td>
																    	<input type="hidden" name="fileDelYn" />
																		<span id="spfileD2'">
																		    <input type="file" name="file" id="file" title="첨부파일" 
																		        class="inputData L w100"/>
																		   <!--      onkeydown="return false;" onchange="checkFile(this.id)"/> -->
																		</span>
																		
															    	</td>
															    </tr>
	                                           				</tbody>
														</table>
														<span class="p11 orange block mt5">※ 첨부파일은 최대5개까지 가능합니다.</span>
													</div>
					</li>
					<li class="mt20">
						<p class="strong line22">8. 공고자 및 연락처</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm3">공고자</label></th>
									<td><input type="text" title="공고자" name="anucItem9" id="anucItem9" class="inputData" value="<%=sessUserName%>"/></td>
								</tr>
								<tr>
									<th scope="row"><label for="zip3">주소</label></th>
									<td><input type="text" title="주소" name="anucItem10" class="inputData w98" value=""/></td>
								</tr>
								<tr>
									<th scope="row"><label for="phone3">연락처</label></th>
									<td><input type="text" title="연락처" name="anucItem11" class="inputData" value="<%//=sessMoblPhon%>"/></td>
								</tr>
								<tr>
									<th scope="row"><label for="admin3">담당자</label></th>
									<td><input type="text" title="담당자" name="anucItem12" class="inputData" />
									<input type="hidden" name="bordCd" id="bordCd" value="1"/>
									<input type="hidden" name="divsCd" id="divsCd" value="5"/>
									<input type="hidden" name="rgstIdnt" id="rgstIdnt" value="<%=sessUserIdnt%>" />
									</td>
								</tr>
							</tbody>
						</table>
					</li>
						<li class="mt20">
						<p class="strong line22">개인정보 동의</p>
						<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<tbody>
									<tr id ="applycheck">
										<th scope="row"><label for="applyProxyName">
										1. 개인 정보 수집의 목적 <br><br>
										   - 저작권 공고에 따른 업무 처리를 위해 아래와 같이 개인 정보를 수집하고 있습니다.  <br><br>
										
										2. 개인 정보 내용     <br><br>
										  - 지적 재산권자 정보 : 성명, 주소, 연락처<br> 
										  - 공고자의 정보: 성명, 주소, 연락처  <br><br>
										
										3. 개인정보의 보유 및 이용기간 <br><br>
										  - 이용자의 개인정보는 원칙적으로 개인정보의 수집 및 이용목적이 달성되면 지체 없이 파기합니다.<br> 
										  - 따라서 최종 로그인 후 2년이 경과하였거나 정보주체의 회원 탈퇴 신청 시 회원의 개인정보를 지체 없이 파기합니다. <br><br>
						
										4. 동의 거부 권리 사실 및 불이익 내용 <br><br>
										  -   이용자는 동의를 거부할 권리가 있습니다.<br> 
										  -  동의를 거부할 경우에는 서비스 이용에 제한됨을 알려드립니다. <br><br>
										  </label></th>
									</tr>
								</tbody>
							</table>
							<h2 style="text-align: center;">위 개인정보 수집,이용에 동의합니다.(필수<input class="ApplyCheck" type="checkbox" title="동의" id="ApplyCheck">)</h2>
					</li>
					<li class="mt20">					
						<p class="strong line13">* 저작권자 조회공고 정보는 담당자에 의해 보완될 수 있습니다. (<input type='checkbox' title="동의" id='chkAgr' class='mb3'/> 동의함)</p>
					</li>
					</ul>
					</form>
					<!-- 버튼 str -->	
					<div class="btnArea">
						<p class="fl"><span class="button medium gray"><a href="javascript:self.close()">취소</a></span></p>
						<p class="fr"><span class="button medium"><input type="button" value="등록" onclick="writeSubmit()" /></span></p>
					</div>
                              <!-- //버튼 -->	


			      	</div>
			
			
			
	<!-- //전체를 감싸는 DIVISION -->
	 <!-- FOOTER str-->
			<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights reserved.</p>
			<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2011/button/pop_close.gif" alt="닫기" title="이 창을 닫습니다." /></a>
			<!-- //FOOTER end -->
	
<!-- <script type="text/javascript" src="http://www.right4me.or.kr:8080/js/2010/calendarcode.js"></script>  -->
<!-- <script type="text/JavaScript">  -->
<!-- Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기 -->
<!-- </script> -->

</body>
</html>