<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
  String login = request.getParameter("login") == null ? "" : request.getParameter("login");
%> 
<%@ page import="java.net.URLEncoder"%>
<%@ page contentType="text/html;charset=euc-kr" %>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>로그인 | 회원정보 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css">
<link rel="stylesheet" type="text/css" href="/css/table.css">
<link rel="stylesheet" type="text/css" href="/css/new20.css"/>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> -->
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript">
// Enter Key 입력시 로그인 수행 (아이디/비밀번호)
function fn_loginChk(obj){

  // EnterKey 입력시 로그인 수행
  if (event.keyCode == 13) {
    fn_login();
  }
}

// 로그인 절차 수행
function fn_login(){
  
  var frm = document.form0;
  var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
 var user_id = frm.userIdnt.value;

  if (regExp.test(user_id)) {
    
    frm.loginDivs.value = "N";
    frm.action = "/ssoLogin.do";
    frm.submit();
    return ;
  }
  if(frm.userIdnt.value == "cpmadmin"){
    location.href = "/console/common/loginPage.page";
      return;
  }else if(frm.userIdnt.value == ""){
    alert("아이디가 입력되지 않았습니다.");
    frm.userIdnt.focus();
    return;
  } else if(frm.pswd.value == ""){
    alert("비밀번호가 입력되지 않았습니다.");
    frm.pswd.focus();
    return;
  } else {
    
      frm.loginDivs.value = "N";
    frm.action = "/userLogin/userLogin.do";
    frm.submit();
  }
}

//로그인 절차 수행
function fn_admin_login(){

  var frm = document.form0;
  //frm.action = "/console/common/loginPage.page";
  frm.action = "/console/main/main.page";
  frm.submit();
}
/* function ajaxTest(){
    $.ajax({
      type : "post",
      url : "http://localhost:8080",
      dataType : "text",
      error : function() {
        alert('통신실패!!');
      },
      success : function(data) {
       alert('5')
      }

    });
  } */
function init() {
  var frm = document.form0;

  document.getElementById("userIdnt").style.imeMode = "inactive"; //영문모드
  frm.userIdnt.focus();
  var loginFlag = '${errorMessage}';
  //console.log(loginFlag)
  if(loginFlag!=''){
    alert(loginFlag);
  }
}

function engKey(){
  document.getElementById("userIdnt").style.imeMode = "inactive"; //영문모드
/*  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-69621660-2', 'auto');
    ga('send', 'pageview'); */
}
</script>
</head>

<body onLoad="init();">
    <!-- HEADER str--><!-- 2017변경 -->
    <jsp:include page="/include/2017/header.jsp" />
    <%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
    <!-- 2017주석처리 -->
    <!-- <script type="text/javascript">initNavigation(0);</script> -->
    <!-- GNB setOn 각페이지에 넣어야합니다. -->

    <!-- HEADER end -->
    
    <div id="contents" class="wrap_n">
      <!-- 래프 -->
        <div class="con_lf" style="width: 22%">
          <h2><div class="con_lf_big_title">회원정보</div></h2>
          <ul class="sub_lf_menu">
            <li><a href="/user/user.do?method=goLogin" class="on">로그인</a></li>
            <li><a href="/user/user.do?method=goPage">회원가입</a></li>
            <li><a href="https://devoneid.copyright.or.kr/member/infoFind/idFindStep1.do">아이디 찾기</a></li>
            <!-- <li><a href="/user/user.do?method=goIdntSrch">아이디 찾기</a></li> -->
            <li><a href="https://devoneid.copyright.or.kr/member/infoFind/passFindStep1.do">비밀번호 찾기</a></li>
            <!-- <li><a href="/user/user.do?method=goPswdSrch">비밀번호 찾기</a></li> -->
          </ul>
        </div>
      <!-- //래프 -->
        
        <!-- 주요컨텐츠 str -->
        <div class="con_rt" id="contentBody">
          <div class="con_rt_head">
            <img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
            &gt;
            회원정보
            &gt;
            <span class="bold">로그인</span>
          </div>
          <h1><div class="con_rt_hd_title">로그인</div></h1>
          <div class="sub_contents_con">
  
          <!-- 
            <form name="form_4" method="post" class="frm" action="http://localhost:8080/ssoMain.do">
              <input type="hidden" name="access_Token" value="MzNkNDE1ZTM3YzBjNTJjMWIyMGIzNzBhOWMyNTNiM2NlNTEyMjFhYmQyMWY3ZjM2ZWYyOGQ5MmEzZTNiN2UyMDk4MWVmMDc3YmI1MjNlZTdkYzc4MmIxMDZjYzA3NmE4NzhmZTc2ZDM4YTNkNzU4ZjFlYjhhOWEzYWUzMDk1OTYzZDJlNTBlYTY1MWZkYWNhOTJlZDMwYWQ1NmRhMTAzZGI2Yjg4ZjE5OGNmZDY0NTBjNTEwYWQ3ODRiMGJhM2UzMzBmZDkxNWI0ZTNlNzBmZWNjMzcyYmQ4MTA0NzdkNDEwODlmM2ExYzg4YTVlYjhkM2JkODVhZWNjMDc4MzJkOA==">
                <input type="hidden" name="refresh_Token" value="http://www.findcopyright.or.kr:8080"/>
                  <input type="submit" value="check">
          </form>
                -->
  <!--           <form name="form_6" method="post" class="frm" action="http://www.291.250.104.102:8080">
              <input type="hidden" name="access_Token" value="MzNiZTA2ZmUxYjI4ZDY0OWQzYzMyOTExYWJhYTk2OTQ0NWM1ODVhYmJmOGJhOTBkZWYzNzUxYWRiYmM4ODRiNGYxM2ZlMGIwOWUzMGJjODI2MzgyNjgwYTc1MDMyNTA4NWRmMmUwODhlNTVmYWI3OTNiMTlkNjlhMTU2YzI5NzQyZjJlMGY4YTc0YmZlZWYxZmQxYTBiZGY3Yzg0ZDRlYzZkMjliYjViYWQ4N2NiODRkYjdmYTU0MWU1MGY2YTE1NWMxMDcwYjYyNmVlMmJlMjg0MWYyOGY4NmMyYzJjYWEzMTFjM2UwNThkYjNhNGI1MmI3YWU1MjZmZTk1Y2Q0OA==">
                <input type="hidden" name="refresh_Token" value="http://www.291.250.104.102:8080"/>
                  <input type="submit" value="check">
          </form>  -->
        
          
            <form name="form0" method="post" class="frm" action="#">
              <input type="hidden" name="login" value="Y">
                <input type="hidden" name="loginDivs" />
                <input type="submit" style="display:none;">
              <div class="login_bg" style="padding: 76px 61px 45px;">
                <div class="login_admin" style="text-align: center;">
                <!-- <a href="/console/common/loginPage.page">관리자 로그인 바로가기</a> -->
                <!-- <a href="#" onclick="fn_admin_login();">관리자 로그인 바로가기</a> -->
                <!-- <a href="#" onclick="ajaxTest();">관리자 로그인 바로가기</a> -->
              </div>
              <div class="login">
                <div class="login_lf"><img src="/images/2020/common/icon_member_4.png" alt="" /></div>
                <div class="login_rt">
                  <div class="login_rt_tp">
                    <div class="login_rt_tp_lf">
                      <div class="login_rt_tp_lf_lf">
                        <label for="userIdnt"><div class="login_word">아이디</div></label>
                        <div class="login_input"><input id="userIdnt"  name="userIdnt" type="text" onclick="engKey()" onkeydown="fn_loginChk(this)" value="" /></div>
                        <p class="clear"></p>
                      </div>
                      <div class="login_rt_tp_lf_rt">
                        <label for="pswd"><div class="login_word">비밀번호</div></label>
                        <input style="display:none" aria-hidden="true">
                        <input type="password" style="display:none" aria-hidden="true">
                        <div class="login_input"><input type="password" id="pswd" autocomplete="off" name="pswd" onkeydown="fn_loginChk(this)" value=""/></div>
                        <p class="clear"></p>
                      </div>
                    </div>
                    <div class="login_rt_tp_rt" style="height: ">
                      <a href="#" onclick="fn_login();">로그인</a>
                    </div>
                    <p class="clear"></p>
                  </div>
                  <div class="login_rt_btm">
                    <a href="/user/user.do?method=goPage" class="button button1">회원가입</a>
                    <!-- <a href="/user/user.do?method=goIdntSrch" class="button button1">아이디찾기</a> -->
                    <!-- <a href="/user/user.do?method=goPswdSrch" class="button button1">비밀번호찾기</a> -->
                    <a href="#" onclick="fn_admin_login();" class="button button2">관리자 로그인 바로가기</a>
                  </div>
                </div>
                <p class="clear"></p>
              </div>
            </div>
            
            </form>
            
    
            </div>
        <!--  --><!--  --><!--  -->
              <div class="login_foot2">
        <h2 class="sub_con_h2 orange">알려드립니다.</h2>
        <p>저작권찾기 사이트를 이용하기 위해서는 로그인 하시기 바랍니다.<br>

관리자는 <strong>[관리자 로그인 바로가기]</strong>로 이동하여 로그인 하시기 바랍니다.</p>
        </div>
          </div>
        <!-- //주요컨텐츠 end -->
        <p class="clear"></p>
      </div>

    <%-- <%  if ("Y".equals(login)) {  %>
    <c:if test="${not empty errorMessage}">
      <script type="text/javascript">
      
            alert(${errorMessage}); 
      
      </script>
    </c:if>
    <%  }  %> --%>
    <!-- //CONTAINER end -->
    
    <!-- FOOTER str-->
    <!-- 2017변경 -->
    <jsp:include page="/include/2017/footer.jsp" />
    <%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
    <!-- FOOTER end -->
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/script.js"></script>
</body>
</html>
