<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
  String login = request.getParameter("login") == null ? "" : request.getParameter("login");
%> 
<%@ page contentType="text/html;charset=euc-kr" %>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>로그인 | 회원정보 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> -->
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<!-- <script type="text/javascript" language="javascript" src="/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript">var Doit = jQuery.noConflict();</script>
<script type="text/javascript" src="/js/2017/quickControl.js"></script> -->

<script>
// Enter Key 입력시 로그인 수행 (아이디/비밀번호)
function fn_loginChk(obj){

	if (event.keyCode == 13) {
		fn_login();
	}
}

function fn_loginChkId(obj){
	// EnterKey 입력시 로그인 수행
	
    var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;

    if (regExp.test(obj)) {
    	 fn_login();
    } else{
    	alert("아이디는 이메일형식입니다.")
    	return ;
    }


}

// 로그인 절차 수행
function fn_login(){
  
  var frm = document.form0;


  var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;

  if (!regExp.test(frm.userIdnt.value)) {
  	 
  	alert("아이디는 이메일형식입니다.")
  	return ;
  }

  
  if(frm.userIdnt.value == "cpmadmin"){
	  location.href = "/console/common/loginPage.page";
	   	return;
  }else if(frm.userIdnt.value == ""){
  	alert("아이디가 입력되지 않았습니다.");
   	frm.userIdnt.focus();
   	return;
  } else if(frm.pswd.value == ""){
   	alert("비밀번호가 입력되지 않았습니다.");
   	frm.pswd.focus();
   	return;
  } else {
    	frm.loginDivs.value = "N";
		frm.action = "/ssoLogin.do";
		frm.submit();
  }
}

//로그인 절차 수행
function fn_admin_login(){
  
  var frm = document.form0;
	//frm.action = "/console/common/loginPage.page";
	frm.action = "/console/main/main.page";
	frm.submit();
}
$(function(){
	init();
})
function init() {
	
	var frm = document.form0;

	document.getElementById("userIdnt").style.imeMode = "inactive"; //영문모드
	frm.userIdnt.focus();
	var loginFlag = '${errorMessage}';
	//console.log(loginFlag)
	if(loginFlag!=''){
		alert(loginFlag);
	}
}

function engKey(){
	document.getElementById("userIdnt").style.imeMode = "inactive"; //영문모드
}

function move(){
	var frm = document.linkForm;
	var url = frm.siteLink.value;
	if(url == ''){
		alert("관련사이트를 선택해 주세요");
		return;
	}else{
		window.open(url,'');
	}
}
	function move1(){
	var frm = document.linkForm;
	var url = frm.siteLink1.value;
	if(url == ''){
		alert("관련사이트를 선택해 주세요");
		return;
	}else{
		window.open(url,'');
	}
}

</script>


</head>

<body>
		<!-- HEADER str--><!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
		<!-- 2017주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<div id="contents">
			<!-- 래프 -->
				<div class="con_lf" style="width: 22%">
					<div class="con_lf_big_title">회원정보</div>
					<ul class="sub_lf_menu">
            <li><a href="/user/user.do?method=goLogin" class="on">로그인</a></li>
            <li><a href="/user/user.do?method=goPage">회원가입</a></li>
            <li><a href="https://devoneid.copyright.or.kr/member/infoFind/idFindStep1.do">아이디 찾기</a></li>
            <!-- <li><a href="/user/user.do?method=goIdntSrch">아이디 찾기</a></li> -->
            <li><a href="https://devoneid.copyright.or.kr/member/infoFind/passFindStep1.do">비밀번호 찾기</a></li>
            <!-- <li><a href="/user/user.do?method=goPswdSrch">비밀번호 찾기</a></li> -->
					</ul>
				</div>
			<!-- //래프 -->
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						회원정보
						&gt;
						<span class="bold">로그인</span>
					</div>
					<div class="con_rt_hd_title">통합 로그인</div>
				    <form name="form0" method="post" class="frm" action="#">
							<input type="hidden" name="login" value="Y">
						    <input type="hidden" name="loginDivs" />
						    <input type="submit" style="display:none;">
							<div class="login_bg">
						   	<div class="login_admin" style="text-align: center;">
								<!-- <a href="/console/common/loginPage.page">관리자 로그인 바로가기</a> -->
								<a href="#" onclick="fn_admin_login();">관리자 로그인 바로가기</a>
							</div>
							<br>
							<!-- <div class="login_admin" style="text-align: center;">
								<a href="/user/user.do?method=goLogin">로그인 바로가기</a>
							</div> -->
							<div class="login">
								<div class="login_lf"><img src="/images/main/login_01.png" alt="그림" /></div>
								<div class="login_rt">
									<div class="login_rt_tp">
										<div class="login_rt_tp_lf">
											<div class="login_rt_tp_lf_lf">
												<div class="login_word">아이디</div>
												<div class="login_input">
												<input id="userIdnt"  name="userIdnt" type="text" title="계정 입력"  value="" placeholder="이메일 형식 통합아이디 기입해주세요." /></div>
												<p class="clear"></p>
											</div>
											<div class="login_rt_tp_lf_rt">
												<div class="login_word">비밀번호</div>
												<input style="display:none" aria-hidden="true">
												<input type="password" style="display:none" aria-hidden="true">
												<div class="login_input"><input type="password" title="비밀번호 입력" id="pswd" autocomplete="false" name="pswd" onkeydown="fn_loginChk(this)" value=""/></div>
												<p class="clear"></p>
											</div>
										</div>
										<div class="login_rt_tp_rt" >
											<a style="height:12.6px; line-height:14px;" href="#" onclick="fn_login();">로그인</a>
										</div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_btm">
                    <a href="/user/user.do?method=goPage" class="button button1">회원가입</a>
                    <!-- <a href="/user/user.do?method=goIdntSrch" class="button button1">아이디찾기</a> -->
                    <!-- <a href="/user/user.do?method=goPswdSrch" class="button button1">비밀번호찾기</a> -->
                    <a href="#" onclick="fn_admin_login();" class="button button2">관리자 로그인 바로가기</a>
									</div>
								</div>
								<p class="clear"></p>
							</div>
							<div class="login_foot">
								<div class="bold"><img src="/images/main/login_02.png" alt="login_01" />&nbsp;알려드립니다</div>
								<p class="pad_lf20 mar_tp10">권리자찾기 사이트를 이용하기 위해서는 로그인 하시기 바랍니다.</p>
								<p class="pad_lf20 mar_tp10">관리자는 [관리자 로그인 바로가기]로 이동하여 로그인 하시기 바랍니다.</p>
							</div>
						</div>
<!-- 							<div class="login_admin" style="padding: 15px 0 0 300px">
								<a href="/console/common/loginPage.page">관리자 로그인 바로가기</a>
								<a href="#" onclick="fn_admin_login();">관리자 로그인 바로가기</a>
							</div> -->
						</form>
						</div>
				<!--  --><!--  --><!--  -->
					</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
			</div>
	
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<!-- ???? -->
		<div id="foot_bor">
	<div class="wrap_menu_tail">
		<div class="inner">
			<div class="menu_tail">
				<ul>
					<li><a href="/main/main.do?method=goUserMgnt" class="first">이용약관</a></li>
					<li><a href="http://www.copyright.or.kr/customer-center/user-guide/privacy-policy/index.do" target="_blank">개인정보처리방침</a></li>
					<li><a href="/main/main.do?method=goHomeCopyright">저작권정책 </a></li>
					<!-- <li><a href="/mlsInfo/guideInfo.jsp">관련사이트안내</a></li> -->
					<li><a href="/board/board.do?menuSeqn=1&amp;page_no=1">자주묻는질문</a></li>
				</ul>
			</div>
	
			<form name="linkForm" action="#">
				<input type="submit" style="display:none;" />
				<div class="foot_rt">
					<select title="관련사이트" name="siteLink">
						<option selected="selected">관련사이트</option>
						<option value="http://www.kdce.or.kr/user/main.do">디지털저작권거래소</option>
						<option value="http://www.copyright.or.kr">한국저작권위원회</option>
						<option value="http://www.copyright.or.kr/customer/">저작권 상담센터</option>
						<option value="http://www.copyright.or.kr/customer/cros/main.do">저작권등록</option>
						<option value="http://www.copyright.or.kr/customer/cras/main.do">저작권 인증</option>
						<option value="http://gongu.copyright.or.kr/index.do">공유마당</option>
						<option value="http://www.copyright.or.kr/customer/adr/main.do">분쟁조정</option>
						<option value="http://www.copy112.or.kr/copy112/main.do">불법복제물신고 copy112</option>
						<option value="http://www.copyright.or.kr/education/online-lifetime/main.do">저작권 원격교육연수원</option>
						<option value="http://www.copyright.or.kr/education/academy/main.do">저작권 교육포털</option>
					</select>
					<a onclick="move();"><img src="/images/2017/new/btn_move.png" alt="이동" /></a>
					<select title="유관기관" name="siteLink1">
						<option value="">유관기관</option>
						<option value="http://www.komca.or.kr/">한국음악저작권협회</option>
						<option value="http://www.koscap.or.kr/">함께하는음악저작인협회</option>
						<option value="http://www.fkmp.kr/">한국음악실연자연합회</option>
						<option value="http://www.riak.or.kr/">한국음반산업협회</option>
						<option value="http://www.copyrightkorea.or.kr/">한국문예학술저작권협회</option>
						<option value="http://www.copycle.or.kr/">한국복제전송저작권협회</option>
						<option value="http://www.ktrwa.or.kr/">한국방송작가협회</option>
						<option value="http://www.mdak.or.kr/">한국영화배급협회</option>
						<option value="http://www.scenario.or.kr/">한국시나리오작가협회</option>
						<option value="http://www.kfpa.net/">한국영화제작가협회</option>
						<option value="http://www.kbpa.co.kr/">한국방송실연자연합회</option>
						<option value="http://www.kpf.or.kr/">한국언론진흥재단</option>
					</select>
					<a onclick="move1();"><img src="/images/2017/new/btn_move.png" alt="이동" /></a>
					
				</div>
			</form>
		</div>
	</div>
	<div id="foot">
		<div class="foot_cen">
			<p class="foot_lf">
				<a href="#none"><img src="/images/2017/new/logo_tail.png" alt="logo" /></a>
			</p>
			<div class="txt">
				대표전화 : 055-792-0129<br />
				52852 경상남도 진주시 충의로 19(LH공사 1,3,5층) 한국저작권 위원회<br />
				Copyright ⓒ 2012 FINDCOPYRIGHT. ALL RIGHTS RESERVED.
			</div>
			
		</div>
		<img src="/images/main/main_40.gif" alt="그림" class="icon" />
	</div>
</div>
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
<!-- <script src="/js/jquery-1.7.1.js"  type="text/javascript"></script> -->
<!-- <script type="text/javascript" src="/js/script.js"></script> -->
</body>
</html>
