<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@ page contentType="text/html;charset=euc-kr"%>
<% 
HttpSession s = request.getSession(true);
s.putValue("NmChkSec","98u9iuhuyg87");
%>


<%@ page language="java" import="Kisinfo.Check.IPINClient"%>
<%@ page language="java" import="NiceID.Check.CPClient"%>
<%
//현재 URL 가져오기
String c_url = javax.servlet.http.HttpUtils.getRequestURL(request).toString(); 

//가져온 URL 에서 도메인부분만 가져오기
String temp = c_url.substring(0, c_url.indexOf("/", 8));
	/********************************************************************************************************************************************
	NICE신용평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
	
	서비스명 : 가상주민번호서비스 (안심본인인증) 서비스
	페이지명 : 가상주민번호서비스 (안심본인인증) 호출 페이지
	*********************************************************************************************************************************************/
    NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();
	
	String sSiteCode1 = "G6840";				//안심본인인증		NICE로부터 부여받은 사이트 코드
    String sSitePassword = "91F92CJ77YO7";		//안심본인인증 	NICE로부터 부여받은 사이트 패스워드
    
    String sRequestNumber = "REQ0000000001";        	// 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로 
														// 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.
    
	sRequestNumber = niceCheck.getRequestNO(sSiteCode1);
  	session.setAttribute("REQ_SEQ" , sRequestNumber);	// 해킹등의 방지를 위하여 세션을 쓴다면, 세션에 요청번호를 넣는다.
  	
   	String sAuthType = "";      	// 없으면 기본 선택화면, M: 핸드폰, C: 신용카드, X: 공인인증서
   	
   	String popgubun 	= "N";		//Y : 취소버튼 있음 / N : 취소버튼 없음
		String customize 	= "";	//없으면 기본 웹페이지 / Mobile : 모바일페이지
		
    // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
    String sReturnUrl = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("checkSuccess"));
    //String sReturnUrl = "http://dev.findcopyright.or.kr/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL
	String sErrorUrl = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("checkFail"));
    //String sErrorUrl = "http://dev.findcopyright.or.kr/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL
    
    if(temp.equalsIgnoreCase("http://dev.copyright.or.kr")){
    	  sReturnUrl = "http://dev.copyright.or.kr/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(로컬)
    	  sErrorUrl = "http://dev.copyright.or.kr/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(로컬)
   }else if(temp.equalsIgnoreCase("http://www.copyright.or.kr:8880")){
	   sReturnUrl = "http://www.copyright.or.kr:8880/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(개발)
	   sErrorUrl = "http://www.copyright.or.kr:8880/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(개발)
   }else if(temp.equalsIgnoreCase("http://localhost:8080")){
	   sReturnUrl = "http://localhost:8080/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(개발)
	   sErrorUrl = "http://localhost:8080/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(개발)
   }else if(temp.equalsIgnoreCase("http://local.findcopyright.or.kr:8080")){
	   sReturnUrl = "http://local.findcopyright.or.kr:8080/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(개발)
	   sErrorUrl = "http://local.findcopyright.or.kr:8080/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(개발)
   }

    // 입력될 plain 데이타를 만든다.
    String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
                        "8:SITECODE" + sSiteCode1.getBytes().length + ":" + sSiteCode1 +
                        "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
                        "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
                        "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
                        "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
                        "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize;
    
    String sMessage = "";
    String sEncData1 = "";
    
    int iReturn = niceCheck.fnEncode(sSiteCode1, sSitePassword, sPlainData);
    if( iReturn == 0 )
    {
        sEncData1 = niceCheck.getCipherData();
    }
    else if( iReturn == -1)
    {
        sMessage = "암호화 시스템 에러입니다.";
    }    
    else if( iReturn == -2)
    {
        sMessage = "암호화 처리오류입니다.";
    }    
    else if( iReturn == -3)
    {
        sMessage = "암호화 데이터 오류입니다.";
    }    
    else if( iReturn == -9)
    {
        sMessage = "입력 데이터 오류입니다.";
    }    
    else
    {
        sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
    }
	

%>
<%
	/********************************************************************************************************************************************
		NICE신용평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
		
		서비스명 : 가상주민번호서비스 (IPIN) 서비스
		페이지명 : 가상주민번호서비스 (IPIN) 호출 페이지
	*********************************************************************************************************************************************/
	
	String sSiteCode				= "D746";			// IPIN 서비스 사이트 코드		(NICE신용평가정보에서 발급한 사이트코드)
	String sSitePw					= "Yagins12";			// IPIN 서비스 사이트 패스워드	(NICE신용평가정보에서 발급한 사이트패스워드)
	
	
	/*
	┌ sReturnURL 변수에 대한 설명  ─────────────────────────────────────────────────────
		NICE신용평가정보 팝업에서 인증받은 사용자 정보를 암호화하여 귀사로 리턴합니다.
		따라서 암호화된 결과 데이타를 리턴받으실 URL 정의해 주세요.
		
		* URL 은 http 부터 입력해 주셔야하며, 외부에서도 접속이 유효한 정보여야 합니다.
		* 당사에서 배포해드린 샘플페이지 중, ipin_process.jsp 페이지가 사용자 정보를 리턴받는 예제 페이지입니다.
		
		아래는 URL 예제이며, 귀사의 서비스 도메인과 서버에 업로드 된 샘플페이지 위치에 따라 경로를 설정하시기 바랍니다.
		예 - http://www.test.co.kr/ipin_process.jsp, https://www.test.co.kr/ipin_process.jsp, https://test.co.kr/ipin_process.jsp
	└────────────────────────────────────────────────────────────────────
	*/
	String sReturnURL				= "";
	
	int port = request.getServerPort();
	String domain = request.getServerName();
	
	if(port == 80 || port == 0){
	    sReturnURL = "https://"+domain+"/iPin/ipin_process.jsp";
	}else{
	    sReturnURL = "https://"+domain+":"+port+"/iPin/ipin_process.jsp";
	}
	

	/*
	┌ sCPRequest 변수에 대한 설명  ─────────────────────────────────────────────────────
		[CP 요청번호]로 귀사에서 데이타를 임의로 정의하거나, 당사에서 배포된 모듈로 데이타를 생성할 수 있습니다.
		
		CP 요청번호는 인증 완료 후, 암호화된 결과 데이타에 함께 제공되며
		데이타 위변조 방지 및 특정 사용자가 요청한 것임을 확인하기 위한 목적으로 이용하실 수 있습니다.
		
		따라서 귀사의 프로세스에 응용하여 이용할 수 있는 데이타이기에, 필수값은 아닙니다.
	└────────────────────────────────────────────────────────────────────
	*/
	String sCPRequest				= "";
	
	
	
	// 객체 생성
	IPINClient pClient = new IPINClient();
	
	
	// 앞서 설명드린 바와같이, CP 요청번호는 배포된 모듈을 통해 아래와 같이 생성할 수 있습니다.
	sCPRequest = pClient.getRequestNO(sSiteCode);
	
	// CP 요청번호를 세션에 저장합니다.
	// 현재 예제로 저장한 세션은 ipin_result.jsp 페이지에서 데이타 위변조 방지를 위해 확인하기 위함입니다.
	// 필수사항은 아니며, 보안을 위한 권고사항입니다.
	session.setAttribute("CPREQUEST" , sCPRequest);
	
	
	// Method 결과값(iRtn)에 따라, 프로세스 진행여부를 파악합니다.
	int iRtn = pClient.fnRequest(sSiteCode, sSitePw, sCPRequest, sReturnURL);
	
	String sRtnMsg					= "";			// 처리결과 메세지
	String sEncData					= "";			// 암호화 된 데이타
	
	// Method 결과값에 따른 처리사항
	if (iRtn == 0)
	{
	
		// fnRequest 함수 처리시 업체정보를 암호화한 데이터를 추출합니다.
		// 추출된 암호화된 데이타는 당사 팝업 요청시, 함께 보내주셔야 합니다.
		sEncData = pClient.getCipherData();		//암호화 된 데이타
		
		sRtnMsg = "정상 처리되었습니다.";
	
	}
	else if (iRtn == -1 || iRtn == -2)
	{
		sRtnMsg =	"배포해 드린 서비스 모듈 중, 귀사 서버환경에 맞는 모듈을 이용해 주시기 바랍니다.<BR>" +
					"귀사 서버환경에 맞는 모듈이 없다면 ..<BR><B>iRtn 값, 서버 환경정보를 정확히 확인하여 메일로 요청해 주시기 바랍니다.</B>";
	}
	else if (iRtn == -9)
	{
		sRtnMsg = "입력값 오류 : fnRequest 함수 처리시, 필요한 4개의 파라미터값의 정보를 정확하게 입력해 주시기 바랍니다.";
	}
	else
	{
		sRtnMsg = "iRtn 값 확인 후, NICE신용평가정보 개발 담당자에게 문의해 주세요.";
	}

%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>아이디 찾기 | 회원정보 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css">
<link rel="stylesheet" type="text/css" href="/css/table.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> -->
<!-- <script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script> -->

<script type='text/javascript'>
	window.name ="Parent_window";
	
	//안심본인인증 아이디 찾기 서비스
	function fnPopup2(){
		window.open('', 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
		document.form_chk.param_r1.value = "idntSrch"
		document.form_chk.target = "popupChk";
		document.form_chk.submit();
	}


</script>

<script type="text/JavaScript">

</script>
<script type="text/javascript">

</script>
</head>
<body onLoad="init();">
	<!-- HEADER str-->
	<!-- 2017변경 -->
	<jsp:include page="/include/2017/header.jsp" />
	<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
	<script type="text/javascript">initNavigation(0);</script>
	<!-- GNB setOn 각페이지에 넣어야합니다. -->

	<!-- HEADER end -->

	<!-- CONTAINER str-->
	<div id="contents">

		<!-- 래프 -->
		<div class="con_lf" style="width: 22%">
			<h2><div class="con_lf_big_title">회원정보</div></h2>
			<ul class="sub_lf_menu">
				<li><a href="/user/user.do?method=goLogin">로그인</a></li>
				<li><a href="/user/user.do?method=goPage">회원가입</a></li>
				<li><a href="/user/user.do?method=goIdntSrch" class="on">아이디 찾기</a></li>
				<li><a href="/user/user.do?method=goPswdSrch">비밀번호 찾기</a></li>
			</ul>
		</div>
		<!-- //래프 -->


		<!-- 안심본인인증 아이디찾기 -->
		<form name="frm1" method="post" action="">
			<input type="hidden" name="userDivs" value="01"> <input type="hidden" name="userName" value=""> <input type="hidden" name="dupInfo" value=""> <input type="hidden" name="connInfo" value=""> <input type="hidden" name="birthDate" value=""> <input type="hidden" name="pswdSrch" value="">
		</form>

		<!-- 안심본인인증 비밀번호 찾기 -->
		<form name="frm2" method="post" action="">
			<input type="hidden" name="userDivs" id="userDivs1" value="01"> <input type="hidden" name="requestNumber" value=""> <input type="hidden" name="responseNumber" value=""> <input type="hidden" name="userName" value=""> <input type="hidden" name="dupInfo" value=""> <input type="hidden" name="connInfo" value=""> <input type="hidden" name="birthDate" value=""> <input type="hidden" name="userIdnt_pwd" value=""> <input type="hidden" name="userEmail_pwd" value=""> <input type="hidden" name="userName_pwd" value=""> <input type="hidden" name="userDivs_pwd" value="01"> <input type="hidden" name="pswdSrch" value="Y">
		</form>


		<!-- 안심본인인증 서비스 팝업을 호출하기 위해서는 다음과 같은 form이 필요합니다. -->
		<form name="form_chk" method="post" action="">
			<input type="hidden" name="m" value="checkplusSerivce">
			<!-- 필수 데이타로, 누락하시면 안됩니다. -->
			<input type="hidden" name="EncodeData" value="<%= sEncData1 %>">
			<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->

			<input type="hidden" name="param_r1" value=""> <input type="hidden" name="param_r2" value=""> <input type="hidden" name="param_r3" value="">
		</form>


		<!-- 가상주민번호 서비스 팝업을 호출하기 위해서는 다음과 같은 form이 필요합니다. -->
		<form name="form_ipin" method="post" action="">
			<input type="hidden" name="m" value="pubmain">
			<!-- 필수 데이타로, 누락하시면 안됩니다. -->
			<input type="hidden" name="enc_data" value="<%= sEncData %>">
			<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
			<!-- 업체에서 응답받기 원하는 데이타를 설정하기 위해 사용할 수 있으며, 인증결과 응답시 해당 값을 그대로 송신합니다.
				    	 해당 파라미터는 추가하실 수 없습니다. -->
			<input type="hidden" name="param_r1" value=""> <input type="hidden" name="param_r2" value=""> <input type="hidden" name="param_r3" value="">
		</form>

		<!-- 가상주민번호 서비스 팝업 페이지에서 사용자가 인증을 받으면 암호화된 사용자 정보는 해당 팝업창으로 받게됩니다.
					 따라서 부모 페이지로 이동하기 위해서는 다음과 같은 form이 필요합니다. -->
		<form name="vnoform" method="post" action="">
			<input type="hidden" name="enc_data">
			<!-- 인증받은 사용자 정보 암호화 데이타입니다. -->

			<!-- 업체에서 응답받기 원하는 데이타를 설정하기 위해 사용할 수 있으며, 인증결과 응답시 해당 값을 그대로 송신합니다.
				    	 해당 파라미터는 추가하실 수 없습니다. -->
			<input type="hidden" name="param_r1" value=""> <input type="hidden" name="param_r2" value=""> <input type="hidden" name="param_r3" value="">
		</form>


		<!-- 주요컨텐츠 str -->
		<div class="con_rt" id="contentBody">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" /> &gt; 회원정보 &gt; <span class="bold">아이디 찾기</span>
			</div>
			<h1><div class="con_rt_hd_title">아이디 찾기</div></h1>
			<div class="sub_contents_con">

				<form class="frm" name="form1" action="#">
					<input type="hidden" name="resdCorpNumb"> <input type="hidden" name="corpNumb"> <input type="hidden" name="userDivs"> <input type="hidden" name="personDivs"> <input type="hidden" name="userName"> <input type="hidden" name="resdCorpNumb1"> <input type="hidden" name="resdCorpNumb2"> <input type="hidden" name="corpNumb1"> <input type="hidden" name="corpNumb2"> <input type="hidden" name="corpNumb3"> <input type="hidden" name="mgnbYsno"> <input type="hidden" name="commName"> <input type="hidden" name="mail"> <input type="hidden" name="pswdSrch" value=""> <input type="submit" style="display: none;"> <br />
					<h2 class="sub_con_h2">아이디찾기</h2>

					<!-- 아이디찾기>안심본인인증 -->
					<div id="box01_01">
						<ul class="sub_menu1 w201 mar_tp40">
							<li class="on"><a href="#1" class="tab01 on" id="a01" title="개인회원 선택됨">개인회원</a></li>
							<li><a href="#1" class="tab01" id="a02" title="법인사업자">법인사업자</a></li>
							<li><a href="#1" class="tab01" id="a03" title="개인사업자">개인사업자</a></li>
							<li><a href="#1" class="last_rt_bor tab01" id="a04" title="관리자">관리자</a></li>
						</ul>
						<p class="clear"></p>
					<!-- 	<div class="mar_tp30">
							<label><input type="radio" id="pDivs04" name="pDivs" value="p01" style="vertical-align: -2px;" /> 안심본인인증</label> 
							<label><input type="radio" id="pDivs05" name="pDivs" value="p02" style="vertical-align: -2px;" /> 아이핀(I-Pin)</label>
						</div> -->
						<div class="login_bg mar_tp20">
							<div class="float_lf pad_lf20">
								<img src="/images/main/login_01.png" alt="" />
							</div>
							<div class="float_lf mar_lf50 mar_tp20">
								<div class="bold">
									 통합회원 로그인으로 인해 통합 <br />본인인증시 제공되는 정보는 해당 인증기관에서 직접 수집하며, 인증 이외의<br />용도로 이용 또는 저장하지 않습니다.
									
								</div>
								<div class="align_cen mar_tp20">
									<!-- <a href="javascript:fnPopup2();" class="pop_check">인증하기</a> -->
									<input type="button" name="userName01" onclick="javascript:fnPopup2();" class="pop_check" style="border: 0px; cursor: pointer;" value="인증하기" title="안심본인인증 새창"/>
								</div>
							</div>
							<p class="clear"></p>
							<div class="mar_tp30" style="border-top: 1px solid #dddddd;"></div>
							<div class="login_foot">
								<div class="bold">
									<img src="/images/main/login_02.png" alt="" />&nbsp;알려드립니다.
								</div>
								<p class="pad_lf20 mar_tp10">회원가입시 입력하신 가입정보를 통해 아이디를 찾으실 수 있습니다.</p>
							</div>
						</div>
					</div>
					<!-- //아이디찾기>안심본인인증 -->

					<!-- 아이디찾기>아이핀인증 -->
					<div id="box01_02" style="display: none;">
						<ul class="sub_menu1 w201 mar_tp40">
							<li class="on"><a href="#1" class="tab01 on" id="a05" title="개인회원 선택됨">개인회원</a></li>
							<li><a href="#1" class="tab01" id="a06" title="법인사업자">법인사업자</a></li>
							<li><a href="#1" class="tab01" id="a07" title="개인사업자">개인사업자</a></li>
							<li><a href="#1" class="last_rt_bor tab01" id="a08" title="관리자">관리자</a></li>
						</ul>
						<p class="clear"></p>
						<div class="mar_tp30">
							<label><input type="radio" id="pDivs06" name="pDivs" value="p01" style="vertical-align: -2px;" /> 안심본인인증</label> 
							<label><input type="radio" id="pDivs07" name="pDivs" value="p02" style="vertical-align: -2px;" /> 아이핀(I-Pin)</label>
						</div>
						<div class="login_bg mar_tp20">
							<div class="float_lf pad_lf20">
								<img src="/images/main/login_01.png" alt="" />
							</div>
							<div class="float_lf mar_lf50 mar_tp20">
								<div class="bold">
									아이핀(I-Pin)인증을 통해 아이디를 찾으실 수 있습니다. <br />본인인증시 제공되는 정보는 해당 인증기관에서 직접 수집하며, 인증 이외의 <br />용도로 이용 또는 저장하지 않습니다.
								</div>
								<div class="align_cen mar_tp20">
									<!-- <a href="javascript:fnPopup();" class="pop_check">인증하기</a> -->
									<input type="button" name="userName06" onclick="javascript:fnPopup();" class="pop_check" style="border: 0px; cursor: pointer;" value="인증하기" title="아이핀(i-Pin)인증 새창"/>
								</div>
							</div>
							<p class="clear"></p>
							<div class="mar_tp30" style="border-top: 1px solid #dddddd;"></div>
							<div class="login_foot">
								<div class="bold">
									<img src="/images/main/login_02.png" alt="" />&nbsp;알려드립니다.
								</div>
								<p class="pad_lf20 mar_tp10">회원가입시 입력하신 가입정보를 통해 아이디를 찾으실 수 있습니다.</p>
							</div>
						</div>
					</div>
					<!-- //아이디찾기>아이핀인증 -->

					<!-- 법인사업자 -->
					<div id="box02_01" style="display: none;">
						<ul class="sub_menu1 w201 mar_tp40">
							<li><a href="#1" class="tab01" id="a09" title="개인회원">개인회원</a></li>
							<li class="on"><a href="#1" class="tab01 on" id="a10" title="법인사업자 선택됨">법인사업자</a></li>
							<li><a href="#1" class="tab01" id="a11" title="개인사업자">개인사업자</a></li>
							<li><a href="#1" class="last_rt_bor tab01" id="a12" title="관리자">관리자</a></li>
						</ul>
						<p class="clear"></p>
						<div class="login_bg mar_tp30">
							<div class="float_lf pad_lf20">
								<img src="/images/main/login_01.png" alt="" />
							</div>
							<div class="login_rt_tp">
								<div class="login_rt_tp_lf pad_lf20">
									<div class="login_rt_tp_lf_lf">
										<div class="login_word">법인명</div>
										<div class="login_input">
											<input type="text" title="법인명" id="userName02" name="userName02" />
										</div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_tp_lf_rt">
										<div class="login_word">법인번호</div>
										<div class="login_input">
											<input type="text" id="resdCorpNumb102" name="resdCorpNumb102" title="법인번호(앞자리)" size="7" maxlength="6" onkeypress="cfInputNumRT(event);" style="width: 27%;" /> - <input type="text" id="resdCorpNumb202" name="resdCorpNumb202" title="법인번호(뒷자리)" size="19" maxlength="7" onkeypress="cfInputNumRT(event);" style="width: 60%;" />
										</div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_tp_lf_rt mar_tp10">
										<div class="login_word">사업자번호</div>
										<div class="login_input">
											<input type="text" id="corpNumb102" name="corpNumb102" title="사업자번호(1번째)" size="7" maxlength="3" onkeypress="cfInputNumRT(event);" style="width: 27%;" /> - <input type="text" id="corpNumb202" name="corpNumb202" title="사업자번호(2번째)" size="7" maxlength="2" onkeypress="cfInputNumRT(event);" style="width: 26%;" /> - <input type="text" id="corpNumb302" name="corpNumb302" title="사업자번호(3번째)" size="9" maxlength="5" onkeypress="cfInputNumRT(event);" style="width: 26%;" />
										</div>
										<p class="clear"></p>
									</div>
								</div>
								<div class="login_rt_tp_rt mar_tp30">
									<a href="#1" onclick="javascript:fn_userIdntSrch();" class="login_bg_2c65aa">확 인</a>
								</div>
								<p class="clear"></p>
							</div>
							<p class="clear"></p>
							<div class="mar_tp30" style="border-top: 1px solid #dddddd;"></div>
							<div class="login_foot">
								<div class="bold">
									<img src="/images/main/login_02.png" alt="" />&nbsp;알려드립니다.
								</div>
								<p class="pad_lf20 mar_tp10">회원가입시 입력하신 가입정보를 통해 아이디를 찾으실 수 있습니다.</p>
							</div>
						</div>
					</div>
					<!-- //법인사업자 -->

					<!-- 개인사업자 -->
					<div id="box03_01" style="display: none;">
						<ul class="sub_menu1 w201 mar_tp40">
							<li><a href="#1" class="tab01" id="a13" title="개인회원">개인회원</a></li>
							<li><a href="#1" class="tab01" id="a14" title="법인사업자">법인사업자</a></li>
							<li class="on"><a href="#1" class="tab01 on" id="a15" title="개인사업자 선택됨">개인사업자</a></li>
							<li><a href="#1" class="last_rt_bor tab01" id="a16" title="관리자">관리자</a></li>
						</ul>
						<p class="clear"></p>
						<div class="login_bg mar_tp30">
							<div class="float_lf pad_lf20">
								<img src="/images/main/login_01.png" alt="" />
							</div>
							<div class="login_rt_tp">
								<div class="login_rt_tp_lf pad_lf20">
									<div class="login_rt_tp_lf_lf">
										<div class="login_word">이름</div>
										<div class="login_input">
											<input type="text" id="userName03" name="userName03" title="이름" />
										</div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_tp_lf_rt mar_tp10">
										<div class="login_word">사업자번호</div>
										<div class="login_input">
											<input type="text" id="corpNumb103" name="corpNumb103" title="사업자번호(1번째)" size="7" maxlength="3" onkeypress="cfInputNumRT(event);" style="width: 27%;" /> - <input type="text" id="corpNumb203" name="corpNumb203" title="사업자번호(2번째)" size="7" maxlength="2" onkeypress="cfInputNumRT(event);" style="width: 26%;" /> - <input type="text" id="corpNumb303" name="corpNumb303" title="사업자번호(3번째)" size="9" maxlength="5" onkeypress="cfInputNumRT(event);" style="width: 25.5%;" />
										</div>
										<p class="clear"></p>
									</div>
								</div>
								<div class="login_rt_tp_rt">
									<a href="#1" onclick="javascript:fn_userIdntSrch();" class="login_bg_2c65aa">확 인</a>
								</div>
								<p class="clear"></p>
							</div>
							<p class="clear"></p>
							<div class="mar_tp30" style="border-top: 1px solid #dddddd;"></div>
							<div class="login_foot">
								<div class="bold">
									<img src="/images/main/login_02.png" alt="" />&nbsp;알려드립니다.
								</div>
								<p class="pad_lf20 mar_tp10">회원가입시 입력하신 가입정보를 통해 아이디를 찾으실 수 있습니다.</p>
							</div>
						</div>
					</div>
					<!-- //개인사업자 -->


					<!-- 관리자 -->
					<div id="box04_01" style="display: none;">
						<ul class="sub_menu1 w201 mar_tp40">
							<li><a href="#1" class="tab01" id="a17" title="개인회원">개인회원</a></li>
							<li><a href="#1" class="tab01" id="a18" title="법인사업자">법인사업자</a></li>
							<li><a href="#1" class="tab01" id="a19" title="개인사업자">개인사업자</a></li>
							<li class="on"><a href="#1" class="last_rt_bor tab01 on" id="a20" title="관리자 선택됨">관리자</a></li>
						</ul>
						<p class="clear"></p>
						<div class="login_bg mar_tp30">
							<div class="float_lf pad_lf20">
								<img src="/images/main/login_01.png" alt="" />
							</div>
							<div class="login_rt_tp">
								<div class="login_rt_tp_lf pad_lf20">
									<div class="login_rt_tp_lf_lf">
										<div class="login_word" style="width: 30%;">이름</div>
										<div class="login_input" style="width: 68%;">
											<input type="text" id="userName04" name="userName04" title="이름" />
										</div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_tp_lf_lf">
										<div class="login_word" style="width: 30%;">단체 및 기관명</div>
										<div class="login_input" style="width: 68%;">
											<input type="text" id="commName04" name="commName04" title="단체 및 기관명" />
										</div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_tp_lf_lf">
										<div class="login_word" style="width: 30%;">이메일</div>
										<div class="login_input" style="width: 68%;">
											<input type="text" id="mail04" name="mail04" title="이메일" />
										</div>
										<p class="clear"></p>
									</div>
								</div>
								<div class="login_rt_tp_rt mar_tp30">
									<a href="#1" onclick="javascript:fn_userIdntSrch();" class="login_bg_2c65aa">확 인</a>
								</div>
								<p class="clear"></p>
							</div>
							<p class="clear"></p>
							<div class="mar_tp30" style="border-top: 1px solid #dddddd;"></div>
							<div class="login_foot">
								<div class="bold">
									<img src="/images/main/login_02.png" alt="" />&nbsp;알려드립니다.
								</div>
								<p class="pad_lf20 mar_tp10">회원가입시 입력하신 가입정보를 통해 아이디를 찾으실 수 있습니다.</p>
							</div>
						</div>
					</div>
					<!-- //관리자 -->
				</form>


			</div>
		</div>
		<!-- //주요컨텐츠 end -->
		<p class="clear"></p>
	</div>
	<!-- //CONTAINER end -->

	<!-- FOOTER str-->
	<!-- 2017변경 -->
	<jsp:include page="/include/2017/footer.jsp" />
	<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
	<!-- FOOTER end -->


</body>
</html>
