<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(저작권찾기) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2010/calendar.css">
<style type="text/css">
<!--
body {
	overflow-x: auto;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: hidden;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>

<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript">
<!--



window.name = "rghtPrpsDetl_etc";

// 처리결과 상세 팝업오픈
function openDetlDesc(mastKey, code, crId, nrId, meId, trstCode)  {
	var param = '';
	
	param += '&PRPS_MAST_KEY='+mastKey;
	param += '&PRPS_IDNT_CODE='+code;
	param += '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	param += '&TRST_ORGN_CODE='+trstCode;
	
	var url = '/rsltInqr/rsltInqr.do?method=rghtPrpsRsltDetl&DIVS=I'+param
	var name = '';
	var openInfo = 'target=rghtPrpsDetl_book, width=500, height=360, scrollbars=yes';
	window.open(url, name, openInfo);
}
// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	
	
	var the_height = document.getElementById(name).offsetHeight;
	var chkObjs = document.getElementsByName("chk");
	  
	document.getElementById(targetName).style.height = the_height + 13 + "px" ;
	  

}

// 스크롤셋팅
function scrollSet(name, targetName, oRowName){
	
	var totalRow = document.getElementsByName(oRowName).length;
		
	// 세로 기준건수 이상인 경우 세로스크롤 제어
	if( totalRow < 6) {
		resizeDiv(name, targetName);
		document.getElementById(targetName).style.overflowY = "hidden";
	}
	
	if( totalRow > 5 ) {
		document.getElementById(targetName).style.overflowY = "auto";
	}
}


// 토탈건수 set
function setTotCnt( oRowName, oSpanId) {

	var totalRow = document.getElementsByName(oRowName).length;
	document.getElementById(oSpanId).innerHTML = "("+totalRow+"건)";
}

// 목록 
function goList(){
	
	var frm = document.srchForm;
	frm.action = '/rsltInqr/rsltInqr.do?DIVS=10&page_no=1';
	frm.method="post";
	frm.submit();
}

// 수정화면으로 이동
function goModi(prpsMastKey, dealStatFlag){
	
	if(dealStatFlag!='0')
	{
		alert("상세처리상태가 '신청'이상 진행된경우는 수정이 불가능합니다."); return;
	}
	
	// 수정가능 URL
	url = "/rsltInqr/rsltInqr.do?method=isValidDealStat&actFlagYn=Y&prpsMastKey=" + prpsMastKey;
	
	if(cfGetBooleanResponseReload(url) == false)
	{
		alert("상세처리상태가 '신청'이상 진행된경우는 수정이 불가능합니다.");
		return;
	}
	else
	{
		var frm = document.prpsForm;
		
		frm.PRPS_MAST_KEY.value = prpsMastKey;
		
		frm.method = "post";
		frm.action = "/rsltInqr/rsltInqr.do?method=rghtPrpsModi&DIVS=X";
		frm.submit();		
	}
}

// 삭제
function goDelt(prpsMastKey, dealStatFlag){
	
	if(dealStatFlag!='0')
	{
		alert("상세처리상태가 '신청'이상 진행된경우는 삭제가 불가능합니다."); return;
	}
	
	if( confirm("삭제 하시겠습니까?") )
	{
		// 삭제가능 URL
		url = "/rsltInqr/rsltInqr.do?method=isValidDealStat&actFlagYn=Y&prpsMastKey=" + prpsMastKey;
		
		if(cfGetBooleanResponseReload(url) == false)
		{
			alert("상세처리상태가 '신청'이상 진행된경우는 삭제가 불가능합니다.");
			return;
		}
		else
		{
			var frm = document.prpsForm;
			
			frm.PRPS_MAST_KEY.value = prpsMastKey;
			
			frm.method = "post";
			frm.action = "/rsltInqr/rsltInqr.do?method=deleteRslt&DIVS=10&page_no=1";
			frm.submit();		
		}
	}
}

// 파일다운로드
	function fn_fileDownLoad(filePath, fileName, realFileName) {
		
		var frm = document.prpsForm;
		
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.target="rghtPrpsDetl_image";
		//frm.action = "/rsltInqr/rsltInqr.do?method=fileDownLoad";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.method="post";
		frm.submit();
  }
  
// 출력물 
function fn_report(mastKey, report){

	var frm = document.prpsForm;
	//frm.mode.value = 'R';
	
	var sUrl = "/rsltInqr/rsltInqr.do?method=rghtPrpsDetl";

	var param = "&DIVS=X";
	     param += "&PRPS_MAST_KEY="+mastKey;
	     param += "&mode=R";
	     param += "&report="+report;
	 
	sUrl += param;
	window.open(sUrl,"PRINT","toolbar=no,menubar=no,resizable=no,status=yes");
}

function initParameter(){
 	// 토탈건수
 	setTotCnt( "iChk", "totalRow");
	// scrollSet("listTab", "div_scroll_1", "iChk");
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 

//-->

</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_6.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
				<!-- 래프 -->
				<jsp:include page="/include/2012/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1","lnb11");
				</script>
				<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>신청현황</span><em>저작권정보 변경신청</em></p>
					<h1><img src="/images/2012/title/content_h1_0601.gif" alt="저작권정보 변경신청" title="저작권정보 변경신청" /></h1>
					
					<div class="section">
					
						<!-- memo 삽입 -->
						<jsp:include page="/common/memo/2011/memo_01.jsp">
							<jsp:param name="DIVS" value="${DIVS}"/>
						</jsp:include>
					<!-- // memo 삽입 -->
						
						<!-- article str -->
						<div class="article mt20">
						<form name="prpsForm" action="#">
							<input type="hidden" name="PRPS_MAST_KEY" />
							<!-- 파일다운로드 -->
							<input type="hidden" name="filePath" />
							<input type="hidden" name="fileName" />
							<input type="hidden" name="realFileName" />
							<input type="submit" style="display:none;">
							<div class="floatDiv">
								<h2 class="fl">기타저작권정보 변경신청 결과 상세</h2>
								<p class="fr"><span class="button small icon"><a href="#1" onclick="javascript:fn_report('${rghtPrps.PRPS_MAST_KEY}', 'report/rghtPrps_etc');">신청서 출력</a><span class="print" onclick="javascript:fn_report('${rghtPrps.PRPS_MAST_KEY}', 'report/rghtPrps_etc');"></span></span></p>
							</div>
							
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">신청목적</th>
										<td>${rghtPrps.PRPS_RGHT_CODE_VALUE}</td>
									</tr>
									<tr>
										<th scope="row">신청인정보</th>
										<td>
											<span class="topLine2"></span>
											<!-- 그리드스타일 -->
											<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
												<colgroup>
												<col width="15%">
												<col width="30%">
												<col width="25%">
												<col width="*">
												</colgroup>
												<tbody>
													<tr>
														<th scope="row">성명</th>
														<td>${rghtPrps.USER_NAME}</td>
														<th scope="row">주민등록번호/사업자번호</th>
														<td>${rghtPrps.RESD_CORP_NUMB_VIEW}</td>
													</tr>
													<tr>
														<th scope="row">전화번호</th>
														<td>
															<ul class="list1">
															<li class="p11">자택 : ${rghtPrps.HOME_TELX_NUMB}</li>
															<li class="p11">사무실 : ${rghtPrps.BUSI_TELX_NUMB}</li>
															<li class="p11">휴대폰 : ${rghtPrps.MOBL_PHON}</li>
															</ul>
														</td>
														<th scope="row">팩스번호</th>
														<td>${rghtPrps.FAXX_NUMB}</td>
													</tr>
													<tr>
														<th scope="row">이메일주소</th>
														<td colspan="3">${rghtPrps.MAIL}</td>
													</tr>
													<tr>
														<th scope="row">주소</th>
														<td colspan="3">
															<ul class="list1">
															<li class="p11">자택 : ${rghtPrps.HOME_ADDR}</li>
															<li class="p11">사무실 : ${rghtPrps.BUSI_ADDR}</li>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
											<!-- //그리드스타일 -->
										</td>
									</tr>
									<tr>
										<th scope="row">신청저작물장르</th>
										<td>
											<c:choose>
												<c:when test="${rghtPrps.PRPS_SIDE_GENRE == 1}">연극</c:when>
												<c:when test="${rghtPrps.PRPS_SIDE_GENRE == 2}">건축</c:when>
												<c:when test="${rghtPrps.PRPS_SIDE_GENRE == 3}">도형</c:when>
												<c:when test="${rghtPrps.PRPS_SIDE_GENRE == 4}">컴퓨터프로그램</c:when>
												<c:when test="${rghtPrps.PRPS_SIDE_GENRE == 5}">편집</c:when>
												<c:when test="${rghtPrps.PRPS_SIDE_GENRE == 7}">뉴스</c:when>
												<c:when test="${rghtPrps.PRPS_SIDE_GENRE == 6}">공공콘텐츠</c:when>
											</c:choose>
											<!--  -->
											<input type="hidden" name="PRPS_SIDE_GENRE" value='${rghtPrps.PRPS_SIDE_GENRE}'/>
										</td>
									</tr>
									<tr>
										<th scope="row">권리구분</th>
										<td>
											<ul class="line22">
											<c:if test="${rghtPrps.PRPS_SIDE_GENRE == 6 }">
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_216" value="216" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_216 == '1'}">checked="checked"</c:if> title="저작권(한국문화콘텐츠진흥원)" /><label for="CHK_216" class="p12">저작권(한국문화콘텐츠진흥원)</label></li>
											</c:if>
											<c:if test="${rghtPrps.PRPS_SIDE_GENRE == 7 }">
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_215" value="215" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_215 == '1'}">checked="checked"</c:if> title="저작권(한국언론진흥재단)" /><label for="CHK_215" class="p12">저작권(한국언론진흥재단)</label></li>
											</c:if>
											<c:if test="${rghtPrps.PRPS_SIDE_GENRE != 6 && rghtPrps.PRPS_SIDE_GENRE != 7 }">
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_201" value="201" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_201 == '1'}">checked="checked"</c:if> title="저작권(한국언론진흥재단)" /><label for="CHK_201" class="p12">저작권(한국언론진흥재단)</label></li>
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_202" value="202" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_202 == '1'}">checked="checked"</c:if> title="저작권자(한국음악실연자연합회)" /><label for="CHK_202" class="p12">저작권자(한국음악실연자연합회)</label></li>
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_203" value="203" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_203 == '1'}">checked="checked"</c:if> title="저작권자(한국음반산업협회)" /><label for="CHK_203" class="p12">저작권자(한국음반산업협회)</label></li>
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_204" value="204" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_204 == '1'}">checked="checked"</c:if> title="저작권자(한국문예학술저작권협회)" /><label for="CHK_204" class="p12">저작권자(한국문예학술저작권협회)</label></li>
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_205" value="205" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_205 == '1'}">checked="checked"</c:if> title="저작권자(한국복사전송권협회)" /><label for="CHK_205" class="p12">저작권자(한국복사전송권협회)</label></li>
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_206" value="206" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_206 == '1'}">checked="checked"</c:if> title="저작권자(한국방송작가협회)" /><label for="CHK_206" class="p12">저작권자(한국방송작가협회)</label></li>
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_211" value="211" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_211 == '1'}">checked="checked"</c:if> title="저작권자(한국영상산업협회)" /><label for="CHK_211" class="p12">저작권자(한국영상산업협회)</label></li>
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_212" value="212" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_212 == '1'}">checked="checked"</c:if> title="저작권자(한국시나리오작가협회)" /><label for="CHK_212" class="p12">저작권자(한국시나리오작가협회)</label></li>
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_213" value="213" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_213 == '1'}">checked="checked"</c:if> title="저작권자(한국영화제작자협회)" /><label for="CHK_213" class="p12">저작권자(한국영화제작자협회)</label></li>
												<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_214" value="214" class="inputRChk" onclick="javascript:return(false);" <c:if test="${rghtPrps.CHK_214 == '1'}">checked="checked"</c:if> title="저작권자(한국방송실연자연합회)" /><label for="CHK_214" class="p12">저작권자(한국방송실연자연합회)</label></li>
											</c:if>
											</ul>
										</td>
									</tr>
									<tr>
										<th scope="row">신청저작물정보<span id="totalRow"></th>
										<td>
											<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1' || rghtPrps.PRPS_RGHT_CODE == '2'}">
												<div id="div_scroll_1" class="div_scroll" style="padding:0 0 0 0;">
												<table id="listTab" cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="권리자 저작권찾기 신청저작물정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="8%">
													<col width="*">
													<col width="15%">
													<col width="12%">
													<col width="14%">
													<col width="15%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">순번</th>
															<th scope="col">저작물명</th>
															<th scope="col">분야</th>
															<th scope="col">공표매체</th>
															<th scope="col">공표일자</th>
															<th scope="col">저작권자</th>
														</tr>
													</thead>
													<tbody>
													<c:if test="${!empty workList}">
														<c:forEach items="${workList}" var="workList">
															<c:set var="NO" value="${workList.totalRow}"/>
															<c:set var="i" value="${i+1}"/>
														<tr>
															<td class="ce">${i}</td>
															<td>${workList.WORK_NAME }</td>
															<td class="ce">
															<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '1'}">
																<c:choose>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 1}">무용</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 2}">발레</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 3}">무언극</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 4}">뮤지컬</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 5}">오페라</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 6}">마당극</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 7}">즉흥극</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 8}">창극</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																</c:choose>
															</c:if>
															<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '2'}">
																<c:choose>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 1}">건축물</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 2}">건축설계서</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 3}">건축물모형</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																</c:choose>
															</c:if>
															<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '3'}">
																<c:choose>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 1}">(통수목적)지도</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 2}">도표</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 3}">설계도(건촉설계도 제외)</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 4}">모형</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 5}">지구의</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 6}">약도</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																</c:choose>
															</c:if>
															<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '4'}">
																<c:choose>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 1}">사무관리</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 2}">과학기술</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 3}">교육</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 4}">오락</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 5}">기업관리</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 6}">콘텐츠개발SW</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 7}">프로그램SW</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 8}">산업용SW</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 9}">서체글꼴SW</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 10}">제어프로그램</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 11}">언어처리</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 12}">유틸리티</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 13}">데이터통신</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 14}">테이터베이스</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 15}">보안SW</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 16}">미들웨어</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																</c:choose>
															</c:if>
															<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '5'}">
																<c:choose>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 1}">사전</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 2}">홈페이지</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 3}">문학전집</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 4}">시집</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 5}">신문</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 6}">잡지</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 7}">악보집</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 8}">논문집</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 9}">백과사전</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 10}">교육교재</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 11}">카탈로그</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 12}">단어집</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 13}">문제집</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 14}">설문지</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 15}">인명부</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 16}">전단</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 17}">데이터베이스</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																</c:choose>
															</c:if>
															<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '6' || rghtPrps.PRPS_SIDE_GENRE == '7'}">
																<c:choose>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																</c:choose>
															</c:if>
															</td>
															<td class="ce">
																<c:choose>
																	<c:when test="${workList.OPEN_MEDI_CODE == 1}">출판사명</c:when>
																	<c:when test="${workList.OPEN_MEDI_CODE == 2}">배포대상</c:when>
																	<c:when test="${workList.OPEN_MEDI_CODE == 3}">인터넷주소</c:when>
																	<c:when test="${workList.OPEN_MEDI_CODE == 4}">공연장소</c:when>
																	<c:when test="${workList.OPEN_MEDI_CODE == 5}">방송사 및 프로그램명</c:when>
																	<c:when test="${workList.OPEN_MEDI_CODE == 6}">기타</c:when>
																</c:choose>
																<c:if test="${workList.OPEN_MEDI_TEXT != null}">/</c:if>${workList.OPEN_MEDI_TEXT }
															</td>
															<td class="ce">${workList.OPEN_DATE }</td>
															<td class="ce">
																${workList.COPT_HODR }
																<input type="hidden" name="iChk" />
															</td>
														</tr>
														</c:forEach>
													</c:if>
													</tbody>
												</table>
												</div>
											</c:if>
											<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3' || rghtPrps.PRPS_RGHT_CODE == null}">
												<div id="div_scroll_1" class="div_scroll" style="padding:0 0 0 0;">
												<table id="listTab" cellspacing="0" cellpadding="0" border="1" class="grid" summary="이용자 저작권조회 신청저작물정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="8%">
													<col width="*">
													<col width="18%">
													<col width="15%">
													<col width="15%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">순번</th>
															<th scope="col">저작물명</th>
															<th scope="col">분야</th>
															<th scope="col">공표매체</th>
															<th scope="col">공표일자</th>
														</tr>
													</thead>
													<tbody>
													<c:if test="${!empty workList}">
														<c:forEach items="${workList}" var="workList">
															<c:set var="NO" value="${workList.totalRow}"/>
															<c:set var="i" value="${i+1}"/>
														<tr>
															<th class="ce">${i}</th>
															<td>${workList.WORK_NAME }</td>
															<td class="ce">
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '1'}">
																<c:choose>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 1}">무용</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 2}">발레</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 3}">무언극</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 4}">뮤지컬</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 5}">오페라</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 6}">마당극</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 7}">즉흥극</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 8}">창극</c:when>
																	<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																</c:choose>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '2'}">
																	<c:choose>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 1}">건축물</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 2}">건축설계서</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 3}">건축물모형</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																	</c:choose>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '3'}">
																	<c:choose>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 1}">(통수목적)지도</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 2}">도표</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 3}">설계도(건촉설계도 제외)</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 4}">모형</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 5}">지구의</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 6}">약도</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																	</c:choose>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '4'}">
																	<c:choose>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 1}">사무관리</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 2}">과학기술</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 3}">교육</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 4}">오락</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 5}">기업관리</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 6}">콘텐츠개발SW</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 7}">프로그램SW</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 8}">산업용SW</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 9}">서체글꼴SW</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 10}">제어프로그램</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 11}">언어처리</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 12}">유틸리티</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 13}">데이터통신</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 14}">테이터베이스</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 15}">보안SW</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 16}">미들웨어</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																	</c:choose>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '5'}">
																	<c:choose>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 1}">사전</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 2}">홈페이지</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 3}">문학전집</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 4}">시집</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 5}">신문</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 6}">잡지</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 7}">악보집</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 8}">논문집</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 9}">백과사전</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 10}">교육교재</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 11}">카탈로그</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 12}">단어집</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 13}">문제집</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 14}">설문지</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 15}">인명부</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 16}">전단</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 17}">데이터베이스</c:when>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																	</c:choose>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '6' || rghtPrps.PRPS_SIDE_GENRE == '7'}">
																	<c:choose>
																		<c:when test="${workList.PRPS_SIDE_R_GENRE == 99}">기타</c:when>
																	</c:choose>
																</c:if>
															</td>
															<td class="ce">
																<c:choose>
																	<c:when test="${workList.OPEN_MEDI_CODE == 1}">출판사명</c:when>
																	<c:when test="${workList.OPEN_MEDI_CODE == 2}">배포대상</c:when>
																	<c:when test="${workList.OPEN_MEDI_CODE == 3}">인터넷주소</c:when>
																	<c:when test="${workList.OPEN_MEDI_CODE == 4}">공연장소</c:when>
																	<c:when test="${workList.OPEN_MEDI_CODE == 5}">방송사 및 프로그램명</c:when>
																	<c:when test="${workList.OPEN_MEDI_CODE == 6}">기타</c:when>
																</c:choose>
																<c:if test="${workList.OPEN_MEDI_TEXT != null}">/</c:if>${workList.OPEN_MEDI_TEXT }
															</td>
															<td class="ce">
																${workList.OPEN_DATE }
																<input type="hidden" name="iChk" />
															</td>
														</tr>
														</c:forEach>
													</c:if>
													</tbody>
												</table>
												</div>
											</c:if>
										</td>
									</tr>
									<tr>
										<th scope="row" class="bgbr lft">신청저작물설명</th>
										<td>
											<%-- <textarea cols="10" rows="7" class="w99" readonly="readonly" title="신청저작물설명">${rghtPrps.PRPS_RGHT_DESC}</textarea> --%>
											<% pageContext.setAttribute("line", "\n"); %>
 											${fn:replace(rghtPrps.PRPS_RGHT_DESC, line, '<br/>')}
										</td>
									</tr>
									<tr>
										<th scope="row">신청내용</th>
										<td><%-- <textarea rows="7" cols="10" class="w99" readonly="readonly" title="신청내용">${rghtPrps.PRPS_DESC}</textarea> --%>
												<% pageContext.setAttribute("line", "\n"); %>
 												${fn:replace(rghtPrps.PRPS_DESC, line, '<br/>')}
										</td>
									</tr>
									<tr>
										<th scope="row">첨부파일<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">(증빙서류)</c:if></th>
										<td>
											<c:if test="${!empty fileList}">
											<c:set var="fileCnt" value="0" />
												<c:forEach items="${fileList}" var="fileList">
													<c:set var="fileCnt" value="${fileCnt+1}" />
													<a href="#1" onclick="javascript:fn_fileDownLoad('${fileList.FILE_PATH}','${fileList.FILE_NAME}','${fileList.REAL_FILE_NAME}')">${fileList.FILE_NAME}</a><br>
												</c:forEach>
											</c:if>
											<c:if test="${fileCnt < 1}">
												파일이 없습니다.
											</c:if>
											<c:if test="${empty fileList}">
												파일이 없습니다.
											</c:if>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<h2 class="mt20">저작권정보 변경신청 처리 결과</h2>
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="저작권찾기 신청정보 처리결과의 저작물명, 신착관리단체, 처리상태 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="*">
								<col width="35%">
								<col width="20%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">저작물명</th>
										<th scope="col">신탁관리단체</th>
										<th scope="col">처리상태</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${!empty rsltList}">
									<c:forEach items="${rsltList}" var="rsltList">
										<c:set var="kk" value="${kk+1}"/>
										<c:set var="NO" value="${rsltList.totalRow}"/>
										
									<tr>
										<c:if test="${rsltList.TITLE!=null}">
										<th rowspan="${rsltList.CNT}" class="bgbr lft">${rsltList.TITLE }</th>
										</c:if>
										<td class="ce">${rsltList.TRST_ORGN_CODE_VALUE }</td>
										<td class="ce">
											<c:if test="${rsltList.DEAL_STAT == '4'}">
											<span class="blue" style="cursor:hand" onclick="javascript:openDetlDesc('${rsltList.PRPS_MAST_KEY }', '${rsltList.PRPS_IDNT_CODE }', '${rsltList.PRPS_IDNT }', '${rsltList.PRPS_IDNT_NR }', '${rsltList.PRPS_IDNT_ME }', '${rsltList.TRST_ORGN_CODE }');"><u>${rsltList.DEAL_STAT_VALUE}</u></span>
											</c:if>
											<c:if test="${rsltList.DEAL_STAT != '4'}">
											${rsltList.DEAL_STAT_VALUE}
											</c:if>
										</td>
									</tr>
									</c:forEach>
								</c:if>
								</tbody>
							</table>
							
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:goList();">목록</a></span></p>
								<c:if test="${rghtPrps.DEAL_STAT_FLAG == 0}">
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:goModi('${rghtPrps.PRPS_MAST_KEY}','${rghtPrps.DEAL_STAT_FLAG}');">수정하기</a></span>
								<span class="button medium"><a href="#1" onclick="javascript:goDelt('${rghtPrps.PRPS_MAST_KEY}','${rghtPrps.DEAL_STAT_FLAG}');">삭제하기</a></span></p>
								</c:if>
							</div>
						</form>
						</div>
						<!-- //article end -->
						
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->		
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
<!-- //전체를 감싸는 DIVISION -->

<form name="srchForm" action="#">
<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
<input type="hidden" name="srchPrpsDivs" value="${srchParam.srchPrpsDivs }"/>
<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
<input type="submit" style="display:none;">
</form>
</body>
</html>
