<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>문항관리</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common2.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style2.css">
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<!-- <script src="/js/jquery-ui-1.8.4.custom.min.js"></script> -->
<script src="/js/jquery-ui-1.10.3.js"></script>
<style type="text/css"> 
	.mt3{
		margin-top:3px;
	}
</style>
<script type="text/javascript">
	$(function() {    $( "#sortable" ).sortable();    $( "#sortable" ).disableSelection();  });
	
	function sendData(){
		if(!confirm("저장하시겠습니까?")){return;}
		$("#frm").attr("action","/admin/eventMgnt/multiQustMgntAction.do").submit();
	}
	
	function chkLegCk(objNo, obj){
		var checkCnt = $("input[name='obj_"+objNo+"']:checked").length;
		var maxCount = $("input[name='max_choice_cnt_"+objNo+"']").val();
		if(Number(checkCnt) > Number(maxCount)){
			alert("최대 선택 항목수는 "+maxCount+"개 입니다.");
			$(obj).attr("checked",false);
		}
	}
	
	function regItem(){
		$("#dufrm").attr("action","/admin/eventMgnt/multiQustItemMgnt.do").submit();
	}
	
	function updateItem(eventId, itemId){
		$("#event_id_du").val(eventId);
		$("#item_id_du").val(itemId);
		$("#dufrm").attr("action","/admin/eventMgnt/uptMultiQustItemMgnt.do").submit();
	}
	
	function deleteItem(eventId, itemId){
		$("#event_id_du").val(eventId);
		$("#item_id_du").val(itemId);
		if(!confirm("삭제하시겠습니까?")){return;}
		$("#dufrm").attr("action","/admin/eventMgnt/delMultiQustItemMgnt.do").submit();
	}
	
	window.onload=function (){
		var scss = '${param.scss}';
		if(scss == 'y'){
			alert("저장하였습니다");
			return;
		}
	}
</script>
</head>
<body class="popup_bg" style="width: 700px;">
	<form id="dufrm" action="#" method="post">
		<input type="hidden" id="event_id_du" name="event_id" value="${param.event_id}">
		<input type="hidden" id="user_id_du" name="user_id" value="${param.user_id}">
		<input type="hidden" id="item_id_du" name="item_id">
	</form>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
			<!-- CONTAINER str-->
			<div id="contentBody" style="padding:0 15px 15px 15px;">
				<div class="section">
					<h2>문항관리</h2>
					<h3 class="ml20">${eventTitle}</h3>
					<span class="topLine mt20"></span>
					<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
						<tr>
							<td>
								<ul>
									<li>
									<b>&middot;진행순서</b><br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;문항 등록 &gt; 순서 및 답변 &gt; 저장 &gt; 완료
									</li>
									<li>
									<b>&middot;문항등록 안내</b><br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;문항 등록 버튼을 누르시고 문항 유형에 따라 설정하시고 질문을 입력하세요.
									</li>
									<li>
									<b>&middot;순서이동 안내</b><br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;순서 이동은 질문에 마우스를 드래그 하시고 원하시는 위치에 옮기셔서 저장 버튼을 눌러주세요.
									</li>
									<li>
									<b>&middot;답변등록 안내</b><br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;원하시는 답변을 등록하시고 저장 버튼을 눌러주세요.
									</li>
								</ul>
							</td>
						</tr>
					</table>
					<table cellspacing="0" cellpadding="0" border="0" style="width: 100%;" summary="">
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td align="right">
								<span class="button small"><a href="javascript:regItem();">문항등록</a></span>
							</td>
						</tr>
					</table>
					<!-- 이벤트 설문조사 -->
					<form id="frm" name="frm" action="#" method="post" enctype="multipart/form-data">
					<input type="hidden" name="event_id" value="${param.event_id}">
					<input type="hidden" name="user_id" value="${param.user_id}">
					<div class="event">
						<!-- 설문조사 -->
						<span class="topLine mt20"></span>
						<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
							<tbody>
								<tr>
									<td>
										<ul class="event_poll" id="sortable">
											<c:forEach var="s" items="${list}" varStatus="seq">
												<li class="mt10">
													<input type="hidden" name="item_id" value="${s.item_id}">
													<input type="hidden" name="item_type_cd" value="${s.item_type_cd}">
													<input type="hidden" name="item_seqn" value="${s.item_seqn}">
													<input type="hidden" name="max_choice_cnt_${s.item_id}" value="${s.max_choice_cnt}">
													<p class="strong line22 blue">${seq.count}) ${s.item_desc}<c:if test="${s.max_choice_cnt > 1}">(최대 ${s.max_choice_cnt}개 선택)</c:if></p>
													<c:choose>
														<c:when test="${s.item_type_cd eq '1'}">
																<input type="text" style="margin-left: 7px;" class="inputData w95" id="txt_${s.item_id}" name="obj_${s.item_id}" rows="3" cols="10" value="${s.rslt_desc}">
														</c:when>
														<c:when test="${s.item_type_cd eq '2'}">
																<textarea style="margin-left: 7px;" class="textarea w95" id="txt_${s.item_id}" name="obj_${s.item_id}" rows="3" cols="10">${s.rslt_desc}</textarea>
														</c:when>
														<c:when test="${s.item_type_cd eq '3'}">
																<c:forEach var="ss" items="${s.listSub}" varStatus="sseq">
																	<c:choose>
																		<c:when test="${s.max_choice_cnt > 1}">
																			<c:choose>
																				<c:when test="${ss.etc_yn eq 'Y'}">
																					<input onclick="chkLegCk('${s.item_id}', this)" type="checkbox" class="mb5 mt5" value="${ss.item_choice_id}" name="obj_${s.item_id}" title=""  <c:if test="${ss.corans_yn eq 'Y'}">checked="checked"</c:if>><span class="mb5">${sseq.count}) 기타 ( <input type="text" class="inputData w70" name="txt2" id="txt2" disabled="disabled"> )</span><br/>
																				</c:when>
																				<c:otherwise>
																					<input onclick="chkLegCk('${s.item_id}', this)" type="checkbox" class="mb5 mt5" value="${ss.item_choice_id}" name="obj_${s.item_id}" title="" <c:if test="${ss.corans_yn eq 'Y'}">checked="checked"</c:if>><span class="mb5">${sseq.count}) ${ss.item_choice_desc}</span><br/>
																				</c:otherwise>
																			</c:choose>
																		</c:when>
																		<c:otherwise>
																			<c:choose>
																				<c:when test="${ss.etc_yn eq 'Y'}">
																					<input type="radio" class="mb5 mt5" value="${ss.item_choice_id}" id="rgstReas" name="obj_${s.item_id}" title=""  <c:if test="${ss.corans_yn eq 'Y'}">checked="checked"</c:if>><span class="mb5">${sseq.count}) 기타 ( <input type="text" class="inputData w70" name="txt1" id="txt1" disabled="disabled"> )</span><br/>
																				</c:when>
																				<c:otherwise>
																					<input type="radio" class="mb5 mt5" value="${ss.item_choice_id}" id="rgstReas" name="obj_${s.item_id}" title="" <c:if test="${ss.corans_yn eq 'Y'}">checked="checked"</c:if>><span class="mb5">${sseq.count}) ${ss.item_choice_desc}</span><br/>
																				</c:otherwise>
																			</c:choose>
																		</c:otherwise>
																	</c:choose>
																</c:forEach>
														</c:when>
														<c:when test="${s.item_type_cd eq '4'}">
																<input style="margin-left: 7px;" type="file" onchange="checkFile(this.id)" onkeydown="return false;" class="inputData L w95" title="첨부파일" id="file" name="file" disabled="disabled">
														</c:when>
													</c:choose>
													<div style="text-align: right; padding-right: 15px; padding-top: 1px;">
														<span class="button small" style=""><a href="javascript:updateItem('${s.event_id}','${s.item_id}');">수정</a></span>
														<span class="button small blue"><a href="javascript:deleteItem('${s.event_id}','${s.item_id}');">삭제</a></span>
													</div>
													<span class="topLine mt10"></span>
												</li>
											</c:forEach>
										</ul>																
									</td>
								</tr>
							</tbody>
						</table>
						<!-- //설문조사 -->																		
						<!-- 버튼 str -->	
						<div class="btnArea mt20">
							<span class="button medium blue" onclick="sendData()"><a href="#">저장</a></span>
						</div>
                        <!-- //버튼 -->
					</div>					
					</form>
					<!-- //이벤트 설문조사 -->										
				</div>
			</div>
			<!-- //CONTAINER end -->
		
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>