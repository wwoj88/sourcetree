<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>문항등록</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common2.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style2.css">
<style>  #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }  #sortable li span { position: absolute; margin-left: -1.3em; }  </style>
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript">
	$(function (){
		if($("#sel").val() != '3'){
			$("#displayTr").hide();
			$("#btn1").hide();
			$("#max_choice_cnt_view").hide();
		}
		setMaxChoiceCnt();
	});
	
	function selectCh(){
		if($("#sel").val() != '3'){
			$("#displayTr").hide();
			$("#max_choice_cnt_view").hide();
			$("#btn1").hide();
		}else{
			$("#displayTr").show();
			$("#max_choice_cnt_view").show();
			$("#btn1").show();
		}
	};
	
	function addChoice(){
		var inStr = $('#inp').val();
		var inType;
		var etcYn;
		
		$('#inp').val('');
		
		if(inStr == ""){
			alert("항목내용을 입력하세요.");
			return;
		}
		
		if($('#itemEtcYN').is(":checked")==true){
			inType = '기타';
			etcYn = 'Y';
		}else{
			inType = '일반';
			etcYn = 'N';
		}
		
		inStr = inStr.split("<").join("&lt;");
		inStr = inStr.split(">").join("&gt;");
		inStr = inStr.split('"').join("&quot;");

		$('#itemTable tbody').append('<tr><td>'+inStr+'<input type="hidden" name="item_choice_desc" value="'+inStr+'"><input type="hidden" name="item_choice_etc_yn" value="'+etcYn+'"></td><td>'+inType+'</td><td><span class="button small" onclick="delChoice(this);"><a href="#">삭제</a></span></td></tr>');
		$("#itemEtcYN").attr("checked",false);
		setMaxChoiceCnt();
	}
	
	function delChoice(row){
		if(!confirm("삭제하시겠습니까?")){return;}
		var parents = $(row).parent().parent();
		parents.remove();
		setMaxChoiceCnt();
	}
	
	function sendDate(){
		if('${stat}' == 'upt'){
			if(!confirm("수정하시겠습니까?\n저장하신 답변은 삭제됩니다.")){return;}
			$('#frm').attr('action','/admin/eventMgnt/uptMultiQustItemMgntAction.do').submit();
		}else{
			$('#frm').attr('action','/admin/eventMgnt/multiQustItemMgntAction.do').submit();
		}
	}
	
	function setMaxChoiceCnt(){
		var crr = '${edata.max_choice_cnt}';
		var cnt = $('#itemTable tbody tr').size();
			$("#max_choice_cnt").html("");
			for(var i = 1 ; i <= cnt ; i++){
				if(i+"" == crr){
					$("#max_choice_cnt").append('<option value="'+i+'" selected="selected">'+i+'</option>');
				}else{
					$("#max_choice_cnt").append('<option value="'+i+'">'+i+'</option>');
				}
			}
	}
</script>
</head>
<body class="popup_bg" style="width: 700px;">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
			<!-- CONTAINER str-->
			<div id="contentBody" style="padding:0 15px 15px 15px;">
				<div class="section">
					<h2><c:choose><c:when test="${stat eq 'upt'}">문항수정</c:when><c:otherwise>문항등록</c:otherwise></c:choose></h2>
					<!-- 이벤트 설문조사 -->
					<div class="event">
						<!-- 설문조사 -->
						<form name="frm" id="frm" action="" method="post">
						<input type="hidden" name="event_id" value="${param.event_id}">
						<input type="hidden" name="user_id" value="${param.user_id }">
						<input type="hidden" name="item_id" value="${edata.item_id }">
						<span class="topLine mt20"></span>
						<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
							<tbody>
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
											<colgroup>
												<col width="10%">
												<col width="90%">
											</colgroup>
											<tbody>
												<tr>
													<td>문항유형</td>
													<td>
														<select name="item_type_cd" id="sel" onchange="selectCh();">
															<option value="1" <c:if test="${edata.item_type_cd eq '1'}">selected="selected"</c:if>>단답형</option>
															<option value="2" <c:if test="${edata.item_type_cd eq '2'}">selected="selected"</c:if>>서술형</option>
															<option value="3" <c:if test="${edata.item_type_cd eq '3'}">selected="selected"</c:if>>객관식</option>
															<option value="4" <c:if test="${edata.item_type_cd eq '4'}">selected="selected"</c:if>>파일첨부</option>
														</select>&nbsp;&nbsp;
														<span id="max_choice_cnt_view">
															최대선택 항목수&nbsp;:&nbsp;
															<select name="max_choice_cnt" id="max_choice_cnt">
																<option value="1">최대선택 항목수</option>
															</select>
														</span>
													</td>
												</tr>
												<tr>
													<td>질문</td>
													<td>
														<textarea class="textarea w95" id="txt3" name="item_desc" rows="3" cols="10">${edata.item_desc}</textarea>
													</td>
												</tr>
												<tr id="displayTr">
													<td>항목</td>
													<td>
														<div style="width: 500px;">
															항목내용:<input type="text" id="inp" size="50">
															기타:<input type="checkbox" id="itemEtcYN">
															<span class="button small" id="btn1"><a href="javascript:addChoice();">문항등록</a></span>
														</div>
														<table id="itemTable" cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
															<colgroup>
																<col width="90%">
																<col width="10%">
															</colgroup>
															<tbody>
																<c:forEach var="s" items="${edata.listSub}">
																	<tr>
																		<td>${s.item_choice_desc}
																		<input type="hidden" name="item_choice_desc" value="${s.item_choice_desc}">
																		<input type="hidden" name="item_choice_etc_yn" value="${s.etc_yn}">
																		</td>
																		<td>
																			<c:choose>
																				<c:when test="${s.etc_yn eq 'Y'}">기타</c:when>
																				<c:otherwise>일반</c:otherwise>
																			</c:choose>
																		</td>
																		<td>
																			<span class="button small" onclick="delChoice(this);"><a href="#">삭제</a></span>
																		</td>
																	</tr>
																</c:forEach>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						</form>
						<!-- //설문조사 -->																		
						<!-- 버튼 str -->	
						<div class="btnArea mt20">
							<span class="button medium blue"><a href="javascript:history.back();">취소</a></span>
							<span class="button medium blue"><a href="javascript:sendDate();">저장</a></span>
						</div>
                        <!-- //버튼 -->
					</div>					
					<!-- //이벤트 설문조사 -->										
				</div>
			</div>
			<!-- //CONTAINER end -->
		
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>