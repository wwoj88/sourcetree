<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ajax" uri="http://ajaxtags.sourceforge.net/tags/ajaxtags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>

<!-- json 공통 시작 -->
<script type="text/javascript" src="<c:url value="/js/jquery-1.7.1.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/json2.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.form.2.34.js"/>"></script>
<!-- json 공통 끝 -->

<script type="text/javascript">
<!--
$.noConflict();

(function($) { // function 에 $를 삽입한다. $를 사용하기 위한 함수를 만든다.
	$(function() {         // 실제사용할 함수를 만든다.
		 /* var option = {
					type		: "POST"
					url			: "/ajaxTest/zip.do",
					contentType : "application/x-www-form-urlencoded;charset=UTF-8",
					dataType    : "json",
					success		: function(data){
										alert("test");
								} */
						//$("#select_box option:selected").val();		
		$("#initCode").bind("change",function(e){
			//var params = {"sidoNm":'대전광역시'};
			var sidoNm = $("#initCode option:selected").val();
			var options = {
			type		: "POST",
			url			: "/ajaxTest/zip.do",
			contentType : "application/x-www-form-urlencoded;charset=UTF-8",
			data		: {"sidoNm":sidoNm},
			dataType    : "json",
			success		: function(data){
							var j = data.codeList;
							var options = '';
							for (var i = 0; i < j.length; i++) {
								options += '<option value="' + j[i].gugunNm + '">' + j[i].gugunNm + '</option>';
							}
							$("#dept").html(options);
						    $('#dept option:first').attr('selected', 'selected');
						}
			};
			$.ajax(options);
			/* $.getJSON("/ajaxTest/zip.do",params,function(data){
				//alert(data.codeList);
				var j = data.codeList;
				var options = '';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].gugunNm + '">' + j[i].gugunNm + '</option>';
				}
				$("#dept").html(options);
			    $('#dept option:first').attr('selected', 'selected');
			}); */
		});
	});
})(jQuery);

//-->	
</script>
<%-- 
<!--Ajax Tags-->
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/prototype.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/ajaxtags.js"></script>
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/ajaxtags/css/ajaxtags.css" />
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/ajaxtags/css/displaytag.css" />



</head>
<body>
<%//<jsp:include page="menu.html" flush="true"/>  %>
<br/>
<h2>AJAX : AutoComplete 기능 </h2>
<div id="searchform">
<form:form commandName="AjaxTestVO" action="/ajaxTest/test.do">
<table width="80%" border="0">
<tr>
	<td>이름 : <form:input path="searchName"/></td>
</tr>
</table>
</form:form>
</div>
<!--Ajax Tags Script-->
<ajax:autocomplete 
  baseUrl="/ajaxTest/suggestName.do"
  source="searchName" 
  target="searchName" 
  className="autocomplete" 
  minimumCharacters="1" />
<br/>
<br/>
<h2>AJAX : AutoSelected 기능 </h2>
<div id="editform">
<form:form commandName="AjaxTestVO">
<table>
    <tr>
        <td>부서번호 : </td>
        <td>
            <form:select path="superdeptid">
                <option value="">상위코드를 선택하세요.</option>
                <form:options items="${deptInfoOneDepthCategory}" />
            </form:select>
            </td><td>
            <form:select path="departmentid">
                <option value="">하위코드를 선택하세요.</option>
                <form:options items="${deptInfoTwoDepthCategory}" />
            </form:select>
        </td>
    </tr>
</table>
</form:form>
</div>
<ajax:select 
    baseUrl="/ajaxTest/autoSelectDept.do"
    parameters="superdeptid={superdeptid}" 
    source="superdeptid" 
    target="departmentid" 
    emptyOptionName="Select model"/>
<br/>
<br/>
<h2>JSON RETURN </h2> --%>

<select id="initCode" name="initCode" id="initCode">
<option>--전체--</option>
<c:forEach items="${initZip}" var="initZip">
	<option value="${initZip.sidoNm}">${initZip.sidoNm}</option>
</c:forEach>
</select>

<select id="dept">
	<option value="">--구군--</option>
</select>
<button id="btn1">버튼</button>    
</body>
</body>
</html>