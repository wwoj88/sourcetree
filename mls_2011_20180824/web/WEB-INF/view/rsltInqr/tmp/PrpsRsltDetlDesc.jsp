<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/popup.css">
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop">
	
		<!-- Header str -->
		<div id="popHeader">
			<h1>저작권찾기신청 처리결과 상세조회</h1>
		</div>
		<!-- //Header end -->
		
		<!-- Container str -->
		<div id="popContainer">
			<div class="popContent">
				<!-- 테이블 영역입니다 -->
				<div class="tabelRound mt10">
					<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="저작권찾기신청 처리결과 상세정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<colgroup>
						<col width="20%">
						<col width="30%">
						<col width="20%">
						<col width="*">
						</colgroup>
						
						<tbody>
							<tr>
								<th class="ce bgbr">저작물명</th>
								<td colspan="3">${rghtPrps.TITLE}</td>
							</tr>
							
							<!-- 권리조회의 경우.. -->
							<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">
							<tr>
								<th class="ce bgbr">확인권리</th>
								<td colspan="3">
								
									<!-- 음악 -->
									<!-- 음저협 -->
									<c:if test="${DIVS=='M' && rghtPrps.TRST_ORGN_CODE == '201'}">
									<b>[작사]</b> ${rghtPrps.LYRICIST} <br/>
									<b>[작곡]</b> ${rghtPrps.COMPOSER} <br/>
									<b>[편곡]</b> ${rghtPrps.ARRANGER}
									</c:if>
									
									<!-- 음실련 -->
									<c:if test="${DIVS=='M' && rghtPrps.TRST_ORGN_CODE == '202'}">
									<b>[가창]</b> ${rghtPrps.SINGER} <br/>
									<b>[연주]</b> ${rghtPrps.PLAYER} <br/>
									<b>[지휘]</b> ${rghtPrps.CONDUCTOR}
									</c:if>
									
									<!-- 음제협 -->
									<c:if test="${DIVS=='M' && rghtPrps.TRST_ORGN_CODE == '203'}">
									<b>[앨범제작]</b> ${rghtPrps.PRODUCER}
									</c:if>
									
									<!-- 어문 -->
									<!-- 문예협 -->
									<c:if test="${DIVS=='O' && rghtPrps.TRST_ORGN_CODE == '204'}">
									<b>[저자]</b> ${rghtPrps.LICENSOR_NAME_KOR} <br/>
									<b>[역자]</b> ${rghtPrps.TRANSLATOR}
									</c:if>
									
									<!-- 복전협 -->
									<c:if test="${DIVS=='O' && rghtPrps.TRST_ORGN_CODE == '205'}">
									<b>[저자]</b> ${rghtPrps.LICENSOR_NAME_KOR} <br/>
									<b>[역자]</b> ${rghtPrps.TRANSLATOR}
									</c:if>
									
									<!-- 영화 -->
									<!-- 영산협 -->
									<c:if test="${rghtPrps.PRPS_DIVS =='V' && rghtPrps.TRST_ORGN_CODE == '211'}">
									<b>[배급사]</b> ${rghtPrps.DISTRIBUTOR}
									</c:if>
									<!-- 한국시나리오작가협회 -->
									<c:if test="${rghtPrps.PRPS_DIVS =='V' && rghtPrps.TRST_ORGN_CODE == '212'}">
									<b>[작가]</b> ${rghtPrps.WRITER}
									</c:if>
									<!-- 한국영화제작가협회 -->
									<c:if test="${rghtPrps.PRPS_DIVS =='V' && rghtPrps.TRST_ORGN_CODE == '213'}">
									<b>[제작사]</b> ${rghtPrps.MV_PRODUCER} <br/>
									<b>[투자사]</b> ${rghtPrps.INVESTOR} <br/>
									<b>[배급사]</b> ${rghtPrps.DISTRIBUTOR}<br/>
									<b>[작가]</b> ${rghtPrps.MV_WRITER}
									</c:if>
									
									<!-- 이미지 -->
									<c:if test="${DIVS=='I' && rghtPrps.TRST_ORGN_CODE == '204'}">
									<b>[저작권자]</b> ${rghtPrps.COPT_HODR} <br/>
									</c:if>
									
									<c:if test="${DIVS=='I' && rghtPrps.TRST_ORGN_CODE == '216'}">
									<b>[저작권자]</b> ${rghtPrps.COPT_HODR} <br/>
									</c:if>
									
									<!-- 복전협 -->
									<c:if test="${DIVS=='M' && rghtPrps.TRST_ORGN_CODE == '205'}">
									<b>[작사]</b> ${rghtPrps.LYRICIST} <br/>
									<b>[작곡]</b> ${rghtPrps.COMPOSER} <br/>
									<b>[편곡]</b> ${rghtPrps.ARRANGER}
									</c:if>
									
									<!-- 방송 -->
									<c:if test="${DIVS=='R' && rghtPrps.TRST_ORGN_CODE == '214'}">
									<b>[제작자]</b> ${rghtPrps.MAKER}
									</c:if>
									
									<!-- 기타 -->
									<c:if test="${rghtPrps.PRPS_DIVS == 'X'}">
									<b>[저작권자]</b> ${rghtPrps.SIDE_COPT_HODR} <br/>
									</c:if>
									
									<!--  이후 저작물 종류 추가시 종류별 저작권에 대한 작성이 필요함 -->
									
								</td>
							</tr>
							</c:if>
							
							<tr>
								<th class="ce bgbr">처리자</th>
								<td>${rghtPrps.MODI_IDNT_NAME}</td>
								<th class="ce bgbr">처리일자</th>
								<td>${rghtPrps.MODI_DTTM}</td>
							</tr>
							<tr>
								<th class="ce bgbr">처리상태</th>
								<td colspan="3"><strong class="orange">${rghtPrps.DEAL_STAT_VALUE}</strong></td>
							</tr>
							<tr>
								<th class="ce bgbr">처리내용</th>
								<td colspan="3"><p class="overflow_y h100">${rghtPrps.RSLT_DESC}</p></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //테이블 영역입니다 -->
			</div>
		</div>
		<!-- //Container end -->
		
		<!-- Footer str -->
		<div id="popFooter">
			<p class="copyright">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		</div>
		<!-- Footer end -->
		
		<p class="close"><a href="javascript:window.close()"><img src="/images/2010/pop/close.gif" alt="X" title="이 창을 닫습니다."/></a></p>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>