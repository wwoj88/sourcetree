<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<%@ page import="java.util.List"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<title>신청현황조회(보상금) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/JavaScript">
<!--
	//체크박스에 따른 화면 설정(첨부파일,오프라인)
	function fn_chgInsertDiv(sOrgn, obj){
		var frm = document.frm;
	
		if(obj.checked){
			if(sOrgn == 'krtra'){
				frm.hddnInsertKrtra.value = 'Y';
			}
			fn_chgDiv(sOrgn, 'Y');
		}else{
			if(sOrgn == 'krtra'){
				if(confirm('도서관 보상금 선택해제 시 관련 선택저작물이 신청이 되지않습니다.\r\n선택해제 하시겠습니까?')){
					frm.hddnInsertKrtra.value = 'N';
				}else{
					obj.checked	= true;
					return;
				}
			}
			fn_chgDiv(sOrgn, 'N');
		}
	}

	//체크박스에 따른 화면 설정(첨부파일,오프라인)
	function fn_chgDiv(sOrgn,sDiv){

		var oFldKrtra = document.getElementById("fldKrtra");
		
		var oInmtInfo, oItemList, oFileList, oOffLine;	//div
		var chkFile, chkOff;	//checkbox
		var oBtn;

		oInmtInfo	= document.getElementById("divKrtraInmtInfo");
		oItemList	= document.getElementById("divKrtraItemList");
		oFileList	= document.getElementById("divKrtraFileList");
		oOffLine	= document.getElementById("divKrtraOffline1");
		oBtn		= document.getElementById("spanKrtraBtn");

		chkOff	= document.getElementById("chkKrtraOff");

		if(sDiv == 'OFF'){
			if(chkOff.checked){
				oInmtInfo.style.display = '';
				oItemList.style.display	= '';
				oFileList.style.display = 'none';
				oOffLine.style.display	= '';
				oBtn.style.display		= '';
			}else{
				oInmtInfo.style.display = '';
				oItemList.style.display	= '';
				oFileList.style.display = '';
				oBtn.style.display		= '';
				oOffLine.style.display	= '';
			}
		}else if(sDiv == 'Y'){
			oFldKrtra.style.display = '';
		}else if(sDiv == 'N'){
			oFldKrtra.style.display = 'none';
		}
	}

	// 선택된 저작물 삭제
	function fn_delete(sStr){
		var frm = document.frm;
		
		//선택 목록
		var chkObjs = document.getElementsByName(sStr);
		var sCheck = 0;
		
		for(i=0; i<chkObjs.length;i++){
			if(chkObjs[i].checked){
				sCheck = 1;
			}
		}
		
		if(sCheck == 0 ){
			alert('선택된 저작물이 없습니다.');
			return;
		}
		
		for(var i=chkObjs.length-1; i >= 0; i--){
			var chkObj = chkObjs[i];
			
			if(chkObj.checked){
				// 선택된 저작물을 삭제한다.
				var oTR = findParentTag(chkObj, "TR");
				if(eval(oTR)) 	oTR.parentNode.removeChild(oTR);
			}
		}
	}

	//보상금신청
	function fn_chkSubmit(){
		var frm = document.frm;

		//신청자 정보 필수체크
		var oFldInmt	= document.getElementById("fldInmtInfo");
		var oInmt	= oFldInmt.getElementsByTagName("input");

		var txtHomeAddr	= "";	var txtBusiAddr	= "";
		var txtHomeTelxNumb	= "";  var txtBusiTelxNumb	= "";  var txtMoblPhon	= "";  var txtFaxxNumb	= ""; 

		txtHomeAddr	= document.getElementById("txtHomeAddr").value;
		txtBusiAddr	= document.getElementById("txtBusiAddr").value;

		txtHomeTelxNumb	= document.getElementById("txtHomeTelxNumb").value;
		txtBusiTelxNumb	= document.getElementById("txtBusiTelxNumb").value;
		txtMoblPhon	= document.getElementById("txtMoblPhon").value;
		txtFaxxNumb	= document.getElementById("txtFaxxNumb").value;
				
		for(i = 0; i < oInmt.length; i++){
			/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
			if(checkField(oInmt[i]) == false){
				return;
			}
			*/
			//nullCheck
			
			/*
			if(oInmt[i].name == 'txtHomeTelxNumb'
				|| oInmt[i].name == 'txtBusiTelxNumb'
				|| oInmt[i].name == 'txtMoblPhon'
				|| oInmt[i].name == 'txtFaxxNumb'
				|| oInmt[i].name == 'txtHomeAddr'
				|| oInmt[i].name == 'txtBusiAddr'
				|| oInmt[i].name == 'txtMail'){
				
				if(!nullCheck(oInmt[i]))	return;
			}
			*/
			
			// 전화번호
			if(oInmt[i].name == 'txtHomeTelxNumb'
				|| oInmt[i].name == 'txtBusiTelxNumb'
				|| oInmt[i].name == 'txtMoblPhon'
				|| oInmt[i].name == 'txtFaxxNumb'){
				
				if(txtHomeTelxNumb == ''){
					if(txtBusiTelxNumb == ''){
						if(txtMoblPhon == ''){
							if(txtFaxxNumb == ''){
								if(!nullCheck(oInmt[i]))	return;
							}	
						}
					}
				}
				
				if(!character(oInmt[i],  'EK'))	return;
			}
			
			// 자택 사무실 주소 nullCheck
			if(oInmt[i].name == 'txtHomeAddr'
				|| oInmt[i].name == 'txtBusiAddr'){
				if(txtHomeAddr == ''){
					if(txtBusiAddr == ''){
						if(!nullCheck(oInmt[i]))	return;
					}	
				}
			}
			
			// 입금처
			if(oInmt[i].name == 'txtBankName'
				|| oInmt[i].name == 'txtAcctNumb'
				|| oInmt[i].name == 'txtDptr'){
				
				if(!nullCheck(oInmt[i]))	return;
			}
		}
		
		//체크박스 값들
		var vChkKrtraOff	= document.getElementById("chkKrtraOff").checked;
		if(vChkKrtraOff)		frm.hddnKrtraOff.value = "Y";

		var oChk205_2 = document.getElementById("chkTrts205_2");
		
		//복전협 정보 필수체크
		if(oChk205_2.checked){
			var oFldKrtra	= document.getElementById("fldKrtra");
			var oKrtra	= oFldKrtra.getElementsByTagName("input");
			for(i = 0; i < oKrtra.length; i++){
				/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
				if(checkField(oKrtra[i]) == false){
					return;
				}
				*/
				// nullCheck
				if(oKrtra[i].name == 'txtKrtraPemrRlnm'
					||oKrtra[i].name == 'txtKrtraResdNumb_1'
					||oKrtra[i].name == 'txtKrtraResdNumb_2'){
					
					if(!nullCheck(oKrtra[i]))	return;
				}

				// 글자길이 범위 체크
				if(oKrtra[i].name == 'txtKrtraResdNumb_1'){
					if(!rangeSize(oKrtra[i], '6'))	return;
				}
				
				if(oKrtra[i].name == 'txtKrtraResdNumb_2'){
					if(!rangeSize(oKrtra[i], '7'))	return;
				}
				
				// 금지할 문자 종류 체크 (E : 영문, K : 한글, N : 숫자)
				if(oKrtra[i].name == 'txtKrtraResdNumb_1'
					||oKrtra[i].name == 'txtKrtraResdNumb_2'){
					
					if(!character(oKrtra[i],  'EKS'))	return;
				}
			}

			//신청저작물 체크
			var oKrtraPrpsIdnt = document.getElementsByName("hddnKrtraPrpsIdnt");
			for(i = 0; i < oKrtraPrpsIdnt.length; i++){
				if(oKrtraPrpsIdnt[i].value == null || (oKrtraPrpsIdnt[i].value).length < 1){
					alert('내용(복사전송권협회) 정보를 입력해주세요.');
					return;
				}
			}
		}

		//내용 필수 체크
		var oContent	= document.getElementById("txtareaPrpsDesc");
		/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
		if(checkField(oContent) == false){
			return;
		}
		*/
		// nullCheck
		if(!nullCheck(oContent))	return;
		if(onkeylengthMax(oContent, 4000, 'txtareaPrpsDesc') == false){
			return;
		}
		
		if(oChk205_2.checked == false){
			alert('분류(을)를 선택 해 주세요.');
			return;
		}
		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrpsUpdate";
		frm.submit();
	}
	
	
	//목록으로 이동
	function fn_list(){
		var frm = document.srchFrm;

		frm.DIVS.value = '20';
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/rsltInqr/rsltInqr.do?method=list";
		frm.submit();
	}


	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.frm;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.method = "post";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
	}

    //maxlength 체크
	function onkeylengthMax(formobj, maxlength, objname) {  
	    var li_byte     = 0;  
	    var li_len      = 0;  
	    for(var i=0; i< formobj.value.length; i++){  
	    	
	        if (escape(formobj.value.charAt(i)).length > 4){  
	            li_byte += 3;  
	        } else {  
	            li_byte++;  
	        }  
	        if(li_byte <= maxlength) {  
	            li_len = i + 1;  
	        }  
	    }

	    if(li_byte > maxlength){  
	        alert('최대 글자 입력수를 초과 하였습니다. \n최대 글자 입력수를 초과된 내용은 자동으로 삭제됩니다.');  
	        formobj.value = formobj.value.substr(0, li_len);
	        return false;  
		    formobj.focus();  
	    }
	    return true;  
	}
	
	//숫자만 입력
	function only_arabic(t){
		var key = (window.netscape) ? t.which :  event.keyCode;

		if (key < 45 || key > 57) {
			if(window.netscape){  // 파이어폭스
				t.preventDefault();
			}else{
				event.returnValue = false;
			}
		} else {
			//alert('숫자만 입력 가능합니다.');
			if(window.netscape){   // 파이어폭스
				return true;
			}else{
				event.returnValue = true;
			}
		}	
	}
	
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_3.png" alt="신청현황조회" title="신청현황조회" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1">저작권찾기<span>&nbsp;</span></a></li>
							<li class="active"><a href="/rsltInqr/rsltInqr.do?DIVS=20&amp;page_no=1">보상금<span>&nbsp;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>신청현황조회</span>&gt;<strong>보상금</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>보상금 신청현황조회
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>

					<form name="srchFrm" action="" class="sch">
						<input type="hidden" name="DIVS"			value="${srchParam.DIVS }"/>
						<input type="hidden" name="srchPrpsDivs"	value="${srchParam.srchPrpsDivs }"/>
						<input type="hidden" name="srchStartDate"	value="${srchParam.srchStartDate }"/>
						<input type="hidden" name="srchEndDate"		value="${srchParam.srchEndDate }"/>
						<input type="hidden" name="srchTitle"		value="${srchParam.srchTitle }"/>
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }"/>
					</form>
					<form name="frm" action="" enctype="multipart/form-data" class="sch">
						<input type="hidden" name="DIVS"			value="${srchParam.DIVS }"/>
						<input type="hidden" name="PRPS_MAST_KEY"	value="${srchParam.PRPS_MAST_KEY }"/>
						<input type="hidden" name="PRPS_SEQN"		value="${srchParam.PRPS_SEQN }"/>
						<input type="hidden" name="mode" 			value="U" />
						
						<input type="hidden" name="srchPrpsDivs"	value="${srchParam.srchPrpsDivs }"/>
						<input type="hidden" name="srchStartDate"	value="${srchParam.srchStartDate }"/>
						<input type="hidden" name="srchEndDate"		value="${srchParam.srchEndDate }"/>
						<input type="hidden" name="srchTitle"		value="${srchParam.srchTitle }"/>
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }"/>
						
						<input type="hidden" name="actFlagYn" />
						<input type ="hidden" name="prpsMastKey" value="${srchParam.PRPS_MAST_KEY}" />
						
						<input type="hidden" name="hddnTrst203"	value="${clientInfo.TRST_203 }" />
						<input type="hidden" name="hddnTrst202"	value="${clientInfo.TRST_202 }" />
						<input type="hidden" name="hddnTrst205"	value="${clientInfo.TRST_205 }" />
						<input type="hidden" name="hddnTrst205_2"	value="${clientInfo.TRST_205_2 }" />
						
						<input type="hidden" name="hddnRgstDttm" value="${clientInfo.RGST_DTTM }" />
						
						<input type="hidden" name="prpsDoblCode"	value="${clientInfo.PRPS_DOBL_CODE }" />
						<input type="hidden" name="prpsDoblKey"	value="${clientInfo.PRPS_DOBL_KEY }" />
						<input type="hidden" name="rghtPrpsMastKey"	value="${clientInfo.RGHT_PRPS_MAST_KEY }" />
						
						<input type="hidden" name="filePath">
						<input type="hidden" name="fileName">
						<input type="hidden" name="realFileName">

					<!-- memo 삽입 -->
						<jsp:include page="/common/memo/memo_01.jsp">
							<jsp:param name="DIVS" value="${srchParam.DIVS}"/>
						</jsp:include>
					<!-- // memo 삽입 -->
				
					<div class="bbsSection">
						<h4>도서관 보상금 신청결과 상세</h4>
							<fieldset id="fldInmtInfo">
								<legend>신청자 정보</legend>
								
								<!-- 보상금신청(기본정보) 테이블 영역입니다 -->
								<div class="tabelRound">
									<span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span><!-- 라운딩효과 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금 신청정보 입력 폼입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row" class="bgbr lft">보상금종류</th>
												<td>도서관</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청자</th>
												<td>
													<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
													<colgroup>
														<col width="15%">
														<col width="*">
														<col width="15%">
														<col width="35%">
														</colgroup>
														<tbody>
															<tr>
																<th class="bgbr3 lft"><label class="necessary">성명</label></th>
																<td>
																	<input type="hidden" name="hddnUserIdnt" value="${clientInfo.USER_IDNT }" />
																	<input type="text" name="txtPrpsName" value="${clientInfo.PRPS_NAME }" readonly class="inputData w60" title="신청자 성명" />
																</td>
																<th class="bgbr3 lft"><label class="necessary">주민번호/사업자번호</label></th>
																<td><input type="text" name="txtPrsdCorpNumbView" value="${clientInfo.RESD_CORP_NUMB_VIEW }" title="신청자 주민번호/사업자번호" readonly class="inputData w60" /></td>
															</tr>
															<tr>
																<th class="bgbr3 lft"><label class="necessary">전화</label></th>
																<td>
																	<label class="labelBlock"><em class="w55">자택</em><input type="text" name="txtHomeTelxNumb" value="${clientInfo.HOME_TELX_NUMB }" maxlength="20" title="신청자 전화(자택)" class="inputData w60" /></label>
																	<label class="labelBlock"><em class="w55">사무실</em><input type="text" name="txtBusiTelxNumb" value="${clientInfo.BUSI_TELX_NUMB }" maxlength="20" title="신청자 전화(사무실)" class="inputData w60" /></label>
																	<label class="labelBlock"><em class="w55">휴대폰</em><input type="text" name="txtMoblPhon" value="${clientInfo.MOBL_PHON }" maxlength="20" title="신청자 전화(휴대폰)" class="inputData w60" /></label>
																</td>
																<th class="bgbr3 lft">Fax</th>
																<td><input name="txtFaxxNumb" value="${clientInfo.FAXX_NUMB }" title="신청자 전화(FAX)" class="inputData" /></td>
															</tr>
															<tr>
																<th class="bgbr3 lft">이메일주소</th>
																<td><input type="text" name="txtMail" value="${clientInfo.MAIL }" maxlength="50" title="신청자 EMAIL" class="inputData" /></td>
																<th class="bgbr3 lft"><label class="necessary">입금처</label></th>
																<td>
																	<label class="labelBlock"><em class="w50">은행명</em><input type="text" name="txtBankName" value="${clientInfo.BANK_NAME }" title="입금 은행명" maxlength="50" class="inputData w60" /></label>
																	<label class="labelBlock"><em class="w50">계좌번호</em><input type="text" name="txtAcctNumb" value="${clientInfo.ACCT_NUMB }" title="입금 계좌번호" maxlength="50" class="inputData w60" /></label>
																	<label class="labelBlock"><em class="w50">예금주</em><input type="text" name="txtDptr" value="${clientInfo.DPTR }" title="입금 예금주" maxlength="33" class="inputData w60" /></label>
																</td>
															</tr>
															<tr>
																<th class="bgbr3 lft"><label class="necessary">주소</label></th>
																<td colspan="3" class="vtop">
																	<label class="labelBlock"><em class="w45">자택</em><input type="text" name="txtHomeAddr" value="${clientInfo.HOME_ADDR }" maxlength="33" title="신청자 주소(자택)" class="inputData w80" /></label>
																	<label class="labelBlock"><em class="w45">사무실</em><input type="text" name="txtBusiAddr" value="${clientInfo.BUSI_ADDR }" maxlength="33" title="신청자 주소(사무실)" class="inputData w80" /></label>
																</td>
															</tr>
														</tbody>
													</table>
													
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청분류</th>
												<td>
												    <label><input type="checkbox" name="chkTrts205_2" id="chkTrts205_2" onclick="javascript:fn_chgInsertDiv('krtra',this);"  <c:if test="${clientInfo.TRST_205_2 == '1'}">checked</c:if>  class="inputRChk" title="도서관 보상금" />도서관 보상금</label>
													<input type="hidden" name="hddnInsertKrtra"		value=" <c:if test="${clientInfo.TRST_205_2 == '1'}">Y</c:if>" />
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청저작물</th>
												<td>
												
													<!-- 테이블 영역입니다 -->
													<div class="tabelRound overflow_y h100">
														<table cellspacing="0" cellpadding="0" border="1" class="grid w95" summary="보상금신청 저작물 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
																<col width="8%">
																<col width="52%">
																<col width="25%">
																<col width="15%">
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH">도서명</th>
																	<th scope="col" class="headH">저자</th>
																	<th scope="col" class="headH">사용년도</th>
																</tr>
															</thead>
															<tbody>
															<c:if test="${!empty tempList}">
																<c:forEach items="${tempList}" var="tempList">	
																	<c:set var="NO" value="${tempListCnt + 1}"/>
																	<c:set var="i" value="${i+1}"/>
																<tr>
																	<td class="ce">
																	    ${NO - i }
																		<input type="hidden" name="hddnSelectInmtSeqn" value="${tempList.INMT_SEQN }" />
																		<input type="hidden" name="hddnSelectPrpsIdntCode" value="${tempList.PRPS_IDNT_CODE }" />
																		<input type="hidden" name="hddnSelectPrpsDivs" value="${tempList.PRPS_DIVS }" />
																		<input type="hidden" name="hddnSelectKapp" value="${tempList.KAPP }" />
																		<input type="hidden" name="hddnSelectFokapo" value="${tempList.FOKAPO }" />
																		<input type="hidden" name="hddnSelectKrtra" value="${tempList.KRTRA }" />
																	</td>
																	<td>${tempList.SDSR_NAME }</td>
																	<td class="ce">${tempList.MUCI_NAME }</td>
																	<td class="ce">${tempList.YYMM }</td>
																</tr>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
													</div>
													<!-- //테이블 영역입니다 -->
													
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청(기본정보) 테이블 영역입니다 -->
								
							</fieldset>
					</div>

		<c:if test="${!empty trstList}">
			<c:forEach items="${trstList}" var="trstList">
				<c:if test="${trstList.TRST_ORGN_CODE=='205_2' }">
                    <!-- 복전협 관련 영역 -->
    				<fieldset id="fldKrtra" style="display:<c:if test="${clientInfo.TRST_205_2 != '1'}">none</c:if>;">
    					<legend>내용(한국복사전송권협회)</legend>
	    					
						<div class="bbsSection">
							<div class="floatDiv">
								<h4 class="fl">내용(한국복사전송권협회) 도서관 보상금
								    <span class="labelFont">
    							        <label><input type="checkbox" id="chkKrtraOff" onclick="javascript:fn_chgDiv('krtra','OFF');" class="inputChk ml10" <c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if> title="오프라인접수(첨부서류)" />오프라인접수(첨부서류)</label>
										<input type="hidden" name="hddnKrtraOff" value="N" /> 
    							    </span>
								</h4>
								<p class="fr"><span id="spanKrtraBtn" class="button small type3"><a href="javascript:fn_delete('chkKrtra');">행삭제</a></span></p>
							</div>
							
							<div>
								<!-- 보상금신청 권리자정보 테이블 영역입니다 -->
								<div id="divKrtraInmtInfo" class="tabelRound">
	    							<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금신청 권리자정보 입력 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
	    								<colgroup>
	    								<col width="15%">
	    								<col width="40%">
	    								<col width="15%">
	    								<col width="*">
	    								</colgroup>
	    								<tbody>
	    									<tr>
	    										<th class="bgbr"><label class="necessary">권리자</label></th>
	    										<td>
	    											<label class="labelBlock"><em class="w45">본명</em><input type="text" name="txtKrtraPemrRlnm" value="${pemrRlnm }" title="권리자(본명)" class="inputData w60" /></label>
	    											<label class="labelBlock"><em class="w45">필명</em><input type="text" name="txtKrtraGrupName" value="${pemrStnm }" title="권리자(필명)" class="inputData w60" /></label>
	    											<label class="labelBlock"><em class="w45">예명</em><input type="text" name="txtKrtraPemrStnm" value="${grupName }" title="권리자(예명)" class="inputData w60" /></label>
	    										</td>
	    										<th class="bgbr"><label class="necessary">주민번호</label></th>
											    <td>
											    <!-- x
											    	<input type="text" name="txtKrtraResdNumb" value="${resdNumb }" title="권리자(주민번호)" class="inputData w80" />
											    	 -->
											    	<input type="text" name="txtKrtraResdNumb_1" maxlength="6" Size="6" onkeypress="javascript:only_arabic(event);" value="${resdNumb_1}" title="권리자 주민번호(1번째)" class="inputData w40" />
													 -
													 <input type="password" autocomplete="off" name="txtKrtraResdNumb_2" maxlength="7" Size="7" onkeypress="javascript:only_arabic(event);" value="${resdNumb_2}" title="권리자 주민번호(2번째)" class="inputData w40" />
											    </td>
										</tr>
									</tbody>
								</table>
								</div>
								<!-- //보상금신청 권리자정보 테이블 영역입니다 -->
								
								<!-- 보상금신청 항목 테이블 영역입니다 -->
								<div id="divKrtraItemList" class="tabelRound">							
								<table cellspacing="0" cellpadding="0" border="1" class="clear grid topLine mt5" summary="보상금신청 저작물 정보 입력폼입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
									<colgroup>
										<col width="6%">
										<col width="44%">
										<col width="19%">
										<col width="15%">
										<col width="8%">
										<col width="8%">
									</colgroup>
									<thead>
										<tr>
											<th scope="col" class="headH"><input type="checkbox" onclick="javascript:checkBoxToggle('frm','chkKrtra',this);" class="vmid" title="전체선택" /></th>
											<th scope="col" class="headH">서명(논문명)</th>
											<th scope="col" class="headH">(공동)저자명</th>
											<th scope="col" class="headH">출판사</th>
											<th scope="col" class="headH">발행년도</th>
											<th scope="col" class="headH">판매유무</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${!empty krtraList_2}">
											<c:forEach items="${krtraList_2}" var="krtraList_2">	
												<c:set var="krtraNo" value="${krtraNo+1}"/>
													<!-- 일반 보상금신청 -->
													<c:if test="${clientInfo.PRPS_DOBL_CODE != '1' }">
														<tr>
															<td class="ce">
																<input type="checkbox" name="chkKrtra" id="chkKrtra" class="vmid" title="선택"/>
																<input type="hidden" name="hddnKrtraPrpsIdnt" value="${krtraList_2.PRPS_IDNT }" />
																<input type="hidden" name="hddnKrtraPrpsDivs" value="${krtraList_2.PRPS_DIVS }" />
																<input type="hidden" name="hddnKrtraPrpsIdntCode" value="${krtraList_2.PRPS_IDNT_CODE }" />
																
																<input type="hidden" name="hddnKrtraWorkName"	value="${krtraList_2.WORK_NAME }" />
																<input type="hidden" name="hddnKrtraWrtrName"	value="${krtraList_2.WRTR_NAME }" />
																<input type="hidden" name="hddnKrtraBookCncn"	value="${krtraList_2.BOOK_CNCN }" />
																<input type="hidden" name="hddnKrtraSctr"		value="${krtraList_2.SCTR }" />
																<input type="hidden" name="hddnKrtraUsexType"	value="${krtraList_2.USEX_TYPE }" />
															</td>
															<td>${krtraList_2.WORK_NAME }</td>
															<td class="ce">${krtraList_2.WRTR_NAME }</td>
															<td class="ce">${krtraList_2.BOOK_CNCN }</td>
															<td class="ce">${krtraList_2.SCTR }</td>
															<td class="ce">${krtraList_2.USEX_TYPE }</td>
														</tr>
													</c:if>
													
													<!-- 동시 보상금신청 -->
													<c:if test="${clientInfo.PRPS_DOBL_CODE == '1' }">
														<tr>
															<td class="ce">
																<input type="checkbox" name="chkKrtra" id="chkKrtra" class="vmid" title="선택" />
																<input type="hidden" name="hddnKrtraPrpsIdnt" value="${krtraList_2.PRPS_IDNT }" />
																<input type="hidden" name="hddnKrtraPrpsDivs" value="${krtraList_2.PRPS_DIVS }" />
																<input type="hidden" name="hddnKrtraPrpsIdntCode" value="${krtraList_2.PRPS_IDNT_CODE }" />
																
																<input type="hidden" name="hddnKrtraWorkName"	value="${krtraList_2.WORK_NAME }" />
																<!-- 
																<input type="hidden" name="hddnKrtraWrtrName"	value="${selectList.MUCI_NAME }" />
																<input type="hidden" name="hddnKrtraBookCncn"	value="${selectList.LISH_COMP }" />
																<input type="hidden" name="hddnKrtraSctr"		value="${selectList.YYMM }" />
																<input type="hidden" name="hddnKrtraUsexType"	value="${selectList.USEX_TYPE }" />
																 -->
															</td>
															<td>${krtraList_2.WORK_NAME }</td>
															<td class="ce"><input type="text" name="hddnKrtraWrtrName" value="${krtraList_2.WRTR_NAME }" maxlength="20" title="저자명" class="inputData w90 ce" /></td>
															<td class="ce"><input type="text" name="hddnKrtraBookCncn" value="${krtraList_2.BOOK_CNCN }" maxlength="50" title="출판사" class="inputData w90 ce" /></td>
															<td class="ce"><input type="text" name="hddnKrtraSctr" value="${krtraList_2.SCTR }" maxlength="4" title="발행년도" class="inputData w90 ce" /></td>
															<td class="ce"><input type="text" name="hddnKrtraUsexType" value="${krtraList_2.USEX_TYPE }" maxlength="4" title="판매유무" class="inputData w90 ce" /></td>
														</tr>
													</c:if>
													
											</c:forEach>
										</c:if>
									</tbody>
								</table>
								</div>
								<!-- //보상금신청 항목 테이블 영역입니다 -->
	
								<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
								<div id="divKrtraFileList" class="tabelRound mb5" style="display:<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>;">

								</div>
								<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
									
								<!-- 보상금신청 오프라인 관련 영역 -->
								<div id="divKrtraOffline1" class="contentsRoundBox">
									<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
									<div class="infoImg1">
										<p><strong>한국복사전송권협회(도서관보상금) 오프라인접수 선택한 경우 해당협회에 관련서류를 제출하여야합니다.</strong></p>
										<ul>
											<li>1. 도서관보상금 신청서<br/> 
												&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금 신청현황조회 상세화면에서 도서관보상금 신청서 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span>
											</li>
											<li class="mt3">2. 저작권자임을 확인할 수 있는 서류(저작권등록증 등)</li>
											<li class="mt3">3. 분배대상 저작물 명세표<br/> 
												&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금 신청현황조회 상세화면에서 분배대상 저작물 명세표 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span>
											</li>
											<li class="mt3">4. 통장사본 1부</li>
										</ul>
									</div>
								</div>
								<!-- //보상금신청 오프라인 관련 영역 -->
								
							</div>
						</div>
					</fieldset>
					<!-- 복전협 관련 영역 -->
				</c:if>
			</c:forEach>
		</c:if>
							
					<!-- 보상금신청(내용) 테이블 영역입니다 -->
					<div class="bbsSection">
						<div id="divContent"  class="tabelRound mb5">
							<table cellspacing="0" cellpadding="0" border="1" class="clear grid topLine mt5" summary="보상금신청 내용입력 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="15%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th class="bgbr"><label for="txtareaPrpsDesc" class="necessary">내용</label></th>
										<td><textarea cols="10" rows="10" name="txtareaPrpsDesc" id="txtareaPrpsDesc" title="내용" class="h100">${clientInfo.PRPS_DESC }</textarea></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- //보상금신청(내용) 테이블 영역입니다 -->
					</form>
					
					<div class="floatDiv mt10">
						<p class="fl"><span class="button medium icon"><span class="list">&nbsp;</span><a href="javascript:fn_list()">목록</a></span></p>
						<p class="fr"><span class="button medium icon"><span class="default">&nbsp;</span><a href="javascript:fn_chkSubmit();">보상금 수정</a></span></p>
					</div>
						
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->



<script type="text/javascript" src="/js/2010/file.js"></script>
<script type="text/javascript">
<!--
	function fn_setFileInfo(){
		var listCnt = '${fn:length(fileList)}';
		<c:if test="${!empty fileList}">
			<c:forEach items="${fileList}" var="fileList">
			fn_addGetFile(	'205_2',
							'${fileList.PRPS_MAST_KEY}',
							'${fileList.PRPS_SEQN}',
							'${fileList.ATTC_SEQN}',
							'${fileList.FILE_PATH}',
							'${fileList.REAL_FILE_NAME}',
							'${fileList.FILE_NAME}',
							'${fileList.TRST_ORGN_CODE}',
							'${fileList.FILE_SIZE}');
			</c:forEach>
		</c:if>		
	}

	window.onload	= function(){	fn_createTable("divKrtraFileList", "205_2");
									fn_setFileInfo();
								}
//-->
</script>
</body>
</html>
