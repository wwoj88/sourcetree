<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(보상금) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript">
<!--
	//목록으로 이동
	function fn_list(){
		var frm = document.frm;

		frm.DIVS.value = '20';
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/rsltInqr/rsltInqr.do?method=list&page_no=1";
		frm.submit();
	}
	
	//수정화면으로 이동
	function fn_update(){
		var frm = document.frm;
		var count = 0;
	
		var oDealStat = document.getElementsByName("hddnDealStat");
		for(i = 0; i < oDealStat.length; i++){
			if(oDealStat[i].value > 1)	count++;
		}
		
		if(count > 0){
			alert('수정가능한 상태가 아닙니다.');
			return;
		}else{
			frm.mode.value = 'U';
			frm.method = "post";
			frm.action = "/rsltInqr/rsltInqr.do?method=inmtPrpsDetl&page_no="+'${srchParam.nowPage }';
			frm.submit();
		}
	}
	
	// 출력물 
	function fn_report(report){
		var frm = document.frm;
		frm.mode.value = 'R';
		
		var sUrl = "/rsltInqr/rsltInqr.do?method=inmtPrpsDetl";
		var param = "&DIVS="+frm.DIVS.value;
		     param += "&PRPS_MAST_KEY="+frm.PRPS_MAST_KEY.value;
		     param += "&PRPS_SEQN="+frm.PRPS_SEQN.value;
		     param += "&mode=R";
		     param += "&report="+report;
		 
		sUrl += param;
		window.open(sUrl,"PRINT","toolbar=no,menubar=no,resizable=no,status=yes");
	}
	
	//보상금신청 취소
	function fn_delete(){
	
		var frm = document.frm;
		var count = 0;
	
		var oDealStat = document.getElementsByName("hddnDealStat");
		for(i = 0; i < oDealStat.length; i++){
			if(oDealStat[i].value > 1)	count++;
		}
		
		if(count > 0){
			alert('삭제가능한 상태가 아닙니다.');
			return;
		}else{
			if(confirm('삭제 하시겠습니까?')){ 
				frm.actFlagYn.value = 'Y';
				
				frm.method = "post";
				frm.action = "/rsltInqr/rsltInqr.do?method=deleteRsltInqr&DIVS=20&page_no=1";
				frm.submit();
			}
		}
		
	}

	var openPopUpFlag;

	// 처리결과 상세 팝업오픈
	function openDetlDesc(prpsSeqn, trstCode, prpsIdnt)  {
		var param = '';
		
		param += '&PRPS_SEQN='+prpsSeqn;
		param += '&TRST_ORGN_CODE='+trstCode;
		param += '&PRPS_IDNT='+prpsIdnt;
		
		var url = '/rsltInqr/rsltInqr.do?method=rghtPrpsRsltDetl'+param
		var name = '';
		var openInfo = 'target=inmtPrpsDetl_musc width=500, height=300';	
		
		//열린팝업창 존재 유무체크
		if(openPopUpFlag) {
			if(!openPopUpFlag.closed) openPopUpFlag.window.close();
		}
		openPopUpFlag = window.open(url, name, openInfo);
	}

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.frm;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.method = "post";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
	}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_3.png" alt="신청현황조회" title="신청현황조회" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1">저작권찾기<span>&nbsp;</span></a></li>
							<li class="active"><a href="/rsltInqr/rsltInqr.do?DIVS=20&amp;page_no=1">보상금<span>&nbsp;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>신청현황조회</span>&gt;<strong>보상금</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>보상금 신청현황조회
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>

					<form name="frm" action="" class="sch">
						<input type="hidden" name="DIVS"			value="${srchParam.DIVS }"/>
						<input type="hidden" name="PRPS_MAST_KEY"	value="${srchParam.PRPS_MAST_KEY }"/>
						<input type="hidden" name="PRPS_SEQN"		value="${srchParam.PRPS_SEQN }"/>
						<input type="hidden" name="mode" />
						
						<input type="hidden" name="srchPrpsDivs"	value="${srchParam.srchPrpsDivs }"/>
						<input type="hidden" name="srchStartDate"	value="${srchParam.srchStartDate }"/>
						<input type="hidden" name="srchEndDate"		value="${srchParam.srchEndDate }"/>
						<input type="hidden" name="srchTitle"		value="${srchParam.srchTitle }"/>
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }"/>
						
						<input type="hidden" name="actFlagYn" />
						<input type ="hidden" name="prpsMastKey" value="${srchParam.PRPS_MAST_KEY}" />
						
						<input type="hidden" name="filePath">
						<input type="hidden" name="fileName">
						<input type="hidden" name="realFileName">

					<!-- memo 삽입 -->
						<jsp:include page="/common/memo/memo_01.jsp">
							<jsp:param name="DIVS" value="${srchParam.DIVS}"/>
						</jsp:include>
					<!-- // memo 삽입 -->
					
					<div class="bbsSection">
						<h4>도서관 보상금 신청결과 상세</h4>
							<fieldset>
								<legend>신청자 정보</legend>
								
								<!-- 보상금신청(기본정보) 테이블 영역입니다 -->
								<div class="tabelRound">
									<span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span><!-- 라운딩효과 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금 신청정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row" class="bgbr lft">보상금종류</th>
												<td>도서관</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청자</th>
												<td>
													<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등 정보입니다.">
													<colgroup>
														<col width="15%">
														<col width="*">
														<col width="15%">
														<col width="35%">
														</colgroup>
														<tbody>
															<tr>
																<th class="bgbr3 lft">성명</th>
																<td>${clientInfo.PRPS_NAME }</td>
																<th class="bgbr3 lft">주민번호/사업자번호</th>
																<td>${clientInfo.RESD_CORP_NUMB_VIEW }</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">전화</th>
																<td>
																	<label class="labelBlock"><em class="w45">자택</em>${clientInfo.HOME_TELX_NUMB }</label>
																	<label class="labelBlock"><em class="w45">사무실</em>${clientInfo.BUSI_TELX_NUMB }</label>
																	<label class="labelBlock"><em class="w45">휴대폰</em>${clientInfo.MOBL_PHON }</label>
																</td>
																<th class="bgbr3 lft">Fax</th>
																<td>${clientInfo.FAXX_NUMB }</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">이메일주소</th>
																<td>${clientInfo.MAIL }</td>
																<th class="bgbr3 lft">입금처</th>
																<td>
																	<label class="labelBlock"><em class="w50">은행명</em>${clientInfo.DPTR }</label>
																	<label class="labelBlock"><em class="w50">계좌번호</em>${clientInfo.ACCT_NUMB }</label>
																	<label class="labelBlock"><em class="w50">예금주</em>${clientInfo.DPTR }</label>
																</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">주소</th>
																<td colspan="3" class="vtop">
																	<label class="labelBlock"><em class="w45">자택</em>${clientInfo.HOME_ADDR }</label>
																	<label class="labelBlock"><em class="w45">사무실</em>${clientInfo.BUSI_ADDR }</label>
																</td>
															</tr>
														</tbody>
													</table>
													
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청분류</th>
												<td>
												    <label><input type="checkbox" class="inputRChk" checked="checked" disabled="disabled" title="도서관 보상"/>도서관 보상금</label>
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr lft">신청저작물</th>
												<td>
												
													<!-- 테이블 영역입니다 -->
													<div class="tabelRound overflow_y h100">
														<table cellspacing="0" cellpadding="0" border="1" class="grid w95" summary="보상금신청 저작물 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
																<col width="8%">
																<col width="52%">
																<col width="25%">
																<col width="15%">
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH">도서명</th>
																	<th scope="col" class="headH">저자</th>
																	<th scope="col" class="headH">사용년도</th>
																</tr>
															</thead>
															<tbody>
															<c:if test="${!empty tempList}">
																<c:forEach items="${tempList}" var="tempList">	
																	<c:set var="NO" value="${tempListCnt + 1}"/>
																	<c:set var="i" value="${i+1}"/>
																<tr>
																	<td class="ce">${NO - i }</td>
																	<td>${tempList.SDSR_NAME }</td>
																	<td class="ce">${tempList.MUCI_NAME }</td>
																	<td class="ce">${tempList.YYMM }</td>
																</tr>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
													</div>
													<!-- //테이블 영역입니다 -->
													
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청(기본정보) 테이블 영역입니다 -->
								
							</fieldset>
					</div>

		<c:if test="${!empty trstList}">
			<c:forEach items="${trstList}" var="trstList">
				<c:if test="${trstList.TRST_ORGN_CODE=='205_2' }">
                    <!-- 복전협 관련 영역 -->
					<div class="bbsSection">
						<div class="floatDiv">
							<h4 class="fl">내용(한국복사전송권협회) 도서관 보상금
							    <span class="labelFont">
							        <label><input type="checkbox" class="inputChk ml10" <c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if> disabled title="오프라인접수(첨부서류)"/>오프라인접수(첨부서류)</label>
							    </span>
							</h4>
							<p class="fr">
							    <span class="button small type2 icon"><span class="print">&nbsp;</span><a href="javascript:fn_report('report/inmtPrps_205_2');">신청서 출력</a></span> 
							    <span class="button small type2 icon"><span class="print">&nbsp;</span><a href="javascript:fn_report('report/inmtPrps_205_2M');">명세표 출력</a></span>
							</p>
						</div>
						
						<div>
							<!-- 보상금신청 권리자정보 테이블 영역입니다 -->
							<div class="tabelRound">
    							<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금신청 권리자정보 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
    								<colgroup>
    								<col width="20%">
    								<col width="30%">
    								<col width="20%">
    								<col width="*">
    								</colgroup>
    								<tbody>
    									<tr>
    										<th class="bgbr">권리자</th>
    										<td>
    											<label class="labelBlock"><em class="w45">본명</em>${pemrRlnm }</label>
    											<label class="labelBlock"><em class="w45">필명</em>${pemrStnm }</label>
    											<label class="labelBlock"><em class="w45">예명</em>${grupName }</label>
    										</td>
    										<th class="bgbr">주민번호</th>
										    <td><!-- x${resdNumbStr }  -->
										    	<input type="text" name="txtFokapoResdNumb_1" value="${resdNumb_1}" title="권리자 주민번호(1번째)" class="whiteR w20" />
												 -
												<input type="password" autocomplete="off" name="txtFokapoResdNumb_2" value="${resdNumb_2}" title="권리자 주민번호(2번째)" class="whiteL w38" />
										    </td>
									</tr>
								</tbody>
							</table>
							</div>
							<!-- //보상금신청 권리자정보 테이블 영역입니다 -->

							<!-- 보상금신청 항목 테이블 영역입니다 -->
							<div id="divFokapoItemList" class="tabelRound">							
							<table cellspacing="0" cellpadding="0" border="1" class="clear grid topLine mt5" summary="보상금신청 저작물 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
									<col width="6%">
									<col width="8%">
									<col width="38%">
									<col width="17%">
									<col width="15%">
									<col width="8%">
									<col width="8%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="headH">번호</th>
										<th scope="col" class="headH">처리상태</th>
										<th scope="col" class="headH">서명(논문명)</th>
										<th scope="col" class="headH">(공동)저자명</th>
										<th scope="col" class="headH">출판사</th>
										<th scope="col" class="headH">발행년도</th>
										<th scope="col" class="headH">이용형태</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${!empty krtraList_2}">
										<c:forEach items="${krtraList_2}" var="krtraList_2">	
											<c:set var="krtraNo" value="${krtraNo+1}"/>
									<tr>
										<td class="ce">${krtraNo }</td>
										<td class="ce">
											<c:choose>
												<c:when test="${krtraList_2.DEAL_STAT=='1' }"><span class="blue underline">신청</span></c:when>
												<c:when test="${krtraList_2.DEAL_STAT=='2' }"><span class="blue underline">접수</span></c:when>
												<c:when test="${krtraList_2.DEAL_STAT=='3' }"><span class="blue underline">처리중</span></c:when>
												<c:when test="${krtraList_2.DEAL_STAT=='4' }"><a href="javascript:openDetlDesc('${krtraList_2.PRPS_SEQN }','${krtraList_2.ORGN_TRST_CODE }','${krtraList_2.PRPS_IDNT }');"><span class="blue underline">완료</span></a></c:when>
											</c:choose>
										</td>
										<td>${krtraList_2.WORK_NAME }</td>
										<td class="ce">${krtraList_2.WRTR_NAME }</td>
										<td class="ce">${krtraList_2.BOOK_CNCN }</td>
										<td class="ce">${krtraList_2.SCTR }</td>
										<td class="ce">${krtraList_2.USEX_TYPE }</td>
									</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
							</div>
							<!-- //보상금신청 항목 테이블 영역입니다 -->
							<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
							<div id="divFokapoFileList" class="tabelRound mb5" style="display:<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>;">
								<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="보상금신청 첨부파일정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
									<colgroup>
									<col width="20%">
									<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="col" class="headH white p12">첨부서류</th>
											<td>
												<dl class="fl categori w100">
													<dt><strong>파일이름</strong></dt>
													<dd>
														<table class="w100">
															<colgroup>
																<col>
															</colgroup>
															<tbody>
																<c:if test="${!empty fileList}">
																	<c:forEach items="${fileList}" var="fileList">	
																<tr>
																	<td style="padding:0px;"><a href="javascript:fn_fileDownLoad('${fileList.FILE_PATH}','${fileList.FILE_NAME}','${fileList.REAL_FILE_NAME}')">${fileList.FILE_NAME}</a><br></td>
																</tr>
																	</c:forEach>
																</c:if>
															</tbody>
														</table>
													</dd>
												</dl>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
								
							<!-- 보상금신청 오프라인 관련 영역 -->
							<div id="divKrtraOffline1" class="contentsRoundBox" style="display:<c:if test="${trstList.OFFX_LINE_RECP!='Y' }">none</c:if>;">
								<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
								<div class="infoImg1">
									<p><strong>한국복사전송권협회(도서관보상금) 오프라인접수 선택한 경우 해당협회에 관련서류를 제출하여야합니다.</strong></p>
									<ul>
										<li>1. 도서관보상금 신청서 <br/>
											&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금 신청현황조회 상세화면에서 도서관보상금 신청서 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span>
										</li>
										<li class="mt3">2. 저작권자임을 확인할 수 있는 서류(저작권등록증 등)</li>
										<li class="mt3">3. 분배대상 저작물 명세표<br/>
											&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue fontSmall">(보상금 신청현황조회 상세화면에서 분배대상 저작물 명세표 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span>
										</li>
										<li class="mt3">4. 통장사본 1부</li>
									</ul>
								</div>
							</div>
							<!-- //보상금신청 오프라인 관련 영역 -->
							
						</div>
					</div>
					<!-- 복전협 관련 영역 -->
				</c:if>
			</c:forEach>
		</c:if>
							
					<!-- 보상금신청(내용) 테이블 영역입니다 -->
					<div class="bbsSection">
						<div id="divContent"  class="tabelRound mb5">
							<table cellspacing="0" cellpadding="0" border="1" class="clear grid topLine mt5" summary="보상금신청 내용정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th class="bgbr">내용</th>
										<td><p class="overflow_y h100">${clientInfo.PRPS_DESC }</p></td>
									</tr>
									<tr>
										<th class="bgbr">처리상태</th>
										<td><strong class="blue">${totDealStat }</strong></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- //보상금신청(내용) 테이블 영역입니다 -->
					</form>
					
					<div class="floatDiv mt10">
						<p class="fl">
							<span class="button medium icon"><span class="list">&nbsp;</span><a href="javascript:fn_list();">목록</a></span>
						</p>
						<p class="rgt">
							<span class="button medium icon"><span class="default">&nbsp;</span><a href="javascript:fn_update();">수정</a></span> 
							<span class="button medium icon"><span class="del">&nbsp;</span><a href="javascript:fn_delete();">삭제</a></span>
						</p>
					</div>
						
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
