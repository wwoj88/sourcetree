<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(보상금) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<link type="text/css" rel="stylesheet" href="/css/2010/calendar.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/JavaScript">
<!--

/*calendar호출*/
function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}

//숫자만 입력
function only_arabic(t){
	var key = (window.netscape) ? t.which :  event.keyCode;

	if (key < 45 || key > 57) {
		if(window.netscape){  // 파이어폭스
			t.preventDefault();
		}else{
			event.returnValue = false;
		}
	} else {
		//alert('숫자만 입력 가능합니다.');
		if(window.netscape){   // 파이어폭스
			return true;
		}else{
			event.returnValue = true;
		}
	}	
}

// 날짜체크 
function checkValDate(){
	var f = document.frm;
	if(f.srchStartDate.value!='' && f.srchEndDate.value!=''){
		if(parseInt(f.srchStartDate.value,10)>parseInt(f.srchEndDate.value,10)){
			alert('만료일이 시작일보다 이전입니다.\n일자를 다시 확인하십시요');
			f.srchEndDate.value='';
		}
	}
}

// 페이지 이동
function goPage(pageNo){
	var frm = document.frm;

	frm.method = "post";
	frm.action = "/rsltInqr/rsltInqr.do?DIVS=20";
	frm.page_no.value = pageNo;
	frm.submit();
}

// 조회 
function fn_srchList(){
	var frm = document.frm;

	frm.method = "post";
	frm.action = "/rsltInqr/rsltInqr.do?DIVS=20&page_no=1";
	frm.submit();
}

// 상세화면이동
function goDetail(mastKey, prpsSeqn, divs){

	var frm = document.frm;
	
	frm.PRPS_MAST_KEY.value = mastKey;
	frm.PRPS_SEQN.value = prpsSeqn;
	frm.page_no.value = '${nowPage}';
	
	frm.method = "post";
	frm.action = "/rsltInqr/rsltInqr.do?method=inmtPrpsDetl&DIVS="+divs;
	frm.submit();
}

// 삭제
function fn_delete(){

	var frm = document.frm;
	var count = 0;
	
	var chkObjs = document.getElementsByName("chk");
	var totalStatObjs = document.getElementsByName("totalStat");
	var maxDealStatObjs = document.getElementsByName("maxDealStat");
	var actFlagYnObjs = document.getElementsByName("actFlagYn");
	var prpsMastKeyObjs = document.getElementsByName("prpsMastKey");
	var url;
	
	for(var i=0; i<chkObjs.length; i++){
		if(chkObjs[i].checked){
			if(totalStatObjs[i].value == '신청' && Number(maxDealStatObjs[i].value) > 1){
				alert("상세처리상태가 '신청'이상 진행된경우는 삭제가 불가능합니다.\n다시 선택 해 주세요.");
				chkObjs[i].checked = false;
				return;
			}else if(totalStatObjs[i].value != '신청'){
				alert("처리상태가 '신청'인 경우만 삭제가 가능합니다.\n다시 선택 해 주세요.");
				chkObjs[i].checked = false;
				return;
			}else{
				url = "/rsltInqr/rsltInqr.do?method=isValidDealStat&actFlagYn=Y&prpsMastKey=" + prpsMastKeyObjs[i].value;
				if(cfGetBooleanResponseReload(url) == false){
					alert("상세처리상태가 '신청'이상 진행된경우는 삭제가 불가능합니다.\n다시 선택 해 주세요.");
					chkObjs[i].checked = false;
					return;
				}
				actFlagYnObjs[i].value = 'Y';
				//frm.actFlagYn[i].value = 'Y';
			}
			count++;
		}
	}
	
	if(count == 0){
		alert('삭제대상을 선택 해 주세요.');
		return;
	}else{
		if(confirm('삭제 하시겠습니까?')){ 
			frm.method = "post";
			frm.action = "/rsltInqr/rsltInqr.do?method=deleteRsltInqr&DIVS=20&page_no=1";
			frm.submit();
		}
	}
	
}

/*
*  Function.js에 있으나 파이어폭스에서는 인식이 안되서 가져왔음
*/
function cfGetBooleanResponseReload(url,params,HttpMethod) {
	var xmlhttp = null;
	if(!HttpMethod){
	    HttpMethod = "GET";
	}
    if(window.ActiveXObject){
	      try {
	    	  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	      } catch (e) {
	        try {
	        	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP") ;
	        } catch (e2) {
	          return null ;
	        }
		}
	}
	//xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	if (xmlhttp == null) return true;
	xmlhttp.open(HttpMethod, url, false);
	xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');

	xmlhttp.send(params);
	
	if (xmlhttp.responseText == "true") {
		return true;
	} else {
		return false;
	}
}	  

//-->
</script>

</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_3.png" alt="신청현황조회" title="신청현황조회" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1">저작권찾기<span>&nbsp;</span></a></li>
							<li class="active"><a href="/rsltInqr/rsltInqr.do?DIVS=20&amp;page_no=1">보상금<span>&nbsp;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>신청현황조회</span>&gt;<strong>보상금</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>보상금 신청현황조회
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
										
					<form name="frm" action="" class="sch mt20">
					<input type="hidden" name="page_no"/>
					<input type ="hidden" name="PRPS_MAST_KEY" />
					<input type ="hidden" name="PRPS_SEQN" />
						<fieldset class="sch">
							<legend>게시판검색</legend>
							<div class="contentsSch">
								<span class="round lb">&nbsp;</span><span class="round rb">&nbsp;</span><span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span>
								<table border="1" summary="보상금 신청현황조회 검색항목 입력 폼입니다." class="w80"><!-- 스타일제거시 테이블형태 유지를 위해 보더를 준다 -->
									<caption>보상금신청현황조회 검색항목 입력</caption><!-- summary가 있을 경우 생략이 가능하다 -->
									<colgroup>
										<col width="18%">
										<col width="15%">
										<col width="20%">
										<col width="13%">
										<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<td rowspan="2"><img src="/images/2010/common/schBoxBg.gif" alt="Search" /></td>
											<th><label for="sch1">구분</label></th><!-- 레이블과 id로 타이틀과 인풋을 연결시켜준다 -->
											<td>
												<select id="sch1" name="srchPrpsDivs">
													<option value="">전체 -----</option>
													<option value="B" <c:if test="${srchPrpsDivs == 'B'}">selected="selected"</c:if>>방송음악 ---</option>
													<option value="S" <c:if test="${srchPrpsDivs == 'S'}">selected="selected"</c:if>>교과용 ----</option>
													<option value="L" <c:if test="${srchPrpsDivs == 'L'}">selected="selected"</c:if>>도서관 ----</option>
												</select>
											</td>
											<th><label for="sch2">신청일자</label></th>
											<td>
												<input class="inputData" type="text" id="sch2" name="srchStartDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_srchList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchStartDate}"/> 
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/> 
													~ 
												<input class="inputData" type="text" id="sch3" name="srchEndDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_srchList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchEndDate }"/> 
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" alt="달력" title="마지막 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>
											</td>
										</tr>
										<tr>
											<th><label for="sch4">검색어</label></th>
											<td colspan="3"><input name="srchTitle" class="inputData w50" id="sch4" value="${srchTitle}"/></td>
										</tr>
									</tbody>
								</table>
								<p class="btnArea top10"><input type="submit" class="imgSchBtn" onclick="javascript:fn_srchList();"/></p>
							</div>
						</fieldset>
					
					<div class="bbsSection mt10">
						<div class="floatDiv mb5">
							<p class="fl pt10 pl5"><input type="text" name="totalRow" id="totalRow" value="${rsltList.totalRow}건" style="border:0px;font-size:12px;font-weight:700;color:black;"/></p>
							<p class="fr"><span class="button medium icon"><span class="del">&nbsp;</span><a href="javascript:fn_delete();">삭제</a></span></p>
						</div>
						
						<!-- 테이블 영역입니다 -->
						<div class="tabelRound">
							<span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span><!-- 라운딩효과 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="보상금 신청내역 구분, 제목, 신청일자, 처리상태 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="5%">
								<col width="15%">
								<col width="*">
								<col width="15%">
								<col width="10%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col"><input type="checkbox" id="chk1231" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" style="cursor:pointer;" title="전체선택" /></th>
										<th scope="col">구분</th>
										<th scope="col">제목</th>
										<th scope="col">신청일자</th>
										<th scope="col">처리상태</th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${rsltList.totalRow == 0}">
									<tr>
									<td class="ce" colspan="5">검색된 신청내역이 없습니다.</td>
									</tr>
								</c:if>
								
								<c:if test="${rsltList.totalRow > 0}">
									<c:forEach items="${rsltList.resultList}" var="rsltList">
										<c:set var="NO" value="${rsltList.totalRow}"/>
										<c:set var="i" value="${i+1}"/>
									<tr>
										<td class="ce">
											<input type="checkbox" name="chk" class="vmid" style="cursor:pointer;" title="선택" />
											<input type="hidden" name="actFlagYn" value="N" />
											<input type ="hidden" name="prpsMastKey" value="${rsltList.PRPS_MAST_KEY}" />
											<input type="hidden" name="totalStat" value="${rsltList.TOTAL_STAT}" />
											<input type="hidden" name="maxDealStat" value="${rsltList.MAX_DEAL_STAT}" />
										</td>
										<td class="ce">${rsltList.PRPS_DIVS_NAME}</td>
										<td><a href="javascript:goDetail('${rsltList.PRPS_MAST_KEY}', '${rsltList.PRPS_SEQN}', '${rsltList.PRPS_DIVS}');">${rsltList.PRPS_TITE}</a></td>
										<td class="ce">${rsltList.RGST_DTTM}</td>
										<td class="ce">${rsltList.TOTAL_STAT}</td>
									</tr>	
									</c:forEach>
								</c:if>		
								</tbody>
							</table>
						</div>
						<!-- //테이블 영역입니다 -->
						
						<!-- 버튼과 페이징영역 -->
						<div class="pagination">
							 <jsp:include page="../common/PageList_2010.jsp" flush="true">
							  	<jsp:param name="totalItemCount" value="${rsltList.totalRow}" />
								<jsp:param name="nowPage"        value="${param.page_no}" />
								<jsp:param name="functionName"   value="goPage" />
								<jsp:param name="listScale"      value="" />
								<jsp:param name="pageScale"      value="" />
								<jsp:param name="flag"           value="M01_FRONT" />
								<jsp:param name="extend"         value="no" />
							</jsp:include>
						</div>
						<!-- //버튼과 페이징영역 -->
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>
