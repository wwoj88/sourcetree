<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(저작권찾기) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2010/calendar.css">
<style type="text/css">
<!--
body {
	overflow-x: auto;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
.box {
	background-color:black;
	position:absolute;
	z-index:9998;
	opacity:0.1;
}
.w62{width:62%};
.w68{width:68%};


-->
</style>

<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>


<script type="text/javascript">
<!--


window.name = "rghtPrpsModi_mvie";

// 영화저작물 상세 팝업오픈
function openMvieDetail( crId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=V'+param
	var name = '';
	var openInfo = 'target=rghtPrpsModi_mvie, width=705, height=450, scrollbars=yes';
	
	window.open(url, name, openInfo);
}

//str
jQuery(window).resize(function(e){
	var docuWidth = jQuery(document).width()-16;
	var docuHeight = jQuery(document).height();
	
	jQuery("#modalBox").css({width:docuWidth,height:docuHeight});
	
	 var yp=document.body.scrollTop;
	   var xp=document.body.scrollLeft;

	   var ws=document.body.clientWidth;
	   var hs=document.body.clientHeight;
	   
	   var ajaxBox=$('ajaxBox');
	   //$('ajaxBoxMent').innerHTML=ment;

	   ajaxBox.style.top=yp+eval(hs)/2-100+"px";
	   ajaxBox.style.left=xp+eval(ws)/2-100+"px";
});
//end

//로딩 이미지 박스
function showAjaxBox(ment){
	//항상 화면 중앙에 나타나도록...
   var yp=document.body.scrollTop;
   var xp=document.body.scrollLeft;
   

   var ws=document.body.clientWidth;
   var hs=document.body.clientHeight;
   
   if(!ment) ment="잠시만 기다려주세요..";
   
   var ajaxBox=$('ajaxBox');
   //$('ajaxBoxMent').innerHTML=ment;

   ajaxBox.style.top=yp+eval(hs)/2+100+"px";
   ajaxBox.style.left=xp+eval(ws)/2-100+"px";

   //str
	var docuWidth = jQuery(document).width()-16;
	var docuHeight = jQuery(document).height();
	
	
	//back_blackBox
	/* jQuery("<div>").addClass("box").attr("id","modalBox").
		css({width:docuWidth,height:docuHeight,left:0,top:0}).appendTo("body"); */
	
	jQuery("#modalBox").click(function(){
		jQuery("#modalBox").remove();
	})
	//end

  Element.show(ajaxBox);    
}

//로딩 이미지 박스 감추기
function hideAjaxBox(){
	Element.hide('ajaxBox');	
	
	//str
	jQuery("#modalBox").remove();
	//end
}

//리사이즈
function resizeIFrame(name) {
	var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
	document.getElementById(name).height= the_height+5;

}

// 파일다운로드
function fn_fileDownLoad(filePath, fileName, realFileName) {
		
		var frm = document.fileForm;
		
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.target="rghtPrpsModi_mvie";
		//frm.action = "/rsltInqr/rsltInqr.do?method=fileDownLoad";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.method="post";
		frm.submit();
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	
	
	var the_height = document.getElementById(name).offsetHeight;
	var chkObjs = document.getElementsByName("chk");
	  
	document.getElementById(targetName).style.height = the_height + 13 + "px" ;
	  

}

// 스크롤셋팅
function scrollSet(name, targetName, oRowName){
	
	var totalRow = document.getElementsByName(oRowName).length;
	
	// 세로 기준건수 이상인 경우 세로스크롤 제어
	if( totalRow < 6) {
		resizeDiv(name, targetName);
		document.getElementById(targetName).style.overflowY = "hidden";
	}
	
	if( totalRow > 5 ) {
		document.getElementById(targetName).style.overflowY = "auto";
	}
}


// 토탈건수 set
function setTotCnt( oRowName, oSpanId) {

	var totalRow = document.getElementsByName(oRowName).length;
	document.getElementById(oSpanId).innerHTML = "("+totalRow+"건)";
}



// 테이블 행추가/삭제
function editTable(type){
	
	// 1. 신청 목적 선택 먼저
	if ( document.getElementById("PRPS_RGHT_CODE").value == '')  {
		
		alert("신청 목적 선택 후 가능한 기능입니다.");
		
		document.getElementById("PRPS_RGHT_CODE").focus();
		
		return;
	}
	
	// 2. 신청 신탁관리단체 먼저
	var trstOrgnLen = document.getElementsByName("TRST_ORGN_CODE").length;
	var kCnt = 0;
	
	for( kk=0; kk<trstOrgnLen && kCnt ==0; kk++) {
		
		if(document.getElementsByName("TRST_ORGN_CODE")[kk].checked){
			kCnt += 1;	break;
		}
	}
	
	if(kCnt==0) {
		alert("권리구분 선택 후 가능한 기능입니다.");
		
		document.getElementsByName("TRST_ORGN_CODE")[0].focus();
		return;
	}
	
	// 3. tableId setting
	var tableId = "listTab";	//권리자/대리인 권리찾기
	var chkId = "iChk";
	var disSeq = "displaySeq";
	var totalRow = "totalRow";
	var divId = "div_1";
	var scrllDivId = "div_scroll_1";
	
	if(document.getElementById("PRPS_RGHT_CODE").value == '3')	{
		 tableId = "listTab_2";		//이용허락
		 chkId = "iChk2";
		 disSeq = "displaySeq2";
		 totalRow = "totalRow2";
		 divId = "div_2";
		 scrllDivId = "div_scroll_2";
	}
	
	if( type == 'D' ) {
	    if( tableId == 'listTab' ) {
		    var oTbl = document.getElementById(tableId);
		    var oChkDel = document.getElementsByName(chkId);
		    var iChkCnt = oChkDel.length;
		    var iDelCnt = 0;
		    
		    if(iChkCnt == 1 && oChkDel[0].checked == true){
		        oTbl.deleteRow(2);    oTbl.deleteRow(2);
		        iDelCnt++;
		    }else if(iChkCnt > 1){
	    	    for(i = iChkCnt-1; i >= 0; i--){
	    	        if(oChkDel[i].checked == true){    	            
	    	            oTbl.deleteRow(2*i+2);     
    	           		oTbl.deleteRow(2*i+2);
    	        	    iDelCnt++;
	    	        }
	    	    }
	    	}
    	
   	 		if(iDelCnt < 1){
    		    alert('삭제할 저작물을 선택하여 주십시요');
    		}
    	}
    	else{  	
	    	var oTbl = document.getElementById(tableId);
		    var oChkDel = document.getElementsByName(chkId);
		    var iChkCnt = oChkDel.length;
		    var iDelCnt = 0;
		    
		    if(iChkCnt == 1 && oChkDel[0].checked == true){
		        oTbl.deleteRow(1);
		        iDelCnt++;
		    }else if(iChkCnt > 1){
	    	    for(i = iChkCnt-1; i >= 0; i--){
	    	        if(oChkDel[i].checked == true){    	            
	    	            oTbl.deleteRow(i+1);
	    	            iDelCnt++;
	    	        }
	    	    }
	    	}
	    	
	    	if(iDelCnt < 1){
	    	    alert('삭제할 저작물을 선택하여 주십시요');
	    	}
    	}
   
    	fn_resetSeq(disSeq);
    		

	}else if( type == 'I' ) {
		
		newRow = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
		
		newSubRow = null;
		if( tableId == 'listTab' ) {
			newSubRow = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
		}
		
		newRow.onmouseover=function(){document.getElementById(tableId).clickedRowIndex=this.rowIndex}
		newRow.valign = 'middle';
		newRow.align = 'center';
		makeCell(tableId, newRow, document.getElementById(tableId).rows.length, newSubRow);
	
	}else if( type == 'A' ) {
		addCell(tableId);
	}
	
	// 스크롤에 의한 height reset
	//resizeDiv(tableId, divId);
	scrollSet(tableId, scrllDivId, chkId);
		
	// 토탈건수
	setTotCnt( chkId, totalRow);
}
	
    
//순번 재지정
function fn_resetSeq(disName){
    var oSeq = document.getElementsByName(disName);
    for(i=0; i<oSeq.length; i++){
        oSeq[i].value = i+1;
    }
}


//추가저작물에 대한 기존저작물 중복확인
function goWorksSearch(worksTitleId, chkId, index){
	
	var worksTitle = document.getElementById(worksTitleId).value;

	if(worksTitle == ''){
		alert('저작물명을 먼저 입력해 주세요.');
		document.getElementById(worksTitleId).focus();
		return;
	} else {
		
		// 중복확인
		document.getElementById(chkId).value = 'Y';	 
		
		// 검색시작
		var ifFrm = document.getElementById("ifMvieRghtSrch").contentWindow.document.ifFrm;
				
		ifFrm.method = "post";
		ifFrm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=V&parentGubun=Y&srchTitle="+worksTitle;
		ifFrm.page_no.value = 1;
		ifFrm.submit();
	}
}


//추가저작물에 대한 기존저작물 중복확인유무 체크
function worksSearchChk(worksTitleId, chkId, index){
	
	var worksTitle = document.getElementById(worksTitleId).value;
	
	if(document.getElementById(chkId).value !='Y') {
		
		alert('추가한 저작물명('+worksTitle+')을 먼저 [검색]해 주세요.');
		
		document.getElementById(worksTitleId).focus();
		return;
	}
}


//선택저작물 테이블 idx
var iRowIdx = 1;
var iRowIdx2 = 1;
       
function makeCell(tableId, cur_row, rowPos, sub_row) {
	
	var i=0;
	var j=0;
	
	var class_211 = "inputDisible2"; 	var read_211 = "readonly";
	var class_212 = "inputDisible2"; 	var read_212 = "readonly";
	var class_213 = "inputDisible2"; 	var read_213 = "readonly";
	var class_214 = "inputDisible2"; 	var read_214 = "readonly";
	
	// 신청목적에 따라 생성 td 가 다르다.
	var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
	
	if( document.getElementById("CHK_211").checked )	{
		class_211 = "inputDataN";		read_211 = "";	
		class_214 = "inputDataN";		read_214 = "";	// 투자
	}
	
	if( document.getElementById("CHK_212").checked )	{
		class_212 = "inputDataN";		read_212 = "";
	}
	
	if( document.getElementById("CHK_213").checked )	{
		class_211 = "inputDataN";		read_211 = "" // 배급사에 쓰인다.
		class_213 = "inputDataN";		read_213 = "";
		class_214 = "inputDataN";		read_214 = "";	// 투자
	}
	
	// 권리자/대리인 권리찾기
	if( tableId == 'listTab' ) {
		
		//체크박스
		cur_cell = cur_row.insertCell(i++);
		cur_cell.rowSpan = 2;
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk\"  id=\"iChk_'+iRowIdx+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		//순번
		cur_cell = cur_row.insertCell(i++);
		cur_cell.rowSpan = 2;
		cur_cell.className = 'ce pd5';
		innerHTMLStr = '<input name=\"displaySeq\" id=\"displaySeq_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작물 정보
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'lft pd5';
		innerHTMLStr = '<input name=\"TITLE\"  id=\"TITLE_'+iRowIdx+'\" class="inputDataN w62" title=\"저작물명\" rangeSize="~100" nullCheck onkeypress=\"javascript:if(event.keyCode==13){goWorksSearch(\'TITLE_'+iRowIdx+'\', \'searchChk_'+iRowIdx+'\',\''+iRowIdx+'\');}\" />';
		// 저작물정보 중복확인 Proc 추가(20101214)
		innerHTMLStr += ' <span class=\"button small type3\" title="저작물명으로 기존저작물정보를 검색합니다." style="margin-top:1px;"><a href=\"javascript:goWorksSearch(\'TITLE_'+iRowIdx+'\', \'searchChk_'+iRowIdx+'\',\''+iRowIdx+'\');\">검색<\/a><\/span>';
		innerHTMLStr += '<input type=\"hidden\" name=\"searchChk\" id=\"searchChk_'+iRowIdx+'\"> ';
		cur_cell.innerHTML = innerHTMLStr;
		
		//연출/감독
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"DIRECTOR\"  id=\"DIRECTOR_'+iRowIdx+'\" class="inputDataN w95" title=\"감독/연출\" rangeSize="~100" nullCheck onfocus=\"javascript:worksSearchChk(\'TITLE_'+iRowIdx+'\', \'searchChk_'+iRowIdx+'\',\''+iRowIdx+'\');\" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		
		//제작일자
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		//innerHTMLStr = '<input name=\"PRODUCE_DATE\"  id=\"PRODUCE_DATE_'+iRowIdx+'\" class="inputDataN w90" title=\"제작일자\" character="KE"  maxlength="8" rangeSize="~8" dateCheck nullCheck />';
		innerHTMLStr = '<input class="inputDataN w70" type="text" id=\"PRODUCE_DATE_'+iRowIdx+'\" name=\"PRODUCE_DATE_'+iRowIdx+'\" size="8" maxlength="8" title=\"제작일자\" character="KE" dateCheck nullCheck value=""/> ';
		innerHTMLStr += '<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'PRODUCE_DATE_'+iRowIdx+'\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'PRODUCE_DATE_'+iRowIdx+'\');" alt="달력" title="제작일자를 선택하세요." align="middle" style="cursor:pointer;"/>'
		innerHTMLStr += '<input name=\"PRODUCE_DATE\" type="hidden" class="inputDataN w75" value="'+iRowIdx+'"/>';
		cur_cell.innerHTML = innerHTMLStr;
		
		//매체
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"MEDIA_CODE_VALUE\"  id=\"MEDIA_CODE_VALUE_'+iRowIdx+'\" class="inputDataN w95" title=\"매체\" rangeSize="~100" nullCheck/>';
		cur_cell.innerHTML = innerHTMLStr;
				
		//작가
		cur_cell = cur_row.insertCell(i++);
		cur_cell.rowSpan = 2;
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"WRITER\" id=\"WRITER_'+iRowIdx+'\" class="'+class_212+' w95" '+read_212+' title="작가" rangeSize="~100" />';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="PRODUCER_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="INVESTOR_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="DISTRIBUTOR_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="WRITER_ORGN" />';
		
		innerHTMLStr += '<!-- 사용자정보입력확인 -->';
		innerHTMLStr += '<input type="hidden" name="CHK_PRODUCER" />';
		innerHTMLStr += '<input type="hidden" name="CHK_INVESTOR" />';
		innerHTMLStr += '<input type="hidden" name="CHK_DISTRIBUTOR" />';
		innerHTMLStr += '<input type="hidden" name="CHK_WRITER" />';
		
		cur_cell.innerHTML = innerHTMLStr;
		
		
		//주요출연진
		cur_cell = sub_row.insertCell(j++);
		cur_cell.className = 'lft pd5';
		innerHTMLStr = '<input name=\"LEADING_ACTOR\"  id=\"LEADING_ACTOR_'+iRowIdx+'\" class="inputDataN w95" title=\"출연진\" rangeSize="~400" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		
		// 제작사
		cur_cell = sub_row.insertCell(j++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"PRODUCER\" id=\"PRODUCER_'+iRowIdx+'\" class="'+class_213+' w95" '+read_213+' title="제작사" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;

		//투자사
		cur_cell = sub_row.insertCell(j++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"INVESTOR\" id=\"INVESTOR_KOR_'+iRowIdx+'\" class="'+class_214+' w95" '+read_214+' title="투자사" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		//배급사
		cur_cell = sub_row.insertCell(j++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"DISTRIBUTOR\" id=\"DISTRIBUTOR_'+iRowIdx+'\" class="'+class_211+' w95" '+read_211+' title="배급사" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		fn_resetSeq("displaySeq");
		
		// setfocus
		//var oFuc = 'MUSIC_TITLE_'+iRowIdx;
		//	document.getElementById(oFuc).focus();
		
		iRowIdx++;
	}
	
	// 이용자 권리조회
	else if( tableId == 'listTab_2' ) {
	//alert(iRowIdx2);
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk2\"  id=\"iChk2_'+iRowIdx2+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce pd5';
		innerHTMLStr = '<input name=\"displaySeq2\" id=\"displaySeq2_'+iRowIdx2+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작물 정보
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'lft pd5';
		innerHTMLStr = '<input name=\"TITLE\"  id=\"TITLE_'+iRowIdx2+'\" class="inputDataN w65" title=\"저작물명\" rangeSize="~100" nullCheck onkeypress=\"javascript:if(event.keyCode==13){goWorksSearch(\'TITLE_'+iRowIdx2+'\', \'searchChk_'+iRowIdx2+'\',\''+iRowIdx2+'\');}\" />';
		// 저작물정보 중복확인 Proc 추가(20101214)
		innerHTMLStr += ' <span class=\"button small type3\" title="저작물명으로 기존저작물정보를 검색합니다." style="margin-top:1px;"><a href=\"javascript:goWorksSearch(\'TITLE_'+iRowIdx2+'\', \'searchChk_'+iRowIdx2+'\',\''+iRowIdx2+'\');\">검색<\/a><\/span>';
		innerHTMLStr += '<input type=\"hidden\" name=\"searchChk\" id=\"searchChk_'+iRowIdx2+'\"> ';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"DIRECTOR\"  id=\"DIRECTOR_'+iRowIdx2+'\" class="inputDataN w95" title=\"감독/연출\" rangeSize="~100" nullCheck onfocus=\"javascript:worksSearchChk(\'TITLE_'+iRowIdx2+'\', \'searchChk_'+iRowIdx2+'\',\''+iRowIdx2+'\');\" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"LEADING_ACTOR\"  id=\"LEADING_ACTOR_'+iRowIdx2+'\" class="inputDataN w95" title=\"출연진\" rangeSize="~400" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		//innerHTMLStr = '<input name=\"PRODUCE_DATE\"  id=\"PRODUCE_DATE_'+iRowIdx2+'\" class="inputDataN w90" title=\"제작일자\" character="KE"  maxlength="8" rangeSize="~8" dateCheck nullCheck />';
		innerHTMLStr = '<input class="inputDataN w65" type="text" id=\"VAL_PRODUCE_DATE_'+iRowIdx2+'\" name=\"VAL_PRODUCE_DATE_'+iRowIdx2+'\" size="8" maxlength="8" title=\"제작일자\" dateCheck nullCheck value=""/> ';
		innerHTMLStr += '<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'VAL_PRODUCE_DATE_'+iRowIdx2+'\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'VAL_PRODUCE_DATE_'+iRowIdx2+'\');" alt="달력" title="제작일자를 선택하세요." align="middle" style="cursor:pointer;"/>'
		innerHTMLStr += '<input name=\"VAL_PRODUCE_DATE\" type="hidden" class="inputDataN w90" value="'+iRowIdx2+'"/>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"MEDIA_CODE_VALUE\"  id=\"MEDIA_CODE_VALUE_'+iRowIdx2+'\" class="inputDataN w95" title=\"매체\" rangeSize="~100" nullCheck/>';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="PRODUCER" />';
		innerHTMLStr += '<input type="hidden" name="INVESTOR" />';
		innerHTMLStr += '<input type="hidden" name="DISTRIBUTOR" />';
		innerHTMLStr += '<input type="hidden" name="WRITER" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		
		fn_resetSeq("displaySeq2");
		
		// setfocus
		//	var oFuc = 'MUSIC_TITLE_'+iRowIdx2;
		//	document.getElementById(oFuc).focus();
		
		iRowIdx2++;
	}
}


function addCell(tableId, cur_row, rowPos) {

	var class_211 = "inputDisible2"; 	var read_211 = "readonly";
	var class_212 = "inputDisible2"; 	var read_212 = "readonly";
	var class_213 = "inputDisible2"; 	var read_213 = "readonly";

	var class_211 = "inputDisible2"; 	var read_211 = "readonly";
	var class_212 = "inputDisible2"; 	var read_212 = "readonly";
	var class_213 = "inputDisible2"; 	var read_213 = "readonly";
	
	// 신청목적에 따라 생성 td 가 다르다.
	var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
	
	if( document.getElementById("CHK_211").checked )	{
		class_211 = "inputDataN";		read_211 = "";	
	}
	
	if( document.getElementById("CHK_212").checked )	{
		class_212 = "inputDataN";		read_212 = "";
	}
	
	if( document.getElementById("CHK_213").checked )	{
		class_211 = "inputDataN";		read_211 = "" // 배급사에 쓰인다.
		class_213 = "inputDataN";		read_213 = "";
	}
	
	//Iframe
	var chkObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("ifrmChk");
	var mvieTitleObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("mvieTitle");
	var directorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("director");
	var leadingActorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("leadingActor");
	var viewGradeValueObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("viewGradeValue");
	var produceDateObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("produceDate");
	var produceYearObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("produceYear");
	var producerObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("producer");	
	var distributorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("distributor");			
	var investorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("investor");
	var mediaCodeValueObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("mediaCodeValue");

	var crIdObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("crId");
	var nrIdObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("nrId");	
	var albumIdObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("albumId");	
	
	// 선택 목록
	var oSelTable = document.getElementById("listTab");
	var oSelTable2 = document.getElementById("listTab_2");

	var selChkObjs = oSelTable.getElementsByTagName("input");
	var selChkObjs2 = oSelTable2.getElementsByTagName("input");

	var selChkCnt = 0;
	var selChkCnt2 = 0;
	
	var isExistYn = "N";	//중복여부	
	var isAdd	= false;	//줄 추가여부

	var count = 0;
	
	for(var cn = 0; cn < chkObjs.length; cn++) {
		if(chkObjs[cn].checked == true){

			if( tableId == 'listTab') {
				//중복여부 검사
				for(var sn=0; sn<selChkObjs.length; sn++) {
					if(chkObjs[cn].checked == true && selChkObjs[sn].name == 'iChkVal') {
						if((crIdObjs[cn].value +'|'+ albumIdObjs[cn].value+'|'+ nrIdObjs[cn].value ) == selChkObjs[sn].value) {
							isExistYn = "Y";
						}
						selChkCnt++;
					}
				}
			}

			if( tableId == 'listTab_2') {
				//중복여부 검사
				for(var sn=0; sn<selChkObjs2.length; sn++) {
					if(chkObjs[cn].checked == true && selChkObjs2[sn].name == 'iChkVal') {
						if((crIdObjs[cn].value +'|'+albumIdObjs[cn].value+'|'+ nrIdObjs[cn].value ) == selChkObjs2[sn].value) {
							isExistYn = "Y";
						}
					}
					selChkCnt2++;
				}
			}
			
			if(isExistYn == "N") {
				//줄 추가여부
				isAdd = true;
				count ++;
			}else{
				alert("선택된 저작물 ["+mvieTitleObjs[cn].value+"]는 이미 추가된 저작물입니다.");
				return;
			}
			

			if( tableId == 'listTab' && selChkCnt > 0) { //권리찾기 선택목록이 비웠을때 
				//줄 추가여부		
				isAdd = true;
				count ++;
			}

			if( tableId == 'listTab_2' && selChkCnt2 > 0) { //권리찾기 선택목록이 비웠을때 
				//줄 추가여부		
				isAdd = true;
				count ++;
			}
		}

		chkObjs[cn].checked = false;  // 처리한 후 체크풀기 


		//줄 추가실행
		if(isAdd){
			var cur_row = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
			
			cur_row.onmouseover=function(){document.getElementById(tableId).clickedRowIndex=this.rowIndex}
			cur_row.valign = 'middle';
			cur_row.align = 'center';
			
			var rowPos = document.getElementById(tableId).rows.length;
	
			var i=0;
			
			var class_211 = "inputDisible2"; 	var read_211 = "readonly";
			var class_212 = "inputDisible2"; 	var read_212 = "readonly";
			var class_213 = "inputDisible2"; 	var read_213 = "readonly";
			var class_214 = "inputDisible2"; 	var read_214 = "readonly";
		
			if( document.getElementById("CHK_211").checked )	{
				class_211 = "inputDataN";		read_211 = "";	
				class_214 = "inputDataN";		read_214 = "";	// 투자
			}
			
			if( document.getElementById("CHK_212").checked )	{
				class_212 = "inputDataN";		read_212 = "";
			}
			
			if( document.getElementById("CHK_213").checked )	{
				class_211 = "inputDataN";		read_211 = "" // 배급사에 쓰인다.
				class_213 = "inputDataN";		read_213 = "";
				class_214 = "inputDataN";		read_214 = "";	// 투자
			}
			
			// 신청목적에 따라 생성 td 가 다르다.
			var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
			
			if( document.getElementById("CHK_211").checked )	{
				classId = "inputDataN";		read = "";
			}
			
			// 권리자/대리인 권리찾기
			if( tableId == 'listTab' ) {
				
				var cur_row2 = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
				var j=0;
				
				//체크박스
				cur_cell = cur_row.insertCell(i++);
				cur_cell.rowSpan = 2;
				cur_cell.className = 'ce pd5';
				innerHTMLStr = '<input type=\"checkbox\" name=\"iChk\" id=\"iChk_'+iRowIdx+'\" class=\"vmid\"  />';
				cur_cell.innerHTML = innerHTMLStr;
				
				//순번
				cur_cell = cur_row.insertCell(i++);
				cur_cell.rowSpan = 2;
				cur_cell.className = 'ce pd5';
				innerHTMLStr = '<input name=\"displaySeq\" id=\"displaySeq_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly=\"readonly\" value=\"'+iRowIdx+'\"/>';
				cur_cell.innerHTML = innerHTMLStr;
				
				// 저작물 정보
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'lft pd5';
				innerHTMLStr = '<a class=\"underline black2\" href=\"javascript:openMvieDetail(\''+crIdObjs[cn].value+'\', \''+albumIdObjs[cn].value+'\', \''+nrIdObjs[cn].value+'\');\">'+mvieTitleObjs[cn].value+'<\/a>';
				cur_cell.innerHTML = innerHTMLStr;
				
				//연출
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = directorObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				//제작일자
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = produceDateObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				//매체
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = mediaCodeValueObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				//작가
				cur_cell = cur_row.insertCell(i++);
				cur_cell.rowSpan = 2;
				cur_cell.className = 'ce pd5';
				innerHTMLStr = '<input name=\"WRITER\" id=\"WRITER_'+iRowIdx+'\" class=\"'+class_212+' w95\" '+read_212+' value=\"\" title=\"작가\" rangeSize=\"~100\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"iChkVal\" value=\''+crIdObjs[cn].value+'|'+nrIdObjs[cn].value+'|'+albumIdObjs[cn].value+'\'/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"TITLE\" value=\''+fncReplaceStr(mvieTitleObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"DIRECTOR\" value=\''+fncReplaceStr(directorObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"LEADING_ACTOR\" value=\''+fncReplaceStr(leadingActorObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" id=\"PRODUCE_DATE_'+iRowIdx+'\" name=\"PRODUCE_DATE_'+iRowIdx+'\" value=\''+produceDateObjs[cn].value+'\' />';
				innerHTMLStr += '<input name=\"PRODUCE_DATE\" type="hidden" value="'+iRowIdx+'"/>';
				
				innerHTMLStr += '<input type=\"hidden\" name=\"MEDIA_CODE_VALUE\" value=\''+mediaCodeValueObjs[cn].value+'\' />';
				
				innerHTMLStr += '<input type=\"hidden\" name=\"PRODUCER_ORGN\" value=\''+fncReplaceStr(producerObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"INVESTOR_ORGN\" value=\''+fncReplaceStr(investorObjs[cn].value, '"', '&quot;')+'\'/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"DISTRIBUTOR_ORGN\" value=\''+fncReplaceStr(directorObjs[cn].value, '"', '&quot;')+'\'/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"WRITER_ORGN\" value=\'\'/>';
				
				innerHTMLStr += '<!-- 사용자정보입력확인 -->';
				innerHTMLStr += '<input type="hidden" name="CHK_PRODUCER" />';
				innerHTMLStr += '<input type="hidden" name="CHK_INVESTOR" />';
				innerHTMLStr += '<input type="hidden" name="CHK_DISTRIBUTOR" />';
				innerHTMLStr += '<input type="hidden" name="CHK_WRITER" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				
				
				
				//주요출연진
				cur_cell = cur_row2.insertCell(j++);
				cur_cell.className = 'lft pd5';
				innerHTMLStr = leadingActorObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				// 제작사
				cur_cell = cur_row2.insertCell(j++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = '<input name=\"PRODUCER\" id=\"PRODUCER_'+iRowIdx+'\" class=\"'+class_213+' w95\" '+read_213+' value=\''+fncReplaceStr(producerObjs[cn].value, '"', '&quot;')+'\' title=\"제작사\" rangeSize=\"~100\"  />';
				cur_cell.innerHTML = innerHTMLStr;
		
				//투자사	
				cur_cell = cur_row2.insertCell(j++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = '<input name=\"INVESTOR\" id=\"INVESTOR_'+iRowIdx+'\" class=\"'+class_214+' w95\" '+read_214+' value=\''+fncReplaceStr(investorObjs[cn].value, '"', '&quot;')+'\' title=\"투자사\" rangeSize=\"~100\"  />';
				cur_cell.innerHTML = innerHTMLStr;
				
				//배급사
				cur_cell = cur_row2.insertCell(j++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = '<input name=\"DISTRIBUTOR\" id=\"DISTRIBUTOR_'+iRowIdx+'\" class=\"'+class_211+' w95\" '+read_211+' value=\''+fncReplaceStr(distributorObjs[cn].value, '"', '&quot;')+'\' title=\"배급사\" rangeSize=\"~100\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				
				fn_resetSeq("displaySeq");
				
				// setfocus
				//var oFuc = 'MUSIC_TITLE_'+iRowIdx;
				//	document.getElementById(oFuc).focus();
				
				iRowIdx++;
			}
			
			// 이용자 권리조회
			else if( tableId == 'listTab_2' ) {
			//alert(iRowIdx2);
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'pd5';
				innerHTMLStr = '<input type=\"checkbox\" name=\"iChk2\" id=\"iChk2_'+iRowIdx2+'\" class=\"vmid\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = '<input name=\"displaySeq2\" id=\"displaySeq2_'+iRowIdx2+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly=\"readonly\" value=\"'+iRowIdx2+'\"/>';
				cur_cell.innerHTML = innerHTMLStr;
				
				// 저작물 정보
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'lft pd5';
				//innerHTMLStr = '<a href=\"javascript:openMvieDetail(\''+crIdObjs[cn].value+'\');\"><u>'+mvieTitleObjs[cn].value+'<\/u><\/a>';
				innerHTMLStr = '<a class=\"underline black2\" href=\"javascript:openMvieDetail(\''+crIdObjs[cn].value+'\', \''+albumIdObjs[cn].value+'\', \''+nrIdObjs[cn].value+'\');\">'+mvieTitleObjs[cn].value+'<\/a>';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = directorObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'pd5';
				innerHTMLStr = leadingActorObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'pd5';
				innerHTMLStr = produceDateObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'pd5';
				innerHTMLStr = mediaCodeValueObjs[cn].value;
				innerHTMLStr += '<input type=\"hidden\" name=\"iChkVal\" value=\''+crIdObjs[cn].value+'|'+nrIdObjs[cn].value+'|'+albumIdObjs[cn].value+'\'/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"TITLE\" value=\''+fncReplaceStr(mvieTitleObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"DIRECTOR\" value=\''+fncReplaceStr(directorObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"LEADING_ACTOR\" value=\''+fncReplaceStr(leadingActorObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" id=\"VAL_PRODUCE_DATE_'+iRowIdx2+'\" name=\"VAL_PRODUCE_DATE_'+iRowIdx2+'\" value=\''+produceDateObjs[cn].value+'\' />';
				innerHTMLStr += '<input name=\"VAL_PRODUCE_DATE\" type="hidden" value="'+iRowIdx2+'"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"MEDIA_CODE_VALUE\" value=\''+mediaCodeValueObjs[cn].value+'\' />';
				
				innerHTMLStr += '<input type=\"hidden\" name=\"PRODUCER\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"INVESTOR\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"DISTRIBUTOR\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"WRITER\" />';
				
				cur_cell.innerHTML = innerHTMLStr;
				
				
				fn_resetSeq("displaySeq2");
				
				// setfocus
				//	var oFuc = 'MUSIC_TITLE_'+iRowIdx2;
				//	document.getElementById(oFuc).focus();
				
				iRowIdx2++;
			}
			isAdd = false;
		}
	}

	if(count == 0){
		alert('선택된 저작물이 없습니다.');
		return;
	}	
	
}

// 신청목적 선택
function prps_check(chk) {
	
	// 권리자 권리찾기
	if(chk.value == '1' || chk.value == '2' ) {
		document.getElementById("div_1").style.display = "";
		document.getElementById("div_2").style.display = "none";
		fn_lock( "listTab", "" );
		fn_lock( "listTab_2", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab", "div_scroll_1", "iChk");
		
		// 토탈건수
		setTotCnt( "iChk", "totalRow");

		// 첨부파일명 및 안내문구 제어
		document.getElementById("attFileTitle").innerHTML = "첨부파일(증빙서류)";		
		document.getElementById("attFileMemo1").style.display = "block";

	} else {
		document.getElementById("div_2").style.display = "";
		document.getElementById("div_1").style.display = "none";
		fn_lock( "listTab_2", "" );
		fn_lock( "listTab", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab_2", "div_scroll_2", "iChk2");
		
		// 토탈건수
		setTotCnt( "iChk2", "totalRow2");

		// 첨부파일명 및 안내문구 제어
		document.getElementById("attFileTitle").innerHTML = "첨부파일";		
		document.getElementById("attFileMemo1").style.display = "none";
	}
	
}

// 신탁관리단체 선택
function trust_check(chk) {
	
	var nRow = document.getElementsByName("PRODUCER").length;	// 전체 길이
	
	var oInPro= document.getElementsByName("PRODUCER");
	var oInInv = document.getElementsByName("INVESTOR");
	var oInDis = document.getElementsByName("DISTRIBUTOR");
	
	var oInWri = document.getElementsByName("WRITER");
		
	for( k=0; k<nRow; k++) {
		
		// 시나리오작가협
		if(chk.id=="CHK_212") {
		
			if(chk.checked){
				oInWri[k].readOnly = false;
				oInWri[k].className = 'inputDataN w95';
			}
			else {
				oInWri[k].readOnly = true;
				oInWri[k].className = 'inputDisible2 w95';
			}
		}
		
		// 영제협
		if(chk.id=="CHK_213") {
		
			if(chk.checked){
				oInPro[k].readOnly = false;
				//oInInv[k].readOnly = false;
				//oInDis[k].readOnly = false;
				
				oInPro[k].className = 'inputDataN w95';
				//oInInv[k].className = 'inputDataN w90';
				//oInDis[k].className = 'inputDataN w90';
			}
			else {
				oInPro[k].readOnly = true;
			//	oInInv[k].readOnly = true;
			//	oInDis[k].readOnly = true;
				
				oInPro[k].className = 'inputDisible2 w95';
			//	oInInv[k].className = 'inputDisible2 w90';
			//	oInDis[k].className = 'inputDisible2 w90';
			}
		}
		
				
		// 영산협 || 영제협 :  배급사
		if(chk.id=="CHK_211" || chk.id=="CHK_213") {
			
			if(document.getElementById("CHK_211").checked || document.getElementById("CHK_213").checked){
				oInDis[k].readOnly = false;
				oInInv[k].readOnly = false;
				
				oInDis[k].className = 'inputDataN w95';
				oInInv[k].className = 'inputDataN w95';
			}
			else {
				oInDis[k].readOnly = true;
				oInInv[k].readOnly = true;
				
				oInDis[k].className = 'inputDisible2 w95';
				oInInv[k].className = 'inputDisible2 w95';
			}
		}
		
	} // .. end for
}

// 권리찾기수정
function rghtPrpsModi() {

	var frm = document.prpsForm;

	var trstOrgnLen = document.getElementsByName("TRST_ORGN_CODE").length;
	var kCnt = 0;
	
	for( kk=0; kk<trstOrgnLen && kCnt ==0; kk++) {
		
		if(document.getElementsByName("TRST_ORGN_CODE")[kk].checked){
			kCnt += 1;	break;
		}
	}
	
	if(kCnt==0) {
		alert('권리구분을(를) 선택하세요.');
		
		document.getElementsByName("TRST_ORGN_CODE")[0].focus();
		return;
	}
	
	/*if( !document.getElementById("CHK_211").checked ){
		alert('권리구분을(를) 선택하세요.');
		document.getElementById("CHK_201").focus();
		return;
	}
	*/
	<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">
	if( document.getElementsByName("iChk").length <1 ){
		alert('신청 저작물(를) 추가하세요.');
		document.getElementById("workAdd").focus();
		return;
	}
	</c:if>
	
	<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">
	if( document.getElementsByName("iChk2").length <1 ){
		alert('신청 저작물(를) 추가하세요.');
		document.getElementById("workAdd2").focus();
		return;
	}
	</c:if>
			
	if(checkForm(frm)) {
		// 신청자 정보입력 확인 : 기존 정보/신청정보 동일유무 확인 (권리자 권리찾기인 경우만)
		if( frm.PRPS_RGHT_CODE.value == '1') {
			if( !dataConfirmcheck())
				return;
		}
		
		frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrpsModiProc&DIVS=V";
		
		frm.method="post";
		frm.submit();

	}
}

// 테이블 하위 disable
function fn_lock( tableId, flag ){
	
	var oTbl = document.getElementById(tableId);
	var oInput = oTbl.getElementsByTagName("input");
	
	for(i=0; i<oInput.length;i++){
			oInput[i].disabled= flag ;
	}
}


// 신청자 정보입력 확인
function dataConfirmcheck() {
	
	var mesg;
	var reVal = true;
	
	var oTitle = document.getElementsByName("TITLE");
	var oFoc = document.getElementsByName("iChk");
	
	// 확인여부
	/*
	var oChkPro = document.getElementsByName("CHK_PRODUCER");
	var oChkInv = document.getElementsByName("CHK_INVESTOR");
	var oChkDis = document.getElementsByName("CHK_DISTRIBUTOR");
	*/
	
	// 신청정보
	var oPro = document.getElementsByName("PRODUCER");
	var oInv = document.getElementsByName("INVESTOR");
	var oDis = document.getElementsByName("DISTRIBUTOR");
	var oWri = document.getElementsByName("WRITER");
	
	// 기존정보
	var oProOrgn = document.getElementsByName("PRODUCER_ORGN");
	var oInvOrgn = document.getElementsByName("INVESTOR_ORGN");
	var oDisOrgn = document.getElementsByName("DISTRIBUTOR_ORGN");
	var oWriOrgn = document.getElementsByName("WRITER_ORGN");
	
	var nRow =oFoc.length;	// 전체 길이
	
	for( k=0; k<nRow; k++) {
	
		var totCnt =0;
		
		if( document.getElementById("CHK_213").checked ) {
			// 제작사
			totCnt += dataConfirm2( oPro[k], oProOrgn[k]);
			
			// 투자사
			totCnt += dataConfirm2( oInv[k], oInvOrgn[k]);
			
			// 배급사
			totCnt += dataConfirm2( oDis[k], oDisOrgn[k]);
		}
		
		if( document.getElementById("CHK_212").checked ) {
			// 작가
			totCnt += dataConfirm2( oWri[k], oWriOrgn[k]);
		}
		
		if( document.getElementById("CHK_211").checked ) {
			// 배급사
			totCnt += dataConfirm2( oDis[k], oDisOrgn[k]);
			
			// 투자사
			totCnt += dataConfirm2( oInv[k], oInvOrgn[k]);
		}
		
		// 수정된 정보가 없다면.
		if(totCnt == 0){
			alert( "신청 저작물정보 ["+oTitle[k].value+"] 권리정보가 입력 또는 변경되지 않았습니다. \n해당저작물을 삭제하거나 권리정보를 변경해주세요.");
			oFoc[k].focus();	reVal = false;
			return;
		}
	}
	
	return reVal;
}

// 진행의 경우 return true
function dataConfirm2( oNew, oOrg){
	
	var reVal = 0;
	var mesg = oNew.title;
	
	if( oNew.value == oOrg.value || oNew.value == '') {
	
		reVal = 0
	
	} else {
		reVal = 1;
	}
	
	return reVal;
}



// 오프라인 접수
function offLine_check(chk) {
	
	if(chk.checked) {
		document.getElementById("td_file_no").style.display = "";
		document.getElementById("td_file_yes").style.display = "none";
	} else{
		document.getElementById("td_file_no").style.display = "none";
		document.getElementById("td_file_yes").style.display = "";
	} 
}

function goList(){
	
	var frm = document.srchForm;
	frm.action = '/rsltInqr/rsltInqr.do?DIVS=10&page_no=1';
	frm.method="post";
	frm.submit();
}

function showAttach(cnt) {
	var i=0;
	var content = "";
	var attach = document.getElementById("attach")
	attach.innerHTML = "";
	//document.all("attach").innerHTML = "";
	content += '<table>';
	for (i=0; i<cnt; i++) {
		content += '<tr>';
		content += '<td><input type="file" name="attachfile'+(i+1)+'" size="70;" class="inputFile"><\/td>';
		content += '<\/tr>';
	}
	content += '<\/table>';
	//document.all("attach").innerHTML = content;
	attach.innerHTML = content;
}

function applyAtch() {
	var ansCnt = document.prpsForm.atchCnt.value;
	showAttach(parseInt(ansCnt));
}

 function initParameter(){
	
	// 신청목적에 따른 저작물 테이블 제어
	prps_check(document.getElementById("PRPS_RGHT_CODE"));
	
	// 신청 관리단체에 따른 저작물 테이블 제어
	trust_check(document.getElementById("CHK_211"));
	trust_check(document.getElementById("CHK_212"));
	trust_check(document.getElementById("CHK_213"));
	
	fn_createTable2("divFileList", "0");
	fn_setFileInfo();
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 

//리사이즈
function resizeIFrame(name) {

var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
document.getElementById(name).height= the_height+5;

}

function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}
//-->

//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_6.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
				<!-- 래프 -->
				<jsp:include page="/include/2012/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1","lnb11");
				</script>
				<!-- //래프 -->
					<div id="ajaxBox" style="position:absolute; z-index:9999; background: url(/images/2012/common/loadingBg.gif) no-repeat 0 0; left:-500px; width: 402px; height: 56px; padding: 102px 0 0 0;">
				</div>
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>신청현황</span><em>저작권정보 변경신청</em></p>
					<h1><img src="/images/2012/title/content_h1_0601.gif" alt="저작권정보 변경신청" title="저작권정보 변경신청" /></h1>
					
					<div class="section">
						<!-- memo 삽입 -->
						<jsp:include page="/common/memo/2011/memo_01.jsp">
							<jsp:param name="DIVS" value="${DIVS}"/>
						</jsp:include>
						<!-- // memo 삽입 -->
						<form name="fileForm" action="#">
							<!-- 파일다운로드 -->
							<input type="hidden" name="filePath" />
							<input type="hidden" name="fileName" />
							<input type="hidden" name="realFileName" />
							<input type="submit" style="display:none;">
						</form>
						<form name="prpsForm" enctype="multipart/form-data" action="#">
							<input type="hidden" name="USER_IDNT" value="${rghtPrps.USER_IDNT}"/>
							<input type="hidden" name="PRPS_MAST_KEY" value="${rghtPrps.PRPS_MAST_KEY}"/>
							<input type="hidden" name="PRPS_SEQN" value="${rghtPrps.PRPS_SEQN}"/>
							<input type="submit" style="display:none;">
						<!-- article str -->
						<div class="article mt20">
							<div class="floatDiv">
								<h2 class="fl">영화저작권정보 변경신청 수정</h2>
							</div>
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="저작권찾기 신청정보 폼 입니다.">
								<colgroup>
								<col width="20%">
								<%-- <col width="11%"> --%>
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row" class="bgbr lft"><label class="necessary">신청목적</label></th>
										<td scope="row">
										<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">
											권리자의 저작권찾기	
										</c:if>
										<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">
											이용자의 저작권조회
										</c:if>
										<input type="hidden" name="PRPS_RGHT_CODE" id="PRPS_RGHT_CODE" value="${rghtPrps.PRPS_RGHT_CODE}" />
										</td>
									</tr>
									<tr>
										<th scope="row">신청인정보</th>
										<td>
											<span class="topLine2"></span>
											<!-- 그리드스타일 -->
											<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
												<colgroup>
												<col width="15%">
												<col width="32%">
												<col width="25%">
												<col width="*">
												</colgroup>
												<tbody>
													<tr>
														<th scope="row">성명</th>
														<td>${rghtPrps.USER_NAME}</td>
														<th scope="row">주민등록번호/사업자번호</th>
														<td>${rghtPrps.RESD_CORP_NUMB_VIEW}</td>
													</tr>
													<tr>
														<th scope="row">전화번호</th>
														<td>
															<ul class="list1">
															<li class="p11"><label for="regi1" class="inBlock w25">자택</label> : <input type="text" id="regi1" size="14" maxlength="20" name="HOME_TELX_NUMB" title="자택 전화" value="${rghtPrps.HOME_TELX_NUMB}" /></li>
															<li class="p11"><label for="regi2" class="inBlock w25">사무실</label> : <input type="text" id="regi2" size="14" maxlength="20" name="BUSI_TELX_NUMB" title="사무실 전화" value="${rghtPrps.BUSI_TELX_NUMB}"/></li>
															<li class="p11"><label for="regi3" class="inBlock w25">휴대폰</label> : <input type="text" id="regi3" size="14" maxlength="20" name="MOBL_PHON" title="휴대폰 전화" value="${rghtPrps.MOBL_PHON}" /></li>
															</ul>
														</td>
														<th scope="row">팩스번호</th>
														<td><input type="text" name="FAXX_NUMB" title="팩스번호" id="regi4" maxlength="20" value="${rghtPrps.FAXX_NUMB}" size="20" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="regi5">이메일주소</label></th>
														<td colspan="3"><input type="text" name="MAIL" id="regi5" title="이메일주소" maxlength="20" value="${rghtPrps.MAIL}" class="w50" /></td>
													</tr>
													<tr>
														<th scope="row">주소</th>
														<td colspan="3">
															<ul class="list1">
															<li class="p11"><label for="regi6" class="inBlock w10">자택</label> : <input type="text" id="regi6" name="HOME_ADDR" title="자택 주소" maxlength="50" value="${rghtPrps.HOME_ADDR}" class="w85" /></li>
															<li class="p11"><label for="regi7" class="inBlock w10">사무실</label> : <input type="text" id="regi7" name="BUSI_ADDR" title="사무실 주소" maxlength="50" value="${rghtPrps.BUSI_ADDR}" class="w85" /></li>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
											<!-- //그리드스타일 -->
										</td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary">권리구분</label></th>
										<td>
											<ul class="line22">
											<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_213" value="213" class="p12" onclick="trust_check(this);" title="제작사,투자사,배급사 - 저작권자 (한국영화제작자협회)" <c:if test="${rghtPrps.CHK_213 == '1'}">checked="checked"</c:if> /><label for="rght1" class="p12">제작사,투자사,배급사 - 저작권자 (한국영화제작자협회)</label></li>
											<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_212" value="212" class="p12" onclick="trust_check(this);" title="시나리오 작가 - 저작권자 (한국시나리오작가협회)" <c:if test="${rghtPrps.CHK_212 == '1'}">checked="checked"</c:if>/><label for="rght2" class="p12">시나리오 작가 - 저작권자 (한국시나리오작가협회)</label></li>
											<li><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_211" value="211" class="p12" onclick="trust_check(this);" title="투자사,배급사 - 저작권자 (한국영상산업협회)"<c:if test="${rghtPrps.CHK_211 == '1'}">checked="checked"</c:if> /><label for="rght3" class="p12">투자사,배급사 - 저작권자 (한국영상산업협회)</label></li>
											</ul>
										</td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary">신청저작물정보</label></th>
										<td>
											<!-- 권리자 저작권찾기 시작 -->
											<div id="div_1" class="tabelRound" style="width:572px; padding:0 0 0 0;">
												<span class="blue2 p11">&lowast; 권리있는 항목만 입력하세요.</span>
												<div class="floatDiv mb5 mt10"><h3 class="fl mt5">권리자 저작권찾기<span id="totalRow"></span></h3>
													<p class="fr">
														<a href="#1" onclick="javascript:editTable('I');" id="workAdd"><img src="/images/2012/button/add.gif" alt="[신청저작물정보] 행을 추가합니다." /></a>
														<a href="#1" onclick="javascript:editTable('D');"><img src="/images/2012/button/delete.gif" alt="[신청저작물정보] 행을 삭제합니다." /></a>
													</p>
												</div>
												<!-- div_scroll_1 str -->
												<div id="div_scroll_1" style="width:569px;">
													<table id="listTab" cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="권리자 저작권찾기 신청저작물정보 입력폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
														<colgroup>
														<col width="4%">
														<col width="6%">
														<col width="*">
														<col width="17%">
														<col width="17%">
														<col width="17%">
														<col width="12%">
														</colgroup>
														<thead>
															<tr>
																<th rowspan="2" scope="col"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk',this);" title="전체선택" /></th>
																<th rowspan="2" scope="col">순<br>번</th>
																<th scope="col"><label class="necessary white">저작물명</label></th>
																<th scope="col"><label class="necessary white">감독/연출</label></th>
																<th scope="col"><label class="necessary white">제작일자</label></th>
																<th scope="col"><label class="necessary white">매체</label></th>
																<th rowspan="2" scope="col">작가</th>
															</tr>
															<tr>
																<th scope="col"><label class="necessary white">주요출연진</label></th>
																<th scope="col">제작사</th>
																<th scope="col">투자사</th>
																<th scope="col">배급사</th>
															</tr>
														</thead>
														<tbody>
														<c:if test="${!empty workList}">
															<c:forEach items="${workList}" var="workList">
																<c:set var="NO" value="${workList.totalRow}"/>
																<c:set var="i" value="${i+1}"/>
														<!-- 기존Meta저작물 -->
														<c:if test="${workList.PRPS_IDNT_CODE == '1'}">		
															<tr>
																<td rowspan="2" class="ce pd5">
																	<input type="checkbox" name="iChk" id="iChk_${i}" class="vmid" title="선택" />
																</td>
																<td rowspan="2" class="ce pd5">
																	<input name="displaySeq" id="displaySeq_${i}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}" title="순번"/>
																</td>
																<td class="lft pd5"><a class="underline black2" href="#1" onclick="javascript:openMvieDetail('${workList.PRPS_IDNT}');">${workList.TITLE }</a></td>
																<td class="ce pd5">${workList.DIRECTOR }</td>
																<td class="ce pd5">${workList.PRODUCE_DATE }</td>
																<td class="ce pd5">
																	${workList.MEDIA_CODE_VALUE }
																	<!-- hidden Value -->
																	<input type="hidden" name="iChkVal" value="${workList.PRPS_IDNT}|${workList.PRPS_IDNT_NR}|${workList.PRPS_IDNT_ME}"/>
																	<input type="hidden" name="TITLE" value="${workList.TITLE_TRNS }"/>
																	<input type="hidden" name="DIRECTOR" />
																	<input type="hidden" name="LEADING_ACTOR" />
																	<!--<input type="hidden" name="PRODUCE_DATE" />-->
																	<input type="hidden" name="PRODUCE_DATE_${i}" value="${workList.PRODUCE_DATE }" />
																	<input type="hidden" name="PRODUCE_DATE" value="${i}"/>
																	<input type="hidden" name="MEDIA_CODE_VALUE" />
																</td>
																<td rowspan="2" class="ce pd5">
																	<input name="WRITER" id="WRITER_${i}" class="inputDisible2 w95 ce" value="${workList.WRITER_TRNS }" title="작가" rangeSize="~100" readonly/>
																	<!-- hidden Value -->
																	<input type="hidden" name="PRODUCER_ORGN" value='${workList.PRODUCER_ORGN_TRNS }' />
																	<input type="hidden" name="INVESTOR_ORGN" value='${workList.INVESTOR_ORGN_TRNS }'/>
																	<input type="hidden" name="DISTRIBUTOR_ORGN" value='${workList.DISTRIBUTOR_ORGN_TRNS }'/>
																	<input type="hidden" name="WRITER_ORGN" value='${workList.WRITER_ORGN_TRNS }'/>
																</td>
															</tr>
															<tr>
																<td class="lft pd5">${workList.LEADING_ACTOR }</td>
																<td class="ce pd5"><input name="PRODUCER" id="PRODUCER_${i}" class="inputDisible2 w95 ce" value="${workList.PRODUCER_TRNS }" title="제작사" rangeSize="~100" readonly /></td>
																<td class="ce pd5"><input name="INVESTOR" id="INVESTOR_${i}" class="inputDisible2 w95 ce" value="${workList.INVESTOR_TRNS }" title="투자사" rangeSize="~100" readonly/></td>
																<td class="ce pd5"><input name="DISTRIBUTOR" id="DISTRIBUTOR_${i}" class="inputDisible2 w95 ce" value="${workList.DISTRIBUTOR_TRNS }" title="배급사" rangeSize="~100" readonly/></td>
															</tr>
														</c:if>
														<!-- 추가등록저작물 -->
														<c:if test="${workList.PRPS_IDNT_CODE == '2'}">		
															<tr>
																<td rowspan="2" class="ce pd5">
																	<input type="checkbox" name="iChk" id="iChk_${i}" class="vmid" title="선택" />
																</td>
																<td rowspan="2" class="ce pd5">
																	<input name="displaySeq" id="displaySeq_${i}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}" title="순번"/>
																</td>
																<td class="lft pd5"><input name="TITLE" id="TITLE_${i}" class="inputDataN w95" value='${workList.TITLE_TRNS}' title="저작물명" rangeSize="~100" nullCheck /></td>
																<td class="ce pd5"><input name="DIRECTOR" id="DIRECTOR_${i}" class="inputDataN w95" value='${workList.DIRECTOR_TRNS}' title="감독/연출" rangeSize="~100" nullCheck /></td>
																<td class="ce pd5">
																	<input name="PRODUCE_DATE_${i}" id="PRODUCE_DATE_${i}" class="inputDataN w60" title="제작일자" maxlength="8" Size="8" character="KE" dateCheck nullCheck value="${workList.PRODUCE_DATE}" />
																	<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('prpsForm','PRODUCE_DATE_${i}');" onkeypress="javascript:fn_cal('prpsForm','PRODUCE_DATE_${i}');" alt="달력" title="제작일자를 선택하세요." align="middle" style="cursor:pointer;" />
																	<!-- hidden Value -->
																	<input type="hidden" name="PRODUCE_DATE" value="${i}"/>
																</td>
																<td class="ce pd5">
																	<input name="MEDIA_CODE_VALUE" id="MEDIA_CODE_VALUE_${i}" class="inputDataN w90" value="${workList.MEDIA_CODE_VALUE }" title="매체" rangeSize="~20" nullCheck />
																	<!-- hidden Value -->
																	<input type="hidden" name="iChkVal" value=""/>
																</td>
																<td rowspan="2" class="ce pd5">
																	<input name="WRITER" id="WRITER_${i}" class="inputDisible2 w90 ce" value="${workList.WRITER_TRNS }" title="작가" rangeSize="~100" readonly/>
																	<!-- hidden Value -->
																	<input type="hidden" name="PRODUCER_ORGN" value='${workList.PRODUCER_ORGN_TRNS }' />
																	<input type="hidden" name="INVESTOR_ORGN" value='${workList.INVESTOR_ORGN_TRNS }'/>
																	<input type="hidden" name="DISTRIBUTOR_ORGN" value='${workList.DISTRIBUTOR_ORGN_TRNS }'/>
																	<input type="hidden" name="WRITER_ORGN" value='${workList.WRITER_ORGN_TRNS }'/>
																</td>
															</tr>
															<tr>
																<td class="lft pd5"><input name="LEADING_ACTOR" id="LEADING_ACTOR_${i}" class="inputDataN w95" value='${workList.LEADING_ACTOR_TRNS}' title="출연진"  rangeSize="~200" nullCheck /></td>
																<td class="ce pd5"><input name="PRODUCER" id="PRODUCER_${i}" class="inputDisible2 w95 ce" value="${workList.PRODUCER_TRNS }" title="제작사" rangeSize="~100" readonly /></td>
																<td class="ce pd5"><input name="INVESTOR" id="INVESTOR_${i}" class="inputDisible2 w95 ce" value="${workList.INVESTOR_TRNS }" title="투자사" rangeSize="~100" readonly/></td>
																<td class="ce pd5"><input name="DISTRIBUTOR" id="DISTRIBUTOR_${i}" class="inputDisible2 w95 ce" value="${workList.DISTRIBUTOR_TRNS }" title="배급사" rangeSize="~100" readonly/></td>
															</tr>
														</c:if>
															</c:forEach>
														</c:if>
														</tbody>
													</table>
												</div>
												<!-- div_scroll_1 end -->
											</div>
											<!-- //권리자 저작권찾기 시작 -->
											<!-- 이용자 저작권찾기 시작 -->
											<div id="div_2" class="tabelRound" style="width:572px; padding:0 0 0 0; display:none">
												<span class="blue2 p11">&lowast; 권리있는 항목만 입력하세요.</span>
												<div class="floatDiv mb5 mt10"><h3 class="fl mt5">이용자 저작권찾기<span id="totalRow2"></span></h3>
													<p class="fr">
														<a href="#1" onclick="javascript:editTable('I');" id="workAdd2"><img src="/images/2012/button/add.gif" alt="[신청저작물정보] 행을 추가합니다." /></a>
														<a href="#1" onclick="javascript:editTable('D');"><img src="/images/2012/button/delete.gif" alt="[신청저작물정보] 행을 삭제합니다." /></a>
													</p>
												</div>
												<!-- div_scroll_1 str -->
												<div id="div_scroll_2" style="width:569px;">
													<table id="listTab_2" cellspacing="0" cellpadding="0" border="1" class="grid" summary="이용자 저작권조회 신청저작물정보 입력폼 입니다.">
														<colgroup>
														<col width="4%">
														<col width="6%">
														<col width="*">
														<col width="12%">
														<col width="19%">
														<col width="15%">
														<col width="12%">
														</colgroup>
														<thead>
															<tr>
																<th scope="col">
																<input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk2',this);" title="전체선택" /></th>
																<th scope="col">순번</th>
																<th scope="col"><label class="necessary">저작물명</label></th>
																<th scope="col"><label class="necessary">감독</label></th>
																<th scope="col"><label class="necessary">출연진</label></th>
																<th scope="col"><label class="necessary">제작일자</label></th>
																<th scope="col"><label class="necessary">매체</label></th>
															</tr>
														</thead>
														<tbody>
															<c:if test="${!empty workList}">
															<c:forEach items="${workList}" var="workList">
																<c:set var="NO" value="${workList.totalRow}"/>
																<c:set var="ii" value="${ii+1}"/>
															<tr>
																<td class="ce pd5"><input type="checkbox" name="iChk2" id="iChk2_${ii}" class="vmid" title="선택" /></td>
																<td class="ce pd5">
																	<input name="displaySeq2" id="displaySeq2_${ii}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${ii}" title="순번"/>
																	<!-- HIDDEN VALUE -->
																	<input type="hidden" name="PRODUCER" />
																	<input type="hidden" name="INVESTOR" />
																	<input type="hidden" name="DISTRIBUTOR" />
																	<input type="hidden" name="WRITER" />
																</td>
																<!-- 기존Meta저작물 -->
																<c:if test="${workList.PRPS_IDNT_CODE == '1'}">
																<td class="pd5 lft"><a class="underline black2" href="#1" onclick="javascript:openMvieDetail('${workList.PRPS_IDNT}');">${workList.TITLE }</a></td>
																<td class="ce pd5">${workList.DIRECTOR }</td>
																<td class="ce pd5">${workList.LEADING_ACTOR }</td>
																<td class="ce pd5">${workList.PRODUCE_DATE }</td>
																<td class="ce pd5">${workList.MEDIA_CODE_VALUE }
																	<!-- hidden Value -->
																	<input type="hidden" name="iChkVal" value="${workList.PRPS_IDNT}|${workList.PRPS_IDNT_NR}|${workList.PRPS_IDNT_ME}"/>
																	<input type="hidden" name="TITLE" value="${workList.TITLE_TRNS }" />
																	<input type="hidden" name="DIRECTOR" />
																	<input type="hidden" name="LEADING_ACTOR" />
																	<!--<input type="hidden" name="PRODUCE_DATE" />-->
																	<input type="hidden" name="VAL_PRODUCE_DATE_${ii}" value="${workList.PRODUCE_DATE }" />
																	<input type="hidden" name="VAL_PRODUCE_DATE" value="${ii}"/>
																	<input type="hidden" name="MEDIA_CODE_VALUE" />
																</td>
																</c:if>
																<c:if test="${workList.PRPS_IDNT_CODE == '2'}">
																<td class="pd5 lft">
																	<input name="TITLE" id="TITLE_${ii}" class="inputDataN w95" value='${workList.TITLE_TRNS }' title="저작물명" rangeSize="~100" nullCheck />
																</td>
																<td class="ce pd5">
																	<input name="DIRECTOR" id="DIRECTOR_${ii}" class="inputDataN w90" value='${workList.DIRECTOR_TRNS }' title="감독/연출" rangeSize="~100" nullCheck />
																</td>
																<td class="ce pd5">
																	<input name="LEADING_ACTOR" id="LEADING_ACTOR_${ii}" class="inputDataN w95" value='${workList.LEADING_ACTOR_TRNS }' title="출연진"  rangeSize="~200" nullCheck />
																</td>
																<td class="ce pd5">
																	<input name="VAL_PRODUCE_DATE_${ii}" id="VAL_PRODUCE_DATE_${ii}" class="inputDataN w65" title="제작일자" maxlength="8" Size="8" character="KE" dateCheck nullCheck value="${workList.PRODUCE_DATE}" />
																	<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('prpsForm','VAL_PRODUCE_DATE_${ii}');" onkeypress="javascript:fn_cal('prpsForm','VAL_PRODUCE_DATE_${ii}');" alt="달력" title="제작일자를 선택하세요." align="middle" style="cursor:pointer;" />
																	<!-- hidden Value -->
																	<input type="hidden" name="VAL_PRODUCE_DATE" value="${ii}"/>
																</td>
																<td class="ce pd5">
																	<input name="MEDIA_CODE_VALUE" id="MEDIA_CODE_VALUE_${ii}" class="inputDataN w95" value="${workList.MEDIA_CODE_VALUE }" title="매체" rangeSize="~20" nullCheck />
																	<!-- hidden Value -->
																	<input type="hidden" name="iChkVal" value=""/>
																</td>
																</c:if>
															</tr>
															</c:forEach>
															</c:if>
														</tbody>
													</table>
												</div>
											</div>
											<!-- //이용자 저작권찾기 시작 -->
											<p class="HBar mt25 mb5">&nbsp;</p>
											<div class="floatDiv mb5 mt10">
											<h3 class="fl mt5">저작물검색</h3>
											<p class="fr" id="pBtnAdd"><a href="#1" onclick="javascript:editTable('A');"><img
												src="/images/2012/button/add_up.gif" alt="추가" /></a></p>
											</div>
											<!-- iframe 영역입니다 -->
											<iframe id="ifMvieRghtSrch" title="저작물조회(영화)" name="mvieRghtSrch" 
												width="569px" height="400" marginwidth="0" marginheight="0" frameborder="0" 
												scrolling="no" style="overflow-y:hidden;"
												src="/rghtPrps/rghtSrch.do?method=subListForm&amp;page_no=1&amp;DIVS=V&amp;dtlYn=Y">
											</iframe>
											<!-- //iframe 영역입니다 -->
										</td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary">신청내용</label></th>
										<td>
											<p class="p11 blue2">&lowast; 신청 목적이 이용자 저작권 조회인 경우 이용내용(이용하고자 하는 목적/방법)을 작성합니다.<br />&lowast; 신청 저작물 정보의 가사 및 저작물 변경 등을 작성합니다.</p>
											<textarea cols="10" rows="10" name="PRPS_DESC" id="PRPS_DESC" class="w99" title="신청내용" nullCheck>${rghtPrps.PRPS_DESC}</textarea>
										</td>
									</tr>
									<tr>
										<th scope="row"><span id="attFileTitle">첨부파일(증빙서류)</span><br />
											<input type="checkbox" title="오프라인접수" name="OFFX_LINE_RECP" value="Y" class="inputChk" onclick="javascript:offLine_check(this);"
											<c:if test="${rghtPrps.OFFX_LINE_RECP == 'Y'}">checked="checked"</c:if> />오프라인접수
										</th>
										<td id="td_file_no" style="display:none">오프라인접수로 선택한 경우 첨부파일을 등록할 수 없습니다.</td>
										<td id="td_file_yes">
										<!--  첨부파일 테이블 영역입니다 -->
										<div id="divFileList" class="tabelRound mb5" style="display;">
										</div>
										</td>
									</tr>
								</tbody>
							</table>
							<div class="white_box" id="attFileMemo1">
								<div class="box5">
									<div class="box5_con floatDiv">
										<p class="fl mt5"><img src="/images/2012/content/box_img4.gif" alt="" /></p>
										<div class="fl ml30 mt5">
											<h4>첨부파일(증빙서류) 안내</h4>
											<ul class="list1 mt10">
											<li>저작권자임을 증명할 수 있는 서류(앨범자켓, 저작권등록증 등)</li>
											<li>상속, 양도, 승계 등의 사실을 증명할 수 있는 서류</li>
											<li>주민등록등본/법인등기부등본</li>
											<li>사업자등록증(사본) 1부</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<!-- //그리드스타일 -->
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:goList();">목록</a></span></p>
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:rghtPrpsModi();">저작권찾기 수정</a></span></p>
							</div>
						</div>
						<!-- //article end -->
						</form>
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->		
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
<!-- //전체를 감싸는 DIVISION -->

<script type="text/javascript" src="/js/2011/file.js"></script>
<script type="text/javascript">
<!--

	<c:if test="${!empty workList}">
		<c:forEach items="${workList}" var="workList">
			iRowIdx++;
		</c:forEach>
	</c:if>
	<c:if test="${!empty workList}">
		<c:forEach items="${workList}" var="workList">
			iRowIdx2++;
		</c:forEach>
	</c:if>

	function fn_setFileInfo(){
		
		var listCnt = '${fn:length(fileList)}';

		<c:if test="${!empty fileList}">
			<c:forEach items="${fileList}" var="fileList">
				fn_addGetFile(	'${fileList.TRST_ORGN_CODE}',
								'${fileList.PRPS_MAST_KEY}',
								'${fileList.PRPS_SEQN}',
								'${fileList.ATTC_SEQN}',
								'${fileList.FILE_PATH}',
								'${fileList.REAL_FILE_NAME}',
								'${fileList.FILE_NAME}',
								'${fileList.TRST_ORGN_CODE}',
								'${fileList.FILE_SIZE}'
								);
			</c:forEach>
		</c:if>		
	}
//-->
</script>
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<form name="srchForm" action="#">
<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
<input type="hidden" name="srchPrpsDivs" value="${srchParam.srchPrpsDivs }"/>
<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
<input type="submit" style="display:none;">
</form>
</body>
</html>