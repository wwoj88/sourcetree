<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(보상금) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2010/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/JavaScript">
<!--

/*calendar호출*/
function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}

//숫자만 입력
function only_arabic(t){
	var key = (window.netscape) ? t.which :  event.keyCode;

	if (key < 45 || key > 57) {
		if(window.netscape){  // 파이어폭스
			t.preventDefault();
		}else{
			event.returnValue = false;
		}
	} else {
		//alert('숫자만 입력 가능합니다.');
		if(window.netscape){   // 파이어폭스
			return true;
		}else{
			event.returnValue = true;
		}
	}	
}

// 날짜체크 
function checkValDate(){
	var f = document.frm;
	if(f.srchStartDate.value!='' && f.srchEndDate.value!=''){
		if(parseInt(f.srchStartDate.value,10)>parseInt(f.srchEndDate.value,10)){
			alert('만료일이 시작일보다 이전입니다.\n일자를 다시 확인하십시요');
			f.srchEndDate.value='';
		}
	}
}

// 페이지 이동
function goPage(pageNo){
	var frm = document.frm;

	frm.method = "post";
	frm.action = "/rsltInqr/rsltInqr.do?DIVS=20";
	frm.page_no.value = pageNo;
	frm.submit();
}

// 조회 
function fn_srchList(){
	var frm = document.frm;

	frm.method = "post";
	frm.action = "/rsltInqr/rsltInqr.do?DIVS=20&page_no=1";
	frm.submit();
}

// 상세화면이동
function goDetail(mastKey, prpsSeqn, divs){

	var frm = document.frm;
	
	frm.PRPS_MAST_KEY.value = mastKey;
	frm.PRPS_SEQN.value = prpsSeqn;
	frm.page_no.value = '${nowPage}';
	
	frm.method = "post";
	frm.action = "/rsltInqr/rsltInqr.do?method=inmtPrpsDetl&DIVS="+divs;
	frm.submit();
}

// 삭제
function fn_delete(){

	var frm = document.frm;
	var count = 0;
	
	var chkObjs = document.getElementsByName("chk");
	var totalStatObjs = document.getElementsByName("totalStat");
	var maxDealStatObjs = document.getElementsByName("maxDealStat");
	var actFlagYnObjs = document.getElementsByName("actFlagYn");
	var prpsMastKeyObjs = document.getElementsByName("prpsMastKey");
	var url;
	
	for(var i=0; i<chkObjs.length; i++){
		if(chkObjs[i].checked){
			if(totalStatObjs[i].value == '신청' && Number(maxDealStatObjs[i].value) > 1){
				alert("상세처리상태가 '신청'이상 진행된경우는 삭제가 불가능합니다.\n다시 선택 해 주세요.");
				chkObjs[i].checked = false;
				return;
			}else if(totalStatObjs[i].value != '신청'){
				alert("처리상태가 '신청'인 경우만 삭제가 가능합니다.\n다시 선택 해 주세요.");
				chkObjs[i].checked = false;
				return;
			}else{
				url = "/rsltInqr/rsltInqr.do?method=isValidDealStat&actFlagYn=Y&prpsMastKey=" + prpsMastKeyObjs[i].value;
				if(cfGetBooleanResponseReload(url) == false){
					alert("상세처리상태가 '신청'이상 진행된경우는 삭제가 불가능합니다.\n다시 선택 해 주세요.");
					chkObjs[i].checked = false;
					return;
				}
				actFlagYnObjs[i].value = 'Y';
				//frm.actFlagYn[i].value = 'Y';
			}
			count++;
		}
	}
	
	if(count == 0){
		alert('삭제대상을 선택 해 주세요.');
		return;
	}else{
		if(confirm('삭제 하시겠습니까?')){ 
			frm.method = "post";
			frm.action = "/rsltInqr/rsltInqr.do?method=deleteRsltInqr&DIVS=20&page_no=1";
			frm.submit();
		}
	}
	
}

/*
*  Function.js에 있으나 파이어폭스에서는 인식이 안되서 가져왔음
*/
function cfGetBooleanResponseReload(url,params,HttpMethod) {
	var xmlhttp = null;
	if(!HttpMethod){
	    HttpMethod = "GET";
	}
    if(window.ActiveXObject){
	      try {
	    	  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	      } catch (e) {
	        try {
	        	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP") ;
	        } catch (e2) {
	          return null ;
	        }
		}
	}
	//xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	if (xmlhttp == null) return true;
	xmlhttp.open(HttpMethod, url, false);
	xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');

	xmlhttp.send(params);
	
	if (xmlhttp.responseText == "true") {
		return true;
	} else {
		return false;
	}
}	  

//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container"  style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
			<div class="container_vis"  style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_6.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<jsp:include page="/include/2012/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1","lnb12");
				</script>
				<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>신청현황</span><em>미분배보상금 신청</em></p>
					<h1><img src="/images/2012/title/content_h1_0602.gif" alt="보상금" title="보상금" /></h1>
					
					<div class="section">
						<form name="frm" action="#" class="sch mt20">
						<input type="hidden" name="page_no"/>
						<input type ="hidden" name="PRPS_MAST_KEY" />
						<input type ="hidden" name="PRPS_SEQN" />
						<input type="submit" style="display:none;">
							<fieldset class="w100">
							<legend>게시판검색</legend>
								<div class="boxStyle">
									<div class="box1 floatDiv">
										<p class="fl mt5 w15"><img title="Search" alt="Search" src="/images/2012/content/sch_txt.gif"></p>
										<table summary="보상금 신청현황조회 검색항목 입력 폼입니다." class="fl schBoxGrid w70">
											<caption>보상금신청현황조회 검색항목 입력</caption>
											<colgroup><col width="15%"><col width="30%"><col width="15%"><col width="*"></colgroup>
											<tbody>
												<tr>
													<th scope="row"><label for="sch1">구분</label></th>
													<td>
														<select id="sch1" name="srchPrpsDivs">
															<option value="">전체 ----------</option>
															<option value="B" <c:if test="${srchPrpsDivs == 'B'}">selected="selected"</c:if>>방송음악</option>
															<option value="S" <c:if test="${srchPrpsDivs == 'S'}">selected="selected"</c:if>>교과용</option>
															<option value="L" <c:if test="${srchPrpsDivs == 'L'}">selected="selected"</c:if>>도서관</option>
														</select>
													</td>
													<th scope="row"><label for="sch2">신청일자</label></th>
													<td>
														<input class="inputData" type="text" id="sch2" title="신청일자(시작)" name="srchStartDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_srchList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchStartDate}"/> 
														<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/> 
															~ 
														<input class="inputData" title="신청일자(종료)" type="text" id="sch3" name="srchEndDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_srchList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchEndDate }"/> 
														<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" alt="달력" title="마지막 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>
													</td>
												</tr>
												<tr>
													<th scope="row"><label for="sch4">검색어</label></th>
													<td colspan="3"><input name="srchTitle" class="inputData w50" id="sch4" value="${srchTitle}"/></td>
												</tr>
											</tbody>
										</table>
										<p class="fl btn_area pt25"><input type="image" src="/images/2012/button/sch.gif" onclick = "javascript:fn_srchList();" alt="검색" title="검색">
									</p></div>
									<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
								</div>
							</fieldset>
						
						<!-- 테이블 리스트 Set -->
						<div class="section mt20">
							<div class="result_area floatDiv">
								<p class="tab fl"><span class="tab2"><strong class="orange">${rsltList.totalRow}</strong>건 검색</span></p>
								<p class="fr"><a href="#1" onclick="javascript:fn_delete();"><img src="/images/2012/button/delete.gif" alt="삭제" /></a></p>
							</div>
							
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="보상금 신청내역 구분, 제목, 신청일자, 처리상태 정보입니다." class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="6%">
								<col width="15%">
								<col width="*">
								<col width="15%">
								<col width="15%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col"><input type="checkbox" id="allChk" name="allChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" style="cursor:pointer;" title="전체선택"/></th>
										<th scope="col">구분</th>
										<th scope="col">제목</th>
										<th scope="col">신청일자</th>
										<th scope="col">처리상태</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${rsltList.totalRow == 0}">
										<tr>
											<td class="ce" colspan="5">검색된 신청내역이 없습니다.</td>
										</tr>
									</c:if>
									<c:if test="${rsltList.totalRow > 0}">
										<c:forEach items="${rsltList.resultList}" var="rsltList">
											<c:set var="NO" value="${rsltList.totalRow}"/>
											<c:set var="i" value="${i+1}"/>
										<tr>
											<td class="ce">
												<input type="checkbox" name="chk" class="vmid" style="cursor:pointer;" title="선택" />
												<input type="hidden" name="actFlagYn" value="N" />
												<input type ="hidden" name="prpsMastKey" value="${rsltList.PRPS_MAST_KEY}" />
												<input type="hidden" name="totalStat" value="${rsltList.TOTAL_STAT}" />
												<input type="hidden" name="maxDealStat" value="${rsltList.MAX_DEAL_STAT}" />
											</td>
											<td class="ce">${rsltList.PRPS_DIVS_NAME}</td>
											<td><a href="#1" onclick="javascript:goDetail('${rsltList.PRPS_MAST_KEY}', '${rsltList.PRPS_SEQN}', '${rsltList.PRPS_DIVS}');">${rsltList.PRPS_TITE}</a></td>
											<td class="ce">${rsltList.RGST_DTTM}</td>
											<td class="ce">${rsltList.TOTAL_STAT}</td>
										</tr>	
										</c:forEach>
									</c:if>		
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<!-- 페이징 -->
							<div class="pagination">
							<ul>
								<li>
						 		<%-- 페이징 리스트 --%>
								  <jsp:include page="../common/PageList_2011.jsp" flush="true">
									  <jsp:param name="totalItemCount" value="${rsltList.totalRow}" />
										<jsp:param name="nowPage"        value="${param.page_no}" />
										<jsp:param name="functionName"   value="goPage" />
										<jsp:param name="listScale"      value="" />
										<jsp:param name="pageScale"      value="" />
										<jsp:param name="flag"           value="M01_FRONT" />
										<jsp:param name="extend"         value="no" />
									</jsp:include>
								</li>	
							</ul>
							</div>
							<!-- //페이징 -->
						</div>
						<!-- //테이블 리스트 Set -->
					</form>	
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->		
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>
