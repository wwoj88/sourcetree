<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html; charset=euc-kr" language="java"
	errorPage=""%>
<%@ page import="java.util.*"%>
<%@page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="java.util.List"%>
<!--[if lte IE 8]> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<![endif]-->
<c:set var="ctx" value="${pageContext.request.contextPath}"
	scope="request" />
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<title>저작권찾기</title>
<link href="/css2/clms.css" rel="stylesheet" type="text/css"/>
<style>
#ajaxBox{
   border:3px solid #000000;
   width:300px;
   height:50px;
   position:absolute;
   background-color:#ffffff;
   text-align:center;
   font-weight:bold;
   padding:20px 0px 0px 0px;
   color:#FF6600;
   
}
</style>

<script type="text/javascript" src="/js2/prototype.js"> </script>
<script type="text/javascript">
	function RTrim(str) {
		var whitespace = new String(" \t\n\r");
	    var s = new String(str);
	    if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
	    	var i = s.length - 1;
	        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
				i--;
			s = s.substring(0, i+1);
		}
	    return s;
	}
	
	function LTrim(str) {
		var whitespace = new String(" \t\n\r");
	    var s = new String(str);
	    if (whitespace.indexOf(s.charAt(0)) != -1) {
	    	var j=0, i = s.length;
	        while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
	           	j++;
	        s = s.substring(j, i);
	    }
	    return s;
	}

	function Trim(str) {
		return RTrim(LTrim(str));
	}

	function chkLimit(strTxt, strLimit, strName){
		var tmp = Trim(strTxt);
		
		if(strTxt.indexOf(strLimit) >-1 ){
			alert(strName + ' '+strLimit +'은(는) 사용할 수 없습니다.');
			return true;
		}
		return false;
	}

	// 통합검색
	function goSearch(){
		var frm = document.frm;
		
		if(document.getElementById("srchWord").value == ""){
			alert("검색어를 입력하세요.");
			document.getElementById("srchWord").focus();
			return;
		}
		
		if(chkLimit(document.getElementById("srchWord").value, "★", "검색어에 ") ){
			return;
		}
		
		if(document.getElementById("result_search_chk").checked==true){
			document.getElementById("resultWord").value = document.getElementById("resultWord").value +"★"+document.getElementById("srchWord").value;
		} else {
			document.getElementById("resultWord").value = document.getElementById("srchWord").value;
		}
		
		document.getElementById("srchWord").blur();
		
		//showAjaxBox();		// 로딩 이미지 박스 보이게..
		
		new Ajax.Request('/test', {   
			onLoading: function() {     
				showAjaxBox();
			},
			onSuccess: function(req) {     
				// Do something with req.responseXML/Text .. ...   
			},
			onComplete: function() {     
				showAjaxBox();
			} 
		});
		
		
		oSelect = document.getElementById("cate").selectedIndex;
		oSelectValue = document.getElementById("cate").options[oSelect].value;
		frm.currentPage.value = 1;
		
		if( oSelect >=1){
			// cate 값이 1 이상일 경우 gubun=cate, more=Y로 검색
			frm.more.value="Y";
			frm.gubun.value=oSelectValue;
		} else {
			frm.more.value="N";
			frm.gubun.value="0";
		}
		frm.action = "/intg/intg.do";
		frm.submit();
		
	}
	// 더보기
	function fn_searchMore(gubun){
		var frm = document.frm;
		frm.currentPage.value = 1;
		frm.more.value="Y";
		frm.gubun.value=gubun;
		frm.cate.value = gubun;
		frm.srchWord.value='${srchWord}';
		frm.action = "/intg/intg.do";
		frm.submit();
	}
	
	// 페이징검색
	function fn_searchList(currentPage){
		var frm = document.frm;
		frm.currentPage.value = parseInt(currentPage);
		frm.more.value="Y";
		frm.srchWord.value='${srchWord}';
		frm.action = "/intg/intg.do";
		frm.submit();
	}
	
	function fn_enterChk(){
		if (event.keyCode == 13) {
			goSearch();
		}
	}
	

	// 이미지
	function fn_popupImg(imgeSeq){
		popWin = window.open("/intg/intg.do?method=goImgDetail&imgeSeq="+imgeSeq,"","menubar=no,toolbar=no,width=670,height=280,location=no,scrollbars=yes");	
	}
	
	// 방송음악
	function fn_popupBrct(inmtSeqn){
		popWin = window.open("/intg/intg.do?method=goBrctDetail&inmtSeqn="+inmtSeqn,"","menubar=no,toolbar=no,width=670,height=310,location=no,scrollbars=yes");	
	}
	
	// 교과용
	function fn_popupSubj(inmtSeqn){
		popWin = window.open("/intg/intg.do?method=goSubjDetail&inmtSeqn="+inmtSeqn,"","menubar=no,toolbar=no,width=670,height=350,location=no,scrollbars=yes");	
	}
	
	// 도서관
	function fn_popupLibr(inmtSeqn){
		popWin = window.open("/intg/intg.do?method=goLibrDetail&inmtSeqn="+inmtSeqn,"","menubar=no,toolbar=no,width=670,height=290,location=no,scrollbars=yes");	
	}
	
	function showAjaxBox(ment){
   
	//항상 화면 중앙에 나타나도록...
	   var yp=document.body.scrollTop;
	   var xp=document.body.scrollLeft;
	
	   var ws=document.body.clientWidth;
	   var hs=document.body.clientHeight;
	   
	   if(!ment) ment="잠시만 기다려주세요..";
	   
	   var ajaxBox=$('ajaxBox');
	   $('ajaxBoxMent').innerHTML=ment;
	
	   ajaxBox.style.top=yp+eval(hs)/2-100;
	   ajaxBox.style.left=xp+eval(ws)/2-100;
	
	   Element.show(ajaxBox);    
	}
	
	// 프레임 싸이즈 set
	function iframeReSize(ifId) {
		var obj = document.getElementById(ifId);
		
		try {
			if(obj) {
				obj.height = obj.contentWindow.document.body.scrollHeight
			}
			
		}
		
		catch(E){}
	}
	
	function initParameter(){
		 iframeReSize('ifrMemo')
	}
	window.attachEvent("onload",initParameter);	
</script>
</head>

<body style="overflow-X:hidden;background:none;">
<form name="frm" method="post"><input type="hidden" name="more"
	value="${more}" /> <input type="hidden" name="gubun" value="${gubun}" />
<input type="hidden" name="currentPage" value="${currentPage}" /> <input
	type="hidden" name="resultWord" value="${resultWord}" /> 
<input type="submit" style="display:none;">	
<div id="ajaxBox"><div id="ajaxBoxMent">잠시만 기다려주세요..</div><img src="/images2/common/ajax-loader.gif" alt=""></div>
<!-- start:wrap -->
 
<div id="wrap" class="subBg">
<!-- start:header --> <!-- jsp:include page="./include/top.jsp"/ -->
<!-- end:header --> <!-- start:contentsBody -->
<div id="contentsBody"> <!-- start:contents -->
<div id="contents">

<h2>통합검색</h2>
<!-- start:searchBox -->
<div id="searchBox"><span class="sch_item" title="검색분류"> <select
	name="cate" id="cate">
	${selectBox}
</select> <input type="text" name="srchWord" title="검색어" id="srchWord" size="30"
	onKeyDown="javaScript:fn_enterChk();" /><img
	src="/images2/button/srch2_btn.gif" alt="검색" width="53" height="21"
	class="btn" onClick="javascript:goSearch();" style="cursor:hand" /><input
	name="result_search_chk" type="checkbox" value="" class="chk" /><label
	for="">결과 내 검색</label> </span> <!-- srchWord_0 --></div>
<!-- end:searchBox --> <!-- 검색결과 시작 --> 

<c:if	test="${not empty Intg.title}">
	<div id="contentsDiv" class="border">
	
	<!--  7. 이미지  --> 
	<c:if test="${gubun=='0' || gubun=='7'}">
		<c:if test="${not empty totalImgWorks}">
			<ul id="srchRlst">
				<li>
				<div class="category"><span class="tlt floatL">이미지</span><span class="total floatL">총 검색(${totalRowImgWorks}건)</span><c:if test="${totalRowImgWorks > 3 && gubun=='0'}"><span><a href="#1" onclick="javascript:fn_searchMore('7');" class="more">이미지 더보기</a></span></c:if></div>

				<c:forEach items="${totalImgWorks}" var="totalImgWorks">
					<dl>
						<dt><span class="ttttt"><a href="#1" onclick="javascript:fn_popupImg('${totalImgWorks.imgeSeq}');">${totalImgWorks.workName}&nbsp;</a></span></dt>
						<dd></dd>
						<dd class="ps">
							<ul>
								<li>저작권자 : <span>${totalImgWorks.coptHodr}&nbsp;</span></li>
							</ul>
						</dd>
						<dd class="ps">
							<ul>
								<li>출판사 : <span>${totalImgWorks.lishComp}</span></li>
								<li>이용년도 : <span>${totalImgWorks.usexYear}</span></li>
							</ul>
						</dd>
					</dl>
				</c:forEach></li>
			</ul>

			<!--  더보기일경우 페이징 -->
			<c:if test="${gubun=='7'}">
				<!-- start:paging -->
				<div id="paging">
					<!-- 페이징테이블 시작 -->
					<ul>
					  <jsp:include page="../common/PageList.jsp" flush="true">
						  <jsp:param name="totalItemCount" value="${totalRow}" />
							<jsp:param name="nowPage"        value="${currentPage}" />
							<jsp:param name="functionName"   value="fn_searchList" />
							<jsp:param name="listScale"      value="" />
							<jsp:param name="pageScale"      value="" />
							<jsp:param name="flag"           value="M01_FRONT" />
							<jsp:param name="extend"         value="no" />
						</jsp:include>
					</ul>
					<!--// 페이징테이블 끝 -->
				</div>
				<!-- end:paging -->	
			</c:if>

		</c:if>

		<c:if test="${empty totalImgWorks}">
			<ul id="srchRlst">
				<li>
				<div class="category"><span class="tlt floatL">이미지</span></div>
				<!-- start:"" -->
				<dl>
					<dt></dt>
					<dd class="collectionName"><span>검색된 결과가 없습니다.</span></dd>
				</dl>
				<!-- end:"" --></li>
			</ul>
		</c:if>
	</c:if>
	
	<!--  8. 방송음악보상금  --> 
	<c:if test="${gubun=='0' || gubun=='8'}">
		<c:if test="${not empty totalBrctWorks}">
			<ul id="srchRlst">
				<li>
				<div class="category"><span class="tlt floatL">방송음악보상금</span><span class="total floatL">총 검색(${totalRowBrctWorks}건)</span><c:if test="${totalRowBrctWorks > 3 && gubun=='0'}"><span><a href="#1" onclick="javascript:fn_searchMore('8')" class="more">방송음악보상금 더보기</a></span></c:if></div>

				<c:forEach items="${totalBrctWorks}" var="totalBrctWorks">
					<dl>
						<dt><span class="ttttt"><a href="#1" onclick="javascript:fn_popupBrct('${totalBrctWorks.inmtSeqn}');">${totalBrctWorks.sdsrName}&nbsp;</a></span></dt>
						<dd></dd>
						<dd class="ps">
							<ul>
								<li>가수명 : <span>${totalBrctWorks.muciName}&nbsp;</span></li>
								<li>방송사용년도 : <span>${totalBrctWorks.yymm}&nbsp;</span></li>
							</ul>
						</dd>
					</dl>
				</c:forEach></li>
			</ul>

			<!--  더보기일경우 페이징 -->
			<c:if test="${gubun=='8'}">
				<!-- start:paging -->
				<div id="paging">
					<!-- 페이징테이블 시작 -->
					<ul>
					  <jsp:include page="../common/PageList.jsp" flush="true">
						  <jsp:param name="totalItemCount" value="${totalRow}" />
							<jsp:param name="nowPage"        value="${currentPage}" />
							<jsp:param name="functionName"   value="fn_searchList" />
							<jsp:param name="listScale"      value="" />
							<jsp:param name="pageScale"      value="" />
							<jsp:param name="flag"           value="M01_FRONT" />
							<jsp:param name="extend"         value="no" />
						</jsp:include>
					</ul>
					<!--// 페이징테이블 끝 -->
				</div>
				<!-- end:paging -->	
			</c:if>

		</c:if>

		<c:if test="${empty totalBrctWorks}">
			<ul id="srchRlst">
				<li>
				<div class="category"><span class="tlt floatL">방송음악보상금</span></div>
				<!-- start:"" -->
				<dl>
					<dt></dt>
					<dd class="collectionName"><span>검색된 결과가 없습니다.</span></dd>
				</dl>
				<!-- end:"" --></li>
			</ul>
		</c:if>
	</c:if>
	
	<!--  9. 교과용보상금  --> 
	<c:if test="${gubun=='0' || gubun=='9'}">
		<c:if test="${not empty totalSubjWorks}">
			<ul id="srchRlst">
				<li>
				<div class="category"><span class="tlt floatL">교과용보상금</span><span class="total floatL">총 검색(${totalRowSubjWorks}건)</span><c:if test="${totalRowSubjWorks > 3 && gubun=='0'}"><span><a href="#1" onclick="javascript:fn_searchMore('9')" class="more">교과용보상금 더보기</a></span></c:if></div>

				<c:forEach items="${totalSubjWorks}" var="totalSubjWorks">
					<dl>
						<dt><span class="ttttt"><a href="#1" onclick="javascript:fn_popupSubj('${totalSubjWorks.inmtSeqn}');">${totalSubjWorks.workName}&nbsp;</a></span></dt>
						<dd></dd>
						<dd class="ps">
							<ul>
								<li>저자명 : <span>${totalSubjWorks.coptHodr}&nbsp;</span></li>
								<li>저작물종류 : <span>${totalSubjWorks.workKind}&nbsp;</span></li>
							</ul>
						</dd>
						<dd class="ps">
							<ul>
								<li>출판사 : <span>${totalSubjWorks.lishComp}&nbsp;</span></li>
								<li>출판년도 : <span>${totalSubjWorks.yymm}&nbsp;</span></li>
							</ul>
						</dd>
					</dl>
				</c:forEach></li>
			</ul>

			<!--  더보기일경우 페이징 -->
			<c:if test="${gubun=='9'}">
				<!-- start:paging -->
				<div id="paging">
					<!-- 페이징테이블 시작 -->
					<ul>
					  <jsp:include page="../common/PageList.jsp" flush="true">
						  <jsp:param name="totalItemCount" value="${totalRow}" />
							<jsp:param name="nowPage"        value="${currentPage}" />
							<jsp:param name="functionName"   value="fn_searchList" />
							<jsp:param name="listScale"      value="" />
							<jsp:param name="pageScale"      value="" />
							<jsp:param name="flag"           value="M01_FRONT" />
							<jsp:param name="extend"         value="no" />
						</jsp:include>
					</ul>
					<!--// 페이징테이블 끝 -->
				</div>
				<!-- end:paging -->	
			</c:if>

		</c:if>

		<c:if test="${empty totalSubjWorks}">
			<ul id="srchRlst">
				<li>
				<div class="category"><span class="tlt floatL">교과용보상금</span></div>
				<!-- start:"" -->
				<dl>
					<dt></dt>
					<dd class="collectionName"><span>검색된 결과가 없습니다.</span></dd>
				</dl>
				<!-- end:"" --></li>
			</ul>
		</c:if>
	</c:if>
	
	<!--  10. 도서관보상금  --> 
	<c:if test="${gubun=='0' || gubun=='10'}">
		<c:if test="${not empty totalLibrWorks}">
			<ul id="srchRlst">
				<li>
				<div class="category"><span class="tlt floatL">도서관보상금</span><span class="total floatL">총 검색(${totalRowLibrWorks}건)</span><c:if test="${totalRowLibrWorks > 3 && gubun=='0'}"><span><a href="#1" class="more" onclick="javascript:fn_searchMore('10')">도서관보상금 더보기</a></span></c:if></div>

				<c:forEach items="${totalLibrWorks}" var="totalLibrWorks">
					<dl>
						<dt><span class="ttttt"><a href="#1" onclick="javascript:fn_popupLibr('${totalLibrWorks.inmtSeqn}');">${totalLibrWorks.workName}&nbsp;</a></span></dt>
						<dd></dd>
						<dd class="ps">
							<ul>
								<li>저자명 : <span>${totalLibrWorks.coptHodr}&nbsp;</span></li>
							</ul>
						</dd>
						<dd class="ps">
							<ul>
								<li>출판사 : <span>${totalLibrWorks.lishComp}&nbsp;</span></li>
								<li>출판년도 : <span>${totalLibrWorks.yymm}&nbsp;</span></li>
							</ul>
						</dd>
					</dl>
				</c:forEach></li>
			</ul>

			<!--  더보기일경우 페이징 -->
			<c:if test="${gubun=='10'}">
				<!-- start:paging -->
				<div id="paging">
					<!-- 페이징테이블 시작 -->
					<ul>
					  <jsp:include page="../common/PageList.jsp" flush="true">
						  <jsp:param name="totalItemCount" value="${totalRow}" />
							<jsp:param name="nowPage"        value="${currentPage}" />
							<jsp:param name="functionName"   value="fn_searchList" />
							<jsp:param name="listScale"      value="" />
							<jsp:param name="pageScale"      value="" />
							<jsp:param name="flag"           value="M01_FRONT" />
							<jsp:param name="extend"         value="no" />
						</jsp:include>
					</ul>
					<!--// 페이징테이블 끝 -->
				</div>
				<!-- end:paging -->	
			</c:if>

		</c:if>

		<c:if test="${empty totalLibrWorks}">
			<ul id="srchRlst">
				<li>
				<div class="category"><span class="tlt floatL">도서관보상금</span></div>
				<!-- start:"" -->
				<dl>
					<dt></dt>
					<dd class="collectionName"><span>검색된 결과가 없습니다.</span></dd>
				</dl>
				<!-- end:"" --></li>
			</ul>
		</c:if>
	</c:if>
	
	</div>
</c:if> <c:if test="${empty Intg.title}">
	<div id="contentsDiv" class="border">
	<ul id="srchRlst">
		<li><!-- start:"" -->
		<dl>
			<dd class="ps"><span>검색된 결과가 없습니다.</span></dd>
		</dl>
		</li>
	</ul>
	</div>
</c:if></div>
<!-- end:contents --></div>
<!-- end:contentsBody --> <!-- start:footer --> <!-- jsp:include page="./include/bottom.jsp"/ -->
<!-- end:footer --></div>
<!-- end:wrap --></form>
</body>
</html>
<script>
Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
