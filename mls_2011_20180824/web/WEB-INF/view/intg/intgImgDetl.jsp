<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html; charset=euc-kr" language="java" errorPage="" %>
<!--[if lte IE 8]> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<![endif]-->
<%@page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="pragma" content="no-cache"/>
<title>이미지정보조회 상세페이지</title>
<link href="/css2/clms.css" rel="stylesheet" type="text/css"/>
<link href="/css2/popup.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<form name="form" method="post">
<div id="wrap">

	<div class="popupContents">		
		<h3>이미지 정보</h3>
		<table style="width:620px" border="0" cellspacing="0" cellpadding="0" class="grid">
			<tr>
				<th class="tdLabel" style="width:120px">이미지명</th>
				<td class="tdData" colspan="3">${ImgDetail[0].workName}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">저작권자</th>
				<td class="tdData" colspan="3">${ImgDetail[0].coptHodr}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">출판사</th>
				<td class="tdData" colspan="3">${ImgDetail[0].lishComp}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">집필진</th>
				<td class="tdData" colspan="3">${ImgDetail[0].wterDivs}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">도서구분</th>
				<td class="tdData" style="width:202px">${ImgDetail[0].subjDivs}&nbsp;</td>
				<th class="tdLabel" style="width:120px">분류</th>
				<td class="tdData">${ImgDetail[0].divs}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">학급</th>
				<td class="tdData" style="width:202px">${ImgDetail[0].schlClss}&nbsp;</td>
				<th class="tdLabel" style="width:120px">과목명</th>
				<td class="tdData">${ImgDetail[0].itemName}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">저작물이용구분</th>
				<td class="tdData" style="width:202px">${ImgDetail[0].usexDivs}&nbsp;</td>
				<th class="tdLabel" style="width:120px">이용페이지</th>
				<td class="tdData">${ImgDetail[0].usexPage}&nbsp;</td>
			</tr>
		</table>

	</div>
	<!-- end:paging -->		
	<div class="popupBottom"><img src="/images2/button/close_btn.gif" alt="닫기" width="76" height="23" onclick="javascript:window.close();" style="cursor:hand;"/></div>
</div>
<input type="submit" style="display:none;">
</form>
</body>
</html>

<link rel="stylesheet" type="text/css" href="/style/calendar.css">
<script type="text/javascript" src="/js/calendarcode.js"></script>
