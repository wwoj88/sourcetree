<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%
	String bordSeqn = request.getParameter("bordSeqn");
	String menuSeqn = request.getParameter("menuSeqn");
	String threaded = request.getParameter("threaded");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");

	Board boardDTO = (Board) request.getAttribute("board");
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작권찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
<script src="/js/Function.js" type="text/javascript"></script>
<script type="text/JavaScript">
<!--
  function fn_commList(){
		var frm = document.form2;
		frm.action = "/board/board.do";
		frm.submit();
	}

  function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }

  function fn_update(){

	  var frm =document.form1;

		if (frm.tite.value == "") {
		  alert("제목을 입력하십시오.");
		  frm.tite.focus();
		  return;
	  } else if (frm.mail.value == "") {
		  alert("이메일을 입력하십시오.");
		  frm.mail.focus();
		  return;
    } else if (frm.bordDesc.value == "") {
		  alert("내용을 입력하십시오.");
		  frm.bordDesc.focus();
		  return;
  	} else {
  		if(confirm("내용을 수정 하시겠습니까?")){
	  	  frm.action = "/board/board.do?method=updateComm";
  	    frm.submit();
  	  }
	  }
	}

	function showAttach(cnt) {
		var i=0;
		var content = "";
		document.all("attach").innerHTML = "";
		content += '					<table>';
		for (i=0; i<cnt; i++) {
			content += '						<tr>';
			content += '							<td><input type="file" name="attachfile'+(i+1)+'" size="80;" class="input"></td> ';
			content += '						</tr>';
		}
		content += '					</table>';
		document.all("attach").innerHTML = content;
	}

	function applyAtch() {
    var ansCnt = document.form1.atchCnt.value;
    showAttach(parseInt(ansCnt));
	}
//-->
</script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp" flush="true">
		<jsp:param name="mNum" value="5" />
	</jsp:include>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','122');
			</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="qustTlt">커뮤니티 목록</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<li class="none"><img src="/images/common/home_ico.gif" alt="Home" />Home</li>
					<li>커뮤니티</li>
					<li class="on">커뮤니티 게시판</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>커뮤니티 게시판 수정</h2>
			<div class="tip R">(<img src="/images/common/ic_necessary.gif" width="10" height="7" alt=""/> ) 항목은 필수입력사항이므로 빠짐없이 기입하여 주시기 바랍니다.</div>
			<table width="710" class="board ViewTop">
				<form name="form1" method="post" enctype="multipart/form-data">
					<input type="submit" style="display:none;">
					<input type="hidden" name="bordSeqn" value="<%=bordSeqn%>">
					<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>">
					<input type="hidden" name="threaded" value="<%=threaded%>">
					<input type="hidden" name="srchDivs" value="<%=srchDivs%>">
					<input type="hidden" name="srchText" value="<%=srchText%>">
					<input type="hidden" name="page_no" value="<%=page_no%>">
					<input type="hidden" name="rgstIdnt" value="<%=boardDTO.getRgstIdnt()%>">
					<input type="hidden" name="filePath">
					<input type="hidden" name="fileName">
					<input type="hidden" name="realFileName">
				<colgroup>
				<col width="80" />
				<col width="" />
				</colgroup>
				<caption summary="질문을 수정합니다">질문수정</caption>
				<thead >
					<tr>
						<th>제목</th>
						<td><input type="text" class="input" name="tite" id="tite" maxlength="250" value="<%=boardDTO.getTite()%>" title="제목" size="94" /></td>
					</tr>
				</thead>
				<tbody class="ViewBody">
					<tr>
						<th>이름</th>
						<td><%=boardDTO.getRgstIdnt()%></td>
					</tr>
					<tr>
						<th>이메일</th>
						<td><input type="text" class="input" name="mail" id="mail" title="이메일" maxlength="25" value="<%=boardDTO.getMail()%>" size="94" /></td>
					</tr>
					<tr>
						<th>내용</th>
						<td class="tdContents"> <textarea name="bordDesc" id="bordDesc" rows="8" class="input" title="내용"><%=boardDTO.getBordDesc()%></textarea> </td>
					</tr>
					<tr>
<%
          	List fileList = (List) boardDTO.getFileList();
          	int listSize = fileList.size();
%>
					<tr>
						<th>첨부파일</th>
						<td>
              <select name="atchCnt" onChange="javascript:applyAtch();" title="첨부파일수">
								<c:forEach begin="1" end="10" step="1" varStatus="cnt">
									<option value="<c:out value="${cnt.count}"/>"><c:out value="${cnt.count}"/></option>
								</c:forEach>
							</select>
<%            if (listSize > 0) {  %>
							<br><br>
							<ul>
						    ※ 삭제를 원하는 파일은 체크하십시요.
						  </ul>
<%            }  %>
							<ul>
<%

          	  for(int i=0; i<listSize; i++) {
          	    Board fileDTO = (Board) fileList.get(i);
%>
                <input type="checkbox" title="체크" name="chk" value="<%=fileDTO.getAttcSeqn()%>,<%=fileDTO.getFileName()%>"><a href="#1" onclick="javascript:fn_fileDownLoad('<%=fileDTO.getFilePath()%>','<%=fileDTO.getFileName()%>','<%=fileDTO.getRealFileName()%>')"><%=fileDTO.getFileName()%></a><br>
<%
	            }
%>
              <div id="attach">
								<table>
								  <tr>
								   	<td><input type="file" name="attachfile0" size="80" title="파일" class="input"></td>
								  </tr>
								</table>
							</div>
						  </ul>
						</td>
					</tr>
				</tbody>
			  </form>
			  <form name="form2" method="post">
				<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>">
				<input type="hidden" name="srchDivs" value="<%=srchDivs%>">
				<input type="hidden" name="srchText" value="<%=srchText%>">
				<input type="hidden" name="page_no" value="<%=page_no%>">
				<input type="submit" style="display:none;">
			</form>
			</table>
			<!--buttonArea start-->
			<div id="buttonArea">
				<div class="floatL"><a href="#1" onclick="javascript:fn_commList();"><img src="/images/button/list_btn.gif" alt="목록보기" /></a></div>
				<div class="floatR"><a href="#1" onclick="javascript:fn_update();"><img src="/images/button/save_btn.gif" alt="저장하기" width="66" height="21" /></a>
					                  <a href="#1" onclick="javascript:document.form1.reset();"><img src="/images/button/cancle_btn.gif" alt="취소하기" width="66" height="21" /></a></div>
			</div>
			<!--buttonArea end-->
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
