<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	String menuSeqn = request.getParameter("menuSeqn");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>권리자찾기</title>
<script type="text/JavaScript">
<!--
	function goPage(pageNo){
		var frm = document.form1;
		frm.page_no.value = pageNo;
		frm.submit();
	}

	function board_search() {
		var frm = document.form1;
		frm.page_no.value = 1;

	  frm.submit();
	}

	function fn_enterCheck(obj){
	  // EnterKey 입력시 공인인증서 로그인 수행
	  if (event.keyCode == 13) {
		  board_search();
	  }
  }

	function boardDetail(bordSeqn,menuSeqn,threaded){
		var frm = document.form1;
		frm.bordSeqn.value = bordSeqn;
		frm.menuSeqn.value = menuSeqn;
		frm.threaded.value = threaded;
		frm.page_no.value = '<%=page_no%>';
		frm.method = "post";
		frm.action = "/board/board.do?method=boardView";
		frm.submit();
  }

  function fn_goComm() {
		var frm = document.form1;
		frm.page_no.value = 1;
		frm.method = "post";
		frm.action = "/board/board.do?method=goComm";
		frm.submit();
	}
//-->
</script>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<div id="header">
	<jsp:include page="/include/top.jsp" flush="true">
		<jsp:param name="mNum" value="5" />
	</jsp:include>
	</div>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
<form name="form1">
  <input type="submit" style="display:none;"/>
  <input type="hidden" name="page_no" />
  <input type="hidden" name="menuSeqn" value="<%=menuSeqn%>" />
  <input type="hidden" name="bordSeqn" />
  <input type="hidden" name="threaded" />
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','122');
			</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="qustTlt">커뮤니티 목록</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>커뮤니티</li>
					<li class="on">커뮤니티 게시판</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>커뮤니티 게시판 목록</h2>
			<!--searchBox start-->
			<div id="searchBox">
				<select name="srchDivs" title="검색분류" title="검색어조건선택">
					<option value="">선택</option>
					<option value="01" <%="01".equals(srchDivs)?"selected":"" %>>제목</option>
					<option value="02" <%="02".equals(srchDivs)?"selected":"" %>>내용</option>
					<option value="03" <%="03".equals(srchDivs)?"selected":"" %>>작성자</option>
					<option value="04" <%="04".equals(srchDivs)?"selected":"" %>>제목+내용</option>
				</select>
				<input type="text" name="srchText" value="<%=srchText%>" class="input" title="검색어" size="35" onkeyup="fn_enterCheck(this)"/><a href="#1" onclick="javascript:board_search();"><img src="/images/button/srch2_btn.gif" alt="검색" align="middle" class="btn" /></a></div>
			<!--searchBox end-->
			<table width="710" class="board ListTop">
				<colgroup>
				<col width="50" />
				<col width="" />
				<col width="100" />
				<col width="80" />
				<col width="60" />
				<col width="50" />
				</colgroup>
				<caption summary="질문과답변 리스트를 보여줍니다.">질문과답변 목록</caption>
				<thead>
					<tr>
						<th>순번</th>
						<th>제목</th>
						<th>작성자</th>
						<th>작성일</th>
						<th>첨부파일</th>
						<th class="right">조회수</th>
					</tr>
				</thead>
				<tbody>
			  <c:if test="${boardList.totalRow == 0}">
					<tr>
						<td width="710" class="C" colspan="6">등록된 게시물이 없습니다.</td>
					</tr>
			  </c:if>
        <c:if test="${boardList.totalRow > 0}">
			    <c:forEach items="${boardList.resultList}" var="board">
	        <c:set var="NO" value="${board.TOTAL_CNT}"/>
        	<c:set var="i" value="${i+1}"/>
					<tr>
						<td class="C"><c:out value="${NO - i}"/></td>
						<td><a href="#1" onclick="javascript:boardDetail('${board.BORD_SEQN}','${board.MENU_SEQN}','${board.THREADED}')">
							<c:if test="${board.DETH == '0'}">
							  ${board.TITE}
							</c:if>
							<c:if test="${board.DETH == '1'}">
							  &nbsp;&nbsp;<img src="/images/re.gif" board="0" alt="">"${board.TITE}
							</c:if>
							<c:if test="${board.DETH == '2'}">
							  &nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/re.gif" board="0" alt="">"${board.TITE}
							</c:if>
							</a></td>
						<td class="C">${board.RGST_IDNT}</td>
						<td class="C">${board.RGST_DTTM}</td>
						<td class="C">${board.FILE_CONT}</td>
						<td class="C">${board.INQR_CONT}</td>
					</tr>
					</c:forEach>
		    </c:if>
				</tbody>
			</table>
			<!--paging start-->
			<div class="paging">
				<ul>
			 		<%-- 페이징 리스트 --%>
					  <jsp:include page="../common/PageList.jsp" flush="true">
						  <jsp:param name="totalItemCount" value="${boardList.totalRow}" />
							<jsp:param name="nowPage"        value="${param.page_no}" />
							<jsp:param name="functionName"   value="goPage" />
							<jsp:param name="listScale"      value="" />
							<jsp:param name="pageScale"      value="" />
							<jsp:param name="flag"           value="M01_FRONT" />
							<jsp:param name="extend"         value="no" />
						</jsp:include>
				</ul>
			</div>
			<!--paging end-->
			<!--buttonArea start-->
			<div id="buttonArea">
				<div class="floatR"><a href="#1" onclick="javascript:fn_goComm();"><img src="/images/button/qutn_btn.gif" alt="질문하기" width="66" height="21" /></a></div>
			</div>
			<!--buttonArea end-->
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</form>
</body>
</html>
