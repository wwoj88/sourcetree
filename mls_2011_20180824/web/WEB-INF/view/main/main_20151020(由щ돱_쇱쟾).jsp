<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%
    String submitChek = request.getParameter("submitChek") == null
					? ""
					: request.getParameter("submitChek");
	User user = SessionUtil.getSession(request);

	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head> 
<meta http-equiv="X-UA-Compatible" content="IE=9,chrome=1"/>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<script type="text/javascript">
<!--
 // SSL 적용 (로그인이 된경우는 https로 접근) + 운영서버에만 적용 (포트가'')
 if(location.protocol == 'http:' && '<%=sessUserIdnt%>' != 'null' && location.port ==''){
	 //location.replace('https://'+location.hostname+":"+location.port);
 }
//-->
</script>

<title>한국저작권위원회 저작권 찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="/js/jquery.cookie.js" type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript">
<!--
//오늘 날짜
today = new Date();
date = today.getDate();
hour = today.getHours();

var nowDateYMD = today.getFullYear()+""
+(today.getMonth()+1)+""
+today.getDate();

var nowDateMonth =  (today.getMonth()+1);

//날짜 비교
function chkValiDate(nowDate, chkDate) {

	var iNowDate = parseInt(nowDate);
	var iChkDate  = parseInt(chkDate);
	
	if(iChkDate > iNowDate) 
		return true;
	else
		return false;
}

/* 
//11월부터 이벤트 팝업존 안보이게 함.
$(document).ready(function() {
    var iNowDate = parseInt(nowDateMonth);
	var iChkDate  = parseInt("11");

	if(iNowDate >= iChkDate)  { 
		 document.getElementById("ban1").style.display = "block";
		 document.getElementById("ban2").style.display = "none";
	 	 document.getElementById('pop_btn1').style.display = "none";
		 document.getElementById('pop_btn2').style.display = "none";
 	}  
});  */

/*
 $(document).ready(function() {
		var height = 0;
		var left   = 0;
        //공고 팝업
        if( $.cookie("event_pop1") != "yes" ){
			window.open("/common/popUp/pop_20121005.html", "pop1", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=630,left=10,top=10");
        }
        if( $.cookie("event_pop2") != "yes" ){
			window.open("/common/popUp/pop_20121008.html", "pop2", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=477,height=608,left=540,top=10");
        }
    }); 

*/
//----------------------------------------------------------------------------------
var begin = 0;
var end = 0;
	function getCookie(name) {
		var cname = name + "=";
		var dc = document.cookie;
		if (dc.length > 0) {
			//alert(dc.length);
			//alert(cname);
			begin = dc.indexOf(cname);
			if (begin != -1) {
				begin += cname.length;
				end = dc.indexOf(";", begin);
				if (end == -1){
					end = dc.length;
					//alert(unescape(dc.substring(begin, end)));
					return unescape(dc.substring(begin, end));
				}
			}
		}
		return null;
	}

	var height = 0;
	var left   = 0;
	
var curDate = "20121009";

/*
if ('20121008' <= curDate && '20121011' >= curDate ) {
if ( $.cookie("event_pop1") != "yes" ){
	height = 605 + 25;
	window.open("/common/popUp/pop_20121005.html", "_blank","width=500, height=" + height + ", top=0, left=" + left + ", toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no");
	left += 500 + 20;
 }
}


if ('20121008' <= curDate && '20121010' >= curDate ) {
if ( $.cookie("event_pop2") != "yes" ){
	height = 584 + 25;
	window.open("/common/popUp/pop_20121008.html", "225", "width=477, height=" + height + ", top=0, left=" + left + ", toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no");
	left += 350 + 20;
 }
}


if ('20121008' <= curDate && '20121010' >= curDate ) {
	if ( $.cookie("event_pop3") != "yes" ){
		height = 634 + 25;
		window.open("/common/popUp/pop_20121109.html", "227", "width=550, height=" + height + ", top=0, left=" + left + ", toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no");
		//left += 700 + 20;
	 }
}
*/
/* //20121101일 부터 팝업오픈 안함.
if( chkValiDate(nowDateYMD, '20121101')	)  {
	if( (date -15)>0 )  {
		if ( $.cookie("event_pop3") != "yes" ){
			height = 672 + 25;
			window.open("/common/popUp/pop_20121012.html", "227", "width=554, height=" + height + ", top=0, left=" + left + ", toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no");
			//left += 700 + 20;
		 }
	}
} */

//----------------------------------------------------------------------------------

function isMobile()
{
	var bFlag = false;
    var mobileKeyWords = new Array('iPhone', 'iPod', 'BlackBerry', 'Android', 'Windows CE', 'LG', 'MOT', 'SAMSUNG', 'SonyEricsson');
    
    for (var word in mobileKeyWords){
    
        if (navigator.userAgent.match(mobileKeyWords[word]) != null){
           
           bFlag = true;
           break;
        }
    }
    
    return bFlag;
}


function  lfn_Start()
{	
	if(isMobile()){
		 alert("모바일접근");
         location.href="http://m.findcopyright.or.kr";
	}else{
		document.frm.submit(); 
	}
        
  return;
}

function baShowImage(n) {
	
	
	if(n==1) {
		document.getElementById("ban1").style.display = "block";
		document.getElementById("ban2").style.display = "none";
		document.getElementById('pop_btn1').className = 'btn on';
		document.getElementById('pop_btn2').className = 'btn';
		
	}else if(n==2) {
		document.getElementById("ban1").style.display = "none";
		document.getElementById("ban2").style.display = "block";
		document.getElementById('pop_btn1').className = 'btn';
		document.getElementById('pop_btn2').className = 'btn on';
	}
}

function ceShowImage(n){
	
	if(n==0) { 
		document.getElementById('rights').src = '/images/2014/main/rights_off.gif';
		document.getElementById('user').src = '/images/2014/main/user_off.gif';

		document.getElementById("box2").style.display = "none";
		document.getElementById("box1").style.display = "none";
		document.getElementById("box3").style.display = "block";
	}else if(n==1) {
		document.getElementById('rights').src = '/images/2014/main/rights_on.gif';
		document.getElementById('user').src = '/images/2014/main/user_off.gif';
		
		document.getElementById("box1").style.display = "block";
		document.getElementById("box2").style.display = "none";
		document.getElementById("box3").style.display = "none";
	}else if(n==2) {
		document.getElementById('rights').src = '/images/2014/main/rights_off.gif';
		document.getElementById('user').src = '/images/2014/main/user_on.gif';

		document.getElementById("box2").style.display = "block";
		document.getElementById("box1").style.display = "none";
		document.getElementById("box3").style.display = "none";
	}else if(n==3){
		document.getElementById("anucBord01").style.display = "block";
		document.getElementById("anucBord06").style.display = "none";
		
		$('#b01').removeClass('tab_m bgNone pl0');
		$('#b01').addClass('tab_m active bgNone pl0');
		$('#b06').removeClass('tab_m active');
		$('#b06').addClass('tab_m');
	}else if(n==4){
		document.getElementById("anucBord01").style.display = "none";
		document.getElementById("anucBord06").style.display = "block";
		
		$('#b01').removeClass('tab_m active bgNone pl0');
		$('#b01').addClass('tab_m bgNone pl0');
		$('#b06').removeClass('tab_m');
		$('#b06').addClass('tab_m active');
		
	}else if(n==5){
		document.getElementById("anucBord03").style.display = "none";
		document.getElementById("anucBord04").style.display = "none";
		document.getElementById("anucBord05").style.display = "none";
		document.getElementById("statSrch").style.display = "block";
		
		$('#bStatSrch').removeClass('tab_m bgNone pl0');
		$('#bStatSrch').addClass('tab_m active bgNone pl0');
		$('#b03').removeClass('tab_m active');
		$('#b03').addClass('tab_m');
		$('#b04').removeClass('tab_m active');
		$('#b04').addClass('tab_m');
		$('#b05').removeClass('tab_m active');
		$('#b05').addClass('tab_m');
	}else if(n==6){
		document.getElementById("anucBord03").style.display = "block";
		document.getElementById("anucBord04").style.display = "none";
		document.getElementById("anucBord05").style.display = "none";
		document.getElementById("statSrch").style.display = "none";
		
		$('#bStatSrch').removeClass('tab_m active bgNone pl0');
		$('#bStatSrch').addClass('tab_m bgNone pl0');
		$('#b03').removeClass('tab_m');
		$('#b03').addClass('tab_m active');
		$('#b04').removeClass('tab_m active');
		$('#b04').addClass('tab_m');
		$('#b05').removeClass('tab_m active');
		$('#b05').addClass('tab_m');
	}else if(n==7){
		document.getElementById("anucBord03").style.display = "none";
		document.getElementById("anucBord04").style.display = "block";
		document.getElementById("anucBord05").style.display = "none";
		document.getElementById("statSrch").style.display = "none";
		
		$('#bStatSrch').removeClass('tab_m active bgNone pl0');
		$('#bStatSrch').addClass('tab_m bgNone pl0');
		$('#b03').removeClass('tab_m active');
		$('#b03').addClass('tab_m');
		$('#b04').removeClass('tab_m');
		$('#b04').addClass('tab_m active');
		$('#b05').removeClass('tab_m active');
		$('#b05').addClass('tab_m');
	}else if(n==8){
		document.getElementById("anucBord03").style.display = "none";
		document.getElementById("anucBord04").style.display = "none";
		document.getElementById("anucBord05").style.display = "block";
		document.getElementById("statSrch").style.display = "none";
		
		$('#bStatSrch').removeClass('tab_m active bgNone pl0');
		$('#bStatSrch').addClass('tab_m bgNone pl0');
		$('#b03').removeClass('tab_m active');
		$('#b03').addClass('tab_m');
		$('#b04').removeClass('tab_m active');
		$('#b04').addClass('tab_m');
		$('#b05').removeClass('tab_m');
		$('#b05').addClass('tab_m active');
	}
	
}

function boardDetail(bordSeqn,menuSeqn,threaded){	//자주묻는질문
	var frm = document.form1;
	frm.bordSeqn.value = bordSeqn;
	frm.menuSeqn.value = menuSeqn;
	frm.threaded.value = threaded;
	frm.page_no.value = '1';
	frm.method = "post";
	frm.action = "/board/board.do?method=boardView";
	frm.submit();
}
// 오픈 팝업 open_popp
function fn_openPopup(){
	
	// var sUrl = "/install/popUp_20100412.jsp";
//	var sUrl = "/popUp/pop_20101208.html";
		var sUrl = "/popUp/pop_20150605.html";
	
	if(fn_getCookie()){
		window.open(sUrl, "openPopup", "toolbar=no,status=yes,scrollbars=no,width=430,height=380");
	}
}

// 팝업 오픈 설정 정보
function fn_getCookie() {
	var rtn = "";
	var search = "mlsmessage=";
	if (document.cookie.length>0){
		offset = document.cookie.indexOf(search);
		if (offset != -1){
			offset += search.length;
			end = document.cookie.indexOf(";", offset);
			if (end == -1)
				end = document.cookie.length;
			rtn = unescape(document.cookie.substring(offset, end));
		}
	}

	if(rtn == "done1") {
		return false;
	}
	
	return true;
}
	function onload(){
	
	}
//-->
</script>

</head>
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">

		<!-- HEADER str-->
		<jsp:include page="/include/2012/mainHeader.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->

		<!-- CONTAINER str-->
		<div id="container" class="main_container">
			<!-- main Link -->
			<div class="main_mid">
				<div class="main_link">

					<!-- 내권리 찾기 -->
					<a href="javascript:ceShowImage(1);" class="box_link rights"><img id="rights" title="내권리찾기 권리자" alt="내권리찾기 권리자" src="/images/2014/main/rights_off.gif">
					</a>
					<div id="box1" class="box_visual rights" style="display: none;">
						<h2>내권리찾기 <em>권리자</em></h2>
						<strong>내 저작물의 저작권 정보를 확인하여 잘못된 정보일 경우 수정해 주세요.</strong>
						<a href="/rghtPrps/rghtSrch.do?DIVS=M" class="rights_btn1">저작권 정보 확인</a>
						<strong>내 저작물이 미분배 보상 저작물로 확인되면 보상금 분배 신청하세요.</strong>
						<a href="/inmtPrps/inmtPrps.do?mNum=4&sNum=0&leftsub=0&srchDIVS=1" class="rights_btn2">미분배 보상금 대상 저작물 확인</a>
					</div>
					<!-- //내권리 찾기 -->

					<!-- 저작권자 찾기 -->
					<a href="javascript:ceShowImage(2);" class="box_link user"><img id="user" title="저작권자 찾기 이용자" alt="저작권자 찾기 이용자" src="/images/2014/main/user_off.gif">
					</a>
					<div id="box2" class="box_visual user" style="display: none;">
						<h2>저작권자찾기 이용자</h2>
						<strong>저작권자 검색 서비스로 편리하게 저작권자를 확인하세요. 검색되지 않은 저작권자의 저작물 이용은 상당한 노력 및 법정허락 이용승인 신청 서비스를 통해 해결하세요.</strong>
						<a href="/srchList.do" class="user_btn1">저작권자 검색 및 상당한 노력 신청</a>
						<a href="/stat/statSrch.do" class="user_btn2">법정허락 이용승인 신청</a>
					</div>
					<!-- //저작권자 찾기 -->

					<!-- 저작권찾기서비스 소개 -->
					<div id="box3" class="box_visual info" style="display: block;">
						<h2>저작권찾기서비스란?</h2>
						<ul>
						<li>권리자에게는 저작권에 대한 정당한 보상을 받을 수 있도록 저작권 정보와 미분배 보상금 대상 저작물 목록을 제공합니다</li>
						<li>이용자에게는 권리자를 알 수 없어 저작물을 이용 못하는 어려움을 해소할 수 있도록 저작권 찾기 서비스를 제공합니다.</li>
						</ul>
						<p>올바른 저작권 이용이 만드는 행복한 문화세상 만들기!! 저작권찾기가 그 시작입니다.</p>
					</div>
					<!-- //저작권찾기서비스 소개 -->
				</div>
			</div>
			<!-- //main Link -->
						
			<!-- main Search + Popup zone -->
			<div class="main_midSch floatDiv">
				<fieldset class="search_box">
					<legend>통합검색</legend>
					<label for="total_sch">통합검색</label><input type="text" name="srchWord" id="total_sch" class="inputData" onkeydown="javascript:fn_enterChk(event, 1);" /><input type="image" src="/images/2014/main/btn_midSearch.gif" alt="검색" class="btn_input" onclick="javascript:goInnerSearch(1);" />
				</fieldset>
				<div class="main_pop"><img src="/images/2014/main/main_pop.gif" alt="" />
					</div>
			</div>
			<!-- //main Search + Popup zone -->

			<!-- main Data -->
			<div class="main_data floatDiv" id="contentBody">
				<div class="data_tb">
					<h2>
						<img alt="저작권자 찾기" src="/images/2012/main/h2_data_tb1.gif">
					</h2>
					<ul>
						<li><a href="javascript:ceShowImage(3);" id="b01" class="tab_m active bgNone pl0" onmouseover="ceShowImage(3);" onmouseout="ceShowImage(3);" onfocus="ceShowImage(3);">저작권자
								조회공고<!--img alt="" src="/images/2012/main/data_tb1_m1_on.gif" -->
						</a>
							<div id="anucBord01">
								<table border="1" summary="저작권자 조회공고 목록">
									<caption>저작권자 조회공고</caption>
									<colgroup>
										<col width="20%">
										<col width="*">
										<col width="20%">
									</colgroup>

									<tbody>
										<c:if test="${bord01Size == 0}">
											<tr>
												<td class="ce" colspan="6">등록된 게시물이 없습니다.</td>
											</tr>
										</c:if>
										<c:if test="${bord01Size > 0 }">
											<c:forEach var="anucBord01" items="${anucBord01}">
												<tr>
													<%-- 									${anucBord01.divsCdName}--%>
													<th scope="col" style="vertical-align:text-top; padding-top: 3px;">${anucBord01.genreCdName}</th>
													<td class="tl" style="vertical-align:text-top; padding-top: 3px;"><a
														href="/statBord/statBo01Detl.do?bordSeqn=${anucBord01.bordSeqn}&amp;bordCd=${anucBord01.bordCd}&amp;divsCd=${anucBord01.divsCd}">
															${fn:replace(anucBord01.tite, '<', '&lt;')}</a>
													</td>
													<td class="rgt" style="vertical-align:text-top; padding-top: 3px;">${anucBord01.openDttm}</td>
												</tr>
												<c:if test="${bord01Size < 4}">
													<c:forEach step="1" begin="0" end="${3-bord01Size}">
														<tr>

														</tr>
													</c:forEach>
												</c:if>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
								<a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5"
									id="anucBord01More" class="more"><img alt="저작권자조회공고 더보기"
									src="/images/2012/main/more2.gif">
								</a>
							</div></li>
						<li><a href="javascript:ceShowImage(4)" class="tab_m"
							id="b06" onmouseover="ceShowImage(4);"
							onmouseout="ceShowImage(4);" onfocus="ceShowImage(4);">상당한노력
								공고<!--img alt="" src="/images/2012/main/data_tb1_m2_off.gif" -->
						</a>
							<div id="anucBord06" style="display: none;">
								<table border="1" summary="상당한노력 공고 목록">
									<caption>상당한노력 공고</caption>
									<colgroup>
										<col width="20%">
										<col width="*">
										<col width="20%">
									</colgroup>

									<tbody>
										<c:if test="${bord06Size == 0}">
											<tr>
												<td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
											</tr>
										</c:if>
										<c:if test="${bord06Size > 0 }">
											<c:forEach var="anucBord06" items="${anucBord06}">
												<tr>
													<th scope="col" style="vertical-align:text-top; padding-top: 3px;">${anucBord06.genreCdName}</th>
													<td class="tl" style="vertical-align:text-top; padding-top: 3px;"><a
														href="/statBord/statBo01Detl.do?bordSeqn=${anucBord06.bordSeqn}&amp;bordCd=${anucBord06.bordCd}&amp;divsCd=${anucBord06.divsCd}">
														${fn:replace(anucBord06.tite, '<', '&lt;')}</a>
													</td>
													<td class="rgt" style="vertical-align:text-top; padding-top: 3px;">${anucBord06.openDttm}</td>

													<c:if test="${bord06Size < 4}">
														<c:forEach step="1" begin="0" end="${3-bord06Size}">
															<tr><th>&nbsp;</th><td>&nbsp;</td><td>&nbsp;</td></tr>
														</c:forEach>
													</c:if>
												</tr>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
								<a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=4"
									id="anucBord06More" class="more"><img alt="상당한노력공고 더보기"
									src="/images/2012/main/more2.gif">
								</a>
							</div></li>
					</ul>
				</div>

				<div class="data_tb ml25">
					<h2>
						<img alt="법정허락" src="/images/2012/main/h2_data_tb2.gif">
					</h2>
					<ul>
						<li><a href="javascript:ceShowImage(5)" id="bStatSrch"
							class="tab_m active bgNone pl0" onmouseover="ceShowImage(5);"
							onmouseout="ceShowImage(5);" onfocus="ceShowImage(5);">신청대상
								저작물<!-- img alt="" src="/images/2012/main/data_tb2_m1_on.gif" -->
						</a>
							<div id="statSrch">
								<table border="1" summary="신청대상 저작물 목록">
									<caption>신청대상 저작물</caption>
									<colgroup>
										<col width="20%">
										<col width="*">
									</colgroup>
									<tbody>
										<c:if test="${statSrchSize == 0}">
											<tr>
												<td class="ce" colspan="2">등록된 게시물이 없습니다.</td>
											</tr>
										</c:if>
										<c:if test="${statSrchSize > 0 }">
											<c:forEach var="statSrch" items="${statSrch}">
												<tr>
													<th scope="col" style="vertical-align:text-top; padding-top: 3px;">${statSrch.genreCdName}</th>
													<td class="tl" style="vertical-align:text-top; padding-top: 3px;">
													${fn:replace(statSrch.workTitle, '<', '&lt;')}</td>
												</tr>
												<c:if test="${statSrchSize < 4}">
													<c:forEach step="1" begin="0" end="${3-statSrchSize}">
														<tr><th>&nbsp;</th><td>&nbsp;</td></tr>
													</c:forEach>
												</c:if>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
								<a href="/stat/statSrch.do" class="more"><img
									alt="신청대상 저작물 더보기" src="/images/2012/main/more2.gif">
								</a>
							</div></li>
						<li><a href="javascript:ceShowImage(6)" id="b03"
							class="tab_m" onmouseover="ceShowImage(6);"
							onmouseout="ceShowImage(6);" onfocus="ceShowImage(6);">이용승인신청<!-- img alt="" src="/images/2012/main/data_tb2_m2_off.gif" -->
						</a>
							<div id="anucBord03" style="display: none;">
								<table border="1" summary="이용승인신청 목록">
									<caption>이용승인신청</caption>
									<colgroup>
										<col width="*">
										<col width="20%">
									</colgroup>

									<tbody>
										<c:if test="${bord03Size == 0}">
											<tr>
												<td class="ce" colspan="2">등록된 게시물이 없습니다.</td>
											</tr>
										</c:if>
										<c:if test="${bord03Size > 0 }">
											<c:forEach var="anucBord03" items="${anucBord03}">
												<tr>
													<%-- 									<th>${anucBord03.divsCdName}</th> --%>
													<%-- 									<td class="ce">${anucBord03.genreCdName}</td> --%>
													<th scope="col" style="vertical-align:text-top; padding-top: 3px;"><a
														href="/statBord/statBo01Detl.do?bordSeqn=${anucBord03.bordSeqn}&amp;bordCd=${anucBord03.bordCd}&amp;divsCd=${anucBord03.divsCd}">
														${fn:replace(anucBord03.tite, '<', '&lt;')}</a>
													</th>
													<td class="rgt" style="vertical-align:text-top; padding-top: 3px;">${anucBord03.openDttm}</td>
												</tr>
												<c:if test="${bord03Size < 4}">
													<c:forEach step="1" begin="0" end="${3-bord03Size}">
														<tr><th>&nbsp;</th><td>&nbsp;</td></tr>
													</c:forEach>
												</c:if>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
								<a href="/statBord/statBo03List.do?bordCd=3" class="more"><img
									alt="이용승인신청 더보기" src="/images/2012/main/more2.gif">
								</a>
							</div></li>
						<li><a href="javascript:ceShowImage(7)" id="b04"
							class="tab_m" onmouseover="ceShowImage(7);"
							onmouseout="ceShowImage(7);" onfocus="ceShowImage(7);">승인공고<!-- img alt="" src="/images/2012/main/data_tb2_m3_off.gif" -->
						</a>
							<div id="anucBord04" style="display: none;">
								<table border="1" summary="승인공고 목록">
									<caption>승인공고</caption>
									<colgroup>
										<col width="*">
										<col width="20%">
									</colgroup>

									<tbody>
										<c:if test="${bord04Size == 0}">
											<tr>
												<td class="ce" colspan="2">등록된 게시물이 없습니다.</td>
											</tr>
										</c:if>
										<c:if test="${bord04Size > 0 }">
											<c:forEach var="anucBord04" items="${anucBord04}">
												<tr>
													<%-- 									<th>${anucBord04.divsCdName}</th> --%>
													<%-- 									<td class="ce">${anucBord04.genreCdName}</td> --%>
													<th scope="col" style="vertical-align:text-top; padding-top: 3px;"><a
														href="/statBord/statBo01Detl.do?bordSeqn=${anucBord04.bordSeqn}&amp;bordCd=${anucBord04.bordCd}&amp;divsCd=${anucBord04.divsCd}">
														${fn:replace(anucBord04.tite, '<', '&lt;')}</a>
													</th>
													<td class="rgt" style="vertical-align:text-top; padding-top: 3px;">${anucBord04.openDttm}</td>
												</tr>
												<c:if test="${bord04Size < 4}">
													<c:forEach step="1" begin="0" end="${3-bord04Size}">
														<tr><th>&nbsp;</th><td>&nbsp;</td></tr>
													</c:forEach>
												</c:if>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
								<a href="/statBord/statBo04List.do?bordCd=4" class="more"><img
									alt="승인공고 더보기" src="/images/2012/main/more2.gif">
								</a>
							</div></li>
						<li><a href="javascript:ceShowImage(8)" id="b05"
							class="tab_m" onmouseover="ceShowImage(8);"
							onmouseout="ceShowImage(8);" onfocus="ceShowImage(8);">보상금 공탁
								공고<!-- img alt="" src="/images/2012/main/data_tb2_m3_off.gif" -->
						</a>
							<div id="anucBord05" style="display: none;">
								<table border="1" summary="보상금 공탁공고 목록">
									<caption>보상금 공탁공고</caption>
									<colgroup>
										<col width="*">
										<col width="20%">
									</colgroup>

									<tbody>
										<c:if test="${bord05Size == 0}">
											<tr>
												<td class="ce" colspan="2">등록된 게시물이 없습니다.</td>
											</tr>
										</c:if>
										<c:if test="${bord05Size > 0 }">
											<c:forEach var="anucBord05" items="${anucBord05}">
												<tr>
													<%-- 									<th>${anucBord05.divsCdName}</th> --%>
													<%-- 									<td class="ce">${anucBord05.genreCdName}</td> --%>
													<th scope="col" style="vertical-align:text-top; padding-top: 3px;"><a
														href="/statBord/statBo01Detl.do?bordSeqn=${anucBord05.bordSeqn}&amp;bordCd=${anucBord05.bordCd}&amp;divsCd=${anucBord05.divsCd}">
														${fn:replace(anucBord05.tite, '<', '&lt;')}</a>
													</th>
													<td class="rgt" style="vertical-align:text-top; padding-top: 3px;">${anucBord05.openDttm}</td>
												</tr>
												<c:if test="${bord05Size < 4}">
													<c:forEach step="1" begin="0" end="${3-bord05Size}">
														<tr><th>&nbsp;</th><td>&nbsp;</td></tr>
													</c:forEach>
												</c:if>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
								<a href="/statBord/statBo05List.do?bordCd=5" class="more"><img
									alt="보상긍공탁공고 더보기" src="/images/2012/main/more2.gif">
								</a>
							</div></li>
					</ul>
				</div>

				<div class="main_faq">
					<form name="form1" method="post" action="">
						<input type="hidden" name="bordSeqn" /> <input type="hidden"
							name="menuSeqn" /> <input type="hidden" name="threaded" /> <input
							type="hidden" name="page_no" />
					</form>

					<h2>
						<img alt="자주묻는질문" src="/images/2012/main/h2_main_faq.gif">
					</h2>
					
					<a
						href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1"
						class="more"><img alt="자주묻는 질문 더보기" title="자주묻는 질문 더보기"
						src="/images/2012/main/more.gif">
					</a>
					<ul class="mt5">
						<c:forEach var="QnABord" items="${QnABord}">
							<li><a href="#1"
								onclick="javascript:boardDetail('${QnABord.bordSeqn}','${QnABord.menuSeqn}','${QnABord.threaded}')">
								${fn:replace(QnABord.tite, '<', '&lt;')}<span>${QnABord.rgstDttm}</span>
							</a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
			<!-- //main Data -->
					<!-- 홍보동영상2015.02.09
			<div id="main_promoteMv">
		    <div class="mov mt3">
		     <object width="303" height="194" type="application/x-shockwave-flash" data="/images/swf/655_420.swf">
		      <param name="movie" value="/images/swf/655_420.swf">  
		      <param name="quality" value="high">   
		      <param name="play" value="true">  
		      <param name="loop" value="true">  
		      <param name="wmode" value="transparent">  
		      <param name="scale" value="showall">  
		      <param name="menu" value="true">  
		      <param name="devicefont" value="false">  
		      <param name="salign" value="">  
		      <param name="allowScriptAccess" value="sameDomain">
		      <embed width="303" height="194" name="" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="/images/swf/655_420.swf" wmode="transparent" quality="high" allowscriptaccess="always" allowfullscreen="true" flashvars="">
		     </object>  
		    </div>
		 		 <p class="ce"><a href="#none" onclick="window.open('https://www.youtube.com/watch?v=eqRh5bWIEZ8','안내동영상','menubar=yes, status=yes, toolbar=yes,fullscreen=yes,resizable=yes,scrollbars=yes');" class="inBlock p12 white line15 mr15 mt8">+ CF 영상(20초) 보기</a> 
		 		 <a href="#none" onclick="window.open('https://www.youtube.com/watch?v=6k8AHSQgcUY','안내동영상','menubar=yes, status=yes, toolbar=yes,fullscreen=yes,resizable=yes,scrollbars=yes');" class="inBlock p12 white line15 mt8">+ 사이트 안내 영상 보기</a></p>
      </div>
			<!-- //홍보동영상 
				</div>
			</div>
			<!-- //홍보동영상 -->
			
		</div>
		<!-- //CONTAINER end -->

		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->

	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>