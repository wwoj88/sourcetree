<jsp:include page="/include/2017/header.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr"/>
<meta name="description" content="분야별 권리자 찾기· 미분배보상금 저작물 찾기· 법적허락승인 신청·위탁관리저작물 등록 안내"/>
<meta name="title" content="권리자 찾기" />
<meta property="og:title"        content="권리자 찾기 정보시스템   올바른 저작물 이용이 만드는 행복한 세상" />
<meta property="og:type"         content="website" />
<meta property="og:url"          content="http://www.findcopyright.or.kr" />
<meta property="og:description"  content="권리자 찾기 서비스는 저작권자를 찾지 못해 저작물을 이용하지 못했던 분들을 위해 저작권 정보 검색 서비스 제공· 미분배보상금 검색 서비스 제공" />
<meta property="og:locale"       content="ko_KR"/>
  <link rel="preload" as="style" href="https://fonts.googleapis.com/css2?family=Nanum+Gothic&display=swap" />
<title>한국저작권위원회 권리자 찾기 사이트</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css"/>
<!-- <link type="text/css" rel="stylesheet" href="/css/2012/style.css"/> -->
<link type="text/css" rel="stylesheet" href="/css/2017/new.css"/>

<script type="text/javascript" language="javascript" src="/js/Function.js"></script>
<script type="text/javascript" language="javascript" src="/js/jquery.cookie.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" language="javascript" src="/js/main/menu.js"></script>
<script type="text/javascript" language="javascript" src="/js/main/js.js"></script>

<script type="text/javascript">
<!--
//오늘 날짜
today = new Date();
date = today.getDate();
hour = today.getHours();

var nowDateYMD = today.getFullYear()+""
+(today.getMonth()+1)+""
+today.getDate();

var nowDateMonth =  (today.getMonth()+1);

//날짜 비교
function chkValiDate(nowDate, chkDate) {

  var iNowDate = parseInt(nowDate);
  var iChkDate  = parseInt(chkDate);
  
  if(iChkDate > iNowDate) 
    return true;
  else
    return false;
}

/* 
//11월부터 이벤트 팝업존 안보이게 함.
$(document).ready(function() {
    var iNowDate = parseInt(nowDateMonth);
  var iChkDate  = parseInt("11");

  if(iNowDate >= iChkDate)  { 
     document.getElementById("ban1").style.display = "block";
     document.getElementById("ban2").style.display = "none";
     document.getElementById('pop_btn1').style.display = "none";
     document.getElementById('pop_btn2').style.display = "none";
  }  
});  */

/*
 $(document).ready(function() {
    var height = 0;
    var left   = 0;
        //공고 팝업
        if( $.cookie("event_pop1") != "yes" ){
      window.open("/common/popUp/pop_20121005.html", "pop1", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=630,left=10,top=10");
        }
        if( $.cookie("event_pop2") != "yes" ){
      window.open("/common/popUp/pop_20121008.html", "pop2", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=477,height=608,left=540,top=10");
        }
    }); 

*/

var begin = 0;
var end = 0;
  function getCookie(name) {
    var cname = name + "=";
    var dc = document.cookie;
    if (dc.length > 0) {
      //alert(dc.length);
      //alert(cname);
      begin = dc.indexOf(cname);
      if (begin != -1) {
        begin += cname.length;
        end = dc.indexOf(";", begin);
        if (end == -1){
          end = dc.length;
          //alert(unescape(dc.substring(begin, end)));
          return unescape(dc.substring(begin, end));
        }
      }
    }
    return null;
  }

  var height = 0;
  var left   = 0;
  
var curDate = "20121009";

/*
if ('20121008' <= curDate && '20121011' >= curDate ) {
if ( $.cookie("event_pop1") != "yes" ){
  height = 605 + 25;
  window.open("/common/popUp/pop_20121005.html", "_blank","width=500, height=" + height + ", top=0, left=" + left + ", toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no");
  left += 500 + 20;
 }
}


if ('20121008' <= curDate && '20121010' >= curDate ) {
if ( $.cookie("event_pop2") != "yes" ){
  height = 584 + 25;
  window.open("/common/popUp/pop_20121008.html", "225", "width=477, height=" + height + ", top=0, left=" + left + ", toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no");
  left += 350 + 20;
 }
}


if ('20121008' <= curDate && '20121010' >= curDate ) {
  if ( $.cookie("event_pop3") != "yes" ){
    height = 634 + 25;
    window.open("/common/popUp/pop_20121109.html", "227", "width=550, height=" + height + ", top=0, left=" + left + ", toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no");
    //left += 700 + 20;
   }
}
*/
/* //20121101일 부터 팝업오픈 안함.
if( chkValiDate(nowDateYMD, '20121101') )  {
  if( (date -15)>0 )  {
    if ( $.cookie("event_pop3") != "yes" ){
      height = 672 + 25;
      window.open("/common/popUp/pop_20121012.html", "227", "width=554, height=" + height + ", top=0, left=" + left + ", toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no");
      //left += 700 + 20;
     }
  }
} */



function isMobile()
{
  var bFlag = false;
    var mobileKeyWords = new Array('iPhone', 'iPod', 'BlackBerry', 'Android', 'Windows CE', 'LG', 'MOT', 'SAMSUNG', 'SonyEricsson');
    
    for (var word in mobileKeyWords){
    
        if (navigator.userAgent.match(mobileKeyWords[word]) != null){
           
           bFlag = true;
           break;
        }
    }
    
    return bFlag;
}


function  lfn_Start()
{ 
  if(isMobile()){
     alert("모바일접근");
         location.href="http://m.findcopyright.or.kr";
  }else{
    document.frm.submit(); 
  }
        
  return;
}

function baShowImage(n) {
  
  
  if(n==1) {
    document.getElementById("ban1").style.display = "block";
    document.getElementById("ban2").style.display = "none";
    document.getElementById('pop_btn1').className = 'btn on';
    document.getElementById('pop_btn2').className = 'btn';
    
  }else if(n==2) {
    document.getElementById("ban1").style.display = "none";
    document.getElementById("ban2").style.display = "block";
    document.getElementById('pop_btn1').className = 'btn';
    document.getElementById('pop_btn2').className = 'btn on';
  }
}

function ceShowImage(n){
  
  if(n==0) { 
    document.getElementById('rights').src = '/images/2014/main/rights_off.gif';
    document.getElementById('user').src = '/images/2014/main/user_off.gif';

    document.getElementById("box2").style.display = "none";
    document.getElementById("box1").style.display = "none";
    document.getElementById("box3").style.display = "block";
  }else if(n==1) {
    document.getElementById('rights').src = '/images/2014/main/rights_on.gif';
    document.getElementById('user').src = '/images/2014/main/user_off.gif';
    
    document.getElementById("box1").style.display = "block";
    document.getElementById("box2").style.display = "none";
    document.getElementById("box3").style.display = "none";
  }else if(n==2) {
    document.getElementById('rights').src = '/images/2014/main/rights_off.gif';
    document.getElementById('user').src = '/images/2014/main/user_on.gif';

    document.getElementById("box2").style.display = "block";
    document.getElementById("box1").style.display = "none";
    document.getElementById("box3").style.display = "none";
  }else if(n==3){
    document.getElementById("anucBord01").style.display = "block";
    document.getElementById("anucBord06").style.display = "none";
    
    $('#b01').removeClass('tab_m bgNone pl0');
    $('#b01').addClass('tab_m active bgNone pl0');
    $('#b06').removeClass('tab_m active');
    $('#b06').addClass('tab_m');
  }else if(n==4){
    document.getElementById("anucBord01").style.display = "none";
    document.getElementById("anucBord06").style.display = "block";
    
    $('#b01').removeClass('tab_m active bgNone pl0');
    $('#b01').addClass('tab_m bgNone pl0');
    $('#b06').removeClass('tab_m');
    $('#b06').addClass('tab_m active');
    
  }else if(n==5){
    document.getElementById("anucBord03").style.display = "none";
    document.getElementById("anucBord04").style.display = "none";
    document.getElementById("anucBord05").style.display = "none";
    document.getElementById("statSrch").style.display = "block";
    
    $('#bStatSrch').removeClass('tab_m bgNone pl0');
    $('#bStatSrch').addClass('tab_m active bgNone pl0');
    $('#b03').removeClass('tab_m active');
    $('#b03').addClass('tab_m');
    $('#b04').removeClass('tab_m active');
    $('#b04').addClass('tab_m');
    $('#b05').removeClass('tab_m active');
    $('#b05').addClass('tab_m');
  }else if(n==6){
    document.getElementById("anucBord03").style.display = "block";
    document.getElementById("anucBord04").style.display = "none";
    document.getElementById("anucBord05").style.display = "none";
    document.getElementById("statSrch").style.display = "none";
    
    $('#bStatSrch').removeClass('tab_m active bgNone pl0');
    $('#bStatSrch').addClass('tab_m bgNone pl0');
    $('#b03').removeClass('tab_m');
    $('#b03').addClass('tab_m active');
    $('#b04').removeClass('tab_m active');
    $('#b04').addClass('tab_m');
    $('#b05').removeClass('tab_m active');
    $('#b05').addClass('tab_m');
  }else if(n==7){
    document.getElementById("anucBord03").style.display = "none";
    document.getElementById("anucBord04").style.display = "block";
    document.getElementById("anucBord05").style.display = "none";
    document.getElementById("statSrch").style.display = "none";
    
    $('#bStatSrch').removeClass('tab_m active bgNone pl0');
    $('#bStatSrch').addClass('tab_m bgNone pl0');
    $('#b03').removeClass('tab_m active');
    $('#b03').addClass('tab_m');
    $('#b04').removeClass('tab_m');
    $('#b04').addClass('tab_m active');
    $('#b05').removeClass('tab_m active');
    $('#b05').addClass('tab_m');
  }else if(n==8){
    document.getElementById("anucBord03").style.display = "none";
    document.getElementById("anucBord04").style.display = "none";
    document.getElementById("anucBord05").style.display = "block";
    document.getElementById("statSrch").style.display = "none";
    
    $('#bStatSrch').removeClass('tab_m active bgNone pl0');
    $('#bStatSrch').addClass('tab_m bgNone pl0');
    $('#b03').removeClass('tab_m active');
    $('#b03').addClass('tab_m');
    $('#b04').removeClass('tab_m active');
    $('#b04').addClass('tab_m');
    $('#b05').removeClass('tab_m');
    $('#b05').addClass('tab_m active');
  }
  
}

function boardDetail(bordSeqn,menuSeqn,threaded){ //자주묻는질문
  var frm = document.form1;
  frm.bordSeqn.value = bordSeqn;
  frm.menuSeqn.value = menuSeqn;
  frm.threaded.value = threaded;
  frm.page_no.value = '1';
  frm.method = "post";
  frm.action = "/board/board.do?method=boardView";
  frm.submit();
}
// 오픈 팝업 open_popp
function fn_openPopup(){
  
  // var sUrl = "/install/popUp_20100412.jsp";
//  var sUrl = "/popUp/pop_20101208.html";
    var sUrl = "/popUp/pop_20150605.html";
  
  if(fn_getCookie()){
    window.open(sUrl, "openPopup", "toolbar=no,status=yes,scrollbars=no,width=430,height=380");
  }
}

// 팝업 오픈 설정 정보
function fn_getCookie() {
  var rtn = "";
  var search = "mlsmessage=";
  if (document.cookie.length>0){
    offset = document.cookie.indexOf(search);
    if (offset != -1){
      offset += search.length;
      end = document.cookie.indexOf(";", offset);
      if (end == -1)
        end = document.cookie.length;
      rtn = unescape(document.cookie.substring(offset, end));
    }
  }

  if(rtn == "done1") {
    return false;
  }
  
  return true;
}
  function onload(){
  
  }
//-->

function open_win(){ 
   // window.open('/common/popUp/pop_20170317.html','popup', 'width=100, height=100,left=0,top=0,toolbar=no, location=no, directories=no, status=no, menubar=no, resizable=no, scrollbars=no, copyhistory=no'); 
var sUrl = "/popUp/pop_20170317.html";
  
  if(fn_getCookie()){
    window.open(sUrl, "openPopup", "location=no, directories=no,status=no,resizable=no,status=no,toolbar=no,menubar=no,width=534,height=735");
  }
 }

//팝업 오픈 설정 정보
function fn_getCookie() {
  var rtn = "";
  var search = "mlsmessage=";
  if (document.cookie.length>0){
    offset = document.cookie.indexOf(search);
    if (offset != -1){
      offset += search.length;
      end = document.cookie.indexOf(";", offset);
      if (end == -1)
        end = document.cookie.length;
      rtn = unescape(document.cookie.substring(offset, end));
    }
  }

  if(rtn == "done1") {
    return false;
  }
  
  return true;
}

$(function(){
  
  init();
  //open_win();
})

function init(){
  
  for(var i=1 ; i<4; i++)
  {
    $('#href'+i).css('cursor', 'pointer');
    $('#href'+i).click(hrefClick);  
    $('#href'+i+" p").click(hrefClick2);  
  }
}

function hrefClick(event){
  //console.log(event.target.id)
  var id = event.target.id;
  if(id == 'href1'){
    location.href = "/rghtPrps/rghtSrch.do?DIVS=M";
  }else if(id == 'href2'){
    location.href = "%2FinmtPrps%2FinmtPrps.do%3FmNum%3D4%26sNum%3D0%26leftsub%3D0%26srchDIVS%3D1";  
  }else if(id == 'href3'){
    /* location.href = "/srchList.do?menuFlag=N"; */
    location.href = "%2FstatBord%2FstatBo01List.do%3FbordCd%3D1%26divsCd%3D5";                      
  }
}

function hrefClick2(event){
  //console.log("hrefClick2")
  //console.log(event.target.parentNode.id)
  var id = event.target.parentNode.id;
  if(id == 'href1'){
    location.href = "/rghtPrps/rghtSrch.do?DIVS=M";
  }else if(id == 'href2'){
    location.href = "%2FinmtPrps%2FinmtPrps.do%3FmNum%3D4%26sNum%3D0%26leftsub%3D0%26srchDIVS%3D1"; 
  }else if(id == 'href3'){
    /* location.href = "/srchList.do?menuFlag=N"; */
    location.href = "%2FstatBord%2FstatBo01List.do%3FbordCd%3D1%26divsCd%3D5";   
  }
  
}

/* $(function(){
  
  init();
  var sUrl = "/popUp/pop_20200901.html";
  window.open(sUrl, "openPopup", "location=no, directories=no, resizable=no, status=no, toolbar=no, menubar=no, width=500, height=430, left=300, top=300");
}) */

function messageBtn1(){
  document.getElementById('message_chile_menu2').style.display='none';
  document.getElementById('message_chile_menu3').style.display='none';
  document.getElementById('message_chile_menu4').style.display='none';
  document.getElementById('message_chile_menu5').style.display='none';
  document.getElementById('message_chile_menu1').style.display='block';
  
  document.getElementById('messageButton2').setAttribute('class','');
  document.getElementById('messageButton3').setAttribute('class','');
  document.getElementById('messageButton4').setAttribute('class','');
  document.getElementById('messageButton5').setAttribute('class','');
  document.getElementById('messageButton1').setAttribute('class','on');
}

function messageBtn2(){
  document.getElementById('message_chile_menu1').style.display='none';
  document.getElementById('message_chile_menu3').style.display='none';
  document.getElementById('message_chile_menu4').style.display='none';
  document.getElementById('message_chile_menu5').style.display='none';
  document.getElementById('message_chile_menu2').style.display='block';
  
  document.getElementById('messageButton1').setAttribute('class','');
  document.getElementById('messageButton3').setAttribute('class','');
  document.getElementById('messageButton4').setAttribute('class','');
  document.getElementById('messageButton5').setAttribute('class','');
  document.getElementById('messageButton2').setAttribute('class','on');
}

function messageBtn3(){
  document.getElementById('message_chile_menu1').style.display='none';
  document.getElementById('message_chile_menu2').style.display='none';
  document.getElementById('message_chile_menu4').style.display='none';
  document.getElementById('message_chile_menu5').style.display='none';
  document.getElementById('message_chile_menu3').style.display='block';
  
  document.getElementById('messageButton1').setAttribute('class','');
  document.getElementById('messageButton2').setAttribute('class','');
  document.getElementById('messageButton4').setAttribute('class','');
  document.getElementById('messageButton5').setAttribute('class','');
  document.getElementById('messageButton3').setAttribute('class','on');
}

function messageBtn4(){
  document.getElementById('message_chile_menu1').style.display='none';
  document.getElementById('message_chile_menu2').style.display='none';
  document.getElementById('message_chile_menu3').style.display='none';
  document.getElementById('message_chile_menu5').style.display='none';
  document.getElementById('message_chile_menu4').style.display='block';
  
  document.getElementById('messageButton1').setAttribute('class','');
  document.getElementById('messageButton2').setAttribute('class','');
  document.getElementById('messageButton3').setAttribute('class','');
  document.getElementById('messageButton5').setAttribute('class','');
  document.getElementById('messageButton4').setAttribute('class','on');
}

function messageBtn5(){
  document.getElementById('message_chile_menu1').style.display='none';
  document.getElementById('message_chile_menu2').style.display='none';
  document.getElementById('message_chile_menu3').style.display='none';
  document.getElementById('message_chile_menu4').style.display='none';
  document.getElementById('message_chile_menu5').style.display='block';
  
  document.getElementById('messageButton1').setAttribute('class','');
  document.getElementById('messageButton2').setAttribute('class','');
  document.getElementById('messageButton3').setAttribute('class','');
  document.getElementById('messageButton4').setAttribute('class','');
  document.getElementById('messageButton5').setAttribute('class','on');
}

function messageBtn_2_1(){
  document.getElementById('message_child_menu_2_2').style.display='none';
  document.getElementById('message_child_menu_2_1').style.display='block';
  
  document.getElementById('messageButton2_2').setAttribute('class','');
  document.getElementById('messageButton2_1').setAttribute('class','on');
}

function messageBtn_2_2(){
  document.getElementById('message_child_menu_2_1').style.display='none';
  document.getElementById('message_child_menu_2_2').style.display='block';
  
  document.getElementById('messageButton2_1').setAttribute('class','');
  document.getElementById('messageButton2_2').setAttribute('class','on');
}
</script>
</head>

<body>
<!-- 전체를 감싸는 DIVISION -->
<!-- <div id="wrap"> -->
<!-- HEADER str-->
<!-- <script type="text/javascript" language="javascript" src="https://www.findcopyright.or.kr/js/makePCookie.js"></script> 
<script type="text/javascript" language="javascript" src="https://www.findcopyright.or.kr/js/menuhover.js"></script>
<script type="text/javascript" language="javascript" src="https://www.findcopyright.or.kr/js/main/flexslider.js"></script>
<script type="text/javascript" language="javascript" src="https://www.findcopyright.or.kr/js/main/js.js"></script> -->

<!-- 추가된 링크 -->


<script type="text/javascript">
  /* $(function(){
    $("#sendMail").click(sendMailClick);
  })
  
  function sendMailClick(event){
    //alert("sendMailClick");
    location.href = "/main/main.do?method=sendMailTest"
  } */
  
  $(function(){
    //for(var i = 0 ; i < 10 ; i ++){
    for(var i = 0 ; i < 10 ; i ++){
      //console.log($('#anucItem'+i).html())
      if($('#anucItem'+i).html()!=undefined){
        $('#anucItem'+i).html(wonAttach($('#anucItem'+i).html()));
      }
    }
  })  
  
  $(function(){
    //for(var i = 0 ; i < 10 ; i ++){
    for(var i = 0 ; i < 10 ; i ++){
      //console.log($('#anucItem'+i).html())
      if($('#anucItem'+i+'_2').html()!=undefined){
        $('#anucItem'+i+'_2').html(wonAttach($('#anucItem'+i+'_2').html()));
      }
    }
  })  

  function wonAttach(text){
    //console.log(text)
      if(text.substr(text.indexOf(-1))!="원"){
        text+="원";
      }
      return text;
  }
</script>
<!-- <input id="sendMail" type="button" value="메일보내기" /> -->
<!-- <script type="text/javascript">initNavigation(0);</script> -->
<!--/* bg_2c65aa -->
<div id="contents" style="margin-top:0px;">
<div id="banners_bg">
  <div id="banners">
    <div class="banners_lf" id="contentBody">
      <div class="blf_lf">
        <ul>
          <li id="href1">
            <!-- <a href="/srchList.do">
            </a> -->
            <p>
              <a href="/rghtPrps/rghtSrch.do?DIVS=M">
              <strong>권리자 찾기<!-- <span>이용자</span> --></strong>
                <span style="color:black">권리자 검색 서비스로<br />편리하게 권리자를 <br />확인하세요,</span>
              </a>
            </p>
          </li>
          <li id="href2">
            <!-- <a href="/inmtPrps/inmtPrps.do?mNum=4&sNum=0&leftsub=0&srchDIVS=1"> -->
            <!-- <a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">
            </a> -->
            <p style="position: relative;" >
              <!-- <a href="/inmtPrps/inmtPrps.do?mNum=4&sNum=0&leftsub=0&srchDIVS=1"> -->
              <a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">
                <strong>미분배 보상금 대상 저작권 확인</strong>
              미분배 보상금 저작물을<br />확인하여 정당한 보상을<br />받으세요
              </a>
            </p>
          </li>
          <li id="href3">
            <!-- <a href="/statBord/statBo01List.do?bordCd=1&divsCd=5"><strong>법정허락 승인 신청</strong></a> -->
            <!-- <a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">
            </a> -->
            <!-- <p><a href="/statBord/statBo01List.do?bordCd=1&divsCd=5"> -->
            <p>
              <!-- <a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5"> -->
              <a href="/mlsInfo/liceSrchInfo07.jsp">
                <strong>법정허락 승인 신청</strong>
                법정허락 승인 신청을 통해<br/>합법적으로 저작물을<br/>이용하세요.
              </a>
            </p>
          </li>
        </ul>
      </div>
      <div class="blf_rt">
        <div class="flexslider">
          <ul class="slides_n">
            <li><a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2017/new/main_1_1.png" alt="권리자 찾기 - 관리자 검색 서비스로 편리하게 권리자를 확인하세요" /></a></li>
            <!-- <li><a href="/inmtPrps/inmtPrps.do?mNum=4&sNum=0&leftsub=0&srchDIVS=1"><img src="/images/2017/new/main_1_3.png" alt="미분배 보상금 대상 저작물 찾기 창작자 창작자가 저작권에 대한 정당한 보상을 받을 수 있도록 미분배 보상금 대상 저작물목록을 제공하는 서비스 입니다." /></a></li> -->
            <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1"><img src="/images/2017/new/main_1_3.png" alt="미분배 보상금 대상 저작물 확인 - 미분배 보상금 대상 저작물 확인하여 정당한 보상을 받으세요" /></a></li>
            <li><a href="/srchList.do"><img src="/images/2017/new/main_1_2.png" alt="법정허락 승인 신청 - 법정허락 승인 신청을 통해 합법적으로 저작물을 이용하세요" /></a></li>
          </ul>
        </div>
        <script type="text/javascript">
          $('.slides_n').bxSlider({
            auto: true,
            autoStart: true,
            speed: 700,
            pager: true,
            mode: 'fade',
            autoHover: false,
            controls: false,
            autoControls: true,
            touchEnabled:(navigator.maxTouchPoints>0),
          });
        </script>
      </div>
    </div>
    <div class="banners_rt">
      <div id="slider002">
        <ul class="slides_r">
          <li><img src="/images/2017/new/main_2_1.png" alt="올바른 저작권 이용이 만드는 행복한 문화생활 만들기! 권리자 찾기가 그 시작입니다!" /></li>
          <li><img src="/images/2017/new/main_2_2.png" alt="권리자 찾기 서비스로 꼭꼭 숨어있는 실연,음반,미분배 보상금 정보를 확인해 당신의 권리를 찾으세요. 숨은 미분배 보상금 찾기" /></li>
          <li><img src="/images/2017/new/main_2_3.png" alt="저작권자를 알수없는 저작물의 합법적인 이용! 법정허락제도가 도와드립니다." /></li>
        </ul>
      </div>
      <script type="text/javascript">
        $('.slides_r').bxSlider({
          auto: true,
          autoStart: true,
          speed: 700,
          pager: true,
          mode: 'fade',
          autoHover: false,
          controls: false,
          autoControls: true,
        });
      </script>
    </div>
    <p class="clear">
    </p>
    <div class="">
    </div>
  </div>
</div>
<!-- //main Link -->
<!-- main Data -->

  <!-- <div class="con_lf"> -->
  <div style="margin-top:25px;">
    <div class="message_bm">
      <ul class="message1">
        <li>
          <a href="javascript:;" class="on" onclick="javascript:messageBtn1(); return false" id="messageButton1"><b>저작권자 조회 공고</b></a>
          <div class="message_child_menu" id="message_chile_menu1">
            <ul>
                <li>
                  <table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;width:100%;" summary="저작권자 조회 표로 장르,제목, 공고일로 구성되어 있습니다">
                <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
                <caption>저작권자 조회</caption>
                <colgroup>
                  <col width="15%"/>
                  <col width="70%"/>
                  <col width="15%"/>
                </colgroup>
                <thead>
                  <tr>
                    <th scope="col" style="text-align: center;">
                      장르
                    </th>
                    <th scope="col" style="text-align: center;">
                      제목
                    </th>
                    <th scope="col" style="text-align: center;">
                      공고일
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <c:if test="${bord01Size == 0}">
                    <tr>
                      <td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
                    </tr>
                  </c:if>
                      <c:if test="${bord01Size > 0 }">
                        <c:forEach var="anucBord01" items="${anucBord01}">
                      <tr>
                            <td class="ce">${fn:substring(anucBord01.genreCdName, 0, 2)}</td>
                            <td class="content" align="left" style="text-align: left; margin-left: 10px;">
                              <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord01.bordSeqn}&amp;bordCd=${anucBord01.bordCd}&amp;divsCd=${anucBord01.divsCd}">
                            ${fn:replace(anucBord01.tite, '<', '&lt;')}
                          </a>
                            </td>
                          <td class="ce">${anucBord01.openDttm}</td>
                          </tr>
                    </c:forEach>
                  </c:if>
                </tbody>
                </table>
                <div class="message_tp_rt" style="margin-top: 10px;">
                  <a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5" title="저작권자 조회 공고 더 보기">더 보기 <strong>+</strong></a>
                </div>
              </li>
            </ul>
            <p class="clear">
            </p>
          </div>
        </li>
    <%--    <li>
        <a href="#none" class=""><b>상당한 노력 공고</b></a>
        <div class="message_child_menu disnone">
          <ul>
            <li>
            <table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;width:100%" summary="상당한 노력 공고 조회 목록입니다.">
            <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
            <caption>상당한 노력 공고</caption>
            <colgroup>
            <col width="15%"/>
            <col width="70%"/>
            <col width="15%"/>
            </colgroup>
            <thead>
            <tr>
              <th scope="col" style="text-align: center;">
                장르
              </th>
              <th scope="col" style="text-align: center;">
                제목
              </th>
              <th scope="col" style="text-align: center;">
                공고일
              </th>
            </tr>
            </thead>
            <tbody>
            <c:if test="${bord06Size == 0}">
                <tr>
                  <td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
                </tr>
            </c:if>
                <c:if test="${bord06Size > 0 }">
                <c:forEach var="anucBord06" items="${anucBord06}">
            
            <tr>
                  <td class="ce">${fn:substring(anucBord06.genreCdName, 0, 2)}</td>
                  <td align="left" style="text-align: left; margin-left: 10px;">
                  <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord06.bordSeqn}&amp;bordCd=${anucBord06.bordCd}&amp;divsCd=${anucBord06.divsCd}">
              ${fn:replace(anucBord06.tite, '<', '&lt;')}</a>
              </td>
                <td class="ce">${anucBord06.openDttm}</td>
                 </tr>
            </c:forEach>
            </c:if>
            </tbody>
            </table>
            <div class="message_tp_rt" style="margin-top: 10px;">
              <a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=4">더 보기 <strong>+</strong></a>
            </div>
            </li>
          </ul>
          <p class="clear">
          </p>
        </div>
        </li> --%>
        <li>
          <a href="javascript:;" onclick="javascript:messageBtn2(); return false" id="messageButton2" class=""><b>법정허락 승인 신청</b></a>
          <div class="message_child_menu disnone" id="message_chile_menu2">
            <ul>
              <li>
                <table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;width:100%" summary="법정허락 승인 신청 표로 구분, 제목, 공고일로 구성되어 있습니다.">
                <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
                  <caption>법정허락 승인 신청</caption>
                  <colgroup>
                    <col width="15%"/>
                    <col width="70%"/>
                    <col width="15%"/>
                  </colgroup>
                  <thead>
                    <tr>
                      <th scope="col" style="text-align: center;">
                        구분
                      </th>
                      <th scope="col" style="text-align: center;">
                        제목
                      </th>
                      <th scope="col" style="text-align: center;">
                        공고일
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <c:if test="${bord03Size == 0}">
                      <tr>
                        <td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
                      </tr>
                    </c:if>
                        <c:if test="${bord03Size > 0 }">
                        <c:forEach var="anucBord03" items="${anucBord03}">
                        <tr>
                              <td class="ce">${anucBord03.divsCdName}</td>
                              <td align="left" style="text-align: left; margin-left: 10px;">
                                <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord03.bordSeqn}&amp;bordCd=${anucBord03.bordCd}&amp;divsCd=${anucBord03.divsCd}">
                              ${fn:replace(anucBord03.tite, '<', '&lt;')}
                            </a>
                              </td>
                            <td class="ce">${anucBord03.openDttm}</td>
                            </tr>
                      </c:forEach>
                    </c:if>
                  </tbody>
                </table>
                <div class="message_tp_rt" style="margin-top: 10px;">
                  <a href="/statBord/statBo03List.do?bordCd=3" title="법정허락 승인신청 더보기">더 보기 <strong>+</strong></a>
                </div>
              </li>
            </ul>
            <p class="clear">
            </p>
          </div>
        </li>
        <li>
          <a href="javascript:;" onclick="javascript:messageBtn3(); return false" class="" id="messageButton3"><b>법정허락 승인공고</b></a>
          <div class="message_child_menu disnone" id="message_chile_menu3">
            <ul>
              <li>
                <table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;width:100%" summary="법정허락 승인공고 표로 구분, 제목, 공고일로 구성되어 있습니다.">
                <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
                  <caption>법정허락 승인공고</caption>
                  <colgroup>
                    <col width="15%"/>
                    <col width="70%"/>
                    <col width="15%"/>
                  </colgroup>
                  <thead>
                    <tr>
                      <th scope="col" style="text-align: center;">
                        구분
                      </th>
                      <th scope="col" style="text-align: center;">
                        제목
                      </th>
                      <th scope="col" style="text-align: center;">
                        공고일
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <c:if test="${bord04Size == 0}">
                      <tr>
                        <td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
                      </tr>
                    </c:if>
                        <c:if test="${bord04Size > 0 }">
                          <c:forEach var="anucBord04" items="${anucBord04}">
                        <tr>
                              <td class="ce">${anucBord04.divsCdName}</td>
                              <td align="left" style="text-align: left; margin-left: 10px;">
                                <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord04.bordSeqn}&amp;bordCd=${anucBord04.bordCd}&amp;divsCd=${anucBord04.divsCd}">
                              ${fn:replace(anucBord04.tite, '<', '&lt;')}
                            </a>
                              </td>
                            <td class="ce">${anucBord04.openDttm}</td>
                            </tr>
                      </c:forEach>
                    </c:if>
                  </tbody>
                </table>
                <div class="message_tp_rt" style="margin-top: 10px;">
                  <a href="/statBord/statBo04List.do?bordCd=4" title="법정허락 승인공고 더보기">더 보기 <strong>+</strong></a>
                </div>
              </li>
            </ul>
            <p class="clear">
            </p>
          </div>
        </li>
        <li>
          <a href="javascript:;" onclick="javascript:messageBtn4(); return false" class="" id="messageButton4"><b>보상금 공탁 공고</b></a>
          <div class="message_child_menu disnone" id="message_chile_menu4">
            <ul>
              <li>
                <table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;width:100%" summary="보상금 공탁 공고 표로 구분, 제목, 공탁금액, 공고일로 구성되어 있습니다.">
                <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
                  <caption>보상금 공탁 공고</caption>
                  <colgroup>
                    <col width="15%" />
                    <col width="55%" />
                    <col width="15%" />
                    <col width="15%" />
                  </colgroup>
                  <thead>
                    <tr>
                      <th scope="col" style="text-align: center;">
                        구분
                      </th>
                      <th scope="col" style="text-align: center;">
                        제목
                      </th>
                      <th scope="col" style="text-align: center;">
                        공탁금액
                      </th>
                      <th scope="col" style="text-align: center;">
                        공고일
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                  <c:if test="${bord05Size == 0}">
                    <tr>
                      <td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
                    </tr>
                  </c:if>
                      <c:if test="${bord05Size > 0 }">
                        <c:forEach var="anucBord05" items="${anucBord05}" varStatus="status">
                      <tr>
                            <td class="ce">${anucBord05.divsCdName}</td>
                            <td align="left" style="text-align: left; margin-left: 10px;">
                              <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord05.bordSeqn}&amp;bordCd=${anucBord05.bordCd}&amp;divsCd=${anucBord05.divsCd}">
                            ${fn:replace(anucBord05.tite, '<', '&lt;')}
                          </a>
                            </td>
                            <td id="anucItem${status.index}" style="text-align: right;" >${anucBord05.anucItem4}</td>
                          <td class="ce">${anucBord05.openDttm}</td>
                          </tr>
                    </c:forEach>
                  </c:if>
                  </tbody>
                </table>
                <div class="message_tp_rt" style="margin-top: 10px;">
                  <a href="/statBord/statBo05List.do?bordCd=5" title="보상금 공탁 공고 더보기">더 보기 <strong>+</strong> </a>
                </div>
              </li>
            </ul>
            <p class="clear">
            </p>
          </div>
        </li>
        <li>
          <a href="javascript:;" onclick="javascript:messageBtn5(); return false" class="" id="messageButton5"><b>보상금 지급사실 공고</b></a>
          <div class="message_child_menu disnone" id="message_chile_menu5">
            <ul>
              <li>
                <table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;width:100%" summary="보상금 공탁 공고 표로 구분, 제목, 공탁금액, 공고일로 구성되어 있습니다.">
                <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
                  <caption>보상금 지급사실 공고</caption>
                  <colgroup>
                    <col width="15%" />
                    <col width="15%" />
                    <col width="55%" />
                    <col width="15%" />
                  </colgroup>
                  <thead>
                    <tr>
                      <th scope="col" style="text-align: center;">
                        공고일
                      </th>
                      <th scope="col" style="text-align: center;">
                        종류
                      </th>
                      <th scope="col" style="text-align: center;">
                        제호
                      </th>
                      <th scope="col" style="text-align: center;">
                        보상금 금액
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                  <c:if test="${bord08Size == 0}">
                    <tr>
                      <td class="ce" colspan="4">등록된 게시물이 없습니다.</td>
                    </tr>
                  </c:if>
                      <c:if test="${bord08Size > 0 }">
                        <c:forEach var="anucBord08" items="${anucBord08}" varStatus="status">
                      <tr>
                            <td class="ce">${anucBord08.openDttm}</td>
                            <td style="text-align:center;">${anucBord08.anucItem4}</td>
                            <td align="left" style="text-align: left; margin-left: 10px;">
                              <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord08.bordSeqn}&amp;bordCd=${anucBord08.bordCd}&amp;divsCd=${anucBord08.divsCd}">
                            ${fn:replace(anucBord08.tite, '<', '&lt;')}
                          </a>
                            </td>
                          <td id="anucItem${status.index}_2" style="text-align: right;" >${anucBord08.anucItem5}</td>
                          </tr>
                    </c:forEach>
                  </c:if>
                  </tbody>
                </table>
                <div class="message_tp_rt" style="margin-top: 10px;">
                  <a href="/statBord/statBo08List.do?bordCd=8" title="보상금 지급사실 공고">더 보기 <strong>+</strong> </a>
                </div>
              </li>
            </ul>
            <p class="clear">
            </p>
          </div>
        </li>
      </ul>
      <p class="clear">
      </p>
    </div>
  </div>
  <div class="con_rt_2">
    <form name="form1" method="post" action="">
      <input type="hidden" name="bordSeqn"/>
      <input type="hidden" name="menuSeqn"/>
      <input type="hidden" name="threaded"/>
      <input type="hidden" name="page_no"/>
    </form>
    <div class="message_bm_2">
      <ul class="message1_2">
        <li>
          <a href="javascript:;" class="on" onclick="javascript:messageBtn_2_1(); return false;" id="messageButton2_1"><b>공지사항</b></a>
          <div class="message_child_menu_2" id="message_child_menu_2_1">
            <ul>
              <li>
              <table class="ntbd sub_tab3_2" cellspacing="0" cellpadding="0" style="width:100%">
              <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
                <colgroup>
                  <col width="75%"/>
                  <col width="25%"/>
                </colgroup>
                <tbody>
                  <c:if test="${notiListSize == 0}">
                    <tr>
                      <td class="ce" colspan="2">등록된 게시물이 없습니다.</td>
                    </tr>
                  </c:if>
                      <c:forEach var="notiList" items="${notiList}">
                     <tr>
                          <td align="left" style="text-align: left; margin-left: 10px;">
                            <a href="javascript:boardDetail('${notiList.bordSeqn}','${notiList.menuSeqn}','${notiList.threaded}')" >
                          ${fn:replace(notiList.tite, '<', '&lt;')}
                        </a>
                      </td>
                        <td class="ce" style="border-right: 1px solid #dddddd;">${notiList.insertDate}</td>
                         </tr>
                  </c:forEach>
                </tbody>
              </table>
              <div class="message_tp_rt">
                <a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1" title="공지사항 조회 더보기">+</a>
              </div>
              </li>
            </ul>
            <p class="clear"></p>
          </div>
        </li>
        <li>
          <a href="javascript:;" onclick="javascript:messageBtn_2_2(); return false;" class="" id="messageButton2_2"><b>자주묻는 질문</b></a>
        <div class="message_child_menu_2 disnone" id="message_child_menu_2_2">
          <ul>
            <li>
            <table class="ntbd sub_tab3_2" cellspacing="0" cellpadding="0" style="width:100%" >
            <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
              <colgroup>
                <col width="75%"/>
                <col width="25%"/>
              </colgroup>
              <tbody>
                <c:if test="${QnABordSize == 0}">
                  <tr>
                    <td class="ce" colspan="2">등록된 게시물이 없습니다.</td>
                  </tr>
                </c:if>
                    <c:forEach var="QnABord" items="${QnABord}">
                  <tr>
                        <td align="left" style="text-align: left; margin-left: 10px;">
                          <a href="javascript:boardDetail('${QnABord.bordSeqn}','${QnABord.menuSeqn}','${QnABord.threaded}')" >
                        ${fn:replace(QnABord.tite, '<', '&lt;')}
                      </a>
                    </td>
                      <td class="ce" style="border-right: 1px solid #dddddd;">${QnABord.rgstDttm}</td>
                      </tr>                 
                </c:forEach>
              </tbody>
            </table>
            <div class="message_tp_rt">
              <a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1" title="자주묻는 질문 더보기">+</a>
            </div>
          </li>
          </ul>
          <p class="clear"></p>
        </div>
        </li>
      </ul>
      <p class="clear">
      </p>
    </div>
    <div class="txt_custom" style="top:300px">
      <p class="tit">상담전화</p>
      <p class="txt1">055-792-0125</p>
      <p class="txt2">평일 09:00~18:00</p>
      <p class="txt3">토.일요일 휴무</p>
      <p class="txt3">공휴일 휴무</p>
      <p class="tit">기술지원 문의</p>
      <p class="txt1">02-3465-7660</p>
      <p class="txt2">평일 09:30~18:30 </p>
      <p class="txt3">토요일 / 일요일 / 공휴일 휴무</p>
    </div>
  </div>
  <p class="clear">
  </p>
</div>
<!-- //main Data -->

<jsp:include page="/include/2017/footer.jsp" />

<!-- </div> -->
<!-- //전체를 감싸는 DIVISION -->
</body>
</html>