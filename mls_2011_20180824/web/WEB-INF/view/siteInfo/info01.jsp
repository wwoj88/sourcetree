<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작권찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp" flush="true">
		<jsp:param name="mNum" value="6" />
	</jsp:include>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','122');
			</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="siteInfoTlt">저작권찾기소개</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>저작권찾기소개</li>
					<li class="on">저작권찾기소개</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>저작권찾기소개</h2>
			<div id="contentsDiv">
				<ul class="tabMenu">
					<li class="active">저작권찾기란?</li>
					<li><a href="/main/main.do?method=goSiteInfo2">저작권찾기의 필요성 </a></li>
					<li><a href="/main/main.do?method=goSiteInfo3">참여방법</a></li>
				</ul>
				<p class="clear" style="padding:25px 0 200px 0;"><img src="/images/sub/info01_01img.gif" alt="작사, 작곡가, 편곡자, 가수, 연주자, 음반제작, 작가(저자), 번역 등 권리자들이 인터넷 상에서 직접 자신이 참여한 저작물에 대한 권리를 확인하고 권리자로서의 정당한 보상을 받도록 
하는 사이트 입니다." /></p>
			</div>
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
