<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%@ page contentType="text/html;charset=euc-kr" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>단체 및 기관명 검색 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script type="text/JavaScript">
function fncSelectCompany(CONAME,CEONAME,TRUSTTEL,CA_ID){
	CONAME = strRep(CONAME)
	$("#COMM_NAME",opener.document).val(CONAME);
	$("#TRST_CEOX_NAME",opener.document).val(CEONAME);
	$("#TRST_TEXL_NUMB",opener.document).val(TRUSTTEL);
	$("#TRST_ORGN_CODE",opener.document).val(CA_ID);
	self.close();
}

$(function(){
	init();
})

function init(){
		
		var tableLen = $('#fdcrAd75List1 tr').length;
		for(var i = 1 ; i < tableLen ; i++){
			var conameHtml = $('#coname'+i).html();
			conameHtml = strRep(conameHtml);
			$('#coname'+i).html(conameHtml);
		}
	}

	function strRep(str)
	{
		if(str.indexOf('&amp;#40;')!=-1||str.indexOf('&#40;')!=-1){
			console.log("strRep call ")
			str = str.replace('&amp;#40;', '(');
			str = str.replace('&amp;#41;', ')');
			str = str.replace('&#40;', '(');
			str = str.replace('&#41;', ')');
		}
		return str;
	}

</script>
</head>
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>단체 및 기관명 찾기</h1>
		</div>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<div class="section">
				<span class="topLine"></span>
				<form name="form1" method="post" class="relative" action="#">
					<input type="hidden" id="gubun" name="gubun" value="${param.GUBUN}" class="w30">
				<table id="fdcrAd75List1" class="table table-bordered table-hover text-center table-list " summary="단체 및 기관명 찾기">
					<caption>단체 및 기관명 찾기</caption>
					<thead>
						<tr>
							<th style="width: 10%">순번</th>
							<th>기관/단체명</th>
							<th style="width: 10%">대표자</th>
							<th style="width: 15%">신고번호</th>
							<th style="width: 17%">사업자등록번호</th>
							<th style="width: 10%">선택</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty list}">
								<tr>
									<td colspan="10">검색된 결과가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="info" items="${list}" varStatus="listStatus">
									<tr>
										<td><c:out value="${listStatus.count}" /></td>
										<td id="coname${listStatus.count}"><c:out value="${info.CONAME}" /></td>
										<td><c:out value="${info.CEONAME}" /></td>
										<td><c:out value="${info.APPNO}" /></td>
										<td><c:out value="${info.CONO}" /></td>
										<td><div class="btn-group"><button type="submit" class="btn btn-block btn-primary btn-xs" onclick="fncSelectCompany('<c:out value="${info.CONAME}" />','<c:out value="${info.CEONAME}" />','<c:out value="${info.TRUSTTEL}" />','<c:out value="${info.CA_ID}" />');return false;">선택</button></div></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				</form>
			</div>
			
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="javascript:self.close()" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="닫기" /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>