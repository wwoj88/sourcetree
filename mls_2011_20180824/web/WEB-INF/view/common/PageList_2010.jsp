<%@ page contentType="text/html;charset=euc-kr"%>

<%
    long totalItemCount   = nullToLong(request.getParameter("totalItemCount"));     // 전체 목록 수 *
    long nowPage          = nullToLong(request.getParameter("nowPage"));            // 현재 페이지 번호 *
    int listScale         = nullToInt(request.getParameter("listScale"), 10);        // 한 페이지당 출력할 목록 수
    int pageScale         = nullToInt(request.getParameter("pageScale"), 10);       // 한 페이지당 출력할 이동 링크 수

    String functionName   = nullTo(request.getParameter("functionName"));           // 페이지 이동 자바스크립트 평션 명 *
    String extend         = nullTo(request.getParameter("extend"), "no");           // 맨처음/나중 버튼 활성화 여부 ('yes' : 사용, 'no' : 사용안함)
    String flag           = nullTo(request.getParameter("flag"), "back");           // 백오피스/프론트 구분 ('fp' : 프론트, 'bo' : 백오피스)
	
   // extend = "yes";
    
    if(totalItemCount > 0 && nowPage > 0 && !functionName.equals(""))
    {
        long totalPage              = ((totalItemCount-1)  / listScale) + 1;
        long totalPageBlock         = ((totalPage-1) / pageScale ) + 1;
        long nowPageBlock           = ((nowPage -1) / pageScale ) + 1;

        long startKey = (nowPageBlock-1) * pageScale + 1 ;
        long stopKey = startKey + pageScale > totalPage ? totalPage : startKey + pageScale - 1;

        if(flag != null && !flag.equals(""))
        {
            // JoinChips 템플릿 구성/////////////////////////////////////////////////////////

            String prevLink         = "";
            String nextLink         = "";
            String startLink        = "";
            String endLink          = "";

            String prevLinkNone     = "";
            String nextLinkNone     = "";
            String startLinkNone    = "";
            String endLinkNone      = "";


            if(flag.equals("M01_FRONT"))
            {
            	/*
                prevLink        = "<img src='/images/board/last_btn.gif' border='0' alt=''>";
                nextLink        = "<img src='/images/board/next_btn.gif' border='0' alt=''>";
                startLink       = "<img src='/images/board/first_btn.gif' border='0' alt=''>";
                endLink         = "<img src='/images/board/last_btn.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/images/board/first_btn.gif' border='1' alt=''>";
                nextLinkNone    = "<img src='/images/board/last_btn.gif' border='1' alt=''>";
                startLinkNone   = "<img src='/images/board/next_btn.gif' border='1' alt=''>";
                endLinkNone     = "<img src='/images/board/last_btn.gif' border='1' alt=''>";
                */
                
                startLink = "<span class=\"first\">&nbsp;</span>";
                prevLink = "<span class=\"prev\">&nbsp;</span>";
                endLink = "<span class=\"last\">&nbsp;</span>";
                nextLink = "<span class=\"next\">&nbsp;</span>";
                
                startLinkNone = "<span class=\"first\">&nbsp;</span>";
                prevLinkNone = "<span class=\"prev\">&nbsp;</span>";
                endLinkNone = "<span class=\"last\">&nbsp;</span>";
                nextLinkNone = "<span class=\"next\">&nbsp;</span>";
                
            }
            else
            {
                prevLink    = "◀";
                nextLink    = "▶";
                startLink   = "◀◀";		
                endLink     = "▶▶";	

                prevLinkNone    = "◁";
                nextLinkNone    = "▷";
                startLinkNone   = "◁◁";
                endLinkNone     = "▷▷";
            }
%>

<%          if (startKey != 1)
            {
                if(extend.equalsIgnoreCase("yes"))
                {   %>
        <a  onclick="<%=functionName%>(1)" class="direction bgNone" style="cursor:pointer"><%=startLink%></a>
<%
				}
%>
        <a  onclick="<%=functionName%>(<%=(startKey-1)%>)" class="direction bgNone" style="cursor:pointer"><%=prevLink%></a>
<%
			}
            else
            {
                if(extend.equalsIgnoreCase("yes"))
                {   %>

        <span class="direction bgNone"><%=startLinkNone%></span>

<%              }   %>

        <span class="direction bgNone"><%=prevLinkNone%></span>
<%          }
			%>
<%
            for(long i = startKey; i <= stopKey ; i++)
            {
                if(nowPage == i)
                {   %>

        			<strong><%=i%></strong>

<%              }
                else
                {
%>
        <a  onclick="this.href='javascript:<%=functionName%>(<%=i%>)'" style="cursor:pointer" class="num_o"><%=i%></a>&nbsp;
<%
            	}
            }
%>

<%
            if (stopKey != totalPage)
            {
%>
        <a  onclick="<%=functionName%>(<%=stopKey + 1 %>)" class="direction bgNone" style="cursor:pointer"><%=nextLink%></a>
<%
				if(extend.equalsIgnoreCase("yes"))
	           {
%>
        <a  onclick="<%=functionName%>(<%=totalPage%>)" class="direction bgNone" style="cursor:pointer"><%=endLink%></a>
<%           }
            }
            else
            {
%>
        <span class="direction bgNone"><%=nextLinkNone%></span>
<%
				if(extend.equalsIgnoreCase("yes"))
                {
%>
			        <span class="direction bgNone"><%=endLinkNone%></span>
<%              }
            }   %>

<%      }
    }
    else
        return;
%>

<%! // 필요 Function

    public static boolean isNull(String str)
    {
        if (str == null || str.equals(null) || str.equals("null") || str.equals(""))
            return true;
        else
            return false;
    }

    public static String nullTo(String str)
    {
        if (isNull(str))
            str = "";

        return str;
    }

    public static String nullTo(String str, String chstr)
    {
        if (isNull(str))
            str = chstr;

        return str;
    }

    public static long nullToLong(String str)
    {
        long result = 0;

        try{ result = Long.parseLong(nullTo(str, "0")); }catch(Exception e){ result = 0; }

        return result;
    }

    public static long nullToLong(String str, long value)
    {
        long result = 0;

        try{ result = Long.parseLong(nullTo(str, "" + value)); }catch(Exception e){ result = 0; }

        return result;
    }

    public static long nullToInt(String str)
    {
        long result = 0;

        try{ result = Integer.parseInt(nullTo(str, "0")); }catch(Exception e){ result = 0; }

        return result;
    }

    public static int nullToInt(String str, int value)
    {
        int result = 0;

        try{ result = Integer.parseInt(nullTo(str, "" + value)); }catch(Exception e){ result = 0; }

        return result;
    }
%>
