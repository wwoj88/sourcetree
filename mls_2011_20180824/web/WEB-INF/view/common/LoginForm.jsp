<%@ page language="java" isELIgnored="false" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>MyWeb~~</title>
<link href="/style/base.css" rel="stylesheet" type="text/css" />
<script src="http://code.jquery.com/jquery-1.6.2.js"></script>
<style type="text/css">
	input{
		font-size: 13pt;
		font-weight: bold;
	}
	#submitBtn{
		cursor:pointer;
	}
</style>
<script>
	$(function() {
		init();
		var docWidth = $(document).width();
		var docHeight = $(document).height();
		var loginFormWidth = $('#loginForm').width();
		var loginFormHeight = $('#loginForm').height();
		var marginLeft = (docWidth/2)-(loginFormWidth/2);
		var marginTop = (docHeight/2)-(loginFormHeight/2);
		$('#loginForm').css({'margin-left':marginLeft+"px",'margin-top':(marginTop)+"px"});
		
		<c:if test="${not empty errorMessage}">
		  alert("${errorMessage}");
		  <c:set var="errorMessage" value="" />
		  var frm = document.getElementById("insert_form");
		  frm.method = "post";
		  frm.action = "${ctx}/login/loginForm.do";
		  frm.submit();
		</c:if>
		
		$(window).resize(function(event){
			var docHeight = $(document).height();
			var loginFormHeight = $('#loginForm').height();
			var marginTop = (docHeight/2)-(loginFormHeight/2);
			$('#loginForm').css({'margin-top':(marginTop-50)+"px"});
		});
		
		$('body').keydown(function(event){
			if(event.keyCode == '13'){
				$('#submitBtn').trigger('click');
			}
		});
		
		$('#submitBtn').bind("click",function(){
			var frm = document.getElementById("form");
			if( $('#j_username').val() == null || $('#j_username').val() == "" ) {
				alert("아이디는 필수 입력사항입니다.");
				$('input').eq(0).focus();
				return;
			} else if( $('#j_password').val() == null || $('#j_password').val() == "" ) {
				alert("비밀번호는 필수 입력사항입니다.");
				$('#j_password').focus();
				return;
			} else {
				$("#form").trigger('submit');
			}
			frm.submit();
		});
		
		
		/*
		$('#insert_form').submit(function(event){
			var data = $(this).serialize();
			alert(data);
			$.post('/login/loginProc.do',data);
			event.preventDefault();
		});
		*/
		
		
		function init(){
			$("#j_username").focus();
		}
	});
</script>

</head>
<body>
	<div id="contents_login">
		<div id="loginForm">
			<fieldset class="login" >
				<form name="form" method="post" id="form" action="/j_spring_security_check"  onsubmit="document.form.submit" >
				<input type="hidden" name="targetURL" value="${param.targetURL}" />
				<table id="login_padding" style="margin: auto;margin-top: 40px; margin-bottom: 30px;">
					<tr>
						<td>
							<table>
								<tr>
									<td><img src="/images/login/text_login.jpg" /></td>
								</tr>
							</table>
						</td>
						<td>
							<table id="login_right">
								<tr>
									<td><img src="/images/login/text_id.jpg" /></td>
									<td><input class="login_input" name="j_username"  id="j_username" /></td>
								</tr>
								<tr>
									<td><img src="/images/login/text_pw.jpg" /></td>
									<td><input class="login_input" type="password" autocomplete="off" name="j_password" id="j_password" /></td>
								</tr>
								<tr>
									<td></td>
									<td id="login_img"><img src="/images/login/btn_event_login.jpg" id="submitBtn" /></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</form>
			</fieldset>
		</div>
	</div>
</body>
</html>