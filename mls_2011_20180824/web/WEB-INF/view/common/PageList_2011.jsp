<%@ page contentType="text/html;charset=euc-kr"%>

<%
    long totalItemCount   = nullToLong(request.getParameter("totalItemCount"));     // 전체 목록 수 *
    long nowPage          = nullToLong(request.getParameter("nowPage"));            // 현재 페이지 번호 *
    int listScale         = nullToInt(request.getParameter("listScale"), 10);        // 한 페이지당 출력할 목록 수
    int pageScale         = nullToInt(request.getParameter("pageScale"), 10);       // 한 페이지당 출력할 이동 링크 수

    String functionName   = nullTo(request.getParameter("functionName"));           // 페이지 이동 자바스크립트 평션 명 *
    String extend         = nullTo(request.getParameter("extend"), "no");           // 맨처음/나중 버튼 활성화 여부 ('yes' : 사용, 'no' : 사용안함)
    String flag           = nullTo(request.getParameter("flag"), "back");           // 백오피스/프론트 구분 ('fp' : 프론트, 'bo' : 백오피스)
	
   // extend = "yes";
    
    if(totalItemCount > 0 && nowPage > 0 && !functionName.equals(""))
    {
        long totalPage              = ((totalItemCount-1)  / listScale) + 1;
        long totalPageBlock         = ((totalPage-1) / pageScale ) + 1;
        long nowPageBlock           = ((nowPage -1) / pageScale ) + 1;

        long startKey = (nowPageBlock-1) * pageScale + 1 ;
        long stopKey = startKey + pageScale > totalPage ? totalPage : startKey + pageScale - 1;

        if(flag != null && !flag.equals(""))
        {
            // JoinChips 템플릿 구성/////////////////////////////////////////////////////////

            String prevLink         = "";
            String nextLink         = "";
            String startLink        = "";
            String endLink          = "";

            String prevLinkNone     = "";
            String nextLinkNone     = "";
            String startLinkNone    = "";
            String endLinkNone      = "";
            
            


            if(flag.equals("M01_FRONT"))
            {
            	/*
                prevLink        = "<img src='/images/board/last_btn.gif' border='0' alt=''>";
                nextLink        = "<img src='/images/board/next_btn.gif' border='0' alt=''>";
                startLink       = "<img src='/images/board/first_btn.gif' border='0' alt=''>";
                endLink         = "<img src='/images/board/last_btn.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/images/board/first_btn.gif' border='1' alt=''>";
                nextLinkNone    = "<img src='/images/board/last_btn.gif' border='1' alt=''>";
                startLinkNone   = "<img src='/images/board/next_btn.gif' border='1' alt=''>";
                endLinkNone     = "<img src='/images/board/last_btn.gif' border='1' alt=''>";
                */
                
                startLink = "<span class=\"first mr10\" style=\"text-indent:-9999px\">첫 페이지로 이동&nbsp;</span>";
                prevLink = "<span class=\"prev mr10\" style=\"text-indent:-9999px\">이전페이지로 이동</span>";
                endLink = "<span class=\"last ml10\" style=\"text-indent:-9999px\">끝 페이지로 이동</span>";
                nextLink = "<span class=\"next ml10\" style=\"text-indent:-9999px\">다음페이지로 이동</span>";
                
                startLinkNone = "<span class=\"first mr10\" style=\"margin-top:3px;\">&nbsp;</span>";
                prevLinkNone = "<span class=\"prev mr10\" style=\"margin-top:3px;\">&nbsp;</span>";
                endLinkNone = "<span class=\"last ml10\" style=\"margin-top:3px;\">&nbsp;</span>";
                nextLinkNone = "<span class=\"next ml10\" style=\"margin-top:3px;\">&nbsp;</span>";
                
            }
            else
            {
                prevLink    = "◀";
                nextLink    = "▶";
                startLink   = "◀◀";		
                endLink     = "▶▶";	

                prevLinkNone    = "◁";
                nextLinkNone    = "▷";
                startLinkNone   = "◁◁";
                endLinkNone     = "▷▷";
            }
%>
<!-- 
<div class="pagination">
 -->
<%          if (startKey != 1)
            {
                if(extend.equalsIgnoreCase("yes"))
                {   %>
		<a  href="javascript:<%=functionName%>(1)" onclick="<%=functionName%>(1)" class="direction bgNone" style="cursor:pointer;" ><%=startLink%></a>
<%
				}
%>
        <a  href="javascript:<%=functionName%>(<%=(startKey-1)%>)" onclick="<%=functionName%>(<%=(startKey-1)%>)" class="direction bgNone" title="<%=(startKey-1)%>페이지로 이동"><%=prevLink%></a>
<%
			}
            else
            {
                if(extend.equalsIgnoreCase("yes"))
                {   %>

        <span class="direction bgNone"><%=startLinkNone%></span>

<%              }   %>

<%--         <span class="direction bgNone"><%=prevLinkNone%></span> --%>
        <span class="direction bgNone"></span>
<%          }
			%>
<%
            for(long i = startKey; i <= stopKey ; i++)
            {
                if(nowPage == i)
                {   %>

        			<span class="num_o" style="margin:0 4px 0 0;" title="<%=i%>페이지 선택됨"><strong><%=i%></strong></span>

<%              }
                else
                {
%>
        <a  href="javascript:<%=functionName%>(<%=i%>)" onclick="this.href='javascript:<%=functionName%>(<%=i%>)'" style="cursor:pointer" class="num_o" title="<%=i%>페이지로 이동"><%=i%></a>&nbsp;
<%
            	}
            }
%>

<%
            if (stopKey != totalPage)
            {
%>
        <a  href="javascript:<%=functionName%>(<%=stopKey + 1 %>)" onclick="<%=functionName%>(<%=stopKey + 1 %>)" class="direction bgNone" style="cursor:pointer" title="<%=stopKey + 1 %>페이지로 이동"><%=nextLink%></a>
<%
				if(extend.equalsIgnoreCase("yes"))
	           {
%>
        <a  href="<%=functionName%>(<%=totalPage%>)" onclick="<%=functionName%>(<%=totalPage%>)" class="direction bgNone" style="cursor:pointer" ><%=endLink%></a>
<%           }
            }
            else
            {
%>
        <%-- <span class="direction bgNone"><%=nextLinkNone%></span> --%>
        <span class="direction bgNone"></span>
<%
				if(extend.equalsIgnoreCase("yes"))
                {
%>
			        <span class="direction bgNone"><%=endLinkNone%></span>
<%              }
            }   %>

<%      }
    }
    else
        return;
%>
<!-- 
</div>
 -->
<%! // 필요 Function

    public static boolean isNull(String str)
    {
        if (str == null || str.equals(null) || str.equals("null") || str.equals(""))
            return true;
        else
            return false;
    }

    public static String nullTo(String str)
    {
        if (isNull(str))
            str = "";

        return str;
    }

    public static String nullTo(String str, String chstr)
    {
        if (isNull(str))
            str = chstr;

        return str;
    }

    public static long nullToLong(String str)
    {
        long result = 0;

        try{ result = Long.parseLong(nullTo(str, "0")); }catch(Exception e){ result = 0; }

        return result;
    }

    public static long nullToLong(String str, long value)
    {
        long result = 0;

        try{ result = Long.parseLong(nullTo(str, "" + value)); }catch(Exception e){ result = 0; }

        return result;
    }

    public static long nullToInt(String str)
    {
        long result = 0;

        try{ result = Integer.parseInt(nullTo(str, "0")); }catch(Exception e){ result = 0; }

        return result;
    }

    public static int nullToInt(String str, int value)
    {
        int result = 0;

        try{ result = Integer.parseInt(nullTo(str, "" + value)); }catch(Exception e){ result = 0; }

        return result;
    }
%>
