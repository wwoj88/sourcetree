<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작권찾기</title>
<script type="text/javascript"> 
function rejcCheck(){
	var frm=document.frm;
	var gubun = "${gubun}";
	
	if(gubun=="insert"){
		if(confirm("수신거부 하시겠습니까?")){
			
			frm.action="/mail/insertRejc.do";			
			frm.submit();			
		}
		else{
			close();
		}
		
	}else{
		close();
	}

}
</script>
</head>
<body onload="rejcCheck();">
<form name="frm" method="post">
<input type="hidden" name="mail_addr" value="${mail_addr }"/>
<input type="hidden" name="mail_rejc_cd" value="${mail_rejc_cd }"/>
</form>
</body>

</html>
