<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="console" uri="/WEB-INF/tld/console.tld"%>
<%@ page contentType="text/html;charset=euc-kr" %>
<% 
HttpSession s = request.getSession(true);
s.putValue("NmChkSec","98u9iuhuyg87");
%>
<%@ page language="java" import="Kisinfo.Check.IPINClient" %>
<%
  String userIdnt = request.getParameter("userIdnt") == null ? "" : request.getParameter("userIdnt");
  String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName");
  String srchCheck = request.getParameter("srchCheck") == null ? "" : request.getParameter("srchCheck");
  String sDiv = request.getParameter("sDiv") == null ? "" : request.getParameter("sDiv");
  String value = request.getParameter("value") == null ? "" : request.getParameter("value");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>실명확인 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script type="text/JavaScript">

  function init(){
	  document.form1.resdCorpNumb1.focus();	
}
  function complete(){
	  var sDiv = '<%=sDiv%>';
	  var value = '<%=value%>';
	  window.opener.document.hidForm.isSsnNo.value = "Y";
	  if(sDiv == '03'){
		  window.opener.fn_goStep('<%=value%>'); 
	  }else{
		  window.opener.fn_goSetp1('<%=sDiv%>'); 
	  }
	  window.close();
  }
  
  function gb_change(chkVal){
	  var GUBUN = $('#GUBUN').val();
	    var f = document.form1;
		if( GUBUN == "5"){
			$('#COMM_NAME').removeAttr("disabled");
			$('#schCommName').hide();
		}else if(GUBUN == "2"){
			f.COMM_NAME.disabled = "true";
			$('#schCommName').show();
		}else if(GUBUN == "1"){
			f.COMM_NAME.disabled = "true";
			$('#schCommName').show();
		}
	}
  
  function isValid(obj, label){
	  if(obj.val() == "" || obj.val() == null){
		alert(label + "을(를) 입력해주세요.");				  
		obj.focus();
		return false;
	  } else {
		  return true;
	  }
  }
  
  function fnChkDupLoginId(){
	  var userIdnt = $('#USER_IDNT').val();
	  var url="/user/user.do?method=fncCheckId";
	  
	  if(isValid($('#USER_IDNT'), 'ID')) { 
			$.ajax({
				async:false,
				url:url,
				dataType:"json",
				data:{USER_IDNT:userIdnt},
				type:"post",
				success:function(data){
					if(data.result > 0) {
						alert("입력하신 ID는 중복된 ID입니다. 다른 ID를 입력해주세요.");
					} else {
						alert("ID로 "+userIdnt +" 를 사용해도 좋습니다.");
						$('#isChkDupLoginId').val("Y")
					}			
				},
				error:function(data){
					alert("ERROR");			
				}
				
			});  
	  }
  }
  
  function schCommName_OnClick(){
	  var GUBUN = $('#TRST_ORGN_DIVS_CODE').val();
	    var COMM_NAME = $('#COMM_NAME').val();
	    
	  	window.open("/user/user.do?method=fncSchCommName&GUBUN="+GUBUN+"&COMM_NAME="+COMM_NAME, "", 'target=users ,width=600, height=1000, resizable=no, scrollbars=yes, status=no');
	  	/* window.open("/user/user.do?method=fncSchCommName&GUBUN="+GUBUN, "", 'target=users ,width=600, height=1000, resizable=no, scrollbars=yes, status=no'); */
  }
  
  function fnSave(){
	  var url = "/user/user.do?method=insertAdmin";
	  var userData = $('form[name=form1]').serialize().replace(/%/g, '%25');
	  
	  if(!isValid($('#COMM_NAME'), '단체 및 기관명')){return false;}
	  if(!isValid($('#TRST_CEOX_NAME'), '대표자명')){return false;}
	  if(!isValid($('#TRST_TEXL_NUMB'), '연락처')){return false;}
	  if(!isValid($('#USER_NAME'), '이름')){return false;}
	  if(!isValid($('#USER_IDNT'), 'ID')){return false;}
	  if($('#isChkDupLoginId').val() == 'N') {alert('ID 중복을 확인해주세요.');return false;}
	  if(!isValid($('#PSWD'), '비밀번호')){return false;}
	  if(!isValid($('#MAIL'), 'E-MAIL')){return false;}
	  if(!isValid($('#MOBL_PHON'), '핸드폰번호')){return false;}
	  
	  console.log(userData);
	  
	   $.ajax({
			async:false,
			url:url,
			dataType:"json",
			data:userData,
			type:"post",
			success:function(data){
				if(data.result == 'Y') {
					alert("정상적으로 등록되었습니다. 로그인해 사용해주세요.");
					window.close();
				} else {
					alert("등록 중 오류가 발생했습니다. 관리자에게 문의 바랍니다.");
					window.close();
				}			
			},
			error:function(data){
				alert("ERROR");			
			}
			
		});  
  }


</script>
</head>
<c:if test="${!empty regiChk}">
<body onload="complete();">
</body>
</c:if>
<c:if test="${empty regiChk}">
<body onload="" class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>담당자 등록</h1>
		</div>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<div class="section">
				<span class="topLine"></span>
				<form name="form1" method="post" class="relative" action="#">
					<input type="hidden" name="srchCheck" value="Y">
					<input type="hidden" name="resdCorpNumb">
					<input type="hidden" id="userIdnt" name="userIdnt" value="<%=userIdnt%>" class="w30">
					<input type="hidden" id="sDiv" name="sDiv" value="<%=sDiv%>" class="w30">
					<input type="hidden" id="value" name="value" value="<%=value%>" class="w30">
					<input type="hidden" id="TRST_ORGN_CODE" name="TRST_ORGN_CODE" value="" />
					<input type="hidden" id="isChkDupLoginId" name="isChkDupLoginId" value="N" />  
					<input type="submit" style="display:none;">
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="담당자 등록 표">
					<caption>담당자등록표</caption>
					<colgroup>
					<col width="25%">
					<col width="*">
					</colgroup>
					<tbody>
						<%-- <tr>
							<th scope="row"><label for="userIdnt">아이디</label></th>
							<td><input id="userIdnt" name="userIdnt" value="<%=userIdnt%>" class="w30"></td>
						</tr> --%>
						<tr>
							<th scope="row">구분</th>
							<td>
								<label><select id="TRST_ORGN_DIVS_CODE" name="TRST_ORGN_DIVS_CODE" style="width: 150px; border: 1px solid;" onchange="javascript:gb_change(this.value);">
									<option value="1">대리중개업체</option>
									<option value="2">신탁관리단체</option>
									<option value="5">문화예술협단체</option>
								</select></label>
							</td>
						</tr>
						<tr>
							<th scope="row">단체 및 기관명</th>
							<td>
							<!-- <input type="text" id="COMM_NAME" name="COMM_NAME" title="단체 및 기관명" style="width: 200px;" disabled="disabled"/> -->
							<!-- <input type="text" id="COMM_NAME" name="COMM_NAME" title="단체 및 기관명" style="width: 200px;" readonly="readonly" /> -->
							<input type="text" id="COMM_NAME" name="COMM_NAME" title="단체 및 기관명" style="width: 200px;" />
							<span class="button small"><a  onclick="schCommName_OnClick();return false;" id="schCommName" name="schCommName">검색</a></span> 
							</td>
						</tr>
						<tr>
							<th scope="row">대표자명</th>
							<td>
							<input type="text" id="TRST_CEOX_NAME" name="TRST_CEOX_NAME" title="대표자명" style="width: 200px;"/> 
							</td>
						</tr>
						<tr>
							<th scope="row">연락처</th>
							<td>
							<input type="text" id="TRST_TEXL_NUMB" name="TRST_TEXL_NUMB" title="연락처" style="width: 200px;"/> 
							</td>
						</tr>
						<tr>
							<th scope="row">이름</th>
							<td>
							<input type="text" id="USER_NAME" name="USER_NAME" title="이름" style="width: 200px;"/> 
							</td>
						</tr>
						<tr>
							<th scope="row">아이디</th>
							<td>
							<input type="text" id="USER_IDNT" name="USER_IDNT" title="아이디" style="width: 200px;"/> 
							<span class="button small"><a onclick="fnChkDupLoginId();return false;">중복확인</a></span>
							<!-- <button type="submit" class="btn btn-block btn-primary btn-xs" onclick="fncCheckId();return false;">중복확인</button> --> 
							</td>
						</tr>
						<tr>
							<th scope="row">비밀번호</th>
							<td>
							<input type="text" id="PSWD" name="PSWD" title="비밀번호" style="width: 200px;"/> 
							</td>
						</tr>
						<tr>
							<th scope="row">E-Mail</th>
							<td>
							<input type="text" id="MAIL" name="MAIL" title="E-Mail" style="width: 200px;"/>
							<input type="checkbox" id="yes" name="agree" value="Y" /><label for="yes" style="font-size: 8pt;">안내메일 수신 동의</label>
							</td>
						</tr>
						<tr>
							<th scope="row">핸드폰번호</th>
							<td>
							<input type="text" id="MOBL_PHON" name="MOBL_PHON" title="핸드폰번호" style="width: 200px;"/> 
							</td>
						</tr>
					</tbody>
				</table>
					<p class="btnArea mt20"><span class="button medium"><a  onclick="javascript:fnSave();">등록</a></span></p>
				</form>
			</div>
			
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a style="cursor: pointer;" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="닫기" /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</c:if>
</html>