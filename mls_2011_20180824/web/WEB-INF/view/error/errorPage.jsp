<!-- <html><head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><title>Error</title></head><body><head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><title>Application Pool Access Denied</title></head>
<script type="text/javascript">
location.href='http://www.findcopyright.or.kr:8080/error/errorPageTest.jsp';
</script>
<body><h1>The specified request cannot be executed from current Application Pool</h1></body>
<div>this page is 403 errorPage</div>
</body></html> -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
  String login = request.getParameter("login") == null ? "" : request.getParameter("login");
%> 
<%@ page contentType="text/html;charset=euc-kr" %>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>권리자 찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">

<script type="text/javascript" src="/js/2012/deScript.js"></script>

<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script>
// Enter Key 입력시 로그인 수행 (아이디/비밀번호)
function fn_loginChk(obj){

	// EnterKey 입력시 로그인 수행
	if (event.keyCode == 13) {
		fn_login();
	}
}

// 로그인 절차 수행
function fn_login(){
  
  var frm = document.form0;

  if(frm.userIdnt.value == ""){
  	alert("아이디가 입력되지 않았습니다.");
   	frm.userIdnt.focus();
   	return;
  } else if(frm.pswd.value == ""){
   	alert("비밀번호가 입력되지 않았습니다.");
   	frm.pswd.focus();
   	return;
  } else {
    	frm.loginDivs.value = "N";
		frm.action = "/userLogin/userLogin.do";
		frm.submit();
  }
}

//로그인 절차 수행
function fn_admin_login(){
  
  var frm = document.form0;
	//frm.action = "/console/common/loginPage.page";
	frm.action = "/console/main/main.page";
	frm.submit();
}

function init() {
	var frm = document.form0;

	document.getElementById("userIdnt").style.imeMode = "inactive"; //영문모드
	frm.userIdnt.focus();
}

function engKey(){
	document.getElementById("userIdnt").style.imeMode = "inactive"; //영문모드
}
</script>
<script>
/* 	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69621660-2', 'auto');
	  ga('send', 'pageview'); */
</script>
</head>

<body onLoad="init();">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
		<!-- 2017 주석추가 : 에러페이지는 내가 만든거 미안하다 복붙했다.. -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- contents -->
		<div id="contents">
			<div id="containerBox" style="width: 1100px; height: 684px; ">
			 	<div style="width: 900px; height: 450px;margin-left: 100px; margin-top:-100px; background-color: #f9f9f9">
					<div id="errorImageBox" style="margin-top: 200px; margin-left: 300px">
						<img alt="errorImage" src="/images/2017/error/errorImg.png" style="margin-left: -110px;margin-top: 134px;">
						<div style="margin-left: 111px; margin-top: -155px;"><h4> 오류가 발생하였습니다.</h4><br>요청하신 URL이 존재하지 않거나 정상적이지 않습니다.</div>
						<a href="/main/main.do"><div style="width: 100px;height: 40px;margin-left : 171px; margin-top: 24px; background-color: #2c65aa; text-align: center;padding-top: 19px;">
							<font color="#ffffff" style="margin-top: 15px;">확인</font>
						</div></a>
					</div>
				</div>
			</div>
		</div>
		
			<!-- FOOTER str-->
			<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
</body>
</html>
		