 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권정보 조회(음악) | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">

<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
<script type="text/JavaScript">
<!--

var srchKeyword = '${mlStatWorksVO.searchKeyword1}';
jQuery(function(){
	
})

//페이징
function goPage(pageNo){
	jQuery("input[name=pageNo]").val(pageNo);
	
	jQuery("form[name=ifFrm]").attr({
		"target":"_self"
		, "method":"post"
		, "action":"/stat/statPopSrchList.do"
	}).submit();
}

//항목 중복선택방지 
function fn_chkRawCd(sNo, sChk) {
	var oObjs = document.getElementsByName("iChk");
	
	for(var i = 0; i < oObjs.length; i++) {
		if(oObjs[i].value == sNo &&sChk == true) {
			oObjs[i].checked = true;
		} else {
			oObjs[i].checked = false;
		}
	}
}

function fn_add(){
	
	var seListChkObjs = jQuery("#tblPop_rghtPrps").find("input[name=iChk]:checkbox:checked");
	
	var openerListChkObjs = jQuery(opener.document).find("input[name=iChk]:checkbox");
	
	var tableObj=jQuery(opener.document).find("#tbl_rghtPrps");
	
	var isExistYn = "N";	//중복여부
	var isAdd = false;		//줄 추가여부
	var isInsert = false;
	
	
	if(seListChkObjs.length == 0){
		alert("선택된 저작물이 없습니다.");
		return;
	}
	
	jQuery(seListChkObjs).each(function(){
		var worksId = jQuery(this).val();//선택 저작물ID
		var genreCd = trim(jQuery(this).parent().parent().children().eq(1).text());//장르
		var worksTitle = jQuery(this).parent().parent().children().eq(2).text();//저작물제호
		var coptHodr = jQuery(this).parent().parent().children().eq(3).text();//저작권자 정보
		var worksDivsCd = trim(jQuery(this).parent().parent().children().eq(4).text());//구분
		
		if(openerListChkObjs.length>0){
			for(var j=0; j<openerListChkObjs.length;j++){
				if(openerListChkObjs[j].value == worksId){
					isExistYn = "Y";
				}
			}
		}
		
		if(isExistYn == "N"){
			isAdd = true;
		}else{
			alert("선택된 저작물 ["+worksTitle+"]는 이미 추가된 저작물입니다.");
			jQuery(this).removeAttr("checked");
		}
		
		if(isAdd){
			jQuery(tableObj).find("tr").each(function(index){
				if(jQuery(this).hasClass("isAddClass")){
					isInsert = true;
					var tR = jQuery(this);
					//jQuery(this).remove();
					jQuery(this).children().each(function(index){
						if(index == 0){
							jQuery(this).empty();
							var chk = jQuery("<input>").attr({
								"type":"checkbox"
								,"value":worksId
								,"name":"iChk"
								,"title":"선택"
							}).css({
								"padding":"0px",
								"margin":"0px"
							});
							
							var chk ="<input type='checkbox' name='iChk' value="+worksId+" style='padding:0px;margin:0px;' title='선택'>"
							
							jQuery(this).html(chk);
						}
						if(index == 2){
							jQuery(this).empty();
							jQuery(this).html(genreCd);
						}
						if(index == 3){
							jQuery(this).empty();
							jQuery(this).html(worksTitle);
						}
						if(index == 4){
							jQuery(this).empty();
							jQuery(this).html(worksDivsCd);
						}
					});
					
					var mainTitle ="";
					var reqCnt = jQuery(tableObj).find("tr").size()-1;
					
					if(reqCnt == 0){
						jQuery(opener.document).find("#isCntChk").attr("checked",false);
					}else if(reqCnt == 1){
						jQuery(opener.document).find("#isCntChk").attr("checked",false);
						jQuery(tableObj).find("tr").each(function(index){
							if(index == 1){
								mainTitle = jQuery(this).children().eq(3).text();
							}
						})
					}else{
						jQuery(tableObj).find("tr").each(function(index){
							if(index == 1){
								mainTitle = jQuery(this).children().eq(3).text();
							}
						})
						jQuery(opener.document).find("#isCntChk").attr("checked",true);
					}
					jQuery(opener.document).find("#mainTitle").val(mainTitle);
					jQuery(opener.document).find("#reqCnt").val(reqCnt);
					
					/* var oTd = jQuery("<td>").addClass("ce").attr("colspan","5");
					chk.appendTo(oTd);
					oTd.appendTo(tR); */
				}else{
					
				};
			});
		}
	})
	if(!isInsert){
		if(isAdd){
			alert("추가한 테이블 로우에 선택저작물을 추가 할 수 없습니다. ");
			window.close();
		}else{
		}
	}else{
		window.close();
	}
}


-->
</script>
</head>
<style type="text/css"> 
	.mt3{
		margin-top:3px;
	}
</style>
<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<form name="ifFrm" action="#">
			<input type="hidden" name="pageNo" value="${paginationInfo.currentPageNo}"/>
			<input type="hidden" name="searchKeyword1" value="${mlStatWorksVO.searchKeyword1}"/>
			<!-- HEADER str-->
			<div id="popHeader">
				<h1>법정허락 신청 대상저작물 조회 및 선택</h1>
			</div>
			<!-- //HEADER end -->
			
			
			<!-- CONTAINER str-->
			<div id="popContents" style="padding-bottom: 15px;">
				
				<div class="section">
					<h2>법정허락 신청 대상저작물 목록</h2>
					<table cellspacing="0" cellpadding="0" border="1" class="grid" id="tblPop_rghtPrps" style="margin-bottom:10px;" summary="음악저작물의 앨범정보 입니다.">
						<colgroup>
							<col width="2%">
							<col width="12%">
							<col width="*">
							<col width="15%">
							<col width="20%">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">
								</th>
								<th scope="col">장르</th>
								<th scope="col">저작물 제호</th>
								<th scope="col">저작권자 정보</th>
								<th scope="col">구분</th>
							</tr>
						</thead>
						<tbody>
						<c:if test="${empty mlStatWorksList}">
							<tr>
								<td class="ce" colspan="5">
								검색된 목록이 없습니다.
								</td>
							</tr>
						</c:if>
						<c:if test="${!empty mlStatWorksList}">
							<c:forEach items="${mlStatWorksList}" var="mlStatWorks">
							<c:set var="i" value="${i+1}"/>
							<tr>
								<td class="ce">
									<input type="checkbox" name="iChk" value="${mlStatWorks.worksId}" style="padding:0px;margin:0px;"
									 title="선택" onclick="fn_chkRawCd(this.value, this.checked);" onkeypress="fn_chkRawCd(this.value, this.checked);">
								</td>
								<td class="ce">
									<label for="sc10">
									<c:forEach items="${genreList}" var="genreList">
										<c:if test="${genreList.code == mlStatWorks.genreCd}">
											${genreList.codeName}
										</c:if>
									</c:forEach>
									</label>
								</td>
								<td>${mlStatWorks.worksTitle}</td>
								<td class="ce">${mlStatWorks.coptHodr}</td>
								<td class="ce">
									<c:forEach items="${worksDivsList}" var="worksDivsList">
										<c:if test="${worksDivsList.code == mlStatWorks.worksDivsCd}">
											${worksDivsList.codeName}
										</c:if>
									</c:forEach>
								</td>
							</tr>
							</c:forEach>
						</c:if>
						</tbody>
					</table>
				</div>
				<!--paging start-->
				<div class="pagination">
					<jsp:include page="../common/PageList_2011.jsp" flush="true">
						<jsp:param name="totalItemCount" value="${paginationInfo.totalRecordCount}" />
						<jsp:param name="nowPage"        value="${paginationInfo.currentPageNo}" />
						<jsp:param name="functionName"   value="goPage" />
						<jsp:param name="listScale"      value="" />
						<jsp:param name="pageScale"      value="" />
						<jsp:param name="flag"           value="M01_FRONT" />
						<jsp:param name="extend"         value="no" />
					</jsp:include>
				</div> 
				<div class="floatDiv mt0 mb5">
					<p class="fl">
						<a title="선택한 항목을 [법정허락 신청 대상저작물 선택목록]에 추가합니다." onclick="javascript:fn_add();" href="#1">
						<img alt="추가" src="/images/2012/button/add_down.gif">
						</a>
					</p>
				</div>
				<!--paging end-->
			</div>
			<!-- //CONTAINER end -->
			<!-- FOOTER str-->
			<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights reserved.</p>
			<!-- //FOOTER end -->
			<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2011/button/pop_close.gif" alt="X" title="이 창을 닫습니다." /></a>
		</form>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>

