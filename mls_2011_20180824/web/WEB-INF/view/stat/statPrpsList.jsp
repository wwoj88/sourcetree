<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 이용승인신청 | 저작권자찾기</title>

<!-- <link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css"> -->
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<style type="text/css">
<!--
table tbody tr td.tbBg {
	background-color: #fafafa;
}
-->
</style>



<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript"
	src="/js/Function.js"></script>
<script type="text/javascript"
	src="<c:url value="/js/jquery-1.7.1.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/json2.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/jquery.form.2.34.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/quickpager2.jquery.js"/>"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>


<script type="text/javascript"> 

function fn_goList(){
	var frm = document.srchForm;

	if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.encoding = "application/x-www-form-urlencoded";
		frm.action = "/stat/statSrch.do";
		frm.submit();
	}
}

function fn_goMyList(){
	var frm = document.srchForm;

	if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.encoding = "application/x-www-form-urlencoded";
		frm.action = "/myStat/statRsltInqrList.do";
		frm.submit();
	}
}



jQuery(function(){
	jQuery(".pageme").quickPager({
		pageSize: 5,
		naviSize: 10,
		currentPage: 1,
		holder: ".pager"
	});
	
	jQuery(window).load(function() {
		//alert("HTML로드 완료");
	});
});

//임시저장 및 화면이동
function fn_doSave(sDiv) {
	
	var frm = document.frm;
	frm.stat_cd.value = '${statCd }';
	frm.action_div.value = sDiv;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		/*if(sDiv == 'goStep3' && frm.existYn.value != 'Y') {
			alert('현재 작성하신 이용승인신청에 대한\n\r\'이용승인명세서\' 관련 첨부파일이 첨부되지 않았습니다.\n\r이전단계 이동합니다.');
			frm.stat_cd.value = '${statCd }';
		}*/
	
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/stat/statPrpsDetl.do";
		frm.submit();
	}
}

//이전화면
function fn_goStep1() {
	var frm = document.frm;

	//로그인 체크
	var userId = <%=sessUserIdnt%>;
		if (userId == 'null' || userId == '') {
			alert('로그인이 필요한 화면입니다.');
			location.href = "/user/user.do?method=goSgInstall";
			return;

		} else {
			frm.stat_cd.value = '${statCd }';
			frm.isModi.value = "02";
			frm.target = "_self";
			frm.method = "post";
			frm.action = "/stat/statPrpsMain.do";
			frm.submit();
		}
	}
//-->
</script>


<!-- <style type="text/css">
#titleImg {
	height: 250px;
}

p {
	background: #e5e5e5;
	margin-bottom: 1px;
	margin-top: 0px;
}

ul.paging li {
	padding: 10px;
	background: #ccc;
	font-family: georgia;
	font-size: 24px;
	color: #fff;
	line-height: 1;
	width: 180px;
	margin-bottom: 1px;
}

ul.pageNav li {
	display: block;
	floaT: left;
	padding: 3px;
	font-family: georgia;
}

ul.pageNav li a {
	color: #333;
	text-decoration: none;
}

/* UI Object */
.paginate_regular {
	padding: 15px 0;
	text-align: center;
	line-height: normal
}

.paginate_regular a, .paginate_regular strong {
	display: inline-block;
	position: relative;
	margin: 0 -2px;
	padding: 2px 8px;
	font-weight: bold;
	font-size: 12px;
	font-family: Tahoma, Sans-serif;
	color: #333;
	line-height: normal;
	text-decoration: none;
	vertical-align: middle
}

.paginate_regular a:hover, .paginate_regular a:active, .paginate_regular a:focus
	{
	background-color: #f8f8f8
}

.paginate_regular strong {
	color: #f60
}

.paginate_regular .direction {
	font-weight: normal;
	color: #767676;
	white-space: nowrap
}

.paginate_regular .direction span {
	font-weight: bold;
	font-size: 14px
}

.paginate_regular .direction:hover, .paginate_regular .direction:active,
	.paginate_regular .direction:focus {
	background-color: #fff;
	color: #333
}

.paginate_regular a.currentPage {
	color: #f60
}

.grid tbody td.errBg {
	background-color: #faebe4;
}

.loadingImg {
	width: 32px;
	height: 32px;
	text-align: right;
	position: absolute;
}

.loadingImg.move {
	opacity: 0.4;
	position: absolute;
}

.loadingImg>img {
	width: 32px;
	height: 32px;
	display: block;
}

/* //UI Object */
.con_lf {
	width: 50px;
}

.content {
	left: 600px;
	width: 500px;
	position: relative;
}

#container {
	margin-left: 200px;
	width: 780px;
}

.contentBody {
	left: -350px;
}
</style> -->
</head>

<body>
  <!-- //래프 -->
<!-- 
	<div id="ajaxBox"
		style="position: absolute; z-index: 1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left: -500px; width: 306px; height: 38px; padding: 102px 0 0 0;">
		<p style="height: 38px; text-align: center; margin: 0;">
			<img src="/images/2012/common/loading.gif" alt=""
				style="margin-top: -4px; margin-bottom: 3px;" /><br /> <span
				id="ajaxBoxMent"
				style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만
				기다려주세요..</span>
		</p>
	</div> -->

	<!-- HEADER str-->
	<!-- 2017변경 -->
	<jsp:include page="/include/2017/header.jsp" />

	<!-- //HEADER end -->
	<!-- CONTAINER str-->
	<div id="contents">
		<div class="con_lf">


			<div class="con_lf_big_title">
				법정허락<br>승인 신청
			</div>
			<ul class="sub_lf_menu">
				<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
				<li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
				<li><a class="on"
					href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회
						공고</a></li>
				<li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>

				<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
			</ul>

		</div>
		<form name="srchForm" action="#">
			<c:if test="${isMyPage == 'Y'}">
				<input type="hidden" name="srchApplyType"
					value="${srchParam.srchApplyType }" />
				<input type="hidden" name="srchApplyFrDt"
					value="${srchParam.srchApplyFrDt }" />
				<input type="hidden" name="srchApplyToDt"
					value="${srchParam.srchApplyToDt }" />
				<input type="hidden" name="srchApplyWorksTitl"
					value="${srchParam.srchApplyWorksTitl }" />
				<input type="hidden" name="srchStatCd"
					value="${srchParam.srchStatCd }" />
				<input type="hidden" name="page_no" value="${srchParam.nowPage }" />
			</c:if>
			<c:if test="${isMyPage == ''}">
				<input type="hidden" name="pageNum" value="${srchVO.pageNum}" />
				<input type="hidden" name="searchCondition"
					value="${srchVO.searchCondition}" />
				<input type="hidden" name="searchKeyword1"
					value="${srchVO.searchKeyword1}" />
			</c:if>
			<input type="submit" style="display: none;">
		</form>
		<!-- <div id="container" class="container_vis3"> -->
			<!-- 추가 -->


			<!-- content st -->
			<!-- <div class="contents"> -->





				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					<c:if test="${isMyPage=='Y'}">
						<!-- <p class="path">
							<span>Home</span><span>마이페이지</span><span>신청현황</span><em>법정허락 이용승인신청</em>
						</p> -->
					</c:if>
					<c:if test="${isMyPage==''}">
						<div class="con_rt_head">
							<img src="/images/sub_img/sub_home.png" alt="홈 페이지" /> &gt; 법정허락
							승인 신청 &gt; <span class="bold">서비스 이용</span>
						</div>
						<div class="con_rt_hd_title">법정허락 승인 신청</div>
					</c:if>

					<!-- section -->
					<div class="section">
						<!-- memo 삽입 -->
						<div class="sub01_con_bg4_tp mar_tp30"></div>
						<div class="sub01_con_bg4">
							<div class="font15">
								<span class="color_2c65aa">법정허락 신청에 관한 문의는 신청분류에 해당하는 단체로
									연락바랍니다.</span>
							</div>
							<h3 class="mar_tp10">
								<span class="w20">- 한국저작권위원회</span> <em class="w20 mar_tp10"
									style="margin-left: 30px;"><img
									src="/images/2012/common/ic_nm.gif" alt="" />심의조사팀</em> <em
									class="w25"><img src="/images/2012/common/ic_tel.gif"
									alt="" />&nbsp;02-2660-0104</em>
							</h3>
						</div>
						<!-- // memo 삽입 -->

						<!-- process -->
						<div class="usr_process mt20">
							<div class="process_box">
								<ul class="floatDiv">
									<li class="fl ml0"><img alt="1단계 약관동의"
										src="/images/2012/content/process21_off.gif"></li>
									<li class="fl on"><img alt="2단계 회원정보입력"
										src="/images/2012/content/process22_on.gif"></li>
									<li class="fl bgNone pr0"><img alt="3단계 가입완료"
										src="/images/2012/content/process23_off.gif"></li>
								</ul>
							</div>
						</div>
						<!-- // process -->
						<form name="frm" action="#">
							<c:if test="${isMyPage == 'Y'}">
								<input type="hidden" name="srchApplyType"
									value="${srchParam.srchApplyType }" />
								<input type="hidden" name="srchApplyFrDt"
									value="${srchParam.srchApplyFrDt }" />
								<input type="hidden" name="srchApplyToDt"
									value="${srchParam.srchApplyToDt }" />
								<input type="hidden" name="srchApplyWorksTitl"
									value="${srchParam.srchApplyWorksTitl }" />
								<input type="hidden" name="srchStatCd"
									value="${srchParam.srchStatCd }" />
								<input type="hidden" name="page_no"
									value="${srchParam.nowPage }" />
								<input type="hidden" name="worksId" value="${worksId}" />
							</c:if>
							<c:if test="${isMyPage == ''}">
								<input type="hidden" name="pageNo" value="${srchVO.pageNum}" />
								<input type="hidden" name="searchCondition"
									value="${srchVO.searchCondition}" />
								<input type="hidden" name="searchKeyword1"
									value="${srchVO.searchKeyword1}" />
								<input type="hidden" name="worksId"
									value="${srchVO.searchWorksId}" />
							</c:if>
							<input type="hidden" name="isMy" value="${isMy}" /> <input
								type="hidden" name="action_div" value="" /> <input
								type="hidden" name="applyWriteYmd"
								value="${srchParam.applyWriteYmd }"> <input
								type="hidden" name="applyWriteSeq"
								value="${srchParam.applyWriteSeq }"> <input
								type="hidden" name="apply_write_ymd"
								value="${srchParam.applyWriteYmd }"> <input
								type="hidden" name="apply_write_seq"
								value="${srchParam.applyWriteSeq }"> <input
								type="hidden" name="stat_cd" /> <input type="hidden"
								name="isModi" /> <input type="hidden" name="existYn"
								value="${existYn}" /> <input type="submit"
								style="display: none;">
							<div class="article">
								<div class="floatDiv">
									<h2 class="fl">
										이용 승인신청 명세서 정보
										<!-- <select id="sc1" class="ml10"><option>1</option></select> <span class="button small thin"><a href="">이동</a></span> -->
									</h2>
									<p class="fr">
										<!--  
                                    <span class="button small icon"><a href="javascript:openSmplDetail('MR')">예시화면 보기</a><span class="help"></span></span>
                                    -->
									</p>
								</div>
								<span class="topLine"></span>
								<!-- 그리드스타일 -->
								<div class="pageme">
									<c:if test="${!empty statApplyWorksList}">
										<c:forEach items="${statApplyWorksList}" var="list">
											<c:set var="i" value="${i+1}" />
											<table cellspacing="0" cellpadding="0" border="1"
												class="grid" summary="">
												<colgroup>
													<col width="6%">
													<col width="15%">
													<col width="20%">
													<col width="20%">
													<col width="20%">
													<col width="*">
												</colgroup>
												<tbody>
													<tr>
														<td class="ce" rowspan="7"><c:out value="${i}" /></td>
														<th scope="row" rowspan="2"><c:forEach
																items="${applyTypeList}" var="applyTypeList">
																<c:if test="${list.APPLYTYPE01 == '1'}">
																	<c:if test="${applyTypeList.code==1}">${applyTypeList.codeName }</c:if>
																</c:if>
																<c:if test="${list.APPLYTYPE02 == '1'}">
																	<c:if test="${applyTypeList.code==2}">${applyTypeList.codeName }</c:if>
																</c:if>
																<c:if test="${list.APPLYTYPE03 == '1'}">
																	<c:if test="${applyTypeList.code==3}">${applyTypeList.codeName }</c:if>
																</c:if>
																<c:if test="${list.APPLYTYPE04 == '1'}">
																	<c:if test="${applyTypeList.code==4}">${applyTypeList.codeName }</c:if>
																</c:if>
																<c:if test="${list.APPLYTYPE05 == '1'}">
																	<c:if test="${applyTypeList.code==5}">${applyTypeList.codeName }</c:if>
																</c:if>
															</c:forEach></th>
														<td class="tbBg">제호(제목)</td>
														<td colspan="3">${list.WORKSTITL }</td>
													</tr>
													<tr>
														<td class="tbBg">종류</td>
														<td>${list.WORKSKIND }</td>
														<td class="tbBg">형태 및 수량</td>
														<td>${list.WORKSFORM }</td>
													</tr>
													<tr>
														<th scope="row" rowspan="2">공표</th>
														<td class="tbBg">공표 연월일</td>
														<td>${list.PUBLYMD }<c:if
																test="${list.PUBLYMD == '' || list.PUBLYMD == null}">
												알수없음
												</c:if>
														</td>
														<td class="tbBg">공표국가</td>
														<td>${list.PUBLNATN }<c:if
																test="${list.PUBLNATN == '' || list.PUBLNATN == null}">
												알수없음
												</c:if>
														</td>
													</tr>
													<tr>
														<td class="tbBg">공표방법</td>
														<td>
															<%--${listPUBLMEDICD}  --%> <c:if
																test="${list.PUBLMEDICD == '' || list.PUBLMEDICD == null || list.PUBLMEDICD == 0}">
												알수없음
												</c:if>
														</td>
														<td class="tbBg">공표매체정보</td>
														<td>${list.PUBLMEDI }<c:if
																test="${list.PUBLMEDI == ''|| list.PUBLMEDI == null}">
												알수없음
												</c:if>
														</td>
													</tr>
													<tr>
														<th scope="row" rowspan="2">권리자</th>
														<td class="tbBg">성명(법인명)</td>
														<td>${list.COPTHODRNAME }<c:if
																test="${list.COPTHODRNAME == '' || list.COPTHODRNAME == null}">
												알수없음
												</c:if>
														</td>
														<td class="tbBg">전화번호</td>
														<td>${list.COPTHODRTELXNUMB }<c:if
																test="${list.COPTHODRTELXNUMB == '' || list.COPTHODRTELXNUMB == null}">
												알수없음
												</c:if>
														</td>
													</tr>
													<tr>
														<td class="tbBg">주소</td>
														<td colspan="3">${list.COPTHODRADDR }<c:if
																test="${list.COPTHODRADDR == '' || list.COPTHODRADDR == null}">
												알수없음
												</c:if>
														</td>
													</tr>
													<tr>
														<th scope="row" class="tbBg">신청물의 내용</th>
														<td colspan="4">${list.WORKSDESC}<c:if
																test="${list.WORKSDESC == '' || list.WORKSDESC == null}">
												알수없음
												</c:if>
														</td>
													</tr>
												</tbody>
											</table>
										</c:forEach>
									</c:if>
								</div>
								<!-- //그리드스타일 -->
							</div>
							<div class="pager"></div>
						</form>
						<!-- 버튼영역 -->
						<div class="btnArea">
							<p class="fl">
								<span class="button medium gray"><a href="#1"
									<c:if test="${isMyPage=='Y'}" > onclick="fn_goMyList();" onkeypress="fn_goMyList();" </c:if>
									<c:if test="${isMyPage==''}" > onclick="fn_goList();" onkeypress="fn_goList();" </c:if>>취소</a></span>
								<span class="button medium gray"><a href="#1"
									onclick="fn_goStep1();" onkeypress="fn_goStep1();">이전단계</a></span>
							</p>
							<p class="fr">
								<span class="button medium">
								<a		onclick="fn_doSave('goList');"		onkeypress="fn_doSave('goList');" href="#1">임시저장</a></span>
								 <span				class="button medium">
								 <a onclick="fn_doSave('goStep3');" 	 href="#">작성내용확인</a></span>
							</p>
						</div>
						<!-- //버튼영역 -->
					</div>
					<!-- //section -->
				<!-- </div> -->
				<!-- //주요컨텐츠 end -->

			<!-- </div> -->
			<!-- //content -->
			<!-- FOOTER str-->
			<!-- 2017변경 -->

			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
			<!-- //FOOTER end -->
		</div>
		<jsp:include page="/include/2017/footer.jsp" />
		<!-- //CONTAINER -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->

	<script type="text/JavaScript">
		
	</script>
	<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>

