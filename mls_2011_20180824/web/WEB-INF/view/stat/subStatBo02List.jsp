<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금신청 | 권리자찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
	
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>

<script type="text/javascript" src="/js/2010/common.js"></script>
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>

<script type="text/JavaScript">
<!--

//window.name = "muscRghtSrch";
	(function($) { // function 에 $를 삽입한다. $를 사용하기 위한 함수를 만든다.
		$(function() {         // 실제사용할 함수를 만든다.
			
			var the_height = $("#tab_scroll").height();
		
			$("#div_scroll").css("height",the_height+13);
			parentResizeCall();
		
		
			$(window).load(function(){
				/* console.log($('#ajaxBox')) */
				
				//$('#ajaxBox').hide();	
				// 로딩 이미지 박스 hidden..
				parent.frames.hideAjaxBox();
				parent.frames.scrollTo(0,0); 
			
			})
		});
	})(jQuery);
	
	
	function parentResizeCall(){
		parent.frames.resizeIFrame("ifSubStatList");
	}
	
	//페이징
	function goPage(pageNo){
		
		// 로딩 이미지 박스 보이게..
		new Ajax.Request('/test', {   
			onLoading: function() {     
			},
			onSuccess: function(req) {     
			},
			onComplete: function() {     
				parent.frames.showAjaxBox();
			} 
		});
		
		jQuery("input[name=pageNo]").val(pageNo);
		
		jQuery("form[name=ifFrm]").attr({
			"target":"_self"
			, "method":"post"
			, "action":"/stat/subStatList.do"
		}).submit();
	}
	
	function objcPop(worksId, divsCd){
		var userId = '<%=sessUserIdnt%>';
		if(userId == 'null' || userId == ''){
			alert('로그인이 필요한 화면입니다.');
		}else if(userId != 'null' || userId!= ''){
		window.open("/statBord/statBo02ObjcPop.do?worksId="+worksId+"&divsCd="+divsCd, "small", "width=650, height=367, scrollbars=yes, menubar=no, location=yes");
		}
	}
//-->
</script>
<!-- <style type="text/css">

body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 8px 0;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	width: 700px;
	margin-left: 0px;
}

</style> -->
</head>
<body>
		<form name="ifFrm" action="#">
			<input type="hidden" name="pageNo" value="${paginationInfo.currentPageNo}"/>
			<input type="hidden" name="searchCondition" value="${mlStatWorksVO.searchCondition}"/>
			<input type="hidden" name="searchKeyword1" value="${mlStatWorksVO.searchKeyword1}"/>
			<input type="hidden" name="searchKeyword2" value="${mlStatWorksVO.searchKeyword2}"/>
			<!-- 테이블 영역입니다 -->
				<table class="sub_tab3 mar_tp20 td_pad5" cellspacing="0" cellpadding="0" width="100%"
					summary="법정허락 승인 신청 목록 표로 장르 , 저작물 제호, 저작권자, 공표일자, 구분, 이의신청으로 구성되어 있습니다.">
					<caption>법정허락 승인 신청 조회표</caption>
					<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
					<colgroup>
						<col width="5%"/>
						<col width="8%"/>
						<col width="*"/>
						<col width="12%"/>
						<col width="13%"/>
						<col width="17%"/>
						<col width="13%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col"><input type="checkbox" id="chk"
								class="vmid"
								onclick="javascript:checkBoxToggle('ifFrm','iChk',this);"
								style="cursor: pointer;" title="전체선택" />
							</th>
							<th scope="col">장르</th>
							<th scope="col">저작물 제호</th>
							<th scope="col">저작권자</th>
							<th scope="col">공표일자</th>
							<th scope="col">구분</th>
							<th scope="col">이의신청</th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${empty mlStatWorksList}">
						<tr>
							<td class="ce"colspan="7">검색된 목록이 없습니다.</td>
						</tr>
					</c:if>
					<c:if test="${!empty mlStatWorksList}">
						<c:forEach items="${mlStatWorksList}" var="mlStatWorks" varStatus="status">
						<c:set var="i" value="${i+1}"/>
						<tr>
							<td class="ce">
								<input type="checkbox" name="iChk" value="${mlStatWorks.worksId}" style="padding:0px;margin:0px;" title="${genreList[mlStatWorks.genreCd-1].codeName} ${mlStatWorks.worksTitle} 선택" >
							</td>
							<td class="ce">
								<c:forEach items="${genreList}" var="genreList">
									<c:if test="${genreList.code == mlStatWorks.genreCd}">
										${genreList.codeName}
									</c:if>
								</c:forEach>
							</td>
							<td style="text-align: left;">${mlStatWorks.worksTitle}</td>
							<td class="ce">${mlStatWorks.coptHodr}</td>
							<td class="ce">${mlStatWorks.mediOpenDate}</td>
							<td class="ce">
								<c:forEach items="${worksDivsList}" var="worksDivsList">
								
									<c:if test="${worksDivsList.code == mlStatWorks.worksDivsCd}">
										${worksDivsList.codeName}
									</c:if>
								</c:forEach>
							</td>
							<td class="ce"><span class="file_delete"><a href="javascript:objcPop(${mlStatWorks.worksId},${mlStatWorks.worksDivsCd})"><font color="#ffffff">이의신청</font></a></span></td>
						</tr>
						</c:forEach>
					</c:if>
					</tbody>
				</table>
			<!-- //테이블 영역입니다 -->
				<!--paging start-->
				<div class="pagination">
					<jsp:include page="../common/PageList_2011.jsp" flush="true">
						<jsp:param name="totalItemCount" value="${paginationInfo.totalRecordCount}" />
						<jsp:param name="nowPage"        value="${paginationInfo.currentPageNo}" />
						<jsp:param name="functionName"   value="goPage" />
						<jsp:param name="listScale"      value="" />
						<jsp:param name="pageScale"      value="" />
						<jsp:param name="flag"           value="M01_FRONT" />
						<jsp:param name="extend"         value="no" />
					</jsp:include>
				</div> 
				<!--paging end-->
			<!--contents end-->
		</form>
</body>
</html>
