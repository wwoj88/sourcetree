<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%
    User user = SessionUtil.getSession(request);
			String sessUserIdnt = user.getUserIdnt();
			String sessUserName = user.getUserName();

			String sessUserAddr = user.getUserAddr();
			String sessMoblPhon = user.getMoblPhon();
			pageContext.setAttribute("UserIdnt", sessUserIdnt);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 찾기 검색 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="http://code.jquery.com/jquery-latest.js"
	type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/Function.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
<script type="text/javascript" language="javascript"
	src="/js/makePCookie.js"></script>
<script type="text/javascript">$(window).load(function(){		//입력후 창닫고 부모창 리로드
	var sucValue = document.getElementById("Suc").value;
	if(sucValue == 1){
		self.close();
		opener.parent.location.reload();
	}
});

var iRowIdx = 1;
function fn_addRow1(){
		  iRowIdx++;
		  if(iRowIdx > 5){
			  alert("첨부파일은 최대5개까지 가능합니다.");
			  iRowIdx--;
			  return;
		  }
	  	 var oTbl = document.getElementById("tblAttachFileIn");
		 var oTbody = oTbl.getElementsByTagName("TBODY")[0];
		  
		  var oTr = document.createElement("TR");
		  oTr.id="fileTr"+iRowIdx;
			//순번(선택)
			var oTd = document.createElement("TD");
			var sTag = '<input name="chkDel1" id="chkDel1_'+iRowIdx+'" type="checkbox" value='+iRowIdx+' />';
			oTd.innerHTML = sTag;
			oTd.id = "fileTd"+iRowIdx;
			oTd.style.width = '7%';
			oTd.className = 'ce';
			oTr.appendChild(oTd);

			
			//첨부화일명
			oTd = document.createElement("TD");
			oTd.colSpan = 2;
			sTag = '<input type="hidden" name="fileDelYn" id="fileDelYn_'+iRowIdx+'" />';
			sTag += '<span id="spfile'+iRowIdx+'"><input type="file" name="file'+iRowIdx+'" id="file_'+iRowIdx+'" class="inputData L w100" onkeydown="return false;" onchange="checkFile(this.id)"/></span>';
			oTd.innerHTML = sTag;
			oTr.appendChild(oTd);
			
			oTbody.appendChild(oTr);
		}
function fn_delRow1(){
	var oChkDel = document.getElementsByName("chkDel1");
	var count = 0;
	var checkArray = new Array();

	for(var i=0; i<oChkDel.length; i++){
	if(oChkDel[i].checked == true){
		count++;
		var checkNum="";
		checkNum += oChkDel[i].value;
		checkArray.push(checkNum);
		iRowIdx--;
		}
	}
	
	if(count == 0){
		alert("삭제하실 내용을 체크해주세요.");
	}else{
			for(var i = 0 ; i < checkArray.length ; i ++){
					element = document.getElementById('fileTr'+checkArray[i]);
					element.parentNode.removeChild(element);
			}
		}
	}

function ObjcInsert(worksId){  // 권리자 이의제기 insert
	var frm = document.getElementById("ObjcInsertForm");
 	frm.action = "/statBord/statBo02ObjcInsert.do?worksId="+worksId;
	frm.submit();

}
function updateOc(statObjcId){	//권리자 이의제기 update 열닫
	var i=statObjcId;
	var docu = document.getElementById("updatediv"+i).style.display;
	var objc = document.getElementById("objc").value;
// 	var objcRepl = objc.replace(/<br/>/g, "\r\n");
	document.getElementById("objcDesc").value = objcRepl;

	if(docu == "none"){
		document.getElementById("updatediv"+i).style.display = "block";
		document.getElementById("commentdiv"+i).style.display = "none";
	}else{
		document.getElementById("updatediv"+i).style.display = "none";
		document.getElementById("commentdiv"+i).style.display = "block";
	}
}
function ObjcInsertoc(){			//권리자 이의제기 insert 열닫
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
	}else if(userId != 'null' || userId!= ''){
		var i = document.getElementById("ObjcInsert").style.display;
		if(i == "none"){
			document.getElementById("ObjcInsert").style.display = "block";
		}else{
			document.getElementById("ObjcInsert").style.display = "none";
		}
	}
}
function updateObjc(){
	var upf = document.getElementById("updateObjcForm");
	upf.submit();
}

</script>
</head>

<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<!-- HEADER str-->
	<div id="pop_wrap">
		<div id="popHeader">
			<h1>법정허락 신청 대상저작물 이의신청</h1>
		</div>
		<!-- //HEADER end -->
		<div id="contentBody" class="floatDiv mt20">
			<div class="section" style="padding: 0 15px 15px 15px;">
				<h2>권리자 이의신청</h2>
				<p>
					<span class="p11 orange block mt5 ml5">※ 아래 법정허락 신청 대상 저작물에 대해 권리자 이의신청을 합니다.</span>
				<div class="layer_pop w80" id="help_pop1">
					<div class="layer_con">
						<p class="p11 black">
							<strong>준비중 입니다.</strong>
						</p>
					</div>

					<a href="javascript:;" onclick="javascript:toggleLayer('help_pop1');"
						class="layer_close" id="toggleClose"><img
						src="/images/2012/button/layer_close.gif" alt="close" /> </a>
				</div>
				<!-- //도움말 레이어 -->

				<p class="rgt mt20">
					<span class="button small icon"><a href="javascript:;"
						onclick="javascript:toggleLayer('help_pop1');"
						class="ml10 underline black2" id="toggleBtn">예시화면 보기</a><span class="help"></span>
					</span>
				</p>

				<table border="1" cellspacing="0" cellpadding="0" summary="권리자 이의신청 리스트"
					class="grid mt5">
					<caption>권리자 이의신청 리스트</caption>
					<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->

					<colgroup>
						<col width="20%">
						<col width="*">
						<col width="15%">
						<col width="15%">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">장르</th>
							<th scope="col">저작물 제호</th>
							<th scope="col">저작권자 정보</th>
							<th scope="col">구분</th>
						</tr>
					</thead>
					<tbody>
						<tr>

							<td class="ce pd5">${AnucBord.genreCdName}</td>
							<td>${AnucBord.worksTitle}</td>
							<td class="ce"><c:if test="${AnucBord.divsCd == 4}">
									<c:forEach var="coptHodr" items="${coptHodr}">
									${coptHodr.coptHodrName}<br />
									</c:forEach>
								</c:if> <c:if test="${AnucBord.divsCd == 6}">
									${AnucBord.coptHodrName}
									</c:if>
							</td>
							<td class="ce pd5">${AnucBord.divsCdName} <input
								type="hidden" id="Suc" value="${Success}" />
							</td>
						</tr>
					</tbody>
				</table>

				<div class="result_area floatDiv mt20">
					<!-- 이의제기 갯수 표시 -->
					<c:if test="${count != 0}">
						<p class="tab fl">
							<span class="tab2 p12">이의제기 <strong>${count}</strong>건</span>
						</p>
					</c:if>
					<p class="fr">
						<span class="p11 line22">이의제기 내용은 작성자 본인만 조회됩니다.</span>
					</p>
				</div>
				<!-- 이의제기 등록 폼 -->
				<c:if test="<%=sessUserIdnt != null%>">
					<form method="post" action="#" id="ObjcInsertForm"
						enctype="multipart/form-data">
						<div class="mt10" id="ObjcInsert" style="display: none">
							<input type="hidden" name="rgstIdnt" id="rgstIdnt"
								value="<%=sessUserIdnt%>" />
							<input type="hidden" id="mgntDivs" name="mgntDivs" value="BO02_01" />
							<input type="hidden" id="worksTitl" name="worksTitl" value="${AnucBord.worksTitle}" />
							
							<div class="mb15">
								<span class="topLine"></span>
								<table id="tblFile" border="1" cellspacing="0"
									class="grid mb15 " summary="첨부화일 표입니다.">
									<!-- summary는 표의 간략한 설명을 써주면 됨 -->
									<caption>첨부화일 표</caption>
									<colgroup>
										<col width="15%">
										<col width="*%">
									</colgroup>

									<tbody>
										<tr>
											<th scope="row">
												<label for="" class="necessary">내용</label>
											</th>
											<td class="ce" align="center">
												<textarea name="objcDesc" style="height: 45px; width: 85%" title="objcDesc"></textarea>&nbsp;
													<span class="button large">
														<a href="javascript:;" onclick="javascript:ObjcInsert(${AnucBord.worksId});">등록</a>
													</span>
											</td>
										</tr>
										<tr>
											<th scope="row"><label for="" class="necessary">첨부서류</label>
											</th>
											<td colspan="3">
												<!-- 첨부화일 시작 -->

												<div class="">
													<div class="fr mb5">
														<p>
															<span title="추가" class="button small"><button
																	id="btnAdd" onkeypress=""
																	onclick="javascript:fn_addRow1('-1', 'Y');"
																	type="button">File 추가</button> </span> <span title="삭제"
																class="button small"><button id="btnDel"
																	onkeypress=""
																	onclick="javascript:fn_delRow1('-1', 'Y');"
																	type="button">File 삭제</button> </span>
														</p>
													</div>
												</div>
												<div class="mb15">
													<table id="tblAttachFileIn" border="1" cellspacing="0"
														class="grid mb15 " summary="첨부화일 표입니다.">
														<!-- summary는 표의 간략한 설명을 써주면 됨 -->
														<caption>첨부화일 표</caption>
														<colgroup>
															<col width="8%">
															<col width="*%">
														</colgroup>
														<thead>
															<tr>
																<th scope="row" class="ce">순번</th>
																<th scope="row">첨부파일</th>
															</tr>
														</thead>
														<tbody>
															<tr id="fileTr1">
																<td class="ce" id="fileTd1"><input name="chkDel1"
																	id="chkDel1" type="checkbox" value="1" title="chkDel1"/>
																</td>
																<td><input type="hidden" name="fileDelYn"  /> <span
																	id="spfile1"> <input type="file" name="file"
																		id="file" title="첨부파일" class="inputData L w100"
																		onkeydown="return false;"
																		onchange="checkFile(this.id)" /> </span>
																</td>
															</tr>
														</tbody>
													</table>
													<span class="p11 orange block mt5">※ 첨부파일은 최대5개까지
														가능합니다. </span>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</form>
				</c:if>
				<div class="btnArea">
					<p class="fl">
						<span class="button medium gray"><a
							href="javascript:self.close()">취소</a> </span>
					</p>
					<p class="fr">
						<span class="button medium"><a href="javascript:;"
							onclick="javascript:ObjcInsertoc()">권리자 이의제기</a> </span>
					</p>
				</div>
			</div>
			<!-- //주요컨텐츠 end -->
		</div>
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights
			reserved.</p>
		<!-- //FOOTER end -->
	</div>
</body>
</html>

