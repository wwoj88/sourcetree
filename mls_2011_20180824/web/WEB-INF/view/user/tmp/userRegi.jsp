<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
 	String userDivs = request.getParameter("userDivs") == null ? "" : request.getParameter("userDivs");
	String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName");
	//String resdCorpNumb = request.getParameter("resdCorpNumb") == null ? "" : request.getParameter("resdCorpNumb");
	String resdCorpNumb1 = request.getParameter("resdCorpNumb1") == null ? "" : request.getParameter("resdCorpNumb1");
	String resdCorpNumb2 = request.getParameter("resdCorpNumb2") == null ? "" : request.getParameter("resdCorpNumb2");

	String corpNumb = request.getParameter("corpNumb") == null ? "" : request.getParameter("corpNumb");

	//String tmpResdCorpNumb = "";
	String tmpCorpNumb = "";
	String userDivsName = "";

	if ("01".equals(userDivs)) {
	  userDivsName = "개인회원";
	  //tmpResdCorpNumb = resdCorpNumb.substring(0,6) + "-" + resdCorpNumb.substring(6,13);
  } else if ("02".equals(userDivs)) {
  	userDivsName = "법인사업자";
  	//tmpResdCorpNumb = resdCorpNumb.substring(0,6) + "-" + resdCorpNumb.substring(6,13);
  	tmpCorpNumb     = corpNumb.substring(0,3) + "-" + corpNumb.substring(3,5) + "-" + corpNumb.substring(5,10);
  } else if ("03".equals(userDivs)) {
  	userDivsName = "개인사업자";
  	tmpCorpNumb     = corpNumb.substring(0,3) + "-" + corpNumb.substring(3,5) + "-" + corpNumb.substring(5,10);
  }
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>회원가입 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--
function goPostNumbSrch() {
  window.open('/user/user.do?method=goPostNumbSrch','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=445');
}

function goIdntConf() {
	var frm = document.form1;
	
	checkForm(frm);
	
	if (frm.userIdnt.value == "") {
		alert("사용하실 아이디를 입력하십시오.");
		frm.userIdnt.focus();
		return;
	} else if(frm.userIdnt.value.length < 5 || frm.userIdnt.value.length > 12){
		alert("아이디는 5자리 이상 12자리 이하로 입력하십시오.");
		frm.userIdnt.focus();
		return;
	}else{
  	var userIdnt = frm.userIdnt.value;
	  window.open('/user/user.do?method=goIdntConf&userIdnt='+userIdnt,'win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=445');
	}
	
}

function insertUser() {
	var frm = document.form1;

  if (frm.userIdChk.value != "Y") {
  	alert("아이디 중복확인을 하시기 바립니다.");
		return;
  }else if (frm.pswd.value == "") {
		alert("비밀번호를 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value.length < 6) {
		alert("비밀번호는 6자리 이상 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value != frm.pswdConf.value) {
		alert("입력하신 비밀번호가 다릅니다. 입력하신 비밀번호를 확인하십시오");
		frm.pswd.focus();
		return;
	}else if (frm.pswd.value == frm.userIdnt.value) {
		alert("아이디와 비밀번호는 동일할수 없습니다.");
		frm.pswd.focus();
		return;
	}else if(!frm.pswd.value.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/)){
        alert("비밀번호는 문자, 숫자, 특수문자의 조합으로  입력해주세요.");
        return;
	}else if (frm.mail.value == "") {
		alert("이메일 주소를 입력하십시오.");
		frm.mail.focus();
		return;
  } else if (frm.zipxNumb.value == "") {
		alert("우편번호를 입력하십시오.");
		frm.zipxNumb.focus();
		return;
  } else if (frm.addr.value == "") {
		alert("주소를 입력하십시오.");
		frm.addr.focus();
		return;
  } else if (frm.detlAddr.value == "") {
		alert("상세주소를 입력하십시오.");
		frm.detlAddr.focus();
		return;
  } else if (frm.moblPhon.value == "") {
		alert("핸드폰번호를 입력하십시오.");
		frm.moblPhon.focus();
		return;
  } else if ((inspectCheckBoxField(frm.mailReceYsno) == false) && (inspectCheckBoxField(frm.smsReceYsno) == false)) {
		alert("수신방법 중 이메일과 핸드폰 하나는 선택하셔야 합니다.");
		frm.mailReceYsno.focus();
		return;
	} else {
		frm.action = "/user/user.do";
  		frm.submit();
	}
}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_9.gif" alt="회원정보" title="회원정보" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/user/user.do?method=goLogin"><img src="/images/2011/content/sub_lnb0901_off.gif" title="로그인" alt="로그인" /></a></li>
					<li id="lnb2"><a href="/user/user.do?method=goPage"><img src="/images/2011/content/sub_lnb0902_off.gif" title="회원가입" alt="회원가입" /></a></li>
					<li id="lnb3"><a href="/user/user.do?method=goIdntPswdSrch"><img src="/images/2011/content/sub_lnb0903_off.gif" title="아이디/비밀번호찾기" alt="아이디/비밀번호찾기" /></a></li>
					</ul>
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb2");
					</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>회원정보</span><em>회원가입</em></p>
					<h1><img src="/images/2011/title/content_h1_0902.gif" alt="회원가입" title="회원가입" /></h1>
					
					<div class="section">
						
						<div class="usr_process">
							<div class="process_box">
								<ul class="floatDiv">
								<li class="fl ml0"><img src="/images/2011/content/process1_off.gif" alt="1단계 약관동의" /></li>
								<li class="fl on"><img src="/images/2011/content/process2_on.gif" alt="2단계 회원정보입력" /></li>
								<li class="fl bgNone pr0"><img src="/images/2011/content/process3_off.gif" alt="3단계 가입완료" /></li>
								</ul>
							</div>
						</div>
						
						<form class="frm" name="form1" method="post" action = "">
												<input name="zip" type="hidden"/>
										<input type="hidden" name="srchAddr">
										<input type="hidden" name="userDivs" value="<%=userDivs%>">
										<input type="hidden" name="userName" value="<%=userName%>">
										<input type="hidden" name="corpNumb" value="<%=corpNumb%>">
										<input type="hidden" name="method" value="goUserInsert">
						<fieldset class="w100">
						<legend>회원가입 폼</legend>
						
						<!-- article str -->
						<div class="article">
							<div class="floatDiv">
								<p class="fl p11 black">( <img alt="" src="/images/2011/common/necessary.gif"> ) 항목은 필수입력사항이므로 빠짐없이 기입하여주시기 바랍니다.</p>
							</div>
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">회원구분</th>
										<td><%=userDivsName%></td>
									</tr>
									<tr>
										<th scope="row">이름/사업자명</th>
										<td><%=userName%></td>
									</tr>
									<tr>
										<th scope="row">주민/법인번호</th>
										<td><%//=tmpResdCorpNumb%>
											<%
											  if ("01".equals(userDivs) || "02".equals(userDivs)) {
										    %>
												<input type="text" name="resdCorpNumb1" id="resdCorpNumb1"  value="<%=resdCorpNumb1%>" title="주민/법인번호(1번째)" class="input" style="border-width:0px; width:39px; text-align:right;" readonly> - <input type="password" autocomplete="off" name="resdCorpNumb2" id="resdCorpNumb2"  value="<%=resdCorpNumb2%>" title="주민/법인번호(2번째)" class="input" style="border-width:0px; width:90px; text-align:left;" readonly>
											<%
											  } 
											%>
											</td>
									</tr>
									<tr>
										<th scope="row">사업자번호</th>
										<td><%=tmpCorpNumb%></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="userIdnt">아이디</label></th>
										<td><input type="text" class="w20" id="userIdnt" name="userIdnt" character="K"> <span class="button small icon"><a href="javascript:goIdntConf();" title="새 창에서 열림">중복확인</a><span class="delete"></span></span>
										<input type="hidden" name="userIdChk">
										</td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="pswd">비밀번호</label></th>
										<td><input type="password" autocomplete="off" class="w20" id="pswd" name="pswd"/><span class="p11 ml10">(특수문자는(!, @, #, $, %, ^, &, *, ?, _, ~)이며 6자리 이상으로 입력하십시오)</span></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="pswdConf">비밀번호 확인</label></th>
										<td><input type="password" autocomplete="off" class="w20" id="pswdConf" name="pswdConf"></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="mail">이메일주소</label></th>
										<td><input type="text" class="w90" id="mail"  name="mail"></td> 
									</tr>
									<tr>
										<th scope="row"><label for="zipxNumb" class="necessary">주소</label></th>
										<td><input type="text" class="w30" readonly="readonly"  id="zipxNumb" /> <span class="button small black"><a href="javascript:goPostNumbSrch();" title="새 창에서 열림">우편번호 찾기</a></span>
											<p class="mt3 line22"><input type="text" class="w80" id="addr" name="addr" /><br /><input name="detlAddr" id="detlAddr" size="88" title="상세주소" type="text" class="w80" /><span class="p11 ml10">나머지 주소</span></p></td>
									</tr>
									<tr>
										<th scope="row"><label for="telxNumb">자택전화번호</label></th>
										<td><input type="text" class="w30"  id="telxNumb" name="telxNumb"><span class="p11 ml10">(예 : 02-123-4567)</span></td>
									</tr>
									<tr>
										<th scope="row"><label for="moblPhon" class="necessary">핸드폰번호</label></th>
										<td><input type="text" class="w30" id="moblPhon" name="moblPhon"><span class="p11 ml10">(예 : 010-123-4567)</span></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary">수신방법</label></th>
										<td><input type="checkbox" id="mailReceYsno" name="mailReceYsno" value="Y" title="이메일"><label class="p12 mr10" for="mailReceYsno">이메일</label><input type="checkbox" id="smsReceYsno"  name="smsReceYsno"  value="Y" title="핸드폰"><label class="p12 mr10" for="smsReceYsno">핸드폰</label></td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<div class="white_box">
								<div class="box5">
									<div class="box5_con floatDiv">
										<p class="fl mt5"><img src="/images/2011/content/box_img2.gif" alt="" /></p>
										<div class="fl ml20 mt5">
											<h4>저작권라이선스관리시스템 통합로그인 동의여부</h4>
											<p class="black2 ml10 mt5 line22">현재 저작권찾기 사이트와 저작권라이선스관리시스템(CLMS)는 하나의 아이디로 로그인하여<br /> 다양한 정보를 제공하고자 “통합로그인서비스”를 시행하고 있습니다.</p>
											<p class="blue2 ml10 mt5 line22">회원님 동의에 따라 저작권라이선스관리시스템(CLMS)에 자동회원가입되었습니다.</p>
											<p class="ml10 mt5 line22"><input type="checkbox" class="inputChk" id="clmsAgrYn"  name="clmsAgrYn" value="Y" title="저작권라이선스관리시스템 통합로그인 동의여부" /><label for="clmsAgrYn">동의합니다.</label></p>
										</div>
									</div>
								</div>
							</div>
							
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="/main/main.do">취소</a></span></p>
								<p class="fr"><span class="button medium"><a href="javascript:insertUser();">저장하기</a></span></p>
							</div>
							
						</div>
						<!-- //article end -->
						
						</fieldset>
						</form>
								
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
