<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>회원가입 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript">
<!--
function init() {
	alert("회원가입이 정상적으로 처리되었습니다.");

	frm = document.form1;

	frm.action = "/user/user.do";
 	frm.submit();
}
//-->
</script>	
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_7.png" alt="회원정보" title="" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/user/user.do?method=goLogin">로그인<span>&lt;</span></a></li>
							<li class="active"><a href="/user/user.do?method=goPage">회원가입<span>&lt;</span></a></li>
							<li><a href="/user/user.do?method=goIdntPswdSrch">아이디/비밀번호찾기<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>회원가입</span>&gt;<strong>회원가입 완료</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>회원가입
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<div class="section">
						<p><img src="/images/2010/contents/userGuideImg.gif" alt="Welcome 저작물권리찾기에 오신 것을 환영합니다." title="Welcome 저작물권리찾기에 오신 것을 환영합니다." /></p>
						
						<h4 class="skip">회원가입 완료</h4>
						
						<div class="stepArea">
							<ul>
								<li class="leftBor">1단계 <strong>약관동의</strong><span><img src="/images/2010/common/stepArrBg.gif" alt="" /></span></li>
								<li>2단계 <strong>회원정보</strong><span><img src="/images/2010/common/stepArrOn2.gif" alt="" /></span></li>
								<li class="on rightBorOn">3단계 <strong>가입완료</strong></li>
							</ul>
						</div>
						
						<form name="form1" method="post" class="frm" action="">
						<input type="hidden" name="method" value="goLogin">
						<fieldset class="sch">
							<legend>회원가입 완료</legend>
							<div class="contentsRoundBox mt0 pb20">
								<span class="round lb"></span><span class="round rb"></span>
								<h5>회원가입을 축하합니다.</h5>
								<p class="ce mt10 mb10"><span class="button medium icon"><span class="default"></span><a href="javascript:init();">메인페이지로 이동</a></span></p>
							</div>
						</fieldset>
					</form>
						
					</div>
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
