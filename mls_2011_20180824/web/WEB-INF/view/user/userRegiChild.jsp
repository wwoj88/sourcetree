<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	Map userData = (HashMap) request.getSession().getAttribute( "userData");
	String decodeResult= (String)request.getSession().getAttribute( "decodeResult" );
 	String userDivs = request.getParameter("userDivs") == null ? "" : request.getParameter("userDivs");
	String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName");
	//String resdCorpNumb = request.getParameter("resdCorpNumb") == null ? "" : request.getParameter("resdCorpNumb");
	String resdCorpNumb1 = request.getParameter("resdCorpNumb1") == null ? "" : request.getParameter("resdCorpNumb1");
	String resdCorpNumb2 = request.getParameter("resdCorpNumb2") == null ? "" : request.getParameter("resdCorpNumb2");

	String corpNumb = request.getParameter("corpNumb") == null ? "" : request.getParameter("corpNumb");
	
	String dupInfo = request.getParameter("dupInfo") == null ? "" : request.getParameter("dupInfo");

	//String tmpResdCorpNumb = "";
	String tmpCorpNumb = "";
	String userDivsName = "";

	if ("01".equals(userDivs)) {
	  userDivsName = "개인회원";
	  //tmpResdCorpNumb = resdCorpNumb.substring(0,6) + "-" + resdCorpNumb.substring(6,13);
  } else if ("02".equals(userDivs)) {
  	userDivsName = "법인사업자";
  	//tmpResdCorpNumb = resdCorpNumb.substring(0,6) + "-" + resdCorpNumb.substring(6,13);
  	tmpCorpNumb     = corpNumb.substring(0,3) + "-" + corpNumb.substring(3,5) + "-" + corpNumb.substring(5,10);
  } else if ("03".equals(userDivs)) {
  	userDivsName = "개인사업자";
  	tmpCorpNumb     = corpNumb.substring(0,3) + "-" + corpNumb.substring(3,5) + "-" + corpNumb.substring(5,10);
  }
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>회원가입 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/JavaScript">
<!--
function goPostNumbSrch() {
	var pop = window.open("/jusoPopup.jsp?method=goPostNumbSrch","pop","width=570,height=420, scrollbars=yes, resizable=yes");
}


function goIdntConf() {
	var frm = document.form1;
	
	checkForm(frm);
	
	if (frm.userIdnt.value == "") {
		alert("사용하실 아이디를 입력하십시오.");
		frm.userIdnt.focus();
		return;
	} else if(frm.userIdnt.value.length < 5 || frm.userIdnt.value.length > 12){
		alert("아이디는 5자리 이상 12자리 이하로 입력하십시오.");
		frm.userIdnt.focus();
		return;
	}else{
  	var userIdnt = frm.userIdnt.value;
	  window.open('/user/user.do?method=goIdntConf&userIdnt='+userIdnt,'win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=393');
	}
	
}

function insertUser() {
	var frm = document.form1;

  if (frm.userIdChk.value != "Y") {
  	alert("아이디 중복확인을 하시기 바립니다.");
		return;
  }else if (frm.pswd.value == "") {
		alert("비밀번호를 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value.length < 6) {
		alert("비밀번호는 6자리 이상 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value != frm.pswdConf.value) {
		alert("입력하신 비밀번호가 다릅니다. 입력하신 비밀번호를 확인하십시오");
		frm.pswd.focus();
		return;
	}else if (frm.pswd.value == frm.userIdnt.value) {
		alert("아이디와 비밀번호는 동일할수 없습니다.");
		frm.pswd.focus();
		return;
	}else if(!frm.pswd.value.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/)){
        alert("비밀번호는 문자, 숫자, 특수문자의 조합으로  입력해주세요.");
        return;
	}else if (frm.mail.value == "") {
		alert("이메일 주소를 입력하십시오.");
		frm.mail.focus();
		return;
  } else if (frm.zipxNumb.value == "") {
		alert("우편번호를 입력하십시오.");
		frm.zipxNumb.focus();
		return;
  } else if (frm.addr.value == "") {
		alert("주소를 입력하십시오.");
		frm.addr.focus();
		return;
  } else if (frm.detlAddr.value == "") {
		alert("상세주소를 입력하십시오.");
		frm.detlAddr.focus();
		return;
  } else if (frm.moblPhon.value == "") {
		alert("핸드폰번호를 입력하십시오.");
		frm.moblPhon.focus();
		return;
  } else if ((inspectCheckBoxField(frm.mailReceYsno) == false) && (inspectCheckBoxField(frm.smsReceYsno) == false)) {
		alert("수신방법 중 이메일과 핸드폰 하나는 선택하셔야 합니다.");
		frm.mailReceYsno.focus();
		return;
	} else {
		$("#saveUser").attr("onclick","");
		frm.action = "/user/user.do";
		zipxNumb = frm.zipxNumb.value;
		frm.zip.value = zipxNumb.substring(0,5);
  		frm.submit();
	}
}

function jusoCallBack(roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn){
	// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
	document.form1.addr.value = roadAddrPart1;
	document.form1.detlAddr.value = addrDetail;
	document.form1.zipxNumb.value = zipNo;
}
//-->

$(function(){
	
	//alert("call")

	var decodeResult='${decodeResult}';
	var userName='<%=userName%>';
	if(decodeResult!=userName){
		alert("부모님의 이름이 일치하지 않습니다.");
		location.href="/user/user.do?method=goPage";
	}
})
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/header.jsp" /> --%>
		<!-- 2017주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container" >
			<!-- <div class="container_vis" style="background: url(/images/2012/content/container_vis7.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_7.gif" alt="회원정보" title="회원정보" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div> -->
			<div id="contents" class="content" style="position: relative; left:-62px;">
			
					<!-- 래프 -->
				<div class="con_lf" >
					<div class="con_lf_big_title">회원정보</div>
					<ul class="sub_lf_menu">
						<li><a href="/user/user.do?method=goLogin">로그인</a></li>
						<li><a href="/user/user.do?method=goPage" class="on">회원가입</a></li>
						<li><a href="/user/user.do?method=goIdntPswdSrch">아이디 / 비밀번호 찾기</a></li>
					</ul>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
				<!-- 	<p class="path"><span>Home</span><span>회원정보</span><em>회원가입</em></p>
					<h1><img src="/images/2012/title/content_h1_0702.gif" alt="회원가입" title="회원가입" /></h1> -->
						<div class="con_rt_head">
						<!-- <img src="/images/sub_img/sub_home.png" alt="홈 페이지" /> -->
						&gt;
						회원정보
						&gt;
						<span class="bold">회원가입</span>
					</div>
					<div class="con_rt_hd_title">회원가입</div>
					<br>
					<div class="section">
						
						<div class="usr_process">
							<div class="process_box">
								<ul class="floatDiv">
								<li class="fl" style=" margin-left: 0px;"><img src="/images/2012/content/process1_off.gif" alt="1단계 약관동의(현재단계)" /></li>
								<li class="fl"><img " src="/images/2017/process_14_2_off.jpg" alt="2단계 본인확인" /></li>
								</ul>
							</div>
							
							<div class="process_box">
							<ul class="floatDiv">
								<li class="fl ml0 on" style="margin-top: 7px; margin-left: 0px;"><img src="/images/2017/process_14_3_on.jpg" alt="3단계 부모동의" /></li>
								<li class="fl" style="margin-top: 7px;"><img src="/images/2017/process_14_4_off.jpg" alt="4단계 회원정보입력" /></li>
								<li class="fl bgNone pr0" style="margin-top: 7px;"><img src="/images/2017/process_14_5_off.jpg" alt="5단계 가입완료" /></li>
								</ul>
							</div>
						</div>
						
						<form class="frm" name="form1" method="post" action = "#">
										<input name="zip" type="hidden"/>
										<input type="hidden" name="srchAddr">
										<input type="hidden" name="road_addr_yn" value=""/>
										<input type="hidden" name="userDivs" value="<%=userDivs%>">
										<input type="hidden" name="userName" value="<%=userName%>">
										<input type="hidden" name="corpNumb" value="<%=corpNumb%>">
										<input type="hidden" name="dupInfo" value="<%=dupInfo%>">
										<input type="hidden" name="method" value="goUserInsert">
										<input type="submit" style="display:none;">
						<!-- <fieldset class="w100"> -->
						<legend>회원가입 폼</legend>
						
						<!-- article str -->
						<div class="article">
							<div class="floatDiv">
								<p class="fl p11 black">( <img alt="필수" src="/images/2012/common/necessary.gif"> ) 항목은 필수입력사항이므로 빠짐없이 기입하여주시기 바랍니다.</p>
							</div>
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">회원구분</th>
										<td><%=userDivsName%></td>
									</tr>
									<tr>
										<th scope="row">이름</th>
										<td>${userData.userName}</td>
									</tr>
									<%-- <tr>
										<th scope="row">법인번호</th>
										<td><%//=tmpResdCorpNumb%>
											<%
											  if ("01".equals(userDivs) || "02".equals(userDivs)) {
										    %>
										    <%
										      	if("01".equals(userDivs) && resdCorpNumb1.equals("")){
										      	    //out.print("아이핀");
										      	}else{
										    %> 
												<input type="text" name="resdCorpNumb1" id="resdCorpNumb1"  value="<%=resdCorpNumb1%>" title="주민/법인번호(1번째)" class="input" style="border-width:0px; width:39px; text-align:right;" readonly>-
												<input type="text" name="resdCorpNumb2" id="resdCorpNumb2"  value="<%=resdCorpNumb2%>" title="주민/법인번호(2번째)" class="input" style="border-width:0px; width:90px; text-align:left;" readonly>
											<%
											  } 
											%>
											<%
											  } 
											%>
											</td>
									</tr>
									<tr>
										<th scope="row">사업자번호</th>
										<td><%=tmpCorpNumb%></td>
									</tr> --%>
									<tr>
										<th scope="row"><label class="necessary" for="userIdnt">아이디</label></th>
										<td><input type="text" class="w20" id="userIdnt" name="userIdnt" style='ime-mode:disabled;'  > <span class="button small icon"><a href="#1" onclick="javascript:goIdntConf();" title="새 창에서 열림">중복확인</a><span class="delete"></span></span>
										<input type="hidden" name="userIdChk">
										</td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="pswd">비밀번호</label></th>
										<td><input type="password" maxlength="30" class="w20" id="pswd" style='ime-mode:disabled;' name="pswd"/><span class="p11 ml10">(특수문자는(!, @, #, $, %, ^, &amp;, *, ?, _, ~)이며 6자리 이상으로 입력하십시오)</span></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="pswdConf">비밀번호 확인</label></th>
										<td><input type="password" maxlength="30" class="w20" id="pswdConf" name="pswdConf"></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="mail">이메일주소</label></th>
										<td><input type="text" class="w90" id="mail"  name="mail"></td> 
									</tr>
									<tr>
										<th scope="row"><label for="zipxNumb" class="necessary">주소</label></th>
										<td><input type="text" class="w30" readonly="readonly"  id="zipxNumb" /> <span class="button small black"><a href="#1" onclick="javascript:goPostNumbSrch();" title="새 창에서 열림">우편번호 찾기</a></span>
											<p class="mt3 line22"><input type="text" class="w80" id="addr" title="주소" name="addr" /><br /><input name="detlAddr" id="detlAddr" size="88" title="상세주소" type="text" class="w80" maxlength="50"/><span class="p11 ml10">나머지 주소</span></p></td>
									</tr>
									<!-- 2014.02.18 개인정보 보호 관련 hide
									<tr>
										<th scope="row"><label for="telxNumb">자택전화번호</label></th>
										<td><input type="text" class="w30"  id="telxNumb" name="telxNumb"><span class="p11 ml10">(예 : 02-123-4567)</span></td>
									</tr>
									-->
									<tr>
										<th scope="row"><label for="moblPhon" class="necessary">핸드폰번호</label></th>
										<td><input type="text" class="w30" id="moblPhon" name="moblPhon"><span class="p11 ml10">(예 : 010-123-4567)</span></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary">수신방법</label></th>
										<td><input type="checkbox" id="mailReceYsno" name="mailReceYsno" value="Y" title="이메일"><label class="p12 mr10" for="mailReceYsno">이메일</label><input type="checkbox" id="smsReceYsno"  name="smsReceYsno"  value="Y" title="핸드폰"><label class="p12 mr10" for="smsReceYsno">핸드폰</label></td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							<!-- ********************** CLMS통합로그인 동의여부 주석처리 str**********************
							271줄********************** CLMS통합로그인 동의여부 주석처리 end ********************** -->
							<%
						      	if("01".equals(userDivs) && resdCorpNumb1.equals("")){
						      	    //out.print("아이핀");
						      	}else{
						    %> 
							<div class="white_box">
								<div class="box5">
									<div class="box5_con floatDiv">
										<p class="fl mt5"><img src="/images/2012/content/box_img2.gif" alt="" /></p>
										<div class="fl ml20 mt5">
											<h4>저작권라이선스관리시스템 통합로그인 동의여부</h4>
											<p class="black2 ml10 mt5 line22">현재 권리자찾기 사이트와 저작권라이선스관리시스템(CLMS)는 하나의 아이디로 로그인하여<br /> 다양한 정보를 제공하고자 “통합로그인서비스”를 시행하고 있습니다.</p>
											<p class="blue2 ml10 mt5 line22">회원님 동의에 따라 저작권라이선스관리시스템(CLMS)에 에 자동회원가입 또는 자동로그인 됩니다.</p>
											<p class="ml10 mt5 line22"><input type="checkbox" class="inputChk" id="clmsAgrYn"  name="clmsAgrYn" value="Y" title="저작권라이선스관리시스템 통합로그인 동의여부" /><label for="clmsAgrYn">동의합니다.</label></p>
										</div>
									</div>
								</div>
							</div>
							<%
						      	}
						    %> 
						  
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="/main/main.do">취소</a></span></p>
								<p class="fr"><span class="button medium"><a id="saveUser" href="#1" onclick="javascript:insertUser();">저장하기</a></span></p>
							</div>
							
						</div>
						<!-- //article end -->
						
						<!-- </fieldset> -->
						</form>
								
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
