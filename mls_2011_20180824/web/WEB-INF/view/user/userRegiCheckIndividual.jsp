<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<% 
HttpSession s = request.getSession(true);
s.putValue("NmChkSec","98u9iuhuyg87");
%>


<%@ page language="java" import="Kisinfo.Check.IPINClient" %>
<%@ page language="java" import="NiceID.Check.CPClient" %>
<%
	/********************************************************************************************************************************************
	NICE신용평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
	
	서비스명 : 가상주민번호서비스 (안심본인인증) 서비스
	페이지명 : 가상주민번호서비스 (안심본인인증) 호출 페이지
	*********************************************************************************************************************************************/
    NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();
	
	String sSiteCode1 = "G6840";				//안심본인인증		NICE로부터 부여받은 사이트 코드
    String sSitePassword = "91F92CJ77YO7";		//안심본인인증 	NICE로부터 부여받은 사이트 패스워드
    
    String sRequestNumber = "REQ0000000001";        	// 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로 
														// 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.
    
	sRequestNumber = niceCheck.getRequestNO(sSiteCode1);
  	session.setAttribute("REQ_SEQ" , sRequestNumber);	// 해킹등의 방지를 위하여 세션을 쓴다면, 세션에 요청번호를 넣는다.
  	
   	String sAuthType = "";      	// 없으면 기본 선택화면, M: 핸드폰, C: 신용카드, X: 공인인증서
   	
   	String popgubun 	= "N";		//Y : 취소버튼 있음 / N : 취소버튼 없음
		String customize 	= "";	//없으면 기본 웹페이지 / Mobile : 모바일페이지
		
    // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
    String sReturnUrl = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("checkSuccess"));
    //String sReturnUrl = "http://www.findcopyright.or.kr:8080/CheckPlusSafe/checkplus_success.jsp";//new String(kr.or.copyright.mls.support.constant.Constants.getProperty("checkSuccess"));
    //String sReturnUrl = "http://dev.findcopyright.or.kr/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL
	String sErrorUrl = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("checkFail"));
    //String sErrorUrl = "http://dev.findcopyright.or.kr/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL
    
    // 입력될 plain 데이타를 만든다.
    String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
                        "8:SITECODE" + sSiteCode1.getBytes().length + ":" + sSiteCode1 +
                        "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
                        "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
                        "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
                        "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
                        "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize;
    
    String sMessage = "";
    String sEncData1 = "";
    
    int iReturn = niceCheck.fnEncode(sSiteCode1, sSitePassword, sPlainData);
    if( iReturn == 0 )
    {
        sEncData1 = niceCheck.getCipherData();
    }
    else if( iReturn == -1)
    {
        sMessage = "암호화 시스템 에러입니다.";
    }    
    else if( iReturn == -2)
    {
        sMessage = "암호화 처리오류입니다.";
    }    
    else if( iReturn == -3)
    {
        sMessage = "암호화 데이터 오류입니다.";
    }    
    else if( iReturn == -9)
    {
        sMessage = "입력 데이터 오류입니다.";
    }    
    else
    {
        sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
    }
	

%>

<%
	/********************************************************************************************************************************************
		NICE신용평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
		
		서비스명 : 가상주민번호서비스 (IPIN) 서비스
		페이지명 : 가상주민번호서비스 (IPIN) 호출 페이지
	*********************************************************************************************************************************************/
	
	String sSiteCode				= "D746";			// IPIN 서비스 사이트 코드		(NICE신용평가정보에서 발급한 사이트코드)
	String sSitePw					= "Yagins12";			// IPIN 서비스 사이트 패스워드	(NICE신용평가정보에서 발급한 사이트패스워드)
	
	
	/*
	┌ sReturnURL 변수에 대한 설명  ─────────────────────────────────────────────────────
		NICE신용평가정보 팝업에서 인증받은 사용자 정보를 암호화하여 귀사로 리턴합니다.
		따라서 암호화된 결과 데이타를 리턴받으실 URL 정의해 주세요.
		
		* URL 은 http 부터 입력해 주셔야하며, 외부에서도 접속이 유효한 정보여야 합니다.
		* 당사에서 배포해드린 샘플페이지 중, ipin_process.jsp 페이지가 사용자 정보를 리턴받는 예제 페이지입니다.
		
		아래는 URL 예제이며, 귀사의 서비스 도메인과 서버에 업로드 된 샘플페이지 위치에 따라 경로를 설정하시기 바랍니다.
		예 - http://www.test.co.kr/ipin_process.jsp, https://www.test.co.kr/ipin_process.jsp, https://test.co.kr/ipin_process.jsp
	└────────────────────────────────────────────────────────────────────
	*/
	String sReturnURL				= "";
	
	int port = request.getServerPort();
	String domain = request.getServerName();
	
	if(port == 80 || port == 0){
	    sReturnURL = "https://"+domain+"/iPin/ipin_process.jsp";
	}else{
	    sReturnURL = "https://"+domain+":"+port+"/iPin/ipin_process.jsp";
	}
	
	
	
	/*
	┌ sCPRequest 변수에 대한 설명  ─────────────────────────────────────────────────────
		[CP 요청번호]로 귀사에서 데이타를 임의로 정의하거나, 당사에서 배포된 모듈로 데이타를 생성할 수 있습니다.
		
		CP 요청번호는 인증 완료 후, 암호화된 결과 데이타에 함께 제공되며
		데이타 위변조 방지 및 특정 사용자가 요청한 것임을 확인하기 위한 목적으로 이용하실 수 있습니다.
		
		따라서 귀사의 프로세스에 응용하여 이용할 수 있는 데이타이기에, 필수값은 아닙니다.
	└────────────────────────────────────────────────────────────────────
	*/
	String sCPRequest				= "";
	
	
	
	// 객체 생성
	IPINClient pClient = new IPINClient();
	
	
	// 앞서 설명드린 바와같이, CP 요청번호는 배포된 모듈을 통해 아래와 같이 생성할 수 있습니다.
	sCPRequest = pClient.getRequestNO(sSiteCode);
	
	// CP 요청번호를 세션에 저장합니다.
	// 현재 예제로 저장한 세션은 ipin_result.jsp 페이지에서 데이타 위변조 방지를 위해 확인하기 위함입니다.
	// 필수사항은 아니며, 보안을 위한 권고사항입니다.
	session.setAttribute("CPREQUEST" , sCPRequest);
	
	
	// Method 결과값(iRtn)에 따라, 프로세스 진행여부를 파악합니다.
	int iRtn = pClient.fnRequest(sSiteCode, sSitePw, sCPRequest, sReturnURL);
	
	String sRtnMsg					= "";			// 처리결과 메세지
	String sEncData					= "";			// 암호화 된 데이타
	
	// Method 결과값에 따른 처리사항
	if (iRtn == 0)
	{
	
		// fnRequest 함수 처리시 업체정보를 암호화한 데이터를 추출합니다.
		// 추출된 암호화된 데이타는 당사 팝업 요청시, 함께 보내주셔야 합니다.
		sEncData = pClient.getCipherData();		//암호화 된 데이타
		
		sRtnMsg = "정상 처리되었습니다.";
	
	}
	else if (iRtn == -1 || iRtn == -2)
	{
		sRtnMsg =	"배포해 드린 서비스 모듈 중, 귀사 서버환경에 맞는 모듈을 이용해 주시기 바랍니다.<BR>" +
					"귀사 서버환경에 맞는 모듈이 없다면 ..<BR><B>iRtn 값, 서버 환경정보를 정확히 확인하여 메일로 요청해 주시기 바랍니다.</B>";
	}
	else if (iRtn == -9)
	{
		sRtnMsg = "입력값 오류 : fnRequest 함수 처리시, 필요한 4개의 파라미터값의 정보를 정확하게 입력해 주시기 바랍니다.";
	}
	else
	{
		sRtnMsg = "iRtn 값 확인 후, NICE신용평가정보 개발 담당자에게 문의해 주세요.";
	}

%>



<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>회원가입 | 회원정보 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script src="/js/Function.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
	<script type="text/javascript">
	window.name ="Parent_window";
	
	//안심본인인증(실명인증) 서비스
	function fnPopup2(){
		window.open('', 'popupChk', 'width=450, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
		document.form_chk.target = "popupChk";
		//document.form_chk.action = "/user/user.do?method=goUserAgree";
		document.form_chk.submit();
	}
	
<!--
	//I-Pin 인증 서비스
	function fnPopup(){
		window.open('', 'popupIPIN2', 'width=450, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		document.form_ipin.target = "popupIPIN2";
		document.form_ipin.action = "https://cert.vno.co.kr/ipin.cb";
		document.form_ipin.submit();
	}
-->
</script>
	
	
<script type="text/JavaScript">//<!--


jQuery(function(){
	
	jQuery("input:radio[name=uDivs]").eq(0).attr("checked",true);
	jQuery("input:radio[name=pDivs]").eq(0).attr("checked",true);
	
	jQuery("input[name=userDivs]").val("01");
	jQuery("input[name=personDivs]").val("01");
	
	jQuery("input[name=userName01]").focus();
	
	jQuery(".tab01").click(function(index){
		var div = jQuery(this).attr("id");
		var divIndex = parseInt(div.substr(1,2))-1;
		
		jQuery("input[name=userName]").val("");
		jQuery("input[name=personDivs]").val("");
		jQuery("input[name=resdCorpNumb]").val("");
		jQuery("input[name=corpNumb]").val("");
		
		
		jQuery("#tab11>li").each(function(index){
			if(divIndex == index){
				jQuery(this).addClass("on");
			}else{
				jQuery(this).removeClass("on");
			}
		})
		console.log(div)
		if(div == "a01"){
			jQuery("#box01_01").css("display","block");
			jQuery("#box01_02").css("display","none");
			jQuery("#box02_01").css("display","none");
			jQuery("#box03_01").css("display","none");
			jQuery("#box04_01").css("display","none");
			jQuery("#box05_01").css("display","none");
			
			jQuery("input:radio[name=pDivs]").eq(0).attr("checked",true);
			jQuery("input[name=userDivs]").val("01");
			jQuery("input[name=userName01]").focus();
			jQuery("#radioBox").css("display","block");
		}else if(div == "a02"){
			jQuery("#box01_01").css("display","none");
			jQuery("#box01_02").css("display","none");
			jQuery("#box02_01").css("display","block");
			jQuery("#box03_01").css("display","none");
			jQuery("#box04_01").css("display","none");
			jQuery("#box05_01").css("display","none");

			jQuery("#radioBox").css("display","none");
			
			jQuery("input[name=userDivs]").val("02");
			jQuery("input[name=userName02]").focus();
		}else if(div == "a03"){
			jQuery("#box01_01").css("display","none");
			jQuery("#box01_02").css("display","none");
			jQuery("#box02_01").css("display","none");
			jQuery("#box03_01").css("display","block");
			jQuery("#box04_01").css("display","none");
			jQuery("#box05_01").css("display","none");
			
			jQuery("#radioBox").css("display","none");
			
			jQuery("input[name=userDivs]").val("03");
			jQuery("input[name=userName03]").focus();
		}else if(div == "a04"){
			jQuery("#box01_01").css("display","none");
			jQuery("#box01_02").css("display","none");
			jQuery("#box02_01").css("display","none");
			jQuery("#box03_01").css("display","none");
			jQuery("#box04_01").css("display","block");
			jQuery("#box05_01").css("display","none");
			jQuery("input[name=userDivs]").val("04");
			jQuery("#radioBox").css("display","none");
		}else if(div == "a05"){
			jQuery("#box01_01").css("display","none");
			jQuery("#box01_02").css("display","none");
			jQuery("#box02_01").css("display","none");
			jQuery("#box03_01").css("display","none");
			jQuery("#box04_01").css("display","none");
			jQuery("#box05_01").css("display","block");
			jQuery("input[name=userDivs]").val("05");
			jQuery("#radioBox").css("display","none");
		}
	})
	
	jQuery(":radio").click(function(){
		var div = jQuery(this).val();
		if(div == "p01"){
			jQuery("#box01_01").css("display","block");
			jQuery("#box01_02").css("display","none");
			jQuery("input[name=userName01]").focus();
			jQuery("#radioBox").css("display","block");
			jQuery("input:radio[name=pDivs]").eq(0).attr("checked",true);
		} else if(div == "p02"){
			jQuery("#box01_02").css("display","block");
			jQuery("#box01_01").css("display","none");
			jQuery("input:radio[name=pDivs]").attr("checked",true);
			jQuery("#radioBox").css("display","block");
		}
// 2013.06.12
// header.jsp에 있는 라디오 버튼에 의해 페이지가 강제로 i-pin 선택으로 바뀌게 되어서 수정
// 
/* 			
		else{
			jQuery("#box01_02").css("display","block");
			jQuery("#box01_01").css("display","none");
			jQuery("input:radio[name=pDivs]").eq(1).attr("checked",true);
			jQuery("#radioBox").css("display","block");
		} */
			
	})
})


/*
function init() {
	var frm = document.form1;
	var userDivs = frm.userDivs.value;

	if (userDivs == "01") {
		frm.corpNumb1.disabled = true;
		frm.corpNumb2.disabled = true;
		frm.corpNumb3.disabled = true;
		frm.resdCorpNumb1.disabled = false;
		frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "02") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
  	frm.resdCorpNumb1.disabled = false;
  	frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "03") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
		frm.resdCorpNumb1.disabled = true;
		frm.resdCorpNumb2.disabled = true;
  }
  frm.userName.focus();

  //frm.reset();
}
*/

// 사업자등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginCrnChk(obj){
	// 사업자등록번호 앞자리의 길이를 확인하여 3자리, 2자리가 입력되면 다음 객체로 이동
	if(obj.name == "corpNumb1"){
		if(obj.value.length == 3)	document.form1.corpNumb2.focus();
	} else if(obj.name == "corpNumb2"){
		if(obj.value.length == 2)	document.form1.corpNumb3.focus();
	}
}

// 주민등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginSsnChk(obj){
	// 주민등록번호 앞자리의 길이를 확인하여 6자리가 입력되면 다음 객체로 이동
	if(obj.name == "resdCorpNumb1"){
		if(obj.value.length == 6)	document.form1.resdCorpNumb2.focus();
	}

	// EnterKey 입력시 공인인증서 로그인 수행
	if (event.keyCode == 13) {
		fn_signLogin();
	}
}

function fn_signLogin() {

	var frm = document.form1;
	var userDivs = frm.userDivs.value;
	
/*   if (Trim(frm.userName.value) == "") {
  	alert("이름/법인명을 입력하십시오.");
  	frm.userName.focus();
  	return;
  } else */
 
 if (userDivs == "01") {
	if (Trim(frm.userName01.value) == ""){
		alert("이름을 입력하십시오.");
		frm.userName01.focus();
		return;
	}else if (frm.resdCorpNumb101.value == "") {
  		alert("주민번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb101.focus();
  		return;
  	} else if (frm.resdCorpNumb101.value.length != 6) {
  		alert("주민번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb101.focus();
  		return;
  	} else if (frm.resdCorpNumb201.value == "") {
  		alert("주민번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb201.focus();
  		return;
  	} else if (frm.resdCorpNumb201.value.length != 7) {
  		alert("주민번호 뒷자리는 7자리 입니다.");
  		frm.resdCorpNumb201.focus();
  		return;
  	} else if (!frm.agree.checked){
  		alert("주민등록번호 처리에 동의해 주시기 바랍니다.");
  		frm.agree.focus();
  		return;
  	}
 		ssn1 = frm.resdCorpNumb101.value;
		ssn2 = frm.resdCorpNumb201.value;
		
		frm.resdCorpNumb.value = frm.resdCorpNumb101.value + frm.resdCorpNumb201.value;
		
		frm.resdCorpNumb1.value = frm.resdCorpNumb101.value;
		frm.resdCorpNumb2.value = frm.resdCorpNumb201.value;
		
		frm.userName.value = frm.userName01.value;

		if(ssnCheck(ssn1, ssn2)) {              // 주민번호 체크
			return;
		}
  } else if(userDivs == "02") {
	if (Trim(frm.userName02.value) == ""){
		alert("법인명을 입력하십시오.");
		frm.userName02.focus();
		return;
	}else if (frm.resdCorpNumb102.value == "") {
  		alert("법인번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb102.focus();
  		return;
  	} else if (frm.resdCorpNumb102.value.length != 6) {
  		alert("법인번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb102.focus();
  		return;
  	} else if (frm.resdCorpNumb202.value == "") {
  		alert("법인번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb202.focus();
  		return;
  	} else if (frm.resdCorpNumb202.value.length != 7) {
  		alert("법인번호 앞자리는 7자리 입니다.");
  		frm.resdCorpNumb202.focus();
  		return;
  	} else if (frm.corpNumb102.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb102.focus();
  		return;
    } else if (frm.corpNumb102.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb102.focus();
  		return;
   	} else if (frm.corpNumb202.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb202.focus();
  		return;
    } else if (frm.corpNumb202.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb202.focus();
  		return;
   	} else if (frm.corpNumb302.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb302.focus();
  		return;
    } else if (frm.corpNumb302.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb302.focus();
  		return;
  	}
  	frm.corpNumb.value = frm.corpNumb102.value + frm.corpNumb202.value + frm.corpNumb302.value;
  	frm.resdCorpNumb.value = frm.resdCorpNumb102.value + frm.resdCorpNumb202.value;
  	frm.userName.value = frm.userName02.value;
  	
  	frm.resdCorpNumb1.value = frm.resdCorpNumb102.value;
	frm.resdCorpNumb2.value = frm.resdCorpNumb202.value;

	frm.corpNumb1.value = frm.corpNumb102.value;
	frm.corpNumb2.value = frm.corpNumb202.value;
	frm.corpNumb3.value = frm.corpNumb302.value;

  	if (fncJuriRegNoCheck(frm.resdCorpNumb102.value + frm.resdCorpNumb202.value) == false) {                // 법인번호 체크
  		var conMsg = "법인등록번호("+frm.resdCorpNumb102.value+"-"+frm.resdCorpNumb202.value+")가 올바르지 않습니다.\n정확히 입력하셨는지 한번더 확인해 주시기 바랍니다.\n입력번호 그대로 진행하시겠습니까?"
			var conFlag = false;
			conFlag = confirm(conMsg);
 		  
 		  if( !conFlag){
 		  	frm.resdCorpNumb102.focus();
 		  	return;
 		  }
 		}

		if (check_busino(frm.corpNumb102.value + frm.corpNumb202.value + frm.corpNumb302.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb102.focus();
 		  return;
 		}
  } else if (userDivs == "03") {
	  if (Trim(frm.userName03.value) == ""){
		alert("이름을 입력하십시오.");
		frm.userName03.focus();
		return;
	}else if(frm.corpNumb103.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb103.focus();
  		return;
    } else if (frm.corpNumb103.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb103.focus();
  		return;
   	} else if (frm.corpNumb203.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb203.focus();
  		return;
    } else if (frm.corpNumb203.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb203.focus();
  		return;
   	} else if (frm.corpNumb303.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb303.focus();
  		return;
    } else if (frm.corpNumb303.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb303.focus();
  		return;
  	}
  	
		frm.corpNumb.value = frm.corpNumb103.value + frm.corpNumb203.value + frm.corpNumb303.value;
		
		frm.userName.value = frm.userName03.value;
		

		frm.corpNumb1.value = frm.corpNumb103.value;
		frm.corpNumb2.value = frm.corpNumb203.value;
		frm.corpNumb3.value = frm.corpNumb303.value;
		
		if (check_busino(frm.corpNumb103.value + frm.corpNumb203.value + frm.corpNumb303.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb103.focus();
 		  return;
 		}
  }

  if(userDivs == "01"){
 	frm.method = "post";
	frm.action = "/NameCheck/nc_p.jsp";
	frm.submit();
  }else{
 	frm.method = "post";
	frm.action = "/user/user.do?method=userRegiCheck";
	frm.submit();
  }
  
}

function fn_userDivs() {
	var frm = document.form1;
	var userDivs = frm.userDivs.value;
	
	//alert("click: "+userDivs);

	if (userDivs == "01") {
		frm.corpNumb1.disabled = true;
		frm.corpNumb2.disabled = true;
		frm.corpNumb3.disabled = true;
		frm.resdCorpNumb1.disabled = false;
		frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "02") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
  	frm.resdCorpNumb1.disabled = false;
  	frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "03") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
		frm.resdCorpNumb1.disabled = true;
		frm.resdCorpNumb2.disabled = true;
  } else if (userDivs == "") {
		
	  var flag = confirm('관리자는 관리자시스템에서 회원가입 후 이용이 가능합니다.');
	  
		 if(flag == true){
		    frm.method = "post";
			frm.action = "/admin/main";
			frm.target="_blank";
			frm.submit();	 
			frm.userDivs.value ="01";
			fn_userDivs();	
		 }else if(flag == false){
			frm.userDivs.value ="01";
			fn_userDivs();
		 }
		 frm.target="";
	}
  frm.userName.focus();

  frm.userName.value = "";
  frm.resdCorpNumb1.value = "";
  frm.resdCorpNumb2.value = "";
  frm.corpNumb1.value = "";
  frm.corpNumb2.value = "";
  frm.corpNumb3.value = "";
}

function childInsertPage(){
	var frm = document.form1;
	//var userDivs = frm.userDivs.value;

	frm.method = "post";
	frm.action = "/user/user.do?method=insertChildUser";
	frm.submit();	 
}

$(function(){
	$('#regiBtn1').click();	
	$('#regiBtn1').click();
})

function regiBtnClick(event){
	var id = event.target.id;
	if(id=="regiBtn1"){
		
	}else if(id=="regiBtn2"){
		
	}
}
//
--></script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69621660-2', 'auto');
	  ga('send', 'pageview');
	  
	//관리자 회원가입 팝업
	  function fncGoPop(){
		  window.open('/user/user.do?method=adminGoPop','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=800, height=555');
			return;
	  	//var popUrl = "/user/adminGoPop.do"; //팝업창에 출력될 페이지 URL
	  	//var popOption = "width=1000, height=700, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
	  	//window.open(popUrl, "", popOption);
	  }
</script>
</head>

<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
		<!-- 2017주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			
				<!-- 래프 -->
				<div class="con_lf" style="width: 22%">
					<div class="con_lf_big_title">회원정보</div>
					<ul class="sub_lf_menu">
						<li><a href="/user/user.do?method=goLogin">로그인</a></li>
						<li><a href="/user/user.do?method=goPage" class="on">회원가입</a></li>
						<!-- <li><a href="/user/user.do?method=goIdntPswdSrch">아이디 / 비밀번호 찾기</a></li> -->
						<li><a href="/user/user.do?method=goIdntSrch">아이디 찾기</a></li>
						<li><a href="/user/user.do?method=goPswdSrch">비밀번호 찾기</a></li>
					</ul>
				</div>
				<!-- //래프 -->
				
				<!-- 안심본인인증 start -->
				<!-- 가상주민번호 서비스 팝업 페이지에서 사용자가 인증을 받으면 암호화된 사용자 정보는 해당 팝업창으로 받게됩니다.
					 따라서 부모 페이지로 이동하기 위해서는 다음과 같은 form이 필요합니다. -->
				<form name="frm2" method="post" action="">
					<input type="hidden" name="userDivs" value="01" >
					<input type="hidden" name="requestNumber" value="" >
					<input type="hidden" name="responseNumber" value="" >
					<input type="hidden" name="userName" value="" >
					<input type="hidden" name="dupInfo" value="" >
					<input type="hidden" name="connInfo" value="" >
					<input type="hidden" name="birthDate" value="" >
				</form>
				
				
				<!-- 가상주민번호 서비스 팝업을 호출하기 위해서는 다음과 같은 form이 필요합니다. -->
				<form name="form_chk" method="post" action="">
					<input type="hidden" name="m" value="checkplusSerivce">			<!-- 필수 데이타로, 누락하시면 안됩니다. -->
					<input type="hidden" name="EncodeData" value="<%= sEncData1 %>">	<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
	 			
	 				<input type="hidden" name="param_r1" value="">
					<input type="hidden" name="param_r2" value="">
					<input type="hidden" name="param_r3" value="">
				</form>
				<!-- 안심본인인증 end -->
				
				<!-- 가상주민번호 서비스 팝업을 호출하기 위해서는 다음과 같은 form이 필요합니다. -->
				<form name="form_ipin" method="post" action="">
					<input type="hidden" name="m" value="pubmain">						<!-- 필수 데이타로, 누락하시면 안됩니다. -->
				    <input type="hidden" name="enc_data" value="<%= sEncData %>">		<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
				    <!-- 업체에서 응답받기 원하는 데이타를 설정하기 위해 사용할 수 있으며, 인증결과 응답시 해당 값을 그대로 송신합니다.
				    	 해당 파라미터는 추가하실 수 없습니다. -->
				    <input type="hidden" name="param_r1" value="">
				    <input type="hidden" name="param_r2" value="">
				    <input type="hidden" name="param_r3" value="">
				</form>
				
				<!-- 가상주민번호 서비스 팝업 페이지에서 사용자가 인증을 받으면 암호화된 사용자 정보는 해당 팝업창으로 받게됩니다.
					 따라서 부모 페이지로 이동하기 위해서는 다음과 같은 form이 필요합니다. -->
				<form name="vnoform" method="post" action="">
					<input type="hidden" name="enc_data">								<!-- 인증받은 사용자 정보 암호화 데이타입니다. -->
					
					<!-- 업체에서 응답받기 원하는 데이타를 설정하기 위해 사용할 수 있으며, 인증결과 응답시 해당 값을 그대로 송신합니다.
				    	 해당 파라미터는 추가하실 수 없습니다. -->
				    <input type="hidden" name="param_r1" value="">
				    <input type="hidden" name="param_r2" value="">
				    <input type="hidden" name="param_r3" value="">
				</form>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" style="width: 74%">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						회원정보
						&gt;
						<span class="bold">회원가입</span>
					</div>
					<div class="con_rt_hd_title">회원가입</div>
					<div id="sub_contents_con">
						
						<h2 class="sub_con_h2">회원가입 여부 확인</h2>
						<form class="frm" name="form1" action="#">
						<input type="hidden" name="resdCorpNumb">
						<input type="hidden" name="corpNumb">
						<input type="hidden" name="userDivs">
						<input type="hidden" name="personDivs">
						<input type="hidden" name="userName">
						
						<input type="hidden" name="resdCorpNumb1">
						<input type="hidden" name="resdCorpNumb2">
						
						<input type="hidden" name="corpNumb1">
						<input type="hidden" name="corpNumb2">
						<input type="hidden" name="corpNumb3">
						
						<input type="submit" style="display:none;">
						<!-- <input type="text" value="test"> -->
						
						<!-- 안심본인인증 -->
							<div id="box01_01">
								<!-- <ul class="sub_menu1 w201 mar_tp40">
									<li class="first on"><a href="#1" class="tab01 on" id="a01">개인회원</a></li>
									<li><a href="#1" class="tab01" id="a02">법인사업자</a></li>
									<li><a href="#1" class="tab01" id="a03">개인사업자</a></li>
									<li><a href="#1" class="tab01 last_rt_bor" id="a04">관리자</a></li>
									<li><a href="#1" class="tab01 last_rt_bor" id="a05">14세 이하</a></li>
								</ul>
								
								<p class="clear"></p>
								<div class="login_bg mar_tp20" style="background-color: #f8f8f8;" margin-top: 50px;>
									<br/>
										 <div style="margin-left: 50px;">
										 <img alt="" src="/images/2017/num.jpg"/>&nbsp;&nbsp;회원유형선택<br/><br/>
										   고객님께서 해당하시는 <em>회원유형</em>을 선택해 주세요.<br/>
									            회원유형에 따라 가입절차가 차이가 있으니 본인이 해당하는 경우를 올바르게 선택해 주시기 바랍니다.
									     <br/>
										 </div>
									     
									     <div style="display:inline-block; margin-top: 50px; margin-left: 50px;">
											<img id="regiBtn1" alt="일반회원가입버튼" src="/images/2017/regi1.jpg">
									     </div>
	        							 
	        							 <div style="display:inline-block; margin-left: 50px;">
											<img id="regiBtn2" alt="14세이하 회원가입 버튼" src="/images/2017/regi2.jpg">
									     </div>
								</div> -->
								<div class="mar_tp30" id="radioBox">
									<input type="radio" id="pDivs04" name="pDivs" value="p01" style="vertical-align: -2px;" /> <label for="pDivs04">안심본인인증</label>
									<input type="radio" id="pDivs05" name="pDivs" value="p02" style="vertical-align: -2px;" /> <label for="pDivs05">아이핀(I-Pin)</label>
								</div>
							<div class="login_bg mar_tp20">
								<div class="float_lf pad_lf20"><img src="/images/main/login_01.png" alt="그림" /></div>
								<div class="float_lf mar_lf50 mar_tp20">
									<div class="bold">안심본인인증은 성명, 생년월일, 성별, 연락처 등을 통해 인증하며, <br />한국신용평가(주)의 DB를 사용합니다.</div>
									<div class="align_cen mar_tp20"><a href="javascript:fnPopup2();" class="pop_check">인증하기</a></div>
								</div>
								<p class="clear"></p>
								<div class="mar_tp30" style="border-top:1px solid #dddddd;"></div>
								<div class="login_foot">
									<div class="bold"><img src="/images/main/login_02.png" alt="login_01" />&nbsp;알려드립니다.</div>
									<p class="pad_lf20 mar_tp10">안심본인인증은 한국신용평가(주)를 통해 받고 있으며, 회원가입시 주민등록번호는 <br />권리자찾기사이트에 저장 되지 않습니다.</p>
								</div>
						
							<div class="sub_blue_point_bg mar_tp20">실명확인이 안될 경우</div>
							<div class="mar_tp20 pad_lf10">
								<p>연령이 만18세 이하이거나 외국인의 경우에는 한국신용평가(주) 콜센터에서 실명확인 등록신청을 해주시기 바랍니다.</p>
								<div class="mar_tp30">
									<span class="bold">- 내국인 실명 확인등록</span>
									<a href="https://www.namecheck.co.kr/front/personal/register_howtoonline.jsp?menu_num=1&amp;page_num=0&amp;page_num_1=1" style="color: blue;" title="내국인 실명 확인 등록 바로가기">[바로가기]</a>
								</div>
								<div class="mar_tp5">
									<span class="bold">- 외국인 실명 확인등록</span>
									<a href="https://www.namecheck.co.kr/front/personal/register_foreigner.jsp?menu_num=1&amp;page_num=0&amp;page_num_1=3" style="color: blue;" title="외국인 실명 확인 등록 바로가기">[바로가기]</a>
								</div>
								<div class="mar_tp30">
									<span class="bold">· 전화문의 :</span>
									<span>(국번없이)1600-1522 (통화가능시간 : 평일09시 ~ 12시, 13시 ~ 18시)토요일 및 공휴일 휴무</span>
								</div>
								<div class="mar_tp5">
									<span class="bold">· 메일문의 :</span>
									<span>callcenter@ksinfo.com</span>
								</div>
								<div class="mar_tp5">
									<span class="bold">· 팩스보내실곳 :</span>
									<span>02-3711-4818</span>
								</div>
								<div class="mar_tp5">
									<span class="bold">· 팩스 및 이메일 업무시간 :</span>
									<span>평일 오전 9시 ~ 오후 9시 (설날, 추석 당일 제외한 연중 처리)</span>
								</div>
							</div>
						</div> 
						</div>
						<!-- //안심본인인증  -->
						<!-- 아이핀 인증 -->
							<div id="box01_02" style="display: none;">
								<!-- <ul class="sub_menu1 w201 mar_tp40">
									<li class="first on"><a href="#1" class="tab01 on" id="a01">개인회원</a></li>
									<li><a href="#1" class="tab01" id="a02">법인사업자</a></li>
									<li><a href="#1" class="tab01" id="a03">개인사업자</a></li>
									<li><a href="#1" class="tab01 last_rt_bor" id="a04">관리자</a></li>
									<li><a href="#1" class="tab01 last_rt_bor" id="a05">14세 이하</a></li>
								</ul> -->
								<p class="clear"></p>
								<div class="mar_tp30" id="radioBox">
									<input type="radio" id="pDivs04" name="pDivs" value="p01" style="vertical-align: -2px;" /> <label for="pDivs04">안심본인인증</label>
									<input type="radio" id="pDivs05" name="pDivs" value="p02" style="vertical-align: -2px;" /> <label for="pDivs05">아이핀(I-Pin)</label>
								</div>
							<div class="login_bg mar_tp20">
								<div class="float_lf pad_lf20"><img src="/images/main/login_01.png" alt="그림" /></div>
								<div class="float_lf mar_lf50 mar_tp20">
									<div class="bold">아이핀(I-Pin)은 주민등록번호를 사용하지 않고 개인식별번호를 이용하여 <br/>본인임을 확인할 수 있는 방법 입니다..</div>
									<div class="align_cen mar_tp20"><a href="javascript:fnPopup();" class="pop_check">인증하기</a></div>
								</div>
								<p class="clear"></p>
								<div class="mar_tp30" style="border-top:1px solid #dddddd;"></div>
								<div class="login_foot">
									<div class="bold"><img src="/images/main/login_02.png" alt="login_01" />&nbsp;알려드립니다.</div>
									<p class="pad_lf20 mar_tp10">아이핀(I-Pin) 인증으로 가입시 I-Pin 인증기관을 통해 실명인증을 받고 있으며, 회원가입시 주민등록번호는 권리자찾기사이트에 저장되지 않습니다.</p>
								</div>
							</div>
						</div>
						<!-- //아이핀 인증 -->
						
						
						
						</form>
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
</body>
</html>
