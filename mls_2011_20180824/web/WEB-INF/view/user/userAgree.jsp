<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
 	String userDivs = request.getParameter("userDivs") == null ? "" : request.getParameter("userDivs");
	String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName");
	//String resdCorpNumb = request.getParameter("resdCorpNumb") == null ? "" : request.getParameter("resdCorpNumb");
	String resdCorpNumb1 = request.getParameter("resdCorpNumb1") == null ? "" : request.getParameter("resdCorpNumb1");
	String resdCorpNumb2 = request.getParameter("resdCorpNumb2") == null ? "" : request.getParameter("resdCorpNumb2");
	String corpNumb = request.getParameter("corpNumb") == null ? "" : request.getParameter("corpNumb");
	String dupInfo = request.getParameter("dupInfo") == null ? "" : request.getParameter("dupInfo");
%>
<html lang="ko">
<head>
<title>회원가입 | 회원정보 | 권리자찾기</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/JavaScript">
<!--

function goPostNumbSrch() {
	  window.open('/user/user.do?method=goPostNumbSrch','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=950, height=860');
	}
		
function agreeCheck(){
	var frm = document.form1;

	if(!frm.agree.checked){
		alert("이용약관을 읽어보시고, 동의해 주시기 바랍니다.");
	} else {
 	  frm.method = "post";
  	frm.action = "/user/user.do?method=goUserRegi";
  	frm.submit();
  }
}


function agreeCheck(){
	
	 var frm = document.form1;
	
	 if(frm.check01[0].checked && frm.check02[0].checked && frm.check03[0].checked  ){
	  frm.method = "post";
  	frm.action = "/user/user.do?method=goUserRegi";
  	frm.submit();
	  return;

	 }
	 /*else if(frm.check01[0].checked){
	  alert("개인정보 수집에 동의해 주세요.");
	  return;

	 }else if(frm.check02[0].checked){
	  alert("이용약관에 동의해 주세요.");
	  return;

	 }else if(frm.check01[1].checked && frm.check02[1].checked){
	  alert("약관에 동의 하셔야 회원가입이 가능합니다.");
	  location.href="/main.asp";
	  return;

	 }*/
	 else{
	  alert("약관에 동의 하셔야 회원가입이 가능합니다.");
	  return;
	 }
}
 
 
function getSelectedRadio(buttonGroup) {
   if (buttonGroup[0]) {
      for (var i=0; i<buttonGroup.length; i++) {
         if (buttonGroup[i].checked) {
            return i
         }
      }
   } else {
      if (buttonGroup.checked) { return 0; }
   }
   return -1;
}

function getSelectedRadioValue(buttonGroup) {
    var i = getSelectedRadio(buttonGroup);
   if (i == -1) {
      return "";
   } else {
      if (buttonGroup[i]) {
         return buttonGroup[i].value;
      } else {
         return buttonGroup.value;
      }
   }
} // Ends the "getSelectedRadioValue" function
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/header.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			
			<div id="contents" style="position: relative; left:-62px;">
			
				<!-- 래프 -->
				<div class="con_lf" >
					<h2><div class="con_lf_big_title">회원정보</div></h2>
					<ul class="sub_lf_menu">
						<li><a href="/user/user.do?method=goLogin">로그인</a></li>
						<li><a href="/user/user.do?method=goPage" class="on">회원가입</a></li>
						<li><a href="/user/user.do?method=goIdntPswdSrch">아이디 / 비밀번호 찾기</a></li>
					</ul>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
				
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						회원정보
						&gt;
						<span class="bold">회원가입</span>
					</div>
					<div class="con_rt_hd_title">회원가입</div>
					<div id="sub_contents_con">
					
					<div class="section">
						
						<div class="usr_process">
							<div class="process_box">
								<ul class="floatDiv">
								<li class="fl ml0 on"><img src="/images/2012/content/process1_on.png" alt="1단계 약관동의(현재단계)" /></li>
								<li class="fl"><img src="/images/2012/content/process2_off.gif" alt="2단계 회원정보입력" /></li>
								<li class="fl bgNone pr0"><img src="/images/2012/content/process3_off.gif" alt="3단계 가입완료" /></li>
								</ul>
							</div>
						</div>
						<form name="form1" class="frm" action="#">
							<input type="hidden" name="userDivs" value="<%=userDivs%>">
							<input type="hidden" name="userName" value="<%=userName%>">
							<!-- input type="hidden" name="resdCorpNumb" value="<%//=resdCorpNumb%>"-->
							<input type="hidden" name="resdCorpNumb1" value="<%=resdCorpNumb1%>">
							<input type="hidden" name="resdCorpNumb2" value="<%=resdCorpNumb2%>">
							<input type="hidden" name="corpNumb" value="<%=corpNumb%>">
							<input type="hidden" name="dupInfo" value="<%=dupInfo%>">
							<input type="submit" style="display:none;">
							
						<h2>이용약관</h2>
						
						<div class="white_box mt0">
							<div class="box5">
								<div class="box5_con">
									<div class="stipulation h150" tabindex="0">
										<jsp:include page="/include/2010/stipulation.txt"/>
									</div>
									<!--
									<p class="ce mt5"><input type="checkbox" id="yes" name="agree" value="Y" /><label for="yes">위 약관에 동의합니다.</label></p>
									-->
									<p class="rgt mt15 vmid">
										이용약관에 동의하십니까?
										&nbsp;
										<input type="radio" id="yes01" name="check01"/><label for="yes01">동의함 </label> &nbsp;         
										<input type="radio" id="no01" name="check01" /><label for="no01">동의하지 않음 </label>
									</p>
								</div>
							</div>
						</div>
						
						<br/>
						
						<h2>개인정보 수집 및 이용에 대한 안내</h2>
						
						<div class="white_box mt0">
							<div class="box5">
								<div class="box5_con">
									<div class="stipulation h150" tabindex="0">
									
									<h6>[수집하려는 개인정보의 항목]<br><br>

									회원가입시 수집하는 개인정보의 범위<br><br>  
									- 개인회원 필수 항목 : 아이디, 패스워드, 이메일주소, 주소, 핸드폰번호, 수신방법<br>
									
									- 사업자회원 필수 항목 : 아이디, 패스워드, 회사명(한글), 대표자명(한글), 사업자등록번호, 업태, 업종,<br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									우편번호, 사업장주소, 담당자명, 담당자전화번호, 담당자메일주소<br>
									
									<br>
									서비스 이용시 자동 수집되는 개인 정보의 범위<br><br>
									홈페이지의 개선과 보완을 위한 통계분석·이용자와 웹사이트 간의 원활한 의사소통 등을 위하여 사용하기 위해<br>
									다음의 정보는 자동으로 수집·저장됩니다.<br>
									
									- 이용자의 인터넷서버 도메인명과 우리 웹사이트를 방문할 때 거친 웹 사이트의 주소, 방문 일시 등<br>
									- 이용자의 브라우저 종류 및 OS <br>
									
									<br>
									[개인정보의 수집·이용 목적]<br><br>
									
									- 서비스 제공<br>
									&nbsp;&nbsp;· 음악, 어문, 방송 등 저작물 정보 제공 등 서비스 제공<br>
									
									- 회원 가입 및 관리<br>
									&nbsp;&nbsp;· 회원제 서비스 이용에 따른 본인확인, 개인 식별, 불량회원의 부정이용 방지와 비인가 사용방지, 
									<br>
									공지사항 전달 등<br>
									
									<br>
									[개인정보의 보유 및 이용 기간 : <font style="font-size: 2em; color: red; font-weight: bold;"><u>3년</u></font>]<br><br>
									
									원칙적으로, 회원탈퇴 혹은 개인정보 처리목적이 달성되면 해당 개인 정보를 지체 없이 파기합니다.<br>
									
									<br>
									[개인 정보 제공 및 위탁 처리]<br><br>
									
									위원회에서 관리하는 개인 정보의 처리를 다른 전문기관에 위탁하는 경우는 없습니다. <br>
									
									<br>
									[동의를 거부할 권리가 있다는 사실 및 동의 거부에 따른 불이익이 있는 경우에는 그 불이익의 내용]<br><br>
									
									회원께서는 개인정보 수집 동의를 거부하실수 있으며, 다만 이 경우 회원가입이 제한됩니다.<br>
									
									</h6>
									
										<%-- <jsp:include page="/include/2012/stipulation02.txt"/> --%>
									</div>
									<!--
									<p class="ce mt5"><input type="checkbox" id="yes" name="agree" value="Y" /><label for="yes">위 약관에 동의합니다.</label></p>
									-->
									<p class="rgt mt15 vmid">
										개인정보의 수집 및 이용에 동의하십니까?
										&nbsp;
										<input type="radio" id="yes02" name="check02" /><label for="yes02">동의함 </label>  &nbsp;        
										<input type="radio" id="no02" name="check02" /><label for="no02">동의하지 않음 </label>
									</p>
								</div>
							</div>
						</div>
						
						<br/>
						
						<h2>고유식별정보 수집 동의</h2>
						
						<div class="white_box mt0 ">
							<div class="box5">
								<div class="box5_con">
									<div class="stipulation h100" tabindex="0">
										<%-- <jsp:include page="/include/2012/stipulation03.txt"/> --%>
										<h6>[수집하려는 고유식별정보의 항목]<br><br>
										 - <font style="font-size: 2em; color: red; font-weight: bold;"><u>주민등록번호 수집</u></font>
										<br><br>
										<h6>[개인정보의 수집·이용 목적]<br><br>
										
										법정허락 승인 신청시 신청자 확인 목적으로 제한적으로 수집합니다. <br>
										<br>
										
										
										<br>
										[고유식별정보의 보유 및 이용 기간<font style="font-size: 2em; color: red; font-weight: bold;"><u>5년</u></font>]<br><br>
										
										원칙적으로, 회원탈퇴 혹은 개인정보 처리목적이 달성되면 해당 개인 정보를 지체 없이 파기합니다.<br>
										
										<br><br>
										[동의를 거부할 권리가 있다는 사실 및 동의 거부에 따른 불이익이 있는 경우에는 그 불이익의 내용]<br><br>
										
										회원께서는 고유식별정보 수집 동의를 거부하실수 있으며, 다만 이 경우 회원가입이 제한됩니다.<br>
									</div>
									<!--
									<p class="ce mt5"><input type="checkbox" id="yes" name="agree" value="Y" /><label for="yes">위 약관에 동의합니다.</label></p>
									-->
									<p class="rgt mt15 vmid">
										고유식별정보의 수집 및 이용에 동의하십니까?
										&nbsp;
										<input type="radio" id="yes03" name="check03" /><label for="yes03">동의함 </label> &nbsp;         
										<input type="radio" id="no03" name="check03" /><label for="no03">동의하지 않음 </label>
									</p>
								</div>
							</div>
						</div>
						
						<div class="btnArea">
							<p class="fl"><span class="button medium gray"><a href="/main/main.do">취소</a></span></p>
							<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:agreeCheck();">확인</a></span></p>
						</div>
						</form>
								
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
		</div>
		<!-- 	</div> -->
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
