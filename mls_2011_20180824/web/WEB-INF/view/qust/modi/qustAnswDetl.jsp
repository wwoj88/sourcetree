<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%
	String bordSeqn = request.getParameter("bordSeqn");
	String menuSeqn = request.getParameter("menuSeqn");
	String threaded = request.getParameter("threaded");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");

	Board boardDTO = (Board) request.getAttribute("board");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>묻고답하기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--
  window.name = "boardView";

  function fn_qustList(){
		var frm = document.form1;
		frm.action = "/board/board.do";
		frm.submit();
	}

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;
		
		frm.target="boardView";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }
	  
  function fn_update(){
	  var frm =document.form1;
		if (frm.pswd.value == "") {
			alert("비밀번호를 입력하십시오.");
			frm.pswd.focus();
			return false;
		} else if(isPwdValidate()){
			frm.method = "post";
			frm.action = "/board/board.do?method=boardView";
			frm.submit();
		}
	}

  function fn_delete(){
	
	  var frm =document.form1;

		if (frm.pswd.value == "") {
			alert("비밀번호를 입력하십시오.");
			frm.pswd.focus();
			return;
		} else if(isPwdValidate()){
			if(confirm("답변내용도 같이 삭제됩니다\n내용을 삭제 하시겠습니까?")){
			  frm.method = "post";
			  frm.action = "/board/board.do?method=deleteQust";
			  frm.submit();
			}
		}
	}

	// 비밀번호 체크
	function isPwdValidate(){
		var bordSeqn = document.form1.bordSeqn.value;
		var menuSeqn = document.form1.menuSeqn.value;
		var pswd     = document.form1.pswd.value;
		var threaded = document.form1.threaded.value;
		var url = "/board/board.do?method=isValidPwd&bordSeqn=" + bordSeqn + "&menuSeqn=" + menuSeqn + "&pswd=" + pswd + "&threaded=" + threaded;
	  if(cfGetBooleanResponseReload(url)){
		    return true;
		} else {
			alert("비밀번호 오류입니다.");
			document.form1.pswd.value = "";
			document.form1.pswd.focus();
		  return false;
		}
  }

	/*
	*  Function.js에 있으나 파이어폭스에서는 인식이 안되서 가져왔음
	*/
	function cfGetBooleanResponseReload(url,params,HttpMethod) {
		var xmlhttp = null;
		if(!HttpMethod){
		    HttpMethod = "GET";
		}
	    if(window.ActiveXObject){
		      try {
		    	  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		      } catch (e) {
		        try {
		        	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP") ;
		        } catch (e2) {
		          return null ;
		        }
			}
		}
		//xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		if (xmlhttp == null) return true;
		xmlhttp.open(HttpMethod, url, false);
		xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
	
		xmlhttp.send(params);
		
		if (xmlhttp.responseText == "true") {
			return true;
		} else {
			return false;
		}
	}
	
	//숫자만 입력
	function only_arabic(t){
		var key = (window.netscape) ? t.which :  event.keyCode;
	
		if (key < 45 || key > 57) {
			if(window.netscape){  // 파이어폭스
				t.preventDefault();
			}else{
				event.returnValue = false;
			}
		} else {
			//alert('숫자만 입력 가능합니다.');
			if(window.netscape){   // 파이어폭스
				return true;
			}else{
				event.returnValue = true;
			}
		}	
	}	  
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(4);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_4.gif" alt="알림마당" title="알림마당" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1"><img src="/images/2011/content/sub_lnb0401_off.gif" title="공지사항" alt="공지사항" /></a></li>
					<li id="lnb2"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1"><img src="/images/2011/content/sub_lnb0402_off.gif" title="자주묻는 질문" alt="자주묻는 질문" /></a></li>
					<li id="lnb3"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=2&amp;page_no=1"><img src="/images/2011/content/sub_lnb0403_off.gif" title="묻고답하기" alt="묻고답하기" /></a></li>
					<li id="lnb4"><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=2&amp;menuSeqn=5&amp;page_no=1"><img src="/images/2011/content/sub_lnb0404_off.gif" title="홍보자료" alt="홍보자료" /></a></li>
					<!--<li id="lnb5"><a href="/board/board.do?method=vocList"><img src="/images/2011/content/sub_lnb0407_off.gif" title="고객의 소리(VOC)" alt="고객의 소리(VOC)" /></a></li>
					--><li id="lnb6"><a href="/main/main.do?method=bannerList"><img src="/images/2011/content/sub_lnb0405_off.gif" title="홍보관" alt="홍보관" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb3");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>알림마당</span><em>묻고답하기</em></p>
					<h1><img src="/images/2011/title/content_h1_0403.gif" alt="묻고답하기" title="묻고답하기" /></h1>
					
					<div class="section">
						<form name="form1" method="post" action = "#">
								<input type="hidden" name="bordSeqn" value="<%=bordSeqn%>">
								<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>">
								<input type="hidden" name="threaded" value="<%=threaded%>">
								<input type="hidden" name="srchDivs" value="<%=srchDivs%>">
								<input type="hidden" name="srchText" value="<%=srchText%>">
								<input type="hidden" name="page_no" value="<%=page_no%>">
								<input type="hidden" name="submitType" value="update">
								<input type="hidden" name="deth" value="<%=boardDTO.getDeth()%>">
								<input type="hidden" name="filePath">
								<input type="hidden" name="fileName">
								<input type="hidden" name="realFileName">
						<!-- 테이블 view Set -->
						<div class="article">
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="묻고답하기내용을 상세조회 합니다.">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">제목</th>
										<td><%=boardDTO.getTite()%></td>
									</tr>
									<tr>
										<th scope="row">작성자</th>
										<td><%=boardDTO.getRgstName()%></td>
									</tr>
									<tr>
										<th scope="row">이메일</th>
										<td><%=boardDTO.getMail() == null ? "" : boardDTO.getMail()%></td>
									</tr>
									<tr>
										<th scope="row"><label for="pswd">비밀번호</label></th>
										<td><input type="password" autocomplete="off" id="pswd" name="pswd" size="15" onkeypress="javascript:only_arabic(event);"></td>
									</tr>
									<tr>
										<th scope="row">내용</th>
										<td>
											<%//=CommonUtil.replaceBr(boardDTO.getBordDesc(),true)%>
											<br/>
											<%=CommonUtil.replaceBr(boardDTO.getBordDescTag(),true)%>
											<br/>
										</td>
									</tr>
									<%
									          	List fileList = (List) boardDTO.getFileList();
									          	int listSize = fileList.size();
									
									          	if (listSize > 0) {
									%>
									<tr>
										<th scope="row">첨부파일</th>
										<td>
									<%

									          	  for(int i=0; i<listSize; i++) {
									          	    Board fileDTO = (Board) fileList.get(i);
									%>	
									<a href="#1" onclick="javascript:fn_fileDownLoad('<%=fileDTO.getFilePath()%>','<%=fileDTO.getFileName()%>','<%=fileDTO.getRealFileName()%>')"><%=fileDTO.getFileName()%></a><br>
									<%
										            }
									%>	
										</td>
									</tr>
									<%
									          }
									%>
								</tbody>
							</table>
							
							<!-- //그리드스타일 -->
							
							<div class="btnArea">
								<p class="lft"><span class="lft button medium gray"><a href="#1" onclick="javascript:fn_qustList();">목록</a></span></p>
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_update();">수정하기</a></span> <span class="button medium"><a href="#1" onclick="javascript:fn_delete();">삭제하기</a></span></p>
							</div>
							
						</div>
						</form>
						<!-- //테이블 view Set -->
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
