<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>

<%
	String menuSeqn = request.getParameter("menuSeqn");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
	String submitChek = request.getParameter("submitChek") == null ? "" : request.getParameter("submitChek");
	User user = SessionUtil.getSession(request);
	
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>묻고답하기 | 권리자찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--
  function fn_qustList(){
		var frm = document.form2;
		frm.action = "/board/board.do";
		frm.submit();
	}
	function showAttach(cnt) {
		var i=0;
		var content = "";
		var attach = document.getElementById("attach")
		attach.innerHTML = "";
		//document.all("attach").innerHTML = "";
		content += '<table>';
		for (i=0; i<cnt; i++) {
			content += '<tr>';
			content += '<td><input type="file" name="attachfile'+(i+1)+'" size="80;" class="inputFile"><\/td>';
			content += '<\/tr>';
		}
		content += '<\/table>';
		//document.all("attach").innerHTML = content;
		attach.innerHTML = content;
	}

	function applyAtch() {
    var ansCnt = document.form1.atchCnt.value;
    showAttach(parseInt(ansCnt));
	}

	function fn_qnaRegi() {
    var frm = document.form1;
	
	
	/********************************************************/
	 // 태그프리 부분
	var str = document.twe.MimeValue();
	frm.bordDesc.value = frm.twe.TextValue;		// 테그제외
	frm.mime_contents.value=str;
	/********************************************************/
	
    if (frm.tite.value == "") {
		  alert("제목을 입력하십시오.");
		  frm.tite.focus();
		  return;
	  } else if (frm.rgstIdnt.value == "") {
		  alert("이름을 입력하십시오.");
		  frm.rgstIdnt.focus();
		  return;
    } else if (frm.mail.value == "") {
		  alert("이메일을 입력하십시오.");
		  frm.mail.focus();
		  return;
    } else if (frm.pswd.value == "") {
		  alert("비밀번호를 입력하십시오.");
		  frm.pswd.focus();
		  return;
    } else if (frm.bordDesc.value == "") {
		  alert("내용을 입력하십시오.");
		  //frm.bordDesc.focus();
		  return;
  	} else {
	  	frm.action = "/board/board.do?method=insertQust";
  	  frm.submit();
	  }
  }
  
function clearStr(s) {
	
	var compareStr = "개인정보보호를위하여내용및첨부파일에주민등록번호,휴대번호,은행계좌번호등을입력하시는것을금하시기바랍니다.";
	var oldStr = Trim2(s); //((s.simpleReplace(" ", "")).simpleReplace("\t","")).simpleReplace("\n","");
			
	if (compareStr == oldStr) {
		document.form1.bordDesc.value="";
	}
	//return document.form1.bordDesc;
}

function Trim2(str){
	var reg = /\s+/g;
	return str.replace(reg,'');
}

function OnInit()
{
	var form = document.form1;
	form.twe.InitDocument();
}	
	
//-->
</script>
<!-- 태그프리 -->
<script language="JScript" FOR="twe" EVENT="OnControlInit()">
	var form = document.form1;
	var htmlStr   = '<P style="TEXT-ALIGN: center" align=center>&nbsp;</P>                                       ';
		htmlStr	 += '<P style="TEXT-ALIGN: center" align=center>&nbsp;</P>                                             ';
		htmlStr	 +=	'<P style="TEXT-ALIGN: center" align=center>개인정보보호를 위하여 내용 및 첨부파일에 </P>    ';
		htmlStr	 +=	'<P style="TEXT-ALIGN: center" align=center>주민등록번호, 휴대번호, 은행계좌번호 등을 </P>   ';
		htmlStr  +=	'<P style="TEXT-ALIGN: center" align=center>입력하시는것을 금하시기바랍니다.</P>             ';
	form.twe.HtmlValue = htmlStr;
</script>
<!-- 태그프리 -->
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2011/header.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(4);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_4.gif" alt="알림마당" title="알림마당" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1"><img src="/images/2011/content/sub_lnb0401_off.gif" title="공지사항" alt="공지사항" /></a></li>
					<li id="lnb2"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1"><img src="/images/2011/content/sub_lnb0402_off.gif" title="자주묻는 질문" alt="자주묻는 질문" /></a></li>
					<li id="lnb3"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=2&amp;page_no=1"><img src="/images/2011/content/sub_lnb0403_off.gif" title="묻고답하기" alt="묻고답하기" /></a></li>
					<li id="lnb4"><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=2&amp;menuSeqn=5&amp;page_no=1"><img src="/images/2011/content/sub_lnb0404_off.gif" title="홍보자료" alt="홍보자료" /></a></li>
					<li id="lnb5"><a href="/board/board.do?method=vocList"><img src="/images/2011/content/sub_lnb0407_off.gif" title="고객의 소리(VOC)" alt="고객의 소리(VOC)" /></a></li>
					<li id="lnb6"><a href="/main/main.do?method=bannerList"><img src="/images/2011/content/sub_lnb0405_off.gif" title="홍보관" alt="홍보관" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb3");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>알림마당</span><em>묻고답하기</em></p>
					<h1><img src="/images/2011/title/content_h1_0403.gif" alt="묻고답하기" title="묻고답하기" /></h1>
					
					<div class="section">
						<form name="form1" method="post" enctype="multipart/form-data" action="#">
									<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>" />
									<input type="hidden" name="srchDivs" value="<%=srchDivs%>" />
									<input type="hidden" name="srchText" value="<%=srchText%>" />
									<input type="hidden" name="page_no" value="<%=page_no%>" />
									<input type="hidden" name="rgstIdnt" value="<%=sessUserIdnt%>" />
									<input type="hidden" name="bordDesc">
									<input type="hidden" name="mime_contents">
									<input type="submit" style="display:none;">
						<!-- 테이블 view Set -->
						<div class="article">
							<p class="black2 p11 mb5">( <img src="/images/2011/common/necessary.gif" alt="" /> ) 항목은 필수입력사항이므로 빠짐없이 기입하여 주시기 바랍니다.</p>
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="regi1" class="necessary">제목</label></th>
										<td><input type="text" id="regi1" class="w90" name="tite" /></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="rgstIdnt">이름</label></th>
										<td>
											<c:set var="rgstName" value="<%=sessUserName%>" />
											<input type="text" class="w40" id="rgstIdnt" name="rgstName"
											<c:if test="${rgstName != null}" >value="<%=sessUserName%>"</c:if>
											<c:if test="${rgstName == null }" >value=""</c:if>
											 />
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="regi3">이메일</label></th>
										<td><input type="text" id="regi3" class="w90" name="mail" /></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="pswd">비밀번호</label></th>
										<td><input type="password"  class="w40" name="pswd" id="pswd" size="15"></td>
									</tr>
									<!-- 
									<tr>
										<th scope="row"><label for="htmlYsno">HTML사용여부</label></th>
										<td><label><input type="checkbox" class="inputChk" id="htmlYsno" name="htmlYsno" /> HTML사용</label></td>
									</tr>
									 -->
									<tr>
										<th scope="row"><label class="necessary">내용</label></th>
										<td height="350">
											<script type="text/javascript" src="/editor/tweditor.js"></script>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="inputFile">첨부파일</label></th>
										<td>
											<div id="attach">
												<table>
												  <tr>
												   	<td><input type="file" name="attachfile0" size="80" class="inputFile" id="inputFile"></td>
												  </tr>
												</table>
											</div>
										   	<!--<input type="text" class="w50" id="regi5" name="attachfile0" /> <span class="button small black"><a href="">찾아보기</a></span>-->
										   	<p class="mt5">
												<select name="atchCnt" id="atchCnt" title="첨부파일수" onChange="javascript:applyAtch();" style="width: 100px;">
													<c:forEach begin="1" end="10" step="1" varStatus="cnt">
														<option value="<c:out value="${cnt.count}"/>"><c:out value="${cnt.count}"/></option>
													</c:forEach>
												</select>
											</p>
										</td>
									</tr>
								</tbody>
							</table>
							
							<!-- //그리드스타일 -->
							
							<div class="btnArea">
								<p class="fl"><span class="button medium gray" title="목록으로 이동합니다."><a href="#1" onclick="javascript:fn_qustList();">목록</a></span></p>
								<p class="fr"><span class="button medium" title="등록된 내용을 저장합니다."><a href="#1" onclick="javascript:fn_qnaRegi();">저장하기</a></span></p>
							</div>
							
						</div>
						</form>
						<form name="form2" method="post" action="#">
							<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>" />
							<input type="hidden" name="srchDivs" value="<%=srchDivs%>" />
							<input type="hidden" name="srchText" value="<%=srchText%>" />
							<input type="hidden" name="page_no" value="<%=page_no%>" />
							<input type="submit" style="display:none;">
						</form>
						<!-- //테이블 view Set -->
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2011/footer.jsp" /> --%>
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
