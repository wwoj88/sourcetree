<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>

<%@ page import="kr.or.copyright.mls.support.dto.MailInfo"%>
<%@ page import="kr.or.copyright.mls.support.util.MailManager"%>
<%@ page import="kr.or.copyright.mls.support.util.MailMessage"%>
<%@ page import="kr.or.copyright.mls.support.constant.Constants"%>
<%
	String iResult = (Integer)request.getAttribute("iResult")+"";
%>

<%
	
	if(iResult.equals("1")) {
		
		String bordTitle = (String) request.getAttribute("title");
		
		// 메일발송
		MailInfo mailInfo = new MailInfo();
		
		mailInfo.setFrom(Constants.SYSTEM_MAIL_ADDRESS);  //발신인 주소 - 시스템 대표 메일
		mailInfo.setFromName(Constants.SYSTEM_NAME);
		mailInfo.setHost(Constants.MAIL_SERVER_IP);
		
		mailInfo.setEmail( new String(kr.or.copyright.mls.support.constant.Constants.getProperty("bord_mailto_addr").getBytes("ISO-8859-1"), "EUC-KR") );
		mailInfo.setEmailName( new String(kr.or.copyright.mls.support.constant.Constants.getProperty("bord_mailto_name").getBytes("ISO-8859-1"), "EUC-KR") );
		
		mailInfo.setSubject(new String(kr.or.copyright.mls.support.constant.Constants.getProperty("bord_mailto_subj").getBytes("ISO-8859-1"), "EUC-KR") );
		
		StringBuffer  message = new StringBuffer();
	
		message.append("<div class=\"mail_title\"></div>");
		message.append("<div class=\"mail_contents\"> ");
		message.append("	<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> ");
		message.append("		<tr> ");
		message.append("			<td> ");
		message.append("				<table width=\"100%\" border=\"0\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\"> ");
	
		message.append("					<tr> ");
	  	message.append("						<td align=\"center\"> ");
	    
		message.append("<br/>");
		message.append("<b>"+new String(kr.or.copyright.mls.support.constant.Constants.getProperty("bord_mailto_subj").getBytes("ISO-8859-1"), "EUC-KR")+"</b>");
		
		message.append("<br/>");
		message.append( "<br/><br/>");
		message.append( "<b>제목 : <u>"+bordTitle+"</u></b>");
		message.append("<br/>");
		message.append("<br/>");
		message.append( "<b>상세내용은 권리자찾기사이트에서 확인 가능합니다.</b>");
	    
		message.append("						</td> ");
		message.append("					</tr> ");
		message.append("				</table> ");
		   
		message.append("			</td> ");
		message.append("		</tr> ");
		message.append("	</table> ");
		message.append("</div> ");
	    
		mailInfo.setMessage(new String(MailMessage.getChangeInfoMailText(message.toString())));
		
		MailManager manager = MailManager.getInstance();
		mailInfo = manager.sendMail(mailInfo);
		
		if(mailInfo.isSuccess()){
	  	  System.out.println("1 >>>>>>>>>>>> 묻고답하기 관리자 메일발송 message success :"+mailInfo.getEmail() );
	  	}else{
	  	  System.out.println("1 >>>>>>>>>>>> 묻고답하기 관리자 메일발송 message false   :"+mailInfo.getEmail() );
	  	}
		
		
		mailInfo.setEmail("kammiya@hanmail.net");
		mailInfo.setEmailName("김소라" );
		
		mailInfo = manager.sendMail(mailInfo);
		
		if(mailInfo.isSuccess()){
	  	  System.out.println("2 >>>>>>>>>>>> 묻고답하기 관리자 메일발송 message success :"+mailInfo.getEmail() );
	  	}else{
	  	  System.out.println("2 >>>>>>>>>>>> 묻고답하기 관리자 메일발송 message false   :"+mailInfo.getEmail() );
	  	}
	}
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>알림마당(묻고답하기) | 권리자찾기</title>
<script type="text/JavaScript">
<!--
function init() {
	
	var frm = document.form1;
	
	if( '1' == '<%=iResult%>') {
		alert("정상적으로 등록되었습니다.");
		
		frm.page_no.value = 1;
	 	frm.submit();
	} else {
		alert("등록실패하였습니다.");
		history.back();
	}
}
//-->
</script>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
</head>
<body onLoad="init();">
	<form name="form1">
    <input type="hidden" name="page_no" value="1">
    <input type="hidden" name="menuSeqn" value="2">
    <input type="submit" style="display:none;">
  </form>
</body>
</html>
