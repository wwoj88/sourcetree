<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="kr.or.copyright.mls.worksPriv.model.WorksPriv"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import = "kr.or.copyright.mls.support.util.DateUtil" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%
	String crId = request.getParameter("crId");
	WorksPriv worksPrivDTO = (WorksPriv)request.getAttribute("worksPriv");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
	User user = SessionUtil.getSession(request);
	String srchTitle = request.getParameter("srchTitle") == null ? "" : request.getParameter("srchTitle");
	String srchGenreDivs = request.getParameter("srchGenreDivs") == null ? "" : request.getParameter("srchGenreDivs");
	String srchStatDivs = request.getParameter("srchStatDivs") == null ? "" : request.getParameter("srchStatDivs");

	String srchStartDate = request.getParameter("srchStartDate") == null ? "" : request.getParameter("srchStartDate");
	String srchEndDate = request.getParameter("srchEndDate") == null ? "" : request.getParameter("srchEndDate");
	
	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>나의 저작물 | 권리자찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/Function.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--
window.name = "worksPrivView";

function fn_worksPrivList(){
	var frm = document.form1;
	frm.action = "/worksPriv/worksPriv.do";
	frm.submit();
}
function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;
		
		frm.target="worksPrivView";
		frm.action = "/worksPriv/worksPriv.do?method=fileDownLoad";
		frm.submit();
  }
function fn_update(){
	var frm =document.form1;
	frm.method = "post";
	frm.action = "/worksPriv/worksPriv.do?method=goView";
	frm.submit();
}

function fn_delete(){
	var frm = document.form1;
	if(confirm("삭제 하시겠습니까?")){
		frm.method = "post";
		frm.action = "/worksPriv/worksPriv.do?method=goDelete";
		frm.submit();
	}
}
//-->
</script>
</head>
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2011/header.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_8.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<jsp:include page="/include/2011/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb2");
				</script>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>저작권콘텐츠</span><em>나의 저작물</em></p>
					<h1><img src="/images/2011/title/content_h1_0803.gif" alt="나의 저작물" title="나의 저작물" /></h1>
					
					<div class="section">
						<form name="form1" method="post" action = "#">
								<input type="hidden" name="crId" value="${worksPriv.crId}">
								<input type="hidden" name="rgstIdnt" value="${worksPriv.rgstIdnt}">
								<input type="hidden" name="page_no" value="<%=page_no%>">
								<input type="hidden" name="submitType" value="update">
								<input type="hidden" name="genreCode" value="<%=worksPrivDTO.getGenreCode()%>">
								<input type="hidden" name="srchTitle" value="<%=srchTitle%>">
								
								<input type="hidden" name="srchGenreDivs" value="<%=srchGenreDivs%>">
								<input type="hidden" name="srchStatDivs" value="<%=srchStatDivs%>">
								
								<input type="hidden" name="srchStartDate" value="<%=srchStartDate%>">
								<input type="hidden" name="srchEndDate" value="<%=srchEndDate%>">
								<input type="hidden" name="filePath">
								<input type="hidden" name="fileName">
								<input type="hidden" name="realFileName">
								<input type="submit" style="display:none;">
						<!-- article str -->
						<div class="article">
							<h2>저작물정보</h2>
							
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="20%">
								<col width="30%">
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">분야</th>
										<td colspan="3">${worksPriv.genreName}</td>
									</tr>
									<tr>
										<th scope="row">종류</th>
										<td>${worksPriv.kindName}</td>
										<th scope="row">창작년도</th>
										<td>${worksPriv.creaYear}</td>
									</tr>
									<tr>
										<th scope="row">저작물명</th>
										<td colspan="3">${worksPriv.title}</td>
									</tr>
									<tr>
										<th scope="row">공표매체</th>
										<td colspan="3">${worksPriv.publCodeName}, ${worksPriv.publName}</td>
									</tr>
									<tr>
										<th scope="row">공표일자</th>
										<td colspan="3"><%=DateUtil.toBaseDate(worksPrivDTO.getPublYmd())%></td>
									</tr>
									<tr>
										<th scope="row">내용</th>
										<td colspan="3">
										<%=CommonUtil.replaceBr(worksPrivDTO.getWorksDesc(),true)%>
										</td>
									</tr>
									<tr>
										<th scope="row">저작물확인</th>
										<td colspan="3">
											<span class="topLine2"></span>
											<!-- 그리드스타일 -->
											<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
												<colgroup>
												<col width="20%">
												<col width="*">
												</colgroup>
												<tbody>
													<tr>
														<th scope="row">URL</th>
														<td><a href="${worksPriv.worksUrl}" target="_blank">${worksPriv.worksUrl}</a></td>
													</tr>
													<%
										          	List fileList = (List) worksPrivDTO.getFileList();
										          	int listSize = fileList.size();
										
										          	if (listSize > 0) {
													%>
													<tr>
														<th scope="row">파일첨부</th>
														<td>
													<%
				
													          	  for(int i=0; i<listSize; i++) {
													          	    WorksPriv fileDTO = (WorksPriv) fileList.get(i);
													%>	
													<a href="#1" onclick="javascript:fn_fileDownLoad('<%=fileDTO.getFilePath()%>','<%=fileDTO.getFileName()%>','<%=fileDTO.getRealFileName()%>')"><%=fileDTO.getFileName()%></a><br />
													<%
														            }
													%>	
														</td>
													</tr>
													<%
													          }
													%>
												</tbody>
											</table>
											<!-- //그리드스타일 -->
										</td>
									</tr>
									<tr>
										<th scope="row">이용허락 표시</th>
										<td colspan="3">
										<c:choose>
											<c:when test="${worksPriv.cclCode eq 1}">
												이용금지
											</c:when>
											<c:when test="${worksPriv.cclCode eq 2}">
												<img src="/images/2011/ccl/by.jpg" alt="저작자표시" title="저작자표시" />
												<span class="ml10">저작자표시(BY)</span>
											</c:when>
											<c:when test="${worksPriv.cclCode eq 3}">
												<img src="/images/2011/ccl/by.jpg" alt="저작자표시" title="저작자표시" />
												<img src="/images/2011/ccl/nc.jpg" alt="비영리" title="비영리" />
												<span class="ml10">저작자표시-비영리(BY-NC)</span>
											</c:when>
											<c:when test="${worksPriv.cclCode eq 4}">
												<img src="/images/2011/ccl/by.jpg" alt="저작자표시" title="저작자표시" />
												<img src="/images/2011/ccl/nd.jpg" alt="변경금지" title="변경금지" />
												<span class="ml10">저작자표시-변경금지(BY-ND)</span>
											</c:when>
											<c:when test="${worksPriv.cclCode eq 5}">
												<img src="/images/2011/ccl/by.jpg" alt="저작자표시" title="저작자표시" />
												<img src="/images/2011/ccl/sa.jpg" alt="동일조건변경" title="동일조건변경" />
												<span class="ml10">저작자표시-동일조건변경(BY-SA)</span>
											</c:when>
											<c:when test="${worksPriv.cclCode eq 6}">
												<img src="/images/2011/ccl/by.jpg" alt="저작자표시" title="저작자표시" />
												<img src="/images/2011/ccl/nc.jpg" alt="비영리" title="비영리" />
												<img src="/images/2011/ccl/nd.jpg" alt="변경금지" title="변경금지" />
												<span class="ml10">저작자표시-비영리-변경금지(BY-NC-ND)</span>
											</c:when>
											<c:when test="${worksPriv.cclCode eq 7}">
												<img src="/images/2011/ccl/by.jpg" alt="저작자표시" title="저작자표시" />
												<img src="/images/2011/ccl/nc.jpg" alt="비영리" title="비영리" />
												<img src="/images/2011/ccl/sa.jpg" alt="동일조건변경" title="동일조건변경" />
												<span class="ml10">저작자표시-비영리-동일조건변경(BY-NC-SA)</span>
											</c:when>
										</c:choose>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<h2 class="mt20">저작권 정보</h2>
							
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">저작권자</th>
										<td>${worksPriv.coptHodr}</td>
									</tr>
									<tr>
										<th scope="row">내용</th>
										<td>
										<%=CommonUtil.replaceBr(worksPrivDTO.getCoptHodrDesc(),true)%>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<h2 class="mt20">ICN 번호 발급정보</h2>
							
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">발급상태</th>
										<td>${worksPriv.commStatName}</td>
									</tr>
									<c:if test="${worksPriv.commStatCode == 4}">
									<tr>
										<th scope="row">발급거절 사유</th>
										<td>${worksPriv.commStatDesc}</td>
									</tr>
									</c:if>
									<tr>
										<th scope="row">ICN 번호</th>
										<td>
										${worksPriv.commId}
										</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
														
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_worksPrivList();">목록</a></span></p>
								<c:if test="${worksPriv.commStatCode == 1}">
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_update();">수정하기</a></span>
								<span class="button medium"><a href="#1" onclick="javascript:fn_delete();">삭제하기</a></span>
								</p>
								</c:if>
							</div>
						</div>
						<!-- //article end -->
						</form>
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2011/footer.jsp" /> --%>
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
