<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
	String menuSeqn = request.getParameter("menuSeqn");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>참여마당(이벤트) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--
	function goPage(pageNo){
		var frm = document.form1;
		frm.page_no.value = pageNo;
		frm.submit();
	}

	function board_search() {
		var frm = document.form1;
		frm.page_no.value = 1;
		frm.submit();
	}

	function fn_enterCheck(obj){
	  if (event.keyCode == 13) {
		  board_search();
	  }
  }
	
	// 상세화면 오픈
	function boardDetail(eventUrl,bordSeqn,menuSeqn,threaded){
		var frm = document.form1;
		var url = eventUrl;
		var name = '';
		var openInfo = 'target=eveentList, width=650, height=450, scrollbars=yes,toolbar=no,location=no,status=yes,menubar=no,resizable=yes';
		window.open(url, name, openInfo);
		frm.bordSeqn.value = bordSeqn;
		frm.menuSeqn.value = menuSeqn;
		frm.threaded.value = threaded;
		frm.page_no.value = '<%=page_no%>';
		frm.method = "post";
		frm.action = "/board/board.do?method=boardView";
		frm.submit();
	}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(5);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_5.gif" alt="참여마당" title="참여마당" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/worksPriv/worksPriv.do?menuSeqn=1&amp;page_no=1"><img src="/images/2011/content/sub_lnb0501_off.gif" title="개인저작물" alt="개인저작물" /></a></li>
					<li id="lnb2"><a href="/board/board.do?menuSeqn=7&amp;page_no=1"><img src="/images/2011/content/sub_lnb0502_off.gif" title="이벤트" alt="이벤트" /></a></li>
					<li id="lnb3"><a href="/main/main.do?method=goSiteList"><img src="/images/2011/content/sub_lnb0503_off.gif" title="관련사이트안내" alt="관련사이트안내" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb2");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>참여마당</span><em>이벤트</em></p>
					<h1 title="저작물에 대한 주민의식이 필요합니다!"><img src="/images/2011/title/content_h1_0502.gif" alt="이벤트" title="이벤트" /></h1>
					
					<div class="section">
						<!-- 검색 -->
						<form name="form1" action="#" class="sch mt20">
					    <input type="hidden" name="page_no" />
					    <input type="hidden" name="menuSeqn" value="7" />
					    <input type="hidden" name="bordSeqn" />
					    <input type="hidden" name="threaded" />
					    
							<fieldset class="w100">
							<legend>게시판검색</legend>
								<div class="boxStyle">
									<div class="box1 floatDiv">
										<p class="fl mt5 w15"><label for="srchDivs"><img src="/images/2011/content/sch_txt.gif" alt="Search" title="Search" /></label></p>
										<p class="fl w70">
										<select id="srchDivs" name="srchDivs" class="w20">
											<option value="">선택</option>
											<option value="01" <%="01".equals(srchDivs)?"selected='selected'":"" %>>제목</option>
										</select>
										<input name="srchText" value="<%=srchText%>" size="35" onkeyup="fn_enterCheck(this)" title="검색어" class="inputData w40" />
										<span class="button small black"><input type="submit" onclick = "javascript:board_search();" value="검색" /></span></p>
									</div>
									<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
								</div>
							</fieldset>
						</form>
						<!-- //검색 -->
						
						<!-- 테이블 리스트 Set -->
						<div class="section mt20">
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="8%">
								<col width="*">
								<col width="10%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">제목</th>
										<th scope="col">조회수</th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${boardList.totalRow == 0}">
										<tr>
											<td class="ce" colspan="6">등록된 게시물이 없습니다.</td>
										</tr>
								</c:if>
								<c:if test="${boardList.totalRow > 0}">
									<c:forEach items="${boardList.resultList}" var="board">
							        <c:set var="NO" value="${board.TOTAL_CNT}"/>
						        	<c:set var="i" value="${i+1}"/>
									<tr>
										<td class="ce"><c:out value="${NO - i}"/></td>
										<td><a href="#1" onclick="javascript:boardDetail('${board.URL}','${board.BORD_SEQN}','7','${board.THREADED}')">${board.TITE}</a></td>
										<td class="ce">${board.INQR_CONT}</td>
									</tr>
									</c:forEach>
		    					</c:if>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<!-- 페이징 -->
							<div class="pagination">
							<ul>
								<li>
						 		<%-- 페이징 리스트 --%>
								  <jsp:include page="../common/PageList_2011.jsp" flush="true">
									  <jsp:param name="totalItemCount" value="${boardList.totalRow}" />
										<jsp:param name="nowPage"        value="${param.page_no}" />
										<jsp:param name="functionName"   value="goPage" />
										<jsp:param name="listScale"      value="" />
										<jsp:param name="pageScale"      value="" />
										<jsp:param name="flag"           value="M01_FRONT" />
										<jsp:param name="extend"         value="no" />
									</jsp:include>
								</li>	
							</ul>
						</div>
							<!-- //페이징 -->
							
						</div>
						<!-- //테이블 리스트 Set -->
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
