<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
	String menuSeqn = request.getParameter("menuSeqn");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>자주묻는질문 | 알림마당 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css">
<link rel="stylesheet" type="text/css" href="/css/table.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript">
<!--
	function goPage(pageNo){
		var frm = document.form1;
		frm.page_no.value = pageNo;
		frm.submit();
	}

	function board_search() {
		var frm = document.form1;
		frm.page_no.value = 1;
		frm.submit();
	}
	
	function fn_enterCheck(obj){
	  // EnterKey 입력시 공인인증서 로그인 수행
	  if (event.keyCode == 13) {
		  board_search();
	  }
  }

	function boardDetail(bordSeqn,menuSeqn,threaded){
		var frm = document.form1;
		frm.bordSeqn.value = bordSeqn;
		frm.menuSeqn.value = menuSeqn;
		frm.threaded.value = threaded;
		frm.page_no.value = '<%=page_no%>';
		frm.method = "post";
		frm.action = "/board/board.do?method=boardView";
		frm.submit();
  }


(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-69621660-1', 'auto');
ga('send', 'pageview');
//-->
</script>
</head>

<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader6.jsp" /> --%>
		<!-- 2017 주석추가 -->
		<!-- <script type="text/javascript">initNavigation(6);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="contents">
			
			<!-- 래프 -->
				<div class="con_lf">
					<h2><div class="con_lf_big_title">알림마당</div></h2>
					<ul class="sub_lf_menu">
						<!-- <li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=4&page_no=1" >공지사항</a></li> -->
						<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1" >공지사항</a></li>
						<li><a href="/mlsInfo/linkList01.jsp">자료실</a></li>
						<!-- <li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=1&page_no=1" class="on">자주묻는 질문</a></li> -->
						<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1" class="on">자주묻는 질문</a></li>
						<!-- <li><a href="/eventMgnt/eventList.do">저작권찾기 이벤트</a></li> -->
						<li><a href="/eventMgnt/eventList.do">이벤트</a></li>
						<li><a href="/mlsInfo/comeInfo.jsp">찾아오시는 길</a></li>
					</ul>
				</div>
			<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						알림마당
						&gt;
						<span class="bold">자주묻는 질문</span>
					</div>
					<h1><div class="con_rt_hd_title">자주묻는 질문</div></h1>
					<div id="sub_contents_con">
						<!-- 검색 -->
						<form name="form1" action="#" class="sch mt20">
					    <input type="hidden" name="page_no" value="1" />
					    <input type="hidden" name="menuSeqn" value="<%=menuSeqn%>" />
					    <input type="hidden" name="bordSeqn" />
					    <input type="hidden" name="threaded" />
									<div class="sub06_rt_con_tp">
										<select id="srchDivs" name="srchDivs" class="w20" title="검색어조건선택">
											<option value="05" <%="05".equals(srchDivs)?"selected='selected'":"" %>>전체</option>
											<option value="01" <%="01".equals(srchDivs)?"selected='selected'":"" %>>제목</option>
											<option value="02" <%="02".equals(srchDivs)?"selected='selected'":"" %>>내용</option>
											<option value="03" <%="03".equals(srchDivs)?"selected='selected'":"" %>>작성자</option>
											<option value="04" <%="04".equals(srchDivs)?"selected='selected'":"" %>>제목+내용</option>
										</select>
										<input name="srchText" value="<%=srchText%>" size="35" onkeyup="fn_enterCheck(this)" title="검색어" class="inputData w40" />
										<!-- <span class="button small black"><input type="submit" onclick = "javascript:board_search();" value="검색" /></span> -->
										
										<a href="#" onclick="javascript:board_search(); return false;" class="music_search" style="margin-left: 60px;"><img src="/images/sub_img/sub_25.gif" alt="조회"></a>
									</div>
						</form>
						<!-- //검색 -->
						
						<!-- 테이블 리스트 Set -->
							<!-- 그리드스타일 -->
							<table class="sub_tab3 mar_tp40" cellspacing="0" cellpadding="0" width="100%" summary="자주묻는 질문에 대한 목록표로 글의 번호, 제목, 작성자, 작성일, 첨부파일, 조회수로 구성되어 있습니다.">
							<caption>자주묻는 질문</caption>	
								<colgroup>
									<col width="7%">
									<col width="*">
									<col width="10%">
									<col width="13%">
									<col width="9%">
									<col width="9%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">제목</th>
										<th scope="col">작성자</th>
										<th scope="col">작성일</th>
										<th scope="col">첨부파일</th>
										<th scope="col">조회수</th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${boardList.totalRow == 0}">
										<tr>
											<td class="ce" colspan="6">등록된 게시물이 없습니다.</td>
										</tr>
								</c:if>
								<c:if test="${boardList.totalRow > 0}">
									<c:forEach items="${boardList.resultList}" var="board">
							        <c:set var="NO" value="${board.TOTAL_CNT}"/>
						        	<c:set var="i" value="${i+1}"/>
									<tr>
										<td class="ce"><c:out value="${NO - i}"/></td>
										<td style="text-align: left;"><a href="#1" onclick="javascript:boardDetail('${board.BORD_SEQN}','${board.MENU_SEQN}','${board.THREADED}')">${board.TITE}</a></td>
										<td class="ce">${board.RGST_IDNT}</td>
										<td class="ce">${board.RGST_DTTM}</td>
										<td class="ce">${board.FILE_CONT}</td>
										<td class="ce">${board.INQR_CONT}</td>
									</tr>
									</c:forEach>
		    					</c:if>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<!-- 페이징 -->
							<div class="pagination">
								<ul>
									<li>
							 		<%-- 페이징 리스트 --%>
									  <jsp:include page="../common/PageList_2011.jsp" flush="true">
										  <jsp:param name="totalItemCount" value="${boardList.totalRow}" />
											<jsp:param name="nowPage"        value="${param.page_no}" />
											<jsp:param name="functionName"   value="goPage" />
											<jsp:param name="listScale"      value="" />
											<jsp:param name="pageScale"      value="" />
											<jsp:param name="flag"           value="M01_FRONT" />
											<jsp:param name="extend"         value="no" />
										</jsp:include>
									</li>	
								</ul>
							</div>
							<!-- //페이징 -->
							
						</div>
						<!-- //테이블 리스트 Set -->
												
					</div>
					<p class="clear"></p>
				</div>
				<!-- //주요컨텐츠 end -->
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
</body>
</html>
