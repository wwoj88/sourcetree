<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금발생저작물 조회 | 저작권찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<style type="text/css">
<!--
.divM_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	width: 700px;
	margin-left: 0px;
}

.box {
	background-color:black;
	position:absolute;
	z-index:99999999;
	display:none;
}
-->
</style>

<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>


<script type="text/JavaScript">
<!--
jQuery(function(){
	var div = '${srchDIVS}';

	if(div=='1'){
		jQuery("#tab11").children().eq(0).addClass("on").html("<strong>방송음악</strong>").css({color:'#2a2a2a'});
	}
	else if(div=='2'){
		jQuery("#tab11").children().eq(1).addClass("on").html("<strong>교과용</strong>").css({color:'#2a2a2a'});
	}
	else if(div=='3'){
		jQuery("#tab11").children().eq(2).addClass("on").html("<strong>도서관</strong>").css({color:'#2a2a2a'});
	}	
});

//str
jQuery(window).resize(function(e){
	var docuWidth = jQuery(document).width()-16;
	var docuHeight = jQuery(document).height();
	
	jQuery("#modalBox").css({width:docuWidth,height:docuHeight});
	
	 var yp=document.body.scrollTop;
	   var xp=document.body.scrollLeft;

	   var ws=document.body.clientWidth;
	   var hs=document.body.clientHeight;
	   
	   var ajaxBox=$('ajaxBox');
	   //$('ajaxBoxMent').innerHTML=ment;

	   ajaxBox.style.top=yp+eval(hs)/2-100+"px";
	   ajaxBox.style.left=xp+eval(ws)/2-100+"px";
});
//end


// 로딩 이미지 박스
function showAjaxBox(ment){
	//항상 화면 중앙에 나타나도록...
   var yp=document.body.scrollTop;
   var xp=document.body.scrollLeft;
   

   var ws=document.body.clientWidth;
   var hs=document.body.clientHeight;
   
   
   if(!ment) ment="잠시만 기다려주세요..";
   
   var ajaxBox=$('ajaxBox');
  // $('ajaxBoxMent').innerHTML=ment;

   ajaxBox.style.top=yp+eval(hs)/2-100+"px";
   ajaxBox.style.left=xp+eval(ws)/2-100+"px";

	//str
	var docuWidth = jQuery('#wrap').width();
	var docuHeight = jQuery('#wrap').height();
	
   //back_blackBox
	 jQuery("<div>").addClass("box").attr("id","modalBox").
		css({width:docuWidth,height:docuHeight,left:0,top:0}).appendTo("body"); 
	 	jQuery("#modalBox").fadeTo(100, 0.3);
		jQuery("#modalBox").click(function(){
		jQuery("#modalBox").remove();
	})
	//end

  Element.show(ajaxBox);    
}

// 로딩 이미지 박스 감추기
function hideAjaxBox(){
	Element.hide('ajaxBox');	
	
	str
	jQuery("#modalBox").remove();
	end
	
// 	통합검색에서 넘어온경우 알림메시지
	alram();
}


//검색 조건이 없는 첫화면 조회 //20120220 정병호
function fn_InitframeList()
{
	var div = '${srchParam.srchDIVS }';
	var frm = document.frm;

	frm.target = "ifMuscInmtList";
	if(div == '2'){
		frm.target = "ifSubjInmtList";
	}else if(div == '3'){
		frm.target = "ifLibrInmtList";
	}else if(div == '4'){
		frm.target = "ifLssnInmtList";
	}
	
	frm.method = "post";
	frm.action = "/inmtPrps/inmtPrps.do?method=muscInmtList&page_no=1&srchDIVS="+'${srchParam.srchDIVS }';
	frm.submit();
}

function chk(event){
	ev = window.event || event;

	if((ev.keyCode>=48 && ev.keyCode<=57) || ev.keyCode == "8" || ev.keyCode == "13" ) return true;
	else return false;
}


//iframe 검색
function fn_frameList()
{
	var div = '${srchParam.srchDIVS }';
	var frm = document.frm;

	frm.target = "ifMuscInmtList";
	if(div == '2'){
		frm.target = "ifSubjInmtList";
	}else if(div == '3'){
		frm.target = "ifLibrInmtList";
	}else if(div == '4'){
		frm.target = "ifLssnInmtList";
	}
	
	//showAjaxBox();		// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			//showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {
			showAjaxBox();
		} 
	});
		
	frm.method = "post";
	frm.action = "/inmtPrps/inmtPrps.do?method=muscInmtList&page_no=1&srchDIVS="+'${srchParam.srchDIVS }';
	frm.submit();
}
//로딩 이미지 박스
function showAjaxBox(ment){
	//항상 화면 중앙에 나타나도록...
   var yp=document.body.scrollTop;
   var xp=document.body.scrollLeft;
   

   var ws=document.body.clientWidth;
   var hs=document.body.clientHeight;
   
   
   if(!ment) ment="잠시만 기다려주세요..";
   
   var ajaxBox=$('ajaxBox');
  // $('ajaxBoxMent').innerHTML=ment;

   ajaxBox.style.top=yp+eval(hs)/2-100+"px";
   ajaxBox.style.left=xp+eval(ws)/2-100+"px";

	//str
	var docuWidth = jQuery('#wrap').width();
	var docuHeight = jQuery('#wrap').height();
	
   //back_blackBox
	 jQuery("<div>").addClass("box").attr("id","modalBox").
		css({width:docuWidth,height:docuHeight,left:0,top:0}).appendTo("body"); 
	 jQuery("#modalBox").fadeTo(100, 0.3);
	jQuery("#modalBox").click(function(){
		jQuery("#modalBox").remove();
	})
	//end

  Element.show(ajaxBox);    
}

// 로딩 이미지 박스 감추기
function hideAjaxBox(){
	Element.hide('ajaxBox');	
	
	//str
	jQuery("#modalBox").remove();
	//end
	
	// 통합검색에서 넘어온경우 알림메시지
	alram();
}

// 상세팝업
function openDetail(url, name, openInfo)
{
	window.open(url, name, openInfo);
}

//리사이즈
function resizeIFrame(name) {
	var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
	document.getElementById(name).height= the_height+5;

}


// 저작물 추가
function fn_add() {
	var div = '${srchParam.srchDIVS }';

	var ifName = "ifMuscInmtList";
	if(div == '2'){
		ifName = "ifSubjInmtList";
	}else if(div == '3'){
		ifName = "ifLibrInmtList";
	}else if(div == '4'){
		ifName = "ifLssnInmtList";
	}
	
	//iFrame 항목
	var chkObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("iChk");
	var inmtSeqnObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("inmtSeqn");
	var sdsrNameObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("sdsrName");
	var muciNameObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("muciName");
	var yymmObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("yymm");
	var divsObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("divs");
	var prpsDivsObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("prpsDivs");
	var kappObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("kapp");
	var fokapoObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("fokapo");
	var krtraObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("krtra");
	var oferEtprObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("oferEtpr");
	var brctContObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("brctCont");
	var lyriWrtrObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("lyriWrtr");
	var comsWrtrObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("comsWrtr");
	var arrgWrtrObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("arrgWrtr");
	var albmNameObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("albmName");
	var duesCodeObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("duesCode");
	var dataTypeObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("dataType");
	var usexTypeObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("usexType");
	var selgYsnoObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("selgYsno");
	var usexLibrObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("usexLibr");
	var ouptPageObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("ouptPage");
	var ctrlNumbObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("ctrlNumb");
	var lishCompObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("lishComp");
	var workCodeObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("workCode");
	var caryDivsObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("caryDivs");
	var schlObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("schl");
	var bookDivsObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("bookDivs");
	var bookSizeDivsObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("bookSizeDivs");
	var schlYearDivsObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("schlYearDivs");
	var pubcContObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("pubcCont");
	var autrDivsObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("autrDivs");
	var workKindObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("workKind");
	var usexPageObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("usexPage");
	var workDivsObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("workDivs");
	var subjNameObjs = document.getElementById(ifName).contentWindow.document.getElementsByName("subjName");

	//선택 목록
	var selInmtSeqnObjs = document.getElementsByName("inmtSeqn");
	
	var params = "";
	var frm = document.frm;

	var isExistYn = "N";	//중복여부	
	var isAdd	= false;	//줄 추가여부
	
	//하단 추가
	if(chkObjs.length <= 0) {
		alert('선택된 저작물이 없습니다.');
		return;
	}

	for(var i = 0; i < chkObjs.length; i++) {

		if(chkObjs[i].checked == true && selInmtSeqnObjs.length > 0) {
			//중복여부 검사
			for(var j=0; j<selInmtSeqnObjs.length; j++) {
				if(inmtSeqnObjs[i].value == selInmtSeqnObjs[j].value) {
					isExistYn = "Y";
				}
			}
			
			if(isExistYn == "N") {
				//줄 추가여부
				isAdd = true;
				
			}else{
				alert("선택된 저작물 ["+sdsrNameObjs[i].value+"]는 이미 추가된 저작물입니다.");
				chkObjs[i].checked = false;
				return;
			}
		} else if(chkObjs[i].checked == true && selInmtSeqnObjs.length == 0) {
			//기본 알림줄 삭제(선택된 목록이 없습니다.)
			var oDummyTr = document.getElementById("dummyTr")
			if(eval(oDummyTr)) 	oDummyTr.parentNode.removeChild(oDummyTr);

			//줄 추가여부		
			isAdd = true;
		}

		//줄 추가실행
		if(isAdd){
			var tbody = document.getElementById("tbl_inmtPrps").getElementsByTagName("TBODY")[0];
			
			var row = document.createElement("TR");
			//tr에 id 지정
			var nLastIdx	= 0;	//현재 대상 테이블 tr의 최대번호을 담음
			var nIdIdx;			//현재 대상 테이블에 있는 tr의 Id 번호를 담음
			var oTrInfo = document.getElementById("tbl_inmtPrps").getElementsByTagName("tr");
			for(h = 0; h < oTrInfo.length; h++){
				if(oTrInfo[h].id != "undefined" && oTrInfo[h].id != ""){
					nIdIdx = Number((oTrInfo[h].id).substr("tbl_inmtPrps".length, oTrInfo[h].id.length));
					if(nLastIdx < nIdIdx)	nLastIdx = nIdIdx;
				}
			}

			var sNewId = "tbl_inmtPrps" + (nLastIdx + 1);		//tr에 지정할 id
		    row.id = sNewId;
			
			var td0 = document.createElement("TD");
			var td1 = document.createElement("TD"); 
			var td2 = document.createElement("TD"); 
			//var td3 = document.createElement("TD"); 
			var td4 = document.createElement("TD"); 
			//var td5 = document.createElement("TD");
			//var td6 = document.createElement("TD");
			//var td7 = document.createElement("TD");
		
			var tdData0 = "";
			tdData0 += '<input type="checkbox" name="chk"           value="0" title="선택">';
			tdData0 += '<input type="hidden" name="inmtSeqn"        value="'+ inmtSeqnObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="sdsrName"        value="'+ sdsrNameObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="muciName"        value="'+ muciNameObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="yymm"            value="'+ yymmObjs[i].value         +'">';
			tdData0 += '<input type="hidden" name="divs"            value="'+ divsObjs[i].value         +'">';
			tdData0 += '<input type="hidden" name="prpsDivs"        value="'+ prpsDivsObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="kapp"            value="'+ kappObjs[i].value         +'">';
			tdData0 += '<input type="hidden" name="fokapo"          value="'+ fokapoObjs[i].value       +'">';
			tdData0 += '<input type="hidden" name="krtra"           value="'+ krtraObjs[i].value        +'">';
			tdData0 += '<input type="hidden" name="oferEtpr"        value="'+ oferEtprObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="brctCont"        value="'+ brctContObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="lyriWrtr"        value="'+ lyriWrtrObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="comsWrtr"        value="'+ comsWrtrObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="arrgWrtr"        value="'+ arrgWrtrObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="albmName"        value="'+ albmNameObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="duesCode"        value="'+ duesCodeObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="dataType"        value="'+ dataTypeObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="usexType"        value="'+ usexTypeObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="selgYsno"        value="'+ selgYsnoObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="usexLibr"        value="'+ usexLibrObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="ouptPage"        value="'+ ouptPageObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="ctrlNumb"        value="'+ ctrlNumbObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="lishComp"        value="'+ lishCompObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="workCode"        value="'+ workCodeObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="caryDivs"        value="'+ caryDivsObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="schl"            value="'+ schlObjs[i].value         +'">';
			tdData0 += '<input type="hidden" name="bookDivs"        value="'+ bookDivsObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="bookSizeDivs"    value="'+ bookSizeDivsObjs[i].value +'">';
			tdData0 += '<input type="hidden" name="schlYearDivs"    value="'+ schlYearDivsObjs[i].value +'">';
			tdData0 += '<input type="hidden" name="pubcCont"        value="'+ pubcContObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="autrDivs"        value="'+ autrDivsObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="workKind"        value="'+ workKindObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="usexPage"        value="'+ usexPageObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="workDivs"        value="'+ workDivsObjs[i].value     +'">';
			tdData0 += '<input type="hidden" name="subjName"        value="'+ subjNameObjs[i].value     +'">';
			
			td0.innerHTML = tdData0;
			td0.className = 'ce';
			td1.innerHTML = sdsrNameObjs[i].value;
			td2.innerHTML = muciNameObjs[i].value;
			td2.className = 'ce';
			//td3.innerHTML = divsObjs[i].value;
			//td3.className = 'ce';
			td4.innerHTML = yymmObjs[i].value;
			td4.className = 'ce';
			/*
			td5.innerHTML = kappObjs[i].value;
			td5.className = 'ce';
			td6.innerHTML = fokapoObjs[i].value;
			td6.className = 'ce';
			td7.innerHTML = krtraObjs[i].value;
			td7.className = 'ce';
			*/
			row.appendChild(td0); 
			row.appendChild(td1); 
			row.appendChild(td2); 
			//row.appendChild(td3); 
			row.appendChild(td4); 
			//row.appendChild(td5); 
			//row.appendChild(td6); 
			//row.appendChild(td7); 
			 
			tbody.appendChild(row); 
			isAdd = false;
		}
	}

	//높이 조절
	resizeSelTbl();
}

// 선택된 저작물 삭제
function fn_delete(){
	var frm = document.frm;
	
	//선택 목록
	var chkObjs = document.getElementsByName("chk");
	var sCheck = 0;
	
	for(i=0; i<chkObjs.length;i++){
		if(chkObjs[i].checked){
			sCheck = 1;
		}
	}
	
	if(sCheck == 0 ){
		alert('선택된 저작물이 없습니다.');
		return;
	}
	
	for(var i=chkObjs.length-1; i >= 0; i--){
		var chkObj = chkObjs[i];
		
		if(chkObj.checked){
			// 선택된 저작물을 삭제한다.
			var oTR = findParentTag(chkObj, "TR");
			if(eval(oTR)) 	oTR.parentNode.removeChild(oTR);
		}
	}

	//높이 조절
	resizeSelTbl();
}

//보상금신청
function fn_chkSubmit(sDiv){
	var frm = document.frm;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}else{
		var div = '${srchParam.srchDIVS }';
		var isExist = 'N';
		var ifForm = '';
		if(sDiv == '1'){
			var hddnFrm = document.hidForm;
			if(div == '1'){
				ifForm = document.getElementById('ifMuscInmtList').contentWindow.document.ifFrm;
				hddnFrm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}else if(div == '2'){
				ifForm = document.getElementById('ifSubjInmtList').contentWindow.document.ifFrm;
				hddnFrm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}else if(div == '3'){
				ifForm = document.getElementById('ifLibrInmtList').contentWindow.document.ifFrm;
				hddnFrm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}else if(div == '4'){
				ifForm = document.getElementById('ifLssnInmtList').contentWindow.document.ifFrm;
				hddnFrm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}
			
			if(inspectCheckBoxField(ifForm.iChk)){
				//iFrame 항목
			    var ifrmInputVal = (getCheckStr(ifForm, 'Y'));

				hddnFrm.srchSdsrName.value	= frm.srchSdsrName.value;
				hddnFrm.srchYymm.value		= frm.srchYymm.value;
				hddnFrm.srchMuciName.value	= frm.srchMuciName.value;
			    
				hddnFrm.ifrmInput.value = ifrmInputVal;
			    
				hddnFrm.target = "_self";
				hddnFrm.method = "post";
				hddnFrm.submit();
			}else{
				alert("신청대상을 선택해 주세요.");
				return;
			}
		}else{
			//선택 목록 항목
			var chkObjs = document.getElementsByName("chk");

			for(var i = 0; i < chkObjs.length; i++) {
				if(chkObjs[i].checked == true){
					isExist = 'Y';
				}
			}
	
			if(isExist == 'N'){
				alert("신청대상을 선택해 주세요.");
				return;
			}
			
			frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			if(div == '2'){
				frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}else if(div == '3'){
				frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&gubun=edit&srchDIVS=" + div;
			}
			frm.target = "_self";
			frm.method = "post";
			frm.submit();
		}
	}
}

//높이 조절
function resizeSelTbl(){
	//var chkObjs = document.getElementsByName("chk");
	//document.getElementById("div_inmtPrps").style.height = (31.5 * (chkObjs.length + 1)) + 13 + "px" ;
	
	resizeDiv("tbl_inmtPrps", "div_inmtPrps");
}

//table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

    var the_height = document.getElementById(name).offsetHeight; //해당 Div의 높이
   var chkId = "chk"//체크박스 네임
   var chkCnt = document.getElementsByName(chkId).length;//현재건수
   var isLong = false;//현재 건수가 15는 넘었는지
   var default_height = 493;
   
   if(chkCnt > 15){
   	document.getElementById(targetName).style.height = default_height+"px";
   	document.getElementById(targetName).style.overflowY = "auto";
   }else{
   	document.getElementById(targetName).style.height = "auto";
   	document.getElementById(targetName).style.overflowY = "hidden";
   }
   //console.log(chkCnt+"개");
   //console.log("the_height[현재의높이는]: "+the_height);
   //console.log("default_height[현재의높이는]: "+default_height);
   
   //document.getElementById(targetName).style.height = the_height+13;
  
}

var gubun = "${gubun}";

function alram(){

	var mesg = '';
	
	// 통합검색에서 넘어온경우 알림메시지 
	//if("${gubun}" == "totalSearch" ){
	if(gubun == "totalSearch" ){
		
		gubun= '';
		
		if("${srchParam.srchSdsrName}"!=''){
			mesg = "[${srchParam.srchSdsrName}] (으)로 ";
		
			if("${srchParam.srchDIVS}" == '1')	mesg+= "곡명";
			if("${srchParam.srchDIVS}" == '2')	mesg+= "저작물명";
			if("${srchParam.srchDIVS}" == '3')	mesg+= "저작물명";
		}
		
		if("${srchParam.srchMuciName}"!=''){
			mesg = "[${srchParam.srchMuciName}] (으)로 ";
		
			if("${srchParam.srchDIVS}" == '1')	mesg+= "가수";
			if("${srchParam.srchDIVS}" == '2')	mesg+= "저작자명";
			if("${srchParam.srchDIVS}" == '3')	mesg+= "저자";
		}
		
		mesg += "(이)가 검색됩니다.\n검색항목 입력으로 상세검색이 가능합니다. ";
	
		alert( mesg);
	}
	
}
//검색 조건이 없는 첫화면 조회 //20120220 정병호
window.onload =	function(){
	fn_InitframeList()	
}

//-->
</script>
</head>
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(2);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_2.gif" alt="내권리찾기" title="내권리찾기" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<!-- CONTENT str-->
			<div class="content">
				
				<!--  ajaxBox str -->
				<div id="ajaxBox" style="position:absolute; z-index:99999999999; background: url(/images/2012/common/loadingBg.gif) no-repeat 0 0; left:-500px; width: 402px; height: 56px; padding: 102px 0 0 0;">
				</div>
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>내권리찾기</span><span>미분배 보상금 대상 저작물 확인</span><em>서비스 이용</em></p>
					<h1>
							<img src="/images/2012/title/content_h1_0202.gif "alt="미분배보상금 신청 " title="미분배보상금 신청 " />
					</h1>
					<!-- Tab str -->
                          <ul id="tab11" class="tab_menuBg">
                             <!--  <li class="first"><a href="/main/main.do?method=inmtInfo01">소개</a></li>
                              <li><a href="/mlsInfo/inmtInfo02.jsp">이용방법</a></li> -->
                              <li class="first"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악</a></li>
							  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용</a></li>
                              <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관</a></li>
                      		</ul>
                    <!-- //Tab -->
			
					<!-- contentForm STR -->
					<!-- hidden form str : 아이프레임데이타 가져가는 용도-->		
					<form name="hidForm" action="#" class="sch">
						<input type="hidden" name="srchDIVS"	value="${srchParam.srchDIVS }">
						<input type="hidden" name="srchSdsrName" />
						<input type="hidden" name="srchYymm" />
						<input type="hidden" name="srchMuciName" />
						<input type="hidden" name="srchAlbmName" />
						<input type="hidden" name="ifrmInput" />
						<input type="submit" style="display:none;">
					</form>
					<form name="frm" action="#" class="sch">
					<input type="submit" style="display:none;">
					<!--  SECTION STR  -->
					<div class="section relative">
						<!-- 검색 -->
						<fieldset class="w100 mt5 relative">
						<legend></legend>
						
							<!-- memo 삽입 -->
							<jsp:include page="/common/memo/2011/memo_01.jsp" >
									<jsp:param name="DIVS" value="${srchDIVS}"/>
							</jsp:include>
							
							<div class="boxStyle mt15">
								<div class="box1 floatDiv">
									<div class="fl w85">
										<p class="fl mt5 w15"><img src="/images/2012/content/sch_txt.gif" alt="Search" title="Search"></p>
										<table class="fl schBoxGrid w85" summary="">
											<caption></caption>
											<colgroup><col width="19%"><col width="24%"><col width="17%"><col width="*"></colgroup>
											<tbody>
												<c:if test="${srchParam.srchDIVS == '1'}">
													<tr>
														<%-- <th scope="row"><label for="sch3">방송년도</label></th>
														<td>
															<select id="sch3" name="srchYymm">
																<option value="" selected>전체</option>
																<option value="2009" <c:if test="${srchParam.srchYymm == 2009 }">selected="selected"</c:if>>2009년</option>
																<option value="2008" <c:if test="${srchParam.srchYymm == 2008 }">selected="selected"</c:if>>2008년</option>
																<option value="2007" <c:if test="${srchParam.srchYymm == 2007 }">selected="selected"</c:if>>2007년</option>
																<option value="2006" <c:if test="${srchParam.srchYymm == 2006 }">selected="selected"</c:if>>2006년</option>
																<option value="2005" <c:if test="${srchParam.srchYymm == 2005 }">selected="selected"</c:if>>2005년</option>
																<option value="2004" <c:if test="${srchParam.srchYymm == 2004 }">selected="selected"</c:if>>2004년</option>
																<option value="2003" <c:if test="${srchParam.srchYymm == 2003 }">selected="selected"</c:if>>2003년</option>
																<option value="2002" <c:if test="${srchParam.srchYymm == 2002 }">selected="selected"</c:if>>2002년</option>
															</select>
														</td> --%>
														<th scope="row"><label for="sch2">곡명</label></th>
														<td colspan="3">
															<input class="inputData" id="sch2" name="srchSdsrName" value="${srchParam.srchSdsrName}" size="55" maxlength="100"/>
														</td>
													</tr>
													<tr>
														<th scope="row"><label for="sch4">가수</label></th>
														<td><input class="inputData" id="sch4" name="srchMuciName" value="${srchParam.srchMuciName}" size="16" maxlength="50"/></td>
														<th scope="row"><label for="sch5">앨범명</label></th>
														<td><input class="inputData" id="sch5" name="srchAlbmName" value="${srchParam.srchAlbmName}" size="19" maxlength="50"/></td>
													</tr>
												</c:if>
												<c:if test="${srchParam.srchDIVS == '2'}">
													<tr>
														<th scope="row"><label for="sch3">출판년도</label></th>
														<td colspan="3">
															<%-- <select id="sch3" name="srchYymm">
																<option value="">전체</option>
																<option value="2009" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2009년</option>
																<option value="2008" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2008년</option>
																<option value="2007" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2007년</option>
																<option value="2005" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2005년</option>
																<option value="2004" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2004년</option>
															</select> --%>
															<input class="inputData" id="sch3" name="srchYymm" character="KE" style="ime-mode:disabled;" onkeypress='return chk()' value="${srchParam.srchYymm}" size="16" maxlength="4"/>
														</td>
													</tr>
													<tr>
														<th scope="row"><label for="sch2">저작물명</label></th>
														<td><input class="inputData" id="sch2" name="srchSdsrName" value="${srchParam.srchSdsrName}" size="16" maxlength="50"/></td>
														<th><label for="sch4">저작자명</label></th>
														<td><input class="inputData" id="sch4" name="srchMuciName" value="${srchParam.srchMuciName}" size="20" maxlength="50"/></td>
													</tr>
												</c:if>
												<c:if test="${srchParam.srchDIVS == '3'}">
													<tr>
														<th scope="row"><label for="sch3">발행년도</label></th>
														<td colspan="3">
															<%-- <select id="sch3" name="srchYymm">
																<option value="">전체</option>
																<option value="2009" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2009년</option>
																<option value="2008" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2008년</option>
																<option value="2007" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2007년</option>
																<option value="2006" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2006년</option>
																<option value="2005" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2005년</option>
																<option value="2004" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>2004년</option>
																<!-- 
																<c:forEach items="${srchYear}" var="year">
																	<option value="${year }" <c:if test="${srchParam.srchYymm == year }">selected</c:if>>${year }년</option>
																</c:forEach>
																 -->
															</select> --%>
															<input class="inputData" id="sch3" name="srchYymm" character="KE" style="ime-mode:disabled;" onkeypress='return chk()' value="${srchParam.srchYymm}" size="16" maxlength="4"/>
														</td>
													</tr>
													<tr>
														<th scope="row"><label for="sch2">저작물명</label></th>
														<td><input class="inputData" id="sch2" name="srchSdsrName" value="${srchParam.srchSdsrName}" size="16" maxlength="50"/></td>
														<th scope="row"><label for="sch4">저자</label></th>
														<td><input class="inputData" id="sch4" name="srchMuciName" value="${srchParam.srchMuciName}" size="20" maxlength="50"/></td>
													</tr>
												</c:if>
											</tbody>
										</table>
										<div>
											<p class="gray_box_line">&lowast; 검색은 원하는 항목에 찾고자 하는 검색어를 기입한 후 오른쪽 ‘조회’ 버튼을 클릭하세요.<a href="#1" onclick="javascript:toggleLayer('help_pop1');" class="ml10 underline black2">도움말<img src="/images/2012/common/ic_help.gif" class="vmid ml5" alt="" /></a></p>
											
											<!-- 도움말 레이어 -->
											<div class="layer_pop w80" id="help_pop1">
												<h1>검색도움말</h1>
												<div class="layer_con">
													<ul class="list1">
													<c:if test="${srchParam.srchDIVS == '1'}">
														<!-- <li class="p11"><strong>방송년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 방송년도와 관계없이 검색합니다.</li> -->
														<li class="p11"><strong>곡명</strong>: 곡명에 따라 해당되는 데이터를 검색 합니다.</li>
														<li class="p11"><strong>가수</strong>: 가수명에 따라 해당되는 데이터를 검색 합니다.</li>
													</c:if>
													<c:if test="${srchParam.srchDIVS == '2'}">
														<li class="p11"><strong>출판년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 출판년도와 관계없이 검색합니다.</li>
														<li class="p11"><strong>저작자명</strong>: 저작권자에 해당되는 데이터를 검색 합니다.</li>
													</c:if>
													<c:if test="${srchParam.srchDIVS == '3'}">
														<li class="p11"><strong>발행년도</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. [전체] 선택 시 발행년도와 관계없이 검색합니다.</li>
														<li class="p11"><strong>저자</strong>: 저자에 해당되는 데이터를 검색 합니다.</li>
													</c:if>
													</ul>
												</div>
												<a href="#1" onclick="javascript:toggleLayer('help_pop1');" class="layer_close"><img src="/images/2012/button/layer_close.gif" alt="" /></a>
											</div>
											<!-- //도움말 레이어 -->
										</div>
									</div>
									<p class="fl btn_area pt75"><input type="image" src="/images/2012/button/sch.gif" onclick="javascript:fn_frameList();" onkeypress="javascript:fn_frameList();" alt="검색" title="검색">
								</p></div>
								<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
							</div>
						</fieldset>
						<!-- //검색 -->
					<%-- 	<p style="margin-top:5px;width:730px;line-height:100%;">
							<label class="blue2" style="line-height:1.1;">
								<c:if test="${srchParam.srchDIVS == '1'}">
									&lowast; 방송사업자가 판매용 음반을 사용하여 방송하는 경우 사용한 음악의 실연자인 가수, 연주자와 그 음악을 최초로 음반으로 제작한 음반제작자에게 보상을 하게 되며 실연자는 한국음악실연자연합회에서 지급하고 음반제작자는 한국음반산업협회에서 지급합니다.
								</c:if>
								<c:if test="${srchParam.srchDIVS == '2'}">
									&lowast; 고등학교 및 이에 준하는 학교에서 학교의 교육 목적상 필요한 교과용도서에 공표된 저작물을 게재한 경우 저작재산권자에게 보상을 하게되며 한국복사전송권협회에서 지급합니다. 
								</c:if>
								<c:if test="${srchParam.srchDIVS == '3'}">
									&lowast; 도서관법에 따른 도서관과 공중의 이용에 제공하는 시설 중 대통령이 정하는 시설에 보관된 도서 저작물을 복제한 경우 저작재산권자에게 보상을 하게되며 한국복사전송권협회에서 지급합니다.
								</c:if>
							</label>
						</p> --%>
						<div class="floatDiv mt20"><!-- <p class="fl"><a href="#1" title="선택한 항목을 [보상금 선택목록]에 추가합니다." onclick="javascript:fn_add();" onkeypress="javascript:fn_add();"><img src="/images/2012/button/add_down.gif" alt="추가" /></a></p>
						<p class="fr rgt"><a href="#1" onclick="javascript:fn_chkSubmit('1');" onkeypress="javascript:fn_chkSubmit('1');"><img src="/images/2012/button/btn_app4.gif" alt="보상금 신청" title="보상금 신청" /></a></p> -->
						<!-- 아이프레임 str -->
						<c:if test="${srchParam.srchDIVS == '1'}">
							<iframe id="ifMuscInmtList" title="저작물조회(방송음악)" name="ifMuscInmtList" 
							width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
							scrolling="no" style="overflow-y:hidden;" ></iframe>
						</c:if>
						<c:if test="${srchParam.srchDIVS == '2'}">
							<iframe id="ifSubjInmtList" title="저작물조회(교과용)" name="ifSubjInmtList" 
							width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
							scrolling="no" style="overflow-y:hidden;" ></iframe>
						</c:if>
						<c:if test="${srchParam.srchDIVS == '3'}">
							<iframe id="ifLibrInmtList" title="저작물조회(도서관)" name="ifLibrInmtList" 
							width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
							scrolling="no" style="overflow-y:hidden;" ></iframe>
						</c:if>
						<c:if test="${srchParam.srchDIVS == '4'}">
							<iframe id="ifLssnInmtList" title="저작물조회(수업목적)" name="ifLssnInmtList" 
							width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
							scrolling="no" style="overflow-y:hidden;" ></iframe>
						</c:if>
						</div>
						<!-- 아이프레임 end -->
						<%-- <div class="floatDiv mt0 mb5">
							<p class="fl">
								<a href="#1" title="선택한 항목을 [보상금 선택목록]에 추가합니다." onclick="javascript:fn_add();" onkeypress="javascript:fn_add();" ><img src="/images/2012/button/add_down.gif" alt="추가" /></a>
								<span class="blue2 p11 ml5">&lowast; 상단  조회목록에서 선택하고  [추가]를 하면, 하단 선택목록에 담겨져서 한번에 신청이 됩니다.</span>
							</p>
						</div>
						<p class="HBar mt15 mb5">&nbsp;</p>
						<div class="floatDiv">
							<h2 class="fl"><c:if test="${srchParam.srchDIVS == '1'}">방송음악</c:if><c:if test="${srchParam.srchDIVS == '2'}">교과용</c:if><c:if test="${srchParam.srchDIVS == '3'}">도서관</c:if> 미분배보상금 신청 선택목록</h2>
						</div>
						<div class="floatDiv mb5">
							<p class="fl"><a href="#1" onclick="javascript:fn_delete();" onkeypress="javascript:fn_delete();"><img src="/images/2012/button/delete.gif" alt="삭제" /></a></p>
							<p class="fr">
								<a href="#1" onclick="javascript:fn_chkSubmit('2');" onkeypress="javascript:fn_chkSubmit('2');">
								<img src="/images/2012/button/btn_app4.gif" alt="보상금 신청" title="보상금 신청" />
								</a>
							</p>
						</div>
						<!-- 그리드스타일 -->
						<div id="div_inmtPrps" class="tabelRound" style="width:726px;padding:0px;margin:0 0 8px 0;">
							<table id="tbl_inmtPrps" cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="6%">
								<col width="*">
								<col width="22%">
								<col width="15%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" onkeypress="javascript:checkBoxToggle('frm','chk');" title="전체선택" /></th>
										<th scope="col"><c:if test="${srchParam.srchDIVS == '1'}">곡명</c:if><c:if test="${srchParam.srchDIVS == '2'}">저작물명</c:if><c:if test="${srchParam.srchDIVS == '3'}">저작물명</c:if></th>
										<th scope="col"><c:if test="${srchParam.srchDIVS == '1'}">가수</c:if><c:if test="${srchParam.srchDIVS == '2'}">저작자명</c:if><c:if test="${srchParam.srchDIVS == '3'}">저자</c:if></th>
										<th scope="col"><c:if test="${srchParam.srchDIVS == '1'}">방송년도</c:if><c:if test="${srchParam.srchDIVS == '2'}">출판년도</c:if><c:if test="${srchParam.srchDIVS == '3'}">발행년도</c:if></th>
									</tr>
								</thead>
								<tbody>
									<tr id="dummyTr">
										<td class="ce" colspan="4">선택된 목록이 없습니다.</td>								
									</tr>
								</tbody>
							</table>
						</div> --%>
						<!-- //그리드스타일 -->
					</div>
					<!--  SECTION end  -->
					</form>
					<!-- contentForm end -->
				</div>
				<!-- //주요컨텐츠 end -->
				<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu02.jsp" />	
				<script type="text/javascript">subSlideMenu("sub_lnb","lnb2","lnb22");</script>			
			<!-- //래프 -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<script type="text/JavaScript">
Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
</body>
</html>