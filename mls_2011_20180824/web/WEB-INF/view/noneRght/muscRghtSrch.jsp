<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2010/prototype.js"> </script>
<title>음악 저작물 | 저작권찾기</title>
<script type="text/JavaScript">
<!--
var emptyYn = '${emptyYn}'; // 검색조건이 없을 경우 //20120220 정병호

window.name = "noneMuscRghtSrch";
	
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/noneRght/rghtSrch.do?method=subList&DIVS=M";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 음악저작물 상세 팝업오픈 : 부모창에서 오픈
function openMusicDetail( crId, nrId, meId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	param += '&NR_ID='+nrId;
	param += '&ALBUM_ID='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=M'+param
	var name = '';
	var openInfo = 'target=noneMuscRghtSrch, width=705, height=570, scrollbars=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 음원 미리듣기
function openMp3(url, name, openInfo) {
		parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=M'+param
	var name = '';
	var openInfo = 'target=noneMuscRghtSrch, width=705, height=476, scrollbars=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifMuscRghtSrch"); 
	
	if(emptyYn != 'Y'){// 검색조건이 없을 경우 //20120220 정병호
		var totalRow = ${muscList.totalRow}
		parent.document.getElementById("totalRow").value = totalRow //+"건"; //초기페이지 예외처리하기.
	}

	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	parent.frames.scrollTo(0,0);
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
.grid tbody td.confirm{
	background: #f2f2f2 url(/images/2011/common/necessary3.gif) no-repeat 2% 10% !important;
	padding-left: 0px !important;
}

-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="sTotalRow" value="${muscList.totalRow}"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
	<input type="hidden" name="srchProducer" value="${srchParam.srchProducer }"/>
	<input type="hidden" name="srchAlbumTitle" value="${srchParam.srchAlbumTitle }"/>
	<input type="hidden" name="srchSinger" value="${srchParam.srchSinger }"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
	<input type="hidden" name="srchLicensor" value="${srchParam.srchLicensor }"/>
	<input type="hidden" name="srchNonPerf" value="${srchParam.srchNonPerf }"/>
	<input type="submit" style="display:none;">
	 
	<c:forEach items="${srchParam.srchNoneRole_arr }" var="srchNoneRole_arr">
		<c:set var="i" value="${i+1}"/>
		<input type="hidden" name="srchNoneRole_arr" value="${srchParam.srchNoneRole_arr[i-1] }" />
	</c:forEach>
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:auto;padding:0 0 0 0;">
			<table id="tab_scroll" cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="음악 저작물의 곡명, 앨범명, 발매일, 작사, 작곡, 가창, 음반제작사, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
					<col width="2%" />
					<col width="*" />
					<col width="15%" />
					<col width="15%" />
					<col width="15%" />
					<col width="15%" />
					<col width="15%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" rowspan="2"><input type="checkbox" id="chk" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','iChk',this);" style="cursor:pointer;" title="전체선택"/></th>
						<th scope="col" colspan="2">곡명</th>
						<th scope="col">작사</th>
						<th scope="col">작곡</th>
						<th scope="col">편곡</th>
						<th scope="col">저작권찾기</th>
					</tr>
					<tr>
						<th scope="col">앨범명</th>
						<th scope="col">발매일자</th>
						<th scope="col">가창</th>
						<th scope="col">연주</th>
						<th scope="col">지휘</th>
						<th scope="col">음반제작사</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${emptyYn == 'Y'}"><!-- 검색조건이 없을 경우 //20120220 정병호 -->
					<tr>
						<td class="ce" colspan="7">검색조건을 입력해 주세요.</td>
					</tr>
				</c:if>
				<c:if test="${muscList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="7">검색된 저작물 정보가 없습니다.</td>
					</tr>
				</c:if>
				<c:if test="${muscList.totalRow > 0}">
					<c:forEach items="${muscList.resultList}" var="muscList">
						<c:set var="NO" value="${muscList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<td class="ce" rowspan="2">
						<input type="checkbox" name="iChk" class="vmid" value="${muscList.CR_ID }|${muscList.NR_ID }|${muscList.ALBUM_ID }" style="cursor:pointer;" title="선택"/>
							<!-- hidden value start -->
							<input type="hidden" name="musicTitle" value="${muscList.MUSIC_TITLE}" />
							<input type="hidden" name="albumTitle" value="${muscList.ALBUM_TITLE}" />
							<input type="hidden" name="issuedDate" value="${muscList.ISSUED_DATE}" />
							<input type="hidden" name="crId" value="${muscList.CR_ID}" />
							<input type="hidden" name="nrId" value="${muscList.NR_ID}" />
							<input type="hidden" name="albumId" value="${muscList.ALBUM_ID}" />
							<input type="hidden" name="lyricist" value="${fn:replace(muscList.LYRICIST, ",", ", ")}" />
							<input type="hidden" name="composer" value="${fn:replace(muscList.COMPOSER, ",", ", ")}" />
							<input type="hidden" name="arranger" value="${fn:replace(muscList.ARRANGER, ",", ", ")}" />
							<input type="hidden" name="translator" value="${muscList.TRANSLATOR}" />
							<input type="hidden" name="singer" value="${muscList.SINGER}" />
							<input type="hidden" name="producer" value="${muscList.PRODUCER}" />
							<input type="hidden" name="player" value="${muscList.PLAYER}" />
							<input type="hidden" name="conductor" value="${muscList.CONDUCTOR}" />
							<input type="hidden" name="featuring" value="${muscList.FEATURING}" />
							<input type="hidden" name="icnNumb" value="${muscList.ICN_NUMB}" />
							<input type="hidden" name="albumProducedCrh" value="${muscList.ALBUM_PRODUCED_CRH}" />
							<!-- 미확인 실연자 : <input type="hidden" name="nonPerf" value="${muscList.NONPERF}" /> -->
							<!-- 저작권미상 -->
							<input type="hidden" name="lyricist_yn" value="${muscList.LYRICIST_YN}" />
							<input type="hidden" name="composer_yn" value="${muscList.COMPOSER_YN}" />
							<input type="hidden" name="arranger_yn" value="${muscList.ARRANGER_YN}" />
							<input type="hidden" name="singer_yn" value="${muscList.SINGER_YN}" />
							<input type="hidden" name="player_yn" value="${muscList.PLAYER_YN}" />
							<input type="hidden" name="conductor_yn" value="${muscList.CONDUCTOR_YN}" />
							<input type="hidden" name="producer_yn" value="${muscList.PRODUCER_YN}" />
							<!-- hidden value end -->
						</td>
						<td colspan="2">
							<a href="#1" onclick="javascript:openMusicDetail('${muscList.CR_ID }','${muscList.NR_ID }','${muscList.ALBUM_ID }');" class="underline black2"z>${muscList.MUSIC_TITLE }</a>
							<c:if test="${muscList.recFile != '' && muscList.recFile != null}">
								<a href="#1" onclick="javascript:openMp3('/common/mediaPlay.jsp?mp3Url=http://db.copyright.or.kr/cdccmp3/${muscList.recFile }','window2','location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=323,height=150,left=250, top=150, scrollbars=auto');"><img src="/images/common/ic_preview.gif" class="vtop" alt="음원 샘플듣기" /></a>
							</c:if>
						</td>
						<td class="ce<c:if test="${muscList.LYRICIST_YN == '0'}"> confirm</c:if>">${fn:replace(muscList.LYRICIST, ",", ", ")}</td>
						<td class="ce<c:if test="${muscList.COMPOSER_YN == '0'}"> confirm</c:if>">${fn:replace(muscList.COMPOSER, ",", ", ")}</td>
						<td class="ce<c:if test="${muscList.ARRANGER_YN == '0'}"> confirm</c:if>">${fn:replace(muscList.ARRANGER, ",", ", ")}</td>
						<td class="ce">
							<c:if test="${muscList.PRPS_CNT >0}">
								<a href="#1" onclick="javascript:openRghtPrps('${muscList.CR_ID }','${muscList.NR_ID }','${muscList.ALBUM_ID }');"><img src="/images/common/ic_newWin.gif" class="vtop" alt="저작권찾기신청내역" /></a>
							</c:if>
						</td>
					</tr>
					<tr>
						<td>${muscList.ALBUM_TITLE }</td>
						<td class="ce">${muscList.ISSUED_DATE }</td>
						<td class="ce<c:if test="${muscList.SINGER_YN == '0'}"> confirm</c:if>">${muscList.SINGER }</td>
						<td class="ce<c:if test="${muscList.PLAYER_YN == '0'}"> confirm</c:if>">${muscList.PLAYER }</td>
						<td class="ce<c:if test="${muscList.CONDUCTOR_YN == '0'}"> confirm</c:if>">${muscList.CONDUCTOR }</td>
						<td class="ce<c:if test="${muscList.PRODUCER_YN == '0'}"> confirm</c:if>">${muscList.PRODUCER }</td>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${muscList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->
		<!--contents end-->
</form>
</body>
</html>
