<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.json.simple.JSONObject"%>
<%-- <%@page import="org.codehaus.jackson.JsonParser"%> --%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><c:if test="${genreCd!='total'}">${genreCd}</c:if><c:if test="${genreCd=='total'}">전체</c:if> | 디지털저작권거래소 | 분야별 권리자 찾기 | 권리자찾기</title>
</head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">

<script src="/js/jquery-1.7.js" type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 

//<!--

/* 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-69621660-2', 'auto');
 ga('send', 'pageview');
  */
 function crosPop(){
    window.open("https://cras.copyright.or.kr/front/right/comm/main_.do");
  }

 
 function statBoRegiPop(){
    window.open("/statBord/statBo06Regi.do", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes"); 
 }
 
 function statBoSelect(){
    window.open("/statBord/statBo08Select.do", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes"); 
}
 
 function goSearch(){
    
      var fnm = document.search;
      var query=fnm.query.value;
      query=query.replace(/^\s+/,""); 

      if (query == "" || query == null) {
          alert("검색어를 입력하세요.");
          return;
      }

      rangeValue="";
      for(i=0; i<fnm.range.length; i++){      
        if(fnm.range[i].checked){       
          if(rangeValue!="") rangeValue = rangeValue+",";
          rangeValue = rangeValue+fnm.range[i].value;               
        }
      }
      
      fnm.arr_range.value = rangeValue;    

      fnm.submit();
      return false;
}
 
 function goCategory(Str){
    var collection = Str;
    var frm = document.search;
   
    if(Str=='cf_reg'){
      frm.collection.value = 'cf_reg';
    }else{
      frm.collection.value = Str;
    }
    frm.subPageStr.value = 'Y';
    frm.action = "/search/search.do";
    frm.submit();
  }
  
function comma(num){
    var len, point, str; 
       
    num = num + ""; 
    point = num.length % 3 ;
    len = num.length; 
    str = num.substring(0, point); 
    while (point < len) { 
        if (str != "") str += ","; 
        str += num.substring(point, point + 3); 
        point += 3; 
    } 
    return str;
}

function goDgCategory(str){
  var frm = document.search;
  
  frm.action = "/search/search.do";
  changeGenreCd(str);
  frm.submit();
}

</script>

</head>

<body>

	<!-- HEADER str-->
	<jsp:include page="/include/2017/header.jsp" />

	<script type="text/javascript"> 
$(function(){
  
  var genreCd ='${genreCd}';
  var menuFlag = '${menuFlag}';
  var collectionName = '${collectionName}';
  var collectionName2 = '${collectionName}';
  var collectionNameArr =  collectionName.split(",");
  var indexNum = collectionName.indexOf(',');
 
  if(indexNum!=-1){
    collectionName = collectionName.substring(0,collectionName.indexOf(','));
  }
  
  if(collectionNameArr.length > 2){
    $('#collection option[value='+collectionName2+']').attr('selected','selected');   
  }else{ 
    $('#collection option[value='+collectionName+']').attr('selected','selected'); //*/ 
  }
  $('#dgCnt').html(comma($.trim($('#dgCnt').html())));
  
  
  var flag = false;
  
  for(var i = 0 ; i < 12 ; i++){
    
    if($("#topMenu"+i).html()==genreCd){
      $("#topMenu"+i).parent().parent().attr("class","first on");
      flag = true;
    }
  }
  /* if(flag==false){
    $("#topMenu0").parent().parent().attr("class","first on");
  } */
  //console.log("genreCd : " + genreCd)
  if($('.con_rt_hd_title').html()!=genreCd){
    if(genreCd=='total'){
      if(menuFlag=='N'){
        $('.con_rt_hd_title').html('상당한 노력 신청 서비스');
      }else{
        $('.con_rt_hd_title').html('전체');
      }
      
    }else{
      //console.log("genreCd : " + genreCd)
      $('.con_rt_hd_title').html(genreCd);
    }
  }
  
  
})  

</script>
	<!--    <script type="text/javascript">initNavigation(1);</script> -->
	<!-- GNB setOn 각페이지에 넣어야합니다. -->

	<!-- HEADER end -->

	<!-- content st -->
	<div id="contents">
		${menuFlag}
		<c:if test="${menuFlag!='N'}">
			<div class="con_lf">
				<div class="con_lf_big_title">
					분야별 <br>권리자 찾기
				</div>
				<ul class="sub_lf_menu">
					<li><a href="#" id="leftMenu0" class="on" onclick="javascript:goInnerSearch('999'); return false;">전체</a></li>
					<li><a href="#" id="leftMenu1" onclick="javascript:goInnerSearch('1'); return false;">음악</a></li>
					<li><a href="#" id="leftMenu2" onclick="javascript:goInnerSearch('2'); return false;">어문</a></li>
					<li><a href="#" id="leftMenu3" onclick="javascript:goInnerSearch('3'); return false;">방송대본</a></li>
					<li><a href="#" id="leftMenu4" onclick="javascript:goInnerSearch('4'); return false;">영화</a></li>
					<li><a href="#" id="leftMenu5" onclick="javascript:goInnerSearch('5'); return false;">방송</a></li>
					<li><a href="#" id="leftMenu6" onclick="javascript:goInnerSearch('6'); return false;">뉴스</a></li>
					<li><a href="#" id="leftMenu7" onclick="javascript:goInnerSearch('7'); return false;">미술</a></li>
					<li><a href="#" id="leftMenu8" onclick="javascript:goInnerSearch('8'); return false;">이미지</a></li>
					<li><a href="#" id="leftMenu9" onclick="javascript:goInnerSearch('9'); return false;">사진</a></li>
					<li><a href="#" id="leftMenu10" onclick="javascript:goInnerSearch('99'); return false;">기타</a></li>
				</ul>
			</div>
		</c:if>
		<c:if test="${menuFlag eq 'N'}">
			<div class="con_lf">
				<div class="con_lf_big_title">
					법정허락<br>승인 신청
				</div>
				<ul class="sub_lf_menu">
					<li><a href="javascript:goInnerSearch('99');">상당한 노력 신청 서비스</a></li>
					<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
					<!-- <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
					<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
					<li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
					<!-- <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li> -->
					<!-- <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li> -->
					<!-- <li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li> -->
				</ul>
			</div>
		</c:if>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" /> &gt; 분야별 권리자 찾기 &gt; 국립중앙 도서관 도서정보 &gt;
				<c:if test="${genreCd=='total'}">
					<span class="bold">전체</span>
				</c:if>
				<c:if test="${genreCd!='total'}">
					<span class="bold">${genreCd}</span>
				</c:if>
			</div>
			<div class="con_rt_hd_title">국립중앙 도서관 도서정보</div>


			<div id="sub_contents_con">
				<form id="search" name="search" action="/search/search.do" method="post" onSubmit="goSearch(); return false">
					<div class="bg_f8f8f8">

						<!-- 2014.11.06 추가 -->
						<div>
							<div class="float_lf">
								<span class="sub_blue_point_bg">검색대상</span>&nbsp;&nbsp;&nbsp;
								<input type="checkbox" value="all" name="range" rel="1" id="chkall" onclick="check(1);">
								<label for="chkall" class="thin p12">전체</label>
								<input type="checkbox" value="title" name="range" rel="2" id="chktl" onclick="check(2);">
								<label for="chktl" class="thin p12">저작물명</label>
								<input type="checkbox" value="licer_detail" name="range" rel="2" id="chktl" onclick="check(2);">
								<label for="chktl" class="thin p12">저작권자</label>
								</td>
							</div>
							<div class="float_rt">
								<span class="sub_blue_point_bg">정렬방식</span>&nbsp;&nbsp;&nbsp;
								<select name="sort" id="line">
									<option value="RANK">정확도순</option>
									<option value="DATE">날짜순</option>
								</select>
								<select name="sortOrder" id="">
									<option value="DESC">내림차순</option>
									<option value="ASC">오름차순</option>
								</select>
							</div>
							<p class="clear"></p>
						</div>

						<div class="mar_tp10">
							<select id="collection" name="collection" title="장르">
								<option value="cf_book">전체</option>

							</select>
							<input type="text" title="검색어" id="query" name="query" size="30" value="${query}" style="IME-MODE: active; padding: 3px; width: 48%;" />
							<input type="submit" value="검색" style="background-color: #2c65aa; color: white;">
							&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="chk1" name="resrch_check" class="checkbox" onclick="javascript:resrch_chk();" />
							<label for="chk1" class="p11 strong black">결과 내 검색</label>
						</div>
					</div>
					<input type="hidden" name="resultcount" value="10" />
					<input type="hidden" name="page" value="" />
					<input type="hidden" name="mode" value="" />
					<input type="hidden" name="arr_range" value="">
					<input type="hidden" name="resrch" value="" />
					<input type="hidden" id="urlCd" name="urlCd" value="01" />
					<input type="hidden" id="subPageStr" name="subPageStr" value="book" />
					<input type="hidden" id="startCount" name="startCount" value="0" />
					<input type="hidden" id="genreCd" name="genreCd" value="" />
				</form>


				<p class="clear"></p>
			</div>
			<%
			     /* request.getAttribute("searchResult") */
						String jsonData = (String) request.getAttribute("searchResult");// 검색엔진 전체 json데이터 
						JSONParser jsonParser = new JSONParser();// json 파서

						JSONObject jsonObject2 = (JSONObject) jsonParser.parse(jsonData);//json 데이터 파싱 시작

						Map jsonMap = (Map) jsonObject2.get("SearchQueryResult");
						JSONArray collection = (JSONArray) jsonMap.get("Collection");
			%>


			<!-- 국립중앙 도서관 도서정보 연동 데이터  -->
			<div class="bg_2e75b6 mar_tp10">
				<h2 class="float_lf color_fff blod">
					국립중앙 도서관 도서정보(<font id="bookCnt"> <%
      int collectionSize = collection.size();

 			Map obj = null;
 			Map objMap = null;
 			Map documentSet_dg_album = null;
 			ArrayList document = null;
 			int documentSize = 0;
 			int x = 0;
 			int bookAllCount = 0;
 			Map documentCntMap = null;
 			for (int y = 0; y < collectionSize; y++) {
 				documentCntMap = (Map) collection.get(y);
 				String collectionId = (String) documentCntMap.get("Id");
 				documentSet_dg_album = (Map) documentCntMap.get("DocumentSet");
 				//out.print(collectionId);
 				if (collectionId.equals("cf_book")) {
 					//out.print("true : "+collectionId+"<br>");
 					int tempInt = Integer.parseInt((String) documentSet_dg_album.get("TotalCount"));
 					bookAllCount += tempInt;
 				}

 			}
 			out.print(bookAllCount);
 %>
					</font> 건)
				</h2>

				<p class="clear"></p>
			</div>
			<table class="sub_tab1" cellspacing="0" cellpadding="0" width="100%" summary="저작권찾기 신청정보입니다." class="grid">
				<colgroup>
					<col width="10%">
					<col>
					<col>
				</colgroup>
				<tbody>
					<%-- <%= documentCntMap %> --%>
					<%
					     for (int z = 0; z < collectionSize; z++) {
									Map documentSet2 = (Map) collection.get(z);
									if (documentSet2.get("Id").equals("cf_book")) {
										documentSet_dg_album = (Map) documentSet2.get("DocumentSet");
										document = (ArrayList) documentSet_dg_album.get("Document");
										documentSize = document.size();
										ArrayList<Map<String, Object>> cf_bookList = new ArrayList<Map<String, Object>>();
										for (x = 0; x < documentSize; x++) {
											Map<String, Object> exMap = new HashMap<String, Object>();
											obj = (Map) document.get(x);
											/*  objMap = (Map)obj.get( "Field" ); */
											exMap = (Map) obj.get("Field");
											cf_bookList.add(exMap);
										}

										request.setAttribute("cf_bookList", cf_bookList);
					%>
					<c:forEach var="item" items="${cf_bookList}" varStatus="status">
						<tr>
							<th scope="row" class="vtop">도서</th>
							<td class="liceSrch">
								<b>${item.TITLE}</b>
								<p class="mt15">
									<c:if test="${!empty item.CREATOR}">
										<span>숨/지은이 : ${item.CREATOR}</span>
										<br />
									</c:if>
									<c:if test="${!empty item.SUBJECT}">
										<span class="w60">주제명 : ${item.SUBJECT}</span>
										<br>
									</c:if>
									<c:if test="${!empty item.PUBLISHER}">
										<span class="w60">발행사항 : ${item.PUBLISHER}</span>
										<br>
									</c:if>
									<c:if test="${!empty item.EXTENT}">
										<span class="w60">형태사항 : ${item.EXTENT}</span>
										<br>
									</c:if>
									<c:if test="${!empty item.ISNI_CD}">
									<span class="w60">표준번호/부호 : <br>
										<c:forTokens items="${item.ISNI_CD }" delims="," var="item">
                          ${item}<br/>
										</c:forTokens>
										</span>

										
										<br>
									</c:if>
									<c:if test="${!empty item.DDC_CLASS_NO}">
										<span class="w60">분류기호 : ${item.DDC_CLASS_NO}</span>
										<br>
									</c:if>

								</p>
							</td>
						</tr>
					</c:forEach>

					<%
					     }

								}
					%>


				</tbody>
			</table>
			<!-- 디지털 저작권 거래소 연동 데이터  -->
			<div id="commPagination" class="pagination">
				<ul>
					<li>
						<!-- <span class="direction bgNone"><span class="prev mr10" style="margin-top:3px;">123123</span></span> -->
					</li>
				</ul>
			</div>

		</div>
		<!-- End sub_contents_con -->
		<script type="text/javascript" language="javascript" src="/js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript">
              var listCount = 10;
              var totalCount = <%=documentSet_dg_album.get("TotalCount")%>;
              
       
              //var totalPage = totalCount/listCount;
              var totalPage = Number(Math.ceil(totalCount*1.0/listCount));
              if(<%=request.getAttribute("startCount")%>==0){
                var currentPageNum = 1;
              }else{
                var currentPageNum = <%=request.getAttribute("startCount")%>+1;
              }
              
              var pageStr='<%=documentCntMap.get("Id")%>';

      $(function() {
        if (currentPageNum != 1) {
          $("#commPagination").append('<span class="direction bgNone"><span class="prev mr10" style="margin-top:3px;" onclick="javascript:prevPageCall()"></span></span>');

        }
        for (var i = 1; i <= totalPage; i++) {

          if (Number(Math.ceil(currentPageNum * 0.1) * 10) + 1 > i && i >= Number(Math.ceil(currentPageNum * 0.1) * 10) + 1 - 10) {
            if (currentPageNum == i) {
              
               $("#commPagination").append("<strong><a href='javascript:changePage(" + i + ",\"" + pageStr + "\")' >" + i + "</a></strong>"); 
              /* $("#commPagination").append("<a href='javascript:changePage(" + i + ",\"" + pageStr + "\")' >" + i + "</a>"); */
            } else {
              $("#commPagination").append("<a href='javascript:changePage(" + i + ",\"" + pageStr + "\")' >" + i + "</a>");
            }

          }
        }
        $("#commPagination").append('<span class="direction bgNone"><span class="next ml10" style="margin-top:3px;" onclick="javascript:nextPageCall()"></span></span>');
        //console.log($("#commPagination").html());
      })

      function prevPageCall() {
        currentPageNum--;
        changePage(currentPageNum, pageStr);
      }

      function nextPageCall() {

        currentPageNum++;

        changePage(currentPageNum, pageStr);
      }

      function changePage(pageNum, callName) {
        //console.log("callName : " + callName );
        var pageNum = pageNum;
        var frm = document.search;
        
      
        frm.collection.value = callName;
        frm.startCount.value = pageNum *10 - 10;
        //alert(frm.startCount.value);
        if(pageNum==1){
          frm.startCount.value=0;
        }
        frm.submit();
      }

      function changeGenreCd(callName) {
        //sconsole.log("call");
        //console.log(callName);
        var frm = document.search;

        switch (callName) {
        case "dg_album":
          frm.genreCd.value = "앨범";
          break;
        case "dg_art":
          frm.genreCd.value = "미술저작물";
          break;
        case "dg_art_licensor":
          frm.genreCd.value = "미술저작권자";
          break;
        case "dg_book":
          frm.genreCd.value = "도서";
          break;
        case "dg_broadcast":
          frm.genreCd.value = "방송저작물";
          break;
        case "dg_broadcast_licensor":
          frm.genreCd.value = "방송저작권자";
          break;
        case "dg_character":
          frm.genreCd.value = "캐릭터저작물";
          break;
        case "dg_character_licensor":
          frm.genreCd.value = "캐릭터저작권자";
          break;
        case "dg_content":
          frm.genreCd.value = "방송컨텐츠저작물";
          break;
        case "dg_content_licensor":
          frm.genreCd.value = "방송컨텐츠저작권자";
          break;
        case "dg_literature":
          frm.genreCd.value = "어문저작물";
          break;
        case "dg_literature_licensor":
          frm.genreCd.value = "어문저작권자";
          break;
        case "dg_movie_licensor":
          frm.genreCd.value = "영화저작권자";
          break;
        case "dg_movie":
          frm.genreCd.value = "영화저작물";
          break;
        case "dg_music_licensor":
          frm.genreCd.value = "음악저작권자";
          break;
        case "dg_music1":
          frm.genreCd.value = "음악저작물";
          break;
        case "dg_news":
          frm.genreCd.value = "뉴스저작물";
          break;
        case "dg_news_licensor":
          frm.genreCd.value = "뉴스저작권자";
          break;
        case "dg_public":
          frm.genreCd.value = "공공저작물";
          break;
        case "dg_public_licensor":
          frm.genreCd.value = "공공저작권자";
          break;
        case "dg_scenario":
          frm.genreCd.value = "방송대본저작물";
          break;
        case "dg_scenario_licensor":
          frm.genreCd.value = "방송대본저작권자";
          break;
        case "ALL":
          frm.collection.value = "dg_music1,dg_literature,dg_movie,dg_news";
          frm.genreCd.value = "전체";
          break;
        default:
          frm.collection.value = "dg_music1,dg_literature,dg_movie,dg_news";
          //console.log(frm.collection.value)
          frm.genreCd.value = "전체";
          break;
        } //*/
      }
    </script>



	</div>
	<!-- End con_rt -->
	<p class="clear"></p>
	</div>
	<!-- End contents -->

	<!-- //주요컨텐츠 end -->
	<!-- //content -->

	<!-- FOOTER str-->
	<jsp:include page="/include/2017/footer.jsp" />
	<!-- FOOTER end -->



	<!-- //전체를 감싸는 DIVISION -->

</body>
</html>