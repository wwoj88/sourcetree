<%@page import="org.json.simple.JSONArray"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.json.simple.JSONObject"%>
<%-- <%@page import="org.codehaus.jackson.JsonParser"%> --%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=euc-kr"
    pageEncoding="euc-kr"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><c:if test="${genreCd!='total'}">${genreCd}</c:if><c:if test="${genreCd=='total'}">전체</c:if> | 저작권 등록부 | 분야별 권리자 찾기 | 권리자찾기</title>
</head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">

<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 

//<!--

/* 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-69621660-2', 'auto');
 ga('send', 'pageview');
  */
 function crosPop(){
    window.open("https://www.cros.or.kr/psnsys/cmmn/infoPage.do?w2xPath=/ui/twc/sch/regInfSerc/regInfSercList.xml");
    /*    window.open("https://cras.copyright.or.kr/front/right/comm/main_.do"); */
  }

 
 function statBoRegiPop(){
    window.open("/statBord/statBo06Regi.do", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes"); 
  }
 
 function goSearch(){
    
      var fnm = document.search;
      var query=fnm.query.value;
      query=query.replace(/^\s+/,""); 

      if (query == "" || query == null) {
          alert("검색어를 입력하세요.");
          return;
      }

      rangeValue="";
      for(i=0; i<fnm.range.length; i++){      
        if(fnm.range[i].checked){       
          if(rangeValue!="") rangeValue = rangeValue+",";
          rangeValue = rangeValue+fnm.range[i].value;               
        }
      }
      
      fnm.arr_range.value = rangeValue;    

      fnm.submit();
      return false;
}
 
function goCategory(Str){
  
}
  
  
function comma(num){
    var len, point, str; 
       
    num = num + ""; 
    point = num.length % 3 ;
    len = num.length; 
    str = num.substring(0, point); 
    while (point < len) { 
        if (str != "") str += ","; 
        str += num.substring(point, point + 3); 
        point += 3; 
    } 
    return str;
}


</script>

</head>

<body>
    
    <!-- HEADER str-->
    <jsp:include page="/include/2017/header.jsp" />
    
<script type="text/javascript"> 
$(function(){
  $('#regCnt').html(comma($('#regCnt').html()));
  var genreCd ='${genreCd}';
  var menuFlag = '${menuFlag}';
  var flag = false;
  
  for(var i = 0 ; i < 12 ; i++){
    
    if($("#topMenu"+i).html()==genreCd){
      $("#topMenu"+i).parent().parent().attr("class","first on");
      flag = true;
    }
  }
  if(flag==false){
    $("#topMenu0").parent().parent().attr("class","first on");
  }
  
  if($('.con_rt_hd_title').html()!=genreCd){
    if(genreCd=='total'){
      if(menuFlag=='N'){
        $('.con_rt_hd_title').html('상당한 노력 신청 서비스');
      }else{
        $('.con_rt_hd_title').html('전체');
      }
      
    }else{
      $('.con_rt_hd_title').html(genreCd);
    }
  }
})  

</script>
<!--    <script type="text/javascript">initNavigation(1);</script> -->
    <!-- GNB setOn 각페이지에 넣어야합니다. -->

    <!-- HEADER end -->
    
    <!-- content st -->
      <div id="contents">
      ${menuFlag}
       <c:if test="${menuFlag!='N'}">
            <div class="con_lf">
      <div class="con_lf_big_title">분야별 <br>권리자 찾기</div>
        <ul class="sub_lf_menu">
        <li><a href="#" id="leftMenu0" class="on" onclick="javascript:goInnerSearch('999'); return false;">전체</a></li>
        <li><a href="#" id="leftMenu1" onclick="javascript:goInnerSearch('1'); return false;">음악</a></li>
        <li><a href="#" id="leftMenu2" onclick="javascript:goInnerSearch('2'); return false;">어문</a></li>
        <li><a href="#" id="leftMenu3" onclick="javascript:goInnerSearch('3'); return false;">방송대본</a></li>
        <li><a href="#" id="leftMenu4" onclick="javascript:goInnerSearch('4'); return false;">영화</a></li>
        <li><a href="#" id="leftMenu5" onclick="javascript:goInnerSearch('5'); return false;">방송</a></li>
        <li><a href="#" id="leftMenu6" onclick="javascript:goInnerSearch('6'); return false;">뉴스</a></li>
        <li><a href="#" id="leftMenu7" onclick="javascript:goInnerSearch('7'); return false;">미술</a></li>
        <li><a href="#" id="leftMenu8" onclick="javascript:goInnerSearch('8'); return false;">이미지</a></li>
        <li><a href="#" id="leftMenu9" onclick="javascript:goInnerSearch('9'); return false;">사진</a></li>
        <li><a href="#" id="leftMenu10" onclick="javascript:goInnerSearch('99'); return false;">기타</a></li>
        </ul>
      </div>
      </c:if>
      <c:if test="${menuFlag eq 'N'}">
      <div class="con_lf">
      <div class="con_lf_big_title">법정허락<br>승인 신청</div>
        <ul class="sub_lf_menu">
        <li><a href="javascript:goInnerSearch('99');">상당한 노력 신청 서비스</a></li>
        <!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
        <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li>
        <li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
        <li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
        <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
        <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
        <li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li>
        </ul>
      </div>
      </c:if>
          <div class="con_rt">
            <div class="con_rt_head">
              <img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
              &gt;
              분야별 권리자 찾기
              &gt;
              저작권 등록부
              &gt;
              <c:if test="${genreCd=='total'}"><span class="bold">전체</span></c:if>  
              <c:if test="${genreCd!='total'}"><span class="bold">${genreCd}</span></c:if>  
            </div>
            <div class="con_rt_hd_title">저작권자 찾기</div>
          
          
            <div id="sub_contents_con">
            <form id="search" name="search" action="/search/search.do" method="post" onSubmit="goSearch(); return false">
                  <div class="bg_f8f8f8">
                  
                  <!-- 2014.11.06 추가 -->
                  <div>
                    <div class="float_lf">
                      <span class="sub_blue_point_bg">검색대상</span>&nbsp;&nbsp;&nbsp;
                          <input type="checkbox" value="all" name="range" rel="1" id="chkall" onclick="check(1);" ><label for="chkall" class="thin p12">전체</label>
                          <input type="checkbox" value="title" name="range" rel="2" id="chktl" onclick="check(2);" ><label for="chktl"  class="thin p12">저작물명</label>
                          <input type="checkbox" value="licer_detail" name="range" rel="2" id="chktl" onclick="check(2);"><label for="chktl"  class="thin p12">저작권자</label></td>
                      </div>
                      <div class="float_rt">
                      <span class="sub_blue_point_bg">정렬방식</span>&nbsp;&nbsp;&nbsp;
                        <select name="sort" id="line">
                              <option value="RANK">정확도순</option>
                              <option value="DATE">날짜순</option>
                        </select>
                        <select name="sortOrder" id="">
                              <option value="DESC">내림차순</option>
                              <option value="ASC">오름차순</option>
                        </select>
                      </div> 
                  <p class="clear"></p>
                  </div>
                  
                    <div class="mar_tp10">
                    <!-- <select name="target" id="srchDivs" title="검색구분">
                      <option>통합검색</option>
                      <option>저작권 등록부</option>
                      <option>위탁관리 저작물</option>
                    </select> -->
                    <select id="collection" name="collection" title="장르">
                      <option value="ALL">전체</option>
                      <option  selected="selected" value="reg_copyright">저작권등록부</option>
                      <option value="cf_music">음악</option>
                      <option value="cf_literature">어문</option>
                      <option value="cf_scenario">방송대본</option>
                      <option value="cf_movie">영화</option>
                      <option value="cf_broadcast">방송</option>
                      <option value="cf_news">뉴스</option>
                      <option value="cf_art">미술</option>
                      <option value="cf_image">이미지</option>
                      <option value="cf_photo">사진</option>
                      <option value="cf_etc">기타</option>
                    </select>
                    <input type="text" title="검색어" id="query" name="query" size="30" value="${query}" style="IME-MODE: active;padding:3px;width:48%;"/>
                    <input type="submit" value="검색" style="background-color: #2c65aa; color: white;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chk1" name="resrch_check" class="checkbox" onclick="javascript:resrch_chk();"/><label for="chk1" class="p11 strong black">결과 내 검색</label>
                    </div>
                  </div>
              <input type="hidden" name="resultcount" value="10"/>
              <input type="hidden" name="page" value=""/>
              <input type="hidden" name="mode" value=""/>
              <input type="hidden" name="arr_range" value="">
              <input type="hidden" name="resrch" value=""/>             
              <input type="hidden" id="urlCd" name="urlCd" value="01"/>
              <input type="hidden" name = "startCount" value="">
              <input type="hidden" name = "subPageStr" value="Y">
              <input type="hidden" name = "genreCd" value="${genreCd}">
            </form>
            
          <!--  <div class="sub01_con_bg4_tp mar_tp30"></div>이용승인 신청 공고
            <div class="sub01_con_bg4">
              <h3>법정허락 승인 신청</h3>
              <p class="word_dian_bg mar_tp5">이미 상당한 노력이 이행되어 법정허락 승인 신청이 가능한 저작물로, 법정허락 승인 신청정보 작성화면으로 이동합니다.</p>
              <h3 class="mar_tp10">저작권 등록 정보 조회</h3>
              <p class="word_dian_bg mar_tp5">검색한 저작물의 정보 조회를 위해서 저작권 등록 시스템 통합 검색으로 이동합니다.</p>
              <h3 class="mar_tp10">저작권자 찾기위한  상당한노력 신청</h3>
              <p class="word_dian_bg mar_tp5">저작권 찾기 정보시스템에서 한 번의 신청으로 상당한 노력 이행 신청과 함께 저작권자 조회공고가 자동으로 처리됩니다.</p>
            </div>
            <div class="subcetcol">
              <p class="cetcol1">검색 결과에 찾으시는 저작물이 없는 경우 클릭하세요</p>
              <p class="cetcol2"><a href="#" onclick="statBoRegiPop();"><img src="/images/sub/btn_3.jpg" alt="검색" /></a></p>
              <p class="clear"></p>
            </div> -->
           <!--  <ul class="tab_menuBg mt20">
              <li><strong><a id="topMenu0" href="javascript:goInnerSearch('999');">전체</a> </strong></li>
              subPageStr=yes 서브페이지 개발용 변수
              <li><strong><a id="topMenu1" href="javascript:goInnerSearch2('1');">음악</a> </strong></li>
              <li><strong><a id="topMenu2" href="javascript:goInnerSearch2('2');">어문</a> </strong></li>
              <li><strong><a id="topMenu3" href="javascript:goInnerSearch2('3');">방송대본</a> </strong></li>
              <li><strong><a id="topMenu4" href="javascript:goInnerSearch2('4');">영화</a> </strong></li>
              <li><strong><a id="topMenu5" href="javascript:goInnerSearch2('5');">방송</a> </strong></li>
              <li><strong><a id="topMenu6" href="javascript:goInnerSearch2('6');">뉴스</a> </strong></li>
              <li><strong><a id="topMenu7" href="javascript:goInnerSearch2('7');">미술</a> </strong></li>
              <li><strong><a id="topMenu8" href="javascript:goInnerSearch2('8');">이미지</a> </strong></li>
              <li><strong><a id="topMenu9" href="javascript:goInnerSearch2('9');">사진</a> </strong></li>
              <li><strong><a id="topMenu10" href="javascript:goInnerSearch2('99');">기타</a> </strong></li>
            </ul> -->
            <%
              /* request.getAttribute("searchResult") */
              String  jsonData = (String) request.getAttribute("searchResult");// 검색엔진 전체 json데이터 
              JSONParser jsonParser = new JSONParser();// json 파서
              
              JSONObject jsonObject2 = (JSONObject)jsonParser.parse( jsonData );//json 데이터 파싱 시작
              
              Map jsonMap = (Map) jsonObject2.get( "SearchQueryResult" ); 
               JSONArray collection = (JSONArray) jsonMap.get("Collection");  
               Map documentSet = (Map) collection.get( 0 );

              Map documentSetMap = (Map) documentSet.get("DocumentSet");
              JSONArray documentJA = (JSONArray) documentSetMap.get("Document");
              
              
            %>
            <%-- <%=documentSet.get("Id") %> --%>
            
            <div class="bg_2e75b6 mar_tp10">  
                  <h2 class="float_lf color_fff blod">저작권 등록부(<font id="regCnt"><%=documentSetMap.get("TotalCount")%></font>건)</h2>
                    <!-- <p class="bzhlag"><a href="#"><img src="/images/sub/mulem.png" alt=" " /></a>
                      <span class="bzhlag22"></span>
                    </p> -->
                    <!-- <a href="" class="float_rt color_fff">더보기</a> -->
            <p class="clear"></p>
            </div>
            <table class="sub_tab1" cellspacing="0" cellpadding="0" width="100%" summary="저작권찾기 신청정보입니다." class="grid">
              <colgroup> 
                <col width="10%">
                <col>
                <col>
              </colgroup>
              <tbody> 
              <%-- <%=documentSet.get("Id") %> --%>
              <% 
                Map documentSet2 = (Map) collection.get( 0 );
                ArrayList<Map<String, Object>> copyrightList = new ArrayList<Map<String, Object>>();
                Map documentSetMap2 = (Map) documentSet.get("DocumentSet");
                JSONArray documentJA2 = (JSONArray) documentSetMap.get("Document");
              
                for( int j = 0 ; j < documentJA2.size() ; j++){
                  /* documentJA 결과값 리스트 */
               
                  Map<String, Object> exMap = new HashMap<String, Object>();
                  Map obj2 = (Map) documentJA2.get(j);
                  /* Map objField2 = (Map) obj2.get("Field"); */
                  exMap = (Map) obj2.get("Field");
                  copyrightList.add(exMap);
                }
                 // out.print(copyrightList);
                request.setAttribute("copyrightList", copyrightList);
              
                  if(documentSet2.get("Id").equals( "reg_copyright" )){
              %>
              <c:forEach var="item" items="${copyrightList}" varStatus="status">
              <%-- <%=obj%> --%>
                  <tr>
                    <th style="width:15%;">저작권등록부</th>
                    <td>
                      <b><c:out value="${item.CONT_TITLE}" /></b>
                      <p class="mt15">
                      <c:if test="${!empty item.REG_ID}">
                         <span class="w60">등록번호 : ${item.REG_ID} </span></br>
                      </c:if>
                      <c:if test="${!empty item.REG_DT}">
                         <span class="w60">등록일자 : <c:set var="TextValue" value="${item.REG_DT}"/>${fn:substring(TextValue,0,10) } </span></br>
                      </c:if>
                      <c:if test="${!empty item.REG_PART1}">
                         <span>등록부문 : ${item.REG_PART1} </span></br>
                      </c:if>
                      <c:if test="${!empty item.AUTHOR_NAME}">
                         <span class="w60">저작자 : ${item.AUTHOR_NAME} </span></br>
                      </c:if>
                      <c:if test="${!empty item.CONT_CLASS_NAME}">
                         <span class="w60">종류 : ${item.CONT_CLASS_NAME} </span></br>
                      </c:if>
                      <c:if test="${!empty item.REG_CAUS_TXT}">
                         <span>등록원인 : ${item.REG_CAUS_TXT}</span></br>
                      </c:if>
                      <c:if test="${!empty item.AUTHOR_NAME}">
                         <span class="w60">등록권리자 : ${item.AUTHOR_NAME} </span></br>
                      </c:if>
                      </p>
                    </td>
  
                  <td><span><a href="javascript:;" onclick="crosPop()" >
                  <img src="/images/sub/btn_2_1.jpg" alt="저작권 등록 정보 조회" /></a></span></td>
                  </tr>
                 </c:forEach>
              <%
                  
                 
                }
              %>
                
              </tbody>
            </table>
            </div><!-- End sub_contents_con -->
            <script type="text/javascript" language="javascript" src="/js/jquery-1.8.3.min.js"></script>
            <script type="text/javascript">
              var listCount = 10;
              var totalCount = <%=documentSetMap.get("TotalCount")%>;
              var totalPage = totalCount / listCount;
              if(<%=request.getAttribute("startCount")%>==0){
                var currentPageNum = 1;
              }else{
                var currentPageNum = <%=request.getAttribute("startCount")%>;
              }
              
              var pageStr='<%=documentSet.get("Id")%>';
            /*  console.log("currentPageNum : " + currentPageNum)
             console.log("totalPage : " + totalPage)
             console.log("pageStr : " + pageStr) */
              
              $(function(){
               if(currentPageNum!=1){
                 $("#commPagination").append('<span class="direction bgNone"><span class="prev mr10" style="margin-top:3px;" onclick="javascript:prevPageCall()"></span></span>');
                 
               }
                for(var i = 1 ; i < totalPage ; i++){
                  
                  if(Number(Math.ceil(currentPageNum*0.1)*10) + 1 > i && i >= Number(Math.ceil(currentPageNum*0.1)*10) + 1-10){
                  if(currentPageNum == i){
                    $("#commPagination").append("<strong><a href='javascript:changePage("+i+",\""+pageStr+"\")' >"+i+"</a></strong>");
                  }else{
                    $("#commPagination").append("<a href='javascript:changePage("+i+",\""+pageStr+"\")' >"+i+"</a>");
                  }
                     
                  }  
                }
                $("#commPagination").append('<span class="direction bgNone"><span class="next ml10" style="margin-top:3px;" onclick="javascript:nextPageCall()"></span></span>');
                //console.log($("#commPagination").html());
              })
             
             function prevPageCall(){
               currentPageNum--;
               changePage(currentPageNum,pageStr); 
             }
              
             function nextPageCall(){
               currentPageNum++;
               changePage(currentPageNum,pageStr); 
             }
              
              function goInnerSearch2(index){
                //console.log(index)
                if(index!=0)
                {
                  var frm = document.search;
              
                   switch (index) {
                   case "0"    : 
                      frm.genreCd.value = "저작권등록부" ;
                      frm.collection.value = "reg_copyright";
                        break;
                    case "1"    : 
                      frm.genreCd.value = "음악" ;
                      frm.collection.value = "cf_music";
                        break;
                    case "2"   : 
                      frm.genreCd.value = "어문" ;
                      frm.collection.value = "cf_literature";
                        break;
                    case "3"  : 
                      frm.genreCd.value = "방송대본" ;
                      frm.collection.value = "cf_scenario";
                        break;
                    case "4"  : 
                      frm.genreCd.value = "영화" ;
                      frm.collection.value = "cf_movie";
                        break;
                    case "5"  : 
                      frm.genreCd.value = "방송" ;
                      frm.collection.value = "cf_broadcast";
                        break;
                    case "6"  : 
                      frm.genreCd.value = "뉴스" ;
                      frm.collection.value = "cf_news";
                        break;    
                    case "7"  : 
                      frm.genreCd.value = "미술" ;
                      frm.collection.value = "cf_art";
                        break;
                    case "8"  : 
                      frm.genreCd.value = "이미지" ;
                      frm.collection.value = "cf_image";
                        break;
                    case "9"  : 
                      frm.genreCd.value = "사진" ;
                      frm.collection.value = "cf_photo";
                        break;
                    case "99"  : 
                      frm.genreCd.value = "기타" ;
                      frm.collection.value = "cf_etc";
                        break;
                    case "999"  : 
                      frm.genreCd.value = "total" ;
                        break;
                    case "9999"  : 
                        frm.genreCd.value = "total" ;
                        frm.menuFlag.value = "N";
                          break;
                    default    : 
                      frm.genreCd.value = "" ;
                          break;
                  } 
                } 
                changePage('1',frm.collection.value);  
              }
              
              function changePage(pageNum,callName){
              var pageNum =  pageNum;
              var frm = document.search;
              frm.collection.value = callName;
              frm.startCount.value = pageNum*10;
              //console.log("callName : " + callName );
                 switch (callName) {
                 case "reg_copyright"    : 
                    frm.genreCd.value = "저작권등록부" ;
                      break;
                  case "cf_music"    : 
                    frm.genreCd.value = "음악" ;
                      break;
                  case "cf_literature"   : 
                    frm.genreCd.value = "어문" ;
                      break;
                  case "cf_scenario"  : 
                    frm.genreCd.value = "방송대본" ;
                      break;
                  case "cf_movie"  : 
                    frm.genreCd.value = "영화" ;
                      break;
                  case "cf_broadcast"  : 
                    frm.genreCd.value = "방송" ;
                      break;
                  case "cf_news"  : 
                    frm.genreCd.value = "뉴스" ;
                      break;    
                  case "cf_art"  : 
                    frm.genreCd.value = "미술" ;
                      break;
                  case "cf_image"  : 
                    frm.genreCd.value = "이미지" ;
                      break;
                  case "cf_photo"  : 
                    frm.genreCd.value = "사진" ;
                      break;
                  case "cf_etc"  : 
                    frm.genreCd.value = "기타" ;
                      break;
                  case "total"  : 
                    frm.genreCd.value = "total" ;
                      break;
                  case "9999"  : 
                      frm.genreCd.value = "total" ;
                      frm.menuFlag.value = "N";
                        break;
                  default    : 
                    frm.genreCd.value = "" ;
                        break;
                } 
              //console.log(frm.collection.value);
              frm.submit();
              }
              
            </script>
            <div id="commPagination" class="pagination">
              <ul>
                <li>
              <!-- <span class="direction bgNone"><span class="prev mr10" style="margin-top:3px;">123123</span></span> -->
                </li>
              </ul>         
            </div>
          </div><!-- End con_rt -->
          <p class="clear"></p>
        </div><!-- End contents -->

        <!-- //주요컨텐츠 end -->
          <!-- //content -->
          
          <!-- FOOTER str-->
        <jsp:include page="/include/2017/footer.jsp" />
        <!-- FOOTER end -->
    
        
                
  <!-- //전체를 감싸는 DIVISION -->

</body>
</html>