<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>내권리찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>

<script src="https://code.jquery.com/jquery-1.7.js"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>



<script type="text/JavaScript"><!--

//검색 조건이 없는 첫화면 조회 //20120220 정병호
var hiddenSrchStartDate = "${hiddenSrchStartDate}";
var hiddenSrchEndDate = "${hiddenSrchEndDate}";

jQuery(function(){
	var div = '${DIVS}';
	if(div=='M'){
		jQuery("#tab11").children().eq(0).addClass("on").html("<strong>음악</strong>").css({color:'#2a2a2a'});
	}
	if(div=='B'){
		jQuery("#tab11").children().eq(1).addClass("on").html("<strong>어문</strong>").css({color:'#2a2a2a'});
	}
	if(div=='N'){
		jQuery("#tab11").children().eq(2).addClass("on").html("<strong>뉴스</strong>").css({color:'#2a2a2a'});
	}
	if(div=='C'){
		jQuery("#tab11").children().eq(3).addClass("on").html("<strong>방송대본</strong>").css({color:'#2a2a2a'});
	}
	if(div=='I'){
		jQuery("#tab11").children().eq(4).addClass("on").html("<strong>이미지</strong>").css({color:'#2a2a2a'});
	}
	if(div=='V'){
		jQuery("#tab11").children().eq(5).addClass("on").html("<strong>영화</strong>").css({color:'#2a2a2a'});
	}
	if(div=='R'){
		jQuery("#tab11").children().eq(6).addClass("on").html("<strong>방송</strong>").css({color:'#2a2a2a'});
	}
});


//str
jQuery(window).resize(function(e){
	var docuWidth = jQuery(document).width()-16;
	var docuHeight = jQuery(document).height();
	
	

	jQuery("#modalBox").css({width:docuWidth,height:docuHeight});
	
	   var yp=document.body.scrollTop;
	   var xp=document.body.scrollLeft;

	   var ws=document.body.clientWidth;
	   var hs=document.body.clientHeight;
	   
	   var ajaxBox=$('ajaxBox');
	   //$('ajaxBoxMent').innerHTML=ment;

	   ajaxBox.style.top=yp+eval(hs)/2-100+"px";
	   ajaxBox.style.left=xp+eval(ws)/2-100+"px";
});
//end

// 로딩 이미지 박스
function showAjaxBox(ment){
	//항상 화면 중앙에 나타나도록...
   var yp=document.body.scrollTop;
   var xp=document.body.scrollLeft;
   

   var ws=document.body.clientWidth;
   var hs=document.body.clientHeight;
   
   
   if(!ment) ment="잠시만 기다려주세요..";
   
   var ajaxBox=$('ajaxBox');
  // $('ajaxBoxMent').innerHTML=ment;

   ajaxBox.style.top=yp+eval(hs)/2-100+"px";
   ajaxBox.style.left=xp+eval(ws)/2-100+"px";

	//str
	var docuWidth = jQuery('#wrap').width();
	var docuHeight = jQuery('#wrap').height();
	
   //back_blackBox
	 jQuery("<div>").addClass("box").attr("id","modalBox").
		css({width:docuWidth,height:docuHeight,left:0,top:0}).appendTo("body"); 
	 jQuery("#modalBox").fadeTo(100, 0.3);
	jQuery("#modalBox").click(function(){
		jQuery("#modalBox").remove();
	})
	//end

  Element.show(ajaxBox);    
}

// 로딩 이미지 박스 감추기
function hideAjaxBox(){
	Element.hide('ajaxBox');	
	
	//str
	jQuery("#modalBox").remove();
	//end
	
	// 통합검색에서 넘어온경우 알림메시지
	alram();
}

/*calendar호출*/
function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}

//숫자만 입력
function only_arabic(t){
	var key = (window.netscape) ? t.which :  event.keyCode;

	if (key < 45 || key > 57) {
		if(window.netscape){  // 파이어폭스
			t.preventDefault();
		}else{
			event.returnValue = false;
		}
	} else {
		//alert('숫자만 입력 가능합니다.');
		if(window.netscape){   // 파이어폭스
			return true;
		}else{
			event.returnValue = true;
		}
	}	
}
	
// 날짜체크 
function checkValDate(){
	var f = document.frm;
	if(f.srchStartDate.value!='' && f.srchEndDate.value!=''){
		if(parseInt(f.srchStartDate.value,10)>parseInt(f.srchEndDate.value,10)){
			alert('만료일이 시작일보다 이전입니다.\n일자를 다시 확인하십시요');
			f.srchEndDate.value='';
			return false;
		}
	}

	if(f.srchStartDate.value!=''){
		if(f.srchStartDate.value.length != 8){
			alert('일자형식은 8자리 입니다.');
			f.srchStartDate.value='';
			return false;
		}
	}
	if(f.srchEndDate.value!=''){
		if(f.srchEndDate.value.length != 8){
			alert('일자형식은 8자리 입니다.');
			f.srchEndDate.value='';
			return false;
		}
	}
}


//검색 조건이 없는 첫화면 조회 //20120220 정병호
function fn_InitframeList()
{
	var div = '${DIVS}';
	var frm = document.frm;

	if(div == 'M'){
		frm.target = "muscRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=M";
	}else if(div == 'B'){
		frm.target = "bookRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=B";
	}else if(div == 'N'){
		frm.target = "newsRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=N";
	}else if(div == 'C'){
		frm.target = "scriptRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=C";
	}else if(div == 'I'){
		frm.target = "imageRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=I";
	}else if(div == 'V'){
		frm.target = "mvieRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=V";
	}else if(div == 'R'){
		frm.target = "broadcastRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=R";
	}
	
	
	frm.method = "post";
	frm.submit();
}


//iframe 검색
function fn_frameList()
{
	var div = '${DIVS}';
	var frm = document.frm;
	
	if(div == 'M'){
		frm.target = "muscRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=M";
	}else if(div == 'B'){
		frm.target = "bookRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=B";
	}else if(div == 'N'){
		frm.target = "newsRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=N";
	}else if(div == 'C'){
		frm.target = "scriptRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=C";
	}else if(div == 'I'){
		frm.target = "imageRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=I";
	}else if(div == 'V'){
		frm.target = "mvieRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=V";
	}else if(div == 'R'){
		frm.target = "broadcastRghtSrch";
		frm.action = "/rghtPrps/rghtSrch.do?method=subList&page_no=1&DIVS=R";
	}
	
	//showAjaxBox();		// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {
			//showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {
			showAjaxBox();
			//alert("완료");
		} 
	});
	
	
	frm.method = "post";
	frm.submit();
}

// 상세팝업
function openDetail(url, name, openInfo)
{
	window.open(url, name, openInfo);
}

// 권리찾기신청- 목적선택화면에서 호출한다.
function goRghtPrpsMain(gubun, PRPS_RGHT_CODE)
{	
	document.hidForm.PRPS_RGHT_CODE.value = PRPS_RGHT_CODE;
	
	// 로그인확인
	var userIdnt = "<%=(String) session.getAttribute("sessUserIdnt")%>";
	
	if(userIdnt=='null' ||userIdnt=='')
	{
		alert('로그인이 필요한 화면입니다.');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}
	
	document.hidForm.userIdnt.value = userIdnt;
	
	var frm;
	
	if(gubun == 'mainList')
	{	
		goRghtPrps();
	}
	else
	{	
		frm = document.frm;
		
		if(!inspectCheckBoxField(frm.chk))
		{
			alert('항목을 선택 해 주세요.'); return;
		}
		else
		{
			goSelRghtPrps();
		}
		
	}
	
}

// 권리찾기 목적선택 팝업오픈
function openRghtPrps(gubun)
{	
	// 로그인확인
	var userIdnt = "<%=(String) session.getAttribute("sessUserIdnt")%>";
	
	if(userIdnt=='null' ||userIdnt=='')
	{
		alert('로그인이 필요한 화면입니다.');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}
	
	// 항목선택 확인
	var frm;
	
	if(gubun == 'mainList')
	{	
		window.open("/rghtPrps/rghtSrch.do?method=rghtPrpsSelc&gubun="+gubun, "", 'target=rghtPrps ,width=705, height=225');
	}
	else
	{	
		frm = document.frm;
		
		if(!inspectCheckBoxField(frm.chk))
		{
			alert('항목을 선택 해 주세요.'); return;
		}
		else
		{
			window.open("/rghtPrps/rghtSrch.do?method=rghtPrpsSelc&gubun="+gubun, "", 'target=rghtPrps, width=705, height=225');
		}
		
	}

}

// 권리찾기신청
function goRghtPrps()
{	
	
	var div = '${DIVS }';	
	
	// 아이프레임 
	var frm ;

	if(div == 'M'){
		//frm = muscRghtSrch.ifFrm;
		frm = document.getElementById("ifMuscRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'B'){
		//frm = bookRghtSrch.ifFrm;
		frm = document.getElementById("ifBookRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'C'){
		//frm = scriptRghtSrch.ifFrm;
		frm = document.getElementById("ifScriptRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'I'){
		//frm = mvieRghtSrch.ifFrm;
		frm = document.getElementById("ifImageRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'V'){
		//frm = mvieRghtSrch.ifFrm;
		frm = document.getElementById("ifMvieRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'R'){
		//frm = broadcastRghtSrch.ifFrm;
		frm = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'N'){
		//frm = newsRghtSrch.ifFrm;
		frm = document.getElementById("ifNewsRghtSrch").contentWindow.document.ifFrm
	}

	if(div != 'X'){
		if(inspectCheckBoxField(frm.iChk))
			conValue = confirm('선택된 항목을 저작권찾기신청합니다.');
		else
			conValue = confirm('선택 항목없이 저작권찾기신청합니다.');
	}else{
			conValue = confirm('기타 저작권찾기신청합니다.');
	}
			
	if(conValue) 
	{			
		if(div != 'X'){
	    	var ifrmInputVal = (getCheckStr(frm, 'Y'));
		}	
		
		frm = document.hidForm;
		    
		if(div != 'X'){    
		    frm.ifrmInput.value = ifrmInputVal;
			//조회조건
			if(div != 'R'){
		    	frm.srchTitle.value = document.getElementById("sch1").value;
		    }else{
		    	frm.srchTitle.value = document.getElementById("sch3").value;
		    }   
		    if(div != 'I'){
			    frm.srchStartDate.value = document.getElementById("sch5").value;
				frm.srchEndDate.value = document.getElementById("sch6").value;
			}
		}
		
		if(div == 'M'){
		
			frm.srchLicensor.value = document.getElementById("sch7").value;
			frm.srchProducer.value = document.getElementById("sch2").value;
			frm.srchAlbumTitle.value = document.getElementById("sch3").value;
			frm.srchSinger.value = document.getElementById("sch4").value;
			//frm.srchNonPerf.value = document.getElementById("sch8").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=M";
			
		}else if(div == 'B'){
			
			frm.srchLicensor.value = document.getElementById("sch7").value;
			frm.srchPublisher.value = document.getElementById("sch2").value;
			frm.srchBookTitle.value = document.getElementById("sch3").value;
			frm.srchLicensorNm.value = document.getElementById("sch4").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=B";
			
		}else if(div == 'C'){
		
			frm.srchWriter.value = document.getElementById("sch2").value;
			frm.srchBroadStatName.value = document.getElementById("sch3").value;
			frm.srchDirect.value = document.getElementById("sch4").value; 
			frm.srchPlayers.value = document.getElementById("sch7").value; 
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=C";
			
		}else if(div == 'I'){
			
			frm.srchWorkName.value = document.getElementById("sch1").value;
			frm.srchWterDivs.value = document.getElementById("sch2").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=I";
			
		}else if(div == 'V'){
			
			frm.srchDistributor.value = document.getElementById("sch2").value;
			frm.srchDirector.value = document.getElementById("sch3").value;
			frm.srchActor.value = document.getElementById("sch7").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=V";
			
		}else if(div == 'R'){
			
			frm.srchProgName.value = document.getElementById("sch1").value;
			frm.srchMaker.value = document.getElementById("sch2").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=R";
			
		}else if(div == 'N'){
		
			frm.srchProviderNm.value = document.getElementById("sch2").value;//언론사명
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=N";
			
		}else if(div == 'X'){
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=X";
		}
	
		frm.method = "post";
		
		frm.submit();
	}
	else
	{
		return;
	}	
}

// 선택목록 권리찾기신청
function goSelRghtPrps()
{	
	// 로그인확인
	var userIdnt = "<%=(String) session.getAttribute("sessUserIdnt")%>";
	
	if(userIdnt=='null' ||userIdnt=='')
	{
		alert('로그인이 필요한 화면입니다');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}
	
	var div = '${DIVS }';	
	var frm = document.frm;
	
	if(inspectCheckBoxField(frm.chk))
	{
		conValue = confirm('선택된 항목을 저작권찾기신청합니다.');

		if(conValue) 
		{		
		    var ifrmInputVal = fncReplaceStr(getCheckStr(frm, 'Y'), "chk", "iChk");

		    frm = document.hidForm;
		    
		    frm.ifrmInput.value = ifrmInputVal;
		  // frm.userIdnt.value = userIdnt;
		    
		    //조회조건
			if(div != 'R'){
		    	frm.srchTitle.value = document.getElementById("sch1").value;
		    }else{
		    	frm.srchTitle.value = document.getElementById("sch3").value;
		    }   
		    if(div != 'C' && div != 'I'){
			    frm.srchStartDate.value = document.getElementById("sch5").value;
				frm.srchEndDate.value = document.getElementById("sch6").value;
			}
			if(div == 'M'){  // 음악
				
				frm.srchLicensor.value = document.getElementById("sch7").value;
				frm.srchProducer.value = document.getElementById("sch2").value;
				frm.srchAlbumTitle.value = document.getElementById("sch3").value;
				frm.srchSinger.value = document.getElementById("sch4").value;
				//frm.srchNonPerf.value = document.getElementById("sch8").value;
				
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=M";
				
			}else if(div == 'B'){  // 도서 
				
				frm.srchLicensor.value = document.getElementById("sch7").value;
				frm.srchPublisher.value = document.getElementById("sch2").value;
				frm.srchBookTitle.value = document.getElementById("sch3").value;
				frm.srchLicensorNm.value = document.getElementById("sch4").value;
				//frm.srchNoneName.value = document.getElementById("sch8").value;
				
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=B";
				
			}else if(div == 'C'){ // 방송대본 
		
				frm.srchWriter.value = document.getElementById("sch2").value;
				frm.srchBroadStatName.value = document.getElementById("sch3").value;
				frm.srchDirect.value = document.getElementById("sch4").value; 
				frm.srchPlayers.value = document.getElementById("sch7").value; 
				//frm.srchNoneName.value = document.getElementById("sch8").value;
				
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=C";
			
			}else if(div == 'I'){ // 이미지 
			
				frm.srchWorkName.value = document.getElementById("sch1").value;
				frm.srchWterDivs.value = document.getElementById("sch2").value;
				
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=I";
				
			}else if(div == 'V'){  // 영화 
			
				frm.srchDistributor.value = document.getElementById("sch2").value;
				frm.srchDirector.value = document.getElementById("sch3").value;
				frm.srchActor.value = document.getElementById("sch7").value;
				//frm.srchNoneName.value = document.getElementById("sch8").value;
				
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=V";
				
			}else if(div == 'R'){  // 방송 
			
				frm.srchProgName.value = document.getElementById("sch1").value;
				frm.srchMaker.value = document.getElementById("sch2").value;
			
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=R";	
				
			}else if(div == 'N'){
				frm.srchProviderNm.value = document.getElementById("sch2").value;//언론사명
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=N";
			}
			
			frm.method = "post";
		
			frm.submit();

		}
		else
		{
			return;
		}
		
	}
	else 
	{
		alert('항목을 선택 해 주세요.');
	}
}

// 리사이즈
function resizeIFrame(name) {

  var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
  document.getElementById(name).height= the_height+5;
 // document.getElementById(name).height= the_height;

}

// 선택된 저작물 추가
function fn_add(){
	
	var div = '${DIVS }';
		
	if(div == 'M'){
		
		//iFrame 항목
		var chk = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("iChk");
		var albumTitleObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumTitle");
		var issuedDateObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("issuedDate");
		var lyricistObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("lyricist");
		var composerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("composer");
		var singerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("singer");
		var producerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("producer");
		var crIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("crId");
		var nrIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("nrId");
		var albumIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumId");
		var arrangerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("arranger");
		var translatorObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("translator");
		var musicTitleObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("musicTitle");
		var playerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("player");
		var conductorObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("conductor");
		var featuringObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("featuring");
		var icnNumbObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("icnNumb");
		var albumProducedCrhObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumProducedCrh");
		//var nonPerfObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("nonPerf");
	
	}else if(div == 'B'){
	
		// 도서
		var chk = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("iChk");
		var titleObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("title");
		var bookTitleObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("bookTitle");
		var subTitleObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("subTitle");
		var issuedDateObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("issuedDate");
		var writerObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("writer");
		var translatorObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("translator");
		var publisherObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("publisher");		
		var crIdObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("crId");
		var nrIdObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("nrId");
		var publishTypeObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("publishType");
		var retrieveTypeObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("retrieveType");
		var icnNumbObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("icnNumb");
		
	}else if(div == 'C'){
	
		// 방송대본 
		var chk = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("iChk");
		var titleObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("title");
		var writerObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("writer");
		var subTitleObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("subTitle");
		var directObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("direct");
		var crhIdOfCaObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("crhIdOfCa");
		var insertDateObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("insertDate");
		var broadOrdObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadOrd");		
		var crIdObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("crId");
		var broadDateObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadDate");
		var broadMediNameObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadMediName");
		var broadStatNameObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadStatName");
		var playersObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("players");
		var makerObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("maker");
		var icnNumbObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("icnNumb");
		
	}else if(div == 'I'){
	
		// 이미지
		var chk = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("iChk");
		var imageSeqnObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("imageSeqn");
		var crIdObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("crId");
		var workNmObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("workName");
		var coptHodrObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("coptHodr");
		var lishCompObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("lishComp");
		var wterDivsObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("wterDivs");
		var usexYearObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("usexYear");
		var imageDivsObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("imageDivs");
		var icnNumbObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("icnNumb");
		var workFileNmObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("workFileName");
	
	}else if(div == 'V'){
	
		// 영화
		var chk = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("iChk");
		var mvieTitleObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("mvieTitle");
		var directorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("director");
		var leadingActorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("leadingActor");
		var viewGradeValueObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("viewGradeValue");
		var produceYearObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("produceYear");
		var producerObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("producer");	
		var distributorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("distributor");			
		var investorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("investor");
		var mediaCodeValueObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("mediaCodeValue");
		
		var crIdObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("crId");
		var nrIdObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("nrId");	
		var albumIdObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("albumId");
		
		//console.log("movie");
		//resizeDiv("tbl_rghtPrps", "div_rghtPrps");
		
	}else if(div == 'R'){
	
		// 방송 
		var chk = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("iChk");
		var titleObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("title");
		var progNameObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("progName");
		var progOrdSeqObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("progOrdSeq");
		var broadDateObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("broadDate");
		var mediCodeNameObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("mediCodeName");
		var chnlCodeNameObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("chnlCodeName");	
		var progGradObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("progGrad");			
		var makerObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("maker");

		var crIdObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("crId");
		var stdCrhIdObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("icnNumb");	

		//resizeDiv("tbl_rghtPrps", "div_rghtPrps");	
	}else if(div == 'N'){
	
		// 뉴스
		var chk = document.getElementById("ifNewsRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifNewsRghtSrch").contentWindow.document.getElementsByName("iChk");
		var titleObjs = document.getElementById("ifNewsRghtSrch").contentWindow.document.getElementsByName("title");
		var providerNameObjs = document.getElementById("ifNewsRghtSrch").contentWindow.document.getElementsByName("providerName");
		var articlPubcSdateObjs = document.getElementById("ifNewsRghtSrch").contentWindow.document.getElementsByName("articlPubcSdate");

		var crIdObjs = document.getElementById("ifNewsRghtSrch").contentWindow.document.getElementsByName("crId");

		//resizeDiv("tbl_rghtPrps", "div_rghtPrps");
	}
	
	// 선택 목록
	var selChkObjs = document.getElementsByName("chk");
	var selCrIdObjs = document.getElementsByName("crId");
	var selNrIdObjs = document.getElementsByName("nrId");
	var selAlbumIdObjs = document.getElementsByName("albumId");
	
	var params = "";
	var frm = document.frm;

	var isExistYn = "N";	//중복여부	
	var isAdd	= false;	//줄 추가여부
	
	var count = 0;
	
	for(var i = 0; i < chkObjs.length; i++) {

		if(chkObjs[i].checked == true && selChkObjs.length > 0) {
			//중복여부 검사
			for(var j=0; j<selChkObjs.length; j++) {
				if(div == 'M'  || div == 'V'){
					if(crIdObjs[i].value == selCrIdObjs[j].value && nrIdObjs[i].value == selNrIdObjs[j].value && albumIdObjs[i].value == selAlbumIdObjs[j].value) {
						isExistYn = "Y";
					}
				}else if(div == 'B'){
					if(crIdObjs[i].value == selCrIdObjs[j].value && nrIdObjs[i].value == selNrIdObjs[j].value ) {
						isExistYn = "Y";
					}
				}else if(div == 'I' || div == 'C' || div == 'R' || div == 'N' ){
					if(crIdObjs[i].value == selCrIdObjs[j].value) {
						isExistYn = "Y";
					}
				}
			}
			
			if(isExistYn == "N") {
				//줄 추가여부
				isAdd = true;
				count ++;
				
			}else{
				if(div == 'M'){
					alert("선택된 저작물 ["+musicTitleObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'B'){
					alert("선택된 저작물 ["+bookTitleObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'C'){
					alert("선택된 저작물 ["+titleObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'I'){
					alert("선택된 저작물 ["+workNmObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'V'){
					alert("선택된 저작물 ["+mvieTitleObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'R'){
					alert("선택된 저작물 ["+progNameObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'N'){
					alert("선택된 저작물 ["+titleObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}
				return;
			}
			
		} else if(chkObjs[i].checked == true && selChkObjs.length == 0) { //권리찾기 선택목록이 비웠을때 
			//기본 알림줄 삭제(선택된 목록이 없습니다.)
			var oDummyTr = document.getElementById("dummyTr")
			if(eval(oDummyTr)) 	oDummyTr.parentNode.removeChild(oDummyTr);

			//줄 추가여부		
			isAdd = true;
			count ++;
			
		}
		
		chkObjs[i].checked = false;  // 처리한 후 체크풀기 
		
		//줄 추가실행
		if(isAdd){
			var tbody = document.getElementById("tbl_rghtPrps").getElementsByTagName("TBODY")[0];
			
			var row = document.createElement("TR");
			//tr에 id 지정
			var nLastIdx	= 0;	//현재 대상 테이블 tr의 최대번호을 담음
			var nIdIdx;			//현재 대상 테이블에 있는 tr의 Id 번호를 담음
			var oTrInfo = document.getElementById("tbl_rghtPrps").getElementsByTagName("tr");
			for(h = 0; h < oTrInfo.length; h++){
				if(oTrInfo[h].id != "undefined" && oTrInfo[h].id != ""){
					nIdIdx = Number((oTrInfo[h].id).substr("tbl_rghtPrps".length, oTrInfo[h].id.length));
					if(nLastIdx < nIdIdx)	nLastIdx = nIdIdx;
				}
			}
			
			var sNewId = "tbl_rghtPrps" + (nLastIdx + 1);		//tr에 지정할 id
		    row.id = sNewId;
		    
			if(div == 'M'){
			
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				var td7 = document.createElement("TD");
				//var td8 = document.createElement("TD");

				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk"           value="'+crIdObjs[i].value+'|'+nrIdObjs[i].value+'|'+albumIdObjs[i].value+'" style="cursor:pointer;" title="선택">';
				tdData0 += '<input type="hidden" name="musicTitle"        value="'+ musicTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="albumTitle"        value="'+ albumTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="issuedDate"        value="'+ issuedDateObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="lyricist"            value="'+ lyricistObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="composer"            value="'+ composerObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="singer"        value="'+ singerObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="producer"            value="'+ producerObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="nrId"           value="'+ nrIdObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="albumId"        value="'+ albumIdObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="arranger"        value="'+ arrangerObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="translator"        value="'+ translatorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="player"        value="'+ playerObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="conductor"        value="'+ conductorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="featuring"        value="'+ featuringObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="icnNumb"        value="'+ icnNumbObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="albumProducedCrh"        value="'+ albumProducedCrhObjs[i].value     +'">';
				//tdData0 += '<input type="hidden" name="nonPerf"        value="'+ nonPerfObjs[i].value     +'">';
				
				
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = musicTitleObjs[i].value;
				td2.innerHTML = albumTitleObjs[i].value;
				td3.innerHTML = issuedDateObjs[i].value;
				td3.className = 'ce';
				td4.innerHTML = lyricistObjs[i].value;
				td4.className = 'ce';
				td5.innerHTML = composerObjs[i].value;
				td5.className = 'ce';
				td6.innerHTML = singerObjs[i].value;
				td6.className = 'ce';
				//td7.innerHTML = producerObjs[i].value;
				//td7.className = 'ce';
				//td8.innerHTML = nonPerfObjs[i].value;
				//td8.className = 'ce';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				//row.appendChild(td7); 
				//row.appendChild(td8); 
				
			}else if(div == 'B'){
				
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				
				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk" value="'+crIdObjs[i].value+'|'+nrIdObjs[i].value+'" style="cursor:pointer;" title="선택">';
				tdData0 += '<input type="hidden" name="title"        value="'+ titleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="bookTitle"        value="'+ bookTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="subTitle"        value="'+ subTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="issuedDate"        value="'+ issuedDateObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="writer"            value="'+ writerObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="translator"        value="'+ translatorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="publisher"        value="'+ publisherObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="nrId"           value="'+ nrIdObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="publishType"        value="'+ publishTypeObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="retrieveType"        value="'+ retrieveTypeObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="icnNumb"        value="'+ icnNumbObjs[i].value     +'">';
				
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = titleObjs[i].value;
				td2.innerHTML = bookTitleObjs[i].value;
				td3.innerHTML = issuedDateObjs[i].value;
				td3.className = 'ce';
				td4.innerHTML = writerObjs[i].value;
				td4.className = 'ce';
				td5.innerHTML = translatorObjs[i].value;
				td5.className = 'ce';
				td6.innerHTML = publisherObjs[i].value;
				td6.className = 'ce';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				
			}else if(div == 'C'){
				
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				var td7 = document.createElement("TD");
				var td8 = document.createElement("TD");
				var td9 = document.createElement("TD");
				
				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk" value="'+crIdObjs[i].value+'" style="cursor:pointer;" title="선택">';
				tdData0 += '<input type="hidden" name="title"        value="'+ titleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="writer"        value="'+ writerObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="subTitle"        value="'+ subTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="direct"            value="'+ directObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="crhIdOfCa"        value="'+ crhIdOfCaObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="insertDate"        value="'+ insertDateObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="broadOrd"        value="'+ broadOrdObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="broadDate"        value="'+ broadDateObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="broadMediName" value="'+ broadMediNameObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="broadStatName" value="'+ broadStatNameObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="players" value="'+ playersObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="maker" value="'+ makerObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="icnNumb" value="'+ icnNumbObjs[i].value        +'">';
								
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = titleObjs[i].value;
				td2.innerHTML = writerObjs[i].value;
				td3.innerHTML = directObjs[i].value;
				td3.className = 'ce';
				td4.innerHTML = broadOrdObjs[i].value;
				td4.className = 'ce';
				td5.innerHTML = broadDateObjs[i].value;
				td5.className = 'ce';
				td6.innerHTML = broadMediNameObjs[i].value;
				td6.className = 'ce';
				td7.innerHTML = broadStatNameObjs[i].value;
				td7.className = 'ce';
				td8.innerHTML = playersObjs[i].value;
				td8.className = 'ce';
				//td9.innerHTML = makerObjs[i].value;
				//td9.className = 'ce';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				row.appendChild(td7); 
				row.appendChild(td8); 
				//row.appendChild(td9); 
				
			} else if(div == 'I'){
				
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				
				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk" value="'+crIdObjs[i].value+'" style="cursor:pointer;" title="선택">';
				tdData0 += '<input type="hidden" name="imageSeqn"        value="'+ imageSeqnObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="workName"        value="'+ workNmObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="coptHodr"        value="'+ coptHodrObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="lishComp"        value="'+ lishCompObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="wterDivs"            value="'+ wterDivsObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="usexYear"        value="'+ usexYearObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="imageDivs"        value="'+ imageDivsObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="icnNumb"        value="'+ icnNumbObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="workFileName"           value="'+ workFileNmObjs[i].value        +'">';
				
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = workNmObjs[i].value;
				td2.innerHTML = lishCompObjs[i].value;
				td3.innerHTML = wterDivsObjs[i].value;
				td3.className = 'ce';
				td4.innerHTML = usexYearObjs[i].value;
				td4.className = 'ce';
				td5.innerHTML = coptHodrObjs[i].value;
				td5.className = 'ce';
				td6.innerHTML = imageDivsObjs[i].value;
				td6.className = 'ce';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				
			}else if(div == 'V'){
				
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				//var td7 = document.createElement("TD");
				//var td8 = document.createElement("TD");
				//var td9 = document.createElement("TD");
				
				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk" value="'+crIdObjs[i].value+'|'+nrIdObjs[i].value+'|'+albumIdObjs[i].value+'" style="cursor:pointer;" title="선택">';
				tdData0 += '<input type="hidden" name="mvieTitle"        value="'+ mvieTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="director"        value="'+ directorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="leadingActor"        value="'+ leadingActorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="viewGradeValue"            value="'+ viewGradeValueObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="produceYear"        value="'+ produceYearObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="producer"        value="'+ producerObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="distributor"        value="'+ distributorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="investor"        value="'+ investorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="mediaCodeValue"        value="'+ mediaCodeValueObjs[i].value     +'">';	
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="nrId"           value="'+ nrIdObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="albumId"           value="'+ albumIdObjs[i].value        +'">';
				
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = mvieTitleObjs[i].value;
				td2.innerHTML = directorObjs[i].value;
				td3.innerHTML = leadingActorObjs[i].value;
				
				td4.innerHTML = produceYearObjs[i].value;
				td4.className = 'ce';
				td5.innerHTML = mediaCodeValueObjs[i].value;
				td5.className = 'ce';
				td6.innerHTML = viewGradeValueObjs[i].value;
				td6.className = 'ce';
				//td7.innerHTML = producerObjs[i].value;
				//td7.className = 'ce';
				//td8.innerHTML = distributorObjs[i].value;
				//td8.className = 'ce';
				//td9.innerHTML = investorObjs[i].value;
				//td9.className = 'ce';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				//row.appendChild(td7); 
				//row.appendChild(td8); 
				//row.appendChild(td9); 
				
			}else if(div == 'R'){
			
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				//var td7 = document.createElement("TD");
				//var td8 = document.createElement("TD");

				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk"           value="'+crIdObjs[i].value+'" style="cursor:pointer;" title="선택">';
				tdData0 += '<input type="hidden" name="title"        value="'+ titleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="progName"        value="'+ progNameObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="progOrdSeq"        value="'+ progOrdSeqObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="broadDate"            value="'+ broadDateObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="mediCodeName"            value="'+ mediCodeNameObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="chnlCodeName"        value="'+ chnlCodeNameObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="progGrad"            value="'+ progGradObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="maker"           value="'+ makerObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="icnNumb"        value="'+ stdCrhIdObjs[i].value     +'">';
				
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = titleObjs[i].value;
				td2.innerHTML = progNameObjs[i].value;
				td3.innerHTML = progOrdSeqObjs[i].value;
				td3.className = 'ce';
				td4.innerHTML = broadDateObjs[i].value;
				td4.className = 'ce';
				td5.innerHTML = mediCodeNameObjs[i].value;
				td5.className = 'ce';
				td6.innerHTML = chnlCodeNameObjs[i].value;
				td6.className = 'ce';
				//td7.innerHTML = progGradObjs[i].value;
				//td7.className = 'ce';
				//td8.innerHTML = makerObjs[i].value;
				//td8.className = 'ce';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				//row.appendChild(td7); 
				//row.appendChild(td8); 
				
			}else if(div == 'N'){
			
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 

				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk"           value="'+crIdObjs[i].value+'" style="cursor:pointer;" title="선택">';
				tdData0 += '<input type="hidden" name="title"        value="'+ titleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="providerName"        value="'+ providerNameObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="articlPubcSdate"        value="'+ articlPubcSdateObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = titleObjs[i].value;
				td2.innerHTML = providerNameObjs[i].value;
				td2.className = 'ce';
				td3.innerHTML = articlPubcSdateObjs[i].value;
				td3.className = 'ce';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				
			} 
			
			tbody.appendChild(row); 
			isAdd = false;
		
		}	
	}	
	
	// 전체선택 해제 
	chk.checked = false;
	
	if(count == 0){
		alert('선택된 저작물이 없습니다.');
		return;
	}	
	
	//높이 조절
	resizeSelTbl();
	
}

// 선택된 저작물 삭제
function fn_delete(){
	
	//선택 목록
	var subChk = document.getElementById("subChk");
	var chkObjs = document.getElementsByName("chk");
	var sCheck = 0;
	
	for(i=0; i<chkObjs.length;i++){
		if(chkObjs[i].checked){
			sCheck = 1;
		}
	}
	
	if(sCheck == 0 ){
		alert('선택된 저작물이 없습니다.');
		return;
	}

	for(var i=chkObjs.length-1; i >= 0; i--){

		var chkObj = chkObjs[i];

		if(chkObj.checked){
			// 선택된 저작물을 삭제한다.
			var oTR = findParentTag(chkObj, "TR");	
			if(eval(oTR)) 	oTR.parentNode.removeChild(oTR);
		}
	}
	
	// 전체선택 해제 
	subChk.checked = false;
	
	//높이 조절
	resizeSelTbl();
}

//높이 조절
function resizeSelTbl(){
	//var chkObjs = document.getElementsByName("chk");
	
	if("${DIVS}" == 'M' || "${DIVS}" == 'N' || "${DIVS}" == 'C' || "${DIVS}" == 'V' || "${DIVS}" == 'R' || "${DIVS}" == 'B' || "${DIVS}" == 'I'){
		resizeDiv("tbl_rghtPrps", "div_rghtPrps");
	}
	//document.getElementById("div_rghtPrps").style.height = (31.5 * (chkObjs.length + 1)) + 13 + "px" ;
	
}

//table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight; //해당 Div의 높이
   var chkId = "chk"//체크박스 네임
   var chkCnt = document.getElementsByName(chkId).length;//현재건수
   var isLong = false;//현재 건수가 15는 넘었는지
   var default_height = 493;
   
   if(chkCnt > 15){
   	document.getElementById(targetName).style.height = default_height+"px";
   	document.getElementById(targetName).style.overflowY = "auto";
   }else{
   	document.getElementById(targetName).style.height = "auto";
   	document.getElementById(targetName).style.overflowY = "hidden";
   }
   //console.log(chkCnt+"개");
   //console.log("the_height[현재의높이는]: "+the_height);
   //console.log("default_height[현재의높이는]: "+default_height);
   
   //document.getElementById(targetName).style.height = the_height+13;

}

var gubun = "${gubun}";

function alram(){

	// 통합검색에서 넘어온경우 알림메시지 
	//if("${gubun}" == "totalSearch" ){
	if(gubun == "totalSearch" ){
		
		gubun= '';
	//	hideAjaxBox();
		
		var srchTitle =  fncReplaceStr("${srchTitle}", "&quot;", "\"");
		var mesg = srchTitle+" (으)로 ";      
	
		var srchLicensor = fncReplaceStr("${srchLicensor}", "&quot;", "\"");
		var srchLicensorNm = fncReplaceStr("${srchLicensorNm}", "&quot;", "\"");
		      
		
	
		if("${DIVS}" == 'M'){
			if("${srchTitle}"!='' ) mesg+= "곡명";
			if("${srchLicensor}"!='' ) mesg= "["+srchLicensor+"] (으)로 "+"작사/작곡/편곡";
		}
		if("${DIVS}" == 'B'){
			if("${srchTitle}"!='' ) mesg+= "작품명";
			if("${srchLicensorNm}"!='' ) mesg= "["+srchLicensorNm+"] (으)로 "+"작가명"; 
		}
		if("${DIVS}" == 'C')	mesg+= "작품명";
		if("${DIVS}" == 'I')	mesg+= "이미지명";
		if("${DIVS}" == 'V')	mesg+= "영화명";
		if("${DIVS}" == 'R')	mesg+= "프로그램명";
		if("${DIVS}" == 'N')	mesg+= "뉴스기사제목";
		
		mesg += "(이)가 검색됩니다.\n검색항목 입력으로 상세검색이 가능합니다. ";
		
		alert( mesg);
	}
}

window.onload =	function(){

	if("${DIVS}" != 'X'){
		fn_InitframeList();		//검색조건이 없는 첫화면 조회 //20120220 정병호
	}
	

	if("${DIVS}" == 'M' || "${DIVS}" == 'N' || "${DIVS}" == 'C' || "${DIVS}" == 'V' || "${DIVS}" == 'R'){
		//resizeDiv("tbl_rghtPrps", "div_rghtPrps");
		//console.log("onLoad");
	}
	
}

//
--></script>
<style type="text/css">
<!--
.box {
	background-color:black;
	position:absolute;
	z-index:99999999;
	display:none;
}

body {
	overflow-x: auto;  
	overflow-y: auto;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}

.w92{width:91%;}
.w26{width:26%;}
-->
</style>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">

		<!-- HEADER str-->
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(2);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_2.gif" alt="내권리찾기" title="내권리찾기" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>

			<div class="content">
			
			<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu02.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb1","lnb12");</script>
			<!-- //래프 -->
				
				<div id="ajaxBox" style="position:absolute; z-index:99999999999; background: url(/images/2012/common/loadingBg.gif) no-repeat 0 0; left:-500px; width: 402px; height: 56px; padding: 102px 0 0 0;">
				</div>
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
	
					<c:set var="divName" value="서비스 이용"/>
					<c:set var="imgPath" value="/images/2012/title/content_h1_0202.gif"/>

					
					<p class="path"><span>Home</span><span>내권리찾기</span><span>저작권 정보 확인</span>
						<em>${divName}</em>
					</p>
					
					<h1><img src="${imgPath}" alt="${divName}" title="${divName}" /></h1>
					
					 <div class="section"> 
		
					<!-- Tab str -->
                          <ul id="tab11" class="tab_menuBg">
                           <!--    <li class="first"><a href="/mlsInfo/rghtInfo01.jsp">소개</a></li>
                              <li><a href="/mlsInfo/rghtInfo02.jsp">이용방법</a></li> -->
                              <li class="first"><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악</a></li>
							  <li><a href="/rghtPrps/rghtSrch.do?DIVS=B">어문</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=N">뉴스</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송</a></li>
                      		</ul>
                      <!-- //Tab -->
                                   
						
					<!-- hidden form str : 아이프레임데이타 가져가는 용도-->		
					<form name="hidForm" action="#" class="sch">
						<input type="hidden" name="userIdnt" />
						<input type="hidden" name="ifrmInput" />
						<input type="hidden" name="PRPS_RGHT_CODE" />
						<input type="hidden" name="srchTitle" />
						<input type="hidden" name="srchProducer" />
						<input type="hidden" name="srchAlbumTitle" />
						<input type="hidden" name="srchSinger" />
						<input type="hidden" name="srchStartDate" />
						<input type="hidden" name="srchEndDate" />
						<!-- 음악 -->
						<input type="hidden" name="srchNonPerf" />
						<!-- 도서 -->
						<input type="hidden" name="srchLicensor" />
						<input type="hidden" name="srchPublisher" />
						<input type="hidden" name="srchBookTitle" />
						<input type="hidden" name="srchLicensorNm" />
						<!-- 방송대본 -->
						<input type="hidden" name="srchWriter" />
						<input type="hidden" name="srchBroadStatName" />
						<input type="hidden" name="srchDirect" />
						<input type="hidden" name="srchPlayers" />
						<!-- 이미지 -->
						<input type="hidden" name="srchWorkName" />
						<input type="hidden" name="srchLishComp" />
						<input type="hidden" name="srchCoptHodr" />
						<input type="hidden" name="srchWterDivs" />
						<!-- 영화 -->
						<input type="hidden" name="srchDistributor" />
						<input type="hidden" name="srchDirector" />
						<input type="hidden" name="srchViewGrade" />
						<input type="hidden" name="srchActor" />
						<!-- 방송 -->
						<input type="hidden" name="srchProgName" />
						<input type="hidden" name="srchMaker" />
						
						<!-- 뉴스 -->
						<input type="hidden" name="srchProviderNm" />
						<input type="submit" style="display:none;">
					</form>
					
					<c:if test="${DIVS != 'X'}">
						<!-- 검색 -->
						<form name="frm" action="#">
							<fieldset class="w100 mt5 relative">
								<!-- 연락처 박스  -->
									<jsp:include page="/common/memo/2011/memo_01.jsp">
											<jsp:param name="DIVS" value="${DIVS}" />
									</jsp:include>
								<!-- //연락처 박스 -->
							<legend></legend>
								<div class="boxStyle mt15">
									<div class="box1 floatDiv">
										<div class="fl w85">
											<p class="fl mt5 w15"><img src="/images/2012/content/sch_txt.gif" alt="Search" title="Search"></p>
											
											<table class="fl schBoxGrid w85" summary="">
												<caption></caption>
												<colgroup><col width="20%"><col width="25%"><col width="15%"><col width="*"></colgroup>
												<tbody>
												<c:if test="${DIVS == 'M'}">
													<tr>
														<th scope="row"><label for="sch7">작사/작곡/편곡</label></th>
														<td><input type="text" id="sch7" class="w80" name="srchLicensor" value="${srchLicensor}" /></td>
														<th scope="row"><label for="sch1">곡명</label></th>
														<td><input type="text" id="sch1" class="w80" name="srchTitle" value="${srchTitle}" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="sch4">가창/연주/지휘</label></th>
														<td><input type="text" id="sch4" class="w80" name="srchSinger" value="${srchSinger}" /></td>
														<th scope="row"><label for="sch3">앨범명</label></th>
														<td><input type="text" id="sch3" class="w80" name="srchAlbumTitle" value="${srchAlbumTitle}" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="sch2">음반제작사</label></th>
														<td><input type="text" id="sch2" class="w80" name="srchProducer" value="${srchProducer}" /></td>
														<th scope="row"><label for="sch5">발매일자</label></th>
														<td>
														<input type="text" class="w25" id="sch5" name="srchStartDate" title="발매일(시작)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchStartDate}" />
														<img title="시작일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" onkeypress="javascript:fn_cal('frm','srchStartDate');" align="middle" style="cursor:pointer;">
														~<input type="text" id="sch6" class="w25 ml10" name="srchEndDate" title="발매일(종료)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchEndDate}" >
														<img title="마지막일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" onkeypress="javascript:fn_cal('frm','srchEndDate');" align="middle" style="cursor:pointer;" /></td>
													</tr>
												</c:if>
												<c:if test="${DIVS == 'B'}">
													<tr>
														<th scope="row"><label for="sch4">작가명</label></th>
														<td><input type="text" id="sch4" class="w80"  name="srchLicensorNm" value="${srchLicensorNm}" / /></td>
														<th scope="row"><label for="sch1">작품명</label></th>
														<td><input type="text" id="sch1" class="w80" name="srchTitle" value="${srchTitle}" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="sch7">역자명</label></th>
														<td><input type="text" id="sch7" class="w80" name="srchLicensor" value="${srchLicensor}" /></td>
														<th scope="row"><label for="sch3">도서명</label></th>
														<td><input type="text" id="sch3" class="w80"  name="srchBookTitle" value="${srchBookTitle}" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="sch2">출판사</label></th>
														<td><input type="text" id="sch2" class="w80" name="srchPublisher" value="${srchPublisher}" /></td>
														<th scope="row"><label for="sch5">발매일자</label></th>
														<td>
														<input type="text" class="w25" id="sch5" name="srchStartDate" title="발매일(시작)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchStartDate}" />
														<img title="시작일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" onkeypress="javascript:fn_cal('frm','srchStartDate');" align="middle" style="cursor:pointer;">
														~<input type="text" class="w25 ml10" id="sch6" name="srchEndDate" title="발매일(종료)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchEndDate}" >
														<img title="마지막일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" onkeypress="javascript:fn_cal('frm','srchEndDate');" align="middle" style="cursor:pointer;" /></td>
													</tr>
												</c:if>
												<c:if test="${DIVS == 'N'}">
													<tr>
														<th scope="row"><label for="sch2">언론사명</label></th>
														<td><input type="text" id="sch2" class="w80"  name="srchProviderNm" value="${srchProviderNm}" / /></td>
														<th scope="row"><label for="sch1">뉴스기사제목</label></th>
														<td><input type="text" id="sch1" class="w80" name="srchTitle" value="${srchTitle}" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="sch5">기사일자</label></th>
														<td colspan="3">
														<input type="text" class="w12" id="sch5" name="srchStartDate" title="기사일(시작)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchStartDate}" />
														<img title="시작일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" onkeypress="javascript:fn_cal('frm','srchStartDate');" align="middle" style="cursor:pointer;">
														~<input type="text" class="w12 ml10" id="sch6" name="srchEndDate" title="기사일(종료)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchEndDate}" >
														<img title="마지막일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" onkeypress="javascript:fn_cal('frm','srchEndDate');" align="middle" style="cursor:pointer;" /></td>
													</tr>
												</c:if>
												<c:if test="${DIVS == 'C'}">
													<tr>
														<th scope="row"><label for="sch2">작가명</label></th>
														<td><input type="text" class="w80" id="sch2" name="srchWriter" value="${srchWriter}" /></td>
														<th scope="row"><label for="sch1">작품명</label></th>
														<td><input type="text" class="w80" id="sch1" name="srchTitle" value="${srchTitle}" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="sch4">연출가</label></th>
														<td><input type="text" class="w80" id="sch4" name="srchDirect" value="${srchDirect}" /></td>
														<th scope="row"><label for="sch3">방송사</label></th>
														<td><input type="text" class="w80" id="sch3" name="srchBroadStatName" value="${srchBroadStatName}" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="sch7">주요출연진</label></th>
														<td><input type="text" class="w80" id="sch7" name="srchPlayers" value="${srchPlayers}" /></td>
														<th scope="row"><label for="sch5">방송일자</label></th>
														<td>
														<input type="text" class="w25" id="sch5" name="srchStartDate" title="방송일자(시작)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchStartDate}" />
														<img title="시작일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" onkeypress="javascript:fn_cal('frm','srchStartDate');" align="middle" style="cursor:pointer;">
														~<input type="text" id="sch6" class="w25 ml10" name="srchEndDate" title="방송일자(종료)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchEndDate}" >
														<img title="마지막일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" onkeypress="javascript:fn_cal('frm','srchEndDate');" align="middle" style="cursor:pointer;" /></td>
													</tr>
												</c:if>
												<c:if test="${DIVS == 'I'}">
													<tr>
														<th scope="row"><label for="sch2">작가명</label></th>
														<td><input type="text" class="w80" id="sch2" name="srchWterDivs" value="${srchWterDivs}" /></td>
														<th scope="row"><label for="sch1">이미지명</label></th>
														<td><input type="text" class="w80" id="sch1" name="srchWorkName" value="${srchWorkName}"/></td>
													</tr>
												</c:if>
												<c:if test="${DIVS == 'V'}">
													<tr>
														<th scope="row"><label for="sch3">감독/연출</label></th>
														<td><input type="text" class="w80" id="sch3" name="srchDirector" value="${srchDirector}" /></td>
														<th scope="row"><label for="sch1">영화명</label></th>
														<td><input type="text" class="w80" id="sch1" name="srchTitle" value="${srchTitle}" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="sch2">제작사/배급사/투자사</label></th>
														<td colspan="3"><input type="text" class="w92" id="sch2" name="srchDistributor" value="${srchDistributor}" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="sch7">주요출연진</label></th>
														<td><input type="text" class="w80" id="sch7" name="srchActor" value="${srchActor}" /></td>
														<th scope="row"><label for="sch5">제작일자</label></th>
														<td>
														<input type="text" class="w26" id="sch5" name="srchStartDate" title="제작일자(시작)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchStartDate}" />
														<img title="시작일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" onkeypress="javascript:fn_cal('frm','srchStartDate');" align="middle" style="cursor:pointer;">
														~<input type="text" id="sch6" class="w26 ml10" name="srchEndDate" title="제작일자(종료)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchEndDate}" >
														<img title="마지막일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" onkeypress="javascript:fn_cal('frm','srchEndDate');" align="middle" style="cursor:pointer;" /></td>
													</tr>
												</c:if>
												<c:if test="${DIVS == 'R'}">
													<tr>
														<th scope="row"><label for="sch2">제작자</label></th>
														<td><input type="text" class="w80" id="sch2" name="srchMaker" value="${srchMaker}" /></td>
														<th scope="row"><label for="sch1">프로그램명</label></th>
														<td><input type="text" class="w80" id="sch1" name="srchProgName" value="${srchProgName}" /></td>
													</tr>
													<tr>
														<th scope="row"><label for="sch3">저작물명</label></th>
														<td><input type="text" class="w80" id="sch3" name="srchTitle" value="${srchTitle}" /></td>
														<th scope="row"><label for="sch5">방송일자</label></th>
														<td>
															<input type="text" class="w25" id="sch5" name="srchStartDate" title="방송일자(시작)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchStartDate}" />
															<img title="시작일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" onkeypress="javascript:fn_cal('frm','srchStartDate');" align="middle" style="cursor:pointer;" />
															~<input type="text" id="sch6" class="w25 ml10" name="srchEndDate" title="방송일자(종료)" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchEndDate}" />
															<img title="마지막일시를 선택하세요." alt="" class="vmid" src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" onkeypress="javascript:fn_cal('frm','srchEndDate');" align="middle" style="cursor:pointer;" />
														</td>
													</tr>
												</c:if> 
												</tbody>
											</table>
											<div>
												<p class="gray_box_line">&lowast; 검색은 원하는 항목에 찾고자 하는 검색어를 기입한 후 오른쪽 ‘조회’ 버튼을 클릭하세요.<a href="#1" onclick="javascript:toggleLayer('help_pop1');" class="ml10 underline black2">도움말<img src="/images/2012/common/ic_help.gif" class="vmid ml5" alt="" /></a></p>
												<!-- 도움말 레이어 -->
												<div class="layer_pop w80" id="help_pop1">
													<h1>검색도움말</h1>
													<c:if test="${DIVS == 'M'}">
														<div class="layer_con">
														<ul class="list1">
														<li class="p11"><strong>작사/작곡/편곡</strong>: 각 항목에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11"><strong>가창/연주/지휘</strong>: 각 항목에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11"><strong>발매일자</strong>: 앨범발매일자를 의미하며 직접 입력하여 검색 할 경우 날짜형식은 ‘20110815’와 같이 년,월,일을 차례로 입력합니다. </li>
														</ul>
													</div>
													</c:if>
													<c:if test="${DIVS == 'B'}">
														<div class="layer_con">
														<ul class="list1">
														<li class="p11"><strong>작가명/역자명/출판사</strong>: 각 항목에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11"><strong>발행일자</strong>: 도서의 최초발행일자를 의미하며 직접 입력하여 검색 할 경우 날짜형식은 ‘20110815’와 같이 년,월,일을 차례로 입력합니다.</li>
														</ul>
													</div>
													</c:if>
													<c:if test="${DIVS == 'N'}">
														<div class="layer_con">
														<ul class="list1">
														<li class="p11"><strong>언론사명</strong>: 언론사명에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11"><strong>기사일자</strong>: 뉴스기사일자를 의미하며 직접 입력하여 검색 할 경우 날짜형식은 ‘20110815’와 같이 년,월,일을 차례로 입력합니다.</li>
														</ul>
													</div>
													</c:if>
													<c:if test="${DIVS == 'C'}">
														<div class="layer_con">
														<ul class="list1">
														<li class="p11"><strong>작가명/연출가/주요출연진</strong>: 각 항목에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11"><strong>방송일자</strong>: 직접 입력하여 검색 할 경우 날짜형식은 ‘20110815’와 같이 년,월,일을 차례로 입력합니다.</li>
														</ul>
													</div>
													</c:if>
													<c:if test="${DIVS == 'I'}">
														<div class="layer_con">
														<ul class="list1">
														<li class="p11"><strong>작가명</strong>: 작가에 해당되는 데이터를 검색 합니다.</li>
														</ul>
													</div>
													</c:if>
													<c:if test="${DIVS == 'V'}">
														<div class="layer_con">
														<ul class="list1">
														<li class="p11"><strong>감독/연출</strong>: 각 항목에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11"><strong>제작사/배급사/투자사</strong>: 각 항목에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11"><strong>주요출연진</strong>: 주요출연진에 해당되는 데이터를 검색합니다.</li>
														<li class="p11"><strong>제작일자</strong>: 영화제작일자를 의미하며 직접 입력하여 검색 할 경우 날짜형식은 ‘20110815’와 같이 년,월,일을 차례로 입력합니다.</li>
														</ul>
													</div>
													</c:if>
													<c:if test="${DIVS == 'R'}">
														<div class="layer_con">
														<ul class="list1">
														<li class="p11"><strong>제작자</strong>: 제작자에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11"><strong>방송일자</strong>: 프로그램 방송일자를 의미하며 직접 입력하여 검색 할 경우 날짜형식은 ‘20110815’와 같이 년,월,일을 차례로 입력합니다.</li>
														</ul>
													</div>
													</c:if>
													
													<a href="#1" onclick="javascript:toggleLayer('help_pop1');" class="layer_close"><img src="/images/2012/button/layer_close.gif" alt="" /></a>
												</div>
												<!-- //도움말 레이어 -->
												
											</div>
											
										</div>
										
										<p class="fl btn_area pt75">
											<input type="image" src="/images/2012/button/sch.gif" 
													onclick="javascript:fn_frameList();" 
													onkeypress="javascript:fn_frameList();" 
													alt="검색" title="검색">
									</p>
									</div>
									
									<span class="btmRound lftTop"></span>
									<span class="btmRound rgtTop"></span>
									<span class="btmRound"></span>
									<span class="btmRound rgt"></span>
								</div>
								
							</fieldset>

							<!-- //검색 -->
							<div class="floatDiv mt20 mb5">
								<input type="hidden" name="totalRow" id="totalRow" value="" style="border:0px;font-size:12px;font-weight:700;color:black;"/>
								<!-- <p class="fl">
									<a href="#1" onclick="javascript:fn_add();" title="선택한 항목을 [저작권찾기 선택목록]에 추가합니다."><img src="/images/2012/button/add_down.gif" alt="추가" /></a>
								</p> -->
								<p class="fr rgt">
									<!--<c:if test="${DIVS == 'M'}">
										<a href="#1" onclick="javascript:goClms('music');" onkeypress="javascript:goClms('music');">
											<img src="/images/2012/button/btn_app1.gif" alt="저작권 이용계약 신청" title="저작권 이용계약 신청" />
										</a>
									</c:if>
									<c:if test="${DIVS == 'B'}">
										<a href="#1" onclick="javascript:goClms('book');" onkeypress="javascript:goClms('book');">
											<img src="/images/2012/button/btn_app1.gif" alt="저작권 이용계약 신청" title="저작권 이용계약 신청" />
										</a>
									</c:if>
							   <a href="#1" onclick="javascript:goRghtPrpsMain('mainList', '1');" 
											onkeypress="javascript:openRghtPrps('mainList', '1');">
										<img src="/images/2012/button/btn_app2.gif" alt="권리자 저작권찾기 신청" title="권리자 저작권찾기 신청" />
									</a>
									 <a href="#1" onclick="javascript:goRghtPrpsMain('mainList', '3');" 
											onkeypress="javascript:openRghtPrps('mainList', '3');">
										<img src="/images/2012/button/btn_app3.gif" alt="이용자 저작권찾기 신청" title="이용자 저작권찾기 신청" />
									</a> -->
								</p>
							</div>
						
							<c:if test="${DIVS == 'M'}">
								<iframe id="ifMuscRghtSrch" title="저작물조회(음악)" name="muscRghtSrch" 
									width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
									scrolling="no" style="overflow-y:hidden;">
								</iframe>
							</c:if>
							<c:if test="${DIVS == 'B'}">
								<iframe id="ifBookRghtSrch" title="저작물조회(도서)" name="bookRghtSrch" 
									width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
									scrolling="no" style="overflow-y:hidden;">
								</iframe>
							</c:if>
							<c:if test="${DIVS == 'N'}">
								<iframe id="ifNewsRghtSrch" title="저작물조회(뉴스)" name="newsRghtSrch" 
									width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
									scrolling="no" style="overflow-y:hidden;">
								</iframe>
							</c:if>
							<c:if test="${DIVS == 'C'}">
								<iframe id="ifScriptRghtSrch" title="저작물조회(방송대본)" name="scriptRghtSrch" 
									width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
									scrolling="no">
								</iframe>
							</c:if>
							<c:if test="${DIVS == 'I'}">
								<iframe id="ifImageRghtSrch" title="저작물조회(이미지)" name="imageRghtSrch" 
									width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
									scrolling="no" style="overflow-y:hidden;">
								</iframe>
							</c:if>
							<c:if test="${DIVS == 'V'}">
								<iframe id="ifMvieRghtSrch" title="저작물조회(영화)" name="mvieRghtSrch" 
									width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
									scrolling="no" style="overflow-y:hidden;">
								</iframe>
							</c:if>
							<c:if test="${DIVS == 'R'}">
								<iframe id="ifBroadcastRghtSrch" title="저작물조회(방송)" name="broadcastRghtSrch" 
									width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
									scrolling="no" style="overflow-y:hidden;">
								</iframe>
							</c:if>
							<!-- iframe end -->
							<%--<div class="floatDiv mt0 mb5">
								<p class="fl">
									<a href="#1" onclick="javascript:fn_add();" title="선택한 항목을 [저작권찾기 선택목록]에 추가합니다."><img src="/images/2012/button/add_down.gif" alt="추가" /></a>
									<span class="blue2 p11 ml5">&lowast; 상단  조회목록에서 선택하고  [추가]를 하면, 하단 선택목록에 담겨져서 한번에 신청이 됩니다.</span>
								</p>
							</div>
							<p class="HBar mt15 mb5">&nbsp;</p>
							<div class="floatDiv">
							<h2 class="fl">
									<c:if test="${DIVS != 'X'}">${divName}</c:if> 저작물 선택목록
								</h2>
							</div>
							<div class="floatDiv">
							<p class="fl mt5">
								<a href="#1" onclick="javascript:fn_delete();"><img src="/images/2012/button/delete.gif" class="mb5" alt="삭제" /></a>
							</p>
							<p class="fr">
								<a href="#1" onclick="javascript:goRghtPrpsMain('subList', '1');" 
									onkeypress="javascript:openRghtPrps('subList', '1');">
								<img src="/images/2012/button/btn_app2.gif" alt="권리자 저작권찾기 신청" title="권리자 저작권찾기 신청" />
								</a>
							<!-- 	<a href="#1" onclick="javascript:goRghtPrpsMain('subList', '3');" 
									onkeypress="javascript:openRghtPrps('subList', '3');">
								<img src="/images/2012/button/btn_app3.gif" alt="이용자 저작권찾기 신청" title="이용자 저작권찾기 신청" />
								</a> -->
							</p>
							</div>
							
							 <!-- 그리드스타일 -->
							<div id="div_rghtPrps" style="width:100%;">
							
								<table id="tbl_rghtPrps" cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<c:if test="${DIVS == 'M'}">
									<colgroup>
										<col width="6%">
										<col width="*">
										<col width="17%">
										<col width="9%">
										<col width="12%">
										<col width="16%">
										<col width="15%">
									</colgroup>
									<thead>
										<tr>
											<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" onkeypress="javascript:checkBoxToggle('frm','chk');" title="전체선택"/></th>
											<th scope="col">곡명</th>
											<th scope="col">앨범명</th>
											<th scope="col">발매일자</th>
											<th scope="col">작사</th>
											<th scope="col">작곡</th>
											<th scope="col">가창</th>
										</tr>
									</thead>
									<tbody>
										<tr id="dummyTr">
											<td class="ce" colspan="7">선택된 목록이 없습니다.</td>								
										</tr>
									</tbody>
								</c:if>
								<c:if test="${DIVS == 'B'}">
									<colgroup>
										<col width="5%">
										<col width="23%">
										<col width="23%">
										<col width="8%">
										<col width="12%">
										<col width="10%">
										<col width="14%">
									</colgroup>
									<thead>
										<tr>
											<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" onkeypress="javascript:checkBoxToggle('frm','chk');" title="전체선택"/></th>
											<th scope="col">작품명</th>
											<th scope="col">도서명</th>
											<th scope="col">발행일자</th>
											<th scope="col">작가명</th>
											<th scope="col">역자명</th>
											<th scope="col">출판사</th>
										</tr>
									</thead>
									<tbody>
										<tr id="dummyTr">
											<td class="ce" colspan="7">선택된 목록이 없습니다.</td>								
										</tr>
									</tbody>
								</c:if>
								<c:if test="${DIVS == 'N'}">
									<colgroup>
										<col width="5%">
										<col width="*">
										<col width="20%">
										<col width="20%">
									</colgroup>
									<thead>
										<tr>
											<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" onkeypress="javascript:checkBoxToggle('frm','chk');" title="전체선택"/></th>
											<th scope="col">뉴스기사제목</th>
											<th scope="col">언론사명</th>
											<th scope="col">기사일자</th>
										</tr>
									</thead>
									<tbody>
										<tr id="dummyTr">
											<td class="ce" colspan="4">선택된 목록이 없습니다.</td>								
										</tr>
									</tbody>
								</c:if>
								<c:if test="${DIVS == 'C'}">
									<colgroup>
										<col width="5%">
										<col width="*">
										<col width="9%">
										<col width="9%">
										<col width="9%">
										<col width="9%">
										<col width="9%">
										<col width="9%">
										<col width="9%">
									</colgroup>
									<thead>
										<tr>
											<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" onkeypress="javascript:checkBoxToggle('frm','chk');" title="전체선택"/></th>
											<th scope="col">작품명</th>
											<th scope="col">작가명</th>
											<th scope="col">연출가</th>
											<th scope="col">방송회차</th>
											<th scope="col">방송일자</th>
											<th scope="col">방송매체</th>
											<th scope="col">방송사</th>
											<th scope="col">주요출연진</th>
										</tr>
									</thead>
									<tbody>
										<tr id="dummyTr">
											<td class="ce" colspan="9">선택된 목록이 없습니다.</td>								
										</tr>
									</tbody>
								</c:if>
								<c:if test="${DIVS == 'I'}">
									<colgroup>
										<col width="5%">
										<col width="*">
										<col width="12%">
										<col width="12%">
										<col width="12%">
										<col width="12%">
										<col width="15%">
									</colgroup>
									<thead>
										<tr>
											<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" onkeypress="javascript:checkBoxToggle('frm','chk');" title="전체선택"/></th>
											<th scope="col">이미지명</th>
											<th scope="col">출판사</th>
											<th scope="col">집필진</th>
											<th scope="col">출판년도</th>
											<th scope="col">작가명</th>
											<th scope="col">분야</th>
										</tr>
									</thead>
									<tbody>
										<tr id="dummyTr">
											<td class="ce" colspan="7">선택된 목록이 없습니다.</td>								
										</tr>
									</tbody>
								</c:if>
								<c:if test="${DIVS == 'V'}">
									<colgroup>
										<col width="5%">
										<col width="*">
										<col width="16%">
										<col width="15%">
										<col width="10%">
										<col width="10%">
										<col width="16%">
									</colgroup>
									<thead>
										<tr>
											<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" onkeypress="javascript:checkBoxToggle('frm','chk');" title="전체선택"/></th>
											<th scope="col">영화명</th>
											<th scope="col">감독/연출</th>
											<th scope="col">주요출연진</th>
											<th scope="col">제작일자</th>
											<th scope="col">매체</th>
											<th scope="col">관람등급</th>
										</tr>
									</thead>
									<tbody>
										<tr id="dummyTr">
											<td class="ce" colspan="7">선택된 목록이 없습니다.</td>								
										</tr>
									</tbody>
								</c:if>
							
								<c:if test="${DIVS == 'R'}">
									<colgroup>
										<col width="5%">
										<col width="*">
										<col width="15%">
										<col width="20%">
										<col width="10%">
										<col width="10%">
										<col width="10%">
									</colgroup>
									<thead>
										<tr>
											<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" onkeypress="javascript:checkBoxToggle('frm','chk');" title="전체선택"/></th>
											<th scope="col">저작물명</th>
											<th scope="col">프로그램명</th>
											<th scope="col">프로그램 회차</th>
											<th scope="col">방송일자</th>
											<th scope="col">매체</th>
											<th scope="col">채널</th>
										</tr>
									</thead>
									<tbody>
										<tr id="dummyTr">
											<td class="ce" colspan="7">선택된 목록이 없습니다.</td>								
										</tr>
									</tbody>
								</c:if>
							</table>
							<div style="height:1px;"></div>
						</div>
						<!-- //그리드스타일 --> --%>
					
						</form>
					</c:if>
					<c:if test="${DIVS == 'X'}">
						<div class="w100">
	                    	<img src="/images/2012/content/rghtPrps_subimg01.gif" alt="기타저작물" class="fl mt15" />
	                       
	                        <!-- <div class="fr strong black w70 mt10"> -->
	                        <div class="fr strong black w70 mt60">
								<p class="w80 mt20">권리자가 자신이 창작한 창작물의 권리관계 정보를 조회한 결과, 
								저작물정보가 누락되는 등 오류가 있을 경우 신청합니다.</p>
								<p class="mt20">
									<a href="#1" onclick="javascript:goRghtPrpsMain('mainList', '1');" 
											onkeypress="javascript:openRghtPrps('mainList', '1');">
										<img title="권리자 저작권찾기 신청" alt="권리자 저작권찾기 신청" src="/images/2012/button/btn_app2.gif">
									</a>
								</p>                    
								<!-- <p class="w80 mt40">이용자가 이용하려고 하는 저작물의 권리정보를 조회한 결과, 
								권리자 미확인 저작물에 해당될 경우 신청합니다.</p>
								<p class="mt20">
									<a href="#1" onclick="javascript:goRghtPrpsMain('mainList', '3');" 
											onkeypress="javascript:openRghtPrps('mainList', '3');">
										<img title="이용자 저작권찾기 신청" alt="이용자 저작권찾기 신청" src="/images/2012/button/btn_app3.gif">
									</a>
								</p>	 -->
							</div>
	                 	</div>	
					</c:if>					
				</div>
				<!-- </div> -->
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
	
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	</div>
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript">
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>

</body>
</html>
