<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page contentType="text/html;charset=euc-kr" %>
<%
	/* 
 	String userDivs = request.getParameter("userDivs") == null ? "" : request.getParameter("userDivs");
	String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName");
	//String resdCorpNumb = request.getParameter("resdCorpNumb") == null ? "" : request.getParameter("resdCorpNumb");
	String resdCorpNumb1 = request.getParameter("resdCorpNumb1") == null ? "" : request.getParameter("resdCorpNumb1");
	String resdCorpNumb2 = request.getParameter("resdCorpNumb2") == null ? "" : request.getParameter("resdCorpNumb2");
	String corpNumb = request.getParameter("corpNumb") == null ? "" : request.getParameter("corpNumb");
	
	String submitChek = request.getParameter("submitChek") == null ? "" : request.getParameter("submitChek");
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	 */	
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>아이디/비밀번호찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script src="/js/general.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->

<script type="text/JavaScript">
<!--

$(function (){
	if($("#userIdnt").val() == '' || $("#userIdnt").val() == null){
		alert("해당 아이디가 없습니다.");
		var frm = document.form1;
	 	frm.method = "post";
		frm.action = "/user/user.do?method=goLogin";
		frm.submit();
	}
});

function goUserAgree() {
	var frm = document.form1;
 	frm.method = "post";
	frm.action = "/user/user.do?method=goUserAgree";
	frm.submit();
}

function goLogin() {
	var frm = document.form1;
 	frm.method = "post";
	frm.action = "/user/user.do?method=goLogin";
	frm.submit();
}

function updateUserInfo() {
	
	var frm = document.form1;
	
	 if (frm.pswd.value == "") {
		alert("비밀번호를 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value.length < 6) {
		alert("비밀번호는 6자리 이상 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value != frm.pswdConf.value) {
		alert("입력하신 비밀번호가 다릅니다. 입력하신 비밀번호를 확인하십시오");
		frm.pswdConf.focus();
		return;
	} 
	else {
	
		if(confirm("비밀번호를 변경하시겠습니까?")){
			alert("비밀번호가 정상적으로 변경되었습니다.");
			frm.action = "/user/user.do?method=userPswdUpd";
		 	frm.submit();
		 }
	}
}

/* function init() {
	alert("비밀번호가 정상적으로 변경되었습니다.");
} */

//-->
</script>


</head>
<%-- <%  if ("Y".equals(submitChek)) {  %> --%>
<body>
<%-- <%  } else {  %> --%>
<!-- <body> -->
<%-- <%  } %> --%>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/header.jsp" /> --%>
		<!-- 2017주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis7.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis7.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_7.gif" alt="회원정보" title="회원정보" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
		
			<!-- 래프 -->
			<jsp:include page="/include/2012/userLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb3");
				</script>
			
			<!-- //래프 -->
			<!-- 주요컨텐츠 str -->
			<div class="contentBody" id="contentBody">
				<p class="path"><span>Home</span><span>회원정보</span><em>아이디/비밀번호찾기</em></p>
			<h1><img src="/images/2012/title/content_h1_0703.gif" alt="아이디/비밀번호찾기" title="아이디/비밀번호찾기" /></h1>
					
			<div class="section">
			
			 	<h2 class="mt20">비밀번호 변경하기</h2>
			 	
					<form class="frm" name="form1" method="post" action="#">
						<input type="submit" style="display:none;">
						<input type="hidden" name="method" value="updateUserInfo"/>
						<!-- <input type="hidden" name="submitChek" value="Y"/> -->
						<%-- <input type="hidden" name="userDivs" value="<%=userDivs%>">
						<input type="hidden" name="userName" value="<%=userName%>"> --%>
						<!-- input type="hidden" name="resdCorpNumb" value="<%//=resdCorpNumb%>"-->
						<%-- <input type="hidden" name="resdCorpNumb1" value="<%=resdCorpNumb1%>">
						<input type="hidden" name="resdCorpNumb2" value="<%=resdCorpNumb2%>">
						<input type="hidden" name="corpNumb" value="<%=corpNumb%>"> --%>
					 
						 <div id="box01_01_pwd">
							<div class="user_area floatDiv">
								<img src="/images/2012/content/box_img6.gif" alt="" style="float: left;"/>
									<dl class="fl ml30">
										<dt><label for="userIdnt">아이디</label></dt>
										<c:if test="${!empty userMap.USER_IDNT}">
										<dd style="margin-left: 150px;"><input type="text" id="userIdnt" name="userIdnt" size="30" value="${userMap.USER_IDNT}"/>&nbsp;(${userMap.RGST_DTTM} 가입)</dd>
										</c:if>
										<dt><label for="userEmail01_pwd">새 비밀번호</label></dt>
										<dd style="margin-left: 150px;"><input type="password" maxlength="30" name="pswd" id="pswd" size="30"/><span class="p11 ml10">(6자리 이상으로 입력하십시오)</span></td></dd>
										<dt><label for="userEmail01_pwd">새 비밀번호 확인</label></dt>
										<dd style="margin-left: 150px;"><input type="password" maxlength="30" name="pswdConf" id="psConf" size="30"/></dd>
									</dl>
									<br><br><br>
								</div>
							</div>
					<!--buttonArea start-->
						<div class="btnArea">
								<p class="fr" style="margin-right: 300px;"><span class="button medium"><a href="#" onclick="javascript:updateUserInfo();">변경하기</a></span></p>
						</div>
				  </form>
					<!--buttonArea end-->
			</div>
		<!--contents end-->
		</div>
	<!--contentsBody end-->
	</div>
	</div>
	<!-- FOOTER str-->
	<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
	<!-- FOOTER end -->
</div>
</body>
</html>
