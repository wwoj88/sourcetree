<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
 	String userDivs = request.getParameter("userDivs") == null ? "" : request.getParameter("userDivs");
	String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName");
	//String resdCorpNumb = request.getParameter("resdCorpNumb") == null ? "" : request.getParameter("resdCorpNumb");
	String corpNumb = request.getParameter("corpNumb") == null ? "" : request.getParameter("corpNumb");
	
	String resdCorpNumb1 = request.getParameter("resdCorpNumb1") == null ? "" : request.getParameter("resdCorpNumb1");
	String resdCorpNumb2 = request.getParameter("resdCorpNumb2") == null ? "" : request.getParameter("resdCorpNumb2");
	
	String dupInfo = request.getParameter("dupInfo") == null ? "" : request.getParameter("dupInfo");

	
%>
<html lang="ko">
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>아이디/비밀번호찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> -->
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/JavaScript">
<!--
function goUserAgree() {
	var frm = document.form1;
 	frm.method = "post";
	frm.action = "/user/user.do?method=goUserAgree";
	frm.submit();
}

function goAdmin(){
	 var frm = document.form1;
	
	 var flag = confirm('관리자는 관리자시스템에서 회원가입 후 이용이 가능합니다.');
	 
	 if(flag == true){
	    frm.method = "post";
		frm.action = "/admin/main";
		frm.target="_blank";
		frm.submit();	
	 }
}

function goLogin() {
	var frm = document.form1;
 	frm.method = "post";
	frm.action = "/user/user.do?method=goLogin";
	frm.submit();
}
//-->
</script>
</head>
<c:if test="${idntSrch != 'Y'}">
<c:if test="${empty userMap.USER_IDNT}">
<body onLoad="goUserAgree();">
<form name="form1" class="frm" action="#">
<input type="hidden" name="userDivs" value="<%=userDivs%>">
<input type="hidden" name="userName" value="<%=userName%>">
<input type="hidden" name="resdCorpNumb1" value="<%=resdCorpNumb1%>">
<input type="hidden" name="resdCorpNumb2" value="<%=resdCorpNumb2%>">
<input type="hidden" name="corpNumb" value="<%=corpNumb%>">
<input type="hidden" name="dupInfo" value="<%=dupInfo%>">
<input type="submit" style="display:none;">
</form>
</body>
</c:if>
</c:if>
<c:if test="${idntSrch == 'Y'}">
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<!-- <div id="wrap"> -->
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
		<!-- 2017주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<!-- <div id="container" style="background: url(/images/2012/content/container_vis7.gif) no-repeat 100% 0;"> -->
			<!-- 
			<div class="container_vis" style="background: url(/images/2012/content/container_vis7.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_7.gif" alt="회원정보" title="회원정보" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			 -->
			<div id="contents">
			
				<!-- 래프 -->
				<%-- 
				<jsp:include page="/include/2012/userLeft.jsp" />
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb3");
					</script>
				 --%>
				 <!-- 래프 -->
				<div class="con_lf" style="width: 22%">
					<div class="con_lf_big_title">회원정보</div>
					<ul class="sub_lf_menu">
						<li><a href="/user/user.do?method=goLogin">로그인</a></li>
						<li><a href="/user/user.do?method=goPage" class="on">회원가입</a></li>
						<li><a href="/user/user.do?method=goIdntPswdSrch">아이디 / 비밀번호 찾기</a></li>
					</ul>
				</div>
				<!-- //래프 -->
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<!-- <div class="contentBody" id="contentBody"> -->
				<div class="con_rt" style="width: 74%">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						회원정보
						&gt;
						<span class="bold">아이디/비밀번호찾기</span>
					</div>
					<div class="con_rt_hd_title">회원가입</div>
					<!-- section str -->
					<div class="section">
						<c:if test="${pswdSrch != 'Y'}">	
							<h1 class="mt20">▶ 아이디찾기 결과</h1>
						</c:if>
						<c:if test="${pswdSrch == 'Y'}">
							<h1 class="mt20">▶ 비밀번호찾기 결과</h1>
						</c:if>
						<form name="form1" class="frm" action="#">
							<input type="hidden" name="userDivs" value="<%=userDivs%>">
							<input type="hidden" name="userName" value="<%=userName%>">
							<input type="hidden" name="resdCorpNumb1" value="<%=resdCorpNumb1%>">
							<input type="hidden" name="resdCorpNumb2" value="<%=resdCorpNumb2%>">
							<input type="hidden" name="corpNumb" value="<%=corpNumb%>">
							<input type="hidden" name="dupInfo" value="<%=dupInfo%>">
							<input type="submit" style="display:none;">
						<c:if test="${pswdSrch != 'Y'}">	
							<!-- <span class="topLine3">&nbsp;</span> -->
							<div class="user_area floatDiv">
							<p class="fl"><img src="/images/2012/content/box_img6.gif" alt="" /></p>
							<dl class="fl ml30 mt10">
							<c:if test="${!empty userMap.USER_IDNT}">
								<dt><label class="strong">아이디</label></dt>
								<dd><strong class="orange underline">${userMap.USER_IDNT}</strong><span class="ml10 gray">&lowast;${userMap.RGST_DTTM} 가입</span></dd>
							</c:if>
							<c:if test="${empty userMap.USER_IDNT}">
								<dt><label class="strong">아이디</label></dt>
								<dd><strong class="orange underline">가입정보가 없습니다.</strong></dd>
							</c:if>	
							<c:if test="${!empty userMap.USER_IDNT}">
									<p class="clear fl ml105 mt15"><span class="button small"><a href="#1" onclick="javascript:goLogin();">로그인하기</a></span></p>
							</c:if>
								<c:if test="${empty userMap.USER_IDNT}">
										<p class="clear fl ml105 mt15">
										<%
											if(!userDivs.equals("")){
										%>
											<span class="button small"><a href="/user/user.do?method=goPage">회원가입</a></span>
										<%
											}else{
										%>
											<span class="button small"><a onclick="javascript:goAdmin();" href="#1">관리자 회원가입</a></span>
										<%
											}
										%>
									<span class="button small"><a href="/user/user.do?method=goIdntPswdSrch">아이디찾기</a></span>
									<span class="button small"><a href="/user/user.do?method=goIdntPswdSrch">비밀번호찾기</a></span>
									</p>
								</c:if>
								</dl>
								</div>
						</c:if>
						<c:if test="${pswdSrch == 'Y'}">
							<span class="topLine3">&nbsp;</span>
							<div class="user_area floatDiv">
								<p class="fl"><img src="/images/2012/content/box_img6.gif" alt="" /></p>
								<dl class="fl ml30 mt10">
								<c:if test="${!empty userMap.USER_IDNT}">
									<dt><label class="strong">아이디</label></dt>
									<dd><strong class="orange underline">${userMap.USER_IDNT}</strong><span class="ml10 gray">&lowast;${userMap.RGST_DTTM} 가입</span></dd>
									<dt><label class="strong">비밀번호</label></dt>
									<dd><strong class="orange underline">${userMap.PSWD}</strong></dd>
								</c:if>
								<c:if test="${empty userMap.USER_IDNT}">
									<dt><label class="strong">비밀번호</label></dt>
									<dd><strong class="orange underline">해당 비밀번호 정보가 없습니다.</strong></dd>
								</c:if>
								
						
								<c:if test="${!empty userMap.USER_IDNT}">
									<p class="clear fl ml105 mt15"><span class="button small"><a href="#1" onclick="javascript:goLogin();">로그인하기</a></span></p>
							</c:if>
								<c:if test="${empty userMap.USER_IDNT}">
									<p class="clear fl ml105 mt15">
									<span class="button small"><a onclick="javascript:goUserAgree();" href="#1">회원가입</a></span>
									<span class="button small"><a href="/user/user.do?method=goIdntPswdSrch">아이디찾기</a></span>
									<span class="button small"><a href="/user/user.do?method=goIdntPswdSrch">비밀번호찾기</a></span>
									</p>
								</c:if>
								</dl>
							</div>
						</c:if>
						</form>
						
										</div>
					<!-- section str -->
				</div>
				<!-- //주요컨텐츠 end -->
			<p class="clear"></p>
			</div>
		<!-- </div> -->
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer_main.jsp" /> --%>
		<!-- FOOTER end -->
	<!-- </div> -->
	<!-- //전체를 감싸는 DIVISION -->
</body>
</c:if>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/script.js"></script>
</html>
