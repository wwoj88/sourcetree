<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작물 내권리찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
<script src="/js/Function.js" type="text/javascript"></script>
<script type="text/JavaScript">
<!--
function init() {
	var frm = document.form1;
	var userDivs = frm.userDivs.value;

	if (userDivs == "01") {
		frm.corpNumb1.disabled = true;
		frm.corpNumb2.disabled = true;
		frm.corpNumb3.disabled = true;
		frm.resdCorpNumb1.disabled = false;
		frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "02") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
  	frm.resdCorpNumb1.disabled = false;
  	frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "03") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
		frm.resdCorpNumb1.disabled = true;
		frm.resdCorpNumb2.disabled = true;
  }

  frm.userName.focus();
}

// 사업자등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginCrnChk(obj){
	// 사업자등록번호 앞자리의 길이를 확인하여 3자리, 2자리가 입력되면 다음 객체로 이동
	if(obj.name == "corpNumb1"){
		if(obj.value.length == 3)	document.form1.corpNumb2.focus();
	} else if(obj.name == "corpNumb2"){
		if(obj.value.length == 2)	document.form1.corpNumb3.focus();
	}
}

// 주민등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginSsnChk(obj){
	// 주민등록번호 앞자리의 길이를 확인하여 6자리가 입력되면 다음 객체로 이동
	if(obj.name == "resdCorpNumb1"){
		if(obj.value.length == 6)	document.form1.resdCorpNumb2.focus();
	}

	// EnterKey 입력시 공인인증서 로그인 수행
	if (event.keyCode == 13) {
		fn_userIdntSrch();
	}
}

function fn_userIdntSrch() {
	var frm = document.form1;
	var userDivs = frm.userDivs.value;

  if (Trim(frm.userName.value) == "") {
  	alert("이름/사업자명을 입력하십시오.");
  	frm.userName.focus();
  	return;
  } else if (userDivs == "01") {
  	if (frm.resdCorpNumb1.value == "") {
  		alert("주민/법인번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb1.value.length != 6) {
  		alert("주민/법인번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value == "") {
  		alert("주민/법인번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb2.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value.length != 7) {
  		alert("주민/법인번호 앞자리는 7자리 입니다.");
  		frm.resdCorpNumb2.focus();
  		return;
  	}
		ssn1 = frm.resdCorpNumb1.value;
		ssn2 = frm.resdCorpNumb2.value;
		frm.resdCorpNumb.value = frm.resdCorpNumb1.value + frm.resdCorpNumb2.value;

		if(ssnCheck(ssn1, ssn2)) {              // 주민번호 체크
			return;
		}
  } else if (userDivs == "02") {
  	if (frm.resdCorpNumb1.value == "") {
  		alert("주민/법인번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb1.value.length != 6) {
  		alert("주민/법인번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value == "") {
  		alert("주민/법인번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb2.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value.length != 7) {
  		alert("주민/법인번호 앞자리는 7자리 입니다.");
  		frm.resdCorpNumb2.focus();
  		return;
  	} else if (frm.corpNumb1.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb1.focus();
  		return;
    } else if (frm.corpNumb1.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb1.focus();
  		return;
   	} else if (frm.corpNumb2.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb2.focus();
  		return;
    } else if (frm.corpNumb2.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb2.focus();
  		return;
   	} else if (frm.corpNumb3.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb3.focus();
  		return;
    } else if (frm.corpNumb3.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb3.focus();
  		return;
  	}
  	frm.corpNumb.value = frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value;
  	frm.resdCorpNumb.value = frm.resdCorpNumb1.value + frm.resdCorpNumb2.value;

  	if (fncJuriRegNoCheck(frm.resdCorpNumb1.value + frm.resdCorpNumb2.value) == false) {                // 법인번호 체크
  		alert("잘못된 법인번호 입니다.");
  		frm.resdCorpNumb1.focus();
 		  return;
 		}

		if (check_busino(frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb1.focus();
 		  return;
 		}
  } else if (userDivs == "03") {
  	if (frm.corpNumb1.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb1.focus();
  		return;
    } else if (frm.corpNumb1.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb1.focus();
  		return;
   	} else if (frm.corpNumb2.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb2.focus();
  		return;
    } else if (frm.corpNumb2.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb2.focus();
  		return;
   	} else if (frm.corpNumb3.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb3.focus();
  		return;
    } else if (frm.corpNumb3.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb3.focus();
  		return;
  	}
  	
		frm.corpNumb.value = frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value;
		
		if (check_busino(frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb1.focus();
 		  return;
 		}
  }
	frm.action = "/user/user.do?method=userIdntPswdSrch";
	frm.submit();
}

function fn_userDivs() {
	var frm = document.form1;
	var userDivs = frm.userDivs.value;

	if (userDivs == "01") {
		frm.corpNumb1.disabled = true;
		frm.corpNumb2.disabled = true;
		frm.corpNumb3.disabled = true;
		frm.resdCorpNumb1.disabled = false;
		frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "02") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
  	frm.resdCorpNumb1.disabled = false;
  	frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "03") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
		frm.resdCorpNumb1.disabled = true;
		frm.resdCorpNumb2.disabled = true;
  }
  frm.userName.focus();

  frm.userName.value = "";
  frm.resdCorpNumb1.value = "";
  frm.resdCorpNumb2.value = "";
  frm.corpNumb1.value = "";
  frm.corpNumb2.value = "";
  frm.corpNumb3.value = "";
}

function fn_userDivs2() {
	var frm = document.form2;
	var userDivs = frm.userDivs.value;

	if (userDivs == "01") {
		frm.corpNumb1.disabled = true;
		frm.corpNumb2.disabled = true;
		frm.corpNumb3.disabled = true;
		frm.resdCorpNumb1.disabled = false;
		frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "02") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
  	frm.resdCorpNumb1.disabled = false;
  	frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "03") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
		frm.resdCorpNumb1.disabled = true;
		frm.resdCorpNumb2.disabled = true;
  }
  frm.userIdnt.focus();

  frm.userName.value = "";
  frm.resdCorpNumb1.value = "";
  frm.resdCorpNumb2.value = "";
  frm.corpNumb1.value = "";
  frm.corpNumb2.value = "";
  frm.corpNumb3.value = "";
}

// 사업자등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginCrnChk2(obj){
	// 사업자등록번호 앞자리의 길이를 확인하여 3자리, 2자리가 입력되면 다음 객체로 이동
	if(obj.name == "corpNumb1"){
		if(obj.value.length == 3)	document.form2.corpNumb2.focus();
	} else if(obj.name == "corpNumb2"){
		if(obj.value.length == 2)	document.form2.corpNumb3.focus();
	}
}

// 주민등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginSsnChk2(obj){
	// 주민등록번호 앞자리의 길이를 확인하여 6자리가 입력되면 다음 객체로 이동
	if(obj.name == "resdCorpNumb1"){
		if(obj.value.length == 6)	document.form2.resdCorpNumb2.focus();
	}

	// EnterKey 입력시 공인인증서 로그인 수행
	if (event.keyCode == 13) {
		fn_userPswdSrch();
	}
}

function fn_userPswdSrch() {
	var frm = document.form2;
	var userDivs = frm.userDivs.value;
  
  if (Trim(frm.userIdnt.value) == "") {
  	alert("아이디를 입력하십시오.");
  	frm.userIdnt.focus();
  	return;
  } else if (Trim(frm.userName.value) == "") {
  	alert("이름/사업자명을 입력하십시오.");
  	frm.userName.focus();
  	return;
  } else if (userDivs == "01") {
  	if (frm.resdCorpNumb1.value == "") {
  		alert("주민/법인번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb1.value.length != 6) {
  		alert("주민/법인번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value == "") {
  		alert("주민/법인번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb2.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value.length != 7) {
  		alert("주민/법인번호 앞자리는 7자리 입니다.");
  		frm.resdCorpNumb2.focus();
  		return;
  	}
		ssn1 = frm.resdCorpNumb1.value;
		ssn2 = frm.resdCorpNumb2.value;
		frm.resdCorpNumb.value = frm.resdCorpNumb1.value + frm.resdCorpNumb2.value;

		if(ssnCheck2(ssn1, ssn2)) {              // 주민번호 체크
			return;
		}
  } else if (userDivs == "02") {
  	if (frm.resdCorpNumb1.value == "") {
  		alert("주민/법인번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb1.value.length != 6) {
  		alert("주민/법인번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value == "") {
  		alert("주민/법인번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb2.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value.length != 7) {
  		alert("주민/법인번호 앞자리는 7자리 입니다.");
  		frm.resdCorpNumb2.focus();
  		return;
  	} else if (frm.corpNumb1.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb1.focus();
  		return;
    } else if (frm.corpNumb1.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb1.focus();
  		return;
   	} else if (frm.corpNumb2.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb2.focus();
  		return;
    } else if (frm.corpNumb2.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb2.focus();
  		return;
   	} else if (frm.corpNumb3.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb3.focus();
  		return;
    } else if (frm.corpNumb3.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb3.focus();
  		return;
  	}
  	frm.corpNumb.value = frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value;
  	frm.resdCorpNumb.value = frm.resdCorpNumb1.value + frm.resdCorpNumb2.value;

  	if (fncJuriRegNoCheck(frm.resdCorpNumb1.value + frm.resdCorpNumb2.value) == false) {                // 법인번호 체크
  		alert("잘못된 법인번호 입니다.");
  		frm.resdCorpNumb1.focus();
 		  return;
 		}

		if (check_busino(frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb1.focus();
 		  return;
 		}
  } else if (userDivs == "03") {
  	if (frm.corpNumb1.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb1.focus();
  		return;
    } else if (frm.corpNumb1.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb1.focus();
  		return;
   	} else if (frm.corpNumb2.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb2.focus();
  		return;
    } else if (frm.corpNumb2.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb2.focus();
  		return;
   	} else if (frm.corpNumb3.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb3.focus();
  		return;
    } else if (frm.corpNumb3.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb3.focus();
  		return;
  	}
  	
		frm.corpNumb.value = frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value;
		
		if (check_busino(frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb1.focus();
 		  return;
 		}
  }
	frm.action = "/user/user.do?method=userIdntPswdSrch";
	frm.submit();
}
//-->
</script>
</head>
<body class="subBg" onLoad="init();">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp"/>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','122');
			</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="memberTlt">회원</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>회원</li>
					<li class="on">아이디/비밀번호찾기</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>아이디/비밀번호찾기</h2>
			<div id="contentsDiv">
				<div class="idpwSrch">
					<h3>아이디 찾기</h3>
					<!--div class="tip R">'-' 없이 입력하세요!</div-->
					<ul class="memBox">
						<form name="form1" method="post">
							<input type="hidden" name="resdCorpNumb">
				      		<input type="hidden" name="corpNumb">
							  <li><span class="tlt"><label for="usrType">회원구분</label></span>
								  <select name="userDivs" onChange="fn_userDivs();">
							        <option value="01">개인회원</option>
							        <option value="02">법인사업자</option>
							        <option value="03">개인사업자</option>
						         </select>
							  </li>
						<li><span class="tlt"><label for="name">이름/사업자명</label></span><input type="text" id="userName" name="userName" class="input" size="37" /></li>
						<li><span class="tlt"><label for="idNum">주민/법인번호</label></span><input type="text" id="resdCorpNumb1" name="resdCorpNumb1" class="input noKR" size="10" maxlength="6" onKeyUp="fn_signLoginSsnChk(this)" onkeypress="cfInputNumRT(event);"/>
							                                                                   -
							                                                                   <input type="password" id="resdCorpNumb2" name="resdCorpNumb2" class="input noKR" size="15" maxlength="7" onKeyUp="fn_signLoginSsnChk(this)" onkeypress="cfInputNumRT(event);"/>
					  </li>
						<li><span class="tlt"><label for="comNum">사업자번호</label></span><input type="text" id="corpNumb1" name="corpNumb1" class="input noKR" size="10" maxlength="3" onKeyUp="fn_signLoginCrnChk(this)" onkeypress="cfInputNumRT(event);"/>
							                                                                 -
							                                                                 <input type="text" id="corpNumb2" name="corpNumb2" class="input noKR" size="10" maxlength="2" onKeyUp="fn_signLoginCrnChk(this)" onkeypress="cfInputNumRT(event);"/>
							                                                                 -
							                                                                 <input type="text" id="corpNumb3" name="corpNumb3" class="input noKR" size="10" maxlength="5" onKeyUp="fn_signLoginCrnChk(this)" onkeypress="cfInputNumRT(event);"/></li>
					</ul>
				  </form>
					<!--buttonArea start-->
					<div id="buttonArea">
						<div class="C"><a href="javascript:fn_userIdntSrch();"><img src="/images/button/conf_btn.gif" alt="확인" /></a> <a href="javascript:document.form1.reset();"><img src="/images/button/cancle2_btn.gif" alt="취소" /></a></div>
					</div>
					<!--buttonArea end-->
					<h3>비밀번호 찾기</h3>
					<ul class="memBox">
						<form name="form2" method="post">
							<input type="hidden" name="resdCorpNumb">
					        <input type="hidden" name="corpNumb">
					        <input type="hidden" name="pswdSrch" value="Y">
						<li><span class="tlt"><label for="usrType">회원구분</label></span>
							  <select name="userDivs" onChange="fn_userDivs2();">
					            <option value="01">개인회원</option>
					            <option value="02">법인사업자</option>
					            <option value="03">개인사업자</option>
				              </select>
						</li>
						<li><span class="tlt"><label for="id">아이디</label></span><input type="text" id="userIdnt" name="userIdnt" tabindex="10" class="input" size="35" /></li>
						<li><span class="tlt"><label for="name">이름/사업자명</label></span><input type="text" id="userName" name="userName" tabindex="11" class="input" size="37" /></li>
						<li><span class="tlt"><label for="idNum">주민/법인번호</label></span><input type="text" id="resdCorpNumb1" name="resdCorpNumb1" tabindex="12" class="input noKR" size="10" maxlength="6" onKeyUp="fn_signLoginSsnChk2(this)" onkeypress="cfInputNumRT(event);"/>
							                                                                   -
							                                                                   <input type="password" id="resdCorpNumb2" name="resdCorpNumb2" tabindex="13" class="input noKR" size="15" maxlength="7" onKeyUp="fn_signLoginSsnChk2(this)" onkeypress="cfInputNumRT(event);"/>
					  </li>
						<li><span class="tlt"><label for="comNum">사업자번호</label></span><input type="text" id="corpNumb1" name="corpNumb1" tabindex="14" class="input" size="10 noKR" maxlength="3" onKeyUp="fn_signLoginCrnChk2(this)" onkeypress="cfInputNumRT(event);"/>
							                                                                 -
							                                                                 <input type="text" id="corpNumb2" name="corpNumb2" tabindex="15" class="input" size="10 noKR" maxlength="2" onKeyUp="fn_signLoginCrnChk2(this)" onkeypress="cfInputNumRT(event);"/>
							                                                                 -
							                                                                 <input type="text" id="corpNumb3" name="corpNumb3" tabindex="16" class="input" size="10 noKR" maxlength="5" onKeyUp="fn_signLoginCrnChk2(this)" onkeypress="cfInputNumRT(event);"/></li>
					</ul>
					</form>
					</ul>
					<!--buttonArea start-->
					<div id="buttonArea">
						<div class="C"><a href="javascript:fn_userPswdSrch();"><img src="/images/button/conf_btn.gif" alt="확인" /></a> <a href="javascript:document.form2.reset();"><img src="/images/button/cancle2_btn.gif" alt="취소" /></a></div>
					</div>
					<!--buttonArea end-->
				</div>
			</div>
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
