<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
  String login = request.getParameter("login") == null ? "" : request.getParameter("login");
%> 
<%@ page contentType="text/html;charset=euc-kr" %>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>로그인 | 회원정보 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/JavaScript">

<!--//
	
// Enter Key 입력시 로그인 수행 (아이디/비밀번호)
function fn_loginChk(obj){

	// EnterKey 입력시 로그인 수행
	if (event.keyCode == 13) {
		fn_login();
	}
}

// 로그인 절차 수행
function fn_login(){
  
  var frm = document.form0;

  if(frm.userIdnt.value == ""){
  	alert("아이디가 입력되지 않았습니다.");
   	frm.userIdnt.focus();
   	return;
  } else if(frm.pswd.value == ""){
   	alert("비밀번호가 입력되지 않았습니다.");
   	frm.pswd.focus();
   	return;
  } else {
    	frm.loginDivs.value = "N";
		frm.action = "/userLogin/userLogin.do";
		frm.submit();
  }
}

function init() {
	var frm = document.form0;

	document.getElementById("userIdnt").style.imeMode = "inactive"; //영문모드
	frm.userIdnt.focus();
}

function engKey(){
	document.getElementById("userIdnt").style.imeMode = "inactive"; //영문모드
}

-->
</script>
</head>

<body onLoad="init();">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/header.jsp" /> --%>
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<div id="container" style="background: url(/images/2012/content/container_vis7.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis7.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_7.gif" alt="회원정보" title="회원정보" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
				<!-- 래프 -->
				<jsp:include page="/include/2012/userLeft.jsp" />
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb3");
					</script>
				
				<!-- //래프 -->
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>회원정보</span><em>아이디/비밀번호 찾기</em></p>
					<h1><img src="/images/2012/title/content_h1_0703.gif" alt="아이디/비밀번호찾기" title="아이디/비밀번호찾기" /></h1>
					
					<div class="section">
						<h2 class="mt20">비밀번호 변경하기</h2>
						<ul class="memBox">
						<form name="form1">
							<input type="submit" style="display:none;">
							<input type="hidden" name="userDivs" value="<%=userDivs%>">
							<input type="hidden" name="userName" value="<%=userName%>">
							<!-- input type="hidden" name="resdCorpNumb" value="<%//=resdCorpNumb%>"-->
							<input type="hidden" name="resdCorpNumb1" value="<%=resdCorpNumb1%>">
							<input type="hidden" name="resdCorpNumb2" value="<%=resdCorpNumb2%>">
							<input type="hidden" name="corpNumb" value="<%=corpNumb%>">
						 <p class="comment">※입력하신 정보와 일치하는 정보는 아래와 같습니다.</p>
						 <c:if test="${!empty userMap.USER_IDNT}">
					     <li class="clear"><span class="tlt"><label for="id">아이디:</label></span><span>${userMap.USER_IDNT} (${userMap.RGST_DTTM}) 가입</span></li>
					     <li class="clear"><span class="tlt"><label for="id">비밀번호:</label></span><span>${userMap.PSWD}</span></li>
						 </c:if>
						 <c:if test="${empty userMap.USER_IDNT}">
						 	 <li class="clear"><span class="tlt"></span><span>가입정보가 없습니다.</span></li>
						 </c:if>
						<!--buttonArea start-->
						<div id="buttonArea">
							<c:if test="${!empty userMap.USER_IDNT}">
							  <div class="C"><a href="#1" onclick="javascript:goLogin();"><img src="/images/button/login2_btn.gif" alt="로그인하기" /></a></div>
	 						</c:if>
						  <c:if test="${empty userMap.USER_IDNT}">
							  <div class="C"><a href="#1" onclick="javascript:goUserAgree();"><img src="/images/button/join_btn.gif" alt="회원가입" /></a></div>
					    </c:if>
						</div>
					  </form>
					  </ul>
						<div class="comment_box">
							<strong class="line15 black"><img src="/images/2012/common/ic_tip.gif" class="vmid" alt=""/> 알려드립니다.</strong>
							<p class="comment block ml20 mt5">저작권찾기 사이트를 이용하기 위해서는 로그인 하시기 바랍니다.</p>
							<!--
							<ul class="commentList">
								<li>
									<p class="strong ine15 black"><span style="color:#0086c3;">위탁관리저작물</span> 및 <span style="color:#0086c3;">미분배 보상금 대상 저작물 보고</span>는 관리자시스템에서 회원가입 후 이용이 가능합니다.</p>
								</li>
								<li style="list-style-type: none; background-image: none; text-align: right; margin-bottom: 5px;">
									<a href="/admin/main" style="margin-right: 28px;" target="_blank"><img src="/images/2012/button/btn_go_admin.gif" alt="관리자시스템 바로가기" title="관리자시스템 바로가기"/></a>
								</li>
							</ul>
							-->
						</div>							
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<%  if ("Y".equals(login)) {  %>
		<c:if test="${not empty errorMessage}">
			<script type="text/javascript">
      		<!--
			  alert("${errorMessage}");
			//-->
			</script>
		</c:if>
		<%  }  %>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/script.js"></script>
</body>
</html>
