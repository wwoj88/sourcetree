<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%//@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%//@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
 	String userDivs = request.getParameter("userDivs") == null ? "" : request.getParameter("userDivs");
	String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName");
	//String resdCorpNumb = request.getParameter("resdCorpNumb") == null ? "" : request.getParameter("resdCorpNumb");
	String resdCorpNumb1 = request.getParameter("resdCorpNumb1") == null ? "" : request.getParameter("resdCorpNumb1");
	String resdCorpNumb2 = request.getParameter("resdCorpNumb2") == null ? "" : request.getParameter("resdCorpNumb2");
	String corpNumb = request.getParameter("corpNumb") == null ? "" : request.getParameter("corpNumb");
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>비밀번호 찾기 결과 | 저작권찾기</title>
<script type="text/JavaScript">
<!--
function goUserAgree() {
	var frm = document.form1;
 	frm.method = "post";
	frm.action = "/user/user.do?method=goUserAgree";
	frm.submit();
}

function goLogin() {
	var frm = document.form1;
 	frm.method = "post";
	frm.action = "/user/user.do?method=goLogin";
	frm.submit();
}
//-->
</script>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp"/>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','127');
			</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="memberTlt">회원</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>회원</li>
					<li class="on">아이디/비밀번호찾기</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>아이디/비밀번호찾기</h2>
			<div id="contentsDiv">
				<div class="idpwSrch">
					<h3>비밀번호 찾기 결과</h3>
					<ul class="memBox">
					<form name="form1">
						<input type="submit" style="display:none;">
						<input type="hidden" name="userDivs" value="<%=userDivs%>">
						<input type="hidden" name="userName" value="<%=userName%>">
						<!-- input type="hidden" name="resdCorpNumb" value="<%//=resdCorpNumb%>"-->
						<input type="hidden" name="resdCorpNumb1" value="<%=resdCorpNumb1%>">
						<input type="hidden" name="resdCorpNumb2" value="<%=resdCorpNumb2%>">
						<input type="hidden" name="corpNumb" value="<%=corpNumb%>">
					 <p class="comment">※입력하신 정보와 일치하는 정보는 아래와 같습니다.</p>
					 <c:if test="${!empty userMap.USER_IDNT}">
				     <li class="clear"><span class="tlt"><label for="id">아이디:</label></span><span>${userMap.USER_IDNT} (${userMap.RGST_DTTM}) 가입</span></li>
				     <li class="clear"><span class="tlt"><label for="id">비밀번호:</label></span><span>${userMap.PSWD}</span></li>
					 </c:if>
					 <c:if test="${empty userMap.USER_IDNT}">
					 	 <li class="clear"><span class="tlt"></span><span>가입정보가 없습니다.</span></li>
					 </c:if>
					</ul>
					<!--buttonArea start-->
					<div id="buttonArea">
						<c:if test="${!empty userMap.USER_IDNT}">
						  <div class="C"><a href="#1" onclick="javascript:goLogin();"><img src="/images/button/login2_btn.gif" alt="로그인하기" /></a></div>
 						</c:if>
					  <c:if test="${empty userMap.USER_IDNT}">
						  <div class="C"><a href="#1" onclick="javascript:goUserAgree();"><img src="/images/button/join_btn.gif" alt="회원가입" /></a></div>
				    </c:if>
					</div>
				  </form>
					<!--buttonArea end-->
				</div>
			</div>
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
