<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<% 
HttpSession s = request.getSession(true);
s.putValue("NmChkSec","98u9iuhuyg87");
%>


<%@ page language="java" import="Kisinfo.Check.IPINClient" %>
<%@ page language="java" import="NiceID.Check.CPClient" %>
<%
//현재 URL 가져오기
String c_url = javax.servlet.http.HttpUtils.getRequestURL(request).toString(); 

//가져온 URL 에서 도메인부분만 가져오기
String temp = c_url.substring(0, c_url.indexOf("/", 8));
	/********************************************************************************************************************************************
	NICE신용평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
	
	서비스명 : 가상주민번호서비스 (안심본인인증) 서비스
	페이지명 : 가상주민번호서비스 (안심본인인증) 호출 페이지
	*********************************************************************************************************************************************/
    NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();
	
	String sSiteCode1 = "G6840";				//안심본인인증		NICE로부터 부여받은 사이트 코드
    String sSitePassword = "91F92CJ77YO7";		//안심본인인증 	NICE로부터 부여받은 사이트 패스워드
    
    String sRequestNumber = "REQ0000000001";        	// 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로 
														// 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.
    
	sRequestNumber = niceCheck.getRequestNO(sSiteCode1);
  	session.setAttribute("REQ_SEQ" , sRequestNumber);	// 해킹등의 방지를 위하여 세션을 쓴다면, 세션에 요청번호를 넣는다.
  	
   	String sAuthType = "";      	// 없으면 기본 선택화면, M: 핸드폰, C: 신용카드, X: 공인인증서
   	
   	String popgubun 	= "N";		//Y : 취소버튼 있음 / N : 취소버튼 없음
		String customize 	= "";	//없으면 기본 웹페이지 / Mobile : 모바일페이지
		
    // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
    String sReturnUrl = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("checkSuccess"));
    //String sReturnUrl = "http://dev.findcopyright.or.kr/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL
	String sErrorUrl = new String(kr.or.copyright.mls.support.constant.Constants.getProperty("checkFail"));
    //String sErrorUrl = "http://dev.findcopyright.or.kr/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL
    
    if(temp.equalsIgnoreCase("http://dev.copyright.or.kr")){
    	  sReturnUrl = "http://dev.copyright.or.kr/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(로컬)
    	  sErrorUrl = "http://dev.copyright.or.kr/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(로컬)
   }else if(temp.equalsIgnoreCase("http://www.copyright.or.kr:8880")){
	   sReturnUrl = "http://www.copyright.or.kr:8880/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(개발)
	   sErrorUrl = "http://www.copyright.or.kr:8880/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(개발)
   }else if(temp.equalsIgnoreCase("http://localhost:8080")){
	   sReturnUrl = "http://localhost:8080/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(개발)
	   sErrorUrl = "http://localhost:8080/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(개발)
   }else if(temp.equalsIgnoreCase("http://local.findcopyright.or.kr:8080")){
	   sReturnUrl = "http://local.findcopyright.or.kr:8080/CheckPlusSafe/checkplus_success.jsp";      // 성공시 이동될 URL(개발)
	   sErrorUrl = "http://local.findcopyright.or.kr:8080/CheckPlusSafe/checkplus_fail.jsp";          // 실패시 이동될 URL(개발)
   }

    // 입력될 plain 데이타를 만든다.
    String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
                        "8:SITECODE" + sSiteCode1.getBytes().length + ":" + sSiteCode1 +
                        "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
                        "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
                        "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
                        "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
                        "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize;
    
    String sMessage = "";
    String sEncData1 = "";
    
    int iReturn = niceCheck.fnEncode(sSiteCode1, sSitePassword, sPlainData);
    if( iReturn == 0 )
    {
        sEncData1 = niceCheck.getCipherData();
    }
    else if( iReturn == -1)
    {
        sMessage = "암호화 시스템 에러입니다.";
    }    
    else if( iReturn == -2)
    {
        sMessage = "암호화 처리오류입니다.";
    }    
    else if( iReturn == -3)
    {
        sMessage = "암호화 데이터 오류입니다.";
    }    
    else if( iReturn == -9)
    {
        sMessage = "입력 데이터 오류입니다.";
    }    
    else
    {
        sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
    }
	

%>
<%
	/********************************************************************************************************************************************
		NICE신용평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
		
		서비스명 : 가상주민번호서비스 (IPIN) 서비스
		페이지명 : 가상주민번호서비스 (IPIN) 호출 페이지
	*********************************************************************************************************************************************/
	
	String sSiteCode				= "D746";			// IPIN 서비스 사이트 코드		(NICE신용평가정보에서 발급한 사이트코드)
	String sSitePw					= "Yagins12";			// IPIN 서비스 사이트 패스워드	(NICE신용평가정보에서 발급한 사이트패스워드)
	
	
	/*
	┌ sReturnURL 변수에 대한 설명  ─────────────────────────────────────────────────────
		NICE신용평가정보 팝업에서 인증받은 사용자 정보를 암호화하여 귀사로 리턴합니다.
		따라서 암호화된 결과 데이타를 리턴받으실 URL 정의해 주세요.
		
		* URL 은 http 부터 입력해 주셔야하며, 외부에서도 접속이 유효한 정보여야 합니다.
		* 당사에서 배포해드린 샘플페이지 중, ipin_process.jsp 페이지가 사용자 정보를 리턴받는 예제 페이지입니다.
		
		아래는 URL 예제이며, 귀사의 서비스 도메인과 서버에 업로드 된 샘플페이지 위치에 따라 경로를 설정하시기 바랍니다.
		예 - http://www.test.co.kr/ipin_process.jsp, https://www.test.co.kr/ipin_process.jsp, https://test.co.kr/ipin_process.jsp
	└────────────────────────────────────────────────────────────────────
	*/
	String sReturnURL				= "";
	
	int port = request.getServerPort();
	String domain = request.getServerName();
	
	if(port == 80 || port == 0){
	    sReturnURL = "https://"+domain+"/iPin/ipin_process.jsp";
	}else{
	    sReturnURL = "https://"+domain+":"+port+"/iPin/ipin_process.jsp";
	}
	

	/*
	┌ sCPRequest 변수에 대한 설명  ─────────────────────────────────────────────────────
		[CP 요청번호]로 귀사에서 데이타를 임의로 정의하거나, 당사에서 배포된 모듈로 데이타를 생성할 수 있습니다.
		
		CP 요청번호는 인증 완료 후, 암호화된 결과 데이타에 함께 제공되며
		데이타 위변조 방지 및 특정 사용자가 요청한 것임을 확인하기 위한 목적으로 이용하실 수 있습니다.
		
		따라서 귀사의 프로세스에 응용하여 이용할 수 있는 데이타이기에, 필수값은 아닙니다.
	└────────────────────────────────────────────────────────────────────
	*/
	String sCPRequest				= "";
	
	
	
	// 객체 생성
	IPINClient pClient = new IPINClient();
	
	
	// 앞서 설명드린 바와같이, CP 요청번호는 배포된 모듈을 통해 아래와 같이 생성할 수 있습니다.
	sCPRequest = pClient.getRequestNO(sSiteCode);
	
	// CP 요청번호를 세션에 저장합니다.
	// 현재 예제로 저장한 세션은 ipin_result.jsp 페이지에서 데이타 위변조 방지를 위해 확인하기 위함입니다.
	// 필수사항은 아니며, 보안을 위한 권고사항입니다.
	session.setAttribute("CPREQUEST" , sCPRequest);
	
	
	// Method 결과값(iRtn)에 따라, 프로세스 진행여부를 파악합니다.
	int iRtn = pClient.fnRequest(sSiteCode, sSitePw, sCPRequest, sReturnURL);
	
	String sRtnMsg					= "";			// 처리결과 메세지
	String sEncData					= "";			// 암호화 된 데이타
	
	// Method 결과값에 따른 처리사항
	if (iRtn == 0)
	{
	
		// fnRequest 함수 처리시 업체정보를 암호화한 데이터를 추출합니다.
		// 추출된 암호화된 데이타는 당사 팝업 요청시, 함께 보내주셔야 합니다.
		sEncData = pClient.getCipherData();		//암호화 된 데이타
		
		sRtnMsg = "정상 처리되었습니다.";
	
	}
	else if (iRtn == -1 || iRtn == -2)
	{
		sRtnMsg =	"배포해 드린 서비스 모듈 중, 귀사 서버환경에 맞는 모듈을 이용해 주시기 바랍니다.<BR>" +
					"귀사 서버환경에 맞는 모듈이 없다면 ..<BR><B>iRtn 값, 서버 환경정보를 정확히 확인하여 메일로 요청해 주시기 바랍니다.</B>";
	}
	else if (iRtn == -9)
	{
		sRtnMsg = "입력값 오류 : fnRequest 함수 처리시, 필요한 4개의 파라미터값의 정보를 정확하게 입력해 주시기 바랍니다.";
	}
	else
	{
		sRtnMsg = "iRtn 값 확인 후, NICE신용평가정보 개발 담당자에게 문의해 주세요.";
	}

%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>아이디/비밀번호찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script src="/js/Function.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> -->
<!-- <script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script> -->

<script language='javascript'>
	window.name ="Parent_window";
	
	//안심본인인증 아이디 찾기 서비스
	function fnPopup2(){
		window.open('', 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
		document.form_chk.param_r1.value = "idntSrch"
		document.form_chk.target = "popupChk";
		document.form_chk.submit();
	}

<!--
	
	function fnPopup(){
		window.open('', 'popupIPIN2', 'width=450, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		document.form_ipin.target = "popupIPIN2";
		document.form_ipin.param_r1.value = "idntSrch"
		document.form_ipin.action = "https://cert.vno.co.kr/ipin.cb";
		document.form_ipin.submit();
	}
	
	function fnPopup_pswd(){
		var frm = document.form2;
		
		var userDivs = frm.userDivs_pwd.value;
		
		if(userDivs == "01"){
			if (Trim(frm.userIdnt01_02_pwd.value) == ""){
				alert("아이디를 입력하십시오.");
				frm.userIdnt01_02_pwd.focus();
				return;
			}else if (frm.userEmail01_02_pwd.value == "") {
	  			alert("이메일을 입력하십시오.");
	  			frm.userEmail01_02_pwd.focus();
	  			return;
			}
	  	}
		
		frm.userIdnt_pwd.value = frm.userIdnt01_02_pwd.value;//아이디
		
		frm.userEmail_pwd.value = frm.userEmail01_02_pwd.value; //이메일
		
		window.open('', 'popupIPIN2', 'width=450, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		document.form_ipin.target = "popupIPIN2";
		document.form_ipin.param_r1.value = "pswdSrch";
		document.form_ipin.param_r2.value = frm.userIdnt_pwd.value; //id
		document.form_ipin.param_r3.value = frm.userEmail_pwd.value; //mail
		document.form_ipin.action = "https://cert.vno.co.kr/ipin.cb";
		document.form_ipin.submit();
	}
	//안심본인인증 비밀번호 찾기 서비스
	function fnPopup_pswd2(){
		var frm = document.form2;
		
		var userDivs = frm.userDivs_pwd.value;
		
		if(userDivs == "01"){
			if (Trim(frm.userIdnt01_pwd.value) == ""){
				alert("아이디를 입력하십시오.");
				frm.userIdnt01_pwd.focus();
				return;
			}
			/* 
			else if (frm.userEmail01_pwd.value == "") {
	  			alert("이메일을 입력하십시오.");
	  			frm.userEmail01_pwd.focus();
	  			return;
			}
			 */
	  	}

		window.open('', 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		document.form_chk.target = "popupChk";
		document.form_chk.param_r1.value = "pswdSrch";
		document.form_chk.param_r2.value = frm.userIdnt01_pwd.value; //id
		//document.form_chk.param_r3.value = frm.userEmail01_pwd.value; //mail
		document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
		document.form_chk.submit();

	}
-->
</script>

<script type="text/JavaScript">
<!--

jQuery(function(){
	
	jQuery("input:radio[name=uDivs]").eq(0).attr("checked",true);
	jQuery("input:radio[name=pDivs]").eq(0).attr("checked",true);
	
	jQuery("input:radio[name=uDivs_pwd]").eq(0).attr("checked",true);
	jQuery("input:radio[name=pDivs_pwd]").eq(0).attr("checked",true);
	
	jQuery("input[name=userDivs]").val("01");
	jQuery("input[name=personDivs]").val("01");
	
	jQuery("input[name=userDivs_pwd]").val("01");
	jQuery("input[name=personDivs_pwd]").val("01");
	
	jQuery("input[name=userName01]").focus();
	
	jQuery(".tab01").click(function(){
		var div = jQuery(this).attr("id");
		var divIndex = parseInt(div.substr(1,2))-1;
		var idLength = div.length;
		
		if(idLength < 4){
			jQuery("input[name=userName]").val("");
			jQuery("input[name=personDivs]").val("");
			jQuery("input[name=resdCorpNumb]").val("");
			jQuery("input[name=corpNumb]").val("");
			jQuery("input[name=mgnbYsno]").val("");
			jQuery("input[name=commName]").val("");
			
			jQuery("#tab11>li").each(function(index){
				if(divIndex == index){
					jQuery(this).addClass("on");
				}else{
					jQuery(this).removeClass("on");
				}
			})
			
			if(div == "a01"){
				jQuery("#box01_01").css("display","block");
				jQuery("#box01_02").css("display","none");
				jQuery("#box02_01").css("display","none");
				jQuery("#box03_01").css("display","none");
				jQuery("#box04_01").css("display","none");
				
				jQuery("input:radio[name=pDivs]").eq(0).attr("checked",true);
				jQuery("input[name=userDivs]").val("01");
				jQuery("input[name=userName01]").focus();
				jQuery("#radioBox").css("display","block");
			}else if(div == "a02"){
				jQuery("#box01_01").css("display","none");
				jQuery("#box01_02").css("display","none");
				jQuery("#box02_01").css("display","block");
				jQuery("#box03_01").css("display","none");
				jQuery("#box04_01").css("display","none");
	
				jQuery("#radioBox").css("display","none");
				
				jQuery("input[name=userDivs]").val("02");
				jQuery("input[name=userName02]").focus();
			}else if(div == "a03"){
				jQuery("#box01_01").css("display","none");
				jQuery("#box01_02").css("display","none");
				jQuery("#box02_01").css("display","none");
				jQuery("#box03_01").css("display","block");
				jQuery("#box04_01").css("display","none");
				
				jQuery("#radioBox").css("display","none");
				
				jQuery("input[name=userDivs]").val("03");
				jQuery("input[name=userName03]").focus();
			}else if(div == "a04"){
				jQuery("#box01_01").css("display","none");
				jQuery("#box01_02").css("display","none");
				jQuery("#box02_01").css("display","none");
				jQuery("#box03_01").css("display","none");
				jQuery("#box04_01").css("display","block");
				jQuery("input[name=userDivs]").val("");
				jQuery("#radioBox").css("display","none");
				jQuery("input[name=userName04]").focus();
			}
		}else{
			jQuery("input[name=userName_pwd]").val("");
			jQuery("input[name=userIdnt_pwd]").val("");
			jQuery("input[name=userEmail_pwd]").val("");
			jQuery("input[name=personDivs]").val("");
			jQuery("input[name=resdCorpNumb_pwd]").val("");
			jQuery("input[name=corpNumb_pwd]").val("");
			jQuery("input[name=mgnbYsno_pwd]").val("");
			jQuery("input[name=commName_pwd]").val("");
			
			jQuery("#tab12>li").each(function(index){
				if(divIndex == index){
					jQuery(this).addClass("on");
				}else{
					jQuery(this).removeClass("on");
				}
			})
			
			if(div == "a01_pwd"){
				jQuery("#box01_01_pwd").css("display","block");
				jQuery("#box01_02_pwd").css("display","none");
				jQuery("#box02_01_pwd").css("display","none");
				jQuery("#box03_01_pwd").css("display","none");
				jQuery("#box04_01_pwd").css("display","none");
				
				jQuery("input:radio[name=pDivs_pwd]").eq(0).attr("checked",true);
				jQuery("input[name=userDivs_pwd]").val("01");
				jQuery("input[name=userIdnt01_pwd]").focus();
				jQuery("#radioBox_pwd").css("display","block");
			}else if(div == "a02_pwd"){
				jQuery("#box01_01_pwd").css("display","none");
				jQuery("#box01_02_pwd").css("display","none");
				jQuery("#box02_01_pwd").css("display","block");
				jQuery("#box03_01_pwd").css("display","none");
				jQuery("#box04_01_pwd").css("display","none");
	
				jQuery("#radioBox_pwd").css("display","none");
				
				jQuery("input[name=userDivs_pwd]").val("02");
				jQuery("input[name=userIdnt02_pwd]").focus();
			}else if(div == "a03_pwd"){
				jQuery("#box01_01_pwd").css("display","none");
				jQuery("#box01_02_pwd").css("display","none");
				jQuery("#box02_01_pwd").css("display","none");
				jQuery("#box03_01_pwd").css("display","block");
				jQuery("#box04_01_pwd").css("display","none");
				
				jQuery("#radioBox_pwd").css("display","none");
				
				jQuery("input[name=userDivs_pwd]").val("03");
				jQuery("input[name=userIdnt03_pwd]").focus();
			}else if(div == "a04_pwd"){
				jQuery("#box01_01_pwd").css("display","none");
				jQuery("#box01_02_pwd").css("display","none");
				jQuery("#box02_01_pwd").css("display","none");
				jQuery("#box03_01_pwd").css("display","none");
				jQuery("#box04_01_pwd").css("display","block");
				jQuery("input[name=userDivs_pwd]").val("04");
				jQuery("#radioBox_pwd").css("display","none");
				jQuery("input[name=userIdnt04_pwd]").focus();
			}
		}
	})
	
	jQuery(":radio").click(function(){
		var div = jQuery(this).val();
		if(div == "p01"){
			jQuery("#box01_01").css("display","block");
			jQuery("#box01_02").css("display","none");
			jQuery("input[name=userName01]").focus();
			jQuery("#radioBox").css("display","block");
			jQuery("input:radio[name=pDivs]").eq(0).attr("checked",true);
		}else if(div == "p02"){
			jQuery("#box01_02").css("display","block");
			jQuery("#box01_01").css("display","none");
			jQuery("input:radio[name=pDivs]").attr("checked",true);
			jQuery("#radioBox").css("display","block");
		}else if(div == "p01_pwd"){
			jQuery("#box01_02_pwd").css("display","none");
			jQuery("#box01_01_pwd").css("display","block");
			jQuery("input:radio[name=pDivs_pwd]").eq(0).attr("checked",true);
			jQuery("#radioBox_pwd").css("display","block");
			jQuery("input[name=userIdnt01_pwd]").focus();
		}else if(div == "p02_pwd"){
			jQuery("#box01_02_pwd").css("display","block");
			jQuery("#box01_01_pwd").css("display","none");
			jQuery("input:radio[name=pDivs_pwd]").attr("checked",true);
			jQuery("#radioBox_pwd").css("display","block");
			jQuery("input[name=userIdnt01_02_pwd]").focus();
		}
	})
})


function init() {
	var frm = document.form1;
	var frm2 = document.form2;
	
	var userDivs = frm.userDivs.value;
	var userDivs2 = frm2.userDivs_pwd.value;
	var pswdUpdate = frm2.userInfoYn.value;
	/* var mail = frm2.mail.value; */
	
	if(pswdUpdate != null || pswdUpdate != ""){
		if(pswdUpdate == "Y"){
			
			alert("「"+mail+"」로 임시 비밀번호 발급 되었습니다.");
			
		}else if(pswdUpdate == "N"){
			
			alert("해당 비밀번호 정보가 없습니다.");
			
		}
	}
	
	/* if (userDivs == "01") {
		frm.corpNumb1.disabled = true;
		frm.corpNumb2.disabled = true;
		frm.corpNumb3.disabled = true;
		frm.resdCorpNumb1.disabled = false;
		frm.resdCorpNumb2.disabled = false;
	  	document.getElementById("commNameDt").style.display = "none";	  	
		document.getElementById("commNameDd").style.display = "none";  
		document.getElementById("mailDt").style.display = "none";
		document.getElementById("mailDd").style.display = "none";
		
  } else if (userDivs == "02") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
  		frm.resdCorpNumb1.disabled = false;
  		frm.resdCorpNumb2.disabled = false;
  		document.getElementById("commNameDt").style.display = "none";	  	
		document.getElementById("commNameDd").style.display = "none";  
		document.getElementById("mailDt").style.display = "none";
		document.getElementById("mailDd").style.display = "none";
  } else if (userDivs == "03") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
		frm.resdCorpNumb1.disabled = true;
		frm.resdCorpNumb2.disabled = true;
	  	document.getElementById("commNameDt").style.display = "none";	  	
		document.getElementById("commNameDd").style.display = "none";  
		document.getElementById("mailDt").style.display = "none";
		document.getElementById("mailDd").style.display = "none";
  }

  frm.userName.focus();
  
	if (userDivs2 == "01") {
		frm2.corpNumb1_pwd.disabled = true;
		frm2.corpNumb2_pwd.disabled = true;
		frm2.corpNumb3_pwd.disabled = true;
		frm2.resdCorpNumb1_pwd.disabled = false;
		frm2.resdCorpNumb2_pwd.disabled = false;
  } else if (userDivs2 == "02") {
  		frm2.corpNumb1_pwd.disabled = false;
		frm2.corpNumb2_pwd.disabled = false;
		frm2.corpNumb3_pwd.disabled = false;
  		frm2.resdCorpNumb1_pwd.disabled = false;
  		frm2.resdCorpNumb2_pwd.disabled = false;
  } else if (userDivs2 == "03") {
  		frm2.corpNumb1_pwd.disabled = false;
		frm2.corpNumb2_pwd.disabled = false;
		frm2.corpNumb3_pwd.disabled = false;
		frm2.resdCorpNumb1_pwd.disabled = true;
		frm2.resdCorpNumb2_pwd.disabled = true;
  } */
}


// 사업자등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginCrnChk(obj){
	// 사업자등록번호 앞자리의 길이를 확인하여 3자리, 2자리가 입력되면 다음 객체로 이동
	if(obj.name == "corpNumb1"){
		if(obj.value.length == 3)	document.form1.corpNumb2.focus();
	} else if(obj.name == "corpNumb2"){
		if(obj.value.length == 2)	document.form1.corpNumb3.focus();
	}
}

// 주민등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginSsnChk(obj){
	// 주민등록번호 앞자리의 길이를 확인하여 6자리가 입력되면 다음 객체로 이동
	if(obj.name == "resdCorpNumb1"){
		if(obj.value.length == 6)	document.form1.resdCorpNumb2.focus();
	}

	// EnterKey 입력시 공인인증서 로그인 수행
	if (event.keyCode == 13) {
		fn_userIdntSrch();
	}
}

function fn_userIdntSrch() {
	var frm = document.form1;
	var userDivs = frm.userDivs.value;
	
/*   if (Trim(frm.userName.value) == "") {
  	alert("이름/법인명을 입력하십시오.");
  	frm.userName.focus();
  	return;
  } else */
  
  if (userDivs == "01") {
	if (Trim(frm.userName01.value) == ""){
		alert("이름을 입력하십시오.");
		frm.userName01.focus();
		return;
	}else if (frm.resdCorpNumb101.value == "") {
  		alert("주민번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb101.focus();
  		return;
  	} else if (frm.resdCorpNumb101.value.length != 6) {
  		alert("주민번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb101.focus();
  		return;
  	} else if (frm.resdCorpNumb201.value == "") {
  		alert("주민번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb201.focus();
  		return;
  	} else if (frm.resdCorpNumb201.value.length != 7) {
  		alert("주민번호 뒷자리는 7자리 입니다.");
  		frm.resdCorpNumb201.focus();
  		return;
  	}
		ssn1 = frm.resdCorpNumb101.value;
		ssn2 = frm.resdCorpNumb201.value;
		
		frm.resdCorpNumb.value = frm.resdCorpNumb101.value + frm.resdCorpNumb201.value;
		
		frm.resdCorpNumb1.value = frm.resdCorpNumb101.value;
		frm.resdCorpNumb2.value = frm.resdCorpNumb201.value;
		
		frm.userName.value = frm.userName01.value;

		if(ssnCheck(ssn1, ssn2)) {              // 주민번호 체크
			return;
		}
  } else if (userDivs == "02") {
	if (Trim(frm.userName02.value) == ""){
		alert("법인명을 입력하십시오.");
		frm.userName02.focus();
		return;
	}else if (frm.resdCorpNumb102.value == "") {
  		alert("법인번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb102.focus();
  		return;
  	} else if (frm.resdCorpNumb102.value.length != 6) {
  		alert("법인번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb102.focus();
  		return;
  	} else if (frm.resdCorpNumb202.value == "") {
  		alert("법인번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb202.focus();
  		return;
  	} else if (frm.resdCorpNumb202.value.length != 7) {
  		alert("법인번호 뒷자리는 7자리 입니다.");
  		frm.resdCorpNumb202.focus();
  		return;
  	} else if (frm.corpNumb102.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb102.focus();
  		return;
    } else if (frm.corpNumb102.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb102.focus();
  		return;
   	} else if (frm.corpNumb202.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb202.focus();
  		return;
    } else if (frm.corpNumb202.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb202.focus();
  		return;
   	} else if (frm.corpNumb302.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb302.focus();
  		return;
    } else if (frm.corpNumb302.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb302.focus();
  		return;
  	}
  	frm.corpNumb.value = frm.corpNumb102.value + frm.corpNumb202.value + frm.corpNumb302.value;
  	frm.resdCorpNumb.value = frm.resdCorpNumb102.value + frm.resdCorpNumb202.value;
  	frm.userName.value = frm.userName02.value;
  	
  	frm.resdCorpNumb1.value = frm.resdCorpNumb102.value;
	frm.resdCorpNumb2.value = frm.resdCorpNumb202.value;

	frm.corpNumb1.value = frm.corpNumb102.value;
	frm.corpNumb2.value = frm.corpNumb202.value;
	frm.corpNumb3.value = frm.corpNumb302.value;

  	if (fncJuriRegNoCheck(frm.resdCorpNumb102.value + frm.resdCorpNumb202.value) == false) {                // 법인번호 체크
  		var conMsg = "법인등록번호("+frm.resdCorpNumb102.value+"-"+frm.resdCorpNumb202.value+")가 올바르지 않습니다.\n정확히 입력하셨는지 한번더 확인해 주시기 바랍니다.\n입력번호 그대로 진행하시겠습니까?"
			var conFlag = false;
			conFlag = confirm(conMsg);
 		  
 		  if( !conFlag){
 		  	frm.resdCorpNumb102.focus();
 		  	return;
 		  }
 		}

		if (check_busino(frm.corpNumb102.value + frm.corpNumb202.value + frm.corpNumb302.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb102.focus();
 		  return;
 		}
  } else if (userDivs == "03") {
	  if (Trim(frm.userName03.value) == ""){
		alert("이름을 입력하십시오.");
		frm.userName03.focus();
		return;
	}else if(frm.corpNumb103.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb103.focus();
  		return;
    } else if (frm.corpNumb103.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb103.focus();
  		return;
   	} else if (frm.corpNumb203.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb203.focus();
  		return;
    } else if (frm.corpNumb203.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb203.focus();
  		return;
   	} else if (frm.corpNumb303.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb303.focus();
  		return;
    } else if (frm.corpNumb303.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb303.focus();
  		return;
  	}
  	
		frm.corpNumb.value = frm.corpNumb103.value + frm.corpNumb203.value + frm.corpNumb303.value;
		
		frm.userName.value = frm.userName03.value;
		

		frm.corpNumb1.value = frm.corpNumb103.value;
		frm.corpNumb2.value = frm.corpNumb203.value;
		frm.corpNumb3.value = frm.corpNumb303.value;
		
		if (check_busino(frm.corpNumb103.value + frm.corpNumb203.value + frm.corpNumb303.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb103.focus();
 		  return;
 		}
  } else if (userDivs == "") {
	  if (Trim(frm.userName04.value) == ""){
			alert("이름을 입력하십시오.");
			frm.userName04.focus();
			return;
		}else if(frm.commName04.value == "") {
	  		alert("단체 및 기관명을 입력하십시오.");
	  		frm.commName04.focus();
	  		return;
	    }else if(frm.mail04.value.value == "") {
	    	alert("이메일을 입력하십시오.");
	  		frm.mail04.focus();
	  		return;
	   	}
			frm.userName.value = frm.userName04.value;
			frm.mail.value = frm.mail04.value;
			frm.commName.value= frm.commName04.value;
			frm.mgnbYsno.value = "Y";
	  }
  if(userDivs == "01"){
 	frm.method = "post";
	frm.action = "/NameCheck/nc_p_idntSrch.jsp";
	frm.submit();
  }else{
 	frm.method = "post";
	frm.action = "/user/user.do?method=userIdntPswdSrch";
	frm.submit();
  }
}

function fn_userDivs() {
	var frm = document.form1;
	var userDivs = frm.userDivs.value;

	if (userDivs == "01") {
		frm.corpNumb1.disabled = true;
		frm.corpNumb2.disabled = true;
		frm.corpNumb3.disabled = true;
		frm.resdCorpNumb1.disabled = false;
		frm.resdCorpNumb2.disabled = false;
		document.getElementById("resdCorpNumbDt").style.display = "block";
		document.getElementById("resdCorpNumbDd").style.display = "block";
		document.getElementById("corpNumbDt").style.display = "block";
	  	document.getElementById("corpNumbDd").style.display = "block";
	  	document.getElementById("commNameDt").style.display = "none";	  	
		document.getElementById("commNameDd").style.display = "none";  
		document.getElementById("mailDt").style.display = "none";
		document.getElementById("mailDd").style.display = "none";
  } else if (userDivs == "02") {
  		frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
  		frm.resdCorpNumb1.disabled = false;
  		frm.resdCorpNumb2.disabled = false;
		document.getElementById("resdCorpNumbDt").style.display = "block";
		document.getElementById("resdCorpNumbDd").style.display = "block";
		document.getElementById("corpNumbDt").style.display = "block";
	  	document.getElementById("corpNumbDd").style.display = "block";
	  	document.getElementById("commNameDt").style.display = "none";	  	
		document.getElementById("commNameDd").style.display = "none";  
		document.getElementById("mailDt").style.display = "none";
		document.getElementById("mailDd").style.display = "none";
  } else if (userDivs == "03") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
		frm.resdCorpNumb1.disabled = true;
		frm.resdCorpNumb2.disabled = true;
		document.getElementById("resdCorpNumbDt").style.display = "block";
		document.getElementById("resdCorpNumbDd").style.display = "block";
		document.getElementById("corpNumbDt").style.display = "block";
	  	document.getElementById("corpNumbDd").style.display = "block";
	  	document.getElementById("commNameDt").style.display = "none";	  	
		document.getElementById("commNameDd").style.display = "none";  
		document.getElementById("mailDt").style.display = "none";
		document.getElementById("mailDd").style.display = "none";
  }else if (userDivs == "") {
	  	document.getElementById("resdCorpNumbDt").style.display = "none";
	  	document.getElementById("resdCorpNumbDd").style.display = "none";
	  	document.getElementById("corpNumbDt").style.display = "none";
	  	document.getElementById("corpNumbDd").style.display = "none";
		document.getElementById("commNameDt").style.display = "block";	  	
		document.getElementById("commNameDd").style.display = "block";  
		document.getElementById("mailDt").style.display = "block";
		document.getElementById("mailDd").style.display = "block";
  }
  frm.userName.focus();

  frm.userName.value = "";
  frm.resdCorpNumb1.value = "";
  frm.resdCorpNumb2.value = "";
  frm.corpNumb1.value = "";
  frm.corpNumb2.value = "";
  frm.corpNumb3.value = "";
}

function fn_userDivs2() {
	var frm = document.form2;
	var userDivs = frm.userDivs_pwd.value;

	if (userDivs == "01") {
		frm.corpNumb1_pwd.disabled = true;
		frm.corpNumb2_pwd.disabled = true;
		frm.corpNumb3_pwd.disabled = true;
		frm.resdCorpNumb1_pwd.disabled = false;
		frm.resdCorpNumb2_pwd.disabled = false;
  } else if (userDivs == "02") {
  	frm.corpNumb1_pwd.disabled = false;
		frm.corpNumb2_pwd.disabled = false;
		frm.corpNumb3_pwd.disabled = false;
  	frm.resdCorpNumb1_pwd.disabled = false;
  	frm.resdCorpNumb2_pwd.disabled = false;
  } else if (userDivs == "03") {
  	frm.corpNumb1_pwd.disabled = false;
		frm.corpNumb2_pwd.disabled = false;
		frm.corpNumb3_pwd.disabled = false;
		frm.resdCorpNumb1_pwd.disabled = true;
		frm.resdCorpNumb2_pwd.disabled = true;
  }
  else if (userDivs == "") {
	  	frm.corpNumb1_pwd.disabled = true;
			frm.corpNumb2_pwd.disabled = true;
			frm.corpNumb3_pwd.disabled = true;
			frm.resdCorpNumb1_pwd.disabled = true;
			frm.resdCorpNumb2_pwd.disabled = true;
	  }
  frm.userIdnt_pwd.focus();

  frm.userName_pwd.value = "";
  frm.resdCorpNumb1_pwd.value = "";
  frm.resdCorpNumb2_pwd.value = "";
  frm.corpNumb1_pwd.value = "";
  frm.corpNumb2_pwd.value = "";
  frm.corpNumb3_pwd.value = "";
  frm.userEmail_pwd.value = "";
}

// 사업자등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginCrnChk2(obj){
	/*
	// 사업자등록번호 앞자리의 길이를 확인하여 3자리, 2자리가 입력되면 다음 객체로 이동
	if(obj.name == "corpNumb1_pwd"){
		if(obj.value.length == 3)	document.form2.corpNumb2_pwd.focus();
	} else if(obj.name == "corpNumb2_pwd"){
		if(obj.value.length == 2)	document.form2.corpNumb3_pwd.focus();
	}
	*/
}

// 주민등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginSsnChk2(obj){
	/*
	// 주민등록번호 앞자리의 길이를 확인하여 6자리가 입력되면 다음 객체로 이동
	if(obj.name == "resdCorpNumb1_pwd"){
		if(obj.value.length == 6)	document.form2.resdCorpNumb2_pwd.focus();
	}

	// EnterKey 입력시 공인인증서 로그인 수행
	if (event.keyCode == 13) {
		fn_userPswdSrch();
	}
	*/
}

function fn_userPswdSrch() {
	var frm = document.form2;
	var userDivs = frm.userDivs_pwd.value;
	
/*   if (Trim(frm.userName.value) == "") {
  	alert("이름/법인명을 입력하십시오.");
  	frm.userName.focus();
  	return;
  } else */
  
  if (userDivs == "01") {
	if (Trim(frm.userIdnt01_pwd.value) == ""){
		alert("아이디를 입력하십시오.");
		frm.userIdnt01_pwd.focus();
		return;
	}else if (Trim(frm.userName01_pwd.value) == ""){
		alert("이름을 입력하십시오.");
		frm.userName01_pwd.focus();
		return;
	}else if (frm.resdCorpNumb101_pwd.value == "") {
  		alert("주민번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb101_pwd.focus();
  		return;
  	} else if (frm.resdCorpNumb101_pwd.value.length != 6) {
  		alert("주민번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb101_pwd.focus();
  		return;
  	} else if (frm.resdCorpNumb201_pwd.value == "") {
  		alert("주민번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb201_pwd.focus();
  		return;
  	} else if (frm.resdCorpNumb201_pwd.value.length != 7) {
  		alert("주민번호 뒷자리는 7자리 입니다.");
  		frm.resdCorpNumb201_pwd.focus();
  		return;
  	} else if (frm.userEmail01_pwd.value == "") {
  		alert("이메일을 입력하십시오.");
  		frm.userEmail01_pwd.focus();
  		return;
  	}
		ssn1 = frm.resdCorpNumb101_pwd.value;
		ssn2 = frm.resdCorpNumb201_pwd.value;
		
		frm.resdCorpNumb_pwd.value = frm.resdCorpNumb101_pwd.value + frm.resdCorpNumb201_pwd.value;//주민번호
		
		frm.resdCorpNumb1_pwd.value = frm.resdCorpNumb101_pwd.value;
		frm.resdCorpNumb2_pwd.value = frm.resdCorpNumb201_pwd.value;
		
		frm.userName_pwd.value = frm.userName01_pwd.value;//이름
		frm.userIdnt_pwd.value = frm.userIdnt01_pwd.value;//아이디
		
		frm.userEmail_pwd.value = frm.userEmail01_pwd.value; //이메일

		if(ssnCheck(ssn1, ssn2)) {              // 주민번호 체크
			return;
		}
  } else if (userDivs == "02") {
	  if (Trim(frm.userIdnt02_pwd.value) == ""){
			alert("아이디를 입력하십시오.");
			frm.userIdnt02_pwd.focus();
			return;
	  }else if (Trim(frm.userName02_pwd.value) == ""){
		  	alert("법인명을 입력하십시오.");
			frm.userName02_pwd.focus();
			return;
	  }else if (frm.resdCorpNumb102_pwd.value == "") {
	 			alert("법인번호 앞자리를 입력하십시오.");
	 			frm.resdCorpNumb102_pwd.focus();
	 			return;
  	} else if (frm.resdCorpNumb102_pwd.value.length != 6) {
  		alert("법인번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb102_pwd.focus();
  		return;
  	} else if (frm.resdCorpNumb202_pwd.value == "") {
  		alert("법인번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb202_pwd.focus();
  		return;
  	} else if (frm.resdCorpNumb202_pwd.value.length != 7) {
  		alert("법인번호 뒷자리는 7자리 입니다.");
  		frm.resdCorpNumb202_pwd.focus();
  		return;
  	} else if (frm.corpNumb102_pwd.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb102_pwd.focus();
  		return;
    } else if (frm.corpNumb102_pwd.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb102_pwd.focus();
  		return;
   	} else if (frm.corpNumb202_pwd.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb202_pwd.focus();
  		return;
    } else if (frm.corpNumb202_pwd.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb202_pwd.focus();
  		return;
   	} else if (frm.corpNumb302_pwd.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb302_pwd.focus();
  		return;
    } else if (frm.corpNumb302_pwd.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb302_pwd.focus();
  		return;
  	}
    /* 
    else if (frm.userEmail02_pwd.value == "") {
  		alert("이메일을 입력하십시오.");
  		frm.userEmail02_pwd.focus();
  		return;
  	}
	 */  
  	frm.corpNumb_pwd.value = frm.corpNumb102_pwd.value + frm.corpNumb202_pwd.value + frm.corpNumb302_pwd.value;//사업자번호
  	frm.resdCorpNumb_pwd.value = frm.resdCorpNumb102_pwd.value + frm.resdCorpNumb202_pwd.value;//주민번호
  	frm.userName_pwd.value = frm.userName02_pwd.value;//이름
  	frm.userIdnt_pwd.value = frm.userIdnt02_pwd.value;//아이디
  	//frm.userEmail_pwd.value = frm.userEmail02_pwd.value;//이메일
  	
  	
  	frm.resdCorpNumb1_pwd.value = frm.resdCorpNumb102_pwd.value;
	frm.resdCorpNumb2_pwd.value = frm.resdCorpNumb202_pwd.value;

	frm.corpNumb1_pwd.value = frm.corpNumb102_pwd.value;
	frm.corpNumb2_pwd.value = frm.corpNumb202_pwd.value;
	frm.corpNumb3_pwd.value = frm.corpNumb302_pwd.value;

  	if (fncJuriRegNoCheck(frm.resdCorpNumb102_pwd.value + frm.resdCorpNumb202_pwd.value) == false) {                // 법인번호 체크
		
			var conMsg = "법인등록번호("+frm.resdCorpNumb102_pwd.value+"-"+frm.resdCorpNumb202_pwd.value+")가 올바르지 않습니다.\n정확히 입력하셨는지 한번더 확인해 주시기 바랍니다.\n입력번호 그대로 진행하시겠습니까?"
			var conFlag = false;
			conFlag = confirm(conMsg);
 		  
 		  if( !conFlag){
 		  	frm.resdCorpNumb102_pwd.focus();
 		  	return;
 		  } 		

 		}

		if (check_busino(frm.corpNumb102_pwd.value + frm.corpNumb202_pwd.value + frm.corpNumb302_pwd.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb102_pwd.focus();
 		  return;
 		}
  } else if (userDivs == "03") {
    if (Trim(frm.userIdnt03_pwd.value) == ""){
		alert("아이디를 입력하십시오.");
		frm.userIdnt03_pwd.focus();
		return;
    }else if (Trim(frm.userName03_pwd.value) == ""){
	  	alert("이름을 입력하십시오.");
		frm.userName03_pwd.focus();
		return;
    }else if(frm.corpNumb103_pwd.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb103_pwd.focus();
  		return;
    } else if (frm.corpNumb103_pwd.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb103_pwd.focus();
  		return;
   	} else if (frm.corpNumb203_pwd.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb203_pwd.focus();
  		return;
    } else if (frm.corpNumb203_pwd.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb203_pwd.focus();
  		return;
   	} else if (frm.corpNumb303_pwd.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb303_pwd.focus();
  		return;
    } else if (frm.corpNumb303_pwd.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb303_pwd.focus();
  		return;
  	} 
    /* 
    else if (frm.userEmail03_pwd.value == "") {
  		alert("이메일을 입력하십시오.");
  		frm.userEmail03_pwd.focus();
  		return;
  	}
  	 */
		frm.corpNumb_pwd.value = frm.corpNumb103_pwd.value + frm.corpNumb203_pwd.value + frm.corpNumb303_pwd.value;//사업자번호
		
		frm.userName_pwd.value = frm.userName03_pwd.value;//이름
		frm.userIdnt_pwd.value = frm.userIdnt03_pwd.value;//아이디
		//frm.userEmail_pwd.value = frm.userEmail03_pwd.value;//이메일
		

		frm.corpNumb1_pwd.value = frm.corpNumb103_pwd.value;
		frm.corpNumb2_pwd.value = frm.corpNumb203_pwd.value;
		frm.corpNumb3_pwd.value = frm.corpNumb303_pwd.value;
		
		if (check_busino(frm.corpNumb103_pwd.value + frm.corpNumb203_pwd.value + frm.corpNumb303_pwd.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb103_pwd.focus();
 		  return;
 		}
  } else if (userDivs == "") {
	  if (Trim(frm.userIdnt04_pwd.value) == ""){
			alert("아이디를 입력하십시오.");
			frm.userIdnt04_pwd.focus();
			return;
	    }else if (Trim(frm.userName04_pwd.value) == ""){
		  	alert("이름을 입력하십시오.");
			frm.userName04_pwd.focus();
			return;
	    }
			frm.userIdnt_pwd.value = frm.userIdnt04_pwd.value;//이름
			frm.userName_pwd.value = frm.userName04_pwd.value;//이름
			frm.mgnbYsno_pwd.value ="Y";
	  }
  if(userDivs == "01"){
 	frm.method = "post";
	frm.action = "/NameCheck/nc_p_pswdSrch.jsp";
	frm.submit();
   }else if(userDivs == "02"){
	  frm.userIdnt_pwd.value = frm.userIdnt02_pwd.value; //id
	 	frm.method = "post";
		/* frm.action = "/user/user.do?method=userIdntPswdSrch"; */
		frm.action = "/user/user.do?method=userPswdSrch2";
		frm.submit();
   }else {
	frm.userIdnt_pwd.value = frm.userIdnt03_pwd.value; //id
 	frm.method = "post";
	/* frm.action = "/user/user.do?method=userIdntPswdSrch"; */
	frm.action = "/user/user.do?method=userPswdSrch2";
	frm.submit();
  }
}
//-->
</script>
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-2', 'auto');
  ga('send', 'pageview');
</script>
</head>
<body onLoad="init();">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			
				<!-- 래프 -->
				<div class="con_lf" style="width: 22%">
					<div class="con_lf_big_title">회원정보</div>
					<ul class="sub_lf_menu">
						<li><a href="/user/user.do?method=goLogin">로그인</a></li>
						<li><a href="/user/user.do?method=goPage">회원가입</a></li>
						<li><a href="/user/user.do?method=goIdntSrch" class="on">아이디 찾기</a></li>
						<li><a href="/user/user.do?method=goPswdSrch">비밀번호 찾기</a></li>
					</ul>
				</div>
				<!-- //래프 -->
				
				
				<!-- 안심본인인증 아이디찾기 -->
				<form name="frm1" method="post" >
					<input type="hidden" name="userDivs" value="01" >
					<input type="hidden" name="userName" value="" >
					<input type="hidden" name="dupInfo" value="" >
					<input type="hidden" name="connInfo" value="" >
					<input type="hidden" name="birthDate" value="" >
					<input type="hidden" name="pswdSrch" value="">
				</form>
				
				<!-- 안심본인인증 비밀번호 찾기 -->
				<form name="frm2" method="post" >
					<input type="hidden" name="userDivs" id="userDivs1" value="01" >
					<input type="hidden" name="requestNumber" value="" >
					<input type="hidden" name="responseNumber" value="" >
					<input type="hidden" name="userName" value="" >
					<input type="hidden" name="dupInfo" value="" >
					<input type="hidden" name="connInfo" value="" >
					<input type="hidden" name="birthDate" value="" >
					<input type="hidden" name="userIdnt_pwd" value="">
					<input type="hidden" name="userEmail_pwd" value="">
					<input type="hidden" name="userName_pwd" value="">
					<input type="hidden" name="userDivs_pwd" value="01">
					<input type="hidden" name="pswdSrch" value="Y">
				</form>
				
				
				<!-- 안심본인인증 서비스 팝업을 호출하기 위해서는 다음과 같은 form이 필요합니다. -->
				<form name="form_chk" method="post">
					<input type="hidden" name="m" value="checkplusSerivce">			<!-- 필수 데이타로, 누락하시면 안됩니다. -->
					<input type="hidden" name="EncodeData" value="<%= sEncData1 %>">	<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
	 			
	 				<input type="hidden" name="param_r1" value="">
					<input type="hidden" name="param_r2" value="">
					<input type="hidden" name="param_r3" value="">
				</form>
				
				
				<!-- 가상주민번호 서비스 팝업을 호출하기 위해서는 다음과 같은 form이 필요합니다. -->
				<form name="form_ipin" method="post">
					<input type="hidden" name="m" value="pubmain">						<!-- 필수 데이타로, 누락하시면 안됩니다. -->
				    <input type="hidden" name="enc_data" value="<%= sEncData %>">		<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
				    <!-- 업체에서 응답받기 원하는 데이타를 설정하기 위해 사용할 수 있으며, 인증결과 응답시 해당 값을 그대로 송신합니다.
				    	 해당 파라미터는 추가하실 수 없습니다. -->
				    <input type="hidden" name="param_r1" value="">
				    <input type="hidden" name="param_r2" value="">
				    <input type="hidden" name="param_r3" value="">
				</form>
				
				<!-- 가상주민번호 서비스 팝업 페이지에서 사용자가 인증을 받으면 암호화된 사용자 정보는 해당 팝업창으로 받게됩니다.
					 따라서 부모 페이지로 이동하기 위해서는 다음과 같은 form이 필요합니다. -->
				<form name="vnoform" method="post">
					<input type="hidden" name="enc_data">								<!-- 인증받은 사용자 정보 암호화 데이타입니다. -->
					
					<!-- 업체에서 응답받기 원하는 데이타를 설정하기 위해 사용할 수 있으며, 인증결과 응답시 해당 값을 그대로 송신합니다.
				    	 해당 파라미터는 추가하실 수 없습니다. -->
				    <input type="hidden" name="param_r1" value="">
				    <input type="hidden" name="param_r2" value="">
				    <input type="hidden" name="param_r3" value="">
				</form>
				
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						회원정보
						&gt;
						<span class="bold">아이디/비밀번호 찾기</span>
					</div>
					<div class="con_rt_hd_title">아이디/비밀번호 찾기</div>
					<div class="sub_contents_con">
						
						<form class="frm" name="form1" action="#">
						<input type="hidden" name="resdCorpNumb">
						<input type="hidden" name="corpNumb">
						<input type="hidden" name="userDivs">
						<input type="hidden" name="personDivs">
						<input type="hidden" name="userName">
						
						<input type="hidden" name="resdCorpNumb1">
						<input type="hidden" name="resdCorpNumb2">
						
						<input type="hidden" name="corpNumb1">
						<input type="hidden" name="corpNumb2">
						<input type="hidden" name="corpNumb3">
						
						<input type="hidden" name="mgnbYsno">
						<input type="hidden" name="commName">
						
						<input type="hidden" name="mail">
						
						<input type="hidden" name="pswdSrch" value="">
						
						
						<input type="submit" style="display:none;">
						
						<br/><h2 class="sub_con_h2">아이디찾기</h2>
						
						<!-- 아이디찾기>안심본인인증 -->
						<div id="box01_01">
							<ul class="sub_menu1 w201 mar_tp40">
								<li class="on"><a href="#1" class="tab01 on" id="a01">개인회원</a></li>
								<li><a href="#1" class="tab01" id="a02">법인사업자</a></li>
								<li><a href="#1" class="tab01" id="a03">개인사업자</a></li>
								<li><a href="#1" class="last_rt_bor tab01" id="a04">관리자</a></li>
							</ul>
							<p class="clear"></p>
							<div class="mar_tp30">
								<input type="radio" id="pDivs04" name="pDivs"  value="p01" style="vertical-align: -2px;" /> <label for="inp1">안심본인인증</label>
								<input type="radio" id="pDivs05" name="pDivs"  value="p02" style="vertical-align: -2px;" /> <label for="inp2">아이핀(I-Pin)</label>
							</div>
							<div class="login_bg mar_tp20">
								<div class="float_lf pad_lf20"><img src="/images/main/login_01.png" alt="그림" /></div>
								<div class="float_lf mar_lf50 mar_tp20">
									<div class="bold">안심본인인증을 통해 아이디를 찾으실 수 있습니다. <br />본인인증시 제공되는 정보는 해당 인증기관에서 직접 수집하며, 인증 이외의<br />용도로 이용 또는 저장하지 않습니다.</div>
									<div class="align_cen mar_tp20"><a href="javascript:fnPopup2();" class="pop_check">인증하기</a></div>
								</div>
								<p class="clear"></p>
								<div class="mar_tp30" style="border-top:1px solid #dddddd;"></div>
								<div class="login_foot">
									<div class="bold"><img src="/images/main/login_02.png" alt="login_01" />&nbsp;알려드립니다.</div>
									<p class="pad_lf20 mar_tp10">회원가입시 입력하신 가입정보를 통해 아이디를 찾으실 수 있습니다.</p>
								</div>
							</div>
						</div>
						<!-- //아이디찾기>안심본인인증 -->
						
						<!-- 아이디찾기>아이핀인증 -->
						<div id="box01_02" style="display: none;">
							<ul class="sub_menu1 w201 mar_tp40">
								<li class="on"><a href="#1" class="tab01 on" id="a01">개인회원</a></li>
								<li><a href="#1" class="tab01" id="a02">법인사업자</a></li>
								<li><a href="#1" class="tab01" id="a03">개인사업자</a></li>
								<li><a href="#1" class="last_rt_bor tab01" id="a04">관리자</a></li>
							</ul>
							<p class="clear"></p>
							<div class="mar_tp30">
								<input type="radio" id="pDivs04" name="pDivs"  value="p01" style="vertical-align: -2px;" /> <label for="inp1">안심본인인증</label>
								<input type="radio" id="pDivs05" name="pDivs"  value="p02" style="vertical-align: -2px;" /> <label for="inp2">아이핀(I-Pin)</label>
							</div>
							<div class="login_bg mar_tp20">
								<div class="float_lf pad_lf20"><img src="/images/main/login_01.png" alt="그림" /></div>
								<div class="float_lf mar_lf50 mar_tp20">
									<div class="bold">아이핀(I-Pin)인증을 통해 아이디를 찾으실 수 있습니다. <br />본인인증시 제공되는 정보는 해당 인증기관에서 직접 수집하며, 인증 이외의 <br/>용도로 이용 또는 저장하지 않습니다.</div>
									<div class="align_cen mar_tp20"><a href="javascript:fnPopup();" class="pop_check">인증하기</a></div>
								</div>
								<p class="clear"></p>
								<div class="mar_tp30" style="border-top:1px solid #dddddd;"></div>
								<div class="login_foot">
									<div class="bold"><img src="/images/main/login_02.png" alt="login_01" />&nbsp;알려드립니다.</div>
									<p class="pad_lf20 mar_tp10">회원가입시 입력하신 가입정보를 통해 아이디를 찾으실 수 있습니다.</p>
								</div>
							</div>
						</div>
						<!-- //아이디찾기>아이핀인증 -->
						
						<!-- 법인사업자 -->
						<div id="box02_01" style="display: none;">
							<ul class="sub_menu1 w201 mar_tp40">
								<li><a href="#1" class="tab01" id="a01">개인회원</a></li>
								<li class="on"><a href="#1" class="tab01 on" id="a02">법인사업자</a></li>
								<li><a href="#1" class="tab01" id="a03">개인사업자</a></li>
								<li><a href="#1" class="last_rt_bor tab01" id="a04">관리자</a></li>
							</ul>
							<p class="clear"></p>
						<div class="login_bg mar_tp30">
							<div class="float_lf pad_lf20"><img src="/images/main/login_01.png" alt="그림" /></div>
							<div class="login_rt_tp">
								<div class="login_rt_tp_lf pad_lf20">
									<div class="login_rt_tp_lf_lf">
										<div class="login_word">법인명</div>
										<div class="login_input"><input type="text" title="계정 입력" id="userName02" name="userName02" /></div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_tp_lf_rt">
										<div class="login_word">법인번호</div>
										<div class="login_input"><input type="text" id="resdCorpNumb102" name="resdCorpNumb102" title="법인번호(앞자리)" size="7" maxlength="6" onkeypress="cfInputNumRT(event);" style="width:27%;" /> - <input type="text" id="resdCorpNumb202" name="resdCorpNumb202" type="password" title="법인번호(뒷자리)" size="19" maxlength="7" onkeypress="cfInputNumRT(event);" style="width:60%;" /></div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_tp_lf_rt mar_tp10">
										<div class="login_word">사업자번호</div>
										<div class="login_input"><input type="text" id="corpNumb102" name="corpNumb102" title="사업자번호(1번째)" size="7" maxlength="3" onkeypress="cfInputNumRT(event);" style="width:27%;" /> - <input type="text" id="corpNumb202" name="corpNumb202" title="사업자번호(2번째)" size="7" maxlength="2" onkeypress="cfInputNumRT(event);" style="width:26%;" /> - <input type="text" id="corpNumb302" name="corpNumb302" title="사업자번호(3번째)" size="9" maxlength="5" onkeypress="cfInputNumRT(event);" style="width:26%;" /></div>
										<p class="clear"></p>
									</div>
								</div>
								<div class="login_rt_tp_rt mar_tp20">
									<a href="#1" onclick="javascript:fn_userIdntSrch();" class="login_bg_2c65aa">확 인</a>
								</div>
								<p class="clear"></p>
							</div>
							<p class="clear"></p>
							<div class="mar_tp30" style="border-top:1px solid #dddddd;"></div>
							<div class="login_foot">
								<div class="bold"><img src="/images/main/login_02.png" alt="login_01" />&nbsp;알려드립니다.</div>
								<p class="pad_lf20 mar_tp10">회원가입시 입력하신 가입정보를 통해 아이디를 찾으실 수 있습니다.</p>
							</div>
						</div>
						</div>
						<!-- //법인사업자 -->
						
						<!-- 개인사업자 -->
						<div id="box03_01" style="display: none;">
							<ul class="sub_menu1 w201 mar_tp40">
								<li><a href="#1" class="tab01" id="a01">개인회원</a></li>
								<li><a href="#1" class="tab01" id="a02">법인사업자</a></li>
								<li class="on"><a href="#1" class="tab01 on" id="a03">개인사업자</a></li>
								<li><a href="#1" class="last_rt_bor tab01" id="a04">관리자</a></li>
							</ul>
							<p class="clear"></p>
						<div class="login_bg mar_tp30">
							<div class="float_lf pad_lf20"><img src="/images/main/login_01.png" alt="그림" /></div>
							<div class="login_rt_tp">
								<div class="login_rt_tp_lf pad_lf20">
									<div class="login_rt_tp_lf_lf">
										<div class="login_word">이름</div>
										<div class="login_input"><input type="text" id="userName03" name="userName03" title="계정 입력" /></div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_tp_lf_rt mar_tp10">
										<div class="login_word">사업자번호</div>
										<div class="login_input"><input type="text" id="corpNumb103" name="corpNumb103" title="사업자번호(1번째)" size="7" maxlength="3" onkeypress="cfInputNumRT(event);" style="width:27%;" /> - <input type="text" id="corpNumb203" name="corpNumb203" title="사업자번호(2번째)" size="7" maxlength="2" onkeypress="cfInputNumRT(event);" style="width:26%;" /> - <input type="text" id="corpNumb303" name="corpNumb303" title="사업자번호(3번째)" size="9" maxlength="5" onkeypress="cfInputNumRT(event);" style="width:25.5%;" /></div>
										<p class="clear"></p>
									</div>
								</div>
								<div class="login_rt_tp_rt">
									<a href="#1" onclick="javascript:fn_userIdntSrch();" class="login_bg_2c65aa">확 인</a>
								</div>
								<p class="clear"></p>
							</div>
							<p class="clear"></p>
							<div class="mar_tp30" style="border-top:1px solid #dddddd;"></div>
							<div class="login_foot">
								<div class="bold"><img src="/images/main/login_02.png" alt="login_01" />&nbsp;알려드립니다.</div>
								<p class="pad_lf20 mar_tp10">회원가입시 입력하신 가입정보를 통해 아이디를 찾으실 수 있습니다.</p>
							</div>
						</div>
						</div>
						<!-- //개인사업자 -->
						
						
						<!-- 관리자 -->
						<div id="box04_01" style="display: none;">
							<ul class="sub_menu1 w201 mar_tp40">
								<li><a href="#1" class="tab01" id="a01">개인회원</a></li>
								<li><a href="#1" class="tab01" id="a02">법인사업자</a></li>
								<li><a href="#1" class="tab01" id="a03">개인사업자</a></li>
								<li class="on"><a href="#1" class="last_rt_bor tab01 on" id="a04">관리자</a></li>
							</ul>
							<p class="clear"></p>
						<div class="login_bg mar_tp30">
							<div class="float_lf pad_lf20"><img src="/images/main/login_01.png" alt="그림" /></div>
							<div class="login_rt_tp">
								<div class="login_rt_tp_lf pad_lf20">
									<div class="login_rt_tp_lf_lf">
										<div class="login_word" style="width:30%;">이름</div>
										<div class="login_input" style="width:68%;"><input type="text" id="userName04" name="userName04" title="계정 입력" /></div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_tp_lf_lf">
										<div class="login_word" style="width:30%;">단체 및 기관명</div>
										<div class="login_input" style="width:68%;"><input type="text" id="commName04" name="commName04" title="계정 입력" /></div>
										<p class="clear"></p>
									</div>
									<div class="login_rt_tp_lf_lf">
										<div class="login_word" style="width:30%;">이메일</div>
										<div class="login_input" style="width:68%;"><input type="text" id="mail04" name="mail04" title="계정 입력" /></div>
										<p class="clear"></p>
									</div>
								</div>
								<div class="login_rt_tp_rt mar_tp30">
									<a href="#1" onclick="javascript:fn_userIdntSrch();" class="login_bg_2c65aa">확 인</a>
								</div>
								<p class="clear"></p>
							</div>
							<p class="clear"></p>
							<div class="mar_tp30" style="border-top:1px solid #dddddd;"></div>
							<div class="login_foot">
								<div class="bold"><img src="/images/main/login_02.png" alt="login_01" />&nbsp;알려드립니다.</div>
								<p class="pad_lf20 mar_tp10">회원가입시 입력하신 가입정보를 통해 아이디를 찾으실 수 있습니다.</p>
							</div>
						</div>
						</div>
						<!-- //관리자 -->
						</form>
						
						
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
			</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->


</body>
</html>
