<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<head>
<meta charset="EUC-KR">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>로그인 | 회원정보 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript">
</script>
<style type="text/css">
	th, td {
    border: 1px solid #444444;
    border: 0px,0px,0px,0px;
  }
  table{
  	border: 0px,0px,0px,0px;
  }
</style>
</head>
<body>


<input type="hidden" name="works_id" value="${ImageDTO.works_id}">
<c:choose>
	<c:when test="${ImageDTO.genre_cd==7}">
		<table>
			<tr>
				<tr>
				<td><img alt="이미지" src="${ImageDTO.img_url}" onError="javascript:this.src='http://www.findcopyright.or.kr:8080/images/noimg/noimg.jpg'"></td>
				<td>
					<table>
						<tr>
							<td colspan="2"><div>제목 : ${ImageDTO.works_title}</div></td>
						</tr>
						<tr>
							<td colspan="2"><div>부제 : ${ImageDTO.works_sub_title}</div></td>
						</tr>
						<tr>
							<td>이미지 코드 :</td><td>${ImageDTO.works_id}</td>
						</tr>
						<tr>
							<td>작가 :</td><td>${ImageDTO.writer}</td>
						</tr>
						<tr>
							<td>창작연도 :</td><td>${ImageDTO.crt_year}</td>
						</tr>
						<tr>
							<td>저작년월일 :</td><td>${ImageDTO.make_date}</td>
						</tr>
						<tr>
							<td>장르 :</td><td>${ImageDTO.genre_cd}</td>
						</tr>
						<tr>
							<td>이미지 크기 :</td><td>${ImageDTO.size_info}</td>
						</tr>
						<tr>
							<td>해상도 :</td><td>${ImageDTO.resolution}</td>
						</tr>
						<tr>
							<td>주재료 :</td><td>${ImageDTO.main_mtrl}</td>
						</tr>
						<tr>
							<td>구조 및 특징 :</td><td>${ImageDTO.txtr}</td>
						</tr>
						<tr>
							<td>소장년월일 :</td><td>${ImageDTO.poss_date}</td>
						</tr>
						<tr>
							<td>소장기관명 :</td><td>${ImageDTO.poss_orgn_name}</td>
						</tr>
						<tr>
							<td>국가 :</td><td>${ImageDTO.nation_cd}</td>
						</tr>
						<tr>
							<td>창작연도 :</td><td>${ImageDTO.crt_year}</td>
						</tr>
						<tr>
							<td>이미지 설명 :</td><td>${ImageDTO.image_desc}</td>
						</tr>
					</table>
				</td>
				<tr>
			</tr>
		</table>
	</c:when>
	<c:otherwise>
		<table>
			<tr>
				<tr>
				<td><img alt="이미지" src="${ImageDTO.image_url}" onError="javascript:this.src='http://www.findcopyright.or.kr:8080/images/noimg/noimg.jpg'"></td>
				<td>
					<table>
						<tr>
							<td colspan="2"><div>제목 : ${ImageDTO.works_title}</div></td>
						</tr>
						<tr>
							<td>이미지 코드 :</td><td>${ImageDTO.works_id}</td>
						</tr>
						<tr>
							<td>키워드 :</td><td>${ImageDTO.keyword}</td>
						</tr>
						<tr>
							<td>제작자 :</td><td>${ImageDTO.producer}</td>
						</tr>
						<tr>
							<td>장르 :</td><td>${ImageDTO.genre_cd}</td>
						</tr>
						<tr>
							<td>국가 :</td><td>${ImageDTO.nation_cd}</td>
						</tr>
						<tr>
							<td>창작연도 :</td><td>${ImageDTO.crt_year}</td>
						</tr>
						<tr>
							<td>이미지 설명 :</td><td>${ImageDTO.image_desc}</td>
						</tr>
					</table>
				</td>
				<tr>
			</tr>
		</table>
	</c:otherwise>
</c:choose>
</body>
</html>