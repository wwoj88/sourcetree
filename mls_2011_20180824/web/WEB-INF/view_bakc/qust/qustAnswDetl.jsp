<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%
	String bordSeqn = request.getParameter("bordSeqn");
	String menuSeqn = request.getParameter("menuSeqn");
	String threaded = request.getParameter("threaded");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
	String deth = request.getParameter("deth") == null ? "" : request.getParameter("deth");
	Board boardDTO = (Board) request.getAttribute("board");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>공지사항(<%=boardDTO.getTite()%>) | 알림마당 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/JavaScript">
<!--
window.name = "boardView";
  
function fn_update(){
	  var frm =document.form1;
		if (frm.pswd.value == "") {
			alert("비밀번호를 입력하십시오.");
			frm.pswd.focus();
			return false;
		} else if(isPwdValidate()){
			frm.method = "post";
			frm.submitType.value = "update";
			frm.action = "/board/board.do?method=boardView";
			frm.submit();
		}
	}

function fn_delete(){
	
	  var frm =document.form1;

		if (frm.pswd.value == "") {
			alert("비밀번호를 입력하십시오.");
			frm.pswd.focus();
			return;
		} else if(isPwdValidate()){
			if(confirm("답변내용도 같이 삭제됩니다\n내용을 삭제 하시겠습니까?")){
			  frm.method = "post";
			  frm.action = "/board/board.do?method=deleteQust";
			  frm.submit();
			}
		}
	}

	// 비밀번호 체크
	function isPwdValidate(){
		var bordSeqn = document.form1.bordSeqn.value;
		var menuSeqn = document.form1.menuSeqn.value;
		var pswd     = document.form1.pswd.value;
		var threaded = document.form1.threaded.value;
		var url = "/board/board.do?method=isValidPwd&bordSeqn=" + bordSeqn + "&menuSeqn=" + menuSeqn + "&pswd=" + pswd + "&threaded=" + threaded;
	  if(cfGetBooleanResponseReload(url)){
		    return true;
		} else {
			alert("비밀번호 오류입니다.");
			document.form1.pswd.value = "";
			document.form1.pswd.focus();
		  return false;
		}
}

	/*
	*  Function.js에 있으나 파이어폭스에서는 인식이 안되서 가져왔음
	*/
	function cfGetBooleanResponseReload(url,params,HttpMethod) {
		var xmlhttp = null;
		if(!HttpMethod){
		    HttpMethod = "GET";
		}
	    if(window.ActiveXObject){
		      try {
		    	  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		      } catch (e) {
		        try {
		        	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP") ;
		        } catch (e2) {
		          return null ;
		        }
			}
		}
		//xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		if (xmlhttp == null) return true;
		xmlhttp.open(HttpMethod, url, false);
		xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
	
		xmlhttp.send(params);
		
		if (xmlhttp.responseText == "true") {
			return true;
		} else {
			return false;
		}
	}
	
	//숫자만 입력
	function only_arabic(t){
		var key = (window.netscape) ? t.which :  event.keyCode;
	
		if (key < 45 || key > 57) {
			if(window.netscape){  // 파이어폭스
				t.preventDefault();
			}else{
				event.returnValue = false;
			}
		} else {
			//alert('숫자만 입력 가능합니다.');
			if(window.netscape){   // 파이어폭스
				return true;
			}else{
				event.returnValue = true;
			}
		}	
	}	  
//-->
</script>
</head>

<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader6.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(6);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			
			<!-- 래프 -->
			<div class="con_lf">
			<div class="con_lf_big_title">알림마당</div>
			<ul class="sub_lf_menu">
				<li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=2&page_no=1" class="on">묻고답하기</a></li>
				<li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=4&page_no=1" class="on">공지사항</a></li>
				<li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=1&page_no=1">자주묻는 질문</a></li>
				<li><a href="/eventMgnt/eventList.do">저작권찾기 이벤트</a></li>
			</ul>
			</div>
			<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						알림마당
						&gt;
						<span class="bold">공지사항</span>
					</div>
					
					<div class="con_rt_hd_title">묻고답하기</div>
					
					<div class="section">
						<form name="form1" method="post" action = "#">
									<input type="hidden" name="bordSeqn" value="<%=bordSeqn%>">
									<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>">
									<input type="hidden" name="threaded" value="<%=threaded%>">
									<input type="hidden" name="srchDivs" value="<%=srchDivs%>">
									<input type="hidden" name="srchText" value="<%=srchText%>">
								 	<input type="hidden" name="deth" value="<%=boardDTO.getDeth()%>">
								 	<input type="hidden" name="submitType">
									<input type="hidden" name="page_no" value="<%=page_no%>">
									<input type="hidden" name="filePath">
									<input type="hidden" name="fileName">
									<input type="hidden" name="realFileName">
									<input type="submit" style="display:none;">
						<!-- 테이블 view Set -->
						<div class="article">
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">제목</th>
										<td><%=boardDTO.getTite()%></td>
									</tr>
									<tr>
										<th scope="row">작성자</th>
										<td><%=boardDTO.getRgstName()%></td>
									</tr>
									<tr>
										<th scope="row">이메일</th>
										<td><%=boardDTO.getMail() == null ? "" : boardDTO.getMail()%></td>
									</tr>
									<tr>
										<th scope="row"><label for="pswd">비밀번호</label></th>
										<td><input type="password" id="pswd" name="pswd" size="15" onkeypress="javascript:only_arabic(event);"></td>
									</tr>
									<tr>
										<th scope="row">내용</th>
										<td><!-- 
											<p class="overflow_y h300">
											-->
											<%=CommonUtil.replaceBr(boardDTO.getBordDesc(),true)%>
											<!-- 
											</p>
											 --></td>
									</tr>
									<%
									          	List fileList = (List) boardDTO.getFileList();
									          	int listSize = fileList.size();
									
									          	if (listSize > 0) {
									%>
									<tr>
										<th scope="row" class="bgbr">첨부파일</th>
										<td>
									<%

									          	  for(int i=0; i<listSize; i++) {
									          	    Board fileDTO = (Board) fileList.get(i);
									%>	
										<a href="#1" onclick="javascript:fn_fileDownLoad('<%=fileDTO.getFilePath()%>','<%=fileDTO.getFileName()%>','<%=fileDTO.getRealFileName()%>')" class="orange underline" ><%=fileDTO.getFileName()%></a><br>
									<%
										            }
									%>	
										</td>
									</tr>
									<%
									          }
									%>
									<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_update();">수정하기</a></span> <span class="button medium"><a href="#1" onclick="javascript:fn_delete();">삭제하기</a></span></p>
								</tbody>
							</table>
							
							<!-- //그리드스타일 -->
							
							<div class="btnArea">
								<p class="lft"><span class="button medium gray"><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=2&page_no=1">목록</a></span></p>
							</div>
							
						</div>
						</form>
						<!-- //테이블 view Set -->
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
			</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->


</body>
</html>
