<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="java.util.List"%>
<%
	String menuSeqn = request.getParameter("menuSeqn");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
	String submitChek = request.getParameter("submitChek") == null ? "" : request.getParameter("submitChek");
	String submitType = request.getParameter("submitType") == null ? "" : request.getParameter("submitType");
	
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	
	Board boardDTO = (Board) request.getAttribute("board");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>공지사항 | 알림마당 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/JavaScript">
<!--
	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }
	
	
//-->
</script>
</head>

<body>
		<!-- HEADER str-->
		<jsp:include page="/include/2012/subHeader6.jsp" />
		<script type="text/javascript">initNavigation(4);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			
			<!-- 래프 -->
			<div class="con_lf">
			<div class="con_lf_big_title">알림마당</div>
			<ul class="sub_lf_menu">
				<li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=2&page_no=1" class="on">묻고답하기</a></li>
				<li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=4&page_no=1" class="on">공지사항</a></li>
				<li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=1&page_no=1">자주묻는 질문</a></li>
				<li><a href="/eventMgnt/eventList.do">저작권찾기 이벤트</a></li>
			</ul>
			</div>
			<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						알림마당
						&gt;
						<span class="bold">공지사항</span>
					</div>
					
					<div class="con_rt_hd_title">묻고답하기</div>
					
					<div class="section">
					
<style>
.table > tbody > tr > th {
	text-align: center;
}
</style>

<script src="/console/plugins/multifile-master/jquery.MultiFile.js" type="text/javascript" ></script>

<script>
	//등록
	/* function fncSave() {
		rules = {
			TITE : "required"
		};

		messages = {
			TITE : "제목은 필수항목입니다"
		};

		if (!fncValidate(rules, messages)) {
			return false;
		}
		
		$("#mailList option").prop("selected", true);
		$("#MSG_STOR_CD").val("1"); // 발신함 1 - 대기, 2 - 발송 ??? 확인필요
		
		var f = document.writeForm;
		f.action = '<c:url value="/console/email/fdcrAdA4Insert1.page"/>';
		f.submit();
	}

	function fncGoList(){
		location.href = '<c:url value="/console/legal/fdcrAd09List1.page"/>';
	}
	
	//팝업
	function fncPopup() {
		var popUrl = '<c:url value="/console/email/fdcrAdA4Pop1.page"/>';
		var popOption = "width=1000, height=650, resizable=no, scrollbars=no, status=no;"; //팝업창 옵션(optoin)
		window.open(popUrl, "", popOption);
	}
	
	function fncSelectEmail(data) {
		var _datas = data.split("/");
		if(_datas[0] == 'group'){
			$("#mailList").append("<option value='"+data+"'>@ "+_datas[6]+"</option>");	
		} else {
			$("#mailList").append("<option value='"+data+"'>"+_datas[2]+" "+_datas[5]+"</option>");
		}
			
	} */
	
</script>

<form name="form1" id="form1" method="post"
	enctype="multipart/form-data">
	<%-- <input type="hidden" id="MENU_SEQN" name="MENU_SEQN" value="<c:out value="${param.MENU_SEQN}"/>" /> 
	<input type="hidden" id="pageIndex" name="pageIndex" value="<c:out value="${param.pageIndex}"/>" />
	<input type="hidden" id="MSG_STOR_CD" name="MSG_STOR_CD" value="" /> --%>
	<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>" />
	<input type="hidden" name="srchDivs" value="<%=srchDivs%>" />
	<input type="hidden" name="srchText" value="<%=srchText%>" />
	<input type="hidden" name="page_no" value="<%=page_no%>" />
	<input type="hidden" name="bordSeqn" value="<%=boardDTO!=null?boardDTO.getBordSeqn():null%>">
	<input type="hidden" name="threaded" value="<%=boardDTO!=null?boardDTO.getThreaded():null%>">
	<input type="hidden" name="rgstIdnt" value="<%=user!=null?user.getUserIdnt():null%>" />
	<input type="hidden" name="mime_contents">
	<input type="submit" style="display:none;">
	<div class="box" style="margin-top: 30px;">
		<!-- /.box-header -->
		<div class="box-body">
			<table class="table table-bordered">
				<tbody>
					
					<tr>
						<th style="width: 20%">제목</th>
						<td><input id="regi1" class="form-control" name="tite" value="<%=boardDTO!=null?boardDTO.getTite():""%>"  placeholder="제목"  style="width: 100%; margin-bottom: 5px;"></td>
					</tr>
					<tr>
						<th style="width: 20%">이름</th>
						<td><input id="rgstName" class="form-control" name="rgstName" value="<%=boardDTO!=null?boardDTO.getRgstIdnt():""%>" placeholder="이름" style="width: 100% ; margin-bottom: 5px;"></td>
					</tr>
						<tr>
						<th style="width: 20%">이메일</th>
						<td><input id="regi3" class="form-control" name="mail" value="<%=boardDTO!=null?boardDTO.getMail():""%>" placeholder="이메일" style="width: 100%; margin-bottom: 5px;"></td>
					</tr>
						<tr>
						<th style="width: 20%">비밀번호</th>
						<td><input type="password" autocomplete="off" id="pswd" class="form-control" name="pswd" placeholder="비밀번호" style="width: 100%; margin-bottom: 5px;"></td>
					</tr>
					<tr>
						<th style="width: 20%">내용</th>
						<td>
							<textarea id="composeTextarea" class="form-control" name="bordDesc" style="width : 100%;height: 500px"></textarea>
						</td>
					</tr>
					<tr>
										<th scope="row"><label for="inputFile">첨부파일</label></th>
										<td>
											<div id="attach">
												<table>
												  <tr>
<%-- 												  	<c:if test = "${boardDTO!=''}">
												  		<%=boardDTO!=null?boardDTO.getFileList():""%>">
												  	</c:if>
 --%>												   	<td><input type="file" name="fileList" size="80" class="inputFile" id="inputFile"></td>
												  </tr>
												</table>
											</div>
										   	<!--<input type="text" class="w50" id="regi5" name="attachfile0" /> <span class="button small black"><a href="">찾아보기</a></span>-->
										   	<p class="mt5">
												<select name="atchCnt" id="atchCnt" title="첨부파일수" onChange="javascript:applyAtch();" style="width: 100px;">
													<c:forEach begin="1" end="10" step="1" varStatus="cnt">
														<option value="<c:out value="${cnt.count}"/>"><c:out value="${cnt.count}"/></option>
													</c:forEach>
												</select>
											</p>
										</td>
									</tr>
				</tbody>
			</table>
		</div>
		<input id="bordDesc" name="bordDesc" type="hidden">
		<div class="box-footer" style="text-align: right">
			<!-- <div class="btn-group"><button type="button" class="btn btn-primary" onclick="javascript:fn_qnaRegi();">저장하기</button></div> -->
			<div class="btn-group"><button id="insertBtn" type="button" class="btn btn-primary" >저장하기</button></div>
			<div class="btn-group"><button id="updateBtn" type="button" class="btn btn-primary" >수정하기</button></div>
		</div>
	</div>
</form>

<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
	var submitType = '<%=submitType%>';
	
	$(function () {
		//alert("test");
		CKEDITOR.replace('composeTextarea');  
		var bordDesc =" <%=boardDTO!=null?boardDTO.getBordDesc().replace(System.getProperty("line.separator"), ""):""%> ";
		
		CKEDITOR.instances.composeTextarea.setData(bordDesc);
		//Add text editor
		initBtn();
		$('#insertBtn').click(fn_qnaRegi);
		$('#updateBtn').click(fn_qnaUpdate);
		
		
	});
	
	function initBtn(){
		if(submitType=="update")
		{
			$('#insertBtn').css({display:"none"});
		}else{
			$('#updateBtn').css({display:"none"});
		}
	}
	function showAttach(cnt) {
		var i=0;
		var content = "";
		var attach = document.getElementById("attach")
		attach.innerHTML = "";
		//document.all("attach").innerHTML = "";
		content += '<table>';
		for (i=0; i<cnt; i++) {
			content += '<tr>';
			/* content += '<td><input type="file" name="attachfile'+(i+1)+'" size="80;" class="inputFile"><\/td>'; */
			content += '<td><input type="file" name="fileList['+i+']" size="80;" class="inputFile"><\/td>';
			
			content += '<\/tr>';
		}
		content += '<\/table>';
		//document.all("attach").innerHTML = content;
		attach.innerHTML = content;
	}

	function applyAtch() {
	    var ansCnt = document.form1.atchCnt.value;
	    showAttach(parseInt(ansCnt));
	}
	
	function fn_qnaRegi(event) {
	    var frm = document.form1;
		//alert(frm)
	    //alert(CKEDITOR.instances.composeTextarea.getData());
		/********************************************************/
		 // 태그프리 부분
		//var str = document.twe.MimeValue();
		//alert(str);
		//frm.bordDesc.value = frm.twe.TextValue;		// 테그제외
		//frm.mime_contents.value=str;
		/********************************************************/
		
		$('#bordDesc').val(CKEDITOR.instances.composeTextarea.getData());
		
	    if (frm.tite.value == "") {
			  alert("제목을 입력하십시오.");
			  frm.tite.focus();
			  return;
		} else if (frm.rgstIdnt.value == "") {
			  alert("이름을 입력하십시오.");
			  frm.rgstIdnt.focus();
			  return;
	    } else if (frm.mail.value == "") {
			  alert("이메일을 입력하십시오.");
			  frm.mail.focus();
			  return;
	    } else if (frm.pswd.value == "") {
			  alert("비밀번호를 입력하십시오.");
			  frm.pswd.focus();
			  return;
	    }  else if ($('#bordDesc').val()=="") {
			  alert("내용을 입력하십시오.");
			  
			  //frm.bordDesc.focus();
			  return;
	  	} else {
		  	frm.action = "/board/board.do?method=insertQust";
	  	  frm.submit();
		} 
	  }
	
	function fn_qnaUpdate(event) {
	    var frm = document.form1;
		//alert(frm)
	    //alert(CKEDITOR.instances.composeTextarea.getData());
		/********************************************************/
		 // 태그프리 부분
		//var str = document.twe.MimeValue();
		//alert(str);
		//frm.bordDesc.value = frm.twe.TextValue;		// 테그제외
		//frm.mime_contents.value=str;
		/********************************************************/
		
		$('#bordDesc').val(CKEDITOR.instances.composeTextarea.getData());
		
	    if (frm.tite.value == "") {
			  alert("제목을 입력하십시오.");
			  frm.tite.focus();
			  return;
		} else if (frm.rgstIdnt.value == "") {
			  alert("이름을 입력하십시오.");
			  frm.rgstIdnt.focus();
			  return;
	    } else if (frm.mail.value == "") {
			  alert("이메일을 입력하십시오.");
			  frm.mail.focus();
			  return;
	    } else if (frm.pswd.value == "") {
			  alert("비밀번호를 입력하십시오.");
			  frm.pswd.focus();
			  return;
	    }  else if ($('#bordDesc').val()=="") {
			  alert("내용을 입력하십시오.");
			  
			  //frm.bordDesc.focus();
			  return;
	  	} else {
		  	frm.action = "/board/board.do?method=updateQust";
	  	  frm.submit();
		} 
	  }
</script>

							<!-- //그리드스타일 -->
							
							<div class="btnArea">
								<p class="lft"><span class="button medium gray"><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=2&page_no=1">목록</a></span></p>
							</div>
							
						</div>
						</form>
						<!-- //테이블 view Set -->
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
			</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->


</body>
</html>
