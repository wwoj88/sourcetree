<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="com.tagfree.util.*"%>
<%@ page import="java.util.List"%>
<%
	String bordSeqn = request.getParameter("bordSeqn");
	String menuSeqn = request.getParameter("menuSeqn");
	String threaded = request.getParameter("threaded");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");

	Board boardDTO = (Board) request.getAttribute("board");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>묻고답하기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>

<script type="text/JavaScript">
<!--
  function fn_qustList(){
		var frm = document.form2;
		frm.action = "/board/board.do";
		frm.submit();
	}

  function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form2;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }

  function fn_update(){
	
	  var frm =document.form1;

		/********************************************************/
		 // 태그프리 부분
		var str = document.twe.MimeValue();
		frm.bordDesc.value = frm.twe.TextValue;		// 테그제외
		frm.mime_contents.value=str;
		/********************************************************/
	
		if (frm.tite.value == "") {
		  alert("제목을 입력하십시오.");
		  frm.tite.focus();
		  return;
	  } else if (frm.mail.value == "") {
		  alert("이메일을 입력하십시오.");
		  frm.mail.focus();
		  return;
    } else if (frm.bordDesc.value == "") {
		  alert("내용을 입력하십시오.");
		 // frm.bordDesc.focus();
		  return;
  	} else {
  		if(confirm("내용을 수정 하시겠습니까?")){
	  	  frm.action = "/board/board.do?method=updateQust";
  	    frm.submit();
  	  }
	  }
	}

	function showAttach(cnt) {
		var i=0;
		var content = "";
		var attach = document.getElementById("attach")
		attach.innerHTML = "";
		//document.all("attach").innerHTML = "";
		content += '<table>';
		for (i=0; i<cnt; i++) {
			content += '<tr>';
			content += '<td><input type="file" name="attachfile'+(i+1)+'" size="80;" class="inputFile"><\/td>';
			content += '<\/tr>';
		}
		content += '<\/table>';
		//document.all("attach").innerHTML = content;
		attach.innerHTML = content;
	}

	function applyAtch() {
    var ansCnt = document.form1.atchCnt.value;
    showAttach(parseInt(ansCnt));
	}

	// 삭제
	function editTable(type){

		var chkId = "chk";
		
		if( type == 'D' ) {

		    var oChkDel = document.getElementsByName(chkId);
		    var iChkCnt = oChkDel.length;
		    var iDelCnt = 0;


		    if(iChkCnt == 1 && oChkDel[0].checked == true){
		        iDelCnt++;
		    }else if(iChkCnt > 1){
	    	    for(i = iChkCnt-1; i >= 0; i--){
	    	        if(oChkDel[i].checked == true){    
	    	            iDelCnt++;
	    	        }
	    	    }
	    	}

	    	if(iDelCnt < 1){
	    	    alert('삭제할 첨부파일을 선택하여 주십시요');
	    	}else{
				result = confirm('삭제 하시겠습니까');
			    if(result == false){

				    if(iChkCnt == 1 && oChkDel[0].checked == true){
					    oChkDel[0].checked = false;
				    }else if(iChkCnt > 1){
			    	    for(i = iChkCnt-1; i >= 0; i--){
			    	        if(oChkDel[i].checked == true){    
			    	        	oChkDel[i].checked = false;
			    	        }
			    	    }
			    	}
			    	return;
			    }	
	    	}		
		    
		    if(iChkCnt == 1 && oChkDel[0].checked == true){
		    	document.getElementById("chk2_0").style.display = "none";
		        iDelCnt++;
		    }else if(iChkCnt > 1){
	    	    for(i = iChkCnt-1; i >= 0; i--){
	    	        if(oChkDel[i].checked == true){    
	    	        	document.getElementById("chk2_"+i).style.display = "none";
	    	            iDelCnt++;
	    	        }
	    	    }
	    	}
		}
	}
	
function OnInit()
{
	var form = document.form1;
	form.twe.InitDocument();
}
//-->
</script>

<!-- 태그프리 -->
<script language="JScript" FOR="twe" EVENT="OnControlInit()">
	var form = document.form1;
	form.twe.HtmlValue = form.bordDescTag.value;
</script>
<!-- 태그프리 -->

</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(4);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_4.gif" alt="알림마당" title="알림마당" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1"><img src="/images/2011/content/sub_lnb0401_off.gif" title="공지사항" alt="공지사항" /></a></li>
					<li id="lnb2"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1"><img src="/images/2011/content/sub_lnb0402_off.gif" title="자주묻는 질문" alt="자주묻는 질문" /></a></li>
					<li id="lnb3"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=2&amp;page_no=1"><img src="/images/2011/content/sub_lnb0403_off.gif" title="묻고답하기" alt="묻고답하기" /></a></li>
					<li id="lnb4"><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=2&amp;menuSeqn=5&amp;page_no=1"><img src="/images/2011/content/sub_lnb0404_off.gif" title="홍보자료" alt="홍보자료" /></a></li>
					<!--<li id="lnb5"><a href="/board/board.do?method=vocList"><img src="/images/2011/content/sub_lnb0407_off.gif" title="고객의 소리(VOC)" alt="고객의 소리(VOC)" /></a></li>
					--><li id="lnb6"><a href="/main/main.do?method=bannerList"><img src="/images/2011/content/sub_lnb0405_off.gif" title="홍보관" alt="홍보관" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb3");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>알림마당</span><em>묻고답하기</em></p>
					<h1><img src="/images/2011/title/content_h1_0403.gif" alt="묻고답하기" title="묻고답하기" /></h1>
					
					<div class="section">
						<form name="form1" action="#"  method="post" enctype="multipart/form-data">
								<input type="hidden" name="bordSeqn" value="<%=bordSeqn%>">
								<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>">
								<input type="hidden" name="threaded" value="<%=threaded%>">
								<input type="hidden" name="srchDivs" value="<%=srchDivs%>">
								<input type="hidden" name="srchText" value="<%=srchText%>">
								<input type="hidden" name="page_no" value="<%=page_no%>">
								<input type="hidden" name="rgstIdnt" value="<%=boardDTO.getRgstIdnt()%>">
								<input type="hidden" name="filePath">
								<input type="hidden" name="fileName">
								<input type="hidden" name="realFileName">
								<input type="hidden" name="bordDescTag" value="<%=(MimeUtil.replace(MimeUtil.replace(boardDTO.getBordDescTag(), "&", "&amp;"), "\"", "&#34;"))%>">
								<input type="hidden" name="bordDesc">
								<input type="hidden" name="mime_contents">
								<input type="submit" style="display:none;">
						<!-- 테이블 view Set -->
						<div class="article">
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="질문을 수정합니다">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="tite" class="necessary">제목</label></th>
										<td><input type="text" name="tite" id="tite" class="w90" value="<%=boardDTO.getTite()%>" /></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary">작성자</label></th>
										<td><%=boardDTO.getRgstName()%></td>
									</tr>
									<tr>
										<th scope="row"><label for="mail" class="necessary">이메일</label></th>
										<td><input type="text" name="mail" id="mail" class="w90" value="<%=boardDTO.getMail()%>" /></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="pswd">비밀번호</label></th>
										<td><input type="password" id="pswd" name="pswd" size="15" value="<%=boardDTO.getPswd()%>" /></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="bordDesc">내용</label></th>
										<td height="350">
											<!-- Active Designer를 실제로 추가하는 부분입니다. 반드시 API 문서를 읽어본 후, 적절히 설정하시기 바랍니다. -->
											<script language="jscript" src="/editor/tweditor.js"></script>
											<!-- Active Designer 추가 끝 -->
										</td>
									</tr>
									<%
									          	List fileList = (List) boardDTO.getFileList();
									          	int listSize = fileList.size();
									%>
									<tr>
										<th scope="row"><label for="inputFile">첨부파일</label></th>
										<td>
											<span></span>
											<div id="attach">
												 <table>
												  	<tr>
												   		<td>
													   		<input type="file" name="attachfile0" size="80" class="inputFile" id="inputFile">
														</td>
													</tr>
												</table>   	
												</div>
											<p class="mt5">
												<select name="atchCnt" title="첨부파일수" id="atchCnt" onChange="javascript:applyAtch();">
												<c:forEach begin="1" end="10" step="1" varStatus="cnt">
													<option value="<c:out value="${cnt.count}"/>"><c:out value="${cnt.count}"/></option>
												</c:forEach>
												</select>
												<span class="button small icon" title="[첨부파일]을 삭제합니다.">
													<a href="#1" onclick="javascript:editTable('D');">삭제</a>
												<span class="delete"></span>
												</span>
												<% if (listSize > 0) { %>
													<br><br>
													<ul>
												    	<li>※ 삭제를 원하는 파일은 체크하십시요.(파일은 '저장하기' 버튼을 클릭할 경우 완전히 삭제 됩니다.)</li>
												  	</ul>
												<% } %>
												<%
												  for(int i=0; i<listSize; i++) {
													Board fileDTO = (Board) fileList.get(i);
												%>
												    <div id="chk2_<%=i%>"><input type="checkbox" name="chk" title="삭제파일 선택" id="chk_<%=i%>" value="<%=fileDTO.getAttcSeqn()%>,<%=fileDTO.getFileName()%>"><a href="#1" onclick="javascript:fn_fileDownLoad('<%=fileDTO.getFilePath()%>','<%=fileDTO.getFileName()%>','<%=fileDTO.getRealFileName()%>')"><%=fileDTO.getFileName()%></a></div>
												<%
													}
												%>
										</td>
									</tr>
								</tbody>
							</table>
							
							<!-- //그리드스타일 -->
							
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_qustList();">목록</a></span></p>
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_update();">저장하기</a></span></p>
							</div>
							
						</div>
						<!-- //테이블 view Set -->
						</form>
						<form name="form2" action="#"  method="post">
							<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>">
							<input type="hidden" name="srchDivs" value="<%=srchDivs%>">
							<input type="hidden" name="srchText" value="<%=srchText%>">
							<input type="hidden" name="page_no" value="<%=page_no%>">
							<input type="hidden" name="filePath">
							<input type="hidden" name="fileName">
							<input type="hidden" name="realFileName">
							<input type="submit" style="display:none;">
						</form>					
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
