<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>참여마당 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(5);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_5.gif" alt="참여마당" title="참여마당" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/worksPriv/worksPriv.do?menuSeqn=1&amp;page_no=1"><img src="/images/2011/content/sub_lnb0501_off.gif" title="개인저작물" alt="개인저작물" /></a></li>
					<li id="lnb2"><a href="/board/board.do?menuSeqn=7&amp;page_no=1"><img src="/images/2011/content/sub_lnb0502_off.gif" title="이벤트" alt="이벤트" /></a></li>
					<li id="lnb3"><a href="/main/main.do?method=goSiteList"><img src="/images/2011/content/sub_lnb0503_off.gif" title="관련사이트안내" alt="관련사이트안내" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb3");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>참여마당</span><em>관련사이트 안내</em></p>
					<h1><img src="/images/2011/title/content_h1_0503.gif" alt="관련사이트 안내" title="관련사이트 안내" /></h1>
					
					<div class="section">
                       
                  	<div class="fl w50 floatDiv info03Bg">
						<p class="fl w35 m20 pt45"><img src="/images/2011/content/siteList_subimg01.gif" alt="사단법인 한국음악저작권협회 " title="사단법인 한국음악저작권협회" /></p>
						<table cellspacing="0" cellpadding="0" border="0" summary="사단법인 한국음악저작권협회">
							<tbody>
								<tr><th align="left" class="pt25">[사단법인]한국음악저작권협회</th></tr>
								<tr><td class="pt10 pb30">서울시 강서구 우장산동 649번지<br>한국음악저작권협회(KOMCA)회관<br>TEL : 02-2660-0400<br>FAX : 02-2660-0401<br>URL : www.komca.or.kr</td></tr>
							</tbody>
						</table>
					</div>
                    <div class="fr w50 floatDiv info03Bg">
						<p class="fl w35 m20 pt45"><img src="/images/2011/content/siteList_subimg02.gif" alt="사단법인 한국복사전송권협회 " title="사단법인 한국복사전송권협회" /></p>
						<table cellspacing="0" cellpadding="0" border="0" summary="사단법인 한국복사전송권협회">
							<tbody>
								<tr><th align="left" class="pt25">[사단법인]한국복사전송권협회</th></tr>
								<tr><td class="pt10 pb30">서울시 강서구 화곡동 71-4<br>대한빌딩 4층<br>TEL : 02-2608-2036~7<br>FAX : 02-2608-2031<br>URL : www.copycle.or.kr</td></tr>
							</tbody>
						</table>
					</div>
                    <div class="fl w50 floatDiv info03Bg">
						<p class="fl w35 m20 pt45"><img src="/images/2011/content/siteList_subimg03.gif" alt="한국문화예술저작권협회 " title="한국문화예술저작권협회" /></p>
						<table cellspacing="0" cellpadding="0" border="0" summary="한국문화예술저작권협회">
							<tbody>
								<tr><th align="left" class="pt25">한국문화예술저작권협회</th></tr>
								<tr><td class="pt10 pb30">서울시 강남구 역삼동 828-10<br>올림피아센터 1020호<br>TEL  : 02-508-0440<br>FAX : 02-539-3993<br>URL : www.copyrightkorea.or.kr</td></tr>
							</tbody>
						</table>
					</div>
                    <div class="fr w50 floatDiv info03Bg">
						<p class="fl w35 m20 pt45"><img src="/images/2011/content/siteList_subimg04.gif" alt="한국음반산업협회 " title="한국음반산업협회" /></p>
						<table cellspacing="0" cellpadding="0" border="0" summary="한국음반산업협회">
							<tbody>
								<tr><th align="left" class="pt25">한국음반산업협회</th></tr>
								<tr><td class="pt10 pb30">서울시 마포구 신공덕동 5-28<br>서원BD 3층<br>TEL  : 02-3270-5900<br>FAX : 02-711-9735<br>URL : www..kapp.or.kr</td></tr>
							</tbody>
						</table>
					</div>
                    <div class="fl w50 floatDiv info03Bg">
						<p class="fl w35 m20 pt45"><img src="/images/2011/content/siteList_subimg05.gif" alt="사단법인 한국방송작가협회 " title="사단법인 한국방송작가협회" /></p>
						<table cellspacing="0" cellpadding="0" border="0" summary="사단법인 한국방송작가협회">
							<tbody>
								<tr><th align="left" class="pt25">[사단법인]한국방송작가협회</th></tr>
								<tr><td class="pt10 pb30">서울시 영등포구 여의도동 17-1<br>금산빌딩 4층<br>TEL  : 02-782-1696<br>FAX : 02-783-3711<br>URL : www.ktrwa.or.kr/</td></tr>
							</tbody>
						</table>
					</div>
                    <div class="fr w50 floatDiv info03Bg">
						<p class="fl w35 m20 pt45"><img src="/images/2011/content/siteList_subimg06.gif" alt="사단법인 한국영화배급협회 " title="사단법인 한국영화배급협회" /></p>
						<table cellspacing="0" cellpadding="0" border="0" summary="사단법인 한국영화배급협회">
							<tbody>
								<tr><th align="left" class="pt25">[사단법인]한국영화배급협회</th></tr>
								<tr><td class="pt10 pb30">서울시 중구 신당동 333-63번지<br>미진빌딩 2층<br>TEL  : 02-3452-1001<br>FAX : 02-3452-1005<br>URL : www.mdak.or.kr/</td></tr>
							</tbody>
						</table>
					</div>
 
                    </div>
				</div>
				<!-- //주요컨텐츠 end -->

				
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>