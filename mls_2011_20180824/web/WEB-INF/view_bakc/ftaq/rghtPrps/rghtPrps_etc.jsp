<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권정보 조회(기타) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<style type="text/css">
<!--
body {
	overflow-x: auto;
	margin: 0 0 0 0 ;
}

.div_scroll {initNavigation
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>

<script type="text/javascript"><!--


window.name = "rghtPrps_etc";

// 이미지저작물 상세 팝업오픈
function openImageDetail( workFileNm, workNm ) {

	var param = '';
	
	param = '&WORK_FILE_NAME='+workFileNm;
	param += '&WORK_NAME='+workNm;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=I'+param
	var name = '';
	var openInfo = 'target=rghtPrps_image, width=320, height=310';
	
	window.open(url, name, openInfo);
}
function openSmplDetail(div) {

	var param = '';
	
	param = 'DVI='+div;
	
	var url = '/common/rghtPrps_smpl.jsp?'+param
	var name = '';
	var openInfo = 'target=rghtPrps_etc, width=705, height=570, scrollbars=yes';
	
	window.open(url, name, openInfo);
}


// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	
	
	var the_height = document.getElementById(name).offsetHeight;

	//div 크기 제한 str--
	var selChkCnt	= 0;
	if( name == 'listTab') {
		var oSelTable = document.getElementById("listTab");
		var selChkObjs = oSelTable.getElementsByTagName("input");
		for(var sn=0; sn<selChkObjs.length; sn++) {
			if(selChkObjs[sn].name == 'iChkVal') {
				selChkCnt++;
			}
		}
	}

	if( name == 'listTab_2') {
		var oSelTable2 = document.getElementById("listTab_2");
		var selChkObjs2 = oSelTable2.getElementsByTagName("input");
		for(var sn=0; sn<selChkObjs2.length; sn++) {
			if(selChkObjs2[sn].name == 'iChkVal') {
				selChkCnt++;
			}
		}
	}

	if(selChkCnt<6){
		document.getElementById(targetName).style.height = the_height + "px" ;
	}
	//div 크기 제한 end--

}

// 스크롤셋팅
function scrollSet(name, targetName, oRowName){
	
	var totalRow = document.getElementsByName(oRowName).length;
		
	// 세로 기준건수 이상인 경우 세로스크롤 제어
	if( totalRow < 6) {
		resizeDiv(name, targetName);
		document.getElementById(targetName).style.overflowY = "hidden";
	}
	
	if( totalRow > 5 ) {
		document.getElementById(targetName).style.overflowY = "auto";
	}
}


// 토탈건수 set
function setTotCnt( oRowName, oSpanId) {

	var totalRow = document.getElementsByName(oRowName).length;
	document.getElementById(oSpanId).innerHTML = "("+totalRow+"건)";
}


var iRowIdx = 1;
var backIrowIdx = 1;
var test = 1;

// 테이블 행추가/삭제
function editTable(type){

	var qw = document.getElementById("IROWIDX").value;
	
	// 1. 신청 목적 선택 먼저 :  앞화면에서 넘어오는 관계로 생략된다.
	if ( document.getElementById("PRPS_RGHT_CODE").value == '')  {
		
		alert("신청 목적 선택 후 가능한 기능입니다.");
		
		document.getElementById("PRPS_RGHT_CODE").focus();
		
		return;
	}

	// 2. 신청저작물장르
	if ( document.getElementById("PRPS_SIDE_GENRE").value == '' )  {
		
		alert("신청저작물장르 선택 후 가능한 기능입니다.");
		
		document.getElementById("PRPS_SIDE_GENRE").focus();
		return;
	}

	// 3. 신청 신탁관리단체
	var trstOrgnLen = document.getElementsByName("TRST_ORGN_CODE").length;
	var kCnt = 0;
	
	for( kk=0; kk<trstOrgnLen && kCnt ==0; kk++) {
		
		if(document.getElementsByName("TRST_ORGN_CODE")[kk].checked){
			kCnt += 1;	break;
		}
	}
	
	if(kCnt==0) {
		alert("권리구분 선택 후 가능한 기능입니다.");
		
		var PRPS_SIDE_GENRE = document.getElementById("PRPS_SIDE_GENRE").value;

		if(PRPS_SIDE_GENRE == '6')
			document.getElementById("CHK_216_2").focus();
		else if(PRPS_SIDE_GENRE == '7')
			document.getElementById("CHK_215_2").focus();
		else
			document.getElementsByName("TRST_ORGN_CODE")[0].focus();
		return;
	}
	
	// 4. tableId setting
	var tableId = "listTab";	//권리자/대리인 권리찾기
	var chkId = "iChk";
	var disSeq = "displaySeq";
	var totalRow = "totalRow";
	var divId = "div_1";
	var scrllDivId = "div_scroll_1";
	var disId = "PRPS_SIDE_R_GENRE";

	if(document.getElementById("PRPS_RGHT_CODE").value == '3')	{
		 tableId = "listTab_2";		//이용허락
		 chkId = "iChk2";
		 disSeq = "displaySeq2";
		 totalRow = "totalRow2";
		 divId = "div_2";
		 scrllDivId = "div_scroll_2";
	}
	
	if( type == 'D' ) {
		
	    var oTbl = document.getElementById(tableId);
	    var oChkDel = document.getElementsByName(chkId);
	    var iChkCnt = oChkDel.length;
	    var iDelCnt = 0;

	    if(iChkCnt == 1 && oChkDel[0].checked == true){
	        oTbl.deleteRow(1);
	        iDelCnt++;
	    }else if(iChkCnt > 1){
    	    for(i = iChkCnt-1; i >= 0; i--){
    	        if(oChkDel[i].checked == true){    	            
    	            oTbl.deleteRow(i+1);
    	            iDelCnt++;
    	        }
    	    }
    	}
    	
    	if(iDelCnt < 1){
    	    alert('삭제할 저작물을 선택하여 주십시요');
    	}

    	fn_resetSeq(disSeq);
    	fn_resetId(disId);
    	//backIrowIdx = iRowIdx-iDelCnt-back_val;
    	var back_idx = document.getElementById("BACK_IROWIDX").value;

		if(back_idx == ''){
			iRowIdx = iRowIdx-iDelCnt-1;
		}else{
			if(iRowIdx > back_idx){
				iRowIdx = iRowIdx-iDelCnt;
			}else{
				iRowIdx = back_idx-iDelCnt;
			}		
			//iRowIdx = iRowIdx-iDelCnt;
		}		
    	document.getElementById("BACK_IROWIDX").value = iRowIdx;
    	document.getElementById("IROWIDX").value = iRowIdx;
    	test = 1;
	}else if( type == 'I' ) {
		
		newRow = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
		
		newRow.onmouseover=function(){document.getElementById(tableId).clickedRowIndex=this.rowIndex}
		newRow.valign = 'middle';
		newRow.align = 'center';
		
		var back_idx = document.getElementById("BACK_IROWIDX").value;

		if(back_idx == ''){
			iRowIdx = parseInt(iRowIdx);
			makeCell(tableId, newRow, document.getElementById(tableId).rows.length, iRowIdx);
			iRowIdx++;
		}else{
			iRowIdx = test + parseInt(back_idx);
			// alert("iRowIdx : "+iRowIdx);
			makeCell(tableId, newRow, document.getElementById(tableId).rows.length, iRowIdx);
			test++;
		}	
	
	}else if( type == 'A' ) {
		addCell(tableId);
	}
	
	// 스크롤에 의한 height reset
	//resizeDiv(tableId, divId);
	scrollSet(tableId, scrllDivId, chkId);
		
	// 토탈건수
	setTotCnt( chkId, totalRow);
}
	
    
//순번 재지정
function fn_resetSeq(disName){
    var oSeq = document.getElementsByName(disName);
    for(i=0; i<oSeq.length; i++){
        oSeq[i].value = i+1;
    }
}

//신청저작물 id순번 재지정
function fn_resetId(disId){
    var oSeq = document.getElementsByName(disId);
    for(i=0; i<oSeq.length; i++){
        var idx = i+1;
        var id = disId+idx;
    	oSeq[i].id = id;
    }
}

//선택저작물 테이블 idx
//var iRowIdx = 1;
var iRowIdx3 = 1;
var iRowIdx2 = 1;
       
function makeCell(tableId, cur_row, rowPos, iRowIdx) {

	var i=0;
	var classId = "inputDisible2"; 	var read = "readonly";
	
	// 신청목적에 따라 생성 td 가 다르다.
	var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
	
	//if( document.getElementById("CHK_200").checked)	{
	//	classId = "inputData";		read = "";
	//}
	
	// 권리자/대리인 권리찾기
	if( tableId == 'listTab' ) {
		
		var genreCode = document.getElementById("PRPS_SIDE_GENRE").value;
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk\"  id=\"iChk_'+iRowIdx+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce pd5';
		innerHTMLStr = '<input name=\"displaySeq\" id=\"displaySeq_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작물 정보
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"WORK_NAME\"  id=\"WORK_NAME_'+iRowIdx+'\" class="inputDataN w95" title=\"저작물명\" rangeSize="~100" nullCheck/>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
	//	innerHTMLStr = '<input name=\"IMAGE_DIVS\"  id=\"IMAGE_DIVS_'+iRowIdx+'\" class="inputDataN w90" title=\"분야\" rangeSize="~100" nullCheck />';
		if(genreCode == "1" ){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" class="w95" title=\"분야코드\" ><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">무용<\/option><option value=\"2\">발레<\/option><option value=\"3\">무언극<\/option><option value=\"4\">뮤지컬<\/option>';
			innerHTMLStr += '<option value=\"5\">오페라<\/option><option value=\"6\">마당극<\/option><option value=\"7\">즉흥극<\/option><option value=\"8\">창극<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "2"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" class="w95" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">건축물<\/option><option value=\"2\">건축설계서<\/option><option value=\"3\">건축물모형<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "3"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" class="w95" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">(통수목적)지도<\/option><option value=\"2\">도표<\/option><option value=\"3\">설계도(건촉설계도 제외)<\/option><option value=\"4\">모형<\/option>';
			innerHTMLStr += '<option value=\"5\">지구의<\/option><option value=\"6\">약도<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "4"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" class="w95" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">사무관리<\/option><option value=\"2\">과학기술<\/option><option value=\"3\">교육<\/option><option value=\"4\">오락<\/option>';
			innerHTMLStr += '<option value=\"5\">기업관리<\/option><option value=\"6\">콘텐츠개발SW<\/option><option value=\"7\">프로그램SW<\/option><option value=\"8\">산업용SW<\/option>';
			innerHTMLStr += '<option value=\"9\">서체글꼴SW<\/option><option value=\"10\">제어프로그램<\/option><option value=\"11\">언어처리<\/option><option value=\"12\">유틸리티<\/option>';
			innerHTMLStr += '<option value=\"13\">데이터통신<\/option><option value=\"14\">테이터베이스<\/option><option value=\"15\">보안SW<\/option><option value=\"16\">미들웨어<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "5"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" class="w95" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">사전<\/option><option value=\"2\">홈페이지<\/option><option value=\"3\">문학전집<\/option><option value=\"4\">시집<\/option>';
			innerHTMLStr += '<option value=\"5\">신문<\/option><option value=\"6\">잡지<\/option><option value=\"7\">악보집<\/option><option value=\"8\">논문집<\/option>';
			innerHTMLStr += '<option value=\"9\">백과사전<\/option><option value=\"10\">교육교재<\/option><option value=\"11\">카탈로그<\/option><option value=\"12\">단어집<\/option>';
			innerHTMLStr += '<option value=\"13\">문제집<\/option><option value=\"14\">설문지<\/option><option value=\"15\">인명부<\/option><option value=\"16\">전단<\/option><option value=\"17\">데이터베이스<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "6" || genreCode == "7"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" class="w95" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"99\">기타<\/option><\/select>';
		}				
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<select name=\"OPEN_MEDI_CODE\" id=\"OPEN_MEDI_CODE_'+iRowIdx+'\" class="w95" title=\"공표매체코드\"><option value=\"\">선택<\/option>';
		innerHTMLStr += '<option value=\"1\">출판사명<\/option><option value=\"2\">배포대상<\/option><option value=\"3\">인터넷주소<\/option><option value=\"4\">공연장소<\/option>';
		innerHTMLStr += '<option value=\"5\">방송사 및 프로그램명<\/option><option value=\"6\">기타<\/option><\/select>';
		innerHTMLStr += '<input name=\"OPEN_MEDI_TEXT\"  id=\"OPEN_MEDI_TEXT_'+iRowIdx+'\" class="inputDataN w95 mt5" title=\"공표매체\" rangeSize="~30" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		//innerHTMLStr = '<input name=\"OPEN_DATE\"  id=\"OPEN_DATE_'+iRowIdx+'\" class="inputDataN w90" title=\"공표일자\" character="KE"  maxlength="8" rangeSize="8" nullCheck />';
		innerHTMLStr = '<input class="inputDataN w65" type="text" id=\"OPEN_DATE_'+iRowIdx3+'\" name=\"OPEN_DATE_'+iRowIdx3+'\" size="8" maxlength="8" value=""/> &nbsp;';
		innerHTMLStr += '<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'OPEN_DATE_'+iRowIdx3+'\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'OPEN_DATE_'+iRowIdx3+'\');" alt="달력" title="공표일자를 선택하세요." align="middle" style="cursor:pointer;"/>'
		innerHTMLStr += '<input name=\"OPEN_DATE\" type="hidden" class="inputDataN w90" value="'+iRowIdx3+'"/>';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작권자
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		//innerHTMLStr = '<input name=\"COPT_HODR\" id=\"COPT_HODR_'+iRowIdx+'\" class="'+classId+' w90" '+read+' title="저작권자" rangeSize="~100" />';
		innerHTMLStr = '<input name=\"COPT_HODR\" id=\"COPT_HODR_'+iRowIdx+'\" class="inputDataN w95" title="저작권자" rangeSize="~100" />';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="COPT_HODR_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="CHK_COPT_HODR" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		fn_resetSeq("displaySeq");
		
		// setfocus
		//var oFuc = 'MUSIC_TITLE_'+iRowIdx;
		//	document.getElementById(oFuc).focus();
		
		//iRowIdx++;
		iRowIdx3++;
		document.getElementById("IROWIDX").value = iRowIdx;
	}
	
	// 이용자 권리조회
	else if( tableId == 'listTab_2' ) {
	
		var genreCode = document.getElementById("PRPS_SIDE_GENRE").value;
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk2\"  id=\"iChk2_'+iRowIdx+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce pd5';
		innerHTMLStr = '<input name=\"displaySeq2\" id=\"displaySeq2_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작물 정보
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"WORK_NAME\"  id=\"WORK_NAME_'+iRowIdx+'\" class="inputDataN w95" title=\"이미지명\" rangeSize="~100"  nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		//innerHTMLStr = '<input name=\"IMAGE_DIVS\"  id=\"IMAGE_DIVS_'+iRowIdx2+'\" class="inputDataN w90" title=\"분야\" rangeSize="~100" nullCheck />';
		if(genreCode == "1" ){
			innerHTMLStr = '<select class=\"w95\" name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\" ><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">무용<\/option><option value=\"2\">발레<\/option><option value=\"3\">무언극<\/option><option value=\"4\">뮤지컬<\/option>';
			innerHTMLStr += '<option value=\"5\">오페라<\/option><option value=\"6\">마당극<\/option><option value=\"7\">즉흥극<\/option><option value=\"8\">창극<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "2"){
			innerHTMLStr = '<select class=\"w95\" name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">건축물<\/option><option value=\"2\">건축설계서<\/option><option value=\"3\">건축물모형<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "3"){
			innerHTMLStr = '<select class=\"w95\" name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">(통수목적)지도<\/option><option value=\"2\">도표<\/option><option value=\"3\">설계도(건촉설계도 제외)<\/option><option value=\"4\">모형<\/option>';
			innerHTMLStr += '<option value=\"5\">지구의<\/option><option value=\"6\">약도<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "4"){
			innerHTMLStr = '<select class=\"w95\" name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">사무관리<\/option><option value=\"2\">과학기술<\/option><option value=\"3\">교육<\/option><option value=\"4\">오락<\/option>';
			innerHTMLStr += '<option value=\"5\">기업관리<\/option><option value=\"6\">콘텐츠개발SW<\/option><option value=\"7\">프로그램SW<\/option><option value=\"8\">산업용SW<\/option>';
			innerHTMLStr += '<option value=\"9\">서체글꼴SW<\/option><option value=\"10\">제어프로그램<\/option><option value=\"11\">언어처리<\/option><option value=\"12\">유틸리티<\/option>';
			innerHTMLStr += '<option value=\"13\">데이터통신<\/option><option value=\"14\">테이터베이스<\/option><option value=\"15\">보안SW<\/option><option value=\"16\">미들웨어<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "5"){
			innerHTMLStr = '<select class=\"w95\" name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">사전<\/option><option value=\"2\">홈페이지<\/option><option value=\"3\">문학전집<\/option><option value=\"4\">시집<\/option>';
			innerHTMLStr += '<option value=\"5\">신문<\/option><option value=\"6\">잡지<\/option><option value=\"7\">악보집<\/option><option value=\"8\">논문집<\/option>';
			innerHTMLStr += '<option value=\"9\">백과사전<\/option><option value=\"10\">교육교재<\/option><option value=\"11\">카탈로그<\/option><option value=\"12\">단어집<\/option>';
			innerHTMLStr += '<option value=\"13\">문제집<\/option><option value=\"14\">설문지<\/option><option value=\"15\">인명부<\/option><option value=\"16\">전단<\/option><option value=\"17\">데이터베이스<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "6" || genreCode == "7"){
			innerHTMLStr = '<select class=\"w95\" name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"99\">기타<\/option><\/select>';
		}				

		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<select class=\"w95\" name=\"OPEN_MEDI_CODE\" id=\"OPEN_MEDI_CODE_'+iRowIdx+'\" title=\"공표매체코드\"><option value=\"\">선택<\/option>';
		innerHTMLStr += '<option value=\"1\">출판사명<\/option><option value=\"2\">배포대상<\/option><option value=\"3\">인터넷주소<\/option><option value=\"4\">공연장소<\/option>';
		innerHTMLStr += '<option value=\"5\">방송사 및 프로그램명<\/option><option value=\"6\">기타<\/option><\/select>';
		innerHTMLStr += '<input name=\"OPEN_MEDI_TEXT\"  id=\"OPEN_MEDI_TEXT_'+iRowIdx+'\" class="inputDataN w95 mt5" title=\"공표매체\" rangeSize="~30" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		//innerHTMLStr = '<input name=\"OPEN_DATE\"  id=\"OPEN_DATE_'+iRowIdx+'\" class="inputDataN w90" title=\"공표일자\" character="KE"  maxlength="8" rangeSize="8" nullCheck />';
		innerHTMLStr = '<input class="inputDataN w65" type="text" id=\"VAL_OPEN_DATE_'+iRowIdx3+'\" name=\"VAL_OPEN_DATE_'+iRowIdx3+'\" size="8" maxlength="8" value=""/> &nbsp;';
		innerHTMLStr += '<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'VAL_OPEN_DATE_'+iRowIdx3+'\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'VAL_OPEN_DATE_'+iRowIdx3+'\');" alt="달력" title="공표일자를 선택하세요." align="middle" style="cursor:pointer;"/>'
		innerHTMLStr += '<input name=\"VAL_OPEN_DATE\" type="hidden" class="inputDataN w90" value="'+iRowIdx3+'"/>';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="COPT_HODR" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		fn_resetSeq("displaySeq2");
		
		// setfocus
		//	var oFuc = 'MUSIC_TITLE_'+iRowIdx2;
		//	document.getElementById(oFuc).focus();
		
		//iRowIdx2++;
		iRowIdx3++;
		document.getElementById("IROWIDX").value = iRowIdx;
		
	}
}

function addCell(tableId) {

	//iFrame 항목
	var chkObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("ifrmChk");
	var imageSeqnObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("imageSeqn");
	var crIdObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("crId");
	var workNameObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("workName");
	var imageDivsObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("imageDivs");	
	var coptHodrObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("coptHodr");			
	var icnNumbObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("icnNumb");
	var workFileNameObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("workFileName");
	
	// 선택 목록
	var oSelTable = document.getElementById("listTab");
	var oSelTable2 = document.getElementById("listTab_2");

	var selChkObjs = oSelTable.getElementsByTagName("input");
	var selChkObjs2 = oSelTable2.getElementsByTagName("input");

	var selChkCnt = 0;
	var selChkCnt2 = 0;
	
	var isExistYn = "N";	//중복여부	
	var isAdd	= false;	//줄 추가여부

	var count = 0;
	
	for(var cn = 0; cn < chkObjs.length; cn++) {
		if(chkObjs[cn].checked == true){

			if( tableId == 'listTab') {
				//중복여부 검사
				for(var sn=0; sn<selChkObjs.length; sn++) {
					if(chkObjs[cn].checked == true && selChkObjs[sn].name == 'iChkVal') {
						if(imageSeqnObjs[cn].value == selChkObjs[sn].value) {
							isExistYn = "Y";
						}
						selChkCnt++;
					}
				}
			}

			if( tableId == 'listTab_2') {
				//중복여부 검사
				for(var sn=0; sn<selChkObjs2.length; sn++) {
					if(chkObjs[cn].checked == true && selChkObjs2[sn].name == 'iChkVal') {
						if(imageSeqnObjs[cn].value == selChkObjs2[sn].value) {
							isExistYn = "Y";
						}
					}
					selChkCnt2++;
				}
			}
			
			if(isExistYn == "N") {
				//줄 추가여부
				isAdd = true;
				count ++;
			}else{
				alert("선택된 저작물 ["+workNameObjs[cn].value+"]는 이미 추가된 저작물입니다.");
				return;
			}
			

			if( tableId == 'listTab' && selChkCnt > 0) { //권리찾기 선택목록이 비웠을때 
				//줄 추가여부		
				isAdd = true;
				count ++;
			}

			if( tableId == 'listTab_2' && selChkCnt2 > 0) { //권리찾기 선택목록이 비웠을때 
				//줄 추가여부		
				isAdd = true;
				count ++;
			}
		}

		chkObjs[cn].checked = false;  // 처리한 후 체크풀기 


		//줄 추가실행
		if(isAdd){
			var cur_row = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
			
			cur_row.onmouseover=function(){document.getElementById(tableId).clickedRowIndex=this.rowIndex}
			cur_row.valign = 'middle';
			cur_row.align = 'center';
			
			var rowPos = document.getElementById(tableId).rows.length;
				
			var i=0;
			
			var classId = "inputDisible2"; 	var read = "readonly";
			
			// 신청목적에 따라 생성 td 가 다르다.
			var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
			
			// 권리자/대리인 권리찾기
			if( tableId == 'listTab' ) {
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '<input type=\"checkbox\" name=\"iChk\" id=\"iChk_'+iRowIdx+'\" class=\"vmid\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"displaySeq\" id=\"displaySeq_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly=\"readonly\" value=\"'+iRowIdx+'\"/>';
				cur_cell.innerHTML = innerHTMLStr;
				
				// 저작물 정보
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '<a href=\"javascript:openImageDetail(\''+workFileNameObjs[cn].value+'\',\''+workNameObjs[cn].value+'\');\"><u>'+workNameObjs[cn].value+'<\/u><\/a>';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = imageDivsObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '&nbsp;';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '&nbsp;';
				cur_cell.innerHTML = innerHTMLStr;
				
				// 저작권자
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '<input name=\"COPT_HODR\" id=\"COPT_HODR_'+iRowIdx+'\"  class=\"'+classId+' w90\" '+read+' value=\"'+coptHodrObjs[cn].value+'\" title=\"저작권자\" rangeSize=\"~100\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"iChkVal\" value=\"'+imageSeqnObjs[cn].value+'|0|0\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"WORK_NAME\"  value=\"'+workNameObjs[cn].value+'\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"IMAGE_DIVS\" value=\"'+imageDivsObjs[cn].value+'\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"OPEN_MEDI_CODE\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"OPEN_MEDI_TEXT\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"OPEN_DATE\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"COPT_HODR_ORGN\" value=\"'+coptHodrObjs[cn].value+'\"/>';
				innerHTMLStr += '<input type="hidden" name="CHK_COPT_HODR" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				fn_resetSeq("displaySeq");
				
				// setfocus
				//var oFuc = 'MUSIC_TITLE_'+iRowIdx;
				//	document.getElementById(oFuc).focus();
				
				iRowIdx++;
			}
			
			// 이용자 권리조회
			else if( tableId == 'listTab_2' ) {
			//alert(iRowIdx2);
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '<input type=\"checkbox\" name=\"iChk2\" id=\"iChk2_'+iRowIdx+'\" class=\"vmid\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"displaySeq2\" id=\"displaySeq2_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly=\"readonly\" value=\"'+iRowIdx+'\"/>';
				cur_cell.innerHTML = innerHTMLStr;
				
				// 저작물 정보
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '<a href=\"javascript:openImageDetail(\''+workFileNameObjs[cn].value+'\',\''+workNameObjs[cn].value+'\');\"><u>'+workNameObjs[cn].value+'<\/u><\/a>';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = imageDivsObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '&nbsp;';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '&nbsp;';
				innerHTMLStr += '<input type=\"hidden\" name=\"iChkVal\" value=\"'+imageSeqnObjs[cn].value+'|0|0\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"WORK_NAME\"  value=\"'+workNameObjs[cn].value+'\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"IMAGE_DIVS\" value=\"'+imageDivsObjs[cn].value+'\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"OPEN_MEDI_CODE\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"OPEN_MEDI_TEXT\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"OPEN_DATE\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"COPT_HODR\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				fn_resetSeq("displaySeq2");
				
				// setfocus
				//	var oFuc = 'MUSIC_TITLE_'+iRowIdx2;
				//	document.getElementById(oFuc).focus();
				
				iRowIdx++;
				
			}
			isAdd = false;
		}
	}

	if(count == 0){
		alert('선택된 저작물이 없습니다.');
		return;
	}	
}

// 신청목적 선택
function prps_check(chk) {

	// 권리자 권리찾기
	if(chk == '1' || chk == '2' ) {
		document.getElementById("div_1").style.display = "";
		document.getElementById("div_2").style.display = "none";

		fn_lock( "listTab", "" );
		fn_lock( "listTab_2", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab", "div_scroll_1", "iChk");
		
		// 토탈건수
		setTotCnt( "iChk", "totalRow");
		
	} else {
		document.getElementById("div_2").style.display = "";
		document.getElementById("div_1").style.display = "none";

		fn_lock( "listTab_2", "" );
		fn_lock( "listTab", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab_2", "div_scroll_2", "iChk2");
		
		// 토탈건수
		setTotCnt( "iChk2", "totalRow2");
		
	}
	
}

//신청목적 선택
function prps_check2() {

	// 권리자 권리찾기
	if(document.getElementById("PRPS_RGHT_CODE2").value == '1' ) {

		document.getElementById("div_1").style.display = "";
		document.getElementById("div_2").style.display = "none";

		fn_lock( "listTab", "" );
		fn_lock( "listTab_2", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab", "div_scroll_1", "iChk");
		
		// 토탈건수
		setTotCnt( "iChk", "totalRow");
		
	} else if(document.getElementById("PRPS_RGHT_CODE2").value == '3'){
		document.getElementById("div_2").style.display = "";
		document.getElementById("div_1").style.display = "none";

		fn_lock( "listTab_2", "" );
		fn_lock( "listTab", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab_2", "div_scroll_2", "iChk2");
		
		// 토탈건수
		setTotCnt( "iChk2", "totalRow2");
		
	}

	if(document.getElementById("OFFX_LINE_RECP2").value == 'Y' ) {
		document.getElementById("td_file_no").style.display = "";
		document.getElementById("td_file_yes").style.display = "none";
	} else{
		document.getElementById("td_file_no").style.display = "none";
		document.getElementById("td_file_yes").style.display = "";
	} 

}

function fn_setFileInfo(){
	var listCnt = '${fn:length(fileList)}';
	<c:if test="${!empty fileList}">
		<c:forEach items="${fileList}" var="fileList">
		fn_addGetFile(	'205',
						'${fileList.PRPS_MAST_KEY}',
						'${fileList.PRPS_SEQN}',
						'${fileList.ATTC_SEQN}',
						'${fileList.FILE_PATH}',
						'${fileList.REAL_FILE_NAME}',
						'${fileList.FILE_NAME}');
		</c:forEach>
	</c:if>		
}

// 신탁관리단체 선택
function trust_check(chk) {
	
	var nRow = document.getElementsByName("COPT_HODR").length;	// 전체 길이
	
	var oInLic = document.getElementsByName("COPT_HODR");

	for( k=0; k<nRow; k++) {
		
		if(chk.checked){ 	
			oInLic[k].readOnly = false;
			
			oInLic[k].className = 'inputDataN w90';
		}
		else {
			
			oInLic[k].readOnly = true;
			
			oInLic[k].className = 'inputDisible w90';
		}
		
	} // .. end for
}

// 권리찾기신청확인 
function rghtPrps() {

	var frm = document.prpsForm;
	
	
	var oFldInmt	= document.getElementById("fldInmtInfo");
	var oInmt	= oFldInmt.getElementsByTagName("input");
	
	var txtHomeAddr	= ""; var txtBusiAddr	= "";
	var txtHomeTelxNumb	= "";  var txtBusiTelxNumb	= "";  var txtMoblPhon	= "";  var txtFaxxNumb	= ""; var txtMail = "";
	
	
	txtHomeAddr	= document.getElementsByName("HOME_ADDR")[0].value;
	txtBusiAddr	= document.getElementsByName("BUSI_ADDR")[0].value;
	

	txtHomeTelxNumb	= document.getElementsByName("HOME_TELX_NUMB")[0].value;
	txtBusiTelxNumb	= document.getElementsByName("BUSI_TELX_NUMB")[0].value;
	txtMoblPhon	= document.getElementsByName("MOBL_PHON")[0].value;
	txtFaxxNumb	= document.getElementsByName("FAXX_NUMB")[0].value;
	txtMail	= document.getElementsByName("MAIL")[0].value;
	
	
	for(i = 0; i < oInmt.length; i++){
		/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
		if(checkField(oInmt[i]) == false){
			return;
		}
		*/
		
		// 전화번호
		if(oInmt[i].name == 'HOME_TELX_NUMB'
			|| oInmt[i].name == 'BUSI_TELX_NUMB'
			|| oInmt[i].name == 'MOBL_PHON'
			|| oInmt[i].name == 'FAXX_NUMB'){
			
			if(txtHomeTelxNumb == ''){
				if(txtBusiTelxNumb == ''){
					if(txtMoblPhon == ''){
						if(!nullCheck(oInmt[i]))	return;
					}
				}
			}
			
			if(!character(oInmt[i],  'EK'))	return;
		}
		
		// E-mail
		if(oInmt[i].name == 'MAIL'){
			if(!character(oInmt[i],  'K'))	return;
		}
		
		// 자택 사무실 주소 nullCheck
		if(oInmt[i].name == 'HOME_ADDR'
			|| oInmt[i].name == 'BUSI_ADDR'){
			if(txtHomeAddr == ''){
				if(txtBusiAddr == ''){
					if(!nullCheck(oInmt[i]))	return;
				}	
			}
		}
	}

	//if( !document.getElementById("CHK_200").checked &&  !document.getElementById("CHK_200").checked){
	/*
	if( document.getElementById("TRST_ORGN_CODE").value == '0'){
		alert('권리구분을(를) 선택하세요.');
		document.getElementById("TRST_ORGN_CODE").focus();
		return;
	}
	*/

	var PRPS_SIDE_GENRE = document.getElementById("PRPS_SIDE_GENRE").value;

	if(PRPS_SIDE_GENRE == ''){
		alert("신청저작물장르를 선택 하세요.");
		document.getElementById("PRPS_SIDE_GENRE").focus();
		return;
	}	

	var trstOrgnLen = document.getElementsByName("TRST_ORGN_CODE").length;
	var kCnt = 0;
	
	for( kk=0; kk<trstOrgnLen && kCnt ==0; kk++) {
		
		if(document.getElementsByName("TRST_ORGN_CODE")[kk].checked){
			kCnt += 1;	break;
		}
	}
	
	if(kCnt==0) {
		alert('권리구분을(를) 선택하세요.');
		
		if(PRPS_SIDE_GENRE == '6')
			document.getElementById("CHK_216_2").focus();
		else if(PRPS_SIDE_GENRE == '7')
			document.getElementById("CHK_215_2").focus();
		else
			document.getElementsByName("TRST_ORGN_CODE")[0].focus();
		return;
	}
	
	if( document.getElementsByName("iChkVal").length <1 ){
		alert('신청 저작물(를) 추가하세요.');
		document.getElementById("workAdd").focus();
		return;
	}

	var length = document.getElementsByName("PRPS_SIDE_R_GENRE").length;

	var u = 1;
	for( i=0; i<length; i++) {
		var s = eval("document.prpsForm.PRPS_SIDE_R_GENRE"+u);
		if(s.value == ''){
			alert("분야를 선택 하세요.");
			return;
		}	
		u++;
	}
		
	if(checkForm(frm)) {
		
		// 신청자 정보입력 확인 : 기존 정보/신청정보 동일유무 확인 (권리자 권리찾기인 경우만)
		if( frm.PRPS_RGHT_CODE.value == '1') {
			if( !dataConfirmcheck())
				return;
		}
				
		frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrpsProcDetl&DIVS=X";
		
		frm.method="post";
		frm.submit();

	}
}

// 신청자 정보입력 확인
function dataConfirmcheck() {
	
	var mesg;
	var reVal = true;
	
	var oTitle = document.getElementsByName("WORK_NAME");
	var oFoc = document.getElementsByName("iChk");
	
	// 확인여부
	//var oChkCop = document.getElementsByName("CHK_COPT_HODR");
	
	// 신청정보
	var oCop = document.getElementsByName("COPT_HODR");
	
	// 기존정보
	var oLicCop = document.getElementsByName("COPT_HODR_ORGN");
	
	var nRow =oFoc.length;	// 전체 길이
	
	for( k=0; k<nRow; k++) {
		
		var totCnt =0;
				
		// 저작권자
		totCnt += dataConfirm2( oCop[k], oLicCop[k])
		
		// 수정된 정보가 없다면.
		if(totCnt == 0){
			alert( "신청 저작물정보 ["+oTitle[k].value+"] 권리정보가 입력 또는 변경되지 않았습니다. \n해당저작물을 삭제하거나 권리정보를 변경해주세요.");
			oFoc[k].focus();	reVal = false;
			return;
		}
	}
	return reVal;
}

// 진행의 경우 return true
function dataConfirm2( oNew, oOrg){
	
	var reVal = 0;
	var mesg = oNew.title;
		
	if( oNew.value == oOrg.value || oNew.value == '') {
	
		reVal = 0
	
	} else {
		reVal = 1;
	}
	
	return reVal;
}

// 테이블 하위 disable
function fn_lock( tableId, flag ){
	
	var oTbl = document.getElementById(tableId);
	
	var oInput = oTbl.getElementsByTagName("input");
	for(i=0; i<oInput.length;i++){
		oInput[i].disabled= flag ;
	}
	
	var oSelect = oTbl.getElementsByTagName("select");
	for(i=0; i<oSelect.length;i++){
		oSelect[i].disabled= flag ;
	}
	
}

// 오프라인 접수
function offLine_check(chk) {
	
	if(chk.checked) {
		document.getElementById("td_file_no").style.display = "";
		document.getElementById("td_file_yes").style.display = "none";
	} else{
		document.getElementById("td_file_no").style.display = "none";
		document.getElementById("td_file_yes").style.display = "";
	} 
}

// 목록 
function goList(){
	
	var frm = document.srchForm;
	frm.action = '/rghtPrps/rghtSrch.do?DIVS=X';
	frm.method="post";
	frm.submit();
}

function showAttach(cnt) {
	var i=0;
	var content = "";
	var attach = document.getElementById("attach")
	attach.innerHTML = "";
	//document.all("attach").innerHTML = "";
	content += '<table>';
	for (i=0; i<cnt; i++) {
		content += '<tr>';
		content += '<td><input type="file" name="attachfile'+(i+1)+'" size="70;" class="inputFile"><\/td>';
		content += '<\/tr>';
	}
	content += '<\/table>';
	//document.all("attach").innerHTML = content;
	attach.innerHTML = content;
}

function applyAtch() {
var ansCnt = document.prpsForm.atchCnt.value;
showAttach(parseInt(ansCnt));
}

function Clear()
{		
	var idx = document.getElementById("IROWIDX").value;
	var PRPS_SIDE_GENRE = document.getElementById("PRPS_SIDE_GENRE").value;

	if(PRPS_SIDE_GENRE == ''){
		alert("신청저작물장르를 선택 하세요.");
		document.getElementById("PRPS_SIDE_GENRE").focus();
		return;
	}else{
		for( i=0; i<idx; i++) {
			var val = i+1;
			var PRPS_SIDE_R_GENRE = eval("document.prpsForm.PRPS_SIDE_R_GENRE"+val);

			//var PRPS_SIDE_R_GENRE = document.getElementById("PRPS_SIDE_R_GENRE1")
			if(PRPS_SIDE_R_GENRE != undefined)
			{
				 while(PRPS_SIDE_R_GENRE.options.length > 0)
					 PRPS_SIDE_R_GENRE.options.remove(0);
		   }
		}	
		var genreArray = null;
		var genre_val = null;
		for( i=0; i<idx; i++) {
			var u = i+1;
			var s = eval("document.prpsForm.PRPS_SIDE_R_GENRE"+u);
			var genreCode = document.getElementById("PRPS_SIDE_GENRE").value;
			
			if(s != undefined)
			{
	
				if(genreCode == "1" ){
					genreArray = new Array("선택","무용", "발레", "무언극", "뮤지컬", "오페라", "마당극","즉흥극","창극","기타");	
					genreArray_val = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "99");
				}else if(genreCode == "2"){
					genreArray = new Array("선택","건축물모형", "건축설계서", "건축물모형","기타");	
					genreArray_val = new Array("0", "1", "2", "3", "99");
				}else if(genreCode == "3"){
					genreArray = new Array("선택","(통수목적)지도", "도표", "설계도(건촉설계도 제외)","모형", "지구의", "약도", "기타");	
					genreArray_val = new Array("0", "1", "2", "3", "4", "5", "6", "99");
				}else if(genreCode == "4"){
					genreArray = new Array("선택","사무관리", "과학기술", "교육","오락", "기업관리", "콘텐츠개발SW", "프로그램SW", "산업용SW", "서체글꼴SW", "제어프로그램", "언어처리", "유틸리티", "데이터통신", "테이터베이스", "보안SW", "미들웨어", "기타");	
					genreArray_val = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "99");
				}else if(genreCode == "5"){
					genreArray = new Array("선택","사전", "홈페이지", "문학전집","시집", "신문", "잡지", "악보집", "논문집", "백과사전", "교육교재", "카탈로그", "단어집", "문제집", "설문지", "인명부", "전단", "데이터베이스", "기타");	
					genreArray_val = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "99");
				}else if(genreCode == "6" || genreCode == "7"){
					genreArray = new Array("선택","기타");	
					genreArray_val = new Array("0","99");
				} 	
		
				 for(i = 0; i < genreArray.length; i++) {
					 Append(genreArray[i],genreArray_val[i],idx);
				 }
			}
		}
	}	
}

// 신청저작물에 따른 권리구분setting
function chgTrstOrgnCode(setYN, selOptVal)
{	
	// 기본setting
	if(setYN != 'N') {
		var trstOrgnLen = document.getElementsByName("TRST_ORGN_CODE").length;
		var kCnt = 0;
		
		for( kk=0; kk<trstOrgnLen && kCnt ==0; kk++) {
			document.getElementsByName("TRST_ORGN_CODE")[kk].checked = 0;
		}
	}

	if(selOptVal== '6'){
		document.getElementById("td_1").style.display = "";
		document.getElementById("td_0").style.display = "none";
		document.getElementById("td_2").style.display = "none";
	}else if(selOptVal== '7'){
		document.getElementById("td_2").style.display = "";
		document.getElementById("td_0").style.display = "none";
		document.getElementById("td_1").style.display = "none";
	}else{
		document.getElementById("td_0").style.display = "";
		document.getElementById("td_1").style.display = "none";
		document.getElementById("td_2").style.display = "none";	}
}

function Append(genre,val,idx)
{
 for(a=0; a<idx; a++){	
	 var option = new Option()
	 var u = a+1;
	 var s = eval("document.prpsForm.PRPS_SIDE_R_GENRE"+u);
	 //var s = document.getElementById("PRPS_SIDE_R_GENRE1");
	 option.value = val;
	 option.text = genre;
	 s.options.add(option);
 }
// option.value = bb;
 //option.text = aa;
 //document.prpsForm.PRPS_SIDE_R_GENRE.options.add(option);
}

 function initParameter(){
	 
		if( document.getElementById("CHK_201").checked ){
			trust_check(document.getElementById("CHK_201"));
		}
		if( document.getElementById("CHK_202").checked ){
			trust_check(document.getElementById("CHK_202"));
		}
		if( document.getElementById("CHK_203").checked ){
			trust_check(document.getElementById("CHK_203"));
		}
		if( document.getElementById("CHK_204").checked ){
			trust_check(document.getElementById("CHK_204"));
		}
		if( document.getElementById("CHK_205").checked ){
			trust_check(document.getElementById("CHK_205"));
		}
		if( document.getElementById("CHK_206").checked ){
			trust_check(document.getElementById("CHK_206"));
		}
		if( document.getElementById("CHK_211").checked ){
			trust_check(document.getElementById("CHK_211"));
		}
		if( document.getElementById("CHK_212").checked ){
			trust_check(document.getElementById("CHK_212"));
		}
		if( document.getElementById("CHK_213").checked ){
			trust_check(document.getElementById("CHK_213"));
		}
		if( document.getElementById("CHK_214").checked ){
			trust_check(document.getElementById("CHK_214"));
		}
		//if( document.getElementById("CHK_215").checked ){
		//	trust_check(document.getElementById("CHK_215"));
		//}
		//if( document.getElementById("CHK_216").checked ){
		//	trust_check(document.getElementById("CHK_216"));
		//}
		if( document.getElementById("CHK_215_2").checked ){
			trust_check(document.getElementById("CHK_215_2"));
		}
		if( document.getElementById("CHK_216_2").checked ){
			trust_check(document.getElementById("CHK_216_2"));
		}
		
		chgTrstOrgnCode('N', "${rghtPrps.PRPS_SIDE_GENRE}");

		fn_createTable2("divFileList", "0");
		fn_setFileInfo();
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 


// 리사이즈
function resizeIFrame(name) {

  var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
  document.getElementById(name).height= the_height+5;
  
}

function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}
//
--></script>
</head>
<c:if test="${back.DIVS != 'BACK'}">
	<body onload="javascript:prps_check('${rghtPrps.PRPS_RGHT_CODE }');">
</c:if>
<c:if test="${back.DIVS == 'BACK'}">
	<body onload="javascript:prps_check2();">
</c:if>
<!-- 전체를 감싸는 DIVISION -->
<div id="wrap"><!-- HEADER str--> <jsp:include
	page="/include/2012/header.jsp" /> <script type="text/javascript">initNavigation(2);</script>
<!-- GNB setOn 각페이지에 넣어야합니다. --> <!-- HEADER end -->
<!-- CONTAINER str-->
<div id="container" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
	<div class="container_vis" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
		<h2><span><img src="/images/2012/title/container_vis_h2_2.gif" alt="내권리찾기" title="내권리찾기" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
		<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
	</div>
	<div class="content">
	
			<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu02.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb1");</script>
			<!-- //래프 -->
		
		<!-- 주요컨텐츠 str -->
		<div class="contentBody" id="contentBody">
			<p class="path"><span>Home</span><span>내권리찾기</span><em>저작권정보 변경신청</em></p>
			<h1><img src="/images/2012/title/content_h1_0201.gif" alt="저작권정보 변경신청" title="저작권정보 변경신청" /></h1>
			
			<div class="section">
				<!-- Tab str -->
                          <ul id="tab11" class="tab_menuBg">
                              <li class="first"><a href="#">소개</a></li>
                              <li><a href="#">이용방법</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악</a></li>
							  <li><a href="/rghtPrps/rghtSrch.do?DIVS=B">도서</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=N">뉴스</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송</a></li>
                              <li class="on"><strong><a href="/rghtPrps/rghtSrch.do?DIVS=X">기타</a></strong></li>
                      		</ul>
                <!-- //Tab -->
			
				<!-- 연락처 박스  -->
				<jsp:include page="/common/memo/2011/memo_01.jsp">
					<jsp:param name="DIVS" value="${DIVS}" />
				</jsp:include>
				<!-- //연락처 박스 -->
				
				<div class="article mt20">
					<div class="floatDiv">
						<h2 class="fl">저작권정보 변경신청</h2>
						<c:set var="smplLink" value=""/>
						<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">
							<c:set var="smplLink" value="javascript:openSmplDetail('XR')"/>
						</c:if>
						<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">
							<c:set var="smplLink" value="javascript:openSmplDetail('XU')"/>
						</c:if>
						<p class="fr"><span class="button small icon"><a href="${smplLink}">예시화면 보기</a><span class="help"></span></span></p>
					</div>
					<form name="prpsForm" enctype="multipart/form-data"  action="#">
						<input type="hidden" name="USER_IDNT" value="${userInfo.USER_IDNT}"/>
						<input type="hidden" name="USER_NAME" value="${userInfo.USER_NAME }"/>
						<input type="hidden" name="USER_DIVS" value="${userInfo.USER_DIVS }"/>
						<input type="hidden" name="PRPS_RGHT_CODE" id="PRPS_RGHT_CODE" value="${rghtPrps.PRPS_RGHT_CODE }"/><!-- 신청목적 -->
						<input type="hidden" name="RESD_CORP_NUMB_VIEW" value="${userInfo.RESD_CORP_NUMB_VIEW }"/>
						<input type="hidden" name="CORP_NUMB" value="${userInfo.CORP_NUMB }"/>
						<input type="hidden" name="PRPS_RGHT_CODE2" id="PRPS_RGHT_CODE2" value="${rghtPrps.PRPS_RGHT_CODE }"/>
						<input type="hidden" name="OFFX_LINE_RECP2" id="OFFX_LINE_RECP2" value="${rghtPrps.OFFX_LINE_RECP }"/>
						<input type="hidden" name="FILE_INFO" value="${rghtPrps.FILE_INFO}" />
						<input type="hidden" name="IROWIDX" id="IROWIDX" value="${length}" />
						<input type="hidden" name="BACK_IROWIDX" id="BACK_IROWIDX" value="${length}" />
						<input type="hidden" name="DIVS_VAL" id="DIVS_VAL" value="${back.DIVS}" />
						<input type="submit" style="display:none;">
					<span class="topLine"></span>
					<!-- 그리드스타일 -->
					<table cellspacing="0" cellpadding="0" border="1" summary="저작권찾기 신청정보 입력 폼입니다." class="grid tableFixed">
						<colgroup>
						<col width="20%">
						<col width="*">
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">신청목적</th>
								<td><!-- <select name="PRPS_RGHT_CODE" id="PRPS_RGHT_CODE"  title="신청목적" nullCheck onchange="prps_check(this);">-->
									<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">권리자의 저작권찾기</c:if>
									<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">이용자의 저작권조회</c:if>
								</td>
							</tr>
							<tr>
								<th scope="row">신청인정보</th>
								<td>
									<span class="topLine2"></span>
									<!-- 그리드스타일 -->
									<div id="fldInmtInfo">
									<table cellspacing="0" cellpadding="0" border="1" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다." class="grid">
										<colgroup>
										<col width="15%">
										<col width="32%">
										<col width="25%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row">성명</th>
												<td>${userInfo.USER_NAME}</td>
												<th scope="row">주민등록번호/사업자번호</th>
												<c:if test="${userInfo.USER_DIVS != '03'}">
												<td>
												${userInfo.RESD_CORP_NUMB_VIEW}
												</td>
												</c:if>
												<c:if test="${userInfo.USER_DIVS != '01'}">
												<td>
												${fn:substring(userInfo.CORP_NUMB,0,3)}-${fn:substring(userInfo.CORP_NUMB,3,5)}-${fn:substring(userInfo.CORP_NUMB,5,10)}
												</td>
												</c:if>
											</tr>
											<tr>
												<th scope="row"><label for="regi1" class="necessary">전화번호</label></th>
												<td>
													<ul class="list1">
													<li class="p11"><label for="regi1" class="inBlock w25">자택</label> : <input type="text" id="regi1" size="14" maxlength="20" name="HOME_TELX_NUMB" title="자택 전화" value="${userInfo.TELX_NUMB}" /></li>
													<li class="p11"><label for="regi2" class="inBlock w25">사무실</label> : <input type="text" id="regi2" size="14"  maxlength="20" name="BUSI_TELX_NUMB" title="사무실 전화" value="${userInfo.BUSI_TELX_NUMB}"/></li>
													<li class="p11"><label for="regi3" class="inBlock w25">휴대폰</label> : <input type="text" id="regi3" size="14" maxlength="20" name="MOBL_PHON" title="휴대폰 전화" value="${userInfo.MOBL_PHON}" /></li>
													</ul>
												</td>
												<th scope="row"><label for="regi4">팩스번호</label></th>
												<td><input type="text" name="FAXX_NUMB" id="regi4" title="팩스번호" maxlength="20" value="${userInfo.FAXX_NUMB}" size="20" /></td>
											</tr>
											<tr>
												<th scope="row"><label for="regi5">이메일주소</label></th>
												<td colspan="3"><input type="text" name="MAIL" id="regi5" title="이메일주소" maxlength="50" value="${userInfo.MAIL}" class="w50" /></td>
											</tr>
											<tr>
												<th scope="row"><label for="regi6" class="necessary">주소</label></th>
												<td colspan="3">
													<ul class="list1">
													<li class="p11"><label for="regi6" class="inBlock w10">자택</label> : <input type="text" id="regi6" name="HOME_ADDR" title="자택 주소" maxlength="50" value="${userInfo.HOME_ADDR}" class="w85" /></li>
													<li class="p11"><label for="regi7" class="inBlock w10">사무실</label> : <input type="text" id="regi7" name="BUSI_ADDR" title="사무실 주소" maxlength="50" value="${userInfo.BUSI_ADDR}" class="w85" /></li>
													</ul>
												</td>
											</tr>
										</tbody>
									</table>
									</div>
									<!-- //그리드스타일 -->
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="PRPS_SIDE_GENRE" class="necessary">신청저작물장르</label></th>
								<td>
								<select name="PRPS_SIDE_GENRE" id="PRPS_SIDE_GENRE" title="신청저작물장르" onChange="Clear();chgTrstOrgnCode('Y', this.value)">
									<option value="">선택</option>
									<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '1'}">selected="selected"</c:if>>연극</option>
									<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '2'}">selected="selected"</c:if>>건축</option>
									<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '3'}">selected="selected"</c:if>>도형</option>
									<option value="4" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '4'}">selected="selected"</c:if>>컴퓨터프로그램</option>
									<option value="5" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '5'}">selected="selected"</c:if>>편집</option>
									<!-- 
									<option value="6" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '6'}">selected="selected"</c:if>>공공콘텐츠</option>
									 -->
									<!--
									<option value="7" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '7'}">selected="selected"</c:if>>뉴스</option>
									  -->
								</select>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="CHK_201" class="necessary">권리구분</label></th>
								<td id="td_0" style="display">
									<ul class="line22">
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_201" value="201" class="inputRChk" 
										onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_201 == '201'}">checked</c:if> title="저작권자(한국음악저작권협회)"/>
										<label class="p12" for="CHK_201">저작권자(한국음악저작권협회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_202" value="202" class="inputRChk" 
										onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_202 == '202'}">checked</c:if> title="저작권자(한국음악실연자연합회)"/>
										<label class="p12" for="CHK_202">저작권자(한국음악실연자연합회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_203" value="203" class="inputRChk" 
										onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_203 == '203'}">checked</c:if> title="저작권자(한국음반산업협회)"/>
										<label class="p12" for="CHK_203">저작권자(한국음반산업협회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_204" value="204" class="inputRChk" 
										onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_204 == '204'}">checked</c:if> title="저작권자(한국문예학술저작권협회)"/>
										<label class="p12" for="CHK_204">저작권자(한국문예학술저작권협회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_205" value="205" class="inputRChk" 
										onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_205 == '205'}">checked</c:if> title="저작권자(한국복사전송권협회)"/>
										<label class="p12" for="CHK_205">저작권자(한국복사전송권협회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_206" value="206" class="inputRChk" 
										onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_206 == '206'}">checked</c:if> title="저작권자(한국방송작가협회)"/>
										<label class="p12" for="CHK_206">저작권자(한국방송작가협회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_211" value="211" class="inputRChk" 
										onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_211 == '211'}">checked</c:if> title="저작권자(한국영화배급협회)"/>
										<label class="p12" for="CHK_211">저작권자(한국영화배급협회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_212" value="212" class="inputRChk" 
										onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_212 == '212'}">checked</c:if> title="저작권자(한국시나리오작가협회)"/>
										<label class="p12" for="CHK_212">저작권자(한국시나리오작가협회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_213" value="213" class="inputRChk" 
										onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_213 == '213'}">checked</c:if> title="저작권자(한국영화제작가협회)"/>
										<label class="p12" for="CHK_213">저작권자(한국영화제작가협회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_214" value="214" class="inputRChk" 
										onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_214 == '214'}">checked</c:if> title="저작권자(한국방송실연자협회)"/>
										<label class="p12" for="CHK_214">저작권자(한국방송실연자협회)</label>
									</li>
									</ul>
								</td>
								<td id="td_1" style="display:none">
									<ul class="line22">
										<li>
											<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_216_2" value="216" class="inputRChk" 
											onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_216 == '216'}">checked</c:if> title="저작권(한국문화콘텐츠진흥원)" />
											<label class="p12" for="CHK_216_2">저작권(한국문화콘텐츠진흥원)</label>
										</li>
									</ul>
								</td>
								<td id="td_2" style="display:none">
									<ul class="line22">
										<li>
											<input type="checkbox" title="선택" name="TRST_ORGN_CODE" id="CHK_215_2" value="215" class="inputRChk" 
											onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_215 == '215'}">checked</c:if> title="뉴스 작가 - 저작권자(한국언론진흥재단)"/>
											<label class="p12" for="CHK_216_2">뉴스 작가 - 저작권자(한국언론진흥재단)</label>
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<th scope="row"><label class="necessary">신청저작물 정보</label></th>
								<td>
									<!-- 권리자 저작권찾기 시작 -->
									<div id="div_1" class="tabelRound" style="width:572px; padding:0 0 0 0; display:none">
										<span class="blue2 p11">&lowast; 권리있는 항목만 입력하세요.</span>
										
										<div class="floatDiv mb5 mt10"><h3 class="fl mt5">권리자 저작권찾기<span id="totalRow"></span></h3>
										<p class="fr"><a href="#1" onclick="javascript:editTable('I');" id="workAdd"><img src="/images/2012/button/add.gif" alt="추가" /></a>
										<a href="#1" onclick="javascript:editTable('D');"><img src="/images/2012/button/delete.gif" alt="삭제" />
										</a></p>
										</div>
										
										<div id="div_scroll_1" style="width:569px; padding:0 0 0 0;">
											<table id="listTab" cellspacing="0" cellpadding="0" border="1" summary="권리자 저작권찾기 신청저작물정보 폼입니다." class="grid ce tableFixed"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
												<colgroup>
												<col width="5%">
												<col width="5%">
												<col width="*">
												<col width="17%">
												<col width="22%">
												<col width="16%">
												<col width="13%">
												</colgroup>
												<thead>
													<tr>
														<th scope="col"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk',this);" title="전체선택" /></th>
														<th scope="col">순번</th>
														<th scope="col"><label class="necessary white">저작물명</label></th>
														<th scope="col"><label class="necessary white">분야</label></th>
														<th scope="col"><label class="necessary white">공표매체</label></th>
														<th scope="col">공표일자</th>
														<th scope="col"><label class="necessary white">저작권자</label></th>
													</tr>
												</thead>
												<tbody>
												<c:if test="${back.DIVS != 'BACK'}">
													<c:if test="${!empty prpsList}">
														<c:forEach items="${prpsList}" var="prpsList">
															<c:set var="NO" value="${prpsList.totalRow}"/>
															<c:set var="i" value="${i+1}"/>
														<tr>
															<td class="ce pd5"><input type="checkbox" title="선택" name="iChk" id="iChk_${i}" class="vmid"  /></td>
																<td class="ce pd5"><input name="displaySeq" title="순번" id="displaySeq_${i}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}"/></td>
																<td class="lft pd5"><a class="underline black2" href="#1" onclick="javascript:openImageDetail('${prpsList.WORK_FILE_NAME }','${prpsList.WORK_NAME }');">${prpsList.WORK_NAME }</a></td>
																<td>${prpsList.IMAGE_DIVS }</td>
																<td class="ce pd5">&nbsp;</td>
																<td class="ce pd5">&nbsp;</td>
																<td class="ce pd5"><input name="COPT_HODR" id="COPT_HODR_${i}" class="inputDisible w90 ce" value='${prpsList.COPT_HODR }' title="저작권자" rangeSize="~100" readonly="readonly" />
																	<!-- hidden Value -->
																	<input type="hidden" name="iChkVal" value="${prpsList.IMGE_SEQN }|0|0"/>
																	<input type="hidden" name="WORK_NAME"  value="${prpsList.WORK_NAME}"/>
																	<input type="hidden" name="IMAGE_DIVS" value="${prpsList.IMAGE_DIVS }"/>
																	<input type="hidden" name="OPEN_MEDI_CODE" />
																	<input type="hidden" name="OPEN_MEDI_TEXT" />
																	<input type="hidden" name="COPT_HODR_ORGN" value="${prpsList.COPT_HODR }" />
																	<!-- 사용자정보입력확인 -->
																	<input type="hidden" name="CHK_COPT_HODR" />
																</td>
															</tr>
															</c:forEach>
														</c:if>
													</c:if>
													<c:if test="${back.DIVS == 'BACK'}">	
													<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1' || rghtPrps.PRPS_RGHT_CODE == '2'}">
														<c:forEach items="${rghtPrps.keyId}" var="prpsList">
															<c:set var="i" value="${i+1}"/>
															<tr>
																<td class="ce pd5"><input type="checkbox" name="iChk" id="iChk_${i}" class="vmid" title="선택" /></td>
																<td class="ce pd5"><input name="displaySeq" id="displaySeq_${i}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}" title="순번"/></td>
															<!-- 추가입력 -->
															<c:if test="${empty rghtPrps.keyId[i-1]}">
																<td class="pd5"><input name="WORK_NAME"  id="WORK_NAME" class="inputDataN w90" title="저작물명" rangeSize="~100"  nullCheck value="${rghtPrps.WORK_NAME_ARR_TRNS[i-1] }"/></td>
																<td class="pd5">
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == 1}">
																	<select name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${i}" class="w95" title="분야코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '1'}">selected="selected"</c:if>>무용</option>
																		<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '2'}">selected="selected"</c:if>>발레</option>
																		<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '3'}">selected="selected"</c:if>>무언극</option>
																		<option value="4" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '4'}">selected="selected"</c:if>>뮤지컬</option>
																		<option value="5" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '5'}">selected="selected"</c:if>>오페라</option>
																		<option value="6" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '6'}">selected="selected"</c:if>>마당극</option>
																		<option value="7" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '7'}">selected="selected"</c:if>>즉흥극</option>
																		<option value="8" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '8'}">selected="selected"</c:if>>창극</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '2'}">
																	<select name="PRPS_SIDE_R_GENRE" class="w95" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '1'}">selected="selected"</c:if>>건축물</option>
																		<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '2'}">selected="selected"</c:if>>건축설계서</option>
																		<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '3'}">selected="selected"</c:if>>건축물모형</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '3'}">
																	<select name="PRPS_SIDE_R_GENRE" class="w95" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '1'}">selected="selected"</c:if>>(통수목적)지도</option>
																		<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '2'}">selected="selected"</c:if>>도표</option>
																		<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '3'}">selected="selected"</c:if>>설계도(건촉설계도 제외)</option>
																		<option value="4" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '4'}">selected="selected"</c:if>>모형</option>
																		<option value="5" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '5'}">selected="selected"</c:if>>지구의</option>
																		<option value="6" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '6'}">selected="selected"</c:if>>약도</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '4'}">
																	<select name="PRPS_SIDE_R_GENRE" class="w95" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '1'}">selected="selected"</c:if>>사무관리</option>
																		<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '2'}">selected="selected"</c:if>>과학기술</option>
																		<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '3'}">selected="selected"</c:if>>교육</option>
																		<option value="4" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '4'}">selected="selected"</c:if>>오락</option>
																		<option value="5" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '5'}">selected="selected"</c:if>>기업관리</option>
																		<option value="6" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '6'}">selected="selected"</c:if>>콘텐츠개발SW</option>
																		<option value="7" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '7'}">selected="selected"</c:if>>프로그램SW</option>
																		<option value="8" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '8'}">selected="selected"</c:if>>산업용SW</option>
																		<option value="9" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '9'}">selected="selected"</c:if>>서체글꼴SW</option>
																		<option value="10" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '10'}">selected="selected"</c:if>>제어프로그램</option>
																		<option value="11" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '11'}">selected="selected"</c:if>>언어처리</option>
																		<option value="12" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '12'}">selected="selected"</c:if>>유틸리티</option>
																		<option value="13" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '13'}">selected="selected"</c:if>>데이터통신</option>
																		<option value="14" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '14'}">selected="selected"</c:if>>테이터베이스</option>
																		<option value="15" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '15'}">selected="selected"</c:if>>보안SW</option>
																		<option value="16" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '16'}">selected="selected"</c:if>>미들웨어</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '5'}">
																	<select name="PRPS_SIDE_R_GENRE" class="w95" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '1'}">selected="selected"</c:if>>사전</option>
																		<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '2'}">selected="selected"</c:if>>홈페이지</option>
																		<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '3'}">selected="selected"</c:if>>문학전집</option>
																		<option value="4" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '4'}">selected="selected"</c:if>>시집</option>
																		<option value="5" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '5'}">selected="selected"</c:if>>신문</option>
																		<option value="6" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '6'}">selected="selected"</c:if>>잡지</option>
																		<option value="7" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '7'}">selected="selected"</c:if>>악보집</option>
																		<option value="8" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '8'}">selected="selected"</c:if>>논문집</option>
																		<option value="9" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '9'}">selected="selected"</c:if>>백과사전</option>
																		<option value="10" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '10'}">selected="selected"</c:if>>교육교재</option>
																		<option value="11" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '11'}">selected="selected"</c:if>>카탈로그</option>
																		<option value="12" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '12'}">selected="selected"</c:if>>단어집</option>
																		<option value="13" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '13'}">selected="selected"</c:if>>문제집</option>
																		<option value="14" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '14'}">selected="selected"</c:if>>설문지</option>
																		<option value="15" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '15'}">selected="selected"</c:if>>인명부</option>
																		<option value="16" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '16'}">selected="selected"</c:if>>전단</option>
																		<option value="17" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '17'}">selected="selected"</c:if>>데이터베이스</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '6' || rghtPrps.PRPS_SIDE_GENRE == '7' }">
																	<select name="PRPS_SIDE_R_GENRE" class="w95" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																		<option value="">선택</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[i-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																</td>
																<td>
																	<select name="OPEN_MEDI_CODE" id="OPEN_MEDI_CODE" class="w95" title="공표매체코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[i-1] == '1'}">selected="selected"</c:if>>출판사명</option>
																		<option value="2" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[i-1] == '2'}">selected="selected"</c:if>>배포대상</option>
																		<option value="3" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[i-1] == '3'}">selected="selected"</c:if>>인터넷주소</option>
																		<option value="4" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[i-1] == '4'}">selected="selected"</c:if>>공연장소</option>
																		<option value="5" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[i-1] == '5'}">selected="selected"</c:if>>방송사 및 프로그램명</option>
																		<option value="6" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[i-1] == '6'}">selected="selected"</c:if>>기타</option>
																	</select>
																	<input name="OPEN_MEDI_TEXT"  id="OPEN_MEDI_TEXT" class="inputDataN w95 mt5" title="공표매체" rangeSize="~50" value='${rghtPrps.OPEN_MEDI_TEXT_ARR_TRNS[i-1] }'/>
																</td>
																<td class="ce pd5">
																<!--<input name="OPEN_DATE"  id="OPEN_DATE" class="inputDataN w90" title="공표일자" character="KE"  maxlength="8" value="${rghtPrps.OPEN_DATE_ARR[i-1] }"/>-->
																<input name="OPEN_DATE_${i}"  id="OPEN_DATE_${i}" class="inputDataN w65" title="공표일자" maxlength="8" Size="8" character="KE" dateCheck value = "${rghtPrps.OPEN_DATE_ARR[i-1] }"/>&nbsp;
																<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('prpsForm','OPEN_DATE_${i}');" onkeypress="javascript:fn_cal('prpsForm','OPEN_DATE_${i}');" alt="달력" title="공표일자를 선택하세요." align="middle" style="cursor:pointer;"/>
																<input type="hidden" name="OPEN_DATE" value="${i}"/>
																</td>
															</c:if>
															<!-- 기존 저작물 -->
															<c:if test="${!empty rghtPrps.keyId[i-1]}">
																<td class="w95">${rghtPrps.WORK_NAME_ARR[i-1] }</td>
																<td class="w95">${rghtPrps.IMAGE_DIVS_ARR[i-1] }</td>
																<td class="ce pd5">&nbsp;</td>
																<td class="ce pd5">&nbsp;</td>
																<input type="hidden" name="WORK_NAME"  value="${rghtPrps.WORK_NAME_ARR_TRNS[i-1]}"/>
																<input type="hidden" name="IMAGE_DIVS" value='${rghtPrps.IMAGE_DIVS_ARR_TRNS[i-1]}'/>
																<input type="hidden" name="OPEN_MEDI_CODE" />
																<input type="hidden" name="OPEN_MEDI_TEXT" />
															</c:if>
															
															<td class="ce pd5"><input name="COPT_HODR" id="COPT_HODR" class="inputDisible w95 ce" value='${rghtPrps.COPT_HODR_ARR_TRNS[i-1] }' title="저작권자" rangeSize="~100" readonly="readonly" />
																<!-- hidden Value -->
																<input type="hidden" name="iChkVal" value="${rghtPrps.keyId[i-1] }"/>
																<input type="hidden" name="COPT_HODR_ORGN" value="${rghtPrps.COPT_HODR_ORGN_ARR_TRNS[i-1] }" />
																<!-- 사용자정보입력확인 -->
																<input type="hidden" name="CHK_COPT_HODR" />
															</td>
														</tr>
													</c:forEach>
												</c:if>
												</c:if>
												</tbody>
											</table>
										</div>
									</div>
									<!-- 권리자의 저작권 찾기 끝 -->
								
									
									<!-- 이용자의 저작권 찾기 시작 -->
									<div id="div_2" class="tabelRound mt10" style="width:572px; display:none">
										<span class="blue2 p11">&lowast; 권리있는 항목만 입력하세요.</span>
										
										<div class="floatDiv mb5 mt10"><h3 class="fl mt5">이용자 저작권조회<span id="totalRow2"></span>
										</h3>
										<p class="fr"><a href="#1" onclick="javascript:editTable('I');" id="workAdd"><img src="/images/2012/button/add.gif" alt="추가" /></a>
										<a href="#1" onclick="javascript:editTable('D');"><img src="/images/2012/button/delete.gif" alt="삭제" />
										</a></p>
										</div>
										
										<div id="div_scroll_2" style="width:569px; padding:0 0 0 0;">
											<table id="listTab_2" cellspacing="0" cellpadding="0" border="1" summary="권리자 저작권찾기 신청저작물정보 폼입니다." class="grid ce"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
												<colgroup>
												<col width="5%">
												<col width="5%">
												<col width="*">
												<col width="20%">
												<col width="23%">
												<col width="16%">
												</colgroup>
												<thead>
													<tr>
														<th scope="col">
														<input type="checkbox" title="선택" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk2',this);"/></th>
														<th scope="col">순번</th>
														<th scope="col"><label class="necessary white">저작물명</label></th>
														<th scope="col"><label class="necessary white">분야</label></th>
														<th scope="col"><label class="necessary white">공표매체</label></th>
														<th scope="col">공표일자</th>
													</tr>
												</thead>
												<tbody>
												<c:if test="${back.DIVS != 'BACK'}">
													<c:if test="${!empty prpsList}">
														<c:forEach items="${prpsList}" var="prpsList">
															<c:set var="NO" value="${prpsList.totalRow}" />
															<c:set var="ii" value="${ii+1}" />
															<tr>
																<td class="ce pd5">
																	<input type="checkbox" name="iChk2" id="iChk2_${ii}" class="vmid" title="선택" />
																</td>
																<td class="ce pd5">
																	<input name="displaySeq2" id="displaySeq2_${ii}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${ii}" title="순번" />
																</td>
																<td class="pd5">
																	<a href="#1" onclick="javascript:openImageDetail('${prpsList.WORK_FILE_NAME }','${prpsList.WORK_NAME }');"><u>${prpsList.WORK_NAME }</u></a>
																</td>
																<td class="ce pd5">${prpsList.IMAGE_DIVS }</td>
																<td class="ce pd5">&nbsp;</td>
																<td class="ce pd5">&nbsp;
																<!-- hidden Value -->
																<input type="hidden" name="iChkVal" value="${prpsList.IMGE_SEQN }|0|0"/>
																<input type="hidden" name="WORK_NAME"  value="${prpsList.WORK_NAME }"/>
																<input type="hidden" name="IMAGE_DIVS" value="${prpsList.IMAGE_DIVS }"/>
																<input type="hidden" name="OPEN_MEDI_CODE" />
																<input type="hidden" name="OPEN_MEDI_TEXT" />
																<!----><input type="hidden" name="OPEN_DATE" />
																<input type="hidden" name="COPT_HODR" />
																</td>
															</tr>
														</c:forEach>
													</c:if>
												</c:if>
												<c:if test="${back.DIVS == 'BACK'}">	
												<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">	
													<c:forEach items="${rghtPrps.keyId}" var="prpsList">
														<c:set var="ii" value="${ii+1}"/>
														<tr>
															<td class="ce pd5"><input type="checkbox" title="선택" name="iChk2" id="iChk2_${ii}" class="vmid" /></td>
															<td class="ce pd5"><input name="displaySeq2" title="순번" id="displaySeq2_${ii}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${ii}"/></td>
															<c:if test="${empty rghtPrps.keyId[ii-1]}">
																<td class="pd5">
																	<input name="WORK_NAME"  id="WORK_NAME" class="inputDataN w95" title="이미지명" rangeSize="~100"  nullCheck value="${rghtPrps.WORK_NAME_ARR_TRNS[ii-1]}"/></td>
																<td class="pd5">
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == 1}">
																	<select class="w95" name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${ii}" title="분야코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '1'}">selected="selected"</c:if>>무용</option>
																		<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '2'}">selected="selected"</c:if>>발레</option>
																		<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '3'}">selected="selected"</c:if>>무언극</option>
																		<option value="4" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '4'}">selected="selected"</c:if>>뮤지컬</option>
																		<option value="5" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '5'}">selected="selected"</c:if>>오페라</option>
																		<option value="6" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '6'}">selected="selected"</c:if>>마당극</option>
																		<option value="7" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '7'}">selected="selected"</c:if>>즉흥극</option>
																		<option value="8" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '8'}">selected="selected"</c:if>>창극</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '2'}">
																	<select class="w95" name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${ii}" title="분야코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '1'}">selected="selected"</c:if>>건축물</option>
																		<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '2'}">selected="selected"</c:if>>건축설계서</option>
																		<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '3'}">selected="selected"</c:if>>건축물모형</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '3'}">
																	<select class="w95" name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${ii}" title="분야코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '1'}">selected="selected"</c:if>>(통수목적)지도</option>
																		<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '2'}">selected="selected"</c:if>>도표</option>
																		<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '3'}">selected="selected"</c:if>>설계도(건촉설계도 제외)</option>
																		<option value="4" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '4'}">selected="selected"</c:if>>모형</option>
																		<option value="5" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '5'}">selected="selected"</c:if>>지구의</option>
																		<option value="6" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '6'}">selected="selected"</c:if>>약도</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '4'}">
																	<select class="w95" name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${ii}" title="분야코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '1'}">selected="selected"</c:if>>사무관리</option>
																		<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '2'}">selected="selected"</c:if>>과학기술</option>
																		<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '3'}">selected="selected"</c:if>>교육</option>
																		<option value="4" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '4'}">selected="selected"</c:if>>오락</option>
																		<option value="5" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '5'}">selected="selected"</c:if>>기업관리</option>
																		<option value="6" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '6'}">selected="selected"</c:if>>콘텐츠개발SW</option>
																		<option value="7" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '7'}">selected="selected"</c:if>>프로그램SW</option>
																		<option value="8" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '8'}">selected="selected"</c:if>>산업용SW</option>
																		<option value="9" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '9'}">selected="selected"</c:if>>서체글꼴SW</option>
																		<option value="10" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '10'}">selected="selected"</c:if>>제어프로그램</option>
																		<option value="11" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '11'}">selected="selected"</c:if>>언어처리</option>
																		<option value="12" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '12'}">selected="selected"</c:if>>유틸리티</option>
																		<option value="13" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '13'}">selected="selected"</c:if>>데이터통신</option>
																		<option value="14" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '14'}">selected="selected"</c:if>>테이터베이스</option>
																		<option value="15" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '15'}">selected="selected"</c:if>>보안SW</option>
																		<option value="16" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '16'}">selected="selected"</c:if>>미들웨어</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '5'}">
																	<select class="w95" name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${ii}" title="분야코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '1'}">selected="selected"</c:if>>사전</option>
																		<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '2'}">selected="selected"</c:if>>홈페이지</option>
																		<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '3'}">selected="selected"</c:if>>문학전집</option>
																		<option value="4" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '4'}">selected="selected"</c:if>>시집</option>
																		<option value="5" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '5'}">selected="selected"</c:if>>신문</option>
																		<option value="6" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '6'}">selected="selected"</c:if>>잡지</option>
																		<option value="7" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '7'}">selected="selected"</c:if>>악보집</option>
																		<option value="8" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '8'}">selected="selected"</c:if>>논문집</option>
																		<option value="9" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '9'}">selected="selected"</c:if>>백과사전</option>
																		<option value="10" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '10'}">selected="selected"</c:if>>교육교재</option>
																		<option value="11" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '11'}">selected="selected"</c:if>>카탈로그</option>
																		<option value="12" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '12'}">selected="selected"</c:if>>단어집</option>
																		<option value="13" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '13'}">selected="selected"</c:if>>문제집</option>
																		<option value="14" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '14'}">selected="selected"</c:if>>설문지</option>
																		<option value="15" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '15'}">selected="selected"</c:if>>인명부</option>
																		<option value="16" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '16'}">selected="selected"</c:if>>전단</option>
																		<option value="17" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '17'}">selected="selected"</c:if>>데이터베이스</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '6' || rghtPrps.PRPS_SIDE_GENRE == '7'}">
																	<select class="w95" name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${ii}" title="분야코드">
																		<option value="">선택</option>
																		<option value="99" <c:if test="${rghtPrps.PRPS_SIDE_R_GENRE_ARR[ii-1] == '99'}">selected="selected"</c:if>>기타</option>
																	</select>
																</c:if>
																</td>
																<td class="ce pd5">
																	<select class="w95" name="OPEN_MEDI_CODE" id="OPEN_MEDI_CODE" title="공표매체코드">
																		<option value="">선택</option>
																		<option value="1" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[ii-1] == '1'}">selected="selected"</c:if>>출판사명</option>
																		<option value="2" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[ii-1] == '2'}">selected="selected"</c:if>>배포대상</option>
																		<option value="3" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[ii-1] == '3'}">selected="selected"</c:if>>인터넷주소</option>
																		<option value="4" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[ii-1] == '4'}">selected="selected"</c:if>>공연장소</option>
																		<option value="5" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[ii-1] == '5'}">selected="selected"</c:if>>방송사 및 프로그램명</option>
																		<option value="6" <c:if test="${rghtPrps.OPEN_MEDI_CODE_ARR[ii-1] == '6'}">selected="selected"</c:if>>기타</option>
																	</select>
																	<input name="OPEN_MEDI_TEXT"  id="OPEN_MEDI_TEXT" class="inputDataN w95 mt5" title="공표매체" rangeSize="~50" value='${rghtPrps.OPEN_MEDI_TEXT_ARR_TRNS[ii-1] }'/>
																</td>
																<td class="ce pd5">
																	<input name="VAL_OPEN_DATE_${ii}"  id="VAL_OPEN_DATE_${ii}" class="inputDataN w65" title="공표일자" maxlength="8" Size="8" character="KE" dateCheck value = "${rghtPrps.OPEN_DATE_ARR[ii-1] }"/>&nbsp;
																	<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('prpsForm','VAL_OPEN_DATE_${ii}');" onkeypress="javascript:fn_cal('prpsForm','VAL_OPEN_DATE_${ii}');" alt="달력" title="공표일자를 선택하세요." align="middle" style="cursor:pointer;"/>
																	<input type="hidden" name="VAL_OPEN_DATE" value="${ii}"/>
																</td>
															</c:if>
															<c:if test="${!empty rghtPrps.keyId[ii-1]}">
																<td class="pd5">${rghtPrps.WORK_NAME_ARR[ii-1] }</td>
																<td class="pd5">${rghtPrps.IMAGE_DIVS_ARR[ii-1] }</td>
																<td class="ce pd5">&nbsp;</td>
																<td class="ce pd5">&nbsp;
																<input type="hidden" name="WORK_NAME"  value="${rghtPrps.WORK_NAME_ARR_TRNS[ii-1]}"/>
																<input type="hidden" name="IMAGE_DIVS" value='${rghtPrps.IMAGE_DIVS_ARR_TRNS[ii-1]}'/>
																<input type="hidden" name="OPEN_MEDI_CODE" />
																<input type="hidden" name="OPEN_MEDI_TEXT" />
																<input type="hidden" name="VAL_OPEN_DATE" />
																</td>
															</c:if>
																<!-- hidden Value -->
																<input type="hidden" name="iChkVal" value="${rghtPrps.keyId[ii-1]}"/>
																<input type="hidden" name="COPT_HODR" />
														</tr>
													</c:forEach>
												</c:if>
												</c:if>
												</tbody>
											</table>
										</div>
									</div>
									<!-- 이용자의 저작권 찾기 끝 -->
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="PRPS_RGHT_DESC" class="necessary">신청저작물설명</label></th>
								<td>
									<textarea cols="10" rows="10" name="PRPS_RGHT_DESC" id="PRPS_RGHT_DESC"
									class="w99" title="신청저작물설명" nullCheck>${rghtPrps.PRPS_RGHT_DESC}</textarea>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="PRPS_DESC" class="necessary">신청내용</label></th>
								<td>
									<p class="p11 blue2">&lowast; 신청 목적이 이용자 저작권 조회인 경우 이용내용(이용하고자 하는 목적/방법)을 작성합니다.<br />&lowast; 신청 저작물 정보의 가사 및 저작물 변경 등을 작성합니다.</p>
									<textarea cols="10" rows="10" name="PRPS_DESC" id="PRPS_DESC"
									class="w99" title="신청내용" nullCheck>${rghtPrps.PRPS_DESC}</textarea>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="file1">첨부파일<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">(증빙서류)</c:if> <br />
								<input type="checkbox" name="OFFX_LINE_RECP" id="file1" value="Y"
								class="inputChk" onclick="javascript:offLine_check(this);"
								<c:if test="${rghtPrps.OFFX_LINE_RECP == 'Y'}">checked="checked"</c:if>
								title="오프라인접수" />오프라인접수</label>
								</th>
								<td id="td_file_no" style="display:none">오프라인접수로 선택한 경우 첨부파일을 등록할 수 없습니다.</td>
								<td id="td_file_yes">
								<!--  첨부파일 테이블 영역입니다 -->
								<div id="divFileList" class="tabelRound mb5" style="display;">
								</div>
								</td>
								<!--
								<td>
									<div class="floatDiv">
										<p class="fl"><input type="checkbox" id="file1" /><label for="file1" class="p11">오프라인접수</label></p>
										<p class="fr"><span class="button small icon"><a href="">파일추가</a><span class="add"></span></span> <span class="button small icon"><a href="">파일삭제</a><span class="delete"></span></span></p>
									</div>
									
									<ul class="list2 bgGray1 mt10 pd5">
									<li><a href="">파일 11111</a></li>
									<li><a href="">파일 11111</a></li>
									</ul>
								</td>
								-->
							</tr>
						</tbody>
					</table>
					<!-- //그리드스타일 -->
					<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">
					<div class="white_box">
						<div class="box5">
							<div class="box5_con floatDiv">
								<p class="fl mt5"><img src="/images/2012/content/box_img4.gif" alt="" /></p>
								<div class="fl ml30 mt5">
									<h4>첨부파일(증빙서류) 안내</h4>
									<ul class="list1 mt10">
									<li>저작권자임을 증명할 수 있는 서류(앨범자켓, 저작권등록증 등)</li>
									<li>상속, 양도, 승계 등의 사실을 증명할 수 있는 서류</li>
									<li>주민등록등본/법인등기부등본</li>
									<li>사업자등록증(사본) 1부</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					</c:if>
					
					<div class="btnArea">
						<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:goList()">목록</a></span></p>
						<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:rghtPrps();">저작권찾기 신청 확인</a></span></p>
					</div>
				</form>
				</div>
			</div>
		</div>
		<!-- //주요컨텐츠 end -->
	</div>
</div>
<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


<form name="srchForm" action="#">
<input type="hidden" name="srchWorkName" value="${srchParam.srchWorkName }"/>
<input type="hidden" name="srchLishComp" value="${srchParam.srchLishComp }"/>
<input type="hidden" name="srchCoptHodr" value="${srchParam.srchCoptHodr }"/>
<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
<input type="submit" style="display:none;">
</form>
<script type="text/javascript" src="/js/2011/file.js"></script>
<script type="text/javascript">
<!--

	<c:if test="${back.DIVS != 'BACK'}">	
		<c:if test="${!empty prpsList}">
			<c:forEach items="${prpsList}" var="prpsList">
				iRowIdx3++;
				iRowIdx2++;
			</c:forEach>
		</c:if>
	</c:if>
	
	<c:if test="${back.DIVS == 'BACK'}">	
		<c:forEach items="${rghtPrps.keyId}" var="prpsList">
			iRowIdx3++;
			iRowIdx2++;
		</c:forEach>
	</c:if>

	function fn_setFileInfo(){
		
		var listCnt = '${fn:length(fileList)}';
		
		<c:if test="${!empty fileList}">
			<c:forEach items="${fileList}" var="fileList">
				fn_addGetFile(	'0',
								'${fileList.PRPS_MAST_KEY}',
								'${fileList.PRPS_SEQN}',
								'${fileList.ATTC_SEQN}',
								'${fileList.FILE_PATH}',
								'${fileList.REAL_FILE_NAME}',
								'${fileList.FILE_NAME}',
								'${fileList.TRST_ORGN_CODE}',
								'${fileList.FILE_SIZE}'
								);
			</c:forEach>
		</c:if>		
	}
//-->
</script>
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>
