<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld"%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr"%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권정보 조회(음악) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">

<style type="text/css">
<!--
body {
	overflow-x: auto;
	margin: 0 0 0 0 ; 
}

.box {
	background-color:black;
	position:absolute;
	z-index:9998;
	opacity:0.1;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}

.w88 {width: 88%;}
.w58 {width: 58%;}
.w59 {width: 59%;}
-->
</style>



<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>




<script type="text/javascript"><!--


window.name = "rghtPrps_musc";

//str
jQuery(window).resize(function(e){
	var docuWidth = jQuery(document).width()-16;
	var docuHeight = jQuery(document).height();
	
	jQuery("#modalBox").css({width:docuWidth,height:docuHeight});
	
	   var yp=document.body.scrollTop;
	   var xp=document.body.scrollLeft;

	   var ws=document.body.clientWidth ;
	   var hs=document.body.clientHeight;
	   
	   var ajaxBox=$('ajaxBox');
	   //$('ajaxBoxMent').innerHTML=ment;

	   ajaxBox.style.top=yp+eval(hs)/2-100+"px";
	   ajaxBox.style.left=xp+eval(ws)/2-100+"px";
});
//end


// 로딩 이미지 박스
function showAjaxBox(ment){
	//항상 화면 중앙에 나타나도록...
   var yp=document.body.scrollTop;
   var xp=document.body.scrollLeft;
   

   var ws=document.body.clientWidth;
   var hs=document.body.clientHeight;
   
   if(!ment) ment="잠시만 기다려주세요..";
   
   var ajaxBox=$('ajaxBox');
   //$('ajaxBoxMent').innerHTML=ment;

   ajaxBox.style.top=yp+eval(hs)/2+100+"px";
   ajaxBox.style.left=xp+eval(ws)/2-100+"px";

 //str
	var docuWidth = jQuery(document).width()-16;
	var docuHeight = jQuery(document).height();
	
	//back_blackBox
	/* jQuery("<div>").addClass("box").attr("id","modalBox").
		css({width:docuWidth,height:docuHeight,left:0,top:0}).appendTo("body"); */
	
	jQuery("#modalBox").click(function(){
		jQuery("#modalBox").remove();
	})
	//end
   
  Element.show(ajaxBox);    
}

// 로딩 이미지 박스 감추기
function hideAjaxBox(){
	Element.hide('ajaxBox');	
	//str
	jQuery("#modalBox").remove();
	//end
}

//리사이즈
function resizeIFrame(name) {
  var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
  document.getElementById(name).height= the_height+5;
  
}

// 음악저작물 상세 팝업오픈
function openMusicDetail( crId, nrId, meId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	param += '&NR_ID='+nrId;
	param += '&ALBUM_ID='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=M'+param
	var name = '';
	var openInfo = 'target=rghtPrps_musc, width=705, height=525, height=570, scrollbars=yes';
	
	window.open(url, name, openInfo);
}

function openSmplDetail(div) {

	var param = '';
	
	param = 'DVI='+div;
	
	var url = '/common/rghtPrps_smpl.jsp?'+param
	var name = '';
	var openInfo = 'target=rghtPrps_musc, width=705, height=570, scrollbars=yes';
	
	window.open(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	
	var the_height = document.getElementById(name).offsetHeight;

	//div 크기 제한 str--
	var selChkCnt	= 0;
	if( name == 'listTab') {
		var oSelTable = document.getElementById("listTab");
		var selChkObjs = oSelTable.getElementsByTagName("input");
		for(var sn=0; sn<selChkObjs.length; sn++) {
			if(selChkObjs[sn].name == 'iChkVal') {
				selChkCnt++;
			}
		}
		//행추가
		if(selChkCnt<6){
		document.getElementById(targetName).style.height = the_height + 13 + "px" ;
		}
		//줄이 5보다 클경우 픽셀로 높이 지정(하드코딩) 한줄의 높이를 계산해서 따로 구해 줘야함
		if(selChkCnt>5){
			document.getElementById(targetName).style.height = 375 + "px" ;
		}
	}

	if( name == 'listTab_2') {
		var oSelTable2 = document.getElementById("listTab_2");
		var selChkObjs2 = oSelTable2.getElementsByTagName("input");
		for(var sn=0; sn<selChkObjs2.length; sn++) {
			if(selChkObjs2[sn].name == 'iChkVal') {
				selChkCnt++;
			}
		}
		//행추가
		if(selChkCnt<11){
		document.getElementById(targetName).style.height = the_height + 13 + "px" ;
		}
		//줄이 5보다 클경우 픽셀로 높이 지정(하드코딩) 한줄의 높이를 계산해서 따로 구해 줘야함
		if(selChkCnt>10){
			document.getElementById(targetName).style.height = 345 + "px" ;
		}
	}
	//div 크기 제한 end--
	
}

// 스크롤셋팅
function scrollSet(name, targetName, oRowName){
	
	var totalRow = document.getElementsByName(oRowName).length;
	if( name == 'listTab') {	
		// 세로 기준건수 이상인 경우 세로스크롤 제어
		if( totalRow < 6) {
			resizeDiv(name, targetName);
			document.getElementById(targetName).style.overflowY = "hidden";
		}
		
		if( totalRow > 5 ) {
			resizeDiv(name, targetName);
			document.getElementById(targetName).style.overflowY = "auto";
		}
	}
	
	if( name == 'listTab_2') {	
		// 세로 기준건수 이상인 경우 세로스크롤 제어
		if( totalRow < 11) {
			resizeDiv(name, targetName);
			document.getElementById(targetName).style.overflowY = "hidden";
		}
		
		if( totalRow > 10 ) {
			resizeDiv(name, targetName);
			document.getElementById(targetName).style.overflowY = "auto";
		}
	}
}


// 토탈건수 set
function setTotCnt( oRowName, oSpanId) {

	var totalRow = document.getElementsByName(oRowName).length;
	document.getElementById(oSpanId).innerHTML = "("+totalRow+"건)";
}



// 테이블 행추가/삭제
function editTable(type){
	
	// 1. 신청 목적 선택 먼저 :  앞화면에서 넘어오는 관계로 생략된다.
	
	if ( document.getElementById("PRPS_RGHT_CODE").value == '')  {
		
		alert("신청 목적 선택 후 가능한 기능입니다.");
		
		document.getElementById("PRPS_RGHT_CODE").focus();
		
		return;
	}
	
	
	// 2. 신청 신탁관리단체 먼저
	var trstOrgnLen = document.getElementsByName("TRST_ORGN_CODE").length;
	var kCnt = 0;
	
	for( kk=0; kk<trstOrgnLen && kCnt ==0; kk++) {
		
		if(document.getElementsByName("TRST_ORGN_CODE")[kk].checked){
			kCnt += 1;	break;
		}
	}
	
	if(kCnt==0) {
		alert("권리구분 선택 후 가능한 기능입니다.");
		
		document.getElementsByName("TRST_ORGN_CODE")[0].focus();
		return;
	}

	// 3. tableId setting
	var tableId = "listTab";	//권리자/대리인 권리찾기
	var chkId = "iChk";
	var disSeq = "displaySeq";
	var totalRow = "totalRow";
	var divId = "div_1";
	var scrllDivId = "div_scroll_1";
	
	if(document.getElementById("PRPS_RGHT_CODE").value == '3')	{
		 tableId = "listTab_2";		//이용허락
		 chkId = "iChk2";
		 disSeq = "displaySeq2";
		 totalRow = "totalRow2";
		 divId = "div_2";
		 scrllDivId = "div_scroll_2";
	}
	
	if( type == 'D' ) {
		
		
		if( tableId == 'listTab' ) {
		    var oTbl = document.getElementById(tableId);
		    var oChkDel = document.getElementsByName(chkId);
		    var iChkCnt = oChkDel.length;
		    var iDelCnt = 0;
		    
		    if(iChkCnt == 1 && oChkDel[0].checked == true){
	        oTbl.deleteRow(2);    oTbl.deleteRow(2);
	        iDelCnt++;
		    }else if(iChkCnt > 1){
	    	    for(i = iChkCnt-1; i >= 0; i--){
	    	        if(oChkDel[i].checked == true){            
	    	            oTbl.deleteRow(2*i+2);     
	    	            oTbl.deleteRow(2*i+2);
	    	            iDelCnt++;
	    	        }
	    	    }
	    	}
    	
   	 		if(iDelCnt < 1){
    		    alert('삭제할 저작물을 선택하여 주십시요');
    		}
    	}
    	else{  	
	    	var oTbl = document.getElementById(tableId);
		    var oChkDel = document.getElementsByName(chkId);
		    var iChkCnt = oChkDel.length;
		    var iDelCnt = 0;
		    
		    if(iChkCnt == 1 && oChkDel[0].checked == true){
		        oTbl.deleteRow(1);
		        iDelCnt++;
		    }else if(iChkCnt > 1){
	    	    for(i = iChkCnt-1; i >= 0; i--){
	    	        if(oChkDel[i].checked == true){    	            
	    	            oTbl.deleteRow(i+1);
	    	            iDelCnt++;
	    	        }
	    	    }
	    	}
	    	
	    	if(iDelCnt < 1){
	    	    alert('삭제할 저작물을 선택하여 주십시요');
	    	}
    	}
   
    	fn_resetSeq(disSeq);
	}else if( type == 'I' ) {
		
		newRow = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
		newSubRow = null;
		if( tableId == 'listTab' ) {
			newSubRow = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
		}
		
		newRow.onmouseover=function(){document.getElementById(tableId).clickedRowIndex=this.rowIndex}
		newRow.valign = 'middle';
		newRow.align = 'center';
		makeCell(tableId, newRow, document.getElementById(tableId).rows.length, newSubRow);
	}else if( type == 'A' ) {
		addCell(tableId);
	}
	
	// 스크롤에 의한 height reset
	resizeDiv(tableId, divId);
	scrollSet(tableId, scrllDivId, chkId); 
		
	// 토탈건수
	setTotCnt( chkId, totalRow);
}
	
    
//순번 재지정
function fn_resetSeq(disName){
    var oSeq = document.getElementsByName(disName);
    for(i=0; i<oSeq.length; i++){
        oSeq[i].value = i+1;
    }
}


//추가저작물에 대한 기존저작물 중복확인
function goWorksSearch(worksTitleId, chkId, index){
	
	var worksTitle = document.getElementById(worksTitleId).value;

	if(worksTitle == ''){
		alert('저작물명을 먼저 입력해 주세요.');
		document.getElementById(worksTitleId).focus();
		return;
	} else {
		
		// 중복확인
		document.getElementById(chkId).value = 'Y';	 
		
		// 검색시작
		var ifFrm = document.getElementById("ifMuscRghtSrch").contentWindow.document.ifFrm;
				
		ifFrm.method = "post";
		ifFrm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=M&parentGubun=Y&srchTitle="+worksTitle;
		ifFrm.page_no.value = 1;
		ifFrm.submit();
	}
}


//추가저작물에 대한 기존저작물 중복확인유무 체크
function worksSearchChk(worksTitleId, chkId, index){
	
	var worksTitle = document.getElementById(worksTitleId).value;
	
	if(document.getElementById(chkId).value !='Y') {
		
		alert('추가한 저작물명('+worksTitle+')을 먼저 [검색]해 주세요.');
		
		document.getElementById(worksTitleId).focus();
		return;
	}
}


//선택저작물 테이블 idx
var iRowIdx = 1;
var iRowIdx2 = 1;
       
function makeCell(tableId, cur_row, rowPos, sub_row) {
	
	var i=0;
	var j=0;
	var class_201 = "inputDisible2"; var read_201 = "readonly";
	var class_202 = "inputDisible2";	var read_202 = "readonly";
	var class_203 = "inputDisible2";	var read_203 = "readonly";
	
	// 신청목적에 따라 생성 td 가 다르다.
	var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
	
	//if( document.getElementById("CHK_201").checked || document.getElementById("CHK_205").checked)
	if( document.getElementById("CHK_201").checked ) {
		class_201 = "inputDataN";		read_201 = "";
	}
	
	if( document.getElementById("CHK_202").checked ) {
		class_202 = "inputDataN";		read_202 = "";
	}
	
	if( document.getElementById("CHK_203").checked ) {
		class_203 = "inputDataN";		read_203 = "";
	}
	
	// 권리자/대리인 권리찾기
	if( tableId == 'listTab' ) {
		
		/* 1ROW STR */
		// 체크박스
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		cur_cell.rowSpan = 2;
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk\"  id=\"iChk_'+iRowIdx+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 순번
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce pd5';
		cur_cell.rowSpan = 2;
		innerHTMLStr = '<input name=\"displaySeq\" id=\"displaySeq_'+iRowIdx+'\" type=\"text\" class=\"w75 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작물 명
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5 lft';
		innerHTMLStr = '<input name=\"MUSIC_TITLE\"  id=\"MUSIC_TITLE_'+iRowIdx+'\" class="inputDataN w60" title=\"저작물명\" rangeSize="~100" nullCheck onkeypress=\"javascript:if(event.keyCode==13){goWorksSearch(\'MUSIC_TITLE_'+iRowIdx+'\', \'searchChk_'+iRowIdx+'\',\''+iRowIdx+'\');}\" />';
		// 저작물정보 중복확인 Proc 추가(20101214)
		innerHTMLStr += ' <span class=\"button small type3\" title="저작물명으로 기존저작물정보를 검색합니다." style="margin-top:1px;"><a href=\"javascript:goWorksSearch(\'MUSIC_TITLE_'+iRowIdx+'\', \'searchChk_'+iRowIdx+'\',\''+iRowIdx+'\');\">검색<\/a><\/span>';
		innerHTMLStr += '<input type=\"hidden\" name=\"searchChk\" id=\"searchChk_'+iRowIdx+'\"> ';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 실연시간
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"PERF_TIME\"  id=\"PERF_TIME_'+iRowIdx+'\" class="inputDataN w88" title=\"실연시간\" character="KE"  rangeSize="~12"  />';
		cur_cell.innerHTML = innerHTMLStr;
		
		//cur_cell = cur_row.insertCell(i++);
		//innerHTMLStr = '<input name=\"LYRICS\"  id=\"LYRICS_'+iRowIdx+'\" class="inputDataN w90" title=\"가사\" rangeSize="~500" />';
		//cur_cell.innerHTML = innerHTMLStr;
		
		// 저작권자 201
		//작사
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"LYRICIST\" id=\"LYRICIST_'+iRowIdx+'\" class="'+class_201+' w85" '+read_201+' title=\"작사\" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		//작곡
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"COMPOSER\" id=\"COMPOSER_'+iRowIdx+'\" class="'+class_201+' w85" '+read_201+'  title=\"작곡\" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		//편곡
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"ARRANGER\" id=\"ARRANGER_'+iRowIdx+'\" class="'+class_201+' w85" '+read_201+'  title=\"편곡\" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작인접권자 203
		// 음반제작
		cur_cell = cur_row.insertCell(i++);
		cur_cell.rowSpan = 2;
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"PRODUCER\" id=\"PRODUCER_'+iRowIdx+'\" class="'+class_203+' w85" '+read_203+'  title=\"앨범제작\" rangeSize="~100" />';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="LYRICIST_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="COMPOSER_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="ARRANGER_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="SINGER_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="PLAYER_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="CONDUCTOR_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="PRODUCER_ORGN" />';
		
		innerHTMLStr += ' <!--사용자정보입력확인--> ';
		innerHTMLStr += '<input type="hidden" name="CHK_LYRICIST" />';
		innerHTMLStr += '<input type="hidden" name="CHK_COMPOSER" />';
		innerHTMLStr += '<input type="hidden" name="CHK_ARRANGER" />';
		innerHTMLStr += '<input type="hidden" name="CHK_SINGER" />';
		innerHTMLStr += '<input type="hidden" name="CHK_PLAYER" />';
		innerHTMLStr += '<input type="hidden" name="CHK_CONDUCTOR" />';
		innerHTMLStr += '<input type="hidden" name="CHK_PRODUCER" />';
		
		cur_cell.innerHTML = innerHTMLStr;
		/* 1ROW END  */
		
		/* 2row str */
		//앨범명
		cur_cell = sub_row.insertCell(j++);
		cur_cell.className = 'pd5 lft';
		innerHTMLStr = '<input name=\"ALBUM_TITLE\"  id=\"ALBUM_TITLE_'+iRowIdx+'\" class="inputDataN w95" title=\"앨범명\" rangeSize=\"~100\" nullCheck onfocus=\"javascript:worksSearchChk(\'MUSIC_TITLE_'+iRowIdx+'\', \'searchChk_'+iRowIdx+'\',\''+iRowIdx+'\');\" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		//발매일자
		cur_cell = sub_row.insertCell(j++);
		cur_cell.className = 'pd5';
		//innerHTMLStr = '<input name=\"ISSUED_DATE\"  id=\"ISSUED_DATE_'+iRowIdx+'\" class="inputDataN w90" title=\"발매일\" maxlength="8" rangeSize="~8" character="KE" dateCheck nullCheck />';
		innerHTMLStr = '<input class="inputDataN" type="text" id=\"ISSUED_DATE_'+iRowIdx+'\" name=\"ISSUED_DATE_'+iRowIdx+'\" title=\"발매일\" character=\"KE\" dateCheck nullCheck size="8" maxlength="8" value=""/> ';
		innerHTMLStr += '<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'ISSUED_DATE_'+iRowIdx+'\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'ISSUED_DATE_'+iRowIdx+'\');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>'
		innerHTMLStr += '<input name=\"ISSUED_DATE\" type="hidden" value="'+iRowIdx+'"/>';
		//innerHTMLStr = '<input class="inputDataN w90" type="text" id=\"ISSUED_DATE_'+iRowIdx+'\" name=\"ISSUED_DATEqwe\" size="8" maxlength="8" value=""/>';
		//innerHTMLStr += '<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'ISSUED_DATEqwe\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'ISSUED_DATEqwe\');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>'
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작인접권자 202
		//가창
		cur_cell = sub_row.insertCell(j++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"SINGER\" id=\"SINGER_'+iRowIdx+'\" class="'+class_202+' w85" '+read_202+'  title=\"가창\" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		//연주
		cur_cell = sub_row.insertCell(j++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"PLAYER\" id=\"PLAYER_'+iRowIdx+'\" class="'+class_202+' w85" '+read_202+'  title=\"연주\" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		//지휘
		cur_cell = sub_row.insertCell(j++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"CONDUCTOR\" id=\"CONDUCTOR_'+iRowIdx+'\" class="'+class_202+' w85" '+read_202+'  title=\"지휘\" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		/* 2row end */
		
		fn_resetSeq("displaySeq");
		
		// setfocus
		//var oFuc = 'MUSIC_TITLE_'+iRowIdx;
		//	document.getElementById(oFuc).focus();
		
		iRowIdx++;
	}
	
	// 이용자 권리조회
	else if( tableId == 'listTab_2' ) {
	//alert(iRowIdx2);
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className ='pd5';
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk2\"  id=\"iChk2_'+iRowIdx2+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce pd5';
		innerHTMLStr = '<input name=\"displaySeq2\" id=\"displaySeq2_'+iRowIdx2+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작물 정보
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5 lft';
		innerHTMLStr = '<input name=\"MUSIC_TITLE\"  id=\"MUSIC_TITLE_'+iRowIdx2+'\" class=\"inputDataN w75\" title=\"저작물명\" rangeSize="~100" nullCheck onkeypress=\"javascript:if(event.keyCode==13){goWorksSearch(\'MUSIC_TITLE_'+iRowIdx2+'\', \'searchChk_'+iRowIdx2+'\',\''+iRowIdx2+'\');}\" />';
		// 저작물정보 중복확인 Proc 추가(20101214)
		innerHTMLStr += ' <span class=\"button small type3\" title="저작물명으로 기존저작물정보를 검색합니다." style="margin-top:1px;"><a href=\"javascript:goWorksSearch(\'MUSIC_TITLE_'+iRowIdx2+'\', \'searchChk_'+iRowIdx2+'\',\''+iRowIdx2+'\');\">검색<\/a><\/span>';
		innerHTMLStr += '<input type=\"hidden\" name=\"searchChk\" id=\"searchChk_'+iRowIdx2+'\"> ';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"ALBUM_TITLE\" id=\"ALBUM_TITLE_'+iRowIdx2+'\" class="inputDataN w95" title=\"앨범명\" rangeSize="~100" nullCheck onfocus=\"javascript:worksSearchChk(\'MUSIC_TITLE_'+iRowIdx2+'\', \'searchChk_'+iRowIdx2+'\',\''+iRowIdx2+'\');\" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'pd5';
		innerHTMLStr = '<input name=\"PERF_TIME\"  id=\"PERF_TIME_'+iRowIdx2+'\" class="inputDataN w90" title=\"실연시간\" character="KE"  rangeSize="~12"  />';
		cur_cell.innerHTML = innerHTMLStr;
		
		//cur_cell = cur_row.insertCell(i++);
		//innerHTMLStr = '<input name=\"LYRICS\"  id=\"LYRICS_'+iRowIdx2+'\" class="inputDataN w90" title=\"가사\" rangeSize="~500" />';
		//cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce pd5';
		//innerHTMLStr = '<input name=\"ISSUED_DATE\"  id=\"ISSUED_DATE_'+iRowIdx2+'\" class="inputDataN w90" title=\"발매일\" maxlength="8" rangeSize="~8" character="KE" dateCheck nullCheck />';
		innerHTMLStr = '<input class="inputDataN w65" type="text" id=\"VAL_ISSUED_DATE_'+iRowIdx2+'\" name=\"VAL_ISSUED_DATE_'+iRowIdx2+'\" title=\"발매일\" maxlength=\"8\" rangeSize=\"~8\" character=\"KE\" dateCheck nullCheck value=\"\"/> ';
		innerHTMLStr += '<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'VAL_ISSUED_DATE_'+iRowIdx2+'\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'VAL_ISSUED_DATE_'+iRowIdx2+'\');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>'
		innerHTMLStr += '<input name=\"VAL_ISSUED_DATE\" type="hidden" class="inputDataN w90" value="'+iRowIdx2+'"/>';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="LYRICIST" />';
		innerHTMLStr += '<input type="hidden" name="COMPOSER" />';
		innerHTMLStr += '<input type="hidden" name="ARRANGER" />';
		innerHTMLStr += '<input type="hidden" name="SINGER" />';
		innerHTMLStr += '<input type="hidden" name="PLAYER" />';
		innerHTMLStr += '<input type="hidden" name="CONDUCTOR" />';
		innerHTMLStr += '<input type="hidden" name="PRODUCER" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		fn_resetSeq("displaySeq2");
		
		// setfocus
		//	var oFuc = 'MUSIC_TITLE_'+iRowIdx2;
		//	document.getElementById(oFuc).focus();
		
		iRowIdx2++;
		
	}
}


function addCell(tableId) {

	//iFrame 항목
	var albumTitleObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumTitle");
	var issuedDateObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("issuedDate");
	var lyricistObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("lyricist");
	var composerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("composer");
	var singerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("singer");
	var producerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("producer");
	var chkObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("ifrmChk");
	var crIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("crId");
	var nrIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("nrId");
	var albumIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumId");
	var arrangerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("arranger");
	var translatorObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("translator");
	var musicTitleObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("musicTitle");
	var playerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("player");
	var conductorObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("conductor");
	var featuringObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("featuring");
	var icnNumbObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("icnNumb");
	var albumProducedCrhObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumProducedCrh");
	var perfTimeObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("perfTime");

	// 선택 목록
	var oSelTable = document.getElementById("listTab");
	var oSelTable2 = document.getElementById("listTab_2");

	var selChkObjs = oSelTable.getElementsByTagName("input");
	var selChkObjs2 = oSelTable2.getElementsByTagName("input");

	var selChkCnt = 0;
	var selChkCnt2 = 0;
	
	var isExistYn = "N";	//중복여부	
	var isAdd	= false;	//줄 추가여부

	var count = 0;
	
	for(var cn = 0; cn < chkObjs.length; cn++) {
		if(chkObjs[cn].checked == true){

			if( tableId == 'listTab') {  // 권리자 권리찾기 
				//중복여부 검사
				for(var sn=0; sn<selChkObjs.length; sn++) {
					if(chkObjs[cn].checked == true && selChkObjs[sn].name == 'iChkVal') {
						if((crIdObjs[cn].value +'|'+ nrIdObjs[cn].value +'|'+ albumIdObjs[cn].value) == selChkObjs[sn].value) {
							isExistYn = "Y";
						}
						selChkCnt++;
					}
				}
			}

			if( tableId == 'listTab_2') {  // 이용자 권리조회 
				//중복여부 검사
				for(var sn=0; sn<selChkObjs2.length; sn++) {
					if(chkObjs[cn].checked == true && selChkObjs2[sn].name == 'iChkVal') {
						if((crIdObjs[cn].value +'|'+ nrIdObjs[cn].value +'|'+ albumIdObjs[cn].value) == selChkObjs2[sn].value) {
							isExistYn = "Y";
						}
					}
					selChkCnt2++;
				}
			}
			
			if(isExistYn == "N") {
				//줄 추가여부
				isAdd = true;
				count ++;
			}else{
				alert("선택된 저작물 ["+musicTitleObjs[cn].value+"]는 이미 추가된 저작물입니다.");
				return;
			}
			

			if( tableId == 'listTab' && selChkCnt > 0) { //권리찾기 선택목록이 비웠을때 
				//줄 추가여부		
				isAdd = true;
				count ++;
			}

			if( tableId == 'listTab_2' && selChkCnt2 > 0) { //권리찾기 선택목록이 비웠을때 
				//줄 추가여부		
				isAdd = true;
				count ++;
			}
		}

		chkObjs[cn].checked = false;  // 처리한 후 체크풀기 


		//줄 추가실행
		if(isAdd){
			var cur_row = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
			cur_row.onmouseover=function(){document.getElementById(tableId).clickedRowIndex=this.rowIndex}
			cur_row.valign = 'middle';
			cur_row.align = 'left';
			
			var rowPos = document.getElementById(tableId).rows.length;
			
			var i=0;
			
			var class_201 = "inputDisible2"; var read_201 = "readonly";
			var class_202 = "inputDisible2";	var read_202 = "readonly";
			var class_203 = "inputDisible2";	var read_203 = "readonly";
			
			// 신청목적에 따라 생성 td 가 다르다.
			var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
			//if( document.getElementById("CHK_201").checked || document.getElementById("CHK_205").checked)	{
			if( document.getElementById("CHK_201").checked )	{
				class_201 = "inputDataN";		read_201 = "";
				
			}

			if( document.getElementById("CHK_202").checked ) {
				class_202 = "inputDataN";		read_202 = "";
			}

			if( document.getElementById("CHK_203").checked ) {
				class_203 = "inputDataN";		read_203 = "";
			}
			
			// 권리자/대리인 권리찾기
			if( tableId == 'listTab' ) {
				
				var cur_row2 = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
				var j=0;
				
				//체크박스
				cur_cell = cur_row.insertCell(i++);
				cur_cell.rowSpan = 2;
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = '<input type=\"checkbox\" name=\"iChk\" id=\"iChk_'+iRowIdx+'\"class="vmid" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				//순번
				cur_cell = cur_row.insertCell(i++);
				cur_cell.rowSpan = 2;
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"displaySeq\" id=\"displaySeq_'+iRowIdx+'\" type=\"text\" class=\"w75 ce\" style=\"border:0px;\" readonly="readonly" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				// 저작물 명
				cur_cell = cur_row.insertCell(i++);
				cur_cell.style.padding = 5;
				innerHTMLStr = '<a class=\"underline black2\" href=\"javascript:openMusicDetail(\''+crIdObjs[cn].value+'\',\''+nrIdObjs[cn].value+'\',\''+albumIdObjs[cn].value+'\');\">'+musicTitleObjs[cn].value+'<\/a>';
				cur_cell.className = '';
				cur_cell.innerHTML = innerHTMLStr;
				
				//실연시간
				cur_cell = cur_row.insertCell(i++);
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = perfTimeObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;

				// 저작권자 201
				cur_cell = cur_row.insertCell(i++);
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"LYRICIST\" id=\"LYRICIST_'+iRowIdx+'\" class=\"'+class_201+' w85\" '+read_201+' value=\"'+fncReplaceStr(lyricistObjs[cn].value, '"', '&quot;')+'\" title=\"작사\" rangeSize=\"~100\"  />';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"COMPOSER\" id=\"COMPOSER_'+iRowIdx+'\" class=\"'+class_201+' w85\" '+read_201+' value=\"'+fncReplaceStr(composerObjs[cn].value, '"', '&quot;')+'\" title=\"작곡\" rangeSize=\"~100\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"ARRANGER\" id=\"ARRANGER_'+iRowIdx+'\" class=\"'+class_201+' w85\" '+read_201+' value=\"'+fncReplaceStr(arrangerObjs[cn].value, '"', '&quot;')+'\" title=\"편곡\" rangeSize=\"~100\" />';
				cur_cell.innerHTML = innerHTMLStr;

				// 저작인접권자 203
				cur_cell = cur_row.insertCell(i++);
				cur_cell.rowSpan = 2;
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"PRODUCER\" id=\"PRODUCER_'+iRowIdx+'\" class=\"'+class_203+' w85\" '+read_203+' value=\"'+fncReplaceStr(producerObjs[cn].value, '"', '&quot;')+'\" title=\"앨범제작\" rangeSize=\"~100\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"iChkVal\" value=\"'+crIdObjs[cn].value+'|'+nrIdObjs[cn].value+'|'+albumIdObjs[cn].value+'\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"MUSIC_TITLE\"  value=\"'+fncReplaceStr(musicTitleObjs[cn].value, '"', '&quot;')+'\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"ALBUM_TITLE\" value=\"'+fncReplaceStr(albumTitleObjs[cn].value, '"', '&quot;')+'\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"PERF_TIME\" value=\"'+perfTimeObjs[cn].value+'\" />';
				innerHTMLStr += '<input type=\"hidden\" id=\"ISSUED_DATE_'+iRowIdx+'\" name=\"ISSUED_DATE_'+iRowIdx+'\" value=\"'+issuedDateObjs[cn].value+'\" />';
				innerHTMLStr += '<input name=\"ISSUED_DATE\" type="hidden" value="'+iRowIdx+'"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"LYRICS\" value=\"\" />';
				
				innerHTMLStr += '<input type=\"hidden\" name=\"LYRICIST_ORGN\" value=\"'+fncReplaceStr(lyricistObjs[cn].value, '"', '&quot;')+'\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"COMPOSER_ORGN\" value=\"'+fncReplaceStr(composerObjs[cn].value, '"', '&quot;')+'\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"ARRANGER_ORGN\" value=\"'+fncReplaceStr(arrangerObjs[cn].value, '"', '&quot;')+'\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"SINGER_ORGN\" value=\"'+fncReplaceStr(singerObjs[cn].value, '"', '&quot;')+'\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"PLAYER_ORGN\" value=\"'+fncReplaceStr(playerObjs[cn].value, '"', '&quot;')+'\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"CONDUCTOR_ORGN\" value=\"'+fncReplaceStr(conductorObjs[cn].value, '"', '&quot;')+'\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"PRODUCER_ORGN\" value=\"'+fncReplaceStr(producerObjs[cn].value, '"', '&quot;')+'\" />';

				innerHTMLStr += ' <!--사용자정보입력확인--> ';
				innerHTMLStr += '<input type="hidden" name="CHK_LYRICIST" />';
				innerHTMLStr += '<input type="hidden" name="CHK_COMPOSER" />';
				innerHTMLStr += '<input type="hidden" name="CHK_ARRANGER" />';
				innerHTMLStr += '<input type="hidden" name="CHK_SINGER" />';
				innerHTMLStr += '<input type="hidden" name="CHK_PLAYER" />';
				innerHTMLStr += '<input type="hidden" name="CHK_CONDUCTOR" />';
				innerHTMLStr += '<input type="hidden" name="CHK_PRODUCER" />';

				cur_cell.innerHTML = innerHTMLStr;


				//앨범명
				cur_cell = cur_row2.insertCell(j++);
				cur_cell.style.padding = 5;
				innerHTMLStr = albumTitleObjs[cn].value;
				cur_cell.className = 'lft';
				cur_cell.innerHTML = innerHTMLStr;
				
				//발매일
				cur_cell = cur_row2.insertCell(j++);
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = issuedDateObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				//cur_cell = cur_row.insertCell(i++);
				//innerHTMLStr = '';
				//cur_cell.innerHTML = innerHTMLStr;
				
				
				// 저작인접권자 202
				cur_cell = cur_row2.insertCell(j++);
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"SINGER\" id=\"SINGER_'+iRowIdx+'\" class=\"'+class_202+' w85\" '+read_202+' value=\"'+fncReplaceStr(singerObjs[cn].value, '"', '&quot;')+'\" title=\"가창\" rangeSize=\"~100\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row2.insertCell(j++);
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"PLAYER\" id=\"PLAYER_'+iRowIdx+'\" class=\"'+class_202+' w85\" '+read_202+' value=\"'+fncReplaceStr(playerObjs[cn].value, '"', '&quot;')+'\" title=\"연주\" rangeSize=\"~100\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row2.insertCell(j++);
				cur_cell.style.padding = 5;
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"CONDUCTOR\" id=\"CONDUCTOR_'+iRowIdx+'\" class=\"'+class_202+' w85\" '+read_202+' value=\"'+fncReplaceStr(conductorObjs[cn].value, '"', '&quot;')+'\" title=\"지휘\" rangeSize=\"~100\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				
				
				fn_resetSeq("displaySeq");
				
				// setfocus
				//var oFuc = 'MUSIC_TITLE_'+iRowIdx;
				//	document.getElementById(oFuc).focus();
				
				iRowIdx++;
			}
			
			// 이용자 권리조회
			else if( tableId == 'listTab_2' ) {
			//alert(iRowIdx2);
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = '<input type=\"checkbox\" name=\"iChk2\"  id=\"iChk2_'+iRowIdx2+'\"class="vmid" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = '<input name=\"displaySeq2\" id=\"displaySeq2_'+iRowIdx2+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				// 저작물 정보
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'pd5';
				innerHTMLStr = '<a class=\"underline black2\" href=\"javascript:openMusicDetail(\''+crIdObjs[cn].value+'\',\''+nrIdObjs[cn].value+'\',\''+albumIdObjs[cn].value+'\');\">'+musicTitleObjs[cn].value+'<\/a>';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'pd5';
				innerHTMLStr = albumTitleObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'pd5 ce';
				innerHTMLStr = perfTimeObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce pd5';
				innerHTMLStr = issuedDateObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				//cur_cell = cur_row.insertCell(i++);
				innerHTMLStr += '<input type=\"hidden\" name=\"iChkVal\" value=\"'+crIdObjs[cn].value+'|'+nrIdObjs[cn].value+'|'+albumIdObjs[cn].value+'\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"MUSIC_TITLE\" value=\"'+fncReplaceStr(musicTitleObjs[cn].value, '"', '&quot;')+'\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"ALBUM_TITLE\" value=\"'+fncReplaceStr(albumTitleObjs[cn].value, '"', '&quot;')+'\"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"PERF_TIME\"  value=\"'+perfTimeObjs[cn].value+'\"/>';
				innerHTMLStr += '<input type=\"hidden\" id=\"VAL_ISSUED_DATE_'+iRowIdx2+'\" name=\"VAL_ISSUED_DATE_'+iRowIdx2+'\" value=\"'+issuedDateObjs[cn].value+'\"/>';
				innerHTMLStr += '<input name=\"VAL_ISSUED_DATE\" type="hidden" value="'+iRowIdx+'"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"LYRICS\" value=\"\"/>';
				
				innerHTMLStr += '<input type=\"hidden\" name=\"LYRICIST\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"COMPOSER\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"ARRANGER\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"SINGER\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"PLAYER\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"CONDUCTOR\" />';
				innerHTMLStr += '<input type=\"hidden\" name=\"PRODUCER\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				fn_resetSeq("displaySeq2");
				
				// setfocus
				//	var oFuc = 'MUSIC_TITLE_'+iRowIdx2;
				//	document.getElementById(oFuc).focus();
				
				iRowIdx2++;
				
			}
			isAdd = false;
		}
	}

	if(count == 0){
		alert('선택된 저작물이 없습니다.');
		return;
	}	
	
}

// 신청목적 선택
function prps_check(chk) {

	// 권리자 권리찾기
	if(chk == '1' || chk == '2' ) {
		document.getElementById("div_1").style.display = "";
		document.getElementById("div_2").style.display = "none";

		fn_lock( "listTab", "" );
		fn_lock( "listTab_2", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab", "div_scroll_1", "iChk");
		
		// 토탈건수
		setTotCnt( "iChk", "totalRow");
		
	} else {
		document.getElementById("div_2").style.display = "";
		document.getElementById("div_1").style.display = "none";
		
		fn_lock( "listTab_2", "" );
		fn_lock( "listTab", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab_2", "div_scroll_2", "iChk2");
		
		// 토탈건수
		setTotCnt( "iChk2", "totalRow2");
		
	}

}

//신청목적 선택
function prps_check2() {

	// 권리자 권리찾기
	if(document.getElementById("PRPS_RGHT_CODE2").value == '1' || document.getElementById("PRPS_RGHT_CODE2").value == '2' ) {
		document.getElementById("div_1").style.display = "";
		document.getElementById("div_2").style.display = "none";

		fn_lock( "listTab", "" );
		fn_lock( "listTab_2", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab", "div_scroll_1", "iChk");
		
		// 토탈건수
		setTotCnt( "iChk", "totalRow");
		
	} else if(document.getElementById("PRPS_RGHT_CODE2").value == '3'){
		document.getElementById("div_2").style.display = "";
		document.getElementById("div_1").style.display = "none";

		fn_lock( "listTab_2", "" );
		fn_lock( "listTab", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab_2", "div_scroll_2", "iChk2");
		
		// 토탈건수
		setTotCnt( "iChk2", "totalRow2");
		
	}

	if(document.getElementById("OFFX_LINE_RECP2").value == 'Y' ) {
		document.getElementById("td_file_no").style.display = "";
		document.getElementById("td_file_yes").style.display = "none";
	} else{
		document.getElementById("td_file_no").style.display = "none";
		document.getElementById("td_file_yes").style.display = "";
	} 

}


// 신탁관리단체 선택
function trust_check(chk) {
	
	// 보상금동시신청 체크박스 셋팅
	if( document.getElementById("PRPS_DOBL_CODE") != null) {
	
		if( document.getElementById("CHK_201").checked && !document.getElementById("CHK_202").checked  && !document.getElementById("CHK_203").checked){
			document.getElementById("PRPS_DOBL_CODE").disabled = true;
			
		}else{
			document.getElementById("PRPS_DOBL_CODE").disabled = false;
		}
	}

	var nRow = document.getElementsByName("LYRICIST").length;	// 전체 길이
	
	var oInLyr = document.getElementsByName("LYRICIST");
	var oInCom = document.getElementsByName("COMPOSER");
	var oInArr = document.getElementsByName("ARRANGER");

	var oInSin = document.getElementsByName("SINGER");
	var oInPla = document.getElementsByName("PLAYER");
	var oInCon = document.getElementsByName("CONDUCTOR");
	var oInPro = document.getElementsByName("PRODUCER");
	
		
	for( k=0; k<nRow; k++) {
	
		// 음저협	 ||  복전협
		//if( chk.id=="CHK_201" || chk.id=="CHK_205") {
		if( chk.id=="CHK_201" ) {
			
			if(!chk.checked) {
				oInLyr[k].readOnly = true;
				oInCom[k].readOnly = true;
				oInArr[k].readOnly = true;
				
															
				oInLyr[k].className = 'inputDisible2 w85';
				oInCom[k].className = 'inputDisible2 w85'; 
				oInArr[k].className = 'inputDisible2 w85';
			} else {
				oInLyr[k].readOnly = false;
				oInCom[k].readOnly = false;
				oInArr[k].readOnly = false;

				oInLyr[k].className = 'inputDataN w85';
				oInCom[k].className = 'inputDataN w85';
				oInArr[k].className = 'inputDataN w85';
			}
		}
		
		// 음실연	
		if( chk.id=="CHK_202" ) {
			
			if(!chk.checked) {
				oInSin[k].readOnly = true;
				oInPla[k].readOnly = true;
				oInCon[k].readOnly = true;
				
				oInSin[k].className = 'inputDisible2 w85';
				oInPla[k].className = 'inputDisible2 w85';
				oInCon[k].className = 'inputDisible2 w85';
			} else {
				oInSin[k].readOnly = false;
				oInPla[k].readOnly = false;
				oInCon[k].readOnly = false;

				oInSin[k].className = 'inputDataN w85';
				oInPla[k].className = 'inputDataN w85';
				oInCon[k].className = 'inputDataN w85';
			}
		}
		
		// 음제협
		if( chk.id=="CHK_203" ) {
			
			if(!chk.checked) {
				oInPro[k].readOnly = true;
				oInPro[k].className = 'inputDisible2 w85';
			} else {
				oInPro[k].readOnly = false;
				oInPro[k].className = 'inputDataN w85';
			}
		}
		
	} // .. end for
}

// 권리찾기신청
function rghtPrps() {

	var frm = document.prpsForm;
	
	var oFldInmt	= document.getElementById("fldInmtInfo");
	var oInmt	= oFldInmt.getElementsByTagName("input");
	
	var txtHomeAddr	= ""; var txtBusiAddr	= "";
	var txtHomeTelxNumb	= "";  var txtBusiTelxNumb	= "";  var txtMoblPhon	= "";  var txtFaxxNumb	= "";  var txtMail = "";
	
	
	txtHomeAddr	= document.getElementsByName("HOME_ADDR")[0].value;
	txtBusiAddr	= document.getElementsByName("BUSI_ADDR")[0].value;
	

	txtHomeTelxNumb	= document.getElementsByName("HOME_TELX_NUMB")[0].value;
	txtBusiTelxNumb	= document.getElementsByName("BUSI_TELX_NUMB")[0].value;
	txtMoblPhon	= document.getElementsByName("MOBL_PHON")[0].value;
	txtFaxxNumb	= document.getElementsByName("FAXX_NUMB")[0].value;
	txtMail	= document.getElementsByName("MAIL")[0].value;
	
	
	for(i = 0; i < oInmt.length; i++){
		/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
		if(checkField(oInmt[i]) == false){
			return;
		}
		*/
		
		// 전화번호
		if(oInmt[i].name == 'HOME_TELX_NUMB'
			|| oInmt[i].name == 'BUSI_TELX_NUMB'
			|| oInmt[i].name == 'MOBL_PHON'
			|| oInmt[i].name == 'FAXX_NUMB'){
			
			if(txtHomeTelxNumb == ''){
				if(txtBusiTelxNumb == ''){
					if(txtMoblPhon == ''){
						if(txtFaxxNumb == ''){
							if(!nullCheck(oInmt[i]))	return;
						}
					}
				}
			}
			
			if(!character(oInmt[i],  'EK'))	return;
		}
		
		// 자택 사무실 주소 nullCheck
		if(oInmt[i].name == 'HOME_ADDR'
			|| oInmt[i].name == 'BUSI_ADDR'){
			if(txtHomeAddr == ''){
				if(txtBusiAddr == ''){
					if(!nullCheck(oInmt[i]))	return;
				}	
			}
		}
		
		// E-mail
		if(oInmt[i].name == 'MAIL'){
			if(!character(oInmt[i],  'K'))	return;
		}
	}
			
	
	var trstOrgnLen = document.getElementsByName("TRST_ORGN_CODE").length;
	var kCnt = 0;
	
	for( kk=0; kk<trstOrgnLen && kCnt ==0; kk++) {
		
		if(document.getElementsByName("TRST_ORGN_CODE")[kk].checked){
			kCnt += 1;	break;
		}
	}
	
	if(kCnt==0) {
		alert('권리구분을(를) 선택하세요.');
		
		document.getElementsByName("TRST_ORGN_CODE")[0].focus();
		return;
	}
	
	
	/*
	if( !document.getElementById("CHK_201").checked &&  !document.getElementById("CHK_202").checked && !document.getElementById("CHK_203").checked){
		alert('권리구분을(를) 선택하세요.');
		document.getElementById("CHK_201").focus();
		return;
	}
	*/
	<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">
	if( document.getElementsByName("iChk").length <1 ){
		alert('신청 저작물(를) 추가하세요.');
		document.getElementById("workAdd").focus();
		return;
	}
	</c:if>
	
	<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">
	if( document.getElementsByName("iChk2").length <1 ){
		alert('신청 저작물(를) 추가하세요.');
		document.getElementById("workAdd2").focus();
		return;
	}
	</c:if>

	
	if(checkForm(frm)) {
		
		// 신청자 정보입력 확인 : 기존 정보/신청정보 동일유무 확인 (권리자 권리찾기인 경우만)
		if( frm.PRPS_RGHT_CODE.value == '1') {
			if( !dataConfirmcheck())
				return;
		}
		// 첨부파일 추가하기.
		//resort();
		
		frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrpsProcDetl&DIVS=M";
		
		frm.method="post";
		frm.submit();

	}
}

// 신청자 정보입력 확인
function dataConfirmcheck() {
	
	var mesg;
	
	var reVal = true;
	
	var oTitle = document.getElementsByName("MUSIC_TITLE");
	var oFoc = document.getElementsByName("iChk");
	
	// 확인여부
	/*
	var oChkLyr = document.getElementsByName("CHK_LYRICIST");
	var oChkCom = document.getElementsByName("CHK_COMPOSER");
	var oChkArr = document.getElementsByName("CHK_ARRANGER");
	var oChkSin = document.getElementsByName("CHK_SINGER");
	var oChkPla = document.getElementsByName("CHK_PLAYER");
	var oChkCon = document.getElementsByName("CHK_CONDUCTOR");
	var oChkPro = document.getElementsByName("CHK_PRODUCER");
	*/
	
	// 신청정보
	var oLyr = document.getElementsByName("LYRICIST");
	var oCom = document.getElementsByName("COMPOSER");
	var oArr = document.getElementsByName("ARRANGER");
	var oSin = document.getElementsByName("SINGER");
	var oPla = document.getElementsByName("PLAYER");
	var oCon = document.getElementsByName("CONDUCTOR");
	var oPro = document.getElementsByName("PRODUCER");
	
	// 기존정보
	var oLyrOrgn = document.getElementsByName("LYRICIST_ORGN");
	var oComOrgn = document.getElementsByName("COMPOSER_ORGN");
	var oArrOrgn = document.getElementsByName("ARRANGER_ORGN");
	var oSinOrgn = document.getElementsByName("SINGER_ORGN");
	var oPlaOrgn = document.getElementsByName("PLAYER_ORGN");
	var oConOrgn = document.getElementsByName("CONDUCTOR_ORGN");
	var oProOrgn = document.getElementsByName("PRODUCER_ORGN");
	
	var nRow =oFoc.length;	// 전체 길이

	for( k=0; k<nRow; k++) {
		
		var totCnt =0;
		//if( document.getElementById("CHK_201").checked || document.getElementById("CHK_205").checked) {
		if( document.getElementById("CHK_201").checked ) {	
			// 작사
			totCnt += dataConfirm2( oLyr[k], oLyrOrgn[k]);
			
			// 작곡
			totCnt += dataConfirm2( oCom[k], oComOrgn[k]) ;
			
			// 편곡
			totCnt += dataConfirm2( oArr[k], oArrOrgn[k]) ;
		}
		
		if( document.getElementById("CHK_202").checked ) {
			
			// 가창
			totCnt += dataConfirm2( oSin[k], oSinOrgn[k]) ;
			
			// 연주
			totCnt += dataConfirm2( oPla[k], oPlaOrgn[k]) ;
			
			// 지휘
			totCnt += dataConfirm2( oCon[k], oConOrgn[k]) ;
		}
		
		if( document.getElementById("CHK_203").checked){
			// 음반제작
			totCnt += dataConfirm2( oPro[k], oProOrgn[k]) ;
		}
		
		// 수정된 정보가 없다면.
		if(totCnt == 0){
			alert( "신청 저작물정보 ["+oTitle[k].value+"] 권리정보가 입력 또는 변경되지 않았습니다. \n해당저작물을 삭제하거나 권리정보를 변경해주세요.");
			oFoc[k].focus();	reVal = false;
			return;
		}
	}
	return reVal;
}

// 진행의 경우 return true
function dataConfirm2( oNew, oOrg){
	
	var reVal = 0;
	var mesg = oNew.title;
		
	if( oNew.value == oOrg.value || oNew.value == '') {
	
		reVal = 0
	
	} else {
		reVal = 1;
	}
	
	return reVal;
}

/*
// 한 항목에 대한 권리정보 수정 여부확인
function dataConfirmcheck() {
	
	var mesg;
	var oFoc;
	var reVal = true;
	
	var oTitle = document.getElementsByName("MUSIC_TITLE");
	
	// 확인여부
	var oChkLyr = document.getElementsByName("CHK_LYRICIST");
	var oChkCom = document.getElementsByName("CHK_COMPOSER");
	var oChkArr = document.getElementsByName("CHK_ARRANGER");
	var oChkSin = document.getElementsByName("CHK_SINGER");
	var oChkPla = document.getElementsByName("CHK_PLAYER");
	var oChkCon = document.getElementsByName("CHK_CONDUCTOR");
	var oChkPro = document.getElementsByName("CHK_PRODUCER");
	
	// 신청정보
	var oLyr = document.getElementsByName("LYRICIST");
	var oCom = document.getElementsByName("COMPOSER");
	var oArr = document.getElementsByName("ARRANGER");
	var oSin = document.getElementsByName("SINGER");
	var oPla = document.getElementsByName("PLAYER");
	var oCon = document.getElementsByName("CONDUCTOR");
	var oPro = document.getElementsByName("PRODUCER");
	
	// 기존정보
	var oLyrOrgn = document.getElementsByName("LYRICIST_ORGN");
	var oComOrgn = document.getElementsByName("COMPOSER_ORGN");
	var oArrOrgn = document.getElementsByName("ARRANGER_ORGN");
	var oSinOrgn = document.getElementsByName("SINGER_ORGN");
	var oPlaOrgn = document.getElementsByName("PLAYER_ORGN");
	var oConOrgn = document.getElementsByName("CONDUCTOR_ORGN");
	var oProOrgn = document.getElementsByName("PRODUCER_ORGN");
	
	var nRow =oChkLyr.length;	// 전체 길이

	for( k=0; k<nRow; k++) {
		
		if( document.getElementById("CHK_201").checked ) {
			
			// 작사
			if(! dataConfirm2( oTitle[k], oChkLyr[k], oLyr[k], oLyrOrgn[k])) {
					oFoc = oLyr[k];  reVal = false;  break;
			}
			
			// 작곡
			if(! dataConfirm2( oTitle[k], oChkCom[k], oCom[k], oComOrgn[k])) {
					oFoc = oCom[k];  reVal = false;	break;
			}
			
			// 편곡
			if(! dataConfirm2( oTitle[k], oChkArr[k], oArr[k], oArrOrgn[k])) {
					oFoc = oArr[k];  reVal = false;	break;
			}
		}
		
		if( document.getElementById("CHK_202").checked ) {
			
			// 가창
			if(! dataConfirm2( oTitle[k], oChkSin[k], oSin[k], oSinOrgn[k])) {
					oFoc = oSin[k];  reVal = false;	break;
			}
			
			// 연주
			if(! dataConfirm2( oTitle[k], oChkPla[k], oPla[k], oPlaOrgn[k])) {
					oFoc = oPla[k];  reVal = false;	break;
			}
			
			// 지휘
			if(! dataConfirm2( oTitle[k], oChkCon[k], oCon[k], oConOrgn[k])) {
					oFoc = oCon[k];  reVal = false;	break;
			}
		}
		
		if( document.getElementById("CHK_203").checked){
			// 음반제작
			if(! dataConfirm2( oTitle[k], oChkPro[k], oPro[k], oProOrgn[k])) {
					oFoc = oPro[k];  reVal = false;	break;
			}
		}
	}
	
	if(!reVal)
		oFoc.focus();
		
	return reVal;
}

// 진행의 경우 return true
function dataConfirm( oTitle, oChk, oNew, oOrg){
	
	var reVal = true;
	var mesg = oNew.title;
		
	if( oChk.value != 'Y' ) {
		
		if( oNew.value == oOrg.value ) {

			var conf = confirm( "신청 저작물정보 ["+oTitle.value+"-"+mesg+"](이)가 변경되지 않았습니다. \n계속 진행하시겠습니까?");
			
			if(conf ) 
				oChk.value = 'Y';
			else 
				reVal =  false;
	
		} else {
			oChk.value = 'Y';
		}
	}
	
	return reVal;
}

*/

// 테이블 하위 disable
function fn_lock( tableId, flag ){
	var oTbl = document.getElementById(tableId);
	var oInput = oTbl.getElementsByTagName("input");
	
	for(i=0; i<oInput.length;i++){
			oInput[i].disabled= flag ;
	}
}

// 오프라인 접수
function offLine_check(chk) {
	
	if(chk.checked) {
		document.getElementById("td_file_no").style.display = "";
		document.getElementById("td_file_yes").style.display = "none";
	} else{
		document.getElementById("td_file_no").style.display = "none";
		document.getElementById("td_file_yes").style.display = "";
	} 
}

// 목록 
function goList(){
	
	var listDivs = '${listDivs}';
	
	var frm = document.srchForm;
	
	// 접근가능한 목록으로 보낸다.
	if(listDivs == 'noneRghtList')
		frm.action = '/noneRght/rghtSrch.do?DIVS=M';
	else
		frm.action = '/rghtPrps/rghtSrch.do?DIVS=M';
		
	frm.method="post";
	frm.submit();
}

function showAttach(cnt) {
	var i=0;
	var content = "";
	var attach = document.getElementById("attach")
	attach.innerHTML = "";
	//document.all("attach").innerHTML = "";
	content += '<table>';
	for (i=0; i<cnt; i++) {
		content += '<tr>';
		content += '<td><input type="file" name="attachfile'+(i+1)+'" size="70;" class="inputFile"><\/td>';
		content += '<\/tr>';
	}
	content += '<\/table>';
	//document.all("attach").innerHTML = content;
	attach.innerHTML = content;
}

function applyAtch() {
	var ansCnt = document.prpsForm.atchCnt.value;
	showAttach(parseInt(ansCnt));
}

 function initParameter(){
	 
		if( document.getElementById("CHK_201").checked ){
			trust_check(document.getElementById("CHK_201"));
		}
		if( document.getElementById("CHK_202").checked ){
			trust_check(document.getElementById("CHK_202"));
		}
		if( document.getElementById("CHK_203").checked ){
			trust_check(document.getElementById("CHK_203"));
		}
		//if( document.getElementById("CHK_205").checked ){
		//	trust_check(document.getElementById("CHK_205"));
		//}
		
		fn_createTable2("divFileList", "0");
		fn_setFileInfo();
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 


/////////////////////////////////////////////////////////////////////////
// about file
// 파일첨부하기
	function fileUpload(filename, filefullname){

		var len=document.prpsForm.filename.length;
		var sform = "prpsForm";	// formName
		if(len > 3){
			alert("첨부파일은 "+3+"개까지 제한하고 있습니다.");
			return;
		}

		window.open("/common/common_file_p2.jsp?fname="+filename+"&ffullname="+filefullname+"&sform="+sform,"FILEUP","width=440, height=276, toolbar=no,menubar=no,resizable=no,status=yes,scrollbars=no");
	}

	// 파일삭제하기
	function deleteItem(filename, filefullname) {

		var str = document.getElementById(filefullname).value;

		//document.frm.select.options[document.f.select.selectedIndex].value
		var itemNo = document.prpsForm.filename.selectedIndex;

		if(str.length == 0){
			alert('삭제할 파일이 없습니다.');
			return;
		} else {

			if(confirm('파일을 삭제하시겠습니까?')){

				url = "/common/real_file_del2.jsp?gubun=multi&ffullname="+str;

				result = trim(synchRequest(url));

				if(result=='true'){
					//document.frm.filename.options[document.frm.filename.selectedIndex].value = '';
					//document.frm.filename.options[document.frm.filename.selectedIndex].text = '';
					delOption(document.prpsForm.filename.options[itemNo].value)

				}else {
					alert('파일삭제 실패');
					return;
				}
			}
		}
	}

	function delOption(aa) {
 		var len = document.prpsForm.filename.options.length;
		for (i=0;i<len;i++) {
			if (document.prpsForm.filename.options[i].value != null && aa != null){
				if(aa == document.prpsForm.filename.options[i].value){
					document.prpsForm.filename.options[i]=null;
					break;
				}
			}
		}
   		//resort();
	}
	
	//파일 전송시에 모두 선택해서 전송하는 스크립트
	function resort() {
 		var temp = "";
 		var len = document.prpsForm.filename.options.length;
 		for (i=0;i<len;i++) {
 			temp += document.prpsForm.filename.options[i].value+",";
 		}
 		temp= temp.substring(0, temp.length-1);
 		document.prpsForm.FILE_INFO.value = temp;
	}
	
	//파일 전송시에 모두 선택해서 전송하는 스크립트
	function selOption(){
		var len=document.prpsForm.filename.length;
		
		for (i=1;i<len;i++ ){
		 document.prpsForm.filename.options[i].selected=true;
		}
	
	}
	

	function fn_cal(frmName, objName){
		showCalendar(frmName, objName);
	}



	
//
--></script>
</head>
<c:if test="${back.DIVS != 'BACK'}">
	<body onload="javascript:prps_check('${rghtPrps.PRPS_RGHT_CODE }');">
</c:if>
<c:if test="${back.DIVS == 'BACK'}">
	<body onload="javascript:prps_check2();">
</c:if>
<!-- 전체를 감싸는 DIVISION -->
<div id="wrap">

<!-- HEADER str--> 
<jsp:include	page="/include/2012/header.jsp" /> 
<script type="text/javascript">initNavigation(2);</script>
<!-- GNB setOn 각페이지에 넣어야합니다. --> <!-- HEADER end -->
<!-- CONTAINER str-->
	
<div id="container" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
	<div class="container_vis" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
		<h2><span><img src="/images/2012/title/container_vis_h2_2.gif" alt="내권리찾기" title="내권리찾기" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
		<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
	</div>
	<div class="content">
			<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu02.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb1","lnb12");</script>
			<!-- //래프 -->
		<div id="ajaxBox" style="position:absolute; z-index:9999; background: url(/images/2012/common/loadingBg.gif) no-repeat 0 0; left:-500px; width: 402px; height: 56px; padding: 102px 0 0 0;">
		</div>
		<!-- 주요컨텐츠 str -->
		<div class="contentBody" id="contentBody">
			<p class="path"><span>Home</span><span>내권리찾기</span><em>저작권정보 변경신청</em></p>
			<h1><img src="/images/2012/title/content_h1_0201.gif" alt="저작권정보 변경신청" title="저작권정보 변경신청" /></h1>
			
			<div class="section">
				
				<!-- Tab str -->
                          <ul id="tab11" class="tab_menuBg">
                              <li class="first on"><strong><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악</a></strong></li>
							  <li><a href="/rghtPrps/rghtSrch.do?DIVS=B">도서</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=N">뉴스</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송</a></li>
                              <li><a href="/rghtPrps/rghtSrch.do?DIVS=X">기타</a></li>
                      		</ul>
                      <!-- //Tab -->
			
				<!-- 연락처 박스  -->
				<jsp:include page="/common/memo/2011/memo_01.jsp">
					<jsp:param name="DIVS" value="${DIVS}" />
				</jsp:include>
				<!-- //연락처 박스 -->
				
				<div class="article mt20">
					<div class="floatDiv">
						<h2 class="fl">저작권정보 변경신청</h2>
						<c:set var="smplLink" value=""/>
						<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">
							<c:set var="smplLink" value="javascript:openSmplDetail('MR')"/>
						</c:if>
						<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">
							<c:set var="smplLink" value="javascript:openSmplDetail('MU')"/>
						</c:if>
						<p class="fr"><span class="button small icon"><a href="${smplLink}">예시화면 보기</a><span class="help"></span></span></p>
					</div>
					<form name="prpsForm" enctype="multipart/form-data" action="#">
					<input type="hidden" name="USER_IDNT" value="${userInfo.USER_IDNT}" />
					<input type="hidden" name="USER_NAME" value="${userInfo.USER_NAME }" />
					<input type="hidden" name="USER_DIVS" value="${userInfo.USER_DIVS }" />
					<input type="hidden" name="PRPS_RGHT_CODE" id="PRPS_RGHT_CODE"
					value="${rghtPrps.PRPS_RGHT_CODE }" /><!-- 신청목적 -->
					<input type="hidden" name="RESD_CORP_NUMB_VIEW" value="${userInfo.RESD_CORP_NUMB_VIEW }" />
					<input type="hidden" name="CORP_NUMB" value="${userInfo.CORP_NUMB }" />
					<input type="hidden" name="PRPS_RGHT_CODE2" id="PRPS_RGHT_CODE2" value="${rghtPrps.PRPS_RGHT_CODE }" />
					<input type="hidden" name="OFFX_LINE_RECP2" id="OFFX_LINE_RECP2" value="${rghtPrps.OFFX_LINE_RECP }" />
					<input type="hidden" name="FILE_INFO" value="${rghtPrps.FILE_INFO}" /> <!-- 접근목록 -->
					<input type="hidden" name="listDivs" value="${listDivs}" />
					<input type="submit" style="display:none;">
					<span class="topLine"></span>
					<!-- 그리드스타일 -->
					<table cellspacing="0" cellpadding="0" border="1" summary="저작권찾기 신청정보 입력 폼입니다." class="grid tableFixed">
						<colgroup>
						<col width="20%">
						<col width="*">
						</colgroup>
						<tbody>
							<tr>
								<th scope="row">신청목적</th>
								<td><!-- <select name="PRPS_RGHT_CODE" id="PRPS_RGHT_CODE"  title="신청목적" nullCheck onchange="prps_check(this);">-->
									<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">권리자의 저작권찾기</c:if>
									<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">이용자의 저작권조회</c:if>
								</td>
							</tr>
							<tr>
								<th scope="row">신청인정보</th>
								<td>
									<span class="topLine2"></span>
									<!-- 그리드스타일 -->
									<div id="fldInmtInfo">
									<table cellspacing="0" cellpadding="0" border="1" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다." class="grid">
										<colgroup>
										<col width="15%">
										<col width="32%">
										<col width="25%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row">성명</th>
												<td>${userInfo.USER_NAME}</td>
												<th scope="row">주민등록번호/사업자번호</th>
												<c:if test="${userInfo.USER_DIVS != '03'}">
												<td>
												${userInfo.RESD_CORP_NUMB_VIEW}
												</td>
												</c:if>
												<c:if test="${userInfo.USER_DIVS != '01'}">
												<td>
												${fn:substring(userInfo.CORP_NUMB,0,3)}-${fn:substring(userInfo.CORP_NUMB,3,5)}-${fn:substring(userInfo.CORP_NUMB,5,10)}
												</td>
												</c:if>
											</tr>
											<tr>
												<th scope="row"><label for="regi1" class="necessary">전화번호</label></th>
												<td>
													<ul class="list1">
													<li class="p11"><label for="regi1" class="inBlock w25">자택</label> : <input type="text" id="regi1" size="14" maxlength="20" name="HOME_TELX_NUMB" title="자택 전화" value="${userInfo.TELX_NUMB}" /></li>
													<li class="p11"><label for="regi2" class="inBlock w25">사무실</label> : <input type="text" id="regi2" size="14" maxlength="20" name="BUSI_TELX_NUMB" title="사무실 전화" value="${userInfo.BUSI_TELX_NUMB}"/></li>
													<li class="p11"><label for="regi3" class="inBlock w25">휴대폰</label> : <input type="text" id="regi3" size="14" maxlength="20" name="MOBL_PHON" title="휴대폰 전화" value="${userInfo.MOBL_PHON}" /></li>
													</ul>
												</td>
												<th scope="row"><label for="regi4">팩스번호</label></th>
												<td><input type="text" name="FAXX_NUMB" id="regi4" value="${userInfo.FAXX_NUMB}" size="20" title="팩스번호" maxlength="20" /></td>
											</tr>
											<tr>
												<th scope="row"><label for="regi5">이메일주소</label></th>
												<td colspan="3"><input type="text" name="MAIL" id="regi5" maxlength="50" value="${userInfo.MAIL}" title="신청자 EMAIL" class="w50" /></td>
											</tr>
											<tr>
												<th scope="row"><label for="regi6" class="necessary">주소</label></th>
												<td colspan="3">
													<ul class="list1">
													<li class="p11"><label for="regi6" class="inBlock w10">자택</label> : <input type="text" id="regi6" name="HOME_ADDR" title="자택 주소" maxlength="50" value="${userInfo.HOME_ADDR}" class="w85" /></li>
													<li class="p11"><label for="regi7" class="inBlock w10">사무실</label> : <input type="text" id="regi7" name="BUSI_ADDR" title="사무실 주소" maxlength="50" value="${userInfo.BUSI_ADDR}" class="w85" /></li>
													</ul>
												</td>
											</tr>
										</tbody>
									</table>
									</div>
									<!-- //그리드스타일 -->
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="CHK_201" class="necessary">권리구분</label></th>
								<td>
									<ul class="line22">
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_201" value="201" 
										title="작사,작곡,편곡 - 저작권자(한국음악저작권협회)" class="inputRChk"	onclick="trust_check(this);"
										<c:if test="${rghtPrps.CHK_201 == '201'}">checked="checked"</c:if> />
										<label class="p12" for="CHK_201">작사, 작곡, 편곡 - 저작권자(한국음악저작권협회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE" id="CHK_203" value="203"
										title="음반제작 - 저작인접권자(한국음반산업협회)"
										class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_203 == '203'}">checked="checked"</c:if> />
										<label class="p12" for="CHK_203">음반제작 - 저작인접권자(한국음반산업협회)</label>
									</li>
									<li>
										<input type="checkbox" name="TRST_ORGN_CODE"
										id="CHK_202" value="202" title="가창,연주,지휘 - 저작인접권자(한국음악실연자연합회)"
										class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_202 == '202'}">checked="checked"</c:if> />
										<label class="p12" for="CHK_202">가창, 연주, 지휘 - 저작인접권자(한국음악실연자연합회)</label></li>
									</ul>
								</td>
							</tr>
							<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">
							<tr>
								<th scope="row"><label for="PRPS_DOBL_CODE">동시신청 여부</label></th>
								<td>
									<p class="blue2 p11">&lowast; 저작권찾기와 보상금을 동시에 신청하는 경우 선택합니다.<br />권리구분이 방송음악보상금 관리단체인 한국음반산업협회, 한국음악실연자연합회인 경우에만 선택 가능합니다.</p>
									<input type="checkbox" name="PRPS_DOBL_CODE" id="PRPS_DOBL_CODE" value="Y" class="inputRChk" <c:if test="${rghtPrps.PRPS_DOBL_CODE == 'Y'}">checked="checked"</c:if> />
									<label class="p12" for="PRPS_DOBL_CODE">보상금 동시신청</label>
								</td>
							</tr>
							</c:if><tr>
								<th scope="row"><label class="necessary">신청저작물 정보</label></th>
								<td>
									<!-- 권리자 저작권찾기 시작 -->
									<div id="div_1" class="tabelRound" style="width:572px; padding:0 0 0 0; display:none">
										<span class="blue2 p11">&lowast; 권리있는 항목만 입력하세요.</span>
										
										<div class="floatDiv mb5 mt10"><h3 class="fl mt5">권리자 저작권찾기<span id="totalRow"></span></h3>
										<p class="fr"><a href="#1" onclick="javascript:editTable('I');" id="workAdd"><img src="/images/2012/button/add.gif" alt="추가" /></a>
										<a href="#1" onclick="javascript:editTable('D');"><img src="/images/2012/button/delete.gif" alt="삭제" />
										</a></p>
										</div>
										<div id="div_scroll_1" style="width:570px;">
											<table id="listTab" style="padding:0 0 0 0;" cellspacing="0" cellpadding="0" border="1" summary="권리자 저작권찾기 신청저작물정보 폼입니다." class="grid ce tableFixed"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
												<colgroup>
												<col width="4%">
												<col width="6%">
												<col width="*">
												<col width="16%">
												<col width="12%">
												<col width="12%">
												<col width="12%">
												<col width="12%">
												</colgroup>
												<thead>
													<tr>
														<th rowspan="2" scope="col"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk',this);" title="전체선택" /></th>
														<th rowspan="2" scope="col">순<br>번</th>
														<th scope="col"><label class="necessary white">저작물명</label></th>
														<th scope="col">실연시간</th>
														<th scope="col">작사</th>
														<th scope="col">작곡</th>
														<th scope="col">편곡</th>
														<th rowspan="2" scope="col">음반제작</th>
													</tr>
													<tr>
														<th scope="col"><label class="necessary white">앨범명</label></th>
														<th scope="col"><label class="necessary white">발매일</label></th>
														<th scope="col">가창</th>
														<th scope="col">연주</th>
														<th scope="col">지휘</th>
													</tr>
												</thead>
												<c:if test="${back.DIVS != 'BACK'}">
													<c:if test="${!empty prpsList}">
														<c:forEach items="${prpsList}" var="prpsList">
															<c:set var="NO" value="${prpsList.totalRow}" />
															<c:set var="i" value="${i+1}" />
															<tbody>
															<tr>
																<td rowspan="2" class="ce pd5">
																	<input type="checkbox" name="iChk" id="iChk_${i}" class="vmid" title="선택" />
																</td>
																<td rowspan="2" class="ce pd5">
																<input name="displaySeq" id="displaySeq_${i}" title="순번" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}" />
																</td>
																<td class="lft pd5"><a class="underline black2" href="#1" onclick="javascript:openMusicDetail('${prpsList.CR_ID }','${prpsList.NR_ID }','${prpsList.ALBUM_ID }');">${prpsList.MUSIC_TITLE}</a></td>
																<td class="ce pd5">${prpsList.PERF_TIME }</td>
																<td class="ce pd5"><input name="LYRICIST" id="LYRICIST_${i}"
																class="inputDisible2 w85" value="${prpsList.LYRICIST_TRNS}"
																title="작사" rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="COMPOSER" id="COMPOSER_${i}"
																class="inputDisible2 w85" value="${prpsList.COMPOSER_TRNS }"
																title="작곡" rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="ARRANGER" id="ARRANGER_${i}"
																class="inputDisible2 w85" value="${prpsList.ARRANGER_TRNS }"
																title="편곡" rangeSize="~100" readonly="readonly" /></td>
																<td rowspan="2" class="ce pd5"><input name="PRODUCER" id="PRODUCER_${i}"
																class="inputDisible2 w85" value="${prpsList.PRODUCER_TRNS }"
																title="앨범제작" rangeSize="~100" readonly />
																<!-- hidden Value -->
																<input type="hidden" name="iChkVal" value="${prpsList.CR_ID }|${prpsList.NR_ID }|${prpsList.ALBUM_ID }" />
																<input type="hidden" name="MUSIC_TITLE" value="${prpsList.MUSIC_TITLE_TRNS }" />
																<input type="hidden" name="ALBUM_TITLE" value="${prpsList.ALBUM_TITLE }" />
																<input type="hidden" name="PERF_TIME" value="${prpsList.PERF_TIME }" />
																<input type="hidden" name="ISSUED_DATE_${i}" value="${prpsList.ISSUED_DATE }" />
																<input type="hidden" name="ISSUED_DATE" value="${i}" /> <!-- <input type="hidden" name="LYRICS" value=${prpsList.LYRICS } /> -->
																<input type="hidden" name="LYRICIST_ORGN" value="${prpsList.LYRICIST_TRNS }" />
																<input type="hidden" name="COMPOSER_ORGN" value="${prpsList.COMPOSER_TRNS }" />
																<input type="hidden" name="ARRANGER_ORGN" value="${prpsList.ARRANGER_TRNS }" />
																<input type="hidden" name="SINGER_ORGN" value="${prpsList.SINGER_TRNS }" />
																<input type="hidden" name="PLAYER_ORGN" value="${prpsList.PLAYER_TRNS }" />
																<input type="hidden" name="CONDUCTOR_ORGN" value="${prpsList.CONDUCTOR_TRNS }" />
																<input type="hidden" name="PRODUCER_ORGN" value="${prpsList.PRODUCER_TRNS }" /> <!-- 사용자정보입력확인 -->
																<input type="hidden" name="CHK_LYRICIST" />
																<input type="hidden" name="CHK_COMPOSER" />
																<input type="hidden" name="CHK_ARRANGER" />
																<input type="hidden" name="CHK_SINGER" />
																<input type="hidden" name="CHK_PLAYER" />
																<input type="hidden" name="CHK_CONDUCTOR" />
																<input type="hidden" name="CHK_PRODUCER" />
																</td>
															</tr>
															<tr>
																<td class="lft pd5">${prpsList.ALBUM_TITLE }</td>
																<td class="ce">${prpsList.ISSUED_DATE }</td>
																<td class="ce pd5"><input name="SINGER" id="SeditTableINGER_${i}"
																class="inputDisible2 w85" value="${prpsList.SINGER_TRNS }"
																title="가창" rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="PLAYER" id="PLAYER_${i}"
																class="inputDisible2 w85" value="${prpsList.PLAYER_TRNS }"
																title="연주" rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="CONDUCTOR" id="CONDUCTOR_${i}"
																class="inputDisible2 w85" value="${prpsList.CONDUCTOR_TRNS }"
																title="지휘" rangeSize="~100" readonly="readonly" />
																</td>
															</tr>
															</tbody>
														</c:forEach>
													</c:if>
												</c:if>
												<c:if test="${back.DIVS == 'BACK'}">
													<c:forEach items="${rghtPrps.keyId}" var="prpsList">
														<c:set var="i" value="${i+1}" />
														<c:if test="${empty rghtPrps.keyId[i-1]}">
															<tbody>
															<tr>
																<td rowspan="2" class="ce pd5">
																<input type="checkbox" name="iChk" id="iChk_${i}" class="vmid" title="선택" />
																</td>
																<td rowspan="2" class="ce pd5">
																<input name="displaySeq" title="순번" id="displaySeq_${i}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}" />
																</td>
																<td class="lft pd5"><input name="MUSIC_TITLE" id="MUSIC_TITLE" class="inputDataN w95" title="저작물명" rangeSize="~100" nullCheck value=${rghtPrps.MUSIC_TITLE_ARR_TRNS[i-1] } /></td>
																<td class="ce pd5"><input name="PERF_TIME" id="PERF_TIME" class="inputDataN w88" title="실연시간" character="KE" 
																rangeSize="~12" value="${rghtPrps.PERF_TIME_ARR[i-1] }" /></td>
																<td class="ce pd5"><input name="LYRICIST" id="LYRICIST_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.LYRICIST_ARR_TRNS[i-1] }" title="작사"
																rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="COMPOSER" id="COMPOSER_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.COMPOSER_ARR_TRNS[i-1] }" title="작곡"
																rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="ARRANGER" id="ARRANGER_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.ARRANGER_ARR_TRNS[i-1] }" title="편곡"
																rangeSize="~100" readonly="readonly" /></td>
																<td rowspan="2" class="ce pd5">
																<input name="PRODUCER" id="PRODUCER_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.PRODUCER_ARR_TRNS[i-1] }" title="앨범제작"
																rangeSize="~100" readonly="readonly" />
																<!-- hidden Value -->
																<input type="hidden" name="iChkVal" value="${rghtPrps.keyId[i-1] }" />
																<input type="hidden" name="LYRICIST_ORGN" value="${rghtPrps.LYRICIST_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="COMPOSER_ORGN" value="${rghtPrps.COMPOSER_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="ARRANGER_ORGN" value="${rghtPrps.ARRANGER_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="SINGER_ORGN" value="${rghtPrps.SINGER_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="PLAYER_ORGN" value="${rghtPrps.PLAYER_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="CONDUCTOR_ORGN" value="${rghtPrps.CONDUCTOR_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="PRODUCER_ORGN" value="${rghtPrps.PRODUCER_ORGN_ARR_TRNS[i-1] }" />
																<!-- 사용자정보입력확인 -->
																<input type="hidden" name="CHK_LYRICIST" />
																<input type="hidden" name="CHK_COMPOSER" />
																<input type="hidden" name="CHK_ARRANGER" />
																<input type="hidden" name="CHK_SINGER" />
																<input type="hidden" name="CHK_PLAYER" />
																<input type="hidden" name="CHK_CONDUCTOR" />
																<input type="hidden" name="CHK_PRODUCER" />
																</td>
															</tr>
															<tr>
																<td class="lft pd5"><input name="ALBUM_TITLE" id="ALBUM_TITLE"
																class="inputDataN w95" title="앨범명" rangeSize="~100" nullCheck
																value=${rghtPrps.ALBUM_TITLE_ARR_TRNS[i-1] } /></td>
																<td class="ce pd5"><input name="ISSUED_DATE_${i}"
																id="ISSUED_DATE_${i}" class="inputDataN" title="발매일"
																maxlength="8" Size="8" character="KE" dateCheck nullCheck
																value="${rghtPrps.ISSUED_DATE_ARR[i-1] }" /> <img
																src="/images/2012/common/calendar.gif"
																onclick="javascript:fn_cal('prpsForm','ISSUED_DATE_${i}');"
																onkeypress="javascript:fn_cal('prpsForm','ISSUED_DATE_${i}');"
																alt="달력" title="시작 날짜를 선택하세요." align="middle"
																style="cursor:pointer;" /> <input type="hidden"
																name="ISSUED_DATE" value="${i}" /></td>
																<td class="ce pd5"><input name="SINGER" id="SINGER_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.SINGER_ARR_TRNS[i-1] }" title="가창"
																rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="PLAYER" id="PLAYER_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.PLAYER_ARR_TRNS[i-1] }" title="연주"
																rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="CONDUCTOR" id="CONDUCTOR_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.CONDUCTOR_ARR_TRNS[i-1] }" title="지휘"
																rangeSize="~100" readonly="readonly" />
																</td>
															</tr>
															</tbody>
														</c:if>
														<c:if test="${!empty rghtPrps.keyId[i-1]}">
															<tbody>
															<tr>
																<td rowspan="2" class="ce pd5">
																<input type="checkbox" name="iChk" id="iChk_${i}" class="vmid" title="선택" />
																</td>
																<td rowspan="2" class="ce pd5">
																<input name="displaySeq" title="순번" id="displaySeq_${i}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}" />
																</td>
																<td class="lft pd5">${rghtPrps.MUSIC_TITLE_ARR[i-1] }</td>
																<td class="ce pd5">${rghtPrps.PERF_TIME_ARR[i-1] }</td>
																<td class="ce pd5"><input name="LYRICIST" id="LYRICIST_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.LYRICIST_ARR_TRNS[i-1] }" title="작사"
																rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="COMPOSER" id="COMPOSER_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.COMPOSER_ARR_TRNS[i-1] }" title="작곡"
																rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="ARRANGER" id="ARRANGER_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.ARRANGER_ARR_TRNS[i-1] }" title="편곡"
																rangeSize="~100" readonly="readonly" /></td>
																<td rowspan="2" class="ce pd5">
																<input name="PRODUCER" id="PRODUCER_${i}"
																class="inputDisible2 w80 ce"
																value="${rghtPrps.PRODUCER_ARR_TRNS[i-1] }" title="앨범제작"
																rangeSize="~100" readonly="readonly" />
																<!-- hidden Value -->
																<input type="hidden" name="iChkVal" value="${rghtPrps.keyId[i-1] }" />
																<input type="hidden" name="LYRICIST_ORGN" value="${rghtPrps.LYRICIST_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="COMPOSER_ORGN" value="${rghtPrps.COMPOSER_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="ARRANGER_ORGN" value="${rghtPrps.ARRANGER_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="SINGER_ORGN" value="${rghtPrps.SINGER_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="PLAYER_ORGN" value="${rghtPrps.PLAYER_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="CONDUCTOR_ORGN" value="${rghtPrps.CONDUCTOR_ORGN_ARR_TRNS[i-1] }" />
																<input type="hidden" name="PRODUCER_ORGN" value="${rghtPrps.PRODUCER_ORGN_ARR_TRNS[i-1] }" />
																<!-- 사용자정보입력확인 -->
																<input type="hidden" name="CHK_LYRICIST" />
																<input type="hidden" name="CHK_COMPOSER" />
																<input type="hidden" name="CHK_ARRANGER" />
																<input type="hidden" name="CHK_SINGER" />
																<input type="hidden" name="CHK_PLAYER" />
																<input type="hidden" name="CHK_CONDUCTOR" />
																<input type="hidden" name="CHK_PRODUCER" />
																</td>
															</tr>
															<tr>
																<td class="lft pd5">${rghtPrps.ALBUM_TITLE_ARR[i-1] }</td>
																<td class="ce pd5">${rghtPrps.ISSUED_DATE_ARR[i-1] }</td>
																<input type="hidden" name="MUSIC_TITLE" value="${rghtPrps.MUSIC_TITLE_ARR_TRNS[i-1] }" />
																<input type="hidden" name="ALBUM_TITLE" value="${rghtPrps.ALBUM_TITLE_ARR_TRNS[i-1] }" />
																<input type="hidden" name="PERF_TIME" value="${rghtPrps.PERF_TIME_ARR[i-1] }" />
																<input type="hidden" name="ISSUED_DATE_${i}" value="${rghtPrps.ISSUED_DATE_ARR[i-1] }" />
																<input type="hidden" name="ISSUED_DATE" value="${i}" />
																<td class="ce pd5"><input name="SINGER" id="SINGER_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.SINGER_ARR_TRNS[i-1] }" title="가창"
																rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="PLAYER" id="PLAYER_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.PLAYER_ARR_TRNS[i-1] }" title="연주"
																rangeSize="~100" readonly="readonly" /></td>
																<td class="ce pd5"><input name="CONDUCTOR" id="CONDUCTOR_${i}"
																class="inputDisible2 w85 ce"
																value="${rghtPrps.CONDUCTOR_ARR_TRNS[i-1] }" title="지휘"
																rangeSize="~100" readonly="readonly" />
																</td>
															</tr>
															</tbody>
														</c:if>
													</c:forEach>
												</c:if>
											</table>
										</div>
									</div>
									<!-- 권리자의 저작권 찾기 끝 -->
									
									<!-- 이용자의 저작권 찾기 시작 -->
									<div id="div_2" class="tabelRound mt10" style="width:572px; display:none">
										<span class="blue2 p11">&lowast; 권리있는 항목만 입력하세요.</span>
										<div class="floatDiv mb5 mt10">
										<h3 class="fl mt5">이용자 저작권조회<span id="totalRow2"></span></h3>
										<p class="fr"><a href="#1" onclick="javascript:editTable('I');" id="workAdd2"><img src="/images/2012/button/add.gif" alt="추가" /></a>
										<a href="#1" onclick="javascript:editTable('D');"><img src="/images/2012/button/delete.gif" alt="삭제" />
										</a></p>
										</div>
										
										<div id="div_scroll_2" style="width:569px; padding:0 0 0 0;">
											<table id="listTab_2" cellspacing="0" cellpadding="0" border="1" summary="권리자 저작권찾기 신청저작물정보 폼입니다." class="grid ce"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
												<colgroup>
												<col width="3%">
												<col width="6%">
												<col width="*">
												<col width="24%">
												<col width="12%">
												<col width="16%">
												</colgroup>
												<thead>
													<tr>
														<th scope="col">
														<input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk2',this);" title="전체선택" /></th>
														<th scope="col">순번</th>
														<th scope="col"><label class="necessary white">저작물명</label></th>
														<th scope="col"><label class="necessary white">앨범명</label></th>
														<th scope="col">실연시간</th>
														<th scope="col"><label class="necessary white">발매일</label></th>
													</tr>
												</thead>
												<tbody>
												<c:if test="${back.DIVS != 'BACK'}">
													<c:if test="${!empty prpsList}">
														<c:forEach items="${prpsList}" var="prpsList">
															<c:set var="NO" value="${prpsList.totalRow}" />
															<c:set var="ii" value="${ii+1}" />
															<tr>
																<td class="ce pd5">
																	<input type="checkbox" name="iChk2" id="iChk2_${ii}" class="vmid" title="선택" />
																</td>
																<td class="ce pd5">
																	<input name="displaySeq2" title="순번" id="displaySeq2_${ii}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${ii}" />
																</td>
																<td class="lft pd5">
																	<a class="underline black2" href="#1" onclick="javascript:openMusicDetail('${prpsList.CR_ID }','${prpsList.NR_ID }','${prpsList.ALBUM_ID }');"><u>${prpsList.MUSIC_TITLE}</u></a>
																</td>
																<td class="lft pd5">${prpsList.ALBUM_TITLE }</td>
																<td class="ce pd5">${prpsList.PERF_TIME }</td>
																<td class="ce pd5">${prpsList.ISSUED_DATE }
																<input type="hidden" name="iChkVal" value="${prpsList.CR_ID }|${prpsList.NR_ID }|${prpsList.ALBUM_ID }" />
																<input type="hidden" name="MUSIC_TITLE" value="${prpsList.MUSIC_TITLE_TRNS }" />
																<input type="hidden" name="ALBUM_TITLE" value="${prpsList.ALBUM_TITLE_TRNS }" />
																<input type="hidden" name="PERF_TIME" value="${prpsList.PERF_TIME }" />
																<!--<input type="hidden" name="ISSUED_DATE" value="${prpsList.ISSUED_DATE }"/>-->
																<input type="hidden" name="VAL_ISSUED_DATE_${ii}" value="${prpsList.ISSUED_DATE }" />
																<input type="hidden" name="VAL_ISSUED_DATE" value="${ii}" />
																<!-- <input type="hidden" name="LYRICS" value="${prpsList.LYRICS }"/>  -->
																<input type="hidden" name="LYRICIST" />
																<input type="hidden" name="COMPOSER" />
																<input type="hidden" name="ARRANGER" />
																<input type="hidden" name="SINGER" />
																<input type="hidden" name="PLAYER" />
																<input type="hidden" name="CONDUCTOR" />
																<input type="hidden" name="PRODUCER" />
																</td>
															</tr>
														</c:forEach>
													</c:if>
												</c:if>
												<c:if test="${back.DIVS == 'BACK'}">
													<c:forEach items="${rghtPrps.keyId}" var="prpsList">
													<c:set var="ii" value="${ii+1}" />
														<tr>
															<td class="ce pd5">
																<input type="checkbox" title="선택" name="iChk2" id="iChk2_${ii}" class="vmid" />
															</td>
															<td class="ce pd5">
																<input name="displaySeq2" id="displaySeq2_${ii}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${ii}" title="순번" />
															</td>
															<c:if test="${empty rghtPrps.keyId[ii-1]}">
																<td class="lft pd5">
																	<input name="MUSIC_TITLE" id="MUSIC_TITLE" class="inputDataN w95" title="저작물명" rangeSize="~100" nullCheck
																	value="${rghtPrps.MUSIC_TITLE_ARR_TRNS[ii-1] }" /></td>
																<td class="ce pd5">
																	<input name="ALBUM_TITLE" id="ALBUM_TITLE"
																	class="inputDataN w95" title="앨범명" rangeSize="~100" nullCheck
																	value="${rghtPrps.ALBUM_TITLE_ARR_TRNS[ii-1] }" />
																</td>
																<td class="ce pd5">
																	<input name="PERF_TIME" id="PERF_TIME"
																	class="inputDataN w90" title="실연시간" character="KE"
																	rangeSize="~12" value="${rghtPrps.PERF_TIME_ARR[ii-1] }" />
																</td>
																<td class="ce pd5">
																	<input name="VAL_ISSUED_DATE_${ii}"
																	id="VAL_ISSUED_DATE_${ii}" class="inputDataN w65" title="발매일"
																	maxlength="8" Size="8" character="KE" dateCheck nullCheck
																	value="${rghtPrps.ISSUED_DATE_ARR[ii-1] }" /> <img
																	src="/images/2012/common/calendar.gif"
																	onclick="javascript:fn_cal('prpsForm','VAL_ISSUED_DATE_${ii}');"
																	onkeypress="javascript:fn_cal('prpsForm','VAL_ISSUED_DATE_${ii}');"
																	alt="달력" title="시작 날짜를 선택하세요." align="middle"
																	style="cursor:pointer;" />
																	<input type="hidden" name="VAL_ISSUED_DATE" value="${ii}" />
																</td>
															</c:if>
															<c:if test="${!empty rghtPrps.keyId[ii-1]}">
																<td class="lft pd5">${rghtPrps.MUSIC_TITLE_ARR[ii-1] }</td>
																<td class="ce pd5">${rghtPrps.ALBUM_TITLE_ARR[ii-1] }
																</td>
																<td class="ce pd5">${rghtPrps.PERF_TIME_ARR[ii-1] }</td>
																<td class="ce pd5">${rghtPrps.ISSUED_DATE_ARR[ii-1] }
																</td>
																<input type="hidden" name="MUSIC_TITLE" value="${rghtPrps.MUSIC_TITLE_ARR_TRNS[ii-1] }" />
																<input type="hidden" name="ALBUM_TITLE" value="${rghtPrps.ALBUM_TITLE_ARR_TRNS[ii-1] }" />
																<input type="hidden" name="PERF_TIME" value="${rghtPrps.PERF_TIME_ARR[ii-1] }" />
																<input type="hidden" name="VAL_ISSUED_DATE_${ii}" value="${rghtPrps.ISSUED_DATE_ARR[ii-1] }" />
																<input type="hidden" name="VAL_ISSUED_DATE" value="${ii}" />
																<!-- <input type="hidden" name="LYRICS" value="${rghtPrps.LYRICS_ARR[ii-1] }"/> -->
															</c:if>
															<!-- hidden Value -->
															<input type="hidden" name="iChkVal"
																value="${rghtPrps.keyId[ii-1] }" />
															<input type="hidden" name="LYRICIST" />
															<input type="hidden" name="COMPOSER" />
															<input type="hidden" name="ARRANGER" />
															<input type="hidden" name="SINGER" />
															<input type="hidden" name="PLAYER" />
															<input type="hidden" name="CONDUCTOR" />
															<input type="hidden" name="PRODUCER" />
														</tr>
													</c:forEach>
												</c:if>
												</tbody>
											</table>
										</div>
									</div>
									
									<!-- 이용자의 저작권 찾기 끝 -->
									<p class="HBar mt25 mb5 mt70">&nbsp;</p>
									<div class="floatDiv mb5 mt10">
									<h3 class="fl mt5">저작물검색</h3>
									<p class="fr" id="pBtnAdd"><a href="#1" onclick="javascript:editTable('A');"><img
										src="/images/2012/button/add_up.gif" alt="추가" /></a></p>
									</div>
									<!-- iframe 영역입니다 -->
									<iframe id="ifMuscRghtSrch"
									title="저작물조회(음악)" name="muscRghtSrch" width="572" height="130"
									marginwidth="0" marginheight="0" frameborder="0" scrolling="no"
									style="overflow-y:hidden;overflow-x:hidden;"
									src="/rghtPrps/rghtSrch.do?method=subListForm&amp;page_no=1&amp;DIVS=M&amp;dtlYn=Y">
									</iframe>
									<!-- //iframe 영역입니다 -->
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="PRPS_DESC" class="necessary">신청내용</label></th>
								<td>
									<p class="p11 blue2">&lowast; 신청 목적이 이용자 저작권 조회인 경우 이용내용(이용하고자 하는 목적/방법)을 작성합니다.<br />&lowast; 신청 저작물 정보의 가사 및 저작물 변경 등을 작성합니다.</p>
									<textarea cols="10" rows="10" name="PRPS_DESC" id="PRPS_DESC"
									class="w99" title="신청내용" nullCheck>${rghtPrps.PRPS_DESC}</textarea>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="file1">첨부파일<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">(증빙서류)</c:if> <br />
								<input type="checkbox" name="OFFX_LINE_RECP" value="Y"
								class="inputChk" onclick="javascript:offLine_check(this);" id="file1"
								<c:if test="${rghtPrps.OFFX_LINE_RECP == 'Y'}">checked="checked"</c:if>
								title="오프라인접수" />오프라인접수
								</label>
								</th>
								<td id="td_file_no" style="display:none">오프라인접수로 선택한 경우 첨부파일을 등록할 수 없습니다.</td>
								<td id="td_file_yes">
								<!--  첨부파일 테이블 영역입니다 -->
								<div id="divFileList" class="tabelRound mb5" style="display;">
								</div>
								</td>
								<!--
								<td>
									<div class="floatDiv">
										<p class="fl"><input type="checkbox" id="file1" /><label for="file1" class="p11">오프라인접수</label></p>
										<p class="fr"><span class="button small icon"><a href="">파일추가</a><span class="add"></span></span> <span class="button small icon"><a href="">파일삭제</a><span class="delete"></span></span></p>
									</div>
									
									<ul class="list2 bgGray1 mt10 pd5">
									<li><a href="">파일 11111</a></li>
									<li><a href="">파일 11111</a></li>
									</ul>
								</td>
								-->
							</tr>
						</tbody>
					</table>
					<!-- //그리드스타일 -->
					<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">
					<div class="white_box">
						<div class="box5">
							<div class="box5_con floatDiv">
								<p class="fl mt5"><img src="/images/2012/content/box_img4.gif" alt="" /></p>
								<div class="fl ml30 mt5">
									<h4>첨부파일(증빙서류) 안내</h4>
									<ul class="list1 mt10">
									<li>저작권자임을 증명할 수 있는 서류(앨범자켓, 저작권등록증 등)</li>
									<li>상속, 양도, 승계 등의 사실을 증명할 수 있는 서류</li>
									<li>주민등록등본/법인등기부등본</li>
									<li>사업자등록증(사본) 1부</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					</c:if>
					
					<div class="btnArea">
						<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:goList()">목록</a></span></p>
						<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:rghtPrps();">저작권찾기 신청 확인</a></span></p>
					</div>
					</form>
				</div>
			</div>
		</div>
		
		<!-- //주요컨텐츠 end -->
	
	</div>
	
</div>

				
<!-- //CONTAINER end -->

 <!-- FOOTER str--> <jsp:include
	page="/include/2012/footer.jsp" /> <!-- FOOTER end --></div>
	
<!-- //전체를 감싸는 DIVISION -->

<form name="srchForm" action="#"><input type="hidden"
	name="srchTitle" value="${srchParam.srchTitle }" /> <input
	type="hidden" name="srchProducer" value="${srchParam.srchProducer }" />
<input type="hidden" name="srchAlbumTitle"
	value="${srchParam.srchAlbumTitle }" /> <input type="hidden"
	name="srchSinger" value="${srchParam.srchSinger }" /> <input
	type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }" />
<input type="hidden" name="srchEndDate"
	value="${srchParam.srchEndDate }" /> <input type="hidden"
	name="srchLicensor" value="${srchParam.srchLicensor }" />
	<input type="submit" style="display:none;">
	</form>

<script type="text/javascript" src="/js/2011/file.js"></script>
<script type="text/javascript">
<!--
	<c:if test="${back.DIVS != 'BACK'}">	
		<c:if test="${!empty prpsList}">
			<c:forEach items="${prpsList}" var="prpsList">
				iRowIdx++;
				iRowIdx2++;
			</c:forEach>
		</c:if>
	</c:if>

	<c:if test="${back.DIVS == 'BACK'}">	
		<c:forEach items="${rghtPrps.keyId}" var="prpsList">
			iRowIdx++;
			iRowIdx2++;
		</c:forEach>
	</c:if>
	

	function fn_setFileInfo(){
		
		var listCnt = '${fn:length(fileList)}';

		<c:if test="${!empty fileList}">
			<c:forEach items="${fileList}" var="fileList">
				fn_addGetFile(	'0',
								'${fileList.PRPS_MAST_KEY}',
								'${fileList.PRPS_SEQN}',
								'${fileList.ATTC_SEQN}',
								'${fileList.FILE_PATH}',
								'${fileList.REAL_FILE_NAME}',
								'${fileList.FILE_NAME}',
								'${fileList.TRST_ORGN_CODE}',
								'${fileList.FILE_SIZE}'
								);
			</c:forEach>
		</c:if>		
	}
//-->
</script>
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>
