<%@ page language="java" isELIgnored="false" contentType="text/html; charset=EUC-KR"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR" />
<script type="text/javascript" src="/js/common/Common.js" ></script> 
<link rel="STYLESHEET" type="text/css" href="/css/common/default.css" > 


<script type="text/javascript">

    var act = {
        goWriteForm : function(){ 
    	    location.href="/article/form.do?communityId=${article.communityId}";                        
        },
        articleList : function(){
            location.href="/article/list.do?communityId=${article.communityId}";
        },
        goReplyForm : function(){
        	var f = document.getElementById("form");
            f.action = '/article/replyForm.do';
            f.submit();
        },
        goModifyForm : function(){
        	var f = document.getElementById("form");
            f.action = '/article/modifyForm.do';
            f.submit();
        },
        runDelete : function(){
        	var f = document.getElementById("form");
            if(confirm('정말 삭제 하시겠습니까?')){           
                f.action = '/article/remove.do';
                f.submit();
            }else{
                return;
            }
        }
    }; 
    
</script>
</head>
<body>
<form name="form" id="form" action="" method="post" >
<input type="hidden" name="articleId"   value="${article.articleId}" id="articleId" />
<input type="hidden" name="communityId" value="${article.communityId}" />
    
<table width="650" cellspacing="0" cellpadding="0" border="0">
    <tr height="20"><td></td></tr>
    <tr height="20"> 
        <td class="tableTtile" style='padding:2px 5px 0 10px' width="550"> 자유게시판</td>
        <td class="grayB" width="100">right4me.or.kr</td>
    </tr>
</table>


<table width="650" cellspacing="0" cellpadding="0" border="0">
    
    <tr><td class="tableHL"  height="1" colspan="2"></td></tr>
    <tr height="22">
        <td width="450" style='padding:2px 5px 0 15px' ><div id="scrapTitle">${article.title}</div></td>
        <td width="200" align="right" class="idxS">${article.name} : <fmt:formatDate value="${article.registYmdt}" pattern="yyyy-MM-dd" />&nbsp; </td>
    </tr>    
    <tr><td class="tableHL"  height="1" colspan="2"></td></tr>
    

    <tr height="200">   
        <td  colspan="2" valign="top" align="center">
            <table width="630" cellspacing="0" cellpadding="0" border="0">
                <tr height="10"><td colspan="2"></td></tr>
                <tr>
                    <td colspan="2">
                        ${article.content}
                    </td>
                </tr>       
                <tr height="10"><td colspan="2"></td></tr>                        
            </table>
        </td>
    </tr>
    
    <tr><td class="tableHL"  height="1" colspan="2"></td></tr>
</table>
</form>
<!--  본문 끝  --> 

<table width="650" cellspacing="0" cellpadding="0" border="0">
    <tr height="5"><td></td></tr> 
    <tr>
        <td width="50%" align="left">
            <a href="javascript:act.goWriteForm();"  class="btn_big"><span>글 등 록</span></a>
            <a href="javascript:act.goReplyForm();"  class="btn_big"><span>글 답 변</span></a>
            <a href="javascript:act.goModifyForm();"  class="btn_big"><span>글 수 정</span></a>
            <a href="javascript:act.runDelete();"  class="btn_big"><span>글 삭 제</span></a>            
        </td>
        <td width="50%" align="right">
            <a href="javascript:act.articleList();"  class="btn_big"><span>목록보기</span></a>
        </td>
    </tr>
    <tr height="20"><td></td></tr>
</table>
</body>
</html>