<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작권찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp"/>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
    	   						GetFlash('/images/swf/subVisual.swf','725','122');
       					</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="siteInfoTlt">저작권찾기소개</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>저작권찾기소개</li>
					<li class="on">이용안내</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>이용안내</h2>
			<div id="contentsDiv">
				<ul class="tabMenu">
					<li><a href="/main/main.do?method=goRghtInfo">저작권찾기신청 이용안내</a></li>
					<li class="active">보상금 신청 이용안내</li>
				</ul>
				<h3>보상금 신청 이용안내</h3>
				<div class="box"><span>* 아래 썸네일 이미지를 클릭하면 해당 화면에 대한 확대 이미지를 조회할 수 있습니다.</span><br /> <img src="/images/sub/process02.gif" alt="보상금신청업무절차" border="0" usemap="#Map" class="img" />
					<map name="Map" id="Map">
						<area shape="rect" alt="" coords="14,63,161,105" href="#" onclick="MM_openBrWindow('/main/main.do?method=goInmtInfo1','','scrollbars=yes,width=920,height=600')" />
						<area shape="rect" alt="" coords="17,140,159,184" href="#" onclick="MM_openBrWindow('/main/main.do?method=goInmtInfo2','','scrollbars=yes,width=920,height=600')" />
						<area shape="rect" alt="" coords="217,211,361,246" href="#" onclick="MM_openBrWindow('/main/main.do?method=goInmtInfo3','','scrollbars=yes,width=920,height=600')" />
						<area shape="rect" alt="" coords="417,213,562,247" href="#" onclick="MM_openBrWindow('/main/main.do?method=goInmtInfo4','','scrollbars=yes,width=920,height=600')" />
						<area shape="rect" alt="" coords="217,284,361,316" href="#" onclick="MM_openBrWindow('/main/main.do?method=goInmtInfo5','','scrollbars=yes,width=920,height=600')" />
						<area shape="rect" alt="" coords="16,285,159,318" href="#" onclick="MM_openBrWindow('/main/main.do?method=goInmtInfo6','','scrollbars=yes,width=920,height=600')" />
					</map>
				</div>
			</div>
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
