<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작권찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp" flush="true">
		<jsp:param name="mNum" value="6" />
	</jsp:include>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','122');
			</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="siteInfoTlt">저작권찾기소개</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>저작권찾기소개</li>
					<li class="on">저작권찾기소개</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>저작권찾기소개</h2>
			<div id="contentsDiv">
				<ul class="tabMenu">
					<li><a href="/main/main.do?method=goSiteInfo1">내권리찾기란?</a></li>
					<li class="active">내권리찾기의 필요성</li>
					<li><a href="/main/main.do?method=goSiteInfo3">참여방법</a></li>
				</ul>
				<p class="clear" style="padding:25px 0 200px 0;"><span class="clear" style="padding:25px 0 200px 0;"><img src="/images/sub/info02_01img.gif" alt="1.음악에 대한 내 권리 확인 2.어문에 대한 내 권리 확인 3.보상금 찾기" /></span></p>
			</div>
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
