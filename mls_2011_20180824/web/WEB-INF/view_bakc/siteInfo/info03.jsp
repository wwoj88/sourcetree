<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작권찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp" flush="true">
		<jsp:param name="mNum" value="6" />
	</jsp:include>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','122');
			</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="siteInfoTlt">저작권찾기소개</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>저작권찾기소개</li>
					<li class="on">저작권찾기소개</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>저작권찾기소개</h2>
			<div id="contentsDiv">
				<ul class="tabMenu">
					<li><a href="/main/main.do?method=goSiteInfo1">저작권찾기란?</a></li>
					<li><a href="/main/main.do?method=goSiteInfo2">저작권찾기의 필요성</a></li>
					<li class="active">참여방법</li>
				</ul>
				<p class="clear" style="padding:0 0 20px 0;">저작권찾기 사이트에 대한 궁금한 점은 먼저 FAQ를 참조하세요. 그 밖의 질문은  Q&amp;A를 이용하시기 바랍니다.</p>
				<h3>신탁단체 비회원</h3>
				<p class="clear" style="padding:0 0 20px 0;"><img src="/images/sub/info03_01img.gif" alt="신탁단체 비회원" /></p>
				<h3>신탁단체 회원</h3>
				<p class="clear" style="padding:0 0 20px 0;"><img src="/images/sub/info03_02img.gif" alt="신탁단체 회원" /></p>
				<!-- 
				<h3>저작권찾기 업무절차</h3>
				<p class="clear" style="padding:0 0 20px 0;"><img src="/images/sub/process01.gif" alt="저작권찾기 업무절차" /></p>
				<h3>보상금신청 업무절차</h3>
				<p class="clear"><img src="/images/sub/process02.gif" alt="보상금신청 업무절차" /></p>
				 -->
			</div>
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
