<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(6);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_6.gif" alt="이용안내" title="이용안내" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/main/main.do?method=info01"><img src="/images/2011/content/sub_lnb0601_off.gif" title="저작권찾기 의미" alt="저작권찾기 의미" /></a></li>
					<li id="lnb2"><a href="/main/main.do?method=info02"><img src="/images/2011/content/sub_lnb0602_off.gif" title="권리자 저작권찾기 신청" alt="권리자 저작권찾기 신청" /></a></li>
					<li id="lnb3"><a href="/main/main.do?method=info03"><img src="/images/2011/content/sub_lnb0603_off.gif" title="이용자 저작권찾기 신청" alt="이용자 저작권찾기 신청" /></a></li>
					<li id="lnb4"><a href="/main/main.do?method=info04"><img src="/images/2011/content/sub_lnb0604_off.gif" title="보상금 신청" alt="보상금 신청" /></a></li>
					<li id="lnb5"><a href="/main/main.do?method=info05"><img src="/images/2011/content/sub_lnb0605_off.gif" title="권리자미확인저작물 이용" alt="권리자미확인저작물 이용" /></a></li>
                    <li id="lnb6"><a href="/main/main.do?method=goContactUs"><img src="/images/2011/content/sub_lnb0606_off.gif" title="이용문의" alt="이용문의" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb1");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>이용안내</span><em>저작권찾기 의미</em></p>
					<h1><img src="/images/2011/title/content_h1_0601.gif" alt="저작권찾기 의미" title="저작권찾기 의미" /></h1>
					
					<div class="section">
                    	<div class="w100">
                    	<img src="/images/2011/content/guideInfo01_subimg01.gif" alt="저작권찾기 의미 이미지" class="fl" />
                       
                        <div class="fr w70 mt10 info01Bg">
                    	<img src="/images/2011/content/guideInfo01_subimg02.gif" alt="저작권찾기는 문화와 경제를 품요롭게 하는 첫걸음!" />
                        <p class="w65 mt20">저작물은 창작자의 각고의 고통 속에서 오랜 시간과 많은 비용을 들여 탄생합니다. 그리고 이렇게 창작된 저작물은 권리자에겐 창작의 보람과 경제적 보상을, 이용자에겐 사업의 기회와 문화적 풍요로움을 선사합니다.<br><br><br>
그러나 안타깝게도 우리가 매일 이용하는 저작물 중 상당수는 그 권리관계가 명확하지 않은 저작물이 많습니다. 이로인해. 이용자는 원하는 저작물의 권리자를 확인할 수 없어, 이용의 불편과 상당한 사회적 비용을 지출하고 있습니다. 또한 권리자는 자신의 정당한 권리를 행사하지 못하기도 하며 따라서 당연히 받아야 할 보상금의 혜택도 누리지 못하고 있습니다.</p>
                    	                      
                    	<p class="mt40"><img src="/images/2011/content/guideInfo01_subimg04.gif" alt="이용자는 이용하려고 하는 저작물에 대한 권리정보를 손쉽게 확인할 수 있습니다. 권리자는 자신의 저작물 권리관계에 대한 정확한 정보를 확인할 수 있습니다. 또한, 미분배된 보상금을 확인하여 관련 신탁단체에 보상금 분배를 신청할 수 있습니다." /><span class="fr"><img src="/images/2011/content/QRCODE_devServer.gif" alt="QR코드(저작권찾기모바일)"/></span></p></div> 
                    	
					</div>
                    </div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>