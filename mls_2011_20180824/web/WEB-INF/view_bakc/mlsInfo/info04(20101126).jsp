<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권찾기소개 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(5);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_6.png" alt="저작권찾기소개" title="저작권찾기소개" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/main/main.do?method=info01">저작권찾기란?<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info02">저작권찾기의 필요성<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info03">저작권 미확인 저작물<br>이란?<span>&lt;</span></a></li>
							<li class="active"><a href="/main/main.do?method=info04">참여방법<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>저작권찾기소개</span>&gt;<strong>참여방법</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>참여방법
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<div class="section">
						<div class="tabStyle">
							<ul>
								<li><a href="/main/main.do?method=info04" class="active"><strong>저작권찾기 신청 이용안내</strong></a></li>
								<li><a href="/main/main.do?method=info05"><strong>보상금 신청 이용안내</strong></a></li>
							</ul>
						</div>
						<p class="mt10 mb10 gray">* 아래 썸네일 이미지를 클릭하면 해당 화면에 대한 확대 이미지를 조회할 수 있습니다.</p>
						<p>
							<img src="/images/2010/contents/info04_01.jpg" class="vtop mt20" alt="" usemap="#info4" />
							<map name="info4">
								<area shape="rect" coords="17,47,184,80" href="/main/main.do?method=info4_pop1" alt="저작물검색" target="_blank" onclick="window.open(this.href, '', 'width=660, height=640'); return false">
								<area shape="rect" coords="247,68,414,100" href="/main/main.do?method=info4_pop2" alt="저작물 상세조회" target="_blank" onclick="window.open(this.href, '', 'width=660, height=640'); return false">
								<area shape="rect" coords="248,223,414,255" href="/main/main.do?method=info4_pop3" alt="저작권 찾기 신청" target="_blank" onclick="window.open(this.href, '', 'width=660, height=850'); return false">
								<area shape="rect" coords="248,313,413,344" href="/main/main.do?method=info4_pop4" alt="결과내역 등록" target="_blank" onclick="window.open(this.href, '', 'width=660, height=640'); return false">
								<area shape="rect" coords="18,313,184,343" href="/main/main.do?method=info4_pop5" alt="결과내역 조회" target="_blank" onclick="window.open(this.href, '', 'width=660, height=700'); return false">
							</map>
						</p>
					</div>	
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>