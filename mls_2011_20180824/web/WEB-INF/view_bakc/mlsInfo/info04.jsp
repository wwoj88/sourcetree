<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_6.png" alt="이용안내" title="이용안내" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/main/main.do?method=info01">저작권찾기란?<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info02">저작권찾기의 필요성<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info03">저작권 미확인 저작물<br>이란?<span>&lt;</span></a></li>
							<li class="active"><a href="/main/main.do?method=info04">참여방법<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=goContactUs">이용문의<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>이용안내</span>&gt;<strong>참여방법</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>참여방법
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<div class="section">
						<ul>
							<li>
								<h4>권리자의 저작권찾기 신청-신탁단체 회원</h4>
								<div>
									<img src="/images/2010/contents/info04_3.jpg" longdesc="#info4" class="vtop" alt="" />
									
									<div class="skip" id="info4">
										<ol>
											<li>
												<h5>저작권찾기 사이트</h5>
												<ol>
													<li>신청인</li>
													<li>저작물(보상금)정보조회</li>
													<li>로그인</li>
													<li>보상금/저작권 찾기 신청</li>
												</ol>
											</li>
											<li>
												<h5>신탁관리단체</h5>
												<ol>
													<li>신청접수</li>
													<li>저작물 권리관계 확인</li>
													<li>보상금 분배 내역확인</li>
													<li>보상금 신청 결과 처리</li>
												</ol>
												<ol>
													<li>신청접수</li>
													<li>저작물 권리관계 확인</li>
													<li>저작권찾기 신청 결과 처리</li>
												</ol>
											</li>
										</ol>
									</div>
								</div>
							</li>
							<li class="mt20">
								<h4>권리자의 저작권찾기 신청-신탁단체 비회원</h4>
								<div>
									<img src="/images/2010/contents/info04_4.jpg" longdesc="#info5" class="vtop" alt="" />
									<div class="skip" id="info5">
										<ol>
											<li>
												<h5>저작권찾기 사이트</h5>
												<ol>
													<li>신청인</li>
													<li>저작물(보상금)정보조회</li>
													<li>로그인</li>
													<li>보상금/저작권 찾기 신청</li>
												</ol>
											</li>
											<li>
												<h5>신탁관리단체</h5>
												
												<h6>보상회원일 경우</h6>
												<ol>
													<li>신청/접수</li>
													<li>저작물 권리관계 확인</li>
													<li>보상금 분배 내역확인</li>
													<li>보상금 신청 결과 처리</li>
												</ol>
												
												<h6>비회원일 경우</h6>
												<ol>
													<li>신청접수</li>
													<li>저작물 권리관계 확인</li>
													<li>저작권 및 저작인접권 등록(www.crow.or.kr 02-2660-0002~3 한국저작권위원회 등록인증됨)</li>
												</ol>
											</li>
										</ol>
									</div>
								</div>
							</li>
							<li class="mt20">
								<h4>이용자가 저작물 이용을 위한 저작물 저작권찾기 신청</h4>
								<div>
									<img src="/images/2010/contents/info04_5.jpg"  longdesc="#info6" class="vtop" alt="" />
									
									<div class="skip" id="info6">
										<ol>
											<li>
												<h5>저작권찾기 사이트</h5>
												<ol>
													<li>신청인</li>
													<li>저작물(보상금)정보조회</li>
													<li>로그인</li>
													<li>보상금/저작권 찾기 신청</li>
												</ol>
											</li>
											<li>
												<h5>신탁관리단체</h5>
												
												<ol>
													<li>신청접수</li>
													<li>저작물 권리관계 확인</li>
													<li>처리완료</li>
												</ol>
											</li>
										</ol>
									</div>
								</div>
							</li>
						</ul>
					</div>	
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->

</body>
</html>