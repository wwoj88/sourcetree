<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권보호정책 | 저작권찾기</title>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
</head>

<body>
		<!-- HEADER str-->
		<jsp:include page="/include/2012/subHeader1.jsp" />
		<script type="text/javascript">initNavigation(1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title" style="padding:23px 0;">홈페이지<br />이용안내</div>
			<ul class="sub_lf_menu">
				<li><a href="/main/main.do?method=goUserMgnt">이용약관</a></li>
				<li><a href="/main/main.do?method=goHomeCopyright" class="on">저작권정책</a></li>
				<li><a href="/main/main.do?method=goSiteUs">홈페이지문의</a></li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				홈페이지 이용안내
				&gt;
				<span class="bold">저작권정책</span>
			</div>
			<div class="con_rt_hd_title">저작권정책</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<div class="sub05_02_con_rt_con">
					<p>저작권법 제24조의2에 따라 한국저작권위원회에서 저작재산권의 전부를 보유한 공공저작물의 경우에는 별도의 이용허락 없이 자유</p>
					<p class="mar_tp5">이용이 가능합니다. </p>
					<div class="mar_tp10">단, 자유이용이 가능한 자료는 "공공저작물 자유이용허락 표시기준(공공누리,KOGL) 제1유형 <img src="/images/sub_img/sub_57.gif" alt="그림" style="vertical-align:bottom" />"을 부착하여 개방하</p>
					<p class="mar_tp5">고 있으므로 공공누리 표시가 부착된 저작물인지를 확인한 이후에 자유이용 하시기 바랍니다. 자유이용의 경우에는 반드시 저작물의 </p>
					<p class="mar_tp5">출처를 구체적으로 표시하여야 합니다. </p></div>
					<p class="mar_tp30">공공누리가 부착되지 않은 자료들은 사용하고자 할 경우에는 담당자와 사전에 협의한 이후에 이용하여 주시기 바랍니다. </p>
					<h4 class="mar_tp30">문의 ▶ 한국저작권위원회 저작권상담센터 (1800-5455)</h4>
				</div>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
</body>
</html>
