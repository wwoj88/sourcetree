<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자료실 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }
//-->
</script>	
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(3);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<form name="form1" method="post" action="#">
			<input type="hidden" name="filePath">
			<input type="hidden" name="fileName">
			<input type="hidden" name="realFileName">
			<input type="submit" style="display:none;">
		</form>
		
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_3.gif" alt="저작권 자료실" title="저작권 자료실" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/main/main.do?method=worksInfo01"><img src="/images/2011/content/sub_lnb0301_off.gif" title="저작권 상식" alt="저작권 상식" /></a></li>
					<li id="lnb2"><a href="/main/main.do?method=worksInfo02"><img src="/images/2011/content/sub_lnb0302_off.gif" title="저작권 법령, 판례" alt="저작권 법령, 판례" /></a></li>
					<li id="lnb3"><a href="/main/main.do?method=worksInfo03"><img src="/images/2011/content/sub_lnb0303_off.gif" title="자유이용 저작물" alt="자유이용 저작물" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb2");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>저작권자료실</span><em>저작권 법령, 판례</em></p>
					<h1><img src="/images/2011/title/content_h1_0302.gif" alt="저작권 법령, 판례" title="저작권 법령, 판례" /></h1>
					
					<div class="section">
						<div class="w100">
                    	<img src="/images/2011/content/guideInfo05_subimg01.gif" alt="이용자 저작권찾기 신청 이미지" class="fl" />
                        	<div class="fr w70 mt10">
                        	
                        	<h2>저작권법</h2>
                        	
                            <p class="w100">
                              &nbsp;저작물에 대한 권리의식은 15세기 출판인쇄술의 발명으로 문서의 대량복제가 가능해지면서 태동되기 시작하였다. 
							그러나 아직 저작권이라는 권리 개념이 생긴 것은 아니었다. 그러다가 1684년 독일 황제의 칙령에 의하여 
							비로소 저작권이 권리로서 처음 인정받게 되었다.  그 이후 저작권은 세계 최초의 저작권법인 1709년 영국 앤여왕법
							이래 구미 각국에서 국내법으로 보호되어 왔으며, 오늘날은 문학적 예술적 저작물의 보호를 위한 베른협약이나 
							무역관련 지적소유권협정(TRIPs협정) 등을 통한 국제적 보호에까지 이르게 되었다.
							<br/><br/>
							&nbsp;우리나라에서의 저작권 보호는 1908년 대한제국 당시 한국저작권령(칙령 제200호)에서 처음으로 도입되었으나, 
							일본 저작권법을 의용(依用)한 데 불과한 것이었다. 그러나 이 칙령은 조선총독부와 군정 그리고 대한민국 정부 수립
							이후에까지 영향을 미쳐 우리의 저작권법이 모습을 드러낸 1957년 1월 28일까지 그 효력을 이어갔다. 
							1957년에 제정된 저작권법은 그 후 국내외의 저작권 환경 변화에 따른 몇 차례의 개정을 거쳐 오늘에 이르고 있다. 
							즉, 1986년의 전면개정과 TRIPs협정의 발효에 따라 동 협정 회원국으로서의 의무 이행을 위한 
							개정(1995. 12. 6. 공포, 법률 제5015호) 등 크고 작은 몇 차례의 개정을 거쳐 디지털 시대의 효율적인 저작권 보호를 
							위하여 개정된 저작권법(2004. 10. 16. 공포, 법률 제7233호)이 2000년 1월 17일부터 시행되고 있다. 
							<br/><br/>
							&nbsp;한편 1957년 법 제정 이래 잦은 개정으로 흐트러진 법체계를 바로 잡고, 저작물 이용환경의 변화를 반영하기 위한 
							저작권법이 전부 개정되어 2007. 6. 29일부터 시행되었다. 
							<br/><br/>
							&nbsp;저작권법은 총 11장으로 본문 142개조, 부칙 16개조를 합하여 모두 158개조로 구성되어 있으며, 
							제1조에서 “이 법은 저작자의 권리와 이에 인접하는 권리를 보호하고 저작물의 공정한 이용을 도모함으로써
							문화의 향상 발전에 이바지함을 목적으로 한다”고 그 목적을 명문으로 규정하고 있다. 이는 저작자의 창작 노력에 대한 
							경제적 보상과 인격 존중으로 저작자의 창작의욕을 고취시키고 또 한편으로는 저작물의 공정한 이용을 도모하여 
							그 원활한 이용을 촉진시킴으로써 궁극적으로 문화의 향수자인 국민의 복리 증진에 이바지하기 위함이다.
							</p>
     						
                           	<p class="floatDiv mt30"><img src="/images/2011/button/btn_app10_01.gif" alt="저작권법" /><span class="f1"><a href="#1" onclick="javascript:fn_fileDownLoad('G:/Server/mls/mls_2011/web/upload/form/','저작권법+[시행+2011.+7.+1].hwp','저작권법+[시행+2011.+7.+1].hwp')"><img src="/images/2011/button/btn_app10_05.gif" alt="다운로드1" /></a></span>
                            &nbsp;<img src="/images/2011/button/btn_app10_02.gif" alt="저작권법 시행령" /><span class="f1"><a href="#1" onclick="javascript:fn_fileDownLoad('G:/Server/mls/mls_2011/web/upload/form/','저작권법+시행령+[시행+2011.+7.+1].hwp','저작권법+시행령+[시행+2011.+7.+1].hwp')"><img src="/images/2011/button/btn_app10_05.gif" alt="다운로드2" /></a></span></p>
                           	<p class="floatDiv mt5"><img src="/images/2011/button/btn_app10_03.gif" alt="저작권법 시행규칙" /><span class="f1"><a href="#1" onclick="javascript:fn_fileDownLoad('G:/Server/mls/mls_2011/web/upload/form/','저작권법_시행규칙[시행+2009.+7.24].hwp','저작권법_시행규칙[시행+2009.+7.24].hwp')"><img src="/images/2011/button/btn_app10_05.gif" alt="다운로드3" /></a></span>
                            &nbsp;<img src="/images/2011/button/btn_app10_04.gif" alt="판례" /><span class="f1"><a href="#1" onclick="javascript:fn_fileDownLoad('G:/Server/mls/mls_2011/web/upload/form/','판례.hwp','판례.hwp')"><img src="/images/2011/button/btn_app10_05.gif" alt="다운로드4" /></a></span></p>     
                            </div>
                        </div>
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>