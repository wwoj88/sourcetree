<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>배너 자료 | 홍보자료 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js" type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript" language="javascript"
	src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.form1;
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
} 

//-->
</script>
<script> 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-69621660-1', 'auto');
ga('send', 'pageview');
</script>
</head>

<body>

		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader5.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(5);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->

<!-- CONTAINER str-->
<form name="form1" method="post" action="#"><input type="hidden"
	name="filePath"><input type="hidden" name="fileName"><input
	type="hidden" name="realFileName"><input type="submit"
	style="display:none;"></form>

<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title">홍보자료</div>
			<ul class="sub_lf_menu">
				<li><a href="/mlsInfo/linkList01.jsp">서비스 안내자료</a></li>
				<li><a href="/main/main.do?method=bannerList" class="on">배너 자료</a></li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				홍보자료
				&gt;
				<span class="bold">배너 자료</span>
			</div>
			<div class="con_rt_hd_title">배너 자료</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<div class="">
					<div class="sub05_02_con_rt_tp">배너사이즈 - 310x78 Pixel</div>
					<div class="sub05_02_con_rt_con">
						<div class="align_cen">
							<img src="/images/sub_img/sub_60.gif" alt="그림" /><br />
							<!-- 배너 링크 수정 -->
							<!-- <a type="application/octet-stream" target="_blank" download href="http://www.findcopyright.or.kr/images/2011/banner/banner_310_78.jpg"class="pop_check mar_tp20" >배너 다운로드</a> -->
							<SCRIPT type="text/javascript">
							/* $(function(){
								var agent = navigator.userAgent.toLowerCase();
								if (agent.indexOf("chrome") != -1) {
									for(var i = 0 ; i < 5 ; i++)
									{
										$('#exp'+i).css({'display':'none'});
									}
									
								}else{
									for(var i = 0 ; i < 5 ; i++)
									{
										$('#cro'+i).css({'display':'none'});	
									}
									
								}
							});
								  
							function downloadImage(imageUrl,fName,event){
									//alert(imageUrl+fName);
									
								    if(/msie|trident/i.test(navigator.userAgent)){ // IE 인지 체크

								      var _window = window.open(imageUrl+fName, '_blank');// 새창으로 열어서..

								       _window.document.close();

								      _window.document.execCommand('SaveAs', true, fName);// 저장하라, false 로 해도 동일

								      _window.close();// 끝나면 새창 닫음 

								    }else{

								     alert(event.target);
								      
								      $a[0].click();

								      $a.remove();
 
								    }

						    } */
							</SCRIPT>
							
							<!--  <a id="exp0" href="#1" onclick="downloadImage('http://www.findcopyright.or.kr/images/2011/banner/','banner_310_78.jpg')" class="pop_check mar_tp20">배너 다운로드</a>
							 <a id="cro0" href="http://www.findcopyright.or.kr/images/2011/banner/banner_310_78.jpg"class="pop_check mar_tp20" download>배너 다운로드</a>  -->
							 <a href="/board/board.do?method=fileDownLoad&&filename=banner_310_78.jpg" class="pop_check mar_tp20">배너 다운로드</a>
						</div>
						<div class="mar_tp20 color_ff6000">배너달기 소스 코드</div>
						<div class="mar_tp10">
							<textarea cols="" rows="" style="width:98%;padding:10px;line-height:20px;">&lt;a href="http://www.findcopyright.or.kr"&gt;
&lt;image src="http://www.findcopyright.or.kr/images/2011/banner/banner_310_78.jpg"&gt;&lt;/a&gt;</textarea>
						</div>
					</div>
				</div>
				<div class="mar_tp30">
					<div class="sub05_02_con_rt_tp">배너사이즈 - 310x78 Pixel</div>
					<div class="sub05_02_con_rt_con">
						<div class="align_cen">
							<img src="/images/sub_img/sub_61.gif" alt="그림" /><br />
							<%-- <a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.banner' />','배너 301_72_이미지.jpg','banner_301_72.jpg')" class="pop_check mar_tp20">배너 다운로드</a> --%>
							<!-- <a id="exp1" href="#1" onclick="downloadImage('http://www.findcopyright.or.kr/images/2011/banner/','banner_301_72.jpg')" class="pop_check mar_tp20">배너 다운로드</a>
							 <a id="cro1" href="http://www.findcopyright.or.kr/images/2011/banner/banner_301_72.jpg"class="pop_check mar_tp20" download>배너 다운로드</a> -->
							 <a href="/board/board.do?method=fileDownLoad&&filename=banner_301_72.jpg" class="pop_check mar_tp20">배너 다운로드</a>
						</div>
						<div class="mar_tp20 color_ff6000">배너달기 소스 코드</div>
						<div class="mar_tp10">
							<textarea cols="" rows="" style="width:98%;padding:10px;line-height:20px;">&lt;a href="http://www.findcopyright.or.kr"&gt;
&lt;image src="http://www.findcopyright.or.kr/images/2011/banner/banner_301_72.jpg"&gt;&lt;/a&gt;</textarea>
						</div>
					</div>
				</div>
				<div class="mar_tp30">
					<div class="sub05_02_con_rt_tp">배너사이즈 - 310x78 Pixel</div>
					<div class="sub05_02_con_rt_con">
						<div class="align_cen">
							<img src="/images/sub_img/sub_62.gif" alt="그림" /><br />
							<%-- <a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.banner' />','배너 268_57_이미지.jpg','banner_268_57.jpg')" class="pop_check mar_tp20">배너 다운로드</a> --%>
							<!-- <a id="exp2" href="#1" onclick="downloadImage('http://www.findcopyright.or.kr/images/2011/banner/','banner_268_57.jpg')" class="pop_check mar_tp20">배너 다운로드</a>
							 <a id="cro2" href="http://www.findcopyright.or.kr/images/2011/banner/banner_268_57.jpg"class="pop_check mar_tp20" download>배너 다운로드</a> -->
							 <a href="/board/board.do?method=fileDownLoad&&filename=banner_268_57.jpg" class="pop_check mar_tp20">배너 다운로드</a>
						</div>
						<div class="mar_tp20 color_ff6000">배너달기 소스 코드</div>
						<div class="mar_tp10">
							<textarea cols="" rows="" style="width:98%;padding:10px;line-height:20px;">&lt;a href="http://www.findcopyright.or.kr"&gt;
&lt;image src="http://www.findcopyright.or.kr/images/2011/banner/banner_268_57.jpg"&gt;&lt;/a&gt;</textarea>
						</div>
					</div>
				</div>
				<div class="mar_tp30">
					<div class="sub05_02_con_rt_tp">배너사이즈 - 310x78 Pixel</div>
					<div class="sub05_02_con_rt_con">
						<div class="align_cen">
							<img src="/images/sub_img/sub_63.gif" alt="그림" /><br />
							<%-- <a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.banner' />','배너 190_86_이미지.jpg','banner_190_86.jpg')" class="pop_check mar_tp20">배너 다운로드</a> --%>
							<!-- <a id="exp3" href="#1" onclick="downloadImage('http://www.findcopyright.or.kr/images/2011/banner/','banner_190_86.jpg')" class="pop_check mar_tp20">배너 다운로드</a>
							 <a id="cro3" href="http://www.findcopyright.or.kr/images/2011/banner/banner_190_86.jpg"class="pop_check mar_tp20" download>배너 다운로드</a> -->
							 <a href="/board/board.do?method=fileDownLoad&&filename=banner_190_86.jpg" class="pop_check mar_tp20">배너 다운로드</a>
						</div>
						<div class="mar_tp20 color_ff6000">배너달기 소스 코드</div>
						<div class="mar_tp10">
							<textarea cols="" rows="" style="width:98%;padding:10px;line-height:20px;">&lt;a href="http://www.findcopyright.or.kr"&gt;
&lt;image src="http://www.findcopyright.or.kr/images/2011/banner/banner_190_86.jpg"&gt;&lt;/a&gt;</textarea>
						</div>
					</div>
				</div>
				<div class="mar_tp30">
					<div class="sub05_02_con_rt_tp">배너사이즈 - 310x78 Pixel</div>
					<div class="sub05_02_con_rt_con">
						<div class="align_cen">
							<img src="/images/sub_img/sub_64.gif" alt="그림" /><br />
							<%-- <a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.banner' />','배너 150_116_이미지.jpg','banner_150_116.jpg')" class="pop_check mar_tp20">배너 다운로드</a> --%>
							<!-- <a id="exp4" href="#1" onclick="downloadImage('http://www.findcopyright.or.kr/images/2011/banner/','banner_150_116.jpg')" class="pop_check mar_tp20">배너 다운로드</a>
							<a id="cro4" href="http://www.findcopyright.or.kr/images/2011/banner/banner_150_116.jpg"class="pop_check mar_tp20" download>배너 다운로드</a> -->
							<a href="/board/board.do?method=fileDownLoad&&filename=banner_150_116.jpg" class="pop_check mar_tp20">배너 다운로드</a>
						</div>
						<div class="mar_tp20 color_ff6000">배너달기 소스 코드</div>
						<div class="mar_tp10">
							<textarea cols="" rows="" style="width:98%;padding:10px;line-height:20px;">&lt;a href="http://www.findcopyright.or.kr"&gt;
&lt;image src="http://www.findcopyright.or.kr/images/2011/banner/banner_150_116.jpg"&gt;&lt;/a&gt;</textarea>
						</div>
					</div>
				</div>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
<!-- //CONTAINER end --> 
	<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
	<!-- FOOTER end -->
</html>
