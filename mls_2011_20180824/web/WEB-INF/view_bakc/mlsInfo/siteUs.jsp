<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>홈페이지문의 | 홈페이지 이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
</head>

<body>
		<!-- HEADER str-->
		<jsp:include page="/include/2012/subHeader4.jsp" />
		<script type="text/javascript">initNavigation(4);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title" style="padding:23px 0;">홈페이지<br />이용안내</div>
			<ul class="sub_lf_menu">
				<li><a href="/main/main.do?method=goUserMgnt">이용약관</a></li>
				<li><a href="/main/main.do?method=goHomeCopyright">저작권정책</a></li>
				<li><a href="/main/main.do?method=goSiteUs" class="on">홈페이지문의</a></li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				홈페이지 이용안내
				&gt;
				<span class="bold">홈페이지문의</span>
			</div>
			<div class="con_rt_hd_title">홈페이지문의</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<h2 class="sub_con_h2">한국저작권위원회</h2>
				<table class="sub_tab3 mar_tp20" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
					<caption>한국저작권위원회</caption>
					<colgroup>
						<col width="19%"/>
						<col width="*"/>
						<col width="25%"/>
						<col width="30%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col">담당업무</th>
							<th scope="col">담당자</th>
							<th scope="col">전화번호</th>
							<th scope="col">E-mail</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>유통진흥팀</td>
							<td>유동헌</td>
							<td>055-792-0122</td>
							<td>dhyou@copyright.or.kr</td>
						</tr>
					</tbody>
				</table>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
</body>
</html>