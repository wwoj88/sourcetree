<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>개인정보보호정책 | 저작권찾기</title>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(10);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container"  style="background: url(/images/2012/content/container_vis8.gif) no-repeat 100% 0;">
			<div class="container_vis"  style="background: url(/images/2012/content/container_vis8.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_8.gif" alt="홈페이지이용안내" title="홈페이지이용안내" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<jsp:include page="/include/2012/mlsInfoLeft.jsp" />
				<script type="text/javascript">subSlideMenu("sub_lnb","lnb2");</script>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>홈페이지 이용안내</span><em>개인정보처리방침</em></p>
					<h1><img src="/images/2012/title/content_h1_0802.gif" alt="개인정보처리방침" title="개인정보처리방침" /></h1>
					
					<div class="section">
				      		<div class="white_box mt0">
	                                          <div class="box5">
		                                          <div class="box5_con">
				                            	<p>한국저작권위원회(이하 ‘위원회’)의 저작권찾기 사이트 이용에 대해 감사드립니다. 위원회는 개인정보보호법 등 관련 법령상의 규정을 준수하며, 저작권찾기 사이트에서 취급하는 모든 개인정보는 관련 법령에 근거하거나 정보주체의 동의에 의하여 수집·보유 및 처리되고 있습니다. </p>
				                                   <p class="mt10">저작권찾기 사이트는「개인정보보호법」에 따라 이용자의 개인정보 보호 및 권익을 보호하고 개인정보와 관련한 이용자의 고충을 원활하게 처리할 수 있도록 다음과 같은 개인정보 처리방침을 두고 있습니다. </p>
				                                   <p class="mt10">본 방침은 2012년 10월 1일부터 시행됩니다.</p>
		                                          </div>
	                                          </div>
                                         </div>
                                         
                                         <div id="style2" class="mt20">
                                         		<h2>1. 개인정보의 처리 목적</h2>
                                         		<p class="ml10">위원회는 개인정보를 다음의 목적을 위해 관리합니다. 관리중인 개인정보는 다음의 목적이외의 용도로는 이용되지 않으며 이용 목적이 변경될 시에는 사전동의를 구할 예정입니다.</p>
                                         		
                                         		<ul class="ml10 mt10">
                                         		<li>가. 서비스 제공 <p>- 음악, 어문, 방송등 저작물 정보 제공 등 서비스 제공에 관련한 목적으로 개인정보를 처리합니다. </p></li>
                                         		<li>나. 회원 가입 및 관리 <p>- 회원제 서비스 이용에 따른 본인확인, 개인 식별, 불량회원의 부정이용 방지와 비인가 사용방지, 공지사항 전달 등을 목적으로 개인정보를 처리합니다. </p></li>
                                         		</ul>
                                         		
                                         		<h2 class="mt20">2. 개인 정보의 수집 및 보유</h2>
                                         		<ul class="ml10 mt10">
                                         		<li>가. 위원회는 법령의 규정과 정보주체의 동의에 의해서만 개인 정보를 수집보유합니다. 위원회가 법령의 규정에 근거하여 수집ㆍ보유하고 있는 주요 개인 정보파일은 다음과 같습니다. 
                                         			<span class="topLine2 mt5"></span>
                                         			<table border="1" class="grid mb10" summary="">
                                         				<thead>
                                         					<tr>
                                         						<th class="bgNone black">개인정보 파일명</th>
                                         						<th class="bgNone black">저작권찾기 사이트 회원정보</th>
                                         					</tr>
                                         				</thead>
                                         				<tbody>
                                         					<tr>
                                         						<td>개인정보 항목</td>
                                         						<td>성명, 주민번호, 전화번호, 주소, 이메일</td>
                                         					</tr>
                                         					<tr>
                                         						<td>운영 목적</td>
                                         						<td>법정허락 이용승인 신청 및 저작권자 찾기 상당한 노력 신청 </td>
                                         					</tr>
                                         					<tr>
                                         						<td>운영 근거</td>
                                         						<td>저작권법 기준으로 개인정보보호법 제15조(개인정보수집이용)저작권법  제120조(저작권정보센터) 및 시행령 제66조(저작권정보센터 조직 및 운영등)저작권법 제50(저작재산권자 불명인 저작물의 이용) 및 시행령 제18조(상당한 노력기준)</td>
                                         					</tr>
                                         					<tr>
                                         						<td>보유기간</td>
                                         						<td>회원탈퇴 시 까지</td>
                                         					</tr>
                                         				</tbody>
                                         			</table>
                                         		</li>
                                         		<li>나. 홈페이지 이용 시 자동으로 수집ㆍ저장되는 정보
                                         			<ul class="mt10">
                                         			<li class="pl0 bgNone black">홈페이지의 개선과 보완을 위한 통계분석ㆍ이용자와 웹사이트 간의 원활한 의사소통 등을 위하여 사용하기 위해 다음의 정보는 자동으로 수집ㆍ저장됩니다.
                                         				<ul class="mt5 mb10">
                                         				<li class="pl0 bgNone black2 p11">- 이용자의 인터넷서버 도메인명과 우리 웹사이트를 방문할 때 거친 웹 사이트의 주소, 방문  일시 등</li>
                                         				<li class="pl0 bgNone black2 p11">- 이용자의 브라우저 종류 및 OS</li>
                                         				</ul>
                                         			</li>
                                         			<li class="pl0 bgNone black">위원회는 보유하고 있는 개인정보를 관계법령에 따라 적법하고 적정하게 처리하여 권익이 침해받지 않도록 노력할 것입니다. 다만, 관련법령 등에 따라 부분적으로 공개될 수도 있음을 유념하시기 바랍니다.</li>
                                         			</ul>
                                         		</li>
                                         		</ul>
                                         		
                                         		<h2 class="mt20">3. 개인정보의 이용 및 제공</h2>
                                         		<ul class="ml10 mt10">
                                         		<li>가. 개인정보의 제3자 제공
                                         			<ul class="mt5">
                                         			<li class="pl0 bgNone black">위원회에서 제공하는 개인정보를 제공받은 기관은 우리 위원회의 동의 없이 타 기관에 제공할 수 없습니다.</li>
                                         			<li class="pl0 bgNone black">우리 위원회는 고지하거나 동의한 범위를 초과하여 회원의 개인정보를 이용하거나 제3자에게 제공 또는 공개하지 않습니다. 다만 법률의 규정에 의한 정보 제공의 경우나 기타 법령이 허용한 범위 내에서는 예외로 합니다.</li>
                                         			</ul>
                                         			
                                         		</li>
                                         		<li>나. 개인정보의 위탁
                                         			<ul class="mt5">
                                         			<li class="pl0 bgNone black">위원회에서 관리하는 개인 정보의 처리를 다른 전문기관에 위탁하는 경우, 「개인정보보호법」제26조(업무위탁에 따른 개인정보의 처리 제한)에 따라 필요한 제한이나 절차를 정하고 수탁 기관으로 하여금 준수토록 하고 있으며, 실태 점검도 실시하고 있습니다.</li>
                                         			</ul>
                                         		</li>
                                         		<li>다. 이용 및 제공의 제한
                                         			<ul class="mt5">
                                         			<li class="pl0 bgNone black">위원회가 수집ㆍ보유하고 있는 개인 정보는 이용 및 제공을 엄격하게 제한하고 있습니다. 「개인정보보호법」제18조(개인정보의 이용ㆍ제공의 제한)는 예외 경우에 관하여 다음과 같이 규정하고 있습니다.
                                         				<ul class="mt5 mb10">
                                         				<li class="pl0 bgNone black2 p11">1. 정보주체로부터 별도의 동의를 받은 경우</li>
                                         				<li class="pl0 bgNone black2 p11">2. 다른 법률에 특별한 규정이 있는 경우</li>
                                         				<li class="pl0 bgNone black2 p11">3. 정보주체 또는 그 법정대리인이 의사표시를 할 수 없는 상태에 있거나 주소불명 등으로 사전 동의를 받을 수 없는 경우로서 명백히 정보주체 또는 제3자의 급박한 생명, 신체, 재산의 이익을 위하여 필요하다고 인정되는 경우</li>
                                         				<li class="pl0 bgNone black2 p11">4. 통계작성 및 학술연구 등의 목적을 위하여 필요한 경우로서 특정 개인을 알아볼 수 없는 형태로 개인정보를 제공하는 경우</li>
                                         				<li class="pl0 bgNone black2 p11">5. 개인정보를 목적 외의 용도로 이용하거나 이를 제3자에게 제공하지 아니하면 다른 법률에서 정하는 소관 업무를 수행할 수 없는 경우로서 보호위원회의 심의ㆍ의결을 거친 경우</li>
                                         				<li class="pl0 bgNone black2 p11">6. 조약, 그 밖의 국제협정의 이행을 위하여 외국정부 또는 국제기구에 제공하기 위하여 필요한 경우</li>
                                         				<li class="pl0 bgNone black2 p11">7. 범죄의 수사와 공소의 제기 및 유지를 위하여 필요한 경우</li>
                                         				<li class="pl0 bgNone black2 p11">8. 형(刑) 및 감호, 보호처분의 집행을 위하여 필요한 경우</li>
                                         				</ul>
                                         			</li>
                                         			</ul>
                                         		</li>
                                         		</ul>
                                         		
                                         		<h2 class="mt20">4. 개인정보의 파기</h2>
                                         		<p class="ml10">이용자의 개인정보는 원칙적으로 개인정보 처리목적이 달성되면 지체 없이 해당 개인정보를 파기합니다. 위원회의 개인정보 파기절차 및 방법은 다음과 같습니다.</p>
                                         		<ul class="ml10 mt10">
                                         		<li>가. 파기절차
                                         			<ul class="mt5">
                                         			<li class="pl0 bgNone black">이용자가 회원가입 등을 위해 입력한 정보는 목적 달성 후 내부 방침 및 관련 법령에 따라 일정기간 저장된 후 혹은 즉시 파기됩니다.</li>
                                         			<li class="pl0 bgNone black">동 개인정보는 법률에 의한 경우가 아니고서는 보유되는 이외의 다른 목적으로 이용되지 않습니다.</li>
                                         			</ul>
                                         			
                                         		</li>
                                         		<li>나. 파기방법
                                         			<ul class="mt5">
                                         			<li class="pl0 bgNone black">종이에 출력된 개인정보는 분쇄기로 분쇄하거나 소각을 통하여 파기합니다.</li>
                                         			<li class="pl0 bgNone black">전자적 파일 형태로 저장된 개인정보는 기록을 재생할 수 없는 기술적 방법을 사용하여 파기합니다.</li>
                                         			</ul>
                                         		</li>
                                        	 	</ul>
                                        	 	
                                        	 	<h2 class="mt20">5. 정보주체의 권리 보장</h2>
                                        	 	<p class="ml10">「개인정보보호법」 제5장 정보주체의 권리 보장에 따라 정보주체는 자신의 개인정보 처리와 관련하여 다음 각 호의 권리를 가지고 있습니다.</p>
                                         		<ul class="ml10 mt10">
                                         		<li> 1. 개인정보의 열람</li>
                                         		<li> 2. 개인정보의 정정 및 삭제</li>
                                         		<li>3. 개인정보의 처리정지 등</li>
                                         		<li>4. 권리행사의 방법 및 절차</li>
                                         		<li>5. 손해배상책임</li>
                                        	 	</ul>
                                        	 	
                                        	 	<h2 class="mt20">6. 개인정보 분쟁조정위원회</h2>
                                         		<p class="ml10">「개인정보보호법」 제6장 개인정보 분쟁조정위원회에 따라 개인정보 주체는 본인의 개인정보정보와 관련하여 수집자와 분쟁이 발생시 정부산하 “개인정보 분쟁조정위원회”에 조정을 신청할 수 있습니다.</p>
                                         		
                                         		<h2 class="mt20">7. 개인정보파일의 열람, 정정, 삭제 및 처리정지 청구</h2>
                                         		<p class="ml10">개인정보보호법 제35조(개인정보의 열람), 제36조(개인정보의 정정ㆍ삭제), 제37조(개인정보의 처리정지 등)에 따라 정보주체는 위원회가 보유하고 있는 개인정보의 열람 및 정정, 삭제 및 처리정지를 청구할 수 있습니다.</p>
                                         		<span class="topLine2 mt5"></span>
                                         		<table border="1" class="grid mb10" summary="">
                                         			<tbody>
                                         				<tr>
                                         					<th>청구장소</th>
                                         					<td>한국저작권위원회 6층 정보화정책팀</td>
                                         				</tr>
                                         				<tr>
                                         					<th>청구방법</th>
                                         					<td>개인정보파일대장의 열람청구부서로 서면 청구</td>
                                         				</tr>
                                         				<tr>
                                         					<th>청구 관련 문의</th>
                                         					<td>담당자 : 정보화정책팀 노지용<br />연락처 : 02-2660-0055</td>
                                         				</tr>
                                         			</tbody>
                                         		</table>
                                         		
                                         		<ul class="ml10 mt10">
                                         		<li>가. 개인정보의 열람
                                         			<ul class="mt5">
                                         			<li class="pl0 bgNone black">위원회가 보유하고 있는 개인정보파일은 「개인정보보호법」 및 관련 법령의 규정이 정하는 바에 따라 본인에 한하여 열람을 청구할 수 있으며 청구 절차는 다음과 같습니다.
                                         				<p class="mt5 mb10"><img src="/images/2012/content/persmgnt1.gif" alt="" /></p>
                                         				<p>단, 다음의 사항은「개인정보보호법」제35조(개인정보의 열람) 제4항 규정에 의하여 열람을 제한할 수 있습니다.</p>
                                         				
                                         				<ul class="mt5 mb10">
                                         				<li class="pl0 bgNone black2 p11">1. 법률에 따라 열람이 금지되거나 제한되는 경우</li>
                                         				<li class="pl0 bgNone black2 p11">2. 다른 사람의 생명·신체를 해할 우려가 있거나 다른 사람의 재산과 그 밖의 이익을 부당하게 침해할 우려가 있는 경우</li>
                                         				<li class="pl0 bgNone black2 p11">3. 공공기관이 다음 각 목의 어느 하나에 해당하는 업무를 수행할 때 중대한 지장을 초래하는 경우
                                         					<ul>
                                         					<li class="pl0 bgNone black2 p11">가) 조세의 부과·징수 또는 환급에 관한 업무</li>
                                         					<li class="pl0 bgNone black2 p11">나) 「초·중등교육법」 및 「고등교육법」에 따른 각급 학교, 「평생교육법」에 따른 평생교육시설, 그 밖의 다른 법률에 따라 설치된 고등교육기관에서의 성적 평가 또는 입학자 선발에 관한 업무</li>
                                         					<li class="pl0 bgNone black2 p11">다) 학력·기능 및 채용에 관한 시험, 자격 심사에 관한 업무</li>
                                         					<li class="pl0 bgNone black2 p11">라) 보상금·급부금 산정 등에 대하여 진행 중인 평가 또는 판단에 관한 업무</li>
                                         					<li class="pl0 bgNone black2 p11">마) 다른 법률에 따라 진행 중인 감사 및 조사에 관한 업무 </li>
                                         					</ul>
                                         				</li>
                                         				</ul>
                                         			</li>
                                         			</ul>
                                         		</li>
                                         		<li>나. 개인정보의 정정ㆍ삭제 및 처리정지
                                         			<ul class="mt5">
                                         			<li class="pl0 bgNone black">
                                         				<p>위원회가 보유하고 있는 개인정보파일은 「개인정보보호법」 및 관련 법령의 규정이 정하는 바에 따라 본인에 한하여 정정ㆍ삭제 또는 처리정지를 요구할 수 있습니다. 다만, 다른 법령에서 그 개인정보가 수집 대상으로 명시되어 있는 경우에는 그 삭제를 요구할 수 없으며, 처리정지 요구를 거절할 수 있습니다.</p>
                                         				
                                         				<ul class="mt5 mb10">
                                         				<li class="pl0 bgNone black2 p11">1. 법률에 특별한 규정이 있거나 법령상 의무를 준수하기 위하여 불가피한 경우</li>
                                         				<li class="pl0 bgNone black2 p11">2. 다른 사람의 생명 ·신체를 해할 우려가 있거나 다른 사람의 재산과 그 밖의 이익을 부당하게 침해할 우려가 있는 경우</li>
                                         				<li class="pl0 bgNone black2 p11">3. 공공기관이 개인정보를 처리하지 아니하면 다른 법률에서 정하는 소관 업무를 수행할 수 없는 경우</li>
                                         				<li class="pl0 bgNone black2 p11">개인정보를 처리하지 아니하면 정보주체와 약정한 서비스를 제공하지 못하는 등 계약의 이행이 곤란한 경우로서 정보주체가 그 계약의 해지 의사를 명확하게 밝히지 아니한 경우 정정ㆍ삭제 또는 처리정지 청구 절차는 다음과 같습니다.
                                         					<p class="mt5 mb10"><img src="/images/2012/content/persmgnt2.gif" alt="" /></p>
                                         				</li>
                                         				</ul>
                                         			</li>
                                         			</ul>
                                         		</li>
                                        	 	</ul>
                                         			
                                         		
                                         		<h2 class="mt20">8. 개인정보의 안전성 확보 조치</h2>
                                         		<p class="ml10">위원회는 이용자들의 개인정보를 취급함에 있어 개인정보가 분실, 도난, 누출, 변조 또는 훼손되지 않도록 안전성 확보를 위하여 다음과 같은 기술적/관리적/물리적 보호 대책을 강구하고 있습니다.</p>
                                         		
                                         		<ul class="ml10 mt10">
                                         		<li>가. 비밀번호 암호화 
                                         			<p>회원의 비밀번호는 암호화되어 저장 및 관리되고 있어 본인만이 알고 있으며, 개인정보의 확인 및 변경도 비밀번호를 알고 있는 본인에 의해서만 가능합니다</p>
                                         		</li>
                                         		<li>나. 해킹 등에 대비한 대책
                                         			<p>위원회는 해킹이나 컴퓨터 바이러스 등에 의해 회원의 개인정보가 유출되거나 훼손되는 것을 막기 위해 최선을 다하고 있습니다. 
    개인정보의 훼손에 대비해서 자료를 수시로 백업하고 있고, 최신 백신프로그램을 이용하여 이용자들의 개인정보나 자료가 누출되거나 손상되지 않도록 방지하고 있으며, 암호화통신 등을 통하여 네트워크상에서 개인정보를 안전하게 전송할 수 있도록 하고 있습니다. 
    또한, 외부로부터 접근이 통제된 구역에 시스템을 설치하고 기술적/물리적으로 감시 및 차단하고 있습니다.</p>
                                         		</li>
                                         		<li>다. 개인정보에 대한 접근 제한
                                         			<p>개인정보를 처리하는 데이터베이스시스템에 대한 접근권한의 부여, 변경, 말소를 통하여 개인정보에 대한 접근통제를 위하여 필요한 조치를 하고 있으며 침입차단시스템을 이용하여 외부로부터의 무단 접근을 통제하고 있습니다.</p>
                                         		</li>
                                         		<li>라. 비인가자에 대한 출입 통제
                                         			<p>개인정보를 보관하고 있는 시스템을 운영하는 전산실 및 등록물을 보관중인 등록실은 출입통제 절차를 수립 및 운영하고 있습니다.</p>
                                         		</li>
                                         		<li>마. 취급 직원의 최소화 및 교육
                                         			<p>위원회의 개인정보관련 취급 직원은 담당자에 한정시키고 있고, 이를 위한 별도의 비밀번호를 부여하여 정기적으로 갱신하고 있으며, 담당자에 대한 수시 교육을 통하여 위원회 개인정보취급방침의 준수를 항상 강조하고 있습니다. </p>
                                         		</li>
                                      	   	</ul>
                                      	   	
                                      	   	
                                      	   	<h2 class="mt20">9. 개인정보보호책임자등의 지정 안내</h2>
                                      	   	<p class="ml10">위원회는 개인정보를 보호하고 개인정보와 관련한 불만을 처리하기 위하여 아래와 같이 개인정보 보호책임자 및 실무담당자를 지정하고 있습니다.(「개인정보보호법」 제31조제1항에 따른 개인정보보호책임자)</p>
                                         		
                                         		<span class="topLine2 mt5"></span>
                                         		<table border="1" class="grid mb10" summary="">
                                         			<thead>
                                         				<tr>
                                         					<th colspan="2" class="bgNone black">개인정보보호책임자</th>
                                         					<th colspan="2" class="bgNone black">개인정보처리담당</th>
                                         				</tr>
                                         			</thead>
                                         			<tbody>
                                         				<tr>
                                         					<th>부  서</th>
                                         					<td>저작권정보센터</td>
                                         					<th>팀  명</th>
                                         					<td>유통진흥팀</td>
                                         				</tr>
                                         				<tr>
                                         					<th>성  명</th>
                                         					<td>여정호</td>
                                         					<th>성  명</th>
                                         					<td>한호</td>
                                         				</tr>
                                         				<tr>
                                         					<th>전  화</th>
                                         					<td>02-2660-0120</td>
                                         					<th>전  화</th>
                                         					<td>02-2660-0121</td>
                                         				</tr>
                                         				<tr>
                                         					<th>이메일</th>
                                         					<td>jhyeo@copyright.or.kr</td>
                                         					<th>이메일</th>
                                         					<td>hanho@copyright.or.kr</td>
                                         				</tr>
                                         			</tbody>
                                         		</table>
                                         		
                                         		<h2 class="mt20">10. 권익침해 구제방법 안내</h2>
                                      	   	<p class="ml10">정보주체는 개인정보침해로 인한 구제를 받기 위하여 개인정보분쟁조정위원회, 한국인터넷진흥원 개인정보침해신고센터 등에 분쟁해결이나 상담 등을 신청할 수 있습니다. 이 밖에 기타 개인정보침해의 신고, 상담에 대하여는 아래의 기관에 문의하시기 바랍니다.</p>
                                      	   	
                                      	   	<ul class="ml10 mt10">
                                      	   	<li>1. 개인분쟁조정위원회 : (국번없이)118 <p> → 1. 한국인터넷진흥원(개인정보분쟁조정위원회) : (국번없이)118(http://privacy.kisa.or.kr)</p></li>
                                      	   	<li>2. 대검찰청 첨단범죄수사과 : 02-3480-2000(http://www.spo.go.kr)</li>
                                      	   	<li>3. 경찰청 사이버테러대응센터 : 02-1566-0112(http://www.netan.go.kr)</li>
                                      	   	</ul>
                                         		
                                         		<p class="strong mt20 ml10 p14">* 2012년 10월 01일 시행 <a href="" class="underline blue">개인정보처리방침(클릭)</a>  <- 변경전 개인정보처리방침 </p>
                                         </div>
                                  </div>
                          </div>
                  </div>
          </div>           
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>