<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자료실 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(3);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_3.gif" alt="저작권 자료실" title="저작권 자료실" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/main/main.do?method=worksInfo01"><img src="/images/2011/content/sub_lnb0301_off.gif" title="저작권 상식" alt="저작권 상식" /></a></li>
					<li id="lnb2"><a href="/main/main.do?method=worksInfo02"><img src="/images/2011/content/sub_lnb0302_off.gif" title="저작권 법령, 판례" alt="저작권 법령, 판례" /></a></li>
					<li id="lnb3"><a href="/main/main.do?method=worksInfo03"><img src="/images/2011/content/sub_lnb0303_off.gif" title="자유이용 저작물" alt="자유이용 저작물" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb3");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>저작권자료실</span><em>자유이용 저작물</em></p>
					<h1><img src="/images/2011/title/content_h1_0303.gif" alt="자유이용 저작물" title="자유이용 저작물" /></h1>
					
					<div class="section">
						<div class="w100">
                    	<img src="/images/2011/content/guideInfo03_subimg01.gif" alt="이용자 저작권찾기 신청 이미지" class="fl" />
                        	<div class="fr w70 mt10">
                        	
                        	<h2>자유이용저작물</h2>
                        	
                            <p class="w100">
                              &nbsp;"자유이용저작물"이란 저작권 보호 기간이 지나서 저작권이 소멸되었거나 저작권이 기증된 저작물
							     또는 저작권자가 무료로 이용하도록 허락한 저작물을 말합니다.
							  <br/>  <br/>
							   &nbsp;저작권 만료 저작물과 이용허락표시 저작물은 별도의 이용 허락 또는 승인절차를
							     거치지 않고 무료로 이용할 수 있으나 저작권 기증 저작물은 한국저작권위원회로부터
							     승인 받아 무료로 이용할 수 있습니다.
							   <br/>  <br/>
							   &nbsp;<b>자유이용사이트</b>에서는 자유이용저작물을 제공하고 있습니다. 저작권자에게는 저작재산권 뿐만 아니라 저작인격권도 있습니다. 
							     저작인격권이란 공표권, 성명표시권, 동일성유지권으로 구분하며, 양도나 포기가 불가능한 저작자 일신에 전속하는 권리로
							     그러므로 자유이용사이트에서 제공하는 저작물 이용 시, 저작자의 성명을 표시해야 하며 내용의 변형을 크게 가하지 않아야 합니다.
							</p>
     
                            <p class="mt10"><a href="http://freeuse.copyright.or.kr/" target="_blank"><img title="자유이용 사이트 바로가기" alt="자유이용 사이트 바로가기" src="/images/2011/button/btn_goFreeSite.gif"></a></p>
                            
                            
                            <h2 class="mt30">오픈소스SW라이선스</h2>
                        	
                            <p class="w100">
                              &nbsp;"오픈소스SW 라이선스"란 오픈소스SW 개발자와 이용자간에 사용 방법 및 조건의 범위를 명시한 계약으로
							    오픈소스SW를 이용하기 위해서는 개발자가 규정한 라이선스를 지켜야 하며, 이를 위반할 경우에는 
							    라이선스 위반 및 저작권 침해가 발생하고, 이에 대한 처벌을 받게 됩니다.
							  <br/><br/>
							    &nbsp;이런 오픈소스SW 라이선스는 기본적으로 사용자의 자유로운 사용, 수정, 배포를 보장하고 있으며 
							    오픈소스SW가 이와 같은 라이선스를 만들어서 운영하는 이유는 오픈소스SW를 이용하여 개발한 SW를 
							    법의 테두리 안에서 소스코드 공개를 강제하기 위함입니다.
							  <br/><br/>
							  &nbsp;<b>OLIS</b>(Open Source License Information System)는 웹 상에서 손쉽게 다운받아 사용(사용 제작 재배포)하고 있는 
							    오픈소스SW에 대해 보다 전문적인 라이선스 준수사항과 관련 국 내외 최신 정보를 한글화하여 제공함으로써 
							    국내 오픈소스SW 사용기관 업체 및 SW개발자의 편의를 증진하고 관련 SW산업 발전을 위해 제공하는 
							    무료 오픈소스SW 라이선스 웹 사이트입니다. </p>
    
    						         <p class="mt10"><a href="http://www.olis.or.kr/" target="_blank"><img title="OLIS 바로가기" alt="OLIS 바로가기" src="/images/2011/button/btn_goOlis.gif"></a></p>
     
                            </div>
                        </div>
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>