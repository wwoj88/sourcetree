<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>홈페이지문의 | 홈페이지 이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(10);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis8.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis8.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_8.gif" alt="홈페이지이용안내" title="홈페이지이용안내" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<jsp:include page="/include/2012/mlsInfoLeft.jsp" />
				<script type="text/javascript">subSlideMenu("sub_lnb","lnb4");</script>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>홈페이지 이용안내</span><em>홈페이지문의</em></p>
					<h1 title="저작물에 대한 주민의식이 필요합니다!"><img src="/images/2012/title/content_h1_0804.gif" alt="홈페이지문의" title="홈페이지문의" /></h1>

                    <!-- 테이블 리스트 Set -->
					  <div class="section mt20">
                      
                      <h3 class="fl">한국저작권위원회</h3><br>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="한국저작권위원회 안내 표입니다." class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<caption>한국저작권위원회</caption>
								<colgroup>
								<col width="20%">
								<col width="20%">
								<col width="*">
								<col width="30%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">담당업무</th>
										<th scope="col">담당자</th>
										<th scope="col">전화번호</th>
										<th scope="col">E-mail</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce">유통진흥팀</td>
										<td class="ce">강호기 PM</td>
										<td class="ce">02-2669-0074</td>
										<td class="ce">kanghogi@copyright.or.kr</td>
									</tr>
									
								</tbody>
							</table>
							<!-- //그리드스타일 -->
                            </div>
                            <div class="section mt20">
                            
						</div>
						<!-- //테이블 리스트 Set -->
                    
                    	
						
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>