<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권찾기소개 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(5);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_6.png" alt="저작권찾기소개" title="저작권찾기소개" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li class="active"><a href="/main/main.do?method=info01">저작권찾기란?<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info02">저작권찾기의 필요성<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info03">저작권 미확인 저작물<br>이란?<span>&lt;</span></a></li>
							<li><a href="/main/main.do?method=info04">참여방법<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>저작권찾기소개</span>&gt;<strong>저작권찾기란?</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>저작권찾기란?
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<div class="section">
						<p><img src="/images/2010/contents/infoImg1.gif" alt="Welcome 저작권찾기에 오신 것을 환영합니다." title="Welcome 저작권찾기에 오신 것을 환영합니다." /></p>
						<p class="gray ml30">우리가 이용하는 저작물 중 상당수는 아직까지 권리관계가 명확하게 밝혀지지 않았습니다. <br />
누가 작사, 작곡, 노래, 연주, 음반 제작을 했는지, 시, 소설, 수필 등 어문저작물의 권리자는 누구인지, <br />
또한 사진, 그림 등의 이미지 저작권자는 누구인지 그 권리관계가 확실히 밝혀지지 않아, <br />
저작물 유통이나 권리자에 대한 보상이 원활히 이루어지지 않고 있습니다.</p>
						<p class="ml30 mt10 line15"><strong class="blue"><em class="black fontLarge strong">‘저작권찾기’란</em> 각 저작권자가 저작물에 대한 권리를 찾고 보상금 신청을 함으로써 권리자로서의<br />  정당한 보상을 받도록 하는 사이트입니다. </strong></p>
						<p class="gray ml30 mt10">‘저작권찾기 사이트(<spring:message code='domain.realhome' />)'를 통해 각 권리자들은 인터넷 상으로 쉽게 자신의 권리를 확인하고, <br />
보상금을 신청할 수 있습니다.</p>
					</div>	
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
