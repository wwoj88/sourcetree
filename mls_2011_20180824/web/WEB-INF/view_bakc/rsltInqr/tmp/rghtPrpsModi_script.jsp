<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(저작권찾기) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<style type="text/css">
<!--
body {
	overflow-x: auto;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript">
<!--

window.name = "rghtPrpsModi_script";

// 음악저작물 상세 팝업오픈
function openScriptDetail( crId ) {

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=C'+param
	var name = '';
	var openInfo = 'target=rghtPrpsModi_script width=705, height=500';
	
	window.open(url, name, openInfo);
}

// 파일다운로드
function fn_fileDownLoad(filePath, fileName, realFileName) {
		
		var frm = document.fileForm;
		
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.target="rghtPrpsModi_script";
		//frm.action = "/rsltInqr/rsltInqr.do?method=fileDownLoad";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.method="post";
		frm.submit();
}
  
// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	
	
	var the_height = document.getElementById(name).offsetHeight;
	var chkObjs = document.getElementsByName("chk");
	  
	document.getElementById(targetName).style.height = the_height + 13 + "px" ;
	  

}

// 스크롤셋팅
function scrollSet(name, targetName, oRowName){
	
	var totalRow = document.getElementsByName(oRowName).length;
	
	// 세로 기준건수 이상인 경우 세로스크롤 제어
	if( totalRow < 6) {
		resizeDiv(name, targetName);
		document.getElementById(targetName).style.overflowY = "hidden";
	}
	
	if( totalRow > 5 ) {
		document.getElementById(targetName).style.overflowY = "auto";
	}
}


// 토탈건수 set
function setTotCnt( oRowName, oSpanId) {

	var totalRow = document.getElementsByName(oRowName).length;
	document.getElementById(oSpanId).innerHTML = "("+totalRow+"건)";
}



// 테이블 행추가/삭제
function editTable(type){
	
	// 1. 신청 목적 선택 먼저
	if ( document.getElementById("PRPS_RGHT_CODE").value == '')  {
		
		alert("신청 목적 선택 후 가능한 기능입니다.");
		
		document.getElementById("PRPS_RGHT_CODE").focus();
		
		return;
	}
	
	// 2. 신청 신탁관리단체 먼저
	if ( !document.getElementById("CHK_206").checked )  {
		
		alert("권리구분 선택 후 가능한 기능입니다.");
		
		document.getElementById("CHK_206").focus();
		return;
	}
	
	// 3. tableId setting
	var tableId = "listTab";	//권리자/대리인 권리찾기
	var chkId = "iChk";
	var disSeq = "displaySeq";
	var totalRow = "totalRow";
	var divId = "div_1";
	var scrllDivId = "div_scroll_1";
	
	if(document.getElementById("PRPS_RGHT_CODE").value == '3')	{
		 tableId = "listTab_2";		//이용허락
		 chkId = "iChk2";
		 disSeq = "displaySeq2";
		 totalRow = "totalRow2";
		 divId = "div_2";
		 scrllDivId = "div_scroll_2";
	}
	
	if( type == 'D' ) {
		
	    var oTbl = document.getElementById(tableId);
	    var oChkDel = document.getElementsByName(chkId);
	    var iChkCnt = oChkDel.length;
	    var iDelCnt = 0;
	    
	    if(iChkCnt == 1 && oChkDel[0].checked == true){
	        oTbl.deleteRow(1);
	        iDelCnt++;
	    }else if(iChkCnt > 1){
    	    for(i = iChkCnt-1; i >= 0; i--){
    	        if(oChkDel[i].checked == true){    	            
    	            oTbl.deleteRow(i+1);
    	            iDelCnt++;
    	        }
    	    }
    	}
    	
    	if(iDelCnt < 1){
    	    alert('삭제할 저작물을 선택하여 주십시요');
    	}
   
    	fn_resetSeq(disSeq);
    		

	}else if( type == 'I' ) {
		
		newRow = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
		
		newRow.onmouseover=function(){document.getElementById(tableId).clickedRowIndex=this.rowIndex}
		newRow.valign = 'middle';
		newRow.align = 'center';
		makeCell(tableId, newRow, document.getElementById(tableId).rows.length);
	
	}else if( type == 'A' ) {
		addCell(tableId);
	}
	
	// 스크롤에 의한 height reset
	//resizeDiv(tableId, divId);
	scrollSet(tableId, scrllDivId, chkId);
		
	// 토탈건수
	setTotCnt( chkId, totalRow);
}
	
    
//순번 재지정
function fn_resetSeq(disName){
    var oSeq = document.getElementsByName(disName);
    for(i=0; i<oSeq.length; i++){
        oSeq[i].value = i+1;
    }
}


//추가저작물에 대한 기존저작물 중복확인
function goWorksSearch(worksTitleId, chkId, index){
	
	var worksTitle = document.getElementById(worksTitleId).value;

	if(worksTitle == ''){
		alert('저작물명을 먼저 입력해 주세요.');
		document.getElementById(worksTitleId).focus();
		return;
	} else {
		
		// 중복확인
		document.getElementById(chkId).value = 'Y';	 
		
		// 검색시작
		var ifFrm = document.getElementById("ifScriptRghtSrch").contentWindow.document.ifFrm;
				
		ifFrm.method = "post";
		ifFrm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=C&parentGubun=Y&srchTitle="+worksTitle;
		ifFrm.page_no.value = 1;
		ifFrm.submit();
	}
}


//추가저작물에 대한 기존저작물 중복확인유무 체크
function worksSearchChk(worksTitleId, chkId, index){
	
	var worksTitle = document.getElementById(worksTitleId).value;
	
	if(document.getElementById(chkId).value !='Y') {
		
		/*
		// 이후 필수항목이 없는 이유로 추가된 Proc
		var forms = document.prpsForm;
		
		for(i = 0; i<forms.length; i++)
		{	
			alert(forms[i].name);
			if(forms[i].name != "searchChk") 
			{
				alert('추가한 저작물명('+worksTitle+')을 먼저 [검색]해 주세요.');
				break;
			}
		}
		// .....
		*/
		alert('추가한 저작물명('+worksTitle+')을 먼저 [검색]해 주세요.');
		document.getElementById(worksTitleId).focus();
		return;
	}
}


//선택저작물 테이블 idx
var iRowIdx = 1;
var iRowIdx2 = 1;
       
function makeCell(tableId, cur_row, rowPos) {
	
	var i=0;
	
	var classId = "inputDisible"; 	var read = "readonly";
	
	// 신청목적에 따라 생성 td 가 다르다.
	var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
	
	if( document.getElementById("CHK_206").checked )	{
		classId = "inputData";		read = "";
	}
	
	// 권리자/대리인 권리찾기
	if( tableId == 'listTab' ) {
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk\"  id=\"iChk_'+iRowIdx+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce';
		innerHTMLStr = '<input name=\"displaySeq\" id=\"displaySeq_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
																		
		// 저작물 정보
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"TITLE\"  id=\"TITLE_'+iRowIdx+'\" class="inputData w75" title=\"저작물명\" rangeSize="~100" nullCheck onkeypress=\"javascript:if(event.keyCode==13){goWorksSearch(\'TITLE_'+iRowIdx+'\', \'searchChk_'+iRowIdx+'\',\''+iRowIdx+'\');}\" \/>';
		// 저작물정보 중복확인 Proc 추가(20101214)
		innerHTMLStr += ' <span class=\"button small type3\" title="저작물명으로 기존저작물정보를 검색합니다." style="margin-top:1px;"><a href=\"javascript:goWorksSearch(\'TITLE_'+iRowIdx+'\', \'searchChk_'+iRowIdx+'\',\''+iRowIdx+'\');\">검색</a><\/span>';
		innerHTMLStr += '<input type=\"hidden\" name=\"searchChk\" id=\"searchChk_'+iRowIdx+'\"> ';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<select name=\"GENRE_KIND\" id=\"GENRE_KIND_'+iRowIdx+'\" title=\"장르\" nullCheck onfocus=\"javascript:worksSearchChk(\'TITLE_'+iRowIdx+'\', \'searchChk_'+iRowIdx+'\',\''+iRowIdx+'\');\" ><option value=\"\">선택<\/option>';
		<c:forEach items="${genreList}" var="genreList">
			innerHTMLStr +='<option value="${genreList.code}">${genreList.codeName}<\/option>'
		</c:forEach>
		innerHTMLStr += '<\/select>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<select name=\"BROAD_MEDI\" id=\"BROAD_MEDI_'+iRowIdx+'\" title=\"방송매체\"><option value=\"\">선택<\/option>';
		innerHTMLStr += '<option value=\"T\">TV<\/option><option value=\"R\">RADIO<\/option><option value=\"C\">CABLE<\/option><option value=\"I\">INTERNET<\/option>';
		innerHTMLStr += '<option value=\"X\">기타<\/option><\/select>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<select name=\"BROAD_STAT\" id=\"BROAD_MEDI_'+iRowIdx+'\" title=\"방송사\"><option value=\"\">선택<\/option>';
		<c:forEach items="${broadList}" var="broadList">
			innerHTMLStr +='<option value="${broadList.code}">${broadList.codeName}<\/option>'
		</c:forEach>
		innerHTMLStr += '<\/select>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"BROAD_ORD\"  id=\"BROAD_ORD_'+iRowIdx+'\" class="inputData w90" title=\"방송회차\" rangeSize="~400" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		//innerHTMLStr = '<input name=\"BROAD_DATE\"  id=\"BROAD_DATE_'+iRowIdx+'\" class="inputData w90" title=\"방송일시\" character="KE"  maxlength="8" rangeSize="~8" dateCheck />';
		innerHTMLStr = '<input class="inputData w70" type="text" id=\"BROAD_DATE_'+iRowIdx+'\" name=\"BROAD_DATE_'+iRowIdx+'\" size="8" maxlength="8"  title=\"방송일시\" character="KE" dateCheck value=""/> ';
		innerHTMLStr += '<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'BROAD_DATE_'+iRowIdx+'\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'BROAD_DATE_'+iRowIdx+'\');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>'
		innerHTMLStr += '<input name=\"BROAD_DATE\" type="hidden" class="inputData w90" value="'+iRowIdx+'"/>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"DIRECT\"  id=\"DIRECT_'+iRowIdx+'\" class="inputData w90" title=\"연출\" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"MAKER\"  id=\"MAKER_'+iRowIdx+'\" class="inputData w90" title=\"제작사\" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작권자 
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"WRITER\" id=\"WRITER_'+iRowIdx+'\" class="'+classId+' w90" '+read+' title="작가" rangeSize="~100" />';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="WRITER_ORGN" />';
		
		innerHTMLStr += '<!-- 사용자정보입력확인 -->';
		innerHTMLStr += '<input type="hidden" name="CHK_WRITER" />';
		
		cur_cell.innerHTML = innerHTMLStr;
		
		fn_resetSeq("displaySeq");
		
		// setfocus
		//var oFuc = 'MUSIC_TITLE_'+iRowIdx;
		//	document.getElementById(oFuc).focus();
		
		iRowIdx++;
	}
	
	// 이용자 권리조회
	else if( tableId == 'listTab_2' ) {
	//alert(iRowIdx2);
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk2\"  id=\"iChk2_'+iRowIdx2+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce';
		innerHTMLStr = '<input name=\"displaySeq2\" id=\"displaySeq2_'+iRowIdx2+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작물 정보
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"TITLE\"  id=\"TITLE_'+iRowIdx2+'\" class="inputData w75" title=\"저작물명\" rangeSize="~100" nullCheck onkeypress=\"javascript:if(event.keyCode==13){goWorksSearch(\'TITLE_'+iRowIdx2+'\', \'searchChk_'+iRowIdx2+'\',\''+iRowIdx2+'\');}\" \/>';
		// 저작물정보 중복확인 Proc 추가(20101214)
		innerHTMLStr += ' <span class=\"button small type3\" title="저작물명으로 기존저작물정보를 검색합니다." style="margin-top:1px;"><a href=\"javascript:goWorksSearch(\'TITLE_'+iRowIdx2+'\', \'searchChk_'+iRowIdx2+'\',\''+iRowIdx2+'\');\">검색</a><\/span>';
		innerHTMLStr += '<input type=\"hidden\" name=\"searchChk\" id=\"searchChk_'+iRowIdx2+'\"> ';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<select name=\"GENRE_KIND\" id=\"GENRE_KIND_'+iRowIdx2+'\" title=\"장르\" nullCheck onfocus=\"javascript:worksSearchChk(\'TITLE_'+iRowIdx2+'\', \'searchChk_'+iRowIdx2+'\',\''+iRowIdx2+'\');\"><option value=\"\">선택<\/option>';
		<c:forEach items="${genreList}" var="genreList">
			innerHTMLStr +='<option value="${genreList.code}">${genreList.codeName}<\/option>'
		</c:forEach>
		innerHTMLStr += '<\/select>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<select name=\"BROAD_MEDI\" id=\"BROAD_MEDI_'+iRowIdx2+'\" title=\"방송매체\"><option value=\"\">선택<\/option>';
		innerHTMLStr += '<option value=\"T\">TV<\/option><option value=\"R\">RADIO<\/option><option value=\"C\">CABLE<\/option><option value=\"I\">INTERNET<\/option>';
		innerHTMLStr += '<option value=\"X\">기타<\/option><\/select>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<select name=\"BROAD_STAT\" id=\"BROAD_MEDI_'+iRowIdx2+'\" title=\"방송사\"><option value=\"\">선택<\/option>';
		<c:forEach items="${broadList}" var="broadList">
			innerHTMLStr +='<option value="${broadList.code}">${broadList.codeName}<\/option>'
		</c:forEach>
		innerHTMLStr += '<\/select>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"BROAD_ORD\"  id=\"BROAD_ORD_'+iRowIdx2+'\" class="inputData w90" title=\"방송회차\" rangeSize="~400" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		//innerHTMLStr = '<input name=\"BROAD_DATE\"  id=\"BROAD_DATE_'+iRowIdx2+'\" class="inputData w90" title=\"방송일시\" character="KE"  maxlength="8" rangeSize="~8" dateCheck />';
		innerHTMLStr = '<input class="inputData w70" type="text" id=\"VAL_BROAD_DATE_'+iRowIdx2+'\" name=\"VAL_BROAD_DATE_'+iRowIdx2+'\" size="8" title=\"방송일시\"  maxlength="8" character="KE" dateCheck value=""/> ';
		innerHTMLStr += '<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'VAL_BROAD_DATE_'+iRowIdx2+'\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'VAL_BROAD_DATE_'+iRowIdx2+'\');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>'
		innerHTMLStr += '<input name=\"VAL_BROAD_DATE\" type="hidden" value="'+iRowIdx2+'"/>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"DIRECT\"  id=\"DIRECT_'+iRowIdx2+'\" class="inputData w90" title=\"연출\" rangeSize="~100" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"MAKER\"  id=\"MAKER_'+iRowIdx2+'\" class="inputData w90" title=\"제작사\" rangeSize="~100" />';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="WRITER" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		fn_resetSeq("displaySeq2");
		
		// setfocus
		//	var oFuc = 'MUSIC_TITLE_'+iRowIdx2;
		//	document.getElementById(oFuc).focus();
		
		iRowIdx2++;
	}
}


function addCell(tableId) {

	//iFrame 항목
	var chkObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("ifrmChk");
	var titleObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("title");
	var genreKindObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("genreKind");
	var genreKindNameObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("genreKindName");
	var broadMediObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadMedi");
	var broadMediNameObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadMediName");
	var broadStatObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadStat");
	var broadStatNameObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadStatName");
	var broadOrdObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadOrd");
	var broadDateObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadDate");
	var directObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("direct");	
	var makerObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("maker");
	var writerObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("writer");			

	var crIdObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("crId");

	// 선택 목록
	var oSelTable = document.getElementById("listTab");
	var oSelTable2 = document.getElementById("listTab_2");

	var selChkObjs = oSelTable.getElementsByTagName("input");
	var selChkObjs2 = oSelTable2.getElementsByTagName("input");

	var selChkCnt = 0;
	var selChkCnt2 = 0;
	
	var isExistYn = "N";	//중복여부	
	var isAdd	= false;	//줄 추가여부

	var count = 0;
	
	for(var cn = 0; cn < chkObjs.length; cn++) {
		if(chkObjs[cn].checked == true){

			if( tableId == 'listTab') {
				//중복여부 검사
				for(var sn=0; sn<selChkObjs.length; sn++) {
					if(chkObjs[cn].checked == true && selChkObjs[sn].name == 'iChkVal') {
						if(crIdObjs[cn].value == selChkObjs[sn].value) {
							isExistYn = "Y";
						}
						selChkCnt++;
					}
				}
			}

			if( tableId == 'listTab_2') {
				//중복여부 검사
				for(var sn=0; sn<selChkObjs2.length; sn++) {
					if(chkObjs[cn].checked == true && selChkObjs2[sn].name == 'iChkVal') {
						if(crIdObjs[cn].value == selChkObjs2[sn].value) {
							isExistYn = "Y";
						}
					}
					selChkCnt2++;
				}
			}
			
			if(isExistYn == "N") {
				//줄 추가여부
				isAdd = true;
				count ++;
			}else{
				alert("선택된 저작물 ["+titleObjs[cn].value+"]는 이미 추가된 저작물입니다.");
				return;
			}
			

			if( tableId == 'listTab' && selChkCnt > 0) { //권리찾기 선택목록이 비웠을때 
				//줄 추가여부		
				isAdd = true;
				count ++;
			}

			if( tableId == 'listTab_2' && selChkCnt2 > 0) { //권리찾기 선택목록이 비웠을때 
				//줄 추가여부		
				isAdd = true;
				count ++;
			}
		}

		chkObjs[cn].checked = false;  // 처리한 후 체크풀기 


		//줄 추가실행
		if(isAdd){
			var cur_row = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
			
			cur_row.onmouseover=function(){document.getElementById(tableId).clickedRowIndex=this.rowIndex}
			cur_row.valign = 'middle';
			cur_row.align = 'center';
			
			var rowPos = document.getElementById(tableId).rows.length;
			
			var i=0;
			
			var classId = "inputDisible"; 	var read = "readonly";
			
			// 신청목적에 따라 생성 td 가 다르다.
			var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
			if( document.getElementById("CHK_206").checked )	{
				classId = "inputData";		read = "";
			}

			// 권리자/대리인 권리찾기
			if( tableId == 'listTab' ) {
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '<input type=\"checkbox\" name=\"iChk\"  id=\"iChk_'+iRowIdx+'\"class="vmid" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"displaySeq\" id=\"displaySeq_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				// 저작물 정보
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'lft';
				innerHTMLStr = '<a href=\"javascript:openScriptDetail(\''+crIdObjs[cn].value+'\');\"><u>'+titleObjs[cn].value+'<\/u><\/a>';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'lft';
				innerHTMLStr = genreKindNameObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = broadMediNameObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = broadStatNameObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = broadOrdObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = broadDateObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = directObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = makerObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				// 저작권자
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '<input name=\"WRITER\" id=\"WRITER_'+iRowIdx+'\" class=\"'+classId+' w90\" '+read+' value=\''+fncReplaceStr(writerObjs[cn].value, '"', '&quot;')+'\' title=\"작가\" rangeSize=\"~100\"  />';
				innerHTMLStr += '<input type=\"hidden\" name=\"iChkVal\" value=\''+crIdObjs[cn].value+'|0|0\'/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"TITLE\" value=\''+fncReplaceStr(titleObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"GENRE_KIND\" value=\''+genreKindObjs[cn].value+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"GENRE_KIND_NAME\" value=\''+fncReplaceStr(genreKindNameObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"BROAD_MEDI\" value=\''+broadMediObjs[cn].value+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"BROAD_MEDI_NAME\" value=\''+fncReplaceStr(broadMediNameObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"BROAD_STAT\" value=\''+broadStatObjs[cn].value+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"BROAD_STAT_NAME\" value=\''+fncReplaceStr(broadStatNameObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"BROAD_ORD\" value=\''+broadOrdObjs[cn].value+'\' />';
				innerHTMLStr += '<input type=\"hidden\" id=\"BROAD_DATE_'+iRowIdx+'\" name=\"BROAD_DATE_'+iRowIdx+'\" value=\''+broadDateObjs[cn].value+'\' />';
				innerHTMLStr += '<input name=\"BROAD_DATE\" type="hidden" value="'+iRowIdx+'"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"DIRECT\" value=\''+fncReplaceStr(directObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"MAKER\" value=\''+fncReplaceStr(makerObjs[cn].value, '"', '&quot;')+'\' />';
				
				innerHTMLStr += '<input type=\"hidden\" name=\"WRITER_ORGN\" value=\''+fncReplaceStr(writerObjs[cn].value, '"', '&quot;')+'\' />';
				
				innerHTMLStr += '<!-- 사용자정보입력확인 -->';
				innerHTMLStr += '<input type="hidden" name="CHK_WRITER" />';
				
				cur_cell.innerHTML = innerHTMLStr;
				
				fn_resetSeq("displaySeq");
				
				// setfocus
				//var oFuc = 'MUSIC_TITLE_'+iRowIdx;
				//	document.getElementById(oFuc).focus();
				
				iRowIdx++;
			}
			
			// 이용자 권리조회
			else if( tableId == 'listTab_2' ) {
			//alert(iRowIdx2);
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = '<input type=\"checkbox\" name=\"iChk2\"  id=\"iChk2_'+iRowIdx2+'\"class="vmid" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'ce';
				innerHTMLStr = '<input name=\"displaySeq2\" id=\"displaySeq2_'+iRowIdx2+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				// 저작물 정보
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'lft';
				innerHTMLStr = '<a href=\"javascript:openScriptDetail(\''+crIdObjs[cn].value+'\');\"><u>'+titleObjs[cn].value+'<\/u><\/a>';
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				cur_cell.className = 'lft';
				innerHTMLStr = genreKindNameObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = broadMediNameObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = broadStatNameObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = broadOrdObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = broadDateObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = directObjs[cn].value;
				cur_cell.innerHTML = innerHTMLStr;
				
				cur_cell = cur_row.insertCell(i++);
				innerHTMLStr = makerObjs[cn].value;
				innerHTMLStr += '<input type=\"hidden\" name=\"iChkVal\" value=\''+crIdObjs[cn].value+'|0|0\'/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"TITLE\" value=\''+fncReplaceStr(titleObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"GENRE_KIND\" value=\''+genreKindObjs[cn].value+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"GENRE_KIND_NAME\" value=\''+fncReplaceStr(genreKindNameObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"BROAD_MEDI\" value=\''+broadMediObjs[cn].value+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"BROAD_MEDI_NAME\" value=\''+fncReplaceStr(broadMediNameObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"BROAD_STAT\" value=\''+broadStatObjs[cn].value+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"BROAD_STAT_NAME\" value=\''+fncReplaceStr(broadStatNameObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"BROAD_ORD\" value=\''+broadOrdObjs[cn].value+'\' />';
				innerHTMLStr += '<input type=\"hidden\" id=\"VAL_BROAD_DATE_'+iRowIdx2+'\" name=\"VAL_BROAD_DATE_'+iRowIdx2+'\" value=\''+broadDateObjs[cn].value+'\' />';
				innerHTMLStr += '<input name=\"VAL_BROAD_DATE\" type="hidden" value="'+iRowIdx2+'"/>';
				innerHTMLStr += '<input type=\"hidden\" name=\"DIRECT\" value=\''+fncReplaceStr(directObjs[cn].value, '"', '&quot;')+'\' />';
				innerHTMLStr += '<input type=\"hidden\" name=\"MAKER\" value=\''+fncReplaceStr(makerObjs[cn].value, '"', '&quot;')+'\' />';
				
				innerHTMLStr += '<input type=\"hidden\" name=\"WRITER\" />';
				cur_cell.innerHTML = innerHTMLStr;
				
				
				fn_resetSeq("displaySeq2");
				
				// setfocus
				//	var oFuc = 'MUSIC_TITLE_'+iRowIdx2;
				//	document.getElementById(oFuc).focus();
				
				iRowIdx2++;
				
			}
			isAdd = false;
		}
	}

	if(count == 0){
		alert('선택된 저작물이 없습니다.');
		return;
	}	
	
}

// 신청목적 선택
function prps_check(chk) {
	
	// 권리자 권리찾기
	if(chk.value == '1' || chk.value == '2' ) {
		document.getElementById("div_1").style.display = "";
		document.getElementById("div_2").style.display = "none";
		fn_lock( "listTab", "" );
		fn_lock( "listTab_2", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab", "div_scroll_1", "iChk");
		
		// 토탈건수
		setTotCnt( "iChk", "totalRow");

		// 첨부파일명 및 안내문구 제어
		document.getElementById("attFileTitle").innerHTML = "첨부파일(증빙서류)";		
		document.getElementById("attFileMemo1").style.display = "block";
		document.getElementById("attFileMemo2").style.display = "block";
		
	} else {
		document.getElementById("div_2").style.display = "";
		document.getElementById("div_1").style.display = "none";
		fn_lock( "listTab_2", "" );
		fn_lock( "listTab", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab_2", "div_scroll_2", "iChk2");
		
		// 토탈건수
		setTotCnt( "iChk2", "totalRow2");

		// 첨부파일명 및 안내문구 제어
		document.getElementById("attFileTitle").innerHTML = "첨부파일";		
		document.getElementById("attFileMemo1").style.display = "none";
		document.getElementById("attFileMemo2").style.display = "none";

	}
	
}

// 신탁관리단체 선택
function trust_check(chk) {
	
	var nRow = document.getElementsByName("WRITER").length;	// 전체 길이
	
	var oInWri= document.getElementsByName("WRITER");
	
	for( k=0; k<nRow; k++) {
		
		if(document.getElementById("CHK_206").checked){
			
			oInWri[k].readOnly = false;
			
			oInWri[k].className = 'inputData w90';
		}
		else {
			
			oInWri[k].readOnly = true;
			
			oInWri[k].className = 'inputDisible w90';
		}
		
	} // .. end for
}

// 권리찾기수정
function rghtPrpsModi() {

	var frm = document.prpsForm;

	if( !document.getElementById("CHK_206").checked ){
		alert('권리구분을(를) 선택하세요.');
		document.getElementById("CHK_206").focus();
		return;
	}
	
	if( document.getElementsByName("iChkVal").length <1 ){
		alert('신청 저작물(를) 추가하세요.');
		document.getElementById("workAdd").focus();
		return;
	}
			
	if(checkForm(frm)) {
		
		frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrpsModiProc&DIVS=C";
		
		frm.method="post";
		frm.submit();

	}
}

// 테이블 하위 disable
function fn_lock( tableId, flag ){
	
	var oTbl = document.getElementById(tableId);
	var oInput = oTbl.getElementsByTagName("input");
	
	for(i=0; i<oInput.length;i++){
			oInput[i].disabled= flag ;
	}
	
	var oSelect = oTbl.getElementsByTagName("select");
	for(i=0; i<oSelect.length;i++){
		oSelect[i].disabled= flag ;
	}
}

// 오프라인 접수
function offLine_check(chk) {
	
	if(chk.checked) {
		document.getElementById("td_file_no").style.display = "";
		document.getElementById("td_file_yes").style.display = "none";
	} else{
		document.getElementById("td_file_no").style.display = "none";
		document.getElementById("td_file_yes").style.display = "";
	} 
}

function goList(){
	
	var frm = document.prpsForm;
	frm.action = '/rsltInqr/rsltInqr.do?DIVS=10&page_no=1';
	frm.method="post";
	frm.submit();
}

function showAttach(cnt) {
	var i=0;
	var content = "";
	var attach = document.getElementById("attach")
	attach.innerHTML = "";
	//document.all("attach").innerHTML = "";
	content += '<table>';
	for (i=0; i<cnt; i++) {
		content += '<tr>';
		content += '<td><input type="file" name="attachfile'+(i+1)+'" size="70;" class="inputFile"><\/td>';
		content += '<\/tr>';
	}
	content += '<\/table>';
	//document.all("attach").innerHTML = content;
	attach.innerHTML = content;
}

function applyAtch() {
	var ansCnt = document.prpsForm.atchCnt.value;
	showAttach(parseInt(ansCnt));
}

 function initParameter(){
	
	// 신청목적에 따른 저작물 테이블 제어
	prps_check(document.getElementById("PRPS_RGHT_CODE"));
	
	// 신청 관리단체에 따른 저작물 테이블 제어
	trust_check(document.getElementById("CHK_206"));
	
	fn_createTable2("divFileList", "0");
	fn_setFileInfo();
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 


// 리사이즈
function resizeIFrame(name) {

  var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
  document.getElementById(name).height= the_height+5;
  
}

function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}

//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_3.png" alt="신청현황조회" title="신청현황조회" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li class="active"><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1">저작권찾기<span>&lt;</span></a></li>
							<li><a href="/rsltInqr/rsltInqr.do?DIVS=20&amp;page_no=1">보상금<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>신청현황조회</span>&gt;<strong>저작권찾기</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>방송대본저작물 저작권찾기신청 수정
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>

					<!-- memo 삽입 -->
						<jsp:include page="/common/memo/memo_01.jsp">
							<jsp:param name="DIVS" value="${DIVS}"/>
						</jsp:include>
					<!-- // memo 삽입 -->
															
					<div class="bbsSection">
						<h4>방송대본저작권찾기신청 수정</h4>
						<form name="fileForm" action="">
							<!-- 파일다운로드 -->
							<input type="hidden" name="filePath" />
							<input type="hidden" name="fileName" />
							<input type="hidden" name="realFileName" />
						</form>
						<form name="prpsForm" enctype="multipart/form-data" action="">
							<input type="hidden" name="USER_IDNT" value="${rghtPrps.USER_IDNT}"/>
							<input type="hidden" name="PRPS_MAST_KEY" value="${rghtPrps.PRPS_MAST_KEY}"/>
							<input type="hidden" name="PRPS_SEQN" value="${rghtPrps.PRPS_SEQN}"/>
							
							<fieldset>
								<legend>저작권찾기 신청자 정보</legend>
								<!-- 테이블 영역입니다 -->
								<div class="tabelRound">
									<span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span><!-- 라운딩효과 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="저작권찾기 신청정보 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="10%">
										<col width="10%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row" rowspan="2" class="bgbr lft">신청정보</th>
												<th scope="row" class="bgbr2 lft"><label for="PRPS_RGHT_CODE" class="necessary">신청목적</label></th>
												<td>
													<select name="PRPS_RGHT_CODE" id="PRPS_RGHT_CODE"  title="신청목적" nullCheck onchange="prps_check(this);">
														<option value="">선택 ------</option>
														<option value="1" <c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">selected="selected"</c:if>>권리자의 저작권찾기</option>
														<!-- 
														<option value="2" <c:if test="${rghtPrps.PRPS_RGHT_CODE == '2'}">selected="selected"</c:if>>대리인 권리찾기</option>
														 -->
														<option value="3" <c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">selected="selected"</c:if>>이용자의 저작권조회</option>
													</select>
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr2 lft">신청인 정보</th>
												<td>
													<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
													<colgroup>
														<col width="15%">
														<col width="*">
														<col width="20%">
														<col width="30%">
														</colgroup>
														<tbody>
															<tr>
																<th class="bgbr3 lft">성명</th>
																<td>${rghtPrps.USER_NAME}</td>
																<th class="bgbr3 lft">주민번호/사업자번호</th>
																<td>${rghtPrps.RESD_CORP_NUMB_VIEW}</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">전화</th>
																<td>
																	<label class="labelBlock"><em class="w45">자택</em><input name="HOME_TELX_NUMB" title="자택전화" class="inputData w60" value="${rghtPrps.HOME_TELX_NUMB}"/></label>
																	<label class="labelBlock"><em class="w45">사무실</em><input name="BUSI_TELX_NUMB" title="사무실전화" class="inputData w60" value="${rghtPrps.BUSI_TELX_NUMB}"/></label>
																	<label class="labelBlock"><em class="w45">휴대폰</em><input name="MOBL_PHON" title="휴대폰전화" class="inputData w60" value="${rghtPrps.MOBL_PHON}"/></label>
																</td>
																<th class="bgbr3 lft"><label for="sch3">Fax</label></th>
																<td><input class="inputData" name="FAXX_NUMB" id="sch3" value="${rghtPrps.FAXX_NUMB}"/></td>
															</tr>
															<tr>
																<th class="bgbr3 lft"><label for="sch4">이메일주소</label></th>
																<td colspan="3"><input name="MAIL" class="inputData w90" id="sch4" value="${rghtPrps.MAIL}"/></td>
															</tr>
															<tr>
																<th class="bgbr3 lft">주소</th>
																<td colspan="3">
																	<label class="labelBlock"><em class="w45">자택</em><input name="HOME_ADDR" title="자택 주소" class="inputData w80" value="${rghtPrps.HOME_ADDR}"/></label>
																	<label class="labelBlock"><em class="w45">사무실</em><input name="BUSI_ADDR" title="사무실 주소" class="inputData w80" value="${rghtPrps.BUSI_ADDR}"/></label>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft"><label class="necessary">권리구분</label></th>
												<td>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_206" value="206" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_206 == '1'}">checked="checked"</c:if> title="작가 - 저작권자(한국방송작가협회)" />작가 - 저작권자(한국방송작가협회)</label><br/>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft"><label class="necessary">신청저작물정보</label></th>
												<td>	
													<div class="floatDiv mb5">
														<p class="fl"><label class="blue"> * 권리있는 항목만 입력하세요.</label></p>
														<p class="fr">
														<span class="button small type4 icon"><span class="arrow">&nbsp;</span><a href="javascript:editTable('I');"  id="workAdd">추가</a></span> 
														<span class="button small type3"><a href="javascript:editTable('D');">삭제</a></span>
														</p>
													</div>
													
													<!-- 테이블 영역입니다 -->
													<div id="div_1" class="tabelRound" style="width:583px; padding:0 0 0 0; display:none"> 
														<h5>권리자 저작권찾기 <span id="totalRow">&nbsp;</span></h5>
														<div id="div_scroll_1" class="tabelRound div_scroll" style="width:583px; padding:0 0 0 0;">
														<table id="listTab" style="width:1000px;" cellspacing="0" cellpadding="0" border="1" class="grid" summary="권리자 저작권찾기 신청저작물정보 입력폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 --><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
																<col width="10">
																<col width="10">
																<col width="150">
																<col width="60">
																<col width="35">
																<col width="50">
																<col width="30">
																<col width="65">
																<col width="70">
																<col width="70">
																<col width="70">
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk',this);" title="전체선택" /></th>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH"><label class="necessary">저작물명</label></th>
																	<th scope="col" class="headH"><label class="necessary">장르</label></th>
																	<th scope="col" class="headH">방송매체</th>
																	<th scope="col" class="headH">방송사</th>
																	<th scope="col" class="headH">방송회차</th>
																	<th scope="col" class="headH">방송일시</th>
																	<th scope="col" class="headH">연출</th>
																	<th scope="col" class="headH">제작사</th>
																	<th scope="col" class="headH">작가</th>
																</tr>
															</thead>
															<tbody>
															<c:if test="${!empty workList}">
																<c:forEach items="${workList}" var="workList">
																	<c:set var="NO" value="${workList.totalRow}"/>
																	<c:set var="i" value="${i+1}"/>
																<tr>
																	<td class="ce"><input type="checkbox" name="iChk" id="iChk_${i}" class="vmid"  title="선택" /></td>
																	<td class="ce"><input name="displaySeq" id="displaySeq_${i}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}" title="순번" /></td>
																	
																	<!-- 기존Meta저작물 -->
																	<c:if test="${workList.PRPS_IDNT_CODE == '1'}">
																	<td><a href="javascript:openScriptDetail('${workList.PRPS_IDNT}');"><u>${workList.TITLE }</u></a></td>
																	<td class="ce">${workList.GENRE_KIND_NAME }</td>
																	<td class="ce">
																		<c:choose>
																			<c:when test="${workList.BROAD_MEDI == 'T'}">TV</c:when>
																			<c:when test="${workList.BROAD_MEDI == 'R'}">RADIO</c:when>
																			<c:when test="${workList.BROAD_MEDI == 'C'}">CABLE</c:when>
																			<c:when test="${workList.BROAD_MEDI == 'I'}">INTERNET</c:when>
																			<c:when test="${workList.BROAD_MEDI == 'X'}">기타</c:when>
																		</c:choose>
																	</td>
																	<td class="ce">${workList.BROAD_STAT_NAME }</td>
																	<td class="ce">${workList.BROAD_ORD }</td>
																	<td class="ce">${workList.BROAD_DATE }</td>
																	<td class="ce">${workList.DIRECT }</td>
																	<td class="ce">${workList.MAKER }
																		<!-- hidden Value -->
																		<input type="hidden" name="iChkVal" value="${workList.PRPS_IDNT }|0|0"/>
																		<input type="hidden" name="TITLE" />
																		<input type="hidden" name="GENRE_KIND" />
																		<input type="hidden" name="BROAD_MEDI" />
																		<input type="hidden" name="BROAD_STAT" />
																		<input type="hidden" name="BROAD_ORD" />
																		<!--<input type="hidden" name="BROAD_DATE" />-->
																		<input type="hidden" name="BROAD_DATE_${i}" value="${workList.BROAD_DATE }" />
																		<input type="hidden" name="BROAD_DATE" value="${i}"/>
																		<input type="hidden" name="DIRECT" />
																		<input type="hidden" name="MAKER" />
																	</td>
																	</c:if>
																	
																	<!-- 추가등록저작물 -->
																	<c:if test="${workList.PRPS_IDNT_CODE == '2'}">
																	<td class="ce"><input name="TITLE" id="TITLE_${i}" class="inputData w95" value="${workList.TITLE_TRNS }" title="저작물명" rangeSize="~100" nullCheck /></td>
																	<td class="ce">
																		<select name="GENRE_KIND" id="GENRE_KIND_${i}" title="장르" nullCheck>
																			<option value="">선택</option>
																			<c:forEach items="${genreList}" var="genreList">
																			<option value="${genreList.code}" <c:if test="${workList.GENRE_KIND == genreList.code}">selected="selected"</c:if>>${genreList.codeName}</option>
																			</c:forEach>
																		</select>
																	</td>
																	<td class="ce">
																		<select name="BROAD_MEDI"  id="BROAD_MEDI_${i}" title="방송매체">
																			<option value="">선택</option>
																			<option value="T" <c:if test="${workList.BROAD_MEDI == 'T'}">selected="selected"</c:if>>TV</option>
																			<option value="R" <c:if test="${workList.BROAD_MEDI == 'R'}">selected="selected"</c:if>>RADIO</option>
																			<option value="C" <c:if test="${workList.BROAD_MEDI == 'C'}">selected="selected"</c:if>>CABLE</option>
																			<option value="I" <c:if test="${workList.BROAD_MEDI == 'I'}">selected="selected"</c:if>>INTERNET</option>
																			<option value="X" <c:if test="${workList.BROAD_MEDI == 'X'}">selected="selected"</c:if>>기타</option>
																		</select>	
																	</td>
																	<td class="ce">
																		<select name="BROAD_STAT"  id="BROAD_STAT_${i}" title="방송사">
																			<option value="">선택</option>
																			<c:forEach items="${broadList}" var="broadList">
																			<option value="${broadList.code}" <c:if test="${workList.BROAD_STAT == broadList.code}">selected="selected"</c:if>>${broadList.codeName}</option>
																			</c:forEach>
																		</select>
																	</td>
																	<td class="ce"><input name="BROAD_ORD"  id="BROAD_ORD_${i}" class="inputData w90" title="방송회차" rangeSize="~400" value = "${workList.BROAD_ORD}"/></td>
																	<td class="ce">
																	<!--<input name="BROAD_DATE"  id="BROAD_DATE_${i}" class="inputData w90" title="방송일시" character="KE"  maxlength="8" rangeSize="~8" dateCheck value = "${workList.BROAD_DATE}"/>-->
																	<input name="BROAD_DATE_${i}"  id="BROAD_DATE_${i}" class="inputData w70" title="방송일시" maxlength="8" Size="8" character="KE" dateCheck  value = "${workList.BROAD_DATE}"/>
																	<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('prpsForm','BROAD_DATE_${i}');" onkeypress="javascript:fn_cal('prpsForm','BROAD_DATE_${i}');" alt="달력" title="방송일시를 선택하세요." align="middle" style="cursor:pointer;"/>
																	<input type="hidden" name="BROAD_DATE" value="${i}"/>
																	</td>
																	<td class="ce"><input name="DIRECT"  id="DIRECT_${i}" class="inputData w90" title="연출" rangeSize="~100" value = "${workList.DIRECT_TRNS}"/></td>
																	<td class="ce"><input name="MAKER"  id="MAKER_${i}" class="inputData w90" title="제작사" rangeSize="~100" value = "${workList.MAKER_TRNS}"/>	
																	<!-- hidden Value -->
																	<input type="hidden" name="iChkVal" value=""/>
																	</td>
																	</c:if>
																	
																	<td class="ce"><input name="WRITER" id="WRITER_${i}" class="inputDisible w90 ce" value="${workList.WRITER_TRNS}" title="작가" rangeSize="~100" readonly="readonly" />
																		<!-- hidden Value -->
																		<input type="hidden" name="WRITER_ORGN" value="${workList.WRITER_ORGN_TRNS }" />
																	</td>
																</tr>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
														</div>
													</div>
													<!-- //테이블 영역입니다 -->
													
													<!-- 테이블 영역입니다 -->
													<div id="div_2" class="tabelRound mt10" style="width:583px; display:none"> 
														<h5>이용자 저작권조회<span id="totalRow2">&nbsp;</span></h5>
														<div id="div_scroll_2" class="tabelRound div_scroll" style="width:583px; padding:0 0 0 0;">
														<table id="listTab_2" style="width:900px;" cellspacing="0" cellpadding="0" border="1" class="grid" summary="이용자 저작권조회 신청저작물정보 입력폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
																<col width="10">
																<col width="10">
																<col width="150">
																<col width="60">
																<col width="35">
																<col width="50">
																<col width="30">
																<col width="65">
																<col width="70">
																<col width="70">
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk2',this);" title="전체선택" /></th>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH"><label class="necessary">저작물명</label></th>
																	<th scope="col" class="headH"><label class="necessary">장르</label></th>
																	<th scope="col" class="headH">방송매체</th>
																	<th scope="col" class="headH">방송사</th>
																	<th scope="col" class="headH">방송회차</th>
																	<th scope="col" class="headH">방송일시</th>
																	<th scope="col" class="headH">연출</th>
																	<th scope="col" class="headH">제작사</th>
																</tr>
															</thead>
															<tbody>
																<c:if test="${!empty workList}">
																<c:forEach items="${workList}" var="workList">
																	<c:set var="NO" value="${workList.totalRow}"/>
																	<c:set var="ii" value="${ii+1}"/>
																<tr>
																	<td class="ce"><input type="checkbox" name="iChk2" id="iChk2_${ii}" class="vmid" title="선택" /></td>
																	<td class="ce">
																		<input name="displaySeq2" id="displaySeq2_${ii}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${ii}" title="순번" />
																		<!-- HIDDEN VALUE -->
																		<input type="hidden" name="WRITER" />
																	</td>
																	
																	<!-- 기존Meta저작물 -->
																	<c:if test="${workList.PRPS_IDNT_CODE == '1'}">
																	<td><a href="javascript:openScriptDetail('${workList.PRPS_IDNT}');"><u>${workList.TITLE }</u></a></td>
																	<td class="ce">${workList.GENRE_KIND_NAME }</td>
																	<td class="ce">${workList.BROAD_MEDI_NAME }</td>
																	<td class="ce">${workList.BROAD_STAT_NAME }</td>
																	<td class="ce">${workList.BROAD_ORD }</td>
																	<td class="ce">${workList.BROAD_DATE }</td>
																	<td class="ce">${workList.DIRECT }</td>
																	<td class="ce">${workList.MAKER }
																		<!-- hidden Value -->
																		<input type="hidden" name="iChkVal" value="${workList.PRPS_IDNT}|0|0"/>
																		<input type="hidden" name="TITLE" />
																		<input type="hidden" name="GENRE_KIND" />
																		<input type="hidden" name="BROAD_MEDI" />
																		<input type="hidden" name="BROAD_STAT" />
																		<input type="hidden" name="BROAD_ORD" />
																		<!--<input type="hidden" name="BROAD_DATE" />-->
																		<input type="hidden" name="VAL_BROAD_DATE_${ii}" value="${workList.BROAD_DATE }" />
																		<input type="hidden" name="VAL_BROAD_DATE" value="${ii}"/>
																		<input type="hidden" name="DIRECT" />
																		<input type="hidden" name="MAKER" />
																	</td>
																	</c:if>
																	
																	<!-- 추가등록저작물 -->
																	<c:if test="${workList.PRPS_IDNT_CODE == '2'}">
																	<td class="ce"><input name="TITLE" id="TITLE_${ii}" class="inputData w95" value="${workList.TITLE_TRNS }" title="저작물명" rangeSize="~100" nullCheck /></td>
																	<td class="ce">
																		<select name="GENRE_KIND" id="GENRE_KIND_${i}" title="장르" nullCheck>
																			<option value="">선택</option>
																			<c:forEach items="${genreList}" var="genreList">
																			<option value="${genreList.code}" <c:if test="${workList.GENRE_KIND == genreList.code}">selected="selected"</c:if>>${genreList.codeName}</option>
																			</c:forEach>
																		</select>
																	</td>
																	<td class="ce">
																		<select name="BROAD_MEDI"  id="BROAD_MEDI_${i}" title="방송매체">
																			<option value="">선택</option>
																			<option value="T" <c:if test="${workList.BROAD_MEDI == 'T'}">selected="selected"</c:if>>TV</option>
																			<option value="R" <c:if test="${workList.BROAD_MEDI == 'R'}">selected="selected"</c:if>>RADIO</option>
																			<option value="C" <c:if test="${workList.BROAD_MEDI == 'C'}">selected="selected"</c:if>>CABLE</option>
																			<option value="I" <c:if test="${workList.BROAD_MEDI == 'I'}">selected="selected"</c:if>>INTERNET</option>
																			<option value="X" <c:if test="${workList.BROAD_MEDI == 'X'}">selected="selected"</c:if>>기타</option>
																		</select>	
																	</td>
																	<td class="ce">
																		<select name="BROAD_STAT"  id="BROAD_STAT_${i}" title="방송사">
																			<option value="">선택</option>
																			<c:forEach items="${broadList}" var="broadList">
																			<option value="${broadList.code}" <c:if test="${workList.BROAD_STAT == broadList.code}">selected="selected"</c:if>>${broadList.codeName}</option>
																			</c:forEach>
																		</select>
																	</td>
																	<td class="ce"><input name="BROAD_ORD"  id="BROAD_ORD_${i}" class="inputData w90" title="방송회차" rangeSize="~400" value = "${workList.BROAD_ORD}"/></td>
																	<td class="ce">
																	<!--<input name="BROAD_DATE"  id="BROAD_DATE_${i}" class="inputData w90" title="방송일시" character="KE"  maxlength="8" rangeSize="~8" dateCheck value = "${workList.BROAD_DATE}"/>-->
																	<input name="VAL_BROAD_DATE_${ii}"  id="VAL_BROAD_DATE_${ii}" class="inputData w70" title="방송일시" maxlength="8" Size="8" character="KE" dateCheck  value = "${workList.BROAD_DATE}"/>
																	<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('prpsForm','VAL_BROAD_DATE_${ii}');" onkeypress="javascript:fn_cal('prpsForm','VAL_BROAD_DATE_${ii}');" alt="달력" title="방송일시를 선택하세요." align="middle" style="cursor:pointer;"/>
																	<input type="hidden" name="VAL_BROAD_DATE" value="${ii}"/>
																	</td>
																	<td class="ce"><input name="DIRECT"  id="DIRECT_${i}" class="inputData w90" title="연출" rangeSize="~100" value = "${workList.DIRECT_TRNS}"/></td>
																	<td class="ce"><input name="MAKER"  id="MAKER_${i}" class="inputData w90" title="제작사" rangeSize="~100" value = "${workList.MAKER_TRNS}"/>	
																	<!-- hidden Value -->
																		<input type="hidden" name="iChkVal" value=""/>
																	</td>
																	<!--
																	<td class="ce">
																		<input name="LYRICS" id="LYRICS_${ii}" class="inputData w90" value="${workList.LYRICS }" title="가사" rangeSize="~500" />
																	</td>
																	-->
																	</c:if>
																	
																</tr>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
														</div>
													</div>
													<!-- //테이블 영역입니다 -->
													
													<!-- //버튼영역 -->
													<p id="pBtnAdd" class="ce mt5">
														<span class="button small type4 icon"><span class="arrow">&nbsp;</span><a href="javascript:editTable('A');">추가</a></span>
													</p>
													<!-- //버튼영역 -->
													<!-- iframe 영역입니다 -->
													<iframe id="ifScriptRghtSrch" title="저작물조회(방송대본)" name="scriptRghtSrch" 
														width="100%" height="130" marginwidth="0" marginheight="0" frameborder="0" 
														scrolling="no" style="overflow-y:hidden;"
														src="/rghtPrps/rghtSrch.do?method=subListForm&amp;page_no=1&amp;DIVS=C&amp;dtlYn=Y">
													</iframe>
													<!-- //iframe 영역입니다 -->
													
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft"><label for="PRPS_DESC" class="necessary">신청내용</label></th>
												<td>
													<p class="blue">* 신청 목적이 이용자 저작권조회인 경우 이용내용(이용하고자 하는 목적/방법)을 작성합니다.
													<br/>* 신청 저작물정보의 가사 및 저작물 변경 등을 작성합니다.</p>
													<textarea cols="10" rows="10" name="PRPS_DESC" id="PRPS_DESC" class="h100" title="신청내용" nullCheck>${rghtPrps.PRPS_DESC}</textarea>
												</td>
											</tr>
											<tr>									
												<th scope="row" colspan="2" class="bgbr lft">
													<span id="attFileTitle">첨부파일(증빙서류)</span><br />
													<label class="ml5"><input type="checkbox" name="OFFX_LINE_RECP" value="Y" class="inputChk" <c:if test="${rghtPrps.OFFX_LINE_RECP == 'Y'}">checked="checked"</c:if> onclick="javascript:offLine_check(this);" title="오프라인접수" />오프라인접수</label>
												</th>
												<td id="td_file_no" style="display:none">오프라인접수로 선택한 경우 첨부파일을 등록할 수 없습니다.</td>
												<td id="td_file_yes">
								                    <div id="divFileList" class="tabelRound mb5" style="display;">
								                    </div>
												</td>	
											</tr>
											<tr> 
												<th  id="attFileMemo1"  scope="row" colspan="2" class="bgbr lft">첨부파일(증빙서류)<br/>안내</th>
												<td  id="attFileMemo2" >
													<div class="infoImg1">
														<ul>
															<li>1. 저작권자임을 증명할 수 있는 서류(저작권등록증 등)</li>
															<li class="mt3">2. 상속, 양도, 승계 등의 사실을 증명할 수 있는 서류 </li>
															<li class="mt3">3. 주민등록등본/법인등기부등본</li>
															<li class="mt3">4. 사업자등록증(사본) 1부</li>
														</ul>
													</div>
												</td>
											</tr>
											<!-- 2010.08.16 : 정확한 데이터입력이 어려움으로 인한 기능제거 (from 관련기관회의) START -->
											<!-- 
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">이용허락 저작재산권</th>
												<td>
													<label class="label_inBlock w20"><input type="checkbox" />복제권</label>
													<label class="label_inBlock w20"><input type="checkbox" />배포권</label>
													<label class="label_inBlock w20"><input type="checkbox" />대여권</label>
													<label class="label_inBlock w20"><input type="checkbox" />공연권</label><br />
													
													<label class="label_inBlock"><input type="checkbox" />공중송신권</label> (<label class="label_inBlock"><input type="checkbox" />방송권</label><label class="label_inBlock ml5"><input type="checkbox" />전송권</label><label class="label_inBlock ml5"><input type="checkbox" />디지털음성송신권</label>)<br />
													
													<label class="label_inBlock w20"><input type="checkbox" />전시권</label>
													<label class="label_inBlock w30"><input type="checkbox" />2차적저작물작성권</label>
												</td>
											</tr>
											 -->
											<!-- 2010.08.16 : 정확한 데이터입력이 어려움으로 인한 기능제거 (from 관련기관회의) END -->
										</tbody>
									</table>
								</div>
								<!-- //테이블 영역입니다 -->
							</fieldset>
						</form>
					</div>
					
					<div class="floatDiv mt10">
						<p class="fl"><span class="button medium icon"><span class="list">&nbsp;</span><a href="javascript:goList()">목록</a></span></p>
						<p class="fr">
							<!-- <span class="button medium icon"><span class="default"></span><a href="javascript:alert('미진행')">보상금 동시 신청</a></span>  -->
							<span class="button medium icon"><span class="default">&nbsp;</span><a href="javascript:rghtPrpsModi();">수정</a></span>
						</p>
					</div>
						
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->

<script type="text/javascript" src="/js/2010/file.js"></script>
<script type="text/javascript">
<!--
	<c:if test="${!empty workList}">
		<c:forEach items="${workList}" var="workList">
			iRowIdx++;
		</c:forEach>
	</c:if>
	<c:if test="${!empty workList}">
		<c:forEach items="${workList}" var="workList">
			iRowIdx2++;
		</c:forEach>
	</c:if>
	

	function fn_setFileInfo(){
		
		var listCnt = '${fn:length(fileList)}';

		<c:if test="${!empty fileList}">
			<c:forEach items="${fileList}" var="fileList">
				fn_addGetFile(	'${fileList.TRST_ORGN_CODE}',
								'${fileList.PRPS_MAST_KEY}',
								'${fileList.PRPS_SEQN}',
								'${fileList.ATTC_SEQN}',
								'${fileList.FILE_PATH}',
								'${fileList.REAL_FILE_NAME}',
								'${fileList.FILE_NAME}',
								'${fileList.TRST_ORGN_CODE}',
								'${fileList.FILE_SIZE}'
								);
			</c:forEach>
		</c:if>		
	}
//-->
</script>
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>
