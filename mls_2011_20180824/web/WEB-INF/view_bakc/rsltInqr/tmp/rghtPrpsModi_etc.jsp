<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(저작권찾기) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<style type="text/css">
<!--
body {
	overflow-x: auto;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript">
<!--

window.name = "rghtPrpsModi_etc";

// 파일다운로드
function fn_fileDownLoad(filePath, fileName, realFileName) {
		
		var frm = document.fileForm;
		
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.target="rghtPrpsModi_image";
		//frm.action = "/rsltInqr/rsltInqr.do?method=fileDownLoad";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.method="post";
		frm.submit();
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	
	
	var the_height = document.getElementById(name).offsetHeight;
	var chkObjs = document.getElementsByName("chk");
	  
	document.getElementById(targetName).style.height = the_height + "px" ;
	  

}

// 스크롤셋팅
function scrollSet(name, targetName, oRowName){
	
	var totalRow = document.getElementsByName(oRowName).length;
		
	// 세로 기준건수 이상인 경우 세로스크롤 제어
	if( totalRow < 6) {
		resizeDiv(name, targetName);
		document.getElementById(targetName).style.overflowY = "hidden";
	}
	
	if( totalRow > 5 ) {
		document.getElementById(targetName).style.overflowY = "auto";
	}
}


// 토탈건수 set
function setTotCnt( oRowName, oSpanId) {

	var totalRow = document.getElementsByName(oRowName).length;
	document.getElementById(oSpanId).innerHTML = "("+totalRow+"건)";
}

var iRowIdx = 1;
var backIrowIdx = 1;
var test = 1;

// 테이블 행추가/삭제
function editTable(type){
	
	// 1. 신청 목적 선택 먼저
	if ( document.getElementById("PRPS_RGHT_CODE").value == '')  {
		
		alert("신청 목적 선택 후 가능한 기능입니다.");
		
		document.getElementById("PRPS_RGHT_CODE").focus();
		
		return;
	}
	
	// 2. 신청저작물장르
	if ( document.getElementById("PRPS_SIDE_GENRE").value == '' )  {
		
		alert("신청저작물장르 선택 후 가능한 기능입니다.");
		
		document.getElementById("PRPS_SIDE_GENRE").focus();
		return;
	}

	// 3. 신청 신탁관리단체
	var trstOrgnLen = document.getElementsByName("TRST_ORGN_CODE").length;
	var kCnt = 0;
	
	for( kk=0; kk<trstOrgnLen && kCnt ==0; kk++) {
		
		if(document.getElementsByName("TRST_ORGN_CODE")[kk].checked){
			kCnt += 1;	break;
		}
	}
	
	if(kCnt==0) {
		alert("권리구분 선택 후 가능한 기능입니다.");
		
		var PRPS_SIDE_GENRE = document.getElementById("PRPS_SIDE_GENRE").value;

		if(PRPS_SIDE_GENRE == '6')
			document.getElementById("CHK_216_2").focus();
		else if(PRPS_SIDE_GENRE == '7')
			document.getElementById("CHK_215_2").focus();
		else
			document.getElementsByName("TRST_ORGN_CODE")[0].focus();
		return;
	}
	
	// 4. tableId setting
	var tableId = "listTab";	//권리자/대리인 권리찾기
	var chkId = "iChk";
	var disSeq = "displaySeq";
	var totalRow = "totalRow";
	var divId = "div_1";
	var scrllDivId = "div_scroll_1";
	var disId = "PRPS_SIDE_R_GENRE";
	
	//if(document.getElementById("PRPS_RGHT_CODE").value == '3')	{
		 //tableId = "listTab_2";		//이용허락
		// chkId = "iChk2";
		// disSeq = "displaySeq2";
		//totalRow = "totalRow2";
		// divId = "div_2";
		// scrllDivId = "div_scroll_2";
	//}
	
	if( type == 'D' ) {
		
	    var oTbl = document.getElementById(tableId);
	    var oChkDel = document.getElementsByName(chkId);
	    var iChkCnt = oChkDel.length;
	    var iDelCnt = 0;
	    
	    if(iChkCnt == 1 && oChkDel[0].checked == true){
	        oTbl.deleteRow(1);
	        iDelCnt++;
	    }else if(iChkCnt > 1){
    	    for(i = iChkCnt-1; i >= 0; i--){
    	        if(oChkDel[i].checked == true){    	            
    	            oTbl.deleteRow(i+1);
    	            iDelCnt++;
    	        }
    	    }
    	}
    	
    	if(iDelCnt < 1){
    	    alert('삭제할 저작물을 선택하여 주십시요');
    	}

    	fn_resetSeq(disSeq);
    	fn_resetId(disId);
    	//backIrowIdx = iRowIdx-iDelCnt-back_val;
    	var back_idx = document.getElementById("BACK_IROWIDX").value;

		if(back_idx == ''){
			iRowIdx = iRowIdx-iDelCnt-1;
		}else{
			if(iRowIdx > back_idx){
				iRowIdx = iRowIdx-iDelCnt;
			}else{
				iRowIdx = back_idx-iDelCnt;
			}		
			//iRowIdx = iRowIdx-iDelCnt;
		}		
    	document.getElementById("BACK_IROWIDX").value = iRowIdx;
    	document.getElementById("IROWIDX").value = iRowIdx;
    	test = 1;

  	   
    	//fn_resetSeq(disSeq);
    		

	}else if( type == 'I' ) {
		
		newRow = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
		
		newRow.onmouseover=function(){document.getElementById(tableId).clickedRowIndex=this.rowIndex}
		newRow.valign = 'middle';
		newRow.align = 'center';
		var back_idx = document.getElementById("BACK_IROWIDX").value;

		if(back_idx == ''){
			iRowIdx = parseInt(iRowIdx);
			makeCell(tableId, newRow, document.getElementById(tableId).rows.length, iRowIdx);
			iRowIdx++;
		}else{
			iRowIdx = test + parseInt(back_idx);
			document.getElementById("IROWIDX").value = iRowIdx;
			makeCell(tableId, newRow, document.getElementById(tableId).rows.length, iRowIdx);
			test++;
		}	
	
	}else if( type == 'A' ) {
		addCell(tableId);
	}
	
	// 스크롤에 의한 height reset
	//resizeDiv(tableId, divId);
	scrollSet(tableId, scrllDivId, chkId);
		
	// 토탈건수
	setTotCnt( chkId, totalRow);
}
	
    
//순번 재지정
function fn_resetSeq(disName){
    var oSeq = document.getElementsByName(disName);
    for(i=0; i<oSeq.length; i++){
        oSeq[i].value = i+1;
    }
}

//신청저작물 id순번 재지정
function fn_resetId(disId){
    var oSeq = document.getElementsByName(disId);
    for(i=0; i<oSeq.length; i++){
        var idx = i+1;
        var id = disId+idx;
    	oSeq[i].id = id;
    }
}

//선택저작물 테이블 idx
var iRowIdx3 = 1;
var iRowIdx2 = 1;
       
function makeCell(tableId, cur_row, rowPos, iRowIdx) {
	
	var i=0;
	
	var classId = "inputDisible"; 	var read = "readonly";
	
	// 신청목적에 따라 생성 td 가 다르다.
	var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
	
	//if( document.getElementById("CHK_200").checked)	{
	//	classId = "inputData";		read = "";
	//}
	
	// 권리자/대리인 권리찾기
	if( tableId == 'listTab' ) {

		var genreCode = document.getElementById("PRPS_SIDE_GENRE").value;
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk\"  id=\"iChk_'+iRowIdx+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce';
		innerHTMLStr = '<input name=\"displaySeq\" id=\"displaySeq_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
																		
																				
		// 저작물 정보
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"WORK_NAME\"  id=\"WORK_NAME_'+iRowIdx+'\" class="inputData w90" title=\"저작물명\" rangeSize="~100" nullCheck/>';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		//innerHTMLStr = '<input name=\"IMAGE_DIVS\"  id=\"IMAGE_DIVS_'+iRowIdx+'\" class="inputData w90" title=\"분야\" rangeSize="~100" nullCheck />';
		if(genreCode == "1" ){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\" ><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">무용<\/option><option value=\"2\">발레<\/option><option value=\"3\">무언극<\/option><option value=\"4\">뮤지컬<\/option>';
			innerHTMLStr += '<option value=\"5\">오페라<\/option><option value=\"6\">마당극<\/option><option value=\"7\">즉흥극<\/option><option value=\"8\">창극<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "2"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">건축물<\/option><option value=\"2\">건축설계서<\/option><option value=\"3\">건축물모형<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "3"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">(통수목적)지도<\/option><option value=\"2\">도표<\/option><option value=\"3\">설계도(건촉설계도 제외)<\/option><option value=\"4\">모형<\/option>';
			innerHTMLStr += '<option value=\"5\">지구의<\/option><option value=\"6\">약도<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "4"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">사무관리<\/option><option value=\"2\">과학기술<\/option><option value=\"3\">교육<\/option><option value=\"4\">오락<\/option>';
			innerHTMLStr += '<option value=\"5\">기업관리<\/option><option value=\"6\">콘텐츠개발SW<\/option><option value=\"7\">프로그램SW<\/option><option value=\"8\">산업용SW<\/option>';
			innerHTMLStr += '<option value=\"9\">서체글꼴SW<\/option><option value=\"10\">제어프로그램<\/option><option value=\"11\">언어처리<\/option><option value=\"12\">유틸리티<\/option>';
			innerHTMLStr += '<option value=\"13\">데이터통신<\/option><option value=\"14\">테이터베이스<\/option><option value=\"15\">보안SW<\/option><option value=\"16\">미들웨어<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "5"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">사전<\/option><option value=\"2\">홈페이지<\/option><option value=\"3\">문학전집<\/option><option value=\"4\">시집<\/option>';
			innerHTMLStr += '<option value=\"5\">신문<\/option><option value=\"6\">잡지<\/option><option value=\"7\">악보집<\/option><option value=\"8\">논문집<\/option>';
			innerHTMLStr += '<option value=\"9\">백과사전<\/option><option value=\"10\">교육교재<\/option><option value=\"11\">카탈로그<\/option><option value=\"12\">단어집<\/option>';
			innerHTMLStr += '<option value=\"13\">문제집<\/option><option value=\"14\">설문지<\/option><option value=\"15\">인명부<\/option><option value=\"16\">전단<\/option><option value=\"17\">데이터베이스<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "6" || genreCode == "7"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"99\">기타<\/option><\/select>';
		}				

		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<select name=\"OPEN_MEDI_CODE\" id=\"OPEN_MEDI_CODE_'+iRowIdx+'\" title=\"공표매체코드\"><option value=\"\">선택<\/option>';
		innerHTMLStr += '<option value=\"1\">출판사명<\/option><option value=\"2\">배포대상<\/option><option value=\"3\">인터넷주소<\/option><option value=\"4\">공연장소<\/option>';
		innerHTMLStr += '<option value=\"5\">방송사 및 프로그램명<\/option><option value=\"6\">기타<\/option><\/select>';
		innerHTMLStr += '<input name=\"OPEN_MEDI_TEXT\"  id=\"OPEN_MEDI_TEXT_'+iRowIdx+'\" class="inputData w90" title=\"공표매체\" rangeSize="~30" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		//innerHTMLStr = '<input name=\"OPEN_DATE\"  id=\"OPEN_DATE_'+iRowIdx+'\" class="inputData w90" title=\"공표일자\" character="KE"  maxlength="8" rangeSize="8" nullCheck />';
		innerHTMLStr = '<input class="inputData" type="text" id=\"OPEN_DATE_'+iRowIdx3+'\" name=\"OPEN_DATE_'+iRowIdx3+'\" size="8" maxlength="8" value=""/> &nbsp;';
		innerHTMLStr += '<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'OPEN_DATE_'+iRowIdx3+'\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'OPEN_DATE_'+iRowIdx3+'\');" alt="달력" title="공표일자를 선택하세요." align="middle" style="cursor:pointer;"/>'
		innerHTMLStr += '<input name=\"OPEN_DATE\" type="hidden" class="inputData w90" value="'+iRowIdx3+'"/>';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		cur_cell.innerHTML = innerHTMLStr;

		if(document.getElementById("PRPS_RGHT_CODE").value == '1' || document.getElementById("PRPS_RGHT_CODE").value == '2' ) {
		// 저작권자
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"COPT_HODR\" id=\"COPT_HODR_'+iRowIdx+'\" class="'+classId+' w90"  title="저작권자" rangeSize="~100" />';
		innerHTMLStr += '<input type="hidden" name="COPT_HODR_ORGN" />';
		cur_cell.innerHTML = innerHTMLStr;
		}
		fn_resetSeq("displaySeq");
		
		// setfocus
		//var oFuc = 'MUSIC_TITLE_'+iRowIdx;
		//	document.getElementById(oFuc).focus();
		
		iRowIdx++;
		iRowIdx3++;
	}
	
	// 이용자 권리조회
	else if( tableId == 'listTab_2' ) {
		var genreCode = document.getElementById("PRPS_SIDE_GENRE").value;
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk2\"  id=\"iChk2_'+iRowIdx+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce';
		innerHTMLStr = '<input name=\"displaySeq2\" id=\"displaySeq2_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작물 정보
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"WORK_NAME\"  id=\"WORK_NAME_'+iRowIdx+'\" class="inputData w90" title=\"이미지명\" rangeSize="~100"  nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		//innerHTMLStr = '<input name=\"IMAGE_DIVS\"  id=\"IMAGE_DIVS_'+iRowIdx2+'\" class="inputData w90" title=\"분야\" rangeSize="~100" nullCheck />';
		if(genreCode == "1" ){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\" ><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">무용<\/option><option value=\"2\">발레<\/option><option value=\"3\">무언극<\/option><option value=\"4\">뮤지컬<\/option>';
			innerHTMLStr += '<option value=\"5\">오페라<\/option><option value=\"6\">마당극<\/option><option value=\"7\">즉흥극<\/option><option value=\"8\">창극<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "2"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">건축물<\/option><option value=\"2\">건축설계서<\/option><option value=\"3\">건축물모형<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "3"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">(통수목적)지도<\/option><option value=\"2\">도표<\/option><option value=\"3\">설계도(건촉설계도 제외)<\/option><option value=\"4\">모형<\/option>';
			innerHTMLStr += '<option value=\"5\">지구의<\/option><option value=\"6\">약도<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "4"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">사무관리<\/option><option value=\"2\">과학기술<\/option><option value=\"3\">교육<\/option><option value=\"4\">오락<\/option>';
			innerHTMLStr += '<option value=\"5\">기업관리<\/option><option value=\"6\">콘텐츠개발SW<\/option><option value=\"7\">프로그램SW<\/option><option value=\"8\">산업용SW<\/option>';
			innerHTMLStr += '<option value=\"9\">서체글꼴SW<\/option><option value=\"10\">제어프로그램<\/option><option value=\"11\">언어처리<\/option><option value=\"12\">유틸리티<\/option>';
			innerHTMLStr += '<option value=\"13\">데이터통신<\/option><option value=\"14\">테이터베이스<\/option><option value=\"15\">보안SW<\/option><option value=\"16\">미들웨어<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "5"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"1\">사전<\/option><option value=\"2\">홈페이지<\/option><option value=\"3\">문학전집<\/option><option value=\"4\">시집<\/option>';
			innerHTMLStr += '<option value=\"5\">신문<\/option><option value=\"6\">잡지<\/option><option value=\"7\">악보집<\/option><option value=\"8\">논문집<\/option>';
			innerHTMLStr += '<option value=\"9\">백과사전<\/option><option value=\"10\">교육교재<\/option><option value=\"11\">카탈로그<\/option><option value=\"12\">단어집<\/option>';
			innerHTMLStr += '<option value=\"13\">문제집<\/option><option value=\"14\">설문지<\/option><option value=\"15\">인명부<\/option><option value=\"16\">전단<\/option><option value=\"17\">데이터베이스<\/option><option value=\"99\">기타<\/option><\/select>';
		}else if(genreCode == "6" || genreCode == "7"){
			innerHTMLStr = '<select name=\"PRPS_SIDE_R_GENRE\" id=\"PRPS_SIDE_R_GENRE'+iRowIdx+'\" title=\"분야코드\"><option value=\"\">선택<\/option>';
			innerHTMLStr += '<option value=\"99\">기타<\/option><\/select>';
		}				

		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<select name=\"OPEN_MEDI_CODE\" id=\"OPEN_MEDI_CODE_'+iRowIdx+'\" title=\"공표매체코드\"><option value=\"\">선택<\/option>';
		innerHTMLStr += '<option value=\"1\">출판사명<\/option><option value=\"2\">배포대상<\/option><option value=\"3\">인터넷주소<\/option><option value=\"4\">공연장소<\/option>';
		innerHTMLStr += '<option value=\"5\">방송사 및 프로그램명<\/option><option value=\"6\">기타<\/option><\/select>';
		innerHTMLStr += '<input name=\"OPEN_MEDI_TEXT\"  id=\"OPEN_MEDI_TEXT_'+iRowIdx+'\" class="inputData w90" title=\"공표매체\" rangeSize="~30" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		//innerHTMLStr = '<input name=\"OPEN_DATE\"  id=\"OPEN_DATE_'+iRowIdx+'\" class="inputData w90" title=\"공표일자\" character="KE"  maxlength="8" rangeSize="8" nullCheck />';
		innerHTMLStr = '<input class="inputData" type="text" id=\"VAL_OPEN_DATE_'+iRowIdx3+'\" name=\"VAL_OPEN_DATE_'+iRowIdx3+'\" size="8" maxlength="8" value=""/> &nbsp;';
		innerHTMLStr += '<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal(\'prpsForm\',\'VAL_OPEN_DATE_'+iRowIdx3+'\');" onkeypress="javascript:fn_cal(\'prpsForm\',\'VAL_OPEN_DATE_'+iRowIdx3+'\');" alt="달력" title="공표일자를 선택하세요." align="middle" style="cursor:pointer;"/>'
		innerHTMLStr += '<input name=\"VAL_OPEN_DATE\" type="hidden" class="inputData w90" value="'+iRowIdx3+'"/>';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="COPT_HODR" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		fn_resetSeq("displaySeq2");
		
		// setfocus
		//	var oFuc = 'MUSIC_TITLE_'+iRowIdx2;
		//	document.getElementById(oFuc).focus();
		
		iRowIdx2++;
		iRowIdx3++;
		
	}
	
}

// 신청목적 선택
function prps_check(chk) {

	// 권리자 권리찾기
	if(chk.value == '1' || chk.value == '2' ) {
		document.getElementById("div_1").style.display = "";
		//document.getElementById("div_2").style.display = "";
		//fn_lock( "listTab", "" );
		//fn_lock( "listTab_2", "true" );

		var idx = document.getElementById("IROWIDX").value;
		for( i=0; i<idx; i++) {

			var u = i+1;
			var s = eval("document.prpsForm.COPT_HODR_"+u);
			s.style.display = ""
		}
		
		// 스크롤에 의한 height reset
		scrollSet("listTab", "div_scroll_1", "iChk");
		
		// 토탈건수
		setTotCnt( "iChk", "totalRow");

		// 첨부파일명 및 안내문구 제어
		document.getElementById("attFileTitle").innerHTML = "첨부파일(증빙서류)";		
		document.getElementById("attFileMemo1").style.display = "block";
		document.getElementById("attFileMemo2").style.display = "block";

	} else {
		//document.getElementById("div_2").style.display = "none";
		document.getElementById("div_1").style.display = "";
		//fn_lock( "listTab_2", "" );
		//fn_lock( "listTab", "true" );
		
		var idx = document.getElementById("IROWIDX").value;
		for( i=0; i<idx; i++) {

			var u = i+1;
			var s = eval("document.prpsForm.COPT_HODR_"+u);
			s.style.display = "none"
		}
			
		// 스크롤에 의한 height reset
		//scrollSet("listTab_2", "div_scroll_2", "iChk2");
		scrollSet("listTab", "div_scroll_1", "iChk");
		// 토탈건수
		//setTotCnt( "iChk2", "totalRow2");
		setTotCnt( "iChk", "totalRow");

		// 첨부파일명 및 안내문구 제어
		document.getElementById("attFileTitle").innerHTML = "첨부파일";		
		document.getElementById("attFileMemo1").style.display = "none";
		document.getElementById("attFileMemo2").style.display = "none";
	}
	
}

// 신탁관리단체 선택
function trust_check(chk) {
	
	var nRow = document.getElementsByName("COPT_HODR").length;	// 전체 길이
	
	var oInLic = document.getElementsByName("COPT_HODR");
	
	for( k=0; k<nRow; k++) {
		
		if(chk.checked){
			
			oInLic[k].readOnly = false;
			
			oInLic[k].className = 'inputData w90';
		}
		else {
			
			oInLic[k].readOnly = true;
			
			oInLic[k].className = 'inputDisible w90';
		}
		
	} // .. end for
}

// 권리찾기수정
function rghtPrpsModi() {

	var frm = document.prpsForm;

	var PRPS_SIDE_GENRE = document.getElementById("PRPS_SIDE_GENRE").value;

	if(PRPS_SIDE_GENRE == ''){
		alert("신청저작물장르를 선택 하세요.");
		document.getElementById("PRPS_SIDE_GENRE").focus();
		return;
	}	

	var trstOrgnLen = document.getElementsByName("TRST_ORGN_CODE").length;
	var kCnt = 0;
	
	for( kk=0; kk<trstOrgnLen && kCnt ==0; kk++) {
		
		if(document.getElementsByName("TRST_ORGN_CODE")[kk].checked){
			kCnt += 1;	break;
		}
	}
	
	if(kCnt==0) {
		alert('권리구분을(를) 선택하세요.');
		
		if(PRPS_SIDE_GENRE == '6')
			document.getElementById("CHK_216_2").focus();
		else if(PRPS_SIDE_GENRE == '7')
			document.getElementById("CHK_215_2").focus();
		else
			document.getElementsByName("TRST_ORGN_CODE")[0].focus();
		return;
	}
	
	if( document.getElementsByName("iChkVal").length <1 ){
		alert('신청 저작물(를) 추가하세요.');
		document.getElementById("workAdd").focus();
		return;
	}

	var length = document.getElementsByName("PRPS_SIDE_R_GENRE").length;

	var u = 1;
	for( i=0; i<length; i++) {
		var s = eval("document.prpsForm.PRPS_SIDE_R_GENRE"+u);
		if(s.value == ''){
			alert("분야를 선택 하세요.");
			return;
		}	
		u++;
	}

	if(checkForm(frm)) {
		
		frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrpsModiProc&DIVS=X";
		
		frm.method="post";
		frm.submit();

	}
}

// 테이블 하위 disable
function fn_lock( tableId, flag ){
	
	var oTbl = document.getElementById(tableId);
	
	var oInput = oTbl.getElementsByTagName("input");
	for(i=0; i<oInput.length;i++){
		oInput[i].disabled= flag ;
	}
	
	var oSelect = oTbl.getElementsByTagName("select");
	for(i=0; i<oSelect.length;i++){
		oSelect[i].disabled= flag ;
	}
	
}

// 오프라인 접수
function offLine_check(chk) {
	
	if(chk.checked) {
		document.getElementById("td_file_no").style.display = "";
		document.getElementById("td_file_yes").style.display = "none";
	} else{
		document.getElementById("td_file_no").style.display = "none";
		document.getElementById("td_file_yes").style.display = "";
	} 
}

// 목록 
function goList(){
	
	var frm = document.prpsForm;
	frm.action = '/rsltInqr/rsltInqr.do?DIVS=10&page_no=1';
	frm.method="post";
	frm.submit();
}

function showAttach(cnt) {
	var i=0;
	var content = "";
	var attach = document.getElementById("attach")
	attach.innerHTML = "";
	//document.all("attach").innerHTML = "";
	content += '<table>';
	for (i=0; i<cnt; i++) {
		content += '<tr>';
		content += '<td><input type="file" name="attachfile'+(i+1)+'" size="70;" class="inputFile"><\/td>';
		content += '<\/tr>';
	}
	content += '<\/table>';
	//document.all("attach").innerHTML = content;
	attach.innerHTML = content;
}

function applyAtch() {
var ansCnt = document.prpsForm.atchCnt.value;
showAttach(parseInt(ansCnt));
}

function Clear()
{		
	var idx = document.getElementById("IROWIDX").value;
	var PRPS_SIDE_GENRE = document.getElementById("PRPS_SIDE_GENRE").value;

	if(PRPS_SIDE_GENRE == ''){
		alert("신청저작물장르를 선택 하세요.");
		document.getElementById("PRPS_SIDE_GENRE").focus();
		return;
	}else{
		for( i=0; i<idx; i++) {
			var val = i+1;
			var PRPS_SIDE_R_GENRE = eval("document.prpsForm.PRPS_SIDE_R_GENRE"+val);

			//var PRPS_SIDE_R_GENRE = document.getElementById("PRPS_SIDE_R_GENRE1")
			if(PRPS_SIDE_R_GENRE != undefined)
			{
				 while(PRPS_SIDE_R_GENRE.options.length > 0)
					 PRPS_SIDE_R_GENRE.options.remove(0);
		   }
		}	
		var genreArray = null;
		var genre_val = null;
		for( i=0; i<idx; i++) {
			var u = i+1;
			var s = eval("document.prpsForm.PRPS_SIDE_R_GENRE"+u);
			var genreCode = document.getElementById("PRPS_SIDE_GENRE").value;
			
			if(s != undefined)
			{
	
				if(genreCode == "1" ){
					genreArray = new Array("선택","무용", "발레", "무언극", "뮤지컬", "오페라", "마당극","즉흥극","창극","기타");	
					genreArray_val = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "99");
				}else if(genreCode == "2"){
					genreArray = new Array("선택","건축물모형", "건축설계서", "건축물모형","기타");	
					genreArray_val = new Array("0", "1", "2", "3", "99");
				}else if(genreCode == "3"){
					genreArray = new Array("선택","(통수목적)지도", "도표", "설계도(건촉설계도 제외)","모형", "지구의", "약도", "기타");	
					genreArray_val = new Array("0", "1", "2", "3", "4", "5", "6", "99");
				}else if(genreCode == "4"){
					genreArray = new Array("선택","사무관리", "과학기술", "교육","오락", "기업관리", "콘텐츠개발SW", "프로그램SW", "산업용SW", "서체글꼴SW", "제어프로그램", "언어처리", "유틸리티", "데이터통신", "테이터베이스", "보안SW", "미들웨어", "기타");	
					genreArray_val = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "99");
				}else if(genreCode == "5"){
					genreArray = new Array("선택","사전", "홈페이지", "문학전집","시집", "신문", "잡지", "악보집", "논문집", "백과사전", "교육교재", "카탈로그", "단어집", "문제집", "설문지", "인명부", "전단", "데이터베이스", "기타");	
					genreArray_val = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "99");
				}else if(genreCode == "6" || genreCode == "7"){
					genreArray = new Array("선택","기타");	
					genreArray_val = new Array("0","99");
				} 	
		
				 for(i = 0; i < genreArray.length; i++) {
					 Append(genreArray[i],genreArray_val[i],idx);
				 }
			}
		}
	}	
}

// 신청저작물에 따른 권리구분setting
function chgTrstOrgnCode(setYN, selOptVal)
{	
	// 기본setting
	if(setYN != 'N') {
		var trstOrgnLen = document.getElementsByName("TRST_ORGN_CODE").length;
		var kCnt = 0;
		
		for( kk=0; kk<trstOrgnLen && kCnt ==0; kk++) {
			document.getElementsByName("TRST_ORGN_CODE")[kk].checked = 0;
		}
	}

	if(selOptVal== '6'){
		document.getElementById("td_1").style.display = "";
		document.getElementById("td_0").style.display = "none";
		document.getElementById("td_2").style.display = "none";

		document.getElementById("CHK_216_2").disabled = false;
		
		document.getElementById("CHK_201").disabled = true;
		document.getElementById("CHK_202").disabled = true;
		document.getElementById("CHK_203").disabled = true;
		document.getElementById("CHK_204").disabled = true;
		document.getElementById("CHK_205").disabled = true;
		document.getElementById("CHK_206").disabled = true;
		document.getElementById("CHK_211").disabled = true;
		document.getElementById("CHK_212").disabled = true;
		document.getElementById("CHK_213").disabled = true;
		document.getElementById("CHK_214").disabled = true;
		document.getElementById("CHK_215").disabled = true;
		document.getElementById("CHK_215_2").disabled = true;
	}else if(selOptVal== '7'){
		document.getElementById("td_2").style.display = "";
		document.getElementById("td_0").style.display = "none";
		document.getElementById("td_1").style.display = "none";

		document.getElementById("CHK_215_2").disabled = false;
		
		document.getElementById("CHK_201").disabled = true;
		document.getElementById("CHK_202").disabled = true;
		document.getElementById("CHK_203").disabled = true;
		document.getElementById("CHK_204").disabled = true;
		document.getElementById("CHK_205").disabled = true;
		document.getElementById("CHK_206").disabled = true;
		document.getElementById("CHK_211").disabled = true;
		document.getElementById("CHK_212").disabled = true;
		document.getElementById("CHK_213").disabled = true;
		document.getElementById("CHK_214").disabled = true;
		document.getElementById("CHK_215").disabled = true;
		document.getElementById("CHK_216_2").disabled = true;
		
	}else{
		document.getElementById("td_0").style.display = "";
		document.getElementById("td_1").style.display = "none";
		document.getElementById("td_2").style.display = "none";

		document.getElementById("CHK_201").disabled = false;
		document.getElementById("CHK_202").disabled = false;
		document.getElementById("CHK_203").disabled = false;
		document.getElementById("CHK_204").disabled = false;
		document.getElementById("CHK_205").disabled = false;
		document.getElementById("CHK_206").disabled = false;
		document.getElementById("CHK_211").disabled = false;
		document.getElementById("CHK_212").disabled = false;
		document.getElementById("CHK_213").disabled = false;
		document.getElementById("CHK_214").disabled = false;
		document.getElementById("CHK_215").disabled = false;
		
		document.getElementById("CHK_215_2").disabled = true;
		document.getElementById("CHK_216_2").disabled = true;	
	}
}

function Append(genre,val,idx)
{
 for(a=0; a<idx; a++){	
	 var option = new Option()
	 var u = a+1;
	 var s = eval("document.prpsForm.PRPS_SIDE_R_GENRE"+u);
	 //var s = document.getElementById("PRPS_SIDE_R_GENRE1");
	 option.value = val;
	 option.text = genre;
	 s.options.add(option);
 }
// option.value = bb;
 //option.text = aa;
 //document.prpsForm.PRPS_SIDE_R_GENRE.options.add(option);
}

 function initParameter(){
	
	// 신청목적에 따른 저작물 테이블 제어
	prps_check(document.getElementById("PRPS_RGHT_CODE"));
	
	// 신청 관리단체에 따른 저작물 테이블 제어
	if( document.getElementById("CHK_201").checked ){
		trust_check(document.getElementById("CHK_201"));
	}
	if( document.getElementById("CHK_202").checked ){
		trust_check(document.getElementById("CHK_202"));
	}
	if( document.getElementById("CHK_203").checked ){
		trust_check(document.getElementById("CHK_203"));
	}
	if( document.getElementById("CHK_204").checked ){
		trust_check(document.getElementById("CHK_204"));
	}
	if( document.getElementById("CHK_205").checked ){
		trust_check(document.getElementById("CHK_205"));
	}
	if( document.getElementById("CHK_206").checked ){
		trust_check(document.getElementById("CHK_206"));
	}
	if( document.getElementById("CHK_211").checked ){
		trust_check(document.getElementById("CHK_211"));
	}
	if( document.getElementById("CHK_212").checked ){
		trust_check(document.getElementById("CHK_212"));
	}
	if( document.getElementById("CHK_213").checked ){
		trust_check(document.getElementById("CHK_213"));
	}
	if( document.getElementById("CHK_214").checked ){
		trust_check(document.getElementById("CHK_214"));
	}
	if( document.getElementById("CHK_215").checked ){
		trust_check(document.getElementById("CHK_215"));
	}
	//if( document.getElementById("CHK_216").checked ){
	//	trust_check(document.getElementById("CHK_216"));
	//}
	if( document.getElementById("CHK_215_2").checked ){
		trust_check(document.getElementById("CHK_215_2"));
	}
	if( document.getElementById("CHK_216_2").checked ){
		trust_check(document.getElementById("CHK_216_2"));
	}

	chgTrstOrgnCode('N', "${rghtPrps.PRPS_SIDE_GENRE}");

	fn_createTable2("divFileList", "0");
	fn_setFileInfo();
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 


// 리사이즈
function resizeIFrame(name) {

  var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
  document.getElementById(name).height= the_height+5;
  
}

function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}

//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_3.png" alt="신청현황조회" title="신청현황조회" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li class="active"><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1">저작권찾기<span>&lt;</span></a></li>
							<li><a href="/rsltInqr/rsltInqr.do?DIVS=20&amp;page_no=1">보상금<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>신청현황조회</span>&gt;<strong>저작권찾기</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>기타저작물 저작권찾기신청 수정
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>

					<!-- memo 삽입 -->
						<jsp:include page="/common/memo/memo_01.jsp">
							<jsp:param name="DIVS" value="${DIVS}"/>
						</jsp:include>
					<!-- // memo 삽입 -->
					
					<div class="bbsSection">
						<h4>기타저작권찾기신청 수정</h4>
						<form name="fileForm" action="">
							<!-- 파일다운로드 -->
							<input type="hidden" name="filePath" />
							<input type="hidden" name="fileName" />
							<input type="hidden" name="realFileName" />
						</form>
						<form name="prpsForm" enctype="multipart/form-data" action="">
							<input type="hidden" name="USER_IDNT" value="${rghtPrps.USER_IDNT}"/>
							<input type="hidden" name="PRPS_MAST_KEY" value="${rghtPrps.PRPS_MAST_KEY}"/>
							<input type="hidden" name="PRPS_SEQN" value="${rghtPrps.PRPS_SEQN}"/>
							<input type="hidden" name="IROWIDX" id="IROWIDX" value="${IROWIDX}" />
							<input type="hidden" name="BACK_IROWIDX" id="BACK_IROWIDX" value="${IROWIDX}" />
							
							<fieldset>
								<legend></legend>
								<!-- 테이블 영역입니다 -->
								<div class="tabelRound">
									<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="저작권찾기 신청정보 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="10%">
										<col width="10%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row" rowspan="2" class="bgbr lft">신청정보</th>
												<th scope="row" class="bgbr2 lft"><label for="PRPS_RGHT_CODE" class="necessary">신청목적</label></th>
												<td>
													<select name="PRPS_RGHT_CODE" id="PRPS_RGHT_CODE"  title="신청목적" nullCheck onchange="prps_check(this);">
														<option value="">선택 ------</option>
														<option value="1" <c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">selected="selected"</c:if>>권리자 저작권찾기</option>
														<!-- 
														<option value="2" <c:if test="${rghtPrps.PRPS_RGHT_CODE == '2'}">selected="selected"</c:if>>대리인 권리찾기</option>
														 -->
														<option value="3" <c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">selected="selected"</c:if>>이용자 저작권조회</option>
													</select>
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr2 lft">신청인 정보</th>
												<td>
													<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
													<colgroup>
														<col width="15%">
														<col width="*">
														<col width="20%">
														<col width="30%">
														</colgroup>
														<tbody>
															<tr>
																<th class="bgbr3 lft">성명</th>
																<td>${rghtPrps.USER_NAME}</td>
																<th class="bgbr3 lft">주민번호/사업자번호</th>
																<td>${rghtPrps.RESD_CORP_NUMB_VIEW}</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">전화</th>
																<td>
																	<label class="labelBlock"><em class="w45">자택</em><input name="HOME_TELX_NUMB" title="자택전화" class="inputData w60" value="${rghtPrps.HOME_TELX_NUMB}"/></label>
																	<label class="labelBlock"><em class="w45">사무실</em><input name="BUSI_TELX_NUMB" title="사무실전화" class="inputData w60" value="${rghtPrps.BUSI_TELX_NUMB}"/></label>
																	<label class="labelBlock"><em class="w45">휴대폰</em><input name="MOBL_PHON" title="휴대폰" class="inputData w60" value="${rghtPrps.MOBL_PHON}"/></label>
																</td>
																<th class="bgbr3 lft"><label for="sch3">Fax</label></th>
																<td><input class="inputData" name="FAXX_NUMB" id="sch3" value="${rghtPrps.FAXX_NUMB}"/></td>
															</tr>
															<tr>
																<th class="bgbr3 lft"><label for="sch4">이메일주소</label></th>
																<td colspan="3"><input name="MAIL" class="inputData w90" id="sch4" value="${rghtPrps.MAIL}"/></td>
															</tr>
															<tr>
																<th class="bgbr3 lft">주소</th>
																<td colspan="3">
																	<label class="labelBlock"><em class="w45">자택</em><input name="HOME_ADDR" title="자택 주소" class="inputData w80" value="${rghtPrps.HOME_ADDR}"/></label>
																	<label class="labelBlock"><em class="w45">사무실</em><input name="BUSI_ADDR" title="사무실 주소" class="inputData w80" value="${rghtPrps.BUSI_ADDR}"/></label>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft"><label class="necessary">신청저작물장르</label></th>
												<td>
													<select name="PRPS_SIDE_GENRE" id="PRPS_SIDE_GENRE" title="PRPS_SIDE_GENRE" onChange="Clear();chgTrstOrgnCode('Y', this.value)">
														<option value="">선택</option>
														<option value="1" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '1'}">selected="selected"</c:if>>연극</option>
														<option value="2" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '2'}">selected="selected"</c:if>>건축</option>
														<option value="3" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '3'}">selected="selected"</c:if>>도형</option>
														<option value="4" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '4'}">selected="selected"</c:if>>컴퓨터프로그램</option>
														<option value="5" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '5'}">selected="selected"</c:if>>편집</option>
														<!-- <option value="6" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '6'}">selected="selected"</c:if>>공공콘텐츠</option>  -->
														<option value="7" <c:if test="${rghtPrps.PRPS_SIDE_GENRE == '7'}">selected="selected"</c:if>>뉴스</option>
													</select>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft"><label class="necessary">권리구분</label></th>
												<td id="td_0" style="display">
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_201" value="201" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_201 == '1'}">checked</c:if> title="저작권자(한국음악저작권협회)" />저작권자(한국음악저작권협회)</label><br/>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_202" value="202" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_202 == '1'}">checked</c:if> title="저작권자(한국음악실연자연합회)" />저작권자(한국음악실연자연합회)</label><br/>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_203" value="203" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_203 == '1'}">checked</c:if> title="저작권자(한국음원제작자협회)" />저작권자(한국음원제작자협회)</label><br/>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_204" value="204" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_204 == '1'}">checked</c:if> title="저작권자(한국문예학술저작권협회)" />저작권자(한국문예학술저작권협회)</label><br/>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_205" value="205" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_205 == '1'}">checked</c:if> title="저작권자(한국복사전송권협회)" />저작권자(한국복사전송권협회)</label><br/>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_206" value="206" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_206 == '1'}">checked</c:if> title="저작권자(한국방송작가협회)" />저작권자(한국방송작가협회)</label><br/>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_211" value="211" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_211 == '1'}">checked</c:if> title="저작권자(한국영상산업협회)" />저작권자(한국영상산업협회)</label><br/>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_212" value="212" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_212 == '1'}">checked</c:if> title="저작권자(한국시나리오작가협회)" />저작권자(한국시나리오작가협회)</label><br/>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_213" value="213" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_213 == '1'}">checked</c:if> title="저작권자(한국영화제작자협회)" />저작권자(한국영화제작자협회)</label><br/>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_214" value="214" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_214 == '1'}">checked</c:if> title="저작권자(한국방송실연자연합회)" />저작권자(한국방송실연자연합회)</label><br/>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_215" value="215" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_215 == '1'}">checked</c:if> title="뉴스 작가 - 저작권자(한국언론진흥재단)" />뉴스 작가 - 저작권자(한국언론진흥재단)</label><br/>
													<!-- 	
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_216" value="216" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_216 == '1'}">checked</c:if>/>저작권(한국문화콘텐츠진흥원)</label>
													 -->
												</td>
												<td id="td_1" style="display:none">
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_216_2" value="216" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_216 == '1'}">checked</c:if> title="저작권(한국문화콘텐츠진흥원)" />저작권(한국문화콘텐츠진흥원)</label>
												</td>
												<td id="td_2" style="display:none">
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_215_2" value="215" class="inputRChk" onclick="trust_check(this);" <c:if test="${rghtPrps.CHK_215 == '1'}">checked</c:if> title="뉴스 작가 - 저작권자(한국언론진흥재단)" />뉴스 작가 - 저작권자(한국언론진흥재단)</label>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft"><label class="necessary">신청저작물정보</label></th>
												<td>	
													<div class="floatDiv mb5">
														<p class="fl"><label class="blue"> * 권리있는 항목만 입력하세요.</label></p>
														<p class="fr">
														<span class="button small type4 icon"><span class="arrow"></span><a href="javascript:editTable('I');"  id="workAdd">추가</a></span> 
														<span class="button small type3"><a href="javascript:editTable('D');">삭제</a></span>
														</p>
													</div>
													
													<!-- 테이블 영역입니다 -->
													<div id="div_1" class="tabelRound" style="width:583px; padding:0 0 0 0; display:none"> 
														<h5>권리자 저작권찾기 <span id="totalRow"></span></h5>
														<div id="div_scroll_1" class="tabelRound div_scroll" style="width:583px; padding:0 0 0 0;">
														<table id="listTab" cellspacing="0" cellpadding="0" border="1" class="grid" summary="권리자 저작권찾기 신청저작물정보 입력폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
																<col width="10">
																<col width="10">
																<col width="80">
																<col width="50">
																<col width="80">
																<col width="80">
																<col width="80">
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk',this);" title="전체선택" /></th>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH"><label class="necessary">저작물명</label></th>
																	<th scope="col" class="headH"><label class="necessary">분야</label></th>
																	<th scope="col" class="headH"><label class="necessary">공표매체</label></th>
																	<th scope="col" class="headH">공표일자</th>
																	<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">
																	<th scope="col" class="headH"><label class="necessary">저작권자</label></th>
																	</c:if>
																	<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">
																	<th scope="col" class="headH">저작권자</th>
																	</c:if>
																</tr>
															</thead>
															<tbody>
															<c:if test="${!empty workList}">
																<c:forEach items="${workList}" var="workList">
																	<c:set var="NO" value="${workList.totalRow}"/>
																	<c:set var="i" value="${i+1}"/>
																<tr>
																	<td class="ce"><input type="checkbox" name="iChk" id="iChk_${i}" class="vmid" title="선택" /></td>
																	<td class="ce"><input name="displaySeq" id="displaySeq_${i}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}" title="순번" /></td>
																
																	<!-- 기존Meta저작물 -->
																	<!--<c:if test="${workList.PRPS_IDNT_CODE == '1'}">
																	<td><a href="javascript:openImageDetail('${workList.WORK_FILE_NAME }','${workList.WORK_NAME }');"><u>${workList.WORK_NAME }</u></a></td>
																	<td>${workList.IMAGE_DIVS }</td>
																	<td>&nbsp;</td>
																	<td class="ce">&nbsp;
																		<input type="hidden" name="iChkVal" value="${workList.PRPS_IDNT}|${workList.PRPS_IDNT_NR}|${workList.PRPS_IDNT_ME}"/>
																		<input type="hidden" name="WORK_NAME" value="${workList.WORK_NAME }"/>
																		<input type="hidden" name="IMAGE_DIVS" value="${workList.IMAGE_DIVS }"/>
																		<input type="hidden" name="OPEN_MEDI_CODE" />
																		<input type="hidden" name="OPEN_MEDI_TEXT" />
																		<input type="hidden" name="OPEN_DATE" />
																	</td>
																	</c:if>
																	-->
																	<!-- 추가등록저작물 -->
																	<!--<c:if test="${workList.PRPS_IDNT_CODE == '2'}">-->
																	<td><input name="WORK_NAME" id="WORK_NAME_${i}" class="inputData w90" value="${workList.WORK_NAME_TRNS }" title="이미지명" rangeSize="~100" nullCheck /></td>
																	<td>
																		<c:if test="${rghtPrps.PRPS_SIDE_GENRE == 1}">
																			<select name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																				<option value="">선택</option>
																				<option value="1" <c:if test="${workList.PRPS_SIDE_R_GENRE == '1'}">selected="selected"</c:if>>무용</option>
																				<option value="2" <c:if test="${workList.PRPS_SIDE_R_GENRE == '2'}">selected="selected"</c:if>>발레</option>
																				<option value="3" <c:if test="${workList.PRPS_SIDE_R_GENRE == '3'}">selected="selected"</c:if>>무언극</option>
																				<option value="4" <c:if test="${workList.PRPS_SIDE_R_GENRE == '4'}">selected="selected"</c:if>>뮤지컬</option>
																				<option value="5" <c:if test="${workList.PRPS_SIDE_R_GENRE == '5'}">selected="selected"</c:if>>오페라</option>
																				<option value="6" <c:if test="${workList.PRPS_SIDE_R_GENRE == '6'}">selected="selected"</c:if>>마당극</option>
																				<option value="7" <c:if test="${workList.PRPS_SIDE_R_GENRE == '7'}">selected="selected"</c:if>>즉흥극</option>
																				<option value="8" <c:if test="${workList.PRPS_SIDE_R_GENRE == '8'}">selected="selected"</c:if>>창극</option>
																				<option value="99" <c:if test="${workList.PRPS_SIDE_R_GENRE == '99'}">selected="selected"</c:if>>기타</option>
																			</select>
																		</c:if>
																		<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '2'}">
																			<select name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																				<option value="">선택</option>
																				<option value="1" <c:if test="${workList.PRPS_SIDE_R_GENRE == '1'}">selected="selected"</c:if>>건축물</option>
																				<option value="2" <c:if test="${workList.PRPS_SIDE_R_GENRE == '2'}">selected="selected"</c:if>>건축설계서</option>
																				<option value="3" <c:if test="${workList.PRPS_SIDE_R_GENRE == '3'}">selected="selected"</c:if>>건축물모형</option>
																				<option value="99" <c:if test="${workList.PRPS_SIDE_R_GENRE == '99'}">selected="selected"</c:if>>기타</option>
																			</select>
																		</c:if>
																		<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '3'}">
																			<select name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																				<option value="">선택</option>
																				<option value="1" <c:if test="${workList.PRPS_SIDE_R_GENRE == '1'}">selected="selected"</c:if>>(통수목적)지도</option>
																				<option value="2" <c:if test="${workList.PRPS_SIDE_R_GENRE == '2'}">selected="selected"</c:if>>도표</option>
																				<option value="3" <c:if test="${workList.PRPS_SIDE_R_GENRE == '3'}">selected="selected"</c:if>>설계도(건촉설계도 제외)</option>
																				<option value="4" <c:if test="${workList.PRPS_SIDE_R_GENRE == '4'}">selected="selected"</c:if>>모형</option>
																				<option value="5" <c:if test="${workList.PRPS_SIDE_R_GENRE == '5'}">selected="selected"</c:if>>지구의</option>
																				<option value="6" <c:if test="${workList.PRPS_SIDE_R_GENRE == '6'}">selected="selected"</c:if>>약도</option>
																				<option value="99" <c:if test="${workList.PRPS_SIDE_R_GENRE == '99'}">selected="selected"</c:if>>기타</option>
																			</select>
																		</c:if>
																		<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '4'}">
																			<select name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																				<option value="">선택</option>
																				<option value="1" <c:if test="${workList.PRPS_SIDE_R_GENRE == '1'}">selected="selected"</c:if>>사무관리</option>
																				<option value="2" <c:if test="${workList.PRPS_SIDE_R_GENRE == '2'}">selected="selected"</c:if>>과학기술</option>
																				<option value="3" <c:if test="${workList.PRPS_SIDE_R_GENRE == '3'}">selected="selected"</c:if>>교육</option>
																				<option value="4" <c:if test="${workList.PRPS_SIDE_R_GENRE == '4'}">selected="selected"</c:if>>오락</option>
																				<option value="5" <c:if test="${workList.PRPS_SIDE_R_GENRE == '5'}">selected="selected"</c:if>>기업관리</option>
																				<option value="6" <c:if test="${workList.PRPS_SIDE_R_GENRE == '6'}">selected="selected"</c:if>>콘텐츠개발SW</option>
																				<option value="7" <c:if test="${workList.PRPS_SIDE_R_GENRE == '7'}">selected="selected"</c:if>>프로그램SW</option>
																				<option value="8" <c:if test="${workList.PRPS_SIDE_R_GENRE == '8'}">selected="selected"</c:if>>산업용SW</option>
																				<option value="9" <c:if test="${workList.PRPS_SIDE_R_GENRE == '9'}">selected="selected"</c:if>>서체글꼴SW</option>
																				<option value="10" <c:if test="${workList.PRPS_SIDE_R_GENRE == '10'}">selected="selected"</c:if>>제어프로그램</option>
																				<option value="11" <c:if test="${workList.PRPS_SIDE_R_GENRE == '11'}">selected="selected"</c:if>>언어처리</option>
																				<option value="12" <c:if test="${workList.PRPS_SIDE_R_GENRE == '12'}">selected="selected"</c:if>>유틸리티</option>
																				<option value="13" <c:if test="${workList.PRPS_SIDE_R_GENRE == '13'}">selected="selected"</c:if>>데이터통신</option>
																				<option value="14" <c:if test="${workList.PRPS_SIDE_R_GENRE == '14'}">selected="selected"</c:if>>테이터베이스</option>
																				<option value="15" <c:if test="${workList.PRPS_SIDE_R_GENRE == '15'}">selected="selected"</c:if>>보안SW</option>
																				<option value="16" <c:if test="${workList.PRPS_SIDE_R_GENRE == '16'}">selected="selected"</c:if>>미들웨어</option>
																				<option value="99" <c:if test="${workList.PRPS_SIDE_R_GENRE == '99'}">selected="selected"</c:if>>기타</option>
																			</select>
																		</c:if>
																		<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '5'}">
																			<select name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																				<option value="">선택</option>
																				<option value="1" <c:if test="${workList.PRPS_SIDE_R_GENRE == '1'}">selected="selected"</c:if>>사전</option>
																				<option value="2" <c:if test="${workList.PRPS_SIDE_R_GENRE == '2'}">selected="selected"</c:if>>홈페이지</option>
																				<option value="3" <c:if test="${workList.PRPS_SIDE_R_GENRE == '3'}">selected="selected"</c:if>>문학전집</option>
																				<option value="4" <c:if test="${workList.PRPS_SIDE_R_GENRE == '4'}">selected="selected"</c:if>>시집</option>
																				<option value="5" <c:if test="${workList.PRPS_SIDE_R_GENRE == '5'}">selected="selected"</c:if>>신문</option>
																				<option value="6" <c:if test="${workList.PRPS_SIDE_R_GENRE == '6'}">selected="selected"</c:if>>잡지</option>
																				<option value="7" <c:if test="${workList.PRPS_SIDE_R_GENRE == '7'}">selected="selected"</c:if>>악보집</option>
																				<option value="8" <c:if test="${workList.PRPS_SIDE_R_GENRE == '8'}">selected="selected"</c:if>>논문집</option>
																				<option value="9" <c:if test="${workList.PRPS_SIDE_R_GENRE == '9'}">selected="selected"</c:if>>백과사전</option>
																				<option value="10" <c:if test="${workList.PRPS_SIDE_R_GENRE == '10'}">selected="selected"</c:if>>교육교재</option>
																				<option value="11" <c:if test="${workList.PRPS_SIDE_R_GENRE == '11'}">selected="selected"</c:if>>카탈로그</option>
																				<option value="12" <c:if test="${workList.PRPS_SIDE_R_GENRE == '12'}">selected="selected"</c:if>>단어집</option>
																				<option value="13" <c:if test="${workList.PRPS_SIDE_R_GENRE == '13'}">selected="selected"</c:if>>문제집</option>
																				<option value="14" <c:if test="${workList.PRPS_SIDE_R_GENRE == '14'}">selected="selected"</c:if>>설문지</option>
																				<option value="15" <c:if test="${workList.PRPS_SIDE_R_GENRE == '15'}">selected="selected"</c:if>>인명부</option>
																				<option value="16" <c:if test="${workList.PRPS_SIDE_R_GENRE == '16'}">selected="selected"</c:if>>전단</option>
																				<option value="17" <c:if test="${workList.PRPS_SIDE_R_GENRE == '17'}">selected="selected"</c:if>>데이터베이스</option>
																				<option value="99" <c:if test="${workList.PRPS_SIDE_R_GENRE == '99'}">selected="selected"</c:if>>기타</option>
																			</select>
																		</c:if>
																		<c:if test="${rghtPrps.PRPS_SIDE_GENRE == '6' || rghtPrps.PRPS_SIDE_GENRE == '7' }">
																			<select name="PRPS_SIDE_R_GENRE" id="PRPS_SIDE_R_GENRE${i}" title="분야코드">
																				<option value="">선택</option>
																				<option value="99" <c:if test="${workList.PRPS_SIDE_R_GENRE == '99'}">selected="selected"</c:if>>기타</option>
																			</select>
																		</c:if>
																	</td>
																	<td>
																		<select name="OPEN_MEDI_CODE" id="OPEN_MEDI_CODE_${i}" title="공표매체코드">
																			<option value="">선택</option>
																			<option value="1" <c:if test="${workList.OPEN_MEDI_CODE == '1'}">selected="selected"</c:if>>출판사명</option>
																			<option value="2" <c:if test="${workList.OPEN_MEDI_CODE == '2'}">selected="selected"</c:if>>배포대상</option>
																			<option value="3" <c:if test="${workList.OPEN_MEDI_CODE == '3'}">selected="selected"</c:if>>인터넷주소</option>
																			<option value="4" <c:if test="${workList.OPEN_MEDI_CODE == '4'}">selected="selected"</c:if>>공연장소</option>
																			<option value="5" <c:if test="${workList.OPEN_MEDI_CODE == '5'}">selected="selected"</c:if>>방송사 및 프로그램명</option>
																			<option value="6" <c:if test="${workList.OPEN_MEDI_CODE == '6'}">selected="selected"</c:if>>기타</option>
																		</select>
																		<input name="OPEN_MEDI_TEXT" id="OPEN_MEDI_TEXT_${i}" class="inputData w90" value="${workList.OPEN_MEDI_TEXT_TRNS }" title="공표매체" rangeSize="~50" />
																	</td>
																	<td class="ce">
																	<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">
																		<!--<input name="OPEN_DATE" id="OPEN_DATE_${i}" class="inputData w90" value="${workList.OPEN_DATE }" title="공표일자" character='KE'  maxlength="8" rangeSize="8" />-->
																		<input name="OPEN_DATE_${i}"  id="OPEN_DATE_${i}" class="inputData" title="공표일자" maxlength="8" Size="8" character="KE" dateCheck nullCheck  value = "${workList.OPEN_DATE}"/>&nbsp;
																		<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('prpsForm','OPEN_DATE_${i}');" onkeypress="javascript:fn_cal('prpsForm','OPEN_DATE_${i}');" alt="달력" title="공표일자를 선택하세요." align="middle" style="cursor:pointer;"/>
																		<input type="hidden" name="OPEN_DATE" value="${i}"/>
																		<input type="hidden" name="VAL_OPEN_DATE_${i}" id="VAL_OPEN_DATE_${i}"  ="${i}" value = "${workList.OPEN_DATE}"/>
																		<input type="hidden" name="VAL_OPEN_DATE" value="${i}"/>
																	</c:if>	
																	<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">
																		<input name="VAL_OPEN_DATE_${i}"  id="VAL_OPEN_DATE_${i}" class="inputData" title="공표일자" maxlength="8" Size="8" character="KE" dateCheck nullCheck  value = "${workList.OPEN_DATE}"/>&nbsp;
																		<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('prpsForm','VAL_OPEN_DATE_${i}');" onkeypress="javascript:fn_cal('prpsForm','VAL_OPEN_DATE_${i}');" alt="달력" title="공표일자를 선택하세요." align="middle" style="cursor:pointer;"/>
																		<input type="hidden" name="VAL_OPEN_DATE" value="${i}"/>
																		<input type="hidden" name="OPEN_DATE_${i}" id="OPEN_DATE_${i}"  ="${i}" value = "${workList.OPEN_DATE}"/>
																		<input type="hidden" name="OPEN_DATE" value="${i}"/>
																	</c:if>	
																		<input type="hidden" name="iChkVal" value=""/>
																	</td>
																	<!--</c:if>-->
																	<td class="ce"><input name="COPT_HODR" id="COPT_HODR_${i}" class="inputData w90 ce" value="${workList.COPT_HODR_TRNS }" title="저작권자" rangeSize="~100"/>
																		<!-- hidden Value -->
																		<input type="hidden" name="COPT_HODR_ORGN" value="${workList.COPT_HODR_ORGN_TRNS}" />
																	</td>
																</tr>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
														</div>
													</div>
													<!-- //테이블 영역입니다 -->
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">신청저작물설명</th>
												<td>
													<textarea cols="10" rows="10" name="PRPS_RGHT_DESC" id="PRPS_RGHT_DESC" class="h100" title="신청저작물설명" nullCheck>${rghtPrps.PRPS_RGHT_DESC}</textarea>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft"><label for="PRPS_DESC" class="necessary">신청내용</label></th>
												<td>
													<p class="blue">* 신청 목적이 이용자 저작권조회인 경우 이용내용(이용하고자 하는 목적/방법)을 작성합니다.</p>
													<textarea cols="10" rows="10" name="PRPS_DESC" id="PRPS_DESC" class="h100" title="신청내용" nullCheck>${rghtPrps.PRPS_DESC}</textarea>
												</td>
											</tr>
											<tr>									
												<th scope="row" colspan="2" class="bgbr lft">
													<span id="attFileTitle">첨부파일(증빙서류)</span><br />
													<label class="ml5"><input type="checkbox" name="OFFX_LINE_RECP" value="Y" class="inputChk" onclick="javascript:offLine_check(this);" <c:if test="${rghtPrps.OFFX_LINE_RECP == 'Y'}">checked="checked"</c:if> title="오프라인접수" />오프라인접수</label>
												</th>
												<td id="td_file_no" style="display:none">오프라인접수로 선택한 경우 첨부파일을 등록할 수 없습니다.</td>
												<td id="td_file_yes">
								                    <div id="divFileList" class="tabelRound mb5" style="display;">
								                    </div>
												</td>
											</tr>
											<tr> 
												<th  id="attFileMemo1"  scope="row" colspan="2" class="bgbr lft">첨부파일(증빙서류)<br/>안내</th>
												<td  id="attFileMemo2" >
													<div class="infoImg1">
														<ul>
															<li>1. 저작권자임을 증명할 수 있는 서류(저작권등록증 등)</li>
															<li class="mt3">2. 상속, 양도, 승계 등의 사실을 증명할 수 있는 서류 </li>
															<li class="mt3">3. 주민등록등본/법인등기부등본</li>
															<li class="mt3">4. 사업자등록증(사본) 1부</li>
														</ul>
													</div>
												</td>
											</tr>

											<!-- 2010.08.16 : 정확한 데이터입력이 어려움으로 인한 기능제거 (from 관련기관회의) START -->
											<!-- 
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">이용허락 저작재산권</th>
												<td>
													<label class="label_inBlock w20"><input type="checkbox" />복제권</label>
													<label class="label_inBlock w20"><input type="checkbox" />배포권</label>
													<label class="label_inBlock w20"><input type="checkbox" />대여권</label>
													<label class="label_inBlock w20"><input type="checkbox" />공연권</label><br />
													
													<label class="label_inBlock"><input type="checkbox" />공중송신권</label> (<label class="label_inBlock"><input type="checkbox" />방송권</label><label class="label_inBlock ml5"><input type="checkbox" />전송권</label><label class="label_inBlock ml5"><input type="checkbox" />디지털음성송신권</label>)<br />
													
													<label class="label_inBlock w20"><input type="checkbox" />전시권</label>
													<label class="label_inBlock w30"><input type="checkbox" />2차적저작물작성권</label>
												</td>
											</tr>
											 -->
											<!-- 2010.08.16 : 정확한 데이터입력이 어려움으로 인한 기능제거 (from 관련기관회의) END -->
										</tbody>
									</table>
								</div>
								<!-- //테이블 영역입니다 -->
							</fieldset>
						</form>
					</div>
					
					<div class="floatDiv mt10">
						<p class="fl"><span class="button medium icon"><span class="list"></span><a href="javascript:goList()">목록</a></span></p>
						<p class="fr">
							<!-- <span class="button medium icon"><span class="default"></span><a href="javascript:alert('미진행')">보상금 동시 신청</a></span>  -->
							<span class="button medium icon"><span class="default"></span><a href="javascript:rghtPrpsModi();">수정</a></span>
						</p>
					</div>
						
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->

<script type="text/javascript" src="/js/2010/file.js"></script>
<script type="text/javascript">
<!--

	<c:if test="${!empty workList}">
		<c:forEach items="${workList}" var="workList">
			iRowIdx3++;
		</c:forEach>
	</c:if>
	<c:if test="${!empty workList}">
		<c:forEach items="${workList}" var="workList">
			iRowIdx2++;
		</c:forEach>
	</c:if>

	function fn_setFileInfo(){
		
		var listCnt = '${fn:length(fileList)}';

		<c:if test="${!empty fileList}">
			<c:forEach items="${fileList}" var="fileList">
				fn_addGetFile(	'${fileList.TRST_ORGN_CODE}',
								'${fileList.PRPS_MAST_KEY}',
								'${fileList.PRPS_SEQN}',
								'${fileList.ATTC_SEQN}',
								'${fileList.FILE_PATH}',
								'${fileList.REAL_FILE_NAME}',
								'${fileList.FILE_NAME}',
								'${fileList.TRST_ORGN_CODE}',
								'${fileList.FILE_SIZE}'
								);
			</c:forEach>
		</c:if>		
	}
//-->
</script>
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<form name="srchForm" action="">
<input type="hidden" name="srchWorkName" value="${srchParam.srchWorkName }"/>
<input type="hidden" name="srchLishComp" value="${srchParam.srchLishComp }"/>
<input type="hidden" name="srchCoptHodr" value="${srchParam.srchCoptHodr }"/>
<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
</form>
</body>
</html>
