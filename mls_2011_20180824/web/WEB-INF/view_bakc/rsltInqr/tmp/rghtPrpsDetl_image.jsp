<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(저작권찾기) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<style type="text/css">
<!--
body {
	overflow-x: auto;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/general.js"></script>	
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript"><!--


window.name = "rghtPrpsDetl_image";

// 이미지저작물 상세 팝업오픈
function openImageDetail( workFileNm, workNm ) {

	var param = '';
	
	param = '&WORK_FILE_NAME='+workFileNm;
	param += '&WORK_NAME='+workNm;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=I'+param
	var name = '';
	var openInfo = 'target=rghtPrpsDetl_image width=320, height=310';
	
	window.open(url, name, openInfo);
}

// 처리결과 상세 팝업오픈
function openDetlDesc(mastKey, code, crId, nrId, meId, trstCode)  {
	var param = '';
	
	param += '&PRPS_MAST_KEY='+mastKey;
	param += '&PRPS_IDNT_CODE='+code;
	param += '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	param += '&TRST_ORGN_CODE='+trstCode;
	
	var url = '/rsltInqr/rsltInqr.do?method=rghtPrpsRsltDetl&DIVS=I'+param
	var name = '';
	var openInfo = 'target=rghtPrpsDetl_book width=500, height=330';	
	window.open(url, name, openInfo);
}
// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	
	
	var the_height = document.getElementById(name).offsetHeight;
	var chkObjs = document.getElementsByName("chk");
	  
	document.getElementById(targetName).style.height = the_height + 13 + "px" ;
	  

}

// 스크롤셋팅
function scrollSet(name, targetName, oRowName){
	
	var totalRow = document.getElementsByName(oRowName).length;
		
	// 세로 기준건수 이상인 경우 세로스크롤 제어
	if( totalRow < 6) {
		resizeDiv(name, targetName);
		document.getElementById(targetName).style.overflowY = "hidden";
	}
	
	if( totalRow > 5 ) {
		document.getElementById(targetName).style.overflowY = "auto";
	}
}


// 토탈건수 set
function setTotCnt( oRowName, oSpanId) {

	var totalRow = document.getElementsByName(oRowName).length;
	document.getElementById(oSpanId).innerHTML = "("+totalRow+"건)";
}

// 목록 
function goList(){
	
	var frm = document.srchForm;
	frm.action = '/rsltInqr/rsltInqr.do?DIVS=10&page_no=1';
	frm.method="post";
	frm.submit();
}

// 수정화면으로 이동
function goModi(prpsMastKey, dealStatFlag){
	
	if(dealStatFlag!='0')
	{
		alert("상세처리상태가 '신청'이상 진행된경우는 수정이 불가능합니다."); return;
	}
	
	// 수정가능 URL
	url = "/rsltInqr/rsltInqr.do?method=isValidDealStat&actFlagYn=Y&prpsMastKey=" + prpsMastKey;
	
	if(cfGetBooleanResponseReload(url) == false)
	{
		alert("상세처리상태가 '신청'이상 진행된경우는 수정이 불가능합니다.");
		return;
	}
	else
	{
		var frm = document.prpsForm;
		
		frm.PRPS_MAST_KEY.value = prpsMastKey;
		
		frm.method = "post";
		frm.action = "/rsltInqr/rsltInqr.do?method=rghtPrpsModi&DIVS=I";
		frm.submit();		
	}
}

// 삭제
function goDelt(prpsMastKey, dealStatFlag){
	
	if(dealStatFlag!='0')
	{
		alert("상세처리상태가 '신청'이상 진행된경우는 삭제가 불가능합니다."); return;
	}
	
	if( confirm("삭제 하시겠습니까?") )
	{
		// 삭제가능 URL
		url = "/rsltInqr/rsltInqr.do?method=isValidDealStat&actFlagYn=Y&prpsMastKey=" + prpsMastKey;
		
		if(cfGetBooleanResponseReload(url) == false)
		{
			alert("상세처리상태가 '신청'이상 진행된경우는 삭제가 불가능합니다.");
			return;
		}
		else
		{
			var frm = document.prpsForm;
			
			frm.PRPS_MAST_KEY.value = prpsMastKey;
			
			frm.method = "post";
			frm.action = "/rsltInqr/rsltInqr.do?method=deleteRslt&DIVS=10&page_no=1";
			frm.submit();		
		}
	}
}

// 파일다운로드
	function fn_fileDownLoad(filePath, fileName, realFileName) {
		
		var frm = document.prpsForm;
		
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.target="rghtPrpsDetl_image";
		//frm.action = "/rsltInqr/rsltInqr.do?method=fileDownLoad";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.method="post";
		frm.submit();
  }

// 출력물 
function fn_report(mastKey, report){

	var frm = document.prpsForm;
	//frm.mode.value = 'R';
	
	var sUrl = "/rsltInqr/rsltInqr.do?method=rghtPrpsDetl";

	var param = "&DIVS=I";
	     param += "&PRPS_MAST_KEY="+mastKey;
	     param += "&mode=R";
	     param += "&report="+report;
	 
	sUrl += param;
	window.open(sUrl,"PRINT","toolbar=no,menubar=no,resizable=no,status=yes");
}
 
function initParameter(){
 	// 토탈건수
 	setTotCnt( "iChk", "totalRow");
	// scrollSet("listTab", "div_scroll_1", "iChk");
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//
--></script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_3.png" alt="신청현황조회" title="신청현황조회" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li class="active"><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1">저작권찾기<span>&lt;</span></a></li>
							<li><a href="/rsltInqr/rsltInqr.do?DIVS=20&amp;page_no=1">보상금<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>신청현황조회</span>&gt;<strong>저작권찾기</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>저작권찾기 신청현황조회
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>

					<!-- memo 삽입 -->
						<jsp:include page="/common/memo/memo_01.jsp">
							<jsp:param name="DIVS" value="${DIVS}"/>
						</jsp:include>
					<!-- // memo 삽입 -->
															
					<div class="bbsSection">
						<h4>이미지저작권찾기 신청결과 상세</h4>
						<form name="prpsForm" action="">
							<input type="hidden" name="PRPS_MAST_KEY" />
							<!-- 파일다운로드 -->
							<input type="hidden" name="filePath" />
							<input type="hidden" name="fileName" />
							<input type="hidden" name="realFileName" />
							
							<fieldset>
								<legend></legend>
								<!-- 테이블 영역입니다 -->
								<div class="tabelRound">
									<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="저작권찾기 신청정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="10%">
										<col width="10%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row" rowspan="2" class="bgbr lft">신청정보</th>
												<th scope="row" class="bgbr2 lft">신청목적</th>
												<td>${rghtPrps.PRPS_RGHT_CODE_VALUE}</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr2 lft">신청인 정보</th>
												<td>
													<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
													<colgroup>
														<col width="15%">
														<col width="*">
														<col width="20%">
														<col width="30%">
														</colgroup>
														<tbody>
															<tr>
																<th class="bgbr3 lft">성명</th>
																<td>${rghtPrps.USER_NAME}</td>
																<th class="bgbr3 lft">주민번호/사업자번호</th>
																<td>${rghtPrps.RESD_CORP_NUMB_VIEW}</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">전화</th>
																<td>
																	<label class="labelBlock"><em class="w45">자택</em>${rghtPrps.HOME_TELX_NUMB}</label>
																	<label class="labelBlock"><em class="w45">사무실</em>${rghtPrps.BUSI_TELX_NUMB}</label>
																	<label class="labelBlock"><em class="w45">휴대폰</em>${rghtPrps.MOBL_PHON}</label>
																</td>
																<th class="bgbr3 lft">Fax</th>
																<td>${rghtPrps.FAXX_NUMB}</td>
															</tr>
															<tr>
																<th class="bgbr3 lft"><label>이메일주소</label></th>
																<td colspan="3">${rghtPrps.MAIL}</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">주소</th>
																<td colspan="3">
																	<label class="labelBlock"><em class="w45">자택</em>${rghtPrps.HOME_ADDR}</label>
																	<label class="labelBlock"><em class="w45">사무실</em>${rghtPrps.BUSI_ADDR}</label>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">권리구분</th>
												<td>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_204" value="204" class="inputRChk" <c:if test="${rghtPrps.CHK_204 == '1'}">checked="checked"</c:if>  onclick="return(false);" title="미술,사진 작가 - 저작권자(한국문예학술저작권협회)" />미술,사진 작가 - 저작권자(한국문예학술저작권협회)</label><br/>
													<!-- 
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_216" value="216" class="inputRChk" <c:if test="${rghtPrps.CHK_216 == '1'}">checked="checked"</c:if>  onclick="return(false);" />미술,사진 저작권자(한국문화콘텐츠진흥원)</label><br/>
													 -->
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_205" value="205" class="inputRChk" <c:if test="${rghtPrps.CHK_205 == '1'}">checked="checked"</c:if> onclick="return(false);" title="미술,사진 작가 - 복사,전송권자(한국복사전송권협회)" />미술,사진 작가 - 복사,전송권자(한국복사전송권협회)</label>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">신청저작물정보<span id="totalRow"></span></th>
												<td>	
													
													<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1' || rghtPrps.PRPS_RGHT_CODE == '2'}">
													
													<!-- 테이블 영역입니다 -->
													<div id="div_1" class="tabelRound" style="width:583px; padding:0 0 0 0;"> 
														<div id="div_scroll_1" class="tabelRound div_scroll" style="width:583px; padding:0 0 0 0;">
														<table id="listTab" cellspacing="0" cellpadding="0" border="1" class="grid" summary="권리자 저작권찾기 신청저작물정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
																<col width="10">
																<col width="80">
																<col width="80">
																<col width="80">
																<col width="30">
																<col width="50">
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH">이미지명</th>
																	<th scope="col" class="headH">분야</th>
																	<th scope="col" class="headH">공표매체</th>
																	<th scope="col" class="headH">공표일자</th>
																	<th scope="col" class="headH">저작권자</th>
																</tr>
															</thead>
															<tbody>
															<c:if test="${!empty workList}">
																<c:forEach items="${workList}" var="workList">
																	<c:set var="NO" value="${workList.totalRow}"/>
																	<c:set var="i" value="${i+1}"/>
																<tr>
																	<td class="ce">${i}</td>
																	<td>
																		<c:if test="${workList.PRPS_IDNT_CODE == '1'}">
																		<a href="javascript:openImageDetail('${workList.WORK_FILE_NAME }','${workList.WORK_NAME }');"><u>${workList.WORK_NAME }</u></a>
																		</c:if>
																		<c:if test="${workList.PRPS_IDNT_CODE != '1'}">
																		[${workList.PRPS_IDNT_CODE_VALUE }]${workList.WORK_NAME }
																		</c:if>
																	</td>
																	<td>${workList.IMAGE_DIVS }</td>
																	<td>
																		<c:choose>
																			<c:when test="${workList.OPEN_MEDI_CODE == 1}">출판사명</c:when>
																			<c:when test="${workList.OPEN_MEDI_CODE == 2}">배포대상</c:when>
																			<c:when test="${workList.OPEN_MEDI_CODE == 3}">인터넷주소</c:when>
																			<c:when test="${workList.OPEN_MEDI_CODE == 4}">공연장소</c:when>
																			<c:when test="${workList.OPEN_MEDI_CODE == 5}">방송사 및 프로그램명</c:when>
																			<c:when test="${workList.OPEN_MEDI_CODE == 6}">기타</c:when>
																		</c:choose>
																		<c:if test="${workList.OPEN_MEDI_TEXT != null}">/</c:if>${workList.OPEN_MEDI_TEXT }</td>
																	<td class="ce">${workList.OPEN_DATE }</td>
																	<td class="ce">${workList.COPT_HODR }
																		<input type="hidden" name="iChk" />
																	</td>
																</tr>
																
																</c:forEach>
															</c:if>
															</tbody>
														</table>
														</div>
													</div>
													<!-- //테이블 영역입니다 -->
													</c:if>

													<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3' || rghtPrps.PRPS_RGHT_CODE == null}">													
													<!-- 테이블 영역입니다 -->
													<div id="div_1" class="tabelRound mt10" style="width:583px;"> 
														<div id="div_scroll_1" class="tabelRound div_scroll" style="width:583px; padding:0 0 0 0;">
														<table id="listTab" cellspacing="0" cellpadding="0" border="1" class="grid" summary="이용자 저작권조회 신청저작물정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
																<col width="10">
																<col width="80">
																<col width="80">
																<col width="50">
																<col width="30">
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH">이미지명</th>
																	<th scope="col" class="headH">분야</th>
																	<th scope="col" class="headH">공표매체</th>
																	<th scope="col" class="headH">공표일자</th>
																</tr>
															</thead>
															<tbody>
																<c:if test="${!empty workList}">
																<c:forEach items="${workList}" var="workList">
																	<c:set var="NO" value="${workList.totalRow}"/>
																	<c:set var="i" value="${i+1}"/>
																<tr>
																	<td class="ce">${i}</td>
																	<td>
																		<c:if test="${workList.PRPS_IDNT_CODE == '1'}">
																		<a href="javascript:openImageDetail('${workList.WORK_FILE_NAME }','${workList.WORK_NAME}');"><u>${workList.WORK_NAME }</u></a>
																		</c:if>
																		<c:if test="${workList.PRPS_IDNT_CODE != '1'}">
																		[${workList.PRPS_IDNT_CODE_VALUE }]${workList.WORK_NAME }
																		</c:if>
																	</td>
																	<td>${workList.IMAGE_DIVS }</td>
																	<td>
																		<c:choose>
																			<c:when test="${workList.OPEN_MEDI_CODE == 1}">출판사명</c:when>
																			<c:when test="${workList.OPEN_MEDI_CODE == 2}">배포대상</c:when>
																			<c:when test="${workList.OPEN_MEDI_CODE == 3}">인터넷주소</c:when>
																			<c:when test="${workList.OPEN_MEDI_CODE == 4}">공연장소</c:when>
																			<c:when test="${workList.OPEN_MEDI_CODE == 5}">방송사 및 프로그램명</c:when>
																			<c:when test="${workList.OPEN_MEDI_CODE == 6}">기타</c:when>
																		</c:choose>
																		<c:if test="${workList.OPEN_MEDI_TEXT != null}">/</c:if>${workList.OPEN_MEDI_TEXT }
																	</td>
																	<td class="ce">${workList.OPEN_DATE }
																		<input type="hidden" name="iChk" />
																	</td>
																</tr>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
														</div>
													</div>
													<!-- //테이블 영역입니다 -->
													</c:if>
													
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">신청내용</th>
												<td>
													<textarea rows="7" cols="10" readonly="readonly">${rghtPrps.PRPS_DESC}</textarea>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">첨부파일<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">(증빙서류)</c:if></th>
												<td>
													<c:if test="${!empty fileList}">
														<c:forEach items="${fileList}" var="fileList">
															<c:set var="NO" value="${fileList.totalRow}"/>
															<c:set var="k" value="${k+1}"/>
														<span class="block">${k}. <a href="javascript:fn_fileDownLoad('${fileList.FILE_PATH}','${fileList.FILE_NAME}','${fileList.REAL_FILE_NAME}');" class="blue underline">${fileList.FILE_NAME}</a></span>
														</c:forEach>
													</c:if>
												</td>
											</tr>
											<!-- 2010.08.16 : 정확한 데이터입력이 어려움으로 인한 기능제거 (from 관련기관회의) START -->
											<!-- 
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">이용허락 저작재산권</th>
												<td>
													<label class="label_inBlock w20"><input type="checkbox" />복제권</label>
													<label class="label_inBlock w20"><input type="checkbox" />배포권</label>
													<label class="label_inBlock w20"><input type="checkbox" />대여권</label>
													<label class="label_inBlock w20"><input type="checkbox" />공연권</label><br />
													
													<label class="label_inBlock"><input type="checkbox" />공중송신권</label> (<label class="label_inBlock"><input type="checkbox" />방송권</label><label class="label_inBlock ml5"><input type="checkbox" />전송권</label><label class="label_inBlock ml5"><input type="checkbox" />디지털음성송신권</label>)<br />
													
													<label class="label_inBlock w20"><input type="checkbox" />전시권</label>
													<label class="label_inBlock w30"><input type="checkbox" />2차적저작물작성권</label>
												</td>
											</tr>
											 -->
											<!-- 2010.08.16 : 정확한 데이터입력이 어려움으로 인한 기능제거 (from 관련기관회의) END -->
										</tbody>
									</table>
								</div>
								<!-- //테이블 영역입니다 -->
							</fieldset>
						</form>
					</div>
					
					<div class="floatDiv">
						<p class="fr">
							<span class="button small type2 icon"><span class="print">&nbsp;</span><a href="javascript:fn_report('${rghtPrps.PRPS_MAST_KEY}', 'report/rghtPrps_imge');">신청서 출력</a></span> 
						</p>
					</div>
					
					<div class="bbsSection">
						<h4>저작권찾기 신청처리 결과</h4>
						<!-- 테이블 영역입니다 -->
						<div class="tabelRound">
							<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="저작권찾기 신청정보 처리결과의 저작물명, 신착관리단체, 처리상태 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="35%">
								<col width="35%">
								<col width="*">
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="headH">저작물명</th>
										<th scope="col" class="headH">신탁관리단체</th>
										<th scope="col" class="headH">처리상태</th>
									</tr>
								</thead>
								<tbody>
									
								<c:if test="${!empty rsltList}">
									<c:forEach items="${rsltList}" var="rsltList">
										<c:set var="kk" value="${kk+1}"/>
										<c:set var="NO" value="${rsltList.totalRow}"/>
										
									<tr>
										<c:if test="${rsltList.TITLE!=null}">
										<th rowspan="${rsltList.CNT}" class="bgbr lft ce">${rsltList.TITLE }</th>
										</c:if>
										<td class="ce">${rsltList.TRST_ORGN_CODE_VALUE }</td>
										<td class="ce">
											<c:if test="${rsltList.DEAL_STAT == '4'}">
											<span class="orange" style="cursor:hand" onclick="javascript:openDetlDesc('${rsltList.PRPS_MAST_KEY }', '${rsltList.PRPS_IDNT_CODE }', '${rsltList.PRPS_IDNT }', '${rsltList.PRPS_IDNT_NR }', '${rsltList.PRPS_IDNT_ME }', '${rsltList.TRST_ORGN_CODE }');"><u>${rsltList.DEAL_STAT_VALUE}</u></span>
											</c:if>
											<c:if test="${rsltList.DEAL_STAT != '4'}">
											<span class="blue">${rsltList.DEAL_STAT_VALUE}</span>
											</c:if>
										</td>
									</tr>
									
									</c:forEach>
								</c:if>
								
								</tbody>
							</table>
						</div>
						<!-- //테이블 영역입니다 -->
					</div>
					
					<div class="floatDiv mt10">
						<p <c:if test="${rghtPrps.DEAL_STAT_FLAG == 0}"> class="fl" </c:if> 
							<c:if test="${rghtPrps.DEAL_STAT_FLAG != 0}">  class="fr" </c:if> ><span class="button medium icon"><span class="list"></span><a href="javascript:goList()">목록</a></span>
						</p>
						<c:if test="${rghtPrps.DEAL_STAT_FLAG == 0}">
						<p class="fr">
							<span class="button medium icon"><span class="default"></span><a href="javascript:goModi('${rghtPrps.PRPS_MAST_KEY}','${rghtPrps.DEAL_STAT_FLAG}');">저작권찾기 수정</a></span> 
							<span class="button medium icon"><span class="del"></span><button type="button" onclick="javascript:goDelt('${rghtPrps.PRPS_MAST_KEY}','${rghtPrps.DEAL_STAT_FLAG}');">저작권찾기 삭제</button></span>
						</p>
						</c:if>
					</div>
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


<form name="srchForm" action="">
<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
<input type="hidden" name="srchPrpsDivs" value="${srchParam.srchPrpsDivs }"/>
<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
</form>
</body>
</html>
