<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(보상금) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2010/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/JavaScript">
<!--
	//목록으로 이동
	function fn_list(){
		var frm = document.frm;

		frm.DIVS.value = '20';
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/rsltInqr/rsltInqr.do?method=list&page_no=1";
		frm.submit();
	}
	
	//수정화면으로 이동
	function fn_update(){
		var frm = document.frm;
		var count = 0;
	
		var oDealStat = document.getElementsByName("hddnDealStat");
		for(i = 0; i < oDealStat.length; i++){
			if(oDealStat[i].value > 1)	count++;
		}
		
		if(count > 0){
			alert('수정가능한 상태가 아닙니다.');
			return;
		}else{
			frm.mode.value = 'U';
			frm.method = "post";
			frm.action = "/rsltInqr/rsltInqr.do?method=inmtPrpsDetl&page_no="+'${srchParam.nowPage }';
			frm.submit();
		}
	}
	
	// 출력물 
	function fn_report(report){
		var frm = document.frm;
		frm.mode.value = 'R';
		
		var sUrl = "/rsltInqr/rsltInqr.do?method=inmtPrpsDetl";
		var param = "&DIVS="+frm.DIVS.value;
		     param += "&PRPS_MAST_KEY="+frm.PRPS_MAST_KEY.value;
		     param += "&PRPS_SEQN="+frm.PRPS_SEQN.value;
		     param += "&mode=R";
		     param += "&report="+report;
		 
		sUrl += param;
		window.open(sUrl,"PRINT","toolbar=no,menubar=no,resizable=no,status=yes");
	}
	
	//보상금신청 취소
	function fn_delete(){
	
		var frm = document.frm;
		var count = 0;
	
		var oDealStat = document.getElementsByName("hddnDealStat");
		for(i = 0; i < oDealStat.length; i++){
			if(oDealStat[i].value > 1)	count++;
		}
		
		if(count > 0){
			alert('삭제가능한 상태가 아닙니다.');
			return;
		}else{
			if(confirm('삭제 하시겠습니까?')){ 
				frm.actFlagYn.value = 'Y';
				
				frm.method = "post";
				frm.action = "/rsltInqr/rsltInqr.do?method=deleteRsltInqr&DIVS=20&page_no=1";
				frm.submit();
			}
		}
		
	}

	var openPopUpFlag;

	// 처리결과 상세 팝업오픈
	function openDetlDesc(prpsSeqn, trstCode, prpsIdnt)  {
		var param = '';
		
		param += '&PRPS_SEQN='+prpsSeqn;
		param += '&TRST_ORGN_CODE='+trstCode;
		param += '&PRPS_IDNT='+prpsIdnt;
		
		var url = '/rsltInqr/rsltInqr.do?method=rghtPrpsRsltDetl'+param
		var name = '';
		var openInfo = 'target=inmtPrpsDetl_musc, width=500, height=300, scrollbars=yes';	
		
		//열린팝업창 존재 유무체크
		if(openPopUpFlag) {
			if(!openPopUpFlag.closed) openPopUpFlag.window.close();
		}
		openPopUpFlag = window.open(url, name, openInfo);
	}

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.frm;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.method = "post";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
	}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container"  style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
			<div class="container_vis"  style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_6.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
				<!-- 래프 -->
				<jsp:include page="/include/2012/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1","lnb12");
				</script>
				<!-- //래프 -->
			
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>신청현황</span><em>미분배보상금 신청</em></p>
					<h1><img src="/images/2012/title/content_h1_0602.gif" alt="미분배보상금 신청" title="미분배보상금 신청" /></h1>
					<form name="frm" action="#" class="sch">
						<input type="submit" style="display:none;">
						<input type="hidden" name="DIVS"			value="${srchParam.DIVS }"/>
						<input type="hidden" name="PRPS_MAST_KEY"	value="${srchParam.PRPS_MAST_KEY }"/>
						<input type="hidden" name="PRPS_SEQN"		value="${srchParam.PRPS_SEQN }"/>
						<input type="hidden" name="mode" />
						
						<input type="hidden" name="srchPrpsDivs"	value="${srchParam.srchPrpsDivs }"/>
						<input type="hidden" name="srchStartDate"	value="${srchParam.srchStartDate }"/>
						<input type="hidden" name="srchEndDate"		value="${srchParam.srchEndDate }"/>
						<input type="hidden" name="srchTitle"		value="${srchParam.srchTitle }"/>
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }"/>
						
						<input type="hidden" name="actFlagYn" />
						<input type ="hidden" name="prpsMastKey" value="${srchParam.PRPS_MAST_KEY}" />
						
						<input type="hidden" name="filePath">
						<input type="hidden" name="fileName">
						<input type="hidden" name="realFileName">
					<div class="section">
						<!-- memo 삽입 -->
						<jsp:include page="/common/memo/2011/memo_01.jsp">
							<jsp:param name="DIVS" value="${srchParam.DIVS}"/>
						</jsp:include>
						<!-- // memo 삽입 -->
						<!-- article str -->
						<div class="article mt20">
							<div class="floatDiv">
								<h2 class="fl">도서관 미분배보상금 신청결과 상세</h2>
							</div>
							
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="보상금 신청정보입니다.">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">보상금종류</th>
										<td>도서관</td>
									</tr>
									<tr>
										<th scope="row">신청인정보</th>
										<td>
											<span class="topLine2"></span>
											<!-- 그리드스타일 -->
											<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등 정보입니다.">
												<colgroup>
												<col width="15%">
												<col width="30%">
												<col width="25%">
												<col width="*">
												</colgroup>
												<tbody>
													<tr>
														<th scope="row">성명</th>
														<td>${clientInfo.PRPS_NAME }</td>
														<th scope="row">주민번호/사업자번호</th>
														<td>${clientInfo.RESD_CORP_NUMB_VIEW }</td>
													</tr>
													<tr>
														<th scope="row">전화번호</th>
														<td>
															<ul class="list1">
															<li class="p11">자택 : ${clientInfo.HOME_TELX_NUMB }</li>
															<li class="p11">사무실 : ${clientInfo.BUSI_TELX_NUMB }</li>
															<li class="p11">휴대폰 : ${clientInfo.MOBL_PHON }</li>
															</ul>
														</td>
														<th scope="row">팩스번호</th>
														<td>${clientInfo.FAXX_NUMB }</td>
													</tr>
													<tr>
														<th scope="row">이메일주소</th>
														<td>${clientInfo.MAIL }</td>
														<th scope="row">입금처</th>
														<td>
															<ul class="list1">
															<li class="p11">은행명 : ${clientInfo.BANK_NAME}</li>
															<li class="p11">계좌번호 : ${clientInfo.ACCT_NUMB }</li>
															<li class="p11">예금주 : ${clientInfo.DPTR }</li>
															</ul>
														</td>
													</tr>
													<tr>
														<th scope="row">주소</th>
														<td colspan="3">
															<ul class="list1">
															<li class="p11">자택 : ${clientInfo.HOME_ADDR }</li>
															<li class="p11">사무실 : ${clientInfo.BUSI_ADDR }</li>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
											<!-- //그리드스타일 -->
										</td>
									</tr>
									<tr>
										<th scope="row">신청분류</th>
										<td>
											<ul class="line22">
											<li><input type="checkbox" id="rght1" <c:if test="${clientInfo.TRST_205_2 == '1'}">checked</c:if> disabled class="inputRChk" title="교과용 보상금"/><label for="rght1" class="p12">교과용 보상금</label></li>
											</ul>
										</td>
									</tr>
									<tr>
										<th scope="row">신청저작물</th>
										<td>
											<table cellspacing="0" cellpadding="0" border="1" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
												<colgroup>
												<col width="10%">
												<col width="*">
												<col width="20%">
												<col width="20%">
												</colgroup>
												<thead>
													<tr>
														<th scope="col">순번</th>
														<th scope="col">도서명</th>
														<th scope="col">저자</th>
														<th scope="col">사용연도</th>
													</tr>
												</thead>
												<tbody>
												<c:if test="${!empty tempList}">
													<c:forEach items="${tempList}" var="tempList">	
														<c:set var="NO" value="${tempListCnt + 1}"/>
														<c:set var="i" value="${i+1}"/>
													<tr>
														<td class="ce">${NO - i }</td>
														<td>${tempList.SDSR_NAME }</td>
														<td class="ce">${tempList.MUCI_NAME }</td>
														<td class="ce">${tempList.YYMM }</td>
													</tr>
													</c:forEach>
												</c:if>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
				<!-- //그리드스타일 -->
				<c:if test="${!empty trstList}">
					<c:forEach items="${trstList}" var="trstList">
						<c:if test="${trstList.TRST_ORGN_CODE=='205_2' }">
		                    <!-- 복전협 관련 영역 -->
							<div class="floatDiv mt20">
								<h3 class="fl">내용(한국복사전송권협회) 도서관 보상금<span class="ml10"><input type="checkbox" id="off_1" <c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if> disabled title="오프라인접수(첨부서류)"/><label for="off_1" class="orange thin">오프라인접수</label></span></h3>
								<p class="fr">
								<span class="button small icon"><a href="#1" onclick="javascript:fn_report('report/inmtPrps_205_2');">신청서 출력</a><span class="print" onclick="javascript:fn_report('report/inmtPrps_205_2');"></span></span>
								<span class="button small icon"><a href="#1" onclick="javascript:fn_report('report/inmtPrps_205_2M');">명세표 출력</a><span class="print" onclick="javascript:fn_report('report/inmtPrps_205_2M');"></span></span>
								</p>
							</div>
							
							<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col width="30%">
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="col">권리자</th>
										<td>
											<ul class="list1">
											<li>본명 : ${pemrRlnm }</li>
											<li>필명 : ${pemrStnm }</li>
											<li>예명 : ${grupName }</li>
											</ul>
										</td>
										<th scope="col">주민등록번호</th>
										<td>
											<%-- <input type="text" name="txtFokapoResdNumb_1" value="${resdNumb_1}" title="권리자(주민번호)" class="whiteR w20"  readonly="readonly" /> --%>
											${resdNumb_1}
											 - *******
											<%-- <input type="password" name="txtFokapoResdNumb_2" value="${resdNumb_2}" title="권리자(주민번호)" class="whiteL w38" readonly="readonly" /> --%>
										</td>
									</tr>
								</tbody>
							</table>
							
							<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="6%">
								<col width="10%">
								<col width="*">
								<col width="13%">
								<col width="15%">
								<col width="10%">
								<col width="10%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">처리상태</th>
										<th scope="col">서명(논문명)</th>
										<th scope="col">(공동)저자명</th>
										<th scope="col">출판사</th>
										<th scope="col">발행년도</th>
										<th scope="col">이용형태</th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${!empty krtraList_2}">
									<c:forEach items="${krtraList_2}" var="krtraList_2">	
										<c:set var="krtraNo" value="${krtraNo+1}"/>
									<tr>
										<td scope="col" class="ce">${krtraNo }</td>
										<td class="ce">
											<c:choose>
												<c:when test="${krtraList_2.DEAL_STAT=='1' }">신청</c:when>
												<c:when test="${krtraList_2.DEAL_STAT=='2' }">접수</c:when>
												<c:when test="${krtraList_2.DEAL_STAT=='3' }">처리중</c:when>
												<c:when test="${krtraList_2.DEAL_STAT=='4' }"><a href="#1" onclick="javascript:openDetlDesc('${krtraList_2.PRPS_SEQN }','${krtraList_2.ORGN_TRST_CODE }','${krtraList_2.PRPS_IDNT }');"><span class="blue underline">완료</span></a></c:when>
											</c:choose>
										</td>
										<td>${krtraList_2.WORK_NAME }</td>
										<td class="ce">${krtraList_2.WRTR_NAME }</td>
										<td class="ce">${krtraList_2.BOOK_CNCN }</td>
										<td class="ce">${krtraList_2.SCTR }</td>
										<td class="ce">${krtraList_2.USEX_TYPE }</td>
									</tr>
									</c:forEach>
								</c:if>
								</tbody>
							</table>
							
							<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
							<table id="divFokapoFileList" cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="보상금신청 첨부파일정보 입니다." style="display:<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>;"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="col">첨부파일</th>
										<td>
											<c:if test="${!empty fileList}">
											<c:set var="fileCnt" value="0" />
												<c:forEach items="${fileList}" var="fileList">
													<c:set var="fileCnt" value="${fileCnt+1}" />
													<a href="#1" onclick="javascript:fn_fileDownLoad('${fileList.FILE_PATH}','${fileList.FILE_NAME}','${fileList.REAL_FILE_NAME}')">${fileList.FILE_NAME}</a><br>
												</c:forEach>
											</c:if>
											<c:if test="${fileCnt < 1}">
												파일이 없습니다.
											</c:if>
											<c:if test="${empty fileList}">
												파일이 없습니다.
											</c:if>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
						</c:if>
					</c:forEach>
				</c:if>
							<div class="mt20">
								<span class="topLine"></span>
								<table cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
									<colgroup>
									<col width="20%">
									<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="col">내용</th>
											<td>
												<% pageContext.setAttribute("line", "\n"); %>
 												${fn:replace(clientInfo.PRPS_DESC, line, '<br/>')}
											</td>
										</tr>
										<tr>
											<th scope="col">처리상태</th>
											<td><strong class="orange">${totDealStat }</strong></td>
										</tr>
									</tbody>
								</table>
							</div>
							
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_list();">목록</a></span></p>
								<c:if test="${clientInfo.DEAL_STAT_FLAG == 0}">
									<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_update();">수정하기</a></span> <span class="button medium"><a href="#1" onclick="javascript:fn_delete();">삭제하기</a></span></p>
								</c:if>
							</div>
						</div>
						<!-- //article end -->
					</div>
					</form>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
			<!--  //content end -->
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->		
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
