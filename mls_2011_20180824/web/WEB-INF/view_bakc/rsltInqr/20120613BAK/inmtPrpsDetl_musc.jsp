<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(보상금) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2010/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/JavaScript">
<!--
	//목록으로 이동
	function fn_list(){
		var frm = document.frm;

		frm.DIVS.value = '20';
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/rsltInqr/rsltInqr.do?method=list&page_no=1";
		frm.submit();
	}
	
	//수정화면으로 이동
	function fn_update(){
		var frm = document.frm;
		var count = 0;
	
		var oDealStat = document.getElementsByName("hddnDealStat");
		for(i = 0; i < oDealStat.length; i++){
			if(oDealStat[i].value > 1)	count++;
		}
		
		if(count > 0){
			alert('수정가능한 상태가 아닙니다.');
			return;
		}else{
			frm.mode.value = 'U';
			frm.method = "post";
			frm.action = "/rsltInqr/rsltInqr.do?method=inmtPrpsDetl&page_no="+'${srchParam.nowPage }';
			frm.submit();
		}
	}
	
	// 출력물 
	function fn_report(report){
		var frm = document.frm;
		frm.mode.value = 'R';
		
		var sUrl = "/rsltInqr/rsltInqr.do?method=inmtPrpsDetl";
		var param = "&DIVS="+frm.DIVS.value;
		     param += "&PRPS_MAST_KEY="+frm.PRPS_MAST_KEY.value;
		     param += "&PRPS_SEQN="+frm.PRPS_SEQN.value;
		     param += "&mode=R";
		     param += "&report="+report;
		 
		sUrl += param;
		window.open(sUrl,"PRINT","toolbar=no,menubar=no,resizable=no,status=yes");
	}
	
	// 삭제
	function fn_delete(){
	
		var frm = document.frm;
		var count = 0;
	
		var oDealStat = document.getElementsByName("hddnDealStat");
		for(i = 0; i < oDealStat.length; i++){
			if(oDealStat[i].value > 1)	count++;
		}
		
		if(count > 0){
			alert('삭제가능한 상태가 아닙니다.');
			return;
		}else{
			if(confirm('삭제 하시겠습니까?')){ 
				frm.actFlagYn.value = 'Y';
				
				frm.method = "post";
				frm.action = "/rsltInqr/rsltInqr.do?method=deleteRsltInqr&DIVS=20&page_no=1";
				frm.submit();
			}
		}
		
	}

	var openPopUpFlag;

	// 처리결과 상세 팝업오픈
	function openDetlDesc(prpsSeqn, trstCode, prpsIdnt)  {
		var param = '';
		
		param += '&PRPS_SEQN='+prpsSeqn;
		param += '&TRST_ORGN_CODE='+trstCode;
		param += '&PRPS_IDNT='+prpsIdnt;
		
		var url = '/rsltInqr/rsltInqr.do?method=rghtPrpsRsltDetl'+param
		var name = '';
		var openInfo = 'target=inmtPrpsDetl_musc width=500, height=300, scrollbars=yes';	
		
		//열린팝업창 존재 유무체크
		if(openPopUpFlag) {
			if(!openPopUpFlag.closed) openPopUpFlag.window.close();
		}
		openPopUpFlag = window.open(url, name, openInfo);
	}
	
	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.frm;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.method = "post";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
	}

	// table name 사이즈에 대한 div targetName리사이즈
	//function resizeDiv(name) {
	//	var the_height = document.getElementById(name).height;
	//	document.getElementById(name).style.height = the_height + 13  + "px" ;
	//}
	
	//table name 사이즈에 대한 div targetName리사이즈
	function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height;
  
}
//-->
</script>
<style type="text/css">
<!--

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}

-->
</style>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_8.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
				<!-- 래프 -->
				<jsp:include page="/include/2011/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1");
				</script>
				<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<form name="frm" action="#" class="sch">
						<input type="hidden" name="DIVS"			value="${srchParam.DIVS }"/>
						<input type="hidden" name="PRPS_MAST_KEY"	value="${srchParam.PRPS_MAST_KEY }"/>
						<input type="hidden" name="PRPS_SEQN"		value="${srchParam.PRPS_SEQN }"/>
						<input type="hidden" name="mode" />
						
						<input type="hidden" name="srchPrpsDivs"	value="${srchParam.srchPrpsDivs }"/>
						<input type="hidden" name="srchStartDate"	value="${srchParam.srchStartDate }"/>
						<input type="hidden" name="srchEndDate"		value="${srchParam.srchEndDate }"/>
						<input type="hidden" name="srchTitle"		value="${srchParam.srchTitle }"/>
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }"/>
						
						<input type="hidden" name="actFlagYn" />
						<input type ="hidden" name="prpsMastKey" value="${srchParam.PRPS_MAST_KEY}" />
			
						<input type="hidden" name="filePath">
						<input type="hidden" name="fileName">
						<input type="hidden" name="realFileName">
						<input type="submit" style="display:none;">
					<p class="path"><span>Home</span><span>마이페이지</span><span>신청현황</span><em>보상금</em></p>
					<h1><img src="/images/2011/title/content_h1_0802.gif" alt="보상금" title="보상금" /></h1>
					<c:choose>
						<c:when test="${srchParam.DIVS eq 'B'}">
							<c:set var="num" value="1" />
						</c:when>
						<c:when test="${srchParam.DIVS eq 'S'}">
							<c:set var="num" value="2" />
						</c:when>
						<c:otherwise>
							<c:set var="num" value="3" />
						</c:otherwise>
					</c:choose>	
					<div class="section">
						<!-- memo 삽입 -->
						<jsp:include page="/common/memo/2011/memo_01.jsp">
							<jsp:param name="DIVS" value="${num}"/>
						</jsp:include>
						<!-- // memo 삽입 -->
						<!-- article str -->
						<div class="article mt20">
							<div class="floatDiv">
								<h2 class="fl">방송음악보상금 신청결과 상세</h2>
							</div>
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">보상금종류</th>
										<td>방송음악</td>
									</tr>
									<tr>
										<th scope="row">신청인정보</th>
										<td>
											<span class="topLine2"></span>
											<!-- 그리드스타일 -->
											<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
												<colgroup>
												<col width="15%">
												<col width="30%">
												<col width="25%">
												<col width="*">
												</colgroup>
												<tbody>
													<tr>
														<th scope="row">성명</th>
														<td>${clientInfo.PRPS_NAME }</td>
														<th scope="row">주민등록번호/사업자번호</th>
														<td>${clientInfo.RESD_CORP_NUMB_VIEW}</td>
													</tr>
													<tr>
														<th scope="row">전화번호</th>
														<td>
															<ul class="list1">
															<li class="p11">자택 : ${clientInfo.HOME_TELX_NUMB }</li>
															<li class="p11">사무실 : ${clientInfo.BUSI_TELX_NUMB }</li>
															<li class="p11">휴대폰 : ${clientInfo.MOBL_PHON }</li>
															</ul>
														</td>
														<th scope="row">팩스번호</th>
														<td>${clientInfo.FAXX_NUMB }</td>
													</tr>
													<tr>
														<th scope="row">이메일주소</th>
														<td>${clientInfo.MAIL }</td>
														<th scope="row">입금처</th>
														<td>
															<ul class="list1">
															<li class="p11">은행명 : ${clientInfo.BANK_NAME}</li>
															<li class="p11">계좌번호 : ${clientInfo.ACCT_NUMB }</li>
															<li class="p11">예금주 : ${clientInfo.DPTR }</li>
															</ul>
														</td>
													</tr>
													<tr>
														<th scope="row">주소</th>
														<td colspan="3">
															<ul class="list1">
															<li class="p11">자택 : ${clientInfo.HOME_ADDR }</li>
															<li class="p11">사무실 : ${clientInfo.BUSI_ADDR }</li>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
											<!-- //그리드스타일 -->
										</td>
									</tr>
									<tr>
										<th scope="row">신청구분</th>
										<td>
											<ul class="line22">
											<li><input type="checkbox" id="rght1" <c:if test="${clientInfo.TRST_203 == '1'}">checked</c:if> disabled class="inputRChk" title="저작인접권(앨범제작)" /><label for="rght1" class="p12">저작권인접권(앨범제작)</label></li>
											<li><input type="checkbox" id="rght2" <c:if test="${clientInfo.TRST_202 == '1'}">checked="checked"</c:if> disabled title="저작인접권(가창, 연주, 지휘 등)" /><label for="rght2" class="p12">저작권인접권(가창, 연주, 지휘 등)</label></li>
											</ul>
										</td>
									</tr>
									<tr>
										<th scope="row">신청저작물정보</th>
										<td>
											<table cellspacing="0" cellpadding="0" border="1" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
												<colgroup>
												<col width="10%">
												<col width="*">
												<col width="20%">
												<col width="20%">
												</colgroup>
												<thead>
													<tr>
														<th scope="col">순번</th>
														<th scope="col">곡명</th>
														<th scope="col">가수</th>
														<th scope="col">방송년도</th>
													</tr>
												</thead>
												<tbody>
													<c:if test="${!empty tempList}">
														<c:forEach items="${tempList}" var="tempList">	
															<c:set var="NO" value="${tempListCnt + 1}"/>
															<c:set var="i" value="${i+1}"/>
														<tr>
															<td class="ce">${NO - i }</td>
															<td>${tempList.SDSR_NAME }</td>
															<td class="ce">${tempList.MUCI_NAME }</td>
															<td class="ce">${tempList.YYMM }</td>
														</tr>
														</c:forEach>
													</c:if>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
				<c:if test="${!empty trstList}">
					<c:forEach items="${trstList}" var="trstList">
						<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
							<!-- 음제협 관련 영역 -->
							<div class="floatDiv mt20">
								<h3 class="fl">내용(한국음원제작자협회)<span class="ml10"><input type="checkbox" id="off_1" <c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if> disabled="disabled" title="오프라인접수" /><label for="off_1" class="orange thin">오프라인접수</label></span></h3>
								<p class="fr">
								<span class="button small icon"><a href="#1" onclick="javascript:fn_report('report/inmtPrps_203');">명세표 출력</a><span class="print"></span></span>
								<span class="button small icon"><a href="#1" onclick="javascript:fn_report('report/inmtPrps_203M');">신청서 출력</a><span class="print"></span></span>
								</p>
							</div>
							<div id="divKappItemList" style="padding:0 0 0 0;"><!-- 따로 추가한 DIV -->
								<table id="tabKappItemList" cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
									<colgroup>
									<col width="6%">
									<col width="10%">
									<col width="*">
									<col width="8%">
									<col width="15%">
									<col width="12%">
									<col width="10%">
									<col width="10%">
									<col width="10%">
									</colgroup>
									<thead>
										<tr>
											<th scope="col" rowspan="2">번호</th>
											<th scope="col" rowspan="2">처리상태</th>
											<th scope="col" rowspan="2">음반명</th>
											<th scope="col" rowspan="2">CD코드</th>
											<th scope="col" rowspan="2">곡명</th>
											<th scope="col">가수명</th>
											<th scope="col">국가명</th>
											<th scope="col">작사가</th>
											<th scope="col" rowspan="2">음악장르<br />(테마코드)</th>
										</tr>
										<tr>
											<th scope="col">발매일</th>
											<th scope="col">권리근거</th>
											<th scope="col">작곡가</th>
										</tr>
									</thead>
									<tbody>
									<c:if test="${!empty kappList}">
										<c:forEach items="${kappList}" var="kappList">	
											<c:set var="kappNo" value="${kappNo+1}"/>
										<tr>
											<td scope="col" class="ce" rowspan="2" >${kappNo }</td>
											<td class="ce" rowspan="2">
												<c:choose>
													<c:when test="${kappList.DEAL_STAT=='1' }">신청</c:when>
													<c:when test="${kappList.DEAL_STAT=='2' }">접수</c:when>
													<c:when test="${kappList.DEAL_STAT=='3' }">처리중</c:when>
													<c:when test="${kappList.DEAL_STAT=='4' }"><a href="#1" onclick="javascript:openDetlDesc('${kappList.PRPS_SEQN }','${kappList.ORGN_TRST_CODE }','${kappList.PRPS_IDNT }');"><span class="blue underline">완료</span></a></c:when>
												</c:choose>
												<input type="hidden" name="hddnDealStat" value="${kappList.DEAL_STAT}" />
											</td>
											<td rowspan="2">${kappList.DISK_NAME }</td>
											<td class="ce" rowspan="2">${kappList.CD_CODE }</td>
											<td rowspan="2">${kappList.SONG_NAME }</td>
											<td class="ce">${kappList.SNER_NAME }</td>
											<td class="ce">${kappList.NATN_NAME }</td>
											<td class="ce">${kappList.LYRI_WRTR }</td>
											<td rowspan="2">${kappList.MUSC_GNRE_STR }</td>
										</tr>
										<tr>
											<td class="ce">${kappList.DATE_ISSU_STR }</td>
											<td class="ce">${kappList.RGHT_GRND }</td>
											<td class="ce">${kappList.COMS_WRTR }</td>
										</tr>
										</c:forEach>
									</c:if>
									</tbody>
								</table>
							</div>
							<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
							<table id="divKappFileList" cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="(한국음원제작자협회)보상금신청 첨부파일 정보입니다." style="display:<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>;"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="col">첨부파일</th>
										<td>
											<c:if test="${!empty fileList}">
											<c:set var="fileCnt" value="0" />
												<c:forEach items="${fileList}" var="fileList">
													<c:if test="${fileList.TRST_ORGN_CODE=='203' }">
													<c:set var="fileCnt" value="${fileCnt+1}" />
													<a href="#1" onclick="javascript:fn_fileDownLoad('${fileList.FILE_PATH}','${fileList.FILE_NAME}','${fileList.REAL_FILE_NAME}')">${fileList.FILE_NAME}</a><br>
													</c:if>
												</c:forEach>
											</c:if>
											<c:if test="${fileCnt < 1}">
												파일이 없습니다.
											</c:if>
											<c:if test="${empty fileList}">
												파일이 없습니다.
											</c:if>
										</td>
									</tr>
								</tbody>
							</table>
						</c:if>
					</c:forEach>
				</c:if>
				<c:if test="${!empty trstList}">
					<c:forEach items="${trstList}" var="trstList">
						<c:if test="${trstList.TRST_ORGN_CODE=='202' }">
							<!-- 실연자 관련 영역 -->
							<div class="floatDiv mt20">
								<h3 class="fl">내용(한국음악실연자연합회)<span class="ml10"><input type="checkbox" id="off_2" <c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if> disabled="disabled" title="오프라인접수" /><label for="off_2" class="orange thin">오프라인접수</label></span></h3>
								<p class="fr"><span class="button small icon"><a href="#1" onclick="javascript:fn_report('report/inmtPrps_202');">신청서 출력</a><span class="print"></span></span></p>
							</div>
							
							<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col width="30%">
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="col">실연자</th>
										<td>
											<ul class="list1">
											<li>본명 : ${pemrRlnm }</li>
											<li>예명 : ${pemrStnm }</li>
											<li>그룹명 : ${grupName }</li>
											</ul>
										</td>
										<th scope="col">주민등록번호</th>
										<td>
											<input type="text" name="txtFokapoResdNumb_1" value="${fn:substring(fokapoList[0].RESD_NUMB, 0, 6)}" title="실연자 주민번호(1번째)" class="whiteR w20" />
											 -
											<input type="password" name="txtFokapoResdNumb_2" value="${fn:substring(fokapoList[0].RESD_NUMB, 6, 13)}" title="실연자 주민번호(2번째)" class="whiteL w38" />
										</td>
									</tr>
								</tbody>
							</table>
							
							<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="">
								<colgroup>
								<col width="6%">
								<col width="10%">
								<col width="*">
								<col width="8%">
								<col width="15%">
								<col width="12%">
								<col width="10%">
								<col width="10%">
								<col width="10%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">처리상태</th>
										<th scope="col">작품명</th>
										<th scope="col">가수명</th>
										<th scope="col">앨범명</th>
										<th scope="col">앨범발매일</th>
										<th scope="col">실연종류</th>
										<th scope="col">실연내용</th>
										<th scope="col">기여율(%)</th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${!empty fokapoList}">
									<c:forEach items="${fokapoList}" var="fokapoList">	
										<c:set var="fokapoNo" value="${fokapoNo+1}"/>
									<tr>
										<td class="ce">${fokapoNo }</td>
										<td class="ce">
											<c:choose>
												<c:when test="${fokapoList.DEAL_STAT=='1' }">신청</c:when>
												<c:when test="${fokapoList.DEAL_STAT=='2' }">접수</c:when>
												<c:when test="${fokapoList.DEAL_STAT=='3' }">처리중</c:when>
												<c:when test="${fokapoList.DEAL_STAT=='4' }"><a href="#1" onclick="javascript:openDetlDesc('${fokapoList.PRPS_SEQN }','${fokapoList.ORGN_TRST_CODE }','${fokapoList.PRPS_IDNT }');"><span class="blue2 underline">완료</span></a></c:when>
											</c:choose>
											<input type="hidden" name="hddnDealStat" value="${fokapoList.DEAL_STAT}" />
										</td>
										<td>${fokapoList.PDTN_NAME }</td>
										<td class="ce">${fokapoList.SNER_NAME }</td>
										<td>${fokapoList.ABUM_NAME }</td>
										<td class="ce">${fokapoList.ABUM_DATE_ISSU_STR }</td>
										<td class="ce">${fokapoList.PEMS_KIND }</td>
										<td class="ce">${fokapoList.PEMS_DESC }</td>
										<td class="rgt">${fokapoList.CTBT_RATE }</td>
									</tr>
									</c:forEach>
								</c:if>
								</tbody>
							</table>
							<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
							<table id="divFokapoFileList" cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="(한국음악실연자연합회)보상금신청 첨부파일 정보 입니다." style="display:<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>;"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="col">첨부파일</th>
										<td>
											<c:if test="${!empty fileList}">
											<c:set var="fileCnt" value="0" />
												<c:forEach items="${fileList}" var="fileList">
													<c:if test="${fileList.TRST_ORGN_CODE=='202' }">
													<c:set var="fileCnt" value="${fileCnt+1}" />
													<a href="#1" onclick="javascript:fn_fileDownLoad('${fileList.FILE_PATH}','${fileList.FILE_NAME}','${fileList.REAL_FILE_NAME}')">${fileList.FILE_NAME}</a><br>
													</c:if>
												</c:forEach>
											</c:if>
											<c:if test="${fileCnt < 1}">
												파일이 없습니다.
											</c:if>
											<c:if test="${empty fileList}">
												파일이 없습니다.
											</c:if>
										</td>
									</tr>
								</tbody>
							</table>
						</c:if>
					</c:forEach>
				</c:if>
							<!-- 보상금신청(내용) 테이블 영역입니다 -->
							<div class="mt20">
								<span class="topLine"></span>
								<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="보상금신청 내용정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
									<colgroup>
									<col width="20%">
									<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="col">내용</th>
											<td>
												${clientInfo.PRPS_DESC }
											</td>
										</tr>
										<tr>
											<th scope="col">처리상태</th>
											<td><strong class="orange">${totDealStat }</strong></td>
										</tr>
									</tbody>
								</table>
							</div>
							
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_list();">목록</a></span></p>
								<c:if test="${clientInfo.DEAL_STAT_FLAG == 0}">
									<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_update();">수정하기</a></span> <span class="button medium"><a href="#1" onclick="javascript:fn_delete();">삭제하기</a></span></p>
								</c:if>
							</div>
						</div>
					</div>
					</form>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
			<!--  //content end -->
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->		
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<c:if test="${!empty trstList}">
	<c:forEach items="${trstList}" var="trstList">
		<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
			<script type="text/javascript">
			<!--
				window.onload = function(){	
					resizeDiv('tabKappItemList', 'divKappItemList');
				}
			//-->
			</script>
		</c:if>
	</c:forEach>
</c:if>
</body>
</html>
