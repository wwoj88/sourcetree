<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>권리찾기수정 | 신청현황조회 | 저작물 권리찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/general.js"></script>	
<script type="text/javascript">
<!--

window.name = "rghtPrpsModi_musc";

// 음악저작물 상세 팝업오픈
function openMusicDetail( crId, nrId, meId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	param += '&NR_ID='+nrId;
	param += '&ALBUM_ID='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=M'+param
	var name = '';
	var openInfo = 'target=rghtPrps_musc width=705, height=525';
	
	window.open(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	
	
	var the_height = document.getElementById(name).offsetHeight;
	var chkObjs = document.getElementsByName("chk");
	  
	document.getElementById(targetName).style.height = the_height + 13 + "px" ;
	  

}

// 스크롤셋팅
function scrollSet(name, targetName, oRowName){
	
	var totalRow = document.getElementsByName(oRowName).length;
		
	// 세로 기준건수 이상인 경우 세로스크롤 제어
	if( totalRow < 6) {
		resizeDiv(name, targetName);
		document.getElementById(targetName).style.overflowY = "hidden";
	}
	
	if( totalRow > 5 ) {
		document.getElementById(targetName).style.overflowY = "auto";
	}
}


// 토탈건수 set
function setTotCnt( oRowName, oSpanId) {

	var totalRow = document.getElementsByName(oRowName).length;
	document.getElementById(oSpanId).innerHTML = "("+totalRow+"건)";
}



// 테이블 행추가/삭제
function editTable(type){
	
	// 1. 신청 목적 선택 먼저
	if ( document.getElementById("PRPS_RGHT_CODE").value == '')  {
		
		alert("신청 목적 선택 후 가능한 기능입니다.");
		
		document.getElementById("PRPS_RGHT_CODE").focus();
		
		return;
	}
	
	// 2. 신청 신탁관리단체 먼저
	if ( !document.getElementById("CHK_201").checked && !document.getElementById("CHK_202").checked && !document.getElementById("CHK_203").checked)  {
		
		alert("신청 신탁관리단체 선택 후 가능한 기능입니다.");
		
		document.getElementById("CHK_201").focus();
		return;
	}
	
	// 3. tableId setting
	var tableId = "listTab";	//권리자/대리인 권리찾기
	var chkId = "iChk";
	var disSeq = "displaySeq";
	var totalRow = "totalRow";
	var divId = "div_1";
	var scrllDivId = "div_scroll_1";
	
	if(document.getElementById("PRPS_RGHT_CODE").value == '3')	{
		 tableId = "listTab_2";		//이용허락
		 chkId = "iChk2";
		 disSeq = "displaySeq2";
		 totalRow = "totalRow2";
		 divId = "div_2";
		 scrllDivId = "div_scroll_2";
	}
	
	if( type == 'D' ) {
		
	    var oTbl = document.getElementById(tableId);
	    var oChkDel = document.getElementsByName(chkId);
	    var iChkCnt = oChkDel.length;
	    var iDelCnt = 0;
	    
	    if(iChkCnt == 1 && oChkDel[0].checked == true){
	        oTbl.deleteRow(1);
	        iDelCnt++;
	    }else if(iChkCnt > 1){
    	    for(i = iChkCnt-1; i >= 0; i--){
    	        if(oChkDel[i].checked == true){    	            
    	            oTbl.deleteRow(i+1);
    	            iDelCnt++;
    	        }
    	    }
    	}
    	
    	if(iDelCnt < 1){
    	    alert('삭제할 저작물을 선택하여 주십시요');
    	}
   
    	fn_resetSeq(disSeq);
    		

	}else if( type == 'I' ) {
		
		newRow = document.getElementById(tableId).insertRow(document.getElementById(tableId).rows.length);
		
		newRow.onmouseover=function(){document.getElementById(tableId).clickedRowIndex=this.rowIndex}
		newRow.valign = 'middle';
		newRow.align = 'center';
		makeCell(tableId, newRow, document.getElementById(tableId).rows.length);
	
	}
	
	// 스크롤에 의한 height reset
	//resizeDiv(tableId, divId);
	scrollSet(tableId, scrllDivId, chkId);
		
	// 토탈건수
	setTotCnt( chkId, totalRow);
}
	
    
//순번 재지정
function fn_resetSeq(disName){
    var oSeq = document.getElementsByName(disName);
    for(i=0; i<oSeq.length; i++){
        oSeq[i].value = i+1;
    }
}

//선택저작물 테이블 idx
var iRowIdx = 1;
var iRowIdx2 = 1;
       
function makeCell(tableId, cur_row, rowPos) {
	
	var i=0;
	
	var class_201 = "inputDisible"; 	var read_201 = "readonly";
	var class_202 = "inputDisible";	var read_202 = "readonly";
	var class_203 = "inputDisible";	var read_203 = "readonly";
	
	// 신청목적에 따라 생성 td 가 다르다.
	var prpsDiv = document.getElementById("PRPS_RGHT_CODE").value;
	
	if( document.getElementById("CHK_201").checked )	{
		class_201 = "inputData";		read_201 = "";
	}
	
	if( document.getElementById("CHK_202").checked ) {
		class_202 = "inputData";		read_202 = "";
	}
	
	if( document.getElementById("CHK_203").checked ) {
		class_203 = "inputData";		read_203 = "";
	}
	
	// 권리자/대리인 권리찾기
	if( tableId == 'listTab' ) {
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk\"  id=\"iChk_'+iRowIdx+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce';
		innerHTMLStr = '<input name=\"displaySeq\" id=\"displaySeq_'+iRowIdx+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작물 정보
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"MUSIC_TITLE\"  id=\"MUSIC_TITLE_'+iRowIdx+'\" class="inputData w90" title=\"저작물명\" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"ALBUM_TITLE\"  id=\"ALBUM_TITLE_'+iRowIdx+'\" class="inputData w90" title=\"앨범명\"  nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"PERF_TIME\"  id=\"PERF_TIME_'+iRowIdx+'\" class="inputData w90" title=\"실연시간\" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"ISSUED_DATE\"  id=\"ISSUED_DATE_'+iRowIdx+'\" class="inputData w90" title=\"발매일\" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"LYRICS\"  id=\"LYRICS_'+iRowIdx+'\" class="inputData w90" title=\"가사\" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작권자 201
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"LYRICIST\" id=\"LYRICIST_'+iRowIdx+'\" class="'+class_201+' w90" '+read_201+' />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"COMPOSER\" id=\"COMPOSER_'+iRowIdx+'\" class="'+class_201+' w90" '+read_201+' />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"ARRANGER\" id=\"ARRANGER_'+iRowIdx+'\" class="'+class_201+' w90" '+read_201+' />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작인접권자 202
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"SINGER\" id=\"SINGER_'+iRowIdx+'\" class="'+class_202+' w90" '+read_202+' />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"PLAYER\" id=\"PLAYER_'+iRowIdx+'\" class="'+class_202+' w90" '+read_202+' />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"CONDUCTOR\" id=\"CONDUCTOR_'+iRowIdx+'\" class="'+class_202+' w90" '+read_202+' />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작인접권자 203
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"PRODUCER\" id=\"PRODUCER_'+iRowIdx+'\" class="'+class_203+' w90" '+read_203+' />';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="LYRICIST_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="COMPOSER_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="ARRANGER_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="SINGER_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="PLAYER_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="CONDUCTOR_ORGN" />';
		innerHTMLStr += '<input type="hidden" name="PRODUCER_ORGN" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		fn_resetSeq("displaySeq");
		
		// setfocus
		//var oFuc = 'MUSIC_TITLE_'+iRowIdx;
		//	document.getElementById(oFuc).focus();
		
		iRowIdx++;
	}
	
	// 이용자 권리찾기
	else if( tableId == 'listTab_2' ) {
	//alert(iRowIdx2);
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input type=\"checkbox\" name=\"iChk2\"  id=\"iChk2_'+iRowIdx2+'\"class="vmid" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		cur_cell = cur_row.insertCell(i++);
		cur_cell.className = 'ce';
		innerHTMLStr = '<input name=\"displaySeq2\" id=\"displaySeq2_'+iRowIdx2+'\" type=\"text\" class=\"w100 ce\" style=\"border:0px;\" readonly="readonly" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 저작물 명
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"MUSIC_TITLE\"  id=\"MUSIC_TITLE_'+iRowIdx2+'\" class="inputData w90" title=\"저작물명\" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 앨범명
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"ALBUM_TITLE\"  id=\"ALBUM_TITLE_'+iRowIdx2+'\" class="inputData w90" title=\"앨범명\" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 연주시간
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"PERF_TIME\"  id=\"PERF_TIME_'+iRowIdx2+'\" class="inputData w90" nullCheck />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 발매일
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"ISSUED_DATE\"  id=\"ISSUED_DATE_'+iRowIdx2+'\" class="inputData w90" nullCheck  />';
		cur_cell.innerHTML = innerHTMLStr;
		
		// 가사
		cur_cell = cur_row.insertCell(i++);
		innerHTMLStr = '<input name=\"LYRICS\"  id=\"LYRICS_'+iRowIdx2+'\" class="inputData w90"  />';
		innerHTMLStr += '<input type="hidden" name="iChkVal" />';
		innerHTMLStr += '<input type="hidden" name="LYRICIST" />';
		innerHTMLStr += '<input type="hidden" name="COMPOSER" />';
		innerHTMLStr += '<input type="hidden" name="ARRANGER" />';
		innerHTMLStr += '<input type="hidden" name="SINGER" />';
		innerHTMLStr += '<input type="hidden" name="PLAYER" />';
		innerHTMLStr += '<input type="hidden" name="CONDUCTOR" />';
		innerHTMLStr += '<input type="hidden" name="PRODUCER" />';
		cur_cell.innerHTML = innerHTMLStr;
		
		
		fn_resetSeq("displaySeq2");
		
		// setfocus
		//	var oFuc = 'MUSIC_TITLE_'+iRowIdx2;
		//	document.getElementById(oFuc).focus();
		
		iRowIdx2++;
	}
}

// 신청목적 선택
function prps_check(chk) {
	
	// 권리자 권리찾기
	if(chk.value == '1' || chk.value == '2' ) {
		document.getElementById("div_1").style.display = "";
		document.getElementById("div_2").style.display = "none";
		fn_lock( "listTab", "" );
		fn_lock( "listTab_2", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab", "div_scroll_1", "iChk");
		
		// 토탈건수
		setTotCnt( "iChk", "totalRow");
		
	} else {
		document.getElementById("div_2").style.display = "";
		document.getElementById("div_1").style.display = "none";
		fn_lock( "listTab_2", "" );
		fn_lock( "listTab", "true" );
		
		// 스크롤에 의한 height reset
		scrollSet("listTab_2", "div_scroll_2", "iChk2");
		
		// 토탈건수
		setTotCnt( "iChk2", "totalRow2");
		
	}
	
}

// 신탁관리단체 선택
function trust_check(chk) {

	var nRow = document.getElementsByName("LYRICIST").length;	// 전체 길이
	
	var oInLyr = document.getElementsByName("LYRICIST");
	var oInCom = document.getElementsByName("COMPOSER");
	var oInArr = document.getElementsByName("ARRANGER");

	var oInSin = document.getElementsByName("SINGER");
	var oInPla = document.getElementsByName("PLAYER");
	var oInCon = document.getElementsByName("CONDUCTOR");
	var oInPro = document.getElementsByName("PRODUCER");
	
		
	for( k=0; k<nRow; k++) {
	
		// 음저협	
		if( chk.id=="CHK_201" ) {
			
			if(!chk.checked) {
				oInLyr[k].readOnly = true;
				oInCom[k].readOnly = true;
				oInArr[k].readOnly = true;
				
				oInLyr[k].className = 'inputDisible w90';
				oInCom[k].className = 'inputDisible w90';
				oInArr[k].className = 'inputDisible w90';
			} else {
				oInLyr[k].readOnly = false;
				oInCom[k].readOnly = false;
				oInArr[k].readOnly = false;

				oInLyr[k].className = 'inputData w90';
				oInCom[k].className = 'inputData w90';
				oInArr[k].className = 'inputData w90';
			}
		}
		
		// 음실연	
		if( chk.id=="CHK_202" ) {
			
			if(!chk.checked) {
				oInSin[k].readOnly = true;
				oInPla[k].readOnly = true;
				oInCon[k].readOnly = true;
				
				oInSin[k].className = 'inputDisible w90';
				oInPla[k].className = 'inputDisible w90';
				oInCon[k].className = 'inputDisible w90';
			} else {
				oInSin[k].readOnly = false;
				oInPla[k].readOnly = false;
				oInCon[k].readOnly = false;

				oInSin[k].className = 'inputData w90';
				oInPla[k].className = 'inputData w90';
				oInCon[k].className = 'inputData w90';
			}
		}
		
		// 음제협
		if( chk.id=="CHK_203" ) {
			
			if(!chk.checked) {
				oInPro[k].readOnly = true;
				oInPro[k].className = 'inputDisible w90';
			} else {
				oInPro[k].readOnly = false;
				oInPro[k].className = 'inputData w90';
			}
		}
		
	} // .. end for
}

// 권리찾기수정
function rghtPrpsModi() {

	var frm = document.prpsForm;
	
	if(checkForm(frm)) {
		
		if( !document.getElementById("CHK_201").checked &&  !document.getElementById("CHK_202").checked && !document.getElementById("CHK_203").checked){
			alert('신청 신탁관리단체을(를) 선택하세요.');
			document.getElementById("CHK_201").focus();
			return;
		}
		
		if( document.getElementsByName("iChkVal").length <1 ){
			alert('신청 저작물(를) 추가하세요.');
			document.getElementById("workAdd").focus();
			return;
		}
		
		frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrpsModiProc&DIVS=M";
		
		frm.method="post";
		frm.submit();

	}
}

// 테이블 하위 disable
function fn_lock( tableId, flag ){
	
	var oTbl = document.getElementById(tableId);
	var oInput = oTbl.getElementsByTagName("input");
	
	for(i=0; i<oInput.length;i++){
			oInput[i].disabled= flag ;
	}
}

// 오프라인 접수
function offLine_check(chk) {
	
	if(chk.checked) {
		document.getElementById("td_file_no").style.display = "";
		document.getElementById("td_file_yes").style.display = "none";
	} else{
		document.getElementById("td_file_no").style.display = "none";
		document.getElementById("td_file_yes").style.display = "";
	} 
}

function goList(){
	
	var frm = document.prpsForm;
	frm.action = '/rsltInqr/rsltInqr.do?DIVS=10&page_no=1';
	frm.method="post";
	frm.submit();
}

function showAttach(cnt) {
	var i=0;
	var content = "";
	var attach = document.getElementById("attach")
	attach.innerHTML = "";
	//document.all("attach").innerHTML = "";
	content += '<table>';
	for (i=0; i<cnt; i++) {
		content += '<tr>';
		content += '<td><input type="file" name="attachfile'+(i+1)+'" size="70;" class="inputFile"><\/td>';
		content += '<\/tr>';
	}
	content += '<\/table>';
	//document.all("attach").innerHTML = content;
	attach.innerHTML = content;
}

function applyAtch() {
	var ansCnt = document.prpsForm.atchCnt.value;
	showAttach(parseInt(ansCnt));
}

 function initParameter(){
	
	// 신청목적에 따른 저작물 테이블 제어
	prps_check(document.getElementById("PRPS_RGHT_CODE"));
	
	// 신청 관리단체에 따른 저작물 테이블 제어
	trust_check(document.getElementById("CHK_201"));
	trust_check(document.getElementById("CHK_202"));
	trust_check(document.getElementById("CHK_203"));
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(3);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_3.png" alt="신청현황조회" title="신청현황조회" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li class="active"><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1">권리찾기<span>&lt;</span></a></li>
							<li><a href="/rsltInqr/rsltInqr.do?DIVS=20&amp;page_no=1">보상금<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>신청현황조회</span>&gt;<strong>권리찾기</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>음악저작물 권리찾기신청 수정
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
										
					<div class="bbsSection">
						<h4>음악권리찾기신청 수정</h4>
						<form name="prpsForm" enctype="multipart/form-data" action="">
							<input type="hidden" name="USER_IDNT" value="${rghtPrps.USER_IDNT}"/>
							<input type="hidden" name="PRPS_MAST_KEY" value="${rghtPrps.PRPS_MAST_KEY}"/>
							<input type="hidden" name="PRPS_SEQN" value="${rghtPrps.PRPS_SEQN}"/>
							
							<fieldset>
								<legend></legend>
								<!-- 테이블 영역입니다 -->
								<div class="tabelRound">
									<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="10%">
										<col width="10%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row" rowspan="2" class="bgbr lft">신청정보</th>
												<th scope="row" class="bgbr2 lft"><label for="sch1" class="necessary">신청목적</label></th>
												<td>
													<select name="PRPS_RGHT_CODE" id="PRPS_RGHT_CODE"  title="신청목적" nullCheck onchange="prps_check(this);">
														<option value="">선택 ------</option>
														<option value="1" <c:if test="${rghtPrps.PRPS_RGHT_CODE == '1'}">selected</c:if>>권리자 권리찾기</option>
														<option value="2" <c:if test="${rghtPrps.PRPS_RGHT_CODE == '2'}">selected</c:if>>대리인 권리찾기</option>
														<option value="3" <c:if test="${rghtPrps.PRPS_RGHT_CODE == '3'}">selected</c:if>>이용자 권리찾기</option>
													</select>
												</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr2 lft">신청인 정보</th>
												<td>
													<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
													<colgroup>
														<col width="15%">
														<col width="*">
														<col width="20%">
														<col width="30%">
														</colgroup>
														<tbody>
															<tr>
																<th class="bgbr3 lft">성명</th>
																<td>${rghtPrps.USER_NAME}</td>
																<th class="bgbr3 lft">주민번호/사업자번호</th>
																<td>${rghtPrps.RESD_CORP_NUMB}</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">전화</th>
																<td>
																	<label class="labelBlock"><em class="w45">자택</em><input name="HOME_TELX_NUMB" class="inputData w60" value="${rghtPrps.HOME_TELX_NUMB}"/></label>
																	<label class="labelBlock"><em class="w45">사무실</em><input name="BUSI_TELX_NUMB" class="inputData w60" value="${rghtPrps.BUSI_TELX_NUMB}"/></label>
																	<label class="labelBlock"><em class="w45">휴대폰</em><input name="MOBL_PHON" class="inputData w60" value="${rghtPrps.MOBL_PHON}"/></label>
																</td>
																<th class="bgbr3 lft"><label for="sch3">Fax</label></th>
																<td><input class="inputData" name="FAXX_NUMB" id="sch3" value="${rghtPrps.FAXX_NUMB}"/></td>
															</tr>
															<tr>
																<th class="bgbr3 lft"><label for="sch4">이메일주소</label></th>
																<td colspan="3"><input name="MAIL" class="inputData w90" id="sch4" value="${rghtPrps.MAIL}"/></td>
															</tr>
															<tr>
																<th class="bgbr3 lft">주소</th>
																<td colspan="3">
																	<label class="labelBlock"><em class="w45">자택</em><input name="HOME_ADDR" class="inputData w80" value="${rghtPrps.HOME_ADDR}"/></label>
																	<label class="labelBlock"><em class="w45">사무실</em><input name="BUSI_ADDR" class="inputData w80" value="${rghtPrps.BUSI_ADDR}"/></label>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft"><label class="necessary">신청 신탁관리단체</label></th>
												<td>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_201" value="201" class="inputRChk" onclick="trust_check(this);"  <c:if test="${rghtPrps.CHK_201 == '1'}">checked</c:if> />음저협</label>
													<label class="ml10"><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_203" value="203" class="inputRChk" onclick="trust_check(this);"  <c:if test="${rghtPrps.CHK_203 == '1'}">checked</c:if> />음제협</label>
													<label class="ml10"><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_202" value="202" class="inputRChk" onclick="trust_check(this);"  <c:if test="${rghtPrps.CHK_202 == '1'}">checked</c:if> />음실연</label>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft"><label class="necessary">신청저작물</label></th>
												<td>	
													<p class="rgt">
														<span class="button small type4 icon"><span class="arrow"></span><a href="javascript:editTable('I');"  id="workAdd">추가</a></span> 
														<span class="button small type3"><a href="javascript:editTable('D');">삭제</a></span>
													</p>
													
													<!-- 테이블 영역입니다 -->
													<div id="div_1" class="tabelRound" style="width:585px; padding:0 0 0 0; display:none"> 
														<h5>권리자/대리인 권리찾기 <span id="totalRow"></span></h5>
														<div id="div_scroll_1" class="tabelRound div_scroll" style="width:585px; padding:0 0 0 0;">
														<table id="listTab" style="width:1000px;" cellspacing="0" cellpadding="0" border="1" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
																<col width="10" />
																<col width="10" />
																<col width="80" />
																<col width="80" />
																<col width="30" />
																<col width="30" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk');"/></th>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH"><label class="necessary">저작물명</label></th>
																	<th scope="col" class="headH"><label class="necessary">앨범명</label></th>
																	<th scope="col" class="headH"><label class="necessary">실연시간</label></th>
																	<th scope="col" class="headH"><label class="necessary">발매일</label></th>
																	<th scope="col" class="headH">가사</th>
																	<th scope="col" class="headH">작사</th>
																	<th scope="col" class="headH">작곡</th>
																	<th scope="col" class="headH">편곡</th>
																	
																	<th scope="col" class="headH">가창</th>
																	<th scope="col" class="headH">연주</th>
																	<th scope="col" class="headH">지휘</th>
																	
																	<th scope="col" class="headH">앨범제작</th>
																</tr>
															</thead>
															<tbody>
															<c:if test="${!empty workList}">
																<c:forEach items="${workList}" var="workList">
																	<c:set var="NO" value="${workList.totalRow}"/>
																	<c:set var="i" value="${i+1}"/>
																<tr>
																	<td class="ce"><input type="checkbox" name="iChk" id="iChk_${i}" class="vmid"  /></td>
																	<td class="ce"><input name="displaySeq" id="displaySeq_${i}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${i}"/></td>
																	
																	<!-- 기존Meta저작물 -->
																	<c:if test="${workList.PRPS_IDNT_CODE == '1'}">
																	<td><a href="javascript:openMusicDetail('${workList.PRPS_IDNT}','${workList.PRPS_IDNT_NR}','${workList.PRPS_IDNT_ME}');"><u>${workList.TITLE }</u></a></td>
																	<td>${workList.ALBUM_TITLE }</td>
																	<td class="ce">${workList.PERF_TIME }</td>
																	<td class="ce">${workList.ISSUED_DATE }</td>
																	<td class="ce">
																		${workList.LYRICS }
																		<!-- hidden Value -->
																		<input type="hidden" name="iChkVal" value="${workList.PRPS_IDNT}|${workList.PRPS_IDNT_NR}|${workList.PRPS_IDNT_ME}"/>
																		<input type="hidden" name="MUSIC_TITLE" />
																		<input type="hidden" name="ALBUM_TITLE" />
																		<input type="hidden" name="PERF_TIME" />
																		<input type="hidden" name="ISSUED_DATE" />
																		<input type="hidden" name="LYRICS" />
																	</td>
																	</c:if>
																	
																	<!-- 추가등록저작물 -->
																	<c:if test="${workList.PRPS_IDNT_CODE == '2'}">
																	<td class="ce"><input name="MUSIC_TITLE" id="MUSIC_TITLE_${i}" class="inputData w90" value="${workList.TITLE }" /></td>
																	<td class="ce"><input name="ALBUM_TITLE" id="ALBUM_TITLE_${i}" class="inputData w90" value="${workList.ALBUM_TITLE }" /></td>
																	<td class="ce"><input name="PERF_TIME" id="PERF_TIME_${i}" class="inputData w90" value="${workList.PERF_TIME }" /></td>
																	<td class="ce"><input name="ISSUED_DATE" id="ISSUED_DATE_${i}" class="inputData w90" value="${workList.ISSUED_DATE }" /></td>
																	<td class="ce">
																		<input name="LYRICS" id="LYRICS_${i}" class="inputData w90" value="${workList.LYRICS }" />
																		<!-- hidden Value -->
																		<input type="hidden" name="iChkVal" value=""/>
																	</td>
																	</c:if>
																	
																	<td class="ce"><input name="LYRICIST" id="LYRICIST_${i}" class="inputDisible w90 ce" value="${workList.LYRICIST }" readonly /></td>
																	<td class="ce"><input name="COMPOSER" id="COMPOSER_${i}" class="inputDisible w90 ce" value="${workList.COMPOSER }" readonly/></td>
																	<td class="ce"><input name="ARRANGER" id="ARRANGER_${i}" class="inputDisible w90 ce" value="${workList.ARRANGER }" readonly/></td>
																	
																	<td class="ce"><input name="SINGER" id="SINGER_${i}" class="inputDisible w90 ce" value="${workList.SINGER }" readonly/></td>
																	<td class="ce"><input name="PLAYER" id="PLAYER_${i}" class="inputDisible w90 ce" value="${workList.PLAYER }" readonly/></td>
																	<td class="ce"><input name="CONDUCTOR" id="CONDUCTOR_${i}" class="inputDisible w90 ce" value="${workList.CONDUCTOR }" readonly/></td>
																	<td class="ce">
																		<input name="PRODUCER" id="PRODUCER_${i}" class="inputDisible w90 ce" value="${workList.PRODUCER }" readonly/>
																		<!-- hidden Value -->
																		<input type="hidden" name="LYRICIST_ORGN" value="${workList.LYRICIST_ORGN }" />
																		<input type="hidden" name="COMPOSER_ORGN" value="${workList.COMPOSER_ORGN }" />
																		<input type="hidden" name="ARRANGER_ORGN" value="${workList.ARRANGER_ORGN }"/>
																		<input type="hidden" name="SINGER_ORGN" value="${workList.SINGER_ORGN }" />
																		<input type="hidden" name="PLAYER_ORGN" value="${workList.PLAYER_ORGN }" />
																		<input type="hidden" name="CONDUCTOR_ORGN" value="${workList.CONDUCTOR_ORGN}" />
																		<input type="hidden" name="PRODUCER_ORGN" value="${workList.PRODUCER_ORGN}" />
																	</td>
																</tr>
																
																<script type="text/javascript">
																<!--
																	iRowIdx++;
																//-->
																</script>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
														</div>
													</div>
													<!-- //테이블 영역입니다 -->
													
													<!-- 테이블 영역입니다 -->
													<div id="div_2" class="tabelRound mt10" style="width:585px; display:none"> 
														<h5>이용허락 <span id="totalRow2"></span></h5>
														<div id="div_scroll_2" class="tabelRound div_scroll" style="width:585px; padding:0 0 0 0;">
														<table id="listTab_2" style="width:700px;" cellspacing="0" cellpadding="0" border="1" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
															<col width="10" />
															<col width="10" />
															<col width="80" />
															<col width="80" />
															<col width="30" />
															<col width="30" />
															<col width="100" />
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('prpsForm','iChk2');"/></th>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH"><label class="necessary">저작물명</label></th>
																	<th scope="col" class="headH"><label class="necessary">앨범명</label></th>
																	
																	<th scope="col" class="headH"><label class="necessary">연주시간</label></th>
																	<th scope="col" class="headH"><label class="necessary">발매일</label></th>
																	<th scope="col" class="headH">가사</th>
																</tr>
															</thead>
															<tbody>
																<c:if test="${!empty workList}">
																<c:forEach items="${workList}" var="workList">
																	<c:set var="NO" value="${workList.totalRow}"/>
																	<c:set var="ii" value="${ii+1}"/>
																<tr>
																	<td class="ce"><input type="checkbox" name="iChk2" id="iChk2_${ii}" class="vmid" /></td>
																	<td class="ce">
																		<input name="displaySeq2" id="displaySeq2_${ii}" type="text" class="w100 ce" style="border:0px;" readonly="readonly" value="${ii}"/>
																		<!-- HIDDEN VALUE -->
																		<input type="hidden" name="LYRICIST" />
																		<input type="hidden" name="COMPOSER" />
																		<input type="hidden" name="ARRANGER" />
																		<input type="hidden" name="SINGER" />
																		<input type="hidden" name="PLAYER"/>
																		<input type="hidden" name="CONDUCTOR" />
																		<input type="hidden" name="PRODUCER" />
																	</td>
																	
																	<!-- 기존Meta저작물 -->
																	<c:if test="${workList.PRPS_IDNT_CODE == '1'}">
																	<td><a href="javascript:openMusicDetail('${workList.PRPS_IDNT}','${workList.PRPS_IDNT_NR}','${workList.PRPS_IDNT_ME}');"><u>${workList.TITLE }</u></a></td>
																	<td>${workList.ALBUM_TITLE }</td>
																	<td class="ce">${workList.PERF_TIME }</td>
																	<td class="ce">${workList.ISSUED_DATE }</td>
																	<td class="ce">
																		${workList.LYRICS }
																		<!-- hidden Value -->
																		<input type="hidden" name="iChkVal" value="${workList.PRPS_IDNT}|${workList.PRPS_IDNT_NR}|${workList.PRPS_IDNT_ME}"/>
																		<input type="hidden" name="MUSIC_TITLE" />
																		<input type="hidden" name="ALBUM_TITLE" />
																		<input type="hidden" name="PERF_TIME" />
																		<input type="hidden" name="ISSUED_DATE" />
																		<input type="hidden" name="LYRICS" />
																	</td>
																	</c:if>
																	
																	<!-- 추가등록저작물 -->
																	<c:if test="${workList.PRPS_IDNT_CODE == '2'}">
																	<td class="ce"><input name="MUSIC_TITLE" id="MUSIC_TITLE_${i}" class="inputData w90" value="${workList.TITLE }" /></td>
																	<td class="ce"><input name="ALBUM_TITLE" id="ALBUM_TITLE_${i}" class="inputData w90" value="${workList.ALBUM_TITLE }" /></td>
																	<td class="ce"><input name="PERF_TIME" id="PERF_TIME_${i}" class="inputData w90" value="${workList.PERF_TIME }" /></td>
																	<td class="ce"><input name="ISSUED_DATE" id="ISSUED_DATE_${i}" class="inputData w90" value="${workList.ISSUED_DATE }" /></td>
																	<td class="ce">
																		<input name="LYRICS" id="LYRICS_${i}" class="inputData w90" value="${workList.LYRICS }" />
																		<!-- hidden Value -->
																		<input type="hidden" name="iChkVal" value=""/>
																	</td>
																	</c:if>
																	
																</tr>
																<script type="text/javascript">
																<!--
																	iRowIdx2++;
																//-->
																</script>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
														</div>
													</div>
													<!-- //테이블 영역입니다 -->
													
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft"><label class="necessary">신청내용</label></th>
												<td>
													<p class="blue">* 신청 목적이 이용자 권리찾기인 경우 이용내용(이용하고자 하는 목적/방법)을 작성합니다.</p>
													<textarea name="PRPS_DESC" id="PRPS_DESC" class="h100" title="신청내용" nullCheck>${rghtPrps.PRPS_DESC}</textarea>
												</td>
											</tr>
											<tr>									
												<th scope="row" colspan="2" class="bgbr lft">
													<label class="ml5"><input type="checkbox" name="OFFX_LINE_RECP" value="Y" class="inputChk" <c:if test="${rghtPrps.OFFX_LINE_RECP == 'Y'}">checked</c:if> onclick="javascript:offLine_check(this);" />오프라인접수</label>
													<br />첨부파일(증빙서류)
												</th>
												<td id="td_file_no" style="display:none">오프라인접수로 선택한 경우 첨부파일을 등록할 수 없습니다.</td>
												<td id="td_file_yes">
													<select name="atchCnt" id="atchCnt" onChange="javascript:applyAtch();">
														<c:forEach begin="1" end="10" step="1" varStatus="cnt">
															<option value="<c:out value="${cnt.count}"/>"><c:out value="${cnt.count}"/></option>
														</c:forEach>	
													</select>
													<div id="attach">
														<table>
														  <tr>
														   	<td><input type="file" name="attachfile0" size="70" class="inputFile"></td>
														  </tr>
														</table>
													</div>
												</td>	
												
												<!--td id="td_file_yes">
													<dl class="fl categori w80">
														<dt><strong>파일이름</strong></dt>
														<dd>
															<select size="4" class="w100"><option></option></select>
														</dd>
													</dl>
													<p class="fl ml10">
														<span class="button small icon"><span class="add"></span><button type="button">File 추가</button></span><br />
														<span class="button small icon mt5"><span class="del"></span><button type="button">File 삭제</button></span>
													</p>
												</td -->
											</tr>
											<!-- 2010.08.16 : 정확한 데이터입력이 어려움으로 인한 기능제거 (from 관련기관회의) START -->
											<!-- 
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">이용허락 저작재산권</th>
												<td>
													<label class="label_inBlock w20"><input type="checkbox" />복제권</label>
													<label class="label_inBlock w20"><input type="checkbox" />배포권</label>
													<label class="label_inBlock w20"><input type="checkbox" />대여권</label>
													<label class="label_inBlock w20"><input type="checkbox" />공연권</label><br />
													
													<label class="label_inBlock"><input type="checkbox" />공중송신권</label> (<label class="label_inBlock"><input type="checkbox" />방송권</label><label class="label_inBlock ml5"><input type="checkbox" />전송권</label><label class="label_inBlock ml5"><input type="checkbox" />디지털음성송신권</label>)<br />
													
													<label class="label_inBlock w20"><input type="checkbox" />전시권</label>
													<label class="label_inBlock w30"><input type="checkbox" />2차적저작물작성권</label>
												</td>
											</tr>
											 -->
											<!-- 2010.08.16 : 정확한 데이터입력이 어려움으로 인한 기능제거 (from 관련기관회의) END -->
										</tbody>
									</table>
								</div>
								<!-- //테이블 영역입니다 -->
							</fieldset>
						</form>
					</div>
					
					<div class="floatDiv mt10">
						<p class="fl"><span class="button medium icon"><span class="list"></span><a href="javascript:goList()">목록</a></span></p>
						<p class="fr">
							<!-- <span class="button medium icon"><span class="default"></span><a href="javascript:alert('미진행')">보상금 동시 신청</a></span>  -->
							<span class="button medium icon"><span class="default"></span><a href="javascript:rghtPrpsModi();">수정</a></span>
						</p>
					</div>
						
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/js/2010/script.js"></script>
</body>
</html>
