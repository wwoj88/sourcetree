<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>신청현황조회(보상금) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2010/calendar.css">
<style type="text/css">
<!--
.w55{width:55%;}
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>

<script type="text/JavaScript">
<!--
	//체크박스에 따른 화면 설정(첨부파일,오프라인)
	function fn_chgInsertDiv(sOrgn, obj){
		var frm = document.frm;
	
		if(obj.checked){
			if(sOrgn == 'krtra'){
				frm.hddnInsertKrtra.value = 'Y';
			}
			fn_chgDiv(sOrgn, 'Y');
		}else{
			if(sOrgn == 'krtra'){
				if(confirm('도서관 보상금 선택해제 시 관련 선택저작물이 신청이 되지않습니다.\r\n선택해제 하시겠습니까?')){
					frm.hddnInsertKrtra.value = 'N';
				}else{
					obj.checked	= true;
					return;
				}
			}
			fn_chgDiv(sOrgn, 'N');
		}
	}

	//체크박스에 따른 화면 설정(첨부파일,오프라인)
	function fn_chgDiv(sOrgn,sDiv){

		var oFldKrtra = document.getElementById("fldKrtra");
		
		var oInmtInfo, oItemList, oFileList, oOffLine;	//div
		var chkFile, chkOff;	//checkbox
		var oBtn;

		oInmtInfo	= document.getElementById("divKrtraInmtInfo");
		oItemList	= document.getElementById("divKrtraItemList");
		oFileList	= document.getElementById("divKrtraFileList");
		oOffLine	= document.getElementById("divKrtraOffline1");
		oBtn		= document.getElementById("spanKrtraBtn");

		chkOff	= document.getElementById("chkKrtraOff");

		if(sDiv == 'OFF'){
			if(chkOff.checked){
				oInmtInfo.style.display = '';
				oItemList.style.display	= '';
				oFileList.style.display = 'none';
				oOffLine.style.display	= '';
				oBtn.style.display		= '';
			}else{
				oInmtInfo.style.display = '';
				oItemList.style.display	= '';
				oFileList.style.display = '';
				oBtn.style.display		= '';
				oOffLine.style.display	= '';
			}
		}else if(sDiv == 'Y'){
			oFldKrtra.style.display = '';
		}else if(sDiv == 'N'){
			oFldKrtra.style.display = 'none';
		}
	}

	// 선택된 저작물 삭제
	function fn_delete(sStr){
		var frm = document.frm;
		
		//선택 목록
		var chkObjs = document.getElementsByName(sStr);
		var sCheck = 0;
		
		for(i=0; i<chkObjs.length;i++){
			if(chkObjs[i].checked){
				sCheck = 1;
			}
		}
		
		if(sCheck == 0 ){
			alert('선택된 저작물이 없습니다.');
			return;
		}
		
		for(var i=chkObjs.length-1; i >= 0; i--){
			var chkObj = chkObjs[i];
			
			if(chkObj.checked){
				// 선택된 저작물을 삭제한다.
				var oTR = findParentTag(chkObj, "TR");
				if(eval(oTR)) 	oTR.parentNode.removeChild(oTR);
			}
		}
	}

	//보상금신청
	function fn_chkSubmit(){
		var frm = document.frm;

		//신청자 정보 필수체크
		var oFldInmt	= document.getElementById("fldInmtInfo");
		var oInmt	= oFldInmt.getElementsByTagName("input");

		var txtHomeAddr	= "";	var txtBusiAddr	= "";
		var txtHomeTelxNumb	= "";  var txtBusiTelxNumb	= "";  var txtMoblPhon	= "";  var txtFaxxNumb	= "";  var txtMail = "";

		
		txtHomeAddr	= document.getElementsByName("txtHomeAddr").value;
		txtBusiAddr	= document.getElementsByName("txtBusiAddr").value;

		txtHomeTelxNumb	= document.getElementsByName("txtHomeTelxNumb").value;
		txtBusiTelxNumb	= document.getElementsByName("txtBusiTelxNumb").value;
		txtMoblPhon	= document.getElementsByName("txtMoblPhon").value;
		txtFaxxNumb	= document.getElementsByName("txtFaxxNumb").value;
		txtMail	= document.getElementsByName("txtMail").value;		
		
		for(i = 0; i < oInmt.length; i++){
			/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
			if(checkField(oInmt[i]) == false){
				return;
			}
			*/
			//nullCheck
			
			/*
			if(oInmt[i].name == 'txtHomeTelxNumb'
				|| oInmt[i].name == 'txtBusiTelxNumb'
				|| oInmt[i].name == 'txtMoblPhon'
				|| oInmt[i].name == 'txtFaxxNumb'
				|| oInmt[i].name == 'txtHomeAddr'
				|| oInmt[i].name == 'txtBusiAddr'
				|| oInmt[i].name == 'txtMail'){
				
				if(!nullCheck(oInmt[i]))	return;
			}
			*/
			
			// 전화번호
			if(oInmt[i].name == 'txtHomeTelxNumb'
				|| oInmt[i].name == 'txtBusiTelxNumb'
				|| oInmt[i].name == 'txtMoblPhon'
				|| oInmt[i].name == 'txtFaxxNumb'){
				
				if(txtHomeTelxNumb == ''){
					if(txtBusiTelxNumb == ''){
						if(txtMoblPhon == ''){
							if(txtFaxxNumb == ''){
								if(!nullCheck(oInmt[i]))	return;
							}	
						}
					}
				}
				
				if(!character(oInmt[i],  'EK'))	return;
			}

			// 자택 사무실 주소 nullCheck
			if(oInmt[i].name == 'txtHomeAddr'
				|| oInmt[i].name == 'txtBusiAddr'){
				if(txtHomeAddr == ''){
					if(txtBusiAddr == ''){
						if(!nullCheck(oInmt[i]))	return;
					}	
				}
			}
				
			// 입금처
			if(oInmt[i].name == 'txtBankName'
				|| oInmt[i].name == 'txtAcctNumb'
				|| oInmt[i].name == 'txtDptr'){
				
				if(!nullCheck(oInmt[i]))	return;
			}
			
			// E-mail
			if(oInmt[i].name == 'txtMail'){
				if(!character(oInmt[i],  'K'))	return;
			}

		}
		
		//체크박스 값들
		var vChkKrtraOff	= document.getElementById("chkKrtraOff").checked;
		if(vChkKrtraOff)		frm.hddnKrtraOff.value = "Y";

		var oChk205_2 = document.getElementById("chkTrts205_2");
		
		//복전협 정보 필수체크
		if(oChk205_2.checked){
			var oFldKrtra	= document.getElementById("fldKrtra");
			var oKrtra	= oFldKrtra.getElementsByTagName("input");
			for(i = 0; i < oKrtra.length; i++){
				/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
				if(checkField(oKrtra[i]) == false){
					return;
				}
				*/
				// nullCheck
				if(oKrtra[i].name == 'txtKrtraPemrRlnm'
					||oKrtra[i].name == 'txtKrtraResdNumb_1'
					||oKrtra[i].name == 'txtKrtraResdNumb_2'){
					
					if(!nullCheck(oKrtra[i]))	return;
				}

				// 글자길이 범위 체크
				if(oKrtra[i].name == 'txtKrtraResdNumb_1'){
					if(!rangeSize(oKrtra[i], '6'))	return;
				}
				
				if(oKrtra[i].name == 'txtKrtraResdNumb_2'){
					if(!rangeSize(oKrtra[i], '7'))	return;
				}
				
				// 금지할 문자 종류 체크 (E : 영문, K : 한글, N : 숫자)
				if(oKrtra[i].name == 'txtKrtraResdNumb_1'
					||oKrtra[i].name == 'txtKrtraResdNumb_2'){
					
					if(!character(oKrtra[i],  'EKS'))	return;
				}
			}

			//신청저작물 체크
			var oKrtraPrpsIdnt = document.getElementsByName("hddnKrtraPrpsIdnt");
			for(i = 0; i < oKrtraPrpsIdnt.length; i++){
				if(oKrtraPrpsIdnt[i].value == null || (oKrtraPrpsIdnt[i].value).length < 1){
					alert('내용(복사전송권협회) 정보를 입력해주세요.');
					return;
				}
			}
		}

		//내용 필수 체크
		var oContent	= document.getElementById("txtareaPrpsDesc");
		/* firefox에서 script가 nullCheck, dateCheck, rangeSize, character 등 이 인식되지 않음
		if(checkField(oContent) == false){
			return;
		}
		*/
		// nullCheck
		if(!nullCheck(oContent))	return;
		if(onkeylengthMax(oContent, 4000, 'txtareaPrpsDesc') == false){
			return;
		}
		
		if(oChk205_2.checked == false){
			alert('분류(을)를 선택 해 주세요.');
			return;
		}
		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrpsUpdate";
		frm.submit();
	}
	
	
	//목록으로 이동
	function fn_list(){
		var frm = document.srchFrm;

		frm.DIVS.value = '20';
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/rsltInqr/rsltInqr.do?method=list";
		frm.submit();
	}


	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.fileDown;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.method = "post";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
	}

    //maxlength 체크
	function onkeylengthMax(formobj, maxlength, objname) {  
	    var li_byte     = 0;  
	    var li_len      = 0;  
	    for(var i=0; i< formobj.value.length; i++){  
	    	
	        if (escape(formobj.value.charAt(i)).length > 4){  
	            li_byte += 3;  
	        } else {  
	            li_byte++;  
	        }  
	        if(li_byte <= maxlength) {  
	            li_len = i + 1;  
	        }  
	    }

	    if(li_byte > maxlength){  
	        alert('최대 글자 입력수를 초과 하였습니다. \n최대 글자 입력수를 초과된 내용은 자동으로 삭제됩니다.');  
	        formobj.value = formobj.value.substr(0, li_len);
	        return false;  
		    formobj.focus();  
	    }
	    return true;  
	}
	
	//숫자만 입력
	function only_arabic(t){
		var key = (window.netscape) ? t.which :  event.keyCode;

		if (key < 45 || key > 57) {
			if(window.netscape){  // 파이어폭스
				t.preventDefault();
			}else{
				event.returnValue = false;
			}
		} else {
			//alert('숫자만 입력 가능합니다.');
			if(window.netscape){   // 파이어폭스
				return true;
			}else{
				event.returnValue = true;
			}
		}	
	}
	
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container"  style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
			<div class="container_vis"  style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_6.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
				<!-- 래프 -->
				<jsp:include page="/include/2012/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1","lnb12");
				</script>
				<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>신청현황</span><em>미분배보상금 신청</em></p>
					<h1><img src="/images/2012/title/content_h1_0602.gif" alt="미분배보상금 신청" title="미분배보상금 신청" /></h1>
					<form name="srchFrm" action="#" class="sch">
						<input type="hidden" name="DIVS"			value="${srchParam.DIVS }"/>
						<input type="hidden" name="srchPrpsDivs"	value="${srchParam.srchPrpsDivs }"/>
						<input type="hidden" name="srchStartDate"	value="${srchParam.srchStartDate }"/>
						<input type="hidden" name="srchEndDate"		value="${srchParam.srchEndDate }"/>
						<input type="hidden" name="srchTitle"		value="${srchParam.srchTitle }"/>
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }"/>
						<input type="submit" style="display:none;">
					</form>
					
					<!-- hidden fileDownload -->
					<form name="fileDown" action="#">
						<input type="hidden" name="filePath">
						<input type="hidden" name="fileName">
						<input type="hidden" name="realFileName">
						<input type="submit" style="display:none;">
					</form>
						
						
					<form name="frm" action="#" enctype="multipart/form-data" class="sch">
						<input type="hidden" name="DIVS"			value="${srchParam.DIVS }"/>
						<input type="hidden" name="PRPS_MAST_KEY"	value="${srchParam.PRPS_MAST_KEY }"/>
						<input type="hidden" name="PRPS_SEQN"		value="${srchParam.PRPS_SEQN }"/>
						<input type="hidden" name="mode" 			value="U" />
						
						<input type="hidden" name="srchPrpsDivs"	value="${srchParam.srchPrpsDivs }"/>
						<input type="hidden" name="srchStartDate"	value="${srchParam.srchStartDate }"/>
						<input type="hidden" name="srchEndDate"		value="${srchParam.srchEndDate }"/>
						<input type="hidden" name="srchTitle"		value="${srchParam.srchTitle }"/>
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }"/>
						
						<input type="hidden" name="actFlagYn" />
						<input type ="hidden" name="prpsMastKey" value="${srchParam.PRPS_MAST_KEY}" />
						
						<input type="hidden" name="hddnTrst203"	value="${clientInfo.TRST_203 }" />
						<input type="hidden" name="hddnTrst202"	value="${clientInfo.TRST_202 }" />
						<input type="hidden" name="hddnTrst205"	value="${clientInfo.TRST_205 }" />
						<input type="hidden" name="hddnTrst205_2"	value="${clientInfo.TRST_205_2 }" />
						
						<input type="hidden" name="hddnRgstDttm" value="${clientInfo.RGST_DTTM }" />
						
						
						<input type="hidden" name="prpsDoblCode"	value="${clientInfo.PRPS_DOBL_CODE }" />
						<input type="hidden" name="prpsDoblKey"	value="${clientInfo.PRPS_DOBL_KEY }" />
						<input type="hidden" name="rghtPrpsMastKey"	value="${clientInfo.RGHT_PRPS_MAST_KEY }" />
						<input type="submit" style="display:none;">
					<div class="section">
						<!-- memo 삽입 -->
						<jsp:include page="/common/memo/2011/memo_01.jsp">
							<jsp:param name="DIVS" value="${srchParam.DIVS}"/>
						</jsp:include>
						<!-- // memo 삽입 -->
						<!-- article str -->
						<div class="article mt20">
							<div class="floatDiv">
								<h2 class="fl">도서관 미분배보상금 신청</h2>
							</div>
							<div id="fldInmtInfo"><!-- fieldset 보상금신청 신청자 정보 --> 
								<span class="topLine"></span>
								<!-- 그리드스타일 -->
								<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
									<colgroup>
									<col width="20%">
									<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">보상금종류</th>
											<td>도서관</td>
										</tr>
										<tr>
											<th scope="row">신청인정보</th>
											<td>
												<span class="topLine2"></span>
												<!-- 그리드스타일 -->
												<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
													<colgroup>
													<col width="15%">
													<col width="35%">
													<col width="25%">
													<col width="*">
													</colgroup>
													<tbody>
														<tr>
															<th scope="row"><label class="necessary">성명</label></th>
															<td>
																<input type="hidden" name="hddnUserIdnt" value="${clientInfo.USER_IDNT }" />
																<input type="text" name="txtPrpsName" value="${clientInfo.PRPS_NAME }" title="신청자 성" readonly/>
															</td>
															<th scope="row"><label class="necessary">주민번호/</label>사업자번호</th>
															<td>
																<input type="text" class="w90" name="txtPrsdCorpNumbView" value="${clientInfo.RESD_CORP_NUMB_VIEW }" title="주민번호/사업자번호" readonly />
															</td>
														</tr>
														<tr>
															<th scope="row">전화번호</th>
															<td>
																<ul class="list1">
																	<li class="p11"><label for="regi1" class="inBlock w25">자택</label> : <input type="text" id="regi1" name="txtHomeTelxNumb" value="${clientInfo.HOME_TELX_NUMB }" maxlength="14" title="신청자 전화(자택)" class="inputDataN w55" /></li>
																	<li class="p11"><label for="regi2" class="inBlock w25">사무실</label> : <input type="text" id="regi2" name="txtBusiTelxNumb" value="${clientInfo.BUSI_TELX_NUMB }" maxlength="14" title="신청자 전화(사무실)" class="inputDataN w55" /></li>
																	<li class="p11"><label for="regi3" class="inBlock w25">휴대폰</label> : <input type="text" id="regi3" name="txtMoblPhon" value="${clientInfo.MOBL_PHON }" maxlength="14" title="신청자 전화(휴대폰)" class="inputDataN w55" /></li>
																</ul>
															</td>
															<th scope="row">팩스번호</th>
															<td>
																<input name="txtFaxxNumb" value="${clientInfo.FAXX_NUMB }" maxlength="15" title="신청자 전화(FAX)" class="inputDataN" />
															</td>
														</tr>
														<tr>
															<th scope="row"><label class="necessary">주소</label></th>
															<td colspan="3">
																<ul class="list1">
																<li class="p11"><label for="txtHomeAddr" class="inBlock w10">자택</label> : <input type="text" name="txtHomeAddr" id="txtHomeAddr" value="${clientInfo.HOME_ADDR }" maxlength="50" title="신청자 주소(자택)" class="inputDataN w85" /></li>
																<li class="p11"><label for="txtBusiAddr" class="inBlock w10">사무실</label> : <input type="text" name="txtBusiAddr" id="txtBusiAddr" value="${clientInfo.BUSI_ADDR }" maxlength="50" title="신청자 주소(사무실)" class="inputDataN w85" /></li>
																</ul>
															</td>
														</tr>
														<tr>
															<th scope="row"><label class="necessary">입금처</label></th>
															<td>
																<ul class="list1">
																<li class="p11"><label for="txtBankName" class="inBlock w25">은행명</label> : <input type="text" id="txtBankName"  name="txtBankName" value="${clientInfo.BANK_NAME }" title="입금 은행명" maxlength="18" class="inputDataN w55" /></li>
																<li class="p11"><label for="txtAcctNumb" class="inBlock w25">계좌번호</label> : <input type="text" id="txtAcctNumb" name="txtAcctNumb" value="${clientInfo.ACCT_NUMB }" title="입금 계좌번호" maxlength="18" class="inputDataN w55" /></li>
																<li class="p11"><label for="txtDptr" class="inBlock w25">예금주</label> : <input type="text" id="txtDptr" name="txtDptr" value="${clientInfo.DPTR }" title="입금 예금주" maxlength="33" class="inputDataN w55" /></li>
																</ul>
															</td>
															<th scope="row">E-Mail</th>
															<td>
																<input type="text" name="txtMail" value="${clientInfo.MAIL }" maxlength="50" title="신청자 EMAIL" class="inputDataN w85" />
															</td>
														</tr>
													</tbody>
												</table>
												<!-- //그리드스타일 -->
											</td>
										</tr>
										<tr>
											<th scope="row">신청구분</th>
											<td>
												<ul class="line22">
													<li>
														<input type="checkbox" name="chkTrts205_2" id="chkTrts205_2" onclick="javascript:fn_chgInsertDiv('krtra',this);"
															<c:if test="${clientInfo.TRST_205_2 == '1'}">
																checked
															</c:if>
														 class="inputRChk" title="도서관 보상금" />
														 <label for="chkTrts205_2" class="p12">도서관 보상금</label>
														 <!-- hidden Value Setting -->
														<c:if test="${clientInfo.TRST_205_2 != '1'}">
															<input type="hidden" name="hddnInsertKrtra" value="N" />
														</c:if>
														<c:if test="${clientInfo.TRST_205_2 == '1'}">
															<input type="hidden" name="hddnInsertKrtra" value="Y" />
														</c:if>
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<th scope="row">신청저작물정보</th>
											<td>
												<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="보상금신청 저작물 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="10%">
													<col width="*">
													<col width="20%">
													<col width="20%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">순번</th>
															<th scope="col">도서명</th>
															<th scope="col">저자</th>
															<th scope="col">사용년도</th>
														</tr>
													</thead>
													<tbody>
													<c:if test="${!empty tempList}">
														<c:forEach items="${tempList}" var="tempList">	
															<c:set var="NO" value="${tempListCnt + 1}"/>
															<c:set var="i" value="${i+1}"/>
														<tr>
															<td class="ce">
																${NO - i }
																<input type="hidden" name="hddnSelectInmtSeqn" value="${tempList.INMT_SEQN }" />
																<input type="hidden" name="hddnSelectPrpsIdntCode" value="${tempList.PRPS_IDNT_CODE }" />
																<input type="hidden" name="hddnSelectPrpsDivs" value="${tempList.PRPS_DIVS }" />
																<input type="hidden" name="hddnSelectKapp" value="${tempList.KAPP }" />
																<input type="hidden" name="hddnSelectFokapo" value="${tempList.FOKAPO }" />
																<input type="hidden" name="hddnSelectKrtra" value="${tempList.KRTRA }" />
															</td>
															<td>${tempList.SDSR_NAME }</td>
															<td class="ce">${tempList.MUCI_NAME }</td>
															<td class="ce">${tempList.YYMM }</td>
														</tr>
														</c:forEach>
													</c:if>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</div> <!-- //fieldset -->
													
							<!-- 복전협 관련 영역 -->
							<div id="fldKrtra" style="display:
								<c:choose>
									<c:when test="${clientInfo.TRST_205_2 != '1'}">none</c:when>
								</c:choose>;"><!-- fieldSet str -->
								<div class="floatDiv mt20">
									<h3 class="fl">내용(한국복사전송권협회) 도서관 보상금
										<span class="ml10">
										<input type="checkbox" id="chkKrtraOff" onclick="javascript:fn_chgDiv('krtra','OFF');" class="inputChk ml10"
											<c:if test="${!empty trstList}">
											<c:forEach items="${trstList}" var="trstList">
												<c:if test="${trstList.TRST_ORGN_CODE=='205_2' }">
													<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if>
												</c:if>
											</c:forEach>
											</c:if> title="오프라인접수(첨부서류)" />
											<label for="chkKrtraOff" class="orange thin">오프라인접수(첨부서류)</label>
										<input type="hidden" name="hddnKrtraOff" value="N" />
										</span>
									</h3>
									<p class="fr">
										<span id="spanKrtraBtn"><a href="#1" onclick="javascript:fn_delete('chkKrtra');"><img src="/images/2012/button/delete.gif" alt="삭제" /></a></span>
									</p>
								</div>
								
								<!-- 보상금신청 실연자정보 테이블 영역입니다 -->
								<div id="divKrtraInmtInfo" class="mb5">
									<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="보상금신청 권리자정보 입력 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="30%">
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><label class="necessary">권리자</label></th>
												<td>
													<ul class="list1">
														<li class="p11"><label for="regi4" class="inBlock w25">본명</label> : <input type="text" id="regi4" name="txtKrtraPemrRlnm" value="${pemrRlnm }" title="실연자(본명)" class="inputDataN w60" /></li>
														<li class="p11"><label for="regi5" class="inBlock w25">필명</label> : <input type="text" id="regi5" name="txtKrtraGrupName" value="${pemrStnm}" title="권리자(필명)" maxlength="16" class="inputDataN w60" /></li>
														<li class="p11"><label for="regi6" class="inBlock w25">예명</label> : <input type="text" id="regi6" name="txtKrtraPemrStnm" value="${grupName}" title="권리자(예명)" maxlength="20" class="inputDataN w60" /></li>
													</ul>
												</td>
												<th scope="col"><label class="necessary">주민등록번호</label></th>
												<td>
												 	<input type="text" name="txtKrtraResdNumb_1" maxlength="6" Size="6" onkeypress="javascript:only_arabic(event);"value="${resdNumb_1}" title="권리자 주민번호(1번째)" class="inputDataN w40" />
													 -
													<input type="password" name="txtKrtraResdNumb_2" maxlength="7" Size="7" onkeypress="javascript:only_arabic(event);" value="${resdNumb_2}" title="권리자 주민번호(2번째)" class="inputDataN w40" />
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 권리자정보 테이블 영역입니다 -->
								<!-- 보상금신청 항목 테이블 영역입니다 -->
								<div id="divKrtraItemList" class="div_scroll" style="width:726px; padding:0 0 0 0;">
									<table id="div_scroll" cellspacing="0" cellpadding="0" border="1" class="grid mt10" summary="보상금신청 저작물 정보 입력폼입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
											<col width="6%">
											<col width="44%">
											<col width="19%">
											<col width="15%">
											<col width="8%">
											<col width="8%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col"><input type="checkbox" onclick="javascript:checkBoxToggle('frm','chkKrtra',this);" class="vmid" title="전체선택" /></th>
												<th scope="col">서명(논문명)</th>
												<th scope="col">(공동)저자명</th>
												<th scope="col">출판사</th>
												<th scope="col">발행년도</th>
												<th scope="col">판매유무</th>
											</tr>
										</thead>
										<tbody>
										<c:if test="${!empty krtraList_2}">
											<c:forEach items="${krtraList_2}" var="krtraList_2">	
												<c:set var="krtraNo" value="${krtraNo+1}"/>
												<!-- 일반 보상금신청 -->
												<c:if test="${clientInfo.PRPS_DOBL_CODE != '1' }">
												<tr>
													<td class="ce">
														<input type="checkbox" name="chkKrtra" id="chkKrtra" class="vmid" title="선택"/>
														<input type="hidden" name="hddnKrtraPrpsIdnt" value="${krtraList_2.PRPS_IDNT }" />
														<input type="hidden" name="hddnKrtraPrpsDivs" value="${krtraList_2.PRPS_DIVS }" />
														<input type="hidden" name="hddnKrtraPrpsIdntCode" value="${krtraList_2.PRPS_IDNT_CODE }" />
														
														<input type="hidden" name="hddnKrtraWorkName"	value="${krtraList_2.WORK_NAME }" />
														<input type="hidden" name="hddnKrtraWrtrName"	value="${krtraList_2.WRTR_NAME }" />
														<input type="hidden" name="hddnKrtraBookCncn"	value="${krtraList_2.BOOK_CNCN }" />
														<input type="hidden" name="hddnKrtraSctr"		value="${krtraList_2.SCTR }" />
														<input type="hidden" name="hddnKrtraUsexType"	value="${krtraList_2.USEX_TYPE }" />
													</td>
													<td>${krtraList_2.WORK_NAME }</td>
													<td class="ce">${krtraList_2.WRTR_NAME }</td>
													<td class="ce">${krtraList_2.BOOK_CNCN }</td>
													<td class="ce">${krtraList_2.SCTR }</td>
													<td class="ce">${krtraList_2.USEX_TYPE }</td>
												</tr>
												</c:if>
												<!-- 동시 보상금신청 -->
												<c:if test="${clientInfo.PRPS_DOBL_CODE == '1' }">
													<tr>
														<td class="ce">
															<input type="checkbox" name="chkKrtra" id="chkKrtra" class="vmid" title="선택" />
															<input type="hidden" name="hddnKrtraPrpsIdnt" value="${krtraList_2.PRPS_IDNT }" />
															<input type="hidden" name="hddnKrtraPrpsDivs" value="${krtraList_2.PRPS_DIVS }" />
															<input type="hidden" name="hddnKrtraPrpsIdntCode" value="${krtraList_2.PRPS_IDNT_CODE }" />
															
															<input type="hidden" name="hddnKrtraWorkName"	value="${krtraList_2.WORK_NAME }" />
															<!-- 
															<input type="hidden" name="hddnKrtraWrtrName"	value="${selectList.MUCI_NAME }" />
															<input type="hidden" name="hddnKrtraBookCncn"	value="${selectList.LISH_COMP }" />
															<input type="hidden" name="hddnKrtraSctr"		value="${selectList.YYMM }" />
															<input type="hidden" name="hddnKrtraUsexType"	value="${selectList.USEX_TYPE }" />
															 -->
														</td>
														<td>${krtraList_2.WORK_NAME }</td>
														<td class="ce"><input type="text" name="hddnKrtraWrtrName" value="${krtraList_2.WRTR_NAME }" maxlength="20" title="저자명" class="inputData w90 ce" /></td>
														<td class="ce"><input type="text" name="hddnKrtraBookCncn" value="${krtraList_2.BOOK_CNCN }" maxlength="50" title="출판사" class="inputData w90 ce" /></td>
														<td class="ce"><input type="text" name="hddnKrtraSctr" value="${krtraList_2.SCTR }" maxlength="4" title="발행년도" class="inputData w90 ce" /></td>
														<td class="ce"><input type="text" name="hddnKrtraUsexType" value="${krtraList_2.USEX_TYPE }" maxlength="4" title="판매유무" class="inputData w90 ce" /></td>
													</tr>
												</c:if>
											</c:forEach>
										</c:if>
										</tbody>
									</table>
								</div>
								<!-- 
								//보상금신청 항목 테이블 영역입니다 --><!--
								<div id="divKappFileList" class="tabelRound mb5" 
									style="display:<c:if test="${!empty trstList}">
									<c:forEach items="${trstList}" var="trstList">
										<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
											<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
										</c:if>
									</c:forEach>
									</c:if>;">
								</div>
								-->
								<div>
									<table cellspacing="0" cellpadding="0" border="1" class="grid mt10" style="margin-top:1px;" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col">첨부서류</th>
												<td>
													<div id="divKrtraFileList" class="mb5"
														style="display:<c:if test="${!empty trstList}">
																		<c:forEach items="${trstList}" var="trstList">
																			<c:if test="${trstList.TRST_ORGN_CODE=='205_2' }">
																				<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
																			</c:if>
																		</c:forEach>
																		</c:if>;">
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
								
								
								<!-- 보상금신청 오프라인 관련 영역 -->
								<div id="divKrtraOffline1" class="white_box">
									<div class="box5">
										<div class="box5_con floatDiv" style="width:713px;">
											<p class="fl mt5"><img src="/images/2012/content/box_img4.gif" alt="" /></p>
											<div class="fl ml30 mt5">
												<h4><strong>한국복사전송권협회(도서관보상금) 오프라인접수 선택한 경우 해당협회에 관련서류를<br> 제출하여야합니다.</strong></h4>
												<ul class="list1 mt10">
													<li>도서관보상금 신청서 
													<br><span class="blue2 fontSmall">(보상금 신청현황조회 상세화면에서 도서관보상금 신청서 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span>
													</li>
													<li>저작권자임을 확인할 수 있는 서류(저작권등록증 등)</li>
													<li>분배대상 저작물 명세표
													<br><span class="blue2 fontSmall">(보상금 신청현황조회 상세화면에서 분배대상 저작물 명세표 출력 가능. [위치] <u>신청현황조회>보상금</u>)</span>
													</li>
													<li>통장사본</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<!-- //보상금신청 오프라인 관련 영역 -->
							</div> <!-- //fieldset -->
							<div class="mt20"><!-- fieldeset보상금신청 내용-->
								<!-- 보상금신청(내용) 테이블 영역입니다 -->
								<div id="divContent" class="tabelRound mb5">
									<span class="topLine"></span>
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="보상금신청 내용입력 폼 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><label for="txtareaPrpsDesc" class="necessary">내용</label></th>
												<td scope="col">
													<textarea rows="10" name="txtareaPrpsDesc" id="txtareaPrpsDesc" title="내용" class="h100 w100">${clientInfo.PRPS_DESC }</textarea>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div><!-- //fieldset -->
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_list()">목록</a></span></p>
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_chkSubmit();">보상금 수정</a></span>
							</div>
						</div>
						<!-- //article end -->
					</div>
					</form>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
			<!--  //content end -->
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->		
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<script type="text/javascript" src="/js/2011/file.js"></script>
<script type="text/javascript">
<!--
	function fn_setFileInfo(){
		var listCnt = '${fn:length(fileList)}';
		<c:if test="${!empty fileList}">
			<c:forEach items="${fileList}" var="fileList">
			fn_addGetFile(	'205_2',
							'${fileList.PRPS_MAST_KEY}',
							'${fileList.PRPS_SEQN}',
							'${fileList.ATTC_SEQN}',
							'${fileList.FILE_PATH}',
							'${fileList.REAL_FILE_NAME}',
							'${fileList.FILE_NAME}',
							'${fileList.TRST_ORGN_CODE}',
							'${fileList.FILE_SIZE}');
			</c:forEach>
		</c:if>		
	}

	window.onload	= function(){	fn_createTable2("divKrtraFileList", "205_2");
									fn_setFileInfo();
								}
//-->
</script>
</body>
</html>
