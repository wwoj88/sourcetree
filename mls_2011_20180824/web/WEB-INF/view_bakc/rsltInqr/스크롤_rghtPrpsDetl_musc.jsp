<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>권리찾기 | 신청현황조회 | 저작물 권리찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/general.js"></script>	
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript">
<!--

window.name = "rghtPrpsDetl_musc";

// 음악저작물 상세 팝업오픈
function openMusicDetail( crId, nrId, meId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	param += '&NR_ID='+nrId;
	param += '&ALBUM_ID='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=M'+param
	var name = '';
	var openInfo = 'target=rghtPrpsDetl_musc width=705, height=525';
	
	window.open(url, name, openInfo);
}

// 처리결과 상세 팝업오픈
function openDetlDesc(mastKey, code, crId, nrId, meId, trstCode)  {
	var param = '';
	
	param += '&PRPS_MAST_KEY='+mastKey;
	param += '&PRPS_IDNT_CODE='+code;
	param += '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	param += '&TRST_ORGN_CODE='+trstCode;
	
	var url = '/rsltInqr/rsltInqr.do?method=rghtPrpsRsltDetl&DIVS=M'+param
	var name = '';
	var openInfo = 'target=rghtPrpsDetl_musc width=500, height=300';	
	window.open(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	
	
	var the_height = document.getElementById(name).offsetHeight;
	var chkObjs = document.getElementsByName("chk");
	  
	document.getElementById(targetName).style.height = the_height + 13 + "px" ;
	  

}

// 스크롤셋팅
function scrollSet(name, targetName, oRowName){
	
	var totalRow = document.getElementsByName(oRowName).length;
		
	// 세로 기준건수 이상인 경우 세로스크롤 제어
	if( totalRow < 6) {
		resizeDiv(name, targetName);
		document.getElementById(targetName).style.overflowY = "hidden";
	}
	
	if( totalRow > 5 ) {
		document.getElementById(targetName).style.overflowY = "auto";
	}
}


// 토탈건수 set
function setTotCnt( oRowName, oSpanId) {

	var totalRow = document.getElementsByName(oRowName).length;
	document.getElementById(oSpanId).innerHTML = "("+totalRow+"건)";
}

function goList(){
	
	var frm = document.prpsForm;
	frm.action = '/rsltInqr/rsltInqr.do?DIVS=10&page_no=1';
	frm.method="post";
	frm.submit();
}

// 수정화면으로 이동
function goModi(prpsMastKey, dealStatFlag){
	
	if(dealStatFlag!='0')
	{
		alert("상세처리상태가 '신청'이상 진행된경우는 수정이 불가능합니다."); return;
	}
	
	// 수정가능 URL
	url = "/rsltInqr/rsltInqr.do?method=isValidDealStat&actFlagYn=Y&prpsMastKey=" + prpsMastKey;
	
	if(cfGetBooleanResponseReload(url) == false)
	{
		alert("상세처리상태가 '신청'이상 진행된경우는 수정이 불가능합니다.");
		return;
	}
	else
	{
		var frm = document.prpsForm;
		
		frm.PRPS_MAST_KEY.value = prpsMastKey;
		
		frm.method = "post";
		frm.action = "/rsltInqr/rsltInqr.do?method=rghtPrpsModi&DIVS=M";
		frm.submit();		
	}
}

// 삭제
function goDelt(prpsMastKey, dealStatFlag){
	
	if(dealStatFlag!='0')
	{
		alert("상세처리상태가 '신청'이상 진행된경우는 삭제가 불가능합니다."); return;
	}
	
	if( confirm("삭제 하시겠습니까?") )
	{
		// 삭제가능 URL
		url = "/rsltInqr/rsltInqr.do?method=isValidDealStat&actFlagYn=Y&prpsMastKey=" + prpsMastKey;
		
		if(cfGetBooleanResponseReload(url) == false)
		{
			alert("상세처리상태가 '신청'이상 진행된경우는 삭제가 불가능합니다.");
			return;
		}
		else
		{
			var frm = document.prpsForm;
			
			frm.PRPS_MAST_KEY.value = prpsMastKey;
			
			frm.method = "post";
			frm.action = "/rsltInqr/rsltInqr.do?method=deleteRslt&DIVS=10&page_no=1";
			frm.submit();		
		}
	}
}

// 파일다운로드
	function fn_fileDownLoad(filePath, fileName, realFileName) {
		
		var frm = document.prpsForm;
		
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.target="rghtPrpsDetl_musc";
		//frm.action = "/rsltInqr/rsltInqr.do?method=fileDownLoad";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.method="post";
		frm.submit();
  }
  
 function initParameter(){
 	// 토탈건수
 	setTotCnt( "iChk", "totalRow");
	//scrollSet("listTab", "div_scroll_1", "iChk");
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(3);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_3.png" alt="신청현황조회" title="신청현황조회" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li class="active"><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1">권리찾기<span>&lt;</span></a></li>
							<li><a href="/rsltInqr/rsltInqr.do?DIVS=20&amp;page_no=1">보상금<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>신청현황조회</span>&gt;<strong>권리찾기</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>권리찾기 신청현황조회
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
										
					<div class="bbsSection">
						<h4>음악권리찾기 신청결과 상세</h4>
						<form name="prpsForm" action="">
							<input type="hidden" name="PRPS_MAST_KEY" />
							<!-- 파일다운로드 -->
							<input type="hidden" name="filePath" />
							<input type="hidden" name="fileName" />
							<input type="hidden" name="realFileName" />
							
							<fieldset>
								<legend></legend>
								<!-- 테이블 영역입니다 -->
								<div class="tabelRound">
									<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="10%">
										<col width="10%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row" rowspan="2" class="bgbr lft">신청정보</th>
												<th scope="row" class="bgbr2 lft">신청목적</th>
												<td>${rghtPrps.PRPS_RGHT_CODE_VALUE}</td>
											</tr>
											<tr>
												<th scope="row" class="bgbr2 lft">신청인 정보</th>
												<td>
													<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="신청인정보로 성명, 주민번호,전화번호,이메일주소 및 주소등을 입력하는 폼입니다.">
													<colgroup>
														<col width="15%">
														<col width="*">
														<col width="20%">
														<col width="30%">
														</colgroup>
														<tbody>
															<tr>
																<th class="bgbr3 lft">성명</th>
																<td>${rghtPrps.USER_NAME}</td>
																<th class="bgbr3 lft">주민번호/사업자번호</th>
																<td>${rghtPrps.RESD_CORP_NUMB}</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">전화</th>
																<td>
																	<label class="labelBlock"><em class="w45">자택</em>${rghtPrps.HOME_TELX_NUMB}</label>
																	<label class="labelBlock"><em class="w45">사무실</em>${rghtPrps.BUSI_TELX_NUMB}</label>
																	<label class="labelBlock"><em class="w45">휴대폰</em>${rghtPrps.MOBL_PHON}</label>
																</td>
																<th class="bgbr3 lft">Fax</th>
																<td>${rghtPrps.FAXX_NUMB}</td>
															</tr>
															<tr>
																<th class="bgbr3 lft"><label for="sch4">이메일주소</label></th>
																<td colspan="3">${rghtPrps.MAIL}</td>
															</tr>
															<tr>
																<th class="bgbr3 lft">주소</th>
																<td colspan="3">
																	<label class="labelBlock"><em class="w45">자택</em>${rghtPrps.HOME_ADDR}</label>
																	<label class="labelBlock"><em class="w45">사무실</em>${rghtPrps.BUSI_ADDR}</label>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">신청 신탁관리단체</th>
												<td>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_201" value="201" class="inputRChk" <c:if test="${rghtPrps.CHK_201 == '1'}">checked</c:if>  disable/>음저협</label>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_203" value="203" class="inputRChk  ml10" <c:if test="${rghtPrps.CHK_203 == '1'}">checked</c:if> disable/>음제협</label>
													<label><input type="checkbox" name="TRST_ORGN_CODE" id="CHK_202" value="202" class="inputRChk  ml10" <c:if test="${rghtPrps.CHK_202 == '1'}">checked</c:if> disable/>음실연</label>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">신청저작물 <span id="totalRow"></span></th>
												<td>	
													
													<c:if test="${rghtPrps.PRPS_RGHT_CODE == '1' || rghtPrps.PRPS_RGHT_CODE == '2'}">
													
													<!-- 테이블 영역입니다 -->
													<div id="div_1" class="tabelRound overflow_y h200 w100"> 
														<table id="listTab" cellspacing="0" cellpadding="0" border="1" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
																<col width="10" />
																<col width="80" />
																<col width="80" />
																<col width="30" />
																<col width="30" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
																<col width="50" />
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH">저작물명</th>
																	<th scope="col" class="headH">앨범명</th>
																	<th scope="col" class="headH">실연시간</th>
																	<th scope="col" class="headH">발매일</th>
																	<th scope="col" class="headH">가사</th>
																	<th scope="col" class="headH">작사</th>
																	<th scope="col" class="headH">작곡</th>
																	<th scope="col" class="headH">편곡</th>
																	
																	<th scope="col" class="headH">가창</th>
																	<th scope="col" class="headH">연주</th>
																	<th scope="col" class="headH">지휘</th>
																	
																	<th scope="col" class="headH">앨범제작</th>
																</tr>
															</thead>
															<tbody>
															<c:if test="${!empty workList}">
																<c:forEach items="${workList}" var="workList">
																	<c:set var="NO" value="${workList.totalRow}"/>
																	<c:set var="i" value="${i+1}"/>
																<tr>
																	<td class="ce">${i}</td>
																	<td>
																		<c:if test="${workList.PRPS_IDNT_CODE == '1'}">
																		<a href="javascript:openMusicDetail('${workList.PRPS_IDNT }','${workList.PRPS_IDNT_NR }','${workList.PRPS_IDNT_ME }');"><u>${workList.TITLE }</u></a>
																		</c:if>
																		<c:if test="${workList.PRPS_IDNT_CODE != '1'}">
																		[${workList.PRPS_IDNT_CODE_VALUE }]${workList.TITLE }
																		</c:if>
																	</td>
																	<td>${workList.ALBUM_TITLE }</td>
																	<td class="ce">${workList.PERF_TIME }</td>
																	<td class="ce">${workList.ISSUED_DATE }</td>
																	<td class="ce">${workList.LYRICS }</td>
																	
																	<td class="ce">${workList.LYRICIST }</td>
																	<td class="ce">${workList.COMPOSER }</td>
																	<td class="ce">${workList.ARRANGER }</td>
																	
																	<td class="ce">${workList.SINGER }</td>
																	<td class="ce">${workList.PLAYER }</td>
																	<td class="ce">${workList.CONDUCTOR }</td>
																	<td class="ce">${workList.PRODUCER }
																		<input type="hidden" name="iChk">
																	</td>
																</tr>
																
																</c:forEach>
															</c:if>
															</tbody>
														</table>
													</div>
													<!-- //테이블 영역입니다 -->
													</c:if>

													<c:if test="${rghtPrps.PRPS_RGHT_CODE == '3' || rghtPrps.PRPS_RGHT_CODE == null}">													
													<!-- 테이블 영역입니다 -->
													<div id="div_1" class="tabelRound overflow_y h100 w100"> 
														<table id="listTab" cellspacing="0" cellpadding="0" border="1" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
															<colgroup>
															<col width="10" />
															<col width="10" />
															<col width="80" />
															<col width="80" />
															<col width="30" />
															<col width="30" />
															<col width="100" />
															</colgroup>
															<thead>
																<tr>
																	<th scope="col" class="headH">순번</th>
																	<th scope="col" class="headH">저작물명</th>
																	<th scope="col" class="headH">앨범명</th>
																	<th scope="col" class="headH">연주시간</th>
																	<th scope="col" class="headH">발매일</th>
																	<th scope="col" class="headH">가사</th>
																</tr>
															</thead>
															<tbody>
																<c:if test="${!empty workList}">
																<c:forEach items="${workList}" var="workList">
																	<c:set var="NO" value="${workList.totalRow}"/>
																	<c:set var="i" value="${i+1}"/>
																<tr>
																	<td class="ce">${i}</td>
																	<td>
																		<c:if test="${workList.PRPS_IDNT_CODE == '1'}">
																		<a href="javascript:openMusicDetail('${workList.PRPS_IDNT }','${workList.PRPS_IDNT_NR }','${workList.PRPS_IDNT_ME }');"><u>${workList.TITLE }</u></a>
																		</c:if>
																		<c:if test="${workList.PRPS_IDNT_CODE != '1'}">
																		[${workList.PRPS_IDNT_CODE_VALUE }]${workList.TITLE }
																		</c:if>
																	</td>
																	<td>${workList.ALBUM_TITLE }</td>
																	<td class="ce">${workList.PERF_TIME }</td>
																	<td class="ce">${workList.ISSUED_DATE }</td>
																	<td class="ce">${workList.LYRICS }
																		<input type="hidden" name="iChk">
																	</td>
																</tr>
																</c:forEach>
															</c:if>
															</tbody>
														</table>
													</div>
													<!-- //테이블 영역입니다 -->
													</c:if>
													
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">신청내용</th>
												<td>
													<p class="mt10">${rghtPrps.PRPS_DESC}</p>
												</td>
											</tr>
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">첨부파일(증빙서류)</th>
												<td>
													<c:if test="${!empty fileList}">
														<c:forEach items="${fileList}" var="fileList">
															<c:set var="NO" value="${fileList.totalRow}"/>
															<c:set var="k" value="${k+1}"/>
														<span class="block">${k}. <a href="javascript:fn_fileDownLoad('${fileList.FILE_PATH}','${fileList.FILE_NAME}','${fileList.REAL_FILE_NAME}');" class="blue underline">${fileList.FILE_NAME}</a></span>
														</c:forEach>
													</c:if>
												</td>
											</tr>
											<!-- 2010.08.16 : 정확한 데이터입력이 어려움으로 인한 기능제거 (from 관련기관회의) START -->
											<!-- 
											<tr>
												<th scope="row" colspan="2" class="bgbr lft">이용허락 저작재산권</th>
												<td>
													<label class="label_inBlock w20"><input type="checkbox" />복제권</label>
													<label class="label_inBlock w20"><input type="checkbox" />배포권</label>
													<label class="label_inBlock w20"><input type="checkbox" />대여권</label>
													<label class="label_inBlock w20"><input type="checkbox" />공연권</label><br />
													
													<label class="label_inBlock"><input type="checkbox" />공중송신권</label> (<label class="label_inBlock"><input type="checkbox" />방송권</label><label class="label_inBlock ml5"><input type="checkbox" />전송권</label><label class="label_inBlock ml5"><input type="checkbox" />디지털음성송신권</label>)<br />
													
													<label class="label_inBlock w20"><input type="checkbox" />전시권</label>
													<label class="label_inBlock w30"><input type="checkbox" />2차적저작물작성권</label>
												</td>
											</tr>
											 -->
											<!-- 2010.08.16 : 정확한 데이터입력이 어려움으로 인한 기능제거 (from 관련기관회의) END -->
										</tbody>
									</table>
								</div>
								<!-- //테이블 영역입니다 -->
							</fieldset>
						</form>
					</div>
					
					<div class="bbsSection">
						<h4>권리찾기 신청처리 결과</h4>
						<!-- 테이블 영역입니다 -->
						<div class="tabelRound">
							<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="35%">
								<col width="35%">
								<col width="*">
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="headH">저작물명</th>
										<th scope="col" class="headH">신탁관리단체</th>
										<th scope="col" class="headH">처리상태</th>
									</tr>
								</thead>
								<tbody>
									
								<c:if test="${!empty rsltList}">
									<c:forEach items="${rsltList}" var="rsltList">
										<c:set var="kk" value="${kk+1}"/>
										<c:set var="NO" value="${rsltList.totalRow}"/>
										
									<tr>
										<c:if test="${rsltList.TITLE!=null}">
										<th rowspan="${rsltList.CNT}" class="bgbr lft ce">${rsltList.TITLE }</th>
										</c:if>
										<td class="ce">${rsltList.TRST_ORGN_CODE_VALUE }</td>
										<td class="ce">
											<c:if test="${rsltList.DEAL_STAT == '4'}">
											<span class="orange" style="cursor:hand" onclick="javascript:openDetlDesc('${rsltList.PRPS_MAST_KEY }', '${rsltList.PRPS_IDNT_CODE }', '${rsltList.PRPS_IDNT }', '${rsltList.PRPS_IDNT_NR }', '${rsltList.PRPS_IDNT_ME }', '${rsltList.TRST_ORGN_CODE }');"><u>${rsltList.DEAL_STAT_VALUE}</u></span>
											</c:if>
											<c:if test="${rsltList.DEAL_STAT != '4'}">
											<span class="blue">${rsltList.DEAL_STAT_VALUE}</span>
											</c:if>
										</td>
									</tr>
									
									</c:forEach>
								</c:if>
								
								</tbody>
							</table>
						</div>
						<!-- //테이블 영역입니다 -->
					</div>
					
					<div class="floatDiv mt10">
						<p class="fl"><span class="button medium icon"><span class="list"></span><a href="javascript:goList()">목록</a></span></p>
						<p class="fr">
							<span class="button medium icon"><span class="default"></span><a href="javascript:goModi('${rghtPrps.PRPS_MAST_KEY}','${rghtPrps.DEAL_STAT_FLAG}');">권리찾기 수정</a></span> 
							<span class="button medium icon"><span class="del"></span><button type="button" onclick="javascript:goDelt('${rghtPrps.PRPS_MAST_KEY}','${rghtPrps.DEAL_STAT_FLAG}');">권리찾기 삭제</button></span>
						</p>
					</div>
						
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/js/2010/script.js"></script>
</body>
</html>
