<%@ page contentType="text/html;charset=euc-kr" %>
<%//@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
  String login = request.getParameter("login") == null ? "" : request.getParameter("login");
%> 
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작물 내권리찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
<script src="/js/Function.js" type="text/javascript"></script>

<%@ include file="/include/sg_include.jsp" %>

<script type="text/javascript">
	
function fn_certInfoOpen() {
	//window.open('/main/main.do?method=goCertPage','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=800, height=550');
	window.open('http://copyright.signra.com/','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=780, height=550');
}

<!--
// 사업자등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginCrnChk(obj){
	// 사업자등록번호 앞자리의 길이를 확인하여 3자리, 2자리가 입력되면 다음 객체로 이동
	if(obj.name == "crnNo1"){
		if(obj.value.length == 3)	document.form1.crnNo2.focus();
	} else if(obj.name == "crnNo2"){
		if(obj.value.length == 2)	document.form1.crnNo3.focus();
	}

	// EnterKey 입력시 공인인증서 로그인 수행
	if (event.keyCode == 13) {
		fn_signLogin();
	}
}

// 공인인증서 로그인 절차 수행
function fn_signLogin(){

}

// Enter Key 입력시 로그인 수행 (아이디/비밀번호)
function fn_loginChk(obj){

	// EnterKey 입력시 로그인 수행
	if (event.keyCode == 13) {
		fn_login();
	}
}

// 로그인 절차 수행
function fn_login(){
	var frm = document.form1;

  if(frm.userIdnt.value == ""){
  	alert("아이디가 입력되지 않았습니다.");
   	frm.userIdnt.focus();
   	return;
  } else if(frm.pswd.value == ""){
   	alert("비밀번호가 입력되지 않았습니다.");
   	frm.pswd.focus();
   	return;
  } else {
//  	frm.method = "goUserLogin";
    frm.loginDivs.value = "N";
		frm.action = "/userLogin/userLogin.do";
		frm.submit();
  }
}

// 공인인증 로그인 유형 선택시 관련 Object 활성화
function fn_selectCertLogin(){
	var frm = document.form1;

	if(frm.certLoginCd[0].checked){
		// 사업자번호 관련 Object 활성화
		frm.crnNo1.disabled = false;
		frm.crnNo2.disabled = false;
		frm.crnNo3.disabled = false;

		// 주민등록번호 관련 Object 비활성화
		frm.ssnNo1.disabled = true;
		frm.ssnNo2.disabled = true;

		fn_showHideLayers('crn');
	} else {
		// 사업자번호 관련 Object 비활성화
		frm.crnNo1.disabled = true;
		frm.crnNo2.disabled = true;
		frm.crnNo3.disabled = true;

		// 주민등록번호 관련 Object 활성화
		frm.ssnNo1.disabled = false;
		frm.ssnNo2.disabled = false;

		fn_showHideLayers('ssn');
	}
}

// 주민등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginSsnChk(obj){
	// 주민등록번호 앞자리의 길이를 확인하여 6자리가 입력되면 다음 객체로 이동
	if(obj.name == "ssnNo1"){
		if(obj.value.length == 6)	document.form1.ssnNo2.focus();
	}

	// EnterKey 입력시 공인인증서 로그인 수행
	if (event.keyCode == 13) {
		fn_signLogin();
	}
}

function fn_showHideLayers(showCase){
	var frm = document.form1;
	frm.certLoginCd.value = showCase;

	if(showCase == "ssn") {
		// 개인 사용자
		fn_SetStyleDisplayObject(tbl_ssnLogin, 'inline');
		fn_SetStyleDisplayObject(tbl_crnLogin, 'none');
		frm.ssnNo1.focus();
	} else {
		// 사업자 사용자
		fn_SetStyleDisplayObject(tbl_crnLogin, 'inline');
		fn_SetStyleDisplayObject(tbl_ssnLogin, 'none');
		frm.crnNo1.focus();
	}
}

function fn_SetStyleDisplayObject(object, value){
	object.style.display = value;
}

function init() {
	var frm = document.form1;

	frm.userIdnt.focus();
}

function fn_signLogin()
{
	var frm = document.form1;
	
	// 사용자ID, 비밀번호 항옥을 초기화
	frm.userIdnt.value = "";
	frm.pswd.value = "";
	
	var strCertID = "<%=strCertId%>"; // 사용자가 선택한 인증서를 KICA SecuKit JavaScript API에서 추적하는데 사용할 값
	frm.certId.value = strCertID;
	
	//alert("CertID : "+strCertID);
	//alert("Challenge : "+frm.Challenge.value);
	//alert("ServerKmCertPem : "+frm.ServerKmCert.value);
	var strChallenge = frm.Challenge.value;
	var strServerKmCertPem = frm.ServerKmCert.value;

	// 공인인증서 로그인 선택에 따라 로그인 절차를 수행한다.
	var strCertLoginCd = "";

	if(frm.certLoginCd[0].checked){
		strCertLoginCd = frm.certLoginCd[0].value;
	} else if(frm.certLoginCd[1].checked){
		strCertLoginCd = frm.certLoginCd[1].value;
	} else {
		alert("공인인증서 유형(사업자/개인)을 선택하여 주시기 바랍니다.");
		return;
	}
	
	var strCertLoginNo = "";
	
	if(strCertLoginCd == "<%=LoginConstants.CERT_LOGIN_CD_SSN%>"){
		// 개인사용자, 외국인, 임의단체(학회) 의 경우 개인인증서를 이용한 로그인
		// 사용자 입력 값 (주민등록번호)
		var strUserSSN = frm.ssnNo1.value + frm.ssnNo2.value;
		frm.ssnNo.value = strUserSSN;
		
		if(strUserSSN == ""){
			alert("주민등록(외국인)번호가 입력되지 않았습니다.");
			frm.ssnNo1.value = "";
			frm.ssnNo2.value = "";
			frm.ssnNo1.focus();
			return;
		}
		
		var strSex = strUserSSN.substring(6,7) ;
		if(strSex  == '1' || strSex == '2' || strSex =='3' || strSex == '4'){
			// 내국인
			// 주민번호 유효성 확인
			if(!fn_ssnNumChk(strUserSSN)){
				alert("올바른 주민등록번호가 아닙니다.\n 주민등록번호를 확인하시기 바랍니다.");
				frm.ssnNo1.value = "";
				frm.ssnNo2.value = "";
				frm.ssnNo1.focus();
				return;
			}
		} else {
			// 외국인
			// 외국인번호 유효성 확인
			if(!fn_fgnNumChk(strUserSSN)){
				alert("올바른 외국인번호가 아닙니다.\n 외국인번호를 확인하시기 바랍니다.");
				frm.ssnNo1.value = "";
				frm.ssnNo2.value = "";
				frm.ssnNo1.focus();
				return;
			}
		}
		
		strCertLoginNo = strUserSSN;
		
	} else {	
		// 개인사업자, 법인사업자인 경우 사업자 공인인증서를 이용한 로그인
		// 사용자 입력 값 (사업자등록번호)
		var strUserCrnNo = frm.crnNo1.value + frm.crnNo2.value + frm.crnNo3.value;
		frm.crnNo.value = strUserCrnNo;
		
		if(strUserCrnNo == ""){
			alert("사업자등록번호가 입력되지 않았습니다.");
			frm.crnNo1.value = "";
			frm.crnNo2.value = "";
			frm.crnNo3.value = "";
			frm.crnNo1.focus();
			return;
		}
			
		// 사업자등록번호 유효성 확인
		if(!fn_bizNumChk(strUserCrnNo)){
			alert("올바른 사업자등록번호가 아닙니다.\n 사업자등록번호를 확인하시기 바랍니다.");
			frm.crnNo1.value = "";
			frm.crnNo2.value = "";
			frm.crnNo3.value = "";
			frm.crnNo3.focus();
			return;
		}
		
		strCertLoginNo = strUserCrnNo;
	}
	
	// 서버와 암호화 통신을 하기 위하여 사용할 비밀키를 추적하는데 사용할 값 확인
	if ( strServerKmCertPem == "" )
	{
		alert( "암호화에 사용할 인증서가 없습니다.\n서버 인증서 경로를 확인하십시오." );
		return;
	}
	
	// 데이터를 암호화 할 때 사용할 비밀키를 생성하고, 서버의 인증서(공개키)로 암호화하여 반환한다.
	// 이때 생성한 비밀키를 추적하기 위하여 strCertID 값이 필요하다.
	var strEncryptedSessionKey = fn_encryptSessionKey( strCertID, strServerKmCertPem );
	if ( strEncryptedSessionKey == "" )
	{
		alert( "서버 인증서로 암호화 과정중 오류가 발생하였습니다.\n시스템 담당자에게 문의하시기 바랍니다." );
		return;
	}
	frm.encryptedSessionKey.value = strEncryptedSessionKey;
	
	// 주민등록번호/사업자등록번호를 암호화 한다.
	var strEncryptedUserSSN = fn_encryptStrData(strCertID, strCertLoginNo);
	frm.encryptedUserSSN.value = strEncryptedUserSSN;
	
	// 사용자가 전자서명을 할 때 사용할 인증서를 선택하고 비밀번호를 입력할 수 있는 창을 띄운다.
	// 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
	var bReturn = reselectCertificate( strCertID );
	if ( !bReturn )
	{
		var strErr = getErrorString(); // 오류 메시지를 얻는다.
		if ( strErr == "" ) // 사용자가 인증서 선택을 취소할 경우 오류메시지는 ""이다.
		{
			alert( "암호화에 사용할 인증서가 없습니다.\n인증서를 선택하시기 바랍니다." );
			return;
		}
		else // 그 밖의 오류 처리
		{
	  		alert( getErrorFunctionName() + ": " + strErr );
			return;
		}
	}
		
	// 위에서 사용자가 선택한 인증서(전자서명용 개인키)로 전자서명 값을 생성한다.
	// 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
	var strOriginalMessage = strChallenge + strCertLoginNo;
	var strUserSignValue = fn_genDigitalSign( strCertID, strOriginalMessage );
	if ( strUserSignValue == "" )
	{
		clearCertificateInfo( strCertID ); // 전자서명 생성에 실패할 경우에 사용자가 선택했던 인증서 정보를 메모리에서 지운다. // 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
		alert( "선택한 인증서로 전자서명 값 생성에 실패하였습니다.\n인증서를 다시 선택하시기 바랍니다." );
		return;
	}
	frm.userSignValue.value = strUserSignValue;
		
	// 위에서 사용자가 선택한 전자서명용 인증서를 얻는다. // 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
	var strSignCert = fn_selectUserSignCert(strCertID);	// 사용자 인증서
	if ( strSignCert == "" )
	{
		alert( "암호화에 사용할 인증서가 없습니다.\n인증서를 선택하시기 바랍니다." );
		return;
	}
	frm.userSignCert.value = strSignCert;
	
	var strRandomNumber = fn_createRandomNumber(strCertID);	// 인증서 소유자 확인을 위한 정보 생성
	
	// 선택된 인증서와 주민등록번호를 비교하여 소유자인지 확인
	if(fn_checkUserSSNCertOwner(strSignCert, strCertLoginNo, strRandomNumber)){
	
		// 인증서 소유자 확인을 위한 정보를 암호화 한다.
		var strEncryptedUserRandomNumber = fn_encryptUserRandomNumber( strCertID, strRandomNumber );
		if ( strEncryptedUserRandomNumber == "" )
		{
			alert( "인증서 소유자 확인을 위한 정보 암호화에 실패하였습니다.\n인증서를 선택하시기 바랍니다." );
			return;
		}
		frm.encryptedUserRandomNumber.value = strEncryptedUserRandomNumber;
		
		// 로그인을 수행하도록 DWR : User Submit을 호출
//		var LogInDTO = {} ;
//    LogInDTO.certLoginCd = frm.certLoginCd.value;
//    if(strCertLoginCd == "LoginConstants.CERT_LOGIN_CD_SSN"){
//    	LogInDTO.ssnNo = frm.ssnNo.value;
//    } else {
//    	LogInDTO.crnNo = frm.crnNo.value;
//    }
//    LogInDTO.certId = frm.certId.value;
//    LogInDTO.userSignCert = frm.userSignCert.value;
//    LogInDTO.userSignValue = frm.userSignValue.value;
//    LogInDTO.encryptedSessionKey = frm.encryptedSessionKey.value;
//    LogInDTO.encryptedUserRandomNumber = frm.encryptedUserRandomNumber.value;
//    LogInDTO.encryptedUserSSN = frm.encryptedUserSSN.value;
//	    
//    UserCert.LogInProcessCert(LogInDTO, function(results){
	    	
//		if(!results){
//			alert('해당 사용자 정보가 존재하지 않습니다.\n주민등록번호/사업자등록번호를 확인하세요');
//			return;
//	    } else {
//		    	var frm = document.form;
//				frm.method = "post";
//				frm.action = "${ctx}/user/userLogIn.do?leftMenuId=03";
//				frm.submit();

    frm.loginDivs.value = "Y";
    frm.action = "/userLogin/userLogin.do";
		frm.submit();

  }
}
-->
</script>
</head>
<body class="subBg" onLoad="init();">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp"/>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
	<form name="form1" method="post" onsubmit="return validateLogin(this)">
		<input type="hidden" name="login" value="Y">
		<!-- 로그인 사용자 인증 데이터를 매번 다르게 하기 위하여 WAS 서버에서 랜덤하게 생성한 nonce 값 -->
	  <input type="hidden"	name="Challenge"	value="<%=strChallenge%>">
	  <!-- 대칭키를 RSA 알고리즘으로 암호화하기 위한 WAS 서버의 암호화용 인증서  -->
	  <input type="hidden"	name="ServerKmCert"	value="<%=ServerKmCertPem%>"> 
	  
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','122');
			</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="memberTlt">회원</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>회원</li>
					<li class="on">로그인</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>로그인</h2>
			<div id="contentsDiv">
				<!--로그인_all start-->
				<div class="login">
					<!--일반 로그인 start-->
					<div class="userLognBox">
						<!--항목 start-->
						<dl>
							<dt><label for="userIdnt">아이디</label></dt>
							<dd><input type="text" class="input" id="userIdnt" name="userIdnt" size="20" onKeyUp="fn_loginChk(this)"/></dd>
							<dt><label for="pswd">비밀번호</label></dt>
							<dd><input type="password" class="input" id="pswd" name="pswd" size="20" onKeyUp="fn_loginChk(this)"/></dd>
						</dl>
						<!--항목 end-->
						<!--로그인버튼 start-->
						<div class="floatR"><a href="javascript:fn_login();"><img src="/images/button/login_btn.gif" alt="로그인" /></a></div>
						<!--로그인버튼 end-->
					</div>
					<!--일반 로그인 end-->
					<!--사업자번호 로그인 start-->
					<div class="userCetiLognBox">
						<!--항목 start-->
						<!--type 선택 start-->
						<div class="CheckBg C"><input type="radio" name="certLoginCd" id="certLoginCd" value="crn" class="chk" onClick="fn_selectCertLogin();" checked /><label for="radio1">사업자</label>&nbsp;<input type="radio" name="certLoginCd" id="certLoginCd" value="ssn" class="chk" onClick="fn_selectCertLogin();"/><label for="radio1">개인</label></div>
						<!--type 선택 end-->
						<dl id="tbl_crnLogin" style="display: inline;">
							<dt><label for="crnNo">사업자번호</label></dt>
							<dd><input type="text" id="crnNo1" name="crnNo1" tabindex="6" class="input" size="3" maxlength="3" onKeyPress="fn_numKeyChk()" onKeyUp="fn_signLoginCrnChk(this)" style="ime-mode:disabled" />
								  -
								  <input type="text" id="crnNo2" name="crnNo2" tabindex="7" class="input" size="2" maxlength="2" onKeyPress="fn_numKeyChk()" onKeyUp="fn_signLoginCrnChk(this)" style="ime-mode:disabled" />
								  -
								  <input type="text" id="crnNo3" name="crnNo3" tabindex="8" class="input" size="5" maxlength="5" onKeyPress="fn_numKeyChk()" onKeyUp="fn_signLoginCrnChk(this)" style="ime-mode:disabled" />
							</dd>
						</dl>
						<dl id="tbl_ssnLogin" style="display: none;">
							<dt><label for="ssnNo">주민번호</label></dt>
							<dd><input type="text" id="ssnNo1" name="ssnNo1" tabindex="9" class="input" size="6" maxlength="6" onKeyPress="fn_numKeyChk()" onKeyUp="fn_signLoginSsnChk(this)" style="ime-mode:disabled" />
								  -
								  <input type="text" id="ssnNo2" name="ssnNo2" tabindex="10" class="input" size="7" maxlength="7" onKeyPress="fn_numKeyChk()" onKeyUp="fn_signLoginSsnChk(this)" style="ime-mode:disabled" />
							</dd>
						</dl>
						<!--항목 end-->
						<!--공인인증서로그인 버튼 start-->
						<div class="floatR"><a href="javascript:fn_signLogin();" tabindex="11"><img src="/images/button/publicLogin_btn.gif" alt="공인인증서 로그인" /></a></div>
						<!--공인인증서로그인 버튼 end-->
					</div>
					<!--공인인증서 로그인 end-->
				</div>
				<!--로그인_all end-->
				<ul class="clear">
					<li>다양한 서비스를 이용하기 위해서 무료회원가입을 하시기 바랍니다.&nbsp;<a href="/user/user.do?method=goPage"><img src="/images/button/join_btn.gif" alt="회원가입" width="76" height="23" align="middle" /></a></li>
					<li>회원 아이디/ 비밀번호를 잊으셨나요?&nbsp;<a href="/user/user.do?method=goIdntPswdSrch"><img src="/images/button/idpwSrch_btn.gif" alt="아이디/비밀번호찾기" align="middle" /></a></li>
					<li>공인인증서 발급에 대한 안내를 보실 수 있습니다.&nbsp;<a href="javascript:fn_certInfoOpen();"><img src="/images/button/publicCertiGuide_btn.gif" alt="공인인증서발급안내" align="middle" /></a></li>
				</ul>
			</div>
		</div>
<%  if ("Y".equals(login)) {  %>
		<c:if test="${not empty errorMessage}">
			<script type="text/javascript">
      <!--
			  alert("${errorMessage}");
			//-->
			</script>
		</c:if>
<%  }  %>
		<!--contents end-->
	</div>
    <!-- 주민등록번호 -->
    <input type="hidden" name="ssnNo" />
    <!-- 사업자등록번호 -->
    <input type="hidden" name="crnNo" />
    <!-- 사용자가 선택한 인증서를 KICA SecuKit JavaScript API에서 추적하는데 사용할 값 -->
    <input type="hidden" name="certId" />
    <!-- 사용자가 선택한 전자서명용 인증서 정보 -->
    <input type="hidden" name="userSignCert" />
    <!-- 사용자가 선택한 인증서(전자서명용 개인키)로 전자서명 값을 생성 -->
    <input type="hidden" name="userSignValue" />
    <!-- 데이터를 암호화 할 때 사용할 비밀키를 생성하고, 서버의 인증서(공개키)로 암호화한 값 -->
    <input type="hidden" name="encryptedSessionKey" />
    <!-- 사용자가 인증서 소유자임을 확인하기 위한 값 -->
    <input type="hidden" name="encryptedUserRandomNumber" />
    <!-- 사용자 주민등록번호를 암호화한 값 -->
    <input type="hidden" name="encryptedUserSSN" />
    <input type="hidden" name="loginDivs" />
  </form>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
