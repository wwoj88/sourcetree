<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 검색 및 상당한 노력신청 저작권자 조회 공고 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />  
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<!-- 공통 스크립트 -->
<script language="javascript">

/**
 * 보고서를 구동 시킨다.
 *
 * turl			: 보고서를 실행시킬 URL
 * servlet		: 보고서를 생성할 URL
 * reportFile	: 컴파일된 보고서 서실 파일
 * process		: 프로세스 (PDF)
 * rptTitle		: 보고서 제목
 * type			: 보고서에서 사용되는 데이터 타입 (xml,json,sql)
 */
function RXEnt_ViewReport(turl, servlet, reportFile, process, rptTitle, type, xmldata, f){
	 
// 	 alert('turl['+turl
// 	             +'] servlet['+servlet
//                  +'] reportFile[]'+reportFile
//                  +'] xmldata['+xmldata+']');
	 console.log('turl['+turl // 보고서를 실행시킬 URL
             +'] servlet['+servlet // 보고서를 생성할 URL
             +'] reportFile[]'+reportFile // 컴파일된 보고서 서실 파일
             +'] xmldata['+xmldata+']'); // 보고서에서 사용되는 xml 데이터
				frm = makeForm(turl);
				frm.appendChild(addData('Servlet', servlet));
				frm.appendChild(addData('ReportFile', reportFile));
				frm.appendChild(addData('Process', process));
				frm.appendChild(addData('rptTitle', rptTitle));
// 				frm.appendChild(addData('data', data.data));
				frm.appendChild(addData('data', xmldata));
				frm.appendChild(addData('type', type));
// 				if(bw=="edge"){
// 					frm.target = "ReportView";
// 				}else{
					window.open ("","ReportView",f);
					frm.target = "ReportView";
// 				}
				frm.submit();
}
function makeForm(url){
	var f = document.createElement("form");
    f.setAttribute("method", "post");
    f.setAttribute("action", url);
    document.body.appendChild(f);
      
    return f;
}

function addData(name, value){
	var i = document.createElement("input");
	i.setAttribute("type","hidden");
	i.setAttribute("name",name);
	i.setAttribute("value",value);
	return i;
}

</script>

<!-- 증명서 종류에 따라 각각 호출 -->
<script type="text/javascript">
function SmartCert_ViewReport1() {
	
	 // :TODO 증명서에서 사용되는 xml 데이터
	/*  var xmldata = '<?xml version="1.0" encoding="euc-kr"?><Root><LData><CURRENTTIME>20.04.30.000</CURRENTTIME><CURRENTDATE>20120906</CURRENTDATE><MESSAGE>정상처리되었습니다.</MESSAGE><MSG_CD>0000</MSG_CD><RT_CD>0</RT_CD><EMP_ID>99999997</EMP_ID><EMP_DEPT>99999998</EMP_DEPT><TERM_IP>999.999.999.998</TERM_IP><MEDIA>H</MEDIA><TR_CODE>CU1125</TR_CODE><CUST_NO>2003334</CUST_NO><ISSUE_UNIT>11</ISSUE_UNIT><USE>test</USE><CURRENCY_CD>KRW</CURRENCY_CD><PRN_LANG>11</PRN_LANG><BASIC_RATE>0</BASIC_RATE><NOREMN_PRN_YN>N</NOREMN_PRN_YN><TAX_STATUS>11</TAX_STATUS><STD_DT>20120813</STD_DT><PROCESS_GB>11</PROCESS_GB><out_list><LMultiData><LData><STDPRC>STDPRCSTDPRC</STDPRC><CASH_AMT>333,333</CASH_AMT><EVAL_AMT>284,8294@@@</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>예수금</FUND_NM><ORIGN_AMT>2848294</ORIGN_AMT><ACCT_NO>888888888100</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>0</REMN_QTY></LData><LData><EVAL_AMT>393869</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>삼성 당신을 위한 코리아대표그룹 증권 제1호[주식] (A)</FUND_NM><ORIGN_AMT>267231</ORIGN_AMT><ACCT_NO>888888888101</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>262,579</REMN_QTY></LData><LData><EVAL_AMT>90296</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>신영 마라톤 증권[주식] (A형)</FUND_NM><ORIGN_AMT>90000</ORIGN_AMT><ACCT_NO>888888888102</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>101107</REMN_QTY></LData><LData><EVAL_AMT>98959</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>신영 마라톤 증권[주식] (A형)</FUND_NM><ORIGN_AMT>100000</ORIGN_AMT><ACCT_NO>888888888103</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>110807</REMN_QTY></LData><LData><EVAL_AMT>1410733</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>생명 기업가치 향상 장기 증권 자[주식] (A)</FUND_NM><ORIGN_AMT>1485150</ORIGN_AMT><ACCT_NO>888888888104</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>1538809</REMN_QTY></LData><LData><EVAL_AMT>164224</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>생명 기업가치 향상 장기 증권 자[주식] (A)</FUND_NM><ORIGN_AMT>198020</ORIGN_AMT><ACCT_NO>888888888105</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>179134</REMN_QTY></LData><LData><EVAL_AMT>667658</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>생명 파워인덱스 증권 자 1호[주식] (A1)</FUND_NM><ORIGN_AMT>694449</ORIGN_AMT><ACCT_NO>888888888106</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>725715</REMN_QTY></LData><LData><EVAL_AMT>2072000</EVAL_AMT><ADDRMRK_NM>두번째통장</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>예수금</FUND_NM><ORIGN_AMT>2072000</ORIGN_AMT><ACCT_NO>888888888700</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>0</REMN_QTY></LData><LData><EVAL_AMT>200000</EVAL_AMT><ADDRMRK_NM>안내문구수정2</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>예수금</FUND_NM><ORIGN_AMT>200000</ORIGN_AMT><ACCT_NO>888888888900</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>0</REMN_QTY></LData></LMultiData></out_list><SUM_CHECK_RECP_AMT>0</SUM_CHECK_RECP_AMT><SUM_BALANCE_AMOUNT_USD>0</SUM_BALANCE_AMOUNT_USD><SUM_CASH_AMT>5120294</SUM_CASH_AMT><SUM_BALANCE_AMOUNT>7946033</SUM_BALANCE_AMOUNT></LData></Root>'; */
	  /* var xmldata = '<?xml version="1.0" encoding="euc-kr"?><Root><LData><USER_IDNT>성명 출력</USER_IDNT><WORKS_TITLE>저작물 제목</WORKS_TITLE><WORKS_CNT>5</WORKS_CNT><CLMS_CNT>2</CLMS_CNT></LData></Root>'; */
	  var xmldata = '<?xml version="1.0" encoding="euc-kr"?><root><USER_IDNT>${user.userName}</USER_IDNT><WORKS_TITLE>${query}</WORKS_TITLE><WORKS_CNT>5</WORKS_CNT><CLMS_CNT>2</CLMS_CNT></root>';
	// 팝업 세팅
	 var f = "width=700,height=800,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=0,top=0";
	
	RXEnt_ViewReport(
			"http://222.231.43.109:8086/smartcert/cdoc/eform/rxscert/run/smartcert.jsp" // [공통] 증명서를 실행시킬 URL = > 해당 url ip port context 맞춰준다 예) http://localhost:9080/rxent/
			,"smartcert_service.jsp" // [공통] 증명서를 생성할 URL
			,"test/basiccert" // :TODO 컴파일된 증명서 서실 파일
			,"PDF"
			,"기본증명서"
			,"xml"
			,xmldata
			,f
			);
}

$(function(){
	
	SmartCert_ViewReport1()
}) 
</script>
</head>
 
<body>

<%-- <div>
${user.userName}${query}
<div>
	<a href='javascript:SmartCert_ViewReport1();'>
		case 2 증명서 1 - 기본증명서 레포트 열기
	</a>
</div> --%>
</div>
 
</body>
</html>

