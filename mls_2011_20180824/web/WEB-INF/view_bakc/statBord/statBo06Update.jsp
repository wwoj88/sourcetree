<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();

	String sessUserAddr = user.getUserAddr();
	String sessMoblPhon = user.getMoblPhon();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 찾기 검색 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript">
<!--
//처음 listSize(fileList갯수)값에 따른 파일 인풋 추가버튼 
$(window).load(function(){			//페이지의 로딩이 모두 끝난후
	if($('#listSize').val() >= 5){	//listSize의 값을 불러와서 
		$('#file').hide();			//갯수 체크후 파일인풋을 show or hide한다
		$('#file_add_bt').hide();
	}else{
		$('#file').show();
		$('#file_add_bt').show();
	}
	var radioValue = document.getElementById("rgstReasCd").value;
	var radio = document.getElementsByName("rgstReas");
	if(radioValue != 0){
		for(var i =0; i<=radio.length; i++){
			if(i == radioValue){
				radio[i-1].checked = true;
			}
		}
	}
});
var iRowIdx = 1;
var fileval = new Array();
var fileLength = 0;
function fn_addRow1(){
	  iRowIdx++;
	  if(iRowIdx > 5){
		  alert("첨부파일은 최대5개까지 가능합니다.");
		  iRowIdx--;
		  return;
	  }
	  var oTbl = document.getElementById("tblAttachFile");
	  var oTbody = oTbl.getElementsByTagName("TBODY")[0];
	  
	  var oTr = document.createElement("TR");
	  oTr.id="fileTr"+iRowIdx;
		//순번(선택)
		var oTd = document.createElement("TD");
		var sTag = '<input name="chkDel1" id="chkDel1_'+iRowIdx+'" type="checkbox" value='+iRowIdx+' />';
		oTd.innerHTML = sTag;
		oTd.id = "fileTd"+iRowIdx;
		oTd.style.width = '7%';
		oTd.className = 'ce';
		oTr.appendChild(oTd);

		
		//첨부파일명
		oTd = document.createElement("TD");
		oTd.colSpan = 2;
		sTag = '<input type="hidden" name="fileDelYn" id="fileDelYn_'+iRowIdx+'" />';
		sTag += '<span id="spfile'+iRowIdx+'"><input type="file" name="file'+iRowIdx+'" id="file_'+iRowIdx+'" class="inputData L w100" onkeydown="return false;" onchange="checkFile(this.id)"/></span>';
				oTd.innerHTML = sTag;
		oTr.appendChild(oTd);
		
		oTbody.appendChild(oTr);

		       
	}

function fn_delRow1(){
var oTbl = document.getElementById("tblAttachFile");
var oChkDel = document.getElementsByName("chkDel1");
var oChkDelL = document.getElementsByName("chkDelL");
var count = 0;
var checkArray = new Array();

	for(var i=0; i<oChkDel.length; i++){
	if(oChkDel[i].checked == true){
		count++;
		var checkNum="";
		checkNum += oChkDel[i].value;
		checkArray.push(checkNum);			
		iRowIdx--;
		}
	}
	for(var i=0; i<oChkDelL.length; i++){
		if(oChkDelL[i].checked == true){
			count++;
			var checkNum="";
			checkNum += oChkDelL[i].value;
			fileval.push(checkNum);			
			iRowIdx--;
		}
}
if(count == 0){
	alert("삭제하실 내용을 체크해주세요.");
}else{
	for(var i = 0 ; i < checkArray.length ; i ++){
			element = document.getElementById('fileTr'+checkArray[i]);
			element.parentNode.removeChild(element);
	}
	for(var i = 0 ; i < fileval.length ; i ++){
		element = document.getElementById('fileLTr'+fileval[i]);
		element.parentNode.removeChild(element);
}
	fileLength++;
	document.getElementById("fileLength").value=fileLength;

	}
}
function nonUpdateSub(){							 //수정 submit
	var title = document.getElementById("worksTitle");
	var anucItem1 = document.getElementById("anucItem1");
	var anucItem2 = document.getElementById("anucItem2");
	var anucItem3 = document.getElementById("anucItem3");
	var anucItem4 = document.getElementById("anucItem4");
	var anucItem5 = document.getElementById("anucItem5");
	var anucItem6 = document.getElementById("anucItem6");
	var anucItem7 = document.getElementById("anucItem7");
	var anucItem8 = document.getElementById("anucItem8");
	var anucItem9 = document.getElementById("anucItem9");
	var chkAgr = document.getElementById("chkAgr");
	 if(title.value == "" || title.value.length ==0){
		alert("저작물의 제호를 입력해주세요");
		worksTitle.focus();
	}else if(anucItem1.value=="" || anucItem1.value.length == 0){
		alert("저작재산권자를 찾는다는 취지를 입력해주세요");
		anucItem1.focus();
	}else if(anucItem2.value=="" || anucItem2.value.length == 0){
		alert("저작재산권자의 성명을 입력해주세요");
		anucItem2.focus();
	}else if(anucItem3.value=="" || anucItem3.value.length == 0){
		alert("저작재산권자의 주소를 입력해주세요");
		anucItem3.focus();
	}else if(anucItem4.value=="" || anucItem4.value.length == 0){
		alert("저작재산권자의 연락처를 입력해주세요");
		anucItem4.focus();
	}else if(anucItem5.value=="" || anucItem5.value.length == 0){
		alert("공표 시 표시된 저작재산권자의 성명을 입력해주세요");
		anucItem5.focus();
	}else if(anucItem6.value=="" || anucItem6.value.length == 0){
		alert("저작물발행을 입력해주세요");
		anucItem6.focus();
	}else if(anucItem7.value=="" || anucItem7.value.length == 0){
		alert("공표연월일을 입력해주세요");
		anucItem7.focus();
	}else if(anucItem7.value.length != 8 || anucItem7.value.indexOf('.') != -1 || anucItem7.value.indexOf('/') != -1 || anucItem7.value.indexOf('-') != -1){
		alert("공표연월일의 형식이 잘못되었습니다.");
		anucItem7.focus();
	}else if(anucItem8.value=="" || anucItem8.value.length == 0){
		alert("저작물의 이용목적을 입력해주세요");
		anucItem8.focus();
	}else if(!chkAgr.checked){
		alert("담당자 보완 항목에 동의하여주세요");
		chkAgr.focus();
	}else{
		var radio = document.getElementsByName("rgstReas");
		var rgstReasCd = document.getElementById("rgstReasCd");
		 for(var i=0; i < radio.length; i++){
				if(radio[i].checked){
					var radioValue = radio[i].value;
					rgstReasCd.value = radioValue;
				}
			}
	var fm = document.nonWriteForm;
	var genreSel = document.getElementById("genre");
	var genreVal = genreSel.options[genreSel.selectedIndex].value;
	var worksId = document.getElementById("worksId").value;
	var min = document.getElementById("numberMin").value;
	var max = document.getElementById("numberMax").value;
//----------------------------파일 삭제  --------------------------
	var fileGrp = fileval.join();
//----------------------------추가 서브밋 스크립트----------------------
 	var nameGrpArray = new Array();
 	if(max >= 1){
	 	for(var i=min; i <= max; i++){
	 		var coptHodrNm = document.getElementById("coptHodrName"+i).value;
		  	if(document.getElementById("coptHodr"+i).value !=0 && coptHodrNm != '' && coptHodrNm.length != 0){
	 			nameGrpArray.push(coptHodrNm);
	  		}else if(document.getElementById("coptHodr"+i).value !=0 && coptHodrNm == '' && coptHodrNm.length == 0){
	  			//저작권자 및 실연자 정보의 셀렉트 박스를 선택하고 텍스트박스에 값이 없을때
	  			alert("저작권자 및 실연자 정보에 맞는 정보를 입력해 주세요.");
	  			document.getElementById("coptHodrName"+i).focus();
	  			return;
  			}
	 	}
 	}
	var nameGrp = nameGrpArray.join();
	var cdGrpCdArray  = new Array();			  //저작권자 및 실연자 정보의 
	if(max >= 1){
		for(var i = min; i <= max; i++){	  		  //값을 추가된 셀렉트의 갯수만큼 
		var coptHodr = document.getElementById("coptHodr"+i).value;
			if(coptHodr == 0 && document.getElementById("coptHodrName"+i).value != '' && document.getElementById("coptHodrName"+i).value.length != 0){
				//저작권자 및 실연자 정보의 셀렉트 박스를 선택하지 않앗는데 텍스트박스에 값이 들어있는 경우
					alert("저작권자 및 실연자 정보에 대한 종류를 먼저 선택해 주세요.");
					document.getElementById("coptHodr"+i).focus();
					return;
			}else if(coptHodr != 0 && document.getElementById("coptHodrName"+i).value != '' || document.getElementById("coptHodrName"+i).value.length != 0){
				cdGrpCdArray.push(coptHodr);			 	  //배열에 담는다
			}
		}
	}
	var cdGrpCdLength = cdGrpCdArray.length; 	  // 배열의 길이와
	var cdGrpCd  = cdGrpCdArray.join(); 	 	  // 배열의 값을 스트링값으로 변환하여 
	
		$('#coptHodrRoleCd').val(cdGrpCd);		  //히든에 담아서 Submit
		$('#coptHodrName').val(nameGrp);
		$('#coptHodrsLength').val(cdGrpCdLength);
		$('#genreCd').val(genreVal);
		$('#fileCheck').val(fileGrp);
		$('#fileLength').val(fileLength);
		fm.action="/statBord/statBo06UpdateProc.do?worksId="+worksId;
		fm.submit();
		}
	}

function addSelect(){
	var value=document.getElementById("genre").value;
	var bigCode = 0;
	var numbers = document.getElementById("numberMax").value;

	if(value == 1){bigCode = 62;}
	else if(value==2){bigCode = 63;}
	else if(value==3){bigCode = 64;}
	else if(value==4){bigCode = 65;}
	else if(value==5){bigCode = 66;}
	else if(value==6){bigCode = 67;}
	else if(value==7){bigCode = 68;}
	else if(value==8){bigCode = 69;}
	
	var options = {
			type		: "POST",
			url 		: '/statBord/codeList.do',
			data		: {"bigCode":bigCode},
			dataType    : "json",
			contentType : "application/x-www-form-urlencoded;charset=euc-kr",
			success		: function(data){
				var seleop = '';
				numbers++;
				document.getElementById("numberMax").value = numbers;
				if(numbers == 1){
					seleop  += '<p id="selP'+numbers+'"><input type="checkbox" id="ck'+numbers+'" name="checkCopt" value="'+numbers+'"/>&nbsp;';
					seleop  += '<select class="inputData" name="coptHodr'+numbers+'"id="coptHodr'+numbers+'"></select>&nbsp;';
					seleop  += '<input type="text" class="inputData" id="coptHodrName'+numbers+'" name="coptHodrName'+numbers+'"/></p>';
				}else{
					seleop  += '<p id="selP'+numbers+'"><input type="checkbox" id="ck'+numbers+'" name="checkCopt" value="'+numbers+'"/>&nbsp;';
					seleop  += '<select class="inputData" name="coptHodr'+numbers+'"id="coptHodr'+numbers+'"></select>&nbsp;';
					seleop  += '<input type="text" class="inputData" id="coptHodrName'+numbers+'" name="coptHodrName'+numbers+'"/></p>';	
				}
				var j = data.list;
				var options = '<option value="0">-- 선택 --</option>';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].midCode + '">' + j[i].codeName+ '</option>';
				}
				if(numbers == 1){
					document.getElementById("numberMin").value = 1;
					element = document.getElementById('coptNull');
					element.parentNode.removeChild(element);
					$("#addT").after(seleop);
					$("#coptHodr"+numbers).append(options);
				}else{
					$("#selP"+(numbers-1)).after(seleop);
					$("#coptHodr"+numbers).append(options);
				}
			}
		};$.ajax(options);
	}
function deleteSelect(){
	var ch = document.getElementsByName("checkCopt");
	var count = 0;
	var ment ='';
	var numbers = document.getElementById("numberMax").value;
	
	var checkArray = new Array();
	for(var i=0; i<ch.length; i++){
		if(ch[i].checked == true){
			count++;
			var checkNum="";
			checkNum += ch[i].value;
			checkArray.push(checkNum);			
			numbers--;
			document.getElementById("numberMax").value = numbers;
		}
	}
	if(count == 0){
		alert("삭제하실 내용을 체크해주세요.");
	}else{
		for(var i = 0 ; i < checkArray.length ; i ++){
				element = document.getElementById('selP'+checkArray[i]);
				element.parentNode.removeChild(element);
		}
		
		if(document.getElementById("numberMin").value > document.getElementById("numberMax").value){
			document.getElementById("numberMax").value = 0;
			document.getElementById("numberMin").value = 0;
			ment += '<p id=coptNull>저작권자 및 실연자 정보를 추가해주세요.</p>';
			$("#addT").before(ment);
		}
	}
}

function changeList(value){			//장르 선택시 저작권자 및 실연자 정보 입력값 변환 ajax
	var ch = document.getElementsByName("checkCopt");
	var checkArray = new Array();
	var numbers = document.getElementById("numberMax").value;	
	var ment ='';
	for(var i=0; i<ch.length; i++){
			var checkNum="";
			checkNum += ch[i].value;
			checkArray.push(checkNum);			
			numbers--;
			document.getElementById("numberMax").value = numbers;
	}
	
	for(var i = 0 ; i < checkArray.length ; i ++){
		element = document.getElementById('selP'+checkArray[i]);
		element.parentNode.removeChild(element);
	}
	document.getElementById("numberMax").value = 0;
	document.getElementById("numberMin").value = 0;
	ment += '<p id=coptNull></p>';
	$("#addT").before(ment);
	addSelect();
	var bigCode = 0;
	if(value == 1){bigCode = 62;}
	else if(value==2){bigCode = 63;}
	else if(value==3){bigCode = 64;}
	else if(value==4){bigCode = 65;}
	else if(value==5){bigCode = 66;}
	else if(value==6){bigCode = 67;}
	else if(value==7){bigCode = 68;}
	else if(value==8){bigCode = 69;}
	var options = {
			type		: "POST",
			url 		: '/statBord/codeList.do',
			data		: {"bigCode":bigCode},
			dataType    : "json",
			contentType : "application/x-www-form-urlencoded;charset=euc-kr",
			success		: function(data){
				var j = data.list;
				var options = '';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].midCode + '">' + j[i].codeName+ '</option>';
				}
				$("#coptHodr").html(options);
			    $('#coptHodr option:first').attr('selected', 'selected');
			}
		};$.ajax(options);
}
function titeSet(){	//제호 자동입력
	var tex = document.getElementById("tite").value;
	document.getElementById("worksTitle").value = tex;
}
$(document).ready(function() {
	 if ($("#contentBody").has("#now_loading").html() == null) {
	  var tmpHtml = "";
	  tmpHtml = "<div id='now_loading' style='position: absolute; top: 0; left: 0; display:none;z-index:9998'>";
	  tmpHtml += "<table width=200 height=100 border=0> ";
	  tmpHtml += "    <tr>";
	  tmpHtml += "        <td align=center><img id='ajax_load_type_img' src='/images/loading.gif'></td>";
	  tmpHtml += "    </tr>";
	  tmpHtml += "</table>";
	  tmpHtml += "</div>";

	  jQuery('#contentBody').append(tmpHtml).ajaxStart(function() {
		  var width = jQuery('#contentBody').width();
		  var height = jQuery('#contentBody').height();
			
			//화면을 가리는 레이어의 사이즈 조정
			jQuery(".backLayer").width(width);
			jQuery(".backLayer").height(height);
			
			//화면을 가리는 레이어를 보여준다 (0.5초동안 30%의 농도의 투명도)
			jQuery(".backLayer").fadeTo(100, 0.0);
	   		showLoading();
	  }).ajaxStop(function() {
	    jQuery(".backLayer").fadeOut(1000);
	    hideLoading();
	  });
	 }
	});
	jQuery(document).resize(function(){
		var width = jQuery('#wrap').width();
		var height = jQuery('#wrap').height();
		jQuery(".backLayer").width(width).height(height);
	});

function showLoading() {
	$(document).bind("mousemove" ,(function(e){
		var x = e.pageX;
		var y = e.pageY;
		var width = jQuery('#contentBody').width();
		var height = jQuery('#contentBody').height();
		
		$('#now_loading').css("left", x-460);
	    $('#now_loading').css("top", y-230);
	}));
	$('#now_loading').show();
}

function hideLoading() {
	$(document).unbind("mousemove" , function(){});
	$('#now_loading').hide();
 }

function checkFile(fileId){
	 var option = {
				 url 		: '/statBord/fileSizeCk.do',
				 type		: "post",
				 dataType   : "json",
				 data 		: jQuery('#nonWriteForm'),
				 success	: function(data){
					if(data.fileSize > 1024*1024*2){
						alert("2MB 이상 파일은 업로드 하실 수 없습니다.");
						if (jQuery.browser.msie) {
						    jQuery("#"+fileId).replaceWith( jQuery("#"+fileId).clone(true) );
						} else {
						    jQuery("#"+fileId).val("");
						}
					}
				}
	 };
		jQuery('#nonWriteForm').ajaxSubmit(option);
}
//-->
</script>
</head>
 <style>
	div.backLayer {
		display:none;
		background-color:black; 
		position:absolute;
		left:0px;
		top:0px;
	}
 </style>
<body>

	<!-- 전체를 감싸는 DIVISION -->
		<div id="wrap">
 		<!-- HEADER str-->
		<div id="header">
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/header.jsp" /> --%>
		<script type="text/javascript">initNavigation(0);</script>
		<!-- 상단 네비게이션 str -->
		</div>
	
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis6.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_6.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
		
				<!-- 래프 -->
				<div class="left">
				
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/statBord/statBo06List.do?bordCd=6"><img src="/images/2012/content/sub_lnb_off.gif" title="신청현황" alt="신청현황" />신청현황</a>
						<ul>
						<!-- 
						<li id="lnb11"><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1">저작권정보 변경신청</a></li>
						<li id="lnb12"><a href="/rsltInqr/rsltInqr.do?DIVS=20&amp;page_no=1">미분배보상금 신청</a></li>
						 -->
						<li id="lnb13"><a href="/statBord/statBo06List.do?bordCd=6">저작권자 찾기위한<br/>상당한 노력 신청</a></li>
						<li id="lnb14"><a href="/myStat/statRsltInqrList.do">법정허락 이용승인신청</a></li>
						<li id="lnb15"><a href="/statBord/statBo01ListMy.do?bordCd=1">저작권자 조회 공고</a></li>
						<li id="lnb16"><a href="/statBord/statBo05ListMy.do?bordCd=5">보상금 공탁 공고</a></li>
						</ul>
						
					</li>
					<li id="lnb2"><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>"><img src="/images/2012/content/sub_lnb_off.gif" title="회원정보" alt="회원정보" />회원정보</a>
						<ul>
						<li id="lnb21"><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보수정</a></li>
						<li id="lnb22"><a href="/user/user.do?method=selectUserInfo&amp;DIVS=D&amp;userIdnt=<%=sessUserIdnt%>">회원탈퇴</a></li>
						</ul>
					</li>
					</ul>
				</div>
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1","lnb13"); 
 				</script>
				<!-- 래프 -->

				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<div class="backLayer" style=''></div> 
					<p class="path"><span>Home</span><span>마이페이지</span><span>신청현황</span><em>저작권자 찾기위한 상당한 노력신청</em></p>
					<h1><img src="/images/2012/title/content_h1_0603.gif" alt="저작권정보 변경신청" title="저작권정보 변경신청" /></h1>
					<div class="section mt20">
					<form method="post" action="#" id="nonWriteForm" name="nonWriteForm" enctype="multipart/form-data" >
						<h2>저작권자 찾기위한 상당한 노력 신청</h2>
						<p class="mt10 mb20"><span class="line22">거소불명저작물의 저작권찾기 상당한노력에 대한 간소화를 신청합니다. </span></p>
<!-- 						<span class="button small icon"><a href="javascript:openSmplDetail('MR')">예시화면 보기</a><span class="help"></span></span></p> -->
						
					<h2>저작권자 찾기위한 상당한 노력 신청 사유</h2>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청" name="rgstReas" id="radioOne" value="1"> <span>찾고자하는 저작물이 검색결과에 없는 경우</span><br/>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청" name="rgstReas" id="radioTwe"  value="2"><span> 검색결과 제호는 동일 하나 찾고자 하는 저작물이 아닌 경우</span><br/>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청" name="rgstReas" id="radioThree"  value="3"> <span>검색결과 저작물은 존재하나 저작권자 정보가 불일치 하는 경우</span><br/>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청" name="rgstReas" id="radioFore"  value="4"> <span>찾고자하는 저작물은 존재하나 인접권자 정보가 불일치 하는 경우</span><br/>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청" name="rgstReas" id="radioFive" value="5"><span> 찾고자하는 저작물은 존재하나 관련기관 확인 후 찾고자 하는 저작물이 아닌경우</span>
							<br/>
							&nbsp;<input type="radio" title="저작권자 찾기위한 상당한 노력 신청" name="rgstReas" id="radioSix"  value="6"><span>기타</span><br/>&nbsp;&nbsp;&nbsp;
							<textarea cols="100" name="rgstReasEtc" id="rgstReasEtc">${AnucBord.rgstReasEtc}</textarea>
							<br/>
							<input type="hidden" name="rgstReasCd" id="rgstReasCd" value="${AnucBord.rgstReasCd}"/>
					
						<br/><h2>저작물 정보</h2>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							<col width="38%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col">장르</th>
									<th scope="col">제호</th>
									<th scope="col">저작권자 및 실연자 정보 <a href="javascript:addSelect()"><img src="/images/2012/button/add2.gif" alt="" title="" /></a> <a href="javascript:deleteSelect()"><img src="/images/2012/button/delete2.gif" alt="" title="" /></a></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="ce pd5"><select onchange="javascript:changeList(this.value)" id="genre"  name="genre" class="inputData w95">
											<option value="0">-- 선택 --</option>
												<c:forEach items="${codeList}" var="codeList">
												<c:if test="${AnucBord.genreCd == codeList.midCode}">
													<option selected="selected" value="${codeList.midCode}">${codeList.codeName}</option>
												</c:if>
												<c:if test="${AnucBord.genreCd != codeList.midCode}">
													<option value="${codeList.midCode}">${codeList.codeName}</option>
												</c:if>
												</c:forEach>
												</select>
									</td>
									<td class="ce pd5"><input type="text" title="제목" class="inputData w95" id="tite" onblur="javascript:titeSet()" value="${AnucBord.worksTitle}"/></td>
									<td class="ce pd5">
									<c:if test="${coptSize != 0 }">
									<c:forEach items="${coptHodr}" var="copthodr">
									<p id="selP${copthodr.coptHodrSeqn}">
									<input type="checkbox" title="Checkbox" id="ck${copthodr.coptHodrSeqn}" name="checkCopt" value="${copthodr.coptHodrSeqn}"/> 
									<select  id="coptHodr${copthodr.coptHodrSeqn}" name="coptHodr${copthodr.coptHodrSeqn}" class="inputData" >
										<option value="0">-- 선택 --</option>
												<c:forEach items="${ListCopt}" var="coptList">
												<c:if test="${copthodr.coptHodrRoleCd == coptList.midCode}">
													<option selected="selected" value="${coptList.midCode}">${coptList.codeName}</option>
												</c:if>
												<c:if test="${copthodr.coptHodrRoleCd != coptList.midCode}">
													<option value="${coptList.midCode}">${coptList.codeName}</option>
												</c:if>
												</c:forEach>
										</select>&nbsp;<input type="text" title="저작권자" class="inputData" id="coptHodrName${copthodr.coptHodrSeqn}" name="coptHodrName${copthodr.coptHodrSeqn}" value="${copthodr.coptHodrName}"/></p>
										<!-- 추가된 셀렉트가 존재한다면 -->
										<!-- numberMax와 numberMin은 새로 추가하거나 삭제할때 셀렉트의 순번에 쓰인다 -->
										<input type="hidden" id="numberMax" name="numberMax" value="${copthodr.coptHodrMax}"/>
										<input type="hidden" id="numberMin" name="numberMin" value="${copthodr.coptHodrMin}"/>
										<input type="hidden" id="addNum" name="addNum" value="0" />
										</c:forEach>
										
										</c:if>
									<!-- 셀렉트가  존재하지 않는다면 -->
									<!-- p addT는 추가될 셀렉트의 기준점이 된다 -->
									<p id="addT"></p>
									<c:if test="${coptSize== 0}">
									<p id="coptNull">저작권자 및 실연자 정보를 추가해주세요.</p>
									<input type="hidden" id="numberMax" name="numberMax" value="0"/>
									<input type="hidden" id="numberMin" name="numberMin" value="1"/>
									</c:if>
										<!-- 새로 추가된 셀렉트와 기존에 있던 셀렉트의 Cd값, 길이, 내용 을 담은 히든 -->
										<input type="hidden" id="coptHodrRoleCd" name="coptHodrRoleCd" value="" />
										<input type="hidden" id="coptHodrsLength" name="coptHodrsLength" value="0" />
										<input type="hidden" id="coptHodrName" name="coptHodrName" value="" />
										
									</td>
								</tr>
							</tbody>
						</table>
						
						<h2 class="mt20">저작권찾기 상당한노력 공고 정보</h2>
						
						<ul class="mt15">
					<li>
						<p class="blue2 line22">1. 저작권자를 찾는다는 취지</p>
						<textarea cols="10" rows="3" id="anucItem1" name="anucItem1" class="textarea w98" >${AnucBord.anucItem1}</textarea>
					</li>
					<li class="mt15">
						<p class="blue2 line22">2. 저작재산권자의 성명 또는 명칭, 주소 또는 거소 등</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm">성명</label></th>
									<td><input type="text" title="성명" id="anucItem2" name="anucItem2" class="inputData" value="${AnucBord.anucItem2}"/>
										<input type="hidden" name="worksId" id="worksId" value="${AnucBord.worksId}"/>
										
									</td>
								</tr>
								<tr>
									<th scope="row"><label for="zip">주소</label></th>
									<td><input type="text" title="주소" name="anucItem3" id="anucItem3" class="inputData w95" value="${AnucBord.anucItem3}"/></td>
								</tr>
								<tr>
									<th scope="row"><label for="phone">연락처</label></th>
									<td><input type="text" title="연락처"  name="anucItem4" id="anucItem4" class="inputData" value="${AnucBord.anucItem4}"/></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="blue2 line22">3. 저작물의 제호</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="tl">제호</label></th>
									<td>
									<input type=hidden id="genreCd" name="genreCd" value="" />
									<input type="text" title="제호" readonly="readonly" name="worksTitle" id="worksTitle" class="inputData w95" value="${AnucBord.worksTitle}"/>
									</td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="blue2 line22">4. 공표 시 표시된 저작재산권자의 성명(실명 또는 이명)</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm2">성명</label></th>
									<td><input type="text" title="성명" name="anucItem5" id="anucItem5" class="inputData" value="${AnucBord.anucItem5}"/></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="blue2 line22">5. 저작물을 발행 또는 공표한자</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="date1">저작물발행</label></th>
									<td><input type="text" title="저작물발행" name="anucItem6" id="anucItem6" class="inputData" value="${AnucBord.anucItem6}"/></td>
								</tr>
								<tr>
									<th scope="row"><label for="date2">공표연월일</label></th>
									<td><input type="text" title="공표연월일" name="anucItem7" id="anucItem7" maxlength="8" class="inputData" value="${AnucBord.anucItem7}"/> <span style="color:red">* ex ) 20120101(2012년 01월 01일)</span></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="blue2 line22">6. 저작물의 이용 목적</p>
						<textarea cols="10" rows="3"  name ="anucItem8" id="anucItem8" class="textarea w98" >${AnucBord.anucItem8}</textarea>
					</li>
					<li class="mt15">
						<p class="blue2 line22">7.복제물의 표지사진 등의 자료</p>
						<div class="floatDiv"  id="filediv">
						<div class="fr mb5">
									<p>
										<span title="추가" class="button small"><button id="btnAdd" onkeypress="" onclick="javascript:fn_addRow1();" type="button">File 추가</button></span>
										<span title="삭제" class="button small"><button id="btnDel" onkeypress="" onclick="javascript:fn_delRow1();" type="button">File 삭제</button></span>
									</p>
								</div>
							 <table id="tblAttachFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부파일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
															<colgroup>
															    <col width="8%">
															    <col width="*%">
															</colgroup>
															<thead>
															    <tr>
						    									    <th scope="row" class="ce">순번</th>
						    									    <th scope="row">첨부파일</th>
															    </tr>
															</thead>
															<tbody>
																<c:forEach items="${fileList}" var="fileList">
																	    <tr id="fileLTr${fileList.attcSeqn}">
																	    	<td class="ce" id="fileLTd${fileList.attcSeqn}" >
															    			<input name="chkDelL" id="chkDelL" type="checkbox" value="${fileList.attcSeqn}" />
															    		</td>
																	    	<td>
																    				${fileList.fileName}
																	    	</td>
																	    </tr>
													    			</c:forEach>
															    <tr id="fileTr1">
															    	<td class="ce" id="fileTd1">
															    		<input name="chkDel1" id="chkDel1" type="checkbox" value="1" />
															    	</td>
															    	<td>
																    	<input type="hidden" name="fileDelYn" />
																		<span id="spfileD2'">
																		    <input type="file" name="file" id="file" title="첨부파일" 
																		        class="inputData L w100"
																		        onkeydown="return false;" onchange="checkFile(this.id)"/>
																		</span>
															    	</td>
															    </tr>
	                                           				</tbody>
														</table>		
									<span class="p11 orange block mt5">※ 첨부파일은 최대5개까지 가능합니다.</span>
									<input type="hidden" id="fileCheck" name="fileCheck" value="" />
									<input type="hidden" id="fileLength" name="fileLength" value="0" />
									<input type="hidden" id="rgstIdnt" name="rgstIdnt" value="<%=sessUserIdnt%>"/>
						
							<p class="fl w70">
						</div>
					</li>
					<li class="mt15">
						<p class="blue2 line22">8. 공고자 및 연락처</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm3">공고자</label></th>
									<td><input type="text" title="공고자" readOnly="readonly" name="anucItem9" id="anucItem9" class="inputData" value="${AnucBord.anucItem9}"/></td>
								</tr>
								<tr>
									<th scope="row"><label for="zip3">주소</label></th>
									<td><input type="text" title="주소" readOnly="readonly" name="anucItem10" id="anucItem10" class="inputData w98" value="${AnucBord.anucItem10}" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="phone3">연락처</label></th>
									<td><input type="text" title="연락처" readOnly="readonly" name="anucItem11" id="anucItem11" class="inputData" value="${AnucBord.anucItem11}" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="admin3">담당자</label></th>
									<td><input type="text" title="담당자" name="anucItem12" id="anucItem12" class="inputData"  value="${AnucBord.anucItem12}"/></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt20">					
						<p class="strong line13">* 상당한 노력 신청 정보는 담당자에 의해 보완될 수 있습니다. (<input type='checkbox' id='chkAgr' class='mb3'/> 동의함)</p>
					</li>
					</ul>
					</form>
					<!-- 버튼 str -->	
						<div class="btnArea">
							<p class="fl"><span class="button medium gray"><a href="/statBord/statBo06Detl.do?worksId=${AnucBord.worksId}">취소</a></span></p>
							<p class="fr"><span class="button medium"><a href="javascript:;" onclick="javascript:nonUpdateSub()">수정</a></span></p>
							
						</div>
	                              <!-- //버튼 -->
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
			<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	</div>

 
</body>
</html>

