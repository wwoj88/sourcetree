<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>


<%
	User user = SessionUtil.getSession(request);
	
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	pageContext.setAttribute("UserName", sessUserName); 

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 공고 승인 신청  법정허락  공고 - 보상금공탁공고(${AnucBord.tite}) | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript">  
<!--
$(window).load(function(){
	var anucItem3 = document.getElementById("anucItem3").value;
	var anuc3Cg = anucItem3.replace(/\r\n/g, "<br>");
	jQuery('#anuc3').append(anuc3Cg);
	var anucItem6 = document.getElementById("anucItem6").value;
	var anuc6Cg = anucItem6.replace(/\r\n/g, "<br>");
	jQuery('#anuc6').append(anuc6Cg);

});
function fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.getElementById("form1");
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.target="boardView";
	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
}
//-->
</script>
<script> 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-2', 'auto');
  ga('send', 'pageview');
</script>
</head>
 
<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader3.jsp" /> --%>
		<script type="text/javascript">initNavigation(3);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
			
				<!-- 래프 -->
				<div class="con_lf">
						<div class="con_lf_big_title">저작권자 찾기</div>
						<ul class="sub_lf_menu">
							<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li>
							<li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li>
							<li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
							<li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
							<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
							<li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
							<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
							<li><a class="on" href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
							<li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li>
						</ul>
					</div>
				<!-- //래프 -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					
					<div class="con_rt_head">
						<img src="../images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						저작권자 찾기
						&gt;
						법정허락 승인 신청
						&gt;
						법정허락 공고
						&gt;
						<span class="bold">보상금 공탁 공고</span>
					</div>
					<div class="con_rt_hd_title">보상금 공탁 공고</div>
					<div class="sub_contents_con">
						<ul class="sub_menu1 w269 mar_tp40">
							<li class="first"><a href="/statBord/statBo03List.do?bordCd=3">승인 신청공고</a></li>
							<li><a href="/statBord/statBo04List.do?bordCd=4">승인 공고</a></li>
							<li class="on"><a href="/statBord/statBo05List.do?bordCd=5" class="last_rt_bor on">보상금 공탁 공고</a></li>
						</ul>
					<p class="clear"></p>
                   	   <br/><br/>
                   	  	<form name="form1" id="form1" method="post" action="">
							
							<input type="hidden" name="filePath"/>
							<input type="hidden" name="fileName"/>
							<input type="hidden" name="realFileName"/>
						
						<table border="1" cellspacing="0" cellpadding="0" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							
							<tbody>
								<tr>
									<th scope="row" class="p12"> &lt;${AnucBord.tite}&gt; 보상금 공탁 공고</th>
								</tr>
								<tr>
									<td><span class="p11">공고자 : ${AnucBord.anucItem8}</span><span class="p11 ml20">공고일 : <c:if test="${empty AnucBord.openDttm}">미공고</c:if>
								          <c:if test="${!empty AnucBord.openDttm}">${AnucBord.openDttm}</c:if></span></td>
								</tr>
								<tr>
									<td>
										<ul class="mt15">
										<li>
											<p class="blue2 line22">1. 저작물의 제호</p>
											<p class="mt5 line22">${AnucBord.anucItem1}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">2. 저작자 및 저작재산권자의 성명</p>
											<p class="mt5 line22">${AnucBord.anucItem2}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">3. 저작물 이용의 내용</p>
											<%-- <p class="mt5 line22" id="anuc3"><input type="hidden" id="anucItem3" value="${AnucBord.anucItem3} "/></p> --%>
											<p class="mt5 line22" id="anuc3">${AnucBord.anucItem3}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">4. 공탁금액</p>
											<p class="mt5 line22">${AnucBord.anucItem4}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">5. 공탁소의 명칭 및 소재지</p>
											<p class="mt5 line22">${AnucBord.anucItem5}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">6. 공탁근거</p>
											<%-- <p class="mt5 line22" id="anuc6"><input type="hidden" id="anucItem6" value="${AnucBord.anucItem6}"/></p> --%>
											<p class="mt5 line22" id="anuc6">${AnucBord.anucItem6}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">7. 저작물이용자의 주소/성명</p>
											<p class="mt5 line22">- 주소 : ${AnucBord.anucItem7}</p>
											<p class="mt5 line22">- 성명 : ${AnucBord.anucItem8}</p>
										</li>
										<li class="mt15">
											<p class="blue2 line22">8. 첨부</p>
											<table id="tblAttachFile" border="1" width="200" cellspacing="0" summary="첨부화일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
															<colgroup>
															    <col width="33%">
															</colgroup>
															<tbody>
															    <tr>
						    									    <th scope="row" style="text-align:center;">첨부파일명</th>
															    </tr>
															    <c:if test="${!empty fileList }">
													    			<c:forEach items="${fileList}" var="fileList">
																	    <tr>
																	    	<td>
																	    	<%-- 
															    				<a href="#1" onclick="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
															    					onkeypress="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
																    				${fileList.fileName }
																    				<input name="attcSeqn" id="attcSeqn_D1" value="${fileList.attcSeqn }" type="hidden" />
															    				</a>
															    			 --%>
															    		    <a href="#1" onclick="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" onkeypress="fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')"> ${fileList.fileName } <input name="attcSeqn" id="attcSeqn_D1${fileList.attcSeqn }" value="${fileList.attcSeqn }" type="hidden" />
															    			 
																	    	</td>
																	    </tr>
													    			</c:forEach>
													    		</c:if>
													    		<c:if test="${empty fileList }">
													    		<tr>
													    			<td>
													    				<p class="blue2 line22">등록된 파일이 없습니다.</p>
													    			</td>
													    		</tr>
													    		</c:if>
															</tbody>
														</table>
														<br/>
													</li>
												</ul>
											</td>
										</tr>
									</tbody>
								</table>
						<!-- //그리드스타일 -->
						</form>
						<!-- 버튼 str -->
						<p class="rgt mt10"><span class="button medium gray"><a href="/statBord/statBo05List.do?bordCd=5">목록</a></span></p>
						<!-- //버튼 -->


					</div>
						
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
				<p class="clear"></p>
		</div>
		<!-- //CONTAINER end -->		
		
		<!-- FOOTER str-->
			<jsp:include page="/include/2012/footer.jsp" />
			<!-- FOOTER end -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

