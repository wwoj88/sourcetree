<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();

	String sessUserAddr = user.getUserAddr();
	String sessMoblPhon = user.getMoblPhon();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 찾기 검색 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
var countRow; // 엑셀 데이터 ROW 번호
var dataTite; // 저작물의 제호

function nonWriteSub(){ //등록 submit
	
	var chkAgr = document.getElementById("chkAgr");
	var errorCount = $("#errorCount").val();
	
	var excelFile= $('#excel').val();
	var ext = excelFile.substring( excelFile.lastIndexOf(".")+1, excelFile.length ).toLowerCase();	
	var EXTENSION_TYPES = ['xls', 'xlsx'];	

	if(excelFile.length==0){		
		alert("업로드된 파일이 없습니다.\n[신청정보 업로드]를 다시 진행해주세요.");
		$('#excel').focus();	
		return false;
	}
	if($("#uploadFlag").val()!="Y"){
		alert("[신청정보 확인]을 진행해주세요.");
		return false;		
	}
	
	if(errorCount!=0)
	{
		alert("오류항목이 있습니다.\n오류사항을 엑셀양식에서 수정한 후\n[신청정보 업로드]를 다시 진행해주세요.");
		return false;	
	}else{		
		for(i=0;i<$("#tblExcel tbody tr").length;i++){			
			if($("#tblExcel tbody tr:eq("+i+") td:eq(1) #chkSch").val()==0){
				alert("저작권 검색여부를 완료해주세요");
				$("#tblExcel tbody tr:eq("+i+") td:eq(1)").focus();
				return;					
			}
		}
			
	}
	if(!chkAgr.checked){
		alert("담당자 보완 항목에 동의하여주세요");
		chkAgr.focus();
	}else{		
		var fm  = document.nonWriteForm;
		alert("신청된 내용은 [마이페이지>신청현황>저작권자 찾기위한 상당한 노력신청]에서\n확인이 가능합니다.");
		fm.action="/statBord/statBo06ExcelWrite.do";
		//showAjaxBox();
		fm.submit();
		//hideAjaxBox();
	}
}


//로딩 이미지 박스
function showAjaxBox(ment){
	//항상 화면 중앙에 나타나도록...
   var yp=document.body.scrollTop;
   var xp=document.body.scrollLeft;
   

   var ws=document.body.clientWidth;
   var hs=document.body.clientHeight;
   
   
   if(!ment) ment="잠시만 기다려주세요..";
   
   var ajaxBox=$('ajaxBox');
  // $('ajaxBoxMent').innerHTML=ment;

   ajaxBox.style.top=yp+eval(hs)/2-100+"px";
   ajaxBox.style.left=xp+eval(ws)/2-100+"px";

	//str
	var docuWidth = jQuery('#wrap').width();
	var docuHeight = jQuery('#wrap').height();
	
   //back_blackBox
	 jQuery("<div>").addClass("box").attr("id","modalBox").
		css({width:docuWidth,height:docuHeight,left:0,top:0}).appendTo("body"); 
	 	jQuery("#modalbox").fadeTo(100, 0.3);
		jQuery("#modalBox").click(function(){
			jQuery("#modalBox").remove();
	})
	//end

  Element.show(ajaxBox);    
}

// 로딩 이미지 박스 감추기
function hideAjaxBox(){
	Element.hide('ajaxBox');	
	
	//str
	jQuery("#modalBox").remove();
	//end
	
	// 통합검색에서 넘어온경우 알림메시지
	//alram();
}


var iRowIdx = 1;
function fn_addRow1(){
	  iRowIdx++;
/*	  if(iRowIdx > 5){
		  alert("첨부파일은 최대5개까지 가능합니다.");
		  iRowIdx--;
		  return;
	  } */
	  var oTbl = document.getElementById("tblAttachFile");
	  var oTbody = oTbl.getElementsByTagName("TBODY")[0];
	  
	  var oTr = document.createElement("TR");
	  oTr.id="fileTr"+iRowIdx;
		//순번(선택)
		var oTd = document.createElement("TD");
		var sTag = '<input name="chkDel1" id="chkDel1_'+iRowIdx+'" type="checkbox" value='+iRowIdx+' />';
		oTd.innerHTML = sTag;
		oTd.id = "fileTd"+iRowIdx;
		oTd.style.width = '7%';
		oTd.className = 'ce';
		oTr.appendChild(oTd);
		

		oTd = document.createElement("TD");		
		sTag = '<select class="inputData w100" name="selTite" id="selTite"'+iRowIdx+'>';
		sTag +='<option value="0">-----------선택------------</option>';	
		for(i=0;i<countRow.length;i++)	{
			sTag +='<option value="'+(countRow[i])+'">'+dataTite[i]+'</option>';
		}
		sTag +='</select>'
		oTd.innerHTML=sTag;
		oTr.appendChild(oTd);
		//첨부파일명
		oTd = document.createElement("TD");
		oTd.colSpan = 2;
		sTag = '<input type="hidden" name="fileDelYn" id="fileDelYn_'+iRowIdx+'" />';
		sTag += '<span id="spfile'+iRowIdx+'"><input type="file" name="file'+iRowIdx+'" id="file_'+iRowIdx+'" class="inputData L w100" onkeydown="return false;" 	onchange="checkFile(this.id)"/></span>';
				oTd.innerHTML = sTag;
		oTr.appendChild(oTd);
		
		oTbody.appendChild(oTr);

		       
}

function fn_delRow1(){
	var oTbl = document.getElementById("tblAttachFile");
	var oChkDel = document.getElementsByName("chkDel1");
	var count = 0;
	var checkArray = new Array();
	
		for(var i=0; i<oChkDel.length; i++){
		if(oChkDel[i].checked == true){
			count++;
			var checkNum="";
			checkNum += oChkDel[i].value;
			checkArray.push(checkNum);			
			iRowIdx--;
		}
	}
	if(count == 0){
		alert("삭제하실 내용을 체크해주세요.");
	}else{
		for(var i = 0 ; i < checkArray.length ; i ++){
				element = document.getElementById('fileTr'+checkArray[i]);
				element.parentNode.removeChild(element);
		}
	}
}

$(document).ready(function() {

	  
	 setWindowResize();
		
		
	
	/* if ($("body").has("#now_loading").html() == null) {
		  var tmpHtml = "";
		  tmpHtml = "<div id='now_loading' style='position: absolute; top: 0; left: 0; display:none;z-index:9000'>";
		  tmpHtml += "<table width=200 height=100 border=0> ";
		  tmpHtml += "    <tr>";
		  tmpHtml += "        <td align=center><img id='ajax_load_type_img' src='/images/loading.gif'></td>";
		  tmpHtml += "    </tr>";
		  tmpHtml += "</table>";
		  tmpHtml += "</div>";
	
		  $('#contentBody').append(tmpHtml).ajaxStart(function() {
			var width = $('#contentBody').width();
			var height = $('#contentBody').height();
				
				//화면을 가리는 레이어의 사이즈 조정
				$(".backLayer").width(width);
				$(".backLayer").height(height);
				
				//화면을 가리는 레이어를 보여준다 (0.5초동안 30%의 농도의 투명도)
				$(".backLayer").fadeTo(500, 0.0);
		  		 showLoading();
			  }).ajaxStop(function() {
			    $(".backLayer").fadeOut(1000);
			    hideLoading();
		  
		  });
	 } */
  
	 
	 
});

	
 function setWindowResize() {
	 //var thisX = parseInt(document.body.scrollWidth);
	 var thisY = parseInt(document.body.scrollHeight);
	 var thisX = parseInt(document.getElementById("pop_wrap").scrollWidth);
	 //var thisY = parseInt(document.getElementById("popup").scrollHeight);
	 
	 var maxThisX = screen.width - 50;
	 var maxThisY = screen.height - 50;
	 var marginY = 0;
	// alert(thisX + "===" + thisY);
	 //alert!("임시 브라우저 확인 : " + navigator.userAgent);
	 // 브라우저별 높이 조절. (표준 창 하에서 조절해 주십시오.)
	 if (navigator.userAgent.indexOf("MSIE 6") > 0) marginY = 45;        // IE 6.x
	 else if(navigator.userAgent.indexOf("MSIE 7") > 0) marginY = 75;    // IE 7.x
	 else if(navigator.userAgent.indexOf("MSIE 9") > 0) marginY = 80;    // IE 9.x
	 else if(navigator.userAgent.indexOf("Firefox") > 0) marginY = 80;   // FF    
	 else if(navigator.userAgent.indexOf("Opera") > 0) marginY = 30;     // Opera
	 else if(navigator.userAgent.indexOf("Chrome") > 0) marginY = 70;     // Chrome
	 else if(navigator.userAgent.indexOf("Netscape") > 0) marginY = -2;  // Netscape


	 if (thisX > maxThisX) {
	  window.document.body.scroll = "yes";
	  thisX = maxThisX;
	 }
	 if (thisY > maxThisY - marginY) {
	  window.document.body.scroll = "yes";
	  thisX += 19;
	  thisY = maxThisY - marginY;
	 }
	 window.resizeTo(thisX+10, thisY+marginY);


	 var windowX = (screen.width - (thisX+10))/2;
	 var windowY = (screen.height - (thisY+marginY))/2 - 20;
	 window.moveTo(windowX,windowY);
}


function checkFile(fileId){
	 var option = {
				 url 		: '/statBord/fileSizeCk.do',
				 type		: "post",				
				 dataType   : "json",
				 data 		: $('#nonWriteForm'),
				 success	: function(data){
					if(data.fileSize > 1024*1024*2){
						alert("2MB 이상 파일은 업로드 하실 수 없습니다.");
						if ($.browser.msie) {
						    $("#"+fileId).replaceWith( $("#"+fileId).clone(true) );
						} else {
						    $("#"+fileId).val("");
						}
					}
				}
	 };
		$('#nonWriteForm').ajaxSubmit(option);
}

function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.form1;
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.action = "/statBord/fileDownLoad.do";
	frm.submit();
} 


//파일 형식 체크
function fn_fileCheck() {
	var excelFile= $('#excel').val();
	var ext = excelFile.substring( excelFile.lastIndexOf(".")+1, excelFile.length ).toLowerCase();	
	var EXTENSION_TYPES = ['xls', 'xlsx'];	

	if(excelFile.length==0){		
		alert("업로드된 파일이 없습니다. 신청정보 업로드를 먼저 진행해 주세요.");
		$('#excel').focus();	
		return false;
	}
	
	
	for(var i=0; i<EXTENSION_TYPES.length; i++) {
		if( EXTENSION_TYPES[i] ==ext)
			break;
	}
		
	if(i == EXTENSION_TYPES.length ) {
		alert('지원하지 않는 엑셀 형식입니다. 지원형식:'+EXTENSION_TYPES.join(', ')+'\n파일을 다시 선택해 주세요.');	
		$('#excel').focus();
		return false;
	}else{
		return true;
	}
	
}


function fn_excelUpload(){
		
	if(fn_fileCheck()){
		//showAjaxBox();
		var option = {
						 url 		: '/statBord/statBo06RegiMultiExcelUp.do',
						 type		: "post",
						 dataType   : "json",						 
						 data 		: $('#excel'),
						 success	: function(data){
							 if(data.bflag=="N"){
								 alert("부적합한 파일입니다.");
								 return false;
							 }
							 var cellData = data.excelData;
							 var totalRow = cellData.length;
							 countRow = new Array(); // 엑셀 데이터 ROW 번호
							 dataTite = new Array(); // 저작물의 제호							 
							 var cell='';
							 var errorCount = 0;							
							 var selOption='<option value="0">-----------선택------------</option>';
							 if(totalRow>50){
								 alert("상당한노력 다건신청은 50건까지만 가능합니다.");
								 return false;
							 }
							 for(i=0;i<totalRow;i++){
								var cellCount = 0;
								var sumError = (cellData[i].errorFlag.split("@").length)-1;
								if(sumError>0){									
									cell+='<tr class="error1 popUp">';
									cell+='<td class="error2">'; 	
								}else{									
									cell+='<tr>';
									cell+='<td>';
								}							
								//오류건수		
								cell+=(cellData[i].errorFlag.split("@").length)-1+'건';								
								cell+='</td>';
								
								// 저작권 검색 여부  ; 
								cell+='<td>';
								cell+='<a> <img id="schImg" name="schImg" src="/images/2012/button/sch2.gif" onclick="javascript:fn_chkSch('+i+')" title="저작권 검색" class="pointer"> </a>';
								cell+='<input type="hidden" name="chkSch" id="chkSch" value="0">';
								cell+='</td>';
								
								//ROW 번호
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';				
									errorCount++;
								}else{
									cell+='<td>';
								}
								cell+=cellData[i].insertRow;
								cell+='<input type="hidden" name="countRow" id="countRow" value="'+(cellData[i].countRow)+'">';
								cell+='</td>';
								//첨부파일의 셀렉스 박스의 옵션값 엑셀데이터의 ROW와 맵핑하기 위해 임의로 순차적인 값(1부터)을 각 ROW에 부여한다.
								selOption+='<option value="'+(cellData[i].countRow)+'">'+cellData[i].tite+'</option>';						
							
								//신청사유 CD	
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}				
								cell+=cellData[i].rgstReasCdAndEtc;
								cell+='</td>';
								
								//장르 CODE
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].genreCd;						
								cell+='</td>';
								
								//저작물 제호
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].tite;
								cell+='</td>';					
								
								//저작권자정보(저작자종류+저작권자)
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].coptHodrCdAndName;	
								cell+='</td>';
								
								
								//취지
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}
								cell+=cellData[i].anucItem1;						
								cell+='</td>';
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].anucItem2;						
								cell+='</td>';
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].anucItem3;						
								cell+='</td>';
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].anucItem4;						
								cell+='</td>';		
								
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								
								cell+=cellData[i].worksTitle;						
								cell+='</td>';
								
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								
								cell+=cellData[i].anucItem5;						
								cell+='</td>';
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}						
								cell+=cellData[i].anucItem6;						
								cell+='</td>';
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								
								cell+=cellData[i].anucItem7;						
								cell+='</td>';
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].anucItem8;						
								cell+='</td>';
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].anucItem9;						
								cell+='</td>';
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].anucItem10;						
								cell+='</td>';
								
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].anucItem11;						
								cell+='</td>';
								
								if(cellData[i].errorFlag.indexOf("A"+(cellCount++)+"@")>=0){
									cell+='<td class="error2">';								
									errorCount++;
								}else{
									cell+='<td>';
								}	
								cell+=cellData[i].anucItem12;						
								cell+='</td>';								
								cell+='</tr>';
								
								
								countRow[i]=cellData[i].countRow;
								dataTite[i]=cellData[i].tite;							
						 	}			 
							$("#tblExcel tbody").html(cell);//엑셀 데이터 html 삽입							
							
							$("#tblAttachFile tbody").empty();//첨부파일 테이블 삭제
							fn_addRow1();//첨부파일 테이블 tr 재생성
						
							$("#selTite").html(selOption);// 첨부파일 저작물 선택 selectbox 생성
							$("#spanTotal").text(totalRow);//전체 건수 표시
							$("#totalRow").val(totalRow);//전체건수 히든값
							$("#spanError").text(errorCount);//에러 건수 표시
							$("#errorCount").val(errorCount);//에러건수 히든값	
							$("#uploadFlag").val("Y");//엑셀파일 업로드 완료를 뜻하는 flag 설정
							
							$("#realFileName").val(data.realFileName);//업로드된 파일이름 값
							$("#filePath").val(data.filePath);//파일경로							
							//hideAjaxBox();					
							
						
						}			
				 		
				 };
				$('#excelUploadForm').ajaxSubmit(option);
	}
		
}

function fn_chkSch(rowCount){
	if(confirm("저작권 검색을 통한 정보검색을 완료합니다.")){		
		$("#tblExcel tbody tr:eq("+rowCount+") td:eq(1) #schImg").attr("src","/images/2012/common/ico_complete.gif").attr("title","검색완료");
		$("#tblExcel tbody tr:eq("+rowCount+") td:eq(1) #schImg").removeAttr("onclick").removeAttr("class");
		$("#tblExcel tbody tr:eq("+rowCount+") td:eq(1) #chkSch").val(1);
		$("#tblExcel tbody tr:eq("+rowCount+") td:eq(1)").focus();
	}
	
}

function scrollX(){	
	
	document.getElementById("headDisplay").scrollLeft=document.getElementById("mainDisplayRock").scrollLeft;
}
//-->
</script>
</head>
<style>
.box {
	background-color:black;
	position:absolute;
	z-index:99999999;
	display:none;
}
	div.backLayer {
		display:none;
		background-color:black; 
		position:absolute;
		left:0px;
		top:0px;
	}

 </style>
<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<div id="ajaxBox" style="position:absolute; z-index:99999999999; background: url(/images/2012/common/loadingBg.gif) no-repeat 0 0; left:-500px; width: 402px; height: 56px; padding: 102px 0 0 0;">
		</div>	 
			<!-- HEADER str-->
			<div id="popHeader">
				<h1>저작권자 찾기위한 상당한 노력 신청</h1>
			</div>			
			<span class="p11 orange block mt5 ml5">※ 저작재산권자나 그의 거소가 명확하지 않은 저작물의 저작권자를 찾기위한 상당한 노력에 대한 이행신청을 합니다.
			</span>

			<!-- //HEADER end -->
			<form name="form1" method="post" action="#">
								<input type="hidden" name="filePath"> 
								<input type="hidden" name="fileName"> 
								<input type="hidden" name="realFileName">
								<input type="hidden" name="action_div"> 
								<input type="submit" style="display: none;">
			</form>
			
			<div id="contentBody"  style="padding:0 15px 15px 15px;">						
				<div class="section mt15">
						<form method="post" action="#" name="excelUploadForm" id="excelUploadForm" enctype="multipart/form-data">
						<!-- 엑셀양식 테이블 -->
						<input type="hidden" name="uploadFlag" id="uploadFlag" value="N">
						<span class="topLine"></span>
						<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
							<tbody>
								<tr>
									<td>
										<p class="pl10"><label for="sch1" class="schDot1">엑셀 양식</label> <span class="button small icon ml30">
										<a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form'/>','상당한노력신청양식.xlsx','상당한노력신청양식.xlsx')" class="btn_download">
										다운로드</a><span class="delete"></span></span></p>								
										<p class="mt10 topDotted pt10 pl10"><label for="sch3" class="schDot1">신청정보 업로드</label>
										<input type="file" id="excel" name="excel" class="inputData L w60 mr30" onchange="javascript:fn_fileCheck();"  onkeydown="return false;"/> <span class="button small icon"><a href="#1" onclick="javascript:fn_excelUpload();">신청정보 확인</a><span class="delete"></span></span></p>																	
									</td>
								</tr>
							</tbody>
						</table>	
						</form>
				</div>
				<div class="section mt15">
						<form method="post" action="#" name="nonWriteForm" id="nonWriteForm" enctype="multipart/form-data">						
						<input type=hidden id="mgntDivs" name="mgntDivs" value="BO06" />
						<input type="hidden" name="rgstIdnt" value="<%=sessUserIdnt%>"/>						
						<input type="hidden" name="realFileName" id="realFileName" value="${realFileName }"/>
						<input type="hidden" name="filePath" id="filePath" value="${filePath }"/>						
														
						<h2>신청정보 업로드 안내</h2>	
						<ul class="list">						
							<li>1. 엑셀양식을 다운로드 받는다.</li>
							<li>2. 엑셀양식에 맞게 데이터를 입력(50건 이내) 및 저장한다.(양식 내 작성가이드 참고)</li>
							<li>3. 신청정보 업로드[찾아보기]에서 업로드할 엑셀파일을 선택한 후 [신청정보 확인] 버튼을 선택하여 신청정보를 확인한다.</li>
							<li>4. 신청정보확인에서 오류항목이 없으면 [저작권검색여부] 확인 후 [신청] 버튼을 선택하여 신청을 완료한다.</li>
						</ul>						
						<h2 class="mt20">신청정보 확인</h2>	
						<ul class="list1 ml10 blue2">										
							<li> 전체 : <span id="spanTotal">0</span>건
							<input type="hidden" name="totalRow" id="totalRow"/>
							</li>				
							<li> 입력정보 기입오류 <span id="spanError">0</span>건
							<input type="hidden" name="errorCount" id="errorCount"/>
							</li>
						</ul>
						<ul class="list mt10">							
							<li>1. <img src="/images/2012/common/ic_error.gif" alt="오류"/>
								 표시된 항목은 유효성확인 오류항목입니다.
							       오류사항을 엑셀양식에서 수정한 후 [신청정보 업로드]를 다시 진행해주세요</li>								 
							<li>2. 저작물의 [저작권검색]을 통한 저작권검색여부를 완료합니다.	</li>
						</ul>
						<div id="headDisplay" class="mt15" style="width:885px; overflow-x:hidden">
						<table id=""  style="width:1800px;" cellspacing="0" summary="" cellpadding="0" class="grid popUp tableFixed">
							<colgroup>
							<col width="3%"><col width="5%"><col width="2%"><col width="3%"><col width="3%">
							<col width="5%"><col width="5%"><col width="8%"><col width="5%">
							<col width="7%"><col width="5%"><col width="5%">
							<col width="7%"><col width="5%"><col width="5%">
							<col width="7%"><col width="5%"><col width="7%"><col width="5%">
							<col width="5%">
							</colgroup>		
							<thead>
								<tr>
									<th scope="row" rowspan="3" class="wh_narmal">오류 <br/>항목</th>
									<th scope="row" rowspan="3" class="wh_narmal">저작권 <br/>검색 여부</th>
									<th scope="row" rowspan="3" class="wh_narmal">순번</th>
									<th scope="row" rowspan="3" class="wh_narmal">신청<br/>사유<br/>CODE</th>
									<th scope="col" colspan="3" class="wh_narmal">저작물정보</th>
									<th scope="col"  colspan="13" class="wh_narmal">저작권자 조회공고 정보</th>
								</tr>
								<tr>
									<th scope="row" rowspan="2" class="wh_narmal">장르<br/>CODE</th>
									<th scope="row" rowspan="2" class="wh_narmal">제호</th>
									<th scope="row" rowspan="2" class="wh_narmal">저작권자 <br/>정보</th>
									<th scope="row" rowspan="2" class="wh_narmal">1. 저작권 재산권자를
									 찾는다는 취지</th>
									<th scope="col"  colspan="3" class="wh_narmal">2. 저작재산권자<br/>
									 성명,주소,연락처</th>
									<th scope="col" rowspan="2" class="wh_narmal">3.저작물의 제호</th>
									<th scope="col" rowspan="2" class="wh_narmal">4. 공표시 표시된<br/>
									 저작재산권자 성명</th>
									<th scope="col"  colspan="2" class="wh_narmal">5. 저작물을 발행<br/> 
									또는 공표한자</th>
									<th scope="col" rowspan="2" class="wh_narmal">6. 저작물의<br/> 이용목적</th>
									<th scope="col" colspan="4" class="wh_narmal">8. 공고자 및 연락처</th>
								</tr>
								<tr>
									<th scope="row" class="wh_narmal">성명</th>
									<th scope="row" class="wh_narmal">주소</th>
									<th scope="row" class="wh_narmal">연락처</th>
									<th scope="row" class="wh_narmal">저작물발행</th>
									<th scope="row" class="wh_narmal">공표연월일</th>
									<th scope="row" class="wh_narmal">공고자</th>
									<th scope="row" class="wh_narmal">주소</th>
									<th scope="row" class="wh_narmal">연락처</th>
									<th scope="row" class="wh_narmal">담당자</th>	
								</tr>						
							</thead>
						</table>
						</div>
				<div id="mainDisplayRock" class="mb15" style="width:902px; height:480px; overflow-x:scroll; overflow-y:scroll;" onscroll="javascript:scrollX();">							
						<table id="tblExcel" style="width:1800px;" border="1" cellspacing="0" summary="" cellpadding="0" class="grid popUp mb15 tableFixed" >
							<colgroup>
							<col width="3%"><col width="5%"><col width="2%"><col width="3%"><col width="3%">
							<col width="5%"><col width="5%"><col width="8%"><col width="5%">
							<col width="7%"><col width="5%"><col width="5%">
							<col width="7%"><col width="5%"><col width="5%">
							<col width="7%"><col width="5%"><col width="7%"><col width="5%">
							<col width="5%">
							</colgroup>								
							<tbody>
							<tr>							
							<td colspan="10" class="ce">
								등록된 정보가 없습니다.
							</td>
							<td colspan="10">
							</td>
							</tr>					
							</tbody>						
						</table>
					</div>			
					<ul>
					<li class="mt15">
						<div class="fl mb5">
							<h2 class="strong">복제물의 표지사진 등의 자료</h2>
						</div>
						<div class="fr mb5">
							<p>
								<span title="추가" class="button small"><button id="btnAdd" onkeypress="" onclick="javascript:fn_addRow1();" type="button">File 추가</button></span>
								<span title="삭제" class="button small"><button id="btnDel" onkeypress="" onclick="javascript:fn_delRow1();" type="button">File 삭제</button></span>
							</p>
						</div>
						
						<div class="mb15">
						     <table id="tblAttachFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부파일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
								<colgroup>
								    <col width="8%">
								    <col width="30%">
								    <col width="*%">
								</colgroup>
								<thead>
								    <tr>
   									    <th scope="row" class="ce">선택</th>
   									    <th scope="row" class="ce">저작물 정보(순번/저작물명)</th>
   									    <th scope="row">첨부파일</th>
								    </tr>
								</thead>
								<tbody>
								    <tr id="fileTr1">
								    	<td class="ce" id="fileTd1">
								    		<input name="chkDel1" id="chkDel1" type="checkbox" value="1" />
								    	</td>
								    	<td>
								    		<select class="inputData w100" name="selTite" id="selTite" >
								    		<option value="0">-----------선택------------</option>								    									    									    		
								    		</select>								    		
								    	</td>								    	
								    	<td>
									    	<input type="hidden" name="fileDelYn" />
											<span id="spfileD2'">
											    <input type="file" name="file" id="file" title="첨부파일" 
											        class="inputData L w100"
											        onkeydown="return false;" onchange="checkFile(this.id)"/>
											</span>
								    	</td>
								    </tr>
                  				</tbody>
							</table>
						<!-- <span class="p11 orange block mt5">※ 첨부파일은 최대5개까지 가능합니다.</span> -->
						</div>
					</li>					
					<li class="mt20">					
						<p class="strong line13">* 상당한 노력 신청 정보는 담당자에 의해 보완될 수 있습니다. (<input type='checkbox' id='chkAgr' class='mb3'/> 동의함)</p>
					</li>
					</ul>								
				</form>				
					<!-- 버튼 str -->	
						<div class="btnArea mt20">
							<p class="fl"><span class="button medium gray"><a href="javascript:self.close()">취소</a></span></p>
						
							<p class="fr"><span class="button medium"><a onclick="javascript:nonWriteSub()">신청</a></span></p>							
							
						</div>
	                  <!-- //버튼 -->
	                 
				</div>			
				<!-- //주요컨텐츠 end -->
				
			</div>
	<!-- FOOTER str-->
			<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights reserved.</p>
			<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2011/button/pop_close.gif" alt="X" title="이 창을 닫습니다." /></a>
			<!-- //FOOTER end -->

		</div>
	

 <script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>

