<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	
	String sessUserAddr = user.getUserAddr();
	String sessMoblPhon = user.getMoblPhon();

	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 이용승인신청 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 

<!--
function writeSubmit(){ //등록 submit
	var tite = document.getElementById("tite");
	var boadDesc = document.getElementById("bordDesc");
	var genre = document.getElementById("genre");
	var anucItem1 = document.getElementById("anucItem1");
	var anucItem2 = document.getElementById("anucItem2");
	var anucItem3 = document.getElementById("anucItem3");
	var anucItem4 = document.getElementById("anucItem4");
	var anucItem5 = document.getElementById("anucItem5");
	var anucItem6 = document.getElementById("anucItem6");
	var anucItem7 = document.getElementById("anucItem7");
	var anucItem8 = document.getElementById("anucItem8");
	var anucItem9 = document.getElementById("anucItem9");
	var anucItem10 = document.getElementById("anucItem10");
	var anucItem11 = document.getElementById("anucItem11");
	var anucItem12 = document.getElementById("anucItem12");
	if(tite.value == "" || tite.value.length ==0){
		alert("저작물의 제목을 입력해주세요");
		tite.focus();
// 	}else if(bordDesc.value=="" || bordDesc.value.length == 0){
// 		alert("저작재산권자를 찾는다는 취지를 입력해주세요");
// 		bordDesc.focus();
	}else if(anucItem1.value=="" || anucItem1.value.length == 0){
		alert("저작재산권자의 성명을 입력해주세요");
		anucItem1.focus();
	}else if(anucItem2.value=="" || anucItem2.value.length == 0){
		alert("저작재산권자의 주소를 입력해주세요");
		anucItem2.focus();
	}else if(anucItem3.value=="" || anucItem3.value.length == 0){
		alert("저작재산권자의 연락처를 입력해주세요");
		anucItem3.focus();
	}else if(genre.value == 0){ 
		alert("제호의 장르를 선택해주세요");
		genre.focus();
	}else if(anucItem4.value=="" || anucItem4.value.length == 0){
		alert("제호를 입력해주세요");
		anucItem4.focus();
	}else if(anucItem5.value=="" || anucItem5.value.length == 0){
		alert("공표 시 표시된 저작재산권자의 성명을 입력해주세요");
		anucItem5.focus();
	}else if(anucItem6.value=="" || anucItem6.value.length == 0){
		alert("저작물발행을 입력해주세요");
		anucItem6.focus();
	}else if(anucItem7.value=="" || anucItem7.value.length == 0){
		alert("공표연월일을 입력해주세요");
		anucItem7.focus();
	}else if(anucItem7.value.length != 8 || anucItem7.value.indexOf('.') != -1 || anucItem7.value.indexOf('/') != -1 || anucItem7.value.indexOf('-') != -1){
		alert("공표연월일의 입력값은 잘못된 날짜형식입니다.");
		anucItem7.focus();
// 	}else if(anucItem7.value.length == 8 || anucItem7.value.indexOf('.') == -1 || anucItem7.value.indexOf('/') == -1 || anucItem7.value.indexOf('-') == -1){
// 		var result = true;
// 		var yearStr		= new Number(anucItem7.value.substring(0, 4));
// 		var monthStr	= new Number(anucItem7.value.substring(4, 6));
// 		var dayStr		= new Number(anucItem7.value.substring(6, 8));
// 		if(monthStr > 12 || monthStr < 1)
// 				result = false;
// 			else if(dayStr > 31 || dayStr < 1)
// 				result = false;
// 			if(result == false)
// 			alert("공표연월일의 입력값은 잘못된 날짜형식입니다.");
// 			anucItem7.focus();
			
	}else if(anucItem8.value=="" || anucItem8.value.length == 0){
		alert("저작물의 이용목적을 입력해주세요");
		anucItem8.focus();
	}else{
	var frm = document.statWriteForm;
//  	if(checkForm(frm)) {
		alert("등록된 내용은 관리자 승인 후 공고 됩니다.\n[마이페이지>신청현황>저작권자 조회 공고] 에서 등록내용\n확인이 가능합니다.");
		//장르(select) 값 hidden genreCd value에 옮겨서 값 넘기기
		var sel = document.getElementById("genre");
		var val = sel.options[sel.selectedIndex].value;
		document.getElementById("genreCd").value = val;
		frm.action = "/statBord/statBo01Write.do";
		frm.submit();
	}
}

var filenum = 1;
function addfile(){			// 파일 input 추가
	filenum++;
	var fd= document.getElementById("p");
	if(filenum > 5){
		alert("첨부파일은 최대 5개까지 가능합니다.");
	}else
// 	fd.innerHTML += "<p id='fileP"+filenum+"'><input type='file' id='file" + filenum +"'name='file" + filenum +"'>&nbsp;&nbsp;&nbsp;<input type='button' name='"+ filenum +"' id='del" + filenum +"' value='삭제' onclick='javascript:delfile(this.name)'></p>";
	var inputFile = "<p id='fileP"+filenum+"'><input type='file' id='file" + filenum +"'name='file" + filenum +"'>&nbsp;&nbsp;&nbsp;<input type='button' name='"+ filenum +"' id='del" + filenum +"' value='삭제' onclick='javascript:delfile(this.name)'></p>";
	jQuery("#p").append(inputFile);
}
function delfile(id){		//파일 input 삭제
	element = document.getElementById("fileP"+id);
	element.parentNode.removeChild(element);
  	filenum--;
}
 -->
</script>
</head>

<body>
	
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		
		<!-- HEADER str-->
		<div id="header">
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include	page="/include/2012/header.jsp" />  --%>
		<script type="text/javascript">initNavigation(3);</script>
		</div>
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="container" class="container_vis3">
		    	<div class="container_vis">
		      <h2><img src="/images/2012/title/container_vis_h2_3.gif" alt="저작권자찾기" title="저작권자찾기" /></h2>
		    	</div>
		    	<!-- content st -->
		    	<div class="content">
		    		
		    		<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu03.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1","lnb13"); 
 				</script>
 				
			      <!-- //래프 -->
			      <div id="ajaxBox" style="position:absolute; z-index:1; background: url(images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
			        <p style="height: 38px; text-align: center; margin: 0;"> <img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
			          <span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span> </p>
			      </div>
			      
			      <!-- 주요컨텐츠 str -->
			      <div class="contentBody" id="contentBody">
			      <c:if test="${bordCd == 1}">
			      	<p class="path"><span>Home</span><span>저작권자찾기</span><span>저작권자 검색 및 상당한 노력 신청</span> <em>저작권자 조회 공고</em> </p>
			      	<h1><img src="/images/2012/title/content_h1_0303.gif" alt="상당한노력 공고" title="상당한노력 공고" /></h1>
			       </c:if>
			      <c:if test="${bordCd == 2}">
			      	<p class="path"><span>Home</span><span>저작권자찾기</span><span>법정허락이용승인신청</span><span>법정허락 공고</span> <em>이용승인신청공고</em> </p>
			      	<h1><img src="/images/2012/title/content_h1_0306.gif" alt="법정허락 공고" title="법정허락 공고" /></h1>
			      </c:if>
			      <c:if test="${bordCd == 3}">
			      	<p class="path"><span>Home</span><span>저작권자찾기</span><span>법정허락이용승인신청</span><span>법정허락 공고</span> <em>이용승인신청 심의결과 공고</em> </p>
			      	<h1><img src="/images/2012/title/content_h1_0306.gif" alt="법정허락 공고" title="법정허락 공고" /></h1>
			      </c:if>
			      	
			      	<!-- section -->
			      	
					<div class="section">
					
					<div class="floatDiv mt20">
                              		<h2 class="fl">저작권자 조회 공고 등록</h2> 
<!--                   	<p class="fr"><span class="button small icon"><a href="javascript:openSmplDetail('MR')">예시화면 보기</a><span class="help"></span></span></p> -->
					</div>
					<!-- 그리드 str -->
					<form method="post" action="#" name="statWriteForm"  enctype="multipart/form-data">
					<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<colgroup>
						<col width="*">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">제목&nbsp; </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="ce pd5"><input type="text"  title="제목" name="tite" id="tite" class="inputData w95" /></td>
							</tr>
						</tbody>
					</table>
					<!-- //그리드 -->
					
					<ul class="mt15">
					<li>
						<p class="blue2 line22">1. 저작권자를 찾는다는 취지&nbsp; </p>
							<textarea cols="10" rows="3" id="bordDesc" name="bordDesc"  title="취지" class="textarea w98" ></textarea>
					</li>
					<li class="mt15">
						<p class="blue2 line22">2. 저작재산권자의 성명 또는 명칭, 주소 또는 거소 등</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm">성명</label></th>
									<td><input type="text" title="성명" name="anucItem1" id="anucItem1" class="inputData" />
									</td>
								</tr>
								<tr>
									<th scope="row"><label for="zip">주소</label></th>
									<td><input type="text" title="주소" name="anucItem2" id="anucItem2" class="inputData w85" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="phone">연락처</label></th>
									<td><input type="text" title="연락처" name="anucItem3" id="anucItem3" class="inputData" /></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="blue2 line22">3. 저작물의 제호</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="tl">제호</label></th>
									<td>
										<select id="genre" name="genre" class="inputData">
													<option value="0">--전체--</option>
													<c:forEach items="${codeList}" var="codeList">
															<option  value="${codeList.midCode}">${codeList.codeName}</option>
													</c:forEach>
										</select>
									<input type=hidden id="genreCd" name="genreCd" value="" />
									<input type="text" title="제호" name="anucItem4" id="anucItem4" class="inputData w65" />
									</td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="blue2 line22">4. 공표 시 표시된 저작재산권자의 성명(실명 또는 이명)</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm2">성명</label></th>
									<td><input type="text" title="성명" name="anucItem5"  id="anucItem5" class="inputData" /></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="blue2 line22">5. 저작물을 발행 또는 공표한자</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="date1">저작물발행</label></th>
									<td><input type="text" title="저작물발행" id="anucItem6" name="anucItem6" class="inputData" /></td>
								</tr>
								<tr>
									<th scope="row"><label for="date2">공표연월일</label></th>
									<td><input type="text" maxlength="8" title="공표연월일" id="anucItem7" name="anucItem7" class="inputData" /> <span style="color:red">* ex ) 20120101(2012년 01월 01일)</span></td>
								</tr>
							</tbody>
						</table>
					</li>
					<li class="mt15">
						<p class="blue2 line22">6. 저작물의 이용 목적</p>
						<textarea cols="10" rows="3" name ="anucItem8" id="anucItem8" class="textarea w98" ></textarea>
					</li>
					<li class="mt15">
						<p class="blue2 line22">7.복제물의 표지사진 등의 자료</p>
						<div class="floatDiv"  id="filediv">
							<p class="fl w70" id="p">
									 <input type="file" title="파일" name="file" id="file" />&nbsp;&nbsp;
									 <input type="button" value="추가" onclick="javascript:addfile()"/>
									 <input type="button" id="del" name="" value="삭제" onclick="javascript:delfile()" style="display: none"/>
							</p>
							<p class="fl w70" id="p">
							<span class="p11 orange block mt5">※ 첨부파일은 최대5개까지 가능합니다.</span></p>	
						</div>
						
					</li>
					<li class="mt15">
						<p class="blue2 line22">8. 공고자 및 연락처</p>
						<table border="1" cellspacing="0" cellpadding="0" summary="" class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="20%">
							<col>
							</colgroup>
							<tbody>
								<tr>
									<th scope="row"><label for="nm3">공고자</label></th>
									<td><input type="text" title="공고자" name="anucItem9" id="anucItem9" class="inputData" value="<%=sessUserName%>"/></td>
								</tr>
								<tr>
									<th scope="row"><label for="zip3">주소</label></th>
									<td><input type="text" title="주소" name="anucItem10" class="inputData w98" value="<%=sessUserAddr%>"/></td>
								</tr>
								<tr>
									<th scope="row"><label for="phone3">연락처</label></th>
									<td><input type="text" title="연락처" name="anucItem11" class="inputData" value="<%=sessMoblPhon%>"/></td>
								</tr>
								<tr>
									<th scope="row"><label for="admin3">담당자</label></th>
									<td><input type="text" title="담당자" name="anucItem12" class="inputData" />
									<input type="hidden" name="bordCd" id="bordCd" value="${bordCd}"/>
									<input type="hidden" name="divsCd" id="divsCd" value="5"/>
									<input type="hidden" name="rgstIdnt" id="rgstIdnt" value="<%=sessUserIdnt%>" />
									</td>
								</tr>
							</tbody>
						</table>
					</li>
					</ul>
					</form>
					<!-- 버튼 str -->	
					<div class="btnArea">
						<p class="fl"><span class="button medium gray"><a href="javascript:history.back(-1)">취소</a></span></p>
						<p class="fr"><span class="button medium"><input type="button" value="등록" onclick="writeSubmit()" /></span></p>
					</div>
                              <!-- //버튼 -->	
                                    
			      	</div>
			      	<!-- //section -->
			    </div>
	      	    	<!-- //주요컨텐츠 -->
                        
		    	</div>
	    		<!-- //content -->
	    		
	    	<!-- FOOTER str-->
			<jsp:include page="/include/2012/footer.jsp" />
			<!-- FOOTER end -->
		
	  	</div>
	  	<!-- //CONTAINER --> 
	    	
	    		    	
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	
<!-- <script type="text/javascript" src="http://www.right4me.or.kr:8080/js/2010/calendarcode.js"></script>  -->
<!-- <script type="text/JavaScript">  -->
<!-- Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기 -->
<!-- </script> -->

</body>
</html>