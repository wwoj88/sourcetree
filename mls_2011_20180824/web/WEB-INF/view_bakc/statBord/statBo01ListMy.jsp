<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 찾기 검색 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">

<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>

<script type="text/javascript">  
<!--
$(window).load(function(){			//페이지의 로딩이 모두 끝난후 라디오박스 값셋팅
	var sucValue = document.getElementById("Suc").value;
	if(sucValue == 1){
		self.close();
		opener.parent.location.reload();
	}
});
function goPage(pageNo) {
	 var findForm = document.getElementById("findForm");
	 var searchStr = document.getElementById("title");
	 findForm.page_no.value = pageNo;
	 findForm.target = "_self";  
	  if(trim(searchStr.value)=='' || searchStr.value.length==0)
	  findForm.action = "/statBord/statBo01ListMy.do";
	  else
	   findForm.action = "/statBord/statBo01ListMyFind.do";
	  findForm.submit();
	}
//엄동규 -- 장르 제목 검색

function fn_frameList(){
	var f = document.findForm;
	if(f.joinDay.value =='' || f.endDay.value == ''){
		alert("등록일을 입력해주세요.");
		f.action = "/statBord/statBo01ListMy.do?bordCd=1";
		f.submit();
	}else if(f.joinDay.value!='' && f.endDay.value!=''){
		f.action = "/statBord/statBo01ListMyFind.do";
		f.submit();
	}
	var sel = document.getElementById("genre");		//장르 셀렉트박스
 	var val = sel.options[sel.selectedIndex].value;	//셀렉트박스 값
 	var text = document.getElementById("title").value; //검색어 값
 	
 	document.getElementById("genreCd").value = val;
 	document.getElementById("tite").value = text;
 	}
 	
 	
function keyCheck(key){
	if(event.keyCode == 13){
		fn_frameList();
	}
}
function loginCheck(){
	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	var bordCd = 1;
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
	return;
	}else{
		location.href ="/statBord/statBo01Regi.do?bordCd="+bordCd;
	}
}
function writeForm(){
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
	}else{
	window.open("/statBord/statBo01RegiPop.do?bordCd=1", "small", "width=750, height=650, scrollbars=yes, menubar=no, location=yes");
	}
}
//달력
function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}
//날짜체크 
function checkValDate(){
	var f = document.findForm;
	if(f.joinDay.value!='' && f.endDay.value!=''){
		if(parseInt(f.joinDay.value,10)>parseInt(f.endDay.value,10)){
			alert('만료일이 시작일보다 이전입니다.\n일자를 다시 확인하십시요');
			f.endDay.value='';
		}
	}
}
 
//-->
</script>
<script>

</script>
</head>
 
<body>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="contents">
				<!-- 래프 -->																														
				<div class="con_lf" style="width: 22%">
					<div class="con_lf_big_title">마이페이지</div>
					<ul class="sub_lf_menu">
						<li><a href="/myStat/statRsltInqrList.do" class="on">신청현황</a>
							<ul class="sub_lf_menu2">
								<!-- <li><a href="/statBord/statBo06List.do?bordCd=6">저작권자 찾기위한 상당한 노력<br/>신청</a></li>-->
								<li><a href="/myStat/statRsltInqrList.do">법정허락 승인신청</a></li>
								<li><a href="/statBord/statBo01ListMy.do?bordCd=1" class="on">저작권자 조회 공고</a></li>
								<li><a href="/statBord/statBo05ListMy.do?bordCd=5">보상금 공탁 공고</a></li>
							</ul>
						</li>
						<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보</a>
							<ul class="sub_lf_menu2 disnone">
								<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보 수정</a></li>
								<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=D&amp;userIdnt=<%=sessUserIdnt%>">회원탈퇴</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<!-- //래프 -->

				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						마이페이지
						&gt;
						신청현황
						&gt;
						<span class="bold">저작권자 조회 공고</span>
					</div>
					<div class="con_rt_hd_title">저작권자 조회 공고</div>
					<br/><br/>
					<div class="section">

					<form method="get" action="#" name="findForm" id ="findForm">						
						<fieldset class="w100" style="width: 815px;">
							<legend></legend>
								<div class="boxStyle mt20">
									<div class="box1 floatDiv">
									
									<table class="fl schBoxGrid w70" summary="">
											<caption></caption>
											<colgroup><col width="25%"><col width="*"></colgroup>
											<tbody>
												<tr>      
													<th scope="row"><label for="sch1">장르</label></th>
													<td>
													<input type="hidden" id = "page_no" name="page_no" value="1" />
													<input type="hidden" id = "genreCd" name="genreCd" value="${genreCd}" />
													<input type="hidden" id = "tite"	name="tite"    value="${title}" />
													<input type="hidden" id = "bordCd" 	name="bordCd"  value="${bordCd }" />
													<input type="hidden" id="Suc" name="Suc" value="${Success}"/>
													<select  id="genre"  style="width:105px">
													<c:if test="${!empty genreCd}">
															<option value="0">--전체--</option>
													<c:forEach items="${codeList}" var="codeList">
														<c:if test="${genreCd == codeList.midCode}">
															<option selected="selected" value="${codeList.midCode}">${codeList.codeName}</option>
														</c:if>
														<c:if test="${genreCd!= codeList.midCode}">
															<option value="${codeList.midCode}">${codeList.codeName}</option>
														</c:if>
													</c:forEach>
													</c:if>
													<c:if test="${empty genreCd}">
													<option value="0">--전체--</option>
													<c:forEach items="${codeList}" var="codeList">
														<option value="${codeList.midCode}">${codeList.codeName}</option>
													</c:forEach>
													</c:if>
													</select>
													</td>
													<th scope="row"><label for="app2">등록일</label></th>
													<td>
													<input type="text" id="joinDay" title="검색날짜" name="joinDay" style="width:50px" value="${defaultJoinDay}" onblur='javascript:checkValDate();'> 
													<img src="/images/2012/common/calendar.gif" onclick="javascript:fn_cal('findForm','joinDay');" class="vmid" alt="" title="신청일자 시작일시를 선택하세요." /> 
													~
													<input type="text" title="검색날짜" id="endDay" name="endDay" value="${defaultEndDay}" onblur='javascript:checkValDate();' style="width:50px">
													<img src="/images/2012/common/calendar.gif" class="vmid" onclick="javascript:fn_cal('findForm','endDay');" alt="달력" title="마지막 날짜를 선택하세요." />
													</td>
										
												</tr>
												<tr>
													<th scope="row"><label for="sch2">제목</label></th>
													<td><input title="제목" type="text" class="inputData w150" name="title" id="title" value="${title}" onkeydown="javascript:keyCheck(event)"></td>
												</tr>
											</tbody>
										</table>
										
										<p class="fl btn_area pt25"><img title="검색" alt="검색" onkeypress="javascript:fn_frameList();" onclick="javascript:fn_frameList();" src="/images/2012/button/sch.gif">
									</p></div>
									<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
									
								
								</div>

							</fieldset><br/><br/><br/>
						
							<!-- 그리드스타일 -->
							<table border="1" cellspacing="0" cellpadding="0" class="grid mt20" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="8%">
								<col width="15%">
								<col width="*">
								<col width="15%">
								<col width="15%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">장르</th>
										<th scope="col">제목</th>
										<th scope="col">등록일</th>
										<th scope="col">공고일</th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${totalRow == 0}">
										<tr>
											<td class="ce" colspan="6">등록된 게시물이 없습니다.</td>
										</tr>
								</c:if>
						        <c:if test="${totalRow > 0 }">
						        <c:forEach var="AnucBord" items="${AnucBord}">
						         <tr>
						          <td class="ce">
						          <c:set var = "num" value="${totalRow-(10 * (AnucBordnum.nowPage-1))-1}" />
						        	${totalRow-AnucBord.rowNo+1}  
						   		 </td>
						          <td class="ce">${AnucBord.genreCdName}</td>
						          <td align="left"><a href="/statBord/statBo01DetlMy.do?bordSeqn=${AnucBord.bordSeqn}&bordCd=${AnucBord.bordCd}">${AnucBord.tite}</a>&nbsp;&nbsp;&nbsp;&nbsp;
<%-- 						          <c:if test="${AnucBord.objcCount !=0 }"> --%>
<%-- 						          &lt;이의제기 : ${AnucBord.objcCount}건&gt; --%>
<%-- 						          </c:if> --%>
						          </td>
						          <td class="ce">${AnucBord.rgstDttm}</td>
						          <c:if test="${empty AnucBord.openDttm}">
						          <td class="ce">미공고</td>
						          </c:if>
						          <c:if test="${!empty AnucBord.openDttm}">
						          <td class="ce">${AnucBord.openDttm}</td>
						          </c:if>
						          
						         </tr>
						        </c:forEach> 
								</c:if>
								
		    					</tbody>
		    					
							</table>
							</form>
							<!-- //그리드스타일 -->
							  <!-- 페이징 시작 -->
							  <!-- 페이징 -->
						<div class="pagination">
							<ul>
								<li>
						<!--페이징 리스트 -->
<%-- 						 	<jsp:include page="../common/PageList_2011.jsp" flush="true">
							  	<jsp:param name="totalItemCount" value="${totalRow}" />
								<jsp:param name="nowPage"        value="${AnucBordnum.nowPage}" />
								<jsp:param name="functionName"   value="goPage" />
								<jsp:param name="listScale"      value="" />
								<jsp:param name="pageScale"      value="" />
								<jsp:param name="flag"           value="M01_FRONT" />
								<jsp:param name="extend"         value="no" />
							</jsp:include> --%>
								</li>	
							</ul>
						</div>
							<!-- //페이징 -->
					<!-- 버튼 str -->
					<p class="rgt"><span class="button medium"><a href="javascript:;" onclick="writeForm()">등록</a></span></p>
                     <!-- //버튼 -->
					</div>
				<!-- //주요컨텐츠 end -->
			</div>
			<p class="clear"></p>
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
			<jsp:include page="/include/2017/footer.jsp" />
		<!-- FOOTER end -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>

 
</body>
</html>

