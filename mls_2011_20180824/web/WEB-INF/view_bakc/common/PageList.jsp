<%@ page contentType="text/html;charset=euc-kr"%>

<%
    long totalItemCount   = nullToLong(request.getParameter("totalItemCount"));     // 전체 목록 수 *
    long nowPage          = nullToLong(request.getParameter("nowPage"));            // 현재 페이지 번호 *
    int listScale         = nullToInt(request.getParameter("listScale"), 10);        // 한 페이지당 출력할 목록 수
    int pageScale         = nullToInt(request.getParameter("pageScale"), 10);       // 한 페이지당 출력할 이동 링크 수

    String functionName   = nullTo(request.getParameter("functionName"));           // 페이지 이동 자바스크립트 평션 명 *
    String extend         = nullTo(request.getParameter("extend"), "no");           // 맨처음/나중 버튼 활성화 여부 ('yes' : 사용, 'no' : 사용안함)
    String flag           = nullTo(request.getParameter("flag"), "back");           // 백오피스/프론트 구분 ('fp' : 프론트, 'bo' : 백오피스)

    if(totalItemCount > 0 && nowPage > 0 && !functionName.equals(""))
    {
        long totalPage              = ((totalItemCount-1)  / listScale) + 1;
        long totalPageBlock         = ((totalPage-1) / pageScale ) + 1;
        long nowPageBlock           = ((nowPage -1) / pageScale ) + 1;

        long startKey = (nowPageBlock-1) * pageScale + 1 ;
        long stopKey = startKey + pageScale > totalPage ? totalPage : startKey + pageScale - 1;

        if(flag != null && !flag.equals(""))
        {
            // JoinChips 템플릿 구성/////////////////////////////////////////////////////////

            String prevLink         = "";
            String nextLink         = "";
            String startLink        = "";
            String endLink          = "";

            String prevLinkNone     = "";
            String nextLinkNone     = "";
            String startLinkNone    = "";
            String endLinkNone      = "";


            if(flag.equals("M01_FRONT"))
            {
                prevLink        = "<img src='/images/board/last_btn.gif' border='0' alt=''>";
                nextLink        = "<img src='/images/board/next_btn.gif' border='0' alt=''>";
                startLink       = "<img src='/images/board/first_btn.gif' border='0' alt=''>";
                endLink         = "<img src='/images/board/last_btn.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/images/board/first_btn.gif' border='1' alt=''>";
                nextLinkNone    = "<img src='/images/board/last_btn.gif' border='1' alt=''>";
                startLinkNone   = "<img src='/images/board/next_btn.gif' border='1' alt=''>";
                endLinkNone     = "<img src='/images/board/last_btn.gif' border='1' alt=''>";
            }
            else if(flag.equals("M01_ADMIN"))
            {
                prevLink        = "<img src='/admin/busiintro/images/bt_rw.gif' border='0' alt=''>";
                nextLink        = "<img src='/admin/busiintro/images/bt_fw.gif' border='0' alt=''>";
                startLink       = "<img src='/admin/busiintro/images/bt_rrw.gif' border='0' alt=''>";
                endLink         = "<img src='/admin/busiintro/images/bt_ffw.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/admin/busiintro/images/bt_rw.gif' border='0' alt=''>";
                nextLinkNone    = "<img src='/admin/busiintro/images/bt_fw.gif'  border='0' alt=''>";
                startLinkNone   = "<img src='/admin/busiintro/images/bt_rrw.gif' border='0' alt=''>";
                endLinkNone     = "<img src='/admin/busiintro/images/bt_ffw.gif' border='0' alt=''>";
            }
            else if(flag.equals("M02_ADMIN"))
            {
                prevLink        = "<img src='/admin/ptcyard/images/bt_rw.gif' border='0' alt=''>";
                nextLink        = "<img src='/admin/ptcyard/images/bt_fw.gif' border='0' alt=''>";
                startLink       = "<img src='/admin/ptcyard/images/bt_rrw.gif' border='0' alt=''>";
                endLink         = "<img src='/admin/ptcyard/images/bt_ffw.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/admin/ptcyard/images/bt_rw.gif' border='0' alt=''>";
                nextLinkNone    = "<img src='/admin/ptcyard/images/bt_fw.gif'  border='0' alt=''>";
                startLinkNone   = "<img src='/admin/ptcyard/images/bt_rrw.gif' border='0' alt=''>";
                endLinkNone     = "<img src='/admin/ptcyard/images/bt_ffw.gif' border='0' alt=''>";
            }

            else if(flag.equals("M02"))
            {
                prevLink        = "<img src='/ptcyard/images/bt_rw.gif' border='0' alt=''>";
                nextLink        = "<img src='/ptcyard/images/bt_fw.gif' border='0' alt=''>";
                startLink       = "<img src='/ptcyard/images/bt_rrw.gif' border='0' alt=''>";
                endLink         = "<img src='/ptcyard/images/bt_ffw.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/ptcyard/images/bt_rw.gif' border='0' alt=''>";
                nextLinkNone    = "<img src='/ptcyard/images/bt_fw.gif'  border='0' alt=''>";
                startLinkNone   = "<img src='/ptcyard/images/bt_rrw.gif' border='0' alt=''>";
                endLinkNone     = "<img src='/ptcyard/images/bt_ffw.gif' border='0' alt=''>";
            }

            else if(flag.equals("M03_ADMIN"))
            {
                prevLink        = "<img src='/admin/ntcyard/images/bt_rw.gif' border='0' alt=''>";
                nextLink        = "<img src='/admin/ntcyard/images/bt_fw.gif' border='0' alt=''>";
                startLink       = "<img src='/admin/ntcyard/images/bt_rrw.gif' border='0' alt=''>";
                endLink         = "<img src='/admin/ntcyard/images/bt_ffw.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/admin/ntcyard/images/bt_rw.gif' border='0' alt=''>";
                nextLinkNone    = "<img src='/admin/ntcyard/images/bt_fw.gif'  border='0' alt=''>";
                startLinkNone   = "<img src='/admin/ntcyard/images/bt_rrw.gif' border='0' alt=''>";
                endLinkNone     = "<img src='/admin/ntcyard/images/bt_ffw.gif' border='0' alt=''>";
            }

            else if(flag.equals("M03"))
            {
                prevLink        = "<img src='/ntcyard/images/bt_rw.gif' border='0' alt=''>";
                nextLink        = "<img src='/ntcyard/images/bt_fw.gif' border='0' alt=''>";
                startLink       = "<img src='/ntcyard/images/bt_rrw.gif' border='0' alt=''>";
                endLink         = "<img src='/ntcyard/images/bt_ffw.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/ntcyard/images/bt_rw.gif' border='0' alt=''>";
                nextLinkNone    = "<img src='/ntcyard/images/bt_fw.gif'  border='0' alt=''>";
                startLinkNone   = "<img src='/ntcyard/images/bt_rrw.gif' border='0' alt=''>";
                endLinkNone     = "<img src='/ntcyard/images/bt_ffw.gif' border='0' alt=''>";
            }

            else if(flag.equals("M04"))
            {
                prevLink        = "<img src='/tipaintro/images/bt_rw.gif' border='0' alt=''>";
                nextLink        = "<img src='/tipaintro/images/bt_fw.gif' border='0' alt=''>";
                startLink       = "<img src='/tipaintro/images/bt_rrw.gif' border='0' alt=''>";
                endLink         = "<img src='/tipaintro/images/bt_ffw.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/tipaintro/images/bt_rw.gif' border='0' alt=''>";
                nextLinkNone    = "<img src='/tipaintro/images/bt_fw.gif'  border='0' alt=''>";
                startLinkNone   = "<img src='/tipaintro/images/bt_rrw.gif' border='0' alt=''>";
                endLinkNone     = "<img src='/tipaintro/images/bt_ffw.gif' border='0' alt=''>";
            }
            else if(flag.equals("M04_ADMIN"))
            {
                prevLink        = "<img src='/tipaintro/images/bt_rw.gif' border='0' alt=''>";
                nextLink        = "<img src='/tipaintro/images/bt_fw.gif' border='0' alt=''>";
                startLink       = "<img src='/tipaintro/images/bt_rrw.gif' border='0' alt=''>";
                endLink         = "<img src='/tipaintro/images/bt_ffw.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/tipaintro/images/bt_rw.gif' border='0' alt=''>";
                nextLinkNone    = "<img src='/tipaintro/images/bt_fw.gif'  border='0' alt=''>";
                startLinkNone   = "<img src='/tipaintro/images/bt_rrw.gif' border='0' alt=''>";
                endLinkNone     = "<img src='/tipaintro/images/bt_ffw.gif' border='0' alt=''>";
            }

            else if(flag.equals("M06"))
            {
                prevLink        = "<img src='/mypage/images/bt_rw.gif' border='0' alt=''>";
                nextLink        = "<img src='/mypage/images/bt_fw.gif' border='0' alt=''>";
                startLink       = "<img src='/mypage/images/bt_rrw.gif' border='0' alt=''>";
                endLink         = "<img src='/mypage/images/bt_ffw.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/mypage/images/bt_rw.gif' border='0' alt=''>";
                nextLinkNone    = "<img src='/mypage/images/bt_fw.gif'  border='0' alt=''>";
                startLinkNone   = "<img src='/mypage/images/bt_rrw.gif' border='0' alt=''>";
                endLinkNone     = "<img src='/mypage/images/bt_ffw.gif' border='0' alt=''>";
            }

            else if(flag.equals("WEBZINE"))
            {
                prevLink        = "<img src='/popup/images/bt_rw.gif' border='0' alt=''>";
                nextLink        = "<img src='/popup/images/bt_fw.gif' border='0' alt=''>";
                startLink       = "<img src='/popup/images/bt_rrw.gif' border='0' alt=''>";
                endLink         = "<img src='/popup/images/bt_ffw.gif' border='0' alt=''>";

                prevLinkNone    = "<img src='/popup/images/bt_rw.gif' border='0' alt=''>";
                nextLinkNone    = "<img src='/popup/images/bt_fw.gif'  border='0' alt=''>";
                startLinkNone   = "<img src='/popup/images/bt_rrw.gif' border='0' alt=''>";
                endLinkNone     = "<img src='/popup/images/bt_ffw.gif' border='0' alt=''>";
            }
            else
            {
                prevLink    = "◀";
                nextLink    = "▶";
                startLink   = "◀◀";
                endLink     = "▶▶";

                prevLinkNone    = "◁";
                nextLinkNone    = "▷";
                startLinkNone   = "◁◁";
                endLinkNone     = "▷▷";
            }
%>
    <table cellspacing="5" cellpadding="0">
    <tr height="10" align="center">
        <tr>
	        <td style='padding-top:3'>
<%          if (startKey != 1)
            {
                if(extend.equalsIgnoreCase("yes"))
                {   %>
        <a onClick="<%=functionName%>(1)" style="cursor:hand"><%=startLink%></a>
<%
				}
%>
        <a onClick="<%=functionName%>(<%=(startKey-1)%>)" style="cursor:hand"><%=prevLink%></a>
<%
			}
            else
            {
                if(extend.equalsIgnoreCase("yes"))
                {   %>

        <%=startLinkNone%>

<%              }   %>

        <%=prevLinkNone%>
<%          }
			%>
			</td>
			<td class="num">
<%
            for(long i = startKey; i <= stopKey ; i++)
            {
                if(nowPage == i)
                {   %>

        			<strong><%=i%>&nbsp;</strong>

<%              }
                else
                {
%>
        <a onClick="this.href='javascript:<%=functionName%>(<%=i%>)'" style="cursor:hand" class="num_o"><%=i%></a>&nbsp;
<%
            	}
            }
%>
		</td>
		<td style='padding-top:3'>
<%
            if (stopKey != totalPage)
            {
%>
        <a onClick="<%=functionName%>(<%=stopKey + 1 %>)" style="cursor:hand"><%=nextLink%></a>
<%
			if(extend.equalsIgnoreCase("yes"))
            {
%>
        <a onClick="<%=functionName%>(<%=totalPage%>)" style="cursor:hand"><%=endLink%></a>
<%              }
            }
            else
            {
%>
        <%=nextLinkNone%>
<%
				if(extend.equalsIgnoreCase("yes"))
                {
%>
			        <%=endLinkNone%>
<%              }
            }   %>
        </td>
    </tr>
    </table>
<%      }
    }
    else
        return;
%>

<%! // 필요 Function

    public static boolean isNull(String str)
    {
        if (str == null || str.equals(null) || str.equals("null") || str.equals(""))
            return true;
        else
            return false;
    }

    public static String nullTo(String str)
    {
        if (isNull(str))
            str = "";

        return str;
    }

    public static String nullTo(String str, String chstr)
    {
        if (isNull(str))
            str = chstr;

        return str;
    }

    public static long nullToLong(String str)
    {
        long result = 0;

        try{ result = Long.parseLong(nullTo(str, "0")); }catch(Exception e){ result = 0; }

        return result;
    }

    public static long nullToLong(String str, long value)
    {
        long result = 0;

        try{ result = Long.parseLong(nullTo(str, "" + value)); }catch(Exception e){ result = 0; }

        return result;
    }

    public static long nullToInt(String str)
    {
        long result = 0;

        try{ result = Integer.parseInt(nullTo(str, "0")); }catch(Exception e){ result = 0; }

        return result;
    }

    public static int nullToInt(String str, int value)
    {
        int result = 0;

        try{ result = Integer.parseInt(nullTo(str, "" + value)); }catch(Exception e){ result = 0; }

        return result;
    }
%>
