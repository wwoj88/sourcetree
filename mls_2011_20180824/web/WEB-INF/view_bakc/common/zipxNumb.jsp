<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page contentType="text/html;charset=euc-kr" %>
<%
  String dongName = request.getParameter("dongName") == null ? "" : request.getParameter("dongName");
  String srchCheck = request.getParameter("srchCheck") == null ? "" : request.getParameter("srchCheck");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>주소검색 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<style>
	.hxxx{
		height:207px;
	}
</style>
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/JavaScript"><!--

var arr1 = [["예시1","서울시 송파구 잠실동 27-12","잠실동 27-12","<strong>잠실동</strong>(동명) <strong>27-12</strong>(지번)"],["예시2","서울시 강동구 길동 403","길동 403","<strong>길동</strong>(동명) <strong>403</strong>(지번)"]];
var arr2 = [["예시1","서울시 송파구 잠실로 51-33","잠실로 51-33","<strong>잠실로</strong>(도로명) <strong>51-33</strong>(건물번호)"],["예시2","서울시 강동구 양재대로112길 57","양재대로112길 57","<strong>양재대로112길</strong>(도로명) <strong>57</strong>(건물번호)"]];
var arr3 = ["예시1","서울특별시 마포구 백범로31길 21","한국산업인력공단","건물명"];
var arr4 = [["* 검색방법:동(읍/면/리)명+지번"],["* 검색방법:도로명(~로,~길)+건물번호"],["* 검색방법:건물명"]];

$(function(){
	
	 var sidoNm='${param.sido_nm}'; 
     var rowSido=document.form1.sido_nm.length; 

     for ( var i = 0; i < rowSido; i++) {
        if (document.form1.sido_nm.options[i].value == sidoNm) {
            document.form1.sido_nm.options[i].selected = true;
            break;
        }
    }
    if($("#sido_nm option:selected").val() == '세종특별자치시'){
			options += '';
	    	$("#gugun_nm").html(options);
			$("#gugunSel").attr("style","display : none");	    	 
	}
    else if(document.form1.sido_nm.value !='--선택--'){
    	var gugunNm='${param.gugun_nm}';	
 		var options = {
 		type		: "POST",
 		url			: "/ajaxGugun/zip.do",
 		contentType : "application/x-www-form-urlencoded;charset=UTF-8",
 		data		: {"sidoNm":sidoNm},
 		dataType    : "json",
 		success	    : function(result){
 						var j = result.codeList;
 						var options = '';
 						for (var i = 0; i < j.length; i++) {
 							options += '<option value="' + j[i].gugunNm + '">' + j[i].gugunNm + '</option>';
 						}
 						$("#gugun_nm").html(options);
 					     for ( var i = 1; i < j.length; i++) {
 					        if (document.form1.gugun_nm.options[i].value == gugunNm) {
 					            document.form1.gugun_nm.options[i].selected = true;
 					              break;
 					         	}
 					     	} 
 						}
 				};
 			$.ajax(options);	 
     }else {
    	 var options = '';
    	 options += '<option value="' +  '">' + '--구군--' + '</option>';
    	 $("#gugun_nm").html(options);
     }

	$("#sido_nm").bind("change",function(e){
		
		if($("#sido_nm option:selected").val() == '세종특별자치시'){
			options += '';
	    	$("#gugun_nm").html(options);
			$("#gugunSel").attr("style","display : none");	    	 
		}
		else if($("#sido_nm option:selected").val() != '--선택--'){
			var sidoNm = $("#sido_nm option:selected").val();
			var options = {
			type		: "POST",
			url			: "/ajaxGugun/zip.do",
			contentType : "application/x-www-form-urlencoded;charset=UTF-8",
			data		: {"sidoNm":sidoNm},
			dataType    : "json",
			success		: function(result){
							var j = result.codeList;
							var options = '';
							for (var i = 0; i < j.length; i++) {
								options += '<option value="' + j[i].gugunNm + '">' + j[i].gugunNm + '</option>';
							}
							$("#gugun_nm").html(options);
						    $('#gugun_nm option:first').attr('selected', 'selected');
						}
			};
			$.ajax(options);
			$("#gugunSel").attr("style","");	
		}
		else{
			var options = '';
	    	 options += '<option value="' +  '">' + '--구군--' + '</option>';
	    	 $("#gugun_nm").html(options);
	    	 $("#gugunSel").attr("style","");
			}
	});
	
	var curSel = Number(${param.search});
	
	if(curSel != 0){
		var curNum = curSel-1;
		tbChange(curNum);
	}
	
	$("#search").change(function(){
		var curNum = Number($(this).val())-1;
		tbChange(curNum);
	})
	
	$("#dongName:text").keypress(function(e){
		if(e.keyCode == 13){
			e.preventDefault();
			$("#searchBtn").trigger('click');
		}
	});
})

function tbChange(param){
	var tBody = $("#examTable").find("tbody");
	tBody.empty();
	$("span").filter("[id=selSearch]").text(arr4[0]);
	var curNum1 = Number(param);
	if(curNum1 == 0){
		$("span").filter("[id=selSearch]").text(arr4[0]);
		for(var i=0; i<arr1.length; i++){
			var oTr = $("<tr>");
			for(var j=0; j<arr1[0].length; j++){
				if(j==1){
					var oTd = $("<td>").text(arr1[i][j]).addClass("ce");
				}else if(j==3){
					var oTd = $("<td>").html(arr1[i][j]).addClass("ce");
				}else{
					var oTd = $("<td>").text(arr1[i][j]).addClass();
				}
				oTd.appendTo(oTr);
			}
			oTr.appendTo(tBody);
		}
	}else if(curNum1 == 1){

		$("span").filter("[id=selSearch]").text(arr4[1]);
		for(var i=0; i<arr2.length; i++){
			var oTr = $("<tr>");
			for(var j=0; j<arr2[0].length; j++){
				if(j==1){
					var oTd = $("<td>").text(arr2[i][j]).addClass("ce");
				}else if(j==3){
					var oTd = $("<td>").html(arr2[i][j]).addClass("ce");
				}else{
					var oTd = $("<td>").text(arr2[i][j]).addClass();
				}
				oTd.appendTo(oTr);
			}
			oTr.appendTo(tBody);
			
		}
	}else if(curNum1 == 2){
		 $("span").filter("[id^=selSearch]").text(arr4[2]);
		var oTr = $("<tr>");
		for(var i=0; i<arr3.length; i++){
			if(i==1){
				var oTd = $("<td>").text(arr3[i]).addClass("ce");
			}else if(i==3){
				var oTd = $("<td>").text(arr3[i]).addClass("ce");
			}else{
				var oTd = $("<td>").text(arr3[i]).addClass();
			}
			oTd.appendTo(oTr);
		}
		oTr.appendTo(tBody);
	}else{
		tBody.empty();
	}	
}

  function init() {
  	var frm = document.form1;
    selop();
  	//frm.dongName.focus(); 웹접근성에서 상단에 있는 검색값에 포커스가 가야한다고 제시하여 주석처리 -  2014.08.29
  	frm.sido_nm.focus();
  }
 
  function choosePostNumb(zip1,zip2,sidoNm,gugunNm,roadNm,buldNo,buldSubNo,dongNm,buldNm,dongDiv) {
		 
	 var frm= document.zipregfrm;
	
	 frm.address2.value = '';
	 frm.apartNm.value = '';
	 frm.apartdong.value = '';
	 frm.apartho.value = '';
	 
	 frm.zip1.value=zip1;
	 frm.zip2.value=zip2;
	 frm.buldNm.value=buldNm;
	 
	 
	 if(buldSubNo == 0){
	 	if(dongDiv=='동' || dongDiv=='가'){
	 		frm.address1.value =sidoNm+" "+gugunNm+" "+roadNm+" "+buldNo;
	 	}else{
	 		frm.address1.value =sidoNm+" "+gugunNm+" "+dongNm+" "+roadNm+" "+buldNo;
	 	}
	 }
	 else {
	 	if(dongDiv=='동' || dongDiv=='가'){
	 		frm.address1.value =sidoNm+" "+gugunNm+" "+roadNm+" "+buldNo+"-"+buldSubNo;
	 	}else{
	 		frm.address1.value =sidoNm+" "+gugunNm+" "+dongNm+" "+roadNm+" "+buldNo+"-"+buldSubNo;	
	 	}
	 }
	 
	 if(buldNm != null){
		 var aprt=buldNm.substr(-3,3);
	 	if(aprt =='아파트'){
	 		frm.apartNm.value = buldNm;
	 		frm.apart_yn[0].checked=true;
	 		frm.apartdong.focus(); 
	 	}else if(aprt !='아파트'){
	 		 frm.address2.value=buldNm;
	 		 frm.apart_yn[1].checked=true;
	 		 frm.address2.focus();	
	 	}
	 }
	 if(dongDiv == '동' || dongDiv=='가'){
	 frm.dongNm.value=dongNm;
	 } 

  }
  
    function postNumb() {
	 var frm=document.zipregfrm;
      var zip1 = frm.zip1.value;
      var zip2 =frm.zip2.value;
      var road_addr_yn ='Y';
      var buldNm = frm.buldNm.value;
      var apartNm = frm.apartNm.value;
      var address2 =frm.address2.value;
      
      if(frm.address1.value ==''){
      	alert('주소를 선택하십시요!');
      	return false;
      }
      
      window.opener.document.form1.zip.value = zip1+zip2;
	  window.opener.document.form1.zipxNumb.value = zip1+"-"+zip2;
	  window.opener.document.form1.addr.value = frm.address1.value;
	  window.opener.document.form1.srchAddr.value = frm.address1.value;
	  window.opener.document.form1.road_addr_yn.value =road_addr_yn;
	  
	  var dong = frm.dongNm.value;
	
	 	if(apartNm != null){
	 		if(dong == ''){
	 			window.opener.document.form1.detlAddr.value = frm.apartdong.value+"동 "+frm.apartho.value+"호("+frm.apartNm.value+")";
	 		}
	 		else{
	 			window.opener.document.form1.detlAddr.value = frm.apartdong.value+"동 "+frm.apartho.value+"호 ("+frm.dongNm.value+","+frm.apartNm.value+")";
	 		 }
	 		window.opener.document.form1.detlAddr.focus();
	 		window.close();
	 	}
	
	 if(address2 != ''){
	 	 if(dong == ''){
	 		 if(buldNm==''){
	 			 window.opener.document.form1.detlAddr.value =frm.address2.value;
	 			 }
	 		 else if(buldNm !=''){
	 			window.opener.document.form1.detlAddr.value = frm.address2.value+"("+frm.buldNm.value+")";
	 		 }
	 		window.opener.document.form1.detlAddr.focus();
	 		window.close();
	 	 }else if(dong != ''){
	 		 if(buldNm==''){
	 	 		 window.opener.document.form1.detlAddr.value =frm.address2.value+"("+frm.dongNm.value+")";
	 	 	 }
	 		 else if(buldNm !=''){
	 			  window.opener.document.form1.detlAddr.value = frm.address2.value+"("+frm.dongNm.value+","+frm.buldNm.value+")";
	 		 }
	 		window.opener.document.form1.detlAddr.focus();
	 	  	 window.close();
	 	}
  	} 
	 
	 if(apartNm == '' && address2== ''){
		 window.opener.document.form1.detlAddr.value ="("+frm.dongNm.value+")";
		 window.opener.document.form1.detlAddr.focus();
 	  	 window.close();
	 }
  }
  
  function inputchk(num){
    var frm = document.getElementById("zipregfrm");
    if(num == 1){
        frm.apart_yn[0].checked=true;
        frm.address2.value = "";
        frm.apartNm.focus();
    }else if(num == 2){
        frm.apart_yn[1].checked=true;
        frm.apartNm.value = "";
        frm.apartdong.value = "";
        frm.apartho.value = "";
        frm.address2.focus();
	}
}

function selop(){
	var valueSR='${param.search}';
    var rowSR=document.form1.search.length;
    
      for ( var i = 0; i < rowSR; i++) {
        if (document.form1.search.options[i].value == valueSR) {
            document.form1.search.options[i].selected = true;
            break;
        }
    }
  }
  
 function IsRoad(sText){
    var ValidChars = "개길도로리";
    var IsRoad=true;
    var Char;
 
    for (i = 0; i < sText.length && IsRoad == true; i++) { 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1){
            IsRoad = false;
        }
    }
    return IsRoad;
}

function chkcnum(obj) {
    var num="1234567890-";
 
    for (i=0;i<obj.value.length; i++) {
        if (num.indexOf(obj.value.charAt(i)) < 0) {
        return false;
        }
    }
    return true;
}
 
function checkNull( toCheck ) 
{
     var chkstr = toCheck + "";
     var is_Space = true ;
    
     if ( ( chkstr == "") || ( chkstr == null ) ) 
       return( true );
 
     for ( j = 0 ; is_Space && ( j < chkstr.length ) ; j++)
     {
         if( chkstr.substring( j , j+1 ) != " " ) 
         {
           is_Space = false ;
         }
     }
     return ( is_Space );
}
 
function IsNumeric(sText){
    var ValidChars = "0123456789-";
    var IsNumber=true;
    var Char;
 
    for (i = 0; i < sText.length && IsNumber == true; i++) { 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1){
            IsNumber = false;
        }
    }
    return IsNumber;
}

   function postNumbSrch(){
	
	var frm = document.form1;
	var key=document.form1.search.value;
	
	 frm.law_dong_nm.value='';
	 frm.jibun_orig_no.value='';
	 frm.jibun_sub_no.value='';
	 frm.law_li_nm.value='';
	 
	if(frm.sido_nm.value=='--선택--'){
	alert('시도를 선택하십시오.');
	frm.sido_nm.focus();
	return false;
	}
	if(frm.gugun_nm.value==''&&frm.sido_nm.value!='세종특별자치시'){
	alert('시군구를 선택하십시오.');
	frm.gugun_nm.focus();
	return;
	}
	if(key==1){   //동/읍/면/리+지번 선택시
		var str = frm.dongName.value.split(" ");
		var dongRi=str[0];
		var RiJibun=str[1];
		var jibunNo=str[2];	

	if(dongRi != undefined){
          if(dongRi.slice(-1) == '동' || dongRi.slice(-1) == '가' || dongRi.slice(-1) == '읍' || dongRi.slice(-1) == '면'){
          		 frm.law_dong_nm.value= dongRi;
          		 frm.jibun_orig_no.value = RiJibun;	 
          		 
          	 	if(jibunNo == undefined){
					if(((frm.law_dong_nm.value == "")||(frm.jibun_orig_no.value==undefined) || (frm.jibun_orig_no.value=="") || !IsNumeric(frm.jibun_orig_no.value))){
	      				alert("동/읍/면/리/명+지번을 입력하십시오!");
	            		return ;
	      			}
				}else {
					 if(((frm.law_dong_nm.value == "")||(frm.jibun_orig_no.value==undefined) || (frm.jibun_orig_no.value==""))){
	      				alert("동/읍/면/리/명+지번을 입력하십시오!");
	            		return ;
      				}
				}
          }else if(dongRi.slice(-1) == '리'){
          	 frm.law_li_nm.value= dongRi;
          	 frm.jibun_orig_no.value = RiJibun;
          	 if ((key==1)&&((frm.law_li_nm.value == "")||(frm.jibun_orig_no.value==undefined) || !IsNumeric(frm.jibun_orig_no.value) ||(frm.jibun_orig_no.value.slice(-1)=="리") ||(frm.jibun_orig_no.value==""))) {
              		alert("동/읍/면/리/명+지번을 입력하십시오!");
           			 return false;
          		}
          }else{
          	alert("동/읍/면/리/명+지번을 입력하십시오!");
          	return false;
          }
        }
      if(RiJibun != undefined){
          if(RiJibun.slice(-1) == '리'){
          	 frm.law_li_nm.value= RiJibun;
          	 frm.jibun_orig_no.value = jibunNo;
          	 if ((key==1)&&((frm.law_li_nm.value == "")||(frm.jibun_orig_no.value=="") || (frm.jibun_orig_no.value == undefined) || !IsNumeric(frm.jibun_orig_no.value) || (frm.jibun_orig_no.value == ''))) {
              		alert("동/읍/면/리/명+지번을 입력하십시오!");
           			 return false;
          		}
          	}
          	else if(RiJibun.slice(-1) == '동' || !IsNumeric(RiJibun.slice(-1)) || frm.jibun_orig_no.value == undefined){
          		alert("동/읍/면/리/명+지번을 입력하십시오!");
           			 return false;
          	}
          	else {
          	 frm.jibun_orig_no.value = RiJibun;
          	}   	
  		 }
          if(frm.dongName.value.length<2){
              alert("검색어를 2글자이상으로입력하세요!");
              frm.dongName.focus();
              return false;
          }
    	 
          frm.jibun_orig_no.value = frm.jibun_orig_no.value.replace("산", "");
          var Temp     = frm.jibun_orig_no.value.split("-");
          frm.jibun_orig_no.value  = Temp[0];
          frm.jibun_sub_no.value = "";
      
          for (var i = 1; i < Temp.length; i++) {
        	  frm.jibun_sub_no.value = Temp[i];
              break;
          }
          
          frm.jibun_sub_no.value = frm.jibun_sub_no.value.replace(" ", "");
          
          if (frm.jibun_sub_no.value.length == 0) {
        	  frm.jibun_sub_no.value = "";
          }
       
          if ( (key.value==1)&&(!IsNumeric(frm.jibun_orig_no.value) || !IsNumeric(frm.jibun_sub_no.value)) ) {
              alert("번지는 반드시 숫자만 입력하세요!");
              return false;
          }     
         else{
          	frm.action = "/user/user.do";
			frm.submit();
          }
 	}
		if(key==2){    //도로명 선택시
		
		var str = frm.dongName.value.split(" ").join("");
          var strlen = str.length;
          var strdiv = "";
          for(var i = strlen-1; i > -1; i--){
              if(IsRoad(str.charAt(i))){
                  strdiv = i+1;
                  break;
              }
          } 
          frm.road_nm.value = str.substring(0,strdiv);
          frm.buld_orig_no.value = str.substring(strdiv,strlen);
          
          if(frm.road_nm.value == "시장북로"){
            alert("\"시장북로\"는 지번주소(법정동) 입니다.");
            return false;
          } 
      	    if ((key==2)&&((frm.road_nm.value == ""))) {
              alert("도로명+건물번호를 입력하십시오!");
              return false;
          }
           if(frm.dongName.value.length<2){
              alert("검색어를 2글자이상으로입력하세요!");
              frm.dongName.focus();
              return false;
          }
          var Temp     = frm.buld_orig_no.value.split("-");
          
          frm.buld_orig_no.value   = Temp[0];
          frm.buld_sub_no.value = "";
      
          for (var i = 1; i < Temp.length; i++) {
              frm.buld_sub_no.value = Temp[i];
              break;
          }
          
          frm.buld_sub_no.value = frm.buld_sub_no.value.replace(" ", "");
          
          if (frm.buld_sub_no.value.length == 0) {
              frm.buld_sub_no.value = "";
          }
          if ( (key==2)&&(!IsNumeric(frm.buld_orig_no.value) || !IsNumeric(frm.buld_sub_no.value)) ) {
              alert("건물번호는 반드시 숫자만 입력하세요!");
              return false;
          }  
           else{
          	frm.action = "/user/user.do";
			frm.submit();
          }
	}
		else if(key==3){		//건물명(아파트 등) 선택시
    	  
          if(frm.dongName.value==""){
           	  alert("건물명(아파트명 등)을 입력하세요!");
           	  frm.dongName.focus();
              return false;
          }else if(frm.dongName.value.length<2){
              alert("검색어를 2글자이상으로입력하세요!");
              frm.dongName.focus();
              return false;
          }  
          frm.buld_nm.value=frm.dongName.value;
          	frm.action = "/user/user.do";
			frm.submit();
      }
  }

//
--></script>
</head>

<body class="popup_bg" onLoad="javascript:init();">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>주소검색</h1>
		</div>
		<!-- //HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<div class="section">
			<span class="topLine"></span>
			<form name="form1" id="form1" method="post" class="relative" >
					<input type="submit" style="display:none;">
					<input type="hidden" name="srchCheck" value="Y">
					<input type="hidden" name="method" value="postNumbSrch">
					<input type="hidden" name="law_dong_nm" value="" />
					<input type="hidden" name="jibun_orig_no" value="" />
					<input type="hidden" name="jibun_sub_no" value="" />
					<input type="hidden" name="road_nm" value="" />
					<input type="hidden" name="buld_orig_no" value="" />
					<input type="hidden" name="buld_sub_no" value="" />
					<input type="hidden" name="buld_nm" value="" />
					<input type="hidden" name="law_li_nm" value="" />
				<table cellspacing="0" cellpadding="0" border="1" class="grid " summary="">
					<tbody>
						<tr>
							<td>
								<p class="pl10" style="height:15px">
									<label for="sido_nm" class="schDot1">시도( <img alt="" src="/images/2012/common/necessary.gif" class="mt5"/> )</label> 
								     <select id="sido_nm" name="sido_nm" id="sido_nm">
										<option>--선택--</option>
										<c:forEach items="${initZip}" var="initZip">
											<option value="${initZip.sidoNm}">${initZip.sidoNm}</option>
										</c:forEach>
									 </select>	
									<span id="gugunSel">							
									<label for="gugun_nm" class="schDot1 ml15">시군구( <img alt="" src="/images/2012/common/necessary.gif" class="mt5"/> )</label> 
									<select id="gugun_nm" name="gugun_nm" title="시군구 선택" style="width:140px; height:20px;">
										<option value=""></option>
									</select>
									</span>		
								</p>
								
								<p class="mt10 topDotted pt5 pl10" style="height:16px"><label for="sch3" class="schDot1">검색선택</label> 
								<select id="search" name="search" title="검색" style="width:150px; height:20px;">
                    			 	<option value="1" >동(읍/면/리)명+지번</option>
                    				<option value="2" >도로명+건물번호</option>
                    			 	<option value="3" >건물명</option>
                 				 </select>
                 				 <input id="dongName" name="dongName" value="<%=dongName%>" class="w30"/>
                 				 <span class="button small icon"><a href="#1" id="searchBtn" onclick="javascript:postNumbSrch();">검색</a><span class="delete"></span></span>
								<span class="topLine2 mt10"></span>
								
								<p class="black pt5 mt20"><span id="selSearch"> &nbsp;&nbsp;* 검색방법:동(읍/면/리)명+지번</span></p>
								<table  cellspacing="0" cellpadding="0" border="1" class="gray_grid w100 mt5 ce" summary="" id="examTable">
									<colgroup>
										<col width="10%">
										<col width="35%">
										<col width="15%">
										<col width="*">
									</colgroup>
								<thead>
									<tr>
										<th scope="col">예시</th>
										<th scope="col">검색할 주소</th>
										<th scope="col">검색어</th>
										<th scope="col">설명</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce" >예시1</td>
										<td>서울시 송파구 잠실동 27-12</td>
										<td align="center">잠실동 27-12</td>
										<td align="center"><strong>잠실동</strong>(동명) <strong>27-12</strong>(지번)</td>
									</tr>
									<tr>
										<td class="ce ">예시2</td>
										<td>서울시 강동구 길동 403</td>
										<td align="center">길동 403</td>
										<td align="center"><strong>길동</strong>(동명) <strong>403</strong>(지번)</td>
									</tr>
								</tbody>
							</table>
							</td>
						</tr>
					</tbody>
				</table>
				</form>
				<h2 class="mt20">검색결과</h2>
				<form name="searchResult" class="relative" action="#">
				<input type="submit" style="display:none;">
				<div class="border_box">
					<div class="overflow_y hxxx">
						
						<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
							<colgroup>
							<col width="20%">
							<col width="38%">
							<col width="38%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col">우편번호</th>
									<th scope="col">지번주소</th>
									<th scope="col">도로명주소</th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${!empty postNumbList}">
										<c:forEach items="${postNumbList}" var="postNumb">
											<tr>
						
												<td class="ce">${postNumb.ZIPCODE1}-${postNumb.ZIPCODE2}
												</td>
												<td class="ce">
																<c:choose>
																	<c:when test="${postNumb.JIBUN_SUB_NO == 0}">
																		 ${postNumb.SIDO_NM} ${postNumb.GUGUN_NM} ${postNumb.DONG_NM} ${postNumb.LAW_LI_NM} ${postNumb.JIBUN_ORIG_NO}
                                 					  				</c:when>
																	<c:otherwise>
                                 					 					${postNumb.SIDO_NM} ${postNumb.GUGUN_NM} ${postNumb.DONG_NM} ${postNumb.LAW_LI_NM} ${postNumb.JIBUN_ORIG_NO}-${postNumb.JIBUN_SUB_NO}
                                 					  				</c:otherwise>
																</c:choose>
												
												</td>
												<td class="ce"> 
												<a href="#1" onclick="javascript:choosePostNumb('${postNumb.ZIPCODE1}','${postNumb.ZIPCODE2}','${postNumb.SIDO_NM}','${postNumb.GUGUN_NM}','${postNumb.ROAD_NM}','${postNumb.BULD_ORIG_NO}','${postNumb.BULD_SUB_NO}','${postNumb.DONG_NM}','${postNumb.BULD_NM}','${postNumb.DONG_DIV}');">
														<u>
																<c:choose>
																	<c:when test="${postNumb.BULD_NM == null}">
																			<c:choose>
																				<c:when test="${postNumb.DONG_DIV == '동' || postNumb.DONG_DIV == '가'}">
																						${postNumb.SIDO_NM} ${postNumb.GUGUN_NM} ${postNumb.ROAD_NM} ${postNumb.BULD_ORIG_NO}
																				</c:when>
																				<c:otherwise>
																						${postNumb.SIDO_NM} ${postNumb.GUGUN_NM} ${postNumb.DONG_NM} ${postNumb.ROAD_NM} ${postNumb.BULD_ORIG_NO}
                                 					  							</c:otherwise>
																			</c:choose>	
																		 	<c:choose>
																				<c:when test="${postNumb.BULD_SUB_NO == 0}">
																					<c:choose>
																						<c:when test="${postNumb.DONG_DIV == '동' || postNumb.DONG_DIV == '가'}">
																								(${postNumb.DONG_NM})
																						</c:when>
																						<c:otherwise>
                                 					  									</c:otherwise>
																					</c:choose>	
                                 					  							</c:when>
																				<c:otherwise>
																					<c:choose>
																						<c:when test="${postNumb.DONG_DIV == '동' || postNumb.DONG_DIV == '가'}">
																								- ${postNumb.BULD_SUB_NO} (${postNumb.DONG_NM})
																						</c:when>
																						<c:otherwise>
																								- ${postNumb.BULD_SUB_NO}
                                 					  									</c:otherwise>
                                 					  								</c:choose>
                                 					  							</c:otherwise>
																			</c:choose>	
                                 									</c:when>
																	<c:otherwise>
																			<c:choose>
																				<c:when test="${postNumb.DONG_DIV == '동' || postNumb.DONG_DIV == '가'}">
																						${postNumb.SIDO_NM} ${postNumb.GUGUN_NM} ${postNumb.ROAD_NM} ${postNumb.BULD_ORIG_NO}
																				</c:when>
																				<c:otherwise>
																						${postNumb.SIDO_NM} ${postNumb.GUGUN_NM} ${postNumb.DONG_NM} ${postNumb.ROAD_NM} ${postNumb.BULD_ORIG_NO}
                                 					  							</c:otherwise>
																			</c:choose>	
                                 					 					 	<c:choose>
																					<c:when test="${postNumb.BULD_SUB_NO == 0}">
																							<c:choose>
																								<c:when test="${postNumb.DONG_DIV == '동' || postNumb.DONG_DIV == '가'}">
																									(${postNumb.DONG_NM},${postNumb.BULD_NM})
																								</c:when>
																								<c:otherwise>
																									(${postNumb.BULD_NM})
                                 					  											</c:otherwise>
																							</c:choose>	
                                 					  								</c:when>
																					<c:otherwise>
																							<c:choose>
																								<c:when test="${postNumb.DONG_DIV == '동' || postNumb.DONG_DIV == '가'}">
																									- ${postNumb.BULD_SUB_NO} (${postNumb.DONG_NM},${postNumb.BULD_NM})
																								</c:when>
																								<c:otherwise>
																									- ${postNumb.BULD_SUB_NO} (${postNumb.BULD_NM})
                                 					  											</c:otherwise>
																							</c:choose>	
                                 					  								</c:otherwise>
																				</c:choose>		
                                 					  					</c:otherwise>
																</c:choose>
														</u></a>
												</td>
												<input type="hidden" name="zip" value="${postNumb.ZIP}">
											</tr>
										</c:forEach>
								</c:if>
								 <!-- 주소 없는 경우 시작-->
			  						<% if ("Y".equals(srchCheck)) { %>
										<c:if test="${empty postNumbList}">
											<tr>
												<td colspan = "3" class="ce">해당하는 주소지가 없습니다.<br/> 다시 검색해 주세요. </td>
											</tr>
										</c:if>
									<% } else { %>
										<c:if test="${empty postNumbList}">
											<tr>
												<td colspan = "3" class="ce">주소를 입력하고 검색하십시오. </td>
											</tr>
										</c:if>
		    						<% } %>		
							</tbody>
						</table>
					</div>		
				</div>
				</form>
				<!-- <span class="topLine mt"></span> -->
				<div class="white_box " style="padding:10px; margin-top: 0px;">
							<div class="box5">
								<div class="box5_con floatDiv" style="padding: 12px 17px 12px; width:1000px; ">
									<p class="fl mt50"><img src="/images/2012/content/box_img4.gif" alt="" /></p>
									<div class="fl ml20 mt5" style="height:130px; ">
									<div style="float:left; width:350px;">
					        			<div class="addtopTextbox mt5">
					          			<p><span style="font-weight:bold;">&nbsp;&nbsp;- 해당주소를 선택 후 상세주소를 입력해 주세요.</span></p>
					      				</div>
					      	 			<div class="addtopTextbox mt5">
					      	 			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ㆍ아파트(동ㆍ호)</p>
					       	  			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- 아파트 명칭이 없는 경우 동호수만 입력</p>
					       	 			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- 호수는 있으나 동과 건물 명칭이 없는 경우 호수만 입력</p>
					      	 			</div>
					      	 			<div class="addtopTextbox mt10">
					      	 			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ㆍ아파트 이외</p>
					       	 			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- 동ㆍ호수로 표기할 수 없는 형태</p>
					       	  			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- 회사 또는 부서명, 건물명칭 등</p>
					      	 			</div> 
					    			</div>
				      			<div style="float:right; width:400px; margin-top: 20px ; margin-right: 20px;">
				      				<div class="addtopTextbox">
				        				<form name="zipregfrm" id="zipregfrm" action="/" method="post">
					        		<div class="inputtopbox">
							        	<ul >
							            	<li style="margin-bottom:5px;"><input name="zip1" id="zip1" type="text" size="5" style="IME-MODE:active;width:40px;" value="" class="popUpinput" title="우편번호 앞자리" readonly="readonly"/> - <input name="zip2" id="zip2" type="text" size="5" style="IME-MODE:active;width:40px;" value="" class="popUpinput" title="우편번호 뒷자리" readonly="readonly"/></li>
							                <li style="margin-bottom:5px;"><input name="address1" id="address1" type="text" size="55" style="IME-MODE:active;width:370px;" value="" class="popUpinput" title="기본주소" readonly="readonly"/></li>
							                <li>
							                	<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="새주소 상세주소입력" >
							                    	<caption>새주소 상세주소입력</caption>
							                        <tbody>
							                        	<tr>
							                            	<td style="padding:5px;"><input type="radio" name="apart_yn" id="apart_yn1" onclick="inputchk(1);" onkeypress="inputchk(1);" value="S1"/> <label for="apart_yn1">아파트(동ㆍ호)</label></td>
							                                <td><input name="apartNm" id="apartNm" type="text" size="15" style="IME-MODE:active;width:70px;" value="" class="popUpinput" title="아파트명" onclick="inputchk(1);" /> <input name="apartdong" id="apartdong" type="text" size="3" style="IME-MODE:active;width:30px;" value="" class="popUpinput" title="동" onclick="inputchk(1);" />동 <input name="apartho" id="apartho" type="text" size="3" style="IME-MODE:active;width:30px;" value="" class="popUpinput" title="호" onclick="inputchk(1);" />호</td>
							                                <td rowspan="2">(동명)&nbsp;<input name="dongNm" id="dongNm" type="text" size="5" style="IME-MODE:active;width:40px;" value="" class="popUpinput" title="동명" readonly="readonly" />
							                                <input type="hidden" name="buldNm" value="" /></td>
							                            </tr>
							                        	<tr>
							                            	<td style="padding:5px;"><input type="radio" name="apart_yn" id="apart_yn2" onclick="inputchk(2);" onkeypress="inputchk(2);" value="S2" /> <label for="apart_yn2">아파트 이외</label></td>
							                                <td><input name="address2" id="address2" type="text" size="28" style="IME-MODE:active;width:180px;" value="" class="popUpinput" title="아파트 이외" onclick="inputchk(2);" /></td>
							                            </tr>
							                        </tbody>
							                    </table>
							                </li>
							            </ul>
							      		</div>
						      		</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="mt20 style="border:none;">
					<p class="ce"><span class="button medium"><a id="saveUser" href="#1" onclick="javascript:postNumb();">완료</a></span>
					<span class="button medium gray"><a href="#1" onclick="javascript:self.close();">취소</a></span></p>
				</div>			
			</div>
		</div>	
	<!-- //CONTAINER end -->
	<!-- FOOTER str-->
	<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
	<!-- //FOOTER end -->
	<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="" /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>