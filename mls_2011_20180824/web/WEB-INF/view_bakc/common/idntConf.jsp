<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page contentType="text/html;charset=euc-kr" %>
<%
  String userIdnt = request.getParameter("userIdnt") == null ? "" : request.getParameter("userIdnt");
  String srchCheck = request.getParameter("srchCheck") == null ? "" : request.getParameter("srchCheck");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>아이디 중복확인 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/JavaScript">
<!--
  function init() {
  	var frm = document.form1;

  	frm.userIdnt.value = "<%=userIdnt%>";
		frm.action = "/user/user.do";
		frm.submit();
  }

  function endIdntConf() {
	  
	  window.opener.document.form1.userIdnt.value = "<%=userIdnt%>";
	  window.opener.document.form1.userIdChk.value = "Y";
  	  window.close();
  }
//-->
</script>
</head>

<% if ("Y".equals(srchCheck)) {  %>
<body class="popup_bg">
<% } else {  %>
<body onLoad="init();" class="popup_bg">
<% } %>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>아이디중복확인</h1>
		</div>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<div class="section">
				<span class="topLine"></span>
				<form name="form1" method="post" class="relative" action="#">
					<input type="hidden" name="method" value="getIdntConf">
					<input type="hidden" name="srchCheck" value="Y">
					<input type="submit" style="display:none;">
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
					<colgroup>
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row"><label for="userIdnt">아이디</label></th>
							<td><input id="userIdnt" name="userIdnt" value="<%=userIdnt%>" class="w30"> <span class="button small icon"><a href="#1" onclick="javascript:document.form1.submit();">중복확인</a><span class="delete"></span></span></td>
						</tr>
					</tbody>
				</table>
				<c:if test="${!empty userIdntMap.USER_IDNT}">
					<p class="ce mt30">
						<img src="/images/2012/content/ic_chk2.gif" alt="" class="vtop">
						<span class="ml20 inBlock lft"><strong class="orange underline line25 block">&quot;${userIdntMap.USER_IDNT}&quot; 이미 사용중인 아이디 입니다.</strong>다른 아이디를 입력하고 중복확인버튼을 누르세요!</span>
					</p>
				</c:if>
				<c:if test="${empty userIdntMap.USER_IDNT}">
					<p class="ce mt30">
						<img src="/images/2012/content/ic_chk.gif" alt="" class="vtop">
						<span class="ml20 inBlock lft"><strong class="blue2 underline line25 block">해당 아이디를 사용할 수 있습니다.</strong>이 아이디를 사용라려면 사용하기버튼을 누르세요!</span>
					</p>
					<p class="btnArea mt50"><span class="button medium"><a href="#1" onclick="javascript:endIdntConf();">사용하기</a></span></p>
					<p class="topDotted mt10 pt10 p11 ce">다른 아이디를 사용하려면 다른 아이디를 입력하고 중복확인버튼을 누르세요.</p>
				</c:if>
				</form>
			</div>
			
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="닫기" /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>