<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
  String dongName = request.getParameter("dongName") == null ? "" : request.getParameter("dongName");
  String srchCheck = request.getParameter("srchCheck") == null ? "" : request.getParameter("srchCheck");
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="pragma" content="no-cache"/>
<title>우편번호 검색</title>
<script type="text/JavaScript">
<!--
  function init() {
  	var frm = document.form1;

  	frm.dongName.focus();
  }

  function postNumbSrch(){
	var frm = document.form1;

	if (frm.dongName.value == "") {
		alert("주소입력에 데이터를 입력하십시오.");
		frm.dongName.focus();
		return;
	} else {
		//frm.method = "post";
		frm.action = "/user/user.do";
		frm.submit();
	}
  }

  function choosePostNumb(zipcode, addr, zip) {
  	opener.form1.zipxNumb.value = zipcode;
  	opener.form1.addr.value = addr;
  	opener.form1.zip.value = zip;
  	opener.form1.srchAddr.value = addr;
  	opener.form1.detlAddr.focus();

  	window.close();
  }
//-->
</script>
<link href="/css/popup.css" rel="stylesheet" type="text/css"/>
</head>
<body onLoad="javascript:init();">
<div id="wrap">
	<div class="popupTitle">
		<h2>주소검색</h2>
	</div>
	<div class="popupContents">
		<table border="0" align="center" cellpadding="0" cellspacing="5" class="grid">
		<form name="form1" method="post" action="/user/user.do">
			<input type="hidden" name="srchCheck" value="Y">
			<input type="hidden" name="method" value="postNumbSrch">
			<!-- 검색 시작-->
			<tr>
				<td height="12" align="center">거주지의 읍면동 명칭을 입력하시기 바랍니다.</td>
			</tr>
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<th class="thTitle">주소입력</th>
							<td class="tdData"><input type="text" id="dongName" name="dongName" value="<%=dongName%>" class="inputdata"/> <a href="javascript:postNumbSrch();"><img src="/images/button/srch2_btn.gif" alt="검색" align="middle" /></a></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>예) 서울시 강남구 신사동 이라면 '신사'만 입력하세요. </td>
			</tr>
		</form>
		</table>
		<h3>검색결과</h3>
		<div style="overflow: auto;">
			<!-- 검색결과 시작 -->
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="grid">
				<thead>
					<tr>
						<th class="tdLabel">우편번호</th>
						<th class="thTitle">주소</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${!empty postNumbList}">
			    <c:forEach items="${postNumbList}" var="postNumb">
					<tr>
						<td class="tdData C"><a href="javascript:choosePostNumb('${postNumb.POST_NUMB}','${postNumb.ADDR}','${postNumb.ZIP}');">${postNumb.POST_NUMB}</a></td>
						<td class="tdData"><a href="javascript:choosePostNumb('${postNumb.POST_NUMB}','${postNumb.ADDR}','${postNumb.ZIP}');">${postNumb.ADDR}</a></td>
						<input type="hidden" name="zip" value="${postNumb.ZIP}">
					</tr>
					</c:forEach>
		    </c:if>
			  <!-- 주소 없는 경우 시작-->
			  <% if ("Y".equals(srchCheck)) { %>
		    <c:if test="${empty postNumbList}">
			    <tr>
			    	<td height="13" align="center" colspan="2" class="tdData C">
			    		<table border="0" align="center" cellpadding="0" cellspacing="5">
			    			<tr>
			    				<td width="40" valign="top"><img src="/images/common/ico_inform.gif" width="32" height="32"/></td>
			    				<td align="left">해당하는 주소지가 없습니다.<br/> 다시 검색해 주세요. </td>
			    			</tr>
			    		</table>
			    	</td>
			    </tr>
			  </c:if>
			  <% } else { %>
			  <c:if test="${empty postNumbList}">
			    <tr>
			    	<td height="13" align="center" colspan="2" class="tdData C">
			    		<table border="0" align="center" cellpadding="0" cellspacing="5">
			    			<tr>
			    				<td width="40" valign="top"><img src="/images/common/ico_confirm2.gif" width="32" height="32"/></td>
			    				<td align="left">주소를 입력하고 검색하십시오.</td>
			    			</tr>
			    		</table>
			    	</td>
			    </tr>
		    </c:if>
		    <% } %>
			    <!-- 주소 없는 경우 끝-->
				</tbody>
			</table>
		</div>
		<!-- 검색결과 끝 -->
	</div>
	<div class="popupBottom"><a href="javascript:self.close();"><img src="/images/button/close_btn.gif" alt="닫기" width="76" height="23" /></a></div>
</div>
</body>
</html>
