<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page contentType="text/html;charset=euc-kr" %>
<% 
HttpSession s = request.getSession(true);
s.putValue("NmChkSec","98u9iuhuyg87");
%>
<%@ page language="java" import="Kisinfo.Check.IPINClient" %>
<%
  String userIdnt = request.getParameter("userIdnt") == null ? "" : request.getParameter("userIdnt");
  String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName");
  String srchCheck = request.getParameter("srchCheck") == null ? "" : request.getParameter("srchCheck");
  String sDiv = request.getParameter("sDiv") == null ? "" : request.getParameter("sDiv");
  String value = request.getParameter("value") == null ? "" : request.getParameter("value");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>실명확인 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script type="text/JavaScript">
<!--
  function init(){
	  document.form1.resdCorpNumb1.focus();	
}
  function complete(){
	  var sDiv = '<%=sDiv%>';
	  var value = '<%=value%>';
	  window.opener.document.hidForm.isSsnNo.value = "Y";
	  if(sDiv == '03'){
		  window.opener.fn_goStep('<%=value%>'); 
	  }else{
		  window.opener.fn_goSetp1('<%=sDiv%>'); 
	  }
	  window.close();
  }

  function ssnNoConf(){
		var frm = document.form1;
		if (frm.resdCorpNumb1.value == "") {
	  		alert("주민번호 앞자리를 입력하십시오.");
	  		frm.resdCorpNumb1.focus();
	  		return;
	  	} else if (frm.resdCorpNumb1.value.length != 6) {
	  		alert("주민번호 앞자리는 6자리 입니다.");
	  		frm.resdCorpNumb1.focus();
	  		return;
	  	} else if (frm.resdCorpNumb2.value == "") {
	  		alert("주민번호 뒷자리를 입력하십시오.");
	  		frm.resdCorpNumb2.focus();
	  		return;
	  	} else if (frm.resdCorpNumb2.value.length != 7) {
	  		alert("주민번호 뒷자리는 7자리 입니다.");
	  		frm.resdCorpNumb2.focus();
	  		return;
	  	} else if (!frm.agree.checked){
	  		alert("주민등록번호 처리에 동의해 주시기 바랍니다.");
	  		frm.agree.focus();
	  		return;
	  	}
		ssn1 = frm.resdCorpNumb1.value;
		ssn2 = frm.resdCorpNumb2.value;
		
		frm.resdCorpNumb.value = frm.resdCorpNumb1.value + frm.resdCorpNumb2.value;

		if(ssnCheck(ssn1, ssn2)) {              // 주민번호 체크
			return;
		}
		
		frm.method = "post";
		frm.action = "/NameCheck/nc_p_regiSsnNo.jsp";
		frm.submit();
		
	 //window.opener.document.form1.userIdChk.value = "Y";
  	 //window.close();
  }
  
//-->
</script>
</head>
<c:if test="${!empty regiChk}">
<body onload="complete();">
</body>
</c:if>
<c:if test="${empty regiChk}">
<body onload="init();" class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>실명확인</h1>
		</div>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<div class="section">
				<span class="topLine"></span>
				<form name="form1" method="post" class="relative" action="#">
					<input type="hidden" name="srchCheck" value="Y">
					<input type="hidden" name="resdCorpNumb">
					<input type="hidden" id="userIdnt" name="userIdnt" value="<%=userIdnt%>" class="w30">
					<input type="hidden" id="sDiv" name="sDiv" value="<%=sDiv%>" class="w30">
					<input type="hidden" id="value" name="value" value="<%=value%>" class="w30">
					<input type="submit" style="display:none;">
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
					<colgroup>
					<col width="25%">
					<col width="*">
					</colgroup>
					<tbody>
						<%-- <tr>
							<th scope="row"><label for="userIdnt">아이디</label></th>
							<td><input id="userIdnt" name="userIdnt" value="<%=userIdnt%>" class="w30"></td>
						</tr> --%>
						<tr>
							<th scope="row"><label for="userName">성명</label></th>
							<td><input type="text" id="userName" name="userName" value="<%=userName%>" class="w30" style="border-color: #ffffff;"></td>
						</tr>
						<tr>
							<th scope="row"><label for="resdCorpNumb1">주민등록번호</label></th>
							<td>
							<input type="text" id="resdCorpNumb1" name="resdCorpNumb1" title="주민번호(앞자리)" size="8" maxlength="6" onkeypress="cfInputNumRT(event);"/> 
							- 
							<input id="resdCorpNumb2" name="resdCorpNumb2" type="password" title="주민번호(뒷자리)" size="18" maxlength="7" onkeypress="cfInputNumRT(event);" />
							</td>
						</tr>
					</tbody>
				</table>
<!-- 					<p class="strong ine15 black" style="text-align: center;"><input type="checkbox" id="yes" name="agree" value="Y" />
									<label for="yes" style="font-size: 8pt;"><strong>주민등록번호 처리에 동의</strong> 합니다.</label></p> -->
					<p class="ce mt20">
						<span class="ml20 inBlock lft">법정허락 이용승인 온라인신청 서비스와 관련하여 <br/> 공인인증서 본인확인을 위한사용자의 주민등록번호를 활용하고 있습니다.</span>
					</p>			
					<p class="ce mt10">
						<img src="/images/2012/content/ic_chk.gif" alt="" class="vtop">
						<span class="mt10 ml20 inBlock lft"><input type="checkbox" id="yes" name="agree" value="Y" /><label for="yes" style="font-size: 8pt;"><strong>주민등록번호 처리에 동의</strong> 합니다.</label></span>
					</p>	
					<%--<p class="ce mt30">
						<img src="/images/2012/content/ic_chk2.gif" alt="" class="vtop">
						<span class="ml20 inBlock lft"><strong class="orange underline line25 block">&quot;${userIdntMap.USER_IDNT}&quot; 이미 사용중인 아이디 입니다.</strong>다른 아이디를 입력하고 중복확인버튼을 누르세요!</span>
					</p>--%>
					<%-- <p class="ce mt30">
						<img src="/images/2012/content/ic_chk.gif" alt="" class="vtop">
						<span class="ml20 inBlock lft"><strong class="blue2 underline line25 block">해당 아이디를 사용할 수 있습니다.</strong>이 아이디를 사용라려면 사용하기버튼을 누르세요!</span>
					</p> --%>
					<p class="btnArea mt20"><span class="button medium"><a href="#1" onclick="javascript:ssnNoConf();">확인</a></span></p>
					<!-- <p class="topDotted mt10 pt10 p11 ce">다른 아이디를 사용하려면 다른 아이디를 입력하고 중복확인버튼을 누르세요.</p> -->
				</form>
			</div>
			
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="닫기" /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</c:if>
</html>