<%@ page contentType="text/html; charset=euc-kr" language="java" %>
<%
response.setHeader("Cache-Control","no-cache,no-store");
response.setHeader("Pragma","no-cache,no-store");
	
String url = request.getParameter("url") == null ? "" : request.getParameter("url");
String queryString = request.getQueryString();
String nextPageUrl = queryString.substring(queryString.indexOf(url), queryString.length());
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<META http-equiv="Content-Type" content="text/html; charset=euc-kr">
<META http-equiv="Pragma" content="no-cache,no-store">
<META http-equiv="Cache-Control" content="no-cache,no-store">
<script type="text/javascript" >
<!--//
	// 20110418 : 인증서 로그인 기능삭제로 인한 인증서모듈 설치화면 접근시 강제 이동
	location.replace("/user/user.do?method=goLogin");
-->
location.replace("/user/user.do?method=goLogin");
</script>
<script type="text/javascript" >
var flag=true;

// 보안 소프트웨어 설치 성공
function ok(){
	if ( flag ){
		//alert('보안 소프트웨어가 자동으로 설치었습니다');
		location.replace("/user/user.do?method=goLogin");
	}
}

// 보안 소프트웨어 설치 실패
function fail(){
	flag = false;
 	alert( '보안 소프트웨어가 자동으로 설치되지 않았습니다.\n\n윈도우XP 서비스팩2를 설치하셨으면 상단의 설치탭을 눌러서 설치하십시오.\n아니면 보안 소프트웨어 설치 프로그램을 실행하여 설치하여 주십시오.\n\n"보안 소프트웨어 설치 프로그램 받기" 링크를 눌러서 설치 프로그램을 받으실 수 있습니다.\n파일 다운로드 창에서 "저장"을 눌러 바탕화면에 설치 프로그램을 저장하시고\n열려있는 웹 브라우저를 모두 닫으신 뒤에 실행시켜 주십시오.' );
}

// OBJECT TAG 자동적용
function __WS__(__NSID__){
	document.write( __NSID__.innerHTML );
	__NSID__.id = "";
}

</script>
<!--style-->
<style type="text/css">
<!--
	body { font-size: 9pt; line-height: 150%}
	td { font-size: 9pt; line-height: 150%}
	a:link {color: #0066cc; font-size: 9pt; text-decoration:underline;}
	a:active {color: #0066cc; font-size: 9pt; text-decoration:none}
	a:visited {color: #0066cc;  font-size: 9pt; text-decoration:none}
	a:hover {color: #0066cc; font-size: 9pt; text-decoration:underline;}
	.button   { height:20px; border-width:1; border-style:ridge; border-color:#cccccc; background-color:#F5f5f5;}
	.editbox  { border:1 solid black; background-color:oooooo; }
	select    { background-color:white;}
	p,br,body,table,td,select,input,form,textarea,option { font-family:굴림; font-size:9pt; }
-->
</style>
<title>저작권찾기</title>
</head>
<% 
/*
<!--marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" 스타일로 수정 필요-->
*/
/*
<!--cellspacing="0" cellpadding="0" 스타일로 수정 필요-->
*/
%>
<body style="margin:0;" onLoad="javascript:ok();">
<!-- <table width="100%" border="0" style="padding:0;height:100%;">
	<tr>
		<td>
			<table width="550" border="0" cellspacing="0" cellpadding="0" align="center" style="background-image:../images/loading002.gif">
				<tr>
					<td><img src="../images/loading001.gif" width="550" height="120" alt=""></td>
				</tr>
				<tr>
					<td height="40"> 
						<div align="center"><img src="../images/loading004.gif" width="500" height="23" alt=""> 
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<table width="550" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="59"></td>
								<td width="448">
									<img src="../images/loading006.gif" width="12" height="13" alt=""><b>한국정보인증(주)의 보안 소프트웨어를 
									설치중입니다.</b><br />
									<br />
									이미 다운로드를 받으신 분도 새버전을 설치중이니 기다려 주세요.<br />
									컴퓨터 및 네트웍의 상황에 따라 많은 시간이 소요되는 경우가 있습니다.<br />
									<br />
									잠시 기다려 주십시오....<br />
									<br />
									설치도중 <b>'보안경고'</b> 창이 뜨면 <b>( 예 )</b> 눌러서 S/W를 설치하세요.<br />
									<br />
									<img src="../images/loading005.gif" width="12" height="13" alt="">만약 오랜 시간이 지나도 설치되지 않으면 다음과 같이
					<br /><b>보안 소프트웨어 설치 프로그램</b>를 받아서 실행한 후에 다시 접속하여 주십시오.
					<br /><font color="006F66">▶</font> <b><a href="http://download.signgate.com/download/ews/ewsinstaller_full.exe">보안 소프트웨어 설치 프로그램 받기</a></b>
									<br /><font color="006F66">▶</font> 보안 소프트웨어 설치 프로그램을 바탕화면에 저장하십시오.
									<br /><font color="006F66">▶</font> 실행중인 웹브라우저 모두 종료하십시오.
									<br /><font color="006F66">▶</font> 바탕화면에 저장한 보안 소프트웨어 설치 프로그램을 실행하십시오.
									<br />
								</td>
								<td width="43"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><img src="../images/loading003.gif" width="550" height="48" alt=""></td>
				</tr>
			</table>
		</td>
	</tr>
</table> -->
</body>
</html>
<!-- EWSC -->
<!-- 
<comment id="__NSID__"><object id="SG_ATL" classid="clsid:9FC84F7D-D177-4A75-A7BB-429DA5BD0A3E" codeBase="http://download.signgate.com/download/ews/ewsinstaller.cab#version=3,0,0,13" style="display: none;" onError="javascript:fail();">
</object></comment> <script> __WS__(__NSID__); </script>
 -->
