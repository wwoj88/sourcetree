<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
  String userIdnt = request.getParameter("userIdnt") == null ? "" : request.getParameter("userIdnt");
  String srchCheck = request.getParameter("srchCheck") == null ? "" : request.getParameter("srchCheck");
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="pragma" content="no-cache"/>
<title>아이디 중복확인</title>
<script type="text/JavaScript">
<!--
  function init() {
  	var frm = document.form1;

  	frm.userIdnt.value = "<%=userIdnt%>";
		frm.action = "/user/user.do";
		frm.submit();
  }

  function endIdntConf() {
  	opener.form1.userIdnt.value = "<%=userIdnt%>";
  	opener.form1.userIdChk.value = "Y";

  	window.close();
  }
//-->
</script>
<link href="/css/popup.css" rel="stylesheet" type="text/css"/>
</head>
<% if ("Y".equals(srchCheck)) {  %>
<body>
<% } else {  %>
<body onLoad="init();">
<% } %>
<div id="wrap">
	<div class="popupTitle">
		<h2>아이디 중복체크</h2>
	</div>
	<div class="popupContents">
		<table border="0" align="center" cellpadding="0" cellspacing="5" class="grid">
			<!-- 검색 시작-->
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<form name="form1" method="post">
							  <input type="hidden" name="method" value="getIdntConf">
							  <input type="hidden" name="srchCheck" value="Y">
							<tr>
								<th class="thTitle">아이디</th>
								<td class="tdData"><input type="text" id="userIdnt" name="userIdnt" value="<%=userIdnt%>" class="inputdata"/> <a href="javascript:document.form1.submit();" ><img src="/images/button/overlap_btn.gif" alt="중복확인" align="middle" /></a></td>
							</tr>
							<!-- 주소 없는 경우 시작-->
							<tr>
								<td height="13" align="center" colspan="2" class="tdData">
									<c:if test="${!empty userIdntMap.USER_IDNT}">
										<table border="0" align="center" cellpadding="0" cellspacing="5">
											<tr>
												<td width="40" valign="top"><img src="/images/common/ico_error.gif" alt="" width="32" height="32"/></td>
												<td align="left"><span class="B">&quot;${userIdntMap.USER_IDNT}&quot;</span><span class="comment">&nbsp;이미 사용중인 아이디 입니다.</span><br />다른 아이디를 입력하고 중복확인을 누르세요.&nbsp;</span></td>
											</tr>
										</table>
									</c:if>
									<c:if test="${empty userIdntMap.USER_IDNT}">
										<table border="0" align="center" cellpadding="0" cellspacing="5">
											<tr>
												<td width="40" valign="top"><img src="/images/common/ico_info.gif" alt="" width="32" height="32"/></td>
												<td align="left"><span class="comment">해당 아이디는 사용할 수 있습니다. <br/> 이 아이디를 사용하려면 사용하기를 누르세요.&nbsp;</span></td>
											</tr>
											<tr>
												<td valign="top">&nbsp;</td>
												<td height="40" align="center" valign="top"><a href="javascript:endIdntConf();"><img src="/images/button/use_btn.gif" alt="사용하기" width="76" height="23" /></a></td>
											</tr>
											<tr>
												<td valign="top">&nbsp;</td>
												<td align="left">다른 아이디를 사용하려면 다른 아이디를 <br />입력하고 중복확인을 누르세요.</td>
											</tr>
										</table>
									</c:if>
								</td>
							</tr>
						  </form>
							<!-- 주소 없는 경우 끝-->
						</tbody>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<div class="popupBottom"><a href="javascript:self.close();"><img src="/images/button/close_btn.gif" alt="닫기" width="76" height="23" /></a></div>
</div>
</body>
</html>
