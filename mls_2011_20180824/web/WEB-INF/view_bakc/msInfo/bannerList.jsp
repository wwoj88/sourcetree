<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이용안내 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">

<!--
 	
	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }
//-->
</script>	
</head>

<body>

	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(4);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<form name="form1" method="post" action="#">
			<input type="hidden" name="filePath">
			<input type="hidden" name="fileName">
			<input type="hidden" name="realFileName">
			<input type="submit" style="display:none;">
		</form>
		
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_4.gif" alt="알림마당" title="알림마당" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1"><img src="/images/2011/content/sub_lnb0401_off.gif" title="공지사항" alt="공지사항" /></a></li>
					<li id="lnb2"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1"><img src="/images/2011/content/sub_lnb0402_off.gif" title="자주묻는 질문" alt="자주묻는 질문" /></a></li>
					<li id="lnb3"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=2&amp;page_no=1"><img src="/images/2011/content/sub_lnb0403_off.gif" title="묻고답하기" alt="묻고답하기" /></a></li>
					<li id="lnb4"><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=2&amp;menuSeqn=5&amp;page_no=1"><img src="/images/2011/content/sub_lnb0404_off.gif" title="홍보자료" alt="홍보자료" /></a></li>
					<!--<li id="lnb5"><a href="/board/board.do?method=vocList"><img src="/images/2011/content/sub_lnb0407_off.gif" title="고객의 소리(VOC)" alt="고객의 소리(VOC)" /></a></li>
					--><li id="lnb6"><a href="/main/main.do?method=bannerList"><img src="/images/2011/content/sub_lnb0405_off.gif" title="홍보관" alt="홍보관" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb6");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>알림마당</span><em>홍보관</em></p>
					<h1><img src="/images/2011/title/content_h1_0405.gif" alt="홍보관" title="홍보관" /></h1>

					<div class="section">
                    	<div class="w100">
                    		<p class="pb20"><img src="/images/2011/content/bannerlist_lnb01.gif" alt="배너사이즈 301_72 pixel" title="배너사이즈 301_72 pixel" /></p>
                            <p class="pagination pb10"><img src="/images/2011/banner/banner_301_72.jpg" alt="배너 301_72 이미지" title="배너 301_72 이미지" /></p>
                            <p class="pagination"><a href="#1" onclick="javascript:fn_fileDownLoad('G:/Server/mls/mls_2011/web/images/2011/banner/','배너 301_72_이미지.jpg','banner_301_72.jpg')"><img src="/images/2011/button/btn_app10.gif" alt="배너다운로드" title="배너다운로드" /></a></p>
                            <p class="pl10 pb10"><img src="/images/2011/content/bannerlist_lnb01_01.gif" alt="배너달기 소스 코드" title="배너달기 소스 코드" /></p>
                            <p class="bgGray1 pl10 pt10 pb10">&lt;a href="http://www.right4me.or.kr"&gt;&lt;image src="http://www.right4me.or.kr"/images/2011/banner/banner_301_72.jpg"&gt;&lt;/a&gt;</p>
						</div>
                        <div class="w100 pt10">
                    		<p class="pb20"><img src="/images/2011/content/bannerlist_lnb02.gif" alt="배너사이즈 268_57 pixel" title="배너사이즈 268_57 pixel" /></p>
                            <p class="pagination pb10"><img src="/images/2011/banner/banner_268_57.jpg" alt="배너 268_57 이미지" title="배너 268_57 이미지" /></p>
                            <p class="pagination"><a href="#1" onclick="javascript:fn_fileDownLoad('G:/Server/mls/mls_2011/web/images/2011/banner/','배너 268_57_이미지.jpg','banner_268_57.jpg')"><img src="/images/2011/button/btn_app10.gif" alt="배너다운로드" title="배너다운로드" /></a></p>
                            <p class="pl10 pb10"><img src="/images/2011/content/bannerlist_lnb01_01.gif" alt="배너달기 소스 코드" title="배너달기 소스 코드" /></p>
                            <p class="bgGray1 pl10 pt10 pb10">&lt;a href="http://www.right4me.or.kr"&gt;&lt;image src="http://www.right4me.or.kr"/images/2011/banner/banner_268_57.jpg"&gt;&lt;/a&gt;</p>
						</div>
                        <div class="w100 pt10">
                    		<p class="pb20"><img src="/images/2011/content/bannerlist_lnb03.gif" alt="배너사이즈 190_86 pixel" title="배너사이즈 190_86 pixel" /></p>
                            <p class="pagination pb10"><img src="/images/2011/banner/banner_190_86.jpg" alt="배너 190_86 이미지" title="배너 190_86 이미지" /></p>
                            <p class="pagination"><a href="#1" onclick="javascript:fn_fileDownLoad('G:/Server/mls/mls_2011/web/images/2011/banner/','배너 190_86_이미지.jpg','banner_190_86.jpg')"><img src="/images/2011/button/btn_app10.gif" alt="배너다운로드" title="배너다운로드" /></a></p>
                            <p class="pl10 pb10"><img src="/images/2011/content/bannerlist_lnb01_01.gif" alt="배너달기 소스 코드" title="배너달기 소스 코드" /></p>
                            <p class="bgGray1 pl10 pt10 pb10">&lt;a href="http://www.right4me.or.kr"&gt;&lt;image src="http://www.right4me.or.kr"/images/2011/banner/banner_190_86.jpg"&gt;&lt;/a&gt;</p>
						</div>
                        <div class="w100 pt10">
                    		<p class="pb20"><img src="/images/2011/content/bannerlist_lnb04.gif" alt="배너사이즈 150_116 pixel" title="배너사이즈 150_116 pixel" /></p>
                            <p class="pagination pb10"><img src="/images/2011/banner/banner_150_116.jpg" alt="배너 150_116 이미지" title="배너 150_116 이미지" /></p>
                            <p class="pagination"><a href="#1" onclick="javascript:fn_fileDownLoad('G:/Server/mls/mls_2011/web/images/2011/banner/','배너 150_116_이미지.jpg','banner_150_116.jpg')"><img src="/images/2011/button/btn_app10.gif" alt="배너다운로드" title="배너다운로드" /></a></p>
                            <p class="pl10 pb10"><img src="/images/2011/content/bannerlist_lnb01_01.gif" alt="배너달기 소스 코드" title="배너달기 소스 코드" /></p>
                            <p class="bgGray1 pl10 pt10 pb10">&lt;a href="http://www.right4me.or.kr"&gt;&lt;image src="http://www.right4me.or.kr"/images/2011/banner/banner_150_116.jpg"&gt;&lt;/a&gt;</p>
						</div>
						
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
