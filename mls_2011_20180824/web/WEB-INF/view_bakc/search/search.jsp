<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.json.simple.JSONObject"%>
<%-- <%@page import="org.codehaus.jackson.JsonParser"%> --%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@ page language="java" contentType="text/html; charset=euc-kr"
    pageEncoding="euc-kr"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<title><c:if test="${genreCd!='total'}">${genreCd}</c:if><c:if test="${genreCd=='total'}">전체</c:if> | 분야별 권리자 찾기 | 권리자찾기</title>
</head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">

<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 

//<!--

 
 function crosPop(){
    window.open("https://www.cros.or.kr/psnsys/cmmn/infoPage.do?w2xPath=/ui/twc/sch/regInfSerc/regInfSercList.xml");
  }

 
 function statBoRegiPop(){
    window.open("/statBord/statBo06Regi.do", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes"); 
 }
 
 function statBoSelect(){
    window.open("/statBord/statBo08Select.do?query=${query}", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes");  
}
 
 function goSearch(){
    
      var fnm = document.search;
      var query=fnm.query.value;
      query=query.replace(/^\s+/,""); 

      if (query == "" || query == null) {
          alert("검색어를 입력하세요.");
         $('#query').focus();
          return;
      }

      rangeValue="";
      for(i=0; i<fnm.range.length; i++){      
        if(fnm.range[i].checked){       
          if(rangeValue!="") rangeValue = rangeValue+",";
          rangeValue = rangeValue+fnm.range[i].value;               
        }
      }
      var target = document.getElementById("collection");
      fnm.genreCd.value = target.options[target.selectedIndex].text;   
      fnm.arr_range.value = rangeValue;    

      fnm.submit();
      return false;
}
 
 function goCategory(Str){
   //alert(Str);
    var frm = document.search;
    if(Str=='reg_copyright'){
     // frm.collection.value = 'reg_copyright';
     $("#collection option:eq(1)").prop("selected", true);
      frm.subPageStr.value = 'Y';
    }else if(Str=='dg_music1'){
      frm.collection.value = 'dg_music1,dg_literature,dg_movie,dg_news';
      frm.subPageStr.value = 'dg';
    }else if(Str=='ALL'){
      frm.collection.value = 'ALL';
      frm.subPageStr.value = 'Y';
    }else{
      frm.collection.value = Str;
      frm.subPageStr.value = 'Y';
    }
    
    
    
    frm.action = "/search/search.do";
    //console.log(frm)
    frm.submit();
}
function comma(num){
    var len, point, str; 
       
    num = num + ""; 
    point = num.length % 3 ;
    len = num.length; 
    str = num.substring(0, point); 
    while (point < len) { 
        if (str != "") str += ","; 
        str += num.substring(point, point + 3); 
        point += 3; 
    } 
    return str;
}

</script>

<!-- 공통 스크립트 -->
<script language="javascript">

/**
 * 보고서를 구동 시킨다.
 *
 * turl     : 보고서를 실행시킬 URL
 * servlet    : 보고서를 생성할 URL
 * reportFile : 컴파일된 보고서 서실 파일
 * process    : 프로세스 (PDF)
 * rptTitle   : 보고서 제목
 * type     : 보고서에서 사용되는 데이터 타입 (xml,json,sql)
 */
function RXEnt_ViewReport(turl, servlet, reportFile, process, rptTitle, type, xmldata, f){
   
//   alert('turl['+turl
//               +'] servlet['+servlet
//                  +'] reportFile[]'+reportFile
//                  +'] xmldata['+xmldata+']');
   console.log('turl['+turl // 보고서를 실행시킬 URL
             +'] servlet['+servlet // 보고서를 생성할 URL
             +'] reportFile[]'+reportFile // 컴파일된 보고서 서실 파일
             +'] xmldata['+xmldata+']'); // 보고서에서 사용되는 xml 데이터
        frm = makeForm(turl);
        frm.appendChild(addData('Servlet', servlet));
        frm.appendChild(addData('ReportFile', reportFile));
        frm.appendChild(addData('Process', process));
        frm.appendChild(addData('rptTitle', rptTitle));
//        frm.appendChild(addData('data', data.data));
        frm.appendChild(addData('data', xmldata));
        frm.appendChild(addData('type', type));
//        if(bw=="edge"){
//          frm.target = "ReportView";
//        }else{
          window.open ("","ReportView",f);
          frm.target = "ReportView";
//        }
        frm.submit();
}
function makeForm(url){
  var f = document.createElement("form");
    f.setAttribute("method", "post");
    f.setAttribute("action", url);
    document.body.appendChild(f);
      
    return f;
}

function addData(name, value){
  var i = document.createElement("input");
  i.setAttribute("type","hidden");
  i.setAttribute("name",name);
  i.setAttribute("value",value);
  return i;
}

</script>

<!-- 증명서 종류에 따라 각각 호출 -->
<script type="text/javascript">
function SmartCert_ViewReport1() {
  var userName = '${user.userName}';
  if(userName=""||userName==null){
    alert("로그인이 필요한 서비스입니다.");
    return false;
  }
  
   // :TODO 증명서에서 사용되는 xml 데이터
  /*  var xmldata = '<?xml version="1.0" encoding="euc-kr"?><Root><LData><CURRENTTIME>20.04.30.000<\/CURRENTTIME><CURRENTDATE>20120906</CURRENTDATE><MESSAGE>정상처리되었습니다.<\/MESSAGE><MSG_CD>0000<\/MSG_CD><RT_CD>0</RT_CD><EMP_ID>99999997</EMP_ID><EMP_DEPT>99999998</EMP_DEPT><TERM_IP>999.999.999.998</TERM_IP><MEDIA>H</MEDIA><TR_CODE>CU1125</TR_CODE><CUST_NO>2003334</CUST_NO><ISSUE_UNIT>11</ISSUE_UNIT><USE>test</USE><CURRENCY_CD>KRW</CURRENCY_CD><PRN_LANG>11</PRN_LANG><BASIC_RATE>0</BASIC_RATE><NOREMN_PRN_YN>N</NOREMN_PRN_YN><TAX_STATUS>11</TAX_STATUS><STD_DT>20120813</STD_DT><PROCESS_GB>11</PROCESS_GB><out_list><LMultiData><LData><STDPRC>STDPRCSTDPRC</STDPRC><CASH_AMT>333,333</CASH_AMT><EVAL_AMT>284,8294@@@</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>예수금</FUND_NM><ORIGN_AMT>2848294</ORIGN_AMT><ACCT_NO>888888888100</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>0</REMN_QTY></LData><LData><EVAL_AMT>393869</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>삼성 당신을 위한 코리아대표그룹 증권 제1호[주식] (A)</FUND_NM><ORIGN_AMT>267231</ORIGN_AMT><ACCT_NO>888888888101</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>262,579</REMN_QTY></LData><LData><EVAL_AMT>90296</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>신영 마라톤 증권[주식] (A형)</FUND_NM><ORIGN_AMT>90000</ORIGN_AMT><ACCT_NO>888888888102</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>101107</REMN_QTY></LData><LData><EVAL_AMT>98959</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>신영 마라톤 증권[주식] (A형)</FUND_NM><ORIGN_AMT>100000</ORIGN_AMT><ACCT_NO>888888888103</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>110807</REMN_QTY></LData><LData><EVAL_AMT>1410733</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>생명 기업가치 향상 장기 증권 자[주식] (A)</FUND_NM><ORIGN_AMT>1485150</ORIGN_AMT><ACCT_NO>888888888104</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>1538809</REMN_QTY></LData><LData><EVAL_AMT>164224</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>생명 기업가치 향상 장기 증권 자[주식] (A)</FUND_NM><ORIGN_AMT>198020</ORIGN_AMT><ACCT_NO>888888888105</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>179134</REMN_QTY></LData><LData><EVAL_AMT>667658</EVAL_AMT><ADDRMRK_NM>생명 펀드투자</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>생명 파워인덱스 증권 자 1호[주식] (A1)</FUND_NM><ORIGN_AMT>694449</ORIGN_AMT><ACCT_NO>888888888106</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>725715</REMN_QTY></LData><LData><EVAL_AMT>2072000</EVAL_AMT><ADDRMRK_NM>두번째통장</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>예수금</FUND_NM><ORIGN_AMT>2072000</ORIGN_AMT><ACCT_NO>888888888700</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>0</REMN_QTY></LData><LData><EVAL_AMT>200000</EVAL_AMT><ADDRMRK_NM>안내문구수정2</ADDRMRK_NM><CHECK_RECP_AMT>0</CHECK_RECP_AMT><FUND_NM>예수금</FUND_NM><ORIGN_AMT>200000</ORIGN_AMT><ACCT_NO>888888888900</ACCT_NO><CUST_NM>김 X</CUST_NM><RNAME_NO>8888888880000</RNAME_NO><REMN_QTY>0</REMN_QTY></LData></LMultiData></out_list><SUM_CHECK_RECP_AMT>0</SUM_CHECK_RECP_AMT><SUM_BALANCE_AMOUNT_USD>0</SUM_BALANCE_AMOUNT_USD><SUM_CASH_AMT>5120294</SUM_CASH_AMT><SUM_BALANCE_AMOUNT>7946033</SUM_BALANCE_AMOUNT></LData></Root>'; */
    /* var xmldata = '<?xml version="1.0" encoding="euc-kr"?><Root><LData><USER_IDNT>성명 출력</USER_IDNT><WORKS_TITLE>저작물 제목</WORKS_TITLE><WORKS_CNT>5</WORKS_CNT><CLMS_CNT>2</CLMS_CNT></LData></Root>'; */
    var xmldata = '<?xml version="1.0" encoding="euc-kr"?><root><USER_IDNT>${user.userName}</USER_IDNT><WORKS_TITLE>${query}</WORKS_TITLE><WORKS_CNT>43,952,562</WORKS_CNT><CLMS_CNT>33,852,184</CLMS_CNT><CROS_CNT>305,995</CROS_CNT><UCI_CNT>0</UCI_CNT></root>';
  // 팝업 세팅
   var f = "width=700,height=800,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=0,top=0";
  
  RXEnt_ViewReport(
      "http://222.231.43.109:8086/smartcert/cdoc/eform/rxscert/run/smartcert.jsp" // [공통] 증명서를 실행시킬 URL = > 해당 url ip port context 맞춰준다 예) http://localhost:9080/rxent/
      ,"smartcert_service.jsp" // [공통] 증명서를 생성할 URL
      ,"test/basiccert" // :TODO 컴파일된 증명서 서실 파일
      ,"PDF"
      ,"기본증명서"
      ,"xml"
      ,xmldata
      ,f
      );
}
</script>

<!-- </head> -->

<body>
    
    <!-- HEADER str-->
    <jsp:include page="/include/2017/header.jsp" />
    
<script type="text/javascript"> 
$(function(){
  
  var genreCd ='${genreCd}';
  var menuFlag = '${menuFlag}';
  var collectionName = '${collectionName}';
  
   
  collectionName = collectionName.substring(0,collectionName.indexOf(','));
  //console.log(collectionName);
  if(collectionName!=""){
    $('#collection option[value='+collectionName+']').attr('selected','selected'); //*/ 
  }
  
  
  
  var flag = false;
  
  for(var i = 0 ; i < 12 ; i++){
    
    if($("#topMenu"+i).html()==genreCd){
      $("#topMenu"+i).parent().parent().attr("class","first on");
      flag = true;
      console.log($("#topMenu"+i).html()==genreCd);
    }
    
    if($("#leftMenu"+i).html()==genreCd){
      $("#leftMenu0").attr("class","");
      $("#leftMenu"+i).attr("class","on");
    }
  }
  if(flag==false){
    $("#topMenu0").parent().parent().attr("class","first on");
  }
  
  if($('.con_rt_hd_title').html()!=genreCd){
    if(genreCd=='total'){
      if(menuFlag=='N'){
        $('.con_rt_hd_title').html('상당한 노력 신청 서비스');
      }else{
        $('.con_rt_hd_title').html('전체');
      }
      
    }else{
      $('.con_rt_hd_title').html(genreCd);
    }
  }
  
  $('#regCnt').html(comma($('#regCnt').html()));
  $('#subCnt').html(comma($.trim($('#subCnt').html())));
  $('#dgCnt').html(comma($.trim($('#dgCnt').html())));
  
})  

$(window).load(function() {
    $('img').each(function() {
      if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
        // image was broken, replace with your new image
        this.src = '/images/noimg/noimg.jpg';
        this.alt='이미지없음';
      }
    });
  });

</script>
<!--    <script type="text/javascript">initNavigation(1);</script> -->
    <!-- GNB setOn 각페이지에 넣어야합니다. -->

    <!-- HEADER end -->
    
    <!-- content st -->
      <div id="contents">
      ${menuFlag}
       <c:if test="${menuFlag!='N'}">
            <div class="con_lf">
      <h2><div class="con_lf_big_title">분야별 <br>권리자 찾기</div></h2>
        <ul class="sub_lf_menu">
        <li><a href="#" id="leftMenu0" class="on" onclick="javascript:goInnerSearch('999'); return false;">전체</a></li>
        <li><a href="#" id="leftMenu1" onclick="javascript:goInnerSearch('1'); return false;">음악</a></li>
        <li><a href="#" id="leftMenu2" onclick="javascript:goInnerSearch('2'); return false;">어문</a></li>
        <li><a href="#" id="leftMenu3" onclick="javascript:goInnerSearch('3'); return false;">방송대본</a></li>
        <li><a href="#" id="leftMenu4" onclick="javascript:goInnerSearch('4'); return false;">영화</a></li>
        <li><a href="#" id="leftMenu5" onclick="javascript:goInnerSearch('5'); return false;">방송</a></li>
        <li><a href="#" id="leftMenu6" onclick="javascript:goInnerSearch('6'); return false;">뉴스</a></li>
        <li><a href="#" id="leftMenu7" onclick="javascript:goInnerSearch('7'); return false;">미술</a></li>
        <li><a href="#" id="leftMenu8" onclick="javascript:goInnerSearch('8'); return false;">이미지</a></li>
        <li><a href="#" id="leftMenu9" onclick="javascript:goInnerSearch('9'); return false;">사진</a></li>
        <li><a href="#" id="leftMenu10" onclick="javascript:goInnerSearch('99'); return false;">기타</a></li>
        </ul>
      </div>
      </c:if>
      <c:if test="${menuFlag eq 'N'}">
      <div class="con_lf">
      <div class="con_lf_big_title">법정허락<br>승인 신청</div>
        <ul class="sub_lf_menu">
        <!-- <li><a href="javascript:goInnerSearch('999');">상당한 노력 신청 서비스</a></li> -->
        <!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
        <!-- <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
        <li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
        <li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
        <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
        <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
        <li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li>
        </ul>
      </div>
      </c:if>
      <%
              /* request.getAttribute("searchResult") */
              String  jsonData = (String) request.getAttribute("searchResult");// 검색엔진 전체 json데이터 
              JSONParser jsonParser = new JSONParser();// json 파서
              
              JSONObject jsonObject2 = (JSONObject)jsonParser.parse( jsonData );//json 데이터 파싱 시작
              //out.println(jsonObject2);
              Map jsonMap = (Map) jsonObject2.get( "SearchQueryResult" ); 
               JSONArray collection = (JSONArray) jsonMap.get("Collection");  
               Map documentSet = (Map) collection.get( 0 );
              Map documentSetMap = (Map) documentSet.get("DocumentSet");
              JSONArray documentJA = (JSONArray) documentSetMap.get("Document");
              
              //out.print("collection : " + collection.get(0));
               
              JSONObject jsonCntObject = (JSONObject)request.getAttribute("SearchColCount");//json 데이터 파싱 시작
              
              Map jsonCntMap = (Map)jsonCntObject.get("SearchColCount");
              
              Iterator<String> keys = jsonCntMap.keySet().iterator();
            //  out.print("jsonCntMap : " + jsonCntMap);
              int subCnt = 0;
              int totalCnt = 0;
              
              while( keys.hasNext() ){
                      String key = keys.next();
                      int temp = (Integer) jsonCntMap.get(key);
                      if(!key.equals( "reg_copyright" ) && !key.equals( "reg_copyright") && temp!=-1 && temp !=-972 && !key.substring( 0,2 ).equals( "dg" ) ){
                        subCnt += temp;
                      }
                      if(!key.equals( "reg_copyright") && temp!=-1 && temp !=-972){
                        totalCnt += temp;
                      }
                  }
              
            %>
          <div class="con_rt" id="contentBody">
            <div class="con_rt_head">
              <img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
              &gt;
              분야별 권리자 찾기
              &gt;
              <c:if test="${genreCd=='total'}"><span class="bold">전체</span></c:if>  
              <c:if test="${genreCd!='total'}"><span class="bold">${genreCd}</span></c:if>  
            </div>
            <h1><div class="con_rt_hd_title">저작권자 찾기</div></h1>
          
          
            <div id="sub_contents_con">
            <form id="search" name="search" action="/search/search.do" method="post" onSubmit="goSearch(); return false">
                  <div class="bg_f8f8f8">
                  
                  <!-- 2014.11.06 추가 -->
                  <div>
                    <div class="float_lf">
                      <span class="sub_blue_point_bg">검색대상</span>&nbsp;&nbsp;&nbsp;
                          <input type="checkbox" value="all" name="range" rel="1" id="chkall" onclick="check(1);" ><label for="chkall" class="thin p12">전체</label>
                          <input type="checkbox" value="title" name="range" rel="2" id="chktl" onclick="check(2);" ><label for="chktl"  class="thin p12">저작물명</label>
                          <input type="checkbox" value="licer_detail" name="range" rel="3" id="chkld" onclick="check(3);"><label for="chkld"  class="thin p12">저작권자</label>
                      </div>
                      <div class="float_rt">
                      <span class="sub_blue_point_bg">정렬방식</span>&nbsp;&nbsp;&nbsp;
                        <select name="sort" id="line" title="정렬방식 1차 구분">
                              <option value="RANK">정확도순</option>
                              <option value="DATE">날짜순</option>
                        </select>
                        <select name="sortOrder" id="" title="정렬방식 2차 구분">
                              <option value="DESC">내림차순</option>
                              <option value="ASC">오름차순</option>
                        </select>
                      </div> 
                  <p class="clear"></p>
                  </div>
                  
                    <div class="mar_tp10">
                    <!-- <select name="target" id="srchDivs" title="검색구분">
                      <option>통합검색</option>
                      <option>저작권 등록부</option>
                      <option>위탁관리 저작물</option>
                    </select> -->
                    <select id="collection" name="collection" title="검색구분">
                      <option value="ALL">전체</option>
                      <option value="reg_copyright">저작권등록부</option>
                      <option value="reg_copyright,cf_music,dg_music1,dg_literature,dg_movie,dg_news">음악</option>
                      <option value="reg_copyright,cf_literature,dg_music1,dg_literature,dg_movie,dg_news">어문</option>
                      <option value="reg_copyright,cf_scenario,dg_music1,dg_literature,dg_movie,dg_news">방송대본</option>
                      <option value="reg_copyright,cf_movie,dg_music1,dg_literature,dg_movie,dg_news">영화</option>
                      <option value="reg_copyright,cf_broadcast,dg_music1,dg_literature,dg_movie,dg_news">방송</option>
                      <option value="reg_copyright,cf_news,dg_music1,dg_literature,dg_movie,dg_news">뉴스</option>
                      <option value="reg_copyright,cf_art,dg_music1,dg_literature,dg_movie,dg_news">미술</option>
                      <option value="reg_copyright,cf_image,dg_music1,dg_literature,dg_movie,dg_news">이미지</option>
                      <option value="reg_copyright,cf_photo,dg_music1,dg_literature,dg_movie,dg_news">사진</option>
                      <option value="reg_copyright,cf_etc,dg_music1,dg_literature,dg_movie,dg_news">기타</option>
                  <!--    <option value="reg_copyright,dg_music1,dg_literature,dg_movie,dg_news" style="visibility: hidden;">이건안보여요</option>
                      <option value="reg_copyright" style="visibility: hidden;">이건안보여요2</option> -->
                    </select>
                    <input type="text" title="검색어" id="query" name="query" size="30" value="${query}" style="IME-MODE: active;padding:3px;width:48%;"/>
                    <input type="button" onclick="goSearch();" value="검색" style="background-color: #2c65aa; color: white;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chk1" name="resrch_check" class="checkbox" onclick="javascript:resrch_chk();"/><label for="chk1" class="p11 strong black">결과 내 검색</label>
                    </div>
                  </div>
              <input type="hidden" name="resultcount" value="10"/>
              <input type="hidden" name="page" value=""/>
              <input type="hidden" name="mode" value=""/>
              <input type="hidden" name="arr_range" value="">
              <input type="hidden" name="resrch" value=""/>             
              <input type="hidden" id="urlCd_1" name="urlCd" value="01"/>
              <input type="hidden" id="genreCd_2" name="genreCd" value=""/>
              <input type="hidden" id="subPageStr" name="subPageStr" value=""/>
            </form>
            
            <!-- <div class="sub01_con_bg4_tp mar_tp30"></div>
            <div class="sub01_con_bg4">
              <h3>법정허락 승인 신청</h3>
              <p class="word_dian_bg mar_tp5">이미 상당한 노력이 이행되어 법정허락 승인 신청이 가능한 저작물로, 법정허락 승인 신청정보 작성화면으로 이동합니다.</p>
              <h3 class="mar_tp10">저작권 등록 정보 조회</h3>
              <p class="word_dian_bg mar_tp5">검색한 저작물의 정보 조회를 위해서 저작권 등록 시스템 통합 검색으로 이동합니다.</p>
              <h3 class="mar_tp10">저작권자 찾기위한  상당한노력 신청</h3>
              <p class="word_dian_bg mar_tp5">저작권 찾기 정보시스템에서 한 번의 신청으로 상당한 노력 이행 신청과 함께 저작권자 조회공고가 자동으로 처리됩니다.</p>
            </div>
            <div class="subcetcol">
              <p class="cetcol1">검색 결과에 찾으시는 저작물이 없는 경우 클릭하세요</p>
              <p class="cetcol2"><a href="#" onclick="statBoRegiPop();"><img src="/images/sub/btn_3.jpg" alt="검색" /></a></p>
              
              <p class="clear"></p>
            </div> -->
            
            <%
              if(totalCnt <=0)
              {
            %>
            <!-- <div>
            <p class="cetcol3" style="float: right; margin-right: 7px; margin-bottom: 7px;"><a href="#" onclick="SmartCert_ViewReport1();"><img src="/images/sub/btn_4.jpg" alt="검색" /></a></p>
            </div> -->
            <%} %>
            <ul id="topMenu" class="tab_menuBg mt20">
              <li><strong><a id="topMenu0"  href="javascript:goInnerSearch('999');">전체</a> </strong></li>
              <!-- subPageStr=yes 서브페이지 개발용 변수 -->
              <li><strong><a id="topMenu1" href="javascript:goInnerSearch('1');">음악</a> </strong></li>
              <li><strong><a id="topMenu2" href="javascript:goInnerSearch('2');">어문</a> </strong></li>
              <li><strong><a id="topMenu3" href="javascript:goInnerSearch('3');">방송대본</a> </strong></li>
              <li><strong><a id="topMenu4" href="javascript:goInnerSearch('4');">영화</a> </strong></li>
              <li><strong><a id="topMenu5" href="javascript:goInnerSearch('5');">방송</a> </strong></li>
              <li><strong><a id="topMenu6" href="javascript:goInnerSearch('6');">뉴스</a> </strong></li>
              <li><strong><a id="topMenu7" href="javascript:goInnerSearch('7');">미술</a> </strong></li>
              <li><strong><a id="topMenu8" href="javascript:goInnerSearch('8');">이미지</a> </strong></li>
              <li><strong><a id="topMenu9" href="javascript:goInnerSearch('9');">사진</a> </strong></li>
              <li><strong><a id="topMenu10" href="javascript:goInnerSearch('99');">기타</a> </strong></li>
            </ul>
            
            <%-- <%=subCnt%> --%>
            <%-- <%=jsonCntMap %> --%>
            <%
            
            //int tempObj= (int)jsonCntMap.get("reg_copyright");
            String tempObj= jsonCntMap.get("reg_copyright").toString();
            /* out.print(tempObj==-972); */
            %>
            
            <%-- <%= tempObj%> --%>
            <div>
          <%--  <div class="bg_2e75b6 mar_tp10">  
            
                  <h2 class="float_lf color_fff blod">저작권 등록부(<font id="regCnt"><%if(!tempObj.equals("-972")){out.print(jsonCntMap.get("reg_copyright"));}else{out.print("0");}%></font>건)</h2>
                    <!-- <p class="bzhlag"><a href="#"><img src="/images/sub/mulem.png" alt=" " /></a>
                      <span class="bzhlag22"></span>
                    </p> -->
                    <a href="javascript: goCategory('reg_copyright')" class="float_rt color_fff">더보기</a>
                    
                    
                    
                    
            <p class="clear"></p>
            </div>
              <div class="bg_2e75b6 mar_tp10">  
            
                  <h2 class="float_lf color_fff blod">저작권 등록부(<font id="regCnt"><%if(!tempObj.equals("-972")){out.print(jsonCntMap.get("reg_copyright"));}else{out.print("0");}%></font>건)</h2>
                    <!-- <p class="bzhlag"><a href="#"><img src="/images/sub/mulem.png" alt=" " /></a>
                      <span class="bzhlag22"></span>
                    </p> -->
                    <a href="javascript: goCategory('reg_copyright')" class="float_rt color_fff">더보기</a>
                    
                    
                    
                    
            <p class="clear"></p>
            </div>--%>
              <div class="bg_2e75b6 mar_tp10">  
            
                  <h2 class="float_lf color_fff blod">저작권 등록부(<font id="regCnt"><%if(!tempObj.equals("-972")){out.print(jsonCntMap.get("reg_copyright"));}else{out.print("0");}%></font>건)</h2>
                    <!-- <p class="bzhlag"><a href="#"><img src="/images/sub/mulem.png" alt=" " /></a>
                      <span class="bzhlag22"></span>
                    </p> -->
                    <a href="javascript: goCategory('reg_copyright')" class="float_rt color_fff" title="저작권 등록부 더보기">더보기</a>
                    
                    
                    
                    
            <p class="clear"></p>
            </div>
            </div>
            <table class="sub_tab1" cellspacing="0" cellpadding="0" width="100%" >
              <colgroup>
                <col width="10%">
                <col>
                <col>
              </colgroup>
              <tbody> 
              <%-- <%= collection%> --%>
               <% 
              String totalStr = (String) documentSetMap.get("TotalCount");
              int totalCount = Integer.parseInt(totalStr);
              Map documentSet2 = null;
              Map documentSetMap2 =null;
              
              for(int z = 0 ; z < collection.size() ; z++){
                Map flag= (Map) collection.get(z);
                
                  if(flag.get("Id").equals( "reg_copyright" )){
                    documentSet2 = flag;
                  }
                  
              }
              
              if(documentSet2!=null){
                ArrayList<Map<String, Object>> copyrightList = new ArrayList<Map<String, Object>>();
                
                documentSetMap2 = (Map) documentSet2.get("DocumentSet");
                JSONArray documentJA2 = (JSONArray) documentSetMap2.get("Document");
               
                for( int j = 0 ; j < documentJA2.size() ; j++){
                  /* documentJA 결과값 리스트 */
                  Map<String, Object> exMap = new HashMap<String, Object>();
                  Map obj2 = (Map) documentJA2.get(j);
                  /* Map objField2 = (Map) obj2.get("Field"); */
                  exMap = (Map) obj2.get("Field");
                  copyrightList.add(exMap);
                }
                 // out.print(copyrightList);
                request.setAttribute("copyrightList", copyrightList);
                %>
          
                <% 
              }
                else{
                   
                   
                   
              }
                  if(documentSet2.get("Id").equals( "reg_copyright" )){
                      
              %>
              
                 <c:forEach var="item" items="${copyrightList}" varStatus="status">
                  <tr>
                    <th style="width:15%;">저작권등록부</th>
                    <td>
                  
                      <b><c:out value="${item.CONT_TITLE}" /></b>
                      <p class="mt15">
                     
                         <span class="w60">등록번호 : <c:out value="${item.REG_ID}" /> </span></br>
                         <span class="w60">등록일자 : <c:out value="${item.REG_DT}" /> </span></br>
                         <span>등록부문 :<c:out value="${item.REG_PART1}" /> </span></br>                    
                         <c:if test="${!empty item.AUTHOR_NAME}"><span class="w60">저작자 : <c:out value="${item.AUTHOR_NAME}" /> </span></br></c:if>
                         <c:if test="${!empty item.CONT_CLASS_NAME}"><span class="w60">종류 : <c:out value="${item.CONT_CLASS_NAME}" /> </span></br></c:if>
                         <c:if test="${!empty item.REG_CAUS_TXT}"> <span>등록원인 : ( <c:out value="${item.REG_CAUS_TXT}" /> )</span></br></c:if>
                         <c:if test="${!empty item.AUTHOR_NAME}"><span class="w60">등록권리자 : <c:out value="${item.AUTHOR_NAME}" /> </span></br></c:if>
                         
                      </p>
                    </td>

                  <td><span><a href="javascript:;" onclick="crosPop()" >
                  <img src="/images/sub/btn_2_1.jpg" alt="저작권 등록 정보 조회" /></a></span></td>
                  </tr>
                  
                  </c:forEach>
                 <%--  
                  <tr>
                    <th style="width:15%;">저작권등록부</th>
                    <td>
                    444
                      <b><%=objField2.get("CONT_TITLE")%></b>
                      <p class="mt15">
                         <span class="w60">등록번호 : <%=objField2.get("REG_ID") %> </span></br>
                         <span class="w60">등록일자 : <%=objField2.get("REG_DT") %> </span></br>
                         <span>등록부문 : <%=objField2.get("REG_PART1") %> </span></br>
                          <span>등록부문 : ${objField2.REGPART1 } </span></br>
                         <span class="w60">저작자 : <%=objField2.get("AUTHOR_NAME") %> </span></br>
                         <span class="w60">종류 : <%=objField2.get("CONT_CLASS_NAME") %> </span></br>
                         <span>등록원인 ( <%=objField2.get("REG_CAUS_TXT") %> )</span></br>
                         <span class="w60">등록권리자 : <%=objField2.get("AUTHOR_NAME") %> </span></br>
                      </p>
                    </td>

                  <td><span><a href="javascript:;" onclick="crosPop()" >
                  <img src="/images/sub/btn_2_1.jpg" alt="저작권 등록 정보 조회" /></a></span></td>
                  </tr> --%>
              <%
                    }
            /*       }
                }else{
                  
                  
                  
                } */
              %>
                
              </tbody>
            </table>
            </div><!-- End sub_contents_con -->
            
            
            <div class="bg_2e75b6 mar_tp10">  
                  <h2 class="float_lf color_fff blod">위탁관리 저작물(<font id="subCnt">
                  
                  <%
                  Map tempMap= (Map)jsonCntMap;
                  /* if(documentSet.get("Id").equals( "cf_music" )){
                    out.println(tempMap.get("cf_music"));
                  }else if(documentSet.get("Id").equals( "cf_literature" )){
                    out.println(tempMap.get("cf_literature"));
                  }else if(documentSet.get("Id").equals( "cf_scenario" )){
                    out.println(tempMap.get("cf_scenario"));
                  }else if(documentSet.get("Id").equals( "cf_broadcast" )){
                    out.println(tempMap.get("cf_broadcast"));
                  }else if(documentSet.get("Id").equals( "cf_news" )){
                    out.println(tempMap.get("cf_news"));
                  }else if(documentSet.get("Id").equals( "cf_art" )){
                    out.println(tempMap.get("cf_art"));
                  }else if(documentSet.get("Id").equals( "cf_image" )){
                    out.println(tempMap.get("cf_image"));
                  }else if(documentSet.get("Id").equals( "cf_photo" )){
                    out.println(tempMap.get("cf_photo"));
                  }else if(documentSet.get("Id").equals( "cf_etc" )){
                    out.println(tempMap.get("cf_etc"));
                  }else{ */
                    out.println(subCnt);
                  /* } */
                  
                  
                  %>
                  </font>
                  
                  건)</h2>
                    <!-- <p class="bzhlag"><a href="#"><img src="/images/sub/mulem.png" alt=" " /></a>
                      <span class="bzhlag22"></span>
                    </p> -->
                    <%
                    String str = (String) request.getAttribute("collectionName");
                    int indexReturn = str.indexOf(",");
                    if(indexReturn != -1){
                      str = str.substring(0,str.indexOf(","));
                    }
                    %>
                    <a href="javascript: goCategory('<%=str%>')" class="float_rt color_fff"title="위탁관리 저작물 더보기">더보기</a>
                    
            <p class="clear"></p>
            </div>
            
            <table class="sub_tab1" cellspacing="0" cellpadding="0" width="100%" >
              <colgroup>
                <col width="10%">
                <col>
                <col>
              </colgroup>
              <tbody> 
              
              <%-- <%=documentSet %> --%>
              <% 
              
              
              if(subCnt > 0){
                int objCont = 0 ;
                for( int i = 0 ; i < collection.size() ; i++){
                  /* documentJA 결과값 리스트 */
                  if(objCont>0){break;}
                  Map obj = (Map) collection.get(i);
                  //out.println("o b j : "+obj);
                  //out.print("collection  : " + collection);
                  //out.print("collection.get(0) : " + collection );
                  String id=(String) obj.get("id");
                  //if(obj.get("Id").equals( "cf_etc" )){out.print("id == cf_etc");}
                
                  %>
                  <%-- <%=obj%> --%>
                  <%
                  if(obj.get("Id").equals( "cf_music" )){
                    ArrayList<Map<String, Object>> cf_musicList = new ArrayList<Map<String, Object>>();
                    documentSet = obj;
                    documentSetMap = (Map) documentSet.get("DocumentSet");
                    documentJA = (JSONArray) documentSetMap.get("Document");
                    //out.print("documentSetMap : " + documentSetMap);
                    if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
                    for( int e = 0 ; e < documentJA.size() ; e++){
                      Map<String, Object> exMap = new HashMap<String, Object>();
                      Map objCon=(Map) documentJA.get( e );
                  /*     Map objField = (Map) objCon.get("Field"); */
                      exMap = (Map) objCon.get("Field");
                      cf_musicList.add(exMap);
                    }
                    request.setAttribute("cf_musicList", cf_musicList);
                  %>
                  
                  <c:forEach var="item" items="${cf_musicList}" varStatus="status">
                  <tr>
                    <td scope="row" class="vtop" style="border-left: none;">음악</td>
                    <td class="liceSrch" style="word-break:break-all">
                    
                    <%-- <td><%=documentJA.get( e ) %></td> --%>
                      <b><c:out value="${item.WORKS_TITLE}" /></b>
                      <p class="mt15">
                        <c:if test="${!empty item.ALBUM_TITLE}">
                          <span class="w60">앨범명 : ${item.ALBUM_TITLE}</span>
                        </c:if>
                        <c:if test="${!empty item.DATE}">
                          <span>앨범발매년도 :  ${item.ALBUM_TITLE}</span><br>
                        </c:if>
                        <c:if test="${!empty item.SING}">
                          <span class="w60">가수 : ${item.ALBUM_TITLE}</span>
                        </c:if>
                        <c:if test="${!empty item.PERF}">
                          <span>연주가 : ${item.PERF}</span><br>
                        </c:if>
                        <c:if test="${!empty item.LYRC}">
                          <span class="w60">작사가 : ${item.LYRC}</span>
                        </c:if>
                        <c:if test="${!empty item.COMP}">
                          <span class="w60">작곡가 : ${item.COMP}</span><br>
                        </c:if>
                        <c:if test="${!empty item.ARRA}">
                          <span>편곡가 : ${item.ARRA}</span>
                        </c:if>
                        <c:if test="${!empty item.TRAN}">
                          <span class="w60">역사가 : ${item.TRAN}</span><br>
                        </c:if>
                        <c:if test="${!empty item.PROD}">
                          <span class="w60">음반제작자 : ${item.PROD}</span>
                        </c:if>
                        <c:if test="${!empty item.RGST_ORGN_NAME}">
                          <span>권리관리기관 : ${item.RGST_ORGN_NAME}</span><br>
                        </c:if>
                      </p>
                    </td>
                  </tr>
              </c:forEach>
              <%
                    
              }else if(obj.get("Id").equals( "cf_literature" )){
                ArrayList<Map<String, Object>> cf_literatureList = new ArrayList<Map<String, Object>>();
                documentSet = obj;
                documentSetMap = (Map) documentSet.get("DocumentSet");
                documentJA = (JSONArray) documentSetMap.get("Document");
                if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
                for( int e = 0 ; e < documentJA.size() ; e++){
                  Map<String, Object> exMap = new HashMap<String, Object>();
                  Map objCon=(Map) documentJA.get( e );
                  Map objField = (Map) objCon.get("Field");
                  exMap = (Map) objCon.get("Field");
                  cf_literatureList.add(exMap);
                }
                request.setAttribute("cf_literatureList", cf_literatureList);
              %>
               <c:forEach var="item" items="${cf_literatureList}" varStatus="status">
                <tr>
                    <td scope="row" class="vtop" style="border-left: none;">어문</td>
                    <td class="liceSrch">
                      <b>${item.WORKS_TITLE}</b>
                      <p class="mt15">
                        <c:if test="${!empty item.WORKS_SUB_TITLE and item.WORKS_SUB_TITLE ne '없음'}">
                          <span class="w60">부제 : ${item.WORKS_SUB_TITLE}</span>
                        </c:if>
                        <c:if test="${!empty item.BOOK_ISSU_YEAR}">
                          <span>창작년도 :  ${item.BOOK_ISSU_YEAR}</span><br>
                        </c:if>
                        <c:if test="${!empty item.BOOK_TITLE}">
                          <span class="w60">도서명 : ${item.BOOK_TITLE}</span>
                        </c:if>
                        <c:if test="${!empty item.BOOK_PUBLISHER}">
                          <span>출판사 : ${item.BOOK_PUBLISHER}</span><br>
                        </c:if>
                        <c:if test="${!empty item.BOOK_ISSU_YEAR}">
                          <span class="w60">발행년도 : ${item.BOOK_ISSU_YEAR}</span>
                        </c:if>
                        <c:if test="${!empty item.COPT_NAME}">
                          <span class="w90">저작자 :  ${item.COPT_NAME}</span><br>
                        </c:if>
                        <c:if test="${!empty item.RGST_ORGN_NAME}">
                          <span>권리관리기관 : ${item.COPT_NAME}</span>
                        </c:if>
                      </p>
                    </td>
                  </tr>
                 </c:forEach>
              <%
                
              }else if(obj.get("Id").equals( "cf_scenario" )){ 
                ArrayList<Map<String, Object>> cf_scenarioList = new ArrayList<Map<String, Object>>();
                documentSet = obj;
                documentSetMap = (Map) documentSet.get("DocumentSet");
                documentJA = (JSONArray) documentSetMap.get("Document");
                if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
                for( int e = 0 ; e < documentJA.size() ; e++){
                   Map<String, Object> exMap = new HashMap<String, Object>();
                   Map objCon=(Map) documentJA.get( e );
                   /* Map objField = (Map) objCon.get("Field"); */
                   exMap = (Map) objCon.get("Field");
                   cf_scenarioList.add(exMap);
                }
                request.setAttribute("cf_scenarioList", cf_scenarioList);
              %>
              <c:forEach var="item" items="${cf_scenarioList}" varStatus="status">
                <tr>
                    <td scope="row" class="vtop" style="border-left: none;">시나리오</td>
                    <td class="liceSrch">
                      <b><c:out value="${item.WORKS_TITLE}" /></b>
                      <p class="mt15">
                        <c:if test="${!empty item.WORKS_TITLE}">
                          <span class="w60">작품명(대제목) : ${item.WORKS_TITLE}</span>
                        </c:if>
                        <c:if test="${!empty item.SCRT_GENRE_CD}">
                          <span>장르/소재 : ${item.SCRT_GENRE_CD}/${item.SCRP_SUBJ_CD}</span>
                        </c:if>
                        <c:if test="${!empty item.WORKS_ORIG_TITLE}">
                          <span class="w60">원작명  : ${item.SCRT_GENRE_CD}</span>
                        </c:if>
                        <c:if test="${!empty item.WRITER}">
                          <span>원작작가 : ${item.WRITER}</span>
                        </c:if>
                        <c:if test="${!empty item.BORD_DATE}">
                          <span class="w60">방송일자/회차  : ${item.WRITER} / ${item.BROD_ORD_SEQ}</span>
                        </c:if>
                        <c:if test="${!empty item.PLAYER}">
                          <span class="w60">주요출연진 : ${item.PLAYER}</span>
                        </c:if>
                        <c:if test="${!empty item.MAKE_CPY}">
                          <span class="w90">제작사 :  ${item.MAKE_CPY}</span>
                        </c:if>
                        <c:if test="${!empty item.WRITER}">
                          <span>작가(저자) : ${item.WRITER}</span>
                        </c:if>
                        <c:if test="${!empty item.DIRECTOR}">
                          <span class="w90">연출자 :  ${item.WRITER}</span>
                        </c:if>
                        <c:if test="${!empty item.RGST_ORGN_NAME}">
                          <span class="w90">권리관리기관 :  ${item.WRITER}</span>
                        </c:if>
                      </p>
                    </td>
                  </tr>
                 </c:forEach>
              <%
                
              }else if(obj.get("Id").equals( "cf_movie" )){ 
                ArrayList<Map<String, Object>> cf_movieList = new ArrayList<Map<String, Object>>();
                documentSet = obj;
                documentSetMap = (Map) documentSet.get("DocumentSet");
                documentJA = (JSONArray) documentSetMap.get("Document");
                if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
                for( int e = 0 ; e < documentJA.size() ; e++){
                  Map<String, Object> exMap = new HashMap<String, Object>();
                  Map objCon=(Map) documentJA.get( e );
                  /* Map objField = (Map) objCon.get("Field"); */
                  exMap = (Map) objCon.get("Field");
                  cf_movieList.add(exMap);
                }
                request.setAttribute("cf_movieList", cf_movieList);
              %>
              <c:forEach var="item" items="${cf_movieList}" varStatus="status">
                <tr>
                    <td scope="row" class="vtop" style="border-left: none;">영화</td>
                    <td class="liceSrch">
                      <b><c:out value="${item.WORKS_TITLE}" /></b>
                      <p class="mt15">
                        <c:if test="${!empty item.MAKE_YEAR}">  
                         <span class="w60">제작년도 : ${item.MAKE_YEAR}</span>
                        </c:if>
                        <c:if test="${!empty item.PLAYER}">
                          <span>출연자 : ${item.PLAYER}</span><br>
                        </c:if>
                        <c:if test="${!empty item.DIRECT}">
                          <span class="w60">감독 : ${item.DIRECT}</span>
                        </c:if>
                        <c:if test="${!empty item.WRITER}">
                          <span>작가 : ${item.WRITER}</span><br>
                        </c:if>
                        <c:if test="${!empty item.DIRECTOR}">
                          <span class="w60">연출자 : ${item.DIRECTOR}</span>
                        </c:if>
                        <c:if test="${!empty item.PRODUCER}">
                          <span>제작자 :  ${item.PRODUCER}</span><br>
                        </c:if>
                        <c:if test="${!empty item.DISTRIBUTOR}">
                          <span class="w60">배급사 :  ${item.DISTRIBUTOR}</span><br>
                        </c:if>
                       <%--  <c:if test="${!empty item.투자사}">
                          <span class="w95">투자자 :  ${item.투자사}</span>
                        </c:if> --%>
                        <c:if test="${!empty item.RGST_ORGN_NAME}">
                          <span>권리관리기관 : ${item.RGST_ORGN_NAME}</span><br>
                        </c:if>
                      </p>
                    </td>
                  </tr>
                 </c:forEach>
              <%
                  
                }else if(obj.get("Id").equals( "cf_broadcast" )){ 
                 ArrayList<Map<String, Object>> cf_broadcastList = new ArrayList<Map<String, Object>>();
                String[] genreArr = {"방송장르","드라마","예능","교양","뉴스","라디오","뮤직","영화","기타"};
                documentSet = obj;
                documentSetMap = (Map) documentSet.get("DocumentSet");
                documentJA = (JSONArray) documentSetMap.get("Document");
                if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
                for( int e = 0 ; e < documentJA.size() ; e++){
                  Map<String, Object> exMap = new HashMap<String, Object>();
                  Map objCon=(Map) documentJA.get( e );
                  /* Map objField = (Map) objCon.get("Field"); */
                  exMap = (Map) objCon.get("Field");
                  cf_broadcastList.add(exMap);
                }
                
                request.setAttribute("cf_broadcastList", cf_broadcastList);
              %>
              <c:forEach var="item" items="${cf_broadcastList}" varStatus="status">
                <tr>
                    <td scope="row" class="vtop" style="border-left: none;">방송</td>
                    <td class="liceSrch">
                      <b><c:out value="${item.WORKS_TITLE}" /></b>
                      <p class="mt15">
                        <c:choose>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '99'}">
                           <span class="w60">장르  : 기타</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '0'}">
                           <span class="w60">장르  : 방송장르</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '1'}">
                           <span class="w60">장르  : 드라마</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '2'}">
                           <span class="w60">장르  : 예능</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '3'}">
                           <span class="w60">장르  : 교양</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '4'}">
                           <span class="w60">장르  : 뉴스</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '5'}">
                           <span class="w60">장르  : 라디오</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '6'}">
                           <span class="w60">장르  : 영화</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '7'}">
                           <span class="w60">장르  : 기타</span>
                          </c:when>
                        </c:choose>
                        <c:if test="${!empty item.MAKE_YEAR}">
                          <span>제작년도 : ${item.MAKE_YEAR }</span><br>
                        </c:if>
                        <c:if test="${!empty item.BROD_ORD_SEQ}">
                          <span class="w60">회차 :${item.BROD_ORD_SEQ }</span>
                        </c:if>
                        <c:if test="${!empty item.DIRECTOR}">
                          <span>연출자 : ${item.DIRECTOR }</span><br>
                        </c:if>
                        <c:if test="${!empty item.WRITER}">
                          <span class="w60">작가 : ${item.WRITER }</span>
                        </c:if>
                        <c:if test="${!empty item.RGST_ORGN_NAME}">
                          <span>권리관리기관 : ${item.RGST_ORGN_NAME } </span><br>
                        </c:if>
                        <c:if test="${!empty item.MAKE_CPY}">
                          <span>제작사 :  ${item.MAKE_CPY }</span>
                        </c:if>
                      </p>
                    </td>
                  </tr>
                 </c:forEach>
              <%
                
                }else if(obj.get("Id").equals( "cf_news" )){ 
                  ArrayList<Map<String, Object>> cf_newsList = new ArrayList<Map<String, Object>>();
                  documentSet = obj;
                  documentSetMap = (Map) documentSet.get("DocumentSet");
                  documentJA = (JSONArray) documentSetMap.get("Document");
                  if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
                  for( int e = 0 ; e < documentJA.size() ; e++){
                    Map<String, Object> exMap = new HashMap<String, Object>();
                    Map objCon=(Map) documentJA.get( e );
                    /* Map objField = (Map) objCon.get("Field"); */
                    exMap = (Map) objCon.get("Field");
                    cf_newsList.add(exMap);
                  }
                  request.setAttribute("cf_newsList", cf_newsList);
                %>
                <c:forEach var="item" items="${cf_newsList}" varStatus="status">
                  <tr>
                      <td scope="row" class="vtop" style="border-left: none;">뉴스</td>
                      <td class="liceSrch">
                        <b><c:out value="${item.WORKS_TITLE}" /></b>
                        <p class="mt15">
                          <c:if test="${!empty item.ARTICL_DATE}">
                            <span class="w60">기사일자 : ${item.WORKS_TITLE}</span>
                          </c:if>
                          <c:if test="${!empty item.PAPE_NO}">
                           <span>지면번호/지면면종 : ${item.PAPE_NO}/${item.PAPE_KIND}</span><br>
                          </c:if>
                          <c:if test="${!empty item.CONTRIBUTOR}">
                           <span class="w60">기고자 : ${item.CONTRIBUTOR}</span>
                          </c:if>
                          <c:if test="${!empty item.REPOTER}">
                           <span>기자 : ${item.REPOTER}</span><br>
                          </c:if>
                          <c:if test="${!empty item.PROVIDER}">
                           <span class="w60">언론사 : ${item.PROVIDER}</span>
                          </c:if>
                          <c:if test="${!empty item.RGST_ORGN_NAME}">
                           <span>권리관리기관 :  ${item.RGST_ORGN_NAME}</span><br>
                          </c:if>
                        </p>
                      </td>
                    </tr>
                  </c:forEach>
              <%
                  
              }else if(obj.get("Id").equals( "cf_art" )){ 
                ArrayList<Map<String, Object>> cf_artList = new ArrayList<Map<String, Object>>();
                documentSet = obj;
                documentSetMap = (Map) documentSet.get("DocumentSet");
                documentJA = (JSONArray) documentSetMap.get("Document");
                if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
                for( int e = 0 ; e < documentJA.size() ; e++){
                     Map<String, Object> exMap = new HashMap<String, Object>();                
                     
                  Map objCon=(Map) documentJA.get( e );
                  Map objField = (Map) objCon.get("Field");
                  exMap = (Map) objCon.get("Field");
                  cf_artList.add(exMap);
                }
                  request.setAttribute("cf_artList", cf_artList);
              %>
              
               <c:forEach var="item" items="${cf_artList}" varStatus="status">
               <tr>
                    <td scope="row" class="vtop" style="border-left: none;">미술</td>
                    <td class="liceSrch">
                      <b><c:out value="${item.WORKS_TITLE}" /></b>
                      <p class="mt15">
                      <c:if test="${!empty item.IMAGE_URL and !empty item.IMAGE_THUMBNAIL_NAME }"> <span class="w60">썸네일 :
                        <img class="thumb" src="${item.IMAGE_URL}${item.IMAGE_THUMBNAIL_NAME}" alt="${item.WRITER}, 작가 - ${item.WORKS_TITLE}(${item.WORKS_ID})">
                      </span><br>
                      </c:if>
                      <c:if test="${!empty item.WORKS_ID}">
                      <span class="w60">WORKS_ID : ${item.WORKS_ID} </span></br>
                      </c:if>
                      <c:if test="${!empty item.WORKS_SUB_TITLE and item.WORKS_SUB_TITLE ne '없음'}">
                        <span class="w60">부제 :  ${item.WORKS_SUB_TITLE}</span></br>
                      </c:if>
                      <c:if test="${!empty item.GENRE_CD}">
                        <span>분류 : ${item.GENRE_CD}</span><br>
                      </c:if>
                      <c:if test="${!empty item.MAKE_DATE and item.MAKE_DATE ne '없음'}">
                        <span class="w60">저작년월일 : ${item.MAKE_DATE}</span>
                      </c:if>
                      <c:if test="${!empty item.SOURCE_INFO and item.SOURCE_INFO ne '없음'}">
                        <span>출처 : ${item.SOURCE_INFO}</span><br>
                      </c:if>
                      <c:if test="${!empty item.MAIN_MTRL and item.MAIN_MTRL ne '없음'}">
                        <span class="w60">주재료 : ${item.MAIN_MTRL}</span>
                      </c:if>
                      <c:if test="${!empty item.TXTR and item.TXTR ne '없음'}">
                        <span>구조 및 특징 : ${item.MAIN_MTRL}</span><br>
                      </c:if>
                      <c:if test="${!empty item.WRITER}">
                        <span class="w60">작가 :  ${item.WRITER}</span>
                      </c:if>
                      <c:if test="${!empty item.POSS_ORGN_NAME and item.POSS_ORGN_NAME ne '없음'}">
                        <span >소장기관명 : ${item.POSS_ORGN_NAME}</span><br>
                      </c:if>
                      <c:if test="${!empty item.POSS_DATE and item.POSS_DATE ne '없음'}">
                        <span class="w60">소장년월일 : ${item.POSS_DATE}</span>
                      </c:if>
                      <c:if test="${!empty item.RGST_ORGN_NAME and item.RGST_ORGN_NAME ne '없음'}">
                        <span>권리관리기관 : ${item.RGST_ORGN_NAME}</span><br>
                      </c:if>
                      </p>
                    </td>
                  </tr>
                  
                  </c:forEach>
              
              <%
              
              }else if(obj.get("Id").equals( "cf_image" )){ 
                ArrayList<Map<String, Object>> cf_imageList = new ArrayList<Map<String, Object>>();
                documentSet = obj;
                documentSetMap = (Map) documentSet.get("DocumentSet");
                documentJA = (JSONArray) documentSetMap.get("Document");
                if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
                for( int e = 0 ; e < documentJA.size() ; e++){
                  Map<String, Object> exMap = new HashMap<String, Object>(); 
                  Map objCon=(Map) documentJA.get( e );
                  /* Map objField = (Map) objCon.get("Field"); */
                  exMap = (Map) objCon.get("Field");
                  cf_imageList.add(exMap);
                }
                  request.setAttribute("cf_imageList", cf_imageList);
              %>
              <c:forEach var="item" items="${cf_imageList}" varStatus="status">
                <tr>
                    <td style="border-left: none;"><%-- <%=documentSet.get("Id")%> --%>이미지</td>
                    <td>
                    <b><c:out value="${item.WORKS_TITLE}" /></b>
                      <p class="mt15">
                      <c:if test="${!empty item.IMAGE_URL}">
                       <span class="w60">썸네일 : <img class="thumb" src="${item.IMAGE_URL}${item.IMAGE_THUMBNAIL_NAME}" alt="${item.WRITER}, 작가 - ${item.WORKS_TITLE}(${item.WORKS_ID})"></span><br>
                      </c:if>
                      <c:if test="${!empty item.WORKS_ID}">
                        <span class="w60">WORKS_ID : ${item.WORKS_ID}</span>
                      </c:if>
                      <c:if test="${!empty item.Uid}">
                        <span class="w60">worksid : ${item.Uid}</span><br>
                      </c:if>
                      <c:if test="${!empty item.WORKS_SUB_TITLE}">
                        <span class="w60">부제 : ${item.WORKS_SUB_TITLE}</span>
                      </c:if>
                      <c:if test="${!empty item.Date}">
                        <span>창착년월일 : ${item.Date}</span><br>
                      </c:if>
                      <c:if test="${!empty item.WRITER}">
                        <span class="w60">작가: ${item.Date}</span>
                      </c:if>
                      <c:if test="${!empty item.KEYWORD}">
                        <span>키워드 : ${item.KEYWORD}</span><br>
                      </c:if>
                      <%-- <c:if test="${!empty item.IMAGE_URL}">
                        <span class="w60">제작자 :  ${item.KEYWORD}<%=objField.get("IMAGE_URL")+""+objField.get("IMAGE_THUMBNAIL_NAME")%></span>
                      </c:if> --%>
                      <c:if test="${!empty item.RGST_ORGN_NAME}">
                        <span>권리관리기관명 : ${item.RGST_ORGN_NAME}</span><br>
                       </c:if>
                      </p>
                    </td>
                  </tr>
                </c:forEach>
              <%
                
                }else if(obj.get("Id").equals( "cf_photo" )){ 
                  ArrayList<Map<String, Object>> cf_photoList = new ArrayList<Map<String, Object>>();
                  documentSet = obj;
                  documentSetMap = (Map) documentSet.get("DocumentSet");
                  documentJA = (JSONArray) documentSetMap.get("Document");
                  if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
                  for( int e = 0 ; e < documentJA.size() ; e++){
                       Map<String, Object> exMap = new HashMap<String, Object>(); 
                       Map objCon=(Map) documentJA.get( e );
                  /* Map objField = (Map) objCon.get("Field"); */
                  exMap = (Map) objCon.get("Field");
                  cf_photoList.add(exMap);
                  }
                  request.setAttribute("cf_photoList", cf_photoList);
              %>
              <c:forEach var="item" items="${cf_photoList}" varStatus="status">
                <tr>
                    <td scope="row" class="vtop" style="border-left: none;"><%=documentSet.get("Id")%>이미지</td>
                    <td class="liceSrch">
                    <b><c:out value="${item.WORKS_TITLE}" /></b>
                    <%-- <%if(image_open_yn.compareTo("Y")==0&&image_url.compareTo("")!=0){%><a href="<%=image_url%>" target="_blank"><img src="../images/2012/common/ic_find_id.gif"></a><%}%> --%>
                      <p class="mt15">
                      <c:if test="${!empty item.IMAGE_URL}">
                        <span class="w60">썸네일 : <img class="thumb" src="${item.IMAGE_URL}${item.IMAGE_THUMBNAIL_NAME}" alt="${item.WRITER},작가 - ${item.WORKS_TITLE}(${item.WORKS_ID})"></span><br>
                      </c:if>
                      <c:if test="${!empty item.WORKS_ID}">
                        <span class="w60">WORKS_ID : ${item.WORKS_TITLE}</span>
                      </c:if>
                      <c:if test="${!empty item.Uid}">
                        <span class="w60">worksid : ${item.WORKS_TITLE}</span><br>
                      </c:if>
                      <c:if test="${!empty item.WORKS_SUB_TITLE and item.WORKS_SUB_TITLE ne '없음'}">
                        <span class="w60">부제 : ${item.WORKS_TITLE}</span>
                      </c:if>
                      <c:if test="${!empty item.Date}">
                        <span>창착년월일 : ${item.Date}</span><br>
                      </c:if>
                      <c:if test="${!empty item.WRITER}">
                        <span class="w60">작가: ${item.WRITER}</span>
                      </c:if>
                      <c:if test="${!empty item.KEYWORD}">
                        <span>키워드 : ${item.WRITER}</span><br>
                      </c:if>
                 <%--      <c:if test="${!empty item.KEYWORD}">
                        <span class="w60">제작자 :  <%=objField.get("IMAGE_URL")+""+objField.get("IMAGE_THUMBNAIL_NAME")%></span>
                      </c:if> --%>
                      <c:if test="${!empty item.RGST_ORGN_NAME}">
                        <span>권리관리기관명 : ${item.WRITER}</span><br>
                      </c:if>
                      </p>
                    </td>
                  </tr>
                 </c:forEach>
                 
              <%
                  
                  
                  }else if(obj.get("Id").equals( "cf_etc" )){
                  
                    ArrayList<Map<String, Object>> cf_etcList = new ArrayList<Map<String, Object>>();
                    documentSet = obj;
                    documentSetMap = (Map) documentSet.get("DocumentSet");
                    documentJA = (JSONArray) documentSetMap.get("Document");
                    if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
                    for( int e = 0 ; e < documentJA.size() ; e++){
                         Map<String, Object> exMap = new HashMap<String, Object>();
                      Map objCon=(Map) documentJA.get( e );
                                  exMap = (Map) objCon.get("Field");
                    cf_etcList.add(exMap);
                    }
                    request.setAttribute("cf_etcList", cf_etcList);
                   
                  %>
                  
                  <c:forEach var="item" items="${cf_etcList}" varStatus="status">
                  <tr style="border-left: none;">
                    <td scope="row" class="vtop">기타</td>
                    <td class="liceSrch">
                    <b><c:out value="${item.WORKS_TITLE}"/></b>
                      <p class="mt15">
                      <c:if test="${!empty item.SIDE_GENRE_CD}">
                        <span class="w60">저작물 종류 : ${item.SIDE_GENRE_CD}</span>
                      </c:if>
                      <c:if test="${!empty item.MAKE_DATE}">
                        <span>창작년도 : ${item.MAKE_DATE}</span><br>
                      </c:if>
                      <c:if test="${!empty item.PUBL_MEDI}">
                        <span class="w60">공표매체 : ${item.PUBL_MEDI}</span>
                      </c:if>
                      <c:if test="${!empty item.PUBL_DATE}">
                        <span>공표일자 : ${item.PUBL_DATE}</span><br>
                      </c:if>
                      <c:if test="${!empty item.COPT_NAME}">
                        <span class="w60">저작자 :  ${item.COPT_NAME}</span>
                      </c:if>
                      <c:if test="${!empty item.RGST_ORGN_NAME}">
                        <span>권리관리기관 : ${item.RGST_ORGN_NAME}</span><br>
                      </c:if>
                      </p>
                    </td>
                  </tr>
                  </c:forEach>
              <%
                   //end if
                    
                  }//end for
                
                }// end for
              }else{
                
              }//end if 
              %>
              </tbody>
            </table>
            
            <!-- 디지털 저작권 거래소 연동 데이터  -->
            <div class="bg_2e75b6 mar_tp10">  
                  <h2 class="float_lf color_fff blod">디지털저작권거래소(<font id="dgCnt">
                  
                  <%
                  int collectionSize = collection.size();
                  
                  Map obj = null;
                      Map objMap = null;
                      Map documentSet_dg_album = null;
                      ArrayList document = null;
                      int documentSize = 0;
                      int x = 0;
                  int dgAllCount = 0;
                  for(int y = 0 ; y < collectionSize; y++){
                    Map documentCntMap = (Map)collection.get( y );
                    String collectionId=(String) documentCntMap.get( "Id" );
                    documentSet_dg_album = (Map)documentCntMap.get( "DocumentSet" );
                    if(collectionId.substring( 0,2 ).equals( "dg" )){
                      //out.print("true : "+collectionId+"<br>");
                      int tempInt= Integer.parseInt( (String) documentSet_dg_album.get("TotalCount") );
                      dgAllCount += tempInt;
                    }
                  
                  }
                  out.print(dgAllCount);
                  %>
                  </font>
                  
                  건)</h2>
                    <!-- <p class="bzhlag"><a href="#"><img src="/images/sub/mulem.png" alt=" " /></a>
                      <span class="bzhlag22"></span>
                    </p> -->
                    
                    <a href="javascript: goCategory('dg_music1')" class="float_rt color_fff" title="디지털저작권거래소 더보기">더보기</a>
                    
                    
                    
                    
            <p class="clear"></p>
            </div>
            <table class="sub_tab1" cellspacing="0" cellpadding="0" width="100%">
              <colgroup>
                <col width="10%">
                <col>
                <col>
              </colgroup>
              <tbody> 
              <% 
              
              int objCont2 = 0;
              if(dgAllCount>0){
                  for(int z = 0 ; z < collectionSize ; z++){
                    documentSet2 = (Map)collection.get( z );
                    if(objCont2>0){break;}
                    
                    
                    if(documentSet2.get( "Id" ).equals( "dg_music1")||documentSet2.get( "Id" ).equals( "dg_literature")||documentSet2.get( "Id" ).equals( "dg_movie")||documentSet2.get( "Id" ).equals( "dg_news")
                      || documentSet2.get( "Id" ).equals( "dg_regcopy")){
                    
                    if(documentSet2.get( "Id" ).equals( "dg_album" )){
                      ArrayList<Map<String, Object>> dg_albumList = new ArrayList<Map<String, Object>>();  
                      documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                      document= (ArrayList)documentSet_dg_album.get("Document");
                      documentSize= document.size();
                      if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                      for(x = 0; x < documentSize ; x++){
                        Map<String, Object> exMap = new HashMap<String, Object>();
                        obj = (Map)document.get( x );
                       /*  objMap = (Map)obj.get( "Field" ); */
                        exMap = (Map) obj.get("Field");
                        dg_albumList.add(exMap);
                        
                      }
                      request.setAttribute("dg_albumList", dg_albumList);
                      %>
                      <c:forEach var="item" items="${dg_albumList}" varStatus="status">
                      <tr>
                        <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">앨범</td>
                        <td class="liceSrch">
                          <b><c:out value="${item.ALBUM_TITLE}" /></b>
                          <p class="mt15">
                          <c:if test="${!empty item.ALBUM_TITLE}">
                            <span class="w60">앨범 명 : ${item.ALBUM_TITLE}</span>
                          </c:if>
                          <c:if test="${!empty item.DOCID}">
                            <span class="w60">앨범 아이디 : ${item.ALBUM_TITLE}</span>
                          </c:if>
                          <c:if test="${!empty item.DATE}">
                            <span>앨범 발매일 : ${item.DATE}</span><br>
                          </c:if>
                          <c:if test="${!empty item.ALBUM_LABLE_NAME}">
                            <span>앨범 라벨명 : ${item.ALBUM_LABLE_NAME}</span><br>
                          </c:if>
                          <c:if test="${!empty item.DISTRIBUTION_COMPANY}">
                            <span class="w60">회사 :  ${item.DISTRIBUTION_COMPANY}</span>
                          </c:if>
                          <c:if test="${!empty item.PRODUCER}">
                            <span class="w60">앨범 제작사 : ${item.PRODUCER}</span><br>
                          </c:if>
                          <c:if test="${!empty item.EDITION}">
                            <span>에디션 : ${item.EDITION}</span>
                          </c:if>
                          <c:if test="${!empty item.ALBUM_TYPE}">
                            <span class="w60">앨범 형태 : ${item.ALBUM_TYPE}</span><br>
                          </c:if>
                          <c:if test="${!empty item.ALBUM_MEDIA_TYPE}">
                            <span class="w60">앨범 미디어타입 : ${item.ALBUM_MEDIA_TYPE}</span>
                          </c:if>
                          </p>
                        </td>
                      </tr>
                  </c:forEach>
                      <%
                      
                      //break;
                }else if(documentSet2.get( "Id" ).equals( "dg_art" )){
                     ArrayList<Map<String, Object>> dg_artList = new ArrayList<Map<String, Object>>();
                     documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                      document= (ArrayList)documentSet_dg_album.get("Document");
                      documentSize= document.size();
                      for(x = 0; x < documentSize ; x++){
                         Map<String, Object> exMap = new HashMap<String, Object>();  
                        obj = (Map)document.get( x );
                        /* objMap = (Map)obj.get( "Field" ); */
                        exMap = (Map) obj.get("Field");
                        dg_artList.add(exMap);
                      }
                      request.setAttribute("dg_artList", dg_artList);
                        %>
                        <c:forEach var="item" items="${copyrightList}" varStatus="status">
                        <tr>
                        <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">미술저작물</td>
                          <td class="liceSrch">
                            <b><c:out value="${item.TITLE}" /></b>
                            <p class="mt15">
                              <span class="w60">저작물명 : <c:out value="${item.TITLE}" /></span>
                              <c:if test="${!empty item.DOCID}">
                               <span class="w60">미술저작물 아이디 : ${item.DOCID}</span>
                              </c:if>
                              <c:if test="${!empty item.SUBTITLE}">
                               <span>부제 : ${item.SUBTITLE}</span><br>
                              </c:if>
                              <c:if test="${!empty item.ART_DIV_INFO}">
                               <span class="w60">분류정보 : ${item.ART_DIV_INFO}</span>
                              </c:if>
                              <c:if test="${!empty item.CLTN_ORG_NM}">
                               <span class="w60">소장기관명 : ${item.CLTN_ORG_NM}</span><br>
                              </c:if>
                              <c:if test="${!empty item.SIZE_INFO}">
                               <span>크기정보 : ${item.SIZE_INFO}</span>
                              </c:if>
                              <c:if test="${!empty item.MAIN_MTRL}">
                               <span class="w60">주재료 : ${item.MAIN_MTRL}</span><br>
                              </c:if>
                              <c:if test="${!empty item.STRU_FTRE}">
                               <span class="w60">구조및특징 : ${item.MAIN_MTRL}</span>
                              </c:if>
                            </p>
                          </td>
                        </tr>
                    </c:forEach>
                        <%
                      
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_art_licensor" )){
                      ArrayList<Map<String, Object>> dg_art_licensorList = new ArrayList<Map<String, Object>>();
                      documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                      document= (ArrayList)documentSet_dg_album.get("Document");
                      documentSize= document.size();
                      for(x = 0; x < documentSize ; x++){
                        Map<String, Object> exMap = new HashMap<String, Object>();
                        obj = (Map)document.get( x );
                       /*  objMap = (Map)obj.get( "Field" ); */
                        exMap = (Map) obj.get("Field");
                        dg_art_licensorList.add(exMap);
                      }
                      request.setAttribute("dg_art_licensorList", dg_art_licensorList);
                        %>
                        <c:forEach var="item" items="${dg_art_licensorList}" varStatus="status">
                        <tr>
                          <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">미술저작권자</td>
                          <td class="liceSrch">
                            <b>${item.TITLE}</b>
                            <p class="mt15">
                            <c:if test="${!empty item.LICENSORROLE}">
                              <span class="w60">저작권역할 : ${item.LICENSORROLE}</span>
                            </c:if>
                            <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                              <span class="w60">저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
                            </c:if>
                            </p>
                          </td>
                         </tr>
                        </c:forEach>
                        <%
                      
                      
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_book" )){
                         ArrayList<Map<String, Object>> dg_bookList = new ArrayList<Map<String, Object>>();
                  documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                      document= (ArrayList)documentSet_dg_album.get("Document");
                      documentSize= document.size();
                      if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                      for(x = 0; x < documentSize ; x++){
                           Map<String, Object> exMap = new HashMap<String, Object>();
                        obj = (Map)document.get( x );
                        /* objMap = (Map)obj.get( "Field" ); */
                        exMap = (Map) obj.get("Field");
                        dg_bookList.add(exMap);
                      }
                       request.setAttribute("dg_bookList", dg_bookList);
                        %>
                        <c:forEach var="item" items="${dg_bookList}" varStatus="status">
                        <tr>
                            <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">도서</td>
                            <td class="liceSrch">
                              <b>${item.TITLE}</b>
                              <p class="mt15">
                                <span class="w60">도서명 : ${item.TITLE}</span>
                                <c:if test="${!empty item.DOCID}">
                                  <span>아이디 : ${item.DOCID}</span><br>
                                </c:if>
                                <c:if test="${!empty item.FIRST_EDITION_YEAR}">
                                  <span>발행년도 : ${item.FIRST_EDITION_YEAR}</span>
                                </c:if>
                                <c:if test="${!empty item.WRITER}">
                                  <span class="w60">작가 : ${item.WRITER}</span><br>
                                </c:if>
                                <c:if test="${!empty item.TRANSLATOR}">
                                  <span>번역가 : ${item.TRANSLATOR}</span>
                                </c:if>
                                <c:if test="${!empty item.PUBLISHER}">
                                  <span class="w60">발행자명 : <%=objMap.get("PUBLISHER")%></span>
                                </c:if>
                              </p>
                            </td>
                        </tr>
                        </c:forEach>
                        <%
                    
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_broadcast" )){
                         ArrayList<Map<String, Object>> dg_broadcastList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                      if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                      for(x = 0; x < documentSize ; x++){
                        Map<String, Object> exMap = new HashMap<String, Object>(); 
                        obj = (Map)document.get( x );
                        exMap = (Map)obj.get("Field");
                        dg_broadcastList.add(exMap);
                      }
                      request.setAttribute("dg_broadcastList", dg_broadcastList);
                        %>
                       <c:forEach var="item" items="${dg_broadcastList}" varStatus="status">
                        <tr>
                            <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">방송저작권자</td>
                            <td class="liceSrch">
                              <b><c:out value="${item.PROGRAM_NAME}" /></b>
                              <p class="mt15">
                                <c:if test="${!empty item.TITLE}">
                                  <span>저작물명 : ${item.TITLE}</span>
                                </c:if>
                                <c:if test="${!empty item.PROGRAM_NAME}">
                                  <span class="w60">프로그램명 : ${item.PROGRAM_NAME}</span>
                                </c:if>
                                <c:if test="${!empty item.DATE}">
                                  <span>방송일자 : ${item.DATE}</span>
                                </c:if>
                                <c:if test="${!empty item.GNRE_CODE_NAME}">
                                  <span class="w60">방송장르 : ${item.GNRE_CODE_NAME}</span><br>
                                </c:if>
                                <c:if test="${!empty item.MAKE_TYPE}">
                                 <span>제작타입 : ${item.MAKE_TYPE}</span>
                                </c:if>
                                <c:if test="${!empty item.MAKE_NAME}">
                                  <span class="w60">제작사 : ${item.MAKE_NAME}</span><br>7
                                </c:if>
                                <c:if test="${!empty item.MAKE_PD}">
                                  <span>담당PD : ${item.MAKE_PD}</span>
                                </c:if>
                                <c:if test="${!empty item.PRODUCER}">
                                  <span class="w60">연출자 : ${item.PRODUCER}</span><br>
                                </c:if>
                                <c:if test="${!empty item.INOUT_TYPE}">
                                  <span>방송회차 : ${item.INOUT_TYPE}</span>
                                </c:if>
                                <c:if test="${!empty item.WRITER}">
                                  <span class="w60">대표작가 : ${item.WRITER}</span><br>
                                </c:if>
                                <c:if test="${!empty item.STARTDAY}">
                                  <span>방송일자 : ${item.STARTDAY}</span>
                                </c:if>
                              </p>
                            </td>
                        </tr>
                       </c:forEach>
                        <%
                      
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_broadcast_licensor" )){
                         ArrayList<Map<String, Object>> dg_broadcast_licensorList = new ArrayList<Map<String, Object>>();
                  documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                      document= (ArrayList)documentSet_dg_album.get("Document");
                      documentSize= document.size();
                      if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                      for(x = 0; x < documentSize ; x++){
                           Map<String, Object> exMap = new HashMap<String, Object>();    
                        obj = (Map)document.get( x );
                        /* objMap = (Map)obj.get( "Field" ); */
                        exMap = (Map)obj.get("Field");
                        dg_broadcast_licensorList.add(exMap);
                      }
                      request.setAttribute("dg_broadcast_licensorList", dg_broadcast_licensorList);
                        %>
                        <c:forEach var="item" items="${dg_broadcast_licensorList}" varStatus="status">
                        <tr>
                            <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">방송저작권자</td>
                            <td class="liceSrch">
                              <b><c:out value="${item.LICENSOR_NAME_KOR}" /></b>
                              <p class="mt15">
                              <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                                <span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
                              </c:if>
                              <c:if test="${!empty item.LICENSORROLE}">
                                <span class="w60">저작권역할 : ${item.LICENSORROLE}</span>
                              </c:if>
                              </p>
                            </td>
                        </tr>
                        </c:forEach>
                        <%
                      
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_character" )){
                      ArrayList<Map<String, Object>> dg_characterList = new ArrayList<Map<String, Object>>();
                      documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                      document= (ArrayList)documentSet_dg_album.get("Document");
                      documentSize= document.size();
                      if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                      for(x = 0; x < documentSize ; x++){
                           Map<String, Object> exMap = new HashMap<String, Object>();   
                           obj = (Map)document.get( x );
                           /* objMap = (Map)obj.get( "Field" ); */
                           exMap = (Map)obj.get("Field");
                           dg_characterList.add(exMap);
                         }
                         request.setAttribute("dg_characterList", dg_characterList);
                        %>
                       <c:forEach var="item" items="${dg_characterList}" varStatus="status">
                        <tr>
                          <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">캐릭터저작물</td>
                          <td class="liceSrch">
                            <b><c:out value="${item.CHAR_NM_KOR}" /></b>
                            <p class="mt15">
                              <c:if test="${!empty item.CHAR_NM_KOR}">
                               <span>저작물명 : ${item.CHAR_NM_KOR}</span>
                              </c:if>
                              <c:if test="${!empty item.CHAR_DESC}">
                               <span class="w60">설명 : ${item.CHAR_NM_KOR}</span>
                              </c:if>
                              <c:if test="${!empty item.WORK_NM_KOR}">
                               <span>작품명 : ${item.WORK_NM_KOR}</span>
                              </c:if>
                              <c:if test="${!empty item.WORKGENRENM}">
                               <span class="w60">장르 : ${item.WORKGENRENM}</span><br>
                              </c:if>
                            </p>
                          </td>
                        </tr>
                       </c:forEach>
                        <%
                      
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_character" )){
                         ArrayList<Map<String, Object>> dg_characterList2 = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_characterList2.add(exMap);
                         }
                            request.setAttribute("dg_characterList2", dg_characterList2);
                        %>
                        <c:forEach var="item" items="${dg_characterList2}" varStatus="status">
                        <tr>
                            <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">캐릭터저작권자</td>
                            <td class="liceSrch">
                              <b><c:out value="${item.LICENSOR_NAME_KOR}" /></b>
                              <p class="mt15">
                                <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                                  <span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
                                </c:if>
                                <c:if test="${!empty item.LICENSORROLE}">
                                  <span class="w60">저작권역할 : ${item.LICENSORROLE} </span>
                                </c:if>
                                <c:if test="${!empty item.NATINAME}">
                                  <span>국적 : ${item.NATINAME} </span>
                                </c:if>
                              </p>
                            </td>
                          </tr>
                         </c:forEach>
                        <%
                      
                    
                    }else if(documentSet2.get( "Id" ).equals( "dg_content" )){
                         
                         
                         ArrayList<Map<String, Object>> dg_contentList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_contentList.add(exMap);
                         }
                            request.setAttribute("dg_contentList", dg_contentList);
                            
                            
                            
                      
                        %>
                       <c:forEach var="item" items="${dg_contentList}" varStatus="status">
                          <tr>
                              <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">방송컨텐츠 저작물</td>
                              <td class="liceSrch">
                                <b><c:out value="${item.TITLE}" /></b>
                                <p class="mt15">
                                  <c:if test="${!empty item.TITLE}">
                                   <span>저작물명 : ${item.TITLE} <%=objMap.get("TITLE")%></span>
                                  </c:if>
                                  <c:if test="${!empty item.SUBPR_NAME}">
                                   <span class="w60">부제 : ${item.SUBPR_NAME} </span><br>
                                  </c:if>
                                  <c:if test="${!empty item.BROAD_DATE}">
                                   <span>방송일자 : ${item.BROAD_DATE} </span>
                                  </c:if>
                                  <c:if test="${!empty item.MEDI_CODE_NM}">
                                   <span class="w60">프로그램명 : ${item.MEDI_CODE_NM} </span><br>
                                  </c:if>
                                  <c:if test="${!empty item.CHNL_CODE_NM}">
                                   <span>매체코드 : ${item.CHNL_CODE_NM} </span>
                                  </c:if>
                                  <c:if test="${!empty item.MAKE_NAME}">
                                   <span class="w60">채널코드 : ${item.MAKE_NAME} </span><br>
                                  </c:if>
                                  <c:if test="${!empty item.PROD_QULTY}">
                                   <span>제작품질 : ${item.PROD_QULTY} </span>
                                  </c:if>
                                  <c:if test="${!empty item.PTABL_TITLE}">
                                   <span class="w60">편성표제목 : ${item.PTABL_TITLE} </span><br>
                                  </c:if>
                                  <c:if test="${!empty item.PROG_GRAD}">
                                   <span>프로그램등급 : ${item.PROG_GRAD} </span>
                                  </c:if>
                                </p>
                              </td>
                          </tr>
                       </c:forEach>
                        <%
                      
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_content_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_content_licensorList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_content_licensorList.add(exMap);
                         }
                            request.setAttribute("dg_content_licensorList", dg_content_licensorList);
                            
                            
                        %>
                        <c:forEach var="item" items="${dg_content_licensorList}" varStatus="status">
                        <tr>
                          <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 방송컨텐츠 저작권자</td>
                          <td class="liceSrch">
                            <b><c:out value="${item.LICENSOR_NAME_KOR}" /> </b>
                            <p class="mt15">
                              <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                               <span>저작권자 명 : ${item.LICENSOR_NAME_KOR}</span>
                              </c:if>
                              <c:if test="${!empty item.LICENSORROLE}">
                               <span class="w60">저작권 역할 : ${item.LICENSORROLE}</span>
                              </c:if>
                              <c:if test="${!empty item.LICENSOR_SEQ}">
                               <span>저작권자 명 : ${item.LICENSOR_SEQ}</span>
                              </c:if>
                            </p>
                          </td>
                         </tr>
                         </c:forEach>
                        <%
                      
                    
                    }else if(documentSet2.get( "Id" ).equals( "dg_literature" )){
                       ArrayList<Map<String, Object>> dg_literatureList = new ArrayList<Map<String, Object>>();
                      documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                      document= (ArrayList)documentSet_dg_album.get("Document");
                      documentSize= document.size();
                      if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                      for(x = 0; x < documentSize ; x++){
                        Map<String, Object> exMap = new HashMap<String, Object>();   
                        obj = (Map)document.get( x );
                        /* objMap = (Map)obj.get( "Field" ); */
                        exMap = (Map) obj.get("Field");
                        dg_literatureList.add(exMap);
                      }
                      request.setAttribute("dg_literatureList", dg_literatureList);
                        %>
                 <c:forEach var="item" items="${dg_literatureList}" varStatus="status">
                   <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 어문저작물</td>
                    <td class="liceSrch">
                      <b><c:out value="${item.TITLE}" /></b>
                      <p class="mt15">
                      <c:if test="${!empty item.TITLE}">
                        <span>저작물명 : ${item.TITLE}</span>
                      </c:if>
                      <c:if test="${!empty item.WRITER}">
                        <span class="w60">작가 : ${item.WRITER}</span><br>
                      </c:if>
                      <c:if test="${!empty item.BOOKTITLE}">
                        <span>수록도서명 : ${item.BOOKTITLE}</span>
                      </c:if>
                      <c:if test="${!empty item.FIRST_EDITION_YEAR}">
                        <span class="w60">발행년도 : ${item.BOOKTITLE}</span><br>
                      </c:if>
                      <c:if test="${!empty item.PUBLISHER}">
                        <span>발행자 : ${item.PUBLISHER}</span>
                      </c:if>
                      <c:if test="${!empty item.INOUT_TYPE}">
                        <span class="w60">발행국가 : ${item.PUBLISHER}</span><br>
                      </c:if>
                      <c:if test="${!empty item.PUB_LANG}">
                        <span>언어코드 : ${item.PUB_LANG}</span>
                      </c:if>
                      <c:if test="${!empty item.MATERIAL_TYPE}">
                        <span class="w60">매체형태 : ${item.MATERIAL_TYPE}</span><br>
                      </c:if>
                      <c:if test="${!empty item.PUBLISH_TYPE}">
                        <span>자료유형 : ${item.PUBLISH_TYPE}</span>
                      </c:if>
                      <c:if test="${!empty item.RETRIEVE_TYPE}">
                        <span class="w60">검색분류 : ${item.RETRIEVE_TYPE}</span><br>
                      </c:if>
                      </p>
                    </td>
                    </tr>
                   </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_literature_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_literature_licensorList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_literature_licensorList.add(exMap);
                         }
                            request.setAttribute("dg_literature_licensorList", dg_literature_licensorList);
                            
                            
                        %>
                        <c:forEach var="item" items="${dg_literature_licensorList}" varStatus="status">
                        <tr>
                          <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 어문저작권자</td>
                          <td class="liceSrch">
                            <b>${itme.LICENSOR_NAME_KOR}</b>
                            <p class="mt15">
                            <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                              <span>저작권자명 : ${itme.LICENSOR_NAME_KOR}</span>
                            </c:if>
                            <c:if test="${!empty item.LICENSORROLE}">
                              <span class="w60">저작권자역할 : ${itme.LICENSORROLE} </span><br>
                             </c:if>
                            </p>
                          </td>
                        </tr>
                        </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_movie" )){
                         
                         ArrayList<Map<String, Object>> dg_movieList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_movieList.add(exMap);
                         }
                            request.setAttribute("dg_movieList", dg_movieList);
                            
                        %>
                      <c:forEach var="item" items="${dg_movieList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 영화저작물</td>
                    <td class="liceSrch">
                      <b>${item.TITLE}</b>
                      <p class="mt15">
                       <c:if test="${!empty item.TITLE}">
                        <span>저작물명 : ${item.TITLE}</span>
                       </c:if>
                       <c:if test="${!empty item.GENRE_NAME}"> 
                        <span class="w60">장르구분 : ${item.GENRE_NAME}</span><br>
                       </c:if>
                       <c:if test="${!empty item.MOVIE_TYPE_NAME}">
                        <span>영화형태 : ${item.MOVIE_TYPE_NAME}</span>
                       </c:if>
                       <c:if test="${!empty item.VIEW_GRADE_NAME}">
                        <span class="w60">관람등급 : ${item.VIEW_GRADE_NAME}</span><br>
                       </c:if>
                       <c:if test="${!empty item.PRODUCE_YEAR}">
                        <span>제작년도 : ${item.PRODUCE_YEAR}</span>
                       </c:if>
                       <c:if test="${!empty item.RELEASE_DATE}">
                        <span class="w60">개봉일자 : ${item.RELEASE_DATE}</span><br>
                       </c:if>
                       <c:if test="${!empty item.LEADING_ACTOR}">
                        <span>출연진 : ${item.LEADING_ACTOR}</span>
                       </c:if>
                       <c:if test="${!empty item.DIRECTOR}">
                        <span class="w60">감독/연출자 : ${item.DIRECTOR}</span><br>
                       </c:if>
                       <c:if test="${!empty item.PRODUCER}">
                        <span>제작사 : ${item.PRODUCER} </span>
                       </c:if>
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_movie_licensor" )){
                     
                         ArrayList<Map<String, Object>> dg_movie_licensorList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_movie_licensorList.add(exMap);
                         }
                            request.setAttribute("dg_movie_licensorList", dg_movie_licensorList);
                            
                        %>
                        <c:forEach var="item" items="${dg_movie_licensorList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 영화저작권자</td>
                    <td class="liceSrch">
                      <b>${item.LICENSOR_NAME_KOR} </b>
                      <p class="mt15">
                      <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                        <span>저작권자명 : ${item.LICENSOR_NAME_KOR} </span>
                      </c:if>
                      <c:if test="${!empty item.LICENSORROLE}">
                        <span class="w60">저작권자역할 : ${item.LICENSORROLE}  </span><br>
                       </c:if>
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                     
                    }else if(documentSet2.get( "Id" ).equals( "dg_music1" )){
                         ArrayList<Map<String, Object>> dg_music1List = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_music1List.add(exMap);
                         }
                            request.setAttribute("dg_music1List", dg_music1List);
                        %>
                       <c:forEach var="item" items="${dg_music1List}" varStatus="status">
                        <tr>
                            <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 음악 저작물</td>
                            <td class="liceSrch">
                              <b>${item.TITLE}</b>
                              <p class="mt15">
                                <c:if test="${!empty item.TITLE}">
                                <span>저작물명 : ${item.TITLE} </span>
                                </c:if>
                                <c:if test="${!empty item.ALBUM_TITLE}">
                                <span class="w60">수록앨범명 : ${item.ALBUM_TITLE} </span><br>
                                </c:if>
                                <c:if test="${!empty item.INOUT_TYPE}">
                                <span>국내외구분 : ${item.INOUT_TYPE}</span>
                                </c:if>
                                <c:if test="${!empty item.SINGER}">
                                <span class="w60">가수 : ${item.SINGER} </span><br>
                                </c:if>
                                <c:if test="${!empty item.LYRICIST}">
                                <span>작사가 : ${item.LYRICIST} </span>
                                </c:if>
                                <c:if test="${!empty item.COMPOSER}">
                                <span class="w60">작곡가 : ${item.COMPOSER} </span><br>
                                </c:if>
                              </p>
                            </td>
                            </tr>
                    </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_music_licensor" )){
                      
                         ArrayList<Map<String, Object>> dg_music_licensorList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_music_licensorList.add(exMap);
                         }
                            request.setAttribute("dg_music_licensorList", dg_music_licensorList);
                        %>
                        <c:forEach var="item" items="${dg_music_licensorList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 음악 저작물</td>
                    <td class="liceSrch">
                      <b>${item.REG_PART1}</b>
                      <p class="mt15">
                      <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                        <span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
                      </c:if>
                      <c:if test="${!empty item.LICENSORROLE}">
                        <span class="w60">저작권자역할 : ${item.LICENSORROLE}</span><br>
                      </c:if>
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_news" )){
                         
                         ArrayList<Map<String, Object>> dg_newsList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_newsList.add(exMap);
                         }
                            request.setAttribute("dg_newsList", dg_newsList);
                        %>
                        <c:forEach var="item" items="${dg_newsList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 뉴스 저작물</td>
                    <td class="liceSrch">
                      <b>${item.TITLE}</b>
                      <p class="mt15">
                      <c:if test="${!empty item.TITLE}">
                        <span>저작물명 : ${item.TITLE}</span>
                      </c:if>
                      <c:if test="${!empty item.REPORTERNM}">
                        <span class="w60">언론사명 : ${item.REPORTERNM}</span><br>
                      </c:if>
                      <c:if test="${!empty item.ARTICLDT}">
                        <span>기사일자 : ${item.ARTICLDT}</span>
                      </c:if>
                      <c:if test="${!empty item.CANAME}">
                        <span class="w60">관리단체 : ${item.CANAME}</span><br>
                      </c:if>
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_news_licensor" )){
                       
                         ArrayList<Map<String, Object>> dg_news_licensorList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_news_licensorList.add(exMap);
                         }
                            request.setAttribute("dg_news_licensorList", dg_news_licensorList);
                        %>
                       <c:forEach var="item" items="${dg_news_licensorList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 뉴스 저작권자</td>
                    <td class="liceSrch">
                      <b>${item.LICENSOR_NAME_KOR}</b>
                      <p class="mt15">
                      <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                        <span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
                       </c:if>
                       <c:if test="${!empty item.LICENSORROLE}">
                        <span class="w60">저작권자역할 : ${item.LICENSORROLE}</span><br>
                        </c:if>
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_public" )){

                         ArrayList<Map<String, Object>> dg_publicList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_publicList.add(exMap);
                         }
                            request.setAttribute("dg_publicList", dg_publicList);
                        %>
                        <c:forEach var="item" items="${dg_publicList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 공공 저작물</td>
                    <td class="liceSrch">
                      <b>${item.TITLE}</b>
                      <p class="mt15">
                      <c:if test="${!empty item.TITLE}">
                        <span>저작물명 : ${item.TITLE}</span>
                      </c:if>
                      <c:if test="${!empty item.GENRE_NAME}">
                        <span class="w60">UCI : ${item.GENRE_NAME}</span><br>
                      </c:if>
                      <c:if test="${!empty item.TYPE_DIV_L_CD}">
                        <span>형식대분류코드 : ${item.TYPE_DIV_L_CD}</span>
                      </c:if>
                      <c:if test="${!empty item.TYPEDIVLNM}">
                        <span class="w60">형식대분류 : ${item.TYPEDIVLNM}</span><br>
                      </c:if>
                      <c:if test="${!empty item.TYPE_DIV_M_CD}">
                        <span>형식중분류코드 : ${item.TYPE_DIV_M_CD}</span>
                      </c:if>
                      <c:if test="${!empty item.TYPEDIVMNM}">
                        <span class="w60">형식중분류 : ${item.TYPEDIVMNM}</span><br>
                      </c:if>
                      <c:if test="${!empty item.MEAN_DIV_L_CD}">
                        <span>의미대분류코드 : ${item.MEAN_DIV_L_CD}</span>
                      </c:if>
                      <c:if test="${!empty item.MEANDIVLNM}">
                        <span class="w60">의미대분류 : ${item.MEANDIVLNM}</span><br>
                      </c:if>
                      <c:if test="${!empty item.MEAN_DIV_M_CD}">
                        <span>의미중분류코드 : ${item.MEAN_DIV_M_CD}</span>
                      </c:if>
                      <c:if test="${!empty item.MEANDIVMNM}">
                        <span class="w60">의미중분류 : ${item.MEANDIVMNM}</span><br>
                       </c:if>
                       <c:if test="${!empty item.ANNC_NATN}">
                        <span>공표국가 : ${item.ANNC_NATN}</span>
                       </c:if>
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_public_licensor" )){

                         ArrayList<Map<String, Object>> dg_public_licensorList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_public_licensorList.add(exMap);
                         }
                            request.setAttribute("dg_public_licensorList", dg_public_licensorList);
                        %>
                       <c:forEach var="item" items="${copyrightList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 뉴스 저작권자</td>
                    <td class="liceSrch">
                      <b><c:out value="${item.LICENSOR_NAME_KOR}" /></b>
                      <p class="mt15">
                      <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                        <span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
                      </c:if>
                      <c:if test="${!empty item.LICENSORROLE}">
                        <span class="w60">저작권자역할 : ${item.LICENSORROLE}</span><br>
                      </c:if>
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_scenario" )){

                         ArrayList<Map<String, Object>> dg_scenarioList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_scenarioList.add(exMap);
                         }
                            request.setAttribute("dg_scenarioList", dg_scenarioList);
                        %>
                         <c:forEach var="item" items="${copyrightList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 방송대본저작물</td>
                    <td class="liceSrch">
                      <b>${item.TITLE}</b>
                      <p class="mt15">
                        <c:if test="${!empty item.TITLE}">
                        <span>저작물명 : ${item.TITLE}</span>
                        </c:if>
                        <c:if test="${!empty item.WRITER}">
                        <span class="w60">작가명 : ${item.WRITER}</span><br>
                        </c:if>
                        <c:if test="${!empty item.GENRE_KIND}">
                        <span>장르구분 : ${item.GENRE_KIND}</span>
                        </c:if>
                        <c:if test="${!empty item.SUBJ_KIND}">
                        <span class="w60">소재분류 : ${item.SUBJ_KIND}</span><br>
                        </c:if>
                        <c:if test="${!empty item.DIRECT}">
                        <span>연출자 : ${item.DIRECT}</span>
                        </c:if>
                        <c:if test="${!empty item.ORIG_WORK}">
                        <span class="w60">원작명 : ${item.ORIG_WORK}</span><br>
                        </c:if>
                        <c:if test="${!empty item.ORIG_WRITER}">
                        <span>원작자명 : ${item.ORIG_WRITER}</span>
                        </c:if>
                        <c:if test="${!empty item.PLAYERS}">
                        <span class="w60">주요출연진 : ${item.PLAYERS}</span><br>
                        </c:if>
                        <c:if test="${!empty item.BROAD_ORD}">
                        <span>방송회차 : ${item.BROAD_ORD}</span>
                        </c:if>
                        <c:if test="${!empty item.BROAD_DATE}">
                        <span class="w60">방송일자 : ${item.BROAD_DATE}</span><br>
                        </c:if>
                        <c:if test="${!empty item.BROAD_MEDINM}">
                        <span>방송매체 : ${item.BROAD_MEDINM}</span>
                        </c:if>
                        <c:if test="${!empty item.BROAD_STATNM}">
                        <span class="w60">방송사 : ${item.BROAD_STATNM}</span><br>
                        </c:if>
                        
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_scenario_licensor" )){
                      
                         ArrayList<Map<String, Object>> dg_scenario_licensorList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_scenario_licensorList.add(exMap);
                         }
                            request.setAttribute("dg_scenario_licensorList", dg_scenario_licensorList);
                        %>
                       <c:forEach var="item" items="${dg_scenario_licensorList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">  방송대본 저작권자</td>
                    <td class="liceSrch">
                      <b>${item.LICENSOR_NAME_KOR}</b>
                      <p class="mt15">
                      <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                        <span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
                       </c:if>
                       <c:if test="${!empty item.LICENSORROLE}">
                        <span class="w60">저작권자역할 : ${item.LICENSORROLE}</span><br>
                        </c:if>
                      </p>
                    </td>
                    </tr>
                   </c:forEach>
                        <%
                      
                    }  else if(documentSet2.get( "Id" ).equals( "dg_scenario" )){
                         
                         ArrayList<Map<String, Object>> dg_scenarioList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_scenarioList.add(exMap);
                         }
                            request.setAttribute("dg_scenarioList", dg_scenarioList);
                        %>
                        <c:forEach var="item" items="${dg_scenarioList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;"> 방송대본저작물</td>
                    <td class="liceSrch">
                      <b>${item.TITLE}</b>
                      <p class="mt15">
                      <c:if test="${!empty item.TITLE}">
                        <span>저작물명 : ${item.TITLE}</span>
                       </c:if>
                       <c:if test="${!empty item.WRITER}">
                        <span class="w60">작가명 : ${item.WRITER}</span><br>
                       </c:if>
                       <c:if test="${!empty item.GENRE_KIND}">
                        <span>장르구분 : ${item.GENRE_KIND}</span>
                        </c:if>
                        <c:if test="${!empty item.SUBJ_KIND}">
                        <span class="w60">소재분류 : ${item.SUBJ_KIND}</span><br>
                        </c:if>
                        <c:if test="${!empty item.DIRECT}">
                        <span>연출자 : ${item.DIRECT}</span>
                        </c:if>
                        <c:if test="${!empty item.ORIG_WORK}">
                        <span class="w60">원작명 : ${item.ORIG_WORK}</span><br>
                        </c:if>
                        <c:if test="${!empty item.ORIG_WRITER}">
                        <span>원작자명 : ${item.ORIG_WRITER}</span>
                        </c:if>
                        <c:if test="${!empty item.PLAYERS}">
                        <span class="w60">주요출연진 : ${item.PLAYERS}</span><br>
                        </c:if>
                        <c:if test="${!empty item.BROAD_ORD}">
                        <span>방송회차 : ${item.BROAD_ORD}</span>
                        </c:if>
                        <c:if test="${!empty item.BROAD_DATE}">
                        <span class="w60">방송일자 : ${item.BROAD_DATE}</span><br>
                        </c:if>
                        <c:if test="${!empty item.BROAD_MEDINM}">
                        <span>방송매체 : ${item.BROAD_MEDINM}</span>
                        </c:if>
                        <c:if test="${!empty item.BROAD_STATNM}">
                        <span class="w60">방송사 : ${item.BROAD_STATNM}</span><br>
                        </c:if>
                        
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                      
                    }else if(documentSet2.get( "Id" ).equals( "dg_scenario_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_scenario_licensorList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_scenario_licensorList.add(exMap);
                         }
                            request.setAttribute("dg_scenario_licensorList", dg_scenario_licensorList);
                        %>
                        <c:forEach var="item" items="${dg_scenario_licensorList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">  방송대본 저작권자</td>
                    <td class="liceSrch">
                      <b>${item.LICENSOR_NAME_KOR}</b>
                      <p class="mt15">
                       <c:if test="${!empty item.LICENSOR_NAME_KOR}">
                        <span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
                        </c:if>
                         <c:if test="${!empty item.LICENSORROLE}">
                        <span class="w60">저작권자역할 : ${item.LICENSORROLE}</span><br>
                        </c:if>
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                      
                    }  else if(documentSet2.get( "Id" ).equals( "dg_regcopy" )){
                         
                         ArrayList<Map<String, Object>> dg_regcopyList = new ArrayList<Map<String, Object>>();
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         if(!documentSet_dg_album.get("TotalCount").equals( "0" )){objCont2++;}
                         for(x = 0; x < documentSize ; x++){
                              Map<String, Object> exMap = new HashMap<String, Object>();   
                              obj = (Map)document.get( x );
                              /* objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_regcopyList.add(exMap);
                         }
                            request.setAttribute("dg_regcopyList", dg_regcopyList);
                        %>
                        <c:forEach var="item" items="${dg_regcopyList}" varStatus="status">
                        <tr>
                    <td scope="row" class="vtop" style="border-left: none; padding-right: 20px;">  <%=objMap.get("CONT_CLASS_NAME")%></td>
                    <td class="liceSrch">
                      <b>${item.CONT_TITLE}</b>
                      <p class="mt15">
                      <c:if test="${!empty item.AUTHOR_NAME}">
                        <span>저작권자명 : <%=objMap.get("AUTHOR_NAME")%></span>
                      </c:if>
                      <c:if test="${!empty item.CONT_TITLE}">
                        <span class="w60">저작권명 : ${item.CONT_TITLE}</span><br>
                      </c:if>
                      </p>
                    </td>
                    </tr>
                    </c:forEach>
                        <%
                      
                    }  
                  }
                  }
                  
                  }else{
                    

                
                  }
              %>

            
              </tbody>
            </table>
            <!-- 디지털 저작권 거래소 연동 데이터  -->
            
            </div><!-- End sub_contents_con -->
      
            
            
            
          </div><!-- End con_rt -->
          <p class="clear"></p>
       <!--  </div> --><!-- End contents -->

        <!-- //주요컨텐츠 end -->
          <!-- //content -->
          
          <!-- FOOTER str-->
        <jsp:include page="/include/2017/footer.jsp" />
        <!-- FOOTER end -->
    
        
                
  <!-- //전체를 감싸는 DIVISION -->

</body>
</html>