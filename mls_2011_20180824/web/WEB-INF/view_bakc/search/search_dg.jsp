<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.json.simple.JSONObject"%>
<%-- <%@page import="org.codehaus.jackson.JsonParser"%> --%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@ page language="java" contentType="text/html; charset=euc-kr"
    pageEncoding="euc-kr"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>저작권 찾기 서비스란? | 저작권 찾기 소개 | 저작권자찾기</title>
</head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">

<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 

//<!--

/* 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-69621660-2', 'auto');
 ga('send', 'pageview');
  */
 function crosPop(){
		window.open("https://cras.copyright.or.kr/front/right/comm/main_.do");
	}

 
 function statBoRegiPop(){
		window.open("/statBord/statBo06Regi.do", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes");	
 }
 
 function statBoSelect(){
		window.open("/statBord/statBo08Select.do", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes");	
}
 
 function goSearch(){
		
	    var fnm = document.search;
	    var query=fnm.query.value;
	    query=query.replace(/^\s+/,""); 

	    if (query == "" || query == null) {
	        alert("검색어를 입력하세요.");
	        return;
	    }

	    rangeValue="";
	    for(i=0; i<fnm.range.length; i++){    	
	    	if(fnm.range[i].checked){    		
	    		if(rangeValue!="") rangeValue = rangeValue+",";
	    		rangeValue = rangeValue+fnm.range[i].value;    		    		
	    	}
	    }
	    
	    fnm.arr_range.value = rangeValue;    

	    fnm.submit();
	    return false;
}
 
 function goCategory(Str){
		var collection = Str;
		var frm = document.search;
		if(Str=='cf_reg'){
			frm.collection.value = 'cf_reg';
		}else{
			frm.collection.value = Str;
		}
		frm.subPageStr.value = 'Y';
		frm.action = "/search/search.do";
		frm.submit();
	}
  
function comma(num){
    var len, point, str; 
       
    num = num + ""; 
    point = num.length % 3 ;
    len = num.length; 
    str = num.substring(0, point); 
    while (point < len) { 
        if (str != "") str += ","; 
        str += num.substring(point, point + 3); 
        point += 3; 
    } 
    return str;
}

function goDgCategory(str){
	var frm = document.search;
	
	frm.action = "/search/search.do";
	changeGenreCd(str);
	frm.submit();
}

</script>

</head>

<body>
		
		<!-- HEADER str-->
		<jsp:include page="/include/2017/header.jsp" />
		
<script type="text/javascript"> 
$(function(){
	
	var genreCd ='${genreCd}';
	var menuFlag = '${menuFlag}';
	var collectionName = '${collectionName}';
	var collectionName2 = '${collectionName}';
	var collectionNameArr =  collectionName.split(",");
	var indexNum = collectionName.indexOf(',');
	console.log(indexNum);
	if(indexNum!=-1){
		collectionName = collectionName.substring(0,collectionName.indexOf(','));
	}
	
 	if(collectionNameArr.length > 2){
		$('#collection option[value='+collectionName2+']').attr('selected','selected'); 	
	}else{ 
		$('#collection option[value='+collectionName+']').attr('selected','selected'); //*/	
	}
 	$('#dgCnt').html(comma($.trim($('#dgCnt').html())));
	
	
	var flag = false;
	
	for(var i = 0 ; i < 12 ; i++){
		
		if($("#topMenu"+i).html()==genreCd){
			$("#topMenu"+i).parent().parent().attr("class","first on");
			flag = true;
		}
	}
	/* if(flag==false){
		$("#topMenu0").parent().parent().attr("class","first on");
	} */
	//console.log("genreCd : " + genreCd)
	if($('.con_rt_hd_title').html()!=genreCd){
		if(genreCd=='total'){
			if(menuFlag=='N'){
				$('.con_rt_hd_title').html('상당한 노력 신청 서비스');
			}else{
				$('.con_rt_hd_title').html('전체');
			}
			
		}else{
			//console.log("genreCd : " + genreCd)
			$('.con_rt_hd_title').html(genreCd);
		}
	}
	
	
})	

</script>
<!-- 		<script type="text/javascript">initNavigation(1);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- content st -->
			<div id="contents">
			${menuFlag}
			 <c:if test="${menuFlag!='N'}">
            <div class="con_lf">
			<div class="con_lf_big_title">분야별 <br>권리자 찾기</div>
				<ul class="sub_lf_menu">
				<li><a onclick="javascript:goInnerSearch('999');">전체</a></li>
				<li><a onclick="javascript:goInnerSearch('1');">음악</a></li>
				<li><a onclick="javascript:goInnerSearch('2');">어문</a></li>
				<li><a onclick="javascript:goInnerSearch('3');">방송대본</a></li>
				<li><a onclick="javascript:goInnerSearch('4');">영화</a></li>
				<li><a onclick="javascript:goInnerSearch('5');">방송</a></li>
				<li><a onclick="javascript:goInnerSearch('6');">뉴스</a></li>
				<li><a onclick="javascript:goInnerSearch('7');">미술</a></li>
				<li><a onclick="javascript:goInnerSearch('8');">이미지</a></li>
				<li><a onclick="javascript:goInnerSearch('9');">사진</a></li>
				<li><a onclick="javascript:goInnerSearch('99');">기타</a></li>
				</ul>
			</div>
			</c:if>
			<c:if test="${menuFlag eq 'N'}">
			<div class="con_lf">
			<div class="con_lf_big_title">법정허락<br>승인 신청</div>
				<ul class="sub_lf_menu">
				<li><a href="javascript:goInnerSearch('99');">상당한 노력 신청 서비스</a></li>
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
				<li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li>
				<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
				<li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
				<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
				<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
				<li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li>
				</ul>
			</div>
			</c:if>
<div class="con_rt">
						<div class="con_rt_head">
							<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
							&gt;
							저작권자 찾기
							&gt;
							디지털저작권거래소
							<%-- <c:if test="${genreCd=='total'}"><span class="bold">전체</span></c:if>	
							<c:if test="${genreCd!='total'}"><span class="bold">${genreCd}</span></c:if> --%>	
						</div>
						<div class="con_rt_hd_title">디지털저작권거래소</div>
					
					
						<div id="sub_contents_con">
						<form id="search" name="search" action="/search/search.do" method="post" onSubmit="goSearch(); return false">
									<div class="bg_f8f8f8">
									
									<!-- 2014.11.06 추가 -->
									<div>
										<div class="float_lf">
											<span class="sub_blue_point_bg">검색대상</span>&nbsp;&nbsp;&nbsp;
												  <input type="checkbox" value="all" name="range" rel="1" id="chkall" onclick="check(1);" ><label for="chkall" class="thin p12">전체</label>
												  <input type="checkbox" value="title" name="range" rel="2" id="chktl" onclick="check(2);" ><label for="chktl"  class="thin p12">저작물명</label>
												  <input type="checkbox" value="licer_detail" name="range" rel="2" id="chktl" onclick="check(2);"><label for="chktl"  class="thin p12">저작권자</label></td>
											</div>
											<div class="float_rt">
											<span class="sub_blue_point_bg">정렬방식</span>&nbsp;&nbsp;&nbsp;
												<select name="sort" id="line">
															<option value="RANK">정확도순</option>
															<option value="DATE">날짜순</option>
												</select>
												<select name="sortOrder" id="">
															<option value="DESC">내림차순</option>
															<option value="ASC">오름차순</option>
												</select>
											</div> 
									<p class="clear"></p>
									</div>
									
										<div class="mar_tp10">
										<select id="collection" name="collection" title="장르">
											<option value="dg_music1,dg_literature,dg_movie,dg_news">전체</option>
											<option value="dg_literature">어문저작물</option>
											<option value="dg_movie">영화저작물</option>
											<option value="dg_music1">음악저작물</option>
											<option value="dg_news">뉴스저작물</option>
										</select>
										<input type="text" title="검색어" id="query" name="query" size="30" value="${query}" style="IME-MODE: active;padding:3px;width:48%;"/>
										<input type="submit" value="검색" style="background-color: #2c65aa; color: white;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chk1" name="resrch_check" class="checkbox" onclick="javascript:resrch_chk();"/><label for="chk1" class="p11 strong black">결과 내 검색</label>
										</div>
									</div>
							<input type="hidden" name="resultcount" value="10"/>
							<input type="hidden" name="page" value=""/>
							<input type="hidden" name="mode" value=""/>
							<input type="hidden" name="arr_range" value="">
							<input type="hidden" name="resrch" value=""/>							
							<input type="hidden" id="urlCd" name="urlCd" value="01"/>
							<input type="hidden" id="subPageStr" name="subPageStr" value="dg"/>
							<input type="hidden" id="startCount" name="startCount" value="1"/>
							<input type="hidden" id="genreCd" name="genreCd" value=""/>
 						</form>
			
										
						<p class="clear"></p>
						</div>
			<%
							/* request.getAttribute("searchResult") */
							String  jsonData = (String) request.getAttribute("searchResult");// 검색엔진 전체 json데이터 
							JSONParser jsonParser = new JSONParser();// json 파서
							
							JSONObject jsonObject2 = (JSONObject)jsonParser.parse( jsonData );//json 데이터 파싱 시작
							//out.print(jsonObject2);
							Map jsonMap = (Map) jsonObject2.get( "SearchQueryResult" ); 
							 JSONArray collection = (JSONArray) jsonMap.get("Collection");  
							 //Map documentSet = (Map) collection.get( 0 );
							//Map documentSetMap = (Map) documentSet.get("DocumentSet");
							/* JSONArray documentJA = (JSONArray) documentSetMap.get("Document");
							
							
							ㅐ
							JSONObject jsonCntObject = (JSONObject)request.getAttribute("SearchColCount");//json 데이터 파싱 시작
							
							Map jsonCntMap = (Map)jsonCntObject.get("SearchColCount");
							Iterator<String> keys = jsonCntMap.keySet().iterator();
							
							int subCnt = 0;
							int totalCnt = 0;
							
							while( keys.hasNext() ){
					            String key = keys.next();
					            int temp = (Integer) jsonCntMap.get(key);
					            if(!key.equals( "cf_reg" ) && !key.equals( "reg_copyright") && temp!=-1 && temp !=-972){
					            	subCnt += temp;
					            }
					            if(!key.equals( "reg_copyright") && temp!=-1 && temp !=-972){
					            	totalCnt += temp;
					            }
					        } */
							
						%>
					
						
						<!-- 디지털 저작권 거래소 연동 데이터  -->
						<div class="bg_2e75b6 mar_tp10">  
									<h2 class="float_lf color_fff blod">디지털저작권거래소(<font id="dgCnt">
									
									<%
									int collectionSize = collection.size();
									
								  	Map obj = null;
					        		Map objMap = null;
					        		Map documentSet_dg_album = null;
					        		ArrayList document = null;
					        		int documentSize = 0;
					        		int x = 0;
									int dgAllCount = 0;
									Map documentCntMap =null;
									for(int y = 0 ; y < collectionSize; y++){
										documentCntMap = (Map)collection.get( y );
										String collectionId=(String) documentCntMap.get( "Id" );
										documentSet_dg_album = (Map)documentCntMap.get( "DocumentSet" );
										if(collectionId.substring( 0,2 ).equals( "dg" )){
											//out.print("true : "+collectionId+"<br>");
											int tempInt= Integer.parseInt( (String) documentSet_dg_album.get("TotalCount") );
											dgAllCount += tempInt;
										}
									
									}
									out.print(dgAllCount);
									%>
									</font>
									
									건)</h2>
										<!-- <p class="bzhlag"><a href="#"><img src="/images/sub/mulem.png" alt=" " /></a>
											<span class="bzhlag22"></span>
										</p> -->
										
									<%-- 	<a href="javascript: goCategory('<%=str%>')" class="float_rt color_fff">더보기</a> --%>
						<p class="clear"></p>
						</div>
						<table class="sub_tab1" cellspacing="0" cellpadding="0" width="100%" summary="저작권찾기 신청정보입니다." class="grid">
							<colgroup>
								<col width="10%">
								<col>
								<col>
							</colgroup>
							<tbody>	
							<%-- <%= documentCntMap %> --%>
							<% 
						
					        for(int z = 0 ; z < collectionSize ; z++){
					        	Map documentSet2 = (Map)collection.get( z );
					        	if(documentSet2.get( "Id" ).equals( "dg_album" )){
					        		documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
					         		document= (ArrayList)documentSet_dg_album.get("Document");
					        		documentSize= document.size();
					        	  ArrayList<Map<String, Object>> dg_albumList = new ArrayList<Map<String, Object>>();     		
					        		for(x = 0; x < documentSize ; x++){
					        		     Map<String, Object> exMap = new HashMap<String, Object>(); 
					        			obj = (Map)document.get( x );
					        		/* 	objMap = (Map)obj.get( "Field" ); */
					        		  exMap = (Map)obj.get("Field");
					        		  dg_albumList.add(exMap);
			                }
					        		
			                request.setAttribute("dg_albumList", dg_albumList);
			               
					        		%>
					        		<c:forEach var="item" items="${dg_albumList}" varStatus="status">
					        		<tr>
										<th scope="row" class="vtop">앨범</th>
										<td class="liceSrch">
											<b>${item.ALBUM_TITLE} </b>
											<p class="mt15">
											<c:if test="${!empty item.ALBUM_TITLE}">
												<span class="w60">앨범 명 : ${item.ALBUM_TITLE} </span>
											</c:if>
											<c:if test="${!empty item.DOCID}">
												<span class="w60">앨범 아이디 : ${item.DOCID} </span>
											</c:if>
											<c:if test="${!empty item.DATE}">
												<span>앨범 발매일 : ${item.DATE} </span></br>
											</c:if>
											<c:if test="${!empty item.ALBUM_LABLE_NAME}">
												<span>앨범 라벨명 : ${item.ALBUM_LABLE_NAME} </span></br>
											</c:if>
											<c:if test="${!empty item.DISTRIBUTION_COMPANY}">
												<span class="w60">회사 : ${item.DISTRIBUTION_COMPANY}  </span>
											</c:if>
											<c:if test="${!empty item.PRODUCER}">
												<span class="w60">앨범 제작사 : ${item.PRODUCER} </span></br>
											</c:if>
											<c:if test="${!empty item.EDITION}">
												<span>에디션 : ${item.EDITION}</span>
											</c:if>
											<c:if test="${!empty item.ALBUM_TYPE}">
												<span class="w60">앨범 형태 : ${item.ALBUM_TYPE} </span></br>
											</c:if>
											<c:if test="${!empty item.ALBUM_MEDIA_TYPE}">
												<span class="w60">앨범 미디어타입 : ${item.ALBUM_MEDIA_TYPE} </span>
											</c:if>
											
											</p>
										</td>
									</tr>
									</c:forEach>
					        		<%
					        		
					        		/* 
					        		break; */
								}else if(documentSet2.get( "Id" ).equals( "dg_art" )){
								     ArrayList<Map<String, Object>> dg_artList = new ArrayList<Map<String, Object>>(); 
									documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
					        		document= (ArrayList)documentSet_dg_album.get("Document");
					        		documentSize= document.size();
					        		for(x = 0; x < documentSize ; x++){
					        		     Map<String, Object> exMap = new HashMap<String, Object>(); 
		                        obj = (Map)document.get( x );
		                      /*  objMap = (Map)obj.get( "Field" ); */
		                        exMap = (Map)obj.get("Field");
		                        dg_artList.add(exMap);
		                      }
					        		
		                      request.setAttribute("dg_artList", dg_artList);
					        			%>
					        			<c:forEach var="item" items="${dg_artList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop">미술저작물</th>
										<td class="liceSrch">
											<b>${item.TITLE}</b>
											<p class="mt15">
											<c:if test="${!empty item.TITLE}">
												<span class="w60">저작물명 : ${item.TITLE}</span>
											</c:if>
											<c:if test="${!empty item.DOCID}">
												<span class="w60">미술저작물 아이디 : ${item.DOCID}</span>
											</c:if>
											<c:if test="${!empty item.SUBTITLE}">
												<span>부제 : ${item.SUBTITLE}</span></br>
											</c:if>
											<c:if test="${!empty item.ART_DIV_INFO}">
												<span class="w60">분류정보 : ${item.ART_DIV_INFO}</span>
											</c:if>
											<c:if test="${!empty item.CLTN_ORG_NM and  item.CLTN_ORG_NM ne '없음'}">
												<span class="w60">소장기관명 : ${item.CLTN_ORG_NM}</span></br>
											</c:if>
											<c:if test="${!empty item.SIZE_INFO}">
												<span>크기정보 : ${item.SIZE_INFO}</span>
											</c:if>
											<c:if test="${!empty item.MAIN_MTRL}">
												<span class="w60">주재료 : ${item.MAIN_MTRL}</span></br>
											</c:if>
											<c:if test="${!empty item.STRU_FTRE}">
												<span class="w60">구조및특징 : ${item.STRU_FTRE}</span>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_art_licensor" )){
					        	     
					        	     ArrayList<Map<String, Object>> dg_art_licensorList = new ArrayList<Map<String, Object>>(); 
				                 documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
				                 document= (ArrayList)documentSet_dg_album.get("Document");
				                 documentSize= document.size();
				                 for(x = 0; x < documentSize ; x++){
	                           Map<String, Object> exMap = new HashMap<String, Object>(); 
	                            obj = (Map)document.get( x );
	                          /*  objMap = (Map)obj.get( "Field" ); */
	                            exMap = (Map)obj.get("Field");
	                            dg_art_licensorList.add(exMap);
	                          }
	                          request.setAttribute("dg_art_licensorList", dg_art_licensorList);
					        			%>
					        			<c:forEach var="item" items="${dg_art_licensorList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop">미술저작권자</th>
										<td class="liceSrch">
											<b>${item.TITLE}</b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권역할 : ${item.LICENSORROLE}</span>
											</c:if>
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span class="w60">저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_book" )){
					        	     
                         ArrayList<Map<String, Object>> dg_bookList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_bookList.add(exMap);
                            }
                            request.setAttribute("dg_bookList", dg_bookList);
					        			%>
					        			  <c:forEach var="item" items="${dg_bookList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop">도서</th>
										<td class="liceSrch">
											<b>${item.TITLE}</b>
											<p class="mt15">
											 <c:if test="${!empty item.TITLE}">
												<span class="w60">도서명 : ${item.TITLE}</span>
											</c:if>
											<c:if test="${!empty item.DOCID}">
												<span>아이디 : ${item.DOCID}</span></br>
											</c:if>
											<c:if test="${!empty item.FIRST_EDITION_YEAR}">
												<span>발행년도 : ${item.FIRST_EDITION_YEAR}</span>
											</c:if>
											<c:if test="${!empty item.WRITER}">
												<span class="w60">작가 : ${item.WRITER}</span></br>
											</c:if>
											<c:if test="${!empty item.TRANSLATOR}">
												<span>번역가 : ${item.TRANSLATOR}</span>
											</c:if>
											<c:if test="${!empty item.PUBLISHER}">
												<span class="w60">발행자명 : ${item.PUBLISHER}</span>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_broadcast" )){
                         
                         ArrayList<Map<String, Object>> dg_broadcastList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_broadcastList.add(exMap);
                            }
                            request.setAttribute("dg_broadcastList", dg_broadcastList);
					        			%>
					        			 <c:forEach var="item" items="${dg_broadcastList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop">방송저작권자</th>
										<td class="liceSrch">
											<b>${item.PROGRAM_NAME}</b>
											<p class="mt15">
											<c:if test="${!empty item.AUTHOR_NAME}">
												<span>저작물명 : ${item.PROGRAM_NAME} </span>
											</c:if>
											<c:if test="${!empty item.PROGRAM_NAME}">
												<span class="w60">프로그램명 :${item.PROGRAM_NAME} </span>
											</c:if>
											<c:if test="${!empty item.DATE}">
												<span>방송일자 : ${item.DATE}</span>
											</c:if>
											<c:if test="${!empty item.GNRE_CODE_NAME}">
												<span class="w60">방송장르 : ${item.GNRE_CODE_NAME}</span></br>
											</c:if>
											<c:if test="${!empty item.MAKE_TYPE}">
												<span>제작타입 : ${item.MAKE_TYPE}</span>
											</c:if>
											<c:if test="${!empty item.MAKE_NAME}">
												<span class="w60">제작사 : ${item.MAKE_NAME}</span></br>
											</c:if>
											<c:if test="${!empty item.MAKE_PD}">
												<span>담당PD : ${item.MAKE_PD}</span>
											</c:if>
											<c:if test="${!empty item.PRODUCER}">
												<span class="w60">연출자 : ${item.PRODUCER}</span></br>
											</c:if>
											<c:if test="${!empty item.INOUT_TYP}">
												<span>방송회차 : ${item.INOUT_TYP}</span>
											</c:if>
											<c:if test="${!empty item.WRITER}">
												<span class="w60">대표작가 : ${item.WRITER}</span></br>
											</c:if>
											<c:if test="${!empty item.STARTDAY}">
												<span>방송일자 :${item.STARTDAY} </span>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_broadcast_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_broadcast_licensorList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_broadcast_licensorList.add(exMap);
                            }
                            request.setAttribute("dg_broadcast_licensorList", dg_broadcast_licensorList);
					        			%>
					        			 <c:forEach var="item" items="${dg_broadcast_licensorList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop">방송저작권자</th>
										<td class="liceSrch">
											<b>${item.LICENSOR_NAME_KOR}</b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권역할 : ${item.LICENSORROLE}</span>
											</c:if>
											</p>
										</td>
										</tr>
									</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_character" )){
                         
                         ArrayList<Map<String, Object>> dg_characterList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_characterList.add(exMap);
                            }
                            request.setAttribute("dg_characterList", dg_characterList);
					        			%>
					        			<c:forEach var="item" items="${dg_characterList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop">캐릭터저작물</th>
										<td class="liceSrch">
											<b>${item.CHAR_NM_KOR}</b>
											<p class="mt15">
											<c:if test="${!empty item.CHAR_NM_KOR}">
												<span>저작물명 : ${item.CHAR_NM_KOR}</span>
											</c:if>
											<c:if test="${!empty item.CHAR_DESC}">
												<span class="w60">설명 : ${item.CHAR_DESC}</span>
											</c:if>
											<c:if test="${!empty item.WORK_NM_KOR}">
												<span>작품명 : ${item.WORK_NM_KOR}</span>
											</c:if>
											<c:if test="${!empty item.WORKGENRENM}">
												<span class="w60">장르 : ${item.WORKGENRENM}</span></br>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_character" )){
                         
                         ArrayList<Map<String, Object>> dg_characterList2 = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_characterList2.add(exMap);
                            }
                            request.setAttribute("dg_characterList2", dg_characterList2);
					        			%>
					        			<c:forEach var="item" items="${dg_characterList2}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop">캐릭터저작권자</th>
										<td class="liceSrch">
											<b>${item.LICENSOR_NAME_KOR}<%=objMap.get("LICENSOR_NAME_KOR")%></b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권역할 : ${item.LICENSORROLE}</span>
											</c:if>
											<c:if test="${!empty item.NATINAME}">
												<span>국적 : ${item.NATINAME}</span>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_content" )){
                         
                         ArrayList<Map<String, Object>> dg_contentList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_contentList.add(exMap);
                            }
                            request.setAttribute("dg_contentList", dg_contentList);
					        			%>
					        			<c:forEach var="item" items="${dg_contentList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop">방송컨텐츠 저작물</th>
										<td class="liceSrch">
											<b>: ${item.TITLE}</b>
											<p class="mt15">
											<c:if test="${!empty item.TITLE}">
												<span>저작물명 : : ${item.TITLE}</span>
											</c:if>
											<c:if test="${!empty item.SUBPR_NAME}">
												<span class="w60">부제 : : ${item.SUBPR_NAME}</span></br>
											</c:if>
											<c:if test="${!empty item.BROAD_DATE}">
												<span>방송일자 : : ${item.BROAD_DATE}</span>
											</c:if>
											<c:if test="${!empty item.MEDI_CODE_NM}">
												<span class="w60">프로그램명 : : ${item.MEDI_CODE_NM}</span></br>
											</c:if>
											<c:if test="${!empty item.CHNL_CODE_NM}">
												<span>매체코드 : : ${item.CHNL_CODE_NM}</span>
											</c:if>
											<c:if test="${!empty item.MAKE_NAME}">
												<span class="w60">채널코드 :: ${item.MAKE_NAME}</span></br>
											</c:if>
											<c:if test="${!empty item.PROD_QULTY}">
												<span>제작품질 : : ${item.PROD_QULTY}</span>
											</c:if>
											<c:if test="${!empty item.PTABL_TITLE}">
												<span class="w60">편성표제목 : : ${item.PTABL_TITLE}</span></br>
											</c:if>
											<c:if test="${!empty item.PROG_GRAD}">
												<span>프로그램등급 : : ${item.PROG_GRAD}</span>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_content_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_content_licensorList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_content_licensorList.add(exMap);
                            }
                            request.setAttribute("dg_content_licensorList", dg_content_licensorList);
					        			%>
					        			<c:forEach var="item" items="${dg_content_licensorList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 방송컨텐츠 저작권자</th>
										<td class="liceSrch">
											<b>${item.LICENSOR_NAME_KOR}</b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span>저작권자 명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권 역할 : ${item.LICENSORROLE}</span>
											</c:if>
											<c:if test="${!empty item.LICENSOR_SEQ}">
												<span>저작권자 명 : ${item.LICENSOR_SEQ}</span>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_literature" )){
                         
                         ArrayList<Map<String, Object>> dg_literatureList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_literatureList.add(exMap);
                            }
                            request.setAttribute("dg_literatureList", dg_literatureList);
					        			%>
					        			<c:forEach var="item" items="${dg_literatureList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 어문저작물</th>
										<td class="liceSrch">
											<b>${item.TITLE}</b>
											<p class="mt15">
											 <c:if test="${!empty item.TITLE}">
												<span>저작물명 : ${item.TITLE}</span>
											 </c:if>
											 <c:if test="${!empty item.WRITER}">
												<span class="w60">작가 : ${item.WRITER}</span></br>
											 </c:if>
											 <c:if test="${!empty item.BOOKTITLE}">
												<span>수록도서명 : ${item.BOOKTITLE}</span>
											 </c:if>
											 <c:if test="${!empty item.FIRST_EDITION_YEAR}">
												<span class="w60">발행년도 : ${item.FIRST_EDITION_YEAR}</span></br>
											 </c:if>
											 <c:if test="${!empty item.PUBLISHER}">
												<span>발행자 : ${item.PUBLISHER}</span>
											 </c:if>
											 <c:if test="${!empty item.INOUT_TYPE}">
												<span class="w60">발행국가 : ${item.INOUT_TYPE} </span></br>
											 </c:if>
											 <c:if test="${!empty item.PUB_LANG}">
												<span>언어코드 : ${item.PUB_LANG}</span>
											 </c:if>
											 <c:if test="${!empty item.MATERIAL_TYPE}">
												<span class="w60">매체형태 : ${item.MATERIAL_TYPE}</span></br>
											 </c:if>
											 <c:if test="${!empty item.PUBLISH_TYPE}">
												<span>자료유형 : ${item.PUBLISH_TYPE}</span>
											 </c:if>
											 <c:if test="${!empty item.RETRIEVE_TYPE}">
												<span class="w60">검색분류 : ${item.RETRIEVE_TYPE}</span></br>
											 </c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_literature_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_literature_licensorList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_literature_licensorList.add(exMap);
                            }
                            request.setAttribute("dg_literature_licensorList", dg_literature_licensorList);
					        			%>
					        			<c:forEach var="item" items="${dg_literature_licensorList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 어문저작권자</th>
										<td class="liceSrch">
											<b>${item.LICENSOR_NAME_KOR}</b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권자역할 : ${item.LICENSORROLE}</span></br>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_movie" )){
                         
                         ArrayList<Map<String, Object>> dg_movieList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_movieList.add(exMap);
                            }
                            request.setAttribute("dg_movieList", dg_movieList);
					        			%>
					        			<c:forEach var="item" items="${dg_movieList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 영화저작물</th>
										<td class="liceSrch">
											<b>${item.TITLE}</b>
											<p class="mt15">
												<span>저작물명 : ${item.TITLE}</span>
												<c:if test="${!empty item.GENRE_NAME}">
												<span class="w60">장르구분 : ${item.GENRE_NAME}</span></br>
												</c:if>
												<c:if test="${!empty item.MOVIE_TYPE_NAME}">
												<span>영화형태 : ${item.MOVIE_TYPE_NAME}</span>
												</c:if>
												<c:if test="${!empty item.VIEW_GRADE_NAME}">
												<span class="w60">관람등급 : ${item.VIEW_GRADE_NAME}</span></br>
												</c:if>
												<c:if test="${!empty item.PRODUCE_YEAR}">
												<span>제작년도 : ${item.PRODUCE_YEAR}</span>
												</c:if>
												<c:if test="${!empty item.RELEASE_DATE}">
												<span class="w60">개봉일자 : ${item.RELEASE_DATE}</span></br>
												</c:if>
												<c:if test="${!empty item.LEADING_ACTOR}">
												<span>출연진 : ${item.LEADING_ACTOR}</span>
												</c:if>
												<c:if test="${!empty item.DIRECTOR}">
												<span class="w60">감독/연출자 : ${item.DIRECTOR}</span></br>
												</c:if>
												<c:if test="${!empty item.PRODUCER}">
												<span>제작사 : ${item.PRODUCER}</span>
												</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_movie_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_movie_licensorList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_movie_licensorList.add(exMap);
                            }
                            request.setAttribute("dg_movie_licensorList", dg_movie_licensorList);
					        			%>
					        			<c:forEach var="item" items="${dg_movie_licensorList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 영화저작권자</th>
										<td class="liceSrch">
											<b>${item.LICENSOR_NAME_KOR}</b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권자역할 : ${item.LICENSORROLE}</span></br>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_music1" )){
                         
                         ArrayList<Map<String, Object>> dg_music1List = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_music1List.add(exMap);
                            }
                            request.setAttribute("dg_music1List", dg_music1List);
					        			%>
					        			<c:forEach var="item" items="${dg_music1List}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 음악 저작물</th>
										<td class="liceSrch">
											<b>${item.TITLE}</b>
											<p class="mt15">
											<c:if test="${!empty item.TITLE}">
												<span>저작물명 : ${item.TITLE}</span>
											</c:if>
											<c:if test="${!empty item.ALBUM_TITLE}">
												<span class="w60">수록앨범명 : ${item.ALBUM_TITLE}</span></br>
											</c:if>
											<c:if test="${!empty item.INOUT_TYPE}">
												<span>국내외구분 : ${item.INOUT_TYPE}</span>
											</c:if>
											<c:if test="${!empty item.SINGER}">
												<span class="w60">가수 : ${item.SINGER}</span></br>
											</c:if>
											<c:if test="${!empty item.LYRICIST}">
												<span>작사가 : ${item.LYRICIST}</span>
											</c:if>
											<c:if test="${!empty item.COMPOSER}">
												<span class="w60">작곡가 : ${item.COMPOSER}</span></br>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_music_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_music_licensorList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_music_licensorList.add(exMap);
                            }
                            request.setAttribute("dg_music_licensorList", dg_music_licensorList);
					        			%>
					        			<c:forEach var="item" items="${dg_music_licensorList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 음악 저작물</th>
										<td class="liceSrch">
											<b>${item.LICENSOR_NAME_KOR}</b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권자역할 : ${item.LICENSORROLE}</span></br>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_news" )){
                         
                         ArrayList<Map<String, Object>> dg_newsList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_newsList.add(exMap);
                            }
                            request.setAttribute("dg_newsList", dg_newsList);
					        			%>
					        			<c:forEach var="item" items="${dg_newsList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 뉴스 저작물</th>
										<td class="liceSrch">
											<b>${item.TITLE} </b>
											<p class="mt15">
											<c:if test="${!empty item.TITLE}">
												<span>저작물명 : ${item.TITLE}</span>
											</c:if>
											<c:if test="${!empty item.REPORTERNM}">
												<span class="w60">언론사명 : ${item.REPORTERNM}</span></br>
											</c:if>
											<c:if test="${!empty item.ARTICLDT}">
												<span>기사일자 : ${item.ARTICLDT}</span>
											</c:if>
											<c:if test="${!empty item.CANAME}">
												<span class="w60">관리단체 : ${item.CANAME}</span></br>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_news_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_news_licensorList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_news_licensorList.add(exMap);
                            }
                            request.setAttribute("dg_news_licensorList", dg_news_licensorList);
					        			%>
					        			<c:forEach var="item" items="${dg_news_licensorList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 뉴스 저작권자</th>
										<td class="liceSrch">
											<b>${item.LICENSOR_NAME_KOR}</b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권자역할 : ${item.LICENSORROLE} </span></br>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_public" )){
                         
                         ArrayList<Map<String, Object>> dg_publicList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_publicList.add(exMap);
                            }
                            request.setAttribute("dg_publicList", dg_publicList);
					        			%>
					        			<c:forEach var="item" items="${dg_publicList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 공공 저작물</th>
										<td class="liceSrch">
											<b>${item.TITLE} </b>
											<p class="mt15">
											<c:if test="${!empty item.TITLE}">
												<span>저작물명 : ${item.TITLE}</span>
											</c:if>
											<c:if test="${!empty item.GENRE_NAME}">
												<span class="w60">UCI : ${item.GENRE_NAME}</span></br>
											</c:if>
											<c:if test="${!empty item.TYPE_DIV_L_CD}">
												<span>형식대분류코드 : ${item.TYPE_DIV_L_CD}</span>
											</c:if>
											<c:if test="${!empty item.TYPEDIVLNM}">
												<span class="w60">형식대분류 : ${item.TYPEDIVLNM}</span></br>
											</c:if>
											<c:if test="${!empty item.TYPE_DIV_M_CD}">
												<span>형식중분류코드 : ${item.TYPE_DIV_M_CD}</span>
											</c:if>
											<c:if test="${!empty item.TYPEDIVMNM}">
												<span class="w60">형식중분류 : ${item.TYPEDIVMNM}</span></br>
											</c:if>
											<c:if test="${!empty item.MEAN_DIV_L_CD}">
												<span>의미대분류코드 : ${item.MEAN_DIV_L_CD}</span>
											</c:if>
											<c:if test="${!empty item.MEANDIVLNM}">
												<span class="w60">의미대분류 : ${item.MEANDIVLNM}</span></br>
											</c:if>
											<c:if test="${!empty item.MEAN_DIV_M_CD}">
												<span>의미중분류코드 : ${item.MEAN_DIV_M_CD}</span>
											</c:if>
											<c:if test="${!empty item.MEANDIVMNM}">
												<span class="w60">의미중분류 : ${item.MEANDIVMNM}</span></br>
											</c:if>
											<c:if test="${!empty item.ANNC_NATN}">
												<span>공표국가 : ${item.ANNC_NATN}</span>
											</c:if>
											
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_public_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_public_licensorList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_public_licensorList.add(exMap);
                            }
                            request.setAttribute("dg_public_licensorList", dg_public_licensorList);
					        			%>
					        			<c:forEach var="item" items="${dg_public_licensorList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 뉴스 저작권자</th>
										<td class="liceSrch">
											<b>${item.LICENSOR_NAME_KOR}</b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권자역할 : ${item.LICENSORROLE}</span></br>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_scenario" )){
                         
                         ArrayList<Map<String, Object>> dg_scenarioList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_scenarioList.add(exMap);
                            }
                            request.setAttribute("dg_scenarioList", dg_scenarioList);
					        			%>
					        			  <c:forEach var="item" items="${dg_scenarioList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 방송대본저작물</th>
										<td class="liceSrch">
											<b>${item.TITLE} </b>
											<p class="mt15">
											 <c:if test="${!empty item.TITLE}">
												<span>저작물명 : ${item.TITLE}</span>
											 </c:if>
											 <c:if test="${!empty item.WRITER}">
												<span class="w60">작가명 : ${item.WRITER}</span></br>
											 </c:if>
											 <c:if test="${!empty item.GENRE_KIND}">
												<span>장르구분 : ${item.GENRE_KIND}</span>
											 </c:if>
											 <c:if test="${!empty item.SUBJ_KIND}">
												<span class="w60">소재분류 : ${item.SUBJ_KIND}</span></br>
											 </c:if>
											 <c:if test="${!empty item.DIRECT}">
												<span>연출자 : ${item.DIRECT}</span>
											 </c:if>
											 <c:if test="${!empty item.ORIG_WORK}">
												<span class="w60">원작명 : ${item.ORIG_WORK}</span></br>
												</c:if>
												<c:if test="${!empty item.ORIG_WRITER}">
												<span>원작자명 : ${item.ORIG_WRITER}</span>
												</c:if>
												<c:if test="${!empty item.PLAYERS}">
												<span class="w60">주요출연진 : ${item.PLAYERS}</span></br>
												</c:if>
												<c:if test="${!empty item.BROAD_ORD}">
												<span>방송회차 : ${item.BROAD_ORD}</span>
												</c:if>
												<c:if test="${!empty item.BROAD_DATE}">
												<span class="w60">방송일자 : ${item.BROAD_DATE}</span></br>
												</c:if>
												<c:if test="${!empty item.BROAD_MEDINM}">
												<span>방송매체 : ${item.BROAD_MEDINM}</span>
												</c:if>
												<c:if test="${!empty item.BROAD_STATNM}">
												<span class="w60">방송사 : ${item.BROAD_STATNM}</span></br>
												</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_scenario_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_scenario_licensorList = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_scenario_licensorList.add(exMap);
                            }
                            request.setAttribute("dg_scenario_licensorList", dg_scenario_licensorList);
					        			%>
					        			 <c:forEach var="item" items="${dg_scenario_licensorList}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop">  방송대본 저작권자</th>
										<td class="liceSrch">
											<b>${item.LICENSOR_NAME_KOR}</b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권자역할 : ${item.LICENSORROLE}</span></br>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        			
					        		
					        	}  else if(documentSet2.get( "Id" ).equals( "dg_scenario" )){
                         
                         ArrayList<Map<String, Object>> dg_scenarioList2 = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_scenarioList2.add(exMap);
                            }
                            request.setAttribute("dg_scenarioList2", dg_scenarioList2);
					        			%>
					        			<c:forEach var="item" items="${dg_scenarioList2}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop"> 방송대본저작물</th>
										<td class="liceSrch">
											<b>${item.TITLE} </b>
											<p class="mt15">
											<c:if test="${!empty item.TITLE}">
												<span>저작물명 : ${item.TITLE}</span>
												</c:if>
												<c:if test="${!empty item.WRITER}">
												<span class="w60">작가명 : ${item.WRITER}</span></br>
												</c:if>
												<c:if test="${!empty item.GENRE_KIND}">
												<span>장르구분 : ${item.GENRE_KIND}</span>
												</c:if>
												<c:if test="${!empty item.SUBJ_KIND}">
												<span class="w60">소재분류 : ${item.SUBJ_KIND}</span></br>
												</c:if>
												<c:if test="${!empty item.DIRECT}">
												<span>연출자 : ${item.DIRECT}</span>
												</c:if>
												<c:if test="${!empty item.ORIG_WORK}">
												<span class="w60">원작명 : ${item.ORIG_WORK}</span></br>
												</c:if>
												<c:if test="${!empty item.ORIG_WRITER}">
												<span>원작자명 : ${item.ORIG_WRITER}</span>
												</c:if>
												<c:if test="${!empty item.PLAYERS}">
												<span class="w60">주요출연진 : ${item.PLAYERS}</span></br>
												</c:if>
												<c:if test="${!empty item.BROAD_ORD}">
												<span>방송회차 : ${item.BROAD_ORD}</span>
												</c:if>
												<c:if test="${!empty item.BROAD_DATE}">
												<span class="w60">방송일자 : ${item.BROAD_DATE}</span></br>
												</c:if>
												<c:if test="${!empty item.BROAD_MEDINM}">
												<span>방송매체 : ${item.BROAD_MEDINM}</span>
												</c:if>
												<c:if test="${!empty item.BROAD_STATNM}">
												<span class="w60">방송사 : ${item.BROAD_STATNM}</span></br>
												</c:if>
												
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}else if(documentSet2.get( "Id" ).equals( "dg_scenario_licensor" )){
                         
                         ArrayList<Map<String, Object>> dg_scenario_licensorList2 = new ArrayList<Map<String, Object>>(); 
                         documentSet_dg_album = (Map)documentSet2.get( "DocumentSet" );
                         document= (ArrayList)documentSet_dg_album.get("Document");
                         documentSize= document.size();
                         for(x = 0; x < documentSize ; x++){
                             Map<String, Object> exMap = new HashMap<String, Object>(); 
                              obj = (Map)document.get( x );
                            /*  objMap = (Map)obj.get( "Field" ); */
                              exMap = (Map)obj.get("Field");
                              dg_scenario_licensorList2.add(exMap);
                            }
                            request.setAttribute("dg_scenario_licensorList2", dg_scenario_licensorList2);
					        			%>
					        			<c:forEach var="item" items="${dg_scenario_licensorList2}" varStatus="status">
					        			<tr>
										<th scope="row" class="vtop">  방송대본 저작권자</th>
										<td class="liceSrch">
											<b>${item.LICENSOR_NAME_KOR}</b>
											<p class="mt15">
											<c:if test="${!empty item.LICENSOR_NAME_KOR}">
												<span>저작권자명 : ${item.LICENSOR_NAME_KOR}</span>
											</c:if>
											<c:if test="${!empty item.LICENSORROLE}">
												<span class="w60">저작권자역할 : ${item.LICENSORROLE}</span></br>
											</c:if>
											</p>
										</td>
										</tr>
										</c:forEach>
					        			<%
					        		
					        	}  
					          
					        }
							%>

						
							</tbody>
						</table>
						<!-- 디지털 저작권 거래소 연동 데이터  -->
						<div id="commPagination" class="pagination">
							<ul>
								<li>
							<!-- <span class="direction bgNone"><span class="prev mr10" style="margin-top:3px;">123123</span></span> -->
								</li>
							</ul>					
						</div>
						 
						</div><!-- End sub_contents_con -->
			<script type="text/javascript" language="javascript" src="/js/jquery-1.8.3.min.js"></script>
						<script type="text/javascript">
						  var listCount = 10;
						  var totalCount = <%=documentSet_dg_album.get("TotalCount")%>;
						  var totalPage = totalCount / listCount;
						  if(<%=request.getAttribute("startCount")%>==0){
							  var currentPageNum = 1;
						  }else{
							  var currentPageNum = <%=request.getAttribute("startCount")%>;
						  }
						  
						  var pageStr='<%=documentCntMap.get("Id")%>';
						  
						  $(function(){
							 if(currentPageNum!=1){
								 $("#commPagination").append('<span class="direction bgNone"><span class="prev mr10" style="margin-top:3px;" onclick="javascript:prevPageCall()"></span></span>');
								 
							 }
							  for(var i = 1 ; i < totalPage ; i++){
								  
								  if(Number(Math.ceil(currentPageNum*0.1)*10) + 1 > i && i >= Number(Math.ceil(currentPageNum*0.1)*10) + 1-10){
									if(currentPageNum == i){
										$("#commPagination").append("<strong><a href='javascript:changePage("+i+",\""+pageStr+"\")' >"+i+"</a></strong>");
									}else{
										$("#commPagination").append("<a href='javascript:changePage("+i+",\""+pageStr+"\")' >"+i+"</a>");
									}
										 
								  }  
							  }
							  $("#commPagination").append('<span class="direction bgNone"><span class="next ml10" style="margin-top:3px;" onclick="javascript:nextPageCall()"></span></span>');
							  //console.log($("#commPagination").html());
						  })
						 
						 function prevPageCall(){
							 currentPageNum--;
							 changePage(currentPageNum,pageStr); 
						 }
						  
						 function nextPageCall(){
							 currentPageNum++;
							 changePage(currentPageNum,pageStr); 
						 }
						  
						  function changePage(pageNum,callName){
							//console.log("callName : " + callName );
							var pageNum =  pageNum;
							var frm = document.search;
							
							frm.collection.value = callName;
							frm.startCount.value = pageNum*10;
							
							
							 	
							frm.submit();
						  }
						  
						  
						  function changeGenreCd(callName){
							  //sconsole.log("call");
							  //console.log(callName);
							  var frm = document.search;
							  
							  switch (callName) {
							 	 case "dg_album"    : 
									  frm.genreCd.value = "앨범" ;
								      break;
								  case "dg_art"    : 
									  frm.genreCd.value = "미술저작물" ;
								      break;
								  case "dg_art_licensor"   : 
									  frm.genreCd.value = "미술저작권자" ;
								      break;
								  case "dg_book"  : 
									  frm.genreCd.value = "도서" ;
								      break;
								  case "dg_broadcast"  : 
									  frm.genreCd.value = "방송저작물" ;
								      break;
								  case "dg_broadcast_licensor"  : 
									  frm.genreCd.value = "방송저작권자" ;
								      break;
								  case "dg_character"  : 
									  frm.genreCd.value = "캐릭터저작물" ;
								      break;    
								  case "dg_character_licensor"  : 
									  frm.genreCd.value = "캐릭터저작권자" ;
								      break;
								  case "dg_content"  : 
									  frm.genreCd.value = "방송컨텐츠저작물" ;
								      break;
								  case "dg_content_licensor"  : 
									  frm.genreCd.value = "방송컨텐츠저작권자" ;
								      break;
								  case "dg_literature"  : 
									  frm.genreCd.value = "어문저작물" ;
								      break;
								  case "dg_literature_licensor"  : 
									  frm.genreCd.value = "어문저작권자" ;
								      break;
								 case "dg_movie_licensor"  : 
										  frm.genreCd.value = "영화저작권자" ;
									      break;
								  case "dg_movie"  : 
									  frm.genreCd.value = "영화저작물" ;
								      break;
								  case "dg_music_licensor"  : 
									  frm.genreCd.value = "음악저작권자" ;
								      break;
								  case "dg_music1"  : 
									  frm.genreCd.value = "음악저작물" ;
								      break;
								  case "dg_news"  : 
									  frm.genreCd.value = "뉴스저작물" ;
								      break;
								  case "dg_news_licensor"  : 
									  frm.genreCd.value = "뉴스저작권자" ;
								      break;
								  case "dg_public"  : 
									  frm.genreCd.value = "공공저작물" ;
								      break;
								  case "dg_public_licensor"  : 
									  frm.genreCd.value = "공공저작권자" ;
								      break;
								  case "dg_scenario"  : 
									  frm.genreCd.value = "방송대본저작물" ;
								      break;
								  case "dg_scenario_licensor"  : 
									  frm.genreCd.value = "방송대본저작권자" ;
								      break;
								  case "ALL"  :
									  frm.collection.value = "dg_music1,dg_literature,dg_movie,dg_news";
									  frm.genreCd.value = "전체" ;
								      break;    
								  default    : 
									  frm.collection.value = "dg_music1,dg_literature,dg_movie,dg_news";
								  	  //console.log(frm.collection.value)
								  	  frm.genreCd.value = "전체" ;
							      	  break;
								}  //*/
						  }
						</script>
						
						
						
					</div><!-- End con_rt -->
					<p class="clear"></p>
				</div><!-- End contents -->

				<!-- //주요컨텐츠 end -->
	    		<!-- //content -->
	    		
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2017/footer.jsp" />
				<!-- FOOTER end -->
		
	    	
	    		    	
	<!-- //전체를 감싸는 DIVISION -->

</body>
</html>