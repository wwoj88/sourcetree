<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.json.simple.JSONObject"%>
<%-- <%@page import="org.codehaus.jackson.JsonParser"%> --%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@ page language="java" contentType="text/html; charset=euc-kr"
    pageEncoding="euc-kr"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>저작권 찾기 서비스란? | 저작권 찾기 소개 | 저작권자찾기</title>
</head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">

<!-- <script src="/js/jquery-1.7.js"  type="text/javascript"></script> -->
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 

//<!--

/* 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-69621660-2', 'auto');
 ga('send', 'pageview');
  */
 function crosPop(){
		window.open("https://cras.copyright.or.kr/front/right/comm/main_.do");
	}

 
 function statBoRegiPop(){
		window.open("/statBord/statBo06Regi.do", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes");	
	}
 
 function goSearch(){
		
	    var fnm = document.search;
	    var query=fnm.query.value;
	    query=query.replace(/^\s+/,""); 

	    if (query == "" || query == null) {
	        alert("검색어를 입력하세요.");
	        return;
	    }

	    rangeValue="";
	    for(i=0; i<fnm.range.length; i++){    	
	    	if(fnm.range[i].checked){    		
	    		if(rangeValue!="") rangeValue = rangeValue+",";
	    		rangeValue = rangeValue+fnm.range[i].value;    		    		
	    	}
	    }
	    var target = document.getElementById("collection");
	    fnm.genreCd.value = target.options[target.selectedIndex].text;  
	    fnm.arr_range.value = rangeValue;    

	    fnm.submit();
	    return false;
}
 
function goCategory(Str){
	
}
  
  
function comma(num){
    var len, point, str; 
       
    num = num + ""; 
    point = num.length % 3 ;
    len = num.length; 
    str = num.substring(0, point); 
    while (point < len) { 
        if (str != "") str += ","; 
        str += num.substring(point, point + 3); 
        point += 3; 
    } 
    return str;
}


</script>

</head>

<body>
		
		<!-- HEADER str-->
		<jsp:include page="/include/2017/header.jsp" />
		
<script type="text/javascript"> 
$(function(){
	$('#subCnt').html(comma($.trim($('#subCnt').html())));
	
	var genreCd ='${genreCd}';
	var menuFlag = '${menuFlag}';
	var collectionName = '${collectionName}';
	$('#collection option[value='+collectionName+']').attr('selected','selected'); //*/

	var flag = false;
	
	for(var i = 0 ; i < 12 ; i++){
		
		if($("#topMenu"+i).html()==genreCd){
			$("#topMenu"+i).parent().parent().attr("class","first on");
			flag = true;
		}
	}
	if(flag==false){
		$("#topMenu0").parent().parent().attr("class","first on");
	}
	if($('.con_rt_hd_title').html()!=genreCd){
		if(genreCd=='total'){
			if(menuFlag=='N'){
				$('.con_rt_hd_title').html('상당한 노력 신청 서비스');
			}else{
				$('.con_rt_hd_title').html('전체');
			}
			
		}else{
			$('.con_rt_hd_title').html(genreCd);
		}
	}
	
})	

</script>
<!-- 		<script type="text/javascript">initNavigation(1);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- content st -->
			<div id="contents">
			${menuFlag}
			 <c:if test="${menuFlag!='N'}">
            <div class="con_lf">
			<div class="con_lf_big_title">분야별 <br>권리자 찾기</div>
				<ul class="sub_lf_menu">
				<li><a onclick="javascript:goInnerSearch('999');">전체</a></li>
				<li><a onclick="javascript:goInnerSearch('1');">음악</a></li>
				<li><a onclick="javascript:goInnerSearch('2');">어문</a></li>
				<li><a onclick="javascript:goInnerSearch('3');">방송대본</a></li>
				<li><a onclick="javascript:goInnerSearch('4');">영화</a></li>
				<li><a onclick="javascript:goInnerSearch('5');">방송</a></li>
				<li><a onclick="javascript:goInnerSearch('6');">뉴스</a></li>
				<li><a onclick="javascript:goInnerSearch('7');">미술</a></li>
				<li><a onclick="javascript:goInnerSearch('8');">이미지</a></li>
				<li><a onclick="javascript:goInnerSearch('9');">사진</a></li>
				<li><a onclick="javascript:goInnerSearch('99');">기타</a></li>
				</ul>
			</div>
			</c:if>
			<c:if test="${menuFlag eq 'N'}">
			<div class="con_lf">
			<div class="con_lf_big_title">법정허락<br>승인 신청</div>
				<ul class="sub_lf_menu">
				<li><a href="javascript:goInnerSearch2('99');">상당한 노력 신청 서비스</a></li>
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
				<li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li>
				<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
				<li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
				<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
				<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
				<li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li>
				</ul>
			</div>
			</c:if>
					<div class="con_rt">
						<div class="con_rt_head">
							<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
							&gt;
							저작권자 찾기
							&gt;
							<c:if test="${genreCd=='total'}"><span class="bold">전체</span></c:if>	
							<c:if test="${genreCd!='total'}"><span class="bold">${genreCd}</span></c:if>	
						</div>
						<div class="con_rt_hd_title">저작권자 찾기</div>
					
					
						<div id="sub_contents_con">
						<form id="search" name="search" action="/search/search.do" method="post">
									<div class="bg_f8f8f8">
									
									<!-- 2014.11.06 추가 -->
									<div>
										<div class="float_lf">
											<span class="sub_blue_point_bg">검색대상</span>&nbsp;&nbsp;&nbsp;
												  <input type="checkbox" value="all" name="range" rel="1" id="chkall" onclick="check(1);" ><label for="chkall" class="thin p12">전체</label>
												  <input type="checkbox" value="title" name="range" rel="2" id="chktl" onclick="check(2);" ><label for="chktl"  class="thin p12">저작물명</label>
												  <input type="checkbox" value="licer_detail" name="range" rel="2" id="chktl" onclick="check(2);"><label for="chktl"  class="thin p12">저작권자</label></td>
											</div>
											<div class="float_rt">
											<span class="sub_blue_point_bg">정렬방식</span>&nbsp;&nbsp;&nbsp;
												<select name="sort" id="line">
															<option value="RANK">정확도순</option>
															<option value="DATE">날짜순</option>
												</select>
												<select name="sortOrder" id="">
															<option value="DESC">내림차순</option>
															<option value="ASC">오름차순</option>
												</select>
											</div> 
									<p class="clear"></p>
									</div>
									
										<div class="mar_tp10">
										<!-- <select name="target" id="srchDivs" title="검색구분">
											<option>통합검색</option>
											<option>저작권 등록부</option>
											<option>위탁관리 저작물</option>
										</select> -->
										<select id="collection" name="collection" title="장르">
											<option value="ALL">전체</option>
											<option value="cf_music">음악</option>
											<option value="cf_literature">어문</option>
											<option value="cf_scenario">방송대본</option>
											<option value="cf_movie">영화</option>
											<option value="cf_broadcast">방송</option>
											<option value="cf_news">뉴스</option>
											<option value="cf_art">미술</option>
											<option value="cf_image">이미지</option>
											<option value="cf_photo">사진</option>
											<option value="cf_etc">기타</option>
										</select>
										<input type="text" title="검색어" id="query" name="query" size="30" value="${query}" style="IME-MODE: active;padding:3px;width:48%;"/>
										<input type="button" onclick="goSearch();" value="검색" style="background-color: #2c65aa; color: white;">&nbsp;&nbsp;&nbsp;
										<input type="checkbox" id="chk1" name="resrch_check" class="checkbox" onclick="javascript:resrch_chk();"/>
										<label for="chk1" class="p11 strong black">결과 내 검색</label>
										</div>
									</div>
							<input type="hidden" name="resultcount" value="10"/>
							<input type="hidden" name="page" value=""/>
							<input type="hidden" name="mode" value=""/>
							<input type="hidden" name="arr_range" value="">
							<input type="hidden" name="resrch" value=""/>							
							<input type="hidden" id="urlCd" name="urlCd" value="01"/>
							<input type="hidden" name = "startCount" value="">
							<input type="hidden" name = "subPageStr" value="Y">
							<input type="hidden" name = "genreCd" value="${genreCd}">
							
 						</form>
						
<!-- 						<div class="sub01_con_bg4_tp mar_tp30"></div>이용승인 신청 공고
 						<div class="sub01_con_bg4">
							<h3>법정허락 승인 신청</h3>
							<p class="word_dian_bg mar_tp5">이미 상당한 노력이 이행되어 법정허락 승인 신청이 가능한 저작물로, 법정허락 승인 신청정보 작성화면으로 이동합니다.</p>
							<h3 class="mar_tp10">저작권 등록 정보 조회</h3>
							<p class="word_dian_bg mar_tp5">검색한 저작물의 정보 조회를 위해서 저작권 등록 시스템 통합 검색으로 이동합니다.</p>
							<h3 class="mar_tp10">저작권자 찾기위한  상당한노력 신청</h3>
							<p class="word_dian_bg mar_tp5">저작권 찾기 정보시스템에서 한 번의 신청으로 상당한 노력 이행 신청과 함께 저작권자 조회공고가 자동으로 처리됩니다.</p>
						</div>
						<div class="subcetcol">
							<p class="cetcol1">검색 결과에 찾으시는 저작물이 없는 경우 클릭하세요</p>
							<p class="cetcol2"><a href="#" onclick="statBoRegiPop();"><img src="/images/sub/btn_3.jpg" alt="검색" /></a></p>
							<p class="clear"></p>
						</div> -->
						<ul class="tab_menuBg mt20">
							<li><strong><a id="topMenu0" href="javascript:goInnerSearch2('999');">전체</a> </strong></li>
							<!-- subPageStr=yes 서브페이지 개발용 변수 -->
							<li><strong><a id="topMenu1" href="javascript:goInnerSearch2('1');">음악</a> </strong></li>
							<li><strong><a id="topMenu2" href="javascript:goInnerSearch2('2');">어문</a> </strong></li>
							<li><strong><a id="topMenu3" href="javascript:goInnerSearch2('3');">방송대본</a> </strong></li>
							<li><strong><a id="topMenu4" href="javascript:goInnerSearch2('4');">영화</a> </strong></li>
							<li><strong><a id="topMenu5" href="javascript:goInnerSearch2('5');">방송</a> </strong></li>
							<li><strong><a id="topMenu6" href="javascript:goInnerSearch2('6');">뉴스</a> </strong></li>
							<li><strong><a id="topMenu7" href="javascript:goInnerSearch2('7');">미술</a> </strong></li>
							<li><strong><a id="topMenu8" href="javascript:goInnerSearch2('8');">이미지</a> </strong></li>
							<li><strong><a id="topMenu9" href="javascript:goInnerSearch2('9');">사진</a> </strong></li>
							<li><strong><a id="topMenu10" href="javascript:goInnerSearch2('99');">기타</a> </strong></li>
						</ul>
						<%
						/* request.getAttribute("searchResult") */
						String  jsonData = (String) request.getAttribute("searchResult");// 검색엔진 전체 json데이터 
						JSONParser jsonParser = new JSONParser();// json 파서
						
						JSONObject jsonObject2 = (JSONObject)jsonParser.parse( jsonData );//json 데이터 파싱 시작
						
						Map jsonMap = (Map) jsonObject2.get( "SearchQueryResult" ); 
						 JSONArray collection = (JSONArray) jsonMap.get("Collection");  
						 Map documentSet = (Map) collection.get( 0 );
						Map documentSetMap = (Map) documentSet.get("DocumentSet");
						JSONArray documentJA = (JSONArray) documentSetMap.get("Document");
						
						//out.print("collection : " + collection.get(0));
						 
						JSONObject jsonCntObject = (JSONObject)request.getAttribute("SearchColCount");//json 데이터 파싱 시작
						
						Map jsonCntMap = (Map)jsonCntObject.get("SearchColCount");
						
						Iterator<String> keys = jsonCntMap.keySet().iterator();
					//	out.print("jsonCntMap : " + jsonCntMap);
						int subCnt = 0;
						int totalCnt = 0;
						
						while( keys.hasNext() ){
				            String key = keys.next();
				            int temp = (Integer) jsonCntMap.get(key);
				            if(!key.equals( "cf_reg" ) && !key.equals( "reg_copyright") && temp!=-1 && temp !=-972 && !key.substring( 0,2 ).equals( "dg" ) ){
				            	subCnt += temp;
				            }
				            if(!key.equals( "reg_copyright") && temp!=-1 && temp !=-972){
				            	totalCnt += temp;
				            }
				        }
						%>
						<%-- <%= jsonData %> --%>
						<div class="bg_2e75b6 mar_tp10">  
									<h2 class="float_lf color_fff blod">위탁관리 저작물(
									<font id="subCnt">
									<%
									//documentSetMap.get("TotalCount")
									Map tempMap= (Map)jsonCntMap;
									out.println(subCnt);
									%>
									</font>
									건)</h2>
									
						<p class="clear"></p>
						</div>
						<table class="sub_tab1" cellspacing="0" cellpadding="0" width="100%" summary="저작권찾기 신청정보입니다." class="grid">
							<colgroup>
								<col width="10%">
								<col>
								<col>
							</colgroup>
							<tbody>	
							<% 
							//out.print("collection.size() : " + collection.size());
							
							if(collection.size() > 0){
								int objCont = 0 ;
								for( int i = 0 ; i < collection.size() ; i++){
									/* documentJA 결과값 리스트 */
									if(objCont>0){break;}
									Map obj = (Map) collection.get(i);
							
									String id=(String) obj.get("id");
							
									%>
									<%-- <%=obj%> --%>
									<%
									
									if(obj.get("Id").equals( "cf_music" )){
									
									  ArrayList<Map<String, Object>> cf_musicList = new ArrayList<Map<String, Object>>();
										documentSet = obj;
										documentSetMap = (Map) documentSet.get("DocumentSet");
										documentJA = (JSONArray) documentSetMap.get("Document");
										//out.print("documentSetMap : " + documentSetMap);
										
										for( int e = 0 ; e < documentJA.size() ; e++){
										  Map<String, Object> exMap = new HashMap<String, Object>(); 
											Map objCon=(Map) documentJA.get( e );
									  	/* Map objField = (Map) objCon.get("Field"); */
									  	exMap = (Map)objCon.get("Field");
									  	cf_musicList.add(exMap);
										
										}
									  request.setAttribute("cf_musicList", cf_musicList);
									%>
									<c:forEach var="item" items="${cf_musicList}" varStatus="status">
									<tr>
										<th scope="row" class="vtop">음악</th>
										<td class="liceSrch">
										
										<%-- <td><%=documentJA.get( e ) %></td> --%>
											<b>${item.WORKS_TITLE}</b>
											<p class="mt15">
											<c:if test="${!empty item.ALBUM_TITLE}">
												<span class="w60">앨범명 : ${item.ALBUM_TITLE}</span>
										  </c:if>
										  <c:if test="${!empty item.DATE}">
												<span>앨범발매년도 : ${item.DATE}</span></br>
											</c:if>
											<c:if test="${!empty item.SING}">
												<span class="w60">가수 : ${item.SING}</span>
											</c:if>
											<c:if test="${!empty item.PERF}">
												<span>연주가 : ${item.PERF}</span></br>
											</c:if>
											<c:if test="${!empty item.LYRC}">
												<span class="w60">작사가 : ${item.LYRC}</span>
											</c:if>
											<c:if test="${!empty item.COMP}">
												<span class="w60">작곡가 : ${item.COMP}</span></br>
											</c:if>
											<c:if test="${!empty item.ARRA}">
												<span>편곡가 : ${item.ARRA}</span>
											</c:if>
											<c:if test="${!empty item.TRAN}">
												<span class="w60">역사가 : ${item.TRAN}</span></br>
											</c:if>
											<c:if test="${!empty item.PROD}">
												<span class="w60">음반제작자 : ${item.PROD}</span>
											</c:if>
											<c:if test="${!empty item.RGST_ORGN_NAME}">
												<span>권리관리기관 : ${item.RGST_ORGN_NAME}</span></br>
											</c:if>
											</p>
										</td>
									</tr>
									</c:forEach>
							
							<%
										
							}else if(obj.get("Id").equals( "cf_literature" )){
							
                   ArrayList<Map<String, Object>> cf_literatureList = new ArrayList<Map<String, Object>>();
                   documentSet = obj;
                   documentSetMap = (Map) documentSet.get("DocumentSet");
                   documentJA = (JSONArray) documentSetMap.get("Document");
                   //out.print("documentSetMap : " + documentSetMap);
                   
                   for( int e = 0 ; e < documentJA.size() ; e++){
                     Map<String, Object> exMap = new HashMap<String, Object>(); 
                     Map objCon=(Map) documentJA.get( e );
                     /* Map objField = (Map) objCon.get("Field"); */
                     exMap = (Map)objCon.get("Field");
                     cf_literatureList.add(exMap);
                   
                   }
                   request.setAttribute("cf_literatureList", cf_literatureList);
       
							%>
							<c:forEach var="item" items="${cf_literatureList}" varStatus="status">
								<tr>
										<th scope="row" class="vtop">어문</th>
										<td class="liceSrch">
											<b>${item.WORKS_TITLE}</b>
											<p class="mt15">
											<c:if test="${!empty item.WORKS_SUB_TITLE}">
												<span class="w60">부제 : ${item.WORKS_SUB_TITLE}</span>
											</c:if>
											<c:if test="${!empty item.BOOK_ISSU_YEAR}">
												<span>창작년도 : ${item.BOOK_ISSU_YEAR}</span></br>
										  </c:if>
										  <c:if test="${!empty item.BOOK_TITLE}">
												<span class="w60">도서명 : ${item.BOOK_TITLE}</span>
										  </c:if>
										  <c:if test="${!empty item.BOOK_PUBLISHER}">
												<span>출판사 : ${item.BOOK_PUBLISHER}</span></br>
											</c:if>
											<c:if test="${!empty item.BOOK_ISSU_YEAR}">
												<span class="w60">발행년도 : ${item.BOOK_ISSU_YEAR}</span>
											</c:if>
											<c:if test="${!empty item.COPT_NAME}">
												<span class="w90">저작자 :  ${item.COPT_NAME}</span></br>
											</c:if>
											<c:if test="${!empty item.RGST_ORGN_NAME}">
												<span>권리관리기관 : ${item.RGST_ORGN_NAME}</span>
											</c:if>
											</p>
										</td>
									</tr>
								</c:forEach>
							<%
								
							}else if(obj.get("Id").equals( "cf_scenario" )){ 
						
							     ArrayList<Map<String, Object>> cf_scenarioList = new ArrayList<Map<String, Object>>();
                   documentSet = obj;
                   documentSetMap = (Map) documentSet.get("DocumentSet");
                   documentJA = (JSONArray) documentSetMap.get("Document");
                   //out.print("documentSetMap : " + documentSetMap);
                   
                   for( int e = 0 ; e < documentJA.size() ; e++){
                     Map<String, Object> exMap = new HashMap<String, Object>(); 
                     Map objCon=(Map) documentJA.get( e );
                     /* Map objField = (Map) objCon.get("Field"); */
                     exMap = (Map)objCon.get("Field");
                     cf_scenarioList.add(exMap);
                   
                   }
                   request.setAttribute("cf_scenarioList", cf_scenarioList);
							%>
							 <c:forEach var="item" items="${cf_scenarioList}" varStatus="status">
								<tr>
										<th scope="row" class="vtop">시나리오</th>
										<td class="liceSrch">
											<b>${item.WORKS_TITLE}</b>
											<p class="mt15">
											<c:if test="${!empty item.WORKS_TITLE}">
												<span class="w60">작품명(대제목) : ${item.WORKS_TITLE}</span>
											</c:if>
											<c:if test="${!empty item.SCRT_GENRE_CD}">
												<span>장르/소재 : ${item.SCRT_GENRE_CD}/${item.SCRP_SUBJ_CD}</span>
											</c:if>
											<c:if test="${!empty item.WORKS_ORIG_TITLE}">
												<span class="w60">원작명  : ${item.WORKS_ORIG_TITLE}</span>
											</c:if>
											<c:if test="${!empty item.WRITER}">
												<span>원작작가 : ${item.WRITER}</span>
											</c:if>
											<c:if test="${!empty item.BORD_DATE}">
												<span class="w60">방송일자/회차  : ${item.BORD_DATE}/${item.BROD_ORD_SEQ}</span>
										  </c:if>
										  <c:if test="${!empty item.PLAYER}">
												<span class="w60">주요출연진 : ${item.PLAYER}</span>
											</c:if>
											<c:if test="${!empty item.MAKE_CPY}">
												<span class="w90">제작사 :  ${item.MAKE_CPY}</span>
											</c:if>
											<c:if test="${!empty item.WRITER}">
												<span>작가(저자) : ${item.WRITER}</span>
											</c:if>
											<c:if test="${!empty item.DIRECTOR}">
												<span class="w90">연출자 :  ${item.DIRECTOR}</span>
											</c:if>
											<c:if test="${!empty item.RGST_ORGN_NAME}">
												<span class="w90">권리관리기관 :  ${item.RGST_ORGN_NAME}</span>
											</c:if>
											
											</p>
										</td>
									</tr>
								</c:forEach>
							<%
								
							}else if(obj.get("Id").equals( "cf_movie" )){ 
				            
                   ArrayList<Map<String, Object>> cf_movieList = new ArrayList<Map<String, Object>>();
                   documentSet = obj;
                   documentSetMap = (Map) documentSet.get("DocumentSet");
                   documentJA = (JSONArray) documentSetMap.get("Document");
                   //out.print("documentSetMap : " + documentSetMap);
                   
                   for( int e = 0 ; e < documentJA.size() ; e++){
                     Map<String, Object> exMap = new HashMap<String, Object>(); 
                     Map objCon=(Map) documentJA.get( e );
                     /* Map objField = (Map) objCon.get("Field"); */
                     exMap = (Map)objCon.get("Field");
                     cf_movieList.add(exMap);
                   
                   }
                   request.setAttribute("cf_movieList", cf_movieList);
							%>
							 <c:forEach var="item" items="${cf_movieList}" varStatus="status">
								<tr>
										<th scope="row" class="vtop">영화</th>
										<td class="liceSrch">
											<b>${item.WORKS_TITLE}</b>
											<p class="mt15">
											<c:if test="${!empty item.MAKE_YEAR}">
												<span class="w60">제작년도 : ${item.MAKE_YEAR}</span>
											</c:if>
											<c:if test="${!empty item.PLAYER}">
												<span>출연자 : ${item.PLAYER}</span></br>
											</c:if>
											<c:if test="${!empty item.DIRECT}">
												<span class="w60">감독 : ${item.DIRECT}</span>
											</c:if>
											<c:if test="${!empty item.WRITER}">
												<span>작가 : ${item.WRITER}</span></br>
											</c:if>
											<c:if test="${!empty item.DIRECTOR}">
												<span class="w60">연출자 : ${item.DIRECTOR}</span>
											</c:if>
											<c:if test="${!empty item.PRODUCER}">
												<span>제작자 :  ${item.PRODUCER}</span></br>
											</c:if>
											<c:if test="${!empty item.DISTRIBUTOR}">
												<span class="w60">배급사 :  ${item.DISTRIBUTOR}</span></br>
											</c:if>
										<%-- 	<c:if test="${!empty item.RGST_ORGN_NAME}">
												<span class="w95">투자자 :  ${item.WORKS_TITLE}<%=objField.get("투자사")%></span>
											</c:if> --%>
											<c:if test="${!empty item.RGST_ORGN_NAME}">
												<span>권리관리기관 : ${item.RGST_ORGN_NAME}</span></br>
											</c:if>
											</p>
										</td>
									</tr>
								</c:forEach>
							<%
									
								}else if(obj.get("Id").equals( "cf_broadcast" )){ 
								String[] genreArr = {"방송장르","드라마","예능","교양","뉴스","라디오","뮤직","영화","기타"};
						    
                ArrayList<Map<String, Object>> cf_broadcastList = new ArrayList<Map<String, Object>>();
                documentSet = obj;
                documentSetMap = (Map) documentSet.get("DocumentSet");
                documentJA = (JSONArray) documentSetMap.get("Document");
                //out.print("documentSetMap : " + documentSetMap);
                
                for( int e = 0 ; e < documentJA.size() ; e++){
                  Map<String, Object> exMap = new HashMap<String, Object>(); 
                  Map objCon=(Map) documentJA.get( e );
                  /* Map objField = (Map) objCon.get("Field"); */
                  exMap = (Map)objCon.get("Field");
                  cf_broadcastList.add(exMap);
                
                }
                request.setAttribute("cf_broadcastList", cf_broadcastList);
							%>
							 <c:forEach var="item" items="${cf_broadcastList}" varStatus="status">
                <tr>
                    <td scope="row" class="vtop" style="border-left: none;">방송</td>
                    <td class="liceSrch">
                      <b><c:out value="${item.WORKS_TITLE}" /></b>
                      <p class="mt15">
                        <c:choose>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '99'}">
                           <span class="w60">장르  : 기타</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '0'}">
                           <span class="w60">장르  : 방송장르</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '1'}">
                           <span class="w60">장르  : 드라마</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '2'}">
                           <span class="w60">장르  : 예능</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '3'}">
                           <span class="w60">장르  : 교양</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '4'}">
                           <span class="w60">장르  : 뉴스</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '5'}">
                           <span class="w60">장르  : 라디오</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '6'}">
                           <span class="w60">장르  : 영화</span>
                          </c:when>
                          <c:when test="${!empty item.BORD_GENRE_CD and item.BORD_GENRE_CD eq '7'}">
                           <span class="w60">장르  : 기타</span>
                          </c:when>
                        </c:choose>
                        <c:if test="${!empty item.MAKE_YEAR}">
                          <span>제작년도 : ${item.MAKE_YEAR }</span><br>
                        </c:if>
                        <c:if test="${!empty item.BROD_ORD_SEQ}">
                          <span class="w60">회차 :${item.BROD_ORD_SEQ }</span>
                        </c:if>
                        <c:if test="${!empty item.DIRECTOR}">
                          <span>연출자 : ${item.DIRECTOR }</span><br>
                        </c:if>
                        <c:if test="${!empty item.WRITER}">
                          <span class="w60">작가 : ${item.WRITER }</span>
                        </c:if>
                        <c:if test="${!empty item.RGST_ORGN_NAME}">
                          <span>권리관리기관 : ${item.RGST_ORGN_NAME } </span><br>
                        </c:if>
                        <c:if test="${!empty item.MAKE_CPY}">
                          <span>제작사 :  ${item.MAKE_CPY }</span>
                        </c:if>
                      </p>
                    </td>
                  </tr>
                 </c:forEach>
							<%
								
								}else if(obj.get("Id").equals( "cf_news" )){ 
		                  ArrayList<Map<String, Object>> cf_newsList = new ArrayList<Map<String, Object>>();
		                  documentSet = obj;
		                  documentSetMap = (Map) documentSet.get("DocumentSet");
		                  documentJA = (JSONArray) documentSetMap.get("Document");
		                  if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
		                  for( int e = 0 ; e < documentJA.size() ; e++){
		                    Map<String, Object> exMap = new HashMap<String, Object>();
		                    Map objCon=(Map) documentJA.get( e );
		                    /* Map objField = (Map) objCon.get("Field"); */
		                    exMap = (Map) objCon.get("Field");
		                    cf_newsList.add(exMap);
		                  }
		                  request.setAttribute("cf_newsList", cf_newsList);
		                %>
		                <c:forEach var="item" items="${cf_newsList}" varStatus="status">
		                  <tr>
		                      <td scope="row" class="vtop" style="border-left: none;">뉴스</td>
		                      <td class="liceSrch">
		                        <b><c:out value="${item.WORKS_TITLE}" /></b>
		                        <p class="mt15">
		                          <c:if test="${!empty item.ARTICL_DATE}">
		                            <span class="w60">기사일자 : ${item.WORKS_TITLE}</span>
		                          </c:if>
		                          <c:if test="${!empty item.PAPE_NO}">
		                           <span>지면번호/지면면종 : ${item.PAPE_NO}/${item.PAPE_KIND}</span><br>
		                          </c:if>
		                          <c:if test="${!empty item.CONTRIBUTOR}">
		                           <span class="w60">기고자 : ${item.CONTRIBUTOR}</span>
		                          </c:if>
		                          <c:if test="${!empty item.REPOTER}">
		                           <span>기자 : ${item.REPOTER}</span><br>
		                          </c:if>
		                          <c:if test="${!empty item.PROVIDER}">
		                           <span class="w60">언론사 : ${item.PROVIDER}</span>
		                          </c:if>
		                          <c:if test="${!empty item.RGST_ORGN_NAME}">
		                           <span>권리관리기관 :  ${item.RGST_ORGN_NAME}</span><br>
		                          </c:if>
		                        </p>
		                      </td>
		                    </tr>
		                  </c:forEach>
							<%
				      
	              }else if(obj.get("Id").equals( "cf_art" )){ 
	                ArrayList<Map<String, Object>> cf_artList = new ArrayList<Map<String, Object>>();
	                documentSet = obj;
	                documentSetMap = (Map) documentSet.get("DocumentSet");
	                documentJA = (JSONArray) documentSetMap.get("Document");
	                if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
	                for( int e = 0 ; e < documentJA.size() ; e++){
	                     Map<String, Object> exMap = new HashMap<String, Object>();                
	                     
	                  Map objCon=(Map) documentJA.get( e );
	                  Map objField = (Map) objCon.get("Field");
	                  exMap = (Map) objCon.get("Field");
	                  cf_artList.add(exMap);
	                }
	                  request.setAttribute("cf_artList", cf_artList);
	              %>
	              
	               <c:forEach var="item" items="${cf_artList}" varStatus="status">
	               <tr>
	                    <td scope="row" class="vtop" style="border-left: none;">미술</td>
	                    <td class="liceSrch">
	                      <b><c:out value="${item.WORKS_TITLE}" /></b>
	                      <p class="mt15">
	                      <c:if test="${!empty item.IMAGE_URL}"> <span class="w60">썸네일 : 
	                        <img class="thumb" src="${item.IMAGE_URL}${item.IMAGE_THUMBNAIL_NAME}" alt="${item.WRITER}작가 - ${item.WORKS_TITLE}(${item.WORKS_ID})">
	                      </span><br>
	                      </c:if>
	                      <c:if test="${!empty item.WORKS_ID}">
	                      <span class="w60">WORKS_ID : ${item.WORKS_ID} </span></br>
	                      </c:if>
	                      <c:if test="${!empty item.WORKS_SUB_TITLE and item.WORKS_SUB_TITLE ne '없음'}">
	                        <span class="w60">부제 :  ${item.WORKS_SUB_TITLE}</span></br>
	                      </c:if>
	                      <c:if test="${!empty item.GENRE_CD}">
	                        <span>분류 : ${item.GENRE_CD}</span><br>
	                      </c:if>
	                      <c:if test="${!empty item.MAKE_DATE and item.MAKE_DATE ne '없음'}">
	                        <span class="w60">저작년월일 : ${item.MAKE_DATE}</span>
	                      </c:if>
	                      <c:if test="${!empty item.SOURCE_INFO and item.SOURCE_INFO ne '없음'}">
	                        <span>출처 : ${item.SOURCE_INFO}</span><br>
	                      </c:if>
	                      <c:if test="${!empty item.MAIN_MTRL and item.MAIN_MTRL ne '없음'}">
	                        <span class="w60">주재료 : ${item.MAIN_MTRL}</span>
	                      </c:if>
	                      <c:if test="${!empty item.TXTR and item.TXTR ne '없음'}">
	                        <span>구조 및 특징 : ${item.MAIN_MTRL}</span><br>
	                      </c:if>
	                      <c:if test="${!empty item.WRITER}">
	                        <span class="w60">작가 :  ${item.WRITER}</span>
	                      </c:if>
	                      <c:if test="${!empty item.POSS_ORGN_NAME and item.POSS_ORGN_NAME ne '없음'}">
	                        <span >소장기관명 : ${item.POSS_ORGN_NAME}</span><br>
	                      </c:if>
	                      <c:if test="${!empty item.POSS_DATE and item.POSS_DATE ne '없음'}">
	                        <span class="w60">소장년월일 : ${item.POSS_DATE}</span>
	                      </c:if>
	                      <c:if test="${!empty item.RGST_ORGN_NAME and item.RGST_ORGN_NAME ne '없음'}">
	                        <span>권리관리기관 : ${item.RGST_ORGN_NAME}</span><br>
	                      </c:if>
	                      </p>
	                    </td>
	                  </tr>
	                  
	                  </c:forEach>
							<%

	              }else if(obj.get("Id").equals( "cf_image" )){ 
	                ArrayList<Map<String, Object>> cf_imageList = new ArrayList<Map<String, Object>>();
	                documentSet = obj;
	                documentSetMap = (Map) documentSet.get("DocumentSet");
	                documentJA = (JSONArray) documentSetMap.get("Document");
	                if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
	                for( int e = 0 ; e < documentJA.size() ; e++){
	                  Map<String, Object> exMap = new HashMap<String, Object>(); 
	                  Map objCon=(Map) documentJA.get( e );
	                  /* Map objField = (Map) objCon.get("Field"); */
	                  exMap = (Map) objCon.get("Field");
	                  cf_imageList.add(exMap);
	                }
	                  // out.print(cf_imageList);
	                  request.setAttribute("cf_imageList", cf_imageList);
	              %>
	              <c:forEach var="item" items="${cf_imageList}" varStatus="status">
	                <tr>
	                    <td style="border-left: none;"><%-- <%=documentSet.get("Id")%> --%>이미지</td>
	                    <td>
	                    <b><c:out value="${item.WORKS_TITLE}" /></b>
	                      <p class="mt15">
	                      <c:if test="${!empty item.IMAGE_URL}">
	                       <span class="w60">썸네일 : <img class="thumb" src="${item.IMAGE_URL}${item.IMAGE_THUMBNAIL_NAME}" alt="${item.WRITER}작가 - ${item.WORKS_TITLE}(${item.WORKS_ID})"></span><br>
	                      </c:if>
	                      <c:if test="${!empty item.WORKS_ID}">
	                        <span class="w60">WORKS_ID : ${item.WORKS_ID}</span>
	                      </c:if>
	                      <c:if test="${!empty item.Uid}">
	                        <span class="w60">worksid : ${item.Uid}</span><br>
	                      </c:if>
	                      <c:if test="${!empty item.WORKS_SUB_TITLE}">
	                        <span class="w60">부제 : ${item.WORKS_SUB_TITLE}</span>
	                      </c:if>
	                      <c:if test="${!empty item.Date}">
	                        <span>창착년월일 : ${item.Date}</span><br>
	                      </c:if>
	                      <c:if test="${!empty item.WRITER}">
	                        <span class="w60">작가: ${item.Date}</span>
	                      </c:if>
	                      <c:if test="${!empty item.KEYWORD}">
	                        <span>키워드 : ${item.KEYWORD}</span><br>
	                      </c:if>
	                      <%-- <c:if test="${!empty item.IMAGE_URL}">
	                        <span class="w60">제작자 :  ${item.KEYWORD}<%=objField.get("IMAGE_URL")+""+objField.get("IMAGE_THUMBNAIL_NAME")%></span>
	                      </c:if> --%>
	                      <c:if test="${!empty item.RGST_ORGN_NAME}">
	                        <span>권리관리기관명 : ${item.RGST_ORGN_NAME}</span><br>
	                       </c:if>
	                      </p>
	                    </td>
	                  </tr>
	                </c:forEach>
							<%
	              }else if(obj.get("Id").equals( "cf_photo" )){ 
	                   ArrayList<Map<String, Object>> cf_photoList = new ArrayList<Map<String, Object>>();
	                   documentSet = obj;
	                   documentSetMap = (Map) documentSet.get("DocumentSet");
	                   documentJA = (JSONArray) documentSetMap.get("Document");
	                   if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
	                   for( int e = 0 ; e < documentJA.size() ; e++){
	                        Map<String, Object> exMap = new HashMap<String, Object>(); 
	                        Map objCon=(Map) documentJA.get( e );
	                   /* Map objField = (Map) objCon.get("Field"); */
	                   exMap = (Map) objCon.get("Field");
	                   cf_photoList.add(exMap);
	                   }
	                   request.setAttribute("cf_photoList", cf_photoList);
	               %>
	               <c:forEach var="item" items="${cf_photoList}" varStatus="status">
	                 <tr>
	                     <td scope="row" class="vtop" style="border-left: none;"><%=documentSet.get("Id")%>이미지</td>
	                     <td class="liceSrch">
	                     <b><c:out value="${item.WORKS_TITLE}" /></b>
	                     <%-- <%if(image_open_yn.compareTo("Y")==0&&image_url.compareTo("")!=0){%><a href="<%=image_url%>" target="_blank"><img src="../images/2012/common/ic_find_id.gif"></a><%}%> --%>
	                       <p class="mt15">
	                       <c:if test="${!empty item.IMAGE_URL}">
	                         <span class="w60">썸네일 : <img class="thumb" src="${item.IMAGE_URL}${item.IMAGE_THUMBNAIL_NAME}" alt="${item.WRITER}작가 - ${item.WORKS_TITLE}(${item.WORKS_ID})"></span><br>
	                       </c:if>
	                       <c:if test="${!empty item.WORKS_ID}">
	                         <span class="w60">WORKS_ID : ${item.WORKS_TITLE}</span>
	                       </c:if>
	                       <c:if test="${!empty item.Uid}">
	                         <span class="w60">worksid : ${item.WORKS_TITLE}</span><br>
	                       </c:if>
	                       <c:if test="${!empty item.WORKS_SUB_TITLE and item.WORKS_SUB_TITLE ne '없음'}">
	                         <span class="w60">부제 : ${item.WORKS_TITLE}</span>
	                       </c:if>
	                       <c:if test="${!empty item.Date}">
	                         <span>창착년월일 : ${item.Date}</span><br>
	                       </c:if>
	                       <c:if test="${!empty item.WRITER}">
	                         <span class="w60">작가: ${item.WRITER}</span>
	                       </c:if>
	                       <c:if test="${!empty item.KEYWORD}">
	                         <span>키워드 : ${item.WRITER}</span><br>
	                       </c:if>
	                  <%--      <c:if test="${!empty item.KEYWORD}">
	                         <span class="w60">제작자 :  <%=objField.get("IMAGE_URL")+""+objField.get("IMAGE_THUMBNAIL_NAME")%></span>
	                       </c:if> --%>
	                       <c:if test="${!empty item.RGST_ORGN_NAME}">
	                         <span>권리관리기관명 : ${item.WRITER}</span><br>
	                       </c:if>
	                       </p>
	                     </td>
	                   </tr>
	                  </c:forEach>
	                  
							<%
	              }else if(obj.get("Id").equals( "cf_etc" )){ 
						  ArrayList<Map<String, Object>> cf_etcList = new ArrayList<Map<String, Object>>();
              documentSet = obj;
              documentSetMap = (Map) documentSet.get("DocumentSet");
              documentJA = (JSONArray) documentSetMap.get("Document");
              if(!documentSetMap.get("TotalCount").equals( "0" )){objCont++;}
              for( int e = 0 ; e < documentJA.size() ; e++){
                   Map<String, Object> exMap = new HashMap<String, Object>();
                Map objCon=(Map) documentJA.get( e );
                            exMap = (Map) objCon.get("Field");
              cf_etcList.add(exMap);
              }
              request.setAttribute("cf_etcList", cf_etcList);
             
            %>
            
            <c:forEach var="item" items="${cf_etcList}" varStatus="status">
            <tr style="border-left: none;">
              <td scope="row" class="vtop">기타</td>
              <td class="liceSrch">
              <b><c:out value="${item.WORKS_TITLE}"/></b>
                <p class="mt15">
                <c:if test="${!empty item.SIDE_GENRE_CD}">
                  <span class="w60">저작물 종류 : ${item.SIDE_GENRE_CD}</span>
                </c:if>
                <c:if test="${!empty item.MAKE_DATE}">
                  <span>창작년도 : ${item.MAKE_DATE}</span><br>
                </c:if>
                <c:if test="${!empty item.PUBL_MEDI}">
                  <span class="w60">공표매체 : ${item.PUBL_MEDI}</span>
                </c:if>
                <c:if test="${!empty item.PUBL_DATE}">
                  <span>공표일자 : ${item.PUBL_DATE}</span><br>
                </c:if>
                <c:if test="${!empty item.COPT_NAME}">
                  <span class="w60">저작자 :  ${item.COPT_NAME}</span>
                </c:if>
                <c:if test="${!empty item.RGST_ORGN_NAME}">
                  <span>권리관리기관 : ${item.RGST_ORGN_NAME}</span><br>
                </c:if>
                </p>
              </td>
            </tr>
            </c:forEach>
							<%
									 //end if
										
									}//end for
								
								}// end for
							}else{
								%>
								<strong class="line15 black" style="margin-left: 300px;"><img alt="" class="vmid" src="images/2012/common/ic_tip.gif"> 찾고자 하는 저작물을 검색 하세요.</strong>
								<%
							}//end if 
							%>
							</tbody>
						</table>
						</div><!-- End sub_contents_con -->
						<script type="text/javascript" language="javascript" src="/js/jquery-1.8.3.min.js"></script>
						<script type="text/javascript">
						  var listCount = 10;
						  var totalCount = <%=documentSetMap.get("TotalCount")%>;
						  var totalPage = totalCount / listCount;
						  if(<%=request.getAttribute("startCount")%>==0){
							  var currentPageNum = 1;
						  }else{
							  var currentPageNum = <%=request.getAttribute("startCount")%>;
						  }
						  
						  var pageStr='<%=documentSet.get("Id")%>';
						/*  console.log("currentPageNum : " + currentPageNum)
						 console.log("totalPage : " + totalPage)
						 console.log("pageStr : " + pageStr) */
						  
						  $(function(){
							 if(currentPageNum!=1){
								 $("#commPagination").append('<span class="direction bgNone"><span class="prev mr10" style="margin-top:3px;" onclick="javascript:prevPageCall()"></span></span>');
								 
							 }
							  for(var i = 1 ; i < totalPage ; i++){
								  
								  if(Number(Math.ceil(currentPageNum*0.1)*10) + 1 > i && i >= Number(Math.ceil(currentPageNum*0.1)*10) + 1-10){
									if(currentPageNum == i){
										$("#commPagination").append("<strong><a href='javascript:changePage("+i+",\""+pageStr+"\")' >"+i+"</a></strong>");
									}else{
										$("#commPagination").append("<a href='javascript:changePage("+i+",\""+pageStr+"\")' >"+i+"</a>");
									}
										 
								  }  
							  }
							  $("#commPagination").append('<span class="direction bgNone"><span class="next ml10" style="margin-top:3px;" onclick="javascript:nextPageCall()"></span></span>');
							  //console.log($("#commPagination").html());
						  })
						 
						 function prevPageCall(){
							 currentPageNum--;
							 changePage(currentPageNum,pageStr); 
						 }
						  
						 function nextPageCall(){
							 currentPageNum++;
							 changePage(currentPageNum,pageStr); 
						 }
						  
						  function goInnerSearch2(index){
							  console.log(index)
							  if(index!=0)
								{
								  var frm = document.search;
							
								 	 switch (index) {
									  case "1"    : 
										  frm.genreCd.value = "음악" ;
										  frm.collection.value = "cf_music";
									      break;
									  case "2"   : 
										  frm.genreCd.value = "어문" ;
										  frm.collection.value = "cf_literature";
									      break;
									  case "3"  : 
										  frm.genreCd.value = "방송대본" ;
										  frm.collection.value = "cf_scenario";
									      break;
									  case "4"  : 
										  frm.genreCd.value = "영화" ;
										  frm.collection.value = "cf_movie";
									      break;
									  case "5"  : 
										  frm.genreCd.value = "방송" ;
										  frm.collection.value = "cf_broadcast";
									      break;
									  case "6"  : 
										  frm.genreCd.value = "뉴스" ;
										  frm.collection.value = "cf_news";
									      break;    
									  case "7"  : 
										  frm.genreCd.value = "미술" ;
										  frm.collection.value = "cf_art";
									      break;
									  case "8"  : 
										  frm.genreCd.value = "이미지" ;
										  frm.collection.value = "cf_image";
									      break;
									  case "9"  : 
										  frm.genreCd.value = "사진" ;
										  frm.collection.value = "cf_photo";
									      break;
									  case "99"  : 
										  frm.genreCd.value = "기타" ;
										  frm.collection.value = "cf_etc";
									      break;
									  case "999"  : 
										  frm.collection.value = "ALL";
										  frm.genreCd.value = "total" ;
									      break;
									  case "9999"  : 
											  frm.genreCd.value = "total" ;
											  frm.menuFlag.value = "N";
										      break;
									  default    : 
										  frm.genreCd.value = "" ;
								      	  break;
									} 
								} 
							  changePage(index,frm.collection.value);  
						  }
						  
						  function changePage(pageNum,callName){
							var pageNum =  pageNum;
							var frm = document.search;
							frm.collection.value = callName;
							frm.startCount.value = pageNum*10;
								
							 	 switch (callName) {
								  case "cf_music"    : 
									  frm.genreCd.value = "음악" ;
								      break;
								  case "cf_literature"   : 
									  frm.genreCd.value = "어문" ;
								      break;
								  case "cf_scenario"  : 
									  frm.genreCd.value = "방송대본" ;
								      break;
								  case "cf_movie"  : 
									  frm.genreCd.value = "영화" ;
								      break;
								  case "cf_broadcast"  : 
									  frm.genreCd.value = "방송" ;
								      break;
								  case "cf_news"  : 
									  frm.genreCd.value = "뉴스" ;
								      break;    
								  case "cf_art"  : 
									  frm.genreCd.value = "미술" ;
								      break;
								  case "cf_image"  : 
									  frm.genreCd.value = "이미지" ;
								      break;
								  case "cf_photo"  : 
									  frm.genreCd.value = "사진" ;
								      break;
								  case "cf_etc"  : 
									  frm.genreCd.value = "기타" ;
								      break;
								  case "total"  : 
									  frm.genreCd.value = "total" ;
								      break;
								  case "9999"  : 
										  frm.genreCd.value = "total" ;
										  frm.menuFlag.value = "N";
									      break;
								  default    : 
									  frm.genreCd.value = "" ;
							      	  break;
								} 
							
							frm.submit();
						  }
						  
						</script>
						<div id="commPagination" class="pagination">
							<ul>
								<li>
							<!-- <span class="direction bgNone"><span class="prev mr10" style="margin-top:3px;">123123</span></span> -->
								</li>
							</ul>					
						</div>
						
					</div><!-- End con_rt -->
					<p class="clear"></p>
				</div><!-- End contents -->
				<!-- //주요컨텐츠 end -->
	    		<!-- //content -->
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2017/footer.jsp" />
				<!-- FOOTER end -->
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>