<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="java.util.*" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.myStat.model.StatApplication" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	
	StatApplication statApplication = (StatApplication) request.getAttribute("statApplication");
	
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/new181203.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/deScript.js"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>	
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>

<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/JavaScript">
<!--
//취소
$(function(){
	var year = "${statApplication.applyDate}".substr(0,4);
	var month = "${statApplication.applyDate }".substr(4,2);
	var day = "${statApplication.applyDate }".substr(6,2);
	//alert(year);
	//alert(month);
	//alert(day);
	
	var date = year+"년 "+month+"월 "+day+"일";
	
	$("#regDate").html(date);
	
})

function fn_goList(){
	var frm = document.frm;

	//if(confirm("신청취소를 하시면 등록된 내용이 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/myStat/statRsltInqrList.do";
		frm.submit();
	//}
}


//삭제
function fn_doCancel(){
	var frm = document.frm;

	if(confirm("신청완료 된 신청서를 취소하겠습니까?")){
		frm.stat_cd.value = '11';
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/myStat/statPrpsCancelDo.do";
		frm.submit();
	}
}

//탭 선택에 따른 화면 변경
function fn_chgTabDisplay(idx) {

	var oA = document.getElementsByTagName("A");
	
	for(i = 0; i < oA.length; i++) {
		if( (oA[i].id).indexOf("tabA") > -1) {
			if(oA[i].id == "tabA"+idx) {
				oA[i].className = "active";
			} else {
				oA[i].className = "";
			}
		}
	}
	
    var oTbl = document.getElementById("tblTab");
	var oTr = oTbl.getElementsByTagName("TR");
	for(i = 0; i < oTr.length; i++) {
		if( (oTr[i].id).indexOf("trTab1") > -1) {
			if(oTr[i].id == "trTab1"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
		if( (oTr[i].id).indexOf("trTab2") > -1) {
			if(oTr[i].id == "trTab2"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
		if( (oTr[i].id).indexOf("trTab3") > -1) {
			if(oTr[i].id == "trTab3"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
		if( (oTr[i].id).indexOf("trTab4") > -1) {
			if(oTr[i].id == "trTab4"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
	}
}

//임시저장 및 화면이동
function fn_doSave() {
	var frm = document.frm;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		if(confirm('작성된 이용승인 신청서를 제출하시겠습니까?')){
    		
    		// 공인인증절차 수행을 위한 원본메시지를 생성한다. (원본메시지는 XML 로 생성한다.)
    		// 원본 메시 데이터 구성 : 화면에 표시된 HTML 정보를 추출한다.
    		frm.originalMessage.value = document.all.goData.innerHTML;
    		
    		// 공인 인증절차를 수행하여 성공한 경우 제출한다.
    		if(fn_signCert(frm)){
    			frm.branch.value='1';
    			
				frm.target = "_self";
				frm.method = "post";
				frm.action = "/myStat/statPrpsInsertDo.do";
				frm.submit();
    		} else {
    			return false;
   			}
		}
	}
}

function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.fileFrm;

	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;
	
	frm.method = "post";
	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
}

// 출력물 
function fn_report(apply_write_ymd, apply_write_seq, report){

	var frm = document.frm;
	
	var sUrl = "/myStat/statPrpsDetl.do";

	var param = "?apply_write_ymd="+apply_write_ymd;
	     param += "&apply_write_seq="+apply_write_seq;
	     param += "&mode=R";
	     param += "&report="+report;
	 
	sUrl += param;
	window.open(sUrl,"PRINT","toolbar=no,menubar=no,resizable=no,status=yes");
}

// 수수료 결제창 
function fn_goPay(apply_write_ymd, apply_write_seq, apply_works_cnt){
	
	var sUrl = "/myStat/statPrps_cPay.do";

	var param = "?apply_write_ymd="+apply_write_ymd;
	     param += "&apply_write_seq="+apply_write_seq;
	     param += "&apply_works_cnt="+apply_works_cnt;
	
	sUrl += param;
		     
	window.open(sUrl, "PAY", "width=740,height=455,scrollbars=no");
	
}

//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- //HEADER end -->
		
		<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title">내 권리 찾기</div>
			<ul class="sub_lf_menu">
				<li><a href="/mlsInfo/rghtInfo01.jsp" class="on">저작권 정보 확인</a>
					<ul class="sub_lf_menu2">
						<li><a href="/mlsInfo/rghtInfo01.jsp" class="on">소개 및 이용방법</a></li>
						<li><a href="/rghtPrps/rghtSrch.do?DIVS=M">서비스 이용</a></li>
					</ul>
				</li>
				<li><a href="/mlsInfo/inmtInfo02.jsp">미분배 보상금 대상 저작물 확인</a>
					<ul class="sub_lf_menu2 disnone">
						<li><a href="/mlsInfo/inmtInfo02.jsp">소개 및 이용방법</a></li>
						<li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">서비스 이용</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="https://www.findcopyright.or.kr/images/sub_img/sub_home.png" alt="홈 페이지">
				&gt;
				법정허락 승인 신청
				&gt;
				<span class="bold">법정허락 승인 신청</span>
			</div>
			<div class="con_rt_hd_title">법정허락 승인 신청</div>
			<div id="sub_contents_con">
				<!-- 2018.12.03 컨텐츠 추가 -->
				
				<div class="list_step_apply">
					<ul>
						<li>
							<div class="wrap">
								<p>STEP 01</p>
								<strong>법정허락 승인 신청 서류 작성</strong>
							</div>
						</li>
						<li>
							<div class="wrap">
								<p>STEP 02</p>
								<strong>첨부서류 제출하기</strong>
							</div>
						</li>
						<li class="active">
							<div class="wrap">
								<p>STEP 03</p>
								<strong>신청정보 확인 및 제출하기</strong>
							</div>
						</li>
					</ul>
				</div>
					
				<!-- 신청정보 영역 -->
				<div class="view_apply">
					
				</div>

				<h2 class="sub_con_h2 mar_tp30">첨부서류</h2>
				<table class="sub_tab td_padding tbl_apply mar_tp15" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
					<caption>한국저작권위원회</caption>
					<colgroup>
						<col style="width:375px">
						<col style="width:auto">
					</colgroup>
					<tbody>
						<tr>
							<th>이용승인신청명세서</th>
							<td><a href="" class="link">이용승인신청명세서엑셀.xltx <span>(13.8 mb)</span></a></td>
						</tr>
						<tr>
							<th>보상금액 산정내역서</th>
							<td><a href="" class="link">보상금액 산정내역서.hwp <span>(3 mb)</span></a></td>
						</tr>
						<tr>
							<th>‘상당한 노력’ 절차 수행 증빙 자료</th>
							<td><a href="" class="link">증빙자료.pdf <span>(2.1 mb)</span></a></td>
						</tr>
						<tr>
							<th>‘상당한 노력’ 절차 외 권리자를 찾기 위해 노력한 활동 자료</th>
							<td><a href="" class="link">활동자료.hwp <span>(3 mb)</span></a></td>
						</tr>
						<tr>
							<th>해당 저작물이 공표되었음을 밝힐 수 있는 서류</th>
							<td><a href="" class="link">증빙서류.hwp <span>(6 mb)</span></a></td>
						</tr>
						<tr>
							<th>신청 저작물의 파일 또는 내용물</th>
							<td><a href="" class="link">신청 저작물.hwp <span>(4 mb)</span></a></td>
						</tr>
					</tbody>
				</table>
				
				
				<div class="btn_area mar_tp50">
					<a href="step_2.html" class="btn_style2">이전단계</a><a href="" class="btn_style2">임시저장</a><a href="step_3.html" class="btn_style3">제출하기</a>
				</div>

				<!-- 2018.12.03 컨텐츠 추가 끝-->
			
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
<!-- //content -->

		<!-- //CONTAINER end -->
	
	<!-- FOOTER str-->
	<jsp:include page="/include/2011/footer.jsp" />
	<!-- //FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	

<script type="text/JavaScript">
<!--
	window.onload = function(){
	
						//이용승인신청_신청서 종류 체크박스 셋팅
						var frm = document.frm;
						
						if('${statApplication.applyType01 }' == '1') {
							frm.applyType01.checked = true;
							frm.dummyApplyType01.checked = true;
						}
						if('${statApplication.applyType02 }' == '1') {
							frm.applyType02.checked = true;
							frm.dummyApplyType02.checked = true;
						}
						if('${statApplication.applyType03 }' == '1') {
							frm.applyType03.checked = true;
							frm.dummyApplyType03.checked = true;
						}
						if('${statApplication.applyType04 }' == '1') {
							frm.applyType04.checked = true;
							frm.dummyApplyType04.checked = true;
						}
						if('${statApplication.applyType05 }' == '1') {
							frm.applyType05.checked = true;
							frm.dummyApplyType05.checked = true;
						}


						<c:if test="${not empty statApplyWorksList}">
							
							//신청서 구분에 값 맵핑
		    				<c:set var="dySeq" value="0"/>
			    			<c:forEach items="${statApplyWorksList}" var="statApplyWorksList">
			    				<c:if test="${statApplyWorksList.APPLYTYPE01 == '1'}">
			    					document.getElementById("dummyApplyType01_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE02 == '1'}">
			    					document.getElementById("dummyApplyType02_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE03 == '1'}">
			    					document.getElementById("dummyApplyType03_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE04 == '1'}">
			    					document.getElementById("dummyApplyType04_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE05 == '1'}">
			    					document.getElementById("dummyApplyType05_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:set var="dySeq" value="${dySeq + 1}"/>
			    			</c:forEach>
						</c:if>

					}
//-->
</script>
</body>
</html>
