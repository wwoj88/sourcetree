<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="java.util.*" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@include file="/include/sg_include.jsp"%>
<title>법정허락 | 저작권찾기</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/deScript.js"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>	
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/JavaScript">
<!--
//취소
function fn_goList(){
	var frm = document.frm;

	//if(confirm("신청취소를 하시면 등록된 내용이 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/myStat/myStat.do?method=statRsltInqrList";
		frm.submit();
	//}
}


//탭 선택에 따른 화면 변경
function fn_chgTabDisplay(idx) {

	var oA = document.getElementsByTagName("A");
	
	for(i = 0; i < oA.length; i++) {
		if( (oA[i].id).indexOf("tabA") > -1) {
			if(oA[i].id == "tabA"+idx) {
				oA[i].className = "active";
			} else {
				oA[i].className = "";
			}
		}
	}
	
    var oTbl = document.getElementById("tblTab");
	var oTr = oTbl.getElementsByTagName("TR");
	for(i = 0; i < oTr.length; i++) {
		if( (oTr[i].id).indexOf("trTab1") > -1) {
			if(oTr[i].id == "trTab1"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
		if( (oTr[i].id).indexOf("trTab2") > -1) {
			if(oTr[i].id == "trTab2"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
		if( (oTr[i].id).indexOf("trTab3") > -1) {
			if(oTr[i].id == "trTab3"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
		if( (oTr[i].id).indexOf("trTab4") > -1) {
			if(oTr[i].id == "trTab4"+idx) {
				oTr[i].style.display = "";
			} else {
				oTr[i].style.display = "none";
			}
		}
	}
}

//임시저장 및 화면이동
function fn_doSave() {
	var frm = document.frm;
	var nowStatCd = '${statApplication.statCd }';

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	var nowStatCd = '${statApplication.statCd }';
	
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		if(confirm('작성된 이용승인 신청서를 제출하시겠습니까?')){
    		
    		// 공인인증절차 수행을 위한 원본메시지를 생성한다. (원본메시지는 XML 로 생성한다.)
    		// 원본 메시 데이터 구성 : 화면에 표시된 HTML 정보를 추출한다.
    		frm.originalMessage.value = document.all.goData.innerHTML;
    		
    		// 공인 인증절차를 수행하여 성공한 경우 제출한다.
    		if(fn_signCert(frm)){
    			frm.branch.value='1';
    			
    			if(nowStatCd == null || nowStatCd == '1') {
    				frm.stat_cd.value = '2';
    			}else if(nowStatCd== '3' || nowStatCd== '4'){
	    			frm.stat_cd.value = '5';
    			}
    			
				frm.target = "_self";
				frm.method = "post";
				frm.action = "/myStat/myStat.do?method=statPrpsInsertDo";
    			if(nowStatCd== '3' || nowStatCd== '4'){
				frm.action = "/myStat/myStat.do?method=statPrpsUpdateDo";
    			}
				frm.submit();
    		} else {
    			return false;
   			}
		}
	}
}

function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.fileFrm;

	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;
	
	frm.method = "post";
	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
}




//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2>
					<span>
						<img src="/images/2011/title/container_vis_h2_8.gif" alt="마이페이지" title="마이페이지" />
						<em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em>
					</span>
				</h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content" id="goData">
			
				<!-- 래프 -->
				<jsp:include page="/include/2011/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1");
					subSlideMenu("sub_lnb","lnb13");
				</script>
				<!-- //래프 -->
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>신청현황</span><em>법정허락</em></p>
					<h1><img src="/images/2011/title/content_h1_0807.gif" alt="법정허락" title="법정허락" /></h1>
					<jsp:include page="/common/memo/2011/memo_01.jsp">
						<jsp:param name="DIVS" value="MS" />
					</jsp:include>					
					<div class="section mt20">
						<div class="usr_process stat_process">
							<div class="process_box">
								<ul class="floatDiv">
								<li class="fl ml0"><img alt="1단계 약관동의" src="/images/2011/content/process21_off.gif"></li>
								<li class="fl"><img alt="2단계 회원정보입력" src="/images/2011/content/process22_off.gif"></li>
								<li class="fl bgNone pr0 on"><img alt="3단계 가입완료" src="/images/2011/content/process23_on.gif"></li>
								</ul>
							</div>
						</div>

						<form name="fileFrm" action="#">
							<input type="hidden" name="filePath">
							<input type="hidden" name="fileName">
							<input type="hidden" name="realFileName">
							<input type="submit" style="display:none;">
						</form>
						
						<form name="frm" action="#">
							<input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
							<input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
							<input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
							<input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
							<input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
							<input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
							<input type="hidden" name="action_div" value=""/>
							<input type="hidden" name="stat_cd" value=""/>
							<input type="hidden" name="rgstIdnt" value="${userInfo.USER_IDNT }"/>
							
							<input type="hidden" name="applyWriteYmd" value="${srchParam.applyWriteYmd }">
							<input type="hidden" name="applyWriteSeq" value="${srchParam.applyWriteSeq }">

							<input type="hidden" name="branch" value="1" />
							
							<input type="submit" style="display:none;">
							
							<!-- 로그인 사용자 인증 데이터를 매번 다르게 하기 위하여 WAS 서버에서 랜덤하게 생성한 nonce 값 -->
							<input type="hidden" name="Challenge" value="<%=strChallenge%>" />
							<!-- 대칭키를 RSA 알고리즘으로 암호화하기 위한 WAS 서버의 암호화용 인증서  -->
							<input type="hidden" name="ServerKmCert" value="<%=ServerKmCertPem%>" />
							<!-- 사용자가 선택한 인증서를 KICA SecuKit JavaScript API에서 추적하는데 사용할 값 -->
							<input type="hidden" name="certId" />
							<!-- 사용자가 선택한 전자서명용 인증서 정보 -->
							<input type="hidden" name="userSignCert" />
							<!-- 사용자가 선택한 인증서(전자서명용 개인키)로 전자서명 을 위한 원본 값  -->
							<textarea name="originalMessage" style="display:none;" title="originalMessage" rows="0" cols="0" readonly="readonly"></textarea>
							<!-- 사용자가 선택한 인증서(전자서명용 개인키)로 전자서명 값을 생성 -->
							<input type="hidden" name="userSignValue" />
							<!-- 데이터를 암호화 할 때 사용할 비밀키를 생성하고, 서버의 인증서(공개키)로 암호화한 값 -->
							<input type="hidden" name="encryptedSessionKey" />
							<!-- 사용자가 인증서 소유자임을 확인하기 위한 값 -->
							<input type="hidden" name="encryptedUserRandomNumber" />
							<!-- 사용자 주민등록번호를 암호화한 값 -->
							<input type="hidden" name="encryptedUserSSN" />
							
							<!-- 회원유형 (MEMBER_TYPE - BIZ_TYPE_CD)  -->
							<input type="hidden" name="memberType" value="<fmt:formatNumber value="${userInfo.USER_DIVS}" type="number"/>" />
							<!-- 주민등록번호 -->
							<input type="hidden" name="ssnNo" value="${userInfo.RESD_CORP_NUMB}" />
							<!-- 사업자번호  -->
							<input type="hidden" name="corpNo" value="${userInfo.CORP_NUMB}" />
							
						<div class="article mt20">
							<!-- 이용 승인신청 정보 str -->
							<div class="floatDiv mt20">
								<h2 class="fl">이용 승인신청 정보</h2>
								<p class="fr"></p>
							</div>
							
							<span class="topLine"></span>
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid" style="table-layout:fixed;">
								<colgroup>
								<col width="20%">
								<col width="*">
								<col width="20%">
								<col width="20%">
								</colgroup>
								<tbody>
									<tr>
										<td colspan="3" rowspan="2">
											<c:set var="dySeq" value="0"/>
											<c:forEach items="${applyTypeList}" var="applyTypeList">
												<c:set var="dySeq" value="${dySeq + 1}"/>
												<input type="checkbox" id="applyType0${dySeq }" title="선택" name="applyType0${dySeq }" value="1" disabled="disabled">
												<label for="" class="p12">${applyTypeList.codeName }</label>&nbsp;
											</c:forEach>
											<span style="color:#000;margin-left: 35px;font-size:16pt;color:#000;font-weight:bold;">이용 승인신청서</span>
										</td>
										<th scope="row" style="text-align: center;"><label for="">처리기간</label></th>
									</tr>
									<tr>
										<td style="text-align: center;">40일</td>
									</tr>
									<tr>
										<th scope="row"><label for="applyWorksTitl">제호(제목)</label></th>
										<td colspan="3">
											<p class="mb5">
												<input type="checkbox" value="Y" title="선택" disabled="disabled" <c:if test="${statApplication.applyWorksCnt > 0}">checked="checked"</c:if>>
												<label for="" class="p12">여러건 신청 : 총 ${statApplication.applyWorksCnt } 건</label>
											</p>
											${statApplication.applyWorksTitl }
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">종류</label></th>
										<td>${statApplication.applyWorksKind }</td>
										<th scope="row"><label for="">형태 및 수량</label></th>
										<td>${statApplication.applyWorksForm }</td>
									</tr>
									<tr>
										<th scope="row"><label for="">이용의 내용</label></th>
										<td colspan="3">
											${statApplication.usexDesc }
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">승인신청사유</label></th>
										<td colspan="3">
											${statApplication.applyReas }
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">보상금액 </label></th>
										<td colspan="3">
											${statApplication.cpstAmnt }
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">신청인</label></th>
										<td colspan="3">
										
											<span class="topLine2"></span>
											<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
												<colgroup>
												<col width="15%">
												<col width="30%">
												<col width="25%">
												<col width="*">
												</colgroup>
												<tbody>
													<tr>
														<th scope="row">성명(법인명)</th>
														<td>${statApplication.applrName }</td>
														<th scope="row">주민등록번호(법인등록번호)</th>
														<td>
														<c:choose>
															<c:when test="${fn:length(statApplication.applrResdCorpNumb) == 10 }">
																${fn:substring(statApplication.applrResdCorpNumb, 0, 3)}-${fn:substring(statApplication.applrResdCorpNumb, 3, 5)}-${fn:substring(statApplication.applrResdCorpNumb, 5, 10)}
															</c:when>
															<c:when test="${fn:length(statApplication.applrResdCorpNumb) == 14 }">
																${fn:substring(statApplication.applrResdCorpNumb, 0, 6) }-${fn:substring(statApplication.applrResdCorpNumb, 7,8) }******
															</c:when>
														</c:choose>
														</td>
													</tr>
													<tr>
														<th scope="row">주소</th>
														<td>${statApplication.applrAddr }</td>
														<th scope="row">전화번호</th>
														<td>${statApplication.applrTelx }</td>
													</tr>
												</tbody>
											</table>
										
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="">대리인</label></th>
										<td colspan="3">
										
											<span class="topLine2"></span>
											<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
												<colgroup>
												<col width="15%">
												<col width="30%">
												<col width="25%">
												<col width="*">
												</colgroup>
												<tbody>
													<tr>
														<th scope="row">성명(법인명)</th>
														<td>${statApplication.applyProxyName }</td>
														<th scope="row">주민등록번호(법인등록번호)</th>
														<td>
														<c:choose>
															<c:when test="${fn:length(statApplication.applyProxyResdCorpNumb) == 10 }">
																${fn:substring(statApplication.applyProxyResdCorpNumb, 0, 3)}-${fn:substring(statApplication.applyProxyResdCorpNumb, 3, 5)}-${fn:substring(statApplication.applyProxyResdCorpNumb, 5, 10)}
															</c:when>
															<c:when test="${fn:length(statApplication.applyProxyResdCorpNumb) == 14 }">
																${fn:substring(statApplication.applyProxyResdCorpNumb, 0, 6) }-${fn:substring(statApplication.applyProxyResdCorpNumb, 7,8) }******
															</c:when>
														</c:choose>
														</td>
													</tr>
													<tr>
														<th scope="row">주소</th>
														<td>${statApplication.applyProxyAddr }</td>
														<th scope="row">전화번호</th>
														<td>${statApplication.applyProxyTelx }</td>
													</tr>
												</tbody>
											</table>
										
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- 이용 승인신청 정보 end -->
						<!-- 대리인 정보 str -->
						<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup>
							<col width="15%">
							<col width="*">
							<col width="20%">
							</colgroup>
							<tbody>
								<tr>
									<td colspan="4">
										<div class="floatDiv ml100 mt20">
											<span class="fl inBlock mt30">저작권법</span> 
											<p class="fl ml10 black">
											<input type="checkbox" value="50" disabled="disabeld" title="선택"
												<c:if test="${statApplication.applyRawCd == '50' }"> checked="checked" </c:if>><label class="p12" for=""> 제 50조</label><br />
											<input type="checkbox" value="51" disabled="disabeld" title="선택"
												<c:if test="${statApplication.applyRawCd == '51' }"> checked="checked" </c:if>><label class="p12" for=""> 제 51조</label><br />
											<input type="checkbox" value="52" disabled="disabeld" title="선택"
												<c:if test="${statApplication.applyRawCd == '52' }"> checked="checked" </c:if>><label class="p12" for=""> 제 52조</label><br />
											<input type="checkbox" value="89" disabled="disabeld" title="선택"
												<c:if test="${statApplication.applyRawCd == '89' }"> checked="checked" </c:if>><label class="p12" for=""> 제 89조</label><br />
											<input type="checkbox" value="97" disabled="disabeld" title="선택"
												<c:if test="${statApplication.applyRawCd == '97' }"> checked="checked" </c:if>><label class="p12" for=""> 제 97조</label><br />
											</p>
											<span class="fl inBlock mt30 ml10">에 따라 위와같이 </span> 
											<p class="fl ml10 black">
											<c:set var="dySeq" value="0"/>
											<c:forEach items="${applyTypeList}" var="applyTypeList">
												<c:set var="dySeq" value="${dySeq + 1}"/>
												<input type="checkbox" title="선택" id="dummyApplyType0${dySeq }" disabled="disabled">
												<label for="" class="p12">${applyTypeList.codeName }</label><br/>
											</c:forEach>
											</p>
											<span class="fl inBlock mt30 ml20">이용의 승인을 신청합니다. </span> 
										</div>
										<div class="floatDiv ml100">
											<span class="fr inBlock mt25 mr20" style="color:#000;">
												<c:set var="date" value="<%= new Date() %>" />
												<fmt:formatDate value="${date}" type="date" pattern="yyyy년 MM월 dd일" />&nbsp;</span>
											</span>
										</div>
										<div class="floatDiv ml100">
											<span class="fr inBlock mt15 mr50" style="color:#000;font-weight:bold;">
												<%=sessUserName%>
											</span>
											<span class="fr inBlock mt15 mr50" style="color:#000;">
												신청인
											</span>
										</div>
										<div style="height: 20px;"></div>
										<span style="font-size:16pt;color:#000;font-weight:bold;">&nbsp;한 국 저 작 권 위 원 회</span>
										<span style="font-size:13pt;color:#000;"> &nbsp;귀 중</span>
									</td>
								</tr>
								<tr>
									<th scope="row" rowspan="2" style="text-align:center;"><label for="">첨부서류</label></th>
									<td rowspan="2">
										<c:if test="${not empty statApplication.fileList}">
										<!-- 첨부화일 시작 -->
				                        <div class="mReset">
											<div class="mb15">
				                                <table id="tblAttachFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부화일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
													<colgroup>
													    <col width="30%">
													    <col width="*%">
													</colgroup>
													<tbody>
													    <tr>
				    									    <th scope="row" style="text-align:center;">서류구분</th>
				    									    <th scope="row" style="text-align:center;">첨부화일명</th>
													    </tr>
											    		<c:if test="${not empty statApplication.fileList}">
											    			<c:forEach items="${statApplication.fileList}" var="fileList">
															    <tr>
															    	<td>${fileList.fileNameCdNm }</td>
															    	<td>
													    				<a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
													    					onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
														    				${fileList.fileName }
														    				<input name="attcSeqn" id="attcSeqn_D1" value="${fileList.attcSeqn }" type="hidden" />
													    				</a>
															    	</td>
															    </tr>
											    			</c:forEach>
											    		</c:if>
													</tbody>
												</table>
											</div>
										</div>
										<!-- 첨부화일 끝 -->
										</c:if>
										<c:if test="${empty statApplication.fileList}">
										첨부서류 미등록 
										</c:if>
									</td>
									<th scope="row" style="text-align:center;" height="20%;"><label for="">수수료(${statApplication.applyWorksCnt }건)</label></th>
								</tr>
								<tr>
									<td style="text-align: center;">
									<c:set var="priceData" value="${statApplication.applyWorksCnt*10000}" />
									<fmt:formatNumber value="${priceData}" pattern="###,###,###,##0" groupingUsed="true" /> 원
									</td>
								</tr>
							</tbody>
						</table>
						<br/>
						<!-- 이용 승인신청 명세서 정보 str -->
						<c:if test="${not empty statApplyWorksList}">
							<div class="floatDiv mt20">
								<h2 class="fl">
									이용 승인신청 명세서 정보 
								</h2>
								<p class="fr"></p>
							</div>
							<!-- 탭 str -->
							<div id="divTab" class="fl newStatus">
								<c:if test="${not empty statApplyWorksList}">
									<c:set var="idx" value="0"/>
									<c:set var="display" value=""/>
									<c:forEach items="${statApplyWorksList}" var="list">
	
										<c:if test="${fn:length(statApplyWorksList) == (idx+1)}">
											<c:set var="display" value="active"/>
										</c:if>
										<UL name="tab" id="tabD${idx }" style="display:block;height:25px">
											<li>
												<a href="#1" id="tabAD${idx }" name="tabA" onclick="fn_chgTabDisplay('D${idx }');" onkeypress="fn_chgTabDisplay('D${idx }');" class="${display }">
													<c:if test="${fn:length(list.WORKSTITL) > 5}">
														<strong id="tabNmD${idx }" style="width:80px;">${fn:substring(list.WORKSTITL, 0, 5)}</strong>
													</c:if>
													<c:if test="${fn:length(list.WORKSTITL) < 6}">
														<strong id="tabNmD${idx }" style="width:80px;">${list.WORKSTITL}</strong>
													</c:if>
												</a>
											</li>
										</UL>
										<c:set var="idx" value="${idx+1}"/>
									</c:forEach>
								</c:if>
							</div>
							<!-- 탭 end -->
							
							<table id="tblTab" cellspacing="0" cellpadding="0" border="1" class="grid " summary="">
								<colgroup>
								<col width="5%">
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<c:if test="${not empty statApplyWorksList}">
										<c:set var="tblSeq" value="0"/>
										<c:set var="idx" value="0"/>
										<c:set var="display" value="none"/>
										<c:forEach items="${statApplyWorksList}" var="list">
											<c:set var="tblSeq" value="${tblSeq + 1}"/>
											<c:if test="${(fn:length(statApplyWorksList)) == (idx+1)}">
												<c:set var="display" value=""/>
											</c:if>
											<TR name="trTab1" id="trTab1D${idx }" style="display:${display };">
												<TH rowSpan="4" scope="row" class="ce">${tblSeq }</TH>
												<TH scope="row">저작물</TH>
												<TD>
													<p class="mb5">
														<c:set var="dySeq" value="0"/>
														<c:forEach items="${applyTypeList}" var="applyTypeList">
															<c:set var="dySeq" value="${dySeq + 1}"/>
															<input type="checkbox" id="dummyApplyType0${dySeq }_D${idx }" name="dummyApplyType0${dySeq }" value="1" 
																disabled="disabled" >
															<label for="dummyApplyType0${dySeq }_D${idx }" class="p12">${applyTypeList.codeName }</label>&nbsp;
														</c:forEach>
													</p>
													<span class="topLine2"></span>
													<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
														<colgroup>
															<col width="20%">
															<col width="30%">
															<col width="20%">
															<col width="*">
														</colgroup>
														<tbody>
															<tr>
																<th scope="row">
																	<label for="">제호(제목)</label>
																</th>
																<td colspan="3">${list.WORKSTITL }</td>
															</tr>
															<tr>
																<th scope="row">
																	<label for="">종류</label>
																</th>
																<td>${list.WORKSKIND }</td>
																<th scope="row">
																	<label for="">형태 및 수량</label>
																</th>
																<td>${list.WORKSFORM }</td>
															</tr>
														</tbody>
													</table>
												</TD>
											</TR>
											<TR name="trTab2" id="trTab2D${idx }" style="display:${display };">
												<TH scope="row">공표<span class="block gray thin p11">(*실연/음반발매/<br>방송 등 포함)</span></TH>
												<Td>
													<span class="topLine2"></span>
													<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
														<colgroup>
															<col width="20%">
															<col width="30%">
															<col width="20%">
															<col width="*">
														</colgroup>
														<tbody>
															<tr>
																<th scope="row">
																	<label for="">공표연월일</label>
																</th>
																<td>${list.PUBLYMD }</td>
																<th scope="row">
																	<label for="">공표국가</label>
																</th>
																<td>${list.PUBLNATN }</td>
															</tr>
															<tr>
																<th scope="row">
																	<label for="">공표방법</label>
																</th>
																<td>
																	<c:set var="dySeq" value="0"/>
																	<c:forEach items="${publMediList}" var="publMediList">
																		<c:set var="dySeq" value="${dySeq + 1}"/>
																		<input type="checkbox" name="dummyPublMediCdD${idx }" id="dummyPublMediCd_${publMediList.code }_D${idx }" value="${publMediList.code }" 
																			<c:if test="${list.PUBLMEDICD == publMediList.code}">checked="checked"</c:if> disabled="disabled" 
																			onclick="fn_chkPublMediCd(this, 'D${idx }');" onkeypress="fn_chkPublMediCd(this, 'D${idx }');">
																		<label for="">${publMediList.codeName }</label>&nbsp;<br/>
																	</c:forEach>
																	<input type="text" name="publMediEtc" id="publMediEtcD${idx }" size="12" value="${list.PUBLMEDIETC}" 
																		 readonly="readonly" style="display:<c:if test="${list.PUBLMEDICD != '99'}">none</c:if>;" >
																</td>
																<th scope="row">
																	<label for="">공표매체정보</label>
																</th>
																<td>${list.PUBLMEDI }</td>
															</tr>
														</tbody>
													</table>
												</Td>
											</TR>
											<TR name="trTab3" id="trTab3D${idx }" style="display:${display };">
												<TH scope="row">권리자</TH>
												<Td>
													<span class="topLine2"></span>
													<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
														<colgroup>
															<col width="20%">
															<col width="30%">
															<col width="20%">
															<col width="*">
														</colgroup>
														<tbody>
															<tr>
																<th scope="row">
																	<label for="">성명(법인명)</label>
																</th>
																<td>${list.COPTHODRNAME }</td>
																<th scope="row">
																	<label for="">전화번호</label>
																</th>
																<td>${list.COPTHODRTELXNUMB }</td>
															</tr>
															<tr>
																<th scope="row">
																	<label for="">주소</label>
																</th>
																<td colspan="3">${list.COPTHODRADDR }</td>
															</tr>
														</tbody>
													</table>
												</Td>
											</TR>
											<TR name="trTab4" id="trTab4D${idx }" style="display:${display };">
												<TH scope="row"><label for="">신청물의 내용</label></TH>
													<Td>
														${list.WORKSDESC}
													</Td>
												</TH>
											</TR>
											<c:set var="idx" value="${idx+1 }"/>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
						</c:if>
						<!-- 대리인 정보 end -->
						</form>
						<!-- 이용 승인신청 명세서 정보 end -->
						
						
						<!-- button area str -->
						<div class="btnArea">
							<p class="fl">
								<span class="button medium gray"><a href="#1" onclick="fn_goList();" onkeypress="fn_goList();">취소</a></span>
							</p>
							<p class="fr">
								<!-- <span class="button medium"><a href="">임시저장</a></span>  -->
								<span class="button medium"><a href="#1" onclick="fn_doSave();" onkeypress="fn_doSave();">신청하기</a></span>
							</p>
						</div>
						<!-- button area end -->
					</div>
					
				</div>
				<!-- //주요컨텐츠 end -->
				
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- //FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	

<script type="text/JavaScript">
<!--
	window.onload = function(){
	
						//이용승인신청_신청서 종류 체크박스 셋팅
						var frm = document.frm;
						
						if('${statApplication.applyType01 }' == '1') {
							frm.applyType01.checked = true;
							frm.dummyApplyType01.checked = true;
						}
						if('${statApplication.applyType02 }' == '1') {
							frm.applyType02.checked = true;
							frm.dummyApplyType02.checked = true;
						}
						if('${statApplication.applyType03 }' == '1') {
							frm.applyType03.checked = true;
							frm.dummyApplyType03.checked = true;
						}
						if('${statApplication.applyType04 }' == '1') {
							frm.applyType04.checked = true;
							frm.dummyApplyType04.checked = true;
						}
						if('${statApplication.applyType05 }' == '1') {
							frm.applyType05.checked = true;
							frm.dummyApplyType05.checked = true;
						}


						<c:if test="${not empty statApplyWorksList}">
							//신청서 구분에 값 맵핑
		    				<c:set var="dySeq" value="0"/>
			    			<c:forEach items="${statApplyWorksList}" var="statApplyWorksList">
			    				<c:if test="${statApplyWorksList.APPLYTYPE01 == '1'}">
			    					document.getElementById("dummyApplyType01_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE02 == '1'}">
			    					document.getElementById("dummyApplyType02_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE03 == '1'}">
			    					document.getElementById("dummyApplyType03_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE04 == '1'}">
			    					document.getElementById("dummyApplyType04_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE05 == '1'}">
			    					document.getElementById("dummyApplyType05_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:set var="dySeq" value="${dySeq + 1}"/>
			    			</c:forEach>
						</c:if>

					}
//-->
</script>
</body>
</html>
