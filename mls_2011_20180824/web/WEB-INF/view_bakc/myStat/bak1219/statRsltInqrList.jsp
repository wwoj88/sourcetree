<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.support.constant.LoginConstants" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>법정허락 | 저작권찾기</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2010/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>	
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/2010/prototype.js"> </script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/JavaScript"><!--

/*calendar호출*/
function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}

//페이징
function fn_goPage(pageNo){
	var frm = document.frm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.target = "_self";
	frm.method = "post";
	frm.action = "/myStat/myStat.do?method=statRsltInqrList";
	frm.page_no.value = pageNo;
	frm.submit();
}

//필수체크
function fn_chkValue() {
	var frm = document.frm;

	return checkForm2(frm);
}

//검색
function fn_search(){
	var frm = document.frm;

	/*if(fn_chkValue()) {
		if(frm.srchApplyFrDt.value != "" || frm.srchApplyToDt.value != "") {
			if(frm.srchApplyFrDt.value > frm.srchApplyToDt.value) {
				alert('신청일자 시작일이 신청일자 종료일 보다 큽니다.');
				frm.srchApplyToDt.focus();
				return;
			}
		}
	}*/

	frm.target = "_self";
	frm.method = "post";
	frm.action = "/myStat/myStat.do?method=statRsltInqrList";
	frm.submit();
}


//삭제
function fn_doDelete(){
	var frm = document.frm;

	var oChk = document.getElementsByName("chk");
	var isChecked = false;
	for(i = 0; i < oChk.length; i++){
		if(oChk[i].checked)	isChecked = true;
	}

	if(!isChecked) {
		alert('삭제대상을 선택 해 주세요');
		return;
	}else{
		if(confirm('삭제 하시겠습니까?')){
			frm.target = "_self";
			frm.method = "post";
			frm.action = "/myStat/myStat.do?method=statPrpsDeleteDo";
			frm.submit();
		}
	}
}


//신청
function fn_goSetp1(sDiv) {
	var frm = document.frm;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/myStat/myStat.do?method=statPrps";
		frm.action_div.value = sDiv;
		frm.submit();
	}
}

//상세
function fn_goDetail(sYmd, sSeq, sStat) {

	if(sYmd != '' && sSeq != '') {
		var frm = document.frm;
		frm.apply_write_ymd.value = sYmd;
		frm.apply_write_seq.value = sSeq;
		frm.stat_cd.value = sStat;
	
		frm.target = "_self";
		frm.method = "post";
		
		if(sStat == '1' || sStat == '3' || sStat == '4') {
			frm.action = "/myStat/myStat.do?method=statPrpsModi";
		}else{
			frm.action = "/myStat/myStat.do?method=statPrpsDetl";
		}
		frm.submit();
	}
}

//
--></script>
</head>

<body>

	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="container">

			<div class="container_vis">
				<h2>
					<span>
						<img src="/images/2011/title/container_vis_h2_8.gif" alt="마이페이지" title="마이페이지" /><em>
						<img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em>
					</span>
				</h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>

			<div class="content">
				<!-- 래프 -->
				<jsp:include page="/include/2011/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb1");
					subSlideMenu("sub_lnb","lnb13");
				</script>
				<!-- //래프 -->
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path">
						<span>Home</span>
						<span>마이페이지</span>
						<span>신청현황</span>
						<em>법정허락</em>
					</p>
					<h1><img src="/images/2011/title/content_h1_0807.gif" alt="법정허락" title="법정허락" /></h1>
					
					<div class="section">
						<!-- 검색 -->
						<form name="frm" action="#">
							<input type="hidden" name="page_no" value="${srchParam.nowPage}"/>
							<input type="hidden" name="action_div"/>

							<input type="hidden" name="apply_write_ymd"/>
							<input type="hidden" name="apply_write_seq"/>
							<input type="hidden" name="stat_cd"/>
							<input type="submit" style="display:none;">
							
						<fieldset class="w100">
							<legend></legend>
							
							<div class="boxStyle">
							
								<div class="box1 floatDiv">
									<p class="fl mt5 w15">
										<img src="/images/2011/content/sch_txt.gif" alt="Search" title="Search">
									</p>
										
									<table class="fl schBoxGrid w75" summary="">
										<caption></caption>
										<colgroup>
											<col width="12%">
											<col width="21%">
											<col width="12%">
											<col width="30%">
											<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row"><label for="srchApplyType">신청서구분</label></th>
												<td>
													<select id="srchApplyType" name="srchApplyType">
														<option value="">전체 ----------</option>
														<c:forEach items="${applyTypeList}" var="applyTypeList">
															<option value="${applyTypeList.code }" <c:if test="${srchParam.srchApplyType == applyTypeList.code }">selected</c:if>>${applyTypeList.codeName }</option>
														</c:forEach>
													</select>
												</td>
												<th scope="row"><label for="srchApplyFrDt">신청일자</label></th>
												<td>
													<input type="text" id="srchApplyFrDt" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }" title="신청일자(시작)" dateCheck class="w25"> 
													<img title="신청일자 시작일시를 선택하세요." alt="" class="vmid" align="middle" style="cursor:pointer;" 
														onclick="javascript:fn_cal('frm','srchApplyFrDt');"
														src="/images/2011/common/calendar.gif"> ~
													<input type="text" id="srchApplyToDt" name="srchApplyToDt" value="${srchParam.srchApplyToDt }" title="신청일자(종료)" dateCheck class="w25 ml10"> 
													<img title="신청일자 마지막일시를 선택하세요." alt="" class="vmid" align="middle" style="cursor:pointer;"
														onclick="javascript:fn_cal('frm','srchApplyToDt');"
														src="/images/2011/common/calendar.gif">
												</td>
											</tr>
											<tr>
												<th scope="row"><label for="srchApplyWorksTitl">제호(제목)</label></th>
												<td>
													<input type="text" id="srchApplyWorksTitl" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }" class="w80">
												</td>
												<th scope="row"><label for="srchStatCd">진행상태</label></th>
												<td>
													<select id="srchStatCd" name="srchStatCd">
														<option value="">전체 ----------</option>
														<c:forEach items="${statCdList}" var="statCdList">
															<option value="${statCdList.code }" <c:if test="${srchParam.srchStatCd == statCdList.code }">selected</c:if>>${statCdList.codeName }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
										</tbody>
									</table>
										
									<p class="fl btn_area pt25">
										<input type="image" src="/images/2011/button/sch.gif" alt="검색" title="검색"
											onClick="fn_search();" onKeyPress="fn_search();">
									</p>
								</div>
									
								<span class="btmRound lftTop"></span>
								<span class="btmRound rgtTop"></span>
								<span class="btmRound"></span>
								<span class="btmRound rgt"></span>
							</div>
						</fieldset>
						<!-- 테이블 리스트 Set -->
						<div class="section mt20">
							<div class="result_area floatDiv">
								<p class="tab fl"><span class="tab2"><strong class="orange">${totalRow}</strong>건 검색</span></p>
								<p class="fr"><a href="#1" onclick="fn_doDelete();"><img src="/images/2011/button/delete.gif" alt="삭제" /></a></p>
							</div>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
									<col width="8%">
									<col width="15%">
									<col width="15%">
									<col width="*">
									<col width="12%">
									<col width="15%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">
											<input type="checkbox" class="vmid" 
												onclick="javascript:checkBoxToggle('frm','chk',this);" 
												onkeypress="javascript:checkBoxToggle('frm','chk');" title="전체선택" />
										</th>
										<th scope="col">신청서번호</th>
										<th scope="col">신청서구분</th>
										<th scope="col">제호(제목)</th>
										<th scope="col">진행상태</th>
										<th scope="col">신청일자</th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${empty list}">
									<tr>
										<td colspan="6" class="ce">선택된 목록이 없습니다.</td>
									</tr>
								</c:if>
								<c:if test="${!empty list}">
									<c:forEach items="${list}" var="list">
										<tr>
											<td class="ce">
												<c:if test="${list.STAT_CD == '1' }">
													<input type="checkbox" title="선택" name="chk" value="${list.APPLY_NO }">
												</c:if>
											</td>
											<td>
												<a href="#1"  class="underline black2"
													onclick="fn_goDetail('${list.APPLY_WRITE_YMD }','${list.APPLY_WRITE_SEQ }','${list.STAT_CD }');" 
													onkeypress="fn_goDetail('${list.APPLY_WRITE_YMD }','${list.APPLY_WRITE_SEQ }','${list.STAT_CD }');">${list.APPLY_NO }</a>
											</td>
											<td>${list.APPLY_TYPE_NM }</td>
											<td>${list.APPLY_WORKS_TITL }</td>
											<td class="ce">${list.STAT_CD_NM }</td>
											<td class="ce">${list.APPLY_DATE }</td>
										</tr>
									</c:forEach>
								</c:if>
								</tbody>
							</table>
						</div>
						</form>
						<!-- //그리드스타일 -->
							
						<!-- 페이징 -->
						<div class="pagination">
						   <jsp:include page="../common/PageList_2011.jsp" flush="true">
							  <jsp:param name="totalItemCount" value="${totalRow}" />
								<jsp:param name="nowPage"        value="${srchParam.nowPage}" />
								<jsp:param name="functionName"   value="fn_goPage" />
								<jsp:param name="listScale"      value="" />
								<jsp:param name="pageScale"      value="" />
								<jsp:param name="flag"           value="M01_FRONT" />
								<jsp:param name="extend"         value="no" />
							</jsp:include>
						</div>
						<!-- //페이징 -->
							
						<div class="btnArea">
							<p class="rgt">
								<span class="button medium">
									<a href="#1" onclick="fn_goSetp1('new');" onkeypress="fn_goSetp1('new');">법정허락 신청</a>
								</span>
							</p>
						</div>
							
					</div>
					<!-- //테이블 리스트 Set -->
											
				</div>
			</div>
		<!-- //주요컨텐츠 end -->
		</div>
	</div>
	<!-- //CONTAINER end -->
		
	<!-- FOOTER str-->
	<jsp:include page="/include/2011/footer.jsp" />
	<!-- //FOOTER end -->
	<!-- //전체를 감싸는 DIVISION -->
	
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>
