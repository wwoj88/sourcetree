<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>MyWeb~~</title>
<link href="/style/base.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.8.18.custom.min.js"></script>
<script>
	$(function(){
		//변수 지정
		var array = ['naver.com','hanmail.net','nate.com','직접입력'];
		
		function selectData(){
			$("#output").empty();
			$.ajax({
				url:'/user/userList.do',
				cache:false,
				success:function(data){
					$(data).each(function(index,item){
						var output ='';
						output += "<tr>";
						output += "<td class='sub_num'>"+item.user_number+"</td>";
						output += "<td class='sub_id'>"+item.user_idnt+"</td>";
						output += "<td class='sub_name'>"+item.user_name+"</td>";
						output += "<td class='sub_addr'>"+item.addr+"</td>";
						output += "<td class='sub_phone'>"+item.telx_num+"</td>";
						output += "<td class='sub_email'>"+item.email+"</td>";
						output += "<td class='sub_date'>"+item.rgst_dttm+"</td>";
						output += "</tr>";
						$('#output').append(output);
					});
				}
			});
		}
		//이벤트 지정
		
		$(".reset").click(function(event){
			event.preventDefault();
			$("#insert_form").each(function(){
				this.reset();
			});
		});
		
		$(".summit").click(function(event){
			event.preventDefault();
			if($('#user_idnt').val() == ''){
				alert("Input id!!");
				$('#user_idnt').focus();
				return;
			}
			
			if($('#pswd').val() == ''){
				alert("Input pswd!!");
				$('#pswd').focus();
				return;
			}
			
			if($('#pswd').val() != $('#pswd_confirm').val()){
				alert("not equal Password");
				$('#pswd_confirm').focus();
				return;
			}
			
			if($('#pswd').val() != $('#pswd_confirm').val()){
				alert("not equal Password");
				$('#pswd_confirm').focus();
				return;
			}
			
			if($("#email1").val() == ''){
				alert("Input email!!");
				$("#email1").focus();
				return;
			}
			
			if($("#email2").val() == ''){
				alert("Select domain!!");
				$("#mail").focus();
				return;
			}
			
			if($('#info_open_yn').val() == 0){
				alert("Choice grade!!");
				$('#info_open_yn').focus();
				return;
			}
			
			
			$("#insert_form").trigger('submit');
		});
		
		$("#insert_form").submit(function(event){
			//생년월일
			var birthday = $('#birth1').val()+$('#birth2').val()+$('#birth3').val();
			$('#birthday').val(birthday);
			
			//전화번호
			var telx_num = $('#tel1').val()+$('#tel2').val()+$('#tel3').val();
			$('#telx_num').val(telx_num);
			
			//휴대전화
			var mobl_num = $('#mtel1').val()+$('#mtel2').val()+$('#mtel3').val();
			$('#mobl_num').val(mobl_num);
			
			//이메일
			var email = $('#email1').val()+$('#email2').val();
			$('#email').val(email);
			
			var data = $(this).serialize();
			alert(data);
			$.post('/user/insert.do',data,selectData);
			event.preventDefault();
		});
		
		$("#mail").change(function(){
			$(this).prev().val($(this).val());
		});
		
		$(".birth1").change(function(){
			$("#yyyymmdd>select:eq(2)").empty();
			var days = new Date(new Date($(this).val(),$(".birth2").val(),1)-86400000).getDate();
			setDaySelect(1,days,1);
		});
		
		$(".birth2").change(function(){
			$("#yyyymmdd>select:eq(2)").empty();
			var days = new Date(new Date($(".birth1").val(),$(this).val(),1)-86400000).getDate();
			setDaySelect(1,days,1);
		});
		

		$.each(array,function(index){
			if(index==3){
				$('<option>').val('').html(array[index]).appendTo('#mail');				
			}else{
				$('<option>').val(array[index]).html(array[index]).appendTo('#mail');
			}
		});
		
		function setYearSelect(s,p,v){
			var tday = new Date();
			if(!s){
				s = tday.getFullYear(); //시작되는 년도
			}
			if(!p){
				p = 100; //출력되는 갯수이다.
			}
			if(!v){
				v = (new Date).getFullYear();
			}
			$("#yyyymmdd>select:eq(0)").css("size","234").each(function(index){
				if($(this).get(index).tagName=='SELECT'){
					for(var i=s;i>s-p;i--){
						if(i == v || v == i)
							$('<option>').attr("selected", "true").val(i).html(i).appendTo($(this).get(index));
						else
							$('<option>').val(i).html(i).appendTo($(this).get(index));
					};
				};
			});
		}
		
		function setMonthSelect(s,p,v){
			if(!s){
				s = 1; //시작되는 월
			}
			if(!p){
				p = 12; //출력되는 갯수이다.
			}
			if(!v){
				v = 1;
			}
			$("#yyyymmdd>select:eq(1)").each(function(index){
				if($(this).get(index).tagName=='SELECT'){
					for(var i=s;i<=p;i++){
						if(i == v || v == i)
							$('<option>').attr("selected", "true").val(i).html(i).appendTo($(this).get(index));
						else
							$('<option>').html(i).val(i).appendTo($(this).get(index));
					};
				};
			});
		}
		
		function setDaySelect(s,p,v){
			if(!s){
				s = 1; //시작되는 월
			}
			if(!p){
				p = 31; //출력되는 갯수이다.
			}
			if(!v){
				v = 1;
			}
			$("#yyyymmdd>select:eq(2)").each(function(index){
				if($(this).get(index).tagName=='SELECT'){
					for(var i=s;i<=p;i++){
						if(i == v || v == i)
							$('<option>').attr("selected", "true").val(i).html(i).appendTo($(this).get(index));
						else
							$('<option>').val(i).html(i).appendTo($(this).get(index));
					};
				};
			});
		}
		//selectData();
		setYearSelect();
		setMonthSelect();
		setDaySelect();
	});
</script>
</head>
<body>
	<!-- HEADER str--> 
	<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
	<%-- <jsp:include page="/include/header.jsp" /> --%>
	<!-- GNB setOn 각페이지에 넣어야합니다. -->
	<!-- HEADER end -->
	<div id="contents">
		<div id="contentBox">
			<div class="outerBox">
				<div class="innerBox">
					<h3 class="templeteTitle mar_t_0">회원가입</h3>
					<form id="insert_form">
						<input type="hidden" name="birthday" id="birthday" value="" />
						<input type="hidden" name="telx_num" id="telx_num" value="" />
						<input type="hidden" name="mobl_num" id="mobl_num" value="" />
						<input type="hidden" name="email" id="email" value="" />
						<fieldset class="f_">
							<div class="form_table">
								<table border="1" cellspacing="0" summary="표의 요약을 반드시 넣어 주세요">
									<tbody>
										<tr>
											<th scope="row">
												<label for="user_name">
													<img src="/images/membership/tb_name.jpg" />
												</label>
											</th>
											<td>
												<div class="item">
													<input class="i_text" title="레이블 텍스트" type="text" name="user_name" id="user_name">
												</div>
											</td>
										</tr>
										<tr>
											<th scope="row">
													<label for="nick_name">
														<img src="/images/membership/tb_nick.jpg" />
													</label>	
												</th>
											<td>
												<div class="item">
													<input class="i_text" title="레이블 텍스트" type="text" name="nick_name" id="nick_name" >
												</div>
												
											</td>
										</tr>
										<tr>
											<th scope="row">
												<label for="user_idnt">
													<img src="/images/membership/tb_id.jpg" />
												</label>
											</th>
											<td>
												<div class="item">
													<input class="i_text" title="레이블 텍스트" type="text" name="user_idnt" id="user_idnt">
												</div>
											</td>
										</tr>
										<tr>
											<th scope="row">
												<label for="pswd">
													<img src="/images/membership/tb_pwd.jpg" />
												</label>
											</th>
											<td>
												<div class="item">
													<input class="i_text" title="레이블 텍스트" type="password" name="pswd" id="pswd">
												</div>
											</td>
										</tr>
										<tr>
											<th scope="row">
												<label for="pswd_confirm">
													<img src="/images/membership/tb_pwd_.jpg" />
												</label>
											</th>
											<td>
												<div class="item">
													<input class="i_text" title="레이블 텍스트" type="password" name="pswd_confirm" id="pswd_confirm">
												</div>
											</td>
										</tr>
										<tr>
											<th scope="row">
												<label for="resd_num">
													<img src="/images/membership/tb_ssn.jpg" />
												</label>	
											</th>
											<td>
												<div class="item">
													<input class="i_text" title="레이블 텍스트" type="text" name="resd_num" id="resd_num">
												</div>
											</td>
										</tr>
										<tr>
											<th scope="row">
												<label for="birth1">
													<img src="/images/membership/tb_birth.jpg" />
												</label>
											</th>
											<td>
												<div class="item" id="yyyymmdd">
													<select name="year" class="birth1" id="birth1">
													</select>
													 년 <select name="month" class="birth2" id="birth2">
													</select>
													 월 <select name="day" class="birth3" id="birth3">
													</select>
													 일
													<input id="c1" class="i_radio" value="1" type="radio" name="birth_yn" checked="checked"><label for="c1">양력</label>
													<input id="c2" class="i_radio" value="2" type="radio" name="birth_yn"><label for="c2">음력</label>
												</div>
											</td>
										</tr>
										<tr>
											<th scope="row">
												<label for="addr">
												<img src="/images/membership/tb_addr.jpg" />
												</label>	
											</th>
											<td>
												<div class="item">
													<input class="i_text_addr" title="레이블 텍스트" type="text" name="addr" id="addr"><br>
												</div>
											</td>
										</tr>
										<tr>
											<th scope="row">
												<label for="tel1">
													<img src="/images/membership/tb_phone.jpg" />
												</label>
											</th>
											<td>
												<div class="item">
													<input class="i_text_phone" title="레이블 텍스트" type="text" name="tel1" id="tel1"> - 
													<input class="i_text_phone" title="레이블 텍스트" type="text" name="tel2" id="tel2"> - 
													<input class="i_text_phone" title="레이블 텍스트" type="text" name="tel3" id="tel3">
												</div>
											</td>
										</tr>
										<tr>
											<th scope="row">
												<label for="mtel1">
													<img src="/images/membership/tb_phone.jpg" />
												</label>
											</th>
											<td>
												<div class="item">
													<input class="i_text_phone" title="레이블 텍스트" type="text" name="mtel1" id="mtel1"> -  
													<input class="i_text_phone" title="레이블 텍스트" type="text" name="mtel2" id="mtel2"> - 
													<input class="i_text_phone" title="레이블 텍스트" type="text" name="mtel3" id="mtel3">
												</div>
											</td>
										</tr>
										<tr>
											<th scope="row">
												<label for="email1">
													<img src="/images/membership/tb_email.jpg" />
												</label>
											</th>
											<td>
												<div class="item">
													<input class="i_text_mail" title="레이블 텍스트" type="text" name="email1" id="email1"> @ 
													<input class="i_text_mail" title="레이블 텍스트" type="text" name="email2" id="email2">
													<select name="mail" id="mail">
														<option selected>메일주소선택</option>
													</select>
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="2"></td>
										</tr>
										<tr>
											<th scope="row">
												<label for="info_open_yn">
													회원 등급
												</label>
											</th>
											<td>
												<div class="item">
													<select name="info_open_yn" class="info_lv" id="info_open_yn">
														<option value="0" selected>선택</option>
														<option value="1">1등급</option>
														<option value="2">2등급</option>
														<option value="3">3등급</option>
													</select>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</fieldset>
						<div class="btn_membership">
							<a href=""><img src="/images/membership/btn_reset.jpg" class="reset" /></a>
							<a href=""><img src="/images/membership/btn_summit.jpg" class="summit" /></a>
						</div>
					</form>
					<h3 class="templeteTitle">회원 리스트</h3>
					<table class="sub4_bbs_list">
						<caption>게시판 리스트</caption>
						<thead>
							<tr class="title">
								<th class="title_num">
								No.
								</th>
								<th class="title_id">
								Id
								</th>
								<th class="title_name">
								Name
								</th>
								<th class="title_addr">
								Addr
								</th>
								<th class="title_phone">
								Mobile
								</th>
								<th class="title_email">
								Email
								</th>
								<th class="title_date">
								Date
								</th>
							</tr>
						</thead>
						<tbody id="output">
							
						</tbody>
					</table>
					<!-- <div class="paginate_regular">
						<a href="#" class="bbs_direction_left"><img
							src="/images/btn_left_list_bbs.png" /> <a href="#"
							class="bbs_d_list">1</a> <a href="#" class="bbs_d_list">2</a> <a
							href="#" class="bbs_d_list">3</a> <a href="#" class="bbs_d_list">4</a>
							<a href="#" class="bbs_d_list">5</a> <a href="#"
							class="bbs_d_list">6</a> <a href="#" class="bbs_d_list">7</a> <a
							href="#" class="bbs_d_list">8</a> <a href="#" class="bbs_d_list">9</a>
							<a href="#" class="bbs_d_list">10</a> <a href="#"
							class="bbs_direction_right"><img
								src="/images/btn_right_list_bbs.png" /> </a>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</body>
</html>