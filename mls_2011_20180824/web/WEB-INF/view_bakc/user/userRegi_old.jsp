<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
 	String userDivs = request.getParameter("userDivs") == null ? "" : request.getParameter("userDivs");
	String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName");
	//String resdCorpNumb = request.getParameter("resdCorpNumb") == null ? "" : request.getParameter("resdCorpNumb");
	String resdCorpNumb1 = request.getParameter("resdCorpNumb1") == null ? "" : request.getParameter("resdCorpNumb1");
	String resdCorpNumb2 = request.getParameter("resdCorpNumb2") == null ? "" : request.getParameter("resdCorpNumb2");

	String corpNumb = request.getParameter("corpNumb") == null ? "" : request.getParameter("corpNumb");

	//String tmpResdCorpNumb = "";
	String tmpCorpNumb = "";
	String userDivsName = "";

	if ("01".equals(userDivs)) {
	  userDivsName = "개인회원";
	  //tmpResdCorpNumb = resdCorpNumb.substring(0,6) + "-" + resdCorpNumb.substring(6,13);
  } else if ("02".equals(userDivs)) {
  	userDivsName = "법인사업자";
  	//tmpResdCorpNumb = resdCorpNumb.substring(0,6) + "-" + resdCorpNumb.substring(6,13);
  	tmpCorpNumb     = corpNumb.substring(0,3) + "-" + corpNumb.substring(3,6) + "-" + corpNumb.substring(6,10);
  } else if ("03".equals(userDivs)) {
  	userDivsName = "개인사업자";
  	tmpCorpNumb     = corpNumb.substring(0,3) + "-" + corpNumb.substring(3,6) + "-" + corpNumb.substring(6,10);
  }
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작물 내권리찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
<script src="/js/Function.js" type="text/javascript"></script>
<script type="text/JavaScript">
<!--
function goPostNumbSrch() {
  //window.open('/user/user.do?method=goPostNumbSrch','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=445');
	var pop = window.open("/jusoPopup.jsp?method=goPostNumbSrch","pop","width=570,height=420, scrollbars=yes, resizable=yes");
  
}

function goIdntConf() {
	var frm = document.form1;

	if (frm.userIdnt.value == "") {
		alert("사용하실 아이디를 입력하십시오.");
		frm.userIdnt.focus();
		return;
	} else {
  	var userIdnt = frm.userIdnt.value;
	  window.open('/user/user.do?method=goIdntConf&userIdnt='+userIdnt,'win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=445');
	}
}

function insertUser() {
	var frm = document.form1;

  if (frm.userIdChk.value != "Y") {
  	alert("아이디 중복확인을 하시기 바립니다.");
		return;
  }else if (frm.pswd.value == "") {
		alert("비밀번호를 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value.length < 6) {
		alert("비밀번호는 6자리 이상 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value != frm.pswdConf.value) {
		alert("입력하신 비밀번호가 다릅니다. 입력하신 비밀번호를 확인하십시오");
		frm.pswd.focus();
		return;
	} else if (frm.mail.value == "") {
		alert("이메일 주소를 입력하십시오.");
		frm.mail.focus();
		return;
  } else if (frm.zipxNumb.value == "") {
		alert("우편번호를 입력하십시오.");
		frm.zipxNumb.focus();
		return;
  } else if (frm.addr.value == "") {
		alert("주소를 입력하십시오.");
		frm.addr.focus();
		return;
  } else if (frm.detlAddr.value == "") {
		alert("상세주소를 입력하십시오.");
		frm.detlAddr.focus();
		return;
  } else if (frm.moblPhon.value == "") {
		alert("핸드폰번호를 입력하십시오.");
		frm.moblPhon.focus();
		return;
  } else if ((inspectCheckBoxField(frm.mailReceYsno) == false) && (inspectCheckBoxField(frm.smsReceYsno) == false)) {
		alert("수신방법 중 이메일과 핸드폰 하나는 선택하셔야 합니다.");
		frm.mailReceYsno.focus();
		return;
	} else {
		frm.action = "/user/user.do";
  	frm.submit();
	}
}

function jusoCallBack(roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn){
	// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
	document.form1.addr.value = roadAddrPart1;
	document.form1.detlAddr.value = addrDetail;
	document.form1.zipxNumb.value = zipNo;
}
//-->
</script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp"/>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','122');
			</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="memberTlt">회원</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>회원</li>
					<li class="on">회원가입 신청</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>회원가입 신청</h2>
			<div class="tip R">(<img src="/images/common/ic_necessary.gif" width="10" height="7" /> ) 항목은 필수입력사항이므로 빠짐없이 기입하여 주시기 바랍니다.</div>
			<table width="710" class="board">
				<colgroup>
				<col width="130" />
				<col width="" />
				</colgroup>
				<caption summary="회원가입 항목을 입력하고, 회원가입을 한다">회원가입 항목입력</caption>
				<tbody class="ViewBody">
				<form name="form1" method="post">
					<input type="hidden" name="zip">
					<input type="hidden" name="srchAddr">
					<input type="hidden" name="userDivs" value="<%=userDivs%>">
					<input type="hidden" name="userName" value="<%=userName%>">
					<!-- input type="hidden" name="resdCorpNumb" value="<%//=resdCorpNumb%>" -->
					<input type="hidden" name="corpNumb" value="<%=corpNumb%>">
					<input type="hidden" name="method" value="goUserInsert">
					<tr>
						<th>회원구분</th>
						<td><%=userDivsName%></td>
					</tr>
					<tr>
						<th>이름/사업자명</th>
						<td><%=userName%></td>
					</tr>
					<tr>
						<th>주민/법인번호</th>
						<td>
						<%//=tmpResdCorpNumb%>
						<%
						  if ("01".equals(userDivs) || "02".equals(userDivs)) {
					    %>
							<input type="text" name="resdCorpNumb1" id="resdCorpNumb1"  value="<%=resdCorpNumb1%>" class="input" style="border-width:0px; width:39px; text-align:right;" readonly>-<input type="text" name="resdCorpNumb2" id="resdCorpNumb2"  value="<%=resdCorpNumb2%>" class="input" style="border-width:0px; width:45px; text-align:left;" readonly>
						<%
						  } 
						%>
						</td>
						<!--td><input type="text" name="resdCorpNumb" id="resdCorpNumb" value="<%//=resdCorpNumb%>" class="input" readonly /></td-->
					</tr>
					<tr>
						<th><label for="comNum">사업자번호</label></th>
						<td><%=tmpCorpNumb%></td>
					</tr>
					<tr>
						<th class="necessary"><label for="userIdnt">아이디</label></th>
						<td><input type="text" name="userIdnt" id="userIdnt" class="input" />&nbsp;<a href="javascript:goIdntConf();" title="새 창에서 열림"><img src="/images/button/overlap_btn.gif" alt="중복확인" width="67" height="20" /></a>
							  <input type="hidden" name="userIdChk">
						</td>
					</tr>
					<tr>
						<th class="necessary"><label for="pswd">비밀번호</label></th>
						<td><input type="password" name="pswd" id="pswd" class="input" /> &nbsp; (6자리 이상으로 입력하십시오.)</td>
					</tr>
					<tr>
						<th class="necessary"><label for="pswdConf">비밀번호 확인</label></th>
						<td><input type="password" name="pswdConf" id="pswdConf" class="input" /></td>
					</tr>
					<tr>
						<th class="necessary"><label for="mail">이메일 주소</label></th>
						<td><input type="text" name="mail" id="mail" class="input" /></td>
					</tr>
					<tr>
						<th class="necessary"><label for="zipxNumb">우편번호</label></th>
						<td><input type="text" name="zipxNumb" id="zipxNumb" class="input" readonly/>&nbsp;<a href="javascript:goPostNumbSrch();" title="새 창에서 열림"><img src="/images/button/postnum_btn.gif" alt="우편번호찾기" width="67" height="20" /></a></td>
					</tr>
					<tr>
						<th class="necessary"><label for="addr">주소</label></th>
						<td><input type="text" class="input" name="addr" id="addr" size="88" readonly/></td>
					</tr>
					<tr>
						<th class="necessary"><label for="detlAddr">상세주소</label></th>
						<td><input name="detlAddr" type="text" class="input" id="detlAddr" size="88" /></td>
					</tr>
					<tr>
						<th><label for="telxNumb">자택전화번호</label></th>
						<td><input type="text" name="telxNumb" id="telxNumb" class="input" />&nbsp;(예:02-123-4567)</td>
					</tr>
					<tr>
						<th class="necessary"><label for="moblPhon">핸드폰번호</label></th>
						<td><input type="text" name="moblPhon" id="moblPhon" class="input" />&nbsp;(예:010-1234-5678)</td>
					</tr>
					<tr>
						<th>수신방법</th>
						<td><input type="checkbox" name="mailReceYsno" id="mailReceYsno" value="Y" class="chk"/><label for="email2">이메일</label>&nbsp;<input type="checkbox" name="smsReceYsno" id="smsReceYsno" value="Y" class="chk"/><label for="hpNum">핸드폰</label></td>
					</tr>
				</form>
				</tbody>
			</table>
			<!--buttonArea start-->
			<div id="buttonArea">
				<div class="floatR"><a href="javascript:insertUser();"><img src="/images/button/join_btn.gif" alt="회원가입" width="76" height="23" /></a> <a href="/main/main.do"><img src="/images/button/cancle2_btn.gif" alt="취소" width="76" height="23" /></a></div>
			</div>
			<!--buttonArea end-->
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
	<%-- <jsp:include page="/include/bottom.jsp"/> --%>
	<!--하단영역 end-->
</div>
</body>
</html>
