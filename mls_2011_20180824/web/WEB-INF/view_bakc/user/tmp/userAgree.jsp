<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
 	String userDivs = request.getParameter("userDivs") == null ? "" : request.getParameter("userDivs");
	String userName = request.getParameter("userName") == null ? "" : request.getParameter("userName");
	//String resdCorpNumb = request.getParameter("resdCorpNumb") == null ? "" : request.getParameter("resdCorpNumb");
	String resdCorpNumb1 = request.getParameter("resdCorpNumb1") == null ? "" : request.getParameter("resdCorpNumb1");
	String resdCorpNumb2 = request.getParameter("resdCorpNumb2") == null ? "" : request.getParameter("resdCorpNumb2");
	String corpNumb = request.getParameter("corpNumb") == null ? "" : request.getParameter("corpNumb");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>회원가입 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--

function goPostNumbSrch() {
	  window.open('/user/user.do?method=goPostNumbSrch','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=445');
	}
		
function agreeCheck(){
	var frm = document.form1;

	if(!frm.agree.checked){
		alert("약관을 읽어보시고, 동의해 주시기 바랍니다.");
	} else {
 	  frm.method = "post";
  	frm.action = "/user/user.do?method=goUserRegi";
  	frm.submit();
  }
}

function getSelectedRadio(buttonGroup) {
   if (buttonGroup[0]) {
      for (var i=0; i<buttonGroup.length; i++) {
         if (buttonGroup[i].checked) {
            return i
         }
      }
   } else {
      if (buttonGroup.checked) { return 0; }
   }
   return -1;
}

function getSelectedRadioValue(buttonGroup) {
    var i = getSelectedRadio(buttonGroup);
   if (i == -1) {
      return "";
   } else {
      if (buttonGroup[i]) {
         return buttonGroup[i].value;
      } else {
         return buttonGroup.value;
      }
   }
} // Ends the "getSelectedRadioValue" function
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_9.gif" alt="회원정보" title="회원정보" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/user/user.do?method=goLogin"><img src="/images/2011/content/sub_lnb0901_off.gif" title="로그인" alt="로그인" /></a></li>
					<li id="lnb2"><a href="/user/user.do?method=goPage"><img src="/images/2011/content/sub_lnb0902_off.gif" title="회원가입" alt="회원가입" /></a></li>
					<li id="lnb3"><a href="/user/user.do?method=goIdntPswdSrch"><img src="/images/2011/content/sub_lnb0903_off.gif" title="아이디/비밀번호찾기" alt="아이디/비밀번호찾기" /></a></li>
					</ul>
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb2");
					</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>회원정보</span><em>회원가입</em></p>
					<h1><img src="/images/2011/title/content_h1_0902.gif" alt="회원가입" title="회원가입" /></h1>
					
					<div class="section">
						
						<div class="usr_process">
							<div class="process_box">
								<ul class="floatDiv">
								<li class="fl ml0 on"><img src="/images/2011/content/process1_on.gif" alt="1단계 약관동의" /></li>
								<li class="fl"><img src="/images/2011/content/process2_off.gif" alt="2단계 회원정보입력" /></li>
								<li class="fl bgNone pr0"><img src="/images/2011/content/process3_off.gif" alt="3단계 가입완료" /></li>
								</ul>
							</div>
						</div>
						<form name="form1" class="frm" action="">
							<input type="hidden" name="userDivs" value="<%=userDivs%>">
							<input type="hidden" name="userName" value="<%=userName%>">
							<!-- input type="hidden" name="resdCorpNumb" value="<%//=resdCorpNumb%>"-->
							<input type="hidden" name="resdCorpNumb1" value="<%=resdCorpNumb1%>">
							<input type="hidden" name="resdCorpNumb2" value="<%=resdCorpNumb2%>">
							<input type="hidden" name="corpNumb" value="<%=corpNumb%>">
						<h2>이용약관</h2>
						
						<div class="white_box mt0">
							<div class="box5">
								<div class="box5_
								con">
									<div class="stipulation h200">
										<jsp:include page="/include/2010/stipulation.txt"/>
									</div>
									<p class="ce mt5"><input type="checkbox" id="yes" name="agree" value="Y" /><label for="yes">위 약관에 동의합니다.</label></p>
								</div>
							</div>
						</div>
						
						<div class="btnArea">
							<p class="fl"><span class="button medium gray"><a href="/main/main.do">취소</a></span></p>
							<p class="fr"><span class="button medium"><a href="javascript:agreeCheck();">확인</a></span></p>
						</div>
						</form>
								
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
