<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>회원가입 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--
function init() {
	var frm = document.form1;
	var userDivs = frm.userDivs.value;

	if (userDivs == "01") {
		frm.corpNumb1.disabled = true;
		frm.corpNumb2.disabled = true;
		frm.corpNumb3.disabled = true;
		frm.resdCorpNumb1.disabled = false;
		frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "02") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
  	frm.resdCorpNumb1.disabled = false;
  	frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "03") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
		frm.resdCorpNumb1.disabled = true;
		frm.resdCorpNumb2.disabled = true;
  }
  frm.userName.focus();

  //frm.reset();
}

// 사업자등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginCrnChk(obj){
	// 사업자등록번호 앞자리의 길이를 확인하여 3자리, 2자리가 입력되면 다음 객체로 이동
	if(obj.name == "corpNumb1"){
		if(obj.value.length == 3)	document.form1.corpNumb2.focus();
	} else if(obj.name == "corpNumb2"){
		if(obj.value.length == 2)	document.form1.corpNumb3.focus();
	}
}

// 주민등록번호 입력 길이를 확인 다음 객체로 자동이동하도록 설정
// Enter Key 입력시 로그인 수행
function fn_signLoginSsnChk(obj){
	// 주민등록번호 앞자리의 길이를 확인하여 6자리가 입력되면 다음 객체로 이동
	if(obj.name == "resdCorpNumb1"){
		if(obj.value.length == 6)	document.form1.resdCorpNumb2.focus();
	}

	// EnterKey 입력시 공인인증서 로그인 수행
	if (event.keyCode == 13) {
		fn_signLogin();
	}
}

function fn_signLogin() {
	var frm = document.form1;
	var userDivs = frm.userDivs.value;

  if (Trim(frm.userName.value) == "") {
  	alert("이름/법인명을 입력하십시오.");
  	frm.userName.focus();
  	return;
  } else if (userDivs == "01") {
  	if (frm.resdCorpNumb1.value == "") {
  		alert("주민/법인번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb1.value.length != 6) {
  		alert("주민/법인번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value == "") {
  		alert("주민/법인번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb2.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value.length != 7) {
  		alert("주민/법인번호 앞자리는 7자리 입니다.");
  		frm.resdCorpNumb2.focus();
  		return;
  	}
		ssn1 = frm.resdCorpNumb1.value;
		ssn2 = frm.resdCorpNumb2.value;
		frm.resdCorpNumb.value = frm.resdCorpNumb1.value + frm.resdCorpNumb2.value;

		if(ssnCheck(ssn1, ssn2)) {              // 주민번호 체크
			return;
		}
  } else if (userDivs == "02") {
  	if (frm.resdCorpNumb1.value == "") {
  		alert("주민/법인번호 앞자리를 입력하십시오.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb1.value.length != 6) {
  		alert("주민/법인번호 앞자리는 6자리 입니다.");
  		frm.resdCorpNumb1.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value == "") {
  		alert("주민/법인번호 뒷자리를 입력하십시오.");
  		frm.resdCorpNumb2.focus();
  		return;
  	} else if (frm.resdCorpNumb2.value.length != 7) {
  		alert("주민/법인번호 앞자리는 7자리 입니다.");
  		frm.resdCorpNumb2.focus();
  		return;
  	} else if (frm.corpNumb1.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb1.focus();
  		return;
    } else if (frm.corpNumb1.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb1.focus();
  		return;
   	} else if (frm.corpNumb2.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb2.focus();
  		return;
    } else if (frm.corpNumb2.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb2.focus();
  		return;
   	} else if (frm.corpNumb3.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb3.focus();
  		return;
    } else if (frm.corpNumb3.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb3.focus();
  		return;
  	}
  	frm.corpNumb.value = frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value;
  	frm.resdCorpNumb.value = frm.resdCorpNumb1.value + frm.resdCorpNumb2.value;

  	if (fncJuriRegNoCheck(frm.resdCorpNumb1.value + frm.resdCorpNumb2.value) == false) {                // 법인번호 체크
  		alert("잘못된 법인번호 입니다.");
  		frm.resdCorpNumb1.focus();
 		  return;
 		}

		if (check_busino(frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb1.focus();
 		  return;
 		}
  } else if (userDivs == "03") {
  	if (frm.corpNumb1.value == "") {
  		alert("사업자번호 앞자리를 입력하십시오.");
  		frm.corpNumb1.focus();
  		return;
    } else if (frm.corpNumb1.value.length != 3) {
  		alert("사업자번호 앞자리는 3자리 입니다.");
  		frm.corpNumb1.focus();
  		return;
   	} else if (frm.corpNumb2.value == "") {
  		alert("사업자번호 중간 자리를 입력하십시오.");
  		frm.corpNumb2.focus();
  		return;
    } else if (frm.corpNumb2.value.length != 2) {
  		alert("사업자번호 중간 자리는 2자리 입니다.");
  		frm.corpNumb2.focus();
  		return;
   	} else if (frm.corpNumb3.value == "") {
      alert("사업자번호 마지막 자리를 입력하십시오.");
  		frm.corpNumb3.focus();
  		return;
    } else if (frm.corpNumb3.value.length != 5) {
  		alert("사업자번호 마지막 자리는 5자리 입니다.");
  		frm.corpNumb3.focus();
  		return;
  	}
  	
		frm.corpNumb.value = frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value;
		
		if (check_busino(frm.corpNumb1.value + frm.corpNumb2.value + frm.corpNumb3.value) == false) {   // 사업자번호 체크
  		alert("잘못된 사업자번호 입니다.");
  		frm.corpNumb1.focus();
 		  return;
 		}
  }
 	frm.method = "post";
	frm.action = "/user/user.do?method=userRegiCheck";
	frm.submit();
}

function fn_userDivs() {
	var frm = document.form1;
	var userDivs = frm.userDivs.value;

	if (userDivs == "01") {
		frm.corpNumb1.disabled = true;
		frm.corpNumb2.disabled = true;
		frm.corpNumb3.disabled = true;
		frm.resdCorpNumb1.disabled = false;
		frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "02") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
  	frm.resdCorpNumb1.disabled = false;
  	frm.resdCorpNumb2.disabled = false;
  } else if (userDivs == "03") {
  	frm.corpNumb1.disabled = false;
		frm.corpNumb2.disabled = false;
		frm.corpNumb3.disabled = false;
		frm.resdCorpNumb1.disabled = true;
		frm.resdCorpNumb2.disabled = true;
  }
  frm.userName.focus();

  frm.userName.value = "";
  frm.resdCorpNumb1.value = "";
  frm.resdCorpNumb2.value = "";
  frm.corpNumb1.value = "";
  frm.corpNumb2.value = "";
  frm.corpNumb3.value = "";
}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(6);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_9.gif" alt="회원정보" title="회원정보" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/user/user.do?method=goLogin"><img src="/images/2011/content/sub_lnb0901_off.gif" title="로그인" alt="로그인" /></a></li>
					<li id="lnb2"><a href="/user/user.do?method=goPage"><img src="/images/2011/content/sub_lnb0902_off.gif" title="회원가입" alt="회원가입" /></a></li>
					<li id="lnb3"><a href="/user/user.do?method=goIdntPswdSrch"><img src="/images/2011/content/sub_lnb0903_off.gif" title="아이디/비밀번호찾기" alt="아이디/비밀번호찾기" /></a></li>
					</ul>
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb2");
					</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>회원정보</span><em>회원가입</em></p>
					<h1><img src="/images/2011/title/content_h1_0902.gif" alt="회원가입" title="회원가입" /></h1>
					
					<div class="section">
						
						<h2>회원가입 여부 확인</h2>
						<form class="frm" name="form1" action="">
							<input type="hidden" name="resdCorpNumb">
							<input type="hidden" name="corpNumb">
							<fieldset class="w100">
							<legend>로그인</legend>
								<span class="topLine3"></span>
								<div class="user_area floatDiv">
									<p class="fl"><img src="/images/2011/content/box_img3.gif" alt="" /></p>
									<dl class="fl ml30 mt10">
									<dt><label for="userDivs" class="strong">회원구분</label></dt>
									<dd>
									<select name="userDivs" id="userDivs"  onChange="fn_userDivs();">
										<option value="01">개인회원</option>
							            <option value="02">법인사업자</option>
							            <option value="03">개인사업자</option>
									</select></dd>
									<dt><label for="userName">이름/법인명</label></dt>
										<dd><input type="text" id="userName" name="userName" size="25" /></dd>
										<dt><label for="resdCorpNumb1">주민/법인번호</label></dt>
										<dd><input type="text" id="resdCorpNumb1" name="resdCorpNumb1" title="주민/법인번호(앞자리)" size="8" maxlength="6" onkeypress="cfInputNumRT(event);"/> 
												- 
											<input id="resdCorpNumb2" name="resdCorpNumb2" type="password" title="주민/법인번호(뒷자리)" size="19" maxlength="7" onkeypress="cfInputNumRT(event);" />
										</dd>
										<dt><label for="corpNumb1">사업자번호</label></dt>
										<dd><input id="corpNumb1" name="corpNumb1" title="사업자번호(1번째)" size="7" maxlength="3" onkeypress="cfInputNumRT(event);" disabled/> 
												- 
											<input id="corpNumb2" name="corpNumb2" title="사업자번호(2번째)" size="7" maxlength="2" onkeypress="cfInputNumRT(event);" disabled/> 
												- 
											<input id="corpNumb3" name="corpNumb3" title="사업자번호(3번째)" size="8" maxlength="5" onkeypress="cfInputNumRT(event);" disabled/></dd>
										</dl>
									<p class="fl ml5 mt70"><span class="button large strong"><a href="javascript:fn_signLogin();">확인</a></span></p>
								</div>
							</fieldset>
						</form>
						<div class="comment_box mb30">
							<p class="strong blue2 line15"><img src="/images/2011/common/ic_tip.gif" class="vmid" alt="" /> 알려드립니다.</p>
							<ul class="list1 mt10 ml20">
							<li>이름/법인명: 개인회원은 이름, 법인회원은 법인명 입력</li>
							<li>주민/법인번호: 개인회원은 주민번호, 법인은 법인번호 입력</li>
							<li>사업자번호: 개인사업자는 사업자번호, 개인 및 법인회원은 입력하지 않음</li>
							</ul>
						</div>
										
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
