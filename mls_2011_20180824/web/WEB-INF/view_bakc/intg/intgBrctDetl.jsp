<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html; charset=euc-kr" language="java" errorPage="" %>
<!--[if lte IE 8]> 
<![endif]-->
<%@page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="pragma" content="no-cache"/>
<title>방송음악보상금 정보조회 상세페이지</title>
<link href="/css2/clms.css" rel="stylesheet" type="text/css"/>
<link href="/css2/popup.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<form name="form" method="post">
<div id="wrap">

	<div class="popupContents">		
		<h3>방송음악보상금 정보</h3>
		<table style="width:620px" border="0" cellspacing="0" cellpadding="0" class="grid">
			<tr>
				<th class="tdLabel" style="width:120px">음원명</th>
				<td class="tdData" colspan="3">${BrctDetail[0].sdsrName}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">가수명</th>
				<td class="tdData" colspan="3">${BrctDetail[0].muciName}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">앨범명</th>
				<td class="tdData" colspan="3">${BrctDetail[0].albmName}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">작사가</th>
				<td class="tdData" colspan="3">${BrctDetail[0].lyriWrtr}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">작곡가</th>
				<td class="tdData" colspan="3">${BrctDetail[0].comsWrtr}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">편곡자</th>
				<td class="tdData" colspan="3">${BrctDetail[0].arrgWrtr}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">방송사용년도 </th>
				<td class="tdData" style="width:202px">${BrctDetail[0].yymm}&nbsp;</td>
				<th class="tdLabel" style="width:120px">방송횟수</th>
				<td class="tdData">${BrctDetail[0].brctCont}&nbsp;</td>
			</tr>
			<tr>
				<th class="tdLabel" style="width:120px">제공업체</th>
				<td class="tdData" colspan="3">${BrctDetail[0].oferEtpr}&nbsp;</td>
			</tr>
		</table>

	</div>
	<!-- end:paging -->		
	<div class="popupBottom"><img src="/images2/button/close_btn.gif" alt="닫기" width="76" height="23" onclick="javascript:window.close();" style="cursor:hand;"/></div>
</div>
<input type="submit" style="display:none;">
</form>
</body>
</html>

