<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>저작권 정보 확인 서비스 이용 - 음악상세(${music.TITLE}) | 내권리찾기 | 저작권찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css"> 
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/deScript.js"></script>
<style type="text/css">
	.mt3{
		margin-top:3px;
	}
</style>
</head>

<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>음악저작물 상세조회</h1>
		</div>
		<!-- //HEADER end -->
	
		<!-- CONTAINER str-->
		<div id="popContents">
			<p class="red p11" ><strong>* 저작권정보 변경신청에 관한 문의는 검색화면의 단체로 연락바랍니다.</strong></p>
			<div class="section">
				<h2 class="mt15">앨범정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="음악저작물의 앨범정보(앨범명, 앨범미디어타입, 앨범형태, 발매일자, 에디션번호, 앨범레이블명, 앨범레이블번호, 음반제작사) 입니다.">
					<caption>음악저작물 앨범정보</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">앨범명</th>
							<td colspan="3">${music.ALBUM_TITLE}</td>
						</tr>
						<tr>
							<th scope="row">앨범미디어타입</th>
							<td>${music.ALBUM_MEDIA_TYPE}</td>
							<th scope="row">앨범형태</th>
							<td>${music.ALBUM_TYPE}</td>
						</tr>
						<tr>
							<th scope="row">발매일자</th>
							<td>${music.ISSUED_DATE}</td>
							<th scope="row">에디션번호</th>
							<td>${music.EDITION}</td>
						</tr>
						<tr>
							<th scope="row">앨범레이블명</th>
							<td>${music.ALBUM_LABLE_NAME}</td>
							<th scope="row">앨범레이블번호</th>
							<td>${music.ALBUM_LABLE_NO}</td>
						</tr>
						<tr>
							<th scope="row">음반제작사</th>
							<td colspan="3">${music.ALBUM_PRODUCED_CRH}</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="section mt20">
				<h2>곡정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="음악저작물의 곡정보(곡명, 디스크번호, 트랙번호, 곡구분, 프로듀서, 실연시간) 입니다.">
					<caption>음악저작물 곡정보</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">곡명</th>
							<td>${music.TITLE}</td>
							<th scope="row">디스크번호</th>
							<td>${music.DISK_NO}</td>
						</tr>
						<tr>
							<th scope="row">트랙번호</th>
							<td>${music.TRACK_NO}</td>
							<th scope="row">곡구분(일반/클래식)</th>
							<td>${music.MUSIC_TYPE}</td>
						</tr>
						<tr>
							<th scope="row">프로듀서</th>
							<td>${music.PRODUCER}</td>
							<th scope="row">실연시간</th>
							<td>${music.PERFORMANCE_TIME}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="section mt20">
				<h2>권리정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="음악저작물의 권리정보(작사, 작곡, 편곡, 한국음악저작권협회) 입니다.">
					<caption>음악저작물 권리정보(저작권)</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="25%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row" >작사</th>
							<td>${fn:replace(music.LYRICIST,",",", ")}</td>
							<th scope="row">작곡</th>
							<td>${fn:replace(music.COMPOSER,",",", ")}</td>
						</tr>
						<tr>
							<th scope="row">편곡</th>
							<td>${music.ARRANGER}</td>
							<th scope="row">한국음악저작권협회</th>
							<td>
								<c:if test="${music.TRUST_YN == 'Y' }">신탁</c:if>
								<c:if test="${music.TRUST_YN == 'N' }">비신탁</c:if>
							</td>
						</tr>
					</tbody>
				</table>
				<table cellspacing="0" cellpadding="0" border="1" class="grid mt3" summary="음악저작물의 권리정보(가창, 연주, 지휘, 한국음악실연자연합회) 입니다.">
					<caption>음악저작물 권리정보(실연자)</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="25%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">가창</th>
							<td>${music.SINGER}</td>
							<th scope="row">연주</th>
							<td>${music.PLAYER}</td>
						</tr>
						<tr>
							<th scope="row">지휘</th>
							<td>${music.CONDUCTOR}</td>
							<th scope="row">한국음악실연자연합회</th>
							<td>
								<c:if test="${music.PAK_TRUST_YN == 'Y' }">신탁</c:if>
								<c:if test="${music.PAK_TRUST_YN == 'N' }">비신탁</c:if>
							</td>
						</tr>
					</tbody>
				</table>
				<table cellspacing="0" cellpadding="0" border="1" class="grid mt3" summary="음악저작물의 권리정보(음반제작사, 한국음반산업협회) 입니다.">
					<caption>음악저작물 권리정보(음잔제작자)</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="25%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">음반제작사</th>
							<td><c:if test="${music.KAPP_TRUST_YN =='Y' }">비공개</c:if></td>
							<th scope="row">한국음반산업협회</th>
							<td>
								<c:if test="${music.KAPP_TRUST_YN == 'Y' }">신탁</c:if>
								<c:if test="${music.KAPP_TRUST_YN == 'N' }">비신탁</c:if>
								<c:if test="${music.KAPP_TRUST_YN == 'B' }">보상금분배</c:if>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div>
			<p class="fl mt20 p11" >* 디지털저작권거래소(kdce.or.kr)에서 저작권 이용계약 서비스를 이용할 수 있습니다. </p>
			<a class="fr mt15"  href="http://www.kdce.or.kr/user/main.do" target="_blank"><img src="/images/2012/button/btn_go_site.gif" alt="바로가기" title="바로가기" /></a><br/>
			</div>
			
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="닫기" title="이 창을 닫습니다." /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>