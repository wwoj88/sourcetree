<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>저작권 정보 확인 서비스 이용 - 영화상세(${movie.TITLE}) | 내권리찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>영화저작물 상세조회</h1>
		</div>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<p class="red p11" ><strong>* 저작권정보 변경신청에 관한 문의는 검색화면의 단체로 연락바랍니다.</strong></p>
			<div class="section">
				<h2 class="mt15">영화정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="영화저작물 정보(영화명, 영화원문명, 장르, 형태, 관람등급, 상영시간, 감독/연출, 출연진) 입니다.">
					<caption>영화 저작물정보</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">영화명</th>
							<td colspan="3">${movie.TITLE}</td>
						</tr>
						<tr>
							<th scope="row">영화원문명</th>
							<td colspan="3">${movie.TITLE_ORG}</td>
						</tr>
						<tr>
							<th scope="row">장르</th>
							<td>${movie.GENRE_VALUE}</td>
							<th scope="row">형태</th>
							<td>${movie.MOVIE_TYPE_VALUE}</td>
						</tr>
						<tr>
							<th scope="row">관람등급</th>
							<td>${movie.VIEW_GRADE_VALUE}</td>
							<th scope="row">상영시간</th>
							<td>${movie.RUN_TIME}</td>
						</tr>
						<tr>
							<th scope="row">감독/연출</th>
							<td>${movie.DIRECTOR}</td>
							<th scope="row">출연진</th>
							<td>${movie.LEADING_ACTOR}</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="section mt20">
				<h2>매체정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="영화저작물의 매체정보(매체, 매체 관람등급, 제작일자, 공표일자) 입니다.">
					<caption>영화 매체정보</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">매체</th>
							<td>${movie.MEDIA_CODE_VALUE}</td>
							<th scope="row">매체관람등급</th>
							<td>${movie.MEDIA_VIEW_GRADE_VIEW}</td>
						</tr>
						<tr>
							<th scope="row">제작일자</th>
							<td>${movie.PRODUCE_DATE}</td>
							<th scope="row">공표일자</th>
							<td>${movie.DISCLOSURE_DATE}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="section mt20">
				<h2>권리정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="영화저작물의 권리정보(제작사, 투자사, 배급사, 한국영화배급협회) 입니다.">
					<caption>영화 권리정보</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">제작사</th>
							<td>${movie.PRODUCER}</td>
							<th scope="row">투자사</th>
							<td>${movie.INVESTOR}</td>
						</tr>
						<tr>
							<th scope="row">배급사</th>
							<td colspan="3">${movie.DISTRIBUTOR}</td>
						</tr>
						<tr>
							<th scope="row">한국영화배급협회</th>
							<td colspan="3">
								<c:if test="${movie.TRUST_YN == 'Y' }">신탁</c:if>
								<c:if test="${movie.TRUST_YN == 'N' }">비신탁</c:if>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="X" title="이 창을 닫습니다." /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
