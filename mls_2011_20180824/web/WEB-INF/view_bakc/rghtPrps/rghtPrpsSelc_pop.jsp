<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>저작권찾기 신청 목적선택|저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/popup.css">
<script type="text/JavaScript">
<!--

// 선택목록 권리찾기신청
function goRghtPrps(PRPS_RGHT_CODE)
{	
	
	//conValue = confirm('선택된 항목을 권리찾기신청합니다.');
	
	//if(conValue) 
	//{		
	    frm = document.hidForm;
	    
		// 부모창에서 신청화면으로 이동한다.
		opener.goRghtPrpsMain('${gubun}', PRPS_RGHT_CODE);
		window.close();
		
//	}
//	else
//	{
//		return;
//	}
		
}

//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop">
		
		<!-- Header str -->
		<div id="popHeader">
			<h1>저작권찾기 신청목적 안내</h1>
		</div>
		<!-- //Header end -->
		
		<!-- Container str -->
		<div id="popContainer">
		
			<div class="popContent">
				<h2>저작권찾기 신청목적 선택</h2>
				<!-- 테이블 영역입니다 -->
				<div class="tabelRound">
					<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
					<table cellspacing="0" cellpadding="0" border="1" class="grid topLine" summary="저작권찾기 목적선택 폼"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<colgroup>
						<col width="*">
						</colgroup>
						<tbody>
							<tr>
								<td>
									<br/>
									<b>* 권리자 저작권찾기</b> <br/>
									  &nbsp;&nbsp;&nbsp;저작물 권리자 찾기를 위한 저작권찾기 신청
									  <a href="#1" onclick="javascript:goRghtPrps('1');"><u><b>권리자의 저작권찾기</b> 저작권찾기신청</u></a>
									  <br/><br/>
									  
									  <b>* 이용자 저작권조회</b> <br/>
									  &nbsp;&nbsp;&nbsp;저작물 이용을 위한 저작권조회 신청
									  <a href="#1" onclick="javascript:goRghtPrps('3');"><u><b>이용자의 저작권조회</b> 저작권찾기신청</u></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //테이블 영역입니다 -->
			</div>
			
			
			
		</div>
		<!-- //Container end -->
		
		<!-- Footer str -->
		<div id="popFooter">
			<p class="copyright">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		</div>
		<!-- Footer end -->
		
		<p class="close"><a href="#1" onclick="javascript:window.close()"><img src="/images/2010/pop/close.gif" alt="X" title="이 창을 닫습니다."/></a></p>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
