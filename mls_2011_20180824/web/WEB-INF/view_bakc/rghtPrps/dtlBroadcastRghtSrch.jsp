<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2012/prototype.js"></script>
<title>권리찾기(방송) | 저작권찾기</title>
<script type="text/JavaScript">
<!--

window.name = "broadcastRghtSrch";

function fn_frameList(){
	var frm = document.ifFrm;

	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=R";
	
	new Ajax.Request('/test', {   
		onLoading: function() {
			//parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {
			parent.frames.showAjaxBox();
			//alert("완료");
		} 
	});
	
	frm.page_no.value = 1;
	frm.submit();
}

function goPage(pageNo){
	var frm = document.ifFrm;
	
	new Ajax.Request('/test', {   
		onLoading: function() {     
		//	parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});

	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=R";
	frm.page_no.value = pageNo;
	frm.submit();
}


function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 음악저작물 상세 팝업오픈 : 부모창에서 오픈
function openBroadcastDetail( crId ) {

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=R'+param
	var name = '';
	var openInfo = 'target=broadcastRghtSrch, width=705, height=500';
	
	window.open(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifBroadcastRghtSrch"); 
	
	var totalRow = ${broadcastList.totalRow};
	totalRow = cfInsertComma(totalRow);
	parent.document.getElementById("totalRow").value = totalRow+"건";
	
		// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();

	// 부모창 알림- 추가저작물에 대한 중복확인검색 시 사용.
	if('${parentGubun}' == 'Y') {
	
		var srchTitleObj = document.ifFrm.srchTitle; 
		var srchTitle = srchTitleObj.value;
		
		if(totalRow=='0') {
			alert('저작물명 ['+srchTitle+'] 의 정보는 존재하지 않습니다. 계속진행 해주세요. \n' );
		}
		else {
			// 검색결과가 있는경우
			alert('저작물명 ['+srchTitle+'] 으로 '+totalRow+'건 검색되었습니다. \n추가등록을 원하는 저작물정보가 존재하는 경우 아래 검색결과에서 [선택 추가]해 주세요.' );
			srchTitleObj.focus();
		}
	}
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="dtlYn" value="Y"/>
	
		<!-- search form str -->
		<fieldset class="w99">
			<legend></legend>
			<p class="box1">
				<label for="sch1" class="schDot1 pl10">
				<strong>저작물명</strong></label>
				<input class="inputData" id="sch1" name="srchTitle" value="${srchParam.srchTitle}" size="12"/>
				<label for="sch2" class="schDot1 ml pl10">
				<strong>프로그램명</strong></label>
				<input class="inputData" id="sch2" name="srchProgName" value="${srchParam.srchProgName}" size="12"/>
				<label for="sch3" class="schDot1 ml pl10">
				<strong>제작자</strong></label>
				<input class="inputData" id="sch3" name="srchMaker" value="${srchParam.srchMaker}" size="12"/>
				<span class="button small black"><input type="submit" value="검색" onclick="javascript:fn_frameList();"></span>
			</p>
		</fieldset>
		<div id="div_scroll" class="tabelRound div_scroll" style="width:569px;height:auto;padding:0 0 0 0;">
		<table id="tab_scroll" cellspacing="0" cellpadding="0" border="1" summary="방송저작물의 저작물명, 프로그램명, 프로그램 회차, 방송일자, 매체, 채널, 프로그램등급, 제작자 정보입니다." class="grid mt5"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
			<colgroup>
			<col width="5%" />
			<col width="*" />
			<col width="16%" />
			<col width="16%" />
			<col width="16%" />
			<col width="16%" />
			</colgroup>
			<thead>
				<tr>
					<th rowspan="2" scope="col"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','ifrmChk',this);" style="cursor:pointer;" title="전체선택"/></th>
					<th rowspan="2" scope="col">저작물명</th>
					<th colspan="2" scope="col">프로그램명</th>
					<th scope="col">방송일자</th>
					<th scope="col">제작자</th>
				</tr>
				<tr>
					<th scope="col">회차</th>
					<th scope="col">등급</th>
					<th scope="col">매체</th>
					<th scope="col">채널</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${broadcastList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="6">검색된 방송저작물이 없습니다.</td>
					</tr>
				</c:if>
				
				<c:if test="${broadcastList.totalRow > 0}">
					<c:forEach items="${broadcastList.resultList}" var="broadcastList">
						<c:set var="NO" value="${broadcastList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
						<tr>
							<td rowspan="2" class="ce">
								<input type="checkbox" name="ifrmChk" class="vmid" value="${broadcastList.CR_ID }|0|0" style="cursor:pointer;" title="선택" />
								<!-- hidden value start -->
								<input type="hidden" name="title" value="${broadcastList.TITLE_TRNS}" />
								<input type="hidden" name="progName" value="${broadcastList.PROG_NAME_TRNS}" />
								<input type="hidden" name="progOrdSeq" value="${broadcastList.PROG_ORDSEQ_TRNS}" /> <!-- 프로그램회차 -->
								<input type="hidden" name="broadDate" value="${broadcastList.BROAD_DATE}" />
								<input type="hidden" name="mediCode" value="${broadcastList.MEDI_CODE}" />
								<input type="hidden" name="chnlCode" value="${broadcastList.CHNL_CODE}" />
								<input type="hidden" name="mediCodeName" value="${broadcastList.MEDI_CODE_NAME_TRNS}" />
								<input type="hidden" name="chnlCodeName" value="${broadcastList.CHNL_CODE_NAME_TRNS}" />
								<input type="hidden" name="progGrad" value="${broadcastList.PROG_GRAD}" />
								<input type="hidden" name="crId" value="${broadcastList.CR_ID}" />
								<input type="hidden" name="maker" value="${broadcastList.MAKER_TRNS}" />
								<input type="hidden" name="icnNumb" value="${broadcastList.stdCrhId}" />
								<!-- hidden value end -->
							</td>
							<td rowspan="2"><a class="underline black2" href="#1" onclick="javascript:openBroadcastDetail('${broadcastList.CR_ID }');" title="새창 열림">${broadcastList.TITLE }</td>
							<td colspan="2" class="ce">${broadcastList.PROG_NAME }</td>
							<td class="ce">${broadcastList.BROAD_DATE }</td>
							<td class="ce">${broadcastList.MAKER }</td>
						</tr>
						<tr>
							<td class="ce">${broadcastList.PROG_ORDSEQ }</td>
							<td class="ce">${broadcastList.PROG_GRAD }</td>
							<td class="ce">${broadcastList.MEDI_CODE_NAME }</td>
							<td class="ce">${broadcastList.CHNL_CODE_NAME }</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${broadcastList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
