<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권찾기 신청내역 조회 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">

<style type="text/css">
body {
	overflow-x: auto;
	margin: 0 0 0 0 ; 
}

.div_scroll {
	overflow-x: hidden;
	overflow-y: auto;
	margin-left: 0px;
}
</style>
</head>

<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>저작권정보 변경신청내역 조회</h1>
		</div>
		<!-- //HEADER end -->
		
		
		<!-- CONTAINER str-->
		<div id="popContents">
			
			<div class="section">
				<h2>저작물 정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="저작권찾기 신청내역 저작물명, 저작권찾기 신청횟수 정보입니다.">
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th class="ce bgbr">저작물명</th>
							<td colspan="3"><b>${rghtPrpsHist[0].TITLE}</b></td>
						</tr>
						<tr>
							<th class="ce bgbr">저작권정보 변경신청 횟수</th>
							<td colspan="3">${totalrow} 회</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="section mt20">
				<h2>저작권정보 변경신청내역</h2>
				<span class="topLine"></span>
				<div class="div_scroll" style="width:100%;height:190px;padding:0 0 0 0;">
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="저작권찾기 신청내역 신청순번, 신청일자, 진행상태, 신청저작권정보 정보입니다.">
					<colgroup>
					<col width="15%">
					<col width="20%">
					<col width="15%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th class="ce bgbr">신청순번</th>
							<th class="ce bgbr">신청일자</th>
							<th class="ce bgbr">진행상태</th>
							<th class="ce bgbr">신청 저작권정보</th>
						</tr>
						<c:forEach items="${rghtPrpsHist}" var="rghtPrpsHist">
						<tr>
							<td class="ce">${totalrow - i}<c:set var="i" value="${i+1}"/></td>
							<td class="ce">${rghtPrpsHist.RGST_DTTM}</td>
							<td class="ce">${rghtPrpsHist.TOTAL_STAT}</td>
							<td>
							<!-- 음악 -->
							<c:if test="${DIVS=='M'}">
								<c:set var="k" value="0"/>
								<c:set var="kk" value="0"/>
								<c:if test="${rghtPrpsHist.LYRICIST!=null || rghtPrpsHist.COMPOSER!=null || rghtPrpsHist.ARRANGER!=null}">
									<b>[작사]</b> ${rghtPrpsHist.LYRICIST} 
									<b>[작곡]</b> ${rghtPrpsHist.COMPOSER}
									<b>[편곡]</b> ${rghtPrpsHist.ARRANGER}
									<c:set var="k" value="1"/>
								</c:if>
								
								<c:if test="${rghtPrpsHist.SINGER!=null || rghtPrpsHist.PLAYER!=null || rghtPrpsHist.CONDUCTOR!=null}">
									<c:if test="${ k == 1 }">
										<br/>
									</c:if>
									<c:set var="kk" value="1"/>
									<b>[가창]</b> ${rghtPrpsHist.SINGER} 
									<b>[연주]</b> ${rghtPrpsHist.PLAYER}	
									<b>[지휘]</b> ${rghtPrpsHist.CONDUCTOR} 
								</c:if>
							
								<c:if test="${rghtPrpsHist.PRODUCER!=null}">
									<c:if test="${ kk == 1  || k==1}">
										<br/>
									</c:if>
									<b>[앨범제작]</b> ${rghtPrpsHist.PRODUCER} 
								</c:if>
								
							</c:if>
							
							<!-- 어문 -->
							<!-- 문예협 -->
							<c:if test="${DIVS=='O'}">
								<b>[저자]</b> ${rghtPrpsHist.LICENSOR_NAME_KOR} 
								<b>[역자]</b> ${rghtPrpsHist.TRANSLATOR} 
							</c:if>
							
							<!-- 방송대본 -->
							<!--  -->
							<c:if test="${DIVS=='C'}">
								<b>[작가]</b> ${rghtPrpsHist.WRITER} 
							</c:if>
							
							<!-- 영화 -->
							<!-- 영산협 -->
							<c:if test="${DIVS=='V'}">
								<b>[제작사]</b> ${rghtPrpsHist.MV_PRODUCER}  
								<b>[투자사]</b> ${rghtPrpsHist.INVESTOR}  
								<b>[배급사]</b> ${rghtPrpsHist.DISTRIBUTOR} 
								<b>[작가]</b> ${rghtPrpsHist.MV_WRITER} 
							</c:if>
							
							<!-- 이미지 -->
							<!--  -->
							<c:if test="${DIVS=='I'}">
								<b>[저작권자]</b> ${rghtPrpsHist.COPT_HODR} 
							</c:if>
							
							<!-- 방송 -->
							<c:if test="${DIVS=='R'}">
								<b>[제작자]</b> ${rghtPrpsHist.MAKER} 
							</c:if>
							
							<!-- 뉴스  -->
							<c:if test="${DIVS=='N'}">
								<b>[언론사명]</b> ${rghtPrpsHist.PROVIDER_NM} 
							</c:if>
							
						<!--  이후 저작물 종류 추가시 종류별 저작권에 대한 작성이 필요함 -->
							</td>	
						</tr>
						</c:forEach>
					</tbody>
				</table>
				</div>
			</div>
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="X" title="이 창을 닫습니다." /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>