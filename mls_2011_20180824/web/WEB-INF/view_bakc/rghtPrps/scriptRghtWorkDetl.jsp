<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>저작권 정보 확인 서비스 이용 - 뉴스상세(${script.TITLE}) | 내권리찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop">
	
		<!-- Header str -->
		<div id="popHeader">
			<h1>방송대본저작물 상세조회</h1>
		</div>
		<!-- //Header end -->
		
		<!-- CONTAINER str-->
		<div id="popContents">
			<p class="red p11" ><strong>* 저작권정보 변경신청에 관한 문의는 검색화면의 단체로 연락바랍니다.</strong></p>
			<div class="section">
				<h2 class="mt15">방송대본 저작물정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="방송대본 저작물정보(작품제목, 작품부제목)입니다.">
					<caption>방송대본 저작물정보</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">작품제목</th>
							<td>${script.TITLE}</td>
						</tr>
						<tr>
							<th scope="row">작품부제목</th>
							<td>${script.SUBTITLE}</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="section mt20">
				<h2>작품정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="방송대본 작품정보(장르분류, 소재분류, 연출가, 원작명, 주요출연진, 줄거리, 방송회차, 방송일자)입니다.">
					<caption>방송대본  작품정보</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">장르분류</th>
							<td>${script.GENRE_KIND_NAME}</td>
							<th scope="row">소재분류</th>
							<td>${script.SUBJ_KIND_NAME}</td>
						</tr>
						<tr>
							<th scope="row">연출가</th>
							<td colspan="3">${script.DIRECT}</td>
						</tr>
						<tr>
							<th scope="row">원작명</th>
							<td colspan="3">${script.ORIG_WORK}</td>
						</tr>
						<tr>
							<th scope="row">주요출연진</th>
							<td colspan="3">${script.PLAYERS}</td>
						</tr>
						<tr>
							<th scope="row">줄거리</th>
							<td colspan="3">${script.SYNNOPSIS}</td>
						</tr>
						<tr>
							<th scope="row">방송회차</th>
							<td>${script.BROAD_ORD}</td>
							<th scope="row">방송일자</th>
							<td>${script.BROAD_DATE}</td>
						</tr>
						<tr>
							<th scope="row">방송시간/분</th>
							<td>${script.TELE_TIME}</td>
							<th scope="row">제작사</th>
							<td>${script.MAKER}</td>
						</tr>
						<tr>
							<th scope="row">방송사</th>
							<td>${script.BROAD_STAT_NAME}</td>
							<th scope="row">주요출연진</th>
							<td>${script.PLAYERS}</td>
						</tr>
						<tr>
							<th scope="row">진행자</th>
							<td colspan="3">${script.MC}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="section mt20">
				<h2>권리정보</h2>
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="방송대본 권리정보(작가명, 한국방송작가협회)입니다.">
					<caption>방송대본  권리정보</caption>
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">작가명</th>
							<td>${script.WRITER}</td>
							<th scope="row">한국방송작가협회</th>
							<td>신탁</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="닫기" title="이 창을 닫습니다." /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
