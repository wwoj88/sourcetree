<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
	
<title>저작권찾기(방송대본) | 저작권찾기</title>
<script type="text/JavaScript">
<!--

var emptyYn = '${emptyYn}'; // 검색조건이 없을 경우 //20120220 정병호

window.name = "scriptRghtSrch";
	
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
		},
		onSuccess: function(req) {     
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/rghtPrps/rghtSrch.do?method=subList&DIVS=C";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 방송대본저작물 상세 팝업오픈 : 부모창에서 오픈
function openScriptDetail( crId ) {

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=C'+param
	var name = '';
	var openInfo = 'target=scriptRghtSrch, width=705, height=500, scrollbars=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=C'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch, width=705, height=480';
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifScriptRghtSrch"); 
	
	if(emptyYn != 'Y'){// 검색조건이 없을 경우 //20120220 정병호
		var totalRow = ${scriptList.totalRow}
		parent.document.getElementById("totalRow").value = totalRow //+"건"; //초기페이지 예외처리하기.
	}

	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();

	parent.frames.scrollTo(0,0);
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="sTotalRow" value="${scriptList.totalRow}"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
	<input type="hidden" name="srchWriter" value="${srchParam.srchWriter }"/>
	<input type="hidden" name="srchBroadStatName" value="${srchParam.srchBroadStatName }"/>
	<input type="submit" style="display:none;"/>
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:auto;padding:0 0 0 0;margin-top:1px;">
			<table id="tab_scroll" class="sub_tab3 mar_tp40" cellspacing="0" cellpadding="0" width="100%"  summary="방송대본 저작물의 작품명, 작가명, 연출가, 방송회차, 방송일시, 방송매체, 방송사, 제작사, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="5%"/>
				<col width="*" />
				<col width="15%" />
				<col width="15%" />
				<col width="15%" />
				<col width="15%" />
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" scope="col">순번</th>
						<th scope="col" rowspan="2">작품명</th>
						<th scope="col" rowspan="2">작가</th>
						<th scope="col">방송사</th>
						<th scope="col">방송매체</th>
						<th scope="col">연출가</th>
					</tr>
					<tr>
						<th scope="col">방송회차</th>
						<th scope="col">방송일자</th>
						<th scope="col">주요출연진</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${emptyYn == 'Y'}"><!-- 검색조건이 없을 경우 //20120220 정병호 -->
					<tr>
						<td class="ce" colspan="6">검색조건을 입력해 주세요.</td>
					</tr>
				</c:if>
				<c:if test="${scriptList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="6">검색된 방송대본이 없습니다.</td>
					</tr>
				</c:if>
				<c:if test="${scriptList.totalRow > 0}">
					<c:forEach items="${scriptList.resultList}" var="scriptList">
						<c:set var="NO" value="${scriptList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<c:choose>
						 	<c:when test="${scriptList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num" rowspan="2" >${scriptList.ROW_NUM}</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce" rowspan="2" >${scriptList.ROW_NUM}</td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${scriptList.ROW_NUM % 2 == 0}">
						 		<td class="ev_num" rowspan="2" style="cursor:pointer;">
							<!-- hidden value start -->
							<input type="hidden" name="title" value="${scriptList.TITLE_TRNS}" />
							<input type="hidden" name="writer" value="${scriptList.WRITER_TRNS}" /><!-- 작가 -->
							<input type="hidden" name="subTitle" value="${scriptList.SUBTITLE_TRNS}" />
							<input type="hidden" name="direct" value="${scriptList.DIRECT_TRNS}" />
							<input type="hidden" name="insertDate" value="${scriptList.INSERT_DATE}" />
							<input type="hidden" name="broadOrd" value="${scriptList.BROAD_ORD}" /><!-- 방송회차 -->
							<input type="hidden" name="crId" value="${scriptList.CR_ID}" />
							<input type="hidden" name="broadDate" value="${scriptList.BROAD_DATE}" />
							<input type="hidden" name="broadMediName" value="${scriptList.BROAD_MEDI_NAME_TRNS}" /><!-- 방송매체 -->
							<input type="hidden" name="broadStatName" value="${scriptList.BROAD_STAT_NAME_TRNS}" /><!-- 방송사 -->
							<input type="hidden" name="players" value="${scriptList.PLAYERS_TRNS}" /><!-- 주요출연진 -->
							<input type="hidden" name="maker" value="${scriptList.MAKER_TRNS}" /><!-- 연출 -->
							<input type="hidden" name="genreKindName" value="${scriptList.GENRE_KIND_NAME}"/><!-- 장르 -->
							<input type="hidden" name="crhIdOfCa" value="${scriptList.CRH_ID_OF_CA}"/>
							<input type="hidden" name="icnNumb" value="${scriptList.ICN_NUMB}" />
							<!-- hidden value end -->
							 <a href="#1" class="underline black2" onclick="javascript:openScriptDetail('${scriptList.CR_ID }');" title="새창 열림">${scriptList.TITLE }</a>
						</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td rowspan="2" style="cursor:pointer;">
							<!-- hidden value start -->
							<input type="hidden" name="title" value="${scriptList.TITLE_TRNS}" />
							<input type="hidden" name="writer" value="${scriptList.WRITER_TRNS}" /><!-- 작가 -->
							<input type="hidden" name="subTitle" value="${scriptList.SUBTITLE_TRNS}" />
							<input type="hidden" name="direct" value="${scriptList.DIRECT_TRNS}" />
							<input type="hidden" name="insertDate" value="${scriptList.INSERT_DATE}" />
							<input type="hidden" name="broadOrd" value="${scriptList.BROAD_ORD}" /><!-- 방송회차 -->
							<input type="hidden" name="crId" value="${scriptList.CR_ID}" />
							<input type="hidden" name="broadDate" value="${scriptList.BROAD_DATE}" />
							<input type="hidden" name="broadMediName" value="${scriptList.BROAD_MEDI_NAME_TRNS}" /><!-- 방송매체 -->
							<input type="hidden" name="broadStatName" value="${scriptList.BROAD_STAT_NAME_TRNS}" /><!-- 방송사 -->
							<input type="hidden" name="players" value="${scriptList.PLAYERS_TRNS}" /><!-- 주요출연진 -->
							<input type="hidden" name="maker" value="${scriptList.MAKER_TRNS}" /><!-- 연출 -->
							<input type="hidden" name="genreKindName" value="${scriptList.GENRE_KIND_NAME}"/><!-- 장르 -->
							<input type="hidden" name="crhIdOfCa" value="${scriptList.CRH_ID_OF_CA}"/>
							<input type="hidden" name="icnNumb" value="${scriptList.ICN_NUMB}" />
							<!-- hidden value end -->
							 <a href="#1" class="underline black2" onclick="javascript:openScriptDetail('${scriptList.CR_ID }');" title="새창 열림">${scriptList.TITLE }</a>
						</td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${scriptList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num" rowspan="2">${scriptList.WRITER }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce" rowspan="2">${scriptList.WRITER }</td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${scriptList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${scriptList.BROAD_STAT_NAME }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${scriptList.BROAD_STAT_NAME }</td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${scriptList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${scriptList.BROAD_MEDI_NAME }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${scriptList.BROAD_MEDI_NAME }</td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${scriptList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${scriptList.DIRECT }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${scriptList.DIRECT }</td>
						 	</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
						 	<c:when test="${scriptList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${scriptList.BROAD_ORD}<c:if test="${scriptList.BROAD_ORD}">&nbsp;</c:if></td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${scriptList.BROAD_ORD}<c:if test="${scriptList.BROAD_ORD}">&nbsp;</c:if></td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${scriptList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${scriptList.BROAD_DATE }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${scriptList.BROAD_DATE }</td>
						 	</c:otherwise>
						</c:choose>
						<c:choose>
						 	<c:when test="${scriptList.ROW_NUM % 2 == 0}">
						 		<td class="ce ev_num">${scriptList.PLAYERS }</td>
						 	</c:when>
						 	<c:otherwise>
						 		<td class="ce">${scriptList.PLAYERS }</td>
						 	</c:otherwise>
						</c:choose>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${scriptList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
