<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%
	String bordSeqn = request.getParameter("bordSeqn");
	String rgstKindCode = "";
	String menuSeqn = request.getParameter("menuSeqn");
	if(menuSeqn.equals("5")){
		rgstKindCode = request.getParameter("leftsub");
	}else{
		rgstKindCode = "";
	}
	String threaded = request.getParameter("threaded");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");

	Board boardDTO = (Board) request.getAttribute("board");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>홍보자료 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--
  function fn_promList(){
		var frm = document.form1;
		frm.action = "/board/board.do";
		frm.submit();
	}

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }

	function getFlash(swfName,wt,ht,id){
	    document.write('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="'+wt+'" height="'+ht+'" id="'+id+'">');
	    document.write('<param name="movie" value="'+swfName+'">');
	    document.write('<param name="quality" value="high">');
	    document.write('<param name="wmode" value="transparent">');
	    document.write('<param name="allowScriptAccess" value="always">');
	    document.write('<embed src="'+swfName+'" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent" width="'+wt+'" height="'+ht+'" allowScriptAccess="sameDomain" id="'+id+'"><\/embed><\/object>');
	}	  
//-->
</script>	
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(4);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_4.gif" alt="알림마당" title="알림마당" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1"><img src="/images/2011/content/sub_lnb0401_off.gif" title="공지사항" alt="공지사항" /></a></li>
					<li id="lnb2"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1"><img src="/images/2011/content/sub_lnb0402_off.gif" title="자주묻는 질문" alt="자주묻는 질문" /></a></li>
					<li id="lnb3"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=2&amp;page_no=1"><img src="/images/2011/content/sub_lnb0403_off.gif" title="묻고답하기" alt="묻고답하기" /></a></li>
					<li id="lnb4"><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=2&amp;menuSeqn=5&amp;page_no=1"><img src="/images/2011/content/sub_lnb0404_off.gif" title="홍보자료" alt="홍보자료" /></a></li>
					<!--<li id="lnb5"><a href="/board/board.do?method=vocList"><img src="/images/2011/content/sub_lnb0407_off.gif" title="고객의 소리(VOC)" alt="고객의 소리(VOC)" /></a></li>
					--><li id="lnb6"><a href="/main/main.do?method=bannerList"><img src="/images/2011/content/sub_lnb0405_off.gif" title="홍보관" alt="홍보관" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb4");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>알림마당</span><em>홍보자료</em></p>
					<h1><img src="/images/2011/title/content_h1_0404.gif" alt="홍보자료" title="홍보자료" /></h1>
					<div class="section">
						<form name="form1" method="post" action = "#">
									<input type="hidden" name="bordSeqn" value="<%=bordSeqn%>"/>
									<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>"/>
									<input type="hidden" name="threaded" value="<%=threaded%>"/>
									<input type="hidden" name="srchDivs" value="<%=srchDivs%>"/>
									<input type="hidden" name="srchText" value="<%=srchText%>"/>
									<input type="hidden" name="page_no" value="<%=page_no%>"/>
									<input type="hidden" name="leftsub" value="<%=boardDTO.getRgstKindCode()%>"/>
									<input type="hidden" name="filePath"/>
									<input type="hidden" name="fileName"/>
									<input type="hidden" name="realFileName"/>
									<input type="submit" style="display:none;">
						<div class="tabelRound">
						<!-- 테이블 view Set -->
						<div class="article">
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">제목</th>
										<td colspan="3"><%=boardDTO.getTite()%></td>
									</tr>
									<tr>
										<th scope="row" >작성자</th>
										<td><%=boardDTO.getRgstIdnt()%></td>
										<th scope="row" >작성일</th>
										<td><%=boardDTO.getRgstDate()%></td>
									</tr>
									<tr>
										<th scope="row">내용</th>
										<td colspan="3">
											<%=CommonUtil.replaceBr(boardDTO.getBordDesc(),true)%>
												<%
									          	List fileList1 = (List) boardDTO.getFileList();
									          	int listSize1 = fileList1.size();
									
									          	if (listSize1 > 0) {
									%>
											<div class="files">
															<%
															String type = "";
										          	  for(int i=0; i<listSize1; i++) {
										          	    Board fileDTO1 = (Board) fileList1.get(i);
										          	    type = fileDTO1.getRealFileName().substring(fileDTO1.getRealFileName().indexOf(".")+1);
										          	    if(type.equals("avi")||type.equals("wmv")){
										%>
												<embed src="/promUpload/<%=fileDTO1.getRealFileName()%>" width="420" height="310" autostart="false"></embed>
											<%
											            }else if(type.equals("swf")){
										%>		
										<script type="text/javascript">getFlash('/promUpload/<%=fileDTO1.getRealFileName()%>','420','310','id');</script>
												<%
											            }else{
											            	%>
											            	<img src="/promUpload/<%=fileDTO1.getRealFileName()%>" alt="<%=fileDTO1.getRealFileName()%>" />
											            	<% 
											            }
											      	}
										%>
											</div>
											<%
										          }
									%>
										</td>
									</tr>
									<%
									          	List fileList = (List) boardDTO.getFileList();
									          	int listSize = fileList.size();
									
									          	if (listSize > 0) {
									%>
									<tr>
										<th scope="row" class="bgbr">첨부파일</th>
										<td colspan="3">
									<%

									          	  for(int i=0; i<listSize; i++) {
									          	    Board fileDTO = (Board) fileList.get(i);
									%>	
										<a href="#1" onclick="javascript:fn_fileDownLoad('<%=fileDTO.getFilePath()%>','<%=fileDTO.getFileName()%>','<%=fileDTO.getRealFileName()%>')" class="orange underline" ><%=fileDTO.getFileName()%></a><br>
									<%
										            }
									%>	
										</td>
									</tr>
									<%
									          }
									%>
								</tbody>
							</table>
							
							<!-- //그리드스타일 -->
							
							<div class="btnArea">
								<p class="lft"><span class="button medium gray"><a href="#1" onclick="javascript:fn_promList();">목록</a></span></p>
							</div>
							
						</div>
						<!-- //테이블 view Set -->
					</div>
						</form>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
