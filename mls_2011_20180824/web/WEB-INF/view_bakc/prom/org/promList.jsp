<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
	String rgstKindCode = "";
	String menuSeqn = request.getParameter("menuSeqn");
	if(menuSeqn.equals("5")){
		rgstKindCode = request.getParameter("leftsub");
	}else{
		rgstKindCode = "";
	}

	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>홍보자료 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--
	function goPage(pageNo){
		var frm = document.form1;
		frm.page_no.value = pageNo;
		frm.submit();
	}

	function board_search() {
		var frm = document.form1;
		frm.page_no.value = 1;
		frm.submit();
	}

	function fn_enterCheck(obj){
	  // EnterKey 입력시 공인인증서 로그인 수행
	  if (event.keyCode == 13) {
		  board_search();
	  }
  }

	function boardDetail(bordSeqn,menuSeqn,threaded){
		var frm = document.form1;
		frm.bordSeqn.value = bordSeqn;
		frm.menuSeqn.value = menuSeqn;
		frm.threaded.value = threaded;
		frm.page_no.value = '<%=page_no%>';
		frm.method = "post";
		frm.action = "/board/board.do?method=boardView";
		frm.submit();
  }
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(4);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_4.gif" alt="알림마당" title="알림마당" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1"><img src="/images/2011/content/sub_lnb0401_off.gif" title="공지사항" alt="공지사항" /></a></li>
					<li id="lnb2"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1"><img src="/images/2011/content/sub_lnb0402_off.gif" title="자주묻는 질문" alt="자주묻는 질문" /></a></li>
					<li id="lnb3"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=2&amp;page_no=1"><img src="/images/2011/content/sub_lnb0403_off.gif" title="묻고답하기" alt="묻고답하기" /></a></li>
					<li id="lnb4"><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=2&amp;menuSeqn=5&amp;page_no=1"><img src="/images/2011/content/sub_lnb0404_off.gif" title="홍보자료" alt="홍보자료" /></a></li>
					<li id="lnb5"><a href="/board/board.do?method=vocList"><img src="/images/2011/content/sub_lnb0407_off.gif" title="고객의 소리(VOC)" alt="고객의 소리(VOC)" /></a></li>
					<li id="lnb6"><a href="/main/main.do?method=bannerList"><img src="/images/2011/content/sub_lnb0405_off.gif" title="홍보관" alt="홍보관" /></a></li>
					</ul>
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb4");</script>
				</div>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>알림마당</span><em>홍보자료</em></p>
					<h1><img src="/images/2011/title/content_h1_0404.gif" alt="홍보자료" title="홍보자료" /></h1>
					
					<div class="section">
						<!-- 검색 -->
						<form name="form1" action="#" class="sch mt20">
					    <input type="hidden" name="page_no" />
					 	<input type="hidden" name="leftsub" value="<%=rgstKindCode%>" />
						<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>" />
					 	<input type="hidden" name="bordSeqn" />
					  	<input type="hidden" name="threaded" />
							<fieldset class="w100">
							<legend>홍보자료 검색</legend>
								<div class="boxStyle">
									<div class="box1 floatDiv">
										<p class="fl mt5 w15"><label for="srchDivs"><img src="/images/2011/content/sch_txt.gif" alt="Search" title="Search" /></label></p>
										<p class="fl w70">
										<select id="srchDivs" name="srchDivs" class="w20">
											<option value="">선택</option>
											<option value="01" <%="01".equals(srchDivs)?"selected='selected'":"" %>>제목</option>
											<option value="02" <%="02".equals(srchDivs)?"selected='selected'":"" %>>내용</option>
											<option value="03" <%="03".equals(srchDivs)?"selected='selected'":"" %>>작성자</option>
											<option value="04" <%="04".equals(srchDivs)?"selected='selected'":"" %>>제목+내용</option>
										</select>
										<input name="srchText" value="<%=srchText%>" size="35" onkeyup="fn_enterCheck(this)" title="검색어" class="inputData w40" />
										<span class="button small black"><input type="submit" onclick = "javascript:board_search();" value="검색" /></span></p>
									</div>
									<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
								</div>
							</fieldset>
						</form>
						<!-- //검색 -->
						
						<!-- 테이블 리스트 Set -->
						<div class="section mt20">
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="8%">
								<col width="*">
								<col width="10%">
								<col width="15%">
								<col width="10%">
								<col width="10%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">제목</th>
										<th scope="col">작성자</th>
										<th scope="col">작성일</th>
										<th scope="col">첨부파일</th>
										<th scope="col">조회수</th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${boardList.totalRow == 0}">
										<tr>
											<td class="ce" colspan="6">등록된 게시물이 없습니다.</td>
										</tr>
								</c:if>
								<c:if test="${boardList.totalRow > 0}">
									<c:forEach items="${boardList.resultList}" var="board">
							        <c:set var="NO" value="${board.TOTAL_CNT}"/>
						        	<c:set var="i" value="${i+1}"/>
									<tr>
										<td class="ce"><c:out value="${NO - i}"/></td>
										<td><a href="#1" onclick="javascript:boardDetail('${board.BORD_SEQN}','${board.MENU_SEQN}','${board.THREADED}')">${board.TITE}</a></td>
										<td class="ce">${board.RGST_IDNT}</td>
										<td class="ce">${board.RGST_DTTM}</td>
										<td class="ce">${board.FILE_CONT}</td>
										<td class="ce">${board.INQR_CONT}</td>
									</tr>
									</c:forEach>
		    					</c:if>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<!-- 페이징 -->
							<div class="pagination">
							<ul>
								<li>
						 		<%-- 페이징 리스트 --%>
								  <jsp:include page="../common/PageList_2011.jsp" flush="true">
									  <jsp:param name="totalItemCount" value="${boardList.totalRow}" />
										<jsp:param name="nowPage"        value="${param.page_no}" />
										<jsp:param name="functionName"   value="goPage" />
										<jsp:param name="listScale"      value="" />
										<jsp:param name="pageScale"      value="" />
										<jsp:param name="flag"           value="M01_FRONT" />
										<jsp:param name="extend"         value="no" />
									</jsp:include>
								</li>	
							</ul>
						</div>
							<!-- //페이징 -->
							
						</div>
						<!-- //테이블 리스트 Set -->
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
