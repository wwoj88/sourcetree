<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>
<script>
function selectMenu(_menu) {
	location.href = '/m/stat/stat.do?method='+_menu;
}
</script>
<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>법정허락</h1>
		</header>
		
		<ul class="list ln">
		<li><a href="javascript:selectMenu('statBo01List')">저작권자 조회공고<span class="ic"></span></a></li>
		<li class="or"><a href="javascript:selectMenu('statBo03List')">이용승인 신청공고<span class="ic"></span></a></li>
		<li><a href="javascript:selectMenu('statBo04List')">승인 공고<span class="ic"></span></a></li>
		<li class="or"><a href="javascript:selectMenu('statBo05List')">보상금공탁공고<span class="ic"></span></a></li>
		</ul>
		
	</section>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>