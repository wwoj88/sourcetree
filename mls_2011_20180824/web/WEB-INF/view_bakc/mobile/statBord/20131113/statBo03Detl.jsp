<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>

<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>법정허락</h1>
			<h2>이용승인신청공고</h2>
		</header>
		
		<article class="list_detl">
			<span class="sh"></span>
			<div>
				<header><h1>${mobileStat.tite}</h1></header>
				<span class="tl">공고자 : ${mobileStat.anucItem3}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="tl">공고일 : ${mobileStat.openDttm}</span>
				<ul class="mt15">
										<li><p class="line22" align="left">문화체육관광부 공고 제 ${mobileStat.anucItem1}호</p></li><br/>
										<li><p class="line22"><span>&nbsp;&nbsp;&nbsp;${mobileStat.anucItem2}</span></p></li><br/>
										<li><p class="line22" align="center"><b>저작물 이용의 법정허락 신청 내용 공고</b></p></li><br/>
										<li><p class="line22"><span class="blue2 ">1. 신청인</span> : ${mobileStat.anucItem3 }</p></li>
										<li><p class="line22"><span class="blue2 ">2. 대상저작물</span> </p></li>
										<li><p class="line22"><span class="blue2 ">&nbsp;&nbsp;&nbsp;가. 저작물 종류</span> :  ${mobileStat.anucItem4} </p></li>
										<li><p class="line22"><span class="blue2 ">&nbsp;&nbsp;&nbsp;나. 저작물 제호(명칭)</span> :  ${mobileStat.anucItem5} </p></li>
										<li><p class="line22"><span class="blue2 ">&nbsp;&nbsp;&nbsp;다. 공표시 표시된 저작자 성명</span> :  ${mobileStat.anucItem6} </p></li>
										<li><p class="line22"><span class="blue2 ">&nbsp;&nbsp;&nbsp;라. 공표매체</span> :  ${mobileStat.anucItem7} </p></li>
										<li><p class="line22"><span class="blue2 ">&nbsp;&nbsp;&nbsp;마. 공표연월일</span> :  ${mobileStat.anucItem8} </p></li>
										<li><p class="line22"><span class="blue2 ">3. 신청목적 </span> : ${mobileStat.anucItem9}</li>
										<li><p class="line22"><span class="blue2 ">4. 법적근거 </span> : ${mobileStat.anucItem10}</li>
										<li><p class="line22"><span class="blue2 ">5. 신청경위 </span> : ${mobileStat.anucItem11}</li>
										<li><p class="line22"><span class="blue2 ">6. 이의신청 </span> :&nbsp;&nbsp; 동 저작물이 이용승인신청에 대하여 이의를 제기하려는 저작재산권자는<br/>
																										승인이전에 다음 사항이 기재된 서류 등을 한국저작권위원회 심의조정팀(02-2660-0102)으로<br/>
																										제출 하시기 바랍니다.<br/><br/>
																										&nbsp;&nbsp;&nbsp;가. 법정하럭 신청 내용에 대한 이의 내용 및 그 이유<br/>
																										&nbsp;&nbsp;&nbsp;나. 성명(단체인 경우 단체명과 그 대표자)ㆍ주소ㆍ전화번호<br/>
																										&nbsp;&nbsp;&nbsp;다. 자신이 그 권리자로 표시된 저작권 등의 등록증 사본 또는 그에 상당하는 자료<br/>
																										&nbsp;&nbsp;&nbsp;라. 자신의 성명이나 또는 예명, 아호ㆍ약칭 등으로서 널리 알려진 것이 표시되어<br/>
																										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 있는 그 저작물의 사본 또는 그에 상당하는 자료
																										</li>
																		
										</ul>
			</div>
			<div class="list_btn"><a href="javascript:history.back();">목록</a></div>
		</article>
		
	</section>
	<hr>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>