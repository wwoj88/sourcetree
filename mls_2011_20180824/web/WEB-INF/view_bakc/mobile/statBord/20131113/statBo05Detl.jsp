<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>

<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>법정허락</h1>
			<h2>보상금공탁공고</h2>
		</header>
		
		<article class="list_detl">
			<span class="sh"></span>
			<div>
				<header><h1><c:out value="${mobileStat.tite}"/></h1></header>
				<span class="tl">공고자 : ${mobileStat.anucItem8}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="tl">공고일 : ${mobileStat.openDttm}</span>
				<span class="tl">첨부파일 : 
				<c:forEach items="${fileList}" var="mobileStat">
					<a href="#" onclick="javascript:fileDownLoad('${mobileStat.filePath}','${mobileStat.fileName}','${mobileStat.realFileName}')">${mobileStat.fileName}&nbsp;</a>
				</c:forEach>
				</span>
				<ul class="mt15">
				<li>
					 <p class="blue2 line22"><br/>1. 저작물의 제호</p>
					 <p class="mt5 line22">${mobileStat.anucItem1}</p><br/>
				</li>
				<li class="mt15">
				    <p class="blue2 line22">2. 저작자 및 저작재산권자의 성명</p>
					<p class="mt5 line22">${mobileStat.anucItem2}</p><br/>
				</li>
				<li class="mt15">
					<p class="blue2 line22">3. 저작물 이용의 내용</p>
					<p class="mt5 line22">${mobileStat.anucItem3}</p><br/>
				</li>
				<li class="mt15">
					<p class="blue2 line22">4. 공탁금액</p>
					<p class="mt5 line22">${mobileStat.anucItem4}</p><br/>
				</li>
				<li class="mt15">
					<p class="blue2 line22">5. 공탁소의 명칭 및 소재지</p>
					<p class="mt5 line22">${mobileStat.anucItem5}</p><br/>
				</li>
				<li class="mt15">
					<p class="blue2 line22">6. 공탁근거</p>
					<p class="mt5 line22">${mobileStat.anucItem6}</p><br/>
				</li>
					<li class="mt15">
					<p class="blue2 line22">7. 저작물이용자의 주소/성명</p>
					<p class="mt5 line22">- 주소 : ${mobileStat.anucItem7}</p>
					<p class="mt5 line22">- 성명 : ${mobileStat.anucItem8}</p><br/>
				</li>
			</ul>
			</div>
			<div class="list_btn"><a href="javascript:history.back();">목록</a></div>
		</article>
		
	</section>
	<hr>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>