<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>

<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>저작권정보</h1>
			<h2>이미지</h2>
		</header>
		
		<article class="list_detl imge_detail">
			<span class="sh"></span>
			<div>
				<header>
					<h1><c:out value="${imageDetl.workName}"/></h1>
					<img src='<c:out value="${imageDetl.imageUrl}"/>' style="width: <c:out value='${imge_x}'/>; height: <c:out value='${imge_x}'/>;"  alt="<c:out value="${imageDetl.workName}"/>">
				</header>
				<ul class="detl">
				<li><strong>이미지정보</strong>
					<ul>
					<li>이미지명 : <c:out value="${imageDetl.workName}"/></li>
   					<li>분야 : <c:out value="${imageDetl.divs}"/></li>
					</ul>
				</li>
				<li><strong>도서정보</strong>
					<ul>
   					<li>출판사 : <c:out value="${imageDetl.lishComp}"/></li>
   					<li>출판년도 : <c:out value="${imageDetl.usexYear}"/>년</li>
   					<li>집필진 : <c:out value="${imageDetl.wterDivs}"/></li>
					</ul>
				</li>
				<li><strong>권리정보</strong>
					<ul>
					<li>작가 : <c:out value="${imageDetl.coptHodr}"/></li>
					</ul>
				</li>
				</ul>
			</div>
			<div class="list_btn"><a href="javascript:history.back();">목록</a></div>
		</article>
		
	</section>
	<hr>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>