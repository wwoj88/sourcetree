<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>

<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>저작권정보</h1>
			<h2>음악</h2>
		</header>
		
		<article class="list_detl">
			<span class="sh"></span>
			<div>
				<header><h1><c:out value="${musicDetl.title}"/></h1></header>
				<ul class="detl">
				<li><strong>- 앨범정보</strong>
					<ul class="news">
					<li><span class="tl">앨범명</span><span class="con"><c:out value="${musicDetl.albumTitle}"/></span></li>
   					<li><span class="tl">앨범발매일</span><span class="con"><c:out value="${musicDetl.issuedDate}"/></span></li>
   					<li><span class="tl">앨범형태</span><span class="con"><c:out value="${musicDetl.albumTypeStr}"/></span></li>
					</ul>
				</li>
				<li><strong>- 곡정보</strong>
					<ul class="news">
					<li><span class="tl">곡명</span><span class="con"><c:out value="${musicDetl.title}"/></span></li>
   					<li><span class="tl">곡구분</span><span class="con"><c:out value="${musicDetl.musicTypeStr}"/></span></li>
   					<li><span class="tl">실연시간</span><span class="con"><c:out value="${musicDetl.performanceTime}"/></span></li>
   					<li><span class="tl">트랙번호</span><span class="con"><c:out value="${musicDetl.trackNo}"/></span></li>
					</ul>
				</li>
				<li><strong>- 권리정보</strong>
					<ul>
					<li>작사 : <span class="con"><c:out value="${musicDetl.lyricist}"/></span></li>
   					<li>작곡 : <span class="con"><c:out value="${musicDetl.composer}"/></span></li>
   					<li>편곡 : <span class="con"><c:out value="${musicDetl.arranger}"/></span></li>
   					<li>음악저작권협회 : <span class="con"><c:out value="${musicDetl.trustYn}"/></span></li>
   					<li>가창 : <span class="con"><c:out value="${musicDetl.singer}"/></span></li>
   					<li>연주 : <span class="con"><c:out value="${musicDetl.player}"/></span></li>
   					<li>한국음악실연자연합회 : <span class="con"><c:out value="${musicDetl.pakTrustYn}"/></span></li>
   					<li>음반제작사 : <span class="con"><c:out value="${musicDetl.kappTrustYnStr}"/></span></li>
   					<li>한국음원제작자협회 : <span class="con"><c:out value="${musicDetl.kappTrustYn}"/></span></li>
					</ul>
				</li>
				</ul>
			</div>
			<div class="list_btn"><a href="javascript:history.back();">목록</a></div>
		</article>
		
	</section>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>