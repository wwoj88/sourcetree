<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>
<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>법정허락저작물</h1>
		</header>
		
		<article class="list">
			<header class="stat_tl">
				<strong class="f_orange">[<c:out value="${stat.gubun}"/>]</strong><c:out value="${stat.tite}"/>
				<c:if test="${stat.licensorName!=null }">
					<em class="stat"><c:out value="${rec.licensorName}"/></em>
				</c:if>
				<span class="descript"><em>장르 : <c:out value="${stat.genre}"/></em><em><c:out value="${stat.gubun}"/>일자 : <c:out value="${stat.apprDttm}"/></em></span>
			</header>
			<section class="list_detail" id="list1">
				<span class="sh"></span>
					<div class="detail_view">
					<p>
					<c:out value="${stat.bordDesc}" escapeXml="false"/>
					</p>
					</div>
			</section>
			<div class="stat_btnList"><a href="javascript:history.back();">목록</a></div>
		</article>
		
	</section>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>