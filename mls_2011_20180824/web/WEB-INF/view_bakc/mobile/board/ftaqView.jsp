<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.mobile.board.model.MBoard"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%
	MBoard board = (MBoard) request.getAttribute("board");
	MBoard srcBoard = (MBoard) request.getAttribute("srcBoard");
%>
<html lang="ko">
<head>
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>자주묻는질문 | 저작권찾기</title>
<script type="text/JavaScript">
<!--

function fn_ftaqList(){

	var frm = document.form;
	
	frm.action = "/m/faq.do";
	frm.submit();
}

//-->
</script>
</head>

<body>
	
	<a href="http://m.findcopyright.or.kr"> === 모바일 저작권찾기 Home === </a>
	<br/><br/>
	<a href="/m/faq.do">자주묻는질문</a>
	
	<br/>
	
	<!-- 테이블 리스트 Set -->
	<div class="section mt20">
		<!-- 검색 -->
		<form name="form" action="#" class="sch mt20">
			<input type="hidden" name="menuSeqn" value="<%=srcBoard.getMenuSeqn()%>">
			<input type="hidden" name="threaded" value="<%=srcBoard.getThreaded()%>">
			<input type="hidden" name="srchText" value="<%=srcBoard.getSrchText()%>">
			<input type="hidden" name="page_no" value="<%=srcBoard.getPage_no()%>">
			<input type="submit" style="display:none;">
		</form>
				   
		<!-- 그리드스타일 -->
		<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
			
			<tbody>
				<tr>
					<th scope="row"><%=board.getTite()%></th>
				</tr>
				<tr>
					<th scope="row"><%=CommonUtil.replaceBr(board.getBordDesc(),true)%></th>
				</tr>
			</tbody>
		</table>
		<!-- //그리드스타일 -->
		
	</div>
	<!-- //테이블 리스트 Set -->
	
	
	<br/><br/>
	<a href="#1" onclick="javascript:fn_ftaqList();">목록</a>
</body>
