<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="euc-kr">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale, user-scalable=no, target-densitydpi=medium-dpi" /><!-- user-scalable 설정시 미니멈 스케일과 맥시멈 스케일이 모두 "1" 으로 지정되어 있으면 줌인/줌아웃이 되지 않는다. -->
<title>저작권찾기</title>
<link rel="stylesheet" href="/css/2011/m.css">
<script src="/js/2011/mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/mobile/m_common.js"></script>

<!?[if lt IE 9]> 
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]?>
</head>

<body id="m">
	<!-- 메뉴 건너뛰기 -->
	<div class="m_skip"><a href="#content">본문 바로가기</a></div>
	<hr>
	<!-- 상단 -->
	<c:out value="${commonHtm.headerHtm}" escapeXml="false"/>
	<hr>
	<!-- 본문 -->
	<section id="content">
		<header>
			<h1>저작권정보</h1>
			<h2>방송대본</h2>
		</header>
		
		<article class="list_detl">
			<span class="sh"></span>
			<div>
				<header><h1><c:out value="${scriptDetl.title}"/></h1></header>
				<ul class="detl">
				<li><strong>- 방송대본정보</strong>
					<ul class="news">
					<li><span class="tl">작품제목</span><span class="con"><c:out value="${scriptDetl.title}"/></span></li>
					<li><span class="tl">작품부제목</span><span class="con"><c:out value="${scriptDetl.subtitle}"/></span></li>
   					<li><span class="tl">장르분류</span><span class="con"><c:out value="${scriptDetl.genreStr}"/></span></li>
   					<li><span class="tl">소재분류</span><span class="con"><c:out value="${scriptDetl.subjKind}"/></span></li>
   					<li><span class="tl">연출가</span><span class="con"><c:out value="${scriptDetl.direct}"/></span></li>
   					<li><span class="tl">방송사</span><span class="con"><c:out value="${scriptDetl.broadStat}"/></span></li>
   					<li><span class="tl">방송일자</span><span class="con"><c:out value="${scriptDetl.broadDate}"/></span></li>
					</ul>
				</li>
				<li><strong>- 권리정보</strong>
					<ul>
					<li>작가 : <c:out value="${scriptDetl.writer}"/></li>
   					<li>한국방송작가협회 : <c:out value="${scriptDetl.trustYnStr}"/></li>
					</ul>
				</li>
				</ul>
			</div>
			<div class="list_btn"><a href="javascript:history.back();">목록</a></div>
		</article>
		
	</section>
	<hr>
	<hr>
	<!-- 푸터 -->
	<c:out value="${commonHtm.footerHtm}" escapeXml="false"/>
</body>
</html>