<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
  User user = SessionUtil.getSession(request);
if(user==null){
    user=  SessionUtil.getSSO2(request);
  }
  String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 이용승인신청 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new181203.css">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> -->

<script type="text/javascript"> 

function fn_goList(){
  var frm = document.frm;

  if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
    frm.target = "_self";
    frm.method = "post";
    frm.encoding = "application/x-www-form-urlencoded";
    frm.action = "/stat/statSrch.do";
    frm.submit();
  }
}

function fn_goMyList(){
  var frm = document.frm;

  if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
    frm.target = "_self";
    frm.method = "post";
    frm.encoding = "application/x-www-form-urlencoded";
    frm.action = "/myStat/statRsltInqrList.do";
    frm.submit();
  }
}


//신청자, 대리인 정보 맵핑
function fn_fillDate(oObj, sDiv) {
  var frm = document.frm;

  var dummyResdCorpNumb = '';
  var resdCorpNumb = '';
  <c:choose>
    <c:when test="${userInfo.USER_DIVS == '01' }">
      <c:if test="${userInfo.RESD_CORP_NUMB != null || userInfo.RESD_CORP_NUMB != ''}">
          dummyResdCorpNumb = '${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 0, 6) }-${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 6, 13) }';
          resdCorpNumb = '${userInfo.RESD_CORP_NUMB1 }-${userInfo.RESD_CORP_NUMB2 }';
      </c:if>
    </c:when>
    <c:otherwise>
      dummyResdCorpNumb ='${fn:substring(userInfo.CORP_NUMB, 0, 3)}-${fn:substring(userInfo.CORP_NUMB, 3, 5)}-${fn:substring(userInfo.CORP_NUMB, 5, 10)}'; 
      resdCorpNumb = '${fn:substring(userInfo.CORP_NUMB, 0, 3)}-${fn:substring(userInfo.CORP_NUMB, 3, 5)}-${fn:substring(userInfo.CORP_NUMB, 5, 10)}';
      
    </c:otherwise>
  </c:choose>
  
  if(oObj.checked) {
    if(sDiv == 'APPLR') {
      //신청자
      frm.applrName.value = '${userInfo.USER_NAME }';
      frm.dummyApplrResdCorpNumb.value = dummyResdCorpNumb;
      frm.applrResdCorpNumb.value = resdCorpNumb;
      frm.applrAddr.value = '${userInfo.HOME_ADDR }';
      frm.applrTelx.value = '${userInfo.TELX_NUMB }';
    
    } else {
      //대리인
      frm.applyProxyName.value = '${userInfo.USER_NAME }';
      frm.dummyApplyProxyResdCorpNumb.value = dummyResdCorpNumb;
      frm.applyProxyResdCorpNumb.value = resdCorpNumb;
      frm.applyProxyAddr.value = '${userInfo.HOME_ADDR }';
      frm.applyProxyTelx.value = '${userInfo.TELX_NUMB }';
    
    }
  } else {
    if(sDiv == 'APPLR') {
      //신청자
      frm.applrName.value = '';
      frm.dummyApplrResdCorpNumb.value = '';
      frm.applrResdCorpNumb.value = '';
      frm.applrAddr.value = '';
      frm.applrTelx.value = '';
    
    } else {
      //대리인
      frm.applyProxyName.value = '';
      frm.dummyApplyProxyResdCorpNumb.value = '';
      frm.applyProxyResdCorpNumb.value = '';
      frm.applyProxyAddr.value = '';
      frm.applyProxyTelx.value = '';
    
    }
  }
}

function openSmplDetail(div) {

  var param = '';
  
  param = 'DVI='+div;
  
  var url = '/common/rghtPrps_smpl.jsp?'+param
  var name = '';
  var openInfo = 'target=rghtPrps_mvie, width=705, height=570, scrollbars=yes';
  
  window.open(url, name, openInfo);
}

//신청서 구분(신청서명) 동기화
function fn_chkSycn(oObj, sNo) {
  var oChkApplyType = document.getElementsByTagName("input");
  for(i = 0; i < oChkApplyType.length; i++) {
    if(oChkApplyType[i].type == "checkbox") {
      
      if( ((oChkApplyType[i].id).toUpperCase()).indexOf("APPLYTYPE0") > -1 && oChkApplyType[i].id != oObj.id) {
        oChkApplyType[i].checked = false;
      }
    }
  }
  
  document.getElementById("dummyApplyType0"+sNo).checked = oObj.checked;

}

//저작권법 항목 
function fn_chkRawCd(sNo, sChk) {
  var oObjs = document.getElementsByName("dummyApplyRawCd");
  
  for(var i = 0; i < oObjs.length; i++) {
    if(oObjs[i].value == sNo &&sChk == true) {
      oObjs[i].checked = true;
    } else {
      oObjs[i].checked = false;
    }
  }
  /*
  document.getElementById("applyRawCd").value = sNo;
  */
  document.getElementById("applyRawCd").value = "";
}

/* 첨부화일 관련 시작 */
//첨부화일 테이블 idx
var iRowIdx = 0;

//순번 재지정1
function fn_resetSeq1(){
    var oSeq = document.getElementsByName("fileNameCd");
    for(i=0; i<oSeq.length; i++){
        oSeq[i].value = i+1;
    }
}
<c:if test="${isModi == '01'}">
  //첨부화일 추가
  function fn_addRow1(){
      var oTbl = document.getElementById("tblAttachFile");
      var oTbody = oTbl.getElementsByTagName("TBODY")[0];
      
      var oTr = document.createElement("TR");
      
    //순번(선택)
    var oTd = document.createElement("TD");
    var sTag = '<input name=\"chkDel1\" id=\"chkDel1_'+iRowIdx+'\" type=\"checkbox\" />';
    sTag += '<input name=\"fileNameCd\" id=\"fileNameCd_'+iRowIdx+'\" type=\"hidden\" value=\"99\" style=\"border:0px;\" readonly=\"readonly\"  />';
    oTd.innerHTML = sTag;
    oTd.style.width = '10%';
    oTd.className = 'ce';
    oTr.appendChild(oTd);
    
    //서류구분
    oTd = document.createElement("TD");
    oTd.innerHTML = '기타';
    oTd.style.width = '35%';
    oTr.appendChild(oTd);
      
    //첨부화일명
    oTd = document.createElement("TD");
    sTag = '<input type=\"hidden\" name=\"fileDelYn\" />';
    sTag += '<span id=\"spfile'+iRowIdx+'\"><input type=\"file\" name=\"file'+iRowIdx+'\" id=\"file_'+iRowIdx+'\" class=\"inputData L w100\" onkeydown=\"return false;\" onchange=\"fn_chkFileType(this,'+iRowIdx+');\"/></span>';
    sTag += '<input name=\"hddnFile\" id=\"hddnFile_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"fileName\" id=\"fileName_'+iRowIdx+'\" type=\"text\" onkeyup=\"onkeylengthMax(this, 255, this.name);\" class=\"inputDate w95\" style=\"display:none;\" />';
    
    sTag += '<input name=\"realFileName\" id=\"realFileName_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"fileSize\" id=\"fileSize_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"filePath\" id=\"filePath_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    oTd.innerHTML = sTag;
    oTr.appendChild(oTd);
    
    oTbody.appendChild(oTr);
    iRowIdx++;
           
  }
  
  //첨부화일 삭제
  function fn_delRow1(){
      var oTbl = document.getElementById("tblAttachFile");
      var oChkDel = document.getElementsByName("chkDel1");
      var iChkCnt = oChkDel.length;
      var iDelCnt = 0;
      
      if(iChkCnt == 1 && oChkDel[0].checked == true){
          oTbl.deleteRow(2);
          iDelCnt++;
      }else if(iChkCnt > 1){
          for(i = iChkCnt-1; i >= 0; i--){
              if(oChkDel[i].checked == true){
                  oTbl.deleteRow(i+2);
                  iDelCnt++;
              }
          }
      }
      
      if(iDelCnt < 1){
          alert('삭제할 첨부화일을 선택하여 주십시요');
      }
  }
</c:if>

<c:if test="${isModi == '02'}" >
  /*수정기능 추가str*/
  //첨부화일 추가
  function fn_addRow1(){
    var oTbl = document.getElementById("tblAttachFile");
    var oTbody = oTbl.getElementsByTagName("TBODY")[0];
    
    var oTr = document.createElement("TR");
    
    //순번(선택)
    var oTd = document.createElement("TD");
    var sTag = '<input name=\"chkDel1\" id=\"chkDel1_'+iRowIdx+'\" type=\"checkbox\" />';
    sTag += '<input name=\"fileNameCd\" id=\"fileNameCd_'+iRowIdx+'\" type=\"hidden\" value=\"99\" style=\"border:0px;\" readonly=\"readonly\"  />';
    oTd.innerHTML = sTag;
    oTd.style.width = '7%';
    oTd.className = 'ce';
    oTr.appendChild(oTd);
    
    //서류구분
    oTd = document.createElement("TD");
    oTd.innerHTML = '기타';
    oTd.style.width = '25%';
    oTr.appendChild(oTd);
    
    //첨부화일명
    oTd = document.createElement("TD");
    oTd.colSpan = 2;
    sTag = '<input type=\"hidden\" name=\"fileDelYn\" id=\"fileDelYn_'+iRowIdx+'\" />';
    sTag += '<span id=\"spfile'+iRowIdx+'\"><input type=\"file\" name=\"file'+iRowIdx+'\" id=\"file_'+iRowIdx+'\" class=\"inputData L w100\" onkeydown=\"return false;\" onchange=\"fn_chkFileType(this,'+iRowIdx+');\"/></span>';
    sTag += '<input name=\"hddnFile\" id=\"hddnFile_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"fileName\" id=\"fileName_'+iRowIdx+'\" type=\"text\" onkeyup=\"onkeylengthMax(this, 255, this.name);\" class=\"inputDate w95\" style=\"display:none;\" />';
    
    sTag += '<input name=\"realFileName\" id=\"realFileName_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"fileSize\" id=\"fileSize_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"filePath\" id=\"filePath_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    oTd.innerHTML = sTag;
    oTr.appendChild(oTd);
    
    oTbody.appendChild(oTr);
    iRowIdx++;
           
  }
  
  //첨부화일 삭제
  function fn_delRow1(){
    var oTbl = document.getElementById("tblAttachFile");
    var oChkDel = document.getElementsByName("chkDel1");
    var iChkCnt = oChkDel.length;
    var iDelCnt = 0;
    
    /* if(iChkCnt == 1 && oChkDel[0].checked == true){
        //수정시 기타 관련 파일의 tr에 ID가 존재함
        //해당 체크 박스의 idx와 동일한 tr ID가 존재하면 해당 파일은 기존에 입력 되었던 파일
      var oTr = document.getElementById("trAttcFile_D0");
        if(oTr == null) { //해당 ID를 가진 tr 이 없으면 현재 화면에서 새로 추가한 기타 파일
            oTbl.deleteRow(2);
            
        }else{  //해당 ID를 가진 tr이 있으면 기존에 입력 되었던 파일
          oTr.style.display = "none";   //화면상에서 보이지만 않게 함
          document.getElementById("fileDelYn_D0").value = "Y";  //파일 처리 구분을 삭제로 셋팅
          oChkDel[0].checked = false;   //체크박스 체크 해제
        }
        iDelCnt++;
    }else if(iChkCnt > 1){
        for(i = iChkCnt-1; i >= 0; i--){
            if(oChkDel[i].checked == true){
              //수정시 기타 관련 파일의 tr에 ID가 존재함
              //해당 체크 박스의 idx와 동일한 tr ID가 존재하면 해당 파일은 기존에 입력 되었던 파일
              var oTr = document.getElementById("trAttcFile_D"+(oChkDel[i].id).substr((oChkDel[i].id).indexOf('_')+1, (oChkDel[i].id).length));
              if(oTr == null) { //해당 ID를 가진 tr 이 없으면 현재 화면에서 새로 추가한 기타 파일
                    oTbl.deleteRow(i+2);
                    
                }else{  //해당 ID를 가진 tr이 있으면 기존에 입력 되었던 파일
                oTr.style.display = "none";   //화면상에서 보이지만 않게 함
                document.getElementById("fileDelYn_D"+(oChkDel[i].id).substr((oChkDel[i].id).indexOf('_')+1, (oChkDel[i].id).length)).value = "Y";  //파일 처리 구분을 삭제로 셋팅
                oChkDel[i].checked = false;   //체크박스 체크 해제
              }
                iDelCnt++;
            }
        }
    } */
    if(iChkCnt > 0){
        for(i = iChkCnt-1; i >= 0; i--){
            if(oChkDel[i].checked == true){
              //수정시 기타 관련 파일의 tr에 ID가 존재함
              //해당 체크 박스의 idx와 동일한 tr ID가 존재하면 해당 파일은 기존에 입력 되었던 파일
              var oTr = document.getElementById("trAttcFile_D"+(oChkDel[i].id).substr((oChkDel[i].id).indexOf('_')+1, (oChkDel[i].id).length));
              if(oTr == null) { //해당 ID를 가진 tr 이 없으면 현재 화면에서 새로 추가한 기타 파일
                    oTbl.deleteRow(i+2);
                    
                }else{  //해당 ID를 가진 tr이 있으면 기존에 입력 되었던 파일
                oTr.style.display = "none";   //화면상에서 보이지만 않게 함
                document.getElementById("fileDelYn_D"+(oChkDel[i].id).substr((oChkDel[i].id).indexOf('_')+1, (oChkDel[i].id).length)).value = "Y";  //파일 처리 구분을 삭제로 셋팅
                oChkDel[i].checked = false;   //체크박스 체크 해제
              }
              iDelCnt++;
            }
      }
    }
    
    if(iDelCnt < 1){
        alert('삭제할 첨부화일을 선택하여 주십시요');
    }
  }
  /*수정기능 추가end*/
</c:if>


//첨부화일 확장자 제한
function fn_chkFileType(obj,iRowIdx){
  
  
  
  var frm = document.frm;
  var refuseFile = ["HTM","HTML","PHP","PHP3","ASP","JSP","CGI","INI","PL","TIF","PPT","DOCX","DOC","GUL"];
  var str = obj.value;
  var nPos = 0;
  var sepCnt = 0;
  var isAlert = false;
  var msg = "";
  
  //확장자 구하기
  if( str != null ){
    var splitLength = (str.split(".")).length;    
    str = str.split(".")[splitLength-1];
  }

  //확장자 구하기
  /*
  while(nPos >= 0){
    nPos = str.indexOf(".");
    if(nPos> 0){
      str = str.substring(nPos+1,str.length);
    }
    sepCnt++;
  }
  */
   
  for(i=0; i<refuseFile.length; i++){
    if(msg.length > 0){
      msg += ", ";
    }
    msg += refuseFile[i];
    
    if(str.toUpperCase() == refuseFile[i]){
      isAlert = true;
    }
  }
  
  if(sepCnt > 2 || isAlert == true){
    alert(msg + "확장자를 가진 화일이나 \n\r이중확장자(\"---.---.---\") 화일은 올리실 수 없습니다. ");
    
    var spObj = document.getElementById("spfile"+iRowIdx);
      spObj.outerHTML = '<span id=\"spfile'+iRowIdx+'\"><input type=\"file\" name=\"file\" id=\"file_'+iRowIdx+'\" class=\"inputData L w100\" onkeydown=\"return false;\" onchange=\"fn_chkFileType(this,\''+iRowIdx+'\');\"/></span>';
      return false;
  }
  
  if(iRowIdx == 'D1') {
    frm.existYn.value = 'Y';
  }
}
/* 첨부화일 관련 끝*/

//table row 별 필수체크
function checkTr(oTr) {
  
  
  var result = ""; 
  
  if(oTr.id == "applrInfo1"){
    //alert(oTr.id);
    var oInput = oTr.getElementsByTagName("input");
    
    for(z = 0; z<oInput.length; z++){
      if(checkField2(oInput[z]) == false){
        result = oInput[z].id;
        break;
      }else{
        
      }   
    }
  }
  
  if(oTr.id == "applrInfo2"){
    var oTextArea = oTr.getElementsByTagName("textarea");
    var oInput = oTr.getElementsByTagName("input");
    for(z = 0; z<oTextArea.length; z++){
      if(checkField2(oTextArea[z]) == false){
        result = oTextArea[z].id;
        break;
      }else{
        for(z = 0; z<oInput.length; z++){
          if(checkField2(oInput[z]) == false){
            result = oInput[z].id;
            break;
          }   
        }
      }   
    }
  }
  
  return result;
}

function checkForm3(forms) {
  var result = ""; 

  if(typeof(forms) != 'undefined') {
    for(y = 0; y<forms.length; y++) {
      if(checkField2(forms[y]) == false)  {
        result = forms[y].id;
        break;
      }
    }
  }
  else
    result = "";
    
  return result;
}

//필수체크
function fn_chkValue() {
  var frm = document.frm;
    var chk1=document.getElementById("ApplyCheck").checked;
    if(!chk1){
      alert("개인정보 수집에 동의해주세요.");
      return false;
    }
  //신청인정보 체크
  var oTr = document.getElementById("applrInfo1");
  tgtId = checkTr(oTr);
  
  if(tgtId != "") {
    document.getElementById(tgtId).focus();
    return false;
  }
  
    //신청인 주민/사업자번호 체크
  var sApplrResdCorpNumb = document.getElementById("applrResdCorpNumb").value;
  if(sApplrResdCorpNumb !=""){
    while(sApplrResdCorpNumb.indexOf('-') > -1){
      sApplrResdCorpNumb = sApplrResdCorpNumb.replace('-', '');
    }
    if(sApplrResdCorpNumb.length == 10){
      /*if(!check_busino(sApplrResdCorpNumb)){
        document.getElementById("dummyApplrResdCorpNumb").focus();
          alert("유효하지 않은 사업자등록번호입니다.");
          return;
      }*/
      if(!check_busino(sApplrResdCorpNumb)){
        document.getElementById("dummyApplrResdCorpNumb").focus();
          alert("유효하지 않은 법인등록번호입니다.");
          return;
      }
    } else if(sApplrResdCorpNumb.length == 13){
      var sStr1 = sApplrResdCorpNumb.substr(0,6);
      var sStr2 = sApplrResdCorpNumb.substr(6,7);
      if(ssnCheck3(sStr1, sStr2)){
        document.getElementById("dummyApplrResdCorpNumb").focus();
        return;
      }
    } else {
      document.getElementById("dummyApplrResdCorpNumb").focus();
      alert("유효하지 않은 주민등록번호입니다.");
      return;
    }
  }
  
  var oTr = document.getElementById("applrInfo2");
  tgtId = checkTr(oTr);
  
  if(tgtId != "") {
    document.getElementById(tgtId).focus();
    return false;
  }
  
  

  
  //대리인 주민/사업자번호 체크
  var sApplyProxyResdCorpNumb = document.getElementById("applyProxyResdCorpNumb").value;
  if(sApplyProxyResdCorpNumb !=""){
    while(sApplyProxyResdCorpNumb.indexOf('-') > -1){
      sApplyProxyResdCorpNumb = sApplyProxyResdCorpNumb.replace('-', '');
    }
    if(sApplyProxyResdCorpNumb.length == 10){
      /*if(!check_busino(sApplyProxyResdCorpNumb)){
        document.getElementById("dummyApplyProxyResdCorpNumb").focus();
          alert("유효하지 않은 사업자등록번호입니다.");
          return;
      }*/
      if(!check_busino(sApplyProxyResdCorpNumb)){
        document.getElementById("dummyApplyProxyResdCorpNumb").focus();
          alert("유효하지 않은 법인등록번호입니다.");
          return;
      }
    } else if(sApplyProxyResdCorpNumb.length == 13){
      var sStr1 = sApplyProxyResdCorpNumb.substr(0,6);
      var sStr2 = sApplyProxyResdCorpNumb.substr(6,7);
      if(ssnCheck3(sStr1, sStr2)){
        document.getElementById("dummyApplyProxyResdCorpNumb").focus();
        return;
      }
    } else {
      document.getElementById("dummyApplyProxyResdCorpNumb").focus();
      alert("유효하지 않은 주민등록번호입니다.");
      return;
    }
  }
  
  var chkCnt = 0;
  var focusTgt = '';
  
  //신청서 구분 체크여부 확인
  <c:set var="dySeq" value="${0}"/>
  <c:forEach items="${applyTypeList}" var="applyTypeList">
    <c:set var="dySeq" value="${dySeq + 1}"/>
    if('${dySeq}' == 1) {
      focusTgt = 'applyType0${dySeq }';
    }
    if(document.getElementById("applyType0${dySeq }").checked) {
      chkCnt++;
    }
  </c:forEach>
  if(chkCnt == 0) {
    alert("신청서 구분을(를) 선택하세요.");
    document.getElementById(focusTgt).focus();
    return false;
  }
  
  //폼체크
  var tgtId = checkForm3(frm);
      
  if(tgtId != "") {
  
    document.getElementById(tgtId).focus();
    return false;
  }
  
  //저작권법 값 셋팅
  /* var oObjs = document.getElementsByName("dummyApplyRawCd");
  chkCnt = 0;
  for(var i = 0; i < oObjs.length; i++) {
    if(oObjs[i].checked == true) {
      document.getElementById("applyRawCd").value = oObjs[i].value;
      chkCnt++;
    }
  }
  if(chkCnt == 0) {
    alert("저작권법을(를) 선택하세요.");
    document.getElementById(oObjs[0].id).focus();
    return false;
  } */
  
  
  
  //법정허락 대상 저작물 추가 체크
  var seListChkObjs = jQuery("#tbl_rghtPrps").find("input[name=iChk]:checkbox");
  
  if(seListChkObjs.length == 0){
    alert("신청 저작물을 추가하세요.");
    return false;
  }
  
  var isNotSearch = false;
  for(var i=seListChkObjs.length-1; i >= 0; i--){
    var chkObj = seListChkObjs[i];
    
    if(chkObj.value == ""){
      isNotSearch = true;
    }
    /* //var oTR = findParentTag(chkObj,"TR");
    if(eval(oTR)){
      oTR.parentNode.removeChild(oTR);
    } */
  }
  
  if(isNotSearch){
    alert("추가된 로우에 신청 저작물을 등록하세요.")
    return false;
  }
  
  
    
  //첨부화일
  var oInput = document.getElementsByTagName("input");
  var arrVal = new Array();
  var arrCnt = 0;
  for(i=0; i<oInput.length; i++){
    var tmpId = oInput[i].id;
    if(tmpId.substring(0,5) == "file_"){
      var tmpVal = oInput[i].value;
      var sep = 0;
      for(j=0; j<tmpVal.length; j++){
            if(tmpVal.charCodeAt(j) == 92){ // 아스키 92 = '\'
                sep = j;
            }
        }
      arrVal[arrCnt] = tmpVal.substring(sep,tmpVal.length);
      arrVal[arrCnt] = arrVal[arrCnt].replace("\\","");
      
      arrCnt++;
    }
  }
  arrVal.length = arrCnt;
  
  var oHddnFile = document.getElementsByName("hddnFile");
  for(i=0; i<oHddnFile.length; i++){
    /*
    if(arrVal[i].length < 1){
      alert('파일명을 입력하세요');
      return false;
    }
    */
    <c:if test="${isModi == '02'}">
      if(i==0){
        var isDel = jQuery("#fileDelYnChkD2").attr("checked");
        if(isDel == 'checked'){
          jQuery("#fileDelYn_D2").val('Y');
        }
        if(arrVal[i] != ''){
          //alert(arrVal[i]);
          jQuery("#fileDelYn_D2").val('Y');
        }
      }
    </c:if>
    oHddnFile[i].value = arrVal[i];
  }
  
  var worksId = '';
  
  var seListChkObjs = jQuery("#tbl_rghtPrps").find("input[name=iChk]:checkbox");
  
  jQuery(seListChkObjs).each(function(index){
    jQuery(this).attr("checked",true);
    
    if(index==0){
      worksId += jQuery(this).val();
    }else{
      worksId += ","+jQuery(this).val();
    }
  });
  
  document.frm.worksId.value = worksId;
    
  return true;
}

//임시저장 및 화면이동
function fn_doSave(sDiv) {
  var frm = document.frm;
  frm.action_div.value = sDiv;

  //로그인 체크
  var userId = '<%=sessUserIdnt%>';
  if(userId == 'null' || userId == ''){
    alert('로그인이 필요한 화면입니다.');
    location.href = "/user/user.do?method=goSgInstall";
    return;
    
  }else{
    if(fn_chkValue()) {
    
      /* if(sDiv == 'goStep2' && frm.applyWorksCnt.value > 5 && frm.existYn.value != 'Y') {
        alert('신청건이 5건 초과 시,\n\r \'이용승인명세서\' 관련 첨부파일을 첨부하셔야 합니다.');
        return;
      } */
      //선택 저작물인 관계로 이용승인명세서 파일 첨부 기능 제외
    
      frm.target = "_self";
      frm.method = "post";
      frm.action = "/stat/statPrpsList.do";
      frm.submit();
    }
  }
}

function fn_matchResdCorpNumb(oObj) {
  var frm = document.frm;

  if(oObj.id == 'dummyApplrResdCorpNumb') {
    frm.applrResdCorpNumb.value = oObj.value;
  }
  if(oObj.id == 'dummyApplyProxyResdCorpNumb') {
    frm.applyProxyResdCorpNumb.value = oObj.value;
  }
}

function fn_goSample(){
  window.open("/images/2011/smpl/stat_step1.jpg", "", "width=770,height=850,scrollbars=yes");
}

function fn_fileDownLoad(filePath, fileName, realFileName) {
  var frm = document.form1;
  frm.filePath.value     = filePath;
  frm.fileName.value     = fileName;
  frm.realFileName.value = realFileName;

  frm.action = "/board/board.do?method=fileDownLoad";
  frm.submit();
} 

var defaultHeight ='';

function resizeSelTbl(){
  //var chkObjs = document.getElementsByName("chk");
  //document.getElementById("div_inmtPrps").style.height = (31.5 * (chkObjs.length + 1)) + 13 + "px" ;
  
  resizeDiv("tbl_rghtPrps", "div_rghtPrps");
}

//table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight; //해당 Div의 높이
   var chkId = "iChk"//체크박스 네임
   var chkCnt = document.getElementsByName(chkId).length;//현재건수
   var isLong = false;//현재 건수가 15는 넘었는지
   if(chkCnt > 10){
    document.getElementById(targetName).style.height = defaultHeight+"px";
    document.getElementById(targetName).style.overflowY = "auto";
   }else{
    document.getElementById(targetName).style.height = "auto";
    document.getElementById(targetName).style.overflowY = "hidden";
   }
   //console.log(chkCnt+"개");
   //console.log("the_height[현재의높이는]: "+the_height);
   //console.log("default_height[현재의높이는]: "+default_height);
   
   //document.getElementById(targetName).style.height = the_height+13;
}

var statCd = '${statApplication.statCd}';

jQuery(function(){
  
  //alert("상태코드는:"+statCd);
  
  jQuery("#addTr").click(function(){
    var tBody = jQuery("#tbl_rghtPrps").find("tbody");
    
    jQuery("#tbl_rghtPrps").find("input[type=text]").each(function(index){
      if(jQuery(this).hasClass("currBox")){
        jQuery(this).removeClass("currBox");
      }
    })
    
    var oTr = jQuery("<tr>").addClass("ce");
  
    var chk = jQuery("<input>").attr({
      "type":"checkbox"
      ,"name":"iChk"
      ,"title":"선택"
      ,"value":""
    }).css({
      "padding":"0px",
      "margin":"0px"
    });
    var oTd = jQuery("<td>").addClass("ce");
    chk.appendTo(oTd);
    oTd.appendTo(oTr);
    
    oTd = jQuery("<td>").addClass("ce");
    oTd.appendTo(oTr);
    
    oTd = jQuery("<td>").addClass("ce");
    oTd.appendTo(oTr);
    
    var addInputText = jQuery("<input>").attr({
      "type":"text",
      "class":"currBox inputText"
    }).addClass("w80 mr10");
    
    var addButton = jQuery("<input>").attr({
      "type":"button",
      "value":"검색",
      "id":"openPop"
    });
    
    var addSpan = jQuery("<span>").addClass("button small black");
    
    addButton.appendTo(addSpan);
    
    oTd = jQuery("<td>").css("text-align","left");
    
    addInputText.appendTo(oTd);
    addSpan.appendTo(oTd);
    oTd.appendTo(oTr);
    
    oTd = jQuery("<td>").addClass("ce");
    oTd.appendTo(oTr);

    //jQuery(tBody).children().eq(0).css("display","block");
    
    oTr.appendTo(tBody);
    
    
    jQuery(".currBox").focus();
    
    jQuery("#tbl_rghtPrps").find(".inputText").each(function(index){
      if(jQuery(this).hasClass("keyEvent")){
        jQuery(this).removeClass("keyEvent");
      }
    })
    var size = jQuery("#tbl_rghtPrps").find(".inputText").size();
    
    jQuery("#tbl_rghtPrps").find(".inputText").each(function(index){
      if(index == (size-1)){
        jQuery(this).addClass("keyEvent");
      }
    })
    
    var trObjs = jQuery("#tbl_rghtPrps").contents().find("tr");
    
    
    
    jQuery(trObjs).each(function(index){
      if(index != 0){
        jQuery(this).children().eq(1).text(index);
      }
    })
    
    if(trObjs.length == 11){
      defaultHeight = jQuery("#tbl_rghtPrps").height();
    }
    
    resizeSelTbl();
    
    
    <c:if test="${isMy != 'Y'}">
    autoFill();
    </c:if>
  })
  
  jQuery("input[name='fileDelYnChk']").each(function(index){
    jQuery(this).bind('click',function(index){
        var fileName = jQuery(this).parent().children().eq(0).css({"text-decoration":"line-through"});
    })  
  })
  
  jQuery("#delTr").click(function(){
    
    var seListChkObjs = jQuery("#tbl_rghtPrps").find("input[name=iChk]:checkbox:checked");
    
    if(seListChkObjs.length == 0){
      alert("선택된 저작물이 없습니다.");
      return;
    }
    
    for(var i=seListChkObjs.length-1; i >= 0; i--){
      var chkObj = seListChkObjs[i];
      
      var oTR = findParentTag(chkObj,"TR");
      if(eval(oTR)){
        oTR.parentNode.removeChild(oTR);
      }
    }
    
    var trObjs = jQuery("#tbl_rghtPrps").contents().find("tr");
    
    jQuery(trObjs).each(function(index){
      if(index != 0){
        jQuery(this).children().eq(1).text(index);
      }
    })
    
    resizeSelTbl();
    jQuery("#chk").removeAttr("checked");
    
    autoFill();
  });
  
  jQuery(".inputText").live("click",function(){
    
    jQuery("#tbl_rghtPrps").find(".inputText").each(function(index){
      if(jQuery(this).hasClass("keyEvent")){
        jQuery(this).removeClass("keyEvent");
      }
    })
    
    jQuery(this).addClass("keyEvent");
    //var thisParentObj = jQuery(this).parent().parent();
  })
  
  jQuery(".keyEvent").live("keypress",function(e){
  /*  jQuery("#tbl_rghtPrps").find(".inputText").each(function(index){
      if(jQuery(this).hasClass("keyEvent")){
        jQuery(this).removeClass("keyEvent");
      }
    }) */
    if(e.keyCode == 13){
      var thisParentObj = jQuery(this).parent().parent();
      var srchKeyword = jQuery(this).val();
      
      jQuery("#tbl_rghtPrps").find("tr").each(function(index){
        if(jQuery(this).hasClass("isAddClass")){
          jQuery(this).removeClass("isAddClass");
        }
      })
      
      thisParentObj.addClass("isAddClass");
      
      if(srchKeyword == "" || srchKeyword == null){
        alert("제호(제목)을 입력해야합니다.");
        jQuery(this).focus();
        return false;
      }
      
      window.open("","popForm1","toolbar=0,scrollbars=1,directories=0,location=0,status=0,menubar=0,width=800,height=563");
      
      document.popupForm.searchKeyword1.value=srchKeyword;
      document.popupForm.target = "popForm1";
      document.popupForm.action = "/stat/statPopSrchList.do";
      document.popupForm.submit();
    }
  })
  
  jQuery("#openPop").live("click",function(){
    
    var thisParentObj = jQuery(this).parent().parent().parent();
    
    var srchKeyword = thisParentObj.children().eq(3).children().eq(0).val();
    
    jQuery("#tbl_rghtPrps").find("tr").each(function(index){
      if(jQuery(this).hasClass("isAddClass")){
        jQuery(this).removeClass("isAddClass");
      }
    })
    
    thisParentObj.addClass("isAddClass");
    
    if(srchKeyword == "" || srchKeyword == null){
      alert("제호(제목)을 입력해야합니다.");
      jQuery(this).parent().parent().children().eq(0).focus();
      return false;
    }
    
    window.open("","popForm1","toolbar=0,scrollbars=1,directories=0,location=0,status=0,menubar=0,width=800,height=563");
    
    document.popupForm.searchKeyword1.value=srchKeyword;
    document.popupForm.target = "popForm1";
    document.popupForm.action = "/stat/statPopSrchList.do";
    document.popupForm.submit();
  });
  
  //페이징
  function goPage(pageNo){
    $("input[name=pageNo]").val(pageNo);
    
    $("form[name=ifFrm]").attr({
      "target":"_self"
      , "method":"post"
      , "action":"/stat/subStatList.do"
    }).submit();
  }
  
})

function autoFill(){
    var mainTitle ="";
    var reqCnt = jQuery("#tbl_rghtPrps").find("tr").size()-1;
    
    if(reqCnt == 0){
      jQuery("#isCntChk").attr("checked",false);
    }else if(reqCnt == 1){
      jQuery("#isCntChk").attr("checked",false);
      jQuery("#tbl_rghtPrps").find("tr").each(function(index){
        if(index == 1){
          mainTitle = jQuery(this).children().eq(3).text();
        }
      })
    }else{
      jQuery("#tbl_rghtPrps").find("tr").each(function(index){
        if(index == 1){
          if(statCd == '3' || statCd =='4'){
            mainTitle = jQuery(this).children().eq(2).text();
          }else{
            mainTitle = jQuery(this).children().eq(3).text();
          }
          
        }
      })
      jQuery("#isCntChk").attr("checked",true);
    }
    jQuery("#mainTitle").val(mainTitle);
    jQuery("#reqCnt").val(reqCnt);
  }

jQuery(window).load(function(){
    var trObjs = jQuery("#tbl_rghtPrps").contents().find("tr");

    if(trObjs.length >= 11){
      var maxHeight = jQuery("#tbl_rghtPrps").height();
      var trHeight = (maxHeight/trObjs.length)*11;
      defaultHeight = trHeight;
    }
  
    resizeSelTbl();
    
    <c:if test="${isMy != 'Y'}">
    autoFill();
    </c:if>
    
/*    jQuery("input[name^='iChk']").filter(function(index){
      alert("test");
      jQuery(this).attr({
        "name":"iChk"+index,
        "id":"iChkID"+index
      })
    })
    
    jQuery("body").find("input[name^=iChk]").each(function(index){
      var name = jQuery(this).attr("name");
      var id = jQuery(this).attr("id");
      
      alert(index+"번째 id: "+id+", name: "+name);
    }) */
  })

 -->
</script>
<script>
/*  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-69621660-2', 'auto');
    ga('send', 'pageview'); */
</script>


<style type="text/css">
<!--

.grid tbody tr td.tbBorder{
  
  border-right-color: #ff0000;
}

-->
</style>
<link type="text/css" rel="stylesheet" href="/css/new181203.css">
</head>
<body>
  <c:set var="isMyPage" value=""/>
  <c:if test="${isMy == 'Y' }">
    <c:set var="isMyPage" value="Y"/>
  </c:if>
  <!-- 전체를 감싸는 DIVISION -->

    <!-- HEADER str-->
    <!-- 2017변경 -->
    <jsp:include page="/include/2017/header.jsp" />
    <c:if test="${isMyPage=='Y'}">
      <script type="text/javascript">initNavigation(0);</script>
    </c:if>
    <c:if test="${isMyPage==''}">
      <script type="text/javascript">initNavigation(3);</script>
    </c:if>
    <form name="form1" method="post" action="#">
      <input type="hidden" name="filePath"> <input type="hidden"
        name="fileName"> <input type="hidden" name="realFileName">
      <input type="hidden" name="action_div"> <input type="submit"
        style="display: none;">
    </form>
    <form name="srchForm" action="#">
      <c:if test="${isMyPage == 'Y'}">
        <input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
        <input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
        <input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
        <input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
        <input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
        <input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
      </c:if>
      <c:if test="${isMyPage == ''}">
        <input type="hidden" name="pageNum"   value="${srchVO.pageNum}" />
        <input type="hidden" name="searchCondition" value="${srchVO.searchCondition}" />
        <input type="hidden" name="searchKeyword1"  value="${srchVO.searchKeyword1}" />
      </c:if>
      <input type="submit" style="display:none;">
    </form>
    <form id="popupForm" name="popupForm" method="post">
      <!-- 레스포트 정보 -->
      <input type="hidden" name="searchKeyword1" id="searchKeyword1" >
    </form> 
    <div id="contents">
      <!-- 래프 -->
        <c:if test="${isMyPage=='Y'}">
        <div class="con_lf">
          <div class="con_lf_big_title">법정허락<br>승인 신청</div>
          <ul class="sub_lf_menu">
          <!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
          <!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
          <!-- <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
          <li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
          <li><a class="on" href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
          <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
          <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
          </ul>
        </div>
        </c:if>
        <c:if test="${isMyPage==''}">
          <!-- <div class="con_lf">
          <div class="con_lf_big_title">저작권자 찾기</div>
          <ul class="sub_lf_menu">
            <li><a href="/mlsInfo/liceSrchInfo01.jsp">저작권자 검색 및 상당한 노력 신청</a>
              <ul class="sub_lf_menu2 disnone">
                <li><a href="/mlsInfo/liceSrchInfo01.jsp">소개 및 이용방법</a></li>
                <li><a href="/srchList.do">서비스 이용</a></li>
                <li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
              </ul>
            </li>
            <li><a href="/mlsInfo/statInfo01.jsp" class="on">법정허락 승인 신청</a>
              <ul class="sub_lf_menu2">
                <li><a href="/mlsInfo/statInfo01.jsp">소개 및 이용방법</a></li>
                <li><a href="/stat/statSrch.do" class="on">서비스 이용</a></li>
                <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>
              </ul>
            </li>
          </ul>
        </div> -->
        <div class="con_lf">
          <div class="con_lf_big_title">법정허락<br>승인 신청</div>
          <ul class="sub_lf_menu">
          <!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
          <!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
          <!-- <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
          <li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
          <li><a class="on" href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
          <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
          <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
          </ul>
        </div>
        </c:if>
        <%-- 
        
        <!-- 래프 -->
        <c:if test="${isMyPage=='Y'}">
          <jsp:include page="/include/2012/myPageLeft.jsp" />
          <script type="text/javascript">
            subSlideMenu("sub_lnb","lnb1","lnb14");
          </script>
        </c:if>
        <c:if test="${isMyPage==''}">
          <div class="con_lf">
          <div class="con_lf_big_title">저작권자 찾기</div>
          <ul class="sub_lf_menu">
                <!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
          <!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
          <!-- <li><a class="on" href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
            <li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
            <li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
            <li><a class="on" href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
            <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
            <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
            <li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li>
          </ul>
        </div>
        </c:if>
        <!-- //래프 -->
<!--         <iframe id="goLogin" title="법정허락 공인인증서 설치 아이프레임"
          src="/include/sg_install.html" frameborder="0" width="242"
          height="139" scrolling="no" marginwidth="0" marginheight="0"
          style="display: none;"></iframe>
        
        <div id="ajaxBox"
          style="position: absolute; z-index: 1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left: -500px; width: 306px; height: 38px; padding: 102px 0 0 0;">
          <p style="height: 38px; text-align: center; margin: 0;">
            <img src="/images/2012/common/loading.gif" alt=""
              style="margin-top: -4px; margin-bottom: 3px;" /><br /> <span
              id="ajaxBoxMent"
              style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만
              기다려주세요..</span>
          </p> -->
        </div> --%>

        <!-- 주요컨텐츠 str -->
        <div class="con_rt" id="contentBody">
          <c:if test="${isMyPage=='Y'}">
            <div class="con_rt_head">
            <img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
            &gt;
            저작권자 찾기
            &gt;
            법정허락 승인 신청
            &gt;
            <span class="bold">서비스 이용</span>
            </div>
            <div class="con_rt_hd_title">법정허락 승인 신청</div>
          </c:if>
          <c:if test="${isMyPage==''}">
            <div class="con_rt_head">
            <img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
            &gt;
            저작권자 찾기
            &gt;
            법정허락 승인 신청
            &gt;
            <span class="bold">서비스 이용</span>
            </div>
            <div class="con_rt_hd_title">법정허락 승인 신청</div>
          </c:if>
          <!-- section -->
          <div id="sub_contents_con">
            <div class="list_step_apply">
          <ul>
            <li class="active">
              <div class="wrap">
                <p>STEP 01</p>
                <strong>법정허락 승인 신청 서류 작성</strong>
              </div>
            </li>
            <li>
              <div class="wrap">
                <p>STEP 02</p>
                <strong>첨부서류 제출하기</strong>
              </div>
            </li>
            <li>
              <div class="wrap">
                <p>STEP 03</p>
                <strong>신청정보 확인 및 제출하기</strong>
              </div>
            </li>
          </ul>
        </div>
            
            <!-- // process --><!-- 여기 시작 -->
            <form name="frm" action="#" enctype="multipart/form-data">
              <c:if test="${isMyPage == 'Y'}">
                <input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
                <input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
                <input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
                <input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
                <input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
                <input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
                <input type="hidden" name="worksId" value="${worksId}" />
              </c:if>
              
              <c:if test="${isMyPage == ''}">
                <input type="hidden" name="pageNo" value="${srchVO.pageNum}"/>
                <input type="hidden" name="searchCondition" value="${srchVO.searchCondition}"/>
                <input type="hidden" name="searchKeyword1" value="${srchVO.searchKeyword1}"/>
                <input type="hidden" name="worksId" value="${srchVO.searchWorksId}" />
              </c:if>
              <input type="hidden" name="isMy" value="${isMy}"/>
              <input type="hidden" name="applyDivsCd" value="2"/>
              <input type="hidden" name="action_div" value=""/>
              
              <c:if test="${isModi == '02' }">
                <!-- 수정기능 추가str -->
                <input type="hidden" name="statCd" value="${statApplication.statCd}"/>
                <input type="hidden" name="applyWriteYmd" value="${statApplication.applyWriteYmd }">
                <input type="hidden" name="applyWriteSeq" value="${statApplication.applyWriteSeq }">
                <!--  -->
              </c:if>
              
              <input type="submit" style="display:none;">
            <div class="article">
              <h2 class="sub_con_h2 mar_tp50">회원정보</h2>
              <span class="topLine"></span>
              <!-- 그리드스타일 -->
            <table class="sub_tab td_padding tbl_apply mar_tp15" summary="한국저작권위원회" width="100%" cellspacing="0" cellpadding="0">
          <caption>한국저작권위원회</caption>
          <colgroup>
            <col style="width:106px">
            <col style="width:278px">
            <col style="width:178px">
            <col style="width:auto">
          </colgroup>
          <tbody>
            <tr>
                    <th>성명(법인명)<strong class="required">*</strong></th>
                    <td>${userInfo.USER_NAME }</td>
                    <th scope="row">주민등록번호(법인등록번호)</th>
                    <td class="td_center">
                      <c:choose>
                        <c:when test="${userInfo.USER_DIVS == '01' }">
                          <c:if test="${userInfo.RESD_CORP_NUMB != null || userInfo.RESD_CORP_NUMB != ''}">
                              ${userInfo.RESD_CORP_NUMB1 }-${userInfo.RESD_CORP_NUMB2 }
                          </c:if>
                          <c:if test="${userInfo.RESD_CORP_NUMB != null || userInfo.RESD_CORP_NUMB != ''}">
                            ${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 0, 6) }-${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 6, 13) }
                          </c:if>
                        </c:when>
                        <c:otherwise>
                          ${fn:substring(userInfo.CORP_NUMB, 0, 3)}-${fn:substring(userInfo.CORP_NUMB, 3, 5)}-${fn:substring(userInfo.CORP_NUMB, 5, 10)}
                        </c:otherwise>
                      </c:choose>
                    </td>
                  </tr>
                  <tr>
                    <th>주소</th>
                    <td>${userInfo.HOME_ADDR }</td>
                    <th>전화번호</th>
                    <td class="td_center">
                      ${userInfo.TELX_NUMB }
                      <input type="hidden" name="rgstIdnt" value="${userInfo.USER_IDNT }"/>
                    </td>
                  </tr>
                </tbody>
              </table>
              <!-- //그리드스타일 -->
            </div>
            <div class="article">
              <!-- 신청인 정보 str -->
              <h2 class="sub_con_h2 mar_tp50">신청인 정보
              <div class="btm_form_box">
                <input type="checkbox" id="chkApplr" onclick="fn_fillDate(this, 'APPLR');" onkeypress="fn_fillDate(this, 'APPLR');"/>
                <label for="chkApplr">회원정보와 동일</label>
              </div>
              
              </h2>
              <span class="topLine"></span>
                <table class="sub_tab tbl_apply mar_tp15" summary="한국저작권위원회" width="100%" cellspacing="0" cellpadding="0">
                <caption>한국저작권위원회</caption>
                <colgroup>
                  <col style="width:106px">
                  <col style="width:278px">
                  <col style="width:178px">
                  <col style="width:auto">
                </colgroup>
                <tbody>
                  <tr id="applrInfo1">
                    <th for="applrName">성명(법인명)<strong class="required">*</strong></th>
                    <td><input type="text" class="input_style1" id="applrName" name="applrName" rangeSize="0~200" title="성명(법인명)" value="${statApplication.applrName }" nullCheck/></td>
                    <th for="dummyApplrResdCorpNumb">주민등록번호(법인등록번호)<strong class="required">*</strong></th>
                    <td>
                      <input type="text"  id="dummyApplrResdCorpNumb" name="dummyApplrResdCorpNumb"  
                        rangeSize="0~20" title="주민등록번호(법인등록번호)" nullCheck value="${statApplication.dummyApplrResdCorpNumb }"
                        onchange="fn_matchResdCorpNumb(this)"/>
                      <input type="hidden"  class="input_style1 w106" id="applrResdCorpNumb" name="applrResdCorpNumb" value="${statApplication.applrResdCorpNumb }">
                    </td>
                  </tr>
                  <tr id="applrInfo2">
                    <th for="applrAddr">주소<strong class="required">*</strong></th>
                    <td>
                    <textarea cols="10" class="inputData w90" style="height: 30px !important" rows="2" id="applrAddr" name="applrAddr" title="주소" rangeSize="0~200" nullCheck>
                      ${statApplication.applrAddr }</textarea></td>
                    <!--<td><input type="text" id="applrAddr" name="applrAddr" class="w98" rangeSize="0~200" title="주소" nullCheck/></td>-->
                    <th for="applrTelx">전화번호<strong class="required">*</strong></th>
                    <td><input type="text" id="applrTelx" name="applrTelx" value="${statApplication.applrTelx }" rangeSize="0~20" title="전화번호" nullCheck/></td>
                  </tr>
                </tbody>
              </table>
              <!-- 신청인 정보 end -->
            </div>
            <div class="article">
              <h2 class="sub_con_h2 mar_tp50">
              대리인 정보
              <div class="btm_form_box">
              <input type="checkbox" id="chkApplyProxy" onclick="fn_fillDate(this, 'APPLYPROXY');" onkeypress="fn_fillDate(this, 'APPLYPROXY');" />
              <label for="chkApplyProxy">회원정보와 동일</label>
              </div>
              </h2>
              <table class="sub_tab tbl_apply mar_tp15" summary="한국저작권위원회" width="100%" cellspacing="0" cellpadding="0">
                <caption>한국저작권위원회</caption>
                <colgroup>
                  <col style="width:106px">
                  <col style="width:278px">
                  <col style="width:178px">
                  <col style="width:auto">
                </colgroup>
                <tbody>
                  <tr>
                    <th for="applyProxyName">성명(법인명)</th>
                    <td><input type="text" class="input_style1" id="applyProxyName" name="applyProxyName"  value="${statApplication.applyProxyName }" rangeSize="0~200"/></td>
                    <th  for="dummyApplyProxyResdCorpNumb">주민등록번호(법인등록번호)</th>
                    <td>
                      <input type="text" id="dummyApplyProxyResdCorpNumb" value="${statApplication.dummyApplyProxyResdCorpNumb }" name="dummyApplyProxyResdCorpNumb"  
                        rangeSize="0~20"
                        onchange="fn_matchResdCorpNumb(this)"/>
                      <input type="hidden" id="applyProxyResdCorpNumb" name="applyProxyResdCorpNumb" value="${statApplication.applyProxyResdCorpNumb }">
                    </td>
                  </tr>
                  <tr>
                    <th  for="applyProxyAddr">주소</th>
                    <td><textarea cols="10" class="inputData w90" style="height: 30px !important" rows="2" id="applyProxyAddr" name="applyProxyAddr" title="주소" rangeSize="0~200">${statApplication.applyProxyAddr }</textarea></td>
                    <!--<td><input type="text" id="applyProxyAddr" name="applyProxyAddr"  class="w98" rangeSize="0~200"/></td>-->
                    <th for="applyProxyTelx">전화번호</th>
                    <td><input type="text" id="applyProxyTelx" name="applyProxyTelx" value="${statApplication.applyProxyTelx }" rangeSize="0~20"/></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <c:if test="${statApplication.statCd != '3' && statApplication.statCd != '4' }">
            <!-- //선택저작물 목록 str -->
            <div class="floatDiv">
              <h3 class="sub_con_h2 mar_tp30">법정허락 이용 승인신청 저작물 목록</h3>
              <p class="fr">
                <img alt="추가" id="addTr" src="/images/2012/button/add.gif" style="cursor: pointer;">
                <img alt="삭제" id="delTr" src="/images/2012/button/delete.gif" style="cursor: pointer;">
              </p>
            </div>

            <!-- 그리드스타일 -->
            <div id="div_rghtPrps" style="width: 100%;">
            <table id="tbl_rghtPrps" cellspacing="0" cellpadding="0"
              border="1" summary="" class="grid">
              <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->

              <colgroup>
                <col width="3%">
                <col width="6%">
                <col width="10%">
                <col width="*">
                <col width="20%">
              </colgroup>
              <thead>
                <tr>
                  <th scope="col"><input type="checkbox" id="chk"
                    class="vmid"
                    onclick="javascript:checkBoxToggle('frm','iChk',this);"
                    style="cursor: pointer;" title="전체선택" />
                  </th>
                  <th scope="col">순번</th>
                  <th scope="col">장르</th>
                  <th scope="col">저작물 제호</th>
                  <th scope="col">구분</th>
                </tr>
              </thead>
              <tbody>
              <c:if test="${!empty mlStatWorksList}">
              <c:forEach items="${mlStatWorksList}" var="mlStatWorks">
                <c:set var="i" value="${i+1}"/>
                <tr>
                  <td class="ce"><input type="checkbox" name="iChk" value="${mlStatWorks.worksId}" style="padding:0px;margin:0px;" title="선택" ></td>
                  <td class="ce">
                    <label for="co1"><c:out value="${i}"/></label>
                  </td>
                  <td class="ce">
                    <c:forEach items="${genreList}" var="genreList">
                      <c:if test="${genreList.code == mlStatWorks.genreCd}">
                        ${genreList.codeName}
                      </c:if>
                    </c:forEach>
                  </td>
                  <td>${mlStatWorks.worksTitle}</td>
                  <td class="ce">
                    <c:forEach items="${worksDivsList}" var="worksDivsList">
                      <c:if test="${worksDivsList.code == mlStatWorks.worksDivsCd}">
                        ${worksDivsList.codeName}
                      </c:if>
                    </c:forEach>
                  </td>
                </tr>
              </c:forEach>
              </c:if>
              </tbody>
            </table>
            </div>
            </c:if>
            <!-- //그리드스타일 -->
            <!-- //선택저작물 목록 end -->
              <div class="article">
              <h2 class="sub_con_h2 mar_tp30">개인정보 동의</h2>
              <span class="topLine"></span>
              <table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
                <tbody>
                  <tr id ="applycheck">
                    <th scope="row"><label for="applyProxyName">
                    1. 개인 정보 수집의 목적 <br><br>
                       - 저작권 공고에 따른 업무 처리를 위해 아래와 같이 개인 정보를 수집하고 있습니다.  <br><br>
                    
                    2. 개인 정보 내용     <br><br>
                      - 지적 재산권자 정보 : 성명, 주소, 연락처<br> 
                      - 공고자의 정보: 성명, 주소, 연락처  <br><br>
                    
                    3. 개인정보의 보유 및 이용기간 <br><br>
                      - 이용자의 개인정보는 원칙적으로 개인정보의 수집 및 이용목적이 달성되면 지체 없이 파기합니다.<br> 
                      - 따라서 최종 로그인 후 2년이 경과하였거나 정보주체의 회원 탈퇴 신청 시 회원의 개인정보를 지체 없이 파기합니다. <br><br>
            
                    4. 동의 거부 권리 사실 및 불이익 내용 <br><br>
                      -   이용자는 동의를 거부할 권리가 있습니다.<br> 
                      -  동의를 거부할 경우에는 서비스 이용에 제한됨을 알려드립니다. <br><br>
                      </label></th>
                  </tr>
                </tbody>
              </table>
              <h2 style="text-align: center;">위 개인정보 수집,이용에 동의합니다.(필수)<input class="ApplyCheck" type="checkbox" title="동의" id="ApplyCheck"></h2>
            </div>
            <c:if test="${statApplication.statCd == '3' || statApplication.statCd == '4' }">
            <!-- //선택저작물 목록 str -->
            <div class="floatDiv">
              <h3 class="sub_con_h2 mar_tp30">법정허락 이용 승인신청 저작물 목록</h3>
            </div>

            <!-- 그리드스타일 -->
            <div id="div_rghtPrps" style="width: 100%;">
            <table id="tbl_rghtPrps" cellspacing="0" cellpadding="0"
              border="1" summary="" class="grid">
              <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->

              <colgroup>
                <col width="6%">
                <col width="10%">
                <col width="*">
                <col width="20%">
              </colgroup>
              <thead>
                <tr>
                  <th scope="col">순번</th>
                  <th scope="col">장르</th>
                  <th scope="col">저작물 제호</th>
                  <th scope="col">구분</th>
                </tr>
              </thead>
              <tbody>
              <c:if test="${!empty mlStatWorksList}">
              <c:forEach items="${mlStatWorksList}" var="mlStatWorks">
                <c:set var="i" value="${i+1}"/>
                <tr>
                  <td class="ce">
                    <label for="co1"><input type="checkbox" name="iChk" value="${mlStatWorks.worksId}" style="padding:0px;margin:0px;" title="선택" ></label>
                  </td>
                  <td class="ce">
                    <c:forEach items="${genreList}" var="genreList">
                      <c:if test="${genreList.code == mlStatWorks.genreCd}">
                        ${genreList.codeName}
                      </c:if>
                    </c:forEach>
                  </td>
                  <td>${mlStatWorks.worksTitle}</td>
                  <td class="ce">
                    <c:forEach items="${worksDivsList}" var="worksDivsList">
                      <c:if test="${worksDivsList.code == mlStatWorks.worksDivsCd}">
                        ${worksDivsList.codeName}
                      </c:if>
                    </c:forEach>
                  </td>
                </tr>
              </c:forEach>
              </c:if>
              </tbody>
            </table>
            </div>
            </c:if>
            <!-- //그리드스타일 -->
            <!-- //선택저작물 목록 end -->
            
            <div class="article">
              <div class="floatDiv">
                <h2 class="sub_con_h2 mar_tp50 ovflow-h">이용 승인신청 정보</h2>
                <p class="fr"><span class="button small icon"><a href="#1" class="btn_example" onclick="openSmplDetail('MS');" onkeypress="openSmplDetail('MS02')">예시화면 보기</a><span class="help"></span></span></p>
              </div>
              <table class="sub_tab td_padding tbl_apply mar_tp15" summary="한국저작권위원회" width="100%" cellspacing="0" cellpadding="0" >
              <caption>한국저작권위원회</caption>
              <colgroup>
                <col style="width:auto">
                <col style="width:143px">
              </colgroup>
              <tbody>
                <tr>
                               <th colspan="2" >이용 승인신청서</th>
                            </tr>
                             <tr>
                                    <td>
                                      <c:set var="dySeq" value="0"/>
                        <c:forEach items="${applyTypeList}" var="applyTypeList">
                          <c:set var="dySeq" value="${dySeq + 1}"/>
                          <div class="btm_form_box">
                        <input type="checkbox" class="inputRChk" title="선택" id="applyType0${dySeq }" name="applyType0${dySeq }" value="1" title="${applyTypeList.codeName }"
                          onclick="fn_chkSycn(this, '${dySeq }');" onkeypress="fn_chkSycn(this, '${dySeq }');">
                          <label for="applyType0${dySeq }" class="p12">${applyTypeList.codeName }</label>&nbsp;
                            </div>
                          </c:forEach>
                                              </td>
                                              <td><strong>처리기간</strong> : 40일</td>
                                            </tr>
                                           </tbody>
                                           </table>
                              <table class="sub_tab tbl_apply" summary="한국저작권위원회" width="100%" cellspacing="0" cellpadding="0" style="border-top:0">
                  <caption>한국저작권위원회</caption>
                  <colgroup>
                    <col style="width:140px">
                    <col style="width:244px">
                    <col style="width:149px">
                    <col style="width:auto">
                  </colgroup>
                  <tbody>
                                            <tr>
                                            <th for="mainTitle">제호(제목)<strong class="required">*</strong></th>
                                               <td colspan="3">
                                               <div>
                        <div class="btm_form_box ml0">
                                                <input class="inputRChk" type="checkbox" title="저작물" id="isCntChk" <c:if test="${statApplication.applyWorksCnt > 0}">checked="checked"</c:if> />
                                                <label  class="fs13">여러건 신청 : 총</label>
                                                </div>
                                                <input type="text" class="inputData w10" id="reqCnt" name="applyWorksCnt" size="5" value="${statApplication.applyWorksCnt }" character="EK" readonly="readonly"> 건 <p class="mt3">
                                                <div class="btm_form_box">
                                                <input type="text" class="inputData w80" id="mainTitle" value="${statApplication.applyWorksTitl }" name="applyWorksTitl" readonly="readonly"><p>
                                                </div>
                                               </td>
                                           </tr>
                                           <tr>
                                            <th for="applyWorksKind">종류<strong class="required">*</strong></th>
                                               <td>
                                               <input type="text" class="inputData w80" id="applyWorksKind" name="applyWorksKind" value="${statApplication.applyWorksKind }" title="종류" rangeSize="0~100" nullCheck /> 
                                               </td>
                                               <th for="tx2">
                                                형태 및 수량<strong class="required">*</strong>
                                               </th>
                                               <td>
                                               <input type="text" class="inputData w80" id="applyWorksForm" name="applyWorksForm" value="${statApplication.applyWorksForm }" title="형태 및 수량" rangeSize="0~100" nullCheck />
                                               </td>
                                           </tr>
                                           <tr>
                                            <th  for="usexDesc">
                                              이용의 내용<strong class="required">*</strong>
                                            </th>
                                               <td colspan="3">
                                               <!-- <span class="blue2 p11">&lowast; 이용하고자 하는 목적, 방법 등을 상세히 기재</span> -->
                                               <textarea  class="input_style1 height" id="usexDesc" name="usexDesc" title="이용의 내용" rangeSize="0~4000" nullCheck  cols="10" rows="5" style="height: 70px !important"  placeholder="*이용하고자 하는 목적, 방법 등을 상세히 기재해주세요.">${statApplication.usexDesc }</textarea></td>
                                           </tr>
                                           <tr>
                                            <th for="applyReas">
                                              승인신청사유<strong class="required">*</strong>
                                            </th>
                                               <td colspan="3"><!-- <span class="blue2 p11">&lowast; 권리자 거소 확인 불가 등의 해당 사유를 기재</span> -->
                                               <textarea class="input_style1 height" style="height: 70px !important" id="applyReas" name="applyReas" title="승인신청사유" rangeSize="0~4000" nullCheck title="신청내용" rows="5" cols="10" placeholder="*권리자 거소 확인불가 등의 해당 사유를 기재해주세요.">${statApplication.applyReas }</textarea></td>
                                           </tr>
                                           <tr>
                                            <th for="cpstAmnt">
                                            보상금액<strong class="required">*</strong>
                                            </th>
                                               <td colspan="3">
                                               <input type="text" class="input_style1 w224" id="cpstAmnt" value="${statApplication.cpstAmnt }" name="cpstAmnt" title="보상금액" nullCheck rangeSize="0~15">
                                               </td>
                                           </tr>
                                           <tr>
                                            <td colspan="4">
                                            <p class="addition mar_tp20 ml0"><span>※</span> 저작권법 <strong>[제50조, 제51조, 제52조, 제89조, 제97조]에 따라 위와 같이 [저작물, 실연  &#183; 음반 &#183;  방송, 데이터베이스]</strong> 이용의 승인을 신청합니다.</p>
                                            

                      <div class="btn_area mar_tp40">
                        <a href="#1" onclick="fn_doSave('goList');" onkeypress="fn_doSave('goList');" class="btn_style2">임시저장</a>
                        <a href="#1" onclick="fn_doSave('goStep2');" onkeypress="fn_doSave('goStep2');" class="btn_style1">다음단계</a>
                      </div>
                                               </td>            <!-- button area end -->
                                         </tr>
                                </tbody>
                        </table>
                        </div>
          </form>
          </div>
          <!-- //section -->
        </div>
        <!-- //주요컨텐츠 end -->
        <p class="clear"></p>
          
      </div> <!-- contents -->
      <jsp:include page="/include/2017/footer.jsp" />
  <!-- //전체를 감싸는 DIVISION -->
<script type="text/JavaScript">
<!--
  window.onload = function(){

  //이용승인신청_신청서 종류 체크박스 셋팅
  var frm = document.frm;
  
  if('${statApplication.applyType01 }' == '1') {
    frm.applyType01.checked = true;
    frm.dummyApplyType01.checked = true;
  }
  if('${statApplication.applyType02 }' == '1') {
    frm.applyType02.checked = true;
    frm.dummyApplyType02.checked = true;
  }
  if('${statApplication.applyType03 }' == '1') {
    frm.applyType03.checked = true;
    frm.dummyApplyType03.checked = true;
  }
  if('${statApplication.applyType04 }' == '1') {
    frm.applyType04.checked = true;
    frm.dummyApplyType04.checked = true;
  }
  if('${statApplication.applyType05 }' == '1') {
    frm.applyType05.checked = true;
    frm.dummyApplyType05.checked = true;
  }
}
//-->
</script>
</body>
</html>

