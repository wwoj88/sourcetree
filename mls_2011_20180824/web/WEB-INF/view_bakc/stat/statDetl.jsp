<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%
	String bordSeqn = request.getParameter("bordSeqn");
	String menuSeqn = request.getParameter("menuSeqn");
	String srchGubun = request.getParameter("srchGubun") == null ? "" : request.getParameter("srchGubun");
	String srchTitle = request.getParameter("srchTitle") == null ? "" : request.getParameter("srchTitle");
	String srchApprDttm = request.getParameter("srchApprDttm") == null ? "" : request.getParameter("srchApprDttm");
	String srchClmsTite = request.getParameter("srchClmsTite") == null ? "" : request.getParameter("srchClmsTite");
	String srchGenre = request.getParameter("srchGenre") == null ? "" : request.getParameter("srchGenre");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");

	Board boardDTO = (Board) request.getAttribute("board");
	
	String srchFILD1 = request.getParameter("FILD1") == null ? "" : request.getParameter("FILD1");
	String srchFILD2 = request.getParameter("FILD2") == null ? "" : request.getParameter("FILD2");
	String srchFILD3 = request.getParameter("FILD3") == null ? "" : request.getParameter("FILD3");
	String srchFILD4 = request.getParameter("FILD4") == null ? "" : request.getParameter("FILD4");
	String srchFILD5 = request.getParameter("FILD5") == null ? "" : request.getParameter("FILD5");
	String srchFILD6 = request.getParameter("FILD6") == null ? "" : request.getParameter("FILD6");
	String srchFILD7 = request.getParameter("FILD7") == null ? "" : request.getParameter("FILD7");
	String srchFILD8 = request.getParameter("FILD8") == null ? "" : request.getParameter("FILD8");
	String srchFILD9 = request.getParameter("FILD9") == null ? "" : request.getParameter("FILD9");
	String srchFILD10 = request.getParameter("FILD10") == null ? "" : request.getParameter("FILD10");
	String srchFILD11 = request.getParameter("FILD11") == null ? "" : request.getParameter("FILD11");
	String srchFILD99 = request.getParameter("FILD99") == null ? "" : request.getParameter("FILD99");
	String srchFILD = request.getParameter("FILD") == null ? "" : request.getParameter("FILD");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락저작물 | 저작권정보 조회</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>

<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>

<script type="text/javascript" src="/js/2010/prototype.js"> </script>
<script type="text/JavaScript">
<!--
  function fn_ftaqList(){
		var frm = document.form1;
		frm.action = "/board/board.do";
		frm.submit();
	}

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }
	  
	// 음악저작물 상세 팝업오픈 : 부모창에서 오픈
	function openMusicDetail( crId, nrId, meId, bkId, workNm, workFileNm) {
	
		var param = '';
		
		param = '&CR_ID='+crId;
		param += '&NR_ID='+nrId;
		param += '&ALBUM_ID='+meId;

		var frm = document.form1;
		var genre = frm.genre.value
		var openInfo = null;
		if(genre == 1){		// 음악
			param = '&DIVS=M';
			param += '&CR_ID='+crId;
			param += '&NR_ID='+nrId;
			param += '&ALBUM_ID='+meId;
			openInfo = 'target=muscRghtSrch width=705, height=525';
		}else if(genre == 2){	// 도서
			param = '&DIVS=B';
			param += '&CR_ID='+crId;
			param += '&NR_ID='+bkId;
			openInfo = 'target=bookRghtSrch width=705, height=400';
		}else if(genre == 3){	// 방송대본
			param = '&DIVS=C';
			param += '&CR_ID='+crId;
			openInfo = 'target=scriptRghtSrch width=705, height=500';
		}else if(genre == 4){	// 방송
			param = '&DIVS=R';
			param += '&CR_ID='+crId;
			openInfo = 'target=broadcastRghtSrch width=705, height=500';
		}else if(genre == 5){	// 영화
			param = '&DIVS=V';
			param += '&CR_ID='+crId;
			openInfo = 'target=mvieRghtSrch width=705, height=450';
		}else if(genre == 6){	// 이미지
			param = '&DIVS=I';
			param += '&WORK_FILE_NAME='+workFileNm;
			param += '&WORK_NAME='+workNm;
			openInfo = 'target=imageRghtSrch width=320, height=310';
		}


		var url = '/rghtPrps/rghtSrch.do?method=detail'+param
		var name = '';
		//var openInfo = 'target=bookRghtSrch width=705, height=400';
		//var openInfo = 'target=muscRghtSrch width=705, height=525';
		window.open(url, name, openInfo);
	}
//-->
</script>

<script type="text/css">
<!--
.overflow_y{
	overflow-y: scroll;
}
.h300{ height: 300px;}
-->
</script>
</head>
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">

		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2011/header.jsp" /> --%>
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_1.gif" alt="저작권정보조회 및 신청" title="저작권정보조회 및 신청" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>

			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
						<li id="lnb1"><a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2011/content/sub_lnb0101_off.gif" title="음악" alt="음악" /></a></li>
						<li id="lnb2"><a href="/rghtPrps/rghtSrch.do?DIVS=B"><img src="/images/2011/content/sub_lnb0102_off.gif" title="도서" alt="도서" /></a></li>
						<li id="lnb3"><a href="/rghtPrps/rghtSrch.do?DIVS=N"><img src="/images/2011/content/sub_lnb0103_off.gif" title="뉴스" alt="뉴스" /></a></li>
						<li id="lnb4"><a href="/rghtPrps/rghtSrch.do?DIVS=C"><img src="/images/2011/content/sub_lnb0104_off.gif" title="방송대본" alt="방송대본" /></a></li>
						<li id="lnb5"><a href="/rghtPrps/rghtSrch.do?DIVS=I"><img src="/images/2011/content/sub_lnb0105_off.gif" title="이미지" alt="이미지" /></a></li>
						<li id="lnb6"><a href="/rghtPrps/rghtSrch.do?DIVS=V"><img src="/images/2011/content/sub_lnb0106_off.gif" title="영화" alt="영화" /></a></li>
						<li id="lnb7"><a href="/rghtPrps/rghtSrch.do?DIVS=R"><img src="/images/2011/content/sub_lnb0107_off.gif" title="방송" alt="방송" /></a></li>
						<li id="lnb8"><a href="/rghtPrps/rghtSrch.do?DIVS=X"><img src="/images/2011/content/sub_lnb0108_off.gif" title="기타" alt="기타" /></a></li>
						<li id="lnb9"><a href="/noneRght/rghtSrch.do?DIVS=M"><img src="/images/2011/content/sub_lnb0109_off.gif" title="권리자미확인저작물" alt="권리자미확인저작물" /></a>
							<ul>
							<li id="lnb91"><a href="/noneRght/rghtSrch.do?DIVS=M">음악</a></li>
							<li id="lnb92"><a href="/noneRght/rghtSrch.do?DIVS=B">도서</a></li>
							<li id="lnb93"><a href="/noneRght/rghtSrch.do?DIVS=N">뉴스</a></li>
							<li id="lnb94"><a href="/noneRght/rghtSrch.do?DIVS=C">방송대본</a></li>
							<li id="lnb95"><a href="/noneRght/rghtSrch.do?DIVS=I">이미지</a></li>
							<li id="lnb96"><a href="/noneRght/rghtSrch.do?DIVS=V">영화</a></li>
							<li id="lnb97"><a href="/noneRght/rghtSrch.do?DIVS=R">방송</a></li>
							</ul>
						</li>
						<li id="lnb10"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=8&amp;page_no=1"><img src="/images/2011/content/sub_lnb0110_off.gif" title="법정허락 저작물" alt="법정허락 저작물" /></a></li>
					</ul>
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb10");
					</script>
				</div>
				<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>알림마당</span><em>법정허락 저작물</em></p>
					<h1><img src="/images/2011/title/content_h1_0110.gif" alt="법정허락 저작물" title="법정허락 저작물" /></h1>
					<div class="section">
					<!-- 테이블 영역입니다 -->
						<form name="form1" method="post" action="#">
								<input type="hidden" name="bordSeqn" value="<%=bordSeqn%>">
								<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>">
								<input type="hidden" name="srchGubun" value="<%=srchGubun%>">
								<input type="hidden" name="srchTitle" value="<%=srchTitle%>">
								<input type="hidden" name="srchApprDttm" value="<%=srchApprDttm%>">
								<input type="hidden" name="srchClmsTite" value="<%=srchClmsTite%>">
								<input type="hidden" name="srchGenre" value="<%=srchGenre%>">
								<input type="hidden" name="page_no" value="<%=page_no%>">
								<input type="hidden" name="genre" value="<%=boardDTO.getGenre()%>">
								<input type="hidden" name="filePath">
								<input type="hidden" name="fileName">
								<input type="hidden" name="realFileName">
								<input type="hidden" name="FILD" value="<%=srchFILD%>">
								<input type="hidden" name="FILD1" value="<%=srchFILD1%>">
								<input type="hidden" name="FILD2" value="<%=srchFILD2%>">
								<input type="hidden" name="FILD3" value="<%=srchFILD3%>">
								<input type="hidden" name="FILD4" value="<%=srchFILD4%>">
								<input type="hidden" name="FILD5" value="<%=srchFILD5%>">
								<input type="hidden" name="FILD6" value="<%=srchFILD6%>">
								<input type="hidden" name="FILD7" value="<%=srchFILD7%>">
								<input type="hidden" name="FILD8" value="<%=srchFILD8%>">
								<input type="hidden" name="FILD9" value="<%=srchFILD9%>">
								<input type="hidden" name="FILD10" value="<%=srchFILD10%>">
								<input type="hidden" name="FILD11" value="<%=srchFILD11%>">
								<input type="hidden" name="FILD99" value="<%=srchFILD99%>">
								<input type="submit" style="display:none;">
								
						<!-- article str -->
						<div class="article">
							
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="법정허락 저작물 내용을 상세조회 합니다.">
								<colgroup>
								<col width="20%">
								<col width="30%">
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">제목</th>
										<td colspan="3"><%=boardDTO.getTite() == null ? "" : boardDTO.getTite()%></td>
									</tr>
									<tr>
										<th scope="row">이름</th>
										<td><%=boardDTO.getRgstIdnt() == null ? "" : boardDTO.getRgstIdnt()%></td>
										<th scope="row">작성일</th>
										<td><%=boardDTO.getRgstDate() == null ? "" : boardDTO.getRgstDate()%></td>
									</tr>
									<tr>
										<th scope="row">저작물명</th>
										<%if(boardDTO.getClmsTite() == null || boardDTO.getGenre().equals("5") || boardDTO.getGenre().equals("6")){ %>
											<td colspan="3"><b><%=boardDTO.getClmsTite() == null ? "" : boardDTO.getClmsTite()%></b></td>
										<%}else{%>
											<td colspan="3" onclick="javascript:openMusicDetail('<%=boardDTO.getCrId()%>','<%=boardDTO.getNrId()%>','<%=boardDTO.getAlbumId()%>','<%=boardDTO.getBookNrId()%>','<%=boardDTO.getClmsTite()%>','<%=boardDTO.getWorkFileName()%>');" style="cursor:pointer;"><b><%=boardDTO.getClmsTite() == null ? "" : boardDTO.getClmsTite()%></b></td>
										<%}%>
									</tr>
									<tr>
										<th scope="row">공고승인구분</th>
										<td><%=boardDTO.getGubunName()%></td>
										<th scope="row">공고/승인일자</th>
										<td><%=boardDTO.getApprDttm() == null ? "" : boardDTO.getApprDttm()%></td>
									</tr>
									<tr>
										<th scope="row">ICN번호</th>
										<%if(boardDTO.getIcn() == null){ %>
										<td><%=boardDTO.getIcn() == null ? "" : boardDTO.getIcn()%></td>
										<%}else{ %>
										<td onclick="javascript:openMusicDetail('<%=boardDTO.getCrId()%>','<%=boardDTO.getNrId()%>','<%=boardDTO.getAlbumId()%>','<%=boardDTO.getBookNrId()%>','<%=boardDTO.getClmsTite()%>','<%=boardDTO.getWorkFileName()%>');" style="cursor:pointer;"><%=boardDTO.getIcn() == null ? "" : boardDTO.getIcn()%></td>
										<% }%>
										<th scope="row">저작(인접)권자</th>
										<td><%=boardDTO.getLicensorName() == null ? "" : boardDTO.getLicensorName()%></td>
									</tr>
									<tr>
										<th scope="row">분야</th>
										<td colspan="3">
										<input type="checkbox" name="FILD_1" id="FILD_1" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_1() == 1){ %> checked="checked"<% }%>/><label for="FILD_1">도서</label>&nbsp;
										<input type="checkbox" name="FILD_2" id="FILD_2" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_2() == 1){ %> checked="checked"<% }%>/><label for="FILD_2">음악</label>&nbsp;
										<input type="checkbox" name="FILD_3" id="FILD_3" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_3() == 1){ %> checked="checked"<% }%>/><label for="FILD_3">연극</label>&nbsp;
										<input type="checkbox" name="FILD_4" id="FILD_4" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_4() == 1){ %> checked="checked"<% }%>/><label for="FILD_4">미술</label>&nbsp;
										<input type="checkbox" name="FILD_5" id="FILD_5" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_5() == 1){ %> checked="checked"<% }%>/><label for="FILD_5">건축</label>&nbsp;
										<input type="checkbox" name="FILD_6" id="FILD_6" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_6() == 1){ %> checked="checked"<% }%>/><label for="FILD_6">사진</label>&nbsp;
										<input type="checkbox" name="FILD_7" id="FILD_7" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_7() == 1){ %> checked="checked"<% }%>/><label for="FILD_7">영상</label><br>
										<input type="checkbox" name="FILD_8" id="FILD_8" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_8() == 1){ %> checked="checked"<% }%>/><label for="FILD_8">도형</label>&nbsp;
										<input type="checkbox" name="FILD_9" id="FILD_9" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_9() == 1){ %> checked="checked"<% }%>/><label for="FILD_9">컴퓨터프로그램</label>&nbsp;
										<input type="checkbox" name="FILD_10" id="FILD_10" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_10() == 1){ %> checked="checked"<% }%>/><label for="FILD_10">2차저작물</label>&nbsp;
										<input type="checkbox" name="FILD_11" id="FILD_11" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_11() == 1){ %> checked="checked"<% }%>/><label for="FILD_11">편집</label>&nbsp;
										<input type="checkbox" name="FILD_99" id="FILD_99" class="inputRChk" onclick="javascript:return(false);" <%if(boardDTO.getFild_99() == 1){ %> checked="checked"<% }%>/><label for="FILD_99">기타</label>
										</td>
									</tr>
									<tr>
										<th scope="row">내용</th>
										<td colspan="3">
										<%=CommonUtil.replaceBr(boardDTO.getBordDesc(),true)%>
										</td>
									</tr>
									<%
									          	List fileList = (List) boardDTO.getFileList();
									          	int listSize = fileList.size();
									
									          	if (listSize > 0) {
									%>
									<tr>
										<th scope="row">첨부파일</th>
										<td colspan="3">
										<%
							          	  for(int i=0; i<listSize; i++) {
							          	  	Board fileDTO = (Board) fileList.get(i);
										%>
										<a href="#1" onclick="javascript:fn_fileDownLoad('<%=fileDTO.getFilePath()%>','<%=fileDTO.getFileName()%>','<%=fileDTO.getRealFileName()%>')"><%=fileDTO.getFileName()%></a>
										<%
											            }
										%>
										</td>
									</tr>
									<%
										          }
									%>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_ftaqList();">목록</a></span></p>
							</div>
						</div>
						<!-- //article end -->
						</form>
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
	</body>
</html>
