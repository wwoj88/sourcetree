<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	String sessSsnNo = user.getSsnNo();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 승인 신청 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<style type="text/css">
<!--
.box {
	background-color:black;
	position:absolute;
	z-index:99999999;
	display:none;
}

body {
	overflow-x: auto;  
	overflow-y: auto;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}

.w92{width:91%;}
.w26{width:26%;}
-->
</style>

<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<!--
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
-->
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<!--
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
-->

<script type="text/JavaScript">
<!--
	/* var test = '${genreList}';
	alert(test); */
	
	var defaultHeight = 0;
	
	(function($) { // function 에 $를 삽입한다. $를 사용하기 위한 함수를 만든다.
		$(function() {         // 실제사용할 함수를 만든다.
			$(window).load(function(){
				fn_InitIframeView();
			
			
			})
		});
	})(jQuery);
	

	//검색이 없는 첫화면 아이프레임 뷰
	function fn_InitIframeView(){
		var isEmptyYn = '${mlStatWorksVO.pageNum}';
		
		if(isEmptyYn == '' || isEmptyYn == null){
			document.frm.pageNo.value = '1';
		}else{
			document.frm.pageNo.value = isEmptyYn;
		}
		
		jQuery("#frm").attr({
			"target":"ifSubStatList"
			, "method":"post"
			, "action":"/stat/subStatList.do"
		}).submit();
		
	}

	//str
jQuery(window).resize(function(e){
	var docuWidth = jQuery(document).width()-16;
	var docuHeight = jQuery(document).height();
	
	

	jQuery("#modalBox").css({width:docuWidth,height:docuHeight});
	
	   var yp=document.body.scrollTop;
	   var xp=document.body.scrollLeft;

	   var ws=document.body.clientWidth;
	   var hs=document.body.clientHeight;
	   
	   var ajaxBox=$('ajaxBox');
	   //$('ajaxBoxMent').innerHTML=ment;
	   ajaxBox.style.top=yp+eval(hs)/2-100+"px";
	   ajaxBox.style.left=xp+eval(ws)/2-360+"px";
});
//end

// 로딩 이미지 박스
function showAjaxBox(ment){
	//항상 화면 중앙에 나타나도록...
   var yp=document.body.scrollTop;
   var xp=document.body.scrollLeft;
   

   var ws=document.body.clientWidth;
   var hs=document.body.clientHeight;
   
   
   if(!ment) ment="잠시만 기다려주세요..";
   
   var ajaxBox=$('ajaxBox');
  // $('ajaxBoxMent').innerHTML=ment;

   ajaxBox.style.top=yp+eval(hs)/2-100+"px";
   ajaxBox.style.left=xp+eval(ws)/2-360+"px";

	//str
	var docuWidth = jQuery('#wrap').width();
	var docuHeight = jQuery('#wrap').height();
	
   //back_blackBox
	 jQuery("<div>").addClass("box").attr("id","modalBox").
		css({width:docuWidth,height:docuHeight,left:0,top:0}).appendTo("body"); 
	 	jQuery("#modalbox").fadeTo(100, 0.3);
		jQuery("#modalBox").click(function(){
			jQuery("#modalBox").remove();
	})
	//end

  Element.show(ajaxBox);    
}

// 로딩 이미지 박스 감추기
function hideAjaxBox(){
	Element.hide('ajaxBox');	
	
	//str
	jQuery("#modalBox").remove();
	//end
	
	// 통합검색에서 넘어온경우 알림메시지
	//alram();
}
	
	//리사이즈
	function resizeIFrame(name) {
		var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
		jQuery("#"+name).attr("height",the_height+5);
		
		//var size = jQuery("#ifSubStatList").contents().find("#tab_scroll").height();
		//alert("size: "+size);
	}
	
	//iframe 검색
	function fn_frameList(){
		
		
		//showAjaxBox();		// 로딩 이미지 박스 보이게..
		new Ajax.Request('/test', {   
			onLoading: function() {
				//showAjaxBox();
			},
			onSuccess: function(req) {     
				// Do something with req.responseXML/Text .. ...   
			},
			onComplete: function() {
				showAjaxBox();
				//alert("완료");
			} 
		});
		

		jQuery("#frm").attr({
			"target":"ifSubStatList"
			, "method":"post"
			, "action":"/stat/subStatList.do"
		}).submit();
	}
	
	//저작물 추가
	function fn_add(){
		
		var tBody = jQuery("#tbl_rghtPrps").find("tbody");
		
		var chkObjs = jQuery("#ifSubStatList").contents().find("input[name=iChk]:checkbox:checked");
		
		var seListChkObjs = jQuery("#tbl_rghtPrps").find("input[name=chk]:checkbox");
		//alert(seListChkObjs.length);
		
		var isExistYn = "N";	//중복여부
		var isAdd = false;		//줄 추가여부
		
		
		if(chkObjs.length == 0){
			alert("선택된 저작물이 없습니다.");
			return;
		}
		
		jQuery(chkObjs).each(function(){
			isAdd = false;		//줄 추가여부
			var worksId = jQuery(this).val();//선택 저작물ID
			var genreCd = trim(jQuery(this).parent().parent().children().eq(1).text());//장르
			var worksTitle = jQuery(this).parent().parent().children().eq(2).text();//저작물제호
			var coptHodr = jQuery(this).parent().parent().children().eq(3).text();//저작권자 정보
			var worksDivsCd = trim(jQuery(this).parent().parent().children().eq(5).text());//구분
			
			if(seListChkObjs.length>0){
				for(var j=0; j<seListChkObjs.length;j++){
					if(seListChkObjs[j].value == worksId){
						isExistYn = "Y";
						alert("선택된 저작물 ["+worksTitle+"]는 이미 추가된 저작물입니다.");
						jQuery(this).removeAttr("checked");
						return;
					}else{
						isExistYn = "N";
					}
				}
			}
			
			if(isExistYn == "N"){
				isAdd = true;
			}else{
				isAdd = false;
				alert("선택된 저작물 ["+worksTitle+"]는 이미 추가된 저작물입니다.");
				jQuery(this).removeAttr("checked");
			}
			
			if(isAdd){
				var oTr = jQuery("<tr>");
				
				var chk = jQuery("<input>").attr({
					"type":"checkbox"
					,"value":worksId
					,"name":"chk"
					,"title":"선택"
				})
				var oTd = jQuery("<td>").addClass("ce");
				chk.appendTo(oTd);
				oTd.appendTo(oTr);
				
				oTd = jQuery("<td>").text(genreCd).addClass("ce");
				oTd.appendTo(oTr);
				
				oTd = jQuery("<td>").text(worksTitle);
				oTd.appendTo(oTr);
				
				
				oTd = jQuery("<td>").text(coptHodr).addClass("ce");
				oTd.appendTo(oTr);
				
				oTd = jQuery("<td>").text(worksDivsCd).addClass("ce");
				oTd.appendTo(oTr);
				
				jQuery(tBody).children().eq(0).css("display","none");
				//jQuery(tBody).children().eq(0).css("display","block");
				
				oTr.appendTo(tBody);
				
				var trObjs = jQuery("#tbl_rghtPrps").contents().find("tr");
				
				if(trObjs.length == 12){
					defaultHeight = jQuery("#tbl_rghtPrps").height();
				}
				
				resizeSelTbl();
			}
		})
		
		/* if(chkObjs.length < = 0){
			alert("선택된 저작물이 없습니다.");
			return;
		} */
	}
	
	//높이 조절
	function resizeSelTbl(){
		//var chkObjs = document.getElementsByName("chk");
		//document.getElementById("div_inmtPrps").style.height = (31.5 * (chkObjs.length + 1)) + 13 + "px" ;
		
		resizeDiv("tbl_rghtPrps", "div_rghtPrps");
	}
	
	//table name 사이즈에 대한 div targetName리사이즈
	function resizeDiv(name, targetName) {

	   var the_height = document.getElementById(name).offsetHeight; //해당 Div의 높이
	   var chkId = "chk"//체크박스 네임
	   var chkCnt = document.getElementsByName(chkId).length;//현재건수
	   var isLong = false;//현재 건수가 15는 넘었는지
	   if(chkCnt > 10){
	   	document.getElementById(targetName).style.height = defaultHeight+"px";
	   	document.getElementById(targetName).style.overflowY = "auto";
	   }else{
	   	document.getElementById(targetName).style.height = "auto";
	   	document.getElementById(targetName).style.overflowY = "hidden";
	   }
	   //console.log(chkCnt+"개");
	   //console.log("the_height[현재의높이는]: "+the_height);
	   //console.log("default_height[현재의높이는]: "+default_height);
	   
	   //document.getElementById(targetName).style.height = the_height+13;
	}
	
	function fn_delete(){
		var seListChkObjs = jQuery("#tbl_rghtPrps").find("input[name=chk]:checkbox:checked");
		
		if(seListChkObjs.length==0){
			alert("선택된 저작물이 없습니다.");
			return;
		}
		
		for(var i=seListChkObjs.length-1; i >= 0; i--){
			var chkObj = seListChkObjs[i];
			
			var oTR = findParentTag(chkObj,"TR");
			if(eval(oTR)){
				oTR.parentNode.removeChild(oTR);
			}
		}
		//높이 조절
		resizeSelTbl();
		jQuery("#sc20").removeAttr("checked");
		if(jQuery("#tbl_rghtPrps").find("input[name=chk]:checkbox").length ==0){
			var tBody = jQuery("#tbl_rghtPrps").find("tbody");
			jQuery(tBody).children().eq(0).css("display","block");
		}
	}
	
	//법정허락 신청
	function fn_goSetp1(sDiv){
		var frm = document.frm;
		//로그인 체크
		var userId = '<%=sessUserIdnt%>';
		
		var userName = '<%=sessUserName%>';
		
		
		if(document.hidForm.isSsnNo.value == ''){
			document.hidForm.isSsnNo.value = '<%=sessSsnNo%>';
		}
		
		var isSsnNo = document.hidForm.isSsnNo.value;
		
		var chkObjs = jQuery("#ifSubStatList").contents().find("input[name=iChk]:checkbox:checked");
		
		var seListChkObjs = jQuery("#tbl_rghtPrps").find("input[name=chk]:checkbox:checked");
		
		if(userId == 'null' || userId == ''){
			alert('로그인이 필요한 화면입니다.');
			//location.href = "/user/user.do?method=goLogin";
			location.href = "/user/user.do?method=goSgInstall";
			return;
		}else{
	
			
				var isExist = 'N';
				var hddnFrm ='';
				var ifForm = '';
				if(sDiv == '01'){
					hddnFrm = document.hidForm;
					hddnFrm.action = "/stat/statPrpsMain_priv.do";
					
					hddnFrm.searchCondition.value = frm.searchCondition.value;
					hddnFrm.searchKeyword1.value = frm.searchKeyword1.value;
					
					
					hddnFrm.target = "_self";
					hddnFrm.method = "post";
					
					hddnFrm.submit();
					
				}else if(sDiv == '02'){
					if(chkObjs.length == 0){
						alert("저작물을 선택해주세요");	
						jQuery("#ifSubStatList").contents().find("input[name=iChk]").eq(0).focus();
						return;
					}
					hddnFrm = document.hidForm;
					ifForm = document.getElementById('ifSubStatList').contentWindow.document.ifFrm;
					hddnFrm.action = "/stat/statPrpsMain.do";
					
					if(inspectCheckBoxField(ifForm.iChk)){
						//var ifrmInputVal = (getCheckStr(ifForm, 'Y'));
						//alert(ifrmInputVal);
						var chkObjs = jQuery("#ifSubStatList").contents().find("input[name=iChk]:checkbox:checked");
						
						var worksId = '';
						
						jQuery(chkObjs).each(function(index){
							if(index==0){
								worksId += jQuery(this).val();
							}else{
								worksId += ","+jQuery(this).val();
							}
						});
						
						hddnFrm.worksId.value = worksId;
						hddnFrm.pageNo.value = document.getElementById('ifSubStatList').contentWindow.document.ifFrm.pageNo.value;
						hddnFrm.searchCondition.value = frm.searchCondition.value;
						hddnFrm.searchKeyword1.value = frm.searchKeyword1.value;
						
						hddnFrm.target = "_self";
						hddnFrm.method = "post";
						
						hddnFrm.submit();
					}
				}else if(sDiv == '03'){
					if(seListChkObjs.length == 0){
						alert("저작물을 선택해주세요");	
						jQuery("#tbl_rghtPrps").contents().find("input[name=chk]").eq(0).focus();
						return;
					}
					hddnFrm = document.hidForm;
					//ifForm = document.getElementById('ifSubStatList').contentWindow.document.ifFrm;
					hddnFrm.action = "/stat/statPrpsMain.do";
					
					if(inspectCheckBoxField(frm.chk)){
						//var ifrmInputVal = (getCheckStr(ifForm, 'Y'));
						//alert(ifrmInputVal);
						var chkObjs = jQuery("input[name=chk]:checkbox:checked");
						
						var worksId = '';
						
						jQuery(chkObjs).each(function(index){
							if(index==0){
								worksId += jQuery(this).val();
							}else{
								worksId += ","+jQuery(this).val();
							}
						});
						
						hddnFrm.worksId.value = worksId;
						hddnFrm.pageNo.value = document.getElementById('ifSubStatList').contentWindow.document.ifFrm.pageNo.value;
						hddnFrm.searchCondition.value = frm.searchCondition.value;
						hddnFrm.searchKeyword1.value = frm.searchKeyword1.value;
						
						hddnFrm.target = "_self";
						hddnFrm.method = "post";
						
						hddnFrm.submit();
					}
				}
			// }
		}
	}
	
	// 상당한노력신청(거소불명)
	function statBoRegiPop(){
		var userId = '<%=sessUserIdnt%>';
		
		var userName = '<%=sessUserName%>';
		
	  if(userId == 'null' || userId == ''){
			alert('로그인이 필요한 화면입니다.');
			//location.href = "/user/user.do?method=goLogin";
			location.href = "/user/user.do?method=goSgInstall";
			return;
		}else{
			window.open("/statBord/statBo06Regi.do", "small", "width=800, height=800, scrollbars=yes, menubar=no, location=yes");	
		}
	}
	

//-->
</script>
<script>
</script> 
</head>
<body>
  		<div id="ajaxBox" style="position:absolute; z-index:99999999999; background: url(/images/2012/common/loadingBg.gif) no-repeat 0 0; left:-500px; width: 402px; height: 56px; padding: 102px 0 0 0;">
				  
		</div>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />

		
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		
		<div id="contents">
				<!-- 래프 -->
				<div class="con_lf">
					<div class="con_lf_big_title">저작권자 찾기</div>
					<ul class="sub_lf_menu">
							<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
					<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
					<!-- <li><a class="on" href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
						<li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
						<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
						<li><a class="on" href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
						<!-- <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li> -->
						<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
						<!-- <li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li> -->
					</ul>
				</div>
				<!-- 래프 -->
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						저작권자 찾기
						&gt;
						법정허락 승인 신청
						&gt;
						<span class="bold">서비스 이용</span>
					</div>
					<div class="con_rt_hd_title">법정허락 승인 신청</div>
					<form name="hidForm" action="#" class="sch mt20">
						<input type="hidden" name="worksId" />
						<input type="hidden" name="isSsnNo" />
						<input type="hidden" name="pageNo" />
						<input type="hidden" name="searchCondition" />
						<input type="hidden" name="searchKeyword1" />
						<input type="submit" style="display:none;">
					</form>
					<form name="frm" id="frm" action="#" class="sch">
					<input type="hidden" name="pageNo"/>
					<div id="sub_contents_con">
						<!-- -->
					<div class="sub01_con_bg4_tp mar_tp30"></div>
						<div class="sub01_con_bg4">
							<div class="font15"><span class="color_2c65aa">법정허락 상당한 노력의 이행신청이 완료된 저작물과 법정허락 승인 신청대상물을 조회하여 승인 신청을 합니다.</span></div>
							<h3 class="mar_tp10">단, 직접 저작권자 조회 공고(개인신청)을 진행한 이용자는 검색을 진행하지 않고 [개인 승인 신청] 버튼을 클릭하여 별도 페이지에서 신청하실 수 있습니다.</h3>
						</div>
						<!-- // -->
						<h2 class="sub_con_h2 mar_tp30">법정허락 승인 신청 대상물</h2>
						
						<!-- 검색 -->
							<table class="sub_tab2 mar_tp20" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
								<caption>법정허락 승인 신청</caption>
								<colgroup>
									<col width="17%"/>
									<col width="18%"/>
									<col width="15%"/>
									<col width="20%"/>
									<col width="*"/>
								</colgroup>
								<tbody>
									<tr>
										<th scope="col"><label for="searchCondition">장르</label></th>
										<td>
											<select class="w30"  id="searchCondition" name="searchCondition" style="width:95%;">
											<option value="">전체 ----------</option>
											<c:forEach items="${genreList}" var="genreList">
												<option value="${genreList.code}" <c:if test="${mlStatWorksVO.searchCondition == genreList.code}">selected="selected"</c:if>>${genreList.codeName}</option>
											</c:forEach>													
											</select>
										</td>
										<td colspan="2">&nbsp;</td>
										<td rowspan="3" class="bro175_posi">
											<a href="#fn_frameList" onkeypress="javascript:fn_frameList();" onclick="javascript:fn_frameList();" class="music_search"><img src="/images/sub_img/sub_25.gif" alt="조회" /></a>
											<div class="bro130"></div>
										</td>
									</tr>
									<tr>
										<th scope="col"><label for="searchKeyword1">제목</label></th>
										<td>
											<input type="text" title="제목" class="inputData w70" id="searchKeyword1" name="searchKeyword1" value="${mlStatWorksVO.searchKeyword1 }" style="width:88%;">
										</td>
										<th scope="col"><label for="searchKeyword2">저작권자</label></th>
										<td>
											<input type="text" title="저작권자" class="inputData w70" id="searchKeyword2" name="searchKeyword2" value="${mlStatWorksVO.searchKeyword2 }" style="width:95%;">
										</td>
									</tr>
								</tbody>
							</table>
						<!-- //검색 -->
						<div class="mar_tp30 align_rt">
							<a href="#1" onclick="statBoRegiPop()" onkeypress="statBoRegiPop();" class="pop_apply">저작권자 찾기위한 상당한 노력 신청</a>
							<a href="#1" onclick="javascript:fn_goSetp1('01');" onkeypress="javascript:fn_goSetp1('01');" class="pop_apply">개인 승인 신청</a>
							<a href="#1" onclick="javascript:fn_goSetp1('02');" onkeypress="javascript:fn_goSetp1('02');" class="pop_apply">선택저작물 승인 신청</a>
						</div>
							<iframe id="ifSubStatList" title="법정허락 신청 대상저작물 조회" name="ifSubStatList" 
							width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
							scrolling="no" style="overflow-y:hidden;" ></iframe>						
						<!-- 
						<div class="floatDiv mt0 mb5">
							<p class="fl">
								<a title="선택한 항목을 [법정허락 신청 대상저작물 선택목록]에 추가합니다." onclick="javascript:fn_add();" href="#1">
								<img alt="추가" src="/images/2012/button/add_down.gif">
								</a>
							</p>
						</div>
						 -->
						 <div class="mar_tp30">
						 <a title="선택한 항목을 [법정허락 신청 대상저작물 선택목록]에 추가합니다." onclick="javascript:fn_add();" href="#1">
						 <img src="/images/sub_img/sub_46.gif" alt="추가" /></a></div>
						<p class="mar_tp10" style="border-bottom:1px solid #dddddd;padding-bottom:20px;">* 상단 조회목록에서 선택하고 [추가]를 하면, 하단 선택목록에 담겨져서 한번에 신청이 됩니다.</p>
						<!-- 
						<p class="blue2 p11">&lowast; 상단 조회목록에서 선택하고 [추가]를 하면, 하단
							선택목록에 담겨져서 한번에 신청이 됩니다.</p>
						 -->	
						<p class="HBar mt15 mb5">&nbsp;</p>

						<h2 class="sub_con_h2 mar_tp30">법정허락 승인 신청 대상물</h2>

						<!-- 
						<div class="floatDiv">
							<p class="fl mt5">
								<a href="#1" onclick="javascript:fn_delete();"><img
									src="/images/2012/button/delete.gif" class="mb5" alt="삭제" />
								</a>
							</p>
							<p class="fr">
								<a href="#1"
									onclick="javascript:fn_goSetp1('03');"
									onkeypress="javascript:fn_goSetp1('03');"> <img
									src="/images/2012/button/btn_app13.gif" alt="선택저작물 승인신청 "
									title="선택저작물 승인신청 " /> </a>
							</p>
						</div>
						 -->
						<div class="mar_tp30">
							<div class="float_lf"><a href="#1" onclick="javascript:fn_delete();" class="pop_cancel" style="padding:10px 30px;">삭제</a></div>
							<div class="float_rt"><a href="#1"
									onclick="javascript:fn_goSetp1('03');"
									onkeypress="javascript:fn_goSetp1('03');" class="pop_apply">선택저작물 승인 신청</a></div>
							<p class="clear"></p>
						</div>

						<!-- 그리드스타일 -->
							<table id="tbl_rghtPrps" class="sub_tab3 mar_tp20" cellspacing="0" cellpadding="0" width="100%"  summary="법정허락 승인 신청 정보의 장르, 저작물제호, 저작권자 정보, 구분을 조회한다. ">
								<caption>법정허락 승인 신청</caption>
								<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
									<col width="5%"/>
									<col width="8%"/>
									<col width="*"/>
									<col width="22%"/>
									<col width="23%"/>
								</colgroup>
								<thead>
									<tr>
										<th scope="col"><input type="checkbox" id="sc20"
											class="vmid"
											onclick="javascript:checkBoxToggle('frm','chk',this);"
											style="cursor: pointer;" title="전체선택" />
										</th>
										<th scope="col">장르</th>
										<th scope="col">저작물 제호</th>
										<th scope="col">저작권자 정보</th>
										<th scope="col">구분</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="ce" colspan="5">
										선택된 목록이 없습니다.
										</td>
									</tr>
								</tbody>
							</table>
							<div style="height: 1px;"></div>
						<!-- //그리드스타일 -->
					</div>
					</form>
					<!-- //주요컨텐츠 end -->
				</div>
					<p class="clear"></p>
			</div>
			<!-- //CONTAINER end -->
			<!-- FOOTER str-->
			<jsp:include page="/include/2012/footer.jsp" />
			<!-- FOOTER end -->
	<script type="text/javascript"
		src="/js/2010/calendarcode.js"></script>
	<script type="text/JavaScript">
		Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
	</script>
</body>
</html>
