<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="java.util.*" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.myStat.model.StatApplication" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String sessUserName = user.getUserName();
	
	StatApplication statApplication = (StatApplication) request.getAttribute("statApplication");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%-- <%@include file="/include/sg_include.jsp"%> --%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 이용승인신청 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<link type="text/css" rel="stylesheet" href="/css/new181203.css">

<style type="text/css">
<!--

table tbody tr td.tbBg{
	background-color: #fafafa;	
}

-->
</style>

<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/javascript" src="<c:url value="/js/jquery-1.7.1.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/json2.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.form.2.34.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/quickpager2.jquery.js"/>"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>

<script type="text/javascript"> 
<!--

jQuery(function(){
	jQuery(".pageme").quickPager( {
		pageSize: 5,
		naviSize: 10,
		currentPage: 1,
		holder: ".pager"
	});
	jQuery(window).load(function() {
		//alert("HTML로드 완료");
	});
	
})

function fn_goMyList(){
	var frm = document.srchForm;

	if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.encoding = "application/x-www-form-urlencoded";
		frm.action = "/myStat/statRsltInqrList.do";
		frm.submit();
	}
}


//취소
function fn_goList(){
	var frm = document.srchForm;

	if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.encoding = "application/x-www-form-urlencoded";
		frm.action = "/stat/statSrch.do";
		frm.submit();
	}
}

// 인증서 확인을 위한 주민번호 받기.
function chkUserSsn() {
	alert('1');
	var userId = '<%=sessUserIdnt%>';
    
	var userName = '<%=sessUserName%>';
		
	var nowStatCd = '${statApplication.statCd }';

	if( !(nowStatCd== '3' || nowStatCd== '4') && (frm.ssnNo.value == 'null' || frm.ssnNo.value == '') && (frm.corpNo.value == 'null' || frm.corpNo.value == '') ){
		window.open('/user/user.do?method=goSsnNoConf&userIdnt='+userId+'&userName='+userName+'&sDiv=00','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=375');
		return;
	}else{
		fn_doSave();
	}
}

function setUserSsn(strSsnNo) {
	//alert(strSsnNo);
	document.frm.ssnNo.value = strSsnNo;
	fn_doSave();
}


//임시저장 및 화면이동
function fn_doSave() {
	alert('3');
	var frm = document.frm;
	var nowStatCd = '${statApplication.statCd }';

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	var nowStatCd = '${statApplication.statCd }';
	
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		//if(confirm('작성된 이용승인 신청서를 제출하시겠습니까?')){
			// 공인인증절차 수행을 위한 원본메시지를 생성한다. (원본메시지는 XML 로 생성한다.)
    		// 원본 메시 데이터 구성 : 화면에 표시된 HTML 정보를 추출한다.
    		/* frm.originalMessage.value = document.all.goData.innerHTML;
    		
    		if(nowStatCd == null || nowStatCd == '1') {
    			if(fn_signCert(frm)){// 공인 인증절차를 수행하여 성공한 경우 제출한다.
    				frm.branch.value='1';
	    			frm.stat_cd.value = '2';
	    			frm.target = "_self";
					frm.method = "post";
					frm.action = "/stat/statPrpsInsert.do";
					frm.submit();
    			} else {
        			return false;
       			}
    		}else if(nowStatCd== '3' || nowStatCd== '4'){
    			frm.branch.value='1';
    			frm.stat_cd.value = '5';
    			frm.target = "_self";
				frm.method = "post";
   				frm.action = "/myStat/statPrpsUpdate.do";
				frm.submit();
    		} */
		//}
    		
		if(confirm('작성된 이용승인 신청서를 제출하시겠습니까?')){
			alert('13');
			frm.action = "/stat/statPrpsInsert.do";
			frm.submit();
    		
/*     		// 공인인증절차 수행을 위한 원본메시지를 생성한다. (원본메시지는 XML 로 생성한다.)
    		// 원본 메시 데이터 구성 : 화면에 표시된 HTML 정보를 추출한다.
    		frm.originalMessage.value = document.all.goData.innerHTML;
    		
    		// 공인 인증절차를 수행하여 성공한 경우 제출한다.
    		if(nowStatCd== '3' || nowStatCd== '4' || fn_signCert(frm)){
    			frm.branch.value='1';
    			
    			if(nowStatCd == null || nowStatCd == '1') {
    				frm.stat_cd.value = '2';
    			}else if(nowStatCd== '3' || nowStatCd== '4'){
	    			frm.stat_cd.value = '5';
    			}
    			
				frm.target = "_self";
				frm.method = "post";

				if(nowStatCd== '3' || nowStatCd== '4'){
    				frm.action = "/myStat/statPrpsUpdate.do";
    				frm.submit();
    			} else {
    			
    				frm.action = '/stat/chkSignCert.do';
					var certResult = null;
					var certResult = trim(xmlHttpPost(frm));
					
					
					    				
    				if(trim(certResult).length == 0 ){
    					frm.action = "/stat/statPrpsInsert.do";
    					frm.submit();
    				}else {
    					alert(certResult);
		    			return false;
		   			}
    				
    			}
    			
    		} else {
    			return false;
   			}
		} */
	}
}

function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.fileFrm;

	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;
	
	frm.method = "post";
	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
}

//-->
</script>



<style type="text/css">


p {
	/* background: #e5e5e5; */
	margin-bottom:1px;
	margin-top:0px;
}

ul.paging li {
    padding: 10px;
    background: #ccc;
    font-family: georgia;
    font-size: 24px;
    color: #fff;
    line-height: 1;
    width: 180px;
    margin-bottom: 1px;
}

ul.pageNav li{
    display:block;
    floaT: left;
    padding: 3px;
    font-family: georgia;
}

ul.pageNav li a{
    color: #333;
    text-decoration: none;
}

/* UI Object */ 

.paginate_regular{padding:15px 0;text-align:center;line-height:normal}  

.paginate_regular a,  

.paginate_regular strong{display:inline-block;position:relative;margin:0 -2px;padding:2px 8px;font-weight:bold;font-size:12px;font-family:Tahoma, Sans-serif;color:#333;line-height:normal;text-decoration:none;vertical-align:middle}  

.paginate_regular a:hover,  

.paginate_regular a:active,  

.paginate_regular a:focus{background-color:#f8f8f8}  

.paginate_regular strong{color:#f60}  

.paginate_regular .direction{font-weight:normal;color:#767676;white-space:nowrap}  

.paginate_regular .direction span{font-weight:bold;font-size:14px}  

.paginate_regular .direction:hover,  

.paginate_regular .direction:active,  

.paginate_regular .direction:focus{background-color:#fff;color:#333}  

.paginate_regular a.currentPage{color:#f60} 

.grid tbody td.errBg{
	background-color: #faebe4;
}

.loadingImg {
		width:32px;
		height:32px;
		text-align:right;
		position:absolute;
	}
	
	.loadingImg.move {
		opacity: 0.4;
		position:absolute;
	}
	
	.loadingImg>img {
		width:32px;
		height:32px;
		display: block;
	}

/* //UI Object */ 

</style>

</head>

<body>
	<c:set var="isMyPage" value=""/>
	<c:if test="${isMy == 'Y' }">
		<c:set var="isMyPage" value="Y"/>
	</c:if>
	<!-- 전체를 감싸는 DIVISION -->

		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<c:if test="${isMyPage=='Y'}">
			<script type="text/javascript">initNavigation(0);</script>
		</c:if>
		<c:if test="${isMyPage==''}">
			<script type="text/javascript">initNavigation(3);</script>
		</c:if>
	   <%--  <form name="frm" action="#">
			<input type="hidden" name="isMy" value="${isMy}"/>
			<input type="hidden" name="action_div" value=""/>
			<input type="hidden" name="stat_cd" value=""/>
			<input type="hidden" name="rgstIdnt" value="${userInfo.USER_IDNT }"/>
			<input type="hidden" name="applyWriteYmd" value="${srchParam.applyWriteYmd }">
			<input type="hidden" name="applyWriteSeq" value="${srchParam.applyWriteSeq }">
			<input type="hidden" name="branch" value="1" />
			<input type="submit" style="display:none;">
		</form> --%>
		<div id="contents">
			<div class="con_lf">
				<div class="con_lf_big_title">저작권자 찾기</div>
				<ul class="sub_lf_menu">
							<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
				<!-- <li><a class="on" href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
					<li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
					<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
					<li><a class="on" href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
					<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
					<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
					<li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li>
				</ul>
			</div> <!-- conf_if -->
			
			<div class="con_rt">
				<div class="con_rt_head">
					<img src="https://www.findcopyright.or.kr/images/sub_img/sub_home.png" alt="홈 페이지">
					&gt;
					법정허락 승인 신청
					&gt;
					<span class="bold">법정허락 승인 신청</span>
				</div> <!-- conf_rt_head -->
				<div class="con_rt_hd_title">법정허락 승인 신청</div>
				<div id="sub_contents_con">
					<div class="list_step_apply">
						<ul>
							<li>
								<div class="wrap">
									<p>STEP 01</p>
									<strong>법정허락 승인 신청 서류 작성</strong>
								</div>
							</li>
							<li>
								<div class="wrap">
									<p>STEP 02</p>
									<strong>첨부서류 제출하기</strong>
								</div>
							</li>
							<li class="active">
								<div class="wrap">
									<p>STEP 03</p>
									<strong>신청정보 확인 및 제출하기</strong>
								</div>
							</li>
						</ul>
					</div> <!-- list_step_apply -->
					<!-- 신청정보 영역 -->
					<div class="view_apply">
							<div class="inner">

					<div class="preview">
							<p class="num_top">처리기간 : 40일</p>
							<h1 class="tit">법정허락 승인 신청</h1>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:120px" />
										<col style="width:115px" />
										<col style="width:150px" />
										<col style="width:115px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="2">회원정보</th>
											<th>성명<p>(법인명)</p></th>
											<td>${statApplication.applrName }</td>
											<th>주민등록번호<p>(법인등록번호)</p></th>
											<td>${statApplication.dummyApplrResdCorpNumb }</td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td>${statApplication.applrTelx }</td>
											<th>주소</th>
											<td>${statApplication.applrAddr }</td>
										</tr>
										<tr>
											<th class="bg" rowspan="2">신청인정보</th>
											<th>성명<p>(법인명)</p></th>
											<td>${statApplication.applrName }</td>
											<th>주민등록번호<p>(법인등록번호)</p></th>
											<td>${statApplication.dummyApplrResdCorpNumb }</td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td>${statApplication.applrTelx }</td>
											<th>주소</th>
											<td>${statApplication.dummyApplrResdCorpNumb }</td>
										</tr>
										<tr>
											<th class="bg" rowspan="2">대리인정보</th>
											<th>성명<p>(법인명)</p></th>
											<td>${statApplication.applyProxyName }</td>
											<th>주민등록번호<p>(법인등록번호)</p></th>
											<td>${statApplication.dummyApplyProxyResdCorpNumb }
										</tr>
										<tr>
											<th>전화번호</th>
											<td>${statApplication.applyProxyAddr }</td>
											<th>주소</th>
											<td>${statApplication.applyProxyTelx }</td>
										</tr>
										<tr>
											<th class="bg" rowspan="7">이용 승인신청<br>정보</th>
											<th>구분</th>
										   	<c:forEach items="${statApplyWorksList}" var="applyTypeList">
		                               			<c:if test="${statApplyWorksList.APPLYTYPE01 == '1'}">
		                               				<c:if test="${applyTypeList.code==1}">
		                               					<td colspan="4"> ${applyTypeList.codeName }</td>
		                               				</c:if>
		                               			</c:if>
		                               		<c:if test="${statApplyWorksList.APPLYTYPE02 == '1'}">
		                               			<c:if test="${applyTypeList.code==2}"><td colspan="4"> ${applyTypeList.codeName }</td></c:if>
		                               		</c:if>
		                               		<c:if test="${statApplyWorksList.APPLYTYPE03 == '1'}">
		                               			<c:if test="${applyTypeList.code==3}"><td colspan="4"> ${applyTypeList.codeName }</td></c:if>
		                               		</c:if>
		                               		<c:if test="${statApplyWorksList.APPLYTYPE04 == '1'}">
		                               			<c:if test="${applyTypeList.code==4}"><td colspan="4"> ${applyTypeList.codeName }</td></c:if>
		                               		</c:if>
		                               		<c:if test="${statApplyWorksList.APPLYTYPE05 == '1'}">
		                               			<c:if test="${applyTypeList.code==5}"><td colspan="4"> ${applyTypeList.codeName }</td></c:if>
		                               		</c:if>
											</c:forEach>
									</tr>
										<tr>
											<th>제호<p>(제목)</p></th>
											<td colspan="4">총 ${statApplication.applyWorksCnt }건<br>${statApplication.applyWorksTitl }</td>
										</tr>
										<tr>
											<th>종류</th>
											<td colspan="4">${statApplication.applyWorksKind }</td>
										</tr>
										<tr>
											<th>형태 및 수량</th>
											<td colspan="4">${statApplication.applyWorksForm }</td>
										</tr>
										<tr>
											<th>이용의 내용</th>
											<td colspan="4"><%=CommonUtil.replaceBr(statApplication.getUsexDesc(),true)%></td>
										</tr>
										<tr>
											<th>승인신청 사유</th>
											<td colspan="4"><%=CommonUtil.replaceBr(statApplication.getApplyReas(),true)%></td>
										</tr>
										<tr>
											<th>보상금액</th>
											<td colspan="4">${statApplication.cpstAmnt }원</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
							<p class="txt1">※ <strong>저작권법 [제50조, 제51조, 제52조, 제89조, 제97조]</strong>에 따라 <br>위와 같이 [저작물, 실연 · 음반 · 방송, 데이터베이스] 이용의 승인을 신청합니다.</p>
							<div class="sign">
								  <span class="black">
	                        	  <c:set var="date" value="<%= new Date() %>" />
							  	  <fmt:formatDate value="${date}" type="date" pattern="yyyy년 MM월 dd일" />
                         		 </span>
								<p>신청인   <%=sessUserName%>   (서명 또는 인)</p>
							</div>
							<p class="txt2"><strong>문화체육관광부장관</strong> 귀하</p>
							
						</div>
						</div>
					</div>			
					<h2 class="sub_con_h2 mar_tp30">첨부서류</h2>
					<table class="sub_tab td_padding tbl_apply mar_tp15" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
					<caption>한국저작권위원회</caption>
					<colgroup>
						<col style="width:375px">
						<col style="width:auto">
					</colgroup>
			 		<tbody>
						 	<tr>
							<th>이용승인신청명세서</th>
							<td>
							<c:forEach begin="0" end="0" var="file" items="${fileList2}">
							<a href="" class="link">${file.fileName}<span> (${file.fileSize})</span></a> 
							</c:forEach>
							</td>
						</tr>
					
				 		<tr>
							<th>보상금액 산정내역서</th>
							<td>
							<c:forEach begin="1" end="1" var="file" items="${fileList2}">
							<a href="" class="link">${file.fileName}<span> (${file.fileSize})</span></a> 
							</c:forEach>
							</td>
						</tr>
						<tr>
							<th>‘상당한 노력’ 절차 수행 증빙 자료</th>
							<td>
							<c:forEach begin="2" end="2" var="file" items="${fileList2}">
							<a href="" class="link">${file.fileName}<span> (${file.fileSize})</span></a> 
							</c:forEach>
							</td>
						</tr>
						<tr>
							<th>‘상당한 노력’ 절차 외 권리자를 찾기 위해 노력한 활동 자료</th>
							<td>
							<c:forEach begin="3" end="3" var="file" items="${fileList2}">
							<a href="" class="link">${file.fileName}<span> (${file.fileSize})</span></a> 
							</c:forEach>
							</td>
						</tr>
						<tr>
							<th>해당 저작물이 공표되었음을 밝힐 수 있는 서류</th>
							<td>
					    	<c:forEach begin="4" end="4" var="file" items="${fileList2}">
							<a href="" class="link">${file.fileName}<span> (${file.fileSize})</span></a> 
							</c:forEach>
							</td>
						</tr>
						<tr>
							<th>신청 저작물의 파일 또는 내용물</th>
							<td>
							<c:forEach begin="5" end="5" var="file" items="${fileList2}">
							<a href="" class="link">${file.fileName}<span> (${file.fileSize})</span></a> 
							</c:forEach>
							</td>
						</tr> 
					</tbody>
				</table>
				</div><!-- sub_contents_con -->
				<div class="btn_area mar_tp50">
					<!-- <a href="step_2.html" class="btn_style2">이전단계</a><a href="" class="btn_style2">임시저장</a> -->
				   <a onclick="chkUserSsn();"  class="btn_style3" onkeypress="chkUserSsn();" href="#1">제출하기</a>
				</div>		
			</div><!-- con_rt -->
 	    </div> <!-- contents -->
 	    <jsp:include page="/include/2017/footer.jsp" />
	<!-- //전체를 감싸는 DIVISION -->
<script type="text/JavaScript">
<!--
	window.onload = function(){

	//이용승인신청_신청서 종류 체크박스 셋팅
	var frm = document.frm;
	
	if('${statApplication.applyType01 }' == '1') {
		frm.applyType01.checked = true;
		frm.dummyApplyType01.checked = true;
	}
	if('${statApplication.applyType02 }' == '1') {
		frm.applyType02.checked = true;
		frm.dummyApplyType02.checked = true;
	}
	if('${statApplication.applyType03 }' == '1') {
		frm.applyType03.checked = true;
		frm.dummyApplyType03.checked = true;
	}
	if('${statApplication.applyType04 }' == '1') {
		frm.applyType04.checked = true;
		frm.dummyApplyType04.checked = true;
	}
	if('${statApplication.applyType05 }' == '1') {
		frm.applyType05.checked = true;
		frm.dummyApplyType05.checked = true;
	}
}
//-->
</script>
</body>
</html>

