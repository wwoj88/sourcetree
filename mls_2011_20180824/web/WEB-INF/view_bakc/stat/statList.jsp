<!DOCTYPE html>
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<%
	String menuSeqn = request.getParameter("menuSeqn");
	String srchGubun = request.getParameter("srchGubun") == null ? "" : request.getParameter("srchGubun");
	String gubun = request.getParameter("gubun") == null ? "" : request.getParameter("gubun");
	
	System.out.println("gubun>>>"+gubun);
	
	String srchTitle = request.getParameter("srchTitle") == null ? "" : request.getParameter("srchTitle");
	String srchApprDttm = request.getParameter("srchApprDttm") == null ? "" : request.getParameter("srchApprDttm");
	String srchClmsTite = request.getParameter("srchClmsTite") == null ? "" : request.getParameter("srchClmsTite");
	String srchGenre = request.getParameter("srchGenre") == null ? "" : request.getParameter("srchGenre");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
	String srchFILD1 = request.getParameter("FILD1") == null ? "" : request.getParameter("FILD1");
	String srchFILD2 = request.getParameter("FILD2") == null ? "" : request.getParameter("FILD2");
	String srchFILD3 = request.getParameter("FILD3") == null ? "" : request.getParameter("FILD3");
	String srchFILD4 = request.getParameter("FILD4") == null ? "" : request.getParameter("FILD4");
	String srchFILD5 = request.getParameter("FILD5") == null ? "" : request.getParameter("FILD5");
	String srchFILD6 = request.getParameter("FILD6") == null ? "" : request.getParameter("FILD6");
	String srchFILD7 = request.getParameter("FILD7") == null ? "" : request.getParameter("FILD7");
	String srchFILD8 = request.getParameter("FILD8") == null ? "" : request.getParameter("FILD8");
	String srchFILD9 = request.getParameter("FILD9") == null ? "" : request.getParameter("FILD9");
	String srchFILD10 = request.getParameter("FILD10") == null ? "" : request.getParameter("FILD10");
	String srchFILD11 = request.getParameter("FILD11") == null ? "" : request.getParameter("FILD11");
	String srchFILD99 = request.getParameter("FILD99") == null ? "" : request.getParameter("FILD99");
	String srchFILD = request.getParameter("FILD99") == null ? "" : request.getParameter("FILD");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락저작물 | 저작권정보 조회</title>
<script type="text/JavaScript">
<!--

//신청
function fn_goSetp1(sDiv) {
	var frm = document.frm;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/myStat/myStat.do?method=statPrps";
		frm.action_div.value = sDiv;
		frm.submit();
	}
}

	function goPage(pageNo){
		var frm = document.form1;
		frm.page_no.value = pageNo;
		frm.submit();
	}

	function board_search() {
		var frm = document.form1;

		if(document.getElementById("srchFILD1").checked == true){
			frm.FILD1.value = 1;
		}else{
			frm.FILD1.value = 0;
		}	
		if(document.getElementById("srchFILD2").checked == true){
			frm.FILD2.value = 1;
		}else{
			frm.FILD2.value = 0;
		}	
		if(document.getElementById("srchFILD3").checked == true){
			frm.FILD3.value = 1;
		}else{
			frm.FILD3.value = 0;
		}
		if(document.getElementById("srchFILD4").checked == true){
			frm.FILD4.value = 1;
		}else{
			frm.FILD4.value = 0;
		}	
		if(document.getElementById("srchFILD5").checked == true){
			frm.FILD5.value = 1;
		}else{
			frm.FILD5.value = 0;
		}	
		if(document.getElementById("srchFILD6").checked == true){
			frm.FILD6.value = 1;
		}else{
			frm.FILD6.value = 0;
		}	
		if(document.getElementById("srchFILD7").checked == true){
			frm.FILD7.value = 1;
		}else{
			frm.FILD7.value = 0;
		}	
		if(document.getElementById("srchFILD8").checked == true){
			frm.FILD8.value = 1;
		}else{
			frm.FILD8.value = 0;
		}	
		if(document.getElementById("srchFILD9").checked == true){
			frm.FILD9.value = 1;
		}else{
			frm.FILD9.value = 0;
		}	
		if(document.getElementById("srchFILD10").checked == true){
			frm.FILD10.value = 1;
		}else{
			frm.FILD10.value = 0;
		}	
		if(document.getElementById("srchFILD11").checked == true){
			frm.FILD11.value = 1;
		}else{
			frm.FILD11.value = 0;
		}	
		if(document.getElementById("srchFILD99").checked == true){
			frm.FILD99.value = 1;
		}else{
			frm.FILD99.value = 0;
		}	
		
		if(frm.FILD1.value == 1 || frm.FILD2.value == 1 || frm.FILD3.value == 1 || frm.FILD4.value == 1 || frm.FILD5.value == 1
		   || frm.FILD6.value == 1 || frm.FILD7.value == 1 || frm.FILD8.value == 1 || frm.FILD9.value == 1 || frm.FILD10.value == 1
		   || frm.FILD11.value == 1 || frm.FILD99.value == 1 ){
			frm.FILD.value = 1;
		}else{
			frm.FILD.value = 0;
		}

		var srchApprDttm = frm.srchApprDttm.value; //검색 입력 날자
		var dateStr = new Date().format("YYYY"); // 현제 년도

		if(dateStr < srchApprDttm.substr(0,4)){
			alert(dateStr+"년 이전만 조회 가능 합니다");
			return;
		}	
		
		frm.page_no.value = 1;
		frm.submit();
	}

	function fn_enterCheck(obj){
	  // EnterKey 입력시 공인인증서 로그인 수행
	  if (event.keyCode == 13) {
		  board_search();
	  }
  }
  // 날짜체크 
function checkValDate(){
	var f = document.form1;

	if(f.srchApprDttm.value!=''){
		if(f.srchApprDttm.value.length != 8){
			alert('일자형식은 8자리 입니다.');
			f.srchApprDttm.value='';
			return false;
		}
	}
}

	function boardDetail(bordSeqn,menuSeqn){
		var frm = document.form1;
		frm.bordSeqn.value = bordSeqn;
		frm.menuSeqn.value = '8';
		//frm.menuSeqn.value = menuSeqn;
		frm.page_no.value = '<%=page_no%>';
		frm.method = "post";
		frm.action = "/board/board.do?method=boardView";
		frm.submit();
  }

	/*calendar호출*/
	function fn_cal(frmName, objName){
		showCalendar(frmName, objName);
	}	 

 function initParameter(){


	var mesg = '';
	var gubun = '<%=gubun%>';
	
	// 통합검색에서 넘어온경우 알림메시지 
	if(  gubun == 'totalSearch' ){

			var mesg = '<%=srchTitle%>'+" (으)로 곡명 ";
			mesg += "이 검색됩니다.\n검색항목 입력으로 상세검색이 가능합니다. ";
			
			alert(mesg);
	}
}

//숫자만 입력
function only_arabic(t){
	var key = (window.netscape) ? t.which :  event.keyCode;

	if (key < 45 || key > 57) {
		if(window.netscape){  // 파이어폭스
			t.preventDefault();
		}else{
			event.returnValue = false;
		}
	} else {
		//alert('숫자만 입력 가능합니다.');
		if(window.netscape){   // 파이어폭스
			return true;
		}else{
			event.returnValue = true;
		}
	}	
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">

<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>

<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>

<script type="text/javascript" src="/js/2010/prototype.js"> </script>
</head>
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">

		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2011/header.jsp" /> --%>
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_1.gif" alt="저작권정보조회 및 신청" title="저작권정보조회 및 신청" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>

			<div class="content">
			
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
						<li id="lnb1"><a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2011/content/sub_lnb0101_off.gif" title="음악" alt="음악" /></a></li>
						<li id="lnb2"><a href="/rghtPrps/rghtSrch.do?DIVS=B"><img src="/images/2011/content/sub_lnb0102_off.gif" title="도서" alt="도서" /></a></li>
						<li id="lnb3"><a href="/rghtPrps/rghtSrch.do?DIVS=N"><img src="/images/2011/content/sub_lnb0103_off.gif" title="뉴스" alt="뉴스" /></a></li>
						<li id="lnb4"><a href="/rghtPrps/rghtSrch.do?DIVS=C"><img src="/images/2011/content/sub_lnb0104_off.gif" title="방송대본" alt="방송대본" /></a></li>
						<li id="lnb5"><a href="/rghtPrps/rghtSrch.do?DIVS=I"><img src="/images/2011/content/sub_lnb0105_off.gif" title="이미지" alt="이미지" /></a></li>
						<li id="lnb6"><a href="/rghtPrps/rghtSrch.do?DIVS=V"><img src="/images/2011/content/sub_lnb0106_off.gif" title="영화" alt="영화" /></a></li>
						<li id="lnb7"><a href="/rghtPrps/rghtSrch.do?DIVS=R"><img src="/images/2011/content/sub_lnb0107_off.gif" title="방송" alt="방송" /></a></li>
						<li id="lnb8"><a href="/rghtPrps/rghtSrch.do?DIVS=X"><img src="/images/2011/content/sub_lnb0108_off.gif" title="기타" alt="기타" /></a></li>
						<li id="lnb9"><a href="/noneRght/rghtSrch.do?DIVS=M"><img src="/images/2011/content/sub_lnb0109_off.gif" title="권리자미확인저작물" alt="권리자미확인저작물" /></a>
							<ul>
							<li id="lnb91"><a href="/noneRght/rghtSrch.do?DIVS=M">음악</a></li>
							<li id="lnb92"><a href="/noneRght/rghtSrch.do?DIVS=B">도서</a></li>
							<li id="lnb93"><a href="/noneRght/rghtSrch.do?DIVS=N">뉴스</a></li>
							<li id="lnb94"><a href="/noneRght/rghtSrch.do?DIVS=C">방송대본</a></li>
							<li id="lnb95"><a href="/noneRght/rghtSrch.do?DIVS=I">이미지</a></li>
							<li id="lnb96"><a href="/noneRght/rghtSrch.do?DIVS=V">영화</a></li>
							<li id="lnb97"><a href="/noneRght/rghtSrch.do?DIVS=R">방송</a></li>
							</ul>
						</li>
						<li id="lnb10"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=8&amp;page_no=1"><img src="/images/2011/content/sub_lnb0110_off.gif" title="법정허락 저작물" alt="법정허락 저작물" /></a></li>
					</ul>
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb10");
					</script>
				</div>
				<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>저작권정보조회 및 신청</span><em>법정허락 저작물</em></p>
					<h1 title="저작물에 대한 주민의식이 필요합니다!"><img src="/images/2011/title/content_h1_0110.gif" alt="법정허락 저작물" title="법정허락 저작물" /></h1>
					
					<div class="section">
					<!-- 검색 -->
					<form name="frm" action="#">
							<input type="hidden" name="page_no"/>
							<input type="hidden" name="action_div"/>

							<input type="hidden" name="apply_write_ymd"/>
							<input type="hidden" name="apply_write_seq"/>
							<input type="hidden" name="stat_cd"/>
							<input type="submit" style="display:none;">
					</form>	
					<form name="form1" action="#" class="sch">
					  <input type="hidden" name="page_no" />
					  <input type="hidden" name="menuSeqn" value="<%=menuSeqn%>" />
					  <input type="hidden" name="bordSeqn" value="8"/>
					  <input type="hidden" name="threaded" />
					  <input type="hidden" name="FILD" value="<%=srchFILD%>"/>
					  <input type="hidden" name="FILD1" value="<%=srchFILD1%>"/>
					  <input type="hidden" name="FILD2" value="<%=srchFILD2%>"/>
					  <input type="hidden" name="FILD3" value="<%=srchFILD3%>"/>
					  <input type="hidden" name="FILD4" value="<%=srchFILD4%>"/>
					  <input type="hidden" name="FILD5" value="<%=srchFILD5%>"/>
					  <input type="hidden" name="FILD6" value="<%=srchFILD6%>"/>
					  <input type="hidden" name="FILD7" value="<%=srchFILD7%>"/>
					  <input type="hidden" name="FILD8" value="<%=srchFILD8%>"/>
					  <input type="hidden" name="FILD9" value="<%=srchFILD9%>"/>
					  <input type="hidden" name="FILD10" value="<%=srchFILD10%>"/>
					  <input type="hidden" name="FILD11" value="<%=srchFILD11%>"/>
					  <input type="hidden" name="FILD99" value="<%=srchFILD99%>"/>
							<fieldset class="w100">
							<legend></legend>
								<div class="boxStyle">
									<div class="box1 floatDiv">
										<div class="fl w85">
											<p class="fl mt5 w15"><img src="/images/2011/content/sch_txt.gif" alt="Search" title="Search"></p>
											
											<table class="fl schBoxGrid w85" summary="">
												<caption></caption>
												<colgroup><col width="12%"><col width="38%"><col width="20%"><col width="30%"></colgroup>
												<tbody>
													<tr>
														<th scope="row"><label for="srchGubun">구분</label></th>
														<td>
															<select id="srchGubun" name="srchGubun" class="w70">
																<option value="0">선택</option>
																<option value="1" <%="1".equals(srchGubun)?"selected='selected'":"" %>>승인</option>
																<option value="2" <%="2".equals(srchGubun)?"selected='selected'":"" %>>공고</option>
															</select>
														</td>
														<th scope="row"><label for="sch5">공고/승인일자</label></th>
														<td>
															<input class="inputData" type="text" id="sch5" name="srchApprDttm" size="8" title="공고/승인일" maxlength="8" onkeypress="javascript:if(event.keyCode==13){board_search();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="<%=srchApprDttm%>" /> 
															<img src="/images/2011/common/calendar.gif" onclick="javascript:fn_cal('form1','srchApprDttm');" onkeypress="javascript:fn_cal('frm','srchStartDate');" alt="달력" title="승인일자를 선택하세요." align="middle" style="cursor:pointer;"/> 
														</td>
													</tr>
													<tr>
														<th scope="row"><label for="sch1">제목</label></th>
														<td colspan="3"><input class="inputData w85" id="sch1" size =40 name="srchTitle" value="<%=srchTitle%>" /></td>
													</tr>
													<tr>
														<th scope="row"><label>분야</label></th>
														<td colspan="3">
															<input type="checkbox" name="srchFILD1" id="srchFILD1" class="inputRChk" <%if(srchFILD1.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD1">도서</label>&nbsp;
															<input type="checkbox" name="srchFILD2" id="srchFILD2" class="inputRChk" <%if(srchFILD2.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD2">음악</label>&nbsp;
															<input type="checkbox" name="srchFILD3" id="srchFILD3" class="inputRChk" <%if(srchFILD3.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD3">연극</label>&nbsp;
															<input type="checkbox" name="srchFILD4" id="srchFILD4" class="inputRChk" <%if(srchFILD4.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD4">미술</label>&nbsp;
															<input type="checkbox" name="srchFILD5" id="srchFILD5" class="inputRChk" <%if(srchFILD5.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD5">건축</label>&nbsp;
															<input type="checkbox" name="srchFILD6" id="srchFILD6" class="inputRChk" <%if(srchFILD6.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD6">사진</label>&nbsp;
															<input type="checkbox" name="srchFILD7" id="srchFILD7" class="inputRChk" <%if(srchFILD7.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD7">영상</label>&nbsp;
															<input type="checkbox" name="srchFILD8" id="srchFILD8" class="inputRChk" <%if(srchFILD8.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD8">도형</label>&nbsp;
															<br/>
															<input type="checkbox" name="srchFILD9" id="srchFILD9" class="inputRChk" <%if(srchFILD9.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD9">컴퓨터프로그램</label>&nbsp;
															<input type="checkbox" name="srchFILD10" id="srchFILD10" class="inputRChk" <%if(srchFILD10.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD10">2차저작물</label>&nbsp;
															<input type="checkbox" name="srchFILD11" id="srchFILD11" class="inputRChk" <%if(srchFILD11.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD11">편집</label>&nbsp;
															<input type="checkbox" name="srchFILD99" id="srchFILD99" class="inputRChk" <%if(srchFILD99.equals("1")){ %> checked="checked"<% }%>/><label for="srchFILD99">기타</label>
														</td>
													</tr>
												</tbody>
											</table>
											<div class="layer_relative">
												<p class="gray_box_line">&lowast; 검색은 원하는 항목에 찾고자 하는 검색어를 기입한 후 오른쪽 ‘검색’ 버튼을 클릭하세요.
												<a href="#1" onclick="javascript:toggleLayer('help_pop2');" class="ml10 underline black2">도움말</a>&nbsp;<img src="/images/2011/common/ic_help.gif" class="vmid ml5" alt="" /></p>
												<!-- 도움말 레이어 -->
												<div class="layer_pop w80" id="help_pop2">
													<h1>검색도움말</h1>
														<div class="layer_con">
														<ul class="list1">
														<li class="p11"><strong>구분</strong>: 선택 항목에 해당되는 데이터를 검색 합니다.</li>
														<li class="p11"><strong>공고/승인일자</strong>: 직접 입력하여 검색 할 경우 날짜형식은 ‘20110815’와 같이 년,월,일을</br> 차례로 입력합니다.</li>
														<li class="p11"><strong>제목</strong>: 제목은 대소문자 구분없이 검색 합니다.</li>
														<li class="p11"><strong>분야</strong>: 선택 항목에 해당되는 데이터를 검색 합니다. 중복선택이 가능합니다.</li>
														</ul>
													</div>
													<a href="#1" onclick="javascript:toggleLayer('help_pop2');" class="layer_close"><img src="/images/2011/button/layer_close.gif" alt="" /></a>
												</div>
												<!-- //도움말 레이어 -->
											</div>	
										</div>
										<p class="fl btn_area pt75">
											<input type="image" src="/images/2011/button/sch.gif" 
													onclick="javascript:board_search();" 
													onkeypress="javascript:board_search();" 
													alt="검색" title="검색">
										</p>
									</div>
									<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
								</div>
							</fieldset>
						</form>
						<!-- //검색 -->
							
						<p style="margin-top:10px;width:730px;line-height:100%;">
							<!-- * 저작(권)자 정보가 없는경우. 또는 정보는 있지만 해당항목이 비신탁인 경우 [<span class="necessary"> </span>]으로 표시됩니다. -->
							<label class="blue2">*</label> <label class="fontSmall gray underline blue2"><a href="#1" onclick="javascript:toggleLayer('help_pop1');"><u><b>법정허락 저작물이란</b><img src="/images/2011/common/ic_help.gif" class="vmid ml5" alt="" /></u></a></label>
						</p>
							<!-- 도움말 레이어 -->
							<div class="floatDiv layer_pop w80" id="help_pop1">
								<h1>법정허락 저작물이란?</h1>
								
								<div class="layer_con">
									<ul class="list1">
									<li class="p11">
									법정허락 저작물이란 저작물의 이용자가 상당한 노력을 기울였어도 공표된 저작물의 저작재산권자를 알지 못하거나 
									저작재산권자를 알더라도 그의 거소를 알 수 없어 저작물의 이용을 허락받을 수 없는 경우, 
									이를 문화체육관광부 장관에게 저작물의 이용승인을 얻은 후 문화체육관광부장관이 정하는 기준에 의한 보상금을 공탁하고
									 이를 이용하도록 허락하는 제도를 말합니다. </br>
									※ 저작재산권자 : 저작재산권이란 저작자가 저작물을 스스로 이용하거나 다른 사람이 이용할 수 있도록 허락함으로서
									경제적 이익을 올릴 수 있는 재산권이며, 이러한 재산권을 행사하는 사람을 저작재산권자라고 말합니다.</li>
									</ul>
								</div>
								
								<a href="#1" onclick="javascript:toggleLayer('help_pop1');" class="layer_close"><img src="/images/2011/button/layer_close.gif" alt="" /></a>
							</div>
							
							
							<div class="floatDiv mb5">
								<p class="fr rgt">
									<a href="#1" onclick="javascript:fn_goSetp1('new');" onclick="fn_goSetp1('new');" onkeypress="fn_goSetp1('new');" title="법정허락 신청화면으로 이동합니다." ><img src="/images/2011/button/btn_app4_05.gif" alt="" /></a>
								</p>
							</div>
							
							<!-- //도움말 레이어 -->
						<!-- 테이블 리스트 Set -->
						<div class="section ">
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="7%">
								<col width="8%">
								<col width="30%">
								<col width="10%">
								<col width="15%">
								<col width="15%">
								<col width="15%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">순번</th>
										<th scope="col">구분</th>
										<th scope="col">제목</th>
										<th scope="col">분야</th>
										<th scope="col">저작(인접)권자</th>
										<th scope="col">공고/승인일자</th>
										<th scope="col">ICN</th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${boardList.totalRow == 0}">
										<tr>
											<td class="ce" colspan="7">검색된  게시물정보가  없습니다.</td>
										</tr>
								</c:if>
								 <c:if test="${boardList.totalRow > 0}">
								    <c:forEach items="${boardList.resultList}" var="board">
								        <c:set var="NO" value="${board.TOTAL_CNT}"/>
							        	<c:set var="i" value="${i+1}"/>
									<tr>
										<td class="ce"><c:out value="${NO}"/></td>
										<td class="ce">${board.GUBUN}</td>
										<c:if test="${board.GUBUN_VAL == 1}">
											<td>${board.TITE}</td>
										</c:if>
										<c:if test="${board.GUBUN_VAL == 2}">
											<td><a href="#1" onclick="javascript:boardDetail('${board.BORD_SEQN}','${board.MENU_SEQN}')" class="underline black2" >${board.TITE}</a></td>
										</c:if>
										<td class="ce">${fn:replace(board.FILD_NAME, ",", ", ")}</td>
										<td class="ce">${board.LICENSOR_NAME}</td>
										<td class="ce">${board.APPR_DTTM}</td>
										<td class="ce">${board.ICN}</td>
									</tr>
									</c:forEach>
		    					</c:if>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<!-- 페이징 -->
							<div class="pagination">
							<ul>
								<li>
						 		<%-- 페이징 리스트 --%>
								  <jsp:include page="../common/PageList_2011.jsp" flush="true">
									  <jsp:param name="totalItemCount" value="${boardList.totalRow}" />
										<jsp:param name="nowPage"        value="${param.page_no}" />
										<jsp:param name="functionName"   value="goPage" />
										<jsp:param name="listScale"      value="" />
										<jsp:param name="pageScale"      value="" />
										<jsp:param name="flag"           value="M01_FRONT" />
										<jsp:param name="extend"         value="no" />
									</jsp:include>
								</li>	
							</ul>
						</div>
						<!-- //페이징 -->
						
						</div>
						<!-- //테이블 리스트 Set -->
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
	</body>
</html>
