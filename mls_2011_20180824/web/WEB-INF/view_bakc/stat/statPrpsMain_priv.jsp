<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%-- <%@ page import="com.oreill.servlet.*" %> --%>
<%
  User user = SessionUtil.getSession(request);
  String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 이용승인신청 | 저작권자찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js" type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>

<script type="text/javascript"> 

//임시저장 및 화면이동
function fn_doSave(sDiv) {
var frm = document.frm;
frm.action_div.value = sDiv;

//로그인 체크
var userId = '<%=sessUserIdnt%>';
if(userId == 'null' || userId == ''){
  alert('로그인이 필요한 화면입니다.');
  location.href = "/user/user.do?method=goSgInstall";
  return;
  
}else{
  if(fn_chkValue()) {
    

  
    frm.target = "_self";
    frm.method = "post";
    frm.action = "/stat/statPrpsList_priv.do";
    frm.submit();
  }
}
}
function fn_goList(){
  var frm = document.frm;

  if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
    frm.target = "_self";
    frm.method = "post";
    frm.encoding = "application/x-www-form-urlencoded";
    frm.action = "/stat/statSrch.do";
    frm.submit();
  }
}

function fn_goMyList(){
  var frm = document.frm;

  if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
    frm.target = "_self";
    frm.method = "post";
    frm.encoding = "application/x-www-form-urlencoded";
    frm.action = "/myStat/statRsltInqrList.do";
    frm.submit();
  }
}

//신청자, 대리인 정보 맵핑
function fn_fillDate(oObj, sDiv) {
  var frm = document.frm;

  var dummyResdCorpNumb = '';
  var resdCorpNumb = '';
  <c:choose>
    <c:when test="${userInfo.USER_DIVS == '01' }">
      <c:if test="${userInfo.RESD_CORP_NUMB != null || userInfo.RESD_CORP_NUMB != ''}">
          dummyResdCorpNumb = '${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 0, 6) }-${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 6, 13) }';
          resdCorpNumb = '${userInfo.RESD_CORP_NUMB1 }-${userInfo.RESD_CORP_NUMB2 }';
      </c:if>
    </c:when>
    <c:otherwise>
      dummyResdCorpNumb = '${fn:substring(userInfo.CORP_NUMB, 0, 3)}-${fn:substring(userInfo.CORP_NUMB, 3, 5)}-${fn:substring(userInfo.CORP_NUMB, 5, 10)}';
      resdCorpNumb = '${fn:substring(userInfo.CORP_NUMB, 0, 3)}-${fn:substring(userInfo.CORP_NUMB, 3, 5)}-${fn:substring(userInfo.CORP_NUMB, 5, 10)}';
    </c:otherwise>
  </c:choose>
  
  if(oObj.checked) {
    if(sDiv == 'APPLR') {
      //신청자
      frm.applrName.value = '${userInfo.USER_NAME }';
      frm.dummyApplrResdCorpNumb.value = dummyResdCorpNumb;
      frm.applrResdCorpNumb.value = resdCorpNumb;
      frm.applrAddr.value = '${userInfo.HOME_ADDR }';
      frm.applrTelx.value = '${userInfo.TELX_NUMB }';
    
    } else {
      //대리인
      frm.applyProxyName.value = '${userInfo.USER_NAME }';
      frm.dummyApplyProxyResdCorpNumb.value = dummyResdCorpNumb;
      frm.applyProxyResdCorpNumb.value = resdCorpNumb;
      frm.applyProxyAddr.value = '${userInfo.HOME_ADDR }';
      frm.applyProxyTelx.value = '${userInfo.TELX_NUMB }';
    
    }
  } else {
    if(sDiv == 'APPLR') {
      //신청자
      frm.applrName.value = '';
      frm.dummyApplrResdCorpNumb.value = '';
      frm.applrResdCorpNumb.value = '';
      frm.applrAddr.value = '';
      frm.applrTelx.value = '';
    
    } else {
      //대리인
      frm.applyProxyName.value = '';
      frm.dummyApplyProxyResdCorpNumb.value = '';
      frm.applyProxyResdCorpNumb.value = '';
      frm.applyProxyAddr.value = '';
      frm.applyProxyTelx.value = '';
    
    }
  }
}

function openSmplDetail(div) {

  var param = '';
  
  param = 'DVI='+div;
  
  var url = '/common/rghtPrps_smpl.jsp?'+param
  var name = '';
  var openInfo = 'target=rghtPrps_mvie, width=705, height=570, scrollbars=yes';
  
  window.open(url, name, openInfo);
}

//신청서 구분(신청서명) 동기화
function fn_chkSycn(oObj, sNo) {
  var oChkApplyType = document.getElementsByTagName("input");
  for(i = 0; i < oChkApplyType.length; i++) {
    if(oChkApplyType[i].type == "checkbox") {
      
      if( ((oChkApplyType[i].id).toUpperCase()).indexOf("APPLYTYPE0") > -1 && oChkApplyType[i].id != oObj.id) {
        oChkApplyType[i].checked = false;
      }
    }
  }
  
  document.getElementById("dummyApplyType0"+sNo).checked = oObj.checked;

}

//저작권법 항목 
function fn_chkRawCd(sNo, sChk) {
  var oObjs = document.getElementsByName("dummyApplyRawCd");
  
  for(var i = 0; i < oObjs.length; i++) {
    if(oObjs[i].value == sNo &&sChk == true) {
      oObjs[i].checked = true;
    } else {
      oObjs[i].checked = false;
    }
  }
  /*
  document.getElementById("applyRawCd").value = sNo;
  */
  document.getElementById("applyRawCd").value = "";
}

/* 첨부화일 관련 시작 */
//첨부화일 테이블 idx
var iRowIdx = 0;

//순번 재지정1
function fn_resetSeq1(){
    var oSeq = document.getElementsByName("fileNameCd");
    for(i=0; i<oSeq.length; i++){
        oSeq[i].value = i+1;
    }
}
<c:if test="${isModi == '01'}">
  //첨부화일 추가
  function fn_addRow1(){
      var oTbl = document.getElementById("tblAttachFile");
      var oTbody = oTbl.getElementsByTagName("TBODY")[0];
      
      var oTr = document.createElement("TR");
      
    //순번(선택)
    var oTd = document.createElement("TD");
    var sTag = '<input name=\"chkDel1\" id=\"chkDel1_'+iRowIdx+'\" type=\"checkbox\" />';
    sTag += '<input name=\"fileNameCd\" id=\"fileNameCd_'+iRowIdx+'\" type=\"hidden\" value=\"99\" style=\"border:0px;\" readonly=\"readonly\"  />';
    oTd.innerHTML = sTag;
    oTd.style.width = '10%';
    oTd.className = 'ce';
    oTr.appendChild(oTd);
    
    //서류구분
    oTd = document.createElement("TD");
    oTd.innerHTML = '기타';
    oTd.style.width = '35%';
    oTr.appendChild(oTd);
      
    //첨부화일명
    oTd = document.createElement("TD");
    sTag = '<input type=\"hidden\" name=\"fileDelYn\" />';
    sTag += '<span id=\"spfile'+iRowIdx+'\"><input type=\"file\" name=\"file'+iRowIdx+'\" id=\"file_'+iRowIdx+'\" class=\"inputData L w100\" onkeydown=\"return false;\" onchange=\"fn_chkFileType(this,'+iRowIdx+');\"/></span>';
    sTag += '<input name=\"hddnFile\" id=\"hddnFile_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"fileName\" id=\"fileName_'+iRowIdx+'\" type=\"text\" onkeyup=\"onkeylengthMax(this, 255, this.name);\" class=\"inputDate w95\" style=\"display:none;\" />';
    
    sTag += '<input name=\"realFileName\" id=\"realFileName_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"fileSize\" id=\"fileSize_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"filePath\" id=\"filePath_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    oTd.innerHTML = sTag;
    oTr.appendChild(oTd);
    
    oTbody.appendChild(oTr);
    iRowIdx++;
           
  }
  
  //첨부화일 삭제
  function fn_delRow1(){
      var oTbl = document.getElementById("tblAttachFile");
      var oChkDel = document.getElementsByName("chkDel1");
      var iChkCnt = oChkDel.length;
      var iDelCnt = 0;
      
      if(iChkCnt == 1 && oChkDel[0].checked == true){
          oTbl.deleteRow(6);
          iDelCnt++;
      }else if(iChkCnt > 1){
          for(i = iChkCnt-1; i >= 0; i--){
              if(oChkDel[i].checked == true){
                  oTbl.deleteRow(i+6);
                  iDelCnt++;
              }
          }
      }
      
      if(iDelCnt < 1){
          alert('삭제할 첨부화일을 선택하여 주십시요');
      }
  }
</c:if>

<c:if test="${isModi == '02'}" >
  /*수정기능 추가str*/
  //첨부화일 추가
  function fn_addRow1(){
    var oTbl = document.getElementById("tblAttachFile");
    var oTbody = oTbl.getElementsByTagName("TBODY")[0];
    
    var oTr = document.createElement("TR");
    
    //순번(선택)
    var oTd = document.createElement("TD");
    var sTag = '<input name=\"chkDel1\" id=\"chkDel1_'+iRowIdx+'\" type=\"checkbox\" />';
    sTag += '<input name=\"fileNameCd\" id=\"fileNameCd_'+iRowIdx+'\" type=\"hidden\" value=\"99\" style=\"border:0px;\" readonly=\"readonly\"  />';
    oTd.innerHTML = sTag;
    oTd.style.width = '7%';
    oTd.className = 'ce';
    oTr.appendChild(oTd);
    
    //서류구분
    oTd = document.createElement("TD");
    oTd.innerHTML = '기타';
    oTd.style.width = '25%';
    oTr.appendChild(oTd);
    
    //첨부화일명
    oTd = document.createElement("TD");
    oTd.colSpan = 2;
    sTag = '<input type=\"hidden\" name=\"fileDelYn\" id=\"fileDelYn_'+iRowIdx+'\" />';
    sTag += '<span id=\"spfile'+iRowIdx+'\"><input type=\"file\" name=\"file'+iRowIdx+'\" id=\"file_'+iRowIdx+'\" class=\"inputData L w100\" onkeydown=\"return false;\" onchange=\"fn_chkFileType(this,'+iRowIdx+');\"/></span>';
    sTag += '<input name=\"hddnFile\" id=\"hddnFile_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"fileName\" id=\"fileName_'+iRowIdx+'\" type=\"text\" onkeyup=\"onkeylengthMax(this, 255, this.name);\" class=\"inputDate w95\" style=\"display:none;\" />';
    
    sTag += '<input name=\"realFileName\" id=\"realFileName_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"fileSize\" id=\"fileSize_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    sTag += '<input name=\"filePath\" id=\"filePath_'+iRowIdx+'\" type=\"hidden\" style=\"display:none;\" />';
    oTd.innerHTML = sTag;
    oTr.appendChild(oTd);
    
    oTbody.appendChild(oTr);
    iRowIdx++;
           
  }
  
  //첨부화일 삭제
  function fn_delRow1(){
    var oTbl = document.getElementById("tblAttachFile");
    var oChkDel = document.getElementsByName("chkDel1");
    var iChkCnt = oChkDel.length;
    var iDelCnt = 0;
    
    /* if(iChkCnt == 1 && oChkDel[0].checked == true){
        //수정시 기타 관련 파일의 tr에 ID가 존재함
        //해당 체크 박스의 idx와 동일한 tr ID가 존재하면 해당 파일은 기존에 입력 되었던 파일
      var oTr = document.getElementById("trAttcFile_D0");
        if(oTr == null) { //해당 ID를 가진 tr 이 없으면 현재 화면에서 새로 추가한 기타 파일
            oTbl.deleteRow(2);
            
        }else{  //해당 ID를 가진 tr이 있으면 기존에 입력 되었던 파일
          oTr.style.display = "none";   //화면상에서 보이지만 않게 함
          document.getElementById("fileDelYn_D0").value = "Y";  //파일 처리 구분을 삭제로 셋팅
          oChkDel[0].checked = false;   //체크박스 체크 해제
        }
        iDelCnt++;
    }else if(iChkCnt > 1){
        for(i = iChkCnt-1; i >= 0; i--){
            if(oChkDel[i].checked == true){
              //수정시 기타 관련 파일의 tr에 ID가 존재함
              //해당 체크 박스의 idx와 동일한 tr ID가 존재하면 해당 파일은 기존에 입력 되었던 파일
              var oTr = document.getElementById("trAttcFile_D"+(oChkDel[i].id).substr((oChkDel[i].id).indexOf('_')+1, (oChkDel[i].id).length));
              if(oTr == null) { //해당 ID를 가진 tr 이 없으면 현재 화면에서 새로 추가한 기타 파일
                    oTbl.deleteRow(i+2);
                    
                }else{  //해당 ID를 가진 tr이 있으면 기존에 입력 되었던 파일
                oTr.style.display = "none";   //화면상에서 보이지만 않게 함
                document.getElementById("fileDelYn_D"+(oChkDel[i].id).substr((oChkDel[i].id).indexOf('_')+1, (oChkDel[i].id).length)).value = "Y";  //파일 처리 구분을 삭제로 셋팅
                oChkDel[i].checked = false;   //체크박스 체크 해제
              }
                iDelCnt++;
            }
        }
    } */
    if(iChkCnt > 0){
        for(i = iChkCnt-1; i >= 0; i--){
            if(oChkDel[i].checked == true){
              //수정시 기타 관련 파일의 tr에 ID가 존재함
              //해당 체크 박스의 idx와 동일한 tr ID가 존재하면 해당 파일은 기존에 입력 되었던 파일
              var oTr = document.getElementById("trAttcFile_D"+(oChkDel[i].id).substr((oChkDel[i].id).indexOf('_')+1, (oChkDel[i].id).length));
              if(oTr == null) { //해당 ID를 가진 tr 이 없으면 현재 화면에서 새로 추가한 기타 파일
                    oTbl.deleteRow(i+6);
                    
                }else{  //해당 ID를 가진 tr이 있으면 기존에 입력 되었던 파일
                oTr.style.display = "none";   //화면상에서 보이지만 않게 함
                document.getElementById("fileDelYn_D"+(oChkDel[i].id).substr((oChkDel[i].id).indexOf('_')+1, (oChkDel[i].id).length)).value = "Y";  //파일 처리 구분을 삭제로 셋팅
                oChkDel[i].checked = false;   //체크박스 체크 해제
              }
              iDelCnt++;
            }
      }
    }
    
    if(iDelCnt < 1){
        alert('삭제할 첨부화일을 선택하여 주십시요');
    }
  }
  /*수정기능 추가end*/
</c:if>


//첨부화일 확장자 제한
function fn_chkFileType(obj,iRowIdx){
  
  if(iRowIdx == 'D1'){
    jQuery("input[name='fileD1']").each(function(index){
        jQuery(this).css("color","#868b8f");
      })  
  }
  var frm = document.frm;
  var refuseFile = ["HTM","HTML","PHP","PHP3","ASP","JSP","CGI","INI","PL","TIF","PPT","DOCX","DOC","GUL"];
  var str = obj.value;
  var nPos = 0;
  var sepCnt = 0;
  var isAlert = false;
  var msg = "";
  
  //확장자 구하기
  if( str != null ){
    var splitLength = (str.split(".")).length;    
    str = str.split(".")[splitLength-1];
  }

  //확장자 구하기
  /*
  while(nPos >= 0){
    nPos = str.indexOf(".");
    if(nPos> 0){
      str = str.substring(nPos+1,str.length);
    }
    sepCnt++;
  }
  */
   
  for(i=0; i<refuseFile.length; i++){
    if(msg.length > 0){
      msg += ", ";
    }
    msg += refuseFile[i];
    
    if(str.toUpperCase() == refuseFile[i]){
      isAlert = true;
    }
  }
  
  if(sepCnt > 2 || isAlert == true){
    alert(msg + "확장자를 가진 화일이나 \n\r이중확장자(\"---.---.---\") 화일은 올리실 수 없습니다. ");
    
    var spObj = document.getElementById("spfile"+iRowIdx);
    //alert('<span id=\"spfile'+iRowIdx+'\"><input type=\"file\" name=\"file\" id=\"file_'+iRowIdx+'\" class=\"inputData L w100\" onkeydown=\"return false;\" onchange=\"fn_chkFileType(this,'+iRowIdx+');\"/></span>');
      spObj.outerHTML = '<span id=\"spfile'+iRowIdx+'\"><input type=\"file\" name=\"file\" id=\"file_'+iRowIdx+'\" class=\"inputData L w100\" onkeydown=\"return false;\" onchange=\"fn_chkFileType(this,\''+iRowIdx+'\');\"/></span>';
      
      return false;
  }
  
  if(iRowIdx == 'D1') {
    frm.existYn.value = 'Y';
  }
}
/* 첨부화일 관련 끝*/

//table row 별 필수체크
function checkTr(oTr) {
  
  
  var result = ""; 
  
  if(oTr.id == "applrInfo1"){
    //alert(oTr.id);
    var oInput = oTr.getElementsByTagName("input");
    
    for(z = 0; z<oInput.length; z++){
      if(checkField2(oInput[z]) == false){
        result = oInput[z].id;
        break;
      }else{
        
      }   
    }
  }
  
  if(oTr.id == "applrInfo2"){
    var oTextArea = oTr.getElementsByTagName("textarea");
    var oInput = oTr.getElementsByTagName("input");
    for(z = 0; z<oTextArea.length; z++){
      if(checkField2(oTextArea[z]) == false){
        result = oTextArea[z].id;
        break;
      }else{
        for(z = 0; z<oInput.length; z++){
          if(checkField2(oInput[z]) == false){
            result = oInput[z].id;
            break;
          }   
        }
      }   
    }
  }
  
  return result;
}

function checkForm3(forms) {
  var result = ""; 

  if(typeof(forms) != 'undefined') {
    for(y = 0; y<forms.length; y++) {
      if(checkField2(forms[y]) == false)  {
        result = forms[y].id;
        break;
      }
    }
  }
  else
    result = "";
    
  return result;
}

//필수체크
function fn_chkValue() {
  var frm = document.frm;
    var chk1=document.getElementById("ApplyCheck").checked;
    if(!chk1){
      alert("개인정보 수집에 동의해주세요.");
      return false;
    }
  //신청인정보 체크
  var oTr = document.getElementById("applrInfo1");
  tgtId = checkTr(oTr);
  
  if(tgtId != "") {
    document.getElementById(tgtId).focus();
    return false;
  }
  
    //신청인 주민/사업자번호 체크
  var sApplrResdCorpNumb = document.getElementById("applrResdCorpNumb").value;
  if(sApplrResdCorpNumb !=""){
    while(sApplrResdCorpNumb.indexOf('-') > -1){
      sApplrResdCorpNumb = sApplrResdCorpNumb.replace('-', '');
    }
    if(sApplrResdCorpNumb.length == 10){
      /*if(!check_busino(sApplrResdCorpNumb)){
        document.getElementById("dummyApplrResdCorpNumb").focus();
          alert("유효하지 않은 사업자등록번호입니다.");
          return;
      }*/
      if(!check_busino(sApplrResdCorpNumb)){
        document.getElementById("dummyApplrResdCorpNumb").focus();
          alert("유효하지 않은 법인등록번호입니다.");
          return;
      }
    } else if(sApplrResdCorpNumb.length == 13){
      var sStr1 = sApplrResdCorpNumb.substr(0,6);
      var sStr2 = sApplrResdCorpNumb.substr(6,7);
      if(ssnCheck3(sStr1, sStr2)){
        document.getElementById("dummyApplrResdCorpNumb").focus();
        return;
      }
    } else {
      document.getElementById("dummyApplrResdCorpNumb").focus();
      alert("유효하지 않은 주민등록번호입니다.");
      return;
    }
  }
  
  var oTr = document.getElementById("applrInfo2");
  tgtId = checkTr(oTr);
  
  if(tgtId != "") {
    document.getElementById(tgtId).focus();
    return false;
  }
  
  

  
  //대리인 주민/사업자번호 체크
  var sApplyProxyResdCorpNumb = document.getElementById("applyProxyResdCorpNumb").value;
  if(sApplyProxyResdCorpNumb !=""){
    while(sApplyProxyResdCorpNumb.indexOf('-') > -1){
      sApplyProxyResdCorpNumb = sApplyProxyResdCorpNumb.replace('-', '');
    }
    if(sApplyProxyResdCorpNumb.length == 10){
      /*if(!check_busino(sApplyProxyResdCorpNumb)){
        document.getElementById("dummyApplyProxyResdCorpNumb").focus();
          alert("유효하지 않은 사업자등록번호입니다.");
          return;
      }*/
      if(!check_busino(sApplyProxyResdCorpNumb)){
        document.getElementById("dummyApplyProxyResdCorpNumb").focus();
          alert("유효하지 않은 법인등록번호입니다.");
          return;
      }
    } else if(sApplyProxyResdCorpNumb.length == 13){
      var sStr1 = sApplyProxyResdCorpNumb.substr(0,6);
      var sStr2 = sApplyProxyResdCorpNumb.substr(6,7);
      if(ssnCheck3(sStr1, sStr2)){
        document.getElementById("dummyApplyProxyResdCorpNumb").focus();
        return;
      }
    } else {
      document.getElementById("dummyApplyProxyResdCorpNumb").focus();
      alert("유효하지 않은 주민등록번호입니다.");
      return;
    }
  }
  
  var chkCnt = 0;
  var focusTgt = '';
  
  //신청서 구분 체크여부 확인
  <c:set var="dySeq" value="${0}"/>
  <c:forEach items="${applyTypeList}" var="applyTypeList">
    <c:set var="dySeq" value="${dySeq + 1}"/>
    if('${dySeq}' == 1) {
      focusTgt = 'applyType0${dySeq }';
    }
    if(document.getElementById("applyType0${dySeq }").checked) {
      chkCnt++;
    }
  </c:forEach>
  if(chkCnt == 0) {
    alert("신청서 구분을(를) 선택하세요.");
    document.getElementById(focusTgt).focus();
    return false;
  }
  
  //폼체크
  var tgtId = checkForm3(frm);
      
  if(tgtId != "") {
  
    document.getElementById(tgtId).focus();
    return false;
  }
  
  //저작권법 값 셋팅
  var oObjs = document.getElementsByName("dummyApplyRawCd");
  chkCnt = 0;
  for(var i = 0; i < oObjs.length; i++) {
    if(oObjs[i].checked == true) {
      document.getElementById("applyRawCd").value = oObjs[i].value;
      chkCnt++;
    }
  }
  if(chkCnt == 0) {
    alert("저작권법을(를) 선택하세요.");
    document.getElementById(oObjs[0].id).focus();
    return false;
  }
  //첨부화일
  var oInput = document.getElementsByTagName("input");
  var arrVal = new Array();
  var arrCnt = 0;
  for(i=0; i<oInput.length; i++){
    var tmpId = oInput[i].id;
    if(tmpId.substring(0,5) == "file_"){
      var tmpVal = oInput[i].value;
      var sep = 0;
      for(j=0; j<tmpVal.length; j++){
            if(tmpVal.charCodeAt(j) == 92){ // 아스키 92 = '\'
                sep = j;
            }
        }
      arrVal[arrCnt] = tmpVal.substring(sep,tmpVal.length);
      arrVal[arrCnt] = arrVal[arrCnt].replace("\\","");
      
      arrCnt++;
    }
  }
  arrVal.length = arrCnt;
  
  var oHddnFile = document.getElementsByName("hddnFile");
  for(i=0; i<oHddnFile.length; i++){
    /*
    if(arrVal[i].length < 1){
      alert('파일명을 입력하세요');
      return false;
    }
    */
    
    if(i==0){
      if(arrVal[i] != ''){
        //alert(arrVal[i]);
        
        var str = arrVal[i];
        
        //확장자 구하기
        if( str != null ){
          var splitLength = (str.split(".")).length;    
          str = str.split(".")[splitLength-1];
        }
        
        if(str != 'xlsx' && str != 'xls' && str != 'pdf' && str != 'jpg' && str != 'hwp'  ){
          /* alert("엑셀파일 형식이 아닌 파일은 올리실 수 없습니다."); */
          alert("파일 확장자의 경우 xlsx,xls,pdf,jpg,hwp 파일만 사용하실수있습니다");
              jQuery("input[name='fileD1']").each(function(index){
                jQuery(this).css("color","#c0c1c2");
              })
          return false;
        }else{
          jQuery("#fileDelYn_D1").val('Y');
        }
      }
    }
    
    <c:if test="${isModi == '02'}">
      if(i==0){
        var isDel = jQuery("#fileDelYnChkD1").attr("checked");
        if(isDel == 'checked'){
          jQuery("#fileDelYn_D1").val('Y');
        }
        if(arrVal[i] != ''){
          
          var str = arrVal[i];
          
          //확장자 구하기
          if( str != null ){
            var splitLength = (str.split(".")).length;    
            str = str.split(".")[splitLength-1];
          }
          
          if(str != 'xlsx' && str != 'xls' && str != 'pdf' && str != 'jpg' && str != 'hwp'  ){
            alert("파일 확장자의 경우 xlsx,xls,pdf,jpg,hwp 파일만 사용하실수있습니다");
                //document.frm.fileD1.select();
                jQuery("input[name='fileD1']").each(function(index){
                  alert(index);
                })
            return false;
          }else{
            jQuery("#fileDelYn_D1").val('Y');
          }
        }
      }else if(i==1){
        var isDel = jQuery("#fileDelYnChkD2").attr("checked");
        if(isDel == 'checked'){
          jQuery("#fileDelYn_D2").val('Y');
        }
        if(arrVal[i] != ''){
          //alert(arrVal[i]);
          jQuery("#fileDelYn_D2").val('Y');
        }
      }else if(i==2){
        var isDel = jQuery("#fileDelYnChkD3").attr("checked");
        if(isDel == 'checked'){
          jQuery("#fileDelYn_D3").val('Y');
        }
        if(arrVal[i] != ''){
          //alert(arrVal[i]);
          jQuery("#fileDelYn_D3").val('Y');
        }
      }else if(i==3){
        var isDel = jQuery("#fileDelYnChkD4").attr("checked");
        if(isDel == 'checked'){
          jQuery("#fileDelYn_D4").val('Y');
        }
        if(arrVal[i] != ''){
          //alert(arrVal[i]);
          jQuery("#fileDelYn_D4").val('Y');
        }
      }else if(i==4){
        var isDel = jQuery("#fileDelYnChkD5").attr("checked");
        if(isDel == 'checked'){
          jQuery("#fileDelYn_D5").val('Y');
        }
        if(arrVal[i] != ''){
          //alert(arrVal[i]);
          jQuery("#fileDelYn_D5").val('Y');
        }
      }
        
    </c:if>
    oHddnFile[i].value = arrVal[i];
  }
  
  if(jQuery("#isFileChk").attr("checked")){
    var existYn = frm.existYn.value;
    if(oHddnFile[0].value == '' && existYn != 'Y'){
      alert("이용 승인신청 명세서 첨부파일을 등록하세요.")
      return false;
    }
  }
  
  return true;
}


function fn_matchResdCorpNumb(oObj) {
  var frm = document.frm;

  if(oObj.id == 'dummyApplrResdCorpNumb') {
    frm.applrResdCorpNumb.value = oObj.value;
  }
  if(oObj.id == 'dummyApplyProxyResdCorpNumb') {
    frm.applyProxyResdCorpNumb.value = oObj.value;
  }
}

function fn_goSample(){
  window.open("/images/2011/smpl/stat_step1.jpg", "", "width=770,height=850,scrollbars=yes");
}

function fn_fileDownLoad(filePath, fileName, realFileName) {
  var frm = document.form1;
  frm.filePath.value     = filePath;
  frm.fileName.value     = fileName;
  frm.realFileName.value = realFileName;

  frm.action = "/board/board.do?method=fileDownLoad2";
  frm.submit();
} 

function fn_changeChk(){
  var num = jQuery("#reqCnt").val();
  if(num > 1){
    jQuery("#isCntChk").attr("checked",true);
  }else{
    jQuery("#isCntChk").attr("checked",false);
  }
  
  if(num > 5){
    jQuery("#isFileChk").attr({
      "checked":true,
      "disabled":"disabled"
    });
  }else{
    jQuery("#isFileChk").attr({
      "checked":false,
      "disabled":false
    });
    var fileName = jQuery("input[name='fileDelYnChk']").eq(0).parent().children().eq(0).css({"text-decoration":"line-through"});
    jQuery("input[name='fileDelYnChk']").eq(0).attr("checked",true);
    frm.existYn.value = '';
    
  }
  
}


jQuery(function(){
  
  var statCd = '${statApplication.statCd}';
  
  //alert("상태코드는:"+statCd);
  
  jQuery("#reqCnt").live("keyup",function(e){
    fn_changeChk(jQuery(this));
  })
  
  var isDisable = frm.existYn.value;
  
  if(isDisable == 'Y'){
    jQuery("#isFileChk").attr("disabled",true);
  }
  
  jQuery("input[name='fileDelYnChk']").each(function(index){
    if(index==0){
      jQuery(this).bind('click',function(index){
        var num = jQuery("#reqCnt").val();
        if(num > 5){
          /* var fileName = jQuery(this).parent().children().eq(0).css({"text-decoration":"line-through"}); */
          jQuery(this).attr("checked",false);
          alert("신청 건수가 6건 이상이면 \'이용승인신청 명세서\'를 \n첨부하셔야 합니다..")
        }else{
          jQuery("#isFileChk").attr("disabled",false);
          var fileName = jQuery(this).parent().children().eq(0).css({"text-decoration":"line-through"});
          frm.existYn.value = '';
        }
      })  
    }else{
      jQuery(this).bind('click',function(index){
          var fileName = jQuery(this).parent().children().eq(0).css({"text-decoration":"line-through"});
      })  
    }
  })
})

jQuery(window).load(function(){
  var isExist = document.frm.existYn.value;
  if(isExist == 'Y'){
    jQuery("#isFileChk").attr("checked","checked");
  }
})
 
//-->
</script>
<script>
/*   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-1', 'auto');
  ga('send', 'pageview'); */
</script>


<style type="text/css">
<!--

.grid tbody tr td.tbBorder{
  
  border-right-color: #ff0000;
  //text-decoration:line-through;
}

-->
</style>
</head>

<body>
  <c:set var="isMyPage" value=""/>
  <c:if test="${isMy == 'Y' }">
    <c:set var="isMyPage" value="Y"/>
  </c:if>
    <!-- HEADER str-->
    <c:if test="${isMyPage=='Y'}">
      <!-- 2017변경 -->
      <jsp:include page="/include/2017/header.jsp" />
      <%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
      <script type="text/javascript">initNavigation(0);</script>
    </c:if>
    <c:if test="${isMyPage==''}">
      <!-- 2017변경 -->
      <jsp:include page="/include/2017/header.jsp" />
      <%-- <jsp:include page="/include/2012/subHeader3.jsp" /> --%>
      <script type="text/javascript">initNavigation(3);</script>
    </c:if>
    <!-- //HEADER end -->
    <!-- CONTAINER str-->
    <form name="form1" method="post" action="#">
      <input type="hidden" name="filePath"> <input type="hidden"
        name="fileName"> <input type="hidden" name="realFileName">
      <input type="hidden" name="action_div"> <input type="submit"
        style="display: none;">
    </form>
    <form name="srchForm" action="#">
      <c:if test="${isMyPage == 'Y'}">
        <input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
        <input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
        <input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
        <input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
        <input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
        <input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
      </c:if>
      <c:if test="${isMyPage == ''}">
        <input type="hidden" name="pageNum"   value="${srchVO.pageNum}" />
        <input type="hidden" name="searchCondition" value="${srchVO.searchCondition}" />
        <input type="hidden" name="searchKeyword1"  value="${srchVO.searchKeyword1}" />
      </c:if>
      <input type="submit" style="display:none;">
    </form>
    <form id="popupForm" name="popupForm" method="post">
      <!-- 레스포트 정보 -->
      <input type="hidden" name="searchKeyword1" id="searchKeyword1" >
    </form> 
    <div id="contents">
        
        <!-- 래프 -->
        <c:if test="${isMyPage=='Y'}">
          <%-- <jsp:include page="/include/2012/myPageLeft.jsp" />
          <script type="text/javascript">
            subSlideMenu("sub_lnb","lnb1","lnb14");
          </script> --%>
          <div class="con_lf">
          <div class="con_lf_big_title">법정허락<br> 승인 신청</div>
          <ul class="sub_lf_menu">
<!--            <li><a href="/mlsInfo/liceSrchInfo01.jsp">저작권자 검색 및 상당한 노력 신청</a>
              <ul class="sub_lf_menu2 disnone">
                <li><a href="/mlsInfo/liceSrchInfo01.jsp">소개 및 이용방법</a></li>
                <li><a href="/srchList.do">서비스 이용</a></li>
                <li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
              </ul>
            </li> -->
            <li><a href="/mlsInfo/statInfo01.jsp" class="on">법정허락 승인 신청</a>
              <ul class="sub_lf_menu2">
                <li><a href="/mlsInfo/statInfo01.jsp">소개 및 이용방법</a></li>
                <li><a href="/stat/statSrch.do" class="on">서비스 이용</a></li>
                <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>
              </ul>
            </li>
          </ul>
        </div>
        </c:if>
        <c:if test="${isMyPage==''}">
          <div class="con_lf">
          <div class="con_lf_big_title">법정허락<br> 승인 신청</div>
          <ul class="sub_lf_menu">
<!--            <li><a href="/mlsInfo/liceSrchInfo01.jsp">저작권자 검색 및 상당한 노력 신청</a>
              <ul class="sub_lf_menu2 disnone">
                <li><a href="/mlsInfo/liceSrchInfo01.jsp">소개 및 이용방법</a></li>
                <li><a href="/srchList.do">서비스 이용</a></li>
                <li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
              </ul>
            </li> -->
            <li><a href="/mlsInfo/statInfo01.jsp" class="on">법정허락 승인 신청</a>
              <ul class="sub_lf_menu2">
                <li><a href="/mlsInfo/statInfo01.jsp">소개 및 이용방법</a></li>
                <li><a href="/stat/statSrch.do" class="on">서비스 이용</a></li>
                <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>
              </ul>
            </li>
          </ul>
        </div>
        </c:if>
        <!-- //래프 -->
    <!--    <iframe id="goLogin" title="법정허락 공인인증서 설치 아이프레임"
          src="/include/sg_install.html" frameborder="0" width="242"
          height="139" scrolling="no" marginwidth="0" marginheight="0"
          style="display: none;"></iframe> -->
        

        <div id="ajaxBox"
          style="position: absolute; z-index: 1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left: -500px; width: 306px; height: 38px; padding: 102px 0 0 0;">
          <p style="height: 38px; text-align: center; margin: 0;">
            <img src="/images/2012/common/loading.gif" alt=""
              style="margin-top: -4px; margin-bottom: 3px;" /><br /> <span
              id="ajaxBoxMent"
              style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만
              기다려주세요..</span>
          </p>
        </div>

        <!-- 주요컨텐츠 str -->
        <div class="con_rt" id="contentBody">
          <c:if test="${isMyPage=='Y'}">
            <!-- <p class="path">
              <span>Home</span><span>마이페이지</span><span>신청현황</span><em>법정허락 이용승인신청</em>
            </p> -->
          </c:if>
          <c:if test="${isMyPage==''}">
            <div class="con_rt_head">
            <img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
            
            &gt;
            법정허락 승인 신청
            &gt;
            <span class="bold">서비스 이용</span>
            </div>
            <div class="con_rt_hd_title">법정허락 승인 신청</div>
          </c:if>
          <!-- section -->
          <div id="sub_contents_con">
            <!-- memo 삽입 -->
            <%-- <jsp:include page="/common/memo/2011/memo_01.jsp">
              <jsp:param name="DIVS" value="MS" />
            </jsp:include>   --%>
            <div class="sub01_con_bg4_tp mar_tp30"></div>
                  <div class="sub01_con_bg4">
                    <div class="font15"><span class="color_2c65aa">법정허락 신청에 관한 문의는 신청분류에 해당하는 단체로
                        연락바랍니다.</span></div>
                    <h3 class="mar_tp10">
                      <span class="w20">- 한국저작권위원회</span> 
                      <em class="w20 mar_tp10" style="margin-left: 30px;"><img src="/images/2012/common/ic_nm.gif" alt="" />심의조사팀</em> <em class="w25"><img src="/images/2012/common/ic_tel.gif" alt="" />&nbsp;02-2660-0104</em>
                  </h3>
                  </div>
                  
            <!-- // memo 삽입 -->
            <!-- process -->
            <div class="usr_process mt20">
              <div class="process_box">
                <ul class="floatDiv">
                  <li class="fl ml0 on"><img alt="1단계 약관동의"
                    src="/images/2012/content/process21_on.gif">
                  </li>
                  <li class="fl"><img alt="2단계 회원정보입력"
                    src="/images/2012/content/process22_off.gif">
                  </li>
                  <li class="fl bgNone pr0"><img alt="3단계 가입완료"
                    src="/images/2012/content/process23_off.gif">
                  </li>
                </ul>
              </div>
            </div>
            <!-- // process -->
            <form name="frm" action="#" enctype="multipart/form-data">
              <c:if test="${isMyPage == 'Y'}">
                <input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
                <input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
                <input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
                <input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
                <input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
                <input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
              </c:if>
              <c:if test="${isMyPage == ''}">
                <input type="hidden" name="pageNo" value="${srchVO.pageNum}"/>
                <input type="hidden" name="searchCondition" value="${srchVO.searchCondition}"/>
                <input type="hidden" name="searchKeyword1" value="${srchVO.searchKeyword1}"/>
              </c:if>
              <input type="hidden" name="isMy" value="${isMy}"/>
              
              <input type="hidden" name="applyDivsCd" value="1"/>
              <input type="hidden" name="action_div" value=""/>
              <input type="hidden" name="prevCnt" value="${statApplication.applyWorksCnt}" />
              
              <c:if test="${isModi == '02' }">
                <!-- 수정기능 추가str -->
                <input type="hidden" name="statCd" value="${statApplication.statCd}"/>
                <input type="hidden" name="applyWriteYmd" value="${statApplication.applyWriteYmd }">
                <input type="hidden" name="applyWriteSeq" value="${statApplication.applyWriteSeq }">
                <!--  -->
              </c:if>
              <input type="submit" style="display:none;">
            <div class="article">
            <div class="floatDiv">
                <h2 class="sub_con_h2 mar_tp30">회원정보<span class="thin p11 ml10"> </span></h2>
              </div>
              
              
              <span class="topLine"></span>
              
              <!-- 그리드스타일 -->
              <table cellspacing="0" cellpadding="0" border="1" class="grid"
                summary="">
                <colgroup>
                  <col width="20%">
                  <col width="30%">
                  <col width="20%">
                  <col width="*">
                </colgroup>
                <tbody>
                  <tr>
                    <th scope="row">성명(법인명)</th>
                    <td>${userInfo.USER_NAME }</td>
                    <th scope="row">주민등록번호(법인등록번호)</th>
                    <td>
                      <c:choose>
                        <c:when test="${userInfo.USER_DIVS == '01' }">
                          <!-- 
                          <c:if test="${userInfo.RESD_CORP_NUMB != null || userInfo.RESD_CORP_NUMB != ''}">
                              ${userInfo.RESD_CORP_NUMB1 }-${userInfo.RESD_CORP_NUMB2 }
                          </c:if>
                          -->
                          <c:if test="${userInfo.RESD_CORP_NUMB != null || userInfo.RESD_CORP_NUMB != ''}">
                            ${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 0, 6) }-${fn:substring(userInfo.RESD_CORP_NUMB_VIEW2, 6, 13) }
                          </c:if>
                        </c:when>
                        <c:otherwise>
                          ${fn:substring(userInfo.CORP_NUMB, 0, 3)}-${fn:substring(userInfo.CORP_NUMB, 3, 5)}-${fn:substring(userInfo.CORP_NUMB, 5, 10)}
                        </c:otherwise>
                      </c:choose>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">주소</th>
                    <td>${userInfo.HOME_ADDR }</td>
                    <th scope="row">전화번호</th>
                    <td>
                      ${userInfo.TELX_NUMB }
                      <input type="hidden" name="rgstIdnt" value="${userInfo.USER_IDNT }"/>
                    </td>
                  </tr>
                </tbody>
              </table>
              <!-- //그리드스타일 -->
            </div>
            <div class="article">
              <!-- 신청인 정보 str -->
              <h2 class="sub_con_h2 mar_tp30">신청인 정보<span class="thin p11 ml10"> (<input type="checkbox" id="chkApplr" onclick="fn_fillDate(this, 'APPLR');" onkeypress="fn_fillDate(this, 'APPLR');"/><label for="chkApplr">회원정보와 동일</label>)</span></h2>
              <span class="topLine"></span>
              <table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
                <colgroup>
                <col width="20%">
                <col width="30%">
                <col width="27%">
                <col width="*">
                </colgroup>
                <tbody>
                  <tr id="applrInfo1">
                    <th scope="row"><label for="applrName" class="necessary">성명(법인명)</label></th>
                    <td><input type="text" id="applrName" name="applrName" rangeSize="0~200" title="성명(법인명)" value="${statApplication.applrName }" nullCheck/></td>
                    <th scope="row"><label for="dummyApplrResdCorpNumb" class="necessary">주민등록번호(법인등록번호)</label></th>
                    <td>
                      <input type="text" id="dummyApplrResdCorpNumb" name="dummyApplrResdCorpNumb"  
                        rangeSize="0~20" title="주민등록번호(법인등록번호)" nullCheck value="${statApplication.dummyApplrResdCorpNumb }"
                        onchange="fn_matchResdCorpNumb(this)"/>
                      <input type="hidden" id="applrResdCorpNumb" name="applrResdCorpNumb" value="${statApplication.applrResdCorpNumb }">
                    </td>
                  </tr>
                  <tr id="applrInfo2">
                    <th scope="row"><label for="applrAddr" class="necessary">주소</label></th>
                    <td><textarea cols="10" class="inputData w90" style="height: 30px !important" rows="2" id="applrAddr" name="applrAddr" title="주소" rangeSize="0~200" nullCheck>${statApplication.applrAddr }</textarea></td>
                    <!--<td><input type="text" id="applrAddr" name="applrAddr" class="w98" rangeSize="0~200" title="주소" nullCheck/></td>-->
                    <th scope="row"><label for="applrTelx" class="necessary">전화번호</label></th>
                    <td><input type="text" id="applrTelx" name="applrTelx" value="${statApplication.applrTelx }" rangeSize="0~20" title="전화번호" nullCheck/></td>
                  </tr>
                </tbody>
              </table>
              <!-- 신청인 정보 end -->
            </div>
            <div class="article">
              <h2 class="sub_con_h2 mar_tp30">대리인 정보<span class="thin p11 ml10"> (<input type="checkbox" id="chkApplyProxy" onclick="fn_fillDate(this, 'APPLYPROXY');" onkeypress="fn_fillDate(this, 'APPLYPROXY');" /><label for="chkApplyProxy">회원정보와 동일</label>)</span></h2>
              <span class="topLine"></span>
              <table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
                <colgroup>
                <col width="20%">
                <col width="30%">
                <col width="27%">
                <col width="*">
                </colgroup>
                <tbody>
                  <tr>
                    <th scope="row"><label for="applyProxyName">성명(법인명)</label></th>
                    <td><input type="text" id="applyProxyName" name="applyProxyName"  value="${statApplication.applyProxyName }" rangeSize="0~200"/></td>
                    <th scope="row"><label for="dummyApplyProxyResdCorpNumb">주민등록번호(법인등록번호)</label></th>
                    <td>
                      <input type="text" id="dummyApplyProxyResdCorpNumb" value="${statApplication.dummyApplyProxyResdCorpNumb }" name="dummyApplyProxyResdCorpNumb"  
                        rangeSize="0~20"
                        onchange="fn_matchResdCorpNumb(this)"/>
                      <input type="hidden" id="applyProxyResdCorpNumb" name="applyProxyResdCorpNumb" value="${statApplication.applyProxyResdCorpNumb }">
                    </td>
                  </tr>
                  <tr>
                    <th scope="row"><label for="applyProxyAddr">주소</label></th>
                    <td><textarea cols="10" class="inputData w90" style="height: 30px !important" rows="2" id="applyProxyAddr" name="applyProxyAddr" title="주소" rangeSize="0~200">${statApplication.applyProxyAddr }</textarea></td>
                    <!--<td><input type="text" id="applyProxyAddr" name="applyProxyAddr"  class="w98" rangeSize="0~200"/></td>-->
                    <th scope="row"><label for="applyProxyTelx">전화번호</label></th>
                    <td><input type="text" id="applyProxyTelx" name="applyProxyTelx" value="${statApplication.applyProxyTelx }" rangeSize="0~20"/></td>
                  </tr>
                </tbody>
              </table>
            </div>
          
            <div class="article">
              <h2 class="sub_con_h2 mar_tp30">개인정보 동의</h2>
              <span class="topLine"></span>
              <table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
                <tbody>
                  <tr id ="applycheck">
                    <th scope="row"><label for="applyProxyName">
                    <br>
                    1. 개인 정보 수집의 목적 <br><br>
                       - 저작권 공고에 따른 업무 처리를 위해 아래와 같이 개인 정보를 수집하고 있습니다.  <br><br>
                    
                    2. 개인 정보 내용     <br><br>
                      - 지적 재산권자 정보 : 성명, 주소, 연락처<br> 
                      - 공고자의 정보: 성명, 주소, 연락처  <br><br>
                    
                    3. 개인정보의 보유 및 이용기간 <br><br>
                      - 이용자의 개인정보는 원칙적으로 개인정보의 수집 및 이용목적이 달성되면 지체 없이 파기합니다.<br> 
                      - 따라서 최종 로그인 후 2년이 경과하였거나 정보주체의 회원 탈퇴 신청 시 회원의 개인정보를 지체 없이 파기합니다. <br><br>
            
                    4. 동의 거부 권리 사실 및 불이익 내용 <br><br>
                      -   이용자는 동의를 거부할 권리가 있습니다.<br> 
                      -  동의를 거부할 경우에는 서비스 이용에 제한됨을 알려드립니다. <br><br>
                      </label></th>
                  </tr>
                </tbody>
              </table>
              <h2 style="text-align: center;">위 개인정보 수집,이용에 동의합니다.(필수)<input class="ApplyCheck" type="checkbox" title="동의" id="ApplyCheck"></h2>
            </div>
            <h2 class="mt20">이용 승인신청 진행상태내역</h2>
            <table cellspacing="0" cellpadding="0" border="1" summary=""
              class="grid">
              <!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
              <colgroup>
                <col width="20%">
                <col width="25%">
                <col width="*">
              </colgroup>
              <thead>
                <tr>
                  <th scope="col">일자</th>
                  <th scope="col">상태</th>
                  <th scope="col">상태처리 비고</th>
                </tr>
              </thead>
              <tbody>
                <c:if test="${not empty sHisList}">
                  <c:forEach items="${sHisList}" var="sHisList">
                    <tr>
                      <td class="ce">${sHisList.CHNG_DTTM}</td>
                      <td class="ce">${sHisList.STAT_CD_NM}</td>
                      <td class="ce">${sHisList.STAT_MEMO}</td>
                    </tr>
                  </c:forEach>
                </c:if>
              </tbody>
            </table>
            <!-- 그리드스타일 -->
            <div class="article">
              <div class="floatDiv">
                <h2 class="sub_con_h2 mar_tp30">이용 승인신청 정보</h2>
                <p class="fr"><span class="button small icon"><a href="#1" onclick="openSmplDetail('MS01');" onkeypress="openSmplDetail('MS01')">예시화면 보기</a><span class="help"></span></span></p>
              </div>
              <span class="topLine"></span>
              <!-- 그리드스타일 -->
               <table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
                                        <colgroup>
                                        <col width="20%">
                                         <col width="25%">
                                          <col width="25%">
                                          <col width="*">
                                        </colgroup>
                                        
                                        <tbody>
                                            <tr>
                                                <th scope="row" colspan="4" class="p14 black ce">이용 승인신청서</th>
                                              
                                            </tr>
                                             <tr>
                                              <td colspan="3">
                                                <c:set var="dySeq" value="0"/>
                          <c:forEach items="${applyTypeList}" var="applyTypeList">
                            <c:set var="dySeq" value="${dySeq + 1}"/>
                            <input type="checkbox" class="inputRChk" title="선택" id="applyType0${dySeq }" name="applyType0${dySeq }" value="1" title="${applyTypeList.codeName }"
                              onclick="fn_chkSycn(this, '${dySeq }');" onkeypress="fn_chkSycn(this, '${dySeq }');">
                            <label for="applyType0${dySeq }" class="p12">${applyTypeList.codeName }</label>&nbsp;
                          </c:forEach>
                                              </td>
                                              <td><strong>처리기간</strong> : 40일</td>
                                            </tr>
                                           
                                             <tr>
                                              <th scope="row"><label for="mainTitle" class="necessary">제호(제목)</label></th>
                                                <td colspan="3">
                                                  <input class="inputRChk" type="checkbox" title="저작물" id="isCntChk" <c:if test="${statApplication.applyWorksCnt > 0}">checked="checked"</c:if> />
                                                  <label for="isCntChk">여러건 신청 : 총</label>
                                                  <input type="text" class="inputData w10" id="reqCnt" name="applyWorksCnt" size="5" value="${statApplication.applyWorksCnt }" character="EK"> 건
                                                  &nbsp;&nbsp;&nbsp;&nbsp;<input class="inputRChk" type="checkbox" title="명세서등록여부" id="isFileChk" />
                                                  <label for="isCntChk">'이용 승인신청 명세서'를 첨부파일로 등록합니다.</label>
                                                   <p class="mt3">
                                                  <input type="text" class="inputData w80" id="mainTitle" value="${statApplication.applyWorksTitl }" name="applyWorksTitl" title="제목(제호)" nullCheck ><p>
                                                </td>
                                            </tr>
                                            <tr>
                                              <th scope="row"><label for="applyWorksKind" class="necessary">종류</label></th>
                                                <td><input type="text" class="inputData w80" id="applyWorksKind" name="applyWorksKind" value="${statApplication.applyWorksKind }" title="종류" rangeSize="0~100" nullCheck /> </td>
                                                <th scope="row"><label for="tx2" class="necessary">형태 및 수량</label></th>
                                                <td><input type="text" class="inputData w80" id="applyWorksForm" name="applyWorksForm" value="${statApplication.applyWorksForm }" title="형태 및 수량" rangeSize="0~100" nullCheck /></td>
                                            </tr>
                                            <tr>
                                              <th scope="row"><label for="usexDesc" class="necessary">이용의 내용</label></th>
                                                <td colspan="3"><span class="blue2 p11">&lowast; 이용하고자 하는 목적, 방법 등을 상세히 기재</span>
                                                <textarea class="w99" id="usexDesc" name="usexDesc" title="이용의 내용" rangeSize="0~4000" nullCheck  cols="10" rows="5" style="height: 70px !important">${statApplication.usexDesc }</textarea></td>
                                            </tr>
                                            <tr>
                                              <th scope="row"><label for="applyReas" class="necessary">승인신청사유</label></th>
                                                <td colspan="3"><span class="blue2 p11">&lowast; 권리자 거소 확인 불가 등의 해당 사유를 기재</span>
                                                <textarea style="height: 70px !important" id="applyReas" name="applyReas" title="승인신청사유" rangeSize="0~4000" nullCheck class="w99" title="신청내용" rows="5" cols="10">${statApplication.applyReas }</textarea></td>
                                            </tr>
                                            <tr>
                                              <th scope="row"><label for="cpstAmnt" class="necessary">보상금액</label></th>
                                                <td colspan="3"><input type="text" class="inputData" id="cpstAmnt" value="${statApplication.cpstAmnt }" name="cpstAmnt" title="보상금액" nullCheck rangeSize="0~15"></td>
                                            </tr>
                                            <tr>
                                              <td colspan="4">
                                                  <a href="#1" class="ml20 mr10" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form' />','저작권법(법정허락).pdf','저작권법(법정허락).pdf')">   
                                                     <img src="/images/file/pdf.gif" class="mr5" style="margin-bottom:3px;" /><strong class="inBlock vtop line22" style="text-decoration:underline;">저작권법</strong> 
                                                    </a>
                                                    <span class="inBlock w10 vtop line22">
                                                      <label class="block"><input type="checkbox" title="제 50조" name="dummyApplyRawCd" id="dummyApplyRawCd50" value="50" <c:if test="${statApplication.applyRawCd == '50' }"> checked="checked" </c:if> onclick="fn_chkRawCd(this.value, this.checked);" onkeypress="fn_chkRawCd(this.value, this.checked);"> 제 50조</label>
                                                      <label class="block"><input type="checkbox" title="제 51조" name="dummyApplyRawCd" id="dummyApplyRawCd51" value="51" <c:if test="${statApplication.applyRawCd == '51' }"> checked="checked" </c:if> onclick="fn_chkRawCd(this.value, this.checked);" onkeypress="fn_chkRawCd(this.value, this.checked);"> 제 51조</label>
                                                      <label class="block"><input type="checkbox" title="제 52조" name="dummyApplyRawCd" id="dummyApplyRawCd52" value="52" <c:if test="${statApplication.applyRawCd == '52' }"> checked="checked" </c:if> onclick="fn_chkRawCd(this.value, this.checked);" onkeypress="fn_chkRawCd(this.value, this.checked);"> 제 52조</label>
                                                      <label class="block"><input type="checkbox" title="제 89조" name="dummyApplyRawCd" id="dummyApplyRawCd89" value="89" <c:if test="${statApplication.applyRawCd == '89' }"> checked="checked" </c:if> onclick="fn_chkRawCd(this.value, this.checked);" onkeypress="fn_chkRawCd(this.value, this.checked);"> 제 89조</label>
                                                      <label class="block"><input type="checkbox" title="제 97조" name="dummyApplyRawCd" id="dummyApplyRawCd97" value="97" <c:if test="${statApplication.applyRawCd == '97' }"> checked="checked" </c:if> onclick="fn_chkRawCd(this.value, this.checked);" onkeypress="fn_chkRawCd(this.value, this.checked);"> 제 97조</label>
                                                      <input type="hidden" name="applyRawCd" id="applyRawCd" value="" title="저작권법"/>
                                                    </span> 
                                                    <strong class="inBlock vtop line22">에 따라 위와같이</strong>
                                                    <span class="inBlock w15 vtop line22">
                                                      <c:set var="dySeq" value="0"/>
                            <c:forEach items="${applyTypeList}" var="applyTypeList">
                              <c:set var="dySeq" value="${dySeq + 1}"/>
                              <label class="block">
                              <input type="checkbox" id="dummyApplyType0${dySeq }" disabled="disabled" title="선택">
                                ${applyTypeList.codeName }
                              </label>
                            </c:forEach>
                                                    </span>
                                                    <strong class="inBlock vtop line22">이용의 승인을 신청합니다.</strong>
                                                </td>
                                            </tr>
                                            <tr>
                    <th scope="row"><label for="">첨부서류</label><!--<a href="#1" class="ml10 underline black2" onclick="javascript:toggleLayer('help_pop1');"><img src ="/images/2011/common/ic_find_id.gif" alt="" class="vtop"></a> --></th>
                    <td colspan="3">
                      <!-- 첨부화일 시작 -->
                                  <div class="mReset">
                        <div class="">
                          <div class="fr mb5">
                            <p>
                              <span title="추가" class="button small"><button id="btnAdd" onkeypress="" onclick="javascript:fn_addRow1();" type="button">File 추가</button></span>
                              <span title="삭제" class="button small"><button id="btnDel" onkeypress="" onclick="javascript:fn_delRow1();" type="button">File 삭제</button></span>
                            </p>
                          </div>
                        </div>
                        <c:if test="${isModi == '01'}">
                          <div class="mb15">
                                            <table id="tblAttachFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부화일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
                              <colgroup>
                                  <col width="10%">
                                  <col width="35%">
                                  <col width="*%">
                              </colgroup>
                              <thead>
                                  <tr>
                                      <th scope="row" class="ce">순번</th>
                                      <th scope="row">서류구분</th>
                                      <th scope="row">첨부파일</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                    <td class="ce">
                                      1<input name="fileNameCd" id="fileNameCd_D1" value="1" type="hidden" style="border:0px;" readonly="readonly"  />
                                    </td>
                                    <td>이용승인신청명세서</td>
                                    <td>
                                      <input type="hidden" name="existYn" />
                                      <input type="hidden" name="fileDelYn" />
                                    <span id="spfileD1">
                                        <input type="file" name="fileD1" id="file_D1" title="첨부파일"         class="inputData L w100"    onkeydown="return false;" onchange="fn_chkFileType(this,'D1');"/>
                                    </span>
                                    <input name="hddnFile" id="hddnFile_D1" type="hidden" />
                                    <input name="fileName" id="fileName_D1" type="hidden" title="파일명"   onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" />
                                    <input name="realFileName" id="realFileName_D1" type="hidden" />
                                    <input name="fileSize" id="fileSize_D1" type="hidden" />
                                    <input name="filePath" id="filePath_D1" type="hidden" />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="ce">
                                      2<input name="fileNameCd" id="fileNameCd_D2" value="2" type="hidden" style="border:0px;" readonly="readonly"  />
                                    </td>
                                    <td>보상금액산정내역서</td>
                                    <td>
                                      <input type="hidden" name="existYn" />
                                      <input type="hidden" name="fileDelYn" />
                                    <span id="spfileD1">
                                        <input type="file" name="fileD2" id="file_D2" title="첨부파일" 
                                            class="inputData L w100"
                                            onkeydown="return false;" onchange="fn_chkFileType(this,'D1');"/>
                                    </span>
                                    <input name="hddnFile" id="hddnFile_D2" type="hidden" />
                                    <input name="fileName" id="fileName_D2" type="hidden" title="파일명" 
                                        onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" />
                                    <input name="realFileName" id="realFileName_D2" type="hidden" />
                                    <input name="fileSize" id="fileSize_D2" type="hidden" />
                                    <input name="filePath" id="filePath_D2" type="hidden" />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="ce">
                                      3<input name="fileNameCd" id="fileNameCd_D3" value="3" type="hidden" style="border:0px;" readonly="readonly"  />
                                    </td>
                                    <td>저작물 공표여부 확인서류</td>
                                    <td>
                                      <input type="hidden" name="fileDelYn" />
                                    <span id="spfileD3">
                                        <input type="file" name="fileD3" id="file_D3" title="첨부파일" 
                                            class="inputData L w100"
                                            onkeydown="return false;" onchange="fn_chkFileType(this,'D3');"/>
                                    </span>
                                    <input name="hddnFile" id="hddnFile_D3" type="hidden" />
                                    <input name="fileName" id="fileName_D3" type="hidden" title="파일명" 
                                        onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" />
                                    <input name="realFileName" id="realFileName_D3" type="hidden" />
                                    <input name="fileSize" id="fileSize_D3" type="hidden" />
                                    <input name="filePath" id="filePath_D3" type="hidden" />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="ce">
                                      4<input name="fileNameCd" id="fileNameCd_D4" value="4" type="hidden" style="border:0px;" readonly="readonly"  />
                                    </td>
                                    <td>거소 미확인여부 확인서류</td>
                                    <td>
                                      <input type="hidden" name="fileDelYn" />
                                    <span id="spfileD4">
                                        <input type="file" name="fileD4" id="file_D4" title="첨부파일"       class="inputData L w100"    onkeydown="return false;" onchange="fn_chkFileType(this,'D4');"/>
                                    </span>
                                    <input name="hddnFile" id="hddnFile_D4" type="hidden" />
                                    <input name="fileName" id="fileName_D4" type="hidden" title="파일명" 
                                        onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" />
                                    <input name="realFileName" id="realFileName_D4" type="hidden" />
                                    <input name="fileSize" id="fileSize_D4" type="hidden" />
                                    <input name="filePath" id="filePath_D4" type="hidden" />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="ce">
                                      5<input name="fileNameCd" id="fileNameCd_D5" value="5" type="hidden" style="border:0px;" readonly="readonly"  />
                                    </td>
                                    <td>협의경과서류</td>
                                    <td>
                                      <input type="hidden" name="fileDelYn" />
                                    <span id="spfileD5">
                                        <input type="file" name="fileD5" id="file_D5" title="첨부파일"   class="inputData L w100"   onkeydown="return false;" onchange="fn_chkFileType(this,'D5');"/>
                                    </span>
                                    <input name="hddnFile" id="hddnFile_D5" type="hidden" />
                                    <input name="fileName" id="fileName_D5" type="hidden" title="파일명" 
                                        onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" />
                                    <input name="realFileName" id="realFileName_D5" type="hidden" />
                                    <input name="fileSize" id="fileSize_D5" type="hidden" />
                                    <input name="filePath" id="filePath_D5" type="hidden" />
                                    </td>
                                  </tr>
                                                    </tbody>
                            </table>
                          </div>
                        </c:if>
                        <c:if test="${isModi == '02'}">
                          <div class="mb15">
                                          <table id="tblAttachFile" border="1" cellspacing="0" class="grid mb15 " summary="첨부화일 표입니다."> <!-- summary는 표의 간략한 설명을 써주면 됨 -->
                            <colgroup>
                                <col width="7%">
                                <col width="27%">
                                <col width="28%">
                                <col width="*%">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="row" class="ce">순번</th>
                                    <th scope="row">서류구분</th>
                                    <th scope="row" colspan="2" style="text-align: center;">첨부파일</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- <tr>
                                  <td class="ce">
                                    1<input name="fileNameCd" id="fileNameCd_D2" value="2" type="hidden" style="border:0px;" readonly="readonly"  />
                                  </td>
                                  <td>보상금액산정내역서</td>
                                  <td colspan="2">
                                    <input type="hidden" name="fileDelYn" />
                                  <span id="spfileD2">
                                      <input type="file" name="fileD2" id="file_D2" title="첨부파일" 
                                          class="inputData L w10" style="width: 300px;" 
                                          onkeydown="return false;" onchange="fn_chkFileType(this,'D2');"/>
                                  </span>
                                  <input name="hddnFile" id="hddnFile_D2" type="text" />
                                  <input name="fileName" id="fileName_D2" type="text" title="파일명" 
                                      onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" />
                                  <input name="realFileName" id="realFileName_D2" type="text" />
                                  <input name="fileSize" id="fileSize_D2" type="text" />
                                  <input name="filePath" id="filePath_D2" type="text" />
                                  </td>
                                </tr> -->
                                <!-- 수정기능 추가str -->
                                <tr>
                                  <td class="ce">1</td>
                                  <td>이용승인신청명세서</td>
                                  <td>
                                    <c:set var="attcSeqn" value="0"/>
                                    <c:set var="existYn" value="N"/>
                                    <c:if test="${not empty statApplication.fileList}">
                                  <!--<c:forEach items="${statApplication.fileList}" var="fileList">
                                        <c:if test="${fileList.fileNameCd == '2'}">
                                          <a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
                                            onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
                                            ${fileList.fileName }
                                          </a>
                                          &nbsp;&nbsp;&nbsp;<input type="checkbox" name="fileDelYnChk" id="fileDelYnChkD2" /> 
                                          <span class="blue2 p11">[삭제]</span>
                                          <c:set var="attcSeqn" value="${fileList.attcSeqn }"/>
                                        </c:if>
                                      </c:forEach>-->
                                    </c:if>
                                    <input name="attcSeqn" id="attcSeqn_D1" value="${attcSeqn }" type="hidden" />
                                  </td>
                                  <td>
                                    <input name="fileNameCd" id="fileNameCd_D1" value="1" type="hidden" style="border:0px;" readonly="readonly"  />
                                    <input type="hidden" name="existYn" value="${existYn }"/>
                                    <input type="hidden" name="fileDelYn" id="fileDelYn_D1" />
                                    <span id="spfileD1">
                                      <input type="file" title="파일" name="fileD1" id="file_D1"      class="inputData R w100"          onkeydown="return false;" onchange="fn_chkFileType(this,'D1');"/>
                                  </span>
                                  <input name="hddnFile" id="hddnFile_D1" type="hidden" style="display:none;" />
                                  <input name="fileName" title="파일명" id="fileName_D1" type="text" 
                                      onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
                                  <input name="realFileName" id="realFileName_D1" type="hidden" style="display:none;" />
                                  <input name="fileSize" id="fileSize_D1" type="hidden" style="display:none;" />
                                  <input name="filePath" id="filePath_D1" type="hidden" style="display:none;" />
                                  </td>
                                </tr>
                                <tr>
                                  <td class="ce">2</td>
                                  <td>보상금액산정내역서</td>
                                  <td>
                                    <c:set var="attcSeqn" value="0"/>
                                    <c:if test="${not empty statApplication.fileList}">
                                      <!--<c:forEach items="${statApplication.fileList}" var="fileList">
                                        <c:if test="${fileList.fileNameCd == '2'}">
                                          <a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
                                            onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
                                            ${fileList.fileName }
                                          </a>
                                          &nbsp;&nbsp;&nbsp;<input type="checkbox" name="fileDelYnChk" id="fileDelYnChkD2" /> 
                                          <span class="blue2 p11">[삭제]</span>
                                          <c:set var="attcSeqn" value="${fileList.attcSeqn }"/>
                                        </c:if>
                                      </c:forEach>-->
                                    </c:if>
                                    <input name="attcSeqn" id="attcSeqn_D2" value="${attcSeqn }" type="hidden" />
                                  </td>
                                  <td>
                                    <input name="fileNameCd" id="fileNameCd_D2" value="2" type="hidden" style="border:0px;" readonly="readonly"  />
                                    <input type="hidden" name="fileDelYn" id="fileDelYn_D2" />
                                  <span id="spfileD2">
                                      <input type="file" title="첨부파일" name="fileD2" id="file_D2" 
                                          class="inputData R w100"
                                          onkeydown="return false;" onchange="fn_chkFileType(this,'D2');"/>
                                  </span>
                                  <input name="hddnFile" id="hddnFile_D2" type="text" style="display:none;" />
                                  <input name="fileName" title="파일명" id="fileName_D2" type="text" 
                                      onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
                                  <input name="realFileName" id="realFileName_D2" type="hidden" style="display:none;" />
                                  <input name="fileSize" id="fileSize_D2" type="hidden" style="display:none;" />
                                  <input name="filePath" id="filePath_D2" type="hidden" style="display:none;" />
                                  </td>
                                </tr>
                                <tr>
                                  <td class="ce">3</td>
                                  <td>저작물 공표여부 확인서류</td>
                                  <td>
                                    <c:set var="attcSeqn" value="0"/>
                                    <c:if test="${not empty statApplication.fileList}">
                                       <!--<c:forEach items="${statApplication.fileList}" var="fileList">
                                        <c:if test="${fileList.fileNameCd == '2'}">
                                          <a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
                                            onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
                                            ${fileList.fileName }
                                          </a>
                                          &nbsp;&nbsp;&nbsp;<input type="checkbox" name="fileDelYnChk" id="fileDelYnChkD2" /> 
                                          <span class="blue2 p11">[삭제]</span>
                                          <c:set var="attcSeqn" value="${fileList.attcSeqn }"/>
                                        </c:if>
                                      </c:forEach>-->
                                    
                                    </c:if>
                                    <input name="attcSeqn" id="attcSeqn_D3" value="${attcSeqn }" type="hidden" />
                                  </td>
                                  <td>
                                    <input name="fileNameCd" id="fileNameCd_D3" value="3" type="hidden" style="border:0px;" readonly="readonly"  />
                                    <input type="hidden" name="fileDelYn" id="fileDelYn_D3" />
                                  <span id="spfileD3">
                                      <input type="file" title="첨부파일" name="fileD3" id="file_D3" 
                                          class="inputData R w100"
                                          onkeydown="return false;" onchange="fn_chkFileType(this,'D3');"/>
                                  </span>
                                  <input name="hddnFile" id="hddnFile_D3" type="text" style="display:none;" />
                                  <input name="fileName" title="파일명" id="fileName_D3" type="text" 
                                      onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
                                  <input name="realFileName" id="realFileName_D3" type="hidden" style="display:none;" />
                                  <input name="fileSize" id="fileSize_D3" type="hidden" style="display:none;" />
                                  <input name="filePath" id="filePath_D3" type="hidden" style="display:none;" />
                                  </td>
                                </tr>
                                <tr>
                                  <td class="ce">4</td>
                                  <td>거소 미확인여부 확인서류</td>
                                  <td>
                                    <c:set var="attcSeqn" value="0"/>
                                    <c:if test="${not empty statApplication.fileList}">
                                       <!--<c:forEach items="${statApplication.fileList}" var="fileList">
                                        <c:if test="${fileList.fileNameCd == '2'}">
                                          <a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
                                            onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
                                            ${fileList.fileName }
                                          </a>
                                          &nbsp;&nbsp;&nbsp;<input type="checkbox" name="fileDelYnChk" id="fileDelYnChkD2" /> 
                                          <span class="blue2 p11">[삭제]</span>
                                          <c:set var="attcSeqn" value="${fileList.attcSeqn }"/>
                                        </c:if>
                                      </c:forEach>-->
                                    </c:if>
                                    <input name="attcSeqn" id="attcSeqn_D4" value="${attcSeqn }" type="hidden" />
                                  </td>
                                  <td>
                                    <input name="fileNameCd" id="fileNameCd_D4" value="4" type="hidden" style="border:0px;" readonly="readonly"  />
                                    <input type="hidden" name="fileDelYn" id="fileDelYn_D4" />
                                  <span id="spfileD4">
                                      <input type="file" title="첨부파일" name="fileD4" id="file_D4"   class="inputData R w100"  onkeydown="return false;" onchange="fn_chkFileType(this,'D4');"/>
                                  </span>
                                  <input name="hddnFile" id="hddnFile_D4" type="text" style="display:none;" />
                                  <input name="fileName" title="파일명" id="fileName_D4" type="text"   onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
                                  <input name="realFileName" id="realFileName_D4" type="hidden" style="display:none;" />
                                  <input name="fileSize" id="fileSize_D4" type="hidden" style="display:none;" />
                                  <input name="filePath" id="filePath_D4" type="hidden" style="display:none;" />
                                  </td>
                                </tr>
                                <tr>
                                  <td class="ce">5</td>
                                  <td>협의경과서류</td>
                                  <td>
                                    <c:set var="attcSeqn" value="0"/>
                                    <c:if test="${not empty statApplication.fileList}">
                                      <!--<c:forEach items="${statApplication.fileList}" var="fileList">
                                        <c:if test="${fileList.fileNameCd == '2'}">
                                          <a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
                                            onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
                                            ${fileList.fileName }
                                          </a>
                                          &nbsp;&nbsp;&nbsp;<input type="checkbox" name="fileDelYnChk" id="fileDelYnChkD2" /> 
                                          <span class="blue2 p11">[삭제]</span>
                                          <c:set var="attcSeqn" value="${fileList.attcSeqn }"/>
                                        </c:if>
                                      </c:forEach>-->
                                    </c:if>
                                    <input name="attcSeqn" id="attcSeqn_D5" value="${attcSeqn }" type="hidden" />
                                  </td>
                                  <td>
                                    <input name="fileNameCd" id="fileNameCd_D5" value="5" type="hidden" style="border:0px;" readonly="readonly"  />
                                    <input type="hidden" name="fileDelYn" id="fileDelYn_D5" />
                                  <span id="spfileD5">
                                      <input type="file" title="첨부파일" name="fileD5" id="file_D5"                                          class="inputData R w100"
                                          onkeydown="return false;" onchange="fn_chkFileType(this,'D5');"/>
                                  </span>
                                  <input name="hddnFile" id="hddnFile_D5" type="text" style="display:none;" />
                                  <input name="fileName" title="파일명" id="fileName_D5" type="text" 
                                      onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
                                  <input name="realFileName" id="realFileName_D5" type="hidden" style="display:none;" />
                                  <input name="fileSize" id="fileSize_D5" type="hidden" style="display:none;" />
                                  <input name="filePath" id="filePath_D5" type="hidden" style="display:none;" />
                                  </td>
                                </tr>
                                <c:set var="fileIdx" value="6"/>
                                <c:if test="${not empty statApplication.fileList}">
                                  <c:forEach items="${statApplication.fileList}" var="fileList">
                                    <c:if test="${fileList.fileNameCd == '99'}">
                                <tr id="trAttcFile_D${fileIdx }">
                                  <td class="ce"><input name="chkDel1" title="선택"  id="chkDel1_${fileIdx }" type="checkbox" /></td>
                                  <td>기타</td>
                                  <td colspan="2">
                                    <a href="#1" onclick="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}');" 
                                      onkeypress="fn_fileDownLoad('${fileList.filePath}','${fileList.fileName}','${fileList.realFileName}')">
                                      ${fileList.fileName }
                                      <input name="attcSeqn" id="attcSeqn_D${fileIdx }" value="${fileList.attcSeqn }" type="hidden" />
                                      ${fileList.attcSeqn }
                                    </a>
                                    <input name="fileNameCd" id="fileNameCd_D${fileIdx }" value="${fileList.fileNameCd }" type="hidden" style="border:0px;" readonly="readonly"  />
                                    <input type="hidden" name="fileDelYn" id="fileDelYn_D${fileIdx }" />
                                    <input name="hddnFile" id="hddnFile_D${fileIdx }" type="hidden" style="display:none;" />
                                  <input name="fileName" title="파일명" id="fileName_D${fileIdx }" type="text" 
                                      onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" style="display:none;" />
                                  <input name="realFileName" id="realFileName_D${fileIdx }" type="hidden" style="display:none;" />
                                  <input name="fileSize" id="fileSize_D${fileIdx }" type="hidden" style="display:none;" />
                                  <input name="filePath" id="filePath_D${fileIdx }" type="hidden" style="display:none;" />
                                  </td>
                                </tr>
                                    <c:set var="fileIdx" value="${fileIdx+1 }"/>
                                    </c:if>
                                  </c:forEach>
                                </c:if>
                                <!-- 수정기능 추가end -->
                                                  </tbody>
                          </table>
                        </div>
                        </c:if>
                      </div>
                      <!-- 첨부화일 끝 -->
                    </td>
                  </tr>
                              </tbody>
                             </table>
              <!-- //그리드스타일 -->

              <!-- 보상금신청 오프라인 관련 영역 -->
                      <div id="divKappOffline1" class="white_box">
                        <div class="box5">
                          <div class="box5_con floatDiv" style="width:713px;">
                            <p class="fl mt5"><img alt="" src="/images/2012/content/box_img4.gif"></p>
                            <div class="fl ml30 mt5">
                              <h4><strong>첨부파일(증빙서류) 안내</strong></h4>
                              <ul class="list1 mt10">
                                <li>별지 제2호서식에 따른 이용승인신청명세서(저작물,실명,음반,방송,데이터베이스의 형태 및 내용이 명확하지 아니한 경우에는 그 견본, 도면 또는 사진 등을 첨부하여야 합니다) 1부
                                  <p><img src="/images/2012/button/btn_app8_01.gif" alt="이용승인 신청명세서"/>
                                  <a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form' />','이용_승인신청명세서.hwp','이용_승인신청명세서.hwp')">
                                  <img src="/images/2012/button/btn_app7_02.gif" alt="양식다운로드"/>
                                  </a></p>
                                </li>
                                <li>보상금액산정내역서 1부</li>
                                <li>해당 저작물 등이 공표되었음을 밝힐 수 있는 서류 1부</li>
                                <li>저작재산권자, 저작인접권자 또는 데이터베이스제작자나 그의 거소를 알 수 없을을 밝힐 수 있는 서류 <br/>
                                  <span class="blue2 thin">(위 사유로 승인 신청하는 경우에 한정합니다.)</span> 1부 </li>
                                <li>협의에 관한 경과서류 <span class="blue2 thin">(협의가 성립되지 아니하여 승인 신청하는 경우에 한정합니다)</span> 1부</li>
                                <li>해당 음반이 우리나라에서 판매되어 3년이 경과하였음을 밝힐 수 있는 서류 <span class="blue2 thin">(법 제52조 및 법 제89조에 따라 승인 신청하는 경우에 한정합니다)</span> 1부 </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- //보상금신청 오프라인 관련 영역 --> 
            </div>

            <!-- button area str -->
            <div class="btnArea">
              <p class="fl">
                <span class="button medium gray"><a href="#1" <c:if test="${isMyPage=='Y'}" > onclick="fn_goMyList();" onkeypress="fn_goMyList();" </c:if><c:if test="${isMyPage==''}" > onclick="fn_goList();" onkeypress="fn_goList();" </c:if>>취소</a></span>
              </p>
              <p class="fr">
                <span class="button medium"><a href="#1" onclick="fn_doSave('goList');" onkeypress="fn_doSave('goList');">임시저장</a></span> 
                <span class="button medium"><a href="#1" onclick="fn_doSave('goStep2');" onkeypress="fn_doSave('goStep2');">다음단계</a></span>
              </p>
            </div>
            <!-- button area end -->
          </form>
          </div>
<!--          <form method="post" action="/stat/ww.do" enctype="multipart/form-data">
          <input type="file" name="file1"/>
          <input type="file" name="file2"/>
          <input type="submit" value="upload"/> --> 
        
          </form>
          <!-- //section -->
        </div>
        <!-- //주요컨텐츠 end -->
        <p class="clear"></p>
      </div>
      <!-- //content -->

      <!-- FOOTER str-->
      <!-- 2017변경 -->
      <jsp:include page="/include/2017/footer.jsp" />
      <%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
      <!-- //FOOTER end -->
    <!-- //CONTAINER -->
<script type="text/JavaScript">
<!--
<c:if test="${isModi == '02'}">
  window.onload = function(){

  //체크박스 셋팅
  var frm = document.frm;
  
  if('${statApplication.applyType01 }' == '1') {
    frm.applyType01.checked = true;
    frm.dummyApplyType01.checked = true;
  }
  if('${statApplication.applyType02 }' == '1') {
    frm.applyType02.checked = true;
    frm.dummyApplyType02.checked = true;
  }
  if('${statApplication.applyType03 }' == '1') {
    frm.applyType03.checked = true;
    frm.dummyApplyType03.checked = true;
  }
  if('${statApplication.applyType04 }' == '1') {
    frm.applyType04.checked = true;
    frm.dummyApplyType04.checked = true;
  }
  if('${statApplication.applyType05 }' == '1') {
    frm.applyType05.checked = true;
    frm.dummyApplyType05.checked = true;
  }
}
</c:if> 
//-->
</script>
</body>
</html>

