<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 이용승인신청 | 저작권자찾기</title>

<!-- <link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css"> -->
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<style type="text/css">
<!--

table tbody tr td.tbBg{
	background-color: #fafafa;	
}

-->
</style>

<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/javascript" src="<c:url value="/js/jquery-1.7.1.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/json2.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.form.2.34.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/quickpager2.jquery.js"/>"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>
<script type="text/javascript" src="./js/2012/prototype.js"> </script>


<script type="text/javascript"> 
<!--

function fn_goList(){
	var frm = document.srchForm;

	if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.encoding = "application/x-www-form-urlencoded";
		frm.action = "/stat/statSrch.do";
		frm.submit();
	}
}

function fn_goMyList(){
	var frm = document.srchForm;

	if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.encoding = "application/x-www-form-urlencoded";
		frm.action = "/myStat/statRsltInqrList.do";
		frm.submit();
	}
}



jQuery(function(){
	jQuery(".pageme").quickPager({
		pageSize: 5,
		naviSize: 10,
		currentPage: 1,
		holder: ".pager"
	});
	
	jQuery(window).load(function() {
		//alert("HTML로드 완료");
	});
	
});

//임시저장 및 화면이동
function fn_doSave(sDiv) {
	var frm = document.frm;
	frm.stat_cd.value = '${statCd }';
	frm.action_div.value = sDiv;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		/*if(sDiv == 'goStep3' && frm.existYn.value != 'Y') {
			alert('현재 작성하신 이용승인신청에 대한\n\r\'이용승인명세서\' 관련 첨부파일이 첨부되지 않았습니다.\n\r이전단계 이동합니다.');
			frm.stat_cd.value = '${statCd }';
		}*/
	
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/stat/statPrpsDetl2.do";
		frm.submit();
	}
}

//이전화면
function fn_goStep1() {
	var frm = document.frm;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		frm.stat_cd.value = '${statCd }';
		frm.isModi.value = "02";
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/stat/statPrpsMain.do";
		frm.submit();
	}
}




function addfile(file, formnumber, fileType, textName){
	 
  var fileName = file.value;	
	jQuery('#'+textName).val(fileName);

	var form = $('#fileForm')[formnumber];
    var formData = new FormData(form);
    formData.append("fileType", fileType);
    formData.append("fileObj", file.files[0]);
    formData.append("fileName", encodeURIComponent(fileName));

    $.ajax({
        url: '/stat/statFileSave.do',
                processData: false,
                contentType: false,
                data: formData,
                type: 'POST',
                
                success: function(result){
                  
                    
                }
        }); 
      
        /*  var fileName = file.value;	
        var form = $('#fileForm')[formnumber];
      
        var formData = new FormData(form);
    	jQuery('#'+textName).val(fileName);
    	
        $.ajax({      
   
            url : '/stat/statFileSave.do',

            method : "post",

            contentType: "application/x-www-form-urlencoded; charset=euc-kr",

            data : encodeURI(formData),

            success: function(responseText, statusText){

              

            },

            error: function(){ 

              alert("에러발생!!"); 

            } 

           })  */

}

//-->
</script>


<style type="text/css">
#titleImg{
	height: 250px;
}
p {
	
	margin-bottom:1px;
	margin-top:0px;
}

ul.paging li {
    padding: 10px;
    background: #ccc;
    font-family: georgia;
    font-size: 24px;
    color: #fff;
    line-height: 1;
    width: 180px;
    margin-bottom: 1px;
}

ul.pageNav li{
    display:block;
    floaT: left;
    padding: 3px;
    font-family: georgia;
}

ul.pageNav li a{
    color: #333;
    text-decoration: none;
}

/* UI Object */ 

.paginate_regular{padding:15px 0;text-align:center;line-height:normal}  

.paginate_regular a,  

.paginate_regular strong{display:inline-block;position:relative;margin:0 -2px;padding:2px 8px;font-weight:bold;font-size:12px;font-family:Tahoma, Sans-serif;color:#333;line-height:normal;text-decoration:none;vertical-align:middle}  

.paginate_regular a:hover,  

.paginate_regular a:active,  

.paginate_regular a:focus{background-color:#f8f8f8}  

.paginate_regular strong{color:#f60}  

.paginate_regular .direction{font-weight:normal;color:#767676;white-space:nowrap}  

.paginate_regular .direction span{font-weight:bold;font-size:14px}  

.paginate_regular .direction:hover,  

.paginate_regular .direction:active,  

.paginate_regular .direction:focus{background-color:#fff;color:#333}  

.paginate_regular a.currentPage{color:#f60} 

.grid tbody td.errBg{
	background-color: #faebe4;
}

.loadingImg {
		width:32px;
		height:32px;
		text-align:right;
		position:absolute;
	}
	
	.loadingImg.move {
		opacity: 0.4;
		position:absolute;
	}
	
	.loadingImg>img {
		width:32px;
		height:32px;
		display: block;
	}

/* //UI Object */ 
.con_lf{
	width: 50px;
}

.content{
	left : 600px;
	width: 500px;
	position: relative;
}

 #container{
	margin-left:200px;
	width: 780px;
}
.contentBody{
	left : -350px;  
}
</style>
<link type="text/css" rel="stylesheet" href="/css/new181203.css">
</head>

<body>
	<jsp:include page="/include/2017/header.jsp" />
	
		<div id="contents">
			<div class="con_lf">
				<div class="con_lf_big_title">저작권자 찾기</div>
				<ul class="sub_lf_menu">
							<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
				<!-- <li><a class="on" href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
					<li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
					<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
 					<li><a class="on" href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
					<!--<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li> -->
					<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
					<!-- <li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li> -->
				</ul>
			</div> <!-- conf_if -->
				<form name="srchForm" action="#">
			<c:if test="${isMyPage == 'Y'}">
				<input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
				<input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
				<input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
				<input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
				<input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
				<input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
			</c:if>
			<c:if test="${isMyPage == ''}">
				<input type="hidden" name="pageNum"		value="${srchVO.pageNum}" />
				<input type="hidden" name="searchCondition"	value="${srchVO.searchCondition}" />
				<input type="hidden" name="searchKeyword1"	value="${srchVO.searchKeyword1}" />
			</c:if>
			<input type="submit" style="display:none;">
		</form>
		    <form name="frm" action="#">
                    	<c:if test="${isMyPage == 'Y'}">
							<input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
							<input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
							<input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
							<input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
							<input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
							<input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
							<input type="hidden" name="worksId" value="${worksId}" />
						</c:if>
						<c:if test="${isMyPage == ''}">
							<input type="hidden" name="pageNo" value="${srchVO.pageNum}"/>
							<input type="hidden" name="searchCondition" value="${srchVO.searchCondition}"/>
							<input type="hidden" name="searchKeyword1" value="${srchVO.searchKeyword1}"/>
							<input type="hidden" name="worksId" value="${srchVO.searchWorksId}" />
						</c:if>
						<input type="hidden" name="isMy" value="${isMy}"/>
						<input type="hidden" name="action_div" value=""/>
						<input type="hidden" name="applyWriteYmd" value="${srchParam.applyWriteYmd }">
						<input type="hidden" name="applyWriteSeq" value="${srchParam.applyWriteSeq }">
						<input type="hidden" name="apply_write_ymd" value="${srchParam.applyWriteYmd }">
						<input type="hidden" name="apply_write_seq" value="${srchParam.applyWriteSeq }">
						<input type="hidden" name="stat_cd"/>
						<input type="hidden" name="isModi"/>
				    	<input type="hidden" name="existYn" value="${existYn}"/>
						<input type="submit" style="display:none;">   
					</form>    
			<div id="container">		
			<div class="con_rt">
				<div class="con_rt_head">
					<!-- <img src="https://www.findcopyright.or.kr/images/sub_img/sub_home.png" alt="홈 페이지"> -->
					&gt;
					법정허락 승인 신청
					&gt;
					<span class="bold">법정허락 승인 신청</span>
				</div>
				<div class="con_rt_hd_title">법정허락 승인 신청</div>
				<div id="sub_contents_con">
				<!-- 2018.12.03 컨텐츠 추가 -->
				
					<div class="list_step_apply">
						<ul>
							<li>
								<div class="wrap">
									<p>STEP 01</p>
									<strong>법정허락 승인 신청 서류 작성</strong>
								</div>
							</li>
							<li class="active">
								<div class="wrap">
									<p>STEP 02</p>
									<strong>첨부서류 제출하기</strong>
								</div>
							</li>
							<li>
								<div class="wrap">
									<p>STEP 03</p>
									<strong>신청정보 확인 및 제출하기</strong>
								</div>
							</li>
						</ul>
					</div>
					<div class="sub01_con_bg4_tp mar_tp30"></div>
					<div class="sub01_con_bg4">
						<h3 class="txt_yellow_n fs14">첨부파일(증빙서류) 안내</h3>
						<p class="word_dian_bg big mar_tp5 fs13"><strong>별지 제 2호서식에 따른 이용승인신청명세서 1부</strong> (저작물, 실명, 음반, 방송, 데이터베이스의 형태 및 내용이 명확하지 아니한 경우에는 그 견본, 도면 또는 사진 등을 첨부하여야 합니다.)</p>
						<a href="" class="btn_down_document">이용 승인 신청명세서 양식 다운로드</a>
						<p class="word_dian_bg big mar_tp5 fs13">보상금액산정내역서 1부</p>
						<p class="word_dian_bg big mar_tp5 fs13">해당 저작물 등이 공표되었음을 밝힐 수 있는 서류 1부</p>
						<p class="word_dian_bg big mar_tp5 fs13">저작재산권자, 저작인접권자 또는 데이터베이스 제작자나 그의 거소를 알 수 없음을 밝힐 수 있는 서류 1부<br>
							 <span class="txt_red fs12">(위 사유로 승인 신청하는 경우에 한정합니다.)</span>
						</p>
						<p class="word_dian_bg big mar_tp5 fs13">협의에 관한 경과서류 1부 <span class="txt_red fs12">(협의가 성립되지 아니하여 승인 신청하는 경우에 한정합니다,)</span></p>
						<p class="word_dian_bg big mar_tp5 fs13">해당 음반이 우리나라에서 판매되어 3년이 경과하였음을 밝힐 수 있는 서류 1부 <span class="txt_red fs12">(법 제 52조 및 법 제 89조에 따라 승인 신청하는 경우에  한정합니다.) </span></p>
					</div>
					<h2 class="sub_con_h2 mar_tp30">첨부서류</h2>



					<form id = "fileForm" method="post" enctype="multipart/form-data" >
	 	    		<div class="list_file" style="text-align: center;">
						<div class="ment">
							<p class="txt1">1. 이용 승인신청명세서</p>
							<p class="txt2">(상단에 양식을 다운받아 업로드해주세요. 엑셀만 가능합니다.)</p>
						</div>
						<input type="text" id="text01" class="input_style1 prj_file" readonly value="" >
						<input type="file" class="insert_file_target" id="file01" onchange="javascript:addfile(this,0,1,'text01');"  accept=".xlsx ,.xls,.txt">
						<span class="insert_file btn_style1">
							<span onclick="javascript:jQuery('#file01').click();">업로드</span>
						</span>
					</div>
					</form>
					 <form id = "fileForm" method="post" enctype="multipart/form-data" > 
						<div class="list_file"style="text-align: center;">
							<div class="ment">
								<p class="txt1">2. 보상금액 산정내역서</p>
							</div>
							<input type="text" id="text02" class="input_style1 prj_file" readonly value="" >
							<input type="file" class="insert_file_target" id="file02" onchange="javascript:addfile(this,1,2,'text02');">
							<span class="insert_file btn_style1">
								<span onclick="javascript:jQuery('#file02').click();">업로드</span>
							</span>
						</div>
					</form>
				 	<form id = "fileForm" method="post" enctype="multipart/form-data">
					<div class="list_file"style="text-align: center;">
						<div class="ment">
							<p class="txt1">3. ’상당한 노력’ 절차 수행 증빙 자료</p>
							<p class="txt2">(법정허락 승인 저작물과 미분배 공시 3년이 경과한 저작물은 해당 서류를 제출하지 않으셔도 됩니다.)</p>
						</div>
						<input type="text" id="text03" class="input_style1 prj_file" readonly value="" >
						<input type="file" class="insert_file_target" id="file03" onchange="javascript:addfile(this,2,99,'text03');" >
						<span class="insert_file btn_style1">
							<span onclick="javascript:jQuery('#file03').click();">업로드</span>
						</span>
					</div>
					</form>
				    <form id = "fileForm" method="post" enctype="multipart/form-data" >
					<div class="list_file" style="text-align: center;">
						<div class="ment">
							<p class="txt1">4. ‘상당한 노력’ 절차 외 권리자를 찾기 위해 노력한 활동 자료</p>
							<p class="txt2">(법정허락 승인 저작물과 미분배 공시 3년이 경과한 저작물은 해당 서류를 제출하지 않으셔도 됩니다.)</p>
						</div>
	    			<input type="text" id="text04" class="input_style1 prj_file" readonly value="" >
						<input type="file" class="insert_file_target" id="file04" onchange="javascript:addfile(this,3,99,'text04');" >
						<span class="insert_file btn_style1">
							<span onclick="javascript:jQuery('#file04').click();">업로드</span>
						</span>
						</div>
					</form>	
	              <form id = "fileForm" method="post" enctype="multipart/form-data" >
					<div class="list_file" style="text-align: center;">
						<div class="ment">
							<p class="txt1">5. 해당 저작물이 공표되었음을 밝힐 수 있는 서류</p>
						</div>
						<input type="text" id="text05" class="input_style1 prj_file" readonly value="" >
						<input type="file" class="insert_file_target" id="file05" onchange="javascript:addfile(this,4,99,'text05');" >
						<span class="insert_file btn_style1">
							<span onclick="javascript:jQuery('#file05').click();">업로드</span>
						</span>
					</div>
				</form>
				 <form id = "fileForm" method="post" enctype="multipart/form-data" >
				<div class="list_file" style="text-align: center;">
					<div class="ment">
						<p class="txt1">6. 신청 저작물의 파일 또는 내용물</p>
					</div>
						<input type="text" id="text06" class="input_style1 prj_file" readonly value="" >
					<input type="file" class="insert_file_target" id="file06" onchange="javascript:addfile(this,5,99,'text06');" >
					<span class="insert_file btn_style1">
						<span onclick="javascript:jQuery('#file06').click();">업로드</span>
					</span>
				</div>
				</form>
				
				<div class="btn_area mar_tp50">
					
													<a href="#1" onclick="fn_doSave('goStep3');" onkeypress="fn_doSave('goStep3');" class="btn_style1">다음단계</a>
				<!-- <a href="step_1.html" class="btn_style2">이전단계</a><a href="" class="btn_style2">임시저장</a> --><!-- <a href="step_3.html" class="btn_style1">다음단계</a> -->
				</div>

			<!-- 2018.12.03 컨텐츠 추가 끝-->

			</div>
		</div>
	</div>
</div>	
	<jsp:include page="/include/2017/footer.jsp" />	
<script type="text/JavaScript">
</script>
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>

