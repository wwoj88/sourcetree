<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 이용승인신청 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css"> -->
<style type="text/css">
<!--

	table tbody tr td.tbBg{
		background-color: #fafafa;	
	}
	.loadingImg {
		width:32px;
		height:32px;
		text-align:right;
		position:absolute;
	}
	
	.loadingImg.move {
		opacity: 0.4;
		position:absolute;
	}
	
	.loadingImg>img {
		width:32px;
		height:32px;
		display: block;
	}

-->
</style>

<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/javascript" src="<c:url value="/js/jquery-1.7.1.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/json2.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery.form.2.34.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/quickpager2.jquery.js"/>"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script>

<script type="text/javascript"> 
<!--

function fn_calObj(frmName, objName, obj){
	var pObj = findParentTag(obj, "TR"); 
	var position = findTagByName(pObj, "input", objName); 
	showCalendarObj(frmName, position); 
	//showCalendar(frmName, objName);
} 

var errRow = '${errRow}';
var writeRow = '${writeRow}'
var applyWorksCnt = '${applyWorksCnt}';


/* alert("errRow: "+errRow+"\n"+
		"writeRow: "+writeRow+"\n"+
		"applyWorksCnt: "+applyWorksCnt
		); */

//신청서구분
function fn_chkApplyType(oObj, sNo) {
	var oChkApplyType = document.getElementsByTagName("input");
	for(i = 0; i < oChkApplyType.length; i++) {
		if(oChkApplyType[i].type == "checkbox") {
			
			if( (oChkApplyType[i].id).indexOf("dummyApplyType0") > -1 && (oChkApplyType[i].id).indexOf("_"+sNo) > -1 && oChkApplyType[i].id != oObj.id) {
				oChkApplyType[i].checked = false;
			}
		}
	}
}		
		
//첨부화일 확장자 제한
function fn_chkFileType(obj,iRowIdx){
	
	if(iRowIdx == 'D1'){
		jQuery("input[name='fileD1']").each(function(index){
	    	jQuery(this).css("color","#868b8f");
	    })	
	}
	
	var frm = document.frm;
	var refuseFile = ["HTM","HTML","PHP","PHP3","ASP","JSP","CGI","INI","PL"];
	var str = obj.value;
	var nPos = 0;
	var sepCnt = 0;
	var isAlert = false;
	var msg = "";
	

	
	//확장자 구하기
	if( str != null ){
		var splitLength = (str.split(".")).length;		
		str = str.split(".")[splitLength-1];
	}
	
	//확장자 구하기
	/*
	while(nPos >= 0){
		nPos = str.indexOf(".");
		if(nPos> 0){
			str = str.substring(nPos+1,str.length);
		}
		sepCnt++;
	}
	*/
	 
	for(i=0; i<refuseFile.length; i++){
		if(msg.length > 0){
			msg += ", ";
		}
		msg += refuseFile[i];
		
		if(str.toUpperCase() == refuseFile[i]){
			isAlert = true;
		}
	}
	
	if(sepCnt > 2 || isAlert == true){
		alert(msg + "확장자를 가진 화일이나 \n\r이중확장자(\"---.---.---\") 화일은 올리실 수 없습니다. ");
		
	 	var spObj = document.getElementById("spfile"+iRowIdx);
	  	spObj.outerHTML = '<span id=\"spfile'+iRowIdx+'\"><input type=\"file\" name=\"file\" id=\"file_'+iRowIdx+'\" class=\"inputData L w100\" onkeydown=\"return false;\" onchange=\"fn_chkFileType(this,'+iRowIdx+');\"/></span>';
	  	return false;
	}
	
	if(iRowIdx == 'D1') {
		frm.existYn.value = 'Y';
	}
}
/* 첨부화일 관련 끝*/


function openSmplDetail(div) {

	var param = '';
	
	param = 'DVI='+div;
	
	var url = '/common/rghtPrps_smpl.jsp?'+param
	var name = '';
	var openInfo = 'target=rghtPrps_mvie, width=705, height=570, scrollbars=yes';
	
	window.open(url, name, openInfo);
}

function fn_goList(){
	var frm = document.srchForm;

	if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.encoding = "application/x-www-form-urlencoded";
		frm.action = "/stat/statSrch.do";
		frm.submit();
	}
}

function fn_goMyList(){
	var frm = document.srchForm;

	if(confirm("신청취소를 하시면 저장되지 않은 내용은 삭제 됩니다. 진행 하시겠습니까?")){
		frm.target = "_self";
		frm.method = "post";
		frm.encoding = "application/x-www-form-urlencoded";
		frm.action = "/myStat/statRsltInqrList.do";
		frm.submit();
	}
}

function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.form1;
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.action = "/board/board.do?method=fileDownLoad2";
	frm.submit();
} 

//임시저장 및 화면이동
function fn_doSave(sDiv) {
	var frm = document.frm;
	var nowStatCd = '${statCd }';
	frm.action_div.value = sDiv;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		/*if(sDiv == 'goStep3' && frm.existYn.value != 'Y') {
			alert('현재 작성하신 이용승인신청에 대한\n\r\'이용승인명세서\' 관련 첨부파일이 첨부되지 않았습니다.\n\r이전단계 이동합니다.');
			frm.stat_cd.value = '${statCd }';
		}*/
		
		var existYn = '${existYn}';
		
		if(existYn == 'Y'){
			if(fn_chkValueN()){
				//alert("validation success!!"+'${statCd }');
				frm.target = "_self";
				frm.method = "post";
				frm.stat_cd.value = '${statCd }';
				frm.action = "/stat/statPrpsDetl_priv.do";
				frm.submit();
			}
		}else{
			if(fn_chkValueE()){
				frm.target = "_self";
				frm.method = "post";
				frm.action = "/stat/statPrpsDetl_priv.do";
				frm.submit();
			}
		}
		
	}
}

//이전화면
function fn_goStep1() {
	var frm = document.frm;

	//로그인 체크
	var userId = '<%=sessUserIdnt%>';
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
		return;
		
	}else{
		frm.stat_cd.value = '${statCd }';
		frm.isModi.value = "02";
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/stat/statPrpsMain_priv_modi.do";
		frm.submit();
	}
}

//공표방법
function fn_chkPublMediCd(oObj, sNo) {
	var oChkApplyType = document.getElementsByTagName("input");
	for(i = 0; i < oChkApplyType.length; i++) {
		if(oChkApplyType[i].type == "checkbox") {
			
			if( (oChkApplyType[i].id).indexOf("dummyPublMediCd") > -1 
					&& (oChkApplyType[i].id).indexOf("_"+sNo) > -1 
					&& oChkApplyType[i].id != oObj.id
					&& oChkApplyType[i].name == "dummyPublMediCd"+sNo) {
				oChkApplyType[i].checked = false;
			}
		}
	}
	
	if((oObj.id).indexOf('99') > -1 ) {
		if(oObj.checked == true) {
			document.getElementById("publMediEtc"+sNo).style.display = "";
		}else{
			document.getElementById("publMediEtc"+sNo).style.display = "none";
			document.getElementById("publMediEtc"+sNo).value = "";
		}
	}
}


//필수체크
function fn_chkValue() {
	//첨부화일
	var oInput = document.getElementsByTagName("input");
	var arrVal = new Array();
	var arrCnt = 0;
	for(i=0; i<oInput.length; i++){
		var tmpId = oInput[i].id;
		if(tmpId.substring(0,5) == "file_"){
			var tmpVal = oInput[i].value;
			var sep = 0;
			for(j=0; j<tmpVal.length; j++){
		        if(tmpVal.charCodeAt(j) == 92){	// 아스키 92 = '\'
		            sep = j;
		        }
		    }
			arrVal[arrCnt] = tmpVal.substring(sep,tmpVal.length);
			arrVal[arrCnt] = arrVal[arrCnt].replace("\\","");
			
			arrCnt++;
		}
	}
	arrVal.length = arrCnt;
	
	var oHddnFile = document.getElementsByName("hddnFile");
	for(i=0; i<oHddnFile.length; i++){
		/*
		if(arrVal[i].length < 1){
			alert('파일명을 입력하세요');
			return false;
		}
		*/
		if(i==0){
			if(arrVal[i] != ''){
				//alert(arrVal[i]);
				
				var str = arrVal[i];
				
				//확장자 구하기
				if( str != null ){
					var splitLength = (str.split(".")).length;		
					str = str.split(".")[splitLength-1];
				}
				
				if(str != 'xlsx' && str != 'xls' && str != 'pdf' && str != 'jpg' && str != 'hwp'  ){
					/* alert("엑셀파일 형식이 아닌 파일은 올리실 수 없습니다."); */
					alert("파일 확장자의 경우 xlsx,xls,pdf,jpg,hwp 파일만 사용하실수있습니다");
					jQuery("input[name='fileD1']").each(function(index){
						jQuery(this).css("color","#c0c1c2");
				    })	
					return false;
				}else{
					jQuery("#fileDelYn_D1").val('Y');
				}
			}else{
				alert("첨부할 파일이 없습니다.")
				return false;
			}
			
		}
		oHddnFile[i].value = arrVal[i];
	}
	
	return true;
}

function boxInit(){
	var arrInputName = ["worksTitl","worksKind","worksForm","publYmd","publNatn","dummyPublMediCdD","publMedi","coptHodrName","coptHodrTelxNumb","coptHodrAddr","worksDesc","publMediEtc","dummyApplyType0"];
	for(var i=0; i<arrInputName.length; i++){
		if(i == 10){
			jQuery("textarea[name=\'"+arrInputName[i]+"\'").each(function(index){
				jQuery(this).val("");
			})
			
		}else if(i == 11){
			jQuery("input[name=\'"+arrInputName[i]+"\'").each(function(index){
				jQuery(this).val("").css("display","none");
			})
		}else if(i == 12){
			jQuery("input[name^=\'"+arrInputName[i]+"\']").each(function(index){
				jQuery(this).attr("checked",false);
			})
		}else{
			jQuery("input[name=\'"+arrInputName[i]+"\'").each(function(index){
				jQuery(this).val("");
			})
		}
	}
	jQuery("input[name^='dummyPublMediCdD']").each(function(index){
		jQuery(this).attr("checked",false);
	})
}

//필수체크 ExistYn값이 N이거나 ''일때
function fn_chkValueE() {
	var arrInputName = ["worksTitl","worksKind","worksForm","publYmd","publNatn","dummyPublMediCdD","publMedi","coptHodrName","coptHodrTelxNumb","coptHodrAddr","worksDesc"];
	var isReturn = true;
	for(var i=0; i<arrInputName.length; i++){
		if(i == 5){
			//저작물 갯수까지 for문 돌리고 each문을 돌려서 빈값 체크를 하자!
			var applyWorksCnt = '${applyWorksCnt}';
			//alert(applyWorksCnt);
			for(var j=0; j<applyWorksCnt; j++){
				var isCheckValue = false;
				jQuery("input[name='dummyPublMediCdD"+j+"\']").each(function(index){
					var isCheck = jQuery(this).attr('checked');
					if(isCheck == 'checked'){
						isCheckValue = true;
					}
				})
				if(!isCheckValue){
					alert("공표방법을(를) 선택해주세요.");
					jQuery("input[name='dummyPublMediCdD"+j+"\']").eq(0).focus();
					isReturn = false;
					return false;
				}
			}
		}else if(i == 10){
			jQuery("textarea[name=\'"+arrInputName[i]+"\']").each(function(index){
				if(jQuery(this).text() == ''){
					title = jQuery(this).attr("title");
					alert(title+"을(를) 입력해주세요.");
					jQuery(this).focus();
					isReturn = false;
					return false;
				}
			});
			if(!isReturn){
				return false;
			}
		}else{
			jQuery("input[name=\'"+arrInputName[i]+"\']").each(function(index){
				if(jQuery(this).val() == ''){
					title = jQuery(this).attr("title");
					alert(title+"을(를) 입력해주세요.");
					jQuery(this).focus();
					isReturn = false;
					return false;
				}
			});
			if(!isReturn){
				return false;
			}
		}
	}
	
	var chkCount = 0;
	
	var chkTrueNum = "";
	
	<c:set var="dySeq" value="0"/>
	<c:forEach items="${applyTypeList}" var="applyTypeList">
		<c:set var="dySeq" value="${dySeq + 1}"/>
		var oChk = document.getElementsByName("dummyApplyType0${dySeq}");
		var sVal = "";
		for(i = 0; i < oChk.length; i++) {
			if(oChk[i].checked == true) {
				sVal = sVal+"1";
				if(chkTrueNum == ''){
					chkTrueNum+=i+"";
				}else{
					chkTrueNum+=","+i;
				}
				chkCount++;
			}else{
				sVal = sVal+"0";
			}
		}
		document.getElementById("applyType0${dySeq}").value = sVal;
	</c:forEach>
	
	//alert("chkCount:"+chkCount)
	//alert("chkTrueNum:"+chkTrueNum)
	//alert("oChk.length:"+oChk.length)
	
	//0,2
	if(chkCount < oChk.length){
		alert("신청구분을 체크해주세요 ")
		
		var arrFocus = chkTrueNum.split(",");
		
		for(var a=0;a<arrFocus.length-1;a++){
			for(var b=a+1;b<arrFocus.length;b++){
				if(arrFocus[a]>arrFocus[b]){
					var tmp = arrFocus[a];
					arrFocus[a] = arrFocus[b];
					arrFocus[b] = tmp;
				}
			}
		}
		
		/* for(var c=0;c<arrFocus.length;c++){
			alert("순서대로 변경ㅎㅎㅎㅎ: "+arrFocus[c]);
		} */
		var max = 0;
		
		var isMax = true;
		//alert("arrFocus.length: "+arrFocus.length);
		//alert("처음max: "+max);
		//alert("arrFocus.length: "+arrFocus.length);
		for(var x=0; x<=arrFocus.length; x++){
			//alert("현재 arrFocus[x]는 "+arrFocus[x]+" x는 "+x);
			if(arrFocus[x] != x){
				if(isMax){
					max = x;
					isMax = false;
				}
			}
		}
		
		//alert("max: "+max);
		
			/*
		for(var x=0; x<arrFocus.length; x++){
			if(arrFocus[x] >= max){
				max = arrFocus[x];
			}
		}*/
		jQuery("#dummyApplyType01_D"+max).focus();
		return false;
	}
	
	oInput = document.getElementsByTagName("input");
	sVal = "";
	for(i = 0; i < oInput.length; i++) {
		if(oInput[i].type != 'undefined' && oInput[i].type == 'checkbox' && (oInput[i].name).indexOf('dummyPublMediCd') > -1) {
			if(oInput[i].checked == true) {
				sVal = sVal+oInput[i].value + "!";
			}
		}
	}
	document.getElementById("publMediCd").value = sVal;
	
	return true;
	
}


function fn_chkValueN(){
	if(applyWorksCnt != writeRow){
		alert("신청건수와 작성건수가 같지 않습니다.");
		return false;
	}
	
	if(errRow > 0){
		alert("오류건수("+errRow+"건)를 확인 후 다시 등록하세요.");
		return false;
	}
	return true;
}

jQuery(function(){
	jQuery(".pageme").quickPager({
		pageSize: 5,
		naviSize: 10,
		currentPage: 1,
		holder: ".pager"
	});
	
	var message = jQuery('input[name=message]').val();
	if(message != ''){
		alert(message);
	};
	jQuery(window).load(function() {
		//alert("HTML로드 완료");
		 jQuery(".loadingImg").css("display","none");
	});
	
	jQuery("#uploadBtn").bind("click",function(e){
		//alert("test");
		
		var frm = document.frm;

		//로그인 체크
		var userId = '<%=sessUserIdnt%>';
		if(userId == 'null' || userId == ''){
			alert('로그인이 필요한 화면입니다.');
			location.href = "/user/user.do?method=goSgInstall";
			return;
			
		}else{
			if(fn_chkValue()) {
				<c:if test="${isChk == '01'}">
					frm.action_div.value= 'updateFile'
					frm.target = "_self";
					frm.method = "post";
					frm.action = "/stat/statPrpsList_privU.do";
					frm.submit();
				</c:if>
				<c:if test="${isChk == '02'}">
					frm.action_div.value= 'updateFile'
					frm.target = "_self";
					frm.method = "post";
					frm.action = "/stat/statPrpsList_priv.do";
					frm.submit();
				</c:if>
			}
		}
	})
	
	/* var x = 0;
	var y = 0;
	var alt ="File Uploading...";
	var src ="/images/loading.gif";
	jQuery("<div id='move'><img></div>").addClass("loadingImg move")
	                           .css({
							   	left:x,
								top:y
							   })//x,y 좌표
	                           .find("img")//img요소
	                           .attr({
							   	"alt":alt,
								"src":src
							   })//img요소
							   .end()
							   .appendTo("body"); */
	
	/* jQuery(document).mousemove(function(e){
		 var x = e.pageX+10;
		 var y = e.pageY+10;
		 jQuery(".loadingImg").css({
			left:x,
			top:y
		});
	});  */
})

 
//-->
</script>


<style type="text/css">

/* 
p {
	background: #e5e5e5;
	margin-bottom:1px;
	margin-top:0px;
}

ul.paging li {
    padding: 10px;
    background: #ccc;
    font-family: georgia;
    font-size: 24px;
    color: #fff;
    line-height: 1;
    width: 180px;
    margin-bottom: 1px;
}

ul.pageNav li{
    display:block;
    floaT: left;
    padding: 3px;
    font-family: georgia;
}

ul.pageNav li a{
    color: #333;
    text-decoration: none;
}

/* UI Object */ 

.paginate_regular{padding:15px 0;text-align:center;line-height:normal}  

.paginate_regular a,  

.paginate_regular strong{display:inline-block;position:relative;margin:0 -2px;padding:2px 8px;font-weight:bold;font-size:12px;font-family:Tahoma, Sans-serif;color:#333;line-height:normal;text-decoration:none;vertical-align:middle}  

.paginate_regular a:hover,  

.paginate_regular a:active,  

.paginate_regular a:focus{background-color:#f8f8f8}  

.paginate_regular strong{color:#f60}  

.paginate_regular .direction{font-weight:normal;color:#767676;white-space:nowrap}  

.paginate_regular .direction span{font-weight:bold;font-size:14px}  

.paginate_regular .direction:hover,  

.paginate_regular .direction:active,  

.paginate_regular .direction:focus{background-color:#fff;color:#333}  

.paginate_regular a.currentPage{color:#f60} 

.grid tbody td.errBg{
	background-color: #faebe4;
}

.loadingImg {
		width:32px;
		height:32px;
		text-align:right;
		position:absolute;
	}
	
	.loadingImg.move {
		opacity: 0.4;
		position:absolute;
	}
	
	.loadingImg>img {
		width:32px;
		height:32px;
		display: block;
	}

/* //UI Object */ 
 */
</style>
</head>

<body>
	<c:set var="isMyPage" value=""/>
	<c:if test="${isMy == 'Y' }">
		<c:set var="isMyPage" value="Y"/>
	</c:if>
	<!-- 전체를 감싸는 DIVISION -->
	<!-- <div id="wrap"> -->
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />

		<!-- //HEADER end -->
		<!-- CONTAINER str-->
		<form name="form1" method="post" action="#">
			<input type="hidden" name="filePath"> <input type="hidden"
				name="fileName"> <input type="hidden" name="realFileName">
			<input type="hidden" name="action_div"> <input type="submit"
				style="display: none;">
		</form>
		<!-- CONTAINER str-->
		<form name="srchForm" action="#">
			<c:if test="${isMyPage == 'Y'}">
				<input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
				<input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
				<input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
				<input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
				<input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
				<input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
			</c:if>
			<c:if test="${isMyPage == ''}">
				<input type="hidden" name="pageNum"		value="${srchVO.pageNum}" />
				<input type="hidden" name="searchCondition"	value="${srchVO.searchCondition}" />
				<input type="hidden" name="searchKeyword1"	value="${srchVO.searchKeyword1}" />
			</c:if>
			<input type="submit" style="display:none;">
		</form>
		<div id="contents" >
		
				<!-- 래프 -->
				<c:if test="${isMyPage=='Y'}">
					<%-- <jsp:include page="/include/2012/myPageLeft.jsp" />
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb1","lnb14");
					</script> --%>
					<div class="con_lf">
					<div class="con_lf_big_title">법정허락<br> 승인 신청</div>
					<ul class="sub_lf_menu">
<!-- 						<li><a href="/mlsInfo/liceSrchInfo01.jsp">저작권자 검색 및 상당한 노력 신청</a>
							<ul class="sub_lf_menu2 disnone">
								<li><a href="/mlsInfo/liceSrchInfo01.jsp">소개 및 이용방법</a></li>
								<li><a href="/srchList.do">서비스 이용</a></li>
								<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
							</ul>
						</li> -->
						<li><a href="/mlsInfo/statInfo01.jsp" class="on">법정허락 승인 신청</a>
							<ul class="sub_lf_menu2">
								<li><a href="/mlsInfo/statInfo01.jsp">소개 및 이용방법</a></li>
								<li><a href="/stat/statSrch.do" class="on">서비스 이용</a></li>
								<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>
							</ul>
						</li>
					</ul>
				</div>
				</c:if>
				<c:if test="${isMyPage==''}">
					<div class="con_lf">
					<div class="con_lf_big_title">법정허락<br> 승인 신청</div>
					<ul class="sub_lf_menu">
<!-- 						<li><a href="/mlsInfo/liceSrchInfo01.jsp">저작권자 검색 및 상당한 노력 신청</a>
							<ul class="sub_lf_menu2 disnone">
								<li><a href="/mlsInfo/liceSrchInfo01.jsp">소개 및 이용방법</a></li>
								<li><a href="/srchList.do">서비스 이용</a></li>
								<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
							</ul>
						</li> -->
						<li><a href="/mlsInfo/statInfo01.jsp" class="on">법정허락 승인 신청</a>
							<ul class="sub_lf_menu2">
								<li><a href="/mlsInfo/statInfo01.jsp">소개 및 이용방법</a></li>
								<li><a href="/stat/statSrch.do" class="on">서비스 이용</a></li>
								<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>
							</ul>
						</li>
					</ul>
				</div>
				</c:if>
				<!-- //래프 -->
				<!-- <iframe id="goLogin" title="법정허락 공인인증서 설치 아이프레임"
					src="/include/sg_install.html" frameborder="0" width="242"
					height="139" scrolling="no" marginwidth="0" marginheight="0"
					style="display: none;"></iframe> -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					<c:if test="${isMyPage=='Y'}">
						<!-- <p class="path">
							<span>Home</span><span>마이페이지</span><span>신청현황</span><em>법정허락 이용승인신청</em>
						</p> -->
					</c:if>
					<c:if test="${isMyPage==''}">
						<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						
						&gt;
						법정허락 승인 신청
						&gt;
						<span class="bold">서비스 이용</span>
						</div>
						<div class="con_rt_hd_title">법정허락 승인 신청</div>
					</c:if>
					<!-- <h1><img src="/images/2012/title/content_h1_0305.gif" alt="법정허락 이용승인신청" title="법정허락 이용승인신청" /></h1> -->
					<!-- section -->
					<div class="section">
					<div class="sub01_con_bg4_tp mar_tp30"></div>
									<div class="sub01_con_bg4">
										<div class="font15"><span class="color_2c65aa">법정허락 신청에 관한 문의는 신청분류에 해당하는 단체로
												연락바랍니다.</span></div>
										<h3 class="mar_tp10">
											<span class="w20">- 한국저작권위원회</span> 
											<em class="w20 mar_tp10" style="margin-left: 30px;"><img src="/images/2012/common/ic_nm.gif" alt="" />심의조사팀</em> <em class="w25"><img src="/images/2012/common/ic_tel.gif" alt="" />&nbsp;055-792-0083</em>
									</h3>
									</div>
						<!-- memo 삽입 -->		
		                       <!--  <div class="gray_box">
		                            <div class="box4">
		                                <div class="box4_con floatDiv">
		                                    <p class="fl ml10"><img src="/images/2012/content/box_img1.gif" alt="" /></p>
		                                    <div class="fl ml20 w85">
		                                        <p class="strong mt5 black">법정허락<strong class="blue2"> 신청에 관한 문의는 아래의 단체로 연락바랍니다.</strong></p>
		                                        <ul class="mt15 list2">
		                                        <li>
		                                            <em class="w20">한국저작권위원회</em>
		                                            <span class="w20"><img src="/images/2012/common/ic_nm.gif" alt="" />심의조사팀</span>
		                                            <span class="w25"><img src="/images/2012/common/ic_tel.gif" alt="" />02-2660-0104</span>
		                                        </li>
		                                        </ul>
		                                    </div>
		                                </div>
		                            </div>
		                        </div> -->
						<!-- // memo 삽입 -->
                        
                        	<!-- process -->
		                        <div class="usr_process mt20">
						<div class="process_box">
							<ul class="floatDiv">
							<li class="fl ml0"><img alt="1단계 약관동의" src="/images/2012/content/process21_off.gif"></li>
							<li class="fl on"><img alt="2단계 회원정보입력" src="/images/2012/content/process22_on.gif"></li>
							<li class="fl bgNone pr0"><img alt="3단계 가입완료" src="/images/2012/content/process23_off.gif"></li>
							</ul>
						</div>
					</div>
					
		            <!-- // process -->
                    <form name="frm" action="#" enctype="multipart/form-data">
                    	<c:if test="${isMyPage == 'Y'}">
							<input type="hidden" name="srchApplyType" value="${srchParam.srchApplyType }"/>
							<input type="hidden" name="srchApplyFrDt" value="${srchParam.srchApplyFrDt }"/>
							<input type="hidden" name="srchApplyToDt" value="${srchParam.srchApplyToDt }"/>
							<input type="hidden" name="srchApplyWorksTitl" value="${srchParam.srchApplyWorksTitl }"/>
							<input type="hidden" name="srchStatCd" value="${srchParam.srchStatCd }"/>
							<input type="hidden" name="page_no" value="${srchParam.nowPage }"/>
						</c:if>
						
						<c:if test="${isMyPage == ''}">
							<input type="hidden" name="pageNo" value="${srchVO.pageNum}"/>
							<input type="hidden" name="searchCondition" value="${srchVO.searchCondition}"/>
							<input type="hidden" name="searchKeyword1" value="${srchVO.searchKeyword1}"/>
						</c:if>
						<input type="hidden" name="action_div" value=""/>
						<input type="hidden" name="applyWriteYmd" value="${srchParam.applyWriteYmd }">
						<input type="hidden" name="applyWriteSeq" value="${srchParam.applyWriteSeq }">
						<input type="hidden" name="apply_write_ymd" value="${srchParam.applyWriteYmd }">
						<input type="hidden" name="apply_write_seq" value="${srchParam.applyWriteSeq }">
						<input type="hidden" name="applyWorksCnt" value="${applyWorksCnt }">
						<input type="hidden" name="rgstIdnt" value="${userInfo.USER_IDNT }"/>
						<input type="hidden" name="stat_cd" value="${statCd}"/>
						<input type="hidden" name="isModi"/>
				    	<input type="hidden" name="existYn" value="${existYn}"/>
				    	<input type="hidden" name="message" value="${message}" />
						<input type="hidden" name="isMy" value="${isMy}"/>
						<input type="hidden" name="stat_cd"/>
						<input type="submit" style="display:none;">
						
							<%-- <c:if test="${existYn =='Y'}">
							<div class="article">
								<h2>이용 승인신청 명세서 일괄작성</h2>
								<span class="topLine"></span>
								<table class="grid" border="1" cellspacing="0" cellpadding="0"
									summary="권리자찾기 신청정보입니다.">
									<colgroup>
										<col width="35%">
										<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">이용 승인신청 명세서 엑셀파일</th>
											<td>
												<c:if test="${not empty fileInfo}">
												<c:if test="${fileInfo.fileNameCd == '1'}">
													<input name="attcSeqn" id="attcSeqn_D1" value="${fileInfo.attcSeqn }" type="hidden" />
										    		<input name="fileNameCd" id="fileNameCd_D1" value="1" type="hidden" style="border:0px;" readonly="readonly"  />
										    		<input type="hidden" name="existYn" value="${existYn }"/>
													<input type="hidden" name="fileDelYn" id="fileDelYn_D1" />
													<span id="spfileD1'">
													    <input type="file" name="fileD1" id="file_D1" title="첨부파일" 
													        class="inputData L w70"
													        onkeydown="return false;" onchange="fn_chkFileType(this,'D1');"/>
													</span>
													<span class="button small" id="uploadBtn"><a href="#1">일괄작성</a></span>
													<input name="hddnFile" id="hddnFile_D1" type="hidden" />
													<input name="fileName" id="fileName_D1" type="hidden" title="파일명" 
													    onkeyup="onkeylengthMax(this, 255, this.name);" class="inputDate w95" />
													<input name="realFileName" id="realFileName_D1" type="hidden" style="display:none;" />
													<input name="fileSize" id="fileSize_D1" type="hidden" style="display:none;" />
													<input name="filePath" id="filePath_D1" type="hidden" style="display:none;" />
												</c:if>
												</c:if>
											</td>
										</tr>
										<tr>
											<th scope="row">이용 승인신청 명세서 엑셀샘플파일</th>
											<td><a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form' />','이용승인신청명세서.xlsx','이용승인신청명세서.xlsx')" class="underline mr5">이용 승인신청 명세서 샘플.xlsx</a>
											<span class="button small">
													<a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form' />','이용승인신청명세서.xlsx','이용승인신청명세서.xlsx')">
													다운로드</a>
											</span>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="comment_box">
									<strong class="line15 black"><img class="vmid"
										src="/images/2012/common/ic_tip.gif" alt=""> 이용 승인신청 명세서
										일괄작성 안내</strong>
									<ul class="commentList">
										<li>이용 승인신청 명세서 엑셀샘플파일을 다운로드 받는다.</li>
										<li>샘플파일에 작성된 서식에 맞게 데이터를 입력 저장한다.</li>
										<li>이용 승인신청 명세서 엑셀파일[찾아보기]에서 등록을 원하는 엑셀파일을 선택 한 후
											[일괄작성]버튼을 선택하여 일괄작성 처리한다.</li>
										<li>작성된 ‘이용 승인신청 명세서 정보’를 확인 하고 오류건이 없는 경우 다음단계로 진행한다.</li>
									</ul>
								</div>
							</div>
							<div class="blue_box floatDiv pd5">
								<p class="fl p12 black">
									<strong>일괄작성 결과</strong> (신청 건수 : ${applyWorksCnt}건, 작성 건수 : ${writeRow}건, 오류 건수 : ${errRow}건 )
								</p>
								<span class="fr"><strong class="chk_img"></strong>입력 서식오류
									항목표시</span>
							</div>
							<div class="article">
								<h2>
									이용 승인신청 명세서 정보 
									<!-- <select id="sc1" class="ml10">
										<option>1</option>
									</select> <span class="button small black thin"><a href="">이동</a> -->
									</span>
								</h2>
								<span class="topLine"></span>
								<!-- 그리드스타일 -->
								<div class="pageme">
								<c:if test="${not empty statApplyWorksList}">
									<c:set var="tblSeq" value="0"/>
									<c:set var="idx" value="0"/>
									<c:forEach items="${statApplyWorksList}" var="list">
									<table cellspacing="0" cellpadding="0" border="1" class="grid"
										summary="">
										<colgroup>
											<col width="6%">
											<col width="15%">
											<col width="20%">
											<col width="20%">
											<col width="20%">
											<col width="*">
										</colgroup>
										<tbody>
											<c:set var="err00" value=""/>
											<c:set var="err01" value=""/>
											<c:set var="err02" value=""/>
											<c:set var="err03" value=""/>
											<c:set var="err04" value=""/>
											<c:set var="err05" value=""/>
											<c:set var="err06" value=""/>
											<c:set var="err07" value=""/>
											<c:set var="err08" value=""/>
											<c:set var="err09" value=""/>
											<c:set var="err10" value=""/>
											<c:set var="err11" value=""/>
											<c:set var="err12" value=""/>
											<!-- 1 -->
											<c:forTokens var="errMsg" items="${list.errMsg}" delims="@">
												<c:if test="${errMsg == 'A1'}">
													<c:set var="err00" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A2'}">
													<c:set var="err01" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A3'}">
													<c:set var="err02" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A4'}">
													<c:set var="err03" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A5'}">
													<c:set var="err04" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A6'}">
													<c:set var="err05" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A7'}">
													<c:set var="err06" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A8'}">
													<c:set var="err07" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A9'}">
													<c:set var="err08" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A10'}">
													<c:set var="err09" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A11'}">
													<c:set var="err10" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A12'}">
													<c:set var="err11" value="${errMsg}"/>
												</c:if>
												<c:if test="${errMsg == 'A13'}">
													<c:set var="err12" value="${errMsg}"/>
												</c:if>
											</c:forTokens>
											<c:set var="tblSeq" value="${tblSeq + 1}"/>
											<tr>
												<td class="ce" rowspan="7">${tblSeq }</td>
												<th scope="row" rowspan="2">
												<c:forEach items="${applyTypeList}" var="applyTypeList">
													<c:if test="${applyTypeList.code==list.applyType}">${applyTypeList.codeName }</c:if>
												</c:forEach>
												</br>
												${err00}
												${err01}
												${err02}
												${err03}
												${err04}
												${err05}
												${err06}
												${err07}
												${err08}
												${err09}
												${err10}
												${err11}
												${err12}
												</th>
												<td class="tbBg">제호(제목)</td>
												<td colspan="3" <c:if test="${err01 == 'A2'}">class="chk_img"</c:if>>
													${list.worksTitl }
												</td>
											</tr>
											<tr>
												<td class="tbBg">종류</td>
												<td <c:if test="${err02 == 'A3'}">class="chk_img"</c:if>>${list.worksKind }</td>
												<td class="tbBg">형태 및 수량</td>
												<td <c:if test="${err03 == 'A4'}">class="chk_img"</c:if>>${list.worksForm }</td>
											</tr>
											<tr>
												<th scope="row" rowspan="2">공표</th>
												<td class="tbBg">공표 연월일</td>
												<td <c:if test="${err05 == 'A6'}">class="chk_img"</c:if>>${list.publYmd }</td>
												<td class="tbBg">공표국가</td>
												<td <c:if test="${err06 == 'A7'}">class="chk_img"</c:if>>${list.publNatn }</td>
											</tr>
											<tr>
												<td class="tbBg">공표방법</td>
												<td <c:if test="${err07 == 'A8'}">class="chk_img"</c:if>>
												<c:forEach items="${publMediList}" var="publMediList">
													<c:if test="${publMediList.code==list.publMediCd}">${publMediList.codeName }</c:if>
												</c:forEach>
												<span style="display:<c:if test="${list.publMediCd != '99'}">none</c:if>;" >(${list.publMediEtc })</span>
												</td>
												<td class="tbBg">공표매체정보</td>
												<td <c:if test="${err09 == 'A10'}">class="chk_img"</c:if>>
												${list.publMedi }
											</td>
											</tr>
											<tr>
												<th scope="row" rowspan="2">권리자</th>
												<td class="tbBg">성명(법인명)</td>
												<td <c:if test="${err10 == 'A11'}">class="chk_img"</c:if>>${list.coptHodrName }</td>
												<td class="tbBg">전화번호</td>
												<td <c:if test="${err11 == 'A12'}">class="chk_img"</c:if>>${list.coptHodrTelxNumb }</td>
											</tr>
											<tr>
												<td class="tbBg">주소</td>
												<td colspan="3" <c:if test="${err12 == 'A13'}">class="chk_img"</c:if>>${list.coptHodrAddr }</td>
											</tr>
											<tr>
												<th scope="row">신청물의 내용</th>
												<td colspan="4" <c:if test="${err04 == 'A5'}">class="chk_img""</c:if>>
													${list.worksDesc}
												</td>
											</tr>
											<!-- //1 -->
											<c:set var="idx" value="${idx+1 }"/>
										</tbody>
									</table>
									</c:forEach>
								</c:if>
								</div>
								<!-- //그리드스타일 -->
							</div>
							<div class="pager">
							</div>
							</c:if> --%>
							<%-- <c:if test="${existYn =='' ||existYn =='N' }"> --%>
								<c:set var="dySeq" value="0" />
								<c:forEach items="${applyTypeList}" var="applyTypeList">
									<c:set var="dySeq" value="${dySeq + 1}" />
									<input type="hidden" id="applyType0${dySeq }"
										name="applyType0${dySeq }" value="">
								</c:forEach>
								<input type="hidden" id="publMediCd" name="publMediCd" value="">
								<div class="article">
								<div class="floatDiv">
									<h2 class="fl">이용 승인신청 명세서 정보 </h2>
									<p class="fr">
										<span class="button small"><a href="#1" onclick="boxInit();" onkeypress="boxInit();">항목 초기화</a></span>
										<span class="button small icon"><a href="#1" onclick="openSmplDetail('MS11');" onkeypress="openSmplDetail('MS11')">예시화면 보기</a><span class="help"></span></span>
									</p>
								</div>
								<span class="topLine"></span>
								<!-- 그리드스타일 -->
								<div>
							
									<c:if test="${not empty statApplyWorksList}">
									
									<c:set var="tblSeq" value="0"/>
									<c:set var="idx" value="0"/>
									<c:forEach items="${statApplyWorksList}" var="list">
									<table cellspacing="0" cellpadding="0" border="1" class="grid"
										summary="">
										<colgroup>
											<col width="6%">
											<col width="15%">
											<col width="20%">
											<col width="20%">
											<col width="20%">
											<col width="*">
										</colgroup>
										<tbody>
											<c:set var="tblSeq" value="${tblSeq + 1}"/>
											<tr>
												<td class="ce" rowspan="8">${tblSeq }</td>
												<th scope="row" rowspan="2">
												<c:set var="dySeq" value="0"/>
													<%-- <c:if test="${list.APPLYTYPE01 == 1}">
														<c:forEach items="${applyTypeList}" var="applyTypeList">
															<c:if test="${applyTypeList.code==1}">${applyTypeList.codeName }</c:if>
														</c:forEach>
													</c:if>
													<c:if test="${list.APPLYTYPE02 == 1}">
														<c:forEach items="${applyTypeList}" var="applyTypeList">
															<c:if test="${applyTypeList.code==2}">${applyTypeList.codeName }</c:if>
														</c:forEach>
													</c:if>
													<c:if test="${list.APPLYTYPE03 == 1}">
														<c:forEach items="${applyTypeList}" var="applyTypeList">
															<c:if test="${applyTypeList.code==3}">${applyTypeList.codeName }</c:if>
														</c:forEach>
													</c:if>
													<c:if test="${list.APPLYTYPE04 == 1}">
														<c:forEach items="${applyTypeList}" var="applyTypeList">
															<c:if test="${applyTypeList.code==4}">${applyTypeList.codeName }</c:if>
														</c:forEach>
													</c:if>
													<c:if test="${list.APPLYTYPE05 == 1}">
														<c:forEach items="${applyTypeList}" var="applyTypeList">
															<c:if test="${applyTypeList.code==5}">${applyTypeList.codeName }</c:if>
														</c:forEach>
													</c:if> --%>
													<c:forEach items="${applyTypeList}" var="applyTypeList">
														<c:set var="dySeq" value="${dySeq + 1}"/>
														<input type="checkbox" class="mb5" id="dummyApplyType0${dySeq }_D${idx }" name="dummyApplyType0${dySeq }" value="1" 
															onclick="fn_chkApplyType(this, 'D${idx }');" onkeypress="fn_chkApplyType(this, 'D${idx }');" >
														<label for="dummyApplyType0${dySeq }_D${idx }" >${applyTypeList.codeName }</label></br>
													</c:forEach>
												</th>
												<td class="tbBg"><label class="necessary" for="worksTitlD${idx }">제호(제목)</label></td>
												<td colspan="3">
													<input type="text" name="worksTitl" id="worksTitlD${idx }" value="${list.WORKSTITL }"  class="w98" title="제목(제호)" rangeSize="0~500" nullCheck>
												</td>
											</tr>
											<tr>
												<td class="tbBg"><label class="necessary" for="worksKindD${idx }">종류</label></td>
												<td><input type="text" name="worksKind" id="worksKindD${idx }" 
																	value="${list.WORKSKIND }" 
																	class="w98" title="종류" rangeSize="0~200" nullCheck></td>
												<td class="tbBg"><label class="necessary" for="worksFormD${idx }">형태 및 수량</label></td>
												<td><input type="text" name="worksForm" id="worksFormD${idx }" 
																	value="${list.WORKSFORM }" 
																	class="w98" title="형태 및 수량" rangeSize="0~200" nullCheck></td>
											</tr>
											<tr>
												<th scope="row" rowspan="3">공표</th>
												<td class="tbBg"><label class="necessary" for="publYmdD${idx }">공표 연월일</label></td>
												<td>
													<input type="text" name="publYmd" id="publYmdD${idx }" value="${list.PUBLYMD }" 
													size="12" title="공표연월일" readonly="readonly" character="EK" maxlength="8" dateCheck nullCheck>
													<img title="공표연월일을 선택하세요." alt="" class="vmid" align="middle" style="cursor:pointer"; onclick="javascript:fn_calObj('frm','publYmd',this);" src="/images/2011/common/calendar.gif" >
												</td>
												<td class="tbBg"><label class="necessary" for="publNatnD${idx }">공표국가</label></td>
												<td><input type="text" name="publNatn" id="publNatnD${idx }" 
																	value="${list.PUBLNATN }" 
																	class="w98" title="공표국가" rangeSize="0~100" nullCheck></td>
											</tr>
											<tr>
												<td class="tbBg"><label class="necessary" for="">공표방법</label></td>
												<td colspan="3">
													<c:set var="dySeq" value="0" /> <c:forEach
																	items="${publMediList}" var="publMediList">
																	<c:set var="dySeq" value="${dySeq + 1}" />
																	<c:set var="widthValue" value="70"/>
																	<%-- <c:set var="widthValue" value="105"/> --%>
																	<c:if test="${dySeq==3 || dySeq==6}">
																		<c:set var="widthValue" value="115"/>
																	</c:if>
																	<c:if test="${dySeq==7}">
																		<c:set var="widthValue" value="50"/>
																	</c:if>
																	<div style="width: ${widthValue}px; float:left;"><input type="checkbox" name="dummyPublMediCdD${idx }"
																		id="dummyPublMediCd_${publMediList.code }_D${idx }"
																		value="${publMediList.code }"
																		<c:if test="${list.PUBLMEDICD == publMediList.code}">checked="checked"</c:if>
																		onclick="fn_chkPublMediCd(this, 'D${idx }');"
																		onkeypress="fn_chkPublMediCd(this, 'D${idx }');">
																	<label for="dummyPublMediCd_${publMediList.code }_D${idx }">${publMediList.codeName }</label></div>
																	<c:if test="${dySeq==3}">
																	</br>
																	</c:if>
																	<c:set var="widthValue" value="50"/>
																</c:forEach> <input type="text" name="publMediEtc"
																id="publMediEtcD${idx }" size="12"
																value="${list.PUBLMEDIETC}"
																style="height:14px; display:<c:if test="${list.PUBLMEDICD != '99'}">none</c:if>;">
															</td>
											</tr>
											<tr>
												<td class="tbBg"><label class="necessary" for="publMediD${idx }">공표매체정보</label></td>
												<td colspan="3">
													<input type="text" name="publMedi" id="publMediD${idx }" 
																	value="${list.PUBLMEDI }" 
																	class="w98" title="공표매체정보" rangeSize="0~200" nullCheck>
												</td>
											</tr>
											<tr>
												<th scope="row" rowspan="2">권리자</th>
												<td class="tbBg"><label class="necessary" for="coptHodrNameD${idx }">성명(법인명)</label></td>
												<td>
													<input type="text" name="coptHodrName" id="coptHodrNameD${idx }" 
																	value="${list.COPTHODRNAME }" 
																	class="w98" title="성명(법인명)" rangeSize="0~200" nullCheck>
												</td>
												<td class="tbBg"><label class="necessary" for="coptHodrTelxNumbD${idx }">전화번호</label></td>
												<td><input type="text" name="coptHodrTelxNumb" id="coptHodrTelxNumbD${idx }" 
																	value="${list.COPTHODRTELXNUMB }" 
																	class="w98" title="전화번호" rangeSize="0~20" nullCheck></td>
											</tr>
											<tr>
												<td class="tbBg"><label class="necessary" for="coptHodrAddrD${idx }">주소</label></td>
												<td colspan="3"><input type="text" name="coptHodrAddr" id="coptHodrAddrD${idx }" 
																	value="${list.COPTHODRADDR }" 
																	class="w98" title="주소" rangeSize="0~200" nullCheck></td>
											</tr>
											<tr>
												<th scope="row" class="tbBg"><label class="necessary" for="worksDescD${idx }">신청물의 내용</label></th>
												<td colspan="4">
													<textarea cols="10" name="worksDesc" id="worksDescD${idx }" class="w99" rows="2" title="신청물의 내용" rangeSize="0~4000" nullCheck>${list.WORKSDESC}</textarea>
												</td>
											</tr>
											<c:set var="idx" value="${idx+1 }"/>
											<!-- //1 -->
										</tbody>
									</table>
									</c:forEach>
									</c:if>
								</div>
								<!-- //그리드스타일 -->
							</div>
							<%-- </c:if> --%>
						</form>    
                           <!-- 버튼ㅅㄷㄴㅅ영역 -->
                           <div class="btnArea">
                               <p class="fl">
                               	<span class="button medium gray"><a href="#1" <c:if test="${isMyPage=='Y'}" > onclick="fn_goMyList();" onkeypress="fn_goMyList();" </c:if><c:if test="${isMyPage==''}" > onclick="fn_goList();" onkeypress="fn_goList();" </c:if>>취소</a></span>
                                   <span class="button medium gray"><a href="#1" onclick="fn_goStep1();" onkeypress="fn_goStep1();">이전단계</a></span>
                               </p>
                               <p class="fr">
                                   <span class="button medium"><a onclick="fn_doSave('goList');" onkeypress="fn_doSave('goList');" href="#1">임시저장</a></span>
                                   <span class="button medium"><a onclick="fn_doSave('goStep3');" onkeypress="fn_doSave('goStep3');" href="#1">작성내용확인</a></span>
                               </p>
                           </div>
                          <!-- //버튼영역 -->                     			
					</div>
					<!-- //section -->				
				</div>
	            	<!-- //주요컨텐츠 end -->
	            	
					<p class="clear"></p>
			</div>
			<!-- //content -->
			<!-- FOOTER str-->
			<!-- 2017변경 -->
			
			<jsp:include page="/include/2017/footer.jsp" />
	
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
			<!-- //FOOTER end -->
		</div>
		
		<!-- //CONTAINER -->
	</div>
	<!-- //전체를 감싸는22 DIVISION -->
		
<script type="text/JavaScript">
<!--
<c:if test="${existYn =='' ||existYn =='N' }">
	window.onload = function() {
						<c:if test="${not empty statApplyWorksList}">
							//신청서 구분에 값 맵핑
		    				<c:set var="dySeq" value="0"/>
			    			<c:forEach items="${statApplyWorksList}" var="statApplyWorksList">
			    				<c:if test="${statApplyWorksList.APPLYTYPE01 == '1'}">
			    					document.getElementById("dummyApplyType01_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE02 == '1'}">
			    					document.getElementById("dummyApplyType02_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE03 == '1'}">
			    					document.getElementById("dummyApplyType03_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE04 == '1'}">
			    					document.getElementById("dummyApplyType04_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:if test="${statApplyWorksList.APPLYTYPE05 == '1'}">
			    					document.getElementById("dummyApplyType05_D${dySeq}").checked = true;
			    				</c:if>
			    				<c:set var="dySeq" value="${dySeq + 1}"/>
			    			</c:forEach>
						</c:if>
					}
	</c:if>
-->
</script>
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>
