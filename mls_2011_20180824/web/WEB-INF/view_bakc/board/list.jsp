<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List"%>
<%
    String name ="Expression Language";
    String[] color = {"red","blue","green"};
    java.util.Map map = new java.util.HashMap();
    map.put("name1","red");
    map.put("name2","blue");
    map.put("name3","green");
    
    request.setAttribute("name", name);
    request.setAttribute("color", color);
    request.setAttribute("map", map);
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>Test page</title>
</head>
<body>
<h4>${name}</h4>
<h4>${color[1]}</h4>
<h4>${map["name1"]}</h4>

<!-- 파라미터 값을 읽어오는 JSP 제공 내장객체 -->
<h4>${param.name}  aaa</h4>
<table>
	<c:forEach items="${boardList.resultList}" var="board">
	<c:set var="i" value="${i+1}"/>
	<tr>
		<td>
			<c:out value="${i}"/>
		</td>
		<td>
			<c:out value="${board.empno}"/>
		</td>
		<td>
			<c:out value="${board.ename}"/>
		</td>
		<td>
			${board.job}
		</td>
		<td>
			${board.mgr}
		</td>
	</tr>
	</c:forEach>
</table>
</body>
</html>