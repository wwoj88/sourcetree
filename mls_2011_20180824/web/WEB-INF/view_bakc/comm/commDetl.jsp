<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%
	String bordSeqn = request.getParameter("bordSeqn");
	String menuSeqn = request.getParameter("menuSeqn");
	String threaded = request.getParameter("threaded");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");

	Board boardDTO = (Board) request.getAttribute("board");
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작권찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
<script src="/js/Function.js" type="text/javascript"></script>
<script type="text/JavaScript">
<!--
  window.name = "boardView";

  function fn_commList(){
		var frm = document.form1;
		frm.action = "/board/board.do";
		frm.submit();
	}

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.target="boardView";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }

  function fn_update(){

	  var frm =document.form1;

		if (frm.pswd.value == "") {
			alert("비밀번호를 입력하십시오.");
			frm.pswd.focus();
			return;
		} else if(isPwdValidate()){
			frm.method = "post";
			frm.action = "/board/board.do?method=boardView";
			frm.submit();
		}
	}

  function fn_delete(){

	  var frm =document.form1;

		if (frm.pswd.value == "") {
			alert("비밀번호를 입력하십시오.");
			frm.pswd.focus();
			return;
		} else if(isPwdValidate()){
			if(confirm("답변내용도 같이 삭제됩니다\n내용을 삭제 하시겠습니까?")){
			  frm.method = "post";
			  frm.action = "/board/board.do?method=deleteComm";
			  frm.submit();
			}
		}
	}

	// 비밀번호 체크
	function isPwdValidate(){
		var bordSeqn = document.form1.bordSeqn.value;
		var menuSeqn = document.form1.menuSeqn.value;
		var pswd     = document.form1.pswd.value;
		var threaded = document.form1.threaded.value;
		var url = "/board/board.do?method=isValidPwd&bordSeqn=" + bordSeqn + "&menuSeqn=" + menuSeqn + "&pswd=" + pswd + "&threaded=" + threaded;

	  if(cfGetBooleanResponseReload(url)){
		    return true;
		} else {
			alert("비밀번호 오류입니다.");
			document.form1.pswd.value = "";
			document.form1.pswd.focus();
		  return false;
		}
  }
//-->
</script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp" flush="true">
		<jsp:param name="mNum" value="5" />
	</jsp:include>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
    	  GetFlash('/images/swf/subVisual.swf','725','122');
      </script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="qustTlt">커뮤니티 목록</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<li class="none"><img src="/images/common/home_ico.gif" alt="Home" />Home</li>
					<li>커뮤니티</li>
					<li class="on">커뮤니티 게시판</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>커뮤니티 게시판 상세조회</h2>
			<table width="710" class="board ViewTop">
				<form name="form1" method="post">
					<input type="submit" style="display:none;">
					<input type="hidden" name="bordSeqn" value="<%=bordSeqn%>">
					<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>">
					<input type="hidden" name="threaded" value="<%=threaded%>">
					<input type="hidden" name="srchDivs" value="<%=srchDivs%>">
					<input type="hidden" name="srchText" value="<%=srchText%>">
					<input type="hidden" name="page_no" value="<%=page_no%>">
					<input type="hidden" name="submitType" value="update">
					<input type="hidden" name="deth" value="<%=boardDTO.getDeth()%>">
					<input type="hidden" name="filePath">
					<input type="hidden" name="fileName">
					<input type="hidden" name="realFileName">
				<colgroup>
				<col width="80" />
				<col width="" />
				</colgroup>
				<caption summary="Q&A내용을 상세조회 합니다.">Q&amp;A 내용 상세화면</caption>
				<thead>
					<tr>
						<th>제목</th>
						<td class="tlt"><%=boardDTO.getTite()%></td>
					</tr>
				</thead>
				<tbody class="ViewBody">
					<tr>
						<th>이름</th>
						<td><%=boardDTO.getRgstIdnt()%></td>
					</tr>
					<tr>
						<th>이메일</th>
						<td><%=boardDTO.getMail() == null ? "" : boardDTO.getMail()%></td>
					</tr>
					<tr>
						<th> <label for="pw">비밀번호</label> </th>
						<td><input type="password" title="비밀번호" id="pswd" name="pswd" maxlength="10" class="input" /></td>
					</tr>
					<tr>
						<th>내용</th>
						<td class="tdContents"><%=CommonUtil.replaceBr(boardDTO.getBordDesc(),true)%></td>
					</tr>
<%
          	List fileList = (List) boardDTO.getFileList();
          	int listSize = fileList.size();

          	if (listSize > 0) {
%>
					<tr>
						<th>첨부파일</th>
						<td><ul>
<%

          	  for(int i=0; i<listSize; i++) {
          	    Board fileDTO = (Board) fileList.get(i);
%>
	            <li><a href="#1" onclick="javascript:fn_fileDownLoad('<%=fileDTO.getFilePath()%>','<%=fileDTO.getFileName()%>','<%=fileDTO.getRealFileName()%>')"><%=fileDTO.getFileName()%></a></li>
<%
	            }
%>
						</ul></td>
					</tr>
<%
	          }
%>
					</tr>
				</tbody>
			  </form>
			</table>
			<!--buttonArea start-->
			<div id="buttonArea">
			  <div class="floatL"><a href="#1" onclick="javascript:fn_commList();"><img src="/images/button/list_btn.gif" alt="목록보기" /></a></div>
				<div class="floatR"><!--a href="#"><img src="/images/button/answ_btn.gif" alt="답변하기" width="66" height="21" /></a-->
					                  <a href="#1" onclick="javascript:fn_update();"><img src="/images/button/modi_btn.gif" alt="수정하기" width="66" height="21" /></a>
					                  <a href="#1" onclick="javascript:fn_delete();"><img src="/images/button/delt_btn.gif" alt="삭제하기" width="66" height="21" /></a>
					                  <!--a href="javascript:document.form1.reset();"><img src="/images/button/cancle_btn.gif" alt="취소하기" width="66" height="21" /></a--></div>
			</div>
			<!--buttonArea end-->
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
