<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.main.model.Main"%>
<%@ page import="java.util.List"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.TimeZone" %>
<%
   Main mainDTO = (Main) request.getAttribute("main");

   List qustList = (List) mainDTO.getQnaList();
   int qustListSize = qustList.size();

   List notiList = (List) mainDTO.getNotiList();
   int notiListSize = notiList.size();

   List workList = (List) mainDTO.getWorkList();
   int workListSize = workList.size();
   
   List inmtList = (List) mainDTO.getInmtList();
   int inmtListSize = inmtList.size();
   
   List lawList = (List) mainDTO.getLawList();
   int lawListSize = lawList.size();
   
   List workNoneList = (List) mainDTO.getWorkNoneList();
   int workNoneListSize = workNoneList.size();
   
   List promPotoList = (List) mainDTO.getPromPotoList();
   int promPotoListSize = promPotoList.size();
   
   String alltInmp = mainDTO.getInmt();
   String alltInmp202 = mainDTO.getInmt202();
   String alltInmp203 = mainDTO.getInmt203();
   String alltInmpL = mainDTO.getInmtL();
   String alltInmpS = mainDTO.getInmtS();
   
   //List promMovieList = (List) mainDTO.getPromMovieList();
   //int promMovieListSize = promMovieList.size();
   
 	//오늘 날짜
	String sNOWDATE = new SimpleDateFormat("yyyy-mm-dd").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+09:00")).getTime());
 
   User user = SessionUtil.getSession(request);
   String sessUserIdnt = user.getUserIdnt();
   String loginYn = "N";
   if (sessUserIdnt != null) 
 	  loginYn = "Y";
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript">
<!--
//탭메뉴
function showtab(n) {

//	alert(n);
  for(var i = 1; i < 4; i++) {
    obj = document.getElementById('showtab'+i);  // 보여질 리스트
    //img = document.getElementById('showtab_button'+i); // 버튼 이미지
    
    if(n == 1){
    	document.getElementById('bgn1').className = 'selected';
    	document.getElementById('bgn2').className = '';
    	document.getElementById('bgn3').className = '';
    	
    	document.getElementById('dis1').className = 'noti';
    	document.getElementById('dis2').className = 'law disNone';
    	document.getElementById('dis3').className = 'faq disNone';
    }else if(n == 2){
    
    	document.getElementById('bgn1').className = '';
    	document.getElementById('bgn2').className = 'selected';
    	document.getElementById('bgn3').className = '';
    	
    	document.getElementById('dis1').className = 'noti disNone';
    	document.getElementById('dis2').className = 'law ';
    	document.getElementById('dis3').className = 'faq disNone';
    }else if(n == 3){
    	document.getElementById('bgn1').className = '';
    	document.getElementById('bgn2').className = '';
    	document.getElementById('bgn3').className = 'selected';
    	
    	document.getElementById('dis1').className = 'noti disNone';
    	document.getElementById('dis2').className = 'law disNone';
    	document.getElementById('dis3').className = 'faq ';
    }    
    /*
    if ( n == i ) {
      obj.style.display = "block";
      //img.src = "/images/2010/main/txt_notice.gif";
    } else {
      obj.style.display = "none";
      //img.src = "/images/2010/main/txt_faq.gif";
    }*/
  }
}

//탭메뉴
function ceShowtab(n) {

 // for(var i = 1; i < 4; i++) {
    // obj = document.getElementById('showtab'+i);  // 보여질 리스트
    //img = document.getElementById('showtab_button'+i); // 버튼 이미지
  //  alert(n);
  
      	document.getElementById('ceMore1').className = 'more';
    	document.getElementById('ceMore2').className = 'more none';
    	document.getElementById('ceMore3').className = 'more none';
  
    if(n == 1){
    	document.getElementById('ceMore1').className = 'more';
    	document.getElementById('ceMore2').className = 'more disNone2';
    	document.getElementById('ceMore3').className = 'more disNone2';

    	document.getElementById('ceTab1').className = 'select';
    	document.getElementById('ceTab2').className = '';
    	document.getElementById('ceTab3').className = '';
    	
    	document.getElementById('ceDiv1').className = 'mainGridRound';
    	document.getElementById('ceDiv2').className = 'mainGridRound disNone';
    	document.getElementById('ceDiv3').className = 'mainGridRound disNone';
    	
    }
    if(n == 2){

    	document.getElementById('ceMore1').className = 'more disNone2';
    	document.getElementById('ceMore2').className = 'more';
    	document.getElementById('ceMore3').className = 'more disNone2';

    	document.getElementById('ceTab1').className = '';
    	document.getElementById('ceTab2').className = 'select';
    	document.getElementById('ceTab3').className = '';
    	
    	document.getElementById('ceDiv1').className = 'mainGridRound disNone';
    	document.getElementById('ceDiv2').className = 'mainGridRound';
    	document.getElementById('ceDiv3').className = 'mainGridRound disNone';
    	
    }
    if(n == 3){
    	document.getElementById('ceMore1').className = 'more disNone2';
    	document.getElementById('ceMore2').className = 'more disNone2';
    	document.getElementById('ceMore3').className = 'more';
    	
    	document.getElementById('ceTab1').className = '';
    	document.getElementById('ceTab2').className = '';
    	document.getElementById('ceTab3').className = 'select';
    	
    	document.getElementById('ceDiv1').className = 'mainGridRound disNone';
    	document.getElementById('ceDiv2').className = 'mainGridRound disNone';
    	document.getElementById('ceDiv3').className = 'mainGridRound';
    }    

 // }
}

/*
function ceShowtab(n) {
	  for(var i = 1; i < 4; i++) {
	    obj = document.getElementById('ceShowtab'+i);  // 보여질 리스트

	    if ( n == i ) {
	      obj.style.display = "block";
	    } else {
	      obj.style.display = "none";
	    }
	  }
	
	}
*/

function promtab(n) {
	var frm = document.form1;
	  for(var i = 1; i < 3; i++) {
	    obj = document.getElementById('promtab1');  // 보여질 리스트
	    img = document.getElementById('promtab_button1'); // 버튼 이미지

	    obj2 = document.getElementById('promtab2');  // 보여질 리스트
	    img2 = document.getElementById('promtab_button2'); // 버튼 이미지
	    
	    if ( n == i ) {
	        obj.style.display = "none";
	        obj2.style.display = "block";
	        img.src = "/images/2010/main/publicity_photo.gif";
	        img2.src = "/images/2010/main/publicity_movie_over.gif";
	        frm.leftsub.value = "2";
	      } else {
	        obj.style.display = "block";
	        obj2.style.display = "none";
	        img.src = "/images/2010/main/publicity_photo_over.gif";
	        img2.src = "/images/2010/main/publicity_movie.gif";
	        frm.leftsub.value = "1";
	      }
	  }
	}

function promBoardList(){
	var frm = document.form1;

	var leftsub = frm.leftsub.value;

	frm.mNum.value = '5';
	frm.sNum.value = '0';
	frm.leftsub.value = leftsub;
	frm.menuSeqn.value = '5';
	frm.page_no.value = '1';
	frm.method = "post";
	frm.action = "/board/board.do";
	frm.submit();
}

function boardDetail(bordSeqn,menuSeqn,threaded){
	var frm = document.form1;
	frm.bordSeqn.value = bordSeqn;
	frm.menuSeqn.value = menuSeqn;
	frm.threaded.value = threaded;
	frm.page_no.value = '1';
	frm.method = "post";
	frm.action = "/board/board.do?method=boardView";
	frm.submit();
}

function promBoardDetail(bordSeqn,menuSeqn,threaded,leftsub){
	var frm = document.form1;
	frm.bordSeqn.value = bordSeqn;
	frm.menuSeqn.value = menuSeqn;
	frm.threaded.value = threaded;
	frm.page_no.value = '1';
	frm.leftsub.value = leftsub;
	frm.method = "post";
	frm.action = "/board/board.do?method=boardView";
	frm.submit();
}

function fn_certInfoOpen() {
//	window.open('/main/main.do?method=goCertPage','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=800, height=550');
	window.open('http://copyright.signra.com/','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=780, height=550');

}

// 통합검색
var clmsPop = '';
function goSearch() {
	
		var frm = document.srchForm;
		
		if(document.getElementById("srchWord").value == ""){
			alert("검색어를 입력하세요.");
			document.getElementById("srchWord").focus();
			return;
		}
		
		document.getElementById("srchWord").blur();
		 
		srchWord = document.getElementById("srchWord").value;
		
		oSelect = document.getElementById("cate").value;
		
		// 2009 기존url
		//window.open('http://www.clms.or.kr/user/search/mls/redirect.do?topMenuId=01&leftMenuId=01&target='+oSelect+'&query='+srchWord, 'win2','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, resizable=false, width=810, height=700');
		
		// 2010 newUrl
		//window.open('http://www.clms.or.kr/user/search/right4me/redirect.do?topMenuId=01&leftMenuId=01&target='+oSelect+'&query='+srchWord, 'right4me','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, resizable=false, width=810, height=700');
		
		// 통합검색(clms.or.kr)에서 권리찾기사이트(right4me.or.kr)로 접근가능해야 하므로 right4me 내 팝업을 띄우고 clms통합검색화면으로 접근한다.
		/*
		----- 검색 ------
		 1. 현재화면 		open(right4me/search/pop_a.jsp) 
		 2. pop_a.jsp 		open(clms/통합검색.jsp)
		 3. 통합검색.jsp 	location.href="rightrme/pop_c.jsp"
		 ----- 검색결과 ------
		 
		 ----- 권리찾기신청화면 이동 ------
		 4. pop_c.jsp 		open(right4me/search/pop_a.jsp) 
		 5. pop_a.jsp 		opener(현재화면) form 제어
		 ----- 권리찾기신청화면 호출 ------
		*/
		clmsPop = window.open('/search/pop_a.jsp?gubun=Main&target='+oSelect+'&query='+srchWord,'right4me','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, resizable=false, width=810, height=700');

}

// 통합검색에서 리턴되었다.
function searchSet(url, srchTitle, srchSdsrName) {
	
	clmsPop.close();
	
	document.rghtPrps.srchTitle.value = srchTitle;
	document.rghtPrps.srchSdsrName.value = srchSdsrName;
	document.rghtPrps.gubun.value = "totalSearch";			// 통합검색에서 넘어온 검색어구분
	
	document.rghtPrps.action = url;
	
	document.rghtPrps.submit();
}

function RTrim(str) {
	var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
    	var i = s.length - 1;
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
			i--;
		s = s.substring(0, i+1);
	}
    return s;
}

function LTrim(str) {
	var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(0)) != -1) {
    	var j=0, i = s.length;
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
           	j++;
        s = s.substring(j, i);
    }
    return s;
}

function Trim(str) {
	return RTrim(LTrim(str));
}

function chkLimit(strTxt, strLimit, strName){
	var tmp = Trim(strTxt);
	
	if(strTxt.indexOf(strLimit) >-1 ){
		alert(strName + ' '+strLimit +'은(는) 사용할 수 없습니다.');
		return true;
	}
	return false;
}

function fn_enterChk(){
	if (event.keyCode == 13) {
		goSearch();
	}
}

function goPop(){
	alert('234567890');
}

// 인스톨 설치화면 리턴 함수
function fn_clsInstall(returnVal){
	
	//alert(returnVal);
	
	if (returnVal == 'N' ) {
		if(confirm('컴포넌트 설치가 비정상적으로 종료되었습니다.\n저작권찾기 사이트가 정상적으로 작동하지 않을 수 있습니다.\n\n다시 시도 하시겠습니까?')){
			location.reload();
		}
	} else if (returnVal == 'Y' ) {
		location.href = "/main/main.do";
	}else if (returnVal == '' ) {
		if(confirm('컴포넌트 설치가 비정상적으로 종료되었습니다.\n저작권찾기 사이트가 정상적으로 작동하지 않을 수 있습니다.\n\n다시 시도 하시겠습니까?')){
			location.reload();
		}
	}else {
		location.reload();
	}
}

// 인스톨 화면 
function fn_popInstall(){
	
		// 공지팝업
		// fn_openPopup();	// 팝업종료시 주석처리한다.
		return;
}

// 오픈 팝업 open_popp
function fn_openPopup(){
	
	// var sUrl = "/install/popUp_20100412.jsp";
	var sUrl = "/popUp/pop_20101208.html";
	
	if(fn_getCookie()){
		window.open(sUrl, "openPopup", "toolbar=no,status=yes,scrollbars=no,width=590,height=405");
	}
}

// 팝업 오픈 설정 정보
function fn_getCookie() {
	var rtn = "";
	var search = "mlsmessage=";
	if (document.cookie.length>0){
		offset = document.cookie.indexOf(search);
		if (offset != -1){
			offset += search.length;
			end = document.cookie.indexOf(";", offset);
			if (end == -1)
				end = document.cookie.length;
			rtn = unescape(document.cookie.substring(offset, end));
		}
	}

	if(rtn == "done1") {
		return false;
	}
	
	return true;
}  


if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 

 function initParameter(){
 	
	fn_popInstall();
	
	// 레이어 팝업오픈
	if(fn_getLayerCookie()){
		viewPopUpLayer();
	}
	
}



//팝업 레이어를 화면에 뿌려준다
function viewPopUpLayer()
{
	var layer = document.getElementById('popUpLayer');
	
	if(typeof(layer) != 'undefined')
	{
		var forms = document.helpForm;
		
		forms.pageName.value = "20101216";	// html의 이름만 작성
		forms.action = '/common/popUp_message.jsp';
		
		var result = trim(synchRequest(getQueryString(forms)));
		
		layer.innerHTML = result;
		
		//layer.clientHeight의 값을 정확히 구하기 위해 미리 보이지 않는 사각에 layer을 미리 화면에 띄어준다.
		layer.style.top = -1000;
		layer.style.display = '';

		layer.style.left= '150';
		layer.style.top= '100';
		
		layer.style.display = '';
	}
}

//팝업 레이어를 닫아줌
function closeMessage()
{
	var layer = document.getElementById('popUpLayer');
	
	if(typeof(layer) != 'undefined')
		layer.style.display = 'none';
}

function setCookie(name, value, expiredays) {
	var todayDate = new Date();
	todayDate.setDate( todayDate.getDate() + expiredays );
	document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
}

function onClose(colseVal) {

	if(colseVal == true) {
		setCookie("mlsmessage", "doneLayerPOP_2", 1);
	}
}
	
// 팝업레이어 오픈 설정 정보
function fn_getLayerCookie() {
	var rtn = "";
	var search = "mlsmessage=";
	if (document.cookie.length>0){
		offset = document.cookie.indexOf(search);
		if (offset != -1){
			offset += search.length;
			end = document.cookie.indexOf(";", offset);
			if (end == -1)
				end = document.cookie.length;
			rtn = unescape(document.cookie.substring(offset, end));
		}
	}

	if(rtn == "doneLayerPOP_2") {
		return false;
	}
	
	return true;
}  

action = window.setInterval("floatLayer('popUpLayer')", 1); 

-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
					
		<!-- CONTAINER str-->
		<div id="mainContainer">
			<div class="content pt0">
				<div class="mainVisual">
					<ul>
						<li><a href="/main/main.do?method=info01"><img src="/images/2010/main/moveGo_btn1.gif" alt="저작권찾기란?" title="저작권찾기란?" /></a></li>
						<li><a href="/main/main.do?method=info04"><img src="/images/2010/main/moveGo_btn2.gif" alt="저작권찾기이용안내" title="저작권찾기이용안내" /></a></li>
						<li><a href="/main/main.do?method=info04" ><img src="/images/2010/main/moveGo_btn3.gif" alt="보상금이용안내" title="보상금이용안내" /></a></li>
					</ul>
					
		<!-- hidden form str : 헬프레이어 -->
		<form name="helpForm" method="post" action="">
			<input type="hidden" name="pageName">
		</form>
		
					<div class="visual">
						<div class="price">
							<% 
								String tmpInmt = alltInmp;
								String inmt = "";
								int start = 0;
								int end = 0;
								int cnt = 0;
								String boxClass = "box";
								if(!tmpInmt.equals("|%")){
									int index = tmpInmt.indexOf("|%")+2;
									int length = Integer.parseInt(tmpInmt.substring(index));
								
									for(int i=0; i<length; i++){
										end = length-i;
										start = end-1;
										
										inmt = "<strong class="+boxClass+">"+tmpInmt.substring(start, end)+"</strong>"+inmt;
										cnt++;
										 if(cnt == 3){
											 inmt = "<span>,</span>"+inmt;
											cnt = 0;
										 }
									}
									if(inmt.substring(0,10).equals("<span>,</span>")){
										inmt = inmt.substring(10);
									}
								}else{
									inmt = "<strong class="+boxClass+">0</strong>";
								}
							%>
							<h2><img src="/images/2010/main/price_tl.png" class="png24" alt="미분배보상금액" title="미분배보상금액" /></h2>
							<p class="amountBox">
								<%=inmt%><strong class="black">원</strong>
								<span class="block infoTxt" style="width: 260px;">보상금에 대한 상세금액은 <br />해당 관리단체에 확인 해 주시기 바랍니다.</span>
							</p>
						</div>
						<p>
							<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,115,0" width="960" height="198" id="movie" align="middle"> 
								<param name="quality" value="high"> 
								<param name="movie" value="/images/2010/main/mainVisual.swf"> 
								<!-- param name="wmode" value="window" --> 
								<param name="wmode" value="transparent">
								<param name="allowFullScreen" value="true"> 
								<param name="autostart" VALUE="false"> 
								<param name="allowScriptAccess" value="never"> 
								<param name="allowNetworking" VALUE="all"> <!-- white URL에 등록된 경우 --> 
								
								<!--[if !IE]> <--> 
								<object width="960" height="198" data="/images/2010/main/mainVisual.swf" type="application/x-shockwave-flash"> 
								<param name="quality" value="high"> 
								<param name="movie" value="/images/2010/main/mainVisual.swf"> 
								<!-- param name="wmode" value="window" --> 
								<param name="wmode" value="transparent">
								<param name="allowFullScreen" value="true"> 
								<param name="autostart" VALUE="false"> 
								<param name="allowScriptAccess" value="never"> 
								<param name="allowNetworking" VALUE="all"> <!-- white URL에 등록된 경우 -->
								<p><img src="/images/2010/main/mainVis.jpg" alt=""></p>  <!-- png파일 로드 못했을경우 대체컨텐츠-->
								</object> <!--> <![endif]--> 
							</object>	
						</p>
					</div>
				</div>
				
				<div id="contentBody">
				</div>
				
				<div class="mainContentArea">
					<!-- 왼쪽 영역 -->
					<div class="fl">
						<div class="mainBoard">
							<form name="form1" method="post" action = "">
								<input type="hidden" name="page_no">
					            <input type="hidden" name="menuSeqn">
					            <input type="hidden" name="bordSeqn">
					            <input type="hidden" name="threaded">
					            <input type="hidden" name="leftsub" value="1">
					            <input type="hidden" name="mNum">
					            <input type="hidden" name="sNum">
					            <input type="hidden" name="mainType" id="mainType">
						
						<h2 class="skip">공지사항과 FAQ</h2>
							<ul>
								<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1" OnMouseOver='showtab(1);' id ="bgn1" class="selected"><strong class="noti"><img src="/images/2010/main/txt_notice.gif" alt="공지사항" /></strong></a>
									<ul id = "dis1" class="noti">
										<%
										        if (notiListSize > 0) {
										          for(int i=0; i<notiListSize; i++) {
										            Main notiDTO = (Main) notiList.get(i);
										%>
										<li><a href="javascript:boardDetail('<%=notiDTO.getBordSeqn()%>','<%=notiDTO.getMenuSeqn()%>','<%=notiDTO.getThreaded()%>')"><%=notiDTO.getTite()%></a><span><%=notiDTO.getInsertDate()%></span></li>
										<%
										          }
										        }
										%>

									</ul>
									<!-- p class="disNone"><a href="" class="more"><img src="/images/2010/main/more.gif" alt="더보기"></a></p-->
								</li>
						
								<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=8&amp;page_no=1" OnMouseOver='showtab(2);' OnFocus='showtab(2);'   id ="bgn2"><strong class="law"><img src="/images/2010/main/txt_law.gif" OnMouseOver='showtab(2);' alt="법정허락저작물" /></strong></a>
									<ul id = "dis2" class="law disNone">
										<%
										        if (lawListSize > 0) {
										          for(int i=0; i<lawListSize; i++) {
										            Main lawDTO = (Main) lawList.get(i);
										%>
										<li>
										<%if(lawDTO.getGubun().equals("2")){ %>
											<a href="javascript:boardDetail('<%=lawDTO.getBordSeqn()%>','8','<%=lawDTO.getThreaded()%>')"><%=lawDTO.getTite()%></a>
										<%}else{ %>
											<%=lawDTO.getTite()%>
										<%} %>
											<span><%=lawDTO.getInsertDate()%></span>
										</li>
										<%
										          }
										        }
										%>

									</ul>
									<!-- p><a href="" class="more2"><img src="/images/2010/main/more.gif" alt="더보기"></a></p -->
								</li>
								
								<li class="mr0"><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1" OnMouseOver='showtab(3);' OnFocus='showtab(3);'   id ="bgn3"><strong class="faq"><img src="/images/2010/main/txt_faq.gif" OnMouseOver='showtab(3);' alt="FAQ" /></strong></a>
									<ul id = "dis3" class="faq disNone">
										<%
										        if (qustListSize > 0) {
										          for(int i=0; i<qustListSize; i++) {
										            Main qustDTO = (Main) qustList.get(i);
										%>
										<li><a href="javascript:boardDetail('<%=qustDTO.getBordSeqn()%>','<%=qustDTO.getMenuSeqn()%>','<%=qustDTO.getThreaded()%>')"><%=qustDTO.getTite()%></a><span><%=qustDTO.getInsertDate()%></span></li>
										<%
										          }
										        }
										%>

									</ul>
									<!-- p><a href="" class="more2"><img src="/images/2010/main/more.gif" alt="더보기"></a></p -->
								</li>
							</ul>
							</form>
						</div>
						
						<div class="mainPublicity">
							<h2><img src="/images/2010/main/h2_unknown.gif" alt="저작권미확인저작물" /></h2>
							<ul>
								<li class="ml50 bgNone"><a href="" class="skip">저작권미확인저작물</a>
									<ul>
									<%
										        if (promPotoListSize > 0) {
										          for(int i=0; i<promPotoListSize; i++) {
										            Main potoDTO = (Main) promPotoList.get(i);
										%>
										<li>
											<dl>
												<dt><%=potoDTO.getTite()%></dt>
												<dd>
												<img src="http://www.right4me.or.kr/upload/thumb/image/<%=potoDTO.getRealFileName()%>" width="98" height="63" alt="<%=potoDTO.getWorkTitle()%>"/>
												</dd>
											</dl>
										</li>
										<%
										          }
										        }
										%>
									</ul>
									<p class="more"><a href="/noneRght/rghtSrch.do?DIVS=I"><img src="/images/2010/main/more.gif" alt="더보기"></a></p>
								</li>
							</ul>
						</div>
					</div>
					<!-- //왼쪽 영역 -->
					
					<div class="fl ml20">
						<div class="floatDiv">
							<div class="fl relative">
								<h2 class="skip">통합검색</h2>
								<!-- 검색 str -->
								<form name="rghtPrps" method="post" action = "">
									<input type="hidden" name="srchTitle">
									<input type="hidden" name="srchSdsrName">
									<input type="hidden" name="gubun">
								</form>
								<form name="srchForm" method="post" action = "">
								<div class="mainSchArea">
									<div class="schBox">
										<span class="round lt"></span><span class="round rt"></span><span class="round lb"></span><span class="round rb"></span>
										<p>
											<label for="cate"><img src="/images/2010/main/mainSch.png" alt="Search" class="png24 vmid" /></label>
											<select name="cate" id="cate" class="w30 fontSmall ml15 vtop">
												<option value="total">통합검색</option>
												<option value="music">음악저작물</option>
												<option value="album">앨범</option>
												<option value="literature">어문저작물</option>
												<option value="book">도서</option>
												<option value="movie">영화저작물</option>
												<option value="music_licensor">음악저작권자</option>
												<option value="literature_licensor">어문저작권자</option>
												<option value="movie_licensor">영화저작권자</option>
											</select>
											<label for="srchWord"></label>
											<input name="srchWord" id="srchWord" onKeyDown="javaScript:fn_enterChk();" class="w40 inputData vtop fontSmall" value="" title="검색어" />
											<input class="schBtn" onClick = "javascript:goSearch();" title="검색"/>
										</p>
									</div>
								</div>
								</form>
								<!-- 검색 end -->
								
								<div class="mt40 relative">
									
									<ul class="mainCenterGrid">
										
										<!-- 저작권미확인저작물 str -->
										<li class="tab1"><span id="ceTab1" class="select"><a href="javascript:ceShowtab(1);" OnMouseOver='ceShowtab(1);' OnMouseOut='ceShowtab(1);' OnFocus='ceShowtab(1);' class="tab"><strong OnMouseOver='ceShowtab(1);' >저작권미확인저작물</strong></a></span>
											<div id="ceDiv1" class="mainGridRound">
												<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
												<table cellspacing="0" cellpadding="0" border="1" class="mainGrid" summary="저작권미확인 저작물의 구분, 저작물명, 저작권자, 발행년도 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<caption>저작권미확인 저작물 목록</caption>
													<colgroup>
													<col width="15%">
													<col width="*">
													<col width="20%">
													<col width="20%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">구분</th>
															<th scope="col">저작물명</th>
															<th scope="col">저작권자</th>
															<th scope="col">발행년도</th>
														</tr>
													</thead>
													<tbody>
													
														<%
														                if (workNoneListSize > 0) {
														                   for(int i=0; i<workNoneListSize; i++) {
														                     Main workNoneDTO = (Main) workNoneList.get(i);
														%>
															<tr>
																<td class="ce"><%=workNoneDTO.getGenre().trim()%></td>
																<td><%=workNoneDTO.getWorkTitle().trim()%></td>
																<td class="ce"><%=workNoneDTO.getLicensorNameKor() == null ? "" : workNoneDTO.getLicensorNameKor().trim()%></td>
																<td class="ce"><%=workNoneDTO.getInsertDate().trim()%></td>
															</tr>
														<%
														                   }
														                }
														%>	
													</tbody>
												</table>
											</div>
											<!-- 테이블 영역 -->
											<p id="ceMore1" class="more"><a href="/noneRght/rghtSrch.do?DIVS=M" ><img src="/images/2010/main/more.gif" alt="더보기"></a></p>
										</li>
										<!-- 저작권미확인저작물 end -->
										
										<!-- 보상금저작물 str -->
										<li class="tab2"><span id="ceTab2"><a href="javascript:ceShowtab(2);" OnMouseOver='ceShowtab(2);' OnMouseOut='ceShowtab(2);' OnFocus='ceShowtab(2);' class="tab"><strong>보상금저작물</strong></a></span>
											<div id="ceDiv2" class="mainGridRound disNone">
												<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
												<table cellspacing="0" cellpadding="0" border="1" class="mainGrid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="15%">
													<col width="*">
													<col width="20%">
													<col width="20%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">구분</th>
															<th scope="col">저작물명</th>
															<th scope="col">저작권자</th>
															<th scope="col">사용년도</th>
														</tr>
													</thead>
													<tbody>
														<%
														                if (inmtListSize > 0) {
														                   for(int i=0; i<inmtListSize; i++) {
														                     Main inmtDTO = (Main) inmtList.get(i);
														%>
															<tr>
																<td class="ce"><%=inmtDTO.getGenre().trim()%></td>
																<td><%=inmtDTO.getWorkTitle().trim()%></td>
																<td class="ce"><%=inmtDTO.getLicensorNameKor() == null ? "" : inmtDTO.getLicensorNameKor().trim()%></td>
																<td class="ce"><%=inmtDTO.getInsertDate().trim()%></td>
															</tr>
														<%
														                   }
														                }
														%>

													</tbody>
												</table>
											</div>
											<!-- 테이블 영역 -->
											<p id="ceMore2" class="more disNone"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1"><img src="/images/2010/main/more.gif" alt="더보기"></a></p>
										</li>
										<!-- 보상금저작물 end -->
										
										<!-- 신규ICN발급저작물 str -->
										<li class="tab3"><span id="ceTab3"><a href="javascript:ceShowtab(3);" OnMouseOver='ceShowtab(3);' OnMouseOut='ceShowtab(3);' OnFocus='ceShowtab(3);' class="tab"><strong>신규ICN발급저작물</strong></a></span>
											<div id="ceDiv3" class="mainGridRound disNone">
												<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
												<table cellspacing="0" cellpadding="0" border="1" class="mainGrid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="15%">
													<col width="*">
													<col width="20%">
													<col width="20%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">구분</th>
															<th scope="col">저작물명</th>
															<th scope="col">저작권자</th>
															<th scope="col">등록일자</th>
														</tr>
													</thead>
													<tbody>
													<%
													                if (workListSize > 0) {
													                   for(int i=0; i<workListSize; i++) {
													                     Main workDTO = (Main) workList.get(i);
													%>
														<tr>
															<td class="ce"><%=workDTO.getGenre()%></td>
															<td><%=workDTO.getWorkTitle()%>
															<%if(workDTO.getInsertDate().trim().equals(sNOWDATE)){ %>
																<img src="/images/2010/main/ico_new.gif" alt="N" class="vmid ml5" />
															<%} %>
															</td>
															<td class="ce"><%=workDTO.getLicensorNameKor() == null ? "" : workDTO.getLicensorNameKor().trim()%></td>
															<td class="ce"><%=workDTO.getInsertDate().trim()%></td>
														</tr>
													<%
													                   }
													                }
													%>
													</tbody>
												</table>
											</div>
											<!-- 테이블 영역 -->
											<p id="ceMore3" class="more disNone"><a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2010/main/more.gif" alt="더보기"></a></p>
										</li>
										<!-- 신규ICN발급저작물 end -->
									</ul>
									
								</div>
							</div>
							
							<div class="fr">
								<div class="mainGuide">
									<h2 class="disNone">퀵메뉴</h2>
									<ul>
										<li><a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2010/main/banner_01.gif" alt="이용자 저작물이용을 위한 저작권조회" class="vtop" /></a></li>
										<li><a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2010/main/banner_02.gif" alt="권리자 내저작물에 대한 저작권찾기" class="vtop" /></a></li>
										<li  class="fl"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1"><img src="/images/2010/main/banner_03.gif" alt="보상금신청" class="vtop" /></a></li>
										<li class="fr">
										<% if(loginYn.equals("N")){ %>
											<a href="/user/user.do?method=goLogin" onclick="javascript:window.alert('로그인이 필요한 화면입니다.');" onkeypress="javascript:window.alert('로그인이 필요한 화면입니다.');"><img src="/images/2010/main/banner_04.gif"  class="vtop" alt="신청현황" title="신청현황" /></a>
										<% }else{ %>
											<a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1"><img src="/images/2010/main/banner_04.gif" alt="신청현황" class="vtop" /></a>
										<% } %>	
										</li>
									</ul>
								</div>
								<div class="siteLink">
									<h2 class="fl"><img src="/images/2010/main/siteLinkH2.png" alt="관련사이트링크" class="png24" /></h2>
									<ul class="fr mr10">
										<li><a href="http://www.cros.or.kr" target="_blank">저작권등록</a></li>
										<li><a href="http://www.clms.or.kr/user/main.do" target="_blank">저작권이용계약</a></li>
										<li><a href="http://freeuse.copyright.or.kr/index.do" target="_blank">자유이용</a></li>
									</ul>
								</div>
							</div>
						</div>
						
						<div class="mainLinkArea" style="z-index: 99;">
							<span class="round lt"></span><span class="round rt"></span><span class="round lb"></span><span class="round rb"></span><!-- 라운딩효과 -->
							<div class="box">
							
							<jsp:include page="/include/2010/banner.jsp"/>
							
							<!-- 
								<button type="button" class="prev"><span class="skip">&lt;</span></button>
								<ul>
									<li><a href="http://www.copyrightkorea.or.kr/" target="_blank"><img src="/images/2010/main/btmBanner1.gif" alt="" class="vmid" /><span class="ce">02-508-0440</span></a></li>
									<li><a href="http://www.komca.or.kr/" target="_blank"><img src="/images/2010/main/btmBanner2.gif" alt="" class="vmid" /><span class="ce">02-2660-0440</span></a></li>
									<li><a href="http://www.copycle.or.kr/" target="_blank"><img src="/images/2010/main/btmBanner3.gif" alt="" class="vmid" /><span class="ce">02-2608-2036</span></a></li>
									<li><a href="http://www.fokapo.or.kr/" target="_blank"><img src="/images/2010/main/btmBanner4.gif" alt="" class="vmid" /><span class="ce">02-745-8286</span></a></li>
									<li class="mr0"><a href="http://www.kapp.or.kr/" target="_blank"><img src="/images/2010/main/btmBanner5.gif" alt="" class="vmid" /><span class="ce">02-3270-5900</span></a></li>
								</ul>
								<button type="button" class="next"><span class="skip">&gt;</span></button>
							 -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	
<SPAN id="popUpLayer" style="position:absolute;top:0px;left:0px;height:0px;width:0px;background-color:#ffffff;display:none;z-index:5;border : 1 solid #aaaaaa;">
<!-- 
<iframe frameborder='no' style='border:0px; width:100% ;height:100%;filter:Alpha(Opacity=10)'></iframe>
 -->
</SPAN>
</body>
</html>
