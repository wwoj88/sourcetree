<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.main.model.Main"%>
<%@ page import="java.util.List"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.TimeZone" %>
<%
   Main mainDTO = (Main) request.getAttribute("main");

   List qustList = (List) mainDTO.getQnaList();
   int qustListSize = qustList.size();

   List notiList = (List) mainDTO.getNotiList();
   int notiListSize = notiList.size();

   List workList = (List) mainDTO.getWorkList();
   int workListSize = workList.size();
   
   List promPotoList = (List) mainDTO.getPromPotoList();
   int promPotoListSize = promPotoList.size();
   
   List promMovieList = (List) mainDTO.getPromMovieList();
   int promMovieListSize = promMovieList.size();
   
   List inmtList = (List) mainDTO.getInmtList();
   int inmtListSize = inmtList.size();
   
 	//오늘 날짜
	String sNOWDATE = new SimpleDateFormat("yyyy-mm-dd").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+09:00")).getTime());
 
   User user = SessionUtil.getSession(request);
   String sessUserIdnt = user.getUserIdnt();
   String loginYn = "N";
   if (sessUserIdnt != null) 
 	  loginYn = "Y";
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작물 권리찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript">
<!--
//탭메뉴
function showtab(n) {
  for(var i = 1; i < 3; i++) {
    obj = document.getElementById('showtab'+i);  // 보여질 리스트
    //img = document.getElementById('showtab_button'+i); // 버튼 이미지
    
    if(n == 1){
    	document.getElementById('bgn1').className = 'selected';
    	document.getElementById('bgn2').className = '';
    }else if(n == 2){
    	document.getElementById('bgn1').className = '';
    	document.getElementById('bgn2').className = 'selected';
    }   
    
    if ( n == i ) {
      obj.style.display = "block";
      //img.src = "/images/2010/main/txt_notice.gif";
    } else {
      obj.style.display = "none";
      //img.src = "/images/2010/main/txt_faq.gif";
    }
  }
}

function ceShowtab(n) {
	  for(var i = 1; i < 3; i++) {
	    obj = document.getElementById('ceShowtab'+i);  // 보여질 리스트
	   // img = document.getElementById('ceShowtab_button'+i); // 버튼 이미지

	    if ( n == i ) {
	      obj.style.display = "block";
//	      img.src = "/images/2010/main/main_ce_h2_0"+i+".gif";
	    } else {
	      obj.style.display = "none";
//	      img.src = "/images/2010/main/main_ce_h2_0"+i+"_off.gif";
	    }
	  }
	}

function promtab(n) {
	var frm = document.form1;
	  for(var i = 1; i < 3; i++) {
	    obj = document.getElementById('promtab1');  // 보여질 리스트
	    img = document.getElementById('promtab_button1'); // 버튼 이미지

	    obj2 = document.getElementById('promtab2');  // 보여질 리스트
	    img2 = document.getElementById('promtab_button2'); // 버튼 이미지
	    
	    if ( n == i ) {
	        obj.style.display = "none";
	        obj2.style.display = "block";
	        img.src = "/images/2010/main/publicity_photo.gif";
	        img2.src = "/images/2010/main/publicity_movie_over.gif";
	        frm.leftsub.value = "2";
	      } else {
	        obj.style.display = "block";
	        obj2.style.display = "none";
	        img.src = "/images/2010/main/publicity_photo_over.gif";
	        img2.src = "/images/2010/main/publicity_movie.gif";
	        frm.leftsub.value = "1";
	      }
	  }
	}

function promBoardList(){
	var frm = document.form1;

	var leftsub = frm.leftsub.value;

	frm.mNum.value = '5';
	frm.sNum.value = '0';
	frm.leftsub.value = leftsub;
	frm.menuSeqn.value = '5';
	frm.page_no.value = '1';
	frm.method = "post";
	frm.action = "/board/board.do";
	frm.submit();
}

function boardDetail(bordSeqn,menuSeqn,threaded){
	var frm = document.form1;
	frm.bordSeqn.value = bordSeqn;
	frm.menuSeqn.value = menuSeqn;
	frm.threaded.value = threaded;
	frm.page_no.value = '1';
	frm.method = "post";
	frm.action = "/board/board.do?method=boardView";
	frm.submit();
}

function promBoardDetail(bordSeqn,menuSeqn,threaded,leftsub){
	var frm = document.form1;
	frm.bordSeqn.value = bordSeqn;
	frm.menuSeqn.value = menuSeqn;
	frm.threaded.value = threaded;
	frm.page_no.value = '1';
	frm.leftsub.value = leftsub;
	frm.method = "post";
	frm.action = "/board/board.do?method=boardView";
	frm.submit();
}

function fn_certInfoOpen() {
//	window.open('/main/main.do?method=goCertPage','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=800, height=550');
	window.open('http://copyright.signra.com/','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=780, height=550');

}

// 통합검색
var clmsPop = '';
function goSearch() {
	
		var frm = document.srchForm;
		
		if(document.getElementById("srchWord").value == ""){
			alert("검색어를 입력하세요.");
			document.getElementById("srchWord").focus();
			return;
		}
		
		document.getElementById("srchWord").blur();
		 
		srchWord = document.getElementById("srchWord").value;
		
		oSelect = document.getElementById("cate").value;
		
		// 2009 기존url
		//window.open('http://www.clms.or.kr/user/search/mls/redirect.do?topMenuId=01&leftMenuId=01&target='+oSelect+'&query='+srchWord, 'win2','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, resizable=false, width=810, height=700');
		
		// 2010 newUrl
		//window.open('http://www.clms.or.kr/user/search/right4me/redirect.do?topMenuId=01&leftMenuId=01&target='+oSelect+'&query='+srchWord, 'right4me','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, resizable=false, width=810, height=700');
		
		// 통합검색(clms.or.kr)에서 권리찾기사이트(right4me.or.kr)로 접근가능해야 하므로 right4me 내 팝업을 띄우고 clms통합검색화면으로 접근한다.
		/*
		----- 검색 ------
		 1. 현재화면 		open(right4me/search/pop_a.jsp) 
		 2. pop_a.jsp 		open(clms/통합검색.jsp)
		 3. 통합검색.jsp 	location.href="rightrme/pop_c.jsp"
		 ----- 검색결과 ------
		 
		 ----- 권리찾기신청화면 이동 ------
		 4. pop_c.jsp 		open(right4me/search/pop_a.jsp) 
		 5. pop_a.jsp 		opener(현재화면) form 제어
		 ----- 권리찾기신청화면 호출 ------
		*/
		clmsPop = window.open('/search/pop_a.jsp?gubun=Main&target='+oSelect+'&query='+srchWord,'right4me','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, resizable=false, width=810, height=700');

}

// 통합검색에서 리턴되었다.
function searchSet(url, srchTitle, srchSdsrName) {
	
	clmsPop.close();
	
	document.rghtPrps.srchTitle.value = srchTitle;
	document.rghtPrps.srchSdsrName.value = srchSdsrName;
	document.rghtPrps.gubun.value = "totalSearch";			// 통합검색에서 넘어온 검색어구분
	
	document.rghtPrps.action = url;
	
	document.rghtPrps.submit();
}

function RTrim(str) {
	var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
    	var i = s.length - 1;
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
			i--;
		s = s.substring(0, i+1);
	}
    return s;
}

function LTrim(str) {
	var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(0)) != -1) {
    	var j=0, i = s.length;
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
           	j++;
        s = s.substring(j, i);
    }
    return s;
}

function Trim(str) {
	return RTrim(LTrim(str));
}

function chkLimit(strTxt, strLimit, strName){
	var tmp = Trim(strTxt);
	
	if(strTxt.indexOf(strLimit) >-1 ){
		alert(strName + ' '+strLimit +'은(는) 사용할 수 없습니다.');
		return true;
	}
	return false;
}

function fn_enterChk(){
	if (event.keyCode == 13) {
		goSearch();
	}
}

function goPop(){
	alert('234567890');
}

// 인스톨 설치화면 리턴 함수
function fn_clsInstall(returnVal){
	
	//alert(returnVal);
	
	if (returnVal == 'N' ) {
		if(confirm('컴포넌트 설치가 비정상적으로 종료되었습니다.\n내권리찾기 사이트가 정상적으로 작동하지 않을 수 있습니다.\n\n다시 시도 하시겠습니까?')){
			location.reload();
		}
	} else if (returnVal == 'Y' ) {
		location.href = "/main/main.do";
	}else if (returnVal == '' ) {
		if(confirm('컴포넌트 설치가 비정상적으로 종료되었습니다.\n내권리찾기 사이트가 정상적으로 작동하지 않을 수 있습니다.\n\n다시 시도 하시겠습니까?')){
			location.reload();
		}
	}else {
		location.reload();
	}
}

// 인스톨 화면 
function fn_popInstall(){
	
		// 공지팝업
		//fn_openPopup();	// 팝업종료시 주석처리한다.
		return;
}

// 오픈 팝업 open_popp
function fn_openPopup(){
	
	var sUrl = "/install/popUp_20100412.jsp";
	
	if(fn_getCookie()){
		window.open(sUrl, "openPopup", "toolbar=no,status=yes,scrollbars=no,width=590,height=405");
	}
}

// 팝업 오픈 설정 정보
function fn_getCookie() {
	var rtn = "";
	var search = "mlsmessage=";
	if (document.cookie.length>0){
		offset = document.cookie.indexOf(search);
		if (offset != -1){
			offset += search.length;
			end = document.cookie.indexOf(";", offset);
			if (end == -1)
				end = document.cookie.length;
			rtn = unescape(document.cookie.substring(offset, end));
		}
	}

	if(rtn == "done1") {
		return false;
	}
	
	return true;
}  
-->
</script>
</head>

<body onLoad="fn_popInstall();">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="mainContainer">
			<div class="content pt0">
				<div class="mainVisual">
					<ul>
						<li><a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2010/main/moveGo_btn1.gif" alt="권리찾기신청하기" title="권리찾기신청하기" /></a></li>
						<li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1"><img src="/images/2010/main/moveGo_btn2.gif" alt="보상금 신청하기" title="보상금 신청하기" /></a></li>
						<% if(loginYn.equals("N")){ %>
						<li><a href="/user/user.do?method=goLogin" onclick="javascript:window.alert('회원가입이 필요한 화면입니다.');" onkeypress="javascript:window.alert('회원가입이 필요한 화면입니다.');"><img src="/images/2010/main/moveGo_btn3.gif" alt="신청현황조회하기" title="신청현황조회하기" /></a></li>
						<% }else{ %>	
						<li><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1" ><img src="/images/2010/main/moveGo_btn3.gif" alt="신청현황조회하기" title="신청현황조회하기" /></a></li>
						<% } %>	
					</ul>
					<div class="visual">
						<p>
								<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,115,0" width="960" height="198" id="movie" align="middle"> 
								<param name="quality" value="high"> 
								<param name="movie" value="/images/2010/main/mainVisual.swf"> 
								<!-- param name="wmode" value="window" --> 
								<param name="wmode" value="transparent">
								<param name="allowFullScreen" value="true"> 
								<param name="autostart" VALUE="false"> 
								<param name="allowScriptAccess" value="never"> 
								<param name="allowNetworking" VALUE="all"> <!-- white URL에 등록된 경우 --> 
								
								<!--[if !IE]> <--> 
								<object width="960" height="198" data="/images/2010/main/mainVisual.swf" type="application/x-shockwave-flash"> 
								<param name="quality" value="high"> 
								<param name="movie" value="/images/2010/main/mainVisual.swf"> 
								<!-- param name="wmode" value="window" --> 
								<param name="wmode" value="transparent">
								<param name="allowFullScreen" value="true"> 
								<param name="autostart" VALUE="false"> 
								<param name="allowScriptAccess" value="never"> 
								<param name="allowNetworking" VALUE="all"> <!-- white URL에 등록된 경우 --> 
								<p><img src="/images/2010/mainVis.jpg" alt=""></p>  <!-- png파일 로드 못했을경우 대체컨텐츠-->
								</object> <!--> <![endif]--> 
							</object>	
						</p>
					</div>
				</div>
				
				<div class="mainContentArea">
					<!-- 왼쪽 영역 -->
					<div class="fl">
						<div class="mainBoard">
						<form name="form1" method="post" action = "">
								<input type="hidden" name="page_no">
					            <input type="hidden" name="menuSeqn">
					            <input type="hidden" name="bordSeqn">
					            <input type="hidden" name="threaded">
					            <input type="hidden" name="leftsub" value="1">
					            <input type="hidden" name="mNum">
					            <input type="hidden" name="sNum">
							<h2 class="skip">공지사항과 FAQ</h2>
							<ul>
								<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1" id ="bgn1" class="selected"><strong><img src="/images/2010/main/txt_notice.gif"  OnMouseOver='this.style.cursor="hand";showtab(1);' OnMouseOut='this.style.cursor="default"' id='showtab_button1'  alt="공지사항" /></strong></a>
								<a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1"  id ="bgn2" class = ""><strong><img src="/images/2010/main/txt_faq.gif"  OnMouseOver='this.style.cursor="hand";showtab(2);' OnMouseOut='this.style.cursor="default"' id='showtab_button2'  alt="FAQ" /></strong></a>
								<div style="display:;" id='showtab1'>
									<ul>
										<%
										        if (notiListSize > 0) {
										          for(int i=0; i<notiListSize; i++) {
										            Main notiDTO = (Main) notiList.get(i);
										%>
										<li><a href="javascript:boardDetail('<%=notiDTO.getBordSeqn()%>','<%=notiDTO.getMenuSeqn()%>','<%=notiDTO.getThreaded()%>')"><%=notiDTO.getTite()%></a><span><%=notiDTO.getInsertDate()%></span></li>
										<%
										          }
										        }
										%>
									</ul>
								<p><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1" class="more2"><img src="/images/2010/main/more.gif"  alt="더보기"></a></p>		
								</div>
								<div style="display:none;" id='showtab2'>
									<ul>
										<%
										        if (qustListSize > 0) {
										          for(int i=0; i<qustListSize; i++) {
										            Main qustDTO = (Main) qustList.get(i);
										%>
										<li><a href="javascript:boardDetail('<%=qustDTO.getBordSeqn()%>','<%=qustDTO.getMenuSeqn()%>','<%=qustDTO.getThreaded()%>')"><%=qustDTO.getTite()%></a><span><%=qustDTO.getInsertDate()%></span></li>
										<%
										          }
										        }
										%>
									</ul>
									<p><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1" class="more2"><img src="/images/2010/main/more.gif"  alt="더보기"></a></p>	
									</div>
								</li>
							</ul>
						</form>
						</div>
						
						<div class="mainPublicity">
							<h2><img src="/images/2010/main/h2_publicity.gif" alt="" /></h2>
							<ul>
								<li class="ml50 bgNone"><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=1&amp;menuSeqn=5&amp;page_no=1"><img src="/images/2010/main/publicity_photo_over.gif"  OnClick='promtab(1);' OnMouseOver='this.style.cursor="hand";promtab(1);' OnMouseOut='this.style.cursor="default"' id='promtab_button1'  alt="" /></a>
									<ul style="display:;" id='promtab1'>
									<%
										        if (promPotoListSize > 0) {
										          for(int i=0; i<promPotoListSize; i++) {
										            Main potoDTO = (Main) promPotoList.get(i);
										%>
										<li>
											<dl>
												<dt><%=potoDTO.getTite()%></dt>
												<dd>
												<img src="/promUpload/<%=potoDTO.getRealFileName()%>" width="98" height="63" alt=""  OnMouseOver='this.style.cursor="hand";promtab(1);' onClick = "javascript:promBoardDetail('<%=potoDTO.getBordSeqn()%>','<%=potoDTO.getMenuSeqn()%>','<%=potoDTO.getThreaded()%>','1');"/>
												</dd>
											</dl>
										</li>
										<%
										          }
										        }
										%>
									</ul>
									<p class="more"><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=1&amp;menuSeqn=5&amp;page_no=1"><img src="/images/2010/main/more.gif" alt="더보기"></a></p>
								</li>
								
								<li><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=2&amp;menuSeqn=5&amp;page_no=1"><img src="/images/2010/main/publicity_movie.gif" OnClick='promtab(2);' OnMouseOver='this.style.cursor="hand";promtab(2);' OnMouseOut='this.style.cursor="default"' id='promtab_button2'  alt="" /></a>
									<ul style="display:none;" id='promtab2'>
										<%
										        if (promMovieListSize > 0) {
										          for(int i=0; i<promMovieListSize; i++) {
										            Main movieDTO = (Main) promMovieList.get(i);
										%>
										<li>
											<dl>
												<dt><%=movieDTO.getTite()%></dt>
												<dd><img src="/images/2010/main/thumb_img1.gif" alt="" />
												<p class="play"><a href="javascript:promBoardDetail('<%=movieDTO.getBordSeqn()%>','<%=movieDTO.getMenuSeqn()%>','<%=movieDTO.getThreaded()%>','2')"><img src="/images/2010/main/playBtn.png" class="png24 bdrNone" alt="" /></a></p>
												</dd>
											</dl>
										</li>
										<%
										          }
										        }
										%>
									</ul>
									<p class="more"><a href="javascript:promBoardList();"><img src="/images/2010/main/more.gif" alt="더보기1"></a></p>
								</li>
								
							</ul>
						</div>
					</div>
					<!-- //왼쪽 영역 -->
					
					<div class="fl ml20">
						<div class="floatDiv">
							<div class="fl relative">
								<h2 class="skip">통합검색</h2>
								<!-- 검색 str -->
								<form name="rghtPrps" method="post" action = "">
									<input type="hidden" name="srchTitle">
									<input type="hidden" name="srchSdsrName">
									<input type="hidden" name="gubun">
								</form>
								<form name="srchForm" method="post" action = "">
								<div class="mainSchArea">
									<div class="schBox">
										<span class="round lt"></span><span class="round rt"></span><span class="round lb"></span><span class="round rb"></span>
										<p>
											<label for="cate"><img src="/images/2010/main/mainSch.png" alt="" class="png24 vmid" /></label>
											<select name="cate" id="cate" class="w30 fontSmall ml15 vtop">
												<option value="total">통합검색</option>
												<option value="music">음악저작물</option>
												<option value="album">앨범</option>
												<option value="literature">어문저작물</option>
												<option value="book">도서</option>
												<option value="movie">영화저작물</option>
												<option value="music_licensor">음악저작권자</option>
												<option value="literature_licensor">어문저작권자</option>
												<option value="movie_licensor">영화저작권자</option>
											</select>
											<input name="srchWord" id="srchWord" onKeyDown="javaScript:fn_enterChk();" class="w40 inputData vtop fontSmall" value="" />
											<input class="schBtn" onClick = "javascript:goSearch();"/>
										</p>
									</div>
								</div>
								</form>
								<!-- 검색 end -->
								
								<div class="mt25 relative" style="display:;" id='ceShowtab1'>
									<h2><img src="/images/2010/main/main_ce_h2_01.gif" OnClick='ceShowtab(1);' OnMouseOver='this.style.cursor="hand";ceShowtab(1);' OnMouseOut='this.style.cursor="default"' id='ceShowtab1_button1'  alt="신규저작물 등록현황" title="신규저작물 등록현황" class="vtop pl5 pb5" /> 
									<img src="/images/2010/main/main_ce_h2_02_off.gif" OnClick='ceShowtab(2);' OnMouseOver='this.style.cursor="hand";ceShowtab(2);' OnMouseOut='this.style.cursor="default"' id='ceShowtab1_button2'  alt="보상금저작물현황" title="보상금저작물현황" class="vtop pl5 pb5" /></h2>
									
									<!-- 테이블 영역 -->
									<div class="mainGridRound" >
										<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
										<table cellspacing="0" cellpadding="0" border="1" class="mainGrid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
											<colgroup>
											<col width="15%">
											<col width="*">
											<col width="20%">
											<col width="20%">
											</colgroup>
											<thead>
												<tr>
													<th scope="col">구분</th>
													<th scope="col">저작물명</th>
													<th scope="col">저작권자</th>
													<th scope="col">등록일자</th>
												</tr>
											</thead>
											<tbody>
			<%
			                if (workListSize > 0) {
			                   for(int i=0; i<workListSize; i++) {
			                     Main workDTO = (Main) workList.get(i);
			%>
												<tr>
													<td class="ce"><%=workDTO.getGenre()%></td>
													<td><%=workDTO.getWorkTitle()%>
													<%if(workDTO.getInsertDate().trim().equals(sNOWDATE)){ %>
														<img src="/images/2010/main/ico_new.gif" alt="N" class="vmid ml5" />
													<%} %>
													</td>
													<td class="ce"><%=workDTO.getLicensorNameKor() == null ? "" : workDTO.getLicensorNameKor().trim()%></td>
													<td class="ce"><%=workDTO.getInsertDate().trim()%></td>
												</tr>
			<%
			                   }
			                }
			%>
											</tbody>
										</table>
									</div>
									<!-- 테이블 영역 -->
									<p class="more"><a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2010/main/more.gif" alt="더보기"></a></p>
								</div>
								
								<div class="mt25 relative" style="display:none;" id='ceShowtab2'>
									<h2><img src="/images/2010/main/main_ce_h2_01_off.gif" OnClick='ceShowtab(1);' OnMouseOver='this.style.cursor="hand";ceShowtab(1);' OnMouseOut='this.style.cursor="default"' id='ceShowtab2_button1'  alt="신규저작물 등록현황" title="신규저작물 등록현황" class="vtop pl5 pb5" /> 
									<img src="/images/2010/main/main_ce_h2_02.gif" OnClick='ceShowtab(2);' OnMouseOver='this.style.cursor="hand";ceShowtab(2);' OnMouseOut='this.style.cursor="default"' id='ceShowtab2_button2'  alt="보상금저작물현황" title="보상금저작물현황" class="vtop pl5 pb5" /></h2>
									
									<!-- 테이블 영역 -->
									<div class="mainGridRound" >
										<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
										<table cellspacing="0" cellpadding="0" border="1" class="mainGrid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
											<colgroup>
											<col width="15%">
											<col width="*">
											<col width="20%">
											<col width="20%">
											</colgroup>
											<thead>
												<tr>
													<th scope="col">구분</th>
													<th scope="col">저작물명</th>
													<th scope="col">저작권자</th>
													<th scope="col">발행년도</th>
												</tr>
											</thead>
											<tbody>
<%
                if (inmtListSize > 0) {
                   for(int i=0; i<inmtListSize; i++) {
                     Main inmtDTO = (Main) inmtList.get(i);
%>
									<tr>
										<td class="ce"><%=inmtDTO.getGenre().trim()%></td>
										<td><%=inmtDTO.getWorkTitle().trim()%></td>
										<td class="ce"><%=inmtDTO.getLicensorNameKor() == null ? "" : inmtDTO.getLicensorNameKor().trim()%></td>
										<td class="ce"><%=inmtDTO.getInsertDate().trim()%></td>
									</tr>
<%
                   }
                }
%>
											</tbody>
										</table>
									</div>
									<!-- 테이블 영역 -->
									<p class="more"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1"><img src="/images/2010/main/more.gif" alt="더보기"></a></p>
								</div>
								
							</div>
							
							<div class="fr">
								<div class="mainGuide">
									<h2 class="mr15 mb15"><img src="/images/2010/main/h2_guide.gif" alt="Guide" class="vtop" /></h2>
									<ul class="mr5">
										<li><a href=""><img src="/images/2010/main/guideLink1.gif" alt="" class="vtop mb8" /></a></li>
										<li><a href=""><img src="/images/2010/main/guideLink2.gif" alt="" class="vtop mb8" /></a></li>
										<li><a href=""><img src="/images/2010/main/guideLink3.gif" alt="" class="vtop mb8" /></a></li>
									</ul>
								</div>
								<p>
									<a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2010/main/right_banner1.gif" alt="" class="vtop mb5" /></a>
									<a href="/rghtPrps/rghtSrch.do?DIVS=M" class="block"><img src="/images/2010/main/right_banner2.gif" alt="" class="vtop" /></a>
								</p>
							</div>
						</div>
						<div class="mainLinkArea">
							<span class="round lt"></span><span class="round rt"></span><span class="round lb"></span><span class="round rb"></span><!-- 라운딩효과 -->
							<div class="box ce">
								<button type="button" class="prev"></button>
								<ul>
									<li><a href="http://www.copyrightkorea.or.kr/"><img src="/images/2010/main/btmBanner1.gif" alt="한국문예학술저작권협회" class="vmid" /></a></li>
									<li><a href="http://www.komca.or.kr/"><img src="/images/2010/main/btmBanner2.gif" alt="한국음악저작권협회" class="vmid" /></a></li>
									<li><a href="http://www.copycle.or.kr/"><img src="/images/2010/main/btmBanner3.gif" alt="한국복사전송권협회" class="vmid" /></a></li>
									<li><a href="http://www.fokapo.or.kr/"><img src="/images/2010/main/btmBanner4.gif" alt="한국음악실연자연합회" class="vmid" /></a></li>
									<li><a href="http://www.kapp.or.kr/"><img src="/images/2010/main/btmBanner5.gif" alt="한국음원제작자협회" class="vmid" /></a></li>
								</ul>
								<button type="button" class="next"></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->

</body>
</html>
