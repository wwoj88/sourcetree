<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.main.model.Main"%>
<%@ page import="java.util.List"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.TimeZone" %>
<%
   Main mainDTO = (Main) request.getAttribute("main");

   //List qustList = (List) mainDTO.getQnaList();
   //int qustListSize = qustList.size();

   List notiList = (List) mainDTO.getNotiList();
   int notiListSize = notiList.size();

   List workList = (List) mainDTO.getWorkList();
   int workListSize = workList.size();
   
   List inmtList = (List) mainDTO.getInmtList();
   int inmtListSize = inmtList.size();
   
   List lawList = (List) mainDTO.getLawList();
   int lawListSize = lawList.size();
   
   //List workNoneList = (List) mainDTO.getWorkNoneList();
   //int workNoneListSize = workNoneList.size();
   
   List promPotoList = (List) mainDTO.getPromPotoList();
   int promPotoListSize = promPotoList.size();
   
   String alltInmp = mainDTO.getInmt();
//   String alltInmp202 = mainDTO.getInmt202();
//   String alltInmp203 = mainDTO.getInmt203();
//   String alltInmpL = mainDTO.getInmtL();
//   String alltInmpS = mainDTO.getInmtS();
   
   //List promMovieList = (List) mainDTO.getPromMovieList();
   //int promMovieListSize = promMovieList.size();
   
 	//오늘 날짜
	String sNOWDATE = new SimpleDateFormat("yyyy-mm-dd").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+09:00")).getTime());
 
   User user = SessionUtil.getSession(request);
   String sessUserIdnt = user.getUserIdnt();
   String loginYn = "N";
   if (sessUserIdnt != null) 
 	  loginYn = "Y";
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript">
<!--

// 오늘날짜
var nowDate = new Date();
var nowDateYMD = nowDate.getYear()+""
                          +(nowDate.getMonth()+1)+""
                          +nowDate.getDate();
// 날짜 비교
function chkValiDate(nowDate, chkDate) {

	var iNowDate = parseInt(nowDate);
	var iChkDate  = parseInt(chkDate);
	
	if(iChkDate > iNowDate) 
		return true;
	else
		return false;
}

//탭메뉴
function showtab(n) {

//	alert(n);
  for(var i = 1; i < 6; i++) {
    obj = document.getElementById('showtab'+i);  // 보여질 리스트
    //img = document.getElementById('showtab_button'+i); // 버튼 이미지
    
    if(n == 1){
    	document.getElementById('site1').className = 'active';
    	document.getElementById('site2').className = '';
    	document.getElementById('site3').className = '';
    	document.getElementById('site4').className = '';
    	document.getElementById('site5').className = '';
    	
    	document.getElementById('bgn1').src = '/images/2010/main/siteLink1.gif';
    	document.getElementById('bgn2').href = 'http://www.cros.or.kr/';
    	
    }else if(n == 2){
    
    	document.getElementById('site1').className = '';
    	document.getElementById('site2').className = 'active';
    	document.getElementById('site3').className = '';
    	document.getElementById('site4').className = '';
    	document.getElementById('site5').className = '';
    	
    	document.getElementById('bgn1').src = '/images/2010/main/siteLink2.gif';
    	document.getElementById('bgn2').href = 'http://freeuse.copyright.or.kr/';
    	
    }else if(n == 3){
    
    	document.getElementById('site1').className = '';
    	document.getElementById('site2').className = '';
    	document.getElementById('site3').className = 'active';
    	document.getElementById('site4').className = '';
    	document.getElementById('site5').className = '';
    	
    	document.getElementById('bgn1').src = '/images/2010/main/siteLink3.gif';
    	//document.getElementById('bgn2').href = 'http://www.clms.or.kr/';
    	document.getElementById('bgn2').target = '';
    	document.getElementById('bgn2').href = "javascript:goClms('main');"; // 1.main   2. music     3. book
    	
    }else if(n == 4){
    
    	document.getElementById('site1').className = '';
    	document.getElementById('site2').className = '';
    	document.getElementById('site3').className = '';
    	document.getElementById('site4').className = 'active';
    	document.getElementById('site5').className = '';
    	
    	document.getElementById('bgn1').src = '/images/2010/main/siteLink4.gif';
    	document.getElementById('bgn2').target = '';
    	document.getElementById('bgn2').href = '/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=2&page_no=1';
    	
    }else if(n == 5){
    	// 이벤트
    	document.getElementById('site1').className = '';
    	document.getElementById('site2').className = '';
    	document.getElementById('site3').className = '';
    	document.getElementById('site4').className = '';
    	document.getElementById('site5').className = 'active';
    	
    	document.getElementById('bgn1').src = '/images/2010/main/siteLink5.gif';
    	
    	document.getElementById('bgn2').target = '';
    	document.getElementById('bgn2').href = '/event/renewal.do';
    	
    }     
    /*
    if ( n == i ) {
      obj.style.display = "block";
      //img.src = "/images/2010/main/txt_notice.gif";
    } else {
      obj.style.display = "none";
      //img.src = "/images/2010/main/txt_faq.gif";
    }*/
  }
}

//탭메뉴
function ceShowtab(n) {

 // for(var i = 1; i < 4; i++) {
    // obj = document.getElementById('showtab'+i);  // 보여질 리스트
    //img = document.getElementById('showtab_button'+i); // 버튼 이미지
  //  alert(n);
  
      	document.getElementById('ceMore1').className = 'more';
    	document.getElementById('ceMore2').className = 'more disNone2';
    	document.getElementById('ceMore3').className = 'more disNone2';
  
    if(n == 1){
    	document.getElementById('ceMore1').className = 'more';
    	document.getElementById('ceMore2').className = 'more disNone2';
    	document.getElementById('ceMore3').className = 'more disNone2';

    	document.getElementById('ceTab1').className = 'select';
    	document.getElementById('ceTab2').className = '';
    	document.getElementById('ceTab3').className = '';
    	
    	document.getElementById('ceDiv1').className = 'mainGridRound';
    	document.getElementById('ceDiv2').className = 'mainGridRound disNone';
    	document.getElementById('ceDiv3').className = 'mainGridRound disNone';
    	
    }
    if(n == 2){

    	document.getElementById('ceMore1').className = 'more disNone2';
    	document.getElementById('ceMore2').className = 'more';
    	document.getElementById('ceMore3').className = 'more disNone2';

    	document.getElementById('ceTab1').className = '';
    	document.getElementById('ceTab2').className = 'select';
    	document.getElementById('ceTab3').className = '';
    	
    	document.getElementById('ceDiv1').className = 'mainGridRound disNone';
    	document.getElementById('ceDiv2').className = 'mainGridRound';
    	document.getElementById('ceDiv3').className = 'mainGridRound disNone';
    	
    }
    if(n == 3){
    	document.getElementById('ceMore1').className = 'more disNone2';
    	document.getElementById('ceMore2').className = 'more disNone2';
    	document.getElementById('ceMore3').className = 'more';
    	
    	document.getElementById('ceTab1').className = '';
    	document.getElementById('ceTab2').className = '';
    	document.getElementById('ceTab3').className = 'select';
    	
    	document.getElementById('ceDiv1').className = 'mainGridRound disNone';
    	document.getElementById('ceDiv2').className = 'mainGridRound disNone';
    	document.getElementById('ceDiv3').className = 'mainGridRound';
    }    

 // }
}

/*
function ceShowtab(n) {
	  for(var i = 1; i < 4; i++) {
	    obj = document.getElementById('ceShowtab'+i);  // 보여질 리스트

	    if ( n == i ) {
	      obj.style.display = "block";
	    } else {
	      obj.style.display = "none";
	    }
	  }
	
	}
*/

function promtab(n) {
	var frm = document.form1;
	  for(var i = 1; i < 3; i++) {
	    obj = document.getElementById('promtab1');  // 보여질 리스트
	    img = document.getElementById('promtab_button1'); // 버튼 이미지

	    obj2 = document.getElementById('promtab2');  // 보여질 리스트
	    img2 = document.getElementById('promtab_button2'); // 버튼 이미지
	    
	    if ( n == i ) {
	        obj.style.display = "none";
	        obj2.style.display = "block";
	        img.src = "/images/2010/main/publicity_photo.gif";
	        img2.src = "/images/2010/main/publicity_movie_over.gif";
	        frm.leftsub.value = "2";
	      } else {
	        obj.style.display = "block";
	        obj2.style.display = "none";
	        img.src = "/images/2010/main/publicity_photo_over.gif";
	        img2.src = "/images/2010/main/publicity_movie.gif";
	        frm.leftsub.value = "1";
	      }
	  }
	}

function promBoardList(){
	var frm = document.form1;

	var leftsub = frm.leftsub.value;

	frm.mNum.value = '5';
	frm.sNum.value = '0';
	frm.leftsub.value = leftsub;
	frm.menuSeqn.value = '5';
	frm.page_no.value = '1';
	frm.method = "post";
	frm.action = "/board/board.do";
	frm.submit();
}

function boardDetail(bordSeqn,menuSeqn,threaded){
	var frm = document.form1;
	frm.bordSeqn.value = bordSeqn;
	frm.menuSeqn.value = menuSeqn;
	frm.threaded.value = threaded;
	frm.page_no.value = '1';
	frm.method = "post";
	frm.action = "/board/board.do?method=boardView";
	frm.submit();
}

function promBoardDetail(bordSeqn,menuSeqn,threaded,leftsub){
	var frm = document.form1;
	frm.bordSeqn.value = bordSeqn;
	frm.menuSeqn.value = menuSeqn;
	frm.threaded.value = threaded;
	frm.page_no.value = '1';
	frm.leftsub.value = leftsub;
	frm.method = "post";
	frm.action = "/board/board.do?method=boardView";
	frm.submit();
}

function fn_certInfoOpen() {
//	window.open('/main/main.do?method=goCertPage','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=800, height=550');
	window.open('http://copyright.signra.com/','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=780, height=550');

}

// 통합검색
var clmsPop = '';
function goSearch() {
	
		var frm = document.srchForm;
		
		if(document.getElementById("srchWord").value == ""){
			alert("검색어를 입력하세요.");
			document.getElementById("srchWord").focus();
			return;
		}
		
		document.getElementById("srchWord").blur();
		 
		srchWord = document.getElementById("srchWord").value;
		
		oSelect = document.getElementById("cate").value;
		
		// 2009 기존url
		//window.open('http://www.clms.or.kr/user/search/mls/redirect.do?topMenuId=01&leftMenuId=01&target='+oSelect+'&query='+srchWord, 'win2','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, resizable=false, width=810, height=700');
		
		// 2010 newUrl
		//window.open('http://www.clms.or.kr/user/search/right4me/redirect.do?topMenuId=01&leftMenuId=01&target='+oSelect+'&query='+srchWord, 'right4me','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, resizable=false, width=810, height=700');
		
		// 통합검색(clms.or.kr)에서 권리찾기사이트(right4me.or.kr)로 접근가능해야 하므로 right4me 내 팝업을 띄우고 clms통합검색화면으로 접근한다.
		/*
		----- 검색 ------
		 1. 현재화면 		open(right4me/search/pop_a.jsp) 
		 2. pop_a.jsp 		open(clms/통합검색.jsp)
		 3. 통합검색.jsp 	location.href="rightrme/pop_c.jsp"
		 ----- 검색결과 ------
		 
		 ----- 권리찾기신청화면 이동 ------
		 4. pop_c.jsp 		open(right4me/search/pop_a.jsp) 
		 5. pop_a.jsp 		opener(현재화면) form 제어
		 ----- 권리찾기신청화면 호출 ------
		*/
		clmsPop = window.open('/search/pop_a.jsp?gubun=Main&target='+oSelect+'&query='+srchWord,'right4me','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, resizable=false, width=810, height=700');

}

// 통합검색에서 리턴되었다.
function searchSet(url, srchTitle, srchSdsrName) {
	
	clmsPop.close();
	
	document.rghtPrps.srchTitle.value = srchTitle;
	document.rghtPrps.srchSdsrName.value = srchSdsrName;
	document.rghtPrps.gubun.value = "totalSearch";			// 통합검색에서 넘어온 검색어구분
	
	document.rghtPrps.action = url;
	
	document.rghtPrps.submit();
}

// 법정허락 목록화면으로 이동(검색어 물고 이동한다)
function goStatboard(srchWord){
	
	var url = '/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=8';
	document.mainGo.srchTitle.value = srchWord;
	
	searchSet2(url);
}	

// 저작물정보 목록화면으로 이동(검색어 물고 이동한다)
function goRghtMain(gubun, genre, srchWord){
	
	var srchWord = fncReplaceStr(fncReplaceStr(srchWord, "|%", "'"), '|#', '"');	   
	var url = '';
	var srchTitle = "";
	var srchSdsrName = "";
	
	//alert(genre);
	
	// gubun=1 : 저작물명
	if(genre == '1') {
		// 음악
		url = '/rghtPrps/rghtSrch.do?DIVS=M';
	}
	else if(genre == '2') {  
		// 도서
		url = '/rghtPrps/rghtSrch.do?DIVS=B';
	}
	/* if(genre == '5') {
		// 영화
		url = '/rghtPrps/rghtSrch.do?DIVS=V';
	}
	*/
	else if(genre == '방송음악') {
		// 방송음악
		url = '/inmtPrps/inmtPrps.do?srchDIVS=1';
	}
	else if(genre == '교과용') {
		// 교과용
		url = '/inmtPrps/inmtPrps.do?srchDIVS=2';
	}
	else if(genre == '도서관') {
		// 도서관
		url = '/inmtPrps/inmtPrps.do?srchDIVS=3';
	}
	
	if(gubun == '1') {
		document.mainGo.srchTitle.value = srchWord;
	}else if(gubun == '2') {
		if(genre == '1') document.mainGo.srchLicensor.value = srchWord;
		if(genre == '2') document.mainGo.srchLicensorNm.value = srchWord;
	}else if(gubun == '3') {
		document.mainGo.srchSdsrName.value = srchWord;
	}else if(gubun == '4') {
		document.mainGo.srchMuciName.value = srchWord;
	}
			
	searchSet2(url);

}

// 메인에서 각 목록검색 이동
function searchSet2(url) {

	document.mainGo.gubun.value = "totalSearch";			// 통합검색에서 넘어온 검색어구분
	document.mainGo.action = url;
	document.mainGo.submit();
}

// 이미지저작물 상세 팝업오픈
function openImageDetail( workFileNm, workNm ) {

	var param = '';
	
	param = '&WORK_FILE_NAME='+workFileNm;
	param += '&WORK_NAME='+workNm;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=I'+param
	var name = '';
	var openInfo = 'target=imageRghtSrch width=320, height=310';
	
	window.open(url, name, openInfo);
}

function RTrim(str) {
	var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
    	var i = s.length - 1;
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
			i--;
		s = s.substring(0, i+1);
	}
    return s;
}

function LTrim(str) {
	var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(0)) != -1) {
    	var j=0, i = s.length;
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
           	j++;
        s = s.substring(j, i);
    }
    return s;
}

function Trim(str) {
	return RTrim(LTrim(str));
}

function chkLimit(strTxt, strLimit, strName){
	var tmp = Trim(strTxt);
	
	if(strTxt.indexOf(strLimit) >-1 ){
		alert(strName + ' '+strLimit +'은(는) 사용할 수 없습니다.');
		return true;
	}
	return false;
}

function fn_enterChk(){
	if (event.keyCode == 13) {
		goSearch();
	}
}

function goPop(){
	alert('234567890');
}

// 인스톨 설치화면 리턴 함수
function fn_clsInstall(returnVal){
	
	//alert(returnVal);
	
	if (returnVal == 'N' ) {
		if(confirm('컴포넌트 설치가 비정상적으로 종료되었습니다.\n저작권찾기 사이트가 정상적으로 작동하지 않을 수 있습니다.\n\n다시 시도 하시겠습니까?')){
			location.reload();
		}
	} else if (returnVal == 'Y' ) {
		location.href = "/main/main.do";
	}else if (returnVal == '' ) {
		if(confirm('컴포넌트 설치가 비정상적으로 종료되었습니다.\n저작권찾기 사이트가 정상적으로 작동하지 않을 수 있습니다.\n\n다시 시도 하시겠습니까?')){
			location.reload();
		}
	}else {
		location.reload();
	}
}

// 인스톨 화면 
function fn_popInstall(){
	
		// 공지팝업
		// fn_openPopup();	// 팝업종료시 주석처리한다.
		return;
}

// 오픈 팝업 open_popp
function fn_openPopup(){
	
	// var sUrl = "/install/popUp_20100412.jsp";
	var sUrl = "/popUp/pop_20101208.html";
	
	if(fn_getCookie()){
		window.open(sUrl, "openPopup", "toolbar=no,status=yes,scrollbars=no,width=590,height=405");
	}
}

// 팝업 오픈 설정 정보
function fn_getCookie() {
	var rtn = "";
	var search = "mlsmessage=";
	if (document.cookie.length>0){
		offset = document.cookie.indexOf(search);
		if (offset != -1){
			offset += search.length;
			end = document.cookie.indexOf(";", offset);
			if (end == -1)
				end = document.cookie.length;
			rtn = unescape(document.cookie.substring(offset, end));
		}
	}

	if(rtn == "done1") {
		return false;
	}
	
	return true;
}  


if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 

 function initParameter(){
 	
 	// 이벤트 화면 오픈
 	//openEvent();
 	
	// 마이플래폼 인스톨
	//fn_popInstall();
	
	// 레이어 팝업오픈
	// 20110325 이후 접속은 팝업을 오픈하지 않는다.
	//if( chkValiDate(nowDateYMD, '2011325')	)  {
	//
		if(fn_getLayerCookie()){
			viewPopUpLayer();
		}
	//}
	
}


//function openEvent() {
//	window.open("/event/renewal.do",'win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=1018');
//}

//팝업 레이어를 화면에 뿌려준다
function viewPopUpLayer()
{
	var layer = document.getElementById('popUpLayer');
	
	if(typeof(layer) != 'undefined')
	{
		var forms = document.helpForm;
		
		//forms.pageName.value = "20101216";	// html의 이름만 작성
		forms.pageName.value = "20110412";	// html의 이름만 작성
		forms.action = '/common/popUp_message.jsp';
		
		var result = trim(synchRequest(getQueryString(forms)));
		
		layer.innerHTML = result;
		
		//layer.clientHeight의 값을 정확히 구하기 위해 미리 보이지 않는 사각에 layer을 미리 화면에 띄어준다.
		layer.style.top = -1000;
		layer.style.display = '';

		layer.style.left= '150';
		layer.style.top= '100';
		
		layer.style.display = '';
	}
}

//팝업 레이어를 닫아줌
function closeMessage()
{
	
	// 쿠키 setting
	onClose(document.getElementById('first').checked);
	
	var layer = document.getElementById('popUpLayer');
	
	if(typeof(layer) != 'undefined')
		layer.style.display = 'none';
}

function setCookie(name, value, expiredays) {
	var todayDate = new Date();
	todayDate.setDate( todayDate.getDate() + expiredays );
	document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
}

function onClose(colseVal) {

	if(colseVal == true) {
		setCookie("mlsmessage", "doneLayerPOP_2", 1);
		//closeMessage();
	}
}
	
// 팝업레이어 오픈 설정 정보
function fn_getLayerCookie() {
	var rtn = "";
	var search = "mlsmessage=";
	if (document.cookie.length>0){
		offset = document.cookie.indexOf(search);
		if (offset != -1){
			offset += search.length;
			end = document.cookie.indexOf(";", offset);
			if (end == -1)
				end = document.cookie.length;
			rtn = unescape(document.cookie.substring(offset, end));
		}
	}

	if(rtn == "doneLayerPOP_2") {
		return false;
	}
	
	return true;
}  

action = window.setInterval("floatLayer('popUpLayer')", 1); 

-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(-1);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container"><!-- id="mainContainer" -->
			<div class="content pt0">
				<div class="mainVisual">
					<div class="fl mainVisualInfo">
						<div class="fl infoBox">
							<h2><img src="/images/2010/main/mainVisualInfoBox_h2_1.png" class="png24" alt="저작권찾기 신청안내" /></h2>
							<ul>
								<li onclick="javascript:location.href('/main/main.do?method=info02')"><a href="/main/main.do?method=info02">저작물의 <strong>권리자</strong>인 경우<span><img src="/images/2010/main/ic_go2.gif" style="cursor:hand" alt="" /></span></a></li>
								<li onclick="javascript:location.href('/main/main.do?method=info03')"><a href="/main/main.do?method=info03">저작물의 <strong>이용자</strong>인 경우<span><img src="/images/2010/main/ic_go2.gif" style="cursor:hand" alt="" /></span></a></li>
							</ul>
						</div>
						<div class="fl infoBox infoBoxBg2">
							<h2><img src="/images/2010/main/mainVisualInfoBox_h2_2.png" class="png24" alt="보상금 신청안내" /></h2>
							<ul>
								<!-- 
								<li><a href="/main/main.do?method=info04"><strong>방송</strong>에 사용된 <strong>음악</strong>의 가수, 연주자 또는 최초제작자/<strong>교과서</strong> 또는 <br /><strong>도서관</strong>에서 사용된 저작물의 <br />권리자인 경우<span><img src="/images/2010/main/ic_go2.gif" alt="" /></span></a></li>
								 -->
								<!-- 
								<li>
									<a href="" class="justify">
										<strong>방송</strong>에 사용된 <strong>음악</strong>의 가수, 연주자 
										<br /> 또는 최초제작자/<strong>교과서</strong> 또는  
										<br /><strong>도서관</strong>에서 사용된 저작물의 
										 <br />권리자인 경우
										 <span><img src="/images/2010/main/ic_go2.gif" alt="" /></span>
									</a>
								</li>
								 -->
								<li onclick="javascript:location.href('/main/main.do?method=info04')" class="justify"><a href="/main/main.do?method=info04" class="justify"><strong>방송</strong>에 사용된 <strong>음악</strong>의 가수, 연주자 
									<div class="justify" style="letter-spacing:1.3px; word-spacing: 2px; ">또는 최초제작자 / <strong>교과서</strong> 또는</div>
									<div class="justify" style="letter-spacing:2px; word-spacing: 6px; "> <strong>도서관</strong>에서 사용된 저작물의</div>
								    <div class="justify" style="letter-spacing:1px; word-spacing: 1px; "> 권리자인 경우</div> 
								    <span><img src="/images/2010/main/ic_go2.gif" alt="" style="cursor:hand"/></span></a></li>
							</ul>
						</div>
					</div>
					<div class="fr">
						<div class="mainVis">
						<!-- 이미지일경우
						<img src="/images/2010/main/mainVisualTxt.png" alt="권리자에겐 정당한 보상을, 이용자에겐 편리한 사용을. 저작권찾기가 그 시작입니다." title="권리자에겐 정당한 보상을, 이용자에겐 편리한 사용을. 저작권찾기가 그 시작입니다." class="png24" />
						-->
							<p style="position: relative; left: -30px;">
								<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,115,0" width="310" height="60" id="movie" align="middle"> 
								<param name="quality" value="high"> 
								<param name="movie" value="/images/2010/main/mainVisual2.swf"> 
								<!-- param name="wmode" value="window" --> 
								<param name="wmode" value="transparent">
								<param name="allowFullScreen" value="true"> 
								<param name="autostart" VALUE="false"> 
								<param name="allowScriptAccess" value="never"> 
								<param name="allowNetworking" VALUE="all"> <!-- white URL에 등록된 경우 --> 
								
								<!--[if !IE]> <--> 
								<object width="310" height="60" data="/images/2010/main/mainVisual2.swf" type="application/x-shockwave-flash"> 
								<param name="quality" value="high"> 
								<param name="movie" value="/images/2010/main/mainVisual2.swf"> 
								<!-- param name="wmode" value="window" --> 
								<param name="wmode" value="transparent">
								<param name="allowFullScreen" value="true"> 
								<param name="autostart" VALUE="false"> 
								<param name="allowScriptAccess" value="never"> 
								<param name="allowNetworking" VALUE="all"> <!-- white URL에 등록된 경우 -->
								<p><img src="/images/2010/main/mainVisualTxt.png" alt=""></p>  <!-- png파일 로드 못했을경우 대체컨텐츠-->
								</object> <!--> <![endif]--> 
							</object>	
							</p>
						</div>
						<div class="price">
						<% 
								String tmpInmt = alltInmp;
								String inmt = "";
								int start = 0;
								int end = 0;
								int cnt = 0;
								String boxClass = "box";
								if(!tmpInmt.equals("|%")){
									int index = tmpInmt.indexOf("|%")+2;
									int length = Integer.parseInt(tmpInmt.substring(index));
								
									for(int i=0; i<length; i++){
										end = length-i;
										start = end-1;
										
										inmt = "<strong class="+boxClass+">"+tmpInmt.substring(start, end)+"</strong>"+inmt;
										cnt++;
										 if(cnt == 3){
											 inmt = "<span>,</span>"+inmt;
											cnt = 0;
										 }
									}
									if(inmt.substring(0,10).equals("<span>,</span>")){
										inmt = inmt.substring(10);
									}
								}else{
									inmt = "<strong class="+boxClass+">0</strong>";
								}
							%>
							<h2><img src="/images/2010/main/amount_tl.png" class="png24" alt="미분배보상금액" title="미분배보상금액" /></h2>
							<p class="amountBox">
								<%=inmt%> <strong class="orange">원</strong>
								<span class="block mt5 lft">
									<img src="/images/2010/main/amount_txtInfo.gif" alt="" /><br />
									<a href="/main/main.do?method=goContactUs" class="inBlock lft mt5"><img src="/images/2010/main/amount_goLink.gif" alt="" /></a>
								</span>
							</p>
						</div>
					</div>
				</div>
				
				<div class="mainContentArea">
					<!-- 왼쪽 영역 -->
					<div class="fl">
						<div class="mainBoard">
						<!-- hidden form str : 팝업레이어 -->
						<form name="helpForm" method="post" action="#">
							<input type="hidden" name="pageName">
							<input type="submit" style="display:none;">
						</form>
						<form name="mainGo" method="post" action = "#">
							<input type="hidden" name="srchTitle">
							<input type="hidden" name="srchLicensor">
							<input type="hidden" name="srchLicensorNm">
							<input type="hidden" name="srchDistributor">
							<input type="hidden" name="srchSdsrName">
							<input type="hidden" name="srchMuciName">
							<input type="hidden" name="gubun">
							<input type="submit" style="display:none;">
						</form>
						<form name="form1" method="post" action = "#">
								<input type="hidden" name="page_no">
					            <input type="hidden" name="menuSeqn">
					            <input type="hidden" name="bordSeqn">
					            <input type="hidden" name="threaded">
					            <input type="hidden" name="leftsub" value="1">
					            <input type="hidden" name="mNum">
					            <input type="hidden" name="sNum">
					            <input type="hidden" name="mainType" id="mainType">
					            <input type="submit" style="display:none;">
							<h2 class="skip">공지사항과 FAQ</h2>
							<ul>
								<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1"><strong class="noti"><img src="/images/2010/main/txt_notice.gif" alt="공지사항" /></strong></a>
									<ul>
										<%
										        if (notiListSize > 0) {
										          for(int i=0; i<notiListSize; i++) {
										            Main notiDTO = (Main) notiList.get(i);
										%>
										<li><a href="#1" onclick="javascript:boardDetail('<%=notiDTO.getBordSeqn()%>','<%=notiDTO.getMenuSeqn()%>','<%=notiDTO.getThreaded()%>')"><%=notiDTO.getTite()%></a><span><%=notiDTO.getInsertDate()%></span></li>
										<%
										          }
										        }
										%>
									</ul>
									<p><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1" class="more"><img src="/images/2010/main/more.gif" alt="더보기1"></a></p>
								</li>
							</ul>
							</form>
						</div>
						
						<div class="siteLink">
							<h2><img src="/images/2010/main/siteLinkH2.png" alt="홍보마당" class="png24" /></h2>
							<ul>
								<li id="site5" class="active"><a href="/event/renewal.do" OnMouseOver='showtab(5);' onfocus='showtab(5);' target="_blank">1</a></li>
								<li id="site4"><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=2&page_no=1" OnMouseOver='showtab(4);' onfocus='showtab(4);'>2</a></li>
								<li id="site1"><a href="http://www.cros.or.kr" OnMouseOver='showtab(1);' onfocus='showtab(1);' target="_blank">3</a></li>
								<li id="site2" ><a href="http://freeuse.copyright.or.kr" OnMouseOver='showtab(2);' onfocus='showtab(2);' target="_blank">4</a></li>
								<li id="site3" ><a href="http://www.clms.or.kr" OnMouseOver='showtab(3);' onfocus='showtab(3);' target="_blank">5</a></li>
							</ul>
							<p class="mt10"><a href="/event/renewal.do" target=""  id ="bgn2"><img src="/images/2010/main/siteLink5.gif" id ="bgn1" alt="" /></a></p>
						</div>
						
					</div>
					<!-- //왼쪽 영역 -->
					
					<div class="fl ml20">
						<div class="floatDiv">
							<div class="fl relative">
								
								<div class="mt25 relative" style="width: 450px;">
									
									<ul class="mainCenterGrid">
										<!-- 권리미상저작물 str -->
										<li class="tab1"><span id="ceTab1" class="select"><a href="#1" onclick="javascript:ceShowtab(1);" OnMouseOver='ceShowtab(1);' onblur='ceShowtab(1);' OnMouseOut='ceShowtab(1);' OnFocus='ceShowtab(1);' class="tab"><strong OnMouseOver='ceShowtab(1);' OnFocus='ceShowtab(1);' >저작권정보</strong></a></span>
											<div id="ceDiv1" class="mainGridRound">
												<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
												<table cellspacing="0" cellpadding="0" border="1" class="mainGrid" summary="저작권미확인 저작물의 구분, 저작물명, 저작(권)자 발행일자 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="15%">
													<col width="*">
													<col width="23%">
													<col width="20%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">구분</th>
															<th scope="col">저작물명</th>
															<th scope="col">저작(권)자</th>
															<th scope="col">등록일자</th>
														</tr>
													</thead>
													<tbody>
																											
														<%
														                if (workListSize > 0) {
														                   for(int i=0; i<workListSize; i++) {
														                     Main workDTO = (Main) workList.get(i);
														%>
															<tr>
																<td class="ce"><%=workDTO.getGenre().trim()%></td>
																<td><a href="#1" onclick="javascript:goRghtMain('1','<%=workDTO.getGenreCode()%>','<%=workDTO.getTitleOrg().replaceAll("'", "|%").replaceAll("\"", "|#") %>')"><%=workDTO.getWorkTitle().trim()%></a></td>
																<td class="ce"><a href="#1" onclick="javascript:goRghtMain('2','<%=workDTO.getGenreCode()%>','<%=workDTO.getLicensorNameKorOrg() == null ? "" : workDTO.getLicensorNameKorOrg().indexOf(",")>0 ? workDTO.getLicensorNameKorOrg().substring(0, workDTO.getLicensorNameKorOrg().indexOf(",")).replaceAll("'", "|%").replaceAll("\"", "|#") : workDTO.getLicensorNameKorOrg().replaceAll("'", "|%").replaceAll("\"", "|#") %>')"><%=workDTO.getLicensorNameKor() == null ? "" : workDTO.getLicensorNameKor().trim()%></a></td>
																<td class="ce"><%=workDTO.getInsertDate().trim().equals("..") ? "" : workDTO.getInsertDate().trim()%></td>
															</tr>
														<%
														                   }
														                }
														%>	
													</tbody>
												</table>
											</div>
											<!-- 테이블 영역 -->
											<p id="ceMore1" class="more"><a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2010/main/more.gif" alt="더보기2"></a></p>
										</li>
										<!-- 권리미상저작물 end -->
										
										<!-- 보상금저작물 str -->
										<li class="tab2"><span id="ceTab2"><a href="#1" onclick="javascript:ceShowtab(2);" OnMouseOver='ceShowtab(2);' OnMouseOut='ceShowtab(2);' onblur='ceShowtab(2);' OnFocus='ceShowtab(2);' class="tab"><strong>보상금발생저작물</strong></a></span>
											<div id="ceDiv2" class="mainGridRound disNone">
												<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
												<table cellspacing="0" cellpadding="0" border="1" class="mainGrid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="15%">
													<col width="*">
													<col width="20%">
													<col width="20%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">구분</th>
															<th scope="col">저작물명</th>
															<th scope="col">저작(권)자</th>
															<th scope="col">사용년도</th>
														</tr>
													</thead>
													<tbody>
														<%
														                if (inmtListSize > 0) {
														                   for(int i=0; i<inmtListSize; i++) {
														                     Main inmtDTO = (Main) inmtList.get(i);
														%>
															<tr>
																<td class="ce"><%=inmtDTO.getGenre().trim()%></td>
																<td><a href="#1" onclick="javascript:goRghtMain('3','<%=inmtDTO.getGenre()%>','<%=inmtDTO.getTitleOrg().replaceAll("'", "|%").replaceAll("\"", "|#")%>')"><%=inmtDTO.getWorkTitle().trim()%></a></td>
																<td class="ce"><a href="#1" onclick="javascript:goRghtMain('4','<%=inmtDTO.getGenre()%>','<%=inmtDTO.getLicensorNameKorOrg() == null ? "" : inmtDTO.getLicensorNameKorOrg().indexOf(",")>0 ? inmtDTO.getLicensorNameKorOrg().substring(0, inmtDTO.getLicensorNameKorOrg().indexOf(",")).replaceAll("'", "|%").replaceAll("\"", "|#") : inmtDTO.getLicensorNameKorOrg().replaceAll("'", "|%").replaceAll("\"", "|#") %>')"><%=inmtDTO.getLicensorNameKor() == null ? "" : inmtDTO.getLicensorNameKor().trim()%></td>
																<td class="ce"><%=inmtDTO.getInsertDate().trim()%></td>
															</tr>
														<%
														                   }
														                }
														%>
													</tbody>
												</table>
											</div>
											<!-- 테이블 영역 -->
											<p id="ceMore2" class="more disNone2"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1"><img src="/images/2010/main/more.gif" alt="더보기3"></a></p>
										</li>
										<!-- 보상금저작물 end -->
										<!-- 신규등록저작물 str -->
										<li class="tab3"><span id="ceTab3"><a href="#1" onclick="javascript:ceShowtab(3);" OnMouseOver='ceShowtab(3);' OnMouseOut='ceShowtab(3);' onblur='ceShowtab(3);' OnFocus='ceShowtab(3);' class="tab"><strong>법정허락저작물</strong></a></span>
											<div id="ceDiv3" class="mainGridRound disNone">
												<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
												<table cellspacing="0" cellpadding="0" border="1" class="mainGrid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="15%">
													<col width="*">
													<col width="20%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">구분</th>
															<th scope="col">저작물명</th>
															<th scope="col">등록일자</th>
														</tr>
													</thead>
													<tbody>
													
												<%
										        if (lawListSize > 0) {
										          for(int i=0; i<lawListSize; i++) {
										            Main lawDTO = (Main) lawList.get(i);
												%>
												<tr>
													<td class="ce"><%=lawDTO.getGubunName()%></td>
												<%//if(lawDTO.getGubun().equals("2")){ %>
													<!-- <td><a href="javascript:boardDetail('<%=lawDTO.getBordSeqn()%>','8','<%=lawDTO.getThreaded()%>')"><%=lawDTO.getTite()%></a></td> -->
												<%//}else{ %>
													 <!-- <td><%=lawDTO.getTite()%></td> -->
												<%//} %>
												
												    <td><a href="#1" onclick="javascript:goStatboard('<%=lawDTO.getTite()%>')"><%=lawDTO.getTite()%></a></td>
													<td class="ce"><%=lawDTO.getInsertDate()%></td>
												</tr>
												<%
												          }
												        }
												%>
													
													
													</tbody>
												</table>
											</div>
											<!-- 테이블 영역 -->
											<p id="ceMore3" class="more disNone2"><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=8&page_no=1"><img src="/images/2010/main/more.gif" alt="더보기4"></a></p>
										</li>
										<!-- 신규등록저작물 end -->
									</ul>
									
								</div>
							</div>
							
							<div class="fr">
								<div class="mainGuide">
									<h2 class="disNone">퀵메뉴</h2>
									<p><a href="/main/main.do?method=info05"><img class="vtop" alt="" src="/images/2010/main/banner_05.gif"></a></p>
								</div>
								
								
								<div class="clear mainPublicity" style="height: 120px;">
									<h2><img src="/images/2010/main/h2_unknown.gif" alt="권리자미확인 저작물" /></h2>
									<ul>
										<li class="ml50 bgNone"><a href="#1" class="skip">권리자미확인 저작물</a>
											<ul>
																				<%
										        if (promPotoListSize > 0) {
										          for(int i=0; i<promPotoListSize; i++) {
										            Main potoDTO = (Main) promPotoList.get(i);
												%>
												<%if(i==1){ %>
												<li class="ml7">
												<%}else{ %>
												<li>
												<%} %>
													<dl>
														<dt><%=potoDTO.getTite()%></dt>
														<dd oncontextmenu="return false">
															<img src="http://www.right4me.or.kr/upload/thumb/image/<%=potoDTO.getRealFileName()%>" width="98" height="63" onclick="javascript:openImageDetail('<%=potoDTO.getRealFileName()%>','<%=potoDTO.getWorkTitle()%>');" style="cursor:hand;" alt="<%=potoDTO.getWorkTitle()%>"/>
														</dd>
													</dl>
												</li>
												<%
												          }
												        }
												%>
											</ul>
											<p class="more"><a href="/noneRght/rghtSrch.do?DIVS=I"><img src="/images/2010/main/more.gif" alt="더보기5"></a></p>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="mainLinkArea">
							<span class="round lt"></span><span class="round rt"></span><span class="round lb"></span><span class="round rb"></span><!-- 라운딩효과 -->
							<div class="box">
								<jsp:include page="/include/2010/banner.jsp"/>
								<!--<button type="button" class="prev"><span class="skip">&lt;</span></button>
								<ul>
									<li><a href=""><img src="/images/2010/main/btmBanner1.gif" alt="" class="vmid" /></a></li>
									<li><a href=""><img src="/images/2010/main/btmBanner2.gif" alt="" class="vmid" /></a></li>
									<li><a href=""><img src="/images/2010/main/btmBanner3.gif" alt="" class="vmid" /></a></li>
									<li><a href=""><img src="/images/2010/main/btmBanner4.gif" alt="" class="vmid" /></a></li>
									<li class="mr0"><a href=""><img src="/images/2010/main/btmBanner5.gif" alt="" class="vmid" /></a></li>
								</ul>
								<button type="button" class="next"><span class="skip">&gt;</span></button>
								-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<SPAN id="popUpLayer" style="position:absolute;top:0px;left:0px;height:0px;width:0px;background-color:#ffffff;display:none;z-index:999;border : 1 solid #aaaaaa;">
</body>
</html>
