<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<head>

<meta http-equiv="X-UA-Compatible" content="IE=9,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<script async="" src="https://www.google-analytics.com/analytics.js"></script>
<title>한국저작권위원회 저작권 찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css"/>
<link type="text/css" rel="stylesheet" href="/css/2012/style.css"/>
<script type="text/javascript" language="javascript" src="/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" language="javascript" src="/js/Function.js"></script>
<script type="text/javascript" language="javascript" src="/js/jquery.cookie.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" language="javascript" src="/js/main/menu.js"></script>
<script type="text/javascript" language="javascript" src="/js/main/flexslider.js"></script>
<script type="text/javascript" language="javascript" src="/js/main/js.js"></script>

</head>
<script>
/* 	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69621660-2', 'auto');
	  ga('send', 'pageview'); */

</script>
<body>
<!-- 전체를 감싸는 DIVISION -->
<!-- <div id="wrap"> -->
<!-- HEADER str-->
<script type="text/javascript" language="javascript" src="https://www.findcopyright.or.kr/js/makePCookie.js"></script> 
<script type="text/javascript" language="javascript" src="https://www.findcopyright.or.kr/js/menuhover.js"></script>
<script type="text/javascript" language="javascript" src="https://www.findcopyright.or.kr/js/main/flexslider.js"></script>
<script type="text/javascript" language="javascript" src="https://www.findcopyright.or.kr/js/main/js.js"></script>

<!-- 추가된 링크 -->


<jsp:include page="/include/2017/header.jsp" />
<script type="text/javascript">
	$(function(){
		$("#sendMail").click(sendMailClick);
	})
	
	function sendMailClick(event){
		//alert("sendMailClick");
		location.href = "/main/main.do?method=sendMailTest"
	}
	
	$(function(){
		for(var i = 0 ; i < 10 ; i ++){
			//console.log($('#anucItem'+i).html())
			if($('#anucItem'+i).html()!=undefined){
				$('#anucItem'+i).html(wonAttach($('#anucItem'+i).html()));
			}
		}
	})	

	function wonAttach(text){
		//console.log(text)
			if(text.substr(text.indexOf(-1))!="원"){
				text+="원";
			}
			return text;
	}
</script>
<!-- <input id="sendMail" type="button" value="메일보내기" /> -->

<!-- <script type="text/javascript">initNavigation(0);</script> -->
<!--/* bg_2c65aa -->
<div id="banners_bg">
	<div id="banners">
		<div class="banners_lf">
			<div class="blf_lf">
				<ul>
					<li>
						<a href="/rghtPrps/rghtSrch.do?DIVS=M">
							<strong>내 권리 찾기</strong>
							<p>
								내 저작물의 저작권 정보와<br>미분배 저작물을 확인하세요
							</p>
						</a>
					</li>
					<li>
						<a href="/srchList.do">
							<strong>저작권자 찾기<span>이용자</span></strong>
							<p>
								저작권자 검색 서비스로<br>편리하게 저작권자를 <br>확인하세요,
							</p>
						</a>
					</li>
					<li>
						<a href="/mlsInfo/inmtInfo02.jsp">
							<strong>미분배 보상금 대상 저작권 확인</strong>
							<p>
								미분배 보상금 저작물을<br>확인하여 정당한 보상을<br>받으세요
							</p>
						</a>
					</li>
				</ul>
			</div>
			<div class="blf_rt">
				<div class="flexslider">
					<ul class="slides_n">
						<li><a href="/rghtPrps/rghtSrch.do?DIVS=M"><img src="/images/2017/new/main_1_1.png" alt="내 권리 찾기 내 저작물의 저작권 정보와 미분배 저작물을 확인하세요." draggable="false"></a></li>
						<li><a href="/srchList.do"><img src="/images/2017/new/main_1_2.png" alt="저작권자 찾기 이용자 저작권자 검색 서비스로 편리하게  저작권자를  확인하세요.검색되지 않는 저작권자의 저작물 이용은 상당한 노력 및  법정허락 이용승인 신청 서비스를 통해 해결하세요." draggable="false"></a></li>
						<li><a href="/mlsInfo/useLicestatInfo01.jsp"><img src="/images/2017/new/main_1_3.png" alt="미분배 보상금 대상 저작물 찾기 창작자 창작자가 저작권에 대한 정당한 보상을 받을 수 있도록 미분배 보상금 대상 저작물목록을 제공하는 서비스 입니다." draggable="false"></a></li>
					</ul>
				</div>
				<script type="text/javascript">
				$('.slides_n').bxSlider({
					auto: true,
					autoStart: true,
			   
					speed: 700,
					pager: true,
					mode: 'fade',
					controls: false,
				});
				</script>
			</div>
		</div>
		<div class="banners_rt">
			<div id="slider002">
				<ul class="slides_r">
					<li><img src="/images/2017/new/main_2_1.png" alt="올바른 저작권 이용이 만드는 행복한 문화생활 만들기! 저작권 찾기가그 시작입니다!" draggable="false"></li>
					<li><img src="/images/2017/new/main_2_2.png" alt="저작권을 찾아줘" draggable="false"></li>
					<li><img src="/images/2017/new/main_2_3.png" alt="저작권자를 압수없는 저작물의 합법적인 이용! 법정허락제도가 도와드립니다!" draggable="false"></li>
				</ul>
			</div>
			<script type="text/javascript">
				$('.slides_r').bxSlider({
					auto: true,
					autoStart: true,
				
					speed: 700,
					pager: true,
					mode: 'fade',
					controls: false,
				});
				</script>
		</div>
		<p class="clear">
		</p>
		<div class="">
		</div>
	</div>
</div>
<!-- //main Link -->
<!-- main Data -->
<div id="contents">
	<!-- <div class="con_lf"> -->
	<div>
		<div class="message_bm">
			<ul class="message1">
				<li>
				<a href="#none" class="on"><b>저작권자 조회 공고</b></a>
				<div class="message_child_menu">
					<ul>
						<table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;" width="100%" summary="저작권자 조회 목록입니다.">
						<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<caption>저작권자 조회</caption>
						<colgroup>
						<col width="15%">
						<col width="70%">
						<col width="15%">
						</colgroup>
						<thead>
						<tr>
							<th scope="col" style="text-align: center;">
								장르
							</th>
							<th scope="col" style="text-align: center;">
								제목
							</th>
							<th scope="col" style="text-align: center;">
								공고일
							</th>
						</tr>
						</thead>
						<tbody>
						<c:if test="${bord01Size == 0}">
								<tr>
									<td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
								</tr>
						</c:if>
				        <c:if test="${bord01Size > 0 }">
				        <c:forEach var="anucBord01" items="${anucBord01}" varStatus="status">
						
						<tr>
				          <td class="ce">${fn:substring(anucBord01.genreCdName, 0, 2)}</td>
				          <td class="content" align="left" style="text-align: left; margin-left: 10px;">
				          <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord01.bordSeqn}&amp;bordCd=${anucBord01.bordCd}&amp;divsCd=${anucBord01.divsCd}">
						  ${fn:replace(anucBord01.tite, '<', '&lt;')}</a>
				          </td>
				    	  <td id="anucItem${status.index}" class="ce">${anucBord01.openDttm}</td>
				         </tr>
							<c:if test="${bord01Size < 7}">
								<c:forEach step="1" begin="0" end="${6-bord01Size}">
									<tr>

									</tr>
								</c:forEach>
							</c:if>
						</c:forEach>
						</c:if>
						</tbody>
						</table>
						<div class="message_tp_rt" style="margin-top: 10px;">
							<a href="/statBord/statBo01List.do?bordCd=1&divsCd=5">더 보기 <strong>+</strong></a>
						</div>
					</ul>
					<p class="clear">
					</p>
				</div>
				</li>
				<li>
				<a href="#none" class=""><b>상당한 노력 공고</b></a>
				<div class="message_child_menu disnone">
					<ul>
						<table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;" width="100%" summary="상당한 노력 공고 조회 목록입니다.">
						<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<caption>상당한 노력 공고</caption>
						<colgroup>
						<col width="15%">
						<col width="70%">
						<col width="15%">
						</colgroup>
						<thead>
						<tr>
							<th scope="col" style="text-align: center;">
								장르
							</th>
							<th scope="col" style="text-align: center;">
								제목
							</th>
							<th scope="col" style="text-align: center;">
								공고일
							</th>
						</tr>
						</thead>
						<tbody>
						<c:if test="${bord06Size == 0}">
								<tr>
									<td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
								</tr>
						</c:if>
				        <c:if test="${bord06Size > 0 }">
				        <c:forEach var="anucBord06" items="${anucBord06}">
						
						<tr>
				          <td class="ce">${fn:substring(anucBord06.genreCdName, 0, 2)}</td>
				          <td align="left" style="text-align: left; margin-left: 10px;">
				          <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord06.bordSeqn}&amp;bordCd=${anucBord06.bordCd}&amp;divsCd=${anucBord06.divsCd}">
							${fn:replace(anucBord06.tite, '<', '&lt;')}</a>
						  </td>
				    	  <td class="ce">${anucBord06.openDttm}</td>
				         </tr>
						
							<c:if test="${bord06Size < 7}">
								<c:forEach step="1" begin="0" end="${6-bord06Size}">
									<tr>

									</tr>
								</c:forEach>
							</c:if>
						</c:forEach>
						</c:if>
						</tbody>
						</table>
						<div class="message_tp_rt" style="margin-top: 10px;">
							<a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=4">더 보기 <strong>+</strong></a>
						</div>
					</ul>
					<p class="clear">
					</p>
				</div>
				</li>
				<li>
				<a href="#none"><b>법정허락 승인 신청</b></a>
				<div class="message_child_menu disnone">
					<ul>
						<table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;" width="100%" summary="승인 신청 조회 목록입니다.">
						<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<caption>법정허락 승인 신청</caption>
						<colgroup>
						<col width="15%">
						<col width="70%">
						<col width="15%">
						</colgroup>
						<thead>
						<tr>
							<th scope="col" style="text-align: center;">
								구분
							</th>
							<th scope="col" style="text-align: center;">
								제목
							</th>
							<th scope="col" style="text-align: center;">
								공고일
							</th>
						</tr>
						</thead>
						<tbody>
						<c:if test="${bord03Size == 0}">
								<tr>
									<td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
								</tr>
						</c:if>
				        <c:if test="${bord03Size > 0 }">
				        <c:forEach var="anucBord03" items="${anucBord03}">
						
						<tr>
				          <td class="ce">${anucBord03.divsCdName}</td>
				          <td align="left" style="text-align: left; margin-left: 10px;">
				          <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord03.bordSeqn}&amp;bordCd=${anucBord03.bordCd}&amp;divsCd=${anucBord03.divsCd}">
							${fn:replace(anucBord03.tite, '<', '&lt;')}</a>
				          </td>
				    	  <td class="ce">${anucBord03.openDttm}</td>
				         </tr>
							<c:if test="${bord03Size < 7}">
								<c:forEach step="1" begin="0" end="${6-bord03Size}">
									<tr>

									</tr>
								</c:forEach>
							</c:if>
						</c:forEach>
						</c:if>
						</tbody>
						</table>
						<div class="message_tp_rt" style="margin-top: 10px;">
							<a href="/statBord/statBo03List.do?bordCd=3">더 보기 <strong>+</strong></a>
						</div>
					</ul>
					<p class="clear">
					</p>
				</div>
				</li>
				<li>
				<a href="#none" class=""><b>법정허락 승인공고</b></a>
				<div class="message_child_menu disnone">
					<ul>
						<table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;" width="100%" summary="승인공고 조회 목록입니다.">
						<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<caption>법정허락 승인공고</caption>
						<colgroup>
						<col width="15%">
						<col width="55%">
						<col width="15%">
						</colgroup>
						<thead>
						<tr>
							<th scope="col" style="text-align: center;">
								구분
							</th>
							<th scope="col" style="text-align: center;">
								제목
							</th>
							<th scope="col" style="text-align: center;">
								공고일
							</th>
						</tr>
						</thead>
						<tbody>
						<c:if test="${bord04Size == 0}">
								<tr>
									<td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
								</tr>
						</c:if>
				        <c:if test="${bord04Size > 0 }">
				        <c:forEach var="anucBord04" items="${anucBord04}">
						
						<tr>
				          <td class="ce">${anucBord04.divsCdName}</td>
				          <td align="left" style="text-align: left; margin-left: 10px;">
				          <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord04.bordSeqn}&amp;bordCd=${anucBord04.bordCd}&amp;divsCd=${anucBord04.divsCd}">
							${fn:replace(anucBord04.tite, '<', '&lt;')}</a>
				          </td>
				    	  <td class="ce">${anucBord04.openDttm}</td>
				         </tr>
							<c:if test="${bord04Size < 7}">
								<c:forEach step="1" begin="0" end="${6-bord04Size}">
									<tr>
	
									</tr>
								</c:forEach>
							</c:if>
						</c:forEach>
						</c:if>
						</tbody>
						</table>
						<div class="message_tp_rt" style="margin-top: 10px;">
							<a href="/statBord/statBo04List.do?bordCd=4">더 보기 <strong>+</strong></a>
						</div>
					</ul>
					<p class="clear">
					</p>
				</div>
				</li>
				<li>
				<a href="#none"><b>법정허락 승인 목록</b></a>
				<div class="message_child_menu disnone">
					<ul>
						<table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;" width="100%" summary="보상금 공탁 공고 조회 목록입니다.">
						<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<caption>보상금 공탁 공고</caption>
						<colgroup>
						<col width="15%">
						<col width="55%">
						<col width="15%">
						<col width="15%">
						</colgroup>
						<thead>
						<tr>
							<th scope="col" style="text-align: center;">
								장르
							</th>
							<th scope="col" style="text-align: center;">
								제목
							</th>
							<th scope="col" style="text-align: center;">
								신청자
							</th>
							<th scope="col" style="text-align: center;">
								보상금
							</th>
						</tr>
						</thead>
						<tbody>
						<c:if test="${bord07Size == 0}">
									<tr>
										<td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
									</tr>
							</c:if>
					        <c:if test="${bord07Size > 0 }">
					        <c:forEach var="anucBord07" items="${anucBord07}" varStatus="status">
							
							<tr>
					          <td class="ce">${anucBord07.genreCdName}</td>
					          <td align="left" style="text-align: left; margin-left: 10px;">
					          		${fn:replace(anucBord07.tite, '<', '&lt;')}</a>
					          </td>
					          <td id="anucItem${status.index}" style="text-align: right;">${anucBord07.divsCdName}</td>
					    	    <td class="ce">${anucBord07.anucItem4}</td>
					         </tr>
								<c:if test="${bord07Size < 7}">
									<c:forEach step="1" begin="0" end="${6-bord07Size}">
										<tr>

										</tr>
									</c:forEach>
								</c:if>
							</c:forEach>
							</c:if>
						</tbody>
						</table>
						<div class="message_tp_rt" style="margin-top: 10px;">
							<a href="/statBord/statBo07List.do">더 보기 <strong>+</strong> </a>
						</div>
					</ul>
					<p class="clear">
					</p>
				</div>
				<%-- <a href="#none"><b>보상금 공탁 공고</b></a>
				<div class="message_child_menu disnone">
					<ul>
						<table class="sub_tab3 mar_tp13" cellspacing="0" cellpadding="0" style="margin-left: 2px;" width="100%" summary="보상금 공탁 공고 조회 목록입니다.">
						<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<caption>보상금 공탁 공고</caption>
						<colgroup>
						<col width="15%">
						<col width="55%">
						<col width="15%">
						<col width="15%">
						</colgroup>
						<thead>
						<tr>
							<th scope="col" style="text-align: center;">
								구분
							</th>
							<th scope="col" style="text-align: center;">
								제목
							</th>
							<th scope="col" style="text-align: center;">
								공탁금액
							</th>
							<th scope="col" style="text-align: center;">
								공고일
							</th>
						</tr>
						</thead>
						<tbody>
						<c:if test="${bord05Size == 0}">
									<tr>
										<td class="ce" colspan="3">등록된 게시물이 없습니다.</td>
									</tr>
							</c:if>
					        <c:if test="${bord05Size > 0 }">
					        <c:forEach var="anucBord05" items="${anucBord05}" varStatus="status">
							
							<tr>
					          <td class="ce">${anucBord05.divsCdName}</td>
					          <td align="left" style="text-align: left; margin-left: 10px;">
					          <a href="/statBord/statBo01Detl.do?bordSeqn=${anucBord05.bordSeqn}&amp;bordCd=${anucBord05.bordCd}&amp;divsCd=${anucBord05.divsCd}">
								${fn:replace(anucBord05.tite, '<', '&lt;')}</a>
					          </td>
					          <td id="anucItem${status.index}" style="text-align: right;">${anucBord05.anucItem4}</td>
					    	  <td class="ce">${anucBord05.openDttm}</td>
					         </tr>
								<c:if test="${bord05Size < 7}">
									<c:forEach step="1" begin="0" end="${6-bord05Size}">
										<tr>

										</tr>
									</c:forEach>
								</c:if>
							</c:forEach>
							</c:if>
						</tbody>
						</table>
						<div class="message_tp_rt" style="margin-top: 10px;">
							<a href="/statBord/statBo05List.do?bordCd=5">더 보기 <strong>+</strong> </a>
						</div>
					</ul>
					<p class="clear">
					</p>
				</div> --%>
				</li>
			</ul>
			<p class="clear">
			</p>
		</div>
	</div>
	<div class="con_rt_2">
		<form name="form1" method="post" action="">
			<input type="hidden" name="bordSeqn">
			<input type="hidden" name="menuSeqn">
			<input type="hidden" name="threaded">
			<input type="hidden" name="page_no">
		</form>
		<div class="message_bm_2">
			<ul class="message1_2">
				<li>
				<a href="#none" class="on"><b>공지사항</b></a>
				<div class="message_child_menu_2">
					<ul>
						<table class="ntbd sub_tab3_2" cellspacing="0" cellpadding="0" width="100%" summary="공지사항 조회 목록입니다.">
						<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<caption>공지사항</caption>
						<colgroup>
						<col width="75%">
						<col width="25%">
						</colgroup>
						<thead>
						<tr>
							<th scope="col" style="text-align: center;">
								제목
							</th>
							<th scope="col" style="text-align: center;">
								등록일
							</th>
						</tr>
						</thead>
						<tbody>
							<c:if test="${notiListSize == 0}">
								<tr>
									<td class="ce" colspan="2">등록된 게시물이 없습니다.</td>
								</tr>
						</c:if>
				        <c:forEach var="notiList" items="${notiList}">
						 <tr>
				          <td align="left" style="text-align: left; margin-left: 10px;"><a href="#" onclick="javascript:boardDetail('${notiList.bordSeqn}','${notiList.menuSeqn}','${notiList.threaded}')">
							${fn:replace(notiList.tite, '<', '&lt;')}</a>
						  </td>
				    	  <td class="ce" style="border-right: 1px solid #dddddd;">${notiList.insertDate}</td>
				         </tr>
				         	<c:if test="${notiListSize < 7}">
								<c:forEach step="1" begin="0" end="${6-notiListSize}">
									<tr>

									</tr>
								</c:forEach>
							</c:if>
						</c:forEach>
						</tbody>
						</table>
						<div class="message_tp_rt">
							<a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1">+</a>
						</div>
					</ul>
					<p class="clear">
					</p>
				</div>
				</li>
				<li>
				<a href="#none"><b>자주묻는 질문</b></a>
				<div class="message_child_menu_2 disnone">
					<ul>
						<table class="ntbd sub_tab3_2" cellspacing="0" cellpadding="0" width="100%" summary="자주묻는 질문 조회 목록입니다.">
						<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<caption>자주묻는 질문/caption&gt; </caption><colgroup>
						<col width="75%">
						<col width="25%">
						</colgroup>
						<thead>
						<tr>
							<th scope="col">
								제목
							</th>
							<th scope="col">
								등록일
							</th>
						</tr>
						</thead>
						<tbody>
						<c:if test="${QnABordSize == 0}">
								<tr>
									<td class="ce" colspan="2">등록된 게시물이 없습니다.</td>
								</tr>
						</c:if>
				        <c:forEach var="QnABord" items="${QnABord}">
						 <tr>
				          <td align="left" style="text-align: left; margin-left: 10px;"><a href="#" onclick="javascript:boardDetail('${QnABord.bordSeqn}','${QnABord.menuSeqn}','${QnABord.threaded}')">
							${fn:replace(QnABord.tite, '<', '&lt;')}</a>
						  </td>
				    	  <td class="ce" style="border-right: 1px solid #dddddd;">${QnABord.rgstDttm}</td>
				         </tr>
				         	<c:if test="${QnABordSize < 7}">
								<c:forEach step="1" begin="0" end="${6-QnABordSize}">
									<tr>

									</tr>
								</c:forEach>
							</c:if>
						</c:forEach>
						</tbody>
						</table>
						<div class="message_tp_rt">
							<a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1">+</a>
						</div>
					</ul>
					<p class="clear">
					</p>
				</div>
				</li>
			</ul>
			<p class="clear">
			</p>
		</div>
		<div class="txt_custom">
<!-- 			<p class="tit">상담전화</p>
			<p class="txt1">055-792-0122</p>
			<p class="txt2">평일 09:00~18:00 / 토요일 09:00~13:00</p>
			<p class="txt3">일요일 / 공휴일 휴무</p> -->
		</div>
	</div>
	<p class="clear">
	</p>
</div>
<!-- //main Data -->

<jsp:include page="/include/2017/footer.jsp" />

<!-- </div> -->
<!-- //전체를 감싸는 DIVISION -->
</body>
</html>