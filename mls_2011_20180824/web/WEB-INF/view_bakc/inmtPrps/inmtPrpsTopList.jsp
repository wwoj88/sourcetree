<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="ko">
<head>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>미분배 보상금 대상 저작물 확인 소개 및 이용방법 - 이용방법 | 내권리찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>

<h2 class="sub_con_h2 mar_tp30">최근 3년간 미분배 보상금 금액</h2>
<div class="bg_f8f8f8 mar_tp30">
	<img src="/images/sub_img/sub_40.png" alt="그림" style="margin-right:20px;" />
	
	<%-- <c:forEach var="i" begin="0" end="${fn:length(allt_amnt)}" step="1">
		<span href="#n" class="damages_num"><c:out value="${fn:substring(allt_amnt, i, i + 1)}" /></span>
	</c:forEach> --%>
	<c:forEach var="info" items="${amntList}" varStatus="listStatus">
		<%-- 
		<c:set var="leng" value="${fn:length(info.SUMCNT)}"/>
		<c:forEach var="i" begin="0" end="${leng}" step="1">
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, i, i + 1)}" /></span>
		</c:forEach>
		 --%>
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 0, 1)}"/></span>
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 1, 2)}"/></span>,
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 2, 3)}"/></span>
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 3, 4)}"/></span>
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 4, 5)}"/></span>,
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 5, 6)}"/></span>
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 6, 7)}"/></span>
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 7, 8)}"/></span>,
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 8, 9)}"/></span>
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 9, 10)}"/></span>
		<span href="#n" class="damages_num"><c:out value="${fn:substring(info.SUMCNT, 10,11)}"/></span>&nbsp;&nbsp;원
	</c:forEach>
	
	<!-- 
	<span href="#n" class="damages_num">&nbsp;&nbsp;</span>
	<span href="#n" class="damages_num">&nbsp;&nbsp;</span>
	<span href="#n" class="damages_num">&nbsp;&nbsp;</span>
	,
	<span href="#n" class="damages_num">&nbsp;&nbsp;</span>
	<span href="#n" class="damages_num">1</span>
	<span href="#n" class="damages_num">9</span>
	,
	<span href="#n" class="damages_num">4</span>
	<span href="#n" class="damages_num">8</span>
	<span href="#n" class="damages_num">7</span>
	,
	<span href="#n" class="damages_num">9</span>
	<span href="#n" class="damages_num">8</span>
	<span href="#n" class="damages_num">9</span>
	
	&nbsp;&nbsp;&nbsp;
	원
	 -->
</div>

<h2 class="sub_con_h2 mar_tp30">미분배 보상금 저작물 목록</h2>
<table class="sub_tab td_cen mar_tp15" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
	<caption>한국저작권위원회</caption>
	<colgroup>
		<col width="22%"/>
		<col width="*"/>
		<col width="22%"/>
		<col width="22%"/>
	</colgroup>
	<thead>
		<tr>
			<th scope="row">보상금 종류</th>
			<th scope="row">저작물</th>
			<th scope="row">미분배보상금</th>
			<th scope="row" class="last_bor0">수령단체</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="data" items="${list}">
		<tr>
			<td>${data.KIND}</td>
			<td>${data.NAME}</td>
			<c:set var="priceData" value="${data.ALLT_AMNT}" />
			<td><fmt:formatNumber value="${priceData}" pattern="###,###,###,##0" groupingUsed="true" /> 원</td>
			<td class="last_bor0">
				<c:if test="${data.RGST_ORGN_CODE == '202'}">한국음악실연자연합회</c:if>
				<c:if test="${data.RGST_ORGN_CODE == '203'}">한국음반산업협회</c:if>
				<c:if test="${data.RGST_ORGN_CODE == '205'}">한국복사전송권협회</c:if>
			</td>
		</tr>
		</c:forEach>
	</tbody>
</table>