<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금신청 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 8px 0;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	width: 700px;
	margin-left: 0px;
}
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/2010/prototype.js"> </script>
<script type="text/JavaScript">
<!--

//페이징
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.target = "_self";
	frm.method = "post";
	frm.action = "/inmtPrps/inmtPrps.do?method=muscInmtList";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	var totCnt = cfInsertComma('${inmtList.totalRow}');
	
	if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
		window.parent.document.getElementById("spn_totalRow").innerHTML = totCnt + '건';
	}
}

// 보상금 history오픈 : 부모창에서 오픈
function openInmtPrps (crId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	
	var url = '/inmtPrps/inmtPrps.do?method=inmtPrpsHistList&DIVS=B'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch width=500, height=400';
	
	parent.frames.openDetail(url, name, openInfo);
}

//table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {
	var the_height = document.getElementById(name).offsetHeight;
	document.getElementById(targetName).style.height = the_height+13;
}

function initParameter(){
	resizeDiv("tab_scroll", "div_scroll");
	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifMuscInmtList"); 
	
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
}

if(window.attachEvent){
	window.attachEvent("onload", initParameter);
	window.attachEvent("onload", fn_totalCNT);
}else if(window.addEventListener) {
	window.addEventListener("load", initParameter, false);
	window.addEventListener("load", fn_totalCNT, false);
}else{ 
	window.onload = initParameter;
} 

//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<form name="ifFrm" action="">
			<input type="hidden" name="page_no"/>
			<input type="hidden" name="srchDIVS" value="${srchParam.srchDIVS }"/>
			<input type="hidden" name="srchSdsrName" value="${srchParam.srchSdsrName }"/>
			<input type="hidden" name="srchMuciName" value="${srchParam.srchMuciName }"/>
			<input type="hidden" name="srchYymm" value="${srchParam.srchYymm }"/>
			
			<!-- 테이블 영역입니다 -->
			<div id="div_scroll"  class="tabelRound" style="width:100%;height:300px;padding:0px;">
				<span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span><!-- 라운딩효과 -->
				<table id="tab_scroll"  cellspacing="0" cellpadding="0" border="1" class="grid" summary="방송음악보상금 발생 저작물 목록입니다." style="width:100%"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
					<colgroup>
						<col width="25">
						<col width="*">
						<col width="190">
						<col width="80">
						<col width="80">
						<!-- 
						<col width="70">
						<col width="70">
						<col width="70">
						 -->
						<col width="40">
					</colgroup>
					<thead>
						<tr>
							<th scope="col"><input type="checkbox" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','iChk',this);" title="전체선택"/></th>
							<th scope="col">곡명</th>
							<th scope="col">가수</th>
							<th scope="col">앨범명</th>
							<th scope="col">방송년도</th>
							<!-- 
							<th scope="col">음제협</th>
							<th scope="col">음실연</th>
							<th scope="col">복전협</th>
							-->
							<th scope="col">보상금</th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${empty inmtList.resultList}">
						<tr>
							<td class="ce"colspan="6">검색된 목록이 없습니다.</td>
						</tr>
					</c:if>
					<c:if test="${!empty inmtList.resultList}">
						<c:forEach items="${inmtList.resultList}" var="inmtPrps">
							<c:set var="NO" value="${inmtPrps.totalRow}"/>
							<c:set var="i" value="${i+1}"/>
						<tr>
							<td class="ce">
								<input type="checkbox" name="iChk" value="${inmtPrps.INMT_SEQN}" style="padding:0px;margin:0px;" title="선택" >
								<!-- hidden value start -->
								<input type="hidden" name="inmtSeqn" value="${inmtPrps.INMT_SEQN}"/>
								<input type="hidden" name="sdsrName" value="${inmtPrps.SDSR_NAME}"/>
								<input type="hidden" name="muciName" value="${inmtPrps.MUCI_NAME}"/>
								<input type="hidden" name="yymm" value="${inmtPrps.YYMM}"/>
								<input type="hidden" name="divs" value="${inmtPrps.DIVS}"/>
								<input type="hidden" name="prpsDivs" value="${inmtPrps.PRPS_DIVS}"/>
								<input type="hidden" name="kapp" value="${inmtPrps.KAPP}"/>
								<input type="hidden" name="fokapo" value="${inmtPrps.FOKAPO}"/>
								<input type="hidden" name="krtra" value="${inmtPrps.KRTRA}"/>
								<input type="hidden" name="oferEtpr" value="${inmtPrps.OFER_ETPR}"/>
								<input type="hidden" name="brctCont" value="${inmtPrps.BRCT_CONT}"/>
								<input type="hidden" name="lyriWrtr" value="${inmtPrps.LYRI_WRTR}"/>
								<input type="hidden" name="comsWrtr" value="${inmtPrps.COMS_WRTR}"/>
								<input type="hidden" name="arrgWrtr" value="${inmtPrps.ARRG_WRTR}"/>
								<input type="hidden" name="albmName" value="${inmtPrps.ALBM_NAME}"/>
								<input type="hidden" name="duesCode" value="${inmtPrps.DUES_CODE}"/>
								<input type="hidden" name="dataType" value="${inmtPrps.DATA_TYPE}"/>
								<input type="hidden" name="usexType" value="${inmtPrps.USEX_TYPE}"/>
								<input type="hidden" name="selgYsno" value="${inmtPrps.SELG_YSNO}"/>
								<input type="hidden" name="usexLibr" value="${inmtPrps.USEX_LIBR}"/>
								<input type="hidden" name="ouptPage" value="${inmtPrps.OUPT_PAGE}"/>
								<input type="hidden" name="ctrlNumb" value="${inmtPrps.CTRL_NUMB}"/>
								<input type="hidden" name="lishComp" value="${inmtPrps.LISH_COMP}"/>
								<input type="hidden" name="workCode" value="${inmtPrps.WORK_CODE}"/>
								<input type="hidden" name="caryDivs" value="${inmtPrps.CARY_DIVS}"/>
								<input type="hidden" name="schl" value="${inmtPrps.SCHL}"/>
								<input type="hidden" name="bookDivs" value="${inmtPrps.BOOK_DIVS}"/>
								<input type="hidden" name="bookSizeDivs" value="${inmtPrps.BOOK_SIZE_DIVS}"/>
								<input type="hidden" name="schlYearDivs" value="${inmtPrps.SCHL_YEAR_DIVS}"/>
								<input type="hidden" name="pubcCont" value="${inmtPrps.PUBC_CONT}"/>
								<input type="hidden" name="autrDivs" value="${inmtPrps.AUTR_DIVS}"/>
								<input type="hidden" name="workKind" value="${inmtPrps.WORK_KIND}"/>
								<input type="hidden" name="usexPage" value="${inmtPrps.USEX_PAGE}"/>
								<input type="hidden" name="workDivs" value="${inmtPrps.WORK_DIVS}"/>
								<input type="hidden" name="subjName" value="${inmtPrps.SUBJ_NAME}"/>
								<!-- hidden value end -->
							</td>
							<td>${inmtPrps.SDSR_NAME }</td>
							<td class="ce">${inmtPrps.MUCI_NAME }</td>
							<td class="ce">${inmtPrps.ALBM_NAME }</td>
							<td class="ce">${inmtPrps.YYMM }</td>
							<!-- 
							<td class="ce">${inmtPrps.KAPP }</td>
							<td class="ce">${inmtPrps.FOKAPO }</td>
							<td class="ce">${inmtPrps.KRTRA }</td>
							 -->
							<!-- 보상금 신청여부 -->
							<td class="ce">	
								<c:if test="${inmtPrps.PRPS_CNT >0}">
									<a href="#" onclick="javascript:openInmtPrps('${inmtPrps.INMT_SEQN }');"><img src="/images/common/ic_newWin.gif" class="vtop" alt="보상금신청내역" /></a>
								</c:if>
							</td>
						</tr>
						</c:forEach>
					</c:if>
					</tbody>
				</table>
			</div>

		<!--paging start-->
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			   <jsp:include page="../common/PageList_2010.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${inmtList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
		</form>
	</div>
</body>
</html>
