<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금발생저작물 조회 | 저작권찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<style type="text/css">
<!--

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
.w55{width:55%;}
.necessary{ background: url(/images/2011/common/necessary.gif) no-repeat 98% 2px !important; padding-right: 10px !important;}/* 필수 */
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/javascript" src="/js/2010/prototype.js"> </script>

<script type="text/JavaScript">
<!--
	//목록으로 이동
	function fn_list(){
		var frm = document.frm;
	
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=list&amp;gubun=edit&amp;srchDIVS=${srchParam.srchDIVS }";
		frm.submit();
	}
	
	//뒤로가기
	function fn_prePage(){
		var frm = document.frm;
	
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps";
		frm.submit();
	}
	
	
	//보상금신청
	function fn_chkSubmit(){
		var frm = document.frm;
		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrpsSave";
		frm.submit();
	}
	
	
	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.frm;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;
	
		frm.method = "post";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
	}

	// table name 사이즈에 대한 div targetName리사이즈
	//function resizeDiv(name) {
	//	var the_height = document.getElementById(name).offsetHeight;
	//	document.getElementById(name).style.height = the_height + 13  + "px" ;
	//}
	
	//table name 사이즈에 대한 div targetName리사이즈
	function resizeDiv(name, targetName) {
		var the_height = document.getElementById(name).offsetHeight;
		document.getElementById(targetName).style.height = the_height+13+ "px" ;
	}
	
//-->
</script>
</head>
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2011/header.jsp" />
		<script type="text/javascript">initNavigation(2);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_2.gif" alt="보상금발생저작물 조회 및 신청" title="보상금발생저작물 조회 및 신청" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<!-- CONTENT str-->
			<div class="content">
				<!-- 래프 -->
				<div class="left">
					<ul id="sub_lnb">
					<li id="lnb1"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1"><img src="/images/2011/content/sub_lnb0201_off.gif" title="방송음악" alt="방송음악" /></a></li>
					<li id="lnb2"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2"><img src="/images/2011/content/sub_lnb0202_off.gif" title="교과용" alt="교과용" /></a></li>
					<li id="lnb3"><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3"><img src="/images/2011/content/sub_lnb0203_off.gif" title="도서관" alt="도서관" /></a></li>
					</ul>
					<script type="text/javascript">
						if("${srchParam.srchDIVS}" == '1'){
							subSlideMenu("sub_lnb","lnb1");
						}else if("${srchParam.srchDIVS}" == '2'){
							subSlideMenu("sub_lnb","lnb2");
						}else if("${srchParam.srchDIVS}" == '3'){
							subSlideMenu("sub_lnb","lnb3");
						}
					</script>
				</div>
				<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>보상금발생저작물 조회</span><em>방송음악</em></p>
					<h1><img src="/images/2011/title/content_h1_0802.gif" alt="보상금" title="보상금" /></h1>
					<form name="frm" action="" class="sch">
					<input type="hidden" name="srchDIVS" value="${srchParam.srchDIVS }" />
					<input type="hidden" name="srchSdsrName" value="${srchParam.srchSdsrName }" />
					<input type="hidden" name="srchMuciName" value="${srchParam.srchMuciName }" />
					<input type="hidden" name="srchYymm" value="${srchParam.srchYymm }" />
					<input type="hidden" name="gubun" value="back" />
					<input type="hidden" name="nowPage" value="${srchParam.nowPage }" />
					<input type="hidden" name="filePath">
					<input type="hidden" name="fileName">
					<input type="hidden" name="realFileName">
					<div class="section">
						<!-- 신청에 관한 문의 -->
						<div class="gray_box">
							<div class="box4">
								<div class="box4_con floatDiv">
									<p class="fl ml10"><img src="/images/2011/content/box_img1.gif" alt="" /></p>
									<div class="fl ml20 w85">
										<p class="strong mt5 black">방송음악보상금<strong class="blue2">신청에 관한 문의는 신청분류에 해당하는 단체로 연락바랍니다.</strong></p>
										<ul class="mt15 list2">
										<li>
											<em class="w20">한국음악실연자연합회</em>
											<span class="w20"><img src="/images/2011/common/ic_nm.gif" alt="" />권기태(팀장)</span>
											<span class="w25"><img src="/images/2011/common/ic_tel.gif" alt="" />02-745-8286(6220)</span>
											<span class="w30"><img src="/images/2011/common/ic_mail.gif" alt="" />kjery@fkmp.kr</span>
										</li>
										<li>
											<em class="w20">한국음원제작자협회</em>
											<span class="w20"><img src="/images/2011/common/ic_nm.gif" alt="" />최소정</span>
											<span class="w25"><img src="/images/2011/common/ic_tel.gif" alt="" />02-3270-5933</span>
											<span class="w30"><img src="/images/2011/common/ic_mail.gif" alt="" />csj@kapp.or.kr</span>
										</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!-- //신청에 관한 문의 -->
						
						<!-- article str -->
						<div class="article mt20">
							<div class="floatDiv">
								<h2 class="fl">방송음악 보상금 신청</h2>
							</div>
							<div id="fldInmtInfo"><!-- fieldset 보상금신청 신청자 정보 --> 
								<span class="topLine"></span>
								<!-- 그리드스타일 -->
								<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
									<colgroup>
									<col width="20%">
									<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">보상금종류</th>
											<td>방송음악</td>
										</tr>
										<tr>
											<th scope="row">신청인정보</th>
											<td>
												<span class="topLine2"></span>
												<!-- 그리드스타일 -->
												<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
													<colgroup>
													<col width="15%">
													<col width="35%">
													<col width="25%">
													<col width="*">
													</colgroup>
													<tbody>
														<tr>
															<th scope="row"><label class="necessary">성명</label></th>
															<td>${clientInfo.PRPS_NAME }</td>
															<th scope="row"><label class="necessary">주민번호/</label>사업자번호</th>
															<td>${clientInfo.RESD_CORP_NUMB_VIEW}</td>
														</tr>
														<tr>
															<th scope="row">전화번호</th>
															<td>
																<ul class="list1">
																	<li class="p11"><label for="regi1" class="inBlock w25">자택</label> : ${clientInfo.HOME_TELX_NUMB}</li>
																	<li class="p11"><label for="regi2" class="inBlock w25">사무실</label> : ${clientInfo.BUSI_TELX_NUMB}</li>
																	<li class="p11"><label for="regi3" class="inBlock w25">휴대폰</label> : ${clientInfo.MOBL_PHON}</li>
																</ul>
															</td>
															<th scope="row">팩스번호</th>
															<td>${clientInfo.FAXX_NUMB }</td>
														</tr>
														<tr>
															<th scope="row"><label class="necessary">주소</label></th>
															<td colspan="3">
																<ul class="list1">
																<li class="p11"><label for="txtHomeAddr" class="inBlock w10">자택</label> : ${clientInfo.HOME_ADDR }</li>
																<li class="p11"><label for="txtBusiAddr" class="inBlock w10">사무실</label> : ${clientInfo.BUSI_ADDR}</li>
																</ul>
															</td>
														</tr>
														<tr>
															<th scope="row"><label class="necessary">입금처</label></th>
															<td>
																<ul class="list1">
																	<li class="p11"><label for="txtBankName" class="inBlock w25">은행명</label> : ${clientInfo.BANK_NAME}</li>
																	<li class="p11"><label for="txtAcctNumb" class="inBlock w25">계좌번호</label> : ${clientInfo.ACCT_NUMB}</li>
																	<li class="p11"><label for="txtDptr" class="inBlock w25">예금주</label> : ${clientInfo.DPTR}</li>
																</ul>
															</td>
															<th scope="row">E-Mail</th>
															<td>${clientInfo.MAIL }</td>
														</tr>
													</tbody>
												</table>
												<!-- //그리드스타일 -->
												<!-- 전화면에서 넘어온 값듯 --> 
												<input type="hidden" name="hddnInsertKapp" 	value="${clientInfo.KAPP}" /> 
												<input type="hidden"	name="hddnInsertFokapo" value="${clientInfo.FOKAPO}" /> 
												<input type="hidden" name="hddnUserIdnt" value="${clientInfo.USER_IDNT }" />
												<input type="hidden" name="txtPrpsName"	value="${clientInfo.PRPS_NAME }" /> 
												<input type="hidden"	name="txtPrsdCorpNumbView" value="${clientInfo.RESD_CORP_NUMB }" />
												<input type="hidden" name="txtHomeTelxNumb"	value="${clientInfo.HOME_TELX_NUMB }" /> 
												<input type="hidden"	name="txtBusiTelxNumb" value="${clientInfo.BUSI_TELX_NUMB }" /> 
												<input type="hidden" name="txtMoblPhon" value="${clientInfo.MOBL_PHON }" />
												<input type="hidden" name="txtFaxxNumb" 	value="${clientInfo.FAXX_NUMB }" /> 
												<input type="hidden"	name="txtHomeAddr" value="${clientInfo.HOME_ADDR }" /> 
												<input type="hidden" name="txtBusiAddr" value="${clientInfo.BUSI_ADDR }" />
												<input type="hidden" name="txtBankName" 	value="${clientInfo.BANK_NAME }" /> 
												<input type="hidden"	name="txtAcctNumb" value="${clientInfo.ACCT_NUMB }" /> 
												<input type="hidden" name="txtDptr" value="${clientInfo.DPTR }" /> 
												<input type="hidden" name="txtMail" value="${clientInfo.MAIL }" /> 
												<input type="hidden" name="hddnTrst203" value="${clientInfo.TRST_203 }" />
												<input type="hidden" name="hddnTrst202"	value="${clientInfo.TRST_202 }" /> 
												<input type="hidden"	name="hddnTrst205" value="${clientInfo.TRST_205 }" /> 
												<input type="hidden" name="hddnTrst205_2" value="${clientInfo.TRST_205_2 }" />
												<input type="hidden" name="txtareaPrpsDesc" value="${clientInfo.PRPS_DESC }" />
												<input type="hidden" name="prpsDoblCode"	value="${prpsDoblCode }" />
												<input type="hidden" name="rghtPrpsMastKey"	value="${rghtPrpsMastKey }" />
											</td>
										</tr>
										<tr>
											<th scope="row">신청구분</th>
											<td>
												<ul class="line22">
													<li>
														<input type="checkbox" <c:if test="${clientInfo.KAPP == 'Y'}">checked</c:if> disabled class="inputRChk" title="저작인접권(앨범제작)" id="rght1" />
														<label for="rght1" class="p12">저작인접권(앨범제작)</label>
														<input type="checkbox" <c:if test="${clientInfo.FOKAPO == 'Y'}">checked</c:if> class="inputRChk ml10" disabled title="저작인접권(가창,연주,지휘 등)" id="rght2" />
														<label for="rght2" class="p12">저작인접권(가창,연주,지휘 등)</label>
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<th scope="row">신청저작물정보</th>
											<td>
												<table cellspacing="0" cellpadding="0" border="1" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="10%">
													<col width="*">
													<col width="20%">
													<col width="20%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">순번</th>
															<th scope="col">곡명</th>
															<th scope="col">가수</th>
															<th scope="col">방송년도</th>
														</tr>
													</thead>
													<tbody>
													<c:if test="${!empty tempList}">
													<c:forEach items="${tempList}" var="tempList">
														<c:set var="NO" value="${tempListCnt + 1}" />
														<c:set var="i" value="${i+1}" />
														<tr>
															<td class="ce">
																${NO - i } 
																<!-- 전화면에서 넘어온 값듯 -->
																<input type="hidden" name="hddnSelectInmtSeqn" value="${tempList.INMT_SEQN }" /> 
																<input type="hidden" name="hddnSelectPrpsIdntCode" value="${tempList.PRPS_IDNT_CODE }" />
																<input type="hidden" name="hddnSelectPrpsDivs" value="${tempList.PRPS_DIVS }" /> 
																<input type="hidden" name="hddnSelectKapp" value="${tempList.KAPP }" />
																<input type="hidden" name="hddnSelectFokapo"value="${tempList.FOKAPO }" /> 
																<input type="hidden" name="hddnSelectKrtra" value="${tempList.KRTRA }" /> 
																<input type="hidden" name="hddnSelectSdsrName" value="${tempList.SDSR_NAME }" /> 
																<input type="hidden" name="hddnSelectMuciName" value="${tempList.MUCI_NAME }" /> 
																<input type="hidden" name="hddnSelectYymm" value="${tempList.YYMM }" />
																<input type="hidden" name="hddnSelectLishComp"	value="${tempList.LISH_COMP }" /> 
																<input type="hidden" name="hddnSelectUsexType" value="${tempList.USEX_TYPE }" />
															</td>
															<td>${tempList.SDSR_NAME }</td>
															<td class="ce">${tempList.MUCI_NAME }</td>
															<td class="ce">${tempList.YYMM }</td>
														</tr>
														</c:forEach>
													</c:if>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</div> <!-- //fieldset -->	
							<c:if test="${clientInfo.KAPP =='Y'}">
								<c:if test="${!empty trstList}">
									<c:forEach items="${trstList}" var="trstList">
										<c:if test="${trstList.TRST_ORGN_CODE=='203' }">					
											<!-- 음제협 관련 영역 -->
											<div><!-- fieldSet str -->
												<div class="floatDiv mt20">
													<h3 class="fl">내용(한국음원제작자협회)
														<span class="ml10">
															<input type="checkbox" class="inputChk ml10" <c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if> disabled="disabled" title="오프라인접수(첨부서류)" />
															<label for="chkKappOff" class="orange thin">오프라인접수(첨부서류)</label>
															<input type="hidden" name="hddnKappOff" value="${trstList.OFFX_LINE_RECP }" />
														</span>
													</h3>
												</div>
												<!-- 보상금신청 항목 테이블 영역입니다 -->
												<div id="divKappItemList" class="div_scroll" style="width:735px; padding:0 0 0 0;">
													<table id="tabKappItemList" cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="(한국음원제작자협회)보상금신청 저작물 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
														<colgroup>
														<col width="6%">
														<col width="*">
														<col width="12%">
														<col width="15%">
														<col width="10%">
														<col width="10%">
														<col width="10%">
														<col width="14%">
														</colgroup>
														<thead>
															<tr>
																<th scope="col" rowspan="2">번<br>호</th>
																<th scope="col" rowspan="2">음반명</th>
																<th scope="col" rowspan="2">CD코드</th>
																<th scope="col" rowspan="2">곡명</th>
																<th scope="col">가수명</th>
																<th scope="col">국가명</th>
																<th scope="col">작사가</th>
																<th scope="col" rowspan="2"><label class="necessary white">음악장르</label><br />(테마코드)</th>
															</tr>
															<tr>
																<th scope="col">발매일</th>
																<th scope="col">권리근거</th>
																<th scope="col">작곡가</th>
															</tr>
														</thead>
														<tbody>
															<c:if test="${!empty kappList}">
																<c:forEach items="${kappList}" var="kappList">
																<c:set var="kappNo" value="${kappNo+1}" />
																	<tr>
																		<td rowspan="2" class="pd5">
																			${kappNo } <!-- 전화면에서 넘어온 값듯 -->
																			<input type="hidden" name="hddnKappPrpsIdnt" value="${kappList.PRPS_IDNT }" />
																			<input type="hidden" name="hddnKappPrpsIdntCode" value="${kappList.PRPS_IDNT_CODE }" />
																			<input type="hidden" name="txtKappDiskName" value="${kappList.DISK_NAME }" /> 
																			<input type="hidden" name="txtKappCdCode" value="${kappList.CD_CODE }" />
																			<input type="hidden" name="hddnKappSongName" value="${kappList.SONG_NAME }" /> 
																			<input type="hidden" name="txtKappSnerName" value="${kappList.SNER_NAME }" />
																			<input type="hidden" name="txtKappDateIssu" value="${kappList.DATE_ISSU }" />
																			<input type="hidden" name="txtKappNatuName" value="${kappList.NATN_NAME }" />
																			<input type="hidden" name="txtKappRghtGrnd" value="${kappList.RGHT_GRND }" />
																			<input type="hidden" name="txtKappLyriWrtr" value="${kappList.LYRI_WRTR }" />
																			<input type="hidden" name="txtKappComsWrtr" value="${kappList.COMS_WRTR }" /> 
																			<input type="hidden" name="selKappMuscGnre" value="${kappList.MUSC_GNRE }" />
																		</td>
																		<td rowspan="2" class="pd5">${kappList.DISK_NAME }</td><!-- 음반명  -->
																		<td rowspan="2" class="pd5">${kappList.CD_CODE }</td><!-- cd코드  -->
																		<td rowspan="2" class="pd5 ce">${kappList.SONG_NAME }</td><!-- 곡명 -->
																		<td class="ce pd5">${kappList.SNER_NAME }</td><!-- 가수명 -->
																		<td class="ce pd5">${kappList.NATN_NAME }</td><!-- 국가명 -->
																		<td class="ce pd5">${kappList.LYRI_WRTR }</td><!-- 작사가 -->
																		<td class="ce pd5" rowspan="2"><!-- 음악장르 -->
																			<c:choose>
																				<c:when test="${kappList.MUSC_GNRE=='2' }">일반가요</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='14' }">성인가요</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='3' }">댄스</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='4' }">시낭송</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='5' }">MR</c:when>
																				
																				<c:when test="${kappList.MUSC_GNRE=='6' }">아동음악</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='7' }">발라드</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='8' }">국악</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='9' }">캐롤</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='10' }">기독교</c:when>
																				
																				<c:when test="${kappList.MUSC_GNRE=='11' }">불교</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='12' }">연주곡</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='13' }">가곡</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='1' }">OST</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='15' }">락</c:when>
																				
																				<c:when test="${kappList.MUSC_GNRE=='16' }">일렉트로닉</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='17' }">클래식</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='18' }">힙합</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='19' }">재즈</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='20' }">알앤비/소울</c:when>
																				
																				<c:when test="${kappList.MUSC_GNRE=='21' }">팝</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='22' }">월드뮤직</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='23' }">크로스오버</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='24' }">포크</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='25' }">뉴에이지</c:when>
																				
																				<c:when test="${kappList.MUSC_GNRE=='26' }">로고송</c:when>
																				<c:when test="${kappList.MUSC_GNRE=='99' }">기타</c:when>
																			</c:choose>
																		</td>
																	</tr>
																	<tr>
																		<td class="ce pd5">${kappList.DATE_ISSU }</td><!-- 발매일 -->
																		<td class="ce pd5">${kappList.RGHT_GRND }</td><!-- 권리근거 -->
																		<td class="ce pd5">${kappList.COMS_WRTR }</td><!-- 작곡가 -->
																	</tr>
																</c:forEach>
															</c:if>
														</tbody>
													</table>
												</div>
												<!-- 
												//보상금신청 항목 테이블 영역입니다 --><!--
												<div id="divKappFileList" class="tabelRound mb5" 
													style="display:<c:if test="${!empty trstList}">
													<c:forEach items="${trstList}" var="trstList">
														<c:if test="${trstList.TRST_ORGN_CODE=='203' }">
															<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
														</c:if>
													</c:forEach>
													</c:if>;">
												</div>
												-->
												<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
												<div id="divKappFileList" style="display:<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>;">
													<table cellspacing="0" cellpadding="0" border="1" class="grid" style="margin-top:1px;" summary="(한국음원제작자협회)보상금신청 첨부파일 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
														<colgroup>
														<col width="20%">
														<col width="*">
														</colgroup>
														<tbody>
															<tr>
																<th scope="col"><label class="necessary">첨부파일</label></th>
																<td>
																	<c:if test="${!empty fileList}">
																	<c:set var="fileCnt" value="0" />
																		<c:forEach items="${fileList}" var="fileList">
																			<c:if test="${fileList.TRST_ORGN_CODE=='203' }">
																			<c:set var="fileCnt" value="${fileCnt+1}" />
																			${fileList.FILE_NAME}
																			<input type="hidden" name="hddnGetRealFileName" value="${fileList.REAL_FILE_NAME }" />
																			<input type="hidden" name="hddnGetFilePath" value="${fileList.FILE_PATH }" />
																			<input type="hidden" name="hddnGetFileName" value="${fileList.FILE_NAME }" />
																			<input type="hidden" name="hddnGetFileSize" value="${fileList.FILE_SIZE }" />
																			<input type="hidden" name="hddnGetFileOrgnCode" value="${fileList.TRST_ORGN_CODE }" />
																			<br>
																			</c:if>
																		</c:forEach>
																	</c:if>
																	<c:if test="${fileCnt < 1}">
																		파일이 없습니다.
																	</c:if>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
											</div> <!-- //fieldset -->
										</c:if>
									</c:forEach>
								</c:if>
							</c:if>
							<c:if test="${clientInfo.FOKAPO =='Y'}">
								<c:if test="${!empty trstList}">
									<c:forEach items="${trstList}" var="trstList">
										<c:if test="${trstList.TRST_ORGN_CODE=='202' }">
											<!-- 실연자 관련 영역 -->
											<div id="fldFokapo">
												<div class="floatDiv mt20">
													<h3 class="fl">내용(한국음악실연자연합회)
														<span class="ml10">
															<input type="checkbox" class="inputChk ml10" <c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if> disabled="disabled" title="오프라인접수(첨부서류)" />
															<label for="chkKappOff" class="orange thin">오프라인접수(첨부서류)</label>
															<input type="hidden" name="hddnFokapoOff" value="${trstList.OFFX_LINE_RECP }" /> 
														</span>
													</h3>
												</div>
												<!-- 보상금신청 실연자정보 테이블 영역입니다 -->
												<div id="divFokapoInmtInfo" class="mb5">
													<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="(한국음악실연자연합회)보상금신청 실연자정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
														<colgroup>
														<col width="20%">
														<col width="30%">
														<col width="20%">
														<col width="*">
														</colgroup>
														<tbody>
															<tr>
																<th scope="col"><label class="necessary">실연자</label></th>
																<td>
																	<ul class="list1">
																		<li class="p11"><label for="regi1" class="inBlock w25">본명</label> : ${pemrRlnm}</li>
																		<li class="p11"><label for="regi2" class="inBlock w25">예명</label> : ${pemrStnm}</li>
																		<li class="p11"><label for="regi3" class="inBlock w25">그룹명</label> : ${grupName}</li>
																	</ul>
																</td>
																<th scope="col"><label class="necessary">주민등록번호</label></th>
																<td>
																 	<input type="text" name="txtFokapoResdNumb_1" value="${resdNumbStr_1 }" title="실연자 주민번호(1번째)" class="whiteR w20" />
															    	-
															    	<input type="text" name="txtFokapoResdNumb_2" value="${resdNumbStr_2 }" title="실연자 주민번호(2번째)" class="whiteL w20" />
																	<!-- 전화면에서 넘어온 값듯 -->
																	<input type="hidden" name="txtFokapoPemrRlnm" value="${pemrRlnm }" />
																	<input type="hidden" name="txtFokapoPemrStnm" value="${pemrStnm }" />
								 									<input type="hidden" name="txtFokapoGrupName" value="${grupName }" />
																	<input type="hidden" name="txtFokapoResdNumb" value="${resdNumbStr }" />
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<!-- //보상금신청 실연자정보 테이블 영역입니다 -->
												
												<!-- 보상금신청 항목 테이블 영역입니다 -->
												<div id="divFokapoItemList" class="div_scroll" style="width:735px;">
													<table id="tabFokapoItemList" cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="(한국음악실연자연합회)보상금신청 저작물 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
														<colgroup>
														<col width="6%">
														<col width="*">
														<col width="11%">
														<col width="13%">
														<col width="11%">
														<col width="11%">
														<col width="13%">
														<col width="8%">
														</colgroup>
														<thead>
															<tr>
																<th scope="col">번호</th>
																<th scope="col"><label class="necessary">작품명</label></th>
																<th scope="col"><label class="necessary">가수명</label></th>
																<th scope="col">앨범명</th>
																<th scope="col">앨범발매일</th>
																<th scope="col">실연종류</th>
																<th scope="col">실연내용</th>
																<th scope="col">기여율(%)</th>
															</tr>
														</thead>
														<tbody>
															<c:if test="${!empty fokapoList}">
																<c:forEach items="${fokapoList}" var="fokapoList">
																	<c:set var="fokapoNo" value="${fokapoNo+1}" />
																	<tr>
																		<td class="ce pd5">
																		${fokapoNo }
																		<!-- 전화면에서 넘어온 값듯 -->
																		<input type="hidden" name="hddnFokapoPrpsIdnt"
																			value="${fokapoList.PRPS_IDNT }" />
																		<input type="hidden" name="hddnFokapoPrpsIdntCode"
																			value="${fokapoList.PRPS_IDNT_CODE }" />
																		<input type="hidden" name="hddnFokapoPdtnName"
																			value="${fokapoList.PDTN_NAME }" />
																		<input type="hidden" name="txtFokapoSnerName"
																			value="${fokapoList.SNER_NAME }" />
																		<input type="hidden" name="txtFokapoAbumName"
																			value="${fokapoList.ABUM_NAME }" />
																		<input type="hidden" name="txtFokapoAbumDateIssu"
																			value="${fokapoList.ABUM_DATE_ISSU }" />
																		<input type="hidden" name="txtFokapoPemsKind"
																			value="${fokapoList.PEMS_KIND }" />
																		<input type="hidden" name="txtFokapoPemsDesc"
																			value="${fokapoList.PEMS_DESC }" />
																		<input type="hidden" name="txtFokapoCtbtRate"
																			value="${fokapoList.CTBT_RATE }" />
																		</td>
																		<td>${fokapoList.PDTN_NAME }</td>
																		<td class="ce pd5">${fokapoList.SNER_NAME }</td>
																		<td>${fokapoList.ABUM_NAME }</td>
																		<td class="ce pd5">${fokapoList.ABUM_DATE_ISSU }</td>
																		<td class="ce pd5">${fokapoList.PEMS_KIND }</td>
																		<td class="ce pd5">${fokapoList.PEMS_DESC }</td>
																		<td class="rgt pd5">${fokapoList.CTBT_RATE }</td>
																	</tr>
																</c:forEach>
															</c:if>
														</tbody>
													</table>
												</div>
												<!-- //보상금신청 항목 테이블 영역입니다 -->
												
												<!-- 보상금신청 첨부파일 테이블 영역입니다 
												<div id="divFokapoFileList" class="tabelRound mb5 mt10" style="display:
												<c:if test="${!empty trstList}">
													<c:forEach items="${trstList}" var="trstList">
														<c:if test="${trstList.TRST_ORGN_CODE=='202' }">
															<c:if test="${trstList.OFFX_LINE_RECP=='Y' }">none</c:if>
														</c:if>
													</c:forEach>
												</c:if>;">
												</div>
												 //보상금신청 첨부파일 테이블 영역입니다 
												-->
												
												<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
												<div class="mt10">
													<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
														<colgroup>
														<col width="20%">
														<col width="*">
														</colgroup>
														<tbody>
															<tr>
																<th scope="col"><label class="necessary">첨부파일</label></th>
																<td>
																	<c:if test="${!empty fileList}">
																	<c:set var="fileCnt" value="0" />
																		<c:forEach items="${fileList}" var="fileList">
																			<c:if test="${fileList.TRST_ORGN_CODE=='202' }">
																			<c:set var="fileCnt" value="${fileCnt+1}" />
																			${fileList.FILE_NAME}
																			<input type="hidden" name="hddnGetRealFileName"
																				value="${fileList.REAL_FILE_NAME }" />
																			<input type="hidden" name="hddnGetFilePath"
																				value="${fileList.FILE_PATH }" />
																			<input type="hidden" name="hddnGetFileName"
																				value="${fileList.FILE_NAME }" />
																			<input type="hidden" name="hddnGetFileSize"
																				value="${fileList.FILE_SIZE }" />
																			<input type="hidden" name="hddnGetFileOrgnCode"
																				value="${fileList.TRST_ORGN_CODE }" />
																			<br>
																			</c:if>
																		</c:forEach>
																	</c:if>
																	<c:if test="${fileCnt < 1}">
																		파일이 없습니다.
																	</c:if>
																	<c:if test="${empty fileList}">
																		파일이 없습니다.
																	</c:if>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
											</div><!-- //fieldset -->
											<!-- 실연자 관련 영역 -->
										</c:if>
									</c:forEach>
								</c:if>
							</c:if>
							<div id="fldOffline" class="mt10"><!-- fieldeset보상금신청 내용-->
								<!-- 보상금신청(내용) 테이블 영역입니다 -->
								<div id="divContent" class="tabelRound mb5" style="width:735px;">
									<span class="topLine"></span>
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="보상금신청 내용입력 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><label for="txtareaPrpsDesc" class="necessary">내용</label></th>
												<td scope="col">
													${clientInfo.PRPS_DESC }
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div><!-- //fieldset -->
							<div class="btnArea">
								<p class="fl mr5"><span class="button medium gray"><a href="#" onclick="javascript:fn_list();">목록</a></span></p>
								<p class="fl"><span class="button medium gray"><a href="javascript:fn_prePage();">이전화면</a></span></p>
								<p class="fr"><span class="button medium"><a href="#" onclick="javascript:fn_chkSubmit();">보상금 신청</a></span>
							</div>
						</div>
						<!-- //article end -->
					</div>
				</form>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>	
			<!-- CONTENT end-->
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
		<jsp:include page="/include/2011/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<c:if test="${clientInfo.KAPP =='Y'}">
<script type="text/javascript">
<!--
	window.onload	= function(){	
									resizeDiv('tabKappItemList', 'divKappItemList');
								}
//-->
</script>
</c:if>

<c:if test="${clientInfo.FOKAPO =='Y'}">
<script type="text/javascript">
<!--
	window.onload	= function(){	
		resizeDiv('tabFokapoItemList', 'divFokapoItemList');
	}
//-->
</script>
</c:if>

</body>
</html>