<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>이미지 미리보기 | 교과용 보상금신청</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
</head>

<body oncontextmenu="return false">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop">
	
		<!-- Header str -->
		<div id="popHeader">
			<h1>이미지 미리보기</h1>
		</div>
		<!-- //Header end -->
		
		<!-- CONTAINER str-->
		<div id="popContents">
			
			<div class="section">
				<span class="topLine"></span>
				<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="이미지 미리보기">
					<colgroup>
					<col width="20%">
					<col width="30%">
					<col width="20%">
					<col width="*">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row">저작물명</th>
							<td>${imgTitle}</td>
						</tr>
						<tr>
							<td colspan="2"><img src=${filePath} alt="${imgTitle}" /></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="" /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
