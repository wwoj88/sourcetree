<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>

<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>보상금발생저작물 조회 | 저작권찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<style type="text/css">
<!--

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
.w55{width:55%;}
.necessary{ background: url(/images/2012/common/necessary.gif) no-repeat 98% 2px !important; padding-right: 10px !important;}/* 필수 */
-->
</style>
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>


<script type="text/JavaScript">
<!--
	//목록으로 이동
	function fn_list(){
		var frm = document.frm;
	
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=list&amp;gubun=edit&amp;srchDIVS=${srchParam.srchDIVS }";
		frm.submit();
	}
	
	//뒤로가기
	function fn_prePage(){
		var frm = document.frm;
	
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrps&amp;gubun=&amp;srchDIVS=${srchParam.srchDIVS }";
		frm.submit();
	}
	
	
	//보상금신청
	function fn_chkSubmit(){
		var frm = document.frm;
		
		frm.target = "_self";
		frm.method = "post";
		frm.action = "/inmtPrps/inmtPrps.do?method=inmtPrpsSave";
		frm.submit();
	}
	
	
	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.frm;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;
	
		frm.method = "post";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
	}		
//-->
</script>
</head>
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader2.jsp" /> --%>
		<script type="text/javascript">initNavigation(2);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis2.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_2.gif" alt="내권리찾기" title="내권리찾기" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<!-- CONTENT str-->
			<div class="content">
			<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu02.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb2");</script>
			<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>내권리찾기</span><em>미분배보상금 신청</em></p>
					<h1><img src="/images/2012/title/content_h1_0202.gif" alt="미분배보상금 신청" title="미분배보상금 신청" /></h1>
					<form name="frm" action="#" class="sch">
						<input type="hidden" name="srchDIVS"		value="${srchParam.srchDIVS }"/>
						<input type="hidden" name="srchSdsrName"	value="${srchParam.srchSdsrName }" />
						<input type="hidden" name="srchMuciName"	value="${srchParam.srchMuciName }" />
						<input type="hidden" name="srchYymm"		value="${srchParam.srchYymm }" />
						<input type="hidden" name="gubun"			value="back" />
						<input type="hidden" name="nowPage"			value="${srchParam.nowPage }" />
						
						<input type="hidden" name="filePath">
						<input type="hidden" name="fileName">
						<input type="hidden" name="realFileName">
						
						<input type="submit" style="display:none;">
					<div class="section">
					
						<!-- Tab str -->
                          <ul id="tab11" class="tab_menuBg">
                              <li class="first"><a href="#">소개</a></li>
                              <li><a href="#">이용방법</a></li>
                              <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악</a></li>
							  <li class="on"><strong><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용</a></strong></li>
                              <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관</a></li>
                      		</ul>
                    	<!-- //Tab -->
						<!-- memo 삽입 -->
						<jsp:include page="/common/memo/2011/memo_01.jsp">
							<jsp:param name="DIVS" value="${srchParam.srchDIVS}"/>
						</jsp:include>
						<!-- // memo 삽입 -->
						<!-- article str -->
						<div class="article mt20">
							<div class="floatDiv">
								<h2 class="fl">교과용 미분배보상금 신청</h2>
							</div>
							<div id="fldInmtInfo"><!-- fieldset 보상금신청 신청자 정보 --> 
								<span class="topLine"></span>
								<!-- 그리드스타일 -->
								<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
									<colgroup>
									<col width="20%">
									<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">보상금종류</th>
											<td>교과용</td>
										</tr>
										<tr>
											<th scope="row">신청인정보</th>
											<td>
												<span class="topLine2"></span>
												<!-- 그리드스타일 -->
												<table cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="">
													<colgroup>
													<col width="14%">
													<col width="34%">
													<col width="28%">
													<col width="*">
													</colgroup>
													<tbody>
														<tr>
															<th scope="row">성명</th>
															<td>${clientInfo.PRPS_NAME }</td>
															<th scope="row">주민등록번호/사업자번호</th>
															<td>${clientInfo.RESD_CORP_NUMB }</td>
														</tr>
														<tr>
															<th scope="row">전화번호</th>
															<td>
																<ul class="list1">
																	<li class="p11"><label class="inBlock w25">자택</label> : ${clientInfo.HOME_TELX_NUMB }</li>
																	<li class="p11"><label class="inBlock w25">사무실</label> : ${clientInfo.BUSI_TELX_NUMB }</li>
																	<li class="p11"><label class="inBlock w25">휴대폰</label> : ${clientInfo.MOBL_PHON }</li>
																</ul>
															</td>
															<th scope="row">팩스번호</th>
															<td>${clientInfo.FAXX_NUMB }</td>
														</tr>
														<tr>
															<th scope="row">주소</th>
															<td colspan="3">
																<ul class="list1">
																<li class="p11"><label class="inBlock w10">자택</label> : ${clientInfo.HOME_ADDR }</li>
																<li class="p11"><label class="inBlock w10">사무실</label> : ${clientInfo.BUSI_ADDR }</li>
																</ul>
															</td>
														</tr>
														<tr>
															<th scope="row">입금처</th>
															<td>
																<ul class="list1">
																	<li class="p11"><label class="inBlock w30">은행명</label> : ${clientInfo.BANK_NAME}</li>
																	<li class="p11"><label class="inBlock w30">계좌번호</label> : ${clientInfo.ACCT_NUMB }</li>
																	<li class="p11"><label class="inBlock w30">예금주</label> : ${clientInfo.DPTR }</li>
																</ul>
															</td>
															<th scope="row">E-Mail</th>
															<td>${clientInfo.MAIL }</td>
														</tr>
													</tbody>
												</table>
												<!-- //그리드스타일 -->
												<!-- 전화면에서 넘어온 값듯 -->
												<input type="hidden" name="hddnInsertKrtra"		value="<c:if test="${clientInfo.TRST_205 == '1'}">Y</c:if>" />
												<input type="hidden" name="hddnUserIdnt"		value="${clientInfo.USER_IDNT }"/>
												<input type="hidden" name="txtPrpsName"			value="${clientInfo.PRPS_NAME }"/>
												<input type="hidden" name="txtPrsdCorpNumbView"	value="${clientInfo.RESD_CORP_NUMB }"/>
												<input type="hidden" name="txtHomeTelxNumb"		value="${clientInfo.HOME_TELX_NUMB }"/>
												<input type="hidden" name="txtBusiTelxNumb"		value="${clientInfo.BUSI_TELX_NUMB }"/>
												<input type="hidden" name="txtMoblPhon"			value="${clientInfo.MOBL_PHON }"/>
												<input type="hidden" name="txtFaxxNumb"			value="${clientInfo.FAXX_NUMB }"/>
												<input type="hidden" name="txtHomeAddr"			value="${clientInfo.HOME_ADDR }"/>
												<input type="hidden" name="txtBusiAddr"			value="${clientInfo.BUSI_ADDR }"/>
												<input type="hidden" name="txtBankName"			value="${clientInfo.BANK_NAME }"/>
												<input type="hidden" name="txtAcctNumb"			value="${clientInfo.ACCT_NUMB }"/>
												<input type="hidden" name="txtDptr"				value="${clientInfo.DPTR }"/>
												<input type="hidden" name="txtMail"				value="${clientInfo.MAIL }"/>
												<input type="hidden" name="hddnTrst203"			value="${clientInfo.TRST_203 }"/>
												<input type="hidden" name="hddnTrst202"			value="${clientInfo.TRST_202 }"/>
												<input type="hidden" name="hddnTrst205"			value="${clientInfo.TRST_205 }"/>
												<input type="hidden" name="hddnTrst205_2"		value="${clientInfo.TRST_205_2 }"/>
												<input type="hidden" name="txtareaPrpsDesc"		value="${clientInfo.PRPS_DESC }"/>
											   	<input type="hidden" name="prpsDoblCode"	value="${prpsDoblCode }" />
												<input type="hidden" name="rghtPrpsMastKey"	value="${rghtPrpsMastKey }" />	
											</td>
										</tr>
										<tr>
											<th scope="row">신청구분</th>
											<td>
												<ul class="line22">
													<li>
														<input type="checkbox" title="교과용 보상금" <c:if test="${clientInfo.TRST_205 == '1'}">checked</c:if> disabled class="inputRChk" id="rght1" /><label for="rght1" class="p12">교과용 보상금</label>
													</li>
												</ul>
											</td>
										</tr>
										<tr>
											<th scope="row">신청저작물정보</th>
											<td>
												<table cellspacing="0" cellpadding="0" border="1" class="grid" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="10%">
													<col width="*">
													<col width="25%">
													<col width="15%">
													</colgroup>
													<thead>
														<tr>
															<th scope="col">순번</th>
															<th scope="col">도서명</th>
															<th scope="col">저자</th>
															<th scope="col">사용년도</th>
														</tr>
													</thead>
													<tbody>
													<c:if test="${!empty tempList}">
														<c:forEach items="${tempList}" var="tempList">	
															<c:set var="NO" value="${tempListCnt + 1}"/>
															<c:set var="i" value="${i+1}"/>
														<tr>
															<td class="ce">
																${NO - i }
																<!-- 전화면에서 넘어온 값듯 -->
																<input type="hidden" name="hddnSelectInmtSeqn"	value="${tempList.INMT_SEQN }"/>
																<input type="hidden" name="hddnSelectPrpsIdntCode" value="${tempList.PRPS_IDNT_CODE }" />
																<input type="hidden" name="hddnSelectPrpsDivs"	value="${tempList.PRPS_DIVS }"/>
																<input type="hidden" name="hddnSelectKapp"		value="${tempList.KAPP }"/>
																<input type="hidden" name="hddnSelectFokapo"	value="${tempList.FOKAPO }"/>
																<input type="hidden" name="hddnSelectKrtra"		value="${tempList.KRTRA }"/>
																<input type="hidden" name="hddnSelectSdsrName"	value="${tempList.SDSR_NAME }"/>
																<input type="hidden" name="hddnSelectMuciName"	value="${tempList.MUCI_NAME }"/>
																<input type="hidden" name="hddnSelectYymm"		value="${tempList.YYMM }"/>
																<input type="hidden" name="hddnSelectLishComp"	value="${tempList.LISH_COMP }"/>
																<input type="hidden" name="hddnSelectUsexType"	value="${tempList.USEX_TYPE }"/>
															</td>
															<td>${tempList.SDSR_NAME }</td>
															<td class="ce">${tempList.MUCI_NAME }</td>
															<td class="ce">${tempList.YYMM }</td>
														</tr>
													</c:forEach>
													</c:if>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</div> <!-- //fieldset -->	
							<c:if test="${!empty trstList}">
								<c:forEach items="${trstList}" var="trstList">
									<c:if test="${trstList.TRST_ORGN_CODE=='205' }">
										<!-- 실연자 관련 영역 -->
										<div>
											<div class="floatDiv mt20">
												<h3 class="fl">내용(한국복사전송권협회) 교과용 보상금 
													<span class="ml10">
														<input type="checkbox" id="chkKappOff" class="inputChk ml10" <c:if test="${trstList.OFFX_LINE_RECP=='Y' }">checked="checked"</c:if> disabled="disabled" title="오프라인접수(첨부서류)" />
														<label for="chkKappOff" class="orange thin">오프라인접수(첨부서류)</label>
														<input type="hidden" name="hddnKrtraOff" value="${trstList.OFFX_LINE_RECP }"/> 
													</span>
												</h3>
											</div>
											<!-- 보상금신청 실연자정보 테이블 영역입니다 -->
											<div id="divFokapoInmtInfo">
												<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="보상금신청 권리자정보 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="20%">
													<col width="30%">
													<col width="20%">
													<col width="*">
													</colgroup>
													<tbody>
														<tr>
															<th scope="col">권리자</th>
															<td>
																<ul class="list1">
																	<li class="p11"><label class="inBlock w25">본명</label> : ${pemrRlnm}</li>
																	<li class="p11"><label class="inBlock w25">필명</label> : ${grupName }</li>
																	<li class="p11"><label class="inBlock w25">예명</label> : ${pemrStnm }</li>
																</ul>
															</td>
															<th scope="col">주민등록번호</th>
															<td>
																<%-- <input type="text" name="txtKrtraResdNumb_1" maxlength="6" Size="6" onkeypress="javascript:only_arabic(event);" value="${resdNumbStr_1 }" title="권리자(주민번호)" class="inputDataN w40" /> --%>
																${resdNumbStr_1 }
																 - *******
																<%-- <input type="password" name="txtKrtraResdNumb_2" maxlength="7" Size="7" onkeypress="javascript:only_arabic(event);" value="${resdNumbStr_2 }" title="권리자(주민번호)" class="inputDataN w40" /> --%>
																<!-- 전화면에서 넘어온 값듯 -->
														    	<input type="hidden" name="txtKrtraPemrRlnm"	value="${pemrRlnm }"/>
														    	<input type="hidden" name="txtKrtraPemrStnm"	value="${pemrStnm }"/>
														    	<input type="hidden" name="txtKrtraGrupName"	value="${grupName }"/>
														    	<input type="hidden" name="txtKrtraResdNumb"	value="${resdNumbStr }"/>
														    	<input type="hidden" name="txtKrtraResdNumb_1"	value="${resdNumbStr_1 }"/>
														    	<input type="hidden" name="txtKrtraResdNumb_2"	value="${resdNumbStr_2 }"/>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											<!-- //보상금신청 실연자정보 테이블 영역입니다 -->
											<!-- 보상금신청 항목 테이블 영역입니다 -->
											<div id="divFokapoItemList" class="div_scroll" style="width:726px;">
												<table cellspacing="0" cellpadding="0" border="1" class="grid mt10 tableFixed" summary="보상금신청 저작물 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
														<col width="5%">
														<col width="8%">
														<col width="*">
														<col width="13%">
														<col width="8%">
														<col width="10%">
														<col width="10%">
														<col width="10%">
														<col width="12%">
														</colgroup>
													<thead>
														<tr>
															<th scope="col" rowspan="2">번호</th>
															<th scope="col" rowspan="2">저작물<br>종류</th>
															<th scope="col" rowspan="2">저작물명</th>
															<th scope="col" rowspan="2">저작자명</th>
															<th scope="col" colspan="5">이용현황</th>
														</tr>
														<tr>
															<th scope="col">학교급<br>및 학교</th>
															<th scope="col">발행년도<br>및 학기</th>
															<th scope="col">교과목</th>
															<th scope="col">출판사</th>
															<th scope="col">이용페이지</th>
														</tr>
													</thead>
													<tbody>
													<c:if test="${!empty krtraList}">
														<c:forEach items="${krtraList}" var="krtraList">	
															<c:set var="krtraNo" value="${krtraNo+1}"/>
														<tr>
				    										<td class="ce">
				    											${krtraNo }
																<!-- 전화면에서 넘어온 값듯 -->
																<input type="hidden" name="hddnKrtraPrpsIdnt" 	value="${krtraList.INMT_SEQN }" />
																<input type="hidden" name="hddnKrtraPrpsDivs" 	value="${krtraList.PRPS_DIVS }" />
																<input type="hidden" name="hddnKrtraPrpsIdntCode" 	value="${krtraList.PRPS_IDNT_CODE }" />
															
																<input type="hidden" name="hddnKrtraCoprKind"	value="${krtraList.COPT_KIND }" />
																<input type="hidden" name="hddnKrtraWorkName"	value="${krtraList.WORK_NAME }" />
																<input type="hidden" name="hddnKrtraWrtrName"	value="${krtraList.WRTR_NAME }" />
																<input type="hidden" name="hddnKrtraScyr"		value="${krtraList.SCYR }" />
																<input type="hidden" name="hddnKrtraSctr"		value="${krtraList.SCTR }" />
																<input type="hidden" name="hddnKrtraSjet"		value="${krtraList.SJET }" />
																<input type="hidden" name="hddnKrtraBookCncn"	value="${krtraList.BOOK_CNCN }" />
																<input type="hidden" name="hddnKrtraWorkDivs"	value="${krtraList.WORK_DIVS }" />
															</td>
				    										<td class="ce">${krtraList.COPT_KIND }</td>
				    										<td>${krtraList.WORK_NAME }</td>
				    										<td>${krtraList.WRTR_NAME }</td>
				    										<td class="ce">${krtraList.SCYR }</td>
				    										<td class="ce">${krtraList.SCTR }</td>
				    										<td class="ce">${krtraList.SJET }</td>
				    										<td class="rgt">${krtraList.BOOK_CNCN }</td>
				    										<td class="rgt">${krtraList.WORK_DIVS }</td>
				    									</tr>
														</c:forEach>
													</c:if>
				    								</tbody>
				    							</table>
											</div>
											<!-- //보상금신청 항목 테이블 영역입니다 -->
											
											
											<!-- 보상금신청 첨부파일 테이블 영역입니다 -->
											<div id="divFokapoFileList">
												<table cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary=""><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup>
													<col width="20%">
													<col width="*">
													</colgroup>
													<tbody>
														<tr>
															<th scope="col">첨부파일</th>
															<td>
																<c:if test="${!empty fileList}">
																<c:set var="fileCnt" value="0" />
																	<c:forEach items="${fileList}" var="fileList">
																		<c:if test="${fileList.TRST_ORGN_CODE=='205' }">
																		<c:set var="fileCnt" value="${fileCnt+1}" />
																		${fileList.FILE_NAME}
																		<input type="hidden" name="hddnGetRealFileName"
																			value="${fileList.REAL_FILE_NAME }" />
																		<input type="hidden" name="hddnGetFilePath"
																			value="${fileList.FILE_PATH }" />
																		<input type="hidden" name="hddnGetFileName"
																			value="${fileList.FILE_NAME }" />
																		<input type="hidden" name="hddnGetFileSize"
																			value="${fileList.FILE_SIZE }" />
																		<input type="hidden" name="hddnGetFileOrgnCode"
																			value="${fileList.TRST_ORGN_CODE }" />
																		<br>
																		</c:if>
																	</c:forEach>
																</c:if>
																<c:if test="${fileCnt < 1}">
																	파일이 없습니다.
																</c:if>
																<c:if test="${empty fileList}">
																	파일이 없습니다.
																</c:if>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											<!-- //보상금신청 첨부파일 테이블 영역입니다 -->
										</div><!-- //fieldset -->
										<!-- 실연자 관련 영역 -->
									</c:if>
								</c:forEach>
							</c:if>
							<div id="fldOffline" class="mt10"><!-- fieldeset보상금신청 내용-->
								<!-- 보상금신청(내용) 테이블 영역입니다 -->
								<div id="divContent" class="tabelRound mb5" style="width:726px;">
									<span class="topLine"></span>
									<table cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="보상금신청 내용입력 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
										<col width="20%">
										<col width="*">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col">내용</th>
												<td scope="col">
													${clientInfo.PRPS_DESC }
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div><!-- //fieldset -->
							<div class="btnArea">
								<p class="fl mr5"><span class="button medium gray"><a href="#1" onclick="javascript:fn_list();">목록</a></span></p>
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_prePage();">이전화면</a></span></p>
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_chkSubmit();">보상금 신청</a></span>
							</div>
						</div>
						<!-- //article end -->
					</div>
					</form>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>	
			<!-- CONTENT end-->
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>