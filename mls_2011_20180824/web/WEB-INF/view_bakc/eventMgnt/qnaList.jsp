<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권 찾기 이벤트 Q&A | 알림마당 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
function goPages(pageNo) {
	  location.href = '/eventMgnt/qnaList.do?nowPage='+pageNo;
}

function keyCheck(key){
	if(event.keyCode == 13){
		findFormSubmit();
	}
}

function writeForm(){
	location.href = '/eventMgnt/qnaRegi.do?nowPage='+'${vo.nowPage}';
}

window.onload = function(){
	var comp = '${param.comp}';
	if(comp == 'N'){
		alert("비공개 게시물입니다.");
		return;
	}else if(comp == 'KIN'){
		alert("로그인 하세요.");
		return;
	}
}
//-->
</script>
</head>
 
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
 
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/header.jsp" /> --%>
		<!-- <script type="text/javascript">initNavigation(5);</script> -->
		<!-- CONTAINER str-->
		<div id="container" class="container_vis5">
			<div class="container_vis">
				<h2><img src="/images/2012/title/container_vis_h2_5.gif" alt="알림마당" title="알림마당" /></h2>
			</div>
 
			<div class="content">
			
				<!-- 래프 -->																														
				<jsp:include page="/include/2012/leftMenu05.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb5");</script>
				<!-- //래프 -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					
					<p class="path"><span>Home</span><span>알림마당</span> <em>저작권찾기 이벤트</em> </p>
					<h1><img src="/images/2012/title/content_h1_0505.gif" alt="저작권찾기이벤트" title="저작권찾기이벤트" /></h1>
					
					<div class="section">
						<!-- 상단탭 -->
						<c:import url="/eventMgnt/topMenu.do" charEncoding="EUC-KR">
            				<c:param name="menuno" value="3"/>
            			</c:import>
						<!-- 상단탭 -->
						<!-- 그리드 -->
						<table cellpadding="0" cellspacing="0" border="1" summary="이벤트 Q&amp;A 목록" class="grid mt20 tableFixed"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<colgroup><col width="10%"><col width="*"><col width="14%"><col width="18%"></colgroup>
							<thead>
								<tr>
									<th scope="row">번호</th>
									<th scope="row">제목</th>
									<th scope="row">작성자</th>
									<th scope="row">작성일</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list}" var="s">
								<tr>
									<td class="ce">${(vo.totalRow+1)-s.rnum}</td>
									<td><a href="/eventMgnt/qnaDetail.do?menu_seqn=${s.menu_seqn}&amp;bord_seqn=${s.bord_seqn}&amp;threaded=${s.threaded}&amp;nowPage=${vo.nowPage}">${s.tite}
										<c:if test="${s.open_ysno eq 'N'}">
											<img src="/images/2012/common/ic_key.gif" alt="비공개" />
										</c:if>
									</a></td>
									<td class="ce">${s.rgst_name}</td>
									<td class="ce">${s.rgst_dttm}</td>
								</tr>
								</c:forEach>
								<c:if test="${empty list}">
									<td colspan="4" class="ce"><b>게시물이 없습니다.</b></td>
								</c:if>
							</tbody>
						</table>
						<!-- //그리드 -->
							  <!-- 페이징 시작 -->
						<div class="pagination">
							<ul>
								<li>
						<!--페이징 리스트 -->
						 	<jsp:include page="../common/PageList_2011.jsp" flush="true">
							  	<jsp:param name="totalItemCount" value="${vo.totalRow}" />
								<jsp:param name="nowPage"        value="${vo.nowPage}" />
								<jsp:param name="functionName"   value="goPages" />
								<jsp:param name="listScale"      value="" />
								<jsp:param name="pageScale"      value="" />
								<jsp:param name="flag"           value="M01_FRONT" />
								<jsp:param name="extend"         value="no" />
							</jsp:include>
								</li>	
							</ul>
						</div>
							<!-- //페이징 -->			
						<!-- 버튼 str -->
						<p class="rgt"><span class="button medium"><a href="javascript:writeForm();">등록</a></span></p>
	                     <!-- //버튼 -->							
					</div>
						
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

