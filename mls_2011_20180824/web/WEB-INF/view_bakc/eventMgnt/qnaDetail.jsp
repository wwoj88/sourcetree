<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
%>
<% pageContext.setAttribute("lf", "\n"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권 찾기 이벤트 Q&amp;A | 알림마당 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
function qnaDel(){
	if(confirm("삭제하시겠습니까?")){
		document.getElementById("frm").action = '/eventMgnt/qnaDelete.do';
		document.getElementById("frm").submit();
	}
}

function qnaModi(){
	document.getElementById("frm").action = '/eventMgnt/qnaModi.do';
	document.getElementById("frm").submit();
}
//-->
</script>
</head>
 
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
 
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/header.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(5);</script> -->
		<!-- CONTAINER str-->
		<div id="container" class="container_vis5">
			<div class="container_vis">
				<h2><img src="/images/2012/title/container_vis_h2_5.gif" alt="알림마당" title="알림마당" /></h2>
			</div>
 
			<div class="content">
			
				<!-- 래프 -->																														
				<jsp:include page="/include/2012/leftMenu05.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb5");</script>
				<!-- //래프 -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					
					<p class="path"><span>Home</span><span>알림마당</span> <em>저작권찾기 이벤트</em> </p>
					<h1><img src="/images/2012/title/content_h1_0505.gif" alt="저작권찾기이벤트" title="저작권찾기이벤트" /></h1>
					<!-- section -->
					<div class="section">
						<!-- 상단탭 -->
						<c:import url="/eventMgnt/topMenu.do" charEncoding="EUC-KR">
            				<c:param name="menuno" value="3"/>
            			</c:import>
						<!-- 상단탭 -->
						<form name="frm" id="frm" method="post" action="#">
						<input type="hidden" name="bord_seqn" value="${vo.bord_seqn}"/>
						<input type="hidden" name="menu_seqn" value="9"/>
						<input type="hidden" name="threaded" value="${vo.threaded}"/>
						<input type="hidden" name="nowPage" value="${param.nowPage}"/>
						<!-- 테이블 view Set -->
						<div class="mt20">
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid tableFixed" summary="이벤트 Q&amp;A 등록">
								<colgroup><col width="12%"><col width="38%"><col width="12%"><col width="*"></colgroup>
								<tbody>	
									<tr>
										<th scope="col" class="ce">제목</th>
										<td colspan="3"><c:out value="${vo.tite}"/></td>
									</tr>
									<tr>
										<th scope="col" class="ce">작성자</th>
										<td>${vo.rgst_name}</td>
										<th scope="col" class="ce">작성일자</th>
										<td>${vo.rgst_dttm}</td>
									</tr>
									<tr>
										<th scope="col" class="ce">이메일</th>
										<td><c:out value="${vo.mail}"/></td>
										<th scope="col" class="ce"><label for="ch1">비공개</label></th>
										<td><input type="checkbox" id="ch1" disabled="disabled" name="ch1" <c:if test="${vo.open_ysno eq 'N'}">checked="checked"</c:if>/></td>
									</tr>
									<tr>
										<th scope="col" class="ce">질문</th>
										<td colspan="3"><c:out value="${fn:replace(vo.bord_desc, lf, '<br>')}" escapeXml="false"/></td>
									</tr>
									<c:if test="${not empty vo.answ_desc}">
									<tr>
										<th scope="col" class="ce">답변</th>
										<td colspan="3"><c:out value="${fn:replace(vo.answ_desc, lf, '<br>')}" escapeXml="false"/></td>
									</tr>
									</c:if>							
								</tbody>
							</table>							
							<!-- //그리드스타일 -->
							<!-- 버튼AREA -->
								<div class="btnArea">
									<p class="fl"><span class="button medium gray"><a href="/eventMgnt/qnaList.do?nowPage=${param.nowPage}">목록</a></span></p>
									<c:if test="${UserName eq vo.rgst_idnt}">
										<p class="fr"><span class="button medium"><a href="javascript:qnaModi();" onclick="">수정</a></span> <span class="button medium gray"><a href="javascript:qnaDel();">삭제</a></span></p>
									</c:if>
								</div>
							<!--// 버튼AREA -->
						</div>
						<!-- //테이블 view Set -->
						</form>
																	
					</div>
					<!-- //section -->	
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>