<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("user_idnt", sessUserIdnt);
%>
<%
	pageContext.setAttribute("lf", "\n");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>댓글형 이벤트 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script type="text/javascript" src="/js/jquery-1.7.1.js"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript">
<!--
	window.onload = function() {
		WindowReset();
	}

	function goPages(pageNo) {
		location.href = '/eventMgnt/eventView02.do?event_id=${vo.event_id}&nowPage=' + pageNo;
	}

	function keyCheck(key) {
		if (event.keyCode == 13) {
			findFormSubmit();
		}
	}

	function chDiv() {
		var idck = '${user_idnt}';
		var partCnt = '${partCnt}';
		var partDuplYn = '${vo.part_dupl_yn}';
		if (idck == '') {
			alert("로그인하세요");
			return;
		}

		if (partDuplYn == 'N') {
			if (partCnt = '' || Number(partCnt) > 1) {
				alert('이미 참여하셨습니다.');
				return;
			}
		}

		if ($('#rslt_desc').val() == '') {
			alert("댓글을 입력하세요");
			return;
		}

		$('#pop_wrap1').attr("style", "display:none");
		$('#pop_wrap2').attr("style", "display:inline");
		WindowReset();
	}

	function chDiv2() {

		if ($('#name').val() == '') {
			alert("이름을 입력하세요.");
			$('#name').focus();
			return;
		}
		if ($('#birth_dtae').val() == '') {
			alert("생년월일을 입력하세요.");
			$('#birth_dtae').focus();
			return;
		}
		if ($('#mobl_phon').val() == '') {
			alert("휴대전화를 입력하세요.");
			$('#mobl_phon').focus();
			return;
		}

		var mob = $('#mobl_phon').val();
		if (mob.match(/^[\d]{3}-[\d]{3,4}-[\d]{4}$/) == null) {
			alert("휴대전화를 올바르게 입력하세요.");
			$('#mobl_phon').focus();
			return;
		}

		if ($('#mail').val() == '') {
			alert("이메일을 입력하세요.");
			$('#mail').focus();
			return;
		}

		var t = $('#mail').val();
		if (t.match(/^(\w+)@(\w+)[.](\w+)$/ig) == null && t.match(/^(\w+)@(\w+)[.](\w+)[.](\w+)$/ig) == null) {
			alert("이메일을 올바르게 입력하세요.");
			$('#mail').focus();
			return;
		}

		/* 		if ($('#ckbox1').is(':checked') == false) {
		 alert("동의항목을 체크해주세요.");
		 return;
		 } */

		var ehddml_pass = 'Y';
		$("input.ehddml").each(function() {
			if (!$(this).is(':checked')) {
				ehddml_pass = 'N';
				alert("동의항목을 체크해주세요.");
				return false;
			}
		});
		if (ehddml_pass == 'N')
			return;

		if (confirm("이벤트에 참여하시겠습니까?")) {
			$("#fbtn").attr("href","javascript:alert('잠시만 기다려주세요.');");
			formSubMit();
		}
	}

	function formSubMit() {
		$('#frm').attr("action", "/eventMgnt/addEventView02.do").submit();
	}

	//달력
	function fn_cal(frmName, objName) {
		showCalendar(frmName, objName);
	}

	function WindowReset() {
		var marginY = 67;
		if (navigator.userAgent.indexOf("Firefox") > 0) marginY = 65;
		else if(navigator.userAgent.indexOf("MSIE 8") > 0) marginY = 83;    // IE 8.x
		var hei = $("body").height();
		if (hei > 1000) {
			hei = 1000;
		}
		resizeTo(748, (hei + marginY));
	}
//-->
</script>
</head>

<body>
	<form action="#" method="post" name="frm" id="frm">
		<input type="hidden" name="event_id" value="${vo.event_id}" /> <input type="hidden" name="user_idnt" value="${user_idnt}" /> <input type="hidden" name="item_id" value="${vo.item_id}" />
		<!-- 전체를 감싸는 DIVISION -->
		<div id="pop_wrap1" style="display: inline;">
			<!-- HEADER str-->
			<div id="popHeader">
				<h1>
					<c:choose>
						<c:when test="${vo.opening_yn eq 'S'}">
							<img class="vmid" src="/images/2012/common/ico_ing.gif" alt="진행예정" />
						</c:when>
						<c:when test="${vo.opening_yn eq 'Y'}">
							<img class="vmid" src="/images/2012/common/ico_ing.png" alt="진행중" />
						</c:when>
						<c:otherwise>
							<img class="vmid" src="/images/2012/common/ico_end.gif" alt="종료" />
						</c:otherwise>
					</c:choose>
					<span class="vmid ml5">${vo.event_title }</span> <span class="vmid ml5 thin p12">(기간 : ${vo.start_dttm_part}~${vo.end_dttm_part})</span>
				</h1>
			</div>
			<!-- //HEADER end -->
			<!-- CONTAINER str-->
			<div id="contentBody" style="padding: 0 15px 15px 15px;">
				<div class="section">
					<!-- 이벤트 설문조사 -->
					<div class="event">
						<!-- 이벤트이미지 -->
						<c:if test="${not empty vo.image_file_path && vo.open_yn_image eq 'Y'}">
							<div class="event_box">
								<p class="ce">
									<img src="/imageview.do?filePath=${vo.image_file_path}" alt="" class="event_img" />
								</p>
							</div>
						</c:if>
						<div class="event_descript">
							<h2>이벤트 안내문구</h2>
							<p>
								<c:out value="${fn:replace(vo.item_desc, lf, '<br>')}" escapeXml="false" />
							</p>
						</div>
						<!-- //이벤트이미지 -->
						<!-- 설문조사 -->
						<span class="topLine mt20"></span>
						<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
							<colgroup></colgroup>
							<tbody>
								<tr>
									<th scope="col"><input type="text" class="inputData comment  w70" id="rslt_desc" name="rslt_desc" value="">
									<c:choose>
											<c:when test="${param.type eq 'view'}">
												<a href="#" class="ml15"><img src="/images/2012/button/btn_comment.gif" alt="댓글입력" /></a>
											</c:when>
											<c:otherwise>
												<a href="javascript:chDiv();" class="ml15"><img src="/images/2012/button/btn_comment.gif" alt="댓글입력" /></a>
											</c:otherwise>
										</c:choose></th>
								</tr>
								<tr>
									<td>
										<!-- 댓글 리스트 -->
										<table cellpadding="0" cellspacing="0" border="1" summary="" class="grid eventGrid tableFixed">
											<colgroup>
												<col width="8%">
												<col width="66%">
												<col width="12%">
												<col width="14%">
											</colgroup>
											<tbody>
												<c:forEach items="${listvo.list}" var="s">
													<tr class="topDotted">
														<td>${s.rslt_seq}</td>
														<td><c:out value="${s.rslt_desc}" /></td>
														<td><c:out value="${s.name}" /></td>
														<td>${s.part_dttm}</td>
													</tr>
												</c:forEach>
												<c:if test="${empty listvo.list}">
													<tr class="topDotted">
														<td colspan="4" class="ce"><b>이벤트를 참여하세요!</b></td>
													</tr>
												</c:if>
											</tbody>
										</table> <!-- //댓글 리스트 -->
									</td>
								</tr>
							</tbody>
						</table>
						<!-- //설문조사 -->
						<!-- 페이징 -->
						<!-- 페이징 시작 -->
						<div class="pagination">
							<ul>
								<li>
									<!--페이징 리스트 --> <jsp:include page="../common/PageList_2011.jsp" flush="true">
										<jsp:param name="totalItemCount" value="${listvo.totalRow}" />
										<jsp:param name="nowPage" value="${listvo.nowPage}" />
										<jsp:param name="functionName" value="goPages" />
										<jsp:param name="listScale" value="" />
										<jsp:param name="pageScale" value="" />
										<jsp:param name="flag" value="M01_FRONT" />
										<jsp:param name="extend" value="no" />
									</jsp:include>
								</li>
							</ul>
						</div>
						<!-- //페이징 -->
						<!-- //페이징 -->
					</div>
					<!-- //이벤트 설문조사 -->
				</div>
			</div>
			<!-- //CONTAINER end -->

			<!-- FOOTER str-->
			<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights reserved.</p>
			<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="X" title="이 창을 닫습니다." /></a>
			<!-- //FOOTER end -->

		</div>
		<!-- //전체를 감싸는 DIVISION -->
		<!-- 전체를 감싸는 DIVISION -->
		<div id="pop_wrap2" style="display: none;">
			<!-- HEADER str-->
			<div id="popHeader">
				<h1>이벤트 참여정보 등록</h1>
			</div>
			<!-- //HEADER end -->
			<!-- CONTAINER str-->
			<div id="contentBody" style="padding: 0 15px 15px 15px;">
				<div class="section">
					<!-- 이벤트 설문조사 -->
					<h2 class="mt20">기본정보</h2>
					<span class="topLine"></span>
					<table cellpadding="0" cellspacing="0" border="1" class="grid">
						<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
						<tbody>
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="1" class="grid eventGrid">
										<!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<colgroup>
											<col width="12%">
											<col width="38%">
											<col width="12%">
											<col width="38%">
										</colgroup>
										<tbody>
											<tr>
												<th scope="col"><label for="name">이름</label></th>
												<td><input type="text" id="name" name="name" class="inputData w60"></td>
												<th scope="col" class="bgNone">&nbsp;</th>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<th scope="col">성별</th>
												<td><input type="radio" id="sex1" name="sex" checked="checked" value="M"><label for="sex1" class="vmid">남</label><input type="radio" id="sex2" name="sex" class="ml20" value="F"><label for="sex2" class="vmid">여</label></td>
												<th scope="col"><label for="birth_dtae">생년월일</label></th>
												<td><input type="text" id="birth_dtae" name="birth_dtae" class="inputData w60" readonly="readonly"><a href="#" class="ml5"><img src="/images/2012/common/calendar.gif" alt="달력" class="vmid" onclick="fn_cal('frm','birth_dtae')" /></a></td>
											</tr>
											<tr>
												<th scope="col"><label for="mobl_phon">휴대전화</label></th>
												<td colspan="3"><input type="text" id="mobl_phon" name="mobl_phon" class="inputData w30" />&nbsp;예)010-0000-0000</td>
											</tr>
											<tr>
												<th scope="col"><label for="mail">이메일주소</label></th>
												<td colspan="3"><input type="text" id="mail" name="mail" class="inputData w30"/>&nbsp;예)right@right4me.or.kr</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						<tbody>
					</table>
					<c:forEach items="${agree}" var="s">
						<h2 class="mt20">
							<label for="txt5">${s.item_title}</label>
						</h2>
						<textarea class="textarea w98" name="txt5" id="txt5" rows="3" cols="10" readonly="readonly">${s.item_desc}</textarea>
						<p class="mt5">
							<input type="checkbox" id="ckbox1" name="ckbox1" class="ehddml"><span class="vmid">동의합니다.</span>
						</p>
					</c:forEach>
					<!-- //버튼Area -->
					<p class="btnArea">
						<a id="fbtn" href="javascript:chDiv2();"><img src="/images/2012/button/btn_complete.gif" alt="참여완료" /></a>
					</p>
					<!-- //버튼Area -->
					<!-- //이벤트 설문조사 -->
				</div>
			</div>
			<!-- //CONTAINER end -->

			<!-- FOOTER str-->
			<p id="pop_footer">Copyright (C) 2012 한국저작권위원회. All rights reserved.</p>
			<a href="#1" onclick="javascript:self.close();" class="pop_close"><img src="/images/2012/button/pop_close.gif" alt="X" title="이 창을 닫습니다." /></a>
			<!-- //FOOTER end -->

		</div>
		<!-- //전체를 감싸는 DIVISION -->
	</form>
	<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>