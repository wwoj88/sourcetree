<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
					<c:set var="eve" value="/eventMgnt/eventList.do"/>
					<c:set var="win" value="/eventMgnt/winningList.do"/>
					<c:set var="qna" value="/eventMgnt/qnaList.do"/>
					<ul class="tab_menuBg">
					<c:choose>
						<c:when test="${param.menuno eq '1'}">
							<li class="first on"><strong><a href="${eve}">이벤트</a></strong></li>
							<li><a href="${win}">당첨자 발표</a></li>
							<c:if test="${qnayn eq 'Y'}"><li><a href="${qna}">Q&amp;A</a></li></c:if>
						</c:when>
						<c:when test="${param.menuno eq '2'}">
							<li class="first"><a href="${eve}">이벤트</a></li>
							<li class="on"><strong><a href="${win}">당첨자 발표</a></strong></li>
							<c:if test="${qnayn eq 'Y'}"><li><a href="${qna}">Q&amp;A</a></li></c:if>
						</c:when>
						<c:when test="${param.menuno eq '3'}">
							<li class="first"><a href="${eve}">이벤트</a></li>
							<li><a href="${win}">당첨자 발표</a></li>
							<c:if test="${qnayn eq 'Y'}"><li class="on"><strong><a href="${qna}">Q&amp;A</a></strong></li></c:if>
							<c:if test="${qnayn ne 'Y'}">
								<script type="text/javascript">
								if(location.pathname == '${qna}'){
									location.href = '${eve}';
								}
								</script>
							</c:if>
						</c:when>
					</c:choose>
					</ul>