<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권 찾기 이벤트 | 알림마당 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
function goPages(pageNo) {
	  location.href = '/eventMgnt/eventList.do?nowPage='+pageNo;
}

function keyCheck(key){
	if(event.keyCode == 13){
		findFormSubmit();
	}
}

//-->
</script>
<script> 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-2', 'auto');
  ga('send', 'pageview');
</script>
</head>
 
<body>
 
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader6.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(6);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="contents">
 
				<!-- 래프 -->																														
				<div class="con_lf">
					<div class="con_lf_big_title">알림마당</div>
					<ul class="sub_lf_menu">
						<li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=4&page_no=1">공지사항</a></li>
						<li><a href="/board/board.do?mNum=4&sNum=0&leftsub=0&menuSeqn=1&page_no=1">자주묻는 질문</a></li>
						<li><a href="/eventMgnt/eventList.do" class="on">저작권찾기 이벤트</a></li>
					</ul>
				</div>
				<!-- //래프 -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<!-- 주요컨텐츠 str -->
				<div class="con_rt">
					
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						알림마당
						&gt;
						<span class="bold">저작권 찾기 이벤트</span>
					</div>
					
					<div class="con_rt_hd_title">저작권 찾기 이벤트</div>
					
					<div id="sub_contents_con">
						<!-- 상단탭 -->
						<ul class="sub_menu1 w403 mar_tp40">
							<li class="on"><a href="/eventMgnt/eventList.do" class="on">이벤트</a></li>
							<li><a href="/eventMgnt/winningList.do" class="last_rt_bor">당첨자 발표</a></li>
						</ul>
						<p class="clear"></p>
						<!-- 상단탭 -->
						<!-- 그리드 -->
						<table class="sub_tab3 mar_tp40" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국저작권위원회</caption>
							<colgroup>
							<col width="15%"/>
							<col width="*"/>
							<col width="20%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="row">진행상태</th>
									<th scope="row">내용</th>
									<th scope="row">기간</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${vo.list}" var="s" >
									<tr>
										<td class="ce">
											<c:choose>
												<c:when test="${s.opening_yn eq 'S'}"><img src="/images/2012/common/ico_plan.gif" alt="예정"/></c:when>
												<c:when test="${s.opening_yn eq 'Y'}"><img src="/images/2012/common/ico_ing.gif" alt="진행중"/></c:when>
												<c:otherwise><img src="/images/2012/common/ico_end.gif" alt="종료"/></c:otherwise>
											</c:choose>
										</td>
										<td style="text-align: left;">
											<a href="/eventMgnt/eventDetail.do?event_id=${s.event_id}&amp;nowPage=${vo.nowPage}"><c:out value="${s.event_title}"/></a>
										</td>
										<td class="ce">${s.start_dttm_oper}</td>
									</tr>
								</c:forEach>
								<c:if test="${empty vo.list}">
									<tr><td colspan="3" class="ce"><b>진행중인 이벤트가 없습니다.</b></td></tr>
								</c:if>
							</tbody>
						</table>
						<!-- //그리드 -->
						<!-- 페이징 -->
							  <!-- 페이징 시작 -->
						<div class="page mar_tp30">
							<ul>
								<li>
						<!--페이징 리스트 -->
						 	<jsp:include page="../common/PageList_2011.jsp" flush="true">
							  	<jsp:param name="totalItemCount" value="${vo.totalRow}" />
								<jsp:param name="nowPage"        value="${vo.nowPage}" />
								<jsp:param name="functionName"   value="goPages" />
								<jsp:param name="listScale"      value="" />
								<jsp:param name="pageScale"      value="" />
								<jsp:param name="flag"           value="M01_FRONT" />
								<jsp:param name="extend"         value="no" />
							</jsp:include>
								</li>	
							</ul>
						</div>
							<!-- //페이징 -->
						<!-- //페이징 -->											
					</div>
						<!-- //테이블 리스트 Set -->
												
					</div>
					<p class="clear"></p>
				</div>
				<!-- //주요컨텐츠 end -->
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

