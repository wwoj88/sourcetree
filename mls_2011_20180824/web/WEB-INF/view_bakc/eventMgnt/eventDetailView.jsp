<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
%>
<% pageContext.setAttribute("lf", "\n"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권 찾기 이벤트</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
var popwin;
function openEvent(etype){
	if(etype == '1'){
		document.getElementById('popfrm').action = '/eventMgnt/eventView01.do';
		popwin = window.open('', 'popfrm', 'width=748,height=621,scrollbars=yes');
		document.popfrm.submit();
		popwin.focus();
	}else if(etype == '2'){
		document.getElementById('popfrm').action = '/eventMgnt/eventView02.do';
		popwin = window.open('', 'popfrm', 'width=748,height=621,scrollbars=yes');
		document.popfrm.submit();
		popwin.focus();
	}else{
		alert('잘못된 접근입니다.');
		return;
	}
}
//-->
</script>
</head>
 
<body>
	<form method="post" name="popfrm" id="popfrm" action="/eventMgnt/eventView01.do" target="popfrm">
		<input type="hidden" name="event_id" value="${vo.event_id}"/>
		<input type="hidden" name="type" value="view"/>
	</form>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<div id="container" style="background-image: none;">
 
			<div class="content">
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody" style="float: left;">
					<!-- section -->
					<div class="section">
						<form name="form1" method="post" action = "#">
						<!-- 테이블 view Set -->
						<div class="mt20">
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup><col width="*"></colgroup>
								<tbody>
									<tr>
										<th>
											<p class="fl">
												<img src="/images/2012/common/ico_ing.gif" alt="진행예정" class="vmid" />
												<span class="vmid ml10">${vo.event_title}</span></p><p class="fr">기간 : ${vo.start_dttm_part}~${vo.end_dttm_part}
											</p>
										</th>
									</tr>
									<tr>
										<td>
											<!-- 이벤트 -->
											<div class="event">
												<c:if test="${not empty vo.image_file_path && vo.open_yn_image eq 'Y'}">
													<p class="event_box ce"><img src="/imageview.do?filePath=${vo.image_file_path}" alt="${vo.event_title}" class="event_img"></p>
												</c:if>
												<!-- 이벤트 그리드 -->
												<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid eventGrid mt20 mb20"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
													<colgroup><col width="28%"><col width="*"></colgroup>
													<tbody>
														<c:if test="${vo.open_yn_prop eq 'Y'}">
															<tr>
																<th scope="col">이벤트 목적</th>
																<td>${vo.prop_desc}</td>
															</tr>
														</c:if>
														<c:if test="${vo.open_yn_win_anuc_desc eq 'Y'}">
															<tr>
																<th scope="col">참여안내</th>
																<td><c:out value="${fn:replace(vo.win_anuc_desc, lf, '<br>')}" escapeXml="false"/></td>
															</tr>
														</c:if>
														<c:if test="${vo.open_yn_dttm_part eq 'Y'}">
															<tr>
																<th scope="col">이벤트 기간</th>
																<td>${vo.start_dttm_part}~${vo.end_dttm_part}</td>
															</tr>
														</c:if>
														<c:choose>
															<c:when test="${vo.open_yn_win_anuc_date eq 'Y' and  vo.open_yn_win_cnt eq 'Y'}">
															<tr>
																<th scope="col">당첨자 발표 정보</th>
																<td>당첨자 발표 : <c:out value="${vo.win_anuc_date}"/>, 당첨자 수 : <c:out value="${vo.win_cnt}"/></td>
															</tr>
															</c:when>
															<c:when test="${vo.open_yn_win_anuc_date eq 'Y'}">
															<tr>
																<th scope="col">당첨자 발표 정보</th>
																<td>당첨자 발표 : <c:out value="${vo.win_anuc_date}"/></td>
															</tr>
															</c:when>
															<c:when test="${vo.open_yn_win_cnt eq 'Y'}">
															<tr>
																<th scope="col">당첨자 발표 정보</th>
																<td>당첨자 수 : <c:out value="${vo.win_cnt}"/></td>
															</tr>
															</c:when>
														</c:choose>
													</tbody>
												</table>
												<!-- //이벤트 그리드 -->
												<c:if test="${vo.opening_yn eq 'Y'}">
												<p class="btnArea"><a href="javascript:openEvent('${vo.event_type_cd}');"><img src="/images/2012/button/btn_parti.gif" alt=""/></a></p>
												</c:if>
												</div>
											<!-- //이벤트 -->
										</td>
									</tr>
								</tbody>
							</table>							
							<!-- //그리드스타일 -->
						</div>
						<!-- //테이블 view Set -->
						</form>
																	
					</div>
					<!-- //section -->	
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->		
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>