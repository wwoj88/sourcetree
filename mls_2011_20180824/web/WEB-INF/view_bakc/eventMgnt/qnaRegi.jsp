<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt);
	pageContext.setAttribute("sessionUserName", user.getUserName());
	pageContext.setAttribute("sessionUserMail", user.getMail());
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권 찾기 이벤트</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript"> 
function goQnaList(pageNo) {
	  location.href = '/eventMgnt/qnaList.do?nowPage='+pageNo;
}

function keyCheck(key){
	if(event.keyCode == 13){
		findFormSubmit();
	}
}

function regData(){
	if(document.getElementById("tite").value==''){
		alert("제목을 입력하세요.");
		return;
	}
	if(document.getElementById("mail").value==''){
		alert("이메일을 입력하세요.");
		return;
	}
	if(document.getElementById("bord_desc").value==''){
		alert("질문을 입력하세요.");
		return;
	}
	
	if(confirm("등록하시겠습니까?")){
		document.getElementById("frm").action = '/eventMgnt/addQnaRegi.do';
		document.getElementById("frm").submit();
	}
}

</script>
</head>
 
<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
 
		<!-- HEADER str-->
		<div id="header">
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/header.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(5);</script> -->
		</div>
		<!-- CONTAINER str-->
		<div id="container" class="container_vis5">
			<div class="container_vis">
				<h2><img src="/images/2012/title/container_vis_h2_5.gif" alt="알림마당" title="알림마당" /></h2>
			</div>
 
			<div class="content">
			
				<!-- 래프 -->																														
				<jsp:include page="/include/2012/leftMenu05.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb5");</script>
				<!-- //래프 -->
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					
					<p class="path"><span>Home</span><span>알림마당</span> <em>저작권찾기 이벤트</em> </p>
					<h1><img src="/images/2012/title/content_h1_0505.gif" alt="저작권찾기이벤트" title="저작권찾기이벤트" /></h1>
					<!-- section -->
					<div class="section">
						<!-- 상단탭 -->
						<c:import url="/eventMgnt/topMenu.do" charEncoding="EUC-KR">
            				<c:param name="menuno" value="3"/>
            			</c:import>
						<!-- 상단탭 -->
						<form name="frm" id="frm" method="post" action = "#">
						<input type="hidden" name="menuSeqn" value="9">
						<div class="mt20">
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="이벤트 Q&amp;A 등록">
								<colgroup><col width="12%"><col width="38%"><col width="12%"><col width="*"></colgroup>
								<tbody>	
									<tr>
										<th scope="col" class="ce"><label for="tite">제목</label></th>
										<td colspan="3"><input type="text" id="tite" name="tite" class="inputData w90"></td>
									</tr>
									<tr>
										<th scope="col" class="ce"><label>작성자</label></th>
										<td colspan="3"><c:out value="${sessionUserName}"/></td>
									</tr>
									<tr>
										<th scope="col" class="ce"><label for="mail">이메일</label></th>
										<td><input type="text" id="mail" name="mail" class="inputData w73" value="${sessionUserMail}"></td>
										<th scope="col" class="ce"><label for="open_ysno">비공개</label></th>
										<td><input type="checkbox" id="open_ysno" name="open_ysno" value="N"></td>
									</tr>
									<tr>
										<th scope="col" class="ce"><label for="bord_desc">질문</label></th>
										<td colspan="3"><textarea cols="10" rows="15" name="bord_desc" id="bord_desc" class="textarea w95"></textarea></td>
									</tr>																					
								</tbody>
							</table>							
							<!-- //그리드스타일 -->
							<!-- 버튼AREA -->
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="javascript:goQnaList('${param.nowPage}');">목록</a></span></p>
								<p class="fr"><span class="button medium"><a href="javascript:regData();">등록</a></span></p>
							</div>
							<!--// 버튼AREA -->
						</div>
						<!-- //테이블 view Set -->
						</form>
																	
					</div>
					<!-- //section -->	
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
			<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

