<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>나의 저작물 등록 예시 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--

//-->
</script>
</head>
<body class="popup_bg">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="pop_wrap">
		<!-- HEADER str-->
		<div id="popHeader">
			<h1>나의 저작물 등록 예시</h1>
		</div>
		<!-- //HEADER end -->
		<!-- CONTAINER str-->
		<div id="popContents">
			<div class="section">
				<img src="/images/2011/smpl/priv01.jpg" alt="" />
			</div>
		</div>
		<!-- //CONTAINER end -->
		<!-- FOOTER str-->
		<p id="pop_footer">Copyright (C) 2007 한국저작권위원회. All rights reserved.</p>
		<!-- //FOOTER end -->
		<a href="#1" onclick="javascript:self.close()" class="pop_close"><img src="/images/2011/button/pop_close.gif" alt="" /></a>
	</div>
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
