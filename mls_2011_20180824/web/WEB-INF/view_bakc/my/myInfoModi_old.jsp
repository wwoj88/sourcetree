<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
  String submitChek = request.getParameter("submitChek") == null ? "" : request.getParameter("submitChek");
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작물 내권리찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
<script src="/js/Function.js" type="text/javascript"></script>
<script type="text/JavaScript">
<!--
function goPostNumbSrch() {
  //window.open('/user/user.do?method=goPostNumbSrch','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=445');
	var pop = window.open("/jusoPopup.jsp?method=goPostNumbSrch","pop","width=570,height=420, scrollbars=yes, resizable=yes");
}

function updateUserInfo() {
	var frm = document.form1;

  if (frm.pswd.value == "") {
		alert("비밀번호를 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value.length < 6) {
		alert("비밀번호는 6자리 이상 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value != frm.pswdConf.value) {
		alert("입력하신 비밀번호가 다릅니다. 입력하신 비밀번호를 확인하십시오");
		frm.pswd.focus();
		return;
	} else if (frm.mail.value == "") {
		alert("이메일 주소를 입력하십시오.");
		frm.mail.focus();
		return;
  } else if (frm.zipxNumb.value == "") {
		alert("우편번호를 입력하십시오.");
		frm.zipxNumb.focus();
		return;
  } else if (frm.addr.value == "") {
		alert("주소를 입력하십시오.");
		frm.addr.focus();
		return;
  } else if (frm.detlAddr.value == "") {
		alert("상세주소를 입력하십시오.");
		frm.detlAddr.focus();
		return;
  } else if (frm.moblPhon.value == "") {
		alert("핸드폰번호를 입력하십시오.");
		frm.moblPhon.focus();
		return;
  } else if ((inspectCheckBoxField(frm.mailReceYsno) == false) && (inspectCheckBoxField(frm.smsReceYsno) == false)) {
		alert("수신방법 중 이메일과 핸드폰 하나는 선택하셔야 합니다.");
		frm.mailReceYsno.focus();
		return;
	} else {
		if(confirm("회원정보를 저장하시겠습니까?")){
			frm.action = "/user/user.do";
			zipxNumb = frm.zipxNumb.value;
			frm.zip.value = zipxNumb.substring(0,3)+zipxNumb.substring(4,7);

	  	frm.submit();
	  }
	}
}

function jusoCallBack(roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn){
	// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
	document.form1.addr.value = roadAddrPart1;
	document.form1.detlAddr.value = addrDetail;
	document.form1.zipxNumb.value = zipNo;
}

function init() {
	alert("회원정보가 정상적으로 수정되었습니다.");
}
//-->
</script>
</head>
<%  if ("Y".equals(submitChek)) {  %>
<body class="subBg" onLoad="init();">
<%  } else {  %>
<body class="subBg">
<%  } %>
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp"/>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','122');
			</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="memberTlt">회원</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>회원</li>
					<li class="on">내정보수정</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>내정보 수정</h2>
			<div class="tip R">(<img src="/images/common/ic_necessary.gif" width="10" height="7" /> ) 항목은 필수입력사항이므로 빠짐없이 기입하여 주시기 바랍니다.</div>
			<table width="710" class="board">
				<colgroup>
				<col width="130" />
				<col width="" />
				</colgroup>
				<caption summary="회원가입 항목을 입력하고, 회원가입을 한다">회원가입 항목입력</caption>
				<tbody class="ViewBody">
					<form name="form1" method="post">
						<input type="hidden" name="userIdnt" value="${userInfoMap.USER_IDNT}">
						<input type="hidden" name="zip">
						<input type="hidden" name="srchAddr">
						<input type="hidden" name="method" value="updateUserInfo">
						<input type="hidden" name="submitChek" value="Y">
  				<tr>
						<th>회원구분</th>
						<td>${userInfoMap.USER_DIVS_NAME}</td>
					</tr>
					<tr>
						<th>이름/사업자명</th>
						<td>${userInfoMap.USER_NAME}</td>
					</tr>
				  <c:if test="${userInfoMap.USER_DIVS != '03'}">
					<tr>
						<th>주민/법인등록번호</th>
						<td><input type="text" class="input" name="resdCorpNumb1" id="resdCorpNumb1" class="input" style="border-width:0px; width:39px; text-align:right;" value="${userInfoMap.RESD_CORP_NUMB1}" readonly/>-<input type="text" class="input" name="resdCorpNumb1" id="resdCorpNumb1" class="input" style="border-width:0px; width:45px; text-align:left;" value="${userInfoMap.RESD_CORP_NUMB2}" readonly/></td>
					</tr>
				  </c:if>
				  <c:if test="${userInfoMap.USER_DIVS != '01'}">
					<tr>
						<th><label for="comNum">사업자번호</label></th>
						<td>${fn:substring(userInfoMap.CORP_NUMB,0,3)}-${fn:substring(userInfoMap.CORP_NUMB,3,5)}-${fn:substring(userInfoMap.CORP_NUMB,5,10)}</td>
					</tr>
					</c:if>
					<tr>
						<th class="necessary"><label for="pw">비밀번호</label></th>
						<td><input type="password" name="pswd" id="pswd" class="input" /> &nbsp; (6자리 이상으로 입력하십시오.) </td>
					</tr>
					<tr>
						<th class="necessary"><label for="pwCheck">비밀번호 확인</label></th>
						<td><input type="password" name="pswdConf" id="psConf" class="input" /></td>
					</tr>
					<tr>
						<th class="necessary"><label for="email">이메일 주소</label></th>
						<td><input type="text" class="input" name="mail" id="mail" value="${userInfoMap.MAIL}" /></td>
					</tr>
					<tr>
						<th class="necessary"><label for="postNum">우편번호</label></th>
						<td><input type="text" name="zipxNumb" id="zipxNumb" value="${fn:substring(userInfoMap.ZIPX_CODE, 0, 3)}-${fn:substring(userInfoMap.ZIPX_CODE, 3, 6)}" class="input" readonly />&nbsp;<a href="javascript:goPostNumbSrch();" title="새 창에서 열림"><img src="/images/button/postnum_btn.gif" alt="우편번호찾기" width="67" height="20" /></a></td>
					</tr>
					<tr>
						<th class="necessary"><label for="add">주소</label></th>
						<td><input type="text" class="input" readonly name="addr" id="addr" value="${userInfoMap.ADDR}" size="88" /></td>
					</tr>
					<tr>
						<th class="necessary"><label for="addDtail">상세주소</label></th>
						<td><input type="text" class="input" name="detlAddr" id="detlAddr" value="${userInfoMap.DETL_ADDR}" size="88" /></td>
					</tr>
					<tr>
						<th><label for="phoneNum">자택전화번호</label></th>
						<td><input type="text" name="telxNumb" id="telxNumb" value="${userInfoMap.TELX_NUMB}" class="input" />&nbsp;(예:02-123-4567)</td>
					</tr>
					<tr>
						<th class="necessary"><label for="pNum">핸드폰번호</label></th>
						<td><input type="text" name="moblPhon" id="moblPhon" value="${userInfoMap.MOBL_PHON}" class="input" />&nbsp;(예:010-1234-5678)</td>
					</tr>
					<tr>
						<th>수신방법</th>
						<td><input type="checkbox" name="mailReceYsno" id="mailReceYsno" value="Y" <c:if test="${userInfoMap.MAIL_RECE_YSNO == 'Y' }"> checked  </c:if> class="chk"/><label for="email2">이메일</label>
							  <input type="checkbox" name="smsReceYsno" id="smsReceYsno" value="Y" <c:if test="${userInfoMap.SMS_RECE_YSNO == 'Y' }"> checked  </c:if> class="chk"/><label for="hpNum">핸드폰</label></td>
					</tr>
				</tbody>
			</table>
			<!--buttonArea start-->
			<div id="buttonArea">
				<div class="floatR"><a href="javascript:updateUserInfo();"><img src="/images/button/modi_btn.gif" alt="수정하기" width="66" height="21" /></a>
					                  <a href="javascript:document.form1.reset();"><img src="/images/button/cancle_btn.gif" alt="취소하기" width="66" height="21" /></a></div>
			</div>
			<!--buttonArea end-->
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
