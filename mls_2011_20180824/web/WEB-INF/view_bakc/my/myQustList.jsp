<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
	String menuSeqn = request.getParameter("menuSeqn");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
	String rgstIdnt  = request.getParameter("rgstIdnt") == null ? "" : request.getParameter("rgstIdnt");
	String srchStartDate  = request.getParameter("srchStartDate") == null ? "" : request.getParameter("srchStartDate");
	String srchEndDate  = request.getParameter("srchEndDate") == null ? "" : request.getParameter("srchEndDate");
	User user = SessionUtil.getSession(request);
	
	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>나의 질문과 답 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--
	function goPage(pageNo){
		var frm = document.form1;
		frm.page_no.value = pageNo;
		frm.submit();
	}

	function board_search() {
		var frm = document.form1;
		frm.page_no.value = 1;
		frm.submit();
	}

	function fn_enterCheck(obj){
	  // EnterKey 입력시 공인인증서 로그인 수행
	  if (event.keyCode == 13) {
		  board_search();
	  }
  }

	function boardDetail(bordSeqn,menuSeqn,threaded){
		var frm = document.form1;
		frm.bordSeqn.value = bordSeqn;
		frm.menuSeqn.value = menuSeqn;
		frm.threaded.value = threaded;
		frm.page_no.value = '<%=page_no%>';
		frm.method = "post";
		frm.action = "/board/board.do?method=boardView";
		frm.submit();
  }
  function fn_goQust() {
		var frm = document.form1;
		frm.page_no.value = 1;
		frm.method = "post";
		frm.action = "/board/board.do?method=goQust";
		frm.submit();
	}
	/*calendar호출*/
function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}

//숫자만 입력
function only_arabic(t){
	var key = (window.netscape) ? t.which :  event.keyCode;

	if (key < 45 || key > 57) {
		if(window.netscape){  // 파이어폭스
			t.preventDefault();
		}else{
			event.returnValue = false;
		}
	} else {
		//alert('숫자만 입력 가능합니다.');
		if(window.netscape){   // 파이어폭스
			return true;
		}else{
			event.returnValue = true;
		}
	}	
}
	
// 날짜체크 
function checkValDate(){
	var f = document.form1;
	if(f.srchStartDate.value!='' && f.srchEndDate.value!=''){
		if(parseInt(f.srchStartDate.value,10)>parseInt(f.srchEndDate.value,10)){
			alert('만료일이 시작일보다 이전입니다.\n일자를 다시 확인하십시요');
			f.srchEndDate.value='';
			return false;
		}
	}

	if(f.srchStartDate.value!=''){
		if(f.srchStartDate.value.length != 8){
			alert('일자형식은 8자리 입니다.');
			f.srchStartDate.value='';
			return false;
		}
	}
	if(f.srchEndDate.value!=''){
		if(f.srchEndDate.value.length != 8){
			alert('일자형식은 8자리 입니다.');
			f.srchEndDate.value='';
			return false;
		}
	}
}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2011/header.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_8.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<jsp:include page="/include/2011/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb2");
				</script>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>저작권콘텐츠</span><em>나의 질문과 답</em></p>
					<h1 title="저작물에 대한 주민의식이 필요합니다!"><img src="/images/2011/title/content_h1_0804.gif" alt="나의 질문과 답" title="나의 질문과 답" /></h1>
					
					<div class="section">
					<form name="form1" method="post" action = "#">
						<!-- 검색 -->
					    <input type="hidden" name="page_no" />
					    <input type="hidden" name="menuSeqn" value="<%=menuSeqn%>" />
					    <input type="hidden" name="bordSeqn" />
					    <input type="hidden" name="threaded" />
					    <input type="hidden" name="rgstIdnt" value="<%=rgstIdnt%>" />
					    <input type="hidden" name="srchDivs" value="01" />
							<fieldset class="w100">
							<legend></legend>
								<div class="boxStyle">
									<div class="box1 floatDiv">
										<p class="fl mt5 w15"><img src="/images/2011/content/sch_txt.gif" alt="Search" title="Search"></p>
										<table class="fl schBoxGrid w70" summary="">
											<caption></caption>
											<colgroup><col width="10%"><col width="35%"><col width="15%"><col width="*"></colgroup>
											<tbody>
												<tr>
													<th scope="row"><label for="srchText">제목</label></th>
													<td>
														<input id="srchText" name="srchText" value="<%=srchText%>" size="35" onkeyup="fn_enterCheck(this)" title="검색어" class="inputData w80" />
													</td>
													<th scope="row"><label for="sch5">등록일자</label></th>
													<td><input type="text" id="sch5" name="srchStartDate" title="등록일자 시작일시" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="<%=srchStartDate%>" class="w30">
													<img title="등록일자 시작일시를 선택하세요." class="vmid" src="/images/2011/common/calendar.gif"  onclick="javascript:fn_cal('form1','srchStartDate');" onkeypress="javascript:fn_cal('form1','srchStartDate');" alt="달력" align="middle" style="cursor:pointer;" />
													 &nbsp;~<input type="text" class="w30 ml10" id="sch6" name="srchEndDate" title="등록일자 마지막일시" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="<%=srchEndDate%>">
													 <img title="등록일자 마지막일시를 선택하세요." class="vmid" src="/images/2011/common/calendar.gif" onclick="javascript:fn_cal('form1','srchEndDate');" onkeypress="javascript:fn_cal('form1','srchEndDate');" alt="달력" align="middle" style="cursor:pointer;"/></td>
												</tr>
											</tbody>
										</table>
										<p class="fl btn_area">
										<input type="image" src="/images/2011/button/sch.gif" onclick = "javascript:board_search();" alt="검색" title="검색">
									</p></div>
									<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
								</div>
							</fieldset>
						</form>
						<!-- //검색 -->
						
						<!-- 테이블 리스트 Set -->
						<div class="section mt20">
							<div class="result_area floatDiv">
								<p class="tab fl"><span class="tab2"><strong class="orange">${boardList.totalRow}</strong>건 검색</span></p>
							</div>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
								<colgroup>
								<col width="8%">
								<col width="*">
								<col width="15%">
								<col width="20%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">번호</th>
										<th scope="col">제목</th>
										<th scope="col">답변여부</th>
										<th scope="col">등록일자</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${boardList.totalRow == 0}">
										<tr>
											<td class="ce" colspan="4">등록된 게시물이 없습니다.</td>
										</tr>
									</c:if>
									<c:if test="${boardList.totalRow > 0}">
									<c:forEach items="${boardList.resultList}" var="board">
								    <c:set var="NO" value="${board.TOTAL_CNT}"/>
							        <c:set var="i" value="${i+1}"/>
										<tr>
											<td class="ce"><c:out value="${NO - i}"/></td>
											<td><a href="#1" onclick="javascript:boardDetail('${board.BORD_SEQN}','<%=menuSeqn%>','${board.THREADED}')">${board.TITE}</a></td>
											<td class="ce">
											<c:choose>
												<c:when test="${board.REPL_YN eq 0}">
												미완료
												</c:when>
												<c:otherwise>
												완료
												</c:otherwise>
											</c:choose>
											</td>
											<td class="ce">${board.RGST_DTTM}</td>
										</tr>
										</c:forEach>
			    					</c:if>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<!-- 페이징 -->
							<div class="pagination">
							<ul>
								<li>
						 		<%-- 페이징 리스트 --%>
								  <jsp:include page="../common/PageList_2011.jsp" flush="true">
									  <jsp:param name="totalItemCount" value="${boardList.totalRow}" />
										<jsp:param name="nowPage"        value="${param.page_no}" />
										<jsp:param name="functionName"   value="goPage" />
										<jsp:param name="listScale"      value="" />
										<jsp:param name="pageScale"      value="" />
										<jsp:param name="flag"           value="M01_FRONT" />
										<jsp:param name="extend"         value="no" />
									</jsp:include>
								</li>	
							</ul>
						</div>
						<!-- //페이징 -->
							
						</div>
						<!-- //테이블 리스트 Set -->
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2011/footer.jsp" /> --%>
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>
