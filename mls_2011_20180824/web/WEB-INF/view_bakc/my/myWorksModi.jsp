<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="java.util.List"%>
<%@page import="kr.or.copyright.mls.worksPriv.model.WorksPriv"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	String crId = request.getParameter("crId");
	WorksPriv worksPrivDTO = (WorksPriv)request.getAttribute("worksPriv");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>나의  저작물 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript"><!--
function fn_worksPrivList(){
		var frm = document.form1;
		frm.action = "/worksPriv/worksPriv.do";
		frm.submit();
}

function showAttach(cnt) {
		var i=0;
		var content = "";
		var attach = document.getElementById("attach")
		attach.innerHTML = "";
		//document.all("attach").innerHTML = "";
		for (i=0; i<cnt; i++) {
			content += '<input type="file" name="attachfile'+(i+1)+'" size="52;" class="inputFile mt5"><br />';
		}
		//document.all("attach").innerHTML = content;
		attach.innerHTML = content;
	}

	function applyAtch() {
    var ansCnt = document.form1.atchCnt.value;
    showAttach(parseInt(ansCnt));
	}


//셀렉트 박스 변경
function fn_Chkind(){
	var frm = document.form1;
	/*alert(frm.genreCode.value);*/
	var code = frm.genreCode.value;
	var crId = frm.crId.value;
	var submitType = frm.submitType.value;
	frm.method = "post";
	frm.target = "";
	frm.action = "/worksPriv/worksPriv.do?method=goView&genreCode="+code+"&crId="+crId+"&submitType="+submitType;
	frm.submit(); 
}
function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}

//숫자만 입력
function only_arabic(t){
	var key = (window.netscape) ? t.which :  event.keyCode;

	if (key < 45 || key > 57) {
		if(window.netscape){  // 파이어폭스
			t.preventDefault();
		}else{
			event.returnValue = false;
		}
	} else {
		//alert('숫자만 입력 가능합니다.');
		if(window.netscape){   // 파이어폭스
			return true;
		}else{
			event.returnValue = true;
		}
	}	
}
	
// 날짜체크 
function checkValDate(){
	var f = document.form1;

	if(f.publYmd.value!=''){
		if(f.publYmd.value.length != 8){
			alert('일자형식은 8자리 입니다.');
			f.publYmd.value='';
			return false;
		}
	}
}

var curChkNum =0;
var maxNum = 3; 

function trust_check(chk){
	
	if(document.form1.elements[chk].checked != false){
		if(curChkNum < maxNum){
			curChkNum++;
			/*alert(curChkNum);*/
			
			
			if(document.getElementById("regi014").checked){
				if(document.getElementById("regi012").checked == false){
					document.getElementById("regi012").checked = true;
					
					curChkNum++;
				}
				if(document.getElementById("regi015").checked){
					alert("[변경금지]와 [동일조건변경허락]은 함께 선택 할 수 없습니다.");
					document.getElementById("regi014").checked = false;
					curChkNum--;
				}
			}
			if(document.getElementById("regi015").checked){
				if(document.getElementById("regi012").checked == false){
					document.getElementById("regi012").checked = true;
					curChkNum++;
				}
				if(document.getElementById("regi014").checked){
					alert("[변경금지]와 [동일조건변경허락]은 함께 선택 할 수 없습니다.");
					document.getElementById("regi015").checked = false;
					curChkNum--;
				}
			}
			if(document.getElementById("regi013").checked){
				if(document.getElementById("regi012").checked == false){
					document.getElementById("regi012").checked = true;
					curChkNum++;
				}
			}
		}else{
			if(document.getElementById("regi011").checked == false){
				document.form1.elements[chk].checked = false;
				alert("[변경금지]와 [동일조건변경허락]은 함께 선택 할 수 없습니다.");
			}
		}
	}
	else{
		curChkNum--;
	}
	if(document.getElementById("regi011").checked){
		/*alert("비활성화");*/
		curChkNum = 1;
		document.getElementById("regi012").disabled = true;
		document.getElementById("regi013").disabled = true;
		document.getElementById("regi014").disabled = true;
		document.getElementById("regi015").disabled = true;
		document.getElementById("regi012").checked = false;
		document.getElementById("regi013").checked = false;
		document.getElementById("regi014").checked = false;
		document.getElementById("regi015").checked = false;
	}else{
		if(document.getElementById("regi012").checked == true){
			document.getElementById("regi012").disabled = true;
		}
		else{
			document.getElementById("regi012").disabled = false;
		};
		
		document.getElementById("regi013").disabled = false;
		document.getElementById("regi014").disabled = false;
		document.getElementById("regi015").disabled = false;
	}
}

function checkDetailUrl(strUrl) {
	var expUrl = /^(http\:\/\/)?((\w+)[.])+(asia|biz|cc|cn|com|de|eu|in|info|jobs|jp|kr|mobi|mx|name|net|nz|org|travel|tv|tw|uk|us)(\/(\w*))*$/i;
	return expUrl.test(strUrl);
}

function fn_worksPrivUpdate() {
    var frm = document.form1;
    /*alert(frm.rgstIdnt.value);*/
    
    if(frm.worksUrl.value != ""){
   		if(frm.worksUrl.value.substr(0,7) != "http://"){
   			alert('\"http://\"를 입력해주세요.');
   			frm.worksUrl.focus();
   			return false;
   		}else if(!checkDetailUrl(frm.worksUrl.value)) {
   			alert('유효하지 않은 Url 주소 입니다.');
   			frm.worksUrl.focus();
   			return false;
   		}
   	}
    
    if(document.getElementById("regi011").checked){
    	frm.cclCode.value = "0";
    }else if(document.getElementById("regi012").checked){
    	frm.cclCode.value = "1";
    	if(document.getElementById("regi013").checked){
    		frm.cclCode.value = "2";
    		if(document.getElementById("regi014").checked){
    			frm.cclCode.value = "5";
    		}else if(document.getElementById("regi015").checked){
    			frm.cclCode.value = "6";
    		}
		}else if(document.getElementById("regi014").checked){
			frm.cclCode.value = "3";
		}else if(document.getElementById("regi015").checked){
			frm.cclCode.value = "4";
		}
	}
	
    if (frm.genreCode.value == "0") {
		  alert("분야를 선택하십시오.");
		  frm.genreCode.focus();
		  return;
	} else if (frm.kindCode.value == "0") {
		  alert("해당 분야 종류를 선택하십시오.");
		  frm.kindCode.focus();
		  return;
    } else if (frm.creaYear.value == "") {
		  alert("창작년도를 입력하십시오.");
		  frm.creaYear.focus();
		  return;
    } else if (frm.titleName.value == "") {
		  alert("저작물명을 입력하십시오.");
		  frm.titleName.focus();
		  return;
    } else if (frm.worksDesc.value == "") {
		  alert("저작물정보 내용을 입력하십시오.");
		  frm.worksDesc.focus();
		  return;
  	} else if (frm.worksUrl.value == "" && frm.attachfile0.value == "") {
		  alert("저작물확인 부분을 입력하십시오.");
		  frm.worksUrl.focus();
		  return;
  	} else if (frm.coptHodr.value == "") {
		  alert("저작권자를 입력하십시오.");
		  frm.coptHodr.focus();
		  return;
  	} else if (frm.coptHodrDesc.value == "") {
		  alert("저작물정보 내용을 입력하십시오.");
		  frm.coptHodrDesc.focus();
		  return;
  	} else if(curChkNum==0){
  		alert("이용허락 표시를 체크하십시오.");
  		return;
  	}else {
	  	frm.action = "/worksPriv/worksPriv.do?method=goUpdate";
  	  	frm.submit();
	  }
  }
  function editTable(type){

		var chkId = "chk";
		
		if( type == 'D' ) {

		    var oChkDel = document.getElementsByName(chkId);
		    var iChkCnt = oChkDel.length;
		    var iDelCnt = 0;


		    if(iChkCnt == 1 && oChkDel[0].checked == true){
		        iDelCnt++;
		    }else if(iChkCnt > 1){
	    	    for(i = iChkCnt-1; i >= 0; i--){
	    	        if(oChkDel[i].checked == true){    
	    	            iDelCnt++;
	    	        }
	    	    }
	    	}

	    	if(iDelCnt < 1){
	    	    alert('삭제할 첨부파일을 선택하여 주십시요');
	    	}else{
				result = confirm('삭제 하시겠습니까');
			    if(result == false){

				    if(iChkCnt == 1 && oChkDel[0].checked == true){
					    oChkDel[0].checked = false;
				    }else if(iChkCnt > 1){
			    	    for(i = iChkCnt-1; i >= 0; i--){
			    	        if(oChkDel[i].checked == true){    
			    	        	oChkDel[i].checked = false;
			    	        }
			    	    }
			    	}
			    	return;
			    }	
	    	}		
		    
		    if(iChkCnt == 1 && oChkDel[0].checked == true){
		    	document.getElementById("chk2_0").style.display = "none";
		        iDelCnt++;
		    }else if(iChkCnt > 1){
	    	    for(i = iChkCnt-1; i >= 0; i--){
	    	        if(oChkDel[i].checked == true){    
	    	        	document.getElementById("chk2_"+i).style.display = "none";
	    	            iDelCnt++;
	    	        }
	    	    }
	    	}
		}
	}
	
function init(){
	<c:choose>
		<c:when test="${worksPriv.cclCode eq 7}" >
			curChkNum = 3;
			document.getElementById("regi012").checked = true;
			document.getElementById("regi013").checked = true;
			document.getElementById("regi015").checked = true;
		</c:when>
		<c:when test="${worksPriv.cclCode eq 6}" >
			curChkNum = 3;
			document.getElementById("regi012").checked = true;
			document.getElementById("regi013").checked = true;
			document.getElementById("regi014").checked = true;
		</c:when>
		<c:when test="${worksPriv.cclCode eq 5}" >
			curChkNum = 2;
			document.getElementById("regi012").checked = true;
			document.getElementById("regi015").checked = true;
		</c:when>
		<c:when test="${worksPriv.cclCode eq 4}" >
			curChkNum = 2;
			document.getElementById("regi012").checked = true;
			document.getElementById("regi014").checked = true;
		</c:when>
		<c:when test="${worksPriv.cclCode eq 3}" >
			curChkNum = 2;
			document.getElementById("regi012").checked = true;
			document.getElementById("regi013").checked = true;
		</c:when>
		<c:when test="${worksPriv.cclCode eq 2}" >
			curChkNum = 1;
			document.getElementById("regi012").checked = true;
		</c:when>
		<c:when test="${worksPriv.cclCode eq 1}" >
			/*alert("비활성화");*/
			curChkNum = 1;
			document.getElementById("regi011").checked = true;
			document.getElementById("regi012").disabled = true;
			document.getElementById("regi013").disabled = true;
			document.getElementById("regi014").disabled = true;
			document.getElementById("regi015").disabled = true;
			document.getElementById("regi012").checked = false;
			document.getElementById("regi013").checked = false;
			document.getElementById("regi014").checked = false;
			document.getElementById("regi015").checked = false;
		</c:when>
		<c:otherwise>
		/*alert("코드?");*/
		</c:otherwise>
	</c:choose>
}
//--></script>
</head>
<body onload="init();">
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2011/header.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_8.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<jsp:include page="/include/2011/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb2");
				</script>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>저작권콘텐츠</span><em>나의 저작물</em></p>
					<h1><img src="/images/2011/title/content_h1_0803.gif" alt="나의 저작물" title="나의 저작물" /></h1>
					
					<div class="section">
						<form name="form1" method="post" enctype="multipart/form-data" action="#">
						<input type="hidden" name="crId" value="<%=worksPrivDTO.getCrId()%>" />
						<input type="hidden" name="cclCode" value="<%=worksPrivDTO.getCclCode()%>" />	
						<input type="hidden" name="rgstIdnt" value="<%=sessUserIdnt %>" />
						<input type="hidden" name="submitType" value="update">
						<input type="submit" style="display:none;">
						<!-- article str -->
						<div class="article">
							<div class="floatDiv">
								<p class="fl p11 black">( <img src="/images/2011/common/necessary.gif" alt="" /> ) 항목은 필수입력사항이므로 빠짐없이 기입하여주시기 바랍니다.</p>
								<!--<p class="fr"><span class="button small icon"><a href="">예시화면보기</a><span class="help"></span></span></p>
							-->
							</div>
							<h2>저작물정보</h2>
							
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="30%">
								<col width="30%">
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="genreCode" class="necessary">분야</label></th>
										<td colspan="3">
										<select id="genreCode" name="genreCode" onchange="javascript:fn_Chkind();">
										<c:forEach items="${genreCodeList}" var="genreCodeList">
											<option value="${genreCodeList.code}" <c:if test="${genreCodeList.code==genreCode}">selected</c:if> >${genreCodeList.codeName}</option>
										</c:forEach>
										</select></td>
									</tr>
									<tr>
										<th scope="row"><label for="kindCode" class="necessary">종류</label></th>
										<td><select id="kindCode" name="kindCode">
										<c:forEach items="${kindCodeList}" var="kindCodeList">
											<option value="${kindCodeList.code}" <c:if test="${kindCodeList.code==worksPriv.kindCode}">selected</c:if> >${kindCodeList.codeName}</option>
										</c:forEach>
										</select></td>
										<th scope="row"><label for="creaYear" class="necessary">창작년도</label></th>
										<td><input type="text" id="creaYear" name="creaYear" class="w60" size="4" maxlength="4" value="<%=worksPrivDTO.getCreaYear()%>" ></td>
									</tr>
									<tr>
										<th scope="row"><label for="titleName" class="necessary">저작물명</label></th>
										<td colspan="3"><input type="text" id="titleName" name="titleName" class="w90" value="<%=worksPrivDTO.getTitle()%>"></td>
									</tr>
									<tr>
										<th scope="row"><label for="publCode">공표매체</label></th>
										<td colspan="3"><select id="publCode" name="publCode">
										<c:forEach items="${publCodeList}" var="publCodeList">
											<option value="${publCodeList.code}" <c:if test="${publCodeList.code==worksPriv.publCode}">selected</c:if> >${publCodeList.codeName}</option>
										</c:forEach>
										</select> <input type="text" class="w50" name="publName" title="" value="<%=worksPrivDTO.getPublName()%>" ></td>
									</tr>
									<tr>
										<th scope="row"><label for="publYmd">공표일자</label></th>
										<td colspan="3"><input type="text" id="publYmd" name="publYmd" title="공표일자" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="<%=worksPrivDTO.getPublYmd() %>" class="w10">
										<img title="공표일자를 선택하세요." alt="" class="vmid" src="/images/2011/common/calendar.gif"  onclick="javascript:fn_cal('form1','publYmd');" onkeypress="javascript:fn_cal('form1','publYmd');" alt="달력" align="middle" style="cursor:pointer;" /></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="worksDesc">내용</label></th>
										<td colspan="3">
										<textarea name="worksDesc" id="worksDesc" cols="10" rows="3" class="w90"><%=worksPrivDTO.getWorksDesc()%></textarea>
										</td>
									</tr>
									<tr>
										<th scope="row"><label for="worksUrl" class="necessary">저작물확인</label></th>
										<td colspan="3">
											<span class="topLine2"></span>
											<!-- 그리드스타일 -->
											<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
												<colgroup>
												<col width="20%">
												<col width="*">
												</colgroup>
												<tbody>
													<tr>
														<th scope="row"><label for="worksUrl">URL</label></th>
														<td><input type="text" id="worksUrl" name="worksUrl" class="w85" value="<%=worksPrivDTO.getWorksUrl()%>" ></td>
													</tr>
													<%
									          		List fileList = (List) worksPrivDTO.getFileList();
									          		int listSize = fileList.size();
													%>
													<tr>
														<th scope="row"><label for="attachfile0">파일첨부</label></th>
														<td>
														<select name="atchCnt" id="atchCnt" title="첨부파일수" onChange="javascript:applyAtch();" class="mb5">
														<c:forEach begin="1" end="10" step="1" varStatus="cnt">
															<option value="<c:out value="${cnt.count}"/>"><c:out value="${cnt.count}"/></option>
														</c:forEach>
														</select>
														<ul>
												    		<li>※ 첨부파일 수를 선택해주세요.</li>
												  		</ul>
															<div id="attach">
																<input type="file" id="attachfile0" name="attachfile0" size="52" class="inputFile">
															</div>
														<p>
															<% if (listSize > 0) { %>
																<br>
																<ul>
															    	<li>※ 삭제를 원하는 파일은 체크하십시요.(파일은 '저장하기' 버튼을 클릭할 경우 완전히 삭제 됩니다.)</li>
															  	</ul>
															<% } %>
															<%
															  for(int i=0; i<listSize; i++) {
																WorksPriv fileDTO = (WorksPriv) fileList.get(i);
															%>
															    <div id="chk2_<%=i%>"><input type="checkbox" name="chk" title="삭제파일 선택" id="chk_<%=i%>" value="<%=fileDTO.getAttcSeqn()%>,<%=fileDTO.getFileName()%>"><%=fileDTO.getFileName()%></div>
															<%
																}
															%>
															<% if(listSize != 0){ %>
															<span class="button small icon" title="[첨부파일]을 삭제합니다.">
																<a href="#1" onclick="javascript:editTable('D');">삭제</a>
															<span class="delete"></span>
															</span>
															<%} %>
														</td>
													</tr>
												</tbody>
											</table>
											<!-- //그리드스타일 -->
										</td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary">이용허락 표시</label></th>
										<td colspan="3">
										<input type="checkbox" id="regi011" onclick="javascript:trust_check(this.id);" />
										<label for="regi011" class="p12 mr10">이용금지</label>
										<input type="checkbox" id="regi012" onclick="javascript:trust_check(this.id);" />
										<label for="regi012" class="p12 mr10">저작자표시</label>
										<input type="checkbox" id="regi013" onclick="javascript:trust_check(this.id);" />
										<label for="regi013" class="p12 mr10">비영리</label>
										<input type="checkbox" id="regi014" onclick="javascript:trust_check(this.id);" />
										<label for="regi014" class="p12 mr10">변경금지</label>
										<input type="checkbox" id="regi015" onclick="javascript:trust_check(this.id);" />
										<label for="regi015" class="p12 mr10" >동일조건변경허락</label>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<h2 class="mt20">저작권 정보</h2>
							
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row"><label for="coptHodr" class="necessary">저작권자</label></th>
										<td><input type="text" id="coptHodr" name="coptHodr" class="w90" value="<%=worksPrivDTO.getCoptHodr() %>"></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="coptHodrDesc">내용</label></th>
										<td><textarea name="coptHodrDesc" id="coptHodrDesc" cols="10" rows="3" class="w90"><%=worksPrivDTO.getCoptHodrDesc()%></textarea></td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
														
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_worksPrivList();">목록</a></span></p>
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:fn_worksPrivUpdate();">수정하기</a></span></p>
							</div>
						</div>
						<!-- //article end -->
						</form>
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2011/footer.jsp" /> --%>
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
</body>
</html>
