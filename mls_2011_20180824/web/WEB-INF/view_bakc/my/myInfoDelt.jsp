<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
  String submitChek = request.getParameter("submitChek") == null ? "" : request.getParameter("submitChek");
  String submitChek2 = (String) request.getAttribute("submitChek2");

	User user = SessionUtil.getSession(request);
	
	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>회원탈퇴 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_main.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<script src="/js/Function.js" type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script src="/js/general.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/JavaScript">
<!--
function goPostNumbSrch() {
  window.open('/user/user.do?method=goPostNumbSrch','win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=950, height=860');
}

function updateUserInfo() {
	var frm = document.form1;
  if (frm.pswd.value == "") {
		alert("비밀번호를 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value.length < 12) {
		alert("비밀번호는 12자리 이상 입력하십시오.");
		frm.pswd.focus();
		return;
	} else if (frm.pswd.value != frm.pswdConf.value) {
		alert("입력하신 비밀번호가 다릅니다. 입력하신 비밀번호를 확인하십시오");
		frm.pswd.focus();
		return;
	} else if (frm.scssDesc.value == "") {
		alert("탈퇴사유를 적어주세요.");
		frm.scssDesc.focus();
		return;
	} /* else if (frm.pswd.value.toUpperCase() != frm.tmpPswd.value) {
		alert("비밀번호가 다릅니다. 비밀번호를 확인하십시오.");
		frm.pswd.focus();
		return;
	} */else {
		if(confirm("정말 탈퇴 하시겠습니까?")){
			frm.action = "/user/user.do?DIVS=D&logout=Y";
			
	  		frm.submit();
	  }
	}
}

function init() {
	alert("정상적으로 탈퇴 되었습니다.");
}

function init2() {
	var frm = document.form1;
	
	if(frm.pswdYsno.value =="N"){
		alert("비밀번호가 다릅니다. 비밀번호를 확인하십시오.");
		frm.pswd.focus();
		return;
	}
}

function fn_selectUserInfo() {
	var frm = document.userInfoModi;

	frm.action = "/user/user.do";
	frm.submit();
}
//-->
</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-69621660-2', 'auto');
	  ga('send', 'pageview');
</script>
</head>
<%  if ("Y".equals(submitChek) && "".equals(submitChek2)) {  %>
<body onLoad="init();">
<%  } else if ("N".equals(submitChek2)) {  %>
<body onLoad="init2();">
<%  } %>
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/mainHeader.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(0);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end --> 
		
		<!-- CONTAINER str-->
		<div id="contents">
			
				<!-- 래프 -->
				<div class="con_lf" style="width: 22%">
					<div class="con_lf_big_title">마이페이지</div>
					<ul class="sub_lf_menu">
						<li><a href="#">신청현황</a>
							<ul class="sub_lf_menu2 disnone">
								<li><a href="/statBord/statBo06List.do?bordCd=6">저작권자 찾기위한<br/>상당한 노력 신청</a></li>
								<li><a href="/myStat/statRsltInqrList.do">법정허락 승인신청</a></li>
								<li><a href="/statBord/statBo01ListMy.do?bordCd=1">저작권자 조회 공고</a></li>
								<li><a href="/statBord/statBo05ListMy.do?bordCd=5">보상금 공탁 공고</a></li>
							</ul>
						</li>
						<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>" class="on">회원정보</a>
							<ul class="sub_lf_menu2">
								<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보 수정</a></li>
								<li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=D&amp;userIdnt=<%=sessUserIdnt%>" class="on">회원탈퇴</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<!-- //래프 -->
				<!-- 주요컨텐츠 str -->
				<div class="con_rt" id="contentBody">
					<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						마이페이지
						&gt;
						회원정보
						&gt;
						<span class="bold">회원탈퇴</span>
					</div>
					<div class="con_rt_hd_title">회원탈퇴</div>
					<div class="section">
						<form name="form1" method="post" class="frm" action="#">
							<input type="hidden" name="userIdnt" value="${userInfoMap.USER_IDNT}"/>
							<input type="hidden" name="zip"/>
							<input type="hidden" name="tmpPswd" value="${userInfoMap.PSWD}"/>
							<input type="hidden" name="srchAddr"/>
							<input type="hidden" name="pswdYsno" value="${pswdYsno}"/>
							<input type="hidden" name="method" value="updateUserInfo"/>
							<input type="hidden" name="submitChek" value="Y"/>
							<input type="hidden" name="mlsUserIdnt" value="${userInfoMap.USER_IDNT}"/>
							<input type="submit" style="display:none;">
						<!-- article str -->
						<div class="article">
							<div class="floatDiv">
								<p class="fl p11 black">( <img alt="" src="/images/2012/common/necessary.gif"> ) 항목은 필수입력사항이므로 빠짐없이 기입하여주시기 바랍니다.</p>
							</div>
							<br/>
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" summary="" class="grid">
								<colgroup>
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">회원구분</th>
										<td>${userInfoMap.USER_DIVS_NAME}</td>
									</tr>
									<tr>
										<th scope="row">이름/사업자명</th>
										<td>${userInfoMap.USER_NAME}</td>
									</tr>
									<c:if test="${userInfoMap.USER_DIVS != '03'}">
									<tr>
										<th scope="row">법인번호</th>
										<td>
										<c:if test="${userInfoMap.RESD_CORP_NUMB != ''}">
										<input type="text" class="inputData" name="resdCorpNumb1" id="resdCorpNumb1" title="주민/법인번호(1번째)" style="border-width:0px; width:39px; text-align:right;" value="${userInfoMap.RESD_CORP_NUMB1}" readonly="readonly" />
										-<input type="password" class="inputData" name="resdCorpNumb1" id="resdCorpNumb2" title="주민/법인번호(2번째)"style="border-width:0px; width:85px; text-align:left;" value="${userInfoMap.RESD_CORP_NUMB2}" readonly="readonly" />
										</c:if>
										</td>
									</tr>
									</c:if>
									<c:if test="${userInfoMap.USER_DIVS != '01'}">
										<tr>
											<th>사업자번호</th>
											<td>${fn:substring(userInfoMap.CORP_NUMB,0,3)}-${fn:substring(userInfoMap.CORP_NUMB,3,5)}-${fn:substring(userInfoMap.CORP_NUMB,5,10)}</td>
										</tr>
									</c:if>
									<tr>
										<th scope="row"><label class="necessary" for="pswd">비밀번호</label></th>
										<td><input type="password" class="w20" id="pswd" name="pswd"><span class="p11 ml10">(영문자,숫자,특수문자를 혼용한 12자리 이상으로 입력하십시오)</span></td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="psConf">비밀번호 확인</label></th>
										<td><input type="password" class="w20" name="pswdConf" id="psConf"></td>
									</tr>
									<tr>
										<th scope="row">이메일주소</th>
										<td>${userInfoMap.MAIL}</td>
									</tr>
									<tr>
										<th scope="row">주소</th>
										<td>${fn:substring(userInfoMap.ZIPX_CODE, 0, 3)}${fn:substring(userInfoMap.ZIPX_CODE, 3, 6)}<br />
											${userInfoMap.ADDR} ${userInfoMap.DETL_ADDR}</td>
									</tr>
									<!-- 2014.02.18 개인정보 보호 관련 hide
									<tr>
										<th scope="row">자택전화번호</th>
										<td>${userInfoMap.TELX_NUMB}</td>
									</tr>
									-->
									<tr>
										<th scope="row">핸드폰번호</th>
										<td>${userInfoMap.MOBL_PHON}</td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="mailReceYsno">수신방법</label></th>
										<td>
											<input type="checkbox" name="mailReceYsno"  id="mailReceYsno" onclick="javascript:return(false);"  value="Y" <c:if test="${userInfoMap.MAIL_RECE_YSNO == 'Y' }" > checked="checked"</c:if> class="inputChk" /><label class="p12 mr10" for="mailReceYsno">이메일</label>
											<input type="checkbox" name="smsReceYsno" id="smsReceYsno" onclick="javascript:return(false);" value="Y" <c:if test="${userInfoMap.SMS_RECE_YSNO == 'Y' }"> checked="checked"</c:if> class="inputChk" /><label class="p12 mr10" for="smsReceYsno">핸드폰</label>
										</td>
									</tr>
									<tr>
										<th scope="row"><label class="necessary" for="scssDesc">탈퇴사유</label></th>
										<td colspan="3">
											<textarea name="scssDesc" id="scssDesc" cols="10" rows="3" class="w90"></textarea>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:document.form1.reset();">취소</a></span></p>
								<p class="fr"><span class="button medium"><a href="#1" onclick="javascript:updateUserInfo();">탈퇴하기</a></span></p>
							</div>
							
						</div>
						<!-- //article end -->
					</form>							
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
				<p class="clear"></p>
			</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->

</body>
</html>
