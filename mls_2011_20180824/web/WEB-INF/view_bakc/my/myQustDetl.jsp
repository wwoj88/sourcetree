<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%
	String bordSeqn = request.getParameter("bordSeqn");
	String menuSeqn = request.getParameter("menuSeqn");
	String threaded = request.getParameter("threaded");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String srchStartDate = request.getParameter("srchStartDate") == null ? "" : request.getParameter("srchStartDate");
	String srchEndDate = request.getParameter("srchEndDate") == null ? "" : request.getParameter("srchEndDate");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");

	Board boardDTO = (Board) request.getAttribute("board");
	User user = SessionUtil.getSession(request);
	
	String sessUserIdnt = user.getUserIdnt();
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>묻고답하기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css">
<link type="text/css" rel="stylesheet" href="/css/2011/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2011/deScript.js"></script>
<script type="text/JavaScript">
<!--
  window.name = "boardView";

  function fn_qustList(){
		var frm = document.form1;
		frm.menuSeqn.value = 9;
		frm.action = "/board/board.do";
		frm.submit();
	}

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;
		
		frm.target="boardView";
		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }
	  
  function fn_update(){
	  var frm =document.form1;

		if (frm.pswd.value == "") {
			alert("비밀번호를 입력하십시오.");
			frm.pswd.focus();
			return;
		} else if(isPwdValidate()){
			alert("알림마당 > 묻고 답하기 > 수정페이지로 이동하겠습니다.");
			frm.method = "post";
			frm.action = "/board/board.do?method=boardView";
			frm.submit();
		}
	}

  function fn_delete(){

	  var frm =document.form1;

		if (frm.pswd.value == "") {
			alert("비밀번호를 입력하십시오.");
			frm.pswd.focus();
			return;
		} else if(isPwdValidate()){
			if(confirm("답변내용도 같이 삭제됩니다\n내용을 삭제 하시겠습니까?")){
			  frm.method = "post";
			  frm.action = "/board/board.do?method=deleteQust";
			  frm.submit();
			}
		}
	}

	// 비밀번호 체크
	function isPwdValidate(){
		var bordSeqn = document.form1.bordSeqn.value;
		var menuSeqn = document.form1.menuSeqn.value;
		var pswd     = document.form1.pswd.value;
		var threaded = document.form1.threaded.value;
		var url = "/board/board.do?method=isValidPwd&bordSeqn=" + bordSeqn + "&menuSeqn=" + menuSeqn + "&pswd=" + pswd + "&threaded=" + threaded;
	  if(cfGetBooleanResponseReload(url)){
		    return true;
		} else {
			alert("비밀번호 오류입니다.");
			document.form1.pswd.value = "";
			document.form1.pswd.focus();
		  return false;
		}
  }

	/*
	*  Function.js에 있으나 파이어폭스에서는 인식이 안되서 가져왔음
	*/
	function cfGetBooleanResponseReload(url,params,HttpMethod) {
		var xmlhttp = null;
		if(!HttpMethod){
		    HttpMethod = "GET";
		}
	    if(window.ActiveXObject){
		      try {
		    	  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		      } catch (e) {
		        try {
		        	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP") ;
		        } catch (e2) {
		          return null ;
		        }
			}
		}
		//xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		if (xmlhttp == null) return true;
		xmlhttp.open(HttpMethod, url, false);
		xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
	
		xmlhttp.send(params);
		
		if (xmlhttp.responseText == "true") {
			return true;
		} else {
			return false;
		}
	}
	//숫자만 입력
	function only_arabic(t){
		var key = (window.netscape) ? t.which :  event.keyCode;
	
		if (key < 45 || key > 57) {
			if(window.netscape){  // 파이어폭스
				t.preventDefault();
			}else{
				event.returnValue = false;
			}
		} else {
			//alert('숫자만 입력 가능합니다.');
			if(window.netscape){   // 파이어폭스
				return true;
			}else{
				event.returnValue = true;
			}
		}	
	}	 	  
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2011/header.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(4);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="container_vis">
				<h2><span><img src="/images/2011/title/container_vis_h2_8.gif" alt="마이페이지" title="마이페이지" /><em><img src="/images/2011/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2011/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
				<!-- 래프 -->
				<jsp:include page="/include/2011/myPageLeft.jsp" />
				<script type="text/javascript">
					subSlideMenu("sub_lnb","lnb2");
				</script>
				<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>마이페이지</span><span>저작권콘텐츠</span><em>나의 질문과 답</em></p>
					<h1><img src="/images/2011/title/content_h1_0804.gif" alt="나의 질문과 답" title="나의 질문과 답" /></h1>
					
					<div class="section">
					<form name="form1" method="post" action = "#">
								<input type="hidden" name="bordSeqn" value="${board.bordSeqn}">
								<input type="hidden" name="menuSeqn" value="${board.menuSeqn}">
								<input type="hidden" name="threaded" value="${board.threaded}">
								<input type="hidden" name="rgstIdnt" value="${board.rgstIdnt}">
								<input type="hidden" name="page_no" value="<%=page_no%>">
								<input type="hidden" name="srchText" value="<%=srchText%>">
								<input type="hidden" name="srchStartDate" value="<%=srchStartDate%>">
								<input type="hidden" name="srchEndDate" value="<%=srchEndDate%>">
								<input type="hidden" name="submitType" value="update">
								<input type="hidden" name="deth" value="0">
								<input type="hidden" name="filePath">
								<input type="hidden" name="fileName">
								<input type="hidden" name="realFileName">
								<input type="submit" style="display:none;">
						<!-- article str -->
						<div class="article">
							
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="20%">
								<col width="30%">
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">제목</th>
										<td colspan="3">${board.tite}</td>
									</tr>
									<tr>
										<th scope="row">이름</th>
										<td>${board.rgstName}</td>
										<th scope="row">등록일자</th>
										<td>${board.rgstDate}</td>
									</tr>
									<tr>
										<th scope="row">이메일</th>
										<td>${board.mail}</td>
										<th scope="row"><label for="pswd">비밀번호</label></th>
										<td><input class="w50" type="password" id="pswd" name="pswd" onkeypress="javascript:only_arabic(event);" ></td>
									</tr>
									<tr>
										<th scope="row">내용</th>
										<td colspan="3">
											<%//=CommonUtil.replaceBr(boardDTO.getBordDesc(),true)%>
											<br/>
											<%=boardDTO.getBordDescTag()%>
											<br/>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- //그리드스타일 -->
							<c:if test="${board.replBordSeqn != null}">
							<div class="floatDiv mt20"><h3 class="fl tab"><span>답변</span></h3></div>							
							<span class="topLine"></span>
							<!-- 그리드스타일 -->
							<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="">
								<colgroup>
								<col width="20%">
								<col width="30%">
								<col width="20%">
								<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">등록자</th>
										<td>${board.replRgstName}</td>
										<th scope="row">등록일자</th>
										<td><%=boardDTO.getReplRgstDate()%></td>
									</tr>
									<tr>
										<th scope="row">답변내용</th>
										<td colspan="3">
										<br/>
										<%=CommonUtil.replaceBr(boardDTO.getReplBordDescTag(),true)%>
										<br/>
										</td>
									</tr>
								</tbody>
							</table>
							</c:if>
							<!-- //그리드스타일 -->
							<div class="btnArea">
								<p class="fl"><span class="button medium gray"><a href="#1" onclick="javascript:fn_qustList();">목록</a></span></p>
								
								<c:if test="${board.replBordSeqn == null }" >
									<p class="fr">
										<span class="button medium"><a href="#1" onclick="javascript:fn_update();">수정하기</a></span>
										<span class="button medium"><a href="#1" onclick="javascript:fn_delete();">삭제하기</a></span>
									</p>
								</c:if>
								
								<c:if test="${board.replBordSeqn != null }" >
									<p class="fr"><span class="button medium fr"><a href="#1" onclick="javascript:fn_delete();">삭제하기</a></span></p>
								</c:if>
								
								
							</div>
						</div>
						<!-- //article end -->
						</form>
												
					</div>
				</div>
				<!-- //주요컨텐츠 end -->
			</div>
		</div>
		<!-- //CONTAINER end -->
		
		<!-- FOOTER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
		<%-- <jsp:include page="/include/2011/footer.jsp" /> --%>
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


</body>
</html>
