<!--
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE> 넣으면 페이지 나오지 않음
-->
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

%>
<HTML>
<HEAD>
    <TITLE>ReportExpress</TITLE>
    <META http-equiv=Content-Type content="text/html; charset=ks_c_5601-1987"/>
    <meta http-equiv="X-UA-Compatible" content="IE=5" />
    <meta http-equiv="Cache-Control" content="no-cache"/> 
    <meta http-equiv="Expires" content="0"/> 
    <meta http-equiv="Pragma" content="no-cache"/> 
    <LINK href="lib/common.css" type="text/css" rel="stylesheet"/>
	<style type="text/css">
	 td.txt {color:#615E5E; font-size:12px; font-family:돋움 , 굴림 ; letter-spacing:-10%; padding-left:10px;padding-bottom:15px; }
	</style>
</HEAD>
<SCRIPT LANGUAGE="javascript" src="/cdoc/rxpp/vista/rptrx_div.js"></SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=ErrorMsg(msg)>
<!--
    if (msg != "")  alert(msg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=PrintResult(flag)>
<!--
//        if (flag == true)
//        	alert("출력을 성공적으로 끝냈습니다.");
//        else
//        	alert("출력을 취소하셨거나 실패하였습니다.");
//-->
</SCRIPT>

<BODY bgColor=white leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>
<!-- 뷰어 설치 스크립트 호출 -->
<Table border=0 width=100% height=100%>
	<Tr>
	<Td width=100% height=100%>
		<div id="Inst" style="display:none"></div>
		<div id="objdiv" style="display:none"></div>
	</Td>
  </Tr>
</Table>
<form name=reportform action="#" method=post style="display:none">
<input type="hidden" name="rpx" value="저작권자를 찾기 위한 상당한 노력 신청.rpx">
<input type="hidden" name="fileurl" value="<%//=fileurl%>">
<input type="submit" style="display:none;">
<TEXTAREA name="xmldata" style="visibility:hidden;height:0;width:0;" title="">	
	<!--  저작권자 찾기위한 상당한노력 신청서 -->

<root>	
	<신청자>${AnucBord.anucItem9}</신청자>
	<신청일자>${AnucBord.rgstDttm}</신청일자>
	<c:forEach items="${SuplList}" var="SuplList">	
		 <c:if test="${SuplList.suplItemCd=='10'}">
		 	<c:set var="anuc1" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='21'}">
		 	<c:set var="anuc2" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='22'}">
		 	<c:set var="anuc3" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='23'}">
		 	<c:set var="anuc4" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='31'}">
		 	<c:set var="genreCdName" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='32'}">
		 	<c:set var="worksTitle" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='40'}">
		 	<c:set var="anuc5" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='51'}">
		 	<c:set var="anuc6" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='52'}">
		 	<c:set var="anuc7" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='60'}">
		 	<c:set var="anuc8" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='81'}">
		 	<c:set var="anuc9" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='82'}">
		 	<c:set var="anuc10" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='83'}">
		 	<c:set var="anuc11" value="Y"></c:set>
		 </c:if>
		 <c:if test="${SuplList.suplItemCd=='84'}">
		 	<c:set var="anuc12" value="Y"></c:set>
		 </c:if>		 		 
	</c:forEach>
	
	<신청사유 code='${AnucBord.rgstReasCd}'>
		<c:if test="${AnucBord.rgstReasCd==6}">
		<![CDATA[${AnucBord.rgstReasEtc}]]>
		</c:if>
	</신청사유>
	<신청저작물정보>
		<구분>${AnucBord.genreCdName}</구분>
		<제호><![CDATA[${AnucBord.worksTitle}]]></제호>
		
		<c:forEach items="${coptHodr}" var="coptHodr">
			<저작자>
				<![CDATA[${coptHodr.coptHodrRoleCdName} : ${coptHodr.coptHodrName}]]>
			</저작자>
		</c:forEach>		
		<진행상태>
			<![CDATA[${AnucBord.statCdName}]]>
			<c:if test="${AnucBord.statCd==3}">
			<![CDATA[
(${AnucBord.statRsltCdName})]]>
			</c:if>
		</진행상태> <!--엔터 처리가 되어있으니 cdata 안에 공백을 지우지 마세요. -->
		<상당한노력진행상태>				
			<c:if test="${AnucBord.statRsltCd == 2}">
				<![CDATA[상당한 노력 미진행]]>
			</c:if>
			<c:if test="${AnucBord.statRsltCd != 2}">
				<![CDATA[${AnucBord.systEffortStatCdName}]]>
			</c:if>
		</상당한노력진행상태>
	</신청저작물정보>

	<상당한노력공고정보>
		<제목><![CDATA[${AnucBord.worksTitle} 저작권자 조회 공고]]></제목>
		<공고자><![CDATA[${AnucBord.anucItem9}]]></공고자>
		<공고일자>
			<c:if test="${empty AnucBord.openDttm}">미공고</c:if>
			<c:if test="${!empty AnucBord.openDttm}"><![CDATA[${AnucBord.openDttm}]]></c:if>
		</공고일자>

		<저작권자를찾는다는취지 보완='${anuc1}'><![CDATA[${AnucBord.anucItem1}]]></저작권자를찾는다는취지>
		<저작재산권자의성명등>
			<성명 보완='${anuc2}'><![CDATA[${AnucBord.anucItem2}]]></성명>
			<주소 보완='${anuc3}'><![CDATA[${AnucBord.anucItem3}]]></주소>
			<연락처 보완='${anuc4}'><![CDATA[${AnucBord.anucItem4}]]></연락처>
		</저작재산권자의성명등>
		<저작물의제호>
			<장르 보완='${genreCdName}'>${AnucBord.genreCdName}</장르>
			<제호 보완='${worksTitle}'><![CDATA[${AnucBord.worksTitle}]]></제호>
		</저작물의제호>
		<공표시표시된저작재산권자의성명>
			<성명 보완='${anuc5}'><![CDATA[${AnucBord.anucItem5}]]></성명>
		</공표시표시된저작재산권자의성명>
		<저작물을발행또는공표한자>
			<저작물발행 보완='${anuc6}'><![CDATA[ ${AnucBord.anucItem6}]]></저작물발행>
			<공표연월일 보완='${anuc7}'> ${AnucBord.anucItem7}</공표연월일>
		</저작물을발행또는공표한자>	
		<저작물의이용목적 보완='${anuc8}'><![CDATA[${AnucBord.anucItem8}]]></저작물의이용목적>
		<복제물의표지사진등의자료>
			<c:if test="${!empty fileList }">
				<c:forEach items="${fileList}" var="fileList">
					<파일><![CDATA[${fileList.fileName }]]></파일>
    			</c:forEach>
			</c:if>
			<c:if test="${empty fileList }">
				<파일><![CDATA[없음]]></파일>
			</c:if>
		</복제물의표지사진등의자료>
		<공고자및연락처>		
			<공고자 보완='${anuc9}'><![CDATA[${AnucBord.anucItem9}]]></공고자>
			<주소 보완='${anuc10}'><![CDATA[${AnucBord.anucItem10}]]></주소>
			<연락처 보완='${anuc11}'><![CDATA[${AnucBord.anucItem11}]]></연락처>
			<담당자 보완='${anuc12}'><![CDATA[${AnucBord.anucItem12}]]></담당자>
		</공고자및연락처>
	</상당한노력공고정보>	
	<보완여부 useYn='${AnucBord.suplYn}'> (*) 표시된 항목은 담당자에 의해 보완된 항목입니다.</보완여부>
</root>
	
</TEXTAREA>
</form>
<script language="javascript" src="/cdoc/rxpp/vista/inst_page_div.js?modal=${modal}"></script>

</BODY>
</HTML>