<!--
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE> 넣으면 페이지 나오지 않음
-->
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

%>
<HTML>
<HEAD>
    <TITLE>ReportExpress</TITLE>
    <META http-equiv=Content-Type content="text/html; charset=ks_c_5601-1987"/>
    <meta http-equiv="Cache-Control" content="no-cache"/> 
    <meta http-equiv="Expires" content="0"/> 
    <meta http-equiv="Pragma" content="no-cache"/> 
    <LINK href="lib/common.css" type="text/css" rel="stylesheet"/>
	<style type="text/css">
	 td.txt {color:#615E5E; font-size:12px; font-family:돋움 , 굴림 ; letter-spacing:-10%; padding-left:10px;padding-bottom:15px; }
	</style>
</HEAD>
<SCRIPT LANGUAGE="javascript" src="/cdoc/rxpp/vista/rptrx_div.js"></SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=ErrorMsg(msg)>
<!--
    if (msg != "")  alert(msg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=PrintResult(flag)>
<!--
//        if (flag == true)
//        	alert("출력을 성공적으로 끝냈습니다.");
//        else
//        	alert("출력을 취소하셨거나 실패하였습니다.");
//-->
</SCRIPT>

<BODY bgColor=white leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>
<!-- 뷰어 설치 스크립트 호출 -->
<Table border=0 width=100% height=100%>
	<Tr>
	<Td width=100% height=100%>
		<div id="Inst" style="display:none"></div>
		<div id="objdiv" style="display:none"></div>
	</Td>
  </Tr>
</Table>
<form name=reportform action="#" method=post style="display:none">
<input type="hidden" name="rpx" value="TRST_203_M.rpx">
<input type="hidden" name="fileurl" value="<%//=fileurl%>">
<input type="submit" style="display:none;">
<TEXTAREA name=xmldata style="visibility:hidden;height:0;width:0;" title="">
	
	<!-- 음반(음원)등록명세서 -->
	
	<root>
		<발급일>${clientInfo.RSGT_DTTM_STR}</발급일>    																												 
		<권리자상호>${clientInfo.PRPS_NAME }</권리자상호>                                        
		<사업자등록번호>
 				<c:choose>
 					<c:when test='${fn:length(clientInfo.CORP_NUMB)>9}'>${fn:substring(clientInfo.CORP_NUMB,0,3)}-fn:substring(clientInfo.CORP_NUMB,3,5)}-${fn:substring(clientInfo.CORP_NUMB,5,10)}</c:when>
 					<c:otherwise>${clientInfo.CORP_NUMB}</c:otherwise>
 				</c:choose></사업자등록번호>                                     
		<대표자>${clientInfo.PRPS_NAME }</대표자>                                                            
		<주민등록번호>${clientInfo.RESD_CORP_NUMB_VIEW}
		<!--  				<c:choose>
 					<c:when test='${fn:length(clientInfo.RESD_CORP_NUMB)>6}'>${fn:substring(clientInfo.RESD_CORP_NUMB,0,6)}-${fn:substring(clientInfo.RESD_CORP_NUMB,6,13)}</c:when>
 					<c:otherwise>${clientInfo.RESD_CORP_NUMB}</c:otherwise>
 				</c:choose>
 		 -->
 		</주민등록번호>                                          
		<국가명></국가명>                                                                
		<전화>${clientInfo.HOME_TELX_NUMB }</전화>                                                          
		<팩스>${clientInfo.FAXX_NUMB }</팩스>                                                          
		<핸드폰>${clientInfo.MOBL_PHON }</핸드폰>                                                      
		<이메일>${clientInfo.MAIL }</이메일>                                  
		<주소>${clientInfo.HOME_ADDR }</주소>
		
		<은행명>${clientInfo.BANK_NAME}</은행명>                                                                
		<계좌번호>${clientInfo.ACCT_NUMB }</계좌번호>                                                 
		<예금주>${clientInfo.DPTR }</예금주>                                                              
		<권리자>${clientInfo.PRPS_NAME }</권리자>                                                              
		<대리신고인></대리신고인>                                                      
		<권리자와의관계></권리자와의관계>                                                

		<c:if test="${!empty kappList}">
			<c:forEach items="${kappList}" var="kappList">	
				<c:set var="kappNo" value="${kappNo + 1}"/>
		<list>                                                           						
			<NO>${kappNo }</NO>																			
			<음반명>${kappList.DISK_NAME }</음반명>   
			<CD코드>${kappList.CD_CODE }</CD코드>                                              
			<곡명>${kappList.SONG_NAME }</곡명>                                           
			<가수명>${kappList.SNER_NAME }</가수명>                                     
			<발매일>${kappList.DATE_ISSU_STR }</발매일>                                                 
			<국가명>${kappList.NATN_NAME }</국가명>                                                 
			<권리근거>${kappList.RGHT_GRND }</권리근거>                                             
			<작사가>${kappList.LYRI_WRTR }</작사가>                                         
			<작곡가>${kappList.COMS_WRTR }</작곡가>                                       
			<음악장르>${kappList.MUSC_GNRE_STR }</음악장르>                                                 
		</list>                                                                     
			</c:forEach>
		</c:if>
		
		<c:if test="${empty kappList}">
		<list>                                                           						
			<음반명></음반명>   
			<CD코드></CD코드>                                              
			<곡명></곡명>                                           
			<가수명></가수명>                                     
			<발매일></발매일>                                                 
			<국가명></국가명>                                                 
			<권리근거></권리근거>                                             
			<작사가></작사가>                                         
			<작곡가></작곡가>                                       
			<음악장르></음악장르>                                                 
		</list>                                                                     
		</c:if>
		</root>
		
</TEXTAREA>
</form>
<script language="javascript" src="/cdoc/rxpp/vista/inst_page_div.js?modal=0"></script>

</BODY>
</HTML>

