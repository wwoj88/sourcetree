<!--
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE> 넣으면 페이지 나오지 않음
-->
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

%>
<HTML>
<HEAD>
    <TITLE>ReportExpress</TITLE>
    <META http-equiv=Content-Type content="text/html; charset=ks_c_5601-1987"/>
    <meta http-equiv="Cache-Control" content="no-cache"/> 
    <meta http-equiv="Expires" content="0"/> 
    <meta http-equiv="Pragma" content="no-cache"/> 
    <LINK href="lib/common.css" type="text/css" rel="stylesheet"/>
	<style type="text/css">
	 td.txt {color:#615E5E; font-size:12px; font-family:돋움 , 굴림 ; letter-spacing:-10%; padding-left:10px;padding-bottom:15px; }
	</style>
</HEAD>
<SCRIPT LANGUAGE="javascript" src="/cdoc/rxpp/vista/rptrx_div.js"></SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=ErrorMsg(msg)>
<!--
    if (msg != "")  alert(msg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=PrintResult(flag)>
<!--
//        if (flag == true)
//        	alert("출력을 성공적으로 끝냈습니다.");
//        else
//        	alert("출력을 취소하셨거나 실패하였습니다.");
//-->
</SCRIPT>

<BODY bgColor=white leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>
<!-- 뷰어 설치 스크립트 호출 -->
<Table border=0 width=100% height=100%>
	<Tr>
	<Td width=100% height=100%>
		<div id="Inst" style="display:none"></div>
		<div id="objdiv" style="display:none"></div>
	</Td>
  </Tr>
</Table>

<form name=reportform action="#" method=post style="display:none">
<input type="hidden" name="rpx" value="TRST_203.rpx">
<input type="hidden" name="fileurl" value="<%//=fileurl%>">
<input type="submit" style="display:none;">
<TEXTAREA name=xmldata style="visibility:hidden;height:0;width:0;" title="">
	<!-- 방송음악 보상금분배신청서 -->
	<root>
		 <처리기간>15일</처리기간>																
		 <신청인>                                                                                
		 	<성명 한글='${clientInfo.PRPS_NAME }' 한문='' 영문=''/>	
		 	<상호 한글='${clientInfo.PRPS_NAME }' 영문=''/>         
		 	<주민등록번호>${clientInfo.RESD_CORP_NUMB_VIEW}
		 				<!-- 
		 				<c:choose>
		 					<c:when test='${fn:length(clientInfo.RESD_CORP_NUMB)>6}'>${fn:substring(clientInfo.RESD_CORP_NUMB,0,6)}-${fn:substring(clientInfo.RESD_CORP_NUMB,6,13)}</c:when>
		 					<c:otherwise>${clientInfo.RESD_CORP_NUMB}</c:otherwise>
		 				</c:choose> -->
		 	</주민등록번호>                      		
		 	<주소 >${clientInfo.HOME_ADDR}</주소>                  
		 	<사업장주소 >${clientInfo.BUSI_ADDR}</사업장주소>	    
		 	<사업자등록번호>${clientInfo.CORP_NUMB}</사업자등록번호>                        
		 	<연락처 자택='${clientInfo.HOME_TELX_NUMB }' 사무실='${clientInfo.BUSI_TELX_NUMB }' 휴대폰='${clientInfo.MOBL_PHON }' fax='${clientInfo.FAXX_NUMB }' email='${clientInfo.MAIL }'/>	
		 	<url></url>                                                                         
		 </신청인>                                                                               
		                                                                                         
		 <detail>                                                                                
		 </detail>		                                                                        
		                                                                                         
		 <상호명></상호명>                                                                       
		 <대표자>${clientInfo.PRPS_NAME }</대표자>                   
		 <신청일자 년='${fn:substring(clientInfo.RSGT_DTTM_STR , 0 , 4)}' 월='${fn:substring(clientInfo.RSGT_DTTM_STR , 4 , 6)}' 일='${fn:substring(clientInfo.RSGT_DTTM_STR , 6 , 8)}'/>                               
	</root>
	
</TEXTAREA>
</form>
<script language="javascript" src="/cdoc/rxpp/vista/inst_page_div.js?modal=0"></script>

</BODY>
</HTML>

