<!--
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE> 넣으면 페이지 나오지 않음
-->
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

%>
<HTML>
<HEAD>
    <TITLE>ReportExpress</TITLE>
    <META http-equiv=Content-Type content="text/html; charset=ks_c_5601-1987"/>
    <meta http-equiv="Cache-Control" content="no-cache"/> 
    <meta http-equiv="Expires" content="0"/> 
    <meta http-equiv="Pragma" content="no-cache"/> 
    <LINK href="lib/common.css" type="text/css" rel="stylesheet"/>
	<style type="text/css">
	 td.txt {color:#615E5E; font-size:12px; font-family:돋움 , 굴림 ; letter-spacing:-10%; padding-left:10px;padding-bottom:15px; }
	</style>
</HEAD>
<SCRIPT LANGUAGE="javascript" src="/cdoc/rxpp/vista/rptrx_div.js"></SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=ErrorMsg(msg)>
<!--
    if (msg != "")  alert(msg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=PrintResult(flag)>
<!--
//        if (flag == true)
//        	alert("출력을 성공적으로 끝냈습니다.");
//        else
//        	alert("출력을 취소하셨거나 실패하였습니다.");
//-->
</SCRIPT>

<BODY bgColor=white leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>
<!-- 뷰어 설치 스크립트 호출 -->
<Table border=0 width=100% height=100%>
	<Tr>
	<Td width=100% height=100%>
		<div id="Inst" style="display:none"></div>
		<div id="objdiv" style="display:none"></div>
	</Td>
  </Tr>
</Table>
<form name=reportform action="#" method=post style="display:none">
<input type="hidden" name="rpx" value="법정허락_명세서.rpx">
<input type="hidden" name="fileurl" value="<%//=fileurl%>">
<input type="submit" style="display:none;">
<TEXTAREA name=xmldata style="visibility:hidden;height:0;width:0;" title="">
	
	<!-- 법정허락 명세서 -->
	
<root>
	<report>
		<c:forEach items="${statApplyWorksList}" var="statApplyWorksList">
			<c:set var="i" value="${i+1}"/>
			<c:set var="gbn1" value="0"/><c:set var="gbn2" value="0"/><c:set var="gbn3" value="0"/><c:set var="gbn4" value="0"/><c:set var="gbn5" value="0"/>
			<c:if test="${statApplication.applyType01 == '1'}">
				<c:set var="gbn1" value="1"/>
			</c:if>
			<c:if test="${statApplication.applyType02 == '1'}">
				<c:set var="gbn2" value="1"/>
			</c:if>
			<c:if test="${statApplication.applyType03 == '1'}">
				<c:set var="gbn3" value="1"/>
			</c:if>
			<c:if test="${statApplication.applyType04 == '1'}">
				<c:set var="gbn4" value="1"/>
			</c:if>
			<c:if test="${statApplication.applyType05 == '1'}">
				<c:set var="gbn5" value="1"/>
			</c:if>
			<item>
				<순번><![CDATA[${i}]]></순번>
				<제목 gbn1='${gbn1}' gbn2='${gbn2}' gbn3='${gbn3}' gbn4='${gbn4}' gbn5 = '${gbn5}'><![CDATA[이용 승인신청명세서]]></제목>
				<c:set var="gbn1" value="0"/><c:set var="gbn2" value="0"/><c:set var="gbn3" value="0"/><c:set var="gbn4" value="0"/><c:set var="gbn5" value="0"/>
				<c:if test="${statApplyWorksList.APPLYTYPE01 == '1'}">
					<c:set var="gbn1" value="1"/>
				</c:if>
				<c:if test="${statApplyWorksList.APPLYTYPE02 == '1'}">
					<c:set var="gbn2" value="1"/>
				</c:if>
				<c:if test="${statApplyWorksList.APPLYTYPE03 == '1'}">
					<c:set var="gbn3" value="1"/>
				</c:if>
				<c:if test="${statApplyWorksList.APPLYTYPE04 == '1'}">
					<c:set var="gbn4" value="1"/>
				</c:if>
				<c:if test="${statApplyWorksList.APPLYTYPE05 == '1'}">
					<c:set var="gbn5" value="1"/>
				</c:if>
				<구분 gbn1='${gbn1}' gbn2='${gbn2}' gbn3='${gbn3}' gbn4='${gbn4}' gbn5 = '${gbn5}'></구분>
				<제호><![CDATA[${statApplyWorksList.WORKSTITL}]]></제호>
				<종류><![CDATA[${statApplyWorksList.WORKSKIND}]]></종류>
				<형태및수량><![CDATA[${statApplyWorksList.WORKSFORM}]]></형태및수량>
				<공표연월일><![CDATA[${statApplyWorksList.PUBLYMD2}]]></공표연월일>
				<공표국가><![CDATA[${statApplyWorksList.PUBLNATN}]]></공표국가>
				<c:set var="gbn1" value="0"/><c:set var="gbn2" value="0"/><c:set var="gbn3" value="0"/><c:set var="gbn4" value="0"/><c:set var="gbn5" value="0"/><c:set var="gbn6" value="0"/><c:set var="gbn7" value="0"/>
				<c:if test="${statApplyWorksList.PUBLMEDICD == '1'}">
					<c:set var="gbn1" value="1"/>
				</c:if>
				<c:if test="${statApplyWorksList.PUBLMEDICD == '2'}">
					<c:set var="gbn2" value="1"/>
				</c:if>
				<c:if test="${statApplyWorksList.PUBLMEDICD == '3'}">
					<c:set var="gbn3" value="1"/>
				</c:if>
				<c:if test="${statApplyWorksList.PUBLMEDICD == '4'}">
					<c:set var="gbn4" value="1"/>
				</c:if>
				<c:if test="${statApplyWorksList.PUBLMEDICD == '5'}">
					<c:set var="gbn5" value="1"/>
				</c:if>
				<c:if test="${statApplyWorksList.PUBLMEDICD == '6'}">
					<c:set var="gbn6" value="1"/>
				</c:if>
				<c:if test="${statApplyWorksList.PUBLMEDICD == '99'}">
					<c:set var="gbn7" value="1"/>
				</c:if>
				<공표방법 gbn1='${gbn1}' gbn2='${gbn2}' gbn3='${gbn3}' gbn4='${gbn4}' gbn5 = '${gbn5}' gbn6='${gbn6}' gbn7 = '${gbn7}'><![CDATA[${statApplyWorksList.PUBLMEDIETC}]]></공표방법>
				<공표매체정보><![CDATA[${statApplyWorksList.PUBLMEDI}]]></공표매체정보>
				<성명><![CDATA[${statApplyWorksList.COPTHODRNAME}]]></성명>
				<전화번호><![CDATA[${statApplyWorksList.COPTHODRTELXNUMB}]]></전화번호>
				<주소><![CDATA[${statApplyWorksList.COPTHODRADDR}]]></주소>
				<신청물의내용><![CDATA[${statApplyWorksList.WORKSDESC}]]></신청물의내용>
			</item>
		</c:forEach>
	</report>
</root>  
	
</TEXTAREA>
</form>
<script language="javascript" src="/cdoc/rxpp/vista/inst_page_div.js?modal=${modal}"></script>

</BODY>
</HTML>