<!--
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE> 넣으면 페이지 나오지 않음
-->
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

%>
<HTML>
<HEAD>
    <TITLE>ReportExpress</TITLE>
    <META http-equiv=Content-Type content="text/html; charset=ks_c_5601-1987"/>
    <meta http-equiv="Cache-Control" content="no-cache"/> 
    <meta http-equiv="Expires" content="0"/> 
    <meta http-equiv="Pragma" content="no-cache"/> 
    <LINK href="lib/common.css" type="text/css" rel="stylesheet"/>
	<style type="text/css">
	 td.txt {color:#615E5E; font-size:12px; font-family:돋움 , 굴림 ; letter-spacing:-10%; padding-left:10px;padding-bottom:15px; }
	</style>
</HEAD>
<SCRIPT LANGUAGE="javascript" src="/cdoc/rxpp/vista/rptrx_div.js"></SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=ErrorMsg(msg)>
<!--
    if (msg != "")  alert(msg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=Viewer1 EVENT=PrintResult(flag)>
<!--
//        if (flag == true)
//        	alert("출력을 성공적으로 끝냈습니다.");
//        else
//        	alert("출력을 취소하셨거나 실패하였습니다.");
//-->
</SCRIPT>

<BODY bgColor=white leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>
<!-- 뷰어 설치 스크립트 호출 -->
<Table border=0 width=100% height=100%>
	<Tr>
	<Td width=100% height=100%>
		<div id="Inst" style="display:none"></div>
		<div id="objdiv" style="display:none"></div>
	</Td>
  </Tr>
</Table>
<form name=reportform action="#" method=post style="display:none">
<input type="hidden" name="rpx" value="TRST_205_2_M.rpx">
<input type="hidden" name="fileurl" value="<%//=fileurl%>">
<input type="submit" style="display:none;">
<TEXTAREA name=xmldata style="visibility:hidden;height:0;width:0;" title="">
	
	<!-- 도서관보상금 저작물 명세표 -->
	
	<root>														  						
		<작성일>${clientInfo.RSGT_DTTM_STR}</작성일>															

		<c:if test="${!empty krtraList_2}">
			<c:forEach items="${krtraList_2}" var="krtraList_2">	
				<c:set var="krtraNo" value="${krtraNo+1}"/>
		
		<list>																							
			<논문명>${krtraList_2.WORK_NAME }</논문명>             
			<저자명>${krtraList_2.WRTR_NAME }</저자명>             
			<출판사>${krtraList_2.BOOK_CNCN }</출판사>            	
			<발행년도>${krtraList_2.SCTR }</발행년도>              
			<판매유무>${krtraList_2.SELG_YSNO }</판매유무>         
			<이용도서관>${krtraList_2.USEX_LIBR}</이용도서관>     
			<신탁여부></신탁여부>               														
			<비고></비고>                         														
		</list>                                       													
		
		</c:forEach>
	</c:if>
		
	</root>  
		
</TEXTAREA>
</form>
<script language="javascript" src="/cdoc/rxpp/vista/inst_page_div.js?modal=0"></script>

</BODY>
</HTML>

