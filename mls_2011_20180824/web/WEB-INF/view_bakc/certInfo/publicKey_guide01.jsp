<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="pragma" content="no-cache"/>
<title>저작권찾기 공인인증서 발급 안내</title>
<link href="/css/popup.css" rel="stylesheet" type="text/css"/>
<script>
<!--
function fn_certInfoOpen2() {
	window.open('/main/main.do?method=goCertPage2','win2','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=800, height=550');
}

function fn_certInfoOpen3() {
	window.open('/main/main.do?method=goCertPage3','win2','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=800, height=550');
}
-->
</script>
</head>
<body>
<div id="wrap">
	<div class="popupTitle">
		<h2>저작권찾기 사이트 이용을 위한 공인인증서 안내</h2>
	</div>
	<div class="popupContents">
		<div class="box"><img src="/images/sub/public_01img.gif" alt="" class="floatR"/>공인인증서(공인전자서명)는 전자서명법에 의해 일반상거래에서 사용하는 서명 혹은 기명날인과 동일한 법적 효력을 인정 받습니다. 일상 생활에서 신원을 확인하거나 거래를 할 때 주민등록증이나 인감 날인 또는 서명 등이 필요하듯이, 인터넷에서도 거래를 증명하거나 신원 확인이 필요할 때 이를 확실히 보장해 주는 증명수단이 바로 전자서명입니다.<br /><br />즉, 공인인증서(공인전자서명)는 전자적 형태로 발급되는 자신만의 디지털 인감이며 서명입니다.</div>
		<div>
			<h3>공인인증서 이용 안내</h3>
			저작권찾기 시스템에 이용되는 공인인증서는 범용인 사업자 공인인증서 또는 용도제한용인 저작권 업무용 사업자 공인인증서를 발급 받아 이용하시기 바랍니다.
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="grid">
				<thead>
					<tr>
						<th colspan="2" class="tdLabel">구분</th>
						<th class="thTitle">가격</th>
						<th class="thTitle">사용처</th>
						<th class="thTitle">비고</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td rowspan="2" class="tdData C"><a href="#1">범용</a></td>
						<td class="tdData"><a href="#1">개인용</a></td>
						<td class="tdData">4.400원(부가세 포함)</td>
						<td class="tdData">개인에게 요구되는 모든 전자거래 분야</td>
						<td class="tdData">해당 공인인증기관별로 상세 발급 안내 수록</td>
					</tr>
					<tr>
						<td class="tdData">사업자용</td>
						<td class="tdData">110,000원(부가세 포함)</td>
						<td class="tdData">사업자(개인, 법인)에게 요구되는 모든 전자거래 분야</td>
						<td class="tdData">방문설치서비스 이용시 18,000원 추가</td>
					</tr>
					<tr>
						<td class="tdData C">용도제한용</td>
						<td class="tdData">저작권업무용 사업자</td>
						<td class="tdData">4,400원(부가세 포함)</td>
						<td class="tdData">저작권위원회의 저작권업무 관련 전자계약 등 문화체육관광부 업무</td>
						<td class="tdData">-</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div>
			<h3>공인인증서 발급 공인인증기관 안내</h3>
			<h4>한국정보인증</h4>
			<p>정부에서 주관하여 만든 우리나라 제1호 공인인증기관으로 국내에서 유일하게 원천기술력을 보유하고 있는 전문 공인인증기관으로 우리나라 정부 각 부처 및 기관과 금융권 등을 대상으로 공인인증 서비스를 제공하고 있습니다.</p>
			<a href="#1" onclick="javascript:fn_certInfoOpen2();"><img src="/images/button/btn_business.gif" alt="사업자용 공인인증서 발급 신청" width="216" height="31" /></a>&nbsp;<a href="#1" onclick="javascript:fn_certInfoOpen3();"><img src="/images/button/btn_copyright.gif" alt="저작권업무용 사업자 공인인증서 발급 신청" width="296" height="31" /></a>
			<p>※ 주요 서비스 대상: 문화관광부, 교육인적자원부, 보건복지부, 국방부, 정보통신부, 행정자치부, 조달청, 국세청, 관세청, 한국전력, 우체국, 우리은행, 외환은행, 기업은행, SC제일은행, 부산은행 등 각 정부부처, 공기업 및 기타 은행<br /><br />사업자용 고인인증서의 경우 찾아가는 서비스를 이용하시면 공인인증기관의 등록대행기관에서 직접 방문하여 서류를 접수해 드립니다.(단, 공인인증서 설치까지 원하시는 경우에는 추가비용이 듭니다.)</p>
		</div>
		<div class="popMsg"><span class="floatL">공인인증서 발급문의:1577-8787</span> <span class="floatR">Copyright 저작권위원회. All rights reserved.</span></div>
	</div>
	<div class="popupBottom"><a href="#1" onclick="javascript:window.close();"><img src="/images/button/close_btn.gif" alt="닫기" width="76" height="23" /></a></div>
</div>
</body>
</html>
