<%@ page pageEncoding="euc-kr"%>
<!-- hidden form str : 아이프레임데이타 가져가는 용도-->		
					<form name="hidForm" action="#" class="sch">
						<input type="hidden" name="worksTitle" value="">
						<input type="hidden" name="worksId" />
						<input type="hidden" name="isSsnNo" />
						<input type="hidden" name="userIdnt" />
						<input type="hidden" name="ifrmInput" />
						<input type="hidden" name="PRPS_RGHT_CODE" />
						<input type="hidden" name="srchTitle" />
						<input type="hidden" name="srchProducer" />
						<input type="hidden" name="srchAlbumTitle" />
						<input type="hidden" name="srchSinger" />
						<input type="hidden" name="srchStartDate" />
						<input type="hidden" name="srchEndDate" />
						<!-- 음악 -->
						<input type="hidden" name="srchNonPerf" />
						<!-- 도서 -->
						<input type="hidden" name="srchLicensor" />
						<input type="hidden" name="srchPublisher" />
						<input type="hidden" name="srchBookTitle" />
						<input type="hidden" name="srchLicensorNm" />
						<!-- 방송대본 -->
						<input type="hidden" name="srchWriter" />
						<input type="hidden" name="srchBroadStatName" />
						<input type="hidden" name="srchDirect" />
						<input type="hidden" name="srchPlayers" />
						<!-- 이미지 -->
						<input type="hidden" name="srchWorkName" />
						<input type="hidden" name="srchLishComp" />
						<input type="hidden" name="srchCoptHodr" />
						<input type="hidden" name="srchWterDivs" />
						<!-- 영화 -->
						<input type="hidden" name="srchDistributor" />
						<input type="hidden" name="srchDirector" />
						<input type="hidden" name="srchViewGrade" />
						<input type="hidden" name="srchActor" />
						<!-- 방송 -->
						<input type="hidden" name="srchProgName" />
						<input type="hidden" name="srchMaker" />
						
						<!-- 뉴스 -->
						<input type="hidden" name="srchProviderNm" />
						<input type="submit" style="display:none;">
					</form>
					
						<!-- 검색 -->
						<form id="search" name="search" action="srchList.do" method="post" onSubmit="goSearch(); return false">
							<fieldset class="w100">
							<legend>게시판검색</legend>
								<div class="boxStyle">
									<div class="box1 floatDiv">
										<p class="fl mt5 w15"><label for="srchDivs"><img title="Search" alt="Search" src="/images/2011/content/sch_txt.gif"></label></p>
										<p class="fl w90">
										<select class="w20" name="target" id="srchDivs" onchange="javascript:changeList(this.value,'0')">
											<%for(int j=0; j<=targetcount; j++){%>
												<option value=<%=temptargetlist[j]%> <%if(target.compareTo(temptargetlist[j])==0){%>selected<%}%>><%=targetnamelist[j]%></option>
											<%}%>
										</select>
										<select id="genreCd" name="genreCd" class="w28">
										</select>
										<input class="inputData w40" title="검색어" id="query" name="query" size="30" value="<%=query%>" style="IME-MODE: active;width: 230px;">
										<span class="button small black"><input type="submit" value="검색"></span> <input type="checkbox" id="chk1" name="resrch_check" class="inputChk ml5" onclick="javascript:resrch_chk();"/><label for="chk1" class="p11 strong black">결과 내 검색</label> 
										<!-- <input type="checkbox" id="chk2" class="inputChk ml5" /><label for="chk2" class="p11 strong black">상세검색</label> -->
									</p>
									</div>
									<span class="btmRound lftTop"></span><span class="btmRound rgtTop"></span><span class="btmRound"></span><span class="btmRound rgt"></span>
								</div>
							</fieldset>
							<input type="hidden" name="resultcount" value="10">
							<input type="hidden" name="page" value="">
							<input type="hidden" name="mode" value="">
							<input type="hidden" name="resrch" value="">							
							<input type="hidden" id="urlCd" name="urlCd" value="01"/>
 						</form>
 						<%if(query.compareTo("")!=0){%>   
							<div class="white_box">
								<div class="box5">
									<div class="box5_con floatDiv">
										<p class="fl"><img alt="" src="images/2012/content/box_img4.gif"></p>
										<div class="fl ml30 ">
											<ul class="list1 mt10">
											<li class="bgNone pl0"><strong class="inBlock w30 vtop"><sup class="sup black thin">1)</sup>법정허락 이용승인 신청</strong><span class="inBlock w70">이미 상당한노력이 이행되어 법정허락이용승인신청이 가능한 저작물로, 법정허락 이용승인 신청정보 작성화면으로 이동합니다.</span></li>
											<!-- 
											<li class="bgNone pl0 mt5"><strong class="inBlock w30 vtop"><sup class="sup black thin">2)</sup>이용승인 신청 안내</strong><span class="inBlock w70">위탁관리저작물로 이용승인 신청을 위한 해당관리기관 정보 안내화면을 보여줍니다.</span></li>
											 -->
											<li class="bgNone pl0 mt5"><strong class="inBlock w30 vtop"><sup class="sup black thin">2)</sup>저작권자 찾기위한 상당한<br/>&nbsp;&nbsp;노력 신청</strong><span class="inBlock w70">저작권찾기 정보시스템에서 한번의 신청으로 상당한노력 이행 신청과 함께 저작권자 조회공고 자동으로 처리됩니다.</span></li>
											<li class="bgNone pl0 mt5"><strong class="inBlock w30 vtop"><sup class="sup black thin">3)</sup>저작물 이용허락 신청</strong><span class="inBlock w70">검색한 저작물의 이용계약을 하기 위해서 저작권통합관리시스템으로 이동합니다.</span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<%}%>