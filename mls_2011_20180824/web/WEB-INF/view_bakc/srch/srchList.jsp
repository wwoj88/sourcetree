<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%                                                     
	User user = SessionUtil.getSession(request);      
	String sessUserIdnt = user.getUserIdnt();
	
	String sessUserName = user.getUserName();
	String sessSsnNo = user.getSsnNo();	
	
	pageContext.setAttribute("UserName", sessUserIdnt);
	
	
	                                                  
%>                                      
<html lang="ko">   
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >     
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 검색 및 상당한 노력 신청 서비스 이용 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />  
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript" src="/js/menuhover.js"></script>
<script type="text/javascript"> 
<!--   
function statBoRegiPop(){ 
	var userId = '<%=sessUserIdnt%>';
	
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		location.href = "/user/user.do?method=goSgInstall";
	}else{
		window.open("/statBord/statBo06Regi.do", "small", "width=800, height=750, scrollbars=yes, menubar=no, location=yes");
	}
}
          
/*            
function crosPop(content1){
		window.open("http://www.cros.or.kr/report/search.cc?fl=*%2Cscore&sortField=score&q="+content1, "small", "width=750, height=650, scrollbars=yes, menubar=no, location=yes");

}
*/
function crosPop(content1){
	window.open("/search/pop_cros.jsp?reg_id="+content1, "_blank", "width=1000, height=841, scrollbars=yes, menubar=no, location=yes, resizable=yes ");
}
//법정허락 신청
function fn_goStep(value){
	var userId = '<%=sessUserIdnt%>';
	
	var userName = '<%=sessUserName%>';
	
	if(document.hidForm.isSsnNo.value == ''){
		document.hidForm.isSsnNo.value = '<%=sessSsnNo%>';
	}
	
	var isSsnNo = document.hidForm.isSsnNo.value;
	
	
	if(userId == 'null' || userId == ''){
		alert('로그인이 필요한 화면입니다.');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}else{
		if(isSsnNo == 'null' || isSsnNo == ''){
			window.open('/user/user.do?method=goSsnNoConf&userIdnt='+userId+'&userName='+userName+'&sDiv=03&value='+value,'win1','toolbar=0, status=0, scrollbars=yes, location=0, menubar=0, width=540, height=375');
			return;
		}else{
			var hddnFrm = document.hidForm;
			var frm = document.search;
			hddnFrm.worksTitle.value=frm.query.value;
			hddnFrm.action = "/stat/statPrpsMain.do";
			var worksId = value;
			hddnFrm.worksId.value = worksId;
			hddnFrm.target = "_self";
			hddnFrm.method = "post";
			hddnFrm.submit();
		}
	}
}
 

//-->                            
</script>
<script>
/* 	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69621660-2', 'auto');
	  ga('send', 'pageview');
 */
</script>     
</head>           
               
<body>
   
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader3.jsp" /> --%>
		<script type="text/javascript">initNavigation(3);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<!-- 썸네일 팝업 시작 -->
		<%-- ${menuFlag} --%>
<div class="pop_thumb">
	<div class="inner">
		<h2 class="tit">이미지 정보<a href="javascript:;" class="btn_close"><img src="images/new/btn_close_pop.png" alt="팝업닫기"></a></h2>
		<div class="cont">
			<div class="img">
				<img src="images/new/img_pop.png" alt="">
			</div>
			<div class="ment">
				<p class="subject">
					제목부분
				</p>
				<ul>
					<li><strong>이미지코드 : </strong>16270463</li>
					<li><strong>키워드 : </strong>코사인</li>
					<li><strong>창작년월일 : </strong>2017.05.10</li>
					<li><strong>권리 관리 기관명 : </strong>두영디지텍</li>
					<li><strong>이미지 설명 : </strong>이미지에 대한 설명을 작성해 주세요.</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- 썸네일 팝업 끝 -->

		<!-- HEADER end -->
		                                      
		<!-- CONTAINER str-->    
		<div id="contents">                 
            
          <div class="con_lf">
			<div class="con_lf_big_title">저작권자 찾기</div>
				<ul class="sub_lf_menu">
					<li><a href="/mlsInfo/liceSrchInfo01.jsp" class="on">저작권자 찾기</a>
						<ul class="sub_lf_menu2">
							<li><a href="/mlsInfo/liceSrchInfo01.jsp">소개 및 이용방법</a></li>
							<li><a href="/srchList.do" class="on">서비스 이용</a></li>
							<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
						</ul>
					</li>
					<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청</a>
						<ul class="sub_lf_menu2 disnone">
							<li><a href="/mlsInfo/statInfo01.jsp">소개 및 이용방법</a></li>
							<li><a href="/stat/statSrch.do">서비스 이용</a></li>
							<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="con_rt">                             
			   <div class="con_rt_head">
				<img src="../images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				저작권자 찾기
				&gt;
				<span class="bold">${genreCd}</span>
				<span class="bold">서비스 이용</span>
			   </div>                                            
				           
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>                   
				</div>       
				
				 <!-- //Search str -->   
				<%@include file="mirsearch.jsp"%>                                                              
				 <!-- //Search end -->
				
			
			</div>  
		<!-- //CONTAINER end -->
		<p class="clear"></p>    
  </div>
  
  	<!-- FOOTER str-->
		<jsp:include page="/include/2017/footer.jsp" />
	<!-- FOOTER end -->
<script type="text/javascript" src="/js/2010/calendarcode.js"></script>
<script type="text/JavaScript"> 
	Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>
 
</body>
</html>

