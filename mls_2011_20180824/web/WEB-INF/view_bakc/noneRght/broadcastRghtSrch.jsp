<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2010/prototype.js"> </script>

<title>방송 저작물 | 저작권찾기</title>
<script type="text/JavaScript">
<!--

var emptyYn = '${emptyYn}'; // 검색조건이 없을 경우 //20120220 정병호

window.name = "noneBroadcastRghtSrch";
	
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/noneRght/rghtSrch.do?method=subList&DIVS=R";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 방송대본저작물 상세 팝업오픈 : 부모창에서 오픈
function openBroadcastDetail( crId ) {

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=R'+param
	var name = '';
	var openInfo = 'target=noneBroadcastRghtSrch, width=705, height=570, scrollbars=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=R'+param
	var name = '';
	var openInfo = 'target=noneBroadcastRghtSrch, width=705, height=476, scrollbars=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifBroadcastRghtSrch"); 
	
	if(emptyYn != 'Y'){// 검색조건이 없을 경우 //20120220 정병호
		var totalRow = ${broadcastList.totalRow}
		parent.document.getElementById("totalRow").value = totalRow //+"건"; //초기페이지 예외처리하기.
	}
	
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	parent.frames.scrollTo(0,0);
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
.grid tbody td.confirm{
	background: #f2f2f2 url(/images/2011/common/necessary3.gif) no-repeat 2% 10% !important;
	padding-left: 0px !important;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="sTotalRow" value="${broadcastList.totalRow}"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchProgName" value="${srchParam.srchProgName }"/>
	<input type="hidden" name="srchMaker" value="${srchParam.srchMaker }"/>
	<input type="submit" style="display:none;">
	
	<c:forEach items="${srchParam.srchNoneRole_arr }" var="srchNoneRole_arr">
		<c:set var="i" value="${i+1}"/>
		<input type="hidden" name="srchNoneRole_arr" value="${srchParam.srchNoneRole_arr[i-1] }" />
	</c:forEach>
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:auto;padding:0 0 0 0;margin-top:1px;">
			<table id="tab_scroll" cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="방송 저작물의 저작물명, 프로그램명, 프로그램 회차, 방송일자, 매체, 채널, 프로그램등급, 제작자, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="2%" />
				<col width="*" />
				<col width="8%" />
				<col width="18%" />
				<col width="12%" />
				<col width="14%" />
				<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" rowspan="2"><input type="checkbox" id="chk" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','iChk',this);" style="cursor:pointer;" title="전체선택" /></th>
						<th scope="col" rowspan="2">저작물명</th>
						<th scope="col" colspan="2">프로그램명</th>
						<th scope="col">방송일자</th>
						<th scope="col">제작자</th>
						<th scope="col" rowspan="2">저작권찾기</th>
					</tr>
					<tr>
						<th scope="col">회차</th>
						<th scope="col">등급</th>
						<th scope="col">매체</th>
						<th scope="col">채널</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${emptyYn == 'Y'}"><!-- 검색조건이 없을 경우 //20120220 정병호 -->
					<tr>
						<td class="ce" colspan="7">검색조건을 입력해 주세요.</td>
					</tr>
				</c:if>
				<c:if test="${broadcastList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="7">검색된 저작물정보가  없습니다.</td>
					</tr>
				</c:if>
				<c:if test="${broadcastList.totalRow > 0}">
					<c:forEach items="${broadcastList.resultList}" var="broadcastList">
						<c:set var="NO" value="${broadcastList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<td class="ce" rowspan="2">
							<input type="checkbox" name="iChk" class="vmid" value="${broadcastList.CR_ID }" style="cursor:pointer;" title="선택"/>
							<!-- hidden value start -->
							<input type="hidden" name="title" value="${broadcastList.TITLE}" />
							<input type="hidden" name="progName" value="${broadcastList.PROG_NAME}" />
							<input type="hidden" name="progOrdSeq" value="${broadcastList.PROG_ORDSEQ}" /> <!-- 프로그램회차 -->
							<input type="hidden" name="broadDate" value="${broadcastList.BROAD_DATE}" />
							<input type="hidden" name="mediCode" value="${broadcastList.MEDI_CODE}" />
							<input type="hidden" name="chnlCode" value="${broadcastList.CHNL_CODE}" />
							<input type="hidden" name="mediCodeName" value="${broadcastList.MEDI_CODE_NAME}" />
							<input type="hidden" name="chnlCodeName" value="${broadcastList.CHNL_CODE_NAME}" />
							<input type="hidden" name="progGrad" value="${broadcastList.PROG_GRAD}" />
							<input type="hidden" name="crId" value="${broadcastList.CR_ID}" />
							<input type="hidden" name="maker" value="${broadcastList.MAKER}" />
							<input type="hidden" name="icnNumb" />
							<!-- 저작권미상 -->
							<input type="hidden" name="maker_yn" value="${broadcastList.MAKER_YN}" />
							<!-- hidden value end -->
						</td>
						<td style="cursor:pointer;" rowspan="2">
							<a href="#1" class="underline black2" onclick="javascript:openBroadcastDetail('${broadcastList.CR_ID }');">${broadcastList.TITLE }</a>
						</td>
						<td colspan="2">${broadcastList.PROG_NAME }</td>
						<td class="ce">${broadcastList.BROAD_DATE }</td>
						<td class="ce<c:if test="${broadcastList.MAKER_YN == '0'}"> confirm</c:if>">${broadcastList.MAKER }</td>
						<td class="ce" rowspan="2">	
							<c:if test="${broadcastList.PRPS_CNT >0}">
								<a href="#1" onclick="javascript:openRghtPrps('${broadcastList.CR_ID }','0','0');"><img src="/images/common/ic_newWin.gif" class="vtop" alt="저작권찾기신청내역" /></a>
							</c:if>
						</td>
					</tr>
					<tr>
						<td class="ce">${broadcastList.PROG_ORDSEQ }&nbsp;</td>
						<td class="ce">${broadcastList.PROG_GRAD }</td>
						<td class="ce">${broadcastList.MEDI_CODE_NAME }</td>
						<td class="ce">${broadcastList.CHNL_CODE_NAME }</td>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${broadcastList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
