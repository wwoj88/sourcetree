<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2010/prototype.js"> </script>

<title>방송대본 저작물 | 저작권찾기</title>
<script type="text/JavaScript">
<!--
var emptyYn = '${emptyYn}'; // 검색조건이 없을 경우 //20120220 정병호

window.name = "noneScriptRghtSrch";
	
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/noneRght/rghtSrch.do?method=subList&DIVS=C";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 방송대본저작물 상세 팝업오픈 : 부모창에서 오픈
function openScriptDetail( crId ) {

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=C'+param
	var name = '';
	var openInfo = 'target=scriptRghtSrch, width=705, height=570, scrollbars=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=C'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch, width=705, height=476, scrollbars=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifScriptRghtSrch"); 
	
	if(emptyYn != 'Y'){// 검색조건이 없을 경우 //20120220 정병호
		var totalRow = ${scriptList.totalRow}
		parent.document.getElementById("totalRow").value = totalRow //+"건"; //초기페이지 예외처리하기.
	}

	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	parent.frames.scrollTo(0,0);
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}

.grid tbody td.confirm{
	background: #f2f2f2 url(/images/2011/common/necessary3.gif) no-repeat 2% 10% !important;
	padding-left: 0px !important;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="sTotalRow" value="${scriptList.totalRow}"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
	<input type="hidden" name="srchWriter" value="${srchParam.srchWriter }"/>
	<input type="hidden" name="srchBroadStatName" value="${srchParam.srchBroadStatName }"/>
	<input type="submit" style="display:none;">

	<c:forEach items="${srchParam.srchNoneRole_arr }" var="srchNoneRole_arr">
		<c:set var="i" value="${i+1}"/>
		<input type="hidden" name="srchNoneRole_arr" value="${srchParam.srchNoneRole_arr[i-1] }" />
	</c:forEach>
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:auto;padding:0 0 0 0;margin-top:1px;">
			<table id="tab_scroll" cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="방송대본 저작물의 작품명, 작가명, 연출가, 방송회차, 방송일시, 방송매체, 방송사, 제작사, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="2%" />
				<col width="*" />
				<col width="16%" />
				<col width="13%" />
				<col width="13%" />
				<col width="16%" />
				<col width="12%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" rowspan="2"><input type="checkbox" id="chk" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','iChk',this);" style="cursor:pointer;" title="전체선택"/></th>
						<th scope="col" rowspan="2">작품명</th>
						<th scope="col" rowspan="2">작가</th>
						<th scope="col">방송사</th>
						<th scope="col">방송매체</th>
						<th scope="col">연출가</th>
						<th scope="col" rowspan="2" >저작권찾기</th>
					</tr>
					<tr>
						<th scope="col">방송회차</th>
						<th scope="col">방송일자</th>
						<th scope="col">주요출연진</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${emptyYn == 'Y'}"><!-- 검색조건이 없을 경우 //20120220 정병호 -->
					<tr>
						<td class="ce" colspan="7">검색조건을 입력해 주세요.</td>
					</tr>
				</c:if>
				<c:if test="${scriptList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="7">검색된 저작물정보가 없습니다.</td>
					</tr>
				</c:if>
				<c:if test="${scriptList.totalRow > 0}">
					<c:forEach items="${scriptList.resultList}" var="scriptList">
						<c:set var="NO" value="${scriptList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<td class="ce" rowspan="2">
							<input type="checkbox" name="iChk" class="vmid" value="${scriptList.CR_ID }" style="cursor:pointer;" title="선택"/>
							<!-- hidden value start -->
							<input type="hidden" name="title" value="${scriptList.TITLE}" />
							<input type="hidden" name="writer" value="${scriptList.WRITER}" /><!-- 작가 -->
							<input type="hidden" name="subTitle" value="${scriptList.SUBTITLE}" />
							<input type="hidden" name="direct" value="${scriptList.DIRECT}" />
							<input type="hidden" name="crhIdOfCa" value="${scriptList.CRH_ID_OF_CA}" />
							<input type="hidden" name="insertDate" value="${scriptList.INSERT_DATE}" />
							<input type="hidden" name="broadOrd" value="${scriptList.BROAD_ORD}" /><!-- 방송회차 -->
							<input type="hidden" name="crId" value="${scriptList.CR_ID}" />
							<input type="hidden" name="broadDate" value="${scriptList.BROAD_DATE}" />
							<input type="hidden" name="broadMediName" value="${scriptList.BROAD_MEDI_NAME}" /><!-- 방송매체 -->
							<input type="hidden" name="broadStatName" value="${scriptList.BROAD_STAT_NAME}" /><!-- 방송사 -->
							<input type="hidden" name="players" value="${scriptList.PLAYERS}" /><!-- 주요출연진 -->
							<input type="hidden" name="maker" value="${scriptList.MAKER}" /><!-- 연출 -->
							<input type="hidden" name="genreKindName" value="${scriptList.GENRE_KIND_NAME}"/><!-- 장르 -->
							<input type="hidden" name="icnNumb" value="${scriptList.ICN_NUMB}" />
							<!-- 저작권미상 -->
							<input type="hidden" name="writer_yn" value="${scriptList.WRITER_YN}" />
							<!-- hidden value end -->
						</td>
						<td rowspan="2" style="cursor:pointer;">
							 <a href="#1" class="underline black2" onclick="javascript:openScriptDetail('${scriptList.CR_ID }');">${scriptList.TITLE }</a>
						</td>
						<td rowspan="2"class="ce<c:if test="${scriptList.WRITER_YN == '0'}"> confirm</c:if>">${scriptList.WRITER }</td>
						<td class="ce">${scriptList.BROAD_STAT_NAME }&nbsp;</td>
						<td class="ce">${scriptList.BROAD_MEDI_NAME }</td>
						<td class="ce">${scriptList.DIRECT }</td>
						<td class="ce" rowspan="2">
							<c:if test="${scriptList.PRPS_CNT >0}">
								<a href="#1" onclick="javascript:openRghtPrps('${scriptList.CR_ID }','0','0');"><img src="/images/common/ic_newWin.gif" class="vtop" alt="저작권찾기신청내역" /></a>
							</c:if>
						</td>
					</tr>
					<tr>
						<td class="ce">${scriptList.BROAD_ORD }&nbsp;</td>
						<td class="ce">${scriptList.BROAD_DATE }</td>
						<td class="ce">${scriptList.PLAYERS }</td>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${scriptList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
