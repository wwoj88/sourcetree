<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2010/prototype.js"> </script>
<title>뉴스 저작물 | 저작권찾기</title>
<script type="text/JavaScript">
<!--
var emptyYn = '${emptyYn}'; // 검색조건이 없을 경우 //20120220 정병호

window.name = "noneNewsRghtSrch";
	
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/noneRght/rghtSrch.do?method=subList&DIVS=N";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}


// 뉴스저작물 상세 팝업오픈 : 부모창에서 오픈
function openNewsDetail( crId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=N'+param;
	var name = '';
	var openInfo = 'target=newsRghtSrch, width=705, height=400';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 영화저작물 포스트 팝업오픈 : 부모창에서 오픈
function openNews(postUrl) {
	var url = postUrl;
	var name = '';
	var openInfo = 'target=mvieRghtSrch, width=650, height=450, scrollbars=yes,toolbar=no,location=no,status=yes,menubar=no,resizable=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=O'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch, width=705, height=476, scrollbars=yes';
	
	//alert(url);
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifNewsRghtSrch"); 
	
	if(emptyYn != 'Y'){// 검색조건이 없을 경우 //20120220 정병호
		var totalRow = ${newsList.totalRow}
		parent.document.getElementById("totalRow").value = totalRow //+"건"; //초기페이지 예외처리하기.
	}
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	parent.frames.scrollTo(0,0);
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2011/common.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
.grid tbody td.confirm{
	background: #f2f2f2 url(/images/2011/common/necessary3.gif) no-repeat 2% 10% !important;
	padding-left: 0px !important;
}

-->
</style>
</head>
<body>
<form name="ifFrm" action="#">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="sTotalRow" value="${newsList.totalRow}"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
	<input type="hidden" name="srchPublisher" value="${srchParam.srchPublisher }"/>
	<input type="hidden" name="srchBookTitle" value="${srchParam.srchBookTitle }"/>
	<input type="hidden" name="srchLicensorNm" value="${srchParam.srchLicensorNm }"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
	<input type="hidden" name="srchLicensor" value="${srchParam.srchLicensor }"/>
	<input type="hidden" name="srchProviderNm" value="${srchParam.srchProviderNm }"/>
	<input type="submit" style="display:none;">
	
	<c:forEach items="${srchParam.srchNoneRole_arr }" var="srchNoneRole_arr">
		<c:set var="i" value="${i+1}"/>
		<input type="hidden" name="srchNoneRole_arr" value="${srchParam.srchNoneRole_arr[i-1] }" />
	</c:forEach>
	
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:auto;padding:0 0 0 0;margin-top:1px;">
			<table id="tab_scroll" cellspacing="0" cellpadding="0" border="1" class="grid mt5" summary="도서 저작물의 저작물명, 도서명, 발배년도, 저자, 역자, 출판사, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="2%" />
				<col width="*" />
				<col width="18%" />
				<col width="16%" />
				<col width="12%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col"><input type="checkbox" id="chk" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','iChk',this);" style="cursor:pointer;" title="전체선택"/></th>
						<th scope="col">뉴스기사제목</th>
						<th scope="col">언론사명</th>
						<th scope="col">기사일자</th>
						<th scope="col">저작권찾기</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${emptyYn == 'Y'}"><!-- 검색조건이 없을 경우 //20120220 정병호 -->
					<tr>
						<td class="ce" colspan="5">검색조건을 입력해 주세요.</td>
					</tr>
				</c:if>
				<c:if test="${newsList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="5">검색된 뉴스저작물이 없습니다.</td>
					</tr>
				</c:if>
				<c:if test="${newsList.totalRow > 0}">
					<c:forEach items="${newsList.resultList}" var="newsList">
						<c:set var="NO" value="${newsList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<td class="ce">
						<input type="checkbox" name="iChk" class="vmid" value="${newsList.CR_ID }|${newsList.BOOK_NR_ID }" style="cursor:pointer;" title="선택"/>
							<!-- hidden value start -->
							<input type="hidden" name="title" value="${newsList.TITLE}" />
							<input type="hidden" name="providerName" value="${newsList.PROVIDER_NM}" />
							<input type="hidden" name="articlPubcSdate" value="${newsList.ARTICL_PUBC_SDATE}" />
							<input type="hidden" name="crId" value="${newsList.CR_ID}" />
							
							<!-- hidden value end -->
							<!-- 저작권미상 -->
							<input type="hidden" name="maker_yn" value="${newsList.PROVIDER_YN}" />
							<!-- hidden value end -->
						</td>
						<td>
							<a href="#1" onclick="javascript:openNewsDetail('${newsList.CR_ID }');" class="underline black2">${newsList.TITLE }</a>
							<c:if test="${newsList.LINK_PAGE_URL != '' && newsList.LINK_PAGE_URL != null}">
								<a href="#1" onclick="javascript:openNews('${newsList.LINK_PAGE_URL}');"><img src="/images/common/ic_preview.gif" class="vtop" alt="뉴스저작물 기사조회" /></a>
							</c:if>
						</td>
						<td class="ce<c:if test="${newsList.PROVIDER_YN == '0'}"> confirm</c:if>">${newsList.PROVIDER_NM }</td>
						<td class="ce">${newsList.ARTICL_PUBC_SDATE }</td>
						<td class="ce">
							<c:if test="${newsList.PRPS_CNT >0}">
								<a href="#1" onclick="javascript:openRghtPrps('${newsList.CR_ID }','${newsList.NR_ID }','${newsList.ALBUM_ID }');"><img src="/images/common/ic_newWin.gif" class="vtop" alt="저작권찾기신청내역" /></a>
							</c:if>
						</td>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2011.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${newsList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
