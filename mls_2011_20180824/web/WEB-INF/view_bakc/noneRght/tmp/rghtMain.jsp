<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>권리자미확인 저작물 | 저작권찾기</title>

<link type="text/css" rel="stylesheet" href="/css/2010/style.css">
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<script type="text/javascript" src="/js/2010/DOMScript.js"></script>	
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/2010/prototype.js"> </script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/Function.js"></script>
<script type="text/JavaScript" language="javascript" src="/js/2010/floater.js"></script>
<script type="text/JavaScript">
<!--

// 로딩 이미지 박스
function showAjaxBox(ment){
   
	//항상 화면 중앙에 나타나도록...
   var yp=document.body.scrollTop;
   var xp=document.body.scrollLeft;

   var ws=document.body.clientWidth;
   var hs=document.body.clientHeight;
   
   if(!ment) ment="잠시만 기다려주세요..";
   
   var ajaxBox=$('ajaxBox');
   $('ajaxBoxMent').innerHTML=ment;

   ajaxBox.style.top=yp+eval(hs)/2-100;
   ajaxBox.style.left=xp+eval(ws)/2-100;


  Element.show(ajaxBox);    
}

// 로딩 이미지 박스 감추기
function hideAjaxBox(){
	Element.hide('ajaxBox');	
}

/*calendar호출*/
function fn_cal(frmName, objName){
	showCalendar(frmName, objName);
}

//숫자만 입력
function only_arabic(t){
	var key = (window.netscape) ? t.which :  event.keyCode;

	if (key < 45 || key > 57) {
		if(window.netscape){  // 파이어폭스
			t.preventDefault();
		}else{
			event.returnValue = false;
		}
	} else {
		//alert('숫자만 입력 가능합니다.');
		if(window.netscape){   // 파이어폭스
			return true;
		}else{
			event.returnValue = true;
		}
	}	
}
	
// 날짜체크 
function checkValDate(){
	var f = document.frm;
	if(f.srchStartDate.value!='' && f.srchEndDate.value!=''){
		if(parseInt(f.srchStartDate.value,10)>parseInt(f.srchEndDate.value,10)){
			alert('만료일이 시작일보다 이전입니다.\n일자를 다시 확인하십시요');
			f.srchEndDate.value='';
			return false;
		}
	}

	if(f.srchStartDate.value!=''){
		if(f.srchStartDate.value.length != 8){
			alert('일자형식은 8자리 입니다.');
			f.srchStartDate.value='';
			return false;
		}
	}
	if(f.srchEndDate.value!=''){
		if(f.srchEndDate.value.length != 8){
			alert('일자형식은 8자리 입니다.');
			f.srchEndDate.value='';
			return false;
		}
	}
}

//iframe 검색
function fn_frameList()
{
	var div = '${DIVS}';
	var frm = document.frm;
	
	if(div == 'M'){
		frm.target = "noneMuscRghtSrch";
		frm.action = "/noneRght/rghtSrch.do?method=subList&page_no=1&DIVS=M";
	}else if(div == 'B'){
		frm.target = "noneBookRghtSrch";
		frm.action = "/noneRght/rghtSrch.do?method=subList&page_no=1&DIVS=B";
	}else if(div == 'C'){
		frm.target = "noneScriptRghtSrch";
		frm.action = "/noneRght/rghtSrch.do?method=subList&page_no=1&DIVS=C";
	}else if(div == 'I'){
		frm.target = "noneImageRghtSrch";
		frm.action = "/noneRght/rghtSrch.do?method=subList&page_no=1&DIVS=I";
	}else if(div == 'V'){
		frm.target = "noneMvieRghtSrch";
		frm.action = "/noneRght/rghtSrch.do?method=subList&page_no=1&DIVS=V";
	}else if(div == 'R'){
		frm.target = "noneBroadcastRghtSrch";
		frm.action = "/noneRght/rghtSrch.do?method=subList&page_no=1&DIVS=R";
	}
	
	//showAjaxBox();		// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.submit();
}

// 상세팝업
function openDetail(url, name, openInfo)
{
	window.open(url, name, openInfo);
}

// 권리찾기신청- 목적선택화면에서 호출한다.
function goRghtPrpsMain(gubun, PRPS_RGHT_CODE)
{	
	document.hidForm.PRPS_RGHT_CODE.value = PRPS_RGHT_CODE;
	
	// 로그인확인
	var userIdnt = "<%=(String) session.getAttribute("sessUserIdnt")%>";
	
	if(userIdnt=='null' ||userIdnt=='')
	{
		alert('로그인이 필요한 화면입니다.');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}
	
	document.hidForm.userIdnt.value = userIdnt;
	
	var frm;
	
	if(gubun == 'mainList')
	{	
		goRghtPrps();
	}
	else
	{	
		frm = document.frm;
		
		if(!inspectCheckBoxField(frm.chk))
		{
			alert('항목을 선택 해 주세요.'); return;
		}
		else
		{
			goSelRghtPrps();
		}
		
	}
	
}

// 권리찾기 목적선택 팝업오픈
function openRghtPrps(gubun)
{	
	// 로그인확인
	var userIdnt = "<%=(String) session.getAttribute("sessUserIdnt")%>";
	
	if(userIdnt=='null' ||userIdnt=='')
	{
		alert('로그인이 필요한 화면입니다.');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}
	
	// 항목선택 확인
	var frm;
	
	if(gubun == 'mainList')
	{	
		window.open("/rghtPrps/rghtSrch.do?method=rghtPrpsSelc&gubun="+gubun, "", 'target=rghtPrps width=705, height=225');
	}
	else
	{	
		frm = document.frm;
		
		if(!inspectCheckBoxField(frm.chk))
		{
			alert('항목을 선택 해 주세요.'); return;
		}
		else
		{
			window.open("/rghtPrps/rghtSrch.do?method=rghtPrpsSelc&gubun="+gubun, "", 'target=rghtPrps width=705, height=225');
		}
		
	}

}

// 권리찾기신청
function goRghtPrps()
{	
	
	var div = '${DIVS }';	
	
	// 아이프레임 
	var frm ;

	if(div == 'M'){
		//frm = muscRghtSrch.ifFrm;
		frm = document.getElementById("ifMuscRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'B'){
		//frm = bookRghtSrch.ifFrm;
		frm = document.getElementById("ifBookRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'C'){
		//frm = scriptRghtSrch.ifFrm;
		frm = document.getElementById("ifScriptRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'I'){
		//frm = mvieRghtSrch.ifFrm;
		frm = document.getElementById("ifImageRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'V'){
		//frm = mvieRghtSrch.ifFrm;
		frm = document.getElementById("ifMvieRghtSrch").contentWindow.document.ifFrm
	}else if(div == 'R'){
		//frm = broadcastRghtSrch.ifFrm;
		frm = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.ifFrm
	}

	if(inspectCheckBoxField(frm.iChk))
		conValue = confirm('선택된 항목을 저작권찾기신청합니다.');
	else
		conValue = confirm('선택 항목없이 저작권찾기신청합니다.');
		
	if(conValue) 
	{				
	    var ifrmInputVal = (getCheckStr(frm, 'Y'));
	
	    frm = document.hidForm;
	    
	    frm.ifrmInput.value = ifrmInputVal;
	    
		//조회조건
		if(div != 'R'){
	    	frm.srchTitle.value = document.getElementById("sch1").value;
	    }else{
	    	frm.srchTitle.value = document.getElementById("sch3").value;
	    } 
	  
	    if(div != 'I'){
		    frm.srchStartDate.value = document.getElementById("sch5").value;
			frm.srchEndDate.value = document.getElementById("sch6").value;
		}
		
		
		if(div == 'M'){
		
			frm.srchLicensor.value = document.getElementById("sch7").value;
			frm.srchProducer.value = document.getElementById("sch2").value;
			frm.srchAlbumTitle.value = document.getElementById("sch3").value;
			frm.srchSinger.value = document.getElementById("sch4").value;
			//frm.srchNonPerf.value = document.getElementById("sch9").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=M";
			
		}else if(div == 'B'){
		
			frm.srchLicensor.value = document.getElementById("sch7").value;
			frm.srchPublisher.value = document.getElementById("sch2").value;
			frm.srchBookTitle.value = document.getElementById("sch3").value;
			frm.srchLicensorNm.value = document.getElementById("sch4").value;
			frm.srchNoneName.value = document.getElementById("sch8").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=B";
			
		}else if(div == 'C'){
		
			frm.srchWriter.value = document.getElementById("sch2").value;
			frm.srchBroadStatName.value = document.getElementById("sch3").value;
			frm.srchDirect.value = document.getElementById("sch4").value; 
			frm.srchPlayers.value = document.getElementById("sch7").value; 
			frm.srchNoneName.value = document.getElementById("sch8").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=C";
			
		}else if(div == 'I'){
			
			frm.srchWorkName.value = document.getElementById("sch1").value;
			frm.srchCoptHodr.value = document.getElementById("sch2").value;
			frm.srchWterDivs.value = document.getElementById("sch3").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=I";
			
		}else if(div == 'V'){
			
			frm.srchDistributor.value = document.getElementById("sch2").value;
			frm.srchDirector.value = document.getElementById("sch3").value;
			frm.srchActor.value = document.getElementById("sch7").value;
			frm.srchNoneName.value = document.getElementById("sch8").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=V";
			
		}else if(div == 'R'){
			
			frm.srchProgName.value = document.getElementById("sch1").value;
			frm.srchMaker.value = document.getElementById("sch2").value;
			
			frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=R";
			
		}
	
		frm.method = "post";
		
		frm.submit();
	}
	else
	{
		return;
	}	
}

// 선택목록 권리찾기신청
function goSelRghtPrps()
{	
	// 로그인확인
	var userIdnt = "<%=(String) session.getAttribute("sessUserIdnt")%>";
	
	if(userIdnt=='null' ||userIdnt=='')
	{
		alert('로그인이 필요한 화면입니다');
		//location.href = "/user/user.do?method=goLogin";
		location.href = "/user/user.do?method=goSgInstall";
		return;
	}
	
	var div = '${DIVS }';	
	var frm = document.frm;
	
	if(inspectCheckBoxField(frm.chk))
	{
		conValue = confirm('선택된 항목을 저작권찾기신청합니다.');

		if(conValue) 
		{		
		    var ifrmInputVal = fncReplaceStr(getCheckStr(frm, 'Y'), "chk", "iChk");
				
		    frm = document.hidForm;
		    
		    frm.ifrmInput.value = ifrmInputVal;
		  // frm.userIdnt.value = userIdnt;
		    
		    //조회조건
			if(div != 'R'){
		    	frm.srchTitle.value = document.getElementById("sch1").value;
		    }else{
		    	frm.srchTitle.value = document.getElementById("sch3").value;
		    } 

		    if(div != 'I'){
			    frm.srchStartDate.value = document.getElementById("sch5").value;
				frm.srchEndDate.value = document.getElementById("sch6").value;
			}

			if(div == 'M'){  // 음악
				
				frm.srchLicensor.value = document.getElementById("sch7").value;
				frm.srchProducer.value = document.getElementById("sch2").value;
				frm.srchAlbumTitle.value = document.getElementById("sch3").value;
				frm.srchSinger.value = document.getElementById("sch4").value;
				//frm.srchNonPerf.value = document.getElementById("sch9").value;
				
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=M";
				
			}else if(div == 'B'){  // 어문 
				
				frm.srchLicensor.value = document.getElementById("sch7").value;
				frm.srchPublisher.value = document.getElementById("sch2").value;
				frm.srchBookTitle.value = document.getElementById("sch3").value;
				frm.srchLicensorNm.value = document.getElementById("sch4").value;
				frm.srchNoneName.value = document.getElementById("sch8").value;
				
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=B";
				
			}else if(div == 'C'){ // 방송대본 
		
				frm.srchWriter.value = document.getElementById("sch2").value;
				frm.srchBroadStatName.value = document.getElementById("sch3").value;
				frm.srchDirect.value = document.getElementById("sch4").value; 
				frm.srchPlayers.value = document.getElementById("sch7").value; 
				frm.srchNoneName.value = document.getElementById("sch8").value;
				
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=C";
			
			}else if(div == 'I'){ // 이미지 
			
				frm.srchWorkName.value = document.getElementById("sch1").value;
				frm.srchCoptHodr.value = document.getElementById("sch2").value;
				frm.srchWterDivs.value = document.getElementById("sch3").value;
				
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=I";
				
			}else if(div == 'V'){  // 영화 
			
				frm.srchDistributor.value = document.getElementById("sch2").value;
				frm.srchDirector.value = document.getElementById("sch3").value;
				frm.srchActor.value = document.getElementById("sch7").value;
				frm.srchNoneName.value = document.getElementById("sch8").value;
				
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=V";
				
			}else if(div == 'R'){  // 방송 
			
				frm.srchProgName.value = document.getElementById("sch1").value;
				frm.srchMaker.value = document.getElementById("sch2").value;
			
				frm.action = "/rghtPrps/rghtSrch.do?method=rghtPrps&DIVS=R";	
				
			}
			
			frm.method = "post";
		
			frm.submit();

		}
		else
		{
			return;
		}
		
	}
	else 
	{
		alert('항목을 선택 해 주세요.');
	}
}

// 리사이즈
function resizeIFrame(name) {

  var the_height = document.getElementById(name).contentWindow.document.body.scrollHeight;
  document.getElementById(name).height= the_height+5;

}

// 선택된 저작물 추가
function fn_add(){
	
	var div = '${DIVS }';
		
	if(div == 'M'){
		
		//iFrame 항목
		var chk = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("iChk");
		var albumTitleObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumTitle");
		var issuedDateObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("issuedDate");
		var lyricistObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("lyricist");
		var composerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("composer");
		var singerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("singer");
		var producerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("producer");
		var crIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("crId");
		var nrIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("nrId");
		var albumIdObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumId");
		var arrangerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("arranger");
		var translatorObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("translator");
		var musicTitleObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("musicTitle");
		var playerObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("player");
		var conductorObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("conductor");
		var featuringObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("featuring");
		var icnNumbObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("icnNumb");
		var albumProducedCrhObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("albumProducedCrh");
		//var nonPerfObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("nonPerf");
		
		var lyricistYnObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("lyricist_yn");
		var composerYnObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("composer_yn");
		var singerYnObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("singer_yn");
		var producerYnObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("producer_yn");
		var arrangerYnObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("arranger_yn");
		var playerYnObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("player_yn");
		var conductorYnObjs = document.getElementById("ifMuscRghtSrch").contentWindow.document.getElementsByName("conductor_yn");
		
	}else if(div == 'B'){
	
		// 어문
		var chk = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("iChk");
		var titleObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("title");
		var bookTitleObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("bookTitle");
		var subTitleObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("subTitle");
		var issuedDateObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("issuedDate");
		var writerObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("writer");
		var translatorObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("translator");
		var publisherObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("publisher");		
		var crIdObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("crId");
		var nrIdObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("nrId");
		var publishTypeObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("publishType");
		var retrieveTypeObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("retrieveType");
		var icnNumbObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("icnNumb");
		
		var writerYnObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("writer_yn");
		var translatorYnObjs = document.getElementById("ifBookRghtSrch").contentWindow.document.getElementsByName("translator_yn");
		
	}else if(div == 'C'){
	
		// 방송대본 
		var chk = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("iChk");
		var titleObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("title");
		var writerObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("writer");
		var subTitleObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("subTitle");
		var directObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("direct");
		var crhIdOfCaObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("crhIdOfCa");
		var insertDateObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("insertDate");
		var broadOrdObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadOrd");		
		var crIdObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("crId");
		var broadDateObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadDate");
		var broadMediNameObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadMediName");
		var broadStatNameObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("broadStatName");
		var playersObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("players");
		var makerObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("maker");
		var icnNumbObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("icnNumb");

		var writerYnObjs = document.getElementById("ifScriptRghtSrch").contentWindow.document.getElementsByName("writer_yn");

	}else if(div == 'I'){
	
		// 이미지
		var chk = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("iChk");
		var imageSeqnObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("imageSeqn");
		var crIdObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("crId");
		var workNmObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("workName");
		var coptHodrObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("coptHodr");
		var lishCompObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("lishComp");
		var wterDivsObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("wterDivs");
		var usexYearObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("usexYear");
		var imageDivsObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("imageDivs");
		var icnNumbObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("icnNumb");
		var workFileNmObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("workFileName");
	
		var coptHodrYnObjs = document.getElementById("ifImageRghtSrch").contentWindow.document.getElementsByName("copt_hodr_yn");

	}else if(div == 'V'){
	
		// 영화
		var chk = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("iChk");
		var mvieTitleObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("mvieTitle");
		var directorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("director");
		var leadingActorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("leadingActor");
		var viewGradeValueObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("viewGradeValue");
		var produceYearObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("produceYear");
		var producerObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("producer");	
		var distributorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("distributor");			
		var investorObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("investor");
		var mediaCodeValueObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("mediaCodeValue");
		
		var crIdObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("crId");
		var nrIdObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("nrId");	
		var albumIdObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("albumId");
		
		var producerYnObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("producer_yn");
		var distributorYnObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("distributor_yn");
		var investorYnObjs = document.getElementById("ifMvieRghtSrch").contentWindow.document.getElementsByName("investor_yn");

	}else if(div == 'R'){
	
		// 방송 
		var chk = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementById("chk");
		var chkObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("iChk");
		var titleObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("title");
		var progNameObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("progName");
		var progOrdSeqObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("progOrdSeq");
		var broadDateObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("broadDate");
		var mediCodeNameObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("mediCodeName");
		var chnlCodeNameObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("chnlCodeName");	
		var progGradObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("progGrad");			
		var makerObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("maker");

		var crIdObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("crId");
		var stdCrhIdObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("icnNumb");	
		
		var makerYnObjs = document.getElementById("ifBroadcastRghtSrch").contentWindow.document.getElementsByName("maker_yn");
	}
	
	// 선택 목록
	var selChkObjs = document.getElementsByName("chk");
	var selCrIdObjs = document.getElementsByName("crId");
	var selNrIdObjs = document.getElementsByName("nrId");
	var selAlbumIdObjs = document.getElementsByName("albumId");
	
	var params = "";
	var frm = document.frm;

	var isExistYn = "N";	//중복여부	
	var isAdd	= false;	//줄 추가여부
	
	var count = 0;
	
	for(var i = 0; i < chkObjs.length; i++) {

		if(chkObjs[i].checked == true && selChkObjs.length > 0) {
			//중복여부 검사
			for(var j=0; j<selChkObjs.length; j++) {
				if(div == 'M' || div == 'V'){
					if(crIdObjs[i].value == selCrIdObjs[j].value && nrIdObjs[i].value == selNrIdObjs[j].value && albumIdObjs[i].value == selAlbumIdObjs[j].value) {
						isExistYn = "Y";
					}
				}else if(div == 'B' ){
					if(crIdObjs[i].value == selCrIdObjs[j].value && nrIdObjs[i].value == selNrIdObjs[j].value ) {
						isExistYn = "Y";
					}
				}else if(div == 'I' || div == 'C' || div == 'R'){
					if(crIdObjs[i].value == selCrIdObjs[j].value) {
						isExistYn = "Y";
					}
				}
			}
			
			if(isExistYn == "N") {
				//줄 추가여부
				isAdd = true;
				count ++;
				
			}else{
				if(div == 'M'){
					alert("선택된 저작물 ["+musicTitleObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'B'){
					alert("선택된 저작물 ["+bookTitleObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'C'){
					alert("선택된 저작물 ["+titleObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'I'){
					alert("선택된 저작물 ["+workNmObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'V'){
					alert("선택된 저작물 ["+mvieTitleObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}else if(div == 'R'){
					alert("선택된 저작물 ["+progNameObjs[i].value+"]는 이미 추가된 저작물입니다.");
				}
				return;
			}
			
		} else if(chkObjs[i].checked == true && selChkObjs.length == 0) { //권리찾기 선택목록이 비웠을때 
			//기본 알림줄 삭제(선택된 목록이 없습니다.)
			var oDummyTr = document.getElementById("dummyTr")
			if(eval(oDummyTr)) 	oDummyTr.parentNode.removeChild(oDummyTr);

			//줄 추가여부		
			isAdd = true;
			count ++;
			
		}
		
		chkObjs[i].checked = false;  // 처리한 후 체크풀기 
		
		//줄 추가실행
		if(isAdd){
			var tbody = document.getElementById("tbl_rghtPrps").getElementsByTagName("TBODY")[0];
			
			var row = document.createElement("TR");
			//tr에 id 지정
			var nLastIdx	= 0;	//현재 대상 테이블 tr의 최대번호을 담음
			var nIdIdx;			//현재 대상 테이블에 있는 tr의 Id 번호를 담음
			var oTrInfo = document.getElementById("tbl_rghtPrps").getElementsByTagName("tr");
			for(h = 0; h < oTrInfo.length; h++){
				if(oTrInfo[h].id != "undefined" && oTrInfo[h].id != ""){
					nIdIdx = Number((oTrInfo[h].id).substr("tbl_rghtPrps".length, oTrInfo[h].id.length));
					if(nLastIdx < nIdIdx)	nLastIdx = nIdIdx;
				}
			}
			
			var sNewId = "tbl_rghtPrps" + (nLastIdx + 1);		//tr에 지정할 id
		    row.id = sNewId;
		    
			if(div == 'M'){
			
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				var td7 = document.createElement("TD");
				var td8 = document.createElement("TD");
				var td9 = document.createElement("TD");
				var td10 = document.createElement("TD");
				//var td11 = document.createElement("TD");
				
				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk"           value="'+crIdObjs[i].value+'|'+nrIdObjs[i].value+'|'+albumIdObjs[i].value+'" style="cursor:pointer;"  title="선택">';
				tdData0 += '<input type="hidden" name="musicTitle"        value="'+ musicTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="albumTitle"        value="'+ albumTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="issuedDate"        value="'+ issuedDateObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="lyricist"            value="'+ lyricistObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="composer"            value="'+ composerObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="singer"        value="'+ singerObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="producer"            value="'+ producerObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="nrId"           value="'+ nrIdObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="albumId"        value="'+ albumIdObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="arranger"        value="'+ arrangerObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="translator"        value="'+ translatorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="player"        value="'+ playerObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="conductor"        value="'+ conductorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="featuring"        value="'+ featuringObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="icnNumb"        value="'+ icnNumbObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="albumProducedCrh"        value="'+ albumProducedCrhObjs[i].value     +'">';
				//tdData0 += '<input type="hidden" name="nonPerf"        value="'+ nonPerfObjs[i].value     +'">';
			
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = musicTitleObjs[i].value;
				td2.innerHTML = albumTitleObjs[i].value;
				td3.innerHTML = issuedDateObjs[i].value;
				td3.className = 'ce';
				
				td4.innerHTML = lyricistObjs[i].value;
				td4.className = 'ce';		if(lyricistYnObjs[i].value == '0') td4.className = 'ce confirm';
				td5.innerHTML = composerObjs[i].value;
				td5.className = 'ce';		if(composerYnObjs[i].value == '0') td5.className = 'ce confirm';
				td6.innerHTML = arrangerObjs[i].value;
				td6.className = 'ce';		if(arrangerYnObjs[i].value == '0') td6.className = 'ce confirm';
				td7.innerHTML = singerObjs[i].value;
				td7.className = 'ce';		if(singerYnObjs[i].value == '0') td7.className = 'ce confirm';
				td8.innerHTML = playerObjs[i].value;
				td8.className = 'ce';		if(playerYnObjs[i].value == '0') td8.className = 'ce confirm';
				td9.innerHTML = conductorObjs[i].value;
				td9.className = 'ce';		if(conductorYnObjs[i].value == '0') td9.className = 'ce confirm';
				td10.innerHTML = producerObjs[i].value;
				td10.className = 'ce';	if(producerYnObjs[i].value == '0') td10.className = 'ce confirm';
				//td11.innerHTML = nonPerfObjs[i].value;
				//td11.className = 'ce';	
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 	
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				row.appendChild(td7); 
				row.appendChild(td8); 
				row.appendChild(td9); 
				row.appendChild(td10); 
				//row.appendChild(td11); 
				
			}else if(div == 'B'){
				
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				
				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk" value="'+crIdObjs[i].value+'|'+nrIdObjs[i].value+'" style="cursor:pointer; title="선택">';
				tdData0 += '<input type="hidden" name="title"        value="'+ titleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="bookTitle"        value="'+ bookTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="subTitle"        value="'+ subTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="issuedDate"        value="'+ issuedDateObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="writer"            value="'+ writerObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="translator"        value="'+ translatorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="publisher"        value="'+ publisherObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="nrId"           value="'+ nrIdObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="publishType"        value="'+ publishTypeObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="retrieveType"        value="'+ retrieveTypeObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="icnNumb"        value="'+ icnNumbObjs[i].value     +'">';
				
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = bookTitleObjs[i].value;
				td2.innerHTML = subTitleObjs[i].value;
				td3.innerHTML = issuedDateObjs[i].value;
				td3.className = 'ce';
				td4.innerHTML = writerObjs[i].value; 
				td4.className = 'ce'; if(writerYnObjs[i].value == '0') td4.className = 'ce confirm';
				td5.innerHTML = translatorObjs[i].value;
				td5.className = 'ce';	if(translatorYnObjs[i].value == '0') td5.className = 'ce confirm';
				td6.innerHTML = publisherObjs[i].value;
				td6.className = 'ce';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				
			}else if(div == 'C'){
				
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				var td7 = document.createElement("TD");
				var td8 = document.createElement("TD");
				var td9 = document.createElement("TD");
				
				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk" value="'+crIdObjs[i].value+'" style="cursor:pointer; title="선택">';
				tdData0 += '<input type="hidden" name="title"        value="'+ titleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="writer"        value="'+ writerObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="subTitle"        value="'+ subTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="direct"            value="'+ directObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="crhIdOfCa"        value="'+ crhIdOfCaObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="insertDate"        value="'+ insertDateObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="broadOrd"        value="'+ broadOrdObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="broadDate"        value="'+ broadDateObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="broadMediName" value="'+ broadMediNameObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="broadStatName" value="'+ broadStatNameObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="players" value="'+ playersObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="maker" value="'+ makerObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="icnNumb" value="'+ icnNumbObjs[i].value        +'">';
								
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = titleObjs[i].value;
				
				td2.innerHTML = writerObjs[i].value;
				if(writerYnObjs[i].value == '0') td2.className = 'ce confirm';
								
				td3.innerHTML = directObjs[i].value;
				td3.className = 'ce';
				td4.innerHTML = broadOrdObjs[i].value;
				td4.className = 'ce';
				td5.innerHTML = broadDateObjs[i].value;
				td5.className = 'ce';
				td6.innerHTML = broadMediNameObjs[i].value;
				td6.className = 'ce';
				td7.innerHTML = broadStatNameObjs[i].value;
				td7.className = 'ce';
				td8.innerHTML = playersObjs[i].value;
				td8.className = 'ce';
				td9.innerHTML = makerObjs[i].value;
				td9.className = 'ce';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				row.appendChild(td7); 
				row.appendChild(td8); 
				row.appendChild(td9);
				
			} else if(div == 'I'){
				
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				
				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk" value="'+crIdObjs[i].value+'" style="cursor:pointer; title="선택">';
				tdData0 += '<input type="hidden" name="imageSeqn"        value="'+ imageSeqnObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="workName"        value="'+ workNmObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="coptHodr"        value="'+ coptHodrObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="lishComp"        value="'+ lishCompObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="wterDivs"            value="'+ wterDivsObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="usexYear"        value="'+ usexYearObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="imageDivs"        value="'+ imageDivsObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="icnNumb"        value="'+ icnNumbObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="workFileName"           value="'+ workFileNmObjs[i].value        +'">';
				
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = workNmObjs[i].value;
				td2.innerHTML = lishCompObjs[i].value;
				td3.innerHTML = wterDivsObjs[i].value;
				td3.className = 'ce';
				td4.innerHTML = usexYearObjs[i].value;
				td4.className = 'ce';
				td5.innerHTML = coptHodrObjs[i].value;
				td5.className = 'ce'; if(coptHodrYnObjs[i].value == '0') td5.className = 'ce confirm';
				td6.innerHTML = imageDivsObjs[i].value;
				td6.className = 'ce';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				
			}else if(div == 'V'){
				
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				var td7 = document.createElement("TD");
				var td8 = document.createElement("TD");
				var td9 = document.createElement("TD");
				
				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk" value="'+crIdObjs[i].value+'|'+nrIdObjs[i].value+'|'+albumIdObjs[i].value+'" style="cursor:pointer; title="선택">';
				tdData0 += '<input type="hidden" name="mvieTitle"        value="'+ mvieTitleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="director"        value="'+ directorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="leadingActor"        value="'+ leadingActorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="viewGradeValue"            value="'+ viewGradeValueObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="produceYear"        value="'+ produceYearObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="producer"        value="'+ producerObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="distributor"        value="'+ distributorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="investor"        value="'+ investorObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="mediaCodeValue"        value="'+ mediaCodeValueObjs[i].value     +'">';	
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="nrId"           value="'+ nrIdObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="albumId"           value="'+ albumIdObjs[i].value        +'">';
				
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = mvieTitleObjs[i].value;
				td2.innerHTML = directorObjs[i].value;
				td3.innerHTML = leadingActorObjs[i].value;
				
				td4.innerHTML = produceYearObjs[i].value;
				td4.className = 'ce';
				td5.innerHTML = mediaCodeValueObjs[i].value;
				td5.className = 'ce';
				td6.innerHTML = viewGradeValueObjs[i].value;
				td6.className = 'ce';
				
				td7.innerHTML = producerObjs[i].value;
				td7.className = 'ce'; if(producerYnObjs[i].value == '0') td6.className = 'ce confirm';
				td8.innerHTML = distributorObjs[i].value;
				td8.className = 'ce'; if(distributorYnObjs[i].value == '0') td7.className = 'ce confirm';
				td9.innerHTML = investorObjs[i].value;
				td9.className = 'ce'; if(investorYnObjs[i].value == '0') td8.className = 'ce confirm';
				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				row.appendChild(td7); 
				row.appendChild(td8); 
				row.appendChild(td9); 
				
			} else if(div == 'R'){
			
				var td0 = document.createElement("TD");
				var td1 = document.createElement("TD"); 
				var td2 = document.createElement("TD"); 
				var td3 = document.createElement("TD"); 
				var td4 = document.createElement("TD"); 
				var td5 = document.createElement("TD");
				var td6 = document.createElement("TD");
				var td7 = document.createElement("TD");
				var td8 = document.createElement("TD");

				var tdData0 = "";
				tdData0 += '<input type="checkbox" name="chk"           value="'+crIdObjs[i].value+'" style="cursor:pointer; title="선택">';
				tdData0 += '<input type="hidden" name="title"        value="'+ titleObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="progName"        value="'+ progNameObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="progOrdSeq"        value="'+ progOrdSeqObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="broadDate"            value="'+ broadDateObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="mediCodeName"            value="'+ mediCodeNameObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="chnlCodeName"        value="'+ chnlCodeNameObjs[i].value     +'">';
				tdData0 += '<input type="hidden" name="progGrad"            value="'+ progGradObjs[i].value         +'">';
				tdData0 += '<input type="hidden" name="crId"          value="'+ crIdObjs[i].value       +'">';
				tdData0 += '<input type="hidden" name="maker"           value="'+ makerObjs[i].value        +'">';
				tdData0 += '<input type="hidden" name="icnNumb"        value="'+ stdCrhIdObjs[i].value     +'">';
				
				td0.innerHTML = tdData0;
				td0.className = 'ce';
				td1.innerHTML = titleObjs[i].value;
				td2.innerHTML = progNameObjs[i].value;
				
				td3.innerHTML = makerObjs[i].value;
				td3.className = 'ce';  if(makerYnObjs[i].value == '0') td3.className = 'ce confirm';
				
				td4.innerHTML = progOrdSeqObjs[i].value;
				td4.className = 'ce';
				td5.innerHTML = broadDateObjs[i].value;
				td5.className = 'ce';
				td6.innerHTML = mediCodeNameObjs[i].value;
				td6.className = 'ce';
				td7.innerHTML = chnlCodeNameObjs[i].value;
				td7.className = 'ce';
				td8.innerHTML = progGradObjs[i].value;
				td8.className = 'ce';

				
				row.appendChild(td0); 
				row.appendChild(td1); 
				row.appendChild(td2); 
				row.appendChild(td3); 
				row.appendChild(td4); 
				row.appendChild(td5); 
				row.appendChild(td6); 
				row.appendChild(td7); 
				row.appendChild(td8); 
				
			} 
			
			tbody.appendChild(row); 
			isAdd = false;
		
		}	
	}	
	
	// 전체선택 해제 
	chk.checked = false;
	
	if(count == 0){
		alert('선택된 저작물이 없습니다.');
		return;
	}	
	
	//높이 조절
	resizeSelTbl();
	
}

// 선택된 저작물 삭제
function fn_delete(){
	
	//선택 목록
	var subChk = document.getElementById("subChk");
	var chkObjs = document.getElementsByName("chk");
	var sCheck = 0;
	
	for(i=0; i<chkObjs.length;i++){
		if(chkObjs[i].checked){
			sCheck = 1;
		}
	}
	
	if(sCheck == 0 ){
		alert('선택된 저작물이 없습니다.');
		return;
	}

	for(var i=chkObjs.length-1; i >= 0; i--){

		var chkObj = chkObjs[i];

		if(chkObj.checked){
			// 선택된 저작물을 삭제한다.
			var oTR = findParentTag(chkObj, "TR");	
			if(eval(oTR)) 	oTR.parentNode.removeChild(oTR);
		}
	}
	
	// 전체선택 해제 
	subChk.checked = false;
	
	//높이 조절
	resizeSelTbl();
}

//높이 조절
function resizeSelTbl(){
	
	if("${DIVS}" == 'M' || "${DIVS}" == 'C' || "${DIVS}" == 'V' || "${DIVS}" == 'R'){
		resizeDiv("tbl_rghtPrps", "div_rghtPrps");
	}
	
}

//table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

window.onload =	function(){
	
	fn_frameList();
	
	var div = '${DIVS}';
	
	if("${DIVS}" == 'M' || "${DIVS}" == 'C' || "${DIVS}" == 'V' || "${DIVS}" == 'R'){
		resizeDiv("tbl_rghtPrps", "div_rghtPrps");
	}
}

//헬프메시지 레이어를 화면에 뿌려준다
function viewMessage(pageName)
{
	var layer = document.getElementById('helpMessage');
	
	if(typeof(layer) != 'undefined')
	{
		var forms = document.helpForm;
		
		forms.pageName.value = pageName;
		forms.action = '/common/help_message.jsp';
		
		var result = trim(synchRequest(getQueryString(forms)));
		
		layer.innerHTML = result;
		
		//layer.clientHeight의 값을 정확히 구하기 위해 미리 보이지 않는 사각에 layer을 미리 화면에 띄어준다.
		layer.style.top = -1000;
		layer.style.display = '';
		
		//var top = window.event.clientY + document.body.scrollTop - (layer.clientHeight + 50);
		
		//if(top < 0)
		//	top = 0;
			
		//if(top < document.body.scrollTop)
		//	top = top + (document.body.scrollTop - top);
			
		//layer.style.top = top;
		
		//layer.style.left=document.body.scrollLeft + window.event.clientX;
    	//layer.style.top=document.body.scrollTop + window.event.clientY;
		
		layer.style.left=window.event.clientX+(document.documentElement.scrollLeft || 
            document.body.scrollLeft) - 
            document.documentElement.clientLeft;
            
		layer.style.top=window.event.clientY +(document.documentElement.scrollTop || 
            document.body.scrollTop) - 
            document.documentElement.clientTop;
		
		layer.style.display = '';
	}
}

//헬프메시지 레이어를 닫아줌

function closeMessage()
{
	var layer = document.getElementById('helpMessage');
	
	if(typeof(layer) != 'undefined')
		layer.style.display = 'none';
}

action = window.setInterval("floatLayer('helpMessage')", 1);
//-->
</script>
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: auto;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		<jsp:include page="/include/2010/header.jsp" />
		<script type="text/javascript">initNavigation(0);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<div id="container">
			<div class="content">
				<div class="left">
					<h2><img src="/images/2010/common/left_h2_1.png" alt="저작권정보 조회" title="저작권정보 조회" class="png24" /></h2>
					<!-- left menu str -->
					<div class="sMenu">
						<ul>
							<li><a href="/rghtPrps/rghtSrch.do?DIVS=M">음악<span>&nbsp;</span></a></li>
							<li><a href="/rghtPrps/rghtSrch.do?DIVS=B">도서<span>&nbsp;</span></a></li>
							<li><a href="/rghtPrps/rghtSrch.do?DIVS=C">방송대본<span>&nbsp;</span></a></li>
							<li><a href="/rghtPrps/rghtSrch.do?DIVS=I">이미지<span>&nbsp;</span></a></li>
							<li><a href="/rghtPrps/rghtSrch.do?DIVS=V">영화<span>&nbsp;</span></a></li>
							<li><a href="/rghtPrps/rghtSrch.do?DIVS=R">방송<span>&nbsp;</span></a></li>
							<li><a href="/rghtPrps/rghtSrch.do?DIVS=X">기타<span>&nbsp;</span></a></li>
							<li class="active"><a href="/noneRght/rghtSrch.do?DIVS=M">권리자미확인 저작물<span>&lt;</span></a></li>
							<li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=8&amp;page_no=1">법정허락저작물<span>&lt;</span></a></li>
						</ul>
					</div>
					<!-- left menu end -->
				</div>
				
				<div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2010/common/lodingBg.gif) no-repeat 0 0; width: 306px; height: 38px; padding: 102px 0 0 0;">
					<p style="height: 38px; text-align: center; margin: 0;">
						<img src="/images/2010/common/loading.gif" alt="" /><br />
						<span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #035392; font-weight: bold;">잠시만 기다려주세요..</span>
					</p>
				</div>
				
				<div class="contentBody" id="contentBody">
					<p class="path"><span class="skip">현재위치 : </span><span><img src="/images/2010/common/path_h.gif" alt="Home" class="vmid" /></span>&gt;<span>저작권정보 조회</span>&gt;<strong>권리자미확인 저작물(<c:if test="${DIVS == 'M'}">음악</c:if><c:if test="${DIVS == 'B'}">도서</c:if><c:if test="${DIVS == 'C'}">방송대본</c:if><c:if test="${DIVS == 'I'}">이미지</c:if><c:if test="${DIVS == 'V'}">영화</c:if><c:if test="${DIVS == 'R'}">방송</c:if>)</strong></p>
					<div class="subVis">
						<p class="rgt"><img src="/images/2010/common/top_visual.png" class="png24" alt="탑이미지" /></p>
						<h3>권리자미확인 저작물(<c:if test="${DIVS == 'M'}">음악</c:if><c:if test="${DIVS == 'B'}">도서</c:if><c:if test="${DIVS == 'C'}">방송대본</c:if><c:if test="${DIVS == 'I'}">이미지</c:if><c:if test="${DIVS == 'V'}">영화</c:if><c:if test="${DIVS == 'R'}">방송</c:if>)
							<span><img src="/images/2010/common/typoBg.gif" class="vtop" alt="권리자들은 쉽게 자신의 저작권을 확인하고, 보상금을 신청할 수 있습니다." /></span>
						</h3>
					</div>
					
					<div class="tabStyle">
						<ul id="tab">
							<li><a href="/noneRght/rghtSrch.do?DIVS=M" <c:if test="${DIVS == 'M'}">class="active"</c:if>><strong>음악</strong></a></li>
							<li><a href="/noneRght/rghtSrch.do?DIVS=B" <c:if test="${DIVS == 'B'}">class="active"</c:if>><strong>도서</strong></a></li>
							<li><a href="/noneRght/rghtSrch.do?DIVS=C" <c:if test="${DIVS == 'C'}">class="active"</c:if>><strong>방송대본</strong></a></li>
							<li><a href="/noneRght/rghtSrch.do?DIVS=I" <c:if test="${DIVS == 'I'}">class="active"</c:if>><strong>이미지</strong></a></li>
							<li><a href="/noneRght/rghtSrch.do?DIVS=V" <c:if test="${DIVS == 'V'}">class="active"</c:if>><strong>영화</strong></a></li>
							<li><a href="/noneRght/rghtSrch.do?DIVS=R" <c:if test="${DIVS == 'R'}">class="active"</c:if>><strong>방송</strong></a></li>
						</ul>
					</div>	
					
					<!-- hidden form str : 헬프레이어 -->
					<form name="helpForm" method="post" action="">
						<input type="hidden" name="pageName">
					</form>

					<!-- hidden form str : 아이프레임데이타 가져가는 용도-->		
					<form name="hidForm" action="" class="sch">
						<input type="hidden" name="userIdnt" />
						<input type="hidden" name="ifrmInput" />
						<input type="hidden" name="PRPS_RGHT_CODE" />
						<input type="hidden" name="srchTitle" />
						<input type="hidden" name="srchProducer" />
						<input type="hidden" name="srchAlbumTitle" />
						<input type="hidden" name="srchSinger" />
						<input type="hidden" name="srchStartDate" />
						<input type="hidden" name="srchEndDate" />
						<input type="hidden" name="srchNoneName" />
						<!-- 음악 -->
						<input type="hidden" name="srchNonPerf" />
						<!-- 도서 -->
						<input type="hidden" name="srchLicensor" />
						<input type="hidden" name="srchPublisher" />
						<input type="hidden" name="srchBookTitle" />
						<input type="hidden" name="srchLicensorNm" />
						<!-- 방송대본 -->
						<input type="hidden" name="srchWriter" />
						<input type="hidden" name="srchBroadStatName" />
						<input type="hidden" name="srchDirect" />
						<input type="hidden" name="srchPlayers" />
						<!-- 이미지 -->
						<input type="hidden" name="srchWorkName" />
						<input type="hidden" name="srchLishComp" />
						<input type="hidden" name="srchCoptHodr" />
						<input type="hidden" name="srchWterDivs" />
						<!-- 영화 -->
						<input type="hidden" name="srchDistributor" />
						<input type="hidden" name="srchDirector" />
						<input type="hidden" name="srchViewGrade" />
						<input type="hidden" name="srchActor" />
						<!-- 방송 -->
						<input type="hidden" name="srchProgName" />
						<input type="hidden" name="srchMaker" />
						<!-- 접근목록 -->
						<input type="hidden" name="listDivs" value="noneRghtList"/>
					</form>
					<!-- search form str -->					
					<form name="frm" action="" class="sch mt0">
						<!-- hidden 접근목록 -->
						<input type="hidden" name="listDivs" value="noneRghtList"/>
						
						<fieldset class="sch">
							<legend>저작물검색</legend>
							<div class="contentsSch mt0">
								<span class="round lb"></span><span class="round rb"></span>
								<table border="1" summary="저작물검색" class="w90"><!-- 스타일제거시 테이블형태 유지를 위해 보더를 준다 -->
									<caption>저작물검색 항목 입력을 위한 검색 테이블</caption><!-- summary가 있을 경우 생략이 가능하다 -->
<c:if test="${DIVS == 'M'}">
									<colgroup>
										<col width="16%">
										<col width="19%">
										<col width="23%">
										<col width="12%">
										<col width="*">
									</colgroup>
</c:if>

<c:if test="${DIVS != 'M'}">
									<colgroup>
										<col width="16%">
										<col width="19%">
										<col width="23%">
										<col width="15%">
										<col width="*">
									</colgroup>
</c:if>

									<tbody>
<c:if test="${DIVS == 'M'}">		
										<tr>
											<td rowspan="4"><img src="/images/2010/common/schBoxBg.gif" alt="Search" /></td>
											<th><label for="sch7">작사/작곡/편곡</label></th>
											<td><input class="inputData" id="sch7" name="srchLicensor" value="${srchLicensor}" /></td>
											<th><label for="sch1">곡명</label></th><!-- 레이블과 id로 타이틀과 인풋을 연결시켜준다 -->
											<td><input class="inputData" id="sch1" name="srchTitle" value="${srchTitle}" /></td>
										</tr>	
										<tr>
											<th><label for="sch4">가창/연주/지휘</label></th>
											<td><input class="inputData" id="sch4" name="srchSinger" value="${srchSinger}" /></td>
											<th><label for="sch3">앨범명</label></th>
											<td><input class="inputData" id="sch3" name="srchAlbumTitle" value="${srchAlbumTitle}" /></td>
										</tr>
										<tr>
											<th><label for="sch2">음반제작사</label></th>
											<td><input class="inputData" id="sch2" name="srchProducer" value="${srchProducer}" /></td>
											<th><label for="sch5">발매일자</label></th>
											<td>
												
												<input type="hidden" name="srchStartDate_tmp" value="${srchStartDate_tmp}" title="임시발매일(시작)">
												<input type="hidden" name="srchEndDate_tmp" value="${srchEndDate_tmp}" title="임시발매일(종료)">
											
												<input class="inputData" type="text" id="sch5" title="발매일자(시작)" name="srchStartDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchStartDate}" /> 
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/> 
													~ 
												<input class="inputData" type="text" id="sch6" title="발매일자(종료)" name="srchEndDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchEndDate}" />
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" alt="달력" title="마지막 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>
											</td>
										</tr>
										<tr>
											<th><label for="sch8">저작(권)자명</label></th>
											<td><input class="inputData" id="sch8" name="srchNoneName" value="${srchNoneName}" /></td>
											<th><label>권리자<br/>미확인<br/>항목</label></th>
											<td>
											<!--  
												<label><input type="checkbox" name="srchNoneRole_arr" value="LYRICIST_YN" class="inputRChk" title="작사" />작사</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="COMPOSER_YN" class="inputRChk" title="작곡"/>작곡</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="ARRANGER_YN" class="inputRChk" title="편곡"/>편곡</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="SINGER_YN" class="inputRChk" title="가창"/>가창</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="PLAYER_YN" class="inputRChk" title="연주"/>연주</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="CONDUCTOR_YN" class="inputRChk" title="지휘"/>지휘</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="PRODUCER_YN" class="inputRChk" title="음반제작사"/>음반제작사</label>
											-->	
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_LICENSOR('1',AM.CR_ID,'1','201')" class="inputRChk" title="작사" />작사</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_LICENSOR('1',AM.CR_ID,'2','201')" class="inputRChk" title="작곡"/>작곡</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_LICENSOR('1',AM.CR_ID,'3','201')" class="inputRChk" title="편곡"/>편곡</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_PERFORMER('1',AM.CR_ID, AM.NR_ID, AM.ALBUM_ID, '1','202')" class="inputRChk" title="가창"/>가창</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_PERFORMER('1',AM.CR_ID, AM.NR_ID, AM.ALBUM_ID, '2','202')" class="inputRChk" title="연주"/>연주</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_PERFORMER('1',AM.CR_ID, AM.NR_ID, AM.ALBUM_ID, '3','202')" class="inputRChk" title="지휘"/>지휘</label>
											<!-- 
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_PERFORMER('1',AM.CR_ID, AM.NR_ID, AM.ALBUM_ID, '5','203')" class="inputRChk" title="음반제작사"/>음반제작사</label>
											 -->	
												<label><input type="checkbox" name="srchNoneRole_arr" value="DECODE(NVL(KAPP_TRUST_YN,'N'), 'N', 0, 1 )" class="inputRChk" title="음반제작사"/>음반제작사</label>
											</td>
										</tr>	
										<!-- 
										<tr>
											<td rowspan="5"><img src="/images/2010/common/schBoxBg.gif" alt="Search" /></td>
											<th><label for="sch7">작사/작곡/편곡</label></th>
											<td><input class="inputData" id="sch7" name="srchLicensor" value="${srchLicensor}" /></td>
											<th><label for="sch1">곡명</label></th>
											<td><input class="inputData" id="sch1" name="srchTitle" value="${srchTitle}" /></td>
										</tr>	
										<tr>
											<th><label for="sch4">가창/연주/지휘</label></th>
											<td><input class="inputData" id="sch4" name="srchSinger" value="${srchSinger}" /></td>
											<th><label for="sch3">앨범명</label></th>
											<td><input class="inputData" id="sch3" name="srchAlbumTitle" value="${srchAlbumTitle}" /></td>
										</tr>
										<tr>
											<th><label for="sch2">음반제작사</label></th>
											<td><input class="inputData" id="sch2" name="srchProducer" value="${srchProducer}" /></td>
											<th><label for="sch5">발매일자</label></th>
											<td>
												
												<input type="hidden" name="srchStartDate_tmp" value="${srchStartDate_tmp}" title="임시발매일(시작)">
												<input type="hidden" name="srchEndDate_tmp" value="${srchEndDate_tmp}" title="임시발매일(종료)">
											
												<input class="inputData" type="text" id="sch5" title="발매일자(시작)" name="srchStartDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchStartDate}" /> 
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/> 
													~ 
												<input class="inputData" type="text" id="sch6" title="발매일자(종료)" name="srchEndDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" onblur='javascript:checkValDate();' value="${srchEndDate}" />
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" alt="달력" title="마지막 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>
											</td>
										</tr>
										
										<tr>
											<th><label for="sch9">미확인실연자</label></th>
											<td><input class="inputData" id="sch9" name="srchNonPerf" value="${srchNonPerf}" /></td>
											<th  rowspan="2"><label>권리자<br/>미확인<br/>항목</label></th>
											<td rowspan="2">
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_LICENSOR('1',AM.CR_ID,'1','201')" class="inputRChk" title="작사" />작사</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_LICENSOR('1',AM.CR_ID,'2','201')" class="inputRChk" title="작곡"/>작곡</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_LICENSOR('1',AM.CR_ID,'3','201')" class="inputRChk" title="편곡"/>편곡</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_PERFORMER('1',AM.CR_ID, AM.NR_ID, AM.ALBUM_ID, '1','202')" class="inputRChk" title="가창"/>가창</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_PERFORMER('1',AM.CR_ID, AM.NR_ID, AM.ALBUM_ID, '2','202')" class="inputRChk" title="연주"/>연주</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_PERFORMER('1',AM.CR_ID, AM.NR_ID, AM.ALBUM_ID, '3','202')" class="inputRChk" title="지휘"/>지휘</label>
											-->
											<!-- 
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_PERFORMER('1',AM.CR_ID, AM.NR_ID, AM.ALBUM_ID, '5','203')" class="inputRChk" title="음반제작사"/>음반제작사</label>
											 -->
											 <!-- 
												<label><input type="checkbox" name="srchNoneRole_arr" value="DECODE(NVL(KAPP_TRUST_YN,'N'), 'N', 0, 1 )" class="inputRChk" title="음반제작사"/>음반제작사</label>
											</td>
										</tr>	
										<tr>
											<th><label for="sch8">저작(권)자명</label></th>
											<td><input class="inputData" id="sch8" name="srchNoneName" value="${srchNoneName}" /></td>
										</tr>		
										 -->				
										
</c:if>
<c:if test="${DIVS == 'B'}">							
										<tr>
											<td rowspan="4"><img src="/images/2010/common/schBoxBg.gif" alt="Search"  /></td>
											<th><label for="sch4">작가명</label></th>
											<td><input class="inputData" id="sch4" name="srchLicensorNm" value="${srchLicensorNm}" /></td>
											<th><label for="sch1">작품명</label></th><!-- 레이블과 id로 타이틀과 인풋을 연결시켜준다 -->
											<td><input class="inputData" id="sch1" name="srchTitle" value="${srchTitle}" /></td>
										</tr>
										<tr>
											<th><label for="sch7">역자명</label></th>
											<td><input class="inputData" id="sch7" name="srchLicensor" value="${srchLicensor}" /></td>
											<th><label for="sch3">도서명</label></th>
											<td><input class="inputData" id="sch3" name="srchBookTitle" value="${srchBookTitle}" /></td>
											
										</tr>
										<tr>
											<th><label for="sch2">출판사</label></th>
											<td><input class="inputData" id="sch2" name="srchPublisher" value="${srchPublisher}" /></td>
											<th><label for="sch5">발행일자</label></th>
											<td>
												<input class="inputData" type="text" id="sch5" title="발행일자(시작)" name="srchStartDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}"  value="${srchStartDate}" onblur='javascript:checkValDate();' />
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/> 
												~ 
												<input class="inputData" type="text" id="sch6" title="발행일자(종료)" name="srchEndDate" size="8" maxlength="8"  onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" value="${srchEndDate}" onblur='javascript:checkValDate();' />
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" alt="달력" title="마지막 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>
											</td>
										</tr>
										<tr>
											<th style="border-top-width:2px;border-top-color:#177DA3;"><label for="sch8">저작(권)자명</label></th>
											<td><input class="inputData" id="sch8" name="srchNoneName" value="${srchNoneName}" /></td>
											<th><label>권리자 미확인<br/>항목</label></th>
											<td>
												<!--<label><input type="checkbox" name="srchNoneRole_arr" value="WRITER_YN" class="inputRChk" title="저자" />저자</label>-->
												<!--<label><input type="checkbox" name="srchNoneRole_arr" value="TRANSLATOR_YN" class="inputRChk" title="역자" />역자</label>-->
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_LICENSOR('2', B.CR_ID,'5','204')" class="inputRChk" title="저자" />저자</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="FC_GET_ML_TRUST_LICENSOR('2', B.CR_ID,'6','204')" class="inputRChk" title="역자" />역자</label>
											</td>
										</tr>	
</c:if>		
<c:if test="${DIVS == 'C'}">							
										<tr>
											<td rowspan="4"><img src="/images/2010/common/schBoxBg.gif" alt="Search" /></td>
											<th><label for="sch2">작가명</label></th>
											<td><input class="inputData" id="sch2" name="srchWriter" value="${srchWriter}" /></td>
											<th><label for="sch1">작품명</label></th>
											<td><input class="inputData" id="sch1" name="srchTitle" value="${srchTitle}" /></td>
										</tr>
										<tr>
											<th><label for="sch4">연출가</label></th>
											<td><input class="inputData" id="sch4" name="srchDirect" value="${srchDirect}" /></td>
											<th><label for="sch3">방송사</label></th>
											<td><input class="inputData" id="sch3" name="srchBroadStatName" value="${srchBroadStatName}" /></td>
										</tr>
										<tr>
											<th><label for="sch7">주요출연진</label></th>    
											<td><input class="inputData" id="sch7" name="srchPlayers" value="${srchPlayers}" /></td>
											<th><label for="sch5">방송일자</label></th>
											<td>
												<input class="inputData" type="text" id="sch5" title="방송일자(시작)" name="srchStartDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}"  value="${srchStartDate}" onblur='javascript:checkValDate();' />
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/> 
												~ 
												<input class="inputData" type="text" id="sch6" title="방송일자(종료)" name="srchEndDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" value="${srchEndDate}" onblur='javascript:checkValDate();' />
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" alt="달력" title="마지막 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>
											</td>	
										</tr>
										<tr>
											<th><label for="sch8">저작(권)자명</label></th>
											<td><input class="inputData" id="sch8" name="srchNoneName" value="${srchNoneName}" /></td>
											<th><label>권리자 미확인<br/>항목</label></th>
											<td>
												<label><input type="checkbox" name="srchNoneRole_arr" value="WRITER_YN" class="inputRChk" title="작가"/>작가</label>
											</td>
										</tr>
</c:if>	
<c:if test="${DIVS == 'I'}">				
										<tr>
											<td rowspan="3"><img src="/images/2010/common/schBoxBg.gif" alt="Search" /></td>
											<th><label for="sch3">작가명</label></th>
											<td><input class="inputData" id="sch3" name="srchWterDivs" value="${srchWterDivs}" /></td>
											<th><label for="sch1">이미지명</label></th>
											<td><input class="inputData" id="sch1" name="srchWorkName" value="${srchWorkName}" /></td>
										</tr>
										<tr>
											<th><label for="sch2">저작(권)자명</label></th>
											<td><input class="inputData" id="sch2" name="srchCoptHodr" value="${srchCoptHodr}" /></td>
											<th><label>권리자 미확인<br/>항목</label></th>
											<td>
												<label><input type="checkbox" name="srchNoneRole_arr" value="COPT_HODR_YN" class="inputRChk" title="저작권" />저작권</label>
											</td>
										</tr>
</c:if>			
<c:if test="${DIVS == 'V'}">				
										<tr>
											<td rowspan="4"><img src="/images/2010/common/schBoxBg.gif" alt="Search" /></td>
											<th><label for="sch3">감독/연출</label></th>
											<td><input class="inputData" id="sch3" name="srchDirector" value="${srchDirector}" /></td>
											<th><label for="sch1">영화명</label></th><!-- 레이블과 id로 타이틀과 인풋을 연결시켜준다 -->
											<td><input class="inputData" id="sch1" name="srchTitle" value="${srchTitle}" /></td>
										</tr>
										<tr>
											<th><label for="sch2">제작사/배급사/투자사</label></th>
											<td colspan = 3><input class="inputData" id="sch2" name="srchDistributor" value="${srchDistributor}" /></td>
										</tr>
										<tr>
											<th><label for="sch7">주요출연진</label></th>
											<td><input class="inputData" id="sch7" name="srchActor" value="${srchActor}" /></td>
											<th><label for="sch5">제작일자</label></th>
											<td>
												<input class="inputData" type="text" id="sch5" title="제작일자(시작)" name="srchStartDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}"  value="${srchStartDate}" onblur='javascript:checkValDate();' />
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/> 
												~ 
												<input class="inputData" type="text" id="sch6" title="제작일자(종료)" name="srchEndDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" value="${srchEndDate}" onblur='javascript:checkValDate();' />
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" alt="달력" title="마지막 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>
											</td>
										</tr>
										<tr>
											<th><label for="sch8">저작(권)자명</label></th>
											<td><input class="inputData" id="sch8" name="srchNoneName" value="${srchNoneName}" /></td>
											<th><label>권리자 미확인<br/>항목</label></th>
											<td>
												<label><input type="checkbox" name="srchNoneRole_arr" value="DISTRIBUTOR_YN" class="inputRChk" title="제작사"/>제작사</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="DISTRIBUTOR_YN" class="inputRChk" title="배급사" />배급사</label>
												<label><input type="checkbox" name="srchNoneRole_arr" value="INVESTOR_YN" class="inputRChk" title="투자사" />투자사</label>
											</td>
										</tr>	
</c:if>	
<c:if test="${DIVS == 'R'}">
										<tr>
											<td rowspan="3"><img src="/images/2010/common/schBoxBg.gif" alt="Search" /></td>
											<th><label for="sch2">제작자</label></th>
											<td><input class="inputData" id="sch2" name="srchMaker" value="${srchMaker}" /></td>
											<th><label for="sch1">프로그램명</label></th>
											<td><input class="inputData" id="sch1" name="srchProgName" value="${srchProgName}" /></td>
										</tr>
										<tr>
											<th><label for="sch3">저작물명</label></th>
											<td><input class="inputData" id="sch3" name="srchTitle" value="${srchTitle}" /></td>
											<th><label for="sch5">방송일자</label></th>
											<td>
											    <input class="inputData" type="text" id="sch5" title="방송일자(시작)" name="srchStartDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}"  value="${srchStartDate}" onblur='javascript:checkValDate();' />
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchStartDate');" alt="달력" title="시작 날짜를 선택하세요." align="middle" style="cursor:pointer;"/> 
												~ 
												<input class="inputData" type="text" id="sch6" title="방송일자(종료)" name="srchEndDate" size="8" maxlength="8" onkeypress="javascript:if(event.keyCode==13){fn_frameList();}else{only_arabic(event);}" value="${srchEndDate}" onblur='javascript:checkValDate();' />
												<img src="/images2/common/calendar.gif" onclick="javascript:fn_cal('frm','srchEndDate');" alt="달력" title="마지막 날짜를 선택하세요." align="middle" style="cursor:pointer;"/>
											</td>
										</tr>
										<tr>
											<th><label for="sch8">저작(권)자명</label></th>
											<td><input class="inputData" id="sch8" name="srchNoneName" value="${srchNoneName}" /></td>
											<th><label>권리자 미확인<br/>항목</label></th>
											<td>
												<label><input type="checkbox" name="srchNoneRole_arr" value="MAKER_YN" class="inputRChk" title="제작자"/>제작자</label>
											</td>
										</tr>
</c:if>	
										<tr>
											<td></td>
											<td colspan="4"><label class=" fontSmall gray">* 전체 통합 검색 시 메인화면의 통합검색을 이용해 주세요.</label> <label class=" fontSmall gray"><a href="/main/main.do"><u><b>메인화면</b> 바로가기</u></a></label></td>
										</tr>
									</tbody>
								</table>
								<p class="btnArea"><input type="submit" class="imgSchBtn" onclick="javascript:fn_frameList();" /></p>
							</div>
						</fieldset>
					<!-- search form end -->
					
					
					<p style="margin-top:5px;width:730px;line-height:100%;">
						<!-- * 저작(권)자 정보가 없는경우. 또는 정보는 있지만 해당항목이 비신탁인 경우 [<span class="necessary"> </span>]으로 표시됩니다. -->
						<label class="blue">* 권리자 미확인 항목은  [<img src="/images/2010/common/necessary2.gif" class="vmid" alt="!" />]으로 표시됩니다. </label>
						<label class="fontSmall gray underline"><a href="#" onclick="viewMessage('01_01_01');return false;"><u>권리자 미확인이란?</u></a></label>
					</p>
					
					<!-- 
					<p style="margin-top:10px;width:730px;line-height:120%;">
						<br/>
							<c:if test="${DIVS == 'M' || DIVS == 'B'}">
								<label class="blue">* 저작권 이용서비스 
									<c:if test="${DIVS == 'M'}"><a href="http://clms.or.kr/user/music/main_music.do" target="_blank"><label class=" fontSmall gray underline"><b>CLMS</b> 바로가기</label> <img src="/images/2010/main/ic_go.gif" class="vmid" alt="clms바로가기"/></a></c:if>
									<c:if test="${DIVS == 'B'}"><a href="http://clms.or.kr/user/liter/main_liter.do" target="_blank"><label class=" fontSmall gray underline"><b>CLMS</b> 바로가기</label> <img src="/images/2010/main/ic_go.gif" class="vmid" alt="clms바로가기"/></a></c:if>								
								</label>
							</c:if>
					</p>
					 -->
					 					
					<div class="bbsSection" style="margin: 10px 0 10px">
						<div class="floatDiv mb5">
							<!--<p class="fl pt10 pl5">
								<input type="text" name="totalRow" id="totalRow" value="" style="border:0px;font-size:12px;font-weight:700;color:black;"/>
							</p>-->
							<input type="hidden" name="totalRow" id="totalRow" value="" style="border:0px;font-size:12px;font-weight:700;color:black;"/>
							<p class="fr">
							
								<c:if test="${DIVS == 'M'}">
									<!-- 
									<span class="button medium icon"><span class="default">&nbsp;</span><a href="#" onclick="javascript:window.open('http://clms.or.kr/user/music/main_music.do');" onkeypress="javascript:window.open('http://clms.or.kr/user/music/main_music.do');">저작권이용계약신청&nbsp;</a></span>
									 -->
									<span class="button medium icon"><span class="default">&nbsp;</span><a href="#" onclick="javascript:goClms('music');" onkeypress="javascript:goClms('music');">저작권이용계약신청&nbsp;</a></span>
								</c:if>
								<c:if test="${DIVS == 'B'}">
									<!-- 
									<span class="button medium icon"><span class="default">&nbsp;</span><a href="#" onclick="javascript:window.open('http://clms.or.kr/user/liter/main_liter.do');" onkeypress="javascript:window.open('http://clms.or.kr/user/liter/main_liter.do');">저작권이용계약신청&nbsp;</a></span>
									 -->
									<span class="button medium icon"><span class="default">&nbsp;</span><a href="#" onclick="javascript:goClms('book');" onkeypress="javascript:goClms('book');">저작권이용계약신청&nbsp;</a></span>
								</c:if>
								
								<!--  
								// 목적팝업을 오픈
								<span class="button medium icon"><span class="default">&nbsp;</span><a href="#" onclick="javascript:openRghtPrps('mainList');" onkeypress="javascript:openRghtPrps('mainList');">저작권찾기 신청</a></span>
								 -->
								<span class="button medium icon"><span class="default">&nbsp;</span><a onclick="javascript:goRghtPrpsMain('mainList', '1');" onkeypress="javascript:openRghtPrps('mainList', '1');">권리자 저작권찾기 신청</a></span>
								<span class="button medium icon"><span class="default">&nbsp;</span><a onclick="javascript:goRghtPrpsMain('mainList', '3');" onkeypress="javascript:openRghtPrps('mainList', '3');">이용자 저작권찾기 신청</a></span>
							</p>
						</div>
						
						<!-- iframe str -->
						<c:if test="${DIVS == 'M'}">
							<iframe id="ifMuscRghtSrch" title="저작물조회(음악)" name="noneMuscRghtSrch" 
								width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
								scrolling="no" style="overflow-y:hidden;">
							</iframe>
						</c:if>
						<c:if test="${DIVS == 'B'}">
							<iframe id="ifBookRghtSrch" title="저작물조회(어문)" name="noneBookRghtSrch" 
								width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
								scrolling="no" style="overflow-y:hidden;">
							</iframe>
						</c:if>
						<c:if test="${DIVS == 'C'}">
							<iframe id="ifScriptRghtSrch" title="저작물조회(방송대본)" name="noneScriptRghtSrch" 
								width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
								scrolling="no" style="overflow-y:hidden;">
							</iframe>
						</c:if>
						<c:if test="${DIVS == 'I'}">
							<iframe id="ifImageRghtSrch" title="저작물조회(이미지)" name="noneImageRghtSrch" 
								width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
								scrolling="no" style="overflow-y:hidden;">
							</iframe>
						</c:if>
						<c:if test="${DIVS == 'V'}">
							<iframe id="ifMvieRghtSrch" title="저작물조회(영화)" name="noneMvieRghtSrch" 
								width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
								scrolling="no" style="overflow-y:hidden;">
							</iframe>
						</c:if>
						<c:if test="${DIVS == 'R'}">
							<iframe id="ifBroadcastRghtSrch" title="저작물조회(방송)" name="noneBroadcastRghtSrch" 
								width="100%" height="400" marginwidth="0" marginheight="0" frameborder="0" 
								scrolling="no" style="overflow-y:hidden;">
							</iframe>
						</c:if>
						<!-- iframe end -->
						
					</div>
						
					<!-- //버튼영역 -->
					<p class="ce mt5">
						<span class="button small type4 icon"><span class="arrow"></span><a href="javascript:fn_add();">추가</a></span> 
						<span class="button small type3"><a href="javascript:fn_delete();">삭제</a></span>
					</p>
					<!-- //버튼영역 -->
					
					<div class="floatDiv mb5">
						<h4 class="fl"><c:if test="${DIVS == 'M'}">음악</c:if><c:if test="${DIVS == 'B'}">어문</c:if><c:if test="${DIVS == 'I'}">이미지</c:if><c:if test="${DIVS == 'V'}">영화</c:if>저작권찾기 선택목록</h4>
						<p class="fr">
							<!--  
							// 목적팝업을 오픈
							<span class="button medium icon"><span class="default">&nbsp;</span><a href="#" onclick="javascript:openRghtPrps('subList')" onkeypress="javascript:openRghtPrps('subList')">저작권찾기 신청</a></span>
							 -->
							<span class="button medium icon"><span class="default">&nbsp;</span><a onclick="javascript:goRghtPrpsMain('subList', '1');" onkeypress="javascript:openRghtPrps('subList', '1');">권리자 저작권찾기 신청</a></span>
							<span class="button medium icon"><span class="default">&nbsp;</span><a onclick="javascript:goRghtPrpsMain('subList', '3');" onkeypress="javascript:openRghtPrps('subList', '3');">이용자 저작권찾기 신청</a></span>
						</p>
					</div>
					
					<!-- 테이블 영역입니다 -->
		
					<div id="div_rghtPrps"  class="tabelRound <c:if test="${DIVS == 'M' || DIVS == 'C' || DIVS == 'V' || DIVS == 'R'}">div_scroll</c:if>" style="width:100%;">
						<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
						<table id="tbl_rghtPrps" <c:if test="${DIVS == 'M' }">style="width:1200px" </c:if> <c:if test="${DIVS == 'C' || DIVS == 'V' || DIVS == 'R'}">style="width:1000px" </c:if> cellspacing="0" cellpadding="0" border="1" class="grid" summary="저작물 저작권찾기 신청 선택저작물의 정보 입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<caption>저작권찾기 신청 선택목록</caption>
<c:if test="${DIVS == 'M'}">								
							<colgroup>
							<col width="2%">
							<col width="14%">
							<col width="14%">
							<col width="*">
							<col width="9%">
							<col width="9%">
							<col width="9%">
							<col width="8%">
							<col width="8%">
							<col width="8%">
							<col width="11%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" /></th>
									<th scope="col">곡명</th>
									<th scope="col">앨범명</th>
									<th scope="col">발매일자</th>
									<th scope="col">작사</th>
									<th scope="col">작곡</th>
									<th scope="col">편곡</th>
									<th scope="col">가창</th>
									<th scope="col">연주</th>
									<th scope="col">지휘</th>
									<th scope="col">음반제작사</th>
								</tr>
							</thead>
							<tbody>
								<tr id="dummyTr">
									<td colspan="11" class="ce">선택된 목록이 없습니다.</td>
								</tr>
							</tbody>
</c:if>
<c:if test="${DIVS == 'B'}">
							<colgroup>
							<col width="5%">
							<col width="25%">
							<col width="21%">
							<col width="10%">
							<col width="12%">
							<col width="12%">
							<col width="15%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" /></th>
									<th scope="col">작품명</th>
									<th scope="col">도서명</th>
									<th scope="col">발행일자</th>
									<th scope="col">작가명</th>
									<th scope="col">역자명</th>
									<th scope="col">출판사</th>
								</tr>
							</thead>
							<tbody>
								<tr id="dummyTr">
									<td colspan="7" class="ce">선택된 목록이 없습니다.</td>
								</tr>
							</tbody>
</c:if>			
<c:if test="${DIVS == 'C'}">								
							<colgroup>
								<col width="15" />
								<col width="160" />
								<col width="120" />
								<col width="100" />
								<col width="70" />
								<col width="70" />
								<col width="75" />
								<col width="100" />
								<col width="100" />
								<col width="130" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" onkeypress="javascript:checkBoxToggle('frm','chk');" /></th>
									<th scope="col">작품명</th>
									<th scope="col">작가명</th>
									<th scope="col">연출가</th>
									<th scope="col">방송회차</th>
									<th scope="col">방송일자</th>
									<th scope="col">방송매체</th>
									<th scope="col">방송사</th>
									<th scope="col">주요출연진</th>
									<th scope="col">제작사</th>
								</tr>
							</thead>
							<tbody>
								<tr id="dummyTr">
									<td colspan="10" class="ce">선택된 목록이 없습니다.</td>
								</tr>
							</tbody>
</c:if>		
<c:if test="${DIVS == 'I'}">
							<colgroup>
							<col width="5%">
							<col width="25%">
							<col width="21%">
							<col width="10%">
							<col width="12%">
							<col width="12%">
							<col width="15%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" /></th>
									<th scope="col">이미지명</th>
									<th scope="col">출판사</th>
									<th scope="col">집필진</th>
									<th scope="col">출판년도</th>
									<th scope="col">작가명</th>
									<th scope="col">분야</th>
								</tr>
							</thead>
							<tbody>
								<tr id="dummyTr">
									<td colspan="7" class="ce">선택된 목록이 없습니다.</td>
								</tr>
							</tbody>
</c:if>			
<c:if test="${DIVS == 'V'}">
							<colgroup>
								<col width="5%">
								<col width="15%">
								<col width="13%">
								<col width="15%">
								<col width="8%">
								<col width="*">
								<col width="7%">
								<col width="10%">
								<col width="10%">
								<col width="10%">
								</colgroup>
							<thead>
								<tr>
									<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" /></th>
									<th scope="col">영화명</th>
									<th scope="col">감독/연출</th>
									<th scope="col">주요출연진</th>
									<th scope="col">제작일자</th>
									<th scope="col">매체</th>
									<th scope="col">관람등급</th>
									<th scope="col">제작사</th>
									<th scope="col">배급사</th>
									<th scope="col">투자사</th>
								</tr>
							</thead>
							<tbody>
								<tr id="dummyTr">
									<td colspan="9" class="ce">선택된 목록이 없습니다.</td>
								</tr>
							</tbody>
</c:if>		
<c:if test="${DIVS == 'R'}">								
							<colgroup>
							<col width="5%">
							<col width="15%">
							<col width="15%">
							<col width="10%">
							<col width="*">
							<col width="10%">
							<col width="10%">
							<col width="12%">
							<col width="12%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col"><input type="checkbox" id="subChk" class="vmid" onclick="javascript:checkBoxToggle('frm','chk',this);" onkeypress="javascript:checkBoxToggle('frm','chk');" /></th>
									<th scope="col">저작물명</th>
									<th scope="col">프로그램명</th>
									<th scope="col">제작자</th>
									<th scope="col">프로그램 회차</th>
									<th scope="col">방송일자</th>
									<th scope="col">매체</th>
									<th scope="col">채널</th>
									<th scope="col">프로그램등급</th>
								</tr>
							</thead>
							<tbody>
								<tr id="dummyTr">
									<td colspan="8" class="ce">선택된 목록이 없습니다.</td>
								</tr>
							</tbody>
</c:if>						
						</table>
					</div>
					<!-- //테이블 영역입니다 -->
					
					</form>
					
				</div>
			</div>
		</div>
		<!-- CONTAINER end -->
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2010/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	<!-- //전체를 감싸는 DIVISION -->


<script type="text/javascript" src="/js/2010/calendarcode.js"></script>

<script>
Element.hide('ajaxBox');	// 로딩 이미지 박스 감추기
</script>

<SPAN id="helpMessage" style="position:absolute;top:0px;left:0px;height:0px;width:0px;background-color:#ffffff;display:none;z-index:5;border : 1 solid #aaaaaa;">
<!-- 
<iframe frameborder='no' style='border:0px; width:100% ;height:100%;filter:Alpha(Opacity=10)'></iframe>
 -->
</SPAN>

</body>
</html>

