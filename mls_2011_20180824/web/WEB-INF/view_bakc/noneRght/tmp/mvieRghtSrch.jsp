<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2010/prototype.js"> </script>

<title>영화 저작물 | 저작권찾기</title>
<script type="text/JavaScript">
<!--

window.name = "noneMvieRghtSrch";
	
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/noneRght/rghtSrch.do?method=subList&DIVS=V";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 영화저작물 상세 팝업오픈 : 부모창에서 오픈
function openMvieDetail( crId, meId, nrId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	param += '&ALBUM_ID='+meId;
	param += '&NR_ID='+nrId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=V'+param
	var name = '';
	var openInfo = 'target=mvieRghtSrch width=705, height=450, scrollbars=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 영화저작물 포스트 팝업오픈 : 부모창에서 오픈
function openPost(postUrl, title) {

	var param = 'postUrl='+postUrl;
	      param += '&title='+title;
	
	var url = postUrl;
	var name = '';
	var openInfo = 'target=mvieRghtSrch width=650, height=450, scrollbars=yes,toolbar=no,location=no,status=yes,menubar=no,resizable=yes';
	
	parent.frames.openDetail(url, name, openInfo);
}


// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=V'+param
	var name = '';
	var openInfo = 'target=mvieRghtSrch width=705, height=400';
	
	//alert(url);
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifMvieRghtSrch"); 
	
	var totalRow = ${mvieList.totalRow};
	totalRow = cfInsertComma(totalRow);
	parent.document.getElementById("totalRow").value = totalRow+"건";

 	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchDistributor" value="${srchParam.srchDistributor }"/>
	<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
	<input type="hidden" name="srchDirector" value="${srchParam.srchDirector }"/>
	<input type="hidden" name="srchViewGrade" value="${srchParam.srchViewGrade }"/>
	<input type="hidden" name="srchActor" value="${srchParam.srchActor }"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>

	<c:forEach items="${srchParam.srchNoneRole_arr }" var="srchNoneRole_arr">
		<c:set var="i" value="${i+1}"/>
		<input type="hidden" name="srchNoneRole_arr" value="${srchParam.srchNoneRole_arr[i-1] }" />
	</c:forEach>
	
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:350px;padding:0 0 0 0;">
			<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
			<table id="tab_scroll" style="width:1210px" cellspacing="0" cellpadding="0" border="1" class="grid" 
					summary="영화 저작권미확인 저작물의 영화명, 감독/연출, 주연, 제작년도, 매체, 관랍등급, 제작사, 배급사, 투자사, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="15" />
				<col width="210" />
				<col width="150" />
				<col width="190" />
				<col width="90" />
				<col width="60" />
				
				<col width="82" />
				<col width="81" />
				<col width="81" />
				<col width="81" />
				<col width="70" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col"><input type="checkbox" id="chk" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','iChk',this);" style="cursor:pointer;" title="전체선택"/></th>
						<th scope="col">영화명</th>
						<th scope="col">감독/연출</th>
						<th scope="col">주요출연진</th>
						<th scope="col">제작일자</th>
						<th scope="col">매체</th>
						
						<th scope="col">관람등급</th>
						<th scope="col">제작사</th>
						<th scope="col">배급사</th>
						<th scope="col">투자사</th>
						<th scope="col">저작권찾기</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${mvieList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="10">검색된 영화저작물이 없습니다.</td>
					</tr>
				</c:if>
				
				<c:if test="${mvieList.totalRow > 0}">
					<c:forEach items="${mvieList.resultList}" var="mvieList">
						<c:set var="NO" value="${mvieList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<td class="ce">
							<input type="checkbox" name="iChk" class="vmid" value="${mvieList.CR_ID }|${mvieList.MEDIA_CODE }|${mvieList.MEDIA_ID }" style="cursor:pointer;" title="선택"/>
							<!-- hidden value start -->
							<input type="hidden" name="crId" value="${mvieList.CR_ID}" />
							<input type="hidden" name="nrId" value="${mvieList.MEDIA_CODE}" />
							<input type="hidden" name="albumId" value="${mvieList.MEDIA_ID}" />
							<input type="hidden" name="mvieTitle" value='${mvieList.TITLE}' />
							<input type="hidden" name="director" value='${mvieList.DIRECTOR}' />
							<input type="hidden" name="leadingActor" value='${mvieList.LEADING_ACTOR}' />
							<input type="hidden" name="viewGradeValue" value="${mvieList.VIEW_GRADE_VALUE}" />
							<input type="hidden" name="produceYear" value="${mvieList.PRODUCE_YEAR}" />
							<input type="hidden" name="producer" value="${mvieList.PRODUCER}" />
							<input type="hidden" name="distributor" value="${mvieList.DISTRIBUTOR}" />
							<input type="hidden" name="investor" value="${mvieList.INVESTOR}" />
							<input type="hidden" name="mediaCodeValue" value="${mvieList.MEDIA_CODE_VALUE}" />
							<!-- 저작권미상 -->
							<input type="hidden" name="producer_yn" value="${mvieList.PRODUCER_YN}" />
							<input type="hidden" name="distributor_yn" value="${mvieList.DISTRIBUTOR_YN}" />
							<input type="hidden" name="investor_yn" value="${mvieList.INVESTOR_YN}" />
							<!-- hidden value end -->
						</td>
						<td>
							<a href="#" onclick="javascript:openMvieDetail('${mvieList.CR_ID }', '${mvieList.MEDIA_ID}', '${mvieList.MEDIA_CODE }');" style="cursor:pointer;">${mvieList.TITLE }</a>
							<c:if test="${mvieList.POST_URS != '' && mvieList.POST_URS != null}">
								<a href="#" onclick="javascript:openPost('${mvieList.POST_URS }','${mvieList.TITLE }');"><img src="/images/common/ic_preview.gif" class="vtop" alt="포스터 미리보기" /></a>
							</c:if>
						</td>
						<td>${mvieList.DIRECTOR }</td>
						<td>${mvieList.LEADING_ACTOR }</td>
						<td class="ce">${mvieList.PRODUCE_YEAR }</td>
						<td class="ce">${mvieList.MEDIA_CODE_VALUE }</td>
						<td class="ce">${mvieList.VIEW_GRADE_VALUE }</td>
						<td class="ce<c:if test="${mvieList.PRODUCER_YN == '0'}"> confirm</c:if>">${mvieList.PRODUCER }</td>
						<td class="ce<c:if test="${mvieList.DISTRIBUTOR_YN == '0'}"> confirm</c:if>">${mvieList.DISTRIBUTOR }</td>
						<td class="ce<c:if test="${mvieList.INVESTOR_YN == '0'}"> confirm</c:if>">${mvieList.INVESTOR }</td>
						<!-- 권리찾기 신청여부 -->
						<td class="ce">	
							<c:if test="${mvieList.PRPS_CNT >0}">
								<a href="#" onclick="javascript:openRghtPrps('${mvieList.CR_ID }','${mvieList.MEDIA_ID }','0');"><img src="/images/common/ic_newWin.gif" class="vtop" alt="저작권찾기신청내역" /></a>
							</c:if>
						</td>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2010.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${mvieList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
