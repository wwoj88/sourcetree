<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2010/prototype.js"> </script>
<title>도서 저작물 | 저작권찾기</title>
<script type="text/JavaScript">
<!--

window.name = "noneBookRghtSrch";
	
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/noneRght/rghtSrch.do?method=subList&DIVS=B";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 어문저작물 상세 팝업오픈 : 부모창에서 오픈
function openBookDetail( crId, nrId) {

	var param = '';
	
	param = '&CR_ID='+crId;
	param += '&NR_ID='+nrId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=B'+param
	var name = '';
	var openInfo = 'target=bookRghtSrch width=705, height=400';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=O'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch width=705, height=400';
	
	//alert(url);
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifBookRghtSrch"); 
	
	// 토탈건수 제어 : 삭제 (20101128)
	/*
	var totalRow = ${bookList.totalRow};
	totalRow = cfInsertComma(totalRow);
	parent.document.getElementById("totalRow").value = totalRow+"건";
	*/
	
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchTitle" value="${srchParam.srchTitle }"/>
	<input type="hidden" name="srchPublisher" value="${srchParam.srchPublisher }"/>
	<input type="hidden" name="srchBookTitle" value="${srchParam.srchBookTitle }"/>
	<input type="hidden" name="srchLicensorNm" value="${srchParam.srchLicensorNm }"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>
	<input type="hidden" name="srchLicensor" value="${srchParam.srchLicensor }"/>
	
	<c:forEach items="${srchParam.srchNoneRole_arr }" var="srchNoneRole_arr">
		<c:set var="i" value="${i+1}"/>
		<input type="hidden" name="srchNoneRole_arr" value="${srchParam.srchNoneRole_arr[i-1] }" />
	</c:forEach>
	
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:350px;padding:0 0 0 0;">
			<span class="round lt">&nbsp;</span><span class="round rt">&nbsp;</span><!-- 라운딩효과 -->
			<table id="tab_scroll" style="width:1120px" cellspacing="0" cellpadding="0" border="1" class="grid" 
					   summary="도서 저작권미확인 저작물의 작품명, 도서명, 발배일자, 저자, 역자, 출판사, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="15" />
				<col width="200" />
				<col width="150" />
				<col width="80" />
				<col width="80" />
				<col width="80" />
				<col width="130" />
				<col width="60" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col"><input type="checkbox" id="chk" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','iChk',this);" style="cursor:pointer;" title="전체선택"/></th>
						<th scope="col">작품명</th>
						<th scope="col">도서명</th>
						<th scope="col">발행일자</th>
						<th scope="col">작가명</th>
						<th scope="col">역자명</th>
						<th scope="col">출판사</th>
						<th scope="col">저작권찾기</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${bookList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="8">검색된 어문저작물이 없습니다.</td>
					</tr>
				</c:if>
				
				<c:if test="${bookList.totalRow > 0}">
					<c:forEach items="${bookList.resultList}" var="bookList">
						<c:set var="NO" value="${bookList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<td class="ce">
							<input type="checkbox" name="iChk" class="vmid" value="${bookList.CR_ID }|${bookList.BOOK_NR_ID }" style="cursor:pointer;" title="선택"/>
							<!-- hidden value start -->
							<input type="hidden" name="title" value='${bookList.TITLE}' />
							<input type="hidden" name="bookTitle" value='${bookList.BOOK_TITLE}' />
							<input type="hidden" name="subTitle" value='${bookList.SUBTITLE}' />
							<input type="hidden" name="issuedDate" value="${bookList.FIRST_EDITION_YEAR}" />
							<input type="hidden" name="writer" value='${bookList.LICENSOR_NAME_KOR}' />
							<input type="hidden" name="translator" value='${bookList.TRANSLATOR}' />
							<input type="hidden" name="publisher" value='${bookList.PUBLISHER}' />
							<input type="hidden" name="crId" value="${bookList.CR_ID}" />
							<input type="hidden" name="nrId" value="${bookList.BOOK_NR_ID}" />
							<input type="hidden" name="publishType" value="${bookList.PUBLISH_TYPE}" />
							<input type="hidden" name="retrieveType" value="${bookList.RETRIEVE_TYPE}" />
							<input type="hidden" name="icnNumb" value="${bookList.ICN_NUMB}" />
							<!-- 저작권미상 -->
							<input type="hidden" name="writer_yn" value="${bookList.WRITER_YN}" />
							<input type="hidden" name="translator_yn" value="${bookList.TRANSLATOR_YN}" />
							<!-- hidden value end -->
						</td>
						<td> 
							<a href="javascript:openBookDetail('${bookList.CR_ID }','${bookList.BOOK_NR_ID }');" style="cursor:pointer;">${bookList.TITLE }</a>
						</td>
						<td>${bookList.BOOK_TITLE }</td>
						<td class="ce">${bookList.FIRST_EDITION_YEAR }</td>
						<td class="ce<c:if test="${bookList.WRITER_YN == '0'}"> confirm</c:if>">${bookList.LICENSOR_NAME_KOR }</td>
						<td class="ce<c:if test="${bookList.TRANSLATOR_YN == '0'}"> confirm</c:if>">${bookList.TRANSLATOR }</td>
						<td class="ce">${bookList.PUBLISHER }</td>
						<!-- 권리찾기 신청여부 -->
						<td class="ce">	
							<c:if test="${bookList.PRPS_CNT >0}">
								<a href="#" onclick="javascript:openRghtPrps('${bookList.CR_ID }','${bookList.BOOK_NR_ID }','0');"><img src="/images/common/ic_newWin.gif" class="vtop" alt="저작권찾기신청내역" /></a>
							</c:if>
						</td>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2010.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${bookList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
