<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2010/prototype.js"> </script>
<title>이미지 저작물 | 저작권찾기</title>
<script type="text/JavaScript">
<!--

window.name = "noneImageRghtSrch";
	
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/noneRght/rghtSrch.do?method=subList&DIVS=I";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 이미지저작물 상세 팝업오픈 : 부모창에서 오픈
function openImageDetail( workFileNm, workNm ) {

	var param = '';
	
	param = '&WORK_FILE_NAME='+workFileNm;
	param += '&WORK_NAME='+workNm;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=I'+param
	var name = '';
	var openInfo = 'target=imageRghtSrch width=320, height=310';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=I'+param
	var name = '';
	var openInfo = 'target=muscRghtSrch width=705, height=400';
	
	parent.frames.openDetail(url, name, openInfo);
}


// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifImageRghtSrch"); 
	
	var totalRow = ${imageList.totalRow};
	totalRow = cfInsertComma(totalRow);
	parent.document.getElementById("totalRow").value = totalRow+"건";

	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
	
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchWorkName" value="${srchParam.srchWorkName }"/>
	<input type="hidden" name="srchLishComp" value="${srchParam.srchLishComp }"/>
	<input type="hidden" name="srchCoptHodr" value="${srchParam.srchCoptHodr }"/>
	<input type="hidden" name="srchStartDate" value="${srchParam.srchStartDate }"/>
	<input type="hidden" name="srchEndDate" value="${srchParam.srchEndDate }"/>

	<c:forEach items="${srchParam.srchNoneRole_arr }" var="srchNoneRole_arr">
		<c:set var="i" value="${i+1}"/>
		<input type="hidden" name="srchNoneRole_arr" value="${srchParam.srchNoneRole_arr[i-1] }" />
	</c:forEach>
	
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:350px;padding:0 0 0 0;">
			<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
			<table id="tab_scroll" style="width:745px" cellspacing="0" cellpadding="0" border="1" class="grid"
					   summary="이미지 저작권미확인 저작물의 이미지명, 출판사, 집필진, 출판년도, 작가명, 분야, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="15" />
				<col width="200" />
				<col width="90" />
				<col width="60" />
				<col width="50" />
				<col width="60" />
				<col width="92" />
				<col width="70" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col"><input type="checkbox" id="chk" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','iChk',this);" style="cursor:pointer;" title="전체선택"/></th>
						<th scope="col">이미지명</th>
						<th scope="col">출판사</th>
						<th scope="col">집필진</th>
						<th scope="col">출판년도</th>
						<th scope="col">작가명</th>
						<th scope="col">분야</th>
						<th scope="col">저작권찾기</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${imageList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="8">검색된 이미지저작물이 없습니다.</td>
					</tr>
				</c:if>
				
				<c:if test="${imageList.totalRow > 0}">
					<c:forEach items="${imageList.resultList}" var="imageList">
						<c:set var="NO" value="${imageList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<td class="ce">
							<input type="checkbox" name="iChk" class="vmid" value="${imageList.IMGE_SEQN }" style="cursor:pointer;" title="선택"/>
							<!-- hidden value start -->
							<input type="hidden" name="imageSeqn" value='${imageList.IMGE_SEQN}' />
							<input type="hidden" name="crId" value="${imageList.CR_ID}" />
							<input type="hidden" name="workName" value='${imageList.WORK_NAME}' />
							<input type="hidden" name="coptHodr" value='${imageList.COPT_HODR}' />
							<input type="hidden" name="lishComp" value="${imageList.LISH_COMP}" />
							<input type="hidden" name="wterDivs" value='${imageList.WTER_DIVS}' />
							<input type="hidden" name="usexYear" value='${imageList.USEX_YEAR}' />
							<input type="hidden" name="imageDivs" value='${imageList.IMAGE_DIVS}' />
							<input type="hidden" name="icnNumb" value="${imageList.ICN_NUMB}" />
							<input type="hidden" name="workFileName" value='${imageList.WORK_FILE_NAME}' />
							<!-- 저작권미상 -->
							<input type="hidden" name="copt_hodr_yn" value="${imageList.COPT_HODR_YN}" />
							<!-- hidden value end -->
						</td>
						<td>${imageList.WORK_NAME }
							<c:if test="${imageList.WORK_FILE_NAME !='' || imageList.WORK_FILE_NAME != null }">
							<a href="#" onclick="javascript:openImageDetail('${imageList.WORK_FILE_NAME }','${imageList.WORK_NAME }');"><img src="/images/common/ic_preview.gif" class="vtop" alt="이미지 미리보기" /></a>
							</c:if>
						</td>
						<td>${imageList.LISH_COMP }</td>
						<td class="ce">${imageList.WTER_DIVS }</td>
						<td class="ce">${imageList.USEX_YEAR }</td>
						<td class="ce<c:if test="${imageList.COPT_HODR_YN == '0'}"> confirm</c:if>">${imageList.COPT_HODR }</td>
						<td class="ce">${imageList.IMAGE_DIVS }</td>
						<!-- 권리찾기 신청여부 -->
						<td class="ce">	
							<c:if test="${imageList.PRPS_CNT >0}">
								<a href="#" onclick="javascript:openRghtPrps('${imageList.CR_ID }','0','0');"><img src="/images/common/ic_newWin.gif" class="vtop" alt="저작권찾기신청내역" /></a>
							</c:if>
						</td>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2010.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${imageList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
