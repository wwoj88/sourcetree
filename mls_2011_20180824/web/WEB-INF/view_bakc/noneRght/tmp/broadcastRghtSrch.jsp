<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<script type="text/javascript" src="/js/2010/common.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>	
<script type="text/javascript" src="/js/2010/prototype.js"> </script>

<title>방송 저작물 | 저작권찾기</title>
<script type="text/JavaScript">
<!--

window.name = "noneBroadcastRghtSrch";
	
function goPage(pageNo){
	var frm = document.ifFrm;

	// 로딩 이미지 박스 보이게..
	new Ajax.Request('/test', {   
		onLoading: function() {     
			parent.frames.showAjaxBox();
		},
		onSuccess: function(req) {     
			// Do something with req.responseXML/Text .. ...   
		},
		onComplete: function() {     
			parent.frames.showAjaxBox();
		} 
	});
	
	frm.method = "post";
	frm.action = "/noneRght/rghtSrch.do?method=subList&DIVS=R";
	frm.page_no.value = pageNo;
	frm.submit();
}

function fn_totalCNT(){
	var frm = document.ifFrm;
	if(frm.totalRow.value != null && frm.totalRow.value != undefined){
		if(window.parent.document.getElementById("spn_totalRow") != null && window.parent.document.getElementById("spn_totalRow") != undefined){
			window.parent.document.getElementById("spn_totalRow").innerHTML = frm.totalRow.value;
		}
	}
}

// 방송대본저작물 상세 팝업오픈 : 부모창에서 오픈
function openBroadcastDetail( crId ) {

	var param = '';
	
	param = '&CR_ID='+crId;
	
	var url = '/rghtPrps/rghtSrch.do?method=detail&DIVS=R'+param
	var name = '';
	var openInfo = 'target=noneBroadcastRghtSrch width=705, height=500';
	
	parent.frames.openDetail(url, name, openInfo);
}

// 권리찾기 history오픈 : 부모창에서 오픈
function openRghtPrps (crId, nrId, meId) {

	var param = '';
	
	param = '&PRPS_IDNT='+crId;
	param += '&PRPS_IDNT_NR='+nrId;
	param += '&PRPS_IDNT_ME='+meId;
	
	var url = '/rghtPrps/rghtSrch.do?method=rghtPrpsHistList&DIVS=R'+param
	var name = '';
	var openInfo = 'target=noneBroadcastRghtSrch width=705, height=400';
	
	parent.frames.openDetail(url, name, openInfo);
}

// table name 사이즈에 대한 div targetName리사이즈
function resizeDiv(name, targetName) {

   var the_height = document.getElementById(name).offsetHeight;
   
   document.getElementById(targetName).style.height = the_height+13;
  
}

 function initParameter(){
 	
 	resizeDiv("tab_scroll", "div_scroll");
 	 	
 	// 부모창 프레임 리사이징 호출
	parent.frames.resizeIFrame("ifBroadcastRghtSrch"); 
	
	var totalRow = ${broadcastList.totalRow};
	totalRow = cfInsertComma(totalRow);
	parent.document.getElementById("totalRow").value = totalRow+"건";
	
	// 로딩 이미지 박스 hidden..
	parent.frames.hideAjaxBox();
}

if(window.attachEvent)
	window.attachEvent("onload", initParameter);
else if(window.addEventListener) 
   	window.addEventListener("load", initParameter, false); 
else 
   	window.onload = initParameter; 
 
//-->
</script>
<link type="text/css" rel="stylesheet" href="/css/2010/style.css" />
<style type="text/css">
<!--
body {
	overflow-x: auto;
	overflow-y: hidden;
	margin: 0 0 0 0 ;
}

.div_scroll {
	overflow-x: auto;
	overflow-y: hidden;
	margin-left: 0px;
}
-->
</style>
</head>
<body>
<form name="ifFrm" action="">
	<input type="hidden" name="page_no"/>
	<input type="hidden" name="DIVS" value="${srchParam.DIVS }"/>
	<input type="hidden" name="srchProgName" value="${srchParam.srchProgName }"/>
	<input type="hidden" name="srchMaker" value="${srchParam.srchMaker }"/>
	
	<c:forEach items="${srchParam.srchNoneRole_arr }" var="srchNoneRole_arr">
		<c:set var="i" value="${i+1}"/>
		<input type="hidden" name="srchNoneRole_arr" value="${srchParam.srchNoneRole_arr[i-1] }" />
	</c:forEach>
	
		<!-- 테이블 영역입니다 --> 
		<div id="div_scroll" class="tabelRound div_scroll" style="width:100%;height:350px;padding:0 0 0 0;">
			<span class="round lt"></span><span class="round rt"></span><!-- 라운딩효과 -->
			<table id="tab_scroll" style="width:1020px" cellspacing="0" cellpadding="0" border="1" class="grid" 
					   summary="방송 저작권미확인 저작물의 저작물명, 프로그램명, 제작자, 프로그램 회차, 방송일자, 매체, 채널, 프로그램등급, 저작권찾기 정보입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
				<colgroup>
				<col width="15" />
				<col width="140" />
				<col width="140" />
				<col width="90" />
				<col width="95" />
				<col width="70" />
				<col width="70" />
				<col width="70" />
				<col width="90" />
				<col width="70" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col"><input type="checkbox" id="chk" class="vmid" onclick="javascript:checkBoxToggle('ifFrm','iChk',this);" style="cursor:pointer;" title="전체선택" /></th>
						<th scope="col">저작물명</th>
						<th scope="col">프로그램명</th>
						<th scope="col">제작자</th>
						<th scope="col">프로그램 회차</th>
						<th scope="col">방송일자</th>
						<th scope="col">매체</th>
						<th scope="col">채널</th>
						<th scope="col">프로그램등급</th>
						<th scope="col">저작권찾기</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${broadcastList.totalRow == 0}">
					<tr>
						<td class="ce" colspan="10">검색된 방송저작물이 없습니다.</td>
					</tr>
				</c:if>
				
				<c:if test="${broadcastList.totalRow > 0}">
					<c:forEach items="${broadcastList.resultList}" var="broadcastList">
						<c:set var="NO" value="${broadcastList.totalRow}"/>
						<c:set var="i" value="${i+1}"/>
					<tr>
						<td class="ce">
							<input type="checkbox" name="iChk" class="vmid" value="${broadcastList.CR_ID }" style="cursor:pointer;" title="선택"/>
							<!-- hidden value start -->
							<input type="hidden" name="title" value="${broadcastList.TITLE}" />
							<input type="hidden" name="progName" value="${broadcastList.PROG_NAME}" />
							<input type="hidden" name="progOrdSeq" value="${broadcastList.PROG_ORDSEQ}" /> <!-- 프로그램회차 -->
							<input type="hidden" name="broadDate" value="${broadcastList.BROAD_DATE}" />
							<input type="hidden" name="mediCode" value="${broadcastList.MEDI_CODE}" />
							<input type="hidden" name="chnlCode" value="${broadcastList.CHNL_CODE}" />
							<input type="hidden" name="mediCodeName" value="${broadcastList.MEDI_CODE_NAME}" />
							<input type="hidden" name="chnlCodeName" value="${broadcastList.CHNL_CODE_NAME}" />
							<input type="hidden" name="progGrad" value="${broadcastList.PROG_GRAD}" />
							<input type="hidden" name="crId" value="${broadcastList.CR_ID}" />
							<input type="hidden" name="maker" value="${broadcastList.MAKER}" />
							<input type="hidden" name="icnNumb" />
							<!-- 저작권미상 -->
							<input type="hidden" name="maker_yn" value="${broadcastList.MAKER_YN}" />
							<!-- hidden value end -->
						</td>
						<td style="cursor:pointer;">
							<a href="#" onclick="javascript:openBroadcastDetail('${broadcastList.CR_ID }');">${broadcastList.TITLE }</a>
						</td>
						<td>${broadcastList.PROG_NAME }</td>
						<td class="ce<c:if test="${broadcastList.MAKER_YN == '0'}"> confirm</c:if>">${broadcastList.MAKER }</td>
						<td class="ce">${broadcastList.PROG_ORDSEQ }</td>
						<td class="ce">${broadcastList.BROAD_DATE }</td>
						<td class="ce">${broadcastList.MEDI_CODE_NAME }</td>
						<td class="ce">${broadcastList.CHNL_CODE_NAME }</td>
						<td class="ce">${broadcastList.PROG_GRAD }</td>
						<!-- 권리찾기 신청여부 -->
						<td class="ce">	
							<c:if test="${broadcastList.PRPS_CNT >0}">
								<a href="#" onclick="javascript:openRghtPrps('${broadcastList.CR_ID }','0','0');"><img src="/images/common/ic_newWin.gif" class="vtop" alt="저작권찾기신청내역" /></a>
							</c:if>
						</td>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>
			</table>
		</div>
		<!-- //테이블 영역입니다 -->
		
		<div class="pagination">
	 		<%-- 페이징 리스트 --%>
			  <jsp:include page="../common/PageList_2010.jsp" flush="true">
				  <jsp:param name="totalItemCount" value="${broadcastList.totalRow}" />
					<jsp:param name="nowPage"        value="${param.page_no}" />
					<jsp:param name="functionName"   value="goPage" />
					<jsp:param name="listScale"      value="" />
					<jsp:param name="pageScale"      value="" />
					<jsp:param name="flag"           value="M01_FRONT" />
					<jsp:param name="extend"         value="no" />
				</jsp:include>
		</div>
		<!--paging end-->

		<!--contents end-->
</form>
</body>
</html>
