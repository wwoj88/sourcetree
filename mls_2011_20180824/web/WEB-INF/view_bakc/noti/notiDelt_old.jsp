<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.board.model.Board"%>
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ page import="java.util.List"%>
<%
	String bordSeqn = request.getParameter("bordSeqn");
	String menuSeqn = request.getParameter("menuSeqn");
	String threaded = request.getParameter("threaded");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");

	Board boardDTO = (Board) request.getAttribute("board");
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작물 내권리찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
<script type="text/JavaScript">
<!--
  function fn_notiList(){
		var frm = document.form1;
		frm.menuSeqn.value = '4';
		frm.action = "/board/board.do";
		frm.submit();
	}

	function fn_fileDownLoad(filePath, fileName, realFileName) {
		var frm = document.form1;
		frm.filePath.value     = filePath;
		frm.fileName.value     = fileName;
		frm.realFileName.value = realFileName;

		frm.action = "/board/board.do?method=fileDownLoad";
		frm.submit();
  }
//-->
</script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp" flush="true">
		<jsp:param name="mNum" value="4" />
	</jsp:include>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
				GetFlash('/images/swf/subVisual.swf','725','122');
  		</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="ftaqTlt">공지사항</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<img src="/images/common/home_ico.gif" alt="Home" />Home
					<li>공지사항</li>
					<li class="on">공지사항 상세조회</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<h2>공지사항 상세조회</h2>
			<table width="710" class="board ViewTop">
				<form name="form1" method="post">
					<input type="hidden" name="bordSeqn" value="<%=bordSeqn%>">
					<input type="hidden" name="menuSeqn" value="<%=menuSeqn%>">
					<input type="hidden" name="threaded" value="<%=threaded%>">
					<input type="hidden" name="srchDivs" value="<%=srchDivs%>">
					<input type="hidden" name="srchText" value="<%=srchText%>">
					<input type="hidden" name="page_no" value="<%=page_no%>">
					<input type="hidden" name="filePath">
					<input type="hidden" name="fileName">
					<input type="hidden" name="realFileName">
				<colgroup>
				<col width="80" />
				<col width="" />
				</colgroup>
				<caption summary="FAQ내용을 상세조회 합니다.">공지사항내용 상세화면</caption>
				<thead >
					<tr>
						<th>제목</th>
						<td class="tlt"><%=boardDTO.getTite()%></td>
					</tr>
				</thead>
				<tbody class="ViewBody">
					<tr>
						<th>이름</th>
						<td><%=boardDTO.getRgstIdnt()%></td>
					</tr>
					<tr>
						<th>이메일</th>
						<td><%=boardDTO.getMail() == null ? "" : boardDTO.getMail()%></td>
					</tr>
					<tr>
						<th>내용</th>
						<td class="tdContents"><%=CommonUtil.replaceBr(boardDTO.getBordDesc(),true)%></td>
					</tr>

<%
          	List fileList = (List) boardDTO.getFileList();
          	int listSize = fileList.size();

          	if (listSize > 0) {
%>
					<tr>
						<th>첨부파일</th>
						<td><ul>
<%

          	  for(int i=0; i<listSize; i++) {
          	    Board fileDTO = (Board) fileList.get(i);
%>
	            <li><a href="javascript:fn_fileDownLoad('<%=fileDTO.getFilePath()%>','<%=fileDTO.getFileName()%>','<%=fileDTO.getRealFileName()%>')"><%=fileDTO.getFileName()%></a></li>
<%
	            }
%>
						</ul></td>
					</tr>
<%
	          }
%>
				</tbody>
			</form>
			</table>
			<!--buttonArea start-->
			<div id="buttonArea">
				<div class="floatL"><a href="javascript:fn_notiList();"><img src="/images/button/list_btn.gif" alt="목록보기" /></a></div>
			</div>
			<!--buttonArea end-->
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
