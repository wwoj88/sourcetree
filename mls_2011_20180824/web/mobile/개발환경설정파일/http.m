*DOMAIN
webtob1

*NODE
msKim	WEBTOBDIR="C:/TmaxSoft/JEUS6.0/webserver", 
		SHMKEY = 54000,
		DOCROOT="G:/Server/mls/mls_2011/web",
		PORT = "80", 
		HTH = 1,
		NODENAME = "$(NODENAME)",
		ServiceOrder="EXT,URI",
		IndexName= "/index.jsp",
		LOGGING = "log1",
		ERRORLOG = "log2",
		TIMEOUT=1800

*VHOST

mls2011 	DOCROOT="G:/Server/mls/mls_2011/web",
	HostName = "new.right4me.or.kr",
	HostAlias = "192.168.0.22",
	NodeName = msKim,
	LOGGING  = "mls2011_log1",
	ERRORLOG = "mls2011_log2",
	IndexName= "/index.jsp",
	PORT="80"

mls_mobile		DOCROOT="G:/Server/mls/mls_2011/web",
	HostName = "m.right4me.or.kr",
	HostAlias = "192.168.0.22",
	NodeName = msKim,
	LOGGING  = "mls_mobile_log1",
	ERRORLOG = "mls_mobile_log2",
	IndexName= "/mobile/index.jsp",
	PORT="80"

mls2010 	DOCROOT="G:/Server/mls/mls_new/web",
	HostName = "org.right4me.or.kr",
	NodeName = msKim,
	LOGGING  = "mls2010_log1",
	ERRORLOG = "mls2010_log2",
	IndexName= "/index.jsp",
	PORT="8080"



*SVRGROUP
htmlg		NODENAME = "msKim", SVRTYPE = HTML
cgig		NODENAME = "msKim", SVRTYPE = CGI
ssig		NODENAME = "msKim", SVRTYPE = SSI
jsvg_mls2010	NODENAME = "msKim", SVRTYPE = JSV, VhostName = mls2010
jsvg_mls2011	NODENAME = "msKim", SVRTYPE = JSV, VhostName = mls2011
jsvg_mls_mobile	NODENAME = "msKim", SVRTYPE = JSV, VhostName = mls_mobile


*SERVER
html		SVGNAME = htmlg, MinProc = 2, MaxProc = 10
cgi			SVGNAME = cgig, MinProc = 4, MaxProc = 10 
ssi			SVGNAME = ssig, MinProc = 2, MaxProc = 10 
grp_mls2010		SVGNAME  = jsvg_mls2010,    MinProc = 30, MaxProc = 30
grp_mls2011		SVGNAME  = jsvg_mls2011,    MinProc = 30, MaxProc = 30
grp_mls_mobile		SVGNAME  = jsvg_mls_mobile,    MinProc = 30, MaxProc = 30



*URI
uri1		Uri = "/cgi-bin/",   Svrtype = CGI
uri2 		Uri = "/", Svrtype = JSV ,SvrName = "grp_mls2010", VhostName = mls2010
uri3 		Uri = "/", Svrtype = JSV ,SvrName = "grp_mls2011", VhostName = mls2011
uri4 		Uri = "/", Svrtype = JSV ,SvrName = "grp_mls_mobile", VhostName = mls_mobile, Redirect="/mobile/index.jsp"

*ALIAS
alias1  	URI = "/cgi-bin/", RealPath = "C:/TmaxSoft/JEUS6.0/webserver/cgi-bin/"

*LOGGING
log1		Format = "DEFAULT", FileName = "C:/TmaxSoft/JEUS6.0/webserver/log/access.log", Option = "sync"
log2		Format = "ERROR",	FileName = "C:/TmaxSoft/JEUS6.0/webserver/log/error.log", 	Option = "sync"

mls2010_log1	Format = "DEFAULT", FileName = "C:/TmaxSoft/JEUS6.0/webserver/log/mls2010_access.log", Option = "sync"
mls2010_log2	Format = "ERROR",   FileName = "C:/TmaxSoft/JEUS6.0/webserver/log/mls2010_error.log",  Option = "sync"

mls2011_log1	Format = "DEFAULT", FileName = "C:/TmaxSoft/JEUS6.0/webserver/log/mls2011_access.log", Option = "sync"
mls2011_log2	Format = "ERROR",   FileName = "C:/TmaxSoft/JEUS6.0/webserver/log/mls2011_error.log",  Option = "sync"

mls_mobile_log1	Format = "DEFAULT", FileName = "C:/TmaxSoft/JEUS6.0/webserver/log/mls_mobil_access.log", Option = "sync"
mls_mobile_log2	Format = "ERROR",   FileName = "C:/TmaxSoft/JEUS6.0/webserver/log/mls_mobil_error.log",  Option = "sync"



*EXT
jsp			MimeType = "application/jsp", SvrType = JSV
htm			MimeType = "text/html", SvrType = HTML
html			MimeType = "text/html", SvrType = HTML
bin  		MimeType = "application/octet-stream", SvrType = HTML
zip  		MimeType = "application/octet-stream", SvrType = HTML
cab  		MimeType = "application/octet-stream", SvrType = HTML
exe  		MimeType = "application/octet-stream", SvrType = HTML
swf  		MimeType = "application/octet-stream", SvrType = HTML
wmv  		MimeType = "application/octet-stream", SvrType = HTML
js   		MimeType = "application/x-javascript", SvrType = HTML
css  		MimeType = "text/css", SvrType = HTML
#jpg 		MimeType = "application/octet-stream", SvrType = HTML
jpeg 		MimeType = "image/jpeg", SvrType = HTML
#jpg 		MimeType = "image/jpeg", SvrType = HTML
jpe  		MimeType = "image/jpeg", SvrType = HTML
jfif 		MimeType = "image/jpeg", SvrType = HTML
pjpeg		MimeType = "image/jpeg", SvrType = HTML
pjp  		MimeType = "image/jpeg", SvrType = HTML
bmp  		MimeType = "image/bmp", SvrType = HTML
htc MimeType = "text/x-component", SvrType = HTML
#txt  		MimeType = "text/plain", SvrType = HTML
mpeg 		MimeType = "video/mpeg", SvrType = HTML
mpg  		MimeType = "video/mpeg", SvrType = HTML
mpe  		MimeType = "video/mpeg", SvrType = HTML
mpv  		MimeType = "video/mpeg", SvrType = HTML
vbs  		MimeType = "video/mpeg", SvrType = HTML
mpegv		MimeType = "video/mpeg", SvrType = HTML
avi  		MimeType = "video/msvideo", SvrType = HTML
shtml		MimeType = "magnus-internal/parsed-html", SvrType = HTML
cgi  		MimeType = "magnus-internal/cgi", SvrType = CGI
bat  		MimeType = "magnus-internal/cgi", SvrType = HTML
gif  		MimeType = "image/gif", SvrType = HTML
do   		MimeType = "text/html", SvrType = JSV
hwp  		MimeType = "application/x-hwp" , SvrType = HTML
doc  		MimeType = "application/msword", SvrType = HTML
xla  		MimeType = "application/vnd.ms-excel" , SvrType = HTML
xlc  		MimeType = "application/vnd.ms-excel" , SvrType = HTML
xlm  		MimeType = "application/vnd.ms-excel" , SvrType = HTML
xls  		MimeType = "application/vnd.ms-excel" , SvrType = HTML
xlt  		MimeType = "application/vnd.ms-excel" , SvrType = HTML
xlw  		MimeType = "application/vnd.ms-excel" , SvrType = HTML
ppt  		MimeType = "application/vnd.ms-powerpoint" , SvrType = HTML
gul  		MimeType = "application/octet-stream",  SvrType = HTML
alz  		MimeType = "application/octet-stream", SvrType = HTML
mp3  		MimeType = "video/msvideo", SvrType = HTML
mid  		MimeType = "video/msvideo", Svrtype = HTML
wav  		MimeType = "video/msvideo", Svrtype = HTML
wma  		MimeType = "video/msvideo", Svrtype = HTML
asf  		MimeType = "video/msvideo", SvrType = HTML
asx  		MimeType = "video/msvideo", SvrType = HTML