/****************************************************************************************
 * rxRX.cab:      rxRX 패치 버전(3.0.0.10 이상 버전)
 * rxRX_full.cab: rxRX Full 버전(3.0.0.10 이하 버전)
 ****************************************************************************************/

var rxPath			= "/cdoc/rxpp/vista/";
var _Caption		= "(주)캡소프트";
var _ShowMsg	= false ;		// 설치 정보 보여주기(08.07.30)
var ErrMsg			= "";
var Lang				= "";
var osInfo			= window.navigator.userAgent;
var vHTML			= "";
var verRX				= "3,0,0,10";           	//rxRX 버전(고정)
var verRXUpdator	= "1,0,0,3";           		//RXUpdator 버전

var rxRX					= rxPath + "cab/rxpptype2";      	//rxRX 업그레이드 파일
var rxRXType2		= rxPath + "cab/rxpptype2";    		//rxRX 최초 설치 파일
var rxRXType2_9x	= rxPath + "cab/rxpptype2_9x";    	//rxRX 최초 설치 파일 rxpptype2_9x.cab
var cab					= ""; 							//최종 Cab파일
var RXError				= false;
var ViewErrorMsg	= false;                  //설치 오류 메세지 보여주기 유무
var InstallError			= 0;                  //설치 오류
var isLoadRX			= 1;                  	//rxRX 설치 여부
var timer					= null;
var vRobjStr = "";

vHTML += '<table width="600" height="500" border="0" cellpadding="0" cellspacing="0" align="center">';
vHTML += '    <tr align="center">';
vHTML += '        <td>';
vHTML += '            <table width="598" border="0" cellpadding="0" cellspacing="0">';
vHTML += '              <tr>';
vHTML += '                <td><img src="/cdoc/rxpp/vista/images/system_rxpp.gif" width="100%" height="102" /></td>';
vHTML += '              </tr>';
vHTML += '              <tr>';
vHTML += '                <td valign="top" background="/zz/re/vista/images/system_bg.gif">';
vHTML += '                <table width="100%" border="0">';
vHTML += '                  <tr>';
vHTML += '                    <td style="padding-bottom:30px;"  align="center"><img src="/cdoc/rxpp/vista/images/sys_load.gif" width="540" height="21" /></td>';
vHTML += '                  </tr>';
vHTML += '                  <tr>';
vHTML += '                    <td class="txt"  align="left">&nbsp;&nbsp;<img src="/cdoc/rxpp/vista/images/sys_icon.gif" width="12" height="3" align="absmiddle" />ActiveX 컨트롤 설치여부를 묻는 보안경고창이 나타나면 반드시 <font color="#FF2400">&quot;예&quot;</font>를 선택하여 주시기 바랍니다. </td>';
vHTML += '                  </tr>';
vHTML += '                  <tr>';
vHTML += '                    <td class="txt"  align="left">&nbsp;&nbsp;<img src="/cdoc/rxpp/vista/images/sys_icon.gif" width="12" height="3" align="absmiddle" />고객님의 통신환경에 따라 약 10초에서 수분이 걸릴 수 있습니다. </td>';
vHTML += '                  </tr>';
vHTML += '                  <tr>';
vHTML += '                    <td class="txt"  align="left">&nbsp;&nbsp;<img src="/cdoc/rxpp/vista/images/sys_icon.gif" width="12" height="3" align="absmiddle" />비스타 운영체제의 <font color="#FF2400">&quot;표준사용자&quot;</font>이거나 설치에 문제가 발생한 경우 아래의 수동 설치파일을 다운로드<br />';
vHTML += '                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;하여 설치하시기 바랍니다. <br /></td>';
vHTML += '                  </tr>';
vHTML += '                  <tr>';
vHTML += '                    <td class="txt"  align="left">&nbsp;&nbsp;<img src="/cdoc/rxpp/vista/images/sys_icon.gif" width="12" height="3" align="absmiddle" />설치 후 화면 전환이 되지 않는 경우 <font color="#FF2400">&quot;이전 페이지로 이동&quot;</font>을 선택하여 주시기 바랍니다.</td>';
vHTML += '                  </tr>';
vHTML += '                  <tr>';
vHTML += '                    <td class="txt"  align="left">&nbsp;&nbsp;<img src="/cdoc/rxpp/vista/images/sys_icon.gif" width="12" height="3" align="absmiddle" />자동 또는 수동 설치에 실패한 경우 <font color="#FF2400">&quot;긴급 설치 파일&quot;</font>을 다운로드하여 설치하시기 바랍니다.</td>';
vHTML += '                  </tr>';
vHTML += '                  <tr align="center">';
vHTML += '                    <td><a href="/zz/re/vista/cab/rxpptype1_Korean.exe"><img src="/cdoc/rxpp/vista/images/new_sys_btn01.gif" width="130" height="27" border="0"/></a>';
vHTML += '                    	<a href="/zz/re/vista/cab/rxpp_install.exe"><img src="/cdoc/rxpp/vista/images/new_sys_btn02.gif" width="130" height="27" border="0"/></a>';
vHTML += '							<a href="javascript:closeWin(\"1\");"><img src="/cdoc/rxpp/vista/images/new_sys_btn03.gif" width="130" height="27" border="0"/></a>';
vHTML += '							<a href="javascript:closeWin(\"0\");"><img src="/cdoc/rxpp/vista/images/new_sys_btn04.gif" width="130" height="27" border="0"/></a></td>';
vHTML += '                  </tr>';
vHTML += '                </table></td>';
vHTML += '              </tr>';
vHTML += '              <tr>';
vHTML += '                <td><img src="/cdoc/rxpp/vista/images/system_bottom.gif" width="100%" height="73" /></td>';
vHTML += '              </tr>';
vHTML += '              <!--tr>';
vHTML += '                <td><img src="/cdoc/rxpp/vista/images/new_system_bottom.gif" width="100%" height="73" /></td>';
vHTML += '              </tr-->';
vHTML += '            </table>';
vHTML += '        </td>';
vHTML += '    </tr>';
vHTML += '</table>';

vRobjStr += '<OBJECT id="Viewer1"';
vRobjStr += '	classid="CLSID:7A6395E4-00CD-4B5A-A5AF-F3958618D9CE"';
vRobjStr += '	Style="Display:none"';
vRobjStr += '	width="100%" height="100%">';
vRobjStr += '<PARAM NAME="cWidth" VALUE="850"/>';
vRobjStr += '<PARAM NAME="cHeight" VALUE="600"/>';
vRobjStr += '<PARAM NAME="DOMAIN" ';
vRobjStr += '   VALUE="q8ekIbbOngtljiZrWX7JqsNs+HmuRG7WWJZxeIAo7XBr/nCNTGsaRN65AGzVymG4yToEJ30AvOl9+Y46YHQzXDUBw6NzE78j74t86ipxL+8=">';
vRobjStr += '</OBJECT>';

/****************************************************************************************
 * ReportExpress 설치 판단 및 뷰어 실행
 ****************************************************************************************/

var update     	= "";
var obj = null;
try {
	obj = new ActiveXObject("rxppUpdator.Updator");
	var pos = window.location.href;
	var re = /(\w+):\/\/([^/:]+)(:\d*)?/g ;
	var hostname = pos.match(re);

	obj.RemoteHost= hostname + rxPath + "module/";
	obj.SetupFileName= "rxp.htm";
	obj.ShowErrorMsg = 0 ;
	obj.Timeout=30;
	obj.Caption = _Caption;
	update = obj.CheckUpdate(_ShowMsg);
} catch(e) {
	ErrMsg = e.description;
	update = "2";
}
obj = null;

 if(osLang()!="ko")
		 Lang="_en";
 else
		 Lang="";
if(update != "0"){
	objdiv.style.display="none";
	Inst.style.display="";
	document.getElementById("Inst").innerHTML = vHTML;
	InstallRX("objrxpp",update);
}else{
	Inst.style.display="none";
	objdiv.style.display="";
	document.getElementById("objdiv").innerHTML = vRobjStr;
	ViewerReport ("","","0");
}

/****************************************************************************************
 * ReportExpress 개체 설치
 ****************************************************************************************/
function InstallRX(objName,update){

   if(osLang()!="ko") {	Lang="_en"; }
   else { Lang=""; }


	if ((osInfo.indexOf("NT 5.1") != -1) || (osInfo.indexOf("NT 5.2") != -1) || (osInfo.indexOf("NT 6.0") != -1)) { cab=rxRXType2; }
	else { cab=rxRXType2_9x; }

	if (update == "1") {  verRX=verRXUpdator; }

	var objStr =  '<OBJECT ID="' + objName + '"\n' +
               '     CLASSID="CLSID:67C19373-7A72-463B-8AB2-CBD6DEAC87B5"\n' +
               '     CODEBASE="' + cab + Lang + ".cab" + '#version=' + verRX + '"\n' +
               '     language="javascript"\n' +
               '     onreadystatechange="CheckrxUpdator()"\n' +
               '     onerror="error_activex()"\n' +
               '     style="DISPLAY:none"\n' +
               '     width="100%" height="100%">\n' +
               '    <PARAM NAME="DOMAIN" VALUE="">\n' +
               '</OBJECT>';
	document.getElementById("Inst").innerHTML = vHTML + objStr;
}

/****************************************************************************************
 * 제품 설치 유무 확인
 ****************************************************************************************/
function CheckrxUpdator(){

   var update     = 0;
   try{
	  var obj = new ActiveXObject("rxppUpdator.Updator");
	  obj.Caption = _Caption;
	  obj = null;
	  update = 1;
	}catch(e){
	  update = 0;
	}

	if(update == 1){
		isLoadRX = 1;
		OnLoadRX();
	} else {
		if(timer){clearInterval(timer);timer=null;}
			timer=setInterval("CheckrxUpdator();", 1000);
	}
}

/****************************************************************************************
 * ReportExpress 성공적으로 설치되었을 때 발생되는 이벤트
 ****************************************************************************************/
function OnLoadRX(){

	InstallError = 1;
	if(isLoadRX == 1) {
		try {
			var obj = new ActiveXObject("rxppUpdator.Updator");
			var pos = window.location.href;
			var re = /(\w+):\/\/([^/:]+)(:\d*)?/g ;
			var hostname = pos.match(re);
			obj.RemoteHost= hostname + rxPath + "module/";
			obj.SetupFileName= "rxp.htm";
			obj.ShowErrorMsg = 0 ;
			obj.Timeout=30;
			obj.Caption = _Caption;
			if (obj.Xinstall(_ShowMsg) == 1) {
				InstallError=0;
				windowclose("1");
			}
			obj = null;

		} catch(e) {
			alert(e.description);
		}
   }

}


/****************************************************************************************
 * ReportExpress  설치에서 오류가 발생한 경우
 ****************************************************************************************/
function error_activex(){
  // 설치 에러
  isLoadRX    = -1;


  var osInfo  = window.navigator.userAgent;
  var pname   = "ReportExpress™ RXProPlus";
  var msg = "\n";

  InstallError = 1;
   if(osLang()!="ko") {
      msg += "  An error occurred while processing this ReportExpress™ RXProPlus Installed.\n\n";
   } else {
      msg += "  ReportExpress™ RXProPlus 설치에 실패하였거나 설치를 취소하셨습니다.\n\n";
      msg += "  ReportExpress™ RXProPlus을 설치하기 위해서는\n";
      msg += "  보안 경고창에서 [예]를 선택하시기 바랍니다.\n\n";

	  if (osInfo.indexOf("SV1") != -1)
	  {
	    msg += "  Windows XP SP2가 설치되어 있는 경우 페이지 상단의\n";
	    msg += "  [알림표시줄]을 확인하여 ActiveX를 설치 하시기 바랍니다.\n\n";
	  }
   }
  if (ViewErrorMsg == true) {
      ViewErrorMsg = false;
      alert(msg);
  }
}

function windowclose(gubun){
	try{
		if(InstallError==0){
			if (gubun == "1"){
				Inst.style.display="none";
				objdiv.style.display="";
				document.getElementById("objdiv").innerHTML = vRobjStr;
				ViewerReport ("","","0");
			}else{
				window.open('about:blank','_self').close();
			}
		}
	} catch(e) {
		alert(e.description);
	}
}

function closeWin(flag){
	if(flag==0){
		window.open('about:blank','_self').close();
	}else{
		window.open('about:blank','_self').close();
	}
}


function osLang(){
		var Lang = navigator.browserLanguage;
		if(Lang=="ko" || Lang=="ko-KR"){
			return("ko");
		}else{
			return("en");
		}
}
