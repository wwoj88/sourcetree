//뷰어 외부기능 함수

//프린트
function Printbutton_onclick()
{
	Viewer1.PrintRpt();
}
//다른 파일로 저장
function Exportbutton_onclick()
{
	Viewer1.FileExport();
}
//이전으로
function Prevbutton_onclick()
{
	Viewer1.PagePrevious();
}
//다음으로
function Nextbutton_onclick()
{
	Viewer1.PageNext();
}
//줌아웃 : 축소
function ZoomOutbutton_onclick()
{
	Viewer1.ZoomOut();
}
//줌인 : 확대
function ZoomInbutton_onclick()
{
	Viewer1.ZoomIn();
}
//전체화면 : 한화면에서 전체 리포트 보기
function Fullbutton_onclick()
{
	Viewer1.Zoom(-2);
}
