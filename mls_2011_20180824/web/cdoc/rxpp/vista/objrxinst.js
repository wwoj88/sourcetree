<!--// ReportExpress Viewer install //-->
var rxPath        = "/cdoc/rxpp/vista/";    //ReportExpress 제품 설치 폴더
var _Caption      = "한국저작권위원회";			//설치 기관명
var _ShowMsg	  = false ;					// 설치 정보 보여주기
var ViewErrorMsg  = false;                  //설치 오류 메세지 보여주기 유무


/****************************************************************************************
 * rxRX.cab:      rxRX 패치 버전(3.0.0.10 이상 버전)
 * rxRX_full.cab: rxRX Full 버전(3.0.0.10 이하 버전)
 ****************************************************************************************/
var Lang		    = ""; 
var verRX       	= "3,0,0,10";           	//rxRX 버전(고정)
var verRXUpdator   	= "1,0,0,3";           		//RXUpdator 버전
var rxRX        	= "cab/rxpptype2";      	//rxRX 업그레이드 파일
var rxRXType2    	= "cab/rxpptype2";    		//rxRX 최초 설치 파일
var rxRXType2_9x    = "cab/rxpptype2_9x";    	//rxRX 최초 설치 파일 rxpptype2_9x.cab
var cab  			= ""; 							//최종 Cab파일
var RXError     	= false;
var osInfo        	= window.navigator.userAgent;

var InstallError	= 0;                  //설치 오류
var isLoadRX		= 1;                  	//rxRX 설치 여부
var timer			= null;

/****************************************************************************************
 * ReportExpress 개체 설치
 ****************************************************************************************/
function InstallRX(objName,update){

   if(osLang()!="ko") {	Lang="_en"; }
   else { Lang=""; }


	if ((osInfo.indexOf("NT 5.1") != -1) || (osInfo.indexOf("NT 5.2") != -1) || (osInfo.indexOf("NT 6.0") != -1)) { cab=rxRXType2; }
	else { cab=rxRXType2_9x; }

	if (update == "1") {  verRX=verRXUpdator; }

	var objStr =  '<OBJECT ID="' + objName + '"\n' +
               '     CLASSID="CLSID:67C19373-7A72-463B-8AB2-CBD6DEAC87B5"\n' +
               '     CODEBASE="' + cab + Lang + ".cab" + '#version=' + verRX + '"\n' +
               '     language="javascript"\n' +
               '     onreadystatechange="CheckrxUpdator()"\n' +
               '     onerror="error_activex()"\n' +
               '     style="DISPLAY:none"\n' +
               '     width="100%" height="100%">\n' +
               '    <PARAM NAME="DOMAIN" VALUE="">\n' +
               '</OBJECT>';

	document.write(objStr);
}

/****************************************************************************************
 * 제품 설치 유무 확인
 ****************************************************************************************/
function CheckrxUpdator(){
   var update     = 0;
   try{
	  var obj = new ActiveXObject("rxppUpdator.Updator");
	  obj.Caption = _Caption;
	  obj = null;
	  update = 1;
	}catch(e){
	  update = 0;
	}

	if(update == 1){
		isLoadRX = 1;
		OnLoadRX();
	} else {
		if(timer){clearInterval(timer);timer=null;}
			timer=setInterval("CheckrxUpdator();", 1000);
	}
}

/****************************************************************************************
 * ReportExpress 성공적으로 설치되었을 때 발생되는 이벤트
 ****************************************************************************************/
function OnLoadRX(){
//   alert("OnLoadRX,"+isLoadRX);
//   alert("OnLoadRX,"+objName.readyState+","+isLoadRX);

	InstallError = 1;
	if(isLoadRX == 1) {
		try {
			var obj = new ActiveXObject("rxppUpdator.Updator");
			//var hostip = window.location.host;
			var pos = window.location.href;
			pos = pos.substring(0,pos.lastIndexOf("/")+1);
			obj.RemoteHost= pos + "module/";
			obj.SetupFileName= "rxp.htm";
			obj.ShowErrorMsg = 0 ;
			obj.Timeout=30;
			obj.Caption = _Caption;
			if (obj.Xinstall(_ShowMsg) == 1) {
				InstallError=0; 
//
//				try {
//					obj = new ActiveXObject("RXProPlus.Viewer");
//					alert(obj.version);
//				}
//				catch(e) {
//					alert(e.description);
//				}
//				obj = null;
				windowclose("1");
			}
			obj = null;
			
		} catch(e) {
			alert(e.description);
		}
   }

}


/****************************************************************************************
 * ReportExpress  설치에서 오류가 발생한 경우
 ****************************************************************************************/
function error_activex(){
  // 설치 에러
  isLoadRX    = -1; 

  	
  var osInfo  = window.navigator.userAgent;
  var pname   = "ReportExpress™ RXProPlus";
  var msg = "\n";
  
  InstallError = 1;
   if(osLang()!="ko") {	
      msg += "  An error occurred while processing this ReportExpress™ RXProPlus Installed.\n\n";
   } else { 
      msg += "  ReportExpress™ RXProPlus 설치에 실패하였거나 설치를 취소하셨습니다.\n\n";
      msg += "  ReportExpress™ RXProPlus을 설치하기 위해서는\n";
      msg += "  보안 경고창에서 [예]를 선택하시기 바랍니다.\n\n";
  
	  if (osInfo.indexOf("SV1") != -1)
	  {
	    msg += "  Windows XP SP2가 설치되어 있는 경우 페이지 상단의\n";
	    msg += "  [알림표시줄]을 확인하여 ActiveX를 설치 하시기 바랍니다.\n\n";
	  }
   }
//    msg = msg + "  " + pname + " 설치에 계속해서 문제가 발생하는 경우\n"
//              + "  (주) 캡소프트로 연락바랍니다.\n\n"
//              + "  [연락처] 전      화: 0505-998-0888\n"
//              + "               팩      스: 0505-987-0888\n"
//              + "               메      일: support@cabsoftware.com\n"
              + "" ;
  if (ViewErrorMsg == true) {
      ViewErrorMsg = false;
      alert(msg);
  }
}

function osLang(){
    var Lang = navigator.browserLanguage;
    if(Lang=="ko" || Lang=="ko-KR"){
    	return("ko");
    }else{
    	return("en");
    }
}
