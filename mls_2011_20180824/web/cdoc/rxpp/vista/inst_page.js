/****************************************************************************************
 * ReportExpress Viewer
 ****************************************************************************************/

var rxPath			= "/cdoc/rxpp/vista/";
var _Caption		= "저작권찾기";
var _ShowMsg		= true ;
var ErrMsg          = "";
var Lang		= "";
var osInfo        = window.navigator.userAgent;
if(osInfo.indexOf("Windows NT 6.0")==-1 && 0 ){
//if(osInfo.indexOf("Windows NT 6.0")==-1){
} else {
	var update     	= "";
	var obj = null;
	try {
		obj = new ActiveXObject("rxppUpdator.Updator");
		var pos = window.location.href;
		//pos = pos.substring(0,pos.lastIndexOf("/")+1);
		var re = /(\w+):\/\/([^/:]+)(:\d*)?/g ;
		var hostname = pos.match(re);

		obj.RemoteHost= hostname + rxPath + "module/";
		obj.SetupFileName= "rxp.htm";
		obj.ShowErrorMsg = 0 ;
		obj.Timeout=30;
		obj.Caption = _Caption;
		update = obj.CheckUpdate(_ShowMsg);
	} catch(e) {
		ErrMsg = e.description;
		update = "2";
	}
	obj = null;

   if(osLang()!="ko")
   		Lang="_en";
   else
   		Lang="";
	if(update != "0"){
	  window.location.href = rxPath + "inst_vista_js" + Lang + ".htm?update=" + update;
	}
}


function osLang(){
    var Lang = navigator.browserLanguage;
    if(Lang=="ko" || Lang=="ko-KR"){
    	return("ko");
    }else{
    	return("en");
    }
}
