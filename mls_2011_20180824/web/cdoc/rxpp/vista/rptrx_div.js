<!--// 레포트 보여주기 함수
function ViewerReport(rpx,xml,modal) {

	if(modal==null)
		modal = "0" ;	// 모달로띄운다. : 1 else:0
		
	var hostip = window.location.host;
	var hostpath = window.location.pathname;
	hostpath = hostpath.substring(0,hostpath.lastIndexOf("/")+1);
	if (hostpath.substr(0,1) != "/" ) hostpath = "/" + hostpath;

	var Viewer1 = document.getElementById('Viewer1');
	try {
		Viewer1.Initialize();
		Viewer1.Server = "http://" + hostip + "/cdoc/rxpp/" ;
		Viewer1.ReportLayout = reportform.rpx.value;
		if(reportform.fileurl.value != "" && reportform.fileurl.value != null){
			Viewer1.Fileurl = reportform.fileurl.value;
		}else{
			Viewer1.xml = reportform.xmldata.value;
		}
		Viewer1.Modal = modal;		//  모달창으로 띄우기
		if (modal == 1){ 
			Viewer1.style.display = "none" ;
		}else {
			Viewer1.style.display = "" ;
		}
		Viewer1.Export = 1;		    // 내보내기 기능 사용유무
		//Viewer1.NewExport.ExportFormat = 9; // 내보내기 기능별 추가
		Viewer1.ViewToolBar = 1;	// 도구바 보여주기 기능 유무
		Viewer1.debugging = 1;		// 에러 보이기
		Viewer1.SetZoom = 100;       // 보고서 보기 상태
		Viewer1.RunViewer();
	} catch(e) {
		alert("ReportExpress™ Pro Plus Viewer가 정상적으로 동작하지 않습니다.\n\n(Error: "+ e.description +")");
	}
}
//-->