/******* hwp 문서 만들기 위한 javascript **********/
/******* 1.0.0.19 sp3 2008-12-08 10:00오전  **********/
////		logs(thobj,CFilter);
var _DEBUG = false;			// Debug mode.
var xmldom;
var xmldom2;
var tFname = new Array();
var tAttr= new Array();
var strMerge = "";
var MinVersion = 0x05050100;  //권장버전
var CurVersion = "";
var tmpstr = 0;
var _Fctl = false;			// Field 찾기
var _UNICODE = false;		// UNICODE
var DFilter= "";				// 기본 필터값
var CFilter= "";				// 기본 필터값
var hosthref = "";
var ReportLayoutType = true;	// hwpFiles

function GetServer(strVal) { hosthref = strVal ; }
function SetReportLayoutType(strVal) { ReportLayoutType = strVal ; }
//버전체크
function VerifyVersion(hobj)
{
	// 설치확인
	if(hobj.getAttribute("Version") == null)
	{
		alert("한글 프로그램이 설치되지 않았습니다.\n" +
		"한글 프로그램 설치 후 다시 시도하시기 바랍니다.");
		//window.open('about:blank','_self').close();
		return;
	}
	//버젼 확인
	CurVersion = hobj.Version; //현재버전
//	alert("현재 설치되어있는 한글 컨트롤 버전은\n"+
//			CurVersion +" 입니다.");

	if(CurVersion < MinVersion)
	{
		alert("hobj의 버젼이 낮아서 정상적으로 동작하지 않을 수 있습니다.\n"+
					"최신 버젼으로 업데이트하신 후 다시 시도하시기 바랍니다.\n\n"+
					"현재 버젼: 0x" + CurVersion.toString(16) + "\n"+
					"권장 버젼: 0x" + MinVersion.toString(16) + " 이상");
		window.open('about:blank','_self').close();
	}
}

function PutFieldText(hobj, cid, cval,unicode) {
	if (unicode = null) { unicode = _UNICODE; }

	_Fctl = hobj.MoveToField(cid);
	if ( _Fctl == true) {
		if (_UNICODE == true) {
			hobj.SetTextFile(Convert(cval),"UNICODE", "insertfile");
		} else {
			hobj.PutFieldText(cid,Convert(cval));
		}
	}
}

//툴바설정
function InitToolBarJS(hobj)
{
	hobj.ReplaceAction("FileNew", "HwpCtrlFileNew");
	hobj.ReplaceAction("FileOpen", "HwpCtrlFileOpen");
	hobj.ReplaceAction("FileSave", "HwpCtrlFileSave");
	hobj.ReplaceAction("FileSaveAs", "HwpCtrlFileSaveAs");
	hobj.ReplaceAction("FindDlg", "HwpCtrlFindDlg");
	hobj.ReplaceAction("ReplaceDlg", "HwpCtrlReplaceDlg");

//	hobj.SetToolBar(-1, "TOOLBAR_MENU");
//	hobj.SetToolBar(-1, "TOOLBAR_STANDARD");
//	hobj.SetToolBar(-1, "TOOLBAR_FORMAT");
	hobj.ShowStatusBar(1);

	hobj.SetToolBar(0, "#2;1:기본 도구 상자, FileNew, FileOpen, FileSave, FileSaveAs, InsertFile, Separator, FilePreview, Print, Separator, Undo, Redo, Separator, Cut, Copy, Paste,");
//	+ "Separator, ParaNumberBullet, MultiColumn, SpellingCheck, HwpDic, Separator, PictureInsertDialog, InsertFieldMemo, DeleteFieldMemo, MemoShape, ViewOptionMemo, ViewOptionMemoGuideline");

//	hobj.SetToolBar(1, "DrawObjCreatorLine, DrawObjCreatorRectangle, DrawObjCreatorEllipse,"
//	+"DrawObjCreatorArc, DrawObjCreatorPolygon, DrawObjCreatorCurve, DrawObjCreator, DrawObjTemplateLoad,"
//	+"Separator, ShapeObjSelect, ShapeObjGroup, ShapeObjUngroup, Separator, ShapeObjBringToFront,"
//	+"ShapeObjSendToBack, ShapeObjDialog, ShapeObjAttrDialog");

	hobj.SetToolBar(1, "DrawObjCreatorPolygon, StyleCombo, CharShapeLanguage, CharShapeTypeFace, CharShapeHeight,"
	+"CharShapeBold, CharShapeItalic, CharShapeUnderline, ParagraphShapeAlignJustify, ParagraphShapeAlignLeft,"
	+"ParagraphShapeAlignCenter, ParagraphShapeAlignRight, Separator, ParaShapeLineSpacing,"
	+"ParagraphShapeDecreaseLeftMargin, ParagraphShapeIncreaseLeftMargin");
	hobj.ShowToolBar(true);
}



////////////////////////////////////////////// 문서 오픈
function InsertDoc(hobj,pos,hwpurl,fmt,type)
{
	if (pos != "")
	{
		hobj.movepos(pos);
	}

	if (hwpurl.indexOf("://") == -1) {
		if (ReportLayoutType ) {
			hobj.open(hosthref+"hwpFiles/"+hwpurl,fmt, type);
		} else {
			hobj.Insert(hosthref+"hwpFiles/"+hwpurl,fmt, type);
		}
	}else {
		hobj.open(hwpurl,fmt, type);
	}
}


function OpenDoc(hobj,thobj,pos,path,filenm)
{
	if (pos != "")
	{
		thobj.movepos(pos);
	}

	if(!thobj.open(path + filenm))
	{
		Run(hobj, 'DeleteBack');
	}
}

////////////////////////////////////////////// 문서 오픈
function SetTextFile(hobj,thobj,pos,hwptext,fmt,type)
{
	if (pos != "")
	{
		hobj.movepos(pos);
	}
	if (hwptext != "")
	{
		hobj.SetTextFile(hwptext,fmt, type);
	} else {
		hobj.SetTextFile(thobj.GetTextFile("HWP",""),fmt, type);
	}
	hobj.MovePos(pos);
}

   // 셀 줄 나누기
 function TableSplitCellRow2(hobj,fName,count)
 {
   var tFname = new Array();

   for (i=0 ;i<count ;i++ )
   {
	   hobj.MoveToField(fName);
	   hobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동

	   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	   if (i==0) { tFname[0]=hobj.GetCurFieldName();}
	   //   InsertText("10");
	   hobj.Run("TableSplitCellRow2");
	   hobj.MovePos(103);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	   hobj.SetCurFieldName(tFname[0] + (i+1));
	//   InsertText("11");

	   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	//   InsertText("12");
	   if (i==0) { tFname[1]=hobj.GetCurFieldName(); }
	   hobj.Run("TableSplitCellRow2");
	   hobj.MovePos(103);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	   hobj.SetCurFieldName(tFname[1] + (i+1));

	   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	//   InsertText("12");
	   if (i==0) { tFname[2]=hobj.GetCurFieldName(); }
	   hobj.Run("TableSplitCellRow2");
	   hobj.MovePos(103);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	   hobj.SetCurFieldName(tFname[2] + (i+1));

	   hobj.CellFill
	//   InsertText("13");

   }
 }


   // 셀 줄 나누기
 function TableSplitCellRow3(hobj,crows,count)
 {

	//alert(crows  + " - " + tFname[0]);
   for (i=crows ;i< (crows+count) ;i++ )
   {
		//alert("셀나누기" + i);
		//alert(GetCurFieldName());
//	   hobj.MoveToField(tFname[0]);
	   hobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
	   hobj.SetCurFieldName(tFname[0] + (i));

	   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
//	   if (i==0) { tFname[0]=hobj.GetCurFieldName(); alert(tFname[0]);}
	   //   InsertText("10");
	   hobj.Run("TableSplitCellRow2");
	   hobj.MovePos(103);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	   hobj.SetCurFieldName(tFname[0] + (i));
	//   InsertText("11");

	   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	//   InsertText("12");
//	   if (i==0) { tFname[1]=hobj.GetCurFieldName(); }
	   hobj.Run("TableSplitCellRow2");
	   hobj.MovePos(103);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	   hobj.SetCurFieldName(tFname[1] + (i));

	   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	//   InsertText("12");
//	   if (i==0) { tFname[2]=hobj.GetCurFieldName(); }
	   hobj.Run("TableSplitCellRow2");
	   hobj.MovePos(103);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	   hobj.SetCurFieldName(tFname[2] + (i));

	   hobj.CellFill
	//   InsertText("13");

   }
 }

   // 셀 줄 나누기
 function TableSplitCellRow4(hobj,crows,count,cols)
 {

//	alert(crows  + " - " + tFname[0]);
   for (i=crows ;i< (crows+count) ;i++ )
   {
//		alert("셀나누기" + i);
//	   hobj.MoveToField(tFname[0]);
	   hobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
	   hobj.SetCurFieldName(tFname[0] + (i));
	   //alert(hobj.GetCurFieldName());

	   for (j=1;j<cols ;j++ )
	   {
		   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
//		   hobj.Run("TableSplitCellRow2");
//		   hobj.MovePos(103);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
		   hobj.SetCurFieldName(tFname[j] + (i));
//		   alert(hobj.GetCurFieldName());

	   }

	   hobj.CellFill
	//   InsertText("13");

   }
 }


   // 셀 줄 나누기
 function TableSplitCellRow5(hobj,crows,count,cols)
 {
   if (count>1)
   {

	//	alert(crows  + " - " + tFname[0]);
	   for (i=(crows-count)+1 ;i < (crows) ;i++ )		// 하나적게 조개기
	   {
	//		alert("셀나누기" + i);
		   if(hobj.MoveToField(tFname[0]+(crows-count)) == false) {
////			   if (_DEBUG ==true) { alert("["+crows+"] "+tFname[0]+(crows-count)+'을 찾을 수 없습니다');  }
		   }
		   hobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
		   hobj.SetCurFieldName(tFname[0] + (i+1));


		   if (count>1)
		   {
			   ////셀 나누기
			   for (j=1;j<=cols ;j++ )
			   {
				   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
				   hobj.SetCurFieldName(tFname[j] + (i));
				   hobj.Run("TableSplitCellRow2");
				   hobj.MovePos(103);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
				   hobj.SetCurFieldName(tFname[j] + (i+1));
		//		   alert(hobj.GetCurFieldName());

			   }

		   } else {
			   ////셀 나누지 않기
			   for (j=1;j<=cols ;j++ )
			   {
				   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
				   hobj.SetCurFieldName(tFname[j] + (i));
			   }
		   }

		   hobj.CellFill
		//   InsertText("13");

	   }
   } else {
		   i= crows;	// 현재 값으로 셋팅
//			alert("셀나누지 않기" + tFname[0] + (i));
		   if(hobj.MoveToField(tFname[0]+(crows-count)) == false) {
//			   if (_DEBUG ==true) { alert("["+crows+"] "+tFname[0]+(crows-count)+'을 찾을 수 없습니다');  }
		   }
		   hobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
		   hobj.SetCurFieldName(tFname[0] + (i));


	   ////셀 나누지 않기
		   for (j=1;j<=cols ;j++ )
		   {
			   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
			   hobj.SetCurFieldName(tFname[j] + (i));
		   }
		   hobj.CellFill
   }

	return;

 }


   // 셀 줄 나누기
 function TableSplitCellRow6(hobj,crows,count,cols)
 {
   if (count>1)
   {

	//	alert(crows  + " - " + tFname[0]);
	   for (i=(crows-count)+1 ;i < (crows) ;i++ )		// 하나적게 조개기
	   {
	//		alert("셀나누기" + i);
		   if(hobj.MoveToField(tFname[0]+(crows-count)) == false) {
//			   if (_DEBUG ==true) { alert("["+crows+"] "+tFname[0]+(crows-count)+'을 찾을 수 없습니다');  }
		   }
		   hobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
		   hobj.SetCurFieldName(tFname[0] + (i+1));


		   if (count>1)
		   {
			   ////셀 나누기
			   for (j=1;j<=cols ;j++ )
			   {
				   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
				   hobj.SetCurFieldName(tFname[j] + (i));
				   hobj.Run("TableSplitCellRow2");
				   hobj.MovePos(103);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
				   hobj.SetCurFieldName(tFname[j] + (i+1));
		//		   alert(hobj.GetCurFieldName());

			   }

		   } else {
			   ////셀 나누지 않기
			   for (j=1;j<=cols ;j++ )
			   {
				   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
				   hobj.SetCurFieldName(tFname[j] + (i));
			   }
		   }

		   hobj.CellFill
		//   InsertText("13");

	   }
   } else {
		   i= crows;	// 현재 값으로 셋팅
//			alert("셀나누지 않기" + tFname[0] + (i));
		   if(hobj.MoveToField(tFname[0]+(crows-count)) == false) {
//			   if (_DEBUG ==true) { alert("["+crows+"] "+tFname[0]+(crows-count)+'을 찾을 수 없습니다');  }
		   }
		   hobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
		   hobj.SetCurFieldName(tFname[0] + (i));


	   ////셀 나누지 않기
		   for (j=1;j<=cols ;j++ )
		   {
			   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
			   hobj.SetCurFieldName(tFname[j] + (i));
		   }
		   hobj.CellFill
   }

	return;

 }



function RenameTableCell(hobj,rows)
 {
   if (rows>1)
   {

	//	alert(crows  + " - " + tFname[0]);
	   for (i=(crows-count)+1 ;i < (crows) ;i++ )		// 하나적게 조개기
	   {
	//		alert("셀나누기" + i);
		   if(hobj.MoveToField(tFname[0]+(crows-count)) == false) {
//			   if (_DEBUG ==true) { alert("["+crows+"] "+tFname[0]+(crows-count)+'을 찾을 수 없습니다');  }
		   }
		   hobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
		   hobj.SetCurFieldName(tFname[0] + (i+1));


		   if (count>1)
		   {
			   ////셀 나누기
			   for (j=1;j<=cols ;j++ )
			   {
				   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
				   hobj.SetCurFieldName(tFname[j] + (i));
				   hobj.Run("TableSplitCellRow2");
				   hobj.MovePos(103);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
				   hobj.SetCurFieldName(tFname[j] + (i+1));
		//		   alert(hobj.GetCurFieldName());

			   }

		   } else {
			   ////셀 나누지 않기
			   for (j=1;j<=cols ;j++ )
			   {
				   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
				   hobj.SetCurFieldName(tFname[j] + (i));
			   }
		   }

		   hobj.CellFill
		//   InsertText("13");

	   }
   } else {
		   i= crows;	// 현재 값으로 셋팅
//			alert("셀나누지 않기" + tFname[0] + (i));
		   if(hobj.MoveToField(tFname[0]+(crows-count)) == false) {
//			   if (_DEBUG ==true) { alert("["+crows+"] "+tFname[0]+(crows-count)+'을 찾을 수 없습니다');  }
		   }
		   hobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
		   hobj.SetCurFieldName(tFname[0] + (i));


	   ////셀 나누지 않기
		   for (j=1;j<=cols ;j++ )
		   {
			   hobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
			   hobj.SetCurFieldName(tFname[j] + (i));
		   }
		   hobj.CellFill
   }

	return;

 }


// 해당 테이블의 마지막 행에 이어서 행을 추가한다.
// FirstCellName : 표의 첫번째 행, 첫번째 열의 셀필드 명.
function TableAppendRow(hobj,FirstCellName)
{
	if (hobj.MoveToField(FirstCellName, false, false, false))
	{
		hobj.Run("TableCellBlock");
		hobj.Run("TableColPageDown");
		hobj.Run("Cancel");
		hobj.Run("TableAppendRow");


		hobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
//		hobj.SetCurFieldName("tesst");
//		hobj.SetCurFieldName("tesst");
	    InsertText(hobj,"11");


  	    TableSplitCellRow3(hobj,"성과목표명",2);
		return true;
	}
	else
	{
//		if (_DEBUG) alert("셀필드(" + FirstCellName +")가 존재하지 않습니다.");
		return false;
	}
}


// 해당 테이블의 마지막 행에 이어서 행을 추가한다.
// FirstCellName : 표의 첫번째 행, 첫번째 열의 셀필드 명.
function TableAppendRow1(hobj,FirstCellName)
{
	if (hobj.MoveToField(FirstCellName, false, false, false))
	{
		hobj.Run("TableCellBlock");
		hobj.Run("TableColPageDown");
		hobj.Run("Cancel");
		hobj.Run("TableAppendRow");
		return true;
	}
	else
	{
//		if (_DEBUG) alert("셀필드(" + FirstCellName +")가 존재하지 않습니다.");
		return false;
	}
}


// 해당 테이블의 마지막 행에 이어서 행을 추가한다.
// FirstCellName : 표의 첫번째 행, 첫번째 열의 셀필드 명.
function TableAppendRow2(hobj,FirstCellName)
{
	if (hobj.MoveToField(FirstCellName, false, false, false))
	{
		hobj.Run("TableCellBlock");
		hobj.Run("TableColPageDown");
		hobj.Run("Cancel");
		hobj.Run("TableAppendRow");

		return true;
	}
	else
	{
//		if (_DEBUG) alert("셀필드(" + FirstCellName +")가 존재하지 않습니다.");
		return false;
	}
}

// 해당 테이블의 마지막 행에 이어서 행을 추가한다.
// FirstCellName : 표의 첫번째 행, 첫번째 열의 셀필드 명.
function TableAppendRowSetName(thobj,FirstCellName,crow,cols)
{
	if (thobj.MoveToField(FirstCellName, false, false, false))
	{
		thobj.Run("TableCellBlock");
		thobj.Run("TableColPageDown");
		thobj.Run("Cancel");
		thobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
		thobj.Run("TableAppendRow");
		//InsertText(hobj,"AAAA");

	   ////셀 나누기
	   //hobj.MovePos(104);   // 현재 캐럿이 위치한 셀에서 맨처음으로 이동
	   for (j=0;j<cols ;j++ )
	   {
		   thobj.SetCurFieldName(tFname[j] + crow);
		   //InsertText(hobj,tFname[j] + crow);
		   thobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	   }
		return true;
	}
}

// 해당 테이블의 마지막 행에 이어서 행을 추가한다.
// FirstCellName : 표의 첫번째 행, 첫번째 열의 셀필드 명.
function TableAppendRowSetFieldName(thobj,FirstCellName,crow,cols)
{
	if (thobj.MoveToField(FirstCellName, false, false, false))
	{
		thobj.Run("TableCellBlock");
		thobj.Run("TableColPageDown");
		thobj.Run("Cancel");
		thobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
		thobj.Run("TableAppendRow");
		//InsertText(hobj,"AAAA");

	   ////셀 나누기
	   //hobj.MovePos(104);   // 현재 캐럿이 위치한 셀에서 맨처음으로 이동
	   for (j=0;j<cols ;j++ )
	   {
		   thobj.SetCurFieldName(tFname[j] + crow);
		   //InsertText(hobj,tFname[j] + crow);
		   thobj.MovePos(101);   // 현재 캐럿이 위치한 셀에서 오른쪽으로 이동
	   }
		return true;
	} else {
		alert("[TableAppendRowSetFieldName] Field Not found. - " + FirstCellName);
	}
}

//텍스트 집어넣기
  function InsertText(thobj,strVal)
 {
  var act = thobj.CreateAction("InsertText");
  var set = act.CreateSet();
  set.SetItem("Text", strVal);
  act.Execute(set);

   //window.setTimeout("InsertText();", 100);
 }

function logs(thobj,strVal)
 {
 	InsertText(thobj,"\r\n[Debug]"+strVal);
 }

function FindCtrl(hobj,ctrlID)
{
	code = hobj.HeadCtrl;
	var paramSet;
	var list, para, pos;
	while (code && code != hobj.LastCtrl) {
		var strID = code.Ctrlid;
		if (strID == ctrlID) {
			paramSet = code.GetAnchorPos(0);
			list = paramSet.Item("List");
			para  = paramSet.Item("Para");
			pos  = paramSet.Item("Pos");

			hobj.SetPos(list, para, pos);
			break;
		}
		code = code.Next;
	}
	hobj.MovePos(16, para, pos);

}

function DeleteCtrl(hobj,ctrlID)
{
var ctrl =null;
var nxtctrl =null ;
var ctrl = hobj.HeadCtrl;
	while (ctrl  != null) {
		var nxtctrl = ctrl.Next;
	    if (ctrl.CtrlID == ctrlID) {
			    hobj.DeleteCtrl (ctrl);
	    }
		var ctrl = nxtctrl;
	}

//	logs(hobj,"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
//	code = hobj.HeadCtrl;
//	var paramSet;
//	var list, para, pos;
//	while (code && code != hobj.LastCtrl) {
//		var strID = code.Ctrlid;
//			logs(hobj,"DeleteCtrl"+strID+"_"+ctrlID);
//		if (strID == ctrlID) {
//			hobj.DeleteCtrl (code);
//			break;
//		}
//		code = code.Next;
//	}
}

/////////// 필드값 변환
function xPutFieldText(thobj,xpath,idx,chkfilter,filter)
{
	  if (filter == null) { filter="";}
	  if (chkfilter == null) { chkfilter="";}
	  if (chkfilter == "") { chkfilter="0";}

	  try
	  {
		if (chkfilter == "0") {
			CFilter = xpath+"["+idx+"]";
		} else {
			CFilter = xpath+"["+idx+"]";
	    }
	    if (_DEBUG) logs(thobj,"xPutFieldText xpath-"+CFilter);

		var oNodes = xmldom.selectNodes(CFilter);
		for (i=0; i<oNodes.length; i++)
		{
		   oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
//			  	if (_DEBUG) logs(thobj,oNode.length);
			  	if (_DEBUG) logs(thobj,oNode.childNodes.length);
//	   	    	if (_DEBUG) logs(thobj,"     xPutFieldText "+"Node ("+i+"), <"+ oNode.nodeName + ">:\r\n" +oNode.xml);
			   for( j = 0 ; j< oNode.childNodes.length ; j++) {
			   		if (_DEBUG) logs(thobj, "[child]"+ oNode.childNodes(j).nodeName);
//			   		if (_DEBUG) logs(thobj, "[child]"+ oNode.childNodes(j).text);
			   		if (filter != "") {
			   				if (filter == oNode.childNodes(j).nodeName) {
			   					DFilter = 	oNode.childNodes(j).nodeName + "='" + oNode.childNodes(j).text +"'";
			   				}
			   		}
					PutFieldText (thobj,oNode.childNodes(j).nodeName,oNode.childNodes(j).text);
			   }
		   }
		}
	  }
	  catch (e)
	  {
		logs(thobj,"[xPutFieldText]"+e.description);
	  }
}

/////////// 필드값 변환
function xPutText(hobj,thobj,xpath,hname,fieldnm,idx,chkfilter,filter)
{

	var tAttr = new Array();
	var newString = "";

	  if (filter == null) { filter="";}
	  if (chkfilter == null) { chkfilter="";}
	  if (chkfilter == "") { chkfilter="0";}

	  try
	  {
//		if (chkfilter == "0") {
//			CFilter = xpath+"["+idx+"]";
//		} else {
//			CFilter = xpath+"["+idx+"]";
//	    }

		CFilter = xpath;

		var oNodes = xmldom.selectNodes(CFilter);
		for (i=0; i<oNodes.length; i++)
		{
		   oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
		   			InsertDoc(thobj,'3',hname,'HWP','');


//			  logs(hobj,"Node ("+i+"), <"+ oNode.nodeName + ">:\r\n" +oNode.xml);
//			  alert(oNode.childNodes.length);
			   for( j = 0 ; j< oNode.childNodes.length ; j++) {



					//logs(hobj, "[child]"+ oNode.childNodes(j).nodeName + "-" + oNode.childNodes(j).text);
//					logs(hobj, "[child]"+ oNode.childNodes(j).nodeName );

//						row_1 = FindAttribute(oNode.childNodes(j).attributes,"chk","doc");
//						if (row_1 == true)
//						{
//							_Fctl = thobj.MoveToField('내용1');
//							if ( _Fctl == true) {
//								thobj.SetTextFile(oNode.childNodes(j).text,'HWP','insertfile');
//							}
//						}


						var attrs = oNode.childNodes(j).attributes;
						for(var l = 0 ; l<attrs.length ; l++){
							var attr=attrs.item(l);
							if (attr.nodeName == "chk" && attr.nodeValue == "doc"){
								thobj.MoveToField(fieldnm);
								thobj.SetTextFile(oNode.childNodes(j).text,'HWP','insertfile');
							} else {

							}

							newString = oNode.childNodes(j).text;

							if (attr.nodeName == "chk" && attr.nodeValue == "txt_1") 	{
								if (tAttr[j] == oNode.childNodes(j).text) {
//									DeleteCtrl(thobj,oNode.childNodes(j).nodeName);
//									logs(hobj,"같다!!!!!!!!!!");
									newString ="";
									// delete 두번
//									Run(thobj,'DeleteBack');

								} else {
									tAttr[j] =  oNode.childNodes(j).text;
							   }
							} else {
									newString = oNode.childNodes(j).text;
							}
							//alert(attr.nodeName + " : " + attr.nodeValue + "<br>");
						}
						PutFieldText (thobj,oNode.childNodes(j).nodeName,newString);
			   }
			   	SetTextFile (hobj,thobj,'3','','HWP','insertfile');
		   }
		}
	  }
	  catch (e)
	  {
			//logs(hobj,e.description);
	  }
}


function xPutFieldText_SetTextFile(hobj,thobj,xpath,chkfilter,hwp1,filter1,hwp2,filter2,filter)
{
	  var DFilter_01= "";
	  if (filter == null) { filter="";}
	  if (chkfilter == null) { chkfilter="";}
	  if (chkfilter == "") { chkfilter="0";}

	  try
	  {
	  	CFilter = xpath;//+"["+idx+"]";
		if (DFilter != "") {
			CFilter += "["+DFilter+"]";
		}
		if (filter1 != "") {
			CFilter += filter1;
		}
		var oNodes = xmldom.selectNodes(CFilter);
		for (i=0; i<oNodes.length; i++)
		{
		   oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
		   	  InsertDoc(thobj,'3',hwp1,'HWP','');
//			  alert("Node ("+i+"), <"+ oNode.nodeName + ">:\r\n" +oNode.xml);
//			  alert(oNode.childNodes.length);
			   for( j = 0 ; j< oNode.childNodes.length ; j++) {
			   		if (filter != "") {
			   				if (filter == oNode.childNodes(j).nodeName) {
			   					DFilter_01 = 	oNode.childNodes(j).nodeName + "='" + oNode.childNodes(j).text +"'";
			   				}
			   		}
			   		PutFieldText (thobj,oNode.childNodes(j).nodeName,oNode.childNodes(j).text);
			   }
			   // 문서 삽입
			   SetTextFile (hobj,thobj,'3','','HWP','insertfile');
			   // 상세 내용


			  	CFilter = xpath;//+"["+idx+"]";
				if (DFilter != "") CFilter += "["+DFilter+"]";
				if (DFilter_01 != "") CFilter += "["+DFilter_01+"]";
				if (filter2 != "") CFilter += filter2;

				var oNodes1 = xmldom.selectNodes(CFilter);
				for (ii=0; ii<oNodes1.length; ii++)
				{
				   oNode = oNodes1.nextNode;
				   if (oNode != null)
				   {
				   	  InsertDoc(thobj,'3',hwp2,'HWP','');
		//			  alert("Node ("+i+"), <"+ oNode.nodeName + ">:\r\n" +oNode.xml);
		//			  alert(oNode.childNodes.length);
					   for( j = 0 ; j< oNode.childNodes.length ; j++) {
							//alert( "[child]"+ oNode.childNodes(j).nodeName + "-" + oNode.childNodes(j).text);
		//					thobj.PutFieldText(oNode.childNodes(j).nodeName,Convert(oNode.childNodes(j).text));
//							InsertText(thobj, "[child]"+ oNode.childNodes(j).nodeName + "-" + oNode.childNodes(j).text);

			   				PutFieldText (thobj,oNode.childNodes(j).nodeName,oNode.childNodes(j).text);
					   }
					   // 문서 삽입
					   SetTextFile (hobj,thobj,'3','','HWP','insertfile');
					   // 상세 내용

				   }
				}
		   }
		}
	  }
	  catch (e)
	  {
		alert(e.description);
	  }
//	  logs(thobj,"===================="+DFilter);
}


///////// 테이블 행추가,
function xTableAppendRow(thobj,xpath,crows,rows,cols)
{
	  if ((crows-rows)>0)
	  {
			TableAppendRow1(thobj,tFname[0]+(crows-rows));
	  }

	  TableSplitCellRow5(thobj,crows,rows,cols-1);

// 테이블 값넣기
		for (k=(crows-rows)+1 ;k<=crows ;k++ )
		{
			//k=(crows-rows)+1;

		  try
		  {
			var oNodes = xmldom.selectNodes(xpath+"["+(k-1)+"]");
			for (i=0; i<oNodes.length; i++)
			{
			   oNode = oNodes.nextNode;
			   if (oNode != null)
			   {
	//			  alert("Node ("+i+"), <"+ oNode.nodeName + ">:\r\n" +oNode.xml);
	//			  alert(oNode.childNodes.length);
				   for( j = 0 ; j< oNode.childNodes.length ; j++) {
						//alert( "[SetTAB]"+ oNode.childNodes(j).nodeName );
			   			PutFieldText (thobj,oNode.childNodes(j).nodeName+k,oNode.childNodes(j).text);
				   }
			   }
			}
		  }
		  catch (e)
		  {
			alert(e.description);
		  }
	  }
}

function xTableAppendRow_color(thobj,xpath,crows,rows,cols)
{
	  if ((crows-rows)>0)
	  {
			TableAppendRow1(thobj,tFname[0]+(crows-rows));
	  }

	  TableSplitCellRow5(thobj,crows,rows,cols-1);

// 테이블 값넣기
		for (k=(crows-rows)+1 ;k<=crows ;k++ )
		{
			//k=(crows-rows)+1;

		  try
		  {
			var oNodes = xmldom.selectNodes(xpath+"["+(k-1)+"]");
			for (i=0; i<oNodes.length; i++)
			{
			   oNode = oNodes.nextNode;
			   if (oNode != null)
			   {
	//			  alert("Node ("+i+"), <"+ oNode.nodeName + ">:\r\n" +oNode.xml);
	//			  alert(oNode.childNodes.length);
				   for( j = 0 ; j< oNode.childNodes.length ; j++) {
						//alert( "[SetTAB]"+ oNode.childNodes(j).nodeName );
						_UNICODE = true;
						if (_UNICODE == true)
						{
							_Fctl = thobj.MoveToField(oNode.childNodes(j).nodeName+k);
							if ( _Fctl == true) {

								thobj.SetTextFile(Convert(oNode.childNodes(j).text),"UNICODE", "insertfile");


								// 현재줄의 맨 앞으로 커서를 옮긴후 한단어를 블럭지정한다.
								thobj.Run("MoveLineBegin");
								thobj.Run("Select");
								thobj.Run("MoveSelNextWord");

								// 블럭 지정된 단어의 글자 속성을 변경한다.
								var dAct = thobj.CreateAction("CharShape");
									var dSet = dAct.CreateSet();
									dAct.GetDefault(dSet);
									dSet.SetItem("TextColor", 0xFF0000);		// 글자 색을 파란색으로
									dAct.Execute(dSet);

									thobj.Run("Cancel");

								// 블럭 지정된 셀배경을 변경한다.
								var dAct = thobj.CreateAction("CellFill");
									var dSet = dAct.CreateSet();
									dAct.GetDefault(dSet);
									dSet.SetItem("ShadeColor", 0x00FF00);		// 글자 색을 파란색으로
									dAct.Execute(dSet);

									thobj.Run("Cancel");

								// 블럭 지정된 셀배경을 변경한다.
								var dAct = thobj.CreateAction("CellBorderFill");
									var dSet = dAct.CreateSet();
									dAct.GetDefault(dSet);
									dSet.SetItem("ShadeColor", 0x00FF00);		// 글자 색을 파란색으로
									dAct.Execute(dSet);

									thobj.Run("Cancel");
							}	//
						} else {
							thobj.PutFieldText(oNode.childNodes(j).nodeName+k,Convert(oNode.childNodes(j).text));
						}
				   }
			   }
			}
		  }
		  catch (e)
		  {
			alert(e.description);
		  }
	  }
}

function xTableAppendRowMergeRow(thobj,xpath,crows,rows,cols,premerge)
{
	var prerownum = crows-rows;
	  if ((crows-rows)>0)
	  {
			TableAppendRow1(thobj,tFname[0]+(crows-rows));
	  }

	  TableSplitCellRow5(thobj,crows,rows,cols-1);

// 테이블 값넣기
		for (k=(crows-rows)+1 ;k<=crows ;k++ )
		{
			//k=(crows-rows)+1;

		  try
		  {
			var oNodes = xmldom.selectNodes(xpath+"["+(k-1)+"]");
			for (i=0; i<oNodes.length; i++)
			{
			   oNode = oNodes.nextNode;
			   if (oNode != null)
			   {

	//			  alert("Node ("+i+"), <"+ oNode.nodeName + ">:\r\n" +oNode.xml);
	//			  alert(oNode.childNodes.length);
				   for( j = 0 ; j< oNode.childNodes.length ; j++) {

						var mmerge=false;
						//alert( "[SetTAB]"+ oNode.childNodes(j).nodeName );

						var attrs = oNode.childNodes(j).attributes;
						for(var l = 0 ; l<attrs.length ; l++){
							var attr=attrs.item(l);
							if (attr.nodeName == "M" && attr.nodeValue == "1")
							{
								mmerge = true;
							}
							//alert(attr.nodeName + " : " + attr.nodeValue + "<br>");
						}
			   			PutFieldText (thobj,oNode.childNodes(j).nodeName+k,oNode.childNodes(j).text);

				   }	//for


						if (premerge ==  true && k== crows ) // 해당 그룹의 마지막 데이터 인경우만 수행
						{
							j=0;
									_Fctl = thobj.MoveToField(oNode.childNodes(j).nodeName + prerownum);
									if ( _Fctl == true) {
										thobj.Run("TableCellBlock");
										thobj.Run("TableCellBlockExtend");
										for ( m=1;m<cols ;m++ )
										{
											thobj.Run("TableRightCell");
										}
										thobj.Run("TableMergeCell");
									}
						}
			   }
			}
		  }
		  catch (e)
		  {
			alert(e.description);
		  }
	  }
}

function FindAttribute (attrs, nname, nvalue)
{

	var mFind=false;

	for(var n = 0 ; n < attrs.length ; n++){
		var attr=attrs.item(n);
		if (attr.nodeName == nname && attr.nodeValue == nvalue )  {	mFind = true; 	}
	}

	return(mFind);
}



function xTableAppendRow_01(thobj,xpath,cols,nomerge,mtype,findrows)
{
//	alert(findrows);
	if (nomerge == null)	nomerge = 0;
	if (mtype == null)	mtype = '';
//	if (findrows == null)	findrows = 0;

	var tAttr = new Array();
	var tAttr2 = new Array();
	var row_1 = false;
	var row_2 = false;

	  try
	  {
		var oNodes = xmldom.selectNodes(xpath);

		for (row =1; row <= oNodes.length ; row++)
		{
		   oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
			   if( row > 1 ) {
						alert("[행찾기]"+tFname[0]+ (row-1));
						// 새로운 행 추가
						TableAppendRowSetFieldName(thobj,tFname[0]+ (row-1) , row , cols);

				}

			   for( col = 1 ; col <= oNode.childNodes.length ; col++) {
			   		PutFieldText (thobj,oNode.childNodes(col-1).nodeName+row,oNode.childNodes(col-1).text);
			   }	//for
		   }
		}
	  }
	  catch (e)
	  {
		alert("[xTableAppendRow_01]"+e.description);
	  }
}



///////// 테이블 행추가,행병합
function xTableAppendRowMerge(thobj,xpath,crows,rows,cols)
{
	  if ((crows-rows)>0)
	  {
			TableAppendRow1(thobj,tFname[0]+(crows-rows));
	  }

	  TableSplitCellRow5(thobj,crows,rows,cols-1);

// 테이블 값넣기
		for (k=(crows-rows)+1 ;k<=crows ;k++ )
		{
			//k=(crows-rows)+1;

		  try
		  {
			var oNodes = xmldom.selectNodes(xpath+"["+(k-1)+"]");
			for (i=0; i<oNodes.length; i++)
			{
			   oNode = oNodes.nextNode;
			   if (oNode != null)
			   {
				   for( j = 0 ; j< oNode.childNodes.length ; j++) {

						var mmerge=false;
						//alert( "[SetTAB]"+ oNode.childNodes(j).nodeName );

						var attrs = oNode.childNodes(j).attributes;
						for(var l = 0 ; l<attrs.length ; l++){
							var attr=attrs.item(l);
							if (attr.nodeName == "M" && attr.nodeValue == "1")
							{
								mmerge = true;
							}
							//alert(attr.nodeName + " : " + attr.nodeValue + "<br>");
						}

						if (mmerge ==  true)
						{
							if ( k != (crows-rows+1) )
							{

								if (thobj.GetFieldText(oNode.childNodes(j).nodeName+(k-1)) == Convert(oNode.childNodes(j).text) )
								{
									_Fctl = thobj.MoveToField(oNode.childNodes(j).nodeName+k);
									if ( _Fctl == true) {
										thobj.MovePos(102);
										thobj.Run("TableCellBlock");
										thobj.Run("TableCellBlockExtend");
										thobj.Run("TableLowerCell");
										thobj.Run("TableMergeCell");
										thobj.RenameField(oNode.childNodes(j).nodeName+(k-1),oNode.childNodes(j).nodeName+(k));
									}	//
								}
							}
						}
			   			PutFieldText (thobj,oNode.childNodes(j).nodeName+k,oNode.childNodes(j).text);
				   }
			   }
			}
		  }
		  catch (e)
		  {
			alert(e.description);
		  }
	  }

}


///////// 테이블 행추가,행병합
function xTableAppendRowMerge1(thobj,xpath,crows,rows,cols)
{
	  if ((crows-rows)>0)
	  {
			TableAppendRow1(thobj,tFname[0]+(crows-rows));
	  }

	  TableSplitCellRow5(thobj,crows,rows,cols-1);

// 테이블 값넣기
		for (k=(crows-rows)+1 ;k<=crows ;k++ )
		{
			//k=(crows-rows)+1;

		  try
		  {
			var oNodes = xmldom.selectNodes(xpath+"["+(k-1)+"]");
			for (i=0; i<oNodes.length; i++)
			{
			   oNode = oNodes.nextNode;
			   if (oNode != null)
			   {
				   for( j = 0 ; j< oNode.childNodes.length ; j++) {

						var mmerge=false;
						//alert( "[SetTAB]"+ oNode.childNodes(j).nodeName );

						var attrs = oNode.childNodes(j).attributes;
						for(var l = 0 ; l<attrs.length ; l++){
							var attr=attrs.item(l);
							if (attr.nodeName == "M" && attr.nodeValue == "1")
							{
								mmerge = true;
							}
						}

						if (mmerge ==  true)
						{
							if ( k != (crows-rows+1) )
							{
								if (thobj.GetFieldText(oNode.childNodes(j).nodeName+(k-1)) == Convert(oNode.childNodes(j).text) )
								{
									_Fctl = thobj.MoveToField(oNode.childNodes(j).nodeName+k);
									if ( _Fctl == true) {
										thobj.MovePos(102);
										thobj.Run("TableCellBlock");
										thobj.Run("TableCellBlockExtend");
										thobj.Run("TableLowerCell");
										thobj.Run("TableMergeCell");
										thobj.RenameField(oNode.childNodes(j).nodeName+(k-1),oNode.childNodes(j).nodeName+(k));

										thobj.MovePos(101);
										thobj.Run("TableCellBlock");
										thobj.Run("TableCellBlockExtend");
										thobj.Run("TableLowerCell");
										thobj.Run("TableMergeCell");

									}	//
								}
							}

						}
			   			PutFieldText (thobj,oNode.childNodes(j).nodeName+k,oNode.childNodes(j).text);
				   }
			   }
			}
		  }
		  catch (e)
		  {
			alert(e.description);
		  }
	  }
}

function xTableAppendRowMerge2(thobj,xpath,crows,rows,cols)
{
	  if ((crows-rows)>0)
	  {
			TableAppendRow1(thobj,tFname[0]+(crows-rows));
	  }

	  TableSplitCellRow5(thobj,crows,rows,cols-1);

		for (k=(crows-rows)+1 ;k<=crows ;k++ )
		{
		  try
		  {
			var oNodes = xmldom.selectNodes(xpath+"["+(k-1)+"]");
			for (i=0; i<oNodes.length; i++)
			{
			   oNode = oNodes.nextNode;
			   if (oNode != null)
			   {
				   // 라인 그리기 위에다가 (
				   var mline=false;
					var attrs = oNode.attributes;
					for(var l = 0 ; l<attrs.length ; l++){
						var attr=attrs.item(l);
						if (attr.nodeName == "type" && attr.nodeValue == "0")
						{
							mline = true;
						}
					}
				   for( j = 0 ; j< oNode.childNodes.length ; j++) {
						var mmerge=false;
						//alert( "[SetTAB]"+ oNode.childNodes(j).nodeName );
						var attrs = oNode.childNodes(j).attributes;
						for(var l = 0 ; l<attrs.length ; l++){
							var attr=attrs.item(l);
							if (attr.nodeName == "M" && attr.nodeValue == "1")
							{
								mmerge = true;
							}
							//alert(attr.nodeName + " : " + attr.nodeValue + "<br>" + mmerge);
						}
						if (mmerge ==  true)
						{
							if ( k > 1 )  // 2번째 값부터 비교
							{
								if (oNode.childNodes(j).text != "")		// 값이 ""이 아닌 경우만
								{
									if (thobj.GetFieldText(oNode.childNodes(j).nodeName+(k-1)) == Convert(oNode.childNodes(j).text) )
									{
										_Fctl = thobj.MoveToField(oNode.childNodes(j).nodeName+k);
										if ( _Fctl == true) {
											thobj.MovePos(102);
											thobj.Run("TableCellBlock");
											thobj.Run("TableCellBlockExtend");
											thobj.Run("TableLowerCell");
											thobj.Run("TableMergeCell");
											thobj.RenameField(oNode.childNodes(j).nodeName+(k-1),oNode.childNodes(j).nodeName+(k));
										}	//
									}
								}
							}

						}
						PutFieldText (thobj,oNode.childNodes(j).nodeName+k,oNode.childNodes(j).text);

						if (mline == true && k >0 )
						{
							_Fctl = thobj.MoveToField(oNode.childNodes(j).nodeName+(k-1));
							if ( _Fctl == true) {
								  //alert(k + " : _Fctl " + oNode.childNodes(j).nodeName+k );

								  var act = thobj.CreateAction("CellBorderFill"); //CellBorderFill
								  var set = act.CreateSet();
								  act.GetDefault(set);
								  set.SetItem("BorderTypeBottom", 1);
								  set.SetItem("BorderWidthBottom", 1);
								  act.Execute(set);
							}
						} else {
							_Fctl = thobj.MoveToField(oNode.childNodes(j).nodeName+(k-1));
							if ( _Fctl == true) {
								  var act = thobj.CreateAction("CellBorderFill"); //CellBorderFill
								  var set = act.CreateSet();
								  act.GetDefault(set);
								  set.SetItem("BorderTypeBottom", 0);
								  act.Execute(set);
							}
						}
				   }
			   }
			}
		  }
		  catch (e)
		  {
			alert(e.description);
		  }
	  }
}


///////// 테이블 행추가,
function xTableAppendRow_F(thobj,xpath,crows,rows,cols)
{
	  if ((crows-rows)>0)
	  {
			TableAppendRow2(thobj,tFname[0]+(crows-rows));
	  }

//	  RenameTableCell(thobj,crows);
	  TableSplitCellRow5(thobj,crows,rows,cols-1);

// 테이블 값넣기
		for (k=(crows-rows)+1 ;k<=crows ;k++ )
		{
			//k=(crows-rows)+1;
		  try
		  {
			var oNodes = xmldom.selectNodes(xpath+"["+(k-1)+"]");
			for (i=0; i<oNodes.length; i++)
			{
			   oNode = oNodes.nextNode;
			   if (oNode != null)
			   {
				   for( j = 0 ; j< oNode.childNodes.length ; j++) {
						//alert( "[SetTAB]"+ oNode.childNodes(j).nodeName );
			   			PutFieldText (thobj,oNode.childNodes(j).nodeName+k,oNode.childNodes(j).text);
				   }
			   }
			}
		  }
		  catch (e)
		  {
			alert(e.description);
		  }
	  }
}

///// 테이블 정보 설정
function SetTable(thobj,xpath,ColName,cols)
{

	if (ColName == null || ColName == '') {
		// xml정의
		try
		{
			var oNodes = xmldom.selectNodes(xpath);
			for (i=0; i<oNodes.length; i++)
			{
			   oNode = oNodes.nextNode;
			   if (oNode != null)
			   {
				   for( j = 0 ; j< oNode.childNodes.length ; j++) {
						//alert( "[SetTAB]"+ oNode.childNodes(j).nodeName );
						tFname[j]=oNode.childNodes(j).nodeName;
				   }
			   }
			}
		}
		catch (e)
		{
			alert("[SetTable(xml)]"+e.description);
		}
	} else {
		// 파일정의
		if (thobj.MoveToField(ColName, false, false, false))
		{
		   for( j = 0 ; j< cols; j++) {
			   tFname[j] = thobj.GetCurFieldName();
			   thobj.SetCurFieldName(tFname[j] + "1");
			   thobj.MovePos(101);
		   }
		}
	}
}

////IBSheet 에서 문서 통합
function SetTextFileDOM(hobj,xpath,pos,hwptext,fmt,type)
{
	if (pos != "")
	{
		hobj.movepos(pos);
	}

	hwptext = GetXMLDOM(xpath);

	if (hwptext != "")
	{
		hobj.SetTextFile(hwptext,fmt, type);
	} else {
		//hobj.SetTextFile(hobj.GetTextFile("HWP",""),fmt, type);
	}
}

////IBSheet 에서 문서 통합(필드찾아서 가기)
function SetTextFileDOMField(hobj,xpath,fieldnm,hwptext,fmt,type)
{
	if (fieldnm != "")
	{
		hobj.MoveToField(fieldnm);
	}

	hwptext = GetXMLDOM(xpath);

	if (hwptext != "")
	{
		hobj.SetTextFile(hwptext,fmt, type);
	} else {
		//hobj.SetTextFile(hobj.GetTextFile("HWP",""),fmt, type);
	}
}

////IBSheet 에서 문서 통합
function GetXMLDOM(xpath)
{
	  try
	  {
		var oNodes = xmldom.selectNodes(xpath);
		//alert(oNodes.length);
		for (i=0; i<oNodes.length; i++)
		{
		   oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
			   for( j = 0 ; j< oNode.childNodes.length ; j++) {

					return oNode.childNodes(j).text;
			   }
		   }
		}
	  }
	  catch (e)
	  {
		alert(e.description);
	  }
}


/************** xmldom 함수 **********************/
function CreateDOM(xmldata)
{
    xmldom = MakeDOM(null,xmldom);

	// 데이터 가져오기
    xmldom.loadXML(xmldata);

    if(xmldom.parseError.errorCode != 0) {
		alert("[CreateDOM]errorCode: " + xmldom.parseError.errorCode + "\n" +
			"filepos: " + xmldom.parseError.filepos + "\n" +
			"line: " + xmldom.parseError.line + "\n" +
			"linepos: " + xmldom.parseError.linepos + "\n" +
			"reason: " + xmldom.parseError.reason + "\n" +
			"srcText: " + xmldom.parseError.srcText + "\n" +
			"url: " + xmldom.parseError.url);
		return (false);
	} else {
		return (true);
	}
}

function MakeDOM(progID,txmldom)
{
  if (progID == null) {
    progID = "msxml2.DOMDocument"; //"msxml2.DOMDocument.3.0"
  }

  try {
    txmldom = new ActiveXObject(progID);
  }
  catch (e) {
    alert(e.description);
  }
  return txmldom;
}

function DestoryDOM()
{
    xmldom = null;
}


function Docend(hobj)
{
		Run(hobj, "ViewZoomFitWidth");
		DeleteCtrl(hobj,'%clk');
		//Movepos(hobj, '2');
		hobj.Movepos(2);
}

function Docend_eipms(hobj)
{
		Run(hobj, "ViewZoomFitWidth");
		hobj.Movepos(2);
}

/*******************************************************************
일반 함수
*******************************************************************/

function Convert(strV)
{
	try {
		var strVt ,re ;

		strVt = strV;
		re = /\n/g ;             // 줄바꿈
		strVt = strV.replace(re, "\r\n");    //줄바꿈 적용
		re = /&#([0-9]*);/g ;             // 줄바꿈
		strVt = strVt.replace(re,
			  function($0,$1,$2) {
				var tmp;
				tmp = String.fromCharCode($1);
	//			alert(tmp);
				return(tmp);
			  }
		);
		return(strVt);
	} catch(e) {
		return(strV);
	}
}

function GetTime(){
   var d, s, t;
   var MinMilli = 1000 * 60;
   var HrMilli = MinMilli * 60;
   var DyMilli = HrMilli * 24;
   d = new Date();
   t = d.getTime();
//   s = "70/1/1 이후로 "
//   s += Math.round(t / DyMilli) + " 일이 지났습니다.";수
   return(t);
}


///표 여러개 나올때 간격조절 함
function MoveDocEnd(){
	hobj.Run("MoveDocEnd");
	hobj.Run("BreakPara");
}


///////////////////////////////////////////////목차 만들기 함수 시작///////////////////////////////////
// 현재 페이지 정보
function h_PageInfo(hobj)
{
	var vact = hobj.CreateAction("DocumentInfo");
	var set = vact.CreateSet();
	vact.GetDefault(set);
	set.SetItem("DetailInfo", 1);
	vact.Execute(set);
	var getItem = set.Item("DetailCurPage") -2;
	return getItem;
}

var vBText = new Array();
var vSText = new Array();
var vBpage = new Array();
var vSpage = new Array();

var vArrayField = new Array();
var vArrayB = new Array();
var vArrayN = new Array();
var vArrayS = new Array();
var vArrayP = new Array();
var vArrayS_ix = new Array();

function h_MakeIndex(hobj,k, l,hwpfile1,hwpfile2)
{
	hobj.MoveToField("목차내용");

	for (var i = 0; i < k; i++)
	{
		if (ReportLayoutType ) {
			hobj.Insert(hosthref + "hwpFiles/"+hwpfile1+".hwp", "HWP", "template:true");
		} else {
			hobj.Insert(hosthref + hwpfile1+".hwp", "HWP", "template:true");
		}
		hobj.PutFieldText("대제목_목차{{"+i+"}}", vArrayN[i] + ". " + vArrayB[i]);
		hobj.MovePos(23);
		var vact = hobj.CreateAction("InsertText");
		var vset = vact.CreateSet();

		vact.GetDefault(vset);
		vset.SetItem("Text", ""+vBpage[i]);

		vact.Execute(vset);
		hobj.Run("BreakLine");

		for (var j = 0; j < l ; j++ )
		{
			if (vArrayS_ix[j] == i+1)
			{
				if (ReportLayoutType ) {
					hobj.Insert(hosthref + "hwpFiles/"+hwpfile2+".hwp", "HWP", "template:true");
				} else {
					hobj.Insert(hosthref + hwpfile2+".hwp", "HWP", "template:true");
				}

				hobj.PutFieldText("소제목_목차{{"+j+"}}", vArrayS[j]);
				hobj.MovePos(23);
				var vact = hobj.CreateAction("InsertText");
				var vset = vact.CreateSet();
				vact.GetDefault(vset);

				vset.SetItem("Text", ""+vSpage[j]);
				vact.Execute(vset);
				hobj.Run("BreakLine");
			}
		}
	}
}

function h_OnMakeContent(hobj,hwpfile1,hwpfile2)
{
	var bBField = 1;
	var bSField = 1;

	// 대분류 개수 구하기
	var vIndex = 0;
	var vTemp = 0;
	var i = 0;
	var j = 0;
	var k = 0;
	var l = 0;

	var vFieldList = hobj.GetFieldList(1,2);
	if (vFieldList.length == 0) return 0;

	//logs(hobj,FieldList.length + " : " + vFieldList);
	for(var a = 0; a < vFieldList.length; a++)
	{
		if(vFieldList.charCodeAt(a) == 0x02)
		{
			vArrayField[vIndex] = vFieldList.substring(vTemp, a);
			vTemp = a+1;	// 0x02 넘어서기

			if (vArrayField[vIndex] == "대제목{{"+k+"}}")
			{
				hobj.MoveToField("대제목{{"+k+"}}", true, true, true);
				vArrayB[k] = hobj.GetFieldText("대제목{{"+k+"}}");
				vArrayN[k] = hobj.GetFieldText("카운트{{"+k+"}}");

				//logs(hobj,vArrayN[k]);
				hobj.MovePos(20);
				vBpage[k] = h_PageInfo(hobj);

				k = k+1;

			} else if (vArrayField[vIndex] == "소제목{{"+l+"}}")
			{
				hobj.MoveToField("소제목{{"+l+"}}", true, true, true);
				vArrayS[l] = hobj.GetFieldText("소제목{{"+l+"}}");
				vSpage[l] = h_PageInfo(hobj);

				vArrayS_ix[l] = k;
				l = l+1;
			}
			vIndex++;
		}
	}
	hobj.Run("MoveTopLevelEnd");
	h_MakeIndex(hobj,k, l,hwpfile1,hwpfile2);
}

///////////////////////////////////////////////목차 만들기 함수 끝///////////////////////////////////

///////////////////////////////////////////////서로 다른 테이블 합치기 시작///////////////////////////////////

function h_TableMerge(hobj,type,field)
{

	var _Fctl = hobj.MoveToField(field);
	if (type == 0){
		hobj.MoveToField(field);
	}else if (type == 1){
		//자체평가계획 중 RD 사업에서 성과및 지표 표에서 합계부분 머지하기 위한 특별한 유형
		hobj.Run("MoveUp");
	   hobj.Run("TableMergeTable");
　	}

	hobj.Run("MoveUp");
   hobj.Run("TableMergeTable");

}

///////////////////////////////////////////////서로 다른 테이블 합치기 끝///////////////////////////////////

function xTableAppendRowGetSub_03(thobj,xpath,cols,xpathSub,Filter,Filter2)
{

	var tAttr = new Array();
	var tAttr2 = new Array();
	var row_1 = false;
	var row_2 = false;
	var subFiler = xpathSub+"["+Filter+"='";
	var BONUS_YN="";

	  try
	  {
		var oNodes = xmldom.selectNodes(xpath);

		for (row =1; row <= oNodes.length ; row++)
		{
		   var oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
			   if( row > 1 ) {
						//alert("[행찾기]"+tFname[0]+ (row-1));
						// 새로운 행 추가
						TableAppendRowSetFieldName(thobj,tFname[0]+ (row-1) , row , cols);
				}

			   for( col = 1 ; col <= oNode.childNodes.length ; col++) {

				   if (oNode.childNodes(col-1).nodeName == "BONUS_YN"){
						BONUS_YN = Convert(oNode.childNodes(col-1).text);
					}
		   			PutFieldText (thobj,oNode.childNodes(col-1).nodeName+(row),oNode.childNodes(col-1).text);

					if (oNode.childNodes(col-1).nodeName == Filter) { 	subFiler =xpathSub+"["+Filter+"='"+oNode.childNodes(col-1).text+"']" ;}
					if (oNode.childNodes(col-1).nodeName == Filter2) { 	subFiler =subFiler+"["+Filter2+"='"+oNode.childNodes(col-1).text+"']" ;}
			   }	//for

			   ////////////////// 한행 추가 후 서브 확인

			  try
			  {
				var oNodesSub = xmldom.selectNodes(subFiler);
				var hAct = null;
				var hSet = null;
				//alert(oNodesSub.length);
				if (oNodesSub.length > 0) {
				   		_Fctl = thobj.MoveToField("CHARGE_POSITION"+(row));

				   		if ( _Fctl == true) {
									//thobj.MovePos(102);	// 위로
									thobj.Run("TableCellBlock");
									thobj.Run("TableCellBlockExtend");
									thobj.Run("TableRightCell");
									thobj.Run("TableRightCell");
									hAct = thobj.CreateAction("TableSplitCell");
 									hSet = hAct.CreateSet();

 									hAct.GetDefault(hSet);

								 hSet.SetItem("DistributeHeight", 1);
								 hSet.SetItem("Cols", 0);
								 hSet.SetItem("Rows", oNodesSub.length);
								 hAct.Execute(hSet);

								 thobj.Run("Cancel");
					}
				}
				for (rowSub =1; rowSub <= oNodesSub.length ; rowSub++)
				{
				   oNodeSub = oNodesSub.nextNode;
				   if (oNodeSub != null)
				   {
			   				if (rowSub == 1) {
	//							alert(oNodeSub.text);
							   for( col = 1 ; col <= oNodeSub.childNodes.length ; col++) {

									if (_UNICODE == true)
									{
										_Fctl = thobj.MoveToField(oNodeSub.childNodes(col-1).nodeName+(row));
										if ( _Fctl == true) {thobj.SetTextFile(thobj.GetFieldText(oNodeSub.childNodes(col-1).nodeName+(row))+Convert(oNodeSub.childNodes(col-1).text),"UNICODE", "insertfile");}
									} else {
										_Fctl = thobj.MoveToField(oNodeSub.childNodes(col-1).nodeName+(row));
										//alert("FctlName-"+oNodeSub.childNodes(col-1).nodeName+(row));
										if ( _Fctl == true) {
											//alert(thobj.GetFieldText(oNodeSub.childNodes(col-1).nodeName+(rowSub))+Convert(oNodeSub.childNodes(col-1).text));
											thobj.PutFieldText(oNodeSub.childNodes(col-1).nodeName+(row),Convert(oNodeSub.childNodes(col-1).text));
										}
									}

							   }	//for
									if (BONUS_YN == "N" ) {
							   		_Fctl = thobj.MoveToField("REQUEST_BONUS_AMT"+(row));

								   		if ( _Fctl == true) {
								   					for(col=1; col<rowSub;col++){
														thobj.MovePos(103);	// 아래로
													}
													thobj.Run("TableCellBlock");
													thobj.Run("TableDeleteCell");
													thobj.Run("Cancel");
													//InsertText(thobj,"aaaaaaaaaa"+rowSub);
										}
							   		}
							} else {

								///////////////// 찾아가서 찍기

						   		_Fctl = thobj.MoveToField("CHARGE_POSITION"+(row));

						   		if ( _Fctl == true) {
						   					for(col=1; col<rowSub;col++){
												thobj.MovePos(103);	// 아래로
											}
											InsertText(thobj,Convert(oNodeSub.childNodes(6).text));
											thobj.MovePos(101);	// 오른쪽
											InsertText(thobj,Convert(oNodeSub.childNodes(7).text));
											thobj.MovePos(101);	// 오른쪽
											InsertText(thobj,Convert(oNodeSub.childNodes(8).text));
								}

								//BONUS_YN 값이 Y 일때만
								//alert("BONUS_YN -> " + BONUS_YN);
								if (BONUS_YN == "Y" ) {
						   		_Fctl = thobj.MoveToField("REQUEST_BONUS_AMT"+(row));

							   		if ( _Fctl == true) {
							   					for(col=1; col<rowSub;col++){
													thobj.MovePos(103);	// 아래로
												}
												InsertText(thobj,Convert(oNodeSub.childNodes(12).text));
									}
								}else{
									//alert("rt");
						   		_Fctl = thobj.MoveToField("REQUEST_BONUS_AMT"+(row));

							   		if ( _Fctl == true) {
							   					for(col=1; col<rowSub;col++){
													thobj.MovePos(103);	// 아래로
												}
												InsertText(thobj,"PPPPPPPPPPP ");
									}
								}
							}
					   ////////////////// 한행 추가 후 서브 확인
				   }
				}
			  }
			  catch (e)
			  {
				alert("[xTableAppendRowGetSub_01]"+e.description);
			  }
		   }
		}
	  }
	  catch (e)
	  {
		alert("[xTableAppendRowGetSub_03]"+e.description);
	  }
}

function brs210(hobj,thobj,xpath)
{

	var oNodes2 = xmldom.selectNodes(xpath);
	for (ii2=0; ii2<oNodes2.length; ii2++)
	{
	   oNode = oNodes2.nextNode;
	   if (oNode != null)
	   {
				if ( Getxmldom(oNodes2.item(ii2).xml,'FIS_BRANCH') != "" ) {
					InsertDoc(thobj,'3',"brs210.hwp",'HWP','');
					xPutFieldText (thobj,'//Report/list',ii2,'0','IPYO0');

					SetTextFile (hobj,thobj,'3','','HWP','insertfile');

					xPutFieldText_SetTextFile (hobj,thobj,'//Report/Sub/list','1','brs210_01.hwp','[NO=1]','brs210_03.hwp','[NO>1]','REDUCTION_TYPE_CD');
					Run(hobj,'BreakPage');
				}
	   }
	}

}


function brs310(hobj,thobj,xpath)
{

	var oNodes2 = xmldom.selectNodes(xpath);
	for (ii2=0; ii2<oNodes2.length; ii2++)
	{
	   oNode = oNodes2.nextNode;
	   if (oNode != null)
	   {
	   		try {

				InsertDoc(thobj,'3',"brs310.hwp",'HWP','');
				xPutFieldText (thobj,'//Report/list',ii2);
				SetTextFile (hobj,thobj,'3','','HWP','insertfile');

				Run(hobj,'BreakPage');
			} catch(e) {
				//InsertText(hobj,"[Error] => " + e.Description);
			}

	   }
	}

}


function brs410_2(hobj,thobj,xpath)
{

	var oNodes2 = xmldom.selectNodes(xpath);
	for (ii2=0; ii2<oNodes2.length; ii2++)
	{
	   oNode = oNodes2.nextNode;
	   if (oNode != null)
	   {
	   		try {
				InsertDoc(thobj,'3',"brs410_2.hwp",'HWP','');
				xPutFieldText (thobj,'//Report/list',ii2);
				xTableAppendRowGetSub (thobj,'//Report/list['+ii2+']',8,'//Report/Sub1/list','CUR_DEPT_NM','IPYO0','DBIZ_CD');

				SetTextFile (hobj,thobj,'3','','HWP','insertfile');

				Run(hobj,'BreakPage');
			} catch(e) {
				//InsertText(hobj,"[Error] => " + e.Description);
			}

	   }
	}

}


function brs420(hobj,thobj,xpath)
{

	var oNodes2 = xmldom.selectNodes(xpath);
	for (ii2=0; ii2<oNodes2.length; ii2++)
	{
	   oNode = oNodes2.nextNode;
	   if (oNode != null)
	   {
	   		try {

				InsertDoc(thobj,'3',"brs420.hwp",'HWP','');
				xPutFieldText (thobj,'//Report/list',ii2);
				xPutFieldText_Getsub (thobj,'//Report/list['+ii2+']',5,'//Report/Sub1/list','CUR_DEPT_NM','IPYO0','DBIZ_CD');
				SetTextFile (hobj,thobj,'3','','HWP','insertfile');

				Run(hobj,'BreakPage');
			} catch(e) {
				//InsertText(hobj,"[Error] => " + e.Description);
			}

	   }
	}

}


function brs410(hobj,thobj,xpath)
{

	var oNodes2 = xmldom.selectNodes(xpath);
	for (ii2=0; ii2<oNodes2.length; ii2++)
	{
	   oNode = oNodes2.nextNode;
	   if (oNode != null)
	   {
	   		try {

				InsertDoc(thobj,'3',"brs410.hwp",'HWP','');
				xPutFieldText (thobj,'//Report/list',ii2);
				xTableAppendRowGetSub (thobj,'//Report/list['+ii2+']',8,'//Report/Sub1/list','CUR_DEPT_NM','IPYO0','DBIZ_CD');
				xPutFieldText_Listsub (thobj,'//Report/list['+ii2+']','//Report/Sub2/list','REF_DEPT_NM','REF_DEPT_OPINION','IPYO0','DBIZ_CD');
				SetTextFile (hobj,thobj,'3','','HWP','insertfile');

				Run(hobj,'BreakPage');
			} catch(e) {
				//InsertText(hobj,"[Error] => " + e.Description);
			}

	   }
	}

}


function Getxmldom(xmldata,tagname)
{
	try {
		var tmpstr = xmldata;
		xmldom2 = MakeDOM(null,xmldom2);
		xmldom2.loadXML(tmpstr);
		tmpstr = xmldom2.getElementsByTagName(tagname).item(0).text;

	} catch(e) {
			tmpstr = "";
		//InsertText(hobj,"[Error] => " + e.Description);
	}
	return tmpstr;
}


//////////////////////////////////일반 한글 명령어

function Run(hobj,action){
	hobj.Run(action);
}


function Movepos(hobj,action){
	hobj.movepos(action);
}



function xTableAppendRowGetSub(thobj,xpath,cols,xpathSub,Fieldnm,Filter,Filter2)
{

	var tAttr = new Array();
	var tAttr2 = new Array();
	var Fname = new Array();
	var row_1 = false;
	var row_2 = false;
	var subFiler = "";

	  try
	  {
		var oNodes = xmldom.selectNodes(xpath);
		for (row =1; row <= 1 ; row++)
		{
		   var oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
			   for( col = 1 ; col <= oNode.childNodes.length ; col++) {
					if (oNode.childNodes(col-1).nodeName == Filter) { 	subFiler =xpathSub+"["+Filter+"='"+oNode.childNodes(col-1).text+"']" ;}
					if (oNode.childNodes(col-1).nodeName == Filter2) { 	subFiler =subFiler+"["+Filter2+"='"+oNode.childNodes(col-1).text+"']" ;}
			   }

							   if (row ==1 ) {
									try
									{
											if (thobj.MoveToField(Fieldnm, false, false, false)){
											for (x=0;x<=cols;x++) {
//												logs(thobj,thobj.GetCurFieldName());
												Fname[x] = thobj.GetCurFieldName();
												thobj.MovePos(101);
											}
										}
									}catch (e){}
							  }
		   }
		}

		var oNodes = xmldom.selectNodes(subFiler);
		if (oNodes.length == 0){
			thobj.Run("TableDeleteRow");
		}

		for (row =1; row <= oNodes.length ; row++)
		{
		   var oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
		   		//logs(thobj,Fname[0]+ (row-1));
			   if( row > 1 ) {
					// 새로운 행 추가
					if (thobj.MoveToField(Fname[0]+ (row-1), false, false, false)){
						thobj.Run("TableCellBlock");
						thobj.Run("TableColPageDown");
						thobj.Run("Cancel");
						thobj.MovePos(107);   // 현재 캐럿이 위치한 셀에서 열의 끝으로 이동
						thobj.Run("TableAppendRow");
						thobj.SetCurFieldName(Fname[0] + row);
					}
				}
				if (thobj.MoveToField(Fname[0]+ row, false, false, false)){
						for (x=0;x<Fname.length;x++) {
							thobj.SetCurFieldName(Fname[x] + row);
							InsertText(thobj,Getxmldom(oNodes.item(row-1).xml,Fname[x]));
							thobj.MovePos(101);
						}
				}
		   }
		}
	  } catch (e) {
		alert("[xTableAppendRowGetSub_03]"+e.description);
	  }

}


function xPutFieldText_Getsub(thobj,xpath,cols,xpathSub,Fieldnm,Filter,Filter2)
{

	var tAttr = new Array();
	var tAttr2 = new Array();
	var Fname = new Array();
	var row_1 = false;
	var row_2 = false;
	var subFiler = "";

	  try
	  {
		var oNodes = xmldom.selectNodes(xpath);
		for (row =1; row <= 1 ; row++)
		{
		   var oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
			   for( col = 1 ; col <= oNode.childNodes.length ; col++) {
					if (oNode.childNodes(col-1).nodeName == Filter) { 	subFiler =xpathSub+"["+Filter+"='"+oNode.childNodes(col-1).text+"']" ;}
					if (oNode.childNodes(col-1).nodeName == Filter2) { 	subFiler =subFiler+"["+Filter2+"='"+oNode.childNodes(col-1).text+"']" ;}
			   }
							   if (row ==1 ) {
									try
									{
											if (thobj.MoveToField(Fieldnm, false, false, false)){
											for (x=0;x<cols;x++) {
												Fname[x] = thobj.GetCurFieldName();
												thobj.MovePos(101);
											}
										}
									}catch (e){}
							  }
		   }
		}

		var oNodes = xmldom.selectNodes(subFiler);
		for (row =1; row <= oNodes.length ; row++)
		{
		   var oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
					for (x=0;x<Fname.length;x++) {
						thobj.PutFieldText(Fname[x] + row,Getxmldom(oNodes.item(row-1).xml,Fname[x]));
					}
		   }
		}
		var oNodes = xmldom.selectNodes(subFiler);
	  }  catch (e)  {
		alert("[xTableAppendRowGetSub_03]"+e.description);
	  }

}

function xPutFieldText_Listsub(thobj,xpath,xpathSub,Fieldnm,Fieldnm2,Filter,Filter2)
{

	var tAttr = new Array();
	var tAttr2 = new Array();
	var Fname = new Array();
	var row_1 = false;
	var row_2 = false;
	var subFiler = "";
	var Fieldnm2txt = "false";

	  try
	  {
		var oNodes = xmldom.selectNodes(xpath);
		for (row =1; row <= 1 ; row++)
		{
		   var oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
			   for( col = 1 ; col <= oNode.childNodes.length ; col++) {
					if (oNode.childNodes(col-1).nodeName == Filter) { 	subFiler =xpathSub+"["+Filter+"='"+oNode.childNodes(col-1).text+"']" ;}
					if (oNode.childNodes(col-1).nodeName == Filter2) { 	subFiler =subFiler+"["+Filter2+"='"+oNode.childNodes(col-1).text+"']" ;}
			   }
							   if (row ==1 ) {
									try
									{
											thobj.MoveToField(Fieldnm, false, false, false);
									}catch (e){}
							  }
		   }
		}

		var oNodesub = xmldom.selectNodes(subFiler);
		for (xx=0; xx<oNodesub.length; xx++){
		   oNode = oNodesub.nextNode;
		   if (oNode != null) {
			   for( col = 1 ; col <= oNode.childNodes.length ; col++) {
				   if (oNode.childNodes(col-1).nodeName == Fieldnm){
				   		if (Fieldnm2txt == "false"){
							InsertText(thobj,"【" + Convert(oNode.childNodes(col-1).text) + "】\r\n");
						}else{
							InsertText(thobj,"\r\n【" + Convert(oNode.childNodes(col-1).text) + "111】\r\n");
						}
					}

				   if (oNode.childNodes(col-1).nodeName == Fieldnm2){
						InsertText(thobj,Convert(oNode.childNodes(col-1).text) + "\r\n");
						Fieldnm2txt = Convert(oNode.childNodes(col-1).text);
					}
					//logs(thobj,Fieldnm2txt+ "\r\n");
				}
			}
		}
	  }  catch (e)  {
		alert("[xTableAppendRowGetSub_03]"+e.description);
	  }

}


function InsertPicture(thobj,field, path)
{
	// 셀에 그림을 넣기 전에, 셀의 내용을 전부 지운다.
	thobj.MoveToField(field, true, true, false);
	thobj.Run("SelectAll");
	thobj.Run("Delete");
	thobj.MoveToField(field, true, true, false);
	thobj.InsertPicture(path, true, 2, 0, 0, 0);
}

function Nodatacell(thobj,fnm,msg){
	thobj.MoveToField(fnm);
	InsertText(thobj,msg);
}

function h_PageDef(hobj,Lmargin,Rmargin,Bmargin,Tmargin,HeaderLen,FooterLen,GutterLen){
		//각 셋팅값은 null 경우 0으로 셋팅
		/* mm 단위로 했을경우
		Lmargin = HWPUNITs(Lmargin);
		Rmargin = HWPUNITs(Rmargin);
		Bmargin = HWPUNITs(Bmargin);
		Tmargin = HWPUNITs(Tmargin);
		HeaderLen = HWPUNITs(HeaderLen);
		FooterLen = HWPUNITs(FooterLen);
		GutterLen = HWPUNITs(GutterLen);
		*/
		var act = hobj.CreateAction("PageSetup");
		var set = act.CreateSet();
		act.GetDefault(set);
		set.SetItem ("ApplyTo", 3);	  //적용범위 : 문서전체
		var pset = set.CreateItemSet("PageDef","PageDef");
		pset.SetItem ("LeftMargin",parseInt (Lmargin));
		pset.SetItem ("RightMargin",parseInt (Rmargin));
		pset.SetItem ("BottomMargin",parseInt (Bmargin));
		pset.SetItem ("TopMargin",parseInt (Tmargin));
		pset.SetItem ("HeaderLen",parseInt (HeaderLen));
		pset.SetItem ("FooterLen",parseInt (FooterLen));
		pset.SetItem ("GutterLen",parseInt (GutterLen));  //제본
		act.Execute(set);

}

function HWPUNITs(tmpint){
		// 1mm = 283.465 HWPUNITs
		var UNITs = 283.465;
		tmpint =  tmpint *  UNITs;
		tmpint = tmpint.toFixed(0);
		return tmpint;
}

//온나라국정관리시스템에서
function InitToolBarJS_ogm(hobj)
{
	hobj.SetToolBar(-1, "TOOLBAR_FORMAT");
	hobj.ShowStatusBar(1);
	hobj.ShowToolBar(true);
}

function xTableAppendRowMergeRowAll(thobj,xpath,cols,nomerge,mtype,align,bold)
{
	if (nomerge == null)	nomerge = 0;
	if (mtype == null)	mtype = '';
	if (align == null)	align = 'Left';
	if (bold == null)	bold = '0';

	var tAttr = new Array();
	var tAttr2 = new Array();
	var row_1 = false;
	var row_2 = false;
	var vAlign = "ParagraphShapeAlign"+align;

	  try
	  {
		var oNodes = xmldom.selectNodes(xpath);

		for (row =1; row <= oNodes.length ; row++)
		{
		   oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
			   if( row > 1 ) {
					// 새로운 행 추가
					TableAppendRowSetName(thobj,tFname[0]+ (row-1) , row , cols);

					if (row_1 == true) // 이전열을 머지해야 하는 경우
					{
						_Fctl = thobj.MoveToField(tFname[0] + (row-1));
						if ( _Fctl == true) {
							thobj.Run("TableCellBlock");
							thobj.Run("TableCellBlockExtend");
							for (m=1;m<(cols-nomerge) ;m++ )
							{
								thobj.Run("TableRightCell");
							}
							thobj.Run("TableMergeCell");
							thobj.Run(vAlign);

							if (bold=1){//병합 후 글자 진하기
								thobj.Run("TableCellBlock");
								thobj.Run("CharShapeBold");
							}
						}
					}

					// 마지막 행인 경우
					if (row == oNodes.length)
					{
						row_1 = FindAttribute(oNode.attributes,"M","1");
//						alert("마지막:"+row_1);
						if (row_1 == true) // 이전열을 머지해야 하는 경우
						{
							_Fctl = thobj.MoveToField(tFname[0] + (row));
							if ( _Fctl == true) {
								thobj.Run("TableCellBlock");
								thobj.Run("TableCellBlockExtend");
								for (m=1;m<(cols-nomerge) ;m++ )
								{
									thobj.Run("TableRightCell");
								}
								thobj.Run("TableMergeCell");

								thobj.Run(vAlign);

								if (bold=1){//병합 후 글자 진하기
									thobj.Run("TableCellBlock");
									thobj.Run("CharShapeBold");
								}
							}
						}
					}
				}
				// 첫번째 행부터 값 판단(다음값 판단을 위하여 설정(머지 여부)
				row_1 = FindAttribute(oNode.attributes,"M","1");

			   for( col = 1 ; col <= oNode.childNodes.length ; col++) {

					if(row == 1) {	// 첫번째 데이터인 경우
						tAttr[col] = FindAttribute(oNode.childNodes(col-1).attributes,"M","1");
						tAttr2[col] = FindAttribute(oNode.childNodes(col-1).attributes,"P","1");
					}

					var chkstr = false;
					if (tAttr[col] == true && row > 1)		// 적어도 현재값이 m=1이고 두번째 행부터
					{
						if (col == 1)
						{
							if (tAttr[col] ==  true) chkstr = true;
						} else {
							if (tAttr[col-1] ==  true ) {	// 이전꺼
									_Fctl = thobj.MoveToField(oNode.childNodes(col-2).nodeName+(row-1));
									if ( _Fctl == false) {chkstr = true;}	// 이미 병합이 되었으므로 없음
							} else  { chkstr = true; }
						}
					}

					if (chkstr ==  true )
					{
						if ( row > 1 )
						{
							if (thobj.GetFieldText(oNode.childNodes(col-1).nodeName+(row-1)) == Convert(oNode.childNodes(col-1).text) )
							{

								_Fctl = thobj.MoveToField(oNode.childNodes(col-1).nodeName+(row));
								if ( _Fctl == true) {
									thobj.MovePos(102);	// 위로
									thobj.Run("TableCellBlock");
									thobj.Run("TableCellBlockExtend");
									thobj.Run("TableLowerCell");
									thobj.Run("TableMergeCell");
									thobj.RenameField(oNode.childNodes(col-1).nodeName+(row-1),oNode.childNodes(col-1).nodeName+row);

									if (mtype != '') {	//R034.jsp를 위한 루틴 (특정열을 머지할경우)
										thobj.MovePos(105);	// 행의끝
										thobj.MovePos(100);
										thobj.Run("TableCellBlock");
										thobj.Run("TableCellBlockExtend");
										thobj.Run("TableLowerCell");
										thobj.Run("TableMergeCell");
										thobj.RenameField(oNode.childNodes(col-1).nodeName+(row-1),oNode.childNodes(col-1).nodeName+row);

										thobj.MovePos(105);	// 행의끝
										thobj.Run("TableCellBlock");
										thobj.Run("TableCellBlockExtend");
										thobj.Run("TableLowerCell");
										thobj.Run("TableMergeCell");
										thobj.RenameField(oNode.childNodes(col-1).nodeName+(row-1),oNode.childNodes(col-1).nodeName+row);
									}
								}	//
							}
						}
					}
			   		PutFieldText (thobj,oNode.childNodes(col-1).nodeName+row,oNode.childNodes(col-1).text);
			   }	//for
		   }
		}
	  }
	  catch (e)
	  {
		alert(e.description);
	  }
}

///////////////////////////////////////////////목차 만들기 함수 시작///////////////////////////////////
// 현재 페이지 정보

var vArrayZ = new Array();

function vMakeIndex(hobj,k, l,hwpfile1,hwpfile2)
{
	hobj.MoveToField("목차내용");

	for (var i = 0; i < k; i++)
	{
		if (ReportLayoutType ) {
			hobj.Insert(hosthref + "hwpFiles/"+hwpfile1+".hwp", "HWP", "template:true");
		} else {
			hobj.Insert(hosthref + hwpfile1+".hwp", "HWP", "template:true");
		}
		hobj.PutFieldText("대제목_목차{{"+i+"}}", vArrayN[i] + ". " + vArrayB[i]);
		hobj.MovePos(23);
		var vact = hobj.CreateAction("InsertText");
		var vset = vact.CreateSet();

		vact.GetDefault(vset);
		vset.SetItem("Text", ""+vBpage[i]);

		vact.Execute(vset);
		hobj.Run("BreakLine");

		for (var j = 0; j < l ; j++ )
		{
			if (vArrayS_ix[j] == i+1){
				if (ReportLayoutType ) {
					hobj.Insert(hosthref + "hwpFiles/"+hwpfile2+".hwp", "HWP", "template:true");
				} else {
					hobj.Insert(hosthref + hwpfile2+".hwp", "HWP", "template:true");
				}

				if(vArrayZ[j] != null){
					hobj.PutFieldText("소제목_목차{{"+j+"}}", vArrayZ[j]+"."+vArrayS[j]);
				}else{
					hobj.PutFieldText("소제목_목차{{"+j+"}}", vArrayS[j]);
				}
				hobj.MovePos(23);
				var vact = hobj.CreateAction("InsertText");
				var vset = vact.CreateSet();
				vact.GetDefault(vset);

				vset.SetItem("Text", ""+vSpage[j]);
				vact.Execute(vset);
				hobj.Run("BreakLine");
			}
		}
	}
}

function vOnMakeContent(hobj,hwpfile1,hwpfile2)
{
	var bBField = 1;
	var bSField = 1;

	// 대분류 개수 구하기
	var vIndex = 0;
	var vTemp = 0;
	var i = 0;
	var j = 0;
	var k = 0;
	var l = 0;
	var z = 0;
	var y = 0;

	var vFieldList = hobj.GetFieldList(1,2);
	if (vFieldList.length == 0) return 0;

	//logs(hobj,FieldList.length + " : " + vFieldList);
	for(var a = 0; a < vFieldList.length; a++)
	{
		if(vFieldList.charCodeAt(a) == 0x02)
		{
			vArrayField[vIndex] = vFieldList.substring(vTemp, a);
			vTemp = a+1;	// 0x02 넘어서기

			if (vArrayField[vIndex] == "대제목{{"+k+"}}"){
				hobj.MoveToField("대제목{{"+k+"}}", true, true, true);
				vArrayB[k] = hobj.GetFieldText("대제목{{"+k+"}}");
				vArrayN[k] = hobj.GetFieldText("카운트{{"+k+"}}");
				hobj.MovePos(20);
				vBpage[k] = h_PageInfo(hobj);
				k = k+1;
			} else if (vArrayField[vIndex] == "소제목{{"+y+"}}"){
				hobj.MoveToField("소제목{{"+y+"}}", true, true, true);
				vArrayS[l] = hobj.GetFieldText("소제목{{"+y+"}}");
				vSpage[l] = h_PageInfo(hobj);

				vArrayS_ix[l] = k;
				l = l+1;
				y = y+1;
			} else if (vArrayField[vIndex] == "전략목표내용{{"+z+"}}"){
				hobj.MoveToField("전략목표내용{{"+z+"}}", true, true, true);
				vArrayS[l] = hobj.GetFieldText("전략목표내용{{"+z+"}}");
				vArrayZ[l] = hobj.GetFieldText("전략목표{{"+z+"}}");
				vSpage[l] = h_PageInfo(hobj);

				vArrayS_ix[l] = k;
				l = l+1;
			   z =z+1;
			}
			vIndex++;
		}
	}
	hobj.Run("MoveTopLevelEnd");
	vMakeIndex(hobj,k, l,hwpfile1,hwpfile2);
}

///////////////////////////////////////////////목차 만들기 함수 끝///////////////////////////////////

function xPutFieldSetText(thobj,xpath,idx,chkfilter,filter)
{
	  if (filter == null) { filter="";}
	  if (chkfilter == null) { chkfilter="";}
	  if (chkfilter == "") { chkfilter="0";}

	  try
	  {
		if (chkfilter == "0") {
			CFilter = xpath+"["+idx+"]";
		} else {
			CFilter = xpath+"["+idx+"]";
	    }
	    if (_DEBUG) logs(thobj,"xPutFieldText xpath-"+CFilter);

		var oNodes = xmldom.selectNodes(CFilter);
		for (i=0; i<oNodes.length; i++)
		{
		   oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
			  	if (_DEBUG) logs(thobj,oNode.childNodes.length);
			   for( j = 0 ; j< oNode.childNodes.length ; j++) {
			   		if (_DEBUG) logs(thobj, "[child]"+ oNode.childNodes(j).nodeName);
			   		if (filter != "") {
			   				if (filter == oNode.childNodes(j).nodeName) {
			   					DFilter = 	oNode.childNodes(j).nodeName + "='" + oNode.childNodes(j).text +"'";
			   				}
			   		}

					//데이터가 clob일경우
			   		xTstr = oNode.childNodes(j).text;
			   		if(xTstr.length > 7){
			   			vTstr = xTstr.substring(0,7);
			   			vTstr = xSetTextType(vTstr);
			   			if(vTstr == true){
							thobj.MoveToField(oNode.childNodes(j).nodeName);
							thobj.SetTextFile(xTstr,"HWP", "insertfile");
			   			}else{
							PutFieldText (thobj,oNode.childNodes(j).nodeName,xTstr);
			   			}
			   		}else{
			   			PutFieldText (thobj,oNode.childNodes(j).nodeName,xTstr);
			   		}
			   }
		   }
		}
	  }
	  catch (e)
	  {
		logs(thobj,"[xPutFieldSetText]"+e.description);
	  }
}

function xTableAppendSetText(thobj,xpath,cols,nomerge,mtype,align,bold)
{
	if (nomerge == null)	nomerge = 0;
	if (mtype == null)	mtype = '';
	if (align == null)	align = 'Left';
	if (bold == null)	bold = '0';

	var tAttr = new Array();
	var tAttr2 = new Array();
	var row_1 = false;
	var row_2 = false;
	var vAlign = "ParagraphShapeAlign"+align;

	  try
	  {
		var oNodes = xmldom.selectNodes(xpath);

		for (row =1; row <= oNodes.length ; row++)
		{
		   oNode = oNodes.nextNode;
		   if (oNode != null)
		   {
			   if( row > 1 ) {
					// 새로운 행 추가
					TableAppendRowSetName(thobj,tFname[0]+ (row-1) , row , cols);

					if (row_1 == true) // 이전열을 머지해야 하는 경우
					{
						_Fctl = thobj.MoveToField(tFname[0] + (row-1));
						if ( _Fctl == true) {
							thobj.Run("TableCellBlock");
							thobj.Run("TableCellBlockExtend");
							for (m=1;m<(cols-nomerge) ;m++ )
							{
								thobj.Run("TableRightCell");
							}
							thobj.Run("TableMergeCell");
							thobj.Run(vAlign);

							if (bold=1){//병합 후 글자 진하기
								thobj.Run("TableCellBlock");
								thobj.Run("CharShapeBold");
							}
						}
					}

					// 마지막 행인 경우
					if (row == oNodes.length)
					{
						row_1 = FindAttribute(oNode.attributes,"M","1");
						if (row_1 == true) // 이전열을 머지해야 하는 경우
						{
							_Fctl = thobj.MoveToField(tFname[0] + (row));
							if ( _Fctl == true) {
								thobj.Run("TableCellBlock");
								thobj.Run("TableCellBlockExtend");
								for (m=1;m<(cols-nomerge) ;m++ )
								{
									thobj.Run("TableRightCell");
								}
								thobj.Run("TableMergeCell");

								thobj.Run(vAlign);

								if (bold=1){//병합 후 글자 진하기
									thobj.Run("TableCellBlock");
									thobj.Run("CharShapeBold");
								}
							}
						}
					}
				}
				// 첫번째 행부터 값 판단(다음값 판단을 위하여 설정(머지 여부)
				row_1 = FindAttribute(oNode.attributes,"M","1");

			   for( col = 1 ; col <= oNode.childNodes.length ; col++) {

					if(row == 1) {	// 첫번째 데이터인 경우
						tAttr[col] = FindAttribute(oNode.childNodes(col-1).attributes,"M","1");
						tAttr2[col] = FindAttribute(oNode.childNodes(col-1).attributes,"P","1");
					}

					var chkstr = false;
					if (tAttr[col] == true && row > 1)		// 적어도 현재값이 m=1이고 두번째 행부터
					{
						if (col == 1)
						{
							if (tAttr[col] ==  true) chkstr = true;
						} else {
							if (tAttr[col-1] ==  true ) {	// 이전꺼
									_Fctl = thobj.MoveToField(oNode.childNodes(col-2).nodeName+(row-1));
									if ( _Fctl == false) {chkstr = true;}	// 이미 병합이 되었으므로 없음
							} else  { chkstr = true; }
						}
					}

					if (chkstr ==  true )
					{
						if ( row > 1 )
						{
							if (thobj.GetFieldText(oNode.childNodes(col-1).nodeName+(row-1)) == Convert(oNode.childNodes(col-1).text) )
							{

								_Fctl = thobj.MoveToField(oNode.childNodes(col-1).nodeName+(row));
								if ( _Fctl == true) {
									thobj.MovePos(102);	// 위로
									thobj.Run("TableCellBlock");
									thobj.Run("TableCellBlockExtend");
									thobj.Run("TableLowerCell");
									thobj.Run("TableMergeCell");
									thobj.RenameField(oNode.childNodes(col-1).nodeName+(row-1),oNode.childNodes(col-1).nodeName+row);

									if (mtype == 1) {	//R034.jsp를 위한 루틴 (확인점검일때)
										thobj.MovePos(105);	// 행의끝
										thobj.MovePos(100);
										thobj.Run("TableCellBlock");
										thobj.Run("TableCellBlockExtend");
										thobj.Run("TableLowerCell");
										thobj.Run("TableMergeCell");
										thobj.RenameField(oNode.childNodes(col-1).nodeName+(row-1),oNode.childNodes(col-1).nodeName+row);

										thobj.MovePos(105);	// 행의끝
										thobj.Run("TableCellBlock");
										thobj.Run("TableCellBlockExtend");
										thobj.Run("TableLowerCell");
										thobj.Run("TableMergeCell");
										thobj.RenameField(oNode.childNodes(col-1).nodeName+(row-1),oNode.childNodes(col-1).nodeName+row);
									}else if(mtype == 2) {//R034.jsp를 위한 루틴 (자체점검일때)
										thobj.MovePos(105);	// 행의끝
										thobj.Run("TableCellBlock");
										thobj.Run("TableCellBlockExtend");
										thobj.Run("TableLowerCell");
										thobj.Run("TableMergeCell");
									}
								}
							}
						}
					}
					//데이터가 clob일경우
			   		xTstr = oNode.childNodes(col-1).text;
			   		if(xTstr.length > 7){
			   			vTstr = xTstr.substring(0,7);
			   			vTstr = xSetTextType(vTstr);
			   			if(vTstr == true){
							thobj.MoveToField(oNode.childNodes(col-1).nodeName+row);
							thobj.SetTextFile(xTstr,"HWP", "insertfile");
			   			}else{
							PutFieldText (thobj,oNode.childNodes(col-1).nodeName+row,xTstr);
			   			}
			   		}else{
			   			PutFieldText (thobj,oNode.childNodes(col-1).nodeName+row,xTstr);
			   		}
			   }	//for
		   }
		}
	  }
	  catch (e)
	  {
		logs(thobj,"[xTableAppendSetText]"+e.description);
	  }
}

//Data가 clob이면 true 텍스트이면 false
function xSetTextType(tmpstr)
{
	try{
		if (tmpstr.length > 1){
			if (tmpstr == "0M8R4KG"){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}catch(e){
		logs(thobj,"[xSetTextType]"+e.description);
	}
}

function Nodata(thobj,fnm,msg){
	if(thobj.MoveToField(fnm)){
		thobj.Run("TableCellBlock");
		thobj.Run("TableCellBlockExtend");
		thobj.MovePos(105);
		thobj.Run("TableMergeCell");
		InsertText(thobj,msg);
	}
}

function SetTextFileHTM(hobj,xpath,fieldnm,type)
{
	if (hobj.MoveToField(fieldnm)){
	
	hwptext = GetXMLDOM(xpath);
		if (hwptext != "")
		{
			if(type == "tagReplace"){
				hwptext = replaceHTM(hwptext);
			}
			hobj.SetTextFile(hwptext,"HTML", "insertfile");
		}
	}
}

function replaceHTM(strV)
{
	try {
		strV = strV.replace(new RegExp("&lt;","gi"),"<");
		strV = strV.replace(new RegExp("&gt;","gi"),">");
		strV = strV.replace(new RegExp("&quot;","gi"),"\"");
		strV = strV.replace(new RegExp("&amp;","gi"),"&");
		return(strV);
	} catch(e) {
		return(strV);
	}
}

function replaceTEXT(strV,str1,str2)
{
	try {
		strV = strV.replace(new RegExp(str1,"gi"),str2);
		return(strV);
	} catch(e) {
		return(strV);
	}
}

function EditMode(hobj,type)
{
	/*0 : 읽기 전용 1 : 일반 편집모드 2 : 양식 모드(양식 사용자 모드) 16 : 배포용 문서 (SetEditMode로 지정 불가능)*/
	hobj.EditMode = type;
}
function SetFieldViewOption(hobj,type)
{
	/*1 : 누름틀 영역 표시 안하기 2 : 누름틀 영역 빨강색 3 : 누름틀 영역 하얀색*/
	hobj.SetFieldViewOption(type);
}
