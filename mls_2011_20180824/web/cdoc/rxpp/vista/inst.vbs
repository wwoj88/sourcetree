	Dim update, osinfo
	update = 3							'// 1(모달창),3(페이지 이동 방식), 5(프래임 설치 방식), -1(설치하지않고 빠져 나가기)
	rxPath	= "/cdoc/rxpp/vista/"       '//ReportExpress 제품 설치 폴더
	CaptionA = "저작권 찾기"			'//설치 기관명
	ShowMsgA = true 					'// 설치 정보 보여주기

	// 1. 버젼 정보 확인
	On Error Resume Next
	Set obj=CreateObject("rxppUpdator.Updator")
	if err.number = 0 then
		hostname = window.location.host
		obj.RemoteHost= hostname + rxPath + "module/"
		obj.SetupFileName= "rxp.htm"
		obj.ShowErrorMsg = 0
		obj.Timeout=30
		obj.Caption = CaptionA
		update = obj.CheckUpdate(ShowMsgA)
	end if
	Set obj=Nothing

	if update <> 0 then
		window.location.href = rxPath & "inst_vista_js.htm?update=" & update
	end if

