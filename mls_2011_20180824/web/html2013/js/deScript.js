/*--------------------------------------------------------------------------*/
/* Top Gnb Menu
/*--------------------------------------------------------------------------*/

function initNavigation(seq,currentOn) {
	nav = document.getElementById("topGnb");
	nav.menu = new Array();
	nav.current = null;
	nav.menuseq = 0;
	navLen = nav.childNodes.length;
	
	allA = nav.getElementsByTagName("a")
	for(k = 0; k < allA.length; k++) {
		allA.item(k).onmouseover = allA.item(k).onfocus = function () {
			nav.isOver = true;
		}
		allA.item(k).onmouseout = allA.item(k).onblur = function () {
			nav.isOver = false;
			setTimeout(function () {
				if (nav.isOver == false) {
					if (nav.menu[seq])
						nav.menu[seq].onmouseover();
					else if(nav.current) {
						menuImg = nav.current.childNodes.item(0);
						menuImg.src = menuImg.src.replace("_on.gif", "_off.gif");
						if (nav.current.submenu)
							nav.current.submenu.style.display = "none";
						nav.current = null;
					}
				}
			}, 1000);
		}
	}

	for (i = 0; i < navLen; i++) {
		navItem = nav.childNodes.item(i);
		if (navItem.tagName != "LI")
			continue;

		navAnchor = navItem.getElementsByTagName("a").item(0);
		navAnchor.submenu = navItem.getElementsByTagName("div").item(0);
		
		navAnchor.onmouseover = navAnchor.onfocus = function () {
			if (nav.current) {
				menuImg = nav.current.childNodes.item(0);
				menuImg.src = menuImg.src.replace("_on.gif", "_off.gif");
				if (nav.current.submenu)
					nav.current.submenu.style.display = "none";
				nav.current = null;
			}
			if (nav.current != this) {
				menuImg = this.childNodes.item(0);
				menuImg.src = menuImg.src.replace("_off.gif", "_on.gif");
				if (this.submenu)
					this.submenu.style.display = "block";
					if (this.submenu && currentOn && this.submenu.parentNode.className == "menu"+seq && this.submenu.getElementsByTagName("li")[currentOn - 1])	{
						this.submenu.getElementsByTagName("li")[currentOn - 1].className = this.submenu.getElementsByTagName("li")[currentOn - 1].className + " current";
					}
				nav.current = this;
			}
			nav.isOver = true;
		}
		nav.menuseq++;
		nav.menu[nav.menuseq] = navAnchor;
	}
	if (nav.menu[seq])	{
		nav.menu[seq].onmouseover();
	}
}

/*--------------------------------------------------------------------------*/
/*  subLeft Slide Menu
/*--------------------------------------------------------------------------*/
function	subSlideMenu(subSlideMenuId,firstOn)	{
	var subSlideMenu = document.getElementById(subSlideMenuId);
	var menuUl = subSlideMenu.getElementsByTagName("ul");
	var menuLink = subSlideMenu.getElementsByTagName("a");
	var menuImg = subSlideMenu.getElementsByTagName("img");

	subSlideMenuFirst();

	function	subSlideMenuFirst()	{
		for (i=0;menuUl.length>i;i++)
		{
			if (menuUl[i].parentNode != subSlideMenu)
			{
				menuUl[i].style.display = "none";
			}
		}
	}

	for (j=0;menuLink.length>j;j++)
	{
		menuLink[j].onclick = function()	{
			var thisChild = this.parentNode.childNodes;
			var thisEl = this;
			for (k=0;thisChild.length > k;k++)
			{
				if (thisChild[k].nodeName=="UL" && thisChild[k].style.display == 'none') {
					subSlideMenuFirst();
					thisChild[k].style.display = 'block';
					while (thisEl.parentNode.parentNode) {
						if (thisEl == subSlideMenu) break;
						thisEl.parentNode.style.display = 'block';
						thisEl = thisEl.parentNode;
					}
					return false;
				}
				else if (thisChild[k].nodeName=="UL" && thisChild[k].style.display == 'block') {
					thisChild[k].style.display = 'none';
					return false;
				}
			}
		}
	}

	for (l=0;menuImg.length > l;l++)
	{
		menuImg[l].onmouseover = function()	{
			this.src = this.src.replace("_off","_on");
		}
		menuImg[l].onmouseout = function()	{
			this.src = this.src.replace("_on","_off");
		}
	}

	if (firstOn && document.getElementById(firstOn))
	{
		if (document.getElementById(firstOn).getElementsByTagName("img").length > 0)
		{
			document.getElementById(firstOn).getElementsByTagName("img")[0].src = document.getElementById(firstOn).getElementsByTagName("img")[0].src.replace("_off","_on");//온으로
			document.getElementById(firstOn).getElementsByTagName("img")[0].onmouseover = "";
			document.getElementById(firstOn).getElementsByTagName("img")[0].onmouseout = "";
		}
		else	{
			document.getElementById(firstOn).className = "nowPage";
		}
		var onEl = document.getElementById(firstOn);
		while (onEl.parentNode.parentNode) {
			if (onEl == subSlideMenu) break;
			onEl.parentNode.style.display = 'block';
			onEl = onEl.parentNode;
		}
	}
}

/*--------------------------------------------------------------------------*/
/*  image on off
/*--------------------------------------------------------------------------*/
function imgOn(imgElement)
{
	imgElement.src = imgElement.src.replace("_off.gif", "_on.gif");
}

function imgOut(imgElement)
{
	imgElement.src = imgElement.src.replace("_on.gif", "_off.gif");
}


<!--
function setPng24(obj) {
obj.width=obj.height=1;
obj.className=obj.className.replace(/\bpng24\b/i,'');
obj.style.filter =
"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+ obj.src +"',sizingMethod='image');"
obj.src='';
return '';
}
//-->

/*--------------------------------------------------------------------------*/
/*  toggleLayer
/*--------------------------------------------------------------------------*/
function toggleLayer(whichLayer) {
  var elem, vis;
  if(document.getElementById) // this is the way the standards work
    elem = document.getElementById(whichLayer);
  else if(document.all) // this is the way old msie versions work
      elem = document.all[whichLayer];
  else if(document.layers) // this is the way nn4 works
    elem = document.layers[whichLayer];
  vis = elem.style;
  // if the style.display value is blank we try to figure it out here
  if(vis.display==''&&elem.offsetWidth!=undefined&&elem.offsetHeight!=undefined)
    vis.display = (elem.offsetWidth!=0&&elem.offsetHeight!=0)?'block':'none';
  vis.display = (vis.display==''||vis.display=='block')?'none':'block';
}


var old='';
function menu(name){
    submenu=eval("submenu_"+name+".style");
    if(old!=submenu)
    {
        if(old!='')
        {
            old.display='none';
        }
        submenu.display='block';
        old=submenu;
    }
    else
    {
        submenu.display='none';
        old='';
    }
}

/*--------------------------------------------------------------------------*/
/*  Select
/*--------------------------------------------------------------------------*/
jQuery(function($){
	
	// Common
	var select_root = $('div.select');
	var select_value = $('.myValue');
	var select_a = $('div.select>ul>li>a');
	var select_input = $('div.select>ul>li>input[type=radio]');
	var select_label = $('div.select>ul>li>label');
	
	// Radio Default Value
	$('div.myValue').each(function(){
		var default_value = $(this).next('.iList').find('input[checked]').next('label').text();
		$(this).append(default_value);
	});
	
	// Line
	select_value.bind('focusin',function(){$(this).addClass('outLine');});
	select_value.bind('focusout',function(){$(this).removeClass('outLine');});
	select_input.bind('focusin',function(){$(this).parents('div.select').children('div.myValue').addClass('outLine');});
	select_input.bind('focusout',function(){$(this).parents('div.select').children('div.myValue').removeClass('outLine');});
	
	// Show
	function show_option(){
		$(this).parents('div.select:first').toggleClass('open');
	}
	
	// Hover
	function i_hover(){
		$(this).parents('ul:first').children('li').removeClass('hover');
		$(this).parents('li:first').toggleClass('hover');
	}
	
	// Hide
	function hide_option(){
		var t = $(this);
		setTimeout(function(){
			t.parents('div.select:first').removeClass('open');
		}, 1);
	}
	
	// Set Input
	function set_label(){
		var v = $(this).next('label').text();
		$(this).parents('ul:first').prev('.myValue').text('').append(v);
		$(this).parents('ul:first').prev('.myValue').addClass('selected');
	}
	
	// Set Anchor
	function set_anchor(){
		var v = $(this).text();
		$(this).parents('ul:first').prev('.myValue').text('').append(v);
		$(this).parents('ul:first').prev('.myValue').addClass('selected');
	}

	// Anchor Focus Out
	$('*:not("div.select a")').focus(function(){
		$('.aList').parent('.select').removeClass('open');
	});
	
	select_value.click(show_option);
	select_root.find('ul').css('position','absolute');
	select_root.removeClass('open');
	select_root.mouseleave(function(){$(this).removeClass('open');});
	select_a.click(set_anchor).click(hide_option).focus(i_hover).hover(i_hover);
	select_input.change(set_label).focus(set_label);
	select_label.hover(i_hover).click(hide_option);
	
	// Form Reset
	$('input[type="reset"], button[type="reset"]').click(function(){
		$(this).parents('form:first').find('.myValue').each(function(){
			var origin = $(this).next('ul:first').find('li:first label').text();
			$(this).text(origin).removeClass('selected');
		});
	});
	
});
