<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %>
<%
	DSHttpServletResponse res = new DSHttpServletResponse(response);
	res.setRequest(request);
	String challenge = "";
	try{
		challenge = res.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
		return;
	}
	out.println("challenge=" + challenge + "&sid=" + request.getSession().getId());
%>