<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	String goPage = request.getParameter("goPage");
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>JAVA 프로그램 설치 | 공인인증서 솔루션( Dreamsecurity MagicLine )</title>

<!--

<link href="http://www.hometax.go.kr/css/front/hometax/table.css" rel="stylesheet" type="text/css" />
<link href="http://www.hometax.go.kr/css/front/hometax/common_style.css" rel="stylesheet" type="text/css" />
<link href="http://www.hometax.go.kr/css/front/hometax/font.css" rel="stylesheet" type="text/css" />
<link href="http://www.hometax.go.kr/css/front/hometax/help_popup.css" rel="stylesheet" type="text/css" />
-->


<script type="text/javascript" src="/MagicLine4/js/deployJava.js"></script>
<script type="text/javascript" src="/MagicLine4/js/PluginDetect.js"></script>
<script type="text/javascript" src="/MagicLine4/js/MagicLine.js"></script>
<script type="text/javascript">

	<%
	if(goPage == null ||  goPage.equals("")){
	%>
		deployJava.returnPage = returnIndexPage;
	<%
	}else{
	%>
		deployJava.returnPage = '<%=goPage%>';
	<%
	}
	%>

	//~! ADDED BY DREAMSECURITY. 2010-03-03 16:15
	//[[
	function open_help()
	{
		deployJava.getBrowser();
		browser = deployJava.browserName2;

		var page_name = "";

		if (browser == "Safari") {
			page_name = "jre_install_help_s.html";
		} else if (browser == "Firefox") {
			page_name = "jre_install_help_ff.html";
		} else {
			page_name = "jre_install_help_ie.html";
		}

		window.open(page_name, "", "status=0, toolbar=0, resizable=0, scrollbars=1, height=800, width=700");
	}

	function download(){
		var url = javaInstallUrl();
		window.location = url;
	}

	function open_Error(){
		var port = location.port;
		var common_url = "";
		if(port == "" || port == "80"){
			common_url  = 'http://localhost:8080/MagicLine4/lib';		// 운영서버 (CDN)
		}else{
			common_url  = 'http://localhost:8080/MagicLine4/lib';		// 개발서버
		}

		var error_doc_Url = common_url+"wmagic/java_help.hwp";
		window.location = error_doc_Url;
	}




	//]]
</script>
<style type="text/css">
body {_text-align:center; font-family:"돋움", Dotum, "굴림", Gulim, AppleGothic, Sans-serif;}
#center {width:620px; _text-align:left; margin:0px auto;}
.ck_install
{
	border:#c6d7e9 solid 3px;
	width:600px;
	padding-left:20px;
}

.f_14	{font-size:14px}
.f_blue_ty1b 	{color:#0169A8; font-weight:bold;}
.f_gy_ty1b		{color:#666666; font-weight:bold;}
.letter_sm1  {letter-spacing:-1px}
.f_dotum  	{font-family: "돋움"}
.f_black		{color:#000}
.bold {font-weight:bold}
.block {display:block}
.f_or_ty1		{color:#ED7012}
img {vertical-align:middle; border:0;}

body{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	font-family:"돋움", Dotum, "굴림", Gulim, AppleGothic, Sans-serif;
	font-size:12px;
	color:#383d41;
	line-height:160%;
}
.tx-center {text-align:center; }




/* margin & padding */
.mgt10 {margin-top:10px}
.mgt60 {margin-top:60px}



/* //margin & padding */




</style>

</head>
<body id="center" onload="installJRE();"><!-- installJRE();info_text();installJRE(); -->
<div id="MagicLineDiv"></div>
<div class="ck_install">
  <img src="/MagicLine4/images/logo.gif" width="579" height="43" alt="자바프로그램 설치" />
  <img src="/MagicLine4/images/tit4.gif" width="579" height="39" alt="자바프로그램 설치안내" />
  <img src="/MagicLine4/images/headline4.gif" width="579" height="39" alt="원할한 사이트 이용을 위해  보안프로그램을 설치하여 주십시요." />

	<div class="f_14 f_gy_ty1b letter_sm1 pat210" style="width:570px;padding: 3px 3px 3px 3px; border:#c6d7e9 solid 2px; ">
	본 사이트는 다양한 브라우저(IE, 파이어폭스, 사파리) 사용자 및 장애인들도 본 사이트를  이용할 수 있도록 웹표준 준수 및 장애인 접근성을 개선한 서비스를 제공하고 있습니다.
	</div>

	<div style="width:570px; height:300px; padding:10px 0px 0px 0px; text-align:left; margin:0px 0px 5px 10px;">
	<span class="f_14 f_dotum f_blue_ty1b letter_sm1">
	본 사이트는 자바환경 기반으로 웹표준서비스를 제공하므로 자바프로그램이 정상적으로 설치되어야  접속이 가능합니다.<br/><br/>
	</span>
	<div class="f_13 f_gy_ty1b letter_sm1" style="width:570px;padding: 5px 3px 3px 3px; ">
	&lt;접속을 위한 프로그램 설치 과정&gt;
	</div>
    <ol>
	  <li><span class="f_black bold block">자바 프로그램 설치:</span>
      				<p style="position:absolute; float:left; display:block; padding:45px 0px 0px 0px;margin:0px; ">
					<a href="http://www.boho.or.kr/kor/download/download_01.jsp" target="_blank">
						<img src="/MagicLine4/images/vir_btn01.gif"  alt="바이러스 백신 프로그램 안내" width="190" height="30" />
					</a>
                    </p>
		아래의 [JAVA프로그램 수동설치]를 클릭하여 설치.
        <div class="f_or_ty1 letter_sm1">※ PC에 바이러스가 있는 경우 정상적으로 설치가 안될 수 있으므로 바이러스 치료 후 설치하셔야 합니다.</div>
	  </li>
      <li class="mgt60"><span class="f_black bold block">보안프로그램(매직라인) 설치:</span>
		자바 설치가 완료되면 자동으로 설치됨.
	  </li>
      <li class="mgt10">
      			<span class="f_black bold block">사이트 접속:</span>

                    <span  style="position:absolute; float:left; display:block; padding:60px 5px 0px 230px; margin:0px; width:200px;color:#ff3333">왼쪽의 [자바수동설치하기] 클릭하여<br/>설치 중 오류가 발생된 경우 사용</span>
                       <p style="position:absolute; float:left; display:block; padding:25px 5px 0px 0px; margin:0px; ">
						<a href="javascript:download();" title="자바 수동 설치하기">
							<img src="/MagicLine4/images/java_btn01.gif"  alt="자바 수동 설치하기"  />
						</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="javascript:open_Error();" title="자바 설치오류 조치방법">
							<img src="/MagicLine4/images/help_btn01.gif"  alt="자바 설치오류 조치방법"  />
						</a>

                    </p>
		보안프로그램이 정상적으로 설치되면 자동으로 접속됨.
	  </li>
    </ol>
  </div>
</div>

<!-- JAVA Installed Pooling -->
<script type="text/javascript" src="/MagicLine4/js/polling.js"></script>
</body>
</html>
