<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
String goPage = request.getParameter("goPage");
%>
<html>
<head>
<title>보안프로그램 설치</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
body,td { FONT-FAMILY: 굴림,돋움, arial, verdana, helvetica ; FONT-SIZE: 9pt; line-height:170%; color: #333333;}

<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.mb {color:#222222;LINE-HEIGHT: 18px;font-size: 12px;text-decoration:none}
.mb A:link {color: #222222;LINE-HEIGHT: 18px;font-size: 12px;text-decoration:none}
.mb A:visited {color:#222222;LINE-HEIGHT: 18px;font-size: 12px;text-decoration:none}
.mb A:active {color:#222222;LINE-HEIGHT: 18px;font-size: 12px;text-decoration:none}
.mb A:hover {color: #0d72a1;LINE-HEIGHT: 18px;font-size: 12px;text-decoration:underline}

-->
</style>
<script language="javascript" src="js/deployJava.js"></script>
<script language="javascript" src="js/PluginDetect.js"></script>
<script language="javascript" src="js/MagicLine.js"></script>
<SCRIPT LANGUAGE="JavaScript">
//====================================================================
// MagicLine4 INSTALL START !!
//====================================================================

// install Page require!!!
function goMagicLineProgressInstall(){
	//to do.....
	//location.href = returnIndexPage+'?installStatus=ok&goPage=<%=goPage%>';
}


function installed(){

	if(mlEnv.Product =='ALL' || mlEnv.Product =='MBX')
	{
		if(isMsie)
		{
			if (MagicLine_install == true)
			{
				location.href = returnIndexPage;
			}
		}
	}
	else
	{
		if(javaFunc.java16VersionMSIECheck())
		{
			runMagicLine('install');
		}

	}
}

//====================================================================
// MagicLine4 INSTALL END !!
//====================================================================
</SCRIPT>


</head>

<BODY onload="installed()">

<table width="620" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td><img src="images/activeX_title.gif" width="620" height="87"></td>
	</tr>
	<tr>
		<td align="center" background="images/bg_mid_line.gif">
		<table width="620" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
		        <td align="center"><img src="images/top_magicline_1.gif" width="620" height="143"></td>
			</tr>
			<!-- install page 수정시작  kj  -->

			<tr id="install_ie" style="display: none;">
				<td align="center">
					<div style="width:100%;padding:10 10 10 10;overflow:hidden">
						<table width="580" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>1. 설치 페이지에서 아래와 같은 대화상자가 나타나면, ‘설치’를 클릭 하십시오.
									<br/>프로그램이 설치된 후에 자동으로 초기 화면으로 이동합니다.
								</td>
							</tr>
						</table>
					</div>
					<img src="images/magicline_install03.gif" >
				</td>
			</tr>

			<tr id="install_java" style="display: none;">
				<td align="center">
					<div style="width:100%;padding:10 10 10 10;overflow:hidden">
						<table width="580" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>1. 프로그램 설치가 정상적으로 완료되면 자동으로 초기화면으로 이동됩니다.</td>
							</tr>
						</table>
					</div>
					<img src="images/magicline_install02.gif" width="399" height="249">
				</td>
			</tr>

			<tr id="install_jre" style="display: none;">
			<td align="center">
			<div style="width:100%;padding:10 10 10 10;overflow:hidden">
				<table width="580" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>1. 자바환경 기반으로 웹표준서비스를 제공하므로 자바프로그램이 정상적으로 설치되어야 서비스 접속이
								 <br/>가능합니다. 설치 페이지에서 아래와 같은 대화상자가 나타나면, ‘설치’를 클릭 하십시오.
								<br/>프로그램이 설치된 후에 자동으로 웹암호화보안프로그램설치 페이지로 이동합니다.</td>
					</tr>
				</table>
			</div>
			<img src="images/magicline_jreinstall.gif" width="540" height="200">
			</td>
			</tr>
		</td>
	</tr>
	<!--  install page 수정 끝 -->

	<tr id="install_ie_2" style="display: none;">
		<td align="center">
			<div style="width:100%;padding:10 10 10 10;overflow:hidden">
				<table width="580" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>2. 웹암호 보안프로그램이 자동으로 설치되지 않는 경우나, 현재화면에서 정지가 되어 있는 경우 아래의 <br>&nbsp;
								 [수동설치] 버튼을 클릭하여 수동으로 설치하시기 바랍니다.
						</td>
					</tr>
				</table>
			</div>
			<a href="./lib/MagicLineMBXSetup.exe"><img src="images/magicline_install_btn1.gif" width="215" height="36" border="0"></a>
		</td>
	</tr>
	<tr id="install_ie_64" style="display: none;">
		<td align="center">
			<div style="width:100%;padding:10 10 10 10;overflow:hidden">
				<table width="580" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>2. 웹암호 보안프로그램이 자동으로 설치되지 않는 경우나, 현재화면에서 정지가 되어 있는 경우 아래의 <br>&nbsp;
								 [수동설치] 버튼을 클릭하여 수동으로 설치하시기 바랍니다.
						</td>
					</tr>
				</table>
			</div>
			<a href="./lib/MagicLineMBXSetup64.exe"><img src="images/magicline_install_btn1.gif" width="215" height="36" border="0"></a>
		</td>
	</tr>
	<tr>
	</tr>

	<tr id="install_ie_3" style="display: none;">
		<td align="center">
			<div style="width:100%;padding:10 10 10 10;overflow:hidden">
				<table width="580" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>3. MagicLine 웹암호보안프로그램은 아래 브라우저들을 지원합니다.</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>

	<tr id="install_java_2" style="display: none;">
		<td align="center">
			<div style="width:100%;padding:10 10 10 10;overflow:hidden">
				<table width="580" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>2. MagicLine 웹암호보안프로그램은 아래 브라우저들을 지원합니다.</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>

	<tr>
		<td align="center"><img src="images/magicline_browser_3.jpg" width="597" height="152"></td>
	</tr>

	<tr>
		<td align="center">
			<div style="width:100%;padding:10 10 10 10;overflow:hidden">
				<table width="580" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center" class="mb"><strong><a href="http://www.dreamsecurity.com" target="_blank">http://www.dreamsecurity.com</a></strong><br></td>
						<td width="198"><a href="http://www.dreamsecurity.com"><img src="images/dreamsecurity_logo.gif" width="198" height="102" border="0"></a></td>
					</tr>
					<tr>
					</tr>
				</table>
			</div>
		</td>
	</tr>
  	<tr>
    	<td><img src="images/bg_bottom_line.gif" width="620" height="29"></td>
  	</tr>
</table>

<!--
@dreamsecurity
Magic Line Javascript
-->
<script language="javascript">

// MagicLine install

/* NTS by kj
   매직라인이 설치가 완료 되었을 경우 호출되는 함수.
   Index 페이지로 이동한다.
   (반드시 이 함수가 있어야 한다.)
*/
function goMagicLineInstalledComplete(){
	//아래 location.href에 파라미터를 넣어서 페이지 처리를 할수있다
	//예제 location.href = returnIndexPage+'?goPage=<%=goPage%>';
	//returnIndexPage  == MagicLine.js 에 정의 되어있다.

    	location.href = returnIndexPage;
}

// 설치페이지에서는 runMagicLine('install') 에 인수를 install로 주어야 한다.
//runMagicLine('install');


if(mlEnv.Product =='ALL'|| mlEnv.Product =='MBX')
{
	if(isMsie){

		//install page 추가
		document.getElementById("install_ie").style.display = "inline";

		var is64 = window.navigator.userAgent.toLowerCase().indexOf('win64') >-1;
		if(is64)
			document.getElementById("install_ie_64").style.display = "inline";
		else
			document.getElementById("install_ie_2").style.display = "inline";

		document.getElementById("install_ie_3").style.display = "inline";

		var classid = mlProp.getClassId();
		var codebase = mlProp.activeXCodeBase();
		var objHtml =  '<OBJECT ID="MagicLine" '+classid+' CODEBASE="'+codebase+'" WIDTH ="0" HEIGHT = "0" onError="MagicLine_install=false;"></OBJECT>';
		document.write(objHtml);
		//mlFunc.insertHtml('install',objHtml);

	}else{
		//install page 추가
		document.getElementById("install_java").style.display = "inline";
		document.getElementById("install_java_2").style.display = "inline";

		runMagicLine('install');
	}
}
else
{
	if(!javaFunc.java16VersionMSIECheck())
	{
		document.getElementById("install_jre").style.display = "inline";
		document.getElementById("install_java_2").style.display = "inline";
		document.write(javaFunc.getInstallJRETag(browser));
	}
	else
	{
		document.getElementById("install_java").style.display = "inline";
		document.getElementById("install_java_2").style.display = "inline";
	}


}

</script>

</body>
</html>
