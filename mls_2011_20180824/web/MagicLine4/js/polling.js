// @(#)polling.js	1.0 05/21/2009

	deployJava.preInstallJREList = deployJava.getJREs();
	var pollInterval = null;

	if (deployJava.returnPage != null && deployJava.returnPage.length > 0 ) {
		// Check to see if the plugin is installed		
		if(deployJava.isPluginInstalled()) {
			pollInterval = setInterval("callpoll()", 3000);
		} else {
			deployJava.myInterval = setInterval("deployJava.poll()", 3000);
		}
	}

  function callpoll() {
      deployJava.refresh();
       if (arraysDifferent(deployJava.preInstallJREList, deployJava.getJREs()) ) {
          clearInterval(pollInterval);
          if (deployJava.returnPage != null) {
               location.href = deployJava.returnPage;
          }
      }
  }

   function arraysDifferent(a1, a2) {
       if (a1.length != a2.length) {
           return true;
       }
       for (var i=0; i < a1.length; i++) {
           if (a1[i] != a2[i]) {
               return true;
           }
       }
       return false;
   }
