<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %>
<%@ page import="com.dreamsecurity.jcaos.cms.SignedData" %>
<%@ page import="com.dreamsecurity.jcaos.cms.SignerInfo" %>
<%@ page import="com.dreamsecurity.jcaos.util.encoders.Base64" %>
<%@ page import="java.util.ArrayList"%>

<%

	String encData= request.getParameter("encryptedData");
	X509Certificate cert = null;
	String subDN = "";
	java.math.BigInteger serialNumber = null;
	String signData = "";

	byte[] decData = Base64.decode(encData);
	try
	{
			SignedData signedData = SignedData.getInstance(decData);
  		signedData.verify();
  		ArrayList signerInfos = signedData.getSignerInfos();
  		for(int i=0; i<signerInfos.size(); i++)
  		{
  				SignerInfo signerInfo = (SignerInfo)signerInfos.get( i );
					cert = signedData.getSignerCert(signerInfo.getSid());
  		}
 			signData = new String(signedData.getContent());
 			
			subDN = cert.getSubjectDN().getName();
			serialNumber = cert.getSerialNumber();
	}
	catch(Exception e){
			StringBuffer sb =  new StringBuffer(1500);
			sb.append(e.getMessage());
			session.setAttribute("magiclineErr",sb.toString());
			response.sendRedirect("magicline_err.jsp");
			return;
	}
%>


<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>



<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	runMagicLine();
</script>

<div id="middle">
<h2>MagicLine Digital Signature Result</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Digital Signature Result </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
사용자가 선택한 인증서를 이용하여 원문데이터에 전자서명값을 추출하여 서버에서 전자서명 검증을 실행하며<br/>
서버는 사용자 인증서의 유효성 여부를 확인한다.</br>

                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="login_renewR.jsp" method="post" name="popForm">
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Digital Signature Request Data</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>Sign Data</td>
					<td><%=signData%></td>
				</tr>

			</table>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Certificate Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>USER Certificate Dn:</td>
					<td><%=subDN%></td>
				</tr>
				<tr>
					<td>Serial Number:</td>
					<td><%=serialNumber.toString()%></td>
				</tr>
					
			</table>
			</td>
		</tr>
	</tbody>
</table>
</form>

<p>&nbsp;</p>


<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>1.MagicLine class include </p>
			<pre class="programlisting">

&lt;@ page import="java.net.URLEncoder" &gt;
&lt;@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" &gt;
&lt;@ page import="com.dreamsecurity.jcaos.cms.SignedData" &gt;
&lt;@ page import="com.dreamsecurity.jcaos.cms.SignerInfo" &gt;
&lt;@ page import="com.dreamsecurity.jcaos.util.encoders.Base64" &gt;
&lt;@ page import="java.util.ArrayList"&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>2.MagicLine Server Digital Signature result </p>
<pre class="programlisting"><span class="emphasis"><em>&lt;!-- 다음과 같이 선언하여 MagicLine Server에서 전자서명을 수행하도록 한다. --&gt; </em></span>

&lt;%
	String encData= request.getParameter("encryptedData");
	X509Certificate cert = null;
	String subDN = "";
	java.math.BigInteger serialNumber = null;
	String signData = "";

	byte[] decData = Base64.decode(encData);
	try
	{
			SignedData signedData = SignedData.getInstance(decData);
  		signedData.verify();
  		ArrayList signerInfos = signedData.getSignerInfos();
  		for(int i=0; i< signerInfos.size(); i++)
  		{
  				SignerInfo signerInfo = (SignerInfo)signerInfos.get( i );
					cert = signedData.getSignerCert(signerInfo.getSid());
  		}
 			signData = new String(signedData.getContent());
 			
			subDN = cert.getSubjectDN().getName();
			serialNumber = cert.getSerialNumber();
	}
	catch(Exception e){
			StringBuffer sb =  new StringBuffer(1500);
			sb.append(e.getMessage());
			session.setAttribute("magiclineErr",sb.toString());
			response.sendRedirect("magicline_err.jsp");
			return;
	}

	<span class="emphasis"><em>//서명 검증 성공. </em></span>

%&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			</ul>
			</td>
		</tr>
	</tbody>
</table>

</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
	document.onload = setBreadcrumDiv();
	function setBreadcrumDiv() {
		var breadcrumbDiv = document.getElementById('breadcrumb-div');
		breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Digital Signature</td>';
	}
</script>

<jsp:include page="include/footer.jsp"></jsp:include>