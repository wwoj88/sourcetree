<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="com.dreamsecurity.jcaos.exception.IdentifyException"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletRequest" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %>
<%@ page import="com.dreamsecurity.magicline.MessageConstants" %>
<%@ page import="com.dreamsecurity.magicline.config.Logger" %>
<%
	DSHttpServletResponse dRes = null;
	DSHttpServletRequest dReq = null;

	X509Certificate cert = null;
	byte[] privatekeyRandom = null;
	String signType = "";
	String subDN = "";
	java.math.BigInteger serialNumber = null;
	int messageType;

	try {
		dRes = new DSHttpServletResponse(response);
		dReq = new DSHttpServletRequest(request);
		dRes.setRequest(dReq);

		//Infomation
		cert = dReq.getSignerCert();
		subDN = cert.getSubjectDN().getName();
		serialNumber = cert.getSerialNumber();
		messageType = dReq.getRequestMessageType();
		privatekeyRandom = dReq.getSignerRValue();
		signType = dReq.getSignType();

		//주민번호 본인확인 기능을 사용할 경우
		String ssn = dReq.getParameter("ssn").trim();
		if(null != ssn && !ssn.equals("")){
			cert.verifyVID(ssn,privatekeyRandom);
		}
	}catch(IdentifyException e){
		// 본인확인 Error
		StringBuffer sb = new StringBuffer(1500);
		sb.append(e.getMessage());
		session.setAttribute("magiclineErr", "[본인확인실패]"+sb.toString());
		response.sendRedirect("magicline_err.jsp");
		return;

	} catch (Exception e) {
		//인증서 검증
		//정책 검증
		StringBuffer sb = new StringBuffer(1500);
		sb.append(e.getMessage());
		session.setAttribute("magiclineErr",sb.toString());
		response.sendRedirect("magicline_err.jsp");
		return;
	}
%>


<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>



<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	runMagicLine();
</script>

<div id="middle">
<h2>MagicLine Login Result</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Certificate Login( cert ) Result </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
<br/>
<br/>
<br/>

                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="login_renewR.jsp" method="post" name="popForm">
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Login Request Data</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>Challenge</td>
					<td><%=dReq.getParameter("challenge")%></td>
				</tr>
				<tr>
					<td>SSN</td>
					<td><%=dReq.getParameter("ssn")%></td>
				</tr>
				<tr>
					<td>ETC Data</td>
					<td><%=dReq.getParameter("etc")%></td>
				</tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Certificate Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>USER Certificate Dn:</td>
					<td><%=subDN%></td>
				</tr>
				<tr>
					<td>Serial Number:</td>
					<td><%=serialNumber.toString() %></td>
				</tr>
				<tr>
					<td>Private Key Random:</td>
					<td><%=privatekeyRandom %></td>
				</tr>
				<tr>
					<td>Message Type:</td>
					<td><%=messageType %></td>
				</tr>
				<tr>
					<td>Sign Type:</td>
					<td><%=signType %></td>
				</tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</form>

<p>&nbsp;</p>


<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>1.MagicLine class include </p>
			<pre class="programlisting">
&lt;%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %&gt;
&lt;%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletRequest" %&gt;
&lt;%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %&gt;
&lt;%@ page import="com.dreamsecurity.jcaos.exception.IdentifyException"%&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>2.MagicLine Server login result </p>
<pre class="programlisting"><span class="emphasis"><em>&lt;!-- 다음과 같이 선언하여 MagicLine Server에서 로그인 검증을 수행하도록 한다. --&gt; </em></span>

&lt;%
	DSHttpServletResponse dRes = null;
	DSHttpServletRequest dReq = null;

	X509Certificate cert = null;
	byte[] privatekeyRandom = null;
	String signType = "";
	String subDN = "";
	java.math.BigInteger serialNumber = null;
	int messageType;


	try {
		dRes = new DSHttpServletResponse(response);<span class="emphasis"><em>// HttpResponse -&gt; DSHttpResponse </em></span>
		dReq = new DSHttpServletRequest(request);<span class="emphasis"><em>// HttpRequest -&gt; DSHttpRequest </em></span>
		dRes.setRequest(dReq);

		<span class="emphasis"><em>//Infomation (기타 정보는 java doc 참조.) </em></span>
		cert = dReq.getSignerCert();<span class="emphasis"><em>//사용자의 인증서 정보 (X509Certificate)  </em></span>
		subDN = cert.getSubjectDN().getName();<span class="emphasis"><em>//사용자 인증서 DN  </em></span>
		serialNumber = cert.getSerialNumber();<span class="emphasis"><em>//사용자 인증서 SerialNumber  </em></span>
		messageType = dReq.getRequestMessageType();<span class="emphasis"><em>// Client Message Type  </em></span>
		privatekeyRandom = dReq.getSignerRValue();<span class="emphasis"><em>//사용자 Private Random </em></span>

		<span class="emphasis"><em>//본인확인 검증. 주민번호가 있을경우 </em></span>
		String ssn = dReq.getParameter("ssn").trim();
		if(null != ssn && !ssn.equals("")){
			cert.verifyVID(ssn, dReq.getSignerRValue());
		}

	}catch(IdentifyException e){
	<span class="emphasis"><em>//본인확인 Error </em></span>
		out.println("[본인확인 실패] error message:"+e.getMessage());
		return;
	} catch (Exception e) {
		<span class="emphasis"><em>//인증서 검증  Error </em></span>
		<span class="emphasis"><em>//정책 검증 Error </em></span>
		<span class="emphasis"><em>//로그인 메세지 복호화 Error </em></span>
		out.println("[로그인 실패] error message:"+e.getMessage());
		return;
	}

	<span class="emphasis"><em>//인증서 검증, 정책검증 복호화 성공. </em></span>

%&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>3.복호화된 Request 사용  </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- Request Data --&gt; </em></span>
<span class="emphasis"><em>&lt;!-- http request는 암호화 된 값을 가지고 있고, DSHttpServletRequest dReq; 에서 복호화된 parameter을 얻을수 있다.  --&gt; </em></span>
&lt;%=dReq.getParameter("ssn")%&gt;
&lt;%=dReq.getParameter("etc")%&gt;

				</pre>
				<p>&nbsp;</p>
			</li>


			</ul>
			</td>
		</tr>
	</tbody>
</table>


</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
	document.onload = setBreadcrumDiv();
	function setBreadcrumDiv() {
		var breadcrumbDiv = document.getElementById('breadcrumb-div');
		breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Certificate Login</td>';
	}
</script>

<jsp:include page="include/footer.jsp"></jsp:include>