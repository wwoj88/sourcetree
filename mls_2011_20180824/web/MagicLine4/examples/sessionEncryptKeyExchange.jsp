<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="com.dreamsecurity.magicline.handle.info.DSKeyInfo"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %>

<%
/*
	DSHttpServletResponse res = new DSHttpServletResponse(response);
	res.setRequest(request);
	String challenge = "";
	try{
		challenge = res.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
		return;
	}
*/


%>



<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>


<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	// MagicLine Client Module 구동.
	runMagicLine();


	function keyExchange(form, type) {
	    onKeyExchange();
	    location.href = 'sessionSvrEncrypt.jsp';
	}






</script>
<div id="middle">
<h2>MagicLine Session Encryption</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Session Encryption Samples </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
						로그인을 수행하지 않고 onKeyExchange() 함수를 호출하여 서버와 클라이언트간에 키를 공유한다<br>
						공유후 테스트를 위해 [세션키 서버 암호화]페이지로 이동하여 서버 암복호화를 수행한다.
                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="sessionEncryptR.jsp" method="post" name="sessionForm">
<table style="width: 100%" class="styledLeft">
            		<thead>
	             		<tr>
				         <th colspan="2">Client Session Encryption Information</th>
			        </tr>
                    	</thead>
                    	<tbody>
				<tr>
					<td class="formRow">
						<table class="normal" cellspacing="0">
						<!--
				                    <tr>
				                        <td>Server Challenge code<font class="required">*</font></td>
				                        <td><input type="text" class="text-box-big" id="challenge"
				                                   name="challenge" type="text"
				                                   value="<%//=challenge%>"> &nbsp; Replay Attack을 방지하기 위한 Challenge code </td>
				                    </tr>
 						-->
				                    <tr >
				                        <td>onKeyExchange Start</td>
				                        <td></td>
				                    </tr>
								</table>
							</td>
						</tr>
	                    <tr>
	                        <td colspan="2" class="buttonRow">
	                            <input type="button" class="button" value="Encrypted(post) KeyExchange" onclick="keyExchange(sessionForm, '1');"/><br>
	                        </td>
	                    </tr>
                    </tbody>
                </table>

	<p>&nbsp;</p>

</form>

<p>&nbsp;</p>

<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">

			<li>
				<p>1.MagicLine 세션키 암호화 기동 및 Encrypt 함수 정의.</p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- MagicLine Javascript include --&gt; </em></span>
&lt;script language="javascript" src="../js/deployJava.js"&gt;&lt;/script&gt;
&lt;script language="javascript" src="../js/PluginDetect.js"&gt;&lt;/script&gt;
&lt;script language="javascript" src="../js/MagicLine.js"&gt;&lt;/script&gt;
&lt;script language='javascript'&gt;
<span class="emphasis"><em>	// MagicLine Client Module 구동.</em></span>
	runMagicLine();

		function doAction(sessionForm){
		if (MagicLine_install == true){
<span class="emphasis"><em>			// MagicLine.js 에 있는 onKeyExchange() 함수를 호출한다. </em></span>
			onKeyExchange();
		}
	}
&lt;/script&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			</ul>
			</td>
		</tr>
	</tbody>
</table>

</div><!-- DIV END  -->

<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
    document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Certificate Login</td>';
    }
</script>

<jsp:include page="include/footer.jsp"></jsp:include>