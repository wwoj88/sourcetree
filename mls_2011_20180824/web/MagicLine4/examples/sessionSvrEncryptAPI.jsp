<%@ page contentType="text/html;charset=euc-kr"%>
<%@ taglib prefix="DS" uri="/WEB-INF/ds-encrypt.tld" %>
<%@ page import="com.dreamsecurity.magicline.handle.info.DSKeyInfo" %>
<%@ page import="com.dreamsecurity.magicline.ex.DSJCrypt" %>

<%
	String toBeEncData = "암호화 할 데이터. test124";
	String encText1 = "";
	
	try {
		// 세션키 획득
		DSKeyInfo keyinfo = (DSKeyInfo)session.getAttribute("DSSession");
		byte[] key = keyinfo.getServerKey();
		byte[] iv = keyinfo.getServerIV();
		
		// 암호화
		encText1 = new String(DSJCrypt.b64encode(DSJCrypt.encrypt(key, iv, toBeEncData.getBytes())));

	} catch (Exception e) {
		StringBuffer sb =  new StringBuffer(1500);
		sb.append(e.getMessage());
		session.setAttribute("magiclineErr",sb.toString());
		response.sendRedirect("magicline_err.jsp");
		return;
	}
%>



<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>


<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	// MagicLine Client Module 구동.
	runMagicLine();
	// MagicLine Client 인증서 창을 호출한다.
	
	function confirm_Decrypt(data)
	{
		nResult = Init();
		if( nResult == 117)
			return nResult;
		nResult = document.MagicLine.CRYPT_Decrypt(mlProp.SiteID, data);
		return nResult;		
	}
	
	function DecryptEach(data)
	{
		nResult = confirm_Decrypt(data);
		strReturnData = document.MagicLine.GetReturnData();
		if( nResult != 1 )
		{
			alert(strReturnData);
			return "";			
		}

		if(mlEnv.debug){
			alert("decrypted each data = " + strReturnData);
		}

		return strReturnData;
	}
	
	function doAction(sessionForm){

	}
</script>
<div id="middle">
<h2>MagicLine Session Encryption</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Session Encryption Samples </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
						로그인 후 서버와 클라이언트 사이에 공유한 세션키를 이용하여 암복호화 한다.  <br>
						서버 세션암호화는 jsp TagLibrary를 이용하여 직접 Contents를 암호화 한다.<br>
						클라이언트에서는 암호화된 데이터를 JavaScript를 이용하여 복호화 한다.<br>
                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<table style="width: 100%" class="styledLeft">
            		<thead>
	             		<tr>
				         <th colspan="2">Server Session Encryption </th>
			        </tr>
                    	</thead>
                    	<tbody>
					<tr>
						<td class="formRow">
							<b>암호화된 데이터!!</b><br/><br/>
	
							<script language='javascript'>
								document.write(DecryptEach("<%=encText1%>"));
							</script>
										
<table><tr><td></td></tr></table> <!-- Tag -->
							<br/><br/>
						</td>
					</tr>

                    </tbody>
                </table>

	<p>&nbsp;</p>


<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>1.MagicLine TagLibrary include  </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- 해당 JSP 상단에 다음과 같이  Tag library 를 Include 하여 사용하고 --&gt; </em></span>
<span class="emphasis"><em>&lt;!-- ds-encrypt.tld 는 해당 Context WEB-INF에 복사하여 사용한다. --&gt; </em></span>
&lt;%@ taglib prefix="DS" uri="/WEB-INF/ds-encrypt.tld" %&gt;

				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>2.MagicLine Client Module 구동 (복호화시 사용) </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- MagicLine Javascript include --&gt; </em></span>
&lt;script language="javascript" src="../js/deployJava.js"&gt;&lt;/script&gt;
&lt;script language="javascript" src="../js/PluginDetect.js"&gt;&lt;/script&gt;
&lt;script language="javascript" src="../js/MagicLine.js"&gt;&lt;/script&gt;
&lt;script language='javascript'&gt;
<span class="emphasis"><em>	// MagicLine Client Module 구동.</em></span>
	runMagicLine();
&lt;/script&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>3.MagicLine Server Session Encrypt시 TagLibrary사용법. </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- 암호화 할 데이터를 다음과 같이 TagLibrary로 감싼다. --&gt; </em></span>


&lt;DS:ENCRYPT&gt;
  &lt;ENCRYPT_DATA&gt;

암호화될 데이터  <span class="emphasis"><em>&lt;!-- Text --&gt; </em></span>
&lt;input type="hidden" id="etc" name="etc" value=""/&gt; <span class="emphasis"><em>&lt;!-- Input --&gt; </em></span>
&lt;table&gt;&lt;tr&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt; <span class="emphasis"><em>&lt;!-- Tag --&gt; </em></span>

  &lt;/DS:ENCRYPT&gt;
&lt;/ENCRYPT_DATA&gt;

				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>4.MagicLine Server Session Encrypt 확인 </p>
				<pre class="programlisting"><span class="emphasis"><em>브라우저의 html 소스보기를 하면 다음과 같이 암호화 되어있음을 확인할 수 있다.</em></span>
&lt;script language ='javaScript'&gt;
var data = '';
data = '&lt;MagicLine:ENCRYTPED_DATA>AAAAAAABQOlPqjB7mvYSNVrDKUNgfjCMo45v6KmM9Y7oHbQtfVZvMT'
     + 'SRm0nrXVr45yOhus5rxbLy44wenQs8F8neI3+SFuxsJUczq+F+8b7J/6aAgxu/MNHm6yLtBbrbT/Uqn/'
     + 'MXot1iL1h+h1uQar9lzn9sd76DUsADXVRajTMOepOrlFdE+s7rXnQQDluO9MwfiP0GfdFl0++U/zoyb5'
     + 'd5KvUx+vjrCenmTm7eKvJ3rch1B+XsXv9qT+g2t09Tq7AeFbBzwVZItsDCK8NpWGClrgnSEWyHYWV+Ht'
     + 'YdpcOYXSGB9f9iaGdtW25boFw+rQDgMDK7RpeCADtZGrEEw/5vaW0at72059LRWUwSq/iH5eNJ/sjMot'
     + 'kv7nmL+2o4RCz6LVzzc0KIIuv0isPK/i52J5auSI1ovgGKJHI1DvMdMExl8SPS&lt;/MagicLine:ENCRYT'
     + 'PED_DATA&gt;';
document.write(Decrypt(data)); <span class="emphasis"><em>// MagicLine.js 공통함수를 호출하여 서버세션 암호 데이터를 클라이언트 모듈로 복호화 한다. </em></span>
&lt;/script&gt;
				</pre>
				<p>&nbsp;</p>
			</li>
			</ul>
			</td>
		</tr>
	</tbody>
</table>



</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
    document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Session Encryption.</td>';
    }
</script>

<jsp:include page="include/footer.jsp"></jsp:include>