<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>

<script type="text/javascript" src="NTSMagicLine4/js/deployJava.js"></script>
<script type="text/javascript" src="NTSMagicLine4/js/PluginDetect.js"></script>
<script type="text/javascript" src="NTSMagicLine4/js/MagicLine.js"></script>

<style type = "text/css">
<!--
.box
{
position: relative;
border: dashed 1px #dadada;
margin-top: 15px;
background: inherit;
color: #AAB165;
}

.box_inner
{
border: solid 1px #fff;
padding: 15px;
background: #fffff0 /*url('images/a4.gif') repeat-x*/;
color: inherit;
}
-->
</style>

<h2>전자서명 및 검증</h2>
<div id="workArea"><!-- DIV START  -->
<h3 style = "color:BLUE;">웹 서버 전자서명 후 웹 브라우저 검증</h3>
<div class="box">
		<div class="box_inner">
			웹 서버에서 서명용 인증서를 이용하여 서명한 데이터를 검증하고,<br>
			서명 인증서 정보를 보여준다.<br>
		</div>
	</div>
	
	<h4 style = "color:SKYBLUE;">실행</h4>
	
	<form name="send_form" action="serversignature.jsp" method="post">
	
		<div align="center">
		<table id="input_space" cellspacing="0" cellpadding="2" width="100%" align="center">
		<tr>
		<td class="input_field" width="30%">원본 메시지</td>
		<td class="input_data"><textarea name="OrgMsg" cols="60" rows="2" ></textarea></td>
		</tr>
		<tr>			
		<td class="input_field1" width="30%">서명 메시지</td>
		<td class="input_data"><textarea name="SignMsg" cols="60" rows="8" ></textarea></td>
		</tr>
		<tr>
		<td class="input_field" width="30%">클라이언트 검증 메시지</td>
		<td class="input_data"><textarea name="VerifyMsg" cols="60" rows="2" ></textarea></td>
		</tr>
		</table>
		</div>
		<div id="button_space" align="center">	
			<input type="button" value="메시지 검증" onclick="VerifyData()"/>		
		    <input type="button" value="이전" onclick="javascript:history.back()"/>
		</div>		
	</form>
	
</div><!-- DIV END  -->
</html>
<jsp:include page="include/footer.jsp"></jsp:include>