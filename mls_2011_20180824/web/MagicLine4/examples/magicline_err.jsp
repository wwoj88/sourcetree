<%@page import="com.dreamsecurity.magicline.util.DSUtil"%>
<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %>

<%

	String desc = (String)session.getAttribute("magiclineErr");
	String errmsg = DSUtil.getErrMessge(desc);




%>

<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>
<div id="middle">
<h2>MagicLine Error Page.</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Error Message </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> Error  :
					 				</nobr></td>
					<td style="border: 0;">
					<%= errmsg%>

                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>

	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
					<%= desc%>

                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>

</div><!-- DIV END  -->

<script type="text/javascript">
    document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Error Page</td>';
    }
</script>

<jsp:include page="include/footer.jsp"></jsp:include>