<%@ page contentType="text/html;charset=euc-kr"%>
<%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %>
<%@ page import="com.dreamsecurity.jcaos.cms.SignedData" %>
<%@ page import="com.dreamsecurity.jcaos.cms.SignerInfo" %>
<%@ page import="com.dreamsecurity.jcaos.util.encoders.Base64" %>
<%@ page import="com.dreamsecurity.magicline.processor.ProcessorChecker" %>
<%
    String[] signData = new String[3];
    String[] content = new String[3];
    X509Certificate[] signerCerts = new X509Certificate[3];
    String[] verify_res = new String[3];
    
	try
	{
		for (int i = 0; i < 3; i++)
		{
			// 1. 전자서명 수신
			signData[i] = request.getParameter("signtext" + (i + 1));
			SignedData sign = SignedData.getInstance(Base64.decode(signData[i]));
			
			// 2. 전자서명 검증
			sign.verify();
			
			// 3. 원문 획득
			content[i] = new String(sign.getContent(), "euc-kr");
			
			// 4. 서명자 인증서 획득
			SignerInfo sinfo = (SignerInfo)sign.getSignerInfos().get(0);
			X509Certificate[] certs = new X509Certificate[1];
			signerCerts[i] = certs[0] = sign.getSignerCert(sinfo.getSid());
			
			// 5. 인증서 검증
			try
			{
	            ProcessorChecker pc = new ProcessorChecker();
	            pc.verifyCert(certs);
	            verify_res[i] = "succeeded";
	        }
	        catch (Exception ex)
	        {
	           verify_res[i] = ex.getMessage();
	        }
        }
	}
	catch (Exception e)
	{
        StringBuffer sb =  new StringBuffer(1500);
        sb.append(e.getMessage());
        session.setAttribute("magiclineErr",sb.toString());
        response.sendRedirect("magicline_err.jsp");
        return;
	}
%>

<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>

<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	runMagicLine();
</script>

<div id="middle">
<h2>MagicLine Digital Signature for Multi-Items Result</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Digital Signature for Mutli-Items Result </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
사용자로부터 수신한 전자서명값을 추출하여 서버에서 전자서명 검증을 실행한 후 원문을 획득하고,<br/>
서버는 사용자 인증서의 유효성 여부를 확인한다.</br>

                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="login_renewR.jsp" method="post" name="popForm">
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Digital Signature for Multi-Items Request Data</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
                <tr>
                    <td>Sign Data1</td>
                    <td><%=signData[0]%></td>
                </tr>
                <tr>
                    <td>Sign Data2</td>
                    <td><%=signData[1]%></td>
                </tr>
                <tr>
                    <td>Sign Data3</td>
                    <td><%=signData[2]%></td>
                </tr>

			</table>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Certificate Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
                <tr>
                    <td><h5>Signed Data 1</h5></td>
                    <td></td>
                </tr>
                <tr>
                    <td>original data</td>
                    <td><%=content[0]%></td>
                </tr>
                <tr>
                    <td>signer certificate</td>
                    <td><%=signerCerts[0].getSubjectDN().getName() %></td>
                </tr>
                <tr>
                    <td>signer certificate verification result</td>
                    <td><%=verify_res[0] %></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><h5>Signed Data 2</h5></td>
                    <td></td>
                </tr>
                <tr>
                    <td>original data</td>
                    <td><%=content[1]%></td>
                </tr>
                <tr>
                    <td>signer certificate</td>
                    <td><%=signerCerts[1].getSubjectDN().getName() %></td>
                </tr>
                <tr>
                    <td>signer certificate verification result</td>
                    <td><%=verify_res[1] %></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><h5>Signed Data 3</h5></td>
                    <td></td>
                </tr>
                <tr>
                    <td>original data</td>
                    <td><%=content[2]%></td>
                </tr>
                <tr>
                    <td>signer certificate</td>
                    <td><%=signerCerts[2].getSubjectDN().getName() %></td>
                </tr>
                <tr>
                    <td>signer certificate verification result</td>
                    <td><%=verify_res[2] %></td>
                </tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>

</form>

<p>&nbsp;</p>






<table style="width: 100%" class="styledLeft">
    <thead>
        <tr>
            <th >Program Guide (Examples)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="formRow">
            <ul type="disc">
            <li>
                <p>1.MagicLine class include </p>
            <pre class="programlisting">


&lt;@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" &gt;
&lt;@ page import="com.dreamsecurity.jcaos.cms.SignedData" &gt;
&lt;@ page import="com.dreamsecurity.jcaos.cms.SignerInfo" &gt;
&lt;@ page import="com.dreamsecurity.jcaos.util.encoders.Base64" &gt;
&lt;@ page import="com.dreamsecurity.magicline.processor.ProcessorChecker" &gt;
                </pre>
                <p>&nbsp;</p>
            </li>

            <li>
                <p>2.MagicLine Server Digital Signature result </p>
<pre class="programlisting"><span class="emphasis"><em>&lt;!-- 다음과 같이 선언하여 MagicLine Server에서 전자서명 검증을 수행하도록 한다. --&gt; </em></span>

&lt;%
    String[] signData = new String[3];
    String[] content = new String[3];
    X509Certificate[] signerCerts = new X509Certificate[3];
    String[] verify_res = new String[3];
    
    try
    {
        for (int i = 0; i < 3; i++)
        {
            // 1. 전자서명 수신
            signData[i] = request.getParameter("signtext" + (i + 1));
            SignedData sign = SignedData.getInstance(Base64.decode(signData[i]));
            
            // 2. 전자서명 검증
            sign.verify();
            
            // 3. 원문 획득
            content[i] = new String(sign.getContent(), "euc-kr");
            
            // 4. 서명자 인증서 획득
            SignerInfo sinfo = (SignerInfo)sign.getSignerInfos().get(0);
            X509Certificate[] certs = new X509Certificate[1];
            signerCerts[i] = certs[0] = sign.getSignerCert(sinfo.getSid());
            
            // 5. 인증서 검증
            try
            {
                ProcessorChecker pc = new ProcessorChecker();
                pc.verifyCert(certs);
                verify_res[i] = "succeeded";
            }
            catch (Exception ex)
            {
               verify_res[i] = ex.getMessage();
            }
        }
    }
    catch (Exception e)
    {
        StringBuffer sb =  new StringBuffer(1500);
        sb.append(e.getMessage());
        session.setAttribute("magiclineErr",sb.toString());
        response.sendRedirect("magicline_err.jsp");
        return;
    }

    <span class="emphasis"><em>//서명 검증 성공. </em></span>

%&gt;
                </pre>
                <p>&nbsp;</p>
            </li>

            </ul>
            </td>
        </tr>
    </tbody>
</table>


</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
	document.onload = setBreadcrumDiv();
	function setBreadcrumDiv() {
		var breadcrumbDiv = document.getElementById('breadcrumb-div');
		breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Digital Signature</td>';
	}
</script>

<jsp:include page="include/footer.jsp"></jsp:include>