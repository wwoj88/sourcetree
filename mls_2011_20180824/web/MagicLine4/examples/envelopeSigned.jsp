<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse"%>
<%
	DSHttpServletResponse res = new DSHttpServletResponse(response);
	res.setRequest(request);
	String challenge = "";
	try{
		challenge = res.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
		return;
	}	
%>

<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>

<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	runMagicLine();

	function doAction(envelopeForm){
		if (MagicLine_install == true)
			EnvelopedSignData(envelopeForm);
	}

	function typeChange(sel){
		var val  = sel.value;
		if(val == 'a'){
			document.envelopeForm.action = 'envelopeSignedRAPI.jsp';
		}else{
			document.envelopeForm.action = 'envelopeSignedR.jsp';
		}
	}
</script>
<div id="middle">
<h2>MagicLine Envelope Encryption & Signed</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Envelope Encryption & Signed Samples </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
						클라이언트에서 서버인증서의 공개키를 이용하여 메세지를 암호화고 암호화된 메세지를 Envelope 형태로 가공하여
						전자서명 후 서버에 전달합니다.<br>
                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="envelopeSignedR.jsp" method="post" name="envelopeForm">
<table style="width: 100%" class="styledLeft">
            		<thead>
	             		<tr>
				         <th colspan="2">Client Encryption Information</th>
			        </tr>
                    	</thead>
                    	<tbody>
				<tr>
					<td class="formRow">
						<table class="normal" cellspacing="0">

				                    <tr>
				                        <td>Server Challenge code<font class="required">*</font></td>
				                        <td><input type="text" class="text-box-big" id="challenge"
				                                   name="challenge" type="text"
				                                   value="<%=challenge%>"> &nbsp; Replay Attack을 방지하기 위한 Challenge code </td>
				                    </tr>

				                    <tr >
				                        <td>plain text1<font class="required">*</font></td>
				                        <td><input class="text-box-big" id="plaintext"
				                                   name="text1" type="text"
				                                   value=""> &nbsp; 암호화를 실행할 원문 데이터를 입력한다.</td>
				                    </tr>
				                    <tr >
				                        <td>plain text2<font class="required">*</font></td>
				                        <td><input class="text-box-big" id="plaintext"
				                                   name="text2" type="text"
				                                   value=""> &nbsp; 암호화를 실행할 원문 데이터를 입력한다.</td>
				                    </tr>
				                    <tr >
				                        <td>plain text3<font class="required">*</font></td>
				                        <td><input class="text-box-big" id="plaintext"
				                                   name="text3" type="text"
				                                   value=""> &nbsp; 암호화를 실행할 원문 데이터를 입력한다.</td>
				                    </tr>

								</table>
							</td>
						</tr>
	                    <tr>
	                        <td colspan="2" class="buttonRow">
	                        Submit Type <select onchange="typeChange(this)"><option value="h">HTTP</option><option value="a">API</option></select>
	                            <input type="button" class="button" value="Send"  onclick="doAction(envelopeForm);return false;" />
	                            <input type="reset" class="button" value="Reset"/>
	                        </td>
	                    </tr>
                    </tbody>
                </table>

	<p>&nbsp;</p>

	<table class="styledLeft" id="sgTable"  width="100%">
	<thead>
		<tr>
			<th colspan="3" >Server Send Envelope Encryption & Signed Data.</th>

		</tr>
	</thead>
	<tbody>
		<tr bgcolor="white">
			<td width="10px" style="text-align: center;">1</td>
			<td width="200px" ><nobr>
			sessionID </nobr></td>
			<td><input type="text" name="sessionIDT"
				size="58" value="" />&nbsp;현재 Session Id</td>
		</tr>
		<tr bgcolor="#EEEFFB">
			<td width="10px" style="text-align: center;">2</td>
			<td><nobr>
			Plain Text </nobr></td>
			<td>
			<textarea name="pText" rows="2" cols="66"></textarea>
			&nbsp;암호화할 데이터</td>
		</tr>
		<tr bgcolor="white">
			<td width="10px" style="text-align: center;">3</td>
			<td><nobr>
			Encrypt Text </nobr></td>
			<td>
			<textarea name="eText" rows="5" cols="66"></textarea>
			&nbsp;암호화 되어 전송될 데이터</td>
	</tbody>
</table>
</form>

<p>&nbsp;</p>

<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">

			<li>
				<p>1.MagicLine Challenge 발급. </p>
				<pre class="programlisting"><span class="emphasis"><em>// Replay Attack 방지 Challenge 발급 </em></span>
&lt;%
	String challenge = "";
	try{
	DSHttpServletResponse res = new DSHttpServletResponse(response);
	res.setRequest(request);
	challenge = res.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
		return;
	}
%&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>2.MagicLine 전자봉투 & 전자서명 기동 및 EncryptedSignData 함수 정의.</p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- MagicLine Javascript include --&gt; </em></span>
&lt;script language="javascript" src="../js/deployJava.js"&gt;&lt;/script&gt;
&lt;script language="javascript" src="../js/PluginDetect.js"&gt;&lt;/script&gt;
&lt;script language="javascript" src="../js/MagicLine.js"&gt;&lt;/script&gt;
&lt;script language='javascript'&gt;
<span class="emphasis"><em>	// MagicLine Client Module 구동.</em></span>
	runMagicLine();

		function doAction(envelopeForm){
		if (MagicLine_install == true)
<span class="emphasis"><em>			// MagicLine.js 에 있는 EnvelopeSignData 함수를 호출한다. </em></span>
			EnvelopedSignData(envelopeForm);
	}
&lt;/script&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>3.MagicLine Digital signature & Envelope Encryption Form 작성. </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- Envelope Form --&gt; </em></span>
&lt;form action="..(Response Url)" method="post" name="envelopeForm"&gt;
&lt;input type="text" id="plaintext" name="text" value=""/&gt; <span class="emphasis"><em>&lt;!-- 암호화를 실행할 원문 데이터를 입력한다. --&gt; </em></span>
&lt;input type="button" value="Send" onclick="doAction(envelopeForm);return false;" /&gt; <span class="emphasis"><em>&lt;!-- 암호화 실행 및 데이터 전송 --&gt; </em></span>
&lt;/form&gt;

				</pre>
				<p>&nbsp;</p>
			</li>

			</ul>
			</td>
		</tr>
	</tbody>
</table>

</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
    document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Envelope</td>';
    }
</script>

<jsp:include page="include/footer.jsp"></jsp:include>