<%@ page contentType="text/html;charset=euc-kr"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %>

<%
	DSHttpServletResponse res = new DSHttpServletResponse(response);
	res.setRequest(request);
	String challenge = "";
	try{
		challenge = res.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
		return;
	}
%>

<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>


<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	// MagicLine Client Module 구동.
	runMagicLine();

	// MagicLine Client 인증서 창을 호출한다.
	function doAction(form){
        if (MagicLine_install == true)
       	{
            var i;
			if(Login(form)==100)
				return;
            for (i = 0; i < 3; i++)
           	{
                var plain_data = eval("form.plaintext" + (i+1));
                var sign_data = eval("form.signtext" + (i+1));                
                sign_data.value = SignItemByOption(plain_data.value);
            }
       	}
	}

	function typeChange(sel){
		var val  = sel.value;
		if(val == 'a'){
			document.signForm.action = 'signedRAPI.jsp';
		}else{
			document.signForm.action = 'signedR.jsp';
		}
	}
	
    function SignatureOpt(sel) {
        var val  = sel.value;
        if(val == '0'){
            mlProp.CertOption = 0;
        }else if(val == '1'){
            mlProp.CertOption = 1;
        }else{
            mlProp.CertOption = 2;
        }
    }

</script>
<div id="middle">
<h2>MagicLine Digital Signature for Multi-Items</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Certificate Digital Signature Samples (for Multi-items) </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
						입력한 데이터를 전자서명합니다. 여러 건에 대해 개별 전자서명이 가능합니다. 이예제는 3건의 전자서명을 생성해서 서버로 전송하는 샘플입니다.<br>
                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="signedDataMultiItemR.jsp" method="post" name="signForm">
<input type="hidden" class="text-box-big" id="challenge"
						name="challenge" type="text" value="<%=challenge%>">
<table style="width: 100%" class="styledLeft">
            		<thead>
	             		<tr>
				         <th colspan="2">Client Digital Signature Information</th>
			        </tr>
                    	</thead>
                    	<tbody>
				<tr>
					<td class="formRow">
						<table class="normal" cellspacing="0">
					                <tr>
					                    <td><h5>Signed Data 1</h5></td>
					                    <td></td>
					                </tr>
				                    <tr >
				                        <td>plain text (원문 데이터)<font class="required">*</font></td>
				                        <td><input class="text-box-big" id="plaintext1"
				                                   name="signData" type="text"
				                                   value="샘플 SignedData 1"> &nbsp;</td>
				                    </tr>
                                    <tr bgcolor="white">
                                        <td><nobr>
                                        Signed Text (전자서명 데이터)</nobr></td>
                                        <td>
                                        <textarea name="signtext1" id="signtext1" rows="5" cols="80"></textarea>
                                        </td>
                                    </tr>                                   
					                <tr>
					                    <td></td>
					                    <td></td>
					                </tr>
					                <tr>
					                    <td><h5>Signed Data 2</h5></td>
					                    <td></td>
					                </tr>
                                    <tr >
                                        <td>plain text (원문 데이터)<font class="required">*</font></td>
                                        <td><input class="text-box-big" id="plaintext2"
                                                   name="signData" type="text"
                                                   value="샘플 SignedData 2"> &nbsp;</td>
                                    </tr>
                                    <tr bgcolor="white">
                                        <td><nobr>
                                        Signed Text (전자서명 데이터)</nobr></td>
                                        <td>
                                        <textarea name="signtext2" id="signtext2" rows="5" cols="80"></textarea>
                                        </td>
                                    </tr>                                   
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Signed Data 3</h5></td>
                                        <td></td>
                                    </tr>
                                    <tr >
                                        <td>plain text (원문 데이터)<font class="required">*</font></td>
                                        <td><input class="text-box-big" id="plaintext3"
                                                   name="signData" type="text"
                                                   value="샘플 SignedData 3"> &nbsp;</td>
                                    </tr>
                                    <tr bgcolor="white">
                                        <td><nobr>
                                        Signed Text (전자서명 데이터)</nobr></td>
                                        <td>
                                        <textarea name="signtext3" id="signtext3" rows="5" cols="80"></textarea>
                                        </td>
                                    </tr>                                   
								</table>
							</td>
						</tr>
                        <tr>
                            <td colspan="2" class="buttonRow">
                            전자서명 타입 <select onchange="SignatureOpt(this)"><option value="0">로그인한 인증서로 전자서명 수행</option><option value="1">사용 가능한 모든 인증서 중 선택해서 전자서명</option>
                                          <option value="2">로그인한 인증서로 전자서명 수행 (자동)</option></select>
                                <input name="Submit" type="button" class="button"
                                       value="전자서명"
                                        onclick="doAction(signForm);return false;"/>
                                <input name="Submit" type="submit" class="button"
                                       value="전송"/>
                                <input type="reset" class="button" value="Reset"/>
                            </td>
	                    </tr>

                    </tbody>
                </table>

	<p>&nbsp;</p>
</form>

<p>&nbsp;</p>

<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>1.MagicLine 전자서명 기동 및 다건 전자서명 함수 호출.</p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- MagicLine Javascript include --&gt; </em></span>
&lt;script language="javascript" src="../js/deployJava.js"&gt;&lt;/script&gt;
&lt;script language="javascript" src="../js/PluginDetect.js"&gt;&lt;/script&gt;
&lt;script language="javascript" src="../js/MagicLine.js"&gt;&lt;/script&gt;
&lt;script language='javascript'&gt;
<span class="emphasis"><em>	// MagicLine Client Module 구동.</em></span>
	runMagicLine();

    function doAction(form){
        if (MagicLine_install == true)
        {
            var i;
            for (i = 0; i < 3; i++)
            {
                var plain_data = eval("form.plaintext" + (i+1));
                var sign_data = eval("form.signtext" + (i+1));                
<span class="emphasis"><em>                // MagicLine.js 에 있는  SignItemByOption 함수를 매 건별로 호출한다. </em></span>
<span class="emphasis"><em>                // 호출 시에는 mlProp.CertOption 을 지정해서 서명 건별로 비밀번호를 확인할 지 연속서명할 지를 지정한다. </em></span>
                sign_data.value = SignItemByOption(plain_data.value);
            }
        }
    }
&lt;/script&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>2.MagicLine Digital Signature Form 작성. </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- Digital Signature Form --&gt; </em></span>
&lt;form action="..(Response Url)" method="post" name="signForm"&gt;
&lt;input type="text" id="plaintext" name="text" value=""/&gt; <span class="emphasis"><em>&lt;!-- 전자서명을 실행할 원문 데이터를 입력한다. --&gt; </em></span>
&lt;input type="button" value="전자서명" onclick="doAction(signForm);return false;" /&gt; <span class="emphasis"><em>&lt;!-- 전자서명 실행 --&gt; </em></span>
&lt;/form&gt;

				</pre>
				<p>&nbsp;</p>
			</li>

			</ul>
			</td>
		</tr>
	</tbody>
</table>

</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
    document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Digital Signature</td>';
    }
</script>

<jsp:include page="include/footer.jsp"></jsp:include>