<%@ page contentType="text/html;charset=utf-8"%>

<%@ page import="com.dreamsecurity.magicline.handle.RequestContent" %>
<%@ page import="com.dreamsecurity.magicline.processor.Processor" %>
<%@ page import="com.dreamsecurity.magicline.processor.ProcessorFactory" %>
<%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %>
<%@page import="com.dreamsecurity.jcaos.cms.SignedData" %>
<%@ page import="com.dreamsecurity.jcaos.exception.IdentifyException"%>
<%@page import="com.dreamsecurity.jcaos.util.encoders.Base64" %>
<%@ page import="java.net.URLEncoder" %>
<%

	String encData= request.getParameter("encryptedData");
	System.out.print("aaaaa");
	Base64 base64 = new Base64();
	System.out.print("bbbbbbb");
	byte[] decData = base64.decode(encData);
	System.out.print("ccccccccc");
	SignedData signedData = new SignedData();
	System.out.print("dddddddd");
  signedData.verify(decData);
  System.out.print("eeeeeeeeeeee");
    return;
////	X509Certificate cert = null;
//	byte[] privatekeyRandom = null;
//	String signType = "";
//	String subDN = "";
//	java.math.BigInteger serialNumber = null;
//	int messageType;
//	String signData = "";
//
//	try {
//
//		RequestContent reqContent = new RequestContent(query);
//		ProcessorFactory factory = new ProcessorFactory();
//		Processor process = factory.createProcessor(reqContent);
//		process.execute();
//
//		//Infomation
//		X509Certificate cert = process.getSignerCerts()[0];
//		subDN = cert.getSubjectDN().getName();
//		serialNumber = cert.getSerialNumber();
//		messageType = process.getMessageType();
//	    privatekeyRandom = process.getPrivateKeyRandom();
//	    signData = process.getParameter("signData");
////		signType =
//
//	} catch (Exception e) {
//		// 인증서 검증 실패
//		// 인증서 정책검증 실패
//		// 본인확인 실패
//		// 복화화 실패
//		StringBuffer sb =  new StringBuffer(1500);
//		sb.append(e.getMessage());
//		session.setAttribute("magiclineErr",sb.toString());
//		response.sendRedirect("magicline_err.jsp");
//		return;
//	}
%>

<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>



<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	runMagicLine();
</script>

<div id="middle">
<h2>MagicLine Digital Signature Result</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Digital Signature Result </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
사용자가 선택한 인증서를 이용하여 원문데이터에 전자서명값을 추출하여 서버에서 전자서명 검증을 실행하며<br/>
서버는 사용자 인증서의 유효성 여부를 확인한다.</br>

                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="login_renewR.jsp" method="post" name="popForm">
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Digital Signature Request Data</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>Sign Data</td>
					<td><%= signData %></td>
				</tr>

			</table>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Certificate Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>USER Certificate Dn:</td>
					<td><%=subDN%></td>
				</tr>
				<tr>
					<td>Serial Number:</td>
					<td><%=serialNumber.toString() %></td>
				</tr>
				<tr>
					<td>Private Key Random:</td>
					<td><%=privatekeyRandom %></td>
				</tr>
				<tr>
					<td>Message Type:</td>
					<td><%=messageType %></td>
				</tr>
				<tr>
					<td>Sign Type:</td>
					<td><%=signType %></td>
				</tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</form>

<p>&nbsp;</p>


<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>1.MagicLine class include </p>
			<pre class="programlisting">
&lt;%@ page import="com.dreamsecurity.magicline.handle.RequestContent" %&gt;
&lt;%@ page import="com.dreamsecurity.magicline.processor.Processor" %&gt;
&lt;%@ page import="com.dreamsecurity.magicline.processor.ProcessorFactory" %&gt;
&lt;%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %&gt;
&lt;%@ page import="com.dreamsecurity.jcaos.exception.IdentifyException"%&gt;
&lt;%@ page import="java.net.URLEncoder" %&gt;

				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>2.MagicLine Server Digital Signature result </p>
<pre class="programlisting"><span class="emphasis"><em>&lt;!-- DSHttpRequest를 사용할 수 없는 경우, Request 객채를 넘겨줄 수 없는 경우, 다음과 같이 MagicLine API 를 직접 사용할 수 있다. --&gt; </em></span>

&lt;%
	String query = request.getParameter("encryptedData");

	byte[] privatekeyRandom = null;
	String signType = "";
	String subDN = "";
	java.math.BigInteger serialNumber = null;
	int messageType;
	String signData = "";

	try {

		RequestContent reqContent = new RequestContent(query); <span class="emphasis"><em>// 암호화된 데이터를  RequestContent에 담아 생성한다. </em></span>
		ProcessorFactory factory = new ProcessorFactory();     <span class="emphasis"><em>// ProcessorFactory 객체를 생성한다. </em></span>
		Processor process = factory.createProcessor(reqContent); <span class="emphasis"><em>// ProcessorFactory 에서 해당 암호화 데이터에대한 Process를 축출한다. </em></span>
		process.execute();  <span class="emphasis"><em>// Processor 처리. </em></span>

		<span class="emphasis"><em>//Infomation (기타 정보는 java doc 참조.) </em></span>
		X509Certificate cert = process.getSignerCerts()[0];<span class="emphasis"><em>//사용자의 인증서 정보 (X509Certificate)</em></span>
		subDN = cert.getSubjectDN().getName();<span class="emphasis"><em>//사용자 인증서 DN  </em></span>
		serialNumber = cert.getSerialNumber();<span class="emphasis"><em>//사용자 인증서 SerialNumber  </em></span>
		messageType = process.getMessageType();<span class="emphasis"><em>// Client Message Type  </em></span>
	    privatekeyRandom = process.getPrivateKeyRandom();<span class="emphasis"><em>//사용자 Private Random </em></span>
	    signData = process.getParameter("signData");

	} catch (Exception e) {
		out.println("[전자서명 실패] error message:" + e.getMessage());
		return;
	}

	<span class="emphasis"><em>//서명 검증 성공. </em></span>

%&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			</ul>
			</td>
		</tr>
	</tbody>
</table>

</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
	document.onload = setBreadcrumDiv();
	function setBreadcrumDiv() {
		var breadcrumbDiv = document.getElementById('breadcrumb-div');
		breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Digital Signature</td>';
	}
</script>

<jsp:include page="include/footer.jsp"></jsp:include>