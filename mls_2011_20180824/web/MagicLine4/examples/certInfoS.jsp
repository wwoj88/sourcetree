<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>

<script type="text/javascript" src="NTSMagicLine4/js/deployJava.js"></script>
<script type="text/javascript" src="NTSMagicLine4/js/PluginDetect.js"></script>
<script type="text/javascript" src="NTSMagicLine4/js/MagicLine.js"></script>

<style type = "text/css">
<!--
.box
{
position: relative;
border: dashed 1px #dadada;
margin-top: 15px;
background: inherit;
color: #AAB165;
}

.box_inner
{
border: solid 1px #fff;
padding: 15px;
background: #fffff0 /*url('images/a4.gif') repeat-x*/;
color: inherit;
}
-->
</style>

<h2>전자서명 및 검증</h2>
<div id="workArea"><!-- DIV START  -->
<h3 style = "color:BLUE;">전자서명 데이터 인증서 정보 추출</h3>
<div class="box">
		<div class="box_inner">
			클라이언트에서 전자서명한 메시지에서 <br>
			서명 인증서 정보를 추출해 나열한다.<br>
		</div>
	</div>
	
	<h4 style = "color:SKYBLUE;">실행</h4>
	
	<form name="send_form" action="CertInfo_Demo2.jsp" method="post">
		<input type="hidden" name="SignedData"/>		
		<div id="button_space" align="center">
			<input type="button" value="다음" "/>		    
		    <input type="button" value="이전" "/>
		</div>		
	</form>
	
</div><!-- DIV END  -->
</html>
<jsp:include page="include/footer.jsp"></jsp:include>