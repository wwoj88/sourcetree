<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %>

<%
	DSHttpServletResponse res = new DSHttpServletResponse(response);
	res.setRequest(request);
	String challenge = "";
	try{
		challenge = res.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
		return;
	}
%>



<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>


<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	// MagicLine Client Module 구동.
	runMagicLine();

	// MagicLine Client 인증서 창을 호출한다.
	function loginpop(){
		if (MagicLine_install == true)
			// MagicLine.js 에 있는 Login 공통함수를 호출한다.
			Login(document.popForm);
	}
</script>
<div id="middle">
<h2>MagicLine Digital Signature</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Certificate Digital Signature Envelope Samples </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
						사용자 인증서 인터페이스를 통해 선택된 인증서로 서명 후<br>
						서버 인증서로 데이터를 암호화 합니다.<br>
                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="signEnvelopeR.jsp" method="post" name="signEnvelopeForm">
<table style="width: 100%" class="styledLeft">
            		<thead>
	             		<tr>
				         <th colspan="2">Client Digital Signature Envelope Information</th>
			        </tr>
                    	</thead>
                    	<tbody>
				<tr>
					<td class="formRow">
						<table class="normal" cellspacing="0">
				                    <tr>
				                        <td>Server Challenge code<font class="required">*</font></td>
				                        <td><input type="text" class="text-box-big" id="challenge"
				                                   name="challenge" type="text"
				                                   value="<%=challenge%>"> &nbsp; Replay Attack을 방지하기 위한 Challenge code </td>
				                    </tr>
				                    <tr >
				                        <td>plain text<font class="required">*</font></td>
				                        <td><input class="text-box-big" id="plaintext"
				                                   name="ssn" type="text"
				                                   value=""> &nbsp;  전자서명과 공개키 암호화를 실행할 원문 데이터를 입력한다.</td>
				                    </tr>

								</table>
							</td>
						</tr>
	                    <tr>
	                        <td colspan="2" class="buttonRow">
	                            <input name="Submit" type="button" class="button"
	                                   value="암호화"
	                                    onclick="EnvelopedSignData(signEnvelopeForm);return false;"     />
	                            <input type="reset" class="button" value="Reset"/>
	                        </td>
	                    </tr>
                    </tbody>
                </table>

	<p>&nbsp;</p>

	<table class="styledLeft" id="sgTable"  width="100%">
	<thead>
		<tr>
			<th colspan="3" >Server Send Digital Signature Envelope Data.</th>

		</tr>
	</thead>
	<tbody>
		<tr bgcolor="white">
			<td width="10px" style="text-align: center;">1</td>
			<td width="200px" ><nobr>
			sessionID </nobr></td>
			<td><input type="text" name="sessionIDT"
				size="58" value="" />&nbsp;현재 Session Id</td>
		</tr>
		<tr bgcolor="#EEEFFB">
			<td width="10px" style="text-align: center;">2</td>
			<td><nobr>
			Plain Text </nobr></td>
			<td>
			<textarea name="pText" rows="2" cols="66"></textarea>
			&nbsp;전자서명 및 암호화 할 데이터</td>
		</tr>
		<tr bgcolor="white">
			<td width="10px" style="text-align: center;">3</td>
			<td><nobr>
			Encrypt Text </nobr></td>
			<td>
			<textarea name="eText" rows="5" cols="66"></textarea>
			&nbsp;전자서명 및 암호화되어 전송될 데이터</td>
	</tbody>
</table>
</form>

</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
	document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Certificate Login</td>';
    }
</script>

<jsp:include page="include/footer.jsp"></jsp:include>