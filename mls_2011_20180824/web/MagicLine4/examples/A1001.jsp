<%@ page contentType="text/html;charset=utf-8"%>
<%@page import="com.dreamsecurity.magicline.check.ProcessorChecker"%>
<%@page import="com.dreamsecurity.JCAOSProvider"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.dreamsecurity.jcaos.util.encoders.Base64"%>
<%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %>
<%@ page import="com.dreamsecurity.jcaos.cms.SignedData" %>
<%@ page import="com.dreamsecurity.jcaos.exception.IdentifyException"%>
<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu_01.jsp"></jsp:include>



<%

	JCAOSProvider.installProvider();
	boolean loginOk = false;

	String signedData = request.getParameter("loginData");
	String errMessgae = "";

	byte[] decodeSignData = Base64.decode(signedData);


	X509Certificate cert = null;
	SignedData signdata = null;
	String signedContent = "";
	SimpleDateFormat dateFormat = null;
	try{

		signdata = SignedData.getInstance(decodeSignData);
		signdata.verify();
		signedContent = new String(signdata.getContent());
		cert = (X509Certificate)signdata.getCertificates().get(0);
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		//String cainfoPath = "D:\\workspace\\DreamSecurity_Real_Workspace\\MagicLine4\\WebContent\\WEB-INF\\magicline\\conf\\";
		//ProcessorChecker ck = new ProcessorChecker(cainfoPath);
		//ck.verifyCert(cert);
		session.setAttribute("Login","1");
		loginOk = true;
	}catch(Exception e){
		loginOk = false;
		e.printStackTrace();
		errMessgae = e.getMessage();
	}




	//String certPublicKey =  cert.getPublicKey();
	String subjectDN = cert.getSubjectDN().getName();
	String issurDN = cert.getIssuerDN().getName();
	int serialNo = cert.getSerialNumber().intValue();
	String notAfter = dateFormat.format(cert.getNotAfter());
	String notBefore = dateFormat.format(cert.getNotBefore());


%>


<div id="middle">
<h2>Sample Page 1</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><h1> 로그인 완료 </h1></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0;"><nobr> description : </nobr></td>
					<td style="border: 0;">클라이언트 공인인증 솔루션을 이용한 사용자 로그인.<br/>
					 데이터를 서버에서 서명검증<br/>
					</td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>

<%if(loginOk){ %>
<table class="styledLeft" id="sgTable" width="100%">
	<thead>
		<tr>
			<th colspan="3">Server Send Signed Data.</th>

		</tr>
	</thead>
	<tbody>
		<tr bgcolor="white" height="300px">
			<td width="150px" ><nobr> 로그인 성공 (서명 데이터) </nobr></td>
			<td><%=signedContent %></td>
		</tr>

	</tbody>
</table>
<%}else{ %>

<table class="styledLeft" id="sgTable" width="100%">
	<thead>
		<tr>
			<th colspan="3">Server Send Signed Data.</th>

		</tr>
	</thead>
	<tbody>
		<tr bgcolor="white" height="300px">
			<td width="150px"><nobr> 로그인 실패(에러 메세지) </nobr></td>
			<td><%=errMessgae %></td>
		</tr>
		<tr>

	</tbody>
</table>
<%} %>

<p>&nbsp;</p>




<!--
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>2.MagicLine Client 기동 및 Login 함수 정의. </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- MagicLine Javascript include --&gt; </em></span>
&lt;script language="javascript" src="../js/MagicLine.js"&gt;&lt;/script&gt;
&lt;script language='javascript'&gt;
<span class="emphasis"><em>	// MagicLine Client Module 구동.</em></span>
	runMagicLine();
<span class="emphasis"><em>	// MagicLine Client 인증서 창을 호출한다. </em></span>
	function doAction(loginForm){
		if (MagicLine_install == true){
<span class="emphasis"><em>			// MagicLine.js 에 있는 Login 공통함수를 호출한다. </em></span>
			Login(loginForm);
		}
	}
&lt;/script&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			</ul>
			</td>
		</tr>
	</tbody>
</table>
 -->

</div>
<!-- DIV END  --> <script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
    document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;Sample Page 2</td><td class="breadcrumb-link">&nbsp;>&nbsp;이체 샘플 페이지</td>';
    }
</script> <jsp:include page="include/footer.jsp"></jsp:include>