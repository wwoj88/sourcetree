<%@ page contentType="text/html;charset=utf-8"%>
<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu_01.jsp"></jsp:include>

    <div id="middle">
        <table cellspacing="0" width="100%">
            <tr>
                <td>
                    <div id="features">
                        <table cellspacing="0">
                            <tr class="feature feature-top">
                                <td>
                                    <a target="_blank" href="../templete/document/MagicLine V4.0_Install_Guide.pdf"><img src="../templete/images/user-guide.gif"/></a>
                                </td>
                                <td width="100%">
                                    <h3><a target="_blank" href="../templete/document/MagicLine V4.0_Install_Guide.pdf">Install Guide</a></h3>

                                    <p>MagicLine4 설치문서 <br/>
                                    	MagicLine 클라이언트 모듈 및  MagicLine 서버 모듈
                                    </p>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td>
                                    <a target="_blank" href="../templete/document/MagicLineV4.0_Function_Guide.pdf"><img
                                            src="../templete/images/forum.gif"/></a>
                                </td>
                                <td>
                                    <h3><a target="_blank" href="../templete/document/MagicLineV4.0_Function_Guide.pdf">Function Guide</a>
                                    </h3>

                                    <p> MagicLine 클라이언트 및 서버모듈에 관한 기능 명세서.</p>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td>
                                    <a target="_blank"
                                       href="../templete/document/MagicLineServerV4.2/index.html"><img
                                            src="../templete/images/issue-tracker.gif"/></a>
                                </td>
                                <td>
                                    <h3><a target="_blank"
                                           href="../templete/document/MagicLineServerV4.2/index.html">
                                        MagicLine Java Docs </a></h3>

                                    <p>MagicLine4 Server Java Docs.</p>

                                </td>
                            </tr>
                            <tr class="feature">
                                <td>
                                    <a target="_blank"
                                       href="../templete/document/Jcaos_docs/index.html"><img
                                            src="../templete/images/issue-tracker.gif"/></a>
                                </td>
                                <td>
                                    <h3><a target="_blank"
                                           href="../templete/document/Jcaos_docs/index.html">
                                        MagicPKI(Jcaos) Java Docs </a></h3>

                                    <p> MagicPKI(Jcaos) Java Docs.</p>

                                </td>
                            </tr>
                            <tr class="feature">
                                <td>
                                    <a target="_blank" href="#"><img
                                            src="../templete/images/mailing-list.gif"/></a>
                                </td>
                                <td>
                                    <h3><a target="_blank" href="#">
                                        Mailing</a></h3>

                                    <p></p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td width="25%">
                    <div id="loginbox">
                        <h2>Sign-in</h2>
                        <form action='login' method="POST" target="_self">
                            <table>
                                <tr>
                                    <td>
                                        <nobr><label for="txtUserName">Server URL</label></nobr>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="txtUserName">Username</label>
                                    </td>
                                    <td>
                                        <input type="text" id="txtUserName" name="username"
                                               class="user" tabindex="1" value="admin" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="txtPassword">Password</label>
                                    </td>
                                    <td>
                                        <input type="password" id="txtPassword" name="password"
                                               class="password" value="admin" tabindex="2"/>

                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <input type="button" value="Sign-in"
                                               class="button" tabindex="3" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="f_red_ty1">


                                    </td>
                                </tr>
                            </table>
                        </form>
                        <br/>
			            <a target="_blank" href="" tabindex="4">Sign-in Help</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
<!-- DIV END  --> <script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
    document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;index</td>';
    }
</script> <jsp:include page="include/footer.jsp"></jsp:include>