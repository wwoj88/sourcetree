<%@ page import="com.dreamsecurity.magicline.io.DSJspWriter" %>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletRequest" %>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %>
<%@ page import="com.dreamsecurity.magicline.MessageConstants" %>
<%@ page import="com.dreamsecurity.magicline.handle.info.*" %> 
<%@ page import="com.dreamsecurity.magicline.config.Config" %>
<%@ page import="com.dreamsecurity.magicline.config.Configuration" %>
<%@ page import="com.dreamsecurity.magicline.config.Logger" %>
<%@ page import="java.net.*"%>
<%
    DSHttpServletResponse magiclineresponse = null;
    DSHttpServletRequest magiclinerequest = null; 

    try{
        magiclineresponse=new DSHttpServletResponse(response);
	    magiclinerequest= new DSHttpServletRequest(request);

	    
        magiclineresponse.setRequest(magiclinerequest);

        out=new DSJspWriter(out,(DSKeyInfo)session.getAttribute("DSSession"));
		Logger.debug.println(this, "[current_thread]["+Thread.currentThread()+"] magicline ref= " + out.toString());
    }catch(Exception e){
        Config dsjdf_config = new Configuration();
        StringBuffer sb=new StringBuffer(1500);
        sb.append(dsjdf_config.get("MagicLine.errorPage"));
        sb.append("?errmsg=");
        sb.append(URLEncoder.encode(e.getMessage()));

        response.sendRedirect(sb.toString()) ;

		return;
    }
%>