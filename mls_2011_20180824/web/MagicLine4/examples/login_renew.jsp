<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse"%>

<%
//
	String challenge = "";
	try{
	DSHttpServletResponse res = new DSHttpServletResponse(response);
	res.setRequest(request);
	challenge = res.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
		return;
	}	
%>



<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>


<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	// MagicLine Client Module 구동.
	runMagicLine('aftercall','','','');

	// MagicLine Client 인증서 창을 호출한다.
	function doAction(loginForm){
		if (MagicLine_install == true)
			// MagicLine.js 에 있는 Login 공통함수를 호출한다.
			Login(loginForm);

	}

	function typeChange(sel){
		var val  = sel.value;
		if(val == 'a'){
			document.loginForm.action = 'login_renewRAPI.jsp';
		}else if(val == 'h'){
			document.loginForm.action = 'login_renewR.jsp';
		}else{
			document.loginForm.action = '/MagicLine4/TESTFilterServlet';
		}
	}
</script>
<div id="middle">
<h2>MagicLine Login</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Certificate Login( cert )
		Samples </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0;"><nobr> description : </nobr></td>
					<td style="border: 0;">사용자가 선택한 인증서를 이용하여 만들어진 로그인 요청 메시지를
					이용하여, 로그인 기능을 구현한다.<br />
					서버는 로그인 요청 메시지에 포함되어 있는 사용자 인증서의 유효성 여부를 확인하며,<br />
					인증서 정보를 추출하여 적용 기관 별로 추가적인 사용자 인증서 확인 작업을 진행 할 수 있습니다.<br />

					</td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="login_renewR.jsp" method="post" name="loginForm">
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Login Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>Server Challenge code<font class="required">*</font></td>
					<td><input type="text" class="text-box-big" id="challenge"
						name="challenge" type="text" value="<%=challenge%>">
					&nbsp; Replay Attack을 방지하기 위한 Challenge code</td>
				</tr>
				<tr>
					<td>User SSN<font class="required">*</font></td>
					<td><input class="text-box-big" id="ssn" name="ssn"
						type="text" value=""> &nbsp; 주민등록번호는 서버에서 본인확인 검증을 할 경우
					사용된다.</td>
				</tr>
				<tr>
					<td>ETC Data<font class="required">*</font></td>
					<td><input type="text" class="text-box-big" id="etc"
						name="etc" type="text" value=""> &nbsp; 기타 정보를 암호화하여 전송할 수
					있다.</td>
				</tr>

			</table>
			</td>
		</tr>

		<tr>
			<td colspan="2" class="buttonRow">
			Submit Type <select onchange="typeChange(this)"><option value="h">HTTP</option><option value="a">API</option><option value="f">Filter</option></select>
				 <input name="Submit"
				type="button" class="button" value="Login"
				onclick="doAction(loginForm);return false;" />
				<input
				type="reset" class="button" value="Reset" />
		</td>

	</tbody>
</table>

<p>&nbsp;</p>

<table class="styledLeft" id="sgTable" width="100%">
	<thead>
		<tr>
			<th colspan="3">Server Send Login Data.</th>

		</tr>
	</thead>
	<tbody>
		<tr bgcolor="white">
			<td width="10px" style="text-align: center;">1</td>
			<td width="200px"><nobr> sessionID </nobr></td>
			<td><input type="text" name="sessionIDT" size="58" value="" />&nbsp;현재
			Session Id</td>
		</tr>
		<tr bgcolor="#EEEFFB">
			<td width="10px" style="text-align: center;">2</td>
			<td><nobr> Plain Text </nobr></td>
			<td><textarea name="pText" rows="2" cols="66"></textarea>
			&nbsp;암호화할 데이터</td>
		</tr>
		<tr bgcolor="white">
			<td width="10px" style="text-align: center;">3</td>
			<td><nobr> Encrypt Text </nobr></td>
			<td><textarea name="eText" rows="5" cols="66"></textarea>
			&nbsp;암호화 되어 전송될 데이터</td>
	</tbody>
</table>
</form>


<p>&nbsp;</p>


<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>1.MagicLine Challenge 발급. </p>
				<pre class="programlisting"><span class="emphasis"><em>// Replay Attack 방지 Challenge 발급 </em></span>
&lt;%
	String challenge = "";
	try{
	DSHttpServletResponse res = new DSHttpServletResponse(response);
	res.setRequest(request);
	challenge = res.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
		return;
	}
%&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>2.MagicLine Client 기동 및 Login 함수 정의. </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- MagicLine Javascript include --&gt; </em></span>
&lt;script language="javascript" src="../js/deployJava.js"&gt;&lt;/script&gt;
&lt;script language="javascript" src="../js/PluginDetect.js"&gt;&lt;/script&gt;
&lt;script language="javascript" src="../js/MagicLine.js"&gt;&lt;/script&gt;
&lt;script language='javascript'&gt;
<span class="emphasis"><em>	// MagicLine Client Module 구동.</em></span>
	runMagicLine();
<span class="emphasis"><em>	// MagicLine Client 인증서 창을 호출한다. </em></span>
	function doAction(loginForm){
		if (MagicLine_install == true){
<span class="emphasis"><em>			// MagicLine.js 에 있는 Login 공통함수를 호출한다. </em></span>
			Login(loginForm);
		}
	}
&lt;/script&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>3.MagicLine Login Form 작성. </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- Login Form --&gt; </em></span>
&lt;form action="..(Response Url)" method="post" name="loginForm"&gt;
&lt;input type="hidden" id="challenge" name="challenge" value="&lt;%=challenge%&gt"/&gt; <span class="emphasis"><em>&lt;!-- 생성한 challenge 값을 담는다 --&gt; </em></span>
&lt;input type="text" id="ssn" name="ssn" value=""/&gt; <span class="emphasis"><em>&lt;!-- 본인확인이 필요할 경우 주민번호를 담는다. --&gt; </em></span>
&lt;input type="text" id="etc" name="etc" value=""/&gt;  <span class="emphasis"><em>&lt;!-- 업무에 필요한 기타 데이터를 담는다. --&gt; </em></span>
&lt;input type="button" value="Login" onclick="doAction(loginForm);return false;" /&gt; <span class="emphasis"><em>&lt;!-- 인증서창 PopUp 및 전송 --&gt; </em></span>
&lt;/form&gt;

				</pre>
				<p>&nbsp;</p>
			</li>


			</ul>
			</td>
		</tr>
	</tbody>
</table>


</div>
<!-- DIV END  --> <script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
    document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Certificate Login</td>';
    }
</script> <jsp:include page="include/footer.jsp"></jsp:include>