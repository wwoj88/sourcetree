<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>

<script type="text/javascript" src="NTSMagicLine4/js/deployJava.js"></script>
<script type="text/javascript" src="NTSMagicLine4/js/PluginDetect.js"></script>
<script type="text/javascript" src="NTSMagicLine4/js/MagicLine.js"></script>

<style type = "text/css">
<!--
.box
{
position: relative;
border: dashed 1px #dadada;
margin-top: 15px;
background: inherit;
color: #AAB165;
}

.box_inner
{
border: solid 1px #fff;
padding: 15px;
background: #fffff0 /*url('images/a4.gif') repeat-x*/;
color: inherit;
}
-->
</style>

<h2>전자서명 및 검증</h2>
<div id="workArea"><!-- DIV START  -->
<h3 style = "color:BLUE;">전자서명 데이터 인증서 정보 추출</h3>
<div class="box">
		<div class="box_inner">
			클라이언트에서 전자서명한 메시지에서 <br>
			서명 인증서 정보를 추출해 나열한다.<br>
		</div>
	</div>
	
	<h4 style = "color:SKYBLUE;">결과</h4>
		<form name="send_form">
		<input type="hidden" name="SignedData"/>
		<div align="center">
			<table id="input_space" cellspacing="0" cellpadding="2" width="100%" align="center">
			<tr>
			<td class="input_field" width="30%">클라이언트에서 전송된 전자 서명 값</td>
			<td class="input_data"><textarea cols="60" rows="8" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field1" width="30%">서버에서 검증된 값</td>
			<td class="input_data"><textarea cols="60" rows="2" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field" width="30%">Version</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field1" width="30%">Serial Number</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field" width="30%">Signature Algorithm</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field1" width="30%">Issuer Name</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field" width="30%">Subject Name</td>
			<td class="input_data"><textarea cols="60" rows="2" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field1" width="30%">Valid From</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field" width="30%">Valid To</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field1" width="30%">Signature</td>
			<td class="input_data"><textarea cols="60" rows="3" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field" width="30%">Key Usage</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field1" width="30%">Extended Key Usage</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field" width="30%">Certificate Policy</td>
			<td class="input_data"><textarea cols="60" rows="3" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field1" width="30%">Certificate Policy ID</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>			
			<tr>
			<td class="input_field" width="30%">Basic Constraints</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field1" width="30%">Subject Alt Name</td>
			<td class="input_data"><textarea cols="60" rows="3" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field" width="30%">Authority Key ID</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field1" width="30%">Subject Key ID</td>
			<td class="input_data"><textarea cols="60" rows="1" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field" width="30%">DistributionPoint</td>
			<td class="input_data"><textarea cols="60" rows="2" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field1" width="30%">AuthorityInfoAccess</td>
			<td class="input_data"><textarea cols="60" rows="3" ></textarea></td>
			</tr>
			<tr>
			<td class="input_field" width="30%">PublicKey</td>
			<td class="input_data"><textarea cols="60" rows="3" ></textarea></td>
			</tr>
			</table>		
		</div>
		<div id="button_space" align="center">			
		    <input type="button" value="이전" onclick="javascript:history.back()"/>
    		<input type="button" value="처음" onclick="location.href='./EPKI_Demo.html'"/>
		</div>		
	</form>
	
</div><!-- DIV END  -->
</html>
<jsp:include page="include/footer.jsp"></jsp:include>