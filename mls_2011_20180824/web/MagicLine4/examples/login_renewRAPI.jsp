<%@ page contentType="text/html;charset=utf-8"%>

<%@ page import="com.dreamsecurity.magicline.handle.RequestContent" %>
<%@ page import="com.dreamsecurity.magicline.processor.Processor" %>
<%@ page import="com.dreamsecurity.magicline.processor.ProcessorFactory" %>

<%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %>
<%@ page import="com.dreamsecurity.jcaos.exception.IdentifyException"%>
<%@ page import="java.net.URLEncoder" %>

<%

	String query = request.getParameter("encryptedData");
	String subjectDN = "";

	String ssn = "";
	String challenge = "";
	String etc = "";

	try {

		RequestContent reqContent = new RequestContent(query);
		ProcessorFactory factory = new ProcessorFactory();
		Processor process = factory.createProcessor(reqContent);
		process.setKeyInfoRequest(request);
		process.execute();

		if(process.isAddSessionKey()){
			process.addSessionKey(request);
		}

		//Infomation
		X509Certificate cert = process.getSignerCerts()[0];
		subjectDN = cert.getSubjectDN().getName();

		//request parameter
		ssn = process.getParameter("ssn");
		challenge = process.getParameter("challenge");
		etc = process.getParameter("etc");

		//주민번호 본인확인 기능을 사용할 경우
		if(null != ssn && !ssn.equals("")){
			cert.verifyVID(ssn, process.getPrivateKeyRandom());
		}


	}catch(IdentifyException e){
		// 본인확인 Error
		StringBuffer sb = new StringBuffer(1500);
		sb.append(e.getMessage());
		session.setAttribute("magiclineErr", "[본인확인실패]"+sb.toString());
		response.sendRedirect("magicline_err.jsp");
		return;

	} catch (Exception e) {
		//인증서 검증
		//정책 검증
		StringBuffer sb = new StringBuffer(1500);
		sb.append(e.getMessage());
		session.setAttribute("magiclineErr",sb.toString());
		response.sendRedirect("magicline_err.jsp");
		return;
	}
%>


<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>



<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	runMagicLine();
</script>

<div id="middle">
<h2>MagicLine Login Result</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Certificate Login( cert ) Result </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
<br/>
<br/>
<br/>

                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="login_renewR.jsp" method="post" name="popForm">
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Login Request Data</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>Challenge</td>
					<td><%=challenge%></td>
				</tr>
				<tr>
					<td>SSN</td>
					<td><%=ssn%></td>
				</tr>
				<tr>
					<td>ETC Data</td>
					<td><%=etc%></td>
				</tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Certificate Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>USER Certificate Dn:</td>
					<td><%=subjectDN%></td>
				</tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</form>

<p>&nbsp;</p>


<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>1.MagicLine class include </p>
			<pre class="programlisting">
&lt;%@ page import="com.dreamsecurity.magicline.handle.RequestContent" %&gt;
&lt;%@ page import="com.dreamsecurity.magicline.processor.Processor" %&gt;
&lt;%@ page import="com.dreamsecurity.magicline.processor.ProcessorFactory" %&gt;
&lt;%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %&gt;
&lt;%@ page import="com.dreamsecurity.jcaos.exception.IdentifyException"%&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>2.MagicLine Server login result </p>
<pre class="programlisting"><span class="emphasis"><em>&lt;!-- DSHttpRequest를 사용할 수 없는 경우, Request 객채를 넘겨줄 수 없는 경우, 다음과 같이 MagicLine API 를 직접 사용할 수 있다.  --&gt; </em></span>

&lt;%
<span class="emphasis"><em>// Client에서 암호화된 데이터를 받는다. </em></span>
	String query = request.getParameter("encryptedData");

	try {
		RequestContent reqContent = new RequestContent(query); <span class="emphasis"><em>// 암호화된 데이터를  RequestContent에 담아 생성한다. </em></span>
		ProcessorFactory factory = new ProcessorFactory();     <span class="emphasis"><em>// ProcessorFactory 객체를 생성한다. </em></span>
		Processor process = factory.createProcessor(reqContent); <span class="emphasis"><em>// ProcessorFactory 에서 해당 암호화 데이터에대한 Process를 축출한다. </em></span>
		process.execute();  <span class="emphasis"><em>// Processor 처리. </em></span>


		ssn = process.getParameter("ssn");<span class="emphasis"><em>// 암호화되어  넘어온 Parameter 를 얻는다.  </em></span>
		challenge = process.getParameter("challenge");<span class="emphasis"><em>// 암호화되어  넘어온 Parameter 를 얻는다.  </em></span>
		etc = process.getParameter("etc");<span class="emphasis"><em>// 암호화되어  넘어온 Parameter 를 얻는다.  </em></span>

	} catch (Exception e) {
		<span class="emphasis"><em>//인증서 검증  Error </em></span>
		<span class="emphasis"><em>//정책 검증 Error </em></span>
		<span class="emphasis"><em>//로그인 메세지 복호화 Error </em></span>
		out.println("[로그인 실패] error message:"+e.getMessage());
		return;
	}

	<span class="emphasis"><em>//인증서 검증, 정책검증 복호화 성공. </em></span>

%&gt;
				</pre>
				<p>&nbsp;</p>
			</li>
			</ul>
			</td>
		</tr>
	</tbody>
</table>


</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
	document.onload = setBreadcrumDiv();
	function setBreadcrumDiv() {
		var breadcrumbDiv = document.getElementById('breadcrumb-div');
		breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Certificate Login</td>';
	}
</script>

<jsp:include page="include/footer.jsp"></jsp:include>