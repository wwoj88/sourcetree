<%@page import="java.util.Date"%>
<%@ page contentType="text/html;charset=utf-8"%>
<%@page import="java.text.SimpleDateFormat"%>

<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu_01.jsp"></jsp:include>






<%
SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd");
SimpleDateFormat df2 = new SimpleDateFormat("HH:mm:ss");
String date = df.format(new java.util.Date());
String time = df2.format(new java.util.Date());

%>


<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>

<%


	String loginBool = (String)session.getAttribute("Login");
	if(null == loginBool || loginBool.equals("0")){
%>
	alert('로그인이 필요합니다.');
	history.back(-1);
<%
	}
%>

	// MagicLine Client Module 구동.
	runMagicLine();

	// MagicLine Client 인증서 창을 호출한다.
	function doAction(){
		if (MagicLine_install == true){
			// MagicLine.js 에 있는 Login 공통함수를 호출한다.
			var planText = signDataRange();
			alert(planText);
			var data =  ShowCertWindow(true,true,signDataRange());
			document.creditForm.signedData.value = data;
		}
	}


	function goPage(){
		document.creditForm.submit();
	}

	function signDataRange(){

		var dateData = '(1)거래일자: <%=date%>\n';
		var timeData = '(2)거래시간: <%=time%>\n';
		var a0 = '(3)출금계좌번호: '+document.creditForm.a0.value+'\n';
		var a1 = '(4)이체금액: '+document.creditForm.a1.value+'(원)\n';
		var a2 = '(5)수수료: '+document.creditForm.a2.value+'(원)\n';
		var a3 = '(6)내통장 표시내용: '+document.creditForm.a3.value+'\n';
		var a4 = '(7)입금은행: '+document.creditForm.a4.value+'\n';
		var a5 = '(8)입금계좌번호: '+document.creditForm.a5.value+'\n';
		var a6 = '(9)수취인: '+document.creditForm.a6.value+'\n';
		var a7 = '(10)수취인 통장표시내용: '+document.creditForm.a7.value+'\n';
		var a8 = '(11)CMS코드: 111\n';
		return dateData+timeData+a0+a1+a2+a3+a4+a5+a6+a7+a8;

	}


</script>
<div id="middle">
<h2>Sample Page 2</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><h1> 이체 페이지 샘플 </h1></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0;"><nobr> description : </nobr></td>
					<td style="border: 0;">클라이언트 공인인증 솔루션을 이용한 사용자 로그인.<br/>
					사용자는 공인인증 설루션을 이용하여 사용자의 인증서를 선택후 PIN번호를 입력하여 서명데이터를 생성, 이 데이터를 서버에 전송하여 서명검증을 하여 로그인 세션을 생성한다.<br/>
					</td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>

<form action="A1101.jsp" method="post" name="creditForm">
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Credit transfer information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td width="130px">출금계좌번호<font class="required">*</font></td>
					<td><input type="text" class="text-box-big" id="challenge"
						name="a0" type="text" value="123456-01-123456">
					&nbsp; </td>
				</tr>
				<tr>
					<td width="130px">이체금액<font class="required">*</font></td>
					<td><input type="text" class="text-box-big" id="challenge"
						name="a1" type="text" value="1000">
					&nbsp; </td>
				</tr>
				<tr>
					<td width="130px">수수료<font class="required">*</font></td>
					<td><input type="text" class="text-box-big" id="challenge"
						name="a2" type="text" value="0">
					&nbsp; </td>
				</tr>
				<tr>
					<td width="130px">내 통장 표시내용<font class="required">*</font></td>
					<td><input type="text" class="text-box-big" id="challenge"
						name="a3" type="text" value="테스트 출금">
					&nbsp; </td>
				</tr>
				<tr>
					<td width="130px">입금은행<font class="required">*</font></td>
					<td><input type="text" class="text-box-big" id="challenge"
						name="a4" type="text" value="국민은행">
					&nbsp; </td>
				</tr>
				<tr>
					<td width="130px">입금계좌번호<font class="required">*</font></td>
					<td><input type="text" class="text-box-big" id="challenge"
						name="a5" type="text" value="123456-01-000000">
					&nbsp; </td>
				</tr>
				<tr>
					<td width="130px">수취인<font class="required">*</font></td>
					<td><input type="text" class="text-box-big" id="challenge"
						name="a6" type="text" value="홍길동">
					&nbsp; </td>
				</tr>
				<tr>
					<td width="130px">수취인 통장 표시내용<font class="required">*</font></td>
					<td><input type="text" class="text-box-big" id="challenge"
						name="a7" type="text" value="테스트 이체">
					&nbsp; </td>
				</tr>
			</table>
			</td>
		</tr>

		<tr>
			<td colspan="2" class="buttonRow">
				<select name="certVerify">
					<option value="NONE">인증서 검증 없음 </option>
					<option value="CRL">인증서 CRL 검증 </option>
				</select>

				 <input name="Submit"
				type="button" class="button" value="확인"
				onclick="doAction();return false;" />
		</td>

	</tbody>
</table>

<p>&nbsp;</p>

<table class="styledLeft" id="sgTable" width="100%">
	<thead>
		<tr>
			<th colspan="3">Server Send Signed Data.</th>

		</tr>
	</thead>
	<tbody>
		<tr bgcolor="white">
			<td width="150px"><nobr> 서명 데이터 </nobr></td>
			<td><textarea name="signedData" rows="10" cols="100"></textarea>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="buttonRow">
				 <input type="button" class="button" value="전송"
				onclick="goPage();return false;" />
		</td>

	</tbody>
</table>
</form>


<p>&nbsp;</p>

<!--
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>2.MagicLine Client 기동 및 Login 함수 정의. </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- MagicLine Javascript include --&gt; </em></span>
&lt;script language="javascript" src="../js/MagicLine.js"&gt;&lt;/script&gt;
&lt;script language='javascript'&gt;
<span class="emphasis"><em>	// MagicLine Client Module 구동.</em></span>
	runMagicLine();
<span class="emphasis"><em>	// MagicLine Client 인증서 창을 호출한다. </em></span>
	function doAction(loginForm){
		if (MagicLine_install == true){
<span class="emphasis"><em>			// MagicLine.js 에 있는 Login 공통함수를 호출한다. </em></span>
			Login(loginForm);
		}
	}
&lt;/script&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			</ul>
			</td>
		</tr>
	</tbody>
</table>
 -->

</div>
<!-- DIV END  --> <script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
    document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;Sample Page 2</td><td class="breadcrumb-link">&nbsp;>&nbsp;이체 샘플 페이지</td>';
    }
</script> <jsp:include page="include/footer.jsp"></jsp:include>