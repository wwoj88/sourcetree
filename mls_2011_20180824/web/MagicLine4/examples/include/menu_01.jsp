<%@ page contentType="text/html;charset=euc-kr"%>
<table id="main-table" border="0" cellspacing="0">
	<tr>
		<td id="header" colspan="2">
		<div id="header-div">
		<div class="right-logo">MagicLineV4.0 Console</div>
		<div class="left-logo"><a href="index.jsp" class="header-home">
			<img src="../templete/images/1px.gif" width="300px" height="32px" /></a>
		</div>
		<div class="middle-ad"></div>
		<div class="header-links">
		<div class="right-links">
		<ul>
			<li class="middle"><label id="logged-user"> <strong>Signed-in
			as:</strong>&nbsp;admin@ </label></li>
			<li class="middle">|</li>
			<li class="right"><a href="../templete/document/MagicLine V4.0_Install_Guide.pdf">Install Docs </a></li>
			<li class="middle">|</li>
			<li class="right"><a href="../templete/document/MagicLineV4.0_Function_Guide.pdf">Function Docs </a></li>
			<li class="middle">|</li>
			<li class="middle"><a target="_blank" href="../templete/document/MagicLineServerV4.2/index.html">MagicLine Java Docs</a></li>
			<li class="middle">|</li>
			<li class="middle"><a target="_blank" href="../templete/document/Jcaos_docs/index.html">Jcaos Java Docs</a></li>
			<li class="middle">|</li>
			<li class="left"><a target="_blank" href="http://www.dreamsecurity.com">About</a></li>
		</ul>
		</div>
		</div>
		</div>
		</td>
	</tr>
	<tr>

		<td id="menu-panel" valign="top">
		<table id="menu-table" border="0" cellspacing="0">
			<tr>
				<td id="magicline">

				<div id="menu">
				<ul class="main">
					<li id="magicline_v40_menu" class="menu-header"
						onclick="mainMenuCollapse(this.childNodes[0])"
						style="cursor: pointer"><img src="../templete/images/up-arrow.gif"
						class="mMenuHeaders" id="magicline_v40_menu" />[Samples]MagicLine4</li>
					<li class="normal">
					<ul class="sub">
						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">Demo Page 1</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/A1000.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">��ġ �� �α��� ���� </a></li>
						</ul>
						</li>

						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">Demo Page 2</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/A1100.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">��ü  ����</a></li>
						</ul>
						</li>
						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">Demo Page 3</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/A1200.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">������ ���� ���� </a></li>
						</ul>
						</li>
					</ul>
					</li>

				</ul>
				</div>
				</td>
			</tr>
			<tr>
				<td><img src="../templete/images/1px.gif" width="225px" height="1px" /></td>
			</tr>
		</table>
		</td>
		<td id="middle-content">
		<table id="content-table" border="0" cellspacing="0">
			<tr>
				<td id="page-header-links">
				<table class="page-header-links-table" cellspacing="0">
					<tr>
						<td class="breadcrumbs">
						<table class="breadcrumb-table" cellspacing="0">
							<tr>
								<td>
								<div id="breadcrumb-div"></div>
								</td>
							</tr>

						</table>
						</td>

						<td class="page-header-help"><a href="./docs/userguide.html"
							target="_blank"></a></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td id="body"><img src="../templete/images/1px.gif" width="735px" height="1px" />
