<%@ page contentType="text/html;charset=euc-kr"%>
<table id="main-table" border="0" cellspacing="0">
	<tr>
		<td id="header" colspan="2">
		<div id="header-div">
		<div class="right-logo">MagicLineV4.0 Console</div>
		<div class="left-logo"><a href="index.jsp" class="header-home">
			<img src="../templete/images/1px.gif" width="300px" height="32px" /></a>
		</div>
		<div class="middle-ad"></div>
		<div class="header-links">
		<div class="right-links">
		<ul>
			<li class="middle"><label id="logged-user"> <strong>Signed-in
			as:</strong>&nbsp;admin@ </label></li>
			<li class="middle">|</li>
			<li class="right"><a href="../templete/document/MagicLine V4.0_Install_Guide.pdf">Install Docs </a></li>
			<li class="middle">|</li>
			<li class="right"><a href="../templete/document/MagicLineV4.0_Function_Guide.pdf">Function Docs </a></li>
			<li class="middle">|</li>
			<li class="middle"><a target="_blank" href="../templete/document/MagicLineServerV4.2/index.html">MagicLine Java Docs</a></li>
			<li class="middle">|</li>
			<li class="middle"><a target="_blank" href="../templete/document/Jcaos_docs/index.html">Jcaos Java Docs</a></li>
			<li class="middle">|</li>
			<li class="left"><a target="_blank" href="http://www.dreamsecurity.com">About</a></li>
		</ul>
		</div>
		</div>
		</div>
		</td>
	</tr>
	<tr>

		<td id="menu-panel" valign="top">
		<table id="menu-table" border="0" cellspacing="0">
			<tr>
				<td id="magicline">

				<div id="menu">
				<ul class="main">

					<li id="magicline_v40_menu" class="menu-header"
						onclick="mainMenuCollapse(this.childNodes[0])"
						style="cursor: pointer"><img src="../templete/images/up-arrow.gif"
						class="mMenuHeaders" id="magicline_v40_menu" />[Samples]MagicLine4</li>
					<li class="normal">
					<ul class="sub">
						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">로그인</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/login_renew.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">인증서 로그인</a></li>
						</ul>
						</li>

						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">세션키 암호화 (로그인 후 사용)</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/sessionEncrypt.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">세션키 클라이언트 암호화</a></li>

							<li><a href="../examples/sessionEncryptKeyExchange.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">세션키 Exchange</a></li>

							<li><a href="../examples/sessionSvrEncrypt.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">세션키 서버 암호화</a></li>
						</ul>
						</li>
						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">전자서명</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/signed.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">전자서명</a></li>
							<li><a href="../examples/signoption.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">전자서명OPT</a></li>
							<li><a href="../examples/signEncrypt.jsp" class="menu-default"
								style="background-image: url(../endpoints/images/endpoints-icon.gif);">전자서명 & 암호화</a></li>

						</ul>
						</li>

						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">전자봉투</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/envelope.jsp" class="menu-default"
								style="background-image: url(../endpoints/images/endpoints-icon.gif);">전자봉투</a></li>
							<li><a href="../examples/envelopeSigned.jsp" class="menu-default"
								style="background-image: url(../endpoints/images/endpoints-icon.gif);">전자봉투 & 전자서명</a></li>
						</ul>
						</li>
					</ul>
					</li>

<!--
					<li id="magicline_v42_menu" class="menu-header"
						onclick="mainMenuCollapse(this.childNodes[0])"
						style="cursor: pointer"><img src="../templete/images/up-arrow.gif"
						class="mMenuHeaders" id="magicline_v42_menu"  />[Samples]MagicLine4_new</li>
					<li class="normal">
					<ul class="sub">
						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">전자서명 및 검증</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/esignatureBrowser.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">브라우저 전자서명</a></li>
							<li><a href="../examples/esignatureServer.jsp" class="menu-default"
								style="background-image: url(../endpoints/images/endpoints-icon.gif);">서버 전자서명</a></li>
							<li><a href="../examples/esignatureCertInfoC.jsp" class="menu-default"
								style="background-image: url(../endpoints/images/endpoints-icon.gif);">인증서 정보 추출</a></li>
							<li><a href="../examples/esignatureCertInfoS.jsp" class="menu-default"
								style="background-image: url(../endpoints/images/endpoints-icon.gif);">인증서 정보 추출(서버)</a></li>
						</ul>
						</li>

						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">대칭키</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/encryptBrowser.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">브라우저 암호화</a></li>
							<li><a href="../examples/encryptServer.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">서버암호화</a></li>
						</ul>
						</li>

						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">전자봉투</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/envelopeEncrypt.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">브라우져암호화</a></li>
						</ul>
						</li>

						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">인증서 로그인</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/login_new.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">로그인</a></li>
							<li><a href="sample-nts-etc" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">세션키 암호화</a></li>
							<li><a href="sample-nts-etc" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">세션키 복호화</a></li>
						</ul>
						</li>

						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">인증서 로그인(신원확인)</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/loginVID_new.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">로그인</a></li>
							<li><a href="sample-nts-etc" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">세션키 암호화</a></li>
							<li><a href="sample-nts-etc" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">세션키 복호화</a></li>
						</ul>
						</li>

						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">인증서 기반 신원확인</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="sample-nts-etc" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">인증서 기반 신원 확인</a></li>
						</ul>
						</li>

						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">MAC</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/macData.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">서버 메시지 인증</a></li>
						</ul>
						</li>

						<li class="menu-disabled-link"
							style="background-image: url(../mediation/images/mediation-icon.gif);">Hash</li>
						<li class="normal">
						<ul class="sub">
							<li><a href="../examples/hashData.jsp" class="menu-default"
								style="background-image: url(../sequences/images/sequences.gif);">서버 Hash 생성</a></li>
						</ul>
						</li>
					</ul>
					</li>
 -->

				</ul>
				</div>
				</td>
			</tr>
			<tr>
				<td><img src="../templete/images/1px.gif" width="225px" height="1px" /></td>
			</tr>
		</table>
		</td>
		<td id="middle-content">
		<table id="content-table" border="0" cellspacing="0">
			<tr>
				<td id="page-header-links">
				<table class="page-header-links-table" cellspacing="0">
					<tr>
						<td class="breadcrumbs">
						<table class="breadcrumb-table" cellspacing="0">
							<tr>
								<td>
								<div id="breadcrumb-div"></div>
								</td>
							</tr>

						</table>
						</td>

						<td class="page-header-help"><a href="./docs/userguide.html"
							target="_blank"></a></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td id="body"><img src="../templete/images/1px.gif" width="735px" height="1px" />
