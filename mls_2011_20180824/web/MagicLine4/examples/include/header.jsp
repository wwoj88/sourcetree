<%@ page contentType="text/html;charset=euc-kr"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html;charset=euc-kr" />
<title>Welcome to MagicLineV4.2 Page</title>

<link href="../templete/css/global.css" rel="stylesheet" type="text/css" media="all" />
<link rel="icon" href="../templete/images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../templete/images/favicon.ico"	type="image/x-icon" />
</head>
<body>
<div id="dcontainer"></div>

<!-- JS imports for collapsible menu -->
<script src="../templete/js/yahoo-dom-event.js" type="text/javascript"></script>
<script src="../templete/js/animation-min.js" type="text/javascript"></script>
<script src="../templete/js/cookies.js" type="text/javascript"></script>
<script type="text/javascript">
//Javascript for collapsible menu
        var oneYear = 1000 * 60 * 60 * 24*365;
        var cookie_date = new Date ( );  // current date & time
        cookie_date.setTime ( cookie_date.getTime() + oneYear );

       function nextObject(obj) {
            var n = obj;
            do n = n.nextSibling;
            while (n && n.nodeType != 1);
            return n;
        }
        function mainMenuCollapse(img) {
            var theLi = img.parentNode;
            var nextLi = nextObject(theLi);
            var attributes = "";
            if (nextLi.style.display == "none") {
                //remember the state
                document.cookie = img.id + "=;path=/;expires=" + cookie_date.toGMTString();
                var ver = getInternetExplorerVersion();

                if (ver > -1)
                {
                    if (ver >= 8.0) {
                        attributes = {
                            opacity: { to: 1 }
                        };
                        var anim = new YAHOO.util.Anim(nextLi, attributes);
                        anim.animate();
                        nextLi.style.display = "";
                        nextLi.style.height = "auto";
                    } else {
                        nextLi.style.display = "";
                    }
                } else
                {
                    attributes = {
                        opacity: { to: 1 }
                    };
                    var anim = new YAHOO.util.Anim(nextLi, attributes);
                    anim.animate();
                    nextLi.style.display = "";
                    nextLi.style.height = "auto";
                }
                img.src = "../templete/images/up-arrow.gif";

            } else {
                //remember the state
                document.cookie = img.id + "=none;path=/;expires=" + cookie_date.toGMTString();
                var ver = getInternetExplorerVersion();

                if (ver > -1)
                {
                    if (ver >= 8.0) {
                        attributes = {
                            opacity: { to: 0 }
                        };
                        anim = new YAHOO.util.Anim(nextLi, attributes);
                        anim.duration = 0.3;
                        anim.onComplete.subscribe(hideTreeItem, nextLi);

                        anim.animate();
                    } else {
                        nextLi.style.display = "none";
                    }
                } else {
                    attributes = {
                        opacity: { to: 0 }
                    };
                    anim = new YAHOO.util.Anim(nextLi, attributes);
                    anim.duration = 0.3;
                    anim.onComplete.subscribe(hideTreeItem, nextLi);

                    anim.animate();
                }
                img.src = "../templete/images/down-arrow.gif";

            }
        }
       function hideTreeItem(state, opts, item) {
           item.style.display = "none";
       }
       YAHOO.util.Event.onDOMReady(setMainMenus);
        function setMainMenus(){
            var els = YAHOO.util.Dom.getElementsByClassName('mMenuHeaders', 'img');
            for(var i=0;i<els.length;i++){
                var theLi = els[i].parentNode;
                var nextLi = nextObject(theLi);
                var cookieName = els[i].id;
                var nextLiState = get_cookie(cookieName);
                nextLi.style.display = nextLiState;
                if(nextLiState == "none"){
                    els[i].src = "../templete/images/down-arrow.gif";
                } else{
                    els[i].src = "../templete/images/up-arrow.gif";
                }
            }
        }

        function get_cookie( check_name ) {
            // first we'll split this cookie up into name/value pairs
            // note: document.cookie only returns name=value, not the other components
            var a_all_cookies = document.cookie.split( ';' );
            var a_temp_cookie = '';
            var cookie_name = '';
            var cookie_value = '';
            var b_cookie_found = false; // set boolean t/f default f

            for ( i = 0; i < a_all_cookies.length; i++ )
            {
                a_temp_cookie = a_all_cookies[i].split( '=' );
                cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
                if ( cookie_name == check_name )
                {
                    b_cookie_found = true;
                    if ( a_temp_cookie.length > 1 )
                    {
                        cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
                    }
                    return cookie_value;
                    break;
                }
                a_temp_cookie = null;
                cookie_name = '';
            }
            if ( !b_cookie_found )
            {
                return null;
            }
        }
        function getInternetExplorerVersion()
            // Returns the version of Internet Explorer or a -1
            // (indicating the use of another browser).
        {
            var rv = -1; // Return value assumes failure.
            if (navigator.appName == 'Microsoft Internet Explorer')
            {
                var ua = navigator.userAgent;
                var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                if (re.exec(ua) != null)
                    rv = parseFloat(RegExp.$1);
            }
            return rv;
        }
</script>


