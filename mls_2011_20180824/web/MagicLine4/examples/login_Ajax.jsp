<%@page import="com.dreamsecurity.magicline.handle.RequestContent"%>
<%@page import="com.dreamsecurity.magicline.processor.ProcessorFactory"%>
<%@page import="com.dreamsecurity.magicline.processor.Processor"%>
<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse"%>
<%
	String query = request.getParameter("encryptedData");

	// 아무값도 안들어올 경우 실패
	if(null == query || "".equals(query)){
		out.print("F");
		return;
	}


	try{
		RequestContent reqContent = new RequestContent(query);
		ProcessorFactory factory = new ProcessorFactory();
		Processor process = factory.createProcessor(reqContent);
		process.setKeyInfoRequest(request);
		process.execute();
		out.print("S");
		// 세션 암복호화 사용시
		//if(process.isAddSessionKey()){
		//	process.addSessionKey(request);
		//}
	}catch(Exception e){
		out.print("F");
	}
%>