<%@ page contentType="text/html;charset=utf-8"%>

<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu_01.jsp"></jsp:include>


<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	// MagicLine Client Module 구동.
	runMagicLine();

	// MagicLine Client 인증서 창을 호출한다.
	function doAction(){
			ShowCertManage();
	}


</script>
<div id="middle">
<h2>Sample Page 3</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><h1> 인증서 관리 샘플 </h1></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0;"><nobr> description : </nobr></td>
					<td style="border: 0;">&nbsp;클라이언트 공인인증 솔루션을 이용한 사용자 인증서 관리
					<br/>
					● 인증서를 PC 또는 이동식 저장매체로 이동 <br/>
					● 인증서 내보내기 <br/>
					● 인증서 가져오기 <br/>
					● 인증서 비밀번호 변경<br/>
					</td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>

<form action="login_renewR.jsp" method="post" name="loginForm">
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Certificate Management</th>
		</tr>
	</thead>
	<tbody>
		<tr>


		<tr>
			<td colspan="2" class="buttonRow">
				 <input name="Submit"
				type="button" class="button" value="인증서 관리"
				onclick="doAction();return false;" />
		</td>

	</tbody>
</table>

<p>&nbsp;</p>
<!--
<table class="styledLeft" id="sgTable" width="100%">
	<thead>
		<tr>
			<th colspan="3">Server Send Signed Data.</th>

		</tr>
	</thead>
	<tbody>
		<tr bgcolor="white">
			<td width="150px"><nobr> Signed Data </nobr></td>
			<td><textarea name="pText" rows="10" cols="100"></textarea>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="buttonRow">
				 <input name="Submit"
				type="button" class="button" value="전송"
				onclick="doAction(loginForm);return false;" />
		</td>

	</tbody>
</table>
</form>
 -->

<p>&nbsp;</p>

<!--
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>2.MagicLine Client 기동 및 Login 함수 정의. </p>
				<pre class="programlisting"><span class="emphasis"><em>&lt;!-- MagicLine Javascript include --&gt; </em></span>
&lt;script language="javascript" src="../js/MagicLine.js"&gt;&lt;/script&gt;
&lt;script language='javascript'&gt;
<span class="emphasis"><em>	// MagicLine Client Module 구동.</em></span>
	runMagicLine();
<span class="emphasis"><em>	// MagicLine Client 인증서 창을 호출한다. </em></span>
	function doAction(loginForm){
		if (MagicLine_install == true){
<span class="emphasis"><em>			// MagicLine.js 에 있는 Login 공통함수를 호출한다. </em></span>
			Login(loginForm);
		}
	}
&lt;/script&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			</ul>
			</td>
		</tr>
	</tbody>
</table>
 -->

</div>
<!-- DIV END  --> <script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
    document.onload=setBreadcrumDiv();
    function setBreadcrumDiv () {
        var breadcrumbDiv = document.getElementById('breadcrumb-div');
        breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;Sample Page 2</td><td class="breadcrumb-link">&nbsp;>&nbsp;인증서 관리 샘플 페이지</td>';
    }
</script> <jsp:include page="include/footer.jsp"></jsp:include>