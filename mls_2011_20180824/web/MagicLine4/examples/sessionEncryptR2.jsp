<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletRequest" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %>
<%@ page import="com.dreamsecurity.magicline.MessageConstants" %>
<%@ page import="com.dreamsecurity.magicline.config.Logger" %>
<%
	DSHttpServletResponse dRes = null;
	DSHttpServletRequest dReq = null;

	String text1 = "";
	String text2 = "";
	String text3 = "";
 
	

	try {
		dRes = new DSHttpServletResponse(response);
		dReq = new DSHttpServletRequest(request);
	//	dRes.setRequest(dReq);

	//	text1 = dReq.getParameter("text1");
	//	text2 = dReq.getParameter("text2");
	//	text3 = dReq.getParameter("text3");
		text1 = dReq.getQueryString();

	java.util.Enumeration e = dReq.getParameterNames();
    

    while(e.hasMoreElements()){
        String paramKey = (String)e.nextElement();
		%>
        <tr>
            <td>Key :<%=paramKey %> </td>
            <br>
			<br>
			<td>Value :<%= dReq.getParameter(paramKey) %> </td>
			<br>
			<br>
			<br>
        </tr>        
<%      

	}



	} catch (Exception e) {
		// 인증서 검증 실패
		// 인증서 정책검증 실패
		// 본인확인 실패
		// 복화화 실패
		StringBuffer sb =  new StringBuffer(1500);
		sb.append(e.getMessage());
		session.setAttribute("magiclineErr",sb.toString());
		response.sendRedirect("magicline_err.jsp");
		return;
	}
%>


<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>



<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	runMagicLine();
</script>

<div id="middle">
<h2>MagicLine Session Encrypted Result</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Session Encrypted Result </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
로그인 후 서버와 클라이언트 사이에 공유한 세션키를 이용한 웹 구간 전달 메시지
전체에 대해 암호화 메시지를 구성합니다.
                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="sessionEncryptR.jsp" method="post" name="popForm">
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Session Encrypted Request Data</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>text1</td>
					<td><%=text1%></td>
				</tr>
				<tr>
				</tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>


</form>

</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
	document.onload = setBreadcrumDiv();
	function setBreadcrumDiv() {
		var breadcrumbDiv = document.getElementById('breadcrumb-div');
		breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Certificate Login</td>';
	}
</script>

<jsp:include page="include/footer.jsp"></jsp:include>