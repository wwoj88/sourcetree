<%@ page contentType="text/html;charset=utf-8"%>

<%@ page import="com.dreamsecurity.magicline.handle.RequestContent" %>
<%@ page import="com.dreamsecurity.magicline.processor.Processor" %>
<%@ page import="com.dreamsecurity.magicline.processor.ProcessorFactory" %>

<%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %>
<%@ page import="com.dreamsecurity.jcaos.exception.IdentifyException"%>

<%@ page import="java.net.URLEncoder" %>
<%
	String query = request.getParameter("encryptedData");

	String text1 = "";
	String text2 = "";
	String text3 = "";

	byte[] privatekeyRandom = null;
	String signType = "";
	String subDN = "";
	java.math.BigInteger serialNumber = null;
	int messageType;
	String signData = "";


	try {
		RequestContent reqContent = new RequestContent(query);
		ProcessorFactory factory = new ProcessorFactory();
		Processor process = factory.createProcessor(reqContent);
		process.execute();

		text1 = process.getParameter("text1");
		text2 = process.getParameter("text2");
		text3 = process.getParameter("text3");

		//Infomation
		X509Certificate cert = process.getSignerCerts()[0];
		subDN = cert.getSubjectDN().getName();
		serialNumber = cert.getSerialNumber();
		messageType = process.getMessageType();
		privatekeyRandom = process.getPrivateKeyRandom();
		signData = process.getParameter("signData");


	} catch (Exception e) {
		// 인증서 검증 실패
		// 인증서 정책검증 실패
		// 본인확인 실패
		// 복화화 실패
		StringBuffer sb  = new StringBuffer(1500);
		sb.append(e.getMessage());
		session.setAttribute("magiclineErr",sb.toString());
		response.sendRedirect("magicline_err.jsp");
		return;
	}
%>


<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>



<script language="javascript" src="../js/deployJava.js"></script>
<script language="javascript" src="../js/PluginDetect.js"></script>
<script language="javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	runMagicLine();
</script>

<div id="middle">
<h2>MagicLine Envelope Result</h2>
<div id="workArea"><!-- DIV START  -->

<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> Envelope Encrypted Result </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0; ">
					<nobr> description  :
					 				</nobr></td>
					<td style="border: 0;">
서버는 클라이언트에서 전달된 Envelope 메세지를 가공하여 암호화된 데이터를 추출해 서버의 개인키로 복호화 합니다.
                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="login_renewR.jsp" method="post" name="popForm">
<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Envelope Encrypted Request Data</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>text1</td>
					<td><%=text1%></td>
				</tr>
				<tr>
					<td>text2</td>
					<td><%=text2%></td>
				</tr>
				<tr>
					<td>text3</td>
					<td><%=text3%></td>
				</tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th colspan="2">Client Certificate Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<table class="normal" cellspacing="0">
				<tr>
					<td>USER Certificate Dn:</td>
					<td><%=subDN%></td>
				</tr>
				<tr>
					<td>Serial Number:</td>
					<td><%=serialNumber.toString() %></td>
				</tr>
				<tr>
					<td>Private Key Random:</td>
					<td><%=privatekeyRandom %></td>
				</tr>
				<tr>
					<td>Message Type:</td>
					<td><%=messageType %></td>
				</tr>
				<tr>
					<td>Sign Type:</td>
					<td><%=signType %></td>
				</tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</form>

<p>&nbsp;</p>

<table style="width: 100%" class="styledLeft">
	<thead>
		<tr>
			<th >Program Guide (Examples)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="formRow">
			<ul type="disc">
			<li>
				<p>1.MagicLine class include </p>
			<pre class="programlisting">
&lt;%@ page import="com.dreamsecurity.magicline.handle.RequestContent" %&gt;
&lt;%@ page import="com.dreamsecurity.magicline.processor.Processor" %&gt;
&lt;%@ page import="com.dreamsecurity.magicline.processor.ProcessorFactory" %&gt;
&lt;%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %&gt;
&lt;%@ page import="com.dreamsecurity.jcaos.exception.IdentifyException"%&gt;
&lt;%@ page import="java.net.URLEncoder" %&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			<li>
				<p>2.MagicLine Envelope result </p>
<pre class="programlisting"><span class="emphasis"><em>&lt;!-- 다음과 같이 선언하여 MagicLine Server에서 복호화를 수행하도록 한다. --&gt; </em></span>

&lt;%
	String query = request.getParameter("encryptedData");
	String text1 = "";
	String text2 = "";
	String text3 = "";


	try {
		RequestContent reqContent = new RequestContent(query);
		ProcessorFactory factory = new ProcessorFactory();
		Processor process = factory.createProcessor(reqContent);
		process.execute();

		text1 = process.getParameter("text1");<span class="emphasis"><em>//text1 항목 복호화 정보</em></span>
		text2 = process.getParameter("text2");<span class="emphasis"><em>//text2 항목 복호화 정보</em></span>
		text3 = process.getParameter("text3");<span class="emphasis"><em>//text3 항목 복호화 정보</em></span>

		//Infomation
		cert = dReq.getSignerCert();<span class="emphasis"><em>//사용자의 인증서 정보 (X509Certificate)  </em></span>
		subDN = cert.getSubjectDN().getName();<span class="emphasis"><em>//사용자 인증서 DN  </em></span>
		serialNumber = cert.getSerialNumber();<span class="emphasis"><em>//사용자 인증서 SerialNumber  </em></span>
		messageType = dReq.getRequestMessageType();<span class="emphasis"><em>// Client Message Type  </em></span>
		privatekeyRandom = dReq.getSignerRValue();<span class="emphasis"><em>//사용자 Private Random </em></span>
		signType = dReq.getSignType();<span class="emphasis"><em>//사용자 인증서 타입 </em></span>


	} catch (Exception e) {
		out.println("[전자서명 검증 및 복호화 실패] error message:" + e.getMessage());
		return;
	}

	<span class="emphasis"><em>//복호화 성공. </em></span>

%&gt;
				</pre>
				<p>&nbsp;</p>
			</li>

			</ul>
			</td>
		</tr>
	</tbody>
</table>

</div><!-- DIV END  -->


<script type="text/javascript">
	setCookie('current-breadcrumb', 'magicline_v40_menu');
	document.onload = setBreadcrumDiv();
	function setBreadcrumDiv() {
		var breadcrumbDiv = document.getElementById('breadcrumb-div');
		breadcrumbDiv.innerHTML = '<table cellspacing="0"><tr><td class="breadcrumb-link"><a href="index.jsp">Home</a></td><td class="breadcrumb-link">&nbsp;>&nbsp;MagicLine4</td><td class="breadcrumb-link">&nbsp;>&nbsp;Certificate Login</td>';
	}
</script>

<jsp:include page="include/footer.jsp"></jsp:include>