<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %>
<jsp:include page="include/header.jsp"></jsp:include>
<jsp:include page="include/menu.jsp"></jsp:include>


<%
	DSHttpServletResponse magiclineresponse = null;
	String challenge = "";
	try{
		challenge = magiclineresponse.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
	}
%>


<script type="text/javascript" src="../js/deployJava.js"></script>
<script type="text/javascript" src="../js/PluginDetect.js"></script>
<script type="text/javascript" src="../js/MagicLine.js"></script>

<script language='javascript'>
	runMagicLine();
</script>

<h2>NTS Login</h2>
<div id="workArea"><!-- DIV START  -->
<table class="styledLeft">
	<tr>
		<td style="border: 0;"><nobr> NTS Login( cert ) Samples </nobr></td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<table style="border: 0;">
			<tbody>
				<tr style="border: 0;">
					<td style="border: 0;"><nobr> Debug mode : <select
						name="debug" id="debug" onchange="setDebugMod()">
						<option value="true">true</option>
						<option value="false">false</option>
					</select> </nobr></td>
					<td style="border: 0;">
                    </td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>


<form action="sample-nts-login-action" name="cert" method="post">
<input type="hidden" name="challenge" value="<%=challenge%>">
<table>
	<tr>
		<td><a href="#" onclick="CertLogin();return false;" id="icon"
			style="cursor: pointer; background-image: url(NTSMagicLine4/images/user-login.gif);">Cert
		Login</a>&nbsp;</td>
		<td width="20%">&nbsp;</td>
	</tr>
</table>

<p>&nbsp;</p>

<table class="styledLeft" id="sgTable" width="100%">
	<thead>
		<tr>
			<th colspan="2">function</th>
			<th colspan="3">return value</th>
		</tr>
	</thead>
	<tbody>
		<tr bgcolor="white">
			<td width="10px" rowspan="1" style="text-align: center;">1</td>
			<td width="200px" rowspan="1"><nobr>
			document.MagicLine.genSessionKey(was_enccert); </nobr></td>
			<td width="420px" colspan="3"><input type="text" name="p_skey"
				size="58" value="" />&nbsp;</td>
		</tr>
		<tr bgcolor="#EEEFFB">
			<td width="10px" rowspan="1" style="text-align: center;">2</td>
			<td width="200px" rowspan="1"><nobr>
			document.MagicLine.ntsStrongAuth(); </nobr></td>
			<td width="320px" colspan="3"><input type="text" name="p_logstr"
				size="58" value="" />&nbsp;</td>
		</tr>
		<tr bgcolor="white">
			<td width="10px" rowspan="1" style="text-align: center;">3</td>
			<td width="200px" rowspan="1"><nobr>
			document.MagicLine.tranx2PEM(); </nobr></td>
			<td width="420px" colspan="3"><input type="text" name="p_cert"
				size="58" value="" />&nbsp;</td>
		</tr>

		<tr bgcolor="#EEEFFB">
			<td width="10px" rowspan="1" style="text-align: center;">4</td>
			<td width="200px" rowspan="1"><nobr>
			document.MagicLine.getRandomfromPrivateKey(); </nobr></td>
			<td width="320px" colspan="3"><input type="text" name="p_rnd"
				size="58" value="" />&nbsp;</td>
		</tr>
		<tr bgcolor="white">

			<td width="10px" rowspan="1" style="text-align: center;">5</td>

			<td width="200px" rowspan="1"><nobr>
			document.MagicLine.encryptWebData(sData); </nobr></td>
			<td width="420px" colspan="3"><input type="text"
				name="p_rnd_enc" size="58" value="" />&nbsp;</td>
		</tr>

		<tr bgcolor="#EEEFFB">
			<td width="10px" rowspan="1" style="text-align: center;">6</td>
			<td width="200px" rowspan="1"><nobr>
			document.MagicLine.genHashValue(StrMsg, algo); </nobr></td>
			<td width="320px" colspan="3"><input type="text" name="p_hash"
				size="58" value="" />&nbsp;</td>
		</tr>

		<tr bgcolor="white">
			<td width="10px" rowspan="1" style="text-align: center;">7</td>
			<td width="200px" rowspan="1"><nobr> p_login_rnd </nobr></td>
			<td width="420px" colspan="3"><input type="text"
				name="p_login_rnd" size="58" value="" />&nbsp;</td>
		</tr>
	</tbody>
</table>
</form>


</div><!-- DIV END  -->
<jsp:include page="include/footer.jsp"></jsp:include>



