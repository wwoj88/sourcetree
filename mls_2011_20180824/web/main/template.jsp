<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작권찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
</head>
<body class="subBg">
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp"/>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<div id="subVisual">
			<script type="text/javascript" language="javascript">
    	   						GetFlash('/images/swf/subVisual.swf','725','122');
       					</script>
		</div>
		<!--subNavi start-->
		<div id="subNavi"></div>
		<!--subNavi end-->
		<!--contents start-->
		<div id="contents">
			<h1 class="rightSrchTlt">내권리찾기 신청</h1>
			<!--현재위치 start-->
			<div id="location">
				<ul>
					<li class="none"><img src="/images/common/home_ico.gif" alt="Home" />Home</li>
					<li>내권리찾기신청</li>
					<li class="on">내권리찾기 목록</li>
				</ul>
			</div>
			<!--현재위치 end-->
			<!--miplatform start-->
			<div id="miplatform"><span class="hide">마이플랫폼 들어갈 자리임</span></div>
			<!--miplatform end-->
		</div>
		<!--contents end-->
	</div>
	<!--contentsBody end-->
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
