<%@ page contentType="text/html;charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title>저작권 찾기</title>
<link href="/css/mls.css" rel="stylesheet" type="text/css" />
<script src="/js/flash.js" type="text/javascript"></script>
<script src="/js/link.js" type="text/javascript"></script>
</head>
<body class="mainBg">
<!--searchBox start-->
<div id="indexSrch">
	<select name="select" id="select" class="select">
		<option>통합검색</option>
		<option>음악</option>
		<option>어문</option>
		<option>방송컨텐츠</option>
	</select>
	<div class="inputBox"> <input type="text" class="input" size="21" /> <a href="#"><img src="/images/search_btn.gif" alt="검색" width="40" height="16" class="btn" /></a></div>
</div>
<!--searchBox end-->
<div id="wrap">
	<!--header start-->
	<jsp:include page="/include/top.jsp"/>
	<!--header end-->
	<!--contentsBody start-->
	<div id="contentsBody">
		<!-- part01 start-->
		<div id="part01">
			<div id="mainVisual">
				<h1 class="hide">이용안내바로가기</h1>
				<script type="text/javascript" language="javascript">
    	   						GetFlash('/images/swf/mainVisual.swf','686','263');
       					</script>
			</div>
			<!-- all start-->
			<div id="all">
				<div class="sideLeft">
					<h1 class="hide">신청화면바로가기</h1>
					<div>
						<script type="text/javascript" language="javascript">
    	   						GetFlash('/images/swf/quickApp.swf','211','182');
       					</script>
					</div>
				</div>
				<!-- sideRight start-->
				<div class="sideRight">
					<!-- status start-->
					<div class="status">
						<div class="tabBox">
							<ul>
								<li><img src="/images/newWrite_on.gif" alt="신규저작물 등록현황" /></li>
								<li><a href="#"><img src="/images/inmt_off.gif" alt="보상금 저작물 현황" /></a></li>
							</ul>
						</div>
						<h1 class="hide">신규 저작물 현황</h1>
						<div class="title"><img src="/images/newWrite_tlt.gif" alt="신규 저작물 현황" class="floatL" /><img src="/images/more.gif" alt="더보기" align="bottom" class="floatR" /></div>
						<!-- listBox start-->
						<div class="listBox">
							<table width="414" border="0" cellspacing="0" cellpadding="0">
								<colgroup>
								<col width="62" />
								<col width="176" />
								<col width="94" />
								<col width="82" />
								</colgroup>
								<thead class="header">
									<tr>
										<th scope="col">구분</th>
										<th scope="col">저작물명</th>
										<th scope="col">저작권자</th>
										<th scope="col">등록일자</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>음악</td>
										<td>Gee</td>
										<td>소녀시대</td>
										<td>2009.0102</td>
									</tr>
									<tr>
										<td>음악</td>
										<td>붉은노을</td>
										<td>빅뱅</td>
										<td>2009.01.02</td>
									</tr>
									<tr>
										<td>어문</td>
										<td>퇴마록</td>
										<td>이우혁</td>
										<td>2009.02.01</td>
									</tr>
									<tr>
										<td>음악</td>
										<td>Gee</td>
										<td>소녀시대</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>음악</td>
										<td>붉은노을</td>
										<td>빅뱅</td>
										<td>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</div>
						<!-- listBox start-->
					</div>
					<!-- status end-->
				</div>
				<!-- sideRight start-->
			</div>
			<!-- all start-->
		</div>
		<!-- part01 end-->
		<!-- part02 start-->
		<div id="part02">
			<div class="faq">
				<h1 class="hide">FAQ</h1>
				<div class="faqTlt"><img src="/images/faq_tlt.gif" alt="FAQ" class="floatL" /><a href="#"><img src="/images/more2.gif" alt="더보기" width="32" height="9" align="bottom" class="floatR" /></a></div>
				<ul>
					<li><a href="#">외국인에게도 방송보상금이 지급...</a></li>
					<li><a href="#">외국인에게도 방송보상금이 지급...</a></li>
					<li><a href="#">외국인에게도 방송보상금이 지급...</a></li>
					<li><a href="#">외국인에게도 방송보상금이 지급...</a></li>
					<li><a href="#">외국인에게도 방송보상금이 지급...</a></li>
				</ul>
			</div>
			<div class="banner">
				<ul>
					<li><a href="#"><img src="/images/certifiGuide.gif" alt="" /></a></li>
					<li><a href="#"><img src="/images/faqQuick.gif" alt="자주묻는 질문과답변" /></a></li>
					<li><a href="#"><img src="/images/qnaQuick.gif" alt="문의하기" /></a></li>
				</ul>
			</div>
		</div>
		<!-- part02 start-->
	</div>
	<!--contentsBody end-->
	<div id="refSite">
		<h1 class="hide">유관기관사이트링크</h1>
		<ul>
			<li><img src="/images/refSite01.gif" alt="한국문예학술저작권협회" /></li>
			<li><a href="#"><img src="/images/refSite02.gif" alt="한국음악실연자연합회" /></a></li>
			<li><a href="#"><img src="/images/refSite03.gif" alt="한국음악저작권협회" /></a></li>
			<li><a href="#"><img src="/images/refSite04.gif" alt="한국음원제작자협회" /></a></li>
			<li><a href="#"><img src="/images/refSite05.gif" alt="KBPA" /></a></li>
		</ul>
	</div>
	<!--하단영역 start-->
	<jsp:include page="/include/bottom.jsp"/>
	<!--하단영역 end-->
</div>
</body>
</html>
