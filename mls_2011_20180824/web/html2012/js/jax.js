/**
 * // 페이지 이동 없이 현재 페이지에서 다른 페이지의 내용을 가져와 결과를 처리할 수 있다.
 * 
 * // 다음과 같이 동기 호출 함수를 호출하면 호출 페이지의 소스를 기다린 후 값을 받는다.
 * var src = synchRequest(url);
 *
 * // 다음과 같이 비동기 처리 함수를 호출하면 호출 후 결과를 기다릴 필요 없이 다음 처리를 할 수 있다.
 * // 그리고 호출 결과는 Call Back함수에서 처리한다. 호출 페이지의 소스가 Call Back 함수에 전달된다.
 * var src = asynchRequest(url, fnCallBack);
 * function fnCallBack(src) {
 *   alert(src);
 * }
 */

	function getXmlHttp() 
	{
		var req;
	
		try 
		{ 
			req = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e) 
		{
			try 
			{
				req = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e) 
			{
				req = false;
			}
		}
	
		if (!req && typeof XMLHttpRequest != 'undefined')
			req = new XMLHttpRequest();
	
		return req;
	}
	
	function getHandler(req, reqHandler) 
	{
		return function () 
		{
			if (req.readyState == 4) 
			{
				if (req.status == 200) 
					reqHandler(req.responseText);
				else 
				{
					alert("실행중 에러가 발생했습니다.\n다시 실행해 주세요.");
					reqHandler("_error");
				}
			}
		};
	}

	function asynchRequest(strUrl, reqHandler) 
	{
		var req = getXmlHttp();
	
		req.onreadystatechange = getHandler(req, reqHandler);
	
		req.open("POST", strUrl, true);
		req.send();
	}

	function asynchRequestGet(strUrl, reqHandler) 
	{
		var req = getXmlHttp();
	
		req.onreadystatechange = getHandler(req, reqHandler);
	
		req.open("GET", strUrl, true);
		req.send(null);
	}

	function synchRequest(urlStr) 
	{
		try
		{
			var req = getXmlHttp();
	
			req.open("POST", urlStr, false);
			req.send();
	
			if(req.status == 200)
				return req.responseText;
			else
			{
				alert("실행중 에러가 발생했습니다.\n[page error code : " + req.status + "]\n다시 실행해 주세요.");
				
				return "_error";
			}
		}
		catch(e)
		{
			alert("URL 및 폼 정보 구성오류입니다.\n\n" + urlStr);
			
			return "_error";
		}	
	}

	function synchRequestGet(urlStr) 
	{
		var req = getXmlHttp();

		req.open("GET", urlStr, false);
		req.send();

		if(req.status == 200)
			return req.responseText;
		else
		{
			alert("실행중 에러가 발생했습니다.\n다시 실행해 주세요.");
			return "_error";
		}
	}


////////////////////////////////////////////////////////////////////////////////////////

	function xmlHttpPost(forms)
	{
		var result = "";
	    
	    var xmlHttpRequest = false;
	    
	    if(window.ActiveXObject)
	        xmlHttpRequest = new ActiveXObject('Microsoft.XMLHTTP');
	    else
	    {
	        xmlHttpReq = new XMLHttpRequest();
	        xmlHttpReq.overrideMimeType('text/xml');
	    }

		var actionUrl		= forms.action;

	    xmlHttpRequest.open('POST', actionUrl, false);
	    xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=euc-kr');
	    
	    xmlHttpRequest.onreadystatechange = function() 
	    {
	        if(xmlHttpRequest.readyState == 4)
	        {
	        	if(xmlHttpRequest.status == 404)
					alert('오류: ' + actionUrl + '이 존재하지 않음');

				else if(xmlHttpRequest.status == 500)
					alert('오류: ' + xmlHttpRequest.responseText);

				else
					result = xmlHttpRequest.responseText;
	        }
	    }

// alert(formData2QueryString(forms, 'Y'));

	    xmlHttpRequest.send(getQueryStr(forms, 'Y'));
//	    xmlHttpRequest.send(formData2QueryString(forms, 'Y'));

	    return result;
	}                                    

////////////////////////////////////////////////////////////////////////////////////////

	// 폼에 있는 모든 값을 읽어서 get방식의 Query String 문자열 구하기

	function getQueryString(frm) 
	{
		return frm.action + "?" + getQueryStr(frm, 'Y');
	}
	
	function getQueryStr(frm, flag) 
	{
		var str = '';
	
		var strQuery = "";
		
		for (var i = 0 ; i < frm.elements.length ; i++)
		{
			if(frm.elements[i].name) 
			{
				switch(frm.elements[i].tagName) 
				{
					case "INPUT" :
					
						switch(frm.elements[i].type.toLowerCase()) 
						{
							case "checkbox" :

								if(frm.elements[i].checked) 
									str += frm.elements[i].name + "=" + urlEncode(frm.elements[i].value, flag) + "&";

							case "radio" :

								if(frm.elements[i].checked)
									str += frm.elements[i].name + "=" + urlEncode(frm.elements[i].value, flag) + "&";
								break;

							case "file" :

								str += frm.elements[i].name + "=" + urlEncode(frm.elements[i].value, flag) + "&";
								break;

							case "image" :

								break;

							case "submit" :

								break;

							default :

								str += frm.elements[i].name + "=" + urlEncode(frm.elements[i].value, flag) + "&";
						}
						
						break;
						
					case "SELECT" :

						for(var j = 0; j < frm.elements[i].options.length; j++) 
						{
							if(frm.elements[i].options[j].selected)
								str += frm.elements[i].name + "=" + urlEncode(frm.elements[i].options[j].value, flag) + "&";
						}

						break;

					default :

						str += frm.elements[i].name + "=" + urlEncode(frm.elements[i].value, flag) + "&";
				}
	
				if(str != "") 
				{
					strQuery += str;
					str = "";
				}
			}
		}
		//alert(strQuery);
		return strQuery;
	}
	
	// 2010.08.16 김소라 추가
	// check 박스만 사용목적
	function getCheckStr(frm, flag) 
	{
		var str = '';
	
		var strQuery = "";
		
		for (var i = 0 ; i < frm.elements.length ; i++)
		{
			if(frm.elements[i].name) 
			{
				switch(frm.elements[i].tagName) 
				{
					case "INPUT" :
					
						switch(frm.elements[i].type.toLowerCase()) 
						{
							case "checkbox" :

								if(frm.elements[i].checked) 
									str += "&"+frm.elements[i].name + "=" + urlEncode(frm.elements[i].value, flag) ;

						}
						
						break;
						

				}
	
				if(str != "") 
				{
					strQuery += str;
					str = "";
				}
			}
		}
	
		return strQuery;
	}

	//URL QUERYSTRING ENCDOE

	function urlEncode(str, flag) 
	{
		var tNum, num1, num2;
		var strReturn = "";
		
		if(flag == 'Y')
		{
			for(var i = 0; i < str.length; i++) 
			{
				if(/[_0-9A-Za-z]/.test(str.charAt(i))) 
					strReturn += str.charAt(i);
				else 
				{
					tNum = str.charCodeAt(i);
					
					if(tNum > 255) 
						strReturn += str.charAt(i);
					else 
						strReturn += "%" + toHexCode(tNum);
				}
		    }
		}
		else
			strReturn = str;
		
		return strReturn;
	}

	// 10진수를 16진수 문자열 구하기	
	
	function toHexCode(num) 
	{
		var str = new Array;
		var tNum = new Array;
	
		tNum[0] = num >> 4;
		tNum[1] = num & 15;
		
		for(var i in tNum) 
		{
			if(tNum[i] > 9) 
				str[i] = String.fromCharCode(tNum[i] + 55);//10 + 55 > "A"
			else
				str[i] = tNum[i].toString();
		}
	
		return str[0] + str[1];
	}
	
	
	function formData2QueryString(frm, flag)
	{    
		var str = '';
	
		var strQuery = "";
		
		for (var i = 0 ; i < frm.elements.length ; i++)
		{
			if(frm.elements[i].name) 
			{
				switch(frm.elements[i].tagName) 
				{
					case "INPUT" :
					
						switch(frm.elements[i].type.toLowerCase()) 
						{
							case "checkbox" :

								if(frm.elements[i].checked) 
									str += frm.elements[i].name + "=" + paramEscape(frm.elements[i].value, flag) + "&";

							case "radio" :

								if(frm.elements[i].checked)
									str += frm.elements[i].name + "=" + paramEscape(frm.elements[i].value, flag) + "&";
								break;

							case "file" :

								str += frm.elements[i].name + "=" + paramEscape(frm.elements[i].value, flag) + "&";
								break;

							case "image" :

								break;

							case "submit" :

								break;

							default :

								str += frm.elements[i].name + "=" + paramEscape(frm.elements[i].value, flag) + "&";
						}
						
						break;
						
					case "SELECT" :

						for(var j = 0; j < frm.elements[i].options.length; j++) 
						{
							if(frm.elements[i].options[j].selected)
								str += frm.elements[i].name + "=" + paramEscape(frm.elements[i].options[j].value, flag) + "&";
						}

						break;

					default :

						str += frm.elements[i].name + "=" + paramEscape(frm.elements[i].value, flag) + "&";
				}
	
				if(str != "") 
				{
					strQuery += str;
					str = "";
				}
			}
		}
		
		alert(strQuery);
		return strQuery;
	}

	
	function paramEscape(paramValue, flag)
	{
		var result = paramValue;
		
		if(flag == 'Y')
//			result = escape(paramValue).replace(/\+/g, '%2B')
			result = encodeURIComponent(paramValue);
		else
			result = paramValue;	
	
	    return result;
	}	