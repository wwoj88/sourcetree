function df(){
	return document.forms[0];
}

//---------------------------------------------------------------------------------
// 함수 설명 :	왼쪽의 공백을 제거해주는 함수
//---------------------------------------------------------------------------------
// 인자 설명 :	문자
//---------------------------------------------------------------------------------
// 반 환 값  :  왼쪽의 공백이 제거된 문자
//---------------------------------------------------------------------------------
function LTrim(str) {
	var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(0)) != -1) {
    	var j=0, i = s.length;
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
           	j++;
        s = s.substring(j, i);
    }
    return s;
}
//---------------------------------------------------------------------------------
// 함수 설명 :	오른쪽의 공백을 제거해주는 함수
//---------------------------------------------------------------------------------
// 인자 설명 :	문자
//---------------------------------------------------------------------------------
// 반 환 값  :  오른쪽의 공백이 제거된 문자
//---------------------------------------------------------------------------------
function RTrim(str) {
	var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
    	var i = s.length - 1;
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
			i--;
		s = s.substring(0, i+1);
	}
    return s;
}
//---------------------------------------------------------------------------------
// 함수 설명 :	양쪽의 공백을 제거해주는 함수
//---------------------------------------------------------------------------------
// 인자 설명 :	문자
//---------------------------------------------------------------------------------
// 반 환 값  :  양쪽의 공백이 제거된 문자
//---------------------------------------------------------------------------------
function Trim(str) {
	return RTrim(LTrim(str));
}

//-------------------------------
// 동일한 name의 체크박스 모두 선택
// @param  pCheckAll : 전체 체크박스 name
//					pCheck : 선택될 체크박스 name
//--------------------------------
function checkAll(pCheckAll, pCheck){
	var df = document.forms[0];
	var ss ="";

	for(var i=0; i< df.elements.length; i++) {
		var ele1 = df.elements[i];
		if(ele1.name == pCheckAll){
			if(ele1.checked == true){
				var ss = 2;
			}else{
				var ss = 1;
			}
		}
	}

	for(var i=0; i< df.elements.length; i++) {
		var ele = df.elements[i];
		if(ele.name == pCheck){
			if(ss == 2){
				ele.checked=true;
			}else{
				ele.checked=false;
			}
		}
	}
}

//-------------------------------
// 동일한 id의 체크박스 모두 선택
// @param  pCheckAll_Id : 전체 체크박스 id
//					pCheckId : 선택될 체크박스 id
//--------------------------------
function checkAll_Id(pCheckAll_Id, pCheckId){
	var df = document.forms[0];
	var ss ="";

	for(var i=0; i< df.elements.length; i++) {
		var ele1 = df.elements[i];
		if(ele1.id == pCheckAll_Id){
			if(ele1.checked == true){
				var ss = 2;
			}else{
				var ss = 1;
			}
		}
	}

	for(var i=0; i< df.elements.length; i++) {
		var ele = df.elements[i];
		if(ele.id == pCheckId){
			if(ss == 2){
				ele.checked=true;
			}else{
				ele.checked=false;
			}
		}
	}
}


//-------------------------------
// ??????? ????? ? ???? ????? ? (???? ???)
// @param  pCheckAll_id : ???? ????? ???????,
//					pCheckId : ????? ?????? ???????
//--------------------------------
function check_sub(pCheckAll_id, pCheckId){
	var df= document.forms[0];
	var dfElement = "";
	var dfTemp = "";

	for(var i=0; i< df.elements.length; i++) {
		var ele = df.elements[i];
		if(ele.id == pCheckId){
			if(ele.checked == false){
				dfElement = 2;
				dfTemp +="2";
			} else {
				dfTemp += "1";
			}
		}
	}

	for(var i=0; i< df.elements.length; i++) {
		var ele = df.elements[i];
		if(ele.id == pCheckAll_id){
			if(dfElement == 2){
				ele.checked=false;
			} else {

				for(var k=0; k< dfTemp.length; k++){
					if((dfTemp.indexOf("2"))<0){
						ele.checked=true;
					} else {
						ele.checked=false;
					}
				}
			}
		}
	}
}

//---------------------------------------------------------------------------------
// 함수 설명 :	페이지이동
//---------------------------------------------------------------------------------
// 인자 설명 :	이동할 페이지 주소
//---------------------------------------------------------------------------------
function goMove(movePage){
	var df = document.forms[0];
	df.action=movePage;
	df.target="_self";
	df.submit();
}

//---------------------------------------------------------------------------------
// 함수 설명 :	페이지이동
//---------------------------------------------------------------------------------
// 인자 설명 :	이동할 페이지 주소
//				   :	타켓
//---------------------------------------------------------------------------------
function goMoveTarget(movePage, target){
	var df = document.forms[0];
	df.action=movePage;
	df.target=target;
	df.submit();
}

//-------------------------------
// 입력창(값길이 체크 스크립트)
// 적용:<textarea>에서 사용시 onkeyup="fc_chk_byte(this,적용byte수)"
//-------------------------------
	function fc_chk_byte(aro_name,ari_max){

		var ls_str = aro_name.value; 				// 이벤트가 일어난 컨트롤의 value 값
		var li_str_len = ls_str.length; 			// 전체길이

		// 변수초기화
		var li_max = ari_max; 						// 제한할 글자수 크기
		var i = 0; 									// for문에 사용
		var li_byte = 0; 							// 한글일경우는 2 그밗에는 1을 더함
		var li_len = 0; 							// substring하기 위해서 사용
		var ls_one_char = ""; 						// 한글자씩 검사한다
		var ls_str2 = ""; 							// 글자수를 초과하면 제한할수 글자전까지만 보여준다.

		for(i=0; i< li_str_len; i++){

			// 한글자추출
			ls_one_char = ls_str.charAt(i);

			// 한글이면 2를 더한다.
			if (escape(ls_one_char).length > 4){
				li_byte += 2;
			}else{	// 그밗의 경우는 1을 더한다.
				li_byte++;
			}

			// 전체 크기가 li_max를 넘지않으면
			if(li_byte <= li_max){
				li_len = i + 1;
			}
		}

		//alert(li_str_len);

		// 전체길이를 초과하면
		if(li_byte > li_max){
			alert( li_max + " 글자를 초과 입력할수 없습니다. \n 초과된 내용은 자동으로 삭제 됩니다. ");
			ls_str2 = ls_str.substr(0, li_len);
			aro_name.value = ls_str2;
		}
		aro_name.focus();
}





//---------------------------------------------------------------------------------
// 함수 설명 :	텍스트 필드에 반드시 글자를 입력해야 할 경우...
//---------------------------------------------------------------------------------
// 인자 설명 :	문자열, 폼요소 이름
//---------------------------------------------------------------------------------
// 반 환 값  :  양쪽의 공백이 제거된 문자
//---------------------------------------------------------------------------------
function chkTxtNull(strTxt, strName) {
	var tmp = Trim(strTxt);

	if(tmp.length == 0) {
		alert (strName + ' 입력해주세요.');
		return true;
	}
	return false;
}


//---------------------------------------------------------------------------------
// 함수 설명 :	숫자만 쓰게 만들어 보자...
//---------------------------------------------------------------------------------
// 인자 설명 :	strTmp  : 숫자인지 검사할 문자열
//			 	   :	msg		 : 숫자가 아닐경우 alert 창에 보여줄 메시지
//---------------------------------------------------------------------------------
// 반 환 값  :  양쪽의 공백이 제거된 문자
//---------------------------------------------------------------------------------
function chkNum(strTmp, msg) {

	var Num = '0123456789';

	for (i=0; i<strTmp.length; i++) {
		if (Num.indexOf(strTmp.substring(i,i+1))<0) {
			alert(msg+ '숫자만 입력해주세요');

			return true;
		}
	}
	return false;
}

//---------------------------------------------------------------------------------
// 함수 설명 :	숫자와 점(.)만 쓰게 만들어 보자...
//---------------------------------------------------------------------------------
// 인자 설명 :	strTmp  : 숫자인지 검사할 문자열
//			 	   :	msg		 : 숫자가 아닐경우 alert 창에 보여줄 메시지
//---------------------------------------------------------------------------------
// 반 환 값  :  양쪽의 공백이 제거된 문자
//---------------------------------------------------------------------------------
function chkNum2(strTmp, msg) {

	var Num = '0123456789.';

	for (i=0; i<strTmp.length; i++) {
		if (Num.indexOf(strTmp.substring(i,i+1))<0) {
			alert(msg+ '숫자와 소수점만 입력해주세요');

			return true;
		}
	}
	return false;
}

//---------------------------------------------------------------------------------
// 함수 설명 :	영문 숫자 _ 만 입력 가능
//---------------------------------------------------------------------------------
// 인자 설명 :	strTmp  : 숫자인지 검사할 문자열
//			 	   :	msg		 : 숫자가 아닐경우 alert 창에 보여줄 메시지
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
	function Valid_INT_ENG(strTmp, msg) {
		var Num = "abcdefghijklmnopqrstuvwxyz0123456789_"

		for (i=0; i<strTmp.length; i++) {
			if (Num.indexOf(strTmp.substring(i,i+1))<0) {
				alert(msg+ '영문자, 숫자, _만 입력해주세요(공백없이) ');
				return true;
			}
		}
		return false;
	}


//---------------------------------------------------------------------------------
// 함수 설명 :	이미지 확장자 체크
//---------------------------------------------------------------------------------
// 인자 설명 :	이미지 파일 값
//			 	   :	확장자
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
 function checkImage(ImageValue, StringExt){
	if(ImageValue != null){
	 	var splitLength = (ImageValue.split(".")).length;

	 	if(( ImageValue.split(".")[splitLength-1] ).toUpperCase() == (StringExt.toUpperCase()) ){
	 		return true;
	 	}else{

	 		return false;
	 	}
	}
}
//---------------------------------------------------------------------------------
// 함수 설명 :	날짜체크
//---------------------------------------------------------------------------------
// 인자 설명 :	시작날짜
//			 	   :	종료날짜
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
function checkDate(StrDateValue, EndDateValue){

	if(StrDateValue != null && EndDateValue != null){

		var iStartDt = parseInt(StrDateValue);
		var iEndDt  = parseInt(EndDateValue);

		if(iStartDt > iEndDt) {
			alert('시작일이 종료일보다 이후입니다. 일자를 확인하세요.');
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}


//---------------------------------------------------------------------------------
// 함수 설명 :	금지어
//---------------------------------------------------------------------------------
// 인자 설명 :	금지어 포함한 문자열 (예:df().group_name.value)
//			 	   :	금지어						(예:'◆')
//			 	   :	요소이름 					(예:'분류명에')
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
function chkLimit(strTxt, strLimit, strName){
	var tmp = Trim(strTxt);

	if(strTxt.indexOf(strLimit) >-1 ){
		alert(strName + ' '+strLimit +'은(는) 사용할 수 없습니다.');
		return true;
	}
	return false;
}

//---------------------------------------------------------------------------------
// 함수 설명 :	엔터
//---------------------------------------------------------------------------------
// 인자 설명 :
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
function tabeffect(type) {
	 if(event.keyCode ==13){
		  if(type == "text" || type =="password"){
		      return true;
		  } else{
		   	  return false;
		   return;
		  }
	 }
}

//---------------------------------------------------------------------------------
// 함수 설명 :	주민등록 번호 체크 스크립트
//---------------------------------------------------------------------------------
// 인자 설명 :
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
	function ssnCheck(ssn1, ssn2)
	{
		var chk =0;
		var yy = ssn1.substring(0,2);
		var mm = ssn1.substring(2,4);
		var dd = ssn1.substring(4,6);
		var sex = ssn2.substring(0,1);

		if (ssn2.split(" ").join("") == "") {
			return;
		}

	 	if ((ssn1.length!=6)||(mm <1||mm>12||dd<1)){
	    		alert ('주민등록번호 앞부분이 잘못되었습니다.');
	    		document.form1.resdCorpNumb1.focus();
	    		return true;
	  	}

	  	if ((sex != 1 && sex !=2 )||(ssn2.length != 7 )){
	    		alert ('주민등록번호 뒷부분이 잘못되었습니다.');
	    		document.form1.resdCorpNumb2.focus();
	    		return true;
	  	}

	  	for (var i = 0; i <=5 ; i++){
			chk = chk + ((i%8+2) * parseInt(ssn1.substring(i,i+1)))
	 	}

	  	for (var i = 6; i <=11 ; i++){
	        	chk = chk + ((i%8+2) * parseInt(ssn2.substring(i-6,i-5)))
	 	}


	  	chk = 11 - (chk %11)
	  	chk = chk % 10


	  	if (chk != ssn2.substring(6,7))
	  	{
	    		alert ('유효하지 않은 주민등록번호입니다.');
	    		document.form1.resdCorpNumb1.focus();
	    		return true;
	  	}
	  	return false
	}

	function ssnCheck2(ssn1, ssn2)
	{
		var chk =0;
		var yy = ssn1.substring(0,2);
		var mm = ssn1.substring(2,4);
		var dd = ssn1.substring(4,6);
		var sex = ssn2.substring(0,1);

		if (ssn2.split(" ").join("") == "") {
			return;
		}

	 	if ((ssn1.length!=6)||(mm <1||mm>12||dd<1)){
	    		alert ('주민등록번호 앞부분이 잘못되었습니다.');
	    		document.form2.resdCorpNumb1.focus();
	    		return true;
	  	}

	  	if ((sex != 1 && sex !=2 )||(ssn2.length != 7 )){
	    		alert ('주민등록번호 뒷부분이 잘못되었습니다.');
	    		document.form2.resdCorpNumb2.focus();
	    		return true;
	  	}

	  	for (var i = 0; i <=5 ; i++){
			chk = chk + ((i%8+2) * parseInt(ssn1.substring(i,i+1)))
	 	}

	  	for (var i = 6; i <=11 ; i++){
	        	chk = chk + ((i%8+2) * parseInt(ssn2.substring(i-6,i-5)))
	 	}


	  	chk = 11 - (chk %11)
	  	chk = chk % 10


	  	if (chk != ssn2.substring(6,7))
	  	{
	    		alert ('유효하지 않은 주민등록번호입니다.');
	    		document.form2.resdCorpNumb1.focus();
	    		return true;
	  	}
	  	return false
	}
	
	function ssnCheck3(ssn1, ssn2)
	{
		var chk =0;
		var yy = ssn1.substring(0,2);
		var mm = ssn1.substring(2,4);
		var dd = ssn1.substring(4,6);
		var sex = ssn2.substring(0,1);

		if (ssn2.split(" ").join("") == "") {
			return;
		}

	 	if ((ssn1.length!=6)||(mm <1||mm>12||dd<1)){
	    		alert ('주민등록번호 앞부분이 잘못되었습니다.');
	    	//	document.form1.resdCorpNumb1.focus();
	    		return true;
	  	}

	  	if ((sex != 1 && sex !=2 )||(ssn2.length != 7 )){
	    		alert ('주민등록번호 뒷부분이 잘못되었습니다.');
	    	//	document.form1.resdCorpNumb2.focus();
	    		return true;
	  	}

	  	for (var i = 0; i <=5 ; i++){
			chk = chk + ((i%8+2) * parseInt(ssn1.substring(i,i+1)))
	 	}

	  	for (var i = 6; i <=11 ; i++){
	        	chk = chk + ((i%8+2) * parseInt(ssn2.substring(i-6,i-5)))
	 	}


	  	chk = 11 - (chk %11)
	  	chk = chk % 10


	  	if (chk != ssn2.substring(6,7))
	  	{
	    		alert ('유효하지 않은 주민등록번호입니다.');
	    		return true;
	  	}
	  	return false
	}

//---------------------------------------------------------------------------------
// 함수 설명 :	사업자번호 체크 스크립트
//---------------------------------------------------------------------------------
// 인자 설명 :
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
	function check_busino(vencod)
	{
	  var sum = 0;
	  var getlist =new Array(10);
	  var chkvalue =new Array("1","3","7","1","3","7","1","3","5");

	  for(var i=0; i<10; i++) { getlist[i] = vencod.substring(i, i+1); }
	  for(var i=0; i<9; i++) { sum += getlist[i]*chkvalue[i]; }

	  sum = sum + parseInt((getlist[8]*5)/10);
	  sidliy = sum % 10;
	  sidchk = 0;

	  if(sidliy != 0) { sidchk = 10 - sidliy; }
	  else { sidchk = 0; }

	  if(sidchk != getlist[9]) { return false; }
	  return true;
	}

//---------------------------------------------------------------------------------
// 함수 설명 :	법인번호 체크 체크 스크립트
//---------------------------------------------------------------------------------
// 인자 설명 :	111111-1111111
//---------------------------------------------------------------------------------
// 반 환 값  :     boolean
//---------------------------------------------------------------------------------
// 법인번호 체크
function fncJuriRegNoCheck(juriRegNo) {

   var no_ck = 0;

   juriRegNo = fncReplaceStr(juriRegNo, " ", "");
   juriRegNo = fncReplaceStr(juriRegNo, "-", "");
   juriRegNo = fncReplaceStr(juriRegNo, "/", "");

   if (juriRegNo.length != 13) 
      return false;
   
   no_ck = no_ck + juriRegNo.substring(0, 1) * 1;
   no_ck = no_ck + juriRegNo.substring(1, 2) * 2;
   no_ck = no_ck + juriRegNo.substring(2, 3) * 1;
   no_ck = no_ck + juriRegNo.substring(3, 4) * 2;
   no_ck = no_ck + juriRegNo.substring(4, 5) * 1;
   no_ck = no_ck + juriRegNo.substring(5, 6) * 2;
   no_ck = no_ck + juriRegNo.substring(6, 7) * 1;
   no_ck = no_ck + juriRegNo.substring(7, 8) * 2;
   no_ck = no_ck + juriRegNo.substring(8, 9) * 1;
   no_ck = no_ck + juriRegNo.substring(9,10) * 2;
   no_ck = no_ck + juriRegNo.substring(10,11) * 1;
   no_ck = no_ck + juriRegNo.substring(11,12) * 2;

   no_ck = (no_ck % 10);
   no_ck = 10 - no_ck;

   if (no_ck > 9) no_ck = 0;

   if (no_ck == juriRegNo.substring(12, 13))
        return true;
   else
        return false;
  
}

/**string replcae function **/
function fncReplaceStr(source, oldstr, newstr)
{
	var buffer="";
    var pos = source.indexOf(oldstr);
    if(pos >= 0) 
	{
        var currpos = 0;
        while(pos >= 0) 
		{
            buffer += source.substring(currpos, pos);
            buffer += newstr;
            currpos = pos + oldstr.length;
            pos = source.indexOf(oldstr, currpos);
        }
        buffer += source.substring(currpos);
    } 
	else buffer = source;
    return buffer;
}

//---------------------------------------------------------------------------------
// 함수 설명 :	법인번호 체크 체크 스크립트
//---------------------------------------------------------------------------------
// 인자 설명 :
//---------------------------------------------------------------------------------
// 반 환 값  :     boolean
//---------------------------------------------------------------------------------
	function checkCorpcode(num) {
	    var reg = /([0-9]{6})-?([0-9]{7})/;
	    if (!reg.test(num)) return false;
	    num = RegExp.$1 + RegExp.$2;
	    var cVal = 0;
	    for (var i=0; i<12; i++) {
	        var cKeyNum = parseInt(((_tmp = i % 2) == 0) ? 1 : 2);
	        cVal += parseFloat(num.substring(i,i+1)) * cKeyNum;
	    }
	    cVal = '0' + cVal;

	    var prty = (cVal.length==3) ? parseInt(10- cVal.substring(2, 3)) : parseInt(10- cVal.substring(3, 4));
	    return (parseInt(num.substring(12,13))==prty);
	}



//---------------------------------------------------------------------------------
// 함수 설명 :	 Email Check 스크립트
//---------------------------------------------------------------------------------
// 인자 설명 :
//---------------------------------------------------------------------------------
// 사 용 법  :  return emailCheck(this) 형태로 쓰시면 됩니다.(Email 오류시 포커스 이동)
//							콤마(',')를 구분으로 여러개의 메일주소를 입력할 때는 emailCheck(this, ',') 형태로 쓰시면 됩니다.
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
	function emailCheck(_email, _delim) {
	    var email = _email.value;
	    var aidx = -1; // @ 위치
	    var didx = -1; // . 위치
	    var valid = true;
	    var arrSpChSet= new Array(" ", "\"", "'", "#", "%"); // 체크할 특수문제 셋

	    // 입력정보가 없을 경우
	    if(_email==null || email.length < 1) { return true; }
	    if(_delim==null || _delim.length < 1) _delim = " ";

	    var emails = email.split(_delim);

	    for(var i=0; i < emails.length; i++) {
	        email = emails[i];


			if (/^[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+)*@[0-9a-zA-Z-]+(\.)+([0-9a-zA-Z-]+)([\.0-9a-zA-Z-])*$/.test(email) == false){
	            valid = false; break;
			}
	    }

	    // 주소가 형식에 맞지 않을 때
	    if(!valid) {
	        alert("E-mail ( " + email + " )주소가 정확하지 않습니다!");
	        _email.focus();
	        valid = false;
	    }

	    return valid;
	}

//---------------------------------------------------------------------------------
// 함수 설명 :	아이프레임 높이 자동으로 조절1
//---------------------------------------------------------------------------------
// 인자 설명 :
//---------------------------------------------------------------------------------
// 사 용 법  :  아이프레임에 onload="resizeIFrame(프레임명)"
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------

	function resizeIFrame(sFrameName) {
	    var isIE     = (navigator.userAgent.toLowerCase().indexOf("msie")!=-1 && window.document.all) ? true:false;
	    var objBody  = window.frames[sFrameName].document.body;
	    var objFrame = document.getElementById(sFrameName);
	    ifrmHeight = objBody.scrollHeight + (objBody.offsetHeight - objBody.clientHeight);
	    objFrame.style.height = ifrmHeight+10;
	    if (isIE==false)
	    {
	        var iframeElement = parent.document.getElementById(sFrameName);
	        iframeElement.height =  parent.frames['dataFrame'].document.getElementById('offsettop_layer').offsetTop + 10;
	    }
	}

//---------------------------------------------------------------------------------
// 함수 설명 :	아이프레임 높이 자동으로 조절2
//---------------------------------------------------------------------------------
// 인자 설명 :
//---------------------------------------------------------------------------------
// 사 용 법  :
//<script type="text/javascript">
//	window.setInterval("iframeReSize('프레임아이디')",50);
//</script>
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
	function iframeReSize(ifId){
		var obj = document.getElementById(ifId)
		try{
			if(obj){
				obj.height = obj.contentWindow.document.body.scrollHeight
			}
		}catch(E){}
	}

//---------------------------------------------------------------------------------
// 함수 설명 :	주민번호 앞자리 입력후 뒷자리 입력란으로 포커스 이동
//---------------------------------------------------------------------------------
// 인자 설명 :
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
function checkLength(source, target, pos)
	{
		var value;

		if(typeof(source.length) != 'undefined')
			value = source[pos].value;
		else
			value = source.value;

		if(value != 'undefined')
		{
			if(window.event.keyCode >= 48 || window.event.keyCode <= 57)
			{
				if(value.length == 6)
				{
					if(typeof(source.length) != 'undefined')
						target[pos].focus();
					else
						target.focus();
				}
			}
		}
	}

//---------------------------------------------------------------------------------
// 함수 설명 : 앞자리 입력후 뒷자리 입력란으로 포커스 이동
//---------------------------------------------------------------------------------
// 인자 설명 :
//---------------------------------------------------------------------------------
// 반 환 값  :
//---------------------------------------------------------------------------------
function checkLength2(source, target, pos, len)
	{
		var value;

		if(typeof(source.length) != 'undefined')
			value = source[pos].value;
		else
			value = source.value;

		if(value != 'undefined')
		{
			if(window.event.keyCode >= 48 || window.event.keyCode <= 57)
			{
				if(value.length == len)
				{
					if(typeof(source.length) != 'undefined')
						target[pos].focus();
					else
						target.focus();
				}
			}
		}
	}


	// 체크 박스 선택 유무 검사

	function inspectCheckBoxField(checkForm)
	{
		var result = false;

		if(typeof(checkForm) != 'undefined')
		{
			if(checkForm.length)
			{
				for(i=0; i<checkForm.length; i++)
				{
					if(checkForm[i].checked == true)
					{
						result = true;
						break;
					}
				}
			}
			else
			{
				if(checkForm.checked == true)
					result = true;
			}
		}

		return result;
	}


function fn_numKeyChk(){

    //숫자만 입력가능하게
    event.returnValue=false;
    if (event.keyCode == 8) event.returnValue=true;             // 백스페이스일 경우 허가
    if (event.keyCode == 9) event.returnValue=true;             //	탭허용
    if(event.keyCode >= 37 && event.keyCode <= 40) event.returnValue=true;    //방향키허용
    if(event.keyCode >= 48 && event.keyCode <= 57) event.returnValue=true;    //숫자허용
    if(event.keyCode >= 96 && event.keyCode <= 105) event.returnValue=true;

}

/*
*  서버에서 true, false 리턴
*/
function cfGetBooleanResponseReload(url,params,HttpMethod) {
	var xmlhttp = null;
	if(!HttpMethod){
	    HttpMethod = "GET";
	}
    if(window.ActiveXObject){
	      try {
	    	  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	      } catch (e) {
	        try {
	        	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP") ;
	        } catch (e2) {
	          return null ;
	        }
		}
	}
	//xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");

	if (xmlhttp == null) return true;

	xmlhttp.open(HttpMethod, url, false);
	xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');

	xmlhttp.send(params);

	if (xmlhttp.responseText == "true") {
		return true;
	} else {
		return false;
	}
}

function cfInputNumRT(e){

	if(window.event){    //IE
       	e = window.event;
       	var lkeycode = e.keyCode;  
    }else{				//W3C
    	var lkeycode = e.which; 
    }
  
    if(e.type == "keypress"){
	    if( !((48 <= lkeycode && lkeycode <=57) || lkeycode == 13 || lkeycode == 8 || lkeycode == 0) ){
			if( window.event ){
	    		e.returnValue = false;
	    	}else{
	        	e.preventDefault();
	    	}    	
	    }     
    }else{
	    if( !((48 <= lkeycode && lkeycode <=57) || (96 <= lkeycode && lkeycode <=105) || lkeycode == 13 || lkeycode == 8 || lkeycode == 46 || lkeycode == 229) ){						
			var charVal = String.fromCharCode(lkeycode);
			if(charVal != null && charVal != ""){
				var eVal = e.srcElement.value;				
				if(eVal.length > 0) e.srcElement.value = eVal = eVal.replace(charVal, "");
			}
	    }  
    }
}