<%@ page contentType="text/html;charset=euc-kr"%>
<%@ page import="com.dreamsecurity.jcaos.exception.IdentifyException"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse" %>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletRequest" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.dreamsecurity.jcaos.x509.X509Certificate" %>
<%@ page import="com.dreamsecurity.magicline.MessageConstants" %>
<%@ page import="com.dreamsecurity.magicline.config.Logger" %>
<%
	DSHttpServletResponse dRes = null;
	DSHttpServletRequest dReq = null;

	X509Certificate cert = null;
	byte[] privatekeyRandom = null;
	String signType = "";
	String subDN = "";
	java.math.BigInteger serialNumber = null;
	int messageType;

	try {
		dRes = new DSHttpServletResponse(response);
		dReq = new DSHttpServletRequest(request);
		dRes.setRequest(dReq);

		//Infomation
		cert = dReq.getSignerCert();
		subDN = cert.getSubjectDN().getName();
		serialNumber = cert.getSerialNumber();
		messageType = dReq.getRequestMessageType();
		privatekeyRandom = dReq.getSignerRValue();
		signType = dReq.getSignType();

		//주민번호 본인확인 기능을 사용할 경우
		String ssn = dReq.getParameter("ssn").trim();
		if(null != ssn && !ssn.equals("")){
			cert.verifyVID(ssn,privatekeyRandom);
		}
	}catch(IdentifyException e){
		// 본인확인 Error
		StringBuffer sb = new StringBuffer(1500);
		sb.append(e.getMessage());
		session.setAttribute("magiclineErr", "[본인확인실패]"+sb.toString());
		response.sendRedirect("magicline_err.jsp");
		return;

	} catch (Exception e) {
		//인증서 검증
		//정책 검증
		StringBuffer sb = new StringBuffer(1500);
		sb.append(e.getMessage());
		session.setAttribute("magiclineErr",sb.toString());
		response.sendRedirect("magicline_err.jsp");
		return;
	}
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>공인인증서로그인 | 저작권찾기</title>
<script type="text/javascript">
<!--
function init() {
	var frm = document.form0;
	
    frm.action = "/userLogin/userLogin.do";
	frm.submit();

  }

//-->
</script>
</head>

<body onLoad="init();">


<form name="form0" method="post" class="frm" action="">
	<input type="text" name="certDN" value="<%=subDN%>">
   	<input type="hidden" name="adminLogin" value="Y" />
   	<input type="hidden" name="adminType" value="certLogin" />
</form>
</body>
</html>				