<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse"%>
<%

	String challenge = "";
	try{
	DSHttpServletResponse res = new DSHttpServletResponse(response);
	res.setRequest(request);
	challenge = res.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
		return;
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/login.css"/>
<title>::: LOGIN :::</title>
<!-- MagicLine Javascript include --> 
<script language="javascript" src="../../MagicLine4/js/deployJava.js"></script>
<script language="javascript" src="../../MagicLine4/js/PluginDetect.js"></script>
<script language="javascript" src="../../MagicLine4/js/MagicLine.js"></script>
<script language='javascript'>

	

	// MagicLine Client Module 구동.
	runMagicLine();
	// MagicLine Client 인증서 창을 호출한다. 
	function doAction(loginForm){
		if (MagicLine_install == true){
			// MagicLine.js 에 있는 Login 공통함수를 호출한다. 
			Login(loginForm);
		}
	}
</script>
</head>

<body>
<!-- Wrap -->
<div id="login_Wrap">
 <div id="login_logo"><a href="#"><img src="images/logo_copy.gif" /></a></div>
 <!-- login_box -->
 <div id="login_bg01">
  <div class="reg_box">
    <h1>다음 사용자의 인증서를 등록합니다.</h1>
   <dl>
   <!-- //임시 세션 이름은 m_user_id, m_user_name  -->
    <dd><span class="reg_text">성명 : <%= session.getAttribute("m_user_name") %></span></dd>
    <dd><span class="reg_text">ID : <%= session.getAttribute("m_user_id") %></span></dd>
   </dl>
  </div>
  <div class="reg_box_btn">
  <form action="login_renewR2.jsp" method="post" name="loginForm">
  
	<input type="hidden" id="challenge" name="challenge" value="<%=challenge%>"/> <!-- 생성한 challenge 값을 담는다 --> 
	<input type="hidden" id="ssn" name="ssn" value=""/> <!-- 본인확인이 필요할 경우 주민번호를 담는다. --> 
	<input type="hidden" id="etc" name="etc" value=""/>  <!-- 업무에 필요한 기타 데이터를 담는다. --> 
	<input type="hidden" name="sessionIDT" value="" />
	<input type="hidden" name="pText" value="" />
	<input type="hidden" name="eText" value="" />
	
   <a href="#"><img src="images/btn_cert_reg_ok.gif" onclick="doAction(loginForm);return false;"></a>
   <a href="login.jsp"><img src="images/btn_cancel.gif"></a>
   </form>
  </div>
 </div>
 <!--// login_box -->
 <div class="footer_copy">135-240 서울특별시 강남구 개포로 619(개포동) 6,7층 </div>
</div>
<!--// Wrap -->
</body>
</html>
