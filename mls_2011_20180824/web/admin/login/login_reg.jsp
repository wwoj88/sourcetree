<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	String errorCode = request.getParameter("errorCode");
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/login.css"/>
<title>::: LOGIN :::</title>
<script language="javascript">

if('<%=errorCode%>'=='201') {
	alert('이미 탈퇴한 회원 입니다.');
}else if('<%=errorCode%>'=='202') {
	alert('회원정보가 없습니다.');
}else if('<%=errorCode%>'=='203') {
	alert('비밀번호가 일치하지 않습니다.');
}else if('<%=errorCode%>'=='204') {
	alert('시스템관리자만 이용가능합니다.');
}

function adminLogin(){
	var join_id, join_pin;
	join_id = document.loginForm.userIdnt.value;
	join_pin = document.loginForm.pswd.value;
	
	if (join_id == "") {
		alert("아이디를 입력하세요"); 
		document.loginForm.userIdnt.focus();
		return false;}
	if (join_pin == "") {
		alert("패스워드를 입력하세요"); 
		document.loginForm.pswd.focus();
		return false;
	}
	
	form = document.loginForm;
	form.submit();

}



function init(){
document.loginForm.userIdnt.focus();
}
function enter(){
	if(event.keyCode == 13){
			adminLogin();
	}
}
//document.onkeydown = enter;
</script>
</head>

<body onLoad="init();">
<!-- Wrap -->
<div id="login_Wrap">
 <div id="login_logo"><a href="#"><img src="images/logo.gif" /></a></div>
 <!-- login_box -->
 <div id="login_bg01">
  <div class="login_box">
    <h1>인증서 등록을 위한 로그인 입니다.</h1>
   <form action="/userLogin/userLogin.do" method="post" name="loginForm">
   	<input type="hidden" name="adminLogin" value="Y" />
   	<input type="hidden" name="adminType" value="adminCheck" />
   	
   <dl>
    <dd><span class="login_text">ID :</span> <input type="text" name="userIdnt" class="itf" value="" /></dd>
    <dd><span class="login_text">password :</span> <input type="password" name="pswd" class="itf" value="" /></dd>
   </dl>
   <div class="login_box_btn"><input name="" type="image" src="images/btn_login.gif" class="isb" onClick="adminLogin(); return false;" /></div>
   </form>
  </div>
 </div>
 <!--// login_box -->
 <div class="footer_copy">주소: (110-360) 서울시 종로구 창경궁로 215 &nbsp;&nbsp; 대표전화 : 02-3704-9114 &nbsp;&nbsp; 팩스 : 02-3704-9154
 <br />COPYRIGHTⓒMINISTRY OF CULTURE, SPORTS &amp; TOURISM.</div>
</div>
<!--// Wrap -->
</body>
</html>
