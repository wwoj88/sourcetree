<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.dreamsecurity.magicline.servlet.DSHttpServletResponse"%>
<%
	String errorCode = request.getParameter("errorCode");
	String returnError = "";
	 if(errorCode !=null ) returnError = errorCode.substring(0,1);

	String challenge = "";
	try{
	DSHttpServletResponse res = new DSHttpServletResponse(response);
	res.setRequest(request);
	challenge = res.getChallenge();
	}catch(Exception e){
		out.println(e.getMessage());
		return;
	}
%>
<%
	//세션에 따라 인증서 등록 url을 다르게 설정
	String login_reg_url = "login_reg.jsp";
	//임시 세션 이름은 m_user_id, m_user_name 
	if(session.getAttribute("m_user_id") != null && session.getAttribute("m_user_id").toString().length() > 0) {
		login_reg_url = "login_reg_conf.jsp";
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/login.css"/>
<title>::: LOGIN :::</title>
<script language="javascript">


function show_leemocon(tab){
	var d1 = document.getElementById("tab1");
	var d2 = document.getElementById("tab2");
	d1.style.display = "none";
	d2.style.display = "none";
	
	switch(tab){
		case 1:
		 d1.style.display = "";
		 break;
		case 2:
		 d2.style.display = "";
		 break;
	}
}
</script>
<!-- MagicLine Javascript include --> 
<script language="javascript" src="../../MagicLine4/js/deployJava.js"></script>
<script language="javascript" src="../../MagicLine4/js/PluginDetect.js"></script>
<script language="javascript" src="../../MagicLine4/js/MagicLine.js"></script>
<script language='javascript'>
	if('<%=errorCode%>'=='001') {
		alert('등록된 인증서가 없습니다.');
	}
	else if('<%=errorCode%>'=='002') {
		alert('인증서가 등록되었습니다.');
	}
	else if('<%=errorCode%>'=='201') {
		alert('이미 탈퇴한 회원 입니다.');
	}else if('<%=errorCode%>'=='202') {
		alert('회원정보가 없습니다.');
	}else if('<%=errorCode%>'=='203') {
		alert('비밀번호가 일치하지 않습니다.');
	}else if('<%=errorCode%>'=='205') {
		alert('위탁관리단체 담당자만 이용가능합니다.\n시스템관리자는 공인인증서 로그인을 이용해주세요.');
	}

	// MagicLine Client Module 구동.
	runMagicLine();
	// MagicLine Client 인증서 창을 호출한다. 
	function doAction(loginForm){
		if (MagicLine_install == true){
			// MagicLine.js 에 있는 Login 공통함수를 호출한다. 
			Login(loginForm);
		}
	}
</script>
</head>

<body>
<!-- Wrap -->
<div id="login_Wrap">
 <div id="login_logo">
  <div class="top_btn_left"><a href="#"><img src="images/Logo.gif" /></a></div>
  <div class="top_btn_right"><a href="/mi/main.jsp" target="_blank"><img src="images/btn_member_join.gif" /></a></div>
 </div>
 <!-- cert login_box -->
 <div id="tab1" class="login_bg" style="display:">
   <!--// Tab Menu 추가 -->
  <div class="Tab_Menu">
   <ul class="Tab_Menu_list">
    <li class="sele">위원회로그인</li>
    <li><a onclick="show_leemocon(2);" style="cursor:hand;">일반관리자 로그인</a></li>
   </ul>
  </div>
   <!--// Tab Menu 끝 -->
  <div class="cert_box">
   <h1>시스템 관리자는 개인 인증서를 이용하여 로그인하실 수 있습니다.</h1>
   <form action="login_renewR.jsp" method="post" name="loginForm">
	<input type="hidden" id="challenge" name="challenge" value="<%=challenge%>"/> <!-- 생성한 challenge 값을 담는다 --> 
	<input type="hidden" id="ssn" name="ssn" value=""/> <!-- 본인확인이 필요할 경우 주민번호를 담는다. --> 
	<input type="hidden" id="etc" name="etc" value=""/>  <!-- 업무에 필요한 기타 데이터를 담는다. --> 
	<input type="hidden" name="sessionIDT" value="" />
	<input type="hidden" name="pText" value="" />
	<input type="hidden" name="eText" value="" />
	
   <dl>
    <dd><a href="#"><img src="images/btn_cert_login.gif" onclick="doAction(loginForm);return false;"></a></dd>
    <dd><a href="<%=login_reg_url%>"><img src="images/btn_cert_reg.gif"></a></dd>
   </dl>
   </form>
  </div>
  <div class="install_box">
  <h1><img src="images/tit_01.gif" /></h1>
  <dl>
    <dd><a href="../../MagicLine4/lib/MagicLineMBXSetup.exe"><img src="images/btn_install32.gif"></a></dd>
    <dd><a href="../../MagicLine4/lib/MagicLineMBXSetup64.exe"><img src="images/btn_install64.gif"></a></dd>
   </dl>
  </div>
 </div>
 <!--// cert login_box -->
 
  <!-- ipin login_box -->
 <div id="tab2" class="login_bg" style="display:none;">
   <!--// Tab Menu 추가 -->
  <div class="Tab_Menu">
   <ul class="Tab_Menu_list">
    <li><a onclick="show_leemocon(1);" style="cursor:hand;">위원회로그인</a></li><li class="sele">일반관리자 로그인</li>
   </ul>
  </div>
   <!--// Tab Menu 끝 -->
  <div class="login_box">
    <h1>일반관리자만 이용이 가능합니다.</h1>
   <form action="/userLogin/userLogin.do" method="post" name="loginForm2">
   	<input type="hidden" name="adminLogin" value="Y" />
   	<input type="hidden" name="adminType" value="idLogin" />
   <dl>
    <dd><span class="login_text">ID :</span> <input type="text" name="userIdnt" class="itf" value="" /></dd>
    <dd><span class="login_text">password :</span> <input type="password" autocomplete="false" name="pswd" class="itf" value="" /></dd>
   </dl>
   <div class="login_box_btn"><input name="" type="image" src="images/btn_login.gif" class="isb" /></div>
   </form>
  </div>
 </div>
 <!--// ipin login_box -->
 <div class="footer_copy">135-240 서울특별시 강남구 개포로 619(개포동) 6,7층 </div>
</div>
<!--// Wrap -->
</body>
<script language="javascript">

//alert('<%=returnError%>');

if('<%=returnError%>'=='2') {
	show_leemocon(2);
}
</script>
</html>

