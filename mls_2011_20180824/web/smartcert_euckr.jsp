<%@ page language="java" contentType="text/html;charset=euc-kr"%>
<%@ page language="java" import="com.cabsoft.utils.StringUtils"%>
<%@ page language="java" import="com.cabsoft.utils.SystemUtils"%>
<%@ page language="java" import="com.cabsoft.smartcert.SCertSession"%>
<%@ page language="java" import="com.cabsoft.smartcert.session.SmartCertSession"%>
<%@ page language="java" import="com.cabsoft.smartcert.request.SmartCertRequest"%>
<%@ page language="java" import="java.util.HashMap"%>
<%@ page errorPage="../error/error.jsp" %>
<%@ include file="../beans/ExceptionUrl.jsp"%>
<% 
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
	 
	request.setCharacterEncoding("euc-kr");
	
	com.cabsoft.EForminfo eforminfo = com.cabsoft.EForminfo.getInstance();

	String errUrl = "../error/error.jsp?msg=";
	if (!request.isRequestedSessionIdValid()) {
		response.sendRedirect(errUrl + java.net.URLEncoder.encode("서버와의 연결(세션)이 종료되었습니다. ", "euc-kr"));
	} else {
		// 이전 jobID 제거
		String jobID = (String) session.getAttribute("jobID");
		if (!StringUtils.isEmpty(jobID)) {
			session.setAttribute(jobID + "_session", null);
			session.removeAttribute(jobID + "_session");
			session.setAttribute("jobID", null);
			session.removeAttribute("jobID");
		}

		SmartCertRequest reqs = new SmartCertRequest();
		HashMap<String, String> paramMap = reqs.getParaterMaps(request);
		
		//jobid생성
		jobID = SystemUtils.GenerateID();
		session.setAttribute("jobID", jobID);

		String servlet = paramMap.get("Servlet");

		// html 뷰어로 전달
		String filename = paramMap.get("ReportFile");
		String process = paramMap.get("Process");
		String rptTitle = paramMap.get("rptTitle");
		String toolbarType = paramMap.get("toolbarType");
		
		String data = paramMap.get("data");
		String dataType = paramMap.get("type");
		
		String lang = (String) paramMap.get("lang");
		lang = lang==null || "".equals(lang) ? "ko" : lang;
		
		String title1 = "생성 중 ...";
		String title2 = "증명서 생성 중 ....";
		if(!"ko".equals(lang)){
			title1 = "Creating ...";
			title2 = "Report Creating ....";
		}
		
		ExceptionUrl exceptionUrl = new ExceptionUrl();
		
		// 설정값 세션에 저장
		SmartCertSession smartcertSession = new SmartCertSession(request);
		
		eforminfo.setMapCount("[R]"+filename);
		
		// 보고서 설정값 설정
		SCertSession ss = new SCertSession();
		ss.setJobID(jobID);
		ss.setRxprintData("");
		ss.setIssueID("");
		ss.setRptFile(filename);
		ss.setData(data);
		ss.setDataType(dataType);
		ss.setPdfData(null);
		ss.setUserpwd("");
		smartcertSession.setSCertSession(ss, exceptionUrl.getExceptionUrl(request));
		
%>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8" Cache-control="no-cache" Pragma="no-cache">
<meta name="viewport" content="user-scalable=yes; initial-scale=0.5; minimum-scale=0.5; maximum-scale-1.0;"/>
<link href="../../viewers/fonts/fonts.css" rel="stylesheet" type="text/css" />
<link href="css/jquery.loadmask.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-latest.pack.js"></script>
<script type='text/javascript' src='js/jquery.loadmask.js'></script>
<style>
body {
	font-size: 11px;
	font-family: tahoma;
}

#content {
	padding: 5px;
	width: 200px;
}

#buttons {
	padding-left: 40px;
}
</style>
<script language="javascript">
	var lint;
	var des = "";
	var status;
	var process  ="";
	var monitor = "../monitor/smartcert_monitor.jsp?jobID=<%=jobID%>";

	$(document).ready(function() {
		$("#body").mask("<span id='proc' name = 'proc' style='font-family: Icon-Fonts;'>&#xe61c;</span><span id='msg' name='msg' [<%=rptTitle%>] 증명서를 발급 중입니다.");
		lint = setInterval(checkStatus,100);
	} );
	
		function checkStatus(){
			$.getJSON(monitor,function(data) {
				process  = data.process;
				console.log(process);
				if(process=="fill"){
					status = data.status;
					if(status==-1){
						des = "시작";
					}else if(status==-2){
						des = "종료";
						//clearInterval(lint);
					}else{
						des = status + " 페이지 생성 중 ...";
					}
				}else if(process=="html"){
					des = "HTML 미리보기 생성 중 ...";
				}else if(process=="pdf"){
					des = "PDF 생성 중 ...";
				}else if(process=="sign"){
					des = "PDF에 전자서명 중 ...";
				}


				var msg = " [<%=rptTitle%>] 증명서를 발급 중입니다.";
				if(status!=-600){
					msg = " [" + data.docName + "] " + des;
				}
				var o = $("#msg")[0];
				o.innerHTML = msg;
			});
		}
		
	function makeForm(url){
		var f = document.createElement("form");
	    f.setAttribute("method", "post");
	    f.setAttribute("action", url);
	    document.body.appendChild(f);
	      
	    return f;
	}
	
	function addData(name, value){
		var i = document.createElement("input");
		i.setAttribute("type","hidden");
		i.setAttribute("name",name);
		i.setAttribute("value",value);
		return i;
	}
	
	window.onload = function(){
		f = makeForm('../service/<%=servlet%>');
		f.appendChild(addData('ReportFile', '<%=filename%>'));
		f.appendChild(addData('Process', '<%=process%>'));
		f.appendChild(addData('rptTitle', '<%=rptTitle%>'));
		f.appendChild(addData('lang', '<%=lang%>'));
		f.submit();
	}
</script>

</head>
<body id="body" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">

</body>
</html>
<%
	}
%>

