/**
 * Copyright 2005 Darren L. Spurgeon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.tags;

import javax.servlet.jsp.JspException;

/**
 * Tag handler for the HTML Content AJAX tag.
 * 
 * @author Darren Spurgeon
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:25 $ $Author: yagin14 $
 */
public class AjaxHtmlContentTag extends BaseAjaxBodyTag {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;


    @Override
    protected boolean skipBody() {
	return true;
    }

    @Override
    public int doEndTag() throws JspException {
	OptionsBuilder options = getOptionsBuilder();
	StringBuffer script = new StringBuffer();
	script.append("<script type=\"text/javascript\">\n");

	script.append(getJSVariable());

	script.append("new AjaxJspTag.HtmlContent(").append("{\n").append(
		options.toString())
		.append("});\n").append("</script>\n\n");

	out(script);
	return EVAL_PAGE;
    }

    @Override
    public void releaseTag() {
	// XXX missing
    }

}
