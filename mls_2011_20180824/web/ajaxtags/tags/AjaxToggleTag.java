/**
 * Copyright 2005 Darren L. Spurgeon
 * Copyright 2007-2008 Jens Kapitza
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.tags;

import javax.servlet.jsp.JspException;

/**
 * Tag handler for the toggle (on/off, true/false) AJAX tag.
 * 
 * @author Darren Spurgeon
 * @author Jens Kapitza
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:26 $ $Author: yagin14 $
 */
public class AjaxToggleTag extends BaseAjaxBodyTag {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
 

    private String ratings;

    private String defaultRating;

    private String state;

    private String onOff;

    private String containerClass;

    private String messageClass;

    private String selectedClass;

    private String selectedOverClass;

    private String selectedLessClass;

    private String overClass;

    private String updateFunction;

    public String getUpdateFunction() {
	return updateFunction;
    }

    public void setUpdateFunction(String updateFunction) {
	this.updateFunction = updateFunction;
    }

    public String getContainerClass() {
	return containerClass;
    }

    public void setContainerClass(String containerClass) {
	this.containerClass = containerClass;
    }

    public String getDefaultRating() {
	return defaultRating;
    }

    public void setDefaultRating(String defaultRating) {
	this.defaultRating = defaultRating;
    }

    public String getMessageClass() {
	return messageClass;
    }

    public void setMessageClass(String messageClass) {
	this.messageClass = messageClass;
    }

    public String getOnOff() {
	return onOff;
    }

    public void setOnOff(String onOff) {
	this.onOff = onOff;
    }

    public String getOverClass() {
	return overClass;
    }

    public void setOverClass(String overClass) {
	this.overClass = overClass;
    }
 

    public String getRatings() {
	return ratings;
    }

    public void setRatings(String ratings) {
	this.ratings = ratings;
    }

    public String getSelectedClass() {
	return selectedClass;
    }

    public void setSelectedClass(String selectedClass) {
	this.selectedClass = selectedClass;
    }

    public String getSelectedLessClass() {
	return selectedLessClass;
    }

    public void setSelectedLessClass(String selectedLessClass) {
	this.selectedLessClass = selectedLessClass;
    }

    public String getSelectedOverClass() {
	return selectedOverClass;
    }

    public void setSelectedOverClass(String selectedOverClass) {
	this.selectedOverClass = selectedOverClass;
    }

    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    } 

    @Override
    protected boolean skipBody() {
	return true;
    }

    @Override
    public int doEndTag() throws JspException {
	OptionsBuilder options = getOptionsBuilder();
	options.add("ratings", this.ratings,
		true).add("containerClass", this.containerClass, true).add(
		"selectedClass", this.selectedClass, true).add(
		"selectedOverClass", this.selectedOverClass, true).add(
		"selectedLessClass", this.selectedLessClass, true).add(
		"overClass", this.overClass, true);

	options.add("messageClass", this.messageClass, true);
	options.add("state", this.state, true);
	options.add("onOff", this.onOff, true);
	options.add("defaultRating", this.defaultRating, true);
	options.add("updateFunction", this.updateFunction, false);

	StringBuffer script = new StringBuffer();
	// write opening div
	script.append("<div id=\"").append(getSource()).append("\" class=\"")
		.append(this.containerClass);
	boolean xOnOff = Boolean.parseBoolean(this.onOff);
	if (xOnOff) {
	    script.append(" onoff");
	}
	script.append("\">");
	// write links
	if (xOnOff) {
		script.append("<a href=\"").append(AJAX_VOID_URL).append("\" title=\"");
	    if (defaultRating != null
		    && defaultRating.length() != 0
		    && ratings != null
		    && ratings.length() != 0
		    && this.defaultRating.equalsIgnoreCase(this.ratings
			    .split(",")[0])) {
		script.append(this.ratings.split(",")[0]).append(
				"\" class=\"").append(this.selectedClass)
			.append("\"></a>");
	    } else {
		script.append(this.ratings.split(",")[1]).append("\"></a>");
	    }
	} else {
	    boolean ratingMatch = false;
	    String[] ratingValues = this.ratings.split(",");
	    for (int i = 0; i < ratingValues.length; i++) {
		String val = ratingValues[i];
		    script.append("<a href=\"").append(AJAX_VOID_URL).append("\" title=\"");
		
		if (defaultRating == null || defaultRating.trim().length() == 0
			|| ratingMatch) {
			    script.append(val).append("\"></a>");
		} else if ( ! ratingMatch || this.defaultRating.equalsIgnoreCase(val)) {
		    script.append(val).append("\" class=\"").append(
				    this.selectedClass).append("\"></a>");
		    if (this.defaultRating.equalsIgnoreCase(val))
			ratingMatch = true;
		}
	    }
	}
	script.append("<br/></div>\n");

	// write script
	script.append("<script type=\"text/javascript\">\n");

	script.append(getJSVariable());

	script.append("new AjaxJspTag.Toggle( {\n").append(options.toString())
		.append("});\n").append("</script>\n\n");
	out(script);
	return EVAL_PAGE;
    }

    @Override
    public void releaseTag() {
	this.ratings = null;
	this.defaultRating = null;
	this.state = null;
	this.onOff = null;
	this.containerClass = null;
	this.messageClass = null;
	this.selectedClass = null;
	this.selectedOverClass = null;
	this.selectedLessClass = null;
	this.overClass = null;
    }
}
