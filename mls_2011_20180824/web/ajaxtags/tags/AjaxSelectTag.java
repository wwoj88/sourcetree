/**
 * Copyright 2005 Darren L. Spurgeon
 * Copyright 2007 Jens Kapitza
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.tags;

import javax.servlet.jsp.JspException;

/**
 * Tag handler for the select AJAX tag.
 * 
 * @author Darren Spurgeon
 * @author Jens Kapitza
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:23 $ $Author: yagin14 $
 * 
 */
public class AjaxSelectTag extends BaseAjaxBodyTag {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
 
    private String emptyOptionValue;
    private String emptyOptionName;

    public void setEmptyOptionName(String emptyOptionName) {
	this.emptyOptionName = emptyOptionName;
    }

    public String getEmptyOptionName() {
	return emptyOptionName;
    }

    public void setEmptyOptionValue(String emptyOptionValue) {
	this.emptyOptionValue = emptyOptionValue;
    }

    public String getEmptyOptionValue() {
	return emptyOptionValue;
    }

    private String executeOnLoad;

    private String defaultOptions;
 
  
    public String getExecuteOnLoad() {
	return executeOnLoad;
    }

    public void setExecuteOnLoad(String executeOnLoad) {
	this.executeOnLoad = executeOnLoad;
    }

    public String getDefaultOptions() {
	return defaultOptions;
    }

    public void setDefaultOptions(String defaultOptions) {
	this.defaultOptions = defaultOptions;
    }

    @Override
    protected boolean skipBody() {
	return true;
    }

    @Override
    public int doEndTag() throws JspException {
	OptionsBuilder options = getOptionsBuilder();
	options.add("executeOnLoad", this.executeOnLoad, true);
	options.add("defaultOptions", this.defaultOptions, true);
	options.add("emptyOptionValue", emptyOptionValue, true);
	options.add("emptyOptionName", emptyOptionName, true);

	StringBuffer script = new StringBuffer();
	script.append("<script type=\"text/javascript\">\n");
	script.append(getJSVariable());
	script.append("new AjaxJspTag.Select(\n");
	script.append("{\n");
	script.append(options.toString());
	script.append("});\n");
	script.append("</script>\n\n");

	out(script);
	return EVAL_PAGE;
    }

    @Override
    public void releaseTag() {
	this.executeOnLoad = null;
	this.defaultOptions = null;
    }

}
