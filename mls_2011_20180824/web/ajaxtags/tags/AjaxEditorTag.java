/**
 * Copyright 2005 Darren L. Spurgeon
 * Copyright 2007-2008 Jens Kapitza
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.tags;

import javax.servlet.jsp.JspException;

/**
 * Wraps the scriptaculous' in-place editor
 * (http://github.com/madrobby/scriptaculous/wikis/ajax-inplaceeditor)
 * 
 * @author Musachy Barroso
 * @author Jens Kapitza
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:23 $ $Author: yagin14 $
 */
public class AjaxEditorTag extends BaseAjaxBodyTag {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String showAcceptButton;

    private String acceptText;

    private String showCancelLink;

    private String cancelText;

    private String savingText;

    private String mouseOverText;

    private String formId;

    private String rows;

    private String columns;

    private String highlightColor;
    
    @Override
    protected void releaseTag() {
	showAcceptButton = null;
	acceptText = null;
	showCancelLink = null;
	cancelText = null;
	savingText = null;
	mouseOverText = null;
	formId = null;
	rows = null;
	columns = null;
	highlightColor = null;
	
    }

    @Override
    protected boolean skipBody() {
	return true;
    }

    @Override
    public int doEndTag() throws JspException {
	OptionsBuilder options = getOptionsBuilder();

	options.add("okControl", this.showAcceptButton, false);
	options.add("okText", this.acceptText, true);
	options.add("cancelControl", this.showCancelLink, false);
	options.add("cancelText", this.cancelText, true);
	options.add("savingText", this.savingText, true);
	options.add("clickToEditText", this.mouseOverText, true);

	// -- is wrong!
	// options.add("callback", getPreFunction(), false);

	// externalControl, externalControlOnly, submitOnBlur, ajaxOptions

	options.add("formId", this.formId, true);
	options.add("rows", this.rows, true);
	options.add("cols", this.columns, true);
	options.add("highlightcolor", this.highlightColor, true);

	StringBuffer script = new StringBuffer();
	script.append("<script type=\"text/javascript\">\n");
	script.append("var $editor_").append(getTarget());
	script.append(" = new Ajax.InPlaceEditor(\"");
	script.append(getTarget());
	script.append("\", \"");
	script.append(getBaseUrl());
	script.append("\", {\n");
	script.append(options.toString());
	script.append("});\n");
	script.append("</script>\n\n");

	out(script);
	return EVAL_PAGE;
    }

    public String getAcceptText() {
	return acceptText;
    }

    public void setAcceptText(String acceptText) {
	this.acceptText = acceptText;
    }

    public String getCancelText() {
	return cancelText;
    }

    public void setCancelText(String cancelText) {
	this.cancelText = cancelText;
    }

    public String getColumns() {
	return columns;
    }

    public void setColumns(String columns) {
	this.columns = columns;
    }

    public String getHighlightColor() {
	return highlightColor;
    }

    public void setHighlightColor(String highlightColor) {
	this.highlightColor = highlightColor;
    }

    public String getMouseOverText() {
	return mouseOverText;
    }

    public void setMouseOverText(String mouseOverText) {
	this.mouseOverText = mouseOverText;
    }

    public String getRows() {
	return rows;
    }

    public void setRows(String rows) {
	this.rows = rows;
    }

    public String getSavingText() {
	return savingText;
    }

    public void setSavingText(String savingText) {
	this.savingText = savingText;
    }

    public String getShowAcceptButton() {
	return showAcceptButton;
    }

    public void setShowAcceptButton(String showAcceptButton) {
	this.showAcceptButton = showAcceptButton;
    }

    public String getShowCancelLink() {
	return showCancelLink;
    }

    public void setShowCancelLink(String showCancelLink) {
	this.showCancelLink = showCancelLink;
    }

    public String getFormId() {
	return formId;
    }

    public void setFormId(String formId) {
	this.formId = formId;
    }
}
