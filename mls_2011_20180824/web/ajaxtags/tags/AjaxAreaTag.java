/**
 * Copyright 2005 Darren L. Spurgeon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.tags;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

/**
 * Wraps any area on the page (with a DIV element) so that actions within that
 * area refresh/load inside the defined DIV region rather than inside the whole
 * browser window.
 * 
 * @author Darren Spurgeon
 * @author Jens Kapitza
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:25 $ $Author: yagin14 $
 */
public class AjaxAreaTag extends BaseAjaxBodyTag {

    public final static String TARGET_HEADER = "x-request-target";

    @Override
    public boolean isAjaxRequest() {
	// this is only a ajaxrequest if the target is right!
	HttpServletRequest req = (HttpServletRequest) pageContext.getRequest();
	String id = getId();
	return super.isAjaxRequest()
		&& id.equalsIgnoreCase(req.getHeader(TARGET_HEADER));
    }

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7940387487602588115L;

    private String styleClass;

    private String ajaxAnchors = null;


    /**
     * @return Returns the styleClass.
     */
    public String getStyleClass() {
	return this.styleClass;
    }

    /**
     * @param styleClass
     *            The styleClass to set.
     */
    public void setStyleClass(String styleClass) {
	this.styleClass = styleClass;
    }

    /**
     * @return Returns the ajaxAnchors.
     */
    public String getAjaxAnchors() {
	return this.ajaxAnchors;
    }

    private boolean isAjaxAnchors() {
	return Boolean.valueOf(ajaxAnchors).booleanValue();
    }

    /**
     * @param ajaxAnchors
     *            The ajaxAnchors to set.
     */
    public void setAjaxAnchors(String ajaxAnchors) {
	this.ajaxAnchors = ajaxAnchors;
    }

    /**
     * @see javax.servlet.jsp.tagext.Tag#doEndTag()
     */
    @Override
    public int doEndTag() throws JspException {

	String body = processContent(getBody());

	StringBuilder str = new StringBuilder();
	if (!isAjaxRequest()) {
	    str.append("<div");
	    if (id != null && id.length() != 0) {
		str.append(" id='" + this.id + "'");
	    }
	    if (styleClass != null && styleClass.length() != 0) {
		str.append(" class='" + this.styleClass + "'");
	    }
	    
	    str.append(">");
	}
	str.append(body);
	if (!isAjaxRequest()) {
	    str.append("</div>");
	}

	// XXX isBuffered?
	out(str.toString());
	return isAjaxRequest() ? SKIP_PAGE : EVAL_PAGE; // XXX SKIP will not
	// work if
	// it is buffed!
	// check if this is a buffed tag!
	// then all the out string must go to the request!
    }

    /**
     * @see javax.servlet.jsp.tagext.Tag#release()
     */
    @Override
    public void releaseTag() {
	this.ajaxAnchors = null;
	this.styleClass = null;
    }

    /**
     * Set initial parameters.
     * 
     * @throws JspException
     */
    public void initParameters() throws JspException {
	// XXX displaxtag error ????
	// TODO skip page make problems
	if (isAjaxRequest()) {
	    try {
		pageContext.getOut().clearBuffer();
		// pageContext.getOut().clear();
		// pageContext.getOut().flush();
	    } catch (IOException ex) {
		throw new JspException(ex.getMessage());
	    }
	}
    }

    /**
     * Process content.
     * 
     * @param content
     * @return
     * @throws JspException
     * @throws Exception
     */
    protected String processContent(String content) throws JspException {
	return isAjaxAnchors() ? ajaxAnchors(content, this.id) : content;
    }

}
