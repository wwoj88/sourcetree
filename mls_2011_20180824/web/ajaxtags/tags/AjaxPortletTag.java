/**
 * Copyright 2005 Darren L. Spurgeon
 * Copyright 2007 Jens Kapitza
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.tags;

import javax.servlet.jsp.JspException;

/**
 * Tag handler for the portlet AJAX tag.
 * 
 * @author Darren Spurgeon
 * @author Jens Kapitza
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:24 $ $Author: yagin14 $
 */
public class AjaxPortletTag extends BaseAjaxBodyTag {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public AjaxPortletTag() {
	withBar = true;
    }

    private boolean startMinimize;

    public void setStartMinimize(boolean startMinimize) {
	this.startMinimize = startMinimize;
    }

    public boolean getStartMinimize() {
	return this.startMinimize;
    }

    private String classNamePrefix;

    private String title;

    private String imageClose;

    private String imageMaximize;

    private String imageMinimize;

    private String imageRefresh;

    private String refreshPeriod;

    private boolean executeOnLoad;

    public String getClassNamePrefix() {
	return classNamePrefix;
    }

    public void setClassNamePrefix(String classNamePrefix) {
	this.classNamePrefix = classNamePrefix;
    }

    public String getImageClose() {
	return imageClose;
    }

    public void setImageClose(String imageClose) {
	this.imageClose = imageClose;
    }

    public boolean getExecuteOnLoad() {
	return executeOnLoad;
    }

    public void setExecuteOnLoad(boolean executeOnLoad) {
	this.executeOnLoad = executeOnLoad;
    }

    public String getImageMaximize() {
	return imageMaximize;
    }

    public void setImageMaximize(String imageMaximize) {
	this.imageMaximize = imageMaximize;
    }

    public String getImageMinimize() {
	return imageMinimize;
    }

    public void setImageMinimize(String imageMinimize) {
	this.imageMinimize = imageMinimize;
    }

    public String getImageRefresh() {
	return imageRefresh;
    }

    public void setImageRefresh(String imageRefresh) {
	this.imageRefresh = imageRefresh;
    }

    public String getRefreshPeriod() {
	return refreshPeriod;
    }

    public void setRefreshPeriod(String refreshPeriod) {
	this.refreshPeriod = refreshPeriod;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    @Override
    protected void initParameters() throws JspException {
	if (!this.withBar) {
	    this.imageClose = null;
	    this.imageMaximize = null;
	    this.imageMinimize = null;
	    this.imageRefresh = null;
	}
    }

    @Override
    protected boolean skipBody() {
	return true;
    }

    @Override
    public int doEndTag() throws JspException {
	OptionsBuilder options = getOptionsBuilder();
	options.add("classNamePrefix", this.classNamePrefix, true);
	options.add("title", this.title, true);
	options.add("imageClose", this.imageClose, true);
	options.add("imageMaximize", this.imageMaximize, true);
	options.add("imageMinimize", this.imageMinimize, true);
	options.add("imageRefresh", this.imageRefresh, true);
	options.add("refreshPeriod", this.refreshPeriod, true);
	options.add("executeOnLoad", this.executeOnLoad);
	options.add("startMinimize", this.startMinimize);
	options.add("withBar", this.withBar);

	StringBuffer script = new StringBuffer();
	// surrounding box
	script.append("<div id=\"").append(getSource()).append("\" class=\"")
		.append(this.classNamePrefix).append("Box\">");
	// toolbar
	if (this.withBar) {
	    script.append("<div id=\"").append(getSource()).append(
		    "Tools\" class=\"").append(this.classNamePrefix).append(
		    "Tools\">");
	    if (this.imageRefresh != null) {
		script.append("<img id=\"").append(getSource()).append(
			"Refresh\" class=\"").append(this.classNamePrefix)
			.append("Refresh\" src=\"").append(this.imageRefresh)
			.append("\"/>");
	    }
	    if (this.imageMaximize != null && this.imageMinimize != null) {
		script.append("<img id=\"").append(getSource()).append(
			"Size\" class=\"").append(this.classNamePrefix).append(
			"Size\" src=\"").append(this.imageMinimize).append(
			"\"/>");
	    }
	    if (this.imageClose != null) {
		script.append("<img id=\"").append(getSource()).append(
			"Close\" class=\"").append(this.classNamePrefix)
			.append("Close\" src=\"").append(this.imageClose)
			.append("\"/>");
	    }
	    script.append("</div>");
	}
	// title
	script.append("<div id=\"").append(getSource()).append(
		"Title\" class=\"").append(this.classNamePrefix).append(
		"Title\">").append(this.title).append("</div>");
	// content
	script.append("<div id=\"").append(getSource()).append(
		"Content\" class=\"").append(this.classNamePrefix).append(
		"Content\"></div>");
	script.append("</div>\n");
	script.append("<script type=\"text/javascript\">\n");

	script.append(getJSVariable());
	script.append("new AjaxJspTag.Portlet({\n").append(options.toString())
		.append("});\n").append("</script>\n\n");
	out(script);
	return EVAL_PAGE;
    }

    @Override
    public void releaseTag() {
	this.startMinimize = false;
	this.withBar = false;
	this.classNamePrefix = null;
	this.title = null;
	this.imageClose = null;
	this.imageMaximize = null;
	this.imageMinimize = null;
	this.imageRefresh = null;
	this.refreshPeriod = null;
	this.executeOnLoad = false;
    }

    public boolean getWithBar() {
	return withBar;
    }

    private boolean withBar;

    public void setWithBar(boolean withBar) {
	this.withBar = withBar;
    }

}
