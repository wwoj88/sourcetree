/**
 * Copyright 2005 Darren L. Spurgeon
 * Copyright 2007 Jens Kapitza
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.tags;

import javax.servlet.jsp.JspException;

/**
 * Tag handler for the form field AJAX tag.
 * 
 * @author Darren Spurgeon
 * @author Jens Kapitza
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:26 $ $Author: yagin14 $
 */
public class AjaxFormFieldTag extends BaseAjaxBodyTag {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String action;


    public String getAction() {
	return action;
    }

    public void setAction(String action) {
	this.action = action;
    }
 
    private boolean valueUpdateByName;

    private boolean doPost;

    public void setDoPost(boolean doPost) {
	this.doPost = doPost;
    }

    public boolean getDoPost() {
	return this.doPost;
    }

    @Override
    protected boolean skipBody() {
	return true;
    }

    @Override
    public int doEndTag() throws JspException {
	OptionsBuilder options = getOptionsBuilder();
	options.add("action", this.action, true);
	options.add("doPost", String.valueOf(this.doPost), false);
	options.add("valueUpdateByName",
		String.valueOf(this.valueUpdateByName), false);

	StringBuffer script = new StringBuffer();
	script.append("<script type=\"text/javascript\">\n");
	// XXX check js data
	script.append(getJSVariable());
	script.append(" new AjaxJspTag.UpdateField(\n").append("{\n").append(
		options.toString())
		.append("});\n").append("</script>\n\n");

	out(script);
	return EVAL_PAGE;
    }

    @Override
    public void releaseTag() {
	this.valueUpdateByName = false;
	this.action = null;
    }

    public boolean getValueUpdateByName() {
	return valueUpdateByName;
    }

    public void setValueUpdateByName(boolean valueUpdateByName) {
	this.valueUpdateByName = valueUpdateByName;
    }

}
