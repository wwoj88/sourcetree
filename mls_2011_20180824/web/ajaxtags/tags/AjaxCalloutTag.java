/**
 * Copyright 2005 Darren L. Spurgeon
 * Copyright 2007 Jens Kapitza
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;

/**
 * Tag handler for the callout AJAX tag.
 * 
 * @author Darren Spurgeon
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:24 $ $Author: yagin14 $
 */
public class AjaxCalloutTag extends BaseAjaxBodyTag {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private boolean doPost;


    private String title;

    private String overlib;

    private String emptyFunction;

    private String openEventType;

    private String closeEventType;

    public String getCloseEventType() {
	return this.closeEventType;
    }

    public void setCloseEventType(String closeEvent) {
	this.closeEventType = closeEvent;
    }

    public String getOpenEventType() {
	return this.openEventType;
    }

    public void setOpenEventType(String openEvent) {
	this.openEventType = openEvent;
    }

    public String getEmptyFunction() {
	return this.emptyFunction;
    }

    public void setEmptyFunction(String emptyFunction) {
	this.emptyFunction = emptyFunction;
    }

    public String getOverlib() {
	return this.overlib;
    }

    public void setOverlib(String overlib) {
	this.overlib = overlib;
    }
 

    public String getTitle() {
	return this.title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    @Override
    protected boolean skipBody() {
	return true;
    }

    @Override
    public int doEndTag() throws JspException {
	OptionsBuilder options = getOptionsBuilder();
	options.add("title", this.title, true);
	options.add("overlib", this.overlib, true);
	options.add("emptyFunction", this.emptyFunction, false);
	options.add("openEvent", this.openEventType, true);
	options.add("closeEvent", this.closeEventType, true);
	options.add("doPost", String.valueOf(this.doPost), false);

	StringBuffer script = new StringBuffer();
	script.append("<script type=\"text/javascript\">\n");

	script.append(getJSVariable());

	script.append("new AjaxJspTag.Callout(\n").append("{\n").append(options.toString())
		.append("});\n").append("</script>\n\n");

	out(script);
	return Tag.EVAL_PAGE;
    }

    @Override
    public void releaseTag() {
	this.doPost = false;
	this.title = null;
	this.overlib = null;
	this.emptyFunction = null;
	this.openEventType = null;
	this.closeEventType = null;
    }

    public boolean getDoPost() {
	return this.doPost;
    }

    public void setDoPost(boolean doPost) {
	this.doPost = doPost;
    }

}
