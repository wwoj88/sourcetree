/**
 * Copyright 2007-2008 Jens Kapitza
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.tags;

import javax.servlet.jsp.JspException;

/**
 * Tag handler for AJAX tabbed panel.
 * 
 * @author Jens Kapitza
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:25 $ $Author: yagin14 $
 */
public class AjaxTabPanelTag extends BaseAjaxBodyTag {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void initParameters() throws JspException {
	pages = new StringBuilder();
    }

    private StringBuilder pages = new StringBuilder();

    @Override
    public int doEndTag() throws JspException {
	StringBuffer script = new StringBuffer();
	script.append("<div id=\"");
	script.append(getId());
	script.append("\"></div>");

	// tabs
	if (pages.length() > 1) {
	    pages.deleteCharAt(pages.length() - 1);
	} else {
	    throw new JspException("no pages");
	}
	script.append("<script type=\"text/javascript\">\n");

	OptionsBuilder op = getOptionsBuilder();
	op.add("id", getId(), true);
	op.add("pages", "[" + pages.toString() + " ]", false);
	script.append("new AjaxJspTag.TabPanel({" + op + "});");

	script.append("</script>\n\n");
	out(script);
	return EVAL_PAGE;
    }

    @Override
    public void releaseTag() {
	this.pages = null;
    }

    protected void addPage(AjaxTabPageTag ajaxTabPageTag) {
	pages.append(ajaxTabPageTag.toString()).append(",");
    }

}
