/**
 * Copyright 2005 Darren L. Spurgeon
 * Copyright 2007-2008 Jens Kapitza
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.helpers;

import java.util.Map;

/**
 * extend Item to easily have a tree item with more options
 * 
 * @author Musachy Barroso
 * @author Jens Kapitza
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:46 $ $Author: yagin14 $
 */
public class TreeItem extends Item<String> {

	/**
	 * key to set node or a leaf flag
	 */
	static public final String LEAF = "leaf";
	/**
	 * key to set collapsed flag
	 */
	static public final String COLLAPSED = "collapsed";
	/**
	 * key to set url flag
	 */
	static public final String URL = "url";

	/**
	 * check if this treeitem is a leaf or not
	 * 
	 * @return true if this is a leaf else false
	 */
	public boolean isLeaf() {
		return Boolean.parseBoolean(getAttributeValue(LEAF));
	}

	/**
	 * Constructor for TreeItem
	 */
	public TreeItem() {
		super();
	}

	/**
	 * set node to leaf or not
	 * 
	 * @param l
	 *            true if it is leaf else false
	 */
	public void setLeaf(boolean l) {
		setAttributes(LEAF, String.valueOf(l));
	}

	/**
	 * 
	 * @param name
	 * @param value
	 * @param asData
	 */
	public TreeItem(String name, String value, boolean asData) {
		this(name, value, false, null, asData);
	}

	/**
	 * 
	 * @param name
	 * @param value
	 */
	public TreeItem(String name, String value) {
		this(name, value, false, null, false);
	}

	/**
	 * @param name
	 * @param value
	 * @param collapsed
	 * @param url
	 * @param asData
	 */
	public TreeItem(String name, String value, boolean collapsed, String url,
			boolean asData) {
		this(name, value, asData, null);
		setCollapsed(collapsed);
		setUrl(url);
	}

	/**
	 * @param name
	 * @param value
	 * @param asData
	 * @param attributes
	 */
	public TreeItem(String name, String value, boolean asData,
			Map<String, String> attributes) {
		super(name, value, asData);
		setAllAttributes(attributes);
	}

	/**
	 * @return Returns the colapsed value
	 */
	public boolean isCollapsed() {
		return Boolean.parseBoolean(getAttributeValue(COLLAPSED));
	}

	/**
	 * @param collapsed
	 *            The collapsed value to be set
	 */
	public void setCollapsed(boolean collapsed) {
		setAttributes(COLLAPSED, String.valueOf(collapsed));
	}

	/**
	 * @return Return the URL
	 */
	public String getUrl() {
		return getAttributeValue(URL);
	}

	/**
	 * @param url
	 *            The url to be set
	 */
	public void setUrl(String url) {
		setAttributes(URL, url);
	}

}
