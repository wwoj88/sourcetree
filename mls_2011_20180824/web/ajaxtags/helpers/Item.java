/**
 * Copyright 2007-2008 Jens Kapitza
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A generic item class, basically representing a name-value pair.
 * 
 * @author Darren L. Spurgeon
 * @author Jens Kapitza
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:29:46 $ $Author: yagin14 $
 */
class Item<T> {

    private String name;

    private T value;

    private boolean asData;

    private Map<String, String> attributes = new HashMap<String, String>();

    /**
     * set all attributes
     * 
     * @param attributes
     *            the attributes to set
     */
    public void setAllAttributes(Map<String, String> attributes) {
	if (attributes != null) {
	    for (String key : attributes.keySet()) {
		setAttributes(key, attributes.get(key));
	    }
	}
    }

    /**
     * 
     * @return the key set of the attributes
     */
    public Set<String> getAttributeKeySet() {
	return this.attributes.keySet();
    }


    /**
     * removes an attribute
     * 
     * @param name
     *            the name of attribute
     */
    public void removeAttribute(final String name) {
	this.attributes.remove(name);
    }

    /**
     * clear the attributes
     */
    public void clearAttribute() {
	this.attributes.clear();
    }

    /**
     * set an attribute to extend the item
     * 
     * @param name
     *            the name for the attrubute
     * @param value
     *            the value for the attribute
     */
    public void setAttributes(final String name, final String value) {
	setAttributes(name, value, false);
    }

    /**
     * set an attribute to extend the item
     * 
     * @param name
     *            the name for the attrubute
     * @param value
     *            the value for the attribute
     * @param evenIfNull
     *            set attribute even if it is null
     */
    public void setAttributes(final String name, final String value,
	    boolean evenIfNull) {
	if (value == null && !evenIfNull) {
	    return; // don't set null attributes
	}
	this.attributes.put(name.toLowerCase(), value);
    }

    /**
     * read the attribute value
     * 
     * @param name
     *            the attribute name
     * @return the value of attribute <code>name</code>
     */
    public String getAttributeValue(final String name) {
	return this.attributes.get(name);
    }

    /**
     * Constructor for Item.
     */
    public Item() {
	super();
    }

    /**
     * Constructor for Item.
     * 
     * @param name
     *            the name for the item
     * @param value
     *            the value
     * @param asData
     *            response as CDATA
     */
    public Item(String name, T value, boolean asData) {
	super();
	this.name = name;
	this.value = value;
	this.asData = asData;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
	return this.name;
    }

    /**
     * @param name
     *            The name to set.
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * @return Returns the value.
     */
    public T getValue() {
	return this.value;
    }

    /**
     * @param value
     *            The value to set.
     */
    public void setValue(T value) {
	this.value = value;
    }

    /**
     * @return Returns the asCData.
     */
    public boolean isAsCData() {
	return this.asData;
    }

    /**
     * @param asData
     *            The asData to set.
     */
    public void setAsData(boolean asData) {
	this.asData = asData;
    }

}