/**
 * Copyright (C) 2007 Jens Kapitza
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 
 * @author Jens Kapitza
 * 
 */
public class AjaxActionHelper {
	/**
	 * 
	 * Invoke the ajax action and setup the request and response
	 * 
	 * @param action
	 *            the ajaxaction implementation
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the xml content from action
	 * @throws Exception
	 */
	static public String invoke(BaseAjaxXmlAction action,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// prepare CALL
		request.setCharacterEncoding("UTF-8");
		// Set content to xml
		response.setContentType("text/xml; charset=UTF-8");
		// Set standard HTTP/1.1 no-cache headers.
		response.setHeader("Cache-Control",
				"no-store, max-age=0, no-cache, must-revalidate");

		// Set IE extended HTTP/1.1 no-cache headers.
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");

		// Set standard HTTP/1.0 no-cache header.
		response.setHeader("Pragma", "no-cache");

		// no cahe

		return action.getXmlContent(request, response);
	}
	


    public static String trim2Null(String str) {
	if (str != null && str.trim().length() > 0) {
	    return str;
	}
	return null;
    }
    
}
