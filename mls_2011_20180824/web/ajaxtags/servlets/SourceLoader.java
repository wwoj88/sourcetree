/**
 * Copyright 2007 Jens Kapitza
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * this class loads the frameworks, javascript files and css files from the jar
 * file changes to the javascript or css files can only be done by repacking the
 * ajaxtags.jar
 * 
 * @author Jens Kapitza
 * @version $Revision: 1.1 $
 * @since 1.3
 */
// / XXX TODO use service method
public final class SourceLoader extends GenericServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 4621190060172885624L;

    private String prefix = null;

    /**
     * this is the base where we can find the source wich sould be loaded
     */
    public final static String BASE = "/net/sourceforge/ajaxtags";

    /**
     * write the content from the jarfile to the client stream use
     * bufferedwriter to handel newline the filename is found in the requestURI
     * the contextpath is excluded and replaced with the base package name
     */
    @Override
    public void service(ServletRequest req0, ServletResponse resp0)
	    throws ServletException, IOException {
	HttpServletResponse resp = (HttpServletResponse) resp0;
	HttpServletRequest req = (HttpServletRequest) req0;
	// load the frameworks and the javascript things
	String res = req.getRequestURI();
	// finde the needed resource
	// js/...
	// or css/...
	// or img/...
	String sub = res.substring(req.getContextPath().length());

	if (prefix != null && sub.startsWith(prefix)) {
	    sub = sub.substring(prefix.length());
	}
	
	String name = SourceLoader.BASE + sub;
	InputStream in = this.getClass().getResourceAsStream(name);

	if (in == null) {
	    throw new ServletException("resource not found (" + name + ")");
	}

	OutputStream stream = resp.getOutputStream();
	byte[] buffer = new byte[1024];
	int read = -1;

	while ((read = in.read(buffer)) != -1) {
	    stream.write(buffer, 0, read);
	}
	// XXX is there a autodetection for images?

    }
 
    @Override
    public void init() throws ServletException {
	prefix = AjaxActionHelper.trim2Null(getInitParameter("prefix"));
    }
}
