/**
 * Copyright 2005 Darren L. Spurgeon
 * Copyright 2007 Jens Kapitza
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * An abstract class from which eOach example servlet extends. This class wraps
 * the XML creation (delegated to the child servlet class) and submission back
 * through the HTTP response.
 * 
 * @author Darren L. Spurgeon
 * @author Jens Kapitza
 * @version $Revision: 1.1 $ $Date: 2012/07/02 01:30:11 $ $Author: yagin14 $
 */
public abstract class BaseAjaxServlet extends GenericServlet implements
	BaseAjaxXmlAction {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public String getServletInfo() {
	return "Ajax Servlet (ajaxtags)";
    }

    public void service(ServletRequest request, ServletResponse response)
	    throws ServletException, IOException {

	try {
	    String xml = null;
	    xml = AjaxActionHelper.invoke(this, (HttpServletRequest) request,
		    (HttpServletResponse) response);
	    PrintWriter pw = response.getWriter();
	    pw.write(xml);
	    pw.flush(); // alles senden!
	} catch (Exception ex) {
	    throw new ServletException(ex);
	}
    }

    /**
     * Each child class should override this method to generate the specific XML
     * content necessary for each AJAX action.
     * 
     * @param request
     * @param response
     * @return representation of the XML response/content
     */
    public abstract String getXmlContent(HttpServletRequest request,
	    HttpServletResponse response) throws Exception;
}
