/**
 * Copyright (C) 2007 Jens Kapitza
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.sourceforge.ajaxtags.servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Simple Action which can be invoked by AjaxActionHelper
 * @author Jens Kapitza
 *
 */
public interface BaseAjaxXmlAction {

	/**
	 * Each child class should override this method to generate the specific XML
	 * content necessary for each AJAX action.
	 * 
	 * @param request
	 *            the {@javax.servlet.http.HttpServletRequest} object
	 * @param response
	 *            the {@javax.servlet.http.HttpServletResponse} object
	 * @return a {@java.lang.String} representation of the XML response/content
	 */
	public String getXmlContent(HttpServletRequest request,
			HttpServletResponse response) throws Exception;
}
