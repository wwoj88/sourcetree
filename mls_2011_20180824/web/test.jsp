<html lang="ko"><head>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
  <title>회원 정보 입력 &gt; 회원 가입 &gt; 한국저작권위원회-통합회원 &gt; 한국저작권위원회-통합회원</title>

  <link rel="stylesheet" type="text/css" href="/css/layout.css?v1">
  <link rel="stylesheet" type="text/css" href="/css/sub.css?v1">
  <link rel="stylesheet" type="text/css" href="/css/respon.css?v1">
  <link rel="stylesheet" type="text/css" href="/css/customer.css?v1">
  <style type="text/css">
    #hidden{width:1px; height:1px; border:0; }
  </style>

  <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script type="text/javascript" src="/js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="/js/js.js"></script>
  <script type="text/javascript" src="/js/jsutil.js"></script>
  <script type="text/javascript">
   <!--
   var siteContextPath = '';
   window.onload = function(){
    var errorMassege = '';
    if (errorMassege == '') {
      errorMassege = '';
    }
    if (errorMassege) {
      alert('[오류]'+errorMassege);
    }
    var alertMessage = '';
    if (alertMessage == '') {
      alertMessage = '';
    }
    if (alertMessage) {
      alert(alertMessage);
    }
    var alertSSOMessage = '';
    if (alertSSOMessage == '') {
      alertSSOMessage = '';
    }
    
    if (alertSSOMessage) {
      if ('SITE006') {
        fn_siteLink('SITE006', 'hiddenifr');
      }
      alert(alertSSOMessage);
     }
   };
  function fn_siteLink(value, target){
    if (target) {
      $('#footerSiteLinkFrom').attr("target",target);
    }
      if (value == "SITE001" ) {
        // 대표 누리집
      $('#footerSiteLinkFrom').attr("action","https://www.copyright.or.kr/ssoLoginMain.do");
      $('#footerSiteLinkFrom').submit();
      } else if (value == "SITE005" ) {
        // 디지털 저작권 거래소
      $('#footerSiteLinkFrom').attr("action","https://www.kdce.or.kr/user/main.do");
      $('#footerSiteLinkFrom').submit();
      } else if (value == "SITE006" ) {
        // 권리자 찾기
      $('#footerSiteLinkFrom').attr("action","https://www.findcopyright.or.kr/ssoMain.do");
      $('#footerSiteLinkFrom').submit();
      } else if (value == "SITE007" ) {
        // OLIS 오픈소스SW 라이선스
      $('#footerSiteLinkFrom').attr("action","https://www.olis.or.kr/membership/ssoOlisLogin.do");
      $('#footerSiteLinkFrom').submit();
      } else if (value == "SITE008" ) {
        // 공유마당
      $('#footerSiteLinkFrom').attr("action","https://gongu.copyright.or.kr/gongu/member/user/ssoLogin.do");
      $('#footerSiteLinkFrom').submit();
      } else if (value == "SITE009" ) {
        // 저작권 인증
      $('#footerSiteLinkFrom').attr("action","https://cras.copyright.or.kr/front/right/comm/ssoLoginMain.do");
      $('#footerSiteLinkFrom').submit();
      } else if (value == "SITE010" ) {
        // 저작권 등록
      $('#footerSiteLinkFrom').attr("action","https://www.cros.or.kr");
      $('#footerSiteLinkFrom').submit();
      } else if (value == "SITE013" ) {
        // 온라인임치
      $('#footerSiteLinkFrom').attr("action","");
      $('#footerSiteLinkFrom').submit();
      } else {
      window.open(value,"","");
      }
      $('#footerSiteLinkFrom').attr("target","_blank");
      
   }
  //-->
  </script>
  
  
  <script type="text/javascript" src="/js/cmmn/zip_common.js"></script>
  <script type="text/javascript" src="/js/cmmn/common.js"></script>
  <script type="text/javascript" src="/js/jsutil.js"></script>
  <script type="text/javascript" src="/js/cmmn/certification.js"></script>
  <script type="text/javascript" src="/js/cmmn/timer.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      var corpType = "person";

      if(corpType == "person"){
        $('input[name=membDivis]').eq(1).attr("disabled", true);

        $('input[name=membDivis]').eq(0).attr("checked", true);
      }else if(corpType == "law"){
        $('input[name=membDivis]').eq(0).attr("disabled", true);
        $('input[name=membDivis]').eq(2).attr("disabled", true);
        $('input[name=membDivis]').eq(3).attr("disabled", true);
        $('input[name=membDivis]').eq(4).attr("disabled", true);
        $('input[name=membDivis]').eq(1).attr("checked", true);
      }
      
      $(".popup-btn4").click(function(){
//        $(".mb-bg").css({"display":"block"});
        $(".open4").css({"display":"block"});
      });
      var busiRegnum = "220-87-84100";
      $('#busiRegnum').text(busiRegnum.split("-")[0] + "-" + busiRegnum.split("-")[1] + "-" + busiRegnum.split("-")[2])
      $('#membBuisnRegnum').val(busiRegnum.split("-")[0] + "-" + busiRegnum.split("-")[1] + "-" + busiRegnum.split("-")[2]);

      var corpNum = "";
      if(corpNum != ""){
        $('#corpNum').text(corpNum.split("-")[0] + "-" + corpNum.split("-")[1])
        $('#membCorpnum').val(corpNum.split("-")[0] + "-" + corpNum.split("-")[1]); 
      }
      $(".changeEmailDomain").change(function(){
        if($(this).val() != ""){
          $(this).closest(".emailArea").find(".targetEmail").val($(this).val());
        }else{
          $(this).closest(".emailArea").find(".targetEmail").val("");
        }
      });
    });
    
    var checkId ="";
    var checkIdDate;
    var checkCell ="";
    var checkCellDate;
    
    function fn_checkCellCert() {
      if (checkCellDate) {
        var elapsed = ((new Date()).getTime() - checkCellDate.getTime()) / 1000;
        if (elapsed < 60) {
          alert("인증번호가 전송되었습니다. 다시 인증번호를 발송하려면, 1분 후 다시 시도해 주세요.");
          return;
        }
      }
      
      var tempCellnum = $('#membCelnum_first').val() + '-' + $('#popMembCelnum_second').val() + '-' + $('#popMembCelnum_third').val();
      if (ValidationChecker.isEmptyString($('#membCelnum_first').val())
          || ValidationChecker.isEmptyString($('#popMembCelnum_second').val())
          || ValidationChecker.isEmptyString($('#popMembCelnum_third').val())) {
        alert('연락처를 입력하세요.');
        return;
      }
      if (ValidationChecker.isCellFormat(tempCellnum) == false) {
        alert('연락처가 형식에 맞지 않습니다. 다시 입력하세요.');
        return;
      }
      try {
        $.ajax({
          url : "/common/membCellCert.do",
          type : 'post',
          async : false,
          data : {
            'membCelnum' : tempCellnum
          },
          beforeSend : function(xhr)
                    {   /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                        xhr.setRequestHeader("X-CSRF-TOKEN", "22a4e251-5735-4bcf-a6b5-dc951372358c");
                    },
          success : function(data) {
            if (data == 'Y') {
              checkCell = tempCellnum;
              $("#inputCellCert").show();
              checkCellDate = new Date();
              initSmsTime();
              initSmsTimer();
            } else {
              alert('연락처 인증 중 오류가 발생하였습니다. 다시 시도해 주세요.');
              $('membCelnum_first').focus();
              $("#inputCellCert").hide();
            }
            
          },
          error : function(xhr, errmsg, errobj) {
            alert(errmsg);
            $("#inputCellCert").hide();
          }
        });
      } catch(e) {
        alert('연락처 인증 중 오류가 발생하였습니다. 다시 시도해 주세요.');
        $("#inputCellCert").hide();
      }
    }
    
     /**
     * 입력한 연락처의 인증 확인
     */
     function fn_checkCellCertConfirm() {
      if (ValidationChecker.isEmptyString($('#cellCertNum').val())) {
        alert('인증코드를 입력하세요.');
        return;
      }
      try {
        $.ajax({
          url : "/common/membCellCertConfirm.do",
          type : 'post',
          async : false,
          data : {
            'cellCertNum' :  $('#cellCertNum').val()
          },
          beforeSend : function(xhr)
                    {   /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                        xhr.setRequestHeader("X-CSRF-TOKEN", "22a4e251-5735-4bcf-a6b5-dc951372358c");
                    },
          success : function(data) {
            if (data == 'Y') {
              alert('인증 확인 되었습니다.');
              checkCell = checkCell + '_Y';
              $("#inputCellCert").hide();
              $("#cell-authen-code-button").hide();
              $("#membCelnum_first").attr("disabled","disabled");
              $("#membCelnum_second").attr("readonly","readonly");
              $("#membCelnum_third").attr("readonly","readonly");
              clearSmsTimer();
            } else if (data == 'T') {
              alert('인증  대기 시간을 초과 했습니다. 다시 시도해 주십세요.');
              checkCell = '';
              $("#inputCellCert").hide();
            } else {
              alert('인증 실패 화였습니다. 다시 시도해 주세요.');
              $('#cellCertNum').focus();
            }
            
          },
          error : function(xhr, errmsg, errobj) {
            alert(errmsg);
          }
        });
      } catch(e) {
        alert('연락처 인증 중 오류가 발생하였습니다. 다시 시도해 주세요.');
      }
    }
     
    function fn_singUpSubmit() {
      // 유효성 검사 후 Form 저장
      var result = fn_checkSingUpValidation();
      if (result.is_success == false) {
        alert(result.resultmessage);
        result.focus_element.focus();
        return;
      }
      
      if(!confirm('입력하신 정보로 가입하시겠습니까?')) return;
      $('#signUpInputForm').submit();         
    }
    
    function fnDuplicateIdCheck(){
      if (ValidationChecker.isEmptyString($('#membId1').val())
          || ValidationChecker.isEmptyString($('#membId2').val())) {
        alert('아이디(이메일)를 입력하세요.');
        
        if(ValidationChecker.isEmptyString($('#membId1').val())){
          $('#membId1').focus();
          return;
        }

        if(ValidationChecker.isEmptyString($('#membId2').val())){
          $('#membId2').focus();
          return;
        }
        
        return;
      }
      
      if (ValidationChecker.isEmail($('#membId1').val() + '@' + $('#membId2').val()) == false) {
        alert('아이디를 이메일 형식으로 다시 입력하세요.');
        return;
      }
      
      var checkId ="";
      var userEmailId = $('#membId1').val() + "@" + $('#membId2').val();
      //alert('ajax를 호출 하여 userEmailId를 중복 체크 한다. /n결과 값은 form id="emailCheckYn" "Y/N"값을 넣는다. ');
      try {
        $.ajax({
          url : "/member/signUpCorp/membIdCert.do",
          type : 'post',
          async : false,
          data : {
            'membId' : userEmailId
          },
          beforeSend : function(xhr)
                    {   /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                        xhr.setRequestHeader("X-CSRF-TOKEN", "22a4e251-5735-4bcf-a6b5-dc951372358c");
                    },
          success : function(data) {
            if (data == 'Y') {
              checkId = ($('#membId1').val() + '@' + $('#membId2').val());
              checkIdDate = new Date();
              $('#emailCheckYn').val('Y');
              $('#idCheckmsg').text("사용 할수 있는 아이디 입니다.");
              $('#idCheckmsg').css("color","blue");
            } else if (data == 'N') {
              alert('이미 사용하고 있는 아이디 입니다. 다른 아이디를 입력하여 주세요.');
              $('#membId1').focus();
            }
          },
          error : function(xhr, errmsg, errobj) {
            alert('아이디 인증 중 오류가 발생하였습니다. 다시 시도해 주세요.');
            $('#membId1').focus();
          }
        });
      } catch(e) {
        alert('아이디 인증 중 오류가 발생하였습니다. 다시 시도해 주세요.');
        $('#membId1').focus();
      }
    }
    
    function fn_checkSingUpValidation() {
      if ($('#emailCheckYn').val() == "" ){
        return { is_success : false, resultmessage : '아이디(이메일 형식) 중복 확인 해주세요', focus_element : $('#membId1') };
      }
      
      if (ValidationChecker.isEmail($('#membId1').val() + '@' + $('#membId2').val()) == false) {
        return { is_success : false, resultmessage : '아이디를 이메일 형식으로 다시 입력하세요.', focus_element : $('#membId1') };
      }
      
      var userEmailId = $('#membId1').val() + "@" + $('#membId2').val();
      $('#membId').val(userEmailId);
      
      if(ValidationChecker.isEmptyString($('#membPwd').val())) {
        return { is_success : false, resultmessage : '비밀번호를 입력하세요.', focus_element : $('#membPwd') };
      }
      
      var returnRegex = Number(ValidationChecker.pwdCheck($('#membPwd')));
      
            if(returnRegex == -1) {
              return { is_success : false, resultmessage : '비밀번호는 9~20 자로 입력해주세요.', focus_element : $('#membPwd') };
            } else if(returnRegex == -10) {
              return { is_success : false, resultmessage : '비밀번호는 영문 숫자 특수문자 중 2개 이상 혼용 사용하여야 합니다.', focus_element : $('#membPwd') };
            } else if(returnRegex == -30) {
                return { is_success : false, resultmessage : '비밀번호는 영문 숫자 특수문자 중 2개 이상 혼용시 \n 10~20 자리로 구성되어야 합니다.', focus_element : $('#membPwd') };
            } else if(returnRegex == -40) {
                return { is_success : false, resultmessage : '비밀번호는 영문 숫자 특수문자 중 3개 이상 혼용시 \n 9 자리로 구성되어야 합니다.', focus_element : $('#membPwd') };
            }
      
      if($('#membPwd').val() != $('#membPwdCheck').val()) {
        return { is_success : false, resultmessage : '비밀번호와 비밀번호 확인 문자가 일치하지 않습니다. 다시 한번 확인해 주세요.', focus_element : $('#membPwd') };
      }
      
      //in-5-2  - > 주소
      if(ValidationChecker.isEmptyString($('#in-5-2').val())){
        return { is_success : false, resultmessage : '주소를 입력해주세요', focus_element : $('#in-5-2') };
      }
      
      if(ValidationChecker.isEmptyString($('#membCelnumCertYn').val())){
        return { is_success : false, resultmessage : '담당자 본인 인증을 해주세요', focus_element : $('#mangName') };
      }
      
      if(ValidationChecker.isEmptyString($('#mangName').val())){
        return { is_success : false, resultmessage : '담당자를 입력해주세요', focus_element : $('#mangName') };
      }
      
      
      var mangTelNo = $('#membCelnum_first').val() + "-" + $('#membCelnum_second').val() + "-" + $('#membCelnum_third').val();
      $('#membCelnum').val(mangTelNo);
      // ---- 위에꺼 까지 체크 완료
          
      if (ValidationChecker.isEmail($('#corpManaMail_first').val() + '@' + $('#coprManaMail_second').val()) == false) {
        return { is_success : false, resultmessage : '관리자 아이디를 이메일 형식으로 다시 입력하세요.', focus_element : $('#corpManaMail_first') };
      }
      
      $('#membEmail').val($('#corpManaMail_first').val() + '@' + $('#coprManaMail_second').val());
      
      return true;
    }
    
    function fn_certPopup (url) {
      try {
              var width = 450;
              var height = 550;
              var centerWidth = (screen.width-width) / 2;
              var centerHeight = (screen.height-height) / 2;
        var popupWindow = window.open(url, 'CERT_POPUP', 'width=' + width +', height=' + height + ', top=' + centerHeight +  ', left=' + centerWidth + ' ,toolbar=no, location=no, directories=no, status=no, scrollbars=yes, resizeable=yes, copyhistory=no, menubar=no, fullscreen=no');
          if(popupWindow == null) {
                   alert("팝업이 차단되어서 정상적으로 사용하실수 없습니다.\n\n현 사이트 주소를 팝업차단에서 허용해 주시기 바랍니다.\n\n\n================================   참     고   ==================================\n\n1. Explore 도구 - 팝업 차단 설정 에서 팝업차단 사항을 확인하시기 바랍니다.\n\n2. 인터넷에서 툴바를 설치하신 경우에 툴바에서 팝업차단 설정 사항을 확인하시기 바랍니다.");
              } else {
                   popupWindow.focus();
              }
          $(".popup-div01-close").click();
      } catch(e) {
        alert("이름 변경 중 오류가 발생했습니다. ");
      }
    }
    // 밑에 소스는 테스트 소스 작업 후 꼭 지울것
  </script>

</head>
<body>
<iframe id="hidden" name="hiddenifr" title="내용 없음"></iframe>
<!-- skindivi start -->
<div id="skipNavigation" title="스킵내비게이션">
  <a href="#go-con">본문내용 바로가기</a>
    <a href="#go-menu">상단메뉴 가기</a>
</div>
<!-- skindivi end -->
  <div id="wrap">
    <div id="header">
           
           









  <div class="width-auto clearfix">
    <h1><a href="/main.do"><img src="/images/main/logo.gif" alt="한국저작권위원회 통합회원OneID서비스"></a></h1>
    <div id="gnb">
      <ul class="gnb-depth1 clearfix" id="go-menu">
        
          
          
            
              
            
              
                <li><a href="/login/login.do">통합회원 로그인</a></li>
              
            
              
                <li><a href="/member/signUp/signUpStep1.do">통합회원 가입</a></li>
              
            
              
                <li><a href="/member/infoFind/idFindStep1.do">아이디 찾기</a></li>
              
            
              
                <li><a href="/member/infoFind/passFindStep1.do">비밀번호 찾기</a></li>
              
            
              
            
              
            
              
            
              
            
              
                <li><a href="/member/contentsView/contentsView.do?userMenuInputCode=100000">이용안내</a></li>
              
            
              
            
              
            
              
            
              
            
              
            
              
            
              
            
              
            
              
            
              
            
              
            
              
            
          
        
      </ul>
    </div>
    <div id="all-menu"><a href="#n"><img src="/images/main/all_menu.gif" alt="전체메뉴"></a></div>
  </div>
      </div>

    <div id="contents">
      
        
<h2 id="sub-title"><strong>회원가입</strong></h2>
<div class="width-auto clearfix" id="go-con">
  <div id="sub-contents">
    <!-- s -->
    <div class="join-location"><img src="/images/sub/join2_location3.gif" alt="약관동의, 본인인증, 정보입력, 전환완료"></div>
    <h3 class="h3-title">회원 정보 입력</h3>
    <p class="join-location-text">기본정보 <span>*</span> 표시는 필수 입력 사항입니다.  필수정보 입력만으로도 일반적인 회원 서비스를 이용할 수 있습니다.</p>
    <div class="table-style">
      <form id="signUpInputForm" action="/member/signUpTransCorp/signUpTransStep5.do" method="post">
        <input type="hidden" name="_csrf" value="22a4e251-5735-4bcf-a6b5-dc951372358c">
        <input type="hidden" id="emailCheckYn" value="">
        <input type="hidden" id="membId" name="membId" value="">
        <input type="hidden" id="membCelnum" name="membCelnum" value="">
        <input type="hidden" id="membEmail" name="membEmail" value="">
        <input type="hidden" id="membBuisnRegnum" name="membBuisnRegnum" value="220-87-84100">
        <input type="hidden" id="membCorpnum" name="membCorpnum" value="">
        <input type="hidden" id="membCelnumCertYn" name="membCelnumCertYn" value="">
        <input type="hidden" id="siteCode" name="siteCode" value="SITE006">
        <table>
          <caption>회원 정보 입력 : 성명, 생년월일, 아이디(이메일 형식), 비밀번호, 비밀번호 확인, 연락처, 이메일, 이메일 수신여부, SMS 수신여부</caption>
          <colgroup>
            <col style="width: 20%;">
            <col style="width: 80%;">
          </colgroup>
          <tbody>
            <tr>
              <th scope="row"><span>*</span>회원 종류 구분</th>
              <td>
                
                  
                  
                    
                  
                  <input type="radio" id="membDivis" name="membDivis" checked="checked" value="050101"> <label for="diA-1">개인사업자</label>
                
                  
                  
                  <input type="radio" id="membDivis" name="membDivis" value="050102" disabled="disabled"> <label for="diA-1">법인사업자</label>
                
                  
                  
                  <input type="radio" id="membDivis" name="membDivis" value="050103"> <label for="diA-1">기관</label>
                
                  
                  
                  <input type="radio" id="membDivis" name="membDivis" value="050104"> <label for="diA-1">학교</label>
                
                  
                  
                  <input type="radio" id="membDivis" name="membDivis" value="050105"> <label for="diA-1">단체</label>
                
              </td>
            </tr>
            <tr>
              <th scope="row"><label for="in-1"><span>*</span>아이디(이메일 형식)</label></th>
              <td class="emailArea">
                <input type="text" id="membId1" class="mail-width2" title="이메일 앞자리 입력" value="">
                @
                <input type="text" id="membId2" class="mail-width2 targetEmail" title="이메일 뒷자리 입력" value="">
                <select title="이메일 선택" class="new-wp20 changeEmailDomain">
                  <option value="">이메일 선택</option>
                  
                    <option value="naver.com">naver.com</option>
                  
                    <option value="hanmail.net">hanmail.net</option>
                  
                    <option value="nate.com">nate.com</option>
                  
                    <option value="gmail.com">gmail.com</option>
                  
                    <option value="hotmail.com">hotmail.com</option>
                  
                    <option value="empal.com">empal.com</option>
                  
                    <option value="chol.com">chol.com</option>
                  
                    <option value="dreamwiz.com">dreamwiz.com</option>
                  
                    <option value="freechal.com">freechal.com</option>
                  
                    <option value="hanafos.com">hanafos.com</option>
                  
                    <option value="paran.com">paran.com</option>
                  
                    <option value="yahoo.co.kr">yahoo.co.kr</option>
                  
                    <option value="korea.com">korea.com</option>
                  
                </select>               
                <a href="javascript:void(0);" onclick="javascript:fnDuplicateIdCheck();" class="gray-btn">중복확인</a>
                <a id="idCheckmsg"></a>
              </td>
            </tr>
            <tr>
              <th scope="row"><label for="in-2"><span>*</span>비밀번호</label></th>
              <td>
                <input type="password" name="membPwd" id="membPwd" class="m-width" title="비밀번호 입력">
                <p class="p-text">※ 비밀번호는 <span class="color-orange">9~20자의 영문 대/소문자, 숫자, 특수문자를 혼합해서</span> 사용해주세요.
                  <br>- 영문 대/소문자, 숫자, 특수문자 3가지 이상 조합 : <span class="color-orange">9자리 이상</span>
                  <br>- 영문 대/소문자, 숫자, 특수문자 중 2가지 이상 조합 : <span class="color-orange">10자리 이상</span>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row"><label for="in-3"><span>*</span>비밀번호 확인</label></th>
              <td><input type="password" name="membPwdCheck" id="membPwdCheck" class="m-width" title="비밀번호 확인 입력"></td>
            </tr>
            <tr>
              <th scope="row"><span>*</span>상호명</th>
              <td>
                아이티밥
                <input type="hidden" id="membName" name="membName" value="아이티밥">
              </td>
            </tr>
            <tr>
              <th scope="row"><span>*</span>사업자등록번호</th>
              <td id="busiRegnum">220-87-84100</td>
            </tr>
            <tr>
              <th scope="row"><label for="in-4">법인번호</label></th>
              <td id="corpNum"></td>
            </tr>
            <tr>
              <th scope="row"><label for="in-5"><span>*</span>주소</label></th>
              <td>
                <input type="text" id="coprAddr&quot;" name="zipcd" class="new-wp20 bg_gray" title="주소 입력" readonly="">
                <a href="javascript:void(0);" class="gray-btn" onclick="javascript:goPopup('signUpInputForm', 'basic');">우편번호 검색</a>
                <p class="p-text">
                  <input type="text" id="in-5-2" name="addr" class="new-wp50 bg_gray" title="주소 입력" readonly="">
                  <input type="text" id="in-5-3" name="addrDetl" class="new-wp50" title="주소 입력">
                </p>
              </td>
            </tr>
            <tr>
            
              <th scope="row"><label for="in-5"><span>*</span>담당자명</label></th>
              <td>
                <input type="text" id="mangName" name="mangName" value="" class="new-wp25" title="담당자명 입력">
                <a href="#n" class="gray-btn popup-btn6">담당자 본인인증</a>
              </td>
            </tr>
            <tr>
              <th scope="row"><label for="in-6"><span>*</span>담당자 연락처</label></th>
              <td>
                <select id="membCelnum_first" title="연락처 앞자리 선택" class="phone-width">
                  
                    <option value="010">010</option>
                  
                    <option value="011">011</option>
                  
                    <option value="016">016</option>
                  
                    <option value="017">017</option>
                  
                    <option value="018">018</option>
                  
                    <option value="019">019</option>
                  
                </select> -
                <input type="text" title="연락처 중간자리 입력" id="membCelnum_second" class="phone-width" maxlength="4" value="" onkeydown="return keyEvent.onlyNumber(event)"> -
                <input type="text" title="연락처 뒤자리 입력" id="membCelnum_third" class="phone-width" maxlength="4" value="" onkeydown="return keyEvent.onlyNumber(event)">
                <a href="#n" id="cell-authen-code-button" class="gray-btn ml20 popup-btn4">휴대폰번호 인증</a>         
              </td>
            </tr>
            <tr>
              <th scope="row"><label for="in-7"><span>*</span>담당자 이메일</label></th>
              <td class="emailArea">
                <input type="text" id="corpManaMail_first" class="mail-width2" value="" title="담당자 이메일 입력">
                @
                <input type="text" id="coprManaMail_second" class="mail-width2 targetEmail" value="" title="담당자 이메일 입력">
                <select title="이메일 선택" class="new-wp20 changeEmailDomain">
                  <option value="">이메일 선택</option>
                  
                    <option value="naver.com">naver.com</option>
                  
                    <option value="hanmail.net">hanmail.net</option>
                  
                    <option value="nate.com">nate.com</option>
                  
                    <option value="gmail.com">gmail.com</option>
                  
                    <option value="hotmail.com">hotmail.com</option>
                  
                    <option value="empal.com">empal.com</option>
                  
                    <option value="chol.com">chol.com</option>
                  
                    <option value="dreamwiz.com">dreamwiz.com</option>
                  
                    <option value="freechal.com">freechal.com</option>
                  
                    <option value="hanafos.com">hanafos.com</option>
                  
                    <option value="paran.com">paran.com</option>
                  
                    <option value="yahoo.co.kr">yahoo.co.kr</option>
                  
                    <option value="korea.com">korea.com</option>
                  
                </select>               
                <p class="p-text">* 서비스 이용 안내 메일 등 각종 메일 수신 시 사용됩니다. (이메일 아이디는 이메일 수신용으로 사용되지 않습니다.)</p>
              </td>
            </tr>
            <tr>
              <th scope="row">이메일 수신여부</th>
              <td>
                <input type="radio" id="memb_email_cert_yn" name="memb_email_cert_yn"> <label for="memb_email_cert_yn">동의</label>
                <input type="radio" id="memb_email_cert_yn" name="memb_email_cert_yn" checked="checked"> <label for="memb_email_cert_yn">동의 안함</label>
                <p class="p-text2">* 비밀번호 재발급, 회원정보 재동의 등 서비스 이용과 관련된 안내는 수신동의와 관계없이 발송됩니다.</p>
              </td>
            </tr>
            <tr>
              <th scope="row">SMS 수신여부</th>
              <td>
                <input type="radio" id="memb_celnum_cert_yn" name="memb_celnum_cert_yn"> <label for="memb_celnum_cert_yn">동의</label>
                <input type="radio" id="memb_celnum_cert_yn" name="memb_celnum_cert_yn" checked="checked"> <label for="memb_celnum_cert_yn">동의 안함</label>
                <p class="p-text2">* 비밀번호 재발급, 회원정보 재동의 등 서비스 이용과 관련된 안내는 수신동의와 관계없이 발송됩니다.</p>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
    <div class="button-box">
      <a href="javascript:fn_singUpSubmit();" class="y">가입하기</a>
      <a href="/login/login.do">취소하기</a>
    </div>
    <div class="popup-div01 open4 w35p">
      <div class="title-div clearfix">
        <h3>휴대폰번호 인증</h3>
        <a href="#none" class="popup-div01-close"><img src="/images/sub/pop_close.gif" alt="닫기"></a>
      </div>
      <div class="con-div">
        <div class="authentication-code-div">
          <div class="floor1">
            <select id="membCelnum_first" title="연락처 앞자리 선택" class="phone-width wid60">
              <option>010</option>
            </select> -
            <input type="text" id="popMembCelnum_second" title="연락처 중간자리 입력" class="phone-width wid60" maxlength="4"> -
            <input type="text" id="popMembCelnum_third" title="연락처 뒤자리 입력" class="phone-width wid60" maxlength="4"> <a href="javascript:fn_checkCellCert();" class="gray-btn">휴대폰번호 인증</a>
          </div>
          <div class="floor2 none" id="inputCellCert">
            <p class="p-text">휴대폰 문자메시지로 인증코드가 전송되었습니다.<br>아래 인증코드 칸에 전송 받은 인증코드를 입력해 주십시오.</p>
            <label for="cellCertNum">인증코드 : </label><input type="text" id="cellCertNum" class="s-width wid105" title="인증코드 입력"> <a href="javascript:fn_checkCellCertConfirm();" class="gray-btn">확인</a><span id="smsTimer" style="color:red"></span>
          </div>
        </div>
        <div class="button-box">
          <a href="#none" class="popup-div01-close">닫기</a>
        </div>
      </div>
    </div>
    
    <div class="popup-div01 open6 w450">
      <div class="title-div clearfix">
        <h3>담당자 본인 인증</h3>
        <a href="#none" class="popup-div01-close"><img src="/images/sub/pop_close.gif" alt="닫기"></a>
      </div>
      <div class="con-div">
        <div class="authentication-code-div">
          <div class="box-ul">
            <ul>
              <li> <a href="javascript:fn_certPopup('/checkplus/index.do?targetName=CORP_MAMANGER_AUTH&amp;selfAuthYn=Y');"><img src="/images/sub/join3_icon1.gif" alt="휴대폰 본인인증" title="새창열림"></a></li>
              <li> <a href="javascript:fn_certPopup('/ipin/index.do?targetName=CORP_MAMANGER_AUTH&amp;selfAuthYn=Y');"><img src="/images/sub/join3_icon2.gif" alt="아이핀" title="새창열림"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

       </div>
  </div>

  <div id="footer">
       
       








  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-56393753-13"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){
      dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-56393753-13');
  </script>

  <div class="footer-top">
    <div class="width-auto clearfix">
      <ul class="clearfix">
        <li><a href="/member/contentsView/contentsView.do?userMenuInputCode=120000">이용약관</a></li>
        <li><a href="https://www.copyright.or.kr/customer-center/user-guide/privacy-policy/index.do" class="bold" target="_blank" title="새창 열림"><span class="color-orange">개인정보처리방침</span></a></li>
        
      </ul>
      <div>
        <label for="site_move">한국저작권위원회 사이트 바로가기</label>
          <select class="footLink" id="site_move">
            <option value="SITE001">대표홈페이지</option>
            <option value="https://www.cros.or.kr">저작권 등록</option>
            <option value="SITE009">저작권 인증</option>
            <option value="https://www.swes.or.kr">온라인 SW임치</option>
            <option value="SITE006">권리자 찾기</option>
            <option value="SITE008">공유마당</option>
            <option value="SITE005">디지털 저작권거래소</option>
            <option value="SITE007">오픈소스SW 라이선스</option>
            <option value="https://edu-copyright.or.kr">원격교육연수원</option>
            <option value="https://edulife.copyright.or.kr">원격평생교육원</option>
          </select>
          <button class="footGo" type="button" title="새창열림">이동</button>
      </div>
    </div>
  </div>
  <div class="footer-bottom width-auto clearfix">
    <div class="footer-bottom-left clearfix">
      <h1><a href="#n"><img src="/images/main/f_logo.gif" alt="한국저작권위원회 하단로고"></a></h1>
      <p>진주 [우 52852] 경상남도 진주시 충의로 19, 1/2/5층 <span>☎ 대표번호 055.792.0000</span><br>서울 [우 04323] 서울특별시 용산구 후암로 107, 5/16층 <span>☎ 대표번호 02.2669.0010</span></p>
    </div>
    <ul class="footer-bottom-right clearfix">
      <li><a href="http://www.eprivacy.or.kr/seal/mark.jsp?mark=e&amp;code=2019-R068" title="새창열림" target="_blank" onclick="window.open('http://www.eprivacy.or.kr/seal/mark.jsp?mark=e&amp;code=2019-R068', 'popup', 'width=500, height=700, scrollbars=yes, toolbal=no, location=no, status=no, menubar=no, resizable=no'); return false;"><img src="/images/main/main_42.gif" alt="개인정보보호 우수 사이트 안내"></a></li>
      <li><a href="http://www.eprivacy.or.kr/seal/mark.jsp?mark=i&amp;code=2019-R025" title="새창열림" target="_blank" onclick="window.open('http://www.eprivacy.or.kr/seal/mark.jsp?mark=i&amp;code=2019-R025', 'popup', 'width=500, height=700, scrollbars=yes, toolbal=no, location=no, status=no, menubar=no, resizable=no'); return false;"><img src="/images/main/main_44.gif" alt="인터넷 사이트 안전마크 안내"></a></li>
      <li><a title="웹접근성 품질인증서 - [새창열기]" href="/pdf/WA/2019-851_Certification_oneid.pdf" target="_blank" onclick="window.open('/pdf/WA/2019-851_Certification_oneid.pdf', 'popup', 'width=600, height=850, scrollbars=yes, toolbal=no, location=no, status=no, menubar=no, resizable=no'); return false;" style="width:50px;height:50px;">
        <img class="wa" alt="(사)한국장애인단체총연합회 한국웹접근성인증평가원 웹 접근성 우수사이트 인증마크(WA인증마크)" src="/images/common/WAmark.png">
      </a></li>
    </ul>
  </div>
  <form id="footerSiteLinkFrom" method="post" action="#" target="_blank">
    
  </form>

    <div class="m-bg"></div>
  </div>

</body></html>