(function($) {
	$.fn.CKEditorInit = function(option) {
		var ksyCKEditor = {};
		ksyCKEditor.viewObj = $(this);
		ksyCKEditor.viewObj.bodyHtml = $(this).html();
		ksyCKEditor.viewObj.html('');
		
		var editorId = ksyCKEditor.viewObj.attr('id')+"_editor";
		var editorHeight = option.height;
		if(option.width){
			ksyCKEditor.viewObj.css('width',option.width);	
		}else{
			ksyCKEditor.viewObj.css('width',"97%");	
		}
		
		var defaultFont = "굴림";
		var fontNames = '굴림/Gulim;돋움/Dotum;바탕/Batang;궁서/Gungsuh;맑은고딕/Malgun Gothic;Arial;Comic Sans MS;Courier New;Tahoma;Times New Roman;Verdana';
		if(option.language != null && option.language.indexOf("ko") == -1){
			defaultFont = "Arial";
			fontNames = 'Arial;Gulim;Dotum;Batang;Gungsuh;Malgun Gothic;Comic Sans MS;Courier New;Tahoma;Times New Roman;Verdana';
		}
		
		var context = option.context == null || option.context == '' || option.context == '/' ? "" : option.context;
		
		var editorOption =  $.extend(
				{
					path						: "/egov/js/ckeditor/",	
					ImageUploadURL				: context + "/cmm/bbs/ckeditorImgUpload.do",
					filebrowserImageUploadUrl : context + "/cmm/bbs/ckeditorImgUpload.do",
					filebrowserUploadUrl : context + "/cmm/bbs/ckeditorImgUpload.do",
					toolbar : [
						{ name : 'document', items : ['Preview','-','Cut','Copy','Paste','PasteText','PasteWord']},
						{ name : 'clipboard', items : ['Undo','Redo','-','Find','Replace']},
						{ name: 'editing', items : ['Bold','Italic','Underline','StrikeThrough']},
						{ name: 'paragraph', items :['OrderedList','UnorderedList','-','Outdent','Indent', '-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyFull']},
						{ name: 'insert', items : ['Link','Image', 'Table','Rule','SpecialChar']},
						'/',
						{ name: 'styles', items : ['Styles','Format','Font','FontSize']},
						{ name: 'colors', items : ['TextColor','BGColor']},
						{ name: 'html', items : ['Source']}
					],
					enterMode		:'2',
					shiftEnterMode	:'3',
					font_names:fontNames,
					fontSize_defaultLabel:'12',
					font_defaultLabel:defaultFont	,
					notsupportedimageextension : "js,jsp,jspx,php,sh,exe",
					notsupportedlinkeextension : "js,jsp,jspx,php,sh,exe"
				},
				option || {}
			 );
		
		ksyCKEditor.viewObj.append(
						"<div style='width:100%;'><table width=\"100%\"><tr><td style=\"border:0px;padding:10px 0px;height:"
								+ editorHeight
								+ "px;\"><div id=\""+editorId+"\" name=\""+editorId+"_editor"+"\" style=\"display:none\"></div></td></tr></table></div>");
		
		ksyCKEditor.editor = CKEDITOR.replace(editorId,editorOption);	
		
		ksyCKEditor.getHtml = function() {
			var oEditor = ksyCKEditor.editor;
			var html = oEditor.getData( );
			return html;
		};
		
		ksyCKEditor.setHtml = function(html) {
			var oEditor = ksyCKEditor.editor;
			oEditor.setData(html) ;
		};
		
		
		if(ksyCKEditor.viewObj.bodyHtml!=''){
			ksyCKEditor.setHtml(ksyCKEditor.viewObj.bodyHtml);
		}
		
		return {
			getBody : function()
			{
				return ksyCKEditor.getHtml();
			},
			setBody : function(body){
				ksyCKEditor.setHtml(body);
				ksyCKEditor.editor.resetDirty();
			},
			resetDirty : function(){
				ksyCKEditor.editor.resetDirty();
			}
		}
	}
})(jQuery);