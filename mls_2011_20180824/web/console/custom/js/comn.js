/**
 * 메뉴이동
 * @param url
 * @param menuId
 * @param paramIsLogin
 * @param tabIndex
 * @param subTabIndex
 * @returns {Boolean}
 */
function fncGoMenu(url, menuId, paramIsLogin, tabIndex, subTabIndex){
	if(paramIsLogin && paramIsLogin=='Y'){
		if(!isLogin){
			fncGoLoginPage();
			return false;
		}
	}
	if(url=='N'){
		alert('서비스 준비중입니다.');
	}else{
		
		var f = document.createElement("form");
		f.method = "post";
		f.action = url;

		var paramsStr = fncGetCommonParam(menuId, tabIndex, subTabIndex);
		var paramArr = paramsStr.split("|");
		for ( var i = 0; i < paramArr.length; i++ ) {
			var paramName = paramArr[i].split("=")[0];
			var paramVal = paramArr[i].split("=")[1];
			var inputHidden = document.createElement("input");
			inputHidden.type = "hidden";
			inputHidden.name = paramName;
			inputHidden.value = paramVal;
			f.appendChild(inputHidden);
		}
		document.body.appendChild(f);
		f.submit();
	}
}

//메뉴 url 셋팅
function fncGetCommonParam(menuId, tabIndex, subTabIndex){
	var param = "";
	if(menuId){
		if(param!=""){
			param += "|"+"menuId="+menuId;
		}else{
			param += "menuId="+menuId;
		}
	}
	
	if(tabIndex){
		if(param!=""){
			param += "|"+"tabIndex="+tabIndex;
		}else{
			param += "tabIndex="+tabIndex;
		}
	}

	if(subTabIndex){
		if(param!=""){
			param += "|"+"subTabIndex="+subTabIndex;
		}else{
			param += "subTabIndex="+subTabIndex;
		}
	}
	return param;
}

//페이지 url 셋팅
function fncGetBoardParam(url, datas, page){

	var param = url+'?A=A';
	for(data in datas){
		var dataValue = $('#'+datas[data]).val();
		if(dataValue != ''){
			param += ('&'+datas[data]+'='+dataValue);
		}
	}

	var pageIndex = $('#pageIndex').val();
	pageIndex=null;
	if(pageIndex=='' || pageIndex == null){
		if(page){
		 
			param += '&pageIndex='+page;
		}else{
			if(pageIndex != '' || pageIndex !=null){
			  
				param += '&pageIndex='+pageIndex;
			}
		}
	}
	return param;
}

//로그인페이지로
function fncGoLoginPage(){
	location.href="/console/common/loginPage.page";	
}

//로그아웃
function fncLogout(){
	location.href="/console/common/logout.page";
}

//관리자 메인
function fncGoMain(){
	location.href="/console/main/main.page";
}

//주민번호 체크
function CheckJuminNumber( strJunminNumber1, strJunminNumber2){
	var sum;
    sum = 0;
    sum += strJunminNumber1.charAt(0)*2;
    sum += strJunminNumber1.charAt(1)*3;
    sum += strJunminNumber1.charAt(2)*4;
    sum += strJunminNumber1.charAt(3)*5;
    sum += strJunminNumber1.charAt(4)*6;
    sum += strJunminNumber1.charAt(5)*7;
    sum += strJunminNumber2.charAt(0)*8;
    sum += strJunminNumber2.charAt(1)*9;
    sum += strJunminNumber2.charAt(2)*2;
    sum += strJunminNumber2.charAt(3)*3;
    sum += strJunminNumber2.charAt(4)*4;
    sum += strJunminNumber2.charAt(5)*5;
    check = (11 - sum%11)%10;

    if (strJunminNumber2.charAt(6) != check || (strJunminNumber1.length != 6 && strJunminNumber2.length != 7))
    {        return false;    }

    if(!(strJunminNumber2.charAt(0) == '1' || strJunminNumber2.charAt(0)  == '2' || strJunminNumber2.charAt(0) == '3' || strJunminNumber2.charAt(0) == '4'))
    {    	return false;    }

    return true;
}


//Data 게시판 목록 
function fncSetDataBoard(id){
	$("#"+id).DataTable({
		"searching": false,
		"infoCallback": function( settings, start, end, max, total, pre ) {
			return start +"부터 "+end+"까지/ 총 "+ total+"개";
		},
		"language": {
				"lengthMenu": "_MENU_ 개씩",
				"paginate": {
	  				"previous": "이전",
	  				"next": "다음"
	  			}
			}
	});
}

//파일 업로드 관련 스크립트 시작
//파일 갯수
var fileCnt = 1;
//파일추가
function fncFileAdd(){
	fileCnt+=1
	$('#TdIdFile').append(fncFileHtml(fileCnt));
}
//파일삭제
function fncFileMinus(id){
	$('#DivIdFile'+id).remove();
}
//파일Html
function fncFileHtml(id){
	var html = "";
	html = '<div id="DivIdFile'+id+'" ><input type="file" id="file'+id+'" name="file'+id+'" value="" size="50" style="float:left"><input type="button" id="file'+id+'del" value="파일삭제" onclick="fncFileMinus('+id+');"></div>';
	return html;
}
//파일 업로드 관련 스크립트 끝
